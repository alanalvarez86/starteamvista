unit FEditCatAnfitrion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, Mask, ZetaNumero, ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditCatAnfitrion_DevEx = class(TBaseEdicion_DevEx)
    lblEmpleado: TLabel;
    lblDepto: TLabel;
    lblApePat: TLabel;
    lblApeMat: TLabel;
    lblNombre: TLabel;
    lblStatus: TLabel;
    AN_NUMERO: TZetaDBNumero;
    AN_DEPTO: TZetaDBKeyLookup_DevEx;
    AN_NOMBRES: TDBEdit;
    AN_APE_PAT: TDBEdit;
    AN_APE_MAT: TDBEdit;
    AN_STATUS: TZetaDBKeyCombo;
    Label1: TLabel;
    AN_TEL: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatAnfitrion_DevEx: TEditCatAnfitrion_DevEx;

implementation

uses dVisitantes,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TEditCatAnfitrion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_ANFITRION;
     FirstControl := AN_NUMERO;
     AN_DEPTO.LookupDataset := dmVisitantes.cdsDepto;
     //TipoValorActivo1 := stExpediente;
     HelpContext:= H_VISMGR_EDIT_ANFITRIONES;
end;

procedure TEditCatAnfitrion_DevEx.Connect;
begin
     with dmVisitantes do
     begin
          cdsDepto.Conectar;
          DataSource.DataSet:= cdsAnfitrion;
     end;
end;

end.
