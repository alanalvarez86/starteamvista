inherited EditCatAnfitrion: TEditCatAnfitrion
  Left = 272
  Top = 239
  Caption = 'Edici'#243'n de Anfitriones'
  ClientHeight = 245
  ClientWidth = 442
  PixelsPerInch = 96
  TextHeight = 13
  object lblEmpleado: TLabel [0]
    Left = 68
    Top = 42
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object lblDepto: TLabel [1]
    Left = 41
    Top = 138
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object lblApePat: TLabel [2]
    Left = 28
    Top = 66
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Apellido Paterno:'
  end
  object lblApeMat: TLabel [3]
    Left = 26
    Top = 90
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Apellido Materno:'
  end
  object lblNombre: TLabel [4]
    Left = 57
    Top = 114
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre(s):'
  end
  object lblStatus: TLabel [5]
    Left = 75
    Top = 186
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status:'
  end
  object Label1: TLabel [6]
    Left = 6
    Top = 162
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tel'#233'fono / Extensi'#243'n:'
  end
  inherited PanelBotones: TPanel
    Top = 209
    Width = 442
    TabOrder = 9
    inherited OK: TBitBtn
      Left = 274
    end
    inherited Cancelar: TBitBtn
      Left = 359
    end
  end
  inherited PanelSuperior: TPanel
    Width = 442
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 442
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 116
    end
  end
  object AN_NUMERO: TZetaDBNumero [10]
    Left = 112
    Top = 38
    Width = 70
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 2
    DataField = 'AN_NUMERO'
    DataSource = DataSource
  end
  object AN_DEPTO: TZetaDBKeyLookup [11]
    Left = 112
    Top = 134
    Width = 300
    Height = 21
    TabOrder = 6
    TabStop = True
    WidthLlave = 70
    DataField = 'AN_DEPTO'
    DataSource = DataSource
  end
  object AN_NOMBRES: TDBEdit [12]
    Left = 112
    Top = 110
    Width = 300
    Height = 21
    DataField = 'AN_NOMBRES'
    DataSource = DataSource
    TabOrder = 5
  end
  object AN_APE_PAT: TDBEdit [13]
    Left = 112
    Top = 62
    Width = 300
    Height = 21
    DataField = 'AN_APE_PAT'
    DataSource = DataSource
    TabOrder = 3
  end
  object AN_APE_MAT: TDBEdit [14]
    Left = 112
    Top = 86
    Width = 300
    Height = 21
    DataField = 'AN_APE_MAT'
    DataSource = DataSource
    TabOrder = 4
  end
  object AN_STATUS: TZetaDBKeyCombo [15]
    Left = 112
    Top = 182
    Width = 140
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
    ListaFija = lfAnfitrionStatus
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'AN_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object AN_TEL: TDBEdit [16]
    Left = 112
    Top = 158
    Width = 300
    Height = 21
    DataField = 'AN_TEL'
    DataSource = DataSource
    TabOrder = 7
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 9
  end
end
