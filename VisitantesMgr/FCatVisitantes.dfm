inherited CatVisitantes: TCatVisitantes
  Left = 351
  Top = 205
  Caption = 'Visitantes'
  ClientHeight = 450
  ClientWidth = 602
  ExplicitWidth = 602
  ExplicitHeight = 450
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 602
    ExplicitWidth = 602
    inherited ValorActivo2: TPanel
      Width = 343
      ExplicitWidth = 343
      inherited textoValorActivo2: TLabel
        Left = 338
        ExplicitLeft = 338
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 602
    Height = 30
    Align = alTop
    TabOrder = 1
    object btnFoto: TSpeedButton
      Left = 4
      Top = 2
      Width = 116
      Height = 25
      Hint = 'Ver Fotograf'#237'a'
      AllowAllUp = True
      GroupIndex = 1
      Caption = 'Mostrar Fotograf'#237'a'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000C40E0000C40E00000000000000000000000000000000
        00000000000000000000000000000000000000000000808080FF000000FF0000
        00FF808080FF0000000000000000000000000000000000000000000000000000
        0000808080FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF808080FF000000000000
        0000000000FF808080FF808080FF000000FF000000FF808080FF808080FFFFFF
        00FFFFFF00FF000000FF000000FF808080FF808080FF000000FF000000000000
        0000000000FF808080FF808080FF000000FF808080FF808080FF808080FF8080
        80FF808080FFFFFF00FF000000FF808080FF808080FF000000FF000000000000
        0000000000FF808080FF808080FF000000FF808080FFFFFF00FF808080FF8080
        80FF808080FFFFFF00FF000000FF808080FF808080FF000000FF000000000000
        0000000000FF808080FF808080FF000000FF808080FFFFFFFFFFFFFF00FF8080
        80FF808080FF808080FF000000FF808080FF808080FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF808080FF808080FF8080
        80FF808080FF000000FF000000FFFFFFFFFFFFFFFFFF000000FF000000FFFFFF
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000000FF000000FF000000FF0000
        00FF000000FF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000000FF000000FFFFFF
        FFFF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FFFFFF
        FFFFFFFFFFFF000000FF000000FFFFFFFFFF808080FF000000FFFFFFFFFFFFFF
        FFFF000000FF808080FF00000000000000FF000000FF00000000000000FFFFFF
        FFFF808080FF808080FF808080FFFFFFFFFF808080FF808080FF000000FF0000
        00FF808080FF0000000000000000000000000000000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FF0000000000000000000000000000000000000000000000FFFFFF
        FFFF808080FF808080FF808080FFFFFFFFFF808080FF808080FF808080FFFFFF
        FFFF000000FF0000000000000000000000000000000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FF0000000000000000000000000000000000000000800000FF8000
        00FF800000FF800000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF0000000000000000000000000000000000000000800000FF8000
        00FF800000FF800000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF0000000000000000000000000000000000000000}
      OnClick = btnFotoClick
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 49
    Width = 336
    Height = 401
    Align = alClient
    TabOrder = 2
    object ZetaDBGrid1: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 334
      Height = 399
      Align = alClient
      DataSource = DataSource
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'VI_NUMERO'
          Title.Caption = 'N'#250'mero'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pretty_Visit'
          Title.Caption = 'Nombre'
          Width = 174
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EV_NOMBRE'
          Title.Caption = 'Compa'#241#237'a'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TB_ELEMENT'
          Title.Caption = 'Tipo'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VI_STATUS'
          Title.Caption = 'Status'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VI_NACION'
          Title.Caption = 'Nacionalidad'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VI_MIGRA'
          Title.Caption = 'Informaci'#243'n Migratoria'
          Width = 200
          Visible = True
        end>
    end
  end
  object pnFoto: TPanel [3]
    Left = 336
    Top = 49
    Width = 266
    Height = 401
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 3
    Visible = False
    object GroupBox2: TGroupBox
      Left = 18
      Top = -1
      Width = 233
      Height = 194
      Caption = ' Fotograf'#237'a '
      TabOrder = 0
    end
  end
  inherited DataSource: TDataSource
    Left = 512
    Top = 24
  end
end
