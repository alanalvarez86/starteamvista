unit FEditCatCasetas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaNumero, Mask, ZetaDBTextBox, ZetaKeyLookup, ZetaSmartLists;

type
  TEditCatCasetas = class(TBaseEdicion)
    lblCodigo: TLabel;
    lblNombre: TLabel;
    lblUbica: TLabel;
    lblStatus: TLabel;
    lblCorte: TLabel;
    CA_CODIGO: TDBEdit;
    CA_NOMBRE: TDBEdit;
    CA_UBICA: TDBEdit;
    CA_STATUS: TZetaDBKeyCombo;
    CA_CORTE: TZetaDBTextBox;
    Label1: TLabel;
    CA_ABIERTA: TZetaDBTextBox;
    lblReportes: TLabel;
    RE_CODIGO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatCasetas: TEditCatCasetas;

implementation
uses DVisitantes, DReportes, ZAccesosTress, ZetaCommonClasses;

{$R *.DFM}
procedure TEditCatCasetas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_CASETA;
     FirstControl := CA_CODIGO;
     RE_CODIGO.LookupDataset := dmReportes.cdsLookupReportes;
     HelpContext:= H_VISMGR_EDIT_CASETAS;
     //TipoValorActivo1 := stExpediente;
end;

procedure TEditCatCasetas.Connect;
begin
     dmReportes.cdsLookupReportes.Conectar;
     with dmVisitantes do
     begin
          DataSource.DataSet:= cdsCaseta;
     end;
end;

end.
