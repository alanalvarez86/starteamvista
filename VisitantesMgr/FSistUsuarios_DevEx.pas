unit FSistUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseUsuarios_DevEx, Db, StdCtrls, Buttons, Grids, DBGrids, ZetaDBGrid,
  ExtCtrls, ZetaKeyCombo, FSistBaseUsuarios, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxButtons, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TSistUsuarios_DevEx = class(TSistBaseUsuarios_DevEx)
  procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistUsuarios_DevEx: TSistUsuarios_DevEx;

implementation
uses
    ZetaCommonClasses;
{$R *.DFM}

procedure TSistUsuarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_VISMGR_CONS_USUARIOS;
end;



end.
