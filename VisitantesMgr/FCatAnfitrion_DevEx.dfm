inherited CatAnfitriones_DevEx: TCatAnfitriones_DevEx
  Left = 152
  Top = 132
  Caption = 'Anfitriones'
  ClientHeight = 286
  ClientWidth = 605
  ExplicitWidth = 605
  ExplicitHeight = 286
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 605
    ExplicitWidth = 605
    inherited ValorActivo2: TPanel
      Width = 346
      ExplicitWidth = 346
      inherited textoValorActivo2: TLabel
        Width = 340
        ExplicitLeft = 260
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 605
    Height = 267
    ExplicitWidth = 605
    ExplicitHeight = 267
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object AN_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'AN_NUMERO'
        MinWidth = 70
        Styles.Content = cxStyle3
        Width = 70
      end
      object Pretty_Anfi: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'Pretty_Anfi'
        MinWidth = 250
        Styles.Content = cxStyle4
        Width = 250
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Departamento'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 100
        Styles.Content = cxStyle5
        Width = 178
      end
      object AN_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'AN_STATUS'
        MinWidth = 70
        Styles.Content = cxStyle6
        Width = 80
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 32
    Top = 96
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
