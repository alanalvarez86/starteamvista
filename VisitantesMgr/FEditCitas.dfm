inherited EditCitas: TEditCitas
  Left = 241
  Top = 174
  Caption = 'Edici'#243'n de Citas'
  ClientHeight = 397
  ClientWidth = 433
  PixelsPerInch = 96
  TextHeight = 13
  object lblAsunto: TLabel [0]
    Left = 6
    Top = 157
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Asunto:'
  end
  object lblAnfitrion: TLabel [1]
    Left = 25
    Top = 134
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Visitando A:'
  end
  object lblVisitante: TLabel [2]
    Left = 38
    Top = 112
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Visitante:'
  end
  object lblFecha: TLabel [3]
    Left = 48
    Top = 90
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
  end
  object lblHora: TLabel [4]
    Left = 55
    Top = 67
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora:'
  end
  object lblStatus: TLabel [5]
    Left = 40
    Top = 179
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = #191'Lleg'#243'?:'
  end
  object lblFolio: TLabel [6]
    Left = 56
    Top = 47
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Folio:'
  end
  object lblUsuario: TLabel [7]
    Left = 42
    Top = 200
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario:'
  end
  object US_CODIGO: TZetaDBTextBox [8]
    Left = 85
    Top = 198
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_NOMBRE'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CI_FOLIO: TZetaDBTextBox [9]
    Left = 85
    Top = 45
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CI_FOLIO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CI_FOLIO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 361
    Width = 433
    TabOrder = 9
    inherited OK: TBitBtn
      Left = 265
    end
    inherited Cancelar: TBitBtn
      Left = 350
    end
  end
  inherited PanelSuperior: TPanel
    Width = 433
    TabOrder = 0
    inherited DBNavigator: TDBNavigator
      Left = 14
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 433
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 107
    end
  end
  object CI_FECHA: TZetaDBFecha [13]
    Left = 85
    Top = 85
    Width = 102
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '08/Mar/04'
    Valor = 38054.000000000000000000
    DataField = 'CI_FECHA'
    DataSource = DataSource
  end
  object CI_HORA: TZetaDBHora [14]
    Left = 85
    Top = 63
    Width = 40
    Height = 21
    EditMask = '99:99;0'
    TabOrder = 2
    Text = '    '
    Tope = 24
    Valor = '    '
    DataField = 'CI_HORA'
    DataSource = DataSource
  end
  object CI_ASUNTO: TZetaDBKeyLookup [15]
    Left = 85
    Top = 153
    Width = 320
    Height = 21
    TabOrder = 6
    TabStop = True
    WidthLlave = 70
    DataField = 'CI_ASUNTO'
    DataSource = DataSource
  end
  object gbObserva: TGroupBox [16]
    Left = 16
    Top = 218
    Width = 401
    Height = 137
    Caption = ' Observaciones: '
    TabOrder = 8
    object CI_OBSERVA: TDBMemo
      Left = 2
      Top = 15
      Width = 397
      Height = 120
      Align = alClient
      DataField = 'CI_OBSERVA'
      DataSource = DataSource
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object CI_STATUS: TZetaDBKeyCombo [17]
    Left = 85
    Top = 175
    Width = 120
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    ListaFija = lfCitaStatus
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'CI_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object VI_NUMERO: TZetaDBKeyLookup [18]
    Left = 85
    Top = 108
    Width = 320
    Height = 21
    TabOrder = 4
    TabStop = True
    WidthLlave = 70
    DataField = 'VI_NUMERO'
    DataSource = DataSource
  end
  object AN_NUMERO: TZetaDBKeyLookup [19]
    Left = 85
    Top = 130
    Width = 320
    Height = 21
    TabOrder = 5
    TabStop = True
    WidthLlave = 70
    DataField = 'AN_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 1
  end
end
