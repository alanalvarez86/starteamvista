unit FBuscaAnfitriones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FBaseBusquedas, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons, ExtCtrls,
  ZetaKeyLookup, ZetaKeyCombo, Db, ZetaCommonLists, ImgList;

type
  TBuscaAnfitriones = class(TBaseBusquedas)
    GroupBox1: TGroupBox;
    lblDepto: TLabel;
    AN_DEPTO: TZetaKeyLookup;
    AN_STATUS: TZetaKeyCombo;
    lblStatus: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edNombreChange(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure btnAvzClick(Sender: TObject);
    procedure zGridBuscaDblClick(Sender: TObject);
  private
    { Private declarations }
    FNombre: String;
    FAnfitrion: Integer;
    procedure LlenaStatusAnfitrion( oLista: TStrings; oLFijas: ListasFijas );
  public
    { Public declarations }
  end;

var
  BuscaAnfitriones: TBuscaAnfitriones;

const
     K_ALTURA_SGRID = 110;
     K_ALTURA_CGRID = 340;
     K_ALTURA_CAVZ = 185;

function BuscaAnfitrionDialogo( var iAnfitrion:Integer; var sNombre: String ): Boolean;

implementation
uses DVisitantes, ZetaCommonTools, ZetaCommonClasses, ZetaDialogo, ZVisitantesTools;

{$R *.DFM}
function BuscaAnfitrionDialogo( var iAnfitrion:Integer; var sNombre: String ): Boolean;
begin
     Result := False;
     if ( BuscaAnfitriones = nil ) then
        BuscaAnfitriones := TBuscaAnfitriones.Create( Application );
     try
        if ( BuscaAnfitriones <> nil ) then
        begin
             with BuscaAnfitriones do
             begin
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       sNombre := FNombre;
                       iAnfitrion := FAnfitrion;
                       Result := True;
                  end;
             end;
        end;
     finally
            FreeAndNil( BuscaAnfitriones );
     end;
end;

procedure TBuscaAnfitriones.FormCreate(Sender: TObject);
begin
     inherited;
     AN_DEPTO.LookupDataset := dmVisitantes.cdsDepto;
     with AN_STATUS do
     begin
          LlenaStatusAnfitrion( Lista, lfAnfitrionStatus );
          ItemIndex := 1;
     end;
     //HelpContext:=
end;

procedure TBuscaAnfitriones.FormShow(Sender: TObject);
begin
     inherited;
     dmVisitantes.cdsDepto.Conectar;
     Self.Height := K_ALTURA_SGRID;
end;

procedure TBuscaAnfitriones.edNombreChange(Sender: TObject);
begin
     inherited;
     btnAceptar.Enabled := strLleno( edNombre.Text );
end;

procedure TBuscaAnfitriones.btnBuscarClick(Sender: TObject);
var
   FParamList: TZetaParams;
begin
     FParamList := TZetaParams.Create;
     try
        FParamList.AddString( 'NOMBRE', UPPERCASE( edNombre.Text ) );
        FParamList.AddString( 'DEPTO', UPPERCASE( AN_DEPTO.Llave ) );
        FParamList.AddInteger('STATUS', AN_STATUS.ItemIndex );
        with dmVisitantes do
        begin
             HacerBusquedaAnfitrion( FParamList );
             DataSource.DataSet := cdsAnfitrionLookup;
             if ( cdsAnfitrionLookup.IsEmpty ) then //and ( not pnlAvz.Visible ) ) then
             begin
                  //Height := K_ALTURA_SGRID;
                  //ZGridBusca.Visible := False;
                  //pnlAvz.Visible := False;
                  //inherited btnAvzClick( Sender );
                  //btnAvz.Down := False;
                  ZetaDialogo.ZInformation('Información','No Existe Información Con Los Datos Requeridos', 0);
             end
             else
             begin
                  Height := K_ALTURA_CGRID;
                  ZGridBusca.Visible := True;
             end;
             btnAceptar.Enabled := ZGridBusca.Visible;
        end;
     finally
            FreeAndNil( FParamList );
     end;
end;

procedure TBuscaAnfitriones.btnAvzClick(Sender: TObject);
begin
     inherited;
     pnlAvz.Visible := not pnlAvz.Visible;
     if( pnlAvz.Visible ) then
     begin
          if( zGridBusca.Visible )then
             Self.Height := K_ALTURA_CGRID
          else
              Self.Height := K_ALTURA_CAVZ;
     end
     else
     begin
          if( zGridBusca.Visible )then
             Self.Height := K_ALTURA_CGRID
          else
              Self.Height := K_ALTURA_SGRID;
     end;
     AN_DEPTO.Llave := VACIO;
     AN_STATUS.ItemIndex := 1;
end;

procedure TBuscaAnfitriones.zGridBuscaDblClick(Sender: TObject);
begin
     inherited;
     with dmVisitantes.cdsAnfitrionLookup do
     begin
          FNombre := FieldByName('Pretty_Anfi').AsString;
          FAnfitrion := FieldByName('AN_NUMERO').AsInteger;
          ModalResult := mrOk;
     end;
end;

procedure TBuscaAnfitriones.LlenaStatusAnfitrion( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eAnfitrionStatus;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          begin
               BeginUpdate;
               try
                  for eValueStatus := ( Low( eAnfitrionStatus ) ) to High( eAnfitrionStatus ) do
                  begin
                       AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
                  end;
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( FTemp.Count ) do
                  begin
                       if ( i = 0 ) then
                           Add( '0=Todos' )
                       else
                           Add( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     finally
            FreeAndNil( FTemp );
     end;
end;


end.
