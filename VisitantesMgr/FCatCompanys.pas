unit FCatCompanys;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TCatCompanys = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatCompanys: TCatCompanys;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatCompanys}
procedure TCatCompanys.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     HelpContext := H_VISMGR_CONS_COMPANIAS;
end;

procedure TCatCompanys.Connect;
begin
     with dmVisitantes do
     begin
          cdsEmpVisitante.Conectar;
          DataSource.DataSet := cdsEmpVisitante;
     end;
end;

procedure TCatCompanys.Refresh;
begin
     dmVisitantes.cdsEmpVisitante.Refrescar;
end;

procedure TCatCompanys.Agregar;
begin
     dmVisitantes.cdsEmpVisitante.Agregar;
end;

procedure TCatCompanys.Borrar;
begin
    dmVisitantes.cdsEmpVisitante.Borrar;
end;

procedure TCatCompanys.Modificar;
begin
     dmVisitantes.cdsEmpVisitante.Modificar;
end;

end.
