unit DCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBasicoCliente, Db, DBClient,
  {$ifdef DOS_CAPAS}
     DServerVisReportes,
     DServerReportes,
     DServerSistema,
     //DServerCatalogos,
     DServerVisitantes,
     DServerConsultas,
     DServerGlobalVisitantes,
  {$endif}
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  protected
     {$ifdef DOS_CAPAS}
     FServerGlobal: TdmServerGlobalVisitantes;
     FServerReportes : TdmServerReportes;
     FServerVisReportes: TdmServerVisReportes;
     FServerVisitantes: TdmServerVisitantes;
     FServerSistema: TdmServerSistema;
     //FServerCatalogos: TdmServerCatalogos;
     FServerConsultas: TdmServerConsultas;
    {$endif}
  private
    { Private declarations }
    FParamsCliente : TZetaParams;
    procedure CargaActivosSistema( Parametros: TZetaParams );
  public
    { Public declarations }
    function GetEmpresas: OleVariant;
    procedure CierraEmpresa; override;
    procedure ActualizaClaveUsuario( const sAnterior, sClave: String ); override;
    {$ifdef DOS_CAPAS}
    property ServerGlobal: TdmServerGlobalVisitantes read FServerGlobal;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerVisReportes: TdmServerVisReportes read FServerVisReportes;
    property ServerVisitantes: TdmServerVisitantes read FServerVisitantes;
    property ServerSistema: TdmServerSistema read FServerSistema;
    //property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
   {$endif}
    procedure CargaActivosTodos(Parametros: TZetaParams);override;
  end;

var
  dmCliente: TdmCliente;

implementation
uses FTressShell,
     ZetaCommonTools,
     ZetaTipoEntidad;

{$R *.DFM}



procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     TipoCompany := tcVisitas;
     ModoVisitantes := TRUE;
     FParamsCliente := TZetaParams.Create;
{$ifdef DOS_CAPAS}
     FServerGlobal := TdmServerGlobalVisitantes.Create( Self );
     FServerReportes := TdmServerReportes.Create( self );
     FServerVisReportes := TdmServerVisReportes.Create( self );
     FServerVisitantes := TdmServerVisitantes.Create( self );
     FServerSistema := TdmServerSistema.Create( Self );
     //FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( self );
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerConsultas );
     //FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerVisitantes );
     FreeAndNil( FServerVisReportes );
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerGlobal );
{$endif}
     FParamsCliente.Free;
     inherited;
end;

procedure TdmCliente.CierraEmpresa;
begin
{$ifdef DOS_CAPAS}
     ServerGlobal.CierraEmpresa;
     ServerConsultas.CierraEmpresa;
{$endif}
     inherited CierraEmpresa;
end;

procedure TdmCliente.ActualizaClaveUsuario(const sAnterior, sClave: String);
begin
     inherited ActualizaClaveUsuario( sAnterior, sClave );
     TressShell.SetDataChange( [ enUsuarios ] );
end;

function TdmCliente.GetEmpresas: OleVariant;
begin
     Result := Servidor.GetCompanys( 0, Ord( tcVisitas ) );
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', dmCliente.GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosTodos( Parametros: TZetaParams );
begin
     CargaActivosSistema( Parametros );
end;

end.
