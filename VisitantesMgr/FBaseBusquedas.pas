unit FBaseBusquedas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Db, ImgList;

type
  TBaseBusquedas = class(TForm)
    Panel1: TPanel;
    lblNombre: TLabel;
    edNombre: TEdit;
    btnBuscar: TBitBtn;
    pnlAvz: TPanel;
    zGridBusca: TZetaDBGrid;
    Panel2: TPanel;
    btnAceptar: TBitBtn;
    btnCancelar: TBitBtn;
    DataSource: TDataSource;
    ImageList1: TImageList;
    btnAvz: TSpeedButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAvzClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BaseBusquedas: TBaseBusquedas;

implementation

{$R *.DFM}

procedure TBaseBusquedas.btnCancelarClick(Sender: TObject);
begin
     Close;
end;

procedure TBaseBusquedas.btnAvzClick(Sender: TObject);
var
   oImagen: TBitMap;
begin
     try
        oImagen := TBitMap.Create;
        if( pnlAvz.Visible )then
        begin
             btnAvz.Down := False;
             ImageList1.GetBitmap( 0, oImagen )
        end
        else
        begin
             btnAvz.Down := True;
             ImageList1.GetBitmap( 1, oImagen )
        end;
        btnAvz.Glyph := oImagen;
     finally
            FreeAndNil( oImagen );
     end;
end;

end.
