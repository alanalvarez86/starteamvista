inherited GlobalServidorEmail: TGlobalServidorEmail
  Left = 221
  Top = 163
  Caption = 'Servidor de Correos'
  ClientHeight = 232
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 19
    Top = 12
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Servidor de Correos:'
  end
  object Label2: TLabel [1]
    Left = 59
    Top = 82
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'ID. Usuario:'
  end
  object Label6: TLabel [2]
    Left = 24
    Top = 127
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Direcci'#243'n de Email:'
  end
  object Label7: TLabel [3]
    Left = 14
    Top = 150
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descipci'#243'n del Email:'
  end
  object Label3: TLabel [4]
    Left = 81
    Top = 34
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Puerto:'
  end
  object Label4: TLabel [5]
    Left = 85
    Top = 105
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clave:'
  end
  object Label5: TLabel [6]
    Left = 39
    Top = 197
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Plantilla Default:'
    Visible = False
  end
  object Buscar: TSpeedButton [7]
    Left = 420
    Top = 191
    Width = 25
    Height = 25
    Hint = 'Buscar Directorio'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
      300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
      330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
      333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
      339977FF777777773377000BFB03333333337773FF733333333F333000333333
      3300333777333333337733333333333333003333333333333377333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    Visible = False
  end
  object Label8: TLabel [8]
    Left = 7
    Top = 173
    Width = 108
    Height = 13
    Alignment = taRightJustify
    Caption = 'Direcci'#243'n para Errores:'
  end
  object AutenticacionLBL: TLabel [9]
    Left = 47
    Top = 58
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Autenticaci'#243'n:'
    FocusControl = Autenticacion
  end
  inherited PanelBotones: TPanel
    Top = 196
    Width = 455
    TabOrder = 8
    inherited OK: TBitBtn
      Left = 282
    end
    inherited Cancelar: TBitBtn
      Left = 362
    end
  end
  object UserId: TEdit
    Tag = 44
    Left = 118
    Top = 78
    Width = 132
    Height = 21
    MaxLength = 255
    TabOrder = 3
  end
  object UserEmail: TEdit
    Tag = 44
    Left = 118
    Top = 123
    Width = 300
    Height = 21
    MaxLength = 255
    TabOrder = 5
  end
  object DisplayName: TEdit
    Tag = 44
    Left = 118
    Top = 146
    Width = 300
    Height = 21
    MaxLength = 255
    TabOrder = 6
  end
  object Host: TEdit
    Tag = 44
    Left = 118
    Top = 8
    Width = 132
    Height = 21
    MaxLength = 255
    TabOrder = 0
  end
  object Password: TEdit
    Tag = 44
    Left = 118
    Top = 101
    Width = 132
    Height = 21
    MaxLength = 255
    PasswordChar = '*'
    TabOrder = 4
  end
  object Puerto: TZetaNumero
    Left = 118
    Top = 30
    Width = 131
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
  end
  object Plantilla: TEdit
    Tag = 61
    Left = 118
    Top = 193
    Width = 300
    Height = 21
    TabOrder = 9
    Visible = False
  end
  object ErrorEmail: TEdit
    Tag = 44
    Left = 118
    Top = 169
    Width = 300
    Height = 21
    MaxLength = 255
    TabOrder = 7
  end
  object Autenticacion: TZetaKeyCombo
    Left = 118
    Top = 54
    Width = 132
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    ListaFija = lfAuthTressEmail
    ListaVariable = lvPuesto
    Offset = 1
    Opcional = False
    EsconderVacios = False
  end
  object OpenDialog: TOpenDialog
    Filter = 'Documentos HTML|*.htm;*.html|Todos los Archivos|*.*'
    Left = 384
    Top = 16
  end
end
