unit FCortes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,Db, ExtCtrls, ZetaDBTextBox, StdCtrls, Mask, ZetaFecha,
  ZetaKeyLookup, Grids, DBGrids, ZetaDBGrid, ZetaKeyCombo, ZetaCommonLists,
  Buttons, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx,
  cxButtons;

type
  TCortes_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    lblFechaIni: TLabel;
    zfFecIni: TZetaFecha;
    zfFecFin: TZetaFecha;
    lblFechaFin: TLabel;
    lblStatus: TLabel;
    CO_STATUS: TZetaKeyCombo;
    btnBuscar: TcxButton;
    GroupBox1: TGroupBox;
    lblInicorte: TLabel;
    CO_INI_VIG: TZetaKeyLookup_DevEx;
    lblCierraCorte: TLabel;
    CO_FIN_VIG: TZetaKeyLookup_DevEx;
    CO_FOLIO: TcxGridDBColumn;
    CA_NOMBRE: TcxGridDBColumn;
    CO_INI_FEC: TcxGridDBColumn;
    CO_FIN_FEC: TcxGridDBColumn;
    CO_INI_HOR: TcxGridDBColumn;
    CO_FIN_HOR: TcxGridDBColumn;
    US_INI_CORTE: TcxGridDBColumn;
    US_FIN_CORTE: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zfFecIniValidDate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure CO_STATUSChange(Sender: TObject);
    procedure zfFecFinValidDate(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    procedure PreparaFiltro;
    procedure LimpiaFiltros;
    procedure LlenaStatusCorte( oLista: TStrings; oLFijas: ListasFijas );
   protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  Cortes_DevEx: TCortes_DevEx;

implementation
uses DVisitantes, DSistema, ZetaCommonTools, ZetaCommonClasses;

{$R *.DFM}
procedure TCortes_DevEx.FormCreate(Sender: TObject);
begin                            
     inherited;
     with dmVisitantes do
     begin
          SetFechasInicio;
          zfFecIni.Valor := CorteFechaIni;
          zfFecFin.Valor := CorteFechaFin;
     end;
     with dmSistema do
     begin
          CO_INI_VIG.LookupDataset := cdsUsuariosLookup;
          CO_FIN_VIG.LookupDataset := cdsUsuariosLookup;
     end;
     with CO_STATUS do
     begin
          LlenaStatusCorte( Lista, lfCorteStatus );
          ItemIndex := 1;
     end;
     HelpContext := H_VISMGR_CONS_CORTES;
end;

procedure TCortes_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     LimpiaFiltros;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CO_FOLIO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
     dmVisitantes.cdsCortes.Refrescar;
end;

procedure TCortes_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmVisitantes do
     begin
          cdsCortes.Conectar;
          DataSource.DataSet := cdsCortes;
     end;
     PreparaFiltro;
end;

procedure TCortes_DevEx.LimpiaFiltros;
begin
     CO_INI_VIG.Llave := VACIO;
     CO_FIN_VIG.Llave := VACIO;
end;

procedure TCortes_DevEx.Refresh;
begin
     dmVisitantes.cdsCortes.Refrescar;
     DoBestFit;
end;

procedure TCortes_DevEx.Agregar;
begin
     dmVisitantes.cdsCortes.Agregar;
end;

procedure TCortes_DevEx.Borrar;
begin
     dmVisitantes.cdsCortes.Borrar;
     DoBestFit;
end;

procedure TCortes_DevEx.Modificar;
begin
     dmVisitantes.cdsCortes.Modificar;
     DoBestFit;
end;

function TCortes_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Agregar Cortes Desde VisitantesMgr';
end;

function TCortes_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Puede Borrar Cortes Desde VisitantesMgr';
end;


procedure TCortes_DevEx.LlenaStatusCorte( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eCorteStatus;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          begin
               BeginUpdate;
               try
                  for eValueStatus := Low( eCorteStatus ) to High( eCorteStatus ) do
                       AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( FTemp.Count ) do
                  begin
                       if ( i = 0 ) then
                           Add( '0=Todos' )
                       else
                           Add( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     finally
            FreeAndNil( FTemp );
     end;
end;


procedure TCortes_DevEx.zfFecIniValidDate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          CorteFechaIni := zfFecIni.Valor;
     end;
end;

procedure TCortes_DevEx.zfFecFinValidDate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          CorteFechaFin := zfFecFin.Valor;
     end;
end;

procedure TCortes_DevEx.PreparaFiltro;
begin
     FFiltro := VACIO;
     if strLleno( CO_INI_VIG.Llave )then
         FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( CO_INI_VIG ) = %s', [ EntreComillas( UpperCase( CO_INI_VIG.Llave ) ) ]  ) );
     if strLleno( CO_FIN_VIG.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( CO_FIN_VIG ) = %s', [ EntreComillas( UpperCase( CO_FIN_VIG.Llave ) ) ]  ) );
end;


procedure TCortes_DevEx.btnBuscarClick(Sender: TObject);
begin
     inherited;
     PreparaFiltro;
     dmVisitantes.ObtieneFiltros( FFiltro, 'Corte' );
     DoBestFit;
end;

procedure TCortes_DevEx.CO_STATUSChange(Sender: TObject);
begin
     inherited;
     dmVisitantes.CorteStatus := (CO_STATUS.LlaveEntero - 1);
end;



end.
