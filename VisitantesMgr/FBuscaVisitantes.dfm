inherited BuscaVisitantes: TBuscaVisitantes
  Left = 276
  Top = 227
  Caption = 'B�squeda de Visitantes'
  ClientHeight = 191
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited btnAvz: TSpeedButton
      OnClick = btnAvzClick
    end
    inherited edNombre: TEdit
      OnChange = edNombreChange
    end
    inherited btnBuscar: TBitBtn
      OnClick = btnBuscarClick
    end
  end
  inherited pnlAvz: TPanel
    Height = 144
    object GroupBox1: TGroupBox
      Left = 7
      Top = 1
      Width = 405
      Height = 110
      Caption = ' Opciones Avanzadas '
      TabOrder = 0
      object lblTipo: TLabel
        Left = 51
        Top = 21
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object lblStatus: TLabel
        Left = 42
        Top = 87
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object lblEmpresa: TLabel
        Left = 23
        Top = 65
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Compa��a:'
      end
      object lblAnfitrion: TLabel
        Left = 19
        Top = 43
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'Visitando A:'
      end
      object VI_TIPO: TZetaKeyLookup
        Left = 78
        Top = 17
        Width = 318
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 65
      end
      object VI_STATUS: TZetaKeyCombo
        Left = 78
        Top = 83
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfVisitanteStatus
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
      end
      object EV_NUMERO: TZetaKeyLookup
        Left = 78
        Top = 61
        Width = 318
        Height = 21
        TabOrder = 2
        TabStop = True
        WidthLlave = 65
      end
      object AN_NUMERO: TZetaKeyLookup
        Left = 78
        Top = 39
        Width = 318
        Height = 21
        TabOrder = 3
        TabStop = True
        WidthLlave = 65
      end
    end
  end
  inherited zGridBusca: TZetaDBGrid
    Top = 185
    Height = 0
    DataSource = DataSource
    OnDblClick = zGridBuscaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'VI_NUMERO'
        PickList.Strings = ()
        Title.Caption = 'N�mero'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pretty_Visit'
        PickList.Strings = ()
        Title.Caption = 'Nombre'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VI_TIPO'
        PickList.Strings = ()
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_NUMERO'
        PickList.Strings = ()
        Title.Caption = 'Anfitri�n'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VI_SEXO'
        PickList.Strings = ()
        Title.Caption = 'Sexo'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VI_STATUS'
        PickList.Strings = ()
        Title.Caption = 'Status'
        Width = 100
        Visible = True
      end>
  end
  inherited Panel2: TPanel
    Top = 152
    inherited btnAceptar: TBitBtn
      OnClick = zGridBuscaDblClick
    end
  end
  inherited ImageList1: TImageList
    Top = 16
  end
end
