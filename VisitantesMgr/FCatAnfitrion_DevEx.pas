unit FCatAnfitrion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, Data.DB;

type
  TCatAnfitriones_DevEx = class(TBaseGridLectura_DevEx)
    AN_NUMERO: TcxGridDBColumn;
    Pretty_Anfi: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    AN_STATUS: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  CatAnfitriones_DevEx: TCatAnfitriones_DevEx;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatCondiciones }
procedure TCatAnfitriones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     HelpContext := H_VISMGR_CONS_ANFITRIONES;
end;

procedure TCatAnfitriones_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('AN_NUMERO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatAnfitriones_DevEx.Connect;
begin
     with dmVisitantes do
     begin
          cdsAnfitrion.Conectar;
          DataSource.DataSet := cdsAnfitrion;
     end;
end;

procedure TCatAnfitriones_DevEx.Refresh;
begin
     dmVisitantes.cdsAnfitrion.Refrescar;
     DoBestFit;
end;

procedure TCatAnfitriones_DevEx.Agregar;
begin
     dmVisitantes.cdsAnfitrion.Agregar;
     DoBestFit;
end;

procedure TCatAnfitriones_DevEx.Borrar;
begin
     dmVisitantes.cdsAnfitrion.Borrar;
     DoBestFit;
end;

procedure TCatAnfitriones_DevEx.Modificar;
begin
     dmVisitantes.cdsAnfitrion.Modificar;
     DoBestFit;
end;

procedure TCatAnfitriones_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     AN_NUMERO.Options.Grouping:= FALSE;
     Pretty_Anfi.Options.Grouping:= FALSE;
     TB_ELEMENT.Options.Grouping:= TRUE;
     AN_STATUS.Options.Grouping:= TRUE;
end;

end.
