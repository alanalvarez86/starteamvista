inherited SistEditEmpresas: TSistEditEmpresas
  ClientHeight = 158
  ClientWidth = 422
  PixelsPerInch = 96
  TextHeight = 13
  inherited Label1: TLabel
    Enabled = True
  end
  object Label3: TLabel [2]
    Left = 25
    Top = 95
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = '&Base de Datos:'
  end
  inherited PanelBotones: TPanel
    Top = 122
    Width = 422
    TabOrder = 6
    inherited OK: TBitBtn
      Left = 256
    end
    inherited Cancelar: TBitBtn
      Left = 339
    end
  end
  inherited PanelSuperior: TPanel
    Width = 422
    TabOrder = 4
  end
  inherited PanelIdentifica: TPanel
    Width = 422
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 96
    end
  end
  inherited CM_NOMBRE: TDBEdit
    Width = 316
  end
  inherited CM_CODIGO: TZetaDBEdit
    Enabled = True
  end
  inherited GroupBox1: TGroupBox
    Left = 416
    Width = 41
    Height = 22
    Align = alNone
    TabOrder = 3
    Visible = False
    inherited CM_CONTROL: TDBEdit
      ReadOnly = True
    end
  end
  object DB_CODIGO: TZetaDBKeyLookup [9]
    Left = 101
    Top = 91
    Width = 316
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 85
    DataField = 'DB_CODIGO'
    DataSource = DataSource
  end
end
