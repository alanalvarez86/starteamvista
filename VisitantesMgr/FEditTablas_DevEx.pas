unit FEditTablas_DevEx;

interface

uses ZBaseTablas_DevEx;

type
  TEditTipoID_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditTipoCarro_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditTipoAsunto_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditTipoVisita_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditDepto_DevEx = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

var
  EditTipoID_DevEx: TEditTipoID_DevEx;
  EditTipoCarro_DevEx: TEditTipoCarro_DevEx;
  EditTipoAsunto_DevEx: TEditTipoAsunto_DevEx;
  EditTipoVisita_DevEx: TEditTipoVisita_DevEx;
  EditDepto_DevEx: TEditDepto_DevEx;

implementation

uses DVisitantes,
     ZAccesosTress,
     ZetaCommonClasses;

{ ****** TEditTipoID ******* }

procedure TEditTipoID_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoID;
     HelpContext := H_VISMGR_EDIT_TIDENTIF;
     IndexDerechos := D_TAB_TIPO_ID;
end;

{ ****** TEditTipoCarro ******* }

procedure TEditTipoCarro_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoCarro;
     HelpContext := H_VISMGR_EDIT_TVEHICULO;
     IndexDerechos := D_TAB_TIPO_CARRO;
end;

{ ****** TEditTipoAsunto ******* }

procedure TEditTipoAsunto_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoAsunto;
     HelpContext := H_VISMGR_EDIT_TASUNTO;
     IndexDerechos := D_TAB_TIPO_ASUNTO;
end;

{ ****** TEditTipoVisita ******* }

procedure TEditTipoVisita_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoVisita;
     HelpContext := H_VISMGR_EDIT_TVISITANTE;
     IndexDerechos := D_TAB_TIPO_VISITA;
end;

{ ****** TEditDepto ******* }

procedure TEditDepto_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsDepto;
     HelpContext := H_VISMGR_EDIT_DEPARTAMENTO;
     IndexDerechos := D_TAB_DEPTO;
end;

end.
