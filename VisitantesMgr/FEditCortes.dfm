inherited EditCortes: TEditCortes
  Left = 207
  Top = 164
  Caption = 'Edici�n de Cortes'
  ClientHeight = 401
  ClientWidth = 432
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 365
    Width = 432
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 264
    end
    inherited Cancelar: TBitBtn
      Left = 349
    end
  end
  inherited PanelSuperior: TPanel
    Width = 432
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 432
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 106
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 51
    Width = 432
    Height = 314
    ActivePage = tsGrales
    Align = alClient
    TabOrder = 3
    object tsGrales: TTabSheet
      Caption = 'Generales'
      object lblFolio: TLabel
        Left = 56
        Top = 9
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Folio:'
      end
      object CO_FOLIO: TZetaDBTextBox
        Left = 85
        Top = 7
        Width = 71
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CO_FOLIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CO_FOLIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label1: TLabel
        Left = 163
        Top = 9
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object ztbStatus: TZetaTextBox
        Left = 199
        Top = 7
        Width = 113
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CORTE ABIERTO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object btnCerrarCorte: TSpeedButton
        Left = 318
        Top = 2
        Width = 87
        Height = 22
        Hint = 'Cerrar Corte'
        Caption = 'Cerrar Corte'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000003
          333333333F777773FF333333008888800333333377333F3773F3333077870787
          7033333733337F33373F3308888707888803337F33337F33337F330777880887
          7703337F33337FF3337F3308888000888803337F333777F3337F330777700077
          7703337F33377733337F33088888888888033373FFFFFFFFFF73333000000000
          00333337777777777733333308033308033333337F7F337F7F33333308033308
          033333337F7F337F7F33333308033308033333337F73FF737F33333377800087
          7333333373F77733733333333088888033333333373FFFF73333333333000003
          3333333333777773333333333333333333333333333333333333}
        NumGlyphs = 2
        OnClick = btnCerrarCorteClick
      end
      object CA_NOMBRE: TZetaDBTextBox
        Left = 85
        Top = 28
        Width = 320
        Height = 17
        AutoSize = False
        Caption = 'CA_NOMBRE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CA_NOMBRE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblCaseta: TLabel
        Left = 45
        Top = 30
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Caseta:'
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 50
        Width = 193
        Height = 67
        Caption = ' Inicio Del Corte '
        TabOrder = 0
        object lblFechaIni: TLabel
          Left = 40
          Top = 20
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblHoraIni: TLabel
          Left = 47
          Top = 44
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object CO_INI_FEC: TZetaDBTextBox
          Left = 78
          Top = 18
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CO_INI_FEC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CO_INI_FEC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CO_INI_HOR: TZetaDBTextBox
          Left = 78
          Top = 42
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CO_INI_HOR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CO_INI_HOR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object gbTerminaCorte: TGroupBox
        Left = 224
        Top = 50
        Width = 193
        Height = 67
        Caption = 'Termina Corte '
        TabOrder = 1
        Visible = False
        object lblFechaFin: TLabel
          Left = 41
          Top = 18
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblHoraFin: TLabel
          Left = 48
          Top = 42
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object ztbFecha_Fin: TZetaDBTextBox
          Left = 78
          Top = 18
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ztbFecha_Fin'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CO_FIN_FEC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object ztbHora_Fin: TZetaDBTextBox
          Left = 78
          Top = 42
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'ztbHora_Fin'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CO_FIN_HOR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CO_FIN_FEC: TZetaDBFecha
          Left = 78
          Top = 13
          Width = 102
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '08/Mar/04'
          Valor = 38054
          DataField = 'CO_FIN_FEC'
          DataSource = DataSource
        end
        object CO_FIN_HOR: TZetaDBHora
          Left = 78
          Top = 38
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 1
          Text = '    '
          Tope = 24
          Valor = '    '
          DataField = 'CO_FIN_HOR'
          DataSource = DataSource
        end
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 119
        Width = 409
        Height = 65
        Caption = ' Usuarios '
        TabOrder = 2
        object lblIniCorte: TLabel
          Left = 18
          Top = 17
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicia Corte:'
        end
        object lblFinCorte: TLabel
          Left = 5
          Top = 40
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Termina Corte:'
        end
        object US_NOMBRE: TZetaDBTextBox
          Left = 77
          Top = 15
          Width = 320
          Height = 17
          AutoSize = False
          Caption = 'US_NOMBRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_INI_CORTE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object ztbTermina_corte: TZetaDBTextBox
          Left = 77
          Top = 38
          Width = 320
          Height = 17
          AutoSize = False
          Caption = 'ztbTermina_corte'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_FIN_CORTE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object gbObserva: TGroupBox
        Left = 8
        Top = 186
        Width = 409
        Height = 137
        Caption = ' Observaciones: '
        TabOrder = 3
        Visible = False
        object CO_OBSERVA: TDBMemo
          Left = 2
          Top = 15
          Width = 405
          Height = 120
          Align = alClient
          DataField = 'CO_OBSERVA'
          DataSource = DataSource
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Visitantes'
      ImageIndex = 1
      object ZetaDBGrid1: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 424
        Height = 286
        Align = alClient
        DataSource = dsGrid
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'LI_ENT_HOR'
            Title.Caption = 'Hora Entrada'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LI_SAL_HOR'
            Title.Caption = 'Hora Salida'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LI_NOMBRE'
            Title.Caption = 'Visitante'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LI_EMPRESA'
            Title.Caption = 'Compa��a'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LI_ANFITR'
            Title.Caption = 'Visitando A'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CA_NOM_ENT'
            Title.Caption = 'Caseta Entrada'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CA_NOM_SAL'
            Title.Caption = 'Caseta Salida'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'US_ENT_LIB'
            Title.Caption = 'Registr� Entrada'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'US_SAL_LIB'
            Title.Caption = 'Registr� Salida'
            Width = 150
            Visible = True
          end>
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 1
  end
  object dsGrid: TDataSource
    Left = 364
    Top = 99
  end
end
