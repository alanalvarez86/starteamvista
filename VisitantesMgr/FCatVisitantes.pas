unit FCatVisitantes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, Db, ExtCtrls,imageenview, StdCtrls, DBCtrls,
  Buttons, ZetaDBGrid;

type
  TCatVisitantes = class(TBaseConsulta)
    Panel1: TPanel;
    Panel2: TPanel;
    pnFoto: TPanel;
    GroupBox2: TGroupBox;
    VI_FOTO: TImageEnView;
    btnFoto: TSpeedButton;
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure btnFotoClick(Sender: TObject);
   private
   protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  CatVisitantes: TCatVisitantes;

implementation

uses DVisitantes,
     FBuscaVisitantes,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatVisitantes }
procedure TCatVisitantes.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := TRUE;
     HelpContext := H_VISMGR_CONS_VISITANTES;
end;

procedure TCatVisitantes.Connect;
begin
     with dmVisitantes do
     begin
          cdsVisitante.Conectar;
          DataSource.DataSet := cdsVisitante;
     end;
end;

procedure TCatVisitantes.Refresh;
begin
     dmVisitantes.cdsVisitante.Refrescar;
end;

procedure TCatVisitantes.Agregar;
begin
     dmVisitantes.cdsVisitante.Agregar;
end;

procedure TCatVisitantes.Borrar;
begin
    dmVisitantes.cdsVisitante.Borrar;
end;

procedure TCatVisitantes.Modificar;
begin
     dmVisitantes.cdsVisitante.Modificar;
end;

procedure TCatVisitantes.btnFotoClick(Sender: TObject);
begin
     inherited;
     pnFoto.Visible := (not pnFoto.Visible);
     VI_FOTO.Visible := btnFoto.Down;
     btnFoto.Down := not btnFoto.Down;
end;
procedure TCatVisitantes.DoLookup;
var
   iNumero: Integer;
   sDescription: String;
begin
     //inherited;
     if FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription ) then
     begin
          with dmVisitantes.cdsVisitante do
          begin
               Locate( LookupKeyField, IntTostr( iNumero ), [] );
          end;
     end;
end;

end.
