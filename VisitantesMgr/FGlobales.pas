unit FGlobales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ImgList, Db, ToolWin, ComCtrls, ExtCtrls, StdCtrls,
     ZBaseGlobal,ZBaseGlobal_DevEx,
     ZBaseConsulta,
     ZetaDBTextBox, ZBaseConsultaBotones, cxGraphics, dxBar, cxClasses;

type
  TSistGlobales = class(TBaseConsulta)
    PanelTitulos: TPanel;
    Label1: TLabel;
    RazonLbl: TZetaDBTextBox;
    ImageList: TImageList;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    Identificacion_DevEx: TdxBarLargeButton;
    Bitacora_DevEx: TdxBarLargeButton;
    Seguridad_DevEx: TdxBarLargeButton;
    ServidorEmail_DevEx: TdxBarLargeButton;
    Libro_DevEx: TdxBarLargeButton;
    cxImageList1: TcxImageList;
    dxBarDockControl1: TdxBarDockControl;
    procedure FormCreate(Sender: TObject);
    procedure IdentificacionClick(Sender: TObject);
    procedure SeguridadClick(Sender: TObject);
    procedure ServidorEmailClick(Sender: TObject);
    procedure LibroClick(Sender: TObject);
    procedure BitacoraClick(Sender: TObject);
  private
    { Private declarations }
    function AbrirGlobal( GlobalClass: TBaseGlobalClass_DevEx ): Integer;
    //procedure AplicaGlobalDiccion;
  public
    { Public declarations }
    procedure Connect; override;
    procedure Refresh; override;
  end;

var
  SistGlobales: TSistGlobales;

implementation

uses DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     FGlobalIdentificacion_DevEx,
     FGlobalSeguridad_DevEx,
     FGlobalSeguridad,
     FGlobalServidorEmail_DevEx,
     FGlobalLibro_DevEx,
     FGlobalBitacora_DevEX,
     FTressShell;

{$R *.DFM}

procedure TSistGlobales.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_GLOB_EMPRESAS;
     {$ELSE}
     HelpContext := H00050_Globales_de_empresa;     
     {$endif}

end;

procedure TSistGlobales.Connect;
begin
     RazonLbl.Caption := Global.GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
end;

procedure TSistGlobales.Refresh;
begin
end;
{
function TSistGlobales.AbrirGlobal(GlobalClass: TBaseGlobalClass): Integer;
var
   Forma: TBaseGlobal;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             ShowModal;
             Result := LastAction;
          finally
                 Free;
          end;
     end;
end;}

function TSistGlobales.AbrirGlobal( GlobalClass: TBaseGlobalClass_DevEx ): Integer;
var
   Forma: TBaseGlobal_DevEx;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
           //  if ZAccesosMgr.Revisa( IndexDerechos ) then
           //  begin
                  ShowModal;
                  Result := LastAction;
             {end
             else
             begin
                  ZetaDialogo.ZInformation( self.Caption, 'No tiene derechos para consultar estos globales', 0 );
                  Result := K_EDICION_CANCELAR;
             end;}
          finally
                 Free;
          end;
     end;
end;

{
procedure TSistGlobales.AplicaGlobalDiccion;
begin
     TressShell.CreaArbol( False );
end;
}

procedure TSistGlobales.IdentificacionClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalIdentificacion_DevEx ) = K_EDICION_MODIFICACION ) then
        Connect;
end;

procedure TSistGlobales.SeguridadClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalSeguridad_DevEx );
end;

procedure TSistGlobales.ServidorEmailClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalServidorEmail_DevEx );
end;

procedure TSistGlobales.LibroClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalLibro_DevEx );
end;

procedure TSistGlobales.BitacoraClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalBitacora_DevEx );
end;

end.
