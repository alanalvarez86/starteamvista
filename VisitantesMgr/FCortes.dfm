inherited Cortes: TCortes
  Left = 414
  Top = 134
  Caption = 'Consulta de Cortes'
  ClientWidth = 602
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 602
    inherited ValorActivo2: TPanel
      Width = 343
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 602
    Height = 98
    Align = alTop
    TabOrder = 1
    object lblFechaIni: TLabel
      Left = 8
      Top = 10
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Inicial:'
    end
    object lblFechaFin: TLabel
      Left = 13
      Top = 33
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Final:'
    end
    object lblRegistros: TLabel
      Left = 23
      Top = 77
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Registros:'
    end
    object ztbRegistros: TZetaTextBox
      Left = 74
      Top = 75
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object lblCaseta: TLabel
      Left = 210
      Top = 75
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Caseta:'
    end
    object lblStatus: TLabel
      Left = 38
      Top = 57
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object zfFecIni: TZetaFecha
      Left = 75
      Top = 5
      Width = 100
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '18/Mar/04'
      Valor = 38064
      OnValidDate = zfFecIniValidDate
    end
    object zfFecFin: TZetaFecha
      Tag = 1
      Left = 75
      Top = 29
      Width = 100
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '18/Mar/04'
      Valor = 38064
      OnValidDate = zfFecFinValidDate
    end
    object CA_CODIGO: TZetaKeyLookup
      Left = 250
      Top = 71
      Width = 250
      Height = 21
      TabOrder = 4
      TabStop = True
      WidthLlave = 60
    end
    object CO_STATUS: TZetaKeyCombo
      Left = 75
      Top = 52
      Width = 100
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = CO_STATUSChange
      ListaFija = lfCorteStatus
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
    object btnBuscar: TBitBtn
      Left = 512
      Top = 25
      Width = 73
      Height = 33
      Hint = 'Iniciar B�squeda'
      Caption = 'Buscar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnBuscarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
    end
    object GroupBox1: TGroupBox
      Left = 184
      Top = 1
      Width = 321
      Height = 65
      Caption = ' Vigilante '
      TabOrder = 3
      object lblInicorte: TLabel
        Left = 9
        Top = 18
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'Abre Corte:'
      end
      object lblCierraCorte: TLabel
        Left = 4
        Top = 41
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cierra Corte:'
      end
      object CO_INI_VIG: TZetaKeyLookup
        Left = 66
        Top = 14
        Width = 250
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object CO_FIN_VIG: TZetaKeyLookup
        Left = 66
        Top = 37
        Width = 250
        Height = 21
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [2]
    Left = 0
    Top = 117
    Width = 602
    Height = 156
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CO_FOLIO'
        Title.Caption = 'Folio'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_NOMBRE'
        Title.Caption = 'Caseta'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_INI_FEC'
        Title.Caption = 'Inicia Corte'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_FIN_FEC'
        Title.Caption = 'Termina Corte'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_INI_HOR'
        Title.Caption = 'Hora Inicio'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CO_FIN_HOR'
        Title.Caption = 'Hora Fin'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_INI_CORTE'
        Title.Caption = 'Usuario Inicia Corte'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_FIN_CORTE'
        Title.Caption = 'Usuario Termina Corte'
        Width = 150
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 536
    Top = 8
  end
end
