inherited Libros: TLibros
  Left = 414
  Top = 220
  Caption = 'Consulta de Visitas'
  ClientHeight = 366
  ClientWidth = 596
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 596
    inherited ValorActivo2: TPanel
      Width = 337
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 596
    Height = 118
    Align = alTop
    TabOrder = 1
    object lblFechaIni: TLabel
      Left = 3
      Top = 8
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Inicial:'
    end
    object lblFechaFin: TLabel
      Left = 8
      Top = 30
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Final:'
    end
    object lblRegistros: TLabel
      Left = 19
      Top = 75
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Registros:'
    end
    object ztbRegistros: TZetaTextBox
      Left = 69
      Top = 73
      Width = 60
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object btnAnfitrion: TSpeedButton
      Left = 481
      Top = 47
      Width = 21
      Height = 21
      Hint = 'Buscar Anfitri�n'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnAnfitrionClick
    end
    object btnVisitantes: TSpeedButton
      Left = 481
      Top = 4
      Width = 21
      Height = 21
      Hint = 'Buscar Visitante'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnVisitantesClick
    end
    object lblVisitante: TLabel
      Left = 244
      Top = 8
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Visitante:'
    end
    object lblEmpresa: TLabel
      Left = 235
      Top = 30
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Compa��a:'
    end
    object lblAnfitrion: TLabel
      Left = 231
      Top = 51
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Visitando A:'
    end
    object btnEmpresa: TSpeedButton
      Left = 481
      Top = 26
      Width = 21
      Height = 21
      Hint = 'Buscar Visitante'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33033333333333333F7F3333333333333000333333333333F777333333333333
        000333333333333F777333333333333000333333333333F77733333333333300
        033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
        33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
        3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
        33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
        333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
        333333773FF77333333333370007333333333333777333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnEmpresaClick
    end
    object lblCierraCorte: TLabel
      Left = 211
      Top = 73
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Caseta Entrada:'
    end
    object lblRegEntrada: TLabel
      Left = 205
      Top = 94
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Registr� Entrada:'
    end
    object Label1: TLabel
      Left = 33
      Top = 51
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object zfFecIni: TZetaFecha
      Left = 69
      Top = 3
      Width = 110
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '12/Mar/04'
      Valor = 38058
      OnValidDate = zfFecIniValidDate
    end
    object zfFecFin: TZetaFecha
      Tag = 1
      Left = 69
      Top = 25
      Width = 110
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '12/Mar/04'
      Valor = 38058
      OnValidDate = zfFecIniValidDate
    end
    object AN_NUMERO: TEdit
      Left = 289
      Top = 47
      Width = 190
      Height = 21
      MaxLength = 100
      TabOrder = 5
    end
    object VI_NUMERO: TEdit
      Left = 289
      Top = 4
      Width = 190
      Height = 21
      MaxLength = 100
      TabOrder = 3
    end
    object EV_NUMERO: TEdit
      Left = 289
      Top = 26
      Width = 190
      Height = 21
      MaxLength = 100
      TabOrder = 4
    end
    object LI_ENT_CAS: TZetaKeyLookup
      Left = 289
      Top = 69
      Width = 215
      Height = 21
      TabOrder = 6
      TabStop = True
      WidthLlave = 55
    end
    object LI_ENT_VIG: TZetaKeyLookup
      Left = 289
      Top = 90
      Width = 215
      Height = 21
      TabOrder = 7
      TabStop = True
      WidthLlave = 55
    end
    object btnBuscar: TBitBtn
      Left = 513
      Top = 36
      Width = 73
      Height = 33
      Hint = 'Iniciar B�squeda'
      Caption = 'Buscar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = btnBuscarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
    end
    object LI_STATUS: TZetaKeyCombo
      Left = 69
      Top = 47
      Width = 134
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      ListaFija = lfLibroStatus
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [2]
    Left = 0
    Top = 137
    Width = 596
    Height = 229
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'LI_FOLIO'
        Title.Caption = 'Folio'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_ENT_FEC'
        Title.Caption = 'Fecha Entrada'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_SAL_FEC'
        Title.Caption = 'Fecha Salida'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_ENT_HOR'
        Title.Caption = 'Hora Entrada'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_SAL_HOR'
        Title.Caption = 'Hora Salida'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_NOMBRE'
        Title.Caption = 'Visitante'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_EMPRESA'
        Title.Caption = 'Compa��a'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LI_ANFITR'
        Title.Caption = 'Visitando A'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_NOM_ENT'
        Title.Caption = 'Caseta Entrada'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_NOM_SAL'
        Title.Caption = 'Caseta Salida'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_ENT_LIB'
        Title.Caption = 'Registr� Entrada'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_SAL_LIB'
        Title.Caption = 'Registr� Salida'
        Width = 150
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 400
    Top = 176
  end
end
