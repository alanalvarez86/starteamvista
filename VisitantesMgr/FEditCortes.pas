unit FEditCortes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, ZetaHora, Mask, ZetaFecha, ZetaDBTextBox, ComCtrls, Grids,
  DBGrids, ZetaDBGrid, ZetaSmartLists;

type
  TEditCortes = class(TBaseEdicion)
    PageControl1: TPageControl;
    tsGrales: TTabSheet;
    lblFolio: TLabel;
    CO_FOLIO: TZetaDBTextBox;
    Label1: TLabel;
    ztbStatus: TZetaTextBox;
    btnCerrarCorte: TSpeedButton;
    CA_NOMBRE: TZetaDBTextBox;
    lblCaseta: TLabel;
    GroupBox2: TGroupBox;
    lblFechaIni: TLabel;
    lblHoraIni: TLabel;
    CO_INI_FEC: TZetaDBTextBox;
    CO_INI_HOR: TZetaDBTextBox;
    gbTerminaCorte: TGroupBox;
    lblFechaFin: TLabel;
    lblHoraFin: TLabel;
    ztbFecha_Fin: TZetaDBTextBox;
    ztbHora_Fin: TZetaDBTextBox;
    CO_FIN_FEC: TZetaDBFecha;
    CO_FIN_HOR: TZetaDBHora;
    GroupBox1: TGroupBox;
    lblIniCorte: TLabel;
    lblFinCorte: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    ztbTermina_corte: TZetaDBTextBox;
    gbObserva: TGroupBox;
    CO_OBSERVA: TDBMemo;
    TabSheet1: TTabSheet;
    dsGrid: TDataSource;
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure btnCerrarCorteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    FReadOnly: Boolean;
    FCierraCorte: Boolean;
    procedure CierraCorte(const lHabilita: Boolean);
    procedure CambiaControles( const lHabilita: Boolean );
    procedure CambiaForma;
    { Private declarations }
  protected
     procedure Connect; override;
     procedure EscribirCambios; override;
     function PuedeAgregar(var sMensaje: String): Boolean; override;
     function PuedeBorrar(var sMensaje: String): Boolean; override;
     function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
    procedure CerrarCorte;
  end;

var
  EditCortes: TEditCortes;

const
     K_CORTE_ABIERTO = 'CORTE ABIERTO';
     K_CORTE_CERRADO = 'CORTE CERRADO';

implementation
uses DVisitantes, DSistema, DCliente, ZAccesosTress, ZetaCommonClasses;

{$R *.DFM}
procedure TEditCortes.FormCreate(Sender: TObject);
begin
     inherited;
     FReadOnly := True;
     IndexDerechos := ZAccesosTress.D_REG_CORTE;
     PageControl1.ActivePageIndex := 0;
     //FirstControl := btnCerrarCorte;
     //CO_FIN_VIG.LookupDataset := dmSistema.cdsUsuarios;
     HelpContext := H_VISMGR_EDIT_CORTES;
end;

procedure TEditCortes.FormShow(Sender: TObject);
begin
     inherited;
     FCierraCorte := False;
     //CambiaForma;
end;

procedure TEditCortes.CambiaForma;
begin
     if( dmVisitantes.cdsCortes.FieldByName('CO_FIN_FEC').AsDateTime = NullDateTime )then
     begin
          btnCerrarCorte.Enabled := True;
          ztbStatus.Caption := K_CORTE_ABIERTO;
          CambiaControles( False );
          Self.Height := 320;
     end
     else
     begin
          btnCerrarCorte.Enabled := False;
          CambiaControles( True );
          ztbStatus.Caption := K_CORTE_CERRADO;
          Self.Height := 465;
     end;
end;

procedure TEditCortes.CambiaControles( const lHabilita: Boolean );
begin
     gbObserva.Visible := lHabilita;
     ztbHora_Fin.Visible := lHabilita;
     gbTerminaCorte.Visible := lHabilita;
     ztbFecha_Fin.Visible := lHabilita;
     if FCierraCorte then
     begin
          ztbHora_Fin.Visible := false;
     end
     else
     begin
          CO_FIN_FEC.Visible := false;
          CO_FIN_HOR.Visible := false;
          CO_OBSERVA.ReadOnly := lHabilita and FReadOnly;
     end;
end;

procedure TEditCortes.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmVisitantes do
     begin
          cdsCorteDetail.Refrescar;
          dsGrid.DataSet :=  cdsCorteDetail;
          DataSource.DataSet:= cdsCortes;
     end;
end;

function TEditCortes.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Agregar Cortes Desde VisitantesMgr';
end;

function TEditCortes.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Borrar Cortes Desde VisitantesMgr';
end;

procedure TEditCortes.btnCerrarCorteClick(Sender: TObject);
begin
     inherited;
     FCierraCorte := True;
     CierraCorte( True );
end;

procedure TEditCortes.CierraCorte( const lHabilita: Boolean );
begin
     FReadOnly := TRUE;
     gbTerminaCorte.Visible := lHabilita;
     gbObserva.Visible := lHabilita;
     CO_FIN_FEC.Visible := True;
     CO_FIN_HOR.Visible := True;
     with dmVisitantes.cdsCortes do
     begin
          try
             DisableControls;
             Edit;
             FieldByName('CO_FIN_FEC').AsDateTime := Date;
             FieldByName('CO_FIN_HOR').AsString := FormatDateTime( 'hhnn', Now );
             FieldByName('CO_FIN_VIG').AsInteger := dmCliente.Usuario;
          finally
                 EnableControls;
          end;
     end;
     Self.Height := 465;
end;

procedure TEditCortes.CerrarCorte;
begin
     CierraCorte( True );
     FReadOnly := FALSE;
end;

function TEditCortes.PuedeModificar(var sMensaje: String): Boolean; 
begin
     Result := Inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := ( dmVisitantes.cdsCortes.FieldByName('CO_FIN_FEC').AsDateTime = NullDateTime );
          if not Result then
             sMensaje := 'No Se Pueden Modificar Cortes Cerrados';
     end;
end;

procedure TEditCortes.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     CambiaForma;
     dmVisitantes.cdsCorteDetail.Refrescar;
end;

procedure TEditCortes.EscribirCambios;
begin
     ztbStatus.Caption := K_CORTE_CERRADO;
     inherited EscribirCambios;
end;

end.
