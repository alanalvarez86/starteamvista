unit FSistEditAccesos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEditAccesos,ZBaseEditAccesos_DevEx, ImgList, Buttons, ZetaDBTextBox, StdCtrls, ComCtrls,
  ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxBarBuiltInMenu, cxControls, cxContainer, cxEdit, cxTreeView, cxPC, cxButtons;

type
  TSistEditAccesos_DevEx = class(TZetaEditAccesos_DevEx)
  private
    { Private declarations }
  protected
    function GetTipoDerecho(Nodo: TTreeNode): eDerecho; override;
    function TextoEspecial(const sText: String): Boolean; override;
    function NodoEspecial( const iIndex: Integer ): eTipoNodo; override;
    function EsDerechoEspecial(const iIndex: Integer): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistEditAccesos_DevEx: TSistEditAccesos_DevEx;

implementation
uses ZAccesosTress;
const  D_TEXT_DATOS_CONFI = 'Ver Datos Confidenciales';
       D_TEXT_CANCELAR_PROCESOS = 'Cancelar Procesos';


{$R *.DFM}

function TSistEditAccesos_DevEx.EsDerechoEspecial(const iIndex: Integer): Boolean;
begin
     Result:= inherited EsDerechoEspecial( iIndex );
     case iIndex of
          D_SIST_ADMIN_CASETA : Result := True;
     end;
end;

function TSistEditAccesos_DevEx.GetTipoDerecho(Nodo: TTreeNode): eDerecho;
begin
     with Nodo do
     begin
          if ( Text = D_TEXT_DATOS_CONFI ) then
              Result := edBorraSistKardex
          else if ( Text = D_TEXT_CANCELAR_PROCESOS ) then
              Result := edBaja
          else
              Result := inherited GetTipoDerecho( Nodo );
     end;
end;

function TSistEditAccesos_DevEx.NodoEspecial(const iIndex: Integer): eTipoNodo;
begin
     case iIndex of
          D_SIST_ADMIN_CASETA: Result := tpEspecial; { Administación de Caseta }
     else
         Result := inherited NodoEspecial( iIndex );
     end;
end;



function TSistEditAccesos_DevEx.TextoEspecial(const sText: String): Boolean;
begin
     Result := ( sText = 'Registro' ) or
               ( sText = 'Procesos' ) or
               inherited TextoEspecial( sText );
end;

end.
