unit DEntidadesTress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DEntidades, ZetaTipoEntidad;

type
  TdmEntidadesTress = class(TdmEntidades)
  private
    { Private declarations }
  protected
    procedure CreaEntidades; override;
  public
    { Public declarations }
  end;

implementation

uses ZetaCommonLists;

{$R *.DFM}

{ TdmEntidadesTress }

procedure TdmEntidadesTress.CreaEntidades;
begin
     with AddEntidad( enLibro, 'LIBRO','LI_FOLIO' ) do
     begin
          AddRelacion( enCita,'CI_FOLIO');
          AddRelacion( enVisita, 'VI_NUMERO' );
          AddRelacion( enDepto, 'LI_CDEPTO' );
          AddRelacion( enEmpVisita, 'EV_NUMERO' );
          AddRelacion( enTAsunto, 'LI_ASUNTO' );
          AddRelacion( enTCarro, 'LI_CAR_TIP' );
          AddRelacion( enTipoID, 'LI_TIPO_ID' );
          AddRelacion( enAnfitrio,'AN_NUMERO' );

          //Hay dos relaciones de cada uno Cada uno es un View (Hay cuatros VIEWS y aparte la tabla de cada uno)
          
          AddRelacion( enCasetaEntrada, 'LI_ENT_CAS' );
          AddRelacion( enCasetaSalida, 'LI_SAL_CAS' );
          AddRelacion( enCorteEntrada, 'LI_ENT_COR' );
          AddRelacion( enCorteSalida, 'LI_SAL_COR ' );
     end;

     with AddEntidad( enCita, 'CITA','CI_FOLIO' ) do
     begin
          AddRelacion( enAnfitrio,'AN_NUMERO' );
          AddRelacion( enVisita,'VI_NUMERO' );
          AddRelacion( enTAsunto,'CI_ASUNTO' );
     end;


     with AddEntidad( enAnfitrio, 'ANFITRIO','AN_NUMERO' ) do
     begin
          AddRelacion( enDepto,'AN_DEPTO' );
     end;


     with AddEntidad( enVisita, 'VISITA','VI_NUMERO' ) do
     begin
          AddRelacion( enEmpVisita,'EV_NUMERO' );
          AddRelacion( enTVisita,'VI_TIPO' );
     end;

     with AddEntidad( enCorte, 'CORTE','CO_FOLIO' ) do
     begin
          AddRelacion( enCaseta,'CA_CODIGO' );
     end;

     AddEntidad( enCorteEntrada, 'CORTEENT','CO_FOLIO' );
     AddEntidad( enCorteSalida, 'CORTESAL','CO_FOLIO' );


     with AddEntidad( enCaseta, 'CASETA','CA_CODIGO' ) do
     begin
          AddRelacion( enReporte,'RE_CODIGO' );
     end;

     AddEntidad( enCasetaEntrada, 'CASETAENT','CA_CODIGO' );
     AddEntidad( enCasetaSalida, 'CASETASAL','CA_CODIGO' );

     AddEntidad( enDepto, 'DEPTO','TB_CODIGO' );
     AddEntidad( enEmpVisita, 'EMP_VISI','EV_NUMERO' );
     AddEntidad( enTAsunto, 'TASUNTO','TB_CODIGO' );
     AddEntidad( enTCarro, 'TCARRO','TB_CODIGO' );
     AddEntidad( enTipoID, 'TIPO_ID','TB_CODIGO' );
     AddEntidad( enTVisita, 'TVISITA','TB_CODIGO' );



     { *************************************************************** }
     { ******************** Entidades de Uso General ***************** }

     with AddEntidad( enReporte,'REPORTE','RE_CODIGO') do
     begin
          AddRelacion( enQuerys,'QU_CODIGO');
     end;

     with AddEntidad( enCampoRep,'CAMPOREP','RE_CODIGO,CR_TIPO,CR_POSICIO') do
     begin
          AddRelacion( enReporte,'RE_CODIGO');
     end;

     with AddEntidad( enBitacora,'BITACORA','') do
     begin
          AddRelacion( enProceso,'BI_NUMERO');
     end;

     with AddEntidad( enSuscrip,'SUSCRIP','RE_CODIGO,US_CODIGO') do
     begin
          AddRelacion( enReporte,'RE_CODIGO');
     end;

     with AddEntidad( enMisReportes, 'MISREPOR', 'RE_CODIGO,US_CODIGO' ) do
     begin
          AddRelacion( enReporte,'RE_CODIGO');
     end;

     AddEntidad( enQuerys,'QUERYS','QU_CODIGO');
     AddEntidad( enProceso,'PROCESO','PC_NUMERO');
     AddEntidad( enGlobal,'GLOBAL','');
     AddEntidad( enDiccion,'DICCION','DI_CLASIFI,DI_NOMBRE');

end;
end.
