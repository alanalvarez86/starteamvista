unit DSistema;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBaseSistema, Db, DBClient, ZetaClientDataSet, ZetaServerDataSet;

type
  TdmSistema = class(TdmBaseSistema)
    procedure cdsEmpresasAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpresasAlCrearCampos(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure CM_CTRL_RLGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  end;

var
  dmSistema: TdmSistema;

implementation

{$R *.DFM}   
uses
    ZetaCommonLists, DCliente, ZetaBusqueda;

procedure TdmSistema.cdsEmpresasAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsEmpresas.Data := ServerSistema.GetEmpresas( Ord( tcVisitas ), dmCliente.GetGrupoActivo );
end;

procedure TdmSistema.cdsEmpresasAlCrearCampos(Sender: TObject);
begin
  inherited;
             
     with cdsEmpresas do
     begin
          FieldByName( 'CM_CTRL_RL' ).OnGetText := CM_CTRL_RLGetText;
     end;
end;

procedure TdmSistema.CM_CTRL_RLGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tc3Datos ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tc3Datos ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcRecluta ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcRecluta ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcVisitas ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcVisitas ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcOtro ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcOtro ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tcPresupuesto ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tcPresupuesto ) )
     else if Sender.AsString = ObtieneElemento( lfTipoCompany, Ord( tc3Prueba ) ) then
        Text := ObtieneElemento( lfTipoCompanyName, Ord( tc3Prueba ) );
end;

end.
