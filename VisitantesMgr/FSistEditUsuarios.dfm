inherited SistEditUsuarios: TSistEditUsuarios
  ClientHeight = 410
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 377
  end
  inherited PageControl: TPageControl
    Height = 279
    inherited Acceso: TTabSheet
      inherited Label6: TLabel
        Top = 55
      end
      inherited Label3: TLabel
        Top = 97
      end
      inherited Label8: TLabel
        Top = 118
      end
      inherited Label11: TLabel
        Top = 139
      end
      object Label15: TLabel [5]
        Left = 4
        Top = 29
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Usuario del &dominio:'
        FocusControl = US_DOMAIN
      end
      object btnDominioUsuario: TSpeedButton [6]
        Left = 394
        Top = 25
        Width = 20
        Height = 20
        Hint = #191' Usuario repetido ?'
        Enabled = False
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          555555555555555555555555555555555555555555FF55555555555559055555
          55555555577FF5555555555599905555555555557777F5555555555599905555
          555555557777FF5555555559999905555555555777777F555555559999990555
          5555557777777FF5555557990599905555555777757777F55555790555599055
          55557775555777FF5555555555599905555555555557777F5555555555559905
          555555555555777FF5555555555559905555555555555777FF55555555555579
          05555555555555777FF5555555555557905555555555555777FF555555555555
          5990555555555555577755555555555555555555555555555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnDominioUsuarioClick
      end
      inherited US_PASSWRD: TDBEdit
        Top = 51
        TabOrder = 2
      end
      inherited US_CAMBIA: TDBCheckBox
        Top = 75
        TabOrder = 3
      end
      inherited GR_CODIGO: TZetaDBKeyLookup
        Top = 93
        TabOrder = 4
      end
      inherited US_NIVEL: TZetaDBKeyCombo
        Top = 114
        TabOrder = 5
      end
      inherited GroupBox1: TGroupBox
        Top = 155
        TabOrder = 7
      end
      inherited US_LUGAR: TDBEdit
        Top = 135
        TabOrder = 6
      end
      object US_DOMAIN: TDBEdit
        Left = 101
        Top = 25
        Width = 289
        Height = 21
        DataField = 'US_DOMAIN'
        DataSource = DataSource
        TabOrder = 1
      end
    end
  end
  inherited Pn_Codigos: TPanel
    object US_ACTIVO: TDBCheckBox
      Left = 321
      Top = 8
      Width = 72
      Height = 15
      Caption = #191' Activo ?'
      DataField = 'US_ACTIVO'
      DataSource = DataSource
      TabOrder = 2
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
end
