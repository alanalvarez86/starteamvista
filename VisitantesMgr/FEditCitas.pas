unit FEditCitas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaDBTextBox,
  ZetaKeyLookup, ZetaHora, Mask, ZetaFecha, ZetaKeyCombo, ZetaSmartLists;

type
  TEditCitas = class(TBaseEdicion)
    lblAsunto: TLabel;
    lblAnfitrion: TLabel;
    lblVisitante: TLabel;
    lblFecha: TLabel;
    lblHora: TLabel;
    lblStatus: TLabel;
    CI_FECHA: TZetaDBFecha;
    CI_HORA: TZetaDBHora;
    lblFolio: TLabel;
    CI_ASUNTO: TZetaDBKeyLookup;
    gbObserva: TGroupBox;
    lblUsuario: TLabel;
    US_CODIGO: TZetaDBTextBox;
    CI_STATUS: TZetaDBKeyCombo;
    CI_OBSERVA: TDBMemo;
    CI_FOLIO: TZetaDBTextBox;
    VI_NUMERO: TZetaDBKeyLookup;
    AN_NUMERO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
     procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCitas: TEditCitas;

implementation
uses DVisitantes, DSistema, ZAccesosTress, FBuscaAnfitriones, ZetaCommonTools,
     ZetaCommonLists, FBuscaVisitantes, ZetaCommonClasses;
{$R *.DFM}

procedure TEditCitas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_REG_CITAS;
     FirstControl := CI_HORA;
     with dmVisitantes do
     begin
          CI_ASUNTO.LookupDataset := cdsTipoAsunto;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          VI_NUMERO.LookupDataset := cdsVisitaCitas;
     end;
     CI_STATUS.ItemIndex := 0;
     HelpContext := H_VISMGR_EDIT_CITAS;     
end;

procedure TEditCitas.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmVisitantes do
     begin
          cdsAnfitrion.Conectar;
          cdsVisitanteLookup.Conectar;
          cdsTipoAsunto.Conectar;
          DataSource.DataSet:= cdsCitas;
     end;
end;

end.
