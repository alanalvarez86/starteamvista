unit FSistUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseUsuarios, Db, StdCtrls, Buttons, Grids, DBGrids, ZetaDBGrid,
  ExtCtrls, ZetaKeyCombo;

type
  TSistUsuarios = class(TSistBaseUsuarios)
  procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SistUsuarios: TSistUsuarios;

implementation
uses
    ZetaCommonClasses;
{$R *.DFM}

procedure TSistUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_VISMGR_CONS_USUARIOS;
end;



end.
