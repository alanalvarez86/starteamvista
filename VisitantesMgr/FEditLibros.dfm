inherited EditLibros: TEditLibros
  Left = 454
  Top = 167
  Caption = 'Edici'#243'n de Visitas'
  ClientHeight = 386
  ClientWidth = 393
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 350
    Width = 393
    inherited OK: TBitBtn
      Left = 225
    end
    inherited Cancelar: TBitBtn
      Left = 310
    end
  end
  inherited PanelSuperior: TPanel
    Width = 393
  end
  inherited PanelIdentifica: TPanel
    Width = 393
    inherited ValorActivo2: TPanel
      Width = 67
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 51
    Width = 393
    Height = 299
    ActivePage = tsVehiculo
    Align = alClient
    TabOrder = 3
    object tsVehiculo: TTabSheet
      Caption = 'Visitante'
      ImageIndex = 3
      object lblVisitante: TLabel
        Left = 29
        Top = 44
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Visitante:'
      end
      object btnVisitantes: TSpeedButton
        Left = 351
        Top = 40
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnVisitantesClick
      end
      object lblEmpresa: TLabel
        Left = 20
        Top = 66
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Compa'#241#237'a:'
      end
      object btnEmpresa: TSpeedButton
        Left = 351
        Top = 62
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnEmpresaClick
      end
      object lblAsunto: TLabel
        Left = 36
        Top = 88
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asunto:'
      end
      object lblTipoId: TLabel
        Left = 6
        Top = 109
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Identificaci'#243'n:'
      end
      object lblLicId: TLabel
        Left = 29
        Top = 131
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Licencia:'
      end
      object lblGafete: TLabel
        Left = 37
        Top = 153
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Gafete:'
      end
      object lblStatus: TLabel
        Left = 39
        Top = 24
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object LI_STATUS: TZetaDBTextBox
        Left = 74
        Top = 22
        Width = 150
        Height = 17
        AutoSize = False
        Caption = 'LI_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LI_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblFolio: TLabel
        Left = 47
        Top = 5
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Folio:'
      end
      object LI_FOLIO: TZetaDBTextBox
        Left = 74
        Top = 3
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'LI_FOLIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LI_FOLIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LI_ASUNTO: TZetaDBKeyLookup
        Left = 74
        Top = 84
        Width = 300
        Height = 21
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'LI_ASUNTO'
        DataSource = DataSource
      end
      object LI_TIPO_ID: TZetaDBKeyLookup
        Left = 74
        Top = 105
        Width = 300
        Height = 21
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'LI_TIPO_ID'
        DataSource = DataSource
      end
      object LI_ID: TDBEdit
        Left = 74
        Top = 127
        Width = 275
        Height = 21
        DataField = 'LI_ID'
        DataSource = DataSource
        TabOrder = 4
      end
      object LI_GAFETE: TDBEdit
        Left = 74
        Top = 149
        Width = 275
        Height = 21
        CharCase = ecUpperCase
        DataField = 'LI_GAFETE'
        DataSource = DataSource
        TabOrder = 5
      end
      object gbCarro: TGroupBox
        Left = 0
        Top = 166
        Width = 385
        Height = 105
        Align = alBottom
        Caption = ' Veh'#237'culo '
        TabOrder = 6
        object lblCajon: TLabel
          Left = 41
          Top = 80
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Caj'#243'n:'
        end
        object lblDescrip: TLabel
          Left = 12
          Top = 59
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descripci'#243'n:'
        end
        object lblPlacas: TLabel
          Left = 36
          Top = 37
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Placas:'
        end
        object lblCarro: TLabel
          Left = 25
          Top = 16
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Veh'#237'culo:'
        end
        object LI_CAR_EST: TDBEdit
          Left = 74
          Top = 76
          Width = 150
          Height = 21
          DataField = 'LI_CAR_EST'
          DataSource = DataSource
          TabOrder = 3
        end
        object LI_CAR_DES: TDBEdit
          Left = 74
          Top = 55
          Width = 275
          Height = 21
          DataField = 'LI_CAR_DES'
          DataSource = DataSource
          TabOrder = 2
        end
        object LI_CAR_PLA: TDBEdit
          Left = 74
          Top = 33
          Width = 150
          Height = 21
          DataField = 'LI_CAR_PLA'
          DataSource = DataSource
          TabOrder = 1
        end
        object LI_CAR_TIP: TZetaDBKeyLookup
          Left = 74
          Top = 12
          Width = 300
          Height = 21
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'LI_CAR_TIP'
          DataSource = DataSource
        end
      end
      object LI_NOMBRE: TDBEdit
        Left = 74
        Top = 40
        Width = 275
        Height = 21
        DataField = 'LI_NOMBRE'
        DataSource = DataSource
        TabOrder = 0
        OnChange = LI_NOMBREChange
      end
      object LI_EMPRESA: TDBEdit
        Left = 74
        Top = 62
        Width = 275
        Height = 21
        DataField = 'LI_EMPRESA'
        DataSource = DataSource
        TabOrder = 1
        OnChange = LI_EMPRESAChange
      end
    end
    object tsGeneral: TTabSheet
      Caption = 'Visitando A'
      object lblCita: TLabel
        Left = 55
        Top = 12
        Width = 21
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cita:'
      end
      object lblAnfitrion: TLabel
        Left = 36
        Top = 34
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre:'
      end
      object btnAnfitrion: TSpeedButton
        Left = 355
        Top = 30
        Width = 21
        Height = 21
        Hint = 'Buscar Anfitri'#243'n'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnAnfitrionClick
      end
      object CI_FOLIO: TZetaDBTextBox
        Left = 78
        Top = 10
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'CI_FOLIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CI_FOLIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblDepto: TLabel
        Left = 6
        Top = 58
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Departamento:'
      end
      object btnDepto: TSpeedButton
        Left = 355
        Top = 54
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = btnDeptoClick
      end
      object LI_DEPTO: TDBEdit
        Left = 78
        Top = 54
        Width = 274
        Height = 21
        DataField = 'LI_DEPTO'
        DataSource = DataSource
        TabOrder = 1
        OnChange = LI_DEPTOChange
      end
      object gbOtros: TGroupBox
        Left = 0
        Top = 85
        Width = 385
        Height = 89
        Align = alBottom
        Caption = ' Detalles '
        TabOrder = 2
        object lblTexto1: TLabel
          Left = 27
          Top = 19
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #1:'
        end
        object lblTexto3: TLabel
          Left = 27
          Top = 65
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #3:'
        end
        object lblTexto2: TLabel
          Left = 27
          Top = 42
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #2:'
        end
        object LI_TEXTO1: TDBEdit
          Left = 77
          Top = 15
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO1'
          DataSource = DataSource
          TabOrder = 0
        end
        object LI_TEXTO2: TDBEdit
          Left = 77
          Top = 38
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO2'
          DataSource = DataSource
          TabOrder = 1
        end
        object LI_TEXTO3: TDBEdit
          Left = 77
          Top = 61
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO3'
          DataSource = DataSource
          TabOrder = 2
        end
      end
      object gbObserva: TGroupBox
        Left = 0
        Top = 174
        Width = 385
        Height = 97
        Align = alBottom
        Caption = ' Observaciones '
        TabOrder = 3
        object LI_OBSERVA: TDBMemo
          Left = 2
          Top = 15
          Width = 381
          Height = 80
          Align = alClient
          DataField = 'LI_OBSERVA'
          DataSource = DataSource
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object LI_ANFITR: TDBEdit
        Left = 78
        Top = 30
        Width = 275
        Height = 21
        DataField = 'LI_ANFITR'
        DataSource = DataSource
        TabOrder = 0
        OnChange = LI_ANFITRChange
      end
    end
    object tsIO: TTabSheet
      Caption = 'Entradas/Salidas'
      ImageIndex = 1
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 385
        Height = 118
        Align = alTop
        Caption = ' Entradas '
        TabOrder = 0
        object lblFecEnt: TLabel
          Left = 25
          Top = 35
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblHorEnt: TLabel
          Left = 32
          Top = 54
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object lblEntCas: TLabel
          Left = 22
          Top = 74
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Caseta:'
        end
        object lblRegEntrada: TLabel
          Left = 16
          Top = 93
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = 'Registr'#243':'
        end
        object lblCorteEntrada: TLabel
          Left = 30
          Top = 15
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Corte:'
        end
        object LI_ENT_COR: TZetaDBTextBox
          Left = 61
          Top = 13
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_COR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_COR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_FEC: TZetaDBTextBox
          Left = 61
          Top = 33
          Width = 110
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_FEC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_FEC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_HOR: TZetaDBTextBox
          Left = 61
          Top = 52
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_HOR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_HOR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_CAS: TZetaDBTextBox
          Left = 61
          Top = 72
          Width = 268
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_CAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_CAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_VIG: TZetaDBTextBox
          Left = 61
          Top = 91
          Width = 268
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_VIG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_ENT_LIB'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 118
        Width = 385
        Height = 153
        Align = alClient
        Caption = ' Salidas '
        TabOrder = 1
        object lblSalFec: TLabel
          Left = 24
          Top = 41
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblHorSal: TLabel
          Left = 31
          Top = 62
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object lblSalCas: TLabel
          Left = 21
          Top = 84
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Caseta:'
        end
        object lblRegSalida: TLabel
          Left = 15
          Top = 106
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = 'Registr'#243':'
        end
        object lblCorteSal: TLabel
          Left = 29
          Top = 18
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Corte:'
        end
        object LI_SAL_COR: TZetaDBTextBox
          Left = 60
          Top = 16
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_COR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_SAL_COR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_VIG: TZetaDBTextBox
          Left = 60
          Top = 104
          Width = 267
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_VIG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_SAL_LIB'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_FEC2: TZetaDBTextBox
          Left = 60
          Top = 39
          Width = 100
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_FEC2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_SAL_FEC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_HOR2: TZetaDBTextBox
          Left = 60
          Top = 60
          Width = 40
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_HOR2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_SAL_HOR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_CAS2: TZetaDBTextBox
          Left = 60
          Top = 82
          Width = 267
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_CAS2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CA_NOM_SAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_FEC: TZetaDBFecha
          Left = 60
          Top = 36
          Width = 100
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '17/Mar/04'
          Valor = 38063.000000000000000000
          DataField = 'LI_SAL_FEC'
          DataSource = DataSource
        end
        object LI_SAL_HOR: TZetaDBHora
          Left = 60
          Top = 58
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 1
          Text = '    '
          Tope = 24
          Valor = '    '
          DataField = 'LI_SAL_HOR'
          DataSource = DataSource
        end
        object LI_SAL_CAS: TZetaDBKeyLookup
          Left = 60
          Top = 80
          Width = 293
          Height = 21
          TabOrder = 2
          TabStop = True
          WidthLlave = 66
          DataField = 'LI_SAL_CAS'
          DataSource = DataSource
        end
        object LI_DEVGAF: TDBCheckBox
          Left = 41
          Top = 128
          Width = 97
          Height = 17
          Caption = 'Entreg'#243' Gafete'
          DataField = 'LI_DEVGAF'
          DataSource = DataSource
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 412
    Top = 1
  end
end
