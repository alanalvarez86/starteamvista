unit FGlobalIdentificacion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     {$ifndef VER130}MaskUtils,{$endif}
     ZBaseGlobal;

type
  TGlobalIdentificacion = class(TBaseGlobal)
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label1: TLabel;
    RazonSocial: TEdit;
    Calle: TEdit;
    Colonia: TEdit;
    Ciudad: TEdit;
    CodigoPostal: TEdit;
    Telefono: TEdit;
    RepresentanteLegal: TEdit;
    RFC: TMaskEdit;
    Infonavit: TMaskEdit;
    Entidad: TEdit;
    Label3: TLabel;
    Plantillas: TEdit;
    Buscar: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalIdentificacion: TGlobalIdentificacion;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalIdentificacion.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos          := D_CAT_CONFI_GLOBALES;
     RazonSocial.Tag        := K_GLOBAL_RAZON_EMPRESA;
     Calle.Tag              := K_GLOBAL_CALLE_EMPRESA;
     Colonia.Tag            := K_GLOBAL_COLONIA_EMPRESA;
     Ciudad.Tag             := K_GLOBAL_CIUDAD_EMPRESA;
     Entidad.Tag            := K_GLOBAL_ENTIDAD_EMPRESA;
     CodigoPostal.Tag       := K_GLOBAL_CP_EMPRESA;
     Telefono.Tag           := K_GLOBAL_TEL_EMPRESA;
     RepresentanteLegal.Tag := K_GLOBAL_REPRESENTANTE;
     RFC.Tag                := K_GLOBAL_RFC_EMPRESA;
     //Infonavit.Tag          := K_GLOBAL_INFONAVIT_EMPRESA;
     Plantillas.Tag         := K_GLOBAL_DIR_PLANT;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_GE_IDENTIF;
     {$else}
     HelpContext := H00050_Globales_de_empresa;
     {$endif}     
end;

procedure TGlobalIdentificacion.BuscarClick(Sender: TObject);
begin
     inherited;
     Plantillas.Text := BuscarDirectorio( Plantillas.Text );
end;

end.
