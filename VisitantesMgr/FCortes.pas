unit FCortes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, ZetaDBTextBox, StdCtrls, Mask, ZetaFecha,
  ZetaKeyLookup, Grids, DBGrids, ZetaDBGrid, ZetaKeyCombo, ZetaCommonLists,
  Buttons;

type
  TCortes = class(TBaseConsulta)
    Panel1: TPanel;
    lblFechaIni: TLabel;
    zfFecIni: TZetaFecha;
    zfFecFin: TZetaFecha;
    lblFechaFin: TLabel;
    lblRegistros: TLabel;
    ztbRegistros: TZetaTextBox;
    lblCaseta: TLabel;
    CA_CODIGO: TZetaKeyLookup;
    lblStatus: TLabel;
    ZetaDBGrid1: TZetaDBGrid;
    CO_STATUS: TZetaKeyCombo;
    btnBuscar: TBitBtn;
    GroupBox1: TGroupBox;
    lblInicorte: TLabel;
    CO_INI_VIG: TZetaKeyLookup;
    lblCierraCorte: TLabel;
    CO_FIN_VIG: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zfFecIniValidDate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure CO_STATUSChange(Sender: TObject);
    procedure zfFecFinValidDate(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    procedure CuentaRegistros;
    procedure PreparaFiltro;
    procedure LimpiaFiltros;
    procedure LlenaStatusCorte( oLista: TStrings; oLFijas: ListasFijas );
   protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  Cortes: TCortes;

implementation
uses DVisitantes, DSistema, ZetaCommonTools, ZetaCommonClasses;

{$R *.DFM}
procedure TCortes.FormCreate(Sender: TObject);
begin                            
     inherited;
     with dmVisitantes do
     begin
          SetFechasInicio;
          zfFecIni.Valor := CorteFechaIni;
          zfFecFin.Valor := CorteFechaFin;
     end;
     CA_CODIGO.LookupDataset := dmVisitantes.cdsCaseta;
     with dmSistema do
     begin
          CO_INI_VIG.LookupDataset := cdsUsuariosLookup;
          CO_FIN_VIG.LookupDataset := cdsUsuariosLookup;
     end;
     with CO_STATUS do
     begin
          LlenaStatusCorte( Lista, lfCorteStatus );
          ItemIndex := 1;
     end;
     HelpContext := H_VISMGR_CONS_CORTES;
end;

procedure TCortes.FormShow(Sender: TObject);
begin
     inherited;
     LimpiaFiltros;
     CuentaRegistros;
end;

procedure TCortes.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmVisitantes do
     begin
          cdsCortes.Conectar;
          DataSource.DataSet := cdsCortes;
     end;
     PreparaFiltro;
end;

procedure TCortes.LimpiaFiltros;
begin
     CO_INI_VIG.Llave := VACIO;
     CO_FIN_VIG.Llave := VACIO;
     CA_CODIGO.Llave := VACIO;
end;

procedure TCortes.Refresh;
begin
     dmVisitantes.cdsCortes.Refrescar;
end;

procedure TCortes.Agregar;
begin
     dmVisitantes.cdsCortes.Agregar;
end;

procedure TCortes.Borrar;
begin
     dmVisitantes.cdsCortes.Borrar;
end;

procedure TCortes.Modificar;
begin
     dmVisitantes.cdsCortes.Modificar;
end;

function TCortes.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Agregar Cortes Desde VisitantesMgr';
end;

function TCortes.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Puede Borrar Cortes Desde VisitantesMgr';
end;

procedure TCortes.CuentaRegistros;
begin
     ztbRegistros.Caption := InttoStr( dmVisitantes.cdsCortes.RecordCount );
end;

procedure TCortes.LlenaStatusCorte( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eCorteStatus;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          begin
               BeginUpdate;
               try
                  for eValueStatus := Low( eCorteStatus ) to High( eCorteStatus ) do
                       AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( FTemp.Count ) do
                  begin
                       if ( i = 0 ) then
                           Add( '0=Todos' )
                       else
                           Add( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     finally
            FreeAndNil( FTemp );
     end;
end;


procedure TCortes.zfFecIniValidDate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          CorteFechaIni := zfFecIni.Valor;
     end;
end;

procedure TCortes.zfFecFinValidDate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          CorteFechaFin := zfFecFin.Valor;
     end;
end;

procedure TCortes.PreparaFiltro;
begin
     FFiltro := VACIO;
     if strLleno( CO_INI_VIG.Llave )then
         FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( CO_INI_VIG ) = %s', [ EntreComillas( UpperCase( CO_INI_VIG.Llave ) ) ]  ) );
     if strLleno( CO_FIN_VIG.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( CO_FIN_VIG ) = %s', [ EntreComillas( UpperCase( CO_FIN_VIG.Llave ) ) ]  ) );
     if strLleno( CA_CODIGO.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( CA_CODIGO ) = %s', [ EntreComillas( UpperCase( CA_CODIGO.Llave ) ) ]  ) );
end;


procedure TCortes.btnBuscarClick(Sender: TObject);
begin
     inherited;
     PreparaFiltro;
     dmVisitantes.ObtieneFiltros( FFiltro, 'Corte' );
     CuentaRegistros;
end;

procedure TCortes.CO_STATUSChange(Sender: TObject);
begin
     inherited;
     dmVisitantes.CorteStatus := (CO_STATUS.LlaveEntero - 1);
end;



end.
