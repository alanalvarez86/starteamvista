unit FEditLibros;

interface

uses     
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaDBTextBox,
  ComCtrls, Mask, ZetaKeyCombo, ZetaKeyLookup, ZetaHora, ZetaFecha,
  ZetaSmartLists;

type
  TEditLibros = class(TBaseEdicion)
    PageControl1: TPageControl;
    tsGeneral: TTabSheet;
    lblCita: TLabel;
    lblAnfitrion: TLabel;
    tsIO: TTabSheet;
    btnAnfitrion: TSpeedButton;
    GroupBox3: TGroupBox;
    lblFecEnt: TLabel;
    lblHorEnt: TLabel;
    lblEntCas: TLabel;
    lblRegEntrada: TLabel;
    GroupBox4: TGroupBox;
    lblSalFec: TLabel;
    LI_SAL_FEC: TZetaDBFecha;
    LI_SAL_HOR: TZetaDBHora;
    lblHorSal: TLabel;
    lblSalCas: TLabel;
    LI_SAL_CAS: TZetaDBKeyLookup;
    lblRegSalida: TLabel;
    CI_FOLIO: TZetaDBTextBox;
    lblCorteEntrada: TLabel;
    LI_ENT_COR: TZetaDBTextBox;
    lblCorteSal: TLabel;
    LI_SAL_COR: TZetaDBTextBox;
    LI_DEPTO: TDBEdit;
    lblDepto: TLabel;
    btnDepto: TSpeedButton;
    tsVehiculo: TTabSheet;
    lblVisitante: TLabel;
    btnVisitantes: TSpeedButton;
    lblEmpresa: TLabel;
    btnEmpresa: TSpeedButton;
    lblAsunto: TLabel;
    LI_ASUNTO: TZetaDBKeyLookup;
    lblTipoId: TLabel;
    LI_TIPO_ID: TZetaDBKeyLookup;
    lblLicId: TLabel;
    LI_ID: TDBEdit;
    lblGafete: TLabel;
    LI_GAFETE: TDBEdit;
    gbOtros: TGroupBox;
    lblTexto1: TLabel;
    lblTexto3: TLabel;
    lblTexto2: TLabel;
    LI_TEXTO1: TDBEdit;
    LI_TEXTO2: TDBEdit;
    LI_TEXTO3: TDBEdit;
    gbObserva: TGroupBox;
    LI_OBSERVA: TDBMemo;
    gbCarro: TGroupBox;
    lblCajon: TLabel;
    LI_CAR_EST: TDBEdit;
    LI_CAR_DES: TDBEdit;
    lblDescrip: TLabel;
    LI_CAR_PLA: TDBEdit;
    lblPlacas: TLabel;
    lblCarro: TLabel;
    LI_CAR_TIP: TZetaDBKeyLookup;
    lblStatus: TLabel;
    LI_STATUS: TZetaDBTextBox;
    LI_ENT_FEC: TZetaDBTextBox;
    LI_ENT_HOR: TZetaDBTextBox;
    LI_ENT_CAS: TZetaDBTextBox;
    LI_ENT_VIG: TZetaDBTextBox;
    LI_SAL_VIG: TZetaDBTextBox;
    LI_SAL_FEC2: TZetaDBTextBox;
    LI_SAL_HOR2: TZetaDBTextBox;
    LI_SAL_CAS2: TZetaDBTextBox;
    LI_NOMBRE: TDBEdit;
    LI_EMPRESA: TDBEdit;
    LI_ANFITR: TDBEdit;
    lblFolio: TLabel;
    LI_FOLIO: TZetaDBTextBox;
    LI_DEVGAF: TDBCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnAnfitrionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnDeptoClick(Sender: TObject);
    procedure btnVisitantesClick(Sender: TObject);
    procedure btnEmpresaClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure LI_NOMBREChange(Sender: TObject);
    procedure LI_EMPRESAChange(Sender: TObject);
    procedure LI_ANFITRChange(Sender: TObject);
    procedure LI_DEPTOChange(Sender: TObject);
  private
    { Private declarations }
    procedure CambiaControles( const lHabilita: Boolean );
    procedure VerificaControles;
    procedure ChecaGlobales;
    procedure LimpiaCodigo(const sCampo: String);
  protected
     procedure Connect; override;
     procedure EscribirCambios; override;
     function PuedeAgregar(var sMensaje: String): Boolean; override;
     function PuedeBorrar(var sMensaje: String): Boolean; override;
     function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditLibros: TEditLibros;

implementation
uses DVisitantes, DSistema, DCliente, DGlobal, FBuscaVisitantes, ZetaCommonClasses,
     ZetaCommonLists, ZAccesosTress, FBuscaAnfitriones, ZetaCommonTools, ZGlobalTress;

{$R *.DFM}

{ TEditLibros }
procedure TEditLibros.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_REG_LIBRO;
     FirstControl :=  LI_NOMBRE;
     with dmVisitantes do
     begin
          LI_ASUNTO.LookupDataset := cdsTipoAsunto;
          LI_TIPO_ID.LookupDataset := cdsTipoID;
          LI_CAR_TIP.LookupDataset := cdsTipoCarro;
          LI_SAL_CAS.LookupDataset := cdsCaseta;
     end;
     HelpContext:= H_VISMGR_EDIT_LIBROS;
end;

procedure TEditLibros.FormShow(Sender: TObject);
begin
     inherited;
     VerificaControles;
     if( dmVisitantes.cdsLibros.FieldByName('LI_STATUS').AsInteger = Ord( eLibroStatus( lsDentro ) ) )then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_SAL_FEC').AsDateTime := Date;
               FieldByName('LI_SAL_HOR').AsString := HoraAsStr( Now ); 
          end;
     end;
     PageControl1.ActivePageIndex := 0;
     ChecaGlobales;

     {Datasource.AutoEdit := Datasource.AutoEdit AND dmVisitantes.PuedeModificarLibro;
     if NOT dmVisitantes.PuedeModificarLibro then
     begin
          if Datasource.Dataset.State in [dsEdit,dsInsert] then
             Datasource.Dataset.Cancel;

          DBNavigator.Enabled := FALSE;
          AgregarBtn.Enabled := FALSE;
          BorrarBtn.Enabled := FALSE;
          ModificarBtn.Enabled := FALSE;
          OK.Enabled := FALSE;
     end;}
end;

procedure TEditLibros.ChecaGlobales;
begin
     LI_ANFITR.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_ANFI );
     LI_NOMBRE.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_VISI );
     LI_EMPRESA.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_COMPANY );
     LI_DEPTO.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_DEPTO );
end;

procedure TEditLibros.VerificaControles;
begin
     CambiaControles( dmVisitantes.cdsLibros.FieldByName('LI_STATUS').AsInteger = Ord( eLibroStatus( lsDentro ) ) );
end;

procedure TEditLibros.Connect;
begin
     with dmvisitantes do
     begin
          cdsTipoID.Conectar;
          cdsTipoAsunto.Conectar;
          cdsTipoCarro.Conectar;
          cdsCaseta.Conectar;
          cdsEmpVisitante.Conectar;
          cdsDepto.Conectar;
          DataSource.DataSet:= cdsLibros;
     end;
end;

function TEditLibros.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Agregar Registros De Libro Desde VisitantesMgr';
end;

function TEditLibros.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Borrar Registros De Libro Desde VisitantesMgr';
end;

function TEditLibros.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := dmVisitantes.PuedeModificarLibro;
     if not Result then
        sMensaje := 'No Se Pueden Modificar Registros De Libro Desde Visitantes';

end;


procedure TEditLibros.CambiaControles( const lHabilita: Boolean );
begin
     LI_SAL_FEC.Visible := lHabilita;
     LI_SAL_HOR.Visible := lHabilita;
     LI_SAL_CAS.Visible := lHabilita;
     LI_SAL_FEC2.Visible := not lHabilita;
     LI_SAL_HOR2.Visible := not lHabilita;
     LI_SAL_CAS2.Visible := not lHabilita;
end;

procedure TEditLibros.btnAnfitrionClick(Sender: TObject);
var
   iNumero: Integer;
   sDescription: String;
begin
     inherited;
     if FBuscaAnfitriones.BuscaAnfitrionDialogo( iNumero, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_ANFITR').AsString := sDescription;
               FieldByName('AN_NUMERO').AsInteger := iNumero;
          end;
     end;
end;

procedure TEditLibros.btnDeptoClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if dmVisitantes.cdsDepto.Search( VACIO, sKey, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_DEPTO').AsString := sDescription;
               FieldByName('LI_CDEPTO').AsString := sKey;
          end;
     end;
end;

procedure TEditLibros.btnVisitantesClick(Sender: TObject);
var
   iNumero, iEmpresa, iAnfitrion: Integer;
   sDescription, sAsunto, sAnfitrion, sDepto: String;
begin
     inherited;
     if FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_NOMBRE').AsString := sDescription;
               FieldByName('VI_NUMERO').AsInteger := iNumero;
          end;
     end;
     with dmVisitantes do
     begin
          if( iNumero <> 0 )then
          begin
               cdsVisitante.Conectar;
               if cdsVisitante.Locate( 'VI_NUMERO', iNumero, [] ) then
               begin
                    iEmpresa := cdsVisitante.FieldByName('EV_NUMERO').AsInteger;
                    sAsunto := cdsVisitante.FieldByName('VI_ASUNTO').AsString;
                    iAnfitrion := cdsVisitante.FieldByName('AN_NUMERO').AsInteger;
                    if cdsEmpVisitante.Locate( 'EV_NUMERO', iEmpresa, [] ) then
                    begin
                         cdsLibros.FieldByName('EV_NUMERO').AsInteger := iEmpresa;
                         cdsLibros.FieldByName('LI_EMPRESA').AsString := cdsEmpVisitante.FieldByName('EV_NOMBRE').AsString;
                    end;
                    if cdsTipoAsunto.Locate( 'TB_CODIGO', sAsunto, [] ) then
                         cdsLibros.FieldByName('LI_ASUNTO').AsString := cdsTipoAsunto.FieldByName('TB_CODIGO').AsString;
                    cdsAnfitrion.Conectar;
                    if( cdsAnfitrion.Locate( 'AN_NUMERO', iAnfitrion, [] ) )then
                    begin
                         cdsLibros.FieldByName('AN_NUMERO').AsInteger := iAnfitrion;
                         sAnfitrion := Trim( cdsAnfitrion.FieldByName( 'AN_APE_PAT' ).AsString ) + ' ' +
                                         Trim( cdsAnfitrion.FieldByName( 'AN_APE_MAT' ).AsString ) + ', ' +
                                         Trim( cdsAnfitrion.FieldByName( 'AN_NOMBRES' ).AsString ) ;
                         cdsLibros.FieldByName('LI_ANFITR').AsString := sAnfitrion;
                         sDepto := cdsAnfitrion.FieldByName('AN_DEPTO').AsString;
                         if strLleno( sDepto )then
                         begin
                              if( cdsDepto.Locate( 'TB_CODIGO', sDepto, [] ) )then
                              begin
                                   cdsLibros.FieldByName('LI_CDEPTO').AsString := sDepto;
                                   cdsLibros.FieldByName('LI_DEPTO').AsString := cdsDepto.FieldByName('TB_ELEMENT').AsString;
                              end;
                         end;
                    end
                    else
                    begin
                         with cdsLibros do
                         begin
                              FieldByName('AN_NUMERO').AsInteger := 0;
                              FieldByName('LI_ANFITR').AsString := VACIO;
                              FieldByName('LI_CDEPTO').AsString := VACIO;
                              FieldByName('LI_DEPTO').AsString := VACIO;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TEditLibros.btnEmpresaClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if dmVisitantes.cdsEmpVisitante.Search( VACIO, sKey, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_EMPRESA').AsString := sDescription;
               FieldByName('EV_NUMERO').AsInteger := StrtoInt( sKey );
          end;
     end;
end;

procedure TEditLibros.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     VerificaControles;
end;

procedure TEditLibros.EscribirCambios;
var
   lCierraForma: Boolean;
   iFolio : Integer;
begin
     with dmVisitantes.cdsLibros do
     begin
          Edit;
          if StrLleno( FieldByName('LI_SAL_CAS').AsString )then
               FieldByName('LI_STATUS').AsInteger := Ord( lsFuera );
          FieldByName('LI_SAL_VIG').AsInteger := dmCliente.Usuario;
     end;
     lCierraForma := ( dmVisitantes.cdsLibros.State = dsInsert );
     inherited EscribirCambios;
     if lCierraForma then
         Close;

     with dmVisitantes.cdsLibros do
     begin
          DisableControls;
          try
             iFolio:= FieldByName('LI_FOLIO').AsInteger;
             Refrescar;
             Locate( 'LI_FOLIO', iFolio, [] );
          finally
                 EnableControls;
          end;
     end;         
end;

procedure TEditLibros.LimpiaCodigo(const sCampo: String );
begin
     with dmVisitantes.cdsLibros do
     begin
          Edit;
          if( sCampo = 'LI_CDEPTO' )then
              FieldByName( sCampo ).AsString := VACIO
          else
              FieldByName( sCampo ).AsInteger := 0;
     end;
end;

procedure TEditLibros.LI_NOMBREChange(Sender: TObject);
begin
     inherited;
     LimpiaCodigo( 'VI_NUMERO' );
end;

procedure TEditLibros.LI_EMPRESAChange(Sender: TObject);
begin
     inherited;
     LimpiaCodigo( 'EV_NUMERO' );
end;

procedure TEditLibros.LI_ANFITRChange(Sender: TObject);
begin
     inherited;
     LimpiaCodigo( 'AN_NUMERO' );
end;

procedure TEditLibros.LI_DEPTOChange(Sender: TObject);
begin
     inherited;
     LimpiaCodigo( 'LI_CDEPTO' );
end;


end.
