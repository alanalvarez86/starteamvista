inherited GlobalLibro: TGlobalLibro
  Left = 280
  Caption = 'Visitas'
  ClientHeight = 260
  ClientWidth = 310
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 224
    Width = 310
    inherited OK: TBitBtn
      Left = 142
    end
    inherited Cancelar: TBitBtn
      Left = 227
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 310
    Height = 224
    ActivePage = tsGenerales
    Align = alClient
    TabOrder = 1
    object tsGenerales: TTabSheet
      Caption = 'Generales'
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 302
        Height = 129
        Align = alTop
        Caption = ' Campos Requeridos '
        TabOrder = 0
        object cbVisitante: TCheckBox
          Left = 16
          Top = 24
          Width = 65
          Height = 17
          Caption = 'Visitante'
          TabOrder = 0
        end
        object cbAnfitrion: TCheckBox
          Left = 16
          Top = 48
          Width = 73
          Height = 17
          Caption = 'Visitando A'
          TabOrder = 1
        end
        object cbCompany: TCheckBox
          Left = 16
          Top = 72
          Width = 81
          Height = 17
          Caption = 'Compa��a'
          TabOrder = 2
        end
        object cbDepto: TCheckBox
          Left = 16
          Top = 96
          Width = 97
          Height = 17
          Caption = 'Departamento'
          TabOrder = 3
        end
        object cbAsunto: TCheckBox
          Left = 168
          Top = 24
          Width = 97
          Height = 17
          Caption = 'Tipo De Asunto'
          TabOrder = 4
        end
        object cbIdentifica: TCheckBox
          Left = 168
          Top = 48
          Width = 129
          Height = 17
          Caption = 'Tipo de Identificaci�n'
          TabOrder = 5
        end
        object cbGafete: TCheckBox
          Left = 168
          Top = 96
          Width = 97
          Height = 17
          Caption = 'Gafete'
          TabOrder = 7
        end
        object cbVehiculo: TCheckBox
          Left = 168
          Top = 72
          Width = 121
          Height = 17
          Caption = 'Tipo de Veh�culo'
          TabOrder = 6
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 129
        Width = 302
        Height = 67
        Align = alClient
        Caption = ' Permitir Captura Abierta De: '
        TabOrder = 1
        object cbPVisita: TCheckBox
          Left = 16
          Top = 16
          Width = 65
          Height = 17
          Caption = 'Visitante'
          TabOrder = 0
        end
        object cbPAnfi: TCheckBox
          Left = 16
          Top = 40
          Width = 73
          Height = 17
          Caption = 'Visitando A'
          TabOrder = 1
        end
        object cbPCompany: TCheckBox
          Left = 168
          Top = 16
          Width = 81
          Height = 17
          Caption = 'Compa��a'
          TabOrder = 2
        end
        object cbPDepto: TCheckBox
          Left = 168
          Top = 40
          Width = 97
          Height = 17
          Caption = 'Departamento'
          TabOrder = 3
        end
      end
    end
    object tsAlertas: TTabSheet
      Caption = 'Alertas'
      ImageIndex = 1
      object GroupBox3: TGroupBox
        Left = 11
        Top = 61
        Width = 280
        Height = 73
        TabOrder = 1
        object lbMinimo1: TLabel
          Left = 7
          Top = 22
          Width = 176
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tiempo M�ximo Autorizado por Visita:'
        end
        object lbMinimo2: TLabel
          Left = 233
          Top = 22
          Width = 40
          Height = 13
          Caption = 'Minutos '
        end
        object lbMostrarAlerta1: TLabel
          Left = 87
          Top = 46
          Width = 96
          Height = 13
          Alignment = taRightJustify
          Caption = 'Mostrar Alerta Cada:'
        end
        object lbMostrarAlerta2: TLabel
          Left = 233
          Top = 46
          Width = 40
          Height = 13
          Caption = 'Minutos '
        end
        object eTiempoMinimo: TZetaNumero
          Left = 186
          Top = 18
          Width = 41
          Height = 21
          Mascara = mnMinutos
          TabOrder = 0
          Text = '0'
        end
        object eIntervalo: TZetaNumero
          Left = 186
          Top = 42
          Width = 41
          Height = 21
          Mascara = mnMinutos
          TabOrder = 1
          Text = '0'
        end
      end
      object cbPrenderAlertas: TCheckBox
        Left = 24
        Top = 59
        Width = 110
        Height = 17
        Caption = 'Activar Mecanismo'
        TabOrder = 0
        OnClick = cbPrenderAlertasClick
      end
    end
  end
end
