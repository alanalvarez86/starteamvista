inherited dmSistema: TdmSistema
  OldCreateOrder = True
  inherited cdsEmpresas: TZetaLookupDataSet
    AlCrearCampos = cdsEmpresasAlCrearCampos
  end
  inherited cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
  inherited cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
  end
end
