unit ZAccesosTress;

interface

const

// El C�digo de grupo que no tiene restricciones
     D_GRUPO_SIN_RESTRICCION          = 1;
     K_IMAGENINDEX                      = 0;
{Constantes para Derechos}
const
{******** Formas **********}
   D_VISITANTES                       = 1;
     D_REG_LIBRO                      = 27;
     D_REG_CITAS                      = 28;
     D_REG_CORTE                      = 29;
   D_VIS_REGISTRO                     = 2;
   D_VIS_PROCESO                      = 3;
{******** Consultas **********}
   D_CONSULTAS                        = 4;
     D_CONS_REPORTES                  = 5;
     D_CONS_BITACORA                  = 6;
     D_CONS_SQL                       = 7;
     //D_REPORTES_CONFIDENCIALES        = 13;
{******** Tablas  **********}
   D_TABLAS                           = 8;
     D_CAT_GRALES_CONDICIONES         = 11;
     D_TAB_TIPO_ID                    = 12;
     D_TAB_TIPO_CARRO                 = 13;
     D_TAB_TIPO_ASUNTO                = 14;
     D_TAB_TIPO_VISITA                = 15;
     D_TAB_DEPTO                      = 16;
{******** Sistema **********}
   D_SISTEMA                          = 17;
   D_SIST_BASE_DATOS                  = 31;
   D_SIST_DATOS_USUARIOS              = 18;
   D_SIST_DATOS_EMPRESAS              = 19;
   D_SIST_DATOS_GRUPOS                = 20;
   D_SIST_DATOS_IMPRESORAS            = 21;
   D_CAT_CONFI_GLOBALES               = 9;
   D_CAT_CONFI_DICCIONARIO            = 10;
   D_SIST_ADMIN_CASETA                = 30;

{******** Cat�logos  **********}
   D_CATALOGOS                        = 22;
     D_CAT_ANFITRION                  = 23;
     D_CAT_VISITANTE                  = 24;
     D_CAT_CASETA                     = 25;
     D_CAT_COMPANYS                   = 26;

// Nota: Si agregas un derecho, aumenta K_MAX_DERECHOS
     K_MAX_DERECHOS                   = 31;

implementation

end.
