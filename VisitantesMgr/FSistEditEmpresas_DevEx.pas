unit FSistEditEmpresas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditEmpresas_DevEx, Db, StdCtrls, ZetaEdit, Mask, DBCtrls, ExtCtrls,
  Buttons, ZetaSmartLists, ZetaKeyLookup, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer,
  cxEdit, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxGroupBox, cxNavigator,
  cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TSistEditEmpresas_DevEx = class(TSistBaseEditEmpresas_DevEx)
    DB_CODIGO: TZetaDBKeyLookup_DevEx;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations } 
    procedure Connect;override;
  end;

var
  SistEditEmpresas_DevEx: TSistEditEmpresas_DevEx;

implementation
uses
    ZetaCommonClasses,
    DSistema;

{$R *.DFM}

procedure TSistEditEmpresas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_VISMGR_EDIT_EMPRESAS;
      
     with dmSistema do
     begin
          DB_CODIGO.LookupDataset := cdsSistBaseDatosLookUp;
     end;
end;

procedure TSistEditEmpresas_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsSistBaseDatosLookup.Conectar;
     end;
     inherited;
end;


end.
