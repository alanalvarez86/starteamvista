unit FGlobalServidorEmail;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     ZBaseGlobal,
     ZReportTools,
     ZetaNumero, ZetaKeyCombo;

type
  TGlobalServidorEmail = class(TBaseGlobal)
    UserId: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    UserEmail: TEdit;
    Label7: TLabel;
    DisplayName: TEdit;
    Host: TEdit;
    Label3: TLabel;
    Password: TEdit;
    Label4: TLabel;
    Puerto: TZetaNumero;
    Label5: TLabel;
    Buscar: TSpeedButton;
    Label8: TLabel;
    Plantilla: TEdit;
    ErrorEmail: TEdit;
    OpenDialog: TOpenDialog;
    AutenticacionLBL: TLabel;
    Autenticacion: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalServidorEmail: TGlobalServidorEmail;

implementation

uses DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalServidorEmail.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos     := D_CAT_CONFI_GLOBALES;
     Host.Tag          := K_GLOBAL_EMAIL_HOST;
     Puerto.Tag        := K_GLOBAL_EMAIL_PORT;
     UserId.Tag        := K_GLOBAL_EMAIL_USERID;
     Password.Tag      := K_GLOBAL_EMAIL_PSWD;
     UserEmail.Tag     := K_GLOBAL_EMAIL_FROMADRESS;
     DisplayName.Tag   := K_GLOBAL_EMAIL_FROMNAME;
     ErrorEMail.Tag  := K_GLOBAL_EMAIL_MSGERROR;
     Plantilla.Tag  := K_GLOBAL_EMAIL_PLANTILLA;
     Autenticacion.Tag :=  K_GLOBAL_EMAIL_AUTH;

     {$ifdef VISITANTES}     
     HelpContext := H_VISMGR_GE_CORREOS;
     {$else}
     HelpContext := H00084_Servidor_Correos;
     {$endif}

end;

procedure TGlobalServidorEmail.BuscarClick(Sender: TObject);
begin
  inherited;
   with OpenDialog do
     begin
          InitialDir := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
          FileName := StrDef(Plantilla.Text, K_TEMPLATE_HTM);
          if Execute then
          begin
               if UpperCase(ExtractFileDir(FileName)) = UpperCase(InitialDir) then
                  Plantilla.Text := ExtractFileName(FileName)
               else
                  Plantilla.Text := FileName;
          end;
     end;
end;

procedure TGlobalServidorEmail.FormShow(Sender: TObject);
begin
  inherited;
  with Autenticacion do
  begin
       if ItemIndex < 0 then
          ItemIndex := 0;
  end;
end;

end.
