inherited CatAnfitriones: TCatAnfitriones
  Left = 152
  Top = 132
  Caption = 'Anfitriones'
  ClientWidth = 557
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 557
    inherited ValorActivo2: TPanel
      Width = 298
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 557
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'AN_NUMERO'
        Title.Caption = 'N'#250'mero'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pretty_Anfi'
        Title.Caption = 'Nombre'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Departamento'
        Width = 178
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_STATUS'
        Title.Caption = 'Status'
        Width = 80
        Visible = True
      end>
  end
end
