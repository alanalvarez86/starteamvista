unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB, Controls,
     ZArbolTools, ZNavBarTools, ZetaCommonLists,cxTreeView;

function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     DGlobal,
     DSistema,
     DCliente;

const

     K_VISITANTES                       = 2000;
     K_CONSULTAS                        = 2001;
     K_REPORTES                         = 2002;
     K_BITACORA                         = 2003;
     K_TABLAS                           = 2004;
     K_SISTEMA                          = 2005;
     K_SIST_DATOS_USUARIOS              = 2006;
     K_SIST_DATOS_GRUPOS                = 2007;
     K_CONS_SQL                         = 2008;
     K_CAT_GRALES_CONDICIONES           = 2009;
     K_SIST_DATOS_EMPRESAS              = 2010;
     K_SIST_DATOS_IMPRESORAS            = 2011;
     K_CAT_CONFIGURACION                = 2012;  //DACP   PENDIENTE Verficar si otra app lo usa
     K_CAT_CONFI_GLOBALES               = 2013;
     K_CAT_CONFI_DICCIONARIO            = 2014;
     K_ARCH_PROCESO                     = 2015; // DACP   PENDIENTE Verficar si otra app lo usa
     K_TAB_TIPO_ID                      = 2016;
     K_TAB_TIPO_CARRO                   = 2017;
     K_TAB_TIPO_ASUNTO                  = 2018;
     K_TAB_VISITA                       = 2019;
     K_TAB_DEPTO                        = 2020;
     K_VIS_REGISTRO                     = 2021;
     K_VIS_PROCESO                      = 2022;
     K_CATALOGOS                        = 2023;
     K_CAT_ANFITRION                    = 2024;
     K_CAT_VISITANTE                    = 2025;
     K_CAT_CASETA                       = 2026;
     K_REG_LIBRO                        = 2027;
     K_REG_CITAS                        = 2028;
     K_REG_CORTE                        = 2029;
     K_CAT_COMPANYS                     = 2030;

function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     case iNodo of
	            // Registros
               K_VISITANTES              : Result := Folder( 'Registros', D_VISITANTES );
               K_REG_LIBRO               : Result := Forma( 'Visitas', efcRegLibro );
               K_REG_CITAS               : Result := Forma( 'Citas', efcRegCitas );
               K_REG_CORTE               : Result := Forma( 'Cortes', efcRegCorte );
               //K_VIS_REGISTRO            : Result := Forma( 'Registro', efcRegVisitas );
               //K_VIS_PROCESO             : Result := Forma( 'Procesos', efcProcVisitas );

			   //Consultas
               K_CONSULTAS               : Result := Folder( 'Consultas',D_CONSULTAS );//
               K_REPORTES                : Result := Forma( 'Reportes', efcReportes );
               K_BITACORA                : Result := Forma( 'Bit�cora', efcBitacora );
               K_CONS_SQL                : Result := Forma( 'SQL', efcQueryGral );
			   
			   //Cat�logos
               K_CATALOGOS               : Result := Folder( 'Cat�logos',D_CATALOGOS  );//
               K_CAT_ANFITRION           : Result := Forma('Anfitriones', efcAnfitrion );
               K_CAT_VISITANTE           : Result := Forma( 'Visitantes', efcVisitante );
               K_CAT_CASETA              : Result := Forma( 'Casetas', efcCaseta );
               K_CAT_COMPANYS            : Result := Forma( 'Compa��as', efcCatCompanys );

               //Tablas
               K_TABLAS                  : Result := Folder( 'Tablas', D_TABLAS );
               K_TAB_DEPTO               : Result := Forma('Departamento', efcDepto );
               K_TAB_TIPO_ID             : Result := Forma('Tipo de Identificaci�n', efcTipoID );
               K_TAB_TIPO_CARRO          : Result := Forma('Tipo de Veh�culo', efcTipoCarro );
               K_TAB_TIPO_ASUNTO         : Result := Forma('Tipo de Asunto', efcTipoAsunto );
               K_TAB_VISITA              : Result := Forma('Tipo de Visitante', efcTipoVisita );
               K_CAT_GRALES_CONDICIONES  : Result := Forma( 'Condiciones', efcCatCondiciones );
               
			   //Sistema
               K_SISTEMA                 : Result := Folder( 'Sistema', D_SISTEMA );//
               K_SIST_DATOS_EMPRESAS     : Result := Forma( 'Empresas', efcSistEmpresas );
               K_SIST_DATOS_USUARIOS     : Result := Forma( 'Usuarios', efcSistUsuarios );
               K_SIST_DATOS_GRUPOS       : Result := Forma( 'Grupos de Usuarios', efcSistGrupos );
               K_SIST_DATOS_IMPRESORAS   : Result := Forma( 'Impresoras', efcImpresoras );
			   K_CAT_CONFI_GLOBALES      : Result := Forma( 'Globales de Empresa', efcSistGlobales );
               K_CAT_CONFI_DICCIONARIO   : Result := Forma( 'Diccionario de Datos', efcDiccion );
            else
                Result := Folder( '', 0 );
     end;
end;

function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
         // Registros
          D_VISITANTES          : Result := Grupo( 'Registros', iNodo );
         // Consultas
          D_CONSULTAS           : Result := Grupo( 'Consultas', iNodo );
          // Cat�logos
          D_CATALOGOS           : Result := Grupo( 'Cat�logos', iNodo );
          //Tabalas
          D_TABLAS              : Result := Grupo( 'Tablas', iNodo );
		      //Sistema
		      D_SISTEMA		        : Result := Grupo( 'Sistema', iNodo );
     else
          Result := Grupo( '', 0 );
     end;
end;

procedure CreaArbolDefault( oArbolMgr : TArbolMgr );

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;
begin
      //Registros
      NodoNivel( 0, K_VISITANTES );
       NodoNivel( 1, K_REG_LIBRO );
       NodoNivel( 1, K_REG_CITAS );
       NodoNivel( 1, K_REG_CORTE );

	  //Consultas
      NodoNivel( 0, K_CONSULTAS );
       NodoNivel( 1, K_REPORTES );
       NodoNivel( 1, K_BITACORA );
       NodoNivel( 1, K_CONS_SQL );

      //Catalogos
      NodoNivel( 0, K_CATALOGOS );
       NodoNivel( 1, K_CAT_ANFITRION );
	   NodoNivel( 1, K_CAT_VISITANTE );
	   NodoNivel( 1, K_CAT_CASETA );
	   NodoNivel( 1, K_CAT_COMPANYS );

      //Tablas
      NodoNivel( 0,  K_TABLAS );
	   NodoNivel( 1, K_TAB_DEPTO );
	   NodoNivel( 1, K_TAB_TIPO_ID );
	   NodoNivel( 1, K_TAB_TIPO_CARRO );
	   NodoNivel( 1, K_TAB_TIPO_ASUNTO );
	   NodoNivel( 1, K_TAB_VISITA );
	   NodoNivel( 1, K_CAT_GRALES_CONDICIONES );
      
      //Sistema
	  NodoNivel( 0, K_SISTEMA );
	   NodoNivel( 1, K_SIST_DATOS_EMPRESAS );
	   NodoNivel( 1, K_SIST_DATOS_GRUPOS );
	   NodoNivel( 1, K_SIST_DATOS_USUARIOS );
	   NodoNivel( 1, K_SIST_DATOS_IMPRESORAS );
	   NodoNivel( 1, K_CAT_CONFI_GLOBALES );
	   NodoNivel( 1, K_CAT_CONFI_DICCIONARIO );
end;

procedure CreaArbolitoDefault( oArbolMgr : TArbolMgr; iAccesoGlobal: Integer );
function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo); //Siempre mandar 0 en iNodo
end;
begin
      case iAccesoGlobal of
            //Registros
            K_VISITANTES		:
            begin
                 NodoNivel( 0, K_REG_LIBRO );
                 NodoNivel( 0, K_REG_CITAS );
                 NodoNivel( 0, K_REG_CORTE );
            end;
            //Consultas
           K_CONSULTAS      :
           begin
                 NodoNivel( 0, K_REPORTES );
                 NodoNivel( 0, K_BITACORA );
                 NodoNivel( 0, K_CONS_SQL );
            end;
            //Cat�logos
            K_CATALOGOS               :
            begin
                 NodoNivel( 0, K_CAT_ANFITRION );
                 NodoNivel( 0, K_CAT_VISITANTE );
                 NodoNivel( 0, K_CAT_CASETA );
                 NodoNivel( 0, K_CAT_COMPANYS );
            end;
            //Tablas
            K_TABLAS               :
            begin
                 NodoNivel( 0, K_TAB_DEPTO );
                 NodoNivel( 0, K_TAB_TIPO_ID );
                 NodoNivel( 0, K_TAB_TIPO_CARRO );
                 NodoNivel( 0, K_TAB_TIPO_ASUNTO );
                 NodoNivel( 0, K_TAB_VISITA );
                 NodoNivel( 0, K_CAT_GRALES_CONDICIONES );
            end;
            //Sistema
            K_SISTEMA               :
            begin
                 NodoNivel( 0, K_SIST_DATOS_EMPRESAS );
                 NodoNivel( 0, K_SIST_DATOS_GRUPOS );
                 NodoNivel( 0, K_SIST_DATOS_USUARIOS );
                 NodoNivel( 0, K_SIST_DATOS_IMPRESORAS );
                 NodoNivel( 0, K_CAT_CONFI_GLOBALES );
                 NodoNivel( 0, K_CAT_CONFI_DICCIONARIO );
            end;
      end;
end;

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
var NodoRaiz: TGrupoInfo;
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     //CreaArbolitoUsuario( oArbolMgr, '',K_GLOBAL );
                     //Antes metodo: CreaArbolUsuario
                     with oArbolMgr do
                     begin
                           //Configuracion := TRUE;
                           //oArbolMgr.NumDefault := K_GLOBAL ;
                           CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                           Arbol_DevEx.FullCollapse;//edit by mp: el arbol dentro del navbar es el arbol_DevEx
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario. En VisitantesMGR inica desde
      cero porque no hay Arbol de Usuario.}

           if( NodoNivel( D_VISITANTES, 0 )) then
                      CreaArbolito ( K_VISITANTES, Arbolitos[0]);
           // ******** Consultas ***********
           if( NodoNivel( D_CONSULTAS, 1 )) then
                      CreaArbolito ( K_CONSULTAS, Arbolitos[1]);
           // ******* Cat�logos *************
           if( NodoNivel( D_CATALOGOS, 2 ) ) then
                      CreaArbolito ( K_CATALOGOS, Arbolitos[2]);
           // ******* Cat�logos *************
           if( NodoNivel( D_TABLAS, 3 ) ) then
                      CreaArbolito ( K_TABLAS, Arbolitos[3]);
           // ******* Cat�logos *************
           if( NodoNivel( D_SISTEMA, 4 ) ) then
                      CreaArbolito ( K_SISTEMA, Arbolitos[4]);
end;

end.
