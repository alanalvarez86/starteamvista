unit FEditCatVisitantes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaDBTextBox,
  ZetaKeyCombo, ZetaFecha, ZetaKeyLookup, Mask, ComCtrls, {TDMULTIP, MMOpen,}
  ZetaNumero, {Videocap,} ZetaSmartLists;

type
  TEditCatVisitantes = class(TBaseEdicion)
    PageControl1: TPageControl;
    tsGenerales: TTabSheet;
    lblApePat: TLabel;
    VI_APE_PAT: TDBEdit;
    VI_APE_MAT: TDBEdit;
    lblApeMat: TLabel;
    lblNombre: TLabel;
    VI_NOMBRES: TDBEdit;
    lblNoVisita: TLabel;
    VI_TIPO: TZetaDBKeyLookup;
    lblTipo: TLabel;
    EV_NUMERO: TZetaDBKeyLookup;
    lblNacion: TLabel;
    VI_NACION: TDBEdit;
    lblFecNac: TLabel;
    VI_FEC_NAC: TZetaDBFecha;
    lblStatus: TLabel;
    lblSexo: TLabel;
    lblVisitante: TLabel;
    tsFoto: TTabSheet;
    btnAgregaFoto: TBitBtn;
   // MMOpenDialog1: TMMOpenDialog;
    lblMigra: TLabel;
    VI_MIGRA: TDBEdit;
    VI_NUMERO: TZetaDBNumero;
    VI_STATUS: TZetaDBKeyCombo;
    lblAnfitrion: TLabel;
    AN_NUMERO: TZetaDBKeyLookup;
    lblAsunto: TLabel;
    VI_ASUNTO: TZetaDBKeyLookup;
    VI_SEXO: TZetaKeyCombo;
    btnCapturar: TBitBtn;
    btnVideo: TBitBtn;
    lblDriver: TLabel;
    cbDrivers: TComboBox;
    btnBorrar: TBitBtn;
    Panel1: TPanel;
    //Documento: TPDBMultiImage;
    //VideoCap1: TVideoCap;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAgregaFotoClick(Sender: TObject);
    procedure VI_SEXOChange(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure tsFotoShow(Sender: TObject);
    procedure btnCapturarClick(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
   // lBorrado: Boolean;{OP: acl}     
    procedure IniciaGenero;
    procedure SetControls(lHabilita: Boolean);
    procedure SetDriver;
    procedure TomaFotografia;
  protected
    procedure Connect; override;
    procedure EscribirCambios; override;{OP: 05/06/08}
  public
    { Public declarations }
  end;

var
  EditCatVisitantes: TEditCatVisitantes;

implementation

uses dVisitantes,
     ZetaCommonLists,
     ZetaDialogo,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaCommonClasses;
    // FToolsFoto,
    // FToolsVideo;{OP: 05/06/08}

{$R *.DFM}

procedure TEditCatVisitantes.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_VISITANTE;
     FirstControl := VI_NUMERO;
     with dmVisitantes do
     begin
          VI_TIPO.LookupDataset := cdsTipoVisita;
          EV_NUMERO.LookupDataset := cdsEmpVisitante;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          VI_ASUNTO.LookupDataset := cdsTipoAsunto;
     end;
     HelpContext:= H_VISMGR_EDIT_VISITANTES;
    // FToolsVideo.SetDriver := SetDriver; //acl
    // FToolsVideo.SetControls := SetControls; //acl
      //VI_SEXO.ItemIndex := 0;
     //TipoValorActivo1 := stExpediente;
end;

procedure TEditCatVisitantes.FormShow(Sender: TObject);
begin
     inherited;
     with PageControl1 do
          ActivePage := tsGenerales;
  //   VI_SEXO.ItemIndex := 0;
end;

procedure TEditCatVisitantes.Connect;
begin
     with dmVisitantes do
     begin
          cdsTipoVisita.Conectar;
          cdsEmpVisitante.Conectar;
          cdsVisitaCitas.Conectar;
          cdsAnfitrion.Conectar;
          cdsTipoAsunto.Conectar;
          DataSource.DataSet:= cdsVisitante;
     end;
     IniciaGenero;
end;

procedure TEditCatVisitantes.IniciaGenero;
begin
     with dmVisitantes.cdsVisitante, VI_SEXO do
     begin
          if ( FieldByName( 'VI_SEXO' ).AsString = ObtieneElemento( lfSexo, Ord(esFemenino) ) ) then
             ItemIndex:= Ord( esFemenino )
          else
             ItemIndex:= Ord( esMasculino );
     end;
end;

procedure TEditCatVisitantes.btnAgregaFotoClick(Sender: TObject);
begin
     inherited;
  {   if MMOpenDialog1.Execute then
     begin
          SetControls( True );
          with dmVisitantes.cdsVisitante do
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
          Documento.LoadFromFile( MMOpenDialog1.FileName );
     end; }
end;

procedure TEditCatVisitantes.VI_SEXOChange(Sender: TObject);
var
   sSexo: String;
begin
     inherited;
     sSexo:= ObtieneElemento( lfSexo, VI_SEXO.ItemIndex );
     with dmVisitantes.cdsVisitante do
     begin
          if ( sSexo <> FieldByName( 'VI_SEXO' ).AsString ) then
          begin
               if not Editing then
                  Edit;
               FieldByName( 'VI_SEXO' ).AsString:= sSexo;
          end;
     end;
end;

procedure TEditCatVisitantes.btnVideoClick(Sender: TObject);
begin
     inherited;
     SetControls( True );
   //  FToolsVideo.InicializaVideo( VideoCap1, cbDrivers, lblDriver );
end;

procedure TEditCatVisitantes.SetControls( lHabilita: Boolean );
begin
   {  if ( FToolsVideo.DriverManager( cbDrivers, lblDriver ) = true ) then
     begin
        //  Documento.Visible:= lHabilita;
          btnVideo.Enabled := lHabilita;
          btnBorrar.Enabled := lHabilita;
          //VideoCap1.Visible:= not lHabilita;
          btnCapturar.Enabled:= not lHabilita;
     end
     else
     begin
          lHabilita:=false;
        //  Documento.Visible:= not lHabilita;
          btnVideo.Enabled := lHabilita;
          btnBorrar.Enabled := lHabilita;
         // VideoCap1.Visible:= lHabilita;
          btnCapturar.Enabled:= lHabilita;
     end;    }
end;

procedure TEditCatVisitantes.tsFotoShow(Sender: TObject);
begin
     inherited;
     //SetControls( FToolsVideo.DriverManager( cbDrivers, lblDriver ) );
     btnBorrar.Enabled := strLleno( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString );
end;

procedure TEditCatVisitantes.SetDriver;
begin
  //  VideoCap1.DriverIndex := cbDrivers.ItemIndex;
end;

procedure TEditCatVisitantes.btnCapturarClick(Sender: TObject);
begin
     inherited;
     TomaFotografia;
end;

procedure TEditCatVisitantes.TomaFotografia;
begin
     SetControls( True );
  //   VideoCap1.SaveToClipboard;
   //  documento.PasteFromClipboard;
end;

procedure TEditCatVisitantes.btnBorrarClick(Sender: TObject);
begin
     inherited;
     with dmVisitantes.cdsVisitante do
     begin
          if ( not ( State in [ dsEdit, dsInsert ] ) ) then
             Edit;
          TBlobField(FieldByName('VI_FOTO')).Clear;
          TBlobField(FieldByName('VI_FOTO')).AsString := '';
     end;
end;

procedure TEditCatVisitantes.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     btnBorrar.Enabled := strLleno( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString );
end;

{OP: 05/06/08}     //acl
procedure TEditCatVisitantes.EscribirCambios;
begin
     {OP: 25/06/08}
    { if ( strLleno( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString ) ) then
     begin
             with Documento do
             begin
                  CutToClipboard;
                  UpdateAsJPG:=True;
                  PastefromClipboard;
             end;
             try
             if( Documento.Picture.Bitmap.Width > 300 ) then
             begin
                 FToolsFoto.ResizeImageBestFit( Documento, 300, 300 );
             end
          except
                on Error: Exception do
                begin
                        ZError( self.Caption,'No se pudo ajustar el tama�o de la fotograf�a. Se guardar� con su tama�o original'+CR_LF+Error.Message, 0);
                end;
          end;
     end; }
     inherited EscribirCambios;
end;

procedure TEditCatVisitantes.FormDestroy(Sender: TObject);
begin
     inherited;
     {FToolsVideo.SetDriver := nil;
     FToolsVideo.SetControls := nil; }
end;

end.

