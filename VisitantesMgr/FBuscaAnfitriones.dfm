inherited BuscaAnfitriones: TBuscaAnfitriones
  Left = 238
  Top = 201
  Caption = 'B�squeda de Vistando A'
  ClientHeight = 252
  ClientWidth = 416
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 416
    inherited btnAvz: TSpeedButton
      Left = 383
      Top = 7
      OnClick = btnAvzClick
    end
    inherited edNombre: TEdit
      Width = 247
      MaxLength = 90
      OnChange = edNombreChange
    end
    inherited btnBuscar: TBitBtn
      Left = 304
      Top = 7
      Width = 76
      OnClick = btnBuscarClick
    end
  end
  inherited pnlAvz: TPanel
    Width = 416
    Height = 73
    object GroupBox1: TGroupBox
      Left = 7
      Top = 3
      Width = 401
      Height = 65
      Caption = ' Opciones Avanzadas '
      TabOrder = 0
      object lblDepto: TLabel
        Left = 5
        Top = 17
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Departamento:'
      end
      object lblStatus: TLabel
        Left = 42
        Top = 40
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object AN_DEPTO: TZetaKeyLookup
        Left = 78
        Top = 13
        Width = 318
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 65
      end
      object AN_STATUS: TZetaKeyCombo
        Left = 78
        Top = 36
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfAnfitrionStatus
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
      end
    end
  end
  inherited zGridBusca: TZetaDBGrid
    Top = 114
    Width = 416
    Height = 99
    DataSource = DataSource
    OnDblClick = zGridBuscaDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'AN_NUMERO'
        PickList.Strings = ()
        Title.Caption = 'N�mero'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Pretty_Anfi'
        PickList.Strings = ()
        Title.Caption = 'Nombre'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        PickList.Strings = ()
        Title.Caption = 'Departamento'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_STATUS'
        PickList.Strings = ()
        Title.Caption = 'Status'
        Width = 80
        Visible = True
      end>
  end
  inherited Panel2: TPanel
    Top = 213
    Width = 416
    inherited btnAceptar: TBitBtn
      Left = 253
      Anchors = [akTop, akRight, akBottom]
      Enabled = False
      OnClick = zGridBuscaDblClick
    end
    inherited btnCancelar: TBitBtn
      Left = 333
      Anchors = [akTop, akRight, akBottom]
    end
  end
end
