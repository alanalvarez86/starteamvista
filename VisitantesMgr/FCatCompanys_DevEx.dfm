inherited CatCompanys_DevEx: TCatCompanys_DevEx
  Left = 255
  Top = 229
  Caption = 'Compa'#241#237'as'
  ClientHeight = 251
  ClientWidth = 1172
  ExplicitWidth = 1172
  ExplicitHeight = 251
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1172
    ExplicitWidth = 1172
    inherited ValorActivo2: TPanel
      Width = 913
      ExplicitWidth = 913
      inherited textoValorActivo2: TLabel
        Width = 907
        ExplicitLeft = 827
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1172
    Height = 232
    ExplicitWidth = 1172
    ExplicitHeight = 232
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object EV_NUMERO: TcxGridDBColumn
        Caption = 'Compa'#241#237'a'
        DataBinding.FieldName = 'EV_NUMERO'
        MinWidth = 70
        Styles.Content = cxStyle3
        Width = 70
      end
      object EV_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'EV_NOMBRE'
        MinWidth = 150
        Styles.Content = cxStyle4
        Width = 200
      end
      object EV_RAZ_SOC: TcxGridDBColumn
        Caption = 'Raz'#243'n social'
        DataBinding.FieldName = 'EV_RAZ_SOC'
        MinWidth = 100
        Styles.Content = cxStyle5
        Width = 350
      end
      object EV_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'EV_STATUS'
        MinWidth = 70
        Styles.Content = cxStyle6
        Width = 80
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 352
    Top = 0
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 32
    Top = 104
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
