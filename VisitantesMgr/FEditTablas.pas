unit FEditTablas;

interface

uses ZBaseTablas;

type
  TEditTipoID = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditTipoCarro = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditTipoAsunto = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditTipoVisita = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditDepto = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

var
  EditTipoID: TEditTipoID;
  EditTipoCarro: TEditTipoCarro;
  EditTipoAsunto: TEditTipoAsunto;
  EditTipoVisita: TEditTipoVisita;
  EditDepto: TEditDepto;

implementation

uses DVisitantes,
     ZAccesosTress,
     ZetaCommonClasses;

{ ****** TEditTipoID ******* }

procedure TEditTipoID.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoID;
     HelpContext := H_VISMGR_EDIT_TIDENTIF;
     IndexDerechos := D_TAB_TIPO_ID;
end;

{ ****** TEditTipoCarro ******* }

procedure TEditTipoCarro.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoCarro;
     HelpContext := H_VISMGR_EDIT_TVEHICULO;
     IndexDerechos := D_TAB_TIPO_CARRO;
end;

{ ****** TEditTipoAsunto ******* }

procedure TEditTipoAsunto.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoAsunto;
     HelpContext := H_VISMGR_EDIT_TASUNTO;
     IndexDerechos := D_TAB_TIPO_ASUNTO;
end;

{ ****** TEditTipoVisita ******* }

procedure TEditTipoVisita.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoVisita;
     HelpContext := H_VISMGR_EDIT_TVISITANTE;
     IndexDerechos := D_TAB_TIPO_VISITA;
end;

{ ****** TEditDepto ******* }

procedure TEditDepto.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsDepto;
     HelpContext := H_VISMGR_EDIT_DEPARTAMENTO;
     IndexDerechos := D_TAB_DEPTO;
end;

end.
