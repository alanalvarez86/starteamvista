unit FCatAnfitrion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TCatAnfitriones = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  CatAnfitriones: TCatAnfitriones;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatCondiciones }
procedure TCatAnfitriones.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     HelpContext := H_VISMGR_CONS_ANFITRIONES;
end;

procedure TCatAnfitriones.Connect;
begin
     with dmVisitantes do
     begin
          cdsAnfitrion.Conectar;
          DataSource.DataSet := cdsAnfitrion;
     end;
end;

procedure TCatAnfitriones.Refresh;
begin
     dmVisitantes.cdsAnfitrion.Refrescar;
end;

procedure TCatAnfitriones.Agregar;
begin
     dmVisitantes.cdsAnfitrion.Agregar;
end;

procedure TCatAnfitriones.Borrar;
begin
     dmVisitantes.cdsAnfitrion.Borrar;
end;

procedure TCatAnfitriones.Modificar;
begin
     dmVisitantes.cdsAnfitrion.Modificar;
end;

end.
