unit FBuscaAnfitriones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  Db, ZetaKeyCombo, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridLevel, cxGridCustomTableView, cxGridCardView,
  cxClasses, cxGridCustomView, cxGridCustomLayoutView, cxGrid, Vcl.ImgList,
  ZBaseEdicion_DevEx, Vcl.Menus, cxButtons, cxContainer, cxCheckBox,StrUtils,
  dxBarExtItems, dxBar, cxDBNavigator, ZetaCXGrid, cxGridDBTableView;

type
  TBuscaAnfitrion_DevEx = class(TZetaDlgModal_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    eBusca: TEdit;
    btnBuscar: TcxButton;
    SmallImageList: TcxImageList;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle2: TcxStyle;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    Pretty_Visit: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    AN_STATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure eBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

    procedure ZetaDBGridDBTableViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure OK_DevExClick(Sender: TObject);
  private
    FNombre: String;
    FAnfitrion: Integer;
    procedure PreparaFiltros;
    procedure ConfigAgrupamiento;
    { Private declarations }

  published
    { Published declarations }

  public
    { Public declarations }
  end;

var
  BuscaAnfitrion_DevEx: TBuscaAnfitrion_DevEx;

function BuscaAnfitrionDialogo( var iAnfitrion:Integer; var sNombre: String ): Boolean;

implementation
uses
    DVisitantes,
    ZetaDialogo,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaCommonLists,
    ZGridModeTools,
    cxTextEdit;

{$R *.DFM}

type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);


function BuscaAnfitrionDialogo( var iAnfitrion:Integer; var sNombre: String ): Boolean;
begin
     Result := False;
     if ( BuscaAnfitrion_DevEx = nil ) then
        BuscaAnfitrion_DevEx := TBuscaAnfitrion_DevEx.Create( Application );
     try
        if ( BuscaAnfitrion_DevEx <> nil ) then
        begin
             with BuscaAnfitrion_DevEx do
             begin
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       sNombre := FNombre;
                       iAnfitrion := FAnfitrion;
                       Result := True;
                  end;
             end;
        end;
     finally
            FreeAndNil( BuscaAnfitrion_DevEx );
     end;
end;


procedure TBuscaAnfitrion_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
end;

procedure TBuscaAnfitrion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //   HelpContext := H_VIS_BUS_VISITANTE;
end;

procedure TBuscaAnfitrion_DevEx.btnBuscarClick(Sender: TObject);
begin
     inherited;
     if (eBusca.Text <> VACIO) or (sender = btnBuscar)then
        PreparaFiltros;
          //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TBuscaAnfitrion_DevEx.PreparaFiltros;

var
   FParamList: TZetaParams;
begin
     FParamList := TZetaParams.Create;
     try
        FParamList.AddString( 'NOMBRE', UPPERCASE( eBusca.Text ) );
        FParamList.AddString( 'DEPTO', UPPERCASE( '' ) );
        FParamList.AddInteger('STATUS', 1 );   //DACP se pone por default 1 ya que el Query anterior utilizaba un filtro por status y 1 es igual a buscar en todos los status
        with dmVisitantes do
        begin
             HacerBusquedaAnfitrion( FParamList );
             DataSource.DataSet := cdsAnfitrionLookup;
             if ( cdsAnfitrionLookup.IsEmpty ) then
             begin
                  ZetaDialogo.ZInformation('Información','No existe información con los datos requeridos', 0);
             end
        end;
      finally
            FreeAndNil( FParamList );
      end;
end;


procedure TBuscaAnfitrion_DevEx.ZetaDBGridDBTableViewCellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
     inherited;
     with dmVisitantes.cdsAnfitrionLookup do
     begin
          if Active then
          begin
              FNombre := FieldByName('Pretty_Anfi').AsString;
              FAnfitrion := FieldByName('AN_NUMERO').AsInteger;
              ModalResult := mrOk;
          end
          else
              ModalResult := mrOk;
     end;
end;

procedure TBuscaAnfitrion_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );

end;


procedure TBuscaAnfitrion_DevEx.eBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if eBusca.Text <> VACIO then
  if key = 13 then
    PreparaFiltros;
end;


procedure TBuscaAnfitrion_DevEx.FormShow(Sender: TObject);
begin
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ConfigAgrupamiento;
     with dmVisitantes do
     begin
          cdsDepto.Conectar;
          cdsAnfitrion.Conectar;
          cdsEmpVisitante.Conectar;
          DataSource.Dataset := cdsAnfitrionLookup;
     end;
     eBusca.SetFocus;
     eBusca.Text := VACIO;
end;



procedure TBuscaAnfitrion_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     with dmVisitantes.cdsAnfitrionLookup do
     begin
          if Active then
          begin
              FNombre := FieldByName('Pretty_Anfi').AsString;
              FAnfitrion := FieldByName('AN_NUMERO').AsInteger;
              ModalResult := mrOk;
          end
          else
          ModalResult := mrOk;
     end;
end;

procedure TBuscaAnfitrion_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE; //Muestra la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     Pretty_Visit.Options.Grouping:= FALSE;
     Pretty_Visit.Options.Filtering:= FALSE;
end;


end.
