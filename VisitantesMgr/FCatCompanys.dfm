inherited CatCompanys: TCatCompanys
  Left = 255
  Top = 229
  Caption = 'Compa'#241#237'as'
  ClientWidth = 534
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 534
    inherited ValorActivo2: TPanel
      Width = 275
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 534
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'EV_NUMERO'
        Title.Caption = 'Compa'#241#237'a'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_RAZ_SOC'
        Title.Caption = 'Raz'#243'n Social'
        Width = 350
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EV_STATUS'
        Title.Caption = 'Status'
        Width = 80
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 352
    Top = 0
  end
end
