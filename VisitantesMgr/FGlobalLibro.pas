unit FGlobalLibro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGlobal, StdCtrls, Buttons, ExtCtrls, Mask, ZetaNumero, ComCtrls;

type
  TGlobalLibro = class(TBaseGlobal)
    PageControl: TPageControl;
    tsGenerales: TTabSheet;
    tsAlertas: TTabSheet;
    GroupBox1: TGroupBox;
    cbVisitante: TCheckBox;
    cbAnfitrion: TCheckBox;
    cbCompany: TCheckBox;
    cbDepto: TCheckBox;
    cbAsunto: TCheckBox;
    cbIdentifica: TCheckBox;
    cbGafete: TCheckBox;
    cbVehiculo: TCheckBox;
    GroupBox2: TGroupBox;
    cbPVisita: TCheckBox;
    cbPAnfi: TCheckBox;
    cbPCompany: TCheckBox;
    cbPDepto: TCheckBox;
    cbPrenderAlertas: TCheckBox;
    GroupBox3: TGroupBox;
    lbMinimo1: TLabel;
    eTiempoMinimo: TZetaNumero;
    lbMinimo2: TLabel;
    lbMostrarAlerta1: TLabel;
    eIntervalo: TZetaNumero;
    lbMostrarAlerta2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbPrenderAlertasClick(Sender: TObject);
  private
    procedure EnabledAlertas;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalLibro: TGlobalLibro;

implementation
uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;
{$R *.DFM}

procedure TGlobalLibro.FormCreate(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := tsGenerales;
     IndexDerechos := D_CAT_CONFI_GLOBALES;
     cbVisitante.Tag := K_GLOBAL_REQ_VISITANTE;
     cbAnfitrion.Tag := K_GLOBAL_REQ_ANFITRION;
     cbCompany.Tag :=  K_GLOBAL_REQ_COMPANYS;
     cbDepto.Tag := K_GLOBAL_REQ_DEPARTAMENTO;
     cbAsunto.Tag := K_GLOBAL_REQ_ASUNTO;
     cbVehiculo.Tag := K_GLOBAL_REQ_VEHICULO;
     cbIdentifica.Tag := K_GLOBAL_REQ_IDENTIFICA;
     cbGafete.Tag := K_GLOBAL_REQ_GAFETE;
     cbPVisita.Tag := K_GLOBAL_ABIERTA_VISI;
     cbPAnfi.Tag := K_GLOBAL_ABIERTA_ANFI;
     cbPCompany.Tag := K_GLOBAL_ABIERTA_COMPANY;
     cbPDepto.Tag := K_GLOBAL_ABIERTA_DEPTO;
     cbPrenderAlertas.Tag := K_GLOBAL_ALERTAS_ACTIVADA;
     eTiempoMinimo.Tag := K_GLOBAL_ALERTAS_TIEMPO_MINIMO;
     eIntervalo.Tag := K_GLOBAL_ALERTAS_INTERVALO;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_GE_LIBRO;
     {$else}
     HelpContext := H00050_Globales_de_empresa;
     {$endif}     
end;

procedure TGlobalLibro.FormShow(Sender: TObject);
begin
     inherited;
     EnabledAlertas;
end;

procedure TGlobalLibro.cbPrenderAlertasClick(Sender: TObject);
begin
     inherited;
     EnabledAlertas;
end;

procedure TGlobalLibro.EnabledAlertas;
 var
    lEnabled : Boolean;
begin
     lEnabled := cbPrenderAlertas.Checked;

     lbMinimo1.Enabled := lEnabled;
     lbMinimo2.Enabled := lEnabled;
     eTiempoMinimo.Enabled := lEnabled;

     lbMostrarAlerta1.Enabled := lEnabled;
     lbMostrarAlerta2.Enabled := lEnabled;
     eIntervalo.Enabled := lEnabled;
end;

end.
