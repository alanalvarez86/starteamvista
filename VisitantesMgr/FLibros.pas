unit FLibros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Mask, ZetaFecha, ZetaKeyLookup,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, Buttons, ZetaNumero, ComCtrls,
  ZetaKeyCombo, ZetaCommonLists;

type
  TLibros = class(TBaseConsulta)
    Panel1: TPanel;
    lblFechaIni: TLabel;
    zfFecIni: TZetaFecha;
    zfFecFin: TZetaFecha;
    lblFechaFin: TLabel;
    lblRegistros: TLabel;
    ztbRegistros: TZetaTextBox;
    ZetaDBGrid1: TZetaDBGrid;
    AN_NUMERO: TEdit;
    btnAnfitrion: TSpeedButton;
    btnVisitantes: TSpeedButton;
    VI_NUMERO: TEdit;
    lblVisitante: TLabel;
    lblEmpresa: TLabel;
    lblAnfitrion: TLabel;
    EV_NUMERO: TEdit;
    btnEmpresa: TSpeedButton;
    lblCierraCorte: TLabel;
    LI_ENT_CAS: TZetaKeyLookup;
    LI_ENT_VIG: TZetaKeyLookup;
    lblRegEntrada: TLabel;
    btnBuscar: TBitBtn;
    Label1: TLabel;
    LI_STATUS: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure btnAnfitrionClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zfFecIniValidDate(Sender: TObject);
    procedure btnVisitantesClick(Sender: TObject);
    procedure btnEmpresaClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    procedure PreparaFiltro;
    procedure CuentaRegistros;
    procedure LimpiaFiltros;
    procedure LlenaStatusLibro( oLista: TStrings; oLFijas: ListasFijas );
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  Libros: TLibros;

implementation
uses DVisitantes, DSistema, ZetaCommonClasses, ZetaCommonTools, FBuscaAnfitriones,
     FBuscaVisitantes;
{$R *.DFM}

{ TLibros }
procedure TLibros.FormCreate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          SetFechasInicio;
          zfFecIni.Valor := LibroFechaIni;
          zfFecFin.Valor := LibroFechaFin;
          LI_ENT_CAS.LookupDataset := cdsCaseta;
     end;
     with LI_STATUS do
     begin
          LlenaStatusLibro( Lista, lfLibroStatus );
          ItemIndex := 0;
     end;
     LI_ENT_VIG.LookupDataset := dmSistema.cdsUsuariosLookup;
     HelpContext:= H_VISMGR_CONS_LIBROS;
     //HelpContext := H60634_Condiciones;
end;

procedure TLibros.FormShow(Sender: TObject);
begin
     inherited;
     LimpiaFiltros;
     CuentaRegistros;
end;

procedure TLibros.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmVisitantes do
     begin
          cdsEmpVisitante.Conectar;
          cdsCaseta.Conectar;
          cdsLibros.Conectar;
          DataSource.DataSet := cdsLibros;
     end;
end;

procedure TLibros.LimpiaFiltros;
begin
     AN_NUMERO.Text := VACIO;
     VI_NUMERO.Text := VACIO;
     EV_NUMERO.Text := VACIO;
     LI_ENT_CAS.Llave := VACIO;
     LI_ENT_VIG.Llave := VACIO;
end;

procedure TLibros.PreparaFiltro;
begin
     FFiltro := VACIO;
     if( LI_STATUS.Valor <> 0 )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'LI_STATUS = %d', [ ( LI_STATUS.Valor - 1 ) ]  ) ); 
     if strLleno( AN_NUMERO.Text )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( LI_ANFITR ) Like %s', [ EntreComillas( '%'+ UpperCase( AN_NUMERO.Text ) +'%' ) ]  ) );
     if strLleno( VI_NUMERO.Text )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( LI_NOMBRE ) like %s', [ EntreComillas( '%'+ UpperCase( VI_NUMERO.Text ) +'%' ) ]  ) );
     if strLleno( EV_NUMERO.Text )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( LI_EMPRESA ) like %s', [ EntreComillas( '%'+ UpperCase( EV_NUMERO.Text ) +'%' ) ]  ) );
     if strLleno( LI_ENT_CAS.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( LI_ENT_CAS ) = %s', [ EntreComillas( UpperCase( LI_ENT_CAS.Llave ) ) ]  ) );
     if strLleno( LI_ENT_VIG.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( LI_ENT_VIG ) = %s', [ EntreComillas( UpperCase( LI_ENT_VIG.Llave ) ) ] ) );
end;


procedure TLibros.Agregar;
begin
     dmVisitantes.cdsLibros.Agregar
end;

procedure TLibros.Borrar;
begin
     dmVisitantes.cdsLibros.Borrar;
end;

procedure TLibros.Modificar;
begin
     dmVisitantes.cdsLibros.Modificar;
end;

procedure TLibros.Refresh;
begin
     dmVisitantes.cdsLibros.Refrescar;
end;

procedure TLibros.btnAnfitrionClick(Sender: TObject);
var
   iNumero: Integer;
   sDescription: String;
begin
     inherited;
     if FBuscaAnfitriones.BuscaAnfitrionDialogo( iNumero, sDescription ) then
        AN_NUMERO.Text := sDescription;
end;

procedure TLibros.CuentaRegistros;
begin
     ztbRegistros.Caption := InttoStr( dmVisitantes.cdsLibros.RecordCount );
end;

procedure TLibros.zfFecIniValidDate(Sender: TObject);
begin
     inherited;
     with Sender as TZetaFecha do
     begin
          case Tag of
               0: dmVisitantes.LibroFechaIni := Valor;
               1: dmVisitantes.LibroFechaFin := Valor;
          end;
     end;
end;

procedure TLibros.btnVisitantesClick(Sender: TObject);
var
   iNumero: Integer;
   sDescription: String;
begin
     inherited;
     if FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription ) then
        VI_NUMERO.Text := sDescription;
end;

function TLibros.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Agregar Registros De Libro Desde VisitantesMgr';
end;

function TLibros.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No Se Pueden Borrar Registros De Libro Desde VisitantesMgr';
end;

procedure TLibros.btnEmpresaClick(Sender: TObject);
var
   sKey,sDescription: String;
begin
     inherited;
     if  dmVisitantes.cdsEmpVisitante.Search( VACIO, sKey, sDescription ) then
         EV_NUMERO.Text := sDescription;
end;

procedure TLibros.btnBuscarClick(Sender: TObject);
begin
     inherited;
     PreparaFiltro;
     dmVisitantes.ObtieneFiltros( FFiltro, 'Libro' );
     CuentaRegistros;
end;

procedure TLibros.LlenaStatusLibro( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eLibroStatus;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          begin
               BeginUpdate;
               try
                  for eValueStatus := Low( eLibroStatus ) to High( eLibroStatus ) do
                       AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( FTemp.Count ) do
                  begin
                       if ( i = 0 ) then
                           Add( '0=Todos' )
                       else
                           Add( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     finally
            FreeAndNil( FTemp );
     end;
end;

end.
