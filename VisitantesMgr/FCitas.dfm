inherited Citas: TCitas
  Left = 217
  Top = 193
  Caption = 'Citas'
  ClientWidth = 762
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 762
    inherited ValorActivo2: TPanel
      Width = 503
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 97
    Width = 762
    Height = 176
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CI_FOLIO'
        Title.Caption = 'Folio'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CI_FECHA'
        Title.Caption = 'Fecha'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CI_HORA'
        Title.Caption = 'Hora'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VI_NUMERO'
        Title.Caption = 'Visitantes'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AN_NUMERO'
        Title.Caption = 'Visitando A'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CI_STATUS'
        Title.Caption = #191'Lleg'#243'?'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Asunto'
        Width = 150
        Visible = True
      end>
  end
  object pnlFiltros: TPanel [2]
    Left = 0
    Top = 19
    Width = 762
    Height = 78
    Align = alTop
    TabOrder = 2
    object lblFechaIni: TLabel
      Left = 8
      Top = 8
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Inicial:'
    end
    object lblFechaFin: TLabel
      Left = 13
      Top = 31
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Final:'
    end
    object lblAsunto: TLabel
      Left = 210
      Top = 54
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Asunto:'
    end
    object Label4: TLabel
      Left = 190
      Top = 31
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Visitando A:'
    end
    object lblVisitante: TLabel
      Left = 203
      Top = 8
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Visitante:'
    end
    object lblRegistros: TLabel
      Left = 24
      Top = 56
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Registros:'
    end
    object ztbRegistros: TZetaTextBox
      Left = 76
      Top = 54
      Width = 100
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object zfFecIni: TZetaFecha
      Left = 76
      Top = 3
      Width = 100
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '03/Mar/04'
      Valor = 38049.000000000000000000
      OnValidDate = zfFecIniValidDate
    end
    object zfFecFin: TZetaFecha
      Tag = 1
      Left = 76
      Top = 26
      Width = 100
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '03/Mar/04'
      Valor = 38049.000000000000000000
      OnValidDate = zfFecIniValidDate
    end
    object CI_ASUNTO: TZetaKeyLookup
      Left = 250
      Top = 50
      Width = 250
      Height = 21
      TabOrder = 4
      TabStop = True
      WidthLlave = 60
    end
    object btnBuscar: TBitBtn
      Left = 512
      Top = 25
      Width = 73
      Height = 33
      Hint = 'Iniciar B'#250'squeda'
      Caption = 'Buscar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnBuscarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
    end
    object VI_NUMERO: TZetaKeyLookup
      Left = 250
      Top = 4
      Width = 250
      Height = 21
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
    end
    object AN_NUMERO: TZetaKeyLookup
      Left = 250
      Top = 27
      Width = 250
      Height = 21
      TabOrder = 3
      TabStop = True
      WidthLlave = 60
    end
  end
  inherited DataSource: TDataSource
    Left = 608
    Top = 32
  end
end
