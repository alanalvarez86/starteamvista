unit ZetaTipoEntidad;

interface

{.$DEFINE MULTIPLES_ENTIDADES}
{$DEFINE CAMBIO_TNOM}

uses Classes;
type
    { Tipo Entidades}
    TipoEntidad = ( enNinguno,              {0}
                    enLibro,                {1}
                    enCita,                 {2}
                    enTAsunto,              {3}
                    enTCarro,               {4}
                    enTipoID,               {5}
                    enAnfitrio,             {6}
                    enDepto,                {7}
                    enVisita,               {8}
                    enTVisita,              {9}
                    enEmpVisita,            {10}
                    enCaseta,               {11}
                    enCorte,                {12}
                    enReporte,              {13}
                    enCampoRep,             {14}
                    enQuerys,               {15}
                    enBitacora,             {16}
                    enProceso,              {17}
                    enDiccion,              {18}
                    enFunciones,            {19}
                    enFormula,              {20}
                    enSuscrip,              {21}
                    enGlobal,               {22}
                    enMisReportes,          {23}
                    enUsuarios,             {24}
                    enCompanys,             {25}
                    enCasetaEntrada,        {26}
                    enCasetaSalida,         {27}
                    enCorteEntrada,         {28}
                    enCorteSalida           {29}
                    );

    {$ifndef MULTIPLES_ENTIDADES}
    ListaEntidades = set of TipoEntidad;
    {$endif}

const

    {$ifndef MULTIPLES_ENTIDADES}
    EntidadesDescontinuadas: ListaEntidades
    {$else}
    EntidadesDescontinuadas
    {$endif}= [];
    {$ifndef MULTIPLES_ENTIDADES}
    NomParamEntidades: ListaEntidades
    {$else}
    NomParamEntidades
    {$endif}= [];
    EntidadDefaultCondicion = enVisita;
    {$ifndef MULTIPLES_ENTIDADES}
    EntidadConCondiciones: ListaEntidades
    {$else}
    EntidadConCondiciones
    {$endif}=[];
    {$ifndef MULTIPLES_ENTIDADES}
    ReporteEspecial: ListaEntidades
    {$else}
    ReporteEspecial
    {$endif}= [];
    ReportesSinOrdenDef = [];
    {$ifdef CAMBIO_TNOM}
    { Entidades en las cuales se necesita agregar las fechas de n�mina para evaluar}
    EntidadesNomina = [  ];
    {$endif}

var
   aTipoEntidad: array[ TipoEntidad ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = (
        'Ninguno',
        'Libro de Visitas',
        'Citas',
        'Tipo de Asunto',
        'Tipo de Veh�culo',
        'Tipo de Identificaci�n',
        'Visitando A',
        'Departamentos',
        'Visitantes',
        'Tipo de Visitantes',
        'Compa��as (Proveedores)',
        'Casetas',
        'Cambio de Vigilante',
        'Datos Generales de Reportes',
        'Detalle de Campos de Reportes',
        'Condiciones',
        'Bit�cora de Errores',
        'Procesos',
        'Diccionario de Datos',
        'Lista de Funciones',
        'Formula',
        'Suscripci�n a Reportes',
        'Variables Globales, Defaults...',
        'Mis Reportes Favoritos',
        'Usuarios',
        'Empresas',
        'Caseta de Entrada',
        'Caseta de Salida',
        'Corte de Entrada',
        'Corte de Salida'
         );

function ObtieneEntidad( const Index: TipoEntidad ): String;
procedure LlenaTipoPoliza( Lista: TStrings );
function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;

implementation
uses
    ZetaTipoEntidadTools;

function ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     Result :=  aTipoEntidad[ Index  ];
end;

procedure LlenaTipoPoliza( Lista: TStrings );
begin
end;

function Dentro( const Entidad: TipoEntidad; ListaEntidades: Array of TipoEntidad ): Boolean;
begin
     Result := ZetaTipoEntidadTools.Dentro(Entidad,ListaEntidades);
end;

end.
