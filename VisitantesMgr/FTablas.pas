unit FTablas;

interface

uses ZBaseTablasConsulta_DevEx;

type
  TCatTipoID = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatTipoCarro = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatTipoAsunto = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatTipoVisita = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatDepto = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{ ***** TCatTipoID **** }

procedure TCatTipoID.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoID;
     HelpContext := H_VISMGR_CONS_TIDENTIF;
end;

{ ****** TCatTipoCarro ****** }

procedure TCatTipoCarro.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoCarro;
     HelpContext := H_VISMGR_CONS_TVEHICULO;
end;

{ ***** TCatTipoAsunto **** }

procedure TCatTipoAsunto.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoAsunto;
     HelpContext := H_VISMGR_CONS_TASUNTO;
end;

{ ***** TCatTipoVisita **** }

procedure TCatTipoVisita.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoVisita;
     HelpContext := H_VISMGR_CONS_TVISITANTE;
end;

{ ***** TCatDepto **** }

procedure TCatDepto.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsDepto;
     HelpContext := H_VISMGR_CONS_DEPARTAMENTO;
end;


end.
