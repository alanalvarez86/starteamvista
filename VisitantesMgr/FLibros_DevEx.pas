unit FLibros_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,ZBaseConsulta, Db, ExtCtrls, StdCtrls, Mask, ZetaFecha, ZetaKeyLookup,
  ZetaDBTextBox, Grids, DBGrids, ZetaDBGrid, Buttons, ZetaNumero, ComCtrls,
  ZetaKeyCombo, ZetaCommonLists, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  ZetaKeyLookup_DevEx, cxButtons;

type
  TLibros_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    lblFechaIni: TLabel;
    zfFecIni: TZetaFecha;
    zfFecFin: TZetaFecha;
    lblFechaFin: TLabel;
    lblVisitante: TLabel;
    lblAnfitrion: TLabel;
    btnBuscar: TcxButton;
    Label1: TLabel;
    LI_STATUS: TZetaKeyCombo;
    LI_FOLIO: TcxGridDBColumn;
    LI_ENT_FEC: TcxGridDBColumn;
    LI_SAL_FEC: TcxGridDBColumn;
    LI_ENT_HOR: TcxGridDBColumn;
    LI_SAL_HOR: TcxGridDBColumn;
    LI_NOMBRE: TcxGridDBColumn;
    LI_EMPRESA: TcxGridDBColumn;
    LI_ANFITR: TcxGridDBColumn;
    CA_NOM_ENT: TcxGridDBColumn;
    CA_NOM_SAL: TcxGridDBColumn;
    US_ENT_LIB: TcxGridDBColumn;
    US_SAL_LIB: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    VI_NUMERO: TZetaKeyLookup_DevEx;
    AN_NUMERO: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure btnAnfitrionClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zfFecIniValidDate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    procedure PreparaFiltro;
    procedure LimpiaFiltros;
    procedure LlenaStatusLibro( oLista: TStrings; oLFijas: ListasFijas );
    function GetDescLookup(sTexto:String): String;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  Libros_DevEx: TLibros_DevEx;

implementation
uses DVisitantes, DSistema, ZetaCommonClasses, ZetaCommonTools, FBuscaAnfitriones_DevEx,
     FBusquedaLibro;
{$R *.DFM}

{ TLibros }
procedure TLibros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          SetFechasInicio;
          zfFecIni.Valor := LibroFechaIni;
          zfFecFin.Valor := LibroFechaFin;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          VI_NUMERO.LookupDataset := cdsVisitaCitas;
     end;
     with LI_STATUS do
     begin
          LlenaStatusLibro( Lista, lfLibroStatus );
          ItemIndex := 0;
     end;
     HelpContext:= H_VISMGR_CONS_LIBROS;
     //HelpContext := H60634_Condiciones;
end;

procedure TLibros_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     LimpiaFiltros;
     dmvisitantes.cdsLibros.Refrescar;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('LI_FOLIO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TLibros_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmVisitantes do
     begin
          cdsEmpVisitante.Conectar;
          cdsCaseta.Conectar;
          cdsLibros.Conectar;
          cdsVisitanteLookup.Conectar;
          cdsVisitaCitas.Conectar;
          DataSource.DataSet := cdsLibros;
     end;
end;

procedure TLibros_DevEx.LimpiaFiltros;
begin
     AN_NUMERO.Llave := VACIO;
     VI_NUMERO.Llave := VACIO;
end;

procedure TLibros_DevEx.PreparaFiltro;
var
sVisitante, sAnfitrion:string;
begin
     FFiltro := VACIO;
     if( LI_STATUS.Valor <> 0 )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'LI_STATUS = %d', [ ( LI_STATUS.Valor - 1 ) ]  ) );
     if strLleno( AN_NUMERO.Llave )then
     begin
          sAnfitrion  := AN_NUMERO.Descripcion;
          sAnfitrion  := GetDescLookup(sAnfitrion);
          FFiltro := ConcatFiltros( FFiltro, Format({'Upper( L.AN_NUMERO ) = %s'} 'Upper( LI_ANFITR ) Like %s', [ EntreComillas( '%'+ UpperCase( sAnfitrion ) +'%' ) ]  ) );
     end;
     if strLleno( VI_NUMERO.Llave )then
     begin
          sVisitante := VI_NUMERO.Descripcion;
          sVisitante := GetDescLookup(sVisitante);   // Como el lookup muestra la informacion con coma, e iniciando como apellido debemos darle formato antes de realizar el LIKE
          FFiltro := ConcatFiltros( FFiltro, Format( {'Upper( V.VI_NUMERO ) = %s'} 'Upper( LI_NOMBRE ) like %s', [ EntreComillas( '%'+ UpperCase( sVisitante ) +'%' ) ]  ) );
     end;
end;


function TLibros_DevEx.GetDescLookup(sTexto:String): String;
var
sTemp :String;
i : integer;
begin
     i:= Pos( ',', TRIM( sTexto ) );
     sTemp := copy( sTexto, i + 2, sTexto.Length ); // se le suman dos un es el caracter ',' y el otro caracter es un espacio
     sTemp := sTemp + ' '+ copy( sTexto, 1, i-1 );
    Result:= sTemp;
end;

procedure TLibros_DevEx.Agregar;
begin
     dmVisitantes.cdsLibros.Agregar
end;

procedure TLibros_DevEx.Borrar;
begin
     dmVisitantes.cdsLibros.Borrar;
end;

procedure TLibros_DevEx.Modificar;
begin
     dmVisitantes.cdsLibros.Modificar;
     DoBestFit;
end;

procedure TLibros_DevEx.Refresh;
begin
     dmVisitantes.cdsLibros.Refrescar;
     DoBestFit;
end;

procedure TLibros_DevEx.btnAnfitrionClick(Sender: TObject);
var
   iNumero: Integer;
   sDescription: String;
begin
     inherited;
     FBuscaAnfitriones_DevEx.BuscaAnfitrionDialogo( iNumero, sDescription );
end;

procedure TLibros_DevEx.zfFecIniValidDate(Sender: TObject);
begin
     inherited;
     with Sender as TZetaFecha do
     begin
          case Tag of
               0: dmVisitantes.LibroFechaIni := Valor;
               1: dmVisitantes.LibroFechaFin := Valor;
          end;
     end;
end;


function TLibros_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No se pueden agregar registros de libro desde VisitantesMgr';
end;

function TLibros_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No se pueden borrar registros de libro desde VisitantesMgr';
end;


procedure TLibros_DevEx.btnBuscarClick(Sender: TObject);
begin
     inherited;
     PreparaFiltro;
     dmVisitantes.LibroFechaIni :=  zfFecIni.Valor;
     dmVisitantes.LibroFechaFin :=  zfFecFin.Valor;
     dmVisitantes.ObtieneFiltros( FFiltro, 'Libro' );
     DoBestFit;
end;

procedure TLibros_DevEx.LlenaStatusLibro( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eLibroStatus;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          begin
               BeginUpdate;
               try
                  for eValueStatus := Low( eLibroStatus ) to High( eLibroStatus ) do
                       AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( FTemp.Count ) do
                  begin
                       if ( i = 0 ) then
                           Add( '0=Todos' )
                       else
                           Add( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     finally
            FreeAndNil( FTemp );
     end;
end;

end.
