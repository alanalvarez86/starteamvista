unit FCatVisitantes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx,ZBaseConsulta, Grids, DBGrids, Db, ExtCtrls,imageenview, StdCtrls, DBCtrls,
  Buttons, ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  ieview, cxButtons,FToolsImageEn;

type
  TCatVisitantes_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Panel2: TPanel;
    pnFoto: TPanel;
    GroupBox2: TGroupBox;
    btnFoto: TcxButton;
    VI_FOTO: TImageEnView;
    VI_NUMERO: TcxGridDBColumn;
    Pretty_Visit: TcxGridDBColumn;
    EV_NOMBRE: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    VI_STATUS: TcxGridDBColumn;
    VI_NACION: TcxGridDBColumn;
    VI_MIGRA: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    edtPista: TEdit;
    Label1: TLabel;
    btnFiltrar: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btnFotoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ShowFoto;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnFiltrarClick(Sender: TObject);
    procedure edtPistaKeyPress(Sender: TObject; var Key: Char);
   private
    FCargaEstructura: Boolean;
    procedure ConfigAgrupamiento;
    procedure ConectarDatos;
    procedure AplicarFiltro;
    procedure LimpiaEdit(sTexto: string);
   protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  CatVisitantes_DevEx: TCatVisitantes_DevEx;
const
    K_DELETE = 'D';
    K_TITTLE = 'Visitantes';
    K_TITLE_ADVERTENCIA = 'Advertencia';
    K_MSG_BORRAR = 'No hay datos para borrar';
    K_MSG_MODIFICAR = 'No hay datos para modificar';
    K_MSG_ADVERTENCIA3 = 'No se encontraron coincidencias con la pista especificada.';
    K_MSG_ADVERTENCIA1 ='No se ha especificado ning�n filtro,';
    K_MSG_ADVERTENCIA2 = 'esta consulta podr�a tomar m�s tiempo de lo normal.';
    K_VISIT_DEFAULT ='99999999';

implementation

uses DVisitantes,
     FBuscaVisitantes,
     ZetaDialogo,
   	 ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatVisitantes }
procedure TCatVisitantes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := TRUE;
     HelpContext := H_VISMGR_CONS_VISITANTES;
end;

procedure TCatVisitantes_DevEx.FormShow(Sender: TObject);
begin
     ConfigAgrupamiento;
     ApplyMinWidth;
     inherited;
     FCargaEstructura := TRUE;
     ConectarDatos;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('VI_NUMERO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;
procedure TCatVisitantes_DevEx.Connect;
begin
     {with dmVisitantes do
     begin
          cdsVisitante.Conectar;
          DataSource.DataSet := cdsVisitante;
     end; }   //OLD
end;

procedure TCatVisitantes_DevEx.Refresh;
begin
     AplicarFiltro;
end;
procedure TCatVisitantes_DevEx.Agregar;
begin
     dmVisitantes.cdsVisitante.Agregar;
     LimpiaEdit(edtPista.Text);
end;

procedure TCatVisitantes_DevEx.Borrar;
begin
     dmVisitantes.cdsVisitante.Borrar;  //Debe salir No hay datos para borrar, capture una pista y realice un filtrado de datos.
     LimpiaEdit(edtPista.Text);
end;

procedure TCatVisitantes_DevEx.Modificar;
begin
     dmVisitantes.cdsVisitante.Modificar;
     LimpiaEdit(edtPista.Text);
end;

procedure TCatVisitantes_DevEx.AplicarFiltro;
var
   lMuestraAdvertencia:Boolean;
begin
     lMuestraAdvertencia := TRUE;
     if  strVacio( edtPista.Text ) then
     begin
        if ZetaDialogo.ZWarningConfirm( K_TITLE_ADVERTENCIA, K_MSG_ADVERTENCIA1 + CR_LF + K_MSG_ADVERTENCIA2 +CR_LF + '� Desea Continuar ?', 0, mbNo )  then
        begin
             ConectarDatos;
             DoBestFit;
        end
        else
            lMuestraAdvertencia := FALSE;
     end
     else
     begin
          ConectarDatos;
          DoBestFit;
     end;
     //Si despues de aplicar el filtro con o sin pista, la busqueda regresa cero registros. Se le informa al usuario
     if( dmVisitantes.cdsVisitante.RecordCount = 0) and (lMuestraAdvertencia)then
     begin
           ZetaDialogo.ZInformation( K_TITTLE, K_MSG_ADVERTENCIA3 ,0 );
     end;
end;
procedure TCatVisitantes_DevEx.ConectarDatos;
begin
   with dmVisitantes do
   begin
      if FCargaEstructura then
      begin
           FCargaEstructura := FALSE;
           BuscaVisitantes( K_VISIT_DEFAULT );
      end
      else
      begin
           FiltroCatVisitante :=  edtPista.Text;
           BuscaVisitantes( FiltroCatVisitante );
      end;
      DataSource.DataSet := cdsVisitante;
   end;
end;

procedure TCatVisitantes_DevEx.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     AplicarFiltro;
end;

procedure TCatVisitantes_DevEx.btnFotoClick(Sender: TObject);
begin
     inherited;
     pnFoto.Visible := (not pnFoto.Visible);
     VI_FOTO.Visible := btnFoto.Down;
     btnFoto.Down := not btnFoto.Down;
     if ( VI_FOTO.Visible ) then
        ShowFoto;
end;
procedure TCatVisitantes_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if ( Field = nil ) and (VI_FOTO.Visible) then
	    ShowFoto;  
end;

procedure TCatVisitantes_DevEx.DoLookup;
var
   iNumero: Integer;
   sDescription: String;
begin
     if FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription ) then
     begin
          with dmVisitantes.cdsVisitante do
          begin
               Locate( LookupKeyField, IntTostr( iNumero ), [] );
          end;
     end;
end;

procedure TCatVisitantes_DevEx.edtPistaKeyPress(Sender: TObject; var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               Key := Chr( 0 );
               btnFiltrar.Click;
          end;
     end;
end;

procedure TCatVisitantes_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     VI_NUMERO.Options.Grouping  := FALSE;
     VI_NUMERO.Options.Filtering := FALSE;
     Pretty_Visit.Options.Grouping:= FALSE;
     Pretty_Visit.Options.Filtering:= FALSE;
     EV_NOMBRE.Options.Grouping:= FALSE;
     TB_ELEMENT.Options.Grouping:= TRUE;
     VI_STATUS.Options.Grouping:= TRUE;
     VI_NACION.Options.Grouping:= TRUE;
     VI_MIGRA.Options.Grouping:= FALSE;
end;

procedure TCatVisitantes_DevEx.ShowFoto;
begin
     if( strLleno( dmVisitantes.cdsVisitante.FieldByName( 'VI_FOTO' ).AsString ) and
       ( dmVisitantes.cdsVisitante.FieldByName( 'VI_FOTO' ).AsString <> K_DELETE ) ) then
     begin
          FToolsImageEn.AsignaBlobAImagen( VI_FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' );
     end;
     if( strVacio( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString ) or
       ( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString = K_DELETE ) ) then
     begin
          VI_FOTO.Clear;
     end;
end;

procedure TCatVisitantes_DevEx.LimpiaEdit(sTexto: string);
var
   sPista: String;
begin
     sPista :=  sTexto;
     edtPista.Clear;

     if strVacio( sPista ) or (sPista <>  dmVisitantes.FiltroCatVisitante )then
     begin
          sPista := dmVisitantes.FiltroCatVisitante;
     end;
     edtPista.Text := sPista;
end;


end.