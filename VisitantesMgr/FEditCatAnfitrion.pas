unit FEditCatAnfitrion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, Mask, ZetaNumero, ZetaSmartLists;

type
  TEditCatAnfitrion = class(TBaseEdicion)
    lblEmpleado: TLabel;
    lblDepto: TLabel;
    lblApePat: TLabel;
    lblApeMat: TLabel;
    lblNombre: TLabel;
    lblStatus: TLabel;
    AN_NUMERO: TZetaDBNumero;
    AN_DEPTO: TZetaDBKeyLookup;
    AN_NOMBRES: TDBEdit;
    AN_APE_PAT: TDBEdit;
    AN_APE_MAT: TDBEdit;
    AN_STATUS: TZetaDBKeyCombo;
    Label1: TLabel;
    AN_TEL: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatAnfitrion: TEditCatAnfitrion;

implementation

uses dVisitantes,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TEditCatAnfitrion.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_ANFITRION;
     FirstControl := AN_NUMERO;
     AN_DEPTO.LookupDataset := dmVisitantes.cdsDepto;
     //TipoValorActivo1 := stExpediente;
     HelpContext:= H_VISMGR_EDIT_ANFITRIONES;
end;

procedure TEditCatAnfitrion.Connect;
begin
     with dmVisitantes do
     begin
          cdsDepto.Conectar;
          DataSource.DataSet:= cdsAnfitrion;
     end;
end;

end.
