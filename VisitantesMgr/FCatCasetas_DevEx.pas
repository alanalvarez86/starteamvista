unit FCatCasetas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxClasses, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, Data.DB;

type
  TCatCasetas_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGridDBTableView1: TcxGridDBTableView;
    ZetaDBGridDBTableView1CA_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableView1CA_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableView1CA_UBICA: TcxGridDBColumn;
    ZetaDBGridDBTableView1CA_STATUS: TcxGridDBColumn;
    ZetaDBGridDBTableView1CA_CORTE: TcxGridDBColumn;
    ZetaDBGridDBTableView1CA_ABIERTA: TcxGridDBColumn;
    ZetaDBGridDBTableView1RE_NOMBRE: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    CA_CODIGO: TcxGridDBColumn;
    CA_NOMBRE: TcxGridDBColumn;
    CA_UBICA: TcxGridDBColumn;
    CA_STATUS: TcxGridDBColumn;
    CA_CORTE: TcxGridDBColumn;
    CA_ABIERTA: TcxGridDBColumn;
    RE_NOMBRE: TcxGridDBColumn;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  CatCasetas_DevEx: TCatCasetas_DevEx;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatVisitantes }
procedure TCatCasetas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     HelpContext := H_VISMGR_CONS_CASETAS;
end;

procedure TCatCasetas_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CA_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCasetas_DevEx.Connect;
begin
     with dmVisitantes do
     begin
          cdsCaseta.Conectar;
          DataSource.DataSet := cdsCaseta;
     end;
end;

procedure TCatCasetas_DevEx.Refresh;
begin
     dmVisitantes.cdsCaseta.Refrescar;
     DoBestFit;
end;

procedure TCatCasetas_DevEx.Agregar;
begin
     dmVisitantes.cdsCaseta.Agregar;
end;

procedure TCatCasetas_DevEx.Borrar;
begin
    dmVisitantes.cdsCaseta.Borrar;
    DoBestFit;
end;

procedure TCatCasetas_DevEx.Modificar;
begin
     dmVisitantes.cdsCaseta.Modificar;
end;





end.
