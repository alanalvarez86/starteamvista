unit FCatCompanys_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxClasses, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCatCompanys_DevEx = class(TBaseGridLectura_DevEx)
    EV_NUMERO: TcxGridDBColumn;
    EV_NOMBRE: TcxGridDBColumn;
    EV_RAZ_SOC: TcxGridDBColumn;
    EV_STATUS: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatCompanys_DevEx: TCatCompanys_DevEx;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatCompanys}
procedure TCatCompanys_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     HelpContext := H_VISMGR_CONS_COMPANIAS;
end;

procedure TCatCompanys_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('EV_NUMERO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatCompanys_DevEx.Connect;
begin
     with dmVisitantes do
     begin
          cdsEmpVisitante.Conectar;
          DataSource.DataSet := cdsEmpVisitante;
     end;
end;

procedure TCatCompanys_DevEx.Refresh;
begin
     dmVisitantes.cdsEmpVisitante.Refrescar;
     DoBestFit;
end;

procedure TCatCompanys_DevEx.Agregar;
begin
     dmVisitantes.cdsEmpVisitante.Agregar;
end;

procedure TCatCompanys_DevEx.Borrar;
begin
    dmVisitantes.cdsEmpVisitante.Borrar;
    DoBestFit;
end;

procedure TCatCompanys_DevEx.Modificar;
begin
     dmVisitantes.cdsEmpVisitante.Modificar;
end;

procedure TCatCompanys_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     EV_NUMERO.Options.Grouping:= FALSE;
     EV_NOMBRE.Options.Grouping:= FALSE;
     EV_RAZ_SOC.Options.Grouping:= FALSE;
     EV_STATUS.Options.Grouping:= TRUE;
end;

end.
