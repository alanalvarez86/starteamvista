unit FTablas_DevEx;

interface

uses ZBaseTablasConsulta_DevEx;

type
  TCatTipoID_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatTipoCarro_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatTipoAsunto_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatTipoVisita_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatDepto_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{ ***** TCatTipoID **** }

procedure TCatTipoID_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoID;
     HelpContext := H_VISMGR_CONS_TIDENTIF;
end;

{ ****** TCatTipoCarro ****** }

procedure TCatTipoCarro_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoCarro;
     HelpContext := H_VISMGR_CONS_TVEHICULO;
end;

{ ***** TCatTipoAsunto **** }

procedure TCatTipoAsunto_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoAsunto;
     HelpContext := H_VISMGR_CONS_TASUNTO;
end;

{ ***** TCatTipoVisita **** }

procedure TCatTipoVisita_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsTipoVisita;
     HelpContext := H_VISMGR_CONS_TVISITANTE;
end;

{ ***** TCatDepto **** }

procedure TCatDepto_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmVisitantes.cdsDepto;
     HelpContext := H_VISMGR_CONS_DEPARTAMENTO;
end;


end.
