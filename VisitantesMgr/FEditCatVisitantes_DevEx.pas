unit FEditCatVisitantes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaDBTextBox,
  ZetaKeyCombo, ZetaFecha, ZetaKeyLookup, Mask, ComCtrls, {TDMULTIP, MMOpen,}
  imageenview, ieview, dbimageen,ieopensavedlg, hyieutils,
  ZetaNumero, {Videocap,} ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  dxBarBuiltInMenu, cxPC,FToolsImageEn;

type
  TEditCatVisitantes_DevEx = class(TBaseEdicion_DevEx)
    PageControl1: TcxPageControl;
    tsGenerales: TcxTabSheet;
    lblApePat: TLabel;
    VI_APE_PAT: TDBEdit;
    VI_APE_MAT: TDBEdit;
    lblApeMat: TLabel;
    lblNombre: TLabel;
    VI_NOMBRES: TDBEdit;
    lblNoVisita: TLabel;
    VI_TIPO: TZetaDBKeyLookup_DevEx;
    lblTipo: TLabel;
    EV_NUMERO: TZetaDBKeyLookup_DevEx;
    lblNacion: TLabel;
    VI_NACION: TDBEdit;
    lblFecNac: TLabel;
    VI_FEC_NAC: TZetaDBFecha;
    lblStatus: TLabel;
    lblSexo: TLabel;
    lblVisitante: TLabel;
    tsFoto: TcxTabSheet;
    btnAgregaFoto: TcxButton;
   // MMOpenDialog1: TMMOpenDialog;
    lblMigra: TLabel;
    VI_MIGRA: TDBEdit;
    VI_NUMERO: TZetaDBNumero;
    VI_STATUS: TZetaDBKeyCombo;
    lblAnfitrion: TLabel;
    AN_NUMERO: TZetaDBKeyLookup_DevEx;
    lblAsunto: TLabel;
    VI_ASUNTO: TZetaDBKeyLookup_DevEx;
    VI_SEXO: TZetaKeyCombo;
    btnCapturar: TcxButton;
    btnVideo: TcxButton;
    lblDriver: TLabel;
    cbDrivers: TComboBox;
    btnBorrar: TcxButton;
    Panel1: TPanel;
    OpenImageEnDialog: TOpenImageEnDialog;
    SaveImageEnDialog: TSaveImageEnDialog;
    FOTO: TImageEnView;
    Timer: TTimer;
    //VideoCap1: TVideoCap;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAgregaFotoClick(Sender: TObject);
    procedure VI_SEXOChange(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure btnCapturarClick(Sender: TObject);
    procedure btnBorrarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure cbDriversChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerTimer(Sender: TObject);
  private
    { Private declarations }
   // lBorrado: Boolean;{OP: acl}
    FCierraForma: Boolean;
    FOperacionFoto: Boolean;
    FStatusVideo: Boolean;
    procedure IniciaGenero;
    procedure SetControls(lHabilita: Boolean);
    function DriverManager: Boolean;
    procedure TomaFotografia;
    function InicializaVideo: Boolean;
    procedure ShowFoto;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatVisitantes_DevEx: TEditCatVisitantes_DevEx;
const
    K_DELETE = 'D';
    K_APAGAR_CAM_HINT = 'Apagar video';
    K_PRENDER_CAM_HINT ='Iniciar video';
    K_PRENDER_CAM = 'Prender c�mara';
    K_APAGAR_CAM = 'Apagar c�mara';

implementation

uses dVisitantes,
     ZetaCommonLists,
     ZetaDialogo,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaCommonClasses;
    // FToolsFoto,
    // FToolsVideo;{OP: 05/06/08}

{$R *.DFM}

procedure TEditCatVisitantes_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     with FOTO.IO.DShowParams do
     begin
          Stop;
          Disconnect;
          Update;
     end;
end;

procedure TEditCatVisitantes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_VISITANTE;
     FirstControl := VI_NUMERO;
     FCierraForma := False;
     FOperacionFoto := FALSE;
     with dmVisitantes do
     begin
          VI_TIPO.LookupDataset := cdsTipoVisita;
          EV_NUMERO.LookupDataset := cdsEmpVisitante;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          VI_ASUNTO.LookupDataset := cdsTipoAsunto;
     end;
     HelpContext:= H_VISMGR_EDIT_VISITANTES;
     VI_SEXO.ItemIndex := 0;
     TipoValorActivo1 := stExpediente;
end;

procedure TEditCatVisitantes_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetControls( True );
     FStatusVideo:=  FALSE; //@DC - Variable booleana para el control de video(encendido o apagado)
     with PageControl1 do
          ActivePage := tsGenerales;
end;

procedure TEditCatVisitantes_DevEx.Connect;
begin
     with dmVisitantes do
     begin
          cdsTipoVisita.Conectar;
          cdsEmpVisitante.Conectar;
          cdsVisitaCitas.Conectar;
          cdsAnfitrion.Conectar;
          cdsTipoAsunto.Conectar;
          cdsVisitante.Conectar;
          DataSource.DataSet:= cdsVisitante;
     end;
     IniciaGenero;
end;

procedure TEditCatVisitantes_DevEx.IniciaGenero;
begin
     with dmVisitantes.cdsVisitante, VI_SEXO do
     begin
          if ( FieldByName( 'VI_SEXO' ).AsString = ObtieneElemento( lfSexo, Ord(esFemenino) ) ) then
             ItemIndex:= Ord( esFemenino )
          else
             ItemIndex:= Ord( esMasculino );
     end;
end;

procedure TEditCatVisitantes_DevEx.btnAgregaFotoClick(Sender: TObject);
begin
     inherited;
     FOperacionFoto := TRUE;
     if OpenImageEnDialog.Execute then
     begin
          SetControls( True );
          Foto.IO.LoadFromFile( OpenImageEnDialog.FileName );
     end;
     FToolsImageEn.AsignaImagenABlob( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' );
     FOperacionFoto := FALSE;
end;

procedure TEditCatVisitantes_DevEx.VI_SEXOChange(Sender: TObject);
var
   sSexo: String;
begin
     inherited;
     sSexo:= ObtieneElemento( lfSexo, VI_SEXO.ItemIndex );
     with dmVisitantes.cdsVisitante do
     begin
          if ( sSexo <> FieldByName( 'VI_SEXO' ).AsString ) then
          begin
               if not Editing then
                  Edit;
               FieldByName( 'VI_SEXO' ).AsString:= sSexo;
          end;
     end;
end;

procedure TEditCatVisitantes_DevEx.btnVideoClick(Sender: TObject);
begin
     inherited;
     //dmvisitantes.cdsVisitante.CancelUpdates; //INVESTIGAR
     if DriverManager then
     begin
          SetControls( False );
          FCierraForma :=  not InicializaVideo;
          Timer.Enabled := FCierraForma;
     end
     else
     begin
          FCierraForma := TRUE;
          SetControls( True );
          Timer.Enabled := FCierraForma;
     end;
end;

procedure TEditCatVisitantes_DevEx.SetControls( lHabilita: Boolean );
begin
     //btnVideo.Enabled := lHabilita;
     btnCapturar.Enabled := not lHabilita;
end;

procedure TEditCatVisitantes_DevEx.btnCapturarClick(Sender: TObject);
begin
     inherited;
     FOperacionFoto := TRUE;
     TomaFotografia;
     FOperacionFoto := FALSE;
end;

procedure TEditCatVisitantes_DevEx.TomaFotografia;
var
   sMsgError: String;
begin
     SetControls( True );
     with Foto.IO.DShowParams do

     begin
          GetSample( Foto.IEBitmap );
          Stop;
          Disconnect;
          Update;
     end;
     //DACP como el componente imageEn no es  DATAAWARE tenemos que emular la funcionalidad por lo que se agrega el siguiente codigo
     FStatusVideo := FALSE;
     btnVideo.Caption := K_PRENDER_CAM;
     btnVideo.Hint := K_PRENDER_CAM_HINT;
     btnVideo.OptionsImage.ImageIndex:=4;

      { //@DACP - Para visitantes no aplicamos la reduccion de tamanio de las fotos
     if ( not Global.GetGlobalBooleano( K_GLOBAL_CONSERVAR_TAM_FOTOS ) ) then
     begin
          if ( not FToolsImageEn.ResizeImagen( FOTO, sMsgError ) ) then
             ZError( self.Caption, sMsgError, 0);
     end; }
     FToolsImageEn.AsignaImagenABlob( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' );// primero asignamos la imagen que tomamos al Dataset
     FToolsImageEn.AsignaBlobAImagen( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' ); // y luego asignamos la imagen tomada al componente para que se actualize y muestre la foto correcta

end;

procedure TEditCatVisitantes_DevEx.btnBorrarClick(Sender: TObject);
begin
     inherited;
     FOperacionFoto := True;
     with dmVisitantes.cdsVisitante do
     begin
          FOTO.Clear;
          if ( not ( State in [ dsEdit, dsInsert ] ) ) then
             Edit;
          FieldByName('VI_FOTO').AsString := K_DELETE;
     end;
     FOperacionFoto := FALSE;
end;

procedure TEditCatVisitantes_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if not FOperacionFoto then
        ShowFoto;
     if( FStatusVideo ) then
        InicializaVideo;       //apaga el video si esta activo y muestra la foto en caso de que se tenga una
end;

function TEditCatVisitantes_DevEx.DriverManager: Boolean;

begin
     Result := True;
     cbDrivers.Items.Assign( FOTO.IO.DShowParams.VideoInputs );
     with cbDrivers.Items do

     begin
          if( Count > 0 )then
          begin
               cbDrivers.ItemIndex := 0;
               cbDrivers.Visible := ( Count > 1 );
               lblDriver.Visible := ( Count > 1 );
               FToolsImageEn.SetDriver( FOTO, cbDrivers.ItemIndex );
          end


          else
          begin
               Result := False;
               cbDrivers.Visible := False;
               lblDriver.Visible := False;
               OK_DevEx.Enabled := False;
               ZetaDialogo.ZError( 'Error al capturar imagen', 'No se tienen dispositivos de video instalados', 0 );
          end;

     end;
end;

procedure TEditCatVisitantes_DevEx.cbDriversChange(Sender: TObject);
begin
     inherited;
     with FOTO.IO.DShowParams do
     begin
          Stop;
          Disconnect;
          Update;
     end;
     FStatusVideo := FALSE;
     FToolsImageEn.SetDriver( FOTO, cbDrivers.ItemIndex );
     InicializaVideo;
end;

function TEditCatVisitantes_DevEx.InicializaVideo: Boolean;
var
   oCursor: TCursor;
begin
     Result := TRUE;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     if ( not FStatusVideo ) then
     begin

           btnVideo.Caption := K_APAGAR_CAM;
           btnVideo.Hint := K_APAGAR_CAM_HINT;
           btnVideo.OptionsImage.ImageIndex:=3;
           FOTO.Clear;                   //DACP se limpia la foto para poder definir el tama�o del video
           try
              try
                 with FOTO.IO.DShowParams do
                 begin
                      update; //por eso se hace un rezise del objeto y se le asigna un background.
                      FOTO.Proc.ImageResize(272,206, iehCenter, ievCenter);  //DACP Primero se tiene que definir el area que en la que se mostrara la foto
                      FOTO.Background := clBtnFace;
                      Connect;
                      Run;
                 end;
                 SetControls( FALSE );
                 FStatusVideo:=TRUE;
              except
                    on Error: Exception do
                    begin
                         SetControls( TRUE );
                         OK_DevEx.Enabled := False;
                         ZetaDialogo.ZError( 'Error al capturar la imagen', 'El Dispositivo seleccionado no est� disponible', 0 );
                         Result := False;
                    end;
              end;
           finally
                  Screen.Cursor := oCursor;
           end;
     end
     else
     begin
          try
             FStatusVideo:=  FALSE;
             btnVideo.Caption := K_PRENDER_CAM;
             btnVideo.Hint := K_PRENDER_CAM_HINT;
             btnVideo.OptionsImage.ImageIndex:=4;
             with FOTO.IO.DShowParams do
             begin
                  Stop;
                  Disconnect;
                  Update;
             end;
             SetControls( TRUE ); //apagamos el boton de tomar fotografia
             ShowFoto;
          finally
                  Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TEditCatVisitantes_DevEx.TimerTimer(Sender: TObject);
begin
     inherited;
     if FCierraForma then
     begin
          Timer.Enabled := False;
          Close;
     end;
end;

 procedure TEditCatVisitantes_DevEx.ShowFoto;
 begin
     if( strLleno( dmVisitantes.cdsVisitante.FieldByName( 'VI_FOTO' ).AsString ) and
       ( dmVisitantes.cdsVisitante.FieldByName( 'VI_FOTO' ).AsString <> K_DELETE ) ) then
     begin
          btnBorrar.Enabled := TRUE;
          FToolsImageEn.AsignaBlobAImagen( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' );
     end;
     if( strVacio( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString ) or
       ( dmVisitantes.cdsVisitante.FieldByName('VI_FOTO').AsString = K_DELETE ) ) then
     begin
          //FToolsImageEn.AsignaBlobAImagen( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' ); // El campo vendra vacio
          btnBorrar.Enabled := FALSE;
          FOTO.Clear;
          //FOTO.Proc.ImageResize(272,206, iehCenter, ievCenter);  //DACP Si el objeto no tiene cargada una foto, al inicializar el video no se muestra nada
     end;
 end;


end.

