inherited EditCitas_DevEx: TEditCitas_DevEx
  Left = 241
  Top = 174
  Caption = 'Edici'#243'n de Citas'
  ClientHeight = 427
  ClientWidth = 423
  ExplicitWidth = 429
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 13
  object lblAsunto: TLabel [0]
    Left = 6
    Top = 163
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Asunto:'
  end
  object lblAnfitrion: TLabel [1]
    Left = 25
    Top = 138
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Visitando A:'
  end
  object lblVisitante: TLabel [2]
    Left = 38
    Top = 115
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Visitante:'
  end
  object lblFecha: TLabel [3]
    Left = 48
    Top = 93
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
  end
  object lblHora: TLabel [4]
    Left = 55
    Top = 69
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora:'
  end
  object lblStatus: TLabel [5]
    Left = 40
    Top = 188
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = #191'Lleg'#243'?:'
  end
  object lblFolio: TLabel [6]
    Left = 56
    Top = 47
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Folio:'
  end
  object lblUsuario: TLabel [7]
    Left = 42
    Top = 211
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario:'
  end
  object US_CODIGO: TZetaDBTextBox [8]
    Left = 85
    Top = 210
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_NOMBRE'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CI_FOLIO: TZetaDBTextBox [9]
    Left = 85
    Top = 45
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CI_FOLIO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CI_FOLIO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 391
    Width = 423
    TabOrder = 9
    ExplicitTop = 391
    ExplicitWidth = 423
    inherited OK_DevEx: TcxButton
      Left = 257
      ExplicitLeft = 257
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 336
      ExplicitLeft = 336
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 423
    TabOrder = 0
    ExplicitWidth = 423
    inherited ValorActivo2: TPanel
      Width = 97
      ExplicitWidth = 97
      inherited textoValorActivo2: TLabel
        Width = 91
        ExplicitLeft = 11
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 291
    Top = 190
    TabOrder = 13
    ExplicitLeft = 291
    ExplicitTop = 190
  end
  object CI_FECHA: TZetaDBFecha [13]
    Left = 85
    Top = 88
    Width = 102
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '08-Mar-04'
    Valor = 38054.000000000000000000
    DataField = 'CI_FECHA'
    DataSource = DataSource
  end
  object CI_HORA: TZetaDBHora [14]
    Left = 85
    Top = 65
    Width = 40
    Height = 21
    EditMask = '99:99;0'
    TabOrder = 1
    Text = '    '
    Tope = 24
    Valor = '    '
    DataField = 'CI_HORA'
    DataSource = DataSource
  end
  object CI_ASUNTO: TZetaDBKeyLookup_DevEx [15]
    Left = 85
    Top = 160
    Width = 320
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 5
    TabStop = True
    WidthLlave = 70
    DataField = 'CI_ASUNTO'
    DataSource = DataSource
  end
  object gbObserva: TcxGroupBox [16]
    Left = 2
    Top = 243
    Caption = ' Observaciones: '
    TabOrder = 7
    Height = 142
    Width = 414
    object CI_OBSERVA: TcxDBMemo
      Left = 3
      Top = 15
      Align = alClient
      DataBinding.DataField = 'CI_OBSERVA'
      DataBinding.DataSource = DataSource
      Properties.ScrollBars = ssVertical
      TabOrder = 0
      Height = 117
      Width = 408
    end
  end
  object CI_STATUS: TZetaDBKeyCombo [17]
    Left = 85
    Top = 185
    Width = 120
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 6
    ListaFija = lfCitaStatus
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'CI_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object VI_NUMERO: TZetaDBKeyLookup_DevEx [18]
    Left = 85
    Top = 113
    Width = 320
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 70
    DataField = 'VI_NUMERO'
    DataSource = DataSource
  end
  object AN_NUMERO: TZetaDBKeyLookup_DevEx [19]
    Left = 85
    Top = 136
    Width = 320
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 4
    TabStop = True
    WidthLlave = 70
    DataField = 'AN_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 12583312
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF98A1E2FFF6F7FDFFB7BEEBFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFB1B8
          E9FFF6F7FDFFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF8792DEFFDFE2F6FFA1A9E5FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8
          A1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF8CE2B8FFE9F9
          F1FF54D396FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF52D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF52D395FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF5AD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF62D79FFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4FD293FF54D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF8AE1B7FFE6F9F0FF6AD9A4FF4FD293FF4FD293FF4FD2
          93FF5FD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF52D395FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF65D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF68D8A2FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF6AD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF52D395FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF70DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF57D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF81DFB1FF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF5261CFFF6C79D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6F7CD7FF7582D9FF6471D4FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFF969FE2FF6471D4FF969FE2FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4757CCFF4757CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4757CCFF4757CCFF6F7CD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF6976D6FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF5564D0FF7582D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 352
    Top = 56
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670408
  end
end
