unit DVisitantes;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, stdctrls,
  Db, DBClient, ZetaClientDataSet, ZetaCommonClasses, ZetaTipoEntidad, ZetaCommonLists,
{$ifdef DOS_CAPAS}
  DServerVisitantes;
{$else}
  Visitantes_TLB;
{$endif}

type
  TdmVisitantes = class(TDataModule)
    cdsTipoID: TZetaLookupDataSet;
    cdsTipoCarro: TZetaLookupDataSet;
    cdsTipoAsunto: TZetaLookupDataSet;
    cdsTipoVisita: TZetaLookupDataSet;
    cdsDepto: TZetaLookupDataSet;
    cdsCondiciones: TZetaLookupDataSet;
    cdsAnfitrion: TZetaLookupDataSet;
    cdsVisitante: TZetaLookupDataSet;
    cdsCaseta: TZetaLookupDataSet;
    cdsEmpVisitante: TZetaLookupDataSet;
    cdsCitas: TZetaClientDataSet;
    cdsAnfitrionLookup: TZetaLookupDataSet;
    cdsVisitanteLookup: TZetaLookupDataSet;
    cdsCortes: TZetaClientDataSet;
    cdsLibros: TZetaClientDataSet;
    cdsCorteDetail: TZetaClientDataSet;
    cdsVisitaCitas: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsCatalogoAfterDelete(DataSet: TDataSet);
    procedure cdsCatalogoAlAdquirirDatos(Sender: TObject);
    procedure cdsCatalogoAlEnviarDatos(Sender: TObject);
    procedure cdsCatalogoAlModificar(Sender: TObject);
    procedure cdsCatalogoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    {$ifdef VER130}
    procedure cdsCatalogosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsCatalogosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsCondicionesAlAgregar(Sender: TObject);
    procedure cdsCondicionesAlBorrar(Sender: TObject);
    procedure cdsAnfitrionAlCrearCampos(Sender: TObject);
    procedure cdsVisitanteAlCrearCampos(Sender: TObject);
    procedure cdsAnfitrionNewRecord(DataSet: TDataSet);
    procedure cdsAnfitrionAfterPost(DataSet: TDataSet);
    procedure cdsAnfitrionBeforePost(DataSet: TDataSet);
    procedure cdsVisitanteNewRecord(DataSet: TDataSet);
    procedure cdsVisitanteAfterPost(DataSet: TDataSet);
    procedure cdsVisitanteBeforePost(DataSet: TDataSet);
    procedure cdsCasetaAlCrearCampos(Sender: TObject);
    procedure cdsEmpVisitanteAlCrearCampos(Sender: TObject);
    procedure cdsEmpVisitanteNewRecord(DataSet: TDataSet);
    procedure cdsEmpVisitanteAfterPost(DataSet: TDataSet);
    procedure cdsEmpVisitanteBeforePost(DataSet: TDataSet);
    {$ifdef VER130}
    procedure cdsTablasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsTablasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsCitasAlAdquirirDatos(Sender: TObject);
    procedure cdsCitasNewRecord(DataSet: TDataSet);
    procedure cdsCitasAlCrearCampos(Sender: TObject);
    procedure cdsAnfitrionLookupDescription(Sender: TZetaLookupDataSet;
      var sDescription: String);
    procedure cdsAnfitrionLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsAnfitrionLookupAlCrearCampos(Sender: TObject);
    procedure cdsAnfitrionAlEnviarDatos(Sender: TObject);
    procedure cdsVisitanteAlEnviarDatos(Sender: TObject);
    procedure cdsVisitanteLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsVisitanteLookupDescription(Sender: TZetaLookupDataSet;
      var sDescription: String);
    procedure cdsVisitanteLookupAlCrearCampos(Sender: TObject);
    procedure cdsCitasAlEnviarDatos(Sender: TObject);
    procedure cdsCortesAlAdquirirDatos(Sender: TObject);
    procedure cdsCortesAlCrearCampos(Sender: TObject);
    procedure cdsCortesBeforePost(DataSet: TDataSet);
    procedure cdsLibrosAlAdquirirDatos(Sender: TObject);
    procedure cdsLibrosAlCrearCampos(Sender: TObject);
    procedure cdsCorteDetailAlAdquirirDatos(Sender: TObject);
    procedure cdsCorteDetailAlCrearCampos(Sender: TObject);
    procedure cdsLibrosBeforePost(DataSet: TDataSet);
    procedure cdsVisitanteLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsCitasBeforePost(DataSet: TDataSet);

  private
    { Private declarations }
    FMaxAnfitrion: Integer;
    FMaxVisitante: Integer;
    FMaxCompanys: Integer;
    FMaxCitas: Integer;
    FFechaInicio: TDate;
    FFechaFin: TDate;
    FFecIniCorte: TDate;
    FFecFinCorte: TDate;
    FFecIniLibro: TDate;
    FFecFinLibro: TDate;
    FCorteStatus: Integer;
    FPuedeModificarLibro: Boolean;
    FEsBorrado: Boolean;
    FFiltroCatVisitante: String;
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerVisitantes;
    property Servidor: TdmServerVisitantes read GetServidor;
{$else}
    FServidor: IdmServerVisitantesDisp;
    function GetServidor: IdmServerVisitantesDisp;
    property Servidor: IdmServerVisitantesDisp read GetServidor;
    property EsBorrado: Boolean read FEsBorrado write FEsBorrado;
{$endif}
    procedure ShowEdicion( const iCatalogo: Integer );
    function GetIndexRights( const iTabla: Integer ): Integer;
    procedure ObtenerFoliosMaximos( oDataSet: TZetaClientDataSet );
    procedure ObtieneNombreAnfi(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure ObtieneNombreVisit(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure ObtieneAbiertaCaseta(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure QuitaFechasNulas(Sender: TField; var Text: String;DisplayText: Boolean );
  public
    { Public declarations }
    property CitaFechaIni: TDate read FFechaInicio write FFechaInicio;
    property CitaFechaFin: TDate read FFechaFin write FFechaFin;
    property CorteFechaIni: TDate read FFecIniCorte write FFecIniCorte;
    property CorteFechaFin: TDate read FFecFinCorte write FFecFinCorte;
    property CorteStatus: Integer read FCorteStatus write FCorteStatus;
    property LibroFechaIni: TDate read FFecIniLibro write FFecIniLibro;
    property LibroFechaFin: TDate read FFecFinLibro write FFecFinLibro;
    property PuedeModificarLibro: Boolean read FPuedeModificarLibro default TRUE;
    property FiltroCatVisitante: String read FFiltroCatVisitante write FFiltroCatVisitante;
    procedure SetFechasInicio;
    procedure AplicaFiltros( sFiltros: String; sDataSet: TZetaClientDataSet );
    procedure HacerBusquedaAnfitrion( oParamList: TZetaParams );
    procedure HacerBusquedaVisitante( oParamList: TZetaParams );
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad;  const Estado: TipoEstado );
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades;  const Estado: TipoEstado );
    {$endif}
    procedure ObtieneFiltros( const sFiltro: String; const sTabla: String );
    procedure BuscaLibro(const dInicio, dFin: TDate; const sFiltro: string);
    procedure BuscaVisitantes(const sFiltro: string);
  end;

var
  dmVisitantes: TdmVisitantes;
  sPista      : string;

implementation

uses DCliente,
     DSistema,
     DReportes,
     DGlobal,

     ZBaseEdicion_DevEx,
     ZAccesosMgr,
     ZReconcile,
     ZGlobalTress,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaTipoEntidadTools,
     FTressShell,
     FTablas_DevEx,
     FEditTablas_DevEx,
     FEditCatCondiciones_DevEx,
     ZetaDialogo,
     FEditCatAnfitrion_DevEx,
     FEditCatVisitantes_DevEx,
     FEditCatCasetas_DevEx,
     FEditCatCompanys_DevEx,
     FEditCitas_DevEx,
     FBuscaAnfitriones,
     FBuscaAnfitriones_DevEx,
     FBuscaVisitantes,
     FBusquedaLibro, 
     FEditCortes_DevEx,
     FEditLibros_DevEx;

const
     K_TIPO_ID = 0;
     K_TIPO_CARRO = 1;
     K_TIPO_ASUNTO = 2;
     K_TIPO_VISITA = 3;
     K_DEPTO = 4;
     K_CASETA = 5;
     K_ANFITRION = 6;
     K_EDIT_CITA = 7;
     K_EMP_VISI = 8;
     K_EDIT_CORTES = 9;
     K_VISITANTE = 10;
     K_LIBRO = 11;
     K_CONDICIONES = 12;
     K_VISITAN_MGR = 13;
{$R *.DFM}

procedure TdmVisitantes.DataModuleCreate(Sender: TObject);
begin
     FMaxAnfitrion := 0;
     FMaxVisitante := 0;
     FMaxCompanys := 0;
     FMaxCitas := 0;
     FEsBorrado := False;
     cdsTipoID.Tag :=  K_TIPO_ID;
     cdsTipoCarro.Tag :=  K_TIPO_CARRO;
     cdsTipoAsunto.Tag :=  K_TIPO_ASUNTO;
     cdsTipoVisita.Tag :=  K_TIPO_VISITA;
     cdsDepto.Tag := K_DEPTO;
     cdsCondiciones.Tag := K_CONDICIONES;
     cdsAnfitrion.Tag := K_ANFITRION;
     cdsVisitante.Tag := K_VISITAN_MGR;
     cdsCaseta.Tag := K_CASETA;
     cdsEmpVisitante.Tag := K_EMP_VISI;
     cdsCitas.Tag := K_EDIT_CITA;
     cdsCortes.Tag := K_EDIT_CORTES;
     cdsLibros.Tag := K_LIBRO;

     //cdsTipoID.OnReconcileError := cdsCatalogoReconcileError;

end;

{$ifdef DOS_CAPAS}
function TdmVisitantes.GetServidor: TdmServerVisitantes;
begin
     Result := DCliente.dmCliente.ServerVisitantes;
end;
{$else}
function TdmVisitantes.GetServidor: IdmServerVisitantesDisp;
begin
     Result := IdmServerVisitantesDisp( dmCliente.CreaServidor( CLASS_dmServerVisitantes, FServidor ) );
end;
{$endif}

function TdmVisitantes.GetIndexRights( const iTabla: Integer ): Integer;
begin
     Case iTabla of
          K_TIPO_ID :     Result := D_TAB_TIPO_ID;
          K_TIPO_CARRO:   Result := D_TAB_TIPO_CARRO;
          K_TIPO_ASUNTO:  Result := D_TAB_TIPO_ASUNTO;
          K_TIPO_VISITA:  Result := D_TAB_TIPO_VISITA;
          K_DEPTO:        Result := D_TAB_DEPTO;
          K_ANFITRION:    Result := D_CAT_ANFITRION;
          K_VISITANTE:    Result := D_CAT_VISITANTE;
          K_CASETA:       Result := D_CAT_CASETA;
          K_EMP_VISI:     Result := D_CAT_COMPANYS;
          K_EDIT_CITA:    Result := D_REG_CITAS;
          K_EDIT_CORTES:  Result := D_REG_CORTE;
          K_LIBRO:        Result := D_REG_LIBRO;
          K_VISITAN_MGR:  Result := D_CAT_VISITANTE;
     else
         Result := 0;
     end;
end;
                                      

procedure TdmVisitantes.ShowEdicion( const iCatalogo: Integer );
begin
     case iCatalogo of
          K_TIPO_ID: ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoID_DevEx, TEditTipoID_DevEx );
          K_TIPO_CARRO: ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoCarro_DevEX, TEditTipoCarro_DevEx );
          K_TIPO_ASUNTO: ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoAsunto_DevEX, TEditTipoAsunto_DevEx );
          K_TIPO_VISITA: ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoVisita_DevEX, TEditTipoVisita_DevEX );
          K_DEPTO: ZBaseEdicion_DevEX.ShowFormaEdicion( EditDepto_DevEx, TEditDepto_DevEx );
          K_CONDICIONES: ZBaseEdicion_DevEX.ShowFormaEdicion( EditCatCondiciones_DevEx, TEditCatCondiciones_DevEx );
          K_ANFITRION: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatAnfitrion_DevEx, TEditCatAnfitrion_DevEx );
          K_VISITANTE: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatVisitantes_DevEx, TEditCatVisitantes_DevEx );      //DACP
          K_CASETA: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatCasetas_DevEx, TEditCatCasetas_DevEx );
          K_EMP_VISI: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatCompanys_DevEx, TEditCatCompanys_DevEx );
          K_EDIT_CITA: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCitas_DevEx, TEditCitas_DevEx );
          K_EDIT_CORTES: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCortes_DevEx, TEditCortes_DevEx );
          K_LIBRO: ZBaseEdicion_DevEx.ShowFormaEdicion( EditLibros_DevEx, TEditLibros_DevEx );
          K_VISITAN_MGR: ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatVisitantes_DevEx, TEditCatVisitantes_DevEx ); //DACP
     end;
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmVisitantes.NotifyDataChange(const Entidades: array of TipoEntidad;  const Estado: TipoEstado );
{$else}
procedure TdmVisitantes.NotifyDataChange(const Entidades: ListaEntidades;  const Estado: TipoEstado );
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enCita , Entidades )then
      {$else}
     if( enCita in Entidades )then
     {$endif}
        cdsCitas.SetDataChange;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enAnfitrio , Entidades )then
     {$else}
     if( enAnfitrio in Entidades )then
     {$endif}
         cdsAnfitrion.SetDataChange;
end;

{$ifdef VER130}
procedure TdmVisitantes.cdsTablasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$else}
procedure TdmVisitantes.cdsTablasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$endif}


procedure TdmVisitantes.cdsCatalogoAfterDelete(DataSet: TDataSet);
begin
     FEsBorrado:= True;
     TZetaClientDataSet( DataSet ).Enviar;
     FEsBorrado:= False;
end;

procedure TdmVisitantes.cdsCatalogoAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := Servidor.GetCatalogo( dmCliente.Empresa, Tag );
     end;
     ObtenerFoliosMaximos( TZetaClientDataSet( Sender ) );
end;

procedure TdmVisitantes.ObtenerFoliosMaximos( oDataSet: TZetaClientDataSet );
begin
     with oDataSet do
     begin
          DisableControls;
          try
             while not Eof do
             begin
                  if( Name = 'cdsAnfitrion' )then
                      FMaxAnfitrion := FieldByName('AN_NUMERO').AsInteger;
                  if( Name = 'cdsVisitante' )then
                      FMaxVisitante := FieldByName('VI_NUMERO').AsInteger;
                  if( Name = 'cdsEmpVisitante' )then
                      FMaxCompanys := FieldByName('EV_NUMERO').AsInteger;
                  if( Name = 'cdsCitas' )then
                      FMaxCitas := FieldByName('CI_FOLIO').AsInteger;
                  Next;
             end;
          finally
                 First;
                 EnableControls;
          end;
     end;
end;

procedure TdmVisitantes.cdsCatalogoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   lActualizaFiltro: Boolean;
begin
     lActualizaFiltro := False;
     with TZetaClientDataSet( Sender ) do
     begin
          if State = dsInsert then
             lActualizaFiltro := TRUE;
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( Servidor.GrabaCatalogo( dmCliente.Empresa, Delta, Tag, ErrorCount ) );
               if (Tag = K_VISITAN_MGR) then
               begin
                    if ( not FEsBorrado) then
                    begin
                          if ( strVacio( FFiltroCatVisitante ) or lActualizaFiltro) then
                          begin
                               FFiltroCatVisitante:= FieldByName('VI_NUMERO').AsString;
                          end;
                    end;
                    BuscaVisitantes(FFiltroCatVisitante);
               end;
          end;
     end;
end;

procedure TdmVisitantes.cdsCatalogoAlModificar(Sender: TObject);
begin
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

procedure TdmVisitantes.cdsCatalogoGetRights(Sender: TZetaClientDataSet; const iRight: Integer;
          var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( GetIndexRights( Sender.Tag ), iRight );
end;

{$ifdef VER130}
procedure TdmVisitantes.cdsCatalogosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmVisitantes.cdsCatalogosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{ ********* cdsCondiciones *********** }
procedure TdmVisitantes.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := Servidor.GetCondiciones( dmCliente.Empresa );
end;

procedure TdmVisitantes.cdsCondicionesAlAgregar(Sender: TObject);
begin
     cdsCondiciones.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditCatCondiciones_DevEx, TEditCatCondiciones_DevEx );
end;

procedure TdmVisitantes.cdsCondicionesAlBorrar(Sender: TObject);
var
   oCursor: TCursor;
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Esta Condici�n ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with cdsCondiciones do
             begin
                  Delete;
                  Enviar;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

{ *********** cdsAnfitrion ************** }
procedure TdmVisitantes.cdsAnfitrionAlCrearCampos(Sender: TObject);
begin
     cdsDepto.Conectar;
     with cdsAnfitrion do
     begin
          ListaFija( 'AN_STATUS', lfAnfitrionStatus );
          CreateSimpleLookup( cdsDepto, 'TB_ELEMENT', 'AN_DEPTO' );
     end;
end;

procedure TdmVisitantes.cdsAnfitrionAlEnviarDatos(Sender: TObject);
var
   iPos : String;
begin
     iPos := cdsAnfitrion.FieldByName('AN_NUMERO').asString;
     cdsCatalogoAlEnviarDatos( Sender );
    // TressShell.SetDataChange( [ enCita ] );
     cdsAnfitrion.Refrescar;
     if ( cdsAnfitrion.recordcount > 0 ) then   // @DACP -Se agrego esta condicion para que no marque error cuando no tiene registros en dataset
          cdsAnfitrion.Locate('AN_NUMERO',iPos,[]);

      {cdsCatalogoAlEnviarDatos( Sender );
     TressShell.SetDataChange( [ enCita ] );
     cdsAnfitrion.Refrescar;  }
end;

procedure TdmVisitantes.cdsAnfitrionNewRecord(DataSet: TDataSet);
begin
     cdsAnfitrion.FieldByName('AN_NUMERO').AsInteger := FMaxAnfitrion + 1;
end;

procedure TdmVisitantes.cdsAnfitrionAfterPost(DataSet: TDataSet);
var
   iMaxGrabado: Integer;
begin
     iMaxGrabado := cdsAnfitrion.FieldByName('AN_NUMERO').AsInteger;
     if( iMaxGrabado > FMaxAnfitrion )then
         FMaxAnfitrion := iMaxGrabado;
end;

procedure TdmVisitantes.cdsAnfitrionBeforePost(DataSet: TDataSet);
begin
     if( cdsAnfitrion.FieldByName('AN_NUMERO').AsInteger <= 0 )then
         DatabaseError(' El N�mero Del Anfitri�n Debe Ser Mayor Que Cero' );
end;

{ *********** cdsVisitante *************** }
procedure TdmVisitantes.cdsVisitanteAlCrearCampos(Sender: TObject);
begin
     cdsTipoVisita.Conectar;
     cdsEmpVisitante.Conectar;
     with cdsVisitante do
     begin
          ListaFija( 'VI_STATUS', lfVisitanteStatus );
          CreateSimpleLookup( cdsTipoVisita, 'TB_ELEMENT', 'VI_TIPO' );
          CreateSimpleLookup( cdsEmpVisitante, 'EV_NOMBRE', 'EV_NUMERO' );
     end;
end;

//acl1
procedure TdmVisitantes.cdsVisitanteAlEnviarDatos(Sender: TObject);
var
   iPos : String;
begin
     iPos := cdsVisitante.FieldByName('VI_NUMERO').asString;
     cdsCatalogoAlEnviarDatos( Sender );
     if ( cdsVisitante.recordcount >0 ) then   // @DACP -Se agrego esta condicion para que no marque error cuando no tiene registros en dataset
        cdsVisitante.Locate('VI_NUMERO',iPos,[]);
end;

procedure TdmVisitantes.cdsVisitanteNewRecord(DataSet: TDataSet);
begin
     with cdsVisitante do
     begin
          FieldByName('VI_NUMERO').AsInteger := FMaxVisitante + 1;
          FieldByName('VI_FEC_NAC').AsDateTime := Date;
          FieldByName('VI_SEXO').AsInteger := Ord(esFemenino);
     end;
end;

procedure TdmVisitantes.cdsVisitanteAfterPost(DataSet: TDataSet);
var
   iMaxGrabado: Integer;
begin
     iMaxGrabado := cdsVisitante.FieldByName('VI_NUMERO').AsInteger;
     if( iMaxGrabado > FMaxVisitante )then
         FMaxVisitante := iMaxGrabado;
end;

procedure TdmVisitantes.cdsVisitanteBeforePost(DataSet: TDataSet);
begin
     if( cdsVisitante.FieldByName('VI_NUMERO').AsInteger <= 0 )then
         DatabaseError(' El N�mero Del Visitante Debe Ser Mayor Que Cero' );
end;

{ ******* cdsCaseta ************}
procedure TdmVisitantes.cdsCasetaAlCrearCampos(Sender: TObject);
begin
     dmReportes.cdsLookupReportes.Conectar;
     with cdsCaseta do
     begin
          ListaFija( 'CA_STATUS', lfCasetaStatus );
          with FieldByName('CA_ABIERTA') do
          begin
               OnGetText := ObtieneAbiertaCaseta;
               Alignment := taRightJustify;
          end;
          CreateSimpleLookup( dmReportes.cdsLookupReportes, 'RE_NOMBRE', 'RE_CODIGO' );
     end;
end;

procedure TdmVisitantes.ObtieneAbiertaCaseta(Sender: TField; var Text: String;DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with cdsCaseta do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    if( Sender.AsString = 'S' )then
                        Text := 'Si'
                    else
                        Text := 'No';
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;

{ ******* cdsEmpVisitante *********}
procedure TdmVisitantes.cdsEmpVisitanteAlCrearCampos(Sender: TObject);
begin
     with cdsEmpVisitante do
     begin
          ListaFija( 'EV_STATUS', lfCatCompanys );
     end;
end;

procedure TdmVisitantes.cdsEmpVisitanteNewRecord(DataSet: TDataSet);
begin
     cdsEmpVisitante.FieldByName('EV_NUMERO').AsInteger := FMaxCompanys + 1;
end;

procedure TdmVisitantes.cdsEmpVisitanteAfterPost(DataSet: TDataSet);
var
   iMaxGrabado: Integer;
begin
     iMaxGrabado := cdsEmpVisitante.FieldByName('EV_NUMERO').AsInteger;
     if( iMaxGrabado > FMaxCompanys )then
         FMaxCompanys := iMaxGrabado;
end;

procedure TdmVisitantes.cdsEmpVisitanteBeforePost(DataSet: TDataSet);
begin
     if( cdsEmpVisitante.FieldByName('EV_NUMERO').AsInteger <= 0 )then
         DatabaseError(' El N�mero De Compa��a Debe Ser Mayor Que Cero' );
end;

{********* cdsCitas **********}
procedure TdmVisitantes.SetFechasInicio;
begin
     FFechaInicio := DiaDelMes( Date, True );
     FFechaFin := Date;
     FFecIniCorte := DiaDelMes( Date, True );
     FFecFinCorte := Date;
     FFecIniLibro := DiaDelMes( Date, True );
     FFecFinLibro := Date;
end;

procedure TdmVisitantes.AplicaFiltros( sFiltros: String; sDataSet: TZetaClientDataSet );
begin
     with sDataSet do
     begin
          Filter := sFiltros;
          Filtered := True;
     end;
end;

procedure TdmVisitantes.cdsCitasAlAdquirirDatos(Sender: TObject);
begin
     ObtieneFiltros( VACIO, 'Cita' );
end;

procedure TdmVisitantes.cdsCitasNewRecord(DataSet: TDataSet);
begin
     with cdsCitas do
     begin
          FieldByName('CI_FOLIO').AsInteger := 0;  
          FieldByName('CI_FECHA').AsDateTime := Date;
          FieldByName('CI_HORA').AsString := FormatDateTime( 'hhnn', Now );
          FieldByName('CI_FEC_MOD').AsDateTime := Date;
          FieldByName('CI_HOR_MOD').AsString := FormatDateTime( 'hhnn', Now );
          FieldByName('CI_STATUS').AsInteger := Ord(csNo);
     end;
end;

procedure TdmVisitantes.cdsCitasAlCrearCampos(Sender: TObject);
begin
     cdsTipoAsunto.Conectar;
     cdsAnfitrion.Conectar;
     cdsVisitanteLookup.Conectar;
     cdsVisitaCitas.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsCitas do
     begin
          MaskTime( 'CI_HORA' );
          ListaFija( 'CI_STATUS', lfCitaStatus );
          CreateSimpleLookup( cdsTipoAsunto, 'TB_ELEMENT', 'CI_ASUNTO' );
          with FieldByName('AN_NUMERO') do
          begin
               OnGetText := ObtieneNombreAnfi;
               Alignment := taLeftJustify;
          end;
          with FieldByName('VI_NUMERO') do
          begin
               OnGetText := ObtieneNombreVisit;
               Alignment := taLeftJustify;
          end;
          with FieldByName('CI_FECHA') do
          begin
               OnGetText := QuitaFechasNulas;
               Alignment := taRightJustify;
          end;
          CreateSimpleLookup(dmSistema.cdsUsuariosLookUp,'US_NOMBRE','US_CODIGO');
     end;
end;

procedure TdmVisitantes.ObtieneNombreAnfi(Sender: TField; var Text: String;DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with cdsCitas do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    Text := cdsAnfitrion.GetDescripcion( Sender.AsString );
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmVisitantes.ObtieneNombreVisit(Sender: TField; var Text: String;DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with cdsCitas do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    Text := cdsVisitaCitas.GetDescripcion( Sender.AsString );
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmVisitantes.cdsCitasAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iCita: Integer;
begin
     ErrorCount := 0;
     with cdsCitas do
     begin
          PostData;
          {if State in [ dsEdit, dsInsert ] then
             Post;}
             
          if ( ChangeCount > 0 ) then
          begin
               iCita := FieldByName('CI_FOLIO').AsInteger ;
               if Reconcile( Servidor.GrabaCitas( dmCliente.Empresa, Delta, ErrorCount, iCita ) ) then
               begin
                    //if ErrorCount = 1 then Edit;
                    if ( ErrorCount = 0 ) and ( FieldByName( 'CI_FOLIO' ).AsInteger <> iCita ) then
                    begin
                         Edit;
                         FieldByName( 'CI_FOLIO' ).AsInteger := iCita;
                         Post;
                         MergeChangeLog;
                    end;
               end;
          end;
    end;
end;

procedure TdmVisitantes.cdsCitasBeforePost(DataSet: TDataSet);
begin
     with cdsCItas do
     begin
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;

{********* cdsAnfitrionLookup  ***********}
procedure TdmVisitantes.HacerBusquedaAnfitrion( oParamList: TZetaParams );
begin
     cdsAnfitrionLookup.Data := Servidor.BuscaAnfitrion( dmCliente.Empresa, oParamList.VarValues );
end;

procedure TdmVisitantes.cdsAnfitrionLookupDescription( Sender: TZetaLookupDataSet; var sDescription: String);
begin
     with cdsAnfitrion do
     begin
          sDescription := Trim( FieldByName( 'AN_APE_PAT' ).AsString ) + ' ' +
                          Trim( FieldByName( 'AN_APE_MAT' ).AsString ) + ', ' +
                          Trim( FieldByName( 'AN_NOMBRES' ).AsString ) ;
     end;
end;

procedure TdmVisitantes.cdsAnfitrionLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
var
   iNumero: Integer;
begin
     iNumero := StrToIntDef( sKey, 0 );
     lOk := FBuscaAnfitriones_DevEx.BuscaAnfitrionDialogo( iNumero, sDescription );

     if lOk then
        sKey := IntToStr( iNumero );
end;

procedure TdmVisitantes.cdsAnfitrionLookupAlCrearCampos(Sender: TObject);
begin
     cdsDepto.Conectar;
     with cdsAnfitrionLookup do
     begin
          ListaFija( 'AN_STATUS', lfAnfitrionStatus );
          CreateSimpleLookup( cdsDepto, 'TB_ELEMENT', 'AN_DEPTO' );
     end;
end;

{********* cdsVisitanteLookup *********}
procedure TdmVisitantes.HacerBusquedaVisitante( oParamList: TZetaParams );
begin
     cdsVisitanteLookup.Data := Servidor.BuscaVisitante( dmCliente.Empresa, oParamList.VarValues );
end;

procedure TdmVisitantes.cdsVisitanteLookupAlCrearCampos(Sender: TObject);
begin
     cdsTipoVisita.Conectar;
     cdsAnfitrion.Conectar;
     cdsEmpVisitante.Conectar;
     cdsVisitaCitas.Conectar;
     with cdsVisitanteLookup do
     begin
          ListaFija( 'VI_STATUS', lfVisitanteStatus );
          CreateSimpleLookup( cdsTipoVisita, 'TB_ELEMENT', 'VI_TIPO' );
          CreateSimpleLookup( cdsEmpVisitante, 'EV_NOMBRE', 'EV_NUMERO' );
     end;
end;

procedure TdmVisitantes.cdsVisitanteLookupDescription( Sender: TZetaLookupDataSet; var sDescription: String);
begin
     cdsVisitaCitas.Conectar;
     with cdsVisitaCitas do
     begin
          sDescription := Trim( FieldByName( 'VI_APE_PAT' ).AsString ) + ' ' +
                          Trim( FieldByName( 'VI_APE_MAT' ).AsString ) + ', ' +
                          Trim( FieldByName( 'VI_NOMBRES' ).AsString ) ;
     end;
end;

procedure TdmVisitantes.cdsVisitanteLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
var
   iNumero: Integer;
begin
     iNumero := StrToIntDef( sKey, 0 );
     lOk := FBusquedaLibro.BuscaVisitanteDialogo( iNumero, sDescription );
     if lOk then
        sKey := IntToStr( iNumero );
end;

{********* cdsCortes *************}
{procedure TdmVisitantes.SetFechasInicioCorte;
begin
     FFecIniCorte := DiaDelMes( Date, True );
     FFecFinCorte := Date;
end;}

procedure TdmVisitantes.cdsCortesAlAdquirirDatos(Sender: TObject);
begin
     ObtieneFiltros( VACIO, 'Corte' );
end;

procedure TdmVisitantes.cdsCortesAlCrearCampos(Sender: TObject);
begin
     cdsCaseta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsCortes do
     begin
          MaskTime( 'CO_INI_HOR' );
          MaskTime( 'CO_FIN_HOR' );
          CreateSimpleLookup( cdsCaseta, 'CA_NOMBRE', 'CA_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_INI_CORTE', 'CO_INI_VIG' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_FIN_CORTE', 'CO_FIN_VIG' );
          with FieldByName('CO_INI_FEC') do
          begin
               OnGetText := QuitaFechasNulas;
               Alignment := taRightJustify;
          end;
          with FieldByName('CO_FIN_FEC') do
          begin
               OnGetText := QuitaFechasNulas;
               Alignment := taRightJustify;
          end;
     end;
end;

procedure TdmVisitantes.cdsCortesBeforePost(DataSet: TDataSet);
begin

end;

{ ********** cdsLibros *********** }
procedure TdmVisitantes.cdsLibrosAlAdquirirDatos(Sender: TObject);
begin
     ObtieneFiltros( VACIO, 'Libro' );
end;

procedure TdmVisitantes.ObtieneFiltros( const sFiltro: String; const sTabla: String );
var
   oCursor: TCursor;
begin
     try
        oCursor := Screen.Cursor;
        Screen.Cursor := crHourglass;
        if( sTabla = 'Libro' )then
             cdsLibros.Data := Servidor.GetLibros( dmCliente.Empresa, FFecIniLibro, FFecFinLibro, sFiltro )
        else if( sTabla = 'Cita' )then
              cdsCitas.Data := Servidor.GetCitas( dmCliente.Empresa, FFechaInicio, FFechaFin, sFiltro, VACIO, VACIO )
        else
             cdsCortes.Data := Servidor.GetCortes( dmCliente.Empresa, FFecIniCorte, FFecFinCorte, sFiltro, CorteStatus );
     finally
         Screen.Cursor := oCursor;
     end;
end;

procedure TdmVisitantes.cdsLibrosAlCrearCampos(Sender: TObject);
begin
     cdsCaseta.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsLibros do
     begin
          MaskTime( 'LI_ENT_HOR' );
          MaskTime( 'LI_SAL_HOR' );
          ListaFija( 'LI_STATUS', lfLibroStatus );
          CreateSimpleLookup( cdsCaseta, 'CA_NOM_ENT', 'LI_ENT_CAS' );
          CreateSimpleLookup( cdsCaseta, 'CA_NOM_SAL', 'LI_SAL_CAS' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_ENT_LIB', 'LI_ENT_VIG' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_SAL_LIB', 'LI_SAL_VIG' );
          with FieldByName('LI_SAL_FEC') do
          begin
               OnGetText := QuitaFechasNulas;
               Alignment := taRightJustify;
          end;
          with FieldByName('LI_ENT_FEC') do
          begin
               OnGetText := QuitaFechasNulas;
               Alignment := taRightJustify;
          end;
     end;
end;

procedure TdmVisitantes.QuitaFechasNulas(Sender: TField; var Text: String;DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if( Sender.AsDateTime = NullDateTime )then
              Text := VACIO
          else
              Text:= Sender.AsString;
     end
     else
         Text:= Sender.AsString;
end;

{ ************ cdsCorteDetail ***************}
procedure TdmVisitantes.cdsCorteDetailAlAdquirirDatos(Sender: TObject);
begin
     cdsCorteDetail.Data := Servidor.GetLibroActivo( dmCliente.Empresa, cdsCortes.FieldByName('CO_FOLIO').AsInteger );
end;

procedure TdmVisitantes.cdsCorteDetailAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsCorteDetail do
     begin
          MaskTime( 'LI_ENT_HOR' );
          MaskTime( 'LI_SAL_HOR' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_ENT_LIB', 'LI_ENT_VIG' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_SAL_LIB', 'LI_SAL_VIG' );
          CreateSimpleLookup( cdsCaseta, 'CA_NOM_ENT', 'LI_ENT_CAS' );
          CreateSimpleLookup( cdsCaseta, 'CA_NOM_SAL', 'LI_SAL_CAS' );
     end;
end;

procedure TdmVisitantes.cdsLibrosBeforePost(DataSet: TDataSet);
const
     K_MIN_GLOBAL_LIBRO = 25;
     K_MAX_GLOBAL_LIBRO = 32;
var
   i: Integer;
   sCampo: String;
          procedure GetDescripcionError( const iGlobal: Integer; const sCampo: String );
          var
             sError: String;
          begin
               sError := VACIO;
               if( Global.GetGlobalBooleano( iGlobal ) and StrVacio( sCampo ) )then
               begin
                   sError := Format( '%s Para Grabar El Registro De Visitas', [ GetDescripcionGlobal( iGlobal ) ] );
                   DatabaseError( sError );
               end;
          end;
begin
     with cdsLibros do
     begin
          for i := K_MIN_GLOBAL_LIBRO to K_MAX_GLOBAL_LIBRO do
          begin
               case i of
                    K_GLOBAL_REQ_VISITANTE: begin
                                                 sCampo := FieldByName('LI_NOMBRE').AsString;
                                               //  sTitulo := 'El Nombre Del Visitante ';
                                            end;
                    K_GLOBAL_REQ_ANFITRION: begin
                                                 sCampo := FieldByName('LI_ANFITR').AsString;
                                                // sTitulo := 'El Nombre Del Anfitri�n ';
                                            end;
                    K_GLOBAL_REQ_COMPANYS: begin
                                                sCampo := FieldByName('LI_EMPRESA').AsString;
                                                //sTitulo := 'El Nombre De Compa��a ';
                                           end;
                    K_GLOBAL_REQ_DEPARTAMENTO: begin
                                                    sCampo := FieldByName('LI_DEPTO').AsString;
                                                  //  sTitulo := 'El Departamento ';
                                               end;
                    K_GLOBAL_REQ_ASUNTO: begin
                                               sCampo := FieldByName('LI_ASUNTO').AsString;
                                               //sTitulo := ' El Tipo De Asunto ';
                                          end;
                    K_GLOBAL_REQ_VEHICULO: begin
                                                sCampo := FieldByName('LI_CAR_TIP').AsString;
                                                //sTitulo := 'El Tipo De Veh�culo ';
                                           end;
                    K_GLOBAL_REQ_IDENTIFICA: begin
                                                  sCampo := FieldByName('LI_TIPO_ID').AsString;
                                                //  sTitulo := 'El Tipo De Identificaci�n';
                                             end;
                    K_GLOBAL_REQ_GAFETE: begin
                                              sCampo := FieldByName('LI_GAFETE').AsString;
                                              //sTitulo := 'El Gafete ';
                                         end;
               end;
               GetDescripcionError( i, sCampo )
          end;
     end;
end;

procedure TdmVisitantes.cdsVisitanteLookupAlAdquirirDatos(Sender: TObject);
var FParamList:TZetaParams;
begin
     FParamList := TZetaParams.Create;
     try
        FParamList.AddString( 'NOMBRE', VACIO );
        FParamList.AddString( 'TIPO', VACIO );
        FParamList.AddString( 'ANFITRION', VACIO );
        FParamList.AddString( 'EMPRESA', VACIO );
        FParamList.AddInteger('STATUS', 0 );

        HacerBusquedaVisitante( FParamList );
        cdsVisitaCitas.Data := cdsVisitanteLookup.Data;
     finally
            FreeAndNil( FParamList );
     end;
end;

procedure TdmVisitantes.BuscaLibro( const dInicio, dFin: TDate;
                                       const sFiltro: string );
begin
     cdsVisitaCitas.Data := Servidor.GetLibros( dmCliente.Empresa, dInicio, dFin, sFiltro );
end;

procedure TdmVisitantes.BuscaVisitantes( const sFiltro: string );
var
   oCursor: TCursor;
   sMaxVisit : WideString;
begin
     try
        oCursor := Screen.Cursor;
        Screen.Cursor := crHourglass;
        cdsVisitante.Data := Servidor.GetCatalogosVisitantes( dmCliente.Empresa, Tag , Format( '%s', [ EntreComillas( '%'+ UpperCase( sFiltro ) +'%' ) ] ),sMaxVisit);
        FMaxVisitante := strToint( sMaxVisit ) ;
        FMaxVisitante := FMaxVisitante-1;
        //ObtenerFoliosMaximos( TZetaClientDataSet( cdsVisitante ) );   OLD
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
