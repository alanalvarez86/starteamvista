unit FCitas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  ZetaKeyLookup, Mask, ZetaFecha, ZetaDBTextBox, Buttons, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx, cxButtons,ZetaDialogo,
  Vcl.DBCtrls;

type
  TCitas_DevEx = class(TBaseGridLectura_DevEx)
    pnlFiltros: TPanel;
    lblFechaIni: TLabel;
    lblFechaFin: TLabel;
    Label4: TLabel;
    lblVisitante: TLabel;
    zfFecIni: TZetaFecha;
    zfFecFin: TZetaFecha;
    btnBuscar: TcxButton;
    VI_NUMERO: TZetaKeyLookup_DevEx;
    AN_NUMERO: TZetaKeyLookup_DevEx;
    CI_FOLIO: TcxGridDBColumn;
    CI_FECHA: TcxGridDBColumn;
    CI_HORA: TcxGridDBColumn;
    VI_NUMERO_Grid: TcxGridDBColumn;
    AN_NUMERO_Grid: TcxGridDBColumn;
    CI_STATUS: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zfFecIniValidDate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
  private
    { Private declarations }
    FFiltro: String;
    procedure PreparaFiltro;
    procedure LimpiaFiltros;
    procedure ConfigAgrupamiento;

  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Citas_DevEx: TCitas_DevEx;

implementation
uses DVisitantes, ZetaCommonClasses, ZetaCommonTools, FBuscaAnfitriones, FBuscaVisitantes, FBusquedaLibro, FBuscaAnfitriones_DevEx;

{$R *.DFM}

procedure TCitas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          SetFechasInicio;
          zfFecIni.Valor := CitaFechaIni;
          zfFecFin.Valor := CitaFechaFin;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          VI_NUMERO.LookupDataset := cdsVisitaCitas;
     end;
     HelpContext := H_VISMGR_CONS_CITAS;
end;

procedure TCitas_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     LimpiaFiltros;
     dmVisitantes.cdsCitas.Refrescar;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CI_FOLIO'), K_SIN_TIPO , '', skCount);
     ConfigAgrupamiento;
     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCitas_DevEx.LimpiaFiltros;
begin
      AN_NUMERO.Llave := VACIO;
      VI_NUMERO.Llave := VACIO;
end;


procedure TCitas_DevEx.Connect;
begin
     with dmVisitantes do
     begin
          cdsTipoAsunto.Conectar;
          cdsAnfitrion.Conectar;
          cdsVisitanteLookup.Conectar;
          cdsVisitaCitas.Conectar;
          cdsCitas.Conectar;
          DataSource.DataSet := cdsCitas;
     end;
     PreparaFiltro;
end;

procedure TCitas_DevEx.Refresh;
begin
     dmVisitantes.cdsCitas.Refrescar;
     DoBestFit;
end;

procedure TCitas_DevEx.Agregar;
begin
     dmVisitantes.cdsCitas.Agregar;
end;

procedure TCitas_DevEx.Borrar;
begin
     dmVisitantes.cdsCitas.Borrar;
     DoBestFit;
end;

procedure TCitas_DevEx.Modificar;
begin
     dmVisitantes.cdsCitas.Modificar;
end;

procedure TCitas_DevEx.zfFecIniValidDate(Sender: TObject);
begin
     inherited;
     with Sender as TZetaFecha do
     begin
          case Tag of
               0: dmVisitantes.CitaFechaIni := Valor;
               1: dmVisitantes.CitaFechaFin := Valor;
          end;
     end;
end;

procedure TCitas_DevEx.PreparaFiltro;
begin
     FFiltro := VACIO;
     if strLleno( AN_NUMERO.Llave )then
         FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( C.AN_NUMERO ) = %s', [ EntreComillas( UpperCase( AN_NUMERO.Llave) ) ]  ) );
     if strLleno( VI_NUMERO.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( C.VI_NUMERO ) = %s', [ EntreComillas( UpperCase( VI_NUMERO.Llave ) ) ]  ) );
end;


procedure TCitas_DevEx.btnBuscarClick(Sender: TObject);
begin
     inherited;
     PreparaFiltro;
     dmVisitantes.ObtieneFiltros( FFiltro, 'Cita' );
     DoBestFit;
end;

procedure TCitas_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar

     CI_FOLIO.Options.Grouping:= FALSE;
     CI_FECHA.Options.Grouping:= TRUE;
     CI_HORA.Options.Grouping:= FALSE;
     VI_NUMERO_grid.Options.Grouping:= FALSE;
     AN_NUMERO_grid.Options.Grouping:= FALSE;
     CI_STATUS.Options.Grouping:= TRUE;
     TB_ELEMENT.Options.Grouping:= FALSE;
end;

end.
