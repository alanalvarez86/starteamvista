inherited EvaluacionMultiple_DevEx: TEvaluacionMultiple_DevEx
  Left = 553
  Top = 291
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Evaluaci'#243'n m'#250'ltiple'
  ClientHeight = 273
  ClientWidth = 779
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 237
    Width = 779
    inherited OK_DevEx: TcxButton
      Left = 608
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 688
      Cancel = True
      OnClick = Cancelar_DevExClick
    end
  end
  object Panel2: TPanel [1]
    Left = 0
    Top = 31
    Width = 779
    Height = 206
    Align = alClient
    TabOrder = 1
    object GridRenglones: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 777
      Height = 204
      Align = alClient
      DataSource = dsEvaluacionMult
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = GridRenglonesCellClick
      OnColExit = GridRenglonesColExit
      OnDrawColumnCell = GridRenglonesDrawColumnCell
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'PRETTYNAME'
          ReadOnly = True
          Title.Caption = 'Empleado'
          Width = 207
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'CC_ELEMENT'
          ReadOnly = True
          Title.Caption = 'Competencia'
          Width = 195
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NC_DESCRIP'
          ReadOnly = True
          Title.Caption = 'Nivel'
          Width = 154
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EC_COMENT'
          Title.Caption = 'Observaciones'
          Width = 194
          Visible = True
        end>
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object dsEvaluacionMult: TDataSource
    Left = 312
    Top = 8
  end
  object cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 6291544
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000002F3E40006D
          8F9300202B2C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000003E515400BD
          F8FF00B0E8EF002C3A3C000000000000000000000000000000000070939700A4
          D9DF00A4D9DF00A4D9DF00A4D9DF00A4D9DF00A4D9DF00A4D9DF006D8F9300A1
          D5DB00A4D9DF00B9F3FB0026323400000000000000000000000000BDF8FF00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00A7DDE3006D
          8F9300BDF8FF00BDF8FF00A4D9DF000C10100000000000000000007CA2A700BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF0076
          9B9F00A4D9DF00BDF8FF00BDF8FF007CA2A70000000000000000002C3A3C0047
          5D6000475D6000475D6000475D6000475D6000475D6000475D6000475D600047
          5D60004D656800B9F3FB00BDF8FF00BDF8FF004D65680000000000ADE5EB00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00B0E8EF0066888B00BDF8FF00BDF8FF00B3EBF300202B2C00AAE1E700BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF007CA2A70085ADB300BDF8FF00BDF8FF0099C9CF00232F300047
          5D6000475D6000475D6000475D6000475D6000475D6000475D6000475D600047
          5D6000475D60001B2324000F131400A7DDE300B9F3FB00567074008BB6BB00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF007CA2A70000000000263234002936380000000000B9F3FB00BD
          F8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BDF8FF00BD
          F8FF00BDF8FF00A4D9DF00000000000000000000000000000000005F7C80008E
          BABF008EBABF008EBABF008EBABF008EBABF008EBABF008EBABF008EBABF008E
          BABF008EBABF00536D7000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
  object DevEx_BarManagerEdicion: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = cxImageList16Edicion
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    UseSystemFont = True
    OnShowToolbarsPopup = DevEx_BarManagerEdicionShowToolbarsPopup
    Left = 120
    Top = 96
    DockControlHeights = (
      0
      0
      31
      0)
    object BarSuperior: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = 'BarSuperior'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 397
      FloatTop = 233
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton_CambiarNivel'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarButton_CambiarNivel: TdxBarButton
      Align = iaRight
      Caption = 'Cambiar Nivel'
      Category = 0
      Enabled = False
      Hint = 
        'Para activar esta Opci'#243'n de clic la celda correspondiente a la c' +
        'olumna Nivel'
      Visible = ivAlways
      ImageIndex = 0
      PaintStyle = psCaptionGlyph
      OnClick = dxBarButton_CambiarNivelClick
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Seleccionar el nivel de evaluaci'#243'n de cada competencia'
      Category = 0
      Hint = 'Seleccionar el nivel de evaluaci'#243'n de cada competencia'
      Visible = ivAlways
      PropertiesClassName = 'TcxLabelProperties'
    end
  end
end
