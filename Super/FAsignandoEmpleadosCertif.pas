unit FAsignandoEmpleadosCertif;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal_DevEx, DB, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  Buttons, ExtCtrls,ZetaDialogo,FMapasProduccion, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  Menus, ImgList, cxButtons;

type
  TAsignandoEmpleados = class(TZetaDlgModal_DevEx)
    DataSource1: TDataSource;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    {procedure GridEmpleadosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState); }
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure CB_CODIGOCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure OK_DevExClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    AColumn: TcxGridDBColumn;
    procedure ApplyMinWidth;

  public
    { Public declarations }
    FMaquina:string;
    FSilla:TSilla;
    EmpleadoSelecc:Integer;
    function  FiltrarEmpleados(oSilla:TSilla):Boolean;
  end;

var
  AsignandoEmpleados: TAsignandoEmpleados;

implementation

uses DLabor,ZetaCommonClasses,ZetaCommonTools,ZetaCommonLists, DSuper,FLayoutProduccion,
     ZetaClientDataSet;

{$R *.dfm}

procedure TAsignandoEmpleados.OKClick(Sender: TObject);
var
   posx,posy:Integer;
   EmpleadoAnt:Integer;
   Lista:TStrings;
   sMaquinaAnterior :string;
   EmpleadoSelecc:Integer;

procedure RevisarAsignacion;
begin
     if ( FSilla.Estado in [ Ausente,NoCertificado,Certificado] )then
     begin
          if ZConfirm(Caption,Format('Desea asignar al empleado: %d en el lugar de %d',[EmpleadoSelecc,FSilla.Empleado]),0,mbYes )then
          begin
               EmpleadoAnt:= FSilla.Empleado;
               dmLabor.AsignarEmpleado(EmpleadoSelecc,FSilla.Codigo);
               FSilla.Empleado := EmpleadoSelecc;
               LayoutProduccion.LimpiarAsignacionEmpleadoAnterior(EmpleadoSelecc, TSilla(GridEstados[posx][posy]));
          end;
     end
     else
     begin
          if EmpleadoYaOcupado(EmpleadoSelecc) then
          begin
               LayoutProduccion.LimpiarAsignacionEmpleadoAnterior(EmpleadoSelecc, TSilla(GridEstados[posx][posy]));
               //dmLabor.AsignarEmpleado(EmpleadoSelecc,VACIO);
          end;
          dmLabor.AsignarEmpleado(EmpleadoSelecc,FSilla.Codigo);
     end;
end;

begin
     //inherited;
     EmpleadoSelecc := dmSuper.cdsListaEmpleados.FieldByName('CB_CODIGO').AsInteger;
     posx:= 0;
     posy:= 0;
     if not EmpleadoYaOcupado(EmpleadoSelecc)then
     begin
          RevisarAsignacion
     end
     else
     begin
          Lista := TStringList.Create;
          try
             with dmLabor.cdsKarEmpMaq do
             begin
                  if Locate('CB_CODIGO',EmpleadoSelecc,[])THEN
                  begin
                       Lista.CommaText := FieldByName('SILLA').AsString;
                       posx := StrToInt(Lista[0]);
                       posy := StrToInt(Lista[1]);
                       if TSilla(GridEstados[posx][posy]).Maquinas.Count > 0 then
                          sMaquinaAnterior := TMaquina(TSilla(GridEstados[posx][posy]).Maquinas[0]).Codigo;
                  end
             end;
          finally
                 Lista.Free;
          end;

          if (posx = FSilla.PosX  ) and (posy = FSilla.PosY  ) then
          begin
               ZWarning(sELF.Caption,Format('El Empleado : %d ya a est� asignado a %s : %s',[EmpleadoSelecc,dmLabor.GetMaquinaGlobal,sMaquinaAnterior ]),0,mbOk )
          end
          else
          begin
               if ZConfirm(Self.Caption,Format('El empleado: %d est� asignado a %s : %s '+' � Desea asignarlo a %s : %s ?',[EmpleadoSelecc,dmLabor.GetMaquinaGlobal,sMaquinaAnterior,dmLabor.GetMaquinaGlobal,TMaquina(FSilla.Maquinas[0]).Codigo]),0,mbYes ) then
               begin
                    RevisarAsignacion;
               end;
          end;
     end;
     //AsignarEmpleado
     Close;
end;

procedure TAsignandoEmpleados.FormCreate(Sender: TObject);
begin
     inherited;
     DataSource1.DataSet := dmSuper.cdsListaEmpleados;
     EmpleadoSelecc := 0;
     FSilla := TSilla.Create;

     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
end;

function TAsignandoEmpleados.FiltrarEmpleados(oSilla:TSilla):Boolean;
var
   sFiltro:string;
   i:Integer;
begin
     FSilla := oSilla;
     //Caption := Format(Caption,[Maquina]);
     Result := False;
     with dmSuper do
     begin
          with cdsListaEmpleados do
          begin
               Filtered := False;
               First;
               while not eof do
               begin
                    for i := 0 to FSilla.Maquinas.Count -1  do
                    begin
                         Result := dmLabor.EstaCertificado(FieldByName('CB_CODIGO').AsInteger,TMaquina(FSilla.Maquinas[i]).Certificaciones);
                    end;
                    if Result then
                    begin
                         sFiltro := ConcatString(sFiltro,IntToStr(FieldByName('CB_CODIGO').AsInteger),',');
                    end;
                    Next;
               end;
               if strLleno(sFiltro )then
               begin
                    cdsListaEmpleados.Filtered := False;
                    cdsListaEmpleados.Filter := 'CB_CODIGO in ('+sfiltro+')';
                    cdsListaEmpleados.Filtered := True;
                    Result := True;
               end
               else
                   Result:= False;
          end;
     end;
end;

procedure TAsignandoEmpleados.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmSuper.cdsListaEmpleados.Filtered := False;
end;

{procedure TAsignandoEmpleados.GridEmpleadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
      if ( Column.FieldName = 'CB_CODIGO' ) and ( EmpleadoYaOcupado(Column.Field.AsInteger) ) then
      begin
           GridEmpleados.Canvas.Font.Style := [fsBold];
           GridEmpleados.Canvas.Font.Color := clRed;

           GridEmpleados.Canvas.FillRect(Rect);
           GridEmpleados.Canvas.TextOut(Rect.Right - Length(Column.Field.AsString)*8 ,Rect.Top+2,Column.Field.AsString);
      end
      else
      begin
          GridEmpleados.Canvas.Font.Color := clBlack;
          GridEmpleados.Canvas.Font.Style := [];
      end;
end; }

procedure TAsignandoEmpleados.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;
  //Desactiva la posibilidad de agrupar y filtrar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
  //Para que nunca muestre el filterbox inferior
  ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
  //Aplica el ancho ideal a las columnas
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TAsignandoEmpleados.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  OK_DevEx.Click;
end;

procedure TAsignandoEmpleados.CB_CODIGOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;

  if  EmpleadoYaOcupado(CB_CODIGO.DataBinding.Field.AsInteger)  then
           ACanvas.Font.Color := clRed
      else
           ACanvas.Font.Color := clBlack;
end;

procedure TAsignandoEmpleados.OK_DevExClick(Sender: TObject);
var
   posx,posy:Integer;
   EmpleadoAnt:Integer;
   Lista:TStrings;
   sMaquinaAnterior :string;
   EmpleadoSelecc:Integer;

procedure RevisarAsignacion;
begin
     if ( FSilla.Estado in [ Ausente,NoCertificado,Certificado] )then
     begin
          if ZConfirm(Caption,Format('Desea asignar al empleado: %d en el lugar de %d',[EmpleadoSelecc,FSilla.Empleado]),0,mbYes )then
          begin
               EmpleadoAnt:= FSilla.Empleado;
               dmLabor.AsignarEmpleado(EmpleadoSelecc,FSilla.Codigo);
               FSilla.Empleado := EmpleadoSelecc;
               LayoutProduccion.LimpiarAsignacionEmpleadoAnterior(EmpleadoSelecc, TSilla(GridEstados[posx][posy]));
          end;
     end
     else
     begin
          if EmpleadoYaOcupado(EmpleadoSelecc) then
          begin
               LayoutProduccion.LimpiarAsignacionEmpleadoAnterior(EmpleadoSelecc, TSilla(GridEstados[posx][posy]));
               //dmLabor.AsignarEmpleado(EmpleadoSelecc,VACIO);
          end;
          dmLabor.AsignarEmpleado(EmpleadoSelecc,FSilla.Codigo);
     end;
end;

begin
     //inherited;
     EmpleadoSelecc := dmSuper.cdsListaEmpleados.FieldByName('CB_CODIGO').AsInteger;
     posx:= 0;
     posy:= 0;
     if not EmpleadoYaOcupado(EmpleadoSelecc)then
     begin
          RevisarAsignacion
     end
     else
     begin
          Lista := TStringList.Create;
          try
             with dmLabor.cdsKarEmpMaq do
             begin
                  if Locate('CB_CODIGO',EmpleadoSelecc,[])THEN
                  begin
                       Lista.CommaText := FieldByName('SILLA').AsString;
                       posx := StrToInt(Lista[0]);
                       posy := StrToInt(Lista[1]);
                       if TSilla(GridEstados[posx][posy]).Maquinas.Count > 0 then
                          sMaquinaAnterior := TMaquina(TSilla(GridEstados[posx][posy]).Maquinas[0]).Codigo;
                  end
             end;
          finally
                 Lista.Free;
          end;

          if (posx = FSilla.PosX  ) and (posy = FSilla.PosY  ) then
          begin
               ZWarning(sELF.Caption,Format('El Empleado : %d ya a est� asignado a %s : %s',[EmpleadoSelecc,dmLabor.GetMaquinaGlobal,sMaquinaAnterior ]),0,mbOk )
          end
          else
          begin
               if ZConfirm(Self.Caption,Format('El empleado: %d est� asignado a %s : %s '+' � Desea asignarlo a %s : %s ?',[EmpleadoSelecc,dmLabor.GetMaquinaGlobal,sMaquinaAnterior,dmLabor.GetMaquinaGlobal,TMaquina(FSilla.Maquinas[0]).Codigo]),0,mbYes ) then
               begin
                    RevisarAsignacion;
               end;
          end;
     end;
     //AsignarEmpleado
     Close;
end;


procedure TAsignandoEmpleados.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     inherited;
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TAsignandoEmpleados.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
     inherited;
     ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource1.DataSet) );
end;


procedure TAsignandoEmpleados.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions = 30;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;


end.
