unit FGridAutorizaPrenomina;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, StdCtrls, ZetaKeyCombo, Db, Grids, DBGrids, ZetaDBGrid,
  DBCtrls, ZetaSmartLists, Buttons, ExtCtrls, ImgList,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, cxNavigator, cxDBNavigator, cxButtons;

type
  TGridAutorizaPrenomina = class(TBaseGridEdicion_DevEx)
    PanelFiltro: TPanel;
    lblAutorizacion: TLabel;
    cbAutorizacion: TZetaKeyCombo;
    ImageGrid: TImageList;
    lblNomina: TLabel;
    dxBarButton_bbAutorizaTodos: TdxBarButton;
    dxBarButton_bbCancelaTodos: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbAutorizacionChange(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridCellClick(Column: TColumn);
    procedure bbAutorizaTodosClick(Sender: TObject);
    procedure bbCancelaTodosClick(Sender: TObject);
    procedure dxBarButton_bbAutorizaTodosClick(Sender: TObject);
    procedure dxBarButton_bbCancelaTodosClick(Sender: TObject);
//    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure LlenaCombo;
//    procedure HabilitaBotones( const iFiltro: integer );
    procedure AutorizaCancela( const lAutoriza: boolean );
    procedure AplicaFiltro(const sFiltro: string);
    procedure CambiaAutorizacion( const iSupervisor: Integer );
    function TieneDerecho(var sMessage: string): Boolean;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  GridAutorizaPrenomina: TGridAutorizaPrenomina;

implementation

{$R *.DFM}

uses
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists,
    DSistema,
    DCliente,
    DSuper,
    ZAccesosMgr,
    ZAccesosTress;

const
     K_FILTRO_TODOS = 0;
     K_FILTRO_AUTORIZADOS = 1;
     K_FILTRO_SIN_AUTORIZAR = 2;
     aAutorizaciones: array[ K_FILTRO_TODOS..K_FILTRO_SIN_AUTORIZAR ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( VACIO, '( ULT_SUPER > 0 )', '( ULT_SUPER = 0 )' );

{ TGridAutorizaPrenomina }
procedure TGridAutorizaPrenomina.FormCreate(Sender: TObject);
begin
     inherited;
     LlenaCombo;
     PrimerColumna := 3;
     HelpContext := H_Grid_Autorizar_Prenomina;
     //bbAutorizaTodos.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_AUTORIZAR_PRENOMINA, K_DERECHO_CONSULTA );
     //bbCancelaTodos.Enabled :=ZAccesosMgr.CheckDerecho( D_SUPER_AUTORIZAR_PRENOMINA, K_DERECHO_ALTA )
end;

procedure TGridAutorizaPrenomina.FormShow(Sender: TObject);
const
     K_MENSAJE_NOMINA = 'N�mina %s #%d del %s al %s';
begin
     inherited;
     cbAutorizacion.ItemIndex := K_FILTRO_SIN_AUTORIZAR;
     AplicaFiltro( aAutorizaciones[ cbAutorizacion.ItemIndex ] );
     self.ActiveControl := cbAutorizacion;
//     HabilitaBotones( cbAutorizacion.ItemIndex );
     with dmCliente.GetDatosPeriodoActivo do
          lblNomina.Caption := Format( K_MENSAJE_NOMINA, [ ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) ),
                                                           Numero,
                                                           FechaCorta( Inicio ),
                                                           FechaCorta( Fin ) ] );
end;

procedure TGridAutorizaPrenomina.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmSuper do
     begin
          cdsAutorizarPrenomina.Refrescar;
          DataSource.DataSet := cdsAutorizarPrenomina;
     end;
end;

procedure TGridAutorizaPrenomina.cbAutorizacionChange(Sender: TObject);
var
   sFiltro: string;
   iOpcion: integer;
begin
     inherited;
     iOpcion := cbAutorizacion.ItemIndex;
     sFiltro := aAutorizaciones[ iOpcion ] ;
     AplicaFiltro( sFiltro );
//     HabilitaBotones( iOpcion );
end;

procedure TGridAutorizaPrenomina.AplicaFiltro( const sFiltro: string );
begin
     with dmSuper.cdsAutorizarPrenomina do
     begin
          Filtered := strLleno( sFiltro );
          if Filtered then
             Filter := sFiltro
          else
              Filter := VACIO;
     end;
end;

procedure TGridAutorizaPrenomina.LlenaCombo;
begin
     with cbAutorizacion.Lista do
     begin
          BeginUpdate;
          try
             Add( '0=Todos' );
             Add( '1=Autorizados' );
             Add( '2=Sin Autorizar' );
          finally
                 EndUpdate;
          end;
     end;
end;

{
procedure TGridAutorizaPrenomina.HabilitaBotones( const iFiltro: integer );
var
   lActiva: boolean;
begin
     lActiva := not dmSuper.cdsAutorizarPrenomina.IsEmpty;
     if ( ifiltro = K_FILTRO_TODOS ) then    //Filtro Todos, se habilitan ambos botones
     begin
          bbAutorizaTodos.Enabled := True and lActiva;
          bbCancelaTodos.Enabled := True and lActiva;
     end
     else
     begin
          bbAutorizaTodos.Enabled := ( iFiltro = K_FILTRO_SIN_AUTORIZAR ) and lActiva;        //Filtro Autorizados se habilita un boton y el otro es el negativo del primero
          bbCancelaTodos.Enabled := not bbAutorizaTodos.Enabled and lActiva;
     end;
end;
}

procedure TGridAutorizaPrenomina.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
Var
   iImagen: integer;

   function GetPosicion: Integer;
   begin
        Result := Rect.Right - Rect.Left;
        if ( Result > ImageGrid.Width ) then
           Result := Rect.Left + Round( ( Result - ImageGrid.Width ) / 2 ) + 1
        else
            Result := Rect.Left;
   end;

begin
     if ( not DataSource.DataSet.IsEmpty ) and ( Column.Visible ) and ( Column.FieldName = 'AUTORIZADO' ) then
     begin
{
          if ( DataSource.DataSet.FieldByName( 'NO_SUP_OK' ).AsInteger = 0 ) then
             iImagen := 0
          else
              iImagen := 1;
}
          iImagen := iMin( DataSource.DataSet.FieldByName( 'NO_SUP_OK' ).AsInteger, 1 );
          //ImageGrid.Draw( ZetaDBGrid.Canvas, GetPosicion, Rect.top + 1, iImagen );
          ImageGrid.Draw( ZetaDBGrid.Canvas, GetPosicion, Rect.top, iImagen );
     end
     else
         inherited;
         //ZetaDBGrid.DefaultDrawColumnCell( Rect, DataCol, Column, State );

end;

procedure TGridAutorizaPrenomina.ZetaDBGridCellClick(Column: TColumn);
var
   sMessage:string;

begin
     inherited;
     with Column, DataSource.DataSet do
     begin
          if ( not IsEmpty ) and ( FieldName = 'AUTORIZADO' ) then
          begin
               if TieneDerecho(sMessage) then
               begin
                    if ( FieldByName( 'NO_SUP_OK' ).Asinteger > 0 ) then
                    begin
                         CambiaAutorizacion( 0 )
                    end
                    else
                        CambiaAutorizacion( dmCliente.Usuario );
                    self.SeleccionaPrimerColumna;
               end
               else
                   ZetaDialogo.ZWarning('Supervisores',sMessage,0,mbOk);

          end;
     end;
end;

procedure TGridAutorizaPrenomina.CambiaAutorizacion( const iSupervisor: Integer );
begin
     with dmSuper.cdsAutorizarPrenomina do
     begin
          if ( State <> dsEdit ) then
              Edit;
          FieldByName( 'NO_SUP_OK' ).AsInteger := iSupervisor;
          if iSupervisor = 0 then
          begin
               FieldByName( 'NO_FEC_OK' ).AsDateTime := NullDateTime;
               FieldByName( 'NO_HOR_OK' ).AsString := VACIO;
          end;
          Post;
     end;
end;

procedure TGridAutorizaPrenomina.AutorizaCancela(const lAutoriza: boolean);
var
   sMessage:string;
begin
     with dmSuper.cdsAutorizarPrenomina do
     begin
          if not IsEmpty then
          begin
               DisableControls;
               try
                  First;
                  while not EOF do
                  begin
                       if TieneDerecho(sMessage)then
                       begin
                            if ( lAutoriza ) then
                            begin
                                 if ( FieldByName( 'NO_SUP_OK' ).AsInteger = 0 ) then
                                 begin
                                      CambiaAutorizacion( dmCliente.Usuario );
                                      HuboCambios := TRUE;
                                 end;
                            end
                            else
                            begin
                                 if ( FieldByName( 'NO_SUP_OK' ).AsInteger <> 0 ) then
                                 begin
                                      CambiaAutorizacion( 0 );
                                      HuboCambios := TRUE;
                                 end;
                            end;
                       end;
                       Next;
                  end;
               finally
                      EnableControls;
               end;
          end
          else
              ZetaDialogo.ZError( Caption, '�No existen registros de pren�mina por autorizar/cancelar!', 0 );
     end;
end;

procedure TGridAutorizaPrenomina.bbAutorizaTodosClick(Sender: TObject);
begin
     inherited;
     AutorizaCancela( True );
end;

procedure TGridAutorizaPrenomina.bbCancelaTodosClick(Sender: TObject);
begin
     inherited;
     AutorizaCancela( False );
end;

procedure TGridAutorizaPrenomina.Agregar;
begin
     { No hace Nada }
end;

procedure TGridAutorizaPrenomina.Borrar;
begin
     { No hace Nada }
end;

{
procedure TGridAutorizaPrenomina.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'NO_SUP_OK' ) then
     begin
          HabilitaBotones( cbAutorizacion.ItemIndex );
     end;
end;
}

procedure TGridAutorizaPrenomina.HabilitaControles;
begin
     inherited;
     cbAutorizacion.Enabled := ( not Editing );
     lblAutorizacion.Enabled := cbAutorizacion.Enabled;
     Application.ProcessMessages;
end;

function TGridAutorizaPrenomina.TieneDerecho(var sMessage:string):Boolean;
begin
     with DataSource.DataSet do
     begin
          if( FieldByName( 'ULT_SUPER' ).Asinteger > 0 )then
          begin
          		 Result := (ZAccesosMgr.CheckDerecho( D_SUPER_AUTORIZAR_PRENOMINA, K_DERECHO_ALTA ));
          		 if Not Result then
                  sMessage := 'No tiene Derecho para Desautorizar Pren�mina';
          end
          else
          begin
               Result := (ZAccesosMgr.CheckDerecho( D_SUPER_AUTORIZAR_PRENOMINA , K_DERECHO_CONSULTA ));
               if Not Result then
                  sMessage := 'No tiene Derecho para Autorizar Pren�mina';
          end;
     end;
end;


procedure TGridAutorizaPrenomina.dxBarButton_bbAutorizaTodosClick(
  Sender: TObject);
begin
  inherited;
  AutorizaCancela( True );
end;

procedure TGridAutorizaPrenomina.dxBarButton_bbCancelaTodosClick(
  Sender: TObject);
begin
  inherited;
  AutorizaCancela( False );
end;

end.
