unit DLabor;

interface

{$DEFINE MULTIPLES_ENTIDADES}
                                                                                                                                                                                                                    
uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, Mask,
     ZetaClientDataSet,
     {$ifdef DOS_CAPAS}
     DServerLabor,
     {$else}
     Labor_TLB,
     {$endif}
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZBaseEdicion_DevEx,
     ZBaseGridEdicion_DevEx,
     ZetaClientTools,
     FMapasProduccion,
     Variants;

type
  eTipoWOrder = ( ewUnaOrden, ewHistorial, ewKardex, ewLookup );
  TAreaData = record
    HayPrimeraHora: Boolean;
    PrimeraHora: String;
    Shift: eTurnoLabor;
  end;
  TdmLabor = class(TDataModule)
    cdsWorks: TZetaClientDataSet;
    cdsOpera: TZetaLookupDataSet;
    cdsModula1: TZetaLookupDataSet;
    cdsModula2: TZetaLookupDataSet;
    cdsModula3: TZetaLookupDataSet;
    cdsTiempoMuerto: TZetaLookupDataSet;
    cdsWOrderLookup: TZetaLookupDataSet;
    cdsGridLabor: TZetaClientDataSet;
    cdsGridLaborCB_CODIGO: TIntegerField;
    cdsGridLaborPRETTYNAME: TStringField;
    cdsArea: TZetaLookupDataSet;
    cdsPartes: TZetaLookupDataSet;
    cdsGridLaborCB_PUESTO: TStringField;
    cdsMultiLote: TZetaClientDataSet;
    cdsMultiLoteWO_NUMBER: TStringField;
    cdsMultiLoteWO_DESCRIP: TStringField;
    cdsAusencia: TZetaClientDataSet;
    cdsCedulas: TZetaClientDataSet;
    cdsCedEmp: TZetaClientDataSet;
    cdsCedMulti: TZetaClientDataSet;
    cdsCedEmpCE_FOLIO: TIntegerField;
    cdsCedEmpCB_CODIGO: TIntegerField;
    cdsCedEmpCE_POSICIO: TSmallintField;
    cdsCedEmpCB_PUESTO: TStringField;
    cdsCedEmpPRETTYNAME: TStringField;
    cdsCedMultiCE_FOLIO: TIntegerField;
    cdsCedMultiWO_NUMBER: TStringField;
    cdsCedMultiCW_POSICIO: TSmallintField;
    cdsCedMultiWO_DESCRIP: TStringField;
    cdsCedMultiAR_CODIGO: TStringField;
    cdsMultiLoteAR_CODIGO: TStringField;
    cdsCedMultiCW_PIEZAS: TFloatField;
    cdsMultiLoteCW_PIEZAS: TFloatField;
    cdsAsignaAreas: TZetaClientDataSet;
    cdsAsignaAreasCB_CODIGO: TIntegerField;
    cdsAsignaAreasCB_AREA: TStringField;
    cdsAsignaAreasANTERIOR: TStringField;
    cdsCedEmpCB_TIPO: TSmallintField;
    cdsCedEmpCHECADAS: TIntegerField;
    cdsEmpAreas: TZetaLookupDataSet;
    cdsAsignaAreasPRETTYNAME: TStringField;
    cdsKarArea: TZetaClientDataSet;
    cdsAsignaAreasCB_AREA_DESCRIP: TStringField;
    cdsAsignaAreasANTERIOR_DESCRIP: TStringField;
    cdsEmpBreaks: TZetaClientDataSet;
    cdsDataSet: TZetaClientDataSet;
    cdsKardexDiario: TZetaClientDataSet;
    cdsMaquinas: TZetaLookupDataSet;
    cdsLayMaquinas: TZetaClientDataSet;
    cdsKarEmpMaq: TZetaClientDataSet;
    cdsSillasMaquina: TZetaClientDataSet;
    cdsSillasLayout: TZetaClientDataSet;
    cdsCertificEmpleados: TZetaClientDataSet;
    cdsCertificMaq: TZetaClientDataSet;
    cdsKarMaquinas: TZetaClientDataSet;
    cdsLayoutsAsignados: TZetaLookupDataSet;
    cdsCedEmpRETARDOS: TIntegerField;
    cdsCedEmpSTATUS: TIntegerField;
    procedure cdsWorksAlAdquirirDatos(Sender: TObject);
    procedure cdsWorksAlCrearCampos(Sender: TObject);
    procedure cdsOperaAlAdquirirDatos(Sender: TObject);
    procedure cdsWorksNewRecord(DataSet: TDataSet);
    {$ifdef VER130}
    procedure ReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEmpBreaksReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsKardexDiarioReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure ReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEmpBreaksReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsKardexDiarioReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure CatalogosAlAdquirirDatos(Sender: TObject);
    procedure cdsWorksAlModificar(Sender: TObject);
    procedure cdsWorksAlEnviarDatos(Sender: TObject);
    procedure cdsWorksAlBorrar(Sender: TObject);
    procedure cdsWorksLookupsValidate(Sender: TField);
    procedure cdsWOrderLookupLookupSearch(Sender: TZetaLookupDataSet;  var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsWOrderLookupLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure CB_CODIGOValidate(Sender: TField);
    procedure CB_CODIGOCedulasValidate(Sender: TField);
    procedure CB_CODIGOChange(Sender: TField);
    procedure CB_CODIGOBreaksChange(Sender: TField);
    procedure WK_HORA_RChange(Sender: TField);
    procedure WK_HORA_RSetText(Sender: TField; const Text: String);
    procedure CB_AREAChange(Sender: TField);
    procedure cdsGridLaborAlEnviarDatos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsWOrderLookupAlCrearCampos(Sender: TObject);
    procedure cdsMultiLoteWO_NUMBERChange(Sender: TField);
    procedure cdsMultiLoteAlEnviarDatos(Sender: TObject);
    procedure cdsAusenciaAlCrearCampos(Sender: TObject);
    procedure cdsAusenciaCalcFields(DataSet: TDataSet);
    procedure cdsCedulasAlAdquirirDatos(Sender: TObject);
    procedure cdsCedulasAlCrearCampos(Sender: TObject);
    procedure cdsCedEmpAfterOpen(DataSet: TDataSet);
    procedure cdsCedEmpAlAdquirirDatos(Sender: TObject);
    procedure cdsCedMultiAlAdquirirDatos(Sender: TObject);
    procedure cdsCedMultiAfterOpen(DataSet: TDataSet);
    procedure cdsCedMultiWO_NUMBERChange(Sender: TField);
    procedure cdsCedulasAlAgregar(Sender: TObject);
    procedure cdsCedulasAlModificar(Sender: TObject);
    procedure cdsCedulasNewRecord(DataSet: TDataSet);
    procedure cdsCedulasHuboCambios(DataSet: TDataSet);
    procedure cdsCedMultiAlEnviarDatos(Sender: TObject);
    procedure cdsCedulasAlEnviarDatos(Sender: TObject);
    procedure cdsCedulasBeforePost(DataSet: TDataSet);
    procedure cdsCedulasBeforeCancel(DataSet: TDataSet);
    procedure cdsCedulasAfterCancel(DataSet: TDataSet);
    procedure cdsCedulasAfterDelete(DataSet: TDataSet);
    procedure cdsWorksBeforePost(DataSet: TDataSet);
    procedure cdsCedEmpBeforeDelete(DataSet: TDataSet);
    procedure OP_NUMBERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsCedEmpAfterDelete(DataSet: TDataSet);
    procedure CE_TIPOCedulasChange(Sender: TField);
//    procedure cdsAsignaAreasAlEnviarDatos(Sender: TObject);
    procedure cdsCedEmpAlCrearCampos(Sender: TObject);
    procedure cdsCedEmpCHECADASGetText(Sender: TField; var Text: String; DisplayText: Boolean);
//    procedure cdsWorksCalcFields(DataSet: TDataSet);
    procedure cdsAreaAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpAreasAlAdquirirDatos(Sender: TObject);
    procedure cdsKarAreaAlAdquirirDatos(Sender: TObject);
    procedure cdsAsignaAreasCB_AREA_DESCRIPGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure cdsKarAreaAlCrearCampos(Sender: TObject);
    procedure cdsWorksAlAgregar(Sender: TObject);
    procedure cdsEmpBreaksAlCrearCampos(Sender: TObject);
    procedure cdsEmpBreaksAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpBreaksAlEnviarDatos(Sender: TObject);
    procedure cdsAreaAfterOpen(DataSet: TDataSet);
    procedure cdsKardexDiarioAlAdquirirDatos(Sender: TObject);
    procedure cdsKardexDiarioAlEnviarDatos(Sender: TObject);
    procedure cdsKardexDiarioAlCrearCampos(Sender: TObject);
    procedure CB_AREAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsKardexDiarioAlModificar(Sender: TObject);
    procedure cdsKardexDiarioAfterDelete(DataSet: TDataSet);
    procedure cdsKardexDiarioBeforePost(DataSet: TDataSet);
    procedure cdsKardexDiarioNewRecord(DataSet: TDataSet);
    procedure cdsCedulasReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsLayMaquinasAlAdquiriratos(Sender: TObject);
    procedure cdsMaquinasAlAdquirirDatos(Sender: TObject);
    procedure cdsKarEmpMaqAlAdquirirDatos(Sender: TObject);
    procedure cdsCertificEmpleadosAlAdquirirDatos(Sender: TObject);
    procedure cdsKarMaquinasAlAdquirirDatos(Sender: TObject);
    procedure cdsKarMaquinasAlCrearCampos(Sender: TObject);
    procedure cdsCertificMaqAlCrearCampos(Sender: TObject);
    procedure cdsLayoutsAsignadosAlAdquirirDatos(Sender: TObject);
    procedure cdsLayoutsAsignadosAlCrearCampos(Sender: TObject);
    procedure cdsLayoutsAsignadosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsMaquinasNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FSoloAltas: Boolean;
    FParametros : TZetaParams;
    ValidaLookups, FValidaEmpleado, FCambioArea, HuboCambiosMultiLote: Boolean;
    FArea: String;
    FOtrosDatos : Variant;
//    CedEmpleadoBorrado: Integer;
    FFechaArea: TDate;
    FHoraArea: String;
    FDuracion: Integer;
    FAreaData: TAreaData;
    FOperacion : eOperacionConflicto;
{$ifdef DOS_CAPAS}
    function GetServerLabor: TdmServerLabor;
    property ServerLabor: TdmServerLabor read GetServerLabor;
{$else}
    FServidor: IdmServerLaborDisp;
    function GetServerLabor: IdmServerLaborDisp;
    property ServerLabor: IdmServerLaborDisp read GetServerLabor;
{$endif}
    function Check( const Resultado: OleVariant): Boolean;
    procedure RegistrarWorks( var Forma; EdicionClass: TBaseEdicionClass_DevEx );
    procedure EditarGridCedulas(var Forma; EdicionClass: TBaseEdicionClass_DevEx);
    procedure CB_CODIGOWorksChange(Sender: TField);
    procedure AU_FECHAWorksChange(Sender: TField);
    procedure WO_NUMBERWorksChange(Sender: TField);
    procedure InitcdsColectivo(cdsColectivo: TClientDataset);
    procedure InitcdsCedulas(Tipo: eTipoCedula);
    procedure ShowEdicionCedulas;
    function VerificaCambiosMultiLote:Boolean;
    procedure ReportaErrores( oData: OleVariant );
    procedure SetCambioArea( DataSet: TDataSet );
  public
    { Public declarations }
    FCambiosWorks:Boolean;
    property ValidaEmpleado: Boolean read FValidaEmpleado write FValidaEmpleado;
    property Area: String read FArea write FArea;
    property OtrosDatos : Variant read FOtrosDatos write FOtrosDatos;
    property FechaArea: TDate read FFechaArea write FFechaArea;
    property HoraArea: String read FHoraArea write FHoraArea;
    property Duracion: Integer read FDuracion write FDuracion;
    function EsPrecalculada: Boolean;
    function GetcdsLookup(const sCampo: String): TZetaLookupDataSet;
    function GetMultiLote(var sOrden, sParte: String; var rQty: Currency): Boolean;
    function GetHoraDefault: string;
    procedure RegistraBreaks;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad );
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades );
    {$endif}
    procedure EditarGridOperaciones;
    procedure RegistrarLabor;
    procedure RegistrarTMuerto;
    //procedure RegistrarSalida;
    procedure ConectaWOrderLookUp(const Orden: string);
    procedure RefrescarLookupWOrder(Parametros: TZetaParams );
    procedure SetNombreLookups;
    procedure ShowGridLabor(Tipo: eTipoCedula);
    function ShowMultiLote:Boolean;
//    procedure LlenaListaCedEmp(const sArea: String; const lTodos: Boolean = FALSE );
    procedure DepurarListaEmpleados;
    procedure DepuraCedDataSets;
    procedure SetFiltroAreas( const lMisAreas: Boolean );
    procedure ShowAsignarAreas;
    function AgregaOtroEmpleado( const sEmpleado: String ): Boolean;
    function ChecaAreaAnterior( const iEmpleado: Integer; const sArea: String): String;
    procedure GrabaKardexAreas( Parametros: TZetaParams );
    procedure CancelarBreaksGetLista(Parametros: TZetaParams);
    function CancelarBreaks(Parametros: TZetaParams; const lVerificacion: Boolean; SetAnimacion : TProcAnimacion ): Boolean;
    function SumaPiezasMultiLote:TDiasHoras;
    procedure GetPlantillasAsignadas;
    procedure AsignarEmpleado(Empleado:Integer;Maquina:string);
    procedure GetCertificaciones(Maquina:string);
    function EstaCertificado(Empleado:Integer;Certificaciones:TList):Boolean;
    function GetLayoutGlobal:string;
    function GetMaquinaGlobal:string;
  end;

const
     USE_CDSMULTILOTE = 0;
     USE_CDSCEDMULTI = 1;
     K_MASK_FOLIO_CEDULA = '###,###,###';

var
  dmLabor: TdmLabor;
  PlantillaAsignadas : TList;
  LayoutActual:TLayout;

implementation

uses DCliente, DSuper, DCatalogos, DGlobal, DConsultas, DSistema, ZetaAsciiFile,
     FTressShell, FEditWorks_DevEx, FGridOperaciones, FRegistroWorks, FRegistroTMuerto,
     FCedulasOperaciones_DevEx, FCedulasEspeciales, FCedulasTMuerto,
     FGridMultiLote, ZetaLaborTools, ZGlobalTress, FAsignarAreas,
     ZetaCommonTools, ZReconcile, ZetaBuscaWorder_DevEx, ZetaMsgDlg, ZetaDialogo, ZBaseDlgModal_DevEx,
     FGridBreaks_DevEx, FEditKardexArea_DevEx, WizardFeedBack, FConsWorks_DevEx;

{$R *.DFM}

procedure TdmLabor.DataModuleCreate(Sender: TObject);
begin
     ValidaLookups:= FALSE;
     ValidaEmpleado:= FALSE;
     FCambioArea := FALSE;
     LayoutActual := TLayout.Create;
     {$ifdef INTERRUPTORES}
     cdsCedMulti.Fields.FindField( 'AR_CODIGO' ).Size := K_ANCHO_PARTE20;
     cdsCedMulti.Fields.FindField( 'AR_CODIGO' ).DisplayWidth := K_ANCHO_PARTE20;
     cdsMultiLote.Fields.FindField( 'AR_CODIGO' ).Size := K_ANCHO_PARTE20;
     cdsMultiLote.Fields.FindField( 'AR_CODIGO' ).DisplayWidth := K_ANCHO_PARTE20;
     {$endif}
end;

{$ifdef DOS_CAPAS}
function TdmLabor.GetServerLabor: TdmServerLabor;
begin
     Result := DCliente.dmCliente.ServerLabor;
end;
{$else}
function TdmLabor.GetServerLabor: IdmServerLaborDisp;
begin
     Result := IdmServerLaborDisp( dmCliente.CreaServidor( CLASS_dmServerLabor, FServidor ) );
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmLabor.NotifyDataChange(const Entidades: Array of TipoEntidad );
{$else}
procedure TdmLabor.NotifyDataChange(const Entidades: ListaEntidades );
{$endif}
begin

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enEmpleado , Entidades ) or
        Dentro( enAusencia , Entidades ) or
        Dentro( enWorks , Entidades ) or
        Dentro( enArea , Entidades ) then
     {$else}
     if ( enEmpleado in Entidades ) or
        ( enAusencia in Entidades ) or
        ( enWorks in Entidades ) or
        ( enArea in Entidades ) then
     {$endif}
     begin
          cdsWorks.SetDataChange;
          cdsKardexDiario.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enEmpleado , Entidades ) or
        Dentro( enAusencia , Entidades ) or
        Dentro( enArea , Entidades ) then
     {$else}
     if ( enEmpleado in Entidades ) or
        ( enAusencia in Entidades ) or
        ( enArea in Entidades ) then
     {$endif}
        cdsCedulas.SetDataChange;
end;

function TdmLabor.Check( const Resultado: OleVariant): Boolean;
begin
     with TWizardSuperFeedback.Create( Application ) do
     begin
          try
             with ProcessData do
             begin
                  SetResultado( Resultado );
                  Result := ( Status in [ epsEjecutando, epsOK ] );
             end;
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
          finally
             Free;
          end;
     end;
end;

{$ifdef VER130}
procedure TdmLabor.ReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    function ConflictoBreaks: Boolean;
    begin
         Result := ( Pos( 'Esta C�dula Abarca Un Break', E.Message ) > 0 );
    end;
begin
     Action := raAbort;
     if ConflictoBreaks then
     begin
          if ZetaDialogo.ZConfirm( '�Conflicto Con Breaks!', E.Message + CR_LF + CR_LF +
                                          '� Desea Registrarla ?', 0, mbNo ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               FOperacion := ocIgnorar;
               Action := raCorrect;
          end;
     end
     else
          ZetaDialogo.ZError( 'Error', GetErrorDescription( E ), 0 );
     //Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmLabor.ReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    function ConflictoBreaks: Boolean;
    begin
         Result := ( Pos( 'Esta C�dula Abarca Un Break', E.Message ) > 0 );
    end;
begin
     Action := raAbort;
     if ConflictoBreaks then
     begin
          if ZetaDialogo.ZConfirm( '�Conflicto Con Breaks!', E.Message + CR_LF + CR_LF +
                                          '� Desea Registrarla ?', 0, mbNo ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               FOperacion := ocIgnorar;
               Action := raCorrect;
          end;
     end
     else
          cdsCedulasReconcileError(DataSet,E,UpdateKind,Action);
          //ZetaDialogo.ZError( 'Error', GetErrorDescription( E ), 0 );
     //Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmLabor.CatalogosAlAdquirirDatos(Sender: TObject);
begin
    with Sender as TZetaClientDataSet do
        Data := ServerLabor.GetCatalogo( dmCliente.Empresa, Tag );
end;

{ cdsWorks }

procedure TdmLabor.cdsWorksAlAdquirirDatos(Sender: TObject);
var
   oAusencia : OleVariant;
begin
     cdsArea.Conectar;
     with dmCliente do
          cdsWorks.Data := ServerLabor.GetWorksEmpleado(Empresa, oAusencia, dmSuper.EmpleadoNumero, FechaSupervisor );
     cdsAusencia.Data := oAusencia;
end;

procedure TdmLabor.cdsWorksAlCrearCampos(Sender: TObject);
var
   i : Integer;
begin
     cdsOpera.Conectar;
     cdsPartes.Conectar;
     with cdsWorks do
     begin
          MaskPesos( 'WK_PIEZAS' );
          MaskTime( 'WK_HORA_A' );
          MaskTime( 'WK_HORA_R' );
          MaskNumerico( 'WK_CEDULA', K_MASK_FOLIO_CEDULA );
          MaskBool( 'WK_MANUAL' );
          CreateSimpleLookUp( cdsOpera, 'OP_NOMBRE', 'OP_NUMBER' );
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          ListaFija( 'WK_STATUS', lfStatusLectura );
          ListaFija( 'WK_TIPO', lfTipoLectura );
          with FieldByName( 'CB_CODIGO' ) do
          begin
               OnValidate := CB_CODIGOValidate;
               OnChange := CB_CODIGOWorksChange;
          end;
          FieldByName( 'AU_FECHA' ).OnChange := AU_FECHAWorksChange;
          with FieldByName( 'WK_HORA_R' ) do
          begin
               OnChange  := WK_HORA_RChange;
               OnSetText := WK_HORA_RSetText;
          end;
          with FieldByName( 'WO_NUMBER' ) do
          begin
               OnValidate := cdsWorksLookupsValidate;
               OnChange := WO_NUMBERWorksChange;
          end;
          with FieldByName( 'CB_AREA' ) do
          begin
               OnChange  := CB_AREAChange;
               OnValidate := cdsWorksLookupsValidate;
          end;
          FieldByName( 'AR_CODIGO' ).OnValidate := cdsWorksLookupsValidate;
          FieldByName( 'OP_NUMBER' ).OnValidate := cdsWorksLookupsValidate;
          FieldByName( 'WK_TMUERTO' ).OnValidate := cdsWorksLookupsValidate;
          for i := 1 to 3 do
              FieldByName( 'WK_MOD_' + IntToStr( i ) ).OnValidate := cdsWorksLookupsValidate;
     end;
end;

procedure TdmLabor.cdsWorksNewRecord(DataSet: TDataSet);
begin
     with cdsWorks do
     begin
          FCambioArea := TRUE;
          try
             with dmCliente do
             begin
                  FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
                  FieldByName( 'AU_FECHA' ).AsDateTime := FechaSupervisor;
             end;
          finally
                 FCambioArea := FALSE;
          end;
          FieldByName( 'WK_HORA_R' ).AsString := GetHoraDefault; //FormatDateTime( 'hhmm', Now );
          FieldByName('WK_MANUAL').AsString := K_GLOBAL_SI;
          FieldByName( 'WK_TIPO' ).AsInteger := Ord( wtChecada );
          FieldByName('WK_PIEZAS').AsFloat := 1;
          FieldByName('WO_NUMBER').AsString := VACIO;
          FieldByName('WK_MOD_1').AsString := VACIO;
          FieldByName('WK_MOD_2').AsString := VACIO;
          FieldByName('WK_MOD_3').AsString := VACIO;
          FieldByName('WK_TMUERTO').AsString := VACIO;
     end;
end;

procedure TdmLabor.cdsWorksAlAgregar(Sender: TObject);
begin
     with cdsWorks do
     begin
          ValidaLookups := TRUE;
          try
             Append;
          finally
                 ValidaLookups := FALSE;
          end;
          Modificar;
     end;
end;

procedure TdmLabor.cdsWorksAlModificar(Sender: TObject);
var
   ConsWorks: TConsWorks_DevEx;
begin
     with cdsWorks do
     begin{
          if not ( eTipoLectura( FieldByName('WK_TIPO').AsInteger) in [ wtChecada, wtLibre ] ) then
             ZetaDialogo.ZError( 'Operaciones Registradas',
                                 'No se Pueden Editar Checadas de Tipo: '+
                                 ObtieneElemento(lfTipoLectura, FieldByName('WK_TIPO').AsInteger ), 0 )

          else if not ( zStrToBool( FieldByName( 'WK_MANUAL' ).AsString ) ) then
               ZetaDialogo.ZError( 'Operaciones Registradas',
                                   'No se Pueden Editar Checadas de Sistema', 0 )
          else
          }
          if ( zStrToBool( FieldByName( 'WK_MANUAL' ).AsString ) ) then
          begin
               ValidaLookups := TRUE;
               try
                  FCambiosWorks:= FALSE;
                  //DevEx (by am): Se invoca ala nueva forma de EditWorks
                  ZBaseEdicion_DevEx.ShowFormaEdicion( EditWorks_DevEx,TEditWorks_DevEx );
                  //
                  if FCambiosWorks then
                     TressShell.SetDataChange( [ enWorks, enAusencia ] );
               finally
                      ValidaLookups := FALSE;
               end;
          end
          else
          begin
               begin
                    ConsWorks_DevEx := TConsWorks_DeVEx.Create( Self );
                    with ConsWorks_DevEx do
                    begin
                         try
                            Connect;
                            ShowModal
                         finally
                           FreeAndNil( ConsWorks_DevEx );
                           end;
                    end;

               end;
          end;
     end;
end;

procedure TdmLabor.cdsWorksAlBorrar(Sender: TObject);
begin
     {
     if not ( zStrToBool( cdsWorks.FieldByName( 'WK_MANUAL' ).AsString ) ) then
               ZetaDialogo.ZError( 'Operaciones Registradas',
                                   'No se Pueden Borrar Checadas de Sistema', 0 )
     else
     }
     if ( zStrToBool( cdsWorks.FieldByName( 'WK_MANUAL' ).AsString ) ) then
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Operacion ?' ) then
          begin
               with cdsWorks do
               begin
                    Delete;
                    FCambiosWorks:= FALSE;
                    Enviar;
                    if FCambiosWorks then
                       TressShell.SetDataChange( [ enWorks, enAusencia ] );
               end;
          end;
     end
     else
         ZetaDialogo.ZError( 'Operaciones Registradas',
                             'No se Pueden Borrar Checadas de Sistema', 0 );
end;

procedure TdmLabor.cdsWorksBeforePost(DataSet: TDataSet);
var
   sPiezas: String;
begin
     with cdsWorks do
     begin
          if StrVacio( FieldByName( 'WK_HORA_R' ).AsString ) then
          begin
               FieldByName( 'WK_HORA_R' ).FocusControl;
               DataBaseError( 'Debe Especificarse La Hora' );
          end;
          sPiezas := Global.GetGlobalString( K_GLOBAL_LABOR_PIEZAS );
          if ( StrLleno( sPiezas ) ) and ( FieldByName( 'WK_PIEZAS' ).AsFloat < 0 ) and
             ( eTipoLectura( FieldByName( 'WK_TIPO' ).AsInteger ) <> wtLibre ) then
          begin
               FieldByName( 'WK_PIEZAS' ).FocusControl;
               DataBaseError( sPiezas + ' Debe Ser Mayor � Igual A Cero' );
          end;
          {
          if EsPrecalculada then
          begin
          }
               { GA: Fix temporal para almacenar en WK_HRS_3EX la }
               { duraci�n de una checada precalculada. Se debe poner }
               { tambi�n en D:\3Win_20\Labor\DLabor.pas }
               { MV 16/Ago/2002: Se creo un nuevo campo para la checada Precalculada }
          {
               with cdsWorks do
               begin
                    FieldByName( 'WK_PRE_CAL' ).AsFloat := FieldByName( 'WK_TIEMPO' ).AsFloat;
               end;
          end;
          }
     end;
end;

function TdmLabor.EsPrecalculada: Boolean;
begin
     Result := ( eTipoLectura( cdsWorks.FieldByName( 'WK_TIPO' ).AsInteger ) in [ wtLibre, wtPrecalculada ] );
end;

procedure TdmLabor.cdsWorksAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsWorks do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerLabor.GrabaWorks( dmCliente.Empresa, Delta, ErrorCount ) );
               if ErrorCount = 0 then
                  FCambiosWorks := TRUE;
          end;
     end;
end;

{
procedure TdmLabor.cdsWorksCalcFields(DataSet: TDataSet);
var
   sTotal : string;

begin
     with cdsWorks do
     begin
          sTotal :=  aHoraString( AMinutos( FieldByName( 'WK_HORA_R' ).AsString ) - FieldByName( 'WK_TIEMPO' ).AsInteger );
          FieldByName( 'WK_HORA_INI' ).AsString := sTotal;
     end;
end;
}

procedure TdmLabor.cdsWorksLookupsValidate(Sender: TField);
const
     sMensaje = 'No Existe el C�digo: %s en %s';
var
   cdsWorksLookup : TZetaLookupDataSet;
   sValor : String;
begin
     if ValidaLookups then
     begin
          with Sender do
          begin
               sValor := AsString;
               cdsWorksLookup := GetcdsLookup( FieldName );
          end;
          if ( sValor <> VACIO ) then
             with cdsWorksLookup do
                  if ( GetDescripcion( sValor ) = VACIO ) then
                     DataBaseError( Format( sMensaje, [ sValor, LookupName ] ) );
     end;
end;

procedure TdmLabor.CB_CODIGOValidate(Sender: TField);
var
   iEmpleado: Integer;
begin
     iEmpleado := Sender.AsInteger;
     if ( iEmpleado <= 0 ) then
        DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' )
     else if ValidaEmpleado and ( not dmSuper.cdsEmpleados.Locate( 'CB_CODIGO', iEmpleado, [] ) ) then
     begin
          if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( iEmpleado ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
             DataBaseError( 'No Existe el Empleado #' + IntToStr( iEmpleado ) )
          else
             DataBaseError( 'No Tiene Asignado el Empleado #' + IntToStr( iEmpleado ) + ' para esta Fecha' );
     end;
end;

procedure TdmLabor.CB_CODIGOWorksChange(Sender: TField);
begin
     with Sender do
     begin
          DataSet.FieldByName( 'CB_PUESTO' ).AsString := dmSuper.cdsEmpleados.FieldByName( 'CB_PUESTO' ).AsString;
          SetCambioArea( DataSet );
     end;
end;

procedure TdmLabor.SetCambioArea( DataSet: TDataSet );
begin
     if not FCambioArea then
     begin
          with cdsWorks do
               DataSet.FieldByName( 'CB_AREA' ).AsString :=  ServerLabor.GetAreaKardex( dmCliente.Empresa, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'AU_FECHA' ).AsDateTime, FieldByName( 'WK_HORA_R' ).AsString );
     end;
end;

procedure TdmLabor.AU_FECHAWorksChange(Sender: TField);
begin
     SetCambioArea( Sender.DataSet );
end;

procedure TdmLabor.WO_NUMBERWorksChange(Sender: TField);
begin
     with Sender do
          if StrLleno( AsString ) then
             DataSet.FieldByName( 'AR_CODIGO' ).AsString := cdsWOrderLookup.FieldByName( 'AR_CODIGO').AsString;
end;

procedure TdmLabor.WK_HORA_RChange(Sender: TField);
begin
     with Sender do
     begin
          DataSet.FieldByName( 'WK_HORA_A' ).AsString := Sender.AsString;
          SetCambioArea( DataSet );
     end;
end;

procedure TdmLabor.WK_HORA_RSetText(Sender: TField; const Text: String);
begin
     Sender.AsString := ConvierteHora( Text );
end;


procedure TdmLabor.CB_AREAChange(Sender: TField);
begin
     if ( Sender.AsString <> VACIO ) then
        with GetcdsLookup( Sender.FieldName ) do
             Sender.DataSet.FieldByName( 'OP_NUMBER' ).AsString := FieldByName('TB_OPERA').AsString;
end;


procedure TdmLabor.EditarGridOperaciones;
begin
     ValidaLookups:= TRUE;
     cdsWorks.Filtered:= TRUE;
     FCambiosWorks := FALSE;
     try
        //DevEx (by am): Llamada nueva forma del Grid de Edicion
        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridOperaciones, TGridOperaciones, FALSE );
     finally
        cdsWorks.Filtered:= FALSE;
        ValidaLookups:= FALSE;
     end;
     if FCambiosWorks then
        TressShell.SetDataChange( [ enWorks, enAusencia ] );
end;
 
procedure TdmLabor.RegistrarWorks( var Forma; EdicionClass: TBaseEdicionClass_DevEx );
var
   iEmpleadoActual : Integer;
begin
     with cdsWorks do
     begin
          if not Active then
             Conectar;
          Append;              // Si se usara metodo Agregar() se invocaria la otra forma de edici�n
     end;
     try
        dmCliente.SetLookupEmpleado;
        with dmSuper do
        begin
             RefrescarFormaActiva := FALSE;
             iEmpleadoActual := EmpleadoNumero;
             cdsEmpleados.DisableControls;
             FCambiosWorks:= FALSE;
             cdsMultiLote.Active := FALSE;
             try
                ValidaLookups := TRUE;
                ValidaEmpleado := TRUE;
                ZBaseEdicion_DevEx.ShowFormaEdicion( Forma, EdicionClass );
                if not cdsEmpleados.Locate( 'CB_CODIGO', iEmpleadoActual, [] ) then
                   RefrescarFormaActiva := TRUE;
             finally
                cdsEmpleados.EnableControls;
                RefrescarFormaActiva := TRUE;
                ValidaEmpleado:= FALSE;
                ValidaLookups := FALSE;
             end;
        end;
        if FCambiosWorks then
        begin
             dmSuper.RefrescaMisEmpleados;
             TressShell.SetDataChange( [ enWorks, enAusencia ] );
        end;
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmLabor.RegistrarLabor;
begin
     RegistrarWorks( RegistroWorks, TRegistroWorks );
end;

procedure TdmLabor.RegistrarTMuerto;
begin
     RegistrarWorks( RegistroTMuerto, TRegistroTMuerto );
end;

{procedure TdmLabor.RegistrarSalida;
begin
     FCambioArea:= TRUE;
     try
        RegistrarWorks( RegistroSalida, TRegistroSalida );
     finally
        FCambioArea:= FALSE;
     end;
end;}

{ cdsGridLabor }

procedure TdmLabor.cdsGridLaborAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsWorks do          // Nota que se usa Works, cdsGridLabor es solo para edici�n
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               ServerLabor.GrabaGridLabor( dmCliente.Empresa, FArea, Delta, ErrorCount );
               if ErrorCount > 0 then
                  ZError( 'Supervisores', 'Se Detectaron Errores al Registrar Labor' + CR_LF + 'Revisar Bit�cora para Detalles', 0 );
               cdsGridLabor.EmptyDataSet;
               FCambiosWorks := TRUE;
          end;
     end;
end;

{ cdsArea }

procedure TdmLabor.cdsAreaAlAdquirirDatos(Sender: TObject);
begin
     cdsArea.Data := ServerLabor.GetAreas( dmCliente.Empresa );
end;

procedure TdmLabor.cdsAreaAfterOpen(DataSet: TDataSet);
begin
     with cdsArea do
     begin
          with FAreaData do
          begin
               PrimeraHora := FieldByName( 'AR_PRI_HOR' ).AsString;
               HayPrimeraHora := ZetaCommonTools.StrLleno( PrimeraHora );
               Shift := eTurnoLabor( FieldByName( 'AR_SHIFT' ).AsInteger );
          end;
     end;
end;

{ cdsOpera }

procedure TdmLabor.cdsOperaAlAdquirirDatos(Sender: TObject);
begin
    with Sender as TZetaClientDataSet do
         Data := ServerLabor.GetCatalogo( dmCliente.Empresa, Tag );
end;

{ cdsWOrderLookup }

procedure TdmLabor.cdsWOrderLookupLookupSearch(Sender: TZetaLookupDataSet;
          var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     lOk := ZetaBuscaWorder_DevEx.BuscaWorderDialogo( sFilter, sKey, sDescription );
end;

procedure TdmLabor.cdsWOrderLookupLookupKey(Sender: TZetaLookupDataSet;
          var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     ConectaWOrderLookUp( sKey );
     lOk := not cdsWOrderLookup.IsEmpty;
     if lOk then
        sDescription := cdsWOrderLookup.FieldByName('WO_DESCRIP').AsString;
end;

procedure TdmLabor.cdsWOrderLookupAlCrearCampos(Sender: TObject);
begin
     with cdsWOrderLookup do
     begin
          CreateSimpleLookUp( cdsPartes, 'AR_NOMBRE', 'AR_CODIGO' );
          ListaFija( 'WO_STATUS', lfWorkStatus );
          MaskFecha( 'WO_FEC_INI' );
     end;

end;

procedure TdmLabor.ConectaWOrderLookUp(const Orden: string);
begin
     if Orden > VACIO then
     begin
          if FParametros = NIL then
             FParametros := TZetaParams.Create;

          with FParametros do
          begin
               AddInteger( 'Tipo', -1 );
               AddString( 'Parte', VACIO );
               AddDate( 'FechaInicial', NullDateTime );
               AddDate( 'FechaFinal', NullDateTime );
               AddString( 'OrderInicial', Orden );
               AddString( 'OrderFinal', Orden );
          end;
          RefrescarLookupWOrder(FParametros)
     end;
end;

procedure TdmLabor.RefrescarLookupWOrder(Parametros: TZetaParams);
begin
     cdsWOrderLookup.Data := ServerLabor.GetWorder( dmCliente.Empresa, Parametros.VarValues, Ord(ewLookup) );
end;

{ cdsMultiLote }

procedure TdmLabor.cdsMultiLoteWO_NUMBERChange(Sender: TField);
begin
     cdsMultiLote.FieldByName( 'WO_DESCRIP' ).AsString := cdsWOrderLookup.GetDescription;
     cdsMultiLote.FieldByName( 'AR_CODIGO' ).AsString := cdsWOrderLookup.FieldByName('AR_CODIGO').AsString;
end;

procedure TdmLabor.cdsMultiLoteAlEnviarDatos(Sender: TObject);
begin
     with cdsMultiLote do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
             MergeChangeLog;
     end;
end;

{ cdsAusencia }

procedure TdmLabor.cdsAusenciaAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsHorarios.Conectar;
     with cdsAusencia do
     begin
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          CreateCalculated( 'AU_DIA_SEM', ftString, 10 );
          CreateSimpleLookUp( dmCatalogos.cdsHorarios, 'HO_DESCRIP', 'HO_CODIGO' );
     end;
end;

procedure TdmLabor.cdsAusenciaCalcFields(DataSet: TDataSet);
begin
     with cdsAusencia do
          FieldByName( 'AU_DIA_SEM' ).AsString := ZetaCommonTools.DiaSemana( FieldByName( 'AU_FECHA' ).AsDateTime );
end;

{ cdsCedulas }

procedure TdmLabor.cdsCedulasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsCedulas.Data := ServerLabor.GetCedulas(Empresa, FechaSupervisor );
end;

procedure TdmLabor.cdsCedulasAlCrearCampos(Sender: TObject);
begin
     with cdsCedulas do
     begin
          MaskNumerico( 'CE_FOLIO', K_MASK_FOLIO_CEDULA );
          MaskPesos( 'CE_TIEMPO' );
          MaskPesos( 'CE_PIEZAS' );
          MaskTime( 'CE_HORA' );
          //ListaFija( 'CE_STATUS', lfStatusLectura );
          ListaFija( 'CE_TIPO', lfTipoCedula );
          FieldByName( 'CE_TIPO' ).OnChange := CE_TIPOCedulasChange;
          FieldByName( 'CE_HORA' ).OnSetText := WK_HORA_RSetText;
          FieldByName( 'OP_NUMBER' ).OnGetText := OP_NUMBERGetText;
          with FieldByName( 'US_CODIGO' ) do
          begin
               OnGetText := US_CODIGOGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

procedure TdmLabor.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := VACIO
     else
        Text := Trim( Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString ) );
end;


procedure TdmLabor.OP_NUMBERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   iTipo: Integer;
begin
     if DisplayText then
     begin
          iTipo := Sender.DataSet.FieldByName( 'CE_TIPO' ).AsInteger;
          if ( eTipoCedula( iTipo ) <> tcTMuerto ) then
             Text := cdsOpera.GetDescripcion( Sender.AsString )
          else
             Text := cdsTiempoMuerto.GetDescripcion( Sender.DataSet.FieldByName( 'CE_TMUERTO' ).AsString );
     end;
end;

procedure TdmLabor.cdsCedulasNewRecord(DataSet: TDataSet);
begin
     with cdsCedulas do
     begin
          FieldByName( 'CE_FECHA' ).AsDateTime := dmCliente.FechaSupervisor;
          FieldByName( 'CE_HORA' ).AsString := GetHoraDefault;// FormatDateTime( 'hhmm', Now );
          FieldByName( 'CE_PIEZAS' ).AsFloat := 1;
     end;
end;

procedure TdmLabor.cdsCedulasAlAgregar(Sender: TObject);
begin
     ShowGridLabor( tcOperacion );
end;

procedure TdmLabor.cdsCedulasAlModificar(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             cdsCedEmp.Refrescar;
             if zStrToBool( cdsCedulas.FieldByName( 'CE_MULTI' ).AsString ) then
                cdsCedMulti.Refrescar
             else
             begin
                  InitcdsColectivo( cdsCedMulti );
                  HuboCambiosMultiLote:= FALSE;
             end;
          finally
             Cursor := oCursor;
          end;
     end;
     ShowEdicionCedulas;
end;

procedure TdmLabor.cdsCedulasBeforeCancel(DataSet: TDataSet);
begin
     if ( cdsCedulas.State in [ dsEdit, dsInsert ] ) then     // Es Inserci�n � Modificaci�n
        with cdsCedEmp do
        begin
             if ( State in [ dsEdit, dsInsert ] ) then
                Cancel;
             CancelUpdates;
        end;
end;

procedure TdmLabor.cdsCedulasAfterCancel(DataSet: TDataSet);
begin
     if zStrToBool( cdsCedulas.FieldByName( 'CE_MULTI' ).AsString ) then
        cdsCedMulti.Refrescar
     else
     begin
          InitcdsColectivo( cdsCedMulti );
          HuboCambiosMultiLote:= FALSE;
     end;
end;

procedure TdmLabor.cdsCedulasBeforePost(DataSet: TDataSet);
begin
     cdsCedulas.FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmLabor.cdsCedulasAfterDelete(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsCedulas do
          if ( ChangeCount > 0 ) then
          begin
               Reconcile ( ServerLabor.BorraCedula( dmCliente.Empresa, Delta, ErrorCount ) );
               if ErrorCount = 0 then
                  TressShell.SetDataChange( [ enWorks ] )
               else
                  cdsCedulas.CancelUpdates;
          end;
end;

procedure TdmLabor.cdsCedulasAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   Parametros: TZetaParams;
   lReconcilia:Boolean;
begin
     ErrorCount := 0;
     with cdsCedEmp do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsCedulas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount = 0 ) and ( ( cdsCedEmp.ChangeCount > 0 ) or ( HuboCambiosMultiLote ) ) then   //Solo Cambiaron las Empleados o MultiLotes
          begin
               Edit;
               FieldByName( 'SETCAMBIOS' ).AsString := K_GLOBAL_SI;   // Forzar que lleve un Delta
               Post;
          end;
          if ( ChangeCount > 0 ) then
          begin
               Parametros := TZetaParams.Create;
               try
                  FOperacion := ocReportar;
                  Repeat
                        Parametros.AddInteger( 'Operacion', Ord( FOperacion ) );
                        lReconcilia := Reconcile( ServerLabor.GrabaCedula( dmCliente.Empresa, Delta, cdsCedEmp.Data, cdsCedMulti.Data, Parametros.VarValues, ErrorCount ));
                  Until( lReconcilia or ( FOperacion = ocReportar ) );   // Cuando es individual se abortar� y pondr� ocReportar en True
               finally
                      FreeAndNil( Parametros );
               end;

               //Reconcile ( ServerLabor.GrabaCedula( dmCliente.Empresa, Delta, cdsCedEmp.Data, cdsCedMulti.Data, ErrorCount ) );
               if ErrorCount = 0 then
                  FCambiosWorks := TRUE;
          end;
     end;
end;

procedure TdmLabor.cdsCedulasHuboCambios(DataSet: TDataSet);
begin
     with cdsCedulas do
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
end;

procedure TdmLabor.ShowEdicionCedulas;
begin
     case eTipoCedula( cdsCedulas.FieldByName( 'CE_TIPO' ).AsInteger ) of
          tcOperacion : EditarGridCedulas( CedulasOperaciones_DevEx, TCedulasOperaciones_DevEx );
          tcEspeciales: EditarGridCedulas( CedulasEspeciales, TCedulasEspeciales );
          tcTMuerto: EditarGridCedulas( CedulasTMuerto, TCedulasTMuerto );
     end;
end;

procedure TdmLabor.CE_TIPOCedulasChange(Sender: TField);
begin
     if eTipoCedula( cdsCedulas.FieldByName( 'CE_TIPO' ).AsInteger ) = tcTMuerto then
        cdsCedulas.FieldByName( 'CE_PIEZAS' ).AsFloat := 0;
end;

{ cdsCedEmp }

procedure TdmLabor.cdsCedEmpAlAdquirirDatos(Sender: TObject);
begin
     cdsCedEmp.Data := ServerLabor.GetCedEmpleados(dmCliente.Empresa,
                       cdsCedulas.FieldByName( 'CE_FOLIO' ).AsInteger );
end;

procedure TdmLabor.cdsCedEmpAfterOpen(DataSet: TDataSet);
begin
     with cdsCedEmp.FieldByName( 'CB_CODIGO' ) do
     begin
          OnValidate := CB_CODIGOCedulasValidate;
          OnChange := CB_CODIGOChange;
     end;
end;

procedure TdmLabor.cdsCedEmpCHECADASGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := VACIO;
end;

procedure TdmLabor.cdsCedEmpAlCrearCampos(Sender: TObject);
begin
     with dmSuper, cdsCedEmp do
     begin
          CreateLookup( cdsEmpleados, 'CB_TIPO', 'CB_CODIGO', 'CB_CODIGO', 'CB_TIPO' );
          CreateLookup( cdsEmpleados, 'CHECADAS', 'CB_CODIGO', 'CB_CODIGO', 'CHECADAS' );
          //DevEx (by am): LookUps agregados para que los grids Verificar Lista de Empleados siga el mismoa comportamiendo que el del Shell
          CreateLookup( cdsEmpleados, 'RETARDOS', 'CB_CODIGO', 'CB_CODIGO', 'RETARDOS' );
          CreateLookup( cdsEmpleados, 'STATUS', 'CB_CODIGO', 'CB_CODIGO', 'STATUS' );
          //
     end;
end;

procedure TdmLabor.cdsCedEmpBeforeDelete(DataSet: TDataSet);
begin
//     CedEmpleadoBorrado := DataSet.FieldByName( 'CB_CODIGO' ).AsInteger;
end;

procedure TdmLabor.cdsCedEmpAfterDelete(DataSet: TDataSet);
{var
   Pos : TBookMark;}
begin
{     if ValidaEmpleado then
        with DataSet do
        begin
             Pos:= GetBookMark;
             if ( not Locate( 'CB_CODIGO', CedEmpleadoBorrado, [] ) ) and
                ( dmSuper.cdsAsignaciones.Locate( 'CB_CODIGO', CedEmpleadoBorrado, [] ) ) then
                dmSuper.cdsAsignaciones.Delete;
             if ( Pos <> nil ) then
             begin
                  GotoBookMark( Pos );
                  FreeBookMark( Pos );
             end;
        end;
     cdsCedulasHuboCambios( DataSet );}
end;

procedure TdmLabor.CB_CODIGOCedulasValidate(Sender: TField);
var
   iEmpleado: Integer;
   sNombre: String; //sCodigo

{   procedure GetSuperCodigo;
   var
      sCodigoDesc: String;
   begin
        with dmSuper.cdsSupervisores do
        begin
             First;
             sCodigo := FieldByName('TB_CODIGO').AsString;
             if ( RecordCount > 1 ) then       //Son Varios y se debe Preguntar Cual
                if not Search( 'MI_CODIGO <> ''''', sCodigo, sCodigoDesc ) then
                   sCodigo:= VACIO;
        end;
   end;}

begin
     iEmpleado := Sender.AsInteger;
     if ( iEmpleado <= 0 ) then
        DataBaseError( 'N�mero de Empleado Debe Ser Mayor a Cero' )
     else if ValidaEmpleado and ( not dmSuper.cdsEmpleados.Locate( 'CB_CODIGO', iEmpleado, [] ) ) then // and
          //( not dmSuper.cdsAsignaciones.Locate( 'CB_CODIGO', iEmpleado, [] ) ) then
     begin
          sNombre:= dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( iEmpleado ) );
          if ( sNombre = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
             DataBaseError( 'No Existe el Empleado #' + IntToStr( iEmpleado ) )
          else if ( not zStrToBool( dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_ACTIVO' ).AsString ) ) then
             DataBaseError( 'El Empleado # ' + IntToStr( iEmpleado ) + ' est� dado de baja' )
          else //if ZWarningConfirm( '', 'No Tiene Asignado el Empleado #' + CR_LF +
               ZWarning( VACIO, 'No Tiene Asignado el Empleado #' + CR_LF +
                                   IntToStr( iEmpleado ) + ' ' + sNombre + CR_LF +
                                   'Para esta Fecha.', 0, mbOk ) // 'Desea Transferirlo por este D�a ?' then
{          begin
               with dmSuper do
               begin
                    cdsSupervisores.Conectar;
                    SetFiltroSupervisores( TRUE );
                    GetSuperCodigo;
                    if strLleno( sCodigo ) then
                       cdsAsignaciones.AppendRecord( [ iEmpleado, sCodigo, VACIO ] ) //  ... Borrar Asignacion Anterior y Validar si EsAsignacion() se har� en el Servidor
                    else
                       Abort;     // Si decidi� Cancelar en la Lista de Supervisores se hace una excepcion silenciosa
                       //DataBaseError( 'No Tiene Asignado el Empleado #' + IntToStr( iEmpleado ) + ' para esta fecha' );
               end;
          end
          else
              Abort;}
     end;
end;

procedure TdmLabor.CB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet, dmSuper, dmCliente do
     begin
          if ( Sender.AsInteger = EmpleadoNumero ) then
          begin
               FieldByName( 'PRETTYNAME' ).AsString := EmpleadoNombre;
               FieldByName( 'CB_PUESTO' ).AsString := cdsEmpleados.FieldByName( 'CB_PUESTO' ).AsString;

               //DevEx (by am): Se asigna el valor a los campos de RETARDOS Y STATUS
               {FieldByName( 'RETARDOS' ).AsString := cdsEmpleados.FieldByName( 'RETARDOS' ).AsString;
               FieldByName( 'STATUS' ).AsString := cdsEmpleados.FieldByName( 'STATUS' ).AsString;}
          end
          else if ( Sender.AsInteger = cdsEmpleadoLookup.FieldByName( 'CB_CODIGO' ).AsInteger ) then
          begin
               FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
               FieldByName( 'CB_PUESTO' ).AsString := cdsEmpleadoLookup.FieldByName( 'CB_PUESTO' ).AsString;
          end;
     end;
end;

{ cdsCedMulti }

procedure TdmLabor.cdsCedMultiAlAdquirirDatos(Sender: TObject);
begin
     cdsCedMulti.Data := ServerLabor.GetCedMultiLote(dmCliente.Empresa,
                       cdsCedulas.FieldByName( 'CE_FOLIO' ).AsInteger );
     HuboCambiosMultiLote:= FALSE;
end;

procedure TdmLabor.cdsCedMultiAfterOpen(DataSet: TDataSet);
begin
     with cdsCedMulti.FieldByName( 'WO_NUMBER' ) do
     begin
          OnValidate := cdsWorksLookupsValidate;
          OnChange := cdsCedMultiWO_NUMBERChange;
     end;
end;

procedure TdmLabor.cdsCedMultiWO_NUMBERChange(Sender: TField);
begin
     cdsCedMulti.FieldByName( 'WO_DESCRIP' ).AsString := cdsWOrderLookup.GetDescription;
     cdsCedMulti.FieldByName( 'AR_CODIGO' ).AsString := cdsWOrderLookup.FieldByName('AR_CODIGO').AsString;
end;

procedure TdmLabor.cdsCedMultiAlEnviarDatos(Sender: TObject);
begin
     with cdsCedMulti do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               MergeChangeLog;
               HuboCambiosMultiLote:= TRUE;
          end;
     end;
end;

{ Metodos Privados y Publicos }

function TdmLabor.GetcdsLookup( const sCampo: String ): TZetaLookupDataSet;
begin
     if ( sCampo = 'WO_NUMBER' ) then
        Result := cdsWOrderLookup
     else if ( sCampo = 'AR_CODIGO' ) then
        Result := cdsPartes
     else if ( sCampo = 'OP_NUMBER' ) then
        Result := cdsOpera
     else if ( sCampo = 'CB_AREA' ) then
        Result := cdsArea
     else if ( sCampo = 'WK_MOD_1' ) then
        Result := cdsModula1
     else if ( sCampo = 'WK_MOD_2' ) then
        Result := cdsModula2
     else if ( sCampo = 'WK_MOD_3' ) then
        Result := cdsModula3
     else
        Result := cdsTiempoMuerto;
end;

function TdmLabor.GetMultiLote( var sOrden, sParte: String; var rQty: Currency ): Boolean;
begin
     with cdsMultiLote do
     begin
          if not Active then
             CreateDataSet;
          if ( IsEmpty or ( RecordCount < 2 ) ) and StrLleno( sOrden ) then
          begin
               EmptyDataSet;
               Append;
               FieldByName( 'WO_NUMBER' ).AsString := sOrden;
               FieldByName( 'CW_PIEZAS' ).AsFloat := rQty;
               MergeChangeLog;
          end;
          ValidaLookups:= TRUE;
          try
             ZBaseGridEdicion_DevEx.ShowGridEdicion( GridMultiLote, TGridMultiLote, USE_CDSMULTILOTE );
          finally
             ValidaLookups := FALSE;
          end;
          Result := ( RecordCount > 1 );
          if IsEmpty then
             sOrden := VACIO
          else
          begin
               First;
               sOrden := FieldByName( 'WO_NUMBER' ).AsString;
               sParte := FieldByName( 'AR_CODIGO' ).AsString;
               rQty   := FieldByName( 'CW_PIEZAS' ).AsFloat;
          end;
     end;
end;

procedure TdmLabor.SetNombreLookups;
const
     CAPTION_CATALOGO = 'Cat�logo de %s';
begin
     cdsOpera.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_OPERACIONES ) ] );
     cdsWOrderLookup.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_ORDENES ) ] );
     cdsModula1.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_MODULA1 ) ] );
     cdsModula2.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_MODULA2 ) ] );
     cdsModula3.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_MODULA3 ) ] );
     cdsTiempoMuerto.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_TMUERTO ) ] );
     cdsArea.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_AREAS ) ] );
     cdsPartes.LookupName := Format( CAPTION_CATALOGO, [ GetLaborLabel( K_GLOBAL_LABOR_PARTES ) ] );
end;


procedure TdmLabor.InitcdsColectivo( cdsColectivo: TClientDataset );
begin
     with cdsColectivo do
          if Active then
             EmptyDataSet
          else
             CreateDataSet;
end;


procedure TdmLabor.InitcdsCedulas( Tipo: eTipoCedula );
begin
     with cdsCedulas do
     begin
          Conectar;
          Append;
          FieldByName( 'CE_TIPO' ).AsInteger := Ord( Tipo );
     end;
     InitcdsColectivo( cdsCedEmp );
     InitcdsColectivo( cdsCedMulti );
     HuboCambiosMultiLote:= FALSE;
end;

procedure TdmLabor.ShowGridLabor( Tipo: eTipoCedula );
begin
{     if dmSuper.VerificaFiltroEmpleados then
     begin}
          InitcdsCedulas( Tipo );
          ShowEdicionCedulas;
//     end;
end;

procedure TdmLabor.EditarGridCedulas(var Forma; EdicionClass: TBaseEdicionClass_DevEx);
var
   iEmpleadoActual : Integer;

{   procedure InitAsignaciones;
   begin
        with dmSuper.cdsAsignaciones do
        begin
             if Active then
                Active := FALSE;
             CreateDataSet;
        end;
   end;}

begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpCedulas );
        with dmSuper do
        begin
             RefrescarFormaActiva := FALSE;
             iEmpleadoActual := EmpleadoNumero;
             cdsEmpleados.DisableControls;
             FCambiosWorks:= FALSE;
//             InitAsignaciones;
             try
                ValidaEmpleado := TRUE;
                ZBaseEdicion_DevEx.ShowFormaEdicion( Forma, EdicionClass );
                Application.ProcessMessages;
                if not cdsEmpleados.Locate( 'CB_CODIGO', iEmpleadoActual, [] ) then
                   RefrescarFormaActiva := TRUE;
             finally
                cdsEmpleados.EnableControls;
                RefrescarFormaActiva := TRUE;
                ValidaEmpleado := FALSE;
             end;
        end;
        if FCambiosWorks then
        begin
{             with dmSuper do
             begin
                  if ( cdsAsignaciones.ChangeCount > 0 ) then
                     cdsAsignaciones.Enviar             // Este Enviar RefrescaMisEmpleados
                  else
                     RefrescaMisEmpleados;
             end;}
             TressShell.SetDataChange( [ enAusencia ] );
//             cdsCedulas.ResetDataChange;
        end;
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

{
procedure TdmLabor.LlenaListaCedEmp(const sArea: String; const lTodos: Boolean = FALSE );
begin
     try
        cdsCedEmp.DisableControls;
        with dmSuper.cdsEmpleados do
             if Active and ( not IsEmpty ) then
             begin
                  ValidaEmpleado := FALSE;
                  First;
                  while not EOF do
                  begin
                       if lTodos or ( FieldByName( 'CB_AREA' ).AsString = sArea ) then
                          cdsCedEmp.AppendRecord( [ cdsCedulas.FieldByName( 'CE_FOLIO' ).AsInteger,
                                                    FieldByName( 'CB_CODIGO' ).AsInteger,
                                                    0,   // Posicion ... Indefinida se calcula en el Servidor al Grabar
                                                    FieldByName( 'CB_PUESTO' ).AsString,
                                                    FieldByName( 'PRETTYNAME' ).AsString ] );
                       Next;
                  end;
                  ValidaEmpleado := TRUE;
             end;
     finally
        cdsCedEmp.EnableControls;
     end;
end;
}

procedure TdmLabor.DepurarListaEmpleados;
begin
     with cdsCedEmp do
     begin
          try
             DisableControls;
             First;
             while not EOF do
                   Delete;
          finally
             EnableControls;
          end;
     end;
end;

function TdmLabor.ShowMultiLote:Boolean;
var
   sOrden: String;
  // lMulti : Boolean;
begin
     sOrden := cdsCedulas.FieldByName( 'WO_NUMBER' ).AsString;
     with cdsCedMulti do
     begin
          if ( IsEmpty or ( RecordCount < 2 ) ) and StrLleno( sOrden ) then
          begin
               if IsEmpty then
                  Append
               else
                  Edit;
               FieldByName( 'WO_NUMBER' ).AsString := sOrden;
               FieldByName( 'CW_PIEZAS' ).AsFloat := cdsCedulas.FieldByName( 'CE_PIEZAS' ).AsFloat;
               Post;
               MergeChangeLog;
          end;
          ValidaLookups:= TRUE;
          try
             ZBaseGridEdicion_DevEx.ShowGridEdicion( GridMultiLote, TGridMultiLote, USE_CDSCEDMULTI );
          finally
             ValidaLookups := FALSE;
          end;
          Result := VerificaCambiosMultiLote;

     end;
end;

function TdmLabor.VerificaCambiosMultiLote:Boolean;
var
   lMulti : Boolean;
begin
     with cdsCedMulti do
     begin
          Result := ( RecordCount > 1 );
          if HuboCambiosMultiLote then
          begin
               lMulti := ( RecordCount > 1 );
               // Graba Cambios en cdsCedulas
               if not ( cdsCedulas.State in [ dsEdit, dsInsert ] ) then
                  cdsCedulas.Edit;
               cdsCedulas.FieldByName( 'CE_MULTI' ).AsString := zBoolToStr( lMulti );
               if IsEmpty then
               begin
                    cdsCedulas.FieldByName( 'WO_NUMBER' ).AsString := VACIO;
                    cdsCedulas.FieldByName( 'AR_CODIGO' ).AsString := VACIO;
                    cdsCedulas.FieldByName( 'CE_PIEZAS' ).AsFloat := 0;
               end
               else
               begin
                    First;
                    cdsCedulas.FieldByName( 'WO_NUMBER' ).AsString := FieldByName( 'WO_NUMBER' ).AsString;
                    cdsCedulas.FieldByName( 'AR_CODIGO' ).AsString := FieldByName( 'AR_CODIGO' ).AsString;
                    cdsCedulas.FieldByName( 'CE_PIEZAS' ).AsFloat := SumaPiezasMultiLote;//FieldByName( 'CW_PIEZAS' ).AsFloat;
                    if ( not lMulti ) then     // Si no es MultiLote Depurar
                       EmptyDataSet;
               end;
          end;
     end;
end;

function TdmLabor.SumaPiezasMultiLote:TDiasHoras;
var
   Suma : TDiasHoras;
begin
     with cdsCedMulti do
     begin
          First;
          Suma := 0;
          while not Eof do
          begin
               Suma := Suma + FieldByName('CW_PIEZAS').AsFloat;
               Next;
          end;
     end;
     Result := Suma;
end;

procedure TdmLabor.DepuraCedDataSets;
var
   Lista: TStrings;
begin
     Lista:= TStringList.Create;
     try
        ValidaEmpleado := FALSE;             // Para que no Verifique cdsAsignaciones si se borran registros
        RevisaDataSet( 'CB_CODIGO', cdsCedEmp, Lista );
        ValidaEmpleado := TRUE;
        if zStrToBool( cdsCedulas.FieldByName( 'CE_MULTI' ).AsString ) then
        begin
             RevisaDataSet( 'WO_NUMBER', cdsCedMulti, Lista );
             with cdsCedMulti do
             begin
                  if ( ChangeCount > 0 ) then
                  begin
                       Enviar;
                       VerificaCambiosMultiLote;
                  end;
             end;
        end;
     finally
        FreeAndNil( Lista );
     end;
end;

{
procedure TdmLabor.cdsAsignaAreasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsAsignaAreas do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
             with dmCliente do
             begin
                  Reconciliar( ServerLabor.GrabaAsignaAreas( Empresa, Delta, ErrorCount ) );
             end;
     end;
     dmSuper.RefrescaMisEmpleados;
end;
}

procedure TdmLabor.SetFiltroAreas( const lMisAreas: Boolean );
begin
     with cdsArea do
     begin
          Filtered := FALSE;
          if lMisAreas then
             Filter := MIS_AREAS
          else
             Filter := OTRAS_AREAS;
          Filtered := TRUE;
     end;
end;

procedure TdmLabor.cdsEmpAreasAlAdquirirDatos(Sender: TObject);
begin
     with cdsEmpAreas do
     begin
          Data := ServerLabor.GetEmpAreas( dmCliente.Empresa, FFechaArea, FHoraArea );
          LogChanges := FALSE;
     end;
end;

procedure TdmLabor.ShowAsignarAreas;
begin
     dmCliente.SetLookupEmpleado( eLookEmpAsignaArea );
     try
        ZBaseDlgModal_DevEx.ShowDlgModal( AsignarAreas, TAsignarAreas );
     finally
        dmCliente.ResetLookupEmpleado;
     end;
end;

function TdmLabor.AgregaOtroEmpleado( const sEmpleado: String ): Boolean;
var
   sDescripcion: String;

   function GetArea: String;
   begin
        with dmCliente do
             with cdsEmpleadoLookup do
             begin
                  if ( FFechaArea < FieldByName( 'CB_AR_FEC' ).AsDateTime ) or ( ( FFechaArea = FieldByName( 'CB_AR_FEC' ).AsDateTime )
                     and ( FHoraArea < HoraArea ) ) then
                     Result := ServerLabor.GetAreaKardex( Empresa, FieldByName( 'CB_CODIGO' ).AsInteger, FFechaArea, FHoraArea )
                  else
                     Result := FieldByName( 'CB_AREA' ).AsString;
             end;
   end;

begin
     try
        with dmCliente do
        begin
             Result := cdsEmpleadoLookUp.LookupKey( sEmpleado, VACIO, sDescripcion );
             if Result then
             begin
                  with cdsEmpAreas do
                  begin
                       Append;
                       FieldByName( 'CB_CODIGO' ).AsInteger := cdsEmpleadoLookUp.FieldByName( 'CB_CODIGO' ).AsInteger;
                       FieldByName( 'CB_TIPO' ).AsInteger := 2;   // C�digo de Prestado
                       FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookUp.FieldByName( 'PRETTYNAME' ).AsString;
                       FieldByName( 'CB_AREA' ).AsString := GetArea;
                       FieldByName( 'TIPOAREA' ).AsInteger := 0;     //No Afecta
                       FieldByName( 'CB_NIVEL' ).AsString := VACIO;  //No Afecta
                       Post;
                  end;
             end
             else
                 ZetaDialogo.ZError( VACIO, Format( 'No se Encontr� el Empleado %s', [ sEmpleado ] ), 0 );
        end;
     except
        ZetaDialogo.ZError( VACIO, 'Error al Agregar Empleado', 0 );
        Result := FAlSE;
     end;
end;

function TdmLabor.ChecaAreaAnterior( const iEmpleado: Integer; const sArea: String): String;
begin
     Result := sArea;
     with cdsEmpAreas do
          if Locate( 'CB_CODIGO', iEmpleado, [] ) then
             Result := FieldByName( 'CB_AREA' ).AsString;
end;

procedure TdmLabor.ReportaErrores( oData: OleVariant );
const
     K_NOMBRE_ARCHIVO = 'Bitacora_Asigna_Areas';
var
   oDataSet: TClientDataSet;
   sArchivo, sArea_Origen, sArea_Destino, sDirectorio: String;
   oAsciiFile: TAsciiLog;
   function Descrip_Area( sCampo: String ): String;
   begin
        with cdsArea do
        begin
              if Locate( 'TB_CODIGO', sCampo, [] ) then
                   Result := FieldByName('TB_ELEMENT').AsString
              else
                   Result := VACIO;
        end;
   end;
begin
     oDataSet := TClientDataSet.Create( Self );
     oAsciiFile := TAsciiLog.Create;
     try
        with oDataSet do
        begin
             Data := oData;
             sArchivo := GetTempFile( K_NOMBRE_ARCHIVO, 'txt'  );
             with oAsciiFile do
             begin
                  Init( sArchivo );
                  while not Eof do
                  begin
                       sArea_Origen := Descrip_Area( FieldByName('ANTERIOR').AsString );
                       sArea_Destino := Descrip_Area( FieldByName('CB_AREA').AsString );
                       WriteTexto( 'Empleado: '+FieldByName('CB_CODIGO').AsString);
                       WriteTexto(Format('Asignaci�n de %s: %s   A: %s', [GetLaborLabel( K_GLOBAL_LABOR_AREA ), sArea_Origen, sArea_Destino] ) );
                       WriteTexto('Error: '+FieldByName('Error').AsString);
                       Next;
                  end;
             end;
        end;
        if StrLleno( sArchivo ) then
        begin
             oAsciiFile.Close;
             sDirectorio := ExtractFileDir( sArchivo );
             if ZetaDialogo.ZConfirm('Error En La Asignaci�n Del Empleado','Desea Ver La Bit�cora De Errores?',0,mbYes) then
                ExecuteFile( sArchivo, VACIO, sDirectorio  );
             DeleteFile( sDirectorio + sArchivo );
        end;
     finally
            FreeAndNil( oAsciiFile );
            FreeAndNil( oDataSet );
     end;
end;

procedure TdmLabor.GrabaKardexAreas( Parametros: TZetaParams );
var
   ErrorCount : Integer;
   oData: OleVariant;
begin
     ErrorCount := 0;
     with cdsAsignaAreas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               oData := ServerLabor.GrabaAsignaAreas( dmCliente.Empresa, Data, Parametros.VarValues, ErrorCount );  // Si hay errores regresa un Dataset que contiene la lista de errores
               if ErrorCount > 0 then
                     ReportaErrores( oData );
          end;
     end;
end;

{--------------------cdsKarArea------------------------------------}
procedure TdmLabor.cdsKarAreaAlAdquirirDatos(Sender: TObject);
begin
     with dmSuper do
           cdsKarArea.Data := ServerLabor.GetKardexArea( dmCliente.Empresa,cdsDatosEmpleado.FieldByName('CB_CODIGO').AsInteger  );
end;

procedure TdmLabor.cdsKarAreaAlCrearCampos(Sender: TObject);
begin
     cdsArea.Conectar;
     with cdsKarArea do
     begin
          MaskTime( 'KA_HORA' );
          MaskTime( 'KA_HOR_MOV' );
//edit DevEx by MP: Mascaras de fecha para Datos Empleado
          MaskFecha('KA_FECHA');
          maskFecha('KA_FEC_MOV');
//fin edit
          CreateSimpleLookup( cdsArea, 'TB_ELEMENT', 'CB_AREA');
     end;
end;

{------------------- cdsAsignaAreas-----------------------------}
procedure TdmLabor.cdsAsignaAreasCB_AREA_DESCRIPGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
     if strVacio( cdsAsignaAreas.Fields[Sender.Tag].AsString ) then
         Text := 'Sin Asignar'
     else
         Text := Sender.AsString;
end;

{-------------------- cdsEmpBreaks --------------------------------}
procedure TdmLabor.cdsEmpBreaksAlCrearCampos(Sender: TObject);
begin
       with cdsEmpBreaks.FieldByName( 'CB_CODIGO' ) do
       begin
             OnValidate := CB_CODIGOCedulasValidate;
             OnChange := CB_CODIGOBreaksChange;
       end;
end;

procedure TdmLabor.CB_CODIGOBreaksChange( Sender: TField );
begin
     if ValidaEmpleado then
        with Sender, dmSuper, dmCliente do
        begin
             if ( AsInteger = EmpleadoNumero ) then
                  DataSet.FieldByName( 'PRETTYNAME' ).AsString := EmpleadoNombre
             else if ( AsInteger = cdsEmpleadoLookup.FieldByName( 'CB_CODIGO' ).AsInteger ) then
                  DataSet.FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
        end;
end;

procedure TdmLabor.cdsEmpBreaksAlAdquirirDatos(Sender: TObject);
var
   oDataSet: TClientDataset;
begin
     if FSoloAltas then
        cdsEmpBreaks.Data := ServerLabor.GetEmpBreaks( dmCliente.Empresa, Area, FechaArea, HoraArea, FSoloAltas )
     else
     begin
          oDataSet := TClientDataSet.create( Self );
          ValidaEmpleado := FALSE;
          try
             oDataSet.Data := ServerLabor.GetEmpBreaks( dmCliente.Empresa, Area, FechaArea, HoraArea, FSoloAltas );
             cdsEmpBreaks.EmptyDataSet;
             with oDataSet do
                  if not IsEmpty then
                  begin
                       while not EOF do
                       begin
                            cdsEmpBreaks.AppendRecord( [ FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'PRETTYNAME' ).AsString ] );
                            Next;
                       end;
                       cdsEmpBreaks.First;
                  end;
          finally
             ValidaEmpleado := TRUE;
             oDataSet.Free;
          end;
     end;
end;

procedure TdmLabor.cdsEmpBreaksAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsEmpBreaks do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerLabor.GrabaEmpBreaks( dmCliente.Empresa, Delta, FechaArea, HoraArea , Duracion, ErrorCount ) );
               if ErrorCount = 0 then
                  FCambiosWorks := True;
          end;
     end;
end;

{$ifdef VER130}
procedure TdmLabor.cdsEmpBreaksReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
    sError : String;
begin
    sError := GetErrorDescription( E );
    Action := cdsEmpBreaks.ReconciliaError(DataSet, UpdateKind, E, 'CB_CODIGO', sError );
end;
{$else}
procedure TdmLabor.cdsEmpBreaksReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
    sError : String;
begin
    sError := GetErrorDescription( E );
    Action := cdsEmpBreaks.ReconciliaError(DataSet, UpdateKind, E, 'CB_CODIGO', sError );
end;
{$endif}


procedure TdmLabor.RegistraBreaks;
var
   iEmpleadoActual : Integer;
begin
     Area := VACIO;
     FechaArea := dmCliente.FechaDefault;
     HoraArea := GetHoraDefault; //FormatDateTime( 'hhmm', Now );
     FSoloAltas := True;
     try
        cdsEmpBreaks.Refrescar;
     finally
        FSoloAltas := FALSE;
     end;
     with dmSuper do
     begin
          RefrescarFormaActiva := FALSE;
          iEmpleadoActual := EmpleadoNumero;
          cdsEmpleados.DisableControls;
          FCambiosWorks:= FALSE;
       try
          ValidaEmpleado := TRUE;
          ZBaseGridEdicion_DevEx.ShowGridEdicion( RegistroBreak_DevEx, TRegistroBreak_DevEx, TRUE );
          {***}
          Application.ProcessMessages;
          if not cdsEmpleados.Locate( 'CB_CODIGO', iEmpleadoActual, [] ) then
             RefrescarFormaActiva := TRUE;
       finally
          FSoloAltas := False;
          cdsEmpleados.EnableControls;
          RefrescarFormaActiva := TRUE;
          ValidaEmpleado := FALSE;
       end;
       if FCambiosWorks then
             TressShell.SetDataChange( [ enWorks ] );
     end;
end;

procedure TdmLabor.CancelarBreaksGetLista(Parametros: TZetaParams);
begin
     cdsDataset.Data := ServerLabor.CancelarBreaksGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmLabor.GetHoraDefault: string;
var
   sHoraActual: string;
begin
     Result := FormatDateTime( 'hhmm', Now );
     cdsArea.Conectar;
     with FAreaData do
     begin
          if HayPrimeraHora then
          begin
               sHoraActual := Result;
               case Shift of
                    tlNocturnoEntrada:
                    begin
                         if ( dmCliente.FechaSupervisor <> Date ) then
                         begin
                              if ( sHoraActual > PrimeraHora ) then
                                 Result := sHoraActual
                              else
                                  Result := aHoraString( aMinutos( sHoraActual ) + ZetaCommonClasses.K_24HORAS );
                         end;
                    end;
                    tlNocturnoSalida:
                    begin
                         if ( dmCliente.FechaSupervisor = Date ) then
                         begin
                              if ( sHoraActual < PrimeraHora ) then
                                 Result := aHoraString( aMinutos( sHoraActual ) + ZetaCommonClasses.K_24HORAS )
                              else
                                  Result := sHoraActual;
                         end;
                    end;
               end;
          end;
     end;
end;

function TdmLabor.CancelarBreaks(Parametros: TZetaParams; const lVerificacion: Boolean; SetAnimacion : TProcAnimacion ): Boolean;
var
   Resultado : OleVariant;
begin
     SetAnimacion( TRUE );
     try
        if lVerificacion then
        begin
             cdsDataSet.MergeChangeLog;
             Resultado := ServerLabor.CancelarBreaksLista( dmCliente.Empresa, cdsDataSet.Data, Parametros.VarValues );
        end
        else
             Resultado := ServerLabor.CancelarBreaks( dmCliente.Empresa, Parametros.VarValues );
        Result := Check( Resultado );
     finally
        SetAnimacion( FALSE );
     end;
end;

procedure TdmLabor.cdsKardexDiarioAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsKardexDiario.Data := ServerLabor.GetKardexDiario( Empresa, dmSuper.EmpleadoNumero, FechaSupervisor );
end;

procedure TdmLabor.cdsKardexDiarioAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsKardexDiario do
     begin
          if State in [ dsInsert, dsEdit ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerLabor.GrabaKardexArea( dmCliente.Empresa, Delta, ErrorCount ) );
               cdsKardexDiario.Refrescar;
          end;
     end;
end;

procedure TdmLabor.cdsKardexDiarioAlCrearCampos(Sender: TObject);
begin
     with cdsKardexDiario do
     begin
          cdsArea.Conectar;     
          MaskTime( 'KA_HORA' );
          MaskTime( 'KA_HOR_FIN' );
          MaskTime( 'KA_HOR_MOV' );          
          MaskFecha( 'KA_FECHA' );
          MaskFecha( 'KA_FEC_MOV' );
          CreateSimpleLookup( cdsArea, 'TB_ELEMENT', 'CB_AREA' );          
          FieldByName( 'CB_AREA' ).OnGetText := CB_AREAGetText;
     end;
end;

procedure TdmLabor.CB_AREAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   iTipo: integer;
begin
     iTipo := Sender.DataSet.FieldByName( 'CH_TIPO' ).AsInteger;
     if ( iTipo <> 0 ) then
        Text := ZetaCommonLists.ObtieneElemento( lfTipoChecadas, iTipo )
     else
         Text := Sender.AsString;
end;

procedure TdmLabor.cdsKardexDiarioAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditKardexArea_DevEx, TEditKardexArea_DevEx );
end;

procedure TdmLabor.cdsKardexDiarioAfterDelete(DataSet: TDataSet);
begin
     cdsKardexDiario.Enviar;
end;

procedure TdmLabor.cdsKardexDiarioBeforePost(DataSet: TDataSet);
begin
     with TClientDataSet( DataSet ) do
     begin
          if ( StrVacio(FieldByName( 'KA_HORA' ).AsString) ) or ( FieldByName( 'KA_HORA' ).AsString = '0000' ) then
             DataBaseError( '� Hora de Transferencia no Puede Quedar Vac�a !' );
          with dmCliente do
          begin
               FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
               FieldByName( 'KA_FEC_MOV' ).AsDateTime := Date;
          end;
          FieldByName( 'KA_HOR_MOV' ).AsString := FormatDateTime( 'hhmm', Time );
     end;
end;

procedure TdmLabor.cdsKardexDiarioNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger := dmSuper.EmpleadoNumero;
          FieldByName( 'KA_FECHA' ).AsDateTime := dmCliente.FechaSupervisor;
          FieldByName( 'KA_HORA' ).AsString := FormatDateTime( 'hhmm', Time );
     end;
end;

{$ifdef VER130}
procedure TdmLabor.cdsKardexDiarioReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmLabor.cdsKardexDiarioReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmLabor.cdsCedulasReconcileError(DataSet: TCustomClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;
          var Action: TReconcileAction);
var
   sError: String;
begin
     if UK_Violation( E ) then
     begin
          with DataSet do
          begin
               FOperacion:= ocReportar;
               if StrLLeno(Global.GetGlobalString( K_GLOBAL_LABOR_ORDEN ))then
               begin
                    sError := Format( 'Ya existen tiempos capturados para %s : %s ',[Global.GetGlobalString( K_GLOBAL_LABOR_ORDEN ),FieldByName('WO_NUMBER').AsString]);
               end
               else
               if StrLLeno(Global.GetGlobalString( K_GLOBAL_LABOR_PARTE ))then
               begin
                    sError := Format( 'Ya existen tiempos capturados para %s : %s ',[Global.GetGlobalString( K_GLOBAL_LABOR_PARTE ),FieldByName('AR_CODIGO').AsString]);
               end
               else
               if StrLLeno(Global.GetGlobalString( K_GLOBAL_LABOR_AREA ))then
               begin
                    sError := Format( 'Ya existen tiempos capturados para %s : %s ',[Global.GetGlobalString( K_GLOBAL_LABOR_AREA ),FieldByName('CE_AREA').AsString]);
               end;
               DatabaseError(sError);
               Action := raCancel;
          end;
     end
     else
     begin
          {***DevEx (by am): Agredado para desplegar el mensaje indicado segun la excepcion obtenida. Antes no hacia nada y dejaba la pantalla igual, puede checarse en la vs 2013 en la pantalla de Operaciones Registradas ***}
         //sError := GetErrorDescription( E ); //old
         Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     end;
   // cdsValuaciones.ReconciliaError(DataSet,UpdateKind,E,'VP_FOLIO', sError );
end;

procedure TdmLabor.GetPlantillasAsignadas;
var
   Lay:TLayout;
begin
     PlantillaAsignadas := TList.Create;
     with cdsDataSet do
     begin
          Data := ServerLabor.GetLayoutsSuper(dmCliente.Empresa,dmCliente.Usuario );
          First;
          PlantillaAsignadas.Clear; 
          while not Eof do
          begin
               Lay := TLayout.Create;
               Lay.Codigo := FieldByName('LY_CODIGO').AsString;
               Lay.Area :=   FieldByName('CB_AREA').AsString;
               Lay.LayNombre  :=   FieldByName('LY_NOMBRE').AsString;
               Lay.AreaDescrip:=   FieldByName('TB_ELEMENT').AsString;
               PlantillaAsignadas.Add(Lay);
               Next;
          end;
     end;
end;

procedure TdmLabor.cdsLayMaquinasAlAdquiriratos(Sender: TObject);
var
   oSillasLay,oSillasMaquin,oCertMaq:Olevariant;
begin
     cdsLayMaquinas.Data := ServerLabor.GetMapa(dmCliente.Empresa,LayoutActual.Codigo,oSillasLay,oSillasMaquin,oCertMaq);
     cdsSillasLayout.Data := oSillasLay;
     cdsSillasMaquina.Data := oSillasMaquin;
     cdsCertificMaq.Data := oCertMaq;
end;

procedure TdmLabor.cdsMaquinasAlAdquirirDatos(Sender: TObject);
begin
     cdsMaquinas.Data := ServerLabor.GetCatalogo(dmCliente.Empresa,cdsMaquinas.Tag );
end;

procedure TdmLabor.AsignarEmpleado(Empleado:Integer;Maquina:string);
var
   oZParams :TZetaParams;
begin
     oZParams:= TZetaParams.Create;
     with oZParams do
     begin
          AddString('SL_CODIGO',Maquina);
          AddInteger('CB_CODIGO',Empleado);
          AddString('LY_CODIGO',LayoutActual.Codigo);
          AddString('EM_HORA', GetHoraDefault);
          AddString('EM_HOR_MOV', {$ifdef DOS_CAPAS}FormatDateTime( 'hhmmss', Now ){$else}GetHoraDefault{$endif});
          AddDate('EM_FECHA',dmCliente.FechaDefault );
          AddDate('EM_FEC_MOV',Date);
          AddInteger('US_CODIGO',dmCliente.Usuario );
     end;
     ServerLabor.AsignaEmpleado(dmCliente.Empresa,oZParams.VarValues);
end;

procedure TdmLabor.cdsKarEmpMaqAlAdquirirDatos(Sender: TObject);
begin
     cdsKarEmpMaq.Data :=  ServerLabor.GetMaquinaEmpleados(dmCliente.Empresa,LayoutActual.Codigo,GetHoraDefault,dmCliente.FechaDefault);
end;

procedure TdmLabor.cdsCertificEmpleadosAlAdquirirDatos(Sender: TObject);
begin
     cdsCertificEmpleados.Data := ServerLabor.GetCertificacionesEmpleados(dmCliente.Empresa,dmCliente.Usuario,dmCliente.FechaDefault);
end;

procedure TdmLabor.GetCertificaciones(Maquina:string);
begin
     with cdsCertificMaq do
     begin
          Filtered := False;
          Filter   := Format('MQ_CODIGO = ''%s''',[ Maquina ] );
          Filtered := True;
     end;
     //cdsCertificMaq.Data := ServerLabor.GetCertificaciones(dmCliente.Empresa,Maquina);
end;

function TdmLabor.EstaCertificado (Empleado:Integer;Certificaciones:TList):Boolean;
var
   i:Integer;
begin
     Result:= True;
     if Certificaciones <> nil then
     begin
           for i := 0 to Certificaciones.Count - 1  do
           begin
                Result := cdsCertificEmpleados.Locate('CB_CODIGO;CI_CODIGO',VarArrayOf([Empleado,TCertificacion(Certificaciones[i]).Codigo]),[]);
                if not Result then
                   break
           end;
     end;
end;

procedure TdmLabor.cdsKarMaquinasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsKarMaquinas.Data := ServerLabor.GetKardexEmpleadoMaquina(Empresa,Empleado);
     end;
end;

procedure TdmLabor.cdsKarMaquinasAlCrearCampos(Sender: TObject);
begin
     with cdsKarMaquinas do
     begin
          CreateSimpleLookup( cdsMaquinas, 'MQ_NOMBRE', 'MQ_CODIGO' );
     end;
end;

procedure TdmLabor.cdsCertificMaqAlCrearCampos(Sender: TObject);
begin
      with dmCatalogos do
      begin
           cdsCertificaciones.Conectar;
           with cdsCertificMaq do
           begin
                CreateSimpleLookup( cdsCertificaciones, 'CI_DESCRIP', 'CI_CODIGO' );
           end;
      end;
end;

procedure TdmLabor.cdsLayoutsAsignadosAlAdquirirDatos(Sender: TObject);
begin
     cdsLayoutsAsignados.Data := ServerLabor.GetLayoutsSuper(dmCliente.Empresa,dmCliente.Usuario );
end;

procedure TdmLabor.cdsLayoutsAsignadosAlCrearCampos(Sender: TObject);
begin
     cdsArea.Conectar;
     with cdsLayoutsAsignados do
     begin
          CreateSimpleLookup( cdsArea, 'LY_AREA_TXT', 'LY_AREA' );
     end;
end;

procedure TdmLabor.cdsLayoutsAsignadosGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmLabor.cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

function TdmLabor.GetMaquinaGlobal:string;
begin
     Result := Global.GetGlobalString( K_GLOBAL_LABOR_MAQUINA );
end;

function TdmLabor.GetLayoutGlobal:string;
begin
     Result := Global.GetGlobalString( K_GLOBAL_LABOR_PLANTILLA );
end;



procedure TdmLabor.cdsMaquinasNewRecord(DataSet: TDataSet);
begin
     with cdsMaquinas do
     begin
          FieldByName('MQ_MULTIP').AsString := K_GLOBAL_NO;
     end;
end;

end.
