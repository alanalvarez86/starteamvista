unit DCliente;

interface

{$DEFINE REPORTING}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,  dxSkinsForm,
     DBaseCliente,
{$ifdef DOS_CAPAS}
     DServerGlobal,
     DServerConsultas,
     DServerCatalogos,
     DServerSistema,
     DServerTablas,
     DServerReportes,
   {$IFDEF REPORTING}
     DServerReporting,
   {$else}
     DServerCalcNomina,
   {$endif}
     DServerAsistencia,
     DServerNomina,
     DServerRecursos,
     DServerSuper,
     DServerLabor,
{$endif}
     ZetaCommonLists, ZetaCommonClasses,
     ZetaClientDataSet;

{$define QUINCENALES}
{.$undefine QUINCENALES}
     
type
  TdmCliente = class(TBaseCliente)
    cdsPeriodo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FFechaSupervisor: TDate;
    FPeriodoTipo: eTipoPeriodo;
    FPeriodoNumero: Integer;
    function GetFechaSupervisor: TDate;
    procedure SetFechaSupervisor(const Value: TDate);
    procedure InicializaPeriodo;
    procedure SetPeriodoTipo(const Value: eTipoPeriodo);
    function GetFechaAsistencia: TDate;
    function GetIMSSPatron: String;
    function GetIMSSYear: Integer;
    function GetIMSSMes: Integer;
    function GetIMSSTipo: eTipoLiqIMSS;
    function FetchNextPeriod(const iNumero: Integer): Boolean;
    function FetchPeriod(const iNumero: Integer): Boolean;
    function FetchPreviousPeriod(const iNumero: Integer): Boolean;
    procedure SetPeriodo(const Value: Integer);
    function GetEmpleado: integer;
  protected
    { Protected declarations }
{$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    FServerTablas: TdmServerTablas;
    FServerReportes : TdmServerReportes;
    {$ifdef REPORTING}
    FServerReporteador: TdmServerReporting;
    {$else}
    FServerCalcNomina: TdmServerCalcNomina;
    {$endif}
    FServerAsistencia: TdmServerAsistencia;
    FServerNomina: TdmServerNomina;
    FServerRecursos: TdmServerRecursos;
    FServerSuper: TdmServerSuper;
    FServerLabor: TdmServerLabor;
{$endif}
    procedure InitCacheModules; override;
    function PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                        const iEmpleado: integer;
                                        const eTipo: eStatusPeriodo;
                                        const lPuedeModificar: Boolean;
                                        var sMensaje: string): Boolean;override;
    {***DevEx(@am): Metodos virtuales para poder compilar unidades utilizadas en varios proyectos ***}
    function GetSkinController: TdxSkinController; override;
    {***}
  public
    { Public declarations }
    property FechaSupervisor: TDate read GetFechaSupervisor write SetFechaSupervisor;
    property FechaAsistencia: TDate read GetFechaAsistencia;
    property PeriodoTipo: eTipoPeriodo read FPeriodoTipo write SetPeriodoTipo;
    property PeriodoNumero: Integer read FPeriodoNumero write SetPeriodo;
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    property Empleado: integer read GetEmpleado;
{$ifdef DOS_CAPAS}
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerTablas: TdmServerTablas read FServerTablas;
    property ServerAsistencia: TdmServerAsistencia read FServerAsistencia;
    property ServerNomina: TdmServerNomina read FServerNomina;
    property ServerReportes: TdmServerReportes read FServerReportes;
    {$ifdef REPORTING}
    property ServerReporteador: TdmServerReporting read FServerReporteador;
    {$else}
    property ServerCalcNomina: TdmServerCalcNomina read FServerCalcNomina;
    {$endif}
    property ServerRecursos: TdmServerRecursos read FServerRecursos;
    property ServerSuper: TdmServerSuper read FServerSuper;
    property ServerLabor: TdmServerLabor read FServerLabor;
{$endif}
    procedure CierraEmpresa; override;
    procedure InitActivosSupervisor;
    procedure InitActivosPeriodo;
    procedure RefrescaPeriodo;
    function GetListaEmpleados( const sFiltro : string ): string;override;
    function SetPeriodoNumero(const Value: Integer): Boolean;
    function GetPeriodoInicial: Boolean;
    function GetPeriodoAnterior: Boolean;
    function GetPeriodoSiguiente: Boolean;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetEmpleadoDescripcion: String;
    function GetAsistenciaDescripcion: String;
    function GetSegundosTimeout: Integer; override;
    //function GetPeriodoDescripcion: String;
    //function GetIMSSDescripcion: String;
    //function GetSistemaDescripcion: String;
    procedure CargaActivosIMSS(Parametros: TZetaParams);
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);
    function GetValorActivoStr(const eTipo: TipoEstado): String; override;
    procedure CargaActivosTodos(Parametros: TZetaParams);override;
    procedure SetEmpleadoNumero( const Value: TNumEmp );
    function PuedeCambiarTarjeta( const dInicial, dFinal: TDate; const iEmpleado: integer; var sMensaje: string ):Boolean;overload;override;
  end;

var
  dmCliente: TdmCliente;

implementation

uses dSuper,
     dGlobal,
     dTablas,
     dCatalogos,
     dDiccionario,
     ZReportTools,
     ZetaCommonTools,
     ZGlobalTress,
     ZetaRegistryCliente, FTressShell;

{$R *.DFM}

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     ModoSuper := TRUE;
     CaptionLogoff := '&Otro Supervisor';
{$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     FServerTablas := TdmServerTablas.Create( Self );
     FServerAsistencia := TdmServerAsistencia.Create( Self );
     FServerNomina := TdmServerNomina.Create( self );
     FServerReportes := TdmServerReportes.Create( self );
     {$ifdef REPORTING}
     FServerReporteador := TdmServerReporting.Create( self );
     {$else}
     FServerCalcNomina := TdmServerCalcNomina.Create( self );
     {$endif}
     FServerRecursos := TdmServerRecursos.Create( self );
     FServerSuper := TdmServerSuper.Create( self );
     FServerLabor := TdmServerLabor.Create( self );
{$endif}
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerLabor );
     FreeAndNil( FServerSuper );
     FreeAndNil( FServerRecursos );
     {$ifdef REPORTING}
     FreeAndNil( FServerReporteador );
     {$else}
     FreeAndNil( FServerCalcNomina );
     {$endif}
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerNomina );
     FreeAndNil( FServerAsistencia );
     FreeAndNil( FServerTablas );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerCatalogos );
{$endif}
     inherited;
end;

procedure TdmCliente.CierraEmpresa;
begin
     ClientRegistry.TipoNomina := Integer(FPeriodoTipo);
     inherited CierraEmpresa;
     cdsPeriodo.Close;
end;

procedure TdmCliente.InitCacheModules;
begin
     inherited;
     DataCache.AddObject( 'Tabla', dmTablas );
     DataCache.AddObject( 'Cat�logo', dmCatalogos );
     DataCache.AddObject( 'Diccionario', dmDiccionario );
end;

procedure TdmCliente.InitActivosSupervisor;
begin
     FechaSupervisor := Now;
end;

function TdmCliente.GetFechaSupervisor: TDate;
begin
     Result := Trunc( FFechaSupervisor );
end;

procedure TdmCliente.SetFechaSupervisor(const Value: TDate);
begin
     if ( FFechaSupervisor <> Value ) then
     begin
          FFechaSupervisor := Value;
          FechaDefault := Value;
          YearDefault := TheYear( Value )
     end;
end;

procedure TdmCliente.InitActivosPeriodo;
begin
     FPeriodoTipo := eTipoPeriodo(ClientRegistry.TipoNomina);
     InicializaPeriodo;
end;

procedure TdmCliente.SetPeriodoTipo(const Value: eTipoPeriodo);
begin
     FPeriodoTipo := Value;
     InicializaPeriodo;
end;

function TdmCliente.GetFechaAsistencia: TDate;
begin
     Result := FFechaSupervisor;
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := VACIO;          // PENDIENTE : Se Requiere Consultar el Catalogo ???
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := TheYear( FFechaSupervisor );
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := TheMonth( FFechaSupervisor );
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

function TdmCliente.GetEmpleadoDescripcion: String;
begin
     with dmSuper do
     begin
          Result := IntToStr( EmpleadoNumero ) + ': '+ EmpleadoNombre;
     end;
end;

function TdmCliente.GetAsistenciaDescripcion: String;
begin
     Result := FechaCorta( FechaAsistencia );
end;

{
function TdmCliente.GetPeriodoDescripcion: String;
begin
     Result := ObtieneElemento( lfTipoNomina, Ord( FPeriodoTipo ) ) + ': ' + IntToStr( FPeriodoNumero );
end;

function TdmCliente.GetIMSSDescripcion: String;
begin
     Result := 'Patr�n ' +
               IMSSPatron +
               ', ' +
               ShortMonthNames[ IMSSMes ] +
               ' / ' +
               IntToStr( IMSSYear ) +
               ', ' +
               ObtieneElemento( lfTipoLiqIMSS, Ord( IMSSTipo ) );
end;


function TdmCliente.GetSistemaDescripcion: String;
begin
     Result := 'A�o: ' + IntToStr( YearDefault );
end;
}

{ ******* Conexiones y Navegaci�n para Per�odos ********** }

procedure TdmCliente.InicializaPeriodo;
begin
     if GetPeriodoInicial then
     begin
          FPeriodoNumero := cdsPeriodo.FieldByName( 'PE_NUMERO' ).AsInteger;
     end
     else
     begin
          FPeriodoNumero := 0;
     end;
end;

function TdmCliente.GetPeriodoInicial: Boolean;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, YearDefault, Ord( PeriodoTipo ) );
          Result := not IsEmpty;
     end;
end;

procedure TdmCliente.RefrescaPeriodo;
begin
     FetchPeriod( FPeriodoNumero );
end;

function TdmCliente.SetPeriodoNumero(const Value: Integer): Boolean;
begin
     if ( FPeriodoNumero <> Value ) then
     begin
          Result := FetchPeriod( Value );
          if Result then
          begin
               with cdsPeriodo do
               begin
                    FPeriodoNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
               end;
          end;
     end
     else
         Result := True;
end;

function TdmCliente.GetPeriodoAnterior: Boolean;
begin
     Result := FetchPreviousPeriod( FPeriodoNumero );
     if Result then
     begin
          with cdsPeriodo do
          begin
               FPeriodoNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
          end;
     end;
end;

function TdmCliente.GetPeriodoSiguiente: Boolean;
begin
     Result := FetchNextPeriod( FPeriodoNumero );
     if Result then
     begin
          with cdsPeriodo do
          begin
               FPeriodoNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
          end;
     end;
end;

function TdmCliente.FetchPeriod( const iNumero: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetPeriodo( Empresa, YearDefault, Ord( PeriodoTipo ), iNumero, Datos );
     with cdsPeriodo do
     begin
          Data := Datos;
     end;
end;

function TdmCliente.FetchPreviousPeriod( const iNumero: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetPeriodoAnterior( Empresa, YearDefault, Ord( PeriodoTipo ), iNumero, Datos );
     if Result then
     begin
          with cdsPeriodo do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchNextPeriod( const iNumero: Integer ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetPeriodoSiguiente( Empresa, YearDefault, Ord( PeriodoTipo ), iNumero, Datos );
     if Result then
     begin
          with cdsPeriodo do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          Year := YearDefault;
          Tipo := FPeriodoTipo;
          Numero := FPeriodoNumero;
          with cdsPeriodo do
          begin
               {$ifdef QUINCENALES}
               InicioAsis := FieldByName( 'PE_ASI_INI' ).AsDateTime;
               FinAsis := FieldByName( 'PE_ASI_FIN' ).AsDateTime;
               {$endif}
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;

procedure TdmCliente.CargaActivosSistema(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddDate('FechaAsistencia', FechaAsistencia );
          AddDate('FechaDefault', FechaDefault );
          AddInteger('YearDefault', YearDefault );
          AddInteger( 'EmpleadoActivo', dmSuper.EmpleadoNumero );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddInteger( 'Year', YearDefault );
          AddInteger( 'Tipo', Ord( PeriodoTipo ) );
          AddInteger( 'Numero', PeriodoNumero );
     end;
end;

procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', IMSSPatron );
          AddInteger( 'IMSSYear', IMSSYear );
          AddInteger( 'IMSSMes', IMSSMes );
          AddInteger( 'IMSSTipo', Ord( IMSSTipo ) );
     end;
end;

procedure TdmCliente.SetPeriodo(const Value: Integer);
begin
     if ( FPeriodoNumero <> Value ) AND
        FetchPeriod( Value ) then
        with cdsPeriodo do
        begin
             FPeriodoNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
        end;
end;


function TdmCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     case eTipo of
          stEmpleado: Result := GetEmpleadoDescripcion;
          stFecha: Result := GetAsistenciaDescripcion;
     end;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS(Parametros);
     CargaActivosPeriodo(Parametros);
     CargaActivosSistema(Parametros);
end;

procedure TdmCliente.SetEmpleadoNumero(const Value: TNumEmp);
begin
     FEmpleado := Value;
end;

function TdmCliente.GetListaEmpleados( const sFiltro : string ): string;
begin
     Result := ConstruyeListaEmpleados( dmSuper.cdsEmpleados, sFiltro );
end;

{
function TdmCliente.GetListaEmpleados( const sFiltro : string ): string;
 var iCodigo : integer;
begin
     Result := '';
     with dmSuper.cdsEmpleados do
     begin
          DisableControls;
          try
             if IsEmpty then
                Raise Exception.Create('Supervisor no Tiene Lista de Empleados');
             iCodigo := FieldByName('CB_CODIGO').AsInteger;
             First;
             while NOT EOF do
             begin
                  Result := ConcatString( Result, FieldByName('CB_CODIGO').AsString, ',' );
                  Next;
             end;
             if StrLleno(Result) then
                Result := sFiltro + ' IN ('+Result+')';
             Locate('CB_CODIGO', VarArrayOf([iCodigo]), [] );
          finally
                 EnableControls;
          end;
     end;
end;
}
function TdmCliente.GetSegundosTimeout: Integer;
begin
     Result := Global.GetGlobalInteger(K_SUPER_SEGUNDOS);
end;

function TdmCliente.GetEmpleado: integer;
begin
     Result := dmSuper.EmpleadoNumero;
end;

function TdmCliente.PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                               const iEmpleado: integer;
                                               const eTipo: eStatusPeriodo;
                                               const lPuedeModificar: Boolean;
                                               var sMensaje: string ): Boolean;
begin
     Result := dmSuper.PuedeCambiarTarjetaStatus( dInicial, iEmpleado, eTipo, lPuedeModificar, sMensaje );
end;

function TdmCliente.PuedeCambiarTarjeta(const dInicial, dFinal: TDate; const iEmpleado: integer; var sMensaje: string): Boolean;
begin
     Result:= dmSuper.PuedeCambiarTarjeta(dInicial, dFinal, iEmpleado, sMensaje);
end;

function TdmCliente.GetSkinController: TdxSkinController;
begin
     Result:= TressShell.DevEx_SkinController;
end;

end.
