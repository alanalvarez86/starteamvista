unit FEditPermisos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, Mask, DBCtrls, Db, Buttons,
  ExtCtrls,ZetaDBTextBox, ZetaKeyCombo, ZetaNumero,
  ZetaFecha, ZetaSmartLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TEditPermisos = class(TBaseEdicion_DevEx)
    LIN_FEC_INI: TLabel;
    LIN_DIAS: TLabel;
    LIN_FEC_FIN: TLabel;
    Label13: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    UsuarioLbl: TLabel;
    PM_COMENTA: TDBEdit;
    PM_TIPO: TZetaDBKeyLookup_DevEx;
    PM_FEC_INI: TZetaDBFecha;
    PM_DIAS: TZetaDBNumero;
    PM_NUMERO: TDBEdit;
    PM_CAPTURA: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    PM_FEC_FIN: TZetaDBTextBox;
    US_DESCRIP: TZetaDBTextBox;
    PM_CLASIFI: TZetaDBKeyCombo;
    PM_GLOBAL: TDBCheckBox;
    Label1: TLabel;
    PM_FEC_REG: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure PM_CLASIFIChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FiltroPermiso :string;
    procedure SetFiltros;
  protected
    procedure Connect;override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  EditPermisos: TEditPermisos;

implementation

uses  dSuper, DTablas, DSistema, ZAccesosTress, ZetaCommonLists, ZetaCommonClasses,ZetaCommonTools, ZGlobalTress, DGlobal;

{$R *.DFM}

procedure TEditPermisos.FormCreate(Sender: TObject);
begin
     inherited;
     FiltroPermiso := 'TB_INCIDEN ='+ IntToStr( Ord( eiPermiso ) );
     PM_TIPO.Filtro := FiltroPermiso;
     HelpContext:= 11513;
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     IndexDerechos := ZAccesosTress.D_SUPER_PERMISOS;
     FirstControl := PM_FEC_INI;
     PM_TIPO.LookupDataset := dmTablas.cdsIncidencias;
end;

procedure TEditPermisos.SetFiltros;
begin
     PM_TIPO.Filtro := ConcatFiltros( FiltroPermiso ,Format('TB_PERMISO = -1 OR TB_PERMISO = %d',[ PM_CLASIFI.Valor ] ) );
end;

procedure TEditPermisos.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;

     with dmSuper do
     begin
          cdsHisPermisos.Conectar;
          Datasource.DataSet := cdsHisPermisos;
     end;
    SetFiltros;
end;

function TEditPermisos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje ) and dmSuper.IncidenciaValida( 'Borrar', sMensaje );
end;

function TEditPermisos.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje ) and dmSuper.IncidenciaValida( 'Modificar', sMensaje );
end;

procedure TEditPermisos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with DataSource, dmSuper do
         if ( Field = nil ) and ( Modo = dsBrowse ) then
            AutoEdit := CheckDerechos( K_DERECHO_CAMBIO ) and
                        DerechoTipoPermiso( Dataset.FieldByName( 'PM_CLASIFI' ).AsInteger );
                        
     if ( Field = nil )then
        SetFiltros;
end;

procedure TEditPermisos.PM_CLASIFIChange(Sender: TObject);
begin
     inherited;
     SetFiltros;
     PM_TIPO.SetLlaveDescripcion(VACIO,VACIO);
end;

procedure TEditPermisos.FormShow(Sender: TObject);
begin
  inherited;
     if Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
     begin
          PM_FEC_FIN.Visible := false;
          PM_FEC_REG.Visible := true;
          PM_FEC_REG.Left := 104;    
          LIN_FEC_FIN.Caption := 'Fecha Regreso:';
          LIN_FEC_FIN.Left := 23;
     end
     else
     begin
          PM_FEC_FIN.Visible := true;
          PM_FEC_REG.Visible := false;
          PM_FEC_FIN.Left := 104;
          LIN_FEC_FIN.Caption := 'Regresa:';
          LIN_FEC_FIN.Left := 56;
     end;
end;

end.



