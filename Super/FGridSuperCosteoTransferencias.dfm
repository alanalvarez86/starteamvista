inherited GridSuperCosteoTransferencias: TGridSuperCosteoTransferencias
  Left = 630
  Top = 154
  Caption = 'Registro de Transferencias'
  ClientHeight = 521
  ClientWidth = 481
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 485
    Width = 481
    inherited OK_DevEx: TcxButton
      Left = 312
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 394
      Top = 5
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 481
    inherited ValorActivo2: TPanel
      Width = 155
      inherited textoValorActivo2: TLabel
        Width = 149
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 186
    Width = 481
    Height = 299
    TabOrder = 4
    OnTitleClick = ZetaDBGridTitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 277
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TR_HORAS'
        Title.Caption = 'Horas'
        Visible = True
      end>
  end
  object PanelCurso: TPanel [3]
    Left = 0
    Top = 50
    Width = 481
    Height = 136
    Align = alTop
    TabOrder = 8
    object lbDestino: TLabel
      Left = 86
      Top = 13
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Transferir a:'
      FocusControl = CC_CODIGO
    end
    object MotivoLbl: TLabel
      Left = 107
      Top = 61
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Motivo:'
      FocusControl = TR_MOTIVO
    end
    object Label2: TLabel
      Left = 68
      Top = 85
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Observaciones:'
    end
    object TipoAutLbl: TLabel
      Left = 118
      Top = 110
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object Label1: TLabel
      Left = 84
      Top = 37
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Aprobar por:'
      FocusControl = TR_APRUEBA
    end
    object CC_CODIGO: TZetaKeyLookup_DevEx
      Left = 144
      Top = 9
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsNivel1
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnExit = CC_CODIGOExit
    end
    object TR_MOTIVO: TZetaKeyLookup_DevEx
      Left = 144
      Top = 57
      Width = 300
      Height = 21
      LookupDataset = dmTablas.cdsMotivoTransfer
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
    end
    object TR_TEXTO: TZetaEdit
      Left = 144
      Top = 81
      Width = 300
      Height = 21
      TabOrder = 3
    end
    object TR_TIPO: TZetaKeyCombo
      Left = 144
      Top = 106
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 4
      ListaFija = lfTipoHoraTransferenciaCosteo
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object TR_APRUEBA: TZetaKeyLookup_DevEx
      Left = 144
      Top = 33
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 2
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
