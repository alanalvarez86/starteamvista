inherited KardexGeneral: TKardexGeneral
  Left = 719
  Top = 231
  Caption = 'Kardex General'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl_DevEx: TcxPageControl
    inherited General_DevEx: TcxTabSheet
      object MontoLbl: TLabel
        Left = 268
        Top = 42
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto:'
      end
      object CB_TIPOlbl: TLabel
        Left = 37
        Top = 18
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object CB_TIPO: TZetaDBKeyLookup_DevEx
        Left = 68
        Top = 14
        Width = 353
        Height = 21
        Filtro = 'TB_SISTEMA='#39'N'#39
        LookupDataset = dmTablas.cdsMovKardex
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TIPO'
        DataSource = DataSource
      end
      object CB_MONTO: TZetaDBNumero
        Left = 306
        Top = 38
        Width = 115
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'CB_MONTO'
        DataSource = DataSource
      end
    end
    inherited Notas_DevEx: TcxTabSheet
      inherited CB_NOTA: TcxDBMemo
        Properties.ScrollBars = ssVertical
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 104
    Top = 216
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 14680296
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
