program Supervisores;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  DSuper in 'DSuper.pas' {dmSuper: TDataModule},
  ZBaseDlgModal_DevEx in '..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  DCatalogos in 'DCatalogos.pas' {dmCatalogos: TDataModule},
  DTablas in 'DTablas.pas' {dmTablas: TDataModule},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DDiccionario in 'DDiccionario.pas' {dmDiccionario: TDataModule},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion_DevEx in '..\Tools\ZBaseEdicion_DevEx.pas' {BaseEdicion_DevEx},
  FKardexBase_DevEx in '..\Empleados\FKardexBase_DevEx.pas' {KardexBase_DevEx},
  ZBaseEdicionRenglon_DevEx in '..\Tools\ZBaseEdicionRenglon_DevEx.pas' {BaseEdicionRenglon_DevEx},
  ZBaseGridEdicion_DevEx in '..\Tools\ZBaseGridEdicion_DevEx.pas' {BaseGridEdicion_DevEx},
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  DGlobal in '..\DataModules\DGlobal.pas',
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  DLabor in 'DLabor.pas' {dmLabor: TDataModule},
  FBaseCedulas_DevEx in '..\Labor\Edicion\FBaseCedulas_DevEx.pas' {BaseCedulas_DevEx},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  dBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  dmailmerge in '..\DataModules\dmailmerge.pas' {dmMailMerge: TDataModule},
  DConfigPoliza in '..\DataModules\DConfigPoliza.pas' {dmConfigPoliza: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  FCalendarioRitmo_DevEx in '..\Catalogos\FCalendarioRitmo_DevEx.pas' {CalendarioRitmo},
  FEmpleadoGridSelect_DevEx in '..\Wizards\FEmpleadoGridSelect_DevEx.pas',
  FKardexDiario in 'FKardexDiario.pas' {KardexDiario},
  dBaseTressDiccionario in '..\datamodules\dBaseTressDiccionario.pas' {dmBaseTressDiccionario: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.HelpFile := 'SuperWin.chm';
  Application.Title := 'TRESS Supervisores';
  Application.CreateForm(TTressShell, TressShell);
  if TressShell.Inicializa( True ) then
     Application.Run
  else
     TressShell.Free;
end.
