unit FGrabarAreas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, ZetaHora, Mask,
  ZetaFecha, Buttons, ExtCtrls, DLabor, ZetaCommonClasses,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, ImgList,
  cxButtons, cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxClasses, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TGrabarAreas = class(TZetaDlgModal_DevEx)
    PanelParams: TPanel;
    GBCuando: TGroupBox;
    PaFechaLbl: TLabel;
    PaHoraLbl: TLabel;
    PaFecha: TZetaFecha;
    PaHora: TZetaHora;
    GBRegreso: TGroupBox;
    DataSource: TDataSource;
    rbSinRegreso: TRadioButton;
    rbPrimeraHora: TRadioButton;
    rbFechaHora: TRadioButton;
    PaFechaRegresoLbl: TLabel;
    PaFechaRegreso: TZetaFecha;
    PaHoraRegreso: TZetaHora;
    PaHoraRegresoLbl: TLabel;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    ZetaDBGridDBTableViewColumn1: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn2: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn3: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn4: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbRegresoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
    FParameterList: TZetaParams;
    procedure SetControles(const lEnabled: Boolean);
    procedure CargaParametros;
    procedure IniciaValores(const dFecha: TDate; const sHora: String);
  protected
    { Protected declarations }
     AColumn: TcxGridDBColumn;
  public
    { Public declarations }
    property ParameterList: TZetaParams read FParameterList;
    function GetTipoRegreso: Integer;
  end;

var
  GrabarAreas: TGrabarAreas;

function ShowGrabarAreas: Boolean;

implementation

uses ZetaLaborTools, ZetaDialogo, ZGlobalTress, ZAccesosMgr, ZAccesosTress,
     DGlobal,
     DCliente,
     ZetaCommonTools,
     ZetaClientDataSet;

{$R *.DFM}

function ShowGrabarAreas: Boolean;
begin
     GrabarAreas := TGrabarAreas.Create( Application );
     try
        try
           with dmLabor, GrabarAreas do
           begin
                IniciaValores( FechaArea, HoraArea );
                ShowModal;
                Result := ( ModalResult = mrOk );
                if ( Result ) then
                   GrabaKardexAreas( ParameterList );
           end;
        except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( VACIO, 'Error al Grabar Areas' + CR_LF + Error.Message, 0 );
                Result := FALSE;
           end;
        end;
     finally
            GrabarAreas.Free;
     end;
end;

procedure TGrabarAreas.FormCreate(Sender: TObject);
begin
     inherited;
     FParameterList := TZetaParams.Create;
     self.Caption:= Format( self.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] );
     rbSinRegreso.Caption := Format( rbSinRegreso.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] );
     // PENDIENTE HelpContext := 09507;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
end;

procedure TGrabarAreas.FormDestroy(Sender: TObject);
begin
     inherited;
     FParameterList.Free;
end;

procedure TGrabarAreas.FormShow(Sender: TObject);
begin
     inherited;
     rbPrimeraHora.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_AREAS, K_DERECHO_BANCA );
     rbFechaHora.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_AREAS, K_DERECHO_NIVEL0 );
     DataSource.DataSet := dmLabor.cdsAsignaAreas;
     rbSinRegreso.OnClick( self );
     self.ActiveControl := PaFecha;
     ZetaDBGridDBTableView.ApplyBestFit();
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
end;

procedure TGrabarAreas.CargaParametros;
begin
     dmCliente.CargaActivosSistema( FParameterList );
     with FParameterList do
     begin
          AddDate( 'Fecha', PaFecha.Valor );
          AddString( 'Hora', PaHora.Valor );
          AddInteger( 'TipoRegreso', GetTipoRegreso );
          AddDate( 'FechaRegreso', PaFechaRegreso.Valor );
          AddString( 'HoraRegreso', PaHoraRegreso.Valor );
     end;
end;

procedure TGrabarAreas.IniciaValores( const dFecha: TDate; const sHora: String );
begin
     PaFecha.Valor := dFecha;
     PaHora.Valor := sHora;
     PaFechaRegreso.Valor := dFecha + 1;
     PaHoraRegreso.Valor := sHora;
end;

procedure TGrabarAreas.SetControles( const lEnabled: Boolean );
begin
     PaFechaRegreso.Enabled := lEnabled;
     PaFechaRegresoLbl.Enabled := lEnabled;
     PaHoraRegreso.Enabled := lEnabled;
     PaHoraRegresoLbl.Enabled := lEnabled;
end;

procedure TGrabarAreas.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     inherited;
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TGrabarAreas.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
     inherited;
     ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TGrabarAreas.rbRegresoClick(Sender: TObject);
begin
     inherited;
     SetControles( rbFechaHora.Checked );
end;

function TGrabarAreas.GetTipoRegreso: Integer;
begin
     Result := rbSinRegreso.Tag;
     if rbPrimeraHora.Checked then
        Result := rbPrimeraHora.Tag
     else if rbFechaHora.Checked then
        Result := rbFechaHora.Tag;
end;

procedure TGrabarAreas.OKClick(Sender: TObject);
var
   lOK: boolean;
   dFechaCorte: Tdate;
begin
     inherited;
     lOK := not Global.GetGlobalBooleano( K_GLOBAL_LABOR_VALIDA_FECHACORTE );
     if not lOK then
     begin
          dFechaCorte := Global.GetGlobalDate( K_GLOBAL_LABOR_FECHACORTE );
          lOK := ( paFecha.Valor > dFechaCorte );
          if not lOK then
             DataBaseError( Format( 'ˇ No se Permite Agregar Información de Labor !' + CR_LF +
                                    'La Fecha de Corte es : %s', [ FechaCorta( dFechaCorte ) ] ) )
     end;
     if lOK then
     begin
          CargaParametros;
          ModalResult := mrOk;
     end;
end;

end.
