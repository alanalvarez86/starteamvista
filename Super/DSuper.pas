unit DSuper;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$define QUINCENALES}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
{$ifdef DOS_CAPAS}
  DServerSuper, DServerRecursos, DServerAsistencia,
{$else}
  Super_TLB, Recursos_TLB, Asistencia_TLB,
{$endif}
{$ifndef VER130}
  Variants, MaskUtils,
{$endif}
  ZetaClientDataSet, ZetaCommonClasses, ZetaCommonLists, ZetaTipoEntidad,
  ZBaseGridEdicion_DevEx,
  FWizAsistAjusteColectivo_DevEx;

type
  TRangoLista = Class(TObject)
  public
     TipoRango : eTipoRangoActivo;
     Inicial   : String;
     Final     : String;
     Lista     : String;

  end;

type
  TdmSuper = class(TDataModule)
    cdsTarjeta: TZetaClientDataSet;
    cdsChecadas: TZetaClientDataSet;
    cdsGridAsistencia: TZetaClientDataSet;
    cdsEmpleados: TZetaClientDataSet;
    cdsHisPermisos: TZetaClientDataSet;
    cdsHisKardex: TZetaClientDataSet;
    cdsMisAsignados: TZetaClientDataSet;
    cdsSupervisores: TZetaLookupDataSet;
    cdsAsignaciones: TZetaClientDataSet;
    cdsAsignacionesCB_CODIGO: TIntegerField;
    cdsAsignacionesCB_NIVEL: TStringField;
    cdsDatosEmpleado: TZetaClientDataSet;
    cdsAsignacionesANTERIOR: TStringField;
    cdsOtrosHistoriales: TZetaClientDataSet;
    cdsGridAutoXAprobar: TZetaClientDataSet;
    cdsEmpVacacion: TZetaClientDataSet;
    cdsHisVacacion: TZetaClientDataSet;
    cdsDataSet: TZetaClientDataSet;
    cdsClasifiTemp: TZetaClientDataSet;
    cdsCalendarioEmpleado: TZetaClientDataSet;
    cdsAutorizarPrenomina: TZetaClientDataSet;
    cdsPlanVacacion: TZetaClientDataSet;
    cdsAsignacionesFECHA: TDateField;
    cdsPlazas: TZetaClientDataSet;
    cdsSesion: TZetaClientDataSet;
    cdsHisCursos: TZetaClientDataSet;
    cdsHisCursosProg: TZetaClientDataSet;
    cdsAprobados: TZetaClientDataSet;
    cdsGrupos: TZetaClientDataSet;
    cdsGridTarjetasPeriodo: TZetaClientDataSet;
    cdsListaEmpleados: TZetaClientDataSet;
    cdsCertEmp: TZetaClientDataSet;
    cdsCosteoTransferencias: TZetaClientDataSet;
    cdsSuperCosteo: TZetaClientDataSet;
    cdsGridCosteoTransferencia: TZetaClientDataSet;
    cdsSaldoVacacionesTotales: TZetaClientDataSet;
    cdsSaldoVacaciones: TZetaClientDataSet;
    cdsPlanCapacitacion: TZetaClientDataSet;
    cdsHisCompeten: TZetaClientDataSet;
    cdsHisCompEvalua: TZetaClientDataSet;
    cdsMatrizHabilidades: TZetaClientDataSet;
    cdsEvaluacionSel: TZetaClientDataSet;
    cdsMatrizCursos: TZetaClientDataSet;
    cdsEvaluacionDiaria: TZetaClientDataSet;

    procedure cdsHisPermisosAlAdquirirDatos(Sender: TObject);
    procedure cdsTarjetaAlAdquirirDatos(Sender: TObject);
    procedure cdsGridAsistenciaAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpleadosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisKardexAlAdquirirDatos(Sender: TObject);
    procedure cdsMisAsignadosAlAdquirirDatos(Sender: TObject);
    Procedure CdsSupervisoresFiltroActivos(BFiltrado:boolean);
    procedure cdsSupervisoresAlAdquirirDatos(Sender: TObject);
    procedure cdsAsignacionesAlEnviarDatos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
    procedure cdsHisKardexAlCrearCampos(Sender: TObject);
    procedure cdsHisKardexAlModificar(Sender: TObject);
    procedure cdsHisKardexAlEnviarDatos(Sender: TObject);
    procedure cdsHisKardexAfterDelete(DataSet: TDataSet);
    Procedure cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisPermisosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsSesionReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisPermisosRenconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsSesionReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsHisKardexNewRecord(DataSet: TDataSet);
    procedure cdsHisKardexAlAgregar(Sender: TObject);
    procedure cdsHisPermisosAlCrearCampos(Sender: TObject);
    procedure OnPM_FEC_INIChange(Sender: TField);
    procedure OnPM_FEC_FINChange(Sender: TField);
    procedure OnPM_CLASIFIValidate(Sender: TField);
    procedure cdsDatosEmpleadoAlModificar(Sender: TObject);
    procedure cdsDatosEmpleadoAlEnviarDatos(Sender: TObject);
    procedure cdsHisPermisosAlModificar(Sender: TObject);
    procedure cdsHisPermisosAlEnviarDatos(Sender: TObject);
    procedure cdsHisPermisosNewRecord(DataSet: TDataSet);
    procedure cdsHisPermisosBeforePost(DataSet: TDataSet);
    procedure cdsHisPermisosAfterDelete(DataSet: TDataSet);
    procedure cdsTarjetaAlCrearCampos(Sender: TObject);
    procedure HO_CODIGOChange(Sender: TField);
    procedure cdsChecadasAlCrearCampos(Sender: TObject);
    procedure CH_H_AJUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CH_H_REALSetText(Sender: TField; const Text: String);
    procedure CH_H_REALValidate(Sender: TField);
    procedure CH_H_REALChange(Sender: TField);
    procedure CHECADAChange(Sender: TField);
    procedure ChecaPenalizacionChecada(Sender: TField);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PRETTYNAMEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure EmpCHECADASGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure DescansoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AprobadasGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CH_RELOJGetText(Sender: TField; var Text: String; DisplayText: Boolean);{ACL170409}
    procedure CH_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);{ACL170409}
    procedure TR_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsChecadaManualValidate(Sender: TField);{ACL170409}
    procedure cdsChecadasBeforePost(DataSet: TDataSet);
    procedure cdsTarjetaCalcFields(DataSet: TDataSet);
    procedure cdsTarjetaNewRecord(DataSet: TDataSet);
    procedure cdsTarjetaAfterCancel(DataSet: TDataSet);
    procedure cdsTarjetaAlBorrar(Sender: TObject);
    procedure cdsTarjetaAlModificar(Sender: TObject);
    procedure cdsTarjetaAlEnviarDatos(Sender: TObject);
    procedure cdsChecadasAfterCambios(DataSet: TDataSet);
    procedure cdsTarjetaAlAgregar(Sender: TObject);
    procedure cdsGridAsistenciaAlCrearCampos(Sender: TObject);
    procedure cdsGridAsistenciaAlEnviarDatos(Sender: TObject);
    procedure cdsGridAsistenciaAlCrearCamposGenerales(Sender: TObject);
    procedure cdsGridAsistenciaAlCrearCamposDefaults(Sender: TObject);
    procedure cdsMotAutoValidate(Sender: TField);
    procedure cdsHorarioValidate(Sender: TField);
    procedure cdsEmpleadosAlCrearCampos(Sender: TObject);
    procedure cdsTarjetaBeforePost(DataSet: TDataSet);
    procedure cdsGridAutoXAprobarAlAdquirirDatos(Sender: TObject);
    procedure cdsGridAutoXAprobarAlCrearCampos(Sender: TObject);
    procedure CH_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsGridAutoXAprobarAlEnviarDatos(Sender: TObject);
    procedure cdsGridAutoXAprobarBeforePost(DataSet: TDataSet);
    procedure cdsHisVacacionAlAdquirirDatos(Sender: TObject);
    procedure cdsHisVacacionAlCrearCampos(Sender: TObject);
    //procedure OnVA_FEC_INIValidate(Sender: TField);
    procedure OnVA_FEC_INIChange(Sender: TField);
    procedure OnVA_FEC_FINChange(Sender: TField);
    procedure OnVA_TIPOChange(Sender: TField);
    procedure OnVA_GOZOChange(Sender: TField);
    procedure OnVA_OTROSChange(Sender: TField);
    procedure OnVA_PAGOChange(Sender: TField);
    procedure cdsHisVacacionNewRecord(DataSet: TDataSet);
    procedure cdsHisVacacionAlModificar(Sender: TObject);
    procedure cdsHisVacacionAfterDelete(DataSet: TDataSet);
    procedure cdsHisVacacionBeforePost(DataSet: TDataSet);
    procedure cdsHisVacacionAlEnviarDatos(Sender: TObject);
    procedure cdsEmpVacacionAlCrearCampos(Sender: TObject);
    procedure cdsHisVacacionAlBorrar(Sender: TObject);
    procedure cdsHisVacacionBeforeEdit(DataSet: TDataSet);
    procedure cdsHisVacacionBeforeInsert(DataSet: TDataSet);
    procedure cdsHisVacacionAfterCancel(DataSet: TDataSet);
    procedure cdsClasifiTempAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiTempAlCrearCampos(Sender: TObject);
    procedure cdsClasifiTempAlEnviarDatos(Sender: TObject);
    procedure cdsClasifiTempAfterDelete(DataSet: TDataSet);
    procedure cdsClasifiTempAlModificar(Sender: TObject);
    procedure cdsClasifiTempNewRecord(DataSet: TDataSet);
    procedure cdsClasifiTempCB_CODIGOChange(Sender: TField);
    procedure cdsClasifiTempBeforePost(DataSet: TDataSet);
    procedure cdsAutorizarPrenominaAlAdquirirDatos(Sender: TObject);
    procedure cdsAutorizarPrenominaAlEnviarDatos(Sender: TObject);
    procedure cdsAutorizarPrenominaAlCrearCampos(Sender: TObject);
    procedure cdsAutorizarPrenominaBeforePost(DataSet: TDataSet);
    procedure cdsAutorizarPrenominaNO_SUP_OKChange(Sender: TField);
    {$ifdef VER130}
    procedure cdsPlanVacacionReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsPlanVacacionReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsPlanVacacionAfterDelete(DataSet: TDataSet);
    procedure cdsPlanVacacionAlCrearCampos(Sender: TObject);
    procedure cdsPlanVacacionAlAgregar(Sender: TObject);
    procedure cdsPlanVacacionNewRecord(DataSet: TDataSet);
    procedure cdsPlanVacacionBeforePost(DataSet: TDataSet);
    procedure cdsPlanVacacionAlEnviarDatos(Sender: TObject);
    procedure cdsPlanVacacionAlModificar(Sender: TObject);
    procedure cdsPlanVacacionVP_FEC_INIChange(Sender: TField);
    procedure cdsPlanVacacionVP_FEC_FINChange(Sender: TField);
    procedure cdsPlanVacacionVP_STATUSChange(Sender: TField);
    procedure cdsPlanVacacionVP_PAGO_USChange(Sender: TField);
    procedure cdsPlanVacacionCB_CODIGOChange(Sender: TField);
    procedure cdsPlanVacacionCB_CODIGOValidate(Sender: TField);
    procedure GetDefaultCampo(Sender: TField);
    procedure cdsHisCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCursosAlCrearCampos(Sender: TObject);
    procedure cdsHisCursosBeforePost(DataSet: TDataSet);
    procedure cdsCursosAlModificar(Sender: TObject);
    procedure cdsHisCursosProgAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCursosProgAlCrearCampos(Sender: TObject);
    procedure cdsAprobadosAlAdquirirDatos(Sender: TObject);
    procedure cdsSesionAlAdquirirDatos(Sender: TObject);
    procedure cdsAprobadosAlCrearCampos(Sender: TObject);
    procedure CursosCB_CODIGOChange(Sender: TField);
    procedure cdsGridCB_CODIGOValidate(Sender: TField);
    procedure cdsAprobadosAfterEditInsert(DataSet: TDataSet);
    procedure cdsCursosAlAgregar(Sender: TObject);
    procedure cdsHisCursosAlBorrar(Sender: TObject);
    procedure cdsSesionAfterInsert(DataSet: TDataSet);
    procedure cdsSesionAlModificar(Sender: TObject);
    procedure cdsSesionAfterCancel(DataSet: TDataSet);
    procedure cdsSesionAlEnviarDatos(Sender: TObject);
    procedure cdsSesionNewRecord(DataSet: TDataSet);
    procedure cdsSesionBeforePost(DataSet: TDataSet);
    procedure cdsHisCursosAlEnviarDatos(Sender: TObject);
    procedure cdsGruposAlCrearCampos(Sender: TObject);
    procedure cdsGruposCalcFields(DataSet: TDataSet);
    procedure cdsSesionAfterDelete(DataSet: TDataSet);
    //procedure cdsAprobadosBeforePost(DataSet: TDataSet);
    procedure cdsSesionAlCrearCampos(Sender: TObject);
    procedure cdsGruposAlBorrar(Sender: TObject);
    procedure OnPM_CLASIFIChange(Sender: TField);
    procedure cdsGridTarjetasPeriodoAlAdquirirDatos(Sender: TObject);
    procedure cdsGridTarjetasPeriodoAlCrearCampos(Sender: TObject);
    procedure AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsGridTarjetasPeriodoAlEnviarDatos(Sender: TObject);
    procedure cdsGridTarjetasPeriodoBeforePost(DataSet: TDataSet);
    procedure cdsGridTarjetasPeriodoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsChecadasAfterPost(DataSet: TDataSet);{ACL200409}
    procedure cdsMotChecadaValidate(Sender: TField);
    procedure cdsMotChecadaChange(Sender: TField);
    procedure cdsGridTarjetasPeriodoBeforeOpen(DataSet: TDataSet);
    procedure cdsGridAsistenciaBeforePostGenerales(DataSet: TDataSet);
    procedure cdsChecadasAfterOpen(DataSet: TDataSet);
    procedure cdsChecadasAfterCancel(DataSet: TDataSet);
    procedure cdsCertEmpAlAdquirirDatos(Sender: TObject);
    procedure cdsCertEmpAlCrearCampos(Sender: TObject);
    procedure cdsCertEmpCalcFields(DataSet: TDataSet);
    procedure cdsCosteoTransferenciasAlCrearCampos(Sender: TObject);
    procedure cdsCosteoTransferenciasNewRecord(DataSet: TDataSet);
    procedure cdsCosteoTransferenciasAlEnviarDatos(Sender: TObject);
    procedure cdsCosteoTransferenciasAlAgregar(Sender: TObject);
    procedure cdsCosteoTransferenciasAfterPost(DataSet: TDataSet);
    procedure TransferenciasCB_CODIGOChange(Sender: TField);
    procedure TR_STATUSChange(Sender: TField);
    procedure TR_APRUEBAChange(Sender: TField);
    procedure cdsSuperCosteoAlAdquirirDatos(Sender: TObject);
    procedure cdsCosteoTransferenciasAlModificar(Sender: TObject);
    procedure cdsCosteoTransferenciasAlBorrar(Sender: TObject);
    procedure cdsCosteoTransferenciasBeforePost(DataSet: TDataSet);
    procedure cdsGridCosteoTransferenciaAlEnviarDatos(Sender: TObject);
    procedure cdsCosteoTransferenciasBeforeInsert(DataSet: TDataSet);
    procedure cdsSaldoVacacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsSaldoVacacionesAlCrearCampos(Sender: TObject);
    procedure cdsSaldoVacacionesTotalesAlCrearCampos(Sender: TObject);
    procedure cdsPlanCapacitacionAlAdquirirDatos(Sender: TObject);
    procedure cdsPlanCapacitacionAlCrearCampos(Sender: TObject);
    procedure cdsHisCompetenAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCompetenAlCrearCampos(Sender: TObject);
    procedure cdsHisCompetenAlEnviarDatos(Sender: TObject);
    procedure cdsHisCompetenAfterDelete(DataSet: TDataSet);
    procedure cdsHisCompetenNewRecord(DataSet: TDataSet);
    procedure cdsHisCompetenBeforePost(DataSet: TDataSet);
    procedure cdsHisCompetenAlModificar(Sender: TObject);
    procedure cdsHisCompEvaluaAlAdquirirDatos(Sender: TObject);
    procedure cdsHisCompEvaluaAlCrearCampos(Sender: TObject);
    procedure cdsHisCompEvaluaAlEnviarDatos(Sender: TObject);
    procedure cdsHisCompEvaluaAlModificar(Sender: TObject);
    procedure cdsHisCompEvaluaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisCompEvaluaBeforePost(DataSet: TDataSet);
    procedure cdsHisCompEvaluaAfterDelete(DataSet: TDataSet);
    procedure cdsHisCompEvaluaNewRecord(DataSet: TDataSet);
    procedure cdsMatrizHabilidadesAlAdquirirDatos(Sender: TObject);
    procedure cdsMatrizHabilidadesAlCrearCampos(Sender: TObject);
    procedure cdsEvaluacionSelNewRecord(DataSet: TDataSet);
    procedure validacionTipoNominaChange(Sender: TField);
    procedure cdsEmpleadosCalcFields(DataSet: TDataSet);
    procedure cdsMatrizCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsMatrizCursosAlCrearCampos(Sender: TObject);
    procedure cdsEvaluacionDiariaAlAdquirirDatos(Sender: TObject);
    procedure cdsEvaluacionDiariaAlEnviarDatos(Sender: TObject);
    procedure cdsEvaluacionDiariaAlCrearCampos(Sender: TObject);
    procedure cdsEvaluacionDiariaBeforePost(DataSet: TDataSet);

  private
    { Private declarations }
    FCambiaComboFestivo: Boolean;
    FFiltroFijo: Integer;
    FRangoLista : TRangoLista;
    FCondicion: String;
    FFiltro : String;
    FFolioSesion: Integer;
    FAsistenciaStatusLimite, FNivelSupervisor: Integer;
    FVerBajas, FObtenerFoto, FLaborActivado, FCancelaAjuste : Boolean;
    FEmpleadoBOF, FEmpleadoEOF: Boolean;
    BorrandoKardex, RefrescaHisKardex, FRefrescarFormaActiva : Boolean;
    FNoPreguntarFiltro: Boolean;
    FOtrosDatos : Variant;
    FOldChecadas : String;
    FFechaGridAuto: TDate;
    FSaldosVaca : OleVariant;
    FCambioVacaciones: Boolean;
    FVacaCalculaDiasGozados: Boolean;
    FAntDiasGozados, FAntDiasPagados, FAntDiasPrima: TPesos;
    FInsertandoVaca : Boolean;
    FParamsAsistencia: TZetaParams;
    FCambioGrid: Boolean;
    {$ifdef false}
    FMotivoDefaults: TMotivoDefaults;
    {$endif}
    FParametrosSesion: TZetaParams;
    FBufferAprobados: OleVariant;
    FHayBufferAprobados: Boolean;
    //FUltimaEval: Double; AP(31/10/2007): No es necesario en supervisores
    FPosicionaEmpleadoGrupo: Boolean;
    FUltimoMotivoCH: String;
    //Costeo 2011-Transferencia de Empleados
    FParametrosTransferencias : TZetaParams;
    FTransferUltimaHora: TDiasHoras;
    FAgregandoTransferencia: Boolean;
    FFiltroMatHabilidades:TZetaParams;
    FFiltroMatCursos: TZetaParams;
    FAgregandoMatriz:Boolean;
    FEvaluarMatriz:Boolean;
    FCambioMatrizCursos: Boolean;
    {$ifdef ICUMEDICAL_CURSOS}
    FPathICU: string;
    {$endif}
{$ifdef DOS_CAPAS}
    function GetServerSuper : TdmServerSuper;
    property ServerSuper : TdmServerSuper read GetServerSuper;
    function GetServerRecursos : TdmServerRecursos;
    property ServerRecursos : TdmServerRecursos read GetServerRecursos;
    function GetServerAsistencia: TdmServerAsistencia;
    property ServerAsistencia : TdmServerAsistencia read GetServerAsistencia;
{$else}
    FServidor : IdmServerSuperDisp;
    FServidorRH : IdmServerRecursosDisp;
    FServidorAS : IdmServerAsistenciaDisp;
    function GetServerSuper : IdmServerSuperDisp;
    property ServerSuper : IdmServerSuperDisp read GetServerSuper;
    function GetServerRecursos : IdmServerRecursosDisp ;
    property ServerRecursos : IdmServerRecursosDisp read GetServerRecursos;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    property ServerAsistencia : IdmServerAsistenciaDisp read GetServerAsistencia;
{$endif}
   { function GetEmpleadoNumero: TNumEmp; }
    function GetEmpleadoNombre: String;
    function GetRangoLista: String;
    function GetCondicion: String;
    function GetParametroscdsEmpleados : TZetaParams;
    function GetChecadasStr: String;
    function BuscaEmpleado( const iEmpleado: TNumEmp ): Boolean;
    // cdsDatosEmpleado .. Un Registro a la vez
    function FEmpleado: Integer;
    function FetchEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchNextEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchPreviousEmployee(const iEmpleado: TNumEmp): Boolean;
    function GetDatosClasificacion(const iEmpleado: TNumEmp; const dValor: TDate): TDatosClasificacion;
   { function GetStatusHorario(const sTurno: String; const dReferencia: TDate): TStatusHorario;   }
    function GetChecadasVar: OleVariant;
    function Check(const Resultado: OleVariant): Boolean;
    function GetMinPenalizacion(const sHorario: String; const iPenal: Integer): Integer;
    function GetHorarioCodigo(const sNameDataSet: String): String;
    function FiltroEmpleadosActivo: Boolean;
    function GetDescTipo( const nTipo: Integer ): String;
    function GetVacaDiasGozar(const iEmpleado: Integer; const dInicio, dFinal: TDate): TPesos;
    function ValidateTipoVacacion( const UpdateKind: TUpdateKind; var sMensaje: String ): Boolean;
    procedure RefrescaKardex(const sTipo: String);
    procedure AgregarOtrosHistoriales;
    procedure ValidaLookup( const sValor,sFiltro : String ; oLookup: TZetaLookupDataSet  );
    procedure SetFiltroFijo( const Value: Integer );
    procedure RecalculaMontoVaca;
    procedure GetSaldosVacacion;
    procedure ValidaFechaVacacion( DataSet: TDataSet );
    procedure ValidaExcederSaldos( DataSet: TDataSet );
    Procedure cdsPlanVacacionRecalculaDias;
    procedure RecalculaDiasSaldos;
    procedure OnKC_FEC_TOMChange(Sender: TField);
    procedure SESIONCU_CODIGOChange(Sender: TField);
    procedure CU_CODIGOChange(Sender: TField);
    procedure OnCI_CODIGOChange(Sender: TField);
    //function CargaDefaultsAjuste: TInfoAjuste;
  public
  GetCertEmpLocalFromCdsDatosEmp  : Boolean;

    { Public declarations }
     function GetStatusHorario( const sTurno: String; const dReferencia: TDate ): TStatusHorario;//acl
     function GetEmpleadoNumero: TNumEmp;
    property CambiaComboFestivo: boolean read FCambiaComboFestivo write FCambiaComboFestivo;
    property EmpleadoNumero: TNumEmp read GetEmpleadoNumero;
    property EmpleadoNombre: String read GetEmpleadoNombre;
    property FiltroFijo: Integer read FFiltroFijo write SetFiltroFijo;
    property RangoLista: TRangoLista read FRangoLista write FRangoLista;
    property Condicion: String read FCondicion write FCondicion;
    property Filtro: String read FFiltro write FFiltro;
    property AsistenciaStatusLimite: Integer read FAsistenciaStatusLimite write FAsistenciaStatusLimite;
    property NivelSupervisor: Integer read FNivelSupervisor write FNivelSupervisor;
    property VerBajas : Boolean read FVerBajas write FVerBajas;
    property ObtenerFoto : Boolean read FObtenerFoto write FObtenerFoto;
    property LaborActivado : Boolean read FLaborActivado write FLaborActivado;
    property OtrosDatos : Variant read FOtrosDatos write FOtrosDatos;
    property RefrescarFormaActiva : Boolean read FRefrescarFormaActiva write FRefrescarFormaActiva;
    property NoPreguntarFiltro: Boolean read FNoPreguntarFiltro write FNoPreguntarFiltro;
    property FechaGridAuto: TDate read FFechaGridAuto write FFechaGridAuto;
    property CambioGrid: Boolean read FCambioGrid write FCambioGrid;
    property FolioSesion: Integer read FFolioSesion write FFolioSesion;
    //property MaxFolioSesion: Integer read FMaxFolioSesion write FMaxFolioSesion;
    property ParametrosSesion: TZetaParams read FParametrosSesion write FParametrosSesion;
    //property PosicionaEmpleadoGrupo: Boolean read FPosicionaEmpleadoGrupo;
    property ParametrosTransferencias: TZetaParams read FParametrosTransferencias write FParametrosTransferencias;
    property FiltroMatHabilidades: TZetaParams read FFiltroMatHabilidades write FFiltroMatHabilidades;
    property FiltroMatCursos: TZetaParams read FFiltroMatCursos write FFiltroMatCursos;
    property AgregandoMatriz: Boolean read FAgregandoMatriz write FAgregandoMatriz;
    property EvaluarMatriz: Boolean read FEvaluarMatriz write FEvaluarMatriz;
    property CambioMatrizCursos: Boolean read FCambioMatrizCursos write FCambioMatrizCursos;
    {$ifdef ICUMEDICAL_CURSOS}
    property PathICU : String read FPathICU write FPathICU;
    {$endif}
    function PosicionaEmpleado(const iEmpleado: Integer): Boolean;
    function ChecaSuperAnterior(iEmpleado: Integer; sSuper: String): String;
//    function ChecaAreaAnterior(iEmpleado: Integer; sArea: String): String;
    function SetEmpleadoNumero(const Value: TNumEmp; const lForzaRefrescar: Boolean = FALSE ): Boolean;
    function GetEmpleadoAnterior: Boolean;
    function GetEmpleadoSiguiente: Boolean;
    function GetEmpleadoPrimero: Boolean;
    function GetEmpleadoUltimo: Boolean;
    function AjusteColectivoGrabaLista(oWizard: TWizAsistAjusteColectivo_DevEx;Parametros: TZetaParams): Boolean;
    function ExisteKardex(const dFecha: TDate; const sTipo: String): Boolean;
    function GetStatus(iEmpleado: integer; dFecha: TDate): eStatusEmpleado;
    function DerechoTipoPermiso(const Tipo: Integer): Boolean;
    function IncidenciaValida(const sAccion: String; var sMensaje: String): Boolean;
    function VerificaFiltroEmpleados : Boolean;
    function Aprobar_Autorizaciones( oDataSet : TDataSet; const sAprobacion: string ): Boolean;
    function AprobarAut_Derecho( oDataSet : TDataSet; const sAprobacion: string ): Boolean;
    function CambioMasivoTurno(Parametros: TZetaParams; const lVerificacion: Boolean; SetAnimacion : TProcAnimacion ): Boolean;
    function ValidaInicioVacacion( const dFecha: TDate ): Boolean;
    function ValidateInicioVacacion( const dFecha: TDate; const UpdateKind: TUpdateKind; var sMensaje: String ): Boolean;
    {$ifdef ICUMEDICAL_CURSOS}
    function CargaDocumentoICU( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
    procedure AbreDocumentoICU;
    procedure BorraDocumentoICU;
    {$endif}
    procedure SetFiltroSupervisores(const lMisSuper: Boolean);
    procedure RefrescaMisEmpleados;
    procedure ResetFiltros;
    // cdsDatosEmpleado .. Un Registro a la vez
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades);
    {$endif}
    procedure AjusteColectivoGetLista(Parametros: TZetaParams);
    procedure EditarGridAProbarAutorizaciones;
    procedure EditarGridAutorizarPrenomina;
    {$ifdef COMMSCOPE}
    procedure EditarGriEvaluacionDiaria;
    function GetFechaLimiteDiaria( Empresa: OleVariant):TDateTime;
    {$endif}
    procedure SetSaldosVacaciones( const lInfoAniversario: Boolean; var rGozo, rPago, rPrima: TPesos );
    procedure RecalculaVacaDiasGozados;
    procedure RecalculaVacaFechaRegreso;
    procedure CambioMasivoTurnoGetLista(Parametros: TZetaParams);
    procedure MuestraCalendarioEmpleado;
    procedure MuestraCalendarioVacacion( const rDiasGozar : Double );{OP: 11/06/08}
    procedure ResetCalendarioEmpleadoActivo( const dInicio, dFinal: TDate );
    function PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                        const iEmpleado: integer;
                                        const eTipo: eStatusPeriodo;
                                        const lPuedeModificar: Boolean;
                                        var sMensaje: string): Boolean;

    procedure RefrescarPlanVacacion(Parametros : TZetaParams);
    procedure SetAutorizarPlanVacacion( const tipoAutorizacion: eStatusPlanVacacion; const lTodos: Boolean );
    function  GetChecadaPuntual(const sHorario: string; iTipoChecada: Integer; var sChecada: String): boolean;
    procedure RegistrarTarjetaPuntual (const sChecada: string);
    procedure RegistrarTarjetaPuntualColectiva( DataSet : TZetaClientDataSet;const sChecada, sCampoChecada,sTitulo: string);
    function EsEmpleadoPorKardex( const iEmpleado: Integer ): Boolean;
    function RefrescaLista( dFechaAsistencia: TDate ): Boolean;
    function PreparaPlaza( const iEmpleado: Integer ): Boolean;
    function GrabaKardexPlaza( const iEmpleado: Integer ): Integer;
    function GetFiltroEmpleado( const iFiltro: Integer ): String;
    procedure ObtieneSesion( const lConectaEmpleados: Boolean );
    procedure ObtieneSesionesFiltros;
    function ChecaFolioSesion( iFolio: Integer ): Boolean;
    function EsSuSesion( iFolio: Integer ): Boolean;
    function GetTipoMotivo( sCampo: String ): Integer;
    function ValidarMotivoPermisos(const sCodigo :string;const iTipo:Integer):Boolean;
    function GetVacaFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const rGozo: TPesos; var rDiasRango: Double ): TDate;{OP: 12/06/08}
    function PuedeCambiarTarjeta(const dInicial, dFinal: TDate; const iEmpleado: integer; var sMensaje: string): Boolean;
    procedure ObtieneTransferencias( Params: TZetaParams );
    function FiltroUsuariosXCentroCosto( const sCentroCosto: string; var iUsuarioDefault: Integer; var sFiltro, sMensaje: string ): Boolean;
    procedure EditarGridAprobarCancelarTransferencias;
    procedure ObtieneTransferenciasDataset( DatasetCosteo:TZetaClientDataset; Params: TZetaParams );
    function GrabaTransferencias( DatasetCosteo:TZetaClientDataset; const lAsignarData: Boolean; const lResetDataChange : Boolean = TRUE ): Boolean;
    function GetPermisoFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const Dias: integer; var rDiasRango: Double ): TDate;
    function GetPermisoDiasHabiles( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
    procedure AgregarMatrizCurso(FCurso : String);
    function GetHorarioTurno( const iEmpleado: Integer; const dFecha: TDate; iTipo: Integer ): String;
  end;

const
  TKARDEX_GRAL = 1;
  TPUESTO = 2;
  TTURNO = 4;
  TAREA = 8;
  K_HORA_NULA = '----';

  K_T_MULTIPLE = 'MULTIP';

  TP_CON_GOCE   = 1;
  TP_FALTA_JUST = 2;
  TP_SUSPENSION = 4;
  TP_OTROS      = 8;

  // Evaluaci�n de Derechos de Acceso Especiales ( Autorizaciones de Asistencia )
  HORAS_EXTRAS = 1;
  DESCANSO_TRABAJADO = 2;
  PERMISO_CON_GOCE = 4;
  PERMISO_SIN_GOCE = 8;
  DERECHO_TIPO_DIA = 16;
  DERECHO_HORARIO = 32;
  DESCUENTO_COMIDAS = 64;
  APROBAR_AUTORIZACION = 128;
  PER_PREPAG_FUERADENTROJOR  =   256;


  K_FECHA = 0;
  K_OBSERVA = 2;
  K_TITLE_ADVERTENCIA = 'Advertencia';


var
  dmSuper: TdmSuper;
  StringFiltroSuper:String;
  StringFiltroActivo:string;
implementation

uses DCliente, DCatalogos, DSistema, DTablas, DConsultas, DGlobal,
     ZetaClientTools, ZetaDialogo, ZetaCommonTools, ZetaMsgDlg, FToolsRH,
     ZBaseEdicion_DevEx, ZBaseDlgModal_DevEx, ZReconcile, FTressShell,
     FKardexAltaDlg, FKardexGral, FKardexVarios, FEditPermisos,
     FEditVacacion_DevEx,
     FEditTarjeta, FEditClasifTemporal_DevEx,
     WizardFeedBack, ZGlobalTress, ZAccesosMgr,
     FCheckFiltroActivo, ZAccesosTress, DLabor, FGridAutorizacionesPorAprobar,
     FToolsAsistencia, FCalendarioEmpleado_DevEx, FGridAutorizaPrenomina, FEditPlanVacacion_DevEx,
     FRegPlanVacacion_DevEx,FGridCursosSuper,FEditHisCurso_DevEx, {OP: 11/06/08} FCalendarioVacacion_DevEx,
     FSuperEditCosteoTransferencias,
     FGridSuperCosteoTransferencias,
     FGridAprobarCosteoTransferencias_DevEx,
     FEditHisCompeten_DevEx,FEditHisEvaluaCompeten_DevEx,
     ZetaKeyLookup_DevEx{$ifdef COMMSCOPE}, FGridEvaluacionDiaria {$endif}{$ifdef ICUMEDICAL_CURSOS}, ZetaFilesTools {$endif};
{$R *.DFM}

procedure TdmSuper.DataModuleCreate(Sender: TObject);
const
     K_NINGUN_VALOR = 1;
begin
     FRangoLista := TRangoLista.Create;
     BorrandoKardex := FALSE;
     RefrescarFormaActiva:= TRUE;      // Para casos en que no se quiera refrescar forma activa en el ondatachange del Shell
     FOldChecadas:= VACIO;
     FSaldosVaca := NULL;
     FVacaCalculaDiasGozados := TRUE;
     FInsertandoVaca := FALSE;
     FParametrosSesion:= TZetaParams.Create;
     FFolioSesion:= K_NINGUN_VALOR;
     FHayBufferAprobados:= FALSE;
     FPosicionaEmpleadoGrupo:= FALSE;
     FParamsAsistencia:= TZetaParams.Create;
     FAgregandoTransferencia := FALSE;
     FFiltroMatHabilidades := TZetaParams.Create;
     FiltroMatCursos := TZetaParams.Create;
     GetCertEmpLocalFromCdsDatosEmp:=false;
     FCambiaComboFestivo := FALSE;
end;

procedure TdmSuper.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FParamsAsistencia );
     FreeAndNil( FParametrosSesion );
     FreeAndNil( FParametrosTransferencias );
     FreeAndNil( FFiltroMatHabilidades );
     FreeAndNil( FFiltroMatCursos );
     FRangoLista.Free;
end;

{$ifdef DOS_CAPAS}
function TdmSuper.GetServerSuper: TdmServerSuper;
begin
     Result := DCliente.dmCliente.ServerSuper;
end;

function TdmSuper.GetServerRecursos: TdmServerRecursos;
begin
     Result := DCliente.dmCliente.ServerRecursos;
end;

function TdmSuper.GetServerAsistencia: TdmServerAsistencia;
begin
     Result := DCliente.dmCliente.ServerAsistencia;
end;
{$else}
function TdmSuper.GetServerSuper: IdmServerSuperDisp;
begin
     Result:= IdmServerSuperDisp( dmCliente.CreaServidor( CLASS_dmServerSuper, FServidor ) );
end;

function TdmSuper.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidorRH ) );
end;

function TdmSuper.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( dmCliente.CreaServidor( CLASS_dmServerAsistencia, FServidorAS ) );
end;
{$endif}

{$ifdef VER130}
procedure TdmSuper.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmSuper.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

function TdmSuper.Check( const Resultado: OleVariant): Boolean;
begin
     with TWizardSuperFeedback.Create( Application ) do
     begin
          try
             with ProcessData do
             begin
                  SetResultado( Resultado );
                  Result := ( Status in [ epsEjecutando, epsOK ] );
             end;
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
          finally
             Free;
          end;
     end;
end;

{ Metodos de dmSuper }
{$ifdef MULTIPLES_ENTIDADES}
procedure TdmSuper.NotifyDataChange(const Entidades: Array of TipoEntidad);
{$else}
procedure TdmSuper.NotifyDataChange(const Entidades: ListaEntidades);
{$endif}

begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enEmpleado , Entidades ) then
     {$else}
     if ( enEmpleado in Entidades ) then
     {$endif}
     begin
          cdsHisPermisos.SetDataChange;
          cdsHisKardex.SetDataChange;
          cdsTarjeta.SetDataChange;
          cdsHisVacacion.SetDataChange;
          cdsHisCursos.SetDataChange;
          cdsHisCursosProg.SetDataChange;
          cdsSaldoVacaciones.SetDataChange;
          cdsPlanCapacitacion.SetDataChange;
          cdsHisCompEvalua.SetDataChange;
         // cdsCertEmp.SetDataChange;
     end

     {$ifdef MULTIPLES_ENTIDADES}
     else if Dentro( enAusencia , Entidades ) then
     {$else}
     else if ( enAusencia in Entidades ) then
     {$endif}
     begin
          cdsTarjeta.SetDataChange;
          cdsMisAsignados.SetDataChange;
          cdsClasifiTemp.SetDataChange;
          cdsMatrizHabilidades.SetDataChange;
          cdsPlanCapacitacion.SetDataChange; 
     end

    {$ifdef MULTIPLES_ENTIDADES}
     else if Dentro( enPermiso , Entidades ) or
             Dentro( enVacacion , Entidades ) or
             Dentro( enKardex , Entidades ) then
    {$else}
     else if ( enPermiso in Entidades ) or
             ( enVacacion in Entidades ) or
             ( enKardex in Entidades ) then
    {$endif}
     begin
          cdsHisKardex.SetDataChange;
     end

     {$ifdef MULTIPLES_ENTIDADES}
     else if Dentro( enClasifiTemp , Entidades ) then
     {$else}
     else if ( enClasifiTemp in Entidades ) then
     {$endif}
     begin
          cdsTarjeta.SetDataChange;
     end
     else if Dentro( enSesion, Entidades ) then
     begin
          cdsHisCursos.SetDataChange;
          cdsGrupos.SetDataChange;
     end;

     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enTransferencias , Entidades ) then
     {$else}
     if ( enTransferencias in Entidades ) then
     {$endif}
     begin
          cdsCosteoTransferencias.SetDataChange;
          cdsTarjeta.SetDataChange;
     end;
end;

procedure TdmSuper.RefrescaMisEmpleados;
var
   iEmpleadoActual : Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     FRefrescarFormaActiva := FALSE;
     iEmpleadoActual := EmpleadoNumero;    { Empleado Actual }
     cdsEmpleados.DisableControls;
     try
        cdsEmpleados.Refrescar;
        if ( iEmpleadoActual <= 0 ) or ( not BuscaEmpleado( iEmpleadoActual ) ) then
           FRefrescarFormaActiva := TRUE;    { Si lo encuentra lo posiciona }
     finally
        cdsEmpleados.EnableControls;
        FRefrescarFormaActiva := TRUE;
        Screen.Cursor := oCursor;
     end;
end;

function TdmSuper.BuscaEmpleado(const iEmpleado: TNumEmp): Boolean;
begin
     try
        cdsEmpleados.DisableControls;
        Result:= cdsEmpleados.Locate( 'CB_CODIGO', iEmpleado, [] );
     finally
            cdsEmpleados.EnableControls;
     end;
end;

function TdmSuper.GetEmpleadoNumero: TNumEmp;
begin
     Result := 0;
     with cdsEmpleados do
          if Active then
             Result := FieldByName('CB_CODIGO').AsInteger;
end;

function TdmSuper.GetEmpleadoNombre: String;
begin
     Result := VACIO;
     with cdsEmpleados do
          if Active then
             Result := FieldByName('PRETTYNAME').AsString;
end;

procedure TdmSuper.ResetFiltros;
begin
     with FRangoLista do
     begin
          TipoRango := raTodos;
          Inicial   := VACIO;
          Final     := VACIO;
          Lista     := VACIO;
     end;
     FCondicion := VACIO;
     FFiltro := VACIO;
     FFiltroFijo := 0;
     TressShell.SetControlFiltroFijo;
end;

procedure TdmSuper.SetFiltroFijo( const Value: Integer );
begin
     if ( FFiltroFijo <> Value ) then
     begin
          FFiltroFijo := Value;
          FNoPreguntarFiltro := FALSE;
     end;
end;

function TdmSuper.FiltroEmpleadosActivo: Boolean;
begin
     Result := ( FFiltroFijo > 0 ) or ( FRangoLista.TipoRango <> raTodos ) or
               strLleno( FCondicion ) or strLleno( FFiltro );
end;

function TdmSuper.PosicionaEmpleado( const iEmpleado: Integer ): Boolean;
begin
     Result := BuscaEmpleado( iEmpleado );
     if ( not Result ) then        { No es uno de mis empleados }
     begin
         Result := SetEmpleadoNumero( iEmpleado );
         if ( not Result ) then    { No es un empleado de esta compa�ia }
             zWarning( '� Atenci�n !', '��� Empleado No existe en esta compa�ia !!!', 0, mbOK )
         else
             zWarning( '� Atenci�n !', '��� Empleado No Asignado Para Esta Fecha !!!', 0, mbOK );
     end;
end;

function TdmSuper.VerificaFiltroEmpleados: Boolean;
begin
     Result := FNoPreguntarFiltro or ( not FiltroEmpleadosActivo );
     if not Result then
        case FCheckFiltroActivo.PreguntaFiltroActivo( FNoPreguntarFiltro ) of
             afQuitar : begin
                             ResetFiltros;
                             RefrescaMisEmpleados;
                             Result := TRUE;
                         end;
             afContinuar : Result := TRUE;
        end;
end;

function TdmSuper.ChecaSuperAnterior( iEmpleado: Integer; sSuper: String ): String;
begin
     Result := sSuper;
     if cdsEmpleados.Locate( 'CB_CODIGO', iEmpleado, []) then
        Result := cdsEmpleados.FieldByName( 'CB_NIVEL' ).AsString
     else
        if cdsMisAsignados.Locate( 'CB_CODIGO', iEmpleado, []) then
           Result := cdsMisAsignados.FieldByName( 'CB_NIVEL' ).AsString;
end;

{
function TdmSuper.ChecaAreaAnterior(iEmpleado: Integer; sArea: String): String;
begin
     Result := sArea;
     if cdsEmpleados.Locate( 'CB_CODIGO', iEmpleado, [] ) then
        Result := cdsEmpleados.FieldByName( 'CB_AREA' ).AsString;
end;
}
procedure tdmSuper.CdsSupervisoresFiltroActivos(BFiltrado:Boolean);
var
FiltroAdicional:string;
begin
  with dmSuper.cdsSupervisores do
     begin
        Filtered:=False;
        if(length(StringFiltroSuper) > 0) then FiltroAdicional := ' and ';
        if(Bfiltrado=True) then
        begin
              StringFiltroActivo := ' TB_ACTIVO = ''S''';
              Filter   := StringFiltroSuper +FiltroAdicional+ ' TB_ACTIVO = ''S''';
              Filtered := TRUE;
        end
        else
        begin
            StringFiltroActivo := '';
            Filter   := StringFiltroSuper ;
            Filtered := TRUE;
        end;
     end;
end;

procedure TdmSuper.SetFiltroSupervisores( const lMisSuper: Boolean );
var
FiltroAdicional:string;
begin
     with dmSuper.cdsSupervisores do
     begin
          Filtered := FALSE;
          if(length(StringFiltroActivo) > 0) then FiltroAdicional := ' and ';
          if lMisSuper then
             begin
             StringFiltroSuper   := 'MI_CODIGO <> '''' and MI_CODIGO IS NOT NULL';
             Filter := StringFiltroSuper + FiltroAdicional + StringFiltroActivo;
             end
          else
             begin
             StringFiltroSuper   := 'MI_CODIGO = '''' or MI_CODIGO IS NULL';
             Filter:=StringFiltroSuper + FiltroAdicional + StringFiltroActivo;
             end;
          Filtered := TRUE;
     end;

end;

function TdmSuper.EsEmpleadoPorKardex( const iEmpleado: Integer ): Boolean;
var
   lFiltros: Boolean;
begin
     Result := FALSE;
     if SetEmpleadoNumero( iEmpleado ) then
     begin
          cdsSupervisores.Conectar;
          lFiltros := cdsSupervisores.Filtered;
          try
             cdsSupervisores.Filtered := false;
             if cdsSupervisores.Locate( 'TB_CODIGO', cdsDatosEmpleado.FieldByName( 'CB_NIVEL' + IntToStr( Global.GetGlobalInteger(K_GLOBAL_NIVEL_SUPERVISOR) ) ).AsString, [] ) then
                Result := ( cdsSupervisores.FieldByName( 'MI_CODIGO' ).AsString <> VACIO );
          Finally
                 cdsSupervisores.Filtered := lFiltros;
          end;

     end;
end;

// Metodos de Posicionamiento de cdsDatosEmpleado

function TdmSuper.SetEmpleadoNumero(const Value: TNumEmp; const lForzaRefrescar: Boolean = FALSE ): Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with cdsDatosEmpleado do
             begin
                  if ( not Active ) or ( lForzaRefrescar ) or ( FieldByName( 'CB_CODIGO' ).AsInteger <> Value ) then
                  begin
                       Result := FetchEmployee( Value );
                       FEmpleadoBOF := False;
                       FEmpleadoEOF := False;
                  end
                  else
                       Result := True;
             end;
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmSuper.GetEmpleadoAnterior: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := FetchPreviousEmployee( FEmpleado );
             if Result then
                FEmpleadoEOF := False
             else
                FEmpleadoBOF := True;
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmSuper.GetEmpleadoPrimero: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := FetchNextEmployee( 0 );
             if Result then
             begin
                  FEmpleadoBOF := True;
                  FEmpleadoEOF := False;
             end;
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmSuper.GetEmpleadoSiguiente: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := FetchNextEmployee( FEmpleado );
             if Result then
                FEmpleadoBOF := False
             else
                FEmpleadoEOF := True;
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmSuper.GetEmpleadoUltimo: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := FetchPreviousEmployee( MAXINT );
             if Result then
             begin
                  FEmpleadoBOF := False;
                  FEmpleadoEOF := True;
             end;
          finally
             Cursor := oCursor;
          end;
     end;
end;

{ Metodos de ClientDataSets }

{ cdsEmpleados }

procedure TdmSuper.cdsEmpleadosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsEmpleados.Data := ServerSuper.GetEmpleados( Empresa, GetParametroscdsEmpleados.VarValues );
          cdsListaEmpleados.Data := cdsEmpleados.Data;
     // Se requieren otros dos clientDatasets :
     // en dmCliente : cdsEmpleado -> Permite usar el navegador de la forma de consulta ... Quizas no se ocupe
     // en dmSuper   : cdsKardexEmpleado -> Permite usar la fecha del Programa de Supervisores
     //                para obtener los datos del Empleado a una fecha dada ( KARDEX_CAMBIO_MULTIPLE_SELECT )
     //                este se usara para la Consulta de Datos del Empleado y para editar el KardexMultiple
     //                Se Posicionara en base a dmCliente.cdsEmpleado
     // en dmSuper   : cdsIdentFoto - > Si esta en la forma de consulta obtiene la foto del Empleado en base
     //                al Empleado Seleccionado en cdsKardexEmpleado
end;

procedure TdmSuper.cdsEmpleadosAlCrearCampos(Sender: TObject);
var
FindFIeld:Tfield;
begin
     cdsEmpleados.FieldByName( 'PRETTYNAME' ).OnGetText := PRETTYNAMEGetText;
     cdsEmpleados.FieldByName( 'CHECADAS' ).OnGetText := EmpCHECADASGetText;
     findField := cdsEmpleados.FindField('SortOrder');
     if not Assigned(findField) then
        CdsEmpleados.CreateCalculated('SortOrder',ftString,15 );
end;
procedure TdmSuper.cdsEmpleadosCalcFields(DataSet: TDataSet);
begin
    cdsEmpleados.FieldByName('SortOrder').asString := '4';
   if(cdsEmpleados.FieldByName('Checadas').AsInteger >0)
          then
          begin
            if(cdsEmpleados.FieldByName('Retardos').AsInteger >0)
            then
            begin
             //EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_RETARDOS_INDICE );
             cdsEmpleados.FieldByName('SortOrder').asString:='1';
            end
            else
            begin
            //EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_ASISTECIA_INDICE );
            cdsEmpleados.FieldByName('SortOrder').asString:='0';
            end;
          end
          else
          begin
          if(cdsEmpleados.FieldByName('Status').AsInteger =2)
          then
          begin
             //EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_VACACION_INDICE );
             cdsEmpleados.FieldByName('SortOrder').asString:='2';
          end
          else
            if(cdsEmpleados.FieldByName('Status').AsInteger =3)
            then
            begin
              //EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_INCAPACITA_INDICE );
              cdsEmpleados.FieldByName('SortOrder').asString:='3';
            end
            else
            if((DMCLiente.FechaSupervisor - Date())<1)then
              begin
              //EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_FALTA_INDICE );
              cdsEmpleados.FieldByName('SortOrder').asString:='4';
              end
              else
              begin
                cdsEmpleados.FieldByName('SortOrder').asString:='5';
              end;
          end;
end;

procedure TdmSuper.PRETTYNAMEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := PropperCase( Sender.AsString );
end;

procedure TdmSuper.EmpCHECADASGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := '';
end;

{ cdsTarjeta }

procedure TdmSuper.cdsTarjetaAlAdquirirDatos(Sender: TObject);
var
   oChecadas : OleVariant;
begin
     with dmCliente do
          cdsTarjeta.Data := ServerSuper.GetTarjeta( Empresa, EmpleadoNumero, FechaSupervisor, oChecadas );
     cdsChecadas.Data := oChecadas;
     FOldChecadas := GetChecadasStr;
end;

procedure TdmSuper.cdsTarjetaAlCrearCampos(Sender: TObject);
begin
     with cdsTarjeta do
     begin
          with dmCatalogos do
          begin
               CreateSimpleLookup( cdsHorarios, 'HO_DESCRIP', 'HO_CODIGO' );
               CreateSimpleLookup( cdsTurnos, 'TU_DESCRIP', 'CB_TURNO' );
               CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'CB_PUESTO' );
               CreateSimpleLookup( cdsClasifi, 'CLASIFI', 'CB_CLASIFI' );
               CreateSimpleLookup( cdsTPeriodos, 'NO_DESCRIP', 'CB_NOMINA' ); 
          end;
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'IN_ELEMENT', 'AU_TIPO' );
          CreateCalculated( 'AU_PERIODO', ftString, 20 );
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          ListaFija( 'AU_OUT2EAT', lfOut2Eat );
          ListaFija( 'AU_STA_FES', lfStatusFestivos );
          FieldByName( 'HO_CODIGO' ).OnChange := HO_CODIGOChange;
          FieldByName( 'AU_STATUS' ).OnChange := HO_CODIGOChange;
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmSuper.cdsTarjetaCalcFields(DataSet: TDataSet);
begin
     with Dataset do
          FieldByName( 'AU_PERIODO' ).AsString := ZetaCommonTools.ShowNomina( FieldByName( 'PE_YEAR' ).AsInteger,
                                                                              FieldByName( 'PE_TIPO' ).AsInteger,
                                                                              FieldByName( 'PE_NUMERO' ).AsInteger );
end;

procedure TdmSuper.cdsTarjetaNewRecord(DataSet: TDataSet);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Dataset do
        begin
             FieldByName( 'CB_CODIGO' ).AsInteger := dmSuper.EmpleadoNumero;
             FieldByName( 'AU_FECHA' ).AsDateTime := dmCliente.FechaSupervisor;
             with GetDatosClasificacion( FieldByName( 'CB_CODIGO' ).AsInteger, dmCliente.FechaSupervisor ) do
             begin
                  FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
                  FieldByName( 'CB_TURNO' ).AsString := Turno;
                  FieldByName( 'CB_PUESTO' ).AsString := Puesto;
                  FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
                  FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
                  FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
                  FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
                  FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
                  FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
                  FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
                  FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
                  FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
{$ifdef ACS}
                  FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
                  FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
                  FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
{$endif}
{$ifdef QUINCENALES}
                  FieldByName( 'CB_SALARIO' ).AsFloat := Salario;
{$endif}
                  FieldByName( 'CB_NOMINA').AsInteger := Nomina; 

             end;
             with GetStatusHorario( FieldByName( 'CB_TURNO' ).AsString, dmCliente.FechaSupervisor ) do
             begin
                  FieldByName( 'HO_CODIGO' ).AsString := Horario;
                  FieldByName( 'AU_STATUS' ).AsInteger := Ord( Status );
                  FieldByName( 'HORARIO_ANT' ).AsString := Horario;
                  FieldByName( 'STATUS_ANT' ).AsInteger := Ord( Status );
                  FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_NO;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmSuper.cdsTarjetaAlAgregar(Sender: TObject);
begin
     with cdsTarjeta do
     begin
          if IsEmpty then
             Append;
          Modificar;
     end;
end;

procedure TdmSuper.cdsTarjetaAlModificar(Sender: TObject);
begin
     with cdsChecadas do
     begin
          try
             Filtered := True;
             ZBaseEdicion_DevEx.ShowFormaEdicion( EditTarjeta, TEditTarjeta );
          finally
             Filtered := False;
          end;
     end;
end;

procedure TdmSuper.cdsTarjetaAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Tarjeta de Asistencia ?' ) then
     begin
          with cdsTarjeta do
          begin
               Delete;
               Enviar;
               TressShell.SetDataChange( [ enAusencia ] );
          end;
     end;
end;

procedure TdmSuper.cdsTarjetaBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          ValidaAutoBeforePost( acExtras, FieldByName( 'HRS_EXTRAS' ).AsFloat, FieldByName( 'M_HRS_EXTRAS' ) );
          ValidaAutoBeforePost( acDescanso, FieldByName( 'DESCANSO' ).AsFloat, FieldByName( 'M_DESCANSO' ) );
          ValidaAutoBeforePost( acConGoce, FieldByName( 'PER_CG' ).AsFloat, FieldByName( 'M_PER_CG' ) );
          ValidaAutoBeforePost( acConGoceEntrada, FieldByName( 'PER_CG_ENT' ).AsFloat, FieldByName( 'M_PER_CG_ENT' ) );
          ValidaAutoBeforePost( acSinGoce, FieldByName( 'PER_SG' ).AsFloat, FieldByName( 'M_PER_SG' ) );
          ValidaAutoBeforePost( acSinGoceEntrada, FieldByName( 'PER_SG_ENT' ).AsFloat, FieldByName( 'M_PER_SG_ENT' ) );
          ValidaAutoBeforePost( acPrepagFueraJornada, FieldByName( 'PRE_FUERA_JOR' ).AsFloat, FieldByName( 'M_PRE_FUERA_JOR' ) );
          ValidaAutoBeforePost( acPrepagDentroJornada, FieldByName( 'PRE_DENTRO_JOR' ).AsFloat, FieldByName( 'M_PRE_DENTRO_JOR' ) );


          if NOT dmCliente.PuedeCambiarTarjetaDlg( Dataset ) then
             Abort;


     end;
end;

procedure TdmSuper.cdsTarjetaAfterCancel(DataSet: TDataSet);
begin
     with cdsChecadas do
     begin
          if Active then
             CancelUpdates;
     end;
end;

procedure TdmSuper.cdsTarjetaAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsChecadas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
     with cdsTarjeta do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount = 0 ) and ( FOldChecadas <> GetChecadasStr ) then   //Solo Cambiaron las Checadas
          begin
               Edit;
               FieldByName( 'SETCAMBIOS' ).AsString := K_GLOBAL_SI;   // Forzar que lleve un Delta
               Post;
          end;
          if ( ChangeCount > 0 ) then
             Reconcile ( ServerSuper.GrabaTarjeta( dmCliente.Empresa, Delta, FOldChecadas, GetChecadasVar, ErrorCount ) );
     end;
end;

procedure TdmSuper.HO_CODIGOChange(Sender: TField);
begin
     if (not FCambiaComboFestivo) then
     begin
         with Sender.Dataset do
         begin
              FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_SI;
         end;
     end;

     if FCambiaComboFestivo then
        FCambiaComboFestivo := FALSE;
end;

procedure TdmSuper.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
        Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;

{ cdsChecadas }
procedure TdmSuper.DescansoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) in [ chDesconocido, chEntrada, chSalida, chInicio, chFin ] ) then
                Text := FormatFloat( '#0.00;;#', Sender.AsFloat )
          else
                Text := VACIO;

     end;
end;

procedure TdmSuper.AprobadasGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) in [ chConGoce, chSinGoce, chExtras, chDescanso, chConGoceEntrada, chSinGoceEntrada ] ) then
                Text := FormatFloat( '#0.00;;#', Sender.AsFloat )
          else
                Text := VACIO;
     end;
end;

procedure TdmSuper.cdsChecadasAlCrearCampos(Sender: TObject);
begin
     with cdsChecadas do
     begin
          ListaFija( 'CH_TIPO', lfTipoChecadas );
          ListaFija( 'CH_DESCRIP', lfDescripcionChecadas );
          MaskTime( 'CH_H_REAL' );
          MaskTime( 'CH_H_AJUS' );
          MaskHoras( 'CH_HOR_ORD' );
          MaskHoras( 'CH_HOR_EXT' );
          MaskHoras( 'CH_HOR_DES' );
          MaskHoras( 'CH_APRUEBA' );
          with FieldByName( 'CH_H_REAL' ) do
          begin
               OnSetText  := CH_H_REALSetText;
               OnValidate := CH_H_REALValidate;
               OnChange   := CH_H_REALChange;
          end;
          FieldByName( 'CH_H_AJUS' ).OnGetText := CH_H_AJUSGetText;
          FieldByName( 'CH_HOR_DES' ).OnGetText := DescansoGetText;
          FieldByName( 'CH_APRUEBA' ).OnGetText := AprobadasGetText;
          FieldByName( 'CH_RELOJ' ).OnGetText := CH_RELOJGetText; {ACL170409}
          with FieldByName( 'CH_MOTIVO' ) do
          begin
               OnGetText := CH_MOTIVOGetText; {ACL170409}
               OnValidate := cdsChecadaManualValidate;{ACL170409}
          end;
     end;
end;

{ACL170409}
procedure TdmSuper.CH_RELOJGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( PermitirCapturaMotivoCH ) AND ( EsTipoChecadaAutorizacion( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) ) ) then
             Text := VACIO
          else
                Text := Sender.AsString;
     end;
end;

{ACL170409}
procedure TdmSuper.CH_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     with Sender.DataSet do
     begin
          if ( PermitirCapturaMotivoCH ) AND ( EsTipoChecadaAutorizacion( eTipoChecadas( FieldByName( 'CH_TIPO' ).AsInteger ) ) ) then
             Text := FieldByName( 'CH_RELOJ' ).AsString
          else
              Text := Sender.AsString;
     end;
end;

{ACL170409}
procedure TdmSuper.cdsChecadaManualValidate(Sender: TField);
begin
     ValidaMotChecadaManual( Sender.AsString, Sender.DataSet.FieldByName('US_CODIGO').AsInteger);
     ValidaLookup( UpperCase(Sender.AsString),VACIO, dmTablas.cdsMotCheca );
end;

{ACL170409}
procedure TdmSuper.cdsChecadasBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if ( FieldByName( 'US_CODIGO' ).AsInteger <> 0 ) then
          begin
               if ( StrVacio( FieldByName( 'CH_MOTIVO' ).AsString ) ) then
               begin
                    if ( CapturaObligatoriaMotCH ) then
                       DataBaseError('Motivo no puede quedar vac�o ')
               end
               else
               begin //Lo hace por el agrupamiento en reportes de profesional
                    FieldByName('CH_MOTIVO').AsString:= UpperCase(FieldByName('CH_MOTIVO').AsString);
               end;
          end

     end;
end;

{ACL200409}
procedure TdmSuper.cdsChecadasAfterPost(DataSet: TDataSet);
begin
     FUltimoMotivoCH:= cdsChecadas.FieldByName( 'CH_MOTIVO' ).AsString;
end;

procedure TdmSuper.cdsChecadasAfterCambios(DataSet: TDataSet);
begin
     with cdsTarjeta do
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
end;

procedure TdmSuper.CH_H_REALSetText(Sender: TField; const Text: String);
begin
     Sender.AsString := ConvierteHora( Text );
end;

procedure TdmSuper.CH_H_REALValidate(Sender: TField);
var
   sHora: String;
begin
     sHora := Sender.AsString;
     if not ZetaCommonTools.ValidaHora( sHora, '48' ) then
     begin
          raise ERangeError.Create( 'Hora "' + Copy( sHora, 1, 2 ) + ':' +
                                    Copy( sHora, 3, 2 ) + '" Inv�lida' );
     end;
end;

procedure TdmSuper.CH_H_REALChange(Sender: TField);
begin
      with Sender.DataSet do
      begin
           FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
           with FieldByName('CH_MOTIVO') do
           begin
                if StrVacio(AsString) then
                  AsString:= FUltimoMotivoCH;
           end;

     end;
     // Checar Penalizaci�n
     ChecaPenalizacionChecada( Sender );
end;

procedure TdmSuper.ChecaPenalizacionChecada(Sender: TField);
var
   iPenal, iLimite: Integer;
begin
     iPenal := Global.GetGlobalInteger( K_GLOBAL_SUPER_PENALIZACION );
     if strLleno( Sender.AsString ) and ( iPenal > 0 ) then
        with Sender do
        begin
             iLimite := GetMinPenalizacion( GetHorarioCodigo( DataSet.Name ), iPenal );
             if ( aMinutos( AsString ) < iLimite ) then
                AsString := aHoraString( iLimite );
        end;
end;

procedure TdmSuper.CHECADAChange(Sender: TField);
begin
     ChecaPenalizacionChecada(Sender);

     with Sender.DataSet do
     begin
          FieldByName( 'US_' + Sender.FieldName ).AsInteger := dmCliente.Usuario;
          with FieldByName('MC_'+ Sender.FieldName) do
          begin
               if StrVacio(AsString) then
                 AsString:= FUltimoMotivoCH;
          end;

     end;
end;

procedure TdmSuper.CH_H_AJUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   iHoras: Integer;
begin
     Text := Sender.AsString;
     if ( Length( Text ) > 0 ) and ( Text <> K_HORA_NULA ) then
     begin
          iHoras := StrToInt( Text );
          if ( iHoras >= 2400 ) then
             iHoras := iHoras - 2400;
          Text := Format( '%4.4d', [ iHoras ] );
          Text := Copy( Text, 1, 2 ) + ':' + Copy( Text, 3, 2 );
     end;
end;

{ACL 150409}
procedure TdmSuper.cdsChecadasAfterOpen(DataSet: TDataSet);
begin
     FUltimoMotivoCH:= VACIO;
end;

{ cdsGridAsistencia }

procedure TdmSuper.cdsGridAsistenciaAlAdquirirDatos(Sender: TObject);
begin
     // ServerSuper.GetListaAsistencia()
end;

procedure TdmSuper.cdsGridAsistenciaAlCrearCampos(Sender: TObject);
begin
     cdsGridAsistenciaAlCrearCamposGenerales( Sender );
     cdsGridAsistenciaAlCrearCamposDefaults( Sender );
end;

procedure TdmSuper.cdsGridAsistenciaAlCrearCamposGenerales(Sender: TObject);
var
   i: Integer;
begin
     with TZetaClientDataSet(Sender) do
     begin
          with dmCatalogos do
          begin
               cdsHorarios.Conectar;
               CreateSimpleLookup( cdsHorarios, 'HO_DESCRIP', 'HO_CODIGO' );
          end;
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'IN_ELEMENT', 'AU_TIPO' );
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_OUT2EAT', lfOut2Eat );
          MaskHoras( 'AU_HORASCK' );
          MaskHoras( 'AU_NUM_EXT' );
          MaskHoras( 'HRS_EXTRAS' );
          MaskHoras( 'DESCANSO' );
          MaskHoras( 'PER_CG' );
          MaskHoras( 'PER_SG' );
          MaskHoras( 'PER_CG_ENT' );
          MaskHoras( 'PER_SG_ENT' );
          MaskHoras( 'PRE_FUERA_JOR' );
          MaskHoras( 'PRE_DENTRO_JOR' );
          FieldByName( 'HO_CODIGO' ).OnChange := HO_CODIGOChange;
          FieldByName( 'HO_CODIGO' ).OnValidate := cdsHorarioValidate;
          FieldByName( 'AU_STATUS' ).OnChange := HO_CODIGOChange;
          for i := 1 to 4 do
          begin
               MaskTime( 'CHECADA' + IntToStr( i ) );
               with FieldByName( 'CHECADA' + IntToStr( i ) ) do
               begin
                    OnSetText := CH_H_REALSetText;
                    OnValidate := CH_H_REALValidate;
                    OnChange   := CHECADAChange;
               end;

               with FieldByName('MC_CHECADA' + IntToStr( i ) ) do
               begin
                    OnValidate:= cdsMotChecadaValidate;
                    OnChange:= cdsMotChecadaChange;
               end;
          end;


          FieldByName( 'M_HRS_EXTRAS' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_DESCANSO' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_CG' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_CG_ENT' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_SG' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PER_SG_ENT' ).OnValidate := cdsMotAutoValidate;
          FieldByName( 'M_PRE_FUERA_JOR' ).OnValidate:= cdsMotAutoValidate;
          FieldByName( 'M_PRE_DENTRO_JOR' ).OnValidate:= cdsMotAutoValidate;

          dmSistema.cdsUsuarios.Conectar;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_HRS_EXTRAS', 'OK_HRS_EXTRAS' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCANSO', 'OK_DESCANSO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_CG', 'OK_PER_CG' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_CG_ENT', 'OK_PER_CG_ENT' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_SG', 'OK_PER_SG' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_PER_SG_ENT', 'OK_PER_SG_ENT' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookup, 'US_PRE_FUERA_JOR', 'OK_PRE_FUERA_JOR' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookup, 'US_PRE_DENTRO_JOR','OK_PRE_DENTRO_JOR' );
     end;
end;

procedure TdmSuper.cdsMotChecadaValidate(Sender: TField);
const
     K_ERRROR_MOTIVO = 'No se puede especificar motivo: ';
var
   sCampoUsuario, sCampoChecada: String;
begin
     { Revisa que exista valor en la checada y que sea una checada manual }
     sCampoUsuario:= StrTransform( Sender.FieldName, 'MC_', 'US_');
     sCampoChecada:= StrTransform( Sender.FieldName, 'MC_', VACIO );

     with Sender.DataSet do
     begin
          if StrLleno(Sender.AsString) then
          begin
               if StrLleno( FieldByName(sCampoChecada).AsString ) then
               begin
                    if FieldByName(sCampoUsuario).AsInteger = 0  then
                       DataBaseError( K_ERRROR_MOTIVO + 'No es una checada manual' )
                    else
                        ValidaLookup( UpperCase(Sender.AsString),VACIO, dmTablas.cdsMotCheca );
               end
               else
                   DataBaseError(K_ERRROR_MOTIVO + 'No se ha registrado checada' );
          end;
     end;
end;

procedure TdmSuper.cdsMotChecadaChange(Sender: TField);
begin
     if StrLleno( Sender.AsString ) then  //Guarda ultimo motivo
        FUltimoMotivoCh:= Sender.AsString;
end;

procedure TdmSuper.cdsGridAsistenciaAlCrearCamposDefaults(Sender: TObject);
begin
     with TZetaClientDataSet(Sender) do
     begin
          FieldByName( 'HRS_EXTRAS' ).OnChange :=  GetDefaultCampo;
          FieldByName( 'DESCANSO' ).OnChange := GetDefaultCampo;
          FieldByName( 'PER_CG' ).OnChange := GetDefaultCampo;
          FieldByName( 'PER_CG_ENT' ).OnChange := GetDefaultCampo;
          FieldByName( 'PER_SG' ).OnChange := GetDefaultCampo;
          FieldByName( 'PER_SG_ENT' ).OnChange := GetDefaultCampo;
          FieldByName( 'PRE_FUERA_JOR' ).OnChange := GetDefaultCampo;
          FieldByName( 'PRE_DENTRO_JOR' ).OnChange := GetDefaultCampo;
     end;
end;

procedure TdmSuper.cdsGridTarjetasPeriodoBeforeOpen( DataSet: TDataSet);
begin
     FUltimoMotivoCH:= VACIO;
end;

procedure TdmSuper.GetDefaultCampo(Sender: TField );

  procedure SetDefaultMotivo( sCampo, sParam: String );
   const
        K_MOTIVO = 'M_';
   begin
        with cdsGridAsistencia, FParamsAsistencia do
        begin
             if ( Sender.FieldName = sCampo ) and ( ParamByName(sParam).AsFloat = 0 ) then
                FieldByName(K_MOTIVO + sCampo ).AsString:= ParamByName( K_MOTIVO + sParam).AsString;
        end;
   end;

begin
     with cdsGridAsistencia do
     begin
          if ( FieldByName(Sender.FieldName).AsFloat > 0 ) then
          begin
               SetDefaultMotivo( 'HRS_EXTRAS', 'D_EXTRAS' );
               SetDefaultMotivo( 'DESCANSO', 'D_DES_TRA' );
               SetDefaultMotivo( 'PER_CG', 'D_PER_CG' );
               SetDefaultMotivo( 'PER_CG_ENT', 'D_CG_ENT' );
               SetDefaultMotivo( 'PER_SG', 'D_PER_SG' );
               SetDefaultMotivo( 'PER_SG_ENT', 'D_SG_ENT' );
               SetDefaultMotivo( 'PRE_FUERA_JOR', 'D_PRE_FUERA_JOR');
               SetDefaultMotivo( 'PRE_DENTRO_JOR', 'D_PRE_DENTRO_JOR');
          end
          else if FieldByName(Sender.FieldName).IsNull then
               FieldByName(Sender.FieldName).AsFloat := 0;
     end;
end;

procedure TdmSuper.ValidaLookup( const sValor,sFiltro : String ; oLookup: TZetaLookupDataSet  );
const
     sMensaje = 'No Existe el C�digo: %s en %s';
     sMensajeInactivo = 'No est� Activo el C�digo: %s en %s';
var
   sDescrip: String;
   lActivo : Boolean;
   lConfidential : Boolean;
begin
     lActivo := True;
     lConfidential := True;
     if strLleno( sValor ) then
        with oLookup do
             if ( not LookupKey( sValor, sFiltro, sDescrip, lActivo, lConfidential) ) then
                             DataBaseError( Format( sMensaje, [ sValor, LookupName ] ) )
             else
             if ( not lActivo ) then
                          DataBaseError( Format( sMensajeInactivo, [ sValor, LookupName ] ) );
end;

function TdmSuper.GetTipoMotivo( sCampo: String ): Integer;
begin
     Result:= 0;
     if( sCampo = 'M_HRS_EXTRAS' ) Then
        Result := Ord(acExtras);
     if( sCampo = 'M_DESCANSO' )then
         Result := Ord(acDescanso );
     if( sCampo = 'M_PER_CG' ) Then
            Result := Ord(acConGoce );
     if( sCampo = 'M_PER_CG_ENT' ) Then
         Result :=  Ord(acConGoceEntrada );
     if( sCampo = 'M_PER_SG' ) Then
         Result := Ord(acSinGoce );
     if ( sCampo = 'M_PER_SG_ENT' ) Then
        Result :=  Ord(acSinGoceEntrada );
     if( sCampo = 'M_PRE_FUERA_JOR') Then
         Result:=  Ord(acPrepagFueraJornada );
     if( sCampo = 'M_PRE_DENTRO_JOR') Then
         Result:=  Ord(acPrepagDentroJornada );
end;

procedure TdmSuper.cdsMotAutoValidate(Sender: TField);
begin
     ValidaLookup( UpperCase(Sender.AsString),Format( 'TB_TIPO = %d OR TB_TIPO = %d',[ K_MOTIVO_AUTORIZ_OFFSET , GetTipoMotivo( Sender.FieldName )  ] ), dmTablas.cdsMotAuto );
end;

procedure TdmSuper.cdsHorarioValidate(Sender: TField);
begin
     ValidaLookup( Sender.AsString,VACIO, dmCatalogos.cdsHorarios );
end;

procedure TdmSuper.cdsGridAsistenciaAlEnviarDatos(Sender: TObject);
begin
     with cdsGridAsistencia do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;
end;

function TdmSuper.GetFiltroEmpleado( const iFiltro: Integer ): String;
const
     K_TODOS = 0;
     K_HRS_EXTRAS_AUTORIZADAS = 1;
     K_HRS_EXTRAS_NO_AUTORIZADAS = 2;
     K_CHECADAS_INCOMPLETAS = 3;
     K_SIN_CHECADAS = 4;
     K_HRS_AUTO_PARCIALES = 5;

begin
     with dmSuper.cdsGridAsistencia do
     begin
          case iFiltro of
               K_TODOS:                     Result:= VACIO;
               K_HRS_EXTRAS_AUTORIZADAS:    Result:= 'HRS_EXTRAS > 0';
               K_HRS_EXTRAS_NO_AUTORIZADAS: Result:= 'AU_NUM_EXT > HRS_EXTRAS';
               K_CHECADAS_INCOMPLETAS:      Result:= Format( '( CHECADA1 = %0:s AND CHECADA2 <> %0:s  ) OR ( CHECADA2 = %0:s AND CHECADA1 <> %0:s ) OR'+
                                                             '( CHECADA3 = %0:s AND CHECADA4 <> %0:s ) OR ( CHECADA4 = %0:s AND CHECADA3 <> %0:s ) ',[EntreComillas(VACIO)]);
               K_SIN_CHECADAS:              Result:= Format( '( CHECADA1 = %0:s AND CHECADA2 = %0:s AND CHECADA3 = %0:s AND CHECADA4 = %0:s )',[EntreComillas( VACIO )]);
               K_HRS_AUTO_PARCIALES:        Result:='( HRS_EXTRAS > 0 ) AND ( AU_NUM_EXT > HRS_EXTRAS )';
          end;
     end;
end;

function TdmSuper.RefrescaLista( dFechaAsistencia: TDate ): Boolean;
var
   dFechaAnterior: TDate;
   lGuardaCambios: Boolean;
begin

     lGuardaCambios:= ( CambioGrid and ZWarningConfirm('Autorizaciones Colectivas',
                                                       '� Desea guardar los cambios ?', 0, mbOk ) )
                                                       or ( ( not CambioGrid and ( cdsGridAsistencia.ChangeCount > 0 )
                                                       and ZWarningConfirm('Autorizaciones Colectivas',
                                                                           '� Desea ejecutar el proceso de autorizaciones colectivas ?', 0, mbOk ) ) );

     Result:= ( dmCliente.PuedeCambiarTarjetaDlg( dFechaAsistencia, 0 ) );


     if ( Result ) then
     begin
          dFechaAnterior:= FParamsAsistencia.ParamByName('FECHA_AJUSTE').AsDateTime;
          FParamsAsistencia.ParamByName('FECHA_AJUSTE').AsDateTime:= dFechaAsistencia;
          with cdsGridAsistencia do
          begin
               if ( lGuardaCambios ) then
               begin
                    Enviar;
                    if AjusteColectivoGrabaLista( NIL, FParamsAsistencia ) then
                       TressShell.SetDataChange( [ enAusencia ] );
               end;

               AjusteColectivoGetLista( FParamsAsistencia );
               Result:= not IsEmpty;
               if not Result then
               begin
                    ZetaDialogo.zInformation( '� Atenci�n !', 'La Lista A Verificar Est� Vac�a', 0 );
                    FParamsAsistencia.ParamByName('FECHA_AJUSTE').AsDateTime:= dFechaAnterior;
                    AjusteColectivoGetLista( FParamsAsistencia );
               end;
          end;
     end;
end;

procedure TdmSuper.AjusteColectivoGetLista(Parametros: TZetaParams);
begin
     with cdsGridAsistencia do
     begin
          FParamsAsistencia.VarValues:= Parametros.VarValues;
          FCambioGrid:= FALSE;
          Data := ServerSuper.AjusteColectivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;


function TdmSuper.AjusteColectivoGrabaLista( oWizard: TWizAsistAjusteColectivo_DevEx; Parametros: TZetaParams): Boolean;
var
   ErrorCount : Integer;
   Resultado : OleVariant;
   lEsWizard: Boolean;

begin
     //Result:= TRUE;
     ErrorCount := 0;
     lEsWizard:= ( oWizard <> NIL );
     with cdsGridAsistencia do
     begin
          {
          if ChangeCount > 0 then      // No se valida con ChangeCount por que se ocupa registrar la corrida del proceso, a�n si no se hicieron cambios
          begin
          }
          if lEsWizard then
             oWizard.SetAnimacion( TRUE );
          Parametros.AddInteger( 'Cambios', ChangeCount );
          Resultado := ServerSuper.GrabaListaAsistencia( dmCliente.Empresa, DeltaNull, Parametros.VarValues, ErrorCount );
          if lEsWizard then
             oWizard.SetAnimacion( FALSE );
          Result := Check( Resultado );
          {
          end;
          }
     end;
end;


{ cdsHisPermisos }

procedure TdmSuper.cdsHisPermisosAlAdquirirDatos(Sender: TObject);
begin
     cdsHisPermisos.Data := ServerRecursos.GetHisPermiso(dmCliente.Empresa, EmpleadoNumero );
end;

procedure TdmSuper.cdsHisPermisosAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisPermisos do
     begin
          ListaFija( 'PM_CLASIFI', lfTipoPermiso );
          MaskFecha( 'PM_FEC_INI' );
          MaskFecha( 'PM_FEC_FIN' );
          FieldByName('PM_FEC_INI').OnChange := OnPM_FEC_INIChange;
          FieldByName('PM_DIAS').OnChange := OnPM_FEC_INIChange;
          FieldByName('PM_FEC_FIN').OnChange := OnPM_FEC_FINChange;
          FieldByName( 'PM_CLASIFI' ).OnValidate := OnPM_CLASIFIValidate;
          FieldByName( 'PM_CLASIFI' ).OnChange := OnPM_CLASIFIChange;

          CreateSimpleLookup( dmTablas.cdsIncidencias, 'TB_ELEMENT', 'PM_TIPO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmSuper.cdsHisPermisosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditPermisos, TEditPermisos );
end;

procedure TdmSuper.cdsHisPermisosNewRecord(DataSet: TDataSet);
begin
     with cdsHisPermisos do
     begin
          FieldByName('CB_CODIGO').AsInteger := EmpleadoNumero;
          FieldByName('PM_FEC_INI').AsDateTime := dmCliente.FechaSupervisor;
          FieldByName('PM_DIAS').AsInteger := 1;
          FieldByName('PM_CLASIFI').AsInteger := Ord( tpSinGoce);
          FieldByName('PM_GLOBAL' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmSuper.cdsHisPermisosBeforePost(DataSet: TDataSet);
begin
     with cdsHisPermisos do
     begin
          if ( FieldByName( 'PM_DIAS').AsInteger <= 0 ) then
              DataBaseError( 'Dias de Permiso debe ser mayor a 0');
          if not StrLleno(FieldByName( 'PM_TIPO').AsString ) then
              DataBaseError( 'Tipo de Permiso no puede quedar vac�o') ;
          if (FieldByName( 'PM_FEC_FIN').AsDateTime < (FieldByName( 'PM_FEC_INI').AsDateTime + 1)) then
             DataBaseError('Fecha de regreso debe ser mayor/igual a Fecha Inicio m�s 1 d�a');
          FieldByName( 'US_CODIGO').AsInteger   := dmCliente.Usuario;
          FieldByName( 'PM_CAPTURA').AsDateTime := dmCliente.FechaDefault;
     end;
end;

procedure TdmSuper.cdsHisPermisosAfterDelete(DataSet: TDataSet);
begin
     cdsHisPermisos.Enviar;
end;

procedure TdmSuper.cdsHisPermisosAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisPermisos do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               FCancelaAjuste := FALSE;
               Repeat
               Until ( FCancelaAjuste or Reconcile( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) );
               if ErrorCount = 0 then
                  TressShell.SetDataChange( [ enPermiso ] );
          end;
{
          if ChangeCount > 0 then
             if Reconcile ( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
                TressShell.SetDataChange( [ enPermiso ] );
}
     end;
end;

{$ifdef VER130}
procedure TdmSuper.cdsHisPermisosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccion( DataSet, FCancelaAjuste, K_SUB_PERMISO, 'Error al Registrar Permiso', E.Message );
end;
{$else}
procedure TdmSuper.cdsHisPermisosRenconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccionGetDias( DataSet, FCancelaAjuste, K_SUB_PERMISO, 'Error al Registrar Permiso', E.Message, GetPermisoDiasHabiles );
end;
{$endif}

procedure TdmSuper.OnPM_FEC_INIChange(Sender : TField);
var
   DiasRango : Double; // ??
   oCursor: TCursor;
begin 
     cdsHisPermisos.FieldByName( 'PM_FEC_FIN').OnChange := nil;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     with cdsHisPermisos do
     begin
          if Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
          begin
               FieldByName( 'PM_FEC_FIN').AsDateTime := GetPermisoFechaRegreso(dmCliente.Empleado,FieldByName( 'PM_FEC_INI').AsDateTime,FieldByName( 'PM_DIAS').AsInteger, DiasRango);
          end
          else
          begin
               FieldByName( 'PM_FEC_FIN').AsDateTime := FieldByName( 'PM_FEC_INI').AsDateTime + FieldByName( 'PM_DIAS').AsInteger;
          end;
     end;
     Screen.Cursor := oCursor;
     cdsHisPermisos.FieldByName( 'PM_FEC_FIN').OnChange := OnPM_FEC_FINChange;
end;

procedure TdmSuper.OnPM_FEC_FINChange(Sender : TField);
var
   oCursor: TCursor;
begin
     cdsHisPermisos.FieldByName( 'PM_DIAS').OnChange := nil;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     cdsHisPermisos.FieldByName( 'PM_DIAS').AsFloat := GetPermisoDiasHabiles(dmCliente.Empleado,cdsHisPermisos.FieldByName( 'PM_FEC_INI').AsDateTime,cdsHisPermisos.FieldByName( 'PM_FEC_FIN').AsDateTime);
     Screen.Cursor := oCursor;
     cdsHisPermisos.FieldByName( 'PM_DIAS').OnChange := OnPM_FEC_INIChange;
end;

procedure TdmSuper.OnPM_CLASIFIChange(Sender: TField);
begin
     if( not ValidarMotivoPermisos(cdsHisPermisos.FieldByName('PM_TIPO').AsString,cdsHisPermisos.FieldByName('PM_CLASIFI').AsInteger ) )then
          cdsHisPermisos.FieldByName('PM_TIPO').AsString := VACIO;
end;


function TdmSuper.ValidarMotivoPermisos(const sCodigo :string;const iTipo:Integer):Boolean;
begin
     Result := StrVacio(sCodigo);
     if Result  then
     begin
          with dmTablas.cdsIncidencias do
          begin
               Result := ( not Locate('TB_CODIGO',sCodigo,[]) ) and
                         ( ( FieldByName('TB_PERMISO').AsInteger = K_MOTIVO_PERMISOS_OFFSET ) or
                           ( FieldByName('TB_PERMISO').AsInteger = iTipo + 1 )
                         );
          end;
     end;
end;




procedure TdmSuper.OnPM_CLASIFIValidate(Sender: TField);
begin
     with Sender do
          if not DerechoTipoPermiso( AsInteger ) then
             DataBaseError( 'No Tiene Derecho para Permisos de Clase = ' + ObtieneElemento( lfTipoPermiso, AsInteger ) );
end;

function TdmSuper.DerechoTipoPermiso( const Tipo : Integer ): Boolean;
begin
     case eTipoPermiso( Tipo ) of
          tpConGoce : Result := ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_PERMISO, TP_CON_GOCE );
          tpFaltaJustificada : Result := ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_PERMISO, TP_FALTA_JUST );
          tpSuspension : Result := ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_PERMISO, TP_SUSPENSION );
          tpOtros : Result := ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_PERMISO, TP_OTROS );
     else
          Result := TRUE;
     end;
end;

{ cdsHisKardex }

procedure TdmSuper.cdsHisKardexAlAdquirirDatos(Sender: TObject);
var
   oHistoriales : OleVariant;
begin
     with dmCliente do
          cdsHisKardex.Data := ServerSuper.GetHisKardex( Empresa, EmpleadoNumero, oHistoriales );
     // Agregar Otros Historiales ( VACA,PERM,INCA )
     cdsOtrosHistoriales.Data := oHistoriales;
     AgregarOtrosHistoriales;
     // Aqui se haran las ediciones de Kardex General, por lo tanto deber� incluir todos los campos de Kardex
     // La edicion de Kardex Multiple se hara con clientDataset de dmCliente-> cdsEmpleado
end;

procedure TdmSuper.cdsHisKardexAlCrearCampos(Sender: TObject);
begin
     with cdsHisKardex do
     begin
          MaskFecha( 'CB_FECHA' );
          ListaFija( 'CB_STATUS', lfStatusKardex );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmSuper.cdsHisKardexAlModificar(Sender: TObject);
begin
    ZBaseEdicion_DevEx.ShowFormaEdicion( KardexGeneral, TKardexGeneral );
end;

procedure TdmSuper.cdsHisKardexAlAgregar(Sender: TObject);
const
     K_KARDEX_NINGUNO  = -1;
     K_KARDEX_GRAL = 0;
     K_KARDEX_MULTIPLE = 1;
var
   CualKardex : Integer;

begin
     CualKardex:= K_KARDEX_NINGUNO;
     ZBaseDlgModal_DevEx.ShowDlgModal( KardexAltaDlg, TKardexAltaDlg );
     if Assigned( KardexAltaDlg ) then
     begin
          with KardexAltaDlg do
          begin
               if ModalResult = mrOk then
                  CualKardex:= TipoAlta.ItemIndex;
          end;
     end;

     case CualKardex of
          K_KARDEX_GRAL :
             if ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_KARDEX, TKARDEX_GRAL ) then
             begin
                  with cdsHisKardex do
                  begin
                       Append;
                       Modificar;
                       RefrescaKardex( VACIO );
                  end;
             end
             else
                  ZetaDialogo.zInformation( 'Kardex General', 'No Tiene Autorizaci�n Para Agregar Registros Generales de Kardex', 0 );
          K_KARDEX_MULTIPLE :
                  with cdsDatosEmpleado do
                  begin
                       ObtenerFoto := FALSE;
                       Refrescar;
                       Modificar;
                       RefrescaKardex( K_T_MULTIPLE );        // Si se indica Tipo MULTIP, se posiciona solo por Fecha
                  end;
     end;

end;

procedure TdmSuper.cdsHisKardexNewRecord(DataSet: TDataSet);
begin
     with cdsHisKardex do
     begin
          FieldByName( 'CB_CODIGO').AsInteger := EmpleadoNumero;
          FieldByName( 'CB_FECHA').AsDateTime := dmCliente.FechaSupervisor;
          FieldByName( 'CB_GLOBAL').AsString  := 'N';
          FieldByName( 'CB_NIVEL').AsInteger  := 5;
          FieldByName( 'CB_STATUS').AsInteger := Ord( skCapturado );
     end;
end;

procedure TdmSuper.cdsHisKardexAfterDelete(DataSet: TDataSet);
begin
     BorrandoKardex:= TRUE;
     cdsHisKardex.Enviar;
     BorrandoKardex:= FALSE;
end;

procedure TdmSuper.cdsHisKardexAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisKardex do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if BorrandoKardex then
                  Reconcile( ServerRecursos.GrabaBorrarKardex( dmCliente.Empresa, Delta, ErrorCount ) )
               else
               begin
                    Reconcile ( ServerRecursos.GrabaHisKardex( dmCliente.Empresa, Delta, VACIO, ErrorCount ) );
                    if ErrorCount = 0 then
                       RefrescaHisKardex:= TRUE;
               end;
          end;
     end;
end;

procedure TdmSuper.AgregarOtrosHistoriales;
begin
     with cdsOtrosHistoriales do
     begin
          First;
          while not EOF do
          begin
               cdsHisKardex.Append;
               cdsHisKardex.FieldByName( 'CB_FECHA' ).AsDateTime := FieldByName( 'CB_FECHA' ).AsDateTime;
               cdsHisKardex.FieldByName( 'CB_TIPO' ).AsString := FieldByName( 'CB_TIPO' ).AsString;
               cdsHisKardex.FieldByName( 'CB_COMENTA' ).AsString := FieldByName( 'CB_COMENTA' ).AsString;
               cdsHisKardex.FieldByName( 'CB_MONTO' ).AsFloat := FieldByName( 'CB_MONTO' ).AsFloat;
               cdsHisKardex.FieldByName( 'US_CODIGO' ).AsInteger := FieldByName( 'US_CODIGO' ).AsInteger;
               cdsHisKardex.Post;
               Next;              // FDataSet
          end;
     end;
     with cdsHisKardex do
     begin
          MergeChangeLog;    // Para que no se consideren en el Delta
          First;
     end;
end;

{ cdsPlazas }


function TdmSuper.PreparaPlaza( const iEmpleado: Integer ): Boolean;
begin
     with cdsPlazas do
     begin
          Data:= ServerRecursos.GetEmpleadoPlaza( dmCliente.Empresa, iEmpleado );
          Result:= not IsEmpty;
          if Result then
          begin
               Edit;
               FieldByName('PU_CODIGO').AsString:= cdsDatosEmpleado.FieldByName('CB_PUESTO').AsString;
               FieldByName('PL_CLASIFI').AsString:= cdsDatosEmpleado.FieldByName('CB_CLASIFI').AsString;
               FieldByName('PL_TURNO').AsString:= cdsDatosEmpleado.FieldByName('CB_TURNO').AsString;
               FieldByName('PL_NIVEL1').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL1').AsString;
               FieldByName('PL_NIVEL2').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL2').AsString;
               FieldByName('PL_NIVEL3').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL3').AsString;
               FieldByName('PL_NIVEL4').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL4').AsString;
               FieldByName('PL_NIVEL5').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL5').AsString;
               FieldByName('PL_NIVEL6').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL6').AsString;
               FieldByName('PL_NIVEL7').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL7').AsString;
               FieldByName('PL_NIVEL8').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL8').AsString;
               FieldByName('PL_NIVEL9').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL9').AsString;
{$ifdef ACS}
               FieldByName('PL_NIVEL10').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL10').AsString;
               FieldByName('PL_NIVEL11').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL11').AsString;
               FieldByName('PL_NIVEL12').AsString:= cdsDatosEmpleado.FieldByName('CB_NIVEL12').AsString;
{$endif}
               PostData;
          end
          else
              ZError( 'Cambios M�ltiples',  Format( 'Empleado %d no tiene plaza asignada', [iEmpleado] ), 0 );

     end;
end;

function TdmSuper.GrabaKardexPlaza( const iEmpleado: Integer ): Integer;
var
   ErrorCount: Integer;
begin
     with cdsDataSet do
     begin
          Data:= ServerRecursos.GetEditHisKardex( dmCliente.Empresa, iEmpleado, 0, '' );
          if not ( State in [ dsInsert, dsEdit ]  )then
          begin
               Append;
               FieldByName( 'CB_CODIGO').AsInteger := iEmpleado;
               FieldByName( 'CB_GLOBAL').AsString  := 'N';
               FieldByName( 'CB_REINGRE').AsString := 'N';
               FieldByName( 'CB_AUTOSAL').AsString := 'N';
               FieldByName( 'CB_NIVEL').AsInteger  := 5;
               FieldByName('CB_TIPO').AsString:= K_T_PLAZA;

               FieldByName( 'CB_STATUS').AsInteger := Ord( skCapturado );
               FieldByName( 'CB_NOMINA').AsInteger := Ord( dmCliente.PeriodoTipo );
               FieldByName( 'US_CODIGO' ).AsInteger:= dmCliente.Usuario;

               FieldByName('CB_FECHA').AsDateTime:= OtrosDatos[K_FECHA];
               FieldByName('CB_COMENTA').AsString:= OtrosDatos[K_OBSERVA];

               FieldByName('CB_PLAZA').AsString := cdsPlazas.FieldByName('PL_FOLIO').AsString;
               FieldByName('CB_PUESTO').AsString:= cdsPlazas.FieldByName('PU_CODIGO').AsString;
               FieldByName('CB_CLASIFI').AsString:= cdsPlazas.FieldByName('PL_CLASIFI').AsString;
               FieldByName('CB_TURNO').AsString:= cdsPlazas.FieldByName('PL_TURNO').AsString;
               FieldByName('CB_TURNO').AsString:= cdsPlazas.FieldByName('PL_TURNO').AsString;
               FieldByName('CB_NIVEL1').AsString:= cdsPlazas.FieldByName('PL_NIVEL1').AsString;
               FieldByName('CB_NIVEL2').AsString:= cdsPlazas.FieldByName('PL_NIVEL2').AsString;
               FieldByName('CB_NIVEL3').AsString:= cdsPlazas.FieldByName('PL_NIVEL3').AsString;
               FieldByName('CB_NIVEL4').AsString:= cdsPlazas.FieldByName('PL_NIVEL4').AsString;
               FieldByName('CB_NIVEL5').AsString:= cdsPlazas.FieldByName('PL_NIVEL5').AsString;
               FieldByName('CB_NIVEL6').AsString:= cdsPlazas.FieldByName('PL_NIVEL6').AsString;
               FieldByName('CB_NIVEL7').AsString:= cdsPlazas.FieldByName('PL_NIVEL7').AsString;
               FieldByName('CB_NIVEL8').AsString:= cdsPlazas.FieldByName('PL_NIVEL8').AsString;
               FieldByName('CB_NIVEL9').AsString:= cdsPlazas.FieldByName('PL_NIVEL9').AsString;
{$ifdef ACS}
               FieldByName('CB_NIVEL10').AsString:= cdsPlazas.FieldByName('PL_NIVEL10').AsString;
               FieldByName('CB_NIVEL11').AsString:= cdsPlazas.FieldByName('PL_NIVEL11').AsString;
               FieldByName('CB_NIVEL12').AsString:= cdsPlazas.FieldByName('PL_NIVEL12').AsString;
{$endif}
               FieldByName( 'CB_PATRON' ).AsString:= cdsPlazas.FieldByName( 'PL_PATRON' ).AsString;

               PostData;
               if ( ChangeCount > 0 ) then
                  cdsDatosEmpleado.Reconcile ( ServerRecursos.GrabaHisKardex( dmCliente.Empresa, Delta, VACIO, ErrorCount) );
          end;
          Result:= ErrorCount;
     end;


end;

{ cdsDatosEmpleado }

procedure TdmSuper.cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
begin
     with cdsEmpleados do
     begin
          if cdsEmpleados.IsEmpty then
             GetEmpleadoPrimero
          else
          begin
               FetchEmployee( cdsEmpleados.FieldByName( 'CB_CODIGO' ).AsInteger );
               FEmpleadoBOF := False;
               FEmpleadoEOF := False;
          end;
     end;
end;

procedure TdmSuper.cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
begin
     with dmCatalogos, cdsDatosEmpleado, dmLabor do
     begin
          cdsArea.Conectar;
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
          CreateSimpleLookup( cdsTurnos, 'TU_DESCRIP', 'CB_TURNO' );
          CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'CB_PUESTO' );
          CreateSimpleLookup( cdsClasifi, 'TB_ELEMENT', 'CB_CLASIFI' );
          CreateSimpleLookup( cdsArea, 'TB_ELEMENT2', 'CB_AREA' );
     end;
end;

procedure TdmSuper.cdsDatosEmpleadoAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( KardexVarios, TKardexVarios );
end;


procedure TdmSuper.cdsDatosEmpleadoAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;

begin
     ErrorCount := 0;
     with cdsDatosEmpleado do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               if dmCliente.UsaPlazas then
               begin
                    if ( PreparaPlaza( FieldByName('CB_CODIGO').AsInteger ) ) then
                    begin
                         if ( cdsPlazas.ChangeCount > 0 ) then
                            Reconcile( ServerRecursos.GrabaPlazas( dmCliente.Empresa, cdsPlazas.Delta, OtrosDatos[K_FECHA], OtrosDatos[K_OBSERVA], ErrorCount ) )
                         else
                             ErrorCount:= GrabaKardexPlaza( FieldByName('CB_CODIGO').AsInteger );
                    end
                    else
                        Inc(ErrorCount);

               end
               else
               begin
                    Reconcile ( ServerRecursos.GrabaCambiosMultiples( dmCliente.Empresa, Delta, FOtrosDatos, ErrorCount ) );
               end;

               if ErrorCount = 0 then
                  RefrescaHisKardex:= TRUE;

          end;
     end;
end;

{ cdsMisAsignados }

procedure TdmSuper.cdsMisAsignadosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsMisAsignados.Data := ServerSuper.GetMisAsignaciones( Empresa, FechaSupervisor );
     // Incluye todas mis Asignaciones a otros Supervisores
     // Mis Empleados los Obtengo de cdsEmpleados
end;

{ cdsSupervisores }

procedure TdmSuper.cdsSupervisoresAlAdquirirDatos(Sender: TObject);
begin
     cdsSupervisores.Data := ServerSuper.GetListaSupervisores( dmCliente.Empresa );
end;

{ cdsAsignaciones }

procedure TdmSuper.cdsAsignacionesAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsAsignaciones do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconciliar( ServerSuper.GrabaAsignaciones( dmCLiente.Empresa, Delta, ErrorCount ) );
     end;
     RefrescaMisEmpleados;
     cdsMisAsignados.Active := FALSE;    //Solo si se Escribieron Cambios se Volver� a obtener lista de MisAsignados
end;

{ ******* METODOS PRIVADOS DE CLIENTDATASET ************ }

function TdmSuper.GetRangoLista: String;
const
     K_EMPLEADO = 'COLABORA.CB_CODIGO';
begin
     Result := VACIO;
     with FRangoLista do
     begin
          case TipoRango of
               raRango: Result := GetFiltroRango( K_EMPLEADO, Inicial, Final );
               raLista: Result := GetFiltroLista( K_EMPLEADO, Lista );
          else
              Result := '';
          end;
     end;
end;

function TdmSuper.GetCondicion: String;
begin
     with dmCatalogos.cdsCondiciones do
     begin
          if ( Active ) and ( Locate( 'QU_CODIGO', FCondicion, [] ) ) then
             Result := FieldByName( 'QU_FILTRO' ).AsString
          else
             Result := VACIO;
     end;
end;

function TdmSuper.GetParametroscdsEmpleados: TZetaParams;
begin
     Result := TZetaParams.Create( self );
     with Result do
     begin
          AddDate( 'Fecha', dmCliente.FechaSupervisor );
          AddString( 'RangoLista', GetRangoLista );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', Filtro );
          AddInteger( 'FiltroFijo', FFiltroFijo );
          AddBoolean( 'LaborActivado', FLaborActivado );
     end;
end;

function TdmSuper.FetchEmployee(const iEmpleado: TNumEmp): Boolean;
var
   Datos: OleVariant;
begin
     with dmCliente do
          Result := ServerSuper.GetDatosEmpleado( Empresa, iEmpleado, FechaSupervisor, FObtenerFoto, Datos );
     if Result then
     begin
          with cdsDatosEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmSuper.FetchPreviousEmployee(const iEmpleado: TNumEmp): Boolean;
var
   Datos: OleVariant;
begin
     with dmCliente do
          Result := ServerSuper.GetDatosEmpleadoAnterior( Empresa, iEmpleado, FechaSupervisor, FObtenerFoto, Datos );
     if Result then
     begin
          with cdsDatosEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmSuper.FetchNextEmployee(const iEmpleado: TNumEmp): Boolean;
var
   Datos: OleVariant;
begin
     with dmCliente do
          Result := ServerSuper.GetDatosEmpleadoSiguiente( Empresa, iEmpleado, FechaSupervisor, FObtenerFoto, Datos );
     if Result then
     begin
          with cdsDatosEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmSuper.FEmpleado: Integer;
begin
     with cdsDatosEmpleado do
     begin
          if Active then
             Result := FieldByName( 'CB_CODIGO' ).AsInteger
          else
             Result := 0;
     end;
end;

procedure TdmSuper.RefrescaKardex( const sTipo: String );
var
   sGral : String;
   dFecha : TDate;
begin
     if RefrescaHisKardex then
     begin
          with cdsHisKardex do
               if Active then
               begin
                    // Obtener Valores para sGral y/o dFecha
                    if sTipo = K_T_MULTIPLE then
                       dFecha := FOtrosDatos[0]       // Cuando se usa FKardexVarios, aqu� se guarda la fecha del movimiento
                    else
                    begin  // Esta Posicionado sobre el Registro Agregado
                         sGral  := FieldByName( 'CB_TIPO' ).AsString;
                         dFecha := FieldByName( 'CB_FECHA' ).AsDateTime;
                    end;
                    // Refrescar cds y Posicionar
                    Refrescar;
                    if sTipo = K_T_MULTIPLE then
                       Locate('CB_FECHA', dFecha, [] )
                    else
                       Locate('CB_FECHA;CB_TIPO', VarArrayOf( [ dFecha, sGral ] ), [] );
               end;
          RefrescaHisKardex:= FALSE;
     end;
end;

function TdmSuper.GetChecadasStr: String;
begin
     Result := VACIO;
     with cdsChecadas do
          if Active and ( not IsEmpty ) then
          begin
               First;
               while not EOF do
               begin
                    if ( FieldByName( 'CH_H_REAL' ).AsString <> K_HORA_NULA ) and
                       ( not zStrToBool( FieldByName( 'CH_SISTEMA' ).AsString ) ) then
{$ifdef ANTES}
                       Result := ConcatString( Result, FieldByName( 'CH_H_REAL' ).AsString, ',' ) +
                              ConcatString( Result, FieldByName( 'CH_MOTIVO' ).AsString, '-' ); {ACL170409}
{$else}
                       if FToolsAsistencia.PermitirCapturaMotivoCH then
                       begin
                            Result := ConcatString( Result, ConcatString( FieldByName( 'CH_H_REAL' ).AsString,
                                                                          FieldByName( 'CH_MOTIVO' ).AsString,
                                                                          '-' ),
                                                            ',' );
                       end
                       else
                           Result := ConcatString( Result, FieldByName( 'CH_H_REAL' ).AsString, ',' );
{$endif}
                    Next;
               end;
               First;
          end;
end;

function TdmSuper.GetChecadasVar: OleVariant;
var
   i : Integer;
begin
     Result:= Null;
     i := 0;
     with cdsChecadas do
          if Active and ( not IsEmpty ) then
          begin
               Result:= VarArrayCreate( [ 0, RecordCount - 1 ], varVariant );
               First;
               while not EOF do
               begin
                    if ( FieldByName( 'CH_H_REAL' ).AsString <> K_HORA_NULA ) and
                       ( not zStrToBool( FieldByName( 'CH_SISTEMA' ).AsString ) ) then
                    Result[i] := VarArrayOf( [ FieldByName( 'CH_H_REAL' ).AsString,
                                               FieldByName( 'CH_GLOBAL' ).AsString,
                                               FieldByName( 'CH_RELOJ' ).AsString,
                                               FieldByName( 'US_CODIGO' ).AsInteger,
                                               FieldByName( 'CH_MOTIVO' ).AsString ] );
                    Next;
                    Inc( i );
               end;
          end;
end;

function TdmSuper.GetDatosClasificacion(const iEmpleado: TNumEmp; const dValor: TDate): TDatosClasificacion;
begin
     Result := ZetaCommonTools.DatosClasificacionFromVariant( ServerRecursos.GetDatosClasificacion( dmCliente.Empresa, iEmpleado, dValor ) );
end;

function TdmSuper.GetStatusHorario(const sTurno: String; const dReferencia: TDate): TStatusHorario;
var
   Valor: Variant;
begin
     Valor := ServerAsistencia.GetHorarioStatus( dmCliente.Empresa, VarArrayOf( [ sTurno, dReferencia ] ) );
     with Result do
     begin
          Horario := Valor[ 0 ];
          Status := eStatusAusencia( Valor[ 1 ] );
     end;
end;

function TdmSuper.ExisteKardex( const dFecha: TDate; const sTipo: String ): Boolean;
begin
     with cdsHisKardex do
     begin
          //if not Active then
             Refrescar;
          Result := Locate('CB_FECHA;CB_TIPO', VarArrayOf( [ dFecha, sTipo ] ), [] );
     end;
end;

function TdmSuper.GetStatus(iEmpleado: integer; dFecha: TDate): eStatusEmpleado;
begin
     Result := eStatusEmpleado(ServerRecursos.GetStatus( dmCliente.Empresa, iEmpleado, dFecha ) );
end;

function TdmSuper.GetHorarioCodigo( const sNameDataSet: String ): String;
begin
     if ( sNameDataSet = 'cdsChecadas' ) then
        Result := cdsTarjeta.FieldByName( 'HO_CODIGO' ).AsString
     else
     if ( sNameDataSet = 'cdsGridAsistencia' ) then
        Result := cdsGridAsistencia.FieldByName( 'HO_CODIGO' ).AsString
     else if ( sNameDataSet = 'cdsGridTarjetasPeriodo' ) then
          Result := cdsGridTarjetasPeriodo.FieldByName( 'HO_CODIGO' ).AsString;
end;

function TdmSuper.GetMinPenalizacion(const sHorario: String; const iPenal: Integer): Integer;
begin
     with dmCatalogos.cdsHorarios do
     begin
          if ( FieldByName( 'HO_CODIGO' ).AsString <> sHorario ) then
             Locate( 'HO_CODIGO', sHorario, [] );
          Result := aMinutos( FieldByName( 'HO_INTIME' ).AsString ) +
                    FieldByName( 'HO_IN_TARD' ).AsInteger + iPenal;
     end;
end;

function TdmSuper.IncidenciaValida(const sAccion: String; var sMensaje: String): Boolean;
begin
     with cdsHisPermisos.FieldByName( 'PM_CLASIFI' ) do
     begin
          Result := DerechoTipoPermiso( AsInteger );
          if not Result then
             sMensaje := Format( 'NO se Puede %s Permisos de Clase = ', [ sAccion ] ) +
                         ObtieneElemento( lfTipoPermiso, AsInteger );
     end;
end;

procedure TdmSuper.EditarGridAProbarAutorizaciones;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAutorizacionesPorAprobar, TGridAutorizacionesPorAprobar, FALSE );
end;

procedure TdmSuper.cdsGridAutoXAprobarAlAdquirirDatos(Sender: TObject);
begin
     cdsGridAutoXAprobar.Data := ServerAsistencia.GetAutoXAprobar( dmCliente.Empresa, FFechaGridAuto, FALSE );
end;

procedure TdmSuper.cdsGridAutoXAprobarAlCrearCampos(Sender: TObject);
begin
     with cdsGridAutoXAprobar do
     begin
          MaskHoras( 'CH_HORAS' );     
          MaskHoras( 'CH_HOR_DES' );
          with FieldByName( 'CH_TIPO' ) do
          begin
               OnGetText := CH_TIPOGetText;
               Alignment := taLeftJustify;
          end;
          CreateSimpleLookup( cdsSupervisores, 'TB_ELEMENT', 'CB_NIVEL' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DES_OK', 'US_COD_OK' );
     end;
end;

procedure TdmSuper.CH_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
    Text := GetDescTipo( Sender.AsInteger );
end;

function  TdmSuper.GetDescTipo( const nTipo : Integer ) : String;
const
     aAutorizaCorta: array[ eAutorizaChecadas ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}=( 'Per. C/G','Per. S/G','Hrs. Extras', 'Desc. Trab.','Per. C/G Ent','Per. S/G Ent','Pre. F/Jornada','Pre. D/Jornada');
begin
     if ( nTipo >= K_OFFSET_AUTORIZACION ) then
        Result := aAutorizaCorta[ eAutorizaChecadas( nTipo - K_OFFSET_AUTORIZACION ) ]
     else
        Result := VACIO;
end;


procedure TdmSuper.cdsGridAutoXAprobarAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsGridAutoXAprobar do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerAsistencia.GrabaAutoXAprobar( dmCliente.Empresa, Delta, ErrorCount ) );
          end;
     end;
end;

procedure TdmSuper.cdsGridAutoXAprobarBeforePost(DataSet: TDataSet);
begin
     with cdsGridAutoXAprobar do
     begin
          if dmCliente.PuedeCambiarTarjetaDlg( cdsGridAutoXAprobar ) then
          begin
               if ( FieldByName( 'CH_HOR_DES' ).AsFloat <> 0 ) then
                  FieldByName( 'US_COD_OK' ).AsInteger := dmCliente.Usuario
               else
                   FieldByName( 'US_COD_OK' ).AsInteger := 0;
          end
          else
          begin
               Cancel;
               Abort;
          end;
     end;
end;

function TdmSuper.AprobarAut_Derecho( oDataSet : TDataSet;
                                      const sAprobacion: string ): Boolean;
begin
     if Global.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES ) then
     begin
          Result := ZAccesosMgr.CheckDerecho( D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL, APROBAR_AUTORIZACION );
          if NOT Result then
          begin
               with oDataset do
                    Result := (FieldByName( sAprobacion ).AsInteger = 0);
          end;
     end
     else
         Result := TRUE;
end;

function TdmSuper.Aprobar_Autorizaciones( oDataSet : TDataSet;
                                          const sAprobacion: string ): Boolean;
begin
     if Global.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES ) then
     begin
          with oDataset do
               Result := (FieldByName( sAprobacion ).AsInteger = 0);
     end
     else
         Result := TRUE;
end;

{ cdsHisVacacion }

procedure TdmSuper.cdsHisVacacionAlAdquirirDatos(Sender: TObject);
var
   DatosEmpleado : OleVariant;
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with dmCliente do
             begin
                  cdsHisVacacion.Data := ServerRecursos.GetHisVacacion( Empresa, EmpleadoNumero,
                                                                        FechaDefault, DatosEmpleado );
             end;
             cdsEmpVacacion.Data := DatosEmpleado;
          finally
                 Cursor := oCursor;
          end;
     end;
     cdsSaldoVacaciones.Refrescar;  
end;

procedure TdmSuper.cdsHisVacacionAlCrearCampos(Sender: TObject);
begin
     with cdsHisVacacion do
     begin
          ListaFija( 'VA_TIPO', lfTipoVacacion );
          MaskHoras( 'VA_D_PAGO' );
          MaskHoras( 'VA_PAGO' );
          MaskHoras( 'VA_S_PAGO' );
          MaskHoras( 'VA_D_GOZO' );
          MaskHoras( 'VA_GOZO' );
          MaskHoras( 'VA_S_GOZO' );
          MaskHoras( 'VA_D_PRIMA' );
          MaskHoras( 'VA_P_PRIMA' );
          MaskHoras( 'VA_S_PRIMA' );
          {***DevEx (by am): Se agrego la mascara para fechas que manejamos pues estos campos no la tenian. Sin embargo,
              aparecian en el formato indicado pues el componente del ZetaDBGrid muestra las fechas con ese formato. El CXGrid
              que utilizamos no lo hace asi por lo tanto debemos de poner la mascara a los campos tipo fecha***}
          MaskFecha( 'VA_FEC_INI' );
          {***}
          //FieldByName('VA_FEC_INI').OnValidate := OnVA_FEC_INIValidate;
          FieldByName('VA_FEC_INI').OnChange := OnVA_FEC_INIChange;
          FieldByName('VA_FEC_FIN').OnChange := OnVA_FEC_FINChange;
          FieldByName('VA_TIPO').OnChange := OnVA_TIPOChange;
          FieldByName('VA_GOZO').OnChange := OnVA_GOZOChange;
          FieldByName('VA_OTROS').OnChange := OnVA_OTROSChange;
          FieldByName('VA_P_PRIMA').OnChange  := OnVA_OTROSChange;
          FieldByName('VA_PAGO').OnChange  := OnVA_PAGOChange;
          FieldByName('VA_NOMTIPO').OnChange  := validacionTipoNominaChange;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

{
procedure TdmSuper.OnVA_FEC_INIValidate(Sender: TField);
const
     K_INICIO_INVALIDO = 'Fecha de Inicio Inv�lida !' + CR_LF + 'Debe Ser Mayor a %s';
var
   dFecha : TDate;
begin
     dFecha := Sender.AsDateTime;
     if ( not ValidaInicioVacacion( dFecha ) ) then
     begin
          ZetaDialogo.ZWarning( 'Advertencia', Format( K_INICIO_INVALIDO, [ FechaCorta( Date ) ] ), 0, mbOk );
          TField( Sender ).FocusControl;
          Abort;
     end;
end;
}

procedure TdmSuper.OnVA_FEC_INIChange(Sender: TField);
begin
    with cdsHisVacacion do
    begin
         if ( State = dsInsert ) then    // Esta Insertando y se solicitaran los saldos
         begin
              GetSaldosVacacion;
              // Solo funciona para Vacaciones - no se pueden registrar cierres
              FieldByName( 'CB_TABLASS' ).AsString := FSaldosVaca[ K_VACA_TABLA_PREST ];
              FieldByName( 'CB_SALARIO' ).AsFloat := FSaldosVaca[ K_VACA_SAL_DIARIO ];
              FieldByName( 'VA_TASA_PR' ).AsFloat := FSaldosVaca[ K_VACA_PRIMA_VACA ];
              RecalculaVacaDiasGozados;
         end;
    end;
end;

procedure TdmSuper.OnVA_FEC_FINChange(Sender: TField);
begin
     if ( Sender.DataSet.State = dsInsert ) and FVacaCalculaDiasGozados then
        RecalculaVacaDiasGozados;
end;

procedure TdmSuper.OnVA_TIPOChange(Sender: TField);
var
   dFecha: TDate;
begin
     dFecha := dmCliente.FechaDefault;
     if ( not ValidaInicioVacacion( dFecha ) ) then
        dFecha := ( Date + 1 );    // Si solo puede agregar vacaciones futuras El Default Ser� la Fecha de la Computadora + 1
     with cdsHisVacacion do
     begin
          FieldByName( 'VA_FEC_INI' ).AsDateTime:= dFecha;
          FieldByName( 'VA_FEC_FIN' ).AsDateTime:= dFecha;     // Comienzan siendo iguales
     end;
end;

procedure TdmSuper.OnVA_GOZOChange(Sender: TField);
begin
     dmCatalogos.cdsPeriodo.Conectar;
     with cdsHisVacacion do
     begin
          if ZAccesosMgr.CheckDerecho( D_SUPER_VACACIONES, K_DERECHO_SIST_KARDEX ) and ( dmCatalogos.cdsPeriodo.FieldByName('PE_STATUS').AsInteger < Ord(spAfectadaParcial))    then    // Bit #5 de IndexDerechos 'Registrar Vacaciones Pagadas' ( D_SUPER_VACACIONES )
             FieldByName( 'VA_PAGO' ).AsFloat := FieldByName( 'VA_GOZO' ).AsFloat;
     end;
end;

procedure TdmSuper.OnVA_OTROSChange(Sender: TField);
begin
     RecalculaMontoVaca;
end;

procedure TdmSuper.OnVA_PAGOChange(Sender: TField);
begin
     if VarIsNull( FSaldosVaca ) then
           GetSaldosVacacion;
     if Global.GetGlobalBooleano ( K_GLOBAL_PAGO_PRIMA_VACA_ANIV ) then
        RecalculaMontoVaca
     else
        FToolsRH.SetDiasPrima( FSaldosVaca, cdsHisVacacion );
end;

procedure TdmSuper.cdsHisVacacionNewRecord(DataSet: TDataSet);
var
   DbLookup: TZetaKeyLOOKUP_DevEx;
begin
     with cdsHisVacacion do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger := EmpleadoNumero;
          FieldByName( 'VA_TIPO' ).AsInteger    := Ord( tvVacaciones );    // Solo se registran Vacaciones en Supervisores
          //FieldByName( 'VA_FEC_INI' ).AsDateTime:= FechaDefault;      ... Se Agrega en onChange de VA_TIPO
          with dmCliente do
          begin
               FieldByName( 'VA_NOMYEAR' ).AsInteger  := YearDefault;
               FieldByName( 'VA_NOMTIPO' ).AsInteger  := Ord( PeriodoTipo );
               DbLookup:= TZetaKeyLOOKUP_DevEx.Create(Self);
               try
                  dmCatalogos.AplicaFiltroStatusPeriodo(DbLookup,True);
               finally
                      FreeAndNil(DbLookup);
               end;
               FieldByName( 'VA_NOMNUME' ).AsInteger  := 0;
          end;
          FieldByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmSuper.cdsHisVacacionAlModificar(Sender: TObject);
var
   sMensaje: String;
begin
     if ValidateTipoVacacion( ukModify, sMensaje ) then
     begin
          FCambioVacaciones := FALSE;
          with cdsHisVacacion do
          begin
               { Obliga a solicitar FSaldosVaca cuando se modifique VA_PAGO }
               if ( State <> dsInsert ) then
                  FSaldosVaca := NULL;
               ZBaseEdicion_DevEx.ShowFormaEdicion( EditVacacion_DevEx, TEditVacacion_DevEx );
          end;
          if FCambioVacaciones then
             cdsHisVacacion.Refrescar;
     end
     else
         ZetaDialogo.zInformation( '', sMensaje, 0 );
end;

procedure TdmSuper.cdsHisVacacionBeforeEdit(DataSet: TDataSet);
begin
     if ( not FInsertandoVaca ) then
     begin
          with cdsHisVacacion do
          begin
               FAntDiasGozados := FieldByName( 'VA_GOZO' ).AsFloat;
               FAntDiasPagados := FieldByName( 'VA_PAGO' ).AsFloat;
               FAntDiasPrima   := FieldByName( 'VA_P_PRIMA' ).AsFloat;
          end;
     end;
end;

procedure TdmSuper.cdsHisVacacionBeforeInsert(DataSet: TDataSet);
begin
     FInsertandoVaca := TRUE;
     FAntDiasGozados := 0;
     FAntDiasPagados := 0;
     FAntDiasPrima := 0;
end;

procedure TdmSuper.cdsHisVacacionBeforePost(DataSet: TDataSet);
begin
     ValidaFechaVacacion( DataSet );
     ValidaExcederSaldos( DataSet );
     with DataSet do
     begin
          //US 17583: 3. Usabilidad - Supervisores permite captura de vacaciones con 0 d�as o fecha de regreso menor a fecha de inicio
          if( FieldByName( 'VA_TIPO' ).AsInteger = Ord( tvVacaciones ) )then
          begin
               if( FieldByName('VA_FEC_INI').AsDateTime > FieldByName('VA_FEC_FIN').AsDateTime )then
                   DataBaseError( 'La fecha de inicio debe de ser menor � igual a la de fecha de regreso' );
          end;
          //FIN
          FieldByName( 'VA_CAPTURA' ).AsDateTime:= Date;
          FieldByName( 'US_CODIGO' ).AsInteger  := dmCliente.Usuario;
     end;
end;

procedure TdmSuper.cdsHisVacacionAlBorrar(Sender: TObject);
var
   sMensaje : String;
   iYear:Integer;
   TipoPer :eTipoPeriodo;
   bRestringir:Boolean;

   function ValidatePagoVacacion: Boolean;
   begin
        with cdsHisVacacion do
        begin
             Result := ZAccesosMgr.CheckDerecho( D_SUPER_VACACIONES, K_DERECHO_SIST_KARDEX ) or
                       ( ( FieldByName( 'VA_PAGO' ).AsFloat = 0 ) and
                         ( FieldByName( 'VA_OTROS' ).AsFloat = 0 ) and
                         ( FieldByName( 'VA_P_PRIMA' ).AsFloat = 0 ) );
        end;
        if ( not Result ) then
           sMensaje := 'No Tiene Permiso Para Borrar Vacaciones Pagadas';
   end;

begin
     bRestringir := False;
     if ValidateTipoVacacion( ukDelete, sMensaje ) and
        ValidateInicioVacacion( cdsHisVacacion.FieldByName( 'VA_FEC_INI' ).AsDateTime,
                                ukDelete, sMensaje ) and
        ValidatePagoVacacion then
     begin
          iYear := cdsHisVacacion.FieldByName( 'VA_NOMYEAR' ).AsInteger;
          TipoPer := eTipoPeriodo( cdsHisVacacion.FieldByName( 'VA_NOMTIPO' ).AsInteger );
          with dmCatalogos do
          begin
               cdsPeriodo.Filtered := False;
               GetDatosPeriodo(iYear,TipoPer);
               if cdsPeriodo.Locate('PE_NUMERO',cdsHisVacacion.FieldByName( 'VA_NOMNUME' ).AsInteger,[] ) then
                  bRestringir := cdsPeriodo.FieldByName('PE_STATUS').AsInteger > Ord(spAfectadaParcial);
          end;
           if ( ( not bRestringir ) or ( cdsHisVacacion.FieldByName( 'VA_NOMNUME' ).AsInteger = 0) )then
          begin
               if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
               begin
                    cdsHisVacacion.Delete;
               end;
          end
          else
               ZWarning ( 'Al borrar Vacaciones','No tiene derecho a Borrar Vacaciones Pagadas',0,mbOK);
     end
     else
         ZetaDialogo.zInformation( '', sMensaje, 0 );
end;

procedure TdmSuper.cdsHisVacacionAfterDelete(DataSet: TDataSet);
begin
     FCambioVacaciones := FALSE;
     with cdsHisVacacion do
     begin
          Enviar;
          if FCambioVacaciones then
             Refrescar;
     end;
end;

procedure TdmSuper.cdsHisVacacionAfterCancel(DataSet: TDataSet);
begin
     FInsertandoVaca := FALSE;
end;

procedure TdmSuper.cdsHisVacacionAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   lValidarInicio: Boolean;
begin
     ErrorCount := 0;
     with cdsHisVacacion do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               lValidarInicio := not ( ZAccesosMgr.CheckDerecho( D_SUPER_VACACIONES, K_DERECHO_BANCA ) );    // Bit #6 de IndexDerechos 'Registrar Vacaciones Pasadas' ( D_SUPER_VACACIONES )
               Reconcile ( ServerRecursos.GrabaHisVacacion( dmCliente.Empresa, Delta, lValidarInicio, ErrorCount ) );
               if ( ErrorCount = 0 ) then
               begin
                    TressShell.SetDataChange( [ enVacacion ] );
                    FCambioVacaciones := TRUE;
                    FInsertandoVaca := FALSE;
               end;
          end;
     end;
end;

function TdmSuper.ValidaInicioVacacion( const dFecha: TDate ): Boolean;
begin
     Result := ( ZAccesosMgr.CheckDerecho( D_SUPER_VACACIONES, K_DERECHO_BANCA ) ) or    // Bit #6 de IndexDerechos 'Registrar Vacaciones Pasadas' ( D_SUPER_VACACIONES )
               ( dFecha > Date );    // Si no se permiten vacaciones pasadas debe ser mayor a la fecha de la computadora - NOTA: No se usar� la fecha del Shell
end;

function TdmSuper.ValidateInicioVacacion( const dFecha: TDate; const UpdateKind: TUpdateKind;
         var sMensaje: String ): Boolean;
begin
     Result := ValidaInicioVacacion( dFecha );
     if ( not Result ) then
        sMensaje := Format( 'No Tiene Permiso Para %s Vacaciones Pasadas',
                            [ ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ) ] );
end;

{$ifdef ICUMEDICAL_CURSOS}
procedure TdmSuper.AbreDocumentoICU;
begin
     if( FileExists( FPathICU ) ) then
         ZetaFilesTools.AbreDocumentoICU( cdsSesion, cdsSesion.FieldByName('SE_D_RUTA').AsString )
     else
         ZetaDialogo.ZError('Problema al abrir el documento', 'No se pudo abrir el archivo de la ruta almacenada. Favor de asegurarse que exista un archivo con ese nombre.', 0);
end;

procedure TdmSuper.BorraDocumentoICU;
begin
     FPathICU := VACIO;
     {with cdsSesiones do
     begin
          if State = dsBrowse then
             Edit;
          FieldByName('SE_D_NOM').AsString := VACIO;
          FieldByName('SE_D_EXT').AsString := VACIO;
          FieldByName('SE_D_RUTA').AsString := VACIO;
     end;}
end;

function TdmSuper.CargaDocumentoICU( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): boolean;
begin
     Result := StrLleno( sPath ) ;
     if Result then
     begin
          FPathICU := sPath;
     end
     else
     begin
          FPathICU := VACIO;
          ZetaDialogo.ZError( 'Error en el Documento...', 'El nombre no puede quedar vac�o', 0 );
     end;
end;
{$endif}

function TdmSuper.ValidateTipoVacacion( const UpdateKind: TUpdateKind; var sMensaje: String ): Boolean;
begin
     Result := ( eTipoVacacion( cdsHisVacacion.FieldByName( 'VA_TIPO' ).AsInteger ) = tvVacaciones );
     if ( not Result ) then
        sMensaje := Format( 'No Se Pueden %s Cierres De Vacaciones',
                            [ ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ) ] );
end;

procedure TdmSuper.RecalculaVacaDiasGozados;
begin
     FToolsRH.RecalculaVacaDiasGozados( cdsHisVacacion,
                                        eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ),
                                        self.GetVacaDiasGozar );
end;

function TdmSuper.GetVacaDiasGozar( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := ServerAsistencia.GetVacaDiasGozar( dmCliente.Empresa, iEmpleado, dInicio, dFinal );
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TdmSuper.RecalculaVacaFechaRegreso;
begin
     FVacaCalculaDiasGozados := FALSE;
     try
        FToolsRH.RecalculaVacaFechaRegreso( cdsHisVacacion,
                                            eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ),
                                            self.GetVacaFechaRegreso );
     finally
            FVacaCalculaDiasGozados := TRUE;
     end;
end;

function TdmSuper.GetVacaFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const rGozo: TPesos; var rDiasRango: Double ): TDate;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := ServerAsistencia.GetVacaFechaRegreso( dmCliente.Empresa, iEmpleado, dInicio, rGozo, rDiasRango );
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TdmSuper.RecalculaMontoVaca;
begin
     if VarIsNull( FSaldosVaca ) then
        GetSaldosVacacion;
     SetMontosVaca( FSaldosVaca, cdsHisVacacion );
end;

procedure TdmSuper.GetSaldosVacacion;
begin
     FSaldosVaca := ServerRecursos.GetSaldosVacacion( dmCliente.Empresa, EmpleadoNumero,
                                                      cdsHisVacacion.FieldByName( 'VA_FEC_INI' ).AsDateTime );
end;

procedure TdmSuper.SetSaldosVacaciones( const lInfoAniversario: Boolean; var rGozo, rPago, rPrima: TPesos );
begin
     FToolsRH.SetDiasSaldosVacaciones( FSaldosVaca, lInfoAniversario, rGozo, rPago, rPrima );
end;

procedure TdmSuper.ValidaFechaVacacion( DataSet: TDataSet );
const
     K_INICIO_INVALIDO = 'Fecha de Inicio Inv�lida !' + CR_LF + 'Debe Ser Mayor a %s';
var
   dFecha : TDate;
begin
     dFecha := DataSet.FieldByName( 'VA_FEC_INI' ).AsDateTime;
     if ( not ValidaInicioVacacion( dFecha ) ) then
     begin
          ZetaDialogo.ZWarning( '� Advertencia !', Format( K_INICIO_INVALIDO, [ FechaCorta( Date ) ] ), 0, mbOk );
          Abort;
     end;
end;

procedure TdmSuper.ValidaExcederSaldos( DataSet: TDataSet );
var
   FSaldosAnivAnt : OleVariant;
   FParameterList: TZetaParams;
   lCheckGozados, lCheckPagados, lCheckPrima : Boolean;
   rDiasGozados, rDiasPagados, rDiasPrima: TPesos;
   rSaldo : TPesos;
   sError : String;

   function RequiereValidarSaldos: Boolean;     // Se Verifica si es necesario checar con el Servidor
   var
      lExcederGozados, lExcederPagados, lExcederPrima: Boolean;
   begin
        with DataSet do
        begin
             rDiasGozados := FieldByName( 'VA_GOZO' ).AsFloat;
             rDiasPagados := FieldByName( 'VA_PAGO' ).AsFloat;
             rDiasPrima   := FieldByName( 'VA_P_PRIMA' ).AsFloat;
        end;
        lExcederGozados := ZAccesosMgr.CheckDerecho( D_SUPER_VACACIONES, K_DERECHO_NIVEL0 );   // Bit #7 de IndexDerechos 'Exceder Saldos de Vacaciones Gozadas' ( D_SUPER_VACACIONES )
        lExcederPagados := ZAccesosMgr.CheckDerecho( D_SUPER_VACACIONES, K_DERECHO_CONFIGURA );   // Bit #8 de IndexDerechos 'Exceder Saldos de Vacaciones Pagadas' ( D_SUPER_VACACIONES )
        lExcederPrima   := ZAccesosMgr.CheckDerecho( D_SUPER_VACACIONES, K_DERECHO_ADICIONAL9 );   // Bit #9 de IndexDerechos 'Exceder Saldos de Vacaciones Pagadas' ( D_SUPER_VACACIONES )

        lCheckGozados := ( ( not lExcederGozados ) and ( rDiasGozados > FAntDiasGozados ) );
        lCheckPagados := ( ( not lExcederPagados ) and ( rDiasPagados > FAntDiasPagados ) );
        lCheckPrima   := ( ( not lExcederPrima )   and ( rDiasPrima   > FAntDiasPrima   ) );

        Result := ( lCheckGozados or lCheckPagados or lCheckPrima );
   end;

begin
     if RequiereValidarSaldos then
     begin
          sError := VACIO;
          FParameterList := TZetaParams.Create;
          try
             with dmCliente do
             begin
                  CargaActivosTodos( FParameterList );
                  with FParameterList do
                  begin
                       AddInteger( 'Empleado', EmpleadoNumero );
                       AddDate( 'Fecha', DataSet.FieldByName( 'VA_FEC_INI' ).AsDateTime );
                  end;
                  FSaldosAnivAnt := ServerRecursos.GetSaldosVacaAniv( Empresa, FParameterList.VarValues );
             end;
          finally
                 FreeAndNil( FParameterList )
          end;
          if lCheckGozados then     // Checar Gozados
          begin
               rSaldo := ( ( FSaldosAnivAnt[ K_VACA_DERECHO_GOZO ] + FAntDiasGozados )- FSaldosAnivAnt[ K_VACA_GOZADOS ] );
               if ( rDiasGozados > rSaldo ) then
                  sError := Format( 'Se Excede Saldo (%f) de Vacaciones a Gozar', [ rSaldo ] );
          end;
          if lCheckPagados then     // Checar Pagados
          begin
               rSaldo := ( ( FSaldosAnivAnt[ K_VACA_DERECHO_PAGO ] + FAntDiasPagados ) - FSaldosAnivAnt[ K_VACA_PAGADOS ] );
               if ( rDiasPagados > rSaldo ) then
                  sError := ConcatString( sError, Format( 'Se Excede Saldo (%f) de Vacaciones a Pagar', [ rSaldo ] ), CR_LF );
          end;
          if lCheckPrima then     // Checar Prima
          begin
               rSaldo := ( ( FSaldosAnivAnt[ K_VACA_D_PRIMA ] + FAntDiasPrima ) - FSaldosAnivAnt[ K_VACA_P_PRIMA ] );
               if ( rDiasPrima > rSaldo ) then
                  sError := ConcatString( sError, Format( 'Se Excede Saldo (%f) de Prima Vacacional', [ rSaldo ] ), CR_LF );
          end;
          if StrLleno( sError ) then
          begin
               sError := ConcatString( sError, Format( 'Aniversario Anterior: %s', [ FechaCorta( FSaldosAnivAnt [ K_VACA_ANIV_ANTERIOR ] ) ] ), CR_LF + CR_LF );
               ZetaDialogo.ZWarning( '� Advertencia !', sError, 0, mbOk );
               Abort;    // Excepci�n silenciosa para que no haga el post del Dataset
          end;
     end;
end;

{ cdsEmpVacacion }

procedure TdmSuper.cdsEmpVacacionAlCrearCampos(Sender: TObject);
begin
     with cdsEmpVacacion do
     begin
         // Dias Pagados
         MaskPesos( 'CB_DER_PAG' );
         MaskPesos( 'CB_V_PAGO' );
         MaskPesos( 'CB_SUB_PAG' );
         MaskPesos( 'CB_PRO_PAG' );
         MaskPesos( 'CB_TOT_PAG' );
         // Dias Gozados
         MaskPesos( 'CB_DER_GOZ' );
         MaskPesos( 'CB_V_GOZO' );
         MaskPesos( 'CB_SUB_GOZ' );
         MaskPesos( 'CB_PRO_GOZ' );
         MaskPesos( 'CB_TOT_GOZ' );
         // Dias Prima
         MaskPesos( 'CB_DER_PV' );
         MaskPesos( 'CB_V_PRIMA' );
         MaskPesos( 'CB_SUB_PV' );
         MaskPesos( 'CB_PRO_PV' );
         MaskPesos( 'CB_TOT_PV' );
         // Fechas
         MaskFecha( 'CB_FEC_VAC' );
         MaskFecha( 'CB_DER_FEC' );
         MaskFecha( 'CB_FEC_ANT' );
     end;
end;

procedure TdmSuper.CambioMasivoTurnoGetLista(Parametros: TZetaParams);
begin
     cdsDataset.Data := ServerRecursos.CambioMasivoTurnoGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmSuper.CambioMasivoTurno(Parametros: TZetaParams; const lVerificacion: Boolean; SetAnimacion : TProcAnimacion ): Boolean;
var
   Resultado : OleVariant;
begin
     SetAnimacion( TRUE );
     try
        if lVerificacion then
        begin
             cdsDataSet.MergeChangeLog;
             Resultado := ServerRecursos.CambioMasivoTurnoLista( dmCliente.Empresa, cdsDataSet.Data, Parametros.VarValues );
        end
        else
             Resultado := ServerRecursos.CambioMasivoTurno( dmCliente.Empresa, Parametros.VarValues );
        Result := Check( Resultado );
     finally
        SetAnimacion( FALSE );
     end;
end;

{ cdsClasifiTemp }

procedure TdmSuper.cdsClasifiTempAlAdquirirDatos(Sender: TObject);
begin
     FRefrescarFormaActiva := FALSE;
     try
        with dmCliente do
        begin
             cdsClasifiTemp.Data := ServerAsistencia.GetClasificacionTemp( Empresa, FechaSupervisor,
                                    GetListaEmpleados( 'B.CB_CODIGO' ), FALSE );
        end;
     finally
        FRefrescarFormaActiva := TRUE;
     end;
end;

procedure TdmSuper.cdsClasifiTempAlCrearCampos(Sender: TObject);
begin
     with cdsClasifiTemp do
     begin
          FieldByName( 'CB_CODIGO' ).OnChange := cdsClasifiTempCB_CODIGOChange;
          MaskFecha( 'AU_FECHA' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DES_OK', 'US_COD_OK' );
     end;
end;

procedure TdmSuper.cdsClasifiTempNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName( 'AU_FECHA').AsDateTime := dmCliente.FechaSupervisor;
          FieldByName( 'CB_CODIGO' ).AsInteger := EmpleadoNumero;
     end;
end;

procedure TdmSuper.cdsClasifiTempAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditClasifTemporal_DevEx, TEditClasifTemporal_DevEx );
end;

procedure TdmSuper.cdsClasifiTempAfterDelete(DataSet: TDataSet);
begin
     TZetaClientDataSet( DataSet ).Enviar;
end;

procedure TdmSuper.cdsClasifiTempAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsClasifiTemp do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerAsistencia.GrabaClasificacionTemp( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enClasifiTemp ] );
          end;
     end;
end;

procedure TdmSuper.cdsClasifiTempCB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          {
          with GetDatosClasificacion( FieldByName( 'CB_CODIGO' ).AsInteger, dmCliente.FechaSupervisor ) do
          begin
               FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
               FieldByName( 'CB_TURNO' ).AsString := Turno;
               FieldByName( 'CB_PUESTO' ).AsString := Puesto;
               FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
               FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
               FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
               FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
               FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
               FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
               FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
               FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
               FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
          end;
          }
          FieldByName( 'PrettyName' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescripcion( FieldByName( 'CB_CODIGO' ).AsString );
     end;
end;

procedure TdmSuper.cdsClasifiTempBeforePost(DataSet: TDataSet);
const
     K_USUARIO_OK = -1;
begin
     with cdsClasifiTemp do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'US_COD_OK' ).AsInteger := K_USUARIO_OK;
     end;
end;

procedure TdmSuper.MuestraCalendarioEmpleado;
begin
     with dmCliente do
     begin
          FCalendarioEmpleado_DevEx.ShowCalendarioEmpleado( Empleado, FechaAsistencia, cdsCalendarioEmpleado );
     end;
end;

{OP: 11/06/08}
procedure TdmSuper.MuestraCalendarioVacacion( const rDiasGozar : Double );
begin
     with dmCliente do
     begin
          FCalendarioVacacion_DevEx.ShowCalendarioVacacion( Empleado, FechaDefault, cdsCalendarioEmpleado, rDiasGozar );
     end;
end;

procedure TdmSuper.ResetCalendarioEmpleadoActivo( const dInicio, dFinal: TDate );
begin
     with dmCliente do
     begin
          cdsCalendarioEmpleado.Data := ServerAsistencia.GetCalendarioEmpleado( Empresa, Empleado, dInicio, dFinal );
     end;
end;

function TdmSuper.PuedeCambiarTarjetaStatus( const dInicial: TDate;
                                                  const iEmpleado: integer;
                                                  const eTipo: eStatusPeriodo;
                                                  const lPuedeModificar: Boolean;
                                                  var sMensaje: string): Boolean;
begin
     Result := lPuedeModificar;
     if  NOT Result then
     begin
          Result := ServerAsistencia.PuedeCambiarTarjetaStatus( dmCliente.Empresa,
                                                                dInicial,
                                                                iEmpleado,
                                                                Ord( eTipo ) );
          if ( not Result ) then
          begin
               sMensaje := Format( 'No se Puede Modificar Tarjetas de N�minas con Status Mayor a %s', [ ObtieneElemento( lfStatusPeriodo, ord(eTipo) ) ] );
          end;
     end
end;

procedure TdmSuper.EditarGridAutorizarPrenomina;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAutorizaPrenomina, TGridAutorizaPrenomina, FALSE );
end;

{$ifdef COMMSCOPE}
procedure TdmSuper.EditarGriEvaluacionDiaria;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridEvaluacionDiaria, TGridEvaluacionDiaria, FALSE );
end;

function TdmSuper.GetFechaLimiteDiaria( Empresa: OleVariant):TDateTime;
begin
    Result := ServerSuper.GetFechaEvaluacionDiaria( dmCliente.Empresa );
end;

{$endif}


procedure TdmSuper.cdsAutorizarPrenominaAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
	     Parametros.AddDate( 'Fecha', FechaSupervisor );
             cdsAutorizarPrenomina.Data := ServerSuper.GetAutorizaPrenomina( Empresa, Parametros.VarValues );
        end;
     finally
            Parametros.Free;
     end;
end;

procedure TdmSuper.cdsAutorizarPrenominaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsAutorizarPrenomina do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerSuper.GrabaAutorizaPrenomina ( dmCliente.Empresa, Delta, ErrorCount ) );
               if ( ErrorCount = 0 ) then
                  TressShell.SetDataChange( [ enNomina ] );
          end;
     end;
end;

procedure TdmSuper.cdsAutorizarPrenominaAlCrearCampos(Sender: TObject);
begin
     with cdsAutorizarPrenomina do
     begin
          MaskTime( 'NO_HOR_OK' );
          MaskHoras( 'NO_HORAS' );
          MaskHoras( 'NO_DOBLES' );
          MaskHoras( 'NO_TRIPLES' );
          MaskHoras( 'NO_HORA_SG' );
          MaskHoras( 'NO_HORA_CG' );
          MaskHoras( 'NO_DIAS_IN' );
          MaskHoras( 'NO_DIAS_VA' );
          MaskFecha( 'NO_FEC_OK' );
          MaskHoras( 'NO_DES_TRA' );

          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'NO_SUPER', 'NO_SUP_OK' );
          FieldByName( 'NO_SUP_OK' ).OnChange := cdsAutorizarPrenominaNO_SUP_OKChange;
     end;
end;

procedure TdmSuper.cdsAutorizarPrenominaBeforePost(DataSet: TDataSet);
begin
     {
     with cdsAutorizarPrenomina do
     begin
          FieldByName( 'NO_FEC_OK' ).Asdatetime := dmCliente.FechaSupervisor;
          FieldByName( 'NO_HOR_OK' ).AsString   := FormatDateTime( 'hhmm', Now );
     end;
     }
end;

procedure TdmSuper.cdsAutorizarPrenominaNO_SUP_OKChange(Sender: TField);
begin
     with cdsAutorizarPrenomina do
     begin
          FieldByName( 'NO_FEC_OK' ).Asdatetime := dmCliente.FechaSupervisor;
          FieldByName( 'NO_HOR_OK' ).AsString   := FormatDateTime( 'hhmm', Now );
          {
          FieldByName( 'PE_YEAR' ).AsInteger    := dmCliente.YearDefault;
          FieldByName( 'PE_TIPO' ).AsInteger    := Ord( dmCliente.PeriodoTipo );
          FieldByName( 'PE_NUMERO' ).AsInteger  := dmCliente.PeriodoNumero;
          }
     end;
end;

procedure TdmSuper.RefrescarPlanVacacion(Parametros : TZetaParams);
begin
     cdsPlanVacacion.Data := ServerRecursos.GetPlanVacacion( dmCliente.Empresa, Parametros.VarValues );
     cdsPlanVacacion.ResetDataChange;
end;

procedure TdmSuper.cdsPlanVacacionAfterDelete(DataSet: TDataSet);
begin
     cdsPlanVacacion.Enviar;
end;

procedure TdmSuper.cdsPlanVacacionAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsPlanVacacion do
     begin
          ListaFija('VP_STATUS', lfStatusPlanVacacion );
          MaskFecha('VP_FEC_INI');
          MaskFecha('VP_FEC_FIN');
          MaskHoras('VP_DIAS');
          FieldByName( 'VP_FEC_INI' ).OnChange := cdsPlanVacacionVP_FEC_INIChange;
          FieldByName( 'VP_FEC_FIN' ).OnChange := cdsPlanVacacionVP_FEC_FINChange;
          FieldByName( 'VP_STATUS' ).OnChange := cdsPlanVacacionVP_STATUSChange;
          FieldByName( 'CB_CODIGO' ).OnChange := cdsPlanVacacionCB_CODIGOChange;
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsPlanVacacionCB_CODIGOValidate;
          FieldByName( 'VP_PAGO_US' ).OnChange := cdsPlanVacacionVP_PAGO_USChange;
          FieldByName( 'VP_NOMTIPO' ).OnChange := validacionTipoNominaChange;
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'USR_AUT_DES', 'VP_AUT_USR' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'USR_SOL_DES', 'VP_SOL_USR' );
     end;
end;

procedure TdmSuper.cdsPlanVacacionAlAgregar(Sender: TObject);
begin
     cdsPlanVacacion.Append;
     ZbaseEdicion_DevEx.ShowFormaEdicion( RegPlanVacacion_DevEx, TRegPlanVacacion_DevEx );
end;

procedure TdmSuper.cdsPlanVacacionNewRecord(DataSet: TDataSet);
begin
     with cdsPlanVacacion do
     begin
          FieldByName( 'VP_STATUS' ).AsInteger :=  Ord(ZetaCommonlists.spvPendiente);
          FieldByName( 'VP_FEC_INI' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'VP_FEC_FIN' ).AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'VP_SOL_USR').AsInteger := dmCliente.Usuario;
          FieldByName( 'VP_SOL_FEC').AsDateTime := dmCliente.FechaDefault;
          FieldByName( 'VP_PAGO_US').AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmSuper.cdsPlanVacacionCB_CODIGOValidate(Sender: TField);
var
   iEmpleado: Integer;
begin
     iEmpleado := Sender.AsInteger;
     if ( iEmpleado > 0 ) then
     begin
          if ( not cdsEmpleados.Locate( 'CB_CODIGO', iEmpleado, [] ) ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( iEmpleado ) ) = VACIO ) then    // Posiciona el empleado en cdsEmpleadoLookup
                  DataBaseError( 'No Existe el Empleado #' + IntToStr( iEmpleado ) )
               else
                  DataBaseError( 'No Tiene Asignado el Empleado #' + IntToStr( iEmpleado ) + ' para esta Fecha' );
          end;
     end;
end;

procedure TdmSuper.cdsPlanVacacionCB_CODIGOChange(Sender: TField);
begin
     cdsPlanVacacionRecalculaDias;
     RecalculaDiasSaldos;
     with Sender.DataSet do
     begin
          FieldByName( 'PrettyName' ).AsString := dmCliente.cdsEmpleadoLookup.GetDescripcion( FieldByName( 'CB_CODIGO' ).AsString );
     end;
end;

procedure TdmSuper.cdsPlanVacacionVP_FEC_INIChange(Sender: TField);
begin
     cdsPlanVacacionRecalculaDias;
     RecalculaDiasSaldos;
end;

procedure TdmSuper.cdsPlanVacacionVP_FEC_FINChange(Sender: TField);
begin
     cdsPlanVacacionRecalculaDias;
end;
procedure TdmSuper.cdsPlanVacacionVP_STATUSChange(Sender: TField);
begin
     with cdsPlanVacacion do
     begin
          If ( eStatusPlanVacacion( FieldByName('VP_STATUS').AsInteger ) ) in [spvAutorizada , spvRechazada ]  then
          begin
               FieldByName( 'VP_AUT_USR').AsInteger := dmCliente.Usuario;
               FieldByName( 'VP_AUT_FEC').AsDateTime := dmCliente.FechaDefault;
          End
          Else
          begin
               FieldByName( 'VP_AUT_USR' ).AsInteger := 0;
               FieldByName ( 'VP_AUT_FEC' ).AsDateTime := ZetaCommonClasses.NullDateTime;
          End;
     end;
end;

{M�todo repetido en DRecursos.pas por la llamada al servidor ...}
procedure TdmSuper.cdsPlanVacacionVP_PAGO_USChange(Sender: TField);
var
   iAnio,iNumPeriodo :Integer;
   PeriodoValido:Boolean;
begin
     PeriodoValido := True;
     with cdsPlanVacacion do
     begin
          if(  zStrToBool( FieldByName('VP_PAGO_US').AsString  )and
              ( FieldByName('VP_NOMYEAR').AsInteger = 0 ) and
              ( FieldByName('VP_NOMNUME').AsInteger = 0 ) )then
          begin
               //obtener el periodo a sugerir del servidor
               //si no existe ponemos el activo
               if( not ServerRecursos.GetPeriodoPlanVacacion( dmCliente.Empresa, dmCliente.cdsEmpleadoLookUp.FieldByName('CB_NOMINA').AsInteger,
                                                      FieldByName('VP_FEC_INI').AsDateTime,
                                                      iAnio,
                                                      iNumPeriodo ) )then
               begin
                    with dmCliente do
                    begin
                         iAnio  := YearDefault;
                         iNumPeriodo  := PeriodoNumero;
                    end;
               end;
               FieldByName('VP_NOMYEAR').AsInteger := iAnio;
               with dmCatalogos do
               begin
                    if ( ( not ZAccesosMgr.CheckDerecho( D_CONS_PLANVACACION, K_DERECHO_NIVEL0 ) ) )then
                    begin
                         if cdsPeriodo.Locate('PE_NUMERO',iNumPeriodo,[] )then
                            PeriodoValido := cdsPeriodo.FieldByName('PE_STATUS').AsInteger < Ord(spAfectadaParcial)
                         else
                             PeriodoValido := False;
                    end;
               end;
               if  PeriodoValido then
                  iNumPeriodo  := dmCliente.PeriodoNumero
               else
                   iNumPeriodo := 0;
               FieldByName('VP_NOMNUME').AsInteger := iNumPeriodo;
               FieldByName('VP_NOMTIPO').AsInteger := dmCliente.cdsEmpleadoLookUp.FieldByName('CB_NOMINA').AsInteger;
          end;
     end;
end;



Procedure TdmSuper.cdsPlanVacacionRecalculaDias;
begin
     FToolsRH.RecalculaVacaDiasGozados( cdsPlanVacacion,eValorDiasVacaciones( Global.GetGlobalInteger( K_GLOBAL_DIAS_VACACIONES ) ), self.GetVacaDiasGozar, 'VP_', 'VP_DIAS' );
end;

procedure TdmSuper.RecalculaDiasSaldos;
begin
     with cdsPlanVacacion do
          FToolsRH.RecalculaDiasSaldoVacaciones(cdsPlanVacacion, ServerRecursos.GetSaldosVacacion( dmCliente.Empresa, FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'VP_FEC_INI' ).AsDateTime ));
end;

procedure TdmSuper.cdsPlanVacacionBeforePost(DataSet: TDataSet);
begin
     FToolsRH.cdsPlanVacacionBeforePost( DataSet );
end;

procedure TdmSuper.cdsPlanVacacionAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsPlanVacacion do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerRecursos.GrabaPlanVacacion( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enPlanVacacion ] );
               end;
          end;
     end;
end;

procedure TdmSuper.cdsPlanVacacionAlModificar(Sender: TObject);
begin
     with dmCliente do
     begin
          SetTipoLookupEmpleado( eLookEmpNominas );
          cdsEmpleadoLookUp.Init;
          try
             ZbaseEdicion_DevEx.ShowFormaEdicion( EditPlanVacacion_DevEx, TEditPlanVacacion_DevEx );
          finally
             SetTipoLookupEmpleado( eLookEmpGeneral );
          end;
     end;
end;

procedure TdmSuper.SetAutorizarPlanVacacion( const tipoAutorizacion: eStatusPlanVacacion; const lTodos: Boolean );
begin
     FToolsRH.SetStatusPlanVacacion( cdsPlanVacacion, tipoAutorizacion, lTodos );
     cdsPlanVacacion.Enviar;
end;

procedure TdmSuper.RegistrarTarjetaPuntual (const sChecada: string);
begin
      FToolsAsistencia.RegistrarTarjetaPuntual(cdsChecadas, sChecada);
end;

function TdmSuper.GetChecadaPuntual(const sHorario: string; iTipoChecada: Integer; var sChecada: String): boolean;
begin
     Result := FToolsAsistencia.GetChecadaPuntual(dmCatalogos.cdsHorarios, sHorario, iTipoChecada, sChecada);
end;

procedure TdmSuper.RegistrarTarjetaPuntualColectiva( DataSet:TZetaClientDataSet; const sChecada, sCampoChecada, sTitulo: string  );
begin
     with DataSet do
     begin
          if ( (sCampoChecada = 'CHECADA4') AND (FieldByName( 'CHECADA2' ).AsString = sChecada) ) then
             ZError( sTitulo,  Format( 'Ya existe checada a las %s', [MaskHora(ConvierteHora( sChecada ))] ), 0 )
          else
          begin
               edit;
               FieldByName(sCampoChecada).AsString := ConvierteHora( sChecada );
               //post;
          end;
     end;
end;

{ cdsHisCursos }

procedure TdmSuper.cdsHisCursosAlAdquirirDatos(Sender: TObject);
begin
     dmCatalogos.cdsCursos.Conectar;
     with dmCliente do
     begin
          cdsHisCursos.Data := ServerSuper.GetHisCursos( Empresa, Empleado );
     end;

end;

procedure TdmSuper.cdsHisCursosAlCrearCampos(Sender: TObject);
begin
     with cdsHisCursos do
     begin
          MaskNumerico( 'SE_FOLIO', '0;-0;#' );
           {***DevEx (by am): Se agrego la mascara para fechas que manejamos pues estos campos no la tenian. Sin embargo,
              aparecian en el formato indicado pues el componente del ZetaDBGrid muestra las fechas con ese formato. El CXGrid
              que utilizamos no lo hace asi por lo tanto debemos de poner la mascara a los campos tipo fecha***}
          MaskFecha( 'KC_FEC_TOM');
          {***}
          // En realidad la columna se llama CU_DESCRIP, es solamente por compatibilidad con Tress
          CreateSimpleLookup( dmCatalogos.cdsCursos, 'CU_NOMBRE', 'CU_CODIGO');
          FieldByName('KC_FEC_TOM').OnChange := OnKC_FEC_TOMChange;
          FieldByName('CU_CODIGO').OnChange := CU_CODIGOChange;
     end;
end;

procedure TdmSuper.CU_CODIGOChange(Sender: TField);
begin
     with cdsHisCursos do
     begin
          FieldByName('MA_CODIGO').AsString:= dmCatalogos.cdsCursos.FieldByName('MA_CODIGO').AsString;
          FieldByName('KC_HORAS').AsString:= dmCatalogos.cdsCursos.FieldByName('CU_HORAS').AsString;
          FieldByName ('KC_REVISIO').AsString := dmCatalogos.cdsCursos.FieldByName( 'CU_REVISIO' ).AsString;
     end;
end;

procedure TdmSuper.cdsCursosAlModificar(Sender: TObject);
begin
     //Si viene de cdsGrupos siempre va a resultar Verdadero y va a posicionar FFolioSesion
     if ( ChecaFolioSesion( TZetaClientDataSet( Sender ).FieldByName('SE_FOLIO').AsInteger ) ) then
     begin
          FPosicionaEmpleadoGrupo:= ( TZetaClientDataSet( Sender ).Name = 'cdsHisCursos' );
          cdsSesion.Modificar;
     end
     else
         ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisCurso_DevEx, TEditHisCurso_DevEx );
end;

procedure TdmSuper.AgregarMatrizCurso(FCurso : String);
begin
     with cdsHisCursos do
     begin
          Insert;
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('KC_FEC_TOM').AsDateTime := dmCliente.FechaDefault;
          if dmCatalogos.cdsCursos.Locate( 'CU_CODIGO', FCurso, [] ) then
             FieldByName('CU_CODIGO').AsString  := FCurso
          else
          begin
               FieldByName('KC_HORAS').AsInteger  := 0;
               FieldByName('KC_EVALUA').AsInteger  := 0;
          end;
          FieldByName('KC_EST').AsString := VACIO;
          FieldByName('SE_FOLIO').AsInteger  := 0;
          with GetDatosClasificacion( dmCliente.Empleado, dmCliente.FechaDefault ) do
          begin
               FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
               FieldByName( 'CB_TURNO' ).AsString := Turno;
               FieldByName( 'CB_PUESTO' ).AsString := Puesto;
               FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
               FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
               FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
               FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
               FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
               FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
               FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
               FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
               FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
               {$ifdef ACS}
               FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
               FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
               FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
               {$endif}
          end;
     end;

     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisCurso_DevEx, TEditHisCurso_DevEx );
end;

procedure TdmSuper.cdsCursosAlAgregar(Sender: TObject);
begin
     with cdsSesion do
     begin
          Conectar;
          Agregar;
     end;
end;

procedure TdmSuper.cdsHisCursosAlBorrar(Sender: TObject);

  function GetMensajeBorrar: String;
  begin
       if ( ( ChecaFolioSesion( cdsHisCursos.FieldByName('SE_FOLIO').AsInteger  ) ) ) then
       begin
            Result:= Format('� Desea eliminar al empleado %d de la sesi�n %d ?', [cdsHisCursos.FieldByName('CB_CODIGO').AsInteger,
                                                                                  cdsHisCursos.FieldByName('SE_FOLIO').AsInteger ]);
       end
       else
       begin
            Result:= '� Desea eliminar el registro ?';
       end
  end;

begin
     if ZetaMsgDlg.ConfirmaCambio( GetMensajeBorrar ) then
     begin
          with cdsHisCursos do
          begin
               Delete;
               Enviar;
          end;
     end;
end;



procedure TdmSuper.cdsHisCursosBeforePost(DataSet: TDataSet);
begin
     with cdsHisCursos do
     begin
          if ( FieldByName('KC_FEC_FIN').AsDateTime < FieldByName('KC_FEC_TOM').AsDateTime ) then
             DataBaseError( 'Fecha de Terminaci�n debe ser Mayor o Igual a Inicio' );
          if strVacio(FieldByName('CU_CODIGO').AsString) then
             DataBaseError( 'El C�digo del Curso no puede quedar vac�o' );
     end;
end;

procedure TdmSuper.OnKC_FEC_TOMChange(Sender: TField);
begin
     cdsHisCursos.FieldByName('KC_FEC_FIN').AsDateTime := Sender.AsDateTime;
end;


function TdmSuper.ChecaFolioSesion( iFolio: Integer ): Boolean;
begin
     Result := ( iFolio <> 0 );
     if ( Result ) then
        FFolioSesion:= iFolio ;
end;

function TdmSuper.EsSuSesion( iFolio: Integer ): Boolean;
begin
     FFolioSesion:= iFolio; //Anteriormente posicion� FFolioSesion al preguntar si ten�a sesion, es solo para asegurarnos
     ObtieneSesion( FALSE ); //Obtiene las sesion de ese folio
     Result:= ( cdsSesion.FieldByName('US_CODIGO').AsInteger = dmCliente.Usuario );
end;

procedure TdmSuper.cdsHisCursosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsHisCursos do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconciliar( ServerRecursos.GrabaGridCursos( dmCliente.Empresa, Delta, ErrorCount ) );
               FCambioMatrizCursos := True;
          end;
     end;
end;

{cdsHisCursosProg}

procedure TdmSuper.cdsHisCursosProgAlAdquirirDatos(Sender: TObject);
begin
     dmCatalogos.cdsCursos.Conectar;
     with dmCliente do
          cdsHisCursosProg.Data := ServerRecursos.GetHisCursosProg( Empresa, Empleado, TRUE );
end;

procedure TdmSuper.cdsHisCursosProgAlCrearCampos(Sender: TObject);
begin
     with cdsHisCursosProg do
     begin
          MaskFecha( 'KC_FEC_PRO' );
          MaskFecha( 'KC_FEC_TOM' );
          MaskFecha( 'KC_PROXIMO' );
          // En realidad la columna se llama CU_DESCRIP, es solamente por compatibilidad con Tress
          CreateSimpleLookup( dmCatalogos.cdsCursos, 'CU_NOMBRE', 'CU_CODIGO');
     end;

end;

{cdsAprobados}

procedure TdmSuper.cdsAprobadosAlAdquirirDatos(Sender: TObject);
begin
     with cdsAprobados do
     begin
          Data := ServerRecursos.GetAprobados( dmCliente.Empresa, FFolioSesion  );
          FBufferAprobados:= Data;
          FHayBufferAprobados:= FALSE;
     end;
end;

procedure TdmSuper.cdsAprobadosAlCrearCampos(Sender: TObject);
begin
     with cdsAprobados do
     begin
          MaskHoras( 'KC_HORAS' );
          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := CursosCB_CODIGOChange;
     end;
end;



procedure TdmSuper.cdsGridCB_CODIGOValidate(Sender: TField);
begin
     //Se utiliza la validacion en Grid de Cursos y en Registro de Transferencias.
     with Sender do
     begin
          if ( AsInteger > 0 ) then
          begin
               if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( AsInteger ) ) <> VACIO ) then
               begin
                    if not ( BuscaEmpleado ( AsInteger ) ) then
                       DataBaseError( 'Empleado # ' + IntToStr( AsInteger ) + ' No se encuentra asignado para esa fecha'  );
               end
               else
                   DataBaseError( 'No existe el Empleado #' + IntToStr( AsInteger ) );

          end
          else
              DataBaseError( 'N�mero de empleado debe ser mayor a cero' );
     end;
end;

procedure TdmSuper.CursosCB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          with dmCliente do
          begin
               FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
               FieldByName( 'CB_PUESTO' ).AsString := cdsEmpleados.FieldByName( 'CB_PUESTO' ).AsString;

               with GetDatosClasificacion( FieldByName( 'CB_CODIGO' ).AsInteger, dmCliente.FechaSupervisor ) do
               begin
                    FieldByName( 'CB_CLASIFI' ).AsString := Clasificacion;
                    FieldByName( 'CB_TURNO' ).AsString := Turno;
                    FieldByName( 'CB_PUESTO' ).AsString := Puesto;
                    FieldByName( 'CB_NIVEL1' ).AsString := Nivel1;
                    FieldByName( 'CB_NIVEL2' ).AsString := Nivel2;
                    FieldByName( 'CB_NIVEL3' ).AsString := Nivel3;
                    FieldByName( 'CB_NIVEL4' ).AsString := Nivel4;
                    FieldByName( 'CB_NIVEL5' ).AsString := Nivel5;
                    FieldByName( 'CB_NIVEL6' ).AsString := Nivel6;
                    FieldByName( 'CB_NIVEL7' ).AsString := Nivel7;
                    FieldByName( 'CB_NIVEL8' ).AsString := Nivel8;
                    FieldByName( 'CB_NIVEL9' ).AsString := Nivel9;
{$ifdef ACS}
                    FieldByName( 'CB_NIVEL10' ).AsString := Nivel10;
                    FieldByName( 'CB_NIVEL11' ).AsString := Nivel11;
                    FieldByName( 'CB_NIVEL12' ).AsString := Nivel12;
{$endif}
               end;
               { Asigna Valores Default    AP(31/10/2007): No es necesario en supervisores
               if ( FieldByName( 'KC_EVALUA' ).AsFloat = 0 ) then
                  FieldByName( 'KC_EVALUA' ).AsFloat:= FUltimaEval;   }

               FieldByName('KC_HORAS').AsFloat  := cdsSesion.FieldByName('SE_HORAS').AsFloat;
          end;
     end;
end;

procedure TdmSuper.cdsAprobadosAfterEditInsert(DataSet: TDataSet);
begin
     with cdsSesion do
     begin
          if not( State in [dsInsert,dsEdit] ) then
             Edit;
     end;
end;

{cdsSesion}

procedure TdmSuper.cdsSesionAlAdquirirDatos(Sender: TObject);
begin
     ObtieneSesion( TRUE );
end;

procedure TdmSuper.ObtieneSesion( const lConectaEmpleados: Boolean );
begin
     if ( FFolioSesion <> 0 ) then
     {$ifdef ICUMEDICAL_CURSOS}
     begin
     {$endif}
          cdsSesion.Data := ServerSuper.GetSesion( dmCliente.Empresa, FFolioSesion );
     {$ifdef ICUMEDICAL_CURSOS}
          FPathICU := cdsSesion.FieldByName('SE_D_RUTA').AsString;
     end
     else
         FPathICU := VACIO;
     {$endif}
     if ( lConectaEmpleados ) then
     begin
          cdsDatosEmpleado.Conectar;
          cdsAprobados.Refrescar;

          if ( FPosicionaEmpleadoGrupo ) then
          begin
               cdsAprobados.Locate('CB_CODIGO', cdsHisCursos.FieldByName('CB_CODIGO').AsInteger, [] );
          end;
     end;
     FPosicionaEmpleadoGrupo:= FALSE;
end;
procedure TdmSuper.cdsSesionAlCrearCampos(Sender: TObject);
begin
     cdsSesion.FieldByName('CU_CODIGO').OnChange := SESIONCU_CODIGOChange;
end;

procedure TdmSuper.cdsSesionAfterInsert(DataSet: TDataSet);
begin
     with cdsAprobados do
     begin
          if Active then
          begin
               FHayBufferAprobados:= True;
               EmptyDataset;
          end;
     end;
end;

procedure TdmSuper.cdsSesionNewRecord(DataSet: TDataSet);
begin
     with cdsSesion do
     begin
          FieldByName('US_CODIGO').AsInteger:= dmCliente.Usuario;
          FieldByName('SE_FEC_INI').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('SE_FEC_FIN').AsDateTime:= dmCliente.FechaDefault;
          {$ifdef ICUMEDICAL_CURSOS}
          FPathICU := VACIO;
          {$endif}
     end;
end;

procedure TdmSuper.cdsSesionAfterCancel(DataSet: TDataSet);
begin
     with cdsAprobados do
     begin
          if Active then
          begin
               CancelUpdates;
               if FHayBufferAprobados then
                  Data:= FBufferAprobados;
          end;
     end;
end;

procedure TdmSuper.cdsSesionAlModificar(Sender: TObject);
begin
     {***DevEx (by am)}
      ZBaseEdicion_DevEx.ShowFormaEdicion( GridCursosSuper, TGridCursosSuper );
end;


procedure TdmSuper.cdsSesionAfterDelete(DataSet: TDataSet);
begin
     cdsSesion.Enviar;
end;

procedure  TdmSuper.cdsSesionAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorKCCount, iChangeKCCount: Integer;
   iFolio: Integer;
   oSesion, KCResults: OleVariant;
begin
     with cdsAprobados do
     begin
          PostData;
          iChangeKCCount := ChangeCount;
     end;
     ErrorCount := 0;
     ErrorKCCount := 0;
     with cdsSesion do
     begin
          PostData;
          if ( ( ChangeCount > 0 ) or ( iChangeKCCount > 0 ) ) then
          begin
               ErrorCount := ChangeCount;
               ErrorKCCount := iChangeKCCount;
               DisableControls;
               try
                  Repeat
                        oSesion:= dmCatalogos.GrabaSesiones( DeltaNull, cdsAprobados.DeltaNull, KCResults, iFolio, ErrorCount, ErrorKCCount );
                        if (  ( ChangeCount = 0 ) or Reconcile( oSesion )  )then
                        begin
                             if( iFolio > 0 )then
                             begin
                                  Edit;
                                  FieldByName('SE_FOLIO').AsInteger := iFolio;
                                  Post;
                                  MergeChangeLog;
                             end;
                        end;

                  Until ( ( ErrorCount > 0 ) or ( cdsAprobados.Reconcile( KCResults )  ) );

                  if ( ErrorCount = 0 ) then
                     TressShell.SetDataChange( [ enSesion ] )
                  else
                  begin
                       if ( ChangeCount > 0 ) then
                       begin
                            Edit;
                       end;
                  end;
               finally
                      EnableControls;
                      FHayBufferAprobados:= FALSE;
               end;
          end;
     end;
end;

{ AP(31/10/2007): No es necesario en supervisores
procedure TdmSuper.cdsAprobadosBeforePost(DataSet: TDataSet);
begin
     with DataSet do
          FUltimaEval := FieldByName( 'KC_EVALUA' ).AsFloat;
end; }

procedure TdmSuper.cdsSesionBeforePost(DataSet: TDataSet);

procedure ValidaCamposDiferentes;

    procedure ValidaDiferencia( const sCampoAprobados, sCampoSesion: String );
    begin
         with cdsAprobados do
         begin
              if ( FieldByName(sCampoAprobados).Value <> cdsSesion.FieldByName( sCampoSesion ).Value ) then
              begin
                   if not ( cdsAprobados.State in [dsInsert, dsEdit ] ) then
                      cdsAprobados.Edit;
                   cdsAprobados.FieldByName(sCampoAprobados).Value:= cdsSesion.FieldByName(sCampoSesion).Value;
              end;
         end;
    end;

begin
     with cdsAprobados do
     begin
          ValidaDiferencia('CU_CODIGO', 'CU_CODIGO' );
          ValidaDiferencia('SE_FOLIO', 'SE_FOLIO');
          ValidaDiferencia('MA_CODIGO', 'MA_CODIGO');
          ValidaDiferencia('KC_FEC_TOM', 'SE_FEC_INI');
          ValidaDiferencia('KC_FEC_FIN', 'SE_FEC_FIN');
          ValidaDiferencia('KC_REVISIO','SE_REVISIO' );
          ValidaDiferencia('KC_HORAS','SE_HORAS' );
          ValidaDiferencia('KC_EST','SE_EST' );
     end;
end;

begin
     with DataSet do
     begin
          if StrVacio( FieldByName('CU_CODIGO').AsString ) then
          begin
               DataBaseError( 'No se ha especificado un curso' );
          end
          else
          if ( FieldByName('SE_HORAS').AsFloat <= 0 ) then
          begin
               DataBaseError( 'La Duraci�n Debe Ser Mayor A Cero' );
          end;
          if( FieldByName('SE_FEC_FIN').AsDateTime < FieldByName('SE_FEC_INI').AsDateTime )then
          begin
               FieldByName('SE_FEC_FIN').FocusControl;
               DataBaseError( 'La Fecha De Inicio Debe Der Menor o Igual A La Fecha De Terminaci�n' );
          end;

          {$ifdef ICUMEDICAL_CURSOS}
          if ( State in [dsEdit, dsInsert] ) then
          begin
               FieldByName( 'SE_D_NOM' ).AsString := ExtractFileName( FPathICU );
               FieldByName('SE_D_EXT').AsString := UpperCase( Copy( ExtractFileExt( FPathICU ), 2 , 255 ) );
               FieldByName('SE_D_RUTA').AsString := FPathICU;
          end;
          {$endif}

          with cdsAprobados do
          begin
               First;
               while not EOF do
               begin
                    ValidaCamposDiferentes;
                    Next;
               end;
          end;
     end;
end;

{cdsGrupos}

procedure TdmSuper.ObtieneSesionesFiltros;
var
   aUsuario: array [FALSE..TRUE] of Integer;
begin
     //Sino tiene el derecho de Consulta de grupos no llega hasta aqui, es por eso que nada mas se valida el de otros supervisores
     aUsuario[FALSE]:= dmCliente.Usuario;
     aUsuario[TRUE]:= 0;

     //Filtros para integrar cursos en supervisores
     with FParametrosSesion do
     begin
          AddBoolean( 'FiltraCurso', TRUE );   // es para integrar nada mas los de la vista de cursos
          AddInteger('Usuario', aUsuario[ CheckDerecho( D_SUPER_SESIONES_OTROS, K_DERECHO_CONSULTA ) ] );
     end;

     with cdsGrupos do
     begin
          Data := ServerRecursos.GetSesiones( dmCliente.Empresa, FParametrosSesion.VarValues );
     end;
end;


procedure TdmSuper.cdsGruposAlCrearCampos(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsMaestros.Conectar;
          cdsCursos.Conectar;
     end;
     with cdsGrupos do
     begin
          // En realidad la columna se llama CU_DESCRIP, es solamente por compatibilidad con Tress
          CreateSimpleLookup( dmCatalogos.cdsCursos, 'CU_NOMBRE', 'CU_CODIGO' );
          CreateSimpleLookup( dmCatalogos.cdsMaestros, 'MA_NOMBRE', 'MA_CODIGO' );
          CreateCalculated( 'SE_COSTOT', ftFloat, 0 );
          MaskTime( 'SE_HOR_INI' );
          MaskTime( 'SE_HOR_FIN' );
          MaskPesos( 'SE_COSTOT' );
          ListaFija( 'SE_STATUS', lfStatusSesiones );
           {***DevEx (by am): Se agrego la mascara para fechas que manejamos pues estos campos no la tenian. Sin embargo,
              aparecian en el formato indicado pues el componente del ZetaDBGrid muestra las fechas con ese formato. El CXGrid
              que utilizamos no lo hace asi por lo tanto debemos de poner la mascara a los campos tipo fecha***}
          MaskFecha( 'SE_FEC_INI' );
          MaskFecha( 'SE_FEC_FIN' );
          {***}
     end;
end;

procedure TdmSuper.cdsGruposCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'SE_COSTOT' ).AsFloat := FieldByName('SE_COSTO1' ).AsFloat +
                                                FieldByName( 'SE_COSTO2' ).AsFloat +
                                                FieldByName( 'SE_COSTO3' ).AsFloat;
     end;
end;

procedure TdmSuper.SESIONCU_CODIGOChange(Sender: TField);
begin
     with cdsSesion do
     begin
          if ( FieldByName( 'CU_CODIGO' ).AsString <> dmCatalogos.cdsCursos.FieldByName('CU_CODIGO').AsString ) then
               dmCatalogos.cdsCursos.Locate( 'CU_CODIGO', FieldByName( 'CU_CODIGO' ).AsString, [] );
          if( strLleno( FieldByName('CU_CODIGO').AsString ) )then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'SE_HORAS' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_HORAS' ).AsFloat;
               FieldByName( 'MA_CODIGO' ).AsString := dmCatalogos.cdsCursos.FieldByName('MA_CODIGO').AsString;
               FieldByName( 'SE_REVISIO' ).AsString := dmCatalogos.cdsCursos.FieldByName( 'CU_REVISIO' ).AsString;
               FieldByName( 'SE_COSTO1' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_COSTO1' ).AsFloat;
               FieldByName( 'SE_COSTO2' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_COSTO2' ).AsFloat;
               FieldByName( 'SE_COSTO3' ).AsFloat := dmCatalogos.cdsCursos.FieldByName( 'CU_COSTO3' ).AsFloat;
               {$ifdef ICUMEDICAL_CURSOS}
               FPathICU := FieldByName('SE_D_RUTA').AsString;
               {$endif}
          end;
     end;
end;

procedure TdmSuper.cdsGruposAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar el registro ?' ) then
     begin
          FFolioSesion:= cdsGrupos.FieldByName('SE_FOLIO').AsInteger;
          ObtieneSesion( TRUE );
          cdsSesion.Delete;
     end;
end;


{$ifdef VER130}
procedure TdmSuper.cdsSesionReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);

    function CursoNoProg: Boolean;
    begin
         Result := ( Pos( 'Advertencia', E.Message ) > 0 );
    end;

begin
     Action := raCancel;

     if ( CursoNoProg  ) then
     begin
          if ZetaDialogo.ZWarningConfirm( K_TITLE_ADVERTENCIA, E.Message + CR_LF + CR_LF +
                                          '� Desea registrarlo ?', 0, mbCancel ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               with DataSet do
               begin
                    Edit;
                    FieldByName( 'OPERACION' ).NewValue := Ord( ocIgnorar );
                    Post;
                    Action := raCorrect;
               end;
          end
     end
     else
     begin
          MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
     end;
end;

{$else}
procedure TdmSuper.cdsSesionReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);

    function CursoNoProg: Boolean;
    begin
         Result := ( Pos( 'Advertencia', E.Message ) > 0 );
    end;

begin
     Action := raCancel;

     if ( CursoNoProg  ) then
     begin
          if ZetaDialogo.ZWarningConfirm( K_TITLE_ADVERTENCIA, E.Message + CR_LF + CR_LF +
                                          '� Desea registrarlo ?', 0, mbCancel ) then
          begin
               {  Con un solo registro que se elija continuar se enviar� ocIgnorar para volver a reconciliar,
                  los registros que se decide no continuar se cancelan }
               with DataSet do
               begin
                    Edit;
                    FieldByName( 'OPERACION' ).NewValue := Ord( ocIgnorar );
                    Post;
                    Action := raCorrect;
               end;
          end
     end
     else
     begin
          MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
     end;
end;
{$endif}




{$ifdef VER130}
procedure TdmSuper.cdsPlanVacacionReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmSuper.cdsPlanVacacionReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
begin
     ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
     Action := raCancel;
end;

procedure TdmSuper.cdsGridTarjetasPeriodoAlAdquirirDatos( Sender: TObject);
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with cdsGridTarjetasPeriodo do
        begin
             with Parametros do
             begin
                  with dmCliente do
                  begin
                       AddDate('FechaIni',GetDatosPeriodoActivo.InicioAsis  );
                       AddDate('FechaFin',GetDatosPeriodoActivo.FinAsis );
                       AddDate('Year',GetDatosPeriodoActivo.Year );
                       AddInteger('Tipo',Ord( GetDatosPeriodoActivo.Tipo ) );
                       AddInteger('Numero',GetDatosPeriodoActivo.Numero );

                       AddInteger ('Empleado',Empleado);

                       AddBoolean ('PuedeAgregarTarjeta',ZAccesosMgr.CheckDerecho( D_SUPER_AJUSTAR_ASISTENCIA , K_DERECHO_ALTA ) );
                       AddBoolean ('PuedeModificarAnteriores',PuedeModificarTarjetasAnteriores );

                       AddBoolean ('EsFechaLimite',( not Global.GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS ) ) );
                       AddDate('FechaLimite',Global.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ) );

                       AddBoolean('EsSupervisores', True);
                       AddInteger('UsuarioActivo', Usuario );
                  end;
             end;
             Data := ServerSuper.GetTarjetasPeriodo( dmCliente.Empresa, Parametros.VarValues );
        end;
     finally
            FreeAndNil( Parametros );
     end;
end;


procedure TdmSuper.AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.DataSet.IsEmpty then
        Text := ''
     else
        Text := Sender.AsString + ' -  ' + ObtieneElemento( lfDiasSemana, DayOfWeek( Sender.AsDateTime ) - 1 );
end;


procedure TdmSuper.cdsGridTarjetasPeriodoAlCrearCampos(Sender: TObject);
begin
     with cdsGridTarjetasPeriodo do
     begin
          FieldByName( 'AU_FECHA' ).OnGetText := AU_FECHAGetText;
          cdsGridAsistenciaAlCrearCamposGenerales( Sender );
     end;
end;


procedure TdmSuper.cdsGridTarjetasPeriodoAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
   Parametros: TZetaParams;
begin
     ErrorCount := 0;
     with cdsGridTarjetasPeriodo do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Parametros := TZetaParams.Create;
               try
                  Parametros.AddInteger( 'Cambios', ChangeCount );
                  if Reconcile ( ServerSuper.GrabaTarjetaPeriodo( dmCliente.Empresa, Parametros.VarValues, ErrorCount ,DeltaNull ) ) then
                  begin
                       TressShell.SetDataChange( [ enAusencia,enChecadas,enNomina ] );
                  end;
               finally
                      FreeAndNil( Parametros );
               end;
          end;
     end;
end;



procedure TdmSuper.cdsGridAsistenciaBeforePostGenerales(DataSet: TDataSet);

    function ExisteMotivoVacio: Boolean;

          function ChecadaInvalida(sChecada: String): Boolean;
          begin
               with DataSet do
               begin
                    Result:= StrLleno( FieldByName(sChecada).AsString )  and
                             ( FieldByName('US_'+sChecada).AsInteger > 0 ) and
                               StrVacio( FieldByName('MC_'+sChecada).AsString );
                    if Result then
                       FieldByName('MC_'+sChecada).FocusControl;
               end;
          end;
     begin
          Result:= ChecadaInvalida('CHECADA1') or
                   ChecadaInvalida('CHECADA2') or
                   ChecadaInvalida('CHECADA3') or
                   ChecadaInvalida('CHECADA4');
     end;


     procedure ConvierteUpperCaseMotivos;

         procedure Convierte( const sCampo: String );
         begin
              with DataSet.FieldByName(sCampo) do
              begin
                   if StrLleno( AsString ) then
                      AsString:= UpperCase(AsString);
              end;
         end;
     begin
          Convierte('MC_CHECADA1');
          Convierte('MC_CHECADA2');
          Convierte('MC_CHECADA3');
          Convierte('MC_CHECADA4');
     end;


begin
     with dmCliente do
     begin
          with DataSet do
          begin
               if not PuedeCambiarTarjetaDlg( FieldByName( 'AU_FECHA' ).AsDateTime, Empleado )then
               begin
                    Abort;
               end

               else if (CapturaObligatoriaMotCH) and (ExisteMotivoVacio) then
               begin
                    DataBaseError('No se ha especificado un motivo de checada manual');
               end
               else
               begin
                    ValidaAutoBeforePost( acExtras, FieldByName( 'HRS_EXTRAS' ).AsFloat, FieldByName( 'M_HRS_EXTRAS' ) );
                    ValidaAutoBeforePost( acDescanso, FieldByName( 'DESCANSO' ).AsFloat, FieldByName( 'M_DESCANSO' ) );
                    ValidaAutoBeforePost( acConGoce, FieldByName( 'PER_CG' ).AsFloat, FieldByName( 'M_PER_CG' ) );
                    ValidaAutoBeforePost( acConGoceEntrada, FieldByName( 'PER_CG_ENT' ).AsFloat, FieldByName( 'M_PER_CG_ENT' ) );
                    ValidaAutoBeforePost( acSinGoce, FieldByName( 'PER_SG' ).AsFloat, FieldByName( 'M_PER_SG' ) );
                    ValidaAutoBeforePost( acSinGoceEntrada, FieldByName( 'PER_SG_ENT' ).AsFloat, FieldByName( 'M_PER_SG_ENT' ) );
                    ValidaAutoBeforePost( acPrepagFueraJornada, FieldByName( 'PRE_FUERA_JOR' ).AsFloat, FieldByName( 'M_PRE_FUERA_JOR' ) );
                    ValidaAutoBeforePost( acPrepagDentroJornada, FieldByName( 'PRE_DENTRO_JOR' ).AsFloat, FieldByName( 'M_PRE_DENTRO_JOR' ) );
               end;

               if ( PermitirCapturaMotivoCH ) then
                  ConvierteUpperCaseMotivos;
          end;
     end;
end;

procedure TdmSuper.cdsGridTarjetasPeriodoBeforePost(DataSet: TDataSet);
begin
     with cdsGridTarjetasPeriodo do
     begin
          if ( FieldByName('SUPER_DENTRO').AsInteger = 0  )then
             DataBaseError ( 'No se puede modificar tarjetas de Asistencia de otros Supervisores ' )
          else
              cdsGridAsistenciaBeforePostGenerales( DataSet );
     end;
end;

function TdmSuper.PuedeCambiarTarjeta( const dInicial, dFinal: TDate; const iEmpleado: integer; var sMensaje : string ):Boolean;
begin 
     Result := dmCliente.PuedeModificarTarjetasAnteriores; 
     if NOT Result then 
     begin 
          if Global.GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS ) then 
          begin 
               //si el empleado es CERO, quiere decir que viene de un proceso
               Result := iEmpleado = 0; 
               if NOT Result then 
               begin 
                    if ( dInicial = dFinal )   then 
                       Result := PuedeCambiarTarjetaStatus( dInicial, 
                                                            iEmpleado, 
                                                            eStatusPeriodo( Global.GetGlobalInteger( K_LIMITE_MODIFICAR_ASISTENCIA ) ), 
                                                            FALSE, 
                                                            sMensaje ) 
                    else 
                        //En teoria nunca debe de llegar a mostrarse este error. 
                        sMensaje := 'Bloqueo por No se Puede Validar por Rango de Fechas'; 
               end 
          end 
          else 
          begin 
               { Revisa del servidor la fecha l�mite de asistencia} 
               Result := ZetaCommonTools.PuedeCambiarTarjeta( dInicial, 
                                                              dFinal, 
                                                              ServerSuper.GetFechaLimite( dmCliente.Empresa ), 
                                                              FALSE, 
                                                              sMensaje  ); 
          end; 
     end; 
end; 


procedure TdmSuper.cdsGridTarjetasPeriodoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     with DataSet do
          sError:= Format( ' � Error al Realizar Ajuste de Asistencia !' + CR_LF +
                             ' Fecha: %s Empleado: %d ', [ FechaCorta( FieldByName('AU_FECHA').AsDateTime), FieldByName('CB_CODIGO').AsInteger ] );
     cdsGridTarjetasPeriodo.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO;AU_FECHA', sError + CR_LF + GetErrorDescription( E ) );
     Action := raCancel;
end;
                     
procedure TdmSuper.cdsChecadasAfterCancel(DataSet: TDataSet);
begin
     FUltimoMotivoCH := VACIO;
end;

procedure TdmSuper.cdsCertEmpAlAdquirirDatos(Sender: TObject);
begin
	//devex by MP : correccion de bug de refrescado de cercitificaciones de un empleado en vista local de datos de empleado
    if not GetCertEmpLocalFromCdsDatosEmp then
    begin
     with dmCliente do
     begin
          cdsCertEmp.Data := ServerRecursos.GetKarCertificaciones(Empresa,Empleado);
     end;
     end
     else
     begin
     cdsCertEmp.Data := ServerRecursos.GetKarCertificaciones(dmCliente.Empresa,cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger);
     end;
//Fin de la correccion
end;

procedure TdmSuper.cdsCertEmpAlCrearCampos(Sender: TObject);
begin
      with cdsCertEmp do
     begin
          CreateSimpleLookup( dmCatalogos.cdsCertificaciones, 'CI_NOMBRE', 'CI_CODIGO' );
          CreateCalculated( 'VENCIMIENTO', ftstring,15 );
          FieldByName( 'CI_CODIGO' ).OnChange := OnCI_CODIGOChange;
//Edit by MP : Se agregaron mascaras de Fecha para datos de consulta de empleado
          MaskFecha('KI_FEC_CER');
          MaskFecha('VENCIMIENTO');
//Fin del edit
     end;
end;

procedure TdmSuper.OnCI_CODIGOChange(Sender: TField);
begin
     with dmCatalogos.cdsCertificaciones do
     begin
          if Active and Locate( 'CI_CODIGO', Sender.AsString, [] ) then
             cdsCertEmp.FieldByName('KI_RENOVAR').AsInteger := FieldByName('CI_RENOVAR').AsInteger;
     end;
end;

procedure TdmSuper.cdsCertEmpCalcFields(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if FieldByName( 'KI_RENOVAR' ).AsInteger = 0 then
          begin
               FieldByName( 'VENCIMIENTO' ).AsString := VACIO
          end
          else
          begin
               FieldByName( 'VENCIMIENTO' ).AsString := FechaCorta( FieldByName( 'KI_FEC_CER' ).AsDateTime +
                                                                FieldByName( 'KI_RENOVAR' ).AsInteger);
          end;
     end;
end;

procedure TdmSuper.ObtieneTransferencias( Params: TZetaParams );
begin
     ObtieneTransferenciasDataset( cdsCosteoTransferencias, Params );
end;

procedure TdmSuper.ObtieneTransferenciasDataset( DatasetCosteo:TZetaClientDataset; Params: TZetaParams );
begin
     with dmCliente do
     begin
          DatasetCosteo.Data := ServerAsistencia.GetCosteoTransferencias( Empresa, Params.VarValues );
     end;
end;

{ cdsCosteoTransferencias }

procedure TdmSuper.cdsCosteoTransferenciasAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     dmTablas.GetDataSetTransferencia.Conectar;
     with TZetaClientDataset(Sender) do
     begin
          ListaFija( 'TR_TIPO', lfTipoHoraTransferenciaCosteo );
          ListaFija( 'TR_STATUS', lfStatusTransCosteo );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_APRUEBA', 'TR_APRUEBA' );
          CreateSimpleLookup( dmTablas.GetDataSetTransferencia, 'CC_DESCRIP', 'CC_ORIGEN' );

          MaskFecha('TR_FECHA');
          {***DevEx (by am): Se agrego la mascara para fechas que manejamos pues estos campos no la tenian. Sin embargo,
              aparecian en el formato indicado pues el componente del ZetaDBGrid muestra las fechas con ese formato. El CXGrid
              que utilizamos no lo hace asi por lo tanto debemos de poner la mascara a los campos tipo fecha***}
          MaskFecha( 'AU_FECHA' );
          {***}
          MaskHoras( 'TR_HORAS');

          with FieldByName( 'TR_MOTIVO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := TR_MOTIVOGetText;
          end;

          with FieldByName( 'US_CODIGO' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := US_CODIGOGetText;
          end;

          with FieldByName( 'TR_APRUEBA' ) do
          begin
               Alignment := taLeftJustify;
               OnGetText := US_CODIGOGetText;
               OnChange  := TR_APRUEBAChange;
          end;

          FieldByName( 'TR_STATUS' ).OnChange  := TR_STATUSChange;

          FieldByName( 'CB_CODIGO' ).OnValidate := cdsGridCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange := TransferenciasCB_CODIGOChange;
     end;
end;

procedure TdmSuper.TransferenciasCB_CODIGOChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          with dmCliente do
          begin
               FieldByName( 'PRETTYNAME' ).AsString := cdsEmpleadoLookup.GetDescription;
          end
     end;
end;

procedure TdmSuper.TR_STATUSChange(Sender: TField);
begin
     case eStatusTransCosteo(Sender.AsInteger) of
          stcAprobada..stcCancelada: Sender.DataSet.FieldByName( 'TR_FEC_APR' ).AsDateTime := Date;
          stcPorAprobar: Sender.DataSet.FieldByName( 'TR_FEC_APR' ).AsDateTime := NullDateTime;
     end;
end;

procedure TdmSuper.TR_APRUEBAChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          if ( FieldByName( 'US_CODIGO' ).AsInteger = FieldByName( 'TR_APRUEBA' ).AsInteger ) then
          begin
               FieldByName( 'TR_FEC_APR' ).AsDateTime :=  FieldByName( 'TR_FECHA' ).AsDateTime;
               FieldByName( 'TR_STATUS' ).AsInteger := ord(stcAprobada);
          end;
     end;
end;

procedure TdmSuper.TR_MOTIVOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if StrLleno( Sender.AsString ) then
        Text := Sender.AsString + '=' + dmTablas.cdsMotivoTransfer.GetDescripcion( Sender.AsString );
end;

procedure TdmSuper.cdsCosteoTransferenciasNewRecord(DataSet: TDataSet);
begin
     with cdsCosteoTransferencias do
     begin
          FieldByName( 'AU_FECHA' ).AsDateTime := dmCliente.FechaSupervisor;
          FieldByName( 'TR_TIPO' ).AsInteger := Ord( tcOrdinarias ) ;
          FieldByName( 'TR_HORAS' ).AsFloat := FTransferUltimaHora;
          FieldByName( 'CC_CODIGO' ).AsString := VACIO;
          FieldByName( 'TR_MOTIVO' ).AsString := VACIO;
          FieldByName( 'TR_NUMERO' ).AsFloat := 0;
          FieldByName( 'TR_TEXTO' ).AsString := VACIO;
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'TR_FECHA' ).AsDateTime := dmCliente.FechaSupervisor;
          FieldByName( 'TR_GLOBAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'TR_STATUS' ).AsInteger := ord(stcPorAprobar);
          FieldByName( 'TR_TXT_APR').AsString := VACIO;
     end;
end;

procedure TdmSuper.cdsCosteoTransferenciasBeforePost(DataSet: TDataSet);
begin
     with cdsCosteoTransferencias do
     begin
          if ( FieldByName( 'TR_HORAS' ).AsFloat <= 0 ) then
          begin
               ZetaDialogo.ZError('Tranferencias', 'La duraci�n de la Transferencia debe de ser mayor a cero', 0);
               Abort;
          end;
     end;
end;

procedure TdmSuper.cdsCosteoTransferenciasAfterPost(DataSet: TDataSet);
begin
     with cdsCosteoTransferencias do
     begin
          FTransferUltimaHora := FieldByName( 'TR_HORAS' ).AsFloat;
     end;
end;

procedure TdmSuper.cdsCosteoTransferenciasAlEnviarDatos(Sender: TObject);
begin
     with cdsCosteoTransferencias do
          //GrabaTransferencias( cdsCosteoTransferencias, FALSE, ( State=dsInsert ) or CambiaCampo(FieldByName('TR_STATUS')) );
          GrabaTransferencias( cdsCosteoTransferencias, FALSE, FALSE );
end;

function TdmSuper.GrabaTransferencias( DatasetCosteo:TZetaClientDataset; const lAsignarData: Boolean; const lResetDataChange : Boolean = TRUE ): Boolean;
var
   ErrorCount: Integer;
   ResultData: OleVariant;
begin
     Result := TRUE;
     with DatasetCosteo do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
             Result := Reconciliar( ServerAsistencia.GrabaCosteoTransferencias( dmCliente.Empresa, Delta, ParametrosTransferencias.VarValues, ResultData, ErrorCount ) );

          if Result then
          begin
               DisableControls;
               try
                  if lAsignarData then
                     Data := ResultData;

                  if lResetDataChange then
                     TressShell.SetDataChange( [ enTransferencias ] );
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TdmSuper.cdsCosteoTransferenciasAlAgregar(Sender: TObject);
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpTransfer );
        FRefrescarFormaActiva := FALSE;
        cdsEmpleados.DisableControls;
        cdsCosteoTransferencias.IndexFieldNames := VACIO;

        ZBaseGridEdicion_DevEx.ShowGridEdicion( GridSuperCosteoTransferencias, TGridSuperCosteoTransferencias, TRUE );

     finally
        cdsCosteoTransferencias.IndexFieldNames := 'AU_FECHA;CB_CODIGO';
        cdsEmpleados.EnableControls;
        FRefrescarFormaActiva := TRUE;
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmSuper.cdsCosteoTransferenciasAlModificar(Sender: TObject);
var
   Pos: TBookMark;
begin
     try
        dmCliente.SetLookupEmpleado( eLookEmpTransfer );
        FRefrescarFormaActiva := FALSE;
        cdsEmpleados.DisableControls;

        ZBaseEdicion_DevEx.ShowFormaEdicion( SuperEditCosteoTransferencias, TSuperEditCosteoTransferencias );

        with cdsCosteoTransferencias do
        begin
             Pos:= GetBookMark;
             TressShell.SetDataChange( [ enTransferencias ] );
             if ( Pos <> nil ) then
             begin
                  if BookMarkValid( Pos ) then
                     GotoBookMark( Pos );
                  FreeBookMark( Pos );
             end;
        end;
     finally
        cdsEmpleados.EnableControls;
        FRefrescarFormaActiva := TRUE;
        dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TdmSuper.cdsCosteoTransferenciasAlBorrar(Sender: TObject);
begin
     with cdsCosteoTransferencias do
     begin
          if ( eStatusTransCosteo(FieldByName('TR_STATUS').AsInteger) <> stcAprobada ) then    //Solo se pueden borrar transferencias que no estan aprobadas
          begin
               if ( FieldByName('US_CODIGO').AsInteger  = dmCliente.Usuario ) then               //Solo se pueden borrar transferencia que realiz� el Supervisor activo
               begin
                    if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Transferencia ?' ) then
                    begin
                         Delete;
                         GrabaTransferencias( cdsCosteoTransferencias, FALSE, FALSE );
                    end
               end
               else
               begin
                    ZetaDialogo.ZWarning( 'Transferencias', 'Solo el Supervisor que agreg� esta Transferencia la puede borrar', 0, mbOk);
               end;
          end
          else
          begin
               ZetaDialogo.ZWarning( 'Transferencias', 'Solo se pueden borrar Transferencias que no han sido Aprobadas', 0, mbOk);
          end;
     end;
end;

{ cdsSuperCosteo }

procedure TdmSuper.cdsSuperCosteoAlAdquirirDatos(Sender: TObject);
begin
     cdsSuperCosteo.Data := ServerAsistencia.GetSuperXCentroCosto( dmCliente.Empresa  );
end;

function TdmSuper.FiltroUsuariosXCentroCosto( const sCentroCosto: string; var iUsuarioDefault: Integer;
         var sFiltro, sMensaje: string ): Boolean;
var
   sListaUsuarios, SOldFiltro: string;
   iPrimerUsuario: Integer;
   sOldFilter: Boolean;
begin
     sListaUsuarios := '-1';           // Si no hay supervisores para que el lookup no regrese ningun registro
     sMensaje := VACIO;
     with cdsSuperCosteo do
     begin
          sOldFilter := Filtered;
          sOldFiltro := Filter;
          try
             Filtered  := FALSE;
             Filter := Format( 'CB_NIVEL =  ''%s''', [sCentroCosto] );
             Filtered  := TRUE;
             Result := ( not EOF );
             if Result then
             begin
                  iPrimerUsuario := FieldByName('US_CODIGO').AsInteger;
                  while (not EOF) do
                  begin
                       sListaUsuarios := ConcatString( sListaUsuarios, FieldByName('US_CODIGO').AsString, ',' );
                       if ( iUsuarioDefault = FieldByName('US_CODIGO').AsInteger ) then
                          iPrimerUsuario := iUsuarioDefault;
                       Next;
                  end;
                  iUsuarioDefault := iPrimerUsuario;
             end
             else
             begin
                  sMensaje := Format( 'No hay Supervisores asignados a %0:s = %1:s ' +CR_LF +
                                      'Favor de seleccionar un(a) %0:s diferente', [Global.NombreCosteo, sCentroCosto] );
             end;
             sFiltro := Format( 'US_CODIGO IN ( %s )', [sListaUsuarios] );
          finally
                 Filter := sOldFiltro;
                 Filtered := sOldFilter;
          end;
     end;
end;

procedure TdmSuper.EditarGridAprobarCancelarTransferencias;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAprobarCosteoTransferencias_DevEx, TGridAprobarCosteoTransferencias_DevEx, FALSE );
end;

procedure TdmSuper.cdsGridCosteoTransferenciaAlEnviarDatos( Sender: TObject);
begin
     GrabaTransferencias( cdsGridCosteoTransferencia , FALSE );
end;

procedure TdmSuper.cdsCosteoTransferenciasBeforeInsert(DataSet: TDataSet);
begin
     FAgregandoTransferencia := TRUE;
end;


procedure TdmSuper.cdsSaldoVacacionesAlAdquirirDatos(Sender: TObject);
var
   DatosTotales : OleVariant;
begin
     with dmCliente do
     begin
          cdsSaldoVacaciones.Data  := ServerRecursos.GetSaldosVacaciones(Empresa,Empleado,DatosTotales );
          cdsSaldoVacacionesTotales.Data := DatosTotales;
     end;
end;

procedure TdmSuper.cdsSaldoVacacionesAlCrearCampos(Sender: TObject);
begin
     with cdsSaldoVacaciones do
     begin
          MaskPesos( 'VS_D_PAGO' );
          MaskHoras( 'VS_PAGO' );
          MaskHoras( 'VS_S_PAGO' );

          MaskPesos( 'VS_D_GOZO' );
          MaskHoras( 'VS_GOZO' );
          MaskHoras( 'VS_S_GOZO' );

          MaskPesos( 'VS_D_PRIMA' );
          MaskHoras( 'VS_PRIMA' );
          MaskHoras( 'VS_S_PRIMA' );
          MaskHoras( 'VS_CB_SAL' );
          MaskFecha( 'VS_FEC_VEN' );
     end;
end;

procedure TdmSuper.cdsSaldoVacacionesTotalesAlCrearCampos(Sender: TObject);
begin
     with cdsSaldoVacacionesTotales do
     begin
          MaskHoras( 'SaldoGozo' );
          MaskHoras( 'SaldoPago' );
          MaskHoras( 'SaldoPrima' );
     end;
end;

procedure TdmSuper.validacionTipoNominaChange(Sender: TField);
begin
    if Sender.AsString <> Sender.OldValue then
     begin
          with Sender do
          begin
               dmCliente.cdsTPeriodos.Locate('TP_TIPO', Sender.AsString, []);
               if not ListaIntersectaConfidencialidad(
                  dmCliente.cdsTPeriodos.FieldByName (dmCliente.cdsTPeriodos.LookupConfidenField).AsString,
                  dmCliente.Confidencialidad) then
                  begin
                       FocusControl;
                       DataBaseError( Format ('No es posible asignar el tipo de n�mina: ''%s'' ',
                             [ ObtieneElemento(lfTipoPeriodo, Sender.AsInteger) ]));
                  end;
          end;
     end;
end;

function TdmSuper.GetPermisoDiasHabiles( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := ServerAsistencia.GetPermisoDiasHabiles( dmCliente.Empresa,iEmpleado,dInicio,dFinal);
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmSuper.GetPermisoFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const Dias: integer; var rDiasRango: Double ): TDate;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result :=   ServerAsistencia.GetPermisoFechaRegreso( dmCliente.Empresa, iEmpleado, dInicio, Dias, rDiasRango );
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TdmSuper.cdsPlanCapacitacionAlAdquirirDatos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsPlanCapacitacion.Data := ServerRecursos.GetPlanCapacitacion( Empresa, Empleado );
end;

procedure TdmSuper.cdsPlanCapacitacionAlCrearCampos(Sender: TObject);
begin
      with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          cdsCatPerfiles.Conectar;
          //cdsMatPerfilComps.Conectar;
     end;                                                                                                                                   
     with cdsPlanCapacitacion do
     begin
          MaskFecha('EC_FECHA');
          CreateSimpleLookup(dmCatalogos.cdsCompetencias ,'TB_ELEMENT','CC_CODIGO');
          CreateSimpleLookup(dmCatalogos.cdsCatPerfiles  ,'CP_ELEMENT','CP_CODIGO');

          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmSuper.cdsHisCompetenAlAdquirirDatos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCliente do
          cdsHisCompeten.Data := ServerRecursos.GetCompetenciasEmp( Empresa, Empleado );
end;

procedure TdmSuper.cdsHisCompetenAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsCompetencias.Conectar;
     with cdsHisCompeten do
     begin
          MaskFecha('ECC_FECHA');
          CreateSimpleLookup(dmCatalogos.cdsCompetencias ,'TB_ELEMENT','CC_CODIGO');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmSuper.cdsHisCompetenAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsHisCompeten do
     begin
          PostData;
          if ( Changecount > 0 )then
          begin
               DisableControls;
               try
                  if Reconcile( ServerRecursos.GrabaCompetenciasEmp(dmCliente.Empresa, Delta, ErrorCount ) )then
                  begin
                       cdsPlanCapacitacion.Refrescar;
                  end;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TdmSuper.cdsHisCompetenAfterDelete(DataSet: TDataSet);
begin
     cdsHisCompeten.Enviar;
end;

procedure TdmSuper.cdsHisCompetenNewRecord(DataSet: TDataSet);
begin
     with cdsHisCompeten do
     begin
          with dmCliente do
          begin
               FieldByName('CB_CODIGO').AsInteger := Empleado;
               FieldByName('ECC_FECHA').AsDateTime := FechaDefault;
               FieldByName('US_CODIGO').AsInteger := Usuario;
               FieldByName('ECC_COMENT').AsString := VACIO;
          end;
     end;
end;

procedure TdmSuper.cdsHisCompetenBeforePost(DataSet: TDataSet);
begin
     with cdsHisCompeten do
     begin
          if StrVacio( FieldByName('CC_CODIGO').AsString ) then
          begin
               DataBaseError('Competencia No Puede Quedar Vac�o');
          end;

           if FieldByName('NC_NIVEL').AsInteger <= 0  then
          begin
               DataBaseError('Nivel No Puede Quedar Vac�o');
          end;

          FieldByName('RC_FEC_INI').AsDateTime := dmCatalogos.GetUltimaRevisionCompetencia; 
     end;
end;

procedure TdmSuper.cdsHisCompetenAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisCompetencia_DevEx , TEditHisCompetencia_DevEx );

end;

procedure TdmSuper.cdsHisCompEvaluaAlAdquirirDatos(Sender: TObject);
var
   iEmpleado:Integer;
begin
     dmSistema.cdsUsuarios.Conectar;
     iEmpleado := dmCliente.Empleado;
     if FEvaluarMatriz then
        iEmpleado := -1;
     with dmCliente do
          cdsHisCompEvalua.Data := ServerRecursos.GetEvaluaCompEmp( Empresa, iEmpleado );
end;

procedure TdmSuper.cdsHisCompEvaluaAlCrearCampos(Sender: TObject);
begin
     dmCatalogos.cdsCompetencias.Conectar;
     with cdsHisCompEvalua do
     begin
          MaskFecha('EC_FECHA');
          CreateSimpleLookup(dmCatalogos.cdsCompetencias ,'TB_ELEMENT','CC_CODIGO');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmSuper.cdsHisCompEvaluaAlEnviarDatos(Sender: TObject);
var
   ErrorCount,iNivel: Integer;
   sCodigo:string;
begin
     ErrorCount := 0;
     with cdsHisCompEvalua do
     begin
          PostData;
          sCodigo := FieldByName('CC_CODIGO').AsString;
          iNivel  := FieldByName('NC_NIVEL').AsInteger;
          if ( Changecount > 0 )then
          begin
               if Reconcile( ServerRecursos.GrabaEvaluaCompEmp(dmCliente.Empresa, Delta, ErrorCount ) )then
               begin
                    try
                       DisableControls;
                       cdsHisCompEvalua.Refrescar;
                       Locate('CC_CODIGO;NC_NIVEL',VarArrayOf([sCodigo,iNivel]),[]);
                       cdsPlanCapacitacion.SetDataChange;
                       cdsMatrizHabilidades.Refrescar;
                    finally
                           EnableControls;
                    end;
               end;
          end;
     end;
end;

procedure TdmSuper.cdsHisCompEvaluaAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisEvaluaComp_DevEx, TEditHisEvaluaComp_DevEx );
end;

procedure TdmSuper.cdsHisCompEvaluaReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError:string;

   function MensajeCompe:Boolean;
   begin
        Result :=  Pos( 'C_REV_COMP',E.Message) > 0;
   end;

begin
     if ( dmCatalogos.GetUltimaRevisionCompetencia = NullDateTime  ) and ( UpdateKind = ukInsert ) and ( MensajeCompe ) then
     begin
          with DataSet do
          begin
               sError := Format('No se Puede Agregar Competencia %s ; No Tiene Capturada al menos una Revisi�n ',[ FieldByName('CC_CODIGO').AsString ]);
          end;
          ZetaDialogo.zError( 'Error al ' + ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( UpdateKind ) ), sError, 0 ); // UpdateKindStr[ UpdateKind ], sError, 0 );
          case UpdateKind of
               ukInsert: Action := raCancel;
          else
              Action := raAbort;
          end;
     end
     else
         Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );

end;

procedure TdmSuper.cdsHisCompEvaluaBeforePost(DataSet: TDataSet);
begin
     with cdsHisCompEvalua do
     begin
          if StrVacio( FieldByName('CC_CODIGO').AsString ) then
          begin
               DataBaseError('Competencia No Puede Quedar Vac�o');
          end;

          if FieldByName('NC_NIVEL').AsInteger <= 0  then
          begin
               DataBaseError('Nivel No Puede Quedar Vac�o');
          end;

          FieldByName('RC_FEC_INI').AsDateTime := dmCatalogos.GetUltimaRevisionCompetencia;
     end;
end;

procedure TdmSuper.cdsHisCompEvaluaAfterDelete(DataSet: TDataSet);
begin
     cdsHisCompEvalua.Enviar; 
end;

procedure TdmSuper.cdsHisCompEvaluaNewRecord(DataSet: TDataSet);
begin
     with cdsHisCompEvalua do
     begin
          with dmCliente do
          begin
               FieldByName('CB_CODIGO').AsInteger := Empleado;
               FieldByName('EC_FECHA').AsDateTime := Now;
               FieldByName('US_CODIGO').AsInteger := Usuario;
               FieldByName('EC_COMENT').AsString := VACIO;
          end;
     end;
end;

procedure TdmSuper.cdsMatrizCursosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsMatrizCursos.Data := ServerRecursos.GetMatrizCursos( Empresa, FiltroMatCursos.VarValues );
     end;
end;

procedure TdmSuper.cdsMatrizCursosAlCrearCampos(Sender: TObject);
begin
     with dmTablas do
     begin
          cdsTipoCursos.Conectar;
          cdsClasifiCurso.Conectar;
     end;
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsPuestos.Conectar;
     end;
end;

procedure TdmSuper.cdsMatrizHabilidadesAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
          cdsMatrizHabilidades.Data := ServerRecursos.GetMatrizHabilidades( Empresa, FiltroMatHabilidades.VarValues );
end;

procedure TdmSuper.cdsMatrizHabilidadesAlCrearCampos(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          cdsCatPerfiles.Conectar;
          with cdsMatrizHabilidades do
          begin
               CreateSimpleLookup(cdsCompetencias ,'CC_ELEMENT','CC_CODIGO');
               CreateSimpleLookup(cdsCatPerfiles ,'CP_ELEMENT','CP_CODIGO');
          end;
     end;
end;

procedure TdmSuper.cdsEvaluacionSelNewRecord(DataSet: TDataSet);
begin
     if not FAgregandoMatriz then
     begin
          DataSet.Cancel;
          DataSet.Edit;
     end;
end;

procedure TdmSuper.cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

function TdmSuper.GetHorarioTurno( const iEmpleado: Integer; const dFecha: TDate; iTipo: Integer ): String;
var
   Valor: Variant;
begin
     Valor := ServerAsistencia.GetHorarioTurno( dmCliente.Empresa, VarArrayOf( [ iEmpleado, dFecha, iTipo ] ) );

    // with Result do
    // begin
          Result := Valor[0];
   //  end;
end;


procedure TdmSuper.cdsEvaluacionDiariaAlAdquirirDatos(Sender: TObject);
var
   Parametros: TZetaParams;
begin
     {$ifdef COMMSCOPE}
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
	     Parametros.AddDate( 'Fecha', dmSuper.FechaGridAuto );
             cdsEvaluacionDiaria.Data := ServerSuper.GetEvaluacionDiaria( Empresa, Parametros.VarValues );
        end;
     finally
            Parametros.Free;
     end;
     {$else}
     inherited;
     {$endif}
end;

procedure TdmSuper.cdsEvaluacionDiariaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     {$ifdef COMMSCOPE}
     ErrorCount := 0;
     with cdsEvaluacionDiaria do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerSuper.GrabaEvaluacionDiaria( dmCliente.Empresa, Delta, ErrorCount ) );
               //if ( ErrorCount = 0 ) then
                  //TressShell.SetDataChange( [ enNomina ] );
          end;
     end;
     {$else}
     inherited;
     {$endif}

end;

procedure TdmSuper.cdsEvaluacionDiariaAlCrearCampos(Sender: TObject);
begin
     {$ifdef COMMSCOPE}
     dmSistema.cdsUsuarios.Conectar;
     with cdsEvaluacionDiaria do
     begin
          MaskTasa( 'ED_EVALUA' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
     {$else}
     inherited;
     {$endif}
end;

procedure TdmSuper.cdsEvaluacionDiariaBeforePost(DataSet: TDataSet);
var
   rTotal: currency;
begin
     {$ifdef COMMSCOPE}
     rTotal := 0;
     with cdsEvaluacionDiaria do
     begin
          DisableControls;
          try
             if FieldByName( 'ED_ASISTEN' ).AsFloat > 0 then
                rTotal := rTotal + 1;
             if FieldByName( 'ED_CALIDAD' ).AsFloat > 0 then
                rTotal := rTotal + 1;
             if FieldByName( 'ED_PRODUCT' ).AsFloat > 0 then
                rTotal := rTotal + 1;
             if FieldByName( 'ED_TRABAJO' ).AsFloat > 0 then
                rTotal := rTotal + 1;
             if FieldByName( 'ED_SEGURID' ).AsFloat > 0 then
                rTotal := rTotal + 1;
             FieldByName( 'ED_EVALUA' ).AsFloat := ( rTotal / 5 );
             FieldByName( 'ED_EVALUA' ).AsFloat := ( FieldByName( 'ED_EVALUA' ).AsFloat * 100 );
             FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          finally
                 EnableControls;
          end;
     end;
     {$else}
     inherited;
     {$endif}
end;



end.



