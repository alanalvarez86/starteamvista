unit FGridBreaks;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion, Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, ComCtrls, ZetaKeyLookup, ZetaNumero, ZetaHora, Mask,
  ZetaFecha, ZetaSmartLists;

type
  TRegistroBreak = class(TBaseGridEdicion)
    Panel1: TPanel;
    Fechalbl: TLabel;
    Horalbl: TLabel;
    Duracionlbl: TLabel;
    Arealbl: TLabel;
    ZFecha: TZetaFecha;
    ZHora: TZetaHora;
    ZDuracion: TZetaNumero;
    AreaKL: TZetaKeyLookup;
    Label1: TLabel;
    HoraFinal: TZetaHora;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure AreaKLExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZHoraExit(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  RegistroBreak: TRegistroBreak;

implementation

uses DLabor, DCliente,ZetaDialogo, DGlobal, ZetaBuscaEmpleado, ZetaCommonTools, ZetaLaborTools,
     ZGlobalTress,
     ZetaCommonClasses;

{$R *.DFM}

{ TRegistroBreak }

procedure TRegistroBreak.FormShow(Sender: TObject);
begin
     inherited;
     with dmLabor do
     begin
          ZFecha.Valor := FechaArea;
          ZHora.Valor := HoraArea;
          HoraFinal.Valor := HoraArea;
     end;
     Self.ActiveControl := ZFecha;
     Arealbl.Caption := GetLaborLabel( K_GLOBAL_LABOR_AREA ) + ':';
end;

procedure TRegistroBreak.FormCreate(Sender: TObject);
begin
     inherited;
     AreaKL.Filtro := MIS_AREAS;
     AreaKL.LookupDataset := dmLabor.cdsArea;
     HelpContext := H9508_Registro_manual_de_Breaks;
end;

procedure TRegistroBreak.Connect;
begin
     with dmLabor do
     begin
          cdsArea.Conectar;
          //cdsEmpBreaks.Refrescar;
          DataSource.DataSet:= cdsEmpBreaks;
     end;
end;

procedure TRegistroBreak.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ) then
     begin
          with dmLabor.cdsEmpBreaks do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TRegistroBreak.Buscar;
begin
  inherited;
  with ZetaDBGrid.SelectedField do
  begin
       if ( FieldName = 'CB_CODIGO' ) then
          BuscaEmpleado;
  end;
end;

procedure TRegistroBreak.AreaKLExit(Sender: TObject);
begin
     if not StrVacio( AreaKL.Llave ) then
     begin
          with dmLabor do
          begin
               Area := AreaKL.Llave;
               FechaArea := ZFecha.Valor;
               HoraArea := ZHora.Valor;
               cdsEmpBreaks.Refrescar;
               HabilitaControles;
          end;
     end;
end;

procedure TRegistroBreak.EscribirCambios;
begin
     if ( ZDuracion.Valor = 0 ) then
     begin
          ZDuracion.SetFocus;
          DatabaseError('No se puede registrar un break sin duración');
     end
     else if strVacio( ZHora.Valor ) then
          begin
               ZHora.SetFocus;
               DatabaseError('No se puede registrar un break sin un hora determinada');
          end
          else
          begin
               with dmLabor do
               begin
                    Area := AreaKL.Llave;
                    FechaArea := ZFecha.Valor;
                    HoraArea := HoraFinal.Valor;
                    Duracion := ZDuracion.ValorEntero;
               end;
               inherited;
          end;

end;

procedure TRegistroBreak.ZHoraExit(Sender: TObject);
var
   sInicio, sFinal : String;

begin
     inherited;
     sInicio := ZHora.Valor;
     sFinal  := HoraFinal.Valor;
     if( aMinutos( sInicio ) > aMinutos( sFinal ) )then
     begin
          zWarning( Caption,'La Duración del Break no puede ser negativa ',0,mbOK);
          ZHora.SetFocus;  
     end
     else
     if StrLleno( sInicio ) and StrLleno( sFinal ) then
        ZDuracion.Valor := aMinutos( sFinal ) - aMinutos( sInicio );
end;

procedure TRegistroBreak.OKClick(Sender: TObject);
var
   lOK: boolean;
   dFechaCorte: Tdate;
begin
     lOK := not Global.GetGlobalBooleano( K_GLOBAL_LABOR_VALIDA_FECHACORTE );
     if not lOK then
     begin
          dFechaCorte := Global.GetGlobalDate( K_GLOBAL_LABOR_FECHACORTE );
          lOK := ( zFecha.Valor > dFechaCorte );
          if not lOK then
             DataBaseError( Format( 'ˇ No se Permite Agregar Información de Labor !' + CR_LF +
                                    'La Fecha de Corte es : %s', [ FechaCorta( dFechaCorte ) ] ) )
     end;
     if lOK then
        inherited;
end;

end.
