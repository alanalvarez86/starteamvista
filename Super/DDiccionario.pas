unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  DBaseTressDiccionario,
  DBaseDiccionario,
  ZetaCommonLists,
  ZetaClientDataSet ;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
  private
    { Private declarations }
  protected

  public
    { Public declarations }
    procedure SetLookupNames; override;
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation

uses dTablas,
     ZReportConst,
     ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

{ TdmDiccionario }

procedure TdmDiccionario.GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);
begin
     //En el m�dulo de supervisores solo se puede consultar la clasificacion de Supervisores.
     {$ifdef RDD}
     with cdsClasificaciones do
     begin
          Conectar;
          if Locate('RC_CODIGO', crSupervisor, [] ) then
             oLista.AddObject( FieldByName('RC_NOMBRE').AsString, TObject( FieldByName('RC_CODIGO').AsInteger ) )
          else
              ZError('Supervisores', 'La clasificaci�n de Supervisores no existe' +CR_LF+
                                     'No es posible mostrar los reportes', 0 );
     end;
     {$else}
     AgregaClasifi( oLista, crSupervisor );
     {$endif}
end;

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     dmTablas.SetLookupNames;
end;

end.
