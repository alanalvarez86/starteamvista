object ConsultaEmpleados: TConsultaEmpleados
  Left = 745
  Top = 242
  Hint = 'Mostrar Foto'
  BorderIcons = [biSystemMenu]
  Caption = 'Datos del Empleado'
  ClientHeight = 308
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  DesignSize = (
    630
    308)
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TcxPageControl
    Left = 0
    Top = 33
    Width = 417
    Height = 275
    Hint = 'Datos del Empleado'
    Align = alLeft
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    Properties.ActivePage = TabGenerales
    Properties.CustomButtons.Buttons = <>
    Properties.MultiLine = True
    Properties.TabHeight = 32
    Properties.TabPosition = tpBottom
    OnChange = PageControl1Change
    ClientRectBottom = 234
    ClientRectLeft = 2
    ClientRectRight = 415
    ClientRectTop = 2
    object TabGenerales: TcxTabSheet
      Hint = 'Datos Generales'
      Caption = 'Datos Generales'
      object Label2: TLabel
        Left = 21
        Top = 6
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Apellido Paterno:'
        FocusControl = CB_APE_PAT
      end
      object Label3: TLabel
        Left = 21
        Top = 30
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Apellido Materno:'
        FocusControl = CB_APE_MAT
      end
      object Label4: TLabel
        Left = 21
        Top = 54
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nombres:'
        FocusControl = CB_NOMBRES
      end
      object Label6: TLabel
        Left = 21
        Top = 78
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Turno:'
      end
      object Label7: TLabel
        Left = 21
        Top = 102
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Puesto:'
      end
      object Label8: TLabel
        Left = 21
        Top = 125
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Clasificaci'#243'n:'
      end
      object Label9: TLabel
        Left = 21
        Top = 149
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Contrato:'
      end
      object Label10: TLabel
        Left = 21
        Top = 173
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Fecha de Ingreso:'
      end
      object Area_Lbl: TLabel
        Left = 91
        Top = 197
        Width = 16
        Height = 13
        Alignment = taRightJustify
        Caption = '%s:'
      end
      object CB_APE_PAT: TDBEdit
        Left = 113
        Top = 2
        Width = 175
        Height = 21
        Hint = 'Apellido Paterno del Empleado'
        DataField = 'CB_APE_PAT'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 0
      end
      object CB_APE_MAT: TDBEdit
        Left = 113
        Top = 26
        Width = 175
        Height = 21
        Hint = 'Apellido Materno del Empleado'
        DataField = 'CB_APE_MAT'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 1
      end
      object CB_NOMBRES: TDBEdit
        Left = 113
        Top = 50
        Width = 175
        Height = 21
        Hint = 'Nombre(s) del Empleado'
        DataField = 'CB_NOMBRES'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 2
      end
      object TU_DESCRIP: TDBEdit
        Left = 113
        Top = 74
        Width = 175
        Height = 21
        Hint = 'Turno del Empleado'
        DataField = 'TU_DESCRIP'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 3
      end
      object PU_DESCRIP: TDBEdit
        Left = 113
        Top = 98
        Width = 175
        Height = 21
        Hint = 'Puesto del Empleado'
        DataField = 'PU_DESCRIP'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 4
      end
      object TB_ELEMENT: TDBEdit
        Left = 113
        Top = 121
        Width = 175
        Height = 21
        Hint = 'Clasificaci'#243'n del Empleado'
        DataField = 'TB_ELEMENT'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 5
      end
      object CONTRATO: TDBEdit
        Left = 113
        Top = 145
        Width = 175
        Height = 21
        Hint = 'Tipo de Contrato'
        DataField = 'CONTRATO'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 6
      end
      object CB_FEC_ING: TDBEdit
        Left = 113
        Top = 169
        Width = 175
        Height = 21
        Hint = 'Fecha de Ingreso'
        DataField = 'CB_FEC_ING'
        DataSource = DataSource
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        TabOrder = 7
      end
      object CB_AREA: TDBEdit
        Left = 113
        Top = 193
        Width = 175
        Height = 21
        DataField = 'TB_ELEMENT2'
        DataSource = DataSource
        TabOrder = 8
      end
      object bbMostrarCalendario_DevEx: TcxButton
        Left = 294
        Top = 75
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        LookAndFeel.SkinName = 'TressMorado2013'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnClick = bbMostrarCalendarioClick
      end
    end
    object TabNiveles: TcxTabSheet
      Hint = 'Niveles de Organigrama'
      Caption = 'Niveles'
      object CB_NIVEL1lbl: TLabel
        Left = 80
        Top = 2
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 80
        Top = 21
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 80
        Top = 40
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 80
        Top = 59
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 80
        Top = 79
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL5: TZetaDBTextBox
        Left = 115
        Top = 77
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL5'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL4: TZetaDBTextBox
        Left = 115
        Top = 57
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL4'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL3: TZetaDBTextBox
        Left = 115
        Top = 38
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL3'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL2: TZetaDBTextBox
        Left = 115
        Top = 19
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL2'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL1: TZetaDBTextBox
        Left = 115
        Top = 0
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL1'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 80
        Top = 98
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL6: TZetaDBTextBox
        Left = 115
        Top = 96
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL6'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 80
        Top = 117
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL7: TZetaDBTextBox
        Left = 115
        Top = 115
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL7'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 80
        Top = 137
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL8: TZetaDBTextBox
        Left = 115
        Top = 135
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL8'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 80
        Top = 156
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL9: TZetaDBTextBox
        Left = 115
        Top = 154
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL9'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL1: TZetaTextBox
        Left = 166
        Top = 0
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL1'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL2: TZetaTextBox
        Left = 166
        Top = 19
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL2'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL3: TZetaTextBox
        Left = 166
        Top = 38
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL3'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL4: TZetaTextBox
        Left = 166
        Top = 57
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL4'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL5: TZetaTextBox
        Left = 166
        Top = 77
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL5'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL6: TZetaTextBox
        Left = 166
        Top = 96
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL6'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL7: TZetaTextBox
        Left = 166
        Top = 115
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL7'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL8: TZetaTextBox
        Left = 166
        Top = 135
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL8'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL9: TZetaTextBox
        Left = 166
        Top = 154
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL9'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL10lbl: TLabel
        Left = 74
        Top = 175
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL10: TZetaDBTextBox
        Left = 115
        Top = 173
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL10'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL10: TZetaTextBox
        Left = 166
        Top = 173
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 74
        Top = 194
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL11: TZetaDBTextBox
        Left = 115
        Top = 192
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL11'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL11: TZetaTextBox
        Left = 166
        Top = 192
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 74
        Top = 213
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL12: TZetaDBTextBox
        Left = 115
        Top = 211
        Width = 48
        Height = 17
        AutoSize = False
        Caption = 'CB_NIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NIVEL12'
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ZNIVEL12: TZetaTextBox
        Left = 166
        Top = 211
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
    end
    object TabKardex: TcxTabSheet
      Caption = 'Kardex de %s'
      ImageIndex = 2
      object ZetaCXGrid2: TZetaCXGrid
        Left = 0
        Top = 0
        Width = 413
        Height = 232
        Hint = ''
        Align = alClient
        TabOrder = 0
        object ZetaCXGrid2DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
        end
        object ZetaCXGrid2DBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FilterBox.Visible = fvNever
          DataController.DataSource = DSKarArea
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.OnSortingChanged = ZetaCXGrid2DBTableView2DataControllerSortingChanged
          OptionsBehavior.FocusCellOnTab = True
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.UnselectFocusedRecordOnExit = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          OnColumnHeaderClick = ZetaCXGrid2DBTableView2ColumnHeaderClick
          object ZetaCXGrid2DBTableView2KA_FECHA: TcxGridDBColumn
            Caption = 'Fecha'
            DataBinding.FieldName = 'KA_FECHA'
            Width = 75
          end
          object ZetaCXGrid2DBTableView2KA_HORA: TcxGridDBColumn
            Caption = 'Hora'
            DataBinding.FieldName = 'KA_HORA'
            Width = 45
          end
          object ZetaCXGrid2DBTableView2CB_AREA: TcxGridDBColumn
            Caption = '%s'
            DataBinding.FieldName = 'CB_AREA'
            Width = 70
          end
          object ZetaCXGrid2DBTableView2TB_ELEMENT: TcxGridDBColumn
            Caption = 'Descripci'#243'n'
            DataBinding.FieldName = 'TB_ELEMENT'
            Width = 130
          end
          object ZetaCXGrid2DBTableView2KA_FEC_MOV: TcxGridDBColumn
            Caption = 'Fecha Cambio'
            DataBinding.FieldName = 'KA_FEC_MOV'
            Width = 74
          end
          object ZetaCXGrid2DBTableView2KA_HOR_MOV: TcxGridDBColumn
            Caption = 'Hora Cambio'
            DataBinding.FieldName = 'KA_HOR_MOV'
            Width = 65
          end
          object ZetaCXGrid2DBTableView2US_CODIGO: TcxGridDBColumn
            Caption = 'Usuario'
            DataBinding.FieldName = 'US_CODIGO'
            Width = 45
          end
        end
        object ZetaCXGrid2Level1: TcxGridLevel
          GridView = ZetaCXGrid2DBTableView2
        end
      end
    end
    object Certificaciones: TcxTabSheet
      Caption = 'Certificaciones'
      ImageIndex = 4
      object ZetaCXGrid1: TZetaCXGrid
        Left = 0
        Top = 0
        Width = 413
        Height = 232
        Hint = ''
        Align = alClient
        TabOrder = 0
        object ZetaCXGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
        end
        object ZetaCXGrid1DBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsKarCertific
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.OnSortingChanged = ZetaCXGrid1DBTableView2DataControllerSortingChanged
          OptionsBehavior.FocusCellOnTab = True
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.UnselectFocusedRecordOnExit = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          OnColumnHeaderClick = ZetaCXGrid1DBTableView2ColumnHeaderClick
          object ZetaCXGrid1DBTableView2KI_FEC_CER: TcxGridDBColumn
            Caption = 'Fecha'
            DataBinding.FieldName = 'KI_FEC_CER'
          end
          object ZetaCXGrid1DBTableView2CI_CODIGO: TcxGridDBColumn
            Caption = 'C'#243'digo'
            DataBinding.FieldName = 'CI_CODIGO'
            Width = 55
          end
          object ZetaCXGrid1DBTableView2CI_NOMBRE: TcxGridDBColumn
            Caption = 'Descripci'#243'n'
            DataBinding.FieldName = 'CI_NOMBRE'
            Width = 171
          end
          object ZetaCXGrid1DBTableView2KI_APROBO: TcxGridDBColumn
            Caption = 'Aprob'#243
            DataBinding.FieldName = 'KI_APROBO'
            Width = 41
          end
          object ZetaCXGrid1DBTableView2VENCIMIENTO: TcxGridDBColumn
            Caption = 'Vencimiento'
            DataBinding.FieldName = 'VENCIMIENTO'
            Options.Sorting = False
          end
        end
        object ZetaCXGrid1Level1: TcxGridLevel
          GridView = ZetaCXGrid1DBTableView2
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 630
    Height = 33
    Align = alTop
    TabOrder = 1
    DesignSize = (
      630
      33)
    object PRETTYNAME: TDBText
      Left = 250
      Top = 9
      Width = 72
      Height = 14
      Hint = 'Pretty Name del Empleado'
      AutoSize = True
      DataField = 'PRETTYNAME'
      DataSource = DataSource
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
    end
    object PaEmpLbl: TLabel
      Left = 3
      Top = 10
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
    end
    object PanelBotones: TPanel
      Left = 599
      Top = 1
      Width = 30
      Height = 31
      Align = alRight
      TabOrder = 0
    end
    object PanelEmpleadoActivo: TPanel
      Left = 534
      Top = 1
      Width = 65
      Height = 31
      Hint = 'Status Actual del Empleado'
      Align = alRight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object Empleado: TStateComboBox
      Left = 46
      Top = 6
      Width = 80
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Ctl3D = False
      DropDownCount = 0
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 2
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      EsconderVacios = False
      LlaveNumerica = True
      MaxItems = 10
      Offset = 0
      OnLookUp = EmpleadoLookUp
    end
    object CB_CODIGO: TDBEdit
      Left = 46
      Top = 6
      Width = 83
      Height = 21
      Hint = 'Apellido Paterno del Empleado'
      DataField = 'CB_CODIGO'
      DataSource = DataSource
      MaxLength = 9
      ParentShowHint = False
      ReadOnly = True
      ShowHint = False
      TabOrder = 3
      Visible = False
    end
    object EmpleadoBtn_DevEx: TcxButton
      Left = 134
      Top = 6
      Width = 21
      Height = 21
      Hint = 'Busca Empleado'
      LookAndFeel.SkinName = 'TressMorado2013'
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCAC6
        CBFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFB5AEB5FFFFFFFFFFFDFDFDFF9F97A0FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFBAB4BBFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF988F99FFC0BAC0FFD2CED2FFBEB8BFFFEFEDEFFFFFFFFFFFD3D0
        D4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFA8A1A9FFFAF9FAFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE9E7E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8F8590FFF4F3F4FFF0EF
        F1FFAFA9B0FF908791FFAFA9B0FFFBFBFBFFE9E7E9FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFFA8A1A9FFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFF
        FFFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFB7B0B7FF8B818CFF8B81
        8CFF8B818CFFC5C0C6FFFFFFFFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFFBFB
        FBFFE9E7E9FF8B818CFF8B818CFF968D97FFEDEBEDFFF8F7F8FF908791FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFF8F7F8FFDCD9DDFFFAF9FAFFFDFD
        FDFFB5AEB5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFAEA7AEFFE7E5
        E8FFF0EFF1FFE0DDE0FFAAA3ABFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = EmpleadoBtn_DevExClick
    end
    object ImprimirFormaBtn_DevEx: TcxButton
      Left = 602
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Busca Empleado'
      Anchors = [akTop, akRight]
      LookAndFeel.SkinName = 'TressMorado2013'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        2000000000000009000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDCB38DFFE7CA
        AFFFE7CAAFFFE7CAAFFFE7CAAFFFE7CAAFFFE7CAAFFFDFB995FFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEFDCCBFFFFFF
        FFFFF9F3EDFFF9F3EDFFE5C6A9FFFFFFFFFFFFFFFFFFF5E8DDFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF0DFCFFFFFFF
        FFFFD9AB81FFF1E0D1FFDDB58FFFFEFEFDFFFFFFFFFFF5EADFFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF0DFCFFFFFFF
        FFFFD8AA7FFFF0DFCFFFDDB58FFFE4C5A7FFF8F0E9FFF5EADFFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFE4C5A7FFF8EFE7FFFAF4EFFFEDD8C5FFF0DFCFFFFFFF
        FFFFD8AA7FFFF6EBE1FFECD6C1FFE2BF9FFFF6EBE1FFF5EADFFFE1BD9BFFFAF4
        EFFFF9F2EBFFE7CBB1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFFFFFFF0DFCFFFF0DFCFFFFFFF
        FFFFD8AA7FFFFAF4EFFFFFFFFFFFF9F3EDFFFFFFFFFFF5EADFFFE2BF9FFFF6EB
        E1FFECD6C1FFFDFBF9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFF0DFCFFFEFDCCBFFFFFF
        FFFFF1E0D1FFFEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFE2BF9FFFEEDB
        C9FFDAAE85FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFF6EBE1FFDBB189FFE7CA
        AFFFE7CAAFFFE7CAAFFFE7CAAFFFE7CAAFFFE7CAAFFFDEB691FFE7CAAFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F2EBFFF5EA
        DFFFF5EADFFFF5EADFFFF5EADFFFF5EADFFFF5EADFFFF6ECE3FFFEFEFDFFFFFF
        FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFFEFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFF9F3EDFFF5EA
        DFFFF5EADFFFF5EADFFFF5EADFFFF5EADFFFF5EADFFFFDFAF7FFFFFFFFFFFFFF
        FFFFFFFFFFFFF5E8DDFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFBA97FFE2BF9FFFE2BF9FFFD9AB81FFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDCB28BFFE2BF9FFFE2BF
        9FFFE1BD9BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = ImprimirFormaBtnClick
    end
    object EmpleadoPrimero_DevEx: TcxButton
      Left = 162
      Top = 6
      Width = 21
      Height = 21
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00002521263F38333B5F00000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000202
        0203726A81D96A6275C300000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000000A0A0B107269
        7DCA837B99FF5A5362A200000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000100F111B766E84D7857C
        9AFF8C839DFF554E5C9900000000000000000000000000000000000000000000
        0000000000000000000000000000000000001413172579718AE2887F9BFF8C82
        9DFF8A819CFF564F5D9B00000000000000000000000000000000000000000000
        0000000000000000000000000000201D21357C758FEC8A809CFF8B819CFF8B81
        9CFF8A819CFF564F5D9B00000000000000000000000000000000000000000000
        000000000000000000003F3A436A827998FF8B829DFF8B819CFF8B819CFF8B81
        9CFF8A819CFF564F5D9B00000000000000000000000000000000000000000000
        00000000000000000000403A4368827997FF8B819DFF8B819CFF8B819CFF8B81
        9CFF8A819CFF564F5D9B00000000000000000000000000000000000000000000
        00000000000000000000000000001F1C20327A728EEB89809CFF8B819CFF8B81
        9CFF8A819CFF564F5D9B00000000000000000000000000000000000000000000
        000000000000000000000000000000000000131115237A718AE1897F9CFF8B81
        9CFF8A819CFF564F5D9B00000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000F0E1019766D83D6877E
        9BFF8B829DFF554E5C9900000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000000908090E7169
        7DC7857C9BFF5A5361A200000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000101
        0101746B82D76A6073C300000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00002521263D38323A5D00000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      TabOrder = 6
      OnClick = EmpleadoPrimeroClick
    end
    object EmpleadoAnterior_DevEx: TcxButton
      Left = 182
      Top = 6
      Width = 21
      Height = 21
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000001E1B1D30625B
        70B760596BAD665E74C6615A6DB6242226380000000000000000000000000000
        00000000000000000000000000000000000000000000030303056F667AC88980
        9DFF897E9AFF7D7392FF66606C9B0F0E0F150000000000000000000000000000
        000000000000000000000000000000000000030303056E657AC8867E9DFF897F
        9AFF7C7391FF4A454E7100000000000000000000000000000000000000000000
        0000000000000000000000000000030303056F667AC8857E9DFF897F9AFF7D73
        92FF48434C6E0000000000000000000000000000000000000000000000000000
        00000000000000000000060506086B6176CA857F9DFF897F99FF7D7391FF4844
        4C6E000000000000000000000000000000000000000000000000000000000000
        0000000000000B0B0B0F776F85D5837A99FF897F9AFF7E7593FF4A454D6F0000
        0000000000000000000000000000000000000000000000000000000000000000
        000037333A587F768FEC827895FF8C829CFF877D98FF3330354B000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000037333A587F768FEC827895FF8B819CFF8B829EFF2724273D000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000B0B0B0F756D82D5807694FF8B819DFF817998FF3A343A600000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000B0B0B0F746C81D5807693FF8C829EFF837C9CFF3A34
        3B60000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000B0B0B0F746C81D5807693FF8C829EFF847C
        9CFF3A343B600000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000B0B0B0F746D82D5807693FF8B82
        9EFF837C9CFF3C363D6300000000000000000000000000000000000000000000
        000000000000000000000000000000000000000000000B0B0B0F756D83D4867B
        97FF8A819DFF847C9CFF564D568D0807080D0000000000000000000000000000
        000000000000000000000000000000000000000000000000000029272B3C6159
        6FBB5E5768AB6B647CCA5E576AAD1C191C2D0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      TabOrder = 7
      OnClick = EmpleadoAnteriorClick
    end
    object EmpleadoSiguiente_DevEx: TcxButton
      Left = 202
      Top = 6
      Width = 21
      Height = 21
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00001B191B285A5467A96B647ACC5D5768AA635C71BC29262A40000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000006060609544E59887F7796FF897F9BFF887E9AFF776F85D80C0B0C120000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000037323B5D857C99FF8C829DFF837A99FF766D85D80C0B
        0C12000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000003430385A857B98FF8D829CFF837A98FF766D
        84D80C0B0C120000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000322E3759857C99FF8C829CFF837A
        98FF766C85D80C0B0C1200000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000332F3859857B98FF8C82
        9CFF837A98FF776D85D80C0B0C12000000000000000000000000000000000000
        000000000000000000000000000000000000000000000000000024222639887E
        9AFF8C819CFF857C9AFF7D738DED342F36560000000000000000000000000000
        0000000000000000000000000000000000000000000000000000322E344D857C
        99FF8C839DFF7F7593FF7E758FE938353B530000000000000000000000000000
        0000000000000000000000000000000000000000000047434B70807796FF8B80
        9BFF827A98FF766E84D40B0A0B0E000000000000000000000000000000000000
        00000000000000000000000000000000000047424B707F7594FF8A7F9AFF867E
        9DFF6A6075C90605060800000000000000000000000000000000000000000000
        000000000000000000000000000047434B707F7594FF8A7F9AFF867D9DFF6E66
        7AC8030303050000000000000000000000000000000000000000000000000000
        0000000000000000000048444C727F7594FF897F9AFF857E9DFF6F657AC80303
        0305000000000000000000000000000000000000000000000000000000000000
        00000F0E0F15635D699C7F7595FF8A7F9AFF89809DFF6F667AC8030303050000
        0000000000000000000000000000000000000000000000000000000000000000
        00002522263860596DB5665E75C55F586BAD625B70B71E1B1D30000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      TabOrder = 8
      OnClick = EmpleadoSiguienteClick
    end
    object EmpleadoUltimo_DevEx: TcxButton
      Left = 222
      Top = 6
      Width = 21
      Height = 21
      OptionsImage.Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000413B446E0A090B120000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000007C7187E25651629F0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000006E6478C6867D9CFF4D48579000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000665D6FB78A819DFF857B99FF5F5766A4000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000685F70BA8A819DFF8C829DFF867D9AFF625A6EB90302
        0304000000000000000000000000000000000000000000000000000000000000
        00000000000000000000685F70BA8A819DFF8B819CFF8C829CFF867D9BFF746C
        7FCC0C0B0D140000000000000000000000000000000000000000000000000000
        00000000000000000000685F70BA8A819DFF8B819CFF8B819CFF8C829DFF877E
        9BFF7B7290F129252A4100000000000000000000000000000000000000000000
        00000000000000000000685F70BA8A819DFF8B819CFF8B819CFF8C829CFF877E
        9CFF79708EEF2A272B4100000000000000000000000000000000000000000000
        00000000000000000000685F70BA8A819DFF8B819CFF8B819CFF857D9AFF736A
        7EC90B0A0B120000000000000000000000000000000000000000000000000000
        00000000000000000000685F70BA8A819DFF8B819CFF887F9BFF615A6EB60101
        0101000000000000000000000000000000000000000000000000000000000000
        00000000000000000000665D6FB78A819EFF867C9AFF5D5564A2000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000006E6578C6847C9BFF4C46568D00000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000007B7187E2554F5F9D0000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000413B446D0A090B120000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      TabOrder = 9
      OnClick = EmpleadoUltimo_DevExClick
    end
  end
  object BotonSalir_DevEx: TcxButton
    Left = 536
    Top = 275
    Width = 89
    Height = 26
    Hint = 'Busca Empleado'
    Anchors = [akRight, akBottom]
    BiDiMode = bdRightToLeft
    Caption = '     &Salir'
    LookAndFeel.SkinName = 'TressMorado2013'
    ModalResult = 3
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      20000000000000090000000000000000000000000000000000004858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF5362CFFF6D7AD6FF8D97DFFFACB3E8FFC8CD
      F0FFE8EAF9FFBAC0ECFF707DD7FF7682D9FF6572D4FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFF969FE2FF6572D4FF969FE2FF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
      FEFFD4D8F3FFBAC0ECFF4858CCFF4858CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
      FFFFA4ACE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFE5E7F8FFBAC0ECFF4858CCFF4858CCFF707DD7FFD1D5F2FFE5E7F8FFBAC0
      ECFF818CDCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF6A77D6FF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFF4858CCFF4858CCFFA4ACE6FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF5665D0FF7682D9FF969FE2FFB1B8
      E9FFD1D5F2FFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
    OptionsImage.Margin = 1
    ParentBiDiMode = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
  end
  object Panel2: TPanel
    Left = 418
    Top = 34
    Width = 211
    Height = 234
    Anchors = [akTop, akRight, akBottom]
    TabOrder = 3
    DesignSize = (
      211
      234)
    object Panel3: TPanel
      Left = -2
      Top = -2
      Width = 215
      Height = 238
      Anchors = [akTop, akRight, akBottom]
      TabOrder = 0
      object FOTOGRAFIA: TImageEnView
        Left = 1
        Top = 1
        Width = 213
        Height = 236
        Cursor = crDefault
        Hint = 'Foto del Empleado'
        ParentCustomHint = False
        Background = clBtnFace
        ParentCtl3D = False
        BorderStyle = bsNone
        LegacyBitmap = False
        AutoFit = True
        AutoStretch = True
        EnableInteractionHints = True
        PlayLoop = False
        Align = alClient
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
  end
  object DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 486
    Top = 140
  end
  object DSKarArea: TDataSource
    Left = 254
    Top = 115
  end
  object dsKarCertific: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 488
    Top = 248
  end
end
