unit FRegistroEspeciales;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FRegistroWorks, Db, StdCtrls, ZetaKeyCombo, ZetaNumero, ZetaHora,
  ComCtrls, Mask, ZetaFecha, ExtCtrls, DBCtrls, Buttons,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxPC, ZetaKeyLookup_DevEx, cxNavigator, cxDBNavigator, cxButtons,
  dxBarBuiltInMenu;

type
  TRegistroEspeciales = class(TRegistroWorks)
    WK_HORA_I: TZetaHora;
    Label3: TLabel;
    WK_PRE_CAL: TZetaDBNumero;
    LblDuracion: TLabel;
    Label4: TLabel;
    procedure WK_HORA_IExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetHoraInicial;
  protected
    procedure EscribirCambios; override;
    procedure SetInfoNuevoRegistro; override;
    procedure ValidaDatosLabor; override;    
  public
    { Public declarations }
  end;

var
  RegistroEspeciales: TRegistroEspeciales;

implementation

uses ZetaCommonTools, ZetaCommonClasses;

{$R *.DFM}

procedure TRegistroEspeciales.FormShow(Sender: TObject);
begin
     inherited;
     SetHoraInicial;
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      OP_NUMBER.WidthLlave := K_WIDTHLLAVE;
      CB_AREA.WidthLlave:= K_WIDTHLLAVE;

      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      OP_NUMBER.Width := K_WIDTH_LOOKUP;
      CB_AREA.Width:= K_WIDTH_LOOKUP;

      Self.Width:= K_WIDTH_BASECEDULAS;
     {$endif}
end;

procedure TRegistroEspeciales.EscribirCambios;
begin
     ValidaDatosLabor;
     inherited;
end;

procedure TRegistroEspeciales.ValidaDatosLabor;
begin
     inherited;
     if ( WK_PRE_CAL.Valor <= 0 ) then
     begin
          ActiveControl := WK_HORA_R;
          DataBaseError( 'Duraci�n debe ser mayor que Cero' );
     end;
end;

procedure TRegistroEspeciales.WK_HORA_IExit(Sender: TObject);
var
   sInicio, sFinal : String;
   iInicio, iFinal : Integer;
begin
     inherited;
     sInicio := WK_HORA_I.Valor;
     sFinal  := WK_HORA_R.Valor;
     if StrLleno( sInicio ) and StrLleno( sFinal ) then
     begin
          iInicio := aMinutos( sInicio );
          iFinal := aMinutos( sFinal );
          WK_PRE_CAL.Valor := iFinal - iInicio;
     end;
end;

procedure TRegistroEspeciales.SetInfoNuevoRegistro;
begin
     SetHoraInicial;
     inherited;
end;

procedure TRegistroEspeciales.SetHoraInicial;
begin
     FValidaHora:= FALSE;
     WK_HORA_I.Valor := aHoraString( aMinutos( WK_HORA_R.Valor ) - WK_PRE_CAL.ValorEntero );
     FValidaHora:= TRUE;
end;

end.
