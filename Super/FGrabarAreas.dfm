inherited GrabarAreas: TGrabarAreas
  Left = 298
  Top = 180
  Caption = 'Grabar Cambios de %s '
  ClientHeight = 368
  ClientWidth = 648
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 332
    Width = 648
    inherited OK_DevEx: TcxButton
      Left = 480
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 558
    end
  end
  object PanelParams: TPanel [1]
    Left = 0
    Top = 0
    Width = 648
    Height = 153
    Align = alTop
    TabOrder = 1
    object GBCuando: TGroupBox
      Left = 14
      Top = 3
      Width = 235
      Height = 140
      Caption = ' Asignaci'#243'n a Partir de: '
      TabOrder = 0
      object PaFechaLbl: TLabel
        Left = 19
        Top = 24
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
        FocusControl = PaFecha
      end
      object PaHoraLbl: TLabel
        Left = 26
        Top = 48
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hora:'
      end
      object PaFecha: TZetaFecha
        Left = 56
        Top = 19
        Width = 110
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '28/May/02'
        Valor = 37404.000000000000000000
      end
      object PaHora: TZetaHora
        Left = 56
        Top = 43
        Width = 40
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 1
        Tope = 48
      end
    end
    object GBRegreso: TGroupBox
      Left = 279
      Top = 3
      Width = 266
      Height = 140
      TabOrder = 1
      object PaFechaRegresoLbl: TLabel
        Left = 49
        Top = 90
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
        FocusControl = PaFechaRegreso
      end
      object PaHoraRegresoLbl: TLabel
        Left = 56
        Top = 115
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hora:'
      end
      object rbSinRegreso: TRadioButton
        Left = 8
        Top = 15
        Width = 215
        Height = 17
        Caption = 'Sin Regreso a %s Original'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = rbRegresoClick
      end
      object rbPrimeraHora: TRadioButton
        Tag = 1
        Left = 8
        Top = 40
        Width = 215
        Height = 17
        Caption = 'Regresa Ma'#241'ana a Primera Hora'
        TabOrder = 1
        OnClick = rbRegresoClick
      end
      object rbFechaHora: TRadioButton
        Tag = 2
        Left = 8
        Top = 65
        Width = 215
        Height = 17
        Caption = 'Regresa a Fecha y Hora Espec'#237'fica'
        TabOrder = 2
        OnClick = rbRegresoClick
      end
      object PaFechaRegreso: TZetaFecha
        Left = 86
        Top = 85
        Width = 110
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '28/May/02'
        Valor = 37404.000000000000000000
      end
      object PaHoraRegreso: TZetaHora
        Left = 86
        Top = 110
        Width = 40
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 4
        Tope = 48
      end
    end
  end
  object ZetaDBGrid: TZetaCXGrid [2]
    Left = 0
    Top = 153
    Width = 648
    Height = 179
    Align = alClient
    TabOrder = 2
    object ZetaDBGridDBTableView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      DataController.DataSource = DataSource
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnSortingChanged = ZetaDBGridDBTableViewDataControllerSortingChanged
      Filtering.ColumnFilteredItemsList = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      OnColumnHeaderClick = ZetaDBGridDBTableViewColumnHeaderClick
      object ZetaDBGridDBTableViewColumn1: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 100
      end
      object ZetaDBGridDBTableViewColumn2: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        MinWidth = 100
      end
      object ZetaDBGridDBTableViewColumn3: TcxGridDBColumn
        Caption = 'De'
        DataBinding.FieldName = 'ANTERIOR_DESCRIP'
        HeaderAlignmentHorz = taCenter
        MinWidth = 100
        Styles.Content = cxStyle1
      end
      object ZetaDBGridDBTableViewColumn4: TcxGridDBColumn
        Caption = 'A'
        DataBinding.FieldName = 'CB_AREA_DESCRIP'
        HeaderAlignmentHorz = taCenter
        MinWidth = 100
        Styles.Content = cxStyle1
      end
    end
    object ZetaDBGridLevel: TcxGridLevel
      GridView = ZetaDBGridDBTableView
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource: TDataSource
    Left = 328
    Top = 208
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
  end
end
