inherited SuperEditCosteoTransferencias: TSuperEditCosteoTransferencias
  Left = 591
  Top = 188
  Caption = 'Transferencias'
  ClientHeight = 384
  ClientWidth = 480
  PixelsPerInch = 96
  TextHeight = 13
  object TipoAutLbl: TLabel [0]
    Left = 118
    Top = 234
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  object HorasLbl: TLabel [1]
    Left = 111
    Top = 159
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Horas:'
    FocusControl = TR_HORAS
  end
  object MotivoLbl: TLabel [2]
    Left = 107
    Top = 184
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Motivo:'
    FocusControl = TR_MOTIVO
  end
  object Label3: TLabel [3]
    Left = 92
    Top = 259
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Transfiere:'
    FocusControl = TR_MOTIVO
  end
  object US_DESCRIP: TZetaDBTextBox [4]
    Left = 144
    Top = 255
    Width = 196
    Height = 21
    AutoSize = False
    Caption = 'US_DESCRIP'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lbDestino: TLabel [5]
    Left = 85
    Top = 109
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'A Transferir:'
    FocusControl = CC_CODIGO
  end
  object CC_ORIGEN: TZetaDBTextBox [6]
    Left = 144
    Top = 81
    Width = 60
    Height = 21
    AutoSize = False
    Caption = 'CC_ORIGEN'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CC_ORIGEN'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lbOrigen: TLabel [7]
    Left = 91
    Top = 85
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'CC Origen:'
    FocusControl = CC_CODIGO
  end
  object CC_DESCRIP: TZetaDBTextBox [8]
    Left = 206
    Top = 81
    Width = 238
    Height = 21
    AutoSize = False
    Caption = 'CC_DESCRIP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CC_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object TR_FECHA: TZetaDBTextBox [9]
    Left = 344
    Top = 255
    Width = 100
    Height = 21
    AutoSize = False
    Caption = 'TR_FECHA'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'TR_FECHA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [10]
    Left = 68
    Top = 209
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
  end
  object Label4: TLabel [11]
    Left = 84
    Top = 134
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Aprobar por:'
    FocusControl = TR_APRUEBA
  end
  object EmpleadoLbl: TLabel [12]
    Left = 92
    Top = 61
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleado:'
    FocusControl = CB_CODIGO
  end
  inherited PanelBotones: TPanel
    Top = 348
    Width = 480
    TabOrder = 10
    inherited OK_DevEx: TcxButton
      Left = 314
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 393
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 480
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 154
      inherited textoValorActivo2: TLabel
        Width = 148
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 14
  end
  object TR_MOTIVO: TZetaDBKeyLookup_DevEx [16]
    Left = 144
    Top = 180
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsMotivoTransfer
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 5
    TabStop = True
    WidthLlave = 60
    DataField = 'TR_MOTIVO'
    DataSource = DataSource
  end
  object TR_HORAS: TZetaDBNumero [17]
    Left = 144
    Top = 155
    Width = 41
    Height = 21
    Mascara = mnHoras
    TabOrder = 4
    Text = '0.00'
    DataField = 'TR_HORAS'
    DataSource = DataSource
  end
  object CC_CODIGO: TZetaDBKeyLookup_DevEx [18]
    Left = 144
    Top = 105
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsNivel1
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 60
    OnValidKey = CC_CODIGOValidKey
    DataField = 'CC_CODIGO'
    DataSource = DataSource
  end
  object TR_TIPO: TZetaDBKeyCombo [19]
    Left = 144
    Top = 230
    Width = 196
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 7
    ListaFija = lfTipoHoraTransferenciaCosteo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TR_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TR_TEXTO: TZetaDBEdit [20]
    Left = 144
    Top = 205
    Width = 300
    Height = 21
    TabOrder = 6
    Text = 'TR_TEXTO'
    DataField = 'TR_TEXTO'
    DataSource = DataSource
  end
  object TR_APRUEBA: TZetaDBKeyLookup_DevEx [21]
    Left = 144
    Top = 130
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsNivel1
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 60
    DataField = 'TR_APRUEBA'
    DataSource = DataSource
  end
  object CB_CODIGO: TZetaDBKeyLookup_DevEx [22]
    Left = 144
    Top = 57
    Width = 300
    Height = 21
    LookupDataset = dmCliente.cdsEmpleadoLookUp
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object GroupBox1: TGroupBox [23]
    Left = 0
    Top = 280
    Width = 480
    Height = 68
    Align = alBottom
    Caption = ' Aprobaci'#243'n: '
    TabOrder = 8
    object Label1: TLabel
      Left = 109
      Top = 19
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object TR_FEC_APR: TZetaDBTextBox
      Left = 344
      Top = 15
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'TR_FEC_APR'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TR_FEC_APR'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label6: TLabel
      Left = 81
      Top = 43
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Comentarios:'
    end
    object TR_STATUS: TZetaDBKeyCombo
      Left = 144
      Top = 15
      Width = 198
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      ListaFija = lfStatusTransCosteo
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'TR_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object TR_TXT_APR: TZetaDBEdit
      Left = 144
      Top = 39
      Width = 300
      Height = 21
      TabOrder = 1
      Text = 'TR_TXT_APR'
      DataField = 'TR_TXT_APR'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  object ActionList1: TActionList
    Left = 48
    Top = 56
  end
end
