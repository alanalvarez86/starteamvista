unit FGridAprobarCosteoTransferencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ZetaKeyCombo, Db, Grids, DBGrids, ZetaDBGrid,
  DBCtrls, ZetaSmartLists, Buttons, ExtCtrls, ImgList, Mask,
  ZetaFecha, ZetaCommonLists, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup, ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxGroupBox,
  cxRadioGroup;

type
  TGridAprobarCosteoTransferencias_DevEx = class(TBaseGridEdicion_DevEx)
    PanelFiltro: TPanel;
    ImageGrid: TImageList;
    lblNomina: TLabel;
    Label1: TLabel;
    AU_FECHA: TZetaFecha;
    Label5: TLabel;
    cbStatusTransferencia: TZetaKeyCombo;
    rgTipo: TcxRadioGroup;
    CCOrigen: TZetaKeyLookup_DevEx;
    CCDestino: TZetaKeyLookup_DevEx;
    TR_MOTIVO: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    lblMaestro: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    btnFiltrar: TcxButton;
    ComentariosLbl: TLabel;
    EdComenta: TEdit;
    bbApruebaTodos: TdxBarButton;
    bbCancelaTodos: TdxBarButton;
    bbPorAprobarTodos: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridCellClick(Column: TColumn);
    procedure bbPorAprobarTodos2Click(Sender: TObject);
    procedure bbApruebaTodos2Click(Sender: TObject);
    procedure bbCancelaTodos2Click(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure Refresh;
    procedure CambiaAprobacion(const eStatus: eStatusTransCosteo );
    procedure CambiaAprobacionTodos( const eStatus: eStatusTransCosteo );
   
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure HabilitaControles; override;
    procedure CancelarCambios; override;
    procedure LlenaStatusTransferencias;
  public
    { Public declarations }
  end;

var
  GridAprobarCosteoTransferencias_DevEx: TGridAprobarCosteoTransferencias_DevEx;

implementation

{$R *.DFM}

uses
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    DSistema,
    DCliente,
    DSuper,
    DTablas, DBClient;

{ TGridAprobarCosteoTransferencias }
procedure TGridAprobarCosteoTransferencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PrimerColumna := 9;
     HelpContext := H_Grid_Aprobar_Costeo_Transferencia;

     CCOrigen.LookupDataset := dmTablas.GetDataSetTransferencia;
     CCDestino.LookupDataset := dmTablas.GetDataSetTransferencia;
     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;
     Datasource.DataSet := dmSuper.cdsGridCosteoTransferencia;

     LlenaStatusTransferencias;
     cbStatusTransferencia.ItemIndex := 0;
     rgTipo.ItemIndex := 0;
     EdComenta.MaxLength := K_ANCHO_DESCRIPCION;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := AU_FECHA;
     AU_FECHA.Valor := dmCliente.FechaSupervisor;
     EdComenta.Text := VACIO;
     Refresh;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.Connect;
begin
     with dmTablas do
     begin
          GetDataSetTransferencia.Conectar;
          cdsMotivoTransfer.Conectar;
     end;
     dmSistema.cdsUsuariosLookUp.Conectar;
     Datasource.Dataset := dmSuper.cdsGridCosteoTransferencia;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
     K_CHECKED   = 0;
     K_UNCHECKED = 1;
var
   iImagen: integer;

   function GetPosicion: Integer;
   begin
        Result := Rect.Right - Rect.Left;
        if ( Result > ImageGrid.Width ) then
           Result := Rect.Left + Round( ( Result - ImageGrid.Width ) / 2 ) + 1
        else
            Result := Rect.Left;
   end;

   procedure SetImagen( const sField: string; const eStatus: eStatusTransCosteo );
   begin
        if ( Column.FieldName = sField ) then
        begin
             if ( eStatusTransCosteo( DataSource.DataSet.FieldByName( 'TR_STATUS' ).AsInteger ) =  eStatus ) then
                iImagen := K_CHECKED
             else
                 iImagen := K_UNCHECKED;
        end
   end;

begin
     if ( not DataSource.DataSet.IsEmpty ) and ( Column.Visible ) and
        ( ( Column.FieldName = 'POR_APROBAR' ) or
          ( Column.FieldName = 'APROBADA' ) or
          ( Column.FieldName = 'CANCELADA' ) ) then
     begin
          SetImagen( 'POR_APROBAR', stcPorAprobar );
          SetImagen( 'APROBADA', stcAprobada );
          SetImagen( 'CANCELADA', stcCancelada );

          ImageGrid.Draw( ZetaDBGrid.Canvas, GetPosicion, Rect.top + 1, iImagen );
     end
     else
         ZetaDBGrid.DefaultDrawColumnCell( Rect, DataCol, Column, State );
end;

procedure TGridAprobarCosteoTransferencias_DevEx.ZetaDBGridCellClick(Column: TColumn);
const
     K_COMENTA_APRUEBA = 11;
begin
     inherited;
     with DataSource.DataSet do
     begin
          if ( not IsEmpty ) then
          begin
               if ( Column.FieldName = 'POR_APROBAR' ) then
                    CambiaAprobacion( stcPorAprobar )
               else if ( Column.FieldName = 'APROBADA' ) then
                    CambiaAprobacion( stcAprobada )
               else if ( Column.FieldName = 'CANCELADA' ) then
                    CambiaAprobacion( stcCancelada );

               if  ( Column.FieldName = 'POR_APROBAR' ) or
                   ( Column.FieldName = 'APROBADA' ) or
                   ( Column.FieldName = 'CANCELADA' )  then
                   ZetaDBGrid.SelectedField := ZetaDBGrid.Columns[ K_COMENTA_APRUEBA ].Field;
          end;
     end;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.CambiaAprobacion( const eStatus: eStatusTransCosteo );
begin
     with dmSuper.cdsGridCosteoTransferencia do
     begin
          if ( eStatusTransCosteo( FieldByName('TR_STATUS').AsInteger ) <> eStatus ) then
          begin
               if ( State <> dsEdit ) then
                  Edit;
               FieldByName( 'TR_APRUEBA' ).AsInteger := dmCliente.Usuario;
               FieldByName( 'TR_STATUS' ).AsInteger := Ord(eStatus);
               FieldByName( 'TR_FEC_APR' ).AsDateTime := dmCliente.FechaSupervisor;
               Post;
          end;
     end;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.CambiaAprobacionTodos( const eStatus: eStatusTransCosteo );
var
   sFiltro: string;
begin
     with dmSuper.cdsGridCosteoTransferencia do
     begin
          if NOT IsEmpty then
          begin
               DisableControls;
               try
                  sFiltro := Filter;
                  Filtered := FALSE;

                  First;
                  while NOT EOF do
                  begin
                       if ( eStatusTransCosteo( FieldByName('TR_STATUS').AsInteger ) <> eStatus ) then
                       begin
                            if ( State <> dsEdit ) then
                               Edit;

                            FieldByName( 'TR_STATUS' ).AsInteger := Ord(eStatus);
                       end;
                       Next;
                  end;
                  HuboCambios := TRUE;
               finally
                      EnableControls;
               end;
          end
          else
          begin
               ZetaDialogo.ZError( Self.Caption, '� No hay Transferencias por Aprobar/Cancelar !', 0 );
          end;
     end;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if strLleno( EdComenta.Text ) and Assigned( Field ) and ( Field.FieldName = 'TR_STATUS' ) then
     begin
          with DataSource.DataSet do
          begin
               if StrVacio( FieldByName( 'TR_TXT_APR' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'TR_TXT_APR' ).AsString := EdComenta.Text;
               end;
          end;
     end;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.Agregar;
begin
     { No hace Nada }
end;

procedure TGridAprobarCosteoTransferencias_DevEx.Borrar;
begin
     { No hace Nada }
end;

procedure TGridAprobarCosteoTransferencias_DevEx.HabilitaControles;
var
   lEnabled : Boolean;
begin
     inherited;
     lEnabled := ( not Editing );
     AU_FECHA.Enabled := lEnabled;
     CCOrigen.Enabled := lEnabled;
     CCDestino.Enabled := lEnabled;
     rgTipo.Enabled := lEnabled;
     cbStatusTransferencia.Enabled := lEnabled;
     TR_MOTIVO.Enabled := lEnabled;
     btnFiltrar.Enabled := lEnabled;

     Application.ProcessMessages;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.CancelarCambios;
begin
     inherited;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.LlenaStatusTransferencias;
var
   eTipo: eStatusTransCosteo;
begin
     with cbStatusTransferencia.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for eTipo := low( eStatusTransCosteo ) to high( eStatusTransCosteo ) do
             begin
                  Add( Format( '%d=%s', [ ord( eTipo ), ObtieneElemento( lfStatusTransCosteo, Ord( eTipo ) ) ] ) );
             end;
             Add( '3=Todas' );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.Refresh;
begin
     with dmSuper do
     begin
          if ParametrosTransferencias = NIL then
             ParametrosTransferencias := TZetaParams.Create;

          with ParametrosTransferencias do
          begin
               AddDate( 'FechaIni', AU_FECHA.Valor );
               AddDate( 'FechaFin', AU_FECHA.Valor );

               AddString( 'CCOrigen', CCOrigen.Llave );
               AddString( 'CCDestino', CCDestino.LLave );
               AddString( 'Motivo', TR_MOTIVO.Llave );
               AddInteger( 'Tipo', rgTipo.ItemIndex -1 );
               AddBoolean( 'SoloNegativos', FALSE );
               AddInteger( 'Globales', - 1 ); //Indica que son todas
               AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );

               AddBoolean( 'ModuloSupervisores', TRUE );
               //En esta pantalla siempre se muestra las transferencias del supervisor activo
               AddBoolean( 'MisPendientes', TRUE );

               //El supervisor activo solo puede modificar las transferencias que tienen
               if ( cbStatusTransferencia.ItemIndex  <= ord( stcCancelada ) ) then
                  AddInteger( 'Status', cbStatusTransferencia.ItemIndex )
               else
                   AddInteger( 'Status', -1 );
          end;

          ObtieneTransferenciasDataset( cdsGridCosteoTransferencia, ParametrosTransferencias );
     end;
end;

procedure TGridAprobarCosteoTransferencias_DevEx.bbPorAprobarTodos2Click( Sender: TObject);
begin
     inherited;
     CambiaAprobacionTodos( stcPorAprobar )
end;

procedure TGridAprobarCosteoTransferencias_DevEx.bbApruebaTodos2Click( Sender: TObject);
begin
     inherited;
     CambiaAprobacionTodos( stcAprobada )
end;

procedure TGridAprobarCosteoTransferencias_DevEx.bbCancelaTodos2Click( Sender: TObject);
begin
     inherited;
     CambiaAprobacionTodos( stcCancelada );
end;

procedure TGridAprobarCosteoTransferencias_DevEx.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     Refresh;
end;

end.
