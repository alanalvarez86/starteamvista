unit FGridSesiones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, {ZBaseConsulta,} ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, Buttons, Mask, ZetaFecha, ZBaseConsulta,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx, cxButtons;

type
  TGridSesiones = class(TBaseGridLectura_DevEx)
    pnFiltros: TPanel;
    lblFecIni: TLabel;
    lblFecFin: TLabel;
    Label1: TLabel;
    lblMaestro: TLabel;
    zFechaIni: TZetaFecha;
    zFechaFin: TZetaFecha;
    CU_CODIGO: TZetaKeyLookup_DevEx;
    MA_CODIGO: TZetaKeyLookup_DevEx;
    SE_FOLIO: TcxGridDBColumn;
    CU_CODIGO_GRID: TcxGridDBColumn;
    CU_NOMBRE: TcxGridDBColumn;
    SE_FEC_INI: TcxGridDBColumn;
    MA_NOMBRE: TcxGridDBColumn;
    SE_CUPO: TcxGridDBColumn;
    SE_INSCRITO: TcxGridDBColumn;
    SE_FEC_FIN: TcxGridDBColumn;
    SE_APROBADO: TcxGridDBColumn;
    SE_COSTOT: TcxGridDBColumn;
    btnFiltrar_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFiltrar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    //procedure Setcontroles( const lHabilita: Boolean );
  public
    { Public declarations }
  protected
     procedure Connect; override;
     procedure Refresh; override;
     procedure Agregar; override;
     procedure Borrar; override;
     procedure Modificar; override;
     //function PuedeModificar( var sMensaje: String ): Boolean; override;
     function PuedeBorrar( var sMensaje: String ): Boolean; override;
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     function PuedeAgregar( var sMensaje: String ): Boolean; override;
     function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

var
  GridSesiones: TGridSesiones;

implementation

uses DCatalogos,
     ZetaCommonTools,
     DSuper,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress,
     DCliente,
     FAutoClasses;

{$R *.dfm}

{ TGridSesiones }

procedure TGridSesiones.FormCreate(Sender: TObject);
begin
     inherited;
     lblMaestro.Visible:=FALSE;
     MA_CODIGO.Visible:= FALSE;
     with dmCatalogos do
     begin
          MA_CODIGO.LookupDataSet := cdsMaestros;
          CU_CODIGO.LookUpDataSet := cdsCursos;
     end;
     with dmSuper do
     begin
          zFechaIni.Valor:= DiaDelMes( Date, True );
          zFechaFin.Valor:= DiaDelMes( Date, False );
     end;
     HelpContext := H_SESIONES;
end;

procedure TGridSesiones.Agregar;
begin
     inherited;
     dmSuper.cdsGrupos.Agregar;
end;

procedure TGridSesiones.Borrar;
begin
     inherited;
     dmSuper.cdsGrupos.Borrar;
     DoBestFit;
end;

procedure TGridSesiones.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
	  cdsMaestros.Conectar;
     end;
     DoRefresh;
     DataSource.DataSet:= dmSuper.cdsGrupos;
end;

procedure TGridSesiones.Modificar;
begin
     inherited;
     dmSuper.cdsGrupos.Modificar;
end;


function TGridSesiones.PuedeBorrar(var sMensaje: String): Boolean;
const
     aDerechoGrupo: array[FALSE..TRUE] of Integer = ( D_SUPER_SESIONES_OTROS, D_SUPER_GRUPOS );
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          with dmSuper.cdsGrupos do
          begin
               Result:= CheckDerecho( aDerechoGrupo[ ( FieldByName('US_CODIGO').AsInteger = dmCliente.Usuario ) ], K_DERECHO_BAJA );
               if not ( Result ) then
               begin
                    sMensaje:= 'No tiene derechos de borrar registros '
               end;
          end;
     end;
end;

function TGridSesiones.PuedeModificar( var sMensaje: String ): Boolean;
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TGridSesiones.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

procedure TGridSesiones.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TGridSesiones.Refresh;
begin
     inherited;
     with dmSuper do
     begin
          with ParametrosSesion do
          begin
               AddString( 'Curso', CU_CODIGO.Llave );
               AddString( 'Maestro', VACIO );
               AddDate( 'FechaIniSesion', zFechaIni.Valor );
               AddDate( 'FechaFinSesion', zFechaFin.Valor );

          end;
         ObtieneSesionesFiltros;
    end;
    DoBestFit;
end;

procedure TGridSesiones.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;
end;

procedure TGridSesiones.btnFiltrar_DevExClick(Sender: TObject);
begin
  inherited;
  DoRefresh;
end;

end.
