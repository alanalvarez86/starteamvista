unit FGridAutorizacionesPorAprobar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridAutorizacionesPorAprobar_DevEx, Db, ZetaKeyLookup, StdCtrls, Mask,
  ZetaFecha, ZetaKeyCombo, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  ZetaKeyLookup_DevEx, cxNavigator, cxDBNavigator, cxButtons;

type
  TGridAutorizacionesPorAprobar = class(TBaseGridAutorizacionesPorAprobar_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure zFechaValidDate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function ChecaDerecho: boolean; override;      
    procedure Connect; override;
    procedure InicializaControles; override;
  public
    { Public declarations }
  end;

var
  GridAutorizacionesPorAprobar: TGridAutorizacionesPorAprobar;

implementation

{$R *.DFM}

uses
    DSuper,
    DCliente,
    DGlobal,
    zGlobalTress,
    ZAccesosTress,
    ZAccesosMgr,
    ZetaCommonTools,
    ZetaCommonClasses;

{ TGridAutorizacionesPorAprobar }

procedure TGridAutorizacionesPorAprobar.FormCreate(Sender: TObject);
begin
     with Supervisores do
     begin
          LookupDataset := dmSuper.cdsSupervisores;
          Filtro := 'MI_CODIGO <> '''' and MI_CODIGO IS NOT NULL';
     end;
     AutoXAprobar := dmSuper.cdsGridAutoXAprobar;
     //HelpContext := H20223_Autorizaciones_por_aprobar;
     HelpContext := H20223_Autorizaciones_por_aprobarSuper;   //act. HC, se creo nueva HC;
     inherited;
end;

procedure TGridAutorizacionesPorAprobar.Connect;
begin
     with dmSuper do
     begin
          cdsSupervisores.Conectar;
          FechaGridAuto := dmCliente.FechaSupervisor;
     end;
     inherited;
end;

function TGridAutorizacionesPorAprobar.ChecaDerecho: boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL, APROBAR_AUTORIZACION );
end;

procedure TGridAutorizacionesPorAprobar.InicializaControles;
begin
     inherited;
     zFecha.Valor := dmSuper.FechaGridAuto;
end;

procedure TGridAutorizacionesPorAprobar.zFechaValidDate(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     dmSuper.FechaGridAuto := zFecha.Valor;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        AutoXAprobar.Refrescar;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
