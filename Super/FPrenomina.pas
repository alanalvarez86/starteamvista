unit FPrenomina;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}
  StdCtrls, Mask, ZetaNumero, ZetaKeyCombo, ZetaDBTextBox, Buttons,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxPCdxBarPopupMenu, cxPC, cxButtons;

type
  TPrenomina = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    PanelPer: TPanel;
    Label37: TLabel;
    Label38: TLabel;
    LabDel: TLabel;
    PerInicio: TZetaDBTextBox;
    LabAl: TLabel;
    PerFinal: TZetaDBTextBox;
    PaTipo: TZetaKeyCombo;
    PaPer: TZetaNumero;
    PanelStatus: TPanel;
    Label39: TLabel;
    LBStatus: TLabel;
    dsAusencia: TDataSource;
    dsPeriodo: TDataSource;
    ImgNoPrenomina: TImage;
    au_posicio: TcxGridDBColumn;
    AU_FECHA: TcxGridDBColumn;
    CHECADA1: TcxGridDBColumn;
    CHECADA2: TcxGridDBColumn;
    CHECADA3: TcxGridDBColumn;
    CHECADA4: TcxGridDBColumn;
    au_horas: TcxGridDBColumn;
    AU_NUM_EXT: TcxGridDBColumn;
    au_extras: TcxGridDBColumn;
    au_per_cg: TcxGridDBColumn;
    au_per_sg: TcxGridDBColumn;
    au_des_tra: TcxGridDBColumn;
    au_tardes: TcxGridDBColumn;
    AU_STATUS: TcxGridDBColumn;
    AU_TIPODIA: TcxGridDBColumn;
    au_tipo: TcxGridDBColumn;
    ho_codigo: TcxGridDBColumn;
    us_codigo_GRID: TcxGridDBColumn;
    cxPageControl1: TcxPageControl;
    TabSheet_Dias: TcxTabSheet;
    TabSheet_Horas: TcxTabSheet;
    TabSheet_Turno: TcxTabSheet;
    TabSheet4_Generales: TcxTabSheet;
    Label17: TLabel;
    NO_DIAS_SG: TZetaDBTextBox;
    NO_DIAS: TZetaDBTextBox;
    Label18: TLabel;
    NO_DIAS_AS: TZetaDBTextBox;
    Label22: TLabel;
    NO_DIAS_NT: TZetaDBTextBox;
    Label24: TLabel;
    NO_DIAS_SU: TZetaDBTextBox;
    Label21: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    Label23: TLabel;
    Label27: TLabel;
    NO_DIAS_CG: TZetaDBTextBox;
    Label28: TLabel;
    NO_DIAS_OT: TZetaDBTextBox;
    Label29: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    Label30: TLabel;
    NO_DIAS_RE: TZetaDBTextBox;
    NO_DIAS_EM: TZetaDBTextBox;
    NO_DIAS_SS: TZetaDBTextBox;
    Label25: TLabel;
    Label26: TLabel;
    Label19: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    Label20: TLabel;
    NO_DIAS_FJ: TZetaDBTextBox;
    NO_DIAS_FV: TZetaDBTextBox;
    Label46: TLabel;
    Label31: TLabel;
    NO_DIAS_VA: TZetaDBTextBox;
    NO_DIAS_AG: TZetaDBTextBox;
    Label32: TLabel;
    Label40: TLabel;
    NO_DIAS_SI: TZetaDBTextBox;
    gbAutorizacion: TGroupBox;
    Label41: TLabel;
    NO_SUP_OK: TZetaDBTextBox;
    Label42: TLabel;
    NO_FEC_OK: TZetaDBTextBox;
    Label43: TLabel;
    NO_HOR_OK: TZetaDBTextBox;
    ModificarAsistencia_DevEx: TcxButton;
    Label4: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label5: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label6: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label7: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label8: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label45: TLabel;
    NO_HORASNT: TZetaDBTextBox;
    Label10: TLabel;
    NO_HORA_PD: TZetaDBTextBox;
    Label44: TLabel;
    NO_HORAPDT: TZetaDBTextBox;
    Label9: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label11: TLabel;
    NO_DES_TRA: TZetaDBTextBox;
    Label36: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label15: TLabel;
    Label14: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    NO_FES_TRA: TZetaDBTextBox;
    Label12: TLabel;
    Label13: TLabel;
    Label1: TLabel;
    CB_TURNO: TZetaDBTextBox;
    Turno: TZetaDBTextBox;
    Label2: TLabel;
    NO_D_TURNO: TZetaDBTextBox;
    Label3: TLabel;
    NO_JORNADA: TZetaDBTextBox;
    bbMostrarCalendario_DevEx: TcxButton;
    Label33: TLabel;
    STATUSNOM: TZetaDBTextBox;
    NO_OBSERVA: TZetaDBTextBox;
    Label16: TLabel;
    Label34: TLabel;
    US_CODIGO: TZetaDBTextBox;
    NO_USER_RJ: TZetaDBTextBox;
    Label35: TLabel;
    btnUp_DevEx: TcxButton;
    btnDown_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure PaPerKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure PaTipoChange(Sender: TObject);
    procedure PaPerExit(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    //procedure bbMostrarCalendarioClick(Sender: TObject);
   // procedure ModificarAsistenciaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure ModificarAsistencia_DevExClick(Sender: TObject);
    procedure bbMostrarCalendario_DevExClick(Sender: TObject);
    procedure btnUp_DevExClick(Sender: TObject);
    procedure btnDown_DevExClick(Sender: TObject);
  private
    { Private declarations }
    lConectando : Boolean;
    procedure Refrescar;
    procedure SetLabelStatus;
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function ValoresGrid: Variant;override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ApplyMinWidth; override;
  public
    { Public declarations }
  end;

var
  Prenomina: TPrenomina;

implementation

uses DNomina, DSistema, DCatalogos, DCliente, DSuper,
     ZetaCommonTools, ZetaCommonLists,
     ZetaCommonClasses,
     DTablas,ZBaseGridEdicion_DevEx,ZetaDialogo,{FGridAjusteTarjetaPeriodo} FGridAjusteTarjetaPeriodo_DevEx;

{$R *.DFM}

const
     aTitulo: array[ False..True ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( ' Pren�mina No Autorizada ', ' Pren�mina Autorizada ' );

procedure TPrenomina.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 00501;
     lConectando := TRUE;
     TipoValorActivo1 := stNinguno;
     TipoValorActivo2 := stNinguno;
end;

procedure TPrenomina.Connect;
begin
     lConectando := TRUE;
     // Igual que FNomDatosAsist
     dmSistema.cdsUsuarios.Conectar;
     dmCatalogos.cdsTurnos.Conectar;
     with dmNomina do
     begin
          cdsDatosAsist.Conectar;
          DataSource.DataSet := cdsDatosAsist;
          dsAusencia.DataSet := cdsMovDatosAsist;
     end;
     with dmCliente do
     begin
          dsPeriodo.DataSet := cdsPeriodo;
          with PaTipo do //acl
          begin
               if ( Llave <> IntToStr( Ord( PeriodoTipo ) ) ) then
                  Llave := IntToStr( Ord( PeriodoTipo ) );
          end;
          with PaPer do
               if ( Valor <> PeriodoNumero ) then
               begin
                    Valor := PeriodoNumero;
                   // UpDownPer.Position := PeriodoNumero;
               end;
     end;
     lConectando := FALSE;
     SetLabelStatus;

     {***DevEx(by am): Se agrega un DoBesfit para que cuando se cambie de empleado en la navegacion del grid
     se ajusten los tamanos de nuevo. ***}
     DoBestFit;
end;

procedure TPrenomina.Refresh;
begin
     dmNomina.cdsDatosAsist.Refrescar;
     DoBestFit;
end;

function TPrenomina.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar En Esta Pantalla';
end;

procedure TPrenomina.Agregar;
begin
end;

function TPrenomina.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Esta Pantalla';
end;

procedure TPrenomina.Borrar;
begin
end;

function TPrenomina.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar En Esta Pantalla';
end;

procedure TPrenomina.Modificar;
begin
end;

procedure TPrenomina.Refrescar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        DoRefresh;
        SetLabelStatus;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TPrenomina.SetLabelStatus;
begin
     lbStatus.Caption := ZetaCommonTools.GetDescripcionStatusPeriodo(  eStatusPeriodo( dmCliente.cdsPeriodo.FieldByName('PE_STATUS').AsInteger ), eStatusTimbrado( dmCliente.cdsPeriodo.FieldByName('PE_TIMBRO').AsInteger )  );
end;

procedure TPrenomina.PaPerKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
     if Key in [13, 9] then
        ZetaDBGrid.SetFocus;
end;

{ Se debe de poder controlar PaPer.Valor manualmente porque la sincronizazion con el objeto UpDown cuando no es una valida
  existente no lo sincroniza   se cambio de tipo de objeto
procedure TPrenomina.UpDownPerClick(Sender: TObject; Button: TUDBtnType);
begin
     inherited;
     with dmCliente do
     begin
          case Button of
               btNext :
               begin
                    GetPeriodoSiguiente;
               end;
               btPrev :
               begin
                    GetPeriodoAnterior;
               end;
          end;
          Refrescar;
     end;
end;}

procedure TPrenomina.PaTipoChange(Sender: TObject);
begin
     inherited;
     if not lConectando then
     begin
          with dmCliente do
          begin
               if ( ( StrLleno ( PaTipo.Llave ) ) and  ( IntToStr( Ord( PeriodoTipo ) ) <> PaTipo.Llave ) ) then //acl
               begin
                    PeriodoTipo := eTipoPeriodo( StrAsInteger( PaTipo.Llave ) ); //acl
                    PaPer.Valor:= PeriodoNumero;
                    Refrescar;
               end;
          end;
     end;
end;

procedure TPrenomina.PaPerExit(Sender: TObject);
begin
     inherited;
     if not lConectando then
     begin
          with dmCliente do
          begin
               if ( PeriodoNumero <> PaPer.ValorEntero ) then
               begin
                    PeriodoNumero := PaPer.ValorEntero;
                    if ( PeriodoNumero <> PaPer.ValorEntero ) then
                    begin
                         ZetaDialogo.zInformation( 'Error', '! No Existe El Per�odo De N�mina !', 0 );
                         PaPer.Valor:= PeriodoNumero;
                    end;
                    Refrescar;
               end;
          end;
     end;
end;

function TPrenomina.ValoresGrid: Variant;
begin
     with dsPeriodo.DataSet do
          Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                                  ObtieneElemento( lfTipoNomina, FieldByName('PE_TIPO').AsInteger )+ ' '+
                                  IntToStr(PaPer.ValorEntero)+ ' del ' +
                                  FechaCorta(FieldByName('PE_FEC_INI').AsDateTime) + ' al ' +
                                  FechaCorta(FieldByName('PE_FEC_INI').AsDateTime),
                                  stEmpleado,stPeriodo] );
end;

procedure TPrenomina.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     ImgNoPrenomina.Visible:= ImgNoRecord.Visible;
     with Datasource do
     begin
          if ( Dataset <> nil ) then
          begin
               with DataSet do
               begin
                    //bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
                    bbMostrarCalendario_DevEx.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
                    gbAutorizacion.Caption := aTitulo[ ( FieldByName( 'NO_SUP_OK' ).AsInteger <> 0 ) ];
               end;
          end;
     end;
end;

{procedure TPrenomina.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;}

{procedure TPrenomina.ModificarAsistenciaClick(Sender: TObject);
begin
     if dmCliente.ValidaTarjetaStatusDlg( dmCliente.GetDatosPeriodoActivo.Status )then
     begin
          with dmSuper.cdsGridTarjetasPeriodo do
          begin
               dmTablas.cdsIncidencias.Conectar;
               Refrescar;
               if not IsEmpty then
                  ZBaseGridEdicion.ShowGridEdicion( GridAjusteTarjetasPeriodo,TGridAjusteTarjetasPeriodo,False )
               else
                   ZInformation('Pr�nomina','La Lista de Tarjetas del Per�odo Esta Vac�a ',0);
          end;
     end;
end; }

procedure TPrenomina.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;
  //DevEx (by am): Remueve la opcion de filtrado de las columnas
  ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;

     with PaTipo do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;
end;


procedure TPrenomina.btnUpClick(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          GetPeriodoSiguiente;
          PaPer.Valor:= PeriodoNumero;
     end;
     Refrescar;
end;

procedure TPrenomina.btnDownClick(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          GetPeriodoAnterior;
          PaPer.Valor:= PeriodoNumero;
     end;
     Refrescar;
end;

procedure TPrenomina.ModificarAsistencia_DevExClick(Sender: TObject);
begin
  inherited;
  if dmCliente.ValidaTarjetaStatusDlg( dmCliente.GetDatosPeriodoActivo.Status )then
     begin
          with dmSuper.cdsGridTarjetasPeriodo do
          begin
               dmTablas.cdsIncidencias.Conectar;
               Refrescar;
               if not IsEmpty then
                  ZBaseGridEdicion_DevEx.ShowGridEdicion( GridAjusteTarjetasPeriodo_DevEx,TGridAjusteTarjetasPeriodo_DevEx,False )
               else
                   ZInformation('Pr�nomina','La Lista de Tarjetas del Per�odo Esta Vac�a ',0);
          end;
     end;
end;

procedure TPrenomina.bbMostrarCalendario_DevExClick(Sender: TObject);
begin
  inherited;
  dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

procedure TPrenomina.btnUp_DevExClick(Sender: TObject);
begin
  inherited;
  with dmCliente do
     begin
          GetPeriodoSiguiente;
          PaPer.Valor:= PeriodoNumero;
     end;
     Refrescar;
end;

procedure TPrenomina.btnDown_DevExClick(Sender: TObject);
begin
  inherited;
  with dmCliente do
     begin
          GetPeriodoAnterior;
          PaPer.Valor:= PeriodoNumero;
     end;
     Refrescar;
end;

//DevEx (by am): Se deja menos espacio extra pues se removio el filtrado y se requiere que todo el grid se vea en la pantalla
procedure TPrenomina.ApplyMinWidth;
var
   i: Integer;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption);
          end;
     end;

end;

end.
