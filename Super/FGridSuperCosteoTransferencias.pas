unit FGridSuperCosteoTransferencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls,
  StdCtrls, ZetaNumero, Mask, ZetaFecha, ZetaDBGrid,
  ZetaHora, ZetaSmartLists, ZetaKeyCombo, ZetaEdit,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TGridSuperCosteoTransferencias = class(TBaseGridEdicion_DevEx)
    PanelCurso: TPanel;
    lbDestino: TLabel;
    CC_CODIGO: TZetaKeyLookup_DevEx;
    MotivoLbl: TLabel;
    TR_MOTIVO: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    TR_TEXTO: TZetaEdit;
    TipoAutLbl: TLabel;
    TR_TIPO: TZetaKeyCombo;
    Label1: TLabel;
    TR_APRUEBA: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CC_CODIGOExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OKClick(Sender: TObject);
    procedure ZetaDBGridTitleClick(Column: TColumn);
    procedure OK_DevExClick(Sender: TObject);
  private
    ResultData: OleVariant;
    procedure BuscaEmpleado;
    procedure AsignaDatosCosteo;
    procedure ValidaDatosCosteo;
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure EscribirCambios; override;
  public
  end;

var
  GridSuperCosteoTransferencias: TGridSuperCosteoTransferencias;

implementation

uses dCliente, DTablas, DSuper, DSistema, DGlobal, ZGlobalTress, ZetaCommonLists,ZetaCommonClasses, ZetaCommonTools,
     ZetaDialogo, ZetaBuscaEmpleado_DevEx;

{$R *.DFM}

procedure TGridSuperCosteoTransferencias.FormCreate(Sender: TObject);
var
   sNivel : string;
begin
     inherited;
     TipoValorActivo1 := stFecha;
     HelpContext := H_Registro_Transferencias_Supervisores;
     
     TR_TIPO.ItemIndex := 0;

     //CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     CC_CODIGO.LookupDataset := dmTablas.GetDataSetTransferencia;
     TR_APRUEBA.LookupDataset := dmSistema.cdsUsuariosLookUp;
     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;
     TR_TEXTO.MaxLength := K_ANCHO_DESCRIPCION;

     with Global do
     begin
          sNivel := Global.NombreCosteo;
          lbDestino.Caption := sNivel + ' Destino:';
     end;
end;

procedure TGridSuperCosteoTransferencias.Connect;
begin
     with dmTablas do
     begin
          GetDataSetTransferencia.Conectar;
          cdsMotivoTransfer.Conectar;
     end;
     dmSistema.cdsUsuariosLookUp.Conectar;
     with dmSuper do
     begin
          Datasource.Dataset :=  cdsCosteoTransferencias;
          ResultData := cdsCosteoTransferencias.Data;
          cdsCosteoTransferencias.EmptyDataset;
          cdsSuperCosteo.Conectar;
     end;
end;

procedure TGridSuperCosteoTransferencias.Buscar;
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

procedure TGridSuperCosteoTransferencias.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmSuper.cdsCosteoTransferencias do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

procedure TGridSuperCosteoTransferencias.ValidaDatosCosteo;

 procedure ValidaLookup( oControl: TZetaKeyLookup_DevEx; const sMensaje: string );
 begin
      if StrVacio( oControl.Llave ) then
      begin
           ActiveControl := oControl;
           DataBaseError( Format( 'No se ha especificado %s', [sMensaje]) );
      end
 end;

begin
     ValidaLookup( CC_CODIGO, Format( 'a cual %s se realizará la Transferencia', [Global.NombreCosteo])  );
     ValidaLookup( TR_APRUEBA, 'el usuario que aprobará la Transferencia' );
     //ValidaLookup( TR_MOTIVO, 'motivo de la Transferencia' );

     //Lista de Empleados vacia?
end;

procedure TGridSuperCosteoTransferencias.AsignaDatosCosteo;
begin
     with dmSuper do
     begin
          try
             with cdsCosteoTransferencias do
             begin
                  if State in [ dsEdit, dsInsert ] then
                     Post;
                  DisableControls;
                  try
                     First;
                     while not EOF do
                     begin
                          Edit;
                          FieldByName( 'CC_CODIGO' ).AsString := CC_CODIGO.Llave;
                          FieldByName( 'TR_TIPO' ).AsInteger := TR_TIPO.ItemIndex;
                          FieldByName( 'TR_MOTIVO' ).AsString := TR_MOTIVO.Llave;
                          FieldByName( 'TR_APRUEBA' ).AsString := TR_APRUEBA.Llave;
                          FieldByName( 'TR_TEXTO' ).AsString := TR_TEXTO.Text;
                          Next;
                     end;
                  finally
                     EnableControls;
                  end;
             end;
             GrabaTransferencias( cdsCosteoTransferencias, TRUE, FALSE );
          except
                on Error: Exception do
                     Application.HandleException( Error );
          end;
     end;
end;

procedure TGridSuperCosteoTransferencias.EscribirCambios;
begin
     ValidaDatosCosteo;
     AsignaDatosCosteo;
end;

procedure TGridSuperCosteoTransferencias.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := CC_CODIGO;
end;

procedure TGridSuperCosteoTransferencias.CC_CODIGOExit(Sender: TObject);
var
   iUsuarioDefault: Integer;
   sFiltro, sMensaje: string;
begin
     inherited;
     if StrLleno( CC_CODIGO.LLave ) then
     begin
          if dmSuper.FiltroUsuariosXCentroCosto( CC_CODIGO.LLave, iUsuarioDefault, sFiltro, sMensaje ) then
          begin
               with TR_APRUEBA do
               begin
                    Filtro := sFiltro;
                    Llave := IntToStr( iUsuarioDefault );
               end;
          end
          else
          begin
               ZError( Self.Caption, sMensaje, 0 );
               CC_CODIGO.SetFocus;
          end;
     end;
end;

procedure TGridSuperCosteoTransferencias.FormClose(Sender: TObject;    var Action: TCloseAction);
begin
     inherited;
     if ModalResult <> mrOk then
        //Si se cancela la pantalla, que la consulta quede con el dataset que tenia.
        dmSuper.cdsCosteoTransferencias.Data := ResultData;
end;

procedure TGridSuperCosteoTransferencias.OKClick(Sender: TObject);
begin
     inherited;
     ModalResult := mrOK;
end;

procedure TGridSuperCosteoTransferencias.ZetaDBGridTitleClick(Column: TColumn);
begin
     {No hace nada}
end;

procedure TGridSuperCosteoTransferencias.OK_DevExClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end;

end.
