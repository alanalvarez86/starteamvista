inherited RegistroBreak: TRegistroBreak
  Left = 350
  Top = 270
  Caption = 'Registro de Breaks'
  ClientHeight = 331
  ClientWidth = 446
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 295
    Width = 446
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 278
    end
    inherited Cancelar: TBitBtn
      Left = 363
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 446
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 120
    end
  end
  inherited PanelSuperior: TPanel
    Width = 446
    TabOrder = 1
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 152
    Width = 446
    Height = 143
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Del Empleado'
        Width = 330
        Visible = True
      end>
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 51
    Width = 446
    Height = 101
    Align = alTop
    TabOrder = 2
    object Fechalbl: TLabel
      Left = 82
      Top = 10
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object Horalbl: TLabel
      Left = 59
      Top = 32
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora Inicial:'
    end
    object Duracionlbl: TLabel
      Left = 237
      Top = 31
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Duraci'#243'n:'
    end
    object Arealbl: TLabel
      Left = 90
      Top = 76
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Area:'
    end
    object Label1: TLabel
      Left = 64
      Top = 54
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora Final:'
    end
    object Label2: TLabel
      Left = 330
      Top = 31
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'minutos'
    end
    object ZFecha: TZetaFecha
      Left = 118
      Top = 5
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '06/Jun/02'
      Valor = 37413.000000000000000000
    end
    object ZHora: TZetaHora
      Left = 118
      Top = 28
      Width = 42
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 1
      Tope = 48
      OnExit = ZHoraExit
    end
    object ZDuracion: TZetaNumero
      Left = 286
      Top = 27
      Width = 42
      Height = 21
      TabStop = False
      Color = clBtnFace
      Mascara = mnDias
      ReadOnly = True
      TabOrder = 4
      Text = '0'
    end
    object AreaKL: TZetaKeyLookup
      Left = 118
      Top = 72
      Width = 297
      Height = 21
      LookupDataset = dmLabor.cdsArea
      TabOrder = 3
      TabStop = True
      WidthLlave = 60
      OnExit = AreaKLExit
    end
    object HoraFinal: TZetaHora
      Left = 118
      Top = 50
      Width = 42
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 2
      Tope = 48
      OnExit = ZHoraExit
    end
  end
  inherited DataSource: TDataSource
    Left = 340
    Top = 217
  end
end
