unit DTablas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
{$ifdef DOS_CAPAS}
     DServerTablas,
{$else}
     Tablas_TLB,
{$endif}
  Db, DBClient, ZetaClientDataSet;

type
  TdmTablas = class(TDataModule)
    cdsNivel1: TZetaLookupDataSet;
    cdsNivel2: TZetaLookupDataSet;
    cdsNivel3: TZetaLookupDataSet;
    cdsNivel4: TZetaLookupDataSet;
    cdsNivel5: TZetaLookupDataSet;
    cdsNivel6: TZetaLookupDataSet;
    cdsNivel7: TZetaLookupDataSet;
    cdsNivel8: TZetaLookupDataSet;
    cdsNivel9: TZetaLookupDataSet;
    cdsIncidencias: TZetaLookupDataSet;
    cdsMotAuto: TZetaLookupDataSet;
    cdsMovKardex: TZetaLookupDataSet;
    cdsNivel10: TZetaLookupDataSet;
    cdsNivel11: TZetaLookupDataSet;
    cdsNivel12: TZetaLookupDataSet;
    cdsMotCheca: TZetaLookupDataSet;
    cdsMotivoTransfer: TZetaLookupDataSet;
    cdsTipoCursos: TZetaLookupDataSet;
    cdsClasifiCurso: TZetaLookupDataSet;
    procedure cdsTablaAlAdquirirDatos(Sender: TObject);
    procedure cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsMotChecaBeforePost(DataSet: TDataSet);{ACL170409}
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerTablas: TdmServerTablas;
    property ServerTablas: TdmServerTablas read GetServerTablas;
{$else}
    FServidor: IdmServerTablasDisp;
    function GetServerTablas: IdmServerTablasDisp;
    property ServerTablas: IdmServerTablasDisp read GetServerTablas;
{$endif}
  public
    { Public declarations }
    function EsTipoSistema( const sTipo : String ): Boolean;
    procedure SetLookupNames;
    function GetDataSetTransferencia(const lLevantaException: Boolean = TRUE): TZetaLookupDataset;
    function HayDataSetTransferencia( var sMensaje: String ): Boolean;
  end;
const
     K_SIN_CONFIG_COSTEO = 'El Nivel de Organigrama para Costeo no ha sido definido.';     

var
  dmTablas: TdmTablas;

implementation

uses DCliente,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.DFM}

{ TdmTablas }

{$ifdef DOS_CAPAS}
function TdmTablas.GetServerTablas: TdmServerTablas;
begin
     Result := DCliente.dmCliente.ServerTablas;
end;
{$else}
function TdmTablas.GetServerTablas: IdmServerTablasDisp;
begin
     Result := IdmServerTablasDisp( dmCliente.CreaServidor( CLASS_dmServerTablas, FServidor ) );
end;
{$endif}

procedure TdmTablas.cdsTablaAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerTablas.GetTabla( dmCliente.Empresa, Tag );
     end;
end;

function TdmTablas.EsTipoSistema(const sTipo: String): Boolean;
begin
     with cdsMovKardex do
     begin
          if Locate( 'TB_CODIGO', sTipo, [] ) then
             Result := zStrToBool( FieldByName( 'TB_SISTEMA' ).AsString )
          else
             Result := FALSE;
     end;
end;

procedure TdmTablas.SetLookupNames;
begin
     cdsNivel1.LookupName := Global.NombreNivel(1);
     cdsNivel2.LookupName := Global.NombreNivel(2);
     cdsNivel3.LookupName := Global.NombreNivel(3);
     cdsNivel4.LookupName := Global.NombreNivel(4);
     cdsNivel5.LookupName := Global.NombreNivel(5);
     cdsNivel6.LookupName := Global.NombreNivel(6);
     cdsNivel7.LookupName := Global.NombreNivel(7);
     cdsNivel8.LookupName := Global.NombreNivel(8);
     cdsNivel9.LookupName := Global.NombreNivel(9);
{$ifdef ACS}
     cdsNivel10.LookupName := Global.NombreNivel(10);
     cdsNivel11.LookupName := Global.NombreNivel(11);
     cdsNivel12.LookupName := Global.NombreNivel(12);
{$endif}
end;

procedure TdmTablas.cdsSuperLookupGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmTablas.cdsMotChecaBeforePost(DataSet: TDataSet);
const
     CODIGO = 0;
begin
     with DataSet do
     begin
          if Fields[ CODIGO ].IsNull then
             DB.DatabaseError( 'C�digo No Puede Quedar Vac�o' );
     end;
end;

function TdmTablas.GetDataSetTransferencia( const lLevantaException: Boolean ): TZetaLookupDataset;
begin
     case Global.GetGlobalInteger( K_GLOBAL_NIVEL_COSTEO ) of
          1: Result := cdsNivel1;
          2: Result := cdsNivel2;
          3: Result := cdsNivel3;
          4: Result := cdsNivel4;
          5: Result := cdsNivel5;
          6: Result := cdsNivel6;
          7: Result := cdsNivel7;
          8: Result := cdsNivel8;
          9: Result := cdsNivel9;
          {$ifdef ACS}
          10: Result := cdsNivel10;
          11: Result := cdsNivel11;
          12: Result := cdsNivel12;
          {$endif}
     else
         Result := nil;
     end;

     if (Result = NIL) and lLevantaException then
        raise Exception.Create( K_SIN_CONFIG_COSTEO );
end;

function TdmTablas.HayDataSetTransferencia( var sMensaje: String ): Boolean;
begin
     Result := Assigned(dmTablas.GetDataSetTransferencia( False ) );
     if ( not Result ) then
          sMensaje := K_SIN_CONFIG_COSTEO;
end;

end.
