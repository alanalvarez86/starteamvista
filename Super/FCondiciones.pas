unit FCondiciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, ZetaEdit, StdCtrls, Buttons, ExtCtrls,
  ZetaCommonLists, ZetaTipoEntidad, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, cxButtons, ZBaseDlgModal, ImgList, ZetaKeyLookup_DevEx;

type
  TCondiciones = class(TZetaDlgModal_DevEx)
    FiltrosGB: TGroupBox;
    sFiltroLBL: TLabel;
    sCondicionLBl: TLabel;
    EFiltro: TMemo;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    RBTodos: TRadioButton;
    RBRango: TRadioButton;
    RBLista: TRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    ECondicion: TZetaKeyLookup_DevEx;
    BLista_DevEx: TcxButton;
    BInicial_DevEx: TcxButton;
    BFinal_DevEx: TcxButton;
    BAgregaCampo_DevEx: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure TipoRangoClick(Sender: TObject);
    //procedure BInicialClick(Sender: TObject);
    //procedure BFinalClick(Sender: TObject);
    //procedure BAgregaCampoClick(Sender: TObject);
    procedure ComponenteChange(Sender: TObject);
    //procedure CancelarClick(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BLista_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure BInicial_DevExClick(Sender: TObject);
    procedure BFinal_DevExClick(Sender: TObject);
    procedure BAgregaCampo_DevExClick(Sender: TObject);
  private
    FHuboCambios: Boolean;
    FTipoRangoActual : eTipoRangoActivo;
    procedure SetHuboCambios( const Value : Boolean );
    property HuboCambios: Boolean read FHuboCambios write SetHuboCambios;
    procedure ChecaHuboCambios;
    procedure SetTipoRangoActual( const Value : eTipoRangoActivo );
    property TipoRangoActual: eTipoRangoActivo read FTipoRangoActual write SetTipoRangoActual;
    function BuscaEmpleado(const lConcatena: Boolean; const sLlave: String): String;
    procedure InicializaControles;
    procedure SetControles;
    function GrabaCambios: Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Condiciones: TCondiciones;

implementation

uses dSuper, dCatalogos, ZetaCommonClasses, ZetaCommonTools, ZetaBuscaEmpleado_DevEx, ZConstruyeFormula;

{$R *.DFM}

procedure TCondiciones.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsCondiciones.Conectar;
     InicializaControles;
end;

{ *************** METODOS Y FUNCIONES DE CONDICIONES ******************* }

function TCondiciones.BuscaEmpleado(const lConcatena: Boolean; const sLlave: String): String;
var
   sKey, sDescripcion: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescripcion ) then
     begin
          if lConcatena and ZetaCommonTools.StrLleno( Text ) then
             Result := sLlave + ',' + sKey
          else
              Result := sKey;
     end;
end;

procedure TCondiciones.InicializaControles;
begin
     with dmSuper do
     begin
          with RangoLista do
          begin
               RBTodos.Checked := ( TipoRango = raTodos );
               RBRango.Checked := ( TipoRango = raRango );
               RBLista.Checked := ( TipoRango = raLista );
               TipoRangoActual := TipoRango;
               EInicial.Text := Inicial;
               EFinal.Text   := Final;
               ELista.Text   := Lista;
          end;
          ECondicion.Llave := Condicion;
          EFiltro.Text := Filtro;
     end;
     HuboCambios:= FALSE;
end;

procedure TCondiciones.SetTipoRangoActual(const Value: eTipoRangoActivo);
var
   lEnabled: Boolean;
begin
     if FTipoRangoActual <> Value then
     begin
          lEnabled := ( Value = raRango );
          lbInicial.Enabled := lEnabled;
          lbFinal.Enabled := lEnabled;
          EInicial.Enabled := lEnabled;
          EFinal.Enabled := lEnabled;
          //bInicial.Enabled := lEnabled;
          bInicial_DevEx.Enabled := lEnabled;
          //bFinal.Enabled := lEnabled;
          bFinal_DevEx.Enabled := lEnabled;
          lEnabled := ( Value = raLista );
          ELista.Enabled := lEnabled;
          BLista_DevEx.Enabled := lEnabled;
          FTipoRangoActual := Value;
     end;
end;

procedure TCondiciones.SetControles;
begin
     //OK.Enabled := HuboCambios;
     OK_DevEx.Enabled := HuboCambios;
     if HuboCambios then
     begin
          with Cancelar_DevEx do
          begin
               //Kind := bkCancel;
               OptionsImage.ImageIndex := 0;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar_DevEx do
          begin
               //Kind := bkClose;
               OptionsImage.ImageIndex := 2;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Application.ProcessMessages;
end;

procedure TCondiciones.ChecaHuboCambios;
begin
     with dmSuper do
     begin
          with RangoLista do
          begin
               HuboCambios := ( ( TipoRangoActual <> TipoRango ) or
                                ( EInicial.Text <> Inicial ) or
                                ( EFinal.Text <> Final ) or
                                ( ELista.Text <> Lista ) or
                                ( ECondicion.LLave <> Condicion ) or
                                ( EFiltro.Text <> Filtro ) );
          end;
     end;
end;

procedure TCondiciones.SetHuboCambios(const Value: Boolean);
begin
     if FHuboCambios <> Value then
     begin
          FHuboCambios:= Value;
          SetControles;
     end;
end;

function TCondiciones.GrabaCambios: Boolean;
var
   OldRangoLista: TRangoLista;
   OldCondicion, OldFiltro : String;
begin
     Result := FALSE;
     OldRangoLista := TRangoLista.Create;
     try
        with dmSuper do
        begin
             // Respalda los Actuales
             with RangoLista do
             begin
                  OldRangoLista.TipoRango := TipoRango;
                  OldRangoLista.Inicial   := Inicial;
                  OldRangoLista.Final     := Final;
                  OldRangoLista.Lista     := Lista;
             end;
             OldCondicion  := Condicion;
             OldFiltro        := Filtro;
             // Asigna nuevos Filtros
             with RangoLista do
             begin
                  TipoRango := TipoRangoActual;
                  Inicial   := EInicial.Text;
                  Final     := EFinal.Text;
                  Lista     := ELista.Text;
             end;
             Condicion := ECondicion.Llave;
             Filtro := EFiltro.Text;
             try
                RefrescaMisEmpleados;
                Result := TRUE;
             except
                with RangoLista do
                begin
                     TipoRango := OldRangoLista.TipoRango;
                     Inicial   := OldRangoLista.Inicial;
                     Final     := OldRangoLista.Final;
                     Lista     := OldRangoLista.Lista;
                end;
                Condicion  := OldCondicion;
                Filtro     := OldFiltro;
                raise;
             end;
        end;
     finally
        FreeAndNil( OldRangoLista );
     end;
end;

{ *************** EVENTOS DE COMPONENTES ******************* }

procedure TCondiciones.TipoRangoClick(Sender: TObject);
begin
     TipoRangoActual := eTipoRangoActivo( TComponent( Sender ).Tag );
     ChecaHuboCambios;
end;

{procedure TCondiciones.BInicialClick(Sender: TObject);
begin
     EInicial.Text := BuscaEmpleado( False, EInicial.Text );
end;  }

{procedure TCondiciones.BFinalClick(Sender: TObject);
begin
     EFinal.Text := BuscaEmpleado( False, EFinal.Text );
end;}

{procedure TCondiciones.BAgregaCampoClick(Sender: TObject);
begin
     EFiltro.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, EFiltro.Text, EFiltro.SelStart, evBase );
end;}

procedure TCondiciones.ComponenteChange(Sender: TObject);
begin
     ChecaHuboCambios;
end;

{procedure TCondiciones.OKClick(Sender: TObject);
begin
     if GrabaCambios then
     begin
          dmSuper.NoPreguntarFiltro := FALSE;
          Close;
     end;
end;}

{procedure TCondiciones.CancelarClick(Sender: TObject);
begin
     if HuboCambios then
        InicializaControles
     else
        Close;
end; }

procedure TCondiciones.FormCreate(Sender: TObject);
begin
     inherited;
     eCondicion.LookupDataset := dmCatalogos.cdsCondiciones;
end;

procedure TCondiciones.BLista_DevExClick(Sender: TObject);
begin
  inherited;
   ELista.Text := BuscaEmpleado( True, ELista.Text );
end;

procedure TCondiciones.OK_DevExClick(Sender: TObject);
begin
  inherited;
  if GrabaCambios then
     begin
          dmSuper.NoPreguntarFiltro := FALSE;
          Close;
     end;
end;

procedure TCondiciones.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  if HuboCambios then
        InicializaControles
     else
        Close;
end;

procedure TCondiciones.BInicial_DevExClick(Sender: TObject);
begin
  inherited;
  EInicial.Text := BuscaEmpleado( False, EInicial.Text );
end;

procedure TCondiciones.BFinal_DevExClick(Sender: TObject);
begin
  inherited;
   EFinal.Text := BuscaEmpleado( False, EFinal.Text );
end;

procedure TCondiciones.BAgregaCampo_DevExClick(Sender: TObject);
begin
  inherited;
  EFiltro.Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, EFiltro.Text, EFiltro.SelStart, evBase );
end;

end.
