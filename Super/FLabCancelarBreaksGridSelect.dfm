inherited LabCancelarBreaksGridSelect: TLabCancelarBreaksGridSelect
  Left = 557
  Top = 275
  Width = 502
  Caption = 'Seleccione Los Breaks Deseados'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 486
    inherited OK_DevEx: TcxButton
      Left = 322
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 400
    end
  end
  inherited PanelSuperior: TPanel
    Width = 486
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 486
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Completo'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object WK_PRE_CAL: TcxGridDBColumn
        Caption = 'Duraci'#243'n'
        DataBinding.FieldName = 'WK_PRE_CAL'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
