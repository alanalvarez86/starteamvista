unit FKardexDiario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,} ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TKardexDiario = class(TBaseGridLectura_DevEx)
    KA_FECHA: TcxGridDBColumn;
    CB_AREA: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    KA_HORA: TcxGridDBColumn;
    KA_HOR_FIN: TcxGridDBColumn;
    KA_TIEMPO: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    {procedure GridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState); }
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
  public
    { Public declarations }
  end;

var
  KardexDiario: TKardexDiario;

implementation

{$R *.DFM}

uses
    dLabor,
    dSuper,
    dGlobal,
    ZAccesosMgr,
    ZAccesosTress,
    ZetaLaborTools,
    ZGlobalTress,
    ZetaCommonClasses;

{ TKardexDiario }

procedure TKardexDiario.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 81000;
end;

procedure TKardexDiario.Connect;
begin
     dmSuper.cdsDatosEmpleado.Conectar;
     with dmLabor do
     begin
          cdsKardexDiario.Conectar;
          Datasource.DataSet := cdsKArdexDiario;
     end;
     DoBestFit;
end;

procedure TKardexDiario.FormShow(Sender: TObject);
begin
     CB_AREA.Caption :=  Format( CB_AREA.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ]);
     ApplyMinWidth;
     inherited;
     //Grid.Columns[ 1 ].Title.Caption := Format( Grid.Columns[ 1 ].Title.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ]);

end;

procedure TKardexDiario.Agregar;
begin
     { Agregar con el ClientDataSet de Areas de Labor }
     dmLabor.cdsKardexDiario.Agregar;     
end;

procedure TKardexDiario.Borrar;
begin
     { Borrar sobre el ClientDataSet de Areas de Labor }
     dmLabor.cdsKardexDiario.Borrar;
     DoBestFit;
end;

procedure TKardexDiario.Modificar;
begin
     { Modificar sobre el ClientDataSet de Areas de Labor }
     dmLabor.cdsKardexDiario.Modificar
end;

function TKardexDiario.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
end;

function TKardexDiario.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := ( DataSource.DataSet.FieldByName( 'CH_TIPO' ).AsInteger = 0 );
          if not Result then
             sMensaje := '� No Se Pueden Borrar Checadas De Asistencia !';
     end;
end;

function TKardexDiario.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := ( DataSource.DataSet.FieldByName( 'CH_TIPO' ).AsInteger = 0 );
          if not Result then
             sMensaje := '� No Se Pueden Modificar Checadas De Asistencia !';
     end;
end;

procedure TKardexDiario.Refresh;
begin
     dmLabor.cdsKardexDiario.Refrescar;
     DoBestFit;
end;

{procedure TKardexDiario.GridDrawColumnCell(Sender: TObject;const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     begin
          with Grid do
          begin
               if ( gdSelected in State )  then
               begin
                    with Canvas do
                    begin
                         Font.Color := clWhite;
                         Brush.Color := clNavy;
                    end;
               end
               else
               begin
                    with Canvas do
                    begin
                         Brush.Color := clWhite;
                         if ( DataSource.Dataset.FieldByName( 'CH_TIPO' ).AsInteger <> 0 ) then
                            Font.Color := clBlue
                         else
                             Font.Color := Grid.Font.Color;
                    end;
               end;
               DefaultDrawDataCell( Rect, Column.Field, State );
          end;
     end;
end; }

end.
