unit FLayoutProduccion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, ComCtrls, DB, ExtCtrls,DLabor,FMapasProduccion,
  //GIFImage,
  FTressShell,DSuper,ZetaDialogo,DCliente, ZAccesosTress,Menus,
  Buttons,ZetaCommonClasses, StdCtrls,FHintLayout, ZetaKeyLookup,
  ZetaDBTextBox, //fcpanel, fctrackbar,
  jpeg, gtClasses3, gtCstDocEng,
  gtCstPlnEng, gtCstPDFEng, gtExPDFEng, gtPDFEng,Printers, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, cxButtons, ImgList, dxZoomTrackBar,
  ZetaKeyLookup_DevEx, cxControls, cxContainer, cxEdit, cxTrackBar, cxLabel;

type
  TMyHintWindow = class(THintWindow)
       constructor Create(AOwner: TComponent); override;
end;

type
  TLayoutProduccion = class(TBaseConsulta)
    mnAsignar: TPopupMenu;
    AsignarEmpleado1: TMenuItem;
    Panel1: TPanel;
    GridLayout: TDrawGrid;
    imgNoCertificado: TImage;
    imgEmpleado: TImage;
    imgAusente: TImage;
    imgCertificado: TImage;
    mnuLimpiarTodas: TMenuItem;
    Layout: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    LY_AREA: TZetaDBTextBox;
    Label2: TLabel;
    DataSource1: TDataSource;
    mnuLimpiarAsignacion: TMenuItem;
    mnuEmpNomEmp1: TMenuItem;
    Panel2: TPanel;
   // trkZoom: TfcTrackBar;
    imgMapaAux: TImage;
    gtPDFGenerador: TgtPDFEngine;
    btnAsignar_DevEx: TcxButton;
    btnUbicaEmpleado_DevEx: TcxButton;
    btnExportar_DevEx: TcxButton;
    ZoomIn: TcxButton;
    ZoomOut: TcxButton;
    cxImage_AsignaEmpleados: TcxImageList;
    cxImageList_PopUp: TcxImageList;
    trkZoom: TdxZoomTrackBar;
    LblPorcentajeZoom: TcxLabel;
    procedure GridLayoutDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GridLayoutSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure GridLayoutDblClick(Sender: TObject);
    procedure mnuLimpiarTodasClick(Sender: TObject);
    procedure mnAsignarPopup(Sender: TObject);
    procedure AsignarEmpleado1Click(Sender: TObject);
    procedure LayoutValidKey(Sender: TObject);
    procedure mnuLimpiarAsignacionClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure GridLayoutMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure GridLayoutMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure GridLayoutKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridLayoutMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure mnuEmpNomEmp1Click(Sender: TObject);
    procedure trkZoomChange(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure btnUbicaEmpleado_DevExClick(Sender: TObject);
    procedure btnAsignar_DevExClick(Sender: TObject);
    procedure btnExportar_DevExClick(Sender: TObject);
    procedure ZoomInClick(Sender: TObject);
    procedure ZoomOutClick(Sender: TObject);
  private
    procedure AsignarEmpleadoMaquina(Empleado: Integer; Silla:TSilla);
    procedure CargarAsignacionesEmpleados;
    function EmpleadoCertificadoSilla(Silla: TSilla): Boolean;
    function CertificadoSilla(Empleado:Integer;Silla:TSilla):Boolean;
    procedure RefrescarLayout;
    procedure RefrescarListaEmpleados;
    { Private declarations }
  public
    { Public declarations }
    FIndX:Integer;
    FIndY:Integer;
    FLastCellHint:string;
    FAsignando:Boolean;
    CoordX :Integer;
    CoordY :Integer;

    procedure CargarMatrizStatusEmpleados;
    procedure SeleccionarEmpleado(const EmpleadoSelecc :Integer);
    procedure LimpiarAsignacionEmpleadoAnterior(Empleado: Integer;oSilla:TSilla);
    procedure RePintarAsignacion;


  protected
     procedure Connect;override;
     procedure Refresh;override;
     function PuedeAgregar(var sMensaje: String): Boolean;override;
     function PuedeBorrar(var sMensaje: String): Boolean; override;
     function PuedeModificar(var sMensaje: String): Boolean;override;
  end;

var
  LayoutProduccion: TLayoutProduccion;

implementation

   uses dGlobal,ZetaCommonTools,DConsultas,FConsultaEmpleados,FMaquinaDetalle,ZAccesosMgr,FAsignandoEmpleadosCertif;


{$R *.dfm}

{ TLayoutProduccion }

procedure TLayoutProduccion.CargarMatrizStatusEmpleados;
var
   i,j:Integer;
begin
     with dmSuper do
     begin
          for i := 0 to K_MAX_COL do
          begin
               for j := 0 to K_MAX_ROW do
               begin
                    if ( ( GridEstados[i][j] <> nil )and (GridEstados[i][j].Estado in [Silla,Certificado,NoCertificado,Ausente] ) and ( TSilla(GridEstados[i][j]).Empleado <> 0 ) ) then
                    begin
                         if cdsListaEmpleados.Locate('CB_CODIGO',TSilla(GridEstados[i][j]).Empleado,[]) then
                         begin
                              if cdsListaEmpleados.FieldByName('CHECADAS').AsInteger > 0 then
                              begin
                                   if EmpleadoCertificadoSilla(TSilla(GridEstados[i][j])) then
                                   begin
                                        GridEstados[i][j].Estado := Certificado;
                                        GridEstados[i][j].Imagen := imgCertificado.Picture;
                                   end
                                   else
                                   begin
                                        GridEstados[i][j].Estado := NoCertificado;
                                        GridEstados[i][j].Imagen := imgNoCertificado.Picture;
                                   end;
                              end
                              else
                              begin
                                   GridEstados[i][j].Estado := Ausente;
                                   GridEstados[i][j].Imagen := imgAusente.Picture;
                              end;
                         end
                         else
                         begin
                              GridEstados[i][j].Estado := Silla; 
                              GridEstados[i][j].Imagen := imgEmpleado.Picture;
                         end;
                    end;
               end;
          end;
     end;
end;

function TLayoutProduccion.EmpleadoCertificadoSilla(Silla:TSilla):Boolean;
var
   i:Integer;
begin
     Result := False;
     for i := 0 to Silla.Maquinas.Count -1  do
     begin
           Result := dmLabor.EstaCertificado(Silla.Empleado,TMaquina(Silla.Maquinas[i]).Certificaciones);
           if not Result then
              break;
     end;
end;

procedure TLayoutProduccion.CargarAsignacionesEmpleados;
var
   i,j:Integer;
begin
     //Cargar las asignaciones hechas en el kardex del empleado
     dmLabor.cdsKarEmpMaq.Refrescar;
     with dmLabor.cdsKarEmpMaq do
     begin
          First;
          while not eof do
          begin
               for i := 0 to K_MAX_COL do
               begin
                    for j := 0 to K_MAX_ROW do
                    begin
                         if ( ( GridEstados[i][j]<> nil) and ( GridEstados[i][j].Estado = Silla ) ) then
                         begin
                              if ( TSilla(GridEstados[i][j]).Codigo =  FieldByName('SILLA').AsString )then
                              begin
                                   TSilla(GridEstados[i][j]).Empleado := FieldByName('CB_CODIGO').AsInteger;
                                   TSilla(GridEstados[i][j]).Hora := FieldByName('HORA').AsString;
                                   //HoraAnt :=
                              end;
                         end;
                    end
               end;
               Next;
          end;
     end;
end;

procedure TLayoutProduccion.Connect;
begin
     inherited;
     //stand by
     with dmLabor do
     begin
          cdsLayoutsAsignados.Conectar;
          Layout.Llave := cdsLayoutsAsignados.FieldByName('LY_CODIGO').AsString;
          LayoutActual.Codigo := cdsLayoutsAsignados.FieldByName('LY_CODIGO').AsString;
     end;
     RefrescarLayout;
     
end;

function TLayoutProduccion.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          sMensaje := 'No se puede Borrar';
          Result := False;

     end;

end;

function TLayoutProduccion.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          sMensaje := 'No se puede Modificar';
          Result := False;
     end;
end;

function TLayoutProduccion.PuedeAgregar(var sMensaje: String ): Boolean;
begin
      Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          sMensaje := 'No se puede Agregar';
          Result := False;
     end;
end;


procedure TLayoutProduccion.RefrescarLayout;
var
   oCursor:TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     LimpiarLayout; 
     try
        with dmLabor do
        begin
             dmSuper.RefrescarFormaActiva := FALSE;
             cdsLayMaquinas.Refrescar;
             cdsMaquinas.Conectar;
             cdsCertificEmpleados.Refrescar;

             InicializarLayout;

             CargarMaquinas(cdsLayMaquinas,cdsMaquinas);
             CargarSillas(cdsSillasLayout,cdsSillasMaquina,imgEmpleado.Picture );
             CargarAsignacionesEmpleados;
             CargarMatrizStatusEmpleados;
             GridLayout.Repaint;
             RefrescarListaEmpleados;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     tressshell.GridEmp.BeginUpdate();
     tressshell.GridEmpDBTableView1.DataController.UpdateItems(False);
     tressshell.GridEmp.EndUpdate;
     
end;

procedure TLayoutProduccion.RefrescarListaEmpleados;
begin

end;

procedure TLayoutProduccion.GridLayoutDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
     inherited;
     if GridEstados[ACol][ARow] <> nil then
     begin
           if GridEstados[ACol][ARow].Estado = Libre then
           begin
                GridLayout.Canvas.Brush.Color := clWhite;
                GridLayout.Canvas.FillRect(Rect)
           end
           else if (GridEstados[ACol][ARow].Estado in [Maquina,Silla,Ausente,Certificado,NoCertificado] ) then
           begin
                GridLayout.Canvas.StretchDraw(Rect,GridEstados[ACol][ARow].Imagen.Graphic );
           end;

     end;
end;

procedure TLayoutProduccion.GridLayoutSelectCell(Sender: TObject; ACol,ARow: Integer; var CanSelect: Boolean);
var
   EmpleadoAnt,posx,posy:Integer;
   Lista:TStrings;
   sMaquinaAnterior:string;

procedure RevisarAsignacion;
begin
     if ( GridEstados[ACol][ARow].Estado in [ Ausente,NoCertificado,Certificado] )then
     begin
          if ZConfirm(Caption,Format('Desea asignar al empleado: %d en el lugar de %d',[dmSuper.EmpleadoNumero,TSilla(GridEstados[ACol][ARow]).Empleado]),0,mbYes )then
          begin
               EmpleadoAnt:= TSilla(GridEstados[ACol][ARow]).Empleado;
               AsignarEmpleadoMaquina(dmSuper.EmpleadoNumero,TSilla(GridEstados[ACol][ARow]));
               LimpiarAsignacionEmpleadoAnterior(EmpleadoAnt, TSilla(GridEstados[posx][posy]));
          end;
     end
     else
     begin
          if EmpleadoYaOcupado(dmSuper.EmpleadoNumero)then
            LimpiarAsignacionEmpleadoAnterior(dmSuper.EmpleadoNumero,TSilla(GridEstados[posx][posy]));
          AsignarEmpleadoMaquina(dmSuper.EmpleadoNumero,TSilla(GridEstados[ACol][ARow]));
     end;
     RePintarAsignacion;
end;

begin
     inherited;
     posx:=0;
     posy:=0;
     if GridEstados[ACol][ARow] = nil then
        GridEstados[ACol][ARow] := TElemento.Crear;

     if  GridEstados[ACol][ARow].Estado = Maquina then
     begin
          with dmLabor do
          begin
               cdsMaquinas.Locate('MQ_CODIGO',GridEstados[ACol][ARow].Codigo,[]);
               GetCertificaciones(GridEstados[ACol][ARow].Codigo);
          end;

          if ( MaquinaDetalle = nil ) then
               MaquinaDetalle := TMaquinaDetalle.Create( Application );
          MaquinaDetalle.ShowModal;
     end
     else
     begin
          if FAsignando then
          begin
               if CheckDerecho(D_LAY_PRODUCCION,K_DERECHO_ALTA)then
               begin
                    if GridEstados[ACol][ARow].Estado <> Libre then
                    begin
                         try
                            GridLayout.Enabled := False;

                         if not EmpleadoYaOcupado(dmSuper.EmpleadoNumero)then
                         begin
                              if CertificadoSilla(dmSuper.EmpleadoNumero,TSilla(GridEstados[ACol][ARow]))then
                              begin
                                   RevisarAsignacion;
                              end
                              else
                              begin
                                   if ZConfirm(Self.Caption,Format('El empleado: %d no est� certificado '+'� Desea asignarlo a %s: %s ?',[dmSuper.EmpleadoNumero,dmLabor.GetMaquinaGlobal,TMaquina(TSilla(GridEstados[ACol][ARow]).Maquinas[0]).Codigo]),0,mbYes ) then
                                   begin
                                        RevisarAsignacion;
                                   end;
                              end;
                         end
                         else
                         begin
                              Lista := TStringList.Create;
                              try
                                 with dmLabor.cdsKarEmpMaq do
                                 begin
                                      if Locate('CB_CODIGO',dmSuper.EmpleadoNumero,[])THEN
                                      begin
                                           Lista.CommaText := FieldByName('SILLA').AsString;
                                           posx := StrToInt(Lista[0]);
                                           posy := StrToInt(Lista[1]);
                                           if TSilla(GridEstados[posx][posy]).Maquinas.Count > 0 then
                                              sMaquinaAnterior := TMaquina(TSilla(GridEstados[posx][posy]).Maquinas[0]).Codigo;
                                      end
                                 end;
                              finally
                                     Lista.Free;
                              end;
                              if (posx = ACol) and (posy = ARow) then
                              begin
                                   ZWarning(sELF.Caption,Format('El Empleado : %d ya a est� asignado a %s:%s',[dmSuper.EmpleadoNumero,dmLabor.GetMaquinaGlobal,sMaquinaAnterior ]),0,mbOk );
                              end
                              else
                              begin
                                   if ZConfirm(Self.Caption,Format('El empleado: %d est� asignado a %s:%s '+'� Desea asignarlo a %s : %s ?',[dmSuper.EmpleadoNumero,dmLabor.GetMaquinaGlobal,sMaquinaAnterior,dmLabor.GetMaquinaGlobal,TMaquina(TSilla(GridEstados[ACol][ARow]).Maquinas[0]).Codigo]),0,mbYes ) then
                                   begin

                                        if CertificadoSilla(dmSuper.EmpleadoNumero,TSilla(GridEstados[ACol][ARow]))then
                                        begin
                                             RevisarAsignacion;
                                        end
                                        else
                                        begin
                                             if ZConfirm(Self.Caption,Format('El empleado: %d no est� certificado '+'� Desea asignarlo a %s: %s ?',[dmSuper.EmpleadoNumero,dmLabor.GetMaquinaGlobal,TMaquina(TSilla(GridEstados[ACol][ARow]).Maquinas[0]).Codigo]),0,mbYes ) then
                                             begin
                                                  RevisarAsignacion;
                                             end;
                                        end;
                                   end;
                              end;

                         end;
                         Finally
                               GridLayout.Enabled := True;
                         end;
                         //RefrescarLayout;
                    end;
               end
               else
                   ZWarning(Caption,Format('No tiene derechos para Asignar Empleados a %s',[dmLabor.GetMaquinaGlobal]),0,mbOK);
          end
          else
          begin
               if GridEstados[ACol][ARow].Estado in [ Ausente,Certificado,NoCertificado ] then
               begin
                    dmSuper.cdsEmpleados.Locate('CB_CODIGO',TSilla(GridEstados[ACol][ARow]).Empleado,[]);
                    dmCliente.SetEmpleadoNumero( TSilla(GridEstados[ACol][ARow]).Empleado );
                    if TressShell.VerificaDerechoConsulta( D_SUPER_DATOS_EMPLEADOS ) then
                    begin
                         if ( ConsultaEmpleados = nil ) then
                                 ConsultaEmpleados := TConsultaEmpleados.Create( Application );
                              ConsultaEmpleados.ShowModal;
                    end;
               end;
          end;
     end;
     GridLayout.Repaint;  
end;

procedure TLayoutProduccion.AsignarEmpleadoMaquina(Empleado:Integer;Silla:TSilla);
begin
     Silla.Empleado := Empleado;
     dmLabor.AsignarEmpleado(Empleado,Silla.Codigo);
end;

function TLayoutProduccion.CertificadoSilla(Empleado:Integer;Silla:TSilla):Boolean;
var
   i:Integer;
begin
     Result := True;
     for i := 0 to Silla.Maquinas.Count - 1 do
     begin
          if Silla.Maquinas[i] <> nil then
          begin
               Result := dmLabor.EstaCertificado(Empleado,TMaquina(Silla.Maquinas[i]).Certificaciones);
               if not Result then
                  Break;
          end;
     end;
end;

procedure TLayoutProduccion.LimpiarAsignacionEmpleadoAnterior(Empleado:Integer;oSilla:TSilla);
begin
     dmLabor.AsignarEmpleado(Empleado,VACIO);
     if oSilla <> nil then
     begin
          with oSilla do
          begin
              Empleado := -1;
              Estado := Silla;
              //if Imagen <> nil then
                // Imagen := imgEmpleado.Picture;
          end;
     end;
end;


procedure TLayoutProduccion.Refresh;
begin
     inherited;
     with dmLabor do
     begin
          cdsCertificEmpleados.Refrescar;
     end;
     RefrescarLayout;
end;

procedure TLayoutProduccion.RePintarAsignacion;
begin
     CargarAsignacionesEmpleados;
     CargarMatrizStatusEmpleados;
    tressshell.GridEmp.BeginUpdate();
    tressshell.GridEmpDBTableView1.DataController.UpdateItems(False);
     tressshell.GridEmp.EndUpdate;

     GridLayout.Repaint;
end;
{ TMyHintWindow }

constructor TMyHintWindow.Create(AOwner: TComponent);
begin
     inherited Create(AOwner);
     with Canvas.Font do
     begin
        Name := 'Arial';
        Size := Size + 5;
        Style := [fsBold];
     end;
end;

procedure TLayoutProduccion.FormCreate(Sender: TObject);
begin
     inherited;
     dmLabor.cdsLayoutsAsignados.ReadOnly := True;
     Layout.LookupDataset := dmLabor.cdsLayoutsAsignados;
     DataSource1.DataSet := dmLabor.cdsLayoutsAsignados;

     HelpContext := H_LAYOUT_PRODUCCION;  //act. de HC, nueva
end;

procedure TLayoutProduccion.GridLayoutDblClick(Sender: TObject);
begin
     inherited;
     if ((FIndX > 0) and (FIndY > 0 ))then
     begin
          FIndX := 0
     end;
end;


procedure TLayoutProduccion.mnuLimpiarTodasClick(Sender: TObject);
var
   iIndX,iIndY:Integer;
   Lista :TStrings;
begin
     inherited;
     //Preguntar ....
     //dmLabor.cdsKarEmpMaq.Refrescar;

     with dmLabor.cdsKarEmpMaq do
     begin
          First;
          while not eof do
          begin
               dmLabor.AsignarEmpleado(FieldByName('CB_CODIGO').AsInteger,VACIO);
               Lista := TStringList.Create;
               try
                  if StrLleno(FieldByName('SILLA').AsString) then
                  begin
                       Lista.CommaText := FieldByName('SILLA').AsString;
                       iIndX := StrToInt(Lista[0]);
                       iIndY := StrToInt(Lista[1]);
                       TSilla(GridEstados[iIndX][iIndY]).Empleado := -1;
                       GridEstados[iIndX][iIndY].Estado := Silla;
                       GridEstados[iIndX][iIndY].Imagen := imgEmpleado.Picture;
                  end;
               Finally
                      Lista.Free;
               end;
               Next;
          end;
          RePintarAsignacion; 
     end;
end;

procedure TLayoutProduccion.mnAsignarPopup(Sender: TObject);
var
   HayEmpleado:Boolean;
begin
     inherited;
     HayEmpleado:= False;

     if GridEstados[CoordX][CoordY] = nil THEN
             GridEstados[CoordX][CoordY] := TElemento.Crear;

     if FAsignando then
     begin
          AsignarEmpleado1.Visible :=( ( CheckDerecho(D_LAY_PRODUCCION,K_DERECHO_ALTA ) )and ( GridEstados[CoordX][CoordY].Estado in [Silla,Ausente,Certificado,NoCertificado]));
          mnuLimpiarTodas.Visible := GridEstados[CoordX][CoordY].Estado in [Libre];
          if GridEstados[CoordX][CoordY].Estado in [Ausente,Certificado,NoCertificado] then
             HayEmpleado := TSilla(GridEstados[CoordX][CoordY]).Empleado > 0;

          mnuLimpiarAsignacion.Visible := AsignarEmpleado1.Visible and HayEmpleado;
     end
     else
     begin
          AsignarEmpleado1.Visible := FAsignando;
          mnuLimpiarTodas.Visible := FAsignando;
          mnuLimpiarAsignacion.Visible := mnuLimpiarTodas.Visible;
          mnuEmpNomEmp1.Visible := mnuLimpiarTodas.Visible;
     end;

     mnuEmpNomEmp1.Visible := HayEmpleado;
     if mnuEmpNomEmp1.Visible then
     begin
          dmSuper.cdsListaEmpleados.Locate('CB_CODIGO',TSilla(GridEstados[CoordX][CoordY]).Empleado,[]);
          mnuEmpNomEmp1.Caption := Format( '%d - %s',[TSilla(GridEstados[CoordX][CoordY]).Empleado,dmSuper.cdsListaEmpleados.FieldByName('PRETTYNAME').AsString]);
     end;
end;

procedure TLayoutProduccion.AsignarEmpleado1Click(Sender: TObject);
begin
     inherited;
     AsignandoEmpleados :=  TAsignandoEmpleados.Create(Self);
     if AsignandoEmpleados.FiltrarEmpleados(TSilla(GridEstados[CoordX][CoordY]))then
     begin
          AsignandoEmpleados.ShowModal;
          RePintarAsignacion;
     end
     else
         ZWarning('Supervisores',Format('No existen empleados certificados ',[dmLabor.cdsMaquinas.FieldByName('MQ_CODIGO').AsString]),0,mbOk);
end;

procedure TLayoutProduccion.LayoutValidKey(Sender: TObject);
begin
     inherited;
     with dmLabor do
     begin
          LayoutActual.Codigo  := cdsLayoutsAsignados.FieldByName('LY_CODIGO').AsString;
          LY_AREA.Caption := cdsLayoutsAsignados.FieldByName('LY_AREA_TXT').AsString;
     end;
     RefrescarLayout;
     SendMessage(GridLayout.Handle, WM_VSCROLL, SB_TOP,0);
     SendMessage(GridLayout.Handle, WM_HSCROLL, SB_LEFT,0);
end;

procedure TLayoutProduccion.mnuLimpiarAsignacionClick(Sender: TObject);
begin
     inherited;
     dmLabor.AsignarEmpleado(TSilla(GridEstados[CoordX][CoordY]).Empleado,VACIO );
     GridEstados[CoordX][CoordY].Estado := Silla;
     GridEstados[CoordX][CoordY].Imagen := imgEmpleado.Picture;
     TSilla(GridEstados[CoordX][CoordY]).Empleado := -1;

     RePintarAsignacion;
end;

procedure TLayoutProduccion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     LimpiarLayout;
     FreeAndNil(GridEstados);
end;

procedure TLayoutProduccion.FormDestroy(Sender: TObject);
begin
     inherited;
      FreeAndNil(GridEstados);
end;

procedure TLayoutProduccion.GridLayoutMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
     if( ssCtrl in Shift )then
     begin
   //       trkZoom.Position := trkZoom.Position - trkZoom.Increment;
            trkZoom.Position := trkZoom.Position - trkZoom.Properties.TrackSize;
     end;
     Handled := True;
end;

procedure TLayoutProduccion.GridLayoutMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
     if( ssCtrl in Shift )then
     begin
     //     trkZoom.Position := trkZoom.Position + trkZoom.Increment;
     trkZoom.Position := trkZoom.Position + trkZoom.Properties.TrackSize;
     end;
     Handled := True;
end;

procedure TLayoutProduccion.FormShow(Sender: TObject);
var
   sTitulo:String;
const
     AnchoImagen=24;
begin
     inherited;
     btnAsignar_DevEx.Enabled := CheckDerecho(D_LAY_PRODUCCION,K_DERECHO_ALTA);
     sTitulo := Format('Asignar Empleados a %s',[dmLabor.GetMaquinaGlobal]);
     btnAsignar_DevEx.Width := (Length(sTitulo)*6) + AnchoImagen;
     btnAsignar_DevEx.Caption := Format('Asignar Empleados a %s ',[dmLabor.GetMaquinaGlobal]);
     btnAsignar_DevEx.OptionsImage.ImageIndex := 0;
     btnAsignar_DevEx.Hint := 'Iniciar Asignaci�n de Empleados';

     FAsignando:= False;
end;

procedure TLayoutProduccion.GridLayoutKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     case Key of
       VK_PRIOR:Key:= 0;   // PgUp //
       VK_NEXT: Key:=0;   // PgDn //
       VK_UP: Key:=0;   //Up Arrow //
       VK_DOWN: Key:=0;   //Down Arrow //
       VK_RIGHT : Key := 0;
       VK_LEFT : Key := 0;
       VK_HOME : Key := 0;
       VK_END : Key := 0;
     end;
end;

procedure TLayoutProduccion.GridLayoutMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     inherited;
     if Button = mbRight then
     begin
          with GridLayout do
          begin
               CoordX := X div ( DefaultColWidth  + 1 );
               CoordY := Y div ( DefaultRowHeight + 1 );

               CoordX := CoordX + LeftCol;
               CoordY := CoordY + TopRow;
          end;
          if ( ( CoordX <= K_MAX_COL  ) and ( CoordY <= K_MAX_ROW ) )then
          begin
               if ( ( GridEstados[CoordX][CoordY]<> nil ) and  (GridEstados[CoordX][CoordY].Estado in [Maquina] ) ) then
               begin
                    dmLabor.cdsMaquinas.Locate('MQ_CODIGO',GridEstados[CoordX][CoordY].Codigo,[]);
               end;
          end;
     end;
end;

procedure TLayoutProduccion.mnuEmpNomEmp1Click(Sender: TObject);
begin
     inherited;
     if GridEstados[CoordX][CoordY].Estado in [ Ausente,Certificado,NoCertificado ] then
     begin
          dmSuper.cdsEmpleados.Locate('CB_CODIGO',TSilla(GridEstados[CoordX][CoordY]).Empleado,[]);
         // dmCliente.SetEmpleadoNumero( TSilla(GridEstados[CoordX][CoordY]).Empleado );
          if TressShell.VerificaDerechoConsulta( D_SUPER_DATOS_EMPLEADOS ) then
          begin
               if ( ConsultaEmpleados = nil ) then
                       ConsultaEmpleados := TConsultaEmpleados.Create( Application );
                    ConsultaEmpleados.ShowModal;
          end;
     end;
end;


procedure TLayoutProduccion.trkZoomChange(Sender: TObject);
var
   iValor:Integer;
begin
     inherited;
     iValor := Trunc(trkZoom.Position * 30);
     GridLayout.DefaultColWidth := Trunc(iValor div 100);
     GridLayout.DefaultRowHeight:= Trunc(iValor div 100);
     with dmLabor do
     begin
          GridLayout.Repaint;
     end;
     LblPorcentajeZoom.Caption := IntToStr( trkZoom.Position ) + ' %';
end;

procedure TLayoutProduccion.FormMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
     //inherited;
     if( ssCtrl in Shift )then
     begin
    //      trkZoom.Position := trkZoom.Position - trkZoom.Increment;
    trkZoom.Position := trkZoom.Position - trkZoom.Properties.TrackSize;
     end;
     Handled := True;
end;

procedure TLayoutProduccion.FormMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
     //inherited;
     if( ssCtrl in Shift )then
     begin
     //     trkZoom.Position := trkZoom.Position + trkZoom.Increment;
     trkZoom.Position := trkZoom.Position + trkZoom.Properties.TrackSize;
     end;
     Handled := True;
end;

procedure TLayoutProduccion.SeleccionarEmpleado(const EmpleadoSelecc :Integer);
var
   X,Y :Integer;
   Lista : TStrings;
//  iValor:Integer;
const
     sMessage = 'Empleado No Est� Asignado';
begin
     with dmLabor.cdsKarEmpMaq do
     begin
          if Locate('CB_CODIGO',EmpleadoSelecc,[])THEN
          begin
               Lista := TStringList.Create;
               if StrLleno(FieldByName('SILLA').AsString ) then
               begin
                     try
                        Lista.CommaText := FieldByName('SILLA').AsString;
                         X := StrToInt(Lista[0]);
                         Y := StrToInt(Lista[1]);
                     finally
                            Lista.Free;
                     end;

//                     iValor := Trunc(trkZoom.Position * 30)div 100;

                     if GridLayout.LeftCol > X  then
                     begin
                          if X < 3 then
                             GridLayout.LeftCol := 0
                          else
                              GridLayout.LeftCol := X - 3;
                     end
                     else
                     begin

                     end;

                     if GridLayout.TopRow > Y  then
                     begin
                          if Y < 3 then
                             GridLayout.TopRow := 0
                          else
                              GridLayout.TopRow := Y - 3;
                     end;
                     GridLayout.Repaint;
                     //GridLayout.SetFocus;
                     GridLayout.Canvas.Brush.Color := clRed;
                     GridLayout.Canvas.FrameRect( GridLayout.CellRect( X, Y ) );
               end
               else
                   ZWarning('Supervisores',sMessage,0,mbOk);
          end
          else
              ZWarning('Supervisores',sMessage,0,mbOk);
     end;
end;

procedure TLayoutProduccion.btnUbicaEmpleado_DevExClick(Sender: TObject);
begin
  inherited;
  SeleccionarEmpleado(dmCliente.Empleado );
end;

procedure TLayoutProduccion.btnAsignar_DevExClick(Sender: TObject);
var
   sTitulo:string;
const
     AnchoImagen=24;
begin
  inherited;
  //SpeedButton1.Down := NOT SpeedButton1.Down;
     FAsignando := not FAsignando;
     if not FAsignando then
     begin
        GridLayout.Options := [goFixedVertLine, goFixedHorzLine,goDrawFocusSelected ];
        sTitulo := Format('Asignar Empleados a %s',[dmLabor.GetMaquinaGlobal]);
        btnAsignar_DevEx.Width := (Length(sTitulo)*6) + AnchoImagen;
        btnAsignar_DevEx.Caption := Format('Asignar Empleados a %s',[dmLabor.GetMaquinaGlobal]);
        btnAsignar_DevEx.OptionsImage.ImageIndex := 0;
        btnAsignar_DevEx.Hint := 'Iniciar Asignaci�n de Empleados';
     end
     else
     begin
         GridLayout.Options := [goFixedVertLine, goFixedHorzLine,goHorzLine,goVertLine,goDrawFocusSelected];
         sTitulo := Format('Terminar de Asignar Empleados a %s',[dmLabor.GetMaquinaGlobal]);
         btnAsignar_DevEx.Width := Length(sTitulo)*6 + AnchoImagen;
         btnAsignar_DevEx.Caption := sTitulo;
         btnAsignar_DevEx.OptionsImage.ImageIndex := 1;
         btnAsignar_DevEx.Hint := 'Terminar Asignaci�n de Empleados';
     end;
     GridLayout.Repaint;
end;

procedure TLayoutProduccion.btnExportar_DevExClick(Sender: TObject);
begin
  inherited;
  try
          with gtPDFGenerador do
          begin
               Page.Orientation := poLandscape;
               FileName := Application.GetNamePath+Format('\%s_%s.pdf',[dmLabor.GetLayoutGlobal,Layout.Descripcion]);

               imgMapaAux.Width := GridLayout.Width;
               imgMapaAux.Height := GridLayout.Height;

               imgMapaAux.Canvas.CopyRect(Rect(0,0,GridLayout.Width ,GridLayout.Height),GridLayout.Canvas,Rect(0,0,GridLayout.Width ,GridLayout.Height ) );

               Page.PaperSize := Letter;
               Preferences.ShowSetupDialog := false;
               BeginDoc;

               TextOut(0.2,0.1,dmLabor.GetLayoutGlobal+':'+ Layout.Llave   +'  ' +Layout.Descripcion );
               TextOut(0.2,0.3,'L�nea : '+ LY_AREA.Caption );
               Line(0,0.5,500,0.5);

               DrawImage(0.1,0.6,imgMapaAux.Picture.Graphic);

               EndDoc;

               imgMapaAux.Canvas.FillRect(Rect(0,0,GridLayout.Width,GridLayout.Height));
          end;
      except on Error: Exception do
             begin
                  ZError (sELF.Caption , 'No se pudo exportar el mapa a archivo PDF - ' + Error.Message,0 );
             end;
      end;
end;

procedure TLayoutProduccion.ZoomInClick(Sender: TObject);
begin
  inherited;
//  trkZoom.Position := trkZoom.Position - trkZoom.Increment;
    trkZoom.Position := trkZoom.Position - trkZoom.Properties.TrackSize;
end;

procedure TLayoutProduccion.ZoomOutClick(Sender: TObject);
begin
  inherited;
 // trkZoom.Position := trkZoom.Position + trkZoom.Increment;
    trkZoom.Position := trkZoom.Position + trkZoom.Properties.TrackSize;
end;

end.
