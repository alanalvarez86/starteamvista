unit DSistema;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
{$ifdef DOS_CAPAS}
  DServerSistema;
{$else}
  Sistema_TLB;
{$endif}

type
  TdmSistema = class(TDataModule)
    cdsUsuarios: TZetaLookupDataSet;
    cdsImpresoras: TZetaClientDataSet;
    cdsGrupos: TZetaLookupDataSet;
    cdsUsuariosLookUp: TZetaLookupDataSet;
    cdsSuscrip: TZetaClientDataSet;
    procedure cdsUsuariosAlAdquirirDatos(Sender: TObject);
    procedure cdsImpresorasAlAdquirirDatos(Sender: TObject);
    procedure cdsGruposAlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosLookUpGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
  private
    FUltimaFrecuencia: Integer;
{$ifdef DOS_CAPAS}
    function GetServerSistema: TdmServerSistema;
    property ServerSistema: TdmServerSistema read GetServerSistema;
{$else}
    FServidor: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
{$endif}
  protected
  public
        {***Para Definir a que EXE pertenece este arbol***}
        FTipo_Exe_DevEx : Integer;
        property UltimaFrecuencia: Integer read FUltimaFrecuencia;
        function AgregaSuscripciones( iUsuario, iReporte, iFrecuencia: Integer; iHayDatos:Integer): boolean;
        procedure GrabaSuscrip;virtual;
  end;

var
  dmSistema: TdmSistema;

implementation

uses
    ZetaCommonClasses,
    DCliente;

{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmSistema.GetServerSistema: TdmServerSistema;
begin
     Result := DCliente.dmCliente.ServerSistema;
end;
{$else}
function TdmSistema.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServidor ) );
end;
{$endif}

procedure TdmSistema.cdsUsuariosAlAdquirirDatos(Sender: TObject);
var
   iLongitud: Integer;
   lBloqueoSistema: WordBool;
begin
     cdsUsuarios.Data := ServerSistema.GetUsuarios( iLongitud, dmCliente.GetGrupoActivo, lBloqueoSistema );
     cdsUsuariosLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBloqueoSistema );
end;

procedure TdmSistema.cdsImpresorasAlAdquirirDatos(Sender: TObject);
begin
     cdsImpresoras.Data := ServerSistema.GetImpresoras;
end;

procedure TdmSistema.cdsGruposAlAdquirirDatos(Sender: TObject);
begin
     cdsGrupos.Data := ServerSistema.GetGrupos( dmCliente.GetgrupoActivo );
end;

function TdmSistema.AgregaSuscripciones( iUsuario, iReporte, iFrecuencia: Integer; iHayDatos:Integer): boolean;
begin
     Result:= FALSE;
end;

procedure TdmSistema.GrabaSuscrip;
begin
end;

procedure TdmSistema.cdsUsuariosLookUpGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := False;
end;

end.
