unit FAsignarAreas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, ZetaStateComboBox, ComCtrls, ZetaDBTextBox,
  Buttons, ExtCtrls, Menus, ImgList, ActnList, ZetaSmartLists, ZetaHora,
  Mask, ZetaFecha, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxButtons,
  dxSkinsdxBarPainter, dxBar, dxBarExtItems, cxClasses, cxLabel,
  cxBarEditItem;

type
  TAsignarAreas = class(TZetaDlgModal_DevEx)
    PanelPa: TPanel;
    PanelCutCopy: TPanel;
    ListEmp: TListView;
    PaEmp: TStateComboBox;
    ImageEmpleadosbkup: TImageList;
    PopupMenu1: TPopupMenu;
    Cortar1: TMenuItem;
    Pegar1: TMenuItem;
    N1: TMenuItem;
    DeshacerCambios1: TMenuItem;
    N2: TMenuItem;
    Ordenar1: TMenuItem;
    Nmero1: TMenuItem;
    Ascendente1: TMenuItem;
    Descendente1: TMenuItem;
    Nombre1: TMenuItem;
    Ascendente2: TMenuItem;
    Descendente2: TMenuItem;
    PanelEmp: TPanel;
    PaFecha: TZetaFecha;
    PaHora: TZetaHora;
    ImageAreasBkup: TImageList;
    ImportarEmpleado1: TMenuItem;
    ImageEmpleadosBig: TImageList;
    Splitter1: TSplitter;
    Panel1: TPanel;
    TreeArea: TTreeView;
    StatusBar: TStatusBar;
    ImageEmpleados: TcxImageList;
    ImageAreas: TcxImageList;
    ImageEmpleadosBig2: TcxImageList;
    paFechaLbl: TLabel;
    PaEmpLbl: TLabel;
    PaHoraLbl: TLabel;
    PaEmpBtn_DevEx: TcxButton;
    BtnRefrescar_DevEx: TcxButton;
    PegarBtn_DevEx: TcxButton;
    ImprimirFormaBtn_DevEx: TcxButton;
    Cortarbtn_DevEx: TcxButton;
    UndoBtn_DevEx: TcxButton;
    ImagesMenu: TcxImageList;
    ImportarBtn_DEvEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeAreaExpanding(Sender: TObject; Node: TTreeNode; var AllowExpansion: Boolean);
    procedure TreeAreaChange(Sender: TObject; Node: TTreeNode);
    procedure TreeAreaDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure TreeAreaDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ListEmpStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure ListEmpCompare(Sender: TObject; Item1, Item2: TListItem; Data: Integer; var Compare: Integer);
    procedure SelectListViewType(Sender: TObject);
    procedure SelectListViewTypeMenu(Sender: TObject);
    procedure ListEmpColumnClick(Sender: TObject; Column: TListColumn);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure PaEmpLookUp(Sender: TObject; var lOk: Boolean);
    procedure CortarBtnClick(Sender: TObject);
    procedure ImprimirFormaBtnClick(Sender: TObject);
    procedure PegarBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure OrdenarPopup(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure BtnRefrescarClick(Sender: TObject);
    procedure ImportarBtnClick(Sender: TObject);
    procedure PaFechaValidDate(Sender: TObject);
    procedure PaEmpBtn_DevExClick(Sender: TObject);
    procedure DevEx_BarManagerEdicionShowToolbarsPopup(
      Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
  private
    { Private declarations }
    Reubicando : Boolean;
    FHuboCambios, FPuedeModificar: Boolean;
    FPuedeAreasSinAsignar: boolean;
    FOrden: (Asc, Desc);
    FColumnaOrden: Integer;
    FGrupoMisAreas, FGrupoSinArea : TTreeNode;
    procedure SetHuboCambios(const Value: Boolean);
    property HuboCambios: Boolean read FHuboCambios write SetHuboCambios;
    function EscribirCambios: Boolean;
    function PonerCutsActuales( oArbol: TTreeView; oLista: TListView ) : Integer;
    function GetItem(oLista: TListView; sEmpleado: String): TListItem;
    function GetNodo(oArbol: TTreeView; sEmpleado: String): TTreeNode;
    function EsNodo(oNodoDestino: TTreeNode): Boolean;
    function UbicaEmpleado( const sEmpleado: String): Boolean;
    function EsNodoEmpleado(oNodo: TTreeNode): Boolean;
    function EsNodoHijo( oNodo,oNodoPadre: TTreeNode ): Boolean;
    function ValidaEmpleadosSinAsignar: Boolean;
    procedure SetControles;
    procedure LlenaListaEmp(oNodoArea: TTreeNode);
    procedure LlenaListaAreas;
    procedure LlenaClientDataSet;
    procedure CancelarCambios;
    procedure QuitaCutsAnteriores( oArbol: TTreeView; oLista: TListView );
    procedure MueveCutsActuales( oArbol: TTreeView; NodoDestino: TTreeNode );
    procedure ActualBoton(iTag: Integer);
    procedure ActualPopup(iTag: Integer);
    procedure RefrescaLista;
    procedure SetParametros(const lEnabled: Boolean);
    procedure AgregaNodoHijo(NewNodo: TTreeNode; const sCodigo, sNombre: String; const iTipo: Integer);
    procedure Activa_Controles(const lValor: Boolean);
  public
    { Public declarations }
  end;

const
     K_MIS_EMPLEADOS = 1;
     K_OTROS_EMPLEADOS = 2;
     K_NIVEL_GRUPOS = 0;
     K_NIVEL_AREAS = 1;
     K_NIVEL_EMPLEADOS = 2;
     K_IMAGEN_MIS_AREAS             = 0;
     K_IMAGEN_OTRAS_AREAS           = 1;
     K_IMAGEN_SIN_AREA              = 2;
     K_IMAGEN_FOLDER_GREEN          = 3;
     K_IMAGEN_FOLDER_GREEN_ABIERTO  = 4;
     K_IMAGEN_FOLDER_YELLOW         = 5;
     K_IMAGEN_FOLDER_YELLOW_ABIERTO = 6;
     K_IMAGEN_EMPLEADO              = 7;
     K_IMAGEN_EMPLEADO_ASIG         = 8;

var
  AsignarAreas: TAsignarAreas;

implementation

uses DSuper,
     DLabor,
     DCliente,
     ZetaCommonTools,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses,
     ZCerrarEdicion_DevEx,
     ZetaBuscaEmpleado_DevEx,
     ZGlobalTress,
     ZetaLaborTools,
     {$ifdef BUILDLABOR}
     FBuscaEmpleadoAsignar,
     {$endif}
     FGrabarAreas;

{$R *.DFM}

procedure TAsignarAreas.FormCreate(Sender: TObject);
begin
     inherited;
     self.Caption:= Format( self.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] );
     HelpContext := 09507;
     FHuboCambios := TRUE;       //Para Obligar el SetControles en el OnShow
end;

procedure TAsignarAreas.FormShow(Sender: TObject);
begin
     inherited;
     PaFecha.Valor := dmCliente.FechaSupervisor;
     with dmLabor do
     begin
          cdsArea.Conectar;
          SetFiltroAreas( TRUE );
          if not cdsArea.IsEmpty then
          begin
               cdsArea.First;
               if not ( cdsArea.FieldByName( 'AR_PRI_HOR' ).AsString = VACIO ) then
                  PaHora.Valor := cdsArea.FieldByName( 'AR_PRI_HOR' ).AsString  // Al Filtrar se posiciona en el primer registro
               else
                   PaHora.valor := FormatDateTime( 'hhmm', Now );
          end;
          cdsArea.Filtered := False;
     end;
     Activa_Controles( True );
     RefrescaLista;
     FPuedeModificar := ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_AREAS, K_DERECHO_CAMBIO );     
     FPuedeAreasSinAsignar := ZAccesosMgr.Revisa( D_SUPER_AREAS_SIN_ASIGNAR );     
end;

procedure TAsignarAreas.RefrescaLista;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with dmLabor do
             begin
                  FechaArea := PaFecha.Valor;
                  HoraArea := PaHora.Valor;
                  cdsEmpAreas.Refrescar;
             end;
             LlenaListaAreas;
             HuboCambios := FALSE;
             TreeArea.SetFocus;
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TAsignarAreas.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     CanClose := True;
     if HuboCambios then
     begin
          case ZCerrarEdicion_DevEx.CierraEdicion of
               mrOk: CanClose := EscribirCambios;
               mrIgnore: CancelarCambios;
          else
              CanClose := FALSE;
          end;
     end;
end;

{ ************* Grabar o Cancelar Informaci�n *********** }

function TAsignarAreas.EsNodoHijo( oNodo, oNodoPadre: TTreeNode ):Boolean;
begin
     Result := Assigned( oNodo ) and ( oNodo.Parent = oNodoPadre );
end;

function TAsignarAreas.EsNodoEmpleado( oNodo: TTreeNode ): Boolean;
begin
     Result := Assigned( oNodo ) and (  EsNodoHijo( oNodo, FGrupoSinArea ) or EsNodoHijo( oNodo.Parent, FGrupoMisAreas ) );
end;

procedure TAsignarAreas.LlenaClientDataSet;
var
   oNodo : TTreeNode;
   EmpCodigo : Integer;
   AreaCodigo, AreaAnterior : String;

   function RegresaCodigo( Texto: String ): Integer;
   begin
        Result := StrToInt( Copy( Texto, 2, pos(']',Texto)-2 ) );
   end;

   function RegresaArea( Texto: String ): String;
   begin
        Result := Copy( Texto, 2, pos(']',Texto)-2 );
   end;

begin
     with dmLabor do
     begin
           with cdsAsignaAreas do
           begin
                if Active then
                   EmptyDataSet
                else
                    CreateDataSet;
          end;
          oNodo:= TreeArea.Items[0];
          while ( Assigned ( oNodo ) ) do
          begin
               if EsNodoEmpleado( oNodo ) then     // Es Empleado
               begin
                    AreaCodigo := RegresaArea( oNodo.Parent.Text );
                    EmpCodigo := RegresaCodigo( oNodo.Text );
                    AreaAnterior := ChecaAreaAnterior( EmpCodigo, AreaCodigo );
                    if ( AreaAnterior <> AreaCodigo ) then
                        cdsAsignaAreas.AppendRecord( [ EmpCodigo, AreaCodigo, AreaAnterior ] );
               end;
               oNodo:= oNodo.GetNext;
          end;
          cdsAsignaAreas.First;
     end;
end;

function TAsignarAreas.ValidaEmpleadosSinAsignar: Boolean;
begin
     Result := Assigned( FGrupoSinArea ) and ( ( not FGrupoSinArea.HasChildren ) or ( FGrupoSinArea.HasChildren and
               ZetaDialogo.ZWarningConfirm( '� Atenci�n !', Format( 'Se Tienen Empleados Sin Asignaci�n de %s' + CR_LF + '� Desea Continuar ?', [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] ), 0, mbCancel ) ) );
end;

function TAsignarAreas.EscribirCambios: Boolean;
begin
     Result := ValidaEmpleadosSinAsignar;
     if Result then
        try
           LlenaClientDataSet;
           Result := ShowGrabarAreas;
        except
           Result := FALSE;
        end;
end;

procedure TAsignarAreas.CancelarCambios;
begin
     LlenaListaAreas;
     HuboCambios := FALSE;
     TreeArea.SetFocus;
end;

procedure TAsignarAreas.OKClick(Sender: TObject);
begin
     inherited;
     if EscribirCambios then
     begin
          HuboCambios:= FALSE;
          Close;
     end;
end;

procedure TAsignarAreas.CancelarClick(Sender: TObject);
begin
     inherited;
     if HuboCambios then
        CancelarCambios
     else
        Close;
end;

{ Llena Componentes de Datos TreeArea y ListEmp }

procedure TAsignarAreas.LlenaListaEmp( oNodoArea : TTreeNode );
var
   oNodo : TTreeNode;
   NewItem : TListItem;
   iContador: Integer;

    function ObtieneNumero( Texto: String ): String;
    begin
         Result := Copy( Texto, 2, pos(']',Texto)-2 );
    end;

    function ObtieneNombre( Texto: String ): String;
    begin
         delete( Texto, 1, pos(']',Texto) );
         Result := Texto;
    end;

begin
     iContador := 0;
     with ListEmp.Items do
     begin
          BeginUpdate;
          Clear;
          if Assigned( oNodoArea ) then
          begin
               With oNodoArea do
               Begin
                    oNodo := oNodoArea.GetFirstChild;
                    while Assigned( oNodo ) do
                    begin
                         NewItem:= Add;
                         if Assigned( NewItem ) then
                         begin
                              NewItem.Caption := ObtieneNumero( oNodo.Text );
                              NewItem.Data := oNodo;
                              NewItem.ImageIndex:= oNodo.ImageIndex - 7;
                              NewItem.StateIndex:= oNodo.ImageIndex - 8;
                              NewItem.Cut := oNodo.Cut;
                              NewItem.SubItems.Add( ObtieneNombre( oNodo.Text ) );
                              iContador := iContador + 1;
                         end;
                         oNodo := oNodoArea.GetNextChild(oNodo);
                    end;
               end;
               StatusBar.Panels.Items[0].Text := 'N�mero de Empleados: ' + inttostr(iContador);
          end
          else
              StatusBar.Panels.Items[0].Text := '';
          EndUpdate;
     end;
end;

procedure TAsignarAreas.AgregaNodoHijo( NewNodo: TTreeNode; const sCodigo, sNombre: String; const iTipo: Integer );
var
   NewHijo : TTreeNode;
begin
     NewHijo:= TreeArea.Items.AddChild( NewNodo, Format( '[%s]%s', [ sCodigo, sNombre ] ) );
     if Assigned( NewHijo ) then
     begin
          if ( iTipo = K_MIS_EMPLEADOS ) then
          begin
               NewHijo.ImageIndex:= K_IMAGEN_EMPLEADO;
               NewHijo.SelectedIndex:= K_IMAGEN_EMPLEADO;
          end
          else
          begin
               NewHijo.ImageIndex:= K_IMAGEN_EMPLEADO_ASIG;
               NewHijo.SelectedIndex:= K_IMAGEN_EMPLEADO_ASIG;
          end;
     end;
end;

procedure TAsignarAreas.LlenaListaAreas;
var
   NewGrupo, NewNodo : TTreeNode;

   procedure AgregaNodosEmpleados( const sArea, sDescrip: String; const lMisAreas: Boolean = TRUE; const lAgregaPadre: Boolean = TRUE );
   begin
        with dmLabor.cdsEmpAreas, TreeArea.Items do
        begin
             Filter := Format( 'CB_AREA = ''%s''', [ sArea ] );
             Filtered := TRUE;
             if lAgregaPadre or ( not IsEmpty ) then
             begin
                  if strVacio( sArea ) then
                     NewNodo := NewGrupo           // En Vacios se agregar�n Empleados en Nivel 1
                  else
                  begin
                       NewNodo:= AddChild( NewGrupo, '[' + sArea + '] ' + sDescrip );
                       if Assigned( NewNodo ) then
                          with NewNodo do
                          begin
                               if lMisAreas then
                               begin
                                    ImageIndex:= K_IMAGEN_FOLDER_GREEN;
                                    SelectedIndex:= K_IMAGEN_FOLDER_GREEN_ABIERTO;
                               end
                               else
                               begin
                                    ImageIndex:= K_IMAGEN_FOLDER_YELLOW;
                                    SelectedIndex:= K_IMAGEN_FOLDER_YELLOW_ABIERTO;
                               end;
                          end;
                  end;
                  if Assigned( NewNodo ) then
                  begin
                       First;
                       while ( not EOF ) do
                       begin
                            AgregaNodoHijo( NewNodo, FieldByName('CB_CODIGO').AsString, FieldByName('PRETTYNAME').AsString,
                                            FieldByName( 'CB_TIPO' ).AsInteger );
                            Next;
                       end;
                  end;
             end;
             Filtered := False;
        end;
   end;

begin
     with dmLabor do
     begin
          with TreeArea.Items do
          begin
               Reubicando := True;
               BeginUpdate;
               Clear;

               // Mis Areas
               SetFiltroAreas( TRUE );
               NewGrupo:= Add( nil , Format( 'Mis %s', [ GetLaborLabel( K_GLOBAL_LABOR_AREAS ) ]  ) );
               if Assigned( NewGrupo ) then
               begin
                    NewGrupo.ImageIndex:= K_IMAGEN_MIS_AREAS;
                    NewGrupo.SelectedIndex:= K_IMAGEN_MIS_AREAS;
                    FGrupoMisAreas := NewGrupo;
                    // Mis Areas
                    with cdsArea do
                    begin
                         First;
                         while not EOF do
                         begin
                              AgregaNodosEmpleados( FieldByName('TB_CODIGO').AsString, FieldByName('TB_ELEMENT').AsString );
                              Next;
                         end;
                    end;
               end;

               // Sin Asignar Area
               NewGrupo:= Add( nil , Format( '%s SIN ASIGNAR', [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] ) );
               if Assigned( NewGrupo ) then
               begin
                    NewGrupo.ImageIndex:= K_IMAGEN_SIN_AREA;
                    NewGrupo.SelectedIndex:= K_IMAGEN_SIN_AREA;
                    FGrupoSinArea := NewGrupo;
                    AgregaNodosEmpleados( VACIO, VACIO );
               end;

               // Otras Areas
               SetFiltroAreas( FALSE );
               NewGrupo:= Add( nil , Format( 'Otras %s', [ GetLaborLabel( K_GLOBAL_LABOR_AREAS ) ]  ) );
               if Assigned( NewGrupo ) then
               begin
                    NewGrupo.ImageIndex:= K_IMAGEN_OTRAS_AREAS;
                    NewGrupo.SelectedIndex:= K_IMAGEN_OTRAS_AREAS;
                    with cdsArea do
                    begin
                         First;
                         while not EOF do
                         begin
                              AgregaNodosEmpleados( FieldByName('TB_CODIGO').AsString, FieldByName('TB_ELEMENT').AsString, FALSE, FALSE );
                              Next;
                         end;
                    end;
               end;
               cdsArea.Filtered := False;
               NewGrupo.Expand( False );           { Otras Areas }
               NewGrupo := TreeArea.Items[0];       { Mis Areas }
               NewGrupo.Expand( False );

               EndUpdate;
               Reubicando := False;

               NewNodo := NewGrupo.GetFirstChild;
               while ( NewNodo <> nil ) and ( not NewNodo.HasChildren ) do
                     NewNodo := NewNodo.GetNextSibling;
               if ( NewNodo = nil ) then
                  NewNodo := TreeArea.Items[0];

               if ( NewNodo.HasChildren ) then    { Mostrar Primer Area }
               begin
                    TreeArea.Selected := NewNodo;
                    TreeArea.OnChange( self, TreeArea.Selected );
               end;
          end;
     end;
end;

{ ************** Eventos del Arbol y Lista ************** }

procedure TAsignarAreas.TreeAreaExpanding(Sender: TObject; Node: TTreeNode;
          var AllowExpansion: Boolean);
begin
     inherited;
     if ( not Reubicando ) then
     begin
          if EsNodo( Node ) or ( Node.Level = K_NIVEL_AREAS ) then
          begin
               AllowExpansion := False;
               TreeArea.Selected := Node;
               LlenaListaEmp( Node );
          end;
     end;
end;

procedure TAsignarAreas.TreeAreaChange(Sender: TObject; Node: TTreeNode);
begin
     inherited;
     if not Reubicando then
     begin
         if EsNodo( Node ) or ( Node.Level = K_NIVEL_AREAS ) then     //Esta Seleccionado un Area
            LlenaListaEmp( Node )
         else
            LlenaListaEmp( nil );
     end;
end;

procedure TAsignarAreas.TreeAreaDragOver(Sender, Source: TObject; X, Y: Integer;
          State: TDragState; var Accept: Boolean);
begin
     inherited;
     Accept:= ( FPuedeModificar ) and ( Sender is TTreeView ) and ( Source is TListView ) and
              ( EsNodo( TTreeView( Sender).GetNodeAt( X, Y ) ) );
     if Accept then
        Accept := ( TTreeView( Sender ).GetNodeAt( X, Y ) <> FGrupoSinArea ) or FPuedeAreasSinAsignar;
end;

function TAsignarAreas.EsNodo( oNodoDestino: TTreeNode ): Boolean;
begin
     Result := Assigned( oNodoDestino ) and ( ( oNodoDestino.Parent = FGrupoMisAreas ) or ( oNodoDestino = FGrupoSinArea ) ); 
end;

procedure TAsignarAreas.TreeAreaDragDrop(Sender, Source: TObject; X, Y: Integer);
var
   oNode, NodoOrigen, NodoDestino: TTreeNode;
   oItem : TListItem;
   oListaFuente: TListView;

begin
     inherited;
     NodoOrigen:= TreeArea.Selected;
     NodoDestino:= TreeArea.GetNodeAt(X,Y);

     if EsNodo( NodoDestino ) then
     begin
          oListaFuente:= TListView( source );
          if ( oListaFuente.Selected = nil ) then Exit;

          Reubicando := True;

          oListaFuente.Items.BeginUpdate;
          TreeArea.Items.BeginUpdate;

          with TreeArea.Items do
          begin
               Repeat
                     oItem := oListaFuente.Selected;
                     oNode := oItem.Data;
                     oNode.MoveTo( NodoDestino, naAddChild );
                     oItem.Delete;
               until ( oListaFuente.Selected = nil );
          end;

          HuboCambios := TRUE;

          NodoDestino.Collapse(True);
          TreeArea.Items.EndUpdate;
          oListaFuente.Items.EndUpdate;

          Reubicando := False;

          with TreeArea do
          begin
               SetFocus;
               Selected := NodoOrigen;
               OnChange( self, NodoOrigen );
          end;
     end;
end;

procedure TAsignarAreas.ListEmpStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
     inherited;
     with ListEmp do
     begin
          if ( SelCount > 1 ) then
             DragCursor := crMultiDrag
          else
             DragCursor := crDrag;
     end;
end;

procedure TAsignarAreas.ListEmpCompare(Sender: TObject; Item1, Item2: TListItem;
          Data: Integer; var Compare: Integer);

   function RegresaCodigo( Texto: String ): Integer;
   begin
        Result := StrToIntDef( Texto, 0 );
   end;

begin
     inherited;
     if FColumnaOrden = 0 then { N�mero de Empleado }
        Compare := RegresaCodigo( Item1.Caption ) - RegresaCodigo( Item2.Caption )
     else
        Compare := StrComp( {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}(Item1.SubItems[ FColumnaOrden - 1 ]), {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}(Item2.SubItems[ FColumnaOrden - 1 ]) );

     if FOrden = Desc then
        Compare := Compare * -1;
end;

procedure TAsignarAreas.ListEmpColumnClick(Sender: TObject; Column: TListColumn);
begin
     inherited;
     if ( FColumnaOrden = Column.Index ) then
        if FOrden = Asc then
           FOrden := Desc
        else
           FOrden := Asc;

     FColumnaOrden:= Column.Index;
     ListEmp.AlphaSort; { Activar Evento onCompare }
end;

{ ************** Busqueda de Empleados ************** }

function TAsignarAreas.GetItem( oLista: TListView; sEmpleado: String ): TListItem;
var
   iEmpleado, i : Integer;
   lSalir: Boolean;
begin
     Result:= nil;
     i := 0;
     lSalir:= False;
     iEmpleado := StrToIntDef( sEmpleado, 0 );

     if ( oLista.Items.Count > 0 ) and ( iEmpleado > 0 ) then
     begin
          while ( not lSalir ) do
          begin
               with oLista.Items do
               begin
                    //if ( Pos( sEmpleado, Item[i].Caption ) > 0 ) then
                    if ( iEmpleado = StrToIntDef( Item[i].Caption, 0 ) ) then
                    begin
                         Result := Item[i];
                         lSalir := True;
                         { Seleccionar Item }
                         if Item[i].Selected then
                            Item[i].Selected := False
                         else
                             Item[i].Selected := True;

                         Item[i].MakeVisible( False );
                    end;
               end;
               i := i + 1;
               if ( i = oLista.Items.Count ) then
                  lSalir := True;
          end;
     end;
end;

function TAsignarAreas.GetNodo( oArbol: TTreeView; sEmpleado: String ): TTreeNode;
var
   oNodo: TTreeNode;
   lSalir: Boolean;

   function GetStrEmp: String;
   begin
        Result:= '[' + sEmpleado + ']';
   end;

begin
     Result:= nil;

     Reubicando:= True;

     oNodo:= oArbol.Items[0];
     lSalir:= False;
     repeat
           if ( oNodo.Level = K_NIVEL_EMPLEADOS ) or EsNodoHijo( oNodo, FGrupoSinArea ) then  { Si son empleados }
           begin
                if ( Pos( GetStrEmp, oNodo.Text) > 0 ) then
                begin
                     Result:= oNodo.Parent;
                     lSalir:= True;
                     oArbol.Selected:= oNodo.Parent;
                end;
           end;
           oNodo:= oNodo.GetNext;
     until ( oNodo = nil ) or lSalir;

     Reubicando := False;

     If Assigned( Result ) then
        oArbol.OnChange( self, Result );
end;

function TAsignarAreas.UbicaEmpleado( const sEmpleado: String ): Boolean;
begin
     Result := Assigned( GetItem( ListEmp, sEmpleado ) ); { Lista Actual }
     if not Result then
     begin
          Result := Assigned( GetNodo( TreeArea, sEmpleado ) ); { Buscar en Arbol }
          if Result then
             Result := Assigned( GetItem( ListEmp, sEmpleado ) ); //Si encontr� en Arbol, Rebusca en la Lista ya Actualizada
     end;
end;

procedure TAsignarAreas.PaEmpLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     if not UbicaEmpleado( PaEmp.Text ) then
        zWarning( '� Atenci�n !', '��� Empleado No Le Pertenece !!!', 0, mbOK )
end;

{ ************* Procesos y Funciones de Uso General *********** }

procedure TAsignarAreas.SetHuboCambios(const Value: Boolean);
begin
     if FHuboCambios <> Value then
     begin
          FHuboCambios:= Value;
          SetControles;
     end;
end;

procedure TAsignarAreas.SetControles;
begin
     OK_DevEx.Enabled := HuboCambios;
     if HuboCambios then
     begin
          with Cancelar_DevEx do
          begin
              // Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar_DevEx do
          begin
              // Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     UndoBtn_DevEx.Enabled := HuboCambios;
     DeshacerCambios1.Enabled := HuboCambios;
     SetParametros( not HuboCambios );
     Application.ProcessMessages;
end;

procedure TAsignarAreas.SetParametros( const lEnabled: Boolean );
begin
        PaFecha.Enabled := lEnabled;
     PaFechaLbl.Enabled := lEnabled;
     PaHora.Enabled := lEnabled;
     PaHoraLbl.Enabled := lEnabled;
     BtnRefrescar_DevEx.Enabled := lEnabled;
end;

procedure TAsignarAreas.BtnRefrescarClick(Sender: TObject);
begin
     inherited;
     Activa_Controles( True );
     RefrescaLista;
end;

procedure TAsignarAreas.CortarBtnClick(Sender: TObject);
begin
     inherited;
     QuitaCutsAnteriores( TreeArea, ListEmp );
     PegarBtn_DevEx.Enabled := ( PonerCutsActuales( TreeArea, ListEmp ) > 0 );
     Pegar1.Enabled := PegarBtn_DevEx.Enabled;
end;

procedure TAsignarAreas.PegarBtnClick(Sender: TObject);
var
   NodoDestino: TTreeNode;
   lValida: boolean;

begin
     inherited;
     NodoDestino := TreeArea.Selected;
     lValida := EsNodo( NodoDestino );
     if lValida then
     begin
         lValida := ( NodoDestino <> FGrupoSinArea ) or FPuedeAreasSinAsignar;
         if lValida then
         begin
              MueveCutsActuales( TreeArea, NodoDestino );
              TreeArea.OnChange( self, NodoDestino );
              PegarBtn_DevEx.Enabled := False;
              Pegar1.Enabled := PegarBtn_DEvEx.Enabled;
         end
         else
             ZetaDialogo.ZInformation( '� Atenci�n !', 'No Tiene Derecho de Asignar Empleados a ' + FGrupoSinArea.Text, 0 );
     end
     else
         ZetaDialogo.ZInformation( '� Atenci�n !', 'Solo se Puede Asignar Empleados a ' + FGrupoMisAreas.Text + ' � ' + FGrupoSinArea.Text, 0 );
end;

procedure TAsignarAreas.ImportarBtnClick(Sender: TObject);
var
   NodoDestino : TTreeNode;
   sEmpleado, sEmpleadoDesc: String;
   oCursor: TCursor;
   {$ifdef BUILDLABOR}
   oForma: TBuscaEmpleadoAsignar;
   {$endif}

begin
     {$ifdef BUILDLABOR}
     oForma := TBuscaEmpleadoAsignar.Create( Self );
     try
     {$endif}
        NodoDestino := TreeArea.Selected;
        if EsNodo( NodoDestino ) and ( NodoDestino.Parent = FGrupoMisAreas ) then
        begin
             {$ifdef BUILDLABOR}
             with oForma do
             begin
                  ShowModal;
                  if ( ModalResult = mrOk ) and ( NumeroEmpleado <> 0 ) then
                  begin
                       sEmpleado := edEmpleado.Text;
                  end;
             {$endif}
             if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sEmpleado, sEmpleadoDesc ) then
             begin
                  if UbicaEmpleado( sEmpleado ) then               // Ya Est� en el Arbol: Se simula un Drag-Drop
                  begin
                       CortarBtnClick( self );
                       TreeArea.Selected := NodoDestino;           // Ubica Empleado Cambia el Nodo Seleccionado
                       PegarBtnClick( self );
                  end
                  else
                  begin
                       with Screen do
                       begin
                            oCursor := Cursor;
                            Cursor := crHourglass;
                            try
                               with dmLabor do
                               begin
                                    if AgregaOtroEmpleado( sEmpleado ) then
                                    begin
                                         with cdsEmpAreas do
                                              AgregaNodoHijo( TreeArea.Selected, FieldByName('CB_CODIGO').AsString, FieldByName('PRETTYNAME').AsString,
                                                              FieldByName( 'CB_TIPO' ).AsInteger );
                                         TreeArea.OnChange( self, TreeArea.Selected );
                                         HuboCambios := TRUE;
                                    end;
                               end;
                            finally
                               Cursor := oCursor;
                            end;
                       end;//With Screen
                  end;//else de UbicaEmpleado
             end;//if Dialogo Empleado
        end
        else
            ZetaDialogo.ZInformation( '� Atenci�n !', Format( 'Solo se Puede Agregar Empleados a Mis %0:s', [ GetLaborLabel( K_GLOBAL_LABOR_AREAS ) ] ), 0 );
     {$ifdef BUILDLABOR}
     finally
            FreeAndNil( oForma );
     end;
     {$endif}
end;

procedure TAsignarAreas.UndoBtnClick(Sender: TObject);
begin
     inherited;
     if ( HuboCambios ) then
        if ZWarningConfirm( Caption, 'Seguro de Deshacer los Cambios ?', 0, mbOk ) then
        begin
             LlenaListaAreas;
             HuboCambios := FALSE;
        end;
end;

procedure TAsignarAreas.ImprimirFormaBtnClick(Sender: TObject);
begin
     inherited;
     if zConfirm( 'Imprimir...', '� Desea Imprimir la Pantalla: ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

procedure TAsignarAreas.SelectListViewType(Sender: TObject);
begin
     ListEmp.ViewStyle := TViewStyle(TComponent(Sender).Tag);
     ActualPopup( TComponent(Sender).Tag );
end;

procedure TAsignarAreas.SelectListViewTypeMenu(Sender: TObject);
begin
     ListEmp.ViewStyle := TViewStyle(TComponent(Sender).Tag);
     ActualPopup( TComponent(Sender).Tag );
     ActualBoton( TComponent(Sender).Tag );
end;

procedure TAsignarAreas.ActualPopup( iTag : Integer );
begin
{
     case iTag of
          0 : VistaIconos1.Checked := True;
          1 : VistaIconosPequeos1.Checked := True;
          2 : VistaenListado1.Checked := True;
          3 : VistaenReporte1.checked := True;
     end;
     }
end;

procedure TAsignarAreas.ActualBoton( iTag : Integer );
begin
   //  case iTag of
        //  0 : LargeBtn.Down := True;
       //   1 : SmallBtn.Down := True;
       //   2 : ListBtn.Down := True;
       //   3 : DetailsBtn.Down := True;
  //   end;
end;

procedure TAsignarAreas.OrdenarPopup(Sender: TObject);
begin
     inherited;
     FColumnaOrden := TMenuItem(Sender).Parent.Tag;
     case TMenuItem(Sender).Tag of
          0 : FOrden := Asc;
          1 : FOrden := Desc;
     end;
     ListEmp.AlphaSort;   { Activar Evento onCompare }
end;

procedure TAsignarAreas.PopupMenu1Popup(Sender: TObject);
var
   i, j : ShortInt;
begin
     inherited;
     Ordenar1.Items[ FColumnaOrden ].Checked := True;

     for i := 0 to 1 do
         for j := 0 to 1 do
             Ordenar1.Items[ i ].Items[j].Checked := False;
     case FOrden of
          Asc : Ordenar1.Items[ FColumnaOrden ].Items[0].Checked := True;
          Desc : Ordenar1.Items[ FColumnaOrden ].Items[1].Checked := True;
     end;
end;

{ ************* Procesos de Cut y Paste *********** }

procedure TAsignarAreas.QuitaCutsAnteriores( oArbol: TTreeView; oLista: TListView );
var
   oNodo : TTreeNode;
   oItem, oItemPrevio : TListItem;
begin

     Reubicando := True;

     oNodo:= oArbol.Items[0];

     while ( Assigned ( oNodo ) ) do
     begin
          oNodo.Cut := False;
          oNodo:= oNodo.GetNext;
     end;

     if ( oLista.Items.Count > 0 ) then
        with oLista do
        begin
             if ( Items.Item[0].Cut ) then
                oItem := Items.Item[0]
             else
                oItem := GetNextItem( Items.Item[0], sdAll, [isCut] );

             while ( Assigned ( oItem ) ) do
             begin
                  oItem.Cut := False;
                  oItemPrevio := oItem;
                  oItem := GetNextItem( oItemPrevio, sdAll, [isCut] );
             end;
        end;

     Reubicando := False;

end;

function TAsignarAreas.PonerCutsActuales( oArbol: TTreeView; oLista: TListView ) : Integer;
var
   oItem, oItemPrevio : TListItem;
   oNodo : TTreeNode;
   cuantos : Integer;
begin
     Cuantos := 0;
     Result := 0;

     if ( oLista.Selected = nil ) then Exit;

     Reubicando := True;

     with oLista do
     begin
          if ( Items.Item[0].Selected ) then
             oItem := Items.Item[0]
          else
             oItem := GetNextItem( Items.Item[0], sdAll, [isSelected] );

          while ( Assigned ( oItem ) ) do
          begin
               oNodo := oItem.Data;
               oNodo.Cut := True;
               oItem.Cut := True;
               Inc( Cuantos );
               oItemPrevio := oItem;
               oItem := GetNextItem( oItemPrevio, sdAll, [isSelected] );
          end;
     end;

     Result := Cuantos;
     Reubicando := False;

end;

procedure TAsignarAreas.MueveCutsActuales( oArbol: TTreeView; NodoDestino: TTreeNode );
var
   oNodo : TTreeNode;
begin

     Reubicando := True;

     oArbol.Items.BeginUpdate;

     oNodo:= oArbol.Items[0];

     while Assigned( oNodo ) do
     begin
          if ( oNodo.Cut ) then
          begin
               oNodo.Cut := False;
               oNodo.MoveTo( NodoDestino, naAddChild );
               oNodo:= oArbol.Items[0];
               HuboCambios := TRUE;
          end;
          oNodo:= oNodo.GetNext;
     end;

     NodoDestino.Collapse(True);

     oArbol.Items.EndUpdate;
     Reubicando := False;

end;

procedure TAsignarAreas.PaFechaValidDate(Sender: TObject);
begin
     Activa_Controles( False );
end;

procedure TAsignarAreas.Activa_Controles( const lValor: Boolean );
begin
     ImprimirFormaBtn_DevEx.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_AREAS, K_DERECHO_IMPRESION );
     TreeArea.Enabled  := lValor;
     ListEmp.Enabled   := lValor;
     CortarBtn_DevEx.Enabled := lValor and FPuedeModificar;
     Cortar1.Enabled   := CortarBtn_DevEx.Enabled;
     PegarBtn_DEvEx.Enabled  := FALSE;
     Pegar1.Enabled    := PegarBtn_DevEx.Enabled;
     ImportarBtn_DEvEx.Enabled := lValor and ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_AREAS, K_DERECHO_SIST_KARDEX );
     ImportarEmpleado1.Enabled := ImportarBtn_DEvEx.Enabled;
end;

procedure TAsignarAreas.PaEmpBtn_DevExClick(Sender: TObject);
var
   sEmpleado, sEmpleadoDesc: String;
begin
  inherited;
  if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sEmpleado, sEmpleadoDesc ) then
        PaEmp.AsignaValorEntero( StrToIntDef( sEmpleado, 0 ) );
end;

procedure TAsignarAreas.DevEx_BarManagerEdicionShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
  inherited;
  Abort;
end;

end.

