unit FRegistroWorks;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaHora, ComCtrls, Mask, ZetaFecha, ZetaSmartLists,
  ZetaNumero, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, ZetaKeyLookup_DevEx,
  dxBarBuiltInMenu;

type
  TRegistroWorks = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    AU_FECHA: TZetaDBFecha;
    Operaciones: TcxPageControl;
    Generales: TcxTabSheet;
    Moduladores: TcxTabSheet;
    PanelHora: TPanel;
    LblHora: TLabel;
    LblCantidad: TLabel;
    WK_HORA_R: TZetaDBHora;
    WK_PIEZAS: TZetaDBNumero;
    PanelOrden: TPanel;
    LblOrden: TLabel;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    PanelParte: TPanel;
    LblParte: TLabel;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    PanelOpera: TPanel;
    LblOpera: TLabel;
    OP_NUMBER: TZetaDBKeyLookup_DevEx;
    PanelArea: TPanel;
    LblArea: TLabel;
    CB_AREA: TZetaDBKeyLookup_DevEx;
    PanelStatus: TPanel;
    LblStatus: TLabel;
    WK_STATUS: TZetaDBKeyCombo;
    PanelModula1: TPanel;
    LblModula1: TLabel;
    WK_MOD_1: TZetaDBKeyLookup_DevEx;
    PanelModula2: TPanel;
    LblModula2: TLabel;
    WK_MOD_2: TZetaDBKeyLookup_DevEx;
    PanelModula3: TPanel;
    LblModula3: TLabel;
    WK_MOD_3: TZetaDBKeyLookup_DevEx;
    MultiLote: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure MultiLote2Click(Sender: TObject);
//    procedure CB_AREAValidKey(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FMultiLote:Boolean;
    procedure AgregarMultiLotes;
    procedure SetControlesOperacion;
  protected
    FValidaHora: Boolean;
    FTipoTMuerto:string;
    FDuracion:Integer;
    FControlOperacion: TWinControl;
    procedure Connect; override;
    procedure SetControlesVisibles; virtual;
    procedure SetInfoNuevoRegistro; virtual;
    procedure SetOtrosDatos( const lMultiLote: Boolean ); virtual;
    procedure ValidaDatosLabor; virtual;    
  public
    { Public declarations }
  end;

const
     MARGEN_ALTURA = 27;

var
  RegistroWorks: TRegistroWorks;

implementation

uses DLabor, DGlobal, ZGlobalTress, ZetaCommonTools, ZetaLaborTools, ZetaDialogo,
     ZetaCommonClasses, DCliente;

{$R *.DFM}

{ TRegistroWorks }

procedure TRegistroWorks.FormCreate(Sender: TObject);
begin
     self.Caption:= Format( self.Caption, [ Global.GetGlobalString( K_GLOBAL_LABOR_OPERACION ) ] );
     inherited;
     FValidaHora:= TRUE;
     FirstControl := CB_CODIGO;
     SetControlesVisibles;
     CB_AREA.Filtro := MIS_AREAS;
     Operaciones.Height := iMax( GetAltura( Generales ), GetAltura( Moduladores ) ) + MARGEN_ALTURA;
     HelpContext := 9503;
     FControlOperacion := OK_DevEx;
     Multilote.Visible := Global.GetGlobalBooleano( K_GLOBAL_LABOR_MULTILOTES );
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
end;

procedure TRegistroWorks.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      OP_NUMBER.WidthLlave := K_WIDTHLLAVE;
      CB_AREA.WidthLlave:= K_WIDTHLLAVE;

      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      OP_NUMBER.Width := K_WIDTH_LOOKUP;
      CB_AREA.Width:= K_WIDTH_LOOKUP;
      Self.Width := K_WIDTH_BASECEDULAS;
     {$endif}
     Operaciones.ActivePage := Generales;
     SetInfoNuevoRegistro;
     {
     MultiLote.Down := FALSE;
     WO_NUMBER.Enabled := TRUE;
     }
end;

procedure TRegistroWorks.Connect;
begin
     with dmLabor do
     begin
          cdsArea.Conectar;
          cdsOpera.Conectar;
          cdsModula1.Conectar;
          cdsModula2.Conectar;
          cdsModula3.Conectar;
          cdsWorks.Conectar;
          DataSource.DataSet := cdsWorks;
     end;
end;

procedure TRegistroWorks.OKClick(Sender: TObject);
begin
     if FMultiLote then
        AgregarMultiLotes
     else
        SetOtrosDatos( FALSE );                  // Para formas que heredan de esta pongan sus datos
     ValidaDatosLabor;
     inherited;                                  // Se graba igual aunque sean MultiLotes
     if dmLabor.cdsWorks.ChangeCount = 0 then    // Se Aplicaron los cambios
     begin
          Close;
          {
          SetInfoNuevoRegistro;
          HabilitaControles;
          }
     end;
end;

procedure TRegistroWorks.ValidaDatosLabor;
begin
     if StrVacio( WK_HORA_R.Valor ) then
        ReportaControlError( WK_HORA_R, 'Debe Especificarse la Hora' );
     if ( WK_PIEZAS.Visible ) and ( WK_PIEZAS.Valor < 0 ) then
        ReportaControlError( WK_PIEZAS, LblCantidad.Caption + ' Debe Ser Mayor o Igual Que Cero' );
     ValidaControlLookup( WO_NUMBER, LblOrden );
     ValidaControlLookup( AR_CODIGO, LblParte );
     ValidaControlLookup( CB_AREA, LblArea );
     ValidaControlLookup( OP_NUMBER, LblOpera );
end;

procedure TRegistroWorks.SetInfoNuevoRegistro;
begin
     with dmLabor do
     begin
          cdsMultiLote.Active := FALSE;

          cdsWorks.Append;   // InsertInicial
     
     end;
     //self.MultiLote.Down := FALSE;
     FMultiLote := False;
     self.WO_NUMBER.Enabled:= TRUE;
     self.WK_PIEZAS.Enabled:= TRUE;
end;

procedure TRegistroWorks.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TRegistroWorks.MultiLote2Click(Sender: TObject);
var
   sOrden, sParte : String;
   rQty: Currency;
   lEnabled: Boolean;
begin
     inherited;
     with dmLabor.cdsWorks do
     begin
          sOrden := FieldByName( 'WO_NUMBER' ).AsString;
          rQty := FieldByName( 'WK_PIEZAS' ).AsFloat;
          lEnabled := dmLabor.GetMultiLote( sOrden, sParte, rQty );
          FMultiLote := lEnabled;
          if (not (State in[ dsEdit,dsInsert ]))then
          begin
               Edit;
          end;

          if ( FieldByName( 'WO_NUMBER' ).AsString <> sOrden ) then
          begin
               FieldByName( 'WO_NUMBER' ).AsString := sOrden;
               FieldByName( 'AR_CODIGO' ).AsString := sParte;
          end;
          if ( FieldByName( 'WK_PIEZAS' ).AsFloat <> rQty ) then
             FieldByName( 'WK_PIEZAS' ).AsFloat := rQty;
     end;
     //MultiLote.Down := lEnabled;
     WO_NUMBER.Enabled := not lEnabled;
     WK_PIEZAS.Enabled := not lEnabled;
end;

procedure TRegistroWorks.AgregarMultiLotes;
var
   oActual : TBookMark;
   iFolio : Integer;
   sHoraActual : string;
   sCbArea:string;
   sOPNumber:string;

begin
     with dmLabor do
     begin
          cdsMultiLote.First;       // El Primero ya existe en el registro actual
          sHoraActual := WK_HORA_R.Valor;
          sCbArea := CB_AREA.Llave;
          sOPNumber := OP_NUMBER.Llave;
          cdsMultiLote.Next;
          with cdsWorks do
          begin
               iFolio := 1;
               FieldByName( 'WK_FOLIO' ).AsInteger := iFolio;
               FTipoTMuerto := FieldByName( 'WK_TMUERTO' ).AsString;
               SetOtrosDatos( TRUE );
               oActual := GetBookMark;
               DisableControls;
               Post;
               try
                  while not cdsMultiLote.EOF do
                  begin
                       Inc( iFolio );
                       Append;      // Agregar Datos
                       FieldByName( 'WK_HORA_R' ).AsString := sHoraActual;
                       FieldByName( 'WO_NUMBER' ).AsString := cdsMultiLote.FieldByName( 'WO_NUMBER' ).AsString;
                       FieldByName( 'AR_CODIGO' ).AsString := cdsMultiLote.FieldByName( 'AR_CODIGO' ).AsString;
                       FieldByName( 'WK_PIEZAS' ).AsFloat := cdsMultiLote.FieldByName( 'CW_PIEZAS' ).AsFloat;
                       FieldByName( 'OP_NUMBER' ).AsString := sOPNumber;
                       FieldByName( 'WK_STATUS' ).AsInteger := WK_STATUS.ItemIndex;
                       FieldByName( 'WK_MOD_1' ).AsString := WK_MOD_1.Llave;
                       FieldByName( 'WK_MOD_2' ).AsString := WK_MOD_2.Llave;
                       FieldByName( 'WK_MOD_3' ).AsString := WK_MOD_3.Llave;
                       FieldByName( 'CB_AREA' ).AsString := sCbArea;
                       FieldByName( 'WK_FOLIO' ).AsInteger := iFolio;

                       SetOtrosDatos( TRUE );
                       Post;
                       cdsMultiLote.Next;
                  end;
                  if dmLabor.FCambiosWorks  then
                  GotoBookMark( oActual );
               finally
                  EnableControls;
                  FreeBookMark( oActual );
               end;
          end;
     end;
end;

procedure TRegistroWorks.SetOtrosDatos( const lMultiLote: Boolean );
begin
     // Para que formas que heredan de esta cambien los campos que requieran
end;

procedure TRegistroWorks.SetControlesVisibles;
const
     DOSPUNTOS = ':';
begin
     LblArea.Caption := GetLaborLabel( K_GLOBAL_LABOR_AREA ) + DOSPUNTOS;
     LblCantidad.Caption := Global.GetGlobalString( K_GLOBAL_LABOR_PIEZAS );
     LblCantidad.Visible := strLleno( LblCantidad.Caption );
     WK_PIEZAS.Visible := LblCantidad.Visible;
     LblCantidad.Caption := LblCantidad.Caption + DOSPUNTOS;
     SetPanelLabel( PanelOrden, LblOrden, K_GLOBAL_LABOR_ORDEN );
     SetPanelLabel( PanelParte, LblParte, K_GLOBAL_LABOR_PARTE );
     SetPanelLabel( PanelOpera, LblOpera, K_GLOBAL_LABOR_OPERACION );
     SetPanelLabel( PanelModula1, LblModula1, K_GLOBAL_LABOR_MODULA1 );
     SetPanelLabel( PanelModula2, LblModula2, K_GLOBAL_LABOR_MODULA2 );
     SetPanelLabel( PanelModula3, LblModula3, K_GLOBAL_LABOR_MODULA3 );
     HabilitaPanel( PanelParte, ( not PanelOrden.Visible ) );
end;

procedure TRegistroWorks.SetControlesOperacion;
var
   lEnabled, lActivado : boolean;
begin
     lEnabled := ( ( CB_AREA.Llave <> VACIO ) and ( not( zStrToBool( dmLabor.cdsArea.FieldByName( 'TB_OP_UNI' ).AsString ) ) ) ) or ( CB_AREA.Llave = VACIO );
     lActivado := OP_NUMBER.Enabled;

     if ( ActiveControl <> nil ) and ( ActiveControl.ClassName = 'TZetaLlave' ) and ( ActiveControl.Parent = OP_NUMBER ) and  ( Not lEnabled )  then
        FControlOperacion.SetFocus;
     OP_NUMBER.Enabled := lEnabled;
     lblOpera.Enabled := lEnabled;
     if FControlOperacion.Focused and lEnabled and ( not lActivado ) then
        OP_NUMBER.SetFocus;
end;

procedure TRegistroWorks.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( PanelOpera.Visible ) and ( ( Field = nil ) or ( Field.FieldName = 'CB_AREA' ) ) then
     begin
          SetControlesOperacion;
     end;
end;

procedure TRegistroWorks.OK_DevExClick(Sender: TObject);
begin
 if FMultiLote then
        AgregarMultiLotes
     else
        SetOtrosDatos( FALSE );                  // Para formas que heredan de esta pongan sus datos
     ValidaDatosLabor;
     inherited;                                  // Se graba igual aunque sean MultiLotes
     if dmLabor.cdsWorks.ChangeCount = 0 then    // Se Aplicaron los cambios
     begin
          Close;
          {
          SetInfoNuevoRegistro;
          HabilitaControles;
          }
     end;
end;

procedure TRegistroWorks.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.



