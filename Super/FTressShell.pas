unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::                                                                          
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa principal de Supervisores.exe     ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics,Variants,
     Controls, Forms, Dialogs, ComCtrls,
     ZBaseShell, ExtCtrls, ActnList, Menus, ImgList, Buttons, ToolWin,
     ZetaSmartLists, ZetaKeyCombo, StdCtrls, Mask, ZetaFecha,
     ZetaStateComboBox, Db, Grids, DBGrids, ZetaDBGrid, DBCtrls,FMapasProduccion,
     ZetaClientDataset, ZetaTipoEntidad, ZetaCommonLists, ZBaseConsulta,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxPC,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsForm, cxButtons, cxNavigator, cxDBNavigator,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxBar,
  cxClasses, dxRibbon, cxLocalization, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxTextEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGridLevel, cxGrid, ZetaCXGrid,dxCore, dxStatusBar,
  dxRibbonStatusBar, System.Actions, dxRibbonCustomizationForm;
type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);
type
  TTressShell = class(TBaseShell)                          
    SuperImages: TImageList;
    ActionList: TActionList;
    _A_AjustarAsistencia: TAction;
    _A_AutorizacionColectiva: TAction;
    _A_Permisos: TAction;
    _A_Kardex: TAction;
    _A_ExploradorReportes: TAction;
    _A_Prenomina: TAction;
    _A_Imprimir: TAction;
    _A_ImprimirForma: TAction;
    _A_ConfigurarImpresora: TAction;
    _A_Bitacora: TAction;
    _A_Servidor: TAction;
    _A_Cache: TAction;
    _A_SalirSistema: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    _E_Refrescar: TAction;
    _E_BuscarEmpleado: TAction;
    _E_CambiarEmpleado: TAction;
    _H_Contenido: TAction;
    _H_Glosario: TAction;
    _H_UsoTeclado: TAction;
    _H_ComoUsarAyuda: TAction;
    _H_AcercaDe: TAction;
    EscogeImpresora: TPrinterSetupDialog;
    ImageEmp: TImageList;
    _A_AsignarEmpleados: TAction;
    _A_DatosEmpleado: TAction;
    _A_ClaveAcceso: TAction;
    _A_OtraEmpresa: TAction;
    _A_OtroUsuario: TAction;
    _E_Filtro: TAction;
    _E_Condicion: TAction;
    dsEmpleados: TDataSource;
    PanelPa: TPanel;
    PanelParam: TPanel;
    PaEmpLbl: TLabel;
    Label1: TLabel;
    PaEmp: TStateComboBox;
    PanelFecha: TPanel;
    PaFechaDiaLbl: TLabel;
    PaFecha: TZetaFecha;
    CB_FiltroFijo: TZetaKeyCombo;
    PanelEmp: TPanel;
    Splitter2: TSplitter;
    _L_LaborOperaciones: TAction;
    _L_Labor: TAction;
    _L_LaborIndividual: TAction;
    _L_Especiales: TAction;
    _L_TiempoMuerto: TAction;
    _L_Cedulas: TAction;
    _L_TMuertoIndividual: TAction;
    _L_AsignarArea: TAction;
    ImageEmp2: TImageList;
    ImageStatus: TImageList;
    _L_RegistrodeBreaks: TAction;
    _L_CancelacionBreaks: TAction;
    _A_AutorizacionesPorAprobar: TAction;
    _A_Vacaciones: TAction;
    _A_CambioMasivoTurnos: TAction;
    PanelSB: TPanel;
    _A_ClasificacionesTemporales: TAction;
    _L_KardexDiario: TAction;
    _A_Exportar: TAction;
    _A_Autorizar_Prenomina: TAction;
    _A_Plan_Vacaciones: TAction;
    _A_Cursos_Tomados: TAction;
    _A_Cursos_Prog: TAction;
    _A_Grupos: TAction;
    _A_Layout: TAction;
    _A_Transferencias: TAction;
    _A_Aprobar_Cancelar_Transferencias: TAction;
    _A_PlanCapacitacion: TAction;
    _A_CompetenciasEvaluadas: TAction;
    _A_Matriz_Habilidades: TAction;
    Panel1: TPanel;
    Panel2: TPanel;
    PanelPrinc: TPanel;
    lblTitulo: TLabel;
    BuscarFormaBtn_DevEx: TcxButton;
    SmallImageListSuper: TcxImageList;
    SBAgregar_DevEx: TcxButton;
    SBBorrar_DevEx: TcxButton;
    SBModificar_DevEx: TcxButton;
    RefrescarBtn_DevEx: TcxButton;
    SBImprimir_DevEx: TcxButton;
    SBExportar_DevEx: TcxButton;
    PaEmpNav_DevEx: TcxDBNavigator;
    BotonCondiciones_DevEx: TcxButton;
    dxBarManager1: TdxBarManager;
    TabArchivo: TdxRibbonTab;
    TabAsistencia: TdxRibbonTab;
    TabExpediente: TdxRibbonTab;
    TabCapacitacion: TdxRibbonTab;
    TabCosteo: TdxRibbonTab;
    TabLabor: TdxRibbonTab;
    BarraEmpresa: TdxBar;
    BarraPendienteVacia1: TdxBar;
    BarraCapacitacionVacia1: TdxBar;
    BarraTransferencias: TdxBar;
    BarraUbicacion: TdxBar;
    BarraConsultas: TdxBar;
    BarraImpresora: TdxBar;
    BarraConfiguracion: TdxBar;
    BarraSalir: TdxBar;
    TabArchivo_OtraEmpresa: TdxBarLargeButton;
    TabArchivo_BtnExploradorReportes: TdxBarLargeButton;
    TabArchivo_Bitacora: TdxBarLargeButton;
    TabArchivo_Imprimir: TdxBarButton;
    TabArchivo_ImprimirForma: TdxBarButton;
    TabArchivoImpresora: TdxBarButton;
    TabArchivo_ClaveAcceso: TdxBarLargeButton;
    TabArchivo_Cabiar: TdxBarLargeButton;
    TabArchivo_Salir: TdxBarLargeButton;
    TabPrincipal_Agregar: TdxBarLargeButton;
    TabPrincipal_Buscar: TdxBarLargeButton;
    TabPrincipal_Refrescar: TdxBarLargeButton;
    TabPrincipal_Exportar: TdxBarLargeButton;
    TabPrincipal_Editar: TdxBarLargeButton;
    TabPrincipal_Borrar: TdxBarLargeButton;
    _E_BuscaCodigo: TAction;
    BarraPrincipalVacia1: TdxBar;
    BarraAsistenciaVacia1: TdxBar;
    TabPrincipal_DatosEmpleado: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    BarraPrincipalVacia2: TdxBar;
    dxBarButton1: TdxBarButton;
    TabAsistencia__AsistDiaria: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    BarraAsistenciaVacia2: TdxBar;
    BarraAsistenciaVacia3: TdxBar;
    BarraPrenomina: TdxBar;
    BarraExpedienteVacia2: TdxBar;
    dxBarButton2: TdxBarButton;
    TabAsistencia_ClasTemp: TdxBarLargeButton;
    TabAsistencia_AuColectiva: TdxBarLargeButton;
    TabAsistencia_Consltar: TdxBarLargeButton;
    TabAsistencia_Autorizar: TdxBarLargeButton;
    TabPrincipal_AsigTemp: TdxBarLargeButton;
    BarraCapacitacionVacia2: TdxBar;
    BarraExpedienteVacaciones: TdxBar;
    TabExpediente_Kardex: TdxBarLargeButton;
    TabExpediente_TurnosMasivos: TdxBarLargeButton;
    TabExpediente_Permisos: TdxBarLargeButton;
    TabExpediente_Plan: TdxBarLargeButton;
    TabExpediente_RegistroVaca: TdxBarLargeButton;
    BarraCursos: TdxBar;
    BaraCompetencias: TdxBar;
    BarraCedulas: TdxBar;
    TabCapa_Grupos: TdxBarLargeButton;
    dxBarButton3: TdxBarButton;
    TabCapa_CursosProg: TdxBarLargeButton;
    TabCapa_RegyCons: TdxBarLargeButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    TabCapa_PLanCapa: TdxBarLargeButton;
    TabCapa_CompEval: TdxBarLargeButton;
    TabCapa_MatrzHabil: TdxBarLargeButton;
    dxBarButton6: TdxBarButton;
    TabCapa_LayProd: TdxBarLargeButton;
    TabCosteo_RegyConsulta: TdxBarLargeButton;
    TabCosteo_AproboCancel: TdxBarLargeButton;
    BarraIndividual: TdxBar;
    BarraBreaks: TdxBar;
    TabLabor_KDiario: TdxBarLargeButton;
    TabLabor_AsigaArea: TdxBarLargeButton;
    TabLabor_ConsYregCed: TdxBarLargeButton;
    TabLabor_RegProdCed: TdxBarLargeButton;
    TabLabor_EspecialesCed: TdxBarLargeButton;
    TabLabor_TMuertoCed: TdxBarLargeButton;
    TabLabor_OperacionesInd: TdxBarLargeButton;
    TabLabor_RegProdInd: TdxBarLargeButton;
    TabLabor_TMuertoInd: TdxBarLargeButton;
    TabLabor_RegBreaks: TdxBarLargeButton;
    TabLabor_CancelBreaks: TdxBarLargeButton;
    DevEx_ShellRibbonTab1: TdxRibbonTab;
    BarraAyuda: TdxBar;
    dxBarButton7: TdxBarButton;
    AyudaGlosario: TdxBarButton;
    dxBarButton9: TdxBarButton;
    AyudaCombinacionesTeclas: TdxBarLargeButton;
    AyudaUsandoAyuda: TdxBarLargeButton;
    BarraAcercaDe: TdxBar;
    dxBarLargeButton6: TdxBarLargeButton;
    _H_NuevoTress: TAction;
    DevEx_cxLocalizer: TcxLocalizer;
    TabAsistencia_AuPorAprovar: TdxBarLargeButton;
    EmpleadosImageList: TcxImageList;
    GridEmp: TZetaCXGrid;
    GridEmpLevel1: TcxGridLevel;
    GridEmpDBTableView1: TcxGridDBTableView;
    Imagen_CB_TIPO: TcxGridDBColumn;
    Imagen_Checadas: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_TIPO: TcxGridDBColumn;
    Checadas: TcxGridDBColumn;
    Retardos: TcxGridDBColumn;
    Status: TcxGridDBColumn;
    DataSource1: TDataSource;
    SortOrder: TcxGridDBColumn;
    _A_Matriz_Cursos: TAction;
    TabCapa_MatrzCursos: TdxBarLargeButton;
    _A_EvaluacionDiaria: TAction;
    dxBarManager1Bar1: TdxBar;
    TabExpediente_EvaluacionDiaria: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _SinImplementar(Sender: TObject);
    procedure _A_ClaveAccesoExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_CacheExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _A_ConfigurarImpresoraExecute(Sender: TObject);
    procedure EscogeImpresoraClose(Sender: TObject);
    procedure EscogeImpresoraShow(Sender: TObject);
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    //procedure _E_ContarEmpleadosExecute(Sender: TObject);
    procedure _E_CambiarEmpleadoExecute(Sender: TObject);
    procedure _E_FiltroExecute(Sender: TObject);
    procedure _H_ContenidoExecute(Sender: TObject);
    procedure _H_GlosarioExecute(Sender: TObject);
    procedure _H_UsoTecladoExecute(Sender: TObject);
    procedure _H_ComoUsarAyudaExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure _A_OtroUsuarioExecute(Sender: TObject);
    procedure BarraBotonesResize(Sender: TObject);
    procedure CB_FiltroFijoChange(Sender: TObject);
    procedure PaFechaValidDate(Sender: TObject);
    //procedure GridEmpDrawColumnCell(Sender: TObject; const Rect: TRect;
      //DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure _E_CondicionExecute(Sender: TObject);
    procedure PaEmpLookUp(Sender: TObject; var lOk: Boolean);
    procedure dsEmpleadosDataChange(Sender: TObject; Field: TField);
    procedure _A_DatosEmpleadoExecute(Sender: TObject);
    procedure _A_AsignarEmpleadosExecute(Sender: TObject);
    procedure _A_PrenominaExecute(Sender: TObject);
    procedure _A_BitacoraExecute(Sender: TObject);
    procedure _A_ExploradorReportesExecute(Sender: TObject);
    procedure _A_KardexExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _A_PermisosExecute(Sender: TObject);
    procedure _A_AjustarAsistenciaExecute(Sender: TObject);
    procedure _A_AutorizacionColectivaExecute(Sender: TObject);
    procedure _L_LaborExecute(Sender: TObject);
    procedure _L_LaborOperacionesExecute(Sender: TObject);
    procedure _L_LaborIndividualExecute(Sender: TObject);
    procedure _L_EspecialesExecute(Sender: TObject);
    procedure _L_TiempoMuertoExecute(Sender: TObject);
    procedure _L_CedulasExecute(Sender: TObject);
    procedure _L_TMuertoIndividualExecute(Sender: TObject);
    procedure _L_AsignarAreaExecute(Sender: TObject);
    procedure _L_RegistrodeBreaksExecute(Sender: TObject);
    procedure _L_CancelacionBreaksExecute(Sender: TObject);
    procedure _L_KardexDiarioExecute(Sender: TObject);
    procedure _A_AutorizacionesPorAprobarExecute(Sender: TObject);
    procedure PanelBotonesTopResize(Sender: TObject);
    procedure _A_VacacionesExecute(Sender: TObject);
    procedure _A_CambioMasivoTurnosExecute(Sender: TObject);
    procedure _A_ClasificacionesTemporalesExecute(Sender: TObject);
    procedure _A_ExportarExecute(Sender: TObject);
    procedure _A_Autorizar_PrenominaExecute(Sender: TObject);
    procedure _A_Plan_VacacionesExecute(Sender: TObject);
    procedure _A_GruposExecute(Sender: TObject);
    procedure _A_Cursos_TomadosExecute(Sender: TObject);
    procedure _A_Cursos_ProgExecute(Sender: TObject);
    procedure _A_LayoutExecute(Sender: TObject);
    procedure _A_TransferenciasExecute(Sender: TObject);
    procedure _A_Aprobar_Cancelar_TransferenciasExecute(Sender: TObject);
    procedure _A_PlanCapacitacionExecute(Sender: TObject);
    procedure _A_CompetenciasEvaluadasExecute(Sender: TObject);
    procedure _A_Matriz_HabilidadesExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _H_NuevoTressExecute(Sender: TObject);
    procedure Imagen_CB_TIPOCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure Imagen_ChecadasCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure GridEmpDBTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure CB_CODIGOCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure Imagen_ChecadasHeaderClick(Sender: TObject);
    procedure _A_Matriz_CursosExecute(Sender: TObject);
    procedure _A_EvaluacionDiariaExecute(Sender: TObject);
  private
    { Private declarations }
    FShellVisible: Boolean;
    FFormaActiva: TBaseConsulta;
    FBotonActivo: TSpeedButton;
    function CheckDerechosSuper: Boolean;
    function GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean ): TBaseConsulta;
    function QtyBotonesConsulta(Barra : TToolBar): Integer;
    procedure CierraFormas;
    procedure QuitaBotonActivo;
    procedure CargaSuperActivos;
    procedure RefrescaSuperActivos;
    procedure RefrescaPaFechaLbl;
    procedure SetFormaActiva(Forma: TBaseConsulta; const lValidaDerechos : Boolean = TRUE );
    procedure ActivaFormaPanel(Titulo: String; InstanceClass: TBaseConsultaClass;
              const InstanceName: String; const iDerechosForma: Integer; const lValidaDerechos : Boolean = TRUE );
    //procedure RefrescaBotonActivo(Boton: TSpeedButton); //Edit by MP: Con la nueva interfaz ya no se utilizan
    //procedure SetBoton(Boton: TSpeedButton); //Edit by MP: Con la nueva interfaz ya no se utilizan
    procedure ReconectaFormaActiva;
    Procedure CargaVista;
    procedure CargaTraducciones;
    function ConteoEmpleadosPrestados: Integer;
    procedure ConteoEmpleados;
  protected
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure LogOff; override;
    procedure CloseShell; override;
    procedure HabilitaControles; override;
    //(@am): Se remueve para vs 2015. Por que se requirio mostrar la bienvenida unicamente en TRESS.
    //function CheckVersionDatos:Boolean; override;
  public
    { Public declarations }
    property FormaActiva: TBaseConsulta read FFormaActiva;
    function Inicializa(bOtroUser: Boolean): Boolean;
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet; override;
    procedure AsistenciaFechavalid(fecha : tdate);
    {$ifdef MULTIPLES_ENTIDADES}
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades);
    {$endif}
    procedure SetControlFiltroFijo;
    function VerificaDerechoConsulta( const iDerechosForma: Integer): Boolean;
  end;

const
     K_PANEL_CONTEO_EMPLEADOS=1;
     K_RETARDOS_INDICE = 5;
     K_VACACION_INDICE = 6;
     K_ASISTECIA_INDICE=2;
     K_FALTA_INDICE = 3;
     K_INCAPACITA_INDICE = 4;
var
  TressShell: TTressShell;

function BuscaDialogo( Entidad : TipoEntidad; s, sLLave, sDescripcion : string) : Boolean;

implementation
//{$R Spanish.RES}

uses dGlobal, dSuper, DCliente, DCatalogos, DTablas, DDiccionario, DSistema,
     DReportes, DConsultas, DNomina, DLabor,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
{$endif}
     ZetaCommonTools, ZetaDialogo, ZetaRegistryCliente, ZetaBuscaEmpleado_DevEx,{ZetaBuscaEmpleado, }
     FCondiciones, ZAccesosTress, ZBaseDlgModal,ZBaseDlgModal_DevEx, ZetaClientTools,
     ZGlobalTress, ZetaCommonClasses, ZAccesosMgr,
     FAutoClasses, ZetaAcercaDe_DevEx, ZetaSplash, //FActivaCache,
     FReportes_DevEx, FConsultaEmpleados, FAsignarEmpleados,
     FSistBitacora_DevEx, FKardex, FPrenomina,  {FHisVacacion,} FHisVacacion_DevEx, {FClasifTemporal,} FClasifTemporal_DevEx,
     FPermisos, FAsistTarjeta, FWizAsistAjusteColectivo_DevEx,
     FWorksSuper, FCedulas, ZetaLaborTools, FWizLabCancelarBreaks,ZCXWizardBasico,
     FWizEmpCambioTurno, FKardexDiario, FPlanVacacion_DevEx, FGridSesiones, {FHisCursos,} FHisCursos_DevEx, {FHisCursosProg,}FHisCursosProg_DevEx,
     FLayoutProduccion, FSuperCosteoTransferencias, ZToolsPE, {FHisPlanCapacitacion,} FHisPlanCapacitacion_DevEx, {FHisEvaluaCompeten,} FHisEvaluaCompeten_DevEx,
     FMatrizHabilidadesSup, FMatrizCursos_DevEx{$ifdef COMMSCOPE},FGridEvaluacionDiaria{$endif};


{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmReportes := TdmReportes.Create( self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmSuper := TdmSuper.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmConsultas := TdmConsultas.Create( self );
     dmNomina := TdmNomina.Create( Self );
     dmLabor := TdmLabor.Create( self );
     inherited;
     HelpContext := 1;
     FShellVisible:= TRUE;
     EscogeImpresora.HelpContext := H00010_Impresora;
     WindowState:= wsMaximized;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     dmDiccionario.Free;
     dmSuper.Free;
     dmTablas.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmReportes.Free;
     dmConsultas.Free;
     dmNomina.Free;
     dmLabor.Free;
     inherited;
     dmCliente.Free;
     Global.Free;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
{$endif}
end;

{ ************ Metodos del Shell ************** }

function TTressShell.Inicializa( bOtroUser: Boolean ): Boolean;
var
   lOk : Boolean;
   sMensaje: String;
begin
     //OLD
     {SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
     lOk:= True;

     if ( lOk and bOtroUser ) then
     begin
          lOk:= Login( False );
     end;

     with SplashScreen do
     begin
          Close;
          Free;
     end;}

     {***DevEx (by am): Invocacion al nuevo metodo de LogIn ***}
     lOk:= True;
     if ( lOk and bOtroUser ) then
     begin
          lOk:= Login( False );
     end;
     {***}

     lOk := ( lOk and dmCliente.ModuloAutorizado( okSupervisores ) );
     with dmSuper do
     begin
          LaborActivado := dmCliente.ModuloAutorizadoConsulta( okLabor, sMensaje );
         TabLabor.Visible := LaborActivado;             // Visualiza el Sub-Men� de Labor
          _L_Labor.Visible := LaborActivado;           // Se Controla desde el Action Control
          _L_Labor.Enabled := LaborActivado;

          _L_Cedulas.Visible := LaborActivado;
          _L_Cedulas.Enabled := LaborActivado;

          _L_AsignarArea.Visible := LaborActivado;
          _L_AsignarArea.Enabled := LaborActivado;

          _L_KardexDiario.Visible := LaborActivado;
     end;

     if lOk then
     begin
          Show;
          Update;
          BeforeRun;   { Abre Compa�ia }
//          lOk := EmpresaAbierta;       Aunque no se Abra Compa�ia se dejar� dentro del Sistema
     end;

     Result := lOk;
     GridEmpDBTableview1.ApplyBestFit();
end;

procedure TTressShell.DoOpenAll;
const
     K_MENU_LABOR_ASIGNAR = '%sAsignar %s a Empleados';
     K_MENU_KARDEX_AREAS  = 'Karde%sx Diario de %s';
begin
     try
        inherited DoOpenAll;

        if ( not CheckDerechosSuper ) then
           db.DataBaseError( 'No tiene derechos para utilizar este ejecutable en esta Empresa' );
        Global.Conectar;

        {AV: 2010-11-16 Asigna El Global Usar Validacion Status Activo de Tablas y Cat�logos al ZetaClienteDataSet }
        ZetaClientDataSet.GlobalSoloKeysActivos := Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS );
        ZetaClientDataSet.GlobalListaConfidencialidad := dmCliente.Confidencialidad;

        with dmCliente do
        begin
             InitActivosSupervisor;   // FechaDefault := Date;
             InitActivosPeriodo;
             InitArrayTPeriodo;//acl
             //Definir el exe en el que se esta, para saber que arbol de usuario cargar
             dmSistema.FTipo_Exe_DevEx := Ord( taSuper );
        end;
        with dmSuper do
        begin
             ResetFiltros;
             AsistenciaStatusLimite := Global.GetGlobalInteger(K_LIMITE_MODIFICAR_ASISTENCIA);
             NivelSupervisor := Global.GetGlobalInteger(K_GLOBAL_NIVEL_SUPERVISOR);
             VerBajas := ZAccesosMgr.CheckDerecho( D_SUPER_VER_BAJAS, K_DERECHO_CONSULTA );
             //ZetaBuscaEmpleado.SetOnlyActivos( not VerBajas ); //old
             ZetaBuscaEmpleado_DevEx.SetOnlyActivos( not VerBajas ); //DevEx (by am): Se invoca la nueva busqueda

              //DevEx(@am): Correccion de la validacion de esta opcion. Para mas informacion revisar el Bug 6162 en TP.
             //_A_AjustarAsistencia.enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_PERMISOS, D_SUPER_AJUSTAR_ASISTENCIA ); //OLD
             _A_AjustarAsistencia.enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_AJUSTAR_ASISTENCIA, K_DERECHO_CONSULTA );
             _A_Permisos.Enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_PERMISOS, K_DERECHO_CONSULTA );
             _L_Cedulas.Enabled :=  ZAccesosMgr.CheckDerecho(  D_SUPER_CEDULAS, K_DERECHO_CONSULTA );
             _L_Labor.Enabled:= ZAccesosMgr.CheckDerecho(  D_SUPER_LABOR, K_DERECHO_CONSULTA );
             _L_AsignarArea.Enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_ASIGNAR_AREAS, K_DERECHO_CONSULTA );
             _L_KardexDiario.Enabled:=   ZAccesosMgr.CheckDerecho(  D_SUPER_ASIGNAR_AREAS, K_DERECHO_CONSULTA );
             _A_Layout.Enabled := ZAccesosMgr.CheckDerecho(  D_LAY_PRODUCCION, K_DERECHO_CONSULTA ) ;
             _A_Cursos_Tomados.Enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_CURSOS_TOMADOS, K_DERECHO_CONSULTA ) ;
             _A_Cursos_Prog.Enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_CURSOS_PROGRAMADOS, K_DERECHO_CONSULTA ) ;
             _A_Grupos.Enabled:= ZAccesosMgr.CheckDerecho(  D_SUPER_GRUPOS, K_DERECHO_CONSULTA ) ;
             _A_Vacaciones.Enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_VACACIONES, K_DERECHO_CONSULTA ) ;
             _A_Kardex.Enabled:= ZAccesosMgr.CheckDerecho(  D_SUPER_KARDEX, K_DERECHO_CONSULTA ) ;
             _A_DatosEmpleado.Enabled := ZAccesosMgr.CheckDerecho(  D_SUPER_DATOS_EMPLEADOS, K_DERECHO_CONSULTA ) ;
             _A_Prenomina.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_CONSULTAR_PRENOMINA, K_DERECHO_CONSULTA ) ;
             _A_AsignarEmpleados.enabled := ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_EMPLEADOS, K_DERECHO_CONSULTA ) ;
             _A_ClasificacionesTemporales.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_CLASIFI_TEMP, K_DERECHO_CONSULTA );
             _A_AutorizacionColectiva.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_AUTORIZACION_COLECTIVA, K_DERECHO_CONSULTA );
             _L_RegistrodeBreaks.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_REGISTRO_BREAKS, K_DERECHO_CONSULTA );
             _L_CancelacionBreaks.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_CANCELA_BREAKS, K_DERECHO_CONSULTA );
             _A_Cache.Enabled := _A_Cache.Enabled and ZAccesosMgr.CheckDerecho( D_SUPER_ACTIVA_CACHE, K_DERECHO_CONSULTA );
             if( Global.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES ) and ZAccesosMgr.CheckDerecho(   D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL,APROBAR_AUTORIZACION ))
             then
             _A_AutorizacionesPorAprobar.Enabled :=  true
             else
             _A_AutorizacionesPorAprobar.Enabled :=  false;
             _A_CambioMasivoTurnos.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_CAMBIO_TURNO, K_DERECHO_CONSULTA );
             _A_Autorizar_Prenomina.Enabled := ZAccesosMgr.RevisaCualquiera( D_SUPER_AUTORIZAR_PRENOMINA);
             {$ifdef COMMSCOPE}
             _A_EvaluacionDiaria.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_AUTORIZAR_EVALUACION , K_DERECHO_CONSULTA);
             {$endif}
             _A_Plan_Vacaciones.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_PLANVACACION, K_DERECHO_CONSULTA );
             _A_Transferencias.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_TRANSFERENCIAS, K_DERECHO_CONSULTA );
             _A_Aprobar_Cancelar_Transferencias.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_APROBAR_TRANSFERENCIAS, K_DERECHO_CONSULTA );
             _A_Matriz_Cursos.Enabled := (ZAccesosMgr.CheckDerecho( D_SUP_MAT_CURSOS, K_DERECHO_CONSULTA ));
             {$IFNDEF DOS_CAPAS}
             _A_PlanCapacitacion.Enabled := ( ZAccesosMgr.CheckDerecho( D_SUP_PLAN_CAPACITA, K_DERECHO_CONSULTA ) ) and ( not dmCliente.Sentinel_EsProfesional_MSSQL );
             _A_CompetenciasEvaluadas.Enabled := ( ZAccesosMgr.CheckDerecho( D_SUP_EVAL_COMPETEN, K_DERECHO_CONSULTA ) ) and ( not dmCliente.Sentinel_EsProfesional_MSSQL );
             _A_Matriz_Habilidades.Enabled := (ZAccesosMgr.CheckDerecho( D_SUP_MAT_HBLDS, K_DERECHO_CONSULTA )) and ( not dmCliente.Sentinel_EsProfesional_MSSQL );
             {$ELSE}
             _A_PlanCapacitacion.enabled := False;
             _A_CompetenciasEvaluadas.enabled := False;
             _A_Matriz_Habilidades.enabled := False;
             {$ENDIF}
        end;

      

        dmLabor.SetNombreLookups;
        CargaSuperActivos;            // RefrescaFecha;
        if dmSuper.LaborActivado then
        begin
             _L_LaborIndividual.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_LABOR, K_DERECHO_ALTA );
             _L_TMuertoIndividual.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_LABOR, K_DERECHO_ALTA );
             _L_LaborOperaciones.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_CEDULAS, K_DERECHO_ALTA );
             _L_TiempoMuerto.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_CEDULAS, K_DERECHO_ALTA );
             _L_Especiales.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_CEDULAS, K_DERECHO_ALTA );
  
             _L_AsignarArea.Caption := Format( K_MENU_LABOR_ASIGNAR, [ '&', GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] );
            // _L_AsignarArea.Hint := Format( K_MENU_LABOR_ASIGNAR, [ VACIO, btnAsignarArea.Caption ] );
        
             //BotonArea.Caption := VACIO;
             { Bot�n de Kardex de Areas Diario }
             //BotonKardexDiario.Caption := GetLaborLabel( K_GLOBAL_LABOR_AREA );
             _L_KardexDiario.Caption := Format( K_MENU_KARDEX_AREAS, [ '&', GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] );
             _L_KardexDiario.Hint := Format( K_MENU_KARDEX_AREAS, [ VACIO, GetLaborLabel( K_GLOBAL_LABOR_AREA ) ] );
            // BotonKardexDiario.Hint := _L_KardexDiario.Hint;
        end;
         //DevEx (by am): Se actualiza el conteo de empleados en el statusBar
         ConteoEmpleados;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          CierraDatasets( dmNomina );
          CierraDatasets( dmLabor );
          CierraDataSets( dmSuper );
          CierraDatasets( dmReportes );
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmTablas );
          CierraDatasets( dmCatalogos );
          CierraDatasets( dmSistema );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.HabilitaControles;
var
   i : Integer;
begin
     inherited HabilitaControles;
    // Editar.Enabled := EmpresaAbierta;
    // Supervisores1.Enabled := EmpresaAbierta;
  //Dehabilita Actions del Shell
    _A_ExploradorReportes.Enabled:=EmpresaAbierta;
    _A_Bitacora.Enabled:=EmpresaAbierta;
    _E_Refrescar.Enabled:=EmpresaAbierta;
    _E_BuscaCodigo.Enabled:=EmpresaAbierta;
    _A_DatosEmpleado.Enabled:=EmpresaAbierta;
    _A_AsignarEmpleados.Enabled:=EmpresaAbierta;
    _A_AjustarAsistencia.Enabled:=EmpresaAbierta;
    _A_AutorizacionCOlectiva.Enabled:=EmpresaAbierta;
    _A_ClasificacionesTemporales.Enabled:=EmpresaAbierta;
    _A_Prenomina.Enabled:=EmpresaAbierta;
    _A_Autorizar_Prenomina.Enabled:=EmpresaAbierta;
    _A_Kardex.Enabled:=EmpresaAbierta;
    _A_CambioMasivoTurnos.Enabled:=EmpresaAbierta;
    _A_PErmisos.Enabled:=EmpresaAbierta;
    _A_Plan_Vacaciones.Enabled:=EmpresaAbierta;
    _A_VAcaciones.Enabled:=EmpresaAbierta;
    _A_Grupos.Enabled:=EmpresaAbierta;
    _A_Cursos_prog.Enabled:=EmpresaAbierta;
    _A_Cursos_tomados.Enabled:=EmpresaAbierta;
    _A_PlanCapacitacion.Enabled:=EmpresaAbierta;
    _A_CompetenciasEvaluadas.Enabled:=EmpresaAbierta;
    _A_Matriz_Habilidades.Enabled:=EmpresaAbierta;
    _A_Layout.Enabled:=EmpresaAbierta;
    _A_Transferencias.Enabled:=EmpresaAbierta;
    _A_Aprobar_Cancelar_Transferencias.Enabled:=EmpresaAbierta;
    _L_KardexDiario.Enabled:=EmpresaAbierta;
    _L_AsignarArea.Enabled:=EmpresaAbierta;
    _L_Cedulas.Enabled:=EmpresaAbierta;
    _L_LaborOperaciones.Enabled:=EmpresaAbierta;
    _L_TiempoMuerto.Enabled:=EmpresaAbierta;
    _L_Especiales.Enabled:=EmpresaAbierta;
    _L_Labor.Enabled:=EmpresaAbierta;
    _L_LaborIndividual.Enabled:=EmpresaAbierta;
    _L_TMuertoIndividual.Enabled:=EmpresaAbierta;
    _L_RegistrodeBreaks.Enabled:=EmpresaAbierta;
    _L_CancelacionBreaks.Enabled:=EmpresaAbierta;
    _A_AutorizacionesPorAprobar.Enabled:=EmpresaAbierta;
  //findeshabilitaActions

     for i := 0 to PanelParam.ControlCount - 1 do
         PanelParam.Controls[i].Enabled := EmpresaAbierta;
     _A_Cache.Enabled := EmpresaAbierta;
     _A_ExploradorReportes.Enabled := EmpresaAbierta;
     _E_Refrescar.Enabled := EmpresaAbierta;
     if EmpresaAbierta then
     begin
          _A_Servidor.Enabled := ClientRegistry.CanWrite;
          _A_Cache.Enabled := ClientRegistry.CanWrite;
     end;
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean): Boolean;
begin
     Result := ( eEntidad = enEmpleado );
     if Result then
        lEncontrado := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) //DevEx (by am): Se invoca la nueva busqueda
        //lEncontrado := ZetaBuscaEmpleado.BuscaEmpleadoDialogo( '', sKey, sDescription )   //old
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          enClasifi: Result := dmCatalogos.cdsClasifi;
          enEmpleado: Result := dmCliente.cdsEmpleadoLookUp;
          enConcepto: Result := dmCatalogos.cdsConceptos;
          enHorario: Result := dmCatalogos.cdsHorarios;
          enIncidencia: Result := dmTablas.cdsIncidencias;
          enNivel1: Result := dmTablas.cdsNivel1;
          enNivel2: Result := dmTablas.cdsNivel2;
          enNivel3: Result := dmTablas.cdsNivel3;
          enNivel4: Result := dmTablas.cdsNivel4;
          enNivel5: Result := dmTablas.cdsNivel5;
          enNivel6: Result := dmTablas.cdsNivel6;
          enNivel7: Result := dmTablas.cdsNivel7;
          enNivel8: Result := dmTablas.cdsNivel8;
          enNivel9: Result := dmTablas.cdsNivel9;
          {$ifdef ACS}
          enNivel10: Result := dmTablas.cdsNivel10;
          enNivel11: Result := dmTablas.cdsNivel11;
          enNivel12: Result := dmTablas.cdsNivel12;
          {$endif}
          enPuesto: Result := dmCatalogos.cdsPuestos;
          enQuerys: Result := dmCatalogos.cdsCondiciones;
          enTKardex: Result := dmTablas.cdsMovKardex;
          enTurno: Result := dmCatalogos.cdsTurnos;
          enUsuarios: Result := dmSistema.cdsUsuarios;
          enNomParam: Result := dmCatalogos.cdsNomParam;
          enMotAuto: Result :=  dmTablas.cdsMotAuto;
          { Labor }
          enOpera: Result := dmLabor.cdsOpera;
          enPartes: Result := dmLabor.cdsPartes;
          enWorder: Result := dmLabor.cdsWorderLookup;
          enModula1: Result := dmLabor.cdsModula1;
          enModula2: Result := dmLabor.cdsModula2;
          enModula3: Result := dmLabor.cdsModula3;
          enTMuerto: Result := dmLabor.cdsTiempoMuerto;
          enArea: Result := dmLabor.cdsArea;
          enCurso: Result := dmCatalogos.cdsCursos;
          enEstablecimiento : Result := dmCatalogos.cdsEstablecimientos; 
     else
         Result := nil;
     end;
end;

{ Valores Activos }

procedure TTressShell.CargaSuperActivos;
begin
     PaFecha.Valor := dmCliente.FechaSupervisor;
     SetControlFiltroFijo;
     RefrescaPaFechaLbl;
     RefrescaSuperActivos;
end;

procedure TTressShell.SetControlFiltroFijo;
begin
     CB_FiltroFijo.ItemIndex := dmSuper.FiltroFijo; // el OnChange se encarga de inicializar FiltroFijo := 0;
end;

procedure TTressShell.RefrescaSuperActivos;
begin
     dmSuper.RefrescaMisEmpleados;
end;

procedure TTressShell.RefrescaPaFechaLbl;
begin
     PaFechaDiaLbl.Hint:= ObtieneElemento( lfDiasSemana, DayOfWeek(dmCliente.FechaSupervisor)-1 );
     PaFechaDiaLbl.Caption := Copy(PaFechaDiaLbl.Hint,1,3) + '.';
end;

//DevEx (by am): Conteo de empleados prestados
function TTressShell.ConteoEmpleadosPrestados: Integer;
var
 iEmpPrestados, iEmpleadoNumero:Integer;
const
     K_EMPLEADO_PRESTADO=2;
begin
     iEmpPrestados:=0;
     with dmSuper do
     begin
          cdsEmpleados.DisableControls;
          try
               iEmpleadoNumero:= EmpleadoNumero;
               if  ( cdsEmpleados.Active ) and ( cdsEmpleados.RecordCount>0 )then
               begin
                    //Iniciar desde el primer registro
                    cdsEmpleados.First;
                    //Recorrer cdsEmpleados
                    while not cdsEmpleados.Eof do
                    begin
                         if( cdsEmpleados.FieldByName('CB_TIPO').AsInteger=K_EMPLEADO_PRESTADO ) then
                             iEmpPrestados:= iEmpPrestados+1;
                         cdsEmpleados.Next;
                    end;
                    //Devolder el cursor
                    cdsEmpleados.Locate('CB_CODIGO',iEmpleadoNumero,[]);
               end;
          finally
                 cdsEmpleados.EnableControls;
          end;
     end;
     Result:= iEmpPrestados;
end;

//DevEx (by am): Conteo de empleados
procedure TTressShell.ConteoEmpleados;
var
   TotalEmp, PrestEmp:Integer;
begin
     if (dmSuper<>nil) then
     begin
          if (dmSuper.cdsEmpleados<> nil) then
          begin
               if dmSuper.cdsEmpleados.Active then
               begin
                    TotalEmp:= dmSuper.cdsEmpleados.RecordCount;
                    PrestEmp:= ConteoEmpleadosPrestados;
                    StatusBarMsg('Total Empleados: ' + IntToStr( TotalEmp )+'  Prestados: '+ IntToStr( PrestEmp )+ '  Asignados: '+ IntToStr (TotalEmp-PrestEmp), K_PANEL_CONTEO_EMPLEADOS);
                    //StatusBar.Panels[K_PANEL_CONTEO_EMPLEADOS].Canvas.TextWidth( StatusBar.Panels[K_PANEL_CONTEO_EMPLEADOS].Text );
               end;
          end;
     end;
end;

procedure TTressShell.dsEmpleadosDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if dmSuper.RefrescarFormaActiva then
        TressShell.SetDataChange( [ enEmpleado ] );
     PaEmp.ValorEntero := dmSuper.EmpleadoNumero;
     dmCliente.SetEmpleadoNumero( dmSuper.EmpleadoNumero );
end;

{ Control de Forma Activa }

procedure TTressShell.ReconectaFormaActiva;
begin
      if Assigned( FFormaActiva ) then
        FFormaActiva.Reconnect;
end;

function BuscaDialogo( Entidad : TipoEntidad; s, sLLave, sDescripcion : string) : Boolean;
begin
     Result := TRUE;
end;

procedure TTressShell.CloseShell;
begin
     with dmSuper do
     begin
          RefrescarFormaActiva:= FALSE;
          try
             if cdsEmpleados.Active then
             begin
                  cdsEmpleados.EmptyDataSet;
                  cdsEmpleados.EnableControls;      // Por si Alguna Forma lo Inhabilit�
             end;
          finally
             RefrescarFormaActiva:= TRUE;
          end;
     end;
     //dmCliente.ResetLookupEmpleado;            // Por si Una Forma Modal lo Cre�
     FShellVisible:= FALSE;
     try
        //CierraTodo;
        inherited CloseShell;
     finally
        FShellVisible:= TRUE;
     end;
end;

procedure TTressShell.LogOff;
begin
//     inherited;
     if DesconectaUsuario then
     begin
          CloseShell;
          if not Inicializa( True ) then
             Application.Terminate
          else
          begin
               Application.Restore;
               Application.BringToFront;
          end;
     end;
end;

procedure TTressShell.CierraFormas;
var
   i : Byte;
begin
     SetFormaActiva( nil );
     QuitaBotonActivo;
     for i := Screen.FormCount-1 downto 0 do
     begin
          if ( Screen.Forms[i] <> Self ) then
          begin
               if ( Screen.Forms[i].Visible ) then
                  try
                     Screen.Forms[i].Hide;
                     // Ocultamiento de Formas Modales
                     if ( Screen.Forms[i].Tag > 0 ) and ( Screen.Forms[i].Parent = nil ) and
                        ( fsModal in Screen.Forms[i].FormState ) then                  // Si se Reubicaron para Ocultarse
                     begin
                          Screen.Forms[i].Left := Screen.Forms[i].Tag;  // Esto hace que Regrese la forma a su posici�n
                          Screen.Forms[i].Tag := 0;
                     end;
                     Screen.Forms[i].Close;
                     Application.ProcessMessages;
                     except
                  end;
          end
          else
          begin
               ActiveControl := nil;
               //GridEmp.DataSource := nil;   ... No se requiere ya que se desconecta cdsEmpleados
               //Visible := FALSE;
               {*** US# 11757: Proteccion funcionalidad HTTP por licencia sentinel - Solo hacer visible si la empresa esta abierta***}
               if Self.Visible = TRUE then
                  Visible := FShellVisible
               else
                   Visible := False;
               // Ocultamiento del Shell
               if ( self.Tag > 0 ) then
               begin
                    self.Left := self.Tag;
                    self.Tag := 0;
               end;
          end;
     end;
end;

{procedure TTressShell.SetBoton( Boton: TSpeedButton );
begin
     with Boton do
     begin
          if not Down then
             Down := True;
          FBotonActivo := Boton;
     end;
     GridEmp.Repaint;
end;}

{procedure TTressShell.RefrescaBotonActivo( Boton: TSpeedButton );
begin
     if FBotonActivo <> nil then
        FBotonActivo.Down := true
     else
        Boton.Down := false;
     GridEmp.Repaint;
end;}

procedure TTressShell.QuitaBotonActivo;
begin
{
     BotonAjustar.Down := false;
     BotonPermiso.Down := false;
     BotonKardex.Down :=  false;
     BotonPrenomina.Down := false;
     BotonReportes.Down := false;
     BotonBitacora.Down := false;
     BotonLabor.Down := false;
     BotonCedulas.Down := false;
}
     FBotonActivo := nil;
end;

function TTressShell.VerificaDerechoConsulta( const iDerechosForma: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( iDerechosForma, K_DERECHO_CONSULTA );
     if not Result then
        zError( '� Error Al Crear Forma !', '� No Tiene Derechos de Consulta Para Esta Ventana !', 0 );
end;

function TTressShell.CheckDerechosSuper: Boolean;
begin
     Result := RevisaCualquiera( 163 );
end;


function TTressShell.GetForma( InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean ): TBaseConsulta;
begin
     lNueva := FALSE;
     Result := TBaseConsulta( FindComponent( InstanceName ) );
     if not Assigned( Result ) then
     begin
          try
             Result := InstanceClass.Create( Self ) as TBaseConsulta;
             with Result do
             begin
                  Name := InstanceName;
                  //Align := alClient;           Lo hace el Create de BaseConsulta
                  //BorderStyle := bsNone;
                  Parent := Self.PanelPrinc;
             end;
             lNueva := TRUE;
          except
             Result := nil;
          end;
     end;
end;

procedure TTressShell.ActivaFormaPanel( Titulo: String; InstanceClass: TBaseConsultaClass; const InstanceName: String; const iDerechosForma: Integer; const lValidaDerechos: Boolean );
var
   Forma: TBaseConsulta;
   lNueva : Boolean;
begin
     if VerificaDerechoConsulta( iDerechosForma ) then
     begin
          Forma := GetForma( InstanceClass, InstanceName, lNueva );
          if Assigned( Forma ) then
          begin
               with Forma do
               begin
                    IndexDerechos := iDerechosForma;
                    try
                       if lNueva then
                          DoConnect
                       else
                          Reconnect;
                       Show;
                       SetFormaActiva( Forma, lValidaDerechos );
                       lblTitulo.Caption := Titulo;
                        tressshell.GridEmp.BeginUpdate();
                         tressshell.GridEmpDBTableView1.DataController.UpdateItems(False);
                        tressshell.GridEmp.EndUpdate;
                    except
                         Free;
                    end;
               end;
          end
          else
               zError( '� Error Al Crear Forma !', '� Ventana No Pudo Ser Creada !', 0 );
     end;
end;

procedure TTressShell.SetFormaActiva( Forma: TBaseConsulta; const lValidaDerechos: Boolean );
begin
     if ( FFormaActiva <> Forma ) then
     begin
          if ( Assigned( FFormaActiva ) ) then
             FFormaActiva.DoDisconnect;  { Cerrar Datos de Forma Anterior }
          FFormaActiva := Forma;
          if ( Assigned( FFormaActiva ) ) then
          begin
               with FFormaActiva do
               begin
                    _E_Agregar.Enabled := NOT lValidaDerechos or ( lValidaDerechos AND CheckDerechos( K_DERECHO_ALTA ) );
                    _E_Borrar.Enabled := NOT lValidaDerechos or ( lValidaDerechos AND CheckDerechos( K_DERECHO_BAJA ) );
                    _E_Modificar.Enabled := NOT lValidaDerechos or ( lValidaDerechos AND CheckDerechos( K_DERECHO_CAMBIO ) );
                    _A_Imprimir.Enabled := NOT lValidaDerechos or ( lValidaDerechos AND CheckDerechos( K_DERECHO_IMPRESION ) );
                    _A_Exportar.Enabled := NOT lValidaDerechos or ( lValidaDerechos AND CheckDerechos( K_DERECHO_IMPRESION ) );
                    HelpContext:= FFormaActiva.HelpContext;
                    SetFocus;
               end;
          end
          else
          begin
               _E_Agregar.Enabled := FALSE;
               _E_Borrar.Enabled := FALSE;
               _E_Modificar.Enabled := FALSE;
               _A_Imprimir.Enabled := FALSE;
               _A_Exportar.Enabled := FALSE;
          end;
     end;
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TTressShell.SetDataChange(const Entidades: ListaEntidades); 
{$endif}
begin
     dmSuper.NotifyDataChange( Entidades );
     dmNomina.NotifyDataChange( Entidades );
     dmLabor.NotifyDataChange( Entidades );
     ReconectaFormaActiva;
end;

{ ************ Eventos de Componentes *********** }

procedure TTressShell.EscogeImpresoraClose(Sender: TObject);
begin
     inherited;
     LogoffTimer.Enabled := True;
end;

procedure TTressShell.EscogeImpresoraShow(Sender: TObject);
begin
     inherited;
     LogoffTimer.Enabled := False;
end;

function TTressShell.QtyBotonesConsulta(Barra : TToolBar): Integer;
var
   i : Integer;
begin
     Result := 0;
     with Barra  do
     begin
          for i := 0 to ControlCount - 1 do
              if ( Controls[i].ClassType = TSpeedButton ) and ( Controls[i].Visible ) then
                 Inc( Result );
     end;
end;

procedure TTressShell.PanelBotonesTopResize(Sender: TObject);
const
     K_ESPACIO = 4;    // Para dejar dos espacios a cada lado de los botones modales
begin
     inherited;
     //PanelBotonesModal.Width := Round( ( PanelBotonesTop.Width ) / ( QtyBotonesConsulta + 1 ) ) + K_ESPACIO;
     //BotonAsignar.Width := PanelBotonesModal.Width - K_ESPACIO;
     //BotonArea.Width := PanelBotonesModal.Width - K_ESPACIO;
end;

procedure TTressShell.BarraBotonesResize(Sender: TObject);
var
   i, iBotones : Integer;
begin
     inherited;
     iBotones := QtyBotonesConsulta(TToolBar(Sender));
     with TToolBar(Sender) do
     begin
          for i := 0 to ControlCount - 1 do
              if ( Controls[i].ClassType = TSpeedButton ) and ( Controls[i].Visible ) then
                 Controls[i].Width := Trunc( Width / iBotones );
     end;
end;

procedure TTressShell.CB_FiltroFijoChange(Sender: TObject);
begin
     inherited;
     with dmSuper do
          if FiltroFijo <> CB_FiltroFijo.Valor then
          begin
               FiltroFijo := CB_FiltroFijo.Valor;
               RefrescaSuperActivos;
          end;
     ConteoEmpleados; //DevEx (by am): Recaulcula el contreo de empleados para el satatusBar
end;
procedure TTressShell.CargaVista;
begin
     DevEx_SkinController.NativeStyle := False; //Al principio para evitar ver controles sin estilo
     DevEx_ShellRibbon.ColorSchemeName :=  DevEx_SkinController.SkinName; //Especificar estilo para el Ribbon
     DevEx_ShellRibbon.Visible := True;
     DevEx_ShellRibbon.ActiveTab := tabExpediente; //Asigna Tab Default
    
     Self.Menu := nil;
     Application.ProcessMessages;
end;

procedure TTressShell.PaFechaValidDate(Sender: TObject);

var
   {$ifdef MULTIPLES_ENTIDADES}
   Entidades: array of TipoEntidad;
   {$else}
   Entidades: ListaEntidades;
   {$endif}
   lYear : Boolean;
begin
     inherited;
     with dmCliente do
          if FechaSupervisor <> PaFecha.Valor then
          begin
               lYear := ( TheYear( FechaSupervisor ) <> TheYear( PaFecha.Valor ) );
               if lYear then
               begin
                  {$ifdef MULTIPLES_ENTIDADES}
                  SetLength(Entidades, 2); // Se Refrescaran formas de Consulta
                  Entidades[0] := enAusencia;
                  Entidades[1] := enNomina;
                  {$else}
                  Entidades := [ enAusencia, enNomina ];        // Se Refrescaran formas de Consulta
                  {$endif}
               end
               else
               begin
                  {$ifdef MULTIPLES_ENTIDADES}
                  SetLength(Entidades, 1);                      // Se Refrescaran formas de Consulta
                  Entidades[0] := enAusencia;
                  {$else}
                  Entidades := [ enAusencia ];
                  {$endif}
               end;

               FechaSupervisor := PaFecha.Valor;
               RefrescaPaFechalbl;
               RefrescaSuperActivos;
               if lYear then
                  RefrescaPeriodo;                              // Cambi� el A�o para Periodo

               SetDataChange( Entidades );
               //RefrescaEmpleado;
          end;
     ConteoEmpleados;//DevEx (by am): Recalcula el conteo de empleados para el StatusBar
end;
{
procedure TTressShell.GridEmpDrawColumnCell(Sender: TObject; const Rect: TRect;
          DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( Column.FieldName = 'CB_TIPO' ) then
        ImageEmp2.Draw( GridEmp.Canvas, Rect.left, Rect.top + 1, Column.Field.AsInteger - 1 )
     else if ( Column.FieldName = 'CHECADAS' ) and ( Column.Field.AsInteger > 0 ) then
        ImageEmp2.Draw( GridEmp.Canvas, Rect.left, Rect.top + 1, 2 );
     if FormaActiva <> nil then
     begin
          if FormaActiva.Name = 'PlantillasProduccion' then
          begin
               if ( Column.FieldName = 'CB_CODIGO' ) and ( EmpleadoYaOcupado(Column.Field.AsInteger) ) then
               begin
                    GridEmp.Canvas.Font.Style := [fsBold];
                    GridEmp.Canvas.Font.Color := clRed;

                    GridEmp.Canvas.FillRect(Rect);
                    GridEmp.Canvas.TextOut(Rect.Right - Length(Column.Field.AsString)*8 ,Rect.Top+2,Column.Field.AsString);
               end
               else
               begin
                   GridEmp.Canvas.Font.Color := clBlack;
                   GridEmp.Canvas.Font.Style := [];
               end;
                   //GridEmp.Canvas.TextOut(Rect.Right - Length(Column.Field.AsString)*2 ,Rect.Top,Column.Field.AsString);
          end;
     end;
end;
}
procedure TTressShell.PaEmpLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     lOk := dmSuper.PosicionaEmpleado( PaEmp.ValorEntero );
end;

{ ************ Eventos del ActionList *********** }

procedure TTressShell._SinImplementar(Sender: TObject);
begin
     inherited;
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._A_AutorizacionColectivaExecute(Sender: TObject);
begin
     inherited;
     //ZWizardBasico.ShowWizard( TWizAsistAjusteColectivo );
     ZCXWizardBasico.ShowWizard(TWizAsistAjusteColectivo_DevEx);
     Application.ProcessMessages;
end;

procedure TTressShell._A_CambioMasivoTurnosExecute(Sender: TObject);
begin
     inherited;
     ZCXWizardBasico.ShowWizard( TWizEmpCambioTurno );
     Application.ProcessMessages;
end;

procedure TTressShell._A_AjustarAsistenciaExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_AjustarAsistencia.hint, TAsistTarjeta, 'AsistTarjeta', D_SUPER_AJUSTAR_ASISTENCIA );
end;

procedure TTressShell._A_PermisosExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Permisos.hint, TPermisos, 'Permisos', D_SUPER_PERMISOS );
end;

procedure TTressShell._A_KardexExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( 'Kardex de Empleados', TKardex, 'Kardex', D_SUPER_KARDEX );
end;

procedure TTressShell._A_VacacionesExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( 'Vacaciones de Empleados', THisVacacion_DevEx, 'Vacaciones', D_SUPER_VACACIONES );
end;

procedure TTressShell._A_ClasificacionesTemporalesExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_ClasificacionesTemporales.hint, TClasifTemporal_DevEx, 'ClasifTemporal', D_SUPER_CLASIFI_TEMP );
end;

procedure TTressShell._A_PrenominaExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Prenomina.hint, TPrenomina, 'Prenomina', D_SUPER_CONSULTAR_PRENOMINA );
end;

procedure TTressShell._A_ExploradorReportesExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( 'Explorador de Reportes', TReportes_DevEx, 'Reportes', D_REPORTES_SUPERVISORES, FALSE );
end;

procedure TTressShell._A_BitacoraExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Bitacora.hint, TSistBitacora_DevEx, 'SistBitacora', D_SUPER_BITACORA );
end;

procedure TTressShell._A_AsignarEmpleadosExecute(Sender: TObject);
begin
     with dmSuper do
     begin
          if ( NivelSupervisor < 1 ) or ( NivelSupervisor > K_GLOBAL_NIVEL_MAX ) then
             ZError( '� Error !', 'Nivel Indefinido de Supervisores para esta Compa��a', 0 )
          else if VerificaDerechoConsulta( D_SUPER_ASIGNAR_EMPLEADOS ) and dmSuper.VerificaFiltroEmpleados then
             ZBaseDlgModal_DevEx.ShowDlgModal( AsignaEmpleados, TAsignaEmpleados );
     end;
     //DevEx (by am): Se actualiza el conteo de empleados en el statusBar
     ConteoEmpleados;
end;


procedure TTressShell._A_DatosEmpleadoExecute(Sender: TObject);
begin
     inherited;
     if VerificaDerechoConsulta( D_SUPER_DATOS_EMPLEADOS ) then
     begin
           if not (dmSuper.cdsEmpleados.IsEmpty) then
           begin
                if ( ConsultaEmpleados = nil ) then
                   ConsultaEmpleados := TConsultaEmpleados.Create( Application );
                ConsultaEmpleados.ShowModal;
           end
           else
           begin
                ZError( '� Error !', 'No tiene empleados asignados para esta fecha', 0 )
           end;
     end;
end;

procedure TTressShell._A_GruposExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Grupos.hint, TGridSesiones, 'GridSesiones', D_SUPER_GRUPOS );
end;

procedure TTressShell._A_Cursos_TomadosExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Cursos_Tomados.hint, THisCursos_DevEx, 'HisCursos', D_SUPER_CURSOS_TOMADOS );
     //ActivaFormaPanel( BtnCursosTomados, THisCursos, 'HisCursos', D_SUPER_GRUPOS );
end;


procedure TTressShell._A_Cursos_ProgExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Cursos_Prog.hint, THisCursosProg_DevEx, 'HisCursosProg', D_SUPER_CURSOS_PROGRAMADOS );
end;

procedure TTressShell._A_ServidorExecute(Sender: TObject);
begin
     inherited;
     CambiaServidor; //DevEx (by am): Se cambia el metodo para que invoque a la nueva pantalla de Cambio de servidor.
     GridEmpDBTableView1.ApplyBestFit();
end;

procedure TTressShell._A_CacheExecute(Sender: TObject);
begin
     inherited;
     //ZBaseDlgModal.ShowDlgModal( ActivarCache, TActivarCache );
end;

procedure TTressShell._A_ConfigurarImpresoraExecute(Sender: TObject);
begin
     inherited;
     EscogeImpresora.Execute;
end;

procedure TTressShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     inherited;
     AbreEmpresa( False );  //Inicializa
     GridEmpDBTableView1.ApplyBestFit();
end;

procedure TTressShell._A_OtroUsuarioExecute(Sender: TObject);
begin
     inherited;
     LogOff;
end;

procedure TTressShell._A_ClaveAccesoExecute(Sender: TObject);
begin
     inherited;
     //DevEx (by am): Para que invoque a la nueva forma simepre
     CambiaClaveUsuario;
end;

procedure TTressShell._A_SalirSistemaExecute(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TTressShell._E_AgregarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          if dmSuper.cdsEmpleados.IsEmpty then
             zInformation( Caption, 'No tiene Empleados Asignados para esta fecha', 0 )
          else
             FFormaActiva.DoInsert;
     end;
end;

procedure TTressShell._E_BorrarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoDelete;
     end;
end;

procedure TTressShell._E_ModificarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoEdit;
     end;
end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
     inherited;
     RefrescaSuperActivos;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoRefresh;
     end;
     ConteoEmpleados;//DevEx (by am): Refresca el conteo de empleados;
end;

procedure TTressShell._A_ImprimirExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoPrint;
     end;
end;



procedure TTressShell._E_BuscarEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     {if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        PaEmp.AsignaValorEntero( StrToIntDef( sKey, 0 ) );}
      if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
              PaEmp.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell._E_CambiarEmpleadoExecute(Sender: TObject);
begin
     inherited;
     ActiveControl := PaEmp;
end;

{procedure TTressShell._E_ContarEmpleadosExecute(Sender: TObject);
begin
     inherited;
     ZInformation('','Cantidad de Empleados: ' + IntToStr( dmSuper.cdsEmpleados.RecordCount ),0);
end;}

procedure TTressShell._E_FiltroExecute(Sender: TObject);
begin
     inherited;
     ActiveControl := CB_FiltroFijo;
end;

procedure TTressShell._E_CondicionExecute(Sender: TObject);
begin
     inherited;
     //PENDIENTE DevEx (by am):  Se agrega ZBaseDlgModal_DevEx mientras se hace el cambio
     ZBaseDlgModal_DevEx.ShowDlgModal( Condiciones, TCondiciones );
     ConteoEmpleados; //DevEx (by am): Recaulcula el contreo de empleados para el satatusBar
end;

procedure TTressShell._L_LaborExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _L_Labor.hint, TWorksSuper, 'WorksSuper', D_SUPER_LABOR );
end;

procedure TTressShell._L_CedulasExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _L_Cedulas.hint, TCedulas, 'Cedulas', D_SUPER_CEDULAS );
end;

procedure TTressShell._L_LaborOperacionesExecute(Sender: TObject);
begin
     inherited;
     dmLabor.ShowGridLabor( tcOperacion );
end;

procedure TTressShell._L_EspecialesExecute(Sender: TObject);
begin
     inherited;
     dmLabor.ShowGridLabor( tcEspeciales );
end;

procedure TTressShell._L_TiempoMuertoExecute(Sender: TObject);
begin
     inherited;
     dmLabor.ShowGridLabor( tcTMuerto );
end;

procedure TTressShell._L_LaborIndividualExecute(Sender: TObject);
begin
     inherited;
     dmLabor.RegistrarLabor;
end;

procedure TTressShell._L_TMuertoIndividualExecute(Sender: TObject);
begin
     inherited;
     dmLabor.RegistrarTMuerto;
end;

procedure TTressShell._L_AsignarAreaExecute(Sender: TObject);
begin
     inherited;
     if VerificaDerechoConsulta( D_SUPER_ASIGNAR_AREAS ) then
        dmLabor.ShowAsignarAreas;
end;

procedure TTressShell._L_RegistrodeBreaksExecute(Sender: TObject);
begin
     inherited;
     dmLabor.RegistraBreaks;
end;

procedure TTressShell._L_CancelacionBreaksExecute(Sender: TObject);
begin
     inherited;
     ZCXWizardBasico.ShowWizard( TWizLabCancelarBreaks );
     Application.ProcessMessages;
end;

procedure TTressShell._H_ContenidoExecute(Sender: TObject);
begin
     inherited;
     Application.HelpCommand( HELP_FINDER, 0 );
end;

procedure TTressShell._H_GlosarioExecute(Sender: TObject);
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          HelpFile := 'Superwin.chm';
          HelpContext ( AyudaGlosario.HelpContext );
          HelpFile := sHelpFile;
     end;
end;

procedure TTressShell._H_UsoTecladoExecute(Sender: TObject);
begin
     inherited;
     Application.HelpContext( AyudaCombinacionesTeclas.HelpContext );
end;

procedure TTressShell._H_ComoUsarAyudaExecute(Sender: TObject);
begin
     inherited;
     Application.HelpContext( AyudaUsandoAyuda.HelpContext );
end;

procedure TTressShell._H_AcercaDeExecute(Sender: TObject);
begin
     inherited;
     try
        ZAcercaDe_DevEx := TZAcercaDe_DevEx.Create( Self );
        ZAcercaDe_DevEx.ShowModal;
     finally
            ZAcercaDe_DevEx.Free;
     end;
end;

procedure TTressShell._A_AutorizacionesPorAprobarExecute(Sender: TObject);
begin
     inherited;
     dmSuper.EditarGridAProbarAutorizaciones;
end;


procedure TTressShell._L_KardexDiarioExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _L_KardexDiario.hint, TKardexDiario, 'KardexDiario', D_SUPER_ASIGNAR_AREAS );
end;

procedure TTressShell._A_ExportarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoExportar;
     end;
end;

procedure TTressShell._A_Autorizar_PrenominaExecute(Sender: TObject);
begin
     inherited;
     dmSuper.EditarGridAutorizarPrenomina;
end;

procedure TTressShell._A_Plan_VacacionesExecute(Sender: TObject);
begin
     inherited;
     if ZAccesosMgr.CheckDerecho( D_SUPER_PLANVACACION, K_DERECHO_CONSULTA ) then
     begin
          PlanVacacion_DevEx:= TPlanVacacion_DevEx.Create(Application);
          try
             with PlanVacacion_DevEx do
             begin
                  IndexDerechos := D_SUPER_PLANVACACION;
                  ShowModal;
             end;
          finally
                 FreeAndNil( PlanVacacion_DevEx );
          end;
     end
     else
         ZError( self.Caption, 'No tiene derecho de consultar plan de vacaciones', 0 )
end;






procedure TTressShell._A_LayoutExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Layout.hint, TLayoutProduccion, 'PlantillasProduccion', D_LAY_PRODUCCION );
end;



procedure TTressShell._A_TransferenciasExecute(Sender: TObject);
var
   sMensaje : string;
begin
     inherited;
     if (dmTablas.HayDataSetTransferencia( sMensaje )) then
         ActivaFormaPanel( _A_Transferencias.hint, TSuperCosteoTransferencias, 'SuperCosteoTransferencias', D_SUPER_TRANSFERENCIAS )
     else
         ZInformation( 'Transferencias', sMensaje, 0 );

end;

procedure TTressShell._A_Aprobar_Cancelar_TransferenciasExecute(Sender: TObject);
var
   sMensaje : string;
begin
     inherited;
     if (dmTablas.HayDataSetTransferencia( sMensaje )) then
         dmSuper.EditarGridAprobarCancelarTransferencias
     else
         ZInformation( 'Aprobar Cancelar Transferencias', sMensaje, 0 );
end;

procedure TTressShell._A_PlanCapacitacionExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_PlanCapacitacion.hint, THisPlanCapacita_DevEx, 'PlanCapacitacion', D_SUP_PLAN_CAPACITA );
end;

procedure TTressShell._A_CompetenciasEvaluadasExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_CompetenciasEvaluadas.hint, THisEvaluaCompeten_DevEx, 'EvaluacionCompetencias', D_SUP_EVAL_COMPETEN );
end;

procedure TTressShell._A_Matriz_CursosExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Matriz_Cursos.hint, TMatrizCursos_DevEx, 'MatrizCursos', D_SUP_MAT_CURSOS );
end;

procedure TTressShell._A_Matriz_HabilidadesExecute(Sender: TObject);
begin
     inherited;
     ActivaFormaPanel( _A_Matriz_Habilidades.hint, TMatrizHabilidades, 'MatrizHabilidades', D_SUP_MAT_HBLDS );
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
     CargaVista;
     CargaTraducciones;
     DevEx_ShellRibbon.ShowTabHeaders := True;
     {$ifdef COMMSCOPE}
      TabExpediente_EvaluacionDiaria.visible  := ivAlways;
      dxBarManager1Bar1.visible := TRUE;
     {$else}
      TabExpediente_EvaluacionDiaria.visible  := ivNever;
      dxBarManager1Bar1.visible := FALSE;
     {$endif}

end;

procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)
end;

procedure TTressShell._H_NuevoTressExecute(Sender: TObject);
const
     K_HELP_CAMBIOS = 'Version.chm';
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          try
             HelpFile := K_HELP_CAMBIOS;
             HelpCommand( HELP_FINDER, 0 );
          finally
             HelpFile := sHelpFile;
          end;
     end;
end;

procedure TTressShell.Imagen_CB_TIPOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  var
     Indice:integer;
      APainter: TcxPainterAccess;
begin
  inherited;
   if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
    Indice := strToInt( AViewInfo.GridRecord.DisplayTexts [CB_Tipo.Index]);


   APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + EmpleadosImageList.Width + 1;
           DrawContent;
           DrawBorders;
           with AViewInfo.ClientBounds do
           if(indice = 2) then EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, Indice-1);

        finally
               Free;
        end;
  end;
  ADone := True;
end;

procedure TTressShell.Imagen_ChecadasCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  APainter: TcxPainterAccess;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;

    APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
    with APainter do
    begin
      try
      with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
            Left := Left + EmpleadosImageList.Width + 1;
      DrawContent;
      DrawBorders;
    with AViewInfo.ClientBounds do
    begin
    case  StrToInt(vartostr(AViewInfo.GridRecord.Values [SortOrder.Index])) of
        0 : EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_ASISTECIA_INDICE );
        1 : EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_RETARDOS_INDICE );
        2 : EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_VACACION_INDICE );
        3 : EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_INCAPACITA_INDICE );
        4 : EmpleadosImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_FALTA_INDICE );
      end;
      end;
    finally
      Free;
    end;
end;
  ADone := True;
end;

procedure TTressShell.GridEmpDBTableView1CellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  _A_DatosEmpleado.execute();
end;

procedure TTressShell.CB_CODIGOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
    if FormaActiva <> nil then
     begin
          if FormaActiva.Name = 'PlantillasProduccion' then
          begin
               if ( EmpleadoYaOcupado(strtoint(vartostr(AViewInfo.GridRecord.Values [CB_CODIGO.Index])))) then
               begin
                    ACanvas.Font.Style := [fsBold];
                    ACanvas.Font.Color := clRed;

                  //  ACanvas.FillRect(Rect);
                  //  ACanvas.TextOut(Rect.Right - Length(Column.Field.AsString)*8 ,Rect.Top+2,Column.Field.AsString);
               end
               else
               begin
                   ACanvas.Font.Color := clWindowText;
                   ACanvas.Font.Style := [];
               end;
                   //GridEmp.Canvas.TextOut(Rect.Right - Length(Column.Field.AsString)*2 ,Rect.Top,Column.Field.AsString);
          end;

end;
end;

procedure TTressShell.Imagen_ChecadasHeaderClick(Sender: TObject);
begin
  inherited;

  if(GridEmpDBTableView1.Columns[8].SortOrder =soAscending  )then
  begin
       GridEmpDbTableview1.DataController.ClearSorting(False);
       GridEmpDBTableView1.Columns[8].SortOrder :=soDescending;
  end
  else
  begin
       GridEmpDbTableview1.DataController.ClearSorting(False);
       GridEmpDBTableView1.Columns[8].SortOrder := soAscending;
  end;
end;

procedure TTressShell.AsistenciaFechavalid(fecha : tdate);
begin
     //metodo necesario para fclasifitemp... el metodo no es necesario en supervisores, pero fclasifitemp requiere de este metodo en TRESS
end;

 //(@am): Se remueve para vs 2015. Por que se requirio mostrar la bienvenida unicamente en TRESS.
 {
function TTressShell.CheckVersionDatos:Boolean;
begin
     //DevEx (@am): Se guarda el resultado de CheckVersion para utlizarlo despues en la validacion para mostrar la presentacion de cambios en version.
     with dmCliente do
          EsVersionActualBD := CheckVersion;
     Result := TRUE; //Conecta a la base de datos sin importar la version. Esta solo puede ser actualizada desde TRESS.
end; }

procedure TTressShell._A_EvaluacionDiariaExecute(Sender: TObject);
begin
     inherited;
{$ifdef COMMSCOPE}
          dmSuper.EditarGriEvaluacionDiaria;
{$endif}
     //{$ifdef COMMSCOPE}
     //dmSuper.EditarGriEvaluacionDiaria;
     //{$endif}
end;

end.
