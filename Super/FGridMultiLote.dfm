inherited GridMultiLote: TGridMultiLote
  Left = 736
  Top = 151
  Caption = 'Ordenes del MultiLote'
  ClientHeight = 267
  ClientWidth = 467
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 231
    Width = 467
    inherited OK_DevEx: TcxButton
      Left = 300
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 380
      Top = 5
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 467
    inherited ValorActivo2: TPanel
      Width = 141
      inherited textoValorActivo2: TLabel
        Width = 135
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 467
    Height = 181
    Columns = <
      item
        Expanded = False
        FieldName = 'WO_NUMBER'
        Title.Caption = 'Orden'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_DESCRIP'
        ReadOnly = True
        Title.Caption = 'Descripci'#243'n'
        Width = 215
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AR_CODIGO'
        ReadOnly = True
        Title.Caption = 'Parte'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CW_PIEZAS'
        Title.Caption = 'Cantidad'
        Width = 48
        Visible = True
      end>
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
