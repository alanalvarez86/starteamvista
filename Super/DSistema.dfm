object dmSistema: TdmSistema
  OldCreateOrder = False
  Left = 378
  Top = 261
  Height = 375
  Width = 544
  object cdsUsuarios: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsUsuariosAlAdquirirDatos
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    OnGetRights = cdsUsuariosLookUpGetRights
    Left = 24
    Top = 8
  end
  object cdsImpresoras: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsImpresorasAlAdquirirDatos
    UsaCache = True
    Left = 104
    Top = 8
  end
  object cdsGrupos: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsGruposAlAdquirirDatos
    UsaCache = True
    LookupName = 'Grupos de Usuarios'
    LookupDescriptionField = 'GR_DESCRIP'
    LookupKeyField = 'GR_CODIGO'
    OnGetRights = cdsUsuariosLookUpGetRights
    Left = 176
    Top = 8
  end
  object cdsUsuariosLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    OnGetRights = cdsUsuariosLookUpGetRights
    Left = 264
    Top = 8
  end
  object cdsSuscrip: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 336
    Top = 8
  end
end
