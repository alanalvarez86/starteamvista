unit FSuperEditCosteoTransferencias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, StdCtrls, ZetaKeyCombo, ZetaFecha, Mask,
  ZetaNumero, ZetaDBTextBox, DB, ExtCtrls, DBCtrls,
  ZetaSmartLists, Buttons, ZetaEdit, ActnList, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx, System.Actions;

type
  TSuperEditCosteoTransferencias = class(TBaseEdicion_DevEx)
    TipoAutLbl: TLabel;
    HorasLbl: TLabel;
    MotivoLbl: TLabel;
    Label3: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    lbDestino: TLabel;
    CC_ORIGEN: TZetaDBTextBox;
    lbOrigen: TLabel;
    CC_DESCRIP: TZetaDBTextBox;
    TR_MOTIVO: TZetaDBKeyLookup_DevEx;
    TR_HORAS: TZetaDBNumero;
    CC_CODIGO: TZetaDBKeyLookup_DevEx;
    TR_TIPO: TZetaDBKeyCombo;
    TR_FECHA: TZetaDBTextBox;
    TR_TEXTO: TZetaDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    TR_APRUEBA: TZetaDBKeyLookup_DevEx;
    ActionList1: TActionList;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    TR_FEC_APR: TZetaDBTextBox;
    Label6: TLabel;
    TR_STATUS: TZetaDBKeyCombo;
    TR_TXT_APR: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure CC_CODIGOValidKey(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect;override;
    procedure Agregar;override;
    procedure Modificar;override;
    procedure SetControls;
  public
    { Public declarations }
  end;

var
  SuperEditCosteoTransferencias: TSuperEditCosteoTransferencias;

implementation

uses ZAccesosTress,
     ZGlobalTress,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     DGlobal,
     DCliente,
     DTablas,
     DSuper,
     DSistema,
     DBaseCliente;

{$R *.dfm}

procedure TSuperEditCosteoTransferencias.FormCreate(Sender: TObject);
var
   sNivel : string;
begin
     inherited;
     TipoValorActivo2 := stFecha;

     IndexDerechos := D_SUPER_TRANSFERENCIAS;
     FirstControl := CB_CODIGO;
     HelpContext := H_Transferencias_Supervisores;

     CC_CODIGO.LookupDataset := dmTablas.GetDataSetTransferencia;
     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;
     TR_APRUEBA.LookupDataset := dmSistema.cdsUsuariosLookUp;

     with Global do
     begin
          sNivel := Global.NombreCosteo;
          lbOrigen.Caption :=  sNivel + ' Origen:';
          lbDestino.Caption := sNivel + ' Destino:';
     end;
end;

procedure TSuperEditCosteoTransferencias.Connect;
begin
      dmTablas.GetDataSetTransferencia.Conectar;
      dmTablas.cdsMotivoTransfer.Conectar;
      dmSistema.cdsUsuariosLookUp.Conectar;
      dmSuper.cdsSuperCosteo.Conectar;
      Datasource.Dataset :=  dmSuper.cdsCosteoTransferencias;
end;

procedure TSuperEditCosteoTransferencias.DataSourceDataChange(Sender: TObject; Field: TField);
var
   iUsuarioDefault: Integer;
   sFiltro, sMensaje: String;
begin
     inherited;
     if ( ( Field = nil ) or ( Field.FieldName = 'CC_CODIGO' ) ) then
     begin
          dmSuper.FiltroUsuariosXCentroCosto( DataSource.DataSet.FieldByName( 'CC_CODIGO' ).AsString, iUsuarioDefault, sFiltro, sMensaje );
          TR_APRUEBA.Filtro := sFiltro;
     end;
     SetControls;
end;

procedure TSuperEditCosteoTransferencias.CC_CODIGOValidKey(Sender: TObject);
var
   iUsuarioDefault: Integer;
   sFiltro, sMensaje: string;
begin
     inherited;
     if ( Activecontrol.Name <> 'Cancelar' ) then
     begin
          if StrLleno( CC_CODIGO.Llave ) then
          begin
               iUsuarioDefault := StrToIntDef( TR_APRUEBA.Llave, 0 );
               if dmSuper.FiltroUsuariosXCentroCosto( CC_CODIGO.LLave, iUsuarioDefault, sFiltro, sMensaje ) then
               begin
                    TR_APRUEBA.Filtro := sFiltro;
                    if ( StrToIntDef( TR_APRUEBA.Llave, 0 ) <> iUsuarioDefault ) then    // El usuario no est� en el nuevo CC
                       TR_APRUEBA.LLave := IntToStr( iUsuarioDefault );
               end
               else
               begin
                    ZError( Self.Caption, sMensaje, 0 );
                    CC_CODIGO.SetFocus;
               end;
          end
          else
          begin
               ZError( Self.Caption, Format( 'El c�digo de %0:s no puede quedar vac�o', [Global.NombreCosteo] ), 0 );
               CC_CODIGO.SetFocus;
          end;
     end;
end;

procedure TSuperEditCosteoTransferencias.DataSourceStateChange( Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TSuperEditCosteoTransferencias.SetControls;
var
   lEnableAprobacion: Boolean;
   lEnable: Boolean;
begin
     with dmSuper.cdsCosteoTransferencias do
     begin
          lEnableAprobacion := eStatusTransCosteo(FieldByName('TR_STATUS').AsInteger ) = stcPorAprobar;

          if ( State = dsInsert ) then
          begin
               lEnable := TRUE;
               CB_CODIGO.Enabled := TRUE;
          end
          else
          begin
              //Solo el usuario que dio de alta la transferencia puede cambiar datos, mientras no haya sido aprobada.
              lEnable :=  lEnableAprobacion and ( FieldByName('US_CODIGO').AsInteger = dmCliente.Usuario );
              //El empleado no se puede cambiar una vez que se agrego la transferencia.
              CB_CODIGO.Enabled := FALSE;
          end;

          CC_CODIGO.Enabled := lEnable;

          TR_APRUEBA.Enabled := lEnable;
          TR_HORAS.Enabled := lEnable;
          TR_MOTIVO.Enabled := lEnable;
          TR_TEXTO.Enabled := lEnable;
          TR_TIPO.Enabled := lEnable;

          //Solo el usuario que Aprobara la transferencia puede cambiar el status
          //lEnable :=  lEnableAprobacion and ( FieldByName('TR_APRUEBA').AsInteger = dmCliente.Usuario );
          lEnable :=  ( FieldByName('TR_APRUEBA').AsInteger = dmCliente.Usuario );
          TR_STATUS.Enabled := lEnable;
          TR_TXT_APR.Enabled := lEnable;
     end;
end;

procedure TSuperEditCosteoTransferencias.Agregar;
begin
     inherited;
     with CB_CODIGO do
     begin
          if Visible and CanFocus then
             CB_CODIGO.SetFocus;
     end;
end;

procedure TSuperEditCosteoTransferencias.Modificar;
begin
     inherited;
     if NOT CC_CODIGO.Enabled and TR_STATUS.Enabled then
        TR_STATUS.SetFocus
     else
         Cancelar_DevEx.SetFocus;
end;

end.
