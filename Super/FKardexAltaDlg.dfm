inherited KardexAltaDlg: TKardexAltaDlg
  Left = 589
  Top = 149
  Caption = 'Alta De Kardex'
  ClientHeight = 149
  ClientWidth = 225
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 113
    Width = 225
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 58
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 138
      Top = 5
    end
  end
  object TipoAlta: TRadioGroup [1]
    Left = 9
    Top = 4
    Width = 209
    Height = 103
    Caption = ' Agregar '
    ItemIndex = 0
    Items.Strings = (
      'Registro General de Kardex'
      'Cambio de Puesto, Area '#243' Turno')
    TabOrder = 0
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
