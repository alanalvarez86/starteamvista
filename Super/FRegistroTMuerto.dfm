inherited RegistroTMuerto: TRegistroTMuerto
  Left = 858
  Top = 174
  Caption = 'Registro de %s Individual'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Operaciones: TcxPageControl
    inherited Generales: TcxTabSheet
      inherited PanelHora: TPanel
        inherited WK_PRE_CAL: TZetaDBNumero
          Color = clBtnFace
        end
      end
      inherited PanelOrden: TPanel
        inherited MultiLote: TcxButton
          Visible = False
        end
      end
      inherited PanelOpera: TPanel
        Top = 69
      end
      inherited PanelArea: TPanel
        Top = 115
        TabOrder = 5
      end
      object Panel2: TPanel
        Left = 0
        Top = 92
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object LblMotivo: TLabel
          Left = 87
          Top = 4
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Motivo:'
        end
        object WK_TMUERTO: TZetaDBKeyLookup_DevEx
          Left = 125
          Top = 0
          Width = 327
          Height = 21
          LookupDataset = dmLabor.cdsTiempoMuerto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
          DataField = 'WK_TMUERTO'
          DataSource = DataSource
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 444
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 6815924
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
