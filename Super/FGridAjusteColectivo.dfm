inherited GridAjusteColectivo: TGridAjusteColectivo
  Left = 231
  Top = 174
  Width = 802
  Height = 591
  BorderStyle = bsSizeable
  Caption = 'Autorizaciones Colectivas de Asistencia'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 502
    Width = 794
    inherited OK: TBitBtn
      Left = 626
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
    end
    inherited Cancelar: TBitBtn
      Left = 711
      ModalResult = 0
      Kind = bkCustom
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 794
    inherited ValorActivo2: TPanel
      Width = 468
    end
  end
  inherited PanelSuperior: TPanel
    Width = 794
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Hint = 'Excluir Registro (Shift+Del)'
    end
    object LabDel: TLabel [10]
      Left = 398
      Top = 8
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [12]
      Left = 553
      Top = 8
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Filtro:'
    end
    object btnEntrar: TBitBtn
      Left = 730
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Agregar entrada puntual'
      Anchors = [akTop, akRight]
      Caption = 'E'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = TarjetaPuntualClick
      NumGlyphs = 2
    end
    object btnSalir: TBitBtn
      Tag = 1
      Left = 762
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Agregar salida puntual'
      Anchors = [akTop, akRight]
      Caption = 'S'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = TarjetaPuntualClick
      NumGlyphs = 2
    end
    object CB_FiltroFijo: TZetaKeyCombo
      Left = 582
      Top = 3
      Width = 145
      Height = 21
      Hint = 'Cambiar Filtro'
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Text = 'Todos'
      OnChange = CB_FiltroFijoChange
      Items.Strings = (
        'Todos'
        'Horas Extras Autorizadas'
        'Horas Extras NO Autorizadas'
        'Checadas Incompletas'
        'Sin Checadas'
        'Autorizaciones Parciales')
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object Fecha: TZetaFecha
      Left = 435
      Top = 3
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 4
      Text = '11/Jul/07'
      Valor = 39274.000000000000000000
      OnChange = FechaChange
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 794
    Height = 451
    OnColExit = ZetaDBGridColExit
    OnDrawColumnCell = ZetaDBGridDrawColumnCell
    FixedCols = 2
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        ReadOnly = True
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_ELEMENT'
        ReadOnly = True
        Title.Caption = 'Status'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA1'
        Title.Caption = 'Chk. 1'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA1'
        Title.Caption = 'Motivo'
        Width = 37
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA2'
        Title.Caption = 'Chk. 2'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA2'
        Title.Caption = 'Motivo'
        Width = 37
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA3'
        Title.Caption = 'Chk. 3'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA3'
        Title.Caption = 'Motivo'
        Width = 37
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA4'
        Title.Caption = 'Chk. 4'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA4'
        Title.Caption = 'Motivo'
        Width = 37
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HO_CODIGO'
        Title.Caption = 'Horario'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HO_DESCRIP'
        ReadOnly = True
        Title.Caption = 'Descripci'#243'n'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_STATUS'
        Title.Caption = 'Tipo de D'#237'a'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_HORASCK'
        ReadOnly = True
        Title.Caption = 'Ordinarias'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_NUM_EXT'
        ReadOnly = True
        Title.Caption = 'Hrs. Extras'
        Width = 55
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'HRS_EXTRAS'
        Title.Caption = 'Extras Aut.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_HRS_EXTRAS'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRE_FUERA_JOR'
        Title.Caption = 'Prep. F/J'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PRE_FUERA_JOR'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRE_DENTRO_JOR'
        Title.Caption = 'Prep. D/J'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PRE_DENTRO_JOR'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'DESCANSO'
        Title.Caption = 'Desc. Trab.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_DESCANSO'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_CG'
        Title.Caption = 'Perm. C/G'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_CG'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_CG_ENT'
        Title.Caption = 'C/G Ent.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_CG_ENT'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_SG'
        Title.Caption = 'Perm. S/G'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_SG'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_SG_ENT'
        Title.Caption = 'S/G Ent.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_SG_ENT'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_OUT2EAT'
        Title.Caption = 'Comi'#243
        Width = 160
        Visible = True
      end>
  end
  object zCombo: TZetaDBKeyCombo [4]
    Left = 272
    Top = 144
    Width = 83
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    DropDownCount = 3
    ItemHeight = 13
    TabOrder = 4
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfStatusAusencia
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'AU_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object zComboComio: TZetaDBKeyCombo [5]
    Left = 272
    Top = 168
    Width = 100
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    DropDownCount = 3
    ItemHeight = 13
    TabOrder = 5
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfOut2Eat
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'AU_OUT2EAT'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object StatusBar: TStatusBar [6]
    Left = 0
    Top = 538
    Width = 794
    Height = 19
    Panels = <
      item
        Text = 'dgfg'
        Width = 50
      end>
    SimplePanel = True
  end
  inherited DataSource: TDataSource
    OnDataChange = DataSourceDataChange
  end
end
