unit FMaquinaDetalle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, StdCtrls, ZetaEdit, Mask, Buttons, ExtCtrls, DB, Grids,
  DBGrids, ZetaDBGrid, ImageEnView, //TDMULTIP,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxClasses, ieview;

type
  TMaquinaDetalle = class(TForm)
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    MQ_NOMBRE: TDBEdit;
    MQ_CODIGO: TZetaDBEdit;
    DBCodigoLBL: TLabel;
    DBDescripcionLBL: TLabel;
    dsCertificaciones: TDataSource;
    dsMaquina: TDataSource;
    IMAGEN: TImageEnView;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    CI_CODIGO: TcxGridDBColumn;
    CI_DESCRIP: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
  protected
    { Protected declarations }
     AColumn: TcxGridDBColumn;
  public
    { Public declarations }
  end;

var
  MaquinaDetalle: TMaquinaDetalle;

implementation

uses
    DLabor,DSuper,DGlobal,ZGlobalTress,FAsignandoEmpleadosCertif,ZetaDialogo,
    FToolsImageEn, ZetaClientDataSet;

{$R *.dfm}

procedure TMaquinaDetalle.FormCreate(Sender: TObject);
begin
     dsCertificaciones.DataSet := dmLabor.cdsCertificMaq;
     dsMaquina.DataSet := dmLabor.cdsMaquinas;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;

end;

procedure TMaquinaDetalle.FormShow(Sender: TObject);
begin
     Caption := dmLabor.GetMaquinaGlobal;

     {***Configuraicon Grid***}
     //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
     //Esconde la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
     FToolsImageEn.AsignaBlobAImagen( IMAGEN, dsMaquina.DataSet, 'MQ_IMAGEN' );
end;

procedure TMaquinaDetalle.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TMaquinaDetalle.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
     ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(dsCertificaciones.DataSet) );
end;

end.
