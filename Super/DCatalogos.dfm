object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 254
  Top = 190
  Height = 479
  Width = 729
  object cdsPuestos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsPuestosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    LookupActivoField = 'PU_ACTIVO'
    LookupConfidenField = 'PU_NIVEL0'
    OnGetRights = cdsSuperLookupGetRights
    Left = 24
    Top = 16
  end
  object cdsTurnos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsTurnosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Turnos'
    LookupDescriptionField = 'TU_DESCRIP'
    LookupKeyField = 'TU_CODIGO'
    LookupActivoField = 'TU_ACTIVO'
    LookupConfidenField = 'TU_NIVEL0'
    OnGetRights = cdsSuperLookupGetRights
    Left = 88
    Top = 16
  end
  object cdsClasifi: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsClasifiAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Clasificaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    LookupConfidenField = 'TB_NIVEL0'
    OnGetRights = cdsSuperLookupGetRights
    Left = 152
    Top = 16
  end
  object cdsConceptos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsConceptosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 224
    Top = 16
  end
  object cdsCondiciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 304
    Top = 16
  end
  object cdsNomParamLookUp: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsNomParamLookUpAlAdquirirDatos
    LookupName = 'Par'#225'metros de N'#243'mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    LookupActivoField = 'NP_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 160
    Top = 72
  end
  object cdsConceptosLookUp: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsConceptosLookUpAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 272
    Top = 72
  end
  object cdsHorarios: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'HO_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsHorariosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Horarios'
    LookupDescriptionField = 'HO_DESCRIP'
    LookupKeyField = 'HO_CODIGO'
    LookupActivoField = 'HO_ACTIVO'
    LookupConfidenField = 'HO_NIVEL0'
    OnGetRights = cdsSuperLookupGetRights
    Left = 24
    Top = 72
  end
  object cdsNomParam: TZetaLookupDataSet
    Tag = 21
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    AlAdquirirDatos = cdsNomParamAlAdquirirDatos
    UsaCache = True
    LookupName = 'Par'#225'metros de N'#243'mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    LookupActivoField = 'NP_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 88
    Top = 72
  end
  object cdsPeriodo: TZetaLookupDataSet
    Tag = 17
    Aggregates = <>
    IndexFieldNames = 'PE_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsPeriodoAlAdquirirDatos
    LookupName = 'Periodos de N'#243'mina'
    LookupDescriptionField = 'PE_FEC_INI'
    LookupKeyField = 'PE_NUMERO'
    OnLookupDescription = cdsPeriodoLookupDescription
    OnLookupKey = cdsPeriodoLookupKey
    OnLookupSearch = cdsPeriodoLookupSearch
    OnGetRights = cdsSuperLookupGetRights
    Left = 24
    Top = 128
  end
  object cdsMaestros: TZetaLookupDataSet
    Tag = 13
    Aggregates = <>
    IndexFieldNames = 'MA_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsMaestrosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de maestros'
    LookupDescriptionField = 'MA_NOMBRE'
    LookupKeyField = 'MA_CODIGO'
    LookupActivoField = 'MA_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 160
    Top = 128
  end
  object cdsCursos: TZetaLookupDataSet
    Tag = 49
    Aggregates = <>
    IndexFieldNames = 'CU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCursosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Cursos'
    LookupDescriptionField = 'CU_DESCRIP'
    LookupKeyField = 'CU_CODIGO'
    LookupActivoField = 'CU_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 48
    Top = 200
  end
  object cdsTPeriodos: TZetaLookupDataSet
    Tag = 33
    Aggregates = <>
    IndexFieldNames = 'TP_TIPO'
    Params = <>
    AlAdquirirDatos = cdsTPeriodosAlAdquirirDatos
    LookupName = 'Tipos de periodo'
    LookupDescriptionField = 'TP_DESCRIP'
    LookupKeyField = 'TP_TIPO'
    Left = 153
    Top = 194
  end
  object cdsCertificaciones: TZetaLookupDataSet
    Tag = 31
    Aggregates = <>
    IndexFieldNames = 'CI_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCertificacionesAlAdquirirDatos
    UsaCache = True
    LookupName = 'Certificaciones'
    LookupDescriptionField = 'CI_NOMBRE'
    LookupKeyField = 'CI_CODIGO'
    LookupActivoField = 'CI_ACTIVO'
    Left = 248
    Top = 184
  end
  object cdsEstablecimientos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'ES_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsEstablecimientosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Establecimientos'
    LookupDescriptionField = 'ES_ELEMENT'
    LookupKeyField = 'ES_CODIGO'
    LookupActivoField = 'ES_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 40
    Top = 264
  end
  object cdsCompetencias: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsCompetenciasAlAdquirirDatos
    LookupName = 'Competencias'
    LookupDescriptionField = 'CC_ELEMENT'
    LookupKeyField = 'CC_CODIGO'
    LookupActivoField = 'CC_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 416
    Top = 16
  end
  object cdsCompNiveles: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Niveles '
    LookupDescriptionField = 'NC_DESCRIP'
    LookupKeyField = 'NC_NIVEL'
    OnGetRights = cdsSuperLookupGetRights
    Left = 504
    Top = 64
  end
  object cdsCompCursos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 504
    Top = 112
  end
  object cdsCompRevisiones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 504
    Top = 16
  end
  object cdsCatPerfiles: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsCatPerfilesAlAdquirirDatos
    LookupName = 'Grupo de Competencias'
    LookupDescriptionField = 'CP_ELEMENT'
    LookupKeyField = 'CP_CODIGO'
    LookupActivoField = 'CP_ACTIVO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 592
    Top = 24
  end
  object cdsRevPerfiles: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 664
    Top = 32
  end
  object cdsMatPerfilComps: TZetaClientDataSet
    Tag = 69
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsMatPerfilCompsAlAdquirirDatos
    Left = 592
    Top = 112
  end
end
