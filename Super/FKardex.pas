unit FKardex;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,} Grids, DBGrids,ZBaseGridLectura_DevEx,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}
  ZetaDBGrid, Db, ExtCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid;

type
  TKardex = class(TBaseGridLectura_DevEx)
    CB_FECHA: TcxGridDBColumn;
    CB_TIPO: TcxGridDBColumn;
    CB_COMENTA: TcxGridDBColumn;
    CB_MONTO: TcxGridDBColumn;
    CB_PUESTO: TcxGridDBColumn;
    CB_TURNO: TcxGridDBColumn;
    CB_CLASIFI: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure AgregaNivelesAlGrid;
    function TipoKardexValido(var sMensaje: String): Boolean;
    { Private declarations }
  protected
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma; override;
    function ValoresGrid: Variant;override;
  public
    procedure Connect; override;
    procedure Refresh;override;
  end;

var
  Kardex: TKardex;

implementation

uses DSuper, DCliente, DGlobal, DTablas, DSistema, ZGlobalTress, ZAccesosMgr, ZAccesosTress,
     ZetaCommonLists, ZetaTipoEntidad, ZetaCommonClasses, ZetaCommonTools, FEscogeReporte_DevEx;

{$R *.DFM}

procedure TKardex.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 31313;
     AgregaNivelesAlGrid;
end;

procedure TKardex.Connect;
begin
     dmTablas.cdsMovKardex.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmSuper do
     begin
          DataSource.DataSet := cdsHisKardex;
          cdsHisKardex.Conectar;
     end;
end;

procedure TKardex.Refresh;
begin
     dmSuper.cdsHisKardex.Refrescar;
     DoBestFit;
end;

procedure TKardex.Agregar;
begin
     dmSuper.cdsHisKardex.Agregar;
end;

procedure TKardex.Modificar;
begin
     dmSuper.cdsHisKardex.Modificar;
end;

procedure TKardex.Borrar;
begin
     dmSuper.cdsHisKardex.Borrar;
     DoBestFit;
end;

procedure TKardex.ImprimirForma;
var
   sTipo : string;
begin
     with dmSuper.cdsHisKardex do
     begin
          FEscogeReporte_DevEx.ImprimeUnaForma( enKardex, trForma, VarArrayOf(['CB_CODIGO','CB_FECHA','CB_TIPO']),
                                          VarArrayOf([ FieldByName('CB_CODIGO').AsString,
                                          Comillas(FormatDateTime('mm/dd/yyyy',FieldByName('CB_FECHA').AsDateTime)),
                                          Comillas(sTipo)]));
     end;
end;

function TKardex.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje ) and TipoKardexValido( sMensaje );
end;

function TKardex.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje ) and TipoKardexValido( sMensaje );
end;

function EsOtroHistorial( const sTipo: String ): Boolean;
begin
     Result := ( sTipo = K_T_VACA ) or ( sTipo = K_T_PERM ) or ( sTipo = K_T_INCA );
end;

function TKardex.TipoKardexValido( var sMensaje: String ): Boolean;
begin
     if ( dmTablas.EsTipoSistema( Datasource.Dataset.FieldByName( 'CB_TIPO' ).AsString ) ) or
        ( EsOtroHistorial( Datasource.Dataset.FieldByName( 'CB_TIPO' ).AsString ) ) then
     begin
          sMensaje := 'No se Puede Modificar este Tipo de Registros de Kardex';
          if ( Datasource.Dataset.FieldByName( 'CB_TIPO' ).AsString = K_T_PERM ) then
             sMensaje := 'Active la Pantalla "Permisos" para Editar Permisos sin Goce de Sueldo';
          Result := FALSE;
     end
     else
     begin
          if ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_KARDEX, TKARDEX_GRAL ) then
             Result := TRUE
          else
          begin
               sMensaje := 'No Tiene Autorización Para Accesar Registros Generales de Kardex';
               Result := FALSE;
          end;
     end;
end;

procedure TKardex.AgregaNivelesAlGrid;
 var i, indexColumn : integer;
begin
     {with Global do
     begin
          for i := 1 to NumNiveles do
          begin
               with TColumn.Create(GridKardex.Columns) do
               begin
                    FieldName := 'CB_NIVEL' + IntToStr( i );
                    Title.Caption := NombreNivel( i );
               end;
          end;
     end; }

     with Global do
     begin
          for i := 1 to NumNiveles do
          begin
               with ZetaDBGridDBTableView do
               begin
                    CreateColumn;
                    indexColumn := ColumnCount-1;
                    Columns[indexColumn].DataBinding.FieldName :=  'CB_NIVEL' + IntToStr( i );
                    Columns[indexColumn].Caption := NombreNivel( i );
                    {$ifdef AMEX}
                    Columns[indexColumn].MinWidth := 105;
                    {$else}
                    Columns[indexColumn].MinWidth := 100;
                    {$endif}
               end;
          end;
     end;
end;

function TKardex.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

procedure TKardex.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;
end;

end.
