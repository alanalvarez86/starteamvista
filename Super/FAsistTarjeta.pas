unit FAsistTarjeta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseAsisConsulta_DevEx, Grids, DBGrids, ZetaDBGrid, StdCtrls, DBCtrls,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}
  ZetaDBTextBox, ComCtrls, Db, ExtCtrls, Buttons, ZBaseConsulta,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  TressMorado2013, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, Menus, cxButtons, cxPCdxBarPopupMenu, cxPC, System.Actions,
  Vcl.ActnList, Vcl.ImgList, dxBarBuiltInMenu;

type
  TAsistTarjeta = class(TBaseAsisConsulta_DevEx)
    PageControl1: TcxPageControl;
    TabGenerales: TcxTabSheet;
    Label3: TLabel;
    HO_DESCRIP: TZetaDBTextBox;
    LblDia: TLabel;
    AU_STATUS: TZetaDBTextBox;
    Label4: TLabel;
    AU_TIPODIA: TZetaDBTextBox;
    Label11: TLabel;
    AU_TIPO: TZetaDBTextBox;
    IN_ELEMENT: TZetaDBTextBox;
    HO_CODIGO: TZetaDBTextBox;
    LblUsuario: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label1: TLabel;
    AU_HOR_MAN: TDBCheckBox;
    TabHoras: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    AU_HORASCK: TZetaDBTextBox;
    AU_TARDES: TZetaDBTextBox;
    Label9: TLabel;
    Label6: TLabel;
    AU_PER_CG: TZetaDBTextBox;
    AU_PER_SG: TZetaDBTextBox;
    Label17: TLabel;
    AU_AUT_TRA: TZetaDBTextBox;
    Label10: TLabel;
    AU_DES_TRA: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    AU_NUM_EXT: TZetaDBTextBox;
    Label13: TLabel;
    AU_AUT_EXT: TZetaDBTextBox;
    Label14: TLabel;
    AU_EXTRAS: TZetaDBTextBox;
    Label15: TLabel;
    AU_DOBLES: TZetaDBTextBox;
    Label16: TLabel;
    AU_TRIPLES: TZetaDBTextBox;
    TabClasifica: TcxTabSheet;
    CB_TURNO: TZetaDBTextBox;
    TU_DESCRIP: TZetaDBTextBox;
    LblTurno: TLabel;
    Label2: TLabel;
    CB_PUESTO: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    Label7: TLabel;
    CB_CLASIFI: TZetaDBTextBox;
    CLASIFI: TZetaDBTextBox;
    Label19: TLabel;
    Label21: TLabel;
    AU_POSICIO: TZetaDBTextBox;
    AU_PERIODO: TZetaDBTextBox;
    TabNiveles: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL1: TZetaDBTextBox;
    CB_NIVEL2: TZetaDBTextBox;
    CB_NIVEL3: TZetaDBTextBox;
    CB_NIVEL4: TZetaDBTextBox;
    CB_NIVEL5: TZetaDBTextBox;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL9: TZetaDBTextBox;
    CB_NIVEL8: TZetaDBTextBox;
    CB_NIVEL7: TZetaDBTextBox;
    CB_NIVEL6: TZetaDBTextBox;
    dsChecadas: TDataSource;
    lblTomoComida: TLabel;
    AU_OUT2EAT: TZetaDBTextBox;
    AU_STA_FES: TZetaDBTextBox;
    Label20: TLabel;
    Label8: TLabel;
    Label18: TLabel;
    AU_PRE_EXT: TZetaDBTextBox;
    Label22: TLabel; 
    CB_NOMINA: TZetaDBTextBox; 
    NO_DESCRIP: TZetaDBTextBox;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBTextBox;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaDBTextBox;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBTextBox;
//    ZetaDBGrid: TZetaCXGrid;
  //  ZetaDBGridDBTableView: TcxGridDBTableView;
  //  ZetaDBGridLevel: TcxGridLevel;
    CH_H_REAL: TcxGridDBColumn;
    CH_H_AJUS: TcxGridDBColumn;
    CH_TIPO: TcxGridDBColumn;
    CH_DESCRIP: TcxGridDBColumn;
    CH_HOR_ORD: TcxGridDBColumn;
    CH_HOR_EXT: TcxGridDBColumn;
    CH_HOR_DES: TcxGridDBColumn;
    CH_APRUEBA: TcxGridDBColumn;
    US_CODIGO_GRID: TcxGridDBColumn;
    CH_MOTIVO: TcxGridDBColumn;
    CH_SISTEMA: TcxGridDBColumn;
    CH_RELOJ: TcxGridDBColumn;
    Verificar: TcxButton;
    BtnCalendario: TcxButton;
    bbMostrarCalendario: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure VerificarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure BtnCalendarioClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
  private
    { Private declarations }
    function AsistenciaValida(var sMensaje: String): Boolean;
    procedure SetCamposNivel;
    procedure ApplyMinWidth;
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$endif}
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function ValoresGrid: Variant;override;
  public
    { Public declarat\ions }
  end;

var
  AsistTarjeta: TAsistTarjeta;

implementation

uses dSuper, DCliente, dCatalogos, dTablas, dSistema, dGlobal,
     ZetaDialogo, ZetaCommonLists, ZetaClientTools, ZetaCommonClasses, ZetaCommonTools,
     ZGlobalTress, FToolsAsistencia,StrUtils;

{$R *.DFM}

procedure TAsistTarjeta.FormCreate(Sender: TObject);
begin
     inherited;
     //Desactivar gridmode
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= FALSE;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     {$endif}
     HelpContext := 00500;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     SetCamposNivel;
end;

procedure TAsistTarjeta.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
          cdsHorarios.Conectar;
          cdsTurnos.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTPeriodos.Conectar; 
     end;
     dmTablas.cdsIncidencias.Conectar;
     with dmSuper do
     begin
          cdsTarjeta.Conectar;
          DataSource.DataSet:= cdsTarjeta;
          dsChecadas.DataSet := cdsChecadas;
     end;
end;

procedure TAsistTarjeta.Refresh;
begin
     dmSuper.cdsTarjeta.Refrescar;
     DoBestFit;
end;

procedure TAsistTarjeta.Agregar;
begin
     dmSuper.cdsTarjeta.Agregar;
     DoBestFit;
end;

procedure TAsistTarjeta.Modificar;
begin
     dmSuper.cdsTarjeta.Modificar;
     DoBestFit;
end;

procedure TAsistTarjeta.Borrar;
begin
     dmSuper.cdsTarjeta.Borrar;
     DoBestFit;
end;

function TAsistTarjeta.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje ) and AsistenciaValida( sMensaje );
end;

function TAsistTarjeta.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje ) and AsistenciaValida( sMensaje );
end;

function TAsistTarjeta.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje ) and AsistenciaValida( sMensaje );
end;

function TAsistTarjeta.AsistenciaValida(var sMensaje: String): Boolean;
var
   eStatus: eStatusPeriodo;
begin
     with dmSuper do
     begin
          eStatus := eStatusPeriodo( cdsTarjeta.FieldByName( 'PE_STATUS' ).AsInteger );
          if ( eStatus > eStatusPeriodo( AsistenciaStatusLimite ) ) then
          begin
               sMensaje := 'La N�mina A La Que Pertenece Esta Tarjeta Est� ' + ObtieneElemento( lfStatusPeriodo, Ord( eStatus ) );
               Result := FALSE;
          end
          else
               Result := TRUE;
     end;
end;

procedure TAsistTarjeta.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
{$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
{$endif}
end;

function TAsistTarjeta.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

procedure TAsistTarjeta.VerificarClick(Sender: TObject);
var
   eStatus: eStatusEmpleado;
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        eStatus := dmSuper.GetStatus( dmCliente.Empleado, dmCliente.FechaAsistencia );
     finally
            Screen.Cursor := oCursor;
     end;
     ZetaDialogo.ZInformation( 'Verificaci�n de Status',
                                'Empleado ' + ValorActivo1.Caption +
                                CR_LF +
                                CR_LF +
                                'Status Al ' + ZetaCommonTools.FechaLarga( dmCliente.FechaAsistencia ) + ':' +
                                CR_LF +
                                CR_LF +
                                ZetaCommonLists.ObtieneElemento( lfStatusEmpleado, Ord( eStatus ) ), 0 );
end;

procedure TAsistTarjeta.FormShow(Sender: TObject);
const
     COL_APROBADAS = 7;
     COL_MOTIVO = 9;
begin
     inherited;
     DGlobal.SetDescuentoComida( lblTomoComida, AU_OUT2EAT );
     //ZDBGrid.Columns[COL_APROBADAS].Visible:= Global.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES );
     //ZDBGrid.Columns[COL_MOTIVO].Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;{ACL170409}

     ZetaDBGridDBTableView.GetColumnByFieldName('CH_APRUEBA').Visible:= Global.GetGlobalBooleano( K_GLOBAL_APROBAR_AUTORIZACIONES );
     ZetaDBGridDBTableView.GetColumnByFieldName('CH_MOTIVO').Visible:=  FToolsAsistencia.PermitirCapturaMotivoCH;{ACL170409}

     //Para que ponga el MinWidth ideal dependiendo del texto presente en las columnas.
     ApplyMinWidth;
     //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
      //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
      //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
     {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$endif}


end;

procedure TAsistTarjeta.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with Datasource do
     begin
          if ( Dataset <> nil ) then
          begin
               with DataSet do
                    bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
          end;
     end;
end;

procedure TAsistTarjeta.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

procedure TAsistTarjeta.BtnCalendarioClick(Sender: TObject);
begin
     inherited;
     dmSuper.MuestraCalendarioEmpleado;
end;

procedure TAsistTarjeta.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TAsistTarjeta.ZetaDBGridDBTableViewDblClick(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TAsistTarjeta.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;


{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TAsistTarjeta.CambiosVisuales;
var
  I : Integer;
Const
  K_LABELS_NAME : array[0..5] of string = ( 'CB_NIVEL7lbl', 'CB_NIVEL8lbl', 'CB_NIVEL9lbl', 'CB_NIVEL10lbl', 'CB_NIVEL11lbl', 'CB_NIVEL12lbl') ;
  K_DBTEXTBOX_NAME : array[0..5] of string = ( 'CB_NIVEL7', 'CB_NIVEL8', 'CB_NIVEL9', 'CB_NIVEL10', 'CB_NIVEL11', 'CB_NIVEL12') ;
 begin
     for I := 0 to TabNiveles.ControlCount - 1 do
     begin
          if TabNiveles.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               if  not ( AnsiIndexStr( TabNiveles.Controls[I].Name, K_LABELS_NAME ) <> -1 ) then
               begin
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabNiveles.Controls[I].Width+2);
               end;
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaDBTextBox' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    if  not ( AnsiIndexStr( TabNiveles.Controls[I].Name, K_DBTEXTBOX_NAME ) <> -1 ) then
                    begin
                         TabNiveles.Controls[I].Width := K_WIDTHLLAVE;
                         //TZetaDbKeyLookup_DevEx( TabNiveles.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                         TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT;
                    end
                    else
                    begin
                         TabNiveles.Controls[I].Width := K_WIDTHLLAVE;
                    end;

               end;
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaTextBox' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    TabNiveles.Controls[I].Width := K_WIDTH_LOOKUP-200;
                    //TZetaDbKeyLookup_DevEx( TabNiveles.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT + K_WIDTHLLAVE + 2;
               end;
          end;


     end;
end;
{$endif}

end.
