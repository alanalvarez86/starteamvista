unit FGridCursosSuper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicionRenglon_DevEx, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls,
  ZetaFecha, ZetaHora, ZetaNumero, Mask, ZetaMessages,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, {$ifdef ICUMEDICAL_CURSOS}System.IOUtils,{$endif}
  dxBarBuiltInMenu;

type
  TGridCursosSuper = class(TBaseEdicionRenglon_DevEx)
    SE_REVISIO: TDBEdit;
    SE_LUGAR: TDBEdit;
    SE_HORAS: TZetaDBNumero;
    SE_HOR_INI: TZetaDBHora;
    SE_HOR_FIN: TZetaDBHora;
    SE_FEC_INI: TZetaDBFecha;
    SE_FEC_FIN: TZetaDBFecha;
    SE_CUPO: TZetaDBNumero;
    SE_COSTO3Lbl: TLabel;
    SE_COSTO3: TZetaDBNumero;
    SE_COSTO2Lbl: TLabel;
    SE_COSTO2: TZetaDBNumero;
    SE_COSTO1Lbl: TLabel;
    SE_COSTO1: TZetaDBNumero;
    SE_COMENTA: TDBEdit;
    MA_CODIGOLbl: TLabel;
    MA_CODIGO: TZetaDBKeyLookup_DevEx;
    lblRevision: TLabel;
    lblLugar: TLabel;
    lblHorIni: TLabel;
    lblHorFin: TLabel;
    lblCupo: TLabel;
    lblComenta: TLabel;
    Label1: TLabel;
    KC_HORASLbl: TLabel;
    KC_FEC_TOMLbl: TLabel;
    KC_FEC_FINLbl: TLabel;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    SE_EST: TZetaDBKeyLookup_DevEx;
    lblEstablec: TLabel;
    Documento: TcxTabSheet;
    btnAgregarDocumento: TcxButton;
    btnBorraDocumento: TcxButton;
    btnVerDocumento: TcxButton;
    lblDescripcionArchivo: TLabel;
    lblUbicacionArchivo: TLabel;
    lblTipoArchivo: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SE_HORASExit(Sender: TObject);
    //procedure BuscarBtnClick(Sender: TObject);
    procedure dxBarButton_BuscarBtnClick(Sender: TObject);
    procedure SetEditarSoloActivos;
    procedure btnAgregarDocumentoClick(Sender: TObject);
    procedure btnBorraDocumentoClick(Sender: TObject);
    procedure btnVerDocumentoClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure Buscar;
    procedure SeleccionaSiguienteRenglon;
    {$ifdef ICUMEDICAL_CURSOS}
    procedure EnabledControlDocumento;
    procedure AgregaDocumento;
    procedure DialogoAgregaDocumento( const lAgregando: Boolean );
    {$endif}
    function TomaDerechoGrupos: Boolean;
   protected
    procedure Connect; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  GridCursosSuper: TGridCursosSuper;

implementation

uses dCliente, dCatalogos, ZetaCommonClasses, ZetaCommonTools,
     ZetaDialogo, ZetaBuscaEmpleado_DevEx ,ZAccesosTress, DSuper,ZAccesosMgr
     {$ifdef ICUMEDICAL_CURSOS }, ZetaFilesTools, FEditDocumentoICU_DevEx{$endif};

{$R *.dfm}



procedure TGridCursosSuper.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H10169_Captura_de_cursos;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
     MA_CODIGO.LookupDataset := dmCatalogos.cdsMaestros;
     SE_EST.LookupDataset := dmCatalogos.cdsEstablecimientos;
end;

procedure TGridCursosSuper.FormShow(Sender: TObject);
begin
     inherited;
     if ( DataSource.State = dsInsert ) or ( TomaDerechoGrupos ) then
        IndexDerechos:= D_SUPER_GRUPOS
     else
         IndexDerechos:= D_SUPER_SESIONES_OTROS;
     PageControl.ActivePage := Tabla;
     dmCatalogos.Duracion_Curso := 0;
     self.ActiveControl:= CU_CODIGO;
     DevEx_cxDBNavigatorEdicion.Enabled:= FALSE;
     Datasource.AutoEdit := CheckDerechos( K_DERECHO_CAMBIO );
     dxBarButton_BuscarBtn.Enabled := Datasource.AutoEdit;
     GridRenglones.ReadOnly:= not Datasource.AutoEdit;
     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
     {$ifdef ICUMEDICAL_CURSOS}
     SE_REVISIO.Enabled := False;
     Documento.TabVisible := True;
     Documento.Enabled := True;
     if ( DataSource.State = dsInsert ) then
        dmSuper.PathICU := VACIO;
     //EnabledControlDocumento;
     {$else}
     Documento.TabVisible := False;
     Documento.Enabled := False;
     {$endif}
end;

procedure TGridCursosSuper.SE_HORASExit(Sender: TObject);
begin
     inherited;
     dmCatalogos.Duracion_Curso := SE_HORAS.Valor;
end;


{procedure TGridCursosSuper.BuscarBtnClick(Sender: TObject);
begin
     inherited;
     Buscar;
end;}


procedure TGridCursosSuper.HabilitaControles;
begin
     inherited;
     dxBarButton_BuscarBtn.Enabled := CheckDerechos( K_DERECHO_CAMBIO );  //Se desea que tenga habilitada la b�squeda de empleado, no esta ligada a la del dataset
     {$ifdef ICUMEDICAL_CURSOS}
     if Modo = dsBrowse then
          dmSuper.PathICU := dmSuper.cdsSesion.FieldByName( 'SE_D_RUTA' ).AsString
     else if Modo = dsInsert then
          dmSuper.PathICU := VACIO;

     EnabledControlDocumento;
     {$endif}
end;

// Control de Edici�n del Grid
procedure TGridCursosSuper.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( GridEnfocado) then
     begin
          if ( ssCtrl in Shift ) then { CTRL }
             case Key of
                   66:  { Letra F = Buscar }
                   begin
                        Key := 0;
                        DoLookup;
                   end;
              end;
          if ( Key = VK_RETURN ) then
          begin
               Key := 0;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TGridCursosSuper.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   GridRenglones.selectedIndex := 0;
              end;
     end
     else
          inherited KeyPress( Key );
end;

procedure TGridCursosSuper.SeleccionaSiguienteRenglon;
begin
     if ( Datasource.AutoEdit ) then
     begin
          with dmSuper.cdsAprobados do
          begin
               Next;
               if EOF then
                  Append;
          end;
     end;
end;

procedure TGridCursosSuper.WMExaminar(var Message: TMessage);
begin
     if GridEnfocado then
     begin
          SeleccionaSiguienteRenglon;
     end;
end;

procedure TGridCursosSuper.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsMaestros.Conectar;
          cdsEstablecimientos.Conectar; 
     end;
     with dmSuper do
     begin
          if ( cdsSesion.State <> dsInsert )  then
          begin
               ObtieneSesion( TRUE );
          end
          else
          begin
               if ( cdsAprobados.State = dsInactive ) then
                  cdsAprobados.Refrescar;  //Este es solo para que se conecte la primera vez
               cdsAprobados.EmptyDataSet;
          end;
          DataSource.DataSet:= cdsSesion;
          dsRenglon.DataSet:= cdsAprobados;
     end;
     {$ifdef ICUMEDICAL_CURSOS}
     EnabledControlDocumento;
     {$endif}
end;

{$ifdef ICUMEDICAL_CURSOS}
procedure TGridCursosSuper.EnabledControlDocumento;
var
   lEnabled: Boolean;
   sDescripcion, sTipo, sRuta: string;
   oColor: TColor;
begin
     //with dmCatalogos.cdsSesiones do
     //begin
          lEnabled := StrLleno(dmSuper.PathICU);

          if lEnabled then
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: ' + TPath.GetFileName(dmSuper.PathICU); //ExtractFileName( dmSuper.PathICU );
               sTipo := 'Tipo: ' + ZetaFilesTools.GetTipoDocumento( UpperCase( Copy( ExtractFileExt( dmSuper.PathICU ), 2 , 255 ) ) );
               sRuta := 'Ruta: ' + dmSuper.PathICU;
          end
          else
          begin
               with lblDescripcionArchivo do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay ning�n documento almacenado';
                    sTipo := VACIO;
                    sRuta := VACIO;
               end;
          end;
     //end;

     btnVerDocumento.Enabled := lEnabled;
     btnBorraDocumento.Enabled := lEnabled;

     with lblDescripcionArchivo do
     begin
          Font.Color := oColor;
          Caption := sDescripcion;
          Hint := TPath.GetFileName(dmSuper.PathICU);
     end;

     lblTipoArchivo.Caption := sTipo;
     lblUbicacionArchivo.Caption := sRuta;
     lblUbicacionArchivo.Hint := dmSuper.PathICU;
end;


procedure TGridCursosSuper.AgregaDocumento;
var
   sDocumento: string;
begin
     sDocumento := dmSuper.cdsSesion.FieldByName('SE_D_NOM').AsString;
     if StrVacio( sDocumento ) or ZetaDialogo.ZConfirm( Caption, '�Desea sustituir el documento: ' + sDocumento + ' por uno nuevo?', 0, mbNo ) then
     begin
          DialogoAgregaDocumento( TRUE );
          DoEdit;
     end;
end;


procedure TGridCursosSuper.DialogoAgregaDocumento( const lAgregando: Boolean );
begin
     with dmSuper.cdsSesion do
     begin
          if FEditDocumentoICU_DevEx.EditarDocumento( lAgregando, FIeldByName( 'SE_D_NOM' ).AsString, H_DOCUMENTO_CONCEPTOS, dmSuper.CargaDocumentoICU ) then
          begin
               EnabledControlDocumento;
               {if state in [ dsinsert, dsedit ] then
                  Modo:= dsEdit;}
          end;
     end;
end;
{$endif}

function TGridCursosSuper.PuedeBorrar(var sMensaje: String ): Boolean;
begin
      Result:= FALSE;
      sMensaje:= 'No se puede borrar grupos desde esta pantalla';
end;

function TGridCursosSuper.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= CheckDerecho(D_SUPER_GRUPOS, K_DERECHO_ALTA);
     if ( not Result ) then
        sMensaje:= 'No tiene derechos de agregar registros';
end;


procedure TGridCursosSuper.btnAgregarDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     AgregaDocumento;
     {$endif}
end;

procedure TGridCursosSuper.btnBorraDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     with dmSuper do
     begin
          if ( ZetaDialogo.ZConfirm(Caption, '� Desea borrar el documento ' + cdsSesion.FieldByName('SE_D_NOM').AsString + ' ?', 0, mbNo ) ) then
          begin
               dmSuper.BorraDocumentoICU;
               EnabledControlDocumento;
               DoEdit;
          end;
     end;
     {$endif}
end;

procedure TGridCursosSuper.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     dmSuper.AbreDocumentoICU;
     {$endif}
end;

procedure TGridCursosSuper.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmSuper.cdsAprobados do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

procedure TGridCursosSuper.Buscar;
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then  //Solamente podra buscar si esta seleccionado la columna de n�mero de empleado
             BuscaEmpleado;
     end;
end;

function TGridCursosSuper.TomaDerechoGrupos: Boolean;
begin
     // Sino tiene cualquiera de los derechos de "otros supervisores"
     Result:= ( dmSuper.cdsSesion.FieldByName('US_CODIGO').AsInteger = dmCliente.Usuario );
end;



procedure TGridCursosSuper.dxBarButton_BuscarBtnClick(Sender: TObject);
begin
  inherited;
  Buscar;
end;

procedure TGridCursosSuper.SetEditarSoloActivos;
begin
     CU_CODIGO.EditarSoloActivos := TRUE;
     MA_CODIGO.EditarSoloActivos := TRUE;
     SE_EST.EditarSoloActivos := TRUE;
end;

end.
