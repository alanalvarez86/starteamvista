unit FHintLayout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  THintLayout = class(TForm)
    Timer1: TTimer;
    Panel1: TPanel;
    lblEmp: TLabel;
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetEmpleado(Empleado:string;Nombre:String);
  end;

var
  HintLayout: THintLayout;
implementation

{$R *.dfm}

{ THintLayout }

procedure THintLayout.SetEmpleado(Empleado: string; Nombre: String);
begin
     lblEmp.Caption := Empleado+' : '+Nombre;
    
end;



procedure THintLayout.FormShow(Sender: TObject);
begin
     timer1.enabled := TRUE;

     //
    // Application.ModalFinished;
     //Cerrar;
end;

procedure THintLayout.Button1Click(Sender: TObject);
begin
     Close;
end;

procedure THintLayout.Timer1Timer(Sender: TObject);
begin
     Timer1.Enabled := FALSE;
     Close;
end;

end.
