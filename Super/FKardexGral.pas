unit FKardexGral;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, Db, DBCtrls, Mask, ZetaFecha,
  ExtCtrls, StdCtrls, ZetaDBTextBox, ComCtrls, Buttons, ZetaSmartLists,
  FKardexBase_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons,
  ZetaKeyLookup_DevEx, dxSkinsDefaultPainters;

type
  TKardexGeneral = class(TKardexBase_DevEx)
    MontoLbl: TLabel;
    CB_TIPOlbl: TLabel;
    CB_TIPO: TZetaDBKeyLookup_DevEx;
    CB_MONTO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  KardexGeneral: TKardexGeneral;

implementation

uses dSuper, dTablas, dSistema, ZAccesosTress, ZetaCommonLists;

{$R *.DFM}

procedure TKardexGeneral.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= 31310;
     TipoValorActivo1 := stEmpleado;
     //IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     CB_TIPO.LookupDataset := dmTablas.cdsMovKardex;
end;

procedure TKardexGeneral.FormShow(Sender: TObject);
begin
     inherited;
     Datasource.AutoEdit := TRUE;   // No se evaluan derechos una vez abierta la forma
end;

procedure TKardexGeneral.Connect;
begin
     inherited;
     dmTablas.cdsMovKardex.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmSuper do
     begin
          cdsHisKardex.Conectar;
          DataSource.DataSet := cdsHisKardex;
     end;
end;

end.


