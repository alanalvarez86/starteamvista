inherited Prenomina: TPrenomina
  Left = 294
  Top = 250
  Caption = 'Pre-N'#243'mina'
  ClientHeight = 447
  ClientWidth = 1003
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1003
    inherited ValorActivo2: TPanel
      Width = 744
      inherited textoValorActivo2: TLabel
        Width = 738
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 1003
    Height = 53
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object PanelPer: TPanel
      Left = 0
      Top = 0
      Width = 353
      Height = 53
      Align = alLeft
      TabOrder = 0
      object Label37: TLabel
        Left = 25
        Top = 6
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object Label38: TLabel
        Left = 9
        Top = 29
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
      end
      object LabDel: TLabel
        Left = 249
        Top = 6
        Width = 18
        Height = 13
        Caption = 'del'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PerInicio: TZetaDBTextBox
        Left = 271
        Top = 4
        Width = 70
        Height = 17
        Hint = 'Fecha de Inicio del Periodo'
        AutoSize = False
        Caption = 'PerInicio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_FEC_INI'
        DataSource = dsPeriodo
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LabAl: TLabel
        Left = 254
        Top = 28
        Width = 11
        Height = 13
        Caption = 'al'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PerFinal: TZetaDBTextBox
        Left = 271
        Top = 26
        Width = 68
        Height = 17
        Hint = 'Fecha final del Periodo'
        AutoSize = False
        Caption = 'PerFinal'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'PE_FEC_FIN'
        DataSource = dsPeriodo
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object ImgNoPrenomina: TImage
        Left = 1
        Top = 1
        Width = 16
        Height = 51
        Hint = 'No Existe Informaci'#243'n'
        Align = alLeft
        ParentShowHint = False
        Picture.Data = {
          07544269746D617042010000424D420100000000000076000000280000001100
          0000110000000100040000000000CC0000000000000000000000100000001000
          000000000000000080000080000000808000800000008000800080800000C0C0
          C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
          FF00DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DD0000000000
          000DD0000000D999FFFFFFFF9990D0000000D999900000099990D0000000D099
          999FF99999F0D0000000D0F99999999990F0D0000000D0FFF999999FFFF0D000
          0000D0F00099990000F0D0000000D0FFF999999FFFF0D0000000D0F999999999
          90F0D0000000D099999FF99999F0D0000000D999900000099990D0000000D999
          FFFFFFFF9990D0000000D99000000000099DD0000000DDDDDDDDDDDDDDDDD000
          0000DDDDDDDDDDDDDDDDD0000000}
        ShowHint = True
        Transparent = True
        Visible = False
      end
      object PaTipo: TZetaKeyCombo
        Left = 52
        Top = 2
        Width = 190
        Height = 21
        Hint = 'Tipo de Pren'#243'mina'
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        OnChange = PaTipoChange
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
      end
      object PaPer: TZetaNumero
        Left = 52
        Top = 25
        Width = 58
        Height = 21
        Hint = 'N'#250'mero de Periodo'
        Mascara = mnEmpleado
        TabOrder = 1
        OnExit = PaPerExit
        OnKeyUp = PaPerKeyUp
      end
      object btnUp_DevEx: TcxButton
        Left = 136
        Top = 25
        Width = 21
        Height = 21
        Hint = 'Aumentar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = btnUp_DevExClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF988F99FF9A919AFF9A919AFF9A919AFF9A919AFF9A919AFF9A919AFF9A91
          9AFF9A919AFF9A919AFF9A919AFF9A919AFF9A919AFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFB5AEB5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3BE
          C4FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFBEB8
          BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCCC8CCFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D5D9FF8D838EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD5D2
          D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DDE0FF8D838EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8D838EFFDBD7DBFFFFFFFFFFFFFFFFFFFFFFFFFFE6E3
          E6FF908791FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFE4E1
          E4FFFFFFFFFFEDEBEDFF968D97FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF928993FFE2DFE2FF9A919AFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      end
      object btnDown_DevEx: TcxButton
        Left = 112
        Top = 25
        Width = 21
        Height = 21
        Hint = 'Decrementar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = btnDown_DevExClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D838EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF988F99FFEFEDEFFFA199A2FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF968D97FFEDEBEDFFFFFFFFFFF4F3F4FF9D959EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFE4E1E4FFFFFFFFFFFFFF
          FFFFFFFFFFFFEDEBEDFF968D97FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D838EFFE0DD
          E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E7E9FF908791FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFFD9D5D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE4E1E4FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9D5D9FF8D838EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD0CCD0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      end
    end
    object PanelStatus: TPanel
      Left = 353
      Top = 0
      Width = 650
      Height = 53
      Align = alClient
      TabOrder = 1
      object Label39: TLabel
        Left = 3
        Top = 20
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object LBStatus: TLabel
        Left = 40
        Top = 20
        Width = 52
        Height = 13
        Caption = 'LBStatus'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 218
    Width = 1003
    Height = 229
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopupMenu = nil
      DataController.DataSource = dsAusencia
      object au_posicio: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'au_posicio'
      end
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha y D'#237'a'
        DataBinding.FieldName = 'AU_FECHA'
        Options.Sorting = False
      end
      object CHECADA1: TcxGridDBColumn
        Caption = 'Ent. 1'
        DataBinding.FieldName = 'CHECADA1'
        Options.Sorting = False
      end
      object CHECADA2: TcxGridDBColumn
        Caption = 'Sal. 1'
        DataBinding.FieldName = 'CHECADA2'
        Options.Sorting = False
      end
      object CHECADA3: TcxGridDBColumn
        Caption = 'Ent. 2'
        DataBinding.FieldName = 'CHECADA3'
        Options.Sorting = False
      end
      object CHECADA4: TcxGridDBColumn
        Caption = 'Sal. 2'
        DataBinding.FieldName = 'CHECADA4'
        Options.Sorting = False
      end
      object au_horas: TcxGridDBColumn
        Caption = 'Ordinarias'
        DataBinding.FieldName = 'au_horas'
      end
      object AU_NUM_EXT: TcxGridDBColumn
        Caption = 'Hrs. Extras'
        DataBinding.FieldName = 'AU_NUM_EXT'
      end
      object au_extras: TcxGridDBColumn
        Caption = 'Extras Pago'
        DataBinding.FieldName = 'au_extras'
      end
      object au_per_cg: TcxGridDBColumn
        Caption = 'Permiso CG'
        DataBinding.FieldName = 'au_per_cg'
      end
      object au_per_sg: TcxGridDBColumn
        Caption = 'Permiso SG'
        DataBinding.FieldName = 'au_per_sg'
      end
      object au_des_tra: TcxGridDBColumn
        Caption = 'Des. Trab.'
        DataBinding.FieldName = 'au_des_tra'
      end
      object au_tardes: TcxGridDBColumn
        Caption = 'Tardes'
        DataBinding.FieldName = 'au_tardes'
      end
      object AU_STATUS: TcxGridDBColumn
        Caption = 'D'#237'a'
        DataBinding.FieldName = 'AU_STATUS'
      end
      object AU_TIPODIA: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AU_TIPODIA'
      end
      object au_tipo: TcxGridDBColumn
        Caption = 'Incidencia'
        DataBinding.FieldName = 'au_tipo'
      end
      object ho_codigo: TcxGridDBColumn
        Caption = 'Horario'
        DataBinding.FieldName = 'ho_codigo'
      end
      object us_codigo_GRID: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'us_codigo'
      end
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 72
    Width = 1003
    Height = 146
    Align = alTop
    TabOrder = 3
    Properties.ActivePage = TabSheet_Dias
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 118
    ClientRectLeft = 2
    ClientRectRight = 1001
    ClientRectTop = 2
    object TabSheet_Dias: TcxTabSheet
      Caption = 'D'#237'as'
      ImageIndex = 0
      object Label17: TLabel
        Left = 25
        Top = 9
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarios:'
      end
      object NO_DIAS_SG: TZetaDBTextBox
        Left = 76
        Top = 93
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS: TZetaDBTextBox
        Left = 76
        Top = 8
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label18: TLabel
        Left = 24
        Top = 26
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_DIAS_AS: TZetaDBTextBox
        Left = 76
        Top = 25
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 6
        Top = 43
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajados:'
      end
      object NO_DIAS_NT: TZetaDBTextBox
        Left = 76
        Top = 42
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_NT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_NT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label24: TLabel
        Left = 17
        Top = 60
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Suspensi'#243'n:'
      end
      object NO_DIAS_SU: TZetaDBTextBox
        Left = 76
        Top = 59
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SU'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SU'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 13
        Top = 77
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incapacidad:'
      end
      object NO_DIAS_IN: TZetaDBTextBox
        Left = 76
        Top = 76
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_IN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_IN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 17
        Top = 94
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso SG:'
      end
      object Label27: TLabel
        Left = 143
        Top = 9
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso CG:'
      end
      object NO_DIAS_CG: TZetaDBTextBox
        Left = 202
        Top = 8
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label28: TLabel
        Left = 129
        Top = 26
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otros permisos:'
      end
      object NO_DIAS_OT: TZetaDBTextBox
        Left = 202
        Top = 25
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_OT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_OT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label29: TLabel
        Left = 169
        Top = 43
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ajuste:'
      end
      object NO_DIAS_AJ: TZetaDBTextBox
        Left = 202
        Top = 42
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label30: TLabel
        Left = 155
        Top = 60
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Retardos:'
      end
      object NO_DIAS_RE: TZetaDBTextBox
        Left = 202
        Top = 59
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_RE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_RE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_EM: TZetaDBTextBox
        Left = 202
        Top = 93
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_EM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_EM'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_SS: TZetaDBTextBox
        Left = 202
        Top = 76
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label25: TLabel
        Left = 172
        Top = 77
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'IMSS:'
      end
      object Label26: TLabel
        Left = 169
        Top = 94
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'E Y M:'
      end
      object Label19: TLabel
        Left = 275
        Top = 9
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas injustificadas:'
      end
      object NO_DIAS_FI: TZetaDBTextBox
        Left = 370
        Top = 8
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label20: TLabel
        Left = 283
        Top = 26
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas justificadas:'
      end
      object NO_DIAS_FJ: TZetaDBTextBox
        Left = 370
        Top = 25
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_FV: TZetaDBTextBox
        Left = 370
        Top = 42
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FV'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FV'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label46: TLabel
        Left = 262
        Top = 43
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas por vacaciones:'
      end
      object Label31: TLabel
        Left = 272
        Top = 60
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones a pagar:'
      end
      object NO_DIAS_VA: TZetaDBTextBox
        Left = 370
        Top = 59
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_VA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_VA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_AG: TZetaDBTextBox
        Left = 370
        Top = 76
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label32: TLabel
        Left = 281
        Top = 77
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aguinaldo a pagar:'
      end
      object Label40: TLabel
        Left = 266
        Top = 94
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subsidio incapacidad:'
      end
      object NO_DIAS_SI: TZetaDBTextBox
        Left = 370
        Top = 93
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object gbAutorizacion: TGroupBox
        Left = 435
        Top = 9
        Width = 210
        Height = 80
        Caption = ' Autorizaci'#243'n de pren'#243'mina '
        TabOrder = 0
        object Label41: TLabel
          Left = 8
          Top = 20
          Width = 53
          Height = 13
          Alignment = taRightJustify
          Caption = 'Supervisor:'
        end
        object NO_SUP_OK: TZetaDBTextBox
          Left = 62
          Top = 19
          Width = 140
          Height = 17
          AutoSize = False
          Caption = 'NO_SUP_OK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_DES_OK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label42: TLabel
          Left = 28
          Top = 37
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object NO_FEC_OK: TZetaDBTextBox
          Left = 62
          Top = 36
          Width = 140
          Height = 17
          AutoSize = False
          Caption = 'NO_FEC_OK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FEC_OK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label43: TLabel
          Left = 35
          Top = 54
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object NO_HOR_OK: TZetaDBTextBox
          Left = 62
          Top = 53
          Width = 140
          Height = 17
          AutoSize = False
          Caption = 'NO_HOR_OK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_HOR_OK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object ModificarAsistencia_DevEx: TcxButton
        Left = 664
        Top = 25
        Width = 50
        Height = 50
        Hint = 'Modificar Asistencia'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = ModificarAsistencia_DevExClick
        OptionsImage.Glyph.Data = {
          36240000424D3624000000000000360000002800000030000000300000000100
          200000000000002400000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF04B3E9FF8BDCF5FFEFFA
          FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3FBFEFF9BE1F6FF0CB6EAFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF70D4F3FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF97E0F6FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD7F3FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FDFEFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFE3F7FDFFFFFFFFFFEFFAFEFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFFEBF9FDFFFFFFFFFFE7F8FDFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFFDFF5FCFFF3FBFEFFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FDFEFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFE3F7FDFFFFFFFFFFEFFAFEFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFFEBF9FDFFFFFFFFFFE7F8FDFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFFDFF5FCFFF3FBFEFFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FDFEFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFE3F7FDFFFFFFFFFFEFFAFEFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFFEBF9FDFFFFFFFFFFE7F8FDFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
          FCFFDFF5FCFFDFF5FCFFF3FBFEFFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF3FC5EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF52CAF0FF03B3E9FF06B4EAFF0FB7EAFF00B2
          E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF35C2EDFF00B2E9FF0DB6EAFF07B4E9FF01B2
          E9FF0BB5EAFF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF42C5EFFFF2FBFDFFF8FDFEFFF2FBFEFFD8F3
          FCFF02B2E9FF09B5EAFF70D4F3FFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFEFFAFEFFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFEC
          F9FFC7EEFAFFFFFFFFFFDFF5FCFFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFEC
          F9FFBFECF9FFD7F3FCFFFFFFFFFF42C6EFFFFBFEFEFFFFFFFFFFFFFFFFFFEDF9
          FDFF00B2E9FF00B2E9FF08B4EAFF4BC8F0FFFFFFFFFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF36C2EEFFD1F1FBFFFFFFFFFFD3F1FBFF12B7
          EBFF00B2E9FF00B2E9FF00B2E9FF05B4EAFF31C1EDFFFFFFFFFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF34C2EDFFB2E8F8FFCEF1FBFF00B2E9FF00B2
          E9FF00B2E9FF78D6F3FF6CD3F2FF00B2E9FF03B3EAFF2FC1EDFFDFF5FCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF32C1EDFF04B3E9FF00B2E9FF00B2E9FF04B3
          E9FF9FE2F7FFFFFFFFFFFBFEFFFF48C8EFFF00B2E9FF05B4EAFF12B7EBFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF20BCECFFFFFFFFFF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF60CFF1FFFFFFFFFF2DC0EDFF00B2E9FF00B2E9FF14B8EBFFC3ED
          FAFFFFFFFFFFFFFFFFFFFFFFFFFFF3FBFEFF2CBFEDFF00B2E9FF02B2E9FF0AB5
          EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFC7EEFAFF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF20BC
          ECFF3CC4EEFFFFFFFFFF8FDDF5FF20BCECFF20BCECFF20BCECFF20BCECFF20BC
          ECFF20BCECFF74D5F3FFFFFFFFFF2ABFECFF00B2E9FF10B7EAFFDBF4FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF5FCFF18B9EBFF00B2E9FF04B3
          E9FF03B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5CCEF1FF06B4EAFF00B2E9FF8FDDF5FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7EEFAFF08B4EAFF00B2
          E9FF08B4EAFF01B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF25BDECFF00B2E9FF04B3E9FFB7E9
          F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3E3F7FF00B2
          E9FF00B2E9FF04B3EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF79D7F3FF0BB5EAFF00B2E9FF0CB6
          EAFFD7F3FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80D9
          F4FF00B2E9FF00B2E9FF03B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFDFF5FCFF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFDFF5FCFF80D9F4FF80D9F4FF80D9F4FF52CBF0FF05B4E9FF00B2
          E9FF24BDECFFEBF9FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF58CDF1FF00B2E9FF00B2E9FF01B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FFBFECF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF48C8EFFF07B4
          EAFF00B2E9FF3CC4EEFFFBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF7FDFEFF34C2EDFF00B2E9FF02B3E9FF01B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FF9FE2F7FF9FE2F7FF9FE2
          F7FF64D0F2FF00B2E9FFBFECF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF28BEECFF9FE2F7FF9FE2F7FF9FE2F7FF3AC4
          EEFF05B4E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB7E9F9FF0CB6EAFF00B2E9FF00B2E9FF01B3E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFB7E9F9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FFFFFFFFFFFFFFFFFFFFFF
          FFFF9FE2F7FF00B2E9FFBFECF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFF60CF
          F1FF28BEEDFF01B2E9FF00B2E9FF87DBF5FFFFFFFFFFFFFFFFFFFFFFFFFF97E0
          F6FF04B3E9FF20BCECFFAFE7F8FF18B9EBFF02B3E9FF08B4EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF58CDF1FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FFFFFFFFFFFFFFFFFFFFFF
          FFFF9FE2F7FF00B2E9FFBFECF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFF60CF
          F1FF00B2E9FF05B3E9FF02B3E9FF04B3E9FFB3E8F8FFFBFEFFFF74D5F3FF00B2
          E9FF3CC4EEFFE7F8FDFFFFFFFFFFB7E9F9FF02B3E9FF05B4E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF64D0F2FFCFF1
          FBFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2E9FFFFFFFFFFFFFFFFFFFFFF
          FFFF9FE2F7FF00B2E9FFBFECF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBFECF9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFF60CF
          F1FF00B2E9FF0DB6EAFF07B4EAFF02B3E9FF0CB6EAFF44C7EFFF00B2E9FF5CCE
          F1FFFBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFF11B7EAFF0AB5EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFD3F2FBFFFFFFFFFFFFFF
          FFFF78D6F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFFF3FBFEFFFFFFFFFFFFFFFFFF44C7
          EFFF00B2E9FF00B2E9FF00B2E9FF02B3E9FF02B3E9FF00B2E9FF6CD3F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0DB6EAFF0DB6EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF3CC4EEFFC7EEFAFFA3E3
          F7FF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFCBEFFBFF8FDDF5FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFF05B4EAFF28BEECFFEBF9
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFA7E4F7FF06B4EAFF0DB6EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF03B3E9FF05B3EAFF20BC
          ECFFA3E3F7FFB7E9F9FF64D0F2FF08B4EAFF08B4EAFF0DB6EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF05B3E9FF03B3
          E9FF00B2E9FF00B2E9FF00B2E9FF06B4EAFF10B7EAFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF02B3
          E9FF04B3E9FF04B3E9FF0AB5EAFF12B7EBFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF0AB5EAFF02B3E9FF05B3EAFF11B7EBFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      end
    end
    object TabSheet_Horas: TcxTabSheet
      Caption = 'Horas'
      ImageIndex = 1
      object Label4: TLabel
        Left = 13
        Top = 14
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarias:'
      end
      object NO_HORAS: TZetaDBTextBox
        Left = 65
        Top = 12
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 31
        Top = 32
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extras:'
      end
      object NO_EXTRAS: TZetaDBTextBox
        Left = 65
        Top = 30
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_EXTRAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_EXTRAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 27
        Top = 50
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dobles:'
      end
      object NO_DOBLES: TZetaDBTextBox
        Left = 65
        Top = 48
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DOBLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DOBLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 29
        Top = 68
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Triples:'
      end
      object NO_TRIPLES: TZetaDBTextBox
        Left = 65
        Top = 66
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TRIPLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TRIPLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label8: TLabel
        Left = 27
        Top = 86
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tardes:'
      end
      object NO_TARDES: TZetaDBTextBox
        Left = 65
        Top = 84
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TARDES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TARDES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label45: TLabel
        Left = 145
        Top = 14
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajadas:'
      end
      object NO_HORASNT: TZetaDBTextBox
        Left = 215
        Top = 12
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORASNT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORASNT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label10: TLabel
        Left = 138
        Top = 32
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical:'
      end
      object NO_HORA_PD: TZetaDBTextBox
        Left = 215
        Top = 30
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_PD'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_PD'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label44: TLabel
        Left = 115
        Top = 50
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical total:'
      end
      object NO_HORAPDT: TZetaDBTextBox
        Left = 215
        Top = 48
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAPDT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAPDT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 157
        Top = 68
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Adicionales:'
      end
      object NO_ADICION: TZetaDBTextBox
        Left = 215
        Top = 66
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_ADICION'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_ADICION'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label11: TLabel
        Left = 116
        Top = 86
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descanso trabajado:'
      end
      object NO_DES_TRA: TZetaDBTextBox
        Left = 215
        Top = 84
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label36: TLabel
        Left = 281
        Top = 86
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo pagado:'
      end
      object NO_FES_PAG: TZetaDBTextBox
        Left = 359
        Top = 84
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_PAG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_PAG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_HORA_SG: TZetaDBTextBox
        Left = 359
        Top = 66
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label15: TLabel
        Left = 274
        Top = 68
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso sin goce:'
      end
      object Label14: TLabel
        Left = 269
        Top = 50
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso con goce:'
      end
      object NO_HORA_CG: TZetaDBTextBox
        Left = 359
        Top = 48
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_VAC_TRA: TZetaDBTextBox
        Left = 359
        Top = 30
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_VAC_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_VAC_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_FES_TRA: TZetaDBTextBox
        Left = 359
        Top = 12
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 274
        Top = 14
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo trabajado:'
      end
      object Label13: TLabel
        Left = 263
        Top = 32
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaci'#243'n trabajada:'
      end
    end
    object TabSheet_Turno: TcxTabSheet
      Caption = 'Turno'
      ImageIndex = 2
      object Label1: TLabel
        Left = 20
        Top = 12
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_TURNO: TZetaDBTextBox
        Left = 54
        Top = 10
        Width = 33
        Height = 17
        AutoSize = False
        Caption = 'CB_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Turno: TZetaDBTextBox
        Left = 91
        Top = 10
        Width = 217
        Height = 17
        AutoSize = False
        Caption = 'Turno'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Turno'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 25
        Top = 31
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as:'
      end
      object NO_D_TURNO: TZetaDBTextBox
        Left = 54
        Top = 30
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_D_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_D_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 10
        Top = 51
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Jornada:'
      end
      object NO_JORNADA: TZetaDBTextBox
        Left = 54
        Top = 50
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_JORNADA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_JORNADA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object bbMostrarCalendario_DevEx: TcxButton
        Left = 312
        Top = 8
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = bbMostrarCalendario_DevExClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
    end
    object TabSheet4_Generales: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 3
      object Label33: TLabel
        Left = 47
        Top = 16
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object STATUSNOM: TZetaDBTextBox
        Left = 82
        Top = 14
        Width = 137
        Height = 17
        AutoSize = False
        Caption = 'STATUSNOM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_OBSERVA: TZetaDBTextBox
        Left = 82
        Top = 32
        Width = 239
        Height = 17
        AutoSize = False
        Caption = 'NO_OBSERVA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_OBSERVA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label16: TLabel
        Left = 6
        Top = 34
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
      end
      object Label34: TLabel
        Left = 37
        Top = 52
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 82
        Top = 50
        Width = 239
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_USER_RJ: TZetaDBTextBox
        Left = 82
        Top = 68
        Width = 239
        Height = 17
        AutoSize = False
        Caption = 'NO_USER_RJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_USER_RJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label35: TLabel
        Left = 29
        Top = 70
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 464
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 7864576
  end
  object dsAusencia: TDataSource
    Left = 472
    Top = 32
  end
  object dsPeriodo: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 480
    Top = 40
  end
end
