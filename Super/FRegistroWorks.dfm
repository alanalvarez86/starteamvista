inherited RegistroWorks: TRegistroWorks
  Left = 385
  Top = 189
  Caption = 'Registro Individual De Operaci'#243'n'
  ClientHeight = 283
  ClientWidth = 512
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 247
    Width = 512
    DesignSize = (
      512
      36)
    inherited OK_DevEx: TcxButton
      Left = 346
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 425
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 512
    inherited ValorActivo2: TPanel
      Width = 186
      inherited textoValorActivo2: TLabel
        Width = 180
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 512
    Height = 54
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 61
      Top = 7
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object Label2: TLabel
      Left = 78
      Top = 30
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object CB_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 115
      Top = 3
      Width = 300
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_CODIGO'
      DataSource = DataSource
    end
    object AU_FECHA: TZetaDBFecha
      Left = 115
      Top = 25
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '13/Jun/00'
      Valor = 36690.000000000000000000
      DataField = 'AU_FECHA'
      DataSource = DataSource
    end
  end
  object Operaciones: TcxPageControl [4]
    Left = 0
    Top = 104
    Width = 512
    Height = 143
    Align = alClient
    TabOrder = 4
    Properties.ActivePage = Generales
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 141
    ClientRectLeft = 2
    ClientRectRight = 510
    ClientRectTop = 28
    object Generales: TcxTabSheet
      Caption = 'Generales'
      object PanelHora: TPanel
        Left = 0
        Top = 0
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LblHora: TLabel
          Left = 96
          Top = 4
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object LblCantidad: TLabel
          Left = 194
          Top = 4
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cantidad:'
        end
        object WK_HORA_R: TZetaDBHora
          Left = 125
          Top = 0
          Width = 42
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Text = '    '
          Tope = 48
          Valor = '    '
          DataField = 'WK_HORA_R'
          DataSource = DataSource
        end
        object WK_PIEZAS: TZetaDBNumero
          Left = 242
          Top = 0
          Width = 65
          Height = 21
          Mascara = mnPesos
          TabOrder = 1
          Text = '0.00'
          DataField = 'WK_PIEZAS'
          DataSource = DataSource
        end
      end
      object PanelOrden: TPanel
        Left = 0
        Top = 23
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object LblOrden: TLabel
          Left = 36
          Top = 4
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Orden de Trabajo:'
        end
        object WO_NUMBER: TZetaDBKeyLookup_DevEx
          Left = 125
          Top = 0
          Width = 327
          Height = 21
          LookupDataset = dmLabor.cdsWOrderLookup
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
          DataField = 'WO_NUMBER'
          DataSource = DataSource
        end
        object MultiLote: TcxButton
          Left = 454
          Top = 0
          Width = 21
          Height = 21
          Hint = 'MultiLoteorig'
          TabOrder = 1
          OnClick = MultiLote2Click
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A4050000000000000000000000000000000000004B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF928893FFBBB5
            BCFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5
            BCFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFFBBB5BCFFAAA3ABFF786C7AFF786C7AFF786C7AFF786C7AFF786C
            7AFF786C7AFF786C7AFFEEEDEEFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF8F8590FFF4F3F4FFF7F6F7FF8F8590FF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFE8E6E9FF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF8F8590FFA59DA6FFA097
            A1FFD2CED2FFD2CED2FFD2CED2FFD2CED2FFD2CED2FFD2CED2FFD2CED2FFD2CE
            D2FFD2CED2FF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF5647
            58FFAAA3ABFFBBB5BCFF786C7AFF786C7AFF786C7AFF786C7AFF786C7AFF786C
            7AFF786C7AFF5C4D5EFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF786C7AFFEEEDEEFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5
            BCFFBBB5BCFFBBB5BCFFBBB5BCFFDDDADDFF8F8590FF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFA59DA6FFBBB5BCFFD2CED2FF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFA59DA6FF8F85
            90FF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFEEED
            EEFF9A919BFFF4F3F4FFD2CED2FFD2CED2FFD2CED2FFD2CED2FFD2CED2FFD2CE
            D2FFD2CED2FFE8E6E9FF8F8590FF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFFE8E6E9FF514153FF625463FF625463FF625463FF6254
            63FF625463FF625463FF625463FF625463FF625463FF534455FF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFE8E6E9FFE6E3E6FFD2CED2FFD2CE
            D2FFD2CED2FFD2CED2FFD2CED2FFE6E3E6FF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFFFFF
            FFFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFE8E6E9FF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFFFFFFFFFF8F8590FF8F8590FF8F8590FF8F8590FF8F85
            90FF8F8590FFF1F0F1FF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFFBBB5BCFFBBB5BCFFBBB5
            BCFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5BCFFBBB5BCFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
            4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF}
        end
      end
      object PanelParte: TPanel
        Left = 0
        Top = 46
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object LblParte: TLabel
          Left = 94
          Top = 4
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Parte:'
        end
        object AR_CODIGO: TZetaDBKeyLookup_DevEx
          Left = 125
          Top = 0
          Width = 327
          Height = 21
          LookupDataset = dmLabor.cdsPartes
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
          DataField = 'AR_CODIGO'
          DataSource = DataSource
        end
      end
      object PanelOpera: TPanel
        Left = 0
        Top = 92
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object LblOpera: TLabel
          Left = 70
          Top = 4
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Operaci'#243'n:'
        end
        object OP_NUMBER: TZetaDBKeyLookup_DevEx
          Left = 125
          Top = 0
          Width = 327
          Height = 21
          LookupDataset = dmLabor.cdsOpera
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
          DataField = 'OP_NUMBER'
          DataSource = DataSource
        end
      end
      object PanelArea: TPanel
        Left = 0
        Top = 69
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object LblArea: TLabel
          Left = 97
          Top = 4
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = 'Area:'
        end
        object CB_AREA: TZetaDBKeyLookup_DevEx
          Left = 125
          Top = 0
          Width = 327
          Height = 21
          LookupDataset = dmLabor.cdsArea
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
          DataField = 'CB_AREA'
          DataSource = DataSource
        end
      end
    end
    object Moduladores: TcxTabSheet
      Caption = 'Moduladores'
      ImageIndex = 1
      object PanelStatus: TPanel
        Left = 0
        Top = 0
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LblStatus: TLabel
          Left = 115
          Top = 4
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
        end
        object WK_STATUS: TZetaDBKeyCombo
          Left = 151
          Top = 0
          Width = 170
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfStatusLectura
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'WK_STATUS'
          DataSource = DataSource
          LlaveNumerica = True
        end
      end
      object PanelModula1: TPanel
        Left = 0
        Top = 23
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object LblModula1: TLabel
          Left = 79
          Top = 4
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modulador #1:'
        end
        object WK_MOD_1: TZetaDBKeyLookup_DevEx
          Left = 151
          Top = 0
          Width = 300
          Height = 21
          LookupDataset = dmLabor.cdsModula1
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 45
          DataField = 'WK_MOD_1'
          DataSource = DataSource
        end
      end
      object PanelModula2: TPanel
        Left = 0
        Top = 46
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object LblModula2: TLabel
          Left = 79
          Top = 4
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modulador #2:'
        end
        object WK_MOD_2: TZetaDBKeyLookup_DevEx
          Left = 151
          Top = 0
          Width = 300
          Height = 21
          LookupDataset = dmLabor.cdsModula2
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 45
          DataField = 'WK_MOD_2'
          DataSource = DataSource
        end
      end
      object PanelModula3: TPanel
        Left = 0
        Top = 69
        Width = 508
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object LblModula3: TLabel
          Left = 79
          Top = 4
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modulador #3:'
        end
        object WK_MOD_3: TZetaDBKeyLookup_DevEx
          Left = 151
          Top = 0
          Width = 300
          Height = 21
          LookupDataset = dmLabor.cdsModula3
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 45
          DataField = 'WK_MOD_3'
          DataSource = DataSource
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 12
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
