unit FAsignarEmpleados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ZetaStateComboBox, ComCtrls, ZetaDBTextBox,
  Buttons, ExtCtrls, Menus, ImgList, ActnList, ZetaSmartLists, Mask,
  ZetaFecha, db, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013, cxButtons,
  ZBaseDlgModal_DevEx, dxSkinsdxBarPainter, cxLabel, dxBar, cxBarEditItem,
  dxBarExtItems, cxClasses, cxTextEdit, cxControls, cxContainer, cxEdit,
  cxCheckBox,Variants, cxDropDownEdit, System.Actions;

type
  TAsignaEmpleados = class(TZetaDlgModal_DevEx)
    ListEmp: TListView;
    ImageEmpleadosbkup: TImageList;
    ImageSupervisoresbkup: TImageList;
    PopupMenu1: TPopupMenu;
    Cortar1: TMenuItem;
    Pegar1: TMenuItem;
    N1: TMenuItem;
    DeshacerCambios1: TMenuItem;
    N2: TMenuItem;
    Ordenar1: TMenuItem;
    Nmero1: TMenuItem;
    Ascendente1: TMenuItem;
    Descendente1: TMenuItem;
    Nombre1: TMenuItem;
    Ascendente2: TMenuItem;
    Descendente2: TMenuItem;
    ImageEmpleadosBig: TImageList;
    ImageSupervisores: TcxImageList;
    ImageEmpleados: TcxImageList;
    Panel1: TPanel;
    TreeSup: TTreeView;
    CheckSupervisores: TcxCheckBox;
    ActionList1: TActionList;
    BusquedaAction: TAction;
    PanelPa: TPanel;
    PaEmpLbl: TLabel;
    LabDel: TLabel;
    FechaLbl: TZetaTextBox;
    Label2: TLabel;
    PanelBtnList: TPanel;
    PaEmp: TStateComboBox;
    dFecha2: TZetaFecha;
    PaEmpBtn_DevEx: TcxButton;
    PaSupBtn: TcxButton;
    editBuscaold: TEdit;
    Label1: TLabel;
    Edit1: TEdit;
    Cortarbtn: TcxButton;
    PegarBtn: TcxButton;
    UndoBtn: TcxButton;
    ImprimirFormaBtn: TcxButton;
    ImagesMenu: TcxImageList;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeSupExpanding(Sender: TObject; Node: TTreeNode; var AllowExpansion: Boolean);
    procedure TreeSupChange(Sender: TObject; Node: TTreeNode);
    procedure TreeSupDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState;
              var Accept: Boolean);
    procedure TreeSupDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ListEmpStartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure ListEmpCompare(Sender: TObject; Item1, Item2: TListItem; Data: Integer;
              var Compare: Integer);
    procedure SelectListViewType(Sender: TObject);
    procedure SelectListViewTypeMenu(Sender: TObject);
    procedure ListEmpColumnClick(Sender: TObject; Column: TListColumn);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure PaEmpLookUp(Sender: TObject; var lOk: Boolean);
    procedure CortarBtnClick(Sender: TObject);
    procedure ImprimirFormaBtnClick(Sender: TObject);
    procedure PegarBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure OrdenarPopup(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure dFecha2ValidDate(Sender: TObject);
    procedure PaEmpBtn_DevExClick(Sender: TObject);
    procedure dxBarManager1ShowToolbarsPopup(Sender: TdxBarManager;
      PopupItemLinks: TdxBarItemLinks);
    procedure PaSupBtnClick(Sender: TObject);
    function BuscaNodoTexto( oArbol : TTreeView; sCaption, sTexto : String ) : Boolean;
    procedure BuscaNodoReset;
    procedure CheckSupervisoresClick(Sender: TObject);
    procedure EditBusca2KeyPress(Sender: TObject; var Key: Char);
    procedure BuscaShortcutExecute(Sender: TObject);
    procedure TreeSupKeyPress(Sender: TObject; var Key: Char);
    procedure cxBarEditItem5PropertiesChange(Sender: TObject);
    procedure EditBuscaPropertiesChange(Sender: TObject);
    procedure BusquedaActionExecute(Sender: TObject);
    procedure EditBuscaPropertiesEditValueChanged(Sender: TObject);
    procedure editBuscaoldKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
      procedure BuscaArbol;
    procedure PaEmpKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    Reubicando : Boolean;
    FHuboCambios, FPuedeModificar: Boolean;
    FOrden: (Asc, Desc);
    FColumnaOrden: Integer;
    procedure SetHuboCambios(const Value: Boolean);
    property HuboCambios: Boolean read FHuboCambios write SetHuboCambios;
    procedure SetControles;
    procedure LlenaListaEmp(oNodoSup: TTreeNode);
    procedure LlenaListaSup;
    procedure LlenaClientDataSet;
    procedure CancelarCambios;
    procedure EscribirCambios;
    procedure QuitaCutsAnteriores( oArbol: TTreeView; oLista: TListView );
    procedure MueveCutsActuales( oArbol: TTreeView; NodoDestino: TTreeNode );
    function PonerCutsActuales( oArbol: TTreeView; oLista: TListView ) : Integer;
    function GetItem(oLista: TListView; sEmpleado: String): TListItem;
    function GetNodo(oArbol: TTreeView; sEmpleado: String): TTreeNode;
    function EsNodo(oNodoDestino: TTreeNode): Boolean;
    procedure UbicaEmpleado(sEmpleado: String);
    procedure ActualBoton(iTag: Integer);
    procedure ActualPopup(iTag: Integer);
    procedure SetDerechosAcceso;
  public
    { Public declarations }
  end;

var
  AsignaEmpleados: TAsignaEmpleados;
  FUltimoTexto : String;
  FNodosTexto  : Integer;
  BrincaEnter: Boolean;
implementation

uses DSuper, DCliente, ZetaCommonTools, ZetaDialogo, ZAccesosMgr, ZAccesosTress,
     ZetaCommonClasses,ZCerrarEdicion_DevEx, ZetaBuscaEmpleado_DevEx;
const
     K_IMAGEN_MIS_SUPER      = 0;
     K_IMAGEN_OTROS_SUPER    = 1;
     K_IMAGEN_FOLDER         = 2;
     K_IMAGEN_FOLDER_ABIERTO = 3;
     K_IMAGEN_EMPLEADO       = 4;
     K_IMAGEN_EMPLEADO_ASIG  = 5;
     K_IMAGEN_SUP_NOASIG     = 6;

{$R *.DFM}

procedure TAsignaEmpleados.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 00502;
     FHuboCambios := TRUE;       //Para Obligar el SetControles en el OnShow
     FPuedeModificar := ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_EMPLEADOS, K_DERECHO_CAMBIO );
     SetDerechosAcceso;
end;

procedure TAsignaEmpleados.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with dmSuper do
             begin
                  cdsSupervisores.Conectar;
                  cdsMisAsignados.Conectar;
             end;
             with dmCliente do
             begin
                  FechaLbl.Caption := FechaCorta( FechaSupervisor );
                  dFecha2.Valor := FechaSupervisor;
             end;
             dmSuper.CdsSupervisoresFiltroActivos(True);
             LlenaListaSup;
             HuboCambios := FALSE;
             TreeSup.SetFocus();
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TAsignaEmpleados.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     CanClose := True;
     if HuboCambios then
     begin
          case ZCerrarEdicion_DevEx.CierraEdicion of
               mrOk: EscribirCambios;
               mrIgnore: CancelarCambios;
          else
              CanClose := FALSE;
          end;
     end;
     if(canClose =true) then
     begin
     editBuscaold.Clear;
     PaEmp.Clear;
     CheckSupervisores.Checked := false;
     end;
end;

procedure TAsignaEmpleados.SetDerechosAcceso;
begin
     ImprimirFormaBtn.Enabled := ZAccesosMgr.CheckDerecho( D_SUPER_ASIGNAR_EMPLEADOS, K_DERECHO_IMPRESION );
     CortarBtn.Enabled := FPuedeModificar;
     Cortar1.Enabled := FPuedeModificar;
end;


{ ************* Grabar o Cancelar Informaci�n *********** }

procedure TAsignaEmpleados.LlenaClientDataSet;
var
   oNodo : TTreeNode;
   EmpCodigo, iEmpleadoActual : Integer;
   SuperCodigo, SuperAnterior : String;
   dFechaAsignacion: TDate;

   function RegresaCodigo( Texto: String ): Integer;
   begin
        Result := StrToInt( Copy( Texto, 2, pos(']',Texto)-2 ) );
   end;
   function RegresaSuper( Texto: String ): String;
   begin
        Result := Copy( Texto, 2, pos(']',Texto)-2 );
   end;

begin
     with dmSuper do
     begin
          cdsEmpleados.DisableControls;       // Deshabilita Refrescar el Shell
          try
             iEmpleadoActual := EmpleadoNumero;  // Empleado Actual
             oNodo:= TreeSup.Items[0];

             while ( Assigned ( oNodo ) ) do
             begin
                   if ( oNodo.Level = 2 ) then        // Es Empleado
                   begin
                        SuperCodigo := RegresaSuper( oNodo.Parent.Text );
                        EmpCodigo := RegresaCodigo( oNodo.Text );
                        SuperAnterior := ChecaSuperAnterior( EmpCodigo, SuperCodigo );

                        if ( SuperAnterior <> SuperCodigo ) then
                        begin
                             if (oNodo.ImageIndex = K_IMAGEN_EMPLEADO_ASIG) AND (dmCliente.FechaSupervisor <> dFecha2.Valor)
                                 AND ( not EsEmpleadoPorKardex( EmpCodigo)) then
                             begin
                                  UbicaEmpleado( InttoStr(EmpCodigo) );
                                  DataBaseError('No se pueden asignar empleados prestados, por rango de fechas.' + CR_LF + 'Empleado prestado: ' +
                                               oNodo.Text);
                             end
                             else
                             begin
                                  dFechaAsignacion := dmCliente.FechaSupervisor;
                                  while (dFecha2.Valor >= dFechaAsignacion) do
                                  begin
                                       cdsAsignaciones.AppendRecord( [ EmpCodigo, SuperCodigo, SuperAnterior, dFechaAsignacion ] ); //  ... Borrar Asignacion Anterior y Validar si EsAsignacion() se har� en el Servidor
                                       dFechaAsignacion := dFechaAsignacion + 1;
                                  end;
                             end;
                        end;
                        // cdsAsignaciones.AppendRecord( [ EmpCodigo, SuperCodigo, SuperAnterior ] ); //  ... Borrar Asignacion Anterior y Validar si EsAsignacion() se har� en el Servidor
                   end;

                   oNodo:= oNodo.GetNext;
             end;
             PosicionaEmpleado( iEmpleadoActual );  // Si lo encuentra lo posiciona
          finally
             RefrescarFormaActiva:= FALSE;      // Para que no se refresque la forma Activa
             cdsEmpleados.EnableControls;       // Habilita Refrescar el Shell
             RefrescarFormaActiva:= TRUE;
          end;
     end;
end;

procedure TAsignaEmpleados.EscribirCambios;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmSuper.cdsAsignaciones do
        begin
             if Active then
                Active := FALSE;
             CreateDataSet;
             LlenaClientDataSet;
             Enviar;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TAsignaEmpleados.CancelarCambios;
begin
     LlenaListaSup;
     HuboCambios := FALSE;
     TreeSup.SetFocus();
end;

procedure TAsignaEmpleados.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
     HuboCambios:= FALSE;
{    PENDIENTE : QUE HACER EN CASO DE ERRORES AL GRABAR ?
     if ( ClientDataSet.HuboErrores ) then
        ClientDataSet.PosicionaError
     else }
        Close;
end;

procedure TAsignaEmpleados.CancelarClick(Sender: TObject);
begin
     inherited;
     if HuboCambios then
        CancelarCambios
     else
        Close;
end;

{ Llena Componentes de Datos TreeSup y ListEmp }

procedure TAsignaEmpleados.LlenaListaEmp( oNodoSup : TTreeNode );
var
   oNodo : TTreeNode;
   NewItem : TListItem;

    function ObtieneNumero( Texto: String ): String;
    begin
         Result := Copy( Texto, 2, pos(']',Texto)-2 );
    end;

    function ObtieneNombre( Texto: String ): String;
    begin
         delete( Texto, 1, pos(']',Texto) );
         Result := Texto;
    end;

begin
     with ListEmp.Items do
     begin
          BeginUpdate;
          Clear;
          if Assigned( oNodoSup ) then
          begin
               With oNodoSup do
               Begin
                    oNodo := oNodoSup.GetFirstChild;
                    while Assigned( oNodo ) do
                    begin
                         NewItem:= Add;
                         if Assigned( NewItem ) then
                         begin
                              NewItem.Caption := ObtieneNumero( oNodo.Text );
                              NewItem.Data := oNodo;
                              NewItem.ImageIndex:= oNodo.ImageIndex - 4;
                              NewItem.StateIndex:= oNodo.ImageIndex - 4;
                              NewItem.Cut := oNodo.Cut;
                              NewItem.SubItems.Add( ObtieneNombre( oNodo.Text ) );
                         end;
                         oNodo := oNodoSup.GetNextChild(oNodo);
                    end;
               end;
          end;
          EndUpdate;
     end;
end;

procedure TAsignaEmpleados.LlenaListaSup;

var
   NewGrupo, NewNodo, NewHijo : TTreeNode;
   iEmpleadoActual,inicio : Integer;
   Valor:String;
{
   procedure SetFiltroSupervisores( const lMisSuper: Boolean );
   begin
        with dmSuper.cdsSupervisores do
        begin
             Filtered := FALSE;
             if lMisSuper then
                Filter   := 'MI_CODIGO <> '''''
             else
                Filter   := 'MI_CODIGO = ''''';
             Filtered := TRUE;
        end;
   end;
}
   procedure SetFiltroEmpleados( const sNivel: String );
   begin
        with dmSuper.cdsEmpleados do
        begin
             Filter := Format( 'CB_NIVEL = ''%s''', [ sNivel ] );
             Filtered := TRUE;
        end;
   end;

begin
     with dmSuper do
     begin
          cdsEmpleados.DisableControls;       // Deshabilita Refrescar el Shell
          try
             iEmpleadoActual := EmpleadoNumero;  // Empleado Actual

             with TreeSup.Items do
             begin
                  Reubicando := True;
                  BeginUpdate;
                  Clear;

                  // Mis Supervisores
                  SetFiltroSupervisores( TRUE );
                  NewGrupo:= Add( nil , 'Mis Supervisores' );
                  if Assigned( NewGrupo ) then
                  begin
                       NewGrupo.ImageIndex:= K_IMAGEN_MIS_SUPER;
                       NewGrupo.SelectedIndex:= K_IMAGEN_MIS_SUPER;

                       cdsSupervisores.First;

                       while not cdsSupervisores.EOF do
                       begin
                            NewNodo:= AddChild( NewGrupo, '[' + cdsSupervisores.FieldByName('TB_CODIGO').AsString + '] ' + cdsSupervisores.FieldByName('TB_ELEMENT').AsString );
                            if Assigned( NewNodo ) then
                            begin
                                if(cdsSupervisores.FieldByName('TB_ACTIVO').AsString = 'S') then
                                begin
                                 NewNodo.ImageIndex:= K_IMAGEN_FOLDER;
                                 NewNodo.SelectedIndex:= K_IMAGEN_FOLDER_ABIERTO;
                                end
                                else
                                begin
                                   NewNodo.ImageIndex:= K_IMAGEN_SUP_NOASIG;
                                    NewNodo.SelectedIndex:= K_IMAGEN_SUP_NOASIG
                                end;
                                 SetFiltroEmpleados( cdsSupervisores.FieldByName('TB_CODIGO').AsString );
                                 cdsEmpleados.First;
                                 while ( not cdsEmpleados.EOF ) do
                                 begin
                                      NewHijo:= AddChild( NewNodo, '[' + cdsEmpleados.FieldByName('CB_CODIGO').AsString + ']' + cdsEmpleados.FieldByName('PRETTYNAME').AsString );
                                      if Assigned( NewHijo ) then
                                      begin
                                           if ( cdsEmpleados.FieldByName('CB_TIPO').AsInteger = 1 ) then
                                           begin
                                                NewHijo.ImageIndex:= K_IMAGEN_EMPLEADO;
                                                NewHijo.SelectedIndex:= K_IMAGEN_EMPLEADO;
                                           end
                                           else
                                           begin
                                                NewHijo.ImageIndex:= K_IMAGEN_EMPLEADO_ASIG;
                                                NewHijo.SelectedIndex:= K_IMAGEN_EMPLEADO_ASIG;
                                           end;
                                      end;
                                      cdsEmpleados.Next;
                                 end;
                                 cdsEmpleados.Filtered := False;
                            end;
                            cdsSupervisores.Next;
                       end;
                  end;

                  // Otros Supervisores
                  SetFiltroSupervisores( FALSE );
                  NewGrupo:= Add( nil , 'Otros Supervisores' );
                  if Assigned( NewGrupo ) then
                  begin
                       NewGrupo.ImageIndex:= K_IMAGEN_OTROS_SUPER;
                       NewGrupo.SelectedIndex:= K_IMAGEN_OTROS_SUPER;
                       cdsSupervisores.First;
                       while ( not cdsSupervisores.EOF ) do
                       begin
                            NewNodo:= AddChild( NewGrupo, '[' + cdsSupervisores.FieldByName('TB_CODIGO').AsString + '] ' + cdsSupervisores.FieldByName('TB_ELEMENT').AsString);
                            if Assigned( NewNodo ) then
                            begin
                                 NewNodo.ImageIndex:= K_IMAGEN_FOLDER;
                                 NewNodo.SelectedIndex:= K_IMAGEN_FOLDER_ABIERTO;
                                 if cdsMisAsignados.Locate( 'CB_NIVEL', cdsSupervisores.FieldByName('TB_CODIGO').AsString, []) then
                                 while ( cdsMisAsignados.FieldByName('CB_NIVEL').AsString = cdsSupervisores.FieldByName('TB_CODIGO').AsString ) and ( not cdsMisAsignados.EOF ) do
                                 begin
                                      NewHijo:= AddChild( NewNodo, '[' + cdsMisAsignados.FieldByName('CB_CODIGO').AsString + ']' + cdsMisAsignados.FieldByName('PRETTYNAME').AsString );
                                      if Assigned( NewHijo ) then
                                      begin
                                           NewHijo.ImageIndex:= K_IMAGEN_EMPLEADO;
                                           NewHijo.SelectedIndex:= K_IMAGEN_EMPLEADO;
                                      end;
                                      cdsMisAsignados.Next;
                                 end;
                            end;
                            cdsSupervisores.Next;
                       end;
                  end;

                  NewGrupo.Expand( False );           { Otros Supervisores }
                  NewGrupo := TreeSup.Items[0];       { Mis Supervisores }
                  NewGrupo.Expand( False );

                  EndUpdate;

                  Reubicando := False;

                  if ( NewGrupo.HasChildren ) then    { Mostrar Primer Supervisor }
                  begin
                       TreeSup.Selected := NewGrupo.GetFirstChild;
                       TreeSup.OnChange( self, TreeSup.Selected );
                  end;

             end;
             PosicionaEmpleado( iEmpleadoActual );  // Si lo encuentra lo posiciona
          finally
             RefrescarFormaActiva:= FALSE;      // Para que no se refresque la forma Activa
             cdsEmpleados.EnableControls;       // Habilita Refrescar el Shell
             RefrescarFormaActiva:= TRUE;
          end;
     end;
     For Inicio:=0 to Dmsuper.cdsSupervisores.FieldCount -1 do begin

      valor := DmsUper.cdsSupervisores.Fields[inicio].AsString;
     end;
end;

{ ************** Eventos del Arbol y Lista ************** }

procedure TAsignaEmpleados.TreeSupExpanding(Sender: TObject; Node: TTreeNode;
          var AllowExpansion: Boolean);
begin
     inherited;
     if ( not Reubicando ) then
     begin
         if Node.Level = 1 then
         begin
              AllowExpansion := False;
              TreeSup.Selected := Node;
              LlenaListaEmp( Node );
         end;
     end;
end;

procedure TAsignaEmpleados.TreeSupChange(Sender: TObject; Node: TTreeNode);
begin
     inherited;
     if not Reubicando then
     begin
        if Node.Level = 1 then     //Esta Seleccionado un Supervisor
           LlenaListaEmp( Node )
        else
           LlenaListaEmp( nil );
     end;
end;

procedure TAsignaEmpleados.TreeSupDragOver(Sender, Source: TObject; X, Y: Integer;
          State: TDragState; var Accept: Boolean);
begin
     inherited;
     Accept:= ( FPuedeModificar ) and ( Sender is TTreeView ) and ( Source is TListView ) and
              ( EsNodo( TTreeView( Sender).GetNodeAt(X,Y) ) );
end;

function TAsignaEmpleados.EsNodo( oNodoDestino: TTreeNode ): Boolean;
begin
     Result := False;
     if Assigned( oNodoDestino ) then
        if oNodoDestino.Level = 1 then { Es Supervisor }
           Result := True;
end;

procedure TAsignaEmpleados.TreeSupDragDrop(Sender, Source: TObject; X, Y: Integer);
var
   oNode, NodoOrigen, NodoDestino: TTreeNode;
   oItem : TListItem;
   oListaFuente: TListView;

begin
     inherited;
     NodoOrigen:= TreeSup.Selected;
     NodoDestino:= TreeSup.GetNodeAt(X,Y);

     if EsNodo( NodoDestino ) then
     begin
          oListaFuente:= TListView( source );
          if ( oListaFuente.Selected = nil ) then Exit;

          Reubicando := True;

          oListaFuente.Items.BeginUpdate;
          TreeSup.Items.BeginUpdate;

          with TreeSup.Items do
          begin
               Repeat
                     oItem := oListaFuente.Selected;
                     oNode := oItem.Data;
                     oNode.MoveTo( NodoDestino, naAddChild );
                     oItem.Delete;
               until ( oListaFuente.Selected = nil );
          end;

          HuboCambios := TRUE;

          NodoDestino.Collapse(True);
          TreeSup.Items.EndUpdate;
          oListaFuente.Items.EndUpdate;

          Reubicando := False;

          with TreeSup do
          begin
               SetFocus();
               Selected := NodoOrigen;
               OnChange( self, NodoOrigen );
          end;
     end;
end;

procedure TAsignaEmpleados.ListEmpStartDrag(Sender: TObject; var DragObject: TDragObject);
begin
     inherited;
     with ListEmp do
     begin
          if ( SelCount > 1 ) then
             DragCursor := crMultiDrag
          else
             DragCursor := crDrag;
     end;
end;

procedure TAsignaEmpleados.ListEmpCompare(Sender: TObject; Item1, Item2: TListItem;
          Data: Integer; var Compare: Integer);

   function RegresaCodigo( Texto: String ): Integer;
   begin
        Result := StrToIntDef( Texto, 0 );
   end;

begin
     inherited;
     if FColumnaOrden = 0 then { N�mero de Empleado }
        Compare := RegresaCodigo( Item1.Caption ) - RegresaCodigo( Item2.Caption )
     else
        Compare := StrComp( {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}(Item1.SubItems[ FColumnaOrden - 1 ]), {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}(Item2.SubItems[ FColumnaOrden - 1 ]) );

     if FOrden = Desc then
        Compare := Compare * -1;

end;

procedure TAsignaEmpleados.ListEmpColumnClick(Sender: TObject; Column: TListColumn);
begin
     inherited;
     if ( FColumnaOrden = Column.Index ) then
        if FOrden = Asc then
           FOrden := Desc
        else
           FOrden := Asc;

     FColumnaOrden:= Column.Index;
     ListEmp.AlphaSort; { Activar Evento onCompare }
end;

{ ************** Busqueda de Empleados ************** }

function TAsignaEmpleados.GetItem( oLista: TListView; sEmpleado: String ): TListItem;
var
   iEmpleado, i : Integer;
   lSalir: Boolean;
begin
     Result:= nil;
     i := 0;
     lSalir:= False;
     iEmpleado := StrToIntDef( sEmpleado, 0 );

     if ( oLista.Items.Count > 0 ) then
     begin
          while ( not lSalir ) do
          begin
               with oLista.Items do
               begin
                    //if ( Pos( sEmpleado, Item[i].Caption ) > 0 ) then
                    if ( iEmpleado = StrToIntDef( Item[i].Caption, 0 ) ) then
                    begin
                         Result := Item[i];
                         lSalir := True;
                         { Seleccionar Item }
                         if Item[i].Selected then
                            Item[i].Selected := False
                         else
                             Item[i].Selected := True;

                         Item[i].MakeVisible( False );
                    end;
               end;
               i := i + 1;
               if ( i = oLista.Items.Count ) then
                  lSalir := True;
          end;
     end;
end;

function TAsignaEmpleados.GetNodo( oArbol: TTreeView; sEmpleado: String ): TTreeNode;
var
   oNodo: TTreeNode;
   lSalir: Boolean;

   function GetStrEmp: String;
   begin
        Result:= '[' + sEmpleado + ']';
   end;

begin
     Result:= nil;

     Reubicando:= True;

     oNodo:= oArbol.Items[0];
     lSalir:= False;
     repeat
           if ( oNodo.Level > 1 ) then  { Si son empleados }
           begin
                if ( Pos( GetStrEmp, oNodo.Text) > 0 ) then
                begin
                     Result:= oNodo.Parent;
                     lSalir:= True;
                     oArbol.Selected:= oNodo.Parent;
                end;
           end;
           oNodo:= oNodo.GetNext;
     until ( oNodo = nil ) or lSalir;

     Reubicando := False;

     If Assigned( Result ) then
        oArbol.OnChange( self, Result );
end;

procedure TAsignaEmpleados.UbicaEmpleado( sEmpleado: String );
begin
     if not Assigned( GetItem( ListEmp, sEmpleado ) ) then { Lista Actual }
        if not Assigned( GetNodo( TreeSup, sEmpleado ) ) then { Buscar en Arbol }
           zWarning( '� Atenci�n !', '��� Empleado No Le Pertenece !!!', 0, mbOK )
        else
           if not Assigned( GetItem( ListEmp, sEmpleado ) ) then { Si encontr� en Arbol, Rebusca en la Lista ya Actualizada }
              zWarning( '� Atenci�n !', '��� Empleado No Le Pertenece !!!', 0, mbOK );
end;

procedure TAsignaEmpleados.PaEmpLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     UbicaEmpleado( PaEmp.Text );
end;

{ ************* Procesos y Funciones de Uso General *********** }

procedure TAsignaEmpleados.SetHuboCambios(const Value: Boolean);
begin
     if FHuboCambios <> Value then
     begin
          FHuboCambios:= Value;
          SetControles;
     end;
end;

procedure TAsignaEmpleados.SetControles;
begin
     OK_DevEx.Enabled := HuboCambios;
     if HuboCambios then
     begin
          with Cancelar_DevEx do
          begin

               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar_DevEx do
          begin

               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Application.ProcessMessages;
end;

procedure TAsignaEmpleados.CortarBtnClick(Sender: TObject);
begin
     inherited;
     QuitaCutsAnteriores( TreeSup, ListEmp );
     PegarBtn.Enabled := ( PonerCutsActuales( TreeSup, ListEmp ) > 0 );
     Pegar1.Enabled := PegarBtn.Enabled;
end;

procedure TAsignaEmpleados.PegarBtnClick(Sender: TObject);
var
   NodoDestino : TTreeNode;
begin
     inherited;
     NodoDestino := TreeSup.Selected;
     if Assigned( NodoDestino ) then
        if ( NodoDestino.Level = 1 ) then
        begin
           MueveCutsActuales( TreeSup, NodoDestino );
           TreeSup.OnChange( self, NodoDestino );
           PegarBtn.Enabled := False;
           Pegar1.Enabled := PegarBtn.Enabled;
        end
        else
        begin
             zInformation( Caption, 'No Puede Pegar Empleados en Grupos de Supervisores', 0 );
        end;
end;

procedure TAsignaEmpleados.UndoBtnClick(Sender: TObject);
begin
     inherited;
     if ( HuboCambios ) then
        if ZWarningConfirm( Caption, 'Seguro de Deshacer los Cambios ?', 0, mbOk ) then
        begin
             LlenaListaSup;
             HuboCambios := FALSE;
        end;
end;

procedure TAsignaEmpleados.ImprimirFormaBtnClick(Sender: TObject);
begin
     inherited;
     if zConfirm( 'Imprimir...', '� Desea Imprimir la Pantalla: ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

procedure TAsignaEmpleados.SelectListViewType(Sender: TObject);
begin
     ListEmp.ViewStyle := TViewStyle(TComponent(Sender).Tag);
     ActualPopup( TComponent(Sender).Tag );
end;

procedure TAsignaEmpleados.SelectListViewTypeMenu(Sender: TObject);
begin
     ListEmp.ViewStyle := TViewStyle(TComponent(Sender).Tag);
     ActualPopup( TComponent(Sender).Tag );
     ActualBoton( TComponent(Sender).Tag );
end;

procedure TAsignaEmpleados.ActualPopup( iTag : Integer );
begin
{
     case iTag of
          0 : VistaIconos1.Checked := True;
          1 : VistaIconosPequeos1.Checked := True;
          2 : VistaenListado1.Checked := True;
          3 : VistaenReporte1.checked := True;
     end;
     }
end;

procedure TAsignaEmpleados.ActualBoton( iTag : Integer );
begin
    { case iTag of
          0 : LargeBtn.Down := True;
          1 : SmallBtn.Down := True;
          2 : ListBtn.Down := True;
          3 : DetailsBtn.Down := True;
     end;
     }
end;

procedure TAsignaEmpleados.OrdenarPopup(Sender: TObject);
begin
     inherited;
     FColumnaOrden := TMenuItem(Sender).Parent.Tag;
     case TMenuItem(Sender).Tag of
          0 : FOrden := Asc;
          1 : FOrden := Desc;
     end;
     ListEmp.AlphaSort;   { Activar Evento onCompare }
end;

procedure TAsignaEmpleados.PopupMenu1Popup(Sender: TObject);
var
   i, j : ShortInt;
begin
     inherited;
     Ordenar1.Items[ FColumnaOrden ].Checked := True;

     for i := 0 to 1 do
         for j := 0 to 1 do
             Ordenar1.Items[ i ].Items[j].Checked := False;
     case FOrden of
          Asc : Ordenar1.Items[ FColumnaOrden ].Items[0].Checked := True;
          Desc : Ordenar1.Items[ FColumnaOrden ].Items[1].Checked := True;
     end;
end;

{ ************* Procesos de Cut y Paste *********** }

procedure TAsignaEmpleados.QuitaCutsAnteriores( oArbol: TTreeView; oLista: TListView );
var
   oNodo : TTreeNode;
   oItem, oItemPrevio : TListItem;
begin

     Reubicando := True;

     oNodo:= oArbol.Items[0];

     while ( Assigned ( oNodo ) ) do
     begin
          oNodo.Cut := False;
          oNodo:= oNodo.GetNext;
     end;

     if ( oLista.Items.Count > 0 ) then
        with oLista do
        begin
             if ( Items.Item[0].Cut ) then
                oItem := Items.Item[0]
             else
                oItem := GetNextItem( Items.Item[0], sdAll, [isCut] );

             while ( Assigned ( oItem ) ) do
             begin
                  oItem.Cut := False;
                  oItemPrevio := oItem;
                  oItem := GetNextItem( oItemPrevio, sdAll, [isCut] );
             end;
        end;

     Reubicando := False;

end;

function TAsignaEmpleados.PonerCutsActuales( oArbol: TTreeView; oLista: TListView ) : Integer;
var
   oItem, oItemPrevio : TListItem;
   oNodo : TTreeNode;
   cuantos : Integer;
begin
     Cuantos := 0;
     Result := 0;

     if ( oLista.Selected = nil ) then Exit;

     Reubicando := True;

     with oLista do
     begin
          if ( Items.Item[0].Selected ) then
             oItem := Items.Item[0]
          else
             oItem := GetNextItem( Items.Item[0], sdAll, [isSelected] );

          while ( Assigned ( oItem ) ) do
          begin
               oNodo := oItem.Data;
               oNodo.Cut := True;
               oItem.Cut := True;
               Inc( Cuantos );
               oItemPrevio := oItem;
               oItem := GetNextItem( oItemPrevio, sdAll, [isSelected] );
          end;
     end;

     Result := Cuantos;
     Reubicando := False;

end;

procedure TAsignaEmpleados.MueveCutsActuales( oArbol: TTreeView; NodoDestino: TTreeNode );
var
   oNodo : TTreeNode;
begin

     Reubicando := True;

     oArbol.Items.BeginUpdate;

     oNodo:= oArbol.Items[0];

     while Assigned( oNodo ) do
     begin
          if ( oNodo.Cut ) then
          begin
               oNodo.Cut := False;
               oNodo.MoveTo( NodoDestino, naAddChild );
               oNodo:= oArbol.Items[0];
               HuboCambios := TRUE;
          end;
          oNodo:= oNodo.GetNext;
     end;

     NodoDestino.Collapse(True);

     oArbol.Items.EndUpdate;
     Reubicando := False;

end;

procedure TAsignaEmpleados.dFecha2ValidDate(Sender: TObject);
begin
     inherited;
     If dFecha2.Valor < dmCLiente.FechaSupervisor then
     begin
          ActiveControl := dFecha2;
          DataBaseError('La fecha de terminaci�n debe ser mayor o igual a inicio.');
     end;
end;

procedure TAsignaEmpleados.PaEmpBtn_DevExClick(Sender: TObject);
var
   sEmpleado, sEmpleadoDesc: String;
begin
  inherited;
  if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sEmpleado, sEmpleadoDesc ) then
        PaEmp.AsignaValorEntero( StrToIntDef( sEmpleado, 0 ) );
end;

procedure TAsignaEmpleados.dxBarManager1ShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
  inherited;
  Abort;
end;

procedure TAsignaEmpleados.PaSupBtnClick(Sender: TObject);
begin
     inherited;
    buscaArbol;

end;


procedure TAsignaEmpleados.BuscaNodoReset;
begin
  FUltimoTexto := '';
  FNodosTexto  := 0;
end;

function TAsignaEmpleados.BuscaNodoTexto( oArbol : TTreeView; sCaption, sTexto : String ) : Boolean;
var
   oNodo: TTreeNode;
   sDescrip : String;

begin  { BuscaNodoTexto }
    Result := FALSE;

    TreeSup.HideSelection:=false;
    with oArbol do
    begin
         oArbol.Items[0].Expand(false);
         if ( Items.Count <= 0 ) then Exit;

         sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( sTexto ));
         if ( Selected = NIL ) or ( sTexto <> FUltimoTexto ) then
         begin
            oNodo := Items[ 0 ];
            BuscaNodoReset;
            FUltimoTexto := sTexto;
         end
         else
            oNodo := Selected.GetNextVisible;

         HideSelection := False;
         while ( oNodo <> NIL ) do
         begin
            if ( Pos( sTexto, UpperCase( QuitaAcentos( oNodo.Text ))) > 0 ) then
            begin
                 Result := TRUE;
                 Inc( FNodosTexto ); // Para saber cuantos encontr�
                 Selected := oNodo;
                 SetFocus();
                 Exit;
            end;
            oNodo := oNodo.GetNextVisible;
         end;
         HideSelection := True;
         if not Result then
         begin
            if ( FNodosTexto = 0 ) then
                sDescrip := 'ninguna'
            else
                sDescrip := 'Otra';
            ZInformation( sCaption, Format( 'No hay Supervisor que contenga : ''%s'' ' + CR_LF + '< %s >', [ sDescrip, sTexto ] ) , 0 );
            BuscaNodoReset;
         end;
    end;

end;


procedure TAsignaEmpleados.CheckSupervisoresClick(Sender: TObject);
begin
  inherited;
      if( not checksupervisores.Checked) then
      begin
      dmSuper.CdsSupervisoresFiltroActivos(True);
      llenaListaSup;
      end
      else
      dmSuper.CdsSupervisoresFiltroActivos(False);
      llenaListaSup;
      begin
  end;
end;

procedure TAsignaEmpleados.EditBusca2KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if  ((key = #13)) then
      begin
      PaSupBtn.Click;
      Key := #0;
      end;
  end;

procedure TAsignaEmpleados.BuscaShortcutExecute(Sender: TObject);
begin
  inherited;

  if((treesup.Focused  ) or (editbuscaold.Focused)) then BuscaArbol;
end;

procedure TAsignaEmpleados.TreeSupKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
     if  ((key = #13) or (key = #72)) then
      begin
      if  (key = #13) then brincaenter:=true;
      BuscaArbol;
      Key := #0;
      end;
end;

procedure TAsignaEmpleados.cxBarEditItem5PropertiesChange(Sender: TObject);
begin
  inherited;
     TcxTextEdit(Sender).PostEditValue;
end;

procedure TAsignaEmpleados.EditBuscaPropertiesChange(Sender: TObject);
begin
  inherited;
  TcxTextEdit(Sender).PostEditValue;
end;

procedure TAsignaEmpleados.BusquedaActionExecute(Sender: TObject);
begin
  inherited;
   if((treesup.Focused) or (editbuscaold.Focused)) then
   begin
   BuscaArbol;
   end;
end;

procedure TAsignaEmpleados.EditBuscaPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
   TcxTextEdit(Sender).PostEditValue;
end;

procedure TAsignaEmpleados.editBuscaoldKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
if ( Key = 13 ) then
     begin
          Key := 0;
          brincaenter:=true;
          BuscaArbol;
     end;
end;
Procedure TasignaEmpleados.BuscaArbol;
var
    lEncontro: Boolean;
    //Counter: Integer;
    //Arbolito: TTreeView;
    //sTexto : String;
begin
    if (StrLleno(editBuscaold.text)) then
          begin
             lEncontro := BuscaNodoTexto( TreeSup, 'Supervisor', editBuscaold.text) ;
              if(brincaenter) then
              begin
              focuscontrol(CheckSupervisores);
              end
              else
              begin
              focuscontrol(TreeSup);
              end;
              brincaenter:=false;
              end
          else
              lEncontro := FALSE;
          if not lEncontro and Panel1.Visible then
          begin
               begin
               if(brincaenter) then
              begin
              focuscontrol(edit1);
              end
              else
              begin
              focuscontrol(editbuscaold);
              end;
              brincaenter:=false;
              end
          end;
end;

procedure TAsignaEmpleados.PaEmpKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if(key= #13) then
  PaEmp.AsignaValorEntero( StrToIntDef( Paemp.Text, 0 ) );
end;

end.
