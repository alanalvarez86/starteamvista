unit FRegistroTMuerto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FRegistroEspeciales, Db, StdCtrls, ZetaKeyCombo, ZetaNumero, ZetaHora,
  ComCtrls, Mask, ZetaFecha, ExtCtrls, DBCtrls, Buttons,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013, cxControls,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxPC, ZetaKeyLookup_DevEx,
  cxNavigator, cxDBNavigator, cxButtons, dxBarBuiltInMenu;

type
  TRegistroTMuerto = class(TRegistroEspeciales)
    Panel2: TPanel;
    LblMotivo: TLabel;
    WK_TMUERTO: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure SetControlesVisibles; override;
    procedure SetOtrosDatos( const lMultiLote: Boolean ); override;
    procedure ValidaDatosLabor; override;
  public
    { Public declarations }
  end;

var
  RegistroTMuerto: TRegistroTMuerto;

implementation

uses dLabor, dGlobal, ZGlobalTress, ZetaCommonLists, ZetaCommonTools,ZetaCommonclasses,ZetaLaborTools,
  FRegistroWorks;

{$R *.DFM}

procedure TRegistroTMuerto.FormCreate(Sender: TObject);
begin
     self.Caption:= Format( 'Registro Individual De %s', [ Global.GetGlobalString( K_GLOBAL_LABOR_TMUERTO ) ] );
     inherited;
     HelpContext := 9506;
     WK_TMUERTO.LookupDataset := dmLabor.cdsTiempoMuerto;
end;

procedure TRegistroTMuerto.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef INTERRUPTORES}
      WK_TMUERTO.WidthLlave:= K_WIDTHLLAVE;
      WK_TMUERTO.Width:= K_WIDTH_LOOKUP;
     {$endif}
     { MV: Se les cambia el Tag a UNO para poderlos dejar vacios y no Sean checados como los otros controles }
     WO_NUMBER.Tag := ZetaLaborTools.K_LABOR_NO_VALIDAR_LOOKUP;
     AR_CODIGO.Tag := ZetaLaborTools.K_LABOR_NO_VALIDAR_LOOKUP;
end;

procedure TRegistroTMuerto.Connect;
begin
     inherited;
     dmLabor.cdsTiempoMuerto.Conectar;
end;

procedure TRegistroTMuerto.SetOtrosDatos( const lMultiLote: Boolean );
begin

     with dmLabor.cdsWorks do
     begin
          FieldByName( 'WK_TIPO' ).AsInteger := Ord( wtLibre );
         // FieldByName('WK_PIEZAS').AsFloat := 0;

          if lMultiLote then
          begin
               FieldByName('WK_TMUERTO').AsString := FTipoTMuerto;
               FieldByName('WK_PRE_CAL').AsFloat := FDuracion ;
          end;
     end;
end;

procedure TRegistroTMuerto.SetControlesVisibles;
begin
     inherited;
     PanelOpera.Visible := FALSE;
     LblCantidad.Visible:= FALSE;
     WK_PIEZAS.Visible  := FALSE;
end;

procedure TRegistroTMuerto.ValidaDatosLabor;
begin
     inherited;
     ValidaControlLookup( WK_TMUERTO, LblMotivo );
end;

procedure TRegistroTMuerto.OKClick(Sender: TObject);
begin
     FDuracion := WK_PRE_CAL.ValorEntero;
     inherited;

end;

procedure TRegistroTMuerto.OK_DevExClick(Sender: TObject);
begin
     FDuracion := WK_PRE_CAL.ValorEntero; 
     inherited;
end;

end.


