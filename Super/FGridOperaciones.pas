unit FGridOperaciones;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}
  ExtCtrls, StdCtrls, ZetaKeyCombo, ZetaSmartLists,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TGridOperaciones = class(TBaseGridEdicion_DevEx)
    zCombo: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
              DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    function EsCampoModulador(sCampo: String): Boolean;
    procedure BuscaModulador(const sCampo: String);
    procedure BuscaOperacion;
    procedure BuscaOrden;
    procedure BuscaArea;
    procedure BuscaParte;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    function ValoresGrid: Variant;override;
  public
    { Public declarations }
  end;

var
  GridOperaciones: TGridOperaciones;

implementation

uses DLabor, DSuper, DGlobal, ZetaClientDataSet, ZetaLaborTools, ZetaCommonClasses,
     ZGlobalTress, ZetaCommonLists, DCliente;

{$R *.DFM}

procedure TGridOperaciones.FormCreate(Sender: TObject);
begin
     self.Caption:= Global.GetGlobalString( K_GLOBAL_LABOR_OPERACIONES );
     with ZetaDBGrid do
     begin
          AsignaGlobalColumn( K_GLOBAL_LABOR_PIEZAS, Columns[2] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_ORDEN, Columns[3] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_PARTE, Columns[4] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_AREA, Columns[5] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_OPERACION, Columns[6] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_MODULA1, Columns[8] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_MODULA2, Columns[9] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_MODULA3, Columns[10] );
     end;
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     HelpContext := H9502_Operaciones_del_empleado;
end;

procedure TGridOperaciones.Connect;
begin
     with dmLabor do
     begin
          with ZetaDBGrid do
          begin
               if ( Columns[3].Visible ) or ( Columns[4].Visible ) then
                  cdsPartes.Conectar;
               if ( Columns[5].Visible ) then
                  cdsOpera.Conectar;
               if ( Columns[6].Visible ) then
                  cdsArea.Conectar;
               if ( Columns[8].Visible ) then
                  cdsModula1.Conectar;
               if ( Columns[9].Visible ) then
                  cdsModula2.Conectar;
               if ( Columns[10].Visible ) then
                  cdsModula3.Conectar;
          end;
          cdsWorks.Conectar;
          DataSource.DataSet := cdsWorks;
     end;
end;

procedure TGridOperaciones.Buscar;
var
   sCampo : String;
begin
     sCampo := ZetaDBGrid.SelectedField.FieldName;
     if ( sCampo = 'WO_NUMBER' ) then
        BuscaOrden
     else if ( sCampo = 'AR_CODIGO' ) then
        BuscaParte
     else if ( sCampo = 'OP_NUMBER' ) then
        BuscaOperacion
     else if ( sCampo = 'CB_AREA' ) then
        BuscaArea
     else if ( EsCampoModulador( sCampo ) ) then
        BuscaModulador( sCampo );
end;

procedure TGridOperaciones.BuscaOrden;
var
   sOrden, sOrdenDesc: String;
begin
     with dmLabor.cdsWorks do
     begin
          sOrden := FieldByName( 'WO_NUMBER' ).AsString;
          if ( dmLabor.cdsWOrderLookup.Search_DevEx( '', sOrden, sOrdenDesc ) ) then
          begin
               if ( sOrden <> FieldByName( 'WO_NUMBER' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName('WO_NUMBER').AsString := sOrden;
               end;
          end;
     end;
end;

procedure TGridOperaciones.BuscaParte;
var
   sParte, sParteDesc: String;
begin
     with dmLabor.cdsWorks do
     begin
          sParte := FieldByName( 'AR_CODIGO' ).AsString;
          if ( dmLabor.cdsPartes.Search_DevEx( '', sParte, sParteDesc ) ) then
          begin
               if ( sParte <> FieldByName( 'AR_CODIGO' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName('AR_CODIGO').AsString := sParte;
               end;
          end;
     end;
end;

procedure TGridOperaciones.BuscaOperacion;
var
   sOpera, sOperaDesc: String;
begin
     with dmLabor.cdsWorks do
     begin
          sOpera := FieldByName( 'OP_NUMBER' ).AsString;
          if ( dmLabor.cdsOpera.Search_DevEx( '', sOpera, sOperaDesc ) ) then
          begin
               if ( sOpera <> FieldByName( 'OP_NUMBER' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName('OP_NUMBER').AsString := sOpera;
               end;
          end;
     end;
end;

procedure TGridOperaciones.BuscaArea;
var
   sArea, sAreaDesc: String;
begin
     with dmLabor.cdsWorks do
     begin
          sArea := FieldByName( 'CB_AREA' ).AsString;
          if ( dmLabor.cdsArea.Search_DevEx( '', sArea, sAreaDesc ) ) then
          begin
               if ( sArea <> FieldByName( 'CB_AREA' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName('CB_AREA').AsString := sArea;
               end;
          end;
     end;
end;

procedure TGridOperaciones.BuscaModulador( const sCampo: String );
var
   sModulador, sModuladorDesc: String;

begin
     with dmLabor.cdsWorks do
     begin
          sModulador := FieldByName( sCampo ).AsString;
          if TZetaLookupDataSet( dmLabor.GetcdsLookup( sCampo ) ).Search_DevEx( '', sModulador, sModuladorDesc ) then
          begin
               if ( sModulador <> FieldByName( sCampo ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( sCampo ).AsString := sModulador;
               end;
          end;
     end;
end;

function TGridOperaciones.EsCampoModulador( sCampo: String ): Boolean;
begin
     Result:= False;
     if ( sCampo = 'WK_MOD_1' ) or
        ( sCampo = 'WK_MOD_2' ) or
        ( sCampo = 'WK_MOD_3' ) then
        Result:= True;
end;

procedure TGridOperaciones.KeyPress(var Key: Char);
begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'WK_STATUS' ) then
               begin
                    zCombo.SetFocus;
                    SendMessage( zCombo.Handle, WM_Char, Word( Key ), 0 );
                    Key := #0;
               end;
          end;
     end
     else
         if ( ActiveControl = zCombo ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   SelectedField := dmLabor.cdsWorks.FieldByName( 'WK_MOD_1' );
              end;
         end;
end;

procedure TGridOperaciones.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'WK_STATUS' ) and
             ( zCombo.Visible ) then
             zCombo.Visible:= False;
     end;
end;

procedure TGridOperaciones.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
          DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'WK_STATUS' ) then
          begin
               with zCombo do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end;
     end;
     inherited;
end;

function TGridOperaciones.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

end.
