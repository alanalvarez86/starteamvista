inherited Kardex: TKardex
  Caption = 'Kardex'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    Hint = 'Lista de Eventos de Kardex'
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopupMenu = nil
      DataController.DataSource = DataSource
      object CB_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'CB_FECHA'
        MinWidth = 75
      end
      object CB_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CB_TIPO'
        MinWidth = 75
      end
      object CB_COMENTA: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CB_COMENTA'
        MinWidth = 85
      end
      object CB_MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'CB_MONTO'
        MinWidth = 65
      end
      object CB_PUESTO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'CB_PUESTO'
        MinWidth = 70
      end
      object CB_TURNO: TcxGridDBColumn
        Caption = 'Turno'
        DataBinding.FieldName = 'CB_TURNO'
        MinWidth = 65
      end
      object CB_CLASIFI: TcxGridDBColumn
        Caption = 'Clasific.'
        DataBinding.FieldName = 'CB_CLASIFI'
        MinWidth = 75
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 400
    Top = 0
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
