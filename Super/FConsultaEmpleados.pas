unit FConsultaEmpleados;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaSmartLists, StdCtrls, ZetaStateComboBox, Db, ImageEnView, Buttons,
  DBCtrls, ExtCtrls, ZetaDBTextBox, Mask, ComCtrls, Grids, DBGrids,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxPC, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxCalendar, ieview,
  dxBarBuiltInMenu;

type
  TConsultaEmpleados = class(TForm)
    PageControl1: TcxPageControl;
    TabGenerales: TcxTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    CB_APE_PAT: TDBEdit;
    CB_APE_MAT: TDBEdit;
    CB_NOMBRES: TDBEdit;
    TU_DESCRIP: TDBEdit;
    PU_DESCRIP: TDBEdit;
    TB_ELEMENT: TDBEdit;
    CONTRATO: TDBEdit;
    CB_FEC_ING: TDBEdit;
    TabNiveles: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL5: TZetaDBTextBox;
    CB_NIVEL4: TZetaDBTextBox;
    CB_NIVEL3: TZetaDBTextBox;
    CB_NIVEL2: TZetaDBTextBox;
    CB_NIVEL1: TZetaDBTextBox;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL6: TZetaDBTextBox;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL7: TZetaDBTextBox;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL8: TZetaDBTextBox;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL9: TZetaDBTextBox;
    Panel1: TPanel;
    PRETTYNAME: TDBText;
    PaEmpLbl: TLabel;
    PanelBotones: TPanel;
    PanelEmpleadoActivo: TPanel;
    DataSource: TDataSource;
    Empleado: TStateComboBox;
    ZNIVEL1: TZetaTextBox;
    ZNIVEL2: TZetaTextBox;
    ZNIVEL3: TZetaTextBox;
    ZNIVEL4: TZetaTextBox;
    ZNIVEL5: TZetaTextBox;
    ZNIVEL6: TZetaTextBox;
    ZNIVEL7: TZetaTextBox;
    ZNIVEL8: TZetaTextBox;
    ZNIVEL9: TZetaTextBox;
    CB_CODIGO: TDBEdit;
    Area_Lbl: TLabel;
    CB_AREA: TDBEdit;
    TabKardex: TcxTabSheet;
    DSKarArea: TDataSource;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBTextBox;
    ZNIVEL10: TZetaTextBox;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaDBTextBox;
    ZNIVEL11: TZetaTextBox;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBTextBox;
    ZNIVEL12: TZetaTextBox;
    Certificaciones: TcxTabSheet;
    dsKarCertific: TDataSource;
    EmpleadoBtn_DevEx: TcxButton;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1DBTableView2: TcxGridDBTableView;
    ZetaCXGrid1DBTableView2KI_FEC_CER: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2CI_CODIGO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2CI_NOMBRE: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2KI_APROBO: TcxGridDBColumn;
    ZetaCXGrid1DBTableView2VENCIMIENTO: TcxGridDBColumn;
    ZetaCXGrid2DBTableView1: TcxGridDBTableView;
    ZetaCXGrid2: TZetaCXGrid;
    ZetaCXGrid2Level1: TcxGridLevel;
    ZetaCXGrid2DBTableView2: TcxGridDBTableView;
    ZetaCXGrid2DBTableView2KA_FECHA: TcxGridDBColumn;
    ZetaCXGrid2DBTableView2KA_HORA: TcxGridDBColumn;
    ZetaCXGrid2DBTableView2CB_AREA: TcxGridDBColumn;
    ZetaCXGrid2DBTableView2TB_ELEMENT: TcxGridDBColumn;
    ZetaCXGrid2DBTableView2KA_FEC_MOV: TcxGridDBColumn;
    ZetaCXGrid2DBTableView2KA_HOR_MOV: TcxGridDBColumn;
    ZetaCXGrid2DBTableView2US_CODIGO: TcxGridDBColumn;
    BotonSalir_DevEx: TcxButton;
    bbMostrarCalendario_DevEx: TcxButton;
    ImprimirFormaBtn_DevEx: TcxButton;
    EmpleadoPrimero_DevEx: TcxButton;
    EmpleadoAnterior_DevEx: TcxButton;
    EmpleadoSiguiente_DevEx: TcxButton;
    EmpleadoUltimo_DevEx: TcxButton;
    Panel2: TPanel;
    Panel3: TPanel;
    FOTOGRAFIA: TImageEnView;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EmpleadoPrimeroClick(Sender: TObject);
    procedure EmpleadoAnteriorClick(Sender: TObject);
    procedure EmpleadoSiguienteClick(Sender: TObject);
    procedure EmpleadoUltimo_DevExClick(Sender: TObject);
    procedure ImprimirFormaBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure EmpleadoLookUp(Sender: TObject; var lOk: Boolean);
    procedure FotoSwitchClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure EmpleadoBtn_DevExClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ZetaCXGrid2DBTableView2DataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaCXGrid2DBTableView2ColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ZetaCXGrid1DBTableView2DataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaCXGrid1DBTableView2ColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
  private
    { Private declarations }
    Procedure RefrescarDatasets;
    procedure SetCamposNivel;
    procedure SetPanelEmpleadoActivo(const Activo: Boolean; const Ingreso, Baja: TDateTime);
    procedure ShowFoto(const lEnabled: Boolean);
    procedure SetVerColabora( const lEnabled: Boolean );
    Procedure ApplyMinWidth(DbGrid : TcxGridDBTableView) ;
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$endif}
  protected
  { Protected declarations }
    AColumn: TcxGridDBColumn;

  public
    { Public declarations }
  end;

const
     K_PAGE_GRAL = 0;
     K_PAGE_GRID = 2;
var
  ConsultaEmpleados: TConsultaEmpleados;

implementation

uses dSuper, dGlobal, DTablas, DCatalogos, ZetaClientTools, ZetaCommonTools, FTressShell,
     ZetaCommonClasses, ZetaDialogo, ZetaBuscaEmpleado_DevEx, ZAccesosMgr, ZAccesosTress,
     DLabor, DCliente, ZetaLaborTools, ZGlobalTress, DBaseCliente, FToolsImageEn,
     ZetaClientDataSet, ZetaDBGrid;

{$R *.DFM}

procedure TConsultaEmpleados.FormCreate(Sender: TObject);
begin
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     ZNIVEL10.Visible := True;
     ZNIVEL11.Visible := True;
     ZNIVEL12.Visible := True;
     {$endif}
     HelpContext := 10101;
     SetCamposNivel;
     SetVerColabora( ZAccesosMgr.CheckDerecho( D_SUPER_VER_COLABORA, K_DERECHO_CONSULTA ) );
     Area_lbl.Caption := Format( Area_lbl.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ]);
     TabKardex.Caption := Format( TabKardex.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREAS ) ]);
     ZetaCXGrid2DBTableView2CB_AREA.Caption := Format( ZetaCXGrid2DBTableView2CB_AREA.Caption, [ GetLaborLabel( K_GLOBAL_LABOR_AREA ) ]);

     ZetaCXGrid1DBTableView1.DataController.DataModeController.GridMode:= True;
     ZetaCXGrid1DBTableView2.DataController.DataModeController.GridMode:= True;
     ZetaCXGrid2DBTableView1.DataController.DataModeController.GridMode:= True;
     ZetaCXGrid2DBTableView2.DataController.DataModeController.GridMode:= True;

end;

procedure TConsultaEmpleados.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
       case key of
            VK_ESCAPE: ModalResult := mrCancel;
       end;
end;

procedure TConsultaEmpleados.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with dmTablas do
             begin
                  if ZNIVEL1.Visible then cdsNivel1.Conectar;
                  if ZNIVEL2.Visible then cdsNivel2.Conectar;
                  if ZNIVEL3.Visible then cdsNivel3.Conectar;
                  if ZNIVEL4.Visible then cdsNivel4.Conectar;
                  if ZNIVEL5.Visible then cdsNivel5.Conectar;
                  if ZNIVEL6.Visible then cdsNivel6.Conectar;
                  if ZNIVEL7.Visible then cdsNivel7.Conectar;
                  if ZNIVEL8.Visible then cdsNivel8.Conectar;
                  if ZNIVEL9.Visible then cdsNivel9.Conectar;
                  {$ifdef ACS}
                  if ZNIVEL10.Visible then cdsNivel10.Conectar;{ACS}
                  if ZNIVEL11.Visible then cdsNivel11.Conectar;{ACS}
                  if ZNIVEL12.Visible then cdsNivel12.Conectar;{ACS}
                  {$endif}
             end;
             with dmCatalogos do
             begin
                  cdsTurnos.Conectar;
                  cdsPuestos.Conectar;
                  cdsClasifi.Conectar;
                  cdsCertificaciones.Conectar;
             end;
             with dmSuper do
             begin
                  ObtenerFoto := FOTOGRAFIA.Visible;
                  cdsDatosEmpleado.Refrescar;
                  DataSource.DataSet:= cdsDatosEmpleado;
                  cdsCertEmp.Refrescar;
                  dsKarCertific.DataSet := cdsCertEmp;
             end;
             with dmLabor do
             begin
                  cdsKarArea.Refrescar;
                  DsKarArea.DataSet := cdsKarArea;
             end;
             with PageControl1 do
             begin
                  TabKardex.TabVisible := dmSuper.LaborActivado;
                  ActivePageIndex := K_PAGE_GRAL;
             end;
          finally
             Cursor := oCursor;
          end;
     end;
     ApplyMinWidth(ZetaCxGrid2DBTableView2);
     ApplyMinWidth(ZetaCxGrid1DBTableView2);
     ZetaCxGrid2DBTableView2.ApplyBestFit();
     ZetaCxGrid1DBTableView2.ApplyBestFit();

     ZetaCXGrid1DBTableView2.OptionsCustomize.ColumnFiltering := False;
     ZetaCXGrid2DBTableView2.OptionsCustomize.ColumnFiltering := False;
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
    {$endif}

end;

procedure TConsultaEmpleados.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     with dmSuper do
     begin
          ObtenerFoto := FALSE;
     end;
end;

procedure TConsultaEmpleados.EmpleadoPrimeroClick(Sender: TObject);
begin
     with dmSuper do
     begin
          if GetEmpleadoPrimero then
          begin
          dmCliente.SetEmpleadoNumero(  cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger);
             RefrescarDatasets;
             Empleado.ValorEntero := cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          end;
     ZetaCxGrid2DBTableView2.ApplyBestFit();
     ZetaCxGrid1DBTableView2.ApplyBestFit();
end;

procedure TConsultaEmpleados.EmpleadoAnteriorClick(Sender: TObject);
begin
     with dmSuper do
     begin
          if GetEmpleadoAnterior then
          begin
             Empleado.ValorEntero := cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
             dmCliente.SetEmpleadoNumero(  cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger);
             RefrescarDatasets;
          end;
     end;
      ZetaCxGrid2DBTableView2.ApplyBestFit();
     ZetaCxGrid1DBTableView2.ApplyBestFit();
end;

procedure TConsultaEmpleados.EmpleadoSiguienteClick(Sender: TObject);
begin
     with dmSuper do
     begin
          if GetEmpleadoSiguiente then
          begin
             Empleado.ValorEntero := cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
             dmCliente.SetEmpleadoNumero(  cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger);
             RefrescarDatasets;
          end;
     end;
      ZetaCxGrid2DBTableView2.ApplyBestFit();
     ZetaCxGrid1DBTableView2.ApplyBestFit();
end;

procedure TConsultaEmpleados.EmpleadoUltimo_DevExClick(Sender: TObject);
begin
     with dmSuper do
     begin
          if GetEmpleadoUltimo then
          begin

             RefrescarDatasets;
             Empleado.ValorEntero := cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;

          end;
     end;
      ZetaCxGrid2DBTableView2.ApplyBestFit();
     ZetaCxGrid1DBTableView2.ApplyBestFit();
end;

procedure TConsultaEmpleados.EmpleadoLookUp(Sender: TObject; var lOk: Boolean);
begin
     if not dmSuper.SetEmpleadoNumero( Empleado.ValorEntero ) then
        zInformation( 'Error', '! Empleado No Encontrado !', 0 );
end;

procedure TConsultaEmpleados.ImprimirFormaBtnClick(Sender: TObject);
begin
     if zConfirm( 'Imprimir...', '� Desea Imprimir la Pantalla: ' + self.Caption + ' ?', 0, mbYes ) then
     begin
          CB_CODIGO.Visible := TRUE;
          Print;
          CB_CODIGO.Visible := FALSE;
     end;
end;

procedure TConsultaEmpleados.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     with dmSuper.cdsDatosEmpleado do
     begin
          Empleado.ValorEntero := FieldByName( 'CB_CODIGO' ).AsInteger;
          if ZNIVEL1.Visible then ZNIVEL1.Caption:= dmTablas.cdsNivel1.GetDescripcion( FieldByName( 'CB_NIVEL1' ).AsString );
          if ZNIVEL2.Visible then ZNIVEL2.Caption:= dmTablas.cdsNivel2.GetDescripcion( FieldByName( 'CB_NIVEL2' ).AsString );
          if ZNIVEL3.Visible then ZNIVEL3.Caption:= dmTablas.cdsNivel3.GetDescripcion( FieldByName( 'CB_NIVEL3' ).AsString );
          if ZNIVEL4.Visible then ZNIVEL4.Caption:= dmTablas.cdsNivel4.GetDescripcion( FieldByName( 'CB_NIVEL4' ).AsString );
          if ZNIVEL5.Visible then ZNIVEL5.Caption:= dmTablas.cdsNivel5.GetDescripcion( FieldByName( 'CB_NIVEL5' ).AsString );
          if ZNIVEL6.Visible then ZNIVEL6.Caption:= dmTablas.cdsNivel6.GetDescripcion( FieldByName( 'CB_NIVEL6' ).AsString );
          if ZNIVEL7.Visible then ZNIVEL7.Caption:= dmTablas.cdsNivel7.GetDescripcion( FieldByName( 'CB_NIVEL7' ).AsString );
          if ZNIVEL8.Visible then ZNIVEL8.Caption:= dmTablas.cdsNivel8.GetDescripcion( FieldByName( 'CB_NIVEL8' ).AsString );
          if ZNIVEL9.Visible then ZNIVEL9.Caption:= dmTablas.cdsNivel9.GetDescripcion( FieldByName( 'CB_NIVEL9' ).AsString );
          {$ifdef ACS}
          if ZNIVEL10.Visible then ZNIVEL10.Caption:= dmTablas.cdsNivel10.GetDescripcion( FieldByName( 'CB_NIVEL10' ).AsString );{ACS}
          if ZNIVEL11.Visible then ZNIVEL11.Caption:= dmTablas.cdsNivel11.GetDescripcion( FieldByName( 'CB_NIVEL11' ).AsString );{ACS}
          if ZNIVEL12.Visible then ZNIVEL12.Caption:= dmTablas.cdsNivel12.GetDescripcion( FieldByName( 'CB_NIVEL12' ).AsString );{ACS}
          {$endif}
          SetPanelEmpleadoActivo( zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ),
                                  FieldByName( 'CB_FEC_ING' ).AsDateTime,
                                  FieldByName( 'CB_FEC_BAJ' ).AsDateTime )
     end;
          if ( PageControl1.ActivePageIndex = K_PAGE_GRID ) then
             dmLabor.cdsKarArea.Refrescar;
          with dmSuper.cdsDatosEmpleado do
               bbMostrarCalendario_DevEx.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'TU_DESCRIP' ).AsString );

          if ( Field = nil ) then
          begin
               if (dmSuper.cdsDatosEmpleado.FieldByName('FOTOGRAFIA').IsBlob ) then
                  FToolsImageEn.AsignaBlobAImagen( FOTOGRAFIA, dmSuper.cdsDatosEmpleado, 'FOTOGRAFIA' )
               else
                   FOTOGRAFIA.Blank;
          end;

end;

procedure TConsultaEmpleados.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1, ZNIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2, ZNIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3, ZNIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4, ZNIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5, ZNIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6, ZNIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7, ZNIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8, ZNIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9, ZNIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10, ZNIVEL10 );{ACS}
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11, ZNIVEL11 );{ACS}
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12, ZNIVEL12 );{ACS}
     {$endif}
end;

procedure TConsultaEmpleados.SetPanelEmpleadoActivo( const Activo: Boolean; const Ingreso, Baja : TDateTime );
var
   oInfoEmpleado : TInfoEmpleado;
   oColor : TColor;
   sCaption, sHint : String;
begin
     oInfoEmpleado.Activo := Activo;
     oInfoEmpleado.Baja := Baja;
     oInfoEmpleado.Ingreso := Ingreso;
     oInfoEmpleado.Recontratable := True; //no mostrar hint de no recontratable
     with PanelEmpleadoActivo do
     begin
          dmCliente.GetStatusEmpleado(oInfoEmpleado, sCaption, sHint, oColor);
          Font.Color := oColor;
          Caption := sCaption;
          Hint := sHint;
     end;
end;

procedure TConsultaEmpleados.FotoSwitchClick(Sender: TObject);
begin
     inherited;
     with FOTOGRAFIA do
     begin
          Visible := true;
          ShowFoto( Visible );
     end;
end;

procedure TConsultaEmpleados.ShowFoto( const lEnabled: Boolean );
var
   lFotoActivada: Boolean;
begin
     with dmSuper do
     begin
          lFotoActivada := ObtenerFoto;
          ObtenerFoto := lEnabled;
          if lEnabled and not lFotoActivada then
             SetEmpleadoNumero( Empleado.ValorEntero, TRUE );  // Para que se traiga la Foto del Empleado
     end;

end;

procedure TConsultaEmpleados.ZetaCXGrid1DBTableView2ColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TConsultaEmpleados.ZetaCXGrid1DBTableView2DataControllerSortingChanged(
  Sender: TObject);
begin
     ZetaCXGrid1.OrdenarPor( AColumn , TZetaClientDataset(dsKarCertific.DataSet) );
end;

procedure TConsultaEmpleados.ZetaCXGrid2DBTableView2ColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TConsultaEmpleados.ZetaCXGrid2DBTableView2DataControllerSortingChanged(
  Sender: TObject);
begin
     ZetaCXGrid2.OrdenarPor( AColumn , TZetaClientDataset(DSKarArea.DataSet) );
end;

procedure TConsultaEmpleados.SetVerColabora(const lEnabled: Boolean);
begin
     PaEmpLbl.Enabled := lEnabled;
     Empleado.Enabled := lEnabled;
     EmpleadoBtn_DevEx.Enabled := lEnabled;
     EmpleadoPrimero_DevEx.Enabled := lEnabled;
     EmpleadoAnterior_DevEx.Enabled := lEnabled;
     EmpleadoSiguiente_DevEx.Enabled := lEnabled;
     EmpleadoUltimo_DevEx.Enabled := lEnabled;
     PRETTYNAME.Enabled := lEnabled;
     PanelEmpleadoActivo.Visible := ZAccesosMgr.CheckDerecho( D_SUPER_STATUS_EMPLEADO, K_DERECHO_CONSULTA )
end;

procedure TConsultaEmpleados.PageControl1Change(Sender: TObject);
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             if PageControl1.ActivePageIndex = K_PAGE_GRID then
             begin
                  with dmLabor.cdsKarArea do
                  begin
                       if ( not Active ) or IsEmpty or ( FieldByName('CB_CODIGO').AsInteger <> dmSuper.cdsDatosEmpleado.FieldByName('CB_CODIGO').AsInteger ) then
                            Refrescar;
                  end;
             end;
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TConsultaEmpleados.bbMostrarCalendarioClick(Sender: TObject);
begin
     dmCatalogos.ShowCalendarioRitmo( dmSuper.cdsDatosEmpleado.FieldByName( 'CB_TURNO' ).AsString );
end;

procedure TConsultaEmpleados.EmpleadoBtn_DevExClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        Empleado.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;


procedure TConsultaEmpleados.RefrescarDatasets;
begin
   dmsuper.GetCertEmpLocalFromCdsDatosEmp := true;
  dmCliente.SetEmpleadoNumero(  dmSuper.cdsDatosEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger);
  dmSuper.CdsCertEmp.setDataChange;
 //dmsuper.NotifyDataChange(dmcliente.cdsEmpleadoLookUp);
 dmSuper.cdsCertEmp.Refrescar;
 //dmlabor.NotifyDataChange(dmcliente.cdsEmpleadoLookUp);
 dsKarCertific.DataSet := dmSuper.cdsCertEmp;
 dmLabor.cdsKarArea.Refrescar;
 dmsuper.GetCertEmpLocalFromCdsDatosEmp := false;
end;

procedure TConsultaEmpleados.ApplyMinWidth(DbGrid : TcxGridDBTableView) ;
var
   i: Integer;
const
     widthColumnOptions =30;
begin
     with  DbGrid do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TConsultaEmpleados.CambiosVisuales;
var
  I : Integer;
begin
     for I := 0 to TabNiveles.ControlCount - 1 do
     begin
          if TabNiveles.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabNiveles.Controls[I].Width+2);
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaDBTextBox' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    TabNiveles.Controls[I].Width := K_WIDTHLLAVE;
                    //TZetaDbKeyLookup_DevEx( TabNiveles.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT;
               end;
          end;
          if TabNiveles.Controls[I].ClassNameIs( 'TZetaTextBox' ) then
          begin
               if ( TabNiveles.Controls[I].Visible ) then
               begin
                    TabNiveles.Controls[I].Width := K_WIDTH_LOOKUP-200;
                    //TZetaDbKeyLookup_DevEx( TabNiveles.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabNiveles.Controls[I].Left := K_WIDTH_SMALL_LEFT + K_WIDTHLLAVE + 2;
               end;
          end;


     end;
end;
{$endif}


end.
