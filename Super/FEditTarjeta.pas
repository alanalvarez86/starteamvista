unit FEditTarjeta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, StdCtrls, Mask, ZetaNumero, ZetaKeyCombo, Db, Grids,
  DBGrids, ZetaDBGrid, ComCtrls, ExtCtrls, DBCtrls, Buttons,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}  
  ZetaMessages, ZetaSmartLists, ImgList, ActnList, ZetaKeyLookup_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, cxNavigator,
  cxDBNavigator, cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC,
  System.Actions, dxBarBuiltInMenu;
type
  TEditTarjeta = class(TBaseEdicionRenglon_DevEx)
    LblTipoDia: TLabel;
    AU_STATUS: TZetaDBKeyCombo;
    LblHorario: TLabel;
    Label1: TLabel;
    HRS_EXTRAS: TZetaDBNumero;
    Label2: TLabel;
    DESCANSO: TZetaDBNumero;
    Label3: TLabel;
    PER_CG: TZetaDBNumero;
    Label4: TLabel;
    PER_CG_ENT: TZetaDBNumero;
    Label5: TLabel;
    PER_SG: TZetaDBNumero;
    Label6: TLabel;
    PER_SG_ENT: TZetaDBNumero;
    M_HRS_EXTRAS: TZetaDBKeyLookup_DevEx;
    M_DESCANSO: TZetaDBKeyLookup_DevEx;
    M_PER_CG: TZetaDBKeyLookup_DevEx;
    M_PER_CG_ENT: TZetaDBKeyLookup_DevEx;
    M_PER_SG: TZetaDBKeyLookup_DevEx;
    M_PER_SG_ENT: TZetaDBKeyLookup_DevEx;
    HO_CODIGO: TZetaDBKeyLookup_DevEx;
    lblTomoComida: TLabel;
    AU_OUT2EAT: TZetaDBKeyCombo;
    Label7: TLabel;
    AU_STA_FES: TZetaDBKeyCombo;
    Label8: TLabel;
    PRE_FUERA_JOR: TZetaDBNumero;
    PRE_DENTRO_JOR: TZetaDBNumero;
    Label9: TLabel;
    M_PRE_FUERA_JOR: TZetaDBKeyLookup_DevEx;
    M_PRE_DENTRO_JOR: TZetaDBKeyLookup_DevEx;
    ActionList: TActionList;
    _E_BuscarCodigo: TAction;
    Image: TImageList;
    btnEntrada_DevEx: TcxButton;
    btnSalida_DevEx: TcxButton;
    BuscarBtnCheca_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TarjetaPuntualClick(Sender: TObject);
    procedure _E_BuscarCodigoExecute(Sender: TObject);
    procedure GridRenglonesColEnter(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure AU_STA_FESChange(Sender: TObject);
    procedure SetEditarSoloActivos;
    {procedure dsRenglonDataChange(Sender: TObject; Field: TField);
    {procedure btnEntradaClick(Sender: TObject);
    procedure btnSalidaClick(Sender: TObject);  }
  private
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure ControlesActivos(oControl: TZetaDBNumero; oDBLookup: TZetaDBKeyLookup_DevEx; lEnabled: Boolean);
    procedure Derechos_Autorizaciones(iDerechos: Integer);

    procedure SetFirstControl;
    function GridEnfocado: Boolean;
    procedure SeleccionaSiguienteRenglon;
    function Aprobar_Autorizaciones(const sAprobacion: string): Boolean;
    //function GetFechaTarjeta: TDate;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    function ValoresGrid: Variant;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  EditTarjeta: TEditTarjeta;

implementation

uses DSuper,
     DCatalogos,
     DTablas,
     DGlobal,
     DCliente,
     ZGlobalTress,
     ZAccesosMgr,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaTipoEntidad,
     FTressShell,
     ZetaDialogo,
     FToolsAsistencia;{ACL170409}

{$R *.DFM}

procedure TEditTarjeta.FormCreate(Sender: TObject);
const
     K_FLT_TIPO_MOTIVO = 'TB_TIPO = %d OR TB_TIPO = %d';

function SetFilter (eMotivo:eAutorizaChecadas ):string;
begin
     Result := Format (K_FLT_TIPO_MOTIVO ,[K_MOTIVO_AUTORIZ_OFFSET,Ord(eMotivo)]);
end;

begin
     inherited;
     IndexDerechos := D_SUPER_AJUSTAR_ASISTENCIA;
     TipoValorActivo1 := stEmpleado;
     FirstControl := AU_STATUS;
     HelpContext := 00500;
     //Derechos_Autorizaciones( D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL );

     HO_CODIGO.LookupDataset := dmCatalogos.cdsHorarios;
     M_HRS_EXTRAS.LookupDataset := dmTablas.cdsMotAuto;
     M_DESCANSO.LookupDataset := dmTablas.cdsMotAuto;
     M_PER_CG.LookupDataset := dmTablas.cdsMotAuto;
     M_PER_CG_ENT.LookupDataset := dmTablas.cdsMotAuto;
     M_PER_SG.LookupDataset := dmTablas.cdsMotAuto;
     M_PER_SG_ENT.LookupDataset := dmTablas.cdsMotAuto;
     M_PRE_FUERA_JOR.LookupDataSet := dmTablas.cdsMotAuto;
     M_PRE_DENTRO_JOR.LookupDataSet := dmTablas.cdsMotAuto;

     M_HRS_EXTRAS.Filtro := SetFilter(acExtras);
     M_DESCANSO.Filtro := SetFilter(acDescanso);
     M_PER_CG.Filtro := SetFilter(acConGoce);
     M_PER_CG_ENT.Filtro := SetFilter(acConGoceEntrada);
     M_PER_SG.Filtro := SetFilter(acSinGoce);
     M_PER_SG_ENT.Filtro := SetFilter(acSinGoceEntrada);
     M_PRE_FUERA_JOR.Filtro := SetFilter(acPrepagFueraJornada );
     M_PRE_DENTRO_JOR.Filtro := SetFilter(acPrepagDentroJornada );
     btnEntrada_DevEx.Tag := 0;
     btnSalida_DevEx.Tag := 1;
     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;



procedure TEditTarjeta.FormShow(Sender: TObject);
const
     COL_MOTIVO = 3;    
begin
     Derechos_Autorizaciones( D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL );

     SetFirstControl;
     inherited;
     PageControl.ActivePage := Tabla;
     DGlobal.SetDescuentoComida( lblTomoComida, AU_OUT2EAT );
     GridRenglones.Columns[COL_MOTIVO].Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;{ACL170409}
     BuscarBtnCheca_DevEx.Visible:= FToolsAsistencia.PermitirCapturaMotivoCH;{ACL13059}
end;

procedure TEditTarjeta.AU_STA_FESChange(Sender: TObject);
var
   sChecada: string;
   sTU_HOR_FES : String;
   sHorario: String;
   Valor: Variant;
   sHorarioAutomatico: String;
begin
     inherited;
     sHorario := HO_CODIGO.Llave;
     sHorarioAutomatico := dmSuper.GetStatusHorario(dmSuper.cdsTarjeta.FieldByName( 'CB_TURNO' ).AsString, dmCliente.FechaSupervisor).Horario;

     dmCatalogos.cdsTurnos.Locate('TU_CODIGO', dmSuper.cdsTarjeta.FieldByName( 'CB_TURNO' ).AsString,[]);
     sTU_HOR_FES := dmCatalogos.cdsTurnos.FieldByName( 'TU_HOR_FES' ).AsString;

     if {NOT AU_HOR_MAN.Checked and }( sTU_HOR_FES <> '' ) then
     begin
          if (eStatusFestivo(AU_STA_FES.ItemIndex) = esfFestivo ) and (sTU_HOR_FES <> HO_CODIGO.Llave) then   //FESTIVO
          begin
               if ZetaDialogo.ZConfirm(Caption, '�Asignar el horario de festivo trabajado definido en el turno?', 0, mbYes ) then
               begin
                    // Asignar el horario de festivo trabajado definido en el turno
                    sHorario := sTU_HOR_FES;
               end
          end
          else if (eStatusFestivo(AU_STA_FES.ItemIndex) = esfAutomatico ) and (sHorarioAutomatico <> HO_CODIGO.Llave) then   //AUTOMATICO
          begin
               if (ZetaDialogo.ZConfirm(Caption, '�Asignar el horario determinado por el sistema para este d�a?', 0, mbYes )) then
               begin
                    sHorario := sHorarioAutomatico;
               end
          end
          else if (eStatusFestivo(AU_STA_FES.ItemIndex) = esfFestivoTransferido ) and (sTU_HOR_FES = HO_CODIGO.Llave) then //NO ES FESTIVO
          begin
               if ZetaDialogo.ZConfirm(Caption, '�Asignar el horario ordinario para este d�a?', 0, mbYes ) then
               begin
                    // Asignar el horario ordinario definido en el turno
                    sHorario := dmSuper.GetHorarioTurno(dmSuper.GetEmpleadoNumero, dmCliente.FechaSupervisor, ord (esfFestivoTransferido));
               end
          end
     end;

     HO_CODIGO.Llave := sHorario;
     dmSuper.CambiaComboFestivo := TRUE;
end;

procedure TEditTarjeta.Connect;
begin
     with dmCatalogos do
     begin
         cdsHorarios.Conectar;
         cdsTurnos.Conectar;
         cdsPuestos.Conectar;
         cdsClasifi.Conectar;
     end;
     with dmTablas do
     begin
          cdsIncidencias.Conectar;
          cdsMotAuto.Conectar;
          cdsMotCheca.Conectar; {ACL170409}
     end;
     with dmSuper do
     begin
          cdsTarjeta.Conectar;
          DataSource.DataSet:= cdsTarjeta;
          with cdsChecadas do
               if Active and ( not IsEmpty ) then
                  First;
          dsRenglon.DataSet := cdsChecadas;
     end;
     PageControl.ActivePage := Tabla;
end;

procedure TEditTarjeta.Derechos_Autorizaciones( iDerechos: Integer );
begin
     ControlesActivos( HRS_EXTRAS, M_HRS_EXTRAS, ZAccesosMgr.CheckDerecho( iDerechos, HORAS_EXTRAS ) AND
                                                 Aprobar_Autorizaciones( 'OK_HRS_EXTRAS') );

     ControlesActivos( DESCANSO, M_DESCANSO, ZAccesosMgr.CheckDerecho( iDerechos, DESCANSO_TRABAJADO ) AND
                                             Aprobar_Autorizaciones( 'OK_DESCANSO') );

     ControlesActivos( PER_CG, M_PER_CG, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_CON_GOCE ) AND
                                         Aprobar_Autorizaciones('OK_PER_CG') );

     ControlesActivos( PER_CG_ENT, M_PER_CG_ENT, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_CON_GOCE ) AND
                                                 Aprobar_Autorizaciones('OK_PER_CG_ENT') );

     ControlesActivos( PER_SG, M_PER_SG, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_SIN_GOCE ) AND
                                         Aprobar_Autorizaciones( 'OK_PER_SG') );

     ControlesActivos( PER_SG_ENT, M_PER_SG_ENT, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_SIN_GOCE ) AND
                                                 Aprobar_Autorizaciones('OK_PER_SG_ENT') );

     ControlesActivos( PRE_FUERA_JOR, M_PRE_FUERA_JOR, ZAccesosMgr.CheckDerecho( iDerechos, PER_PREPAG_FUERADENTROJOR ) AND
                                                 Aprobar_Autorizaciones('OK_PRE_FUERA_JOR') );

     ControlesActivos( PRE_DENTRO_JOR, M_PRE_DENTRO_JOR, ZAccesosMgr.CheckDerecho( iDerechos, PER_PREPAG_FUERADENTROJOR ) AND
                                                 Aprobar_Autorizaciones('OK_PRE_DENTRO_JOR') );


     AU_STATUS.Enabled := ZAccesosMgr.CheckDerecho( iDerechos, DERECHO_TIPO_DIA );
     LblTipoDia.Enabled := AU_STATUS.Enabled;
     HO_CODIGO.Enabled := ZAccesosMgr.CheckDerecho( iDerechos, DERECHO_HORARIO );
     LblHorario.Enabled := HO_CODIGO.Enabled;

     AU_OUT2EAT.Enabled := ZAccesosMgr.CheckDerecho( iDerechos, DESCUENTO_COMIDAS );;
     lblTomoComida.Enabled := AU_OUT2EAT.Enabled;

end;

procedure TEditTarjeta.ControlesActivos( oControl : TZetaDBNumero; oDBLookup: TZetaDBKeyLookup_DevEx; lEnabled : Boolean );
begin
     oDBLookup.Enabled:= lEnabled;
     oControl.Enabled := lEnabled;
     if lEnabled then
        oControl.Color := clWindow
     else
        oControl.Color := clBtnFace;
end;

procedure TEditTarjeta.SetFirstControl;
begin
     if ( AU_STATUS.Enabled ) then
        FirstControl := AU_STATUS
     else if ( HO_CODIGO.Enabled ) then
        FirstControl := HO_CODIGO
     else
     begin
          FirstControl := GridRenglones;
          PageControl.ActivePage := Tabla;     // Para que funcione el Focus en onShow de ZBaseEdici�n
     end;
end;

procedure TEditTarjeta.GridRenglonesEnter(Sender: TObject);
begin
     inherited;
     //GridRenglones.SelectedIndex := 0; {ACL200409 No es necesario ya que al cambiar de renglon se posiciona sobre la primer columna. Si se deja se realiza dos veces el onvalide del Motivo }
end;

procedure TEditTarjeta.WMExaminar(var Message: TMessage);
begin
     if GridEnfocado and ( TExaminador(  Message.LParam ) = exEnter ) then
        SeleccionaSiguienteRenglon;
end;

procedure TEditTarjeta.SeleccionaSiguienteRenglon;
begin
     with dmSuper.cdsChecadas do
     begin
          Next;
          if EOF then
             Agregar;
     end;
end;

function TEditTarjeta.GridEnfocado: Boolean;
begin
     Result := ( ActiveControl = GridRenglones );
end;

function TEditTarjeta.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

function TEditTarjeta.Aprobar_Autorizaciones(const sAprobacion: string): Boolean;
begin
     with dmSuper do
          Result := AprobarAut_Derecho( cdsTarjeta, sAprobacion );
end;

{function TEditTarjeta.GetFechaTarjeta: TDate;
begin
     if ( DataSource.DataSet <> NIL ) then
        Result := DataSource.DataSet.FieldByName('AU_FECHA').AsDateTime
     else
         Result := NullDateTime;
end;}

function TEditTarjeta.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjeta( DataSource.DataSet, sMensaje );
     end;
end;

function TEditTarjeta.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjeta( DataSource.DataSet, sMensaje );
     end;
end;


procedure TEditTarjeta.TarjetaPuntualClick(Sender: TObject);
var
   sChecada, sMensaje: string;
begin
     inherited;
     if ( PuedeModificar( sMensaje )  ) then
     begin
          if (dmSuper.GetChecadaPuntual(HO_CODIGO.LLave, TBitBtn(Sender).Tag, sChecada)) then
          begin
               dmSuper.RegistrarTarjetaPuntual(sChecada);
          end
          else
          begin
               Zerror(self.Caption, sChecada, 0);
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, sMensaje, 0 );
     end;
end;

{ACL170409}
procedure TEditTarjeta._E_BuscarCodigoExecute(Sender: TObject);
const
     COL_MOTIVO = 3;
var
   sKey, sDescription: String;
begin
     inherited;
     with GridRenglones do
     begin
          if SelectedIndex = COL_MOTIVO then
          begin
               if dmTablas.cdsMotCheca.Search_DevEx( '', sKey, sDescription ) then
               begin
                    with dmSuper.cdsChecadas do
                    begin
                         if not ( State in [ dsEdit, dsInsert ] ) then
                            Edit;
                         FieldByName( 'CH_MOTIVO' ).AsString := sKey;
                    end;
               end;
          end;
     end;
end;

{ACL210409}
procedure TEditTarjeta.GridRenglonesColEnter(Sender: TObject);
const
     COL_MOTIVO = 3;
begin
     inherited;
     with GridRenglones do
     begin
          if BuscarBtnCheca_DevEx.Visible then
             BuscarBtnCheca_DevEx.Enabled := ( SelectedIndex = COL_MOTIVO );
     end;
end;

procedure TEditTarjeta.OK_DevExClick(Sender: TObject);
begin
  inherited;
     with dmSuper.cdsTarjeta do
     begin
          if ChangeCount = 0 then
          begin
               TressShell.SetDataChange( [ enAusencia ] );
               Self.Close;
          end;
     end;
end;

procedure TEditTarjeta.SetEditarSoloActivos;
begin
     HO_CODIGO.EditarSoloActivos := TRUE;
end;

end.



