inherited HisTools_DevEx: THisTools_DevEx
  Left = 206
  Top = 140
  Caption = 'Historial de Herramienta'
  ClientWidth = 443
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 443
    inherited Slider: TSplitter
      Left = 221
    end
    inherited ValorActivo1: TPanel
      Width = 205
      inherited textoValorActivo1: TLabel
        Width = 199
      end
    end
    inherited ValorActivo2: TPanel
      Left = 224
      Width = 219
      inherited textoValorActivo2: TLabel
        Width = 213
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 443
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object KT_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'KT_FEC_INI'
      end
      object TO_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TO_CODIGO'
      end
      object TO_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TO_DESCRIP'
      end
      object KT_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'KT_REFEREN'
      end
      object KT_TALLA: TcxGridDBColumn
        Caption = 'Talla'
        DataBinding.FieldName = 'KT_TALLA'
      end
      object KT_ACTIVO: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'KT_ACTIVO'
      end
    end
  end
  inherited DataSource: TDataSource
    Top = 208
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
