inherited HisAcumulados_RS_DevEx: THisAcumulados_RS_DevEx
  Left = 189
  Top = 155
  Caption = 'Historial de Acumulados'
  ClientHeight = 654
  ClientWidth = 1069
  ExplicitWidth = 1069
  ExplicitHeight = 654
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1069
    ExplicitWidth = 1069
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 810
      ExplicitWidth = 810
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 804
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Width = 1069
    Height = 597
    ExplicitTop = 57
    ExplicitWidth = 1069
    ExplicitHeight = 597
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CO_NUMERO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'CO_NUMERO'
      end
      object CO_DESCRIP: TcxGridDBColumn
        Caption = 'Concepto'
        DataBinding.FieldName = 'CO_DESCRIP'
      end
      object AC_ANUAL: TcxGridDBColumn
        Caption = 'Anual'
        DataBinding.FieldName = 'AC_ANUAL'
      end
      object AC_MES_01: TcxGridDBColumn
        Caption = 'Enero'
        DataBinding.FieldName = 'AC_MES_01'
      end
      object AC_MES_02: TcxGridDBColumn
        Caption = 'Febrero'
        DataBinding.FieldName = 'AC_MES_02'
      end
      object AC_MES_03: TcxGridDBColumn
        Caption = 'Marzo'
        DataBinding.FieldName = 'AC_MES_03'
      end
      object AC_MES_04: TcxGridDBColumn
        Caption = 'Abril'
        DataBinding.FieldName = 'AC_MES_04'
      end
      object AC_MES_05: TcxGridDBColumn
        Caption = 'Mayo'
        DataBinding.FieldName = 'AC_MES_05'
      end
      object AC_MES_06: TcxGridDBColumn
        Caption = 'Junio'
        DataBinding.FieldName = 'AC_MES_06'
      end
      object AC_MES_07: TcxGridDBColumn
        Caption = 'Julio'
        DataBinding.FieldName = 'AC_MES_07'
      end
      object AC_MES_08: TcxGridDBColumn
        Caption = 'Agosto'
        DataBinding.FieldName = 'AC_MES_08'
      end
      object AC_MES_09: TcxGridDBColumn
        Caption = 'Septiembre'
        DataBinding.FieldName = 'AC_MES_09'
      end
      object AC_MES_10: TcxGridDBColumn
        Caption = 'Octubre'
        DataBinding.FieldName = 'AC_MES_10'
      end
      object AC_MES_11: TcxGridDBColumn
        Caption = 'Noviembre'
        DataBinding.FieldName = 'AC_MES_11'
      end
      object AC_MES_12: TcxGridDBColumn
        Caption = 'Diciembre'
        DataBinding.FieldName = 'AC_MES_12'
      end
      object AC_MES_13: TcxGridDBColumn
        Caption = 'Mes 13'
        DataBinding.FieldName = 'AC_MES_13'
      end
    end
  end
  object pnlRazonSocial: TPanel [2]
    Left = 0
    Top = 19
    Width = 1069
    Height = 38
    Align = alTop
    TabOrder = 2
    object lblRazonSocial: TLabel
      Left = 15
      Top = 13
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Raz'#243'n social:'
    end
    object cbxRazonSocial: TZetaKeyCombo
      Left = 82
      Top = 8
      Width = 289
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      OnChange = cbxRazonSocialChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  inherited DataSource: TDataSource
    Left = 8
    Top = 96
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
