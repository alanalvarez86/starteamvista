inherited EditEmpExperiencia_DevEx: TEditEmpExperiencia_DevEx
  Left = 299
  Top = 198
  Caption = 'Experiencia'
  ClientHeight = 429
  ClientWidth = 652
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 393
    Width = 652
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 484
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 565
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 652
    TabOrder = 3
    inherited ValorActivo2: TPanel
      Width = 326
      inherited textoValorActivo2: TLabel
        Width = 320
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 50
    Width = 652
    Height = 214
    Align = alTop
    Caption = ' Experiencia Acad'#233'mica '
    TabOrder = 0
    object LEstudios: TLabel
      Left = 65
      Top = 19
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grado de Estudios:'
    end
    object CB_EST_HORlbl: TLabel
      Left = 73
      Top = 180
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Carrera y Horario:'
    end
    object Label1: TLabel
      Left = 104
      Top = 43
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Instituci'#243'n:'
    end
    object Label2: TLabel
      Left = 65
      Top = 67
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Instituci'#243'n:'
    end
    object Label3: TLabel
      Left = 7
      Top = 116
      Width = 148
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Documento Probatorio:'
    end
    object Label4: TLabel
      Left = 21
      Top = 140
      Width = 134
      Height = 13
      Alignment = taRightJustify
      Caption = 'A'#241'o de Emisi'#243'n Documento:'
    end
    object LCarrera: TLabel
      Left = 65
      Top = 91
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Carrera Terminada:'
    end
    object CB_ESTUDIO: TZetaDBKeyLookup_DevEx
      Left = 159
      Top = 15
      Width = 420
      Height = 21
      LookupDataset = dmTablas.cdsEstudios
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      DataField = 'CB_ESTUDIO'
      DataSource = DataSource
    end
    object CB_EST_HOY: TDBCheckBox
      Left = 51
      Top = 158
      Width = 121
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Estudia Actualmente:'
      DataField = 'CB_EST_HOY'
      DataSource = DataSource
      TabOrder = 6
      ValueChecked = 'S'
      ValueUnchecked = 'N'
      OnClick = CB_EST_HOYClick
    end
    object CB_EST_HOR: TDBEdit
      Left = 159
      Top = 176
      Width = 420
      Height = 21
      DataField = 'CB_EST_HOR'
      DataSource = DataSource
      TabOrder = 7
    end
    object CB_ESCUELA: TDBEdit
      Left = 159
      Top = 39
      Width = 420
      Height = 21
      DataField = 'CB_ESCUELA'
      DataSource = DataSource
      TabOrder = 1
    end
    object CB_CARRERA: TDBEdit
      Left = 159
      Top = 87
      Width = 420
      Height = 21
      DataField = 'CB_CARRERA'
      DataSource = DataSource
      TabOrder = 3
    end
    object CB_TESCUEL: TZetaDBKeyCombo
      Left = 159
      Top = 63
      Width = 126
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      ListaFija = lfTipoInstitucionSTPS
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'CB_TESCUEL'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object CB_TITULO: TZetaDBKeyCombo
      Left = 159
      Top = 111
      Width = 126
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 4
      ListaFija = lfTipoDocProbatorioSTPS
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'CB_TITULO'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object CB_YTITULO: TZetaDBNumero
      Left = 159
      Top = 135
      Width = 125
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
      DataField = 'CB_YTITULO'
      DataSource = DataSource
    end
  end
  object GroupBox2: TGroupBox [4]
    Left = 0
    Top = 264
    Width = 652
    Height = 129
    Align = alClient
    Caption = ' Habilidades '
    TabOrder = 1
    object CB_IDIOMAlbl: TLabel
      Left = 69
      Top = 42
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Idioma y Dominio:'
    end
    object LMaquinas: TLabel
      Left = 42
      Top = 66
      Width = 110
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#225'quinas que Conoce:'
    end
    object LExperiencia: TLabel
      Left = 94
      Top = 90
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Experiencia:'
    end
    object CB_HABLA: TDBCheckBox
      Left = 61
      Top = 18
      Width = 110
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Habla Otro Idioma:'
      DataField = 'CB_HABLA'
      DataSource = DataSource
      TabOrder = 0
      ValueChecked = 'S'
      ValueUnchecked = 'N'
      OnClick = CB_HABLAClick
    end
    object CB_IDIOMA: TDBEdit
      Left = 157
      Top = 38
      Width = 420
      Height = 21
      DataField = 'CB_IDIOMA'
      DataSource = DataSource
      TabOrder = 1
    end
    object CB_EXPERIE: TDBEdit
      Left = 157
      Top = 86
      Width = 420
      Height = 21
      DataField = 'CB_EXPERIE'
      DataSource = DataSource
      TabOrder = 3
    end
    object CB_MAQUINA: TDBEdit
      Left = 157
      Top = 62
      Width = 420
      Height = 21
      DataField = 'CB_MAQUINA'
      DataSource = DataSource
      TabOrder = 2
    end
  end
  inherited DataSource: TDataSource
    Left = 384
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
