unit FHisNominas_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls,
     StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
     cxGrid, ZetaCXGrid;

type
  THisNominas_DevEx = class(TBaseGridLectura_DevEx)
    PE_TIPO: TcxGridDBColumn;
    PE_NUMERO: TcxGridDBColumn;
    NO_HORAS: TcxGridDBColumn;
    NO_EXTRAS: TcxGridDBColumn;
    NO_PERCEPC: TcxGridDBColumn;
    NO_DEDUCCI: TcxGridDBColumn;
    NO_NETO: TcxGridDBColumn;
    NO_FEC_PAG: TcxGridDBColumn;
    NO_USR_PAG: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
  end;

var
  HisNominas_DevEx: THisNominas_DevEx;

implementation

{$R *.DFM}

uses DRecursos,
     ZetaCommonLists,
     ZetaCommonClasses;

procedure THisNominas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10144_Historial;
end;

procedure THisNominas_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsHisNominas.Conectar;
          DataSource.DataSet:= cdsHisNominas;
     end;
end;

procedure THisNominas_DevEx.Refresh;
begin
     dmRecursos.cdsHisNominas.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisNominas_DevEx.Borrar;
begin
     dmRecursos.cdsHisNominas.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisNominas_DevEx.Modificar;
begin
     dmRecursos.cdsHisNominas.Modificar;
end;

function THisNominas_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se puede agregar al Historial de N�minas';
end;

procedure THisNominas_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
end;

end.
