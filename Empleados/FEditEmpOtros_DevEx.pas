unit FEditEmpOtros_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, Mask, DBCtrls, ZetaDBTextBox, Db, ExtCtrls,
  ZetaFecha, ZetaNumero, ZetaKeyCombo, ZetaKeyLookup_DevEx,
  ComCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxContainer,
  cxEdit, cxGroupBox, dxBarBuiltInMenu;

type
  TEditEmpOtros_DevEx = class(TBaseEdicion_DevEx)
    pcOtros: TcxPageControl;
    tsInfonavit: TcxTabSheet;
    tsAsistencia: TcxTabSheet;
    tsCuentas: TcxTabSheet;
    tsFonacot: TcxTabSheet;
    tsPPrimerEmpleo: TcxTabSheet;
    tsEvaluacion: TcxTabSheet;
    CB_INF_INILbl: TLabel;
    CB_INF_INI: TZetaDBFecha;
    CB_INF_ANTLbl: TLabel;
    CB_INF_ANT: TZetaDBFecha;
    CB_INFCREDLbl: TLabel;
    CB_INFCRED: TDBEdit;
    CB_INFTIPOlbl: TLabel;
    CB_INFTIPO: TZetaDBKeyCombo;
    CB_INFTASALbl: TLabel;
    CB_INFTASA: TZetaDBNumero;
    CB_INFDISM: TDBCheckBox;
    CB_INFACT: TDBCheckBox;
    LblDescuento: TLabel;
    CB_INFMANT: TDBCheckBox;
    CB_INF_OLDLbl: TLabel;
    zkcCB_INF_OLD: TZetaKeyCombo;
    lblPorcentaje: TLabel;
    LTipoCreden: TLabel;
    CB_CREDENC: TDBEdit;
    CB_CHECA: TDBCheckBox;
    LBanca: TLabel;
    CB_BAN_ELE: TDBEdit;
    lblTarjetasGasolina: TLabel;
    CB_CTA_GAS: TDBEdit;
    lblTarjetasDespensa: TLabel;
    CB_CTA_VAL: TDBEdit;
    BtnBAN_ELE: TcxButton;
    btnTarjetaGasolina: TcxButton;
    btnTarjetaDespensa: TcxButton;
    Label2: TLabel;
    CB_SUB_CTA: TDBEdit;
    lbNeto: TLabel;
    CB_NETO: TZetaDBNumero;
    Label3: TLabel;
    CB_FONACOT: TDBEdit;
    CB_EMPLEO: TDBCheckBox;
    LFechaEva: TLabel;
    CB_LAST_EV: TZetaDBFecha;
    LEvalua: TLabel;
    CB_EVALUA: TZetaDBNumero;
    LProxima: TLabel;
    CB_NEXT_EV: TZetaDBFecha;
    tsBrigada: TcxTabSheet;
    lblTipoBrigada: TLabel;
    lblNoPiso: TLabel;
    CB_BRG_TIP: TZetaDBKeyCombo;
    CB_BRG_NOP: TDBEdit;
    CB_BRG_ACT: TDBCheckBox;
    tsDatosMedicos: TcxTabSheet;
    lblTSangre: TLabel;
    lblAlerPadec: TLabel;
    CB_ALERGIA: TDBEdit;
    tsConfidencialidad: TcxTabSheet;
    CB_NIVEL0: TZetaDBKeyLookup_DevEx;
    LblNivel0: TLabel;
    CB_TSANGRE: TDBComboBox;
    CB_BRG_JEF: TDBCheckBox;
    CB_BRG_ROL: TDBCheckBox;
    CB_BRG_CON: TDBCheckBox;
    CB_BRG_PRA: TDBCheckBox;
    lblRol: TLabel;
    lblConocimiento: TLabel;
    cxGroupBox1: TcxGroupBox;
    lBanco: TLabel;
    CB_BANCO: TZetaDBKeyLookup_DevEx;
    tsTimbrado: TcxTabSheet;
    Label16: TLabel;
    //ZetaDBKeyCombo1: TZetaDBKeyCombo;
    Label4: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    CB_REGIMEN: TZetaDBKeyLookup_DevEx;
    procedure CB_INFTIPOChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure BtnBAN_ELEClick(Sender: TObject);
    procedure zkcCB_INF_OLDChange(Sender: TObject);
    procedure btnTarjetaDespensaClick(Sender: TObject);
    procedure btnTarjetaGasolinaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgRolClick(Sender: TObject);
    procedure rgConocimientoClick(Sender: TObject);
    procedure CB_BRG_ACTClick(Sender: TObject);
    procedure HabilitarBrigada(bandera: Boolean);
    procedure pcOtrosChange(Sender: TObject);

  private
    FConectandoTasa: Boolean;
    procedure HabilitaCredInfo;
    //procedure ClearInfonavit;
    procedure SetControlesNivel0;
    procedure HabilitaOtrosControles;
    procedure ControlesInfonavit( const lEnabled: Boolean );
    procedure AsignaValorTasa;
    procedure SetControlesNeto;
  protected
    procedure Connect; override;
    procedure ImprimirForma;override;
    procedure EscribirCambios; override;
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
  public
  end;

const
     K_NINGUNO = 0;

var
   EditEmpOtros_DevEx: TEditEmpOtros_DevEx;


procedure ShowFormaEdicionOtros( iDerecho : integer );

implementation

uses dRecursos, dSistema, dTablas,  ZetaCommonLists, ZAccesosTress, ZetaCommonClasses,
     ZetaCommonTools, ZetaTipoEntidad,ZetaDialogo, ZImprimeForma, ZAccesosMgr, ZetaClientTools,
     ZGlobalTress, DCliente, DGlobal, FSistEditAccesos_DevEx
     {$ifndef DOS_CAPAS}{$ifndef TIMBRADO}{$ifndef ENROLAMIENTO},FEnrolamiento_DevEx{$endif}{$endif}{$endif};

{$R *.DFM}


procedure ShowFormaEdicionOtros( iDerecho : integer );
begin
     if EditEmpOtros_DevEx = nil then
          EditEmpOtros_DevEx :=  TEditEmpOtros_DevEx.Create( Application );

     EditEmpOtros_DevEx.IndexDerechos := iDerecho;

     ZBaseEdicion_DevEX.ShowFormaEdicion( EditEmpOtros_DevEx, TEditEmpOtros_DevEx );
end;
 

procedure TEditEmpOtros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := iIndexDerechos;

     tsInfonavit.Tag :=  D_EMP_DATOS_OTROS_INFONAVIT;
     tsFonacot.Tag :=    D_EMP_DATOS_OTROS_FONACOT;
     tsPPrimerEmpleo.Tag :=  D_EMP_DATOS_OTROS_PRIMER_EMPLEO;
     tsAsistencia.Tag :=  D_EMP_DATOS_OTROS_ASISTENCIA;
     tsEvaluacion.Tag :=  D_EMP_DATOS_OTROS_EVALUACION;
     tsCuentas.Tag := D_EMP_DATOS_OTROS_CUENTAS;
     tsConfidencialidad.Tag :=  D_EMP_DATOS_OTROS_NIVEL0;
     tsDatosMedicos.Tag := D_EMP_DATOS_OTROS_MEDICOS;
     tsBrigada.Tag :=      D_EMP_DATOS_OTROS_BRIGADAS;
     tsTimbrado.Tag :=      D_EMP_DATOS_OTROS_TIMBRADO;     

     FirstControl := CB_INFTIPO;
     HelpContext:= H10115_Otros_datos_empleado;
     CB_NIVEL0.LookupDataset := dmSistema.cdsNivel0;
     TipoValorActivo1 := stEmpleado;
     FConectandoTasa := FALSE;
     SetControlesNivel0;
     SetControlesNeto;
     zkcCB_INF_OLD.Style := csDropDown;
     ZetaClientTools.LlenaTasaAnterior( zkcCB_INF_OLD );
     
     // Verificar constante de AVENT
     // Protecci�n Civil 2013
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          // GBInfonavit.Visible := False;
          tsInfonavit.TabVisible := False;
          // GBFonacot.Visible := False;
          tsFonacot.TabVisible := False;
          // GBEmpleo.Visible := False;
          tsPPrimerEmpleo.TabVisible := False;
          // GBAsistencia.Top := GBInfonavit.Top;
          // GBEvaluacion.Top := GBAsistencia.Top;
          // GBNomina.Top := GBAsistencia.Top + GBAsistencia.Height + 4;
          // Self.Height := Self.Height - GBInfonavit.Height;
     end;//if }
end;

procedure TEditEmpOtros_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsBancos.Conectar;
          CB_BANCO.LookupDataSet := cdsBancos;

          cdsRegimenContrataTrabajador.Conectar;
          CB_REGIMEN.LookupDataSet := cdsRegimenContrataTrabajador;
     end;

     with dmRecursos do
     begin
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
     HabilitaOtrosControles;
     HabilitaCredInfo;
     AsignaValorTasa;
end;

procedure TEditEmpOtros_DevEx.FormShow(Sender: TObject);
var
   lAplicaTodas : boolean;
   iDerechos : integer;
begin
     inherited;

    HabilitarBrigada(CB_BRG_ACT.Checked);

    // Mostrar  Tab Sheet con derecho a modificar (Consulta en el caso de tsInfonavit)
    lAplicaTodas := IndexDerechos <= D_EMP_DATOS_OTROS;

    iDerechos := IndexDerechos;
    tsInfonavit.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_INFONAVIT, K_DERECHO_CONSULTA ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_INFONAVIT)  ) ;
    IndexDerechos := iDerechos;
    tsFonacot.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_FONACOT, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_FONACOT)  ) ;
    IndexDerechos := iDerechos;
    tsPPrimerEmpleo.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_PRIMER_EMPLEO, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_PRIMER_EMPLEO)  ) ;
    IndexDerechos := iDerechos;
    tsAsistencia.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_ASISTENCIA)  ) ;
    IndexDerechos := iDerechos;
    tsEvaluacion.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_EVALUACION,  K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_EVALUACION)  ) ;
    IndexDerechos := iDerechos;
    tsCuentas.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_CUENTAS)  ) ;
    IndexDerechos := iDerechos;
    tsConfidencialidad.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_NIVEL0, K_DERECHO_CAMBIO ) and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_NIVEL0)  ) ;
    IndexDerechos := iDerechos;
    tsDatosMedicos.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_MEDICOS, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_MEDICOS)  ) ;
    IndexDerechos := iDerechos;
    tsBrigada.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_BRIGADAS, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_BRIGADAS)  ) ;
    IndexDerechos := iDerechos;
    tsTimbrado.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_TIMBRADO, K_DERECHO_CAMBIO )and ( lAplicaTodas or (IndexDerechos = D_EMP_DATOS_OTROS_TIMBRADO)  ) ;
    IndexDerechos := iDerechos; 


    case  IndexDerechos of
           D_EMP_DATOS_OTROS_INFONAVIT : pcOtros.ActivePage := tsInfonavit;
           D_EMP_DATOS_OTROS_FONACOT : pcOtros.ActivePage := tsFonacot;
           D_EMP_DATOS_OTROS_PRIMER_EMPLEO : pcOtros.ActivePage := tsPPrimerEmpleo;
           D_EMP_DATOS_OTROS_ASISTENCIA : pcOtros.ActivePage := tsAsistencia;
           D_EMP_DATOS_OTROS_EVALUACION : pcOtros.ActivePage := tsEvaluacion;
           D_EMP_DATOS_OTROS_CUENTAS : pcOtros.ActivePage := tsCuentas;
           D_EMP_DATOS_OTROS_NIVEL0 : pcOtros.ActivePage := tsConfidencialidad;
           D_EMP_DATOS_OTROS_TIMBRADO : pcOtros.ActivePage := tsTimbrado;

    else
        begin
               if tsConfidencialidad.TabVisible then  pcOtros.ActivePage := tsConfidencialidad;
               if tsCuentas.TabVisible then  pcOtros.ActivePage := tsCuentas;
               if tsEvaluacion.TabVisible then  pcOtros.ActivePage := tsEvaluacion;
               if tsAsistencia.TabVisible then  pcOtros.ActivePage := tsAsistencia;
               if tsPPrimerEmpleo.TabVisible then  pcOtros.ActivePage := tsPPrimerEmpleo;
               if tsFonacot.TabVisible then  pcOtros.ActivePage := tsFonacot;
               //DevEx
               if tsDatosMedicos.TabVisible then  pcOtros.ActivePage := tsDatosMedicos;
               if tsBrigada.TabVisible then pcOtros.ActivePage := tsBrigada;
               IndexDerechos := pcOtros.ActivePage.Tag;
               if tsInfonavit.TabVisible then  pcOtros.ActivePage := tsInfonavit;
               if tsTimbrado.TabVisible then pcOtros.ActivePage := tsTimbrado;
        end;

    end;



    if lAplicaTodas then
    begin
       // Verificar constante de AVENT
       if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
       begin
          if tsAsistencia.TabVisible then
             pcOtros.ActivePage := tsAsistencia;
       end
       else
       begin
          if tsInfonavit.TabVisible then
             pcOtros.ActivePage := tsInfonavit;
       end;
    end;



    end;

procedure TEditEmpOtros_DevEx.CB_INFTIPOChange(Sender: TObject);
begin
     HabilitaCredInfo;
end;

procedure TEditEmpOtros_DevEx.HabilitaCredInfo;
var
   lEnabled: Boolean;
begin
     // Si No Tiene Prestamo, deshabilitar Amortizacion
     // Si Tiene poner mascara correcta
    // lEnabled:= (( CB_INFTIPO.Valor ) <> Ord( tiNoTiene )) and (ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_SIST_KARDEX ));
    lEnabled:= FALSE;
    { AP(20/May/2008): Sug 904 Historial de Infonavit, se cambi� la edici�n al historial de Cr�dito Infonavit }
     with CB_INFTASA do
     begin
          case CB_INFTIPO.Valor of
               Ord( tiNoTiene )   : Mascara := mnPesos;
               Ord( tiPorcentaje ): Mascara := mnTasa;
               Ord( tiCuotaFija  ): Mascara := mnPesos;
               Ord( tiVeces )     : Mascara := mnVecesSMGDF;
               Ord( tiVecesUMA)   : Mascara := mnVecesUMA;
          end;
          Enabled:= lEnabled;
     end;
     CB_INFTASALbl.Enabled := lEnabled;
     CB_INFCRED.Enabled:= lEnabled;
     CB_INFCREDLbl.Enabled:= lEnabled;
     CB_INFMANT.Enabled:= lEnabled;
     CB_INF_INI.Enabled:= lEnabled;
     CB_INF_INILbl.Enabled:= lEnabled;
     CB_INF_ANT.Enabled:= lEnabled;
     CB_INF_ANTLbl.Enabled:= lEnabled;
     CB_INFDISM.Enabled:= lEnabled;
     CB_INFACT.Enabled:= lEnabled;     
     zkcCB_INF_OLD.Text := FloattoStr( dmRecursos.cdsDatosEmpleado.FieldByName( 'CB_INF_OLD' ).AsFloat );
     {zkcCB_INF_OLD.Enabled := (( CB_INFTIPO.Valor ) = Ord( tiPorcentaje ));}
     zkcCB_INF_OLD.Enabled:= lEnabled;
     CB_INF_OLDLbl.Enabled := zkcCB_INF_OLD.Enabled;
end;

procedure TEditEmpOtros_DevEx.OK_DevExClick(Sender: TObject);
begin
     {V2013

     Protecci�n Civil}
     with dmRecursos.cdsDatosEmpleado do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;

     if ( CB_NIVEL0.Visible ) and ( strVacio( CB_NIVEL0.Llave ) )  and ( tsConfidencialidad.TabVisible )  then
     begin
          ZError('','Nivel de Confidencialidad no puede quedar Vacio',0);
          pcOtros.ActivePage := tsConfidencialidad;
          ActiveControl := CB_NIVEL0;
     end
     else if (CB_BRG_ACT.Checked ) and (CB_BRG_TIP.ItemIndex = 0) and ( tsBrigada.TabVisible ) then
     begin
          ZError('','Tipo de Brigada no puede ser Ninguna',0);
          pcOtros.ActivePage := tsBrigada;
          ActiveControl := CB_BRG_TIP;
     end
     else if ( strLleno( CB_BAN_ELE.Text ) and ( dmRecursos.ValidacionForzadaCuenta( Caption, Trim( CB_BAN_ELE.text ),'CB_BAN_ELE' )) )  then
     begin
          pcOtros.ActivePage := tsCuentas;
          ActiveControl := CB_BAN_ELE;
     end
     else if ( strLleno( CB_CTA_GAS.Text ) and ( dmRecursos.ValidacionForzadaCuenta( Caption, Trim( CB_CTA_GAS.text ),'CB_CTA_GAS' )) ) then
     begin
          pcOtros.ActivePage := tsCuentas;
          ActiveControl := CB_CTA_GAS;
     end
     else if ( strLleno( CB_CTA_VAL.Text ) and ( dmRecursos.ValidacionForzadaCuenta( Caption, Trim( CB_CTA_VAL.text ),'CB_CTA_VAL' )) ) then
     begin
          pcOtros.ActivePage := tsCuentas;
          ActiveControl := CB_CTA_VAL;
     end
     else
     begin
          {if (( CB_INFTIPO.Valor ) = Ord( tiNoTiene )) then
             ClearInfonavit;}
          inherited;
{$ifdef DOS_CAPAS}
          Close; //Ya no se cerrara para que puedan enrolar su huella.
{$endif}
     end;
end;

procedure TEditEmpOtros_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     HabilitaCredInfo;
end;

procedure TEditEmpOtros_DevEx.BtnBAN_ELEClick(Sender: TObject);
begin
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_BAN_ELE.text ),'CB_BAN_ELE' );
end;



procedure TEditEmpOtros_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEditEmpOtros_DevEx.SetControlesNivel0;
begin
     CB_NIVEL0.Visible := dmSistema.HayNivel0;
     LblNivel0.Visible := CB_NIVEL0.Visible;
end;

procedure TEditEmpOtros_DevEx.SetControlesNeto;
begin
     lbNeto.Visible := dmRecursos.PuedeVerNeto;
     CB_NETO.Visible := lbNeto.Visible;

     { V2013
     2013-03-18. AL.}
     {if NOT lbNeto.Visible then
     begin
          if CB_NIVEL0.Visible then
          begin
               LblNivel0.Top := lbNeto.Top;
               CB_NIVEL0.Top := CB_neto.Top;
          end;
     end; }
end;

procedure TEditEmpOtros_DevEx.HabilitaOtrosControles;
begin
     CB_BAN_ELE.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_BANCA );
     lBanca.Enabled := CB_BAN_ELE.Enabled;
     BtnBAN_ELE.Enabled := CB_BAN_ELE.Enabled;
     lBanco.Enabled := CB_BAN_ELE.Enabled;
     CB_BANCO.Enabled :=CB_BAN_ELE.Enabled;

     if CB_NIVEL0.Visible then
     begin
          CB_NIVEL0.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_NIVEL0, K_DERECHO_CAMBIO );
          LblNivel0.Enabled := CB_NIVEL0.Enabled;
     end;
     ControlesInfonavit( FALSE );

     CB_CTA_VAL.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_ADICIONAL9 );
     lblTarjetasDespensa.Enabled := CB_CTA_VAL.Enabled;
     btnTarjetaDespensa.Enabled := CB_CTA_VAL.Enabled;

     CB_CTA_GAS.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CONFIGURA );
     lblTarjetasGasolina.Enabled := CB_CTA_GAS.Enabled;
     btnTarjetaGasolina.Enabled := CB_CTA_GAS.Enabled;
     { AP(20/May/2008): Sug 904 Historial de Infonavit, se cambi� la edici�n al historial de Cr�dito Infonavit
     ControlesInfonavit( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_SIST_KARDEX ) );}
end;

procedure TEditEmpOtros_DevEx.ControlesInfonavit( const lEnabled: Boolean );
begin
     CB_INFTIPO.Enabled := lEnabled;
     CB_INFTIPOlbl.Enabled := lEnabled;
     CB_INFCRED.Enabled := lEnabled;
     CB_INFCREDlbl.Enabled := lEnabled;
     CB_INFTASA.Enabled := lEnabled;
     CB_INFTASAlbl.Enabled := lEnabled;
     zkcCB_INF_OLD.Enabled := lEnabled;
     CB_INF_OLDlbl.Enabled := lEnabled;
     CB_INFMANT.Enabled := lEnabled;
     CB_INF_INI.Enabled := lEnabled;
     CB_INF_INIlbl.Enabled := lEnabled;
     CB_INFDISM.Enabled:= lEnabled;
end;

procedure TEditEmpOtros_DevEx.AsignaValorTasa;
var
     rTasa: TTasa;
begin
     FConectandoTasa := TRUE;
     try
     begin
          rTasa := dmRecursos.cdsDatosEmpleado.FieldByName('CB_INF_OLD').AsFloat;
          if( rTasa > 0 )then
              zkcCB_INF_OLD.Text := FloattoStr( rTasa )
          else
              zkcCB_INF_OLD. ItemIndex := K_NINGUNO;
     end
     finally
            FConectandoTasa := FALSE;
     end;
end;

procedure TEditEmpOtros_DevEx.zkcCB_INF_OLDChange(Sender: TObject);
var
    rValor: TTasa;
begin
     if not( FConectandoTasa ) then
     begin
          with  zkcCB_INF_OLD do
          begin
               if( Text = ZetaCommonClasses.aArregloTasa[0] )then
                   rValor := K_NINGUNO
               else
               begin
                    try
                       rValor := StrtoFloat( Text );
                    except
                          //on E: Exception do //ErrorDialog(E.Message, E.HelpContext);
                          rValor := K_NINGUNO;
                    end;
               end;
               //with dmRecursos.cdsDatosEmpleado do
               with dmCliente.cdsEmpleado do
               begin
                    if not Editing then
                       Edit;
                    FieldByName( 'CB_INF_OLD' ).AsFloat:= rValor;
               end;
          end;
     end;
end;

procedure TEditEmpOtros_DevEx.EscribirCambios;
begin
     with dmCliente.cdsEmpleado do
     begin
          if ( FieldByName( 'CB_INFTIPO' ).AsInteger = ord(tiPorcentaje) ) then
          begin
               if not( ZetaClientTools.VerificaValorTasa( FieldByName('CB_INF_OLD').AsFloat ) )then
               begin
                    if not( ZConfirm( 'Informaci�n', 'La Tasa Anterior es un valor diferente a los valores propuestos.' + CR_LF +
                                                    '�Desea continuar?',0,mbYes ) )then
                    begin
                         Abort;
                    end;
               end;
          end;
     end;
     inherited EscribirCambios;
end;

procedure TEditEmpOtros_DevEx.btnTarjetaDespensaClick(Sender: TObject);
begin
     inherited;
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_CTA_VAL.text ),'CB_CTA_VAL' );
end;

procedure TEditEmpOtros_DevEx.btnTarjetaGasolinaClick(Sender: TObject);
begin
     inherited;
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_CTA_GAS.text ),'CB_CTA_GAS' );
end;

procedure TEditEmpOtros_DevEx.rgRolClick(Sender: TObject);
begin
  inherited;
  OK_DevEx.Enabled := true;
end;

procedure TEditEmpOtros_DevEx.rgConocimientoClick(Sender: TObject);
begin
  inherited;
  OK_DevEx.Enabled := TRUE;
end;

procedure TEditEmpOtros_DevEx.CB_BRG_ACTClick(Sender: TObject);
begin
  inherited;   
  HabilitarBrigada(CB_BRG_ACT.Checked);

  { if not CB_BRG_ACT.Checked then
     CB_BRG_TIP.ItemIndex := 0; }
end;

procedure TEditEmpOtros_DevEx.HabilitarBrigada(bandera: Boolean);
begin
  inherited;   
  CB_BRG_TIP.Enabled := bandera;
  CB_BRG_ROL.Enabled := bandera;
  CB_BRG_JEF.Enabled := bandera;
  CB_BRG_CON.Enabled := bandera;
  CB_BRG_PRA.Enabled := bandera;
  CB_BRG_NOP.Enabled := bandera;
end;

procedure TEditEmpOtros_DevEx.pcOtrosChange(Sender: TObject);
begin
  inherited;
  if not ( pcOtros.ActivePage = nil )  then         //DevEx
     Self.IndexDerechos :=  pcOtros.ActivePage.Tag;
end;

function TEditEmpOtros_DevEx.CheckDerechos(const iDerecho: Integer): Boolean;
begin
      if iDerecho = K_DERECHO_CAMBIO then
         Result := TRUE
      else
          Result := ZAccesosMgr.CheckDerecho( IndexDerechos, iDerecho );
end;

end.
