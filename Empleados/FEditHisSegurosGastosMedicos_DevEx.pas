unit FEditHisSegurosGastosMedicos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Db, ExtCtrls, ZetaDBTextBox,
  ZetaKeyCombo, ZetaNumero, ZetaFecha, ComCtrls, ZetaSmartLists,
  ZetaCommonLists, ZetaCommonClasses,
  ZetaEdit,Variants, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxGroupBox, cxRadioGroup,
  cxTextEdit, cxMemo, cxDBEdit;


Type
     TEditHisSeguroGastosMedicos_DevEx = class(TBaseEdicion_DevEx)
    gbxTipo: TcxRadioGroup;
    lblDependiente: TLabel;
    Label8: TLabel;
    EP_CERTIFI: TZetaDBEdit;
    Label9: TLabel;
    EP_FEC_INI: TZetaDBFecha;
    Label10: TLabel;
    EP_FEC_FIN: TZetaDBFecha;
    EP_STATUS: TZetaDBKeyCombo;
    Label17: TLabel;
    cbxDependiente: TZetaKeyCombo;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    txtPV_FEC_INI: TZetaTextBox;
    txtPM_NUMERO: TZetaTextBox;
    Label6: TLabel;
    txtPV_FEC_FIN: TZetaTextBox;
    txtPV_CONDIC: TZetaTextBox;
    PM_CODIGO: TZetaDBKeyLookup_DevEx;
    PV_REFEREN: TZetaDBKeyCombo;
    rdgAsegurado: TcxRadioGroup;
    EP_ORDEN: TZetaDBNumero;
    Label7: TLabel;
    pgPaginas: TcxPageControl;
    TabSheet1: TcxTabSheet;
    TabCancelacion: TcxTabSheet;
    bbtnBuscarTablaCotizacion: TcxButton;
    EP_CFIJO: TZetaDBNumero;
    EP_CPOLIZA: TZetaDBNumero;
    EP_CTITULA: TZetaDBNumero;
    txtCEMPRES: TZetaNumero;
    Label15: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    txtTotalPagar: TZetaTextBox;
    TabSheet3: TcxTabSheet;
    EP_OBSERVA: TcxDBMemo;
    Panel2: TPanel;
    Label21: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    Label22: TLabel;
    EP_FEC_REG: TZetaDBTextBox;
    Label23: TLabel;
    EP_GLOBAL: TZetaDBTextBox;
    lblFechaCan: TLabel;
    EP_CAN_FEC: TZetaDBFecha;
    lblMontoCan: TLabel;
    EP_CAN_MON: TZetaDBNumero;
    EP_CAN_MOT: TZetaDBEdit;
    lblMotivoCan: TLabel;
    EP_END_CAN: TZetaDBEdit;
    lblFolioEndoso: TLabel;
    Label16: TLabel;
    EP_END_ALT: TZetaDBEdit;
    lblTablaCotizacion: TLabel;
     procedure FormCreate(Sender: TObject);
    procedure PM_CODIGOExit(Sender: TObject);
    procedure EP_CFIJOChange(Sender: TObject);
    procedure gbxTipoClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EP_STATUSCloseUp(Sender: TObject);
    procedure cbxDependienteChange(Sender: TObject);
    procedure PV_REFERENCloseUp(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rdgAseguradoClick(Sender: TObject);
    procedure EP_CTITULAChange(Sender: TObject);
    procedure bbtnBuscarTablaCotizacionClick(Sender: TObject);
    procedure txtCEMPRESExit(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);

  private
    FNavegando:Boolean;
    procedure LLenarListaParientes;
    procedure LLenarListaReferencias;
    procedure MostrarDatosReferencia;
    procedure ActualizaTotal;
    procedure HabilitaDependiente(lHabilita: Boolean);
    procedure LimpiarControles;
    procedure SeleccionarPariente(const iRelacion,iFolio:Integer);
    function GetSelIndex(sReferencia: string): Integer;
    procedure HabilitaCancelado(const bHabilita: Boolean);
    procedure Edicion;
    procedure CheckControles(const lHabilita: Boolean);
    function CambioCodigo(FieldName: string): Boolean;
    procedure SetFechasReferencia;
    function GetCostoGenero(Genero: string; Edad: Integer): TPesos;

  protected
     procedure Connect;override;
     procedure HabilitaControles; override;
     procedure EscribirCambios; override;
     function PuedeModificar(var sMensaje: String): Boolean;override;
     function PuedeBorrar(var sMensaje: String): Boolean;override;
  public
     { Public declarations }
end;

type
  TPariente = class(TObject)
    Relacion: Integer;
    Folio: Integer;
  end;


var
     EditHisSeguroGastosMedicos_DevEx: TEditHisSeguroGastosMedicos_DevEx;

implementation

uses DRecursos,
     dSistema,
     DCatalogos,
     ZAccesosTress,
     ZetaClientTools,
     ZetaDialogo,
     ZAccesosMgr,
     ZetaCommonTools, DCliente;

{$R *.DFM}

procedure TEditHisSeguroGastosMedicos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := ZetacommonLists.stEmpleado;
     HelpContext:= H_EMP_EXP_SGM_EDIT;
     IndexDerechos := ZAccesosTress.D_EMP_SEGUROS_GASTOS_MEDICOS;
     FirstControl := PM_CODIGO;
     EP_STATUS.ListaFija := lfStatusSGM;
     LLenarListaParientes
end;

procedure TEditHisSeguroGastosMedicos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ActualizaTotal;
     HabilitaControles;
     pgPaginas.ActivePageIndex := 0;
     lblTablaCotizacion.Caption := VACIO;
end;


procedure TEditHisSeguroGastosMedicos_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisSGM.Conectar;
          DataSource.DataSet := cdsHisSGM;
          cdsEmpParientes.Conectar;
     end;
     with dmCatalogos do
     begin
          cdsSegGastosMed.Refrescar;
          PM_CODIGO.LookupDataset := cdsSegGastosMed;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.LimpiarControles;
begin
     txtPV_FEC_INI.Caption := VACIO;
     txtPV_FEC_FIN.Caption := VACIO;
     txtPV_CONDIC.Caption := VACIO;
     txtPM_NUMERO.Caption := VACIO;
     txtTotalPagar.Caption := VACIO;
     PV_REFEREN.Items.Clear;
     txtCEMPRES.Valor := 0; 
end;

procedure TEditHisSeguroGastosMedicos_DevEx.LLenarListaParientes;
var
   Pariente : TPariente;
begin
     cbxDependiente.Items.Clear;
     with dmRecursos.cdsEmpParientes  do
     begin
          Filtered := False;
          Filter := 'PA_RELACIO in (3,4)';
          Filtered := True;
          if RecordCount > 0 then
          begin
               First;
               while not Eof do
               begin
                    Pariente := TPariente.Create;
                    Pariente.Relacion := FieldByName('PA_RELACIO').AsInteger;
                    Pariente.Folio    := FieldByName('PA_FOLIO').AsInteger;
                    cbxDependiente.Items.AddObject(FieldByName('PA_NOMBRE').AsString +' ( ' +ObtieneElemento(lfTipoPariente,FieldByName('PA_RELACIO').AsInteger ) +' )',TObject(Pariente));
                    Next;
               end;
          end;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.LLenarListaReferencias;
begin
     PV_REFEREN.Items.Clear;
     with dmCatalogos.cdsVigenciasSGM do
     begin
          if StrLLeno(PM_CODIGO.Llave)then
          begin
               Filtered := False;
               Filter   := Format('PM_CODIGO = ''%s''',[PM_CODIGO.Llave]);
               Filtered := True;
               First;
               while not Eof do
               begin
                    PV_REFEREN.Items.Add(FieldByName('PV_REFEREN').AsString);
                    Next;
               end;

          end;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.MostrarDatosReferencia;
begin
     with dmCatalogos.cdsVigenciasSGM do
     begin
          Locate('PM_CODIGO;PV_REFEREN',VarArrayOf([PM_CODIGO.Llave,PV_REFEREN.Text]),[]);
          if dmRecursos.cdsHisSGM.State in [dsEdit,dsInsert] then
          begin
               SetFechasReferencia;
               dmRecursos.cdsHisSGM.FieldByName('EP_CFIJO').AsFloat := FieldByName('PV_CFIJO').AsFloat;
               dmRecursos.cdsHisSGM.FieldByName('EP_ORDEN').AsInteger := dmRecursos.GetMaxOrdenPolizas(PM_CODIGO.Llave,PV_REFEREN.Text) + 1;
          end;
          txtPV_FEC_INI.Caption := FieldByName('PV_FEC_INI').AsString;
          txtPV_FEC_FIN.Caption := FieldByName('PV_FEC_FIN').AsString;
          txtPV_CONDIC.Caption := FieldByName('PV_CONDIC').AsString;

     end;
end;

function TEditHisSeguroGastosMedicos_DevEx.CambioCodigo(FieldName:string) :Boolean;
     begin
          with dmRecursos.cdsHisSGM.FieldByName(FieldName) do
          Result := ( not VarIsNull( OldValue ) ) and ( OldValue <> NewValue ) ;
     end;

procedure TEditHisSeguroGastosMedicos_DevEx.PM_CODIGOExit(Sender: TObject);
begin
     inherited;
     if dmRecursos.cdsHisSGM.State in [ dsInsert,dsEdit ] then
     begin
          if StrLleno(PM_CODIGO.Llave)  then
          begin
               if CambioCodigo('PM_CODIGO') or ( dmRecursos.cdsHisSGM.State in [ dsInsert ] ) then
               begin
                    LLenarListaReferencias;
                    if PV_REFEREN.Items.Count > 0 then
                    begin
                         txtPM_NUMERO.Caption := dmCatalogos.cdsSegGastosMed.FieldByName('PM_NUMERO').AsString;
                         PV_REFEREN.ItemIndex := PV_REFEREN.Items.Count - 1;
                         MostrarDatosReferencia;

                    end
                    else
                    begin
                         txtPM_NUMERO.Caption := VACIO;
                         PM_CODIGO.SetFocus;
                         ZWarning('Seguro de Gastos M�dicos',format('La P�liza : %s no est� vigente a la fecha actual',[PM_CODIGO.Descripcion]),0,mbOk);
                    end;
               end;
          end
          else
          begin
                txtPM_NUMERO.Caption := VACIO;
                PV_REFEREN.Items.Clear;
                PV_REFEREN.ItemIndex := -1;
                txtPV_FEC_INI.Caption := VACIO;
                txtPV_FEC_FIN.Caption := VACIO;
                txtPV_CONDIC.Caption := VACIO;
          end;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.EP_CFIJOChange(Sender: TObject);
begin
   inherited;
     ActualizaTotal;
     {with dmRecursos.cdsHisSGM do
     begin
          FieldByName('EP_CEMPRES').AsFloat :=  ( FieldByName('EP_CFIJO').AsFloat + FieldByName('EP_CPOLIZA').AsFloat) - ( FieldByName('EP_CTITULA').AsFloat ) ;
     end;}
end;

procedure TEditHisSeguroGastosMedicos_DevEx.ActualizaTotal;
begin
     with dmRecursos.cdsHisSGM do
     begin
          txtTotalPagar.Caption :=  FormatFloat( '#,0.00', FieldByName('EP_CFIJO').AsFloat + FieldByName('EP_CPOLIZA').AsFloat );
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.HabilitaDependiente(lHabilita:Boolean);
begin
     lblDependiente.Enabled := lHabilita;
     cbxDependiente.ItemIndex := -1;
     gbxTipo.Enabled := lHabilita;
     cbxDependiente.Enabled := lHabilita;
     cbxDependiente.Items.Clear;
     LLenarListaParientes;
     if ( lHabilita ) and ( cbxDependiente.Items.Count = 0) then
     begin
          ZWarning('Seguro de Gastos M�dicos','No existen Parientes para Asignar',0,mbOk);
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.gbxTipoClick(Sender: TObject);
begin
     inherited;

     //HabilitaDependiente(gbxTipo.ItemIndex = 1);
     with dmRecursos.cdsHisSGM do
     begin
          if not FNavegando then
          begin
               Edicion;
               FieldByName('EP_TIPO').AsInteger := gbxTipo.ItemIndex;
          end;
     end
end;

procedure TEditHisSeguroGastosMedicos_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if Field = nil then
        HabilitaControles;
end;


procedure TEditHisSeguroGastosMedicos_DevEx.HabilitaControles;
begin
     inherited;
     with dmRecursos.cdsHisSGM do
     begin
          if state = dsBrowse then
          begin
               FNavegando := True;
               LimpiarControles;
               LLenarListaReferencias;
               ActualizaTotal;
               gbxTipo.ItemIndex :=  FieldByName('EP_TIPO').AsInteger;
               rdgAsegurado.ItemIndex :=  FieldByName('EP_ASEGURA').AsInteger;
               HabilitaDependiente(( rdgAsegurado.ItemIndex = 1) and (ZAccesosMgr.CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_CAMBIO ) ));
               PV_REFEREN.ItemIndex := GetSelIndex( FieldByName('PV_REFEREN').AsString );
               SeleccionarPariente(FieldByName('PA_RELACIO').AsInteger,FieldByName('PA_FOLIO').AsInteger);

               dmCatalogos.cdsSegGastosMed.Locate('PM_CODIGO',FieldByName('PM_CODIGO').AsString,[]);
               txtPM_NUMERO.Caption := dmCatalogos.cdsSegGastosMed.FieldByName('PM_NUMERO').AsString;

               MostrarDatosReferencia;
               HabilitaCancelado(EP_STATUS.ItemIndex = Ord(ssCancelada));
               FNavegando := False;
               if  FieldByName('EP_STATUS').AsInteger = Ord(ssVencida) then
                   CheckControles(   CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_SIST_KARDEX ) and CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_CAMBIO )  )
               else
                   CheckControles(   CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_CAMBIO )  );
               txtCEMPRES.Valor := ( FieldByName('EP_CFIJO').AsFloat + FieldByName('EP_CPOLIZA').AsFloat) - ( FieldByName('EP_CTITULA').AsFloat );
          end;

          if state = dsInsert then
          begin
               LimpiarControles;
               HabilitaDependiente(False);
               rdgAsegurado.ItemIndex := 0;
               gbxTipo.ItemIndex := 0;
               HabilitaCancelado(False);
               CheckControles(   CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_ALTA )  )
          end
          else if state = dsEdit then
          begin
               if  FieldByName('EP_STATUS').AsInteger = Ord(ssVencida) then
                   CheckControles(   CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_SIST_KARDEX )  )
               else
                   CheckControles(   CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_CAMBIO )  );
          end;
     end;
end;

function TEditHisSeguroGastosMedicos_DevEx.GetSelIndex( sReferencia:string):Integer;
var
   i:Integer;
begin
     with PV_REFEREN do
     begin
          Result := -1;

          for i:=0 to Items.Count -1 do
          begin
               if Items[i] = sReferencia then
               begin
                    Result := i;
                    break;
               end;
          end;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.SeleccionarPariente(const iRelacion,iFolio:Integer);
var
   i:Integer;
   Pariente:TPariente;
begin
     if dmRecursos.cdsHisSGM.FieldByName('EP_ASEGURA').AsInteger = Ord(taEmpleado) then
     begin
          cbxDependiente.ItemIndex := -1;
     end
     else
     begin
          for i := 0 to cbxDependiente.Items.Count -1 do
          begin
               Pariente := TPariente( cbxDependiente.Items.Objects[i] );
               if ( iRelacion = Pariente.Relacion ) and ( iFolio = Pariente.Folio )then
               begin
                    cbxDependiente.ItemIndex := i;
                    break;
               end;
          end;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.EscribirCambios;
var
   Pariente:TPariente;
begin
     with dmRecursos.cdsHisSGM do
     begin
          Pariente := TPariente.Create();
          with Pariente do
          begin
               Relacion := 0;
               Folio := 0;
          end;
          if FieldByName('EP_ASEGURA').AsInteger = Ord(taPariente)then
          begin
               if cbxDependiente.ItemIndex >= 0 then
               begin
                    Pariente := TPariente( cbxDependiente.Items.Objects[cbxDependiente.ItemIndex])
               end
               else
                   DatabaseError('Seleccionar Pariente');
          end;
          FieldByName('PA_FOLIO').AsInteger := Pariente.Folio;
          FieldByName('PA_RELACIO').AsInteger := Pariente.Relacion;
          FieldByName('PV_REFEREN').AsString := PV_REFEREN.Text;
          FieldByName('EP_CANCELA').AsString := zBoolToStr(EP_STATUS.ItemIndex = Ord(ssCancelada));

     end;
     inherited;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     dmRecursos.cdsEmpParientes.Filtered := False;
end;

procedure  TEditHisSeguroGastosMedicos_DevEx.HabilitaCancelado(const bHabilita:Boolean);
begin
     TabCancelacion.Enabled := bHabilita;
     EP_CAN_FEC.Enabled := bHabilita;
     EP_CAN_MON.Enabled := bHabilita;
     EP_CAN_MOT.Enabled := bHabilita;
     lblFechaCan.Enabled:=  bHabilita;
     lblMotivoCan.Enabled := bHabilita;
     lblMontoCan.Enabled := bHabilita;
     lblFolioEndoso.Enabled := bHabilita;
     EP_END_CAN.Enabled := bHabilita;
     if not bHabilita then
     begin
          EP_CAN_MON.Valor := 0;
          EP_CAN_MOT.Text := VACIO;
          EP_END_CAN.Text := VACIO;
          EP_CAN_FEC.Valor := NullDateTime; 
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.EP_STATUSCloseUp(Sender: TObject);
begin
     inherited;

     if EP_STATUS.ItemIndex = Ord(ssVencida)then
     begin
          ZWarning('Seguro Gastos M�dicos','No se puede asignar el status de Vencida, este se determina por el rango de fechas',0,mbOk);
          EP_STATUS.ItemIndex := dmRecursos.cdsHisSGM.FieldByName('EP_STATUS').AsInteger;
          EP_STATUS.SetFocus;
     end
     else
     if ( EP_STATUS.ItemIndex = Ord(ssActiva) ) and (dmRecursos.cdsHisSGM.FieldByName('EP_STATUS').AsInteger = Ord(ssVencida)) then
     begin
          ZWarning('Seguro Gastos M�dicos','No se puede asignar el status de Activa, este se determina por el rango de fechas',0,mbOk);
          EP_STATUS.ItemIndex := dmRecursos.cdsHisSGM.FieldByName('EP_STATUS').AsInteger;
          EP_STATUS.SetFocus;
     end
     else
         Edicion;
     if EP_STATUS.ItemIndex = Ord(ssCancelada) then
     begin
          EP_CAN_FEC.Valor := dmCliente.FechaDefault;
          pgPaginas.ActivePageIndex := 2;//Ir a la pagina de cancelacion. 
     end
     else
         EP_CAN_FEC.Valor := NullDateTime;

     HabilitaCancelado(EP_STATUS.ItemIndex = Ord(ssCancelada));

end;

procedure TEditHisSeguroGastosMedicos_DevEx.Edicion;
begin
     with dmRecursos.cdsHisSGM do
     begin
          if state = dsBrowse then
             Edit;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.CheckControles(const lHabilita:Boolean);
begin
     PM_CODIGO.Enabled := lHabilita;
     PV_REFEREN.Enabled := lHabilita;
     gbxTipo.Enabled := lHabilita;
     rdgAsegurado.Enabled := lHabilita;
     cbxDependiente.Enabled := lHabilita;
     EP_CERTIFI.Enabled := lHabilita;
     EP_CFIJO .Enabled := lHabilita;
     EP_CPOLIZA .Enabled := lHabilita;
     EP_CTITULA .Enabled := lHabilita;
     EP_OBSERVA .Enabled := lHabilita;
     EP_STATUS .Enabled := lHabilita;
     EP_CAN_FEC .Enabled := lHabilita;
     EP_CAN_MON .Enabled := lHabilita;
     EP_CAN_MOT .Enabled := lHabilita;
     EP_FEC_INI.Enabled := lHabilita;
     EP_FEC_FIN.Enabled := lHabilita;
     bbtnBuscarTablaCotizacion.Enabled := lHabilita; 
end;

function TEditHisSeguroGastosMedicos_DevEx.PuedeModificar(var sMensaje: String): Boolean;

function EstaVencida :Boolean;
begin
     Result := dmRecursos.cdsHisSGM.FieldByName('EP_STATUS').AsInteger = Ord(ssVencida);
end;

begin
     sMensaje := 'No Tiene Permiso para Modificar';
     Result := CheckDerecho( D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_CAMBIO );
     if Result and EstaVencida then
     begin
          sMensaje := 'No Tiene Permiso Para Modificar P�lizas Vencidas';
          Result := CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_SIST_KARDEX );
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.cbxDependienteChange(
  Sender: TObject);
begin
     inherited;
     Edicion;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.PV_REFERENCloseUp(Sender: TObject);
begin
     inherited;
     if dmRecursos.cdsHisSGM.FieldByName('PV_REFEREN').AsString <> PV_REFEREN.Text then
     begin
          Edicion;
     end;
          MostrarDatosReferencia;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.SetFechasReferencia;

     function GetFechaInicial:TDate;
     begin
          with dmCatalogos.cdsVigenciasSGM do
          begin
               If dmCliente.FechaDefault > FieldByName('PV_FEC_INI').AsDateTime then
                  Result := dmCliente.FechaDefault
               else
                   Result := FieldByName('PV_FEC_INI').AsDateTime;
          end;
     end;
begin
     with dmRecursos.cdsHisSGM do
     begin
          FieldByName('EP_FEC_INI').AsDateTime := GetFechaInicial;
          FieldByName('EP_FEC_FIN').AsDateTime := dmCatalogos.cdsVigenciasSGM.FieldByName('PV_FEC_FIN').AsDateTime;
     end;
end;

function TEditHisSeguroGastosMedicos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Tiene Permisos para Borrar';
     IF dmRecursos.cdsHisSGM.FieldByName('EP_STATUS').AsInteger = Ord(ssVencida) then
     begin
          sMensaje := 'No Tiene Permisos para Borrar P�lizas Vencidas';
          Result := CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_BAJA ) and ZAccesosMgr.CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_SIST_KARDEX );
     end
     else
         Result := CheckDerecho(D_EMP_SEGUROS_GASTOS_MEDICOS , K_DERECHO_BAJA );
end;


procedure TEditHisSeguroGastosMedicos_DevEx.rdgAseguradoClick(Sender: TObject);
begin
     inherited;
     HabilitaDependiente(rdgAsegurado.ItemIndex = 1);
     with dmRecursos.cdsHisSGM do
     begin
          if not FNavegando then
          begin
               Edicion;
               FieldByName('EP_ASEGURA').AsInteger := rdgAsegurado.ItemIndex;
               gbxTipo.ItemIndex := rdgAsegurado.ItemIndex;
          end;
     end
end;

procedure TEditHisSeguroGastosMedicos_DevEx.EP_CTITULAChange(Sender: TObject);
begin
     inherited;
     with dmRecursos.cdsHisSGM do
     begin
          txtCEMPRES.Valor := ( FieldByName('EP_CFIJO').AsFloat + FieldByName('EP_CPOLIZA').AsFloat) - ( FieldByName('EP_CTITULA').AsFloat );
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.bbtnBuscarTablaCotizacionClick(
  Sender: TObject);
var
   sKey,sDescription,sGenero:string;
   Edad : Integer;
   Pariente:TPariente;
begin
     inherited;

     if ( rdgAsegurado.ItemIndex = Ord(taPariente) ) and ( cbxDependiente.ItemIndex = -1 ) then
        ZError(Self.Caption ,'Seleccionar Pariente',0)
     else
     begin
          dmCatalogos.cdsTablasAmortizacion.Conectar;
          if dmCatalogos.cdsTablasAmortizacion.Search_DevEx( VACIO,sKey,sDescription)then
          begin
               with dmRecursos.cdsHisSGM do
               begin
                    Edicion;
                    if FieldByName('EP_ASEGURA').AsInteger = Ord(taPariente) then
                    begin

                         begin
                              Pariente := TPariente( cbxDependiente.Items.Objects[cbxDependiente.ItemIndex]);
                              dmRecursos.cdsEmpParientes.Locate('PA_RELACIO;PA_FOLIO',VarArrayOf([Pariente.Relacion,Pariente.Folio]),[]);
                              sGenero := dmRecursos.cdsEmpParientes.FieldByName('PA_SEXO').AsString;
                              Edad :=  zYearsBetween ( dmRecursos.cdsEmpParientes.FieldByName('PA_FEC_NAC').AsDateTime, EP_FEC_INI.Valor  );
                         end;
                    end
                    else
                    begin
                         sGenero := dmCliente.cdsEmpleado.FieldByName('CB_SEXO').AsString;
                         Edad := zYearsBetween ( dmCliente.cdsEmpleado.FieldByName('CB_FEC_NAC').AsDateTime, EP_FEC_INI.Valor );
                    end;

                    FieldByName('EP_CPOLIZA').AsFloat := GetCostoGenero( sGenero,Edad );
                    lblTablaCotizacion.Caption := sDescription;
               end;
               EP_CTITULA.SetFocus;
          end;
     end;
end;

Function TEditHisSeguroGastosMedicos_DevEx.GetCostoGenero(Genero:string; Edad:Integer ):TPesos;
begin
     with dmCatalogos.cdsCostosAmortizacion do
     begin
          if State = dsBrowse then
          begin
               Filtered := False;
               Filter := Format('AT_CODIGO = ''%s''',[dmCatalogos.cdsTablasAmortizacion.FieldByName('AT_CODIGO').AsString]);
               Filtered := True;
          end;

          Result := 0;
          First;
          while not eof do
          begin
               if ( Edad >= FieldByName('AC_EDADINI').AsInteger ) and ( Edad <= FieldByName('AC_EDADFIN').AsInteger) then
               begin
                    if Genero = 'M' THEN
                       Result := FieldByName('AC_CHOMBRE').AsFloat
                    else
                        Result := FieldByName('AC_CMUJER').AsFloat;
                    Exit;
               end;
               Next;
          end;
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.txtCEMPRESExit(Sender: TObject);
begin
     inherited;
     Edicion;
     with dmRecursos.cdsHisSGM do
     begin
          FieldByName('EP_CTITULA').AsFloat := ( FieldByName('EP_CFIJO').AsFloat + FieldByName('EP_CPOLIZA').AsFloat) - ( txtCEMPRES.Valor );
     end;
end;

procedure TEditHisSeguroGastosMedicos_DevEx.OK_DevExClick(Sender: TObject);
var
   Valor :TPesos;
begin
     with dmRecursos.cdsHisSGM do
     begin
          Valor := ( ( EP_CTITULA.Valor + txtCEMPRES.Valor ) - ( FieldByName('EP_CFIJO').AsFloat + FieldByName('EP_CPOLIZA').AsFloat) );
          if ( Valor <> 0 )THEN
          begin
               FieldByName('EP_CTITULA').FocusControl;
               DataBaseError('Capturar el Costo del Empleado � Empresa para repartir el Total a Pagar de la P�liza');
          end
          else
              inherited;
     end;

end;

end.
