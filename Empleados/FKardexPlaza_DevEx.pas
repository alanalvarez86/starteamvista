unit FKardexPlaza_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, FKardexBase_DevEx, DB, DBCtrls, Mask, ZetaFecha, ExtCtrls, StdCtrls,
  ZetaDBTextBox, ComCtrls, ZetaSmartLists, Buttons, 
  ZetaKeyCombo, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons, ZetaKeyLookup_DevEx;

type
  TKardexPlaza_DevEx = class(TKardexBase_DevEx)
    gbContratacion: TGroupBox;
    Label4: TLabel;
    Label26: TLabel;
    Label29: TLabel;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_PATRON: TZetaDBKeyLookup_DevEx;
    CB_PLAZA: TZetaDBKeyLookup_DevEx;
    PU_CODIGO: TZetaKeyLookup_DevEx;
    CB_PLAZALBL: TLabel;
    CB_PUESTOlbl: TLabel;
    Area_DevEx: TcxTabSheet;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_PLAZAExit(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure PU_CODIGOValidKey(Sender: TObject);
    procedure CB_NIVEL12ValidLookup(Sender: TObject);
    procedure CB_NIVEL11ValidLookup(Sender: TObject);
    procedure CB_NIVEL10ValidLookup(Sender: TObject);
    procedure CB_NIVEL9ValidLookup(Sender: TObject);
    procedure CB_NIVEL8ValidLookup(Sender: TObject);
    procedure CB_NIVEL7ValidLookup(Sender: TObject);
    procedure CB_NIVEL6ValidLookup(Sender: TObject);
    procedure CB_NIVEL5ValidLookup(Sender: TObject);
    procedure CB_NIVEL4ValidLookup(Sender: TObject);
    procedure CB_NIVEL3ValidLookup(Sender: TObject);
    procedure CB_NIVEL2ValidLookup(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    FOldPuesto: string;
    procedure SetCamposNivel;
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
  protected
    procedure Connect;override;
    procedure EscribirCambios;override;
  public
    { Public declarations }
  end;

var
  KardexPlaza_DevEx: TKardexPlaza_DevEx;

{$ifdef ACS}
const
     K_FORMA_ACS = 472;
{$endif}

implementation
uses
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaCommonTools,
    ZetaDialogo,
    ZAccesosTress,
    DRecursos,
    DGlobal,
    DTablas,
    DSistema,
    DCatalogos,
    DCliente;

{$R *.dfm}

procedure TKardexPlaza_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     Self.Height := K_FORMA_ACS;
     {
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     }
     {$endif}
     HelpContext:= H_Kardex_Plaza;
     TipoValorActivo1 := stEmpleado;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX; //Usa el mismo que los demas tipos de Kardex
     SetCamposNivel;

     PU_CODIGO.LookupDataset := dmCatalogos.cdsPuestos;
     PU_CODIGO.Filtro := 'PU_ACTIVO = ''S''';
     CB_PLAZA.LookupDataset := dmRecursos.cdsPlazasLookup;
     CB_PATRON.LookupDataset := dmCatalogos.cdsRPatron;
     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     {$ifdef ACS}
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     {$endif}
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;

      SetEditarSoloActivos;//@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigna en el DFM, Bug#15743
end;

procedure TKardexPlaza_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}

end;

procedure TKardexPlaza_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl_DevEx.ActivePage:= General_DevEx;
     PU_CODIGO.SetFocus;
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}
end;

procedure TKardexPlaza_DevEx.Connect;
begin
     with dmTablas do
     begin
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;

     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
     end;

     with dmSistema do
     begin
          cdsUsuarios.Conectar;
     end;

     with dmRecursos do
     begin
          //cdsPlazas.Conectar;
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
          if ( cdsEditHisKardex.State <> dsInsert ) then
              PU_CODIGO.LLave := cdsEditHisKardex.FieldByName('CB_PUESTO').AsString
          else
              PU_CODIGO.LLave := VACIO;
     end;
     CB_PLAZA.Filtro := dmRecursos.FiltroPlazaVacantes(PU_CODIGO.LLave,CB_PLAZA.Valor);
end;


procedure TKardexPlaza_DevEx.CB_PLAZAExit(Sender: TObject);
var
     lVisible: Boolean;
begin
     inherited;
     //Esto es para no mostrar informacion de una plaza
     //que no es la seleccionada
     lVisible := StrLleno(CB_PLAZA.Llave);
     gbContratacion.Visible := lVisible;
     Area_DevEx.Visible := lVisible;
end;

procedure TKardexPlaza_DevEx.EscribirCambios;
begin
     with dmRecursos do
     begin
{         // ER: 26/Sep: De momento no se procesaran cambios al tabulador del empleado cuando la plaza tiene otra clasificaci�n
          //             deber�n hacerlo disparando un cambio de puesto/clasificaci�n
          if ( FOldPuesto = cdsEditHisKardex.FieldByName( 'CB_PUESTO' ).AsString ) then
          begin
               if zStrToBool( cdsEditHisKardex.FieldByName( 'CB_AUTOSAL' ).AsString ) then
               begin
                    case ZSiNoCancel( 'Cambio de Clasificaci�n', 'Empleado tiene Tabulador de Salario. Desea Generar cambio de Salario ?', 0, mbYes ) of
                         mrYes :
                         begin
                              if ValidaSalario( cdsDatosEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat,
                                                GetSalClasifi( CB_CLASIFI.Llave ) ) then
                              begin
                                   AgregaCambioSalarioPlaza;

                                   //CambiosMultiples:= TRUE;
                                   //try
                                   //   OtrosDatos := VarArrayOf( [ CB_FECHA.Valor, CB_COMENTA.Text, CB_NOTA.Text] );
                                   //   with cdsDatosEmpleado do
                                   //   begin
                                   //        Edit;
                                   //        FieldByName( 'CB_PUESTO' ).AsString := PU_CODIGO.Llave;
                                   //        FieldByName( 'CB_CLASIFI' ).AsString := CB_CLASIFI.Llave;
                                   //        Enviar;
                                   //   end;
                                   //finally
                                   //  CambiosMultiples := FALSE;
                                   //end;
                                   //cdsEditHisKardex.MergeChangeLog;
                                   RefrescaHisKardex:= TRUE;
                                   RefrescaKardex( K_T_CAMBIO );      // Se posiciona y coloca sobre el Cambio de Salario Originado, adem�s refresca Percepciones
                                   if ( dmRecursos.cdsEditHisKardex.ChangeCount = 0 ) then    //Si se aplicaron los cambios
                                      Close;
                                   //inherited;                       // No se requiere grabar, ya se realiz� el cambio atrav�s de cdsDatosEmpleado
                              end;
                         end;
                         mrNo :
                         begin
                              cdsEditHisKardex.FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                              inherited;
                         end;
                    end;
                    // cancel equivale a no hacer el inherited.....
               end
               else
                   inherited;
          end
          else}
              inherited;
              CB_PLAZA.ResetMemory; //AP(24/Oct/2007): Cuando ya grabo es deseable que vuelva evaluar si la plaza esta disponible
     end;
end;


procedure TKardexPlaza_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if Assigned( DataSet ) and ( DataSet.State = dsBrowse ) then
             FOldPuesto := DataSet.FieldByName( 'CB_PUESTO' ).AsString;
     end;
end;

procedure TKardexPlaza_DevEx.PU_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     with dmRecursos do
     begin
//          PuestoPlaza := PU_CODIGO.Llave;
          if StrLleno( PU_CODIGO.Llave ) then
          begin
               CB_PLAZA.Filtro := dmRecursos.FiltroPlazaVacantes(PU_CODIGO.Llave, CB_PLAZA.Valor);
          end
          else if ( self.ActiveControl.Name <> 'Cancelar' ) then
          begin
               ZInformation(Caption, 'El Puesto no puede quedar vac�o',0 );
               PU_CODIGO.SetFocus;
          end;
     end;
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TKardexPlaza_DevEx.CB_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL11.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexPlaza_DevEx.CB_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TKardexPlaza_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 264;
     K_TOP_N1_LBL = 268;
     K_TOP_N2 = 240;
     K_TOP_N2_LBL = 244;
     K_TOP_N3 = 216;
     K_TOP_N3_LBL = 220;
     K_TOP_N4 = 192;
     K_TOP_N4_LBL = 196;
     K_TOP_N5 = 168;
     K_TOP_N5_LBL = 172;
     K_TOP_N6 = 144;
     K_TOP_N6_LBL = 148;
     K_TOP_N7 = 120;
     K_TOP_N7_LBL = 124;
     K_TOP_N8 = 96;
     K_TOP_N8_LBL = 100;
     K_TOP_N9 = 72;
     K_TOP_N9_LBL = 76;
     K_TOP_N10 = 48;
     K_TOP_N10_LBL = 52;
     K_TOP_N11 = 24;
     K_TOP_N11_LBL = 28;
     K_TOP_N12 = 0;
     K_TOP_N12_LBL = 4;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     {Se obtiene la cantidad total de niveles no visibles}
     iNivelesNoVisibles := 0;
    // iTotalHorizontal := 0;
     if Not CB_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     CB_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     CB_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     CB_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     CB_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     CB_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     CB_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     CB_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     CB_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     CB_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     CB_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     CB_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     CB_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     CB_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     CB_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     CB_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     CB_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     CB_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     CB_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     CB_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     CB_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     CB_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     CB_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     CB_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     CB_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TKardexPlaza_DevEx.LimpiaLookUpOlfKey;
begin
          CB_NIVEL1.ResetMemory;
          CB_NIVEL2.ResetMemory;
          CB_NIVEL3.ResetMemory;
          CB_NIVEL4.ResetMemory;
          CB_NIVEL5.ResetMemory;
          CB_NIVEL6.ResetMemory;
          CB_NIVEL7.ResetMemory;
          CB_NIVEL8.ResetMemory;
          CB_NIVEL9.ResetMemory;
          CB_NIVEL10.ResetMemory;
          CB_NIVEL11.ResetMemory;
          CB_NIVEL12.ResetMemory;
end;
{$endif}

procedure TKardexPlaza_DevEx.SetEditarSoloActivos;
begin
     PU_CODIGO.EditarSoloActivos := TRUE;
     CB_PLAZA.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     CB_PATRON.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;

end.

