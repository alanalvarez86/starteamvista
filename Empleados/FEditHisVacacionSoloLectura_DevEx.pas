unit FEditHisVacacionSoloLectura_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FEditHisVacacion_DevEx, Db, DBCtrls, Buttons, StdCtrls, ExtCtrls,
  Mask, ZetaDBTextBox,
  ComCtrls, ZetaKeyCombo, ZetaNumero, ZetaFecha,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, ZetaKeyLookup_DevEx,
  dxSkinsDefaultPainters, cxRadioGroup, dxBarBuiltInMenu;

type
  TEditHisVacacionSoloLectura_DevEx = class(TEditHisVacacion_DevEx)
    procedure RBSaldosClick(Sender: TObject);
    procedure VA_NOMNUMEValidKey(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetFiltroPeriodo;
    procedure SetInfoSaldos;
  protected
    procedure ImprimirForma;override;
    procedure Connect;override;
    procedure SetControls;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  EditHisVacacionSoloLectura_DevEx: TEditHisVacacionSoloLectura_DevEx;

implementation

uses dCatalogos,
     dSistema,
     dCliente,
     dRecursos,
     dGlobal,
     ZGlobalTress,
     ZImprimeForma,
     ZetaTipoEntidad,
     ZAccesosTress, ZetaCommonClasses, ZetaCommonLists,
     ZAccesosMgr;

{$R *.DFM}

procedure TEditHisVacacionSoloLectura_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmCatalogos.cdsPeriodo.Conectar;
     with dmRecursos do
     begin
          cdsHisVacacionSoloLectura.Conectar;
          DataSource.DataSet := cdsHisVacacionSoloLectura;
     end;
     SetFiltroPeriodo;
end;

procedure TEditHisVacacionSoloLectura_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
   TipoPer : eTipoPeriodo;
begin
     VA_NOMNUME.Filtro := VACIO;
     with DataSource.DataSet do
     begin
          iYear := FieldByName( 'VA_NOMYEAR' ).AsInteger;
          TipoPer := eTipoPeriodo( FieldByName( 'VA_NOMTIPO' ).AsInteger );
     end;
     with dmCatalogos do
     begin
          with cdsPeriodo do
          begin
               Filtered := False;
               if ( IsEmpty ) or
                  ( ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPer ) <> FieldByName( 'PE_TIPO' ).AsInteger ) ) then
               begin
                    GetDatosPeriodo( iYear, TipoPer );
                    // Para que se refresque el Control de Lookup:
                    with VA_NOMNUME do
                    begin
                         SetLlaveDescripcion( VACIO, VACIO );
                         Llave := DataSource.DataSet.FieldByName( 'VA_NOMNUME' ).AsString;
                    end;
               end;
          end;
          dmCatalogos.AplicaFiltroStatusPeriodo(VA_NOMNUME,True);
     end;
end;



procedure TEditHisVacacionSoloLectura_DevEx.SetInfoSaldos;
var
   rGozo, rPago, rPrima : TPesos;
   aFecha, bFecha : String;
begin
     if ( gbSaldos.Visible ) then
     begin
          with dmRecursos do
          begin
               SetSaldosVacaciones( RBAniversario.Checked, rGozo, rPago, rPrima );
          end;
          ZSaldoGozo.Caption := FormatFloat( '#,0.00', rGozo );
          ZSaldoPago.Caption := FormatFloat( '#,0.00', rPago );
          ZSaldoPrima.Caption := FormatFloat( '#,0.00', rPrima );

          aFecha := Copy(VA_FEC_INI.ValorAsText, 0, 5);
          bFecha := Copy(FormatDateTime('dd/mm/yy', dmRecursos.cdsEmpVacacion.FieldByName('CB_FEC_ANT').AsDateTime), 0, 5);

          with dmRecursos.cdsHisVacacionSoloLectura do
          begin
               if Global.GetGlobalBooleano( K_GLOBAL_PAGO_PRIMA_VACA_ANIV ) and (aFecha = bFecha) then
                  FieldByName('VA_P_PRIMA').AsString := FormatFloat( '#,0.00', rPrima )
               else
                   FieldByName('VA_P_PRIMA').AsString := FormatFloat( '#,0.00', 0 );
          end;
     end;
end;

procedure TEditHisVacacionSoloLectura_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enVacacion, dmRecursos.cdsHisVacacionSoloLectura );
end;

procedure TEditHisVacacionSoloLectura_DevEx.RBSaldosClick(Sender: TObject);
begin
     inherited;
     if ( TComponent( Sender ).Tag = 0 ) then
        RBFecha.Checked := FALSE
     else
         RBAniversario.Checked := FALSE;
     SetInfoSaldos;
end;

procedure TEditHisVacacionSoloLectura_DevEx.VA_NOMNUMEValidKey(Sender: TObject);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(VA_NOMNUME,True);
     inherited;
end;

procedure TEditHisVacacionSoloLectura_DevEx.SetControls;
var
   lHabilitar:Boolean;
begin
     if ( DataSource.State <> dsInsert )then
     begin
          lHabilitar := dmRecursos.PuedePagarVacaciones( VA_NOMNUME.Valor );
     end
     else
         lHabilitar := True;

     GBNominaPago.Enabled := lHabilitar;
     VA_PAGO.Enabled := lHabilitar;
     VA_MONTO.Enabled := lHabilitar;
     VA_P_PRIMA.Enabled := lHabilitar;
     VA_PRIMA.Enabled := lHabilitar;
     LblPago.Enabled := lHabilitar;
     L_VA_P_PRIMA.Enabled := lHabilitar;
     Label7.Enabled := lHabilitar;
     Label8.Enabled := lHabilitar;
     Label9.Enabled := lHabilitar;
end;
procedure TEditHisVacacionSoloLectura_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(VA_NOMNUME,False);
     inherited;
end;

procedure TEditHisVacacionSoloLectura_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dxBarButton_AgregarBtn.Enabled := FALSE;
     // dxBarButton_BorrarBtn.Enabled := FALSE;
     dxBarButton_ModificarBtn.Enabled := FALSE;
end;

function TEditHisVacacionSoloLectura_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= FALSE;
     sMensaje:= 'No es posible modificar vacaciones anteriores a la fecha de antigŁedad';
end;

end.
