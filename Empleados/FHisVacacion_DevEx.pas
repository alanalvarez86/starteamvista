unit FHisVacacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, Db,
  DBCtrls, StdCtrls, ZetaDBGrid, ExtCtrls, ZetaDBTextBox, ComCtrls, Buttons,
  dxSkinsCore,  TressMorado2013, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxPC, Menus, cxButtons,
  dxBarBuiltInMenu;

type
  THisVacacion_DevEx = class(TBaseConsulta)
    dsVacacion: TDataSource;
    dsSaldoVacaTotales: TDataSource;
    dsSaldoVacaciones: TDataSource;
    PageControl_DevEx: TcxPageControl;
    tabSaldo_DevEx: TcxTabSheet;
    GridSaldoVacaciones_DevEx: TZetaCXGrid;
    GridSaldoVacaciones_DevExDBTableView: TcxGridDBTableView;
    GridSaldoVacaciones_DevExLevel: TcxGridLevel;
    tabGozo_DevEx: TcxTabSheet;
    ZetaDBGrid1_DevEx: TZetaCXGrid;
    ZetaDBGrid1_DevExDBTableView: TcxGridDBTableView;
    ZetaDBGrid1Level_DevEx: TcxGridLevel;
    tabPago_DevEx: TcxTabSheet;
    ZetaDBGrid2_DevEx: TZetaCXGrid;
    ZetaDBGrid2_DevExDBTableView: TcxGridDBTableView;
    ZetaDBGrid2Level_DevEx: TcxGridLevel;
    tabPrima_DevEx: TcxTabSheet;
    VS_D_GOZO: TcxGridDBColumn;
    VS_GOZO: TcxGridDBColumn;
    VS_S_GOZO: TcxGridDBColumn;
    VS_D_PAGO: TcxGridDBColumn;
    VS_PAGO: TcxGridDBColumn;
    VS_S_PAGO: TcxGridDBColumn;
    VS_D_PRIMA: TcxGridDBColumn;
    VS_PRIMA: TcxGridDBColumn;
    VS_S_PRIMA: TcxGridDBColumn;
    VS_ANIV: TcxGridDBColumn;
    VS_FEC_VEN: TcxGridDBColumn;
    VS_CB_SAL: TcxGridDBColumn;
    Panel4: TPanel;
    GroupBox4: TGroupBox;
    Label29: TLabel;
    CB_FEC_ANT_SALDO: TZetaDBTextBox;
    Label27: TLabel;
    CB_TABLASS: TZetaDBTextBox;
    GroupBox6: TGroupBox;
    Label33: TLabel;
    Label37: TLabel;
    SaldoGozo: TZetaDBTextBox;
    SaldoPrima: TZetaDBTextBox;
    SaldoPago: TZetaDBTextBox;
    Label42: TLabel;
    Panel2: TPanel;
    GroupBox3: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    CB_DER_GOZ: TZetaDBTextBox;
    CB_V_GOZO: TZetaDBTextBox;
    Label5: TLabel;
    Label6: TLabel;
    CB_SUB_GOZ: TZetaDBTextBox;
    CB_PRO_GOZ: TZetaDBTextBox;
    CB_TOT_GOZ: TZetaDBTextBox;
    Label7: TLabel;
    Label11: TLabel;
    CB_PEN_GOZ: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    CB_FEC_VAC: TZetaDBTextBox;
    Label4: TLabel;
    CB_DER_FEC: TZetaDBTextBox;
    Antig: TLabel;
    CB_FEC_ANT: TZetaDBTextBox;
    Panel1: TPanel;
    GroupBox5: TGroupBox;
    Label30: TLabel;
    CB_FEC_VAC2: TZetaDBTextBox;
    Label31: TLabel;
    CB_DER_FEC2: TZetaDBTextBox;
    Label32: TLabel;
    CB_FEC_ANT2: TZetaDBTextBox;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    CB_DER_PAG: TZetaDBTextBox;
    CB_V_PAGO: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    CB_SUB_PAG: TZetaDBTextBox;
    CB_PRO_PAG: TZetaDBTextBox;
    CB_TOT_PAG: TZetaDBTextBox;
    CB_PEN_PAG: TZetaDBTextBox;
    Label13: TLabel;
    Label14: TLabel;
    Panel3: TPanel;
    GroupBox8: TGroupBox;
    Label51: TLabel;
    CB_FEC_VAC3: TZetaDBTextBox;
    Label52: TLabel;
    CB_DER_FEC3: TZetaDBTextBox;
    Label53: TLabel;
    CB_FEC_ANT3: TZetaDBTextBox;
    GroupBox9: TGroupBox;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    CB_DER_PV: TZetaDBTextBox;
    CB_V_PRIMA: TZetaDBTextBox;
    Label61: TLabel;
    Label62: TLabel;
    CB_SUB_PV: TZetaDBTextBox;
    CB_PRO_PV: TZetaDBTextBox;
    CB_TOT_PV: TZetaDBTextBox;
    CB_PEN_PV: TZetaDBTextBox;
    Label18: TLabel;
    Label19: TLabel;
    VA_FEC_INI: TcxGridDBColumn;
    VA_TIPO: TcxGridDBColumn;
    VA_YEAR: TcxGridDBColumn;
    VA_D_GOZO: TcxGridDBColumn;
    VA_GOZO: TcxGridDBColumn;
    VA_S_GOZO: TcxGridDBColumn;
    VA_GLOBAL: TcxGridDBColumn;
    VA_COMENTA: TcxGridDBColumn;
    VA_FEC_INI2: TcxGridDBColumn;
    VA_TIPO2: TcxGridDBColumn;
    VA_YEAR2: TcxGridDBColumn;
    VA_D_PAGO: TcxGridDBColumn;
    VA_PAGO: TcxGridDBColumn;
    VA_S_PAGO: TcxGridDBColumn;
    VA_GLOBAL2: TcxGridDBColumn;
    VA_COMENTA2: TcxGridDBColumn;
    ZetaDBGrid3_DevEx: TZetaCXGrid;
    ZetaDBGrid3_DevExDBTableView: TcxGridDBTableView;
    VA_FEC_INI3: TcxGridDBColumn;
    VA_TIPO3: TcxGridDBColumn;
    VA_YEAR3: TcxGridDBColumn;
    VA_D_PRIMA: TcxGridDBColumn;
    VA_P_PRIMA: TcxGridDBColumn;
    VA_S_PRIMA: TcxGridDBColumn;
    VA_GLOBAL3: TcxGridDBColumn;
    VA_COMENTA3: TcxGridDBColumn;
    ZetaDBGrid3Level_DevEx: TcxGridLevel;
    bbCalSubTotal_DevEx: TcxButton;
    bbCalSaldoActual_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridSaldoVacaciones_DevExDBTableViewDblClick(
      Sender: TObject);
    procedure ZetaDBGrid1_DevExDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGrid2_DevExDBTableViewDblClick(Sender: TObject);
    procedure tabGozo_DevExShow(Sender: TObject);
    procedure tabPago_DevExShow(Sender: TObject);
    procedure tabPrima_DevExShow(Sender: TObject);
    procedure tabSaldo_DevExShow(Sender: TObject);
    procedure bbCalSubTotal_DevExClick(Sender: TObject);
    procedure bbCalSaldoActual_DevExClick(Sender: TObject);
    procedure ZetaDBGrid1_DevExDBTableViewKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure ZetaDBGrid2_DevExDBTableViewKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure ZetaDBGrid3_DevExDBTableViewKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure GridSaldoVacaciones_DevExDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure GridSaldoVacaciones_DevExDBTableViewColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure ZetaDBGrid1_DevExDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGrid1_DevExDBTableViewColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure ZetaDBGrid2_DevExDBTableViewColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure ZetaDBGrid2_DevExDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGrid3_DevExDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGrid3_DevExDBTableViewColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure GridSaldoVacaciones_DevExDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGrid1_DevExDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGrid2_DevExDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGrid3_DevExDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
  private
    AColumn: TcxGridDBColumn;
    procedure ExportarTabActivo(TabActivo: TcxTabSheet );
    procedure SetSettingsGridsDevEx;
    function ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
    function DefineGridActivo:TcxGridDBTableView;
    //DevEx: Metodo agregado para aplicar minwidth a todas las col. de los grids
    procedure ApplyMinWidth (ZetaDBGridDBTableView: TcxGridDBTableView);
  protected
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure ImprimirForma;override;
    procedure Exportar;override;
    procedure Imprimir;override;
  public
    { Public declarations }
  end;

var
  HisVacacion_DevEx: THisVacacion_DevEx;

const
     K_COLUMNA_SALARIO = 11;

implementation

uses dSistema,
{$ifdef SUPERVISORES}
     dSuper,
{$else}
     dRecursos,
     ZImprimeForma,
     ZetaTipoEntidad,
     DAsistencia,{OP: 10/06/08}
{$endif}
     ZetaClientDataSet, ZetaCommonLists, ZetaCommonClasses,
     ZetaDialogo, DGlobal, ZGlobalTress,ZAccesosMgr,ZAccesosTress,FBaseReportes_DevEx, ZGridModeTools;

{$R *.DFM}

procedure THisVacacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
     GridSaldoVacaciones_DevExDBTableView.DataController.DataModeController.GridMode:= True;
     GridSaldoVacaciones_DevExDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGrid1_DevExDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaDBGrid1_DevExDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGrid2_DevExDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaDBGrid2_DevExDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ZetaDBGrid3_DevExDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaDBGrid3_DevExDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     {***}
{$ifdef SUPERVISORES}
     HelpContext:= H00504_Vacaciones;
{$else}
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10132_Historial_vacaciones;
{$endif}
end;

procedure THisVacacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
      //***DevEx (by am):Especificaciones de diseno para los grids***
     SetSettingsGridsDevEx;
     
     if Global.GetGlobalBooleano(K_GLOBAL_VER_SALDO_VACACION) then
     begin
          TabSaldo_DevEx.TabVisible := true;
          PageControl_DevEx.ActivePage := TabSaldo_DevEx;

     end
     else
     begin
          TabSaldo_DevEx.TabVisible := false;
          PageControl_DevEx.ActivePage := TabGozo_DevEx;
     end;
     GridSaldoVacaciones_DevExDBTableView.Columns[VS_CB_SAL.Index].Visible := {$ifdef SUPERVISORES} False {$else}ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );{$endif}
end;

procedure  THisVacacion_DevEx.SetSettingsGridsDevEx;
begin
     //Desactiva la posibilidad de agrupar
     GridSaldoVacaciones_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     ZetaDBGrid1_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     ZetaDBGrid2_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     ZetaDBGrid3_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     GridSaldoVacaciones_DevExDBTableView.OptionsView.GroupByBox := False;
     ZetaDBGrid1_DevExDBTableView.OptionsView.GroupByBox := False;
     ZetaDBGrid2_DevExDBTableView.OptionsView.GroupByBox := False;
     ZetaDBGrid3_DevExDBTableView.OptionsView.GroupByBox := False;
     //Para que no se pueda filtrar, es necesario por el cambio a GridMode
     //GridSaldoVacaciones_DevExDBTableView.OptionsCustomize.ColumnFiltering := False;
     //ZetaDBGrid1_DevExDBTableView.OptionsCustomize.ColumnFiltering := False;
     //ZetaDBGrid2_DevExDBTableView.OptionsCustomize.ColumnFiltering := False;
     //ZetaDBGrid3_DevExDBTableView.OptionsCustomize.ColumnFiltering := False;
     //Para que nunca muestre el filterbox inferior
     GridSaldoVacaciones_DevExDBTableView.FilterBox.Visible := fvNever;
     ZetaDBGrid1_DevExDBTableView.FilterBox.Visible := fvNever;
     ZetaDBGrid2_DevExDBTableView.FilterBox.Visible := fvNever;
     ZetaDBGrid3_DevExDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     GridSaldoVacaciones_DevExDBTableView.FilterBox.CustomizeDialog := False;
     ZetaDBGrid1_DevExDBTableView.FilterBox.CustomizeDialog := False;
     ZetaDBGrid2_DevExDBTableView.FilterBox.CustomizeDialog := False;
     ZetaDBGrid3_DevExDBTableView.FilterBox.CustomizeDialog := False;
     //Para que ponga el MinWidth ideal dependiendo del texto presente en las columnas.
     ApplyMinWidth(GridSaldoVacaciones_DevExDBTableView);
     ApplyMinWidth(ZetaDBGrid1_DevExDBTableView);
     ApplyMinWidth(ZetaDBGrid2_DevExDBTableView);
     ApplyMinWidth(ZetaDBGrid3_DevExDBTableView);
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     GridSaldoVacaciones_DevExDBTableView.ApplyBestFit();
     ZetaDBGrid1_DevExDBTableView.ApplyBestFit();
     ZetaDBGrid2_DevExDBTableView.ApplyBestFit();
     ZetaDBGrid3_DevExDBTableView.ApplyBestFit();
end;

procedure THisVacacion_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
{$ifdef SUPERVISORES}
     with dmSuper do

{$else}
     with dmRecursos do
{$endif}
     begin
          cdsHisVacacion.Conectar;
          dsVacacion.DataSet := cdsHisVacacion;
          DataSource.DataSet := cdsEmpVacacion;
          dsSaldoVacaTotales.DataSet := cdsSaldoVacacionesTotales;
          dsSaldoVacaciones.DataSet  := cdsSaldoVacaciones;
     end;
      DefineGridActivo.ApplyBestFit();
end;

procedure THisVacacion_DevEx.Refresh;
begin
     TZetaClientDataSet( dsVacacion.DataSet ).Refrescar;
{
     with dmRecursos do
     begin
          cdsHisVacacion.Refrescar;
     end;
}
      DefineGridActivo.ApplyBestFit();
end;

procedure THisVacacion_DevEx.Agregar;
begin
     TZetaClientDataSet( dsVacacion.DataSet ).Agregar;
{
     dmRecursos.cdsHisVacacion.Agregar;
}
      DefineGridActivo.ApplyBestFit();
end;

procedure THisVacacion_DevEx.Borrar;
begin
{
     with dmRecursos.cdsHisVacacion do
}
     with TZetaClientDataSet( dsVacacion.DataSet ) do
     begin
          if ( RecordCount > 0 ) then
             Borrar
          else
              zInformation( Caption, 'No Hay Datos Para Borrar', 0 );
     end;
     DefineGridActivo.ApplyBestFit();
end;

procedure THisVacacion_DevEx.Modificar;
begin
{
     with dmRecursos.cdsHisVacacion do
}
     if ( PageControl_DevEx.ActivePage <> tabSaldo_DevEx )then
     begin
          with TZetaClientDataSet( dsVacacion.DataSet ) do
          begin
               if ( RecordCount > 0 ) then
                  Modificar
               else
                  zInformation( Caption, 'No Hay Datos Para Modificar', 0 );
          end;
     end;
     DefineGridActivo.ApplyBestFit();
end;

procedure THisVacacion_DevEx.ImprimirForma;
begin
{$ifndef SUPERVISORES}
     ZImprimeForma.ImprimeUnaForma( enVacacion, dmRecursos.cdsHisVacacion );
{$endif}
end;

procedure THisVacacion_DevEx.Exportar;
begin
     //inherited;
     //ExportarTabActivo( PageControl1.ActivePage );  //REMOVER
     ExportarTabActivo( PageControl_DevEx.ActivePage );
end;

function THisVacacion_DevEx.ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
var
i: Integer;
oGrid: TDBGrid;
begin
//Creamos un TDBGrid Virtual
          oGrid := TDBGrid.Create(Self);
          for i := 0 to DevEx_TableView.ColumnCount -1 do
          begin

               with oGrid.Columns.Add do
               begin
                    Visible       := DevEx_TableView.Columns[ i ].Visible;
                    FieldName     := DevEx_TableView.Columns[ i ].DataBinding.FieldName;
                    Title.Caption := DevEx_TableView.Columns[ i ].Caption;
               end;
          end;
          //if (oGrid.Columns.Count >0) then
             oGrid.DataSource :=  DevEx_TableView.DataController.DataSource;
          //else
             //oGrid := nil;
          result := oGrid;
end;

procedure THisVacacion_DevEx.ExportarTabActivo( TabActivo :TcxTabSheet );
var
    lExporta: Boolean;
    i : integer;
    Valor : Variant;
    oGrid: TDBGrid;
    AGridDBTableView: TcxGridDBTableView ;
begin
     lExporta:= FALSE;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          //if ( Components[ i ] is TDBGrid ) and (  TDBGrid(Components[ i ]).CanFocus ) then
          //AGridDBTableView:= nil;
          if (Components[i] is TcxGrid) and (Components[i] as TcxGrid).CanFocus then
          begin
               AGridDBTableView:=TcxGridDBTableView((Components[i] as TcxGrid).Views[0]);
               oGrid := nil;
               lExporta:= TRUE;

               if  AGridDBTableView <> nil then
               begin
                    if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + TabActivo.Caption + ' ?', 0, mbYes ) then
                    begin
                         Valor := ValoresGrid;
                         try
                            oGrid := ConvierteATDBGrid ( AGridDBTableView );
                            //if oGrid <> nil then
                            FBaseReportes_DevEx.ExportarGrid( oGrid, oGrid.DataSource.DataSet, Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
                         finally
                            oGrid.Free;
                         end;
                    end;
               end;
               Break;
          end;
     end;

     if not lExporta then
        zInformation( 'Exportar...',Format( 'La Pantalla %s no Contiene Ning�n Grid para Exportar', [Caption] ), 0);
end;

procedure THisVacacion_DevEx.Imprimir;
 var lImprime : Boolean;
     i : integer;
     Valor : Variant;
     oGrid: TDBGrid;
     AGridDBTableView: TcxGridDBTableView ;
begin
     lImprime := FALSE;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          //if ( Components[ i ] is TDBGrid ) and (  TDBGrid(Components[ i ]).CanFocus ) then
          //AGridDBTableView:= nil;
          if (Components[i] is TcxGrid) and (Components[i] as TcxGrid).CanFocus then
          begin
               lImprime := TRUE;
               AGridDBTableView:=TcxGridDBTableView((Components[i] as TcxGrid).Views[0]);
               oGrid := nil;

               if  AGridDBTableView <> nil then
               begin
                     if zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De La Pantalla ' + PageControl_DevEx.ActivePage.Caption + ' ?', 0, mbYes ) then
                    begin
                         Valor := ValoresGrid;
                         try
                            oGrid := ConvierteATDBGrid ( AGridDBTableView );
                            //if oGrid <> nil then
                            FBaseReportes_DevEx.ImprimirGrid( oGrid, oGrid.DataSource.DataSet , Caption, 'IM',
                                                Valor[0],Valor[1],Valor[2],Valor[3]);
                         finally
                            oGrid.Free;
                         end;
                    end;
               end;
               Break;
          end;
     end;
     if not lImprime AND
        zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

{***DevEx (by am): Metodos OnclickColumnHeader y OnSortingChanged para los grids, son necesarios por el cambio
                   a gridmode.***}
procedure THisVacacion_DevEx.GridSaldoVacaciones_DevExDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure THisVacacion_DevEx.GridSaldoVacaciones_DevExDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if GridSaldoVacaciones_DevExDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( GridSaldoVacaciones_DevExDBTableView, AItemIndex, AValueList );
end;

procedure THisVacacion_DevEx.GridSaldoVacaciones_DevExDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  GridSaldoVacaciones_DevEx.OrdenarPor( AColumn , TZetaClientDataset(dsSaldoVacaciones.DataSet) );
end;

procedure THisVacacion_DevEx.ZetaDBGrid1_DevExDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure THisVacacion_DevEx.ZetaDBGrid1_DevExDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGrid1_DevExDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGrid1_DevExDBTableView, AItemIndex, AValueList );
end;

procedure THisVacacion_DevEx.ZetaDBGrid1_DevExDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaDBGrid1_DevEx.OrdenarPor( AColumn , TZetaClientDataset(dsVacacion.DataSet) );
end;

procedure THisVacacion_DevEx.ZetaDBGrid2_DevExDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure THisVacacion_DevEx.ZetaDBGrid2_DevExDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGrid2_DevExDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGrid2_DevExDBTableView, AItemIndex, AValueList );
end;

procedure THisVacacion_DevEx.ZetaDBGrid2_DevExDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
   {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaDBGrid2_DevEx.OrdenarPor( AColumn , TZetaClientDataset(dsVacacion.DataSet) );
end;

procedure THisVacacion_DevEx.ZetaDBGrid3_DevExDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure THisVacacion_DevEx.ZetaDBGrid3_DevExDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGrid3_DevExDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGrid3_DevExDBTableView, AItemIndex, AValueList );
end;

procedure THisVacacion_DevEx.ZetaDBGrid3_DevExDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaDBGrid3_DevEx.OrdenarPor( AColumn , TZetaClientDataset(dsVacacion.DataSet) );
end;

{***DevEx (by am): Metodos DblClick para los grids***}
procedure THisVacacion_DevEx.GridSaldoVacaciones_DevExDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure THisVacacion_DevEx.ZetaDBGrid1_DevExDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure THisVacacion_DevEx.ZetaDBGrid2_DevExDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
end;

{***DevEx (by am): Metodos agregado para definir el tab activo y aplicar el BestFit a los grids***}
function THisVacacion_DevEx.DefineGridActivo:TcxGridDBTableView;
begin
     case PageControl_DevEx.ActivePageIndex of
         0: Result:= GridSaldoVacaciones_DevExDBTableView;
         1: Result:= ZetaDBGrid1_DevExDBTableView;
         2: Result:= ZetaDBGrid2_DevExDBTableView;
         3: Result:= ZetaDBGrid3_DevExDBTableView;
         else
            Result:= GridSaldoVacaciones_DevExDBTableView;
     end;
end;

procedure THisVacacion_DevEx.tabGozo_DevExShow(Sender: TObject);
begin
  inherited;
  ZetaDBGrid1_DevExDBTableView.ApplyBestFit();
end;

procedure THisVacacion_DevEx.tabPago_DevExShow(Sender: TObject);
begin
  inherited;
  ZetaDBGrid2_DevExDBTableView.ApplyBestFit();
end;

procedure THisVacacion_DevEx.tabPrima_DevExShow(Sender: TObject);
begin
  inherited;
  ZetaDBGrid3_DevExDBTableView.ApplyBestFit();
end;

procedure THisVacacion_DevEx.tabSaldo_DevExShow(Sender: TObject);
begin
  inherited;
  GridSaldoVacaciones_DevExDBTableView.ApplyBestFit();
end;

//DevEx: Metodo agregado para aplicar minWidth a cada columna de los grids
procedure THisVacacion_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

procedure THisVacacion_DevEx.bbCalSubTotal_DevExClick(Sender: TObject);
begin
  inherited;
   {$ifdef SUPERVISORES}
   dmSuper.MuestraCalendarioVacacion( DataSource.DataSet.FieldByName( 'CB_SUB_GOZ' ).AsFloat );
   {$else}
   dmAsistencia.MuestraCalendarioVacacion( DataSource.DataSet.FieldByName( 'CB_SUB_GOZ' ).AsFloat );
   {$endif}
end;

procedure THisVacacion_DevEx.bbCalSaldoActual_DevExClick(Sender: TObject);
begin
  inherited;
  {$ifdef SUPERVISORES}
  dmSuper.MuestraCalendarioVacacion( DataSource.DataSet.FieldByName( 'CB_TOT_GOZ' ).AsFloat );
  {$else}
  dmAsistencia.MuestraCalendarioVacacion( DataSource.DataSet.FieldByName( 'CB_TOT_GOZ' ).AsFloat );
  {$endif}
end;

procedure THisVacacion_DevEx.ZetaDBGrid1_DevExDBTableViewKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
   DoEdit;
  end;
end;

procedure THisVacacion_DevEx.ZetaDBGrid2_DevExDBTableViewKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
   DoEdit;
  end;
end;

procedure THisVacacion_DevEx.ZetaDBGrid3_DevExDBTableViewKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
   DoEdit;
  end;
end;

end.

