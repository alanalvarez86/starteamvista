unit FMatrizCursos_DevEx;



interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, db,
  ZetaDBGrid, ExtCtrls, StdCtrls, Buttons, ZetaKeyCombo, ZetaKeyLookup,
  ComCtrls, ZetaDBTextBox,ZetaCommonClasses,Variants, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxClasses,
  cxCustomData, cxStyles, cxEdit, cxCustomPivotGrid, cxDBPivotGrid,
  dxmdaset, cxPivotGridCustomDataSet, cxPivotGridSummaryDataSet,cxExportPivotGridLink,
  cxLocalization, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ZetaKeyLookup_DevEx, Menus, cxButtons, ZetaDialogo, Math, ImgList,
  Vcl.Mask,
  ZetaNumero, ZetaFecha, ZetaClientDataSet;

type
  TMatrizCursos_DevEx = class(TBaseConsulta)
    Panel1: TPanel;
    PageControl1: TPageControl;
    cxStyleRepRojo: TcxStyleRepository;
    dbPivoteMatrizCursos: TcxDBPivotGrid;
    CB_CODIGO: TcxDBPivotGridField;
    PRETTYNAME: TcxDBPivotGridField;
    CU_NOMBRE: TcxDBPivotGridField;
    CT_ELEMENT: TcxDBPivotGridField;
    TOMADO: TcxDBPivotGridField;
    PU_DESCRIP: TcxDBPivotGridField;
    Nivel_1: TcxDBPivotGridField;
    Nivel_2: TcxDBPivotGridField;
    Nivel_3: TcxDBPivotGridField;
    Nivel_4: TcxDBPivotGridField;
    Nivel_5: TcxDBPivotGridField;
    Nivel_6: TcxDBPivotGridField;
    Nivel_7: TcxDBPivotGridField;
    Nivel_8: TcxDBPivotGridField;
    Nivel_9: TcxDBPivotGridField;
    SaveDialog1: TSaveDialog;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxLocalizer1: TcxLocalizer;
    popRegistroCap: TPopupMenu;
    EvaluarCompetencias1: TMenuItem;
    cxImageList_MenuContextual: TcxImageList;
    CC_ELEMENT: TcxDBPivotGridField;
    VerHistorial1: TMenuItem;
    KC_FEC_PRO: TcxDBPivotGridField;
    Panel2: TPanel;
    PanelTomado: TPanel;
    Label3: TLabel;
    LblTomado: TLabel;
    LblProgramado: TLabel;
    PanelProgramado: TPanel;
    LblVencido: TLabel;
    PanelVencido: TPanel;
    LblAdvertencia: TLabel;
    PanelAdvertencia: TPanel;
    KC_FEC_TOM: TcxDBPivotGridField;
    TU_DESCRIP: TcxDBPivotGridField;
    PanelOpciones: TPanel;
    PanelMostrar: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    CBMostrarFecha: TCheckBox;
    ZN_Dias: TZetaNumero;
    FechaReferencia: TZetaFecha;
    btnEvaluarSeleccion_DevEx: TcxButton;
    BtnVerHistorial_DevEx: TcxButton;
    PanelFiltros: TPanel;
    LblLookup1: TLabel;
    LblLookup3: TLabel;
    LblLookup2: TLabel;
    Lookup4: TZetaKeyLookup_DevEx;
    Lookup3: TZetaKeyLookup_DevEx;
    Lookup1: TZetaKeyLookup_DevEx;
    CBOpcionales: TCheckBox;
    LblLookup4: TLabel;
    Lookup2: TZetaKeyLookup_DevEx;
    CBSoloSTPS: TCheckBox;
    RefrescarCursos: TcxButton;
    BtnConfigurar: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure TOMADOCalculateCustomSummary(Sender: TcxPivotGridField; ASummary: TcxPivotGridCrossCellSummary);
    procedure dbPivoteMatrizHabilidadesStylesGetContentStyle(Sender: TcxCustomPivotGrid; ACell: TcxPivotGridDataCellViewInfo;var AStyle: TcxStyle);
    procedure Lookup1ValidKey(Sender: TObject);
    procedure btnCapturarCursos_DevExClick(Sender: TObject);
    procedure RefrescarCursosClick(Sender: TObject);
    procedure dbPivoteMatrizCursosCustomDrawCell(Sender: TcxCustomPivotGrid; ACanvas: TcxCanvas; AViewInfo: TcxPivotGridDataCellViewInfo; var ADone: Boolean);
    procedure CBMostrarFechaClick(Sender: TObject);
    procedure btnVerHistorial(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TOMADOGetDisplayText(Sender: TcxPivotGridField; ACell: TcxPivotGridDataCellViewInfo; var AText: string);
    procedure BtnConfigurarClick(Sender: TObject);
  private
    { Private declarations }
    FStyleRojo: TcxStyle;
    FStyleVerde: TcxStyle;
    FStyleAmarillo: TcxStyle;
    FStyleAzul: TcxStyle;
    function GetLookupEtiqueta(const iPos: Integer): String;
    function GetLookupParametroName(const iPos: Integer): String;
    function EvaluarCurso(var iValidaCurso, iPosX, iPosY: Integer; var sCurso, sCursoNombre : String) : Boolean;
    procedure SetFiltroNivel;
    procedure InitColoresCeldas;
    procedure AsignaParametros;
    procedure VerHistorialCursos(iTipo : Integer);
    procedure ConfigurarFiltros;
    procedure IniciaControlesLookup;
    procedure ConectaLookupsDataSets;
  protected
    procedure Connect; override;
    procedure Disconnect; override;
    procedure Refresh; override;
    procedure Exportar;override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
   MatrizCursos_DevEx: TMatrizCursos_DevEx;

implementation

{$R *.DFM}

uses {$IFDEF SUPERVISORES}dSuper{$ELSE}dRecursos{$ENDIF}, dCatalogos, dTablas, dSistema, dGlobal, DCliente,
     ZetaCommonLists, ZetaCommonTools, ZAccesosTress, ZAccesosMgr, ZGlobalTress, ZBaseDlgModal_DevEx,
     FVerHistorialMatrizCursos_DevEx, FConfigFiltroCursos, ZetaRegistryCliente;

const
     K_COLOR_ROJO = $8C92F7;
     K_COLOR_VERDE = $9FFFB8;
     K_COLOR_AMARILLO = $00BFFF;
     K_COLOR_AZUL = $FFA500;
     K_DIAS_ADVERTIR = 30;
     K_EVALUAR_CURSOS = 1;
     K_VER_HISTORIAL = 2;
     K_COL_FEC_PROG = 17;
     K_COL_FEC_TOMADO = 18;

procedure TMatrizCursos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= {$IFDEF SUPERVISORES} H_CAP_MATRIZ_CURSOS_SUPER {$ELSE} H_CAP_MATRIZ_CURSOS {$ENDIF};
     IndexDerechos := D_CAP_MATRIZ_CURSOS;
     IniciaControlesLookup;
     SetFiltroNivel;
     cxLocalizer1.Active := TRUE;
     cxLocalizer1.Locale := 2058;  // Cardinal para Espanol (Mexico)
     CBMostrarFecha.Checked := TRUE;
     ZN_Dias.Valor:= K_DIAS_ADVERTIR;
     FechaReferencia.Valor := Date();
     InitColoresCeldas;
end;

procedure TMatrizCursos_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( VerHistorialMatrizCursos_DevEx );
     FreeAndNil( FStyleRojo );
     FreeAndNil( FStyleVerde );
     FreeAndNil( FStyleAmarillo );
     FreeAndNil( FStyleAzul );
end;

procedure TMatrizCursos_DevEx.Connect;
begin
     if ( not Assigned( DataSource.DataSet ) ) then
        DataSource.DataSet:= {$IFDEF SUPERVISORES}dmSuper{$else}dmRecursos{$endif}.cdsMatrizCursos;
     ConectaLookupsDataSets;
     {$IFDEF SUPERVISORES}
     dmSuper.FiltroMatCursos.AddDate('FechaSup',dmCliente.FechaSupervisor );
     {$ENDIF}
end;

procedure TMatrizCursos_DevEx.Disconnect;
begin
     //inherited;     No se quiere que se desconect el dataset
end;

procedure TMatrizCursos_DevEx.Refresh;
begin
     inherited;
     AsignaParametros;
     with {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsMatrizCursos do
     begin
          Refrescar;
          if IsEmpty then
             zInformation( self.Caption, 'No hay registros de cursos programados que cumplan con el criterio de b�squeda', 0 );
     end;
end;

function TMatrizCursos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se Puede Borrar';
     Result := False;
end;

function TMatrizCursos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No Se puede Modificar';
     Result := False;
end;

function TMatrizCursos_DevEx.PuedeAgregar(var sMensaje: String ): Boolean;
begin
     sMensaje := 'No Se puede Agregar';
     Result := False;
end;

procedure TMatrizCursos_DevEx.Exportar;
begin
     if SaveDialog1.Execute then
     begin
          cxExportPivotGridToExcel( SaveDialog1.FileName, dbPivoteMatrizCursos );
     end;
end;

procedure TMatrizCursos_DevEx.CBMostrarFechaClick(Sender: TObject);
begin
     inherited;
     AsignaParametros;
     dbPivoteMatrizCursos.Refresh;
end;

procedure TMatrizCursos_DevEx.RefrescarCursosClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TMatrizCursos_DevEx.BtnConfigurarClick(Sender: TObject);
begin
     inherited;
     ConfigurarFiltros;
end;

procedure TMatrizCursos_DevEx.Lookup1ValidKey(Sender: TObject);
begin
     inherited;
     AsignaParametros;
end;

procedure TMatrizCursos_DevEx.TOMADOCalculateCustomSummary(Sender: TcxPivotGridField;
          ASummary: TcxPivotGridCrossCellSummary);
begin
     inherited;
     ASummary.Custom := (ASummary.Sum / ASummary.Count);
end;

procedure TMatrizCursos_DevEx.TOMADOGetDisplayText(Sender: TcxPivotGridField; ACell: TcxPivotGridDataCellViewInfo;
          var AText: string);
begin
     inherited;
     if ACell.IsTotal and ( not VarIsNull( ACell.Value ) ) then
        AText := FormatFloat( '0.00%', ACell.Value * 100 )
     else if not ( ACell.IsTotal ) and ( not VarIsNull( ACell.Value ) ) then
        AText := FormatFloat( '0', ACell.Value );
end;

procedure TMatrizCursos_DevEx.dbPivoteMatrizCursosCustomDrawCell( Sender: TcxCustomPivotGrid; ACanvas: TcxCanvas;
          AViewInfo: TcxPivotGridDataCellViewInfo; var ADone: Boolean);
var
   aRecords: TcxPivotGridRecords;
   dFecha: TDate;
begin
     if ( CBMostrarFecha.Checked ) then
     begin
          if ( not AViewInfo.IsTotal ) or ( not AViewInfo.IsGrandTotal ) then
          begin
               if ( AViewInfo.Row.Level > 0 ) then
               begin
                    aRecords := AViewInfo.CrossCell.Records;
                    if ( aRecords.Count > 0 ) then
                    begin
                         if VarIsNull( Sender.DataController.Values[ ARecords.Items[0], K_COL_FEC_TOMADO ] ) then   // No ha tomado el curso - Se revisa solo el primer registro (No debe haber mas de 1)
                         begin
                              dFecha := Sender.DataController.Values[ ARecords.Items[0], K_COL_FEC_PROG ];
                              if ( aViewInfo.Selected ) then
                                 ACanvas.FillRect( AViewInfo.Bounds, clDefault )
                              else if ( dFecha < FechaReferencia.Valor ) then
                                 ACanvas.FillRect( AViewInfo.Bounds, TColor( K_COLOR_ROJO ) )
                              else if ( ( dFecha - ZN_Dias.Valor ) < FechaReferencia.Valor ) then
                                 ACanvas.FillRect( AViewInfo.Bounds, TColor( K_COLOR_AMARILLO ) )
                              else if ( AViewInfo.Value = 0 ) then
                                 ACanvas.FillRect( AViewInfo.Bounds, TColor( K_COLOR_AZUL ) )
                              else
                                 ACanvas.FillRect( AViewInfo.Bounds, TColor( K_COLOR_VERDE ) );
                              ACanvas.DrawText( ZetaCommonTools.FechaCorta( dFecha ), AViewInfo.Bounds, cxAlignHCenter or cxAlignVCenter );
                              ADone := True;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TMatrizCursos_DevEx.dbPivoteMatrizHabilidadesStylesGetContentStyle(Sender: TcxCustomPivotGrid;
          ACell: TcxPivotGridDataCellViewInfo; var AStyle: TcxStyle);
var
   aRecords: TcxPivotGridRecords;
   dFecha: TDate;
begin
     inherited;
     if ( not ACell.IsTotal ) or ( not ACell.IsGrandTotal ) then
     begin
          if ( ACell.Value > 0 ) then
            AStyle := FStyleVerde;
          if ( ACell.Row.Level > 0 ) then   //Rengl�n de empleado
          begin
               aRecords := ACell.CrossCell.Records;
               if ( aRecords.Count > 0 ) then
               begin
                    if VarIsNull( Sender.DataController.Values[ ARecords.Items[0], K_COL_FEC_TOMADO ] ) then   // No ha tomado el curso - Se revisa solo el primer registro (No debe haber mas de 1)
                    begin
                         dFecha := Sender.DataController.Values[ ARecords.Items[0], K_COL_FEC_PROG ];
                         if ( dFecha < FechaReferencia.Valor ) then
                            AStyle := FStyleRojo
                         else if ( ( dFecha - ZN_Dias.Valor ) < FechaReferencia.Valor ) then
                            AStyle := FStyleAmarillo
                         else if ( ACell.Value = 0 ) then
                            AStyle := FStyleAzul;
                    end;
               end;
          end;
     end;
end;

procedure TMatrizCursos_DevEx.btnVerHistorial(Sender: TObject);
begin
     inherited;
     {$IFDEF SUPERVISORES}
     if CheckDerecho( D_SUPER_CURSOS_TOMADOS, K_DERECHO_CONSULTA ) then
     {$else}
     if CheckDerecho( D_EMP_EXP_CURSOS_TOMADOS, K_DERECHO_CONSULTA ) then
     {$endif}
     begin
          if ( dbPivoteMatrizCursos.ViewData.Selection.Count > 0 ) then
          begin
               VerHistorialCursos( K_VER_HISTORIAL );
          end
          else
              ZetaDialogo.ZInformation(Self.Caption,'Seleccionar una celda del plan de cursos programados.',0);
     end
     else
         ZetaDialogo.zInformation( Self.Caption,'No tiene derechos de consulta en el historial de cursos tomados.',0 );
end;

procedure TMatrizCursos_DevEx.btnCapturarCursos_DevExClick(Sender: TObject);
begin
     inherited;
     {$IFDEF SUPERVISORES}
     if CheckDerecho( D_SUPER_CURSOS_TOMADOS, K_DERECHO_ALTA ) then
     {$else}
     if CheckDerecho( D_EMP_EXP_CURSOS_TOMADOS, K_DERECHO_ALTA ) then
     {$endif}
     begin
          if ( dbPivoteMatrizCursos.ViewData.Selection.Count > 0 ) then
          begin
               VerHistorialCursos( K_EVALUAR_CURSOS );
          end
          else
              ZetaDialogo.ZInformation(Self.Caption,'Seleccionar una celda del plan de cursos programados.',0);
     end
     else
         ZetaDialogo.zInformation( Self.Caption,'No tiene derechos de agregar en el historial de cursos tomados.',0 );
end;

procedure TMatrizCursos_DevEx.VerHistorialCursos(iTipo : Integer);
var
   iEmpleado, iPosX, iPosY, iValidaCurso, iCuantos: Integer;
   sCurso, sCursoNombre: String;

   function GetHisCursos: TZetaClientDataSet;
   begin
        Result := {$IFDEF SUPERVISORES}dmSuper{$else}dmRecursos{$endif}.cdsHisCursos;
   end;

begin
     inherited;
     iEmpleado := dmCliente.Empleado;
     if EvaluarCurso(iValidaCurso, iPosX, iPosY, sCurso, sCursoNombre) then
     begin
          if ( iTipo = K_VER_HISTORIAL ) then
          begin
               with GetHisCursos do
               begin
                    Refrescar;
                    Filtered := False;
                    Filter := Format('CU_CODIGO = ''%s'' ', [sCurso]);
                    Filtered := True;
                    iCuantos := RecordCount;
               end;
               if ( iCuantos = 0 ) then
                  ZetaDialogo.ZInformation(Self.Caption,'No se encontr� historial del curso seleccionado.',0)
               else
               begin
                    if VerHistorialMatrizCursos_DevEx = nil then
                       VerHistorialMatrizCursos_DevEx:= TVerHistorialMatrizCursos_DevEx.Create(Self);

                    VerHistorialMatrizCursos_DevEx.dsEvaluacionMult.DataSet := GetHisCursos;
                    VerHistorialMatrizCursos_DevEx.FNombre := dmCliente.GetEmpleadoDescripcion;
                    VerHistorialMatrizCursos_DevEx.FCurso := Format('%s : %s', [ sCurso, sCursoNombre ]);

                    VerHistorialMatrizCursos_DevEx.ShowModal;
               end;

               GetHisCursos.Filtered := False;
          end
          else if ( iTipo = K_EVALUAR_CURSOS ) then
          begin
               if iValidaCurso = 0 then
               begin
                    GetHisCursos.Conectar;
                    {$IFNDEF SUPERVISORES}
                    dmRecursos.FUltimoCurso := sCurso;
                    dmRecursos.CambioMatrizCursos := FALSE;
                    GetHisCursos.Agregar;
                    {$else}
                    dmSuper.CambioMatrizCursos := FALSE;
                    dmSuper.AgregarMatrizCurso(sCurso);
                    {$endif}

                    if {$IFDEF SUPERVISORES}dmSuper{$else}dmRecursos{$endif}.CambioMatrizCursos then
                    begin
                         with {$IFDEF SUPERVISORES}dmSuper{$else}dmRecursos{$endif}.cdsMatrizCursos do
                         begin
                              DisableControls;
                              try
                                 if Locate('CB_CODIGO;CU_CODIGO', VarArrayOf( [ dmCliente.Empleado, sCurso ] ), [] ) then
                                 begin
                                      Edit;
                                      FieldByName( 'TOMADO' ).AsInteger := 1;
                                      FieldByName( 'KC_FEC_TOM' ).AsDateTime := GetHisCursos.FieldByName( 'KC_FEC_TOM' ).AsDateTime;
                                      PostData;
                                 end;
                              finally
                                     EnableControls;
                              end;
                         end;
                         dbPivoteMatrizCursos.Refresh;
                    end;
               end
               else
                   ZetaDialogo.ZInformation(Self.Caption,'No se puede agregar el curso tomado. Se debe seleccionar el empleado y curso a consultar.',0);
          end;
     end
     else
         ZetaDialogo.ZInformation(Self.Caption,'No se puede consultar el historial. Se debe seleccionar el empleado y curso a consultar.',0);

     {$IFNDEF SUPERVISORES}
     dmCliente.SetEmpleadoNumero(iEmpleado);
     {$else}
     dmSuper.PosicionaEmpleado(iEmpleado);
     {$endif}

     dbPivoteMatrizCursos.ViewData.FocusedCell := Point(iPosX,iPosY);
end;

function TMatrizCursos_DevEx.EvaluarCurso(var iValidaCurso, iPosX, iPosY: Integer; var sCurso, sCursoNombre : String) : Boolean;
var
   I,X,Y,RightX,BottomY, iEmpleado, iPos, iTotal: Integer;
   ASelection: TRect;
   sVar: string;
   lValidar : Boolean;
   aRecords: TcxPivotGridRecords;

   function GetEmpleado (Renglon:string ):Integer;
   begin
        iPos := Pos(':',Renglon);
        sVar := Copy( Renglon, 0, ( iPos - 2 ));
        Result := StrToIntDef(sVar,0 );
   end;

   procedure GetCurso(Nombre:string);
   begin
        sCursoNombre := '???';
        with dmCatalogos.cdsCursos do
        begin
             if Locate( 'CU_NOMBRE', Nombre, [] ) then
             begin
                  sCursoNombre := FieldByName('CU_NOMBRE').AsString;
                  sCurso := FieldByName('CU_CODIGO').AsString;
             end
        end;
   end;

begin
     lValidar := False;
     iTotal := 0;
     iEmpleado := 0;
     with dbPivoteMatrizCursos.ViewData do
     begin
          for I := 0 to Selection.Count - 1 do
          begin
               ASelection := Selection.Regions[I];
               RightX := Min(ASelection.Right, ColumnCount - 1);
               BottomY := Min(ASelection.Bottom, RowCount - 1);
               for X := ASelection.Left to RightX do
               begin
                    for Y := ASelection.Top to BottomY do
                    begin
                         if not (Rows[Y].IsTotalItem or Columns[X].IsTotalItem)  then
                         begin
                              try
                                 iValidaCurso := Cells[Y,X].Sum;
                                 if ( iValidaCurso = 1 ) then        // Si indica que ya se tom� se revisa si es reprogramado
                                 begin
                                      aRecords := Cells[Y,X].Records;
                                      if ( aRecords.Count > 0 ) and VarIsNull( dbPivoteMatrizCursos.DataController.Values[ ARecords.Items[0], K_COL_FEC_TOMADO ] ) then  // No ha tomado el curso - Se revisa solo el primer registro (No debe haber mas de 1)
                                         iValidaCurso := 0;
                                 end;
                              except
                                    on Error: Exception do
                                    begin
                                        iValidaCurso := -1;
                                    end;
                              end;

                              iEmpleado := GetEmpleado(Rows[Y].Value);
                              GetCurso(Columns[X].Value);
                              iTotal := iTotal + 1;
                              lValidar := ( iEmpleado > 0 ) and (sCurso <> '???');
                              iPosY := BottomY;
                              iPosX := RightX;
                         end;
                    end;
               end;
          end;
     end;

     if ((iTotal > 1) and (lValidar)) or (Not lValidar) then
        Result := False
     else
     begin
          Result := lValidar;
          {$IFNDEF SUPERVISORES}
          dmCliente.SetEmpleadoNumero(iEmpleado);
          {$else}
          dmSuper.PosicionaEmpleado(iEmpleado);
          {$endif}
     end;
end;

procedure TMatrizCursos_DevEx.ConfigurarFiltros;
begin
     ZBaseDlgModal_DevEx.ShowDlgModal( ConfigFiltroCursos, TConfigFiltroCursos );
     if ( ConfigFiltroCursos.ModalResult = mrOK ) then
     begin
          IniciaControlesLookup;
          if ( Lookup1.Enabled and Lookup1.CanFocus ) then
             Lookup1.SetFocus;
     end;
end;

function TMatrizCursos_DevEx.GetLookupEtiqueta( const iPos: Integer ): String;
begin
     case iPos of
          1: Result := 'Puesto';
          2: Result := 'Turno';
          3: Result := 'Tipo de Curso';
          4: Result := 'Clase de Curso';
          5: Result := 'Curso';
          6..14: Result := Global.NombreNivel( iPos - NIVELES_OFFSET );
          else Result := ZetaCommonClasses.VACIO;
     end;
end;

procedure TMatrizCursos_DevEx.IniciaControlesLookup;
var
   i: Integer;
   oListaFiltros: TStrings;

   function GetListaFiltrosTag( const iPos: Integer ): Integer;
   begin
        with oListaFiltros do
        begin
             if ( iPos <= Count - 1 ) then
                Result := StrToIntDef( Strings[ iPos ], 0 )
             else
                 Result := 0;
        end;
   end;

   function GetLookupDataSetFiltro( const iPos: Integer ): TZetaLookupDataset;
   begin
        case iPos of
             1: Result := dmCatalogos.cdsPuestos;
             2: Result := dmCatalogos.cdsTurnos;
             3: Result := dmTablas.cdsTipoCursos;
             4: Result := dmTablas.cdsClasifiCurso;
             5: Result := dmCatalogos.cdsCursos;
             6: Result := dmTablas.cdsNivel1;
             7: Result := dmTablas.cdsNivel2;
             8: Result := dmTablas.cdsNivel3;
             9: Result := dmTablas.cdsNivel4;
             10: Result := dmTablas.cdsNivel5;
             11: Result := dmTablas.cdsNivel6;
             12: Result := dmTablas.cdsNivel7;
             13: Result := dmTablas.cdsNivel8;
             14: Result := dmTablas.cdsNivel9;
             else Result := nil;
        end;
   end;

   procedure SetControlesLookup( const iPos: Integer; oLookup: TZetaKeyLookup_DevEx; oLabel: TLabel );
   begin
        oLookup.Visible := ( oLookup.Tag > 0 );
        oLabel.Visible := oLookup.Visible;
        if oLookup.Visible then
        begin
             oLookup.LookupDataset := GetLookupDataSetFiltro( oLookup.Tag );
             oLabel.Caption := GetLookupEtiqueta( oLookup.Tag ) + ':';
             oLookup.SetLlaveDescripcion( ZetaCommonClasses.VACIO, ZetaCommonClasses.VACIO );
             {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
             if oLookup.Tag >= 6 then
             begin
                  oLookup.Width := 350;
                  oLookup.WidthLlave := K_WIDTHLLAVE;
             end
             else
             begin
                  oLookup.WidthLlave := 60;
             end;
             {$IFEND}
        end
        else
            oLookup.LookupDataset := nil;
   end;

begin
     oListaFiltros := TStringList.Create;
     try
        oListaFiltros.CommaText := ClientRegistry.FiltroCursosProgramados;
        Lookup1.Tag := GetListaFiltrosTag( 0 );
        Lookup2.Tag := GetListaFiltrosTag( 1 );
        Lookup3.Tag := GetListaFiltrosTag( 2 );
        Lookup4.Tag := GetListaFiltrosTag( 3 );
     finally
            FreeAndNil( oListaFiltros );
     end;
     SetControlesLookup( 0, Lookup1, LblLookup1 );
     SetControlesLookup( 1, Lookup2, LblLookup2 );
     SetControlesLookup( 2, Lookup3, LblLookup3 );
     SetControlesLookup( 3, Lookup4, LblLookup4 );

     ConectaLookupsDataSets;

     Application.ProcessMessages;
end;

procedure TMatrizCursos_DevEx.SetFiltroNivel();
var
   i: Integer;
   ColumnaPivot: TcxDBPivotGridField;
begin
     for i := 1 to Global.NumNiveles do
     begin
          case i of
               1: ColumnaPivot := Nivel_1;
               2: ColumnaPivot := Nivel_2;
               3: ColumnaPivot := Nivel_3;
               4: ColumnaPivot := Nivel_4;
               5: ColumnaPivot := Nivel_5;
               6: ColumnaPivot := Nivel_6;
               7: ColumnaPivot := Nivel_7;
               8: ColumnaPivot := Nivel_8;
               9: ColumnaPivot := Nivel_9;
          else
              ColumnaPivot := nil;
          end;
          if Assigned( ColumnaPivot ) then
          begin
               with ColumnaPivot do
               begin
                    Visible := TRUE;
                    Hidden  := FALSE;
                    Caption := Global.NombreNivel(i);
               end;
          end;
     end;
end;

procedure TMatrizCursos_DevEx.ConectaLookupsDataSets;

   procedure ConectaLookup( oLookup: TZetaKeyLookup_DevEx );
   begin
        if oLookup.Visible and Assigned( oLookup.LookupDataset ) then
           oLookup.LookupDataset.Conectar;
   end;

begin
     ConectaLookup( Lookup1 );
     ConectaLookup( Lookup2 );
     ConectaLookup( Lookup3 );
     ConectaLookup( Lookup4 );
end;

function TMatrizCursos_DevEx.GetLookupParametroName( const iPos: Integer ): String;
begin
     case iPos of
          1: Result := 'Puesto';
          2: Result := 'Turno';
          3: Result := 'TipoCurso';
          4: Result := 'ClaseCurso';
          5: Result := 'Curso';
          6..14: Result := Format( 'Nivel%d', [ iPos  - NIVELES_OFFSET ] );
          else Result := ZetaCommonClasses.VACIO;
     end;
end;

procedure TMatrizCursos_DevEx.AsignaParametros;

   procedure AgregaParametroLookup( oLookup: TZetaKeyLookup_DevEx );
   begin
        with oLookup do
        begin
             if Visible then
                {$IFDEF SUPERVISORES}
                dmSuper
                {$ELSE}
                dmRecursos
                {$ENDIF}
                .FiltroMatCursos.AddString( GetLookupParametroName( Tag ), Llave );
        end;
   end;

begin
     with {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.FiltroMatCursos do
     begin
          Clear;
          AddBoolean( 'Opcionales', CBOpcionales.Checked );
          AddBoolean( 'SoloSTPS', CBSoloSTPS.Checked );
          AddDate( 'Referencia', FechaReferencia.Valor );
     end;
     AgregaParametroLookup( Lookup1 );
     AgregaParametroLookup( Lookup2 );
     AgregaParametroLookup( Lookup3 );
     AgregaParametroLookup( Lookup4 );
end;

procedure TMatrizCursos_DevEx.InitColoresCeldas;
begin
     // Colores de Celdas
     FStyleRojo := cxStyleRepRojo.CreateItem(TcxStyle) as TcxStyle;
     FStyleRojo.Color := TColor( K_COLOR_ROJO );

     FStyleVerde := cxStyleRepRojo.CreateItem(TcxStyle) as TcxStyle;
     FStyleVerde.Color := TColor( K_COLOR_VERDE );

     FStyleAmarillo := cxStyleRepRojo.CreateItem(TcxStyle) as TcxStyle;
     FStyleAmarillo.Color := TColor( K_COLOR_AMARILLO );

     FStyleAzul := cxStyleRepRojo.CreateItem(TcxStyle) as TcxStyle;
     FStyleAzul.Color := TColor( K_COLOR_AZUL );

     PanelTomado.Color := TColor( K_COLOR_VERDE );
     PanelProgramado.Color := TColor( K_COLOR_AZUL );
     PanelAdvertencia.Color := TColor( K_COLOR_AMARILLO );
     PanelVencido.Color := TColor( K_COLOR_ROJO );

end;

end.
