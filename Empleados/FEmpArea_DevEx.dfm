inherited EmpArea_DevEx: TEmpArea_DevEx
  Left = 385
  Top = 296
  Caption = #193'rea'
  ClientHeight = 205
  ClientWidth = 475
  PixelsPerInch = 96
  TextHeight = 13
  object CB_NIVEL1lbl: TLabel [0]
    Left = 136
    Top = 28
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel1:'
  end
  object CB_NIVEL2lbl: TLabel [1]
    Left = 136
    Top = 47
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel2:'
  end
  object CB_NIVEL3lbl: TLabel [2]
    Left = 136
    Top = 67
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel3:'
  end
  object CB_NIVEL4lbl: TLabel [3]
    Left = 136
    Top = 86
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel4:'
  end
  object CB_NIVEL5lbl: TLabel [4]
    Left = 136
    Top = 106
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel5:'
  end
  object CB_NIVEL6lbl: TLabel [5]
    Left = 136
    Top = 125
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel6:'
  end
  object CB_NIVEL7lbl: TLabel [6]
    Left = 136
    Top = 144
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel7:'
  end
  object CB_NIVEL8lbl: TLabel [7]
    Left = 136
    Top = 164
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel8:'
  end
  object CB_NIVEL9lbl: TLabel [8]
    Left = 136
    Top = 183
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel9:'
  end
  object ZNIVEL1: TZetaTextBox [9]
    Left = 238
    Top = 26
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL1'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL2: TZetaTextBox [10]
    Left = 238
    Top = 45
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL2'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL3: TZetaTextBox [11]
    Left = 238
    Top = 65
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL3'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL4: TZetaTextBox [12]
    Left = 238
    Top = 84
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL4'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL5: TZetaTextBox [13]
    Left = 238
    Top = 104
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL5'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL6: TZetaTextBox [14]
    Left = 238
    Top = 123
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL6'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL7: TZetaTextBox [15]
    Left = 238
    Top = 142
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL7'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL8: TZetaTextBox [16]
    Left = 238
    Top = 162
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL8'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object ZNIVEL9: TZetaTextBox [17]
    Left = 238
    Top = 181
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL9'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  object CB_NIVEL5: TZetaDBTextBox [18]
    Left = 175
    Top = 104
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL5'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL5'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL2: TZetaDBTextBox [19]
    Left = 175
    Top = 45
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL2'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL2'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL3: TZetaDBTextBox [20]
    Left = 175
    Top = 65
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL3'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL3'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL4: TZetaDBTextBox [21]
    Left = 175
    Top = 84
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL4'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL4'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL1: TZetaDBTextBox [22]
    Left = 175
    Top = 26
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL1'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL1'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL6: TZetaDBTextBox [23]
    Left = 175
    Top = 123
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL6'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL6'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL7: TZetaDBTextBox [24]
    Left = 175
    Top = 142
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL7'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL7'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL8: TZetaDBTextBox [25]
    Left = 175
    Top = 162
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL8'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL8'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL9: TZetaDBTextBox [26]
    Left = 175
    Top = 181
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL9'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL9'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NIVEL12lbl: TLabel [27]
    Left = 130
    Top = 241
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel12:'
    Visible = False
  end
  object CB_NIVEL12: TZetaDBTextBox [28]
    Left = 175
    Top = 239
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL12'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL12'
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZNIVEL12: TZetaTextBox [29]
    Left = 237
    Top = 239
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL12'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clSilver
    Border = False
  end
  object CB_NIVEL11lbl: TLabel [30]
    Left = 130
    Top = 222
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel11:'
    Visible = False
  end
  object CB_NIVEL11: TZetaDBTextBox [31]
    Left = 175
    Top = 220
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL11'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL11'
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZNIVEL11: TZetaTextBox [32]
    Left = 237
    Top = 220
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL11'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clSilver
    Border = False
  end
  object CB_NIVEL10lbl: TLabel [33]
    Left = 130
    Top = 202
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel10:'
    Visible = False
  end
  object CB_NIVEL10: TZetaDBTextBox [34]
    Left = 175
    Top = 200
    Width = 60
    Height = 17
    AutoSize = False
    Caption = 'CB_NIVEL10'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NIVEL10'
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZNIVEL10: TZetaTextBox [35]
    Left = 237
    Top = 200
    Width = 230
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL10'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clSilver
    Border = False
  end
  inherited PanelIdentifica: TPanel
    Width = 475
    inherited Slider: TSplitter
      Left = 371
    end
    inherited ValorActivo1: TPanel
      Width = 355
      inherited textoValorActivo1: TLabel
        Width = 349
      end
    end
    inherited ValorActivo2: TPanel
      Left = 374
      Width = 101
      inherited textoValorActivo2: TLabel
        Width = 95
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 28
  end
end
