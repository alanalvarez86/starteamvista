unit FGridReservacionesSesion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaDBTextBox, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  DBCtrls,Buttons, ExtCtrls, ImgList, ZetaSmartLists, ZBaseSesion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TGridReservacionesSesion_DevEx = class(TBaseSesion_DevEx)
    Label3: TLabel;
    SE_INSCRITO: TZetaDBTextBox;
    lblPeriodo: TLabel;
    ztbPeriodo: TZetaTextBox;
    imgListConflicto: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDblClick(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  GridReservacionesSesion_DevEx: TGridReservacionesSesion_DevEx;

implementation

uses DRecursos,
     DSistema,
     FEditReservaSesion_DevEx,
     ZBaseEdicion_DevEx,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses, DCliente, dCatalogos;

{$R *.DFM}
procedure TGridReservacionesSesion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_RESERVACION_SESION;
end;

procedure TGridReservacionesSesion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmRecursos.cdsSesion do
     begin
          if( FieldbyName('SE_FEC_INI').AsDateTime = FieldbyName('SE_FEC_FIN').AsDateTime )then
              ztbPeriodo.Caption := FechaCorta( FieldbyName('SE_FEC_INI').AsDateTime )
          else
              ztbPeriodo.Caption := Format( 'del  %s  al  %s ', [ FechaCorta( FieldbyName('SE_FEC_INI').AsDateTime ),
                                                                  FechaCorta( FieldbyName('SE_FEC_FIN').AsDateTime ) ] );
     end;
end;

procedure TGridReservacionesSesion_DevEx.Connect;
begin
     inherited;
     dmCatalogos.cdsAulas.Conectar;
     DataSource.DataSet:= dmRecursos.cdsReserva;   // Cuando llega a la forma ya trae los registros agregados
end;

procedure TGridReservacionesSesion_DevEx.Agregar;
var
   iSesion : Integer;
begin
     inherited;
     if PuedeAgregar then
     begin
          with dmRecursos do
          begin
               iSesion := cdsSesion.FieldByName ( 'SE_FOLIO' ).AsInteger;
               with cdsReserva do
               begin
                    // Sobreescribir onNewRecord para agregar a una sesion
                    FieldByName( 'RV_FEC_INI').AsDateTime := cdsSesion.FieldByName('SE_FEC_INI').AsDateTime;
                    FieldByName ( 'SE_FOLIO' ).AsInteger := iSesion;
                    FieldByName( 'RV_TIPO' ).AsInteger := Ord( rvSesion );
                    FieldByName( 'RV_RESUMEN' ).AsString := cdsSesion.FieldByName('CU_NOMBRE').AsString;//Format( 'Reserva de sesi�n %d', [ iSesion ] );
                    FieldByName('MA_CODIGO').AsString := cdsSesion.FieldbyName('MA_CODIGO').AsString;
                    dmCatalogos.cdsAulas.First;
                    FieldbyName('AL_CODIGO').AsString := dmCatalogos.cdsAulas.FieldbyName('AL_CODIGO').AsString;
                    ZBaseEdicion_DevEx.ShowFormaEdicion( EditReservaSesion_DevEx, TEditReservaSesion_DevEx );
                    DataSourceStateChange( Self );
               end;
          end;
     end;
end;

procedure TGridReservacionesSesion_DevEx.Modificar;
begin
     if( ( dmRecursos.cdsReserva.RecordCount > 0 ) and PuedeModificar )then
     begin
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditReservaSesion_DevEx, TEditReservaSesion_DevEx );
          DataSourceStateChange( Self );
     end;
        //dmRecursos.cdsReserva.Modificar;
end;

procedure TGridReservacionesSesion_DevEx.ZetaDBGridDblClick(Sender: TObject);
begin
     self.Modificar;
end;

procedure TGridReservacionesSesion_DevEx.ZetaDBGridDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if( Column.FieldName = 'CONFLICTO' ) and ( Column.Field.AsInteger > 0 ) then
         imgListConflicto.Draw( ZetaDBGrid.Canvas, Rect.left + 1, Rect.top + 1, 0 );
end;

end.
