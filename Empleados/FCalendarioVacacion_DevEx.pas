unit FCalendarioVacacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseCalendarioRitmo_DevEx, ComCtrls, StdCtrls, Spin, ZetaKeyCombo, Buttons,
  ExtCtrls, Grids, ZetaClientDataSet, Mask, ZetaFecha,ZetaSmartLists,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
   TressMorado2013, Menus, cxTextEdit, cxButtons,
  cxGroupBox;

type
  TCalendarioVacacion_DevEx = class(TBaseCalendarioRitmo_DevEx)
    FechaIniVaca: TZetaFecha;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FechaIniVacaValidDate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
    FDiasGozar: Double;{OP: 11/06/08}
    FFechaFinVaca: TDate;{OP: 11/06/08}
    procedure ConfiguraDescipcion;
  protected
    procedure ResetCalendario; override;
    procedure ResetFecha( wDay: Word ); override;
    procedure SetInfoCeldaFecha( const dFecha: TDate; const sDay: String; const iCol, iRow: Integer ); override;
  public
    { Public declarations }
    property Dataset: TZetaClientDataset read FDataset write FDataset;
    property DiasGozar: Double read FDiasGozar write FDiasGozar;{OP: 11/06/08}
    property FechaFinVaca: TDate read FFechaFinVaca write FFechaFinVaca;{OP: 11/06/08}
  end;

var
  CalendarioVacacion_DevEx: TCalendarioVacacion_DevEx;

procedure ShowCalendarioVacacion( const iEmpleado: Integer; const dFecha: TDate; ZetaDataset: TZetaClientDataSet; rDiasGozar: Double );

implementation

uses dCliente,
{$ifdef SUPERVISORES}
     dSuper,
{$else}
     dAsistencia,
{$endif}
     dCatalogos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists;

{$R *.DFM}

procedure ShowCalendarioVacacion( const iEmpleado: Integer; const dFecha: TDate;
                                  ZetaDataset: TZetaClientDataSet; rDiasGozar: Double );
begin
     CalendarioVacacion_DevEx := TCalendarioVacacion_DevEx.Create(Application);
     try
        with CalendarioVacacion_DevEx do
        begin
             DiasGozar := rDiasGozar;{OP: 11/06/08}
             Dataset := ZetaDataset;
             Fecha := dFecha;
             Init;
             ShowModal;
        end;
     finally
            FreeAndNil( CalendarioVacacion_DevEx );
     end;
end;

{ *********** TCalendarioVacacion ************ }

procedure TCalendarioVacacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_Calendario_del_empleado;
     {OP: 11/06/08}
     {Colores de vacaciones.}
     LblNinguno.Caption := 'Vacaci�n';
     PanelShowNinguno.Color := COLOR_DIA_VACA;
end;

procedure TCalendarioVacacion_DevEx.ConfiguraDescipcion;
var
   DescEmp:String;
const
     K_MAX_LETRAS = 40;
     K_MAX_DESCEMP = 28;
begin
     DescEmp := dmCliente.GetEmpleadoDescripcion + '  D�as: ' + FloatToStr( self.DiasGozar );
     //Si el texto total es mayor a 30 caracteres
     if Length ( DescEmp ) > K_MAX_LETRAS then
     begin
          //Cortar el nombre del empleado
          DescEmp := Copy( dmCliente.GetEmpleadoDescripcion,0,K_MAX_DESCEMP );
          DescEmp := DescEmp  + '...  D�as: ' + FloatToStr( self.DiasGozar );
     end;
     PanelSuperior.Caption := DescEmp;
end;

procedure TCalendarioVacacion_DevEx.FormShow(Sender: TObject);

begin
     inherited;
     ConfiguraDescipcion;
     FechaIniVaca.Valor := Fecha;
end;

procedure TCalendarioVacacion_DevEx.ResetCalendario;
var
   rDiasRango: Double;{OP: 11/06/08}
begin
     rDiasRango := 0.0;{OP: 11/06/08}
{$ifdef SUPERVISORES}
     FechaFinVaca := dmSuper.GetVacaFechaRegreso(  dmCliente.Empleado, FechaIniVaca.Valor, DiasGozar, rDiasRango );{OP: 11/06/08}
     dmSuper.ResetCalendarioEmpleadoActivo( FirstDayOfMonth( Fecha ), LastDayOfMonth( Fecha ) );
{$else}
     FechaFinVaca := dmAsistencia.GetVacaFechaRegreso(  dmCliente.Empleado, FechaIniVaca.Valor, DiasGozar, rDiasRango );{OP: 11/06/08}
     dmAsistencia.ResetCalendarioEmpleadoActivo( FirstDayOfMonth( Fecha ), LastDayOfMonth( Fecha ) );
{$endif}
     inherited;
end;

procedure TCalendarioVacacion_DevEx.SetInfoCeldaFecha(const dFecha: TDate; const sDay: String;
          const iCol, iRow: Integer);
const
     K_STATUS_NINGUNO = -1;
var
   sHorario, sDiaVaca, sSabVaca, sDesVaca : String; {OP: 11/06/08}
   iStatus : Integer;
begin
     sDiaVaca := VACIO;{OP: 11/06/08}
     with FDataSet do
     begin
          if Locate( 'AU_FECHA', dFecha, [] ) then
          begin
               iStatus := FieldByName( 'AU_STATUS' ).AsInteger; // Si no est� activo viene K_STATUS_NINGUNO
               sHorario := FieldByName( 'HO_CODIGO' ).AsString; // Si no est� activo Viene VACIO del servidor
               sDiaVaca := FieldByName( 'TU_VACA_HA' ).AsString; {OP: 11/06/08}
               sSabVaca := FieldByName( 'TU_VACA_SA' ).AsString; {OP: 11/06/08}
               sDesVaca := FieldByName( 'TU_VACA_DE' ).AsString; {OP: 11/06/08}
          end
          else // Si no viene el registro se reporta como Status Ninguno
          begin
               iStatus := K_STATUS_NINGUNO;
               sHorario := ZetaCommonClasses.VACIO;
          end;
     end;
     with StringGrid do
     begin
          {OP: 11/06/08}
          if( ( dFecha >= FechaIniVaca.Valor ) and ( dFecha < FechaFinVaca ) ) then
          begin
               Objects[ iCol, iRow ] := TObject( DIA_VACACION );
               if( iStatus = Integer( auHabil ) ) then
                   Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + sDiaVaca
               else if( iStatus = Integer( auSabado ) ) and ( StrToFloat( sSabVaca ) <> 0 ) then
                    Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + sSabVaca
               else if( iStatus = Integer( auDescanso ) ) and ( StrToFloat( sDesVaca ) <> 0 ) then
                    Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + sDesVaca
               else
               begin
                   Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + sHorario;
                   Objects[ iCol, iRow ] := TObject( iStatus );
               end;
          end
          else
          begin
              Cells[ iCol, iRow ] := PadL( sDay, LONGITUD_DIA ) + sHorario;
              Objects[ iCol, iRow ] := TObject( iStatus );
          end;
     end;
end;

procedure TCalendarioVacacion_DevEx.ResetFecha(wDay: Word);
var
   StatusEmp : eStatusEmpleado;
   sDescripTurno: String;
begin
     inherited;
     with FDataSet do
     begin
          //if Locate( 'AU_FECHA',ZetaCommonTools.FechaAsStr( Fecha ), [] ) then
          if Locate( 'AU_FECHA', Fecha, [] ) then
          begin
               StatusEmp := eStatusEmpleado( FieldByName( 'STATUS_ACT' ).AsInteger );
               if ( StatusEmp = steEmpleado ) then
                  sDescripTurno := Format( '%s: %s', [ FieldByName( 'CB_TURNO' ).AsString,
                                   dmCatalogos.cdsTurnos.GetDescripcion( FieldByName( 'CB_TURNO' ).AsString ) ] )

               else
                   sDescripTurno := ZetaCommonLists.ObtieneElemento( lfStatusEmpleado, Ord( StatusEmp ) );
          end
          else
              sDescripTurno := ZetaCommonClasses.VACIO;
     end;
     StatusBar.Panels[ 3 ].Text := sDescripTurno;
end;

{OP: 11/06/08}
procedure TCalendarioVacacion_DevEx.FechaIniVacaValidDate(Sender: TObject);
begin
     inherited;
     Fecha := FechaIniVaca.Valor;
     ResetCalendario;     
end;

procedure TCalendarioVacacion_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

end.
