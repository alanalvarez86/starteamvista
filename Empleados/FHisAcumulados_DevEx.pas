unit FHisAcumulados_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  THisAcumulados_DevEx = class(TBaseGridLectura_DevEx)
    CO_NUMERO: TcxGridDBColumn;
    CO_DESCRIP: TcxGridDBColumn;
    AC_ANUAL: TcxGridDBColumn;
    AC_MES_01: TcxGridDBColumn;
    AC_MES_02: TcxGridDBColumn;
    AC_MES_03: TcxGridDBColumn;
    AC_MES_04: TcxGridDBColumn;
    AC_MES_05: TcxGridDBColumn;
    AC_MES_06: TcxGridDBColumn;
    AC_MES_07: TcxGridDBColumn;
    AC_MES_08: TcxGridDBColumn;
    AC_MES_09: TcxGridDBColumn;
    AC_MES_10: TcxGridDBColumn;
    AC_MES_11: TcxGridDBColumn;
    AC_MES_12: TcxGridDBColumn;
    AC_MES_13: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  HisAcumulados_DevEx: THisAcumulados_DevEx;

implementation

{$R *.DFM}

uses DRecursos,
     DCatalogos,
     ZetaCommonLists,
     ZetaCommonClasses;

procedure THisAcumulados_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stSistema;
     HelpContext:= H10143_Acumulados;
end;

procedure THisAcumulados_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     with dmRecursos do
     begin
          cdsHisAcumulados.Conectar;
          DataSource.DataSet:= cdsHisAcumulados;
     end;
end;

procedure THisAcumulados_DevEx.Refresh;
begin
     dmRecursos.cdsHisAcumulados.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisAcumulados_DevEx.Agregar;
begin
     dmRecursos.cdsHisAcumulados.Agregar;
end;

procedure THisAcumulados_DevEx.Borrar;
begin
     dmRecursos.cdsHisAcumulados.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisAcumulados_DevEx.Modificar;
begin
     dmRecursos.cdsHisAcumulados.Modificar;
end;

procedure THisAcumulados_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;

end;

end.
