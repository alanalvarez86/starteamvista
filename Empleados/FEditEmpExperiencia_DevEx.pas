unit FEditEmpExperiencia_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, DBCtrls, ZetaDBTextBox, Db, Buttons, ExtCtrls, Mask,
  ZetaSmartLists, ZetaKeyCombo, ZetaEdit, ZetaNumero,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, 
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditEmpExperiencia_DevEx= class(TBaseEdicion_DevEx)
    GroupBox1: TGroupBox;
    LEstudios: TLabel;
    CB_ESTUDIO: TZetaDBKeyLookup_DevEx;
    CB_EST_HOY: TDBCheckBox;
    CB_EST_HOR: TDBEdit;
    CB_EST_HORlbl: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    CB_ESCUELA: TDBEdit;
    Label4: TLabel;
    GroupBox2: TGroupBox;
    CB_IDIOMAlbl: TLabel;
    LMaquinas: TLabel;
    LExperiencia: TLabel;
    CB_HABLA: TDBCheckBox;
    CB_IDIOMA: TDBEdit;
    CB_EXPERIE: TDBEdit;
    CB_MAQUINA: TDBEdit;
    LCarrera: TLabel;
    CB_CARRERA: TDBEdit;
    CB_TESCUEL: TZetaDBKeyCombo;
    CB_TITULO: TZetaDBKeyCombo;
    CB_YTITULO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure CB_EST_HOYClick(Sender: TObject);
    procedure CB_HABLAClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    procedure ClearCampo(sCampo: String);

  protected
    procedure Connect; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EditEmpExperiencia_DevEx: TEditEmpExperiencia_DevEx;

implementation

uses dRecursos, dTablas, ZAccesosTress,ZetaTipoEntidad,
     ZImprimeForma,ZetaCommonClasses, ZetaCommonLists;

{$R *.DFM}

procedure TEditEmpExperiencia_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_CURR_EXPERIENCIA;
     FirstControl := CB_ESTUDIO;
     HelpContext:= H10124_Expediente_Experiencia_empleado;
     TipoValorActivo1 := stEmpleado;
     CB_ESTUDIO.LookupDataset := dmTablas.cdsEstudios;
end;

procedure TEditEmpExperiencia_DevEx.Connect;
begin
     with dmRecursos, dmTablas do
     begin
          cdsEstudios.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
     CB_EST_HOYClick( Self );
     CB_HABLACLick( Self );
end;

procedure TEditEmpExperiencia_DevEx.CB_EST_HOYClick(Sender: TObject);
begin
     with CB_EST_HOY do
     begin
          CB_EST_HORlbl.Enabled := Checked;
          CB_EST_HOR.Enabled    := Checked;
     end;
end;

procedure TEditEmpExperiencia_DevEx.CB_HABLAClick(Sender: TObject);
begin
     with CB_HABLA do
     begin
          CB_IDIOMAlbl.Enabled := Checked;
          CB_IDIOMA.Enabled    := Checked;
     end;
end;

procedure TEditEmpExperiencia_DevEx.ClearCampo( sCampo: String );
begin
     with dmRecursos.cdsDatosEmpleado do
     begin
          if not Editing then
             Edit;
          FieldByName( sCampo ).AsString := '';
     end;
end;

procedure TEditEmpExperiencia_DevEx.OKClick(Sender: TObject);
begin
     if not CB_EST_HOY.Checked then
        ClearCampo( 'CB_EST_HOR' );
     if not CB_HABLA.Checked then
        ClearCampo( 'CB_IDIOMA' );
     inherited;
     Close;
end;

procedure TEditEmpExperiencia_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEditEmpExperiencia_DevEx.OK_DevExClick(Sender: TObject);
begin
     if not CB_EST_HOY.Checked then
        ClearCampo( 'CB_EST_HOR' );
     if not CB_HABLA.Checked then
        ClearCampo( 'CB_IDIOMA' );
     inherited;
     Close;
end;

end.







