unit FHisTools_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls,
  Grids, DBGrids, ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  THisTools_DevEx = class(TBaseGridLectura_DevEx)
    KT_FEC_INI: TcxGridDBColumn;
    TO_CODIGO: TcxGridDBColumn;
    TO_DESCRIP: TcxGridDBColumn;
    KT_REFEREN: TcxGridDBColumn;
    KT_TALLA: TcxGridDBColumn;
    KT_ACTIVO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  HisTools_DevEx: THisTools_DevEx;

implementation


{$R *.DFM}

uses dRecursos, dCatalogos, ZetaCommonLists, ZetaCommonClasses;

{ THisTools }

procedure THisTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10138_Historial_de_Herramientas;
end;

procedure THisTools_DevEx.Connect;
begin
     dmCatalogos.cdsTools.Conectar;

     with dmRecursos do
     begin
          cdsHisTools.Conectar;
          DataSource.DataSet:= cdsHisTools;
     end;
end;

procedure THisTools_DevEx.Refresh;
begin
     dmRecursos.cdsHisTools.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisTools_DevEx.Agregar;
begin
     dmRecursos.cdsHisTools.Agregar;
end;

procedure THisTools_DevEx.Borrar;
begin
     dmRecursos.cdsHisTools.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisTools_DevEx.Modificar;
begin
     dmRecursos.cdsHisTools.Modificar;
end;

procedure THisTools_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;

end;

end.
