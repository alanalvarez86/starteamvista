unit FEditPlaza_DevEx;

interface

uses
  Windows, Messages, SysUtils,
  {$ifndef VER130}
  Variants,
  {$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, StdCtrls, DBCtrls, ZetaKeyCombo, 
  ComCtrls, DB, ExtCtrls, ZetaSmartLists, Buttons, ZetaNumero, Mask,
  ZetaFecha, ZetaEdit, ZetaDBTextBox, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, ZetaKeyLookup_DevEx;

const
     KA_CLASIFI = 0;
     KA_NIVEL1 = 1;
     {$ifdef ACS}
     KA_NIVEL12 = 12;
     K_FORMA_ACS = 690;
     K_ALT_DEF = 23;
     {$else}
     KA_NIVEL9 = 9;
     {$endif}
     KA_PUESTO = 10;
     KA_TURNO = 11;

type
  TEditPlaza_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    lblPuesto: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    PL_FOLIO: TZetaDBTextBox;
    lbTitular: TLabel;
    Label3: TLabel;
    PL_NOMBRE: TDBEdit;
    PU_CODIGO: TZetaDBKeyLookup_DevEx;
    PL_REPORTA: TZetaDBKeyLookup_DevEx;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    PL_ORDEN: TZetaDBNumero;
    GroupBox1: TGroupBox;
    PL_TIPO: TZetaDBKeyCombo;
    Label9: TLabel;
    PL_FEC_FIN: TZetaDBFecha;
    lblVence: TLabel;
    ckVence: TCheckBox;
    PL_FEC_INI: TZetaDBFecha;
    Label11: TLabel;
    PL_CODIGO: TDBEdit;
    Label2: TLabel;
    PageControl: TcxPageControl;
    General: TcxTabSheet;
    Contratacion: TcxTabSheet;
    Area: TcxTabSheet;
    TabSheet1: TcxTabSheet;
    Label16: TLabel;
    PL_TIREP: TZetaDBKeyCombo;
    Label7: TLabel;
    PL_INGLES: TDBEdit;
    CO_NUMEROLbl: TLabel;
    PL_NUMERO: TZetaDBNumero;
    Label8: TLabel;
    PL_TEXTO: TDBEdit;
    PL_SUB_CTA: TDBEdit;
    Label10: TLabel;
    PL_NIVEL1lbl: TLabel;
    PL_NIVEL2lbl: TLabel;
    PL_NIVEL9lbl: TLabel;
    PL_NIVEL8lbl: TLabel;
    PL_NIVEL7lbl: TLabel;
    PL_NIVEL6lbl: TLabel;
    PL_NIVEL5lbl: TLabel;
    PL_NIVEL4lbl: TLabel;
    PL_NIVEL3lbl: TLabel;
    PL_NIVEL10lbl: TLabel;
    PL_NIVEL11lbl: TLabel;
    PL_NIVEL12lbl: TLabel;
    PL_NIVEL1: TZetaDBKeyLookup_DevEx;
    PL_NIVEL2: TZetaDBKeyLookup_DevEx;
    PL_NIVEL3: TZetaDBKeyLookup_DevEx;
    PL_NIVEL4: TZetaDBKeyLookup_DevEx;
    PL_NIVEL5: TZetaDBKeyLookup_DevEx;
    PL_NIVEL6: TZetaDBKeyLookup_DevEx;
    PL_NIVEL7: TZetaDBKeyLookup_DevEx;
    PL_NIVEL8: TZetaDBKeyLookup_DevEx;
    PL_NIVEL9: TZetaDBKeyLookup_DevEx;
    PL_NIVEL10: TZetaDBKeyLookup_DevEx;
    PL_NIVEL11: TZetaDBKeyLookup_DevEx;
    PL_NIVEL12: TZetaDBKeyLookup_DevEx;
    LblTabulador: TLabel;
    PL_AUTOSAL: TDBCheckBox;
    LblSalario: TLabel;
    PL_SALARIO: TZetaDBNumero;
    Label33: TLabel;
    PL_PER_VAR: TZetaDBNumero;
    PL_TABLASS: TZetaDBKeyLookup_DevEx;
    Label28: TLabel;
    Label31: TLabel;
    PL_ZONA_GE: TDBComboBox;
    PL_NOMINA: TZetaDBKeyCombo;
    PL_CHECA: TDBCheckBox;
    PL_NIVEL0: TZetaDBKeyLookup_DevEx;
    PL_AREA: TZetaDBKeyLookup_DevEx;
    PL_CONTRAT: TZetaDBKeyLookup_DevEx;
    PL_PATRON: TZetaDBKeyLookup_DevEx;
    PL_TURNO: TZetaDBKeyLookup_DevEx;
    PL_CLASIFI: TZetaDBKeyLookup_DevEx;
    Label12: TLabel;
    Label21: TLabel;
    lblConfidencialidad: TLabel;
    Arealbl: TLabel;
    Label27: TLabel;
    Label29: TLabel;
    Label26: TLabel;
    Label4: TLabel;
    btWizAsignar: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ckVenceClick(Sender: TObject);
    procedure PU_CODIGOValidKey(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PL_AUTOSALClick(Sender: TObject);
    procedure PL_CLASIFIExit(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure btWizAsignarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure PL_NIVEL12ValidLookup(Sender: TObject);
    procedure PL_NIVEL11ValidLookup(Sender: TObject);
    procedure PL_NIVEL10ValidLookup(Sender: TObject);
    procedure PL_NIVEL9ValidLookup(Sender: TObject);
    procedure PL_NIVEL8ValidLookup(Sender: TObject);
    procedure PL_NIVEL7ValidLookup(Sender: TObject);
    procedure PL_NIVEL6ValidLookup(Sender: TObject);
    procedure PL_NIVEL5ValidLookup(Sender: TObject);
    procedure PL_NIVEL4ValidLookup(Sender: TObject);
    procedure PL_NIVEL3ValidLookup(Sender: TObject);
    procedure PL_NIVEL2ValidLookup(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    //FCambioEmpleado: Boolean;
    sClasifi: string;
    aOldValues: array[ KA_CLASIFI..KA_TURNO ] of String;
    iOldEmpleado: Integer;
    function SetSalarioTabulador: Boolean;
    function CambiaKardex: Boolean;
    procedure HabilitaFecha;
    procedure SetCamposNivel;
    procedure RevisaControles;
    procedure LlenaListaTabulador;
    procedure InitOldValues;
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure EscribirCambios;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure HabilitaControles; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
  public
    { Public declarations }

  end;

var
  EditPlaza_DevEx: TEditPlaza_DevEx;

implementation

uses
    dGlobal,
    dCatalogos,
    dRecursos,
    dCliente,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaCommonTools,
    ZetaBuscaEmpleado_DevEx,
    ZAccesosTress,
    ZetaDialogo,
    dSistema,
    dTablas,
    FEditMovtoKardexDlg_DevEx,
    FCambiarTitularPlaza_DevEx, DBaseCliente;

{$R *.dfm}

{ TEditPlaza }

procedure TEditPlaza_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     PL_NIVEL10.DataSource := DataSource;
     PL_NIVEL11.DataSource := DataSource;
     PL_NIVEL12.DataSource := DataSource;
     {
     PL_NIVEL10.Visible := True;
     PL_NIVEL11.Visible := True;
     PL_NIVEL12.Visible := True;
     PL_NIVEL10lbl.Visible := True;
     PL_NIVEL11lbl.Visible := True;
     PL_NIVEL12lbl.Visible := True;
     }

     PL_NIVEL12lbl.Top := PL_NIVEL1lbl.Top;
     PL_NIVEL11lbl.Top := PL_NIVEL12lbl.Top + K_ALT_DEF;
     PL_NIVEL10lbl.Top := PL_NIVEL11lbl.Top + K_ALT_DEF;
     PL_NIVEL9lbl.Top   := PL_NIVEL10lbl.Top + K_ALT_DEF;
     PL_NIVEL8lbl.Top  := PL_NIVEL9lbl.Top + K_ALT_DEF;
     PL_NIVEL7lbl.Top  := PL_NIVEL8lbl.Top + K_ALT_DEF;
     PL_NIVEL6lbl.Top  := PL_NIVEL7lbl.Top + K_ALT_DEF;
     PL_NIVEL5lbl.Top  := PL_NIVEL6lbl.Top + K_ALT_DEF;
     PL_NIVEL4lbl.Top  := PL_NIVEL5lbl.Top + K_ALT_DEF;
     PL_NIVEL3lbl.Top  := PL_NIVEL4lbl.Top + K_ALT_DEF;
     PL_NIVEL2lbl.Top  := PL_NIVEL3lbl.Top + K_ALT_DEF;
     PL_NIVEL1lbl.Top  := PL_NIVEL2lbl.Top + K_ALT_DEF;

     PL_NIVEL12.Top := PL_NIVEL1.Top;
     PL_NIVEL11.Top := PL_NIVEL12.Top + K_ALT_DEF;
     PL_NIVEL10.Top := PL_NIVEL11.Top + K_ALT_DEF;
     PL_NIVEL9.Top   := PL_NIVEL10.Top + K_ALT_DEF;
     PL_NIVEL8.Top  := PL_NIVEL9.Top + K_ALT_DEF;
     PL_NIVEL7.Top  := PL_NIVEL8.Top + K_ALT_DEF;
     PL_NIVEL6.Top  := PL_NIVEL7.Top + K_ALT_DEF;
     PL_NIVEL5.Top  := PL_NIVEL6.Top + K_ALT_DEF;
     PL_NIVEL4.Top  := PL_NIVEL5.Top + K_ALT_DEF;
     PL_NIVEL3.Top  := PL_NIVEL4.Top + K_ALT_DEF;
     PL_NIVEL2.Top  := PL_NIVEL3.Top + K_ALT_DEF;
     PL_NIVEL1.Top  := PL_NIVEL2.Top + K_ALT_DEF;

     PL_NIVEL12.TabOrder := 1;
     PL_NIVEL11.TabOrder := 2;
     PL_NIVEL10.TabOrder := 3;
     PL_NIVEL9.TabOrder := 4;
     PL_NIVEL8.TabOrder := 5;
     PL_NIVEL7.TabOrder := 6;
     PL_NIVEL6.TabOrder := 7;
     PL_NIVEL5.TabOrder := 8;
     PL_NIVEL4.TabOrder := 9;
     PL_NIVEL3.TabOrder := 10;
     PL_NIVEL2.TabOrder := 11;
     PL_NIVEL1.TabOrder := 12;

     Self.Height := K_FORMA_ACS;

     {$endif}
     PageControl.ActivePage := General;
     HelpContext:= H_EMP_EDIT_PLAZA;
     IndexDerechos := D_CONS_ORGANIGRAMA;
     FirstControl := PU_CODIGO;
     PU_CODIGO.LookUpDataSet  := dmCatalogos.cdsPuestos;
     with dmCliente do
     begin
          CB_CODIGO.LookupDataset  := cdsEmpleadoLookUp;
     end;

     with dmCatalogos do
     begin
          PL_CONTRAT.LookupDataset := cdsContratos;
          PL_PATRON.LookupDataset  := cdsRPatron;
          PL_TURNO.LookupDataset   := cdsTurnos;
          PL_CLASIFI.LookupDataset := cdsClasifi;
          PU_CODIGO.LookupDataset  := cdsPuestos;
          PL_AREA.LookupDataset    := cdsAreas;
          PL_TABLASS.LookupDataset := cdsSSocial;
     end;
     PL_NIVEL0.LookupDataset  := dmSistema.cdsNivel0;
     with dmTablas do
     begin
          {$ifdef ACS}
          PL_NIVEL12.LookupDataset := cdsNivel12;
          PL_NIVEL11.LookupDataset := cdsNivel11;
          PL_NIVEL10.LookupDataset := cdsNivel10;
          {$endif}
          PL_NIVEL9.LookupDataset  := cdsNivel9;
          PL_NIVEL8.LookupDataset  := cdsNivel8;
          PL_NIVEL7.LookupDataset  := cdsNivel7;
          PL_NIVEL6.LookupDataset  := cdsNivel6;
          PL_NIVEL5.LookupDataset  := cdsNivel5;
          PL_NIVEL4.LookupDataset  := cdsNivel4;
          PL_NIVEL3.LookupDataset  := cdsNivel3;
          PL_NIVEL2.LookupDataset  := cdsNivel2;
          PL_NIVEL1.LookupDataset  := cdsNivel1;
     end;
     with dmRecursos do
     begin
          PL_REPORTA.LookUpDataSet := cdsPlazasLookup;
          DataSource.Dataset := cdsPlazas;
     end;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditPlaza_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( EditMovtoKardexDlg_DevEx );
end;

procedure TEditPlaza_DevEx.FormShow(Sender: TObject);
begin
     inherited; //Connect
     if dmCliente.UsaPlazas then
     begin
          CB_CODIGO.Enabled :=  (dmRecursos.cdsPlazas.FieldByName('CB_CODIGO').AsInteger = 0);
          lbTitular.Enabled := CB_CODIGO.Enabled;
     end;

     PU_CODIGO.SetFocus;
     SetCamposNivel;
     ckVence.OnClick := Nil;
     RevisaControles;
     ckVence.OnClick := ckVenceClick;
     with dmRecursos do
     begin
          DataSource.Dataset := cdsPlazas;
          PL_REPORTA.Filtro := FiltroReportaAPlaza;
          if cdsPlazas.FieldByName('PL_REPORTA').AsInteger = 0 then
             PL_REPORTA.Llave := VACIO;
          if cdsPlazas.FieldByName('CB_CODIGO').AsInteger = 0 then
             CB_CODIGO.Llave := VACIO;
          sClasifi := cdsPlazas.FieldByName('PL_CLASIFI').AsString;
     end;
     ZetaBuscaEmpleado_DevEx.SetOnlyActivos( True );
     PL_NIVEL0.Enabled := dmSistema.HayNivel0;
     lblConfidencialidad.Enabled := PL_NIVEL0.Enabled;

     if PL_NIVEL0.Enabled then
     begin
         if StrLleno( dmCliente.Confidencialidad ) then
         begin
              PL_NIVEL0.Filtro :=  Format( 'TB_CODIGO in %s', [dmCliente.ConfidencialidadListaIN] ) ;
         end
         else
         begin
              PL_NIVEL0.Filtro := VACIO;
         end;
     end;


     PL_NOMINA.ListaFija:=lfTipoPeriodo; //acl
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}
//     dmRecursos.PuestoPlaza := VACIO;
end;

procedure TEditPlaza_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     ZetaBuscaEmpleado_DevEx.SetOnlyActivos( False );
end;

procedure TEditPlaza_DevEx.Connect;
begin
     //inherited;
     //FCambioEmpleado := False;
     with dmTablas do
     begin
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if PL_NIVEL1.Visible then cdsNivel1.Conectar;
          if PL_NIVEL2.Visible then cdsNivel2.Conectar;
          if PL_NIVEL3.Visible then cdsNivel3.Conectar;
          if PL_NIVEL4.Visible then cdsNivel4.Conectar;
          if PL_NIVEL5.Visible then cdsNivel5.Conectar;
          if PL_NIVEL6.Visible then cdsNivel6.Conectar;
          if PL_NIVEL7.Visible then cdsNivel7.Conectar;
          if PL_NIVEL8.Visible then cdsNivel8.Conectar;
          if PL_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if PL_NIVEL10.Visible then cdsNivel10.Conectar;
          if PL_NIVEL11.Visible then cdsNivel11.Conectar;
          if PL_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmRecursos.cdsPlazasLookup.Conectar;
     with dmSistema do
     begin
          cdsNivel0.Conectar;
     end;

     with dmCatalogos do
     begin
          cdsAreas.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
          cdsContratos.Conectar;
          cdsSSocial.Conectar;
     end;

     PL_AUTOSALClick(NIL);
end;

procedure TEditPlaza_DevEx.Agregar;
begin
     ZetaDialogo.zInformation(Self.Caption,'No puede agregar desde esta forma',0);
end;

procedure TEditPlaza_DevEx.Borrar;
begin
     ZetaDialogo.zInformation(Self.Caption,'No puede borrar desde esta forma',0);
{
     ER/CM: Solo se podr� borrar en organigrama donde los nodos del arbol permiten validar y borrar los hijos en cascada
     with dmRecursos do
     begin
          LlenaGridOrganigrama( cdsPlazas.FieldByName('PL_FOLIO').AsInteger );
          if cdsOrganigrama.RecordCount = 0 then //si no tiene subordinados
          begin
               if( cdsPlazas.FieldByName('CB_CODIGO').AsInteger = 0 )then //si tiene asignado un titular
               begin
                   cdsPlazas.Borrar;
               end
               else zError(Self.Caption,'No se puede borrar la plaza si tiene titular',0);
          end
          else zError(Self.Caption,'No se puede borrar la plaza si tiene subordinados',0);
     end;
}
end;

procedure TEditPlaza_DevEx.HabilitaControles;
begin
     inherited;
     btWizAsignar.Enabled :=( not Editing );
end;

procedure TEditPlaza_DevEx.RevisaControles;
begin
     with dmRecursos.cdsPlazas do
     begin
          ckVence.Checked := not( FieldByName('PL_FEC_FIN').AsDateTime = NullDateTime );
          lblVence.Enabled := ckVence.Checked;
     end;
end;

procedure TEditPlaza_DevEx.EscribirCambios;
begin
     with dmRecursos.cdsPlazas do
     begin
          if (not ckVence.Checked) then
          begin
               if ( State = dsBrowse ) then
                  Edit;
               FieldByName('PL_FEC_FIN').AsDateTime := NullDateTime;
          end;

          if ( FieldByName('CB_CODIGO').AsInteger <> 0 ) and ( ( iOldEmpleado = 0 ) or CambiaKardex ) then  // Se asign� empleado o se cambiaron datos de contrataci�n
          begin
               PostData;
               if not dmCliente.UsaPlazas then
                  inherited; //Enviar
               if ( State = dsBrowse ) AND dmCliente.UsaPlazas then
               begin
                    if EditMovtoKardexDlg_DevEx = nil then
                       EditMovtoKardexDlg_DevEx := TEditMovtoKardexDlg_DevEx.Create( Self );
                    with EditMovtoKardexDlg_DevEx do
                    begin
                        CodigoEmpleado := FieldByName('CB_CODIGO').AsInteger;
                        ShowModal;
                        if ( ModalResult = mrOk ) then
                        begin
                             if( FechaMovto <> NullDateTime )then
                             begin
                                  dmRecursos.ModificarPlaza(FechaMovto);
                             end;
                             {if ( ChangeCount > 0 ) then
                             begin
                                  Modo := dsEdit;
                             end;}
                        end;
                    end;
               end;
          end
          else inherited;//Enviar
     end;
end;

procedure TEditPlaza_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, PL_NIVEL1lbl, PL_NIVEL1 );
     SetCampoNivel( 2, iNiveles, PL_NIVEL2lbl, PL_NIVEL2 );
     SetCampoNivel( 3, iNiveles, PL_NIVEL3lbl, PL_NIVEL3 );
     SetCampoNivel( 4, iNiveles, PL_NIVEL4lbl, PL_NIVEL4 );
     SetCampoNivel( 5, iNiveles, PL_NIVEL5lbl, PL_NIVEL5 );
     SetCampoNivel( 6, iNiveles, PL_NIVEL6lbl, PL_NIVEL6 );
     SetCampoNivel( 7, iNiveles, PL_NIVEL7lbl, PL_NIVEL7 );
     SetCampoNivel( 8, iNiveles, PL_NIVEL8lbl, PL_NIVEL8 );
     SetCampoNivel( 9, iNiveles, PL_NIVEL9lbl, PL_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, PL_NIVEL10lbl, PL_NIVEL10 );
     SetCampoNivel( 11, iNiveles, PL_NIVEL11lbl, PL_NIVEL11 );
     SetCampoNivel( 12, iNiveles, PL_NIVEL12lbl, PL_NIVEL12 );
     {$endif}
end;

procedure TEditPlaza_DevEx.HabilitaFecha;
const
     K_UN_DIA = 1;
begin
     lblVence.Enabled := ckVence.Checked;
     PL_FEC_FIN.Enabled := lblVence.Enabled;
     if lblVence.Enabled and ( PL_FEC_FIN.Valor = NullDateTime ) then
     begin
          with dmRecursos.cdsPlazas do
          begin
               if ( State = dsBrowse ) then
                  Edit;
               FieldByName( 'PL_FEC_FIN').AsDateTime := NULLDATETIME;
               FieldByName( 'PL_FEC_FIN').AsDateTime := dmCliente.FechaDefault + K_UN_DIA;
          end;
     end;
     Modo := dsEdit;
end;

procedure TEditPlaza_DevEx.ckVenceClick(Sender: TObject);
begin
     inherited;
     HabilitaFecha;
end;

procedure TEditPlaza_DevEx.PU_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     if StrVacio( PL_CODIGO.Text )then
        PL_CODIGO.Text := PU_CODIGO.Llave + PL_FOLIO.Caption ;
     if StrVacio( PL_NOMBRE.Text )then
        PL_NOMBRE.Text := PU_CODIGO.Descripcion +' '+ StrZero( PL_FOLIO.Caption,3 );
end;

function TEditPlaza_DevEx.SetSalarioTabulador: Boolean;
begin
     Result := TRUE;
     if ( Modo <> dsInactive ) and ( Modo <> dsBrowse ) and PL_AUTOSAL.Checked then
        if StrLleno( dmRecursos.cdsPlazas.FieldByName( 'PL_CLASIFI' ).AsString ) then
           LLenaListaTabulador
        else
           Result := FALSE;
end;

procedure TEditPlaza_DevEx.PL_AUTOSALClick(Sender: TObject);
begin
     inherited;
     if not SetSalarioTabulador then
     begin
          ZError( self.Caption, 'Para Definir Salario X Tabulador, se debe Asignar la Clasificaci�n' + CR_LF + 'Revise sus Datos de Contrataci�n', 0 );
          PL_AUTOSAL.Checked:= FALSE;
     end;

     PL_SALARIO.Enabled := NOT PL_AUTOSAL.Checked;
     lblSalario.Enabled := PL_SALARIO.Enabled;
end;

procedure TEditPlaza_DevEx.LlenaListaTabulador;
var
   dSalario : TPesos;
   oLista: TStrings;
begin
     oLista := TStringList.Create;
     try
        with dmRecursos.cdsPlazas do
        begin
             dmRecursos.LlenaListaTabulador( oLista, FieldByName( 'PL_CLASIFI' ).AsString, dSalario );
             FieldByName('PL_SALARIO').AsFloat := dSalario;
        end;
     finally
            FreeAndNil(oLista);
     end;
end;

procedure TEditPlaza_DevEx.PL_CLASIFIExit(Sender: TObject);
begin
     inherited;
     if ( sClasifi <> PL_CLASIFI.Llave ) then
     begin
          SetSalarioTabulador;
          sClasifi := PL_CLASIFI.Llave;
     end;
end;

procedure TEditPlaza_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     InitOldValues; { Invoca que se refresque el valor de OldValues }
end;

procedure TEditPlaza_DevEx.InitOldValues;
var
   i : Integer;
begin
     with DataSource do
     begin
          if Assigned( DataSet ) and ( DataSet.State = dsBrowse ) then
          begin
               with DataSet do
               begin
                    iOldEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    for i := KA_NIVEL1 to {$ifdef ACS}KA_NIVEL12{$else}KA_NIVEL9{$endif} - 1 do   //El array esta hasta 12 cuando aOldValues llega hasta 11
                        aOldValues[i] := FieldByName( 'PL_NIVEL' + IntToStr(i) ).AsString;
                    aOldValues[KA_PUESTO] := FieldByName( 'PU_CODIGO' ).AsString;
                    aOldValues[KA_CLASIFI] := FieldByName( 'PL_CLASIFI' ).AsString;
                    aOldValues[KA_TURNO] := FieldByName( 'PL_TURNO' ).AsString;
               end;
          end;
     end;
end;

function TEditPlaza_DevEx.CambiaKardex: Boolean;
var
   i : Integer;
   iEmpleado: Integer;
begin
     Result := FALSE;
     if Assigned( DataSource.DataSet ) then
     begin
          with DataSource.DataSet do
          begin
               Result := ( aOldValues[KA_PUESTO] <> FieldByName( 'PU_CODIGO' ).AsString ) or
                         ( aOldValues[KA_TURNO] <> FieldByName( 'PL_TURNO' ).AsString );
               if ( not Result ) then  // Revisar niveles
               begin
                    for i := KA_NIVEL1 to {$ifdef ACS}KA_NIVEL12{$else}KA_NIVEL9{$endif} do
                        Result := Result or ( aOldValues[i] <> FieldByName( 'PL_NIVEL' + IntToStr(i) ).AsString );
               end;
               if ( not Result ) then  // Revisar Clasificacion
               begin
                    iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                    if ( aOldValues[KA_CLASIFI] <> FieldByName( 'PL_CLASIFI' ).AsString ) and ( iEmpleado <> 0 ) then
                    begin           // Revisa si el empleado tiene tabulador y no aplica el kardex
                         if ( not ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( iEmpleado ) ) = VACIO ) ) then
                            Result := ( not zStrToBool( dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_AUTOSAL' ).AsString ) );
                    end;
               end;
          end;
     end;
end;

procedure TEditPlaza_DevEx.btWizAsignarClick(Sender: TObject);
begin
     inherited;
     with dmRecursos.cdsPlazas do
     begin
          CambiarTitularPlaza_DevEx := TCambiarTitularPlaza_DevEx.Create( Self );
          try
             with CambiarTitularPlaza_DevEx do
             begin
                  EmpleadoActual := FieldByName('CB_CODIGO').AsInteger;
                  ShowModal;
             end;
          finally
                 FreeAndNil( CambiarTitularPlaza_DevEx );
          end;
     end;
end;

procedure TEditPlaza_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);

   procedure RevisaLookupEntero( oControlLookup: TZetaKeyLookup_DevEx );
   begin
        if ( oControlLookup.Valor <= 0 ) then
           oControlLookup.SetLlaveDescripcion( VACIO, VACIO );
   end;

begin
     inherited;
     if ( Field = nil ) then
     begin
          RevisaLookupEntero( CB_CODIGO );
          RevisaLookupEntero( PL_REPORTA );
     end;
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TEditPlaza_DevEx.PL_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL11.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEditPlaza_DevEx.PL_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString )and PL_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'PL_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TEditPlaza_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 271;
     K_TOP_N1_LBL = 275;
     K_TOP_N2 = 247;
     K_TOP_N2_LBL = 251;
     K_TOP_N3 = 223;
     K_TOP_N3_LBL = 227;
     K_TOP_N4 = 199;
     K_TOP_N4_LBL = 203;
     K_TOP_N5 = 175;
     K_TOP_N5_LBL = 179;
     K_TOP_N6 = 151;
     K_TOP_N6_LBL = 155;
     K_TOP_N7 = 127;
     K_TOP_N7_LBL = 131;
     K_TOP_N8 = 103;
     K_TOP_N8_LBL = 107;
     K_TOP_N9 = 79;
     K_TOP_N9_LBL = 83;
     K_TOP_N10 = 55;
     K_TOP_N10_LBL = 59;
     K_TOP_N11 = 31;
     K_TOP_N11_LBL = 35;
     K_TOP_N12 = 7;
     K_TOP_N12_LBL = 11;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     {Se obtiene la cantidad total de niveles no visibles}
     iNivelesNoVisibles := 0;
   //  iTotalHorizontal := 0;
     if Not PL_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not PL_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     PL_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     PL_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     PL_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     PL_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     PL_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     PL_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     PL_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     PL_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     PL_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     PL_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     PL_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     PL_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     PL_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     PL_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     PL_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     PL_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     PL_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     PL_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     PL_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     PL_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     PL_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     PL_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     PL_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     PL_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TEditPlaza_DevEx.LimpiaLookUpOlfKey;
begin
          PL_NIVEL1.ResetMemory;
          PL_NIVEL2.ResetMemory;
          PL_NIVEL3.ResetMemory;
          PL_NIVEL4.ResetMemory;
          PL_NIVEL5.ResetMemory;
          PL_NIVEL6.ResetMemory;
          PL_NIVEL7.ResetMemory;
          PL_NIVEL8.ResetMemory;
          PL_NIVEL9.ResetMemory;
          PL_NIVEL10.ResetMemory;
          PL_NIVEL11.ResetMemory;
          PL_NIVEL12.ResetMemory;
end;
{$endif}

function TEditPlaza_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se permite Imprimir en '+ self.Caption +'.';
     Result := False;
end;


procedure TEditPlaza_DevEx.SetEditarSoloActivos;
begin
     PU_CODIGO.EditarSoloActivos := TRUE;
     PL_CLASIFI.EditarSoloActivos := TRUE;
     PL_TABLASS.EditarSoloActivos := TRUE;
     PL_PATRON.EditarSoloActivos := TRUE;
     PL_CONTRAT.EditarSoloActivos := TRUE;
     PL_TURNO.EditarSoloActivos := TRUE;
     PL_AREA.EditarSoloActivos := TRUE;
     PL_NIVEL1.EditarSoloActivos := TRUE;
     PL_NIVEL2.EditarSoloActivos := TRUE;
     PL_NIVEL3.EditarSoloActivos := TRUE;
     PL_NIVEL4.EditarSoloActivos := TRUE;
     PL_NIVEL5.EditarSoloActivos := TRUE;
     PL_NIVEL6.EditarSoloActivos := TRUE;
     PL_NIVEL7.EditarSoloActivos := TRUE;
     PL_NIVEL8.EditarSoloActivos := TRUE;
     PL_NIVEL9.EditarSoloActivos := TRUE;
     PL_NIVEL10.EditarSoloActivos := TRUE;
     PL_NIVEL11.EditarSoloActivos := TRUE;
     PL_NIVEL12.EditarSoloActivos := TRUE;
end;

end.
