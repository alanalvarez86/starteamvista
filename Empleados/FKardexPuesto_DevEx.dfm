inherited KardexPuesto_DevEx: TKardexPuesto_DevEx
  Left = 270
  Top = 256
  Caption = 'Cambio de Puesto'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl_DevEx: TcxPageControl
    inherited General_DevEx: TcxTabSheet
      Caption = 'Puesto'
      object CB_RANGO_SLbl: TLabel
        Left = 246
        Top = 61
        Width = 72
        Height = 13
        Caption = 'Rango Salarial:'
        Visible = False
      end
      object ClasificacionLbl: TLabel
        Left = 47
        Top = 36
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object PuestoLbl: TLabel
        Left = 73
        Top = 12
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object CB_RANGO_S: TZetaDBNumero
        Left = 323
        Top = 56
        Width = 90
        Height = 21
        Mascara = mnTasa
        TabOrder = 2
        Text = '0.0 %'
        Visible = False
        DataField = 'CB_RANGO_S'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 113
        Top = 32
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 113
        Top = 8
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        OnExit = CB_PUESTOExit
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Top = 192
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
