unit FHisCreInfonavit_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, Db,
  ZetaDBGrid, ExtCtrls, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  THisCreInfonavit_DevEx = class(TBaseGridLectura_DevEx)
    KI_FECHA: TcxGridDBColumn;
    KI_TIPO: TcxGridDBColumn;
    KI_COMENTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  HisCreInfonavit_DevEx: THisCreInfonavit_DevEx;

implementation

{$R *.DFM}

uses dRecursos, dSistema, ZetaCommonLists, ZetaCommonClasses ;

procedure THisCreInfonavit_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;

     with dmRecursos do
     begin
          cdsHisCreInfonavit.Conectar;
          DataSource.DataSet:= cdsHisCreInfonavit;
     end;
end;

procedure THisCreInfonavit_DevEx.Refresh;
begin
     dmRecursos.cdsHisCreInfonavit.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;


procedure THisCreInfonavit_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_EMP_EXP_INFONAVIT;
     {***(@am): El cds de esta pantalla se utiliza para hacer un CloneCursor, no se logro
                hacer que GridMode conviviera con esta logica. Como la pantalla no cuenta con
                un gran numero de registros, se dejara sin GridMode.***}
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= False;
     {***}
end;

procedure THisCreInfonavit_DevEx.Agregar;
begin
     dmRecursos.cdsHisCreInfonavit.Agregar;
end;

procedure THisCreInfonavit_DevEx.Borrar;
begin
     dmRecursos.cdsHisCreInfonavit.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisCreInfonavit_DevEx.Modificar;
begin
     dmRecursos.cdsHisCreInfonavit.Modificar;
end;


procedure THisCreInfonavit_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;

end;

end.
