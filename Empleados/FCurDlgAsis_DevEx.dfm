inherited CurDlgAsis_DevEx: TCurDlgAsis_DevEx
  Left = 845
  Top = 205
  Caption = 'Lista de aprobados'
  ClientHeight = 355
  ClientWidth = 308
  OldCreateOrder = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel [0]
    Left = 12
    Top = 24
    Width = 290
    Height = 13
    Caption = #191' Desea agregar los empleados inscritos al curso ?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel [1]
    Left = 56
    Top = 8
    Width = 196
    Height = 13
    Caption = 'La lista de aprobados esta vac'#237'a. '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 319
    Width = 308
    inherited OK_DevEx: TcxButton
      Left = 141
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 221
    end
  end
  object cxGroupBox1: TcxGroupBox [3]
    Left = 32
    Top = 44
    Caption = ' Aplicar las siguientes condiciones '
    TabOrder = 1
    Height = 261
    Width = 249
    object lblEval1: TLabel
      Left = 52
      Top = 144
      Width = 72
      Height = 13
      Caption = 'Evaluaci'#243'n #1:'
    end
    object lblEval2: TLabel
      Left = 52
      Top = 168
      Width = 72
      Height = 13
      Caption = 'Evaluaci'#243'n #2:'
    end
    object lblEval3: TLabel
      Left = 52
      Top = 192
      Width = 72
      Height = 13
      Caption = 'Evaluaci'#243'n #3:'
    end
    object lblEvalFin: TLabel
      Left = 43
      Top = 216
      Width = 81
      Height = 13
      Caption = 'Evaluaci'#243'n Final:'
    end
    object chkValidarAsistencia: TcxCheckBox
      Left = 32
      Top = 22
      Caption = 'Validar Asistencia:'
      TabOrder = 0
      Transparent = True
      OnClick = chkValidarAsistenciaClick
      Width = 121
    end
    object chkValidarEvaluaciones: TcxCheckBox
      Left = 32
      Top = 120
      Caption = 'Validar Evaluaciones:'
      TabOrder = 3
      Transparent = True
      OnClick = chkValidarEvaluacionesClick
      Width = 129
    end
    object rdAsistenciaMinima: TcxRadioGroup
      Left = 41
      Top = 41
      Properties.Items = <
        item
          Caption = 'Asistencia Perfecta'
        end
        item
          Caption = 'Asistencia M'#237'nima   '
        end>
      ItemIndex = 0
      Style.Edges = []
      TabOrder = 1
      OnClick = rdAsistenciaMinimaClick
      Height = 41
      Width = 137
    end
    object cxLabel1: TcxLabel
      Left = 106
      Top = 88
      Caption = 'sesiones'
      Transparent = True
    end
    object IC_EVA_1: TZetaNumero
      Left = 129
      Top = 140
      Width = 57
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 4
      Text = '0.00'
    end
    object IC_EVA_2: TZetaNumero
      Left = 129
      Top = 164
      Width = 57
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 5
      Text = '0.00'
    end
    object IC_EVA_3: TZetaNumero
      Left = 129
      Top = 188
      Width = 57
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 6
      Text = '0.00'
    end
    object IC_EVA_FIN: TZetaNumero
      Left = 129
      Top = 212
      Width = 57
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 7
      Text = '0.00'
    end
    object NumSesiones: TZetaNumero
      Left = 69
      Top = 84
      Width = 36
      Height = 21
      Enabled = False
      Mascara = mnDias
      TabOrder = 2
      Text = '1'
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 15204368
  end
end
