unit FSaldaVacaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ZetaNumero, ZetaDBTextBox, DB,
  ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, ZBaseDlgModal_DevEx, ZetaCommonClasses,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, ImgList, cxButtons, cxRadioGroup;

type
  TSaldaVacaciones_DevEx = class(TZetaDlgModal_DevEx)
    GroupBox1: TGroupBox;
    SaldaBaja: TcxRadioButton;
    SaldaReingreso: TcxRadioButton;
    GroupBox2: TGroupBox;
    PagadosLbl: TLabel;
    DerechoPago: TZetaNumero;
    GozadosLbl: TLabel;
    DerechoGozo: TZetaNumero;
    PrimaLbl: TLabel;
    DerechoPrima: TZetaNumero;
    gbSaldos: TGroupBox;
    Label5: TLabel;
    ZSaldoGozo: TZetaTextBox;
    Label10: TLabel;
    ZSaldoPago: TZetaTextBox;
    ZSaldoPrima: TZetaTextBox;
    Label11: TLabel;
    EditaSaldo: TcxRadioButton;
    procedure SaldaBajaReingresoClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
      FCanClose: Boolean;
      FPrima,FGozo,FPago: TPesos;
      procedure SetInfoSaldos;
      procedure SetControlesEdicionSaldo;
      procedure LimpiaNewRecordVaca;
  public
    { Public declarations }
  end;

var
  SaldaVacaciones_DevEx: TSaldaVacaciones_DevEx;

implementation

uses DCliente,
     DRecursos,
     ZetaDialogo,
     FToolsRH,
     ZetaCommonLists;

{$R *.dfm}


procedure TSaldaVacaciones_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     HelpContext := H10153_Saldar_Vacaciones;
     SaldaReingreso.Checked:= TRUE;
     SetInfoSaldos;
     SetControlesEdicionSaldo;
     DerechoPago.Valor:= 0;
     DerechoGozo.Valor:= 0;
     DerechoPrima.Valor:= 0;
end;

procedure TSaldaVacaciones_DevEx.SaldaBajaReingresoClick(Sender: TObject);
const
     K_REINGRESO = 0;
     K_BAJA = 1;
     K_EDITAR = 2;
     aCaptionSaldo: array[FALSE..TRUE] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Saldo al Reingreso', 'Saldo a la Baja' );
begin
     inherited;
     with dmRecursos.cdsHisVacacion do
     begin
          if not ( State in [dsEdit, dsInsert ] ) then
             Edit;
          case TRadioButton( Sender ).Tag of
               K_REINGRESO: FieldByName('VA_FEC_INI').AsDateTime:= dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime;
               K_BAJA:
               begin
                    FieldByName('VA_FEC_INI').AsDateTime:= dmCliente.cdsEmpleado.FieldByName('CB_FEC_BAJ').AsDateTime;
               end;
               K_EDITAR: FieldByName('VA_FEC_INI').AsDateTime:= dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime;
          end;

          gbSaldos.Caption:= aCaptionSaldo[ TRadioButton( Sender ).Tag = K_BAJA ];
          SetInfoSaldos;
          SetControlesEdicionSaldo;
     end;
end;

procedure TSaldaVacaciones_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     FCanClose:= True;
     with dmRecursos do
     begin
          with cdsHisVacacion do
          begin
               DisableControls;
               try
                  FieldByName('VA_FEC_INI').AsDateTime:= dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime;
                  FieldByName( 'VA_FEC_FIN' ).AsDateTime := dmCliente.cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime;

                  if not ( EditaSaldo.Checked ) then
                  begin
                       FieldByName( 'VA_GOZO' ).AsFloat := FGozo;
                       FieldByName( 'VA_PAGO' ).AsFloat := FPago;
                       FieldByName( 'VA_P_PRIMA' ).AsFloat := FPrima;
                  end
                  else
                  begin
                       FieldByName( 'VA_GOZO' ).AsFloat := FGozo - DerechoPago.Valor;
                       FieldByName( 'VA_PAGO' ).AsFloat := FPago - DerechoGozo.Valor;
                       FieldByName( 'VA_P_PRIMA' ).AsFloat := FPrima - DerechoPrima.Valor;
                  end;


                  FieldByName( 'VA_COMENTA' ).AsString := K_AJUSTE_SALDO_REINGRESO;
                  FieldByName( 'VA_AJUSTE' ).AsString := K_GLOBAL_SI;
                  LimpiaNewRecordVaca;

                  Post;
                  Enviar;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TSaldaVacaciones_DevEx.CancelarClick(Sender: TObject);
begin
     if ZetaDialogo.zConfirm( Caption, 'Al cancelar los cambios, quedar� a�n un saldo pendiente de vacaciones ' + CR_LF +
                                       '�Esta seguro que desea salir del proceso?', 0, mbCancel ) then
     begin
          dmRecursos.cdsHisVacacion.CancelUpdates;
          FCanClose:= True;
          inherited;
     end
     else
         FCanClose:= False;
end;

procedure TSaldaVacaciones_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     CanClose:= FCanClose;
end;

procedure TSaldaVacaciones_DevEx.SetInfoSaldos;
begin
     dmRecursos.SetSaldosVacaciones( FALSE, FGozo, FPago, FPrima );

     zSaldoGozo.Caption:= FormatFloat( '#,0.00', FGozo );
     zSaldoPago.Caption:= FormatFloat( '#,0.00', FPago);
     zSaldoPrima.Caption:= FormatFloat( '#,0.00', FPrima);

end;

procedure TSaldaVacaciones_DevEx.SetControlesEdicionSaldo;
begin
     DerechoPago.Enabled:=  EditaSaldo.Checked;
     PagadosLbl.Enabled:= DerechoPago.Enabled;
     DerechoGozo.Enabled:=  EditaSaldo.Checked;
     GozadosLbl.Enabled:= DerechoGozo.Enabled;
     DerechoPrima.Enabled:=  EditaSaldo.Checked;
     PrimaLbl.Enabled:= DerechoPrima.Enabled;
end;



procedure TSaldaVacaciones_DevEx.LimpiaNewRecordVaca;
begin
     with dmRecursos.cdsHisVacacion do
     begin
          { Limpia todos los demas campos }
          if not( State in [dsEdit,dsInsert]  ) then
             Edit;
          FieldByName( 'CB_SALARIO' ).AsInteger  := 0;
          FieldByName( 'VA_NOMYEAR' ).AsInteger  := 0;
          FieldByName( 'VA_NOMTIPO' ).AsInteger  := Ord( tpDiario );
          FieldByName( 'VA_NOMNUME' ).AsInteger  := 0;
          FieldByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'VA_MONTO' ).AsFloat := 0;
          FieldByName( 'VA_SEVEN' ).AsFloat := 0;
          FieldByName( 'VA_PRIMA' ).AsFloat := 0;
          FieldByName( 'VA_TOTAL' ).AsFloat := 0;
     end;
end;

end.
