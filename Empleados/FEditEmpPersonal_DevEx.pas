unit FEditEmpPersonal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, ZetaKeyLookup_DevEx, StdCtrls, Mask, DBCtrls, Db,
  ComCtrls, ExtCtrls, ZetaKeyCombo,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC;

type
  TEditEmpPersonal_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    LEdoCivil: TLabel;
    LCB_LA_MAT: TLabel;
    LLugNac: TLabel;
    LNacio: TLabel;
    LResidencia: TLabel;
    LTransporte: TLabel;
    LHabita: TLabel;
    LViveCon: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    CB_PASAPOR: TDBCheckBox;
    CB_LUG_NAC: TDBEdit;
    CB_NACION: TDBEdit;
    CB_EDO_CIV: TZetaDBKeyLookup_DevEx;
    CB_LA_MAT: TDBEdit;
    CB_VIVECON: TZetaDBKeyLookup_DevEx;
    CB_VIVEEN: TZetaDBKeyLookup_DevEx;
    CB_MED_TRA: TZetaDBKeyLookup_DevEx;
    ZYear: TZetaNumero;
    ZMonth: TZetaNumero;
    TabDireccion: TcxTabSheet;
    LDireccion: TLabel;
    LColonia: TLabel;
    LCiudad: TLabel;
    LCP: TLabel;
    LEstado: TLabel;
    LZona: TLabel;
    LTelefono: TLabel;
    CB_TEL: TDBEdit;
    CB_ZONA: TDBEdit;
    CB_ESTADO: TZetaDBKeyLookup_DevEx;
    CB_CODPOST: TDBEdit;
    CB_CIUDAD: TDBEdit;
    CB_COLONIA: TDBEdit;
    CB_CALLE: TDBEdit;
    CB_COD_COL: TZetaDBKeyLookup_DevEx;
    LCB_CLINICA: TLabel;
    CB_CLINICA: TDBEdit;
    CB_DISCAPA: TDBCheckBox;
    CB_INDIGE: TDBCheckBox;
    CB_NUM_EXT: TDBEdit;
    CB_NUM_INT: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    CB_TDISCAP: TZetaDBKeyCombo;
    lblTDiscapacidad: TLabel;
    Label5: TLabel;
    CB_MUNICIP: TZetaDBKeyLookup_DevEx;
    TabContacto: TcxTabSheet;
    lblCorreo: TLabel;
    CB_E_MAIL: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure ZYearExit(Sender: TObject);
    procedure ZMonthExit(Sender: TObject);
    procedure CB_DISCAPAClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CB_ESTADOValidKey(Sender: TObject);
  private
    rMonthActual, rYearActual  : Real;
    procedure IniciaResidencia;
    procedure CambiaFechaResidencia;
    procedure SetControlDiscapa(lHabilitar: Boolean);
  protected
    procedure Connect; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EditEmpPersonal_DevEx: TEditEmpPersonal_DevEx;

implementation

uses dRecursos, dTablas, ZAccesosTress, ZetaCommonClasses,
     ZetaDialogo, ZetaTipoEntidad,ZImprimeForma,
     ZetaCommonTools, ZetaCommonLists,
     DGlobal,ZGlobalTress,
     ZetaClientTools;

{$R *.DFM}

procedure TEditEmpPersonal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_CURR_PERSONALES;
     FirstControl := CB_LUG_NAC;
     HelpContext:= H10121_Expediente_personales_empleado;
     TipoValorActivo1 := stEmpleado;
     CB_EDO_CIV.LookupDataset := dmTablas.cdsEstadoCivil;
     CB_VIVECON.LookupDataset := dmTablas.cdsViveCon;
     CB_VIVEEN.LookupDataset := dmTablas.cdsHabitacion;
     CB_MED_TRA.LookupDataset := dmTablas.cdsTransporte;
     CB_ESTADO.LookupDataset := dmTablas.cdsEstado;
     CB_COD_COL.LookupDataset := dmTablas.cdsColonia;
     CB_MUNICIP.LookupDataset := dmTablas.cdsMunicipios;

     //Verificar constante de AVENT
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          CB_DISCAPA.Visible := False;
          CB_INDIGE.Visible := False;
     end;//if
     
end;

procedure TEditEmpPersonal_DevEx.Connect;
begin
     with dmRecursos, dmTablas do
     begin
          cdsEstadoCivil.Conectar;
          cdsHabitacion.Conectar;
          cdsViveCon.Conectar;
          cdsTransporte.Conectar;
          cdsEstado.Conectar;
          cdsColonia.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
          cdsMunicipios.Conectar;
     end;
     IniciaResidencia;
     CB_ESTADOValidKey(Self);  
end;

procedure TEditEmpPersonal_DevEx.IniciaResidencia;
begin
     PageControl.ActivePage := TabGenerales;
     SetYearsMonths( DataSource.DataSet.FieldByName( 'CB_FEC_RES' ).AsDateTime,
                     rYearActual, rMonthActual );
     ZYear.Valor  := rYearActual;
     ZMonth.Valor := rMonthActual;
end;

procedure TEditEmpPersonal_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := TabGenerales;
end;

procedure TEditEmpPersonal_DevEx.ZYearExit(Sender: TObject);
begin
     if ( rYearActual <> ZYear.Valor ) then
     begin
          CambiaFechaResidencia;
          rYearActual := ZYear.Valor;
     end;
end;

procedure TEditEmpPersonal_DevEx.ZMonthExit(Sender: TObject);
begin
     if ( rMonthActual <> ZMonth.Valor ) then
     begin
          if ZMonth.Valor >= 12 then
          begin
             ZError('','Meses Tiene Que Ser Menor a 12',0);
             ActiveControl:= ZMonth;
          end
          else
          begin
               CambiaFechaResidencia;
               rMonthActual := ZMonth.Valor;
          end;
     end;
end;

procedure TEditEmpPersonal_DevEx.CambiaFechaResidencia;
begin
     with dmRecursos.cdsDatosEmpleado do
     begin
          if not Editing then
             Edit;
          FieldByName( 'CB_FEC_RES' ).AsDateTime:= FechaResidencia( ZYear.Valor, ZMonth.Valor );
     end;
end;

procedure TEditEmpPersonal_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TEditEmpPersonal_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     IniciaResidencia;
end;

procedure TEditEmpPersonal_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEditEmpPersonal_DevEx.CB_DISCAPAClick(Sender: TObject);
begin
     inherited;
     SetControlDiscapa(CB_Discapa.Checked );

end;

procedure TEditEmpPersonal_DevEx.SetControlDiscapa(lHabilitar:Boolean);
begin
     CB_TDISCAP.Enabled := lHabilitar;
     lblTDiscapacidad.Enabled := lHabilitar;
end;

procedure TEditEmpPersonal_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if Field = nil then
       SetControlDiscapa(CB_Discapa.Checked );
end;

procedure TEditEmpPersonal_DevEx.CB_ESTADOValidKey(Sender: TObject);
begin
     inherited;
     if StrLleno(CB_ESTADO.Llave) then
     begin
          CB_MUNICIP.Filtro := Format('TB_ENTIDAD = ''%s''',[CB_ESTADO.Llave]);
     end
     else
         CB_MUNICIP.Filtro := VACIO;
     if ( DataSource.DataSet <> nil ) and ( DataSource.DataSet.State = dsEdit) then
        CB_MUNICIP.Llave := VACIO;
end;

end.







