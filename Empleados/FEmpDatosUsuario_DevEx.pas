unit FEmpDatosUsuario_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, StdCtrls, DBCtrls, Mask,
  ZetaDBTextBox, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxGroupBox;

type
  TEmpDatosUsuario_DevEx = class(TBaseConsulta)
    PanelNoUsuario: TPanel;
    PanelEmpleado: TPanel;
    GroupBox1: TcxGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    US_FEC_IN: TZetaDBTextBox;
    US_FEC_OUT: TZetaDBTextBox;
    Label12: TLabel;
    US_FEC_SUS: TZetaDBTextBox;
    US_MAQUINA: TZetaDBTextBox;
    Label13: TLabel;
    US_BLOQUEA: TDBCheckBox;
    US_DENTRO: TDBCheckBox;
    GroupBox2: TcxGroupBox;
    Label7: TLabel;
    US_FORMATO: TZetaDBTextBox;
    Label2: TLabel;
    SUPERVISORES: TZetaTextBox;
    Label6: TLabel;
    AREAS: TZetaTextBox;
    lbRoles: TLabel;
    ROLES: TZetaTextBox;
    GroupBox3: TcxGroupBox;
    Label1: TLabel;
    US_CODIGO: TZetaDBTextBox;
    US_CORTO: TZetaDBTextBox;
    US_ACTIVO: TDBCheckBox;
    US_PORTAL: TDBCheckBox;
    Label3: TLabel;
    GR_CODIGO: TZetaDBTextBox;
    GR_NOMBRE: TZetaDBTextBox;
    US_LUGAR: TZetaDBTextBox;
    Label11: TLabel;
    Label4: TLabel;
    US_EMAIL: TZetaDBTextBox;
    Label5: TLabel;
    REPORTES: TZetaTextBox;
    Label8: TLabel;
    US_DOMAIN: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FConDerechos: Boolean;
  protected
    procedure Agregar; override;
    procedure Borrar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;

  public
    { Public declarations }
    procedure Connect; override;
    procedure Modificar; override;
    procedure Refresh; override;
  end;

var
  EmpDatosUsuario_DevEx: TEmpDatosUsuario_DevEx;

implementation

uses ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientDataSet,
     ZetaDialogo,
     DSistema,
     DCliente;

{$R *.dfm}

procedure TEmpDatosUsuario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_EMP_USUARIO;
end;

procedure TEmpDatosUsuario_DevEx.Connect;
 function GetCodigos( oDataset: TZetaClientDataSet; const sField: string ): string;
 begin
      Result := VACIO;
      with oDataset do
      begin
           Conectar;
           First;
           while NOT EOf do
           begin
                Result := ConcatString( Result, FieldByName(sField).AsString, ',' );
                Next;
           end;
      end;

 end;
 var oLista: TStringList;
     sMensaje : string;
begin
     dmCliente.cdsEmpleado.Conectar;
     with dmSistema do
     begin
          with cdsUsuarios do
          begin
               DisableControls;
               try
                  Conectar;
                  with dmCliente.cdsEmpleado.FieldByName('US_CODIGO') do
                  begin

                       if AsInteger = 0 then
                       begin
                            PanelNoUsuario.Visible := TRUE ;
                            sMensaje :='El empleado no tiene acceso a Sistema Tress';
                            FConDerechos := TRUE;
                       end
                       else
                       begin
                            PanelNoUsuario.Visible := NOT Locate( 'CM_CODIGO;CB_CODIGO', VarArrayOf([ dmCliente.Compania, dmCliente.cdsEmpleado.FieldByName('CB_CODIGO').AsInteger ]), [] );
                            sMensaje :='El usuario no tiene derecho a ver datos de este empleado' ;
                            FConDerechos := NOT PanelNoUsuario.Visible;
                       end;
                       PanelNoUsuario.Caption := sMensaje;
                  end;
               finally
                      EnableControls;
               end;
          end;

          PanelEmpleado.Visible := NOT PanelNoUsuario.Visible;
          if PanelEmpleado.Visible then
          begin
               oLista:= TStringList.Create;
               try
                  Reportes.Caption := GetCodigos( cdsSuscrip, 'RE_CODIGO' );

                  CargaListaSupervisores(oLista, TRUE);
                  Supervisores.Caption := GetCodigos( cdsUserSuper, 'CB_NIVEL' );

                  CargaListaAreas(oLista);
                  Areas.Caption := GetCodigos(cdsUserSuper, 'CB_AREA' );

                  {$ifdef DOS_CAPAS}
                  lbRoles.Visible := FALSE;
                  Roles.Visible := FALSE;
                  {$else}
                  Roles.Caption := GetCodigos( cdsUserRoles, 'RO_CODIGO');
                  {$endif}
               finally
                      oLista.Free;
               end;

          end;

          DataSource.DataSet:= cdsUsuarios;
     end;

end;

procedure TEmpDatosUsuario_DevEx.Agregar;
begin
     if ZConfirm( Caption, '� Desea darle Acceso a Sistema Tress al Empleado ?' , 0, mbYes ) then
        try
           dmSistema.AgregaEmpleadoUsuario
        finally
               DoConnect;
        end;

end;

procedure TEmpDatosUsuario_DevEx.Borrar;
begin
     if ZConfirm( Caption, '� Desea quitarle el Acceso a Sistema Tress al Empleado ?' , 0, mbNo ) then
        try
           dmSistema.BajaUsuario(dmCliente.cdsEmpleado.FieldByName('CB_CODIGO').AsInteger, TRUE);
        finally
               DoConnect;
        end;
end;

function TEmpDatosUsuario_DevEx.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     sMensaje := 'No tiene derechos para dar Acceso al Empleado';
     Result := FConDerechos and inherited PuedeAgregar(sMensaje);
     if Result then
     begin
          Result := dmCliente.cdsEmpleado.FieldByName('US_CODIGO').AsInteger = 0;
          if NOT Result then
             sMensaje := 'El Empleado ya tiene Acceso a Sistema Tress';
     end;
end;

function TEmpDatosUsuario_DevEx.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     sMensaje := 'No tiene derechos para Borrar Acceso al Empleado';
     Result := FConDerechos and inherited PuedeBorrar(sMensaje);
     if Result then
     begin
          with dmCliente.cdsEmpleado.FieldByName('US_CODIGO') do
          begin
               Result := AsInteger > 0;
               if NOT Result then
                  sMensaje := 'El Empleado no tiene Acceso a Sistema Tress';
          end;
     end;
end;

function TEmpDatosUsuario_DevEx.PuedeModificar( var sMensaje: String ): Boolean;
begin
     sMensaje := 'No tiene derechos para Modificar Acceso al Empleado';
     Result := FConDerechos and inherited PuedeModificar(sMensaje);
     if Result then
     begin
          with dmCliente.cdsEmpleado do
          begin
               Result := //zStrToBool( FieldByName('CB_ACTIVO').AsString ) AND
                         (FieldByName('US_CODIGO').AsInteger > 0);
               if NOT Result then
                  sMensaje := 'El Empleado no tiene Acceso a Sistema Tress';
          end;
     end;
end;

function TEmpDatosUsuario_DevEx.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     sMensaje := 'No tiene derechos para Imprimir Acceso al Empleado';
     Result := FConDerechos and inherited PuedeModificar(sMensaje);
end;

procedure TEmpDatosUsuario_DevEx.Modificar;
begin
     try
        with dmSistema do
        begin
             ModificaUsuarios(FALSE);
        end;
     finally;
             DoConnect;
     end;
end;

procedure TEmpDatosUsuario_DevEx.Refresh;
begin
     with dmSistema.cdsUsuarios do
     begin
          DisableControls;
          try
             Refrescar;
             DoConnect;
          finally
                 EnableControls;
          end;
     end;
end;




end.
