unit FReservaAula_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, StdCtrls, Buttons, Mask, ZetaFecha, Db,
  ExtCtrls, Grids, DBGrids, ImgList, ZetaDBGrid, ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxClasses,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
  ActnList, cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TReservaAula_DevEx = class(TBaseGridLectura_DevEx)
    pnFiltros: TPanel;
    lblFecIni: TLabel;
    zFechaIni: TZetaFecha;
    zFechaFin: TZetaFecha;
    lblFecFin: TLabel;
    lblAula: TLabel;
    AL_CODIGO: TZetaKeyLookup_DevEx;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    btnFiltrar: TcxButton;
    imgListConflicto: TcxImageList;
    ConflictoHiden: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure GridReservasDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure CONFLICTOCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
    function ValidaTipoReserva: Boolean;
    function ObtieneMensaje( const sPalabra: String ): String;
  protected
     procedure Connect; override;
     procedure Refresh; override;
     procedure Agregar; override;
     procedure Borrar; override;
     procedure Modificar; override;
     function PuedeBorrar(var sMensaje: String): Boolean; override;
     function PuedeModificar(var sMensaje: String): Boolean; override;
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     function PuedeAgregar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  ReservaAula_DevEx: TReservaAula_DevEx;

implementation

uses DRecursos,
     DCatalogos,
     DSistema,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     DCliente,
     FAutoClasses;

{$R *.DFM}
procedure TReservaAula_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_RESERVACION_DE_AULAS;
     AL_CODIGO.LookUpDataSet := dmCatalogos.cdsAulas;
end;

procedure TReservaAula_DevEx.FormShow(Sender: TObject);
begin
     inherited;
       ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
       ZetaDBGridDBTableView.OptionsView.GroupByBox := true;
     with dmRecursos do
     begin
          zFechaIni.Valor:= DiaDelMes( Date, True );
          zFechaFin.Valor:= DiaDelMes( Date, False );
     end;
     with dmCatalogos.cdsAulas do
     begin
          Conectar;
          AL_CODIGO.Llave := FieldByName('AL_CODIGO').AsString;
     end;
     DoRefresh;
end;

procedure TReservaAula_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     DataSource.DataSet:= dmRecursos.cdsReserva;
     if( dmRecursos.cdsReserva.HayQueRefrescar )then
         DoRefresh;
end;

procedure TReservaAula_DevEx.Refresh;
begin
     with dmRecursos do
     begin
          with ParametrosReservacion do
          begin               
               AddDate( 'FechaIniReserva', zFechaIni.Valor );
               AddDate( 'FechaFinReserva', zFechaFin.Valor );
	       AddString( 'Aula', AL_CODIGO.Llave );
	       AddInteger( 'FolioSesion', 0 );
          end;
         ObtieneReservaciones;
    end;
end;

procedure TReservaAula_DevEx.Agregar;
begin
     dmRecursos.cdsReserva.Agregar;
end;

procedure TReservaAula_DevEx.Borrar;
begin
     dmRecursos.cdsReserva.Borrar;
end;

procedure TReservaAula_DevEx.Modificar;
begin
     dmRecursos.cdsReserva.Modificar;
end;

function TReservaAula_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          if( inherited PuedeBorrar( sMensaje ) )then
          begin
               Result := ValidaTipoReserva;
               if not Result then
                  sMensaje := ObtieneMensaje( 'borrar' );
          end;
     end;
end;

function TReservaAula_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          if( inherited PuedeModificar( sMensaje ) )then
          begin
               Result := ValidaTipoReserva;
               if not Result then
                  sMensaje := ObtieneMensaje( 'modificar' );
          end;
     end;
end;

function TReservaAula_DevEx.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

procedure TReservaAula_DevEx.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

function TReservaAula_DevEx.ValidaTipoReserva: Boolean;
begin
     Result := ( dmRecursos.cdsReserva.FieldByName('RV_TIPO').AsInteger = Ord( rvBloqueo ) );
end;

function TReservaAula_DevEx.ObtieneMensaje( const sPalabra: String ): String;
begin
     Result :=  Format( 'Solo se pueden %s reservaciones de tipo Bloqueo', [ sPalabra ] );
end;

procedure TReservaAula_DevEx.GridReservasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
    // if ( Column.FieldName = 'CONFLICTO' ) and ( Column.Field.AsInteger > 0 ) then
     //   ( GridReservas.Canvas, Rect.left + 2, Rect.top + 1, 0 );
end;

procedure TReservaAula_DevEx.CONFLICTOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  with AViewInfo.ClientBounds do
    if( AViewInfo.GridRecord.values[ConflictoHiden.Index] >0)then
    begin
   imgListConflicto.Draw(ACanvas.Canvas,Left +1 , Top +1 , 0);
   ADone:=true;
   end
   else
   ADone:=false;
end;

end.
