unit FGridEmpCertific_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, Grids, DBGrids,
  ZetaDBGrid, DBCtrls, ZetaSmartLists, Buttons, ExtCtrls, ZetaKeyCombo,
  Mask, ZetaFecha,ZetaMessages, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TGridEmpCertific_DevEx = class(TBaseGridEdicion_DevEx)
    PanelEmp: TPanel;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    zComboSiNo: TZetaDBKeyCombo;
    zFecha: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure OK_DevExClick(Sender: TObject);

  private
    { Private declarations }
    procedure ValidaDatosCertific;
    procedure AsignaDatosCertific;
    procedure BuscaCertificacion;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  end;

var
  GridEmpCertific_DevEx: TGridEmpCertific_DevEx;

implementation

uses DCliente, dRecursos, dCatalogos,ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx,ZetaCommonTools,ZetaCommonLists;

{$R *.dfm}

{ TGridEditEmpCertific }

procedure TGridEmpCertific_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CertificacionesEmpleado;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     zComboSiNo.Lista.Add( K_GLOBAL_SI +'='+ K_PROPPERCASE_SI );
     zComboSiNo.Lista.Add( K_GLOBAL_NO +'='+ K_PROPPERCASE_NO );
end;


procedure TGridEmpCertific_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
          CB_CODIGO.SetLlaveDescripcion( IntToStr( Empleado ), GetDatosEmpleadoActivo.Nombre );  // Evita que se Haga el DoLookup del Control
     CB_CODIGO.SetFocus;
end;

procedure TGridEmpCertific_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmCatalogos.cdsCertificaciones.Conectar;
     with dmRecursos do
     begin
          cdsGridEmpCertific.Refrescar;
          DataSource.DataSet:= cdsGridEmpCertific;
     end;
end;


{Implementación de métodos privados}
procedure TGridEmpCertific_DevEx.ValidaDatosCertific;
begin
     if StrVacio( CB_CODIGO.Llave ) then
     begin
          ActiveControl := CB_CODIGO;
          DataBaseError( 'No Se Ha Especificado Un Empleado' );
     end;
end;

procedure TGridEmpCertific_DevEx.AsignaDatosCertific;
begin
     with dmRecursos.cdsGridEmpCertific do
     begin
          PostData;
          DisableControls;
          try
             First;
             while not EOF do
             begin
                  Edit;
                  FieldByName( 'CB_CODIGO' ).AsString := CB_CODIGO.Llave;
                  Next;
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TGridEmpCertific_DevEx.OKClick(Sender: TObject);
begin
     try
        ValidaDatosCertific;
        AsignaDatosCertific;
        inherited;
     except
        self.ModalResult:= mrNone;
        raise;
     end;
end;

procedure TGridEmpCertific_DevEx.Buscar;
begin
     inherited;
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CI_CODIGO' ) then
             BuscaCertificacion
     end;
end;

procedure TGridEmpCertific_DevEx.BuscaCertificacion;
var
   sKey, sKeyDesc: String;
begin
     with dmRecursos.cdsGridEmpCertific do
     begin
          sKey := FieldByName( 'CI_CODIGO' ).AsString;
          if dmCatalogos.cdsCertificaciones.Search_DevEx( '', sKey, sKeyDesc ) then
          begin
               if ( sKey <> FieldByName( 'CI_CODIGO' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'CI_CODIGO' ).AsString := sKey;
               end;
          end;
     end;
end;

procedure TGridEmpCertific_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'KI_FEC_CER' ) and ( zFecha.Visible ) then
                  zFecha.Visible:= FALSE
               else if ( SelectedField.FieldName = 'KI_APROBO' ) and ( zComboSiNo.Visible ) then
                   zComboSiNo.Visible := FALSE
          end;
     end;
end;

procedure TGridEmpCertific_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer;
          Column: TColumn; State: TGridDrawState);

   procedure AjustaControlEnCelda( oControl: TControl );
   begin
        with oControl do
        begin
             Left := Rect.Left + ZetaDBGrid.Left;
             Top := Rect.Top + ZetaDBGrid.Top;
             Width := Column.Width + 2;
             Visible := True;
        end;
   end;
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'KI_FEC_CER' ) then
             AjustaControlEnCelda( zFecha )
          else if ( Column.FieldName = 'KI_APROBO' ) then
             AjustaControlEnCelda( zComboSiNo )
     end;
end;

procedure TGridEmpCertific_DevEx.KeyPress(var Key: Char);

   function EsControlGrid: Boolean;
   begin
        Result := ( self.ActiveControl = zFecha ) or ( self.ActiveControl = zComboSiNo );
   end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'KI_FEC_CER' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'KI_APROBO' ) then
               begin
                    if ( zComboSiNo.Enabled ) then
                    begin
                         zComboSiNo.SetFocus;
                         SendMessage( zComboSiNo.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'CI_CODIGO' ) then
               begin
                    if ( ZetaCommonTools.EsMinuscula( Key ) )then
                       Key := Chr( Ord( Key ) - 32 );
               end;
          end;
     end
     else if EsControlGrid and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
     begin
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          end;
     end;
end;

procedure TGridEmpCertific_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) and
             ( StrVacio( dmRecursos.cdsGridEmpCertific.FieldByName( 'CI_CODIGO' ).AsString ) ) then
          begin
               if OK_DevEx.Enabled then
                  OK_DevEx.SetFocus
               else
                  Cancelar_DevEx.SetFocus;
          end
          else
          begin
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
               if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) then
                  SeleccionaSiguienteRenglon;
          end;
     end;
end;

procedure TGridEmpCertific_DevEx.OK_DevExClick(Sender: TObject);
begin
    try
        ValidaDatosCertific;
        AsignaDatosCertific;
        inherited;
     except
        self.ModalResult:= mrNone;
        raise;
     end;
end;
end.

