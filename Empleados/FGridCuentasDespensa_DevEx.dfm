inherited TarjetasDespensa_DevEx: TTarjetasDespensa_DevEx
  Left = 304
  Top = 194
  Caption = 'Tarjetas de Despensa'
  ClientHeight = 348
  ClientWidth = 577
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 312
    Width = 577
    inherited OK_DevEx: TcxButton
      Left = 410
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 490
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 577
    inherited ValorActivo2: TPanel
      Width = 251
      inherited textoValorActivo2: TLabel
        Width = 245
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 577
    Height = 262
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 300
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_CTA_VAL'
        Title.Caption = 'Cuenta Tarjeta Despensa'
        Width = 192
        Visible = True
      end>
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
