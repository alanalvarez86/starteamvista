unit FEvaluarSeleccion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids,
  ZetaDBGrid, DB, dxSkinsCore,  TressMorado2013,
  dxSkinsdxBarPainter, dxBar, cxClasses, ImgList, cxGraphics, 
  cxLookAndFeels, cxLookAndFeelPainters, Menus, cxButtons,
   dxSkinsDefaultPainters, 
  cxLabel, cxBarEditItem;

type
  TEvaluacionMultiple_DevEx = class(TZetaDlgModal_DevEx)
    Panel2: TPanel;
    dsEvaluacionMult: TDataSource;
    GridRenglones: TZetaDBGrid;
    cxImageList16Edicion: TcxImageList;
    DevEx_BarManagerEdicion: TdxBarManager;
    BarSuperior: TdxBar;
    dxBarButton_CambiarNivel: TdxBarButton;
    cxBarEditItem1: TcxBarEditItem;
    //procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //procedure CancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure dxBarButton_CambiarNivelClick(Sender: TObject);
    procedure GridRenglonesCellClick(Column: TColumn);
    procedure DevEx_BarManagerEdicionShowToolbarsPopup(
      Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
     procedure MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  private
    procedure GrabarEvaluaciones;
    { Private declarations }
  public
    procedure CargarDatos;
    { Public declarations }
  end;
  TDBGridHack = class(TDBGrid);
var
  EvaluacionMultiple_DevEx: TEvaluacionMultiple_DevEx;


implementation

uses
    DRecursos, ZetaClientDataSet,DCliente,DCatalogos,FTressShell,ZetaBusqueda_DevEx,ZetaDialogo,ZetaCommonClasses,ZetaCommonTools;

{$R *.dfm}

procedure TEvaluacionMultiple_DevEx.CargarDatos;
begin
     with dmRecursos.cdsEvaluacionSel do
     begin
          Init;
          Fields.Clear;  
          AddIntegerField('CB_CODIGO');
          AddStringField('CC_CODIGO',6);
          AddStringField('PRETTYNAME',150);
          AddStringField('CC_ELEMENT',100);
          AddStringField('NC_DESCRIP',50);
          AddIntegerField('NC_NIVEL');
          AddStringField('EC_COMENT',100);

          //CreateLookup(dmCatalogos.cdsCompNiveles,'NC_DESCRIP','CC_CODIGO;NC_NIVEL','CC_CODIGO;NC_NIVEL','NC_DESCRIP' );
          CreateTempDataset;
     end;
end;


{procedure TEvaluacionMultiple_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     GrabarEvaluaciones;
     Close;
end;}

procedure TEvaluacionMultiple_DevEx.GrabarEvaluaciones;
var
   Empleado,Nivel:Integer;
   Competencia,Observa:string;
   oCursor: TCursor;
procedure GrabarEvaluacion;
begin
     //Asignar empleado Activo

     with dmRecursos.cdsHisCompEvalua do
     begin
          Append;
          FieldByName('CB_CODIGO').AsInteger := Empleado;
          FieldByName('CC_CODIGO').AsString := Competencia;
          FieldByName('NC_NIVEL').AsInteger := Nivel;
          FieldByName('EC_COMENT').AsString := Observa;
          Post;
     end;
end;

begin
     with dmRecursos.cdsEvaluacionSel do
     begin
          DisableControls;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             First;
             dmRecursos.EvaluarMatriz := True;
             dmRecursos.cdsHisCompEvalua.Conectar;
             While Not Eof do
             begin
                  Empleado:=     FieldByName('CB_CODIGO').AsInteger;
                  Competencia := FieldByName('CC_CODIGO').AsString;
                  Nivel:=        FieldByName('NC_NIVEL').AsInteger;
                  Observa :=     FieldByName('EC_COMENT').AsString;
                  GrabarEvaluacion;
                  Next;
             end;
             dmRecursos.EvaluarMatriz := False;
             dmRecursos.cdsHisCompEvalua.Enviar;
          finally
                 EnableControls;
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TEvaluacionMultiple_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CargarDatos;
     //DevEx (by am):Colores Base para el grid de Edicion
    GridRenglones.FixedColor := RGB(235,235,235);//Gris
    GridRenglones.TitleFont.Color := RGB(77,59,75);//Gris-Morado tono fuerte
    GridRenglones.Font.Color := RGB(156,129,139);//Gris-Morado
    //DevEx (by am): Asigna el evento OnMouseWheel al grid de edicion
    TDBGridHack( GridRenglones ).OnMouseWheel:= MyMouseWheel;
end;

procedure TEvaluacionMultiple_DevEx.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with GridRenglones.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;

{procedure TEvaluacionMultiple_DevEx.CancelarClick(Sender: TObject);
begin
  inherited;
  Close;
end;}

procedure TEvaluacionMultiple_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmRecursos do
     begin
          dsEvaluacionMult.DataSet := cdsEvaluacionSel;
     end;
end;


procedure TEvaluacionMultiple_DevEx.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'NC_DESCRIP' ) and
                  ( dxBarButton_CambiarNivel.Enabled ) then
                  dxBarButton_CambiarNivel.Enabled:= False;
          end;
     end;
end;

procedure TEvaluacionMultiple_DevEx.GridRenglonesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
   iCurrentRow, iRecNo: Integer;
begin
     inherited;
     with GridRenglones, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( GridRenglones ).Row;
              if iCurrentRow = iRecNo then
              begin
                   //Color:= clBtnShadow;
                   Color:=  RGB(237,230,249);//RGB(156,129,139); //morado row seleccionado
                   Font.Color := clWindowText;
              end;
         end
         else if gdSelected in State then
         begin
              //Color:= clHighlight;
              Color:= RGB(212,188,251);//RGB(136,174,213); //Azul de celda seleccionado
              Font.Color := clHighlightText; //Texto en blanco para celda seleccionada
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end;
     {if (( gdFocused in State ) and ( dsEvaluacionMult.State = dsEdit ) )then
     begin
          if ( Column.FieldName = 'NC_DESCRIP' ) then
          begin
               with btnBuscarPerfil do
               begin
                    Left := Rect.Left + GridRenglones.Left + Column.Width - Width;
                    Top := Rect.Top;
                    Visible := True;
               end;
          end;
     end
     else if  (dsEvaluacionMult.DataSet.State in [dsBrowse ] )then
     begin
          btnBuscarPerfil.Visible := False;
     end; }

end;

procedure TEvaluacionMultiple_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TEvaluacionMultiple_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  GrabarEvaluaciones;
 // Close;
end;

procedure TEvaluacionMultiple_DevEx.dxBarButton_CambiarNivelClick(
  Sender: TObject);
var
   sKey,sDescrip :string;
begin
  inherited;
  dmRecursos.cdsEvaluacionSel.Edit;
  GridRenglones.SetFocus;

 with dmCatalogos do
     begin
          with cdsCompNiveles do
          begin
               Filtered := False;
               Filter := Format('CC_CODIGO = ''%s''',[dmRecursos.cdsEvaluacionSel.FieldByName('CC_CODIGO').AsString]);
               Filtered := True;
          end;
          ZetaBusqueda_DevEx.ShowSearchForm(cdsCompNiveles,cdsCompNiveles.Filter,sKey,sDescrip,False,True);

          if StrLleno(sKey )then
          begin
               with dmRecursos.cdsEvaluacionSel do
               begin
                    Edit;
                    FieldByName('NC_NIVEL').AsInteger := StrToInt(sKey);// cdsCompNiveles.FieldByName('NC_NIVEL').AsInteger;
                    FieldByName('NC_DESCRIP').AsString := sDescrip;
                    Post;
               end;
          end;
     end;
end;

procedure TEvaluacionMultiple_DevEx.GridRenglonesCellClick(
  Column: TColumn);
begin
  inherited;
  if Column.FieldName = 'NC_DESCRIP' then
     dxBarButton_CambiarNivel.Enabled:=TRUE
  else
     dxBarButton_CambiarNivel.Enabled:=FALSE;
end;

procedure TEvaluacionMultiple_DevEx.DevEx_BarManagerEdicionShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
  inherited;
  Abort;
end;


end.
