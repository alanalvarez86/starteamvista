unit FSesiones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls, Mask, ZetaFecha,
  Buttons, Grids, DBGrids, ZetaDBGrid, ZetaCommonClasses,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons, ZetaKeyLookup_DevEx;

type
TSesiones_DevEx = class(TBaseGridLectura_DevEx)
    pnFiltros: TPanel;
    lblFecIni: TLabel;
    lblFecFin: TLabel;
    zFechaIni: TZetaFecha;
    zFechaFin: TZetaFecha;
    Label1: TLabel;
    CU_CODIGO: TZetaKeyLookup_DevEx;
    lblMaestro: TLabel;
    MA_CODIGO: TZetaKeyLookup_DevEx;
    pnOperaciones: TPanel;
    btnInscripciones: TcxButton;
    btnListaAsistencia: TcxButton;
    btnAprobados: TcxButton;
    btnReservaciones: TcxButton;
    btnEvaluaciones: TcxButton;
    btnFiltrar: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure btnInscripcionesClick(Sender: TObject);
    procedure btnListaAsistenciaClick(Sender: TObject);
    procedure btnAprobadosClick(Sender: TObject);
    procedure btnReservacionesClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnEvaluacionesClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    //FParametros: TZetaParams;
    procedure Setcontroles( const lHabilita: Boolean );
    function HabilitaControles: Boolean;
  protected
     procedure Connect; override;
     procedure Refresh; override;
     procedure Agregar; override;
     procedure Borrar; override;
     procedure Modificar; override;
     function PuedeAgregar( var sMensaje: String ): Boolean; override;
     function PuedeBorrar( var sMensaje: String ): Boolean; override;
     function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  Sesiones_DevEx: TSesiones_DevEx;

implementation

uses DRecursos,
     DCatalogos,
     zAccesosMgr,
     ZAccesosTress,
     ZetaCommonTools,
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     DCliente,
     FAutoClasses;

{$R *.DFM}

procedure TSesiones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          MA_CODIGO.LookupDataSet := cdsMaestros;
          CU_CODIGO.LookUpDataSet := cdsCursos;
     end;
     HelpContext := H_SESIONES;

     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TSesiones_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetaDBGridDBTableView.Columns[0],0 ,'' ,skCount );
     inherited;
     with dmRecursos do
     begin
          zFechaIni.Valor:= DiaDelMes( Date, True );
          zFechaFin.Valor:= DiaDelMes( Date, False );
     end;
     DoRefresh;
       ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
       ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
end;

procedure TSesiones_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
	  cdsMaestros.Conectar;
     end;
     DataSource.DataSet:= dmRecursos.cdsSesion;
end;

procedure TSesiones_DevEx.Refresh;
begin
     with dmRecursos do
     begin
          with ParametrosSesion do
          begin
               AddString( 'Curso', CU_CODIGO.Llave );
               AddString( 'Maestro', MA_CODIGO.Llave );
               AddDate( 'FechaIniSesion', zFechaIni.Valor );
               AddDate( 'FechaFinSesion', zFechaFin.Valor );
          end;
         ObtieneSesiones;
         SetControles( HabilitaControles );
    end;
end;

procedure TSesiones_DevEx.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TSesiones_DevEx.Setcontroles( const lHabilita: Boolean );
begin
     btnInscripciones.Enabled := lHabilita;
     btnEvaluaciones.Enabled := lHabilita;
     btnListaAsistencia.Enabled := lHabilita and ( dmRecursos.cdsSesion.FieldbyName('SE_INSCRITO').AsInteger > 0 );
     btnAprobados.Enabled := lHabilita;
     btnReservaciones.Enabled := lHabilita;
end;

procedure TSesiones_DevEx.btnInscripcionesClick(Sender: TObject);
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          inherited;
          dmRecursos.EditarInscripciones;
     end;
end;

procedure TSesiones_DevEx.btnListaAsistenciaClick(Sender: TObject);
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          inherited;
          dmRecursos.EditarListaAsistencia;
     end;
end;

procedure TSesiones_DevEx.btnAprobadosClick(Sender: TObject);
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          inherited;
          dmRecursos.EditarAprobados;
     end;
end;

procedure TSesiones_DevEx.btnReservacionesClick(Sender: TObject);
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          inherited;
          dmRecursos.EditarGridReservacionesSesion;
     end;
end;

procedure TSesiones_DevEx.Agregar;
begin
     dmRecursos.cdsSesion.Agregar;
end;

procedure TSesiones_DevEx.Borrar;
begin
     dmRecursos.cdsSesion.Borrar;
end;

procedure TSesiones_DevEx.Modificar;
begin
     dmRecursos.cdsSesion.Modificar;
end;

procedure TSesiones_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if( Field = nil )then
     begin
          SetControles( HabilitaControles );
     end;
end;

procedure TSesiones_DevEx.btnEvaluacionesClick(Sender: TObject);
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          inherited;
          dmRecursos.EditarEvaluaciones;
     end;
end;

function TSesiones_DevEx.HabilitaControles:Boolean;
begin
     Result := (dmRecursos.cdsSesion.RecordCount > 0) and (CheckDerecho(D_CAT_CAPA_SESIONES,K_DERECHO_CAMBIO ) )
end;

{***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
function TSesiones_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TSesiones_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TSesiones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );

end;

procedure TSesiones_DevEx.SetEditarSoloActivos;
begin
     CU_CODIGO.EditarSoloActivos := TRUE;
     MA_CODIGO.EditarSoloActivos := TRUE;
end;
{*** FIN ***}

end.
