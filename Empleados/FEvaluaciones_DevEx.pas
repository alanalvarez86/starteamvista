unit FEvaluaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaDBTextBox, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  DBCtrls, ZetaSmartLists, Buttons, ExtCtrls, FInscritos_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEvaluaciones_devEx = class(TInscritos_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function PuedeAgregar: Boolean; override;
    function PuedeBorrar: Boolean; override;
    procedure EscribirCambios; override;
    procedure RefrescaInscritos; override;
  public
    { Public declarations }
  end;

var
  Evaluaciones_devEx: TEvaluaciones_devEx;

implementation

uses dRecursos,
     ZetaCommonClasses;

{$R *.DFM}

procedure TEvaluaciones_devEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_EDIT_EVALUACIONES;
end;

function TEvaluaciones_devEx.PuedeAgregar: Boolean;
begin
     Result := False;
end;

function TEvaluaciones_devEx.PuedeBorrar: Boolean;
begin
     Result := False;
end;

procedure TEvaluaciones_devEx.EscribirCambios;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmRecursos.cdsInscritos do
        begin
             Enviar;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEvaluaciones_devEx.RefrescaInscritos;
begin
     // NO HACER NADA, porque el refrescar de cdsInscritos se hace
     //en el Datamodule de DRecursos EditarEvaluaciones.
end;

end.

