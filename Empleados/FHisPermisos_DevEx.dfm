inherited HisPermisos_DevEx: THisPermisos_DevEx
  Left = 105
  Top = 235
  Caption = 'Historial de Permisos'
  ClientWidth = 654
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 654
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 328
      inherited textoValorActivo2: TLabel
        Width = 322
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 654
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object PM_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'PM_FEC_INI'
      end
      object PM_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'PM_DIAS'
      end
      object PM_FEC_FIN: TcxGridDBColumn
        Caption = 'Regresa'
        DataBinding.FieldName = 'PM_FEC_FIN'
      end
      object PM_CLASIFI: TcxGridDBColumn
        Caption = 'Clase'
        DataBinding.FieldName = 'PM_CLASIFI'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object PM_NUMERO: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'PM_NUMERO'
      end
      object PM_COMENTA: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'PM_COMENTA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
