inherited BancaElectronica_DevEx: TBancaElectronica_DevEx
  Caption = 'Banca Electr'#243'nica'
  ClientHeight = 323
  ClientWidth = 560
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 287
    Width = 560
    inherited OK_DevEx: TcxButton
      Left = 394
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 474
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 560
    inherited ValorActivo2: TPanel
      Width = 234
      inherited textoValorActivo2: TLabel
        Width = 228
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 560
    Height = 237
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 300
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_BAN_ELE'
        Title.Caption = 'Banca Electr'#243'nica'
        Width = 175
        Visible = True
      end>
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
