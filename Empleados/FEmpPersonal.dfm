inherited EmpPersonal: TEmpPersonal
  Left = 354
  Top = 419
  Caption = 'Datos Personales'
  ClientHeight = 274
  ClientWidth = 711
  PixelsPerInch = 96
  TextHeight = 13
  object LEdoCivil: TLabel [0]
    Left = 66
    Top = 82
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado Civil:'
  end
  object LCB_LA_MAT: TLabel [1]
    Left = 10
    Top = 101
    Width = 114
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lugar y A'#241'o Matrimonio:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object LLugNac: TLabel [2]
    Left = 37
    Top = 26
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lugar Nacimiento:'
  end
  object LNacio: TLabel [3]
    Left = 58
    Top = 45
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nacionalidad:'
  end
  object CB_LUG_NAC: TZetaDBTextBox [4]
    Left = 127
    Top = 24
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'CB_LUG_NAC'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_LUG_NAC'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NACION: TZetaDBTextBox [5]
    Left = 127
    Top = 43
    Width = 100
    Height = 17
    AutoSize = False
    Caption = 'CB_NACION'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_NACION'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_LA_MAT: TZetaDBTextBox [6]
    Left = 127
    Top = 99
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'CB_LA_MAT'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_LA_MAT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object LDireccion: TLabel [7]
    Left = 405
    Top = 26
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Calle:'
  end
  object LColonia: TLabel [8]
    Left = 393
    Top = 64
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Colonia:'
  end
  object LCiudad: TLabel [9]
    Left = 395
    Top = 158
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ciudad:'
  end
  object LCP: TLabel [10]
    Left = 363
    Top = 102
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Postal:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object LEstado: TLabel [11]
    Left = 395
    Top = 176
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado:'
  end
  object LZona: TLabel [12]
    Left = 367
    Top = 139
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Zona Ciudad:'
  end
  object LTelefono: TLabel [13]
    Left = 386
    Top = 212
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tel'#233'fono:'
  end
  object CB_CALLE: TZetaDBTextBox [14]
    Left = 435
    Top = 24
    Width = 210
    Height = 17
    AutoSize = False
    Caption = 'CB_CALLE'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CALLE'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_COLONIA: TZetaDBTextBox [15]
    Left = 435
    Top = 81
    Width = 210
    Height = 17
    AutoSize = False
    Caption = 'CB_COLONIA'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_COLONIA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_CIUDAD: TZetaDBTextBox [16]
    Left = 435
    Top = 156
    Width = 100
    Height = 17
    AutoSize = False
    Caption = 'CB_CIUDAD'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CIUDAD'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_CODPOST: TZetaDBTextBox [17]
    Left = 435
    Top = 100
    Width = 57
    Height = 17
    AutoSize = False
    Caption = 'CB_CODPOST'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CODPOST'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_ZONA: TZetaDBTextBox [18]
    Left = 435
    Top = 137
    Width = 65
    Height = 17
    AutoSize = False
    Caption = 'CB_ZONA'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_ZONA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_TEL: TZetaDBTextBox [19]
    Left = 435
    Top = 210
    Width = 100
    Height = 17
    AutoSize = False
    Caption = 'CB_TEL'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_TEL'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object LResidencia: TLabel [20]
    Left = 68
    Top = 156
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Residencia:'
  end
  object LTransporte: TLabel [21]
    Left = 70
    Top = 175
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Transporte:'
  end
  object LHabita: TLabel [22]
    Left = 84
    Top = 138
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vive En:'
  end
  object LViveCon: TLabel [23]
    Left = 78
    Top = 119
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vive Con:'
  end
  object Label1: TLabel [24]
    Left = 42
    Top = 64
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tiene Pasaporte:'
  end
  object ZCB_FEC_RES: TZetaTextBox [25]
    Left = 127
    Top = 154
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'ZCB_FEC_RES'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ZCB_VIVEEN: TZetaTextBox [26]
    Left = 127
    Top = 136
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'ZCB_VIVEEN'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ZCB_VIVECON: TZetaTextBox [27]
    Left = 127
    Top = 117
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'ZCB_VIVECON'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ZCB_MED_TRA: TZetaTextBox [28]
    Left = 127
    Top = 173
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'ZCB_MED_TRA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ZCB_ESTADO: TZetaTextBox [29]
    Left = 435
    Top = 174
    Width = 210
    Height = 17
    AutoSize = False
    Caption = 'ZCB_ESTADO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object ZCB_EDO_CIV: TZetaTextBox [30]
    Left = 127
    Top = 80
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'ZCB_EDO_CIV'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object CB_COD_COL: TZetaDBTextBox [31]
    Left = 435
    Top = 62
    Width = 210
    Height = 17
    AutoSize = False
    Caption = 'CB_COD_COL'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'TB_ELEM_COL'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lblClinica: TLabel [32]
    Left = 395
    Top = 121
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cl'#237'nica:'
  end
  object CB_CLINICA: TZetaDBTextBox [33]
    Left = 435
    Top = 119
    Width = 57
    Height = 17
    AutoSize = False
    Caption = 'CB_CLINICA'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_CLINICA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [34]
    Left = 383
    Top = 45
    Width = 48
    Height = 13
    Caption = '# Exterior:'
  end
  object Label3: TLabel [35]
    Left = 518
    Top = 45
    Width = 45
    Height = 13
    Caption = '# Interior:'
  end
  object CB_NUM_EXT: TZetaDBTextBox [36]
    Left = 435
    Top = 43
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_NUM_EXT'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CB_NUM_EXT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_NUM_INT: TZetaDBTextBox [37]
    Left = 565
    Top = 43
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'CB_NUM_INT'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CB_NUM_INT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label4: TLabel [38]
    Left = 16
    Top = 212
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Discapacidad:'
    FocusControl = CB_DISCAPA
  end
  object CB_TDISCAP: TZetaTextBox [39]
    Left = 127
    Top = 210
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'CB_TDISCAP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Label5: TLabel [40]
    Left = 383
    Top = 194
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Municipio:'
  end
  object ZCB_MUNICIP: TZetaTextBox [41]
    Left = 435
    Top = 192
    Width = 210
    Height = 17
    AutoSize = False
    Caption = 'ZCB_MUNICIP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Label6: TLabel [42]
    Left = 342
    Top = 231
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Correo electr'#243'nico:'
  end
  object CB_E_MAIL: TZetaDBTextBox [43]
    Left = 435
    Top = 229
    Width = 210
    Height = 17
    AutoSize = False
    Caption = 'CB_E_MAIL'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
    DataField = 'CB_E_MAIL'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CB_PASAPOR: TDBCheckBox [44]
    Left = 127
    Top = 62
    Width = 14
    Height = 17
    DataField = 'CB_PASAPOR'
    DataSource = DataSource
    ReadOnly = True
    TabOrder = 1
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited PanelIdentifica: TPanel
    Width = 711
    inherited Slider: TSplitter
      Left = 264
    end
    inherited ValorActivo1: TPanel
      Width = 248
      inherited textoValorActivo1: TLabel
        Width = 242
      end
    end
    inherited ValorActivo2: TPanel
      Left = 267
      Width = 444
      inherited textoValorActivo2: TLabel
        Width = 438
      end
    end
  end
  object CB_DISCAPA: TDBCheckBox [46]
    Left = 5
    Top = 192
    Width = 135
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Sufre una Discapacidad:'
    DataField = 'CB_DISCAPA'
    DataSource = DataSource
    ReadOnly = True
    TabOrder = 2
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object CB_INDIGE: TDBCheckBox [47]
    Left = 61
    Top = 228
    Width = 79
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Es Ind'#237'gena:'
    DataField = 'CB_INDIGE'
    DataSource = DataSource
    ReadOnly = True
    TabOrder = 3
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 276
    Top = 44
  end
end
