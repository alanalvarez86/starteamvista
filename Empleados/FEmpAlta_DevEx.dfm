inherited EmpAlta_DevEx: TEmpAlta_DevEx
  Left = 527
  Top = 127
  Caption = 'Alta de Empleado'
  ClientHeight = 652
  ClientWidth = 653
  OnKeyUp = FormKeyUp
  ExplicitWidth = 659
  ExplicitHeight = 681
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 616
    Width = 653
    Color = clWhite
    TabOrder = 1
    ExplicitTop = 616
    ExplicitWidth = 653
    inherited OK_DevEx: TcxButton
      Left = 488
      ExplicitLeft = 488
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 567
      ExplicitLeft = 567
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 653
    TabOrder = 0
    ExplicitWidth = 653
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 327
      ExplicitWidth = 327
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 321
        ExplicitLeft = 241
      end
    end
  end
  object PageControl: TcxPageControl [3]
    Left = 106
    Top = 50
    Width = 547
    Height = 566
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = TabGafeteBiometrico
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 564
    ClientRectLeft = 2
    ClientRectRight = 545
    ClientRectTop = 2
    object TabIdentifica: TcxTabSheet
      Caption = 'Identificaci'#243'n'
      TabVisible = False
      object LCB_APE_PAT: TLabel
        Left = 42
        Top = 78
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Apellido Paterno:'
        Color = clWhite
        ParentColor = False
      end
      object LCB_APE_MAT: TLabel
        Left = 40
        Top = 101
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Apellido Materno:'
        Color = clWhite
        ParentColor = False
      end
      object LCB_NOMBRES: TLabel
        Left = 71
        Top = 123
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre(s):'
        Color = clWhite
        ParentColor = False
      end
      object LCB_FEC_NAC: TLabel
        Left = 33
        Top = 170
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Nacimiento:'
        Color = clWhite
        ParentColor = False
      end
      object LRFC: TLabel
        Left = 89
        Top = 214
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C.:'
        Color = clWhite
        ParentColor = False
      end
      object LCB_SEGSOC: TLabel
        Left = 43
        Top = 237
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = '# Seguro Social:'
        Color = clWhite
        ParentColor = False
      end
      object LCB_CURP: TLabel
        Left = 77
        Top = 260
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'C.U.R.P.:'
        Color = clWhite
        ParentColor = False
      end
      object lblEmpleado: TLabel
        Left = 82
        Top = 55
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
        Color = clWhite
        ParentColor = False
      end
      object lblSexo: TLabel
        Left = 95
        Top = 146
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Sexo:'
        Color = clWhite
        ParentColor = False
      end
      object LEdad: TLabel
        Left = 264
        Top = 170
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Edad:'
      end
      object ZEdad: TZetaTextBox
        Left = 301
        Top = 168
        Width = 145
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object lblEntidad: TLabel
        Left = 27
        Top = 192
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Entidad Nacimiento:'
        Color = clWhite
        ParentColor = False
      end
      object bDigitoVerificador: TcxButton
        Left = 257
        Top = 233
        Width = 21
        Height = 21
        Hint = 'Calcula D'#237'gito Verificador'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFD0CCD0FFFDFDFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCD9DDFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B95FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA39BA3FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFFB8B2B9FFBEB8BFFFFAF9FAFF948B95FFF6F5
          F6FFCCC8CCFFAFA9B0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFF6F5F6FFF6F5
          F6FFFFFFFFFFEFEDEFFFFFFFFFFFFAF9FAFFF2F1F2FFFFFFFFFFA8A1A9FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A91
          9AFFFFFFFFFFBEB8BFFFCAC6CBFFF8F7F8FFA199A2FFF6F5F6FFD7D4D7FFBEB8
          BFFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF9A919AFFFFFFFFFFE9E7E9FFEFEDEFFFFFFFFFFFDBD7
          DBFFFDFDFDFFEFEDEFFFE4E1E4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFD3D0
          D4FFD7D4D7FFFBFBFBFFBEB8BFFFFAF9FAFFE0DDE0FFCECACEFFFFFFFFFFA8A1
          A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF9A919AFFFFFFFFFFD9D5D9FFDCD9DDFFFBFBFBFFC2BCC2FFFBFBFBFFE6E3
          E6FFD3D0D4FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFFFFFFFAF9FAFFF0EFF1FFF0EF
          F1FFF0EFF1FFF0EFF1FFF0EFF1FFFAF9FAFFFFFFFFFFA8A1A9FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFFFFF
          FFFFA49DA5FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFFFFF
          FFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFF9B939CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF9A919AFFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFFDFDFDFFDCD9DDFFC5C0
          C6FFC5C0C6FFC5C0C6FFC5C0C6FFC5C0C6FFD5D2D6FFFFFFFFFF9F97A0FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFC3BEC4FFF8F7F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9
          FAFFCECACEFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 12
        OnClick = bDigitoVerificadorClick
      end
      object CB_APE_PAT: TDBEdit
        Left = 132
        Top = 74
        Width = 220
        Height = 21
        DataField = 'CB_APE_PAT'
        DataSource = DataSource
        TabOrder = 1
        OnChange = PersonalesChange
      end
      object CB_APE_MAT: TDBEdit
        Left = 132
        Top = 97
        Width = 220
        Height = 21
        DataField = 'CB_APE_MAT'
        DataSource = DataSource
        TabOrder = 2
        OnChange = PersonalesChange
      end
      object CB_NOMBRES: TDBEdit
        Left = 132
        Top = 119
        Width = 220
        Height = 21
        DataField = 'CB_NOMBRES'
        DataSource = DataSource
        TabOrder = 3
        OnChange = PersonalesChange
      end
      object CB_CURP: TDBEdit
        Left = 132
        Top = 256
        Width = 190
        Height = 21
        CharCase = ecUpperCase
        DataField = 'CB_CURP'
        DataSource = DataSource
        TabOrder = 9
        OnChange = CB_RFCChange
      end
      object CB_CODIGO: TZetaDBNumero
        Left = 132
        Top = 51
        Width = 80
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 0
        Text = ''
        DataField = 'CB_CODIGO'
        DataSource = DataSource
      end
      object CB_FEC_NAC: TZetaDBFecha
        Left = 132
        Top = 165
        Width = 121
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 5
        Text = '18/Dec/97'
        Valor = 35782.000000000000000000
        OnExit = CB_FEC_NACExit
        OnValidDate = PersonalesChange
        DataField = 'CB_FEC_NAC'
        DataSource = DataSource
      end
      object CB_RFC: TDBEdit
        Left = 132
        Top = 210
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        DataField = 'CB_RFC'
        DataSource = DataSource
        TabOrder = 7
        OnChange = CB_RFCChange
      end
      object CB_SEGSOC: TDBEdit
        Left = 132
        Top = 233
        Width = 121
        Height = 21
        DataField = 'CB_SEGSOC'
        DataSource = DataSource
        TabOrder = 8
        OnChange = CB_RFCChange
      end
      object Panel1: TPanel
        Left = 344
        Top = 192
        Width = 100
        Height = 41
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 10
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_SEXO: TZetaKeyCombo
        Left = 132
        Top = 142
        Width = 121
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 4
        OnChange = CB_SEXOChange
        ListaFija = lfSexoDesc
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object BtnCandidato: TcxButton
        Left = 402
        Top = 48
        Width = 121
        Height = 24
        Hint = 'Buscar Candidato por Nombre'
        Caption = 'Buscar Candidato'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCECA
          CEFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB5AEB5FFFFFFFFFFFFFF
          FFFFE4E1E4FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFFFFF
          FFFFD2CED2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF908791FFEFEDEFFFFFFFFFFFFFFFFFFFE9E7
          E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFDBD7DBFFFFFFFFFFFFFFFFFFF8F7F8FF9A91
          9AFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFD9D5D9FFFDFD
          FDFFFFFFFFFFF4F3F4FFE4E1E4FFFFFFFFFFFFFFFFFFFFFFFFFFACA5ACFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C2C7FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9B939CFFFDFDFDFFFFFFFFFFEDEBEDFFB5AE
          B5FFA8A1A9FFBEB8BFFFF4F3F4FFFFFFFFFFFFFFFFFF988F99FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFD3D0D4FFFFFFFFFFEBE9EBFF908791FF8B81
          8CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFCAC6CBFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFB3ACB4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFC3BEC4FFFFFFFFFFE7E5E8FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFA69FA7FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EFF1FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFEFEDEFFFFFFFFFFFB8B2B9FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFE4E1E4FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFF4F3F4FF968D97FF8B81
          8CFF8B818CFF8B818CFF9F97A0FFFBFBFBFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFF4F3F4FFC2BC
          C2FFB7B0B7FFC7C2C7FFFAF9FAFFFFFFFFFFF2F1F2FF8F8590FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFF8F7F8FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFEBE9EBFF988F99FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFC5C0C6FFE2DF
          E2FFF0EFF1FFE0DDE0FFBCB6BDFF908791FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        OnClick = BtnCandidatoClick
      end
      object CB_ENT_NAC: TZetaDBKeyLookup_DevEx
        Left = 132
        Top = 188
        Width = 316
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_ENT_NAC'
        DataSource = DataSource
      end
    end
    object TabPersonales: TcxTabSheet
      Caption = 'Personales'
      TabVisible = False
      object LLugNac: TLabel
        Left = 102
        Top = 54
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar Nacimiento:'
        Color = clWhite
        ParentColor = False
      end
      object LNacio: TLabel
        Left = 123
        Top = 77
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nacionalidad:'
        Color = clWhite
        ParentColor = False
      end
      object LEdoCivil: TLabel
        Left = 130
        Top = 123
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado Civil:'
        Color = clWhite
        ParentColor = False
      end
      object LCB_LA_MAT: TLabel
        Left = 74
        Top = 146
        Width = 114
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar y A'#241'o Matrimonio:'
        Color = clWhite
        ParentColor = False
      end
      object LTransporte: TLabel
        Left = 134
        Top = 220
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Transporte:'
        Color = clWhite
        ParentColor = False
      end
      object Label23: TLabel
        Left = 100
        Top = 171
        Width = 88
        Height = 13
        Caption = 'Nombre del Padre:'
        Color = clWhite
        ParentColor = False
      end
      object Label24: TLabel
        Left = 89
        Top = 195
        Width = 99
        Height = 13
        Caption = 'Nombre de la Madre:'
        Color = clWhite
        ParentColor = False
      end
      object lblTDiscapacidad: TLabel
        Left = 81
        Top = 261
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Discapacidad:'
        Color = clWhite
        ParentColor = False
      end
      object CB_LUG_NAC: TDBEdit
        Left = 196
        Top = 50
        Width = 200
        Height = 21
        DataField = 'CB_LUG_NAC'
        DataSource = DataSource
        TabOrder = 0
      end
      object CB_NACION: TDBEdit
        Left = 196
        Top = 73
        Width = 100
        Height = 21
        DataField = 'CB_NACION'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_PASAPOR: TDBCheckBox
        Left = 106
        Top = 98
        Width = 103
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Tiene Pasaporte:'
        Color = clWhite
        DataField = 'CB_PASAPOR'
        DataSource = DataSource
        ParentColor = False
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_LA_MAT: TDBEdit
        Left = 196
        Top = 142
        Width = 200
        Height = 21
        DataField = 'CB_LA_MAT'
        DataSource = DataSource
        TabOrder = 4
      end
      object CB_MED_TRA: TZetaDBKeyLookup_DevEx
        Left = 196
        Top = 216
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_MED_TRA'
        DataSource = DataSource
      end
      object CB_EDO_CIV: TZetaDBKeyLookup_DevEx
        Left = 196
        Top = 119
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 30
        DataField = 'CB_EDO_CIV'
        DataSource = DataSource
      end
      object NombrePadre: TEdit
        Left = 196
        Top = 167
        Width = 200
        Height = 21
        MaxLength = 30
        TabOrder = 5
      end
      object NombreMadre: TEdit
        Left = 196
        Top = 191
        Width = 200
        Height = 21
        MaxLength = 30
        TabOrder = 6
      end
      object Panel2: TPanel
        Left = 422
        Top = 398
        Width = 100
        Height = 33
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 10
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_DISCAPA: TDBCheckBox
        Left = 70
        Top = 239
        Width = 139
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Sufre una Discapacidad:'
        Color = clWhite
        DataField = 'CB_DISCAPA'
        DataSource = DataSource
        ParentColor = False
        TabOrder = 8
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_DISCAPAClick
      end
      object CB_INDIGE: TDBCheckBox
        Left = 126
        Top = 281
        Width = 83
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Es Ind'#237'gena:   '
        Color = clWhite
        DataField = 'CB_INDIGE'
        DataSource = DataSource
        ParentColor = False
        TabOrder = 9
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_TDISCAP: TZetaDBKeyCombo
        Left = 196
        Top = 257
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 11
        ListaFija = lfDiscapacidadSTPS
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CB_TDISCAP'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object TabResidencia: TcxTabSheet
      Caption = 'Domicilio'
      TabVisible = False
      object LDireccion: TLabel
        Left = 115
        Top = 54
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Calle:'
        Color = clWhite
        ParentColor = False
      end
      object LColonia: TLabel
        Left = 103
        Top = 99
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Colonia:'
        Color = clWhite
        ParentColor = False
      end
      object LCiudad: TLabel
        Left = 105
        Top = 210
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
        Color = clWhite
        ParentColor = False
      end
      object LCP: TLabel
        Left = 73
        Top = 143
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo Postal:'
        Color = clWhite
        ParentColor = False
      end
      object LEstado: TLabel
        Left = 105
        Top = 232
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado:'
        Color = clWhite
        ParentColor = False
      end
      object LZona: TLabel
        Left = 77
        Top = 187
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona Ciudad:'
        Color = clWhite
        ParentColor = False
      end
      object LViveCon: TLabel
        Left = 95
        Top = 350
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive Con:'
        Color = clWhite
        ParentColor = False
      end
      object LHabita: TLabel
        Left = 101
        Top = 372
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive En:'
        Color = clWhite
        ParentColor = False
      end
      object Label19: TLabel
        Left = 23
        Top = 394
        Width = 118
        Height = 13
        Alignment = taRightJustify
        Caption = 'Residencia en la Ciudad:'
        Color = clWhite
        ParentColor = False
      end
      object Label11: TLabel
        Left = 204
        Top = 394
        Width = 24
        Height = 13
        Caption = 'A'#241'os'
      end
      object Label21: TLabel
        Left = 292
        Top = 394
        Width = 31
        Height = 13
        Caption = 'Meses'
      end
      object LCB_CLINICA: TLabel
        Left = 105
        Top = 165
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cl'#237'nica:'
        Color = clWhite
        ParentColor = False
      end
      object Label1: TLabel
        Left = 93
        Top = 77
        Width = 48
        Height = 13
        Caption = '# Exterior:'
        Color = clWhite
        ParentColor = False
      end
      object Label2: TLabel
        Left = 234
        Top = 77
        Width = 45
        Height = 13
        Caption = '# Interior:'
      end
      object Label10: TLabel
        Left = 93
        Top = 254
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Municipio:'
        Color = clWhite
        ParentColor = False
      end
      object CB_ESTADO: TZetaDBKeyLookup_DevEx
        Left = 149
        Top = 228
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 45
        OnValidKey = CB_ESTADOValidKey
        DataField = 'CB_ESTADO'
        DataSource = DataSource
      end
      object CB_CALLE: TDBEdit
        Left = 149
        Top = 50
        Width = 205
        Height = 21
        DataField = 'CB_CALLE'
        DataSource = DataSource
        TabOrder = 0
      end
      object CB_COLONIA: TDBEdit
        Left = 149
        Top = 117
        Width = 205
        Height = 21
        DataField = 'CB_COLONIA'
        DataSource = DataSource
        TabOrder = 4
      end
      object CB_CIUDAD: TDBEdit
        Left = 149
        Top = 206
        Width = 100
        Height = 21
        DataField = 'CB_CIUDAD'
        DataSource = DataSource
        TabOrder = 8
      end
      object CB_ZONA: TDBEdit
        Left = 149
        Top = 183
        Width = 100
        Height = 21
        DataField = 'CB_ZONA'
        DataSource = DataSource
        TabOrder = 7
      end
      object CB_CODPOST: TDBEdit
        Left = 149
        Top = 139
        Width = 100
        Height = 21
        DataField = 'CB_CODPOST'
        DataSource = DataSource
        TabOrder = 5
      end
      object CB_VIVECON: TZetaDBKeyLookup_DevEx
        Left = 149
        Top = 346
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 12
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_VIVECON'
        DataSource = DataSource
      end
      object CB_VIVEEN: TZetaDBKeyLookup_DevEx
        Left = 149
        Top = 368
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 13
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_VIVEEN'
        DataSource = DataSource
      end
      object ZYear: TZetaNumero
        Left = 149
        Top = 390
        Width = 45
        Height = 21
        Mascara = mnDias
        TabOrder = 14
        Text = '0'
        OnExit = ZYearExit
      end
      object ZMonth: TZetaNumero
        Left = 237
        Top = 390
        Width = 45
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 15
        Text = '0.00'
        OnExit = ZMonthExit
      end
      object Panel3: TPanel
        Left = 434
        Top = 178
        Width = 68
        Height = 33
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 16
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_COD_COL: TZetaDBKeyLookup_DevEx
        Left = 149
        Top = 95
        Width = 310
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_COD_COL'
        DataSource = DataSource
      end
      object CB_CLINICA: TDBEdit
        Left = 149
        Top = 161
        Width = 100
        Height = 21
        DataField = 'CB_CLINICA'
        DataSource = DataSource
        MaxLength = 3
        TabOrder = 6
        OnChange = PersonalesChange
      end
      object CB_NUM_EXT: TDBEdit
        Left = 149
        Top = 73
        Width = 80
        Height = 21
        DataField = 'CB_NUM_EXT'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_NUM_INT: TDBEdit
        Left = 282
        Top = 73
        Width = 80
        Height = 21
        DataField = 'CB_NUM_INT'
        DataSource = DataSource
        TabOrder = 2
      end
      object CB_MUNICIP: TZetaDBKeyLookup_DevEx
        Left = 149
        Top = 250
        Width = 260
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        WidthLlave = 45
        DataField = 'CB_MUNICIP'
        DataSource = DataSource
      end
      object gbContacto: TcxGroupBox
        Left = 24
        Top = 271
        Caption = ' Contacto: '
        TabOrder = 11
        Height = 64
        Width = 473
        object Label12: TLabel
          Left = 28
          Top = 36
          Width = 89
          Height = 13
          Alignment = taRightJustify
          Caption = 'Correo electr'#243'nico:'
          Color = clWhite
          ParentColor = False
        end
        object LTelefono: TLabel
          Left = 72
          Top = 12
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tel'#233'fono:'
          Color = clWhite
          ParentColor = False
        end
        object CB_TEL: TDBEdit
          Left = 125
          Top = 8
          Width = 100
          Height = 21
          DataField = 'CB_TEL'
          DataSource = DataSource
          TabOrder = 0
        end
        object CB_E_MAIL: TDBEdit
          Left = 125
          Top = 32
          Width = 310
          Height = 21
          Hint = 
            'Correo electr'#243'nico del empleado '#250'til para los procesos de Timbra' +
            'do de N'#243'mina ( capture en formato usuario@servidor.com ) '
          DataField = 'CB_E_MAIL'
          DataSource = DataSource
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
    end
    object TabExperiencia: TcxTabSheet
      Caption = 'Experiencia'
      TabVisible = False
      object Panel4: TPanel
        Left = 432
        Top = 104
        Width = 76
        Height = 33
        BevelOuter = bvNone
        TabOrder = 2
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 543
        Height = 223
        Align = alTop
        Caption = ' Experiencia Acad'#233'mica '
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object LEstudios: TLabel
          Left = 63
          Top = 21
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grado de Estudios:'
        end
        object Label9: TLabel
          Left = 102
          Top = 46
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Instituci'#243'n:'
        end
        object Label6: TLabel
          Left = 63
          Top = 70
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Instituci'#243'n:'
        end
        object LCarrera: TLabel
          Left = 63
          Top = 95
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Carrera Terminada:'
        end
        object Label7: TLabel
          Left = 5
          Top = 118
          Width = 148
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Documento Probatorio:'
        end
        object Label8: TLabel
          Left = 19
          Top = 143
          Width = 134
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o de Emisi'#243'n Documento:'
        end
        object CB_EST_HORlbl: TLabel
          Left = 71
          Top = 186
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Carrera y Horario:'
          Enabled = False
        end
        object CB_ESTUDIO: TZetaDBKeyLookup_DevEx
          Left = 159
          Top = 17
          Width = 365
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_ESTUDIO'
          DataSource = DataSource
        end
        object CB_CARRERA: TDBEdit
          Left = 159
          Top = 91
          Width = 365
          Height = 21
          DataField = 'CB_CARRERA'
          DataSource = DataSource
          TabOrder = 3
        end
        object CB_EST_HOY: TDBCheckBox
          Left = 51
          Top = 162
          Width = 121
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Estudia Actualmente:'
          DataField = 'CB_EST_HOY'
          DataSource = DataSource
          TabOrder = 6
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = CB_EST_HOYClick
        end
        object CB_EST_HOR: TDBEdit
          Left = 159
          Top = 182
          Width = 365
          Height = 21
          DataField = 'CB_EST_HOR'
          DataSource = DataSource
          Enabled = False
          TabOrder = 7
        end
        object CB_ESCUELA: TDBEdit
          Left = 159
          Top = 42
          Width = 365
          Height = 21
          DataField = 'CB_ESCUELA'
          DataSource = DataSource
          TabOrder = 1
        end
        object CB_TESCUEL: TZetaDBKeyCombo
          Left = 159
          Top = 66
          Width = 126
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          ListaFija = lfTipoInstitucionSTPS
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CB_TESCUEL'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_TITULO: TZetaDBKeyCombo
          Left = 159
          Top = 114
          Width = 126
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 4
          ListaFija = lfTipoDocProbatorioSTPS
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CB_TITULO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_YTITULO: TZetaDBNumero
          Left = 159
          Top = 139
          Width = 125
          Height = 21
          Mascara = mnDias
          TabOrder = 5
          Text = '0'
          DataField = 'CB_YTITULO'
          DataSource = DataSource
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 223
        Width = 543
        Height = 339
        Align = alClient
        Caption = ' Habilidades '
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        object CB_IDIOMAlbl: TLabel
          Left = 70
          Top = 39
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Idioma y Dominio:'
          Enabled = False
        end
        object LMaquinas: TLabel
          Left = 43
          Top = 63
          Width = 110
          Height = 13
          Alignment = taRightJustify
          Caption = 'M'#225'quinas que Conoce:'
        end
        object LExperiencia: TLabel
          Left = 95
          Top = 87
          Width = 58
          Height = 13
          Alignment = taRightJustify
          Caption = 'Experiencia:'
        end
        object CB_HABLA: TDBCheckBox
          Left = 64
          Top = 15
          Width = 108
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Habla Otro Idioma:'
          DataField = 'CB_HABLA'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = CB_HABLAClick
        end
        object CB_IDIOMA: TDBEdit
          Left = 158
          Top = 35
          Width = 365
          Height = 21
          DataField = 'CB_IDIOMA'
          DataSource = DataSource
          Enabled = False
          TabOrder = 1
        end
        object CB_MAQUINA: TDBEdit
          Left = 158
          Top = 59
          Width = 365
          Height = 21
          DataField = 'CB_MAQUINA'
          DataSource = DataSource
          TabOrder = 2
        end
        object CB_EXPERIE: TDBEdit
          Left = 158
          Top = 83
          Width = 365
          Height = 21
          DataField = 'CB_EXPERIE'
          DataSource = DataSource
          TabOrder = 3
        end
      end
    end
    object TabContratacion: TcxTabSheet
      Caption = 'Contrataci'#243'n'
      TabVisible = False
      object Label34: TLabel
        Left = 72
        Top = 79
        Width = 89
        Height = 13
        Caption = 'Antig'#252'edad desde:'
        Color = clWhite
        ParentColor = False
      end
      object ZAntiguedad: TZetaTextBox
        Left = 303
        Top = 77
        Width = 153
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object CB_PUESTOlbl: TLabel
        Left = 125
        Top = 195
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Color = clWhite
        ParentColor = False
      end
      object CB_TURNOlbl: TLabel
        Left = 130
        Top = 264
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
        Color = clWhite
        ParentColor = False
      end
      object CB_PATRONlbl: TLabel
        Left = 77
        Top = 287
        Width = 84
        Height = 13
        Caption = 'Registro Patronal:'
        Color = clWhite
        ParentColor = False
      end
      object CB_CLASIFIlbl: TLabel
        Left = 99
        Top = 241
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
        Color = clWhite
        ParentColor = False
      end
      object Label18: TLabel
        Left = 123
        Top = 55
        Width = 38
        Height = 13
        Caption = 'Ingreso:'
        Color = clWhite
        ParentColor = False
      end
      object CB_PLAZALBL: TLabel
        Left = 132
        Top = 218
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'P&laza:'
        Color = clWhite
        Enabled = False
        FocusControl = CB_PLAZA
        ParentColor = False
      end
      object Label4: TLabel
        Left = 83
        Top = 311
        Width = 78
        Height = 13
        Caption = 'Tipo de N'#243'mina:'
        Color = clWhite
        ParentColor = False
      end
      object bbMostrarCalendario: TcxButton
        Left = 469
        Top = 260
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnClick = bbMostrarCalendarioClick
      end
      object CB_FEC_ANT: TZetaDBFecha
        Left = 169
        Top = 74
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '16/Dec/97'
        Valor = 35780.000000000000000000
        DataField = 'CB_FEC_ANT'
        DataSource = DataSource
      end
      object GroupBox1: TcxGroupBox
        Left = 70
        Top = 98
        Caption = 'Contrato'
        ParentBackground = False
        ParentColor = False
        Style.Color = clWhite
        TabOrder = 2
        Height = 87
        Width = 409
        object CB_FEC_CONlbl: TLabel
          Left = 63
          Top = 13
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio:'
        end
        object CB_CONTRATlbl: TLabel
          Left = 67
          Top = 36
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object LVencimiento: TLabel
          Left = 30
          Top = 60
          Width = 61
          Height = 13
          Caption = 'Vencimiento:'
        end
        object CB_FEC_CON: TZetaDBFecha
          Left = 98
          Top = 8
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '16/Dec/97'
          Valor = 35780.000000000000000000
          DataField = 'CB_FEC_CON'
          DataSource = DataSource
        end
        object CB_CONTRAT: TZetaDBKeyLookup_DevEx
          Left = 98
          Top = 32
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          OnValidLookup = CB_CONTRATValidLookup
          DataField = 'CB_CONTRAT'
          DataSource = DataSource
        end
        object CB_FEC_COV: TZetaDBFecha
          Left = 98
          Top = 56
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '12/May/08'
          Valor = 39580.000000000000000000
          DataField = 'CB_FEC_COV'
          DataSource = DataSource
        end
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 191
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnExit = CB_PUESTOExit
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 237
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnExit = CB_CLASIFIExit
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object CB_TURNO: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 260
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_PATRON: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 283
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PATRON'
        DataSource = DataSource
      end
      object CB_FEC_ING: TZetaDBFecha
        Left = 169
        Top = 50
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '16/Dec/97'
        Valor = 35780.000000000000000000
        DataField = 'CB_FEC_ING'
        DataSource = DataSource
      end
      object Panel5: TPanel
        Left = 390
        Top = 340
        Width = 36
        Height = 33
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 9
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_PLAZA: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 214
        Width = 300
        Height = 21
        Enabled = False
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PLAZA'
        DataSource = DataSource
      end
      object CB_NOMINA: TZetaDBKeyCombo
        Left = 168
        Top = 307
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 8
        ListaFija = lfTipoPeriodoConfidencial
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'CB_NOMINA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object cxGroupBox1: TcxGroupBox
        Left = 70
        Top = 343
        Caption = ' Timbrado de N'#243'mina: '
        TabOrder = 11
        Height = 53
        Width = 409
        object Label16: TLabel
          Left = 22
          Top = 23
          Width = 69
          Height = 13
          Caption = 'R'#233'gimen SAT:'
          Color = clWhite
          ParentColor = False
        end
        object CB_REGIMEN: TZetaDBKeyCombo
          Left = 97
          Top = 19
          Width = 300
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfTipoRegimenesSAT
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'CB_REGIMEN'
          DataSource = DataSource
          LlaveNumerica = True
        end
      end
    end
    object TabArea: TcxTabSheet
      Caption = 'Area'
      TabVisible = False
      object CB_NIVEL1lbl: TLabel
        Left = 115
        Top = 54
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL2lbl: TLabel
        Left = 115
        Top = 78
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL3lbl: TLabel
        Left = 115
        Top = 102
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL4lbl: TLabel
        Left = 115
        Top = 126
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL5lbl: TLabel
        Left = 115
        Top = 150
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL6lbl: TLabel
        Left = 115
        Top = 174
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL7lbl: TLabel
        Left = 115
        Top = 198
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL8lbl: TLabel
        Left = 115
        Top = 222
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL9lbl: TLabel
        Left = 115
        Top = 246
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
        Color = clWhite
        ParentColor = False
      end
      object CB_NIVEL10lbl: TLabel
        Left = 109
        Top = 270
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Color = clWhite
        ParentColor = False
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 109
        Top = 294
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Color = clWhite
        ParentColor = False
        Visible = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 109
        Top = 318
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Color = clWhite
        ParentColor = False
        Visible = False
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 242
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL9ValidLookup
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 218
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL8ValidLookup
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 194
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL7ValidLookup
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 170
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL6ValidLookup
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 146
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL5ValidLookup
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 122
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL4ValidLookup
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 98
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL3ValidLookup
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 74
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL2ValidLookup
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 50
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object Panel6: TPanel
        Left = 16
        Top = 264
        Width = 100
        Height = 33
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 12
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 266
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL10ValidLookup
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 290
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL11ValidLookup
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 160
        Top = 314
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL12ValidLookup
        DataField = 'CB_NIVEL12'
      end
    end
    object TabSalario: TcxTabSheet
      Caption = 'Salario'
      TabVisible = False
      object CB_SALARIOlbl: TLabel
        Left = 159
        Top = 76
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario Diario:'
        Color = clWhite
        ParentColor = False
      end
      object CB_PER_VARlbl: TLabel
        Left = 116
        Top = 100
        Width = 108
        Height = 13
        Caption = 'Promedio de Variables:'
        Color = clWhite
        ParentColor = False
      end
      object CB_TABLASSlbl: TLabel
        Left = 115
        Top = 149
        Width = 109
        Height = 13
        Caption = 'Tabla de Prestaciones:'
        Color = clWhite
        ParentColor = False
      end
      object CB_ZONA_GElbl: TLabel
        Left = 141
        Top = 124
        Width = 83
        Height = 13
        Caption = 'Zona Geogr'#225'fica:'
        Color = clWhite
        ParentColor = False
      end
      object CB_RANGO_SLbl: TLabel
        Left = 152
        Top = 173
        Width = 72
        Height = 13
        Caption = 'Rango Salarial:'
        Color = clWhite
        ParentColor = False
      end
      object CB_SALARIO: TZetaDBNumero
        Left = 232
        Top = 72
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        OnExit = CB_SALARIOExit
        DataField = 'CB_SALARIO'
        DataSource = DataSource
      end
      object CB_PER_VAR: TZetaDBNumero
        Left = 232
        Top = 96
        Width = 90
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'CB_PER_VAR'
        DataSource = DataSource
      end
      object CB_TABLASS: TZetaDBKeyLookup_DevEx
        Left = 232
        Top = 145
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TABLASS'
        DataSource = DataSource
      end
      object CB_ZONA_GE: TDBComboBox
        Left = 232
        Top = 120
        Width = 60
        Height = 21
        Style = csDropDownList
        BevelKind = bkFlat
        Ctl3D = False
        DataField = 'CB_ZONA_GE'
        DataSource = DataSource
        Items.Strings = (
          'A'
          'B'
          'C')
        ParentCtl3D = False
        TabOrder = 3
      end
      object CB_AUTOSAL: TDBCheckBox
        Left = 119
        Top = 53
        Width = 126
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Salario por Tabulador:'
        Color = clWhite
        DataField = 'CB_AUTOSAL'
        DataSource = DataSource
        ParentColor = False
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_AUTOSALClick
      end
      object Panel7: TPanel
        Left = 360
        Top = 200
        Width = 100
        Height = 33
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 6
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
      object CB_RANGO_S: TZetaDBNumero
        Left = 232
        Top = 168
        Width = 90
        Height = 21
        Mascara = mnTasa
        TabOrder = 5
        Text = '0.0 %'
        OnExit = CB_RANGO_SExit
        DataField = 'CB_RANGO_S'
        DataSource = DataSource
      end
    end
    object TabPrestacion: TcxTabSheet
      Caption = 'Percepciones'
      TabVisible = False
      object Label20: TLabel
        Left = 58
        Top = 50
        Width = 151
        Height = 13
        Caption = 'Percepciones Disponibles:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object Label22: TLabel
        Left = 302
        Top = 50
        Width = 169
        Height = 13
        Caption = 'Percepciones Seleccionadas:'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object ZetaSmartListsButton2: TZetaSmartListsButton_DevEx
        Left = 249
        Top = 172
        Width = 26
        Height = 26
        Hint = 'Rechazar Registro'
        Enabled = False
        OptionsImage.Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818CA69FA7B1ABB28B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818CB1ABB2FAF9FAB7B0B78B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818CB7B0B7FDFDFDFFFFFFB7B0B78B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818CC0BAC0FFFFFFFFFFFFFF
          FFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CCCC8CCFFFF
          FFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E
          D5D2D6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C908791DBD7DBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C928993E4E1E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8D838ED9D5D9FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838ECECACE
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818CC7C2C7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B78B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFF
          FFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB1AB
          B2FDFDFDFFFFFFFFFFFFB7B0B78B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818CAAA3ABFAF9FAFFFFFFB7B0B78B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818CA69FA7F4F3F4B7B0B78B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C9D
          959EACA5AC8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Tipo = bsRechazar
        SmartLists = ZetaSmartLists
      end
      object ZetaSmartListsButton1: TZetaSmartListsButton_DevEx
        Left = 249
        Top = 141
        Width = 26
        Height = 26
        Hint = 'Escoger Registro'
        Enabled = False
        OptionsImage.Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818CB5AEB5B1ABB28B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FDFDFDBCB6BD
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
          B0B7FFFFFFFFFFFFC3BEC48B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFD9D5
          D98D838E8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFE0DDE09087918B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
          B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E1E4968D978B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFED
          EBED968D978B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFE4E1E49087918B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFD9D5D99087918B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
          B0B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3D0D48D838E8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFFFFFFFCAC6CB8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFFFFFFFFFFFFBEB8BF8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7B0B7FFFFFFFDFDFD
          B5AEB58B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CB7
          B0B7FAF9FAAFA9B08B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818CAFA9B0A69FA78B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Tipo = bsEscoger
        SmartLists = ZetaSmartLists
      end
      object ZetaSmartListsButton3: TZetaSmartListsButton_DevEx
        Left = 492
        Top = 141
        Width = 26
        Height = 26
        Hint = 'Subir Registro'
        Enabled = False
        OptionsImage.Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CACA5
          ACB7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7
          B0B7B7B0B7B7B0B7B7B0B7B1ABB28B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C9D959EF4F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF9FAA69FA78B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818CA69FA7FAF9FAFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDB1ABB28B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CAAA3ABFD
          FDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7B0B7
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818CB1ABB2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFBEB8BF8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CC7C2C7
          FFFFFFFFFFFFFFFFFFFFFFFFD5D2D68B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818CCECACEFFFFFFFFFFFFDBD7DB8D838E8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8D838ED9D5D9E4E1E49087918B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E9087
          918B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        Tipo = bsSubir
        SmartLists = ZetaSmartLists
      end
      object ZetaSmartListsButton4: TZetaSmartListsButton_DevEx
        Left = 492
        Top = 172
        Width = 26
        Height = 26
        Hint = 'Bajar Registro'
        Enabled = False
        OptionsImage.Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000C30E0000C30E000000000000000000008B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C908791968D978B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C908791E4E1E4EDEB
          ED968D978B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8D838E
          D9D5D9FFFFFFFFFFFFE4E1E49087918B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818CD3D0D4FFFFFFFFFFFFFFFFFFFFFFFFE0DDE08D838E8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818CCCC8CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9
          D5D98B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818CBEB8BFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFCCC8CC8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818CB5AEB5FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3BEC48B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CAFA9B0FDFDFDFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          BCB6BD8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818CA69F
          A7FAF9FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFDFDFDB1ABB28B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818CB1ABB2B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0
          B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B7B0B7B5AEB58B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C
          8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B81
          8C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B
          818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C8B818C}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        Tipo = bsBajar
        SmartLists = ZetaSmartLists
      end
      object LBDisponibles: TZetaSmartListBox_DevEx
        Left = 38
        Top = 78
        Width = 200
        Height = 180
        ItemHeight = 13
        TabOrder = 0
      end
      object LBSeleccion: TZetaSmartListBox_DevEx
        Left = 283
        Top = 78
        Width = 200
        Height = 180
        ItemHeight = 13
        TabOrder = 1
      end
      object Panel11: TPanel
        Left = 418
        Top = 282
        Width = 100
        Height = 17
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 2
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
    end
    object TabGafeteBiometrico: TcxTabSheet
      Caption = 'Gafete y biom'#233'trico'
      TabVisible = False
      object gbCodigo: TGroupBox
        Left = 4
        Top = 141
        Width = 533
        Height = 75
        Caption = ' Biom'#233'trico '
        TabOrder = 2
        object CB_ID_BIO: TZetaDBTextBox
          Left = 132
          Top = 18
          Width = 159
          Height = 21
          AutoSize = False
          Caption = 'CB_ID_BIO'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblCB_GP_COD: TLabel
          Left = 32
          Top = 46
          Width = 97
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grupo de terminales:'
          Enabled = False
          Visible = False
        end
        object lblNumeroBiometrico: TLabel
          Left = 17
          Top = 22
          Width = 112
          Height = 13
          Alignment = taRightJustify
          Caption = 'Identificador biom'#233'trico:'
          Enabled = False
          Visible = False
        end
        object btnAsignarBio: TcxButton
          Left = 297
          Top = 18
          Width = 21
          Height = 21
          Hint = 'Asignar identificador biom'#233'trico'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
            FFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFF
            FFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FF66D8
            A1FF66D8A1FF66D8A1FF66D8A1FFFFFFFFFFFFFFFFFFFFFFFFFF79DDACFF66D8
            A1FF66D8A1FF66D8A1FF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFF
            FFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFF
            FFFF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFF66D8A1FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = btnAsignarBioClick
        end
        object btnBorrarBio: TcxButton
          Left = 322
          Top = 18
          Width = 21
          Height = 21
          Hint = 'Borrar identificador biom'#233'trico'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A4050000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF6572D4FFDDE0F5FF6572D4FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF6572D4FFDDE0F5FF6572D4FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF6572D4FFEEEFFAFFFFFF
            FFFFEEEFFAFF6572D4FF4858CCFF4858CCFF4858CCFF6572D4FFEEEFFAFFFFFF
            FFFFEEEFFAFF6572D4FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFFD1D5F2FFFFFFFFFFFFFFFFFFFFFFFFFFEEEFFAFF6572D4FF4858CCFF6572
            D4FFEEEFFAFFFFFFFFFFFFFFFFFFFFFFFFFFD1D5F2FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF5968D1FFE2E5F7FFFFFFFFFFFFFFFFFFFFFF
            FFFFEEEFFAFF818CDCFFEEEFFAFFFFFFFFFFFFFFFFFFFFFFFFFFE2E5F7FF5968
            D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5968
            D1FFE2E5F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFE2E5F7FF5968D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF5968D1FFE2E5F7FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFE2E5F7FF5968D1FF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF7682
            D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7682D9FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF6572D4FFEEEFFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEEEFFAFF6572D4FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF6572D4FFEEEFFAFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEFFAFF6572D4FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF6572D4FFEEEFFAFFFFFF
            FFFFFFFFFFFFFFFFFFFFE2E5F7FF6A77D6FFE2E5F7FFFFFFFFFFFFFFFFFFFFFF
            FFFFEEEFFAFF6572D4FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFFD1D5F2FFFFFFFFFFFFFFFFFFFFFFFFFFE2E5F7FF5968D1FF4858CCFF5968
            D1FFE2E5F7FFFFFFFFFFFFFFFFFFFFFFFFFFD1D5F2FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF5968D1FFE2E5F7FFFFFFFFFFE2E5F7FF5968
            D1FF4858CCFF4858CCFF4858CCFF5968D1FFE2E5F7FFFFFFFFFFE2E5F7FF5968
            D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5968
            D1FFC6CBEFFF5968D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5968
            D1FFC6CBEFFF5968D1FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = btnBorrarBioClick
        end
        object CB_GP_COD: TZetaDBKeyLookup_DevEx
          Left = 132
          Top = 42
          Width = 213
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          Visible = False
          WidthLlave = 60
          DataSource = DataSource
        end
      end
      object GroupBox6: TGroupBox
        Left = 4
        Top = 4
        Width = 533
        Height = 61
        Caption = ' C'#243'digo de barras '
        TabOrder = 0
        object LTipoCreden: TLabel
          Left = 50
          Top = 25
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'Letra credencial:'
        end
        object CB_CREDENC: TDBEdit
          Left = 132
          Top = 21
          Width = 30
          Height = 21
          CharCase = ecUpperCase
          DataField = 'CB_CREDENC'
          DataSource = DataSource
          TabOrder = 0
        end
      end
      object GroupBox7: TGroupBox
        Left = 4
        Top = 75
        Width = 533
        Height = 58
        Caption = ' Proximidad '
        TabOrder = 1
        object Label3: TLabel
          Left = 57
          Top = 22
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero tarjeta:'
        end
        object CB_ID_NUM: TDBEdit
          Left = 132
          Top = 18
          Width = 211
          Height = 21
          DataField = 'CB_ID_NUM'
          DataSource = DataSource
          TabOrder = 0
          OnExit = CB_ID_NUMExit
        end
      end
      object Panel9: TPanel
        Left = 344
        Top = 222
        Width = 100
        Height = 41
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 3
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
    end
    object TabOtros: TcxTabSheet
      Caption = 'Otros'
      TabVisible = False
      object GBAsistencia: TGroupBox
        Left = 2
        Top = 155
        Width = 535
        Height = 48
        Caption = ' Asistencia '
        Color = clWhite
        ParentColor = False
        TabOrder = 3
        object CB_CHECA: TDBCheckBox
          Left = 35
          Top = 20
          Width = 103
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Checa asistencia:'
          DataField = 'CB_CHECA'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object GBEvaluacion: TGroupBox
        Left = 356
        Top = 155
        Width = 181
        Height = 125
        Caption = ' Evaluaci'#243'n '
        Color = clWhite
        ParentColor = False
        TabOrder = 4
        Visible = False
        object lblFechaEv: TLabel
          Left = 25
          Top = 30
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblResultadoEv: TLabel
          Left = 7
          Top = 54
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Resultado:'
        end
        object lblProximaEv: TLabel
          Left = 18
          Top = 78
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pr'#243'xima:'
        end
        object CB_LAST_EV: TZetaDBFecha
          Left = 62
          Top = 24
          Width = 105
          Height = 22
          Cursor = crArrow
          Opcional = True
          TabOrder = 0
          Text = '15/Dec/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_LAST_EV'
          DataSource = DataSource
        end
        object CB_EVALUA: TZetaDBNumero
          Left = 62
          Top = 50
          Width = 73
          Height = 21
          Mascara = mnPesosDiario
          TabOrder = 1
          Text = '0.00'
          DataField = 'CB_EVALUA'
          DataSource = DataSource
        end
        object CB_NEXT_EV: TZetaDBFecha
          Left = 62
          Top = 74
          Width = 105
          Height = 22
          Cursor = crArrow
          Opcional = True
          TabOrder = 2
          Text = '15/Dec/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_NEXT_EV'
          DataSource = DataSource
        end
      end
      object GBFonacot: TGroupBox
        Left = 356
        Top = 10
        Width = 181
        Height = 67
        Caption = ' Fonacot '
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        object Label5: TLabel
          Left = 5
          Top = 31
          Width = 10
          Height = 13
          Alignment = taRightJustify
          Caption = '#:'
        end
        object CB_FONACOT: TDBEdit
          Left = 18
          Top = 27
          Width = 158
          Height = 21
          DataField = 'CB_FONACOT'
          DataSource = DataSource
          TabOrder = 0
        end
      end
      object GBEmpleo: TGroupBox
        Left = 356
        Top = 84
        Width = 181
        Height = 71
        Caption = ' Programa de Primer Empleo '
        Color = clWhite
        ParentColor = False
        TabOrder = 2
        object CB_EMPLEO: TDBCheckBox
          Left = 31
          Top = 28
          Width = 111
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Aplica al programa:'
          DataField = 'CB_EMPLEO'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
      object GBInfonavit: TGroupBox
        Left = 2
        Top = 10
        Width = 351
        Height = 145
        Caption = ' Infonavit  '
        Color = clWhite
        ParentColor = False
        TabOrder = 0
        object CB_INFTASALbl: TLabel
          Left = 24
          Top = 109
          Width = 97
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor de Descuento:'
        end
        object CB_INFCREDLbl: TLabel
          Left = 60
          Top = 85
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = '# de Cr'#233'dito:'
        end
        object LblDescuento: TLabel
          Left = 210
          Top = 56
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = '     '
        end
        object Label13: TLabel
          Left = 35
          Top = 13
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Pr'#233'stamo:'
        end
        object CB_INF_OLDLbl: TLabel
          Left = 246
          Top = 28
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa Anterior:'
          Visible = False
        end
        object CB_INF_INILbl: TLabel
          Left = 23
          Top = 37
          Width = 98
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio de Descuento:'
        end
        object lblPorcentaje: TLabel
          Left = 376
          Top = 140
          Width = 8
          Height = 13
          Alignment = taRightJustify
          Caption = '%'
          Visible = False
        end
        object CB_INF_ANTLbl: TLabel
          Left = 4
          Top = 61
          Width = 117
          Height = 13
          Caption = 'Otorgamiento de Cr'#233'dito:'
        end
        object CB_INFMANT: TDBCheckBox
          Left = 296
          Top = 12
          Width = 11
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Mantenimiento:'
          DataField = 'CB_INFMANT'
          DataSource = DataSource
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Visible = False
        end
        object CB_INFCRED: TDBEdit
          Left = 124
          Top = 81
          Width = 187
          Height = 21
          DataField = 'CB_INFCRED'
          DataSource = DataSource
          TabOrder = 3
        end
        object CB_INFTIPO: TZetaDBKeyCombo
          Left = 124
          Top = 9
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          OnChange = CB_INFTIPOChange
          ListaFija = lfTipoInfonavit
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'CB_INFTIPO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object CB_INFTASA: TZetaDBNumero
          Left = 124
          Top = 105
          Width = 97
          Height = 21
          Enabled = False
          Mascara = mnTasa
          TabOrder = 4
          Text = '0.0 %'
          DataField = 'CB_INFTASA'
          DataSource = DataSource
        end
        object CB_INF_INI: TZetaDBFecha
          Left = 124
          Top = 33
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '15/Dec/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_INF_INI'
          DataSource = DataSource
        end
        object zkcCB_INF_OLD: TZetaKeyCombo
          Left = 280
          Top = 44
          Width = 23
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 6
          Visible = False
          OnChange = zkcCB_INF_OLDChange
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object CB_INFDISM: TDBCheckBox
          Left = 22
          Top = 126
          Width = 115
          Height = 17
          Alignment = taLeftJustify
          BiDiMode = bdLeftToRight
          Caption = 'Disminuci'#243'n Tasa %'
          DataField = 'CB_INFDISM'
          DataSource = DataSource
          ParentBiDiMode = False
          TabOrder = 7
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_INF_ANT: TZetaDBFecha
          Left = 124
          Top = 57
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '15/Dec/97'
          Valor = 35779.000000000000000000
          DataField = 'CB_INF_ANT'
          DataSource = DataSource
        end
      end
      object GroupBox4: TGroupBox
        Left = 2
        Top = 205
        Width = 535
        Height = 141
        Caption = ' Cuentas '
        Color = clWhite
        ParentColor = False
        TabOrder = 5
        object lblTarjDesp: TLabel
          Left = 34
          Top = 116
          Width = 87
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tarjeta Despensa:'
        end
        object lblTarjGas: TLabel
          Left = 41
          Top = 89
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tarjeta Gasolina:'
        end
        object btnVerTarjetaDesp: TcxButton
          Left = 338
          Top = 112
          Width = 21
          Height = 21
          Hint = 
            'Verificar si el N'#250'mero de Tarjeta de Despensa pertenece a otro E' +
            'mpleado'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFBAEDD5FFCEF2E1FF84DFB3FFE9F9F1FFE9F9
            F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE1F7ECFF7CDDAEFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF7FDEB0FFFFFFFFFFFFFF
            FFFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF69D8A2FFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFBDEED6FFD3F4E4FFE4F8EEFFC0EFD8FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFEFCFF5ED6
            9BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
            E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF58D498FFF4FCF8FFA2E7C6FF5BD59AFF92E3BCFF92E3BCFF92E3BCFF92E3
            BCFF8CE2B8FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF71DAA7FF8AE1B7FFB2EBD0FFD0F3E2FF5ED69BFF92E3
            BCFF92E3BCFF92E3BCFF92E3BCFF8FE2BAFF50D293FFD3F4E4FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF53D3
            95FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF92E3BCFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFADEACCFFF7FDFAFFFFFFFFFFFFFFFFFFCBF2DFFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9
            C9FF9DE6C2FFB2EBD0FF5BD59AFF92E3BCFFAAE9CAFFFFFFFFFFFFFFFFFFFFFF
            FFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF92E3BCFFA8E9C9FF87E0B5FF97E4BFFF5ED69BFF92E3BCFFADEA
            CCFFFFFFFFFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF69D8A2FFC8F1DDFF53D395FF50D2
            93FF50D293FF50D293FF7CDDAEFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF9DE6C2FFE1F7ECFFE9F9F1FFE9F9F1FFE9F9F1FFEFFBF5FFDEF7EBFF60D6
            9DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnVerTarjetaDespClick
        end
        object btnVerTarjetaGasolina: TcxButton
          Left = 338
          Top = 87
          Width = 21
          Height = 21
          Hint = 
            'Verificar si el N'#250'mero de Tarjeta de Gasolina pertenece a otro E' +
            'mpleado'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFBAEDD5FFCEF2E1FF84DFB3FFE9F9F1FFE9F9
            F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE1F7ECFF7CDDAEFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF7FDEB0FFFFFFFFFFFFFF
            FFFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF69D8A2FFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFBDEED6FFD3F4E4FFE4F8EEFFC0EFD8FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFEFCFF5ED6
            9BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
            E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF58D498FFF4FCF8FFA2E7C6FF5BD59AFF92E3BCFF92E3BCFF92E3BCFF92E3
            BCFF8CE2B8FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF71DAA7FF8AE1B7FFB2EBD0FFD0F3E2FF5ED69BFF92E3
            BCFF92E3BCFF92E3BCFF92E3BCFF8FE2BAFF50D293FFD3F4E4FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF53D3
            95FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF92E3BCFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFADEACCFFF7FDFAFFFFFFFFFFFFFFFFFFCBF2DFFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9
            C9FF9DE6C2FFB2EBD0FF5BD59AFF92E3BCFFAAE9CAFFFFFFFFFFFFFFFFFFFFFF
            FFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF92E3BCFFA8E9C9FF87E0B5FF97E4BFFF5ED69BFF92E3BCFFADEA
            CCFFFFFFFFFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF69D8A2FFC8F1DDFF53D395FF50D2
            93FF50D293FF50D293FF7CDDAEFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF9DE6C2FFE1F7ECFFE9F9F1FFE9F9F1FFE9F9F1FFEFFBF5FFDEF7EBFF60D6
            9DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnVerTarjetaGasolinaClick
        end
        object CB_CTA_VAL: TDBEdit
          Left = 123
          Top = 112
          Width = 214
          Height = 21
          DataField = 'CB_CTA_VAL'
          DataSource = DataSource
          TabOrder = 2
        end
        object CB_CTA_GAS: TDBEdit
          Left = 123
          Top = 87
          Width = 214
          Height = 21
          DataField = 'CB_CTA_GAS'
          DataSource = DataSource
          TabOrder = 1
        end
        object GroupBox5: TGroupBox
          Left = 10
          Top = 13
          Width = 517
          Height = 69
          TabOrder = 0
          object LBanca: TLabel
            Left = 22
            Top = 44
            Width = 90
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banca Electr'#243'nica:'
          end
          object lBanco: TLabel
            Left = 78
            Top = 20
            Width = 34
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco:'
          end
          object BtnBAN_ELE: TcxButton
            Left = 329
            Top = 40
            Width = 21
            Height = 21
            Hint = 
              'Verificar si el N'#250'mero de Banca Electr'#243'nica Pertenece a otro Emp' +
              'leado'
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A40500000000000000000000000000000000000050D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FFBAEDD5FFCEF2E1FF84DFB3FFE9F9F1FFE9F9
              F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE1F7ECFF7CDDAEFF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF7FDEB0FFFFFFFFFFFFFF
              FFFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF69D8A2FFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FFBDEED6FFD3F4E4FFE4F8EEFFC0EFD8FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFEFCFF5ED6
              9BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
              E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF58D498FFF4FCF8FFA2E7C6FF5BD59AFF92E3BCFF92E3BCFF92E3BCFF92E3
              BCFF8CE2B8FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF71DAA7FF8AE1B7FFB2EBD0FFD0F3E2FF5ED69BFF92E3
              BCFF92E3BCFF92E3BCFF92E3BCFF8FE2BAFF50D293FFD3F4E4FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF53D3
              95FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF92E3BCFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF50D293FF50D293FF50D2
              93FF50D293FF50D293FFADEACCFFF7FDFAFFFFFFFFFFFFFFFFFFCBF2DFFF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9
              C9FF9DE6C2FFB2EBD0FF5BD59AFF92E3BCFFAAE9CAFFFFFFFFFFFFFFFFFFFFFF
              FFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF92E3BCFFA8E9C9FF87E0B5FF97E4BFFF5ED69BFF92E3BCFFADEA
              CCFFFFFFFFFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF69D8A2FFC8F1DDFF53D395FF50D2
              93FF50D293FF50D293FF7CDDAEFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF9DE6C2FFE1F7ECFFE9F9F1FFE9F9F1FFE9F9F1FFEFFBF5FFDEF7EBFF60D6
              9DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtnBAN_ELEClick
          end
          object CB_BAN_ELE: TDBEdit
            Left = 114
            Top = 40
            Width = 214
            Height = 21
            DataField = 'CB_BAN_ELE'
            DataSource = DataSource
            TabOrder = 1
          end
          object CB_BANCO: TZetaDBKeyLookup_DevEx
            Left = 114
            Top = 15
            Width = 238
            Height = 21
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 60
            DataField = 'CB_BANCO'
            DataSource = DataSource
          end
        end
      end
      object GBNomina: TGroupBox
        Left = 2
        Top = 350
        Width = 535
        Height = 63
        Caption = 'Confidencialidad'
        Color = clWhite
        ParentColor = False
        TabOrder = 6
        object LblNivel0: TLabel
          Left = 40
          Top = 41
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confidencialidad:'
        end
        object lbNeto: TLabel
          Left = 26
          Top = 17
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Neto (Piramidaci'#243'n):'
        end
        object CB_NIVEL0: TZetaDBKeyLookup_DevEx
          Left = 123
          Top = 37
          Width = 238
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 55
          DataField = 'CB_NIVEL0'
          DataSource = DataSource
        end
        object CB_NETO: TZetaDBNumero
          Left = 123
          Top = 13
          Width = 97
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'CB_NETO'
          DataSource = DataSource
        end
      end
      object gbDatosMedicos: TGroupBox
        Left = 2
        Top = 415
        Width = 535
        Height = 69
        Caption = 'Datos M'#233'dicos'
        Color = clWhite
        ParentColor = False
        TabOrder = 7
        object Label14: TLabel
          Left = 45
          Top = 19
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Sangre:'
        end
        object Label15: TLabel
          Left = 5
          Top = 47
          Width = 116
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al'#233'rgico / Padecimiento:'
        end
        object CB_ALERGIA: TDBEdit
          Left = 123
          Top = 43
          Width = 337
          Height = 21
          DataField = 'CB_ALERGIA'
          DataSource = DataSource
          TabOrder = 1
        end
        object CB_TSANGRE: TDBComboBox
          Left = 123
          Top = 15
          Width = 193
          Height = 21
          BevelKind = bkFlat
          Ctl3D = False
          DataField = 'CB_TSANGRE'
          DataSource = DataSource
          Items.Strings = (
            ''
            'O +'
            'O -'
            'A +'
            'A -'
            'B +'
            'B -'
            'AB +'
            'AB -')
          ParentCtl3D = False
          TabOrder = 0
        end
        object Panel8: TPanel
          Left = 365
          Top = 20
          Width = 116
          Height = 13
          BevelOuter = bvNone
          Color = clWhite
          TabOrder = 2
          TabStop = True
          OnEnter = SaleUltimoCampo
        end
      end
    end
    object TabVacacion: TcxTabSheet
      Caption = 'Vacaciones'
      TabVisible = False
      object DiasGB: TGroupBox
        Left = 162
        Top = 84
        Width = 217
        Height = 141
        Caption = 'Datos de Vacaciones'
        Color = clWhite
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object FechaCierreLbl: TLabel
          Left = 24
          Top = 34
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha de Cierre:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object PagadosLbl: TLabel
          Left = 52
          Top = 57
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Por Pagar:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object GozadosLbl: TLabel
          Left = 52
          Top = 81
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Por Gozar:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object ZCierre: TZetaTextBox
          Left = 109
          Top = 32
          Width = 80
          Height = 17
          AutoSize = False
          Enabled = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object PrimaLbl: TLabel
          Left = 17
          Top = 105
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prima Vacacional:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object CB_DER_PAG: TZetaDBNumero
          Left = 109
          Top = 53
          Width = 50
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 0
          Text = '0.00'
          DataField = 'CB_DER_PAG'
          DataSource = DataSource
        end
        object CB_DER_GOZ: TZetaDBNumero
          Left = 109
          Top = 77
          Width = 50
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 1
          Text = '0.00'
          DataField = 'CB_DER_GOZ'
          DataSource = DataSource
        end
        object CB_DER_PV: TZetaDBNumero
          Left = 109
          Top = 101
          Width = 50
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 2
          Text = '0.00'
          DataField = 'CB_DER_PV'
          DataSource = DataSource
        end
      end
      object Panel12: TPanel
        Left = 388
        Top = 218
        Width = 100
        Height = 33
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        TabStop = True
        OnEnter = SaleUltimoCampo
      end
    end
  end
  object ListEmp: TcxListBox [4]
    Left = 0
    Top = 50
    Width = 106
    Height = 566
    Align = alLeft
    ItemHeight = 13
    Items.Strings = (
      'Identificaci'#243'n'
      'Personales'
      'Domicilio'
      'Experiencia'
      'Contrataci'#243'n'
      'Area'
      'Salario'
      'Percepciones'
      'Gafete y biom'#233'trico'
      'Otros'
      'Adicionales 1'
      'Adicionales 2'
      'Vacaciones')
    TabOrder = 4
    OnClick = ListEmpClick
  end
  inherited DataSource: TDataSource
    Left = 476
    Top = 2
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 35651808
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF98A1E2FFF6F7FDFFB7BEEBFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF8792DEFFDFE2F6FFA1A9E5FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8
          A1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF8CE2B8FFE9F9
          F1FF53D396FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF51D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6FDAA7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF51D395FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF59D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF61D79FFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4ED293FF53D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF8AE1B7FFE6F9F0FF69D9A4FF4ED293FF4ED293FF4ED2
          93FF5ED69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF51D395FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF64D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF67D8A2FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF69D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF51D395FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF6FDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF56D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF81DFB1FF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF5160CFFF6B78D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6E7BD7FF7482D9FF6370D4FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFF969FE2FF6370D4FF969FE2FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4656CCFF4656CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4656CCFF4656CCFF6E7BD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF6875D6FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF5463D0FF7482D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 160
    Top = 544
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 35651776
  end
  object ZetaSmartLists: TZetaSmartLists_DevEx
    BorrarAlCopiar = True
    CopiarObjetos = True
    ListaDisponibles = LBDisponibles
    ListaEscogidos = LBSeleccion
    Left = 512
    Top = 61
  end
end
