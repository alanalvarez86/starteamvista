unit FDatosPer_DevEx;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ZetaDBTextBox, ZetaKeyCombo, Mask, ZetaNumero,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxButtons, ImgList;

type
  TDatosPer_DevEx = class(TForm)
    Bevel1: TBevel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Panel1: TPanel;
    ZCodigo: TZetaTextBox;
    ZMonto: TZetaNumero;
    ZGravado: TZetaNumero;
    ZCriterio: TZetaKeyCombo;
    cxImageList24_PanelBotones: TcxImageList;
    Salir_DevEx: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure Salir_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses FKardexCambio_DevEx, ZetaCommonClasses;

{$R *.DFM}

procedure TDatosPer_DevEx.FormShow(Sender: TObject);
begin
     ZMonto.SetFocus;
end;

procedure TDatosPer_DevEx.Salir_DevExClick(Sender: TObject);
begin
     Close;
end;

end.
