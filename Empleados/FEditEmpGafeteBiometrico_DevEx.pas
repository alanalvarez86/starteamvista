unit FEditEmpGafeteBiometrico_DevEx;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, Vcl.ImgList, Data.DB, cxNavigator, cxDBNavigator, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxButtons, ZetaDBTextBox, ZetaKeyLookup_DevEx, Vcl.Mask,
  Vcl.DBCtrls;

type
  TEditEmpGafeteBiometrico_DevEx = class(TBaseEdicion_DevEx)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    CB_ID_NUM: TDBEdit;
    gbBiometrico: TGroupBox;
    lblNumeroBiometrico: TLabel;
    lblCB_GP_COD: TLabel;
    chkTieneHuella: TCheckBox;
    lblCantidadHuellas: TLabel;
    BtnEnrolar: TcxButton;
    btnBorrarHuellas: TcxButton;
    CB_GP_COD: TZetaDBKeyLookup_DevEx;
    btnAsignarBio: TcxButton;
    btnBorrarBio: TcxButton;
    CB_ID_BIO: TZetaDBTextBox;
    CANTIDAD_HUELLAS: TZetaDBTextBox;
    BtnBorrarFacial: TcxButton;
    LTipoCreden: TLabel;
    CB_CREDENC: TDBEdit;
    chHuella: TGroupBox;
    chHuellaFacial: TGroupBox;
    GroupBox5: TGroupBox;
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure btnBorrarBioClick(Sender: TObject);
    procedure btnAsignarBioClick(Sender: TObject);
    procedure BtnEnrolarClick(Sender: TObject);
    procedure btnBorrarHuellasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CB_ID_NUMExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnBorrarFacialClick(Sender: TObject);
  private
    { Private declarations }
{$ifndef DOS_CAPAS}
    procedure RegistroBiometrico;
    procedure Huellas;
    procedure VerificarDerechos;
{$endif}
  protected
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditEmpGafeteBiometrico_DevEx: TEditEmpGafeteBiometrico_DevEx;

procedure ShowFormaEdicionOtros( iDerecho : integer );

implementation

uses dRecursos, dSistema, dCliente, ZAccesosMgr, FEnrolamiento_DevEx, ZAccesosTress,
     ZetaDialogo, ZetaCommonClasses, ZetaCommonTools, ZetaCommonLists;

{$R *.dfm}

procedure ShowFormaEdicionOtros( iDerecho : integer );
begin
     if EditEmpGafeteBiometrico_DevEx = nil then
          EditEmpGafeteBiometrico_DevEx :=  TEditEmpGafeteBiometrico_DevEx.Create( Application );

     EditEmpGafeteBiometrico_DevEx.IndexDerechos := iDerecho;

     ZBaseEdicion_DevEX.ShowFormaEdicion( EditEmpGafeteBiometrico_DevEx, TEditEmpGafeteBiometrico_DevEx );
end;


procedure TEditEmpGafeteBiometrico_DevEx.CB_ID_NUMExit(Sender: TObject);
var
   sTarjeta: string;
begin
     sTarjeta := ZetaCommonTools.PreparaGafeteProximidad( CB_ID_NUM.Text );
     with dmRecursos.cdsDatosEmpleado do
     begin
          if ( sTarjeta <> FieldByName( 'CB_ID_NUM' ).AsString ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_ID_NUM' ).AsString := sTarjeta;
          end;
     end;
     Huellas;
end;

procedure TEditEmpGafeteBiometrico_DevEx.Connect;
begin
     {$ifndef DOS_CAPAS}
     with dmSistema do
     begin
         cdsListaGrupos.Conectar;
         CB_GP_COD.LookUpDataSet := cdsListaGrupos;
     end;
     {$endif}

     with dmRecursos do
     begin
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
end;

procedure TEditEmpGafeteBiometrico_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
end;

procedure TEditEmpGafeteBiometrico_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     VerificarDerechos;
end;

procedure TEditEmpGafeteBiometrico_DevEx.VerificarDerechos;
begin
     lblNumeroBiometrico.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL11 ); // SYNERGY
     CB_ID_BIO.DataField := 'CB_ID_BIO'; // SYNERGY
     CB_ID_BIO.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     CB_GP_COD.DataField := 'CB_GP_COD'; // SYNERGY
     btnAsignarBio.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     btnBorrarBio.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     lblCB_GP_COD.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     BtnEnrolar.Enabled := True; // BIOMETRICO
     btnBorrarHuellas.Enabled := True; // BIOMETRICO
     CB_GP_COD.Enabled := ( lblNumeroBiometrico.Enabled and ( CB_ID_BIO.Caption <> '0' ) ); // SYNERGY
     chkTieneHuella.Checked := dmSistema.TieneHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger,dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger);
     lblCantidadHuellas.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL12 ); // SYNERGY
     CANTIDAD_HUELLAS.Enabled := lblCantidadHuellas.Enabled; // SYNERGY
     CANTIDAD_HUELLAS.Caption := IntToStr (dmSistema.CantidadHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger , dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger, 0));

     // BIOMETRICO
     RegistroBiometrico;
     if BtnBorrarFacial.Enabled then
        BtnBorrarFacial.Enabled := chkTieneHuella.Checked; //Verifica si hay plantillas.

     chHuella.Enabled := (BtnEnrolar.Enabled and btnBorrarHuellas.Enabled) or (BtnEnrolar.Enabled) or (btnBorrarHuellas.Enabled);
end;

procedure TEditEmpGafeteBiometrico_DevEx.OK_DevExClick(Sender: TObject);
begin
     with dmRecursos.cdsDatosEmpleado do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;

     inherited;
     {$ifdef DOS_CAPAS}
     Close; //Ya no se cerrara para que puedan enrolar su huella.
     {$endif}
     Huellas;
     VerificarDerechos;
end;

procedure TEditEmpGafeteBiometrico_DevEx.EscribirCambios;
begin
     inherited EscribirCambios;
end;

procedure TEditEmpGafeteBiometrico_DevEx.btnBorrarBioClick(Sender: TObject);
var bContinuar: Boolean;
begin
     {$ifndef DOS_CAPAS}
     bContinuar := TRUE;

     if chkTieneHuella.Checked then
     begin
          if not ( ZConfirm( 'Borrar ID biom�trico', 'El empleado tiene plantillas asignadas.' + CR_LF +
                                          '�Desea continuar?',0, mbNo ) )then
          begin
               bContinuar := FALSE;
          end;
     end;

     if bContinuar then
     begin
          with dmCliente.cdsEmpleado do
          begin
               if not( State in [ dsEdit, dsInsert ] )then
                  Edit;
               FieldByName( 'CB_ID_BIO' ).AsInteger := 0;
               FieldByName( 'CB_GP_COD' ).AsString := VACIO;
          end;
          CB_GP_COD.Enabled := False;
     end;
     {$endif}
end;


procedure TEditEmpGafeteBiometrico_DevEx.BtnBorrarFacialClick(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   sHeader : String;
   iEmpleado : Integer;
{$endif}
begin
     inherited;
{$ifndef DOS_CAPAS}
     iEmpleado := dmCliente.cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
       sHeader := 'Eliminaci�n de Facial';

      with dmSistema do
      begin
           if ( ZConfirm( sHeader, 'Se eliminara la plantilla facial empleado.' + CR_LF + '�Desea continuar?',0,mbNo ) ) then
           begin
                EliminaHuella( iEmpleado, 0, K_TERMINAL_FACIAL );
                ZInformation(sHeader, 'Se elimino la plantilla facial del empleado ' + IntToStr( iEmpleado ) + '.', 0);
           end;

           Huellas;
      end;
      VerificarDerechos;
{$endif}
end;

procedure TEditEmpGafeteBiometrico_DevEx.btnAsignarBioClick(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmCliente.cdsEmpleado do
     begin
          if not( State in [ dsEdit, dsInsert ] )then
             Edit;
          FieldByName( 'CB_ID_BIO' ).AsInteger := dmSistema.ObtenMaxIdBio;
          CB_GP_COD.Enabled := ( FieldByName( 'CB_ID_BIO' ).AsInteger <> 0 );
     end;
     {$endif}
end;


{$ifndef DOS_CAPAS}
procedure TEditEmpGafeteBiometrico_DevEx.RegistroBiometrico;
begin
     //Derechos de Acceso
     with dmRecursos.cdsDatosEmpleado do
     begin
          BtnEnrolar.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL12 ) and ( FieldByName( 'CB_GP_COD' ).AsString <> VACIO ) );
          btnBorrarHuellas.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL13 ) and ( FieldByName( 'CB_GP_COD' ).AsString <> VACIO ) and ( StrToInt ( CANTIDAD_HUELLAS.Caption ) > 0 ) );
          BtnBorrarFacial.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL14 ) and ( FieldByName( 'CB_GP_COD' ).AsString <> VACIO ) );
     end;
end;
{$endif}

procedure TEditEmpGafeteBiometrico_DevEx.BtnEnrolarClick(Sender: TObject);
begin
     inherited;
{$ifndef DOS_CAPAS}
{$ifndef TIMBRADO}
{$ifndef ENROLAMIENTO}
    with TEnrolamiento_DevEx.Create( Self ) do
     begin
          try
             with dmSistema do
             begin

                  IdBiometrico := dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger;

                  Empleado := dmCliente.cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
                  Invitador := 0;

                  ShowModal;
                  Huellas;
             end;
          finally
                 Free;
          end;
     end;
     {$endif}
     {$endif}
{$endif}
end;


procedure TEditEmpGafeteBiometrico_DevEx.btnBorrarHuellasClick(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   sHeader : String;
   iEmpleado : Integer;
{$endif}
begin
     inherited;
{$ifndef DOS_CAPAS}
     iEmpleado := dmCliente.cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger;
       sHeader := 'Eliminaci�n de Huellas';

      with dmSistema do
      begin
           if ( ZConfirm( sHeader, 'Se eliminar�n todas las plantillas del empleado.' + CR_LF + '�Desea continuar?',0,mbNo ) ) then
           begin
                EliminaHuella( iEmpleado, 0, K_TERMINAL_HUELLA );
                ZInformation(sHeader, 'Se eliminaron las plantillas del empleado ' + IntToStr( iEmpleado ) + '.', 0);
           end;

           //Borra archivo Local
           DeleteFile( LocalBioDbPath );

           Huellas;
      end;
{$endif}
end;

procedure TEditEmpGafeteBiometrico_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
{$ifndef DOS_CAPAS}
var
   lBiometrico : Boolean;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmRecursos.cdsDatosEmpleado do
     begin
          lBiometrico := ( ( State = dsBrowse ) and ( FieldByName( 'CB_GP_COD' ).AsString <> VACIO ) );
          BtnEnrolar.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL12 ) and lBiometrico );
          btnBorrarHuellas.Enabled := ( ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL13 ) and lBiometrico );
     end;
     {$endif}
end;

procedure TEditEmpGafeteBiometrico_DevEx.Huellas;
begin
     chkTieneHuella.Checked := dmSistema.TieneHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger, dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger );
     CANTIDAD_HUELLAS.Caption := IntToStr (dmSistema.CantidadHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger , dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger, 0));
     RegistroBiometrico;
end;

end.
