inherited EditHisAcumulados_DevEx: TEditHisAcumulados_DevEx
  Left = 197
  Top = 167
  Caption = 'Acumulados'
  ClientHeight = 322
  ClientWidth = 498
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 288
    Width = 498
    Height = 34
    inherited OK_DevEx: TcxButton
      Left = 334
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 413
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 498
    inherited ValorActivo2: TPanel
      Width = 172
      inherited textoValorActivo2: TLabel
        Width = 166
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 50
    Width = 498
    Height = 238
    Align = alClient
    TabOrder = 3
    object Label1: TLabel
      Left = 36
      Top = 12
      Width = 49
      Height = 13
      Caption = 'Concepto:'
    end
    object Label16: TLabel
      Left = 28
      Top = 34
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total Anual:'
    end
    object Label17: TLabel
      Left = 47
      Top = 57
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Mes 13:'
    end
    object AC_ANUAL: TZetaDBTextBox
      Left = 91
      Top = 32
      Width = 121
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'AC_ANUAL'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AC_ANUAL'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object GroupBox1: TGroupBox
      Left = 44
      Top = 80
      Width = 405
      Height = 149
      TabOrder = 2
      object Label3: TLabel
        Left = 24
        Top = 16
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Enero:'
      end
      object Label4: TLabel
        Left = 16
        Top = 37
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Febrero:'
      end
      object Label5: TLabel
        Left = 23
        Top = 59
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Marzo:'
      end
      object Label6: TLabel
        Left = 32
        Top = 80
        Width = 23
        Height = 13
        Alignment = taRightJustify
        Caption = 'Abril:'
      end
      object Label7: TLabel
        Left = 26
        Top = 101
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Mayo:'
      end
      object Label8: TLabel
        Left = 27
        Top = 122
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Junio:'
      end
      object Label14: TLabel
        Left = 217
        Top = 122
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Diciembre:'
      end
      object Label13: TLabel
        Left = 213
        Top = 102
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Noviembre:'
      end
      object Label12: TLabel
        Left = 226
        Top = 80
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Octubre:'
      end
      object Label11: TLabel
        Left = 211
        Top = 59
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'Septiembre:'
      end
      object Label10: TLabel
        Left = 231
        Top = 37
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Agosto:'
      end
      object Label9: TLabel
        Left = 243
        Top = 16
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Julio:'
      end
      object AC_MES_01: TZetaDBNumero
        Left = 68
        Top = 12
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 0
        Text = '0.00'
        DataField = 'AC_MES_01'
        DataSource = DataSource
      end
      object AC_MES_02: TZetaDBNumero
        Left = 68
        Top = 33
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'AC_MES_02'
        DataSource = DataSource
      end
      object AC_MES_03: TZetaDBNumero
        Left = 68
        Top = 55
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'AC_MES_03'
        DataSource = DataSource
      end
      object AC_MES_04: TZetaDBNumero
        Left = 68
        Top = 76
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 3
        Text = '0.00'
        DataField = 'AC_MES_04'
        DataSource = DataSource
      end
      object AC_MES_05: TZetaDBNumero
        Left = 68
        Top = 97
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
        DataField = 'AC_MES_05'
        DataSource = DataSource
      end
      object AC_MES_06: TZetaDBNumero
        Left = 68
        Top = 118
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 5
        Text = '0.00'
        DataField = 'AC_MES_06'
        DataSource = DataSource
      end
      object AC_MES_07: TZetaDBNumero
        Left = 276
        Top = 12
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 6
        Text = '0.00'
        DataField = 'AC_MES_07'
        DataSource = DataSource
      end
      object AC_MES_08: TZetaDBNumero
        Left = 276
        Top = 33
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 7
        Text = '0.00'
        DataField = 'AC_MES_08'
        DataSource = DataSource
      end
      object AC_MES_09: TZetaDBNumero
        Left = 276
        Top = 55
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 8
        Text = '0.00'
        DataField = 'AC_MES_09'
        DataSource = DataSource
      end
      object AC_MES_10: TZetaDBNumero
        Left = 276
        Top = 76
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 9
        Text = '0.00'
        DataField = 'AC_MES_10'
        DataSource = DataSource
      end
      object AC_MES_11: TZetaDBNumero
        Left = 276
        Top = 98
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 10
        Text = '0.00'
        DataField = 'AC_MES_11'
        DataSource = DataSource
      end
      object AC_MES_12: TZetaDBNumero
        Left = 276
        Top = 118
        Width = 121
        Height = 21
        Mascara = mnPesos
        TabOrder = 11
        Text = '0.00'
        DataField = 'AC_MES_12'
        DataSource = DataSource
      end
    end
    object CO_NUMERO: TZetaDBKeyLookup_DevEx
      Left = 92
      Top = 8
      Width = 300
      Height = 21
      LookupDataset = dmCatalogos.cdsConceptos
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CO_NUMERO'
      DataSource = DataSource
    end
    object AC_MES_13: TZetaDBNumero
      Left = 91
      Top = 52
      Width = 121
      Height = 21
      Mascara = mnPesos
      TabOrder = 1
      Text = '0.00'
      DataField = 'AC_MES_13'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
