inherited EditAprobado_DevEx: TEditAprobado_DevEx
  Left = 266
  Top = 177
  Caption = 'Clasificaci'#243'n'
  ClientHeight = 420
  ExplicitHeight = 448
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 384
    ExplicitTop = 384
  end
  inherited PanelIdentifica: TPanel
    inherited Splitter: TSplitter
      Left = 265
      ExplicitLeft = 265
    end
    inherited ValorActivo1: TPanel
      Width = 265
      ExplicitWidth = 265
      inherited textoValorActivo1: TLabel
        Width = 259
      end
    end
    inherited ValorActivo2: TPanel
      Left = 268
      Width = 206
      ExplicitLeft = 268
      ExplicitWidth = 206
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 200
        ExplicitLeft = 120
      end
    end
  end
  object PageControl: TcxPageControl [3]
    Left = 0
    Top = 50
    Width = 474
    Height = 334
    Hint = ''
    Align = alClient
    TabOrder = 3
    
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 332
    ClientRectLeft = 2
    ClientRectRight = 472
    ClientRectTop = 27
    object TabClasifi: TcxTabSheet
      Caption = 'Clasificaci'#243'n'
      object HorarioLbl: TLabel
        Left = 93
        Top = 155
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object PuestoLbl: TLabel
        Left = 88
        Top = 107
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object ClasificacionLbl: TLabel
        Left = 62
        Top = 131
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object Label1: TLabel
        Left = 68
        Top = 59
        Width = 56
        Height = 13
        Caption = 'Evaluaci'#243'n:'
      end
      object KC_HORASLbl: TLabel
        Left = 93
        Top = 83
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Horas:'
      end
      object CB_TURNO: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 151
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 65
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 103
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 65
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 127
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 65
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object KC_EVALUA: TZetaDBNumero
        Left = 128
        Top = 55
        Width = 65
        Height = 21
        Mascara = mnHoras
        TabOrder = 0
        Text = '0.00'
        DataField = 'KC_EVALUA'
        DataSource = DataSource
      end
      object KC_HORAS: TZetaDBNumero
        Left = 128
        Top = 79
        Width = 65
        Height = 21
        Mascara = mnHoras
        TabOrder = 1
        Text = '0.00'
        DataField = 'KC_HORAS'
        DataSource = DataSource
      end
    end
    object TabNiveles: TcxTabSheet
      Caption = 'Niveles'
      object CB_NIVEL1lbl: TLabel
        Left = 92
        Top = 12
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 92
        Top = 36
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 92
        Top = 60
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 92
        Top = 84
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 92
        Top = 108
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 92
        Top = 132
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 92
        Top = 156
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 92
        Top = 180
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 92
        Top = 204
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL10lbl: TLabel
        Left = 86
        Top = 228
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 86
        Top = 252
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 86
        Top = 276
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 200
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 176
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 152
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 128
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 104
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 80
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 56
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 32
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 8
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 224
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 248
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 272
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL12'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
