inherited KardexPlaza_DevEx: TKardexPlaza_DevEx
  Left = 311
  Top = 170
  Caption = 'Plaza'
  ClientHeight = 357
  ClientWidth = 476
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 321
    Width = 476
  end
  inherited PanelIdentifica: TPanel
    Width = 476
    inherited ValorActivo2: TPanel
      Width = 150
      inherited textoValorActivo2: TLabel
        Width = 144
      end
    end
  end
  inherited PageControl_DevEx: TcxPageControl
    Width = 476
    Height = 241
    ClientRectBottom = 239
    ClientRectRight = 474
    inherited General_DevEx: TcxTabSheet
      object CB_PLAZALBL: TLabel
        Left = 85
        Top = 47
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'P&laza:'
      end
      object CB_PUESTOlbl: TLabel
        Left = 78
        Top = 24
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object gbContratacion: TGroupBox
        Left = 24
        Top = 76
        Width = 425
        Height = 105
        Caption = ' Datos de Contrataci'#243'n '
        TabOrder = 2
        object Label4: TLabel
          Left = 33
          Top = 28
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clasificaci'#243'n:'
        end
        object Label26: TLabel
          Left = 64
          Top = 52
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Turno:'
        end
        object Label29: TLabel
          Left = 11
          Top = 76
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Registro Patronal:'
        end
        object CB_CLASIFI: TZetaDBKeyLookup_DevEx
          Left = 98
          Top = 24
          Width = 300
          Height = 21
          Enabled = False
          LookupDataset = dmCatalogos.cdsClasifi
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_CLASIFI'
          DataSource = DataSource
        end
        object CB_TURNO: TZetaDBKeyLookup_DevEx
          Left = 98
          Top = 48
          Width = 300
          Height = 21
          Enabled = False
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_TURNO'
          DataSource = DataSource
        end
        object CB_PATRON: TZetaDBKeyLookup_DevEx
          Left = 98
          Top = 72
          Width = 300
          Height = 21
          Enabled = False
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_PATRON'
          DataSource = DataSource
        end
      end
      object CB_PLAZA: TZetaDBKeyLookup_DevEx
        Left = 122
        Top = 43
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PLAZA'
        DataSource = DataSource
      end
      object PU_CODIGO: TZetaKeyLookup_DevEx
        Left = 122
        Top = 20
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        OnValidKey = PU_CODIGOValidKey
      end
    end
    object Area_DevEx: TcxTabSheet [1]
      Caption = #193'rea'
      ImageIndex = 3
      object CB_NIVEL12lbl: TLabel
        Left = 76
        Top = 263
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 76
        Top = 239
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL10lbl: TLabel
        Left = 76
        Top = 215
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL1lbl: TLabel
        Left = 82
        Top = 12
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 82
        Top = 34
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 82
        Top = 57
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 82
        Top = 79
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 82
        Top = 102
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 82
        Top = 124
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 82
        Top = 147
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 82
        Top = 169
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 82
        Top = 192
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 259
        Width = 300
        Height = 21
        Enabled = False
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL12ValidLookup
        DataField = 'CB_NIVEL12'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 235
        Width = 300
        Height = 21
        Enabled = False
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL11ValidLookup
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 211
        Width = 300
        Height = 21
        Enabled = False
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL10ValidLookup
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 188
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL9ValidLookup
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 165
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL8ValidLookup
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 143
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL7ValidLookup
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 120
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL6ValidLookup
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 98
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL5ValidLookup
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 75
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL4ValidLookup
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 53
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL3ValidLookup
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 30
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL2ValidLookup
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 120
        Top = 8
        Width = 300
        Height = 21
        Enabled = False
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
    end
    inherited Notas_DevEx: TcxTabSheet
      inherited CB_NOTA: TcxDBMemo
        Height = 211
        Width = 472
      end
    end
    inherited Bitacora_DevEx: TcxTabSheet
      inherited GCapturoLbl: TLabel
        Left = 130
      end
      inherited GCapturaLbl: TLabel
        Left = 118
      end
      inherited GNivelLbl: TLabel
        Left = 146
      end
      inherited GStatusLbl: TLabel
        Left = 140
      end
      inherited CB_FEC_CAP: TZetaDBTextBox
        Left = 184
      end
      inherited CB_NIVEL: TZetaDBTextBox
        Left = 184
      end
      inherited Label2: TLabel
        Left = 140
      end
      inherited US_DESCRIP: TZetaDBTextBox
        Left = 184
      end
      inherited CB_STATUS: TZetaDBTextBox
        Left = 184
      end
      inherited CB_GLOBAL: TDBCheckBox
        Left = 184
      end
    end
  end
  inherited PanelFecha: TPanel
    Width = 476
  end
  inherited DataSource: TDataSource
    Left = 364
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
