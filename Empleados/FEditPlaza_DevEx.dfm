inherited EditPlaza_DevEx: TEditPlaza_DevEx
  Left = 476
  Top = 110
  Caption = 'Edici'#243'n de Plaza'
  ClientHeight = 589
  ClientWidth = 481
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 553
    Width = 481
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      ModalResult = 1
    end
    inherited Cancelar_DevEx: TcxButton
      Cancel = True
      ModalResult = 2
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 481
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 155
      inherited textoValorActivo2: TLabel
        Width = 149
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Enabled = False
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 481
    Height = 254
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 84
      Top = 10
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'ID Plaza:'
    end
    object lblPuesto: TLabel
      Left = 90
      Top = 34
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object Label5: TLabel
      Left = 101
      Top = 58
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio:'
    end
    object Label6: TLabel
      Left = 77
      Top = 107
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reporta a:'
    end
    object PL_FOLIO: TZetaDBTextBox
      Left = 130
      Top = 8
      Width = 49
      Height = 17
      AutoSize = False
      Caption = 'PL_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PL_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbTitular: TLabel
      Left = 95
      Top = 131
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Titular:'
    end
    object Label3: TLabel
      Left = 218
      Top = 10
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '# Plaza:'
    end
    object Label2: TLabel
      Left = 67
      Top = 82
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object PL_NOMBRE: TDBEdit
      Left = 130
      Top = 78
      Width = 275
      Height = 21
      DataField = 'PL_NOMBRE'
      DataSource = DataSource
      TabOrder = 3
    end
    object PU_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 130
      Top = 30
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
      OnValidKey = PU_CODIGOValidKey
      DataField = 'PU_CODIGO'
      DataSource = DataSource
    end
    object PL_REPORTA: TZetaDBKeyLookup_DevEx
      Left = 130
      Top = 103
      Width = 300
      Height = 21
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 4
      TabStop = True
      WidthLlave = 60
      DataField = 'PL_REPORTA'
      DataSource = DataSource
    end
    object CB_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 130
      Top = 127
      Width = 300
      Height = 21
      Filtro = 'CB_ACTIVO = '#39'S'#39
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 5
      TabStop = True
      WidthLlave = 60
      DataField = 'CB_CODIGO'
      DataSource = DataSource
    end
    object PL_ORDEN: TZetaDBNumero
      Left = 260
      Top = 6
      Width = 49
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      DataField = 'PL_ORDEN'
      DataSource = DataSource
    end
    object GroupBox1: TGroupBox
      Left = 80
      Top = 176
      Width = 358
      Height = 72
      Caption = ' Vigencia '
      TabOrder = 7
      object Label9: TLabel
        Left = 24
        Top = 19
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object lblVence: TLabel
        Left = 192
        Top = 43
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vence:'
        Enabled = False
      end
      object Label11: TLabel
        Left = 20
        Top = 43
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio:'
      end
      object PL_TIPO: TZetaDBKeyCombo
        Left = 50
        Top = 15
        Width = 115
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfJITipoPlaza
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'PL_TIPO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object PL_FEC_FIN: TZetaDBFecha
        Left = 228
        Top = 39
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '12/dic/05'
        Valor = 38698.000000000000000000
        DataField = 'PL_FEC_FIN'
        DataSource = DataSource
      end
      object ckVence: TCheckBox
        Left = 176
        Top = 41
        Width = 14
        Height = 17
        TabOrder = 2
        OnClick = ckVenceClick
      end
      object PL_FEC_INI: TZetaDBFecha
        Left = 50
        Top = 39
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '12/dic/05'
        Valor = 38698.000000000000000000
        DataField = 'PL_FEC_INI'
        DataSource = DataSource
      end
    end
    object PL_CODIGO: TDBEdit
      Left = 130
      Top = 54
      Width = 275
      Height = 21
      DataField = 'PL_CODIGO'
      DataSource = DataSource
      TabOrder = 2
    end
    object btWizAsignar: TcxButton
      Left = 130
      Top = 151
      Width = 123
      Height = 26
      Hint = 'Asignar otro titular'
      BiDiMode = bdRightToLeft
      Caption = 'Asignar otro titular'
      ModalResult = 1
      ParentBiDiMode = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = btWizAsignarClick
      LookAndFeel.SkinName = 'TressMorado2013'
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFAAE9CAFFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
        D6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
        D6FFBDEED6FF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FFB2EBD0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFECFAF3FF6EDAA6FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF6EDAA6FFFAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFDFFA8E9C9FF6EDA
        A6FF7CDDAEFF5ED69BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8E9C9FFA2E7C6FFFCFE
        FDFFF1FBF7FFF7FDFAFF84DFB3FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF8CE2B8FFECFAF3FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF76DCABFFFFFFFFFFFFFF
        FFFF92E3BCFFFFFFFFFFECFAF3FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF5BD59AFF84DFB3FFA0E6C4FFBDEE
        D6FFBDEED6FFBDEED6FFBDEED6FFB2EBD0FFA5E8C8FF7CDDAEFFF4FCF8FFA8E9
        C9FF71DAA7FFB2EBD0FFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF5BD59AFFF4FCF8FFFFFF
        FFFFA0E6C4FFFFFFFFFFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFA2E7
        C6FFDBF6E9FFE9F9F1FFCBF2DFFF7CDDAEFF50D293FF50D293FF76DCABFFDEF7
        EBFFFAFEFCFFD0F3E2FF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFD9F5E7FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF97E4BFFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FFA8E9C9FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFEFCFF69D8A2FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FFDBF6E9FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FFE6F9F0FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA5E8C8FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FFCBF2DFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8AE1B7FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4F8EEFF55D396FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFE
        FCFFFFFFFFFFFFFFFFFFFFFFFFFFE4F8EEFF6BD9A4FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
        A1FF9AE5C1FFA8E9C9FF8AE1B7FF55D396FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.ImageIndex = 1
      OptionsImage.Images = cxImageList24_PanelBotones
      OptionsImage.Margin = 1
    end
  end
  object PageControl: TcxPageControl [4]
    Left = 0
    Top = 304
    Width = 481
    Height = 249
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = TabSheet1
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 247
    ClientRectLeft = 2
    ClientRectRight = 479
    ClientRectTop = 27
    object General: TcxTabSheet
      Caption = 'General'
      object Label16: TLabel
        Left = 81
        Top = 12
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Relaci'#243'n:'
      end
      object Label7: TLabel
        Left = 95
        Top = 36
        Width = 31
        Height = 13
        Caption = 'Ingl'#233's:'
      end
      object CO_NUMEROLbl: TLabel
        Left = 86
        Top = 60
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label8: TLabel
        Left = 96
        Top = 85
        Width = 30
        Height = 13
        Caption = 'Texto:'
      end
      object Label10: TLabel
        Left = 70
        Top = 109
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'SubCuenta:'
      end
      object PL_TIREP: TZetaDBKeyCombo
        Left = 129
        Top = 8
        Width = 110
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfJIPlazaReporta
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'PL_TIREP'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object PL_INGLES: TDBEdit
        Left = 129
        Top = 32
        Width = 297
        Height = 21
        DataField = 'PL_INGLES'
        DataSource = DataSource
        TabOrder = 1
      end
      object PL_NUMERO: TZetaDBNumero
        Left = 129
        Top = 56
        Width = 70
        Height = 21
        Mascara = mnDias
        TabOrder = 2
        Text = '0'
        UseEnterKey = True
        ConfirmEdit = True
        DataField = 'PL_NUMERO'
        DataSource = DataSource
      end
      object PL_TEXTO: TDBEdit
        Left = 129
        Top = 80
        Width = 297
        Height = 21
        DataField = 'PL_TEXTO'
        DataSource = DataSource
        TabOrder = 3
      end
      object PL_SUB_CTA: TDBEdit
        Left = 129
        Top = 104
        Width = 297
        Height = 21
        DataField = 'PL_SUB_CTA'
        DataSource = DataSource
        TabOrder = 4
      end
    end
    object Contratacion: TcxTabSheet
      Caption = 'Contrataci'#243'n'
      object Label12: TLabel
        Left = 47
        Top = 81
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de N'#243'mina:'
      end
      object Label21: TLabel
        Left = 55
        Top = 168
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Checa Tarjeta:'
      end
      object lblConfidencialidad: TLabel
        Left = 44
        Top = 103
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Confidencialidad:'
      end
      object Arealbl: TLabel
        Left = 100
        Top = 146
        Width = 25
        Height = 13
        Alignment = taRightJustify
        BiDiMode = bdRightToLeft
        Caption = 'Area:'
        ParentBiDiMode = False
      end
      object Label27: TLabel
        Left = 43
        Top = 125
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Contrato:'
      end
      object Label29: TLabel
        Left = 41
        Top = 57
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Registro Patronal:'
      end
      object Label26: TLabel
        Left = 94
        Top = 34
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object Label4: TLabel
        Left = 63
        Top = 12
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object PL_NOMINA: TZetaDBKeyCombo
        Left = 129
        Top = 76
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'PL_NOMINA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object PL_CHECA: TDBCheckBox
        Left = 129
        Top = 166
        Width = 97
        Height = 17
        DataField = 'PL_CHECA'
        DataSource = DataSource
        TabOrder = 7
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object PL_NIVEL0: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 99
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'PL_NIVEL0'
        DataSource = DataSource
      end
      object PL_AREA: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 143
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'PL_AREA'
        DataSource = DataSource
      end
      object PL_CONTRAT: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 121
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'PL_CONTRAT'
        DataSource = DataSource
      end
      object PL_PATRON: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 52
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'PL_PATRON'
        DataSource = DataSource
      end
      object PL_TURNO: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 30
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'PL_TURNO'
        DataSource = DataSource
      end
      object PL_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 129
        Top = 8
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        OnExit = PL_CLASIFIExit
        DataField = 'PL_CLASIFI'
        DataSource = DataSource
      end
    end
    object Area: TcxTabSheet
      Caption = #193'rea'
      ImageIndex = 2
      object PL_NIVEL1lbl: TLabel
        Left = 94
        Top = 11
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object PL_NIVEL2lbl: TLabel
        Left = 94
        Top = 34
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object PL_NIVEL9lbl: TLabel
        Left = 94
        Top = 195
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object PL_NIVEL8lbl: TLabel
        Left = 94
        Top = 172
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object PL_NIVEL7lbl: TLabel
        Left = 94
        Top = 149
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object PL_NIVEL6lbl: TLabel
        Left = 94
        Top = 126
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object PL_NIVEL5lbl: TLabel
        Left = 94
        Top = 103
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object PL_NIVEL4lbl: TLabel
        Left = 94
        Top = 80
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object PL_NIVEL3lbl: TLabel
        Left = 94
        Top = 57
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object PL_NIVEL10lbl: TLabel
        Left = 88
        Top = 217
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object PL_NIVEL11lbl: TLabel
        Left = 88
        Top = 240
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object PL_NIVEL12lbl: TLabel
        Left = 88
        Top = 264
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object PL_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 7
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'PL_NIVEL1'
        DataSource = DataSource
      end
      object PL_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 30
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL2ValidLookup
        DataField = 'PL_NIVEL2'
        DataSource = DataSource
      end
      object PL_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 53
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL3ValidLookup
        DataField = 'PL_NIVEL3'
        DataSource = DataSource
      end
      object PL_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 76
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL4ValidLookup
        DataField = 'PL_NIVEL4'
        DataSource = DataSource
      end
      object PL_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 99
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL5ValidLookup
        DataField = 'PL_NIVEL5'
        DataSource = DataSource
      end
      object PL_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 122
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL6ValidLookup
        DataField = 'PL_NIVEL6'
        DataSource = DataSource
      end
      object PL_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 145
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL7ValidLookup
        DataField = 'PL_NIVEL7'
        DataSource = DataSource
      end
      object PL_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 168
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL8ValidLookup
        DataField = 'PL_NIVEL8'
        DataSource = DataSource
      end
      object PL_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 191
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL9ValidLookup
        DataField = 'PL_NIVEL9'
        DataSource = DataSource
      end
      object PL_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 214
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PL_NIVEL10ValidLookup
        DataField = 'PL_NIVEL10'
      end
      object PL_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 237
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PL_NIVEL11ValidLookup
        DataField = 'PL_NIVEL11'
      end
      object PL_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 260
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PL_NIVEL12ValidLookup
        DataField = 'PL_NIVEL12'
      end
    end
    object TabSheet1: TcxTabSheet
      Caption = 'Salario'
      ImageIndex = 3
      object LblTabulador: TLabel
        Left = 42
        Top = 10
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = ' Salario por Tabulador:'
      end
      object LblSalario: TLabel
        Left = 114
        Top = 30
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario:'
      end
      object Label33: TLabel
        Left = 38
        Top = 54
        Width = 111
        Height = 13
        Alignment = taRightJustify
        Caption = 'Promedio de  Variables:'
      end
      object Label28: TLabel
        Left = 40
        Top = 78
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tabla de Prestaciones:'
      end
      object Label31: TLabel
        Left = 66
        Top = 102
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona Geogr'#225'fica:'
      end
      object PL_AUTOSAL: TDBCheckBox
        Left = 152
        Top = 8
        Width = 95
        Height = 17
        DataField = 'PL_AUTOSAL'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = PL_AUTOSALClick
      end
      object PL_SALARIO: TZetaDBNumero
        Left = 152
        Top = 26
        Width = 100
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
        DataField = 'PL_SALARIO'
        DataSource = DataSource
      end
      object PL_PER_VAR: TZetaDBNumero
        Left = 152
        Top = 50
        Width = 100
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
        DataField = 'PL_PER_VAR'
        DataSource = DataSource
      end
      object PL_TABLASS: TZetaDBKeyLookup_DevEx
        Left = 152
        Top = 74
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'PL_TABLASS'
        DataSource = DataSource
      end
      object PL_ZONA_GE: TDBComboBox
        Left = 152
        Top = 98
        Width = 60
        Height = 21
        Style = csDropDownList
        BevelKind = bkFlat
        Ctl3D = False
        DataField = 'PL_ZONA_GE'
        DataSource = DataSource
        ItemHeight = 13
        Items.Strings = (
          'A'
          'B'
          'C')
        ParentCtl3D = False
        TabOrder = 4
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 436
    Top = 281
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
