unit FEditMovtoKardexDlg_DevEx;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Mask, ZetaFecha, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxButtons, ImgList;

type
  TEditMovtoKardexDlg_DevEx = class(TForm)
    RadioGroup1: TRadioGroup;
    PL_FEC_FIN: TZetaFecha;
    Panel2: TPanel;
    cxImageList24_PanelBotones: TcxImageList;
    Panel1: TPanel;
    OK: TcxButton;
    Salir: TcxButton;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SalirClick(Sender: TObject);
  private
    { Private declarations }
    FFechaMovto:TDate;
    FCodigoEmpleado:Integer;
    function GetFechaAltaEmpleado:TDate;
  public
     property FechaMovto: TDate read FFechaMovto write FFechaMovto;
     property CodigoEmpleado: Integer read FCodigoEmpleado write FCodigoEmpleado;
    { Public declarations }
  end;

var
  EditMovtoKardexDlg_DevEx: TEditMovtoKardexDlg_DevEx;

implementation


uses ZetaCommonClasses,
     ZetaCommonTools,
     dCliente,
     ZetaDialogo;

{$R *.DFM}

procedure TEditMovtoKardexDlg_DevEx.OKClick(Sender: TObject);
const
     K_FECHA_ESPECIFICA = 1;
var
   dFechaIngreso: TDate;
begin
     if RadioGroup1.ItemIndex = K_FECHA_ESPECIFICA then
        FFechaMovto := PL_FEC_FIN.Valor
     else
     begin
          dFechaIngreso := GetFechaAltaEmpleado;
          if ( dFechaIngreso <> NullDateTime ) then
             FFechaMovto := dFechaIngreso
          else
          begin
               ZetaDialogo.zInformation( Self.Caption,'No existe titular se aplicara la fecha específica: '+DateToStr(PL_FEC_FIN.Valor),0 );
               FFechaMovto := PL_FEC_FIN.Valor;
          end;
     end;
     //Salir.SetFocus;
end;

procedure TEditMovtoKardexDlg_DevEx.FormShow(Sender: TObject);
begin
     RadioGroup1.ItemIndex := 0;
     PL_FEC_FIN.Valor := dmCliente.FechaDefault;
end;

procedure TEditMovtoKardexDlg_DevEx.SalirClick(Sender: TObject);
begin
     Close;
end;

function TEditMovtoKardexDlg_DevEx.GetFechaAltaEmpleado: TDate;
begin
     with dmCliente do
     begin
          SetEmpleadoNumero(FCodigoEmpleado);
          Result := cdsEmpleado.FieldByName('CB_FEC_ING').AsDateTime;
     end;
end;

end.
