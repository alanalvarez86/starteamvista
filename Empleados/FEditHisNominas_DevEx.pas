unit FEditHisNominas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ZetaFecha, ZetaDBTextBox, Db, ExtCtrls,
  DBCtrls, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, 
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxCheckBox, dxSkinsDefaultPainters,
  dxSkinsdxBarPainter;

type
  TEditHisNominas_DevEx = class(TBaseEdicion_DevEx)
    lbPeriodo: TLabel;
    PE_NUMERO: TZetaDBTextBox;
    Label1: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    CBPagado: TCheckBox;
    NO_FEC_PAG: TZetaDBFecha;
    Label13: TLabel;
    US_CODIGO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure CBPagadoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NO_FEC_PAGExit(Sender: TObject);
  private
    lConnect : Boolean;
    procedure ModificaRecibo;

  protected
     procedure Connect; override;
  public
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
  end;

var
  EditHisNominas_DevEx: TEditHisNominas_DevEx;

implementation

uses DCliente, dRecursos, ZetaCommonClasses, ZetaCommonLists, ZAccesosTress;

{$R *.DFM}

procedure TEditHisNominas_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext := ZetaCommonClasses.H10144_Historial;
     IndexDerechos := ZAccesosTress.D_EMP_NOM_HISTORIAL;
     FirstControl := CBPagado;
end;

procedure TEditHisNominas_DevEx.CBPagadoClick(Sender: TObject);
begin
  inherited;
  ModificaRecibo;
end;

procedure TEditHisNominas_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  {$ifdef CAJAAHORRO}
  {$else}
  lbPeriodo.Caption := ObtieneElemento( lfTipoNomina, Ord(dmCliente.PeriodoTipo) )+ ':';
  {$endif}
end;

procedure TEditHisNominas_DevEx.NO_FEC_PAGExit(Sender: TObject);
begin
  inherited;
  ModificaRecibo;
end;

procedure TEditHisNominas_DevEx.ModificaRecibo;
begin
     NO_FEC_PAG.Enabled := CBPagado.Checked;
     if NOT lConnect AND (DataSource.DataSet.State = dsBrowse)  then
        DataSource.DataSet.Edit;
     if ( NO_FEC_PAG.Enabled ) then
         begin
              if ( NO_FEC_PAG.Valor = 0 ) then
              begin
                   NO_FEC_PAG.Valor := Date;
              end
         end
     else NO_FEC_PAG.Valor := 0;
     with dmRecursos.cdsHisNominas do
     begin
        if State = dsEdit then
          FieldByName('NO_USR_PAG').AsInteger := dmCliente.Usuario;
     end;
end;

procedure TEditHisNominas_DevEx.Connect;
begin
  with dmRecursos do
   begin
          cdsHisNominas.Conectar;
          DataSource.DataSet := cdsHisNominas;
     end;
     lConnect := TRUE;
     CBPagado.Checked := NO_FEC_PAG.Valor > 0;
     lConnect := FALSE;
end;

function TEditHisNominas_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se puede agregar al Historial de N�minas';
end;

end.
