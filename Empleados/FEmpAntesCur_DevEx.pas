unit FEmpAntesCur_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,ZBaseGridLectura_DevEx,
  StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TEmpAntesCur_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
  end;

var
  EmpAntesCur_DevEx: TEmpAntesCur_DevEx;

implementation

uses dRecursos, ZetaCommonLists, ZetaCommonClasses;

{$R *.DFM}

procedure TEmpAntesCur_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10125_Expediente_Cursos_anteriores;
end;

procedure TEmpAntesCur_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsEmpAntesCur.Conectar;
          DataSource.DataSet:= cdsEmpAntesCur;
     end;
end;

procedure TEmpAntesCur_DevEx.Refresh;
begin
     dmRecursos.cdsEmpAntesCur.Refrescar;
end;

procedure TEmpAntesCur_DevEx.Agregar;
begin
     dmRecursos.cdsEmpAntesCur.Agregar;
end;

procedure TEmpAntesCur_DevEx.Borrar;
begin
     dmRecursos.cdsEmpAntesCur.Borrar;
end;

procedure TEmpAntesCur_DevEx.Modificar;
begin
     dmRecursos.cdsEmpAntesCur.Modificar;
end;

procedure TEmpAntesCur_DevEx.FormShow(Sender: TObject);
begin
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
  //CreaColumaSumatoria('AR_FOLIO',SkCount);

  end;

end.
