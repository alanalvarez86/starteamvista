unit FGridPrestamos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, ZetaKeyCombo, Mask, ZetaFecha,
  ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TGridPrestamos_DevEx = class(TBaseGridEdicion_DevEx)
    PanelTope: TPanel;
    ZFecha: TZetaDBFecha;
    zComboStatus: TZetaDBKeyCombo;
    Panel2: TPanel;
    PR_TIPO: TZetaKeyLookup_DevEx;
    AH_TIPOLbl: TLabel;
    rgAgregarPrestamos: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
    procedure BuildFormula;
    procedure AsignaDatosPrestamo;
    procedure ValidaDatosPrestamo;
  protected
    { Protected declarations }
    procedure Buscar; override;
    procedure Connect; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  GridPrestamos_DevEx: TGridPrestamos_DevEx;

implementation

uses dRecursos,
     dCliente,
     dTablas,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaBuscaEmpleado_DevEx,
     ZetaTipoEntidad,
     ZConstruyeFormula,
     DGlobal,
     ZGlobalTress;

{$R *.DFM}

{ TGridPrestamos }

procedure TGridPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     PR_TIPO.LookupDataSet := dmTablas.cdsTPresta;
     {$ifdef FAPSA}
     PR_TIPO.Filtro := '(TB_NUMERO <> 1)';
     {$endif}
     HelpContext := H11622_Registro_de_prestamos;
end;

procedure TGridPrestamos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     self.ActiveControl:= PR_TIPO;
     rgAgregarPrestamos.Visible := (Global.GetGlobalBooleano( K_GLOBAL_VALIDAR_PRESTAMOS ))
                                   and (ZAccesosMgr.CheckDerecho(D_EMP_NOM_PRESTAMOS ,K_DERECHO_SIST_KARDEX)) ;
end;

procedure TGridPrestamos_DevEx.Connect;
begin
     dmTablas.cdsTPresta.Conectar;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          cdsGridPrestamos.Refrescar;
          DataSource.DataSet:= cdsGridPrestamos;
     end;
end;

procedure TGridPrestamos_DevEx.EscribirCambios;
begin
     try
        if ZAccesosMgr.CheckDerecho(D_EMP_NOM_PRESTAMOS ,K_DERECHO_SIST_KARDEX) then
           dmRecursos.ForzarGrabarPrestamos := ( rgAgregarPrestamos.ItemIndex = 0 )
        else
            dmRecursos.ForzarGrabarPrestamos := FALSE; 
        ValidaDatosPrestamo;
        AsignaDatosPrestamo;
        inherited EscribirCambios;
     except
        self.ModalResult:= mrNone;
        raise;
     end;
end;

procedure TGridPrestamos_DevEx.ValidaDatosPrestamo;
begin
     if StrVacio( PR_TIPO.Llave ) then
     begin
          ActiveControl := PR_TIPO;
          DB.DatabaseError( 'El C�digo del Pr�stamo no Puede quedar Vacio' );
     end
end;

procedure TGridPrestamos_DevEx.AsignaDatosPrestamo;
begin
     with ZetaDBGrid.DataSource.DataSet do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          DisableControls;
          try
             First;
             while not EOF do
             begin
                  Edit;
                  FieldByName( 'PR_TIPO' ).AsString := PR_TIPO.Llave;
                  Next;                                                 // El Next hace el Post
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TGridPrestamos_DevEx.Buscar;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
          else if ( FieldName = 'PR_FORMULA' ) then
             BuildFormula;
     end;
end;

procedure TGridPrestamos_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ) then
     begin
          with dmRecursos.cdsGridPrestamos do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridPrestamos_DevEx.BuildFormula;
begin
     with dmRecursos.cdsGridPrestamos do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          with FieldByName( 'PR_FORMULA' ) do
               AsString := BorraCReturn( GetFormulaConst( enNomina, AsString, Length( AsString ), evBase ) );
     end;
end;

procedure TGridPrestamos_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'PR_FECHA' ) and ( zFecha.Visible ) then
                  zFecha.Visible:= FALSE
               else if ( SelectedField.FieldName = 'PR_STATUS' ) and ( zComboStatus.Visible ) then
                  zComboStatus.Visible := FALSE;
          end;
     end;
end;

procedure TGridPrestamos_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer;
          Column: TColumn; State: TGridDrawState);

   procedure AjustaControlEnCelda( oControl: TControl );
   begin
        with oControl do
        begin
             Left := Rect.Left + ZetaDBGrid.Left;
             Top := Rect.Top + ZetaDBGrid.Top;
             Width := Column.Width + 2;
             Visible := True;
        end;
   end;

begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'PR_FECHA' ) then
             AjustaControlEnCelda( zFecha )
          else if ( Column.FieldName = 'PR_STATUS' ) then
             AjustaControlEnCelda( zComboStatus );
     end;
end;

procedure TGridPrestamos_DevEx.KeyPress(var Key: Char);

   function EsControlGrid: Boolean;
   begin
        Result := ( self.ActiveControl = zFecha ) or ( self.ActiveControl = zComboStatus );
   end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'PR_FECHA' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'PR_STATUS' ) then
               begin
                    zComboStatus.SetFocus;
                    SendMessage( zComboStatus.Handle, WM_Char, Word( Key ), 0 );
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'PR_REFEREN' ) then
               begin
                    if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
                       Key := #0;                               // No permite capturar Comillas
               end;
          end;
     end
     else if EsControlGrid and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
     begin
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;                                        // SetFocus dispara el evento Exit del control, validando y asignando el valor al tfield
               if Focused then                                  // Si no se puede salir del control ( eg. validaci�n de Fecha ) no se cambia de columna
                  SelectedIndex := PosicionaSiguienteColumna;
          end;
     end;
end;

end.
