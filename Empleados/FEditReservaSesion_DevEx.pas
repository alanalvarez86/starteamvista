unit FEditReservaSesion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, DBCtrls, ZetaNumero, ZetaHora, Mask,
  ZetaFecha, ExtCtrls, ZetaSmartLists, Buttons,
  ZetaDBTextBox, ZetaEdit, FEditReservaAula_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxContainer, cxEdit, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxTextEdit, cxMemo, cxDBEdit,
  ZetaKeyLookup_DevEx, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditReservaSesion_DevEx = class(TEditReservaAula_DevEx)
  private
    { Private declarations }
  protected
    function ValidaTipoReserva: Boolean; override;
    function EsEditable:Boolean; override;
    procedure EscribirCambios; override;
    procedure DoCancelChanges; override;
    procedure Agregar; override;
    procedure Borrar; override;
  public
    { Public declarations }
  end;

var
  EditReservaSesion_DevEx: TEditReservaSesion_DevEx;

implementation

uses DRecursos,
     DCatalogos,
     ZetaCommonLists;

{$R *.DFM}
procedure TEditReservaSesion_DevEx.Agregar;
var
   iSesion : Integer;
   sMensaje: String;
begin
     inherited;
     if PuedeAgregar( sMensaje ) then
     begin
          with dmRecursos do
          begin
               iSesion := cdsSesion.FieldByName ( 'SE_FOLIO' ).AsInteger;
               with cdsReserva do
               begin
                    // Sobreescribir onNewRecord para agregar a una sesion
                    FieldByName( 'RV_FEC_INI').AsDateTime := cdsSesion.FieldByName('SE_FEC_INI').AsDateTime;
                    FieldByName ( 'SE_FOLIO' ).AsInteger := iSesion;
                    FieldByName( 'RV_TIPO' ).AsInteger := Ord( rvSesion );
                    FieldByName( 'RV_RESUMEN' ).AsString := cdsSesion.FieldByName('CU_NOMBRE').AsString;//Format( 'Reserva de sesi�n %d', [ iSesion ] );
                    FieldByName('MA_CODIGO').AsString := cdsSesion.FieldbyName('MA_CODIGO').AsString;
                    dmCatalogos.cdsAulas.First;
                    FieldbyName('AL_CODIGO').AsString := dmCatalogos.cdsAulas.FieldbyName('AL_CODIGO').AsString;
               end;
          end;
     end;
end;

procedure TEditReservaSesion_DevEx.EscribirCambios;
begin
     ClientDataSet.Post;
end;

procedure TEditReservaSesion_DevEx.DoCancelChanges;
begin
     ClientDataset.Cancel;        // Si se deja la herencia cancela todos los post que se hayan hecho
end;

function TEditReservaSesion_DevEx.ValidaTipoReserva: Boolean;
begin
     Result := True;
end;

function TEditReservaSesion_DevEx.EsEditable:Boolean;
begin
     Result := True;
end;

procedure TEditReservaSesion_DevEx.Borrar;
begin
     ClientDataset.Delete;
end;

end.
