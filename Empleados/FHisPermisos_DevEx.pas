unit FHisPermisos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, Db, ZetaDBGrid, ExtCtrls, StdCtrls,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  THisPermisos_DevEx = class(TBaseGridLectura_DevEx)
    PM_FEC_INI: TcxGridDBColumn;
    PM_DIAS: TcxGridDBColumn;
    PM_FEC_FIN: TcxGridDBColumn;
    PM_CLASIFI: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    PM_NUMERO: TcxGridDBColumn;
    PM_COMENTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  HisPermisos_DevEx: THisPermisos_DevEx;

implementation

{$R *.DFM}

uses dRecursos, dSistema, dTablas,ZetaTipoEntidad,
     ZetaCommonLists,
     ZImprimeForma,              
     ZetaCommonClasses;

procedure THisPermisos_DevEx.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisPermiso.Conectar;
          DataSource.DataSet:= cdsHisPermiso;
     end;
end;

procedure THisPermisos_DevEx.Refresh;
begin
     dmRecursos.cdsHisPermiso.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisPermisos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10134_Historial_permisos;
end;

procedure THisPermisos_DevEx.Agregar;
begin
     dmRecursos.cdsHisPermiso.Agregar;
end;

procedure THisPermisos_DevEx.Borrar;
begin
     dmRecursos.cdsHisPermiso.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisPermisos_DevEx.Modificar;
begin
     dmRecursos.cdsHisPermiso.Modificar;
end;

procedure THisPermisos_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enPermiso, dmRecursos.cdsHisPermiso );
end;

procedure THisPermisos_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     PM_FEC_INI.Options.Grouping:= FALSE;
     PM_DIAS.Options.Grouping:= FALSE;
     PM_FEC_FIN.Options.Grouping:= FALSE;
     PM_CLASIFI.Options.Grouping := TRUE;
     TB_ELEMENT.Options.Grouping := TRUE;
     PM_NUMERO.Options.Grouping:= FALSE;
     PM_COMENTA.Options.Grouping:= FALSE;
end;

procedure THisPermisos_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  //CreaColumaAgrupadaSumatoria( PM_COMENTA.Name, skCount );
  inherited;
  //DevEx (by am):Configuracion Especial para agrupamiento
  ConfigAgrupamiento;

end;

end.
