unit FEditPLanVacacion_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ZetaDBTextBox, ZetaNumero, Mask, ZetaFecha, ZetaKeyCombo,
  ZetaCommonClasses,ZetaCommonLists, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit, cxRadioGroup, dxSkinsDefaultPainters;

type
  TEditPlanVacacion_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    VP_STATUS: TZetaDBKeyCombo;
    Label3: TLabel;
    Label4: TLabel;
    txtProcesada: TZetaTextBox;
    Label6: TLabel;
    USR_SOL_DES: TZetaDBTextBox;
    VP_SOL_FEC: TZetaDBTextBox;
    USR_AUT_DES: TZetaDBTextBox;
    VP_AUT_FEC: TZetaDBTextBox;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    VP_AUT_COM: TcxDBMemo;
    gpbxSeccPerPago: TGroupBox;
    RBAutomatico: TcxRadioButton;
    RBEspecPeriodo: TcxRadioButton;
    VP_NOMTIPO: TZetaDBKeyCombo;
    lblNumero: TLabel;
    lblAnio: TLabel;
    lblTipo: TLabel;
    VP_NOMYEAR: TZetaDBNumero;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    VP_SOL_COM: TcxDBMemo;
    GroupBox4: TGroupBox;
    VP_DIAS: TZetaDBTextBox;
    LblGozados: TLabel;
    LVA_VAC_AL: TLabel;
    VP_FEC_FIN: TZetaDBFecha;
    VP_FEC_INI: TZetaDBFecha;
    LVA_VAC_DEL: TLabel;
    gbSaldos: TGroupBox;
    Label5: TLabel;
    Label10: TLabel;
    VP_SAL_ANT: TZetaDBTextBox;
    VP_SAL_PRO: TZetaDBTextBox;
    VP_NOMNUME: TZetaDBKeyLookup_DevEx;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure Connect; override;
    procedure VP_STATUSChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RBEspecPeriodoClick(Sender: TObject);
    procedure VP_NOMNUMEValidKey(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
    procedure ControlesSesion;
    function EsEditable: Boolean;
    procedure HabilitaControlesPeriodo( lHabilita:Boolean );
    procedure SetFiltroPeriodo;
    procedure SetcontrolesPago( eStatus:eStatusPlanVacacion );
    //procedure SetControls;
  public
    { Public declarations }
  end;

const
     K_DERECHO_ESPEC_PER_PAGO = K_DERECHO_BANCA;
var
  EditPlanVacacion_DevEx: TEditPlanVacacion_DevEx;
  lIniciaPeriodoPago :Boolean;
implementation


{$R *.dfm}
uses dCliente, {$ifdef SUPERVISORES}dSuper{$else}dRecursos{$endif},
     ZAccesosTress,ZGlobalTress, ZAccesosMgr,
     ZetaCommonTools, ZetaDialogo, ZetaClientDataSet,DGlobal,
     DCatalogos;

procedure TEditPlanVacacion_DevEx.ControlesSesion;
var
   lAutoEdit: Boolean;
begin
     lAutoEdit := EsEditable;
     DataSource.AutoEdit := lAutoEdit;
     VP_FEC_INI.Enabled := lAutoEdit;
     VP_FEC_FIN.Enabled := lAutoEdit;
     VP_AUT_COM.Enabled := lAUtoEdit;
     VP_STATUS.Enabled := lAUtoEdit;
end;

function TEditPlanVacacion_DevEx.EsEditable: Boolean;
begin
     Result := not ( eStatusPLanVacacion( TZetaClientDataSet( DataSource.DataSet ).FieldByName('VP_STATUS').AsInteger ) in [ZetaCommonLists.spvProcesada{$ifdef SUPERVISORES}, ZetaCommonLists.spvAutorizada, ZetaCommonLists.spvRechazada {$endif} ] );
end;

procedure TEditPlanVacacion_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
var
  lAutorizada :Boolean;
begin
     inherited;
     if ( ( Field = nil ) or ( Field.FieldName = 'VP_PAGO_US' ) or ( Field.FieldName = 'VP_STATUS' ) ) then
     begin
          with DataSource.DataSet do
          begin
               lAutorizada := ( FieldByName('VP_STATUS').AsInteger = Ord( spvAutorizada ) );
               HabilitaControlesPeriodo( zStrToBool( FieldByName('VP_PAGO_US').AsString ) and ( lAutorizada ) );
               RBAutomatico.Enabled := lAutorizada;
               RBEspecPeriodo.Enabled := lAutorizada;
               if ( Field = nil )then
               begin
                    if ( State = dsBrowse ) then
                    begin
                        lIniciaPeriodoPago := TRUE;
                        try
                           RBEspecPeriodo.Checked := zStrToBool( FieldByName('VP_PAGO_US').AsString );
                           RBAutomatico.Checked := not RBEspecPeriodo.Checked;
                        finally
                               lIniciaPeriodoPago := FALSE;
                        end;
                    end;
                    ControlesSesion;
                    if ( FieldByName('VP_STATUS').AsInteger = Ord( spvProcesada ) )then
                       txtProcesada.Caption := ZetaCommonTools.ShowNomina( FieldByName('VP_NOMYEAR').AsInteger,
                                                                      FieldByName('VP_NOMTIPO').AsInteger,
                                                                      FieldByName('VP_NOMNUME').AsInteger)
                    else
                        txtProcesada.Caption := VACIO;
               end;
          end;
     end
     else
     begin
          with Field do
          begin
               if ( FieldName = 'VP_NOMYEAR' ) or ( FieldName = 'VP_NOMTIPO' ) then
                  SetFiltroPeriodo;
          end;
     end;
end;

procedure TEditPlanVacacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef SUPERVISORES}
     IndexDerechos := ZAccesosTress.D_SUPER_PLANVACACION;
     HelpContext:= H_PlanVacacion_Super;
     {$else}
     IndexDerechos := ZAccesosTress.D_CONS_PLANVACACION;
     HelpContext:= H_PlanVacacion;
     {$endif}
     FirstControl := VP_STATUS;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookup;
     VP_NOMNUME.LookupDataSet := dmCatalogos.cdsPeriodo;
     HabilitaControlesPeriodo( not RBAutomatico.Checked);
end;

procedure TEditPlanVacacion_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmCatalogos.cdsPeriodo.Conectar;
     CB_CODIGO.SetLlaveDescripcion( VACIO, VACIO );
     {$ifdef SUPERVISORES}
     with dmSuper do
     {$else}
     with dmRecursos do
     {$endif}
     DataSource.Dataset := cdsPlanVacacion;
end;

procedure TEditPlanVacacion_DevEx.VP_STATUSChange(Sender: TObject);
begin
     inherited;
     IF( VP_STATUS.ItemIndex = Ord(spvProcesada)) then
     begin
          VP_STATUS.ItemIndex := TZetaClientDataSet( DataSource.DataSet ).FieldByName( 'VP_STATUS' ).AsInteger;
          ZError(self.Caption, 'El status no puede cambiar a Procesada',0);
     end;
     SetcontrolesPago( eStatusPlanVacacion( VP_STATUS.ItemIndex ) );
end;

procedure TEditPlanVacacion_DevEx.HabilitaControlesPeriodo( lHabilita:Boolean );
begin
     VP_NOMYEAR.Enabled := lHabilita;
     VP_NOMTIPO.Enabled := lHabilita;
     VP_NOMNUME.Enabled := lHabilita;
     lblAnio.Enabled := lHabilita;
     lblTipo.Enabled := lHabilita;
     lblNumero.Enabled := lHabilita;
     //SetControls 
end;

procedure TEditPlanVacacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     gpbxSeccPerPago.Visible := ( ZAccesosMgr.CheckDerecho( {$ifdef SUPERVISORES}D_SUPER_PLANVACACION{$else}D_CONS_PLANVACACION {$endif}, K_DERECHO_ESPEC_PER_PAGO ) );
     VP_STATUS.Enabled := ( ZAccesosMgr.CheckDerecho( {$ifdef SUPERVISORES}D_SUPER_PLANVACACION{$else}D_CONS_PLANVACACION {$endif}, K_DERECHO_SIST_KARDEX ) );
     VP_NOMTIPO.ListaFija:=lfTipoPeriodo; //acl
     SetFiltroPeriodo;
     //SetControls;
end;

procedure TEditPlanVacacion_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
   TipoPer : eTipoPeriodo;
begin
     with DataSource.DataSet do
     begin
          iYear := FieldByName( 'VP_NOMYEAR' ).AsInteger;
          TipoPer := eTipoPeriodo( FieldByName( 'VP_NOMTIPO' ).AsInteger );
     end;
     with dmCatalogos do
     begin
          with cdsPeriodo do
          begin
               //Filtered := False;
               if ( IsEmpty ) or
                  ( ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPer ) <> FieldByName( 'PE_TIPO' ).AsInteger ) ) then
               begin
                    GetDatosPeriodo( iYear, TipoPer );
                    // Para que se refresque el Control de Lookup:
                    with VP_NOMNUME do
                    begin
                         SetLlaveDescripcion( VACIO, VACIO );
                         Llave := DataSource.DataSet.FieldByName( 'VP_NOMNUME' ).AsString;
                    end;
               end;
          end;
          AplicaFiltroStatusPeriodo(VP_NOMNUME,True);
     end;
end;



procedure TEditPlanVacacion_DevEx.RBEspecPeriodoClick(Sender: TObject);
begin
     inherited;
     with DataSource.DataSet do
     begin
          if( not lIniciaPeriodoPago )then
          begin
               if( not( State in [ dsEdit,dsInsert ] ) )then
               begin
                    Edit;
               end;
               FieldByName('VP_PAGO_US').AsString := zBoolToStr( RBEspecPeriodo.Checked );
          end;
     end;
end;

procedure TEditPlanVacacion_DevEx.SetcontrolesPago( eStatus: eStatusPlanVacacion );
var
   lAutorizada:Boolean;
begin
     lAutorizada := ( VP_STATUS.ItemIndex = Ord( eStatus ) );
     HabilitaControlesPeriodo( zStrToBool( DataSource.DataSet.FieldByName('VP_PAGO_US').AsString ) and ( lAutorizada ) );
     RBAutomatico.Enabled := lAutorizada;
     RBEspecPeriodo.Enabled := lAutorizada;
end;

procedure TEditPlanVacacion_DevEx.VP_NOMNUMEValidKey(Sender: TObject);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(VP_NOMNUME,True);
     inherited;
end;

{procedure TEditPlanVacacion.SetControls;
var
   lHabilitar:Boolean;
begin
     {$ifdef SUPERVISORES
     lHabilitar := False;
     {$else
     lHabilitar :=  ( ZAccesosMgr.CheckDerecho( D_CONS_PLANVACACION, K_DERECHO_BANCA )) and ( dmCatalogos.cdsPeriodo.FieldByName('PE_STATUS').AsInteger < Ord(spAfectadaParcial) );
     {$endif

     VP_NOMYEAR.Enabled := lHabilitar;
     VP_NOMTIPO.Enabled := lHabilitar;
     VP_NOMNUME.Enabled := lHabilitar;
     lblAnio.Enabled := lHabilitar;
     lblTipo.Enabled := lHabilitar;
     lblNumero.Enabled := lHabilitar;

     end;
 }
procedure TEditPlanVacacion_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     dmCatalogos.AplicaFiltroStatusPeriodo(VP_NOMNUME,False);
     inherited;

end;

end.
