inherited HisVacacion_DevEx: THisVacacion_DevEx
  Left = 522
  Top = 456
  Caption = 'Historial de Vacaciones'
  ClientHeight = 479
  ClientWidth = 863
  OnShow = FormShow
  ExplicitWidth = 863
  ExplicitHeight = 479
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 863
    ExplicitWidth = 863
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 537
      ExplicitLeft = 326
      ExplicitWidth = 537
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 531
        ExplicitLeft = 451
      end
    end
  end
  object PageControl_DevEx: TcxPageControl [1]
    Left = 0
    Top = 19
    Width = 863
    Height = 460
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = tabPrima_DevEx
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 439
    ClientRectLeft = 1
    ClientRectRight = 862
    ClientRectTop = 1
    object tabSaldo_DevEx: TcxTabSheet
      Caption = 'Saldo de Vacaciones'
      ImageIndex = 0
      OnShow = tabSaldo_DevExShow
      object GridSaldoVacaciones_DevEx: TZetaCXGrid
        Left = 0
        Top = 149
        Width = 861
        Height = 289
        Align = alClient
        TabOrder = 0
        object GridSaldoVacaciones_DevExDBTableView: TcxGridDBTableView
          OnDblClick = GridSaldoVacaciones_DevExDBTableViewDblClick
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          FilterBox.Visible = fvNever
          DataController.DataSource = dsSaldoVacaciones
          DataController.Filter.OnGetValueList = GridSaldoVacaciones_DevExDBTableViewDataControllerFilterGetValueList
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.OnSortingChanged = GridSaldoVacaciones_DevExDBTableViewDataControllerSortingChanged
          Filtering.ColumnFilteredItemsList = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OnColumnHeaderClick = GridSaldoVacaciones_DevExDBTableViewColumnHeaderClick
          object VS_ANIV: TcxGridDBColumn
            Caption = 'Aniv.'
            DataBinding.FieldName = 'VS_ANIV'
            MinWidth = 60
          end
          object VS_D_GOZO: TcxGridDBColumn
            Caption = 'Derecho Gozo'
            DataBinding.FieldName = 'VS_D_GOZO'
            MinWidth = 110
          end
          object VS_GOZO: TcxGridDBColumn
            Caption = 'Gozados'
            DataBinding.FieldName = 'VS_GOZO'
            MinWidth = 80
          end
          object VS_S_GOZO: TcxGridDBColumn
            Caption = 'Saldo Gozo'
            DataBinding.FieldName = 'VS_S_GOZO'
            MinWidth = 90
          end
          object VS_D_PAGO: TcxGridDBColumn
            Caption = 'Derecho Pago'
            DataBinding.FieldName = 'VS_D_PAGO'
            MinWidth = 105
          end
          object VS_PAGO: TcxGridDBColumn
            Caption = 'Pagados'
            DataBinding.FieldName = 'VS_PAGO'
            MinWidth = 80
          end
          object VS_S_PAGO: TcxGridDBColumn
            Caption = 'Saldo Pago'
            DataBinding.FieldName = 'VS_S_PAGO'
            MinWidth = 90
          end
          object VS_D_PRIMA: TcxGridDBColumn
            Caption = 'Derecho Prima'
            DataBinding.FieldName = 'VS_D_PRIMA'
            MinWidth = 105
          end
          object VS_PRIMA: TcxGridDBColumn
            Caption = 'Prima Pagados'
            DataBinding.FieldName = 'VS_PRIMA'
            MinWidth = 105
          end
          object VS_S_PRIMA: TcxGridDBColumn
            Caption = 'Saldo Prima'
            DataBinding.FieldName = 'VS_S_PRIMA'
            MinWidth = 90
          end
          object VS_FEC_VEN: TcxGridDBColumn
            Caption = 'Vencimiento'
            DataBinding.FieldName = 'VS_FEC_VEN'
            MinWidth = 90
          end
          object VS_CB_SAL: TcxGridDBColumn
            Caption = 'Salario'
            DataBinding.FieldName = 'VS_CB_SAL'
            MinWidth = 60
          end
        end
        object GridSaldoVacaciones_DevExLevel: TcxGridLevel
          GridView = GridSaldoVacaciones_DevExDBTableView
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 861
        Height = 149
        Align = alTop
        TabOrder = 1
        object GroupBox4: TGroupBox
          Left = 204
          Top = 8
          Width = 301
          Height = 139
          TabOrder = 0
          object Label29: TLabel
            Left = 62
            Top = 15
            Width = 57
            Height = 13
            Alignment = taRightJustify
            Caption = 'Antig'#252'edad:'
          end
          object CB_FEC_ANT_SALDO: TZetaDBTextBox
            Left = 122
            Top = 13
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_FEC_ANT_SALDO'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_FEC_ANT'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label27: TLabel
            Left = 10
            Top = 39
            Width = 109
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tabla de Prestaciones:'
          end
          object CB_TABLASS: TZetaDBTextBox
            Left = 122
            Top = 37
            Width = 168
            Height = 17
            AutoSize = False
            Caption = 'CB_TABLASS'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_TABLASS'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
        object GroupBox6: TGroupBox
          Left = 8
          Top = 8
          Width = 185
          Height = 139
          Caption = ' Saldo Total '
          TabOrder = 1
          object Label33: TLabel
            Left = 19
            Top = 17
            Width = 75
            Height = 13
            Alignment = taRightJustify
            Caption = 'D'#237'as por Gozar:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label37: TLabel
            Left = 17
            Top = 57
            Width = 77
            Height = 13
            Alignment = taRightJustify
            Caption = 'D'#237'as Prima Vac:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object SaldoGozo: TZetaDBTextBox
            Left = 103
            Top = 15
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'SaldoGozo'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'SaldoGozo'
            DataSource = dsSaldoVacaTotales
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object SaldoPrima: TZetaDBTextBox
            Left = 103
            Top = 55
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'SaldoPrima'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'SaldoPrima'
            DataSource = dsSaldoVacaTotales
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object SaldoPago: TZetaDBTextBox
            Left = 103
            Top = 35
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'SaldoPago'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'SaldoPago'
            DataSource = dsSaldoVacaTotales
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label42: TLabel
            Left = 19
            Top = 37
            Width = 75
            Height = 13
            Alignment = taRightJustify
            Caption = 'D'#237'as por Pagar:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
        end
      end
    end
    object tabGozo_DevEx: TcxTabSheet
      Caption = 'Vacaciones gozadas'
      ImageIndex = 0
      OnShow = tabGozo_DevExShow
      object ZetaDBGrid1_DevEx: TZetaCXGrid
        Left = 0
        Top = 149
        Width = 861
        Height = 289
        Align = alClient
        TabOrder = 0
        object ZetaDBGrid1_DevExDBTableView: TcxGridDBTableView
          OnDblClick = ZetaDBGrid1_DevExDBTableViewDblClick
          OnKeyDown = ZetaDBGrid1_DevExDBTableViewKeyDown
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          FilterBox.Visible = fvNever
          DataController.DataSource = dsVacacion
          DataController.Filter.OnGetValueList = ZetaDBGrid1_DevExDBTableViewDataControllerFilterGetValueList
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.OnSortingChanged = ZetaDBGrid1_DevExDBTableViewDataControllerSortingChanged
          Filtering.ColumnFilteredItemsList = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OnColumnHeaderClick = ZetaDBGrid1_DevExDBTableViewColumnHeaderClick
          object VA_FEC_INI: TcxGridDBColumn
            Caption = 'Fecha'
            DataBinding.FieldName = 'VA_FEC_INI'
            MinWidth = 75
          end
          object VA_TIPO: TcxGridDBColumn
            Caption = 'Tipo'
            DataBinding.FieldName = 'VA_TIPO'
            MinWidth = 65
          end
          object VA_YEAR: TcxGridDBColumn
            Caption = 'A'#241'o'
            DataBinding.FieldName = 'VA_YEAR'
            MinWidth = 65
          end
          object VA_D_GOZO: TcxGridDBColumn
            Caption = 'Derecho Gozo'
            DataBinding.FieldName = 'VA_D_GOZO'
            MinWidth = 110
          end
          object VA_GOZO: TcxGridDBColumn
            Caption = 'Gozados'
            DataBinding.FieldName = 'VA_GOZO'
            MinWidth = 75
          end
          object VA_S_GOZO: TcxGridDBColumn
            Caption = 'Saldo Gozo'
            DataBinding.FieldName = 'VA_S_GOZO'
            MinWidth = 90
          end
          object VA_GLOBAL: TcxGridDBColumn
            Caption = 'Global'
            DataBinding.FieldName = 'VA_GLOBAL'
            MinWidth = 70
          end
          object VA_COMENTA: TcxGridDBColumn
            Caption = 'Observaciones'
            DataBinding.FieldName = 'VA_COMENTA'
            MinWidth = 110
          end
        end
        object ZetaDBGrid1Level_DevEx: TcxGridLevel
          GridView = ZetaDBGrid1_DevExDBTableView
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 861
        Height = 149
        Align = alTop
        TabOrder = 1
        object GroupBox3: TGroupBox
          Left = 8
          Top = 8
          Width = 201
          Height = 139
          Caption = ' Gozo '
          TabOrder = 0
          object Label20: TLabel
            Left = 50
            Top = 17
            Width = 44
            Height = 13
            Alignment = taRightJustify
            Caption = 'Derecho:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label21: TLabel
            Left = 9
            Top = 97
            Width = 18
            Height = 13
            Caption = '( + )'
          end
          object Label22: TLabel
            Left = 32
            Top = 97
            Width = 62
            Height = 13
            Alignment = taRightJustify
            Caption = 'Proporcional:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label23: TLabel
            Left = 9
            Top = 57
            Width = 15
            Height = 13
            Caption = '( - )'
          end
          object Label24: TLabel
            Left = 49
            Top = 57
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Gozados:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label25: TLabel
            Left = 9
            Top = 117
            Width = 18
            Height = 13
            Caption = '( = )'
          end
          object Label26: TLabel
            Left = 31
            Top = 117
            Width = 63
            Height = 13
            Alignment = taRightJustify
            Caption = 'Saldo Actual:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CB_DER_GOZ: TZetaDBTextBox
            Left = 103
            Top = 15
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_DER_GOZ'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_DER_GOZ'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_V_GOZO: TZetaDBTextBox
            Left = 103
            Top = 55
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_V_GOZO'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_V_GOZO'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label5: TLabel
            Left = 9
            Top = 77
            Width = 18
            Height = 13
            Caption = '( = )'
          end
          object Label6: TLabel
            Left = 45
            Top = 77
            Width = 49
            Height = 13
            Alignment = taRightJustify
            Caption = 'Sub-Total:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CB_SUB_GOZ: TZetaDBTextBox
            Left = 103
            Top = 75
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_SUB_GOZ'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_SUB_GOZ'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_PRO_GOZ: TZetaDBTextBox
            Left = 103
            Top = 95
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_PRO_GOZ'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_PRO_GOZ'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_TOT_GOZ: TZetaDBTextBox
            Left = 103
            Top = 115
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_TOT_GOZ'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_TOT_GOZ'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label7: TLabel
            Left = 9
            Top = 37
            Width = 18
            Height = 13
            Caption = '( + )'
          end
          object Label11: TLabel
            Left = 38
            Top = 37
            Width = 56
            Height = 13
            Alignment = taRightJustify
            Caption = 'Pendientes:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CB_PEN_GOZ: TZetaDBTextBox
            Left = 103
            Top = 35
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_PEN_GOZ'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_PEN_GOZ'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object bbCalSubTotal_DevEx: TcxButton
            Left = 172
            Top = 73
            Width = 21
            Height = 21
            Hint = 'Mostrar Calendario de Vacaciones'
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A4050000000000000000000000000000000000005B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFFBFD4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3E2FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF5F8FFFFADC9
              FFFFADC9FFFFD6E4FFFFB7CFFFFFADC9FFFFB7CFFFFFD6E4FFFFADC9FFFFADC9
              FFFFEAF1FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFFEAF1FFFFEAF1FFFF5B92FFFF5B92FFFFADC9FFFF70A0FFFF5B92FFFF70A0
              FFFFADC9FFFF5B92FFFF5B92FFFFD6E4FFFFFFFFFFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF5F8FFFFADC9FFFFADC9FFFFD6E4
              FFFFB7CFFFFFADC9FFFFB7CFFFFFD6E4FFFFADC9FFFFADC9FFFFEAF1FFFFFFFF
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFEDF3
              FFFF70A0FFFF70A0FFFFB7CFFFFF82ACFFFF70A0FFFF82ACFFFFB7CFFFFF70A0
              FFFF70A0FFFFDBE7FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFFEAF1FFFFEAF1FFFF5B92FFFF5B92FFFFADC9FFFF70A0FFFF5B92
              FFFF70A0FFFFADC9FFFF5B92FFFF5B92FFFFD6E4FFFFFFFFFFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF7FAFFFFC1D6FFFFC1D6
              FFFFE0EAFFFFC9DBFFFFC1D6FFFFC9DBFFFFE0EAFFFFC1D6FFFFC1D6FFFFF0F5
              FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1
              FFFFEAF1FFFF5B92FFFF5B92FFFFADC9FFFF70A0FFFF5B92FFFF70A0FFFFADC9
              FFFF5B92FFFF5B92FFFFD6E4FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFFEAF1FFFFF0F5FFFF84ADFFFF84ADFFFFC1D6FFFF94B8
              FFFF84ADFFFF94B8FFFFC1D6FFFF84ADFFFF84ADFFFFE0EAFFFFFFFFFFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFFEAF1FFFFFFFFFFFF94B8FFFF82ACFFFF7AA7FFFFEDF3FFFFFFFFFFFFFFFF
              FFFF75A3FFFF84ADFFFF84ADFFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFFCCDDFFFFFFFFFFFF84ADFFFFEAF1FFFFADC9
              FFFFEAF1FFFFFFFFFFFFFFFFFFFF84ADFFFFFFFFFFFF84ADFFFFFFFFFFFFE0EA
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFF82AC
              FFFF6599FFFFC9DBFFFF94B8FFFF7FAAFFFF84ADFFFF84ADFFFF72A1FFFFE3EC
              FFFF689BFFFF82ACFFFF6095FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = bbCalSubTotal_DevExClick
          end
          object bbCalSaldoActual_DevEx: TcxButton
            Left = 172
            Top = 112
            Width = 21
            Height = 21
            Hint = 'Mostrar Calendario de Vacaciones'
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A4050000000000000000000000000000000000005B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFFBFD4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3E2FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF5F8FFFFADC9
              FFFFADC9FFFFD6E4FFFFB7CFFFFFADC9FFFFB7CFFFFFD6E4FFFFADC9FFFFADC9
              FFFFEAF1FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFFEAF1FFFFEAF1FFFF5B92FFFF5B92FFFFADC9FFFF70A0FFFF5B92FFFF70A0
              FFFFADC9FFFF5B92FFFF5B92FFFFD6E4FFFFFFFFFFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF5F8FFFFADC9FFFFADC9FFFFD6E4
              FFFFB7CFFFFFADC9FFFFB7CFFFFFD6E4FFFFADC9FFFFADC9FFFFEAF1FFFFFFFF
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFEDF3
              FFFF70A0FFFF70A0FFFFB7CFFFFF82ACFFFF70A0FFFF82ACFFFFB7CFFFFF70A0
              FFFF70A0FFFFDBE7FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFFEAF1FFFFEAF1FFFF5B92FFFF5B92FFFFADC9FFFF70A0FFFF5B92
              FFFF70A0FFFFADC9FFFF5B92FFFF5B92FFFFD6E4FFFFFFFFFFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF7FAFFFFC1D6FFFFC1D6
              FFFFE0EAFFFFC9DBFFFFC1D6FFFFC9DBFFFFE0EAFFFFC1D6FFFFC1D6FFFFF0F5
              FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1
              FFFFEAF1FFFF5B92FFFF5B92FFFFADC9FFFF70A0FFFF5B92FFFF70A0FFFFADC9
              FFFF5B92FFFF5B92FFFFD6E4FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFFEAF1FFFFF0F5FFFF84ADFFFF84ADFFFFC1D6FFFF94B8
              FFFF84ADFFFF94B8FFFFC1D6FFFF84ADFFFF84ADFFFFE0EAFFFFFFFFFFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFFEAF1FFFFFFFFFFFF94B8FFFF82ACFFFF7AA7FFFFEDF3FFFFFFFFFFFFFFFF
              FFFF75A3FFFF84ADFFFF84ADFFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFFCCDDFFFFFFFFFFFF84ADFFFFEAF1FFFFADC9
              FFFFEAF1FFFFFFFFFFFFFFFFFFFF84ADFFFFFFFFFFFF84ADFFFFFFFFFFFFE0EA
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFF82AC
              FFFF6599FFFFC9DBFFFF94B8FFFF7FAAFFFF84ADFFFF84ADFFFF72A1FFFFE3EC
              FFFF689BFFFF82ACFFFF6095FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
              FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = bbCalSaldoActual_DevExClick
          end
        end
        object GroupBox2: TGroupBox
          Left = 215
          Top = 8
          Width = 189
          Height = 139
          TabOrder = 1
          object Label1: TLabel
            Left = 2
            Top = 20
            Width = 96
            Height = 13
            Alignment = taRightJustify
            Caption = 'Ultimas Vacaciones:'
          end
          object CB_FEC_VAC: TZetaDBTextBox
            Left = 102
            Top = 18
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_FEC_VAC'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_FEC_VAC'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label4: TLabel
            Left = 36
            Top = 44
            Width = 62
            Height = 13
            Caption = 'Ultimo Cierre:'
          end
          object CB_DER_FEC: TZetaDBTextBox
            Left = 102
            Top = 42
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_DER_FEC'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_DER_FEC'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Antig: TLabel
            Left = 42
            Top = 66
            Width = 57
            Height = 13
            Alignment = taRightJustify
            Caption = 'Antig'#252'edad:'
          end
          object CB_FEC_ANT: TZetaDBTextBox
            Left = 102
            Top = 64
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_FEC_ANT'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_FEC_ANT'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
      end
    end
    object tabPago_DevEx: TcxTabSheet
      Caption = 'Vacaciones pagadas'
      ImageIndex = 1
      OnShow = tabPago_DevExShow
      object ZetaDBGrid2_DevEx: TZetaCXGrid
        Left = 0
        Top = 149
        Width = 861
        Height = 289
        Align = alClient
        TabOrder = 0
        object ZetaDBGrid2_DevExDBTableView: TcxGridDBTableView
          OnDblClick = ZetaDBGrid2_DevExDBTableViewDblClick
          OnKeyDown = ZetaDBGrid2_DevExDBTableViewKeyDown
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          FilterBox.Visible = fvNever
          DataController.DataSource = dsVacacion
          DataController.Filter.OnGetValueList = ZetaDBGrid2_DevExDBTableViewDataControllerFilterGetValueList
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.OnSortingChanged = ZetaDBGrid2_DevExDBTableViewDataControllerSortingChanged
          Filtering.ColumnFilteredItemsList = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OnColumnHeaderClick = ZetaDBGrid2_DevExDBTableViewColumnHeaderClick
          object VA_FEC_INI2: TcxGridDBColumn
            Caption = 'Fecha'
            DataBinding.FieldName = 'VA_FEC_INI'
            MinWidth = 65
          end
          object VA_TIPO2: TcxGridDBColumn
            Caption = 'Tipo'
            DataBinding.FieldName = 'VA_TIPO'
            MinWidth = 65
          end
          object VA_YEAR2: TcxGridDBColumn
            Caption = 'A'#241'o'
            DataBinding.FieldName = 'VA_YEAR'
            MinWidth = 65
          end
          object VA_D_PAGO: TcxGridDBColumn
            Caption = 'Derecho Pago'
            DataBinding.FieldName = 'VA_D_PAGO'
            MinWidth = 105
          end
          object VA_PAGO: TcxGridDBColumn
            Caption = 'Pagados'
            DataBinding.FieldName = 'VA_PAGO'
            MinWidth = 75
          end
          object VA_S_PAGO: TcxGridDBColumn
            Caption = 'Saldo Pago'
            DataBinding.FieldName = 'VA_S_PAGO'
            MinWidth = 90
          end
          object VA_GLOBAL2: TcxGridDBColumn
            Caption = 'Global'
            DataBinding.FieldName = 'VA_GLOBAL'
            MinWidth = 70
          end
          object VA_COMENTA2: TcxGridDBColumn
            Caption = 'Observaciones'
            DataBinding.FieldName = 'VA_COMENTA'
            MinWidth = 105
          end
        end
        object ZetaDBGrid2Level_DevEx: TcxGridLevel
          GridView = ZetaDBGrid2_DevExDBTableView
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 861
        Height = 149
        Align = alTop
        TabOrder = 1
        object GroupBox5: TGroupBox
          Left = 204
          Top = 8
          Width = 189
          Height = 139
          TabOrder = 0
          object Label30: TLabel
            Left = 2
            Top = 20
            Width = 96
            Height = 13
            Alignment = taRightJustify
            Caption = 'Ultimas Vacaciones:'
          end
          object CB_FEC_VAC2: TZetaDBTextBox
            Left = 102
            Top = 18
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_FEC_VAC2'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_FEC_VAC'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label31: TLabel
            Left = 36
            Top = 44
            Width = 62
            Height = 13
            Caption = 'Ultimo Cierre:'
          end
          object CB_DER_FEC2: TZetaDBTextBox
            Left = 102
            Top = 42
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_DER_FEC2'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_DER_FEC'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label32: TLabel
            Left = 42
            Top = 66
            Width = 57
            Height = 13
            Alignment = taRightJustify
            Caption = 'Antig'#252'edad:'
          end
          object CB_FEC_ANT2: TZetaDBTextBox
            Left = 102
            Top = 64
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_FEC_ANT2'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_FEC_ANT'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
        object GroupBox1: TGroupBox
          Left = 8
          Top = 8
          Width = 185
          Height = 139
          Caption = ' Pago '
          TabOrder = 1
          object Label8: TLabel
            Left = 50
            Top = 17
            Width = 44
            Height = 13
            Alignment = taRightJustify
            Caption = 'Derecho:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label9: TLabel
            Left = 9
            Top = 97
            Width = 18
            Height = 13
            Caption = '( + )'
          end
          object Label10: TLabel
            Left = 32
            Top = 97
            Width = 62
            Height = 13
            Alignment = taRightJustify
            Caption = 'Proporcional:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label12: TLabel
            Left = 9
            Top = 57
            Width = 15
            Height = 13
            Caption = '( - )'
          end
          object Label15: TLabel
            Left = 49
            Top = 57
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Pagados:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label16: TLabel
            Left = 9
            Top = 117
            Width = 18
            Height = 13
            Caption = '( = )'
          end
          object Label17: TLabel
            Left = 31
            Top = 117
            Width = 63
            Height = 13
            Alignment = taRightJustify
            Caption = 'Saldo Actual:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CB_DER_PAG: TZetaDBTextBox
            Left = 103
            Top = 15
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_DER_PAG'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_DER_PAG'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_V_PAGO: TZetaDBTextBox
            Left = 103
            Top = 55
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_V_PAGO'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_V_PAGO'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label2: TLabel
            Left = 9
            Top = 77
            Width = 18
            Height = 13
            Caption = '( = )'
          end
          object Label3: TLabel
            Left = 45
            Top = 77
            Width = 49
            Height = 13
            Alignment = taRightJustify
            Caption = 'Sub-Total:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CB_SUB_PAG: TZetaDBTextBox
            Left = 103
            Top = 75
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_SUB_PAG'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_SUB_PAG'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_PRO_PAG: TZetaDBTextBox
            Left = 103
            Top = 95
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_PRO_PAG'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_PRO_PAG'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_TOT_PAG: TZetaDBTextBox
            Left = 103
            Top = 115
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_TOT_PAG'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_TOT_PAG'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_PEN_PAG: TZetaDBTextBox
            Left = 103
            Top = 35
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_PEN_PAG'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_PEN_PAG'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label13: TLabel
            Left = 38
            Top = 37
            Width = 56
            Height = 13
            Alignment = taRightJustify
            Caption = 'Pendientes:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label14: TLabel
            Left = 9
            Top = 37
            Width = 18
            Height = 13
            Caption = '( + )'
          end
        end
      end
    end
    object tabPrima_DevEx: TcxTabSheet
      Caption = 'Prima vacacional pagada'
      ImageIndex = 2
      OnShow = tabPrima_DevExShow
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 861
        Height = 149
        Align = alTop
        TabOrder = 0
        object GroupBox8: TGroupBox
          Left = 204
          Top = 8
          Width = 189
          Height = 139
          TabOrder = 0
          object Label51: TLabel
            Left = 2
            Top = 20
            Width = 96
            Height = 13
            Alignment = taRightJustify
            Caption = 'Ultimas Vacaciones:'
          end
          object CB_FEC_VAC3: TZetaDBTextBox
            Left = 102
            Top = 18
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_FEC_VAC3'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_FEC_VAC'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label52: TLabel
            Left = 36
            Top = 44
            Width = 62
            Height = 13
            Caption = 'Ultimo Cierre:'
          end
          object CB_DER_FEC3: TZetaDBTextBox
            Left = 102
            Top = 42
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_DER_FEC3'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_DER_FEC'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label53: TLabel
            Left = 42
            Top = 66
            Width = 57
            Height = 13
            Alignment = taRightJustify
            Caption = 'Antig'#252'edad:'
          end
          object CB_FEC_ANT3: TZetaDBTextBox
            Left = 102
            Top = 64
            Width = 80
            Height = 17
            AutoSize = False
            Caption = 'CB_FEC_ANT3'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_FEC_ANT'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
        object GroupBox9: TGroupBox
          Left = 8
          Top = 8
          Width = 185
          Height = 139
          Caption = ' Pago '
          TabOrder = 1
          object Label54: TLabel
            Left = 50
            Top = 17
            Width = 44
            Height = 13
            Alignment = taRightJustify
            Caption = 'Derecho:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label55: TLabel
            Left = 9
            Top = 97
            Width = 18
            Height = 13
            Caption = '( + )'
          end
          object Label56: TLabel
            Left = 32
            Top = 97
            Width = 62
            Height = 13
            Alignment = taRightJustify
            Caption = 'Proporcional:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label57: TLabel
            Left = 9
            Top = 57
            Width = 15
            Height = 13
            Caption = '( - )'
          end
          object Label58: TLabel
            Left = 49
            Top = 57
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Pagados:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label59: TLabel
            Left = 9
            Top = 117
            Width = 18
            Height = 13
            Caption = '( = )'
          end
          object Label60: TLabel
            Left = 31
            Top = 117
            Width = 63
            Height = 13
            Alignment = taRightJustify
            Caption = 'Saldo Actual:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CB_DER_PV: TZetaDBTextBox
            Left = 103
            Top = 15
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_DER_PV'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_DER_PV'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_V_PRIMA: TZetaDBTextBox
            Left = 103
            Top = 55
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_V_PRIMA'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_V_PRIMA'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label61: TLabel
            Left = 9
            Top = 77
            Width = 18
            Height = 13
            Caption = '( = )'
          end
          object Label62: TLabel
            Left = 45
            Top = 77
            Width = 49
            Height = 13
            Alignment = taRightJustify
            Caption = 'Sub-Total:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object CB_SUB_PV: TZetaDBTextBox
            Left = 103
            Top = 75
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_SUB_PV'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_SUB_PV'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_PRO_PV: TZetaDBTextBox
            Left = 103
            Top = 95
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_PRO_PV'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_PRO_PV'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_TOT_PV: TZetaDBTextBox
            Left = 103
            Top = 115
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_TOT_PV'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_TOT_PV'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object CB_PEN_PV: TZetaDBTextBox
            Left = 103
            Top = 35
            Width = 65
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'CB_PEN_PV'
            ShowAccelChar = False
            Brush.Color = clSilver
            Border = False
            DataField = 'CB_PEN_PV'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label18: TLabel
            Left = 38
            Top = 37
            Width = 56
            Height = 13
            Alignment = taRightJustify
            Caption = 'Pendientes:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label19: TLabel
            Left = 9
            Top = 37
            Width = 18
            Height = 13
            Caption = '( + )'
          end
        end
      end
      object ZetaDBGrid3_DevEx: TZetaCXGrid
        Left = 0
        Top = 149
        Width = 861
        Height = 289
        Align = alClient
        TabOrder = 1
        object ZetaDBGrid3_DevExDBTableView: TcxGridDBTableView
          OnDblClick = ZetaDBGrid2_DevExDBTableViewDblClick
          OnKeyDown = ZetaDBGrid3_DevExDBTableViewKeyDown
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          FilterBox.Visible = fvNever
          DataController.DataSource = dsVacacion
          DataController.Filter.OnGetValueList = ZetaDBGrid3_DevExDBTableViewDataControllerFilterGetValueList
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          DataController.OnSortingChanged = ZetaDBGrid3_DevExDBTableViewDataControllerSortingChanged
          Filtering.ColumnFilteredItemsList = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OnColumnHeaderClick = ZetaDBGrid3_DevExDBTableViewColumnHeaderClick
          object VA_FEC_INI3: TcxGridDBColumn
            Caption = 'Fecha'
            DataBinding.FieldName = 'VA_FEC_INI'
            MinWidth = 65
          end
          object VA_TIPO3: TcxGridDBColumn
            Caption = 'Tipo'
            DataBinding.FieldName = 'VA_TIPO'
            MinWidth = 65
          end
          object VA_YEAR3: TcxGridDBColumn
            Caption = 'A'#241'o'
            DataBinding.FieldName = 'VA_YEAR'
            MinWidth = 65
          end
          object VA_D_PRIMA: TcxGridDBColumn
            Caption = 'Derecho PV'
            DataBinding.FieldName = 'VA_D_PRIMA'
            MinWidth = 95
          end
          object VA_P_PRIMA: TcxGridDBColumn
            Caption = 'Pago PV'
            DataBinding.FieldName = 'VA_P_PRIMA'
            MinWidth = 80
          end
          object VA_S_PRIMA: TcxGridDBColumn
            Caption = 'Saldo PV'
            DataBinding.FieldName = 'VA_S_PRIMA'
            MinWidth = 80
          end
          object VA_GLOBAL3: TcxGridDBColumn
            Caption = 'Global'
            DataBinding.FieldName = 'VA_GLOBAL'
            MinWidth = 70
          end
          object VA_COMENTA3: TcxGridDBColumn
            Caption = 'Observaciones'
            DataBinding.FieldName = 'VA_COMENTA'
            MinWidth = 110
          end
        end
        object ZetaDBGrid3Level_DevEx: TcxGridLevel
          GridView = ZetaDBGrid3_DevExDBTableView
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 32
    Top = 0
  end
  object dsVacacion: TDataSource
    Left = 164
  end
  object dsSaldoVacaTotales: TDataSource
    Left = 948
    Top = 16
  end
  object dsSaldoVacaciones: TDataSource
    Left = 996
    Top = 16
  end
end
