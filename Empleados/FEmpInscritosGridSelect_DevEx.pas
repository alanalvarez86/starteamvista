unit FEmpInscritosGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  Buttons, ExtCtrls, ZetaCommonClasses,ZetaCommonLists,
  ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  cxButtons, cxContainer, cxGroupBox, cxRadioGroup;

type
  TEmpInscribirGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    Panel1: TPanel;
    grMostrarEmpleados: TcxRadioGroup;
    btnMostrar: TcxButton;
    lTruncar: TCheckBox;
    btnFiltrosCondiciones: TcxButton;
    procedure btnMostrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnFiltrarClick(Sender: TObject);
    procedure btnFiltrosCondicionesClick(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
    procedure RevertirClick(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
    procedure ImprimirClick(Sender: TObject);
  private
    FTipoRango: eTipoRangoActivo;
    function GetTruncar : boolean;

    { Private declarations }
  public
    { Public declarations }
    property Truncar : boolean read GetTruncar;
  end;

var
  EmpInscribirGridSelect_DevEx: TEmpInscribirGridSelect_DevEx;
  FParametros : TZetaParams;
  FDescripcion : TZetaParams;
  FRangoI,FRangoF,FLista,FCondicion: string;

implementation

uses dRecursos, ZBasicoSelectGrid_DevEx,FFiltroEmpleados_DevEx, ZetaDialogo,FBaseReportes_DevEx;

{$R *.dfm}

procedure TEmpInscribirGridSelect_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FParametros:= TZetaParams.Create;
     FDescripcion :=  TZetaParams.Create;
     with FParametros, dmRecursos.cdsSesion do
     begin
          AddInteger( 'GRUPO', FieldByName('SE_FOLIO').AsInteger  );
          AddDate( 'FECHA', FieldByName('SE_FEC_INI').AsDateTime );
          AddString( 'CURSO', FieldByName('CU_CODIGO').AsString );
     end;
     lTruncar.Checked := true;
     grMostrarEmpleados.ItemIndex := 0;
     ZBasicoSelectGrid_DevEx.ParametrosGrid := FDescripcion;
     FRangoI := VACIO;
     FRangoF := VACIO;
     FLista := VACIO;
     FCondicion := VACIO;
end;

procedure TEmpInscribirGridSelect_DevEx.FormShow(Sender: TObject);
begin
ZetadbGridDBTableView.DataController.CreateAllItems();
     inherited;
     btnMostrar.SetFocus;
     OK_DevEx.Enabled := False;
end;

procedure TEmpInscribirGridSelect_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FDescripcion );
     FreeAndNil( FParametros );
     zetaDbGridDbTableView.ApplyBestFit();
end;

function TEmpInscribirGridSelect_DevEx.GetTruncar: boolean;
begin
     Result := lTruncar.Checked;
end;

procedure TEmpInscribirGridSelect_DevEx.btnMostrarClick(Sender: TObject);
const
     aTipoCurso : array [0 .. 1] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'programado', 'reprobado');
begin
     inherited;

     with FParametros do
     begin
          AddInteger('MOSTRAR', grMostrarEmpleados.ItemIndex);
     end;
     dmRecursos.MostrarEmpleadosAInscribir(FParametros);
     OK_DevEx.Enabled := not DataSet.IsEmpty;
     with FDescripcion do
     begin
          //AddString ('Tipo de empleado', format('Empleados con el curso %s', [temp]));
          AddString ('Tipo de empleado', grMostrarEmpleados.Properties.Items[grMostrarEmpleados.ItemIndex].Caption );
     end;
     zetaDbGridDbTableView.ApplyBestFit();
end;



procedure TEmpInscribirGridSelect_DevEx.BtnFiltrarClick(Sender: TObject);
begin
     if ( not DataSet.IsEmpty ) then
        inherited;
     zetaDbGridDbTableView.ApplyBestFit();

end;

procedure TEmpInscribirGridSelect_DevEx.btnFiltrosCondicionesClick(
  Sender: TObject);
begin
     inherited;
     Try
           FiltroEmpleados_DevEx := TFiltroEmpleados_DevEx.Create(Application);
           FiltroEmpleados_DevEx.ParameterList := FParametros;
           FiltroEmpleados_DevEx.TipoRango := FTipoRango;
           FiltroEmpleados_DevEx.SetRangoLista(FRangoI,FRangoF ,FLista ,FCondicion );
           FiltroEmpleados_DevEx.ShowModal;
           OK_DevEx.Enabled := not DataSet.IsEmpty;
     Finally
            FiltroEmpleados_DevEx.GetRangoLista(FRangoI,FRangoF ,FLista,FCondicion  );
            FTipoRango := FiltroEmpleados_DevEx.TipoRango;
            FiltroEmpleados_DevEx.Free;
     end;
end;

procedure TEmpInscribirGridSelect_DevEx.ExcluirClick(Sender: TObject);
begin
  inherited;
       OK_DevEx.Enabled := not DataSet.IsEmpty;
end;

procedure TEmpInscribirGridSelect_DevEx.RevertirClick(Sender: TObject);
begin
  inherited;
        OK_DevEx.Enabled := not DataSet.IsEmpty;
end;

procedure TEmpInscribirGridSelect_DevEx.ExportarClick(Sender: TObject);
begin
inherited;
//  if ZetaDialogo.zConfirm( 'Exportar...', '� Desea exportar a Excel la lista mostrada ?', 0, mbYes ) then
  //      FBaseReportes_DevEx.ExportarGridParams( ZetaDBGridDBTableView, ZetaDBGridDBTableView.DataController.DataSource.DataSet, ParametrosGrid, Caption, 'IM');

end;

procedure TEmpInscribirGridSelect_DevEx.ImprimirClick(Sender: TObject);
begin
inherited ;
//if ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir La Lista Mostrada ?', 0, mbYes ) then
  //      FBaseReportes_DevEx.ImprimirGridParams( ZetaDBGridDBTableView, ZetaDBGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM', ParametrosGrid);

end;

end.
