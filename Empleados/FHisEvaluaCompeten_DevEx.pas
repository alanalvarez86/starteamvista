unit FHisEvaluaCompeten_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,} ZBaseGridLectura_DevEx, Grids, DBGrids, Db,
  ZetaDBGrid, ExtCtrls, StdCtrls, Buttons, ZetaKeyCombo, 
  ComCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  THisEvaluaCompeten_DevEx = class(TBaseGridLectura_DevEx)
    CC_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    NC_DESCRIP: TcxGridDBColumn;
    EC_FECHA: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    EC_COMENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;

  public
    { Public declarations }
  end;

var
  HisEvaluaCompeten_DevEx: THisEvaluaCompeten_DevEx;

implementation

{$R *.DFM}

uses {$IFDEF SUPERVISORES}
     dSuper,
     {$ELSE}
     dRecursos,
     {$ENDIF}
     DCatalogos,
     dSistema, ZetaCommonLists,ZetaCommonTools, ZetaCommonClasses,ZAccesosTress,ZAccesosMgr ;

procedure THisEvaluaCompeten_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;

     with {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF} do
     begin
          cdsHisCompEvalua.Conectar;
          DataSource.DataSet:= cdsHisCompEvalua;
     end;
end;


procedure THisEvaluaCompeten_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;

     {$IFDEF SUPERVISORES}
     IndexDerechos := D_SUP_EVAL_COMPETEN;
     HelpContext:= H_Sup_EMP_EVALUA_COMPETEN; 
     {$else}
     IndexDerechos := D_EMP_HIS_EVA_COMPETEN;
     HelpContext:= H_EMP_EVALUA_COMPETEN;
     {$endif}
end;

procedure THisEvaluaCompeten_DevEx.Agregar;
begin
     {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsHisCompEvalua.Agregar;
end;

procedure THisEvaluaCompeten_DevEx.Borrar;
begin
     {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsHisCompEvalua.Borrar;
     DoBestFit;
end;

procedure THisEvaluaCompeten_DevEx.Modificar;
begin
     {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsHisCompEvalua.Modificar;
end;


procedure THisEvaluaCompeten_DevEx.Refresh;
begin
     inherited;
     {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsHisCompEvalua.Refrescar;
      DoBestFit;
end;

procedure THisEvaluaCompeten_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
  //CreaColumaSumatoria('EC_COMENT',SkCount);
end;

end.
