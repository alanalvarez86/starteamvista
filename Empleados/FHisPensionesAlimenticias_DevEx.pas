unit FHisPensionesAlimenticias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Grids, DBGrids, Db,
  ZetaDBGrid, ExtCtrls, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  THisPensionesAlimenticias_DevEx = class(TBaseGridLectura_DevEx)
    PS_ORDEN: TcxGridDBColumn;
    PS_FECHA: TcxGridDBColumn;
    PS_EXPEDIE: TcxGridDBColumn;
    PS_BENEFIC: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    PS_ACTIVA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  HisPensionesAlimenticias_DevEx: THisPensionesAlimenticias_DevEx;

implementation

{$R *.DFM}

uses dRecursos, dSistema, ZetaCommonLists, ZetaCommonClasses ;

procedure THisPensionesAlimenticias_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;

     with dmRecursos do
     begin
          cdsHisPensionesAlimenticias.Conectar;
          DataSource.DataSet:= cdsHisPensionesAlimenticias;
     end;
end;

procedure THisPensionesAlimenticias_DevEx.Refresh;
begin
     dmRecursos.cdsHisPensionesAlimenticias.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;


procedure THisPensionesAlimenticias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_EMP_EXP_PENSIONES;
end;

procedure THisPensionesAlimenticias_DevEx.Agregar;
begin
     dmRecursos.cdsHisPensionesAlimenticias.Agregar;
end;

procedure THisPensionesAlimenticias_DevEx.Borrar;
begin
     dmRecursos.cdsHisPensionesAlimenticias.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisPensionesAlimenticias_DevEx.Modificar;
begin
     dmRecursos.cdsHisPensionesAlimenticias.Modificar;
end;


procedure THisPensionesAlimenticias_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;
end;

end.
