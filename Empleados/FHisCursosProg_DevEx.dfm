inherited HisCursosProg_DevEx: THisCursosProg_DevEx
  Left = 65
  Top = 279
  Caption = 'Historial de Cursos Programados'
  ClientWidth = 769
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 769
    inherited Slider: TSplitter
      Left = 348
    end
    inherited ValorActivo1: TPanel
      Width = 332
    end
    inherited ValorActivo2: TPanel
      Left = 351
      Width = 418
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 769
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object KC_FEC_PRO: TcxGridDBColumn
        Caption = 'Programado'
        DataBinding.FieldName = 'KC_FEC_PRO'
        MinWidth = 90
      end
      object CU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CU_CODIGO'
        MinWidth = 70
      end
      object NOMBRE: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'NOMBRE'
        MinWidth = 75
      end
      object KC_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n Tomada'
        DataBinding.FieldName = 'KC_REVISIO'
        MinWidth = 115
      end
      object KC_FEC_TOM: TcxGridDBColumn
        Caption = 'Tomado'
        DataBinding.FieldName = 'KC_FEC_TOM'
        MinWidth = 80
      end
      object KC_PROXIMO: TcxGridDBColumn
        Caption = 'Pr'#243'ximo'
        DataBinding.FieldName = 'KC_PROXIMO'
        MinWidth = 80
      end
      object KC_HORAS: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'KC_HORAS'
        MinWidth = 75
      end
      object KC_EVALUA: TcxGridDBColumn
        Caption = 'Evaluaci'#243'n'
        DataBinding.FieldName = 'KC_EVALUA'
        MinWidth = 85
      end
      object EN_OPCIONA: TcxGridDBColumn
        Caption = 'Opcional'
        DataBinding.FieldName = 'EN_OPCIONA'
        MinWidth = 75
      end
      object MA_CODIGO: TcxGridDBColumn
        Caption = 'Maestro'
        DataBinding.FieldName = 'MA_CODIGO'
        MinWidth = 75
      end
      object CP_MANUAL: TcxGridDBColumn
        Caption = 'Manual'
        DataBinding.FieldName = 'CP_MANUAL'
        MinWidth = 75
      end
      object EP_GLOBAL: TcxGridDBColumn
        Caption = 'Global'
        DataBinding.FieldName = 'EP_GLOBAL'
        MinWidth = 75
      end
    end
  end
  inherited DataSource: TDataSource
    Top = 208
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end