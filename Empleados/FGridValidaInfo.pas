unit FGridValidaInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, StdCtrls, Buttons, Db, ZetaDBGrid,
  ZetaDBTextBox, TDMULTIP, DBCtrls;

type
  TFGridInfoAlta = class(TForm)
    PanelBotones: TPanel;
    ContinuarBtn: TBitBtn;
    CancelarBtn: TBitBtn;
    DataSource: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    GridEmpleados: TZetaDBGrid;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    CB_NOMBRES: TZetaDBTextBox;
    CB_RFC: TZetaDBTextBox;
    CB_SEGSOC: TZetaDBTextBox;
    CB_FEC_NAC: TZetaDBTextBox;
    Label6: TLabel;
    Label7: TLabel;
    CB_APE_MAT: TZetaDBTextBox;
    CB_APE_PAT: TZetaDBTextBox;
    FOTO: TPDBMultiImage;
    Panel4: TPanel;
    Label8: TLabel;
    CB_CURP: TZetaDBTextBox;
    Label9: TLabel;
    CB_RECONTR: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure CancelarBtnClick(Sender: TObject);
    procedure GridEmpleadosDblClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure GridEmpleadosDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure FormShow(Sender: TObject);
  private
    function GetEmpleadoSeleccionado: Integer;
    { Private declarations }
  public
    { Public declarations }
    property EmpleadoSeleccionado: Integer read GetEmpleadoSeleccionado;
  end;

var
  FGridInfoAlta: TFGridInfoAlta;

implementation

uses DRecursos,
     DCliente,
     DGlobal,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{$define CAMBIOS_SENDA}
{.$undefine CAMBIOS_SENDA}

{$R *.DFM}

procedure TFGridInfoAlta.FormCreate(Sender: TObject);
begin
     HelpContext := H10152_Tress_verifica_la_existencia_de_nuevos_empleados;
     DataSource.DataSet := dmRecursos.cdsInfoEmp;
end;

procedure TFGridInfoAlta.FormShow(Sender: TObject);
begin
     {$ifdef CAMBIOS_SENDA}
     Continuarbtn.Visible := not ( Global.GetGlobalString( K_GLOBAL_VALIDA_ALTA_EMP ) = VALIDAR_REPETIDOS_ALTA_IMPIDIENDO );
     {$else}
     Continuarbtn.Visible := True;
     {$endif}
end;

procedure TFGridInfoAlta.CancelarBtnClick(Sender: TObject);
begin
     Close;
end;

procedure TFGridInfoAlta.GridEmpleadosDblClick(Sender: TObject);
begin
     ModalResult :=  mrYes;
end;


function TFGridInfoAlta.GetEmpleadoSeleccionado: Integer;
begin
     Result := GridEmpleados.Columns[0].Field.AsInteger;
end;

procedure TFGridInfoAlta.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     with dmRecursos.cdsInfoEmp do
     begin
          if (strVacio(FieldByName('FOTO').AsString))then
              Foto.Visible := FALSE
          else
              Foto.Visible := TRUE;
     end;
end;

procedure TFGridInfoAlta.GridEmpleadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
      with GridEmpleados do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if zStrToBool( dmRecursos.cdsInfoEmp.FieldByName( 'CB_ACTIVO' ).AsString ) then
                       Font.Color := GridEmpleados.Font.Color
                    else
                        Font.Color := clRed;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

end.
