object FGridInfoAlta: TFGridInfoAlta
  Left = 190
  Top = 191
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Empleados Encontrados'
  ClientHeight = 462
  ClientWidth = 695
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBotones: TPanel
    Left = 0
    Top = 421
    Width = 695
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      695
      41)
    object ContinuarBtn: TBitBtn
      Left = 472
      Top = 8
      Width = 110
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Continuar &Alta'
      ModalResult = 1
      TabOrder = 1
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
        0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
        33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
    end
    object CancelarBtn: TBitBtn
      Left = 588
      Top = 8
      Width = 102
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      ModalResult = 2
      TabOrder = 0
      OnClick = CancelarBtnClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 32
    Width = 695
    Height = 389
    Align = alClient
    TabOrder = 1
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 353
      Height = 387
      Align = alLeft
      TabOrder = 0
      object GridEmpleados: TZetaDBGrid
        Left = 1
        Top = 1
        Width = 351
        Height = 385
        Align = alClient
        DataSource = DataSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = GridEmpleadosDrawColumnCell
        OnDblClick = GridEmpleadosDblClick
        Columns = <
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Caption = 'N'#250'mero'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRETTYNAME'
            Title.Caption = 'Nombre'
            Width = 238
            Visible = True
          end>
      end
    end
    object Panel3: TPanel
      Left = 355
      Top = 1
      Width = 338
      Height = 184
      BevelInner = bvLowered
      TabOrder = 1
      object Label1: TLabel
        Left = 70
        Top = 10
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label2: TLabel
        Left = 59
        Top = 67
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre(s):'
      end
      object Label3: TLabel
        Left = 77
        Top = 85
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C.:'
      end
      object Label4: TLabel
        Left = 31
        Top = 104
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = '# Seguro Social:'
      end
      object Label5: TLabel
        Left = 6
        Top = 142
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Nacimiento:'
      end
      object CB_CODIGO: TZetaDBTextBox
        Left = 113
        Top = 8
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_NOMBRES: TZetaDBTextBox
        Left = 113
        Top = 65
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_NOMBRES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_NOMBRES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_RFC: TZetaDBTextBox
        Left = 113
        Top = 83
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_RFC'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        FormatSpecifier = 'LLLL-999999-AAA;0'
        DataField = 'CB_RFC'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_SEGSOC: TZetaDBTextBox
        Left = 113
        Top = 102
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_SEGSOC'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        FormatSpecifier = '99-99-99-9999-9;0'
        DataField = 'CB_SEGSOC'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_FEC_NAC: TZetaDBTextBox
        Left = 113
        Top = 140
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_FEC_NAC'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_FEC_NAC'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 28
        Top = 48
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Apellido Materno:'
      end
      object Label7: TLabel
        Left = 30
        Top = 29
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Apellido Paterno:'
      end
      object CB_APE_MAT: TZetaDBTextBox
        Left = 113
        Top = 46
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_APE_MAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_APE_MAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_APE_PAT: TZetaDBTextBox
        Left = 113
        Top = 27
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_APE_PAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_APE_PAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_CURP: TZetaDBTextBox
        Left = 113
        Top = 121
        Width = 218
        Height = 17
        AutoSize = False
        Caption = 'CB_CURP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_CURP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 65
        Top = 123
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'C.U.R.P.:'
      end
      object CB_RECONTR: TDBCheckBox
        Left = 24
        Top = 160
        Width = 103
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Es Recontratable:'
        DataField = 'CB_RECONTR'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object FOTO: TPDBMultiImage
      Left = 385
      Top = 213
      Width = 286
      Height = 164
      ImageReadRes = lAutoMatic
      ImageWriteRes = sAutoMatic
      DataEngine = deBDE
      JPegSaveQuality = 25
      JPegSaveSmooth = 0
      ImageDither = True
      UpdateAsJPG = False
      UpdateAsBMP = False
      UpdateAsGIF = False
      UpdateAsPCX = False
      UpdateAsPNG = False
      UpdateAsTIF = True
      Color = clBtnFace
      DataField = 'FOTO'
      DataSource = DataSource
      RichTools = False
      StretchRatio = True
      TabOrder = 2
      TextLeft = 0
      TextTop = 0
      TextRotate = 0
      TifSaveCompress = sNONE
      UseTwainWindow = False
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 695
    Height = 32
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 2
    object Label8: TLabel
      Left = 12
      Top = 5
      Width = 663
      Height = 23
      Alignment = taCenter
      Caption = 'Empleados Que Corresponden Con Los Datos Capturados'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -21
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 112
    Top = 16
  end
end
