unit FKardexCambio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FKardexBase_DevEx, Db, ZetaFecha,
  ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Mask, ComCtrls, Buttons,
  ZetaNumero, ZetaKeyCombo, ZetaSmartLists, FDatosPer_DevEx,
  ZetaCommonClasses,
  cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons, ZetaKeyLookup_DevEx,
  cxListBox, ZetaSmartLists_DevEx;

type
  TKardexCambio_DevEx = class(TKardexBase_DevEx)
    CB_TABLASS: TZetaDBKeyLookup_DevEx;
    CB_RANGO_S: TZetaDBNumero;
    CB_AUTOSAL: TDBCheckBox;
    CB_ZONA_GE: TDBComboBox;
    CB_FECHA_2: TZetaDBFecha;
    CB_PER_VAR: TZetaDBNumero;
    CB_SALARIO: TZetaDBNumero;
    Label17: TLabel;
    CB_SAL_SEM: TZetaTextBox;
    Label5: TLabel;
    CB_RANGO_SLbl: TLabel;
    CB_SAL_TOT: TZetaDBTextBox;
    CB_OTRAS_P: TZetaDBTextBox;
    Label16: TLabel;
    Label15: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    CB_SAL_INT: TZetaDBTextBox;
    CB_TOT_GRA: TZetaDBTextBox;
    CB_PRE_INT: TZetaDBTextBox;
    CB_FAC_INT: TZetaDBTextBox;
    Label10: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    OPLBl: TLabel;
    Label35: TLabel;
    CB_FAC_INTlbl: TLabel;
    CB_SALARIOlbl: TLabel;
    Otras_DevEx: TcxTabSheet;
    Salarios2_DevEx: TcxTabSheet;
    MovimientoIDSE_DevEx: TcxTabSheet;
    CB_OLD_SAL: TZetaDBTextBox;
    CB_FEC_REV: TZetaDBTextBox;
    CB_OLD_INT: TZetaDBTextBox;
    CB_FEC_INT: TZetaDBTextBox;
    Label21: TLabel;
    Label19: TLabel;
    Label18: TLabel;
    Label14: TLabel;
    GroupBox2: TGroupBox;
    lblLoteIDSE: TLabel;
    lblFechaIDSE: TLabel;
    CB_FEC_IDS: TZetaDBFecha;
    CB_LOT_IDS: TDBEdit;
    LBSeleccion: TZetaSmartListBox_DevEx;
    LBDisponibles: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton4: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton3: TZetaSmartListsButton_DevEx;
    Label22: TLabel;
    ZetaSmartListsButton2: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton1: TZetaSmartListsButton_DevEx;
    Label20: TLabel;
    ZetaSmartLists: TZetaSmartLists_DevEx;
    SBMas_DevEx: TcxButton;
    btnRecalculo_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    //procedure OKClick(Sender: TObject);
    procedure CB_AUTOSALClick(Sender: TObject);
    procedure ZetaSmartListsAlModificar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZetaSmartListsAlRechazar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ZetaSmartListsAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure FormShow(Sender: TObject);
    //procedure SBMasClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    //procedure CancelarClick(Sender: TObject);
    procedure CB_RANGO_SExit(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    //procedure BtnRecalculoClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure SBMas_DevExClick(Sender: TObject);
    procedure btnRecalculo_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    DatosPer_DevEx : TDatosPer_DevEx;
    function BorraOtraPer( const sCodigo : string ) : string;
    procedure ControlesSalarios;
    procedure ShowSBMas;
    procedure LlenaListaTabulador;
    procedure SetCambiaronFijas;
    procedure SetMatsushita;
    procedure ChecaRangoSalarial;
  protected
    rRangoSal : TPesos;
    rOldSalario : TPesos;
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure LlenaOtrasPer;
    procedure LlenaKar_fija;
    function GetClasifi : String; virtual;
    function SetSalarioTabulador: Boolean;
    procedure CalcSalSemanal;
  public
  end;

var
  KardexCambio_DevEx: TKardexCambio_DevEx;

implementation

uses ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaDialogo,
     dRecursos,
     dGlobal,
     DSistema,
     dTablas,
     dCatalogos, DCliente;

{$R *.DFM}

procedure TKardexCambio_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     HelpContext:= ZetaCommonClasses.H31314_Kardex_Cambio;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     SetMatsushita;
     CB_TABLASS.LookupDataset := dmCatalogos.cdsSSocial;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TKardexCambio_DevEx.Connect;
begin
     {$ifdef ELECTROLUX}
     Label5.Caption := 'Salario Mensual:';
     {$else}
     Label5.Caption := 'Salario Semanal:';
     {$endif}
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
          cdsOtrasPer.Conectar;
          cdsSsocial.Conectar;
     end;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
          cdsKar_Fija.Refrescar;
     end;
     rRangoSal := CB_RANGO_S.Valor;           // Matsushita
     rOldSalario := DataSource.DataSet.FieldByName( 'CB_SALARIO' ).AsFloat;
     CalcSalSemanal;

     LlenaOtrasPer;
     LlenaKar_fija;
end;

procedure TKardexCambio_DevEx.EscribirCambios;
begin
     with dmRecursos.cdsEditHisKardex do
     begin
          if ( FieldByName( 'CB_TIPO' ).AsString <> K_T_CAMBIO ) or    // Registro de Reingreso no require validar
             dmRecursos.ValidaSalario( rOldSalario, FieldByName( 'CB_SALARIO' ).AsFloat ) then
             inherited;
     end;
end;

function TKardexCambio_DevEx.BorraOtraPer( const sCodigo : string ) : string;
 var i:integer;
begin
     Result := sCodigo + '=< Ya no Existe >';
     with LBDisponibles do
          for i :=0 to Items.Count - 1 do
              if TOtrasPer(Items.Objects[i]).Codigo = sCodigo then
              begin
                   Result := Items[i];
                   TOtrasPer(Items.Objects[i]).Free;
                   Items.Delete(i);
                   Exit;
              end;

end;

procedure TKardexCambio_DevEx.LlenaKar_fija;
 var oDatos : TOtrasPer;
begin
     LimpiaLista( LBSeleccion.Items );
     with dmRecursos.cdsKar_fija do
     begin
          First;
          while NOT eof do
          begin
               oDatos := TOtrasPer.Create;
               oDatos.Codigo   := FieldByName('KF_CODIGO').AsString;
               oDatos.Monto    := FieldByName('KF_MONTO').AsFloat;
               oDatos.Gravado  := FieldByName('KF_GRAVADO').AsFloat;
               oDatos.Imss     := eIMSSOtrasPer(FieldByName('KF_IMSS').AsInteger);
               LbSeleccion.Items.AddObject( BorraOtraPer(FieldByName('KF_CODIGO').AsString),
                                            oDatos);
               Next;
          end;
     end;
end;

procedure TKardexCambio_DevEx.LlenaOtrasPer;

begin
     dmRecursos.LlenaOtrasPer( LBDisponibles.Items );
end;

{procedure TKardexCambio_DevEx.OKClick(Sender: TObject);
var
   i: Byte;
begin
     with dmRecursos do
     begin
          ListaPercepFijas:= '';
          if LbSeleccion.Items.Count > 0 then
             for i:= 0 to LbSeleccion.Items.Count -1 do
                 ListaPercepFijas := ConcatString( ListaPercepFijas, TOtrasPer(LbSeleccion.Items.Objects[i]).Codigo, ',' );
     end;
     inherited;
end;}

procedure TKardexCambio_DevEx.CB_AUTOSALClick(Sender: TObject);
begin
     inherited;
     if not SetSalarioTabulador then
     begin
          ZError( self.Caption, 'Para Definir Salario X Tabulador, se debe Asignar la Clasificación' + CR_LF + 'Revise sus Datos de Contratación', 0 );
          CB_AUTOSAL.Checked:= FALSE;
     end;
     if dmCliente.Matsushita then
        ChecaRangoSalarial;

     ControlesSalarios;
end;

function TKardexCambio_DevEx.SetSalarioTabulador: Boolean;
begin
     Result := TRUE;
     if ( Modo <> dsInactive ) and ( Modo <> dsBrowse ) and CB_AUTOSAL.Checked then
        if StrLleno( GetClasifi ) then
           LLenaListaTabulador
        else
           Result := FALSE;
end;

function TKardexCambio_DevEx.GetClasifi : string;
begin
     Result := dmRecursos.cdsEditHisKardex.FieldByName( 'CB_CLASIFI' ).AsString;
end;

procedure TKardexCambio_DevEx.LlenaListaTabulador;
var
   i: integer;
   dSalario : TPesos;
begin
     LlenaOtrasPer;
     dmRecursos.LlenaListaTabulador( LbSeleccion.Items, GetClasifi, dSalario );
     if LbSeleccion.Items.Count > 0 then
        for i := 0 to LbSeleccion.Items.Count - 1 do
            BorraOtraPer(TOtrasPer(LbSeleccion.Items.Objects[i]).Codigo);
     CB_Salario.Valor := dSalario;
     SetCambiaronFijas;
     if dmCliente.Matsushita then
        CB_SALARIO.Valor := dmRecursos.CalculaRangoSalarial( CB_RANGO_S.Valor,
                            LbSeleccion.Items, dSalario );
end;

procedure TKardexCambio_DevEx.ControlesSalarios;
var
   lAutoEdit : Boolean;
begin
     lAutoEdit := Inserting or DataSource.AutoEdit;
     CB_SALARIOLbl.Enabled := NOT CB_AUTOSAL.Checked;
     CB_SALARIO.Enabled    := CB_SALARIOLbl.Enabled;
     ZetaSmartListsButton1.Visible := CB_SALARIOLbl.Enabled AND lAutoEdit;
     ZetaSmartListsButton2.Visible := CB_SALARIOLbl.Enabled AND lAutoEdit;
     ZetaSmartListsButton3.Visible := CB_SALARIOLbl.Enabled AND lAutoEdit;
     ZetaSmartListsButton4.Visible := CB_SALARIOLbl.Enabled AND lAutoEdit;
     LBDisponibles.Enabled := CB_SALARIOLbl.Enabled AND lAutoEdit;
     LBSeleccion.Enabled   := CB_SALARIOLbl.Enabled AND lAutoEdit;
     // Matsushita
     CB_RANGO_SLbl.Enabled := CB_AUTOSAL.Checked;
     CB_RANGO_S.Enabled := CB_RANGO_SLbl.Enabled;
     ShowSBMas;
end;

procedure TKardexCambio_DevEx.ZetaSmartListsAlModificar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     SetCambiaronFijas;
end;

procedure TKardexCambio_DevEx.ZetaSmartListsAlRechazar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     SetCambiaronFijas;
end;

procedure TKardexCambio_DevEx.SetCambiaronFijas;
begin
     with ClientDataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
     end;
end;

procedure TKardexCambio_DevEx.ZetaSmartListsAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     ShowSBMas;
end;

procedure TKardexCambio_DevEx.ShowSBMas;
begin
     SBMas_DevEx.Enabled := ( CB_SALARIOLbl.Enabled ) and ( LBSeleccion.Items.Count > 0 );
end;

procedure TKardexCambio_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     BtnRecalculo_DevEx.Enabled := DataSource.AutoEdit;
     ControlesSalarios;
end;

{procedure TKardexCambio_DevEx.SBMasClick(Sender: TObject);
var
   iIndex : Integer;
   oOtrasPer : TOtrasPer;
begin
     inherited;
     with LBSeleccion do
     begin
          iIndex := ItemIndex;
          if ( iIndex >= 0  ) then
          begin
               if ( Items.Objects[iIndex] <> Nil ) then
               begin
                    if DatosPer = Nil then DatosPer := TDatosPer.Create( self );
                    with DatosPer do
                    begin
                         oOtrasPer := TOtrasPer( Items.Objects[ iIndex ] );
                         ZCodigo.Caption :=  Items[ iIndex ];
                         ZMonto.Valor    := oOtrasPer.Monto;
                         ZGravado.Valor  := oOtrasPer.Gravado;
                         ZCriterio.Valor := Ord(oOtrasPer.Imss);
                         ShowModal;
                    end;
               end
               else
                   ZInformation(Caption,' La Parte Gravada no ha sido Calculada, al Guardar el Registro se Calculará',0);
          end;
     end;
end;}

procedure TKardexCambio_DevEx.FormDestroy(Sender: TObject);
var
   i : Integer;
begin
  inherited;
  for i := 0 to TStringList( LBSeleccion.Items).Count - 1 do
      if TStringList( LBSeleccion.Items).Objects[ i ] <> NIL then
         TOtrasPer( TStringList( LBSeleccion.Items).Objects[i]).Free ;
end;

{procedure TKardexCambio_DevEx.CancelarClick(Sender: TObject);
begin
     LlenaOtrasPer;
     LlenaKar_fija;
     inherited;
end;}

procedure TKardexCambio_DevEx.SetMatsushita;
begin
     with dmCliente do
     begin
          CB_RANGO_SLbl.Visible:= Matsushita;
          CB_RANGO_S.Visible:= Matsushita;
     end;
end;

procedure TKardexCambio_DevEx.CB_RANGO_SExit(Sender: TObject);
begin
     if ( CB_RANGO_S.Valor <> rRangoSal ) then      //Cambió el Rango
     begin
          if CB_AUTOSAL.Checked then
             CB_SALARIO.Valor:= dmRecursos.CalculaRangoSalarial( CB_RANGO_S.Valor,
                                LbSeleccion.Items, dmRecursos.GetSalClasifi( GetClasifi ) );
          rRangoSal := CB_RANGO_S.Valor;
     end;
end;

procedure TKardexCambio_DevEx.ChecaRangoSalarial;
begin
     if ( Modo <> dsInactive ) and ( Modo <> dsBrowse ) then
     begin
          if not CB_AUTOSAL.Checked then
             CB_RANGO_S.Valor:= 100.00
          else
          begin
               with DataSource.DataSet do
               begin
                    if ( State = dsEdit ) then
                       CB_RANGO_S.Valor := FieldByName( 'CB_RANGO_S' ).AsFloat
                    else
                       CB_RANGO_S.Valor := dmCliente.cdsEmpleado.FieldByName( 'CB_RANGO_S' ).AsFloat;
               end;
          end;
          CB_RANGO_S.OnExit(Self);
     end;
end;

procedure TKardexCambio_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field <> nil ) and ( Field.FieldName = 'CB_SALARIO' ) then
        CalcSalSemanal;
end;

procedure TKardexCambio_DevEx.CalcSalSemanal;
begin
     with DataSource.DataSet do
          CB_SAL_SEM.Caption := FormatFloat( '#,0.00', FieldByName( 'CB_SALARIO' ).AsFloat *{$ifdef ELECTROLUX}30.4{$else}7{$endif}  );
end;

{procedure TKardexCambio_DevEx.BtnRecalculoClick(Sender: TObject);
begin
     inherited;
     with DataSource.DataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
     end;
end; }

procedure TKardexCambio_DevEx.OK_DevExClick(Sender: TObject);
var
   i: Byte;
begin
     with dmRecursos do
     begin
          ListaPercepFijas:= '';
          if LbSeleccion.Items.Count > 0 then
             for i:= 0 to LbSeleccion.Items.Count -1 do
                 ListaPercepFijas := ConcatString( ListaPercepFijas, TOtrasPer(LbSeleccion.Items.Objects[i]).Codigo, ',' );
     end;
     inherited;
end;

procedure TKardexCambio_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     LlenaOtrasPer;
     LlenaKar_fija;
     inherited;
end;

procedure TKardexCambio_DevEx.SBMas_DevExClick(Sender: TObject);
var
   iIndex : Integer;
   oOtrasPer : TOtrasPer;
begin
     inherited;
     with LBSeleccion do
     begin
          iIndex := ItemIndex;
          if ( iIndex >= 0  ) then
          begin
               if ( Items.Objects[iIndex] <> Nil ) then
               begin
                    if DatosPer_DevEx = Nil then DatosPer_DevEx := TDatosPer_DevEx.Create( self ); //DevEx (by am): Se creo la nueva forma y su invoccion se cambio aqui.
                    with DatosPer_DevEx do
                    begin
                         oOtrasPer := TOtrasPer( Items.Objects[ iIndex ] );
                         ZCodigo.Caption :=  Items[ iIndex ];
                         ZMonto.Valor    := oOtrasPer.Monto;
                         ZGravado.Valor  := oOtrasPer.Gravado;
                         ZCriterio.Valor := Ord(oOtrasPer.Imss);
                         ShowModal;
                    end;
               end
               else
                   ZInformation(Caption,' La Parte Gravada no ha sido Calculada, al Guardar el Registro se Calculará',0);
          end;
     end;
end;

procedure TKardexCambio_DevEx.btnRecalculo_DevExClick(Sender: TObject);
begin
     inherited;
     with DataSource.DataSet do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
          FieldByName( 'CB_FAC_INT' ).AsFloat:= 0;
     end;
end;

procedure TKardexCambio_DevEx.SetEditarSoloActivos;
begin
     CB_TABLASS.EditarSoloActivos := TRUE;
end;

end.







