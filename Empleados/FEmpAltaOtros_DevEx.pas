unit FEmpAltaOtros_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, Buttons,
     Mask, ExtCtrls, Db,
     ZetaNumero,
     ZetaDBGrid,
     ZetaDBTextBox,
     ZetaClientDataSet, ComCtrls, ZetaFecha, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  ZetaKeyLookup_DevEx, cxButtons ;
type
  TEmpAltaOtros_DevEx = class(TForm)
    LblEmpleado: TLabel;
    ZEmpleado: TZetaNumero;
    Panel1: TPanel;
    Salir: TcxButton;
    PEmpleado: TPanel;
    Otra: TcxButton;
    OK: TcxButton;
    BtnSugiere: TcxButton;
    PBotones: TPanel;
    Label2: TLabel;
    ZNombre: TZetaTextBox;
    Label3: TLabel;
    ZIngreso: TZetaTextBox;
    BtnFoto: TcxButton;
    BtnParientes: TcxButton;
    BtnCursos: TcxButton;
    BtnPuestos: TcxButton;
    BtnPrestamos: TcxButton;
    BtnAhorros: TcxButton;
    ZPuesto: TZetaKeyLookup_DevEx;
    LblPuesto: TLabel;
    lblPlaza: TLabel;
    zPlaza: TZetaKeyLookup_DevEx;
    btnUsuarios: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure ZEmpleadoExit(Sender: TObject);
    procedure OtraClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OKClick(Sender: TObject);
    procedure BtnSugiereClick(Sender: TObject);
    procedure BtnFotoClick(Sender: TObject);
    procedure BtnParientesClick(Sender: TObject);
    procedure BtnCursosClick(Sender: TObject);
    procedure BtnPuestosClick(Sender: TObject);
    procedure BtnPrestamosClick(Sender: TObject);
    procedure BtnAhorrosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ZPuestoValidKey(Sender: TObject);
    procedure btnUsuariosClick(Sender: TObject);
  private
    { Private declarations }
    iEmpleado: Integer;
    lExisteEmpleado : Boolean;
    ListaAhorros, ListaPrestamos: TStringList;
    function CheckEmpleado: Boolean;
    function ValidaPuesto: Boolean;
    function ValidaVacantes: Boolean;
    procedure SetControles(const lModoAlta: Boolean; const lNuevoEmp: Boolean = FALSE );
    procedure SetNextEmpleado;
    procedure RevisaEmpleado;
    procedure GeneraAlta;
    procedure AgregarAhorrosPrestamos( oCdsDatos: TZetaClientDataSet; const sTipo, sFecha: String; oLista: TStringList );
    procedure AgregarUsuario( cdsUsuarios: TZetaClientDataSet );
    procedure ApagaBotonesEdit( const lPrende: Boolean );
    procedure SetEmpleadoActivoInfo;
    procedure Filtracontrolplazas( const sPuesto: String );
    procedure HabilitaControl;
  public
    { Public declarations }
  end;

var
  EmpAltaOtros_DevEx : TEmpAltaOtros_DevEx;

implementation

uses DRecursos,
     DCliente,
     DTablas,
     DCatalogos,
     DGlobal,
     ZBaseEdicion_DevEx,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaDialogo,
     FTressShell,
     ZAccesosMgr,
     FEmpAlta_DevEx, dSistema;

{$R *.DFM}

procedure TEmpAltaOtros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ListaAhorros := TStringList.Create;
     ListaPrestamos := TStringList.Create;
     Otra.Left := Ok.Left;
     ZPuesto.LookupDataSet := dmCatalogos.cdsPuestos;
     ZPlaza.LookupDataSet := dmRecursos.cdsPlazasLookup;
     HelpContext := H10151_Alta_empleados;
     ZPlaza.EditarSoloActivos := TRUE; //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
     ZPuesto.EditarSoloActivos := TRUE; //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
end;

procedure TEmpAltaOtros_DevEx.FormDestroy(Sender: TObject);
begin
     FreeAndNil( ListaAhorros );
     FreeAndNil( ListaPrestamos );
end;

procedure TEmpAltaOtros_DevEx.FormShow(Sender: TObject);
begin
     with ZPuesto do
     begin
          Filtro := dmCliente.GetFiltroLookupPuestos;
          Llave := VACIO;
     end;

     dmCatalogos.cdsPuestos.Conectar;

     zPlaza.Enabled := dmCliente.UsaPlazas;
     lblPlaza.Enabled := zPlaza.Enabled;
     Filtracontrolplazas( zPuesto.Llave );
{
     if zPlaza.Enabled then
     begin
          dmRecursos.cdsPlazas.Conectar;
          zPuesto.OnValidKey := ZPuestoValidKey;
     end
     else
         zPuesto.OnValidKey := NIL;
}
     SetNextEmpleado;
     with dmTablas do
     begin
          LlenarListaAhorros( ListaAhorros );
          LlenarListaPrestamos( ListaPrestamos );
     end;
     with dmRecursos do
     begin
          if ( ListaAhorros.Count > 0 ) then            // Si Hay Ahorros en la Alta
             cdsHisAhorros.Conectar;
          if ( ListaPrestamos.Count > 0 ) then          // Si Hay Prestamos en la Alta
             cdsHisPrestamos.Conectar;
     end;
     HabilitaControl;
     btnUsuarios.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_USUARIO, K_DERECHO_ALTA );
end;

procedure TEmpAltaOtros_DevEx.HabilitaControl;
begin
     Global.Conectar;
     if Global.GetGlobalBooleano( K_GLOBAL_NUM_EMP_AUTOMATICO ) then
     begin
          zEmpleado.ReadOnly := not ZAccesosMgr.CheckDerecho( D_EMP_REG_ALTA, K_DERECHO_CAMBIO );
          lblEmpleado.Enabled := not zEmpleado.ReadOnly;
          btnSugiere.Enabled := not zEmpleado.ReadOnly;
     end
     else
     begin
          zEmpleado.ReadOnly := False;
          lblEmpleado.Enabled := not zEmpleado.ReadOnly;
          btnSugiere.Enabled := not zEmpleado.ReadOnly;
     end;
end;


procedure TEmpAltaOtros_DevEx.Filtracontrolplazas( const sPuesto: String );
begin
     with zPlaza do
     begin
          LLave := VACIO;
          Filtro := dmRecursos.FiltroPlazaVacantes( sPuesto );
     end;
end;

procedure TEmpAltaOtros_DevEx.SetNextEmpleado;
begin
     if Global.GetGlobalBooleano( K_GLOBAL_NUM_EMP_AUTOMATICO ) then
        iEmpleado := dmRecursos.ProximoEmpleado
     else
        iEmpleado := 0;
     zEmpleado.Valor := iEmpleado;
     zPLaza.Llave := Vacio;
     zPlaza.ResetMemory;
     lExisteEmpleado := FALSE;
     SetControles( TRUE );
     ActiveControl   := ZEmpleado;
end;

procedure TEmpAltaOtros_DevEx.SetControles( const lModoAlta: Boolean; const lNuevoEmp: Boolean = FALSE );
begin
     PBotones.Visible := not lModoAlta;
     Otra.Visible     := ( not lModoAlta ) and lNuevoEmp;
     OK.Visible       := lModoAlta;
     LblPuesto.Enabled:= lModoAlta;
     ZPuesto.Enabled  := lModoAlta;
     if dmCliente.UsaPlazas then
     begin
          lblPlaza.Enabled   := lModoAlta;
          zPlaza.Enabled     := lModoAlta;
     end;
end;

procedure TEmpAltaOtros_DevEx.ApagaBotonesEdit( const lPrende: Boolean );
begin
     BtnFoto.Enabled := lPrende;
     BtnParientes.Enabled := lPrende;
     BtnCursos.Enabled := lPrende;
     BtnPuestos.Enabled := lPrende;
     BtnPrestamos.Enabled := lPrende;
     BtnAhorros.Enabled := lPrende;
end;

function TEmpAltaOtros_DevEx.CheckEmpleado: Boolean;
begin
     Result := ( iEmpleado <> ZEmpleado.ValorEntero );
end;

function TEmpAltaOtros_DevEx.ValidaPuesto: Boolean;
begin
     Result := strLleno( ZPuesto.Llave );
     if ( not Result ) then
     begin
          ZetaDialogo.ZError( self.Caption, '� Falta Especificar el Puesto del Empleado !', 0 );
          ActiveControl := ZPuesto;
     end;
end;

function TEmpAltaOtros_DevEx.ValidaVacantes: Boolean;
begin
     Result := ( not dmCliente.UsaPlazas ) or StrLleno( ZPLaza.Llave );

     if ( not Result ) then
     begin
          ZetaDialogo.ZError( self.Caption, '� Falta Especificar la Plaza del Empleado !', 0 );
          ActiveControl := ZPlaza;
     end;
end;

procedure TEmpAltaOtros_DevEx.SetEmpleadoActivoInfo;
begin
     with dmCliente.GetDatosEmpleadoActivo do
     begin
          ZNombre.Caption := Nombre;
          ZIngreso.Caption:= FechaCorta( Ingreso );
          ApagaBotonesEdit( Activo );
     end;
end;

procedure TEmpAltaOtros_DevEx.RevisaEmpleado;
begin
     iEmpleado := ZEmpleado.ValorEntero;
     with dmCliente do
     begin
          lExisteEmpleado := SetEmpleadoNumero( iEmpleado );
          if lExisteEmpleado then
          begin
               TressShell.RefrescaEmpleado;
               if GetDatosEmpleadoActivo.Activo then
                  ZWarning( 'Alta de Empleado', 'Empleado : ' + IntToStr( iEmpleado ) + ' Ya Existe ', 0, mbOK )
               else if ( ZetaDialogo.ZConfirm( 'Alta del Empleado', 'El Empleado Se Encuentra Dado De Baja. Desea Reingresarlo? ', 0, mbOk  ) ) then
                  dmRecursos.ProcesaReingreso;
               SetEmpleadoActivoInfo;
               SetControles( FALSE );
               ActiveControl := zEmpleado;
          end
          else
          begin
               SetControles( TRUE );
               ActiveControl := ZPuesto;
          end;
     end;
end;

procedure TEmpAltaOtros_DevEx.GeneraAlta;
begin
     with dmRecursos.cdsDatosEmpleado do
     begin
          Conectar;
          Append;
          FieldByName( 'CB_CODIGO' ).AsInteger:= iEmpleado;

          with dmCliente do
          begin
               if UsaPlazas then
                  SetDatosPlaza( zPlaza.Valor )
               else
                   SetDatosPuesto( zPuesto.Llave );
          end;

          ShowFormaEdicion( EmpAlta_DevEx, TEmpAlta_DevEx );
          if ( FieldByName( 'CB_CODIGO' ).AsInteger <> dmCliente.Empleado ) then // Hubo Alta
          begin
               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               ZEmpleado.Valor := iEmpleado;
               if dmCliente.SetEmpleadoNumero( iEmpleado ) then
               begin
                    TressShell.RefrescaEmpleado;
                    SetEmpleadoActivoInfo;
                    SetControles( FALSE, TRUE );
                    ActiveControl := Otra;

                    AgregarAhorrosPrestamos( dmRecursos.cdsHisAhorros, 'AH_TIPO', 'AH_FECHA', ListaAhorros );
                    AgregarAhorrosPrestamos( dmRecursos.cdsHisPrestamos, 'PR_TIPO', 'PR_FECHA', ListaPrestamos );
                    AgregarUsuario(dmSistema.cdsUsuarios);

                    dmRecursos.AgregarFotoSeleccion( FieldByName( 'CB_CANDIDA' ).AsInteger );

               end
               else
               begin
                    SetControles( TRUE );
                    ActiveControl := ZEmpleado;
               end;
          end
          else
          begin
               ActiveControl := ZEmpleado;
          end;
     end;
end;

procedure TEmpAltaOtros_DevEx.AgregarUsuario(cdsUsuarios: TZetaClientDataSet);
 var
    eAlta: eAltaUsuario;
    lAgrega: Boolean;
begin
     lAgrega := False ;
     eAlta := eAltaUsuario( Global.GetGlobalInteger(K_GLOBAL_ENROLL_AUTO_USER ) );

     if ( eAlta in [ apAgregarAutomaticamente, apAgregarPreguntando ] ) then
     begin
          lAgrega:= True;
          if ( eAlta = apAgregarPreguntando ) then
          begin
               lAgrega:= ZConfirm( self.Caption, Format( '� Desea darle Acceso a Tress al Empleado %s ?',
                                                         [ dmCliente.GetDatosEmpleadoActivo.Nombre ] ), 0, mbYes );
           end;
     end;

     if lAgrega then
     begin
          dmSistema.AgregaEmpleadoUsuario;
     end;
end;

procedure TEmpAltaOtros_DevEx.AgregarAhorrosPrestamos( oCdsDatos: TZetaClientDataSet; const sTipo, sFecha: String; oLista: TStringList );
const
     aDescripTabla: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Pr�stamo', 'Ahorro' );
var
   i : Integer;
   lAgrega, lModificarDatos : Boolean;
begin
     if oLista.Count > 0 then
     begin
          lModificarDatos:= False;
          with oCdsDatos do
          begin
               Refrescar;
               for i := 0 to oLista.Count - 1 do
               begin
                    with oLista do
                    begin
                         if TAhorroPresta( Objects[i] ).Confirmar then
                         begin
                              if ( ZConfirm( self.Caption, Format( '� Desea Inscribirlo en el %s [%s] ?',
                                                                   [ aDescripTabla[ TAhorroPresta( Objects[i] ).EsAhorro ],
                                                                     Strings[i] ] ), 0, mbYes ) ) then
                              begin
                                   lAgrega:= True;
                                   lModificarDatos:= True;
                              end
                              else
                                 lAgrega:= False;
                         end
                         else
                             lAgrega:= False;     // True; ER: 2.5 - Se agregan en la paletita de la Alta
                    end;
                    if lAgrega then
                    begin
                         Append;
                         if not TAhorroPresta( oLista.Objects[i] ).EsAhorro then
                         begin
                              FieldByName( 'PR_REFEREN' ).AsString:= 'ALTA'; //Referencia de nuevo empleado al dar de ALTA
                         end;
                         FieldByName( sTipo ).AsString:= TAhorroPresta( oLista.Objects[i] ).Tipo;
                         FieldByName( sFecha ).AsDateTime:= dmCliente.GetDatosEmpleadoActivo.Ingreso;
                         Post;
                         Enviar;
                   end;
               end;
          end;
          if lModificarDatos then
             oCdsDatos.Modificar;
     end;
end;

procedure TEmpAltaOtros_DevEx.ZEmpleadoExit(Sender: TObject);
begin
     if CheckEmpleado then
        RevisaEmpleado;
end;

procedure TEmpAltaOtros_DevEx.OKClick(Sender: TObject);
begin
     if ( not lExisteEmpleado ) and ValidaPuesto and ValidaVacantes then      // lExisteEmpleado toma valor cada que sale del control de Empleado o bien se asigna el Siguiente N�mero disponible
        GeneraAlta;
end;

procedure TEmpAltaOtros_DevEx.OtraClick(Sender: TObject);
begin
     SetNextEmpleado;
end;

procedure TEmpAltaOtros_DevEx.BtnSugiereClick(Sender: TObject);
begin
     SetNextEmpleado;
end;

procedure TEmpAltaOtros_DevEx.BtnFotoClick(Sender: TObject);
begin
     with dmRecursos.cdsEmpFoto do
     begin
          Conectar;     //Refrescar; - No es necesario Refrescar por que TressShell.RefrescaEmpleado; realiza este trabajo al RevisarEmpleado()
          Agregar;
     end;
end;

procedure TEmpAltaOtros_DevEx.BtnParientesClick(Sender: TObject);
begin
     with dmRecursos.cdsEmpParientes do
     begin
          Conectar;     //Refrescar; - No es necesario Refrescar por que TressShell.RefrescaEmpleado; realiza este trabajo al RevisarEmpleado()
          Agregar;
     end;
end;

procedure TEmpAltaOtros_DevEx.BtnCursosClick(Sender: TObject);
begin
     with dmRecursos.cdsEmpAntesCur do
     begin
          Conectar;     //Refrescar; - No es necesario Refrescar por que TressShell.RefrescaEmpleado; realiza este trabajo al RevisarEmpleado()
          Agregar;
     end;
end;

procedure TEmpAltaOtros_DevEx.BtnPuestosClick(Sender: TObject);
begin
     with dmRecursos.cdsEmpAntesPto do
     begin
          Conectar;     //Refrescar; - No es necesario Refrescar por que TressShell.RefrescaEmpleado; realiza este trabajo al RevisarEmpleado()
          Agregar;
     end;
end;

procedure TEmpAltaOtros_DevEx.BtnPrestamosClick(Sender: TObject);
begin
     with dmRecursos.cdsHisPrestamos do
     begin
          Conectar;     //Refrescar; - No es necesario Refrescar por que TressShell.RefrescaEmpleado; realiza este trabajo al RevisarEmpleado()
          Agregar;
     end;
end;

procedure TEmpAltaOtros_DevEx.BtnAhorrosClick(Sender: TObject);
begin
     with dmRecursos.cdsHisAhorros do
     begin
          Conectar;     //Refrescar; - No es necesario Refrescar por que TressShell.RefrescaEmpleado; realiza este trabajo al RevisarEmpleado()
          Agregar;
     end;
end;

procedure TEmpAltaOtros_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

procedure TEmpAltaOtros_DevEx.ZPuestoValidKey(Sender: TObject);
begin
     if dmCliente.UsaPLazas then
     begin
          Filtracontrolplazas( zPuesto.Llave );
{
          with dmRecursos do
          begin
               zPlaza.Llave := VACIO;
               //PuestoPlaza := zPuesto.Llave;

               if StrLleno( zPuesto.Llave ) then
               begin
                    zPlaza.Filtro := FiltroPlazaVacantes(zPuesto.Llave);
                    if cdsPlazas.RecordCount = 0 then
                    begin
                         ZInformation(Caption, Format( 'No existe ninguna plaza vacante para el Puesto %s', [zPuesto.Llave] ), 0 );
                         zPuesto.SetFocus;
                    end;
               end;
          end;
}
     end;
end;

procedure TEmpAltaOtros_DevEx.btnUsuariosClick(Sender: TObject);
begin
     with dmSistema.cdsUsuarios do
     begin
          Conectar;
          //Se busca que no se haya dado ya de alta un usuario con este empleado
          //Si ya existe un usuario con el #Empleado que se acaba de agregar, se edita ese usuario
          //Si no existe ningun usuario con ese #Empleado, se agrega un usuario nuevo
          if Locate('CB_CODIGO', dmCliente.GetDatosEmpleadoActivo.Numero, [] ) then
             dmSistema.ModificaUsuarios(FALSE)
          else dmSistema.AgregaEmpleadoUsuario;
     end;

end;


end.
