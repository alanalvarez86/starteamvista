unit FGridRegresarTools_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, Mask, ZetaFecha, ImgList,
  {$ifndef VER130}
  Variants,
  {$endif}
  ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  ZetaKeyLookup_DevEx, dxBarExtItems, dxBar, cxClasses, cxNavigator,
  cxDBNavigator, cxButtons;

type
  TGridRegresarTools_DevEx = class(TBaseGridEdicion_DevEx)
    PanelEmp: TPanel;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    KT_MOT_FIN: TZetaKeyLookup_DevEx;
    FechaLbl: TLabel;
    KT_FEC_FIN: TZetaFecha;
    MotivoLbl: TLabel;
    ImageGrid: TImageList;
    cxImageGrid: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CB_CODIGOValidKey(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridCellClick(Column: TColumn);
    procedure CancelarClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure ValidaDatosTools;
    procedure AsignaDatosTools;
    function  FechaBaja: TDate;
    function  EsBaja: Boolean;
  protected
    procedure HabilitaControles; override;
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
  public
    { Public declarations }
  end;

var
  GridRegresarTools_DevEx: TGridRegresarTools_DevEx;

implementation

uses dRecursos, dCliente, dCatalogos, dTablas, ZetaCommonClasses, ZetaCommonTools, FAutoClasses;

{$R *.DFM}

procedure TGridRegresarTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H11615_Regresar_Herramienta;
     with dmCliente do
          KT_FEC_FIN.Valor := FechaDefault;
end;

procedure TGridRegresarTools_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     HuboCambios:= EsBaja;
     if EsBaja then
     begin
          with dmCliente do
               CB_CODIGO.SetLlaveDescripcion( IntToStr( Empleado ), GetDatosEmpleadoActivo.Nombre );  // Evita que se Haga el DoLookup del Control
          KT_FEC_FIN.Valor := FechaBaja;
          CB_CODIGO.Enabled := FALSE;
          EmpleadoLbl.Enabled := FALSE;
          self.ActiveControl := KT_FEC_FIN;
     end
     else
         self.ActiveControl:= CB_CODIGO;

     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     KT_MOT_FIN.LookupDataset := dmTablas.cdsMotTool;

end;

procedure TGridRegresarTools_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmTablas do
     begin
          cdsMotTool.Conectar;
          cdsTallas.Conectar;
          LookupMotivoTool := FALSE;
     end;
     dmCatalogos.cdsTools.Conectar;
     with dmRecursos do
     begin
          //cdsGridEmpTools.Refrescar;
          DataSource.DataSet:= cdsGridBackTools;
     end;
end;

procedure TGridRegresarTools_DevEx.Agregar;
begin
     { No hace nada }
end;

procedure TGridRegresarTools_DevEx.Borrar;
begin
     { No hace nada }
end;

procedure TGridRegresarTools_DevEx.HabilitaControles;
begin
     if EsBaja then
     begin
          OK_DevEx.Enabled := TRUE;
          with Cancelar_DevEx do
          begin
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
          end;
     end
     else
          inherited;
end;

function TGridRegresarTools_DevEx.FechaBaja: TDate;
begin
     if ( VarType( General ) = varDate ) then
        Result := VarAsType( General, varDate )
     else
        Result := NullDateTime;
end;

function TGridRegresarTools_DevEx.EsBaja: Boolean;
begin
     Result := ( FechaBaja <> NullDateTime );
end;

procedure TGridRegresarTools_DevEx.ValidaDatosTools;
begin
     if StrVacio( CB_CODIGO.Llave ) then
     begin
          ActiveControl := CB_CODIGO;
          DataBaseError( 'No Se Ha Especificado Un Empleado' );
     end
     else if StrVacio( KT_MOT_FIN.Llave ) then
     begin
          ActiveControl := KT_MOT_FIN;
          DataBaseError( 'No Se Ha Especificado El Motivo de Devolución' );
     end;
end;

procedure TGridRegresarTools_DevEx.AsignaDatosTools;
begin
     with dmRecursos.cdsGridBackTools do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          DisableControls;
          try
             First;
             while not EOF do
             begin
                  if not zStrToBool( FieldByName( 'KT_ACTIVO' ).AsString ) then
                  begin
                       Edit;
                       FieldByName( 'KT_FEC_FIN' ).AsDateTime := KT_FEC_FIN.Valor;
                       FieldByName( 'KT_MOT_FIN' ).AsString := KT_MOT_FIN.Llave;
                       FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                  end
                  else if EsBaja then
                  begin
                       Edit;
                       FieldByName( 'KT_ACTIVO' ).AsString := K_GLOBAL_NO;
                       FieldByName( 'KT_FEC_FIN' ).AsDateTime := FechaBaja;
                       FieldByName( 'KT_MOT_FIN' ).AsString := K_T_BAJA;
                       FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                  end;
                  Next;
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TGridRegresarTools_DevEx.OKClick(Sender: TObject);
begin
     if dmCliente.ModuloAutorizado( okHerramientas ) then
        try
           ValidaDatosTools;
           AsignaDatosTools;

           inherited;

        except
           self.ModalResult:= mrNone;
           raise;
        end;
end;

procedure TGridRegresarTools_DevEx.CancelarClick(Sender: TObject);
begin
     if EsBaja then
     begin
          if Editing then
             CancelarCambios;
          try
             AsignaDatosTools;
             EscribirCambios;
             Close;
          except
             self.ModalResult:= mrNone;
             raise
          end;
     end
     else
         inherited;

end;

procedure TGridRegresarTools_DevEx.CB_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     with dmRecursos do
          if ( EmpleadoTools <> CB_CODIGO.Valor ) then
          begin
               EmpleadoTools := CB_CODIGO.Valor;
               with cdsGridBackTools do
               begin
                    Refrescar;
                    ZetaDBGrid.Enabled := not IsEmpty;
               end;
          end;
end;

procedure TGridRegresarTools_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
          DataCol: Integer; Column: TColumn; State: TGridDrawState);

   function GetPosicion: Integer;
   begin
        Result := Rect.Right - Rect.Left;
        if ( Result > cxImageGrid.Width ) then
           Result := Rect.Left + Round( ( Result - cxImageGrid.Width ) / 2 ) + 1
        else
           Result := Rect.Left;
   end;

begin
     inherited;
     if ( not DataSource.DataSet.IsEmpty ) and ( Column.FieldName = 'KT_ACTIVO' ) and
        ( strLleno( DataSource.DataSet.FieldByName( 'KT_ACTIVO' ).AsString ) ) then
        cxImageGrid.Draw( ZetaDBGrid.Canvas, GetPosicion, Rect.top + 1, BoolToInt( not zStrToBool( Column.Field.AsString ) ) )
     else
        ZetaDBGrid.DefaultDrawColumnCell( Rect, DataCol, Column, State );
end;

procedure TGridRegresarTools_DevEx.ZetaDBGridCellClick(Column: TColumn);
var
   sCampo: String;
begin
     inherited;
     with Column, DataSource.DataSet do
          if ( FieldName = 'KT_ACTIVO' ) and ( not dmTablas.LookupMotivoTool ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               sCampo := FieldByName( FieldName ).AsString;
               if sCampo = K_GLOBAL_SI then
                  FieldByName( FieldName ).AsString := K_GLOBAL_NO
               else
                  FieldByName( FieldName ).AsString := K_GLOBAL_SI;
               ZetaDBGrid.SelectedField := nil;
          end;
     dmTablas.LookupMotivoTool := FALSE;
end;

procedure TGridRegresarTools_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
    if EsBaja then
     begin
          if Editing then
             CancelarCambios;
          try
             AsignaDatosTools;
             EscribirCambios;
             Close;
          except
             self.ModalResult:= mrNone;
             raise
          end;
     end
     else
         inherited;

end;

procedure TGridRegresarTools_DevEx.OK_DevExClick(Sender: TObject);
begin
  if dmCliente.ModuloAutorizado( okHerramientas ) then
        try
           ValidaDatosTools;
           AsignaDatosTools;

           inherited;

        except
           self.ModalResult:= mrNone;
           raise;
        end;
end;

end.
