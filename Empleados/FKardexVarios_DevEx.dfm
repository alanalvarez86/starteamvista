inherited KardexVarios_DevEx: TKardexVarios_DevEx
  Left = 459
  Top = 162
  Caption = 'Cambios M'#250'ltiples'
  ClientHeight = 449
  ClientWidth = 437
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 413
    Width = 437
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 272
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 351
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 437
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 111
      inherited textoValorActivo2: TLabel
        Width = 105
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 437
    Height = 49
    Align = alTop
    TabOrder = 1
    object Label7: TLabel
      Left = 26
      Top = 27
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object Label10: TLabel
      Left = 9
      Top = 6
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object ZFecha: TZetaFecha
      Left = 64
      Top = 24
      Width = 116
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '01/may/98'
      UseEnterKey = True
      Valor = 35916.000000000000000000
    end
    object ZEmpleado: TZetaKeyLookup_DevEx
      Left = 64
      Top = 2
      Width = 335
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = ZEmpleadoValidKey
    end
  end
  object PageControl: TcxPageControl [4]
    Left = 0
    Top = 99
    Width = 437
    Height = 314
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = TabGeneral
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 312
    ClientRectLeft = 2
    ClientRectRight = 435
    ClientRectTop = 27
    object TabCambios: TcxTabSheet
      Caption = '&Cambios'
      object LPuesto: TLabel
        Left = 59
        Top = 6
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Color = clWhite
        ParentColor = False
      end
      object LClasificacion: TLabel
        Left = 33
        Top = 28
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
        Color = clWhite
        ParentColor = False
      end
      object LTurno: TLabel
        Left = 64
        Top = 50
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
        Color = clWhite
        ParentColor = False
      end
      object LRenova: TLabel
        Left = 52
        Top = 72
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contrato:'
        Color = clWhite
        ParentColor = False
      end
      object LNivel1: TLabel
        Left = 5
        Top = 116
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 1'
        Color = clWhite
        ParentColor = False
      end
      object Lnivel2: TLabel
        Left = 5
        Top = 138
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 2'
        Color = clWhite
        ParentColor = False
      end
      object Lnivel3: TLabel
        Left = 5
        Top = 160
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 3'
        Color = clWhite
        ParentColor = False
      end
      object LNivel4: TLabel
        Left = 5
        Top = 182
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel4'
        Color = clWhite
        ParentColor = False
      end
      object LNivel5: TLabel
        Left = 5
        Top = 204
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel5'
        Color = clWhite
        ParentColor = False
      end
      object LNivel6: TLabel
        Left = 5
        Top = 226
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 6'
        Color = clWhite
        ParentColor = False
      end
      object LFecVencimiento: TLabel
        Left = 1
        Top = 94
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Vencimiento:'
        Color = clWhite
        ParentColor = False
      end
      object bbMostrarCalendario: TcxButton
        Left = 403
        Top = 46
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        OnClick = bbMostrarCalendarioClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
      object CB_CONTRAT: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 68
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsContratos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_CONTRAT'
        DataSource = DataSource
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 2
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 24
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object CB_TURNO: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 46
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 112
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 134
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 156
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 178
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 200
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 222
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_FEC_COV: TZetaDBFecha
        Left = 101
        Top = 90
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 4
        Text = '14/may/08'
        Valor = 39582.000000000000000000
        DataField = 'CB_FEC_COV'
        DataSource = DataSource
      end
    end
    object TabArea: TcxTabSheet
      Caption = '&Area'
      object LNivel7: TLabel
        Left = 5
        Top = 148
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 7'
        Color = clWhite
        ParentColor = False
      end
      object LNivel8: TLabel
        Left = 5
        Top = 171
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 8'
        Color = clWhite
        ParentColor = False
      end
      object LNivel9: TLabel
        Left = 5
        Top = 194
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 9'
        Color = clWhite
        ParentColor = False
      end
      object LNivel10: TLabel
        Left = 59
        Top = 217
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10'
        Visible = False
      end
      object LNivel11: TLabel
        Left = 59
        Top = 241
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11'
        Visible = False
      end
      object LNivel12: TLabel
        Left = 59
        Top = 265
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12'
        Visible = False
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 144
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 167
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 190
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 213
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 237
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 261
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL12'
      end
    end
    object TabGeneral: TcxTabSheet
      Caption = '&General'
      object Label1: TLabel
        Left = 15
        Top = 16
        Width = 59
        Height = 13
        Caption = 'Descripci'#243'n:'
        Color = clWhite
        ParentColor = False
      end
      object Label2: TLabel
        Left = 0
        Top = 44
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
        Color = clWhite
        ParentColor = False
      end
      object ZMemo: TcxMemo
        Left = 74
        Top = 38
        Properties.ScrollBars = ssVertical
        TabOrder = 1
        Height = 180
        Width = 330
      end
      object ZDescrip: TEdit
        Left = 74
        Top = 12
        Width = 330
        Height = 21
        TabOrder = 0
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 300
    Top = 49
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
