unit FEditHisTools_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ZetaFecha,
  ZetaSmartLists, ZetaEdit, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditHisTools_DevEx = class(TBaseEdicion_DevEx)
    LblFecha: TLabel;
    KT_FEC_INI: TZetaDBFecha;
    LblHerramienta: TLabel;
    TO_CODIGO: TZetaDBKeyLookup_DevEx;
    LblReferencia: TLabel;
    LblTalla: TLabel;
    KT_TALLA: TZetaDBKeyLookup_DevEx;
    LblObservaciones: TLabel;
    KT_COMENTA: TDBEdit;
    GBDevolucion: TGroupBox;
    KT_ACTIVO: TDBCheckBox;
    LblFecDev: TLabel;
    KT_FEC_FIN: TZetaDBFecha;
    LblMotivo: TLabel;
    KT_MOT_FIN: TZetaDBKeyLookup_DevEx;
    ZetaDBEdit1: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure KT_ACTIVOClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure SetControlesDevolucion( const lEnabled: Boolean );
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditHisTools_DevEx: TEditHisTools_DevEx;

implementation

uses dRecursos, dCliente, dCatalogos, dTablas, ZAccesosTress,
     ZetaCommonLists, ZetaCommonClasses;

{$R *.DFM}

procedure TEditHisTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H10138_Historial_de_Herramientas;
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_TOOLS;
     FirstControl := KT_FEC_INI;

     TO_CODIGO.LookupDataset := dmCatalogos.cdsTools;
     KT_TALLA.LookupDataset := dmTablas.cdsTallas;
     KT_MOT_FIN.LookupDataset := dmTablas.cdsMotTool;

end;

procedure TEditHisTools_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmTablas do
     begin
          cdsMotTool.Conectar;
          cdsTallas.Conectar;
     end;
     dmCatalogos.cdsTools.Conectar;
     with dmRecursos do
     begin
          cdsHisTools.Conectar;
          DataSource.DataSet := cdsHisTools;
     end;
end;

procedure TEditHisTools_DevEx.KT_ACTIVOClick(Sender: TObject);
begin
     SetControlesDevolucion( KT_ACTIVO.Checked );
end;

procedure TEditHisTools_DevEx.SetControlesDevolucion(const lEnabled: Boolean);
begin
     LblFecDev.Enabled := lEnabled;
     KT_FEC_FIN.Enabled := lEnabled;
     LblMotivo.Enabled := lEnabled;
     KT_MOT_FIN.Enabled := lEnabled;
     if ( Modo <> dsInactive ) and ( Modo <> dsBrowse ) and KT_ACTIVO.Checked then
     begin
          KT_FEC_FIN.Valor := dmCliente.FechaDefault;
          KT_MOT_FIN.Llave := dmTablas.cdsMotTool.FieldByName( 'TB_CODIGO' ).AsString;
     end;
     if lEnabled and ( ActiveControl = KT_ACTIVO ) then
        Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
end;

procedure TEditHisTools_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
        KT_ACTIVOClick( self );
end;

end.






