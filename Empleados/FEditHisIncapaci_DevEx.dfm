inherited EditHisIncapaci_DevEx: TEditHisIncapaci_DevEx
  Left = 338
  Top = 182
  Caption = 'Incapacidades'
  ClientHeight = 377
  ClientWidth = 445
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 341
    Width = 445
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 278
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 357
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 445
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 119
      inherited textoValorActivo2: TLabel
        Width = 113
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 5
  end
  object PageControl_DevEx: TcxPageControl [3]
    Left = 0
    Top = 50
    Width = 445
    Height = 291
    Align = alClient
    TabOrder = 7
    Properties.ActivePage = TabGenerales_DevEx
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 289
    ClientRectLeft = 2
    ClientRectRight = 443
    ClientRectTop = 28
    object TabGenerales_DevEx: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 2
      object LIN_FEC_INI: TLabel
        Left = 35
        Top = 13
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha Inicio:'
      end
      object LIN_DIAS: TLabel
        Left = 70
        Top = 36
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as:'
      end
      object LIN_FEC_FIN: TLabel
        Left = 53
        Top = 57
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Regresa:'
      end
      object IN_FEC_FIN: TZetaDBTextBox
        Left = 104
        Top = 55
        Width = 85
        Height = 17
        AutoSize = False
        Caption = 'IN_FEC_FIN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'IN_FEC_FIN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label13: TLabel
        Left = 72
        Top = 79
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object Label11: TLabel
        Left = 56
        Top = 102
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label5: TLabel
        Left = 22
        Top = 125
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
        FocusControl = IN_COMENTA
      end
      object Label10: TLabel
        Left = 23
        Top = 149
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Motivo M'#233'dico:'
      end
      object Label1: TLabel
        Left = 2
        Top = 173
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin de Incapacidad:'
      end
      object IN_TASA_IPLbl: TLabel
        Left = 298
        Top = 173
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = '% Permanente:'
      end
      object Label6: TLabel
        Left = 19
        Top = 198
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Registro en RH:'
      end
      object UsuarioLbl: TLabel
        Left = 52
        Top = 220
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_DESCRIP: TZetaDBTextBox
        Left = 103
        Top = 218
        Width = 145
        Height = 17
        AutoSize = False
        Caption = 'US_DESCRIP'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 40
        Top = 240
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modificado:'
      end
      object IN_CAPTURA: TZetaDBTextBox
        Left = 103
        Top = 236
        Width = 85
        Height = 17
        AutoSize = False
        Caption = 'IN_CAPTURA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'IN_CAPTURA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object IN_FEC_INI: TZetaDBFecha
        Left = 104
        Top = 8
        Width = 115
        Height = 22
        Cursor = crArrow
        DragCursor = crDefault
        TabOrder = 0
        Text = '17/dic/97'
        Valor = 35781.000000000000000000
        DataField = 'IN_FEC_INI'
        DataSource = DataSource
      end
      object IN_DIAS: TZetaDBNumero
        Left = 104
        Top = 32
        Width = 65
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        DataField = 'IN_DIAS'
        DataSource = DataSource
      end
      object IN_TIPO: TZetaDBKeyLookup_DevEx
        Left = 104
        Top = 75
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsIncidencias
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'IN_TIPO'
        DataSource = DataSource
      end
      object IN_NUMERO: TDBEdit
        Left = 104
        Top = 98
        Width = 130
        Height = 21
        CharCase = ecUpperCase
        DataField = 'IN_NUMERO'
        DataSource = DataSource
        TabOrder = 3
      end
      object IN_COMENTA: TDBEdit
        Left = 104
        Top = 121
        Width = 275
        Height = 21
        DataField = 'IN_COMENTA'
        DataSource = DataSource
        MaxLength = 40
        TabOrder = 4
      end
      object IN_MOTIVO: TZetaDBKeyCombo
        Left = 104
        Top = 145
        Width = 185
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 5
        ListaFija = lfMotivoIncapacidad
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'IN_MOTIVO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object IN_FIN: TZetaDBKeyCombo
        Left = 104
        Top = 169
        Width = 185
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 6
        OnChange = IN_FINChange
        ListaFija = lfFinIncapacidad
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'IN_FIN'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object IN_TASA_IP: TZetaDBNumero
        Left = 376
        Top = 169
        Width = 60
        Height = 21
        Mascara = mnTasa
        TabOrder = 7
        Text = '0.0 %'
        DataField = 'IN_TASA_IP'
        DataSource = DataSource
      end
      object IN_FEC_RH: TZetaDBFecha
        Left = 103
        Top = 193
        Width = 115
        Height = 22
        Cursor = crArrow
        DragCursor = crDefault
        TabOrder = 8
        Text = '17/dic/97'
        Valor = 35781.000000000000000000
        DataField = 'IN_FEC_RH'
        DataSource = DataSource
      end
    end
    object TabSubsidio_DevEx: TcxTabSheet
      Caption = 'Pago'
      object LblPago: TLabel
        Left = 38
        Top = 10
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as a Subsidiar:'
      end
      object IN_DIASSUB: TZetaDBNumero
        Left = 124
        Top = 8
        Width = 65
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 0
        Text = '0.00'
        DataField = 'IN_DIASSUB'
        DataSource = DataSource
      end
      object GBNominaPago: TGroupBox
        Left = 6
        Top = 34
        Width = 428
        Height = 90
        Caption = 'N'#243'mina a Pagar '
        TabOrder = 1
        object Label7: TLabel
          Left = 88
          Top = 40
          Width = 24
          Height = 13
          Caption = 'Tipo:'
        end
        object Label8: TLabel
          Left = 72
          Top = 64
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
        end
        object Label9: TLabel
          Left = 90
          Top = 16
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object IN_NOMTIPO: TZetaDBKeyCombo
          Left = 117
          Top = 38
          Width = 190
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'IN_NOMTIPO'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object IN_NOMNUME: TZetaDBKeyLookup_DevEx
          Left = 117
          Top = 62
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsPeriodo
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          DataField = 'IN_NOMNUME'
          DataSource = DataSource
        end
        object IN_NOMYEAR: TZetaDBNumero
          Left = 117
          Top = 14
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'IN_NOMYEAR'
          DataSource = DataSource
        end
      end
    end
    object TabFechaIMSS_DevEx: TcxTabSheet
      Caption = 'Fechas para IMSS'
      object Label3: TLabel
        Left = 146
        Top = 21
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio:'
      end
      object Label4: TLabel
        Left = 131
        Top = 45
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Regresa:'
      end
      object IN_SUA_FIN: TZetaDBTextBox
        Left = 176
        Top = 43
        Width = 80
        Height = 17
        AutoSize = False
        Caption = 'IN_SUA_FIN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'IN_SUA_FIN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object IN_SUA_INI: TZetaDBFecha
        Left = 176
        Top = 16
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '17/dic/97'
        Valor = 35781.000000000000000000
        DataField = 'IN_SUA_INI'
        DataSource = DataSource
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 368
    Top = 204
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
