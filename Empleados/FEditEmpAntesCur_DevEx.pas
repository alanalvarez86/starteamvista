unit FEditEmpAntesCur_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, ZetaNumero, ZetaFecha, StdCtrls, Mask, DBCtrls, Db, ExtCtrls, Buttons,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditEmpAntesCur_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    AR_CURSO: TDBEdit;
    AR_FECHA: TZetaDBFecha;
    AR_LUGAR: TDBEdit;
    AR_FOLIO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect; override;
  public
  end;

var
  EditEmpAntesCur_DevEx: TEditEmpAntesCur_DevEx;

implementation

{$R *.DFM}

uses dRecursos, ZAccesosTress, ZetaCommonClasses, ZetaCommonLists;

procedure TEditEmpAntesCur_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_CURR_CURSOS;
     FirstControl := AR_FOLIO;
     HelpContext:= H10125_Expediente_Cursos_anteriores;
     TipoValorActivo1 := stEmpleado;
end;

procedure TEditEmpAntesCur_DevEx.Connect;
begin
     with dmRecursos do
     begin
          DataSource.DataSet:= cdsEmpAntesCur;
     end;
end;

end.
