inherited EditHisNominas_DevEx: TEditHisNominas_DevEx
  Caption = 'Historial de N'#243'minas'
  ClientHeight = 202
  ClientWidth = 369
  PixelsPerInch = 96
  TextHeight = 13
  object lbPeriodo: TLabel [0]
    Left = 74
    Top = 67
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Semana:'
  end
  object PE_NUMERO: TZetaDBTextBox [1]
    Left = 121
    Top = 65
    Width = 81
    Height = 17
    AutoSize = False
    Caption = 'PE_NUMERO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'PE_NUMERO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label1: TLabel [2]
    Left = 50
    Top = 86
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Neto a Pagar:'
  end
  object ZetaDBTextBox1: TZetaDBTextBox [3]
    Left = 121
    Top = 84
    Width = 81
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox1'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'NO_NETO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label13: TLabel [4]
    Left = 77
    Top = 130
    Width = 39
    Height = 13
    Caption = 'Usuario:'
  end
  object US_CODIGO: TZetaDBTextBox [5]
    Left = 121
    Top = 128
    Width = 196
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'NO_USR_PAG'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 166
    Width = 369
    inherited OK_DevEx: TcxButton
      Left = 205
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 284
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 369
    inherited Splitter: TSplitter
      Left = 161
    end
    inherited ValorActivo1: TPanel
      Width = 161
      inherited textoValorActivo1: TLabel
        Width = 155
      end
    end
    inherited ValorActivo2: TPanel
      Left = 164
      Width = 205
      inherited textoValorActivo2: TLabel
        Width = 199
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 0
  end
  object CBPagado: TCheckBox [9]
    Left = 58
    Top = 106
    Width = 58
    Height = 17
    Caption = 'Pagado:'
    TabOrder = 3
    OnClick = CBPagadoClick
  end
  object NO_FEC_PAG: TZetaDBFecha [10]
    Left = 121
    Top = 103
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 4
    Text = '22/feb/99'
    Valor = 36213.000000000000000000
    OnExit = NO_FEC_PAGExit
    DataField = 'NO_FEC_PAG'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 292
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 2097408
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 216
    Top = 24
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_UndoBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 1048752
  end
end
