inherited KardexPresta_DevEx: TKardexPresta_DevEx
  Caption = 'Cambio de Prestaciones'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl_DevEx: TcxPageControl
    inherited General_DevEx: TcxTabSheet
      Caption = 'Prestaciones'
      object Label3: TLabel
        Left = 38
        Top = 16
        Width = 109
        Height = 13
        Caption = 'Tabla de Prestaciones:'
      end
      object CB_TABLASS: TZetaDBKeyLookup_DevEx
        Left = 153
        Top = 12
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsSSocial
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TABLASS'
        DataSource = DataSource
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Top = 176
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
