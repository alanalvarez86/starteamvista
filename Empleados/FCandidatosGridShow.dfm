inherited CandidatosGridShow: TCandidatosGridShow
  Left = 349
  Top = 220
  Width = 490
  Caption = 'Lista de Candidatos'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 482
    inherited OK: TBitBtn
      Left = 269
      Width = 120
      Hint = 'Aceptar Candidato'
      Caption = '&Aceptar Candidato'
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 399
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 482
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgCancelOnExit]
    OnDrawColumnCell = ZetaDBGridDrawColumnCell
    OnDblClick = OKClick
    Columns = <
      item
        Expanded = False
        FieldName = 'SO_FOLIO'
        PickList.Strings = ()
        Title.Alignment = taCenter
        Title.Caption = 'Folio'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_APE_PAT'
        PickList.Strings = ()
        Title.Alignment = taCenter
        Title.Caption = 'Apellido Paterno'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_APE_MAT'
        PickList.Strings = ()
        Title.Alignment = taCenter
        Title.Caption = 'Apellido Materno'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_NOMBRES'
        PickList.Strings = ()
        Title.Alignment = taCenter
        Title.Caption = 'Nombre(s)'
        Width = 125
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SO_FEC_NAC'
        PickList.Strings = ()
        Title.Alignment = taCenter
        Title.Caption = 'Fecha Nac.'
        Width = 75
        Visible = True
      end>
  end
end
