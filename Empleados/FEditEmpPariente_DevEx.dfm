inherited EditEmpPariente_DevEx: TEditEmpPariente_DevEx
  Left = 364
  Top = 186
  Caption = 'Parientes'
  ClientHeight = 368
  ClientWidth = 444
  PixelsPerInch = 96
  TextHeight = 13
  object PA_RELACIONLbl: TLabel [0]
    Left = 54
    Top = 60
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Parentesco:'
  end
  object Label1: TLabel [1]
    Left = 7
    Top = 253
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de Nacimiento:'
  end
  object PA_TRABAJALbl: TLabel [2]
    Left = 58
    Top = 131
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = ' Empleado:'
  end
  object Label2: TLabel [3]
    Left = 71
    Top = 275
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label3: TLabel [4]
    Left = 81
    Top = 299
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label4: TLabel [5]
    Left = 24
    Top = 227
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre Completo:'
  end
  object Label5: TLabel [6]
    Left = 79
    Top = 84
    Width = 32
    Height = 13
    Caption = 'Orden:'
  end
  object Label6: TLabel [7]
    Left = 84
    Top = 107
    Width = 27
    Height = 13
    Caption = 'Sexo:'
  end
  object Label7: TLabel [8]
    Left = 31
    Top = 179
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Apellido Paterno:'
  end
  object Label8: TLabel [9]
    Left = 66
    Top = 155
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombres:'
  end
  object Label9: TLabel [10]
    Left = 29
    Top = 203
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Apellido Materno:'
  end
  inherited PanelBotones: TPanel
    Top = 332
    Width = 444
    TabOrder = 11
    inherited OK_DevEx: TcxButton
      Left = 280
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 359
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 444
    TabOrder = 13
    inherited ValorActivo2: TPanel
      Width = 118
      inherited textoValorActivo2: TLabel
        Width = 112
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 17
  end
  object PA_RELACIO: TZetaDBKeyCombo [14]
    Left = 120
    Top = 56
    Width = 121
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 0
    ListaFija = lfTipoPariente
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'PA_RELACIO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object PA_NOMBRE: TDBEdit [15]
    Left = 120
    Top = 224
    Width = 275
    Height = 21
    DataField = 'PA_NOMBRE'
    DataSource = DataSource
    TabOrder = 7
  end
  object PA_TEXTO: TDBEdit [16]
    Left = 120
    Top = 295
    Width = 275
    Height = 21
    DataField = 'PA_TEXTO'
    DataSource = DataSource
    TabOrder = 10
  end
  object PA_NUMERO: TZetaDBNumero [17]
    Left = 120
    Top = 271
    Width = 121
    Height = 21
    Mascara = mnPesosDiario
    TabOrder = 9
    Text = '0.00'
    DataField = 'PA_NUMERO'
    DataSource = DataSource
  end
  object PA_FOLIO: TZetaDBNumero [18]
    Left = 120
    Top = 80
    Width = 41
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
    DataField = 'PA_FOLIO'
    DataSource = DataSource
  end
  object PA_SEXO: TZetaKeyCombo [19]
    Left = 120
    Top = 103
    Width = 121
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 2
    OnChange = PA_SEXOChange
    Items.Strings = (
      'Femenino'
      'Masculino')
    ListaFija = lfSexoDesc
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object PA_FEC_NAC: TZetaDBFecha [20]
    Left = 120
    Top = 247
    Width = 121
    Height = 22
    Cursor = crArrow
    TabOrder = 8
    Text = '21/sep/99'
    Valor = 36424.000000000000000000
    DataField = 'PA_FEC_NAC'
    DataSource = DataSource
  end
  object PA_TRABAJA: TZetaDBKeyLookup_DevEx [21]
    Left = 120
    Top = 127
    Width = 300
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 70
    DataField = 'PA_TRABAJA'
    DataSource = DataSource
  end
  object PA_APE_PAT: TDBEdit [22]
    Left = 120
    Top = 176
    Width = 275
    Height = 21
    DataField = 'PA_APE_PAT'
    DataSource = DataSource
    TabOrder = 5
  end
  object PA_NOMBRES: TDBEdit [23]
    Left = 120
    Top = 152
    Width = 275
    Height = 21
    DataField = 'PA_NOMBRES'
    DataSource = DataSource
    TabOrder = 4
  end
  object PA_APE_MAT: TDBEdit [24]
    Left = 120
    Top = 200
    Width = 275
    Height = 21
    DataField = 'PA_APE_MAT'
    DataSource = DataSource
    TabOrder = 6
  end
  inherited DataSource: TDataSource
    Left = 308
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
