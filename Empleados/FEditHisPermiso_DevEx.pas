unit FEditHisPermiso_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, Mask, DBCtrls, Db, Buttons,
  ExtCtrls,ZetaDBTextBox, ZetaKeyCombo, ZetaNumero,
  ZetaFecha, ZetaSmartLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditHisPermiso_DevEx = class(TBaseEdicion_DevEx)
    LIN_FEC_INI: TLabel;
    LIN_DIAS: TLabel;
    LIN_FEC_FIN: TLabel;
    Label13: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    UsuarioLbl: TLabel;
    PM_COMENTA: TDBEdit;
    PM_TIPO: TZetaDBKeyLookup_DevEx;
    PM_FEC_INI: TZetaDBFecha;
    PM_DIAS: TZetaDBNumero;
    PM_NUMERO: TDBEdit;
    PM_CLASIFI: TZetaDBKeyCombo;
    PM_CAPTURA: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    PM_FEC_FIN: TZetaDBTextBox;
    US_DESCRIP: TZetaDBTextBox;
    PM_GLOBAL: TDBCheckBox;
    Label1: TLabel;
    PM_FEC_REG: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure PM_CLASIFIChange(Sender: TObject);
    procedure PM_TIPOValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FiltroPermiso : string;
    procedure SetFiltroPermiso;
  protected
   procedure Connect;override;
    procedure ImprimirForma;override;
  public
  end;

var
  EditHisPermiso_DevEx: TEditHisPermiso_DevEx;


implementation

uses DTablas,
     DRecursos,
     DSistema,
     FToolsRH,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZImprimeForma,
     ZetaCommonTools,
     FTressShell,
     DGlobal,
     ZGlobalTress;

{$R *.DFM}



procedure TEditHisPermiso_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FiltroPermiso := Format( K_INCIDENCIAS_FILTRO, [ Ord( eiPermiso ) ] );
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_PERM;
     FirstControl := PM_FEC_INI;
     with PM_TIPO do
     begin
          LookupDataSet := dmTablas.cdsIncidencias;
          Filtro := FiltroPermiso;
     end;
     HelpContext:= H11513_Permisos;

end;

procedure TEditHisPermiso_DevEx.SetFiltroPermiso;
begin
     PM_TIPO.Filtro := ConcatFiltros( FiltroPermiso, Format( 'TB_PERMISO = %d OR TB_PERMISO = %d',[K_MOTIVO_PERMISOS_OFFSET, PM_CLASIFI.Valor ] ) );
end;


procedure TEditHisPermiso_DevEx.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisPermiso.Conectar;
          Datasource.DataSet := cdsHisPermiso;
     end;
     SetFiltroPermiso
end;

procedure TEditHisPermiso_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enPermiso, dmRecursos.cdsHisPermiso );
end;


procedure TEditHisPermiso_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     SetFiltroPermiso;
end;

procedure TEditHisPermiso_DevEx.PM_CLASIFIChange(Sender: TObject);
begin
     inherited;
     SetFiltroPermiso;
     PM_TIPO.ResetMemory;
end;

procedure TEditHisPermiso_DevEx.PM_TIPOValidKey(Sender: TObject);
begin
     SetFiltroPermiso;
     inherited;

end;

procedure TEditHisPermiso_DevEx.FormShow(Sender: TObject);
begin
  inherited;
     if Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
     begin
          PM_FEC_FIN.Visible := false;
          PM_FEC_REG.Visible := true;
          PM_FEC_REG.Left := 104;    
          LIN_FEC_FIN.Caption := 'Fecha Regreso:';
          LIN_FEC_FIN.Left := 23;
     end
     else
     begin
          PM_FEC_FIN.Visible := true;
          PM_FEC_REG.Visible := false;
          LIN_FEC_FIN.Caption := 'Regresa:';
          LIN_FEC_FIN.Left := 56;
     end;
end;

end.
