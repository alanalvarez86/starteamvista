inherited EmpOtros_DevEx: TEmpOtros_DevEx
  Left = 324
  Top = 262
  Caption = 'Otros Datos del Empleado'
  ClientHeight = 513
  ClientWidth = 664
  OnShow = FormShow
  ExplicitWidth = 664
  ExplicitHeight = 513
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 664
    ExplicitWidth = 664
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 338
      ExplicitLeft = 326
      ExplicitWidth = 338
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 332
        ExplicitLeft = 3
      end
    end
  end
  object pcOtros: TcxPageControl [1]
    Left = 0
    Top = 19
    Width = 664
    Height = 494
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = tsAsistencia
    Properties.CustomButtons.Buttons = <>
    Properties.MultiLine = True
    OnChange = pcOtrosChange
    ClientRectBottom = 492
    ClientRectLeft = 2
    ClientRectRight = 662
    ClientRectTop = 52
    object tsInfonavit: TcxTabSheet
      Caption = 'Infonavit'
      object sbInfonavit: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object LblDescuento: TLabel
          Left = 226
          Top = 80
          Width = 15
          Height = 13
          Alignment = taRightJustify
          Caption = '     '
        end
        object Label3: TLabel
          Left = 22
          Top = 12
          Width = 98
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio de Descuento:'
        end
        object CB_INF_INI: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INF_INI'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INF_INI'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CB_INF_ANTLbl: TLabel
          Left = 3
          Top = 35
          Width = 117
          Height = 13
          Caption = 'Otorgamiento de Cr'#233'dito:'
        end
        object CB_INF_ANT: TZetaDBTextBox
          Left = 123
          Top = 33
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INF_ANT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INF_ANT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LCredito: TLabel
          Left = 59
          Top = 58
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = '# de Cr'#233'dito:'
        end
        object CB_INFCRED: TZetaDBTextBox
          Left = 123
          Top = 56
          Width = 175
          Height = 17
          AutoSize = False
          Caption = 'CB_INFCRED'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INFCRED'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label1: TLabel
          Left = 41
          Top = 77
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo Descuento:'
        end
        object ZDES_TP_PRE: TZetaTextBox
          Left = 122
          Top = 75
          Width = 100
          Height = 17
          AutoSize = False
          Caption = 'ZDES_TP_PRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object LDescuento: TLabel
          Left = 38
          Top = 96
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor Descuento:'
        end
        object CB_INFTASA: TZetaDBTextBox
          Left = 122
          Top = 94
          Width = 100
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INFTASA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INFTASA'
          DataSource = DataSource
          FormatFloat = '%14.4n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 187
          Top = 115
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa Anterior:'
          Visible = False
        end
        object CB_INF_OLD: TZetaDBTextBox
          Left = 255
          Top = 113
          Width = 39
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_INF_OLD'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_INF_OLD'
          DataSource = DataSource
          FormatFloat = '%14.4n'
          FormatCurrency = '%m'
        end
        object CB_INFDISM: TDBCheckBox
          Left = 21
          Top = 112
          Width = 114
          Height = 17
          Alignment = taLeftJustify
          BiDiMode = bdLeftToRight
          Caption = 'Disminuci'#243'n Tasa %:'
          DataField = 'CB_INFDISM'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_INFACT: TDBCheckBox
          Left = 50
          Top = 129
          Width = 85
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Cr'#233'dito Activo:'
          DataField = 'CB_INFACT'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_INFMANT: TDBCheckBox
          Left = 255
          Top = 129
          Width = 39
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Mantenimiento:'
          DataField = 'CB_INFMANT'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          Visible = False
        end
      end
    end
    object tsFonacot: TcxTabSheet
      Caption = 'Fonacot'
      ImageIndex = 3
      object sbFonacot: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object CB_FONACOT: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 125
          Height = 17
          AutoSize = False
          Caption = 'CB_FONACOT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_FONACOT'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 24
          Top = 12
          Width = 97
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero de Fonacot:'
        end
      end
    end
    object tsPPrimerEmpleo: TcxTabSheet
      Caption = 'Programa de Primer Empleo'
      ImageIndex = 4
      object sbPPrimerEmpleo: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object CB_EMPLEO: TDBCheckBox
          Left = 29
          Top = 10
          Width = 107
          Height = 17
          TabStop = False
          Alignment = taLeftJustify
          Caption = 'Aplica al programa: '
          DataField = 'CB_EMPLEO'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object tsAsistencia: TcxTabSheet
      Caption = 'Asistencia'
      ImageIndex = 1
      object sbAsistencia: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object LTipoCreden: TLabel
          Left = 37
          Top = 12
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Letra Credencial:'
          Visible = False
        end
        object CB_CREDENC: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 29
          Height = 17
          AutoSize = False
          Caption = 'CB_CREDENC'
          ShowAccelChar = False
          Visible = False
          Brush.Color = clSilver
          Border = False
          DataField = 'CB_CREDENC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 33
          Top = 31
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Checa asistencia:'
        end
        object CB_CHECA: TDBCheckBox
          Left = 122
          Top = 30
          Width = 14
          Height = 17
          DataField = 'CB_CHECA'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object tsEvaluacion: TcxTabSheet
      Caption = 'Evaluacion'
      ImageIndex = 5
      object sbEvaluacion: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object CB_EVALUA: TZetaDBTextBox
          Left = 123
          Top = 28
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_EVALUA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_EVALUA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LProxima: TLabel
          Left = 79
          Top = 49
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pr'#243'xima:'
        end
        object LFechaEva: TLabel
          Left = 86
          Top = 12
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object LEvalua: TLabel
          Left = 68
          Top = 30
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Resultado:'
        end
        object CB_NEXT_EV: TZetaDBTextBox
          Left = 123
          Top = 47
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_NEXT_EV'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_NEXT_EV'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CB_LAST_EV: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_LAST_EV'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_LAST_EV'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
    object tsCuentas: TcxTabSheet
      Caption = 'Cuentas'
      ImageIndex = 2
      object sbCuentas: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object Label8: TLabel
          Left = 40
          Top = 83
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tarjeta Gasolina:'
        end
        object CB_CTA_GAS: TZetaDBTextBox
          Left = 122
          Top = 81
          Width = 190
          Height = 17
          AutoSize = False
          Caption = 'CB_CTA_GAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_CTA_GAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label7: TLabel
          Left = 33
          Top = 104
          Width = 87
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tarjeta Despensa:'
        end
        object CB_CTA_VAL: TZetaDBTextBox
          Left = 122
          Top = 102
          Width = 190
          Height = 17
          AutoSize = False
          Caption = 'CB_CTA_VAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_CTA_VAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label6: TLabel
          Left = 65
          Top = 126
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Subcuenta:'
        end
        object CB_SUB_CTA: TZetaDBTextBox
          Left = 122
          Top = 124
          Width = 190
          Height = 17
          AutoSize = False
          Caption = 'CB_SUB_CTA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_SUB_CTA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lbNeto: TLabel
          Left = 25
          Top = 149
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Neto (Piramidaci'#243'n):'
        end
        object CB_NETO: TZetaDBTextBox
          Left = 122
          Top = 147
          Width = 84
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'CB_NETO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_NETO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object cxGroupBox1: TcxGroupBox
          Left = 16
          Top = 16
          TabOrder = 0
          Height = 60
          Width = 462
          object LBanca: TLabel
            Left = 13
            Top = 36
            Width = 90
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banca Electr'#243'nica:'
          end
          object CB_BAN_ELE: TZetaDBTextBox
            Left = 105
            Top = 32
            Width = 190
            Height = 17
            AutoSize = False
            Caption = 'CB_BAN_ELE'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_BAN_ELE'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object Label10: TLabel
            Left = 69
            Top = 12
            Width = 34
            Height = 13
            Alignment = taRightJustify
            Caption = 'Banco:'
          end
          object CB_BANCO: TZetaDBTextBox
            Left = 105
            Top = 8
            Width = 84
            Height = 17
            AutoSize = False
            Caption = 'CB_BANCO'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'CB_BANCO'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object TB_ELEM_BAN: TZetaDBTextBox
            Left = 195
            Top = 9
            Width = 255
            Height = 17
            AutoSize = False
            Caption = 'TB_ELEM_BAN'
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
            DataField = 'TB_ELEM_BAN'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
        end
      end
    end
    object tsConfidencialidad: TcxTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 9
      object sbConfidencialidad: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object CB_NIVEL0: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 84
          Height = 17
          AutoSize = False
          Caption = 'CB_NIVEL0'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_NIVEL0'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LblNivel0: TLabel
          Left = 40
          Top = 12
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confidencialidad:'
        end
      end
    end
    object tsMisDatos: TcxTabSheet
      Caption = 'Mis Datos'
      ImageIndex = 6
      object sbMisDatos: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object btnBorrarNIP: TcxButton
          Left = 10
          Top = 10
          Width = 97
          Height = 26
          Hint = 'Borrar NIP'
          Caption = 'Borrar NIP'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            20000000000000090000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1E2FFC3C8EEFF4B5BCDFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFF98A1
            E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
            CDFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
            FFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
            EEFF4B5BCDFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFC3C8EEFF4B5BCDFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
            E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
            CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
            EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFC3C8EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFFC0C5EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFAFB6E9FF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFC0C5EDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
            E9FF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
            FFFFFCFCFEFF848FDDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFAFB6E9FF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFCFC
            FEFF848FDDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF848FDDFFACB3E8FF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFACB3E8FF848F
            DDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnBorrarNIPClick
        end
      end
    end
    object tsDatosMedicos: TcxTabSheet
      Caption = 'Datos M'#233'dicos'
      ImageIndex = 8
      object sbDatosMedicos: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object lblTSangre: TLabel
          Left = 43
          Top = 12
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Sangre:'
        end
        object CB_TSANGRE: TZetaDBTextBox
          Left = 123
          Top = 10
          Width = 245
          Height = 17
          AutoSize = False
          Caption = 'CB_TSANGRE'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_TSANGRE'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblAlerPadec: TLabel
          Left = 5
          Top = 33
          Width = 116
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al'#233'rgico / Padecimiento:'
        end
        object CB_ALERGIA: TZetaDBTextBox
          Left = 123
          Top = 31
          Width = 245
          Height = 17
          AutoSize = False
          Caption = 'CB_ALERGIA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_ALERGIA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
    object tsBrigada: TcxTabSheet
      Caption = 'Brigadas de Protecci'#243'n Civil'
      ImageIndex = 7
      object sbBrigada: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object lblTipoBrigada: TLabel
          Left = 100
          Top = 29
          Width = 24
          Height = 13
          Caption = 'Tipo:'
        end
        object CB_BRG_TIP: TZetaTextBox
          Left = 128
          Top = 27
          Width = 223
          Height = 17
          AutoSize = False
          Caption = 'CB_BRG_TIP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object lblNoPisoBrigada: TLabel
          Left = 66
          Top = 85
          Width = 58
          Height = 13
          Caption = 'No. de Piso:'
        end
        object CB_BRG_NOP: TZetaDBTextBox
          Left = 128
          Top = 83
          Width = 223
          Height = 18
          AutoSize = False
          Caption = 'CB_BRG_NOP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CB_BRG_NOP'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object lblRol: TLabel
          Left = 105
          Top = 49
          Width = 19
          Height = 13
          Caption = 'Rol:'
        end
        object lblConocimiento: TLabel
          Left = 57
          Top = 67
          Width = 67
          Height = 13
          Caption = 'Conocimiento:'
        end
        object CB_BRG_ACT: TDBCheckBox
          Left = 3
          Top = 10
          Width = 138
          Height = 17
          Alignment = taLeftJustify
          BiDiMode = bdLeftToRight
          Caption = 'Pertenece a una brigada:'
          DataField = 'CB_BRG_ACT'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_CON: TDBCheckBox
          Left = 128
          Top = 65
          Width = 66
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Te'#243'rico'
          DataField = 'CB_BRG_CON'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_PRA: TDBCheckBox
          Left = 208
          Top = 65
          Width = 138
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Pr'#225'ctico'
          DataField = 'CB_BRG_PRA'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 4
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_ROL: TDBCheckBox
          Left = 128
          Top = 47
          Width = 74
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Brigadista'
          DataField = 'CB_BRG_ROL'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object CB_BRG_JEF: TDBCheckBox
          Left = 208
          Top = 47
          Width = 138
          Height = 17
          BiDiMode = bdLeftToRight
          Caption = 'Jefe de Brigada'
          DataField = 'CB_BRG_JEF'
          DataSource = DataSource
          ParentBiDiMode = False
          ReadOnly = True
          TabOrder = 2
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object tsTimbrado: TcxTabSheet
      Caption = 'Timbrado de N'#243'mina'
      ImageIndex = 10
      object sbTimbrado: TcxScrollBox
        Left = 0
        Top = 0
        Width = 660
        Height = 440
        Align = alClient
        BorderStyle = cxcbsNone
        TabOrder = 0
        object Label11: TLabel
          Left = 50
          Top = 12
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'R'#233'gimen SAT:'
        end
        object TZetaDBTextBox
          Left = 123
          Top = 12
          Width = 245
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'TL_DESCRIP'
          DataSource = dsRegimenContratTrabajador
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 500
    Top = 128
  end
  object dsRegimenContratTrabajador: TDataSource
    Left = 500
    Top = 200
  end
end
