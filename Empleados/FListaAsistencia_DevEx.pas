unit FListaAsistencia_DevEx;

interface

uses
  Windows, Messages, SysUtils,
  {$ifndef VER130}Variants,{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZetaDBTextBox, StdCtrls, Grids, DBGrids,
  ZetaDBGrid, DBCtrls, ZetaSmartLists, Buttons, ExtCtrls, ImgList, ComCtrls, ZetaMessages,
  ZBaseSesion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, cxNavigator, cxDBNavigator, cxButtons;

type
  TListaAsistencia_DevEx = class(TBaseSesion_DevEx)
    Label9: TLabel;
    cbReservas: TComboBox;
    SeleccionarTodas: TcxButton;
    CancelarTodas: TcxButton;
    ImageGrid: TImageList;
    lblinscritos: TLabel;
    SE_INSCRITO: TZetaDBTextBox;
    btnSincroniza: TcxButton;
    RefrescarBtn: TdxBarButton;
    procedure cbReservasChange(Sender: TObject);
    procedure SeleccionarTodasClick(Sender: TObject);
    procedure CancelarTodasClick(Sender: TObject);
    procedure ZetaDBGridCellClick(Column: TColumn);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure btnSincronizaClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function GetCodigoReserva : integer;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    function PuedeAgregar: Boolean; override;
    function PuedeBorrar: Boolean; override;
    function CheckDerechos( const iDerecho: Integer): Boolean;
  public
    { Public declarations }
  end;

var
  ListaAsistencia_DevEx: TListaAsistencia_DevEx;

implementation

uses DRecursos,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.dfm}


procedure TListaAsistencia_DevEx.FormCreate(Sender: TObject);
const
     K_PRIMER_EVALUACION = 3;
begin
     HelpContext:= H_ASISTENCIA;
     dxBarButton_BuscarBtn.Enabled := False;
     PrimerColumna := K_PRIMER_EVALUACION;
end;

procedure TListaAsistencia_DevEx.Connect;
begin
     inherited Connect;

     with dmRecursos do
     begin
          with cbReservas do
          begin
               LlenaReservaCombo( Items );
               if ( Items.Count <= 0 ) then
                  Exception.Create( 'No existen sesiones para este grupo' );

               ItemIndex := 0;
          end;
          dmRecursos.GetListaAsistencia( GetCodigoReserva );
          DataSource.DataSet := cdsListaAsistencia;
     end;
end;


procedure TListaAsistencia_DevEx.ZetaDBGridCellClick(Column: TColumn);
var
   sCampo: String;
begin
     inherited;
     with Column, DataSource.DataSet do
     begin
          if( ( not IsEmpty ) and ( FieldName = 'CS_ASISTIO' ) )then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               sCampo := FieldByName( FieldName ).AsString;
               if sCampo = K_GLOBAL_SI then
                  FieldByName( FieldName ).AsString := K_GLOBAL_NO
               else
                  FieldByName( FieldName ).AsString := K_GLOBAL_SI;
               Post;
               self.SeleccionaPrimerColumna;
          end;
     end;
end;

procedure TListaAsistencia_DevEx.ZetaDBGridDrawColumnCell( Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);

   function GetPosicion: Integer;
   begin
        Result := Rect.Right - Rect.Left;
        if ( Result > ImageGrid.Width ) then
           Result := Rect.Left + Round( ( Result - ImageGrid.Width ) / 2 ) + 1
           //Result := Round( ( Result - ImageGrid.Width ) / 2 ) + 1
        else
           Result := Rect.Left; 
   end;

begin
     if( ( not DataSource.DataSet.IsEmpty ) and ( Column.FieldName = 'CS_ASISTIO' ) and ( Column.Visible ) )then
        ImageGrid.Draw( ZetaDBGrid.Canvas, GetPosicion, Rect.top + 1, BoolToInt( zStrToBool( Column.Field.AsString ) ) )
     else
        ZetaDBGrid.DefaultDrawColumnCell( Rect, DataCol, Column, State );
end;


procedure TListaAsistencia_DevEx.SeleccionarTodasClick(Sender: TObject);
begin
     with dmRecursos.cdsListaAsistencia do
     begin
          if Active and ( RecordCount > 0 )then
          begin
               DisableControls;
               Try
                  First;
                  while not EOF do
                  begin
                       if( FieldByName('CS_ASISTIO').AsString = K_GLOBAL_NO )then
                       begin
                            if not( State in [ dsEdit, dsInsert ] ) then
                               Edit;
                            FieldByName('CS_ASISTIO').AsString := K_GLOBAL_SI;
                            Post;
                            HuboCambios := True;
                       end;
                       Next;
                  end;
               finally
                      First;
                      EnableControls;
               end;
          end;
     end;
end;

procedure TListaAsistencia_DevEx.CancelarTodasClick(Sender: TObject);
begin
     with dmRecursos.cdsListaAsistencia do
     begin
          if Active and ( RecordCount > 0 ) then
          begin
               DisableControls;
               Try
                  First;
                  while not EOF do
                  begin
                       if( FieldByName('CS_ASISTIO').AsString = K_GLOBAL_SI )then
                       begin
                            if not( State in [ dsEdit, dsInsert ] ) then
                               Edit;
                            FieldByName('CS_ASISTIO').AsString := K_GLOBAL_NO;
                            Post;
                            HuboCambios := True;
                       end;
                       Next;
                  end;
               finally
                      First;
                      EnableControls;
               end;
          end;
     end;
end;


function TListaAsistencia_DevEx.PuedeAgregar: Boolean;
begin
     Result := False;
end;

function TListaAsistencia_DevEx.PuedeBorrar: Boolean;
begin
     Result := False;
end;

function TListaAsistencia_DevEx.CheckDerechos( const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TListaAsistencia_DevEx.cbReservasChange(Sender: TObject);
begin
     inherited;
     dmRecursos.GetListaAsistencia( GetCodigoReserva );
end;

function TListaAsistencia_DevEx.GetCodigoReserva : integer;
begin
     Result := -1;
     with cbReservas do
     begin
          if ( ItemIndex >= 0 ) then
             Result := Integer( Items.Objects[ ItemIndex ] )
          else
              Exception.Create('No se ha seleccionado ning�n grupo');
     end;
end;

procedure TListaAsistencia_DevEx.btnSincronizaClick(Sender: TObject);
begin
     inherited;
     dmRecursos.GetListaAsistencia( GetCodigoReserva, TRUE );
end;

procedure TListaAsistencia_DevEx.KeyPress(var Key: Char);
begin
     if GridEnfocado then
     begin
          if ( ( Key <> Chr(9) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'CS_ASISTIO' ) then
               begin
                    if ( Key = #32 ) then
                    begin
                         with dmRecursos.cdsListaAsistencia do
                         begin
                              if ( not IsEmpty ) then
                              begin
                                   if not ( State in [ dsEdit, dsInsert ] ) then
                                      Edit;
                                   FieldByName( 'CS_ASISTIO' ).AsString := zBoolToStr( not zStrToBool( FieldByName( 'CS_ASISTIO' ).AsString ) );
                                   Post;
                              end;
                         end;
                    end;
                    Key := #0;
                    ZetaDBGrid.EditorMode := FALSE;
               end;
          end;
     end;
     inherited KeyPress( Key );
end;

procedure TListaAsistencia_DevEx.WMExaminar(var Message: TMessage);
var
   lColumnaAsistio : Boolean;
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          lColumnaAsistio := ( ZetaDBGrid.SelectedField.FieldName = 'CS_ASISTIO' );
          ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) and ( not lColumnaAsistio ) then
             SeleccionaSiguienteRenglon;
     end;
end;

end.


