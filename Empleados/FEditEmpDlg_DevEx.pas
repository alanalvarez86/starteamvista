unit FEditEmpDlg_DevEx;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  cxContainer, cxEdit, cxGroupBox, cxRadioGroup;

type
  TEditEmpDlg_DevEx = class(TZetaDlgModal_DevEx)
    Operacion: TcxRadioGroup;
    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditEmpDlg_DevEx: TEditEmpDlg_DevEx;

implementation

uses ZetaCommonClasses;

{$R *.DFM}

procedure TEditEmpDlg_DevEx.OK_DevExClick(Sender: TObject);
begin
    Cancelar_DevEx.SetFocus;
end;

procedure TEditEmpDlg_DevEx.FormShow(Sender: TObject);
begin
     ActiveControl := Operacion;
end;

procedure TEditEmpDlg_DevEx.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TEditEmpDlg_DevEx.FormCreate(Sender: TObject);
begin
     Operacion.ItemIndex := 0;
     HelpContext:= H11131_Tipo_modif_area_percep;
end;

end.
