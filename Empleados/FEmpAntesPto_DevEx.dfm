inherited EmpAntesPto_DevEx: TEmpAntesPto_DevEx
  Left = 770
  Top = 160
  Caption = 'Puestos Anteriores'
  ClientWidth = 525
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 525
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 199
      inherited textoValorActivo2: TLabel
        Width = 193
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 525
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object AP_FOLIO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'AP_FOLIO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 25
        Width = 25
      end
      object AP_PUESTO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'AP_PUESTO'
        MinWidth = 100
        Width = 100
      end
      object AP_EMPRESA: TcxGridDBColumn
        Caption = 'Empresa'
        DataBinding.FieldName = 'AP_EMPRESA'
        MinWidth = 100
        Width = 100
      end
      object AP_FEC_INI: TcxGridDBColumn
        Caption = 'Del'
        DataBinding.FieldName = 'AP_FEC_INI'
        MinWidth = 100
        Width = 100
      end
      object AP_FEC_FIN: TcxGridDBColumn
        Caption = 'Al'
        DataBinding.FieldName = 'AP_FEC_FIN'
        MinWidth = 100
        Width = 100
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end