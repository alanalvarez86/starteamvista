inherited KardexTipoNomina_DevEx: TKardexTipoNomina_DevEx
  Left = 402
  Top = 245
  Caption = 'Cambio de Tipo de N'#243'mina'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl_DevEx: TcxPageControl
    inherited General_DevEx: TcxTabSheet
      Caption = 'Tipo N'#243'mina'
      object HorarioLbl: TLabel
        Left = 97
        Top = 20
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de N'#243'mina:'
      end
      object CB_NOMINA: TZetaDBKeyCombo
        Left = 182
        Top = 16
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'CB_NOMINA'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 48
    Top = 200
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 11010200
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
