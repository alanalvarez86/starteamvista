unit FEmpIdentifica_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZetaDBTextBox, StdCtrls, Db, ExtCtrls,
  DBCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxButtons, imageenview, ieview;

type
  TEmpIdentifica_DevEx = class(TBaseConsulta)
    LCB_CODIGO: TLabel;
    LCB_APE_PAT: TLabel;
    LCB_APE_MAT: TLabel;
    LCB_NOMBRES: TLabel;
    LCB_FEC_NAC: TLabel;
    LRFC: TLabel;
    LCB_SEGSOC: TLabel;
    LCB_CURP: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    CB_APE_PAT: TZetaDBTextBox;
    CB_APE_MAT: TZetaDBTextBox;
    CB_NOMBRES: TZetaDBTextBox;
    CB_CURP: TZetaDBTextBox;
    CB_FEC_NAC: TZetaDBTextBox;
    LEdad: TLabel;
    lblSexo: TLabel;
    dsImagen: TDataSource;
    ZEdad: TZetaTextBox;
    CB_RFC: TZetaDBTextBox;
    CB_SEG_SOC: TZetaDBTextBox;
    FotoSwitch: TcxButton;
    ZSexo: TZetaTextBox;
    LCB_CLINICA: TLabel;
    CB_CLINICA: TZetaDBTextBox;
    LCANDIDA: TLabel;
    CB_CANDIDA: TZetaDBTextBox;
    lblEntidad: TLabel;
    CB_ENT_NAC: TZetaDBTextBox;
    GBBaja: TGroupBox;
    LBaja: TLabel;
    CB_FEC_BAJ: TZetaDBTextBox;
    LBajaIMSS: TLabel;
    CB_FEC_BSS: TZetaDBTextBox;
    Label3: TLabel;
    Label4: TLabel;
    ZNomina: TZetaTextBox;
    ZMotivoBaja: TZetaTextBox;
    FOTO: TImageEnView;
    procedure FotoSwitchClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure ShowFoto;
    procedure SetControlSeleccion( const lEnabled: Boolean );
    procedure CambiaLeyendas;
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpIdentifica_DevEx: TEmpIdentifica_DevEx;

implementation

uses dRecursos, dTablas, dGlobal, ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses,
     ZBaseEdicion_DevEx, ZImprimeForma, ZetaTipoEntidad, ZGlobalTress, FEditEmpIdentific_DevEx,
  DCliente, FToolsImageEn;

{$R *.DFM}

procedure TEmpIdentifica_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10111_Datos_identificacion_empleado;
end;

procedure TEmpIdentifica_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetControlSeleccion( strLleno( Global.GetGlobalString( K_GLOBAL_EMPRESA_SELECCION ) ) );
     CambiaLeyendas;
end;

procedure TEmpIdentifica_DevEx.Connect;
begin
     dmTablas.cdsMotivoBaja.Conectar;
     with dmRecursos do
     begin
          if ( FOTO.Visible ) then
             cdsIdentFoto.Conectar;

          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
          // Foto del Empleado
          //ShowFoto;
          dsImagen.DataSet:= cdsIdentFoto;
     end;
end;

procedure TEmpIdentifica_DevEx.Refresh;
begin
     dmRecursos.cdsDatosEmpleado.Refrescar;
     // Foto del Empleado
     if ( FOTO.Visible ) then
        ShowFoto;
end;

procedure TEmpIdentifica_DevEx.FotoSwitchClick(Sender: TObject);
begin
     inherited;
     FOTO.Visible := FotoSwitch.Down;
     if ( FOTO.Visible ) then
        ShowFoto;
end;

procedure TEmpIdentifica_DevEx.ShowFoto;
begin
     {CV: esto es para REFRESCAR, la imagen,
     a veces cuando se navega,
     como que se 'traba' la foto, con esto se forza
     a que el componente refresque la FOTO}
     dmRecursos.cdsIdentFoto.Refrescar;

     FToolsImageEn.AsignaBlobAImagen( FOTO, dmRecursos.cdsIdentFoto, 'FOTOGRAFIA' )
end;

procedure TEmpIdentifica_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
    inherited;
    with DataSource.DataSet do
    begin
         ZEdad.Caption := Tiempo( FieldbyName( 'CB_FEC_NAC' ).AsFloat, Date, etMeses ) ;
         if ( FieldByName( 'CB_SEXO' ).AsString = 'F' ) then
            ZSexo.Caption := ObtieneElemento( lfSexoDesc,  Ord( esFemenino ))
         else
            ZSexo.Caption := ObtieneElemento( lfSexoDesc,  Ord( esMasculino ));
         {AL: 2013-01-15 SOP-4593
         if zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) then
            GBBaja.Visible := FALSE
         else
         begin
              ZMotivoBaja.Caption:= dmTablas.cdsMotivoBaja.GetDescripcion( FieldByName( 'CB_MOT_BAJ' ).AsString );
              ZNomina.Caption := ShowNomina( FieldByName( 'CB_NOMYEAR' ).AsInteger,
                                             FieldByName( 'CB_NOMTIPO' ).AsInteger,
                                             FieldByName( 'CB_NOMNUME' ).AsInteger );
              GBBaja.Visible := FALSE;
         end;
         }

         { Bug #6122 - Problema en consulta de datos del empleado: ya no muestra los datos de la baja en la vista nueva }
         GBBaja.Visible := ( not dmCliente.GetDatosEmpleadoActivo.Activo );

         if Global.GetGlobalBooleano( K_GLOBAL_CB_ACTIVO_AL_DIA ) then
         begin
              with dmCliente.GetDatosEmpleadoActivo do
              begin
                   GBBaja.Visible := ( ( not Activo ) and ( Baja < dmCliente.FechaDefault ) ) or
                                     ( Activo and ( Baja <> NullDateTime ) and ( Ingreso > dmCliente.FechaDefault ) );
              end;
         end;

         if GBBaja.Visible then
         begin
              ZMotivoBaja.Caption:= dmTablas.cdsMotivoBaja.GetDescripcion( FieldByName( 'CB_MOT_BAJ' ).AsString );
              ZNomina.Caption := ShowNomina( FieldByName( 'CB_NOMYEAR' ).AsInteger,
                                             FieldByName( 'CB_NOMTIPO' ).AsInteger,
                                             FieldByName( 'CB_NOMNUME' ).AsInteger );
         end;
    end;

     inherited;
     if ( Field = nil ) and (FOTO.Visible) then
        FToolsImageEn.AsignaBlobAImagen( FOTO, dmRecursos.cdsIdentFoto, 'FOTOGRAFIA' );
end;

function TEmpIdentifica_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpIdentifica_DevEx.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandar� llamar la Alta
end;

function TEmpIdentifica_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpIdentifica_DevEx.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;     // Se mandar� llamar la Baja
end;

procedure TEmpIdentifica_DevEx.Modificar;
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpIdentific_DevEx, TEditEmpIdentific_DevEx );
end;

procedure TEmpIdentifica_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEmpIdentifica_DevEx.SetControlSeleccion( const lEnabled: Boolean );
{
const
     K_POS_STEP = 20;
var
   i : Integer;
}
begin
     CB_CANDIDA.Visible := lEnabled;
     LCANDIDA.Visible := lEnabled;
{
     // Si se desea poner el control antes de apellido paterno,
     // pero solo se puede llamar en el oncreate, no en el onshow
     if lEnabled then
     begin
          for i := 0 to ( self.ControlCount - 1 ) do     // Recorre todos los controles de la forma
          begin
               with self.Controls[i] do
               begin
                    if ( ClassType = TLabel ) or ( ClassType = TZetaDBTextBox ) then  // Si es TLabel o TZetaDBTextBox se mover� hacia abajo para que se muestre el # de Solicitante
                       Top := Top + K_POS_STEP;
          end;
          with GBBaja do
          begin
               Top := Top + K_POS_STEP;
          end;
     end;
}
end;

procedure TEmpIdentifica_DevEx.CambiaLeyendas;
begin
     Global.Conectar;
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          lblSexo.Caption := 'G�nero:';
          lRFC.Caption := '# de Ced. de Ident.:';
          LCB_SEGSOC.Caption := '# de Carnet de IHSS:';
          LCB_CURP.Visible := FALSE;
          CB_CURP.Visible := LCB_CURP.Visible;
     end
     else
     begin
          lblSexo.Caption := 'Sexo:';
          lRFC.Caption := 'R.F.C.:';
          LCB_SEGSOC.Caption := '# Seguro Social:';
          LCB_CURP.Visible := TRUE;
          CB_CURP.Visible := LCB_CURP.Visible;
     end;
end;


end.
