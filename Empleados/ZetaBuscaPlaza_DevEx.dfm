inherited BuscaPlazas_DevEx: TBuscaPlazas_DevEx
  Caption = 'B'#250'squeda de plazas'
  ClientWidth = 508
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 508
    inherited Cancelar_DevEx: TcxButton
      Left = 430
    end
    inherited OK_DevEx: TcxButton
      Left = 350
    end
  end
  inherited PanelSuperior: TPanel
    Width = 508
    inherited MostrarActivos: TCheckBox
      Left = 324
    end
  end
  inherited DBGrid_DevEx: TZetaCXGrid
    Width = 508
    inherited DBGrid_DevExDBTableView: TcxGridDBTableView
      inherited DBGrid_DevExDBTableViewColumn2: TcxGridDBColumn
        MinWidth = 300
      end
      object PU_CODIGO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'PU_CODIGO'
      end
      object PL_ORDEN: TcxGridDBColumn
        Caption = '# Plaza'
        DataBinding.FieldName = 'PL_ORDEN'
      end
    end
  end
  inherited cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
  end
end
