inherited EditHisEvaluaComp_DevEx: TEditHisEvaluaComp_DevEx
  Left = 530
  Top = 349
  Caption = 'Evaluaci'#243'n Competencia'
  ClientHeight = 228
  ClientWidth = 409
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel [0]
    Left = 35
    Top = 68
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Competencia:'
  end
  object Label5: TLabel [1]
    Left = 25
    Top = 116
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
    FocusControl = EC_COMENT
  end
  object UsuarioLbl: TLabel [2]
    Left = 56
    Top = 144
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
  end
  object EC_FECHA: TZetaDBTextBox [3]
    Left = 104
    Top = 164
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'EC_FECHA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'EC_FECHA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [4]
    Left = 44
    Top = 166
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modificado:'
  end
  object US_DESCRIP: TZetaDBTextBox [5]
    Left = 104
    Top = 142
    Width = 145
    Height = 17
    AutoSize = False
    Caption = 'US_DESCRIP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label1: TLabel [6]
    Left = 73
    Top = 92
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel:'
  end
  inherited PanelBotones: TPanel
    Top = 192
    Width = 409
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 244
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 323
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 409
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 83
      inherited textoValorActivo2: TLabel
        Width = 77
      end
    end
  end
  object EC_COMENT: TDBEdit [10]
    Left = 104
    Top = 112
    Width = 275
    Height = 21
    DataField = 'EC_COMENT'
    DataSource = DataSource
    MaxLength = 40
    TabOrder = 2
  end
  object CC_CODIGO: TZetaDBKeyLookup_DevEx [11]
    Left = 104
    Top = 64
    Width = 300
    Height = 21
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    OnValidKey = CC_CODIGOValidKey
    DataField = 'CC_CODIGO'
    DataSource = DataSource
  end
  object NC_NIVEL: TZetaDBKeyLookup_DevEx [12]
    Left = 104
    Top = 88
    Width = 300
    Height = 21
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    DataField = 'NC_NIVEL'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 324
    Top = 52
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
