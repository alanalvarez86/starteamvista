inherited HisCertificacionesProg_DevEx: THisCertificacionesProg_DevEx
  Left = 208
  Top = 284
  Caption = 'Historial de Certificaciones Programadas'
  ClientWidth = 736
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 736
    inherited Slider: TSplitter
      Left = 348
    end
    inherited ValorActivo1: TPanel
      Width = 332
      inherited textoValorActivo1: TLabel
        Width = 326
      end
    end
    inherited ValorActivo2: TPanel
      Left = 351
      Width = 385
      inherited textoValorActivo2: TLabel
        Width = 379
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 736
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object KI_FEC_PRO: TcxGridDBColumn
        Caption = 'Programado'
        DataBinding.FieldName = 'KI_FEC_PRO'
        MinWidth = 75
        Width = 75
      end
      object CI_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CI_CODIGO'
        MinWidth = 60
        Width = 60
      end
      object NOMBRE: TcxGridDBColumn
        Caption = 'Certificaci'#243'n'
        DataBinding.FieldName = 'NOMBRE'
        MinWidth = 193
        Width = 193
      end
      object KI_FEC_TOM: TcxGridDBColumn
        Caption = 'Tomado'
        DataBinding.FieldName = 'KI_FEC_TOM'
        HeaderImageIndex = 75
        Width = 75
      end
      object KI_CALIF_1: TcxGridDBColumn
        Caption = 'Calificaci'#243'n #1'
        DataBinding.FieldName = 'KI_CALIF_1'
        MinWidth = 90
        Width = 90
      end
      object KI_CALIF_2: TcxGridDBColumn
        Caption = 'Calificaci'#243'n #2'
        DataBinding.FieldName = 'KI_CALIF_2'
        MinWidth = 90
        Width = 90
      end
      object KI_CALIF_3: TcxGridDBColumn
        Caption = 'Calificaci'#243'n #3'
        DataBinding.FieldName = 'KI_CALIF_3'
        MinWidth = 90
        Width = 90
      end
      object PC_OPCIONA: TcxGridDBColumn
        Caption = 'Opcional'
        DataBinding.FieldName = 'PC_OPCIONA'
        MinWidth = 60
        Width = 60
      end
    end
  end
  inherited DataSource: TDataSource
    Top = 208
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
