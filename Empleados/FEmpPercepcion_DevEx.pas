unit FEmpPercepcion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Db,
  ZetaDBTextBox, ExtCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TEmpPercepcion_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    LTablaIMSS: TLabel;
    CB_TABLASS: TZetaDBTextBox;
    LZonaGeo: TLabel;
    CB_ZONA_GE: TZetaDBTextBox;
    CB_FAC_INTlbl: TLabel;
    CB_FAC_INT: TZetaDBTextBox;
    Label2: TLabel;
    CB_PRE_INT: TZetaDBTextBox;
    CB_PER_VAR: TZetaDBTextBox;
    Label3: TLabel;
    Label4: TLabel;
    CB_TOT_GRA: TZetaDBTextBox;
    CB_AUTOSAL: TDBCheckBox;
    dsFijas: TDataSource;
    Bevel1: TBevel;
    LSalCotiza: TLabel;
    CB_SAL_INT: TZetaDBTextBox;
    Label5: TLabel;
    CB_FEC_INT: TZetaDBTextBox;
    Bevel2: TBevel;
    CB_SALARIOlbl: TLabel;
    CB_SALARIO: TZetaDBTextBox;
    Label1: TLabel;
    CB_FEC_REV: TZetaDBTextBox;
    Label6: TLabel;
    CB_RANGO_SLbl: TLabel;
    CB_RANGO_S: TZetaDBTextBox;
    LblSalSem: TLabel;
    CB_SAL_SEM: TZetaTextBox;
    KF_FOLIO: TcxGridDBColumn;
    KF_CODIGO: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    KF_MONTO: TcxGridDBColumn;
    KF_GRAVADO: TcxGridDBColumn;
    CB_CLASIFIlbl: TLabel;
    CB_CLASIFI: TZetaDBTextBox;
    ZCLASIFICACION: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    procedure SetMatsushita;
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpPercepcion_DevEx: TEmpPercepcion_DevEx;

implementation

uses dRecursos, dCliente, {$ifdef ELECTROLUX} dCatalogos, {$endif}ZetaTipoEntidad,
     ZetaCommonLists, ZetaCommonClasses, FEditEmpDlg_DevEx, ZImprimeForma,ZetaDialogo;

{$R *.DFM}

procedure TEmpPercepcion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10114_Datos_percepciones_empleado;
     SetMatsushita;
end;

procedure TEmpPercepcion_DevEx.Connect;
begin
     {$ifdef ELECTROLUX}
     LblSalSem.Caption := 'Salario Mensual:';
     {$else}
     LblSalSem.Caption := 'Salario Semanal:';
     {$endif}

     with dmRecursos do
     begin
          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
          cdsEmpPercepcion.Conectar;
          dsFijas.DataSet:= cdsEmpPercepcion;
     end;
end;

procedure TEmpPercepcion_DevEx.Refresh;
begin
     with dmRecursos do
     begin
          cdsDatosEmpleado.Refrescar;
          cdsEmpPercepcion.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TEmpPercepcion_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpPercepcion_DevEx.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandará llamar la Alta
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TEmpPercepcion_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpPercepcion_DevEx.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;     // Se mandará llamar la Baja
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEmpPercepcion_DevEx.Modificar;
begin
     if ( EditEmpDlg_DevEx = nil ) then
        EditEmpDlg_DevEx := TEditEmpDlg_DevEx.Create( Application );
     with EditEmpDlg_DevEx, dmRecursos do
     begin
          Caption:= 'Modificación de Salario y Percepciones';
          ShowModal;
          if ModalResult = mrOK then
          begin
               if Operacion.ItemIndex = 1 then  //  Movimiento Nuevo
                  AgregaKardex( K_T_CAMBIO )
               else
               begin
                    cdsHisKardex.Refrescar;
                    if KardexBuscaUltimo( K_T_CAMBIO ) then
                    begin
                         TabKardex := K_TAB_SALARIOS;
                         cdsHisKardex.Modificar;
                    end
                    else
                       ZError('','No hay Registro de Alta',0);
               end;
          end;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TEmpPercepcion_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEmpPercepcion_DevEx.SetMatsushita;
begin
     with dmCliente do
     begin
          CB_RANGO_SLbl.Visible:= Matsushita;
          CB_RANGO_S.Visible:= Matsushita;
     end;
     if CB_RANGO_S.Visible then     // Reacomodar Componentes
     begin
          Panel1.Height  := 168;
          Bevel1.Top     := 136;
          LSalCotiza.Top := 144;
          CB_SAL_INT.Top := 142;
          Label5.Top     := 144;
          CB_FEC_INT.Top := 142;
          LblSalSem.Top  := 117;
          LblSalSem.Left :=  29;
          CB_SAL_SEM.Top := 115;
          CB_SAL_SEM.Left:= 112;
     end;
end;

procedure TEmpPercepcion_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with DataSource.DataSet do
     begin
          CB_SAL_SEM.Caption := {$ifdef ELECTROLUX}FormatFloat( '#,0.00', FieldByName( 'CB_SALARIO' ).AsFloat * 30.4 ){$else}FormatFloat( '#,0.00', FieldByName( 'CB_SALARIO' ).AsFloat * 7 ){$endif};
          {$ifdef ELECTROLUX}
          ZCLASIFICACION.Caption:= dmCatalogos.cdsClasifi.GetDescripcion( FieldByName( 'CB_CLASIFI' ).AsString );
          {$endif}
    end;
end;

procedure TEmpPercepcion_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     {$ifdef ELECTROLUX}
     Panel1.Height := 166;
     {$endif}
end;

end.
