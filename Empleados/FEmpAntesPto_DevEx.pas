unit FEmpAntesPto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls,ZbaseGridLectura_DevEx,
  StdCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, 
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TEmpAntesPto_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
  end;

var
  EmpAntesPto_DevEx: TEmpAntesPto_DevEx;

implementation

uses dRecursos, ZetaCommonClasses, ZetaCommonLists;

{$R *.DFM}

procedure TEmpAntesPto_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsEmpAntesPto.Conectar;
          DataSource.DataSet:= cdsEmpAntesPto;
     end;
end;

procedure TEmpAntesPto_DevEx.Refresh;
begin
     dmRecursos.cdsEmpAntesPto.Refrescar;
end;

procedure TEmpAntesPto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10126_Expediente_Puestos_anteriores;
end;

procedure TEmpAntesPto_DevEx.Agregar;
begin
     dmRecursos.cdsEmpAntesPto.Agregar;
end;

procedure TEmpAntesPto_DevEx.Borrar;
begin
     dmRecursos.cdsEmpAntesPto.Borrar;
end;

procedure TEmpAntesPto_DevEx.Modificar;
begin
     dmRecursos.cdsEmpAntesPto.Modificar;
end;

procedure TEmpAntesPto_DevEx.FormShow(Sender: TObject);
begin
  //CreaColumaSumatoria('AP_FOLIO',SkCount);
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;

end;

end.
