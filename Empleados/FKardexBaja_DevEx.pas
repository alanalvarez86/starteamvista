unit FKardexBaja_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ZetaKeyCombo, Db,
  ZetaFecha, ExtCtrls, DBCtrls, ZetaDBTextBox, Mask,
  ComCtrls, Buttons, ZetaNumero, FKardexBase_DevEx, ZetaSmartLists,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxControls, cxContainer, cxEdit, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxTextEdit,
  cxMemo, cxDBEdit, cxPC, cxButtons, ZetaKeyLookup_DevEx;

type
  TKardexBaja_DevEx = class(TKardexBase_DevEx)
    CB_RECONTR: TDBCheckBox;
    CB_MOT_BAJ: TZetaDBKeyLookup_DevEx;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CB_NOMTIPO: TZetaDBKeyCombo;
    CB_NOMYEAR: TZetaDBNumero;
    CB_NOMNUME: TZetaDBKeyLookup_DevEx;
    CB_FECHA_2: TZetaDBFecha;
    Label4: TLabel;
    Label3: TLabel;
    MovimientoIDSE_DevEx: TcxTabSheet;
    GroupBox2: TGroupBox;
    lblLoteIDSE: TLabel;
    lblFechaIDSE: TLabel;
    CB_FEC_IDS: TZetaDBFecha;
    CB_LOT_IDS: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure SetFiltroPeriodo;
    procedure ActivaControlesLiquidacion( const lEnabled: Boolean);

  protected
   procedure Connect;override;
  public
  end;

var
  KardexBaja_DevEx: TKardexBaja_DevEx;

implementation

uses ZAccesosTress,ZetaCommonClasses,ZetaCommonLists, ZetaCommonTools,
     dTablas, dSistema, dRecursos, dCatalogos, DCliente,ZToolsPE,DGlobal;

{$R *.DFM}

procedure TKardexBaja_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H31312_Kardex_Baja;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     CB_MOT_BAJ.LookupDataset := dmTablas.cdsMotivoBaja;
     CB_NOMNUME.LookupDataset := dmCatalogos.cdsPeriodo;
end;

procedure TKardexBaja_DevEx.Connect;
begin
     dmTablas.cdsMotivoBaja.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     CB_NOMNUME.SetLlaveDescripcion( VACIO, VACIO );
     dmCatalogos.cdsPeriodo.Conectar;
     with dmRecursos, dmCliente do
     begin
          with cdsEditHisKardex do
          begin
               Conectar;
{               if ( ( FieldByName( 'CB_NOMYEAR' ).AsInteger <> YearDefault ) or
                    ( eTipoPeriodo( FieldByName( 'CB_NOMTIPO' ).AsInteger ) <> PeriodoTipo ) ) then }
                  SetFiltroPeriodo;         //dmCatalogos.cdsPeriodo.Conectar; .. SetFiltro se encarga de Conectar el DataSet con el a�o y Tipo apropiado seg�n el registro de Kardex que se edita
          end;
          DataSource.DataSet := cdsEditHisKardex;
     end;
end;

procedure TKardexBaja_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Field ) then
     begin
          with Field do
          begin
               if ( FieldName = 'CB_NOMYEAR' ) or ( FieldName = 'CB_NOMTIPO' ) then
                  SetFiltroPeriodo;
          end;
     end;
end;

procedure TKardexBaja_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     if DataSource.State = dsBrowse then
        SetFiltroPeriodo;
end;

procedure TKardexBaja_DevEx.SetFiltroPeriodo;
begin
     with dmRecursos.cdsEditHisKardex, dmCatalogos do
     begin
          if ( not cdsPeriodo.IsEmpty ) then
          begin
               if ( ( FieldByName( 'CB_NOMYEAR' ).AsInteger <> cdsPeriodo.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( FieldByName( 'CB_NOMTIPO' ).AsInteger <> cdsPeriodo.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
               begin
                  GetDatosPeriodo( FieldByName( 'CB_NOMYEAR' ).AsInteger, eTipoPeriodo( FieldByName( 'CB_NOMTIPO' ).AsInteger ) );
               end;
          end
          else
          begin
              GetDatosPeriodo( FieldByName( 'CB_NOMYEAR' ).AsInteger, eTipoPeriodo( FieldByName( 'CB_NOMTIPO' ).AsInteger ) );
          end;
     end;
end;

procedure TKardexBaja_DevEx.FormShow(Sender: TObject);
begin
    inherited;
    CB_NOMTIPO.ListaFija:=lfTipoPeriodo; //acl
    if Global.OK_ProyectoEspecial(peRecontrataciones)then
    begin
         with DataSource.DataSet do
         begin
              ActivaControlesLiquidacion( ( State in [dsInsert] ) or
                                          ZetaCommonTools.zStrToBool( FieldByName('CB_GLOBAL').AsString ) or
                                          ( ( FieldByName('CB_NOMYEAR').AsInteger = 0 ) or
                                            ( FieldByName('CB_NOMNUME').AsInteger = 0 ) )
                                         );
         end;
    end;
end;


procedure TKardexBaja_DevEx.ActivaControlesLiquidacion( const lEnabled: Boolean );
begin
     CB_FECHA.Enabled := lEnabled;
     CB_NOMYEAR.Enabled := lEnabled;
     CB_NOMTIPO.Enabled := lEnabled;
     CB_NOMNUME.Enabled := lEnabled;
     GroupBox1.Enabled := lEnabled;
end;

end.






