inherited KardexCertificaciones_DevEx: TKardexCertificaciones_DevEx
  Left = 243
  Top = 359
  Caption = 'Historial de Certificaciones'
  ClientHeight = 225
  ClientWidth = 614
  ExplicitWidth = 614
  ExplicitHeight = 225
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 614
    ExplicitWidth = 614
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 355
      ExplicitWidth = 355
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 349
        ExplicitLeft = 269
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 614
    Height = 206
    ExplicitWidth = 614
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object KI_FEC_CER: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'KI_FEC_CER'
      end
      object CI_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CI_CODIGO'
      end
      object CI_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CI_NOMBRE'
      end
      object KI_FOLIO: TcxGridDBColumn
        Caption = 'Folio'
        DataBinding.FieldName = 'KI_FOLIO'
      end
      object KI_APROBO: TcxGridDBColumn
        Caption = 'Aprob'#243
        DataBinding.FieldName = 'KI_APROBO'
      end
      object VENCIMIENTO: TcxGridDBColumn
        Caption = 'Vencimiento'
        DataBinding.FieldName = 'VENCIMIENTO'
        Options.Filtering = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 536
    Top = 120
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
