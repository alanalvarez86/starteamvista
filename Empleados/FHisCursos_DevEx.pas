unit FHisCursos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,}ZBaseGridLectura_DevEx, Db, ExtCtrls,
  Grids, DBGrids, ZetaDBGrid, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, dxSkinsDefaultPainters, StdCtrls, System.Actions;

type
  THisCursos_DevEx = class(TBaseGridLectura_DevEx)
    KC_FEC_TOM: TcxGridDBColumn;
    CU_CODIGO: TcxGridDBColumn;
    CU_NOMBRE: TcxGridDBColumn;
    KC_REVISIO: TcxGridDBColumn;
    KC_HORAS: TcxGridDBColumn;
    KC_EVALUA: TcxGridDBColumn;
    SE_FOLIO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  protected
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  HisCursos_DevEx: THisCursos_DevEx;

implementation


{$R *.DFM}

uses
    {$IFDEF SUPERVISORES}
    dSuper,
    ZAccesosTress,
    {$ELSE}
    dRecursos,
     {$ENDIF}
    dCatalogos,
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaClientDataSet,
    ZAccesosMgr, DCliente, FAutoClasses;

{ THisCursos }

procedure THisCursos_DevEx.Connect;
begin
     dmCatalogos.cdsCursos.Conectar;

     {$IFDEF SUPERVISORES}
     with dmSuper do
     {$ELSE}
     with dmRecursos do
     {$ENDIF}
     begin
          cdsHisCursos.Conectar;
          DataSource.DataSet:= cdsHisCursos;
     end;
end;

procedure THisCursos_DevEx.Refresh;
begin
     TZetaClientDataSet( DataSource.DataSet ).Refrescar;
     //DoBestFit; //No se necesita porque la forma de consulta ya lo hace.
     //dmRecursos.cdsHisCursos.Refrescar; se cambi� para compartir l�gica con supervisores
end;

procedure THisCursos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10135_Historial_cursos_tomados;
end;

procedure THisCursos_DevEx.Agregar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Agregar;
     //dmRecursos.cdsHisCursos.Agregar;
end;

procedure THisCursos_DevEx.Borrar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Borrar;
     DoBestFit;
     //dmRecursos.cdsHisCursos.Borrar;
end;

procedure THisCursos_DevEx.Modificar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Modificar;
     //dmRecursos.cdsHisCursos.Modificar;
end;

function THisCursos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
{$IFDEF SUPERVISORES}
const
     aDerechoGrupo: array[FALSE..TRUE] of Integer = ( D_SUPER_SESIONES_OTROS, D_SUPER_GRUPOS );
{$ENDIF}
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          Result := inherited PuedeBorrar( sMensaje );
          {$IFNDEF SUPERVISORES}
          if Result then
          begin
               Result := dmRecursos.ChecaFolioSesion;
               if not Result then
               begin
                    sMensaje := 'Este registro debe ser borrado en la edici�n de grupos de capacitaci�n';
               end;
          end;
          {$ELSE}
          if ( Result ) then
          begin
               with dmSuper do
               begin
                    if ( ChecaFolioSesion ( cdsHisCursos.FieldByName('SE_FOLIO').AsInteger ) ) then
                    begin
                         Result:= CheckDerecho( aDerechoGrupo[ EsSuSesion ( cdsHisCursos.FieldByName('SE_FOLIO').AsInteger ) ], K_DERECHO_CAMBIO );
                         if ( not Result ) then
                            sMensaje:= 'No tiene derechos de borrar registros';
                    end;
               end;
          end;
          {$ENDIF}
     end;
end;

function THisCursos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          {$IFDEF SUPERVISORES}
          Result:= CheckDerecho(D_SUPER_GRUPOS,K_DERECHO_ALTA) and
                   CheckDerecho(D_SUPER_CURSOS_TOMADOS,K_DERECHO_ALTA ); //Se necesitan los dos derechos para poder agregar grupos desde cursos tomados
          if ( not Result ) then
          begin
               sMensaje:= 'No tiene derecho de agregar grupos';
          end;
          {$ELSE}
          Result := inherited PuedeAgregar( sMensaje );
          {$ENDIF}
     end;
end;

function THisCursos_DevEx.PuedeModificar( var sMensaje: String ): Boolean;
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     Result := dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

procedure THisCursos_DevEx.FormShow(Sender: TObject);
begin
      CreaColumaSumatoria(ZetaDbGridDBtableView.Columns[0],0 , '' , SkCount );
      ApplyMinWidth;
      inherited;
end;

end.
