inherited HisPensionesAlimenticias_DevEx: THisPensionesAlimenticias_DevEx
  Left = 507
  Top = 325
  Caption = 'Pensiones Alimenticias'
  ClientHeight = 228
  ClientWidth = 775
  ExplicitWidth = 775
  ExplicitHeight = 228
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 775
    ExplicitWidth = 775
    inherited Slider: TSplitter
      Left = 323
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 449
      ExplicitLeft = 326
      ExplicitWidth = 449
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 443
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 775
    Height = 209
    ExplicitWidth = 775
    ExplicitHeight = 209
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object PS_ORDEN: TcxGridDBColumn
        Caption = 'Orden'
        DataBinding.FieldName = 'PS_ORDEN'
      end
      object PS_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'PS_FECHA'
      end
      object PS_EXPEDIE: TcxGridDBColumn
        Caption = 'Expediente'
        DataBinding.FieldName = 'PS_EXPEDIE'
      end
      object PS_BENEFIC: TcxGridDBColumn
        Caption = 'Beneficiario'
        DataBinding.FieldName = 'PS_BENEFIC'
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_DESCRIP'
      end
      object PS_ACTIVA: TcxGridDBColumn
        Caption = 'Activo'
        DataBinding.FieldName = 'PS_ACTIVA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
