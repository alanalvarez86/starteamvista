unit FEditKardexCertificaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, StdCtrls, DBCtrls, ZetaNumero, ZetaEdit,
  Mask, ZetaFecha, ComCtrls, DB, ExtCtrls, ZetaSmartLists,
  Buttons, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, 
  cxControls, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, ZetaKeyLookup_DevEx, cxPC, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditKardexCertificaciones_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    Observaciones: TcxTabSheet;
    KI_FEC_CER: TZetaDBFecha;
    lblFecha: TLabel;
    CI_CODIGO: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    KI_FOLIO: TZetaDBEdit;
    Label2: TLabel;
    KI_RENOVAR: TZetaDBNumero;
    Label3: TLabel;
    DBCheckBox1: TDBCheckBox;
    GroupBox1: TGroupBox;
    KI_SINOD_1: TDBEdit;
    Label4: TLabel;
    KI_CALIF_1: TZetaDBNumero;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    KI_SINOD_2: TDBEdit;
    KI_CALIF_2: TZetaDBNumero;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    KI_SINOD_3: TDBEdit;
    KI_CALIF_3: TZetaDBNumero;
    KI_OBSERVA: TDBMemo;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Protected declarations }
    procedure Connect; override;

  end;

var
  EditKardexCertificaciones_DevEx: TEditKardexCertificaciones_DevEx;

implementation

uses dCatalogos, dRecursos,ZetaBuscador_DevEx, ZetaCommonClasses,ZetaCommonLists, ZAccesosTress;

{$R *.dfm}

procedure TEditKardexCertificaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := KI_FEC_CER;
     IndexDerechos := D_EMP_EXP_CERTIFICACIONES;
     HelpContext:= H_Kardex_Certificaciones;
     TipoValorActivo1 := stEmpleado;

     with dmCatalogos do
     begin
          CI_CODIGO.LookupDataset := cdsCertificaciones;
     end;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditKardexCertificaciones_DevEx.FormShow(Sender: TObject);
begin
     PageControl.ActivePage :=  TabGenerales;
     inherited;
end;

procedure TEditKardexCertificaciones_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsKarCertificaciones.Conectar;
          Datasource.Dataset := cdsKarCertificaciones;
     end;
end;

procedure TEditKardexCertificaciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption,'CI_CODIGO', dmRecursos.cdsKarCertificaciones );
end;

procedure TEditKardexCertificaciones_DevEx.SetEditarSoloActivos;
begin
     CI_CODIGO.EditarSoloActivos := TRUE;
end;

end.
