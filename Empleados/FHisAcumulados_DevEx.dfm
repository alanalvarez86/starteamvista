inherited HisAcumulados_DevEx: THisAcumulados_DevEx
  Left = 189
  Top = 155
  Caption = 'Historial de Acumulados'
  ClientWidth = 635
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 635
    inherited ValorActivo2: TPanel
      Width = 376
      inherited textoValorActivo2: TLabel
        Width = 370
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 635
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object CO_NUMERO: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'CO_NUMERO'
      end
      object CO_DESCRIP: TcxGridDBColumn
        Caption = 'Concepto'
        DataBinding.FieldName = 'CO_DESCRIP'
      end
      object AC_ANUAL: TcxGridDBColumn
        Caption = 'Anual'
        DataBinding.FieldName = 'AC_ANUAL'
      end
      object AC_MES_01: TcxGridDBColumn
        Caption = 'Enero'
        DataBinding.FieldName = 'AC_MES_01'
      end
      object AC_MES_02: TcxGridDBColumn
        Caption = 'Febrero'
        DataBinding.FieldName = 'AC_MES_02'
      end
      object AC_MES_03: TcxGridDBColumn
        Caption = 'Marzo'
        DataBinding.FieldName = 'AC_MES_03'
      end
      object AC_MES_04: TcxGridDBColumn
        Caption = 'Abril'
        DataBinding.FieldName = 'AC_MES_04'
      end
      object AC_MES_05: TcxGridDBColumn
        Caption = 'Mayo'
        DataBinding.FieldName = 'AC_MES_05'
      end
      object AC_MES_06: TcxGridDBColumn
        Caption = 'Junio'
        DataBinding.FieldName = 'AC_MES_06'
      end
      object AC_MES_07: TcxGridDBColumn
        Caption = 'Julio'
        DataBinding.FieldName = 'AC_MES_07'
      end
      object AC_MES_08: TcxGridDBColumn
        Caption = 'Agosto'
        DataBinding.FieldName = 'AC_MES_08'
      end
      object AC_MES_09: TcxGridDBColumn
        Caption = 'Septiembre'
        DataBinding.FieldName = 'AC_MES_09'
      end
      object AC_MES_10: TcxGridDBColumn
        Caption = 'Octubre'
        DataBinding.FieldName = 'AC_MES_10'
      end
      object AC_MES_11: TcxGridDBColumn
        Caption = 'Noviembre'
        DataBinding.FieldName = 'AC_MES_11'
      end
      object AC_MES_12: TcxGridDBColumn
        Caption = 'Diciembre'
        DataBinding.FieldName = 'AC_MES_12'
      end
      object AC_MES_13: TcxGridDBColumn
        Caption = 'Mes 13'
        DataBinding.FieldName = 'AC_MES_13'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 8
    Top = 96
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
