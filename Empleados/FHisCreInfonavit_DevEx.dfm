inherited HisCreInfonavit_DevEx: THisCreInfonavit_DevEx
  Left = 358
  Top = 417
  Caption = 'Historial de Cr'#233'dito Infonavit'
  ClientWidth = 597
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 597
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 271
      inherited textoValorActivo2: TLabel
        Width = 265
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 597
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object KI_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'KI_FECHA'
      end
      object KI_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'KI_TIPO'
      end
      object KI_COMENTA: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'KI_COMENTA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
