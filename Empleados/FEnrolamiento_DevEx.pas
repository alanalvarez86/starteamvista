unit FEnrolamiento_DevEx;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.7                                 ::
  :: Unidad:      FEnrolamiento.pas                          ::
  :: Descripci�n: Ventana de Enrolamiento de Huellas         ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal_DevEx, OleCtrls, {SFEStandardLib_TLB,} ExtCtrls, Buttons,
  StdCtrls, ZetaKeyCombo, ZetaCommonLists, ZetaDBTextBox, ZetaDialogo, ZetaCommonClasses, SZCodeBaseX,
  IdBaseComponent, IdCoder, IdCoder3to4, IdCoderMIME, ComCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, ImgList, cxButtons, SFE;

type
  TEnrolamiento_DevEx = class(TZetaDlgModal_DevEx)
    lblMsg: TStaticText;
    TimerInicio: TTimer;
    ProgresoHuellas: TProgressBar;
    TimerEnrollTimeout: TTimer;
    BtnEnrolar: TcxButton;
    Recargar: TcxButton;
    function WaitForTakeOff : Integer;
    function GetError(error : Integer): String;
    procedure BtnEnrolarClick(Sender: TObject);
    procedure PrintError(error : Integer);
    procedure PrintMensaje(sMensaje: String; lLimpiaPantalla: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerInicioTimer(Sender: TObject);
    procedure RecargarClick(Sender: TObject);
    procedure TimerEnrollTimeoutTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FDrivers   : boolean;
    FEmpleado  : Integer;
    FInvitador : Integer;
    FAbrioSFE  : boolean;
    function LeeArchivo:Boolean;
    function AdquiereHuellas: Boolean;
    function CaptureFinger: Integer;
    procedure EnableButtons( bEnable : Boolean );
    procedure SwitchWorking( bWorking : Boolean );
  protected
    { Private declarations }
  public
    { Public declarations }
    property Empleado: Integer read FEmpleado write FEmpleado; // SYNERGY
    property Invitador: Integer read FInvitador write FInvitador; // BIOMETRICO
  end;

var
  Enrolamiento_DevEx: TEnrolamiento_DevEx;

implementation

{$R *.dfm}

uses
  DRecursos,
  DCliente,
  dSistema,
  ZetaClientDataSet,
  ConvUtils, DB;

var
  gTemplate         : array[0..1403] of Integer; //351*4 = 1404
  gImageBytes       : array[0..65535] of Byte; // 256 * 256
  {$ifdef ANTES}
  addrOfTemplate    : Integer; //Direccion de memoria del template
  addrOfImageBytes  : Integer; //Direccion de memoria de la imagen a mostrar
  {$else}
  addrOfTemplate    : Pbyte; //Direccion de memoria del template
  addrOfImageBytes  : Pbyte; //Direccion de memoria de la imagen a mostrar
  {$endif}
  gHintStopPos      : Integer;
  gWorking          : Boolean; //Bandera de lector en procesos
  iDedo             : Integer;

const
  FINGER_BITMAP_WIDTH   : Integer = 256; //Ancho de la imagen de la huella
  FINGER_BITMAP_HEIGHT  : Integer = 256; //Alto de la imagen de la huella
  ColorNColor           : Integer = 3;

//--------------------------------------------------------------------------------------------------------

function TEnrolamiento_DevEx.LeeArchivo : Boolean;
var
   iArchivoId, nRet : Integer;
   F : TSearchRec;

   function GetFileDateTime(FileName: string): TDateTime;
   var intFileAge: LongInt;
   begin
     intFileAge := FileAge(FileName);
     if intFileAge = -1 then
       Result := 0
     else
       Result := FileDateToDateTime(intFileAge)
   end;

begin
     nRet := 0;
     with dmSistema do
     begin
          try
             if FileExists( LocalBioDbPath ) then
                DeleteFile( LocalBioDbPath );
             if not FileExists( LocalBioDbPath ) then //Si no existe lo crea vac�o.
             begin
                iArchivoId := FileCreate( LocalBioDbPath );
                FileClose( iArchivoId );
                ReiniciaHuellas;
             end
             else
             begin
                  FindFirst( LocalBioDbPath, faAnyFile, F );
                  FindClose( F );
             end;
             //Inicia con el archivo.
             {$ifdef ANTES}
             nRet := SFE.Open( LocalBioDbPath, 4, 0);
             {$else}
             if ( FDrivers ) then
                nRet := SFE.Open( PWideChar( LocalBioDbPath ), K_TIPO_BIOMETRICO, 0 );
             {$endif}
             if ( nRet = 0 ) then
             begin
                  enableButtons(True);
                  PrintMensaje( Format( 'Huellas encontradas: %s.', [ IntToStr( SFE.GetEnrollCount() ) ] ), False);
                  Result := True;
             end
             else
             begin
                  PrintError( nRet );
                  Result := False;
             end;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                     switchWorking(False);
                     PrintMensaje( 'Lector detenido, error en inicializaci�n, intente de nuevo.' + Error.Message, True);
                     DeleteFile( LocalBioDbPath );
                     Result := False;
                end;
          end;
     end;
end;

procedure TEnrolamiento_DevEx.enableButtons(bEnable: Boolean);
begin
     BtnEnrolar.Enabled := bEnable;
     Recargar.Enabled := bEnable;
     switchWorking( Not bEnable );
end;

function TEnrolamiento_DevEx.AdquiereHuellas : Boolean;
var
   nRet, iId, iDedo, iValor: Integer;
   iContador : Extended;
   sFilename : string;
   ss           :TStringStream;
   ms           :TMemoryStream;
   oCursor: TCursor;
begin
     iId := 0;
     iDedo := 0;
     iContador := 0;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     switchWorking(true);
     try
          PrintMensaje( 'Inicializando dispositivo biom�trico' + CR_LF + 'Espere un momento...' ,True );

          dmSistema.ReportaHuellaBorra;

          with dmSistema.cdsHuellaGti do
          begin
               Refrescar;
               First;
               Filter := Format( 'ID_NUMERO = %d', [ dmSistema.IdBiometrico ] );
               Filtered := True;

               try
               ms := TMemoryStream.Create;
               while not EOF do
               begin
                    //Barra de Progreso.
                    iContador := iContador + 1;
                    iValor := Round( 100 * iContador / RecordCount );
                    ProgresoHuellas.Position := iValor;
                    ProgresoHuellas.Visible := true;
                    Application.ProcessMessages();

                    try
                       iId := FieldByName( 'ID_NUMERO' ).AsInteger;
                       iDedo := FieldByName( 'HU_INDICE' ).AsInteger;

                       nRet := SFE.CheckFingerNum(iId, iDedo);
                       if nRet <> 0 then
                       begin
                            //Logica de enrolamiento
                            ss := TStringStream.Create( FieldByName( 'HU_HUELLA' ).AsString );
                            ms.Position := 0;
                            SZDecodeBase64( ss, ms );

                            sFilename := ExtractFilePath( dmSistema.LocalBioDbPath ) + IntToStr( iId ) + '_' + IntToStr( iDedo ) +  K_EXT_TEMPLATE;
                            ms.SaveToFile( sFilename );

                            nRet := SFE.LoadTemplateFromFile( PWideChar( sFilename ), addrOfTemplate );

                            if nRet < 0 then
                                 //PrintError(nRet)
                            else
                            begin
                                 nRet := SFE.TemplateEnroll( iId , iDedo, 0, addrOfTemplate );
                                 if ( ( nRet < 0 ) and ( nRet <> -102 ) ) then
                                      //PrintError(nRet);
                            end;

                            DeleteFile( sFilename );
                            dmSistema.ReportaHuella( FieldByName( 'HU_ID' ).AsInteger );

                            Application.ProcessMessages();
                       end;
                    except
                          on Error: Exception do
                          begin
                               PrintMensaje(Format( 'ERROR [Id: %d, Indice: %d, IdHuella: %d]: ' + Error.Message, [ iId, iDedo, FieldByName( 'HU_ID' ).AsInteger ] ),False);
                          end;
                    end;
                    Next;
               end;
               finally
                      Filtered := False;
               end;
          end;
          dmSistema.ReportaHuellas;
     finally
            Screen.Cursor := oCursor;
            switchWorking(False);
     end;
     Result := True;
     PrintMensaje( 'Terminado, presione el bot�n de enrolar.' , True);
     ProgresoHuellas.Visible := false;
     ms.Clear;
end;

procedure TEnrolamiento_DevEx.BtnEnrolarClick(Sender: TObject);
var
   nRet, nCount, iId : Integer;
   sArchivoTemporal : String;
   sPrueba: TStringStream;

label stopWorking;
          function BuscaSiguienteDedo : Boolean;
          begin
               Result := True;
               while iDedo <= 10 do
                    if ( SFE.CheckFingerNum( dmSistema.IdBiometrico, iDedo ) < 0 ) then
                         break
                    else
                         iDedo := iDedo + 1;

               if ( iDedo > 10 ) then
               begin
                    PrintMensaje( 'Ya se enrol� la cantidad m�xima de huellas [10].' ,True);
                    Result := False;
               end;
          end;

          function StreamToString(Stream: TStream): String;
          begin
              with TStringStream.Create('') do
                   try
                       CopyFrom(Stream, Stream.Size);
                       Result := DataString;
                   finally
                       Free;
                   end;
          end;

          procedure LoadFromFileToMem(const filename:string; memStream:TStream);
          var afileStream:TFileStream;
          begin
               afileStream:=TFileStream.Create(filename,fmOpenRead);
               try
                  memStream.CopyFrom(afileStream, afilestream.size );
               finally
                  afileStream.Free;
               end;
          end;

          function SaveTemplateToFile( sArchivo : String ) : Boolean;
          var
            nRet      : Integer;
          begin
               nRet := SFE.SaveTemplateToFile( PWideChar( sArchivo ), addrOfTemplate );
               if nRet < 0 then
               begin
                    PrintError(nRet);
                    SaveTemplateToFile := False;
                    Exit;
               end;
               SaveTemplateToFile := True;
          end;

begin
     inherited;
     iDedo := 1;
     if ( BuscaSiguienteDedo ) then
     begin
          with dmSistema do
          begin
               sArchivoTemporal := ExtractFilePath( LocalBioDbPath ) + IntToStr( IdBiometrico ) + '_' + IntToStr( iDedo );
               iId := IdBiometrico;
          end;
          switchWorking(True);

          nRet := SFE.EnrollStart();
          if nRet < 0 then
          begin
               PrintError(nRet);
               goto stopWorking;
          end;

          for nCount := 1 to 3 do
          begin
               PrintMensaje( 'Coloque su dedo por ' + IntToStr(nCount) + 'a vez!',True);
               Application.ProcessMessages();
               TimerEnrollTimeout.Enabled := True;
               nRet := CaptureFinger();
               TimerEnrollTimeout.Enabled := False;

               if nRet <> 1 then
                    goto stopWorking;

               Application.ProcessMessages();
               PrintMensaje( 'Retire el dedo del lector.', True );

               nRet := SFE.EnrollNth(nCount);

               if nRet < 0 then
               begin
                    PrintError(nRet);
                    goto stopWorking;
               end;

               nRet := WaitForTakeOff();

               if nRet <> 1 then
                    goto stopWorking;
          end;

          nRet := SFE.EnrollEnd(iId, iDedo, 0);
          if nRet < 0 then
          begin
               PrintError(nRet);
               goto stopWorking;
          end;

          nRet := SFE.TemplateGetFromDB(iId, iDedo, addrOfTemplate );
          if nRet < 0 then
          begin
               PrintError(nRet);
               Exit;
          end;

          //Guarda Template File
          nRet := SFE.SaveTemplateToFile( PWideChar( sArchivoTemporal + K_EXT_TEMPLATE ), addrOfTemplate );
          if nRet < 0 then
          begin
               PrintError(nRet);
               Exit;
          end;

          try
             try
                sPrueba := TStringStream.Create('');
                SZEncodeBase64( sArchivoTemporal + K_EXT_TEMPLATE, sPrueba );
                //Ya reporta
                dmSistema.InsertaHuella( Empleado, iDedo, sPrueba.DataString, Invitador );
                PrintMensaje( 'Huella enrolada con �xito.', true );
                iDedo := iDedo + 1;
             except
                PrintMensaje( 'Proceso de enrolamiento err�neo.', true );
             end;
          finally
            DeleteFile( sArchivoTemporal + K_EXT_TEMPLATE );
          end;

        stopWorking:
          switchWorking(False);
     end;
end;

procedure TEnrolamiento_DevEx.switchWorking(bWorking: Boolean);
begin
     BtnEnrolar.Enabled := not bWorking;
     gWorking := bWorking;
     gHintStopPos := 0;
     OK_DevEx.Enabled := not bWorking;
     Cancelar_DevEx.Enabled := not bWorking;
     Recargar.Enabled := not bWorking;
end;

procedure TEnrolamiento_DevEx.PrintError(error: Integer);
begin
     PrintMensaje(GetError(error), True);
end;

function TEnrolamiento_DevEx.WaitForTakeOff: Integer;
var
  nRet      : Integer;
  nCapArea  : Integer;
begin
     Result := 0;
     while True do
     begin
          Application.ProcessMessages();

          if Not gWorking then
          begin
               WaitForTakeOff := 0;
               break;
          end;

          nRet := SFE.Capture();

          if nRet < 0 then
          begin
               PrintError(nRet);
               WaitForTakeOff := 0;
               break;
          end;

          nCapArea := SFE.IsFinger();

          if nCapArea < 0 then
          begin
               WaitForTakeOff := 1;
               break;
          end;
     end;
end;

function TEnrolamiento_DevEx.CaptureFinger: Integer;
var
  nRet        : Integer;
  nCapArea    : Integer;
  nMaxCapArea : Integer;
  nResult     : Integer;
begin
     nResult := 1;
     nMaxCapArea := 0;

     while True do
     begin
          try
             Application.ProcessMessages();
             if Not gWorking then
             begin
                  nResult := 0;
                  break;
             end;

             nRet := SFE.Capture();
             if nRet < 0 then
             begin
                  PrintError(nRet);
                  nResult := 0;
                  break;
             end;

             nCapArea := SFE.IsFinger();
             if nCapArea >= 0 then
             begin
                  if nCapArea < nMaxCapArea + 2 then break;
                  if nCapArea > nMaxCapArea then nMaxCapArea := nCapArea;
                  if nCapArea > 45 then break;
             end;
          except
                Result := nResult;
                Exit;
          end;
     end;

     if nResult = 1 then
     begin
          nRet := SFE.GetImage( addrOfImageBytes );
          if nRet < 0 then
          begin
               PrintError(nRet);
               nResult := 0;
          end
     end;
     //CaptureFinger := nResult;
     Result := nResult;
end;

procedure TEnrolamiento_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     switchWorking( False );
     if ( FAbrioSFE ) then    //@DACP si no se abrio SFE no es necesario cerrarla
     begin
          if ( FDrivers ) then
              SFE.Close();
     end;
     inherited;
end;

procedure TEnrolamiento_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FDrivers  := LoadDLL;
     FAbrioSFE := FALSE;//@DACP
end;

procedure TEnrolamiento_DevEx.TimerInicioTimer(Sender: TObject);
begin
     inherited;
     TimerInicio.Enabled := False;
     iDedo := 1;
     {$ifdef ANTES}
     addrOfImageBytes := Integer(addr(gImageBytes));
     addrOfTemplate := Integer(addr(gTemplate));
     {$else}
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
     {$endif}
     gHintStopPos := 0;

     if FDrivers then
     begin
          FAbrioSFE := LeeArchivo;
          if ( FAbrioSFE ) then
               AdquiereHuellas
          else
               enableButtons( False ) ;
          if ( Empleado = 0 ) and ( Invitador = 0 ) then
          begin
               PrintMensaje( 'No existe informaci�n del empleado o invitador.', True );
               BtnEnrolar.Enabled := False;
               Recargar.Enabled := False;
          end;
     end
     else
     begin
          PrintMensaje( 'Driver del dispositivo biom�trico no detectado. ', False );
          EnableButtons( False );
     end;
end;

procedure TEnrolamiento_DevEx.RecargarClick(Sender: TObject);
begin
     inherited;

     if ( ZConfirm(self.Caption, '�Est� seguro de descargar nuevamente todas las huellas? ' + CR_LF +
                                 'Este proceso puede tomar unos minutos.', 0, mbNo ) ) then
     begin
          with dmSistema do
          begin
               switchWorking(False);
               SFE.Close();
               Application.ProcessMessages();
               DeleteFile( LocalBioDbPath );
               ReiniciaHuellas;
          end;
          TimerInicio.Enabled := True;
     end;
end;

function TEnrolamiento_DevEx.GetError(error: Integer): String;
begin
     case error of
        0: Result := 'Exito!';
        -1: Result := 'Error de imagen de huella!';
        -2: Result := 'Huella inv�lida!';
        -3: Result := 'Error de n�mero biom�trico!';
        -4: Result := 'Error de archivo de dispositivo!';
        -6: Result := 'Error de almacenamiento de dispositivo!';
        -7: Result := 'Error de sensor de dispositivo!';
        -8: Result := 'Orden de enrolamiento incorrecto!';
        -9: Result := 'No puede analizar las tres capturas, intente de nuevo!';
        -11: Result := 'La imagen no es un dedo!';
        -100: Result := 'No hay dispositivo biom�trico!';
        -101: Result := 'No puede abrir archivo de dispositivo local!';
        -102: Result := 'Ya ha enrolado esa huella!';
        -103: Result := 'Error de identificaci�n de huella!';
        -104: Result := 'Error de verificaci�n de huella!';
        -105: Result := 'No puede abrir la imagen de huella!';
        -106: Result := 'No puede crear imagen de huella!';
        -107: Result := 'No puede abrir huella!';
        -108: Result := 'No puede crear huella!';
        else Result := 'Error desconocido! (Numero =' + IntToStr(error) + ')';
    end;
end;

procedure TEnrolamiento_DevEx.PrintMensaje(sMensaje: String; lLimpiaPantalla: Boolean);
begin
     if (lLimpiaPantalla) then
        lblMsg.Caption := sMensaje
     else
         lblMsg.Caption :=  lblMsg.Caption + CR_LF + sMensaje; 
end;

procedure TEnrolamiento_DevEx.TimerEnrollTimeoutTimer(Sender: TObject);
begin
     inherited;
     TimerEnrollTimeout.Enabled := False;
     switchWorking(False);
     PrintMensaje( 'Tiempo ha expirado.' + CR_LF + 'Presione nuevamente el bot�n de enrolar.', True );
     Application.ProcessMessages();
end;

end.
