inherited GridPrestamos_DevEx: TGridPrestamos_DevEx
  Left = 273
  Top = 183
  Caption = 'Registro de Pr'#233'stamos'
  ClientHeight = 310
  ClientWidth = 678
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 274
    Width = 678
    inherited OK_DevEx: TcxButton
      Left = 512
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 592
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 678
    inherited ValorActivo2: TPanel
      Width = 352
      inherited textoValorActivo2: TLabel
        Width = 346
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 94
    Width = 678
    Height = 180
    TabOrder = 4
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N'#250'mero'
        Width = 65
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 200
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PR_REFEREN'
        Title.Caption = 'Referencia'
        Width = 60
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PR_FECHA'
        Title.Caption = 'Inicio'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PR_STATUS'
        Title.Caption = 'Status'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PR_FORMULA'
        Title.Caption = 'F'#243'rmula'
        Width = 150
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PR_MONTO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Monto'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PR_SALDO_I'
        Title.Alignment = taRightJustify
        Title.Caption = 'Abonos Anteriores'
        Width = 91
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PR_SALDO'
        ReadOnly = True
        Title.Alignment = taRightJustify
        Title.Caption = 'Saldo'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PR_SUB_CTA'
        Title.Caption = 'Subcuenta'
        Width = 99
        Visible = True
      end>
  end
  object PanelTope: TPanel [4]
    Left = 0
    Top = 50
    Width = 678
    Height = 44
    Align = alTop
    TabOrder = 2
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 676
      Height = 42
      Align = alClient
      TabOrder = 0
      object AH_TIPOLbl: TLabel
        Left = 6
        Top = 14
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object PR_TIPO: TZetaKeyLookup_DevEx
        Left = 36
        Top = 11
        Width = 309
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 50
      end
      object rgAgregarPrestamos: TRadioGroup
        Left = 352
        Top = 1
        Width = 323
        Height = 40
        Align = alRight
        BiDiMode = bdRightToLeft
        Caption = ' En caso de que las reglas no se cumplan '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Agregar los Pr'#233'stamos'
          'No Agregar los Pr'#233'stamos')
        ParentBiDiMode = False
        TabOrder = 1
      end
    end
  end
  object ZFecha: TZetaDBFecha [5]
    Left = 272
    Top = 160
    Width = 105
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 5
    Text = '14/oct/99'
    Valor = 36447.000000000000000000
    Visible = False
    DataField = 'PR_FECHA'
    DataSource = DataSource
  end
  object zComboStatus: TZetaDBKeyCombo [6]
    Left = 272
    Top = 184
    Width = 83
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 6
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfStatusPrestamo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'PR_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 468
    Top = 105
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
