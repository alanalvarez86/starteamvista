inherited EditEmpFoto: TEditEmpFoto
  Left = 192
  Top = 108
  Caption = 'Foto y Documentos'
  ClientHeight = 365
  ClientWidth = 408
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 329
    Width = 408
    inherited OK: TBitBtn
      Left = 241
    end
    inherited Cancelar: TBitBtn
      Left = 326
    end
  end
  inherited PanelSuperior: TPanel
    Width = 408
    inherited ImprimirBtn: TSpeedButton
      Visible = False
    end
    inherited ImprimirFormaBtn: TSpeedButton
      Visible = False
    end
    inherited CortarBtn: TSpeedButton
      Left = 192
    end
    inherited CopiarBtn: TSpeedButton
      Left = 216
    end
    inherited PegarBtn: TSpeedButton
      Left = 240
    end
    inherited UndoBtn: TSpeedButton
      Left = 264
    end
    object PanelExtra: TPanel
      Left = 288
      Top = 2
      Width = 113
      Height = 27
      BevelOuter = bvNone
      TabOrder = 1
      object LeerDisco: TSpeedButton
        Left = 7
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Leer de Disco'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
          07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
          0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
          33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = LeerDiscoClick
      end
      object GrabarDisco: TSpeedButton
        Left = 31
        Top = 2
        Width = 24
        Height = 24
        Hint = 'Grabar a Disco'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
          7700333333337777777733333333008088003333333377F73377333333330088
          88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
          000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
          FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
          99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
          99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
          99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
          93337FFFF7737777733300000033333333337777773333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = GrabarDiscoClick
      end
      object BtnLeerTwain: TSpeedButton
        Left = 55
        Top = 2
        Width = 26
        Height = 24
        Hint = 'Leer Dispositivo Twain'
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          033333FFFF77777773F330000077777770333777773FFFFFF733077777000000
          03337F3F3F777777733F0797A770003333007F737337773F3377077777778803
          30807F333333337FF73707888887880007707F3FFFF333777F37070000878807
          07807F777733337F7F3707888887880808807F333333337F7F37077777778800
          08807F333FFF337773F7088800088803308073FF777FFF733737300008000033
          33003777737777333377333080333333333333F7373333333333300803333333
          33333773733333333333088033333333333373F7F33333333333308033333333
          3333373733333333333333033333333333333373333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BtnLeerTwainClick
      end
      object BtnSeleccionarTwain: TSpeedButton
        Left = 86
        Top = 2
        Width = 26
        Height = 24
        Hint = 'Seleccionar Dispositivo Twain'
        Flat = True
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDD0000000DDDDD000DDDDD000D0000000DDDDD070DDDDD070D0000000DDDD
          D0008DDD8000D0000000DDDDD00000000000D0000000D444407000070000D000
          0000D4FFF07000070000D0000000D4F8800000000000D0000000D4FFFF000070
          000DD0000000D4F88F80088F00DDD0000000D4FFFFF00FFF00DDD0000000D4F8
          8F80088F00DDD0000000D4FFFFFFFFFF4DDDD0000000D444444444444DDDD000
          0000D474474474474DDDD0000000D444444444444DDDD0000000DDDDDDDDDDDD
          DDDDD0000000}
        ParentShowHint = False
        ShowHint = True
        OnClick = BtnSeleccionarTwainClick
      end
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 408
    inherited ValorActivo2: TPanel
      Width = 82
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 51
    Width = 408
    Height = 62
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 60
      Top = 12
      Width = 24
      Height = 13
      Caption = 'Tipo:'
    end
    object Label2: TLabel
      Left = 10
      Top = 36
      Width = 74
      Height = 13
      Caption = 'Observaciones:'
    end
    object IM_TIPO: TDBComboBox
      Left = 88
      Top = 8
      Width = 105
      Height = 21
      DataField = 'IM_TIPO'
      DataSource = DataSource
      ItemHeight = 13
      Items.Strings = (
        'FOTO'
        'FIRMA'
        'HUELLA')
      TabOrder = 0
      OnKeyPress = IM_TIPOKeyPress
    end
    object IM_OBSERVA: TDBEdit
      Left = 88
      Top = 32
      Width = 232
      Height = 21
      DataField = 'IM_OBSERVA'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object Documento: TPDBMultiImage [4]
    Left = 71
    Top = 124
    Width = 266
    Height = 200
    ImageReadRes = lAutoMatic
    ImageWriteRes = sAutoMatic
    DataEngine = deBDE
    JPegSaveQuality = 75
    JPegSaveSmooth = 0
    ImageDither = True
    UpdateAsJPG = False
    UpdateAsBMP = False
    UpdateAsGIF = False
    UpdateAsPCX = False
    UpdateAsPNG = False
    UpdateAsTIF = True
    Color = clBtnFace
    DataField = 'IM_BLOB'
    DataSource = DataSource
    RichTools = False
    StretchRatio = True
    TabOrder = 4
    TextLeft = 0
    TextTop = 0
    TextRotate = 0
    TifSaveCompress = sNONE
    UseTwainWindow = False
  end
  inherited DataSource: TDataSource
    Left = 25
    Top = 207
  end
  object OpenPictureDialog: TOpenPictureDialog
    DefaultExt = 'JPG'
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Title = 'Leer de Disco'
    Left = 24
    Top = 239
  end
  object SavePictureDialog: TSavePictureDialog
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Left = 24
    Top = 272
  end
end
