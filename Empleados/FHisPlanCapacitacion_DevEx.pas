unit FHisPlanCapacitacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,} ZBaseGridLectura_DevEx, Grids, DBGrids, Db,
  ZetaDBGrid, ExtCtrls, StdCtrls, Buttons, ZetaKeyCombo, 
  ComCtrls, ZetaDBTextBox,ZetaCommonClasses,Variants, ZBaseConsulta,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  TressMorado2013, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  THisPlanCapacita_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label2: TLabel;
    txtPorcentaje: TZetaTextBox;
    txtPorcPerfil: TZetaTextBox;
    Label3: TLabel;
    txtPorcIndividual: TZetaTextBox;
    Label4: TLabel;
    CC_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    NC_DESCRIP: TcxGridDBColumn;
    EC_FECHA: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    PC_GLOBAL: TcxGridDBColumn;
    CP_ELEMENT: TcxGridDBColumn;
    Observaciones: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FPorcPerfil:TPesos;
    FPorcIndiv:TPesos;     
    function EsIndividual: Boolean;
    function PorcentajePorEmpleado: TPesos;
    procedure CalculaPorcentajes;
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean;override;

  public
    { Public declarations }
  end;

var
  HisPlanCapacita_DevEx: THisPlanCapacita_DevEx;

implementation

{$R *.DFM}

uses {$IFDEF SUPERVISORES}
     dSuper,
     {$ELSE}
     dRecursos,
     {$ENDIF}
     DCatalogos,
     dSistema, ZetaCommonLists,ZetaCommonTools,ZAccesosTress,ZAccesosMgr ;

procedure THisPlanCapacita_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     {$IFDEF SUPERVISORES}
     with dmSuper do
     begin
     {$ELSE}
     with dmRecursos do
     begin
     {$ENDIF}
          cdsPlanCapacitacion.Conectar;
          cdsHisCompeten.Conectar;
          DataSource.DataSet:= cdsPlanCapacitacion;
     end;
     CalculaPorcentajes;
end;

Procedure THisPlanCapacita_DevEx.CalculaPorcentajes;
begin
     txtPorcentaje.Caption  := FloatToStrF( PorcentajePorEmpleado,ffFixed,3,2)+' %';
     txtPorcPerfil.Caption :=  FloatToStrF( FPorcPerfil ,ffFixed,3,2)+' %';
     txtPorcIndividual.Caption :=  FloatToStrF( FPorcIndiv ,ffFixed,3,2)+' %';
end;

procedure THisPlanCapacita_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;

     {$IFDEF SUPERVISORES}
     IndexDerechos := D_SUP_PLAN_CAPACITA ;
     HelpContext:= H_Sup_PlanCapacitacion;
     {$else}
     IndexDerechos := D_EMP_PLANCAPACITA;
     HelpContext:= H_PlanCapacitacion;
     {$endif}
end;

procedure THisPlanCapacita_DevEx.Agregar;
begin
     {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsHisCompeten.Agregar;
     CalculaPorcentajes;
end;

procedure THisPlanCapacita_DevEx.Borrar;
begin
     {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsHisCompeten.Borrar;
     CalculaPorcentajes;
     DoBestFit;
end;

procedure THisPlanCapacita_DevEx.Modificar;
begin
     with {$IFDEF SUPERVISORES}dmSuper {$ELSE}dmRecursos{$ENDIF} do
     begin
          cdsHisCompeten.Locate('CC_CODIGO;NC_NIVEL',VarArrayOf([cdsPlanCapacitacion.FieldByName('CC_CODIGO').AsString,cdsPlanCapacitacion.FieldByName('NC_NIVEL').AsString]),[]);
          cdsHisCompeten.Modificar;
     end;
     CalculaPorcentajes;
end;


procedure THisPlanCapacita_DevEx.Refresh;
begin
     inherited;
     {$IFDEF SUPERVISORES}dmSuper{$ELSE}dmRecursos{$ENDIF}.cdsPlanCapacitacion.Refrescar;
     CalculaPorcentajes;
     DoBestFit;
end;

function THisPlanCapacita_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          if not( EsIndividual )then
          begin
               sMensaje := 'Solo Se Pueden Borrar Competencias Individuales';
               Result := False;
          end;
     end;

end;

function THisPlanCapacita_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          if not( EsIndividual )then
          begin
               sMensaje := 'Solo Se Pueden Modificar Competencias Individuales';
               Result := False;
          end;
     end;
end;

function THisPlanCapacita_DevEx.PuedeAgregar(var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
end;

function THisPlanCapacita_DevEx.EsIndividual: Boolean;
begin
     Result := DataSource.DataSet.FieldByName('PC_GLOBAL').AsString = 'Individual';
end;


function THisPlanCapacita_DevEx.PorcentajePorEmpleado: TPesos;
var
   SumaPeso:TPesos;
   CompIndiv,SumIndiv,ContPuesto,Factor,PuestoCumplido:Integer;

    function CompetenciaCumplida :Boolean;
    begin
         with DataSource.DataSet do
         begin
              Result := ( FieldByName('EC_FECHA').AsDateTime <> NullDateTime ) and ( FieldByName('EC_NIVEL').AsInteger >= FieldByName('NC_NIVEL').AsInteger );
         end;
    end;


begin
     CompIndiv := 0;
     SumIndiv := 0;
     SumaPeso := 0;
     ContPuesto:= 0;
     PuestoCumplido := 0;
     FPorcIndiv := 0;
     FPorcPerfil := 0;
     with DataSource.DataSet do
     begin
          DisableControls;
          try
              First;
              while Not Eof do
              begin
                   if EsIndividual then
                   begin
                        SumIndiv := SumIndiv + 1;
                        If CompetenciaCumplida then
                        begin
                             CompIndiv := CompIndiv + 1;
                        end
                   end
                   else
                   begin
                        ContPuesto := ContPuesto +1;
                        if CompetenciaCumplida then
                        begin
                             PuestoCumplido := PuestoCumplido + 1;
                             SumaPeso := SumaPeso + FieldByName('PC_PESO').AsFloat;
                        end;
                   end;
                   Next;
              end;


              IF ( ContPuesto > 0 )and ( SumIndiv > 0 )then
                 Factor := 2
              else
                  Factor := 1;

              if ( SumIndiv > 0 ) then
              begin
                   FPorcIndiv := (CompIndiv/SumIndiv);
              end;
              if ( ContPuesto > 0 ) then
              begin
                   FPorcPerfil := (PuestoCumplido/ContPuesto);
              end;

              Result :=  ((FPorcIndiv + FPorcPerfil)/Factor)*100;
              FPorcIndiv := FPorcIndiv * 100;
              FPorcPerfil := FPorcPerfil * 100;

          finally
                 EnableControls;
                 First;
          end;
     end;
end;



procedure THisPlanCapacita_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
   CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
  //CreaColumaSumatoria('Observaciones', skCount);
   ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  end;

end.
