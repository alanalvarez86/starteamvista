unit FEditSesion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls,
  ZetaKeyCombo, ZetaNumero, ZetaHora, ZetaFecha, Mask,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons, {$ifdef ICUMEDICAL_CURSOS}System.IOUtils,{$endif}
  ZetaKeyLookup_DevEx;

type
  TEditSesion_DevEx = class(TBaseEdicion_DevEx)
    lblCurso: TLabel;
    SE_REVISIO: TDBEdit;
    lblRevision: TLabel;
    lblFecIni: TLabel;
    SE_FEC_INI: TZetaDBFecha;
    lblFecFin: TLabel;
    SE_FEC_FIN: TZetaDBFecha;
    SE_HOR_INI: TZetaDBHora;
    lblHorIni: TLabel;
    lblHorFin: TLabel;
    SE_HOR_FIN: TZetaDBHora;
    SE_HORAS: TZetaDBNumero;
    KC_HORASLbl: TLabel;
    lblCupo: TLabel;
    SE_CUPO: TZetaDBNumero;
    MA_CODIGO: TZetaDBKeyLookup_DevEx;
    lblMaestro: TLabel;
    LblCosto1: TLabel;
    SE_COSTO1: TZetaDBNumero;
    SE_COSTO2: TZetaDBNumero;
    LblCosto2: TLabel;
    LblCosto3: TLabel;
    SE_COSTO3: TZetaDBNumero;
    SE_LUGAR: TDBEdit;
    lblLugar: TLabel;
    lblComenta: TLabel;
    SE_COMENTA: TDBEdit;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    pnBotones: TPanel;
    btnInscripciones: TcxButton;
    btnListaAsis: TcxButton;
    btnAprobados: TcxButton;
    btnReservaciones: TcxButton;
    lblStatus: TLabel;
    SE_STATUS: TZetaDBKeyCombo;
    btnEvaluaciones: TcxButton;
    lblEstablec: TLabel;
    SE_EST: TZetaDBKeyLookup_DevEx;
    GroupBox1: TGroupBox;
    lblDescripcionArchivo: TLabel;
    lblTipoArchivo: TLabel;
    lblUbicacionArchivo: TLabel;
    btnAgregarDocumento: TcxButton;
    btnBorraDocumento: TcxButton;
    btnVerDocumento: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btnReservacionesClick(Sender: TObject);
    procedure btnInscripcionesClick(Sender: TObject);
    procedure btnListaAsisClick(Sender: TObject);
    procedure btnAprobadosClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnEvaluacionesClick(Sender: TObject);
    procedure SetEditarSoloActivos;
    procedure FormShow(Sender: TObject);
    procedure btnAgregarDocumentoClick(Sender: TObject);
    procedure btnBorraDocumentoClick(Sender: TObject);
    procedure btnVerDocumentoClick(Sender: TObject);
  private
    { Private declarations }
    procedure Setcontroles;
    {$ifdef ICUMEDICAL_CURSOS}
    procedure EnabledControlDocumento;
    procedure AgregaDocumento;
    procedure DialogoAgregaDocumento( const lAgregando: Boolean );
    {$endif}
  protected
     procedure Connect; override;
     procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditSesion_DevEx: TEditSesion_DevEx;

implementation

uses DCatalogos,
     DRecursos,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZAccesosTress
     {$ifdef ICUMEDICAL_CURSOS }
     , ZetaFilesTools,
     FEditDocumentoICU_DevEx
     {$endif};

{$R *.DFM}

procedure TEditSesion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_SESIONES;
     HelpContext:= H_SESIONES;
     FirstControl := CU_CODIGO;
     with dmCatalogos do
     begin
	      CU_CODIGO.LookupDataSet := cdsCursos;
          MA_CODIGO.LookupDataset := cdsMaestros;
          SE_EST.LookupDataset := cdsEstablecimientos;
     end;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditSesion_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  {$ifdef ICUMEDICAL_CURSOS}
  GroupBox1.Visible := True;
  EditSesion_DevEx.ClientHeight := 583;
  EditSesion_DevEx.ClientWidth := 564;
  {$else}
  GroupBox1.Visible := False;
  {$endif}
end;

procedure TEditSesion_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          cdsMaestros.Conectar;
          cdsEstablecimientos.Conectar;
          {$ifdef ICUMEDICAL_CURSOS}cdsSesiones.Conectar;{$endif}
     end;
     DataSource.DataSet:= dmRecursos.cdsSesion;
     {$ifdef ICUMEDICAL_CURSOS}
     EnabledControlDocumento;
     {$endif}

end;

procedure TEditSesion_DevEx.EscribirCambios;
          function VerificaDuracion: Boolean;
          begin
               Result := True;
               if( dmRecursos.cdsSesion.FieldByName('SE_HORAS').AsFloat <= 0 ) then
               begin
                    if not ZConfirm( Caption, 'La duraci�n debe ser mayor a cero ' + CR_LF + '�Desea continuar?',0,mbNo ) then
                    begin
                         Result := False;
                    end;
               end;
          end;
          function VerificaMaestro: Boolean;
          begin
               Result := True;
               if( strVacio( dmRecursos.cdsSesion.FieldByName('MA_CODIGO').AsString ) )then
               begin
                    if not ZConfirm( Caption, 'No se tiene asignado un maestro' + CR_LF + '�Desea continuar?',0,mbNo ) then
                    begin
                         Result := False;
                    end;
               end;
          end;
begin
     if( VerificaDuracion ) and ( VerificaMaestro ) then
     begin
          inherited EscribirCambios;
     end;
end;

procedure TEditSesion_DevEx.btnReservacionesClick(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridReservacionesSesion;
end;

procedure TEditSesion_DevEx.btnInscripcionesClick(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarInscripciones;
end;

procedure TEditSesion_DevEx.btnListaAsisClick(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarListaAsistencia;
end;

{$ifdef ICUMEDICAL_CURSOS}
procedure TEditSesion_DevEx.EnabledControlDocumento;
var
   lEnabled: Boolean;
   sDescripcion, sTipo, sRuta: string;
   oColor: TColor;
begin
     with dmRecursos.cdsSesion do
     //with dmCatalogos.cdsSesiones do
     begin
          lEnabled := StrLleno(FieldByName( 'SE_D_RUTA' ).AsString);

          if lEnabled then
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: ' + TPath.GetFileName(FieldByName( 'SE_D_RUTA' ).AsString);
               sTipo := 'Tipo: ' + ZetaFilesTools.GetTipoDocumento( FieldByName( 'SE_D_EXT' ).ASString );
               sRuta := 'Ruta: ' + FieldByName( 'SE_D_RUTA' ).AsString;
          end
          else
          begin
               with lblDescripcionArchivo do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay ning�n documento almacenado';
                    sTipo := VACIO;
                    sRuta := VACIO;
               end;
          end;
     end;

     btnVerDocumento.Enabled := lEnabled;
     btnBorraDocumento.Enabled := lEnabled;

     with lblDescripcionArchivo do
     begin
          Font.Color := oColor;
          Caption := sDescripcion;
          Hint := TPath.GetFileName(dmRecursos.cdsSesion.FieldByName( 'SE_D_RUTA' ).AsString);
     end;

     lblTipoArchivo.Caption := sTipo;
     lblUbicacionArchivo.Caption := sRuta;
     lblUbicacionArchivo.Hint := dmRecursos.cdsSesion.FieldByName( 'SE_D_RUTA' ).AsString;
end;


procedure TEditSesion_DevEx.AgregaDocumento;
var
   sDocumento: string;
begin
     //sDocumento := dmCatalogos.cdsSesiones.FieldByName('SE_D_NOM').AsString;
     sDocumento := dmRecursos.cdsSesion.FieldByName( 'SE_D_NOM' ).AsString;
     if StrVacio( sDocumento ) or ZetaDialogo.ZConfirm( Caption, '�Desea sustituir el documento: ' + sDocumento + ' por uno nuevo?', 0, mbNo ) then
     begin
          DialogoAgregaDocumento( TRUE );
     end;
end;


procedure TEditSesion_DevEx.DialogoAgregaDocumento( const lAgregando: Boolean );
begin
     //with dmCatalogos.cdsSesiones do
     with dmRecursos.cdsSesion do
     begin
          if FEditDocumentoICU_DevEx.EditarDocumento( lAgregando, FIeldByName( 'SE_D_NOM' ).AsString, H_DOCUMENTO_CONCEPTOS, dmRecursos.CargaDocumentoICU ) then
          begin
               EnabledControlDocumento;
               if state in [ dsinsert, dsedit ] then
                  Edit;
          end;
     end;
end;
{$endif}

procedure TEditSesion_DevEx.btnBorraDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     with dmRecursos do
     begin
          if ( ZetaDialogo.ZConfirm(Caption, '� Desea borrar el documento ' + cdsSesion.FieldByName('SE_D_NOM').AsString + ' ?', 0, mbNo ) ) then
          begin
               dmRecursos.BorraDocumentoICU;
               EnabledControlDocumento;
          end;
     end;
     {$endif}
end;

procedure TEditSesion_DevEx.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     dmRecursos.AbreDocumentoICU;
     {$endif}
end;

procedure TEditSesion_DevEx.btnAgregarDocumentoClick(Sender: TObject);
begin
     inherited;
     {$ifdef ICUMEDICAL_CURSOS}
     AgregaDocumento;
     {$endif}
end;

procedure TEditSesion_DevEx.btnAprobadosClick(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarAprobados;
end;

procedure TEditSesion_DevEx.Setcontroles;
begin
     btnInscripciones.Enabled := not Editing;
     btnEvaluaciones.Enabled := not Editing;
     btnListaAsis.Enabled := not Editing and ( dmRecursos.cdsSesion.FieldbyName('SE_INSCRITO').AsInteger > 0 );
     btnAprobados.Enabled := not Editing;
     btnReservaciones.Enabled := not Editing;
end;

procedure TEditSesion_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if( Field = nil )then
     begin
          dmRecursos.FSesionCurso := dmRecursos.cdsSesion.FieldByName('CU_CODIGO').AsString;
          {$ifdef ICUMEDICAL_CURSOS}
          EnabledControlDocumento;
          {lblDescripcionArchivo.Caption := dmRecursos.cdsSesion.FieldByName('SE_D_NOM').AsString;
          lblTipoArchivo.Caption := dmRecursos.cdsSesion.FieldByName( 'SE_D_EXT' ).AsString;
          lblUbicacionArchivo.Caption := dmRecursos.cdsSesion.FieldByName( 'SE_D_RUTA' ).AsString;}
          {$endif}
          SetControles;
     end;
end;

procedure TEditSesion_DevEx.btnEvaluacionesClick(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarEvaluaciones;
end;

procedure TEditSesion_DevEx.SetEditarSoloActivos;
begin
     CU_CODIGO.EditarSoloActivos := TRUE;
     MA_CODIGO.EditarSoloActivos := TRUE;
     SE_EST.EditarSoloActivos := TRUE;
end;

end.
