unit FHisAhorros_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Grids, DBGrids, ExtCtrls,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid, StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  THisAhorros_DevEx = class(TBaseGridLectura_DevEx)
    AH_TIPO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    AH_FECHA: TcxGridDBColumn;
    AH_NUMERO: TcxGridDBColumn;
    AH_SALDO: TcxGridDBColumn;
    AH_STATUS: TcxGridDBColumn;
    AH_SUB_CTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  HisAhorros_DevEx: THisAhorros_DevEx;

implementation

{$R *.DFM}

uses DRecursos,
     DTablas,
     DSistema,
     ZetaCommonLists,
     ZetaCommonClasses;

procedure THisAhorros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     {$ifdef CAJAAHORRO}
     TipoValorActivo2 := stTipoAhorro;
     {$endif}
     HelpContext := H10141_Emp_nom_ahorros;
end;

procedure THisAhorros_DevEx.Connect;
begin
     dmTablas.cdsTAhorro.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsHisAhorros.Conectar;
          DataSource.DataSet:= cdsHisAhorros;
     end;
end;

procedure THisAhorros_DevEx.Refresh;
begin
     dmRecursos.cdsHisAhorros.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisAhorros_DevEx.Agregar;
begin
     dmRecursos.cdsHisAhorros.Agregar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisAhorros_DevEx.Borrar;
begin
     dmRecursos.cdsHisAhorros.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisAhorros_DevEx.Modificar;
begin
     dmRecursos.cdsHisAhorros.Modificar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure THisAhorros_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;

end;

end.
