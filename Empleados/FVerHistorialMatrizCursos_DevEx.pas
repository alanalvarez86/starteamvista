unit FVerHistorialMatrizCursos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids,
  ZetaDBGrid, DB, dxSkinsCore,  TressMorado2013,
  dxSkinsdxBarPainter, dxBar, cxClasses, ImgList, cxGraphics, 
  cxLookAndFeels, cxLookAndFeelPainters, Menus, cxButtons,
   dxSkinsDefaultPainters, 
  cxLabel, cxBarEditItem;

type
  TVerHistorialMatrizCursos_DevEx = class(TZetaDlgModal_DevEx)
    Panel2: TPanel;
    dsEvaluacionMult: TDataSource;
    GridRenglones: TZetaDBGrid;
    cxImageList16Edicion: TcxImageList;
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    textoValorActivo1: TLabel;
    ValorActivo2: TPanel;
    textoValorActivo2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    FNombre : String;
    FCurso: String;
    procedure CargarDatos;
    { Public declarations }
  end;
  TDBGridHack = class(TDBGrid);
var
  VerHistorialMatrizCursos_DevEx: TVerHistorialMatrizCursos_DevEx;


implementation

uses ZetaClientDataSet;

{$R *.dfm}

procedure TVerHistorialMatrizCursos_DevEx.CargarDatos;
begin
     textoValorActivo1.Caption := FNombre;
     textoValorActivo2.Caption := FCurso;
end;

procedure TVerHistorialMatrizCursos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.FixedColor := RGB(235,235,235);//Gris
     GridRenglones.TitleFont.Color := RGB(77,59,75);//Gris-Morado tono fuerte
     GridRenglones.Font.Color := RGB(156,129,139);//Gris-Morado
end;

procedure TVerHistorialMatrizCursos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     CargarDatos;
end;

procedure TVerHistorialMatrizCursos_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     Close;
end;

end.
