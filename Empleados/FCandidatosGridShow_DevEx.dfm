inherited CandidatosGridShow_DevEx: TCandidatosGridShow_DevEx
  Left = 349
  Top = 220
  Width = 490
  Caption = 'Lista de Candidatos'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 474
    inherited OK_DevEx: TcxButton
      Left = 248
      Width = 135
      Caption = '  &Aceptar Candidato'
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 388
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 474
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OnDblClick = ZetaDBGridDBTableViewDblClick
      OnKeyDown = ZetaDBGridDBTableViewKeyDown
      OnCustomDrawCell = ZetaDBGridDBTableViewCustomDrawCell
      OptionsSelection.MultiSelect = False
      object SO_FOLIO: TcxGridDBColumn
        Caption = 'Folio'
        DataBinding.FieldName = 'SO_FOLIO'
        Width = 65
      end
      object SO_APE_PAT: TcxGridDBColumn
        Caption = 'Apellido Paterno'
        DataBinding.FieldName = 'SO_APE_PAT'
        Width = 65
      end
      object SO_APE_MAT: TcxGridDBColumn
        Caption = 'Apellido Materno'
        DataBinding.FieldName = 'SO_APE_MAT'
        Width = 65
      end
      object SO_NOMBRES: TcxGridDBColumn
        Caption = 'Nombre(s)'
        DataBinding.FieldName = 'SO_NOMBRES'
        Width = 65
      end
      object SO_FEC_NAC: TcxGridDBColumn
        Caption = 'Fecha Nac.'
        DataBinding.FieldName = 'SO_FEC_NAC'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yy'
        Width = 65
      end
      object CB_CODIGO: TcxGridDBColumn
        DataBinding.FieldName = 'CB_CODIGO'
        Visible = False
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
