unit FKardexAlta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FKardexCambio_DevEx, StdCtrls, ZetaSmartLists, Db,
  ExtCtrls, DBCtrls, ZetaFecha, Mask,
  ZetaNumero, ZetaDBTextBox, ComCtrls, Buttons, ZetaKeyCombo, ZetaEdit,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxControls, cxContainer, cxEdit, dxSkinsdxBarPainter,
  ZetaSmartLists_DevEx, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxListBox,
  ZetaKeyLookup_DevEx, cxPC, cxButtons;

type
  TKardexAlta_DevEx = class(TKardexCambio_DevEx)
    TabContratacion_DevEx: TcxTabSheet;
    TabArea_DevEx: TcxTabSheet;
    CB_NOMINA: TZetaDBKeyCombo;
    CB_PLAZA: TZetaDBKeyLookup_DevEx;
    Panel1: TPanel;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_PATRON: TZetaDBKeyLookup_DevEx;
    GroupBox1: TGroupBox;
    CB_FEC_CONlbl: TLabel;
    CB_CONTRATlbl: TLabel;
    LVencimiento: TLabel;
    CB_FEC_CON: TZetaDBFecha;
    CB_CONTRAT: TZetaDBKeyLookup_DevEx;
    CB_FEC_COV: TZetaDBFecha;
    CB_FEC_ANT: TZetaDBFecha;
    Label11: TLabel;
    label99: TLabel;
    Label36: TLabel;
    CB_HORARIOlbl: TLabel;
    CB_CLASIFIlbl: TLabel;
    CB_PUESTOlbl: TLabel;
    ZAntiguedad: TZetaTextBox;
    Label34: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    Panel2_plazas: TPanel;
    Panel2: TPanel;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL1lbl: TLabel;
    bbMostrarCalendario_DevEx: TcxButton;
    Panel3: TPanel;
    Panel5: TPanel;
    Panel4: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CB_CONTRATExit(Sender: TObject);
    procedure CB_AUTOSALClick(Sender: TObject);
    procedure CB_PUESTOExit(Sender: TObject);
    procedure CB_CLASIFIExit(Sender: TObject);
    procedure Panel1Enter(Sender: TObject);
    procedure Panel2Enter(Sender: TObject);
    procedure Panel3Enter(Sender: TObject);
    procedure Panel4Enter(Sender: TObject);
    procedure Panel5Enter(Sender: TObject);
    //procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure Panel2_plazasEnter(Sender: TObject);
    procedure CB_NIVEL12ValidLookup(Sender: TObject);
    procedure CB_NIVEL11ValidLookup(Sender: TObject);
    procedure CB_NIVEL10ValidLookup(Sender: TObject);
    procedure CB_NIVEL9ValidLookup(Sender: TObject);
    procedure CB_NIVEL8ValidLookup(Sender: TObject);
    procedure CB_NIVEL7ValidLookup(Sender: TObject);
    procedure CB_NIVEL6ValidLookup(Sender: TObject);
    procedure CB_NIVEL5ValidLookup(Sender: TObject);
    procedure CB_NIVEL4ValidLookup(Sender: TObject);
    procedure CB_NIVEL3ValidLookup(Sender: TObject);
    procedure CB_NIVEL2ValidLookup(Sender: TObject);
    procedure bbMostrarCalendario_DevExClick(Sender: TObject);
    procedure setEditarSoloActivos;
  private
    sPuesto, sClasifi :  string;
    lUsaPlazas : Boolean;
    procedure CalculaVencimiento;
    procedure SetCamposNivel;
    procedure SetControlesPlaza;
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
  protected
    procedure Connect; override;
    function GetClasifi : String;override;
  public
  end;

var
  KardexAlta_DevEx: TKardexAlta_DevEx;

{$ifdef ACS}
const
     K_ALT_DEF = 24;
     K_FORMA_ACS = 482;
{$endif}

implementation

uses DTablas,
     DGlobal,
     DSistema,
     DRecursos,
     DCatalogos,
     DCliente,
     ZetaDialogo,
     ZAccesosTress,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZAccesosMgr,
     ZetaCommonTools,
     ZGlobalTress;

{$R *.DFM}

procedure TKardexAlta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;

     CB_NIVEL12lbl.Top := CB_NIVEL1lbl.Top;
     CB_NIVEL11lbl.Top := CB_NIVEL12lbl.Top + K_ALT_DEF;
     CB_NIVEL10lbl.Top := CB_NIVEL11lbl.Top + K_ALT_DEF;
     CB_NIVEL9lbl.Top   := CB_NIVEL10lbl.Top + K_ALT_DEF;
     CB_NIVEL8lbl.Top  := CB_NIVEL9lbl.Top + K_ALT_DEF;
     CB_NIVEL7lbl.Top  := CB_NIVEL8lbl.Top + K_ALT_DEF;
     CB_NIVEL6lbl.Top  := CB_NIVEL7lbl.Top + K_ALT_DEF;
     CB_NIVEL5lbl.Top  := CB_NIVEL6lbl.Top + K_ALT_DEF;
     CB_NIVEL4lbl.Top  := CB_NIVEL5lbl.Top + K_ALT_DEF;
     CB_NIVEL3lbl.Top  := CB_NIVEL4lbl.Top + K_ALT_DEF;
     CB_NIVEL2lbl.Top  := CB_NIVEL3lbl.Top + K_ALT_DEF;
     CB_NIVEL1lbl.Top  := CB_NIVEL2lbl.Top + K_ALT_DEF;

     CB_NIVEL12.Top := CB_NIVEL1.Top;
     CB_NIVEL11.Top := CB_NIVEL12.Top + K_ALT_DEF;
     CB_NIVEL10.Top := CB_NIVEL11.Top + K_ALT_DEF;
     CB_NIVEL9.Top   := CB_NIVEL10.Top + K_ALT_DEF;
     CB_NIVEL8.Top  := CB_NIVEL9.Top + K_ALT_DEF;
     CB_NIVEL7.Top  := CB_NIVEL8.Top + K_ALT_DEF;
     CB_NIVEL6.Top  := CB_NIVEL7.Top + K_ALT_DEF;
     CB_NIVEL5.Top  := CB_NIVEL6.Top + K_ALT_DEF;
     CB_NIVEL4.Top  := CB_NIVEL5.Top + K_ALT_DEF;
     CB_NIVEL3.Top  := CB_NIVEL4.Top + K_ALT_DEF;
     CB_NIVEL2.Top  := CB_NIVEL3.Top + K_ALT_DEF;
     CB_NIVEL1.Top  := CB_NIVEL2.Top + K_ALT_DEF;

     CB_NIVEL12.TabOrder := 1;
     CB_NIVEL11.TabOrder := 2;
     CB_NIVEL10.TabOrder := 3;
     CB_NIVEL9.TabOrder := 4;
     CB_NIVEL8.TabOrder := 5;
     CB_NIVEL7.TabOrder := 6;
     CB_NIVEL6.TabOrder := 7;
     CB_NIVEL5.TabOrder := 8;
     CB_NIVEL4.TabOrder := 9;
     CB_NIVEL3.TabOrder := 10;
     CB_NIVEL2.TabOrder := 11;
     CB_NIVEL1.TabOrder := 12;

     Self.Height := K_FORMA_ACS;

     {
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     }
     {$endif}
     HelpContext:= H31311_Kardex_Alta;
     TipoValorActivo1 := stEmpleado;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     SetCamposNivel;


     CB_CONTRAT.LookupDataset := dmCatalogos.cdsContratos;
     CB_PATRON.LookupDataset := dmCatalogos.cdsRPatron;
     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     {$ifdef ACS}
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     {$endif}
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     CB_PLAZA.LookupDataset := dmRecursos.cdsPlazasLookup;
     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TKardexAlta_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl_DevEx.ActivePageIndex:= dmRecursos.TabKardex;
     Salarios2_DevEx.TabVisible := FALSE;
     Salarios2_DevEx.Visible :=    FALSE;
     General_DevEx.TabVisible := ZAccesosMgr.CheckDerecho( D_REPORTES_CONFIDENCIALES, K_DERECHO_CONSULTA );
     Otras_DevEx.TabVisible := General_DevEx.TabVisible;
     SetControlesPlaza;
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}

     CB_NOMINA.ListaFija:=lfTipoPeriodo; //acl
     {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$ifend}
end;

procedure TKardexAlta_DevEx.Connect;
begin
     with dmTablas do
     begin
          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
          cdsContratos.Conectar;
          cdsSsocial.Conectar;
//          cdsTPeriodos.Conectar;
     end;

     dmSistema.cdsUsuarios.Conectar;

     with dmRecursos do
     begin
          //cdsPlazas.Conectar;
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
          cdsKar_Fija.Refrescar;
     end;
     rRangoSal := CB_RANGO_S.Valor;           // Matsushita
{     if FAKECB_SALARIO.Visible then
        FAKECB_SALARIO.Valor := CB_SALARIO.Valor; }

     LlenaOtrasPer;
     LlenaKar_fija;
     sClasifi := CB_CLASIFI.Llave;
     sPuesto := CB_PUESTO.Llave;

     CB_PLAZA.Filtro := dmRecursos.FiltroPlazaVacantes(sPuesto,CB_PLAZA.Valor);
end;

procedure TKardexAlta_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     CalculaVencimiento;
     ZAntiguedad.Caption := TiempoDias( dmCliente.FechaDefault - CB_FEC_ANT.Valor + 1, etDias);
     CalcSalSemanal;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmRecursos.cdsEditHisKardex do
               bbMostrarCalendario_DevEx.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
end;

procedure TKardexAlta_DevEx.CalculaVencimiento;
begin
     {OP: 13.May.08}
     {Habilita / deshabilita control de CB_FEC_COV, dependiendo del tipo de
     contrato.}
     CB_FEC_COV.Enabled := ( dmRecursos.cdsEditHisKardex.FieldByName( 'CB_FEC_COV' ).AsDateTime <> NullDateTime );
end;

procedure TKardexAlta_DevEx.CB_CONTRATExit(Sender: TObject);
begin
     inherited;
     CalculaVencimiento;
end;

function TKardexAlta_DevEx.GetClasifi : String;
begin
     Result := CB_Clasifi.Llave;
end;

procedure TKardexAlta_DevEx.CB_AUTOSALClick(Sender: TObject);
begin
     inherited CB_AUTOSALClick(Sender);
end;

procedure TKardexAlta_DevEx.CB_PUESTOExit(Sender: TObject);
begin
     if StrLleno( CB_PUESTO.Llave ) then
     begin
          sPuesto := CB_PUESTO.LLave;
          if dmCliente.UsaPlazas then
          begin
//               dmRecursos.PuestoPlaza := CB_PUESTO.LLave;
               CB_PLAZA.Filtro := dmRecursos.FiltroPlazaVacantes(sPuesto, CB_PLAZA.Valor);
          end
          else if ( sPuesto <> CB_PUESTO.Llave ) then 
          begin
               sClasifi := dmCatalogos.cdsPuestos.FieldByName( 'PU_CLASIFI' ).AsString;
               if StrLleno( sClasifi ) then
               begin
                    CB_CLASIFI.Llave := sClasifi;
                    SetSalarioTabulador;
               end;
          end;
     end
     else if lUsaPlazas and ( self.ActiveControl.Name <> 'Cancelar' ) then
     begin
          ZInformation(Caption, 'El Puesto no puede quedar vac�o',0 );
          CB_PUESTO.SetFocus;
     end;
end;

procedure TKardexAlta_DevEx.CB_CLASIFIExit(Sender: TObject);
begin
     inherited;
     if ( sClasifi <> CB_CLASIFI.Llave ) then
     begin
          SetSalarioTabulador;
          sClasifi := CB_CLASIFI.Llave;
     end;
end;

procedure TKardexAlta_DevEx.Panel1Enter(Sender: TObject);
begin
    PageControl_DevEx.ActivePage := TabArea_DevEx;
    if StrLleno( Global.NombreNivel( 1 ) ) then
    begin
         if dmCliente.UsaPlazas and NOT CB_NIVEL1.Enabled then
            ActiveControl := Panel2_Plazas
         else
             ActiveControl := CB_NIVEL1;
    end;
end;

procedure TKardexAlta_DevEx.Panel2Enter(Sender: TObject);
begin
     if General_DevEx.TabVisible then
     begin
          PageControl_DevEx.ActivePage := General_DevEx;
          ActiveControl := CB_AUTOSAL;
     end
     else
         Panel5Enter( Sender );
end;

procedure TKardexAlta_DevEx.Panel2_plazasEnter(Sender: TObject);
begin
     if Not dmCliente.UsaPlazas then
        Panel2.SetFocus;     
end;

procedure TKardexAlta_DevEx.Panel3Enter(Sender: TObject);
begin
    PageControl_DevEx.ActivePage := Otras_DevEx;
    if LBDisponibles.Enabled then
       ActiveControl := LBDisponibles;
end;

procedure TKardexAlta_DevEx.Panel4Enter(Sender: TObject);
begin
    if LBDisponibles.Enabled then
    begin
         PageControl_DevEx.ActivePage := Notas_DevEx;
         ActiveControl := CB_NOTA;
    end;
end;

procedure TKardexAlta_DevEx.Panel5Enter(Sender: TObject);
begin
     PageControl_DevEx.ActivePage := Notas_DevEx;
     ActiveControl := CB_NOTA;
end;

procedure TKardexAlta_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

{procedure TKardexAlta_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;}

procedure TKardexAlta_DevEx.SetControlesPlaza;
begin

     //Si la plaza es 0 quiere decir que no la tiene asignada o que el movimiento es anterior
     //a cuando entr� en vigor el mecanismo de plaza, cuando el movto no es un reingreso.
     with dmRecursos.cdsEditHisKardex do
     begin
          if ( State = dsInsert ) then
          begin
               lUsaPlazas := dmCliente.UsaPlazas and
                             (FieldByName('CB_REINGRE').AsString = K_GLOBAL_SI);

          end
          else
          begin
               lUsaPlazas := dmCliente.UsaPlazas and
                             (FieldByName('CB_PLAZA').AsInteger <> 0);
          end;
     end;

     CB_PLAZA.Enabled := lUsaPlazas ;
     CB_CLASIFI.Enabled := NOT lUsaPlazas ;
     CB_TURNO.Enabled := NOT lUsaPlazas ;
     CB_PATRON.Enabled := NOT lUsaPlazas ;
     CB_NIVEL1.Enabled := NOT lUsaPlazas ;
     CB_NIVEL2.Enabled := NOT lUsaPlazas ;
     CB_NIVEL3.Enabled := NOT lUsaPlazas ;
     CB_NIVEL4.Enabled := NOT lUsaPlazas ;
     CB_NIVEL5.Enabled := NOT lUsaPlazas ;
     CB_NIVEL6.Enabled := NOT lUsaPlazas ;
     CB_NIVEL7.Enabled := NOT lUsaPlazas ;
     CB_NIVEL8.Enabled := NOT lUsaPlazas ;
     CB_NIVEL9.Enabled := NOT lUsaPlazas ;
     {$ifdef ACS}
     CB_NIVEL10.Enabled := NOT lUsaPlazas ;
     CB_NIVEL11.Enabled := NOT lUsaPlazas ;
     CB_NIVEL12.Enabled := NOT lUsaPlazas ;
     {$endif}
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TKardexAlta_DevEx.CB_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL11.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TKardexAlta_DevEx.CB_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{$ifdef ACS}
{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
procedure TKardexAlta_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 265;
     K_TOP_N1_LBL = 269;
     K_TOP_N2 = 241;
     K_TOP_N2_LBL = 245;
     K_TOP_N3 = 217;
     K_TOP_N3_LBL = 221;
     K_TOP_N4 = 193;
     K_TOP_N4_LBL = 197;
     K_TOP_N5 = 169;
     K_TOP_N5_LBL = 173;
     K_TOP_N6 = 145;
     K_TOP_N6_LBL = 149;
     K_TOP_N7 = 121;
     K_TOP_N7_LBL = 125;
     K_TOP_N8 = 97;
     K_TOP_N8_LBL = 101;
     K_TOP_N9 = 73;
     K_TOP_N9_LBL = 77;
     K_TOP_N10 = 49;
     K_TOP_N10_LBL = 53;
     K_TOP_N11 = 25;
     K_TOP_N11_LBL = 29;
     K_TOP_N12 = 1;
     K_TOP_N12_LBL = 5;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     {Se obtiene la cantidad total de niveles no visibles}
     iNivelesNoVisibles := 0;
     //iTotalHorizontal := 0;
     if Not CB_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     CB_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     CB_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     CB_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     CB_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     CB_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     CB_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     CB_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     CB_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     CB_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     CB_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     CB_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     CB_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     CB_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     CB_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     CB_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     CB_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     CB_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     CB_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     CB_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     CB_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     CB_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     CB_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     CB_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     CB_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TKardexAlta_DevEx.LimpiaLookUpOlfKey;
begin
          CB_NIVEL1.ResetMemory;
          CB_NIVEL2.ResetMemory;
          CB_NIVEL3.ResetMemory;
          CB_NIVEL4.ResetMemory;
          CB_NIVEL5.ResetMemory;
          CB_NIVEL6.ResetMemory;
          CB_NIVEL7.ResetMemory;
          CB_NIVEL8.ResetMemory;
          CB_NIVEL9.ResetMemory;
          CB_NIVEL10.ResetMemory;
          CB_NIVEL11.ResetMemory;
          CB_NIVEL12.ResetMemory;
end;
{$endif}

procedure TKardexAlta_DevEx.bbMostrarCalendario_DevExClick(
  Sender: TObject);
begin
  inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TKardexAlta_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     for I := 0 to TabArea_DevEx.ControlCount - 1 do
     begin
          if TabArea_DevEx.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabArea_DevEx.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabArea_DevEx.Controls[I].Width+2);
          end;
          if TabArea_DevEx.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( TabArea_DevEx.Controls[I].Visible ) then
               begin
                    TabArea_DevEx.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( TabArea_DevEx.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabArea_DevEx.Controls[I].Left := K_WIDTH_SMALL_LEFT;
               end;
          end;
     end;
end;
{$ifend}


procedure TKardexAlta_DevEx.SetEditarSoloActivos;
begin
     CB_CONTRAT.EditarSoloActivos := TRUE;
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_PLAZA.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     CB_PATRON.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
     CB_TABLASS.EditarSoloActivos := TRUE;
end;

end.



