inherited Evaluaciones_devEx: TEvaluaciones_devEx
  Caption = 'Evaluaciones'
  ClientWidth = 663
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 663
    inherited OK_DevEx: TcxButton
      Left = 498
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 578
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 663
    inherited ValorActivo2: TPanel
      Width = 337
      inherited textoValorActivo2: TLabel
        Width = 331
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 663
    Columns = <
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_CODIGO'
        ReadOnly = True
        Title.Caption = 'N'#250'mero'
        Width = 70
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PRETTYNAME'
        ReadOnly = True
        Title.Caption = 'Nombre'
        Width = 200
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IC_EVA_1'
        Title.Caption = 'Eval. 1'
        Width = 45
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IC_EVA_2'
        Title.Caption = 'Eval. 2'
        Width = 45
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IC_EVA_3'
        Title.Caption = 'Eval. 3'
        Width = 45
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'IC_EVA_FIN'
        Title.Caption = 'Eval. Fin'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IC_COMENTA'
        Title.Caption = 'Comentario'
        Width = 170
        Visible = True
      end>
  end
  inherited Panel1: TPanel
    Width = 663
    inherited lblFolio: TLabel
      Left = 540
    end
    inherited SE_FOLIO: TZetaDBTextBox
      Left = 567
    end
    inherited lblCupo: TLabel
      Left = 537
    end
    inherited SE_CUPO: TZetaDBTextBox
      Left = 567
    end
    inherited Label6: TLabel
      Left = 523
    end
    inherited ztbInscritos: TZetaTextBox
      Left = 567
    end
    inherited btnInscripciones: TcxButton
      Left = 520
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
