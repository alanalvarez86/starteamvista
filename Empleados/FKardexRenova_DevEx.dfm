inherited KardexRenova_DevEx: TKardexRenova_DevEx
  Left = 300
  Top = 207
  Caption = 'Cambio de Contrato'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl_DevEx: TcxPageControl
    inherited General_DevEx: TcxTabSheet
      Caption = 'Contrato'
      object Label4: TLabel
        Left = 36
        Top = 41
        Width = 94
        Height = 13
        Caption = 'Fecha Vencimiento:'
      end
      object Label3: TLabel
        Left = 48
        Top = 16
        Width = 82
        Height = 13
        Caption = 'Tipo de Contrato:'
      end
      object CB_FEC_COV: TZetaDBFecha
        Left = 136
        Top = 36
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '13/may/08'
        Valor = 39581.000000000000000000
        DataField = 'CB_FEC_COV'
        DataSource = DataSource
      end
      object CB_CONTRAT: TZetaDBKeyLookup_DevEx
        Left = 136
        Top = 12
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsContratos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_CONTRAT'
        DataSource = DataSource
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 112
    Top = 208
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
