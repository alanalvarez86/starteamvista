unit FGridGlobalCertific_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ZetaDBGrid, DBCtrls,
  ZetaSmartLists, Buttons, ExtCtrls, StdCtrls, ZetaKeyCombo, Mask,
  ZetaFecha, ZetaEdit, ZetaMessages, ZBaseGridEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TGridGlobalCertific_DevEx = class(TBaseGridEdicion_DevEx)
    PanelEmp: TPanel;
    CI_CODIGO: TZetaKeyLookup_DevEx;
    Label4: TLabel;
    KI_SINOD_1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    KI_SINOD_2: TEdit;
    KI_SINOD_3: TEdit;
    Label3: TLabel;
    KI_OBSERVA: TMemo;
    Label5: TLabel;
    zFecha: TZetaDBFecha;
    zComboSiNo: TZetaDBKeyCombo;
    btnAgregarCertificProg: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure btnAgregarCertificProgClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);

  private
    { Private declarations }
    procedure ValidaDatosCertific;
    procedure AsignaDatosCertific;
    procedure BuscaEmpleado;
    procedure WMExaminar(var Message: TMessage);message WM_EXAMINAR;
  public
    { Public declarations }
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  end;

var
  GridGlobalCertific_DevEx: TGridGlobalCertific_DevEx;

implementation

uses DCliente, dRecursos, dCatalogos,ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx,ZetaCommonTools,ZetaCommonLists;

{$R *.dfm}

{ TGridGlobalCertific }

procedure TGridGlobalCertific_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_EmpleadosCertificados;
     CI_CODIGO.LookupDataset := dmCatalogos.cdsCertificaciones;
     zComboSiNo.Lista.Add( K_GLOBAL_SI +'='+ K_PROPPERCASE_SI );
     zComboSiNo.Lista.Add( K_GLOBAL_NO +'='+ K_PROPPERCASE_NO );
end;


procedure TGridGlobalCertific_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmCatalogos.cdsCertificaciones.Conectar;
     with dmRecursos do
     begin
          cdsGridGlobalCertific.Refrescar;
          DataSource.DataSet := cdsGridGlobalCertific;
     end;
end;

procedure TGridGlobalCertific_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     CI_CODIGO.SetFocus;
end;

procedure TGridGlobalCertific_DevEx.AsignaDatosCertific;
begin
     with dmRecursos.cdsGridGlobalCertific do
     begin
          PostData;
          DisableControls;
          try
             First;
             while not EOF do
             begin
                  Edit;
                  FieldByName( 'CI_CODIGO' ).AsString := CI_CODIGO.Llave;
                  FieldByName( 'KI_SINOD_1' ).AsString := KI_SINOD_1.Text;
                  FieldByName( 'KI_SINOD_2' ).AsString := KI_SINOD_2.Text;
                  FieldByName( 'KI_SINOD_3' ).AsString := KI_SINOD_3.Text;
                  FieldByName( 'KI_OBSERVA' ).AsString := KI_OBSERVA.Text;
                  Next;
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TGridGlobalCertific_DevEx.ValidaDatosCertific;
begin
     if StrVacio( CI_CODIGO.Llave ) then
     begin
          ActiveControl := CI_CODIGO;
          DataBaseError( 'No Se Ha Especificado Una Certificación' );
     end;
end;

procedure TGridGlobalCertific_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmRecursos.cdsGridGlobalCertific do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridGlobalCertific_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

{Eventos}

procedure TGridGlobalCertific_DevEx.OKClick(Sender: TObject);
begin
     try
        ValidaDatosCertific;
        AsignaDatosCertific;
        inherited;
     except
        self.ModalResult:= mrNone;
        raise;
     end;
end;


procedure TGridGlobalCertific_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'KI_FEC_CER' ) and ( zFecha.Visible ) then
                  zFecha.Visible:= FALSE
               else if ( SelectedField.FieldName = 'KI_APROBO' ) and ( zComboSiNo.Visible ) then
                   zComboSiNo.Visible := FALSE
          end;
     end;
end;

procedure TGridGlobalCertific_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer;
          Column: TColumn; State: TGridDrawState);

   procedure AjustaControlEnCelda( oControl: TControl );
   begin
        with oControl do
        begin
             Left := Rect.Left + ZetaDBGrid.Left;
             Top := Rect.Top + ZetaDBGrid.Top;
             Width := Column.Width + 2;
             Visible := True;
        end;
   end;
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'KI_FEC_CER' ) then
             AjustaControlEnCelda( zFecha )
          else if ( Column.FieldName = 'KI_APROBO' ) then
             AjustaControlEnCelda( zComboSiNo )
     end;
end;

procedure TGridGlobalCertific_DevEx.KeyPress(var Key: Char);

   function EsControlGrid: Boolean;
   begin
        Result := ( self.ActiveControl = zFecha ) or ( self.ActiveControl = zComboSiNo );
   end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'KI_FEC_CER' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage( zFecha.Handle, WM_CHAR, Word( Key ), 0 );
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'KI_APROBO' ) then
               begin
                    if ( zComboSiNo.Enabled ) then
                    begin
                         zComboSiNo.SetFocus;
                         SendMessage( zComboSiNo.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end;
          end;
     end
     else if EsControlGrid and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
     begin
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
          end;
     end;
end;

procedure TGridGlobalCertific_DevEx.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) and
             ( StrVacio( dmRecursos.cdsGridGlobalCertific.FieldByName( 'CB_CODIGO' ).AsString ) ) then
          begin
               if OK_DevEx.Enabled then
                  OK_DevEx.SetFocus
               else
                  Cancelar_DevEx.SetFocus;
          end
          else
          begin
               ZetaDBGrid.SelectedIndex := PosicionaSiguienteColumna;
               if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) then
                  SeleccionaSiguienteRenglon;
          end;
     end;
end;



procedure TGridGlobalCertific_DevEx.btnAgregarCertificProgClick(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregarCertificacionesProgramadas(CI_CODIGO.Llave );
end;

procedure TGridGlobalCertific_DevEx.OK_DevExClick(Sender: TObject);
begin
  try
        ValidaDatosCertific;
        AsignaDatosCertific;
        inherited;
     except
        self.ModalResult:= mrNone;
        raise;
     end;
end;

end.
