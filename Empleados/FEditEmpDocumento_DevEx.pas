unit FEditEmpDocumento_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, Mask, ZetaKeyCombo, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditEmpDocumento_DevEx = class(TBaseEdicion_DevEx)
    bgDocumento: TGroupBox;
    DO_NOMBRE: TLabel;
    lblTipoArchivo: TLabel;
    Panel1: TPanel;
    DO_OBSERVA: TDBEdit;
    lblNoCredito: TLabel;
    lblTipoPresta: TLabel;
    AbrirDocumento: TOpenDialog;
    GrabaDocumento: TSaveDialog;
    DO_TIPO: TDBComboBox;
    dxBarButton_LeerDisco: TdxBarButton;
    dxBarButton_GrabarDisco: TdxBarButton;
    LeerDisco2_DevEx: TcxButton;
    btnVerDocumento_DevEx: TcxButton;
    GrabarDisco2_DevEx: TcxButton;
    procedure btnVerDocumentoClick(Sender: TObject);
    procedure btnEditarDocumentoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAgregarDocumentoClick(Sender: TObject);
    procedure LeerDiscoClick(Sender: TObject);
    procedure GrabarDiscoClick(Sender: TObject);
    procedure DO_TIPOKeyPress(Sender: TObject; var Key: Char);
    procedure OKClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure DialogoAgregaDocumento(const lAgregando: Boolean);
    procedure EnabledControlDocumento;
  protected
    procedure Connect; override;

  public
    { Public declarations }
  end;

var
  EditEmpDocumento_DevEx: TEditEmpDocumento_DevEx;

implementation

uses DRecursos,
     dSistema,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaClientTools,
     ZetaDialogo,
     ZetaCommonTools,
     ZetacommonLists,
     ZetaFilesTools,
     FEditDocumento_DevEx, DCliente;

{$R *.dfm}

procedure TEditEmpDocumento_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := DO_TIPO;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H00000_Expediente_Documento_Empleado;
     IndexDerechos := ZAccesosTress.D_EMP_CURR_DOCUMENTO;
     DO_NOMBRE.Caption := VACIO;
     lblTipoArchivo.Caption := VACIO;
end;

procedure TEditEmpDocumento_DevEx.btnEditarDocumentoClick(Sender: TObject);
begin
     inherited;
     if StrLleno( dmRecursos.cdsDocumento.FieldByName('DO_NOMBRE').AsString ) then
     begin
          DialogoAgregaDocumento( FALSE );
     end
     else ZetaDialogo.ZError( Caption, 'El Documento No Contiene Informaci�n', 0 );
end;

procedure TEditEmpDocumento_DevEx.DialogoAgregaDocumento(const lAgregando: Boolean);
begin
     with dmRecursos.cdsDocumento do
     begin
      //**    if FEditDocumento.EditarDocumento( lAgregando, FieldByName('DO_NOMBRE').AsString, H_DOCUMENTO_CONCEPTOS, dmRecursos.CargaDocumento ) then
          if FEditDocumento_DevEx.EditarDocumento( lAgregando, FieldByName('DO_NOMBRE').AsString, H_DOCUMENTO_CONCEPTOS, dmRecursos.CargaDocumento ) then
          begin
               EnabledControlDocumento;
               if state in [ dsinsert,dsedit ] then
                  Modo:= dsEdit;
          end;
     end;
end;

procedure TEditEmpDocumento_DevEx.EnabledControlDocumento;
const
     K_MAX_NOMBRE_LENGTH = 38;
var
     lEnabled : Boolean;
     sDescripcion : string;
     oColor : TColor;

     procedure VerificaExtensionGuardar( sExt : String );
     begin
          sExt := LowerCase( sExt );
          if ( sExt = 'pdf') then GrabaDocumento.FilterIndex := 1
          else if ( sExt = 'doc') then GrabaDocumento.FilterIndex := 2
          else if ( sExt = 'docx') then GrabaDocumento.FilterIndex := 3
          else if ( sExt = 'txt') then GrabaDocumento.FilterIndex := 4
          else if ( sExt = 'rtf') then GrabaDocumento.FilterIndex := 5
          else if ( sExt = 'xls') then GrabaDocumento.FilterIndex := 6
          else if ( sExt = 'xlsx') then GrabaDocumento.FilterIndex := 7
          else if ( sExt = 'html') then GrabaDocumento.FilterIndex := 8
          else if ( sExt = 'htm') then GrabaDocumento.FilterIndex := 9;
     end;

begin
     with dmRecursos.cdsDocumento do
     begin
          lEnabled := StrLleno( FieldByName('DO_NOMBRE').AsString );

          if lEnabled then //Hay Documento
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: '+ FieldByName('DO_NOMBRE').AsString;
               lblTipoArchivo.Caption := ZetaFilesTools.GetTipoDocumento( FieldByName('DO_EXT').AsString );

               VerificaExtensionGuardar( FieldByName('DO_EXT').AsString );
          end
          else //NO hay Documento
          begin
               with DO_NOMBRE do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay Ning�n Documento Almacenado';
                    lblTipoArchivo.Caption := VACIO;
               end;
          end;
     end;

     btnVerDocumento_DevEx.Enabled := lEnabled ;

     //LeerDisco.Enabled := not lEnabled;
     dxBarButton_GrabarDisco.Enabled := lEnabled;

     //LeerDisco2.Enabled := LeerDisco.Enabled;
     GrabarDisco2_DevEx.Enabled := dxBarButton_GrabarDisco.Enabled;

     with DO_NOMBRE do
     begin
          Font.Color := oColor;
          Hint := sDescripcion;

          if ( length( sDescripcion ) > K_MAX_NOMBRE_LENGTH ) then
             Caption :=  Copy ( sDescripcion, 0, K_MAX_NOMBRE_LENGTH-3 ) + '...'
          else
             Caption := sDescripcion;

     end;
end;



procedure TEditEmpDocumento_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     EnabledControlDocumento;
end;

procedure TEditEmpDocumento_DevEx.btnAgregarDocumentoClick(Sender: TObject);
begin
     inherited;
     if AbrirDocumento.Execute then
     begin
          with dmRecursos.cdsDocumento do
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
     end;
end;

procedure TEditEmpDocumento_DevEx.LeerDiscoClick(Sender: TObject);
var
   sDocumento, sExtension : String;
begin
     inherited;

     with dmRecursos.cdsDocumento do
     begin
          sDocumento := FieldByName('DO_NOMBRE').AsString;
          sExtension := FieldByName('DO_EXT').AsString;
     end;
     if ( StrVacio( sDocumento ) or ZetaDialogo.ZConfirm( Caption, '� Desea Sustituir el Documento: ' + sDocumento + ' por uno Nuevo ?', 0, mbNo ) )  then
          if AbrirDocumento.Execute then
          begin
               with dmRecursos.cdsDocumento do
               begin
                    if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                         Edit;
                    FieldByName('DO_NOMBRE').AsString := ExtractFileName(AbrirDocumento.Filename);
                    FieldByName('DO_EXT').AsString := StringReplace(ExtractFileExt(AbrirDocumento.Filename), '.', '', [rfReplaceAll]);
                    CargaDocumento( dmRecursos.cdsDocumento, AbrirDocumento.Filename,'DO_BLOB','DO_EXT');
                    FieldByName('CB_CODIGO').AsInteger := dmcliente.Empleado;

                    EnabledControlDocumento;
               end;
          end;
end;

procedure TEditEmpDocumento_DevEx.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     dmRecursos.AbreDocumentoExpediente;

end;

procedure TEditEmpDocumento_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsDocumento.Conectar;
          DataSource.DataSet:= cdsDocumento;
     end;
end;

procedure TEditEmpDocumento_DevEx.GrabarDiscoClick(Sender: TObject);
begin
     EnabledControlDocumento;
     GrabaDocumento.Filename := dmrecursos.cdsDocumento.fieldbyname('DO_NOMBRE').AsString;

     if ( GrabaDocumento.Execute ) and not ( ZetaFilesTools.GuardaDocumento( DataSource.DataSet, GrabaDocumento.Filename, 'DO_BLOB', 'DO_EXT' ) ) then
          ZetaDialogo.ZError( 'El Documento No fu� Guardado', 'El Documento No fu� Guardado', 0 );
end;

procedure TEditEmpDocumento_DevEx.DO_TIPOKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

procedure TEditEmpDocumento_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     EnabledControlDocumento;
end;

procedure TEditEmpDocumento_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  EnabledControlDocumento;
end;

end.
