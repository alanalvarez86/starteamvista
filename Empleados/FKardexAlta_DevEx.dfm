inherited KardexAlta_DevEx: TKardexAlta_DevEx
  Left = 892
  Top = 251
  Caption = 'Alta'
  ClientHeight = 404
  ClientWidth = 600
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 368
    Width = 600
    DesignSize = (
      600
      36)
    inherited OK_DevEx: TcxButton
      Left = 436
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 515
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 600
    inherited ValorActivo2: TPanel
      Width = 274
      inherited textoValorActivo2: TLabel
        Width = 268
      end
    end
  end
  inherited PageControl_DevEx: TcxPageControl
    Width = 600
    Height = 288
    Properties.ActivePage = TabContratacion_DevEx
    ClientRectBottom = 286
    ClientRectRight = 598
    object TabContratacion_DevEx: TcxTabSheet [0]
      Caption = 'Contrataci'#243'n'
      ImageIndex = 6
      object Label11: TLabel
        Left = 108
        Top = 235
        Width = 78
        Height = 13
        Caption = 'Tipo de N'#243'mina:'
      end
      object label99: TLabel
        Left = 157
        Top = 142
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plaza:'
      end
      object Label36: TLabel
        Left = 102
        Top = 211
        Width = 84
        Height = 13
        Caption = 'Registro Patronal:'
      end
      object CB_HORARIOlbl: TLabel
        Left = 155
        Top = 188
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_CLASIFIlbl: TLabel
        Left = 124
        Top = 165
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object CB_PUESTOlbl: TLabel
        Left = 150
        Top = 119
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object ZAntiguedad: TZetaTextBox
        Left = 328
        Top = 14
        Width = 177
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label34: TLabel
        Left = 97
        Top = 16
        Width = 89
        Height = 13
        Caption = 'Antig'#252'edad desde:'
      end
      object CB_NOMINA: TZetaDBKeyCombo
        Left = 194
        Top = 232
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 8
        ListaFija = lfTipoPeriodo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
        DataField = 'CB_NOMINA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object CB_PLAZA: TZetaDBKeyLookup_DevEx
        Left = 194
        Top = 138
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PLAZA'
        DataSource = DataSource
      end
      object Panel1: TPanel
        Left = 520
        Top = 104
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 9
        TabStop = True
        OnEnter = Panel1Enter
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 194
        Top = 115
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnExit = CB_PUESTOExit
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 194
        Top = 161
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnExit = CB_CLASIFIExit
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object CB_TURNO: TZetaDBKeyLookup_DevEx
        Left = 194
        Top = 184
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_PATRON: TZetaDBKeyLookup_DevEx
        Left = 194
        Top = 207
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsRPatron
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PATRON'
        DataSource = DataSource
      end
      object GroupBox1: TGroupBox
        Left = 96
        Top = 32
        Width = 409
        Height = 81
        Caption = 'Contrato'
        TabOrder = 1
        object CB_FEC_CONlbl: TLabel
          Left = 62
          Top = 13
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio:'
        end
        object CB_CONTRATlbl: TLabel
          Left = 63
          Top = 36
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo: '
        end
        object LVencimiento: TLabel
          Left = 29
          Top = 59
          Width = 61
          Height = 13
          Caption = 'Vencimiento:'
        end
        object CB_FEC_CON: TZetaDBFecha
          Left = 98
          Top = 8
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '16/dic/97'
          Valor = 35780.000000000000000000
          DataField = 'CB_FEC_CON'
          DataSource = DataSource
        end
        object CB_CONTRAT: TZetaDBKeyLookup_DevEx
          Left = 98
          Top = 32
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsContratos
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          OnExit = CB_CONTRATExit
          DataField = 'CB_CONTRAT'
          DataSource = DataSource
        end
        object CB_FEC_COV: TZetaDBFecha
          Left = 98
          Top = 55
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 2
          Text = '13/may/08'
          Valor = 39581.000000000000000000
          DataField = 'CB_FEC_COV'
          DataSource = DataSource
        end
      end
      object CB_FEC_ANT: TZetaDBFecha
        Left = 194
        Top = 11
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '16/dic/97'
        Valor = 35780.000000000000000000
        DataField = 'CB_FEC_ANT'
        DataSource = DataSource
      end
      object bbMostrarCalendario_DevEx: TcxButton
        Left = 496
        Top = 184
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = bbMostrarCalendario_DevExClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        PaintStyle = bpsGlyph
      end
    end
    object TabArea_DevEx: TcxTabSheet [1]
      Caption = 'Area'
      ImageIndex = 7
      object CB_NIVEL12lbl: TLabel
        Left = 116
        Top = 274
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 116
        Top = 250
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL10lbl: TLabel
        Left = 116
        Top = 228
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL9lbl: TLabel
        Left = 122
        Top = 204
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 122
        Top = 180
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 122
        Top = 156
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 122
        Top = 132
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 122
        Top = 108
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 122
        Top = 84
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 122
        Top = 60
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 122
        Top = 36
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL1lbl: TLabel
        Left = 122
        Top = 12
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 271
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL12ValidLookup
        DataField = 'CB_NIVEL12'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 247
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL11ValidLookup
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 224
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = CB_NIVEL10ValidLookup
        DataField = 'CB_NIVEL10'
      end
      object Panel2_plazas: TPanel
        Left = 480
        Top = 125
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 12
        TabStop = True
        OnEnter = Panel2_plazasEnter
      end
      object Panel2: TPanel
        Left = 472
        Top = 184
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 13
        TabStop = True
        OnEnter = Panel2Enter
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 8
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 32
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL2ValidLookup
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 56
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL2ValidLookup
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 80
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL4ValidLookup
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 104
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL5ValidLookup
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 128
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL6ValidLookup
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 152
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL7ValidLookup
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 176
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL8ValidLookup
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 168
        Top = 200
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CB_NIVEL9ValidLookup
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
    end
    inherited General_DevEx: TcxTabSheet
      inherited Label17: TLabel
        Left = 92
        Top = 167
      end
      inherited CB_SAL_SEM: TZetaTextBox
        Left = 424
        Top = 142
      end
      inherited Label5: TLabel
        Left = 339
        Top = 144
      end
      inherited CB_RANGO_SLbl: TLabel
        Left = 345
        Top = 81
      end
      inherited CB_SAL_TOT: TZetaDBTextBox
        Left = 424
        Top = 122
      end
      inherited CB_OTRAS_P: TZetaDBTextBox
        Left = 424
        Top = 101
      end
      inherited Label16: TLabel
        Left = 355
        Top = 124
      end
      inherited Label15: TLabel
        Left = 325
        Top = 103
      end
      inherited Label13: TLabel
        Left = 350
        Top = 56
      end
      inherited Label12: TLabel
        Left = 334
        Top = 33
      end
      inherited CB_SAL_INT: TZetaDBTextBox
        Left = 205
        Top = 142
      end
      inherited CB_TOT_GRA: TZetaDBTextBox
        Left = 205
        Top = 119
      end
      inherited CB_PRE_INT: TZetaDBTextBox
        Left = 205
        Top = 74
      end
      inherited CB_FAC_INT: TZetaDBTextBox
        Left = 205
        Top = 52
      end
      inherited Label10: TLabel
        Left = 118
        Top = 144
      end
      inherited Label9: TLabel
        Left = 73
        Top = 144
      end
      inherited Label8: TLabel
        Left = 73
        Top = 121
      end
      inherited Label7: TLabel
        Left = 118
        Top = 121
      end
      inherited Label6: TLabel
        Left = 73
        Top = 98
      end
      inherited Label4: TLabel
        Left = 73
        Top = 76
      end
      inherited Label3: TLabel
        Left = 79
        Top = 54
      end
      inherited OPLBl: TLabel
        Left = 93
        Top = 98
      end
      inherited Label35: TLabel
        Left = 134
        Top = 76
      end
      inherited CB_FAC_INTlbl: TLabel
        Left = 112
        Top = 54
      end
      inherited CB_SALARIOlbl: TLabel
        Left = 136
        Top = 33
      end
      inherited CB_TABLASS: TZetaDBKeyLookup_DevEx
        Left = 205
        Top = 163
        Enabled = True
      end
      inherited CB_RANGO_S: TZetaDBNumero
        Left = 424
        Top = 76
      end
      inherited CB_AUTOSAL: TDBCheckBox
        Left = 95
        Top = 9
      end
      inherited CB_ZONA_GE: TDBComboBox
        Left = 424
        Top = 29
      end
      inherited CB_FECHA_2: TZetaDBFecha
        Left = 424
        Top = 51
      end
      inherited CB_PER_VAR: TZetaDBNumero
        Left = 205
        Top = 94
      end
      inherited CB_SALARIO: TZetaDBNumero
        Left = 205
        Top = 29
      end
      inherited btnRecalculo_DevEx: TcxButton
        Left = 298
        Top = 50
      end
      object Panel3: TPanel
        Left = 480
        Top = 208
        Width = 50
        Height = 41
        BevelOuter = bvNone
        TabOrder = 8
        TabStop = True
        OnEnter = Panel3Enter
      end
    end
    inherited Otras_DevEx: TcxTabSheet
      inherited Label22: TLabel
        Left = 298
        Top = 14
      end
      inherited Label20: TLabel
        Left = 63
        Top = 14
      end
      inherited ZetaSmartListsButton4: TZetaSmartListsButton_DevEx
        Left = 495
        Top = 108
      end
      inherited ZetaSmartListsButton3: TZetaSmartListsButton_DevEx
        Left = 495
        Top = 75
      end
      inherited ZetaSmartListsButton2: TZetaSmartListsButton_DevEx
        Left = 263
        Top = 108
      end
      inherited ZetaSmartListsButton1: TZetaSmartListsButton_DevEx
        Left = 263
        Top = 75
      end
      inherited LBSeleccion: TZetaSmartListBox_DevEx
        Left = 298
        Top = 29
      end
      inherited LBDisponibles: TZetaSmartListBox_DevEx
        Left = 63
        Top = 29
      end
      inherited SBMas_DevEx: TcxButton
        Left = 495
        Top = 29
      end
      object Panel5: TPanel
        Left = 512
        Top = 142
        Width = 26
        Height = 33
        BevelOuter = bvNone
        TabOrder = 8
        TabStop = True
        OnEnter = Panel5Enter
      end
      object Panel4: TPanel
        Left = 512
        Top = 176
        Width = 26
        Height = 33
        BevelOuter = bvNone
        TabOrder = 7
        TabStop = True
        OnEnter = Panel4Enter
      end
    end
    inherited Notas_DevEx: TcxTabSheet
      inherited CB_NOTA: TcxDBMemo
        Height = 258
        Width = 596
      end
    end
    inherited Salarios2_DevEx: TcxTabSheet
      inherited CB_OLD_SAL: TZetaDBTextBox
        Left = 300
      end
      inherited CB_FEC_REV: TZetaDBTextBox
        Left = 300
      end
      inherited CB_OLD_INT: TZetaDBTextBox
        Left = 300
      end
      inherited CB_FEC_INT: TZetaDBTextBox
        Left = 300
      end
      inherited Label21: TLabel
        Left = 197
      end
      inherited Label19: TLabel
        Left = 200
      end
      inherited Label18: TLabel
        Left = 197
      end
      inherited Label14: TLabel
        Left = 218
      end
    end
    inherited Bitacora_DevEx: TcxTabSheet
      inherited GCapturoLbl: TLabel
        Left = 178
      end
      inherited GCapturaLbl: TLabel
        Left = 166
      end
      inherited GNivelLbl: TLabel
        Left = 194
      end
      inherited GStatusLbl: TLabel
        Left = 188
      end
      inherited CB_FEC_CAP: TZetaDBTextBox
        Left = 232
      end
      inherited CB_NIVEL: TZetaDBTextBox
        Left = 232
      end
      inherited Label2: TLabel
        Left = 188
      end
      inherited US_DESCRIP: TZetaDBTextBox
        Left = 232
      end
      inherited CB_STATUS: TZetaDBTextBox
        Left = 232
      end
      inherited CB_GLOBAL: TDBCheckBox
        Left = 232
      end
    end
    inherited MovimientoIDSE_DevEx: TcxTabSheet
      inherited GroupBox2: TGroupBox
        Left = 96
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 480
    Top = 88
  end
  inherited PanelFecha: TPanel
    Width = 600
  end
  inherited DataSource: TDataSource
    Left = 380
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited ZetaSmartLists: TZetaSmartLists_DevEx
    Left = 440
    Top = 120
  end
end
