inherited EditKardexCertificaciones_DevEx: TEditKardexCertificaciones_DevEx
  Left = 358
  Top = 188
  Caption = 'Certificaciones'
  ClientHeight = 531
  ClientWidth = 461
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 495
    Width = 461
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 296
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 375
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 461
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 135
      inherited textoValorActivo2: TLabel
        Width = 129
      end
    end
  end
  object PageControl: TcxPageControl [3]
    Left = 0
    Top = 50
    Width = 461
    Height = 445
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = TabGenerales
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 443
    ClientRectLeft = 2
    ClientRectRight = 459
    ClientRectTop = 28
    object TabGenerales: TcxTabSheet
      Caption = 'Generales'
      object lblFecha: TLabel
        Left = 58
        Top = 14
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
      end
      object Label1: TLabel
        Left = 55
        Top = 36
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 66
        Top = 60
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Folio:'
      end
      object Label3: TLabel
        Left = 20
        Top = 84
        Width = 71
        Height = 13
        Alignment = taRightJustify
        Caption = 'Renovar cada:'
      end
      object Label10: TLabel
        Left = 177
        Top = 84
        Width = 21
        Height = 13
        Alignment = taRightJustify
        Caption = 'd'#237'as'
      end
      object KI_FEC_CER: TZetaDBFecha
        Left = 95
        Top = 9
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '16/mar/06'
        Valor = 38792.000000000000000000
        DataField = 'KI_FEC_CER'
        DataSource = DataSource
      end
      object CI_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 95
        Top = 32
        Width = 333
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CI_CODIGO'
        DataSource = DataSource
      end
      object KI_FOLIO: TZetaDBEdit
        Left = 95
        Top = 56
        Width = 308
        Height = 21
        TabOrder = 2
        Text = 'KI_FOLIO'
        DataField = 'KI_FOLIO'
        DataSource = DataSource
      end
      object KI_RENOVAR: TZetaDBNumero
        Left = 95
        Top = 80
        Width = 77
        Height = 21
        Mascara = mnDias
        TabOrder = 3
        Text = '0'
        DataField = 'KI_RENOVAR'
        DataSource = DataSource
      end
      object DBCheckBox1: TDBCheckBox
        Left = 52
        Top = 104
        Width = 56
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Aprob'#243':'
        DataField = 'KI_APROBO'
        DataSource = DataSource
        TabOrder = 4
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object GroupBox1: TGroupBox
        Left = 18
        Top = 128
        Width = 408
        Height = 81
        Caption = 'Certificaci'#243'n #1'
        TabOrder = 5
        object Label4: TLabel
          Left = 32
          Top = 24
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object Label5: TLabel
          Left = 15
          Top = 48
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calificaci'#243'n:'
        end
        object KI_SINOD_1: TDBEdit
          Left = 76
          Top = 20
          Width = 308
          Height = 21
          DataField = 'KI_SINOD_1'
          DataSource = DataSource
          TabOrder = 0
        end
        object KI_CALIF_1: TZetaDBNumero
          Left = 76
          Top = 44
          Width = 77
          Height = 21
          Mascara = mnNumeroGlobal
          TabOrder = 1
          Text = '0.00'
          DataField = 'KI_CALIF_1'
          DataSource = DataSource
        end
      end
      object GroupBox2: TGroupBox
        Left = 18
        Top = 216
        Width = 408
        Height = 81
        Caption = 'Certificaci'#243'n #2'
        TabOrder = 6
        object Label6: TLabel
          Left = 32
          Top = 24
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object Label7: TLabel
          Left = 15
          Top = 48
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calificaci'#243'n:'
        end
        object KI_SINOD_2: TDBEdit
          Left = 76
          Top = 20
          Width = 308
          Height = 21
          DataField = 'KI_SINOD_2'
          DataSource = DataSource
          TabOrder = 0
        end
        object KI_CALIF_2: TZetaDBNumero
          Left = 76
          Top = 44
          Width = 77
          Height = 21
          Mascara = mnNumeroGlobal
          TabOrder = 1
          Text = '0.00'
          DataField = 'KI_CALIF_2'
          DataSource = DataSource
        end
      end
      object GroupBox3: TGroupBox
        Left = 18
        Top = 304
        Width = 408
        Height = 81
        Caption = 'Certificaci'#243'n #3'
        TabOrder = 7
        object Label8: TLabel
          Left = 32
          Top = 24
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object Label9: TLabel
          Left = 15
          Top = 48
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calificaci'#243'n:'
        end
        object KI_SINOD_3: TDBEdit
          Left = 76
          Top = 20
          Width = 308
          Height = 21
          DataField = 'KI_SINOD_3'
          DataSource = DataSource
          TabOrder = 0
        end
        object KI_CALIF_3: TZetaDBNumero
          Left = 76
          Top = 44
          Width = 77
          Height = 21
          Mascara = mnNumeroGlobal
          TabOrder = 1
          Text = '0.00'
          DataField = 'KI_CALIF_3'
          DataSource = DataSource
        end
      end
    end
    object Observaciones: TcxTabSheet
      Caption = 'Observaciones'
      ImageIndex = 1
      object KI_OBSERVA: TDBMemo
        Left = 0
        Top = 0
        Width = 453
        Height = 416
        Align = alClient
        DataField = 'KI_OBSERVA'
        DataSource = DataSource
        TabOrder = 0
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
