inherited HisEvaluaCompeten_DevEx: THisEvaluaCompeten_DevEx
  Left = 217
  Top = 322
  Caption = 'Competencias Evaluadas'
  ClientHeight = 406
  ClientWidth = 1082
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1082
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 756
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1082
    Height = 387
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CC_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CC_CODIGO'
        MinWidth = 70
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Competencia'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 95
      end
      object NC_DESCRIP: TcxGridDBColumn
        Caption = 'Nivel Conseguido'
        DataBinding.FieldName = 'NC_DESCRIP'
        MinWidth = 115
      end
      object EC_FECHA: TcxGridDBColumn
        Caption = 'Registrado'
        DataBinding.FieldName = 'EC_FECHA'
        MinWidth = 90
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_DESCRIP'
        MinWidth = 80
      end
      object EC_COMENT: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'EC_COMENT'
        MinWidth = 110
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 888
    Top = 32
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end