unit FEditEmpFoto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, StdCtrls, DBCtrls, ZetaDBTextBox, Db, ExtCtrls, Buttons, ExtDlgs,
  Mask, TDMULTIP, ZetaSmartLists;

type
  TEditEmpFoto = class(TBaseEdicion)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    IM_TIPO: TDBComboBox;
    IM_OBSERVA: TDBEdit;
    PanelExtra: TPanel;
    LeerDisco: TSpeedButton;
    GrabarDisco: TSpeedButton;
    BtnLeerTwain: TSpeedButton;
    OpenPictureDialog: TOpenPictureDialog;
    SavePictureDialog: TSavePictureDialog;
    BtnSeleccionarTwain: TSpeedButton;
    Documento: TPDBMultiImage;
    procedure LeerDiscoClick(Sender: TObject);
    procedure GrabarDiscoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnLeerTwainClick(Sender: TObject);
    procedure BtnSeleccionarTwainClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure IM_TIPOKeyPress(Sender: TObject; var Key: Char);
  private
    FTieneTwain: Boolean;
    FRevisaTwain: Boolean;
  protected
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
  end;

var
  EditEmpFoto: TEditEmpFoto;

implementation

uses dRecursos, ZetaCommonClasses, ZAccesosTress, JPEG, ZetaDialogo,
     DLL95V1, ZetaCommonLists, FToolsFoto, DGlobal, ZGlobalTress, ZetaCommonTools;

{$R *.DFM}

procedure TEditEmpFoto.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_CURR_FOTO;
     FirstControl := IM_TIPO;
     HelpContext:= H10122_Expediente_Foto_empleado;
     FRevisaTwain:= True;
     TipoValorActivo1 := stEmpleado;
end;

procedure TEditEmpFoto.Connect;
begin
     with dmRecursos do
     begin
          cdsEmpFoto.Conectar;
          DataSource.DataSet:= cdsEmpFoto;
     end;
end;

procedure TEditEmpFoto.LeerDiscoClick(Sender: TObject);
begin
     inherited;
     if OpenPictureDialog.Execute then
     begin
          with dmRecursos.cdsEmpFoto do
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
          Documento.LoadFromFile( OpenPictureDialog.FileName );
     end;
end;

procedure TEditEmpFoto.GrabarDiscoClick(Sender: TObject);
begin
     inherited;
     SavePictureDialog.FilterIndex := 0;
     SavePictureDialog.FileName := '';
     if SavePictureDialog.Execute then
        with Documento do
             case SavePictureDialog.FilterIndex of
                  1,2 :
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'JPG';
                       SaveToFileAsJPG(SavePictureDialog.FileName);
                  end;
                  3:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'BMP';
                       SaveToFileAsBMP(SavePictureDialog.FileName);
                  end;
                  4:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'GIF';
                       SaveToFileAsGIF(SavePictureDialog.FileName);
                  end;
                  5:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'TIF';
                       SaveToFileAsTIF(SavePictureDialog.FileName);
                  end;
                  6:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PCX';
                       SaveToFileAsPCX(SavePictureDialog.FileName);
                  end;
                  7:
                  begin
                       if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PNG';
                       SaveToFileAsPNG(SavePictureDialog.FileName);
                  end;
             end;
end;

procedure TEditEmpFoto.BtnLeerTwainClick(Sender: TObject);
begin
     if FRevisaTwain then
     begin
          FRevisaTwain:= False;
          FTieneTwain:= HasTwain( Handle );
     end;
     if FTieneTwain then
     begin
         try
            with dmRecursos.cdsEmpFoto do
                 if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                    Edit;
            with Documento do
            begin
                 UseTwainWindow:= True;
                 ScanImage( Handle );
//                 UpdateAsJPG:= True;
                 Refresh;
            end;
            // Modo := meModificacion;
         except
            ZError('','No se pudo obtener la fotograf�a - Error en Dispositivo Twain',0);
         end;
     end;
end;

procedure TEditEmpFoto.BtnSeleccionarTwainClick(Sender: TObject);
begin
     inherited;
     try
        Documento.SelectScanner(Handle);
     except
        ZError('','No se pudo Seleccionar - Error en Dispositivo Twain',0);
     end;
end;

procedure TEditEmpFoto.OKClick(Sender: TObject);
begin
{ Ya no es necesario, esto lo hace EscribirCambios OP: 25/06/08
     with Documento do
     begin
          CutToClipboard;
          UpdateAsJPG:=True;
          PastefromClipboard;
     end;
}
     inherited;
end;

procedure TEditEmpFoto.IM_TIPOKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = ZetaCommonClasses.UnaCOMILLA ) then
        Key := Chr( 0 );
     inherited KeyPress( Key );
end;

{OP: 04/06/08}
procedure TEditEmpFoto.EscribirCambios;
begin
         //acl1
     if ( strLleno( dmRecursos.cdsEmpFoto.FieldByName('IM_BLOB').AsString ) ) then
     begin
       {OP: 24/06/08}
          with Documento do
          begin
               CutToClipboard;
               UpdateAsJPG:=True;
               PastefromClipboard;
          end;
          try
          if( Not Global.GetGlobalBooleano( K_GLOBAL_CONSERVAR_TAM_FOTOS ) )then
              if( Documento.Picture.Bitmap.Width > 300 ) then
                  FToolsFoto.ResizeImageBestFit( Documento, 300, 300 );
       except
             on Error: Exception do
             begin
                  ZError( self.Caption,'No se pudo ajustar el tama�o de la fotograf�a. Se guardar� con su tama�o original'+CR_LF+Error.Message, 0);
             end;
       end;
     end;
     inherited EscribirCambios;
end;

end.
