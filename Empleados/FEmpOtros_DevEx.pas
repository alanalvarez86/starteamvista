unit FEmpOtros_DevEx;

interface

uses                                       
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, StdCtrls, DBCtrls, ZetaDBTextBox, Db, ExtCtrls,
  ComCtrls, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxPC,
  cxScrollBox, Menus, cxButtons, cxContainer, cxEdit, cxGroupBox,
  dxBarBuiltInMenu;

type
  TEmpOtros_DevEx = class(TBaseConsulta)
    pcOtros: TcxPageControl;
    tsInfonavit: TcxTabSheet;
    tsAsistencia: TcxTabSheet;
    tsCuentas: TcxTabSheet;
    tsFonacot: TcxTabSheet;
    LblDescuento: TLabel;
    CB_FONACOT: TZetaDBTextBox;
    Label9: TLabel;
    tsPPrimerEmpleo: TcxTabSheet;
    tsEvaluacion: TcxTabSheet;
    CB_EMPLEO: TDBCheckBox;
    LEvalua: TLabel;
    LProxima: TLabel;
    CB_EVALUA: TZetaDBTextBox;
    CB_NEXT_EV: TZetaDBTextBox;
    CB_LAST_EV: TZetaDBTextBox;
    LFechaEva: TLabel;
    tsMisDatos: TcxTabSheet;
    btnBorrarNIP: TcxButton;
    Label3: TLabel;
    CB_INF_INI: TZetaDBTextBox;
    CB_INF_ANTLbl: TLabel;
    CB_INF_ANT: TZetaDBTextBox;
    LCredito: TLabel;
    CB_INFCRED: TZetaDBTextBox;
    Label1: TLabel;
    ZDES_TP_PRE: TZetaTextBox;
    LDescuento: TLabel;
    CB_INFTASA: TZetaDBTextBox;
    CB_INFDISM: TDBCheckBox;
    CB_INFACT: TDBCheckBox;
    Label4: TLabel;
    CB_INF_OLD: TZetaDBTextBox;
    CB_INFMANT: TDBCheckBox;
    CB_BAN_ELE: TZetaDBTextBox;
    LBanca: TLabel;
    Label8: TLabel;
    CB_CTA_GAS: TZetaDBTextBox;
    Label7: TLabel;
    CB_CTA_VAL: TZetaDBTextBox;
    Label6: TLabel;
    CB_SUB_CTA: TZetaDBTextBox;
    lbNeto: TLabel;
    CB_NETO: TZetaDBTextBox;
    tsBrigada: TcxTabSheet;
    tsDatosMedicos: TcxTabSheet;
    lblTSangre: TLabel;
    CB_TSANGRE: TZetaDBTextBox;
    lblAlerPadec: TLabel;
    CB_ALERGIA: TZetaDBTextBox;
    tsConfidencialidad: TcxTabSheet;
    CB_NIVEL0: TZetaDBTextBox;
    LblNivel0: TLabel;
    sbInfonavit: TcxScrollBox;
    sbFonacot: TcxScrollBox;
    sbPPrimerEmpleo: TcxScrollBox;
    sbEvaluacion: TcxScrollBox;
    sbCuentas: TcxScrollBox;
    sbConfidencialidad: TcxScrollBox;
    sbDatosMedicos: TcxScrollBox;
    sbMisDatos: TcxScrollBox;
    sbBrigada: TcxScrollBox;
    CB_BRG_ACT: TDBCheckBox;
    lblTipoBrigada: TLabel;
    CB_BRG_TIP: TZetaTextBox;
    lblNoPisoBrigada: TLabel;
    CB_BRG_NOP: TZetaDBTextBox;
    CB_BRG_CON: TDBCheckBox;
    CB_BRG_PRA: TDBCheckBox;
    CB_BRG_ROL: TDBCheckBox;
    CB_BRG_JEF: TDBCheckBox;
    lblRol: TLabel;
    lblConocimiento: TLabel;
    cxGroupBox1: TcxGroupBox;
    Label10: TLabel;
    CB_BANCO: TZetaDBTextBox;
    TB_ELEM_BAN: TZetaDBTextBox;
    tsTimbrado: TcxTabSheet;
    sbTimbrado: TcxScrollBox;
    Label11: TLabel;
    dsRegimenContratTrabajador: TDataSource;
    sbAsistencia: TcxScrollBox;
    LTipoCreden: TLabel;
    CB_CREDENC: TZetaDBTextBox;
    Label2: TLabel;
    CB_CHECA: TDBCheckBox;

    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnBorrarNIPClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pcOtrosChange(Sender: TObject);
  private
    procedure SetControlesNivel0;
    procedure SetControlesNeto;
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeImprimir( var sMensaje: String ): Boolean; override;
    function GetDerechoTab  : integer; 
    function ChecaDerechoTab( iTipoDerecho : integer ) : Boolean; 
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpOtros_DevEx: TEmpOtros_DevEx;

implementation

uses dRecursos,
     dTablas,
     dSistema,
     FEditEmpOtros_DevEx,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZImprimeForma,
     ZAccesosTress,
     ZAccesosMgr,
     DGlobal,
     ZetaDialogo,
     ZGlobalTress, DCliente;

{$R *.DFM}

procedure TEmpOtros_DevEx.FormCreate(Sender: TObject);
begin
     inherited;

     tsInfonavit.Tag :=  D_EMP_DATOS_OTROS_INFONAVIT;
     tsFonacot.Tag :=    D_EMP_DATOS_OTROS_FONACOT;
     tsPPrimerEmpleo.Tag :=  D_EMP_DATOS_OTROS_PRIMER_EMPLEO;
     tsAsistencia.Tag :=  D_EMP_DATOS_OTROS_ASISTENCIA;
     tsEvaluacion.Tag :=  D_EMP_DATOS_OTROS_EVALUACION;
     tsCuentas.Tag := D_EMP_DATOS_OTROS_CUENTAS;
     tsConfidencialidad.Tag :=  D_EMP_DATOS_OTROS_NIVEL0;
     tsDatosMedicos.Tag := D_EMP_DATOS_OTROS_MEDICOS;
     tsBrigada.Tag :=      D_EMP_DATOS_OTROS_BRIGADAS;
     tsTimbrado.Tag :=      D_EMP_DATOS_OTROS_TIMBRADO;

     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10115_Otros_datos_empleado;
     SetControlesNivel0;
     SetControlesNeto;

     //Verificar constante de AVENT
     // Protecci�n Civil 2013
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          // GBInfonavit.Visible := False;
          tsInfonavit.TabVisible := False;
          // GBFonacot.Visible := False;
          tsFonacot.TabVisible := False;
          // GBEmpleo.Visible := False;
          tsPPrimerEmpleo.TabVisible := False;
          // GBAsistencia.Top := GBInfonavit.Top;
          // GBEvaluacion.Top := GBAsistencia.Top;
          // GBNomina.Top := GBAsistencia.Top + GBAsistencia.Height + 4;
     end;//if  }
     {$ifdef DOS_CAPAS}
     // grpMisDatos.Visible := False;
     // btnBorrarNip.Enabled := false;
     tsMisDatos.TabVisible := false;
     {$else}
     // Protecci�n Civil 2013
     // grpMisDatos.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_ADICIONAL10 );
     btnBorrarNip.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_MISDATOS, K_DERECHO_ADICIONAL10 );
     {$endif}

     // sbInfonavit
     sbInfonavit.Parent := tsInfonavit;
     sbInfonavit.OnDblClick := Self.OnDblClick;
     // sbFonacot
     sbFonacot.Parent := tsFonacot;
     sbFonacot.OnDblClick := Self.OnDblClick;
     // sbPPrimerEmpleo
     sbPPrimerEmpleo.Parent := tsPPrimerEmpleo;
     sbPPrimerEmpleo.OnDblClick := Self.OnDblClick;
     // sbAsistencia
     sbAsistencia.Parent := tsAsistencia;
     sbAsistencia.OnDblClick := Self.OnDblClick;
     // sbEvaluacion
     sbEvaluacion.Parent := tsEvaluacion;
     sbEvaluacion.OnDblClick := Self.OnDblClick;
     // sbCuentas
     sbCuentas.Parent := tsCuentas;
     sbCuentas.OnDblClick := Self.OnDblClick;
     // sbConfidencialidad
     sbConfidencialidad.Parent := tsConfidencialidad;
     sbConfidencialidad.OnDblClick := Self.OnDblClick;
     // sbDatosMedicos
     sbDatosMedicos.Parent := tsDatosMedicos;
     sbDatosMedicos.OnDblClick := Self.OnDblClick;
     // sbBrigada
     sbBrigada.Parent := tsBrigada;
     sbBrigada.OnDblClick := Self.OnDblClick;
     // sbMisDatos
     sbMisDatos.Parent := tsMisDatos;
     sbMisDatos.OnDblClick := Self.OnDblClick;
     // sbTimbrado
     sbTimbrado.Parent := tsTimbrado;
     sbTimbrado.OnDblClick := Self.OnDblClick;

end;

procedure TEmpOtros_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;

     with dmTablas do
     begin
          cdsRegimenContrataTrabajador.Conectar;
          cdsRegimenContrataTrabajador.locate( 'TL_VALOR', DataSource.DataSet.FieldByName( 'CB_REGIMEN' ).AsInteger , [] );
          dsRegimenContratTrabajador.DataSet:= cdsRegimenContrataTrabajador;
     end;
end;

procedure TEmpOtros_DevEx.FormShow(Sender: TObject);
begin
  inherited;
      { V2013
      Protecci�n Civil.
      2013-03-19. AL. }

      // Verificar constante de AVENT
      if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
         pcOtros.ActivePage := tsAsistencia
      else
          pcOtros.ActivePage := tsInfonavit;

      // Mostrar  Tab Sheet con derecho a consulta
      tsInfonavit.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_INFONAVIT, K_DERECHO_CONSULTA );
      tsFonacot.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_FONACOT, K_DERECHO_CONSULTA );
      tsPPrimerEmpleo.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_PRIMER_EMPLEO, K_DERECHO_CONSULTA );
      tsAsistencia.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_CONSULTA );
      tsEvaluacion.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_EVALUACION,  K_DERECHO_CONSULTA );
      tsCuentas.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CONSULTA );
      tsConfidencialidad.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_NIVEL0, K_DERECHO_CONSULTA );
      tsDatosMedicos.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_MEDICOS, K_DERECHO_CONSULTA );
      tsBrigada.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_BRIGADAS, K_DERECHO_CONSULTA );
      tsTimbrado.TabVisible := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_TIMBRADO, K_DERECHO_CONSULTA );

      Self.IndexDerechos :=  GetDerechoTab;
end;

procedure TEmpOtros_DevEx.Refresh;
begin
     dmRecursos.cdsDatosEmpleado.Refrescar;

     dmTablas.cdsRegimenContrataTrabajador.Refrescar;
end;

function TEmpOtros_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpOtros_DevEx.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandar� llamar la Alta
end;

function TEmpOtros_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpOtros_DevEx.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;    // Se mandar� llamar la Baja
end;

procedure TEmpOtros_DevEx.Modificar;
begin
     ShowFormaEdicionOtros( GetDerechoTab );
     //ZBaseEdicion.ShowFormaEdicion( EditEmpOtros, TEditEmpOtros );
end;

procedure TEmpOtros_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     ZDES_TP_PRE.Caption:= ObtieneElemento( lfTipoInfonavit, DataSource.DataSet.FieldByName( 'CB_INFTIPO' ).AsInteger );

     { V2013
     Protecci�n Civil }
     CB_BRG_TIP.Caption := ObtieneElemento( lfTipoBrigada, DataSource.DataSet.FieldByName( 'CB_BRG_TIP' ).AsInteger );
end;

procedure TEmpOtros_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEmpOtros_DevEx.SetControlesNivel0;
begin
     CB_NIVEL0.Visible := dmSistema.HayNivel0;
     LblNivel0.Visible := CB_NIVEL0.Visible;
end;

procedure TEmpOtros_DevEx.SetControlesNeto;
begin
     lbNeto.Visible := dmRecursos.PuedeVerNeto;
     CB_NETO.Visible := lbNeto.Visible;

     { V2013
     2013-03-18. AL. }
     { if NOT lbNeto.Visible then
     begin
          if CB_NIVEL0.Visible then
          begin
               LblNivel0.Top := lbNeto.Top;
               CB_NIVEL0.Top := CB_neto.Top;
          end;
     end; }
end;

procedure TEmpOtros_DevEx.btnBorrarNIPClick(Sender: TObject);
begin
  	inherited;
    {$ifndef DOS_CAPAS}
    dmSistema.ReiniciaNIP;
    zInformation('Borrar NIP Mis Datos','Se Borro NIP de Empleado, Puede Accesar de nuevo a Mis Datos',0);
    {$endif}
end;

function TEmpOtros_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := VACIO;
     Result := ChecaDerechoTab( K_DERECHO_CAMBIO );

     if not Result then
        sMensaje := 'No es posible modificar datos '+ pcOtros.ActivePage.Caption +' del Empleado';
end;

function TEmpOtros_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result := ChecaDerechoTab( K_DERECHO_IMPRESION );

     if not Result then
        sMensaje := 'No es posible imprimir datos '+ pcOtros.ActivePage.Caption +' del Empleado';
end;

function TEmpOtros_DevEx.ChecaDerechoTab(iTipoDerecho: integer): Boolean;
var
   iDerecho : integer;
begin
      Result := FALSE;
      iDerecho := GetDerechoTab;

      if ( iDerecho <> D_EMP_DATOS_OTROS ) then
            Result := ZAccesosMgr.CheckDerecho( iDerecho, iTipoDerecho );

      if ( iDerecho = D_EMP_DATOS_OTROS_INFONAVIT ) then
           Result := ZAccesosMgr.CheckDerecho( iDerecho, K_DERECHO_CONSULTA );

end;

procedure TEmpOtros_DevEx.pcOtrosChange(Sender: TObject);
begin
  inherited;
  Self.IndexDerechos :=  GetDerechoTab;
end;

function TEmpOtros_DevEx.GetDerechoTab: integer;
begin
     Result := pcOtros.ActivePage.Tag;
end;


end.
