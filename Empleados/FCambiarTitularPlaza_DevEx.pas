unit FCambiarTitularPlaza_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Mask, ZetaFecha, Buttons,
  ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons,
  ZetaKeyLookup_DevEx, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo;

type
  TCambiarTitularPlaza_DevEx = class(TZetaDlgModal_DevEx)
    Label3: TLabel;
    Empleado: TZetaKeyLookup_DevEx;
    lblFecha: TLabel;
    Fecha: TZetaFecha;
    lblObservaciones: TLabel;
    Observaciones: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EmpleadoValidKey(Sender: TObject);
  private
    { Private declarations }
    procedure SetEmpleadoActual(const iValue: Integer);
  public
    { Public declarations }
    property EmpleadoActual: Integer write SetEmpleadoActual;
  end;

var
   CambiarTitularPlaza_DevEx: TCambiarTitularPlaza_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaDialogo,
     dCliente, dRecursos;

{$R *.DFM}

procedure TCambiarTitularPlaza_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with Empleado do
     begin
          LookupDataset := dmCliente.cdsEmpleadoLookUp;
          Llave := VACIO;
     end;
     Fecha.Valor := dmCliente.FechaDefault;
end;

procedure TCambiarTitularPlaza_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          Fecha.Visible := UsaPlazas;
          lblFecha.Visible := UsaPlazas;
          Observaciones.Visible := UsaPlazas;
          lblObservaciones.Visible := UsaPlazas;
          if not UsaPlazas then
             Self.Height := 114;
     end;

     Self.ActiveControl := Empleado;
end;

procedure TCambiarTitularPlaza_DevEx.OKClick(Sender: TObject);
var
   iEmpleado, iPlazaActual : Integer;
   sNombre,sNomPlaza: String;
begin
     inherited;
     iEmpleado := Empleado.Valor;
     if  ( iEmpleado > 0 ) then
     begin
          with dmCliente.cdsEmpleadoLookup do
          begin
               if dmCliente.UsaPlazas then
               begin
                    iPlazaActual := FieldByName( 'CB_PLAZA' ).AsInteger;
                    sNomPlaza := VACIO;
               end
               else
               begin
                    with dmRecursos.cdsEmpPlaza do
                    begin
                          Locate('CB_CODIGO',iEmpleado,[]);
                          iPlazaActual := FieldByName( 'PL_FOLIO' ).AsInteger;
                          sNomPlaza := ' : '+FieldByName( 'PL_NOMBRE' ).AsString;
                    end;
               end;
               sNombre := FieldByName( 'PRETTYNAME' ).AsString;
          end;
          if ZetaDialogo.ZConfirm( self.Caption, Format( 'El empleado %d:%s', [ iEmpleado, sNombre ] ) + CR_LF +
                                                 Format( 'est� asignado a la plaza #%d %s', [ iPlazaActual,sNomPlaza ] ) + CR_LF +
                                                 '� Seguro de intercambiar plazas ?', 0, mbYes ) then
          begin
               if dmRecursos.CambiarTitularPlaza( iEmpleado, iPlazaActual, Fecha.Valor, Observaciones.Lines.Text ) then
                  self.Close;
          end;
     end
     else
     begin
          ZetaDialogo.zError( self.Caption, 'Debe especificar el nuevo titular', 0 );
          Empleado.SetFocus;
     end;
end;

procedure TCambiarTitularPlaza_DevEx.SetEmpleadoActual(const iValue: Integer);
const
     K_FILTRO_LOOKUP = '( CB_PLAZA > 0 ) and ( CB_CODIGO <> %d )';
var
   sFiltro:String;
begin
     sFiltro := K_FILTRO_LOOKUP;
     if not dmCliente.UsaPlazas then
        sFiltro := '( CB_CODIGO <> %d )';

     Empleado.Filtro := Format( sFiltro, [ iValue ] );
end;

procedure TCambiarTitularPlaza_DevEx.EmpleadoValidKey(Sender: TObject);
begin
     inherited;
     if ( Empleado.Valor = dmRecursos.cdsPlazas.FieldByName( 'CB_CODIGO' ).AsInteger ) then
     begin
          ZetaDialogo.ZError( self.Caption, 'Debe seleccionarse un nuevo titular', 0 );
          Empleado.SetFocus;
     end
     {Cuando  CB_PLAZA es Nil significa que no ha seleccionado ningun empleado y viene del cancel}
     else if (( dmCliente.cdsEmpleadoLookup.FindField('CB_PLAZA') <> nil ) and
              ( dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_PLAZA' ).AsInteger = 0 ) and
              ( dmCliente.UsaPlazas ) )  or
             ( ( not dmCliente.UsaPlazas )and
               ( not dmRecursos.cdsEmpPlaza.Locate('CB_CODIGO', Empleado.Valor,[] ) )
             )then
     begin
          ZetaDialogo.ZError( self.Caption, 'El nuevo titular debe tener plaza asignada', 0 );
          Empleado.SetFocus;
     end;
end;

end.
