inherited GridEmpTools_DevEx: TGridEmpTools_DevEx
  Left = 557
  Top = 116
  BorderIcons = [biMaximize]
  Caption = 'Entregar Herramienta'
  ClientHeight = 367
  ClientWidth = 556
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 331
    Width = 556
    inherited OK_DevEx: TcxButton
      Left = 388
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 468
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 556
    inherited ValorActivo2: TPanel
      Width = 230
      inherited textoValorActivo2: TLabel
        Width = 224
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 104
    Width = 556
    Height = 227
    TabOrder = 4
    Columns = <
      item
        Expanded = False
        FieldName = 'TO_CODIGO'
        Title.Caption = 'Herramienta'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TO_DESCRIP'
        ReadOnly = True
        Title.Caption = 'Descripci'#243'n'
        Width = 205
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KT_REFEREN'
        Title.Caption = 'Referencia'
        Width = 105
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'KT_TALLA'
        Title.Caption = 'Talla'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KT_COMENTA'
        Title.Caption = 'Observaciones'
        Width = 350
        Visible = True
      end>
  end
  object PanelEmp: TPanel [4]
    Left = 0
    Top = 50
    Width = 556
    Height = 54
    Align = alTop
    TabOrder = 2
    object EmpleadoLbl: TLabel
      Left = 20
      Top = 9
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empleado:'
      FocusControl = CB_CODIGO
    end
    object FechaLbl: TLabel
      Left = 37
      Top = 30
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Fecha:'
      FocusControl = KT_FEC_INI
    end
    object CB_CODIGO: TZetaKeyLookup_DevEx
      Left = 73
      Top = 4
      Width = 375
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
    object KT_FEC_INI: TZetaFecha
      Left = 73
      Top = 27
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '19/dic/97'
      Valor = 35783.000000000000000000
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
