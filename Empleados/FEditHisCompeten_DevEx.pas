unit FEditHisCompeten_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db, Buttons,
  ExtCtrls,ZetaDBTextBox, ZetaKeyCombo, ZetaNumero,
  ZetaFecha, ZetaSmartLists,Variants, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TEditHisCompetencia_DevEx = class(TBaseEdicion_DevEx)
    Label13: TLabel;
    Label5: TLabel;
    UsuarioLbl: TLabel;
    ECC_COMENT: TDBEdit;
    CC_CODIGO: TZetaDBKeyLookup_DevEx;
    ECC_FECHA: TZetaDBTextBox;
    Label2: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    Label1: TLabel;
    NC_NIVEL: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure CC_CODIGOValidKey(Sender: TObject);
  private
    procedure AplicarFiltroNivel;

  protected
   procedure Connect;override;
  public
  end;

var
  EditHisCompetencia_DevEx: TEditHisCompetencia_DevEx;


implementation

uses DTablas,
     DCatalogos,
     {$IFNDEF SUPERVISORES}
     DRecursos,
     {$ELSE}
     DSuper,
     {$ENDIF}
     DSistema,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZGlobalTress;

{$R *.DFM}



procedure TEditHisCompetencia_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := ZetaCommonLists.stEmpleado;
     {$IFNDEF SUPERVISORES}
     IndexDerechos := ZAccesosTress.D_EMP_PLANCAPACITA;
     {$ELSE}
     IndexDerechos := ZAccesosTress.D_SUP_PLAN_CAPACITA;
     {$ENDIF}
     FirstControl := CC_CODIGO;
     with CC_CODIGO do
     begin
          LookupDataSet := dmCatalogos.cdsCompetencias;
     end;
     HelpContext:= H_Empleados_Edit_His_Competen;

end;

procedure TEditHisCompetencia_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     {$IFNDEF SUPERVISORES}
     with dmRecursos do
     {$ELSE}
     with dmSuper do
     {$ENDIF}
     begin
          cdsHisCompeten.Conectar;
          Datasource.DataSet := cdsHisCompeten;
     end;
     WITH dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          AplicarFiltroNivel;
          NC_NIVEL.LookupDataset := cdsCompNiveles;
          if StrLleno(NC_NIVEL.Llave)then
          begin
               cdsCompNiveles.Locate('CC_CODIGO;NC_NIVEL',VarArrayOf([CC_CODIGO.Llave,NC_NIVEL.Valor]),[]);
               NC_NIVEL.SetLlaveDescripcion(NC_NIVEL.Llave,cdsCompNiveles.FieldByName('NC_DESCRIP').AsString);
          end;
     end;

end;

procedure TEditHisCompetencia_DevEx.CC_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     NC_NIVEL.Enabled := (CC_CODIGO.Llave <> VACIO );
     AplicarFiltroNivel;

     with  {$IFNDEF SUPERVISORES}dmRecursos{$ELSE}dmSuper{$endif}.cdsHisCompeten.FieldByName('CC_CODIGO') do
     begin
          if ( OldValue <> NewValue )then
          begin
               NC_NIVEL.Llave := VACIO;
          end;
     end;
end;

procedure TEditHisCompetencia_DevEx.AplicarFiltroNivel;
begin
     if StrLleno(CC_CODIGO.Llave) then
     begin
          NC_NIVEL.Filtro := Format(' CC_CODIGO = ''%s''',[dmCatalogos.cdsCompetencias.FieldByName('CC_CODIGO').AsString]);
     end
     else
         NC_NIVEL.Filtro := VACIO;
end;

end.
