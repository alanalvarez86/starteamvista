inherited HisNominas_DevEx: THisNominas_DevEx
  Left = 218
  Top = 119
  Caption = 'Historial de N'#243'minas'
  ClientWidth = 494
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 494
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 168
      inherited textoValorActivo2: TLabel
        Width = 162
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 494
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object PE_TIPO: TcxGridDBColumn
        Caption = 'N'#243'mina'
        DataBinding.FieldName = 'PE_TIPO'
        Width = 64
      end
      object PE_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'PE_NUMERO'
        Width = 64
      end
      object NO_HORAS: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'NO_HORAS'
        Width = 64
      end
      object NO_EXTRAS: TcxGridDBColumn
        Caption = 'Extras'
        DataBinding.FieldName = 'NO_EXTRAS'
        Width = 64
      end
      object NO_PERCEPC: TcxGridDBColumn
        Caption = 'Percepciones'
        DataBinding.FieldName = 'NO_PERCEPC'
        Width = 64
      end
      object NO_DEDUCCI: TcxGridDBColumn
        Caption = 'Deducciones'
        DataBinding.FieldName = 'NO_DEDUCCI'
        Width = 64
      end
      object NO_NETO: TcxGridDBColumn
        Caption = 'Neto'
        DataBinding.FieldName = 'NO_NETO'
        Width = 64
      end
      object NO_FEC_PAG: TcxGridDBColumn
        Caption = 'Pagado'
        DataBinding.FieldName = 'NO_FEC_PAG'
        Width = 64
      end
      object NO_USR_PAG: TcxGridDBColumn
        Caption = 'Captur'#243
        DataBinding.FieldName = 'NO_USR_PAG'
        Width = 64
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
