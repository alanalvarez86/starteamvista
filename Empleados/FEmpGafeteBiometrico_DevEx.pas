unit FEmpGafeteBiometrico_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, Mask, Db,
  ZetaDBTextBox, ExtCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions, ZBaseConsulta;

type
  TEmpGafeteBiometrico_DevEx = class(TBaseConsulta)
    CB_ID_NUM: TZetaDBTextBox;
    lblNumeroTarjeta: TLabel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CB_ID_BIO: TZetaDBTextBox;
    lblNumeroBiometrico: TLabel;
    lblCB_GP_COD: TLabel;
    CB_GP_COD: TZetaDBTextBox;
    lblCantidad_Huellas: TLabel;
    CANTIDAD_HUELLAS: TZetaDBTextBox;
    chkTieneHuella: TDBCheckBox;
    LTipoCreden: TLabel;
    CB_CREDENC: TZetaDBTextBox;
    GroupBox3: TGroupBox;
    procedure FormCreate(Sender: TObject);
  private
  protected
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpGafeteBiometrico_DevEx: TEmpGafeteBiometrico_DevEx;

implementation

uses FEditEmpGafeteBiometrico_DevEx, ZAccesosTress, ZAccesosMgr, ZImprimeForma, ZetaTipoEntidad,
    ZetaCommonLists, ZetaCommonClasses, dRecursos, dSistema, dCliente;

{$R *.DFM}

procedure TEmpGafeteBiometrico_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     //HelpContext:= H10114_Datos_percepciones_empleado;

     lblNumeroBiometrico.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL11 ); // SYNERGY
     lblCB_GP_COD.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL11 ); // SYNERGY
     lblNumeroBiometrico.Visible := True; // SYNERGY
     lblCB_GP_COD.Visible := True; // SYNERGY
     CB_ID_BIO.DataField := 'CB_ID_BIO'; // SYNERGY
     CB_GP_COD.DataField := 'CB_GP_COD'; // SYNERGY
     CB_ID_BIO.Visible := True; // SYNERGY
     CB_GP_COD.Visible := True; // SYNERGY
     CB_ID_BIO.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     CB_GP_COD.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     chkTieneHuella.Visible := True;

     lblCantidad_Huellas.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL12 );
     lblCantidad_Huellas.Visible := TRUE;
     CANTIDAD_HUELLAS.Visible := True;
     CANTIDAD_HUELLAS.Enabled := lblCantidad_Huellas.Enabled;
end;

procedure TEmpGafeteBiometrico_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;

     {$ifndef DOS_CAPAS}
     chkTieneHuella.Checked := dmSistema.TieneHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger , dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger);
     CANTIDAD_HUELLAS.Caption := IntToStr (dmSistema.CantidadHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger , dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger, 0));
     {$endif}
end;

procedure TEmpGafeteBiometrico_DevEx.Refresh;
begin
     {$ifndef DOS_CAPAS}
     chkTieneHuella.Checked := dmSistema.TieneHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger , dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger);
     CANTIDAD_HUELLAS.Caption := IntToStr (dmSistema.CantidadHuellaEmpleado( DataSource.DataSet.FieldByName( 'CB_CODIGO' ).AsInteger , dmCliente.cdsEmpleado.FieldByName( 'CB_ID_BIO' ).AsInteger, 0));
     {$endif}
end;


procedure TEmpGafeteBiometrico_DevEx.Agregar;
begin
     ShowFormaEdicionOtros( D_EMP_DATOS_GAFETE_BIOMETRICO );
     Refresh;
end;

function TEmpGafeteBiometrico_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se puede borrar en Gafete y biométrico';
end;

procedure TEmpGafeteBiometrico_DevEx.Modificar;
begin
     ShowFormaEdicionOtros( D_EMP_DATOS_GAFETE_BIOMETRICO );
     Refresh;
end;

procedure TEmpGafeteBiometrico_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;
end.
