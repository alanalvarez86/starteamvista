unit FEmpExperiencia;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, StdCtrls, ZetaDBTextBox, DBCtrls,
  ExtCtrls;

type
  TEmpExperiencia = class(TBaseConsulta)
    DataSource1: TDataSource;
    GroupBox1: TGroupBox;
    LEstudios: TLabel;
    ZCB_ESTUDIO: TZetaTextBox;
    CB_EST_HOY: TDBCheckBox;
    CB_EST_HOR: TZetaDBTextBox;
    CB_EST_HORlbl: TLabel;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    CB_HABLA: TDBCheckBox;
    CB_IDIOMA: TZetaDBTextBox;
    CB_IDIOMAlbl: TLabel;
    LMaquinas: TLabel;
    CB_MAQUINA: TZetaDBTextBox;
    CB_EXPERIE: TZetaDBTextBox;
    LExperiencia: TLabel;
    Label3: TLabel;
    CB_ESCUELA: TZetaDBTextBox;
    Label4: TLabel;
    Label5: TLabel;
    CB_YTITULO: TZetaDBTextBox;
    Label6: TLabel;
    LCarrera: TLabel;
    CB_CARRERA: TZetaDBTextBox;
    ZCB_TESCUEL: TZetaTextBox;
    ZCB_TITULO: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpExperiencia: TEmpExperiencia;

implementation

{$R *.DFM}

uses dRecursos, dTablas, ZetaCommonLists, ZetaCommonClasses,
     ZBaseEdicion_DevEx, ZetaTipoEntidad,ZImprimeForma,FEditEmpExperiencia_DevEx,
  DCliente;

procedure TEmpExperiencia.Connect;
begin
     with dmRecursos, dmTablas do
     begin
          cdsEstudios.Conectar;
          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
end;

procedure TEmpExperiencia.Refresh;
begin
     dmRecursos.cdsDatosEmpleado.Refrescar;
end;

procedure TEmpExperiencia.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10124_Expediente_Experiencia_empleado;
end;

function TEmpExperiencia.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpExperiencia.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandar� llamar la Alta
end;

function TEmpExperiencia.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpExperiencia.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;    // Se mandar� llamar la Baja
end;

procedure TEmpExperiencia.Modificar;
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpExperiencia_DevEx, TEditEmpExperiencia_DevEx )
end;

procedure TEmpExperiencia.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmRecursos.cdsDatosEmpleado do
     begin
          ZCB_ESTUDIO.Caption := dmTablas.cdsEstudios.GetDescripcion( FieldByName( 'CB_ESTUDIO' ).AsString );
          ZCB_TESCUEL.Caption := ObtieneElemento(lfTipoInstitucionSTPS,FieldByName( 'CB_TESCUEL' ).AsInteger);
          ZCB_TITULO.Caption := ObtieneElemento(lfTipoDocProbatorioSTPS ,FieldByName( 'CB_TITULO' ).AsInteger);
     end;
end;

procedure TEmpExperiencia.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

end.
