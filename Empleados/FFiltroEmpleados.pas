unit FFiltroEmpleados;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, DBCtrls, Db, DBGrids,
     {$ifndef ver130}
     variants,
     {$endif}
     ZetaClientDataset,
     ZetaCommonLists,ZetaCommonClasses,
     ZBaseDlgModal, ZetaKeyLookup, ZetaEdit;

type
  TFiltroEmpleados = class(TZetaDlgModal)
    FiltrosGB: TGroupBox;
    sFiltroLBL: TLabel;
    sCondicionLBl: TLabel;
    sFiltro: TMemo;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    BFinal: TSpeedButton;
    BInicial: TSpeedButton;
    BLista: TSpeedButton;
    RBTodos: TRadioButton;
    RBRango: TRadioButton;
    RBLista: TRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    ECondicion: TZetaKeyLookup;
    BAgregaCampo: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);

  private
    { Private declarations }
    FTipoRango: eTipoRangoActivo;
    FEmpleadoCodigo: String;
    FEmpleadoFiltro: String;
    FVerificacion: Boolean;
    FParameterList: TZetaParams;
    FDescripciones: TZetaParams;
    function BuscaEmpleado(const lConcatena: Boolean; const sLlave: String): String;
    procedure DescargarParametros;

  protected
    { Protected declarations }
    property EmpleadoCodigo: String read FEmpleadoCodigo write FEmpleadoCodigo;
    property EmpleadoFiltro: String read FEmpleadoFiltro write FEmpleadoFiltro;
    property Verificacion: Boolean read FVerificacion write FVerificacion;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );

  public
     procedure CargaParametros;
     property ParameterList: TZetaParams read FParameterList write FParameterList;
     property Descripciones: TZetaParams read FDescripciones write FDescripciones;
    { Public declarations }
    property TipoRango: eTipoRangoActivo read FTipoRango write FTipoRango;
    procedure SetRangoLista(var RangoI,RangoF, Lista,Condicion: string);
    procedure GetRangoLista(var RangoI, RangoF, Lista,Condicion: string);
  end;


var
  FiltroEmpleados: TFiltroEmpleados;


implementation

uses ZetaDialogo,
     DCliente, dCatalogos,ZetaClientTools,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaBuscaEmpleado,
     ZConstruyeFormula;

{$R *.DFM}

procedure TFiltroEmpleados.FormShow(Sender: TObject);
begin
     inherited;
     FEmpleadoCodigo := 'COLABORA' + '.CB_CODIGO';
     FEmpleadoFiltro := '';
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;
     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
     DescargarParametros;
end;

{procedure TFiltroEmpleados.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;
  }
procedure TFiltroEmpleados.EnabledBotones( const eTipo: eTipoRangoActivo );
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

procedure TFiltroEmpleados.DescargarParametros;
begin
     with FParameterList do
     begin
          EnabledBotones(FTipoRango);
          RBTodos.Checked := (FTipoRango = raTodos);
          RBRango.Checked := (FTipoRango = raRango);
          RBLista.Checked := (FTipoRango = raLista);


          if FindParam('Filtro')<>  NIL then
          begin
               if StrLleno(ParamByName('Filtro').AsString )then
               begin
                     sFiltro.Text := ParamByName('Filtro').AsString;
               end;
          end;
     end;
end;

function TFiltroEmpleados.GetRango: String;
begin

     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := '@'+GetFiltroLista( FEmpleadoCodigo, Trim( ELista.Text ) );
     else
         Result := '';
     end;
end;


function TFiltroEmpleados.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
     end
     else
         Result := '';
end;

function TFiltroEmpleados.GetFiltro: String;
begin
     Result := Trim( sFiltro.Text );
end;

procedure TFiltroEmpleados.CargaParametros;
begin

     with FParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

end;

function TFiltroEmpleados.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
     begin
          if lConcatena and ZetaCommonTools.StrLleno( Text ) then
             Result := sLlave + ',' + sKey
          else
              Result := sKey;
     end;
end;

{ ********** Eventos ******** }

procedure TFiltroEmpleados.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TFiltroEmpleados.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TFiltroEmpleados.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

procedure TFiltroEmpleados.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TFiltroEmpleados.BFinalClick(Sender: TObject);
begin
     with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TFiltroEmpleados.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TFiltroEmpleados.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, Text, SelStart, evBase );
     end;
end;


procedure TFiltroEmpleados.OKClick(Sender: TObject);
begin
     inherited;
     CargaParametros;
     Close;
end;

procedure TFiltroEmpleados.CancelarClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TFiltroEmpleados.SetRangoLista(var RangoI,RangoF, Lista,Condicion: string);
begin
     Elista.Text := Lista;
     EInicial.Text := RangoI;
     EFinal.Text := RangoF;
     ECondicion.Llave := Condicion;

end;

procedure TFiltroEmpleados.GetRangoLista(var RangoI,RangoF, Lista,Condicion: string);
begin
     Lista := Elista.Text ;
     RangoI := EInicial.Text;
     RangoF := EFinal.Text;
     Condicion := ECondicion.Llave;
end;

end.
