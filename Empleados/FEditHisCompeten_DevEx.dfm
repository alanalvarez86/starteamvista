inherited EditHisCompetencia_DevEx: TEditHisCompetencia_DevEx
  Left = 1987
  Top = 152
  Caption = 'Competencia Individual'
  ClientHeight = 235
  ClientWidth = 409
  PixelsPerInch = 96
  TextHeight = 13
  object Label13: TLabel [0]
    Left = 35
    Top = 68
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Competencia:'
  end
  object Label5: TLabel [1]
    Left = 26
    Top = 116
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
    FocusControl = ECC_COMENT
  end
  object UsuarioLbl: TLabel [2]
    Left = 57
    Top = 142
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
  end
  object ECC_FECHA: TZetaDBTextBox [3]
    Left = 104
    Top = 162
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'ECC_FECHA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'ECC_FECHA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [4]
    Left = 45
    Top = 164
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modificado:'
  end
  object US_DESCRIP: TZetaDBTextBox [5]
    Left = 104
    Top = 140
    Width = 145
    Height = 17
    AutoSize = False
    Caption = 'US_DESCRIP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label1: TLabel [6]
    Left = 73
    Top = 92
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel:'
  end
  inherited PanelBotones: TPanel
    Top = 199
    Width = 409
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 243
      Top = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 322
      Top = 5
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 409
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 83
      inherited textoValorActivo2: TLabel
        Width = 77
      end
    end
  end
  object ECC_COMENT: TDBEdit [10]
    Left = 104
    Top = 112
    Width = 275
    Height = 21
    DataField = 'ECC_COMENT'
    DataSource = DataSource
    MaxLength = 40
    TabOrder = 2
  end
  object CC_CODIGO: TZetaDBKeyLookup_DevEx [11]
    Left = 104
    Top = 64
    Width = 300
    Height = 21
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    OnValidKey = CC_CODIGOValidKey
    DataField = 'CC_CODIGO'
    DataSource = DataSource
  end
  object NC_NIVEL: TZetaDBKeyLookup_DevEx [12]
    Left = 104
    Top = 88
    Width = 300
    Height = 21
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    DataField = 'NC_NIVEL'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 302
    Top = 46
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
