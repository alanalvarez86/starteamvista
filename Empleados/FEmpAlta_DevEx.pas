unit FEmpAlta_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, DBCtrls, Db, Buttons,
     ExtCtrls, ComCtrls, Grids, DBGrids, DBCGrids, ToolWin,
     {$ifndef VER130}Variants,{$endif}
     ZetaFecha,
     ZetaNumero,
     ZetaKeyLookup,
     ZetaCommonClasses,
     ZetaDBTextBox,
     ZetaSmartLists,
     ZetaKeyCombo,
     ZetaDialogo, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC,
  ZetaKeyLookup_DevEx, ZetaSmartLists_DevEx, cxContainer, cxEdit, cxListBox,
  cxGroupBox, dxBarBuiltInMenu;

type
  TEmpAlta_DevEx = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    TabIdentifica: TcxTabSheet;
    TabPersonales: TcxTabSheet;
    TabResidencia: TcxTabSheet;
    TabContratacion: TcxTabSheet;
    TabVacacion: TcxTabSheet;
    DiasGB: TGroupBox;
    FechaCierreLbl: TLabel;
    LCB_APE_PAT: TLabel;
    LCB_APE_MAT: TLabel;
    LCB_NOMBRES: TLabel;
    LCB_FEC_NAC: TLabel;
    LRFC: TLabel;
    LCB_SEGSOC: TLabel;
    LCB_CURP: TLabel;
    lblEmpleado: TLabel;
    lblSexo: TLabel;
    CB_APE_PAT: TDBEdit;
    CB_APE_MAT: TDBEdit;
    CB_NOMBRES: TDBEdit;
    CB_CURP: TDBEdit;
    CB_CODIGO: TZetaDBNumero;
    CB_FEC_NAC: TZetaDBFecha;
    CB_RFC: TDBEdit;
    CB_SEGSOC: TDBEdit;
    LLugNac: TLabel;
    CB_LUG_NAC: TDBEdit;
    LNacio: TLabel;
    CB_NACION: TDBEdit;
    CB_PASAPor: TDBCheckBox;
    LEdoCivil: TLabel;
    LCB_LA_MAT: TLabel;
    CB_LA_MAT: TDBEdit;
    LTransporte: TLabel;
    CB_MED_TRA: TZetaDBKeyLookup_DevEx;
    CB_EDO_CIV: TZetaDBKeyLookup_DevEx;
    LDireccion: TLabel;
    LColonia: TLabel;
    LCiudad: TLabel;
    LCP: TLabel;
    LEstado: TLabel;
    LZona: TLabel;
    LTelefono: TLabel;
    CB_ESTADO: TZetaDBKeyLookup_DevEx;
    CB_CALLE: TDBEdit;
    CB_COLONIA: TDBEdit;
    CB_CIUDAD: TDBEdit;
    CB_ZONA: TDBEdit;
    CB_CODPOST: TDBEdit;
    CB_TEL: TDBEdit;
    TabExperiencia: TcxTabSheet;
    Label34: TLabel;
    CB_FEC_ANT: TZetaDBFecha;
    ZAntiguedad: TZetaTextBox;
    GroupBox1: TcxGroupBox;
    CB_FEC_CONlbl: TLabel;
    CB_CONTRATlbl: TLabel;
    LVencimiento: TLabel;
    CB_FEC_CON: TZetaDBFecha;
    CB_CONTRAT: TZetaDBKeyLookup_DevEx;
    CB_PUESTOlbl: TLabel;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_TURNOlbl: TLabel;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_PATRONlbl: TLabel;
    CB_PATRON: TZetaDBKeyLookup_DevEx;
    CB_CLASIFIlbl: TLabel;
    TabSalario: TcxTabSheet;
    CB_SALARIOlbl: TLabel;
    CB_PER_VARlbl: TLabel;
    CB_TABLASSlbl: TLabel;
    CB_SALARIO: TZetaDBNumero;
    CB_PER_VAR: TZetaDBNumero;
    CB_TABLASS: TZetaDBKeyLookup_DevEx;
    CB_ZONA_GE: TDBComboBox;
    CB_ZONA_GElbl: TLabel;
    CB_AUTOSAL: TDBCheckBox;
    TabPrestacion: TcxTabSheet;
    TabOtros: TcxTabSheet;
    GBAsistencia: TGroupBox;
    LTipoCreden: TLabel;
    CB_CHECA: TDBCheckBox;
    CB_CREDENC: TDBEdit;
    TabArea: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    Label18: TLabel;
    CB_FEC_ING: TZetaDBFecha;
    PagadosLbl: TLabel;
    GozadosLbl: TLabel;
    ZCierre: TZetaTextBox;
    LViveCon: TLabel;
    CB_VIVECON: TZetaDBKeyLookup_DevEx;
    LHabita: TLabel;
    CB_VIVEEN: TZetaDBKeyLookup_DevEx;
    Label19: TLabel;
    ZYear: TZetaNumero;
    Label11: TLabel;
    ZMonth: TZetaNumero;
    Label21: TLabel;
    NombrePadre: TEdit;
    NombreMadre: TEdit;
    Label23: TLabel;
    Label24: TLabel;
    ZetaSmartLists: TZetaSmartLists_DevEx;
    ListEmp: TcxListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Label20: TLabel;
    LBDisponibles: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton2: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton1: TZetaSmartListsButton_DevEx;
    LBSeleccion: TZetaSmartListBox_DevEx;
    Label22: TLabel;
    ZetaSmartListsButton3: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton4: TZetaSmartListsButton_DevEx;
    Panel11: TPanel;
    CB_SEXO: TZetaKeyCombo;
    bDigitoVerificador: TcxButton;
    CB_DER_PAG: TZetaDBNumero;
    CB_DER_GOZ: TZetaDBNumero;
    CB_RANGO_SLbl: TLabel;
    CB_RANGO_S: TZetaDBNumero;
    LEdad: TLabel;
    ZEdad: TZetaTextBox;
    bbMostrarCalendario: TcxButton;
    BtnCandidato: TcxButton;
    GBEvaluacion: TGroupBox;
    lblFechaEv: TLabel;
    lblResultadoEv: TLabel;
    lblProximaEv: TLabel;
    CB_LAST_EV: TZetaDBFecha;
    CB_EVALUA: TZetaDBNumero;
    CB_NEXT_EV: TZetaDBFecha;
    Panel12: TPanel;
    lblEntidad: TLabel;
    CB_ENT_NAC: TZetaDBKeyLookup_DevEx;
    CB_COD_COL: TZetaDBKeyLookup_DevEx;
    LCB_CLINICA: TLabel;
    CB_CLINICA: TDBEdit;
    PrimaLbl: TLabel;
    CB_DER_PV: TZetaDBNumero;
    CB_PLAZALBL: TLabel;
    CB_PLAZA: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    CB_NOMINA: TZetaDBKeyCombo;
    CB_DISCAPA: TDBCheckBox;
    CB_INDIGE: TDBCheckBox;
    GBFonacot: TGroupBox;
    CB_FONACOT: TDBEdit;
    Label5: TLabel;
    GBEmpleo: TGroupBox;
    CB_EMPLEO: TDBCheckBox;
    CB_FEC_COV: TZetaDBFecha;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    CB_NUM_EXT: TDBEdit;
    Label2: TLabel;
    CB_NUM_INT: TDBEdit;
    GBInfonavit: TGroupBox;
    CB_INFTASALbl: TLabel;
    CB_INFCREDLbl: TLabel;
    LblDescuento: TLabel;
    Label13: TLabel;
    CB_INF_OLDLbl: TLabel;
    CB_INF_INILbl: TLabel;
    lblPorcentaje: TLabel;
    CB_INF_ANTLbl: TLabel;
    CB_INFMANT: TDBCheckBox;
    CB_INFCRED: TDBEdit;
    CB_INFTIPO: TZetaDBKeyCombo;
    CB_INFTASA: TZetaDBNumero;
    CB_INF_INI: TZetaDBFecha;
    zkcCB_INF_OLD: TZetaKeyCombo;
    CB_INFDISM: TDBCheckBox;
    CB_INF_ANT: TZetaDBFecha;
    GroupBox2: TGroupBox;
    LEstudios: TLabel;
    CB_ESTUDIO: TZetaDBKeyLookup_DevEx;
    Label9: TLabel;
    Label6: TLabel;
    CB_CARRERA: TDBEdit;
    LCarrera: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    CB_EST_HOY: TDBCheckBox;
    CB_EST_HOR: TDBEdit;
    CB_EST_HORlbl: TLabel;
    GroupBox3: TGroupBox;
    CB_HABLA: TDBCheckBox;
    CB_IDIOMA: TDBEdit;
    CB_IDIOMAlbl: TLabel;
    LMaquinas: TLabel;
    CB_MAQUINA: TDBEdit;
    CB_EXPERIE: TDBEdit;
    LExperiencia: TLabel;
    GroupBox4: TGroupBox;
    lblTarjDesp: TLabel;
    btnVerTarjetaDesp: TcxButton;
    CB_CTA_VAL: TDBEdit;
    GBNomina: TGroupBox;
    LblNivel0: TLabel;
    lbNeto: TLabel;
    CB_NIVEL0: TZetaDBKeyLookup_DevEx;
    CB_NETO: TZetaDBNumero;
    LBanca: TLabel;
    CB_BAN_ELE: TDBEdit;
    BtnBAN_ELE: TcxButton;
    CB_TDISCAP: TZetaDBKeyCombo;
    lblTDiscapacidad: TLabel;
    CB_ESCUELA: TDBEdit;
    CB_TESCUEL: TZetaDBKeyCombo;
    CB_TITULO: TZetaDBKeyCombo;
    Label10: TLabel;
    CB_MUNICIP: TZetaDBKeyLookup_DevEx;
    lblTarjGas: TLabel;
    CB_CTA_GAS: TDBEdit;
    btnVerTarjetaGasolina: TcxButton;
    CB_YTITULO: TZetaDBNumero;
    gbDatosMedicos: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    CB_ALERGIA: TDBEdit;
    CB_TSANGRE: TDBComboBox;
    Panel8: TPanel;
    GroupBox5: TGroupBox;
    lBanco: TLabel;
    CB_BANCO: TZetaDBKeyLookup_DevEx;
    CB_E_MAIL: TDBEdit;
    Label12: TLabel;
    gbContacto: TcxGroupBox;
    cxGroupBox1: TcxGroupBox;
    Label16: TLabel;
    CB_REGIMEN: TZetaDBKeyCombo; // SYNERGY
    TabGafeteBiometrico: TcxTabSheet;
    Label3: TLabel;
    CB_ID_NUM: TDBEdit;
    lblNumeroBiometrico: TLabel;
    CB_ID_BIO: TZetaDBTextBox;
    lblCB_GP_COD: TLabel;
    CB_GP_COD: TZetaDBKeyLookup_DevEx;
    btnAsignarBio: TcxButton;
    btnBorrarBio: TcxButton;
    gbCodigo: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Panel9: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure SaleUltimoCampo(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_EST_HOYClick(Sender: TObject);
    procedure CB_HABLAClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CB_AUTOSALClick(Sender: TObject);
    procedure CB_INFTIPOChange(Sender: TObject);
    procedure ListEmpClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    //procedure DiasGBExit(Sender: TObject);
    procedure CB_SALARIOExit(Sender: TObject);
    procedure ZMonthExit(Sender: TObject);
    procedure CB_PUESTOExit(Sender: TObject);
    procedure CB_CLASIFIExit(Sender: TObject);
    procedure bDigitoVerificadorClick(Sender: TObject);
    procedure CB_SEXOChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ZYearExit(Sender: TObject);
    procedure BtnBAN_ELEClick(Sender: TObject);
    procedure CB_RANGO_SExit(Sender: TObject);
    procedure CB_FEC_NACExit(Sender: TObject);
    procedure PersonalesChange(Sender: TObject);
    procedure CB_RFCChange(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure BtnCandidatoClick(Sender: TObject);
    procedure CB_ID_NUMExit(Sender: TObject);
    procedure zkcCB_INF_OLDChange(Sender: TObject);
    procedure CB_CONTRATValidLookup(Sender: TObject);
    procedure CB_NIVEL12ValidLookup(Sender: TObject);
    procedure CB_NIVEL11ValidLookup(Sender: TObject);
    procedure CB_NIVEL10ValidLookup(Sender: TObject);
    procedure CB_NIVEL9ValidLookup(Sender: TObject);
    procedure CB_NIVEL8ValidLookup(Sender: TObject);
    procedure CB_NIVEL7ValidLookup(Sender: TObject);
    procedure CB_NIVEL6ValidLookup(Sender: TObject);
    procedure CB_NIVEL5ValidLookup(Sender: TObject);
    procedure CB_NIVEL4ValidLookup(Sender: TObject);
    procedure CB_NIVEL3ValidLookup(Sender: TObject);
    procedure CB_NIVEL2ValidLookup(Sender: TObject);
    procedure CB_DISCAPAClick(Sender: TObject);
    procedure btnVerTarjetaDespClick(Sender: TObject);
    procedure btnVerTarjetaGasolinaClick(Sender: TObject);
    procedure CB_ESTADOValidKey(Sender: TObject);
    procedure btnAsignarBioClick(Sender: TObject);
    procedure btnBorrarBioClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;

    //procedure Panel10Enter(Sender: TObject);
  private
    { Private declarations }
    //iLleno: Integer;
    rMonthActual, rYearActual : Real;
    rSalClasifi, rRangoSal : TPesos;
    //bPrimerAdicional ,bSegundoAdicional : Boolean;      // Hay Campos Adicionales ?
    bValidaAltaEmp, bRevisaID: Boolean;      // Proceso de Validaci�n de Duplicados
    lCambiosPuesto : Boolean;                           // Restringir acceso a campos sugeridos por el Puesto
    //tcPrimerAdicional, tcSegundoAdicional: TWinControl;
    sClasifi : String;
    //sPuesto  : String                                 // Para Checar si hay cambio de Clasificaci�n
    function SetSalarioTabulador: Boolean;
    function GetClasifi: string;
    function CheckIdentificacion: Boolean;
    function RevisaInformacion: Boolean;
    function GetPercepcionesFijas: String;
    procedure BorraOtraPer(const sCodigo: string);
    //procedure SetCampoAdicion( sValor: String; oLabel: TLabel; oControl: TWinControl ) ;
    procedure ControlesSalarios;
    procedure SetControlesPuesto;
    procedure IniciaSexo;
    procedure InitControlesNoDB;
    //procedure SetCamposAdicionales;
    procedure SetCamposNivel;
    procedure SetLookupsDataSet;
    procedure SeleccionaPagina( oControl: TWinControl; const iPagina: Integer; const lActivateControl: Boolean = TRUE );
    procedure SetPaginaEdicion( const iPagina: Integer );
    procedure MoveNextPagina( const iSalto: Integer );
    procedure HabilitaCredInfo;
    procedure LlenaListaTabulador;
    procedure CambiaFechaResidencia;
    procedure SetVacaciones(const lEnabled: Boolean);
    procedure SetMatsushita;
    procedure SetCandidato;
    procedure ChecaRangoSalarial;
    procedure SetControlesNivel0;
    procedure CheckPuestoFijas;
    procedure IniciaResidencia;
    procedure AsignaValorTasa;
    //procedure ClearInfonavit;
    procedure CreaAdicionales;
    procedure InitListEmp;
    function GetFirstControlAdicional( oControl: TWinControl ): TWinControl;
    function GetTabVacaciones: Integer;
    procedure SetControlesNeto;
    procedure HabilitaControl;
    procedure CambiaLeyendas;
    {$ifdef ACS}procedure SetPosicionNiveles;{ACS}{$endif}
    {$ifdef ACS}
    procedure LimpiaLookUpOlfKey;{ACS}
    {$endif}
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}

  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
    //iEmpleado: TNumEmp;
  end;

var
  EmpAlta_DevEx: TEmpAlta_DevEx;

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZGlobalTress,
     ZAccesosTress,
     ZAccesosMgr,
     DRecursos,
     DTablas,
     DCatalogos,
     DGlobal,
     DCliente,
     dSistema,
     FCamposMgr;

const
     //K_T_ULTIMO = 999;
     K_TAB_IDENTIFICA   = 0;
     K_TAB_PERSONALES   = 1;
     K_TAB_RESIDENCIA   = 2;
     K_TAB_EXPERIENCIA  = 3;
     K_TAB_CONTRATACION = 4;
     K_TAB_AREAS        = 5;
     K_TAB_SALARIO      = 6;
     K_TAB_PERCEPCIONES = 7;
     K_TAB_GAFETE       = 8;
     K_TAB_OTROS        = 9;
{
     K_TAB_ADICIONALES1 = 9;
     K_TAB_ADICIONALES2 = 10;
     K_TAB_VACACIONES   = 11;
}
{$ifdef ACS}
     K_ALT_DEF = 24;
{$endif}


{$define CAMBIOS_SENDA}
{.$undefine CAMBIOS_SENDA}

{$R *.DFM}

procedure TEmpAlta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;

     CB_NIVEL12lbl.Top := CB_NIVEL1lbl.Top;
     CB_NIVEL11lbl.Top := CB_NIVEL12lbl.Top + K_ALT_DEF;
     CB_NIVEL10lbl.Top := CB_NIVEL11lbl.Top + K_ALT_DEF;
     CB_NIVEL9lbl.Top   := CB_NIVEL10lbl.Top + K_ALT_DEF;
     CB_NIVEL8lbl.Top  := CB_NIVEL9lbl.Top + K_ALT_DEF;
     CB_NIVEL7lbl.Top  := CB_NIVEL8lbl.Top + K_ALT_DEF;
     CB_NIVEL6lbl.Top  := CB_NIVEL7lbl.Top + K_ALT_DEF;
     CB_NIVEL5lbl.Top  := CB_NIVEL6lbl.Top + K_ALT_DEF;
     CB_NIVEL4lbl.Top  := CB_NIVEL5lbl.Top + K_ALT_DEF;
     CB_NIVEL3lbl.Top  := CB_NIVEL4lbl.Top + K_ALT_DEF;
     CB_NIVEL2lbl.Top  := CB_NIVEL3lbl.Top + K_ALT_DEF;
     CB_NIVEL1lbl.Top  := CB_NIVEL2lbl.Top + K_ALT_DEF;

     CB_NIVEL12.Top := CB_NIVEL1.Top;
     CB_NIVEL11.Top := CB_NIVEL12.Top + K_ALT_DEF;
     CB_NIVEL10.Top := CB_NIVEL11.Top + K_ALT_DEF;
     CB_NIVEL9.Top   := CB_NIVEL10.Top + K_ALT_DEF;
     CB_NIVEL8.Top  := CB_NIVEL9.Top + K_ALT_DEF;
     CB_NIVEL7.Top  := CB_NIVEL8.Top + K_ALT_DEF;
     CB_NIVEL6.Top  := CB_NIVEL7.Top + K_ALT_DEF;
     CB_NIVEL5.Top  := CB_NIVEL6.Top + K_ALT_DEF;
     CB_NIVEL4.Top  := CB_NIVEL5.Top + K_ALT_DEF;
     CB_NIVEL3.Top  := CB_NIVEL4.Top + K_ALT_DEF;
     CB_NIVEL2.Top  := CB_NIVEL3.Top + K_ALT_DEF;
     CB_NIVEL1.Top  := CB_NIVEL2.Top + K_ALT_DEF;

     CB_NIVEL12.TabOrder := 1;
     CB_NIVEL11.TabOrder := 2;
     CB_NIVEL10.TabOrder := 3;
     CB_NIVEL9.TabOrder := 4;
     CB_NIVEL8.TabOrder := 5;
     CB_NIVEL7.TabOrder := 6;
     CB_NIVEL6.TabOrder := 7;
     CB_NIVEL5.TabOrder := 8;
     CB_NIVEL4.TabOrder := 9;
     CB_NIVEL3.TabOrder := 10;
     CB_NIVEL2.TabOrder := 11;
     CB_NIVEL1.TabOrder := 12;

     {
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     }
     {$endif}
     IndexDerechos := D_EMP_REG_ALTA;
     SetLookupsDataSet;
     SetCamposNivel;
     SetMatsushita;
     ZetaClientTools.LlenaTasaAnterior( zkcCB_INF_OLD );
     CreaAdicionales;
     InitListEmp;
     HelpContext:= H10151_Alta_empleados;

     //Verificar constante de AVENT
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          //Tab Personales
          CB_DISCAPA.Visible := False;
          CB_INDIGE.Visible := False;
          //Tab Otros
          GBInfonavit.Visible := False;
          GBFonacot.Visible := False;
          GBEmpleo.Visible := False;
          GBAsistencia.Top := GBInfonavit.Top;
          GBEvaluacion.Top := GBAsistencia.Top;
          GBNomina.Top := GBAsistencia.Top + GBAsistencia.Height + 4;
     end;//if }

     {$ifndef DOS_CAPAS}
     {$ifndef OSRAM_INTERFAZ}
     lblNumeroBiometrico.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL11 ); // SYNERGY
     lblCB_GP_COD.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_GAFETE_BIOMETRICO, K_DERECHO_ADICIONAL11 ); // SYNERGY
     {$endif}
     lblNumeroBiometrico.Visible := true; // SYNERGY
     lblCB_GP_COD.Visible := true; // SYNERGY
     CB_ID_BIO.DataField := 'CB_ID_BIO'; // SYNERGY
     CB_GP_COD.DataField := 'CB_GP_COD'; // SYNERGY
     CB_ID_BIO.Visible := true; // SYNERGY
     CB_GP_COD.Visible := true; // SYNERGY
     CB_ID_BIO.Enabled := lblNumeroBiometrico.Enabled; // SYNERGY
     CB_GP_COD.Enabled := ( lblNumeroBiometrico.Enabled and ( CB_ID_BIO.Caption <> '0' ) ); // SYNERGY
     btnAsignarBio.Visible := lblNumeroBiometrico.Enabled; // SYNERGY
     btnBorrarBio.Visible := lblNumeroBiometrico.Enabled; // SYNERGY
     {$endif}

     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEmpAlta_DevEx.CreaAdicionales;
var
   i : Integer;
begin
     DataSource.DataSet:= dmRecursos.cdsDatosEmpleado;
     dmSistema.CamposAdic.ContruyeFormaEdicion( dmSistema.Clasificaciones, self.DataSource, self, PageControl,
                                                dmTablas.GetEntidadAdicionalConnect, FALSE );
     // Agrega evento para salir del ultimo campo
     with PageControl do
     begin
          TabVacacion.PageIndex := PageCount - 1;  // Manda Vacaciones al final
          for i := 0 to PageCount - 1 do
          begin
               if ( Pages[i].Tag > 1 ) then   // Es Adicional
               begin
                    with TPanel.Create( self ) do
                    begin
                         Caption := ZetaCommonClasses.VACIO;
                         BevelOuter := bvNone;
                         Parent := Pages[i];
                         TabStop := True;
                         OnEnter := self.SaleUltimoCampo;
                         Width := 0;
                    end;
               end;
          end;
     end;
end;

procedure TEmpAlta_DevEx.InitListEmp;
var
   i : Integer;
begin
     with ListEmp.Items do
     begin
          BeginUpdate;
          try
             Clear;
             with PageControl do
             begin
                  for i := 0 to PageCount - 1 do
                  begin
                       Add( Pages[i].Caption );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TEmpAlta_DevEx.FormShow(Sender: TObject);
begin
     lCambiosPuesto := not dmCliente.UsaPlazas;
      {$ifdef CAMBIOS_SENDA}
     bValidaAltaEmp := ( ( Global.GetGlobalString( K_GLOBAL_VALIDA_ALTA_EMP ) = VALIDAR_REPETIDOS_ALTA_ADVIRTIENDO ) or
                         ( Global.GetGlobalString( K_GLOBAL_VALIDA_ALTA_EMP ) = VALIDAR_REPETIDOS_ALTA_IMPIDIENDO ) );
     {$else}
     bValidaAltaEmp := Global.GetGlobalBooleano( K_GLOBAL_VALIDA_ALTA_EMP );
     {$endif}
     SetCandidato;
     //SetCamposAdicionales;
     SetControlesPuesto;
     SetControlesNivel0;
     SetControlesNeto;

     SetPaginaEdicion( K_TAB_IDENTIFICA );
{        
     PageControl.ActivePage := TabIdentifica;
     ListEmp.ItemIndex := 0;
     ListEmpClick( Sender );
}
     InitControlesNoDB;
     // Variables de Cambio de Clasificaci�n
     //sPuesto := VACIO;
     sClasifi := VACIO;
     inherited;
     HabilitaCredInfo;
     SetVacaciones( False );
     ControlesSalarios;
     bRevisaID := True;
     HabilitaControl;
     CambiaLeyendas;
     {$ifdef ACS}SetPosicionNiveles;{ACS}{$endif}
     // CB_NOMINA.ListaFija:=lfTipoPeriodo; //acl
     CB_NOMINA.ListaFija:=lfTipoPeriodoConfidencial; // AV, AL Mejora Confidencialidad

     CB_TDISCAP.Enabled := False;

     CB_BAN_ELE.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_BANCA );
     CB_CTA_VAL.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CONFIGURA );
     CB_CTA_GAS.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_ADICIONAL9 );

     btnBAN_ELE.Enabled := CB_BAN_ELE.Enabled;
     btnVerTarjetaGasolina.Enabled := CB_CTA_GAS.Enabled;
     btnVerTarjetaDesp.Enabled := CB_CTA_VAL.Enabled;

     LBanca.Enabled := CB_BAN_ELE.Enabled;
     lblTarjGas.Enabled := CB_CTA_GAS.Enabled;
     lblTarjDesp.Enabled := CB_CTA_VAL.Enabled;
     {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
      CambiosVisuales;
     {$ifend}
end;

procedure TEmpAlta_DevEx.Connect;
begin
     with dmRecursos, dmTablas, dmCatalogos, dmSistema do
     begin
          cdsEstudios.Conectar;
          cdsEstadoCivil.Conectar;
          cdsHabitacion.Conectar;
          cdsViveCon.Conectar;
          cdsTransporte.Conectar;
          cdsEstado.Conectar;
          cdsMunicipios.Conectar;
          cdsColonia.Conectar;
          cdsContratos.Conectar;
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
          cdsSSocial.Conectar;

          {$ifdef ACS}
          LimpiaLookUpOlfKey;
          {$endif}

          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
{
          if CB_G_TAB_1.Visible then cdsExtra1.Conectar;
          if CB_G_TAB_2.Visible then cdsExtra2.Conectar;
          if CB_G_TAB_3.Visible then cdsExtra3.Conectar;
          if CB_G_TAB_4.Visible then cdsExtra4.Conectar;
}
          if CB_NIVEL0.Visible then
          begin
               cdsNivel0.Conectar;
               if strVacio( cdsDatosEmpleado.FieldByName( 'CB_NIVEL0' ).AsString ) and  ( not dmCliente.EsMultipleConfidencialidad )  then
                  cdsDatosEmpleado.FieldByName( 'CB_NIVEL0' ).AsString := cdsNivel0.FieldByName( 'TB_CODIGO' ).AsString;
          end;

          with dmTablas do
          begin
               cdsBancos.Conectar;
               CB_BANCO.LookupDataSet := cdsBancos;
          end;

          DataSource.DataSet:= cdsDatosEmpleado;

          {$ifndef DOS_CAPAS}
          with dmSistema do
          begin
               cdsListaGrupos.Conectar;
               CB_GP_COD.LookUpDataSet := cdsListaGrupos;
          end;
          {$endif}

          LlenaOtrasPer( LBDisponibles.Items );
          LimpiaLista( LBSeleccion.Items );

     end;
     rRangoSal := CB_RANGO_S.Valor;           // Matsushita

     if strLleno( GetClasifi ) then
        CB_CLASIFIExit( self );

     CheckPuestoFijas;

     IniciaSexo;
     AsignaValorTasa;

end;

function TEmpAlta_DevEx.CheckIdentificacion: Boolean;
begin
     bRevisaID := bRevisaID and ( PageControl.ActivePage = tabIdentifica ) and ( not RevisaInformacion );
     Result := bRevisaID;
end;

function TEmpAlta_DevEx.SetSalarioTabulador: Boolean;
begin
     Result := TRUE;
     if ( Modo <> dsInactive ) and ( Modo <> dsBrowse ) and CB_AUTOSAL.Checked then
        if StrLleno( GetClasifi ) then
           LLenaListaTabulador
        else
           Result := FALSE;
end;

function TEmpAlta_DevEx.GetClasifi : string;
begin
     Result:= CB_CLASIFI.Llave;
end;

procedure TEmpAlta_DevEx.HabilitaCredInfo;
var
   lEnabled: Boolean;
begin
     // Si No Tiene Prestamo, deshabilitar Amortizacion
     // Si Tiene poner mascara correcta
     lEnabled:= (( CB_INFTIPO.Valor ) <> Ord( tiNoTiene ));
     with CB_INFTASA do
     begin
          case CB_INFTIPO.Valor of
               Ord( tiNoTiene )   : Mascara := mnPesos;
               Ord( tiPorcentaje ): Mascara := mnTasa;
               Ord( tiCuotaFija  ): Mascara := mnPesos;
               Ord( tiVeces )     : Mascara := mnVecesSMGDF;
               Ord( tiVecesUMA )  : Mascara := mnVecesUMA;
          end;
          Enabled:= lEnabled;
     end;
     CB_INFTASALbl.Enabled := lEnabled;
     CB_INFCRED.Enabled:= lEnabled;
     CB_INFCREDLbl.Enabled:= lEnabled;
     CB_INFMANT.Enabled:= lEnabled;
     CB_INF_INI.Enabled:= lEnabled;
     CB_INF_INILbl.Enabled:= lEnabled;
     CB_INF_ANT.Enabled:= lEnabled;
     CB_INF_ANTLbl.Enabled:= lEnabled;     
     //CB_INF_OLD.Enabled := (( CB_INFTIPO.Valor ) = Ord( tiPorcentaje ));
     {zkcCB_INF_OLD.Enabled := (( CB_INFTIPO.Valor ) = Ord( tiPorcentaje ));
     CB_INF_OLDLbl.Enabled := zkcCB_INF_OLD.Enabled; }
     CB_INFDISM.Enabled:= (( CB_INFTIPO.Valor ) = Ord( tiPorcentaje ));
end;

{
procedure TEmpAlta.ClearInfonavit;
begin
     with dmRecursos.cdsDatosEmpleado do
     begin
          FieldByName( 'CB_INFCRED' ).AsString := '';
          FieldByName( 'CB_INFTASA' ).AsFloat := 0.0;
          FieldByName( 'CB_INF_OLD' ).AsFloat := 0.0;
          FieldByName( 'CB_INFMANT' ).AsString := 'N';
          // EZM: Y1900 Problem
          if FieldByName( 'CB_INF_INI' ).AsDateTime > 0 then
            FieldByName( 'CB_INF_INI' ).AsDateTime := 0;
     end;
end;
}

procedure TEmpAlta_DevEx.SetVacaciones( const lEnabled: Boolean );
begin
     DiasGB.Enabled         := lEnabled;
     FechaCierreLbl.Enabled := lEnabled;
     ZCierre.Enabled        := lEnabled;
     PagadosLbl.Enabled     := lEnabled;
     GozadosLbl.Enabled     := lEnabled;
     PrimaLbl.Enabled       := lEnabled;
     CB_DER_PAG.Enabled     := lEnabled;
     CB_DER_GOZ.Enabled     := lEnabled;
     CB_DER_PV.Enabled      := lEnabled;
     if lEnabled then
        ZCierre.Caption := FechaCorta( dmCliente.cdsEmpleado.FieldByName( 'CB_DER_FEC' ).AsDateTime )
     else
        ZCierre.Caption := VACIO;
end;

procedure TEmpAlta_DevEx.HabilitaControl;
begin
     Global.Conectar;
     if Global.GetGlobalBooleano( K_GLOBAL_NUM_EMP_AUTOMATICO ) then
     begin
          CB_CODIGO.Enabled := ZAccesosMgr.CheckDerecho( D_EMP_REG_ALTA, K_DERECHO_CAMBIO );
          lblEmpleado.Enabled := CB_CODIGO.Enabled;
     end
     else
     begin
          CB_CODIGO.Enabled := True;
          lblEmpleado.Enabled := CB_CODIGO.Enabled;
     end;
end;

procedure TEmpAlta_DevEx.CambiaLeyendas;
begin
     Global.Conectar;
     if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
     begin
          lblSexo.Caption := 'G�nero:';
          lRFC.Caption := '# de Ced. de Ident.:';
          LCB_SEGSOC.Caption := '# de Carnet de IHSS:';
          LCB_CURP.Visible := FALSE;
          CB_CURP.Visible := LCB_CURP.Visible;
     end
     else
     begin
          lblSexo.Caption := 'Sexo:';
          lRFC.Caption := 'R.F.C.:';
          LCB_SEGSOC.Caption := '# Seguro Social:';
          LCB_CURP.Visible := TRUE;
          CB_CURP.Visible := LCB_CURP.Visible;
     end;
end;

procedure TEmpAlta_DevEx.CambiaFechaResidencia;
begin
     with dmRecursos.cdsDatosEmpleado do
     begin
          FieldByName( 'CB_FEC_RES' ).AsDateTime:= FechaResidencia( ZYear.Valor, ZMonth.Valor );
     end;
end;

procedure TEmpAlta_DevEx.IniciaResidencia;
begin
     SetYearsMonths( DataSource.DataSet.FieldByName( 'CB_FEC_RES' ).AsDateTime,
                     rYearActual, rMonthActual );
     ZYear.Valor  := rYearActual;
     ZMonth.Valor := rMonthActual;
end;

procedure TEmpAlta_DevEx.CheckPuestoFijas;
var
   sPuesto: String;
   iPos : Integer;
begin
     sPuesto := CB_PUESTO.Llave;
     if strLleno( sPuesto ) and ( not CB_AUTOSAL.Checked ) then   // Si tiene Salario x Tabulador no se puede sugerir - tomar� lo que est� en la Clasificaci�n
     begin
          dmRecursos.LlenaOtrasPer( LBDisponibles.Items );
          LimpiaLista( LBSeleccion.Items );
          with dmCatalogos do
          begin
               with cdsPuestos do
               begin
                    Conectar;
                    if ( sPuesto <> FieldByName( 'PU_CODIGO' ).AsString ) then
                       Locate( 'PU_CODIGO', sPuesto, [] );
               end;
               with cdsPercepFijas do
               begin
                    Refrescar;     // Trae las Percepciones Fijas del Puesto Seleccionado
                    while not EOF do
                    begin
                         iPos := LBDisponibles.Items.IndexOfName( FieldByName( 'PF_CODIGO' ).AsString );
                         if ( iPos >= 0 ) then
                         begin
                              LBDisponibles.ItemIndex := iPos;
                              ZetaSmartLists.Escoger;   // Pasar� el Objeto a LbSeleccionados
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

procedure TEmpAlta_DevEx.LlenaListaTabulador;
var
   i: integer;
   dSalario : TPesos;
begin
     dmRecursos.LlenaOtrasPer( LBDisponibles.Items );
     dmRecursos.LlenaListaTabulador( LbSeleccion.Items, GetClasifi, dSalario );
     if LbSeleccion.Items.Count > 0 then
        for i := 0 to LbSeleccion.Items.Count - 1 do
            BorraOtraPer(TOtrasPer(LbSeleccion.Items.Objects[i]).Codigo);
     CB_Salario.Valor := dSalario;
     if dmCliente.Matsushita then
     begin
          rSalClasifi := dSalario;   //Para evento onExit de CB_RANGO_S
          CB_SALARIO.Valor := dmRecursos.CalculaRangoSalarial( CB_RANGO_S.Valor, LbSeleccion.Items, dSalario );
     end;
end;

procedure TEmpAlta_DevEx.BorraOtraPer( const sCodigo : string );
var
   i:integer;
begin
     //Result := VACIO;       Antes era una funci�n string
     with LBDisponibles do
     begin
          for i :=0 to Items.Count - 1 do
          begin
               if TOtrasPer(Items.Objects[i]).Codigo = sCodigo then
               begin
                    //Result := Items[i];
                    TOtrasPer(Items.Objects[i]).Free;
                    Items.Delete(i);
                    Exit;
               end;
          end;
     end;
end;

procedure TEmpAlta_DevEx.ControlesSalarios;
var
   lEnabled : Boolean;
begin
     lEnabled := ( not CB_AUTOSAL.Checked );
     ZetaClientTools.SetControlEnabled( CB_SALARIO, CB_SALARIOLbl, lEnabled );
{
     CB_SALARIOLbl.Enabled := lEnabled;
     CB_SALARIO.Enabled    := lEnabled;
}
     LBDisponibles.Enabled := lEnabled;
     LBSeleccion.Enabled   := lEnabled;
     ZetaSmartListsButton1.Visible := lEnabled;
     ZetaSmartListsButton2.Visible := lEnabled;
     ZetaSmartListsButton3.Visible := lEnabled;
     ZetaSmartListsButton4.Visible := lEnabled;
     // Matsushita
     ZetaClientTools.SetControlEnabled( CB_RANGO_S, CB_RANGO_SLbl, CB_AUTOSAL.Checked );
{
     with CB_AUTOSAL do
     begin
          CB_RANGO_SLbl.Enabled := Checked;
          CB_RANGO_S.Enabled := Checked;
     end;
}
end;

procedure TEmpAlta_DevEx.SetControlesPuesto;
begin
     ZetaClientTools.SetControlEnabled( CB_CONTRAT, CB_CONTRATlbl, TRUE );
     ZetaClientTools.SetControlEnabled( CB_PUESTO, CB_PUESTOlbl, FALSE );      // Este siempre est� deshabilitado
     ZetaClientTools.SetControlEnabled( CB_PLAZA, CB_Plazalbl, FALSE );      // Este siempre est� deshabilitado
     ZetaClientTools.SetControlEnabled( CB_CLASIFI, CB_CLASIFIlbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_TURNO, CB_TURNOlbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_PATRON, CB_PATRONlbl, lCambiosPuesto );

     ZetaClientTools.SetControlEnabled( CB_NIVEL1, CB_NIVEL1lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL2, CB_NIVEL2lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL3, CB_NIVEL3lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL4, CB_NIVEL4lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL5, CB_NIVEL5lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL6, CB_NIVEL6lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL7, CB_NIVEL7lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL8, CB_NIVEL8lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL9, CB_NIVEL9lbl, lCambiosPuesto );
     {$ifdef ACS}
     ZetaClientTools.SetControlEnabled( CB_NIVEL10, CB_NIVEL10lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL11, CB_NIVEL11lbl, lCambiosPuesto );
     ZetaClientTools.SetControlEnabled( CB_NIVEL12, CB_NIVEL12lbl, lCambiosPuesto );
     {$endif}

     {ZetaClientTools.SetControlEnabled( CB_PER_VAR, CB_PER_VARlbl, CB_AUTOSAL.Checked );
     ZetaClientTools.SetControlEnabled( CB_ZONA_GE, CB_ZONA_GElbl, CB_AUTOSAL.Checked );
     ZetaClientTools.SetControlEnabled( CB_TABLASS, CB_TABLASSlbl, CB_AUTOSAL.Checked );
     }

     //CB_CHECA.Enabled := lCambiosPuesto;
     // CB_AREA.Enabled := lCambiosPuesto;   En la Alta no se puede capturar el Area inicial de Labor del Empleado
end;

{
procedure TEmpAlta.SetCamposAdicionales;
var
   sGlobal : String;
begin
     iLleno := 0;
     bPrimerAdicional := False;
     bSegundoAdicional := False;
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TEXTO1 ), TextAdicLbl1, CB_G_TEX_1 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TEXTO2 ), TextAdicLbl2, CB_G_TEX_2 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TEXTO3 ), TextAdicLbl3, CB_G_TEX_3 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TEXTO4 ), TextAdicLbl4, CB_G_TEX_4 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_NUM1 ), NumAdicLbl1, CB_G_NUM_1 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_NUM2 ), NumAdicLbl2, CB_G_NUM_2 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_NUM3 ), NumAdicLbl3, CB_G_NUM_3 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_FECHA1 ), FechaAdicLbl1, CB_G_FEC_1 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_FECHA2 ), FechaAdicLbl2, CB_G_FEC_2 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_FECHA3 ), FechaAdicLbl3, CB_G_FEC_3 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TAB1 ), TablaAdicLbl1, CB_G_TAB_1 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TAB2 ), TablaAdicLbl2, CB_G_TAB_2 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TAB3 ), TablaAdicLbl3, CB_G_TAB_3 );
     SetCampoAdicion( Global.GetGlobalString( K_GLOBAL_TAB4 ), TablaAdicLbl4, CB_G_TAB_4 );
     // Adicionales L�gicos
     sGlobal := Global.GetGlobalString( K_GLOBAL_LOG1 );
     SetCampoAdicion( sGlobal, BoolAdicLbl1, CB_G_LOG_1 );
     CB_G_LOG_1.Caption := sGlobal;
     sGlobal := Global.GetGlobalString( K_GLOBAL_LOG2 );
     SetCampoAdicion( sGlobal, BoolAdicLbl2, CB_G_LOG_2 );
     CB_G_LOG_2.Caption := sGlobal;
     sGlobal := Global.GetGlobalString( K_GLOBAL_LOG3 );
     SetCampoAdicion( sGlobal, BoolAdicLbl3, CB_G_LOG_3 );
     CB_G_LOG_3.Caption := sGlobal;
     BoolAdicLbl1.Visible := False;
     BoolAdicLbl2.Visible := False;
     BoolAdicLbl3.Visible := False;
     with Panel10 do
     begin
          TabOrder := TabAdicionales2.ControlCount-1;   // Debe ser el ultimo control del Tabsheet
     end;
end;

procedure TEmpAlta.SetCampoAdicion( sValor: String; oLabel: TLabel; oControl: TWinControl ) ;
begin
     if StrLleno( sValor ) then
     begin
         oLabel.Caption := ' ';
         oLabel.Left := 178;
         oLabel.Caption := Trim( sValor ) + ':';
         oControl.Left := 190;
         if iLleno < 9 then
         begin
              if not bPrimerAdicional then
              begin
                   bPrimerAdicional := True;
                   tcPrimerAdicional := oControl;
              end;
              oControl.Top  := 5 + ( iLleno * 24 );
         end
         else
         begin
              if not bSegundoAdicional then
              begin
                   bSegundoAdicional := True;
                   tcSegundoAdicional := oControl;
              end;
              oControl.Parent := TabAdicionales2;
              oLabel.Parent := TabAdicionales2;
              oControl.Top   := 5 + ( ( iLleno - 9 ) * 24 );
         end;
         oLabel.Top := oControl.Top + 4;
         oLabel.Visible   := True;
         oControl.Visible := True;
         iLLeno := iLLeno + 1;
     end
     else
     begin
          oLabel.Visible := FALSE;
          oControl.Visible := FALSE;
     end;
end;
}

procedure TEmpAlta_DevEx.SetLookupsDataSet;
begin
     with dmTablas do
     begin
          CB_EDO_CIV.LookupDataSet := cdsEstadoCivil;
          CB_MED_TRA.LookupDataSet := cdsTransporte;
          CB_ESTADO.LookupDataSet := cdsEstado;
          CB_MUNICIP.LookupDataSet := cdsMunicipios;
          CB_VIVECON.LookupDataSet := cdsViveCon;
          CB_VIVEEN.LookupDataSet := cdsHabitacion;
          CB_ESTUDIO.LookupDataSet := cdsEstudios;
          CB_NIVEL1.LookupDataSet := cdsNivel1;
          CB_NIVEL2.LookupDataSet := cdsNivel2;
          CB_NIVEL3.LookupDataSet := cdsNivel3;
          CB_NIVEL4.LookupDataSet := cdsNivel4;
          CB_NIVEL5.LookupDataSet := cdsNivel5;
          CB_NIVEL6.LookupDataSet := cdsNivel6;
          CB_NIVEL7.LookupDataSet := cdsNivel7;
          CB_NIVEL8.LookupDataSet := cdsNivel8;
          CB_NIVEL9.LookupDataSet := cdsNivel9;
          {$ifdef ACS}
          CB_NIVEL10.LookupDataset := cdsNivel10;
          CB_NIVEL11.LookupDataset := cdsNivel11;
          CB_NIVEL12.LookupDataset := cdsNivel12;
          {$endif}
{
          CB_G_TAB_1.LookupDataSet := cdsExtra1;
          CB_G_TAB_2.LookupDataSet := cdsExtra2;
          CB_G_TAB_3.LookupDataSet := cdsExtra3;
          CB_G_TAB_4.LookupDataSet := cdsExtra4;
}
          CB_ENT_NAC.LookupDataSet := cdsEstado;
          CB_COD_COL.LookupDataSet := cdsColonia;
     end;
     with dmCatalogos do
     begin
          CB_CONTRAT.LookupDataSet := cdsContratos;
          CB_PUESTO.LookupDataSet := cdsPuestos;
          CB_CLASIFI.LookupDataSet := cdsClasifi;
          CB_TURNO.LookupDataSet := cdsTurnos;
          CB_PATRON.LookupDataSet := cdsRPatron;
          CB_TABLASS.LookupDataSet := cdsSSocial;
     end;
     CB_NIVEL0.LookupDataSet := dmSistema.cdsNivel0;
     CB_PLAZA.LookupDataSet := dmRecursos.cdsPlazasLookup;
end;

procedure TEmpAlta_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

procedure TEmpAlta_DevEx.SetMatsushita;
begin
     with dmCliente do
     begin
          CB_RANGO_SLbl.Visible:= Matsushita;
          CB_RANGO_S.Visible:= Matsushita;
     end;
end;

procedure TEmpAlta_DevEx.SetCandidato;
begin
     BtnCandidato.Visible := strLleno( Global.GetGlobalString( K_GLOBAL_EMPRESA_SELECCION ) );
end;

procedure TEmpAlta_DevEx.ChecaRangoSalarial;
begin
     if not CB_AUTOSAL.Checked then
        CB_RANGO_S.Valor:= 100.00;
     CB_RANGO_S.OnExit(Self);
end;

procedure TEmpAlta_DevEx.SetControlesNivel0;
begin
     CB_NIVEL0.Visible := dmSistema.HayNivel0 ;// and strLleno( dmCliente.Confidencialidad );
     LblNivel0.Visible := CB_NIVEL0.Visible;

     if CB_NIVEL0.Visible  and StrLleno( dmCliente.Confidencialidad ) then
     begin
          CB_NIVEL0.Filtro :=  Format( 'TB_CODIGO in %s', [dmCliente.ConfidencialidadListaIN] ) ;
     end
     else
     begin

          CB_NIVEL0.Filtro := VACIO;

     end;

     //ZetaClientTools.SetControlEnabled( CB_NIVEL0, LblNivel0, lCambiosPuesto );
end;

procedure TEmpAlta_DevEx.IniciaSexo;
begin
     with dmRecursos.cdsDatosEmpleado, CB_SEXO do
     begin
          if ( FieldByName( 'CB_SEXO' ).AsString = 'F' ) then
             ItemIndex:= ord( esFemenino )
          else
             ItemIndex:= ord( esMasculino );
     end;
end;

procedure TEmpAlta_DevEx.InitControlesNoDB;
begin
     ZMonth.Valor  := 0;
     ZYear.Valor   := 0;
     NombrePadre.Text := VACIO;
     NombreMadre.Text := VACIO;
end;

function TEmpAlta_DevEx.RevisaInformacion: Boolean;
var
   FParametros: TZetaParams;
   oCursor: TCursor;
begin
     Result := not bValidaAltaEmp;
     if not Result and bRevisaID then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             FParametros := TZetaParams.Create( Self );
             try
                with FParametros do
                begin
                     AddString('CB_APE_PAT', CB_APE_PAT.Text);
                     AddString('CB_APE_MAT', CB_APE_MAT.Text);
                     AddString('CB_NOMBRES', CB_NOMBRES.Text);
                     AddString('CB_SEXO', ObtieneElemento( lfSexo, CB_SEXO.ItemIndex ));
                     AddDate( 'CB_FEC_NAC', CB_FEC_NAC.Valor );
                     AddString('CB_RFC', CB_RFC.Text);
                     AddString('CB_SEGSOC', CB_SEGSOC.Text);
                     AddString('CB_CURP', CB_CURP.Text);
                end;
                Result := dmRecursos.RevisaInformacionParams( FParametros );
             finally
                    FreeAndNil( FParametros );
             end;
          finally
            Screen.Cursor := oCursor;
          end;
     end;
end;

function TEmpAlta_DevEx.GetTabVacaciones: Integer;
begin
     Result := PageControl.PageCount - 1;
end;

function TEmpAlta_DevEx.GetFirstControlAdicional( oControl: TWinControl ): TWinControl;
var
   i : Integer;
   oControlHijo : TWinControl;
   lOk : Boolean;
begin
     Result := nil;
     for i:= 0 to oControl.ControlCount - 1 do
     begin
          if ( oControl.Controls[i] is TWinControl ) then  // Solo controles que hereden de TWinControl
          begin
               oControlHijo := TWinControl( oControl.Controls[i] );
               with oControlHijo do
               begin
                    lOk := ( ClassType = TDBEdit ) or ( ClassType = TZetaDBNumero ) or
                           ( ClassType = TDBCheckBox ) or ( ClassType = TZetaDBFecha ) or
                           ( ClassType = TZetaDBKeyLookup );
               end;
               if lOk then
               begin
                    Result := oControlHijo;
                    Break;                  // Solo el primer control de ese tipo
               end
               else
               begin
                    if ( oControlHijo.ControlCount > 0 ) then
                       Result := GetFirstControlAdicional( oControlHijo );
               end;
          end;
     end;
end;

procedure TEmpAlta_DevEx.SeleccionaPagina( oControl: TWinControl; const iPagina: Integer; const lActivateControl: Boolean = TRUE );
begin
     with ListEmp do
     begin
          if ( ItemIndex <> iPagina ) then
             ItemIndex := iPagina;
     end;
     PageControl.ActivePageIndex := iPagina;
     if lActivateControl then
     begin
          if oControl <> nil then   //Se agrego porque maracaba AccessViolation, cambiar este enfoqu de controles al evento del pageControl correspondiente.
             if oControl.Enabled then
                ActiveControl := oControl;
     end;
end;

procedure TEmpAlta_DevEx.SetPaginaEdicion( const iPagina: Integer );
var
   iNewPagina: Integer;
   oNivelMaxActivo: TWinControl;
begin
{
     if ( iPagina = K_TAB_ADICIONALES1 ) and ( not bPrimerAdicional ) then   // No hay Adicionales1
     begin
          ZInformation( Caption, 'No se Tienen Definidos Campos Adicionales', 0 );
          iNewPagina := PageControl.ActivePageIndex;
     end
     else if ( iPagina = K_TAB_ADICIONALES2 ) and ( not bSegundoAdicional ) then   // No hay Adicionales2
     begin
          ZInformation( Caption, 'No se Tienen Definidos M�s Campos Adicionales', 0 );
          iNewPagina := PageControl.ActivePageIndex;
     end
     else
}
     {ACS: Verifica cual es el nivel activo m�ximo, para asignarle el focus}
     oNivelMaxActivo := CB_NIVEL1;
     {$ifdef ACS}
     if( CB_NIVEL12.Visible ) then
         oNivelMaxActivo := CB_NIVEL12
     else if( CB_NIVEL11.Visible ) then
         oNivelMaxActivo := CB_NIVEL11
     else if( CB_NIVEL10.Visible ) then
         oNivelMaxActivo := CB_NIVEL10
     else if( CB_NIVEL9.Visible ) then
         oNivelMaxActivo := CB_NIVEL9
     else if( CB_NIVEL8.Visible ) then
         oNivelMaxActivo := CB_NIVEL8
     else if( CB_NIVEL7.Visible ) then
         oNivelMaxActivo := CB_NIVEL7
     else if( CB_NIVEL6.Visible ) then
         oNivelMaxActivo := CB_NIVEL6
     else if( CB_NIVEL5.Visible ) then
         oNivelMaxActivo := CB_NIVEL5
     else if( CB_NIVEL4.Visible ) then
         oNivelMaxActivo := CB_NIVEL4
     else if( CB_NIVEL3.Visible ) then
         oNivelMaxActivo := CB_NIVEL3
     else if( CB_NIVEL2.Visible ) then
         oNivelMaxActivo := CB_NIVEL2
     else if( CB_NIVEL1.Visible ) then
         oNivelMaxActivo := CB_NIVEL1;
     {$endif}

     if ( iPagina <> K_TAB_IDENTIFICA ) and CheckIdentificacion then
     begin
          iNewPagina := K_TAB_IDENTIFICA;
     end
     else
     begin
          iNewPagina := iPagina;
     end;
     case iNewPagina of
          K_TAB_IDENTIFICA   : SeleccionaPagina( CB_CODIGO, K_TAB_IDENTIFICA );
          K_TAB_PERSONALES   : SeleccionaPagina( CB_LUG_NAC, K_TAB_PERSONALES );
          K_TAB_RESIDENCIA   : SeleccionaPagina( CB_CALLE, K_TAB_RESIDENCIA );
          K_TAB_EXPERIENCIA  : SeleccionaPagina( CB_ESTUDIO, K_TAB_EXPERIENCIA );
          K_TAB_CONTRATACION : SeleccionaPagina( CB_FEC_ING, K_TAB_CONTRATACION );
          K_TAB_AREAS        : SeleccionaPagina( oNivelMaxActivo, K_TAB_AREAS, StrLleno( Global.NombreNivel( 1 ) ) and lCambiosPuesto );{ACS: Ahora el focus es sobre el nivel 12}
          K_TAB_SALARIO      : SeleccionaPagina( CB_AUTOSAL, K_TAB_SALARIO, lCambiosPuesto );
          K_TAB_PERCEPCIONES : SeleccionaPagina( LBDisponibles, K_TAB_PERCEPCIONES, LBDisponibles.Enabled );
          K_TAB_GAFETE       : SeleccionaPagina( CB_CREDENC, K_TAB_GAFETE, CB_CREDENC.Enabled );
          K_TAB_OTROS        :
                               begin
                                    //Verificar constante de AVENT
                                    if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
                                    begin
                                         SeleccionaPagina( CB_CREDENC, K_TAB_OTROS );
                                    end//if
                                    else
                                    begin
                                         SeleccionaPagina( CB_INFTIPO, K_TAB_OTROS );
                                    end;//else
                               end;//K_TAB_OTROS        :
{
          K_TAB_ADICIONALES1 : SeleccionaPagina( tcPrimerAdicional, K_TAB_ADICIONALES1, bPrimerAdicional );
          K_TAB_ADICIONALES2 : SeleccionaPagina( tcSegundoAdicional, K_TAB_ADICIONALES2, bSegundoAdicional );
          K_TAB_VACACIONES   : SeleccionaPagina( CB_DER_PAG, K_TAB_VACACIONES, DiasGB.Enabled );
          // else - Se cubren todas los valores posibles de ListEmp 
}
     else
         if ( iNewPagina = GetTabVacaciones ) then
            SeleccionaPagina( CB_DER_PAG, iNewPagina, DiasGB.Enabled )
         else
             SeleccionaPagina( GetFirstControlAdicional( PageControl.Pages[iNewPagina] ), iNewPagina );
     end;
end;

procedure TEmpAlta_DevEx.MoveNextPagina( const iSalto: Integer );
var
   iNewPagina : Integer;
begin
     iNewPagina := ( ListEmp.ItemIndex + iSalto );
     if ( iNewPagina >= K_TAB_IDENTIFICA ) and ( iNewPagina <= GetTabVacaciones ) then
     begin
          if ( iNewPagina = K_TAB_PERCEPCIONES ) and ( not LBDisponibles.Enabled ) then     // No se Pueden editar las Percepciones Fijas
             iNewPagina := iNewPagina + iSalto;    // Incrementa el Salto
{
          if ( iNewPagina = K_TAB_ADICIONALES1 ) and ( not bPrimerAdicional ) then
             iNewPagina := iNewPagina + iSalto;
          if ( iNewPagina = K_TAB_ADICIONALES2 ) and ( not bSegundoAdicional ) then
             iNewPagina := iNewPagina + iSalto;
}
          SetPaginaEdicion( iNewPagina );
     end
     else if ( iNewPagina > GetTabVacaciones ) then   // Se pasa de la �ltima Pagina
     begin
          if ( OK_DevEx.CanFocus ) then
             ActiveControl := OK_DevEx;     // Activar el bot�n Ok
     end;
end;

{ Eventos de Controles }

procedure TEmpAlta_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
var
   dNacimiento : TDateTime;
   rValor: Double;
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_FEC_ANT' ) or ( Field.FieldName = 'CB_FEC_CON' ) or ( Field.FieldName = 'CB_CONTRAT' ) then
     begin
          ZAntiguedad.Caption := TiempoDias( Date - CB_FEC_ANT.Valor + 1, etDias);
          with dmRecursos, dmCatalogos do
               if cdsContratos.Active then
                  CB_FEC_COV.Enabled := ( cdsDatosEmpleado.FieldByName( 'CB_FEC_COV' ).AsDateTime <> NullDateTime );
     end;
     if ( Field <> nil ) and ( Field.FieldName = 'CB_FEC_ANT' ) then
     begin
          if Years( Field.AsDateTime, Date ) > 1 then
          begin
               dmCliente.cdsEmpleado.FieldByName( 'CB_DER_FEC' ).AsDateTime := NextDate( Field.AsDateTime, Date );
               SetVacaciones( True );
          end
          else
          begin
               dmCliente.cdsEmpleado.FieldByName( 'CB_DER_FEC' ).Clear;
               SetVacaciones( False );
          end;
     end;
     // Edad
     rValor := 0;
     if ( Field = nil ) or ( Field.FieldName = 'CB_FEC_NAC' ) then
     begin
          dNacimiento := dmCliente.cdsEmpleado.FieldByName( 'CB_FEC_NAC' ).AsDateTime;
          if ( dNacimiento > 0 ) then
             rValor := Trunc( Now - dNacimiento ) + 1;
          ZEdad.Caption := TiempoDias( rValor, etMeses );
     end;
     //Turno
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmCliente.cdsEmpleado do
               bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
     with BtnCandidato do
     begin
          if Visible then       // Si no est� visible no es necesario hacer nada
          begin
               if ( ( Field = nil ) or ( Field.FieldName = 'CB_APE_PAT' ) or ( Field.FieldName = 'CB_NOMBRES' ) ) then
               begin
                    Enabled := strLleno( dmCliente.cdsEmpleado.FieldByName( 'CB_APE_PAT' ).AsString ) and
                               strLleno( dmCliente.cdsEmpleado.FieldByName( 'CB_NOMBRES' ).AsString );
               end;
          end;
     end;
end;

function TEmpAlta_DevEx.GetPercepcionesFijas: String;
var
   i: Integer;
begin
     Result := VACIO;
     if LbSeleccion.Items.Count > 0 then
        for i:= 0 to LbSeleccion.Items.Count - 1 do
            Result := ConcatString( Result, TOtrasPer( LbSeleccion.Items.Objects[i] ).Codigo, ',' );
end;

procedure TEmpAlta_DevEx.OKClick(Sender: TObject);
   {$ifdef DENSO}
   function SugerirNumeroEmpleado : Boolean;
   var
      iSugerido : integer;
   begin
        Global.Conectar;
        Result:=False;
        if Global.GetGlobalBooleano( K_GLOBAL_NUM_EMP_SUGIERE_ESPECIAL ) then
        begin
             iSugerido := dmRecursos.GetSugiereNumeroEmpleado( dmCliente.cdsEmpleado );

             with dmCliente.cdsEmpleado do
             begin
                  if iSugerido <> dmCliente.cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger then
                  begin
                      FieldByName( 'CB_CODIGO' ).AsInteger:= iSugerido;
                      ZetaDialogo.ZInformation(self.Caption, format( 'Por la numeraci�n especial, el n�mero del empleado cambi� a %d', [iSugerido] ) , 0 );
                      Result := True;
                  end;
             end;
        end;
   end;
   {$endif}

begin
     if CheckIdentificacion then
        SetPaginaEdicion( K_TAB_IDENTIFICA )
     else
     begin
          if dmCliente.EsModoDemo then Exit;
{
          if (( CB_INFTIPO.Valor ) = Ord( tiNoTiene )) then        // Ya se est� validando en BeforePost de dmcliente.cdsEmpleado
             ClearInfonavit;
}
          if CB_SALARIO.Valor < 0 then
          begin
                SeleccionaPagina( CB_SALARIO, K_TAB_SALARIO );
                ZError( self.Caption, 'Salario NO Puede Ser Menor Que Cero', 0 );
{
                PageControl.ActivePage := TabSalario;
                ListEmp.ItemIndex:= 6;
                ActiveControl := CB_SALARIO;
}
          end
          else if CB_CODIGO.Valor <= 0 then
          begin
                SeleccionaPagina( CB_CODIGO, K_TAB_IDENTIFICA );
                ZError( self.Caption , 'N�mero del Empleado No Puede Ser Cero', 0 );
{
                PageControl.ActivePage := TabIdentifica;
                ListEmp.ItemIndex:= 0;
                ActiveControl := CB_CODIGO;
}
          end
          else if ( CB_NIVEL0.Visible ) and ( strVacio( CB_NIVEL0.Llave ) ) then
          begin
                SeleccionaPagina( CB_NIVEL0, K_TAB_OTROS );
                ZError( self.Caption, 'Nivel de Confidencialidad no Puede Quedar Vacio', 0 );
{
                PageControl.ActivePage := TabOtros;
                ListEmp.ItemIndex:= 8;
                ActiveControl := CB_NIVEL0;
}
          end
          else if ( DataSource.DataSet.FieldByName( 'CB_INF_INI').AsString < DataSource.DataSet.FieldByName('CB_INF_ANT').AsString ) and
               not ZWarningConfirm('� Atenci�n !', 'Se indic� una fecha de otorgamiento mayor al inicio del descuento' + CR_LF + '�Seguro de grabar? ', 0, mbCancel) then
                  SeleccionaPagina( CB_INF_INI, K_TAB_OTROS )
          else
          begin
               {$ifdef GRUPOSENDA_VALIDACIONCAMPOS}
               if ( strLleno( NombrePadre.Text ) and strLleno( NombreMadre.Text ) ) then
               begin
               {$endif}

               {$ifdef DENSO}
			   SugerirNumeroEmpleado; { DENSO }
               {$endif} 
               with dmRecursos do
               begin
                    ListaPercepFijas:= GetPercepcionesFijas;       /// Percepciones Fijas
                    OtrosDatos := VarArrayOf( [ NombrePadre.Text, NombreMadre.Text ] );  // Otros Datos
                    DesconectaVaca := ZCierre.Enabled;
               end;
               // Matsushita
               if dmCliente.Matsushita and CB_AUTOSAL.Checked and ( ( CB_RANGO_S.Valor > 0 ) and ( CB_RANGO_S.Valor <> 100 ) ) then
                  CB_SALARIO.Valor := rSalClasifi;

               inherited;

               if dmCliente.cdsEmpleado.ChangeCount = 0 then
                  Close
               else if dmRecursos.TipoErrorEmpleado = teLlavePrimaria then
               begin
                    SeleccionaPagina( CB_CODIGO, K_TAB_IDENTIFICA );
                    with dmCliente.cdsEmpleado do           // el inherited pone el clientDataset en Edici�n
                         FieldByName( 'CB_CODIGO' ).AsInteger:= FieldByName( 'CB_CODIGO' ).AsInteger + 1;
               end;
               {$ifdef GRUPOSENDA_VALIDACIONCAMPOS}
               end
               else
               begin
                    SeleccionaPagina( NombrePadre, K_TAB_PERSONALES );
                    ZError( self.Caption, 'El nombre del padre y la madre del empleado no pueden quedar vac�o.', 0 );
               end;
               {$endif}
          end;
     end;//else
end;

procedure TEmpAlta_DevEx.CB_SALARIOExit(Sender: TObject);
begin
     inherited;
     if ( CB_SALARIO.Valor < 0 ) then
        ZError('','Salario no puede ser menor que cero',0);
end;

procedure TEmpAlta_DevEx.CB_PUESTOExit(Sender: TObject);
begin
{
     Versi�n 2.4.97 : Ya no se ocupa el evento onExit por que el control ya no se puede editar en esta forma ( Se Captura en FEmpAltaOtros )
     if StrLleno( CB_PUESTO.Llave ) and ( sPuesto <> CB_PUESTO.Llave ) then
     begin
          sPuesto := CB_PUESTO.LLave;
          sClasifi := dmCatalogos.cdsPuestos.FieldByName('PU_CLASIFI').AsString ;
          if StrLleno( sClasifi ) then
          begin
               CB_CLASIFI.Llave := sClasifi;
               SetSalarioTabulador;
          end;
     end;
}
end;

procedure TEmpAlta_DevEx.CB_CLASIFIExit(Sender: TObject);
begin
     if ( sClasifi <> GetClasifi ) then
     begin
          SetSalarioTabulador;
          sClasifi := GetClasifi;
     end;
end;

procedure TEmpAlta_DevEx.CB_SEXOChange(Sender: TObject);
var
   sSexo: String;
begin
     sSexo:= ObtieneElemento( lfSexo, CB_SEXO.ItemIndex );
     with dmRecursos.cdsDatosEmpleado do
          if ( sSexo <> FieldByName( 'CB_SEXO' ).AsString ) then
          begin
               FieldByName( 'CB_SEXO' ).AsString:= sSexo;
               PersonalesChange(Sender);
          end;
end;

procedure TEmpAlta_DevEx.ListEmpClick(Sender: TObject);
begin
     SetPaginaEdicion( ListEmp.ItemIndex );
end;

{
procedure TEmpAlta.ListEmpClick(Sender: TObject);
          procedure SeleccionaPagina(Numero: Integer);
          begin
               case Numero of
               0 :  begin
                         PageControl.ActivePage := TabIdentifica;
                         ActiveControl := CB_CODIGO;
                    end;
               1 :  begin
                         PageControl.ActivePage := TabPersonales;
                         ActiveControl := CB_LUG_NAC;
                    end;
               2 :  begin
                         PageControl.ActivePage := TabResidencia;
                         ActiveControl := CB_CALLE;
                    end;
               3 :  begin
                         PageControl.ActivePage := TabExperiencia;
                         ActiveControl := CB_ESTUDIO;
                    end;
               4 :  begin
                         PageControl.ActivePage := TabContratacion;
                         ActiveControl := CB_FEC_ING;
                    end;
               5 :  if StrLleno( Global.NombreNivel( 1 ) ) then
                    begin
                         PageControl.ActivePage := TabArea;
                         if lCambiosPuesto then
                            ActiveControl := CB_NIVEL1;
                    end;
               6 :  begin
                         PageControl.ActivePage := TabSalario;
                         if lCambiosPuesto then
                            ActiveControl := CB_AUTOSAL;
                    end;
               7 :  begin
                         PageControl.ActivePage := TabPrestacion;
                         if LBDisponibles.Enabled then
                            ActiveControl := LBDisponibles;
                    end;
               8 :  begin
                         PageControl.ActivePage := TabOtros;
                         ActiveControl := CB_INFTIPO;
                    end;
               9 :  begin
                         PageControl.ActivePage := TabAdicionales;
                         if bPrimerAdicional then
                            ActiveControl := tcPrimerAdicional;
                    end;
               10 : begin
                         PageControl.ActivePage := TabAdicionales2;
                         if bSegundoAdicional then
                            ActiveControl := tcSegundoAdicional;
                    end;
               11 : begin
                         PageControl.ActivePage := TabVacacion;
                         if DiasGB.Enabled then
                            ActiveControl := CB_DER_PAG;
                    end;
               end;
          end;
begin
     if ( ListEmp.ItemIndex = 9 ) and ( not bPrimerAdicional ) then   // No hay Adicionales1
     begin
          ZInformation( Caption, 'No se Tienen Definidos Campos Adicionales', 0 );
          ListEmp.ItemIndex:= PageControl.ActivePageIndex;
     end;
     if ( ListEmp.ItemIndex = 10 ) and ( not bSegundoAdicional ) then   // No hay Adicionales2
     begin
          ZInformation( Caption, 'No se Tienen Definidos M�s Campos Adicionales', 0 );
          ListEmp.ItemIndex:= PageControl.ActivePageIndex;
     end;
     bRevisaID := CheckIdentificacion;
     if bRevisaID then
        ListEmp.ItemIndex := K_TAB_IDENTIFICA;
     SeleccionaPagina( ListEmp.ItemIndex );
end;
}

procedure TEmpAlta_DevEx.CB_EST_HOYClick(Sender: TObject);
begin
     with CB_EST_HOY do
     begin
          CB_EST_HORlbl.Enabled := Checked;
          CB_EST_HOR.Enabled    := Checked;
     end;
end;

procedure TEmpAlta_DevEx.CB_HABLAClick(Sender: TObject);
begin
     with CB_HABLA do
     begin
          CB_IDIOMAlbl.Enabled := Checked;
          CB_IDIOMA.Enabled    := Checked;
     end;
end;

procedure TEmpAlta_DevEx.CB_AUTOSALClick(Sender: TObject);
begin
     if not SetSalarioTabulador then
     begin
          ZError( self.Caption, 'Para Definir Salario X Tabulador, se debe Asignar la Clasificaci�n' + CR_LF + 'Revise sus Datos de Contrataci�n', 0 );
          CB_AUTOSAL.Checked:= FALSE;
     end;
     if dmCliente.Matsushita then
        ChecaRangoSalarial;
     ControlesSalarios;
end;

procedure TEmpAlta_DevEx.SaleUltimoCampo(Sender: TObject);
begin
     inherited;
     { OBJETIVO : Seleccion en el Arbol equivalente a pr�xima  pesta�a;
                 para el focus primer control}
                      MoveNextPagina( 1 );
{
     ListEmp.ItemIndex := PageControl.ActivePage.PageIndex + 1;
     if not bPrimerAdicional and  ( PageControl.ActivePage = TabOtros ) then
     begin
          ListEmp.ItemIndex := PageControl.ActivePage.PageIndex + 2;
          if not bSegundoAdicional then ListEmp.ItemIndex := ListEmp.ItemIndex + 1;
     end;
     if not bSegundoAdicional and  ( PageControl.ActivePage = TabAdicionales ) then
           ListEmp.ItemIndex := PageControl.ActivePage.PageIndex + 2;
     ListEmpClick(Sender);
}
end;

{
procedure TEmpAlta.Panel10Enter(Sender: TObject);
begin
     inherited;
     //if bSegundoAdicional then
        SaleUltimoCampo( Sender );
end;

procedure TEmpAlta.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if ( Key = VK_PRIOR ) and ( ListEmp.ItemIndex > K_TAB_IDENTIFICA ) then
     begin
          if ( PageControl.ActivePage = TabOtros ) and not LBDisponibles.Enabled then
             iNewPagina := ListEmp.ItemIndex - 2
          else
             iNewPagina := ListEmp.ItemIndex - 1;
        ListEmpClick( Sender );
        Key := 0;
     end
     else if Key = VK_NEXT then
     begin
          ListEmp.ItemIndex := ListEmp.ItemIndex + 1;
          ListEmpClick( Sender );
          Key := 0;
     end;
end;
}

procedure TEmpAlta_DevEx.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
     if ( Key = VK_PRIOR ) then
     begin
          MoveNextPagina( -1 );
          Key := 0;
     end
     else if ( Key = VK_NEXT ) then
     begin
          MoveNextPagina( 1 );
          Key := 0;
     end;
end;

procedure TEmpAlta_DevEx.CB_RANGO_SExit(Sender: TObject);
begin
     if ( CB_RANGO_S.Valor <> rRangoSal ) then      //Cambi� el Rango
     begin
          if CB_AUTOSAL.Checked then
             CB_Salario.Valor := dmRecursos.CalculaRangoSalarial( CB_RANGO_S.Valor, LbSeleccion.Items, rSalClasifi );
          rRangoSal := CB_RANGO_S.Valor;
     end;
end;


procedure TEmpAlta_DevEx.CB_FEC_NACExit(Sender: TObject);
var
   rAnnos: Real;
begin
     inherited;
     if ( CB_FEC_NAC.Valor <> 0 ) then
     begin
          rAnnos := DaysToYears( Trunc( dmCliente.FechaDefault - CB_FEC_NAC.Valor ) + 1 );
          if ( rAnnos < 15 ) or ( rAnnos > 100 ) then
          begin
               ZetaDialogo.ZWarning( self.Caption, 'La Edad debe ser de 15 a 100 a�os', 0, mbOK );
          end;
     end;
end;

procedure TEmpAlta_DevEx.PersonalesChange(Sender: TObject);
begin
     inherited;
     bRevisaID := True;
end;

procedure TEmpAlta_DevEx.CB_RFCChange(Sender: TObject);
var
   sTemporal : String;
begin
  inherited;
     with TDBEdit(Sender) do
     begin
          sTemporal := Trim(Text);
          if ( sTemporal <> Trim(dmRecursos.cdsDatosEmpleado.FieldByName( DataField ).AsString) ) then
              PersonalesChange(Sender);
     end;
end;

procedure TEmpAlta_DevEx.ZYearExit(Sender: TObject);
begin
     if ( rYearActual <> ZYear.Valor ) then
     begin
          CambiaFechaResidencia;
          rYearActual := ZYear.Valor;
     end;
end;

procedure TEmpAlta_DevEx.ZMonthExit(Sender: TObject);
begin
     if ( rMonthActual <> ZMonth.Valor ) then
     begin
          if ZMonth.Valor >= 12 then
          begin
             ZError('','Meses Tiene Que Ser Menor a 12',0);
             ActiveControl:= ZMonth;
          end
          else
          begin
               CambiaFechaResidencia;
               rMonthActual := ZMonth.Valor;
          end;
     end;
end;

procedure TEmpAlta_DevEx.bDigitoVerificadorClick(Sender: TObject);
begin
     dmRecursos.ModificaDigitoVerificador( Trim( CB_SEGSOC.Text ) );
end;

procedure TEmpAlta_DevEx.BtnBAN_ELEClick(Sender: TObject);
begin
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_BAN_ELE.text ),'CB_BAN_ELE' );
end;

{
procedure TEmpAlta.DiasGBExit(Sender: TObject);
begin
     ActiveControl := OK;
end;
}

procedure TEmpAlta_DevEx.CB_INFTIPOChange(Sender: TObject);
begin
     HabilitaCredInfo;
end;

procedure TEmpAlta_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

procedure TEmpAlta_DevEx.BtnCandidatoClick(Sender: TObject);
var
   oParametros: TZetaParams;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        oParametros := TZetaParams.Create( Self );
        try
           with oParametros do
           begin
                AddString( 'SO_APE_PAT', CB_APE_PAT.Text );
                AddString( 'SO_APE_MAT', CB_APE_MAT.Text );
                AddString( 'SO_NOMBRES', CB_NOMBRES.Text );
           end;
           dmRecursos.BuscaCandidato( oParametros );
           IniciaSexo;       // Por si cambia el sexo, para que refresque el control
           IniciaResidencia; // Por si cambia la Fecha de Residencia, para que refresque los controles
        finally
               FreeAndNil( oParametros );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEmpAlta_DevEx.CB_ID_NUMExit(Sender: TObject);
var
   sTarjeta: string;
begin
     sTarjeta := ZetaCommonTools.PreparaGafeteProximidad( CB_ID_NUM.Text );
     with dmRecursos.cdsDatosEmpleado do
     begin
          if ( sTarjeta <> FieldByName( 'CB_ID_NUM' ).AsString ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_ID_NUM' ).AsString := sTarjeta;
          end;
     end;
end;

procedure TEmpAlta_DevEx.zkcCB_INF_OLDChange(Sender: TObject);
var
   rValor: TTasa;
begin
     with zkcCB_INF_OLD do
     begin
          if( Text = aArregloTasa[0] )then
   	      rValor := 0
         else
             rValor := StrtoFloat( Text );
     end;
     with dmRecursos.cdsDatosEmpleado do
     begin
          if not Editing then
             Edit;
          FieldByName( 'CB_INF_OLD' ).AsFloat:= rValor;
     end;
end;

procedure TEmpAlta_DevEx.AsignaValorTasa;
var
   rTasa: TTasa;
begin
     rTasa := dmRecursos.cdsDatosEmpleado.FieldByName('CB_INF_OLD').AsFloat;
     if( rTasa > 0 )then
        zkcCB_INF_OLD.Text := FloattoStr( rTasa )
     else
         zkcCB_INF_OLD.ItemIndex := 0;
end;

procedure TEmpAlta_DevEx.SetControlesNeto;
begin
     lbNeto.Visible := dmRecursos.PuedeVerNeto;
     CB_NETO.Visible := lbNeto.Visible;

     if NOT lbNeto.Visible then
     begin
          if CB_NIVEL0.Visible then
          begin
               LblNivel0.Top := lbNeto.Top;
               CB_NIVEL0.Top := CB_neto.Top;
          end;
     end;
end;

procedure TEmpAlta_DevEx.CB_CONTRATValidLookup(Sender: TObject);
begin
     inherited;
     if( CB_FEC_COV.Enabled ) and ( CB_FEC_COV.Visible ) and ( CB_FEC_COV.CanFocus ) then  {OP: 07/06/08}
         Self.ActiveControl := CB_FEC_COV
end;

{ACS: Se establece la relaci�n del nivel seleccionado con el nivel que tiene relacionado
      (nivel 12 al 2, el nivel 1 no tiene relaci�n por ser el nivel inferior).}
procedure TEmpAlta_DevEx.CB_NIVEL12ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL11.Visible then
          DataSource.DataSet.FieldByName( 'CB_NIVEL11' ).AsString := dmTablas.cdsNivel12.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL11ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL10.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL10' ).AsString := dmTablas.cdsNivel11.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL10ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL9.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL9' ).AsString := dmTablas.cdsNivel10.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL9ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL8.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL8' ).AsString := dmTablas.cdsNivel9.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL8ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL7.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL7' ).AsString := dmTablas.cdsNivel8.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL7ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL6.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL6' ).AsString := dmTablas.cdsNivel7.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL6ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL5.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL5' ).AsString := dmTablas.cdsNivel6.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL5ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL4.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL4' ).AsString := dmTablas.cdsNivel5.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL4ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL3.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL3' ).AsString := dmTablas.cdsNivel4.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL3ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL2.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL2' ).AsString := dmTablas.cdsNivel3.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

procedure TEmpAlta_DevEx.CB_NIVEL2ValidLookup(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     if DataSource.DataSet.State in [dsEdit, dsInsert] then
        if StrLleno( dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString ) and CB_NIVEL1.Visible then
           DataSource.DataSet.FieldByName( 'CB_NIVEL1' ).AsString := dmTablas.cdsNivel2.FieldByName( 'TB_RELACIO' ).AsString;
     {$endif}
end;

{ACS: Este procedimiento re-ajustar� la posici�n de los controles de los niveles.}
{$ifdef ACS}
procedure TEmpAlta_DevEx.SetPosicionNiveles;
const
     K_ALTURA_DEF = 24;
     K_TOP_N1 = 272;
     K_TOP_N1_LBL = 276;
     K_TOP_N2 = 248;
     K_TOP_N2_LBL = 252;
     K_TOP_N3 = 224;
     K_TOP_N3_LBL = 228;
     K_TOP_N4 = 200;
     K_TOP_N4_LBL = 204;
     K_TOP_N5 = 176;
     K_TOP_N5_LBL = 180;
     K_TOP_N6 = 152;
     K_TOP_N6_LBL = 156;
     K_TOP_N7 = 128;
     K_TOP_N7_LBL = 132;
     K_TOP_N8 = 104;
     K_TOP_N8_LBL = 108;
     K_TOP_N9 = 80;
     K_TOP_N9_LBL = 84;
     K_TOP_N10 = 56;
     K_TOP_N10_LBL = 60;
     K_TOP_N11 = 32;
     K_TOP_N11_LBL = 36;
     K_TOP_N12 = 8;
     K_TOP_N12_LBL = 12;
var
     iNivelesNoVisibles, iTotalHorizontal: Integer;
begin
     iNivelesNoVisibles:= 0;
     {Se obtiene la cantidad total de niveles no visibles}
     if Not CB_NIVEL12.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL11.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL10.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL9.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL8.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL7.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL6.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL5.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL4.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL3.Visible then
        inc( iNivelesNoVisibles );
     if Not CB_NIVEL2.Visible then
        inc( iNivelesNoVisibles );

     iTotalHorizontal := ( iNivelesNoVisibles * K_ALTURA_DEF );
     {Se re-ajustan todos los controles en la posici�n horizontal}
     CB_NIVEL12.Top := K_TOP_N12 - iTotalHorizontal;
     CB_NIVEL12lbl.Top := K_TOP_N12_LBL - iTotalHorizontal;
     CB_NIVEL11.Top := K_TOP_N11 - iTotalHorizontal;
     CB_NIVEL11lbl.Top := K_TOP_N11_LBL - iTotalHorizontal;
     CB_NIVEL10.Top := K_TOP_N10 - iTotalHorizontal;
     CB_NIVEL10lbl.Top := K_TOP_N10_LBL - iTotalHorizontal;
     CB_NIVEL9.Top := K_TOP_N9 - iTotalHorizontal;
     CB_NIVEL9lbl.Top := K_TOP_N9_LBL - iTotalHorizontal;
     CB_NIVEL8.Top := K_TOP_N8 - iTotalHorizontal;
     CB_NIVEL8lbl.Top := K_TOP_N8_LBL - iTotalHorizontal;
     CB_NIVEL7.Top := K_TOP_N7 - iTotalHorizontal;
     CB_NIVEL7lbl.Top := K_TOP_N7_LBL - iTotalHorizontal;
     CB_NIVEL6.Top := K_TOP_N6 - iTotalHorizontal;
     CB_NIVEL6lbl.Top := K_TOP_N6_LBL - iTotalHorizontal;
     CB_NIVEL5.Top := K_TOP_N5 - iTotalHorizontal;
     CB_NIVEL5lbl.Top := K_TOP_N5_LBL - iTotalHorizontal;
     CB_NIVEL4.Top := K_TOP_N4 - iTotalHorizontal;
     CB_NIVEL4lbl.Top := K_TOP_N4_LBL - iTotalHorizontal;
     CB_NIVEL3.Top := K_TOP_N3 - iTotalHorizontal;
     CB_NIVEL3lbl.Top := K_TOP_N3_LBL - iTotalHorizontal;
     CB_NIVEL2.Top := K_TOP_N2 - iTotalHorizontal;
     CB_NIVEL2lbl.Top := K_TOP_N2_LBL - iTotalHorizontal;
     CB_NIVEL1.Top := K_TOP_N1 - iTotalHorizontal;
     CB_NIVEL1lbl.Top := K_TOP_N1_LBL - iTotalHorizontal;
end;
{$endif}

{$ifdef ACS}
{Este procedimiento sirve para eliminar cualquier OLDKEY de los lookups de niveles}
procedure TEmpAlta_DevEx.LimpiaLookUpOlfKey;
begin
          CB_NIVEL1.ResetMemory;
          CB_NIVEL2.ResetMemory;
          CB_NIVEL3.ResetMemory;
          CB_NIVEL4.ResetMemory;
          CB_NIVEL5.ResetMemory;
          CB_NIVEL6.ResetMemory;
          CB_NIVEL7.ResetMemory;
          CB_NIVEL8.ResetMemory;
          CB_NIVEL9.ResetMemory;
          CB_NIVEL10.ResetMemory;
          CB_NIVEL11.ResetMemory;
          CB_NIVEL12.ResetMemory;
end;
{$endif}

procedure TEmpAlta_DevEx.CB_DISCAPAClick(Sender: TObject);
begin
     inherited;
     CB_TDISCAP.Enabled := CB_DISCAPA.Checked; 
end;

procedure TEmpAlta_DevEx.btnVerTarjetaDespClick(Sender: TObject);
begin
     inherited;
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_CTA_VAL.text ),'CB_CTA_VAL' );
end;

procedure TEmpAlta_DevEx.btnVerTarjetaGasolinaClick(Sender: TObject);
begin
     inherited;
     dmRecursos.ValidaBancaElectronica( Caption, Trim( CB_CTA_GAS.text ),'CB_CTA_GAS' );
end;

procedure TEmpAlta_DevEx.CB_ESTADOValidKey(Sender: TObject);
begin
     inherited;
     if StrLleno(CB_ESTADO.Llave) then
     begin
          CB_MUNICIP.Filtro := Format('TB_ENTIDAD = ''%s''',[CB_ESTADO.Llave]);
     end
     else
         CB_MUNICIP.Filtro := VACIO;

    if DataSource.DataSet.State in [dsEdit,dsInsert] then
        CB_MUNICIP.Llave := VACIO;
end;

procedure TEmpAlta_DevEx.btnAsignarBioClick(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmCliente.cdsEmpleado do
     begin
          FieldByName( 'CB_ID_BIO' ).AsInteger := dmSistema.ObtenMaxIdBio;
          CB_GP_COD.Enabled := ( FieldByName( 'CB_ID_BIO' ).AsInteger <> 0 );
     end;
     {$endif}
end;

procedure TEmpAlta_DevEx.btnBorrarBioClick(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with dmCliente.cdsEmpleado do
     begin
          FieldByName( 'CB_ID_BIO' ).AsInteger := 0;
          FieldByName( 'CB_GP_COD' ).AsString := VACIO;
     end;
     CB_GP_COD.Enabled := False;
     {$endif}
end;

procedure TEmpAlta_DevEx.OK_DevExClick(Sender: TObject);
   {$ifdef DENSO}
   function SugerirNumeroEmpleado : Boolean;
   var
      iSugerido : integer;
   begin
        Global.Conectar;
        Result:=False;
        if Global.GetGlobalBooleano( K_GLOBAL_NUM_EMP_SUGIERE_ESPECIAL ) then
        begin
             iSugerido := dmRecursos.GetSugiereNumeroEmpleado( dmCliente.cdsEmpleado );

             with dmCliente.cdsEmpleado do
             begin
                  if iSugerido <> dmCliente.cdsEmpleado.FieldByName( 'CB_CODIGO' ).AsInteger then
                  begin
                      FieldByName( 'CB_CODIGO' ).AsInteger:= iSugerido;
                      ZetaDialogo.ZInformation(self.Caption, format( 'Por la numeraci�n especial, el n�mero del empleado cambi� a %d', [iSugerido] ) , 0 );
                      Result := True;
                  end;
             end;
        end;
   end;
   {$endif}
begin
   if CheckIdentificacion then
        SetPaginaEdicion( K_TAB_IDENTIFICA )
     else
     begin
          if dmCliente.EsModoDemo then Exit;
{
          if (( CB_INFTIPO.Valor ) = Ord( tiNoTiene )) then        // Ya se est� validando en BeforePost de dmcliente.cdsEmpleado
             ClearInfonavit;
}
          if CB_SALARIO.Valor < 0 then
          begin
                SeleccionaPagina( CB_SALARIO, K_TAB_SALARIO );
                ZError( self.Caption, 'Salario NO Puede Ser Menor Que Cero', 0 );
{
                PageControl.ActivePage := TabSalario;
                ListEmp.ItemIndex:= 6;
                ActiveControl := CB_SALARIO;
}
          end
          else if CB_CODIGO.Valor <= 0 then
          begin
                SeleccionaPagina( CB_CODIGO, K_TAB_IDENTIFICA );
                ZError( self.Caption , 'N�mero del Empleado No Puede Ser Cero', 0 );
{
                PageControl.ActivePage := TabIdentifica;
                ListEmp.ItemIndex:= 0;
                ActiveControl := CB_CODIGO;
}
          end
          else if ( CB_NIVEL0.Visible ) and ( strVacio( CB_NIVEL0.Llave ) ) then
          begin
                SeleccionaPagina( CB_NIVEL0, K_TAB_OTROS );
                ZError( self.Caption, 'Nivel de Confidencialidad no Puede Quedar Vacio', 0 );
{
                PageControl.ActivePage := TabOtros;
                ListEmp.ItemIndex:= 8;
                ActiveControl := CB_NIVEL0;
}
          end
          else if ( DataSource.DataSet.FieldByName( 'CB_INF_INI').AsString < DataSource.DataSet.FieldByName('CB_INF_ANT').AsString ) and
               not ZWarningConfirm('� Atenci�n !', 'Se indic� una fecha de otorgamiento mayor al inicio del descuento' + CR_LF + '�Seguro de grabar? ', 0, mbCancel) then
                  SeleccionaPagina( CB_INF_INI, K_TAB_OTROS )

          else if ( strLleno( CB_BAN_ELE.Text ) and ( dmRecursos.ValidacionForzadaCuenta( Caption, Trim( CB_BAN_ELE.text ),'CB_BAN_ELE' )) )  then
          begin
                PageControl.ActivePage := TabOtros;
                ActiveControl := CB_BAN_ELE;
          end
          else if ( strLleno( CB_CTA_GAS.Text ) and ( dmRecursos.ValidacionForzadaCuenta( Caption, Trim( CB_CTA_GAS.text ),'CB_CTA_GAS' )) ) then
          begin
                PageControl.ActivePage := TabOtros;
                ActiveControl := CB_CTA_GAS;
          end
          else if ( strLleno( CB_CTA_VAL.Text ) and ( dmRecursos.ValidacionForzadaCuenta( Caption, Trim( CB_CTA_VAL.text ),'CB_CTA_VAL' )) ) then
          begin
                PageControl.ActivePage := TabOtros;
                ActiveControl := CB_CTA_VAL;
          end
          else
          begin
               {$ifdef GRUPOSENDA_VALIDACIONCAMPOS}
               if ( strLleno( NombrePadre.Text ) and strLleno( NombreMadre.Text ) ) then
               begin
               {$endif}
                
               {$ifdef DENSO}
			   SugerirNumeroEmpleado; { DENSO }
               {$endif}
               with dmRecursos do
               begin
                    ListaPercepFijas:= GetPercepcionesFijas;       /// Percepciones Fijas
                    OtrosDatos := VarArrayOf( [ NombrePadre.Text, NombreMadre.Text ] );  // Otros Datos
                    DesconectaVaca := ZCierre.Enabled;
               end;
               // Matsushita
               if dmCliente.Matsushita and CB_AUTOSAL.Checked and ( ( CB_RANGO_S.Valor > 0 ) and ( CB_RANGO_S.Valor <> 100 ) ) then
                  CB_SALARIO.Valor := rSalClasifi;

                  inherited;

                   if dmCliente.cdsEmpleado.ChangeCount = 0 then
                      Close
                   else if dmRecursos.TipoErrorEmpleado = teLlavePrimaria then
                   begin
                        SeleccionaPagina( CB_CODIGO, K_TAB_IDENTIFICA );
                        with dmCliente.cdsEmpleado do           // el inherited pone el clientDataset en Edici�n
                             FieldByName( 'CB_CODIGO' ).AsInteger:= FieldByName( 'CB_CODIGO' ).AsInteger + 1;
                   end;
               {$ifdef GRUPOSENDA_VALIDACIONCAMPOS}
               end
               else
               begin
                    SeleccionaPagina( NombrePadre, K_TAB_PERSONALES );
                    ZError( self.Caption, 'El nombre del padre y la madre del empleado no pueden quedar vac�o.', 0 );
               end;
               {$endif}
          end;
     end;//else

end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TEmpAlta_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     for I := 0 to TabArea.ControlCount - 1 do
     begin
          if TabArea.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               TabArea.Controls[I].Left := K_WIDTH_MEDIUM_LEFT - (TabArea.Controls[I].Width+2);
          end;
          if TabArea.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( TabArea.Controls[I].Visible ) then
               begin
                    TabArea.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( TabArea.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    TabArea.Controls[I].Left := K_WIDTH_MEDIUM_LEFT;
               end;
          end;
     end;
end;
{$ifend}

procedure TEmpAlta_DevEx.SetEditarSoloActivos;
begin
     CB_CONTRAT.EditarSoloActivos := TRUE;
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_PLAZA.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     CB_PATRON.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
     CB_TABLASS.EditarSoloActivos := TRUE;
     CB_BANCO.EditarSoloActivos := TRUE;

end;

end.
