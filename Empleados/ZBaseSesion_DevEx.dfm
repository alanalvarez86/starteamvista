inherited BaseSesion_DevEx: TBaseSesion_DevEx
  Left = 307
  Top = 265
  Caption = 'BaseSesion_DevEx'
  ClientHeight = 423
  ClientWidth = 637
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 387
    Width = 637
    DesignSize = (
      637
      36)
    inherited OK_DevEx: TcxButton
      Left = 472
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 552
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 637
    inherited ValorActivo2: TPanel
      Width = 311
      inherited textoValorActivo2: TLabel
        Width = 305
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 104
    Width = 637
    Height = 283
    OnDblClick = ZetaDBGridDblClick
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 50
    Width = 637
    Height = 54
    Align = alTop
    TabOrder = 4
    object Label1: TLabel
      Left = 16
      Top = 10
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Curso:'
    end
    object CU_CODIGO: TZetaDBTextBox
      Left = 48
      Top = 8
      Width = 73
      Height = 17
      AutoSize = False
      Caption = 'CU_CODIGO'
      Color = clBtnFace
      ParentColor = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CU_CODIGO'
      DataSource = dsSesion
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CU_NOMBRE: TZetaDBTextBox
      Left = 124
      Top = 8
      Width = 315
      Height = 17
      AutoSize = False
      Caption = 'CU_NOMBRE'
      Color = clBtnFace
      ParentColor = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CU_NOMBRE'
      DataSource = dsSesion
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label2: TLabel
      Left = 5
      Top = 29
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Maestro:'
    end
    object MA_CODIGO: TZetaDBTextBox
      Left = 48
      Top = 27
      Width = 73
      Height = 17
      AutoSize = False
      Caption = 'MA_CODIGO'
      Color = clBtnFace
      ParentColor = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'MA_CODIGO'
      DataSource = dsSesion
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object MA_NOMBRE: TZetaDBTextBox
      Left = 124
      Top = 27
      Width = 315
      Height = 17
      AutoSize = False
      Caption = 'MA_NOMBRE'
      Color = clBtnFace
      ParentColor = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'MA_NOMBRE'
      DataSource = dsSesion
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lblFolio: TLabel
      Left = 498
      Top = 10
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio:'
    end
    object SE_FOLIO: TZetaDBTextBox
      Left = 525
      Top = 8
      Width = 73
      Height = 17
      AutoSize = False
      Caption = 'SE_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SE_FOLIO'
      DataSource = dsSesion
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 420
    Top = 25
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  object dsSesion: TDataSource
    Left = 432
    Top = 3
  end
end
