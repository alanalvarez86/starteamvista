unit FHisPagosIMSS_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  Grids, DBGrids, ZetaDBGrid, StdCtrls, ZBaseConsulta,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  THisPagosIMSS_DevEx = class(TBaseGridLectura_DevEx)
    LS_PATRON: TcxGridDBColumn;
    LS_YEAR: TcxGridDBColumn;
    LS_MONTH: TcxGridDBColumn;
    LS_TIPO: TcxGridDBColumn;
    LE_DIAS_CO: TcxGridDBColumn;
    LE_TOT_IMS: TcxGridDBColumn;
    LE_TOT_RET: TcxGridDBColumn;
    LE_TOT_INF: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure ConfigAgrupamiento;
  protected
    procedure Connect; override;
    procedure Refresh; override;
  public
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

var
  HisPagosIMSS_DevEx: THisPagosIMSS_DevEx;

implementation

{$R *.DFM}

uses dRecursos, ZetaCommonLists, ZetaCommonClasses;

{ THisPagosIMSS }

procedure THisPagosIMSS_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsHisPagosIMSS.Conectar;
          DataSource.DataSet:= cdsHisPagosIMSS;
     end;
end;

procedure THisPagosIMSS_DevEx.Refresh;
begin
     dmRecursos.cdsHisPagosIMSS.Refrescar;
end;

procedure THisPagosIMSS_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10137_Pagos_IMSS;
end;

function THisPagosIMSS_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar al Historial de Pagos IMSS';
end;

function THisPagosIMSS_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar en el Historial de Pagos IMSS';
end;

function THisPagosIMSS_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar en el Historial de Pagos IMSS';
end;

procedure THisPagosIMSS_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     LS_PATRON.Options.Grouping:= FALSE;
     LS_YEAR.Options.Grouping := TRUE;
     LS_MONTH.Options.Grouping:= FALSE;
     LS_TIPO.Options.Grouping:= FALSE;
     LE_DIAS_CO.Options.Grouping:= FALSE;
     LE_TOT_IMS.Options.Grouping:= FALSE;
     LE_TOT_RET.Options.Grouping:= FALSE;
     LE_TOT_INF.Options.Grouping:= FALSE;
end;

procedure THisPagosIMSS_DevEx.FormShow(Sender: TObject);
const
     K_TOTAL = 'TOTAL=';
begin
     ApplyMinWidth;
     {***Banda Sumatoria***}
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited;
     //DevEx (by am):Configuracion Especial para agrupamiento
     ConfigAgrupamiento;

end;

end.
