unit FGridCreditoInfonavit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, ZetaKeyCombo, Mask, ZetaFecha;

type
  TCreditoInfonavit = class(TBaseGridEdicion)
    zCombo: TZetaDBKeyCombo;
    zFecha: TZetaDBFecha;
    zSNCombo: TDBComboBox;
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure zSNComboKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
  protected
     procedure Connect; override;
     procedure Buscar; override;
     function PosicionaSiguienteColumna: Integer; override;
     procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  CreditoInfonavit: TCreditoInfonavit;

implementation

uses DCliente,
     DRecursos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaBuscaEmpleado;

{$R *.DFM}

procedure TCreditoInfonavit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext :=  H11621_Registro_de_Credito_Infonavit;
end;

procedure TCreditoInfonavit.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          cdsInfonavit.Refrescar;
          DataSource.DataSet:= cdsInfonavit;
     end;
end;

procedure TCreditoInfonavit.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

procedure TCreditoInfonavit.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ) then
     begin
          with dmRecursos.cdsInfonavit do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

procedure TCreditoInfonavit.KeyPress(var Key: Char);
begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'CB_INFTIPO' ) then
               begin
                    if ( zcombo.Enabled ) then
                    begin
                         zCombo.SetFocus;
                         SendMessage( zCombo.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'CB_INFMANT' ) then
               begin
                    if ( zSNCombo.Enabled ) then
                    begin
                         zSNCombo.SetFocus;
                         SendMessage( zSNCombo.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'CB_INF_INI' ) then
               begin
                    if ( zFecha.Enabled ) then
                    begin
                         zFecha.SetFocus;
                         SendMessage( zFecha.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end;
          end;
     end
     else
         if ( ActiveControl = zCombo ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   if (zCombo.Valor = Ord(tiNoTiene) ) then
                   begin
                        SelectedField := dmRecursos.cdsInfonavit.FieldByName( 'CB_CODIGO' );
                        SeleccionaSiguienteRenglon;
                   end
                   else
                       SelectedField := dmRecursos.cdsInfonavit.FieldByName( 'CB_INFCRED' );
              end;
         end
         else if ( ActiveControl = zSNCombo ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   SelectedField := dmRecursos.cdsInfonavit.FieldByName( 'CB_INF_INI' );
              end;
         end
         else if ( ActiveControl = zFecha ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   SelectedField := dmRecursos.cdsInfonavit.FieldByName( 'CB_CODIGO' );
                   SeleccionaSiguienteRenglon;
              end;
         end;
end;

procedure TCreditoInfonavit.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'CB_INFTIPO' ) and
                  ( zCombo.Visible ) then
                  zCombo.Visible:= False
               else if ( SelectedField.FieldName = 'CB_INFMANT' ) and
                       ( zSNCombo.visible ) then
                       zSNCombo.Visible := False
               else if ( SelectedField.FieldName = 'CB_INF_INI' ) and
                       ( zFecha.visible ) then
                       zFecha.Visible := False;
          end;
     end;
end;

procedure TCreditoInfonavit.ZetaDBGridDrawColumnCell(Sender: TObject;
          const Rect: TRect; DataCol: Integer; Column: TColumn;
          State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CB_INFTIPO' ) then
          begin
               with zCombo do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end
          else if ( Column.FieldName = 'CB_INFMANT' ) then
          begin
               with zSNCombo do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end
          else if ( Column.FieldName = 'CB_INF_INI' ) then
          begin
               with zFecha do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end;
     end;
end;

function TCreditoInfonavit.PosicionaSiguienteColumna: Integer;
const
     K_COLUMNA_MTTO = 6;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CB_INFTIPO' ) and ( AsInteger = Ord(tiNoTiene) ) then
              Result := PrimerColumna
          else if ( FieldName = 'CB_INFTASA' ) and ( DataSet.FieldByName('CB_INFTIPO').AsInteger <> Ord(tiPorcentaje) )then
              Result := K_COLUMNA_MTTO
          else
              Result := inherited PosicionaSiguienteColumna;
     end;
end;

procedure TCreditoInfonavit.zSNComboKeyPress(Sender: TObject; var Key: Char);
          procedure AsignaValores( const Valor: Char );
          begin
               Key := #0;
               with DataSource.DataSet do
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'CB_INFMANT' ).AsString := Valor;
               end;
          end;
begin
     inherited;
     if ( ( Key = Chr(83) ) or ( Key = Chr(115) ) ) then
          AsignaValores( K_GLOBAL_SI )
     else if ( ( Key = Chr(78) ) or ( Key = Chr(110) ) ) then
          AsignaValores( K_GLOBAL_NO );
end;

end.
