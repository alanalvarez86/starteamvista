unit FKardexVarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, 
  {$ifndef VER130}
  Variants,
  {$endif}
  ComCtrls, Mask, ZetaFecha, ZetaSmartLists, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC, dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator,
  cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, cxContainer, cxEdit,
  cxTextEdit, cxMemo;

type
  TKardexVarios_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label7: TLabel;
    ZFecha: TZetaFecha;
    PageControl: TcxPageControl;
    TabCambios: TcxTabSheet;
    LPuesto: TLabel;
    LClasificacion: TLabel;
    LTurno: TLabel;
    LRenova: TLabel;
    LNivel1: TLabel;
    Lnivel2: TLabel;
    Lnivel3: TLabel;
    LNivel4: TLabel;
    LNivel5: TLabel;
    LNivel6: TLabel;
    TabArea: TcxTabSheet;
    LNivel7: TLabel;
    LNivel8: TLabel;
    LNivel9: TLabel;
    TabGeneral: TcxTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    ZMemo: TcxMemo;
    ZDescrip: TEdit;
    CB_CONTRAT: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    ZEmpleado: TZetaKeyLookup_DevEx;
    Label10: TLabel;
    bbMostrarCalendario: TcxButton;
    LFecVencimiento: TLabel;
    CB_FEC_COV: TZetaDBFecha;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    LNivel10: TLabel;
    LNivel11: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    LNivel12: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ZEmpleadoValidKey(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    lArea : Boolean;
    sOldPuesto, sOldClasif, sOldTabula : String;
    procedure SetPosicionNiveles(oControl: TControl; const iIzquierdo: Integer; var iTope: Integer);
    procedure SetCamposNivel( var iHeight: Integer );
    procedure SetLookupsDataSet;
    procedure SetCamposAreas;
    procedure InitOldValues;
    {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
  protected
    procedure Connect;override;
    procedure EscribirCambios; override;
  public
  end;

const
     ALTURA_DEFAULT = 310;
     ALTURA_TOPE = 460;
     LEFT_LBL = 5;
     INICIO_TOP_LOOKUP_AREA = 6;
     INICIO_TOP_LABEL_AREA = 10;
     INICIO_TOP_LOOKUP_CAMBIOS = 114;
     INICIO_TOP_LABEL_CAMBIOS = 118;
     ALTURA_LOOKUP = 20;
     TOP_INCREMENTO = 23;
var
  KardexVarios_DevEx: TKardexVarios_DevEx;

implementation

uses dRecursos, dTablas, dCatalogos, dCliente, dGlobal, ZAccesosTress,
     ZetaCommonLists, ZetaCommonClasses, ZetaCommonTools, ZetaClientTools, ZetaDialogo;

{$R *.DFM}

{ TKardexVarios }

procedure TKardexVarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     LNIVEL10.Visible := True;
     LNIVEL11.Visible := True;
     LNIVEL12.Visible := True;
     {$endif}
     FirstControl := ZEmpleado;
     HelpContext:= h11516_cambios_multiples;
     SetLookupsDataSet;
     //IndexDerechos := ZAccesosTress.D_EMP_REG_CAMBIO_MULTIPLE;
     SetEditarSoloActivos;//@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigna en el DFM, Bug#15743
end;

procedure TKardexVarios_DevEx.FormShow(Sender: TObject);
begin
     SetCamposAreas;
     with dmCliente do
     begin
          ZEmpleado.SetLlaveDescripcion( IntToStr( Empleado ), GetDatosEmpleadoActivo.Nombre );  // Evita que se Haga el DoLookup del Control
          ZFecha.Valor:= FechaDefault;
          //CB_PUESTO.Filtro := GetFiltroLookupPuestos;    No se Pondr� Filtro en Lookup se validar� en OK
     end;
     PageControl.ActivePage:= TabCambios;
     inherited;
     Datasource.AutoEdit := TRUE;
     {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     cambiosVisuales;
     {$ifend}
end;

procedure TKardexVarios_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsContratos.Conectar;
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
          { Se quit� el cambio de Tabla de Prestaciones ya que no est� implementado en esta
            forma la adici�n de un cierre de vacaciones cuando se presente este caso
          cdsSSocial.Conectar; }
     end;
     with dmTablas do
     begin
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     DataSource.DataSet:= dmRecursos.cdsDatosEmpleado;
end;

procedure TKardexVarios_DevEx.EscribirCambios;
{var
   sMensaje: String;}
begin
     with dmRecursos do
     begin
          {if ( sOldPuesto <> cdsDatosEmpleado.FieldByName( 'CB_PUESTO' ).AsString ) and
             ( not ( dmCliente.EsPuestoValido( cdsDatosEmpleado.FieldByName( 'CB_PUESTO' ).AsString ) ) ) then
          begin
               ZetaDialogo.ZError( self.Caption, sMensaje, 0 );
          end
          else}
          if ( sOldPuesto = cdsDatosEmpleado.FieldByName( 'CB_PUESTO' ).AsString ) or
             ( ( dmCliente.EsPuestoValido( cdsDatosEmpleado.FieldByName( 'CB_PUESTO' ).AsString ) ) ) then
          begin
               OtrosDatos := VarArrayOf( [ZFecha.Valor, ZDescrip.Text, ZMemo.Text] );
               if zStrToBool( cdsDatosEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString ) and               // Si tiene Salario x Tabulador y ...
                  ( ( cdsDatosEmpleado.FieldByName( 'CB_CLASIFI' ).AsString <> sOldClasif ) or         // Cambi� la Clasificaci�n �
                    ( sOldTabula <> cdsDatosEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString ) ) then     // El Cambio de Puesto implica ponerle Salario x Tabulador
               begin
                    case ZSiNoCancel( 'Cambio de Clasificaci�n', 'Empleado tiene Tabulador de Salario. Desea Generar cambio de Salario ?', 0, mbYes ) of
                         mrYes :
                         begin
                              if ValidaSalario( cdsDatosEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat,
                                                GetSalClasifi( cdsDatosEmpleado.FieldByName( 'CB_CLASIFI' ).AsString ) ) then
                                 inherited;
                         end;
                         mrNo :
                         begin
                              cdsDatosEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                              inherited;
                         end;
                    end;
                    // cancel equivale a no hacer el inherited.....
               end
               else
                    inherited;
          end;
     end;
end;

procedure TKardexVarios_DevEx.SetLookupsDataSet;
begin
     { Asignar Propiedad LookupDataSet }
     ZEmpleado.LookupDataSet := dmCliente.cdsEmpleadoLookUp;
     with dmCatalogos do
     begin
          CB_PUESTO.LookupDataSet := cdsPuestos;
          CB_CLASIFI.LookupDataSet := cdsClasifi;
          CB_TURNO.LookupDataSet := cdsTurnos;
          CB_CONTRAT.LookupDataSet := cdsContratos;
     end;
     with dmTablas do
     begin
          CB_NIVEL1.LookupDataSet := cdsNivel1;
          CB_NIVEL2.LookupDataSet := cdsNivel2;
          CB_NIVEL3.LookupDataSet := cdsNivel3;
          CB_NIVEL4.LookupDataSet := cdsNivel4;
          CB_NIVEL5.LookupDataSet := cdsNivel5;
          CB_NIVEL6.LookupDataSet := cdsNivel6;
          CB_NIVEL7.LookupDataSet := cdsNivel7;
          CB_NIVEL8.LookupDataSet := cdsNivel8;
          CB_NIVEL9.LookupDataSet := cdsNivel9;
          {$ifdef ACS}
          CB_NIVEL10.LookupDataset := cdsNivel10;
          CB_NIVEL11.LookupDataset := cdsNivel11;
          CB_NIVEL12.LookupDataset := cdsNivel12;
          {$endif}
     end;
end;

procedure TKardexVarios_DevEx.SetCamposAreas;
var
   iHeight, iLeftLookup, iLeftLabel: Integer;
   iTopLookup, iTopLabel: Integer;
begin
     iHeight := ZetaClientTools.GetScaledHeight( ALTURA_DEFAULT );
     iLeftLookup := CB_PUESTO.Left;
     iLeftLabel := ZetaClientTools.GetScaledHeight(LEFT_LBL);

     SetCamposNivel( iHeight );

     lArea:= ( CB_NIVEL7.Visible ) or ( CB_NIVEL8.Visible ) or ( CB_NIVEL9.Visible );

     if lArea then
     begin
          self.Height := ZetaClientTools.GetScaledHeight( ALTURA_TOPE ) + 25 ;
          iTopLookup := ZetaClientTools.GetScaledHeight( INICIO_TOP_LOOKUP_AREA );
          iTopLabel := ZetaClientTools.GetScaledHeight( INICIO_TOP_LABEL_AREA );
     end
     else
     begin
          self.Height := iHeight + 25 ;
          iTopLookup := ZetaClientTools.GetScaledHeight( INICIO_TOP_LOOKUP_CAMBIOS );
          iTopLabel := ZetaClientTools.GetScaledHeight( INICIO_TOP_LABEL_CAMBIOS );
     end;
     SetPosicionNiveles( CB_NIVEL1, iLeftLookup, iTopLookup );
     SetPosicionNiveles( LNivel1, iLeftLabel, iTopLabel );
     SetPosicionNiveles( CB_NIVEL2, iLeftLookup, iTopLookup );
     SetPosicionNiveles( LNivel2, iLeftLabel, iTopLabel );
     SetPosicionNiveles( CB_NIVEL3, iLeftLookup, iTopLookup );
     SetPosicionNiveles( LNivel3, iLeftLabel, iTopLabel );
     SetPosicionNiveles( CB_NIVEL4, iLeftLookup, iTopLookup );
     SetPosicionNiveles( LNivel4, iLeftLabel, iTopLabel );
     SetPosicionNiveles( CB_NIVEL5, iLeftLookup, iTopLookup );
     SetPosicionNiveles( LNivel5, iLeftLabel, iTopLabel );
     SetPosicionNiveles( CB_NIVEL6, iLeftLookup, iTopLookup );
     SetPosicionNiveles( LNivel6, iLeftLabel, iTopLabel );

     TabArea.Visible    := lArea;
     TabArea.TabVisible := lArea;

end;

procedure TKardexVarios_DevEx.SetCamposNivel( var iHeight: Integer );
var
   iNiveles : Integer;

   procedure SetAltura( const lEnabled: Boolean );
   begin
        if lEnabled then
           iHeight := iHeight + ZetaClientTools.GetScaledHeight( ALTURA_LOOKUP );
   end;

begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, LNivel1, CB_NIVEL1 ); SetAltura( LNivel1.Visible );
     SetCampoNivel( 2, iNiveles, LNivel2, CB_NIVEL2 ); SetAltura( LNivel2.Visible );
     SetCampoNivel( 3, iNiveles, LNivel3, CB_NIVEL3 ); SetAltura( LNivel3.Visible );
     SetCampoNivel( 4, iNiveles, LNivel4, CB_NIVEL4 ); SetAltura( LNivel4.Visible );
     SetCampoNivel( 5, iNiveles, LNivel5, CB_NIVEL5 ); SetAltura( LNivel5.Visible );
     SetCampoNivel( 6, iNiveles, LNivel6, CB_NIVEL6 ); SetAltura( LNivel6.Visible );
     SetCampoNivel( 7, iNiveles, LNivel7, CB_NIVEL7 ); SetAltura( LNivel7.Visible );
     SetCampoNivel( 8, iNiveles, LNivel8, CB_NIVEL8 ); SetAltura( LNivel8.Visible );
     SetCampoNivel( 9, iNiveles, LNivel9, CB_NIVEL9 ); SetAltura( LNivel9.Visible );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, LNivel10, CB_NIVEL10 );SetAltura( LNivel10.Visible );
     SetCampoNivel( 11, iNiveles, LNivel11, CB_NIVEL11 );SetAltura( LNivel11.Visible );
     SetCampoNivel( 12, iNiveles, LNivel12, CB_NIVEL12 );SetAltura( LNivel12.Visible );
     {$endif}

end;

procedure TKardexVarios_DevEx.SetPosicionNiveles(oControl: TControl; const iIzquierdo: Integer; var iTope: Integer);
begin
     with oControl do
     begin
          if lArea then
             Parent := TabArea
          else
             Parent := TabCambios;
          Left   := iIzquierdo;
          Top    := iTope;
     end;
     iTope := iTope + ZetaClientTools.GetScaledHeight( TOP_INCREMENTO );
end;

procedure TKardexVarios_DevEx.ZEmpleadoValidKey(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          cdsEmpleado.DisableControls;
          try
             if( SetEmpleadoNumero( ZEmpleado.Valor ) )then
                InitOldValues   { Invoca que se refresque el valor de OldValues }
             else
                 ZetaDialogo.zInformation( 'Error', '! Empleado No Encontrado !', 0 );

             {if ( not SetEmpleadoNumero( ZEmpleado.Valor ) ) then
                ZetaDialogo.zInformation( 'Error', '! Empleado No Encontrado !', 0 );}
          finally
             cdsEmpleado.EnableControls;
             Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TKardexVarios_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     ZEmpleado.SetFocus;
end;

procedure TKardexVarios_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Field ) then
     begin
          if ( Field.FieldName = 'CB_PUESTO' ) then
             dmCliente.SetDatosPuestoMultiples;
     end;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with DataSource.DataSet do
               bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
     if ( Field <> nil ) and ( Field.FieldName = 'CB_CONTRAT' ) then
          CB_FEC_COV.Enabled := ( dmRecursos.cdsDatosEmpleado.FieldByName( 'CB_FEC_COV' ).AsDateTime <> NullDateTime );

end;

procedure TKardexVarios_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     InitOldValues; { Invoca que se refresque el valor de OldValues }
     {with DataSource do
     begin
          if Assigned( DataSet ) and ( DataSet.State = dsBrowse ) then
          begin
               sOldPuesto := DataSet.FieldByName( 'CB_PUESTO' ).AsString;
               sOldClasif := DataSet.FieldByName( 'CB_CLASIFI' ).AsString;
               sOldTabula := DataSet.FieldByName( 'CB_AUTOSAL' ).AsString;
          end;
     end;}
end;

procedure TKardexVarios_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

procedure TKardexVarios_DevEx.InitOldValues;
begin
     with DataSource do
     begin
          if Assigned( DataSet ) and ( DataSet.State = dsBrowse ) then
          begin
               sOldPuesto := DataSet.FieldByName( 'CB_PUESTO' ).AsString;
               sOldClasif := DataSet.FieldByName( 'CB_CLASIFI' ).AsString;
               sOldTabula := DataSet.FieldByName( 'CB_AUTOSAL' ).AsString;
          end;
     end;
end;

procedure TKardexVarios_DevEx.OK_DevExClick(Sender: TObject);
begin
 inherited;
     ZEmpleado.SetFocus;
end;


{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TKardexVarios_DevEx.CambiosVisuales;
var
  I,J, K : Integer;
begin
     KardexVarios_DevEx.Width:= K_WIDTH_SMALLFORM;
     for I := 0 to TabArea.ControlCount - 1 do
     begin
          if TabArea.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               if ( not(  (TabCambios.Controls[I].Name = 'LRenova' ) ) ) then
               begin
                    TabArea.Controls[I].Left := K_WIDTH_SMALL_LEFT - (TabArea.Controls[I].Width+2);
               end;
          end;
          if TabArea.Controls[I].ClassNameIs( 'TZetaDBKeyLookup_DevEx' ) then
          begin

                    if ( TabArea.Controls[I].Visible ) then
                    begin
                         TabArea.Controls[I].Width := K_WIDTH_LOOKUP;
                         TZetaDBKeyLookup_DevEx( TabArea.Controls[I]).WidthLlave :=  K_WIDTHLLAVE;
                         TabArea.Controls[I].Left := K_WIDTH_SMALL_LEFT;
                    end;
          end;
          // Cuando los niveles son mayores a 6 todos los controles se colocan en otra pestania
         if ( (I = 10 )  and  ( Global.NumNiveles  > 6)  )then
         begin
              for k := 12 to Global.NumNiveles+ 14  do
              begin
                   if ( TabArea.Controls[k].Visible ) then
                   begin
                         TabArea.Controls[k].Width := K_WIDTH_LOOKUP;
                         TZetaDBKeyLookup_DevEx( TabArea.Controls[k]).WidthLlave := K_WIDTHLLAVE;
                         TabArea.Controls[k].Left := K_WIDTH_SMALL_LEFT;
                   end;
              end;
              break;
         end;
     end;

     for J := 0 to TabCambios.ControlCount - 1 do
     begin
          if TabCambios.Controls[J].ClassNameIs( 'TLabel' ) then
          begin
               if ( not(  (TabCambios.Controls[J].Name = 'LRENOVA' ) ) ) then
               begin
                    TabCambios.Controls[J].Left := K_WIDTH_SMALL_LEFT - (TabCambios.Controls[J].Width+2);
               end;
          end;
          if TabCambios.Controls[J].ClassNameIs( 'TZetaDBKeyLookup_DevEx' ) then
          begin
               if ( not(  (TabCambios.Controls[J].Name = 'CB_PUESTO' ) or  (TabCambios.Controls[J].Name = 'CB_CLASIFI' ) or  (TabCambios.Controls[J].Name = 'CB_TURNO') or  (TabCambios.Controls[J].Name = 'CB_CONTRAT' )   )  ) then
               begin
                    if ( TabCambios.Controls[J].Visible ) then
                    begin
                         TabCambios.Controls[J].Width := K_WIDTH_LOOKUP;
                         TZetaDBKeyLookup_DevEx( TabCambios.Controls[J]).WidthLlave := K_WIDTHLLAVE;
                         TabCambios.Controls[J].Left := K_WIDTH_SMALL_LEFT;
                    end;
               end;
          end;
     end;
end;
{$ifend}

procedure TKardexVarios_DevEx.SetEditarSoloActivos;
begin
     CB_CONTRAT.EditarSoloActivos := TRUE;
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;

end.
