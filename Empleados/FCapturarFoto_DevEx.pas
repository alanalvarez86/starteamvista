unit FCapturarFoto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, Videocap, StdCtrls, Mask, DBCtrls, db,
  ExtCtrls, ZetaCommonClasses, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, ImgList,
  cxButtons, imageenview, ieview, imageenio;

type
  TCapturaFoto_DevEx = class(TZetaDlgModal_DevEx)
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    lblDriver: TLabel;
    IM_TIPO: TDBComboBox;
    IM_OBSERVA: TDBEdit;
    cbDrivers: TComboBox;
    Panel1: TPanel;
    pnValorActivo: TPanel;
    btnCapturar: TcxButton;
    btnVideo: TcxButton;
    DataSource1: TDataSource;
    Timer: TTimer;
    FOTO: TImageEnView;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCapturarClick(Sender: TObject);
    procedure cbDriversChange(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerTimer(Sender: TObject);
  private
    { Private declarations }
    Nombre: String;
    FCierraForma: Boolean;
    function DriverManager: Boolean;
    function InicializaVideo: Boolean;
    procedure SetControls(lHabilita: Boolean);
    procedure TomaFotografia;

  public
    { Public declarations }
  end;

var
  CapturaFoto_DevEx: TCapturaFoto_DevEx;

function CapturarImagen( const sNombre: String ): Boolean;

implementation

uses dRecursos, ZetaDialogo, ZAccesosTress, ZAccesosMgr,
     FToolsImageEn, DGlobal, ZGlobalTress, ZetaCommonTools;

{$R *.DFM}

function CapturarImagen( const sNombre: String ): Boolean;
begin
     try
        if not Assigned( CapturaFoto_DevEx ) then
        begin
             CapturaFoto_DevEx := TCapturaFoto_DevEx.Create( Application );
        end;
        with CapturaFoto_DevEx do
        begin
             Nombre := sNombre;
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TCapturaFoto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FCierraForma := False;
     HelpContext:= H10122_Expediente_Foto_empleado;
     DataSource1.DataSet := dmRecursos.cdsEmpFoto;
end;

procedure TCapturaFoto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmRecursos.cdsEmpFoto.Conectar;
     pnValorActivo.Caption := Nombre;
     if DriverManager then
     begin
          SetControls( False );
          FCierraForma :=  not InicializaVideo;
          Timer.Enabled := FCierraForma;
     end
     else
     begin
          FCierraForma := TRUE;
          SetControls( True );
          Timer.Enabled := FCierraForma;
     end;
end;

function TCapturaFoto_DevEx.DriverManager: Boolean;
begin
     Result := True;
     cbDrivers.Items.Assign( FOTO.IO.DShowParams.VideoInputs );
     with cbDrivers.Items do
     begin
          if( Count > 0 )then
          begin
               cbDrivers.ItemIndex := 0;
               cbDrivers.Visible := ( Count > 1 );
               lblDriver.Visible := ( Count > 1 );
               FToolsImageEn.SetDriver( FOTO, cbDrivers.ItemIndex );
          end
          else
          begin
               Result := False;
               cbDrivers.Visible := False;
               lblDriver.Visible := False;
               OK_DevEx.Enabled := False;
               ZetaDialogo.ZError( 'Error al capturar imagen', 'No se tienen dispositivos de video instalados', 0 );
          end;
     end;
end;

procedure TCapturaFoto_DevEx.cbDriversChange(Sender: TObject);
begin
     inherited;
     FToolsImageEn.SetDriver( FOTO, cbDrivers.ItemIndex );
     InicializaVideo;
end;

function TCapturaFoto_DevEx.InicializaVideo: Boolean;
var
   oCursor: TCursor;
begin
     Result := TRUE;
     OK_DevEx.Enabled := TRUE;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with FOTO.IO.DShowParams do
           begin
                Connect;
                Run;
           end;
           SetControls( FALSE );
        except
              on Error: Exception do
              begin
                   SetControls( TRUE );
                   OK_DevEx.Enabled := False;
                   ZetaDialogo.ZError( 'Error al capturar la imagen', 'El Dispositivo seleccionado no est� disponible', 0 );
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCapturaFoto_DevEx.btnCapturarClick(Sender: TObject);
begin
     inherited;
     TomaFotografia;
end;

procedure TCapturaFoto_DevEx.TomaFotografia;
var
   sMsgError: String;
begin
     SetControls( True );
     with Foto.IO.DShowParams do
     begin
          GetSample( Foto.IEBitmap );
          Stop;
          Disconnect;
          Update;
     end;
     if ( not Global.GetGlobalBooleano( K_GLOBAL_CONSERVAR_TAM_FOTOS ) ) then
     begin
          if ( not FToolsImageEn.ResizeImagen( FOTO, sMsgError ) ) then
             ZError( self.Caption, sMsgError, 0);
     end;
     FToolsImageEn.AsignaImagenABlob( FOTO, dmRecursos.cdsEmpFoto, 'IM_BLOB' );
end;

procedure TCapturaFoto_DevEx.btnVideoClick(Sender: TObject);
begin
     inherited;
     SetControls( True );
     dmRecursos.cdsEmpFoto.CancelUpdates;
     InicializaVideo;
end;

procedure TCapturaFoto_DevEx.SetControls( lHabilita: Boolean );
begin
     btnVideo.Enabled := lHabilita;
     btnCapturar.Enabled := not lHabilita;
end;

procedure TCapturaFoto_DevEx.OK_DevExClick(Sender: TObject);
begin
     if( btnCapturar.Enabled )then
         TomaFotografia;
     inherited;
end;

procedure TCapturaFoto_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     with FOTO.IO.DShowParams do
     begin
          Stop;
          Disconnect;
          Update;
     end;
end;

procedure TCapturaFoto_DevEx.TimerTimer(Sender: TObject);
begin
     inherited;
     if FCierraForma then
     begin
          Timer.Enabled := False;
          Close;
     end;
end;

end.
