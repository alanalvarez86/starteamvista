unit FEmpDocumento_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls, Menus,
  Buttons,ZbasegridLectura_DevEx, StdCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, 
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ActnList, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEmpDocumento_DevEx = class(TBaseGridLectura_DevEx)
    pmVerDocumento: TPopupMenu;
    DescargarDocumento: TMenuItem;
    pnlVerDocumentos: TPanel;
    btnVerDocumento_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure DescargarDocumentoClick(Sender: TObject);
    procedure btnVerDocumentoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  EmpDocumento_DevEx: TEmpDocumento_DevEx;

implementation

{$R *.dfm}

uses dRecursos, dSistema, ZetaCommonLists, ZetaCommonClasses ;

{ TEmpDocumento }

procedure TEmpDocumento_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H00000_Expediente_Documento_Empleado;
end;

procedure TEmpDocumento_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsDocumento.Conectar;
          DataSource.DataSet:= cdsDocumento;
          pmVerDocumento.Items[0].Visible := ( DataSource.DataSet.RecordCount > 0 );
     end;

end;

procedure TEmpDocumento_DevEx.Agregar;
begin
     dmRecursos.cdsDocumento.Agregar
end;

procedure TEmpDocumento_DevEx.Borrar;
begin
     dmRecursos.cdsDocumento.Borrar
end;

procedure TEmpDocumento_DevEx.Modificar;
begin
     dmRecursos.cdsDocumento.Modificar
end;

procedure TEmpDocumento_DevEx.Refresh;
begin
     dmRecursos.cdsDocumento.Refrescar;
end;

procedure TEmpDocumento_DevEx.DescargarDocumentoClick(Sender: TObject);
begin
     inherited;
     dmRecursos.AbreDocumentoExpediente;
end;

procedure TEmpDocumento_DevEx.btnVerDocumentoClick(Sender: TObject);
begin
  inherited;
  dmRecursos.AbreDocumentoExpediente;
end;

procedure TEmpDocumento_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
 ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := true;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  //CreaColumaSumatoria('DO_TIPO',Skcount);
end;

end.
