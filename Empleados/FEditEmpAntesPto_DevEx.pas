unit FEditEmpAntesPto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaNumero, ZetaFecha, StdCtrls, Mask, DBCtrls, Db, ExtCtrls, Buttons,
  ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons,
  dxSkinsDefaultPainters;

type
  TEditEmpAntesPto_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    AP_PUESTO: TDBEdit;
    AP_FEC_INI: TZetaDBFecha;
    AP_EMPRESA: TDBEdit;
    AP_FOLIO: TZetaDBNumero;
    Label5: TLabel;
    AP_FEC_FIN: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect; override;
  public
  end;

var
  EditEmpAntesPto_DevEx: TEditEmpAntesPto_DevEx;

implementation

uses dRecursos, ZetaCommonClasses, ZAccesosTress, ZetaCommonLists;
{$R *.DFM}

procedure TEditEmpAntesPto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_CURR_PUESTOS;
     FirstControl := AP_FOLIO;
     HelpContext:= H10126_Expediente_Puestos_anteriores;
     TipoValorActivo1 := stEmpleado;
end;

procedure TEditEmpAntesPto_DevEx.Connect;
begin
     with dmRecursos do
     begin
          DataSource.DataSet:= cdsEmpAntesPto;
     end;
end;

end.
