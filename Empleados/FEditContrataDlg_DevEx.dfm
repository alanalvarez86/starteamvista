inherited EditContrataDlg_DevEx: TEditContrataDlg_DevEx
  Left = 527
  Top = 198
  ActiveControl = Operacion
  Caption = 'Modificaci'#243'n de Contrataci'#243'n'
  ClientHeight = 244
  ClientWidth = 270
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 208
    Width = 270
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 106
      Top = 5
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 185
      Top = 5
      Hint = 'Salir'
      Cancel = True
      Caption = '&Salir'
      ModalResult = 0
      OnClick = SalirClick
    end
  end
  object Tipo: TcxRadioGroup [1]
    Left = 42
    Top = 72
    Caption = 'Tipo de Movimiento'
    Properties.Items = <
      item
        Caption = 'Ingreso / Registro Patronal'
        Value = '0'
      end
      item
        Caption = 'Contrato'
        Value = '1'
      end
      item
        Caption = 'Puesto / Clasificaci'#243'n'
        Value = '2'
      end
      item
        Caption = 'Turno'
        Value = '3'
      end
      item
        Caption = 'Tipo de N'#243'mina'
        Value = '4'
      end>
    ItemIndex = 0
    TabOrder = 1
    Height = 120
    Width = 185
  end
  object Operacion: TcxRadioGroup [2]
    Left = 42
    Top = 3
    Caption = 'Operaci'#243'n'
    Properties.Items = <
      item
        Caption = 'Corregir Error de Captura'
        Value = '0'
      end
      item
        Caption = 'Generar Movimiento Nuevo'
        Value = '1'
      end>
    ItemIndex = 0
    TabOrder = 0
    Height = 60
    Width = 185
  end
  object TipoPlaza: TcxRadioGroup [3]
    Left = 42
    Top = 80
    Caption = 'Tipo de Movimiento'
    Properties.Items = <
      item
        Caption = 'Ingreso'
        Value = '0'
      end
      item
        Caption = 'Contrato'
        Value = '1'
      end
      item
        Caption = 'Clasificaci'#243'n'
        Value = '2'
      end
      item
        Caption = 'Plaza'
        Value = '3'
      end
      item
        Caption = 'Tipo de N'#243'mina'
        Value = '4'
      end>
    ItemIndex = 0
    TabOrder = 2
    Height = 113
    Width = 185
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
