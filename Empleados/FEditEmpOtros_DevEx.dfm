inherited EditEmpOtros_DevEx: TEditEmpOtros_DevEx
  Left = 587
  Top = 273
  Caption = 'Otros Datos del Empleado'
  ClientHeight = 567
  ClientWidth = 514
  ExplicitWidth = 520
  ExplicitHeight = 596
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 15
    Top = 86
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Biom'#233'trico:'
    Visible = False
  end
  object ZetaDBTextBox1: TZetaDBTextBox [1]
    Left = 111
    Top = 83
    Width = 148
    Height = 21
    AutoSize = False
    Caption = 'ZetaDBTextBox1'
    ShowAccelChar = False
    Visible = False
    Brush.Color = clBtnFace
    Border = True
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 531
    Width = 514
    ExplicitTop = 531
    ExplicitWidth = 514
    inherited OK_DevEx: TcxButton
      Left = 350
      ExplicitLeft = 350
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 429
      ExplicitLeft = 429
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 514
    ExplicitWidth = 514
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Width = 188
      ExplicitWidth = 188
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 182
        ExplicitLeft = 102
      end
    end
  end
  object pcOtros: TcxPageControl [5]
    Left = 0
    Top = 50
    Width = 514
    Height = 481
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = tsAsistencia
    Properties.CustomButtons.Buttons = <>
    Properties.MultiLine = True
    OnChange = pcOtrosChange
    ClientRectBottom = 479
    ClientRectLeft = 2
    ClientRectRight = 512
    ClientRectTop = 52
    object tsInfonavit: TcxTabSheet
      Caption = 'Infonavit'
      ImageIndex = 0
      object CB_INF_INILbl: TLabel
        Left = 24
        Top = 13
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio de Descuento:'
      end
      object CB_INF_ANTLbl: TLabel
        Left = 5
        Top = 37
        Width = 117
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otorgamiento de Cr'#233'dito:'
      end
      object CB_INFCREDLbl: TLabel
        Left = 61
        Top = 60
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = '# de Cr'#233'dito:'
      end
      object CB_INFTIPOlbl: TLabel
        Left = 43
        Top = 81
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo Descuento:'
      end
      object CB_INFTASALbl: TLabel
        Left = 40
        Top = 103
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Valor Descuento:'
      end
      object LblDescuento: TLabel
        Left = 215
        Top = 120
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'LblDescuento'
        Visible = False
      end
      object CB_INF_OLDLbl: TLabel
        Left = 247
        Top = 127
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tasa Anterior:'
        Visible = False
      end
      object lblPorcentaje: TLabel
        Left = 285
        Top = 119
        Width = 8
        Height = 13
        Alignment = taRightJustify
        Caption = '%'
        Visible = False
      end
      object CB_INF_INI: TZetaDBFecha
        Left = 126
        Top = 9
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '15/Dec/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_INF_INI'
        DataSource = DataSource
      end
      object CB_INF_ANT: TZetaDBFecha
        Left = 126
        Top = 33
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '15/Dec/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_INF_ANT'
        DataSource = DataSource
      end
      object CB_INFCRED: TDBEdit
        Left = 125
        Top = 56
        Width = 190
        Height = 21
        DataField = 'CB_INFCRED'
        DataSource = DataSource
        TabOrder = 2
      end
      object CB_INFTIPO: TZetaDBKeyCombo
        Left = 125
        Top = 78
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 3
        OnChange = CB_INFTIPOChange
        ListaFija = lfTipoInfonavit
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CB_INFTIPO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object CB_INFTASA: TZetaDBNumero
        Left = 125
        Top = 100
        Width = 97
        Height = 21
        Mascara = mnTasa
        TabOrder = 4
        Text = '0.0 %'
        DataField = 'CB_INFTASA'
        DataSource = DataSource
      end
      object CB_INFDISM: TDBCheckBox
        Left = 22
        Top = 121
        Width = 116
        Height = 17
        Alignment = taLeftJustify
        BiDiMode = bdLeftToRight
        Caption = 'Disminuci'#243'n Tasa %:'
        DataField = 'CB_INFDISM'
        DataSource = DataSource
        ParentBiDiMode = False
        ReadOnly = True
        TabOrder = 5
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_INFACT: TDBCheckBox
        Left = 52
        Top = 137
        Width = 86
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Cr'#233'dito Activo:'
        DataField = 'CB_INFACT'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 6
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object CB_INFMANT: TDBCheckBox
        Left = 256
        Top = 108
        Width = 44
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Mantenimiento:'
        DataField = 'CB_INFMANT'
        DataSource = DataSource
        TabOrder = 7
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        Visible = False
      end
      object zkcCB_INF_OLD: TZetaKeyCombo
        Left = 280
        Top = 119
        Width = 24
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 8
        Visible = False
        OnChange = zkcCB_INF_OLDChange
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    object tsFonacot: TcxTabSheet
      Caption = 'Fonacot'
      ImageIndex = 3
      object Label3: TLabel
        Left = 8
        Top = 19
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero de Fonacot:'
      end
      object CB_FONACOT: TDBEdit
        Left = 113
        Top = 15
        Width = 151
        Height = 21
        DataField = 'CB_FONACOT'
        DataSource = DataSource
        TabOrder = 0
      end
    end
    object tsPPrimerEmpleo: TcxTabSheet
      Caption = 'Programa de Primer Empleo'
      ImageIndex = 4
      object CB_EMPLEO: TDBCheckBox
        Left = 8
        Top = 12
        Width = 111
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Aplica al Programa:'
        DataField = 'CB_EMPLEO'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object tsAsistencia: TcxTabSheet
      Caption = 'Asistencia'
      ImageIndex = 1
      ExplicitLeft = 3
      ExplicitTop = 53
      object LTipoCreden: TLabel
        Left = 19
        Top = 15
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Letra Credencial:'
        Visible = False
      end
      object CB_CREDENC: TDBEdit
        Left = 104
        Top = 11
        Width = 30
        Height = 21
        CharCase = ecUpperCase
        DataField = 'CB_CREDENC'
        DataSource = DataSource
        TabOrder = 0
        Visible = False
      end
      object CB_CHECA: TDBCheckBox
        Left = 15
        Top = 37
        Width = 103
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Checa asistencia: '
        DataField = 'CB_CHECA'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object tsEvaluacion: TcxTabSheet
      Caption = 'Evaluaci'#243'n'
      ImageIndex = 5
      object LFechaEva: TLabel
        Left = 25
        Top = 20
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
      end
      object LEvalua: TLabel
        Left = 7
        Top = 43
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Resultado:'
      end
      object LProxima: TLabel
        Left = 18
        Top = 68
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pr'#243'xima:'
      end
      object CB_LAST_EV: TZetaDBFecha
        Left = 64
        Top = 15
        Width = 115
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 0
        Text = '15/Dec/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_LAST_EV'
        DataSource = DataSource
      end
      object CB_EVALUA: TZetaDBNumero
        Left = 64
        Top = 39
        Width = 80
        Height = 21
        Mascara = mnPesosDiario
        TabOrder = 1
        Text = '0.00'
        DataField = 'CB_EVALUA'
        DataSource = DataSource
      end
      object CB_NEXT_EV: TZetaDBFecha
        Left = 64
        Top = 63
        Width = 115
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 2
        Text = '15/Dec/97'
        Valor = 35779.000000000000000000
        DataField = 'CB_NEXT_EV'
        DataSource = DataSource
      end
    end
    object tsCuentas: TcxTabSheet
      Caption = 'Cuentas'
      ImageIndex = 2
      object lblTarjetasGasolina: TLabel
        Left = 22
        Top = 84
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tarjeta Gasolina:'
      end
      object lblTarjetasDespensa: TLabel
        Left = 15
        Top = 112
        Width = 87
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tarjeta Despensa:'
      end
      object Label2: TLabel
        Left = 47
        Top = 136
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subcuenta:'
      end
      object lbNeto: TLabel
        Left = 7
        Top = 160
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Neto (Piramidaci'#243'n):'
      end
      object btnTarjetaDespensa: TcxButton
        Left = 319
        Top = 108
        Width = 21
        Height = 21
        Hint = 
          'Verificar si el N'#250'mero de Tarjeta de Despensa Pertenece a otro E' +
          'mpleado'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFBAEDD5FFCEF2E1FF84DFB3FFE9F9F1FFE9F9
          F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE1F7ECFF7CDDAEFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF7FDEB0FFFFFFFFFFFFFF
          FFFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF69D8A2FFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFBDEED6FFD3F4E4FFE4F8EEFFC0EFD8FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFEFCFF5ED6
          9BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
          E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF58D498FFF4FCF8FFA2E7C6FF5BD59AFF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF8CE2B8FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF71DAA7FF8AE1B7FFB2EBD0FFD0F3E2FF5ED69BFF92E3
          BCFF92E3BCFF92E3BCFF92E3BCFF8FE2BAFF50D293FFD3F4E4FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF53D3
          95FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF92E3BCFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFADEACCFFF7FDFAFFFFFFFFFFFFFFFFFFCBF2DFFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9
          C9FF9DE6C2FFB2EBD0FF5BD59AFF92E3BCFFAAE9CAFFFFFFFFFFFFFFFFFFFFFF
          FFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFA8E9C9FF87E0B5FF97E4BFFF5ED69BFF92E3BCFFADEA
          CCFFFFFFFFFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF69D8A2FFC8F1DDFF53D395FF50D2
          93FF50D293FF50D293FF7CDDAEFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF9DE6C2FFE1F7ECFFE9F9F1FFE9F9F1FFE9F9F1FFEFFBF5FFDEF7EBFF60D6
          9DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        TabStop = False
        OnClick = btnTarjetaDespensaClick
      end
      object btnTarjetaGasolina: TcxButton
        Left = 319
        Top = 80
        Width = 21
        Height = 21
        Hint = 
          'Verificar si el N'#250'mero de Tarjeta de Gasolina Pertenece a otro E' +
          'mpleado'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A40500000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFBAEDD5FFCEF2E1FF84DFB3FFE9F9F1FFE9F9
          F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE1F7ECFF7CDDAEFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF7FDEB0FFFFFFFFFFFFFF
          FFFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF69D8A2FFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFBDEED6FFD3F4E4FFE4F8EEFFC0EFD8FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFEFCFF5ED6
          9BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
          E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF58D498FFF4FCF8FFA2E7C6FF5BD59AFF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF8CE2B8FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF71DAA7FF8AE1B7FFB2EBD0FFD0F3E2FF5ED69BFF92E3
          BCFF92E3BCFF92E3BCFF92E3BCFF8FE2BAFF50D293FFD3F4E4FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF53D3
          95FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF92E3BCFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFADEACCFFF7FDFAFFFFFFFFFFFFFFFFFFCBF2DFFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9
          C9FF9DE6C2FFB2EBD0FF5BD59AFF92E3BCFFAAE9CAFFFFFFFFFFFFFFFFFFFFFF
          FFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFFA8E9C9FF87E0B5FF97E4BFFF5ED69BFF92E3BCFFADEA
          CCFFFFFFFFFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF69D8A2FFC8F1DDFF53D395FF50D2
          93FF50D293FF50D293FF7CDDAEFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF9DE6C2FFE1F7ECFFE9F9F1FFE9F9F1FFE9F9F1FFEFFBF5FFDEF7EBFF60D6
          9DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        TabStop = False
        OnClick = btnTarjetaGasolinaClick
      end
      object CB_CTA_GAS: TDBEdit
        Left = 105
        Top = 80
        Width = 211
        Height = 21
        DataField = 'CB_CTA_GAS'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_CTA_VAL: TDBEdit
        Left = 105
        Top = 108
        Width = 211
        Height = 21
        DataField = 'CB_CTA_VAL'
        DataSource = DataSource
        TabOrder = 2
      end
      object CB_SUB_CTA: TDBEdit
        Left = 105
        Top = 132
        Width = 211
        Height = 21
        DataField = 'CB_SUB_CTA'
        DataSource = DataSource
        TabOrder = 3
      end
      object CB_NETO: TZetaDBNumero
        Left = 105
        Top = 156
        Width = 97
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
        DataField = 'CB_NETO'
        DataSource = DataSource
      end
      object cxGroupBox1: TcxGroupBox
        Left = 7
        Top = 6
        TabOrder = 0
        Height = 68
        Width = 413
        object LBanca: TLabel
          Left = 5
          Top = 42
          Width = 90
          Height = 13
          Alignment = taRightJustify
          Caption = 'Banca Electr'#243'nica:'
        end
        object lBanco: TLabel
          Left = 61
          Top = 16
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'Banco:'
        end
        object CB_BAN_ELE: TDBEdit
          Left = 98
          Top = 38
          Width = 211
          Height = 21
          DataField = 'CB_BAN_ELE'
          DataSource = DataSource
          TabOrder = 1
        end
        object BtnBAN_ELE: TcxButton
          Left = 313
          Top = 38
          Width = 21
          Height = 21
          Hint = 
            'Verificar si el N'#250'mero de Banca Electr'#243'nica Pertenece a otro Emp' +
            'leado'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFBAEDD5FFCEF2E1FF84DFB3FFE9F9F1FFE9F9
            F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE1F7ECFF7CDDAEFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF7FDEB0FFFFFFFFFFFFFF
            FFFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF69D8A2FFC8F1DDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFBDEED6FFD3F4E4FFE4F8EEFFC0EFD8FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFEFCFF5ED6
            9BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFD3F4
            E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF58D498FFF4FCF8FFA2E7C6FF5BD59AFF92E3BCFF92E3BCFF92E3BCFF92E3
            BCFF8CE2B8FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF71DAA7FF8AE1B7FFB2EBD0FFD0F3E2FF5ED69BFF92E3
            BCFF92E3BCFF92E3BCFF92E3BCFF8FE2BAFF50D293FFD3F4E4FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF53D3
            95FF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF92E3BCFFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FFD3F4E4FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF92E3BCFFA8E9C9FF50D293FF50D293FF50D2
            93FF50D293FF50D293FFADEACCFFF7FDFAFFFFFFFFFFFFFFFFFFCBF2DFFF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF92E3BCFFA8E9
            C9FF9DE6C2FFB2EBD0FF5BD59AFF92E3BCFFAAE9CAFFFFFFFFFFFFFFFFFFFFFF
            FFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF92E3BCFFA8E9C9FF87E0B5FF97E4BFFF5ED69BFF92E3BCFFADEA
            CCFFFFFFFFFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF69D8A2FFC8F1DDFF53D395FF50D2
            93FF50D293FF50D293FF7CDDAEFFFFFFFFFFE4F8EEFF60D69DFF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF9DE6C2FFE1F7ECFFE9F9F1FFE9F9F1FFE9F9F1FFEFFBF5FFDEF7EBFF60D6
            9DFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          TabStop = False
          OnClick = BtnBAN_ELEClick
        end
        object CB_BANCO: TZetaDBKeyLookup_DevEx
          Left = 98
          Top = 12
          Width = 238
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_BANCO'
          DataSource = DataSource
        end
      end
    end
    object tsConfidencialidad: TcxTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 8
      object LblNivel0: TLabel
        Left = 22
        Top = 23
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Confidencialidad:'
      end
      object CB_NIVEL0: TZetaDBKeyLookup_DevEx
        Left = 106
        Top = 18
        Width = 238
        Height = 21
        Hint = 'Confidencialidad'
        LookupDataset = dmSistema.cdsNivel0
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        ShowHint = True
        TabOrder = 0
        TabStop = True
        WidthLlave = 55
        DataField = 'CB_NIVEL0'
        DataSource = DataSource
      end
    end
    object tsDatosMedicos: TcxTabSheet
      Caption = 'Datos M'#233'dicos'
      ImageIndex = 7
      object lblTSangre: TLabel
        Left = 76
        Top = 19
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Sangre:'
      end
      object lblAlerPadec: TLabel
        Left = 36
        Top = 45
        Width = 116
        Height = 13
        Alignment = taRightJustify
        Caption = 'Al'#233'rgico / Padecimiento:'
      end
      object CB_ALERGIA: TDBEdit
        Left = 155
        Top = 41
        Width = 318
        Height = 21
        DataField = 'CB_ALERGIA'
        DataSource = DataSource
        TabOrder = 1
      end
      object CB_TSANGRE: TDBComboBox
        Left = 155
        Top = 15
        Width = 150
        Height = 21
        DataField = 'CB_TSANGRE'
        DataSource = DataSource
        Items.Strings = (
          ''
          'O +'
          'O -'
          'A +'
          'A -'
          'B +'
          'B -'
          'AB +'
          'AB -')
        TabOrder = 0
      end
    end
    object tsBrigada: TcxTabSheet
      Caption = 'Brigadas de Protecci'#243'n Civil'
      ImageIndex = 6
      object lblTipoBrigada: TLabel
        Left = 127
        Top = 34
        Width = 24
        Height = 13
        Caption = 'Tipo:'
      end
      object lblNoPiso: TLabel
        Left = 93
        Top = 103
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'No. de Piso:'
      end
      object lblRol: TLabel
        Left = 132
        Top = 59
        Width = 19
        Height = 13
        Alignment = taRightJustify
        Caption = 'Rol:'
      end
      object lblConocimiento: TLabel
        Left = 84
        Top = 79
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Conocimiento:'
      end
      object CB_BRG_TIP: TZetaDBKeyCombo
        Left = 155
        Top = 32
        Width = 207
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        OnChange = CB_INFTIPOChange
        ListaFija = lfTipoBrigada
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'CB_BRG_TIP'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object CB_BRG_NOP: TDBEdit
        Left = 155
        Top = 101
        Width = 206
        Height = 21
        DataField = 'CB_BRG_NOP'
        DataSource = DataSource
        MaxLength = 30
        TabOrder = 6
      end
      object CB_BRG_ACT: TDBCheckBox
        Left = 28
        Top = 14
        Width = 140
        Height = 17
        Alignment = taLeftJustify
        BiDiMode = bdLeftToRight
        Caption = 'Pertenece a una brigada:'
        DataField = 'CB_BRG_ACT'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_JEF: TDBCheckBox
        Left = 235
        Top = 57
        Width = 104
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Jefe de Brigada'
        DataField = 'CB_BRG_JEF'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 3
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_ROL: TDBCheckBox
        Left = 155
        Top = 57
        Width = 70
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Brigadista'
        DataField = 'CB_BRG_ROL'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_CON: TDBCheckBox
        Left = 155
        Top = 77
        Width = 66
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Te'#243'rico'
        DataField = 'CB_BRG_CON'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 4
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
      object CB_BRG_PRA: TDBCheckBox
        Left = 235
        Top = 77
        Width = 102
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Pr'#225'ctico'
        DataField = 'CB_BRG_PRA'
        DataSource = DataSource
        ParentBiDiMode = False
        TabOrder = 5
        ValueChecked = 'S'
        ValueUnchecked = 'N'
        OnClick = CB_BRG_ACTClick
      end
    end
    object tsTimbrado: TcxTabSheet
      Caption = 'Timbrado de N'#243'mina'
      ImageIndex = 9
      object Label16: TLabel
        Left = 21
        Top = 21
        Width = 69
        Height = 13
        Caption = 'R'#233'gimen SAT:'
      end
      object CB_REGIMEN: TZetaDBKeyLookup_DevEx
        Left = 98
        Top = 17
        Width = 343
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_REGIMEN'
        DataSource = DataSource
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 444
    Top = 297
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 11010504
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF98A1E2FFF6F7FDFFB7BEEBFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFB1B8
          E9FFF6F7FDFFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959CDFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4959
          CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4959CDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4959CDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF8792DEFFDFE2F6FFA1A9E5FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8
          A1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF8CE2B8FFE9F9
          F1FF53D396FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF51D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF6FDAA7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF51D395FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF59D59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF61D79FFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4ED293FF53D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF8AE1B7FFE6F9F0FF69D9A4FF4ED293FF4ED293FF4ED2
          93FF5ED69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF51D395FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF64D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF67D8A2FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF69D9A4FFEFFBF5FFFFFFFFFFDEF7EBFF51D395FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF77DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF6FDAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF64D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF56D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF81DFB1FF8FE2BAFF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED2
          93FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF4ED293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF5160CFFF6B78D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6E7BD7FF7482D9FF6370D4FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFF969FE2FF6370D4FF969FE2FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4656CCFF4656CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4656CCFF4656CCFF6E7BD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF6875D6FF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4656CCFF4656CCFFA4ACE6FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF5463D0FF7482D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656
          CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF4656CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 464
    Top = 8
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 524720
  end
end
