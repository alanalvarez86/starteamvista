inherited GridEmpCertific_DevEx: TGridEmpCertific_DevEx
  Left = 296
  Top = 206
  Caption = 'Registro de Certificaciones por Empleado'
  ClientHeight = 473
  ClientWidth = 728
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaDBGrid [0]
    Top = 83
    Width = 728
    Height = 354
    TabOrder = 1
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        Expanded = False
        FieldName = 'CI_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Color = clInfoBk
        Expanded = False
        FieldName = 'CI_NOMBRE'
        ReadOnly = True
        Title.Caption = 'Certificaci'#243'n'
        Width = 200
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KI_FEC_CER'
        Title.Caption = 'Fecha'
        Width = 103
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KI_APROBO'
        Title.Caption = 'Aprob'#243
        Width = 40
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KI_FOLIO'
        Title.Caption = 'Folio'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KI_RENOVAR'
        Title.Caption = 'Renovar'
        Width = 49
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KI_CALIF_1'
        Title.Caption = 'Calific. #1'
        Width = 52
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KI_CALIF_2'
        Title.Caption = 'Calific. #2'
        Width = 52
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'KI_CALIF_3'
        Title.Caption = 'Calific. #3'
        Width = 52
        Visible = True
      end>
  end
  inherited PanelBotones: TPanel [1]
    Top = 437
    Width = 728
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 562
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 642
    end
  end
  inherited PanelIdentifica: TPanel [2]
    Width = 728
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 402
      inherited textoValorActivo2: TLabel
        Width = 396
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 6
  end
  object PanelEmp: TPanel [4]
    Left = 0
    Top = 50
    Width = 728
    Height = 33
    Align = alTop
    TabOrder = 0
    object EmpleadoLbl: TLabel
      Left = 20
      Top = 10
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = '&Empleado:'
      FocusControl = CB_CODIGO
    end
    object CB_CODIGO: TZetaKeyLookup_DevEx
      Left = 73
      Top = 6
      Width = 400
      Height = 21
      LookupDataset = dmCliente.cdsEmpleadoLookUp
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
  end
  object zComboSiNo: TZetaDBKeyCombo [5]
    Left = 304
    Top = 256
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 3
    TabStop = False
    Visible = False
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'KI_APROBO'
    DataSource = DataSource
    LlaveNumerica = False
  end
  object zFecha: TZetaDBFecha [6]
    Left = 304
    Top = 224
    Width = 115
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 2
    Text = '03/abr/07'
    Valor = 39175.000000000000000000
    Visible = False
    DataField = 'KI_FEC_CER'
    DataSource = DataSource
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 588
    Top = 49
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
