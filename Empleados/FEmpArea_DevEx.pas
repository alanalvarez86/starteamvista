unit FEmpArea_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZetaDBTextBox, StdCtrls, Db, Grids,
  DBGrids, ExtCtrls;

type
  TEmpArea_DevEx = class(TBaseConsulta)
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL5: TZetaDBTextBox;
    CB_NIVEL2: TZetaDBTextBox;
    CB_NIVEL3: TZetaDBTextBox;
    CB_NIVEL4: TZetaDBTextBox;
    CB_NIVEL1: TZetaDBTextBox;
    CB_NIVEL6: TZetaDBTextBox;
    CB_NIVEL7: TZetaDBTextBox;
    CB_NIVEL8: TZetaDBTextBox;
    CB_NIVEL9: TZetaDBTextBox;
    ZNIVEL1: TZetaTextBox;
    ZNIVEL2: TZetaTextBox;
    ZNIVEL3: TZetaTextBox;
    ZNIVEL4: TZetaTextBox;
    ZNIVEL5: TZetaTextBox;
    ZNIVEL6: TZetaTextBox;
    ZNIVEL7: TZetaTextBox;
    ZNIVEL8: TZetaTextBox;
    ZNIVEL9: TZetaTextBox;
    CB_NIVEL12lbl: TLabel;
    CB_NIVEL12: TZetaDBTextBox;
    ZNIVEL12: TZetaTextBox;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaDBTextBox;
    ZNIVEL11: TZetaTextBox;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL10: TZetaDBTextBox;
    ZNIVEL10: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    procedure SetCamposNivel;
    {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpArea_DevEx: TEmpArea_DevEx;

{$ifdef ACS}
const
     K_FORMA_ACS = 298;
     K_ALT_DEF = 19;
{$endif}

implementation

uses dRecursos, dTablas, dGlobal, ZGlobalTress, ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses,
     ZetaClientTools, ZetaTipoEntidad,ZImprimeForma,FEditEmpDlg_DevEx, ZetaDialogo,
  DCliente;

{$R *.DFM}

procedure TEmpArea_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     Self.Height := K_FORMA_ACS;
     {
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     ZNIVEL10.Visible := True;
     ZNIVEL11.Visible := True;
     ZNIVEL12.Visible := True;
     }
     {$endif}
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10113_Datos_area_empleado;
     SetCamposNivel;
     {$if Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
     CambiosVisuales;
     {$ifend}
end;

procedure TEmpArea_DevEx.Connect;
begin
     with dmRecursos, dmTablas do
     begin
          if ZNIVEL1.Visible then cdsNivel1.Conectar;
          if ZNIVEL2.Visible then cdsNivel2.Conectar;
          if ZNIVEL3.Visible then cdsNivel3.Conectar;
          if ZNIVEL4.Visible then cdsNivel4.Conectar;
          if ZNIVEL5.Visible then cdsNivel5.Conectar;
          if ZNIVEL6.Visible then cdsNivel6.Conectar;
          if ZNIVEL7.Visible then cdsNivel7.Conectar;
          if ZNIVEL8.Visible then cdsNivel8.Conectar;
          if ZNIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if ZNIVEL10.Visible then cdsNivel10.Conectar;
          if ZNIVEL11.Visible then cdsNivel11.Conectar;
          if ZNIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
end;

procedure TEmpArea_DevEx.Refresh;
begin
     dmRecursos.cdsDatosEmpleado.Refrescar;
end;

function TEmpArea_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpArea_DevEx.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandará llamar la Alta
end;

function TEmpArea_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpArea_DevEx.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;     // Se mandará llamar la Baja
end;

procedure TEmpArea_DevEx.Modificar;
 var
    sCaption, sTipo: string;
begin

     if dmCliente.UsaPlazas then
     begin
          sCaption := 'Modificación de Plaza';
          sTipo := K_T_PLAZA;
     end
     else
     begin
          sCaption := 'Modificación de Área';
          sTipo := K_T_AREA;
     end;

     if ( EditEmpDlg_DevEx = nil ) then
        EditEmpDlg_DevEx := TEditEmpDlg_DevEx.Create( Application );
     with EditEmpDlg_DevEx, dmRecursos do
     begin

          Caption:= sCaption;
          ShowModal;
          if ModalResult = mrOK then
          begin
               if Operacion.ItemIndex = 1 then  //  Movimiento Nuevo
                  AgregaKardex( sTipo )
               else
               begin
                    cdsHisKardex.Refrescar;
                    if KardexBuscaUltimo( sTipo ) then
                    begin
                         TabKardex := K_TAB_AREA;
                         cdsHisKardex.Modificar;
                    end
                    else
                       ZError('','No hay Registro de Alta',0);
               end;
          end;
     end;
end;

procedure TEmpArea_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dmRecursos.cdsDatosEmpleado do
     begin
          if ZNIVEL1.Visible then ZNIVEL1.Caption:= dmTablas.cdsNivel1.GetDescripcion( FieldByName( 'CB_NIVEL1' ).AsString );
          if ZNIVEL2.Visible then ZNIVEL2.Caption:= dmTablas.cdsNivel2.GetDescripcion( FieldByName( 'CB_NIVEL2' ).AsString );
          if ZNIVEL3.Visible then ZNIVEL3.Caption:= dmTablas.cdsNivel3.GetDescripcion( FieldByName( 'CB_NIVEL3' ).AsString );
          if ZNIVEL4.Visible then ZNIVEL4.Caption:= dmTablas.cdsNivel4.GetDescripcion( FieldByName( 'CB_NIVEL4' ).AsString );
          if ZNIVEL5.Visible then ZNIVEL5.Caption:= dmTablas.cdsNivel5.GetDescripcion( FieldByName( 'CB_NIVEL5' ).AsString );
          if ZNIVEL6.Visible then ZNIVEL6.Caption:= dmTablas.cdsNivel6.GetDescripcion( FieldByName( 'CB_NIVEL6' ).AsString );
          if ZNIVEL7.Visible then ZNIVEL7.Caption:= dmTablas.cdsNivel7.GetDescripcion( FieldByName( 'CB_NIVEL7' ).AsString );
          if ZNIVEL8.Visible then ZNIVEL8.Caption:= dmTablas.cdsNivel8.GetDescripcion( FieldByName( 'CB_NIVEL8' ).AsString );
          if ZNIVEL9.Visible then ZNIVEL9.Caption:= dmTablas.cdsNivel9.GetDescripcion( FieldByName( 'CB_NIVEL9' ).AsString );
          {$ifdef ACS}
          if ZNIVEL10.Visible then ZNIVEL10.Caption:= dmTablas.cdsNivel10.GetDescripcion( FieldByName( 'CB_NIVEL10' ).AsString );
          if ZNIVEL11.Visible then ZNIVEL11.Caption:= dmTablas.cdsNivel11.GetDescripcion( FieldByName( 'CB_NIVEL11' ).AsString );
          if ZNIVEL12.Visible then ZNIVEL12.Caption:= dmTablas.cdsNivel12.GetDescripcion( FieldByName( 'CB_NIVEL12' ).AsString );
          {$endif}
     end;
end;

procedure TEmpArea_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1, ZNIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2, ZNIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3, ZNIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4, ZNIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5, ZNIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6, ZNIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7, ZNIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8, ZNIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9, ZNIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10,ZNIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11,ZNIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12,ZNIVEL12 );
     {$endif}
end;

procedure TEmpArea_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TEmpArea_DevEx.CambiosVisuales;
begin
     CB_NIVEL1lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL1lbl.Width+2 ) ;
     CB_NIVEL1.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel1.Left :=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL1.Width := K_WIDTHLLAVE;
     zNivel1.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL2lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL2lbl.Width+2 ) ;
     CB_NIVEL2.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel2.Left:=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL2.Width := K_WIDTHLLAVE;
     zNivel2.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL3lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL3lbl.Width+2 ) ;
     CB_NIVEL3.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel3.Left:=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL3.Width := K_WIDTHLLAVE;
     zNivel3.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL4lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL4lbl.Width+2 ) ;
     CB_NIVEL4.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel4.Left:=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL4.Width := K_WIDTHLLAVE;
     zNivel4.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL5lbl.Left := K_WIDTH_MEDIUM_LEFT  -( CB_NIVEL5lbl.Width+2 ) ;
     CB_NIVEL5.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel5.Left:=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL5.Width := K_WIDTHLLAVE;
     zNivel5.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL6lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL6lbl.Width+2 ) ;
     CB_NIVEL6.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel6.Left:=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL6.Width := K_WIDTHLLAVE;
     zNivel6.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL7lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL7lbl.Width+2 ) ;
     CB_NIVEL7.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel7.Left:=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL7.Width := K_WIDTHLLAVE;
     zNivel7.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL8lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL8lbl.Width+2 ) ;
     CB_NIVEL8.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel8.Left :=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL8.Width := K_WIDTHLLAVE;
     zNivel8.Width := K_WIDTH_MEDIUM_TEXTBOX ;

     CB_NIVEL9lbl.Left := K_WIDTH_MEDIUM_LEFT -( CB_NIVEL9lbl.Width+2 ) ;
     CB_NIVEL9.Left := K_WIDTH_MEDIUM_LEFT;
     zNivel9.Left:=  K_WIDTH_MEDIUM_TEXTBOX_LEFT;
     CB_NIVEL9.Width := K_WIDTHLLAVE;
     zNivel9.Width := K_WIDTH_MEDIUM_TEXTBOX ;
end;
{$ifend}

end.
