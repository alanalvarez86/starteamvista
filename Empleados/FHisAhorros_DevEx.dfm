inherited HisAhorros_DevEx: THisAhorros_DevEx
  Left = 215
  Top = 128
  Caption = 'Historial de Ahorros'
  ClientWidth = 457
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 457
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 131
      inherited textoValorActivo2: TLabel
        Width = 125
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 457
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object AH_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AH_TIPO'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object AH_FECHA: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'AH_FECHA'
      end
      object AH_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'AH_NUMERO'
      end
      object AH_SALDO: TcxGridDBColumn
        Caption = 'Ahorrado'
        DataBinding.FieldName = 'AH_SALDO'
      end
      object AH_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'AH_STATUS'
      end
      object AH_SUB_CTA: TcxGridDBColumn
        Caption = 'Subcuenta'
        DataBinding.FieldName = 'AH_SUB_CTA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 80
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
