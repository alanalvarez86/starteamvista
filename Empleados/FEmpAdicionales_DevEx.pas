unit FEmpAdicionales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZetaDBTextBox, StdCtrls, DBCtrls, Db, ExtCtrls, ComCtrls,
  Mask, ZetaFecha, dxSkinsCore, TressMorado2013, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxPC;

type
  TEmpAdicionales_DevEx = class(TBaseConsulta)
    PageControl: TcxPageControl;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    //iLleno : Integer;
    //procedure SetCampo( sValor: String; oLabel: TLabel; oControl: TControl ) ;
    procedure RevisaControlesObjeto(oControl: TWinControl);
    procedure AsignaDblClickControls;
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;

  public
  end;

var
  EmpAdicionales_DevEx: TEmpAdicionales_DevEx;

implementation

uses dRecursos, dTablas, dGlobal, dSistema, ZGlobalTress, ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses,
     ZBaseEdicion_DevEx, ZetaTipoEntidad,ZImprimeForma,FEditEmpAdicionales_DevEx,DCliente,
  DBasicoCliente;

{$R *.DFM}

procedure TEmpAdicionales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10116_Datos_adicionales_empleado;
end;

procedure TEmpAdicionales_DevEx.FormShow(Sender: TObject);
var
   i : Integer;
begin
{
 Siempre redibujan los controles, esto permite que si se abre la forma con diferente tama�o del shell
 los controles parece que se autoajustan, no es muy costosa la llamada
}
     with PageControl do
     begin
          for i := ( PageCount - 1 ) downto 0 do
          begin
               Pages[i].Free;
          end;
     end;
     with dmSistema do
     begin
          CamposAdic.ContruyeFormaConsulta( Clasificaciones, self.DataSource, self, PageControl, dmTablas.GetEntidadAdicionalConnect);
     end;
     RevisaControlesObjeto( PageControl );   // Al mostrar la forma ya se dispar� evento: DataSourceDataChange

     inherited;
     AsignaDblClickControls;
{
     iLLeno := 0;
     SetCampo( Global.GetGlobalString( K_GLOBAL_TEXTO1 ), TextAdicLbl1, CB_G_TEX_1 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_TEXTO2 ), TextAdicLbl2, CB_G_TEX_2 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_TEXTO3 ), TextAdicLbl3, CB_G_TEX_3 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_TEXTO4 ), TextAdicLbl4, CB_G_TEX_4 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_TAB1 ), TablaAdicLbl1, ZCB_G_TAB_1 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_TAB2 ), TablaAdicLbl2, ZCB_G_TAB_2 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_TAB3 ), TablaAdicLbl3, ZCB_G_TAB_3 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_TAB4 ), TablaAdicLbl4, ZCB_G_TAB_4 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_NUM1 ), NumAdicLbl1, CB_G_NUM_1 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_NUM2 ), NumAdicLbl2, CB_G_NUM_2 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_NUM3 ), NumAdicLbl3, CB_G_NUM_3 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_FECHA1 ), FechaAdicLbl1, CB_G_FEC_1 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_FECHA2 ), FechaAdicLbl2, CB_G_FEC_2 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_FECHA3 ), FechaAdicLbl3, CB_G_FEC_3 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_LOG1 ), BoolAdicLbl1, CB_G_LOG_1 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_LOG2 ), BoolAdicLbl2, CB_G_LOG_2 );
     SetCampo( Global.GetGlobalString( K_GLOBAL_LOG3 ), BoolAdicLbl3, CB_G_LOG_3 );
}
end;

procedure TEmpAdicionales_DevEx.Connect;
begin
     with dmRecursos do
     begin
{
          if ZCB_G_TAB_1.Visible then cdsExtra1.Conectar;
          if ZCB_G_TAB_2.Visible then cdsExtra2.Conectar;
          if ZCB_G_TAB_3.Visible then cdsExtra3.Conectar;
          if ZCB_G_TAB_4.Visible then cdsExtra4.Conectar;
}
          cdsDatosEmpleado.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
end;

procedure TEmpAdicionales_DevEx.Refresh;
begin
     dmRecursos.cdsDatosEmpleado.Refrescar;
end;

function TEmpAdicionales_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpAdicionales_DevEx.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandar� llamar la Alta
end;

function TEmpAdicionales_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpAdicionales_DevEx.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;    // Se mandar� llamar la Baja
end;

procedure TEmpAdicionales_DevEx.Modificar;
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpAdicionales_DevEx, TEditEmpAdicionales_DevEx );
end;

procedure TEmpAdicionales_DevEx.RevisaControlesObjeto( oControl: TWinControl );
var
   i : Integer;
   oControlHijo : TControl;
   sCampo : String;
begin
     for i:= 0 to oControl.ControlCount - 1 do
     begin
          oControlHijo := oControl.Controls[i];
          with oControlHijo do
          begin
               if ( ClassType = TZetaTextBox ) and ( Tag > 0 ) then
               begin
                    sCampo := Copy( Name, 2, Length( Name ) );   // Se quita la 'Z' del principio
                    TZetaTextBox( oControlHijo ).Caption := dmTablas.GetEntidadAdicionalConnect( TipoEntidad( Tag ) ).GetDescripcion( dmRecursos.cdsDatosEmpleado.FieldByName( sCampo ).AsString );
               end
               else if ( oControlHijo is TWinControl ) then
               begin
                    RevisaControlesObjeto( TWinControl( oControlHijo ) );
               end;
          end;
     end;
end;

procedure TEmpAdicionales_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then   // No es necesario cada que se dispare el evento, solo cuando cambia de registro
     begin
          RevisaControlesObjeto( PageControl );
     end;
{
     with dmRecursos.cdsDatosEmpleado do
     begin
          if ZCB_G_TAB_1.Visible then ZCB_G_TAB_1.Caption:= dmTablas.cdsExtra1.GetDescripcion( FieldByName( 'CB_G_TAB_1' ).AsString );
          if ZCB_G_TAB_2.Visible then ZCB_G_TAB_2.Caption:= dmTablas.cdsExtra2.GetDescripcion( FieldByName( 'CB_G_TAB_2' ).AsString );
          if ZCB_G_TAB_3.Visible then ZCB_G_TAB_3.Caption:= dmTablas.cdsExtra3.GetDescripcion( FieldByName( 'CB_G_TAB_3' ).AsString );
          if ZCB_G_TAB_4.Visible then ZCB_G_TAB_4.Caption:= dmTablas.cdsExtra4.GetDescripcion( FieldByName( 'CB_G_TAB_4' ).AsString );
     end;
}
end;

procedure TEmpAdicionales_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

{
procedure TEmpAdicionales.SetCampo( sValor: String; oLabel: TLabel; oControl: TControl ) ;
const CONTROL_TOP = 24;
      LABEL_TOP = 2;
      KILLENO = 19;
      NO_CILLENO_LEFT = 488;
      NO_LILLENO_LEFT = 343;
      //CILLENO_LEFT = 155;
      //LILLENO_LEFT = 8;
      LILLENO_LEFT = 78;
      CILLENO_LEFT = 295;
      //KLWIDTH = 140;
      KLWIDTH = 210;
begin
     if StrLleno( sValor ) then
     begin
          with oControl do
          begin
               Left := CILLENO_LEFT;
               Top   := ( CONTROL_TOP + 3 )  + ( iLleno * ( KILLENO + 3 ) );
          end;
          with oLabel do
          begin
               Caption := sValor + ':';
               Left := LILLENO_LEFT;
               Top := oControl.Top + LABEL_TOP;
               Width := KLWIDTH;
          end;
         iLLeno := iLLeno + 1;
     end
     else
     begin
          oLabel.Visible := False;
          oControl.Visible := False;
     end;
end;
}

function TEmpAdicionales_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
      Result := IntToBool( PageControl.ActivePage.Tag - 1);
      sMensaje := Format ('No tiene permisos para imprimir %s ',[ PageControl.ActivePage.Caption ]);
end;

function TEmpAdicionales_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION );
     if not Result then
     begin
          with dmSistema.cdsGruposAdic do
          begin
               DisableControls;
               Conectar;
               First;
               while (not Eof)do
               begin
                    Result := ( ( FieldByName('GX_DERECHO').AsInteger and K_DERECHO_CAMBIO ) > K_SIN_DERECHOS );
                    if Result then
                       Break;
                    Next;
               end;
               EnableControls;
          end;
          sMensaje := 'No tiene permisos para modificar ninguna clasificaci�n';
     end;
end;


procedure TEmpAdicionales_DevEx.AsignaDblClickControls;
var
   iTab: Integer;
   iControl: Integer;
   Tab: TCxTabSheet;
begin
     for iTab := 0 to ( PageControl.TabCount  - 1 ) do
     begin
          Tab := PageControl.Pages[ iTab ];
          for iControl := 0 to ( Tab.ControlCount  - 1 ) do
          begin
               if ( Tab.Controls[ iControl ].ClassType = TPanel ) then
               begin
                    TPanel(Tab.Controls[ iControl ]).OnDblClick := FormDblClick;
               end;
          end;

     end;
end;
end.
