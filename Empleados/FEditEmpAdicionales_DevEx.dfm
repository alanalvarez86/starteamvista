inherited EditEmpAdicionales_DevEx: TEditEmpAdicionales_DevEx
  Left = 353
  Top = 255
  Caption = 'Datos Adicionales del Empleado'
  ClientHeight = 401
  ClientWidth = 543
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 365
    Width = 543
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 362
      Anchors = [akRight, akBottom]
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 449
      Anchors = [akRight, akBottom]
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 543
    TabOrder = 0
    inherited Splitter: TSplitter
      Left = 0
    end
    inherited ValorActivo1: TPanel
      Left = 3
      Width = 289
      Align = alClient
      inherited textoValorActivo1: TLabel
        Width = 283
      end
    end
    inherited ValorActivo2: TPanel
      Left = 292
      Width = 251
      Align = alRight
      Enabled = False
      Visible = False
      inherited textoValorActivo2: TLabel
        Width = 245
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 8
  end
  object PageControl: TcxPageControl [3]
    Left = 0
    Top = 47
    Width = 543
    Height = 318
    Align = alClient
    TabOrder = 7
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 314
    ClientRectLeft = 4
    ClientRectRight = 539
    ClientRectTop = 4
  end
  inherited DataSource: TDataSource
    Left = 436
    Top = 26
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
