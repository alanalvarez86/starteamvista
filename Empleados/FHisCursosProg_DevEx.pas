unit FHisCursosProg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseConsulta,} ZBaseGridLectura_DevEx, Db, ExtCtrls,
  Grids, DBGrids, ZetaDBGrid, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid,StdCtrls;

type
  THisCursosProg_DevEx = class(TBaseGridLectura_DevEx)
    KC_FEC_PRO: TcxGridDBColumn;
    CU_CODIGO: TcxGridDBColumn;
    NOMBRE: TcxGridDBColumn;
    KC_REVISIO: TcxGridDBColumn;
    KC_FEC_TOM: TcxGridDBColumn;
    KC_PROXIMO: TcxGridDBColumn;
    KC_HORAS: TcxGridDBColumn;
    KC_EVALUA: TcxGridDBColumn;
    EN_OPCIONA: TcxGridDBColumn;
    MA_CODIGO: TcxGridDBColumn;
    CP_MANUAL: TcxGridDBColumn;
    EP_GLOBAL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    {$IFNDEF SUPERVISORES}
    function EsIndividual: Boolean;
    {$ENDIF}
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeAgregar(var sMensaje: String ): Boolean; override;
  public

  end;

var
  HisCursosProg_DevEx: THisCursosProg_DevEx;

implementation

{$R *.DFM}

uses {$IFDEF SUPERVISORES}
     dSuper,
     {$ELSE}
     dRecursos,
     {$ENDIF}
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientDataSet,
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     DCliente,
     FAutoClasses;

procedure THisCursosProg_DevEx.Connect;
begin
     {$IFDEF SUPERVISORES}
     with dmSuper do
     begin
     {$ELSE}
     with dmRecursos do
     begin
     {$ENDIF}
          cdsHisCursosProg.Conectar;
          DataSource.DataSet:= cdsHisCursosProg;
     end;
end;

procedure THisCursosProg_DevEx.Refresh;
begin
     TZetaClientDataSet( DataSource.DataSet ).Refrescar;  //Se cambi� para compartir l�gica con supervisores
     DoBestFit;
end;

procedure THisCursosProg_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10136_Cursos_programados;
end;

{***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
function THisCursosProg_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     {$IFNDEF SUPERVISORES}
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          Result := inherited PuedeBorrar( sMensaje );
          if Result then
          begin
               if not( EsIndividual )then
               begin
                    sMensaje := 'Solo Se Pueden Borrar Cursos Programados Individualmente';
                    Result := False;
               end;
          end;
     end;
     {$ELSE}
     Result:= FALSE;
     sMensaje:= 'No se pueden borrar cursos programados desde supervisores';
     {$ENDIF}
end;

function THisCursosProg_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     {$IFNDEF SUPERVISORES}
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          Result := inherited PuedeModificar( sMensaje );
          if Result then
          begin
               if not( EsIndividual )then
               begin
                    sMensaje := 'Solo Se Pueden Modificar Cursos Programados Individualmente';
                    Result := False;
               end;
          end;
     end;
     {$ELSE}
     Result:= FALSE;
     sMensaje:= 'No se pueden modificar cursos programados desde supervisores';
     {$ENDIF}
end;

function THisCursosProg_DevEx.PuedeAgregar(var sMensaje: String ): Boolean;
begin
     {$IFNDEF SUPERVISORES}
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
     begin
          Result := inherited PuedeAgregar( sMensaje );
     end;
     {$ELSE}
     Result:= FALSE;
     sMensaje:= 'No se pueden agregar cursos programados desde supervisores';
     {$ENDIF}
end;
{*** FIN ***}


procedure THisCursosProg_DevEx.Agregar;
begin
     {$IFNDEF SUPERVISORES}
     dmRecursos.cdsCursosProg.Agregar;
     {$ENDIF}
end;

procedure THisCursosProg_DevEx.Modificar;
begin
     {$IFNDEF SUPERVISORES}
     dmRecursos.cdsCursosProg.Modificar;
     {$ENDIF}
end;

procedure THisCursosProg_DevEx.Borrar;
begin
     {$IFNDEF SUPERVISORES}
     dmRecursos.cdsCursosProg.Borrar;
     DoBestFit;
     {$ENDIF}
end;
{$IFNDEF SUPERVISORES}
function THisCursosProg_DevEx.EsIndividual: Boolean;
begin
     Result := zStrToBool(dmRecursos.cdsHisCursosProg.FieldByName('CP_MANUAL').AsString);
end;
{$ENDIF}

procedure THisCursosProg_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     //CreaColumaSumatoria('NOMBRE',skCount);
     //CreaColumaSumatoria('KC_HORAS',skSum);
     //CreaColumaSumatoria('KC_EVALUA',skAverage);
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
end;

end.
