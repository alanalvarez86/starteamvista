unit FEmpContratacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ZetaDBTextBox, StdCtrls, DBCtrls, Db,
  ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxButtons;

type
  TEmpContratacion_DevEx = class(TBaseConsulta)
    CB_FEC_INGlbl: TLabel;
    CB_CONTRATlbl: TLabel;
    CB_FEC_CONlbl: TLabel;
    CB_PUESTOlbl: TLabel;
    CB_CLASIFIlbl: TLabel;
    CB_HORARIOlbl: TLabel;
    CB_FEC_ING: TZetaDBTextBox;
    ZCONTRATO: TZetaTextBox;
    CB_FEC_CON: TZetaDBTextBox;
    ZPUESTO: TZetaTextBox;
    ZCLASIFICACION: TZetaTextBox;
    ZTURNO: TZetaTextBox;
    LVencimiento: TLabel;
    Label1: TLabel;
    CB_FEC_ANT: TZetaDBTextBox;
    Label2: TLabel;
    ZPATRON: TZetaTextBox;
    ZAntiguedad: TZetaTextBox;
    CB_CONTRAT: TZetaDBTextBox;
    CB_PUESTO: TZetaDBTextBox;
    CB_CLASIFI: TZetaDBTextBox;
    CB_TURNO: TZetaDBTextBox;
    CB_PATRON: TZetaDBTextBox;
    bbMostrarCalendario: TcxButton;
    Label3: TLabel;
    CB_PLAZA: TZetaDBTextBox;
    ZPlaza: TZetaTextBox;
    Label4: TLabel;
    CB_NOMINA: TZetaDBTextBox;
    ZNOMINA: TZetaTextBox;
    CB_FEC_COV: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bbMostrarCalendarioClick(Sender: TObject);
  private
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ImprimirForma;override;
  public
  end;

var
  EmpContratacion_DevEx: TEmpContratacion_DevEx;

implementation

uses dRecursos, dCatalogos, ZetaCommonLists, ZetaCommonTools, ZetaCommonClasses,
     ZetaDialogo, ZetaTipoEntidad,ZImprimeForma, FEditContrataDlg_DevEx;

{$R *.DFM}

procedure TEmpContratacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10112_Datos_contratacion_empleado;
end;

procedure TEmpContratacion_DevEx.Connect;
begin
     with dmRecursos, dmCatalogos do
     begin
//          cdsPlazas.Conectar;
          cdsContratos.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
          cdsDatosEmpleado.Conectar;
          cdsTPeriodos.Conectar;
          DataSource.DataSet:= cdsDatosEmpleado;
     end;
end;

procedure TEmpContratacion_DevEx.Refresh;
begin
     dmRecursos.cdsDatosEmpleado.Refrescar;
end;

function TEmpContratacion_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoAlta( sMensaje );
end;

procedure TEmpContratacion_DevEx.Agregar;
begin
     dmRecursos.cdsDatosEmpleado.Agregar;    // Se mandar� llamar la Alta
end;

function TEmpContratacion_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.DerechoBaja( sMensaje );
end;

procedure TEmpContratacion_DevEx.Borrar;
begin
     dmRecursos.cdsDatosEmpleado.Borrar;     // Se mandar� llamar la Baja
end;

procedure TEmpContratacion_DevEx.Modificar;
var
   sTipoMov: String;
begin
     if ( EditContrataDlg_DevEx = nil ) then
        EditContrataDlg_DevEx := TEditContrataDlg_DevEx.Create( Application );
     with EditContrataDlg_DevEx, dmRecursos do
     begin
          ShowModal;
          if ModalResult = mrOK then
          begin
               sTipoMov  := GetTipoMov;
               if Operacion.ItemIndex = 1 then  //  Movimiento Nuevo
               begin
                    if sTipoMov = K_T_ALTA then  //  Equivale a un Reingreso
                         ProcesaReingreso
                    else
                       AgregaKardex( sTipoMov );
               end
               else
               begin
                    cdsHisKardex.Refrescar;
                    if KardexBuscaUltimo( sTipoMov ) then
                    begin
                         TabKardex := K_TAB_CONTRATA;
                         cdsHisKardex.Modificar;
                    end
                    else
                       ZError('','No hay Registro de Alta',0);
               end;
          end;
     end;
end;

procedure TEmpContratacion_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with DataSource.DataSet do
     begin
          //Lookups
          ZPlaza.Caption := dmRecursos.cdsPlazasLookup.GetDescripcion( FieldByName( 'CB_PLAZA' ).AsString );
          ZCONTRATO.Caption:= dmCatalogos.cdsContratos.GetDescripcion( FieldByName( 'CB_CONTRAT' ).AsString );
          ZPUESTO.Caption:= dmCatalogos.cdsPuestos.GetDescripcion( FieldByName( 'CB_PUESTO' ).AsString );
          ZCLASIFICACION.Caption:= dmCatalogos.cdsClasifi.GetDescripcion( FieldByName( 'CB_CLASIFI' ).AsString );
          ZTURNO.Caption:= dmCatalogos.cdsTurnos.GetDescripcion( FieldByName( 'CB_TURNO' ).AsString );
          ZPATRON.Caption:= dmCatalogos.cdsRPatron.GetDescripcion( FieldByName( 'CB_PATRON' ).AsString );
          ZNOMINA.Caption := dmCatalogos.cdsTPeriodos.GetDescripcion( FieldByName( 'CB_NOMINA').AsString);
          //ZVencimiento
          //ZVencimiento.Caption:= dmCatalogos.DescrVencimiento( FieldByName( 'CB_CONTRAT' ).AsString, FieldByName( 'CB_FEC_CON' ).AsDateTime );
          //ZAntiguedad
          if zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString ) then
             ZAntiguedad.Caption := TiempoDias( Date - FieldbyName('CB_FEC_ANT').AsDateTime + 1, etDias )
          else
             ZAntiguedad.Caption := TiempoDias( FieldByName( 'CB_FEC_BAJ' ).AsDateTime - FieldbyName('CB_FEC_ANT').AsDateTime + 1, etDias );
          //Turnos
          bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
end;

procedure TEmpContratacion_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enEmpleado, dmRecursos.cdsDatosEmpleado );
end;

procedure TEmpContratacion_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

end.
