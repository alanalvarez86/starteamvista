inherited EditCurProg_DevEx: TEditCurProg_DevEx
  Left = 418
  Top = 327
  Caption = 'Cursos Programados'
  ClientHeight = 218
  ClientWidth = 477
  PixelsPerInch = 96
  TextHeight = 13
  object lblCurso: TLabel [0]
    Left = 26
    Top = 57
    Width = 30
    Height = 13
    Caption = 'Curso:'
  end
  inherited PanelBotones: TPanel
    Top = 182
    Width = 477
    TabOrder = 9
    inherited OK_DevEx: TcxButton
      Left = 312
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 391
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 477
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 151
      inherited textoValorActivo2: TLabel
        Width = 145
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 13
  end
  object CU_CODIGO: TZetaDBKeyLookup_DevEx [4]
    Left = 62
    Top = 53
    Width = 383
    Height = 21
    LookupDataset = dmCatalogos.cdsCursos
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    DataField = 'CU_CODIGO'
    DataSource = DataSource
  end
  object EP_OPCIONA: TDBCheckBox [5]
    Left = 11
    Top = 140
    Width = 65
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Opcional:'
    DataField = 'EP_OPCIONA'
    DataSource = DataSource
    TabOrder = 6
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object EP_GLOBAL: TDBCheckBox [6]
    Left = 24
    Top = 160
    Width = 52
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Global:'
    DataField = 'EP_GLOBAL'
    DataSource = DataSource
    Enabled = False
    ReadOnly = True
    TabOrder = 7
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object GBDias: TGroupBox [7]
    Left = 61
    Top = 82
    Width = 165
    Height = 52
    Caption = '                     '
    TabOrder = 3
    object lblDias: TLabel
      Left = 133
      Top = 23
      Width = 23
      Height = 13
      Caption = 'D'#237'as'
    end
    object lblAntig: TLabel
      Left = 8
      Top = 23
      Width = 57
      Height = 13
      Caption = 'Antig'#252'edad:'
    end
    object EP_DIAS: TZetaDBNumero
      Left = 69
      Top = 19
      Width = 60
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      DataField = 'EP_DIAS'
      DataSource = DataSource
    end
  end
  object GBFecha: TGroupBox [8]
    Left = 231
    Top = 82
    Width = 214
    Height = 52
    Caption = '                           '
    TabOrder = 5
    object lblzFecha: TLabel
      Left = 7
      Top = 22
      Width = 87
      Height = 13
      Caption = 'Fecha Espec'#237'fica:'
    end
    object zFecha: TZetaDBFecha
      Left = 95
      Top = 18
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '14/oct/99'
      Valor = 36447.000000000000000000
      DataField = 'EP_FECHA'
      DataSource = DataSource
    end
  end
  object RbDias: TRadioButton [9]
    Left = 71
    Top = 82
    Width = 67
    Height = 14
    Caption = 'Por D'#237'as:'
    TabOrder = 2
    OnClick = RbDiasClick
  end
  object RbFecha: TRadioButton [10]
    Tag = 1
    Left = 241
    Top = 82
    Width = 83
    Height = 14
    Caption = 'Por Fecha a:'
    TabOrder = 4
    OnClick = RbFechaClick
  end
  inherited DataSource: TDataSource
    Left = 420
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
