unit FEditHisAcumulados_RS_DevEx;

interface

{  Folio      :
   Prop�sito  :
   Creaci�n   :
   Responsable:
   Calculados : No
   Lista Calculados[] }


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, ExtCtrls, StdCtrls, Db,
  Buttons, DBCtrls, Mask,
  ZetaDBTextBox, ZetaNumero, ZetaSmartLists,  
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditHisAcumulados_RS_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    lblRazonSocial: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label9: TLabel;
    AC_ANUAL: TZetaDBTextBox;
    RS_CODIGO: TZetaDBKeyLookup_DevEx;
    AC_MES_01: TZetaDBNumero;
    AC_MES_02: TZetaDBNumero;
    AC_MES_03: TZetaDBNumero;
    AC_MES_04: TZetaDBNumero;
    AC_MES_05: TZetaDBNumero;
    AC_MES_06: TZetaDBNumero;
    AC_MES_07: TZetaDBNumero;
    AC_MES_08: TZetaDBNumero;
    AC_MES_09: TZetaDBNumero;
    AC_MES_10: TZetaDBNumero;
    AC_MES_11: TZetaDBNumero;
    AC_MES_12: TZetaDBNumero;
    AC_MES_13: TZetaDBNumero;
    Label2: TLabel;
    CO_NUMERO: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure ActualizarGrid;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditHisAcumulados_RS_DevEx: TEditHisAcumulados_RS_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZAccesosTress,
     dRecursos, dCatalogos;

{$R *.DFM}

procedure TEditHisAcumulados_RS_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsHisAcumuladosRS.Conectar;
          Datasource.DataSet := cdsHisAcumuladosRS;
     end;
end;

procedure TEditHisAcumulados_RS_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stSistema;
     HelpContext:= H10143_Acumulados;
     IndexDerechos := ZAccesosTress.D_EMP_NOM_ACUMULA;
     FirstControl := RS_CODIGO;
     CO_NUMERO.LookupDataset := dmCatalogos.cdsConceptos;
     RS_CODIGO.LookupDataset := dmCatalogos.cdsRSocial;
end;

procedure TEditHisAcumulados_RS_DevEx.ActualizarGrid;
begin
     if not Editing then
     begin
          with dmRecursos do
          begin
               cdsHisAcumuladosRS.Refrescar;
          end;
     end;
end;

procedure TEditHisAcumulados_RS_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     ActualizarGrid;
end;

procedure TEditHisAcumulados_RS_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     ActualizarGrid;
end;

end.







