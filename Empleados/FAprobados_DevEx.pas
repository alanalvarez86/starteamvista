unit FAprobados_DevEx;

interface

uses
  Windows, Messages, SysUtils,
  {$ifndef VER130}Variants,{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZetaDBTextBox, StdCtrls, Grids, DBGrids, 
  ZetaDBGrid, DBCtrls, ZetaSmartLists, Buttons, ExtCtrls,
  ZBaseSesion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TAprobados_DevEx = class(TBaseSesion_DevEx)
    Label3: TLabel;
    SE_INSCRITO: TZetaDBTextBox;
    lblAprobados: TLabel;
    lblPeriodo: TLabel;
    ztbPeriodo: TZetaTextBox;
    ztbAprobados: TZetaTextBox;
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure Buscar;override;
  public
    { Public declarations }
  end;

var
  Aprobados_DevEx: TAprobados_DevEx;

implementation
uses DRecursos,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx;

{$R *.dfm}
procedure TAprobados_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_APROBADOS;
end;

procedure TAprobados_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmRecursos.cdsSesion do
     begin
          if( FieldbyName('SE_FEC_INI').AsDateTime = FieldbyName('SE_FEC_FIN').AsDateTime )then
              ztbPeriodo.Caption := FechaCorta( FieldbyName('SE_FEC_INI').AsDateTime )
          else
              ztbPeriodo.Caption := Format( 'del  %s  al  %s ', [ FechaCorta( FieldbyName('SE_FEC_INI').AsDateTime ),
                                                                  FechaCorta( FieldbyName('SE_FEC_FIN').AsDateTime ) ] );
     end;
end;

procedure TAprobados_DevEx.Connect;
begin
     inherited Connect;
     with dmRecursos do
     begin
          cdsAprobados.Refrescar;
          DataSource.DataSet := cdsAprobados;
     end;
end;

procedure TAprobados_DevEx.Buscar;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
     end;
end;

procedure TAprobados_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_devEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmRecursos.cdsAprobados do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TAprobados_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if( Field = nil )then
         ztbAprobados.Caption := InttoStr( dmRecursos.cdsAprobados.RecordCount );
end;

end.
