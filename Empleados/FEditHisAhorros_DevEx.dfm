inherited EditHisAhorros_DevEx: TEditHisAhorros_DevEx
  Left = 528
  Top = 163
  Caption = 'Ahorros'
  ClientHeight = 443
  ClientWidth = 476
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 407
    Width = 476
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 311
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 391
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 476
    TabOrder = 4
    inherited ValorActivo2: TPanel
      Width = 150
      inherited textoValorActivo2: TLabel
        Width = 144
      end
    end
  end
  inherited Panel1: TPanel
    Width = 476
    Height = 158
    TabOrder = 0
    object AH_TIPOLbl: TLabel
      Left = 59
      Top = 10
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object Label1: TLabel
      Left = 7
      Top = 32
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha de Inicio:'
    end
    object Label2: TLabel
      Left = 43
      Top = 74
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object UsuarioLbl: TLabel
      Left = 248
      Top = 32
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modific'#243':'
    end
    object Label7: TLabel
      Left = 50
      Top = 54
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object US_DESCRIP: TZetaDBTextBox
      Left = 296
      Top = 30
      Width = 135
      Height = 17
      AutoSize = False
      Caption = 'US_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_DESCRIP'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label8: TLabel
      Left = 28
      Top = 129
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Subcuenta:'
    end
    object AH_FORMULA: TDBMemo
      Left = 91
      Top = 72
      Width = 339
      Height = 53
      DataField = 'AH_FORMULA'
      DataSource = DataSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 255
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 3
    end
    object AH_FECHA: TZetaDBFecha
      Left = 91
      Top = 27
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '17/dic/97'
      Valor = 35781.000000000000000000
      DataField = 'AH_FECHA'
      DataSource = DataSource
    end
    object AH_TIPO: TZetaDBKeyLookup_DevEx
      Left = 91
      Top = 6
      Width = 340
      Height = 21
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'AH_TIPO'
      DataSource = DataSource
    end
    object AH_STATUS: TZetaDBKeyCombo
      Left = 91
      Top = 50
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      ListaFija = lfStatusAhorro
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'AH_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object AH_SUB_CTA: TZetaDBEdit
      Left = 91
      Top = 127
      Width = 211
      Height = 21
      TabOrder = 4
      Text = 'AH_SUB_CTA'
      DataField = 'AH_SUB_CTA'
      DataSource = DataSource
    end
    object SBCO_FORMULA_DevEx: TcxButton
      Left = 434
      Top = 72
      Width = 25
      Height = 25
      Hint = 'Abrir Constructor de F'#243'rmulas'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = SBCO_FORMULAClick
      OptionsImage.Glyph.Data = {
        7A080000424D7A08000000000000360000002800000017000000170000000100
        2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
        7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
        0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
        00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
        0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
        000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
        0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
        8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
    end
  end
  inherited PageControl: TcxPageControl
    Top = 208
    Width = 476
    Height = 199
    TabOrder = 1
    Properties.ActivePage = Tabla
    OnChange = PageControlChange
    ClientRectBottom = 197
    ClientRectRight = 474
    inherited Datos: TcxTabSheet
      Caption = 'Resumen'
      object DedLbl: TLabel
        Left = 156
        Top = 149
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = '# de Aportaciones:'
      end
      object AH_NUMERO: TZetaDBTextBox
        Left = 253
        Top = 147
        Width = 120
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_NUMERO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_NUMERO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GBTotales: TGroupBox
        Left = 58
        Top = 15
        Width = 353
        Height = 118
        Caption = 'Totales '
        TabOrder = 0
        object Label3: TLabel
          Left = 124
          Top = 22
          Width = 60
          Height = 13
          Alignment = taRightJustify
          Caption = 'Saldo Inicial:'
        end
        object Label4: TLabel
          Left = 117
          Top = 60
          Width = 67
          Height = 13
          Alignment = taRightJustify
          Caption = 'Otros Abonos:'
        end
        object AH_CARGOS: TZetaDBTextBox
          Left = 194
          Top = 76
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AH_CARGOS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AH_CARGOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 120
          Top = 78
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Otros Cargos:'
        end
        object AH_ABONOS: TZetaDBTextBox
          Left = 194
          Top = 58
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AH_ABONOS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AH_ABONOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label6: TLabel
          Left = 138
          Top = 42
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ahorrado:'
        end
        object AH_TOTAL: TZetaDBTextBox
          Left = 194
          Top = 40
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AH_TOTAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AH_TOTAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LSigno1: TLabel
          Left = 24
          Top = 60
          Width = 21
          Height = 13
          Caption = '( + ) '
        end
        object LSigno2: TLabel
          Left = 24
          Top = 42
          Width = 21
          Height = 13
          Caption = '( + ) '
        end
        object LSigno3: TLabel
          Left = 24
          Top = 78
          Width = 21
          Height = 13
          Caption = '( -- ) '
        end
        object LSigno4: TLabel
          Left = 24
          Top = 96
          Width = 21
          Height = 13
          Caption = '( = ) '
        end
        object SaldoLbl: TLabel
          Left = 121
          Top = 96
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Saldo Actual:'
        end
        object AH_SALDO: TZetaDBTextBox
          Left = 194
          Top = 94
          Width = 120
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AH_SALDO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AH_SALDO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object ZetaDBNumero1: TZetaDBNumero
          Left = 194
          Top = 18
          Width = 120
          Height = 21
          Mascara = mnPesos
          TabOrder = 0
          Text = '0.00'
          DataField = 'AH_SALDO_I'
          DataSource = DataSource
        end
      end
    end
    inherited Tabla: TcxTabSheet
      Caption = 'Cargos y Abonos'
      inherited GridRenglones: TZetaDBGrid
        Top = 33
        Width = 472
        Height = 136
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        OnColExit = GridRenglonesColExit
        OnDblClick = BBModificarClick
        Columns = <
          item
            Expanded = False
            FieldName = 'CR_FECHA'
            Title.Caption = 'Fecha'
            Width = 100
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CR_CARGO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Cargo'
            Width = 80
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CR_ABONO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Abono'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_OBSERVA'
            Title.Caption = 'Observaci'#243'n'
            Width = 166
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 472
        Height = 33
        inherited BBAgregar_DevEx: TcxButton
          Top = 4
        end
        inherited BBBorrar_DevEx: TcxButton
          Top = 4
        end
        inherited BBModificar_DevEx: TcxButton
          Top = 4
        end
      end
      object ZFecha: TZetaDBFecha
        Left = 120
        Top = 104
        Width = 105
        Height = 22
        Cursor = crArrow
        TabStop = False
        TabOrder = 2
        Text = '14/oct/99'
        Valor = 36447.000000000000000000
        Visible = False
        DataField = 'CR_FECHA'
        DataSource = dsRenglon
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 332
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited dsRenglon: TDataSource
    Left = 384
    Top = 0
  end
end
