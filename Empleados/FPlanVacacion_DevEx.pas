unit FPlanVacacion_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, StdCtrls, Mask,
  ZetaNumero, Buttons, DB, ExtCtrls, ZetaFecha, ZetaCommonClasses,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013, cxButtons,
  ZetaKeyLookup_DevEx, dxSkinsdxBarPainter, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, dxBar, cxContainer, cxCheckBox, dxSkinsDefaultPainters,
  ActnList, ImgList, System.Actions;

type
  TPlanVacacion_DevEx = class(TBaseConsulta)
    PanelFiltro: TPanel;
    AreaLBL: TLabel;
    CondicionLbl: TLabel;
    Label3: TLabel;
    EdadMinimaLBL: TLabel;
    EdadMaximaLBL: TLabel;
    EMPLEADO: TZetaKeyLookup_DevEx;
    CONDICION: TZetaKeyLookup_DevEx;
    cbStatus: TComboBox;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    pnlSalir: TPanel;
    PanelAcciones: TPanel;
    cbTodos: TcxCheckBox;
    FILTRAR_DevEx: TcxButton;
    btnAutorizar_DevEx: TcxButton;
    btnRechazar_DevEx: TcxButton;
    btnCancelar_DevEx: TcxButton;
    Salir: TcxButton;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridDBTableViewColumn1: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn2: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn3: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn4: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    ZetaDBGridDBTableViewColumn5: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn6: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn7: TcxGridDBColumn;
    cxImage16: TcxImageList;
    PopupMenu1: TPopupMenu;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    mImprimir: TMenuItem;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    dxBarManager1: TdxBarManager;
    BarSuperior: TdxBar;
    New_DevEx: TdxBarButton;
    Del_DevEx: TdxBarButton;
    Edit_DevEx: TdxBarButton;
    Refrescar_DevEx: TdxBarButton;
    Print_DevEx: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure FILTRARClick(Sender: TObject);
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar;  override;
    procedure Modificar; override;
    procedure btnProcesaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NewClick(Sender: TObject);
    procedure DelClick(Sender: TObject);
    procedure EditClick(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure PrintClick(Sender: TObject);
    procedure ValidarFechas(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
     procedure dxBarManager1ShowToolbarsPopup(Sender: TdxBarManager;
      PopupItemLinks: TdxBarItemLinks);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
  private
    { Private declarations }
    FParametros: TZetaParams;
    AColumn: TcxGridDBColumn;
    procedure ApplyMinWidth;
{$ifdef SUPERVISORES}
  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
{$endif}
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
  end;

var
  PlanVacacion_DevEx: TPlanVacacion_DevEx;

implementation

uses ZetaCommonLists, {$ifdef SUPERVISORES}dSuper{$else}dRecursos{$endif}, DCliente, DCatalogos, ZetaCommonTools, ZAccesosTress, ZAccesosMgr, ZetaClientDataSet, ZetaDialogo, ZGridModeTools;
{$R *.dfm}

constructor TPlanVacacion_DevEx.Create( AOwner: TComponent );
begin
     inherited Create( Aowner );
     {$ifdef SUPERVISORES}
     self.WindowState := wsNormal;
     self.Align := alNone;
     self.BorderStyle := bsDialog;
     self.Position := poScreenCenter;
     {$endif}
end;

{$ifdef SUPERVISORES}
procedure TPlanVacacion_DevEx.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;
{$endif}

procedure TPlanVacacion_DevEx.FormCreate(Sender: TObject);
const
     K_ANCHO_BOTON_ACCION = 83;

     procedure FillCombo( Control: TComboBox; Tipo: ListasFijas; const sTodos: String );
     begin
          with Control do
          begin
               ZetaCommonLists.LlenaLista( Tipo, Items );
               Items.Insert( 0, Format( '< %s >', [ sTodos ] ) );
               ItemIndex := 1;
          end;
     end;
begin
     inherited;
     {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
       ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     {***}
     FParametros:= TZetaParams.Create;
     {$ifdef SUPERVISORES}
     Condicion.Visible := false;
     CondicionLbl.Visible := false;
     pnlSalir.Visible := true;
     BarSuperior.Visible := true;
     btnCancelar_DevEx.Visible := false;
     cbTodos.Left := ( cbTodos.Left - K_ANCHO_BOTON_ACCION );
     HelpContext:= H_PlanVacacion_Super;
     dmSuper.cdsPlanVacacion.SetDataChange;
     {$else}
     HelpContext:= H_PlanVacacion;
     dmRecursos.cdsPlanVacacion.SetDataChange;
     BarSuperior.Visible := FALSE;
     {$Endif}
     FillCombo( cbStatus, lfStatusPlanVacacion, 'Todos' );
     cbStatus.ItemIndex := 0;
     Empleado.LookupDataset := dmCliente.cdsEmpleadoLookup;
     Condicion.LookupDataset := dmCatalogos.cdsCondiciones;
     btnAutorizar_DevEx.Tag := Ord (spvAutorizada);
     btnRechazar_DevEx.Tag := Ord (spvRechazada);
     btnCancelar_DevEx.Tag := Ord (spvPendiente);
     with dmCliente do
     begin
          FechaFinal.Valor := ZetaCommonTools.LastDayOfMonth(FechaDefault);
          FechaInicial.Valor := ZetaCommonTools.FirstDayOfMonth(FechaDefault);
     end;
end;

procedure TPlanVacacion_DevEx.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
      case key of
            VK_ESCAPE: ModalResult := mrCancel;
       end;
end;

procedure TPlanVacacion_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     dmCatalogos.cdsCondiciones.Conectar;
     {$ifdef SUPERVISORES}
     with dmSuper do
     {$else}
     with dmRecursos do
     {$endif}
     begin
          DataSource.DataSet:= cdsPlanVacacion;
          if ( cdsPlanVacacion.HayQueRefrescar ) then
             Refresh;
     end;
end;

procedure TPlanVacacion_DevEx.Refresh;
begin
     with FParametros do
     begin
          AddInteger( 'Status', CBStatus.ItemIndex - 1 );
          AddDate( 'FechaInicial', FechaInicial.Valor );
          AddDate( 'FechaFinal', FechaFinal.Valor );
          AddInteger( 'Empleado', Empleado.Valor );
          if StrLleno( Condicion.Llave ) then
          begin
               AddString('Condicion', Trim( Condicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString ) );
          end
          else
              AddString('Condicion', ZetaCommonClasses.VACIO);
          {$ifdef SUPERVISORES}
          AddString('ListaEmpleados', dmCliente.GetListaEmpleados( 'VACAPLAN.CB_CODIGO' ) );
          {$else}
          AddString('ListaEmpleados', ZetaCommonClasses.VACIO);
          {$endif}
     end;
     {$ifdef SUPERVISORES}
     with dmSuper do
     {$else}
     with dmRecursos do
     {$endif}
     begin
          RefrescarPlanVacacion( FParametros );
          with cdsPlanVacacion do
               First;
          end;
     end;

procedure TPlanVacacion_DevEx.Agregar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Agregar;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TPlanVacacion_DevEx.Borrar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Borrar;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TPlanVacacion_DevEx.Modificar;
begin
     TZetaClientDataSet( DataSource.DataSet ).Modificar;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TPlanVacacion_DevEx.FILTRARClick(Sender: TObject);
begin
  inherited;
  DoRefresh;
  ZetaDBGridDbtableView.Focused:=true;
  //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TPlanVacacion_DevEx.btnProcesaClick(Sender: TObject);
const
     aAccion : array [0..2] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('cancelar','autorizar','rechazar');
begin
     inherited;
     if ( not cbTodos.Checked ) or ZConfirm(Self.Caption, Format('Seguro de %s todos los registros visibles', [aAccion[TBitBtn(Sender).Tag]]), 0, mbYes) then
     begin
          {$ifdef SUPERVISORES}
          with dmSuper do
          {$else}
          with dmRecursos do
          {$endif}
              SetAutorizarPlanVacacion( eStatusPlanVacacion ( TBitBtn(Sender).Tag ), cbTodos.Checked );
     end;
     ZetaDBGridDbtableView.focused:=true;
end;
procedure TPlanVacacion_DevEx.FormShow(Sender: TObject);
var
   lEnabled: Boolean;
begin
     inherited;
     {$ifdef SUPERVISORES}
     BarSuperior.visible := True;
     DoConnect;
     lEnabled := CheckDerecho(D_SUPER_PLANVACACION,K_DERECHO_SIST_KARDEX);
     {$else}
     lEnabled := CheckDerecho(D_CONS_PLANVACACION,K_DERECHO_SIST_KARDEX);
     BarSuperior.Visible := FALSE;
     {$endif}
     btnAutorizar_DevEx.Enabled := lEnabled;
     btnRechazar_Devex.Enabled := lEnabled;
     btnCancelar_DevEx.Enabled := lEnabled;

     {***CONFIGURACION GRID***}
     {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
      //Desactiva la opcion de filtrar por columna
      //ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;

      //Desactiva la posibilidad de agrupar
      ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
      //Esconde la caja de agrupamiento
      ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
      //Para que nunca muestre el filterbox inferior
      ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
      //Para que no aparezca el Custom Dialog
      ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
      //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
      ApplyMinWidth;
      //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
      ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TPlanVacacion_DevEx.NewClick(Sender: TObject);
begin
     inherited;
     self.DoInsert
end;

procedure TPlanVacacion_DevEx.DelClick(Sender: TObject);
begin
     inherited;
     self.DoDelete;
end;

procedure TPlanVacacion_DevEx.EditClick(Sender: TObject);
begin
     inherited;
     self.DoEdit;
end;

procedure TPlanVacacion_DevEx.RefrescarClick(Sender: TObject);
begin
     inherited;
     self.DoRefresh;
end;

procedure TPlanVacacion_DevEx.PrintClick(Sender: TObject);
begin
     inherited;
     self.DoPrint;
end;

procedure TPlanVacacion_DevEx.ValidarFechas(Sender: TObject);
begin
     if FechaFinal.Valor < FechaInicial.Valor then
     begin
          DataBaseError('La fecha final deber ser mayor � igual a la fecha inicial');
     end;
end;

procedure TPlanVacacion_DevEx.SalirClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TPlanVacacion_DevEx.ZetaDBGridDBTableViewCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  DoEdit;
end;

procedure TPlanVacacion_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TPlanVacacion_DevEx._A_ImprimirExecute(Sender: TObject);
begin
  inherited;
  DoPrint;
end;

procedure TPlanVacacion_DevEx._E_AgregarExecute(Sender: TObject);
begin
  inherited;
  DoInsert;
end;

procedure TPlanVacacion_DevEx._E_BorrarExecute(Sender: TObject);
begin
  inherited;
  DoDelete;
end;

procedure TPlanVacacion_DevEx._E_ModificarExecute(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TPlanVacacion_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );

end;

procedure TPlanVacacion_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
   {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TPlanVacacion_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TPlanVacacion_DevEx.dxBarManager1ShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
  inherited;
  Abort;
end;

procedure TPlanVacacion_DevEx.ZetaDBGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
   DoEdit;
  end;
end;

end.
