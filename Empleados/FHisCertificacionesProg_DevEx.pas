unit FHisCertificacionesProg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls,ZbaseGridLectura_DevEx, Grids, DBGrids,
  ZetaDBGrid, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  THisCertificacionesProg_DevEx = class(TbaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

  protected
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeAgregar(var sMensaje: String ): Boolean; override;
  public

  end;

var
  HisCertificacionesProg_DevEx: THisCertificacionesProg_DevEx;

implementation

{$R *.DFM}

uses
     dRecursos,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientDataSet;

procedure THisCertificacionesProg_DevEx.Connect;
begin
     with dmRecursos do
     begin
          cdsHisCerProg.Conectar;
          DataSource.DataSet:= cdsHisCerProg;
     end;
end;

procedure THisCertificacionesProg_DevEx.Refresh;
begin
     TZetaClientDataSet( DataSource.DataSet ).Refrescar;  //Se cambi� para compartir l�gica con supervisores
end;

procedure THisCertificacionesProg_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_Certificaciones_Programadas;
end;

function THisCertificacionesProg_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          sMensaje := 'No Se Pueden Borrar Certificaciones Programados';
          Result := False;
     end;
end;

function THisCertificacionesProg_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= FALSE;
     sMensaje:= 'No se pueden modificar Certificaciones Programadas';
end;

function THisCertificacionesProg_DevEx.PuedeAgregar(var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          sMensaje := 'No Se Puede Agregar Certificaciones Programadas';
          Result := False;
     end;
end;



procedure THisCertificacionesProg_DevEx.FormShow(Sender: TObject);
begin
//CreaColumaSumatoria('PC_OPCIONA',skCount);
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;

end;

end.
