unit FGridCuentasDespensa_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TTarjetasDespensa_DevEx = class(TBaseGridEdicion_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
  protected
    procedure Connect; override;
    procedure Buscar; override;
  public
    { Public declarations }
  end;

var
  TarjetasDespensa_DevEx: TTarjetasDespensa_DevEx;

implementation

uses DCliente,
     DRecursos,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx;

{$R *.DFM}

{ TBancaElectronica }

procedure TTarjetasDespensa_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_REG_TARJETAS_DESPENSA;
end;

procedure TTarjetasDespensa_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmRecursos do
     begin
          cdsTarjetasDespensa.Refrescar;
          DataSource.DataSet:= cdsTarjetasDespensa;
     end;
end;

procedure TTarjetasDespensa_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

procedure TTarjetasDespensa_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ) then
     begin
          with dmRecursos.cdsTarjetasDespensa do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;      
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

end.
