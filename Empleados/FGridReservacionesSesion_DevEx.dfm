inherited GridReservacionesSesion_DevEx: TGridReservacionesSesion_DevEx
  Caption = 'Reservaciones de sesi'#243'n'
  ClientHeight = 366
  ClientWidth = 742
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 330
    Width = 742
    inherited OK_DevEx: TcxButton
      Left = 574
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 654
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 742
    inherited ValorActivo2: TPanel
      Width = 416
      inherited textoValorActivo2: TLabel
        Width = 410
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Top = 124
    Width = 742
    Height = 206
    ReadOnly = True
    Columns = <
      item
        Expanded = False
        FieldName = 'CONFLICTO'
        Title.Alignment = taCenter
        Title.Caption = #243
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clWindowText
        Title.Font.Height = -11
        Title.Font.Name = 'Wingdings'
        Title.Font.Style = [fsBold]
        Width = 20
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RV_ORDEN'
        Title.Caption = 'Referencia'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RV_DIA'
        Title.Caption = 'D'#237'a'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RV_FEC_INI'
        Title.Caption = 'Fecha inicial'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RV_HOR_INI'
        Title.Caption = 'Hora inicial'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RV_HOR_FIN'
        Title.Caption = 'Hora final'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AL_CODIGO'
        Title.Caption = 'Aula'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MA_NOMBRE'
        Title.Caption = 'Maestro'
        Width = 200
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'ASISTENCIA'
        Title.Caption = 'Asistencia'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RV_RESUMEN'
        Title.Caption = 'Resumen'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DESCRIP'
        Title.Caption = 'Reserv'#243
        Width = 200
        Visible = True
      end>
  end
  inherited Panel1: TPanel
    Width = 742
    Height = 74
    inherited lblFolio: TLabel
      Left = 618
      Top = 9
    end
    inherited SE_FOLIO: TZetaDBTextBox
      Left = 645
    end
    object Label3: TLabel
      Left = 601
      Top = 29
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inscritos:'
    end
    object SE_INSCRITO: TZetaDBTextBox
      Left = 645
      Top = 27
      Width = 73
      Height = 17
      AutoSize = False
      Caption = 'SE_INSCRITO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SE_INSCRITO'
      DataSource = dsSesion
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lblPeriodo: TLabel
      Left = 7
      Top = 48
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Periodo:'
    end
    object ztbPeriodo: TZetaTextBox
      Left = 48
      Top = 46
      Width = 391
      Height = 17
      AutoSize = False
      Caption = 'ztbPeriodo'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  object imgListConflicto: TcxImageList
    FormatVersion = 1
    DesignInfo = 12058936
    ImageInfo = <
      item
        Image.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000000B0F100016
          1D2000161D2000161D2000161D2000161D2000161D2000161D2000161D200016
          1D2000161D2000161D2000161D2000161D2000161D20000E12140092C1D300B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00AADDF300485F6800B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF005975800059758000B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF005F7C87000304040092
          C1D300B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00A0D2E7000B0F1000000000002F
          3E4400B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0085AFBF0085AFBF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00465B6400000000000000000000
          0000007DA4B300B2E9FF00B2E9FF00B2E9FF00B2E9FF005975800059758000B2
          E9FF00B2E9FF00B2E9FF00B2E9FF0090BCCF0003040400000000000000000000
          0000001C242800AEE4FB00B2E9FF00B2E9FF00B2E9FF000000000000000000B2
          E9FF00B2E9FF00B2E9FF00B2E9FF002F3E440000000000000000000000000000
          0000000000000067879300B2E9FF00B2E9FF00B2E9FF000000000000000000B2
          E9FF00B2E9FF00B2E9FF007DA4B3000000000000000000000000000000000000
          000000000000000B0F1000A7D9EF00B2E9FF00B2E9FF000000000000000000B2
          E9FF00B2E9FF00AEE4FB00192124000000000000000000000000000000000000
          00000000000000000000004B626C00B2E9FF00B2E9FF000000000000000000B2
          E9FF00B2E9FF00607E8B00000000000000000000000000000000000000000000
          00000000000000000000000304040095C4D700B2E9FF005975800059758000B2
          E9FF00A3D7EB000B0F1000000000000000000000000000000000000000000000
          00000000000000000000000000000032424800B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00485F680000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000080A6B700B2E9FF00B2E9FF0092
          C1D3000304040000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000001C242800AEE4FB00B2E9FF002F
          3E44000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000607E8B007295A30000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
end
