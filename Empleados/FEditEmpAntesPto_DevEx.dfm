inherited EditEmpAntesPto_DevEx: TEditEmpAntesPto_DevEx
  Left = 114
  Top = 344
  Caption = 'Puestos Anteriores'
  ClientHeight = 219
  ClientWidth = 421
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 27
    Top = 65
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label2: TLabel [1]
    Left = 4
    Top = 138
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha Inicial:'
  end
  object Label3: TLabel [2]
    Left = 23
    Top = 113
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa:'
  end
  object Label4: TLabel [3]
    Left = 31
    Top = 89
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Puesto:'
  end
  object Label5: TLabel [4]
    Left = 9
    Top = 162
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha Final:'
  end
  inherited PanelBotones: TPanel
    Top = 183
    Width = 421
    TabOrder = 5
    inherited OK_DevEx: TcxButton
      Left = 256
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 335
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 421
    TabOrder = 7
    inherited ValorActivo2: TPanel
      Width = 95
      inherited textoValorActivo2: TLabel
        Width = 89
      end
    end
  end
  object AP_PUESTO: TDBEdit [8]
    Left = 83
    Top = 85
    Width = 330
    Height = 21
    DataField = 'AP_PUESTO'
    DataSource = DataSource
    TabOrder = 1
  end
  object AP_FEC_INI: TZetaDBFecha [9]
    Left = 83
    Top = 133
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '15/dic/97'
    Valor = 35779.000000000000000000
    DataField = 'AP_FEC_INI'
    DataSource = DataSource
  end
  object AP_EMPRESA: TDBEdit [10]
    Left = 83
    Top = 109
    Width = 330
    Height = 21
    DataField = 'AP_EMPRESA'
    DataSource = DataSource
    TabOrder = 2
  end
  object AP_FOLIO: TZetaDBNumero [11]
    Left = 83
    Top = 61
    Width = 60
    Height = 21
    Mascara = mnDias
    TabOrder = 0
    Text = '0'
    DataField = 'AP_FOLIO'
    DataSource = DataSource
  end
  object AP_FEC_FIN: TZetaDBFecha [12]
    Left = 83
    Top = 157
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 4
    Text = '15/dic/97'
    Valor = 35779.000000000000000000
    DataField = 'AP_FEC_FIN'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 20
    Top = 189
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
