inherited HisIncapaci_DevEx: THisIncapaci_DevEx
  Left = 115
  Top = 123
  Caption = 'Historial de Incapacidades'
  ClientWidth = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 591
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 265
      inherited textoValorActivo2: TLabel
        Width = 259
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 591
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object IN_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'IN_FEC_INI'
      end
      object IN_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'IN_DIAS'
      end
      object IN_FEC_FIN: TcxGridDBColumn
        Caption = 'Regresa'
        DataBinding.FieldName = 'IN_FEC_FIN'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object IN_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'IN_NUMERO'
      end
      object IN_COMENTA: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'IN_COMENTA'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 8
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
