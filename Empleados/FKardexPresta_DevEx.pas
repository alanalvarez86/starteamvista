unit FKardexPresta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox,
  Mask, ComCtrls, Buttons, FKardexBase_DevEx, ZetaSmartLists, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TKardexPresta_DevEx = class(TKardexBase_DevEx )
    Label3: TLabel;
    CB_TABLASS: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure SetEditarSoloActivos;
  protected
    procedure Connect;override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  KardexPresta_DevEx: TKardexPresta_DevEx;

implementation

uses ZAccesosTress,ZetaCommonClasses,ZetaCommonLists,
     dCatalogos, dSistema, dRecursos;

{$R *.DFM}

procedure TKardexPresta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10159_Cambio_prestaciones;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     CB_TABLASS.LookupDataset := dmCatalogos.cdsSSocial;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TKardexPresta_DevEx.Connect;
begin
     dmCatalogos.cdsSSocial.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmRecursos do
     begin
          cdsEditHisKardex.Conectar;
          DataSource.DataSet := cdsEditHisKardex;
     end;
end;

procedure TKardexPresta_DevEx.SetEditarSoloActivos;
begin
     CB_TABLASS.EditarSoloActivos := TRUE;
end;

end.






