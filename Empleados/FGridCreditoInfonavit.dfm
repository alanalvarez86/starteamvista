inherited CreditoInfonavit: TCreditoInfonavit
  Caption = 'Registro de Cr�dito de Infonavit'
  ClientHeight = 206
  ClientWidth = 606
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 170
    Width = 606
    inherited OK: TBitBtn
      Left = 438
    end
    inherited Cancelar: TBitBtn
      Left = 523
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 606
    inherited ValorActivo2: TPanel
      Width = 280
    end
  end
  inherited PanelSuperior: TPanel
    Width = 606
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 606
    Height = 119
    OnColExit = ZetaDBGridColExit
    OnDrawColumnCell = ZetaDBGridDrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        PickList.Strings = ()
        Title.Caption = 'N�mero'
        Width = 65
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PRETTYNAME'
        PickList.Strings = ()
        ReadOnly = True
        Title.Caption = 'Nombre Completo'
        Width = 300
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_INFTIPO'
        PickList.Strings = ()
        Title.Caption = 'Tipo'
        Width = 100
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_INFCRED'
        PickList.Strings = ()
        Title.Caption = '# de Cr�dito'
        Width = 120
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_INFTASA'
        PickList.Strings = ()
        Title.Caption = 'Amortizaci�n'
        Width = 70
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_INF_OLD'
        PickList.Strings = ()
        Title.Caption = 'Tasa Anterior'
        Width = 70
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_INFMANT'
        PickList.Strings = ()
        Title.Caption = 'Mtto.'
        Width = 60
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CB_INF_INI'
        PickList.Strings = ()
        Title.Caption = 'Inicio de Cr�dito'
        Width = 104
        Visible = True
      end>
  end
  object zCombo: TZetaDBKeyCombo [4]
    Left = 380
    Top = 70
    Width = 103
    Height = 21
    TabStop = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    Visible = False
    ListaFija = lfTipoInfonavit
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'CB_INFTIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object zFecha: TZetaDBFecha [5]
    Left = 716
    Top = 69
    Width = 106
    Height = 22
    Cursor = crArrow
    TabStop = False
    TabOrder = 5
    Text = '15/Apr/03'
    Valor = 37726
    Visible = False
    DataField = 'CB_INF_INI'
    DataSource = DataSource
  end
  object zSNCombo: TDBComboBox [6]
    Left = 648
    Top = 72
    Width = 73
    Height = 21
    TabStop = False
    Style = csDropDownList
    DataField = 'CB_INFMANT'
    DataSource = DataSource
    ItemHeight = 13
    Items.Strings = (
      'S�'
      'No')
    TabOrder = 6
    Visible = False
    OnKeyPress = zSNComboKeyPress
  end
end
