unit ZetaBuscaPlaza_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons, ExtCtrls,
  ZetaBusqueda_DevEx,
  ZetaClientDataset, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TBuscaPlazas_DevEx = class(TBusqueda_DevEx)
    PU_CODIGO: TcxGridDBColumn;
    PL_ORDEN: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BuscaPlazas_DevEx: TBuscaPlazas_DevEx;

function ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

{$R *.DFM}

function ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String ): Boolean;
var
   oBusqueda: TBuscaPlazas_DevEx;
begin
     oBusqueda := TBuscaPlazas_DevEx.Create( Application );
     try
        with oBusqueda do
        begin
             Dataset := LookupDataset;
             Filtro := sFilter;
             Codigo := sKey;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  sKey := Codigo;
                  sDescription := LookupDataset.GetDescription;
             end;
        end;
     finally
            oBusqueda.Free;
     end;
end;

end.
