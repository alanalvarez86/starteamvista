unit FEditContrataDlg_DevEx;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  cxContainer, cxEdit, cxGroupBox, cxRadioGroup;

type
  TEditContrataDlg_DevEx = class(TZetaDlgModal_DevEx)
    Operacion: TcxRadioGroup;
    Tipo: TcxRadioGroup;
    TipoPlaza: TcxRadioGroup;
    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetTipoMov: string;

  end;

var
  EditContrataDlg_DevEx: TEditContrataDlg_DevEx;

implementation


uses ZetaCommonClasses, DCliente;

{$R *.DFM}

procedure TEditContrataDlg_DevEx.OK_DevExClick(Sender: TObject);
begin
     Cancelar_DevEx.setFocus;
end;

procedure TEditContrataDlg_DevEx.FormShow(Sender: TObject);
begin
     Operacion.ItemIndex := 0;
     ActiveControl := Operacion;
     TipoPlaza.Visible := dmCliente.UsaPlazas;
     Tipo.Visible := NOT TipoPlaza.Visible;
end;

procedure TEditContrataDlg_DevEx.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TEditContrataDlg_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= H11121_Tipo_Modificacion_contratacion;

end;

function TEditContrataDlg_DevEx.GetTipoMov: string;
begin
     if dmCliente.UsaPlazas then
     begin
          case TipoPlaza.ItemIndex of
               0: Result := K_T_ALTA;
               1: Result := K_T_RENOVA;
               2: Result := K_T_PUESTO;
               3: Result := K_T_PLAZA;
          else
               Result := K_T_TIPNOM;
          end;
     end
     else
     begin
          case Tipo.ItemIndex of
               0: Result := K_T_ALTA;
               1: Result := K_T_RENOVA;
               2: Result := K_T_PUESTO;
               3: Result := K_T_TURNO;
          else
               Result := K_T_TIPNOM;
          end;
     end;
end;

end.
