inherited EditHisCierre_DevEx: TEditHisCierre_DevEx
  Left = 232
  Top = 209
  Caption = 'Cierre de Vacaciones'
  ClientHeight = 282
  ClientWidth = 434
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 246
    Width = 434
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 268
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 347
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 434
    TabOrder = 3
    inherited ValorActivo2: TPanel
      Width = 108
      inherited textoValorActivo2: TLabel
        Width = 102
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 434
    Height = 196
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 39
      Top = 17
      Width = 78
      Height = 13
      Caption = 'Fecha de Cierre:'
    end
    object VA_D_GOZOLbl: TLabel
      Left = 30
      Top = 41
      Width = 87
      Height = 13
      Caption = 'Derecho de Gozo:'
    end
    object VA_D_PAGOLbl: TLabel
      Left = 30
      Top = 65
      Width = 87
      Height = 13
      Caption = 'Derecho de Pago:'
    end
    object Label4: TLabel
      Left = 43
      Top = 109
      Width = 74
      Height = 13
      Caption = 'Observaciones:'
    end
    object Label2: TLabel
      Left = 44
      Top = 166
      Width = 73
      Height = 13
      Caption = 'Registro global:'
    end
    object Label3: TLabel
      Left = 4
      Top = 89
      Width = 113
      Height = 13
      Caption = 'Derecho de Prima Vac.:'
    end
    object VA_FEC_INI: TZetaDBFecha
      Left = 121
      Top = 12
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '17/dic/97'
      Valor = 35781.000000000000000000
      DataField = 'VA_FEC_INI'
      DataSource = DataSource
    end
    object VA_D_GOZO: TZetaDBNumero
      Left = 121
      Top = 37
      Width = 65
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 1
      Text = '0.00'
      DataField = 'VA_D_GOZO'
      DataSource = DataSource
    end
    object VA_D_PAGO: TZetaDBNumero
      Left = 121
      Top = 61
      Width = 65
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 2
      Text = '0.00'
      DataField = 'VA_D_PAGO'
      DataSource = DataSource
    end
    object VA_GLOBAL: TDBCheckBox
      Left = 121
      Top = 164
      Width = 17
      Height = 17
      DataField = 'VA_GLOBAL'
      DataSource = DataSource
      ReadOnly = True
      TabOrder = 5
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object VA_COMENTA: TDBMemo
      Left = 121
      Top = 109
      Width = 296
      Height = 52
      DataField = 'VA_COMENTA'
      DataSource = DataSource
      ScrollBars = ssVertical
      TabOrder = 4
    end
    object VA_D_PRIMA: TZetaDBNumero
      Left = 121
      Top = 85
      Width = 65
      Height = 21
      Mascara = mnDiasFraccion
      TabOrder = 3
      Text = '0.00'
      DataField = 'VA_D_PRIMA'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 316
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
