inherited HisPlanCapacita_DevEx: THisPlanCapacita_DevEx
  Left = 643
  Top = 254
  Caption = 'Plan de Capacitaci'#243'n'
  ClientHeight = 406
  ClientWidth = 772
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 772
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 446
      inherited textoValorActivo2: TLabel
        Width = 440
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 772
    Height = 38
    Align = alTop
    TabOrder = 2
    DesignSize = (
      772
      38)
    object Label2: TLabel
      Left = 608
      Top = 8
      Width = 74
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Promedio Total:'
    end
    object txtPorcentaje: TZetaTextBox
      Left = 685
      Top = 8
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = 'PorcCumplido'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object txtPorcPerfil: TZetaTextBox
      Left = 304
      Top = 8
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = 'PorcCumplido'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label3: TLabel
      Left = 182
      Top = 8
      Width = 118
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Cumplimiento del Puesto:'
    end
    object txtPorcIndividual: TZetaTextBox
      Left = 501
      Top = 8
      Width = 57
      Height = 17
      Alignment = taRightJustify
      Anchors = [akRight, akBottom]
      AutoSize = False
      Caption = 'PorcCumplido'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label4: TLabel
      Left = 384
      Top = 8
      Width = 113
      Height = 13
      Anchors = [akRight, akBottom]
      Caption = 'Cumplimiento Individual:'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Width = 772
    Height = 349
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CC_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CC_CODIGO'
        MinWidth = 75
        Options.Grouping = False
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Nombre de la Competencia'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 160
        Options.Grouping = False
      end
      object NC_DESCRIP: TcxGridDBColumn
        Caption = 'Nivel'
        DataBinding.FieldName = 'NC_DESCRIP'
        MinWidth = 75
        Options.Grouping = False
      end
      object EC_FECHA: TcxGridDBColumn
        Caption = 'Fecha de Evaluaci'#243'n'
        DataBinding.FieldName = 'EC_FECHA'
        MinWidth = 150
        Options.Grouping = False
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Evalu'#243
        DataBinding.FieldName = 'US_DESCRIP'
        MinWidth = 75
        Options.Grouping = False
      end
      object PC_GLOBAL: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'PC_GLOBAL'
        MinWidth = 75
      end
      object CP_ELEMENT: TcxGridDBColumn
        Caption = 'Grupo de Competencias'
        DataBinding.FieldName = 'CP_ELEMENT'
        MinWidth = 150
      end
      object Observaciones: TcxGridDBColumn
        DataBinding.FieldName = 'Observaciones'
        MinWidth = 105
        Options.Grouping = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 824
    Top = 48
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end