inherited WizPlazas_DevEx: TWizPlazas_DevEx
  Left = 459
  Top = 268
  Caption = 'Registro de Plazas'
  ClientHeight = 366
  ClientWidth = 534
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 534
    Height = 366
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC200000EC2011528
      4A80000003A5494441545847C5975D48536118C767ADB00F08428824F4C6BE56
      04690569174D4C8D6A5B107611815144415110147E6CAF67735FE73DDB740B22
      22FA40D12DA2ECEBA2920CC36AE2DCE6C711BCF0B24B832EBAF0623DCF7ACF3C
      9BAFA6C9E6033F8E7B9F87F7FF3FCFFBC1519348245614EE602EE10EE612EE60
      2EF9EFB84BEE6E1405D7716A73551242F2D970F603C4B4D426BA2481CE000984
      0AE22F6A755A5849764312C42E4538136AA54E56969DA0841EE509AB98F1104F
      092B5F7AD43B5EE6598263E596D0C40373B7FC820465FD79E79B3C96D64856C9
      C6114D034C5E63E5C9F00D198BDAA3061A881B5F0662A67AD7B75A2D4BCD0D10
      2624349150630ECA3E7826F360E01E4F3485550403EEE66431445BE45445206E
      4AA8F1C74CF15B3DFBD7B292D920DDE39B32C5159A9F0E6FC51A98FC0A5758C1
      0A10B7096BAFDE3BAC05B1E94C03081D3C5E87356961E91EDBC113472CDDA315
      582311A90076FC34571C80DCA47224EF3C3FA2E38923BEC8C936AC490B73C748
      214F1C697A1CDEC9CAD0C491794CFC80DC2E56A6B1F41CDBCC1347A470ED6556
      361BE4D194C61294DF668AC3BE1868F2BD4EAD19DE5E2EE2DA86470E8EE42710
      FE4005DA82DD51F24AC01EB8C131F0FBF6ABD24256921EA44BD682099B4ABCBD
      C1D7939C180345708DF1347804FA48010C3C10897813BB8317152BD708BD95AB
      0231639DB217E0F9B1E97DB98EA5D3838446F3C833790309C92570FC742438BE
      AFB123B6B7C571BF18F3207C096FBC8CB6F3185696C2DE57951F889E2EF20D1A
      7607A2C683701CB70B9FF55B1ADE95AFC67C2A1A3B6305F0B65FD4ADFFDB0179
      DADEEA2FA644347384E682A7009E49A360A2BDEFDCC18CF62B4C7A23278A92E2
      0D8FC3EB5028531C69EE1839230A9E52983475EF2F165896AFB01C6BA0EDBD1C
      0349C4707581C61C926FF2C411ABF7D91E782B1F4F20057B6B1E54B4EAFC51C3
      599E38024BF20476FE442F4F1C696B7D980FEDECE74DBE183C5ECB75EFA0F100
      4F1C81EEFC84CDC717873DD18F4B04134D654EBC683C56D1FBDDA0E3892B2C68
      009ECB33005F2B6D43869D3C6185EC1A70DB9DEEBE93653C6185EC1A7039ECCB
      EAC0B2F7001A881877F084153470E3E9CD5DA335E6AEB1EAA6CEB8093F4280A3
      E6CEB1434B3600DF0369BFC1407BD8B0DE1F35EAE138EA7D43276AFCC3864AFC
      DB35505587BFB1030B0213FDDB807217A80DE098C3D98A2FB160F044D5C027D6
      198988F573B0392F4A2DEE0B4904F64454796AB7973199F943FD4FC24AC01DCC
      25DCC15CC21DCC1D09CD1FBF1FA3CB22C0F0220000000049454E44AE426082}
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite crear una o varias plazas en el organigrama ' +
        'de la empresa.'
      Header.Title = 'Agregar Plazas'
      object lblPuesto: TLabel
        Left = 109
        Top = 9
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
        Transparent = True
      end
      object Label5: TLabel
        Left = 86
        Top = 33
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
        Transparent = True
      end
      object Label6: TLabel
        Left = 96
        Top = 57
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Reporta a:'
        Transparent = True
      end
      object Label16: TLabel
        Left = 101
        Top = 81
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Relaci'#243'n:'
        Transparent = True
      end
      object Label2: TLabel
        Left = 57
        Top = 105
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero de Plazas:'
        Transparent = True
      end
      object PU_CODIGO: TZetaKeyLookup_DevEx
        Left = 149
        Top = 5
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        OnValidKey = PU_CODIGOValidKey
      end
      object PL_CODIGO: TZetaEdit
        Left = 149
        Top = 29
        Width = 60
        Height = 21
        MaxLength = 10
        TabOrder = 1
      end
      object PL_NOMBRE: TZetaEdit
        Left = 211
        Top = 29
        Width = 213
        Height = 21
        TabOrder = 2
      end
      object PL_REPORTA: TZetaKeyLookup_DevEx
        Left = 149
        Top = 53
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object PL_TIREP: TZetaKeyCombo
        Left = 149
        Top = 77
        Width = 115
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 4
        ListaFija = lfJIPlazaReporta
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object NumeroPlazas: TZetaNumero
        Left = 149
        Top = 101
        Width = 70
        Height = 21
        Mascara = mnDias
        TabOrder = 5
        Text = '0'
        UseEnterKey = True
      end
      object GroupBox1: TGroupBox
        Left = 96
        Top = 124
        Width = 353
        Height = 73
        Caption = ' Vigencia '
        TabOrder = 6
        object lblVence: TLabel
          Left = 187
          Top = 46
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vence:'
          Enabled = False
          OnClick = ckVenceClick
        end
        object Label1: TLabel
          Left = 18
          Top = 19
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object Label3: TLabel
          Left = 14
          Top = 47
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicio:'
        end
        object PL_FEC_FIN: TZetaFecha
          Left = 223
          Top = 42
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 3
          Text = '12/dic/05'
          Valor = 38698.000000000000000000
        end
        object ckVence: TCheckBox
          Left = 171
          Top = 44
          Width = 14
          Height = 17
          TabOrder = 2
          OnClick = ckVenceClick
        end
        object PL_FEC_INI: TZetaFecha
          Left = 45
          Top = 42
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '12/dic/05'
          Valor = 38698.000000000000000000
        end
        object PL_TIPO: TZetaKeyCombo
          Left = 45
          Top = 15
          Width = 115
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 0
          ListaFija = lfJITipoPlaza
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object btnFinalizar: TcxButton
        Left = 96
        Top = 200
        Width = 131
        Height = 26
        Hint = 'Ir directamente a crear la lista de plazas'
        Caption = 'Omitir Configuraci'#243'n'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = btnFinalizarClick
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFC0EFD8FF8FE2BAFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFDBF6E9FF7CDDAEFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFC5F0
          DBFF69D8A2FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFCFEFDFFB2EBD0FF5BD59AFF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF4FCF8FFA0E6C4FF81DFB1FFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4FCF8FFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9F9F1FFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFE6F9F0FF8AE1B7FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF4FCF8FF9DE6C2FF53D395FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFCFEFDFFAFEB
          CEFF5BD59AFF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFC5F0DBFF69D8A2FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFAFEBCEFF79DDACFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9F9F1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
        OptionsImage.Margin = 1
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 131
        Width = 512
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 512
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 444
          AnchorX = 289
          AnchorY = 51
        end
      end
    end
    object Contratacion: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Complete los campos para indicar la informaci'#243'n de contrataci'#243'n ' +
        'de la plaza.'
      Header.Title = 'Contrataci'#243'n'
      object Label9: TLabel
        Left = 73
        Top = 81
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de N'#243'mina:'
        Transparent = True
      end
      object Label4: TLabel
        Left = 89
        Top = 9
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
        Transparent = True
      end
      object Label26: TLabel
        Left = 120
        Top = 33
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
        Transparent = True
      end
      object Label29: TLabel
        Left = 67
        Top = 57
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Registro Patronal:'
        Transparent = True
      end
      object Label27: TLabel
        Left = 69
        Top = 131
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Contrato:'
        Transparent = True
      end
      object Arealbl: TLabel
        Left = 126
        Top = 156
        Width = 25
        Height = 13
        Alignment = taRightJustify
        BiDiMode = bdRightToLeft
        Caption = 'Area:'
        ParentBiDiMode = False
        Transparent = True
      end
      object lblConfidencialidad: TLabel
        Left = 70
        Top = 106
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Confidencialidad:'
        Transparent = True
      end
      object Label21: TLabel
        Left = 81
        Top = 178
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Checa Tarjeta:'
        Transparent = True
      end
      object PL_NOMINA: TZetaKeyCombo
        Left = 155
        Top = 78
        Width = 190
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        ListaFija = lfTipoPeriodoConfidencial
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = True
      end
      object PL_CLASIFI: TZetaKeyLookup_DevEx
        Left = 155
        Top = 5
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object PL_TURNO: TZetaKeyLookup_DevEx
        Left = 155
        Top = 29
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object PL_PATRON: TZetaKeyLookup_DevEx
        Left = 155
        Top = 54
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
      end
      object PL_CONTRAT: TZetaKeyLookup_DevEx
        Left = 155
        Top = 127
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
      end
      object PL_AREA: TZetaKeyLookup_DevEx
        Left = 155
        Top = 152
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
      end
      object PL_NIVEL0: TZetaKeyLookup_DevEx
        Left = 155
        Top = 102
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
      end
      object PL_CHECA: TCheckBox
        Left = 155
        Top = 176
        Width = 97
        Height = 17
        TabOrder = 7
      end
    end
    object Area: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Complete los campos para indicar la informaci'#243'n de '#225'rea de la pl' +
        'aza.'
      Header.Title = #193'rea'
      object PL_NIVEL12lbl: TLabel
        Left = 108
        Top = 259
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Transparent = True
        Visible = False
      end
      object PL_NIVEL11lbl: TLabel
        Left = 108
        Top = 238
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Transparent = True
        Visible = False
      end
      object PL_NIVEL10lbl: TLabel
        Left = 108
        Top = 215
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Transparent = True
        Visible = False
      end
      object PL_NIVEL9lbl: TLabel
        Left = 114
        Top = 192
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
        Transparent = True
      end
      object PL_NIVEL8lbl: TLabel
        Left = 114
        Top = 169
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
        Transparent = True
      end
      object PL_NIVEL7lbl: TLabel
        Left = 114
        Top = 146
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
        Transparent = True
      end
      object PL_NIVEL6lbl: TLabel
        Left = 114
        Top = 121
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
        Transparent = True
      end
      object PL_NIVEL5lbl: TLabel
        Left = 114
        Top = 100
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
        Transparent = True
      end
      object PL_NIVEL4lbl: TLabel
        Left = 114
        Top = 77
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
        Transparent = True
      end
      object PL_NIVEL3lbl: TLabel
        Left = 114
        Top = 54
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
        Transparent = True
      end
      object PL_NIVEL2lbl: TLabel
        Left = 114
        Top = 31
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
        Transparent = True
      end
      object PL_NIVEL1lbl: TLabel
        Left = 114
        Top = 8
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
        Transparent = True
      end
      object PL_NIVEL12: TZetaKeyLookup_DevEx
        Left = 148
        Top = 257
        Width = 304
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PL_NIVEL12ValidLookup
      end
      object PL_NIVEL11: TZetaKeyLookup_DevEx
        Left = 148
        Top = 234
        Width = 304
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PL_NIVEL11ValidLookup
      end
      object PL_NIVEL10: TZetaKeyLookup_DevEx
        Left = 148
        Top = 211
        Width = 304
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        OnValidLookup = PL_NIVEL10ValidLookup
      end
      object PL_NIVEL9: TZetaKeyLookup_DevEx
        Left = 148
        Top = 188
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL9ValidLookup
      end
      object PL_NIVEL8: TZetaKeyLookup_DevEx
        Left = 148
        Top = 165
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL8ValidLookup
      end
      object PL_NIVEL7: TZetaKeyLookup_DevEx
        Left = 148
        Top = 142
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL7ValidLookup
      end
      object PL_NIVEL6: TZetaKeyLookup_DevEx
        Left = 148
        Top = 119
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL6ValidLookup
      end
      object PL_NIVEL5: TZetaKeyLookup_DevEx
        Left = 148
        Top = 96
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL5ValidLookup
      end
      object PL_NIVEL4: TZetaKeyLookup_DevEx
        Left = 148
        Top = 73
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL4ValidLookup
      end
      object PL_NIVEL3: TZetaKeyLookup_DevEx
        Left = 148
        Top = 50
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL3ValidLookup
      end
      object PL_NIVEL2: TZetaKeyLookup_DevEx
        Left = 148
        Top = 27
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = PL_NIVEL2ValidLookup
      end
      object PL_NIVEL1: TZetaKeyLookup_DevEx
        Left = 148
        Top = 4
        Width = 304
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    object Salario: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Complete los campos para indicar la informaci'#243'n de salario de la' +
        ' plaza.'
      Header.Title = 'Salario'
      object Label31: TLabel
        Left = 86
        Top = 123
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona Geogr'#225'fica:'
        Transparent = True
      end
      object Label28: TLabel
        Left = 60
        Top = 99
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tabla de Prestaciones:'
        Transparent = True
      end
      object Label33: TLabel
        Left = 58
        Top = 75
        Width = 111
        Height = 13
        Alignment = taRightJustify
        Caption = 'Promedio de  Variables:'
        Transparent = True
      end
      object LblSalario: TLabel
        Left = 134
        Top = 51
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario:'
        Transparent = True
      end
      object LblTabulador: TLabel
        Left = 62
        Top = 31
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = ' Salario por Tabulador:'
        Transparent = True
      end
      object PL_ZONA_GE: TZetaKeyCombo
        Left = 171
        Top = 119
        Width = 62
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 4
        ListaFija = lfZonaGeografica
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object PL_TABLASS: TZetaKeyLookup_DevEx
        Left = 171
        Top = 95
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object PL_PER_VAR: TZetaNumero
        Left = 171
        Top = 71
        Width = 100
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
      end
      object PL_SALARIO: TZetaNumero
        Left = 171
        Top = 47
        Width = 100
        Height = 21
        Mascara = mnPesos
        TabOrder = 1
        Text = '0.00'
      end
      object PL_AUTOSAL: TCheckBox
        Left = 171
        Top = 29
        Width = 97
        Height = 17
        TabOrder = 0
        OnClick = PL_AUTOSALClick
      end
    end
    object Generales: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Complete los campos para indicar la informaci'#243'n general de la pl' +
        'aza.'
      Header.Title = 'Generales'
      object Label10: TLabel
        Left = 59
        Top = 90
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = 'SubCuenta:'
        Transparent = True
      end
      object Label8: TLabel
        Left = 85
        Top = 66
        Width = 30
        Height = 13
        Caption = 'Texto:'
        Transparent = True
      end
      object CO_NUMEROLbl: TLabel
        Left = 75
        Top = 41
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
        Transparent = True
      end
      object Label7: TLabel
        Left = 84
        Top = 17
        Width = 31
        Height = 13
        Caption = 'Ingl'#233's:'
        Transparent = True
      end
      object PL_SUB_CTA: TEdit
        Left = 118
        Top = 85
        Width = 325
        Height = 21
        MaxLength = 30
        TabOrder = 3
      end
      object PL_TEXTO: TEdit
        Left = 118
        Top = 61
        Width = 325
        Height = 21
        MaxLength = 30
        TabOrder = 2
      end
      object PL_NUMERO: TZetaNumero
        Left = 118
        Top = 37
        Width = 70
        Height = 21
        Mascara = mnDias
        TabOrder = 1
        Text = '0'
        UseEnterKey = True
      end
      object PL_INGLES: TEdit
        Left = 118
        Top = 13
        Width = 325
        Height = 21
        MaxLength = 30
        TabOrder = 0
      end
    end
    object Titulares: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Asigne los titulares a las plazas creadas.'
      Header.Title = 'Asignar titulares'
      object gridEmpleados: TZetaDBGrid
        Left = 0
        Top = 32
        Width = 512
        Height = 194
        Align = alClient
        DataSource = DataSource
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = gridEmpleadosDrawColumnCell
        OnKeyDown = gridEmpleadosKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'PL_ORDEN'
            ReadOnly = True
            Title.Caption = '# Plaza'
            Width = 42
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Caption = 'N'#250'mero'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRETTYNAME'
            ReadOnly = True
            Title.Caption = 'Nombre'
            Width = 214
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PL_NOMBRE'
            ReadOnly = True
            Title.Caption = 'Descripci'#243'n'
            Width = 154
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 512
        Height = 32
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BBAgregar: TcxButton
          Left = 10
          Top = 2
          Width = 103
          Height = 26
          Hint = 'Asignar titular'
          BiDiMode = bdRightToLeft
          Caption = '&Asignar Titular'
          ParentBiDiMode = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BBAgregarClick
          LookAndFeel.SkinName = 'TressMorado2013'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            200000000000000900000000000000000000000000000000000050D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFAAE9CAFFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
            D6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
            D6FFBDEED6FF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FFB2EBD0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFECFAF3FF6EDAA6FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF6EDAA6FFFAFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFDFFA8E9C9FF6EDA
            A6FF7CDDAEFF5ED69BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF92E3BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA8E9C9FFA2E7C6FFFCFE
            FDFFF1FBF7FFF7FDFAFF84DFB3FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF8CE2B8FFECFAF3FFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF76DCABFFFFFFFFFFFFFF
            FFFF92E3BCFFFFFFFFFFECFAF3FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF5BD59AFF84DFB3FFA0E6C4FFBDEE
            D6FFBDEED6FFBDEED6FFBDEED6FFB2EBD0FFA5E8C8FF7CDDAEFFF4FCF8FFA8E9
            C9FF71DAA7FFB2EBD0FFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF5BD59AFFF4FCF8FFFFFF
            FFFFA0E6C4FFFFFFFFFFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFA2E7
            C6FFDBF6E9FFE9F9F1FFCBF2DFFF7CDDAEFF50D293FF50D293FF76DCABFFDEF7
            EBFFFAFEFCFFD0F3E2FF66D8A1FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFD9F5E7FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF97E4BFFF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFA8E9C9FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFEFCFF69D8A2FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFDBF6E9FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFE6F9F0FFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA5E8C8FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FFCBF2DFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8AE1B7FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4F8EEFF55D396FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF9AE5C1FFFAFE
            FCFFFFFFFFFFFFFFFFFFFFFFFFFFE4F8EEFF6BD9A4FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
            A1FF9AE5C1FFA8E9C9FF8AE1B7FF55D396FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
            93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
          OptionsImage.ImageIndex = 1
          OptionsImage.Margin = 1
        end
        object BBBorrar: TcxButton
          Left = 122
          Top = 2
          Width = 103
          Height = 26
          Hint = 'Eliminar titular'
          BiDiMode = bdRightToLeft
          Caption = '&Eliminar Titular'
          ParentBiDiMode = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BBBorrarClick
          LookAndFeel.SkinName = 'TressMorado2013'
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            20000000000000090000000000000000000000000000000000004858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF707DD7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FA
            FDFFB7BEEBFFFFFFFFFFD1D5F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFF5C6AD2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFFD1D5F2FFFFFFFFFFFFFFFFFFFFFFFFFFD7DA
            F4FF4E5DCEFFDFE2F6FF4E5DCEFFE8EAF9FFFFFFFFFFFFFFFFFFFFFFFFFFD4D8
            F3FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF6572D4FFEEEFFAFFFFFFFFFFFFFFFFFFAFB6
            E9FF4858CCFF818CDCFF4858CCFFB4BBEAFFFFFFFFFFFFFFFFFFF6F7FDFF737F
            D8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF6875D5FFDADDF5FFFFFFFFFF848F
            DDFF5362CFFFD7DAF4FF4E5DCEFF818CDCFFFFFFFFFFEEEFFAFF737FD8FF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF6D7AD6FF5362
            CFFF4858CCFF4858CCFF4858CCFF5160CEFF9EA7E4FF5160CEFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF8D97
            DFFFD4D8F3FFE8EAF9FFC6CBEFFF6D7AD6FF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFA4ACE6FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFEFF7682D9FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5C6AD2FFFCFCFEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDE0F5FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF8792DEFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFEFFB1B8E9FF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF818CDCFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFEBEDF9FF6572D4FF8D97DFFFCED2F1FFDDE0F5FFA9B0
            E7FF5160CEFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5665D0FFF4F5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFF7F8ADBFFA1A9E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFD7DAF4FF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF848FDDFFF9FA
            FDFFFFFFFFFFF1F2FBFF5362CFFFFCFCFEFFEEEFFAFFE8EAF9FFE8EAF9FFE8EA
            F9FFF9FAFDFF8792DEFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF6572
            D4FFA6AEE6FFACB3E8FF707DD7FFDFE2F6FF4858CCFF4858CCFF4858CCFF4858
            CCFF969FE2FFA4ACE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF5362CFFFFCFCFEFFDDE0F5FFD1D5F2FFD1D5F2FFD1D5
            F2FFF6F7FDFF8A94DEFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFFACB3E8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFDDE0F5FF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4B5BCDFF9BA4E3FFDDE0F5FFE2E5F7FFB4BB
            EAFF5362CFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
            CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
          OptionsImage.ImageIndex = 1
          OptionsImage.Margin = 1
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 280
    Top = 200
  end
  object DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 404
    Top = 206
  end
end
