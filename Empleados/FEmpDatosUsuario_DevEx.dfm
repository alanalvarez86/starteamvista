inherited EmpDatosUsuario_DevEx: TEmpDatosUsuario_DevEx
  Left = 347
  Top = 151
  Caption = 'Usuario'
  ClientHeight = 554
  ClientWidth = 547
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 547
    inherited ValorActivo2: TPanel
      Width = 288
      inherited textoValorActivo2: TLabel
        Width = 282
      end
    end
  end
  object PanelNoUsuario: TPanel [1]
    Left = 0
    Top = 19
    Width = 547
    Height = 41
    Align = alTop
    Caption = 'El empleado no tiene acceso a Sistema Tress'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object PanelEmpleado: TPanel [2]
    Left = 0
    Top = 60
    Width = 547
    Height = 494
    Align = alClient
    TabOrder = 2
    object GroupBox1: TcxGroupBox
      Left = 1
      Top = 290
      Align = alTop
      Caption = ' Acceso Al Sistema '
      TabOrder = 1
      Height = 119
      Width = 545
      object Label9: TLabel
        Left = 35
        Top = 19
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ultima Entrada:'
      end
      object Label10: TLabel
        Left = 43
        Top = 38
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ultima Salida:'
      end
      object US_FEC_IN: TZetaDBTextBox
        Left = 108
        Top = 17
        Width = 81
        Height = 17
        AutoSize = False
        Caption = 'US_FEC_IN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_FEC_IN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object US_FEC_OUT: TZetaDBTextBox
        Left = 108
        Top = 36
        Width = 81
        Height = 17
        AutoSize = False
        Caption = 'US_FEC_OUT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_FEC_OUT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 17
        Top = 57
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ultima Suspensi'#243'n:'
      end
      object US_FEC_SUS: TZetaDBTextBox
        Left = 108
        Top = 55
        Width = 81
        Height = 17
        AutoSize = False
        Caption = 'US_FEC_SUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_FEC_SUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object US_MAQUINA: TZetaDBTextBox
        Left = 108
        Top = 74
        Width = 313
        Height = 17
        AutoSize = False
        Caption = 'US_MAQUINA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_MAQUINA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label13: TLabel
        Left = 31
        Top = 76
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ultima M'#225'quina:'
      end
      object US_BLOQUEA: TDBCheckBox
        Left = 296
        Top = 16
        Width = 56
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Prohibir:'
        DataField = 'US_BLOQUEA'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object US_DENTRO: TDBCheckBox
        Left = 242
        Top = 34
        Width = 110
        Height = 14
        Alignment = taLeftJustify
        Caption = 'Dentro del Sistema:'
        DataField = 'US_DENTRO'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
    object GroupBox2: TcxGroupBox
      Left = 1
      Top = 159
      Align = alTop
      Caption = ' Operaci'#243'n '
      TabOrder = 0
      Height = 131
      Width = 545
      object Label7: TLabel
        Left = 5
        Top = 18
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Formato de Reportes:'
        WordWrap = True
      end
      object US_FORMATO: TZetaDBTextBox
        Left = 108
        Top = 16
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'US_FORMATO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_FORMATO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 43
        Top = 62
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Supervisores:'
      end
      object SUPERVISORES: TZetaTextBox
        Left = 108
        Top = 60
        Width = 313
        Height = 17
        AutoSize = False
        Caption = 'SUPERVISORES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label6: TLabel
        Left = 77
        Top = 84
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Areas:'
      end
      object AREAS: TZetaTextBox
        Left = 108
        Top = 82
        Width = 313
        Height = 17
        AutoSize = False
        Caption = 'AREAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object lbRoles: TLabel
        Left = 77
        Top = 106
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Roles:'
      end
      object ROLES: TZetaTextBox
        Left = 108
        Top = 104
        Width = 313
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label5: TLabel
        Left = 16
        Top = 40
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Reportes a Recibir:'
      end
      object REPORTES: TZetaTextBox
        Left = 108
        Top = 38
        Width = 313
        Height = 17
        AutoSize = False
        Caption = 'REPORTES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
    end
    object GroupBox3: TcxGroupBox
      Left = 1
      Top = 1
      Align = alTop
      TabOrder = 2
      Height = 158
      Width = 545
      object Label1: TLabel
        Left = 66
        Top = 18
        Width = 39
        Height = 13
        Caption = 'Usuario:'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 108
        Top = 16
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object US_CORTO: TZetaDBTextBox
        Left = 178
        Top = 16
        Width = 243
        Height = 17
        AutoSize = False
        Caption = 'US_CORTO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CORTO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 73
        Top = 92
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Grupo:'
      end
      object GR_CODIGO: TZetaDBTextBox
        Left = 108
        Top = 90
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'GR_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'GR_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object GR_NOMBRE: TZetaDBTextBox
        Left = 178
        Top = 90
        Width = 243
        Height = 16
        AutoSize = False
        Caption = 'GR_NOMBRE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'GR_DESCRIP'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object US_LUGAR: TZetaDBTextBox
        Left = 108
        Top = 110
        Width = 145
        Height = 17
        AutoSize = False
        Caption = 'US_LUGAR'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_LUGAR'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label11: TLabel
        Left = 60
        Top = 112
        Width = 45
        Height = 13
        Caption = 'Tel'#233'fono:'
        FocusControl = US_LUGAR
      end
      object Label4: TLabel
        Left = 57
        Top = 132
        Width = 48
        Height = 13
        Caption = 'Direcci'#243'n:'
        FocusControl = US_EMAIL
      end
      object US_EMAIL: TZetaDBTextBox
        Left = 108
        Top = 130
        Width = 313
        Height = 17
        AutoSize = False
        Caption = 'US_EMAIL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_EMAIL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label8: TLabel
        Left = 10
        Top = 72
        Width = 95
        Height = 13
        Caption = 'Usuario del dominio:'
      end
      object US_DOMAIN: TZetaDBTextBox
        Left = 108
        Top = 70
        Width = 145
        Height = 17
        AutoSize = False
        Caption = 'US_DOMAIN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_DOMAIN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object US_ACTIVO: TDBCheckBox
        Left = 70
        Top = 35
        Width = 51
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Activo:'
        DataField = 'US_ACTIVO'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object US_PORTAL: TDBCheckBox
        Left = 51
        Top = 54
        Width = 70
        Height = 14
        Alignment = taLeftJustify
        Caption = 'Usa Portal:'
        DataField = 'US_PORTAL'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 144
    Top = 8
  end
end
