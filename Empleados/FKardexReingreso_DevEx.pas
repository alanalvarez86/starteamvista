unit FKardexReingreso_DevEx;

{$define VALIDAEMPLEADOSGLOBAL}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ZetaFecha, Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TKardexReingreso_DevEx = class(TForm)
    ZReingreso: TZetaFecha;
    Label1: TLabel;
    ValorActivo1: TPanel;
    PBotones: TPanel;
    btnIdentificacion: TcxButton;
    btnPersonales: TcxButton;
    btnExperiencia: TcxButton;
    btnAdicionales: TcxButton;
    btnOtros: TcxButton;
    btnAhorro: TcxButton;
    btnAccesoATress: TcxButton;
    cxImageList24_PanelBotones: TcxImageList;
    PanelBotones: TPanel;
    BtnOk: TcxButton;
    btnCancelar: TcxButton;
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnIdentificacionClick(Sender: TObject);
    procedure btnPersonalesClick(Sender: TObject);
    procedure btnExperienciaClick(Sender: TObject);
    procedure btnOtrosClick(Sender: TObject);
    procedure btnAdicionalesClick(Sender: TObject);
    procedure btnAhorroClick(Sender: TObject);
    procedure btnAccesoATressClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    procedure AgregaAhorrosAutomaticos;
    procedure RevisaCreditoInfonavit;
  protected
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure SetControles( const lEnabled: Boolean );
  public
    FechaBaja: TDate;
    FechaBajaIMSS: TDate;
  end;

var
  KardexReingreso_DevEx: TKardexReingreso_DevEx;

implementation

uses ZetaDialogo, ZetaCommonClasses, ZetaSmartLists, DCliente, DRecursos,
     ZAccesosTress, ZAccesosMgr, ZBaseEdicion_DevEx, FEditEmpIdentific_DevEx,
     FEditEmpPersonal_DevEx, FEditEmpExperiencia_DevEx, FEditEmpOtros_DevEx, FEditEmpAdicionales_DevEx, DTablas,
     ZetaCommonLists, dSistema, ZetaCommonTools {$ifdef VALIDAEMPLEADOSGLOBAL}, ZetaLicenseClasses{$endif};

{$R *.DFM}

procedure TKardexReingreso_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= H10153_Reingreso;
end;

procedure TKardexReingreso_DevEx.FormShow(Sender: TObject);
begin
     SetControles( TRUE );
     ActiveControl:= ZReingreso;
end;

procedure TKardexReingreso_DevEx.btnOKClick(Sender: TObject);

{$ifdef VALIDAEMPLEADOSGLOBAL}
function  ValidaLicencia : boolean ;
var
   sMsgAdv, sMsgError : string;
   eEvalua : eEvaluaTotalEmpleados;
begin
      eEvalua := dmCliente.EvaluarEmpleadosCliente( evOpAlta, sMsgAdv, sMsgError );
      if StrLleno(sMsgAdv) then
         ZWarning(  self.Caption, sMsgAdv, 0, mbOK );
      if StrLleno(sMsgError) then
         ZError(  self.Caption, sMsgError, 0 );


      Result := ( eEvalua <> evRebasaRestringe );

end;
{$endif}


begin
     if ( ZReingreso.Valor <= FechaBaja ) then
     begin
        ZError('Reingreso','La fecha  de Reingreso:' + DateToStr( ZReingreso.Valor ) + CR_LF +
                           'DEBE ser mayor que :' + CR_LF +
                           DateToStr( FechaBaja ) + CR_LF +
                           'Fecha de Baja del Empleado ',0);
     end
     else
     begin

     {$ifdef VALIDAEMPLEADOSGLOBAL}
          if (not  ValidaLicencia ) then
             Self.Close
          else
          begin
     {$endif}
               dmRecursos.AplicaKardexReingreso( ZReingreso.Valor );


          if( dmCliente.GetDatosEmpleadoActivo.Activo )then
          begin
               AgregaAhorrosAutomaticos;
               RevisaCreditoInfonavit;
               SetControles( FALSE );
          end
          else
              Self.Close; { Si no se reingreso se sale }
     {$ifdef VALIDAEMPLEADOSGLOBAL}
          end;
     {$endif}
          //ModalResult:= mrOk;
     end;
end;

procedure TKardexReingreso_DevEx.KeyPress(var Key: Char);
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

procedure TKardexReingreso_DevEx.SetControles( const lEnabled: Boolean );

          function PuedeEditarDatosOtros : boolean;
          begin
              Result := (
                     ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_ASISTENCIA, K_DERECHO_CAMBIO ) OR
                     ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CAMBIO ) OR
                     ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_FONACOT, K_DERECHO_CAMBIO ) OR
                     ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_PRIMER_EMPLEO, K_DERECHO_CAMBIO ) OR
                     ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_EVALUACION,  K_DERECHO_CAMBIO ) OR
                     ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_BRIGADAS, K_DERECHO_CAMBIO ) OR
                     ZAccesosMgr.CheckDerecho( D_EMP_DATOS_OTROS_MEDICOS, K_DERECHO_CAMBIO ));
          end;

begin
        PBotones.Visible := not (lEnabled);
        if not( lEnabled )then
	   ZReingreso.Valor := dmCliente.GetDatosEmpleadoActivo.Ingreso; {Puede cambiar con respecto a la que se captur� originalmente }
        ZReingreso.Enabled := not PBotones.Visible;//Enabled;
	btnOk.Visible := lEnabled;

	if lEnabled then
	begin
             with btnCancelar do
             begin
                   //Kind := bkCancel;
               OptionsImage.ImageIndex := 0;
               ModalResult := mrCancel;
               Cancel := true;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
             end;
	     if self.active then
                ZReingreso.SetFocus;
	end
	else
        begin
             with btnCancelar do
             begin
                 //Kind := bkClose;
               OptionsImage.ImageIndex := 2;
               ModalResult := mrAbort;
               Cancel := false;
               Caption := '  &Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then  // EZM: S�lo si la forma es visible
                 SetFocus;
             end;
        end;
     if( PBotones.Visible )then
     begin
          btnIdentificacion.Enabled := CheckDerecho( D_EMP_DATOS_IDENTIFICA, K_DERECHO_CAMBIO );
          btnPersonales.Enabled := CheckDerecho( D_EMP_CURR_PERSONALES, K_DERECHO_CAMBIO );
          btnExperiencia.Enabled := CheckDerecho(D_EMP_CURR_EXPERIENCIA, K_DERECHO_CAMBIO);
          btnOtros.Enabled :=  PuedeEditarDatosOtros; //CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_CAMBIO );
          btnAdicionales.Enabled := CheckDerecho( D_EMP_DATOS_ADICION, K_DERECHO_CAMBIO );
          btnAhorro.Enabled := CheckDerecho( D_EMP_NOM_AHORROS, K_DERECHO_ALTA );
     end;
end;

procedure TKardexReingreso_DevEx.btnIdentificacionClick(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpIdentific_DevEx, TEditEmpIdentific_DevEx );
end;

procedure TKardexReingreso_DevEx.btnPersonalesClick(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpPersonal_DevEx, TEditEmpPersonal_DevEx );
end;

procedure TKardexReingreso_DevEx.btnExperienciaClick(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpExperiencia_DevEx, TEditEmpExperiencia_DevEx );
end;

procedure TKardexReingreso_DevEx.btnOtrosClick(Sender: TObject);
begin
     //ZBaseEdicion.ShowFormaEdicion( EditEmpOtros, TEditEmpOtros );
     ShowFormaEdicionOtros( D_EMP_DATOS_OTROS );
end;

procedure TKardexReingreso_DevEx.btnAdicionalesClick(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmpAdicionales_DevEx, TEditEmpAdicionales_DevEx );
end;

procedure TKardexReingreso_DevEx.AgregaAhorrosAutomaticos ;
var
   lAgrega, lModifica, lFecha: Boolean;
   oStatus : eStatusAhorro;
   sDescripAhorro : String;
begin
     dmRecursos.cdsHisAhorros.Conectar;
     with dmTablas.cdsTAhorro do
     begin
          Conectar;
          First;
     end;
     While ( not dmTablas.cdsTAhorro.EOF ) do
     begin
          oStatus := saCancelado;
          lAgrega := false;
          lModifica := false;
          lFecha := False;
          with dmTablas.cdsTAhorro do
          begin
               sDescripAhorro := FieldByName('TB_CODIGO').AsString + '=' + FieldByName('TB_ELEMENT').AsString;
               if dmRecursos.cdsHisAhorros.Locate('AH_TIPO', FieldByName('TB_CODIGO').AsString, []) then
               begin
                    lModifica := true;
                    Case eAltaAhorroPrestamo (FieldByName('TB_ALTA').AsInteger) of
                         apNoAgregar, apAgregarPreguntando:
                                                            if ( ZConfirm( self.Caption, Format( '� Desea conservar el ahorro [%s] ?',
                                                                          [sDescripAhorro ] ), 0, mbYes ) ) then
                                                               oStatus:= saActivo;

                         apAgregarAutomaticamente : oStatus := saActivo;
                    end;
                    if ( oStatus = saActivo ) then
                       lFecha := ZConfirm( self.Caption, Format ('� Desea actualizar la fecha del ahorro [%s] a la de reingreso?',
                                           [ sDescripAhorro ]), 0, mbYes);
               end
               else
               begin
                    Case eAltaAhorroPrestamo (FieldByName('TB_ALTA').AsInteger) of
                         apNoAgregar : lAgrega := False;
                         apAgregarPreguntando : lAgrega:= ZConfirm( self.Caption, Format( '� Desea Inscribirlo en el ahorro [%s] ?',
                                                                        [ sDescripAhorro ] ), 0, mbYes ) ;
                         apAgregarAutomaticamente : lAgrega := true;
                    end;
               end;
          end;

          with dmRecursos.cdsHisAhorros do
          begin
               if lAgrega then
               begin
                    Append;
                    FieldByName( 'AH_TIPO' ).AsString:= dmTablas.cdsTAhorro.FieldByName('TB_CODIGO').AsString;
                    FieldByName( 'AH_FECHA' ).AsDateTime:= dmCliente.GetDatosEmpleadoActivo.Ingreso;
                    Post;
               end
               else if lModifica then
               begin
                    Edit;
                    FieldByName('AH_STATUS').AsInteger := Ord(oStatus);
                    if lFecha then
                       FieldByName( 'AH_FECHA' ).AsDateTime:= dmCliente.GetDatosEmpleadoActivo.Ingreso;
                    Post;
               end;

          end;
          dmTablas.cdsTAhorro.next;
     end;
     dmRecursos.cdsHisAhorros.Enviar;
end;
procedure TKardexReingreso_DevEx.btnAhorroClick(Sender: TObject);
begin
      dmRecursos.cdsHisAhorros.Agregar;
end;

procedure TKardexReingreso_DevEx.btnAccesoATressClick(Sender: TObject);
begin
     with dmSistema.cdsUsuarios do
     begin
          Conectar;
          //Se busca que no se haya dado ya de alta un usuario con este empleado
          //Si ya existe un usuario con el #Empleado que se acaba de agregar, se edita ese usuario
          //Si no existe ningun usuario con ese #Empleado, se agrega un usuario nuevo
          if Locate('CB_CODIGO', dmCliente.GetDatosEmpleadoActivo.Numero, [] ) then
             dmSistema.ModificaUsuarios(FALSE)
          else dmSistema.AgregaEmpleadoUsuario;
     end;
end;

procedure TKardexReingreso_DevEx.RevisaCreditoInfonavit;

    procedure SuspenderCreditoInfonavit;
    begin
         with dmRecursos.cdsHisCreInfonavit do
         begin
              Conectar;
              Append;
              FieldByName('KI_TIPO').AsInteger:= Ord(infoSuspen);
              FieldByName('KI_FECHA').AsDateTime:= dmCliente.GetDatosEmpleadoActivo.Baja;
              FieldByName('KI_COMENTA').AsString:= 'Suspensi�n al Reingreso';
              Enviar;
         end;
    end;
begin
     dmCliente.SetLookupEmpleado( eLookEmpInfonavit );
     try
        if zStrToBool( dmCliente.cdsEmpleado.FieldByName('CB_INFACT').AsString ) and
           not ZetaDialogo.zConfirm('� Atenci�n !', 'El Empleado cuenta con cr�dito INFONAVIT, �Desea mantener activo su cr�dito?', 0, mbYes )  then
        begin
             SuspenderCreditoInfonavit;
        end;
     finally
            dmCliente.ResetLookupEmpleado;
     end;
end;

procedure TKardexReingreso_DevEx.OK_DevExClick(Sender: TObject);
{$ifdef VALIDAEMPLEADOSGLOBAL}
function  ValidaLicencia : boolean ;
var
   sMsgAdv, sMsgError : string;
   eEvalua : eEvaluaTotalEmpleados;
begin
      eEvalua := dmCliente.EvaluarEmpleadosCliente( evOpAlta, sMsgAdv, sMsgError );
      if StrLleno(sMsgAdv) then
         ZWarning(  self.Caption, sMsgAdv, 0, mbOK );
      if StrLleno(sMsgError) then
         ZError(  self.Caption, sMsgError, 0 );


      Result := ( eEvalua <> evRebasaRestringe );

end;
{$endif}


begin
     if ( ZReingreso.Valor <= FechaBaja ) then
     begin
        ZError('Reingreso','La fecha  de Reingreso:' + DateToStr( ZReingreso.Valor ) + CR_LF +
                           'DEBE ser mayor que :' + CR_LF +
                           DateToStr( FechaBaja ) + CR_LF +
                           'Fecha de Baja del Empleado ',0);
     end
     else if ( ZReingreso.Valor <= FechaBajaIMSS )  then
     begin
          ZError('Reingreso','La fecha de reingreso: ' + DateToStr( ZReingreso.Valor ) + CR_LF +
                             'debe ser mayor que' + CR_LF +
                             'fecha de baja ante IMSS: ' + DateToStr( FechaBajaIMSS ), 0);
     end
     else
     begin

     {$ifdef VALIDAEMPLEADOSGLOBAL}
          if (not  ValidaLicencia ) then
             Self.Close
          else
          begin
     {$endif}
               dmRecursos.AplicaKardexReingreso( ZReingreso.Valor );


          if( dmCliente.GetDatosEmpleadoActivo.Activo )then
          begin
               AgregaAhorrosAutomaticos;
               RevisaCreditoInfonavit;
               SetControles( FALSE );
          end
          else
              Self.Close; { Si no se reingreso se sale }
     {$ifdef VALIDAEMPLEADOSGLOBAL}
          end;
     {$endif}
          //ModalResult:= mrOk;
     end;

end;

end.
