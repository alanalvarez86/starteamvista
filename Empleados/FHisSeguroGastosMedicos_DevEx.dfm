inherited HisSegurosGastosMedicos_DevEx: THisSegurosGastosMedicos_DevEx
  Left = 424
  Top = 318
  Caption = 'Seguros de Gastos M'#233'dicos'
  ClientHeight = 287
  ClientWidth = 1271
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1271
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 945
      inherited textoValorActivo2: TLabel
        Width = 939
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 1271
    Height = 72
    Enabled = False
    TabOrder = 2
    Visible = False
    object Label1: TLabel
      Left = 16
      Top = 5
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 19
      Top = 27
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object Label3: TLabel
      Left = 11
      Top = 51
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = '# P'#243'liza:'
    end
    object lkpSGMS: TZetaKeyLookup_DevEx
      Left = 56
      Top = 3
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
    object zcbStatus: TZetaKeyCombo
      Left = 55
      Top = 25
      Width = 145
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 1
      ListaFija = lfStatusSGM
      ListaVariable = lvPuesto
      Offset = -1
      Opcional = False
      EsconderVacios = False
    end
    object btnRefrescar: TBitBtn
      Left = 368
      Top = 3
      Width = 89
      Height = 41
      Caption = 'Refrescar'
      TabOrder = 2
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      NumGlyphs = 2
    end
    object txtPoliza: TZetaEdit
      Left = 55
      Top = 48
      Width = 144
      Height = 21
      TabOrder = 3
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1271
    Height = 268
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object EP_ORDEN: TcxGridDBColumn
        Caption = 'Orden'
        DataBinding.FieldName = 'EP_ORDEN'
        Options.Grouping = False
        Width = 64
      end
      object PM_NUMERO: TcxGridDBColumn
        Caption = '# de P'#243'liza'
        DataBinding.FieldName = 'PM_NUMERO'
        Width = 64
      end
      object PV_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'PV_REFEREN'
        Options.Grouping = False
        Width = 64
      end
      object PM_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'PM_DESCRIP'
        Width = 64
      end
      object EP_CERTIFI: TcxGridDBColumn
        Caption = 'Certificado'
        DataBinding.FieldName = 'EP_CERTIFI'
        Options.Grouping = False
        Width = 64
      end
      object EP_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'EP_TIPO'
        Width = 64
      end
      object EP_DEPEND: TcxGridDBColumn
        Caption = 'Pariente'
        DataBinding.FieldName = 'EP_DEPEND'
        Options.Grouping = False
        Width = 64
      end
      object PA_RELACIO: TcxGridDBColumn
        Caption = 'Parentesco'
        DataBinding.FieldName = 'PA_RELACIO'
        Options.Grouping = False
        Width = 64
      end
      object EP_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'EP_STATUS'
      end
      object EP_FEC_INI: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'EP_FEC_INI'
        Options.Grouping = False
        Width = 64
      end
      object EP_FEC_FIN: TcxGridDBColumn
        Caption = 'Fin'
        DataBinding.FieldName = 'EP_FEC_FIN'
        Options.Grouping = False
        Width = 64
      end
      object EP_CAN_FEC: TcxGridDBColumn
        Caption = 'Cancelaci'#243'n'
        DataBinding.FieldName = 'EP_CAN_FEC'
        Options.Grouping = False
        Width = 64
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_DESCRIP'
        Options.Grouping = False
        Width = 64
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 504
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
