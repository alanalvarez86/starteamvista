inherited EditEmpDlg_DevEx: TEditEmpDlg_DevEx
  Left = 248
  Top = 201
  ActiveControl = Operacion
  Caption = 'Modificaci'#243'n de '#193'rea'
  ClientHeight = 168
  ClientWidth = 270
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 132
    Width = 270
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 106
      Top = 5
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 185
      Top = 5
      Cancel = True
      Caption = '&Salir'
      ModalResult = 0
      OnClick = SalirClick
    end
  end
  object Operacion: TcxRadioGroup [1]
    Left = 43
    Top = 8
    Caption = 'Operaci'#243'n'
    Properties.Items = <
      item
        Caption = 'Corregir Error de Captura'
        Value = '0'
      end
      item
        Caption = 'Generar Movimiento Nuevo'
        Value = '1'
      end>
    ItemIndex = 0
    TabOrder = 0
    Height = 103
    Width = 185
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670248
  end
end
