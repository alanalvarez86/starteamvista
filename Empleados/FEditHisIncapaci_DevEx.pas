unit FEditHisIncapaci_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, DBCtrls, StdCtrls,
  Mask, Db, Buttons, ExtCtrls, ZetaDBTextBox,
  ZetaKeyCombo,
  ZetaNumero, ZetaFecha, ComCtrls, ZetaSmartLists,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC;

type
  TEditHisIncapaci_DevEx = class(TBaseEdicion_DevEx)
    PageControl_DevEx: TcxPageControl;
    TabSubsidio_DevEx: TcxTabSheet;
    TabFechaIMSS_DevEx: TcxTabSheet;
    TabGenerales_DevEx: TcxTabSheet;
    LIN_FEC_INI: TLabel;
    IN_FEC_INI: TZetaDBFecha;
    LIN_DIAS: TLabel;
    IN_DIAS: TZetaDBNumero;
    LIN_FEC_FIN: TLabel;
    IN_FEC_FIN: TZetaDBTextBox;
    Label13: TLabel;
    IN_TIPO: TZetaDBKeyLookup_DevEx;
    Label11: TLabel;
    IN_NUMERO: TDBEdit;
    Label5: TLabel;
    IN_COMENTA: TDBEdit;
    Label10: TLabel;
    IN_MOTIVO: TZetaDBKeyCombo;
    Label1: TLabel;
    IN_FIN: TZetaDBKeyCombo;
    IN_TASA_IPLbl: TLabel;
    IN_TASA_IP: TZetaDBNumero;
    Label6: TLabel;
    IN_FEC_RH: TZetaDBFecha;
    UsuarioLbl: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    Label2: TLabel;
    IN_CAPTURA: TZetaDBTextBox;
    LblPago: TLabel;
    IN_DIASSUB: TZetaDBNumero;
    GBNominaPago: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    IN_NOMTIPO: TZetaDBKeyCombo;
    IN_NOMNUME: TZetaDBKeyLookup_DevEx;
    IN_NOMYEAR: TZetaDBNumero;
    Label3: TLabel;
    IN_SUA_INI: TZetaDBFecha;
    Label4: TLabel;
    IN_SUA_FIN: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure IN_FINChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
  private
    procedure SetFiltroPeriodo;
  protected
    procedure Connect;override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  EditHisIncapaci_DevEx: TEditHisIncapaci_DevEx;

implementation

uses DRecursos, dTablas, dSistema, dCatalogos, ZetaTipoEntidad,
     ZImprimeForma,ZetaCommonLists, ZetaCommonClasses, ZAccesosTress ;

{$R *.DFM}

procedure TEditHisIncapaci_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IN_TIPO.Filtro := dmRecursos.GetFiltroIncapacidad;
     TipoValorActivo1 := ZetacommonLists.stEmpleado;
     HelpContext:= H11512_Incapacidad;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_INCA;
     FirstControl := IN_FEC_INI;
     IN_TIPO.LookupDataset := dmTablas.cdsIncidencias;
     {$ifndef CAJAAHORRO}
     IN_NOMNUME.LookupDataset := dmCatalogos.cdsPeriodo;
     {$endif}
end;

procedure TEditHisIncapaci_DevEx.FormShow(Sender: TObject);
begin
     PageControl_DevEx.ActivePage := TabGenerales_DevEx;
     inherited;
     IN_FINChange( self );
     IN_NOMTIPO.ListaFija:=lfTipoPeriodo; //acl
end;

procedure TEditHisIncapaci_DevEx.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
          {$ifndef CAJAAHORRO}
     dmCatalogos.cdsPeriodo.Conectar;
     {$endif}

     with dmRecursos do
     begin
          cdsHisIncapaci.Conectar;
          DataSource.DataSet := cdsHisIncapaci;
     end;
     SetFiltroPeriodo;         //dmCatalogos.cdsPeriodo.Conectar; .. SetFiltro se encarga de Conectar     
end;

procedure TEditHisIncapaci_DevEx.IN_FINChange(Sender: TObject);
begin
     inherited;
     IN_TASA_IPLbl.Enabled := ( IN_FIN.Valor = Ord( fiPermanente ));
     IN_TASA_IP.Enabled    := IN_TASA_IPLbl.Enabled;
end;

procedure TEditHisIncapaci_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enIncapacidad, dmRecursos.cdsHisIncapaci );
end;

procedure TEditHisIncapaci_DevEx.SetFiltroPeriodo;
var
   iYear : Integer;
   TipoPer : eTipoPeriodo;
begin
     with DataSource.DataSet do
     begin
          iYear := FieldByName( 'IN_NOMYEAR' ).AsInteger;
          TipoPer := eTipoPeriodo( FieldByName( 'IN_NOMTIPO' ).AsInteger );
     end;
     {$ifndef CAJAAHORRO}
     with dmCatalogos do
     begin
          with cdsPeriodo do
          begin
               if ( IsEmpty ) or
                  ( ( iYear <> FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPer ) <> FieldByName( 'PE_TIPO' ).AsInteger ) ) then
               begin
                    GetDatosPeriodo( iYear, TipoPer );
                    // Para que se refresque el Control de Lookup:
                    with IN_NOMNUME do
                    begin
                         SetLlaveDescripcion( VACIO, VACIO );
                         Llave := DataSource.DataSet.FieldByName( 'IN_NOMNUME' ).AsString;
                    end;
               end;
          end;
     end;
     {$endif}
end;

procedure TEditHisIncapaci_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if Assigned( Field ) then
     begin
          with Field do
          begin
               if ( FieldName = 'IN_NOMYEAR' ) or ( FieldName = 'IN_NOMTIPO' ) then
                  SetFiltroPeriodo;
          end;
     end;
end;

procedure TEditHisIncapaci_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if ( State = dsBrowse ) then
             SetFiltroPeriodo;
     end;
end;

end.







