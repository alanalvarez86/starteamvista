unit FKardexCertificaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TKardexCertificaciones_DevEx = class(TBaseGridLectura_DevEx)
    KI_FEC_CER: TcxGridDBColumn;
    CI_CODIGO: TcxGridDBColumn;
    CI_NOMBRE: TcxGridDBColumn;
    KI_FOLIO: TcxGridDBColumn;
    KI_APROBO: TcxGridDBColumn;
    VENCIMIENTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DoLookup; override;
  protected
    { Protected declarations}
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  end;

var
  KardexCertificaciones_DevEx: TKardexCertificaciones_DevEx;

implementation

uses ZetaCommonLists,
     ZetaCommonClasses,
     dRecursos,
     ZetaBuscador_DevEx,
     dCatalogos;

{$R *.dfm}

procedure TKardexCertificaciones_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H_Kardex_Certificaciones;
     CanLookup := True;
     TipoValorActivo1 := stEmpleado;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
end;

procedure TKardexCertificaciones_DevEx.Connect;
begin
     dmCatalogos.cdsCertificaciones.Conectar;
     with dmRecursos do
     begin
          cdsKarCertificaciones.Conectar;
          DataSource.DataSet := cdsKarCertificaciones;
     end;
end;

procedure TKardexCertificaciones_DevEx.Refresh;
begin
     dmRecursos.cdsKarCertificaciones.Refrescar;
end;

procedure TKardexCertificaciones_DevEx.Agregar;
begin
     dmRecursos.cdsKarCertificaciones.Agregar;
end;

procedure TKardexCertificaciones_DevEx.Borrar;
begin
     dmRecursos.cdsKarCertificaciones.Borrar;
end;

procedure TKardexCertificaciones_DevEx.Modificar;
begin
     dmRecursos.cdsKarCertificaciones.Modificar;
end;


procedure TKardexCertificaciones_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo',Self.Caption,'CI_CODIGO', dmRecursos.cdsKarCertificaciones );
end;

procedure TKardexCertificaciones_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
   //CreaColumaSumatoria('VENCIMIENTO',skCount);
   CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;

end;

end.
