unit FEditCursoProg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, 
  Mask, ZetaNumero, ZetaSmartLists, ZetaFecha, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, 
  cxControls, dxSkinsdxBarPainter, ZetaKeyLookup_DevEx,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditCurProg_DevEx = class(TBaseEdicion_DevEx)
    lblCurso: TLabel;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    EP_OPCIONA: TDBCheckBox;
    EP_GLOBAL: TDBCheckBox;
    GBDias: TGroupBox;
    lblDias: TLabel;
    EP_DIAS: TZetaDBNumero;
    lblAntig: TLabel;
    GBFecha: TGroupBox;
    lblzFecha: TLabel;
    zFecha: TZetaDBFecha;
    RbDias: TRadioButton;
    RbFecha: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure RbDiasClick(Sender: TObject);
    procedure RbFechaClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure SetEditarSoloActivos;
  protected
    procedure Connect; override;
    procedure EscribirCambios; override;
  private
    { Private declarations }
    procedure HabilitaRadioButtons(lHabilita: Boolean );
    procedure ModificaModo(const Value: TDatasetState);
  public
    { Public declarations }
  end;

var
  EditCurProg_DevEx: TEditCurProg_DevEx;

implementation

uses dCatalogos, dRecursos, ZAccesosTress, ZetaCommonClasses;

{$R *.DFM}

procedure TEditCurProg_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_EXP_CURSOS_PROGRAMADOS;
     HelpContext:=  H10139_Cursos_programados_individualmente;
     FirstControl := CU_CODIGO;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditCurProg_DevEx.Connect;
begin
     dmCatalogos.cdsCursos.Conectar;
     with dmRecursos do
     begin
          cdsCursosProg.Conectar;
          DataSource.DataSet:= cdsCursosProg;
     end;
end;

procedure TEditCurProg_DevEx.RbDiasClick(Sender: TObject);
begin
     inherited;
     RbFecha.Checked:= False;
     HabilitaRadioButtons( True );
     ModificaModo(dsEdit);
end;

procedure TEditCurProg_DevEx.RbFechaClick(Sender: TObject);
begin
     inherited;
     RbDias.Checked:= False;
     HabilitaRadioButtons( False );
     ModificaModo(dsEdit);
end;


procedure TEditCurProg_DevEx.HabilitaRadioButtons(lHabilita: Boolean);
begin
     lblAntig.Enabled:= lHabilita;
     EP_DIAS.Enabled:= lHabilita;
     lblDias.Enabled:= lHabilita;

     lblzFecha.Enabled:= NOT lHabilita;
     zFecha.Enabled:= NOT lHabilita;
end;

procedure TEditCurProg_DevEx.EscribirCambios;
begin
     with dmRecursos.cdsCursosProg do
     begin
          Edit;
          if RbDias.Checked = True then
              FieldByName('EP_PORDIAS').AsString := K_GLOBAL_SI
          else
              FieldByName('EP_PORDIAS').AsString := K_GLOBAL_NO;
     end;
     inherited;     
end;

procedure TEditCurProg_DevEx.ModificaModo(const Value: TDatasetState);
begin
     if ( Modo <> Value ) then
     begin
          Modo := Value;
          HabilitaControles;
     end;
end;

procedure TEditCurProg_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with DataSource.DataSet do
     begin
          if ( (State <> dsEdit) and (State <> dsInsert)) then
          begin
               if dmRecursos.cdsCursosProg.FieldByName('EP_PORDIAS').AsString = K_GLOBAL_SI then
                  RbDias.Checked := True
               else
                  RbFecha.Checked := True;
                Modo := dsBrowse;
          end;
     end;
end;

procedure TEditCurProg_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     if dmRecursos.cdsCursosProg.FieldByName('EP_PORDIAS').AsString = K_GLOBAL_SI then
        RbDias.Checked := True
     else
        RbFecha.Checked := True; 
end;

procedure TEditCurProg_DevEx.SetEditarSoloActivos;
begin
     CU_CODIGO.EditarSoloActivos := TRUE;
end;

end.
