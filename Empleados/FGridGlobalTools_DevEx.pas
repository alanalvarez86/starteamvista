unit FGridGlobalTools_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ZetaFecha, Db, Grids,
  DBGrids, ZetaDBGrid, DBCtrls, Buttons, ExtCtrls, ZetaSmartLists,
  ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  ZetaKeyLookup_DevEx, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TGridGlobalTools_DevEx = class(TBaseGridEdicion_DevEx)
    PanelTool: TPanel;
    KT_FEC_INI: TZetaFecha;
    FechaLbl: TLabel;
    HerramientaLbl: TLabel;
    TO_CODIGO: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
    procedure BuscaTalla;
    procedure AsignaDatosTools;
    procedure ValidaDatosTools;
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  GridGlobalTools_DevEx: TGridGlobalTools_DevEx;

implementation

uses dRecursos, dCliente, dCatalogos, dTablas, ZetaCommonClasses, ZetaCommonTools,
     FAutoClasses, ZetaBuscaEmpleado_DevEx;

{$R *.DFM}

procedure TGridGlobalTools_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H11614_Entrega_global;
     KT_FEC_INI.Valor := dmCliente.FechaDefault;
     TO_CODIGO.LookupDataset := dmCatalogos.cdsTools;
end;

procedure TGridGlobalTools_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     self.ActiveControl:= TO_CODIGO;
     with dmCatalogos.cdsTools do
          TO_CODIGO.SetLlaveDescripcion( FieldByName( 'TO_CODIGO' ).AsString,
                                         FieldByName( 'TO_DESCRIP' ).AsString );  // Evita que se Haga el DoLookup del Control
end;

procedure TGridGlobalTools_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmTablas do
     begin
          cdsMotTool.Conectar;
          cdsTallas.Conectar;
     end;
     dmCatalogos.cdsTools.Conectar;
     with dmRecursos do
     begin
          cdsGridGlobalTools.Refrescar;
          DataSource.DataSet:= cdsGridGlobalTools;
     end;
end;

procedure TGridGlobalTools_DevEx.Buscar;
begin
     inherited;
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
          else if ( FieldName = 'KT_TALLA' ) then
             BuscaTalla;
     end;
end;

procedure TGridGlobalTools_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmRecursos.cdsGridGlobalTools do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridGlobalTools_DevEx.BuscaTalla;
var
   sTalla, sTallaDesc: String;
begin
     with dmRecursos.cdsGridGlobalTools do
     begin
          sTalla := FieldByName( 'KT_TALLA' ).AsString;
          if dmTablas.cdsTallas.Search_DevEx( '', sTalla, sTallaDesc ) then
          begin
               if ( sTalla <> FieldByName( 'KT_TALLA' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( 'KT_TALLA' ).AsString := sTalla;
               end;
          end;
     end;
end;

procedure TGridGlobalTools_DevEx.ValidaDatosTools;
begin
     if StrVacio( TO_CODIGO.Llave ) then
     begin
          ActiveControl := TO_CODIGO;
          DataBaseError( 'No Se Ha Especificado la Herramienta' );
     end;
end;

procedure TGridGlobalTools_DevEx.AsignaDatosTools;
begin
     with dmRecursos.cdsGridGlobalTools do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          DisableControls;
          try
             First;
             while not EOF do
             begin
                  Edit;
                  FieldByName( 'TO_CODIGO' ).AsString := TO_CODIGO.Llave;
                  FieldByName( 'KT_FEC_INI' ).AsDateTime := KT_FEC_INI.Valor;
                  FieldByName( 'KT_ACTIVO' ).AsString := K_GLOBAL_SI;
                  FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                  Next;
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TGridGlobalTools_DevEx.OKClick(Sender: TObject);
begin
     if dmCliente.ModuloAutorizado( okHerramientas ) then
        try
           ValidaDatosTools;
           AsignaDatosTools;

           inherited;

        except
           self.ModalResult:= mrNone;
           raise;
        end;
end;

procedure TGridGlobalTools_DevEx.KeyPress(var Key: Char);
begin
     inherited;
     if GridEnfocado then
     begin
          {$IFNDEF TRESS_DELPHIXE5_UP}
          if ( ( Key <> Chr(9) ) and ( Key <> #0 ) and ( Key in [ 'a'..'z' ] ) ) then
          {$ELSE}
          if ( ( Key <> Chr(9) ) and ( Key <> #0 ) and ( CharInSet (Key, [ 'a'..'z' ]) ) ) then
          {$ENDIF}
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'KT_TALLA' ) then
                  Key := Chr( Ord( Key ) - 32 );
          end;
     end;
end;

procedure TGridGlobalTools_DevEx.OK_DevExClick(Sender: TObject);
begin
if dmCliente.ModuloAutorizado( okHerramientas ) then
        try
           ValidaDatosTools;
           AsignaDatosTools;

           inherited;

        except
           self.ModalResult:= mrNone;
           raise;
        end;

end;

end.
