inherited EmpExperiencia: TEmpExperiencia
  Left = 274
  Top = 270
  Caption = 'Experiencia'
  ClientHeight = 336
  ClientWidth = 618
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 618
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 292
      inherited textoValorActivo2: TLabel
        Width = 286
      end
    end
  end
  object GroupBox1: TGroupBox [1]
    Left = 8
    Top = 24
    Width = 609
    Height = 185
    Caption = ' Experiencia Acad'#233'mica '
    TabOrder = 1
    object LEstudios: TLabel
      Left = 67
      Top = 23
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grado de Estudios:'
    end
    object ZCB_ESTUDIO: TZetaTextBox
      Left = 162
      Top = 21
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'ZCB_ESTUDIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object CB_EST_HOR: TZetaDBTextBox
      Left = 162
      Top = 151
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'CB_EST_HOR'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_EST_HOR'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_EST_HORlbl: TLabel
      Left = 75
      Top = 153
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Carrera y Horario:'
    end
    object Label1: TLabel
      Left = 57
      Top = 134
      Width = 100
      Height = 13
      Caption = 'Estudia Actualmente:'
    end
    object Label3: TLabel
      Left = 106
      Top = 42
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Instituci'#243'n:'
    end
    object CB_ESCUELA: TZetaDBTextBox
      Left = 162
      Top = 40
      Width = 419
      Height = 17
      AutoSize = False
      Caption = 'CB_ESCUELA'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_ESCUELA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 67
      Top = 60
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Instituci'#243'n:'
    end
    object Label5: TLabel
      Left = 9
      Top = 97
      Width = 148
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Documento Probatorio:'
    end
    object CB_YTITULO: TZetaDBTextBox
      Left = 162
      Top = 113
      Width = 79
      Height = 18
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CB_YTITULO'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_YTITULO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label6: TLabel
      Left = 23
      Top = 116
      Width = 134
      Height = 13
      Alignment = taRightJustify
      Caption = 'A'#241'o de Emisi'#243'n Documento:'
    end
    object LCarrera: TLabel
      Left = 67
      Top = 78
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Carrera Terminada:'
    end
    object CB_CARRERA: TZetaDBTextBox
      Left = 162
      Top = 76
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'CB_CARRERA'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_CARRERA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ZCB_TESCUEL: TZetaTextBox
      Left = 162
      Top = 58
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'ZCB_TESCUEL'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object ZCB_TITULO: TZetaTextBox
      Left = 162
      Top = 95
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'ZCB_TITULO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object CB_EST_HOY: TDBCheckBox
      Left = 163
      Top = 132
      Width = 21
      Height = 17
      DataField = 'CB_EST_HOY'
      DataSource = DataSource
      ReadOnly = True
      TabOrder = 0
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 8
    Top = 214
    Width = 609
    Height = 113
    Caption = ' Habilidades '
    TabOrder = 2
    object Label2: TLabel
      Left = 70
      Top = 23
      Width = 88
      Height = 13
      Caption = 'Habla Otro Idioma:'
    end
    object CB_IDIOMA: TZetaDBTextBox
      Left = 164
      Top = 42
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'CB_IDIOMA'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_IDIOMA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_IDIOMAlbl: TLabel
      Left = 75
      Top = 43
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Idioma y Dominio:'
    end
    object LMaquinas: TLabel
      Left = 48
      Top = 63
      Width = 110
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#225'quinas que Conoce:'
    end
    object CB_MAQUINA: TZetaDBTextBox
      Left = 163
      Top = 61
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'CB_MAQUINA'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_MAQUINA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CB_EXPERIE: TZetaDBTextBox
      Left = 163
      Top = 80
      Width = 420
      Height = 17
      AutoSize = False
      Caption = 'CB_EXPERIE'
      ShowAccelChar = False
      Brush.Color = clSilver
      Border = False
      DataField = 'CB_EXPERIE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LExperiencia: TLabel
      Left = 100
      Top = 82
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Experiencia:'
    end
    object CB_HABLA: TDBCheckBox
      Left = 164
      Top = 23
      Width = 17
      Height = 15
      DataField = 'CB_HABLA'
      DataSource = DataSource
      ReadOnly = True
      TabOrder = 0
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
  end
  inherited DataSource: TDataSource
    Left = 224
    Top = 12
  end
  object DataSource1: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 224
    Top = 12
  end
end
