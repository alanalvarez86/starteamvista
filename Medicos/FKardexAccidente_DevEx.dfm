inherited KardexAccidente_DevEx: TKardexAccidente_DevEx
  Left = 38
  Top = 152
  Caption = 'Accidentes'
  ClientHeight = 308
  ClientWidth = 746
  ExplicitWidth = 746
  ExplicitHeight = 308
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 746
    ExplicitWidth = 746
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 487
      ExplicitWidth = 487
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 481
        ExplicitLeft = 677
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 746
    Height = 289
    ParentFont = False
    ExplicitWidth = 746
    ExplicitHeight = 289
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object AX_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AX_FECHA'
        Options.Grouping = False
      end
      object AX_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'AX_HORA'
        Options.Grouping = False
      end
      object AX_TIP_LES: TcxGridDBColumn
        Caption = 'Circunstancias'
        DataBinding.FieldName = 'AX_TIP_LES'
      end
      object AX_NUM_INC: TcxGridDBColumn
        Caption = '# Incapacidad'
        DataBinding.FieldName = 'AX_NUM_INC'
        Options.Grouping = False
      end
      object TB_DESCRIP: TcxGridDBColumn
        Caption = 'Causa de Accidente'
        DataBinding.FieldName = 'TB_DESCRIP'
        Options.Grouping = False
      end
      object TB_DESCRIP2: TcxGridDBColumn
        Caption = 'Tipo de Accidente'
        DataBinding.FieldName = 'TB_DESCRIP2'
      end
      object AX_RIESGO: TcxGridDBColumn
        Caption = 'Riesgo'
        DataBinding.FieldName = 'AX_RIESGO'
        Options.Grouping = False
      end
      object AX_INCAPA: TcxGridDBColumn
        Caption = #191'Incapacitado?'
        DataBinding.FieldName = 'AX_INCAPA'
        Options.Grouping = False
      end
      object AX_DAN_MAT: TcxGridDBColumn
        Caption = #191'Da'#241'o Material?'
        DataBinding.FieldName = 'AX_DAN_MAT'
        Options.Grouping = False
      end
      object US_NOMBRE: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_NOMBRE'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
