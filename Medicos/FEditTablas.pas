unit FEditTablas;

interface

uses
  //ZBaseEdicion
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaKeyLookup, Mask, Db,
  ExtCtrls, Buttons, ZetaKeyCombo, ZetaEdit, ZetaClientDataSet, ZetaNumero;

type
  TEditTablas = class(TObject)
  //TEditTablas = class(TBaseEdicion)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    Label6: TLabel;
    TB_TIPO: TZetaDBKeyCombo;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    fZetalookupDataset : TZetaLookupDataSet;
  protected
//    procedure Connect;override;
   // procedure DoLookup; override;
    procedure AfterCreate; virtual;
    property ZetaLookupDataset : TZetaLookupDataSet read fZetaLookupDataset write fZetaLookupDataset;
  public
  end;

type
    TTOEditTipoAccident = class ( TEditTablas )
      protected
      Procedure AfterCreate;override;
    end;

type
    TTOEditTipoCausaAcc = class ( TEditTablas )
      protected
      Procedure AfterCreate;override;
    end;

    TTOEditMotivoAcc = class ( TEditTablas )
      protected
      Procedure AfterCreate;override;
    end;

const
     K_MAX_LENGTH_DESCRIP = 30;

var
   EditTipoAccident : TTOEditTipoAccident;
   EditTipoCausaAcc : TTOEditTipoCausaAcc;
   EditMotivoAcc:  TTOEditMotivoAcc;

implementation

uses DMedico,
     ZetaCommonClasses,
     ZAccesosTress,
     //ZetaBuscador,
     ZetaCommonLists,
     FAyudaContexto;

{$R *.DFM}

procedure TEditTablas.FormCreate(Sender: TObject);
begin
     inherited;
     //FirstControl := TB_CODIGO;
     AfterCreate;
     TB_ELEMENT.MaxLength := K_MAX_LENGTH_DESCRIP;
     TB_INGLES.MaxLength := K_MAX_LENGTH_DESCRIP;
     TB_TEXTO.MaxLength := K_MAX_LENGTH_DESCRIP;
end;

procedure TEditTablas.AfterCreate;
begin
end;

{procedure TEditTablas.Connect;
begin
    // Caption := ZetaLookupDataset.LookupName;
   //  DataSource.DataSet := ZetaLookupDataset;
end;

procedure TEditTablas.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', ZetaLookupdataset );
end;}

procedure TTOEditTipoAccident.AfterCreate;
begin
     inherited;
     ZetaLookupDataset := dmMedico.cdsTAccidente;
     TB_Tipo.ListaFija:= lfTipoAccidente;
    // HelpContext := H00102_Catalogo_de_Tipos_de_Accidente;
    // IndexDerechos := ZAccesosTress.D_CAT_TIPO_ACC;
end;

procedure TTOEditTipoCausaAcc.AfterCreate;
begin
     inherited;
     ZetaLookupDataset := dmMedico.cdsCausaAcc;
     TB_Tipo.ListaFija:= lfMotivoAcc;
    // HelpContext := H00109_Catalogo_de_Causas_de_accidente;
   //  IndexDerechos := ZAccesosTress.D_CAT_MOT_ACC;
end;

{ TTOEditMotivoAcc }

procedure TTOEditMotivoAcc.AfterCreate;
begin
     inherited;
     ZetaLookupDataset := dmMedico.cdsMotivoAcc;
     TB_Tipo.ListaFija:= lfMotivoAcc;
    // HelpContext := H00103_Catalogo_de_motivos_de_accidente;
    // IndexDerechos := ZAccesosTress.D_CAT_MOT_ACC;
end;

end.
