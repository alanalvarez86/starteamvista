unit Ftablas2_DevEx;

interface
uses ZBaseTablasConsulta_DevEx;



type
 TTOTConsulta_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

//MA:Clase agregada por mi
type
    TTOTEstudio_DevEx = class (TTablaLookup)
    protected
    {Protected declarations}
    procedure AfterCreate;override;
  end;

implementation

uses DMedico, ZetaDBGrid, FAyudaContexto, ZBaseGridLectura_DevEx, cxCustomData;

procedure Agrega_Tipo(OGrid : TZetaDBGrid);
begin
     with OGrid.Columns.Add do
      begin
           FieldName:='TB_TIPO';
           Title.Caption:='Tipo';
           Width:=90;
           Index:=2;
      end;
end;
{TTOTConsulta}
procedure TTOTConsulta_DevEx.AfterCreate;
begin
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited AfterCreate;
     LookupDataset := dmMedico.cdsTConsulta;
     HelpContext := H00007_Catalogo_de_Tipos_de_Consulta;

     //Agrega_Tipo(dbGrid);
end;

//MA:Implementacion hecha por mi
{TTOTEstudio}
procedure TTOTEstudio_DevEx.AfterCreate;
begin
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited AfterCreate;
     LookupDataset := dmMedico.cdsTEstudio;
     HelpContext := H00008_Catalogo_de_tipos_de_Estudio;
     //Agrega_Tipo(dbGrid);
end;

end.
 