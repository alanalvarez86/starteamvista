inherited KardexEmbarazo_DevEx: TKardexEmbarazo_DevEx
  Left = 200
  Top = 153
  Caption = 'Embarazos'
  ClientHeight = 295
  ClientWidth = 519
  ExplicitWidth = 519
  ExplicitHeight = 295
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 519
    ExplicitWidth = 519
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 260
      ExplicitWidth = 260
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 254
        ExplicitLeft = 450
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 519
    Height = 276
    ParentFont = False
    ExplicitWidth = 519
    ExplicitHeight = 276
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object EM_FEC_UM: TcxGridDBColumn
        Caption = 'F. U. M.'
        DataBinding.FieldName = 'EM_FEC_UM'
      end
      object EM_FEC_PP: TcxGridDBColumn
        Caption = 'F. P. P.'
        DataBinding.FieldName = 'EM_FEC_PP'
      end
      object EM_PRENAT: TcxGridDBColumn
        Caption = 'Inicio Incapacidad'
        DataBinding.FieldName = 'EM_PRENAT'
      end
      object EM_POSNAT: TcxGridDBColumn
        Caption = 'Fin Incapacidad'
        DataBinding.FieldName = 'EM_POSNAT'
      end
      object EM_INC_INI: TcxGridDBColumn
        Caption = 'Inicio del Riesgo'
        DataBinding.FieldName = 'EM_INC_INI'
      end
      object EM_INC_FIN: TcxGridDBColumn
        Caption = 'Fin del Riesgo'
        DataBinding.FieldName = 'EM_INC_FIN'
      end
      object EM_COMENTA: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'EM_COMENTA'
      end
      object US_NOMBRE: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_NOMBRE'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
