unit FExpediente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ImgList, ZetaDBTextBox, StdCtrls, ComCtrls, ToolWin,
  Grids, DBGrids, Db, ExtCtrls, Menus, ZetaDBGrid, ZetaMessages, EditorDResumen;


type
  ePintaRtf = ( epConsultas, epExpediente );
  TExpediente = class(TBaseConsulta)
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    gbExpediente: TGroupBox;
    gbConsulta: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    ToolBar1: TToolBar;
    btnExpediente: TToolButton;
    btnConsulta: TToolButton;
    btnEmbarazo: TToolButton;
    btnAccidente: TToolButton;
    btnMedicinaEntr: TToolButton;
    ImageList1: TImageList;
    tbImpresion: TToolButton;
    PopupMenu1: TPopupMenu;
    popExpediente: TMenuItem;
    popConsultas: TMenuItem;
    ToolButton1: TToolButton;
    zdbGrid: TZetaDBGrid;
    dsKardexConsultas: TDataSource;
    Splitter6: TSplitter;
    Splitter7: TSplitter;
    reConsulta: TRichEdit;
    reExpediente: TRichEdit;
    procedure FormCreate(Sender: TObject);
    procedure dsKardexConsultasDataChange(Sender: TObject; Field: TField);
    procedure btnExpedienteClick(Sender: TObject);
    procedure Splitter7Moved(Sender: TObject);
    procedure Splitter6Moved(Sender: TObject);
    procedure reExpedienteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure reConsultaMouseUp(Sender: TObject;Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure popExpedienteClick(Sender: TObject);
    procedure popConsultasClick(Sender: TObject);
    procedure tbImpresionClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    FInicial : TDateTime;
    FIntervalo : TDateTime;
    FEditor : TEditorCliente;
    procedure PintaRTFS(const eRtf: ePintaRtf);
    procedure NOExpediente;
    procedure NOConsulta;
    procedure PintaConsulta;
    procedure PintaExpediente;
    procedure SinDatos( const sTipoRtf: String; oRtf: TRichEdit );
    procedure HabilitaControles(const lHabilita: Boolean);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  Expediente: TExpediente;

implementation

uses
//     ZetaBuscador,
     DMedico,ZetaCommonLists, ZAccesosMgr, ZetaCommonClasses,FAyudaContexto,
     ZAccesosTress, ZetaDialogo, FEditKardexConsulta, FTressShell;

{$R *.DFM}

procedure TExpediente.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H00002_Expediente;
     FInicial := Now;
     {if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CONSULTA ) then
     begin
          gbConsulta.Align := alClient;
          splitter7.Align := alLeft;
          gbExpediente.Align := alLeft;
          FIntervalo := EncodeTime(0,0,0,Windows.GetDoubleClickTime);
          FEditor := TEditorCliente.Create;
     end
     else
     begin
          gbConsulta.Visible := False;
          splitter7.Align := alRight;
          gbExpediente.Align := alClient;
     end;}
     popConsultas.Enabled :=  Revisa( D_EMP_KARDEX_CONSULTAS );
     popExpediente.Enabled := Revisa( D_EMP_SERV_EXPEDIENTE );
     btnConsulta.Caption := 'Consultas';
     btnMedicinaEntr.Caption := 'Medicinas';
     btnAccidente.Caption := 'Accidentes  ';
end;

procedure TExpediente.Connect;
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CONSULTA ) then
     begin
          gbConsulta.Align := alClient;
          splitter7.Align := alLeft;
          gbExpediente.Align := alLeft;
          FIntervalo := EncodeTime(0,0,0,Windows.GetDoubleClickTime);
          FEditor := TEditorCliente.Create;
     end
     else
     begin
          gbConsulta.Visible := False;
          splitter7.Align := alRight;
          gbExpediente.Align := alClient;
     end;
     with dmMedico do
     begin
          cdsExpediente.Conectar;
          DataSource.DataSet:= cdsExpediente;
          dsKardexConsultas.DataSet:=cdsKardexConsultas;
     end;
end;

procedure TExpediente.Refresh;
begin
     With dmMedico do
          cdsExpediente.Refrescar;
end;

procedure TExpediente.Agregar;
begin
     dmMedico.cdsExpediente.Agregar;
end;

function TExpediente.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     {if Result then
        Result := dmMedico.CheckAuto( sMensaje );}
end;

procedure TExpediente.Borrar;
begin
     dmMedico.cdsExpediente.Borrar;
end;

procedure TExpediente.Modificar;
begin
     dmMedico.cdsExpediente.Modificar;
end;

procedure TExpediente.dsKardexConsultasDataChange(Sender: TObject;
  Field: TField);
begin
     with dmMedico.cdsKardexConsultas do
     begin
          if not( State IN [ dsEdit, dsInsert ] ) then
             PintaRTFs( epConsultas );
     end;
end;

procedure TExpediente.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dmMedico do
     begin
          if not( cdsExpediente.State IN [ dsEdit, dsInsert ] ) then
             PintaRTFs( epExpediente )
     end;
end;

procedure TExpediente.btnExpedienteClick(Sender: TObject);
begin
      inherited;
      With dmMedico do
      begin
           with TressShell do
           begin
                if ExisteExpediente then
                     _E_Modificar.Execute
                else
                     _E_Agregar.Execute;
           end;
      end;//With
end;


procedure TExpediente.WMExaminar(var Message: TMessage);
begin
     //MA: Linea para que al dar click sobre el grid de consultas no aparezca sin datos al agregar una cons.nueva
     if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CAMBIO ) then
     begin
          With dmMedico.cdsKardexConsultas do
          begin
               if ( FieldByName('EX_CODIGO').AsInteger <> 0 ) then
                   Modificar
               else
                   Agregar;
          end;
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Consultas', 0 );
end;

procedure TExpediente.Splitter7Moved(Sender: TObject);
begin
     reExpediente.Repaint;
     reConsulta.Repaint;
end;

procedure TExpediente.Splitter6Moved(Sender: TObject);
begin
     reExpediente.Repaint;
     reConsulta.Repaint;
end;

procedure TExpediente.reExpedienteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   dAhora : TDateTime;
begin
  inherited;
       dAhora := Now;
       if ( ( dAhora - FInicial ) <= FIntervalo ) then
       begin
            if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_CAMBIO ) then
            begin
                 With dmMedico do
                 begin
                      if ExisteExpediente then
                         cdsExpediente.Modificar;
                 end;
            end
            else
                ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Expediente', 0 );
       end;
       Finicial := dAhora;
end;



procedure TExpediente.reConsultaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   dAhora : TDateTime;
begin
  inherited;
     dAhora := Now;
     if ( ( dAhora - FInicial ) <= FIntervalo ) then
     begin
          if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CAMBIO ) then
          begin
               With dmMedico.cdsKardexConsultas do
               begin
                    if dmMedico.ExisteExpediente then
                    begin
                       //MA: Linea para que al dar click sobre el grid de consultas no aparezca sin datos al agregar una cons.nueva
                         if ( FieldByName('EX_CODIGO').AsInteger <> 0 ) then
                             Modificar
                         else
                             Agregar;
                    end;
               end;
          end
          else
              ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Consultas', 0 );
     end;
     Finicial := dAhora;
end;

procedure TExpediente.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil( FEditor );
end;

procedure TExpediente.HabilitaControles( const lHabilita: Boolean );
begin
     zdbGrid.Enabled := lHabilita;
     with TressShell do
     begin
          btnConsulta.Enabled :=  _R_Consulta.Enabled  and lHabilita;
          btnEmbarazo.Enabled := _R_Embarazo.Enabled and lHabilita;
          btnAccidente.Enabled := _R_Accidente.Enabled and lHabilita;
          btnMedicinaEntr.Enabled := _R_Medicinas.Enabled and lHabilita;
     end;
end;

procedure TExpediente.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
   tbImpresion.Down:=False;
end;


procedure TExpediente.PintaRTFS(const eRtf: ePintaRtf);
begin
     Case eRtf of
          epConsultas :PintaConsulta;
          epExpediente:PintaExpediente;
     end;
end;

procedure TExpediente.PintaExpediente;
 var lTieneExpediente: Boolean;
begin
     lTieneExpediente := dmMedico.ExisteExpediente;

     if lTieneExpediente then
        dmMedico.CreaResumenExpediente(reExpediente)
     else
         NOExpediente;

     HabilitaControles( lTieneExpediente );
end;

procedure TExpediente.PintaConsulta;
begin
     with dmMedico do
     begin
          if ExisteConsulta then
             CreaResumenConsulta(reConsulta)
          else
              NOConsulta;
     end;
end;

procedure TExpediente.SinDatos( const sTipoRtf: String; oRtf: TRichEdit  );
begin
     with oRtf do
     begin
          Lines.BeginUpdate;
          try
             Clear;
             SelAttributes.Color := clRed;
             Paragraph.Alignment:=taCenter;
             SelAttributes.Style:=[fsUnderline,fsBold];
             Lines.Add( Format( '* El Empleado No Tiene %s *', [ sTipoRtf ] ) );
         finally
             Lines.EndUpdate;
         end;
     end;
end;

procedure TExpediente.NOConsulta;
begin
     SinDatos( 'Consultas', reConsulta  );
end;

procedure TExpediente.NOExpediente;
begin
     SinDatos( 'Expediente', reExpediente );
end;

procedure TExpediente.popExpedienteClick(Sender: TObject);
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          reExpediente.Print('Imprimiendo Expediente');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir El Expediente', 0 );
end;

procedure TExpediente.popConsultasClick(Sender: TObject);
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          reConsulta.Print('Imprimiendo Consultas');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir Consultas', 0 );
end;

procedure TExpediente.tbImpresionClick(Sender: TObject);
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          reExpediente.Print('Imprimiendo Expediente');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir El Expediente', 0 );     
end;


end.
