unit FCamposMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DB, DBCtrls, comctrls,
     ZetaNumero,
     ZetaEdit,
     ZetaFecha,
     ZetacommonLists;

const
     K_TOPE_VERTICAL = 17;
type
  //eCampoTipo = ( cdtTexto, cdtNumero, cdtBooleano, cdtBoolStr );
  //eMostrar = ( arSiempre, arNunca, arVacio, arLleno );
  TClasificacion = class( TObject )
  private
    { Private declarations }
    FCodigo: String;
    FNombre: String;
    FPosicion: Word;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Codigo: String read FCodigo write FCodigo;
    property Nombre: String read FNombre write FNombre;
    property Posicion: Word read FPosicion write FPosicion;
    function Comparar(Value: TClasificacion): Integer;
  end;
  TCampo = class( TObject )
  private
    { Private declarations }
    FTipo: eExCampoTipo;
    FLetrero: String;
    FCampo: String;
    FDefault: String;
    FPosicion: Word;
    FClasificacion: String;
    FIndice: Integer;
    FMostrar: eExMostrar;
    function GetNombre: String;
    function GetCapturar: Boolean;
    function GetCampoTexto: String;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Clasificacion: String read FClasificacion write FClasificacion;
    property Campo: String read FCampo write FCampo;
    property CampoTexto: String read GetCampoTexto;
    property Capturar: Boolean read GetCapturar;
    property Indice: Integer read FIndice write FIndice;
    property Letrero: String read FLetrero write FLetrero;
    property Nombre: String read GetNombre;
    property Posicion: Word read FPosicion write FPosicion;
    property Mostrar: eExMostrar read FMostrar write FMostrar;
    property Tipo: eExCampoTipo read FTipo write FTipo;
    property ValorDefault: String read FDefault write FDefault;
    procedure Inicializa;
    function Comparar(Value: TCampo): Integer;
    function TextoDisponible: String;
    function TextoEscogido: String;
  end;
  TListaClasificaciones = class( TObject )
  private
    { Private declarations }
    FClasificaciones: TList;
    function GetClasificacion(Index: Integer): TClasificacion;
    procedure Delete(const Index: Integer);
    procedure Ordena;
    function EsIgual( Grupo: TClasificacion; const sCodigo,sNombre: String ): Boolean;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Clasificacion[ Index: Integer ]: TClasificacion read GetClasificacion;
    function Add( const sCodigo, sNombre: String; const iPosicion: Word ): TClasificacion;
    function Count: Integer;
    //function Repetido(const sCodigo, sNombre: String):Boolean;overload;
    function Repetido(const sCodigo, sNombre: String; Original: TClasificacion = NIL ):Boolean;
    procedure Assign(Source: TListaClasificaciones);
    procedure Borrar(const sCodigo: String);
    procedure Clear;
    procedure Cargar(Lista: TStrings);
  end;
  TListaCampos = class( TObject )
  private
    { Private declarations }
    FCampos: TList;
    FDatasource: TDatasource;
    FForma: TForm;
    FPaginas: TPageControl;
    FRedraw: Boolean;
    //function CuantosHay(const eTipo: eCampoTipo): Integer;
    function GetCampo(Index: Integer): TCampo;
    function CuantosHayEnGrupo(const sCodigo: String): Integer;
    procedure Delete(const Index: Integer);
    procedure Ordena;
    procedure Cargar(Lista: TStrings; const sClasificacion: String );
    procedure ConstruyeGrupo( oGrupo: TClasificacion; const iCuantos: Integer; Padre: TWinControl );
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Campo[ Index: Integer ]: TCampo read GetCampo;
    property Datasource: TDatasource read FDatasource write FDatasource;
    property Forma: TForm read FForma write FForma;
    property Paginas: TPageControl read FPaginas write FPaginas;
    property Redraw: Boolean read FRedraw;
    function Count: Integer;
    procedure Add( const eTipo: eExCampoTipo; const sLetrero, sCampo, sDefault, sClasificacion: String; const iPosicion: Word; const eComoMostrar: eExMostrar );
    procedure Assign(Source: TListaCampos);
    procedure BorrarClasificacion(const sCodigo: String);
    procedure Clear;
    procedure CargarDisponibles( Lista: TStrings );
    procedure CargarEscogidos( Lista: TStrings; const sClasificacion: String );
    procedure ConstruyeForma( Grupos: TListaClasificaciones );
    procedure SetRedraw;
  end;

implementation

uses ZetaCommonClasses;//, FEditExpediente;

const
     K_MENOR = -1;
     K_IGUAL = 0;
     K_MAYOR = 1;
     K_MAX_LENGTH_DESCRIP = 30;

function ComparaCampos( Item1, Item2: Pointer ): Integer;
begin
     Result := TCampo( Item1 ).Comparar( TCampo( Item2 ) );
end;

function ComparaClasificaciones( Item1, Item2: Pointer ): Integer;
begin
     Result := TClasificacion( Item1 ).Comparar( TClasificacion( Item2 ) );
end;

{ ******** TClasificacion ********** }

constructor TClasificacion.Create;
begin
     FCodigo := VACIO;
     FNombre := VACIO;
     FPosicion := 0;
end;

destructor TClasificacion.Destroy;
begin
     inherited Destroy;
end;

function TClasificacion.Comparar(Value: TClasificacion): Integer;
begin
     if ( Self.Posicion < Value.Posicion ) then
        Result := K_MENOR
     else
         if ( Self.Posicion > Value.Posicion ) then
            Result := K_MAYOR
         else
             Result := AnsiCompareText( Self.Nombre, Value.Nombre );
end;

{ ********* TListaClasificaciones *********** }

constructor TListaClasificaciones.Create;
begin
     FClasificaciones := TList.Create;
end;

destructor TListaClasificaciones.Destroy;
begin
     Clear;
     FreeAndNil( FClasificaciones );
     inherited Destroy;
end;

procedure TListaClasificaciones.Assign(Source: TListaClasificaciones);
var
   i: Integer;
begin
     Self.Clear;
     with Source do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Clasificacion[ i ] do
               begin
                    Self.Add( Codigo, Nombre, Posicion );
               end;
          end;
     end;
end;

procedure TListaClasificaciones.Clear;
var
   i: Integer;
begin
     with FClasificaciones do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaClasificaciones.Count: Integer;
begin
     Result := FClasificaciones.Count;
end;

procedure TListaClasificaciones.Delete(const Index: Integer);
begin
     Clasificacion[ Index ].Free;
     FClasificaciones.Delete( Index );
end;

function TListaClasificaciones.GetClasificacion(Index: Integer): TClasificacion;
begin
     with FClasificaciones do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TClasificacion( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TListaClasificaciones.Ordena;
begin
     FClasificaciones.Sort( ComparaClasificaciones );
end;

procedure TListaClasificaciones.Cargar(Lista: TStrings);
var
   i: Integer;
begin
     Ordena;
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  Lista.AddObject( Clasificacion[ i ].Nombre, Clasificacion[ i ] )
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

function TListaClasificaciones.Add(const sCodigo, sNombre: String; const iPosicion: Word): TClasificacion;
begin
     Result := TClasificacion.Create;
     try
        FClasificaciones.Add( Result );
        with Result do
        begin
             Codigo := sCodigo;
             Nombre := sNombre;
             Posicion := iPosicion;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TListaClasificaciones.Borrar(const sCodigo: String );
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Clasificacion[ i ].Codigo = sCodigo ) then
          begin
               Delete( i );
               Break;
          end;
     end;
end;

{function TListaClasificaciones.Repetido(const sCodigo, sNombre: String): Boolean;
begin
     Result := Repetido(sCodigo,sNombre,Nil);
end;}

function TListaClasificaciones.EsIgual( Grupo :TClasificacion; Const sCodigo,sNombre : String ):Boolean;
begin
     Result := ( AnsiCompareText( Grupo.Codigo,sCodigo ) = 0 ) or ( AnsiCompareText( Grupo.Nombre, sNombre ) = 0 );
end;

function TListaClasificaciones.Repetido(const sCodigo, sNombre: String;Original: TClasificacion): Boolean;
 var
    i: Integer;
begin
     Result := False;
     for i := 0 to ( Count - 1 ) do
     begin
          if not Assigned(Original) or not EsIgual(Clasificacion[i],Original.Codigo,Original.Nombre) then
          begin
               if EsIgual(Clasificacion[i],sCodigo,sNombre) then
               begin
                    Result := True;
                    Break;
               end;
          end;
     end;
end;

{ ******** TCampo ********* }

constructor TCampo.Create;
begin
     FTipo := xctTexto;
     FLetrero := VACIO;
     FCampo := VACIO;
end;

destructor TCampo.Destroy;
begin
     inherited Destroy;
end;

function TCampo.GetNombre: String;
begin
     Result := Format( '%s # %d', [ObtieneElemento( lfExCampoTipo, Ord(FTipo) ),FIndice] );
end;

function TCampo.GetCampoTexto: String;
begin
     Result := StringReplace( Campo, '_BOL', '_DES', [ rfReplaceAll, rfIgnoreCase ] );
end;

function TCampo.GetCapturar: Boolean;
begin
     Result := ( FClasificacion <> VACIO );
end;

function TCampo.Comparar( Value: TCampo ): Integer;
begin
     if ( Self.Posicion < Value.Posicion ) then
        Result := K_MENOR
     else
         if ( Self.Posicion > Value.Posicion ) then
            Result := K_MAYOR
         else
             Result := AnsiCompareText( Self.Campo, Value.Campo );
end;

procedure TCampo.Inicializa;
begin
     FClasificacion := VACIO;
     FLetrero := Nombre;
     FDefault := VACIO;
     FMostrar := arSiempre;
     FPosicion := 0;
end;


function TCampo.TextoEscogido: String;
begin
     Result := Format( '%s ( %s )', [ Letrero, Nombre ] );
end;

function TCampo.TextoDisponible: String;
begin
     Result := Format( '%s', [ Nombre ] );
end;

{ ******* TListaCampos ******** }

constructor TListaCampos.Create;
begin
     FCampos := TList.Create;
     SetRedraw;
end;

destructor TListaCampos.Destroy;
begin
     Clear;
     FreeAndNil( FCampos );
     inherited Destroy;
end;

procedure TListaCampos.Assign( Source: TListaCampos );
var
   i: Integer;
begin
     Self.Clear;
     with Source do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Campo[ i ] do
               begin
                    Self.Add( Tipo, Letrero, Campo, ValorDefault, Clasificacion, Posicion, Mostrar );
               end;
          end;
     end;
end;

procedure TListaCampos.SetRedraw;
begin
     FRedraw := True;
     FDatasource := nil;
     FForma := nil;
     FPaginas := nil;
end;

{function TListaCampos.CuantosHay( const eTipo: eCampoTipo ): Integer;
var
   i: Integer;
begin
     Result := 0;
     for i := 0 to ( Count - 1 ) do
     begin
          with Campo[ i ] do
          begin
               if ( Tipo = eTipo ) then
                  Inc( Result );
          end;
     end;
end;}

function TListaCampos.CuantosHayEnGrupo(const sCodigo: String): Integer;
var
   i: Integer;
begin
     Result := 0;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Campo[ i ].Clasificacion = sCodigo ) then
          begin
               Inc( Result );
          end;
     end;
end;

procedure TListaCampos.Add( const eTipo: eExCampoTipo; const sLetrero, sCampo, sDefault, sClasificacion: String;
                            const iPosicion: Word; const eComoMostrar: eExMostrar );
var
   Campo: TCampo;
begin
     Campo := TCampo.Create;
     try
        FCampos.Add( Campo );
        with Campo do
        begin
             Tipo := eTipo;
             Letrero := sLetrero;
             Campo := sCampo;
             ValorDefault := sDefault;
             Clasificacion := sClasificacion;
             Posicion := iPosicion;
             Indice := StrToIntDef(Copy(sCampo,9,2),0);
             Mostrar := eComoMostrar;
        end;
     except
           on Error: Exception do
           begin
                Campo.Free;
                raise;
           end;
     end;
end;

procedure TListaCampos.Clear;
var
   i: Integer;
begin
     with FCampos do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaCampos.Count: Integer;
begin
     Result := FCampos.Count;
end;

function TListaCampos.GetCampo(Index: Integer): TCampo;
begin
     with FCampos do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TCampo( Items[ Index ] )
          else
              Result := nil;
     end;
end;


procedure TListaCampos.Delete(const Index: Integer);
begin
     Campo[ Index ].Free;
     FCampos.Delete( Index );
end;

procedure TListaCampos.Ordena;
begin
     FCampos.Sort( ComparaCampos );
end;

procedure TListaCampos.Cargar(Lista: TStrings; const sClasificacion: String );
var
   i: Integer;
begin
     Ordena;
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             with FCampos do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       if ( Campo[ i ].Clasificacion = sClasificacion ) then
                       begin
                            if Campo[ i ].Capturar then
                               Lista.AddObject( Campo[ i ].TextoEscogido, Campo[ i ] )
                            else
                                Lista.AddObject( Campo[ i ].TextoDisponible, Campo[ i ] );
                       end;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TListaCampos.CargarDisponibles( Lista: TStrings );
begin
     Cargar( Lista, VACIO );
end;

procedure TListaCampos.CargarEscogidos( Lista: TStrings; const sClasificacion: String );
begin
     Cargar( Lista, sClasificacion );
end;

procedure TListaCampos.BorrarClasificacion(const sCodigo: String );
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Campo[ i ].Clasificacion = sCodigo ) then
          begin
               Campo[ i ].Inicializa;
          end;
     end;
end;

procedure TListaCampos.ConstruyeGrupo( oGrupo: TClasificacion; const iCuantos: Integer; Padre: TWinControl );
const K_FACTOR_ANCHO = 0.06;
var
   i, iMargenDerecho, iTop, iMiddle, iHeight, iWidth, iLeftBool, iWidthBool: Integer;
begin
     iWidth := Padre.Parent.Width;
     iMiddle := Trunc( iWidth / 3 );
     iMargenDerecho := Trunc( K_FACTOR_ANCHO * iWidth );
     iTop := 2;
     for i := 0 to ( Count - 1 ) do
     begin
          with Campo[ i ] do
          begin
               if ( Clasificacion = oGrupo.Codigo ) then
               begin
                    with TStaticText.Create( FForma ) do
                    begin
                         Name := Format( '%s_LBL', [ Campo ] );
                         Parent := Padre;
                         Alignment := taRightJustify;
                         Height := 13;
                         Top := iTop + 3;
                         Caption := Letrero + ':';
                         Left := iMiddle - 2 - Width;
                    end;
                    case Tipo of
                         xctTexto:
                         begin
                              with TDBEdit.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Width := iWidth - Left - iMargenDerecho;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                                   //MaxLength := K_MAX_LENGTH_DESCRIP;
                              end;
                         end;
                         xctNumero:
                         begin
                              with TZetaDBNumero.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Parent := Padre;
                                   iHeight := Height;
                                   Top := iTop;
                                   Left := iMiddle + 2;
                                   Mascara := mnNumeroGlobal;
                                   Enabled := True;
                                   Datasource := Self.FDatasource;
                              end;
                         end;
                         xctBooleano, xctBoolStr:
                         begin
                              with TDBCheckBox.Create( FForma ) do
                              begin
                                   Name := Format( '%s', [ Campo ] );
                                   DataField := Campo;
                                   Caption := VACIO;
                                   Parent := Padre;
                                   Top := iTop + 2;
                                   Left := iMiddle + 2;
                                   Width := Height;
                                   AllowGrayed := False;
                                   Enabled := True;
                                   ValueChecked := K_GLOBAL_SI;
                                   ValueUnchecked := K_GLOBAL_NO;
                                   Datasource := Self.FDatasource;
                                   iHeight := Height;
                                   iLeftBool := Left;
                                   iWidthBool := Width;
                                   //Checked := False;
                              end;
                              if ( Tipo = xctBoolStr ) then
                              begin
                                   with TDBEdit.Create( FForma ) do
                                   begin
                                        Name := Format( '%s', [ CampoTexto ] );
                                        DataField := CampoTexto;
                                        Parent := Padre;
                                        iHeight := Height;
                                        Top := iTop;
                                        Left := iLeftBool + iWidthBool; //+ 2;
                                        Width := iWidth - Left - iMargenDerecho;
                                        Enabled := True;
                                        Datasource := Self.FDatasource;
                                        //MaxLength := K_MAX_LENGTH_DESCRIP;
                                   end;
                              end;
                         end;
                    else
                        iHeight := 0;
                    end;
                    iTop := iTop + iHeight + 1;
               end;
          end;
     end;
end;

procedure TListaCampos.ConstruyeForma( Grupos: TListaClasificaciones );
var
   i, j, iCuantos: Integer;
   oScrollBar: TScrollBox;
   oTab: TTabSheet;
   oGrupo: TClasificacion;
begin
     if FRedraw then
     begin
          With FPaginas do
          begin
               for i:=( PageCount - 1 ) downto 1 do
               begin
                    Pages[i].Free;
               end;
          end;
          Ordena; // Esta ordenando la lista de Campos

          { Esta ordenando la lista de los grupos }
          { Est� afuera del with para que sea mas legible }
          Grupos.Ordena;
          with Grupos do
          begin
               for j := 0 to ( Count - 1 ) do
               begin
                    oGrupo := Clasificacion[ j ];
                    with oGrupo do
                    begin
                         iCuantos := CuantosHayEngrupo( Codigo );
                         if ( iCuantos > 0 ) then
                         begin
                              oTab := TTabSheet.Create( FForma );
                              with oTab do
                              begin
                                   PageControl := FPaginas;
                                   Caption := Nombre;
                              end;
                              oScrollBar := TScrollBox.Create( FForma );
                              with oScrollBar do
                              begin
                                   Parent := oTab;
                                   Align := alClient;
                                   BorderStyle := bsNone;
                              end;
                              Application.ProcessMessages;
                              ConstruyeGrupo( oGrupo, iCuantos, oScrollBar );
                         end;
                    end;
               end;
          end;
     end;
     FRedraw := False;
end;



end.
