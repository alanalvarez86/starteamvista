inherited fIncpermisos_DevEx: TfIncpermisos_DevEx
  Left = 251
  Top = 253
  Caption = 'Incapacidades/Permisos'
  ClientHeight = 436
  ClientWidth = 547
  ExplicitWidth = 547
  ExplicitHeight = 436
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 547
    ExplicitWidth = 547
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 288
      ExplicitWidth = 288
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 282
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 547
    Height = 417
    ExplicitWidth = 547
    ExplicitHeight = 417
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object IN_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'IN_FEC_INI'
        Options.Grouping = False
      end
      object IN_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'IN_DIAS'
        Options.Grouping = False
      end
      object IN_FEC_FIN: TcxGridDBColumn
        Caption = 'Regresa'
        DataBinding.FieldName = 'IN_FEC_FIN'
        Options.Grouping = False
      end
      object IN_CLASIFI: TcxGridDBColumn
        Caption = 'Clase'
        DataBinding.FieldName = 'IN_CLASIFI'
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TB_ELEMENT'
      end
      object IN_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'IN_NUMERO'
        Options.Grouping = False
      end
      object IN_COMENTA: TcxGridDBColumn
        Caption = 'Observaci'#243'n'
        DataBinding.FieldName = 'IN_COMENTA'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
