object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 529
  Top = 246
  Height = 479
  Width = 741
  object cdsNomParamLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 60
    Top = 24
  end
  object cdsConceptosLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 160
    Top = 24
  end
  object cdsCondiciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    Left = 308
    Top = 28
  end
  object cdsPuestos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsPuestosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    LookupActivoField = 'PU_ACTIVO'
    LookupConfidenField = 'PU_NIVEL0'
    Left = 40
    Top = 88
  end
  object cdsTurnos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsTurnosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Turnos'
    LookupDescriptionField = 'TU_DESCRIP'
    LookupKeyField = 'TU_CODIGO'
    LookupActivoField = 'TU_ACTIVO'
    LookupConfidenField = 'TU_NIVEL0'
    Left = 112
    Top = 88
  end
  object cdsClasifi: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsClasifiAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Clasificaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    LookupConfidenField = 'TB_NIVEL0'
    Left = 184
    Top = 88
  end
  object cdsHorarios: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'HO_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsHorariosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Horarios'
    LookupDescriptionField = 'HO_DESCRIP'
    LookupKeyField = 'HO_CODIGO'
    LookupActivoField = 'HO_ACTIVO'
    LookupConfidenField = 'HO_NIVEL0'
    Left = 240
    Top = 88
  end
  object cdsContratos: TZetaLookupDataSet
    Tag = 7
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsContratosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Tipos de Contrato'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    LookupConfidenField = 'TB_NIVEL0'
    Left = 40
    Top = 152
  end
end
