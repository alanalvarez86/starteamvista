unit FMedicinaEntr_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TMedicinaEntr_DevEx = class(TBaseGridLectura_DevEx)
    ME_CODIGO: TcxGridDBColumn;
    ME_NOMBRE: TcxGridDBColumn;
    MT_FECHA: TcxGridDBColumn;
    MT_CANTIDAD: TcxGridDBColumn;
    US_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  MedicinaEntr_DevEx: TMedicinaEntr_DevEx;

implementation

uses
//ZetaBuscador
DMedico, ZetaCommonLists, FAyudaContexto;

{$R *.DFM}

procedure TMedicinaEntr_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  //CanLookup := True;
  HelpContext := H00004_Medicina_entregada;
  TipoValorActivo1 := stExpediente;
end;


procedure TMedicinaEntr_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;

end;

procedure TMedicinaEntr_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsMedicinaEntr.Conectar;
          DataSource.DataSet:= cdsMedicinaEntr;
     end;
end;

procedure TMedicinaEntr_DevEx.Refresh;
begin
     dmMedico.cdsMedicinaEntr.Refrescar;
end;

procedure TMedicinaEntr_DevEx.Agregar;
begin
     dmMedico.cdsMedicinaEntr.Agregar;
end;

procedure TMedicinaEntr_DevEx.Borrar;
begin
     dmMedico.cdsMedicinaEntr.Borrar;
end;

procedure TMedicinaEntr_DevEx.Modificar;
begin
     dmMedico.cdsMedicinaEntr.Modificar;
end;

{Se requiere el PuedeAgregar para que no agregue
registros cuando la base de datos esta vacia.}
function TMedicinaEntr_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsMedicinaEntr.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;


end.
