unit FMedicina;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls;

type
  TMedicina = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Medicina: TMedicina;

implementation

uses
//Zetabuscador,
DMedico, FAyudaContexto;

{$R *.DFM}

procedure TMedicina.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TMedicina.Connect;
begin
     with dmMedico do
     begin
          cdsMedicina.Conectar;
          DataSource.DataSet:= cdsMedicina;
     end;
end;

procedure TMedicina.Refresh;
begin
     dmMedico.cdsMedicina.Refrescar;
end;

procedure TMedicina.Agregar;
begin
     dmMedico.cdsMedicina.Agregar;
end;

procedure TMedicina.Borrar;
begin
     dmMedico.cdsMedicina.Borrar;
end;

procedure TMedicina.Modificar;
begin
     dmMedico.cdsMedicina.Modificar;
end;

procedure TMedicina.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', 'Medicinas', 'ME_CODIGO', dmMedico.cdsMedicina );
end;



end.
