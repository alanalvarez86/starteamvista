inherited MedicinaEntr_DevEx: TMedicinaEntr_DevEx
  Left = 195
  Top = 175
  Caption = 'Medicinas Entregadas'
  ClientHeight = 300
  ClientWidth = 534
  ExplicitWidth = 534
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 534
    ExplicitWidth = 534
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 275
      ExplicitWidth = 275
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 269
        ExplicitLeft = 189
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 534
    Height = 281
    ExplicitWidth = 534
    ExplicitHeight = 281
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object ME_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'ME_CODIGO'
      end
      object ME_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'ME_NOMBRE'
      end
      object MT_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'MT_FECHA'
      end
      object MT_CANTIDAD: TcxGridDBColumn
        Caption = 'Cantidad'
        DataBinding.FieldName = 'MT_CANTIDA'
      end
      object US_NOMBRE: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_NOMBRE'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
