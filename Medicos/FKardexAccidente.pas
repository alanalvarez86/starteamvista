unit FKardexAccidente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls;

type
  TKardexAccidente = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function  PuedeAgregar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  KardexAccidente: TKardexAccidente;

implementation

uses
//ZetaBuscador
DMedico,ZetaCommonLists, FAyudaContexto;

{$R *.DFM}

procedure TKardexAccidente.FormCreate(Sender: TObject);
begin
  inherited;
  //CanLookup := True;
  HelpContext := H00104_Accidentes;
  TipoValorActivo1 := stExpediente;
end;

procedure TKardexAccidente.Connect;
begin
     with dmMedico do
     begin
          cdsExpediente.Conectar;
          cdsKardexAccidente.Conectar;
          DataSource.DataSet:= cdsKardexAccidente;
     end;
end;

procedure TKardexAccidente.Refresh;
begin
     dmMedico.cdsKardexAccidente.Refrescar;
end;

procedure TKardexAccidente.Agregar;
begin
     dmMedico.cdsKardexAccidente.Agregar;
end;

procedure TKardexAccidente.Borrar;
begin
     dmMedico.cdsKardexAccidente.Borrar;
end;

procedure TKardexAccidente.Modificar;
begin
     dmMedico.cdsKardexAccidente.Modificar;
end;


{procedure TKardexAccidente.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Accidentes/Enfermedades', 'EX_CODIGO', dmMedico.cdsKardexAccidente );
end;}

function TKardexAccidente.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsKardexAccidente.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end; 

end.
