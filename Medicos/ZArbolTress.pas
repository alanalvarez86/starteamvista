unit ZArbolTress;

interface

uses ComCtrls, Dialogs, SysUtils, DB,Controls,
     ZArbolTools, ZNavBarTools, cxtreeview, ZetaCommonTools;


function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
procedure MuestraArbolOriginal( oArbol: TTreeView );

//DevEx
procedure CreaNavBarUsuario ( oNavBarMgr: TNavBarMgr; const sFileName: String ; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses ZetaDespConsulta,
     ZetaCommonClasses,
     ZAccesosTress,
     ZAccesosMgr,
     ZGlobalTress,
     DGlobal,
     DSistema,
     FTressShell,
     DCliente,
     ZetaTipoEntidad;


const
     K_EMPLEADOS                        = 297; //  = 1000;
     K_EMP_EXPEDIENTE                   = 298; //1001
     //K_EMP_DATOS_CONTRATA               = 1002;
     K_EMP_CONSULTAS_MEDICAS            = 299;//1003
     K_EMP_EMBARAZOS                    = 300;//1004;
     K_EMP_MED_ENTR                     = 320;//1035;
     K_EMP_AX_ENFERMEDADES              = 301;//1005;
     K_EMP_INCAPACIDADES                = 314;//1006;
     //MA: Linea desactivada por la union de incapacidades y permisos
     //K_EMP_PERMISOS                     = 1007;
     K_EMP_REGISTRO                     = 313;//1008;
     //K_EMP_PROCESOS                     = 1017;
     K_CONSULTAS                        = 316;//1022;
     K_REPORTES                         = 321;
     K_CONS_SQL                         = 317;//1024;
     K_PROCESOS                         = 318;//1025;
     K_CATALOGOS                        = 304;
     K_CAT_MEDICAMENTOS                 = 1027;
     K_CAT_DIAGNOSTICO                  = 1028;
     K_CAT_TIPO_CONSULTA                = 1029;
     K_CAT_TIP_EST_LAB                  = 1030;
     K_CAT_TIP_ACC                      = 1031;
     K_CAT_CAU_ACC                      = 1032;
     K_CAT_MOT_ACC                      = 1033;
     K_SISTEMA                          = 1034;
     K_GLOBAL_SERV_MED                  = 1036;


     {******** Cat�logos **********}
     K_NODO_X                                       =99;     //**PENDIENTE
     K_FORMA_X                        = 100;      //**PENDIENTE
     K_FULL_ARBOL_TRESS                 =9999;




function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;



function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;


//DevEx: Funcion para definir Grupos del NavBar
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;


function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     with GLobal do
     begin
          case iNodo of

               // Empleados
                 K_EMPLEADOS               : Result := Folder( 'Pacientes', D_EMP_SERVICIO_MEDICO );
                 K_EMP_EXPEDIENTE          : Result := Forma( 'Expediente', efcExpediente );
                 //K_EMP_DATOS_CONTRATA      : Result := Forma( 'Datos de Contrataci�n', efcContratacion );
                 K_EMP_CONSULTAS_MEDICAS   : Result := Forma( 'Consultas M�dicas', efcConsMedica );
                 K_EMP_EMBARAZOS           : Result := Forma( 'Embarazos', efcEmbarazo );
                 K_EMP_AX_ENFERMEDADES     : Result := Forma( 'Accidentes', efcAxEnfTrabajo );
                 K_EMP_INCAPACIDADES       : Result := Forma( 'Incapacidades/Permisos', efcIncapacidades );
                 K_EMP_MED_ENTR            : Result := Forma( 'Medicinas Entregadas', efcMedicinaEntr );
                 K_EMP_REGISTRO            : Result := Forma( 'Registro', efcRegistro );
                 //K_EMP_PROCESOS            : Result := Forma( 'Procesos', efcProcesos );
                 //K_EMP_PERMISOS            : Result := Forma( 'Permisos', efcPermisos );
               // Consultas
                 K_CONSULTAS               : Result := Folder( 'Consultas Del Sistema', D_SERV_CONSULTAS );
                 K_REPORTES                : Result := Forma( 'Reportes' , efcReportes );
                 K_CONS_SQL                : Result := Forma( 'SQL', efcQueryGral );
                 K_PROCESOS                : Result := Forma( 'Procesos', efcSistProcesos );
               // Catalogos
                 K_CATALOGOS               : Result := Folder( 'Cat�logos', D_SERV_CATALOGOS );
                 K_CAT_MEDICAMENTOS        : Result := Forma( 'Medicinas', efcMedicamentos );
                 K_CAT_DIAGNOSTICO         : Result := Forma( 'Diagn�sticos', efcDiagnosticos );
                 K_CAT_TIPO_CONSULTA       : Result := Forma( 'Tipos de Consulta', efcTipoConsulta );
                 K_CAT_TIP_EST_LAB         : Result := Forma( 'Tipos de Estudio' , efcTipoEstdLab );
                 K_CAT_TIP_ACC             : Result := Forma( 'Tipos de Accidente' , efcTipoAcc );
                 K_CAT_CAU_ACC             : Result := Forma( 'Causas de Accidente' , efcCauAcc );
                 K_CAT_MOT_ACC             : Result := Forma( 'Motivos de Accidente', efcMotAcc );
                 K_GLOBAL_SERV_MED         : Result := Forma( 'Globales de Empresa', efcSistGlobales );
            else
                 Result := Folder( VACIO, 0 );
            end;
     end;
end;

procedure CreaArbolDefault( oArbolMgr: TArbolMgr );

procedure NodoNivel( const iNivel, iNodo: Integer );
begin
     oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;


begin  // CreaArbolDefault

    // ********* Empleados ********
    NodoNivel( 0, K_EMPLEADOS );
      NodoNivel( 1, K_EMP_EXPEDIENTE );
      NodoNivel( 1, K_EMP_CONSULTAS_MEDICAS );
      NodoNivel( 1, K_EMP_EMBARAZOS );
      NodoNivel( 1, K_EMP_AX_ENFERMEDADES );
      NodoNivel( 1, K_EMP_INCAPACIDADES );
      NodoNivel( 1, K_EMP_MED_ENTR );
      NodoNivel( 1, K_EMP_REGISTRO );
      //NodoNivel( 1, K_EMP_PROCESOS );
      //NodoNivel( 1, K_EMP_PERMISOS );

    NodoNivel( 0,K_CONSULTAS );
      NodoNivel( 1,K_REPORTES );
      NodoNivel( 1,K_CONS_SQL );
      NodoNivel( 1,K_PROCESOS );

    NodoNivel( 0,K_CATALOGOS );
      NodoNivel( 1,K_CAT_MEDICAMENTOS);
      NodoNivel( 1,K_CAT_DIAGNOSTICO);
      NodoNivel( 1,K_CAT_TIPO_CONSULTA);
      NodoNivel( 1,K_CAT_TIP_EST_LAB);
      NodoNivel( 1,K_CAT_TIP_ACC);
      NodoNivel( 1,K_CAT_CAU_ACC);
      NodoNivel( 1,K_CAT_MOT_ACC);
      NodoNivel( 1,K_GLOBAL_SERV_MED);
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin  // CreaArbolUsuario
     with oArbolMgr do
     begin
     //MA: Linea activa cuando se implemente la forma expediente
         Configuracion := TRUE;
         oArbolMgr.NumDefault := K_EMP_EXPEDIENTE;
         CreaArbolDefault( oArbolMgr );
         Arbol.FullExpand;
     end;
end;

procedure MuestraArbolOriginal( oArbol: TTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;


{ aniadido recientemente }


//DevEx
procedure CreaNavBarUsuario (  oNavBarMgr: TNavBarMgr; const sFileName: String; Arbolitos: array of TcxtreeView ; ArbolitoUsuario : TcxTreeView);
var
   DataSet: TDataSet;
   NodoRaiz: TGrupoInfo;
//Edit by MP: Metodo Modificado para usar un TcxTreeview
procedure CreaArbolito( Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
    DataSet := dmCliente.cdsUsuario;


   //  GetDatosNavBarUsuario(oNavBarMgr, DataSet, sFileName );
     //CreaNavBarUsuario
     with oNavBarMgr.DatosUsuario do
     begin
          oNavBarMgr.NumDefault := NodoDefault;
          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oNavBarMgr.Configuracion ) or ( TieneArbol ) then
          begin
             with NodoRaiz do
             begin
                      Caption := NombreNodoInicial;
                      IndexDerechos := K_SIN_RESTRICCION;
                    if(oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)) then
                      begin
                          CreaArbolito( ArbolitoUsuario ); //Pendiente cambiar el indice
                      end;

             end;
          end;
          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oNavBarMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
             begin
                if (oNavBarMgr.NumDefault = 0 ) then
                    oNavBarMgr.NumDefault :=  K_NODO_X; //Por si solo se agregara el arbol del sistema

                CreaNavBarDefault(oNavBarMgr, Arbolitos );
             end;

             // Si por derechos no tiene nada, agregar grupo
             if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
             begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     //EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ);
                end;
             end;
          end;
     end;
end;


procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
     Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     with oArbolMgr do
                     begin
                          //Configuracion := TRUE;
                           begin
                            //oArbolMgr.NumDefault := K_GLOBAL ;
                            if(K_global = K_FULL_ARBOL_TRESS) then
                              ZArbolTress.CreaArbolDefault( oArbolMgr)
                            else
                              CreaArbolitoDefault( oArbolMgr, K_GLOBAL  )
                          end;
                               //Edit by MP: si el arbol es nil, utilizar el Arbol_Devex
                          if(arbol = nil) then
                            arbol_Devex.FullCollapse
                          else
                            Arbol.FullCollapse;
                          // Fin Edit
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario}
     // ************ Presupuestos *****************

     begin
        if( NodoNivel( K_EMPLEADOS, 1 )) then
        begin
          CreaArbolito ( K_EMPLEADOS, Arbolitos[1]);
        end;
         if( NodoNivel( K_CONSULTAS, 2 )) then
        begin
          CreaArbolito ( K_CONSULTAS, Arbolitos[2]);
        end;
        if( NodoNivel( K_CATALOGOS, 3 )) then
        begin
          CreaArbolito ( K_CATALOGOS, Arbolitos[3]);
        end;
     end;
end;

//DevEx
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
    case iNodo of
      K_EMPLEADOS : Result := Grupo( 'Pacientes', iNodo );
      K_CONSULTAS : Result := Grupo( 'Consultas del Sistema', iNodo );
      K_CATALOGOS : Result := Grupo( 'Cat�logos', iNodo );
    end;
end;


procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
var
   i, iNiveles: Integer;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
  //**   if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then

          NodoNivel(1,iNodo);
end;

begin  // CreaArbolDefault
    case iAccesoGlobal of
     K_EMPLEADOS:
     begin
         NodoNivel( 0, K_EMP_EXPEDIENTE);
         NodoNivel( 0, K_EMP_CONSULTAS_MEDICAS);
         NodoNivel( 0, K_EMP_EMBARAZOS);
         NodoNivel( 0, K_EMP_AX_ENFERMEDADES);
         NodoNivel( 0, K_EMP_INCAPACIDADES);
         NodoNivel( 0, K_EMP_REGISTRO);
         NodoNivel( 0, K_EMP_MED_ENTR);
     end;
     K_CONSULTAS :
     begin
        NodoNivel( 0, K_REPORTES );
        NodoNivel( 0, K_CONS_SQL );
        NodoNivel( 0, K_PROCESOS );
     end;
     K_CATALOGOS :
     begin
        NodoNivel( 0, K_CAT_MEDICAMENTOS );
        NodoNivel( 0, K_CAT_DIAGNOSTICO );
        NodoNivel( 0, K_CAT_TIPO_CONSULTA );
        NodoNivel( 0, K_CAT_TIP_EST_LAB );
        NodoNivel( 0, K_CAT_TIP_ACC );
        NodoNivel( 0, K_CAT_CAU_ACC );
        NodoNivel( 0, K_CAT_MOT_ACC );
        NodoNivel( 0, K_GLOBAL_SERV_MED );
     end;

end;
end;

{ aniadido recientemente }

end.
