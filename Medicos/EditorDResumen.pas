unit EditorDResumen;

interface
uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Menus, DB,
     DBCtrls, Comctrls, cxRichEdit,Clipbrd;

type
 TEditorDResumen = class
 private
    fERichText : TcxRichEdit;
    fTipoLetra: String;
    fAlineaLetra: TAlignment;
    fColorLetra: TColor;
    fEstiloLetra: TFontStyles;
    fBullets: TNumberingStyle;
    fTamano: Integer;
    fIdent: Integer;
    procedure SetTipoLetra(const Value: String);
    procedure SetAlineaLetra(const Value: TAlignment);
    procedure SetBullets(const Value: TNumberingStyle);
    procedure SetColorLetra(const Value: TColor);
    procedure SetEstiloLetra(const Value: TFontStyles);
    procedure SetTamano(const Value: Integer);
    procedure setIdent(const Value: Integer);
 public
  property ERichText : TcxRichEdit read fERichText write fERichText;
  property TipoLetra : String read fTipoLetra write SetTipoLetra;
  property ColorLetra: TColor read fColorLetra write SetColorLetra;
  property EstiloLetra:TFontStyles read fEstiloLetra write SetEstiloLetra;
  property AlineaLetra:TAlignment read fAlineaLetra write SetAlineaLetra;
  property Bullets : TNumberingStyle read fBullets write SetBullets;
  property Ident : Integer read fIdent write setIdent;
  property Tamano : Integer read fTamano write SetTamano;
  procedure AgregarLinea( Const Texto:WideString );
  procedure Reset;
end;

type
    TEditorCliente = class
  private
    FEditor:TEditorDResumen;
    FTexto : string;
  public
    constructor Create;
    Destructor Destroy; override;
    procedure RenglonVacio;
    procedure CamposExpediente(const sCampo: String);
    procedure CamposConsulta(const sCampo: String);
    procedure Condicion_Float(const sTexto: String; Const sCampo: Real; Const sAdicion: String);
    procedure Condicion_Integer(const sTexto: String; Const sCampo: Integer; Const sAdicion: String);
    procedure Condicion_String(const sTexto: String; Const sCampo, sAdicion: String);
    //procedure Subtitulo(const sTexto: String);
    procedure Clasificaciones(const STexto: String);
    procedure EscribeCampos(const sTexto: String);
    //procedure Subtitulo3(const sTexto: String);
    procedure Titulo(const sTitulo: String);
    procedure Pos_Inicio ( oRichText : TcxRichedit );
    procedure Inicializa( oRichText : TcxRichEdit);
    procedure CamposDefault( Const Subtitulo1:String; Const Campo1:String );
end;

const
     K_SIZE_LETRA = 8;
     K_SIZE_CAMPOS_DEFAULT = 9;
     K_IDENT_CAMPOS_EXP = 5;
     K_IDENT_CLASIFICACIONES = 50;
     K_IDENT_CAMPOS_CONS = 70;

implementation

uses ZetaCommonClasses, ZetaCommonTools;

{TEditorCliente}
procedure TEditorCliente.Inicializa(oRichText : TcxRichEdit);
begin
     with FEditor do
     begin
          ERichText := oRichText;
          Reset;
     end;
end;

procedure TEditorCliente.Titulo(Const sTitulo :String );
const
     K_SIZE_LETRA_TITULO = 16;
begin
     With FEditor do
     begin
          AlineaLetra := taCenter;
          ColorLetra:=clBlack;
          EstiloLetra := [fsBold,fsUnderLine];
          TipoLetra := 'Segoe UI';
          Tamano := K_SIZE_LETRA_TITULO;
          AgregarLinea(sTitulo);
     end;
end;

procedure TEditorCliente.EscribeCampos(Const sTexto :String);
begin
     With FEditor do
     begin
         Ident := K_IDENT_CAMPOS_EXP;
         AlineaLetra := taLeftJustify;
         Tamano := K_SIZE_LETRA;
         ColorLetra:=clBlack;
         FTexto := sTexto;
    end;
end;

procedure TEditorCliente.RenglonVacio;
begin
     CamposExpediente( VACIO );
end;

procedure TEditorCliente.CamposExpediente(Const sCampo: String);
begin
     With FEditor do
     begin
          Ident := K_IDENT_CAMPOS_EXP;
          EstiloLetra := [];
          ColorLetra:=clBlack;
          Tamano := K_SIZE_LETRA;
          AgregarLinea(FTexto + ' ' + sCampo);
          FTexto := VACIO;
    end;
end;

procedure TEditorCliente.CamposConsulta(Const sCampo: String);
begin
     With FEditor do
     begin
          Ident := K_IDENT_CAMPOS_CONS;
          EstiloLetra := [];
          ColorLetra:=clBlack;
          Tamano := K_SIZE_LETRA;
          AgregarLinea(FTexto + ' '+ sCampo);
          FTexto := VACIO;
     end;
end;

procedure TEditorCliente.CamposDefault(const Subtitulo1:String;const Campo1:String );
begin
     With FEditor do
     begin
          AlineaLetra := taLeftJustify;
          EstiloLetra := [];
          ColorLetra:=clBlack;
          Tamano := K_SIZE_CAMPOS_DEFAULT;
          AgregarLinea(Subtitulo1 + Campo1 );
     end;
end;

procedure TEditorCliente.Clasificaciones(Const STexto :String);
begin
     With FEditor do
     begin
          Ident := K_IDENT_CLASIFICACIONES;
          EstiloLetra := [fsBold,fsUnderline];
          Tamano := K_SIZE_LETRA;
          ColorLetra:=clNavy;
          AgregarLinea(sTexto);
     end;
end;

procedure TEditorCliente.Condicion_String(Const sTexto:String; Const sCampo:String; Const sAdicion:String);
begin
     if strLleno( sCampo )then
     begin
          RenglonVacio;
          //EscribeCampos('');
          EscribeCampos(sTexto);
          CamposConsulta(sCampo+' '+sAdicion);
     end;
end;

procedure TEditorCliente.Condicion_Integer(Const sTexto:String; Const sCampo:Integer; Const sAdicion:String);
begin
     if (sCampo > 0 )then
     begin
          EscribeCampos(sTexto);
          CamposConsulta(inttostr(sCampo)+' '+sAdicion);
     end;
end;

procedure TEditorCliente.Condicion_Float(Const sTexto:String; Const sCampo:Real; Const sAdicion:String);
begin
     if (sCampo > 0 )then
     begin
          EscribeCampos(sTexto);
          CamposConsulta(Format('%2.2n',[sCampo])+' '+sAdicion);
     end;
end;

constructor TEditorCliente.Create;
begin
     FEditor := TEditorDResumen.Create;
end;

destructor TEditorCliente.Destroy;
begin
     FreeAndNil(FEditor);
end;

procedure TEditorCliente.Pos_Inicio(oRichText: TcxRichedit);
begin
     { Se utiliza un insert y un delete para posicionar al RTF
       al principio despues de escribir los campos             }
     With oRichText.Lines do
     begin
          Insert( 0, VACIO );
          Delete( 0 );
     end;
end;

{ TEditorDResumen }
procedure TEditorDResumen.AgregarLinea( Const Texto:WideString);
begin
     with fERichText.Lines do
     begin
          Add(Texto);
     end;
end;


procedure TEditorDResumen.Reset;
begin
     fERichText.Clear;
end;

procedure TEditorDResumen.SetAlineaLetra(Const Value: TAlignment);
begin
     fAlineaLetra := Value;
     fERichText.Paragraph.Alignment := fAlineaLetra;
end;

procedure TEditorDResumen.SetBullets(const Value: TNumberingStyle);
begin
     fBullets := Value;
     fERichText.Paragraph.Numbering := fBullets;
end;

procedure TEditorDResumen.SetColorLetra(const Value: TColor);
begin
     fColorLetra := Value;
     fERichText.SelAttributes.Color := fColorLetra;
end;

procedure TEditorDResumen.SetEstiloLetra(const Value: TFontStyles);
begin
     fEstiloLetra := Value;
     fERichText.SelAttributes.Style := fEstiloLetra;
end;

procedure TEditorDResumen.setIdent(const Value: Integer);
begin
     fIdent := Value;
     fERichText.Paragraph.FirstIndent := fIdent;
end;

procedure TEditorDResumen.SetTamano(const Value: Integer);
begin
     fTamano := Value;
     fERichText.SelAttributes.Size := fTamano;
end;

procedure TEditorDResumen.SetTipoLetra(const Value: String);
begin
     //fTipoLetra := Value;
     //fERichText.SelAttributes.Name := fTipoLetra;
end;

end.
