unit FClasificacionEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     FCamposMgr,
     ZBaseDlgModal;

type
  TEditClasificacion = class(TZetaDlgModal)
    CodigoLBL: TLabel;
    Codigo: TEdit;
    DescripcionLBL: TLabel;
    Descripcion: TEdit;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FClasificacion: TClasificacion;
    FAlta: Boolean;
    FClasificaciones: TListaClasificaciones;
    procedure SetClasificacion(const Value: TClasificacion);
    procedure SetEsAlta(const Value: Boolean);
  public
    { Public declarations }
    property Clasificacion: TClasificacion read FClasificacion write SetClasificacion;
    property EsAlta: Boolean write SetEsAlta;
    property Clasificaciones: TListaClasificaciones write FClasificaciones;
  end;

const
     K_MAX_LENGTH_DESCRIP = 50;

var
  EditClasificacion: TEditClasificacion;

implementation

uses DMedico,ZetaDialogo,ZetaCommonTools;

{$R *.DFM}

{ TFEditClasificacion }
procedure TEditClasificacion.FormCreate(Sender: TObject);
begin
  inherited;
    Descripcion.MaxLength := K_MAX_LENGTH_DESCRIP;
end;

procedure TEditClasificacion.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := Codigo;
end;

procedure TEditClasificacion.SetClasificacion(const Value: TClasificacion);
begin
     FClasificacion := Value;
     Codigo.Text := FClasificacion.Codigo;
     Descripcion.Text := FClasificacion.Nombre;
end;

procedure TEditClasificacion.SetEsAlta(const Value: Boolean);
begin
     if Value then
        Caption := 'Agregar Clasificaci�n'
     else
         Caption := 'Modificar Clasificaci�n';
     FAlta:=Value;    
end;

procedure TEditClasificacion.OKClick(Sender: TObject);
var
   lOk :Boolean;
   sCodigo,sNombre : String;
   oClasificacion : TClasificacion;
begin
     inherited;
     lOk := False;
     sCodigo := Codigo.Text;
     sNombre := Descripcion.Text;
     if StrVacio(sCodigo) then
     begin
          ZError(Caption,'C�digo No Puede Quedar Vac�o',0);
          ActiveControl := Codigo;
     end
     else if StrVacio(sNombre) then
     begin
          ZError(Caption,'La Descripci�n No Puede Quedar Vac�a',0);
          ActiveControl := Descripcion;
     end
     else
     begin
          if FAlta then
             oClasificacion := NIL
          else
              oClasificacion := FClasificacion;

          lOk := NOT FClasificaciones.Repetido( sCodigo, sNombre, oClasificacion );
          if NOT lOk then
          begin
               ZError(Caption,'Existe Otro Grupo Con Los Mismos Datos',0);
               ActiveControl := Codigo;
          end
     end;

     if lOk then
     begin
          FClasificacion.Codigo := Codigo.Text;
          FClasificacion.Nombre := Descripcion.Text;
          ModalResult := mrOk;
     end;
end;

end.
