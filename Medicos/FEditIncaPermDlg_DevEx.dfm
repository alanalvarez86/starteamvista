inherited EditIncaPermDlg_DevEx: TEditIncaPermDlg_DevEx
  Left = 435
  Top = 338
  Caption = 'Incapacidades/Permisos'
  ClientHeight = 150
  ClientWidth = 206
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 212
  ExplicitHeight = 178
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 114
    Width = 206
    ExplicitTop = 114
    ExplicitWidth = 206
    object Cancelar: TcxButton
      Left = 123
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Cancelar Cambios'
      Anchors = [akTop, akRight]
      Caption = '&Cancelar '
      OptionsImage.ImageIndex = 0
      OptionsImage.Images = cxImageList24_PanelBotones
      OptionsImage.NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = CancelarClick
    end
    object OK: TcxButton
      Left = 38
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Aceptar y Escribir Datos'
      Anchors = [akTop, akRight]
      Caption = '&OK          '
      ModalResult = 1
      OptionsImage.ImageIndex = 1
      OptionsImage.Images = cxImageList24_PanelBotones
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = OkClick
    end
  end
  object rgOperacion: TRadioGroup [1]
    Left = 37
    Top = 20
    Width = 137
    Height = 78
    Caption = ' Registrar: '
    ItemIndex = 0
    Items.Strings = (
      '&Incapacidad'
      '&Permiso')
    TabOrder = 1
    OnClick = rgOperacionClick
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
