unit FDiagnostico_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TDiagnostico_DevEx = class(TBaseGridLectura_DevEx)
    DA_CODIGO: TcxGridDBColumn;
    DA_NOMBRE: TcxGridDBColumn;
    DA_INGLES: TcxGridDBColumn;
    DA_NUMERO: TcxGridDBColumn;
    DA_TEXTO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Diagnostico_DevEx: TDiagnostico_DevEx;

implementation

uses
ZetaBuscador_DevEx,
DMedico, FAyudaContexto;

{$R *.DFM}

procedure TDiagnostico_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext := H00006_Catalogo_de_Diagnosticos;
end;

procedure TDiagnostico_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;



end;

procedure TDiagnostico_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsDiagnostico.Conectar;
          DataSource.DataSet:= cdsDiagnostico;
     end;
end;

procedure TDiagnostico_DevEx.Refresh;
begin
     dmMedico.cdsDiagnostico.Refrescar;
end;

procedure TDiagnostico_DevEx.Agregar;
begin
     dmMedico.cdsDiagnostico.Agregar;
end;

procedure TDiagnostico_DevEx.Borrar;
begin
     dmMedico.cdsDiagnostico.Borrar;
end;

procedure TDiagnostico_DevEx.Modificar;
begin
     dmMedico.cdsDiagnostico.Modificar;
end;

procedure TDiagnostico_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Diagn�stico', 'DA_CODIGO', dmMedico.cdsDiagnostico );
end;

end.
