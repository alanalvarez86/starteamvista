unit FMedicina_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TMedicina_DevEx = class(TBaseGridLectura_DevEx)
    ME_CODIGO: TcxGridDBColumn;
    ME_NOMBRE: TcxGridDBColumn;
    ME_MEDIDA: TcxGridDBColumn;
    ME_DESCRIP: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Medicina_DevEx: TMedicina_DevEx;

implementation

uses
Zetabuscador_DevEx,
DMedico, FAyudaContexto;

{$R *.DFM}

procedure TMedicina_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext := H00005_Catalogo_de_Medicinas;
end;

procedure TMedicina_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

end;

procedure TMedicina_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsMedicina.Conectar;
          DataSource.DataSet:= cdsMedicina;
     end;
end;

procedure TMedicina_DevEx.Refresh;
begin
     dmMedico.cdsMedicina.Refrescar;
end;

procedure TMedicina_DevEx.Agregar;
begin
     dmMedico.cdsMedicina.Agregar;
end;

procedure TMedicina_DevEx.Borrar;
begin
     dmMedico.cdsMedicina.Borrar;
end;

procedure TMedicina_DevEx.Modificar;
begin
     dmMedico.cdsMedicina.Modificar;
end;

procedure TMedicina_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Medicinas', 'ME_CODIGO', dmMedico.cdsMedicina );
end;



end.
