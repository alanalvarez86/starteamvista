unit FEditKAccidentes_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaKeyCombo, ZetaEdit, ZetaKeyLookup,
  ZetaHora, Mask, ZetaFecha, ExtCtrls, ComCtrls, Db, Buttons, ZetaDBTextBox,
  ZetaNumero, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxGroupBox, cxRadioGroup;

type
    TEditAccidentes_DevEx = class(TBaseEdicion_DevEx)
    PageControl1: TcxPageControl;
    TabSheet1: TcxTabSheet;
    GroupBox1: TGroupBox;
    TabSheet3: TcxTabSheet;
    Label15: TLabel;
    gbTestigo: TGroupBox;
    AX_INF_TES: TcxDBMemo;
    GroupBox9: TGroupBox;
    AX_INF_SUP: TcxDBMemo;
    Label17: TLabel;
    AX_TIPO: TZetaDBKeyCombo;
    TabSheet2: TcxTabSheet;
    Label5: TLabel;
    AX_TIP_LES: TZetaDBKeyCombo;
    AX_RIESGO: TcxDBRadioGroup;
    AX_INCAPA: TcxDBRadioGroup;
    AX_DAN_MAT: TcxDBRadioGroup;
    GroupBox3: TGroupBox;
    AX_DESCRIP: TcxDBMemo;
    lTipoAx: TLabel;
    AX_TIP_ACC: TZetaDBKeyLookup_DevEx;
    gbAccidente: TGroupBox;
    lax_fecha: TLabel;
    AX_FECHA: TZetaDBFecha;
    lAx_Hora: TLabel;
    AX_HORA: TZetaDBHora;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    AX_FEC_SUS: TZetaDBFecha;
    Label8: TLabel;
    AX_HOR_SUS: TZetaDBHora;
    GroupBox5: TGroupBox;
    Label10: TLabel;
    AX_FEC_COM: TZetaDBFecha;
    Label11: TLabel;
    AX_HOR_COM: TZetaDBHora;
    Label12: TLabel;
    AX_ATENDIO: TZetaDBKeyCombo;
    Label14: TLabel;
    Label9: TLabel;
    Label16: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    GroupBox6: TGroupBox;
    AX_INF_ACC: TcxDBMemo;
    Label13: TLabel;
    AX_NUM_TES: TZetaDBKeyLookup_DevEx;
    Label6: TLabel;
    AX_NUM_INC: TDBEdit;
    AX_PER_REG: TDBEdit;
    AX_NUMERO: TDBEdit;
    AX_OBSERVA: TDBEdit;
    lMotivoAx: TLabel;
    AX_CAUSA: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    AX_MOTIVO: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure AX_TIPOChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
 Protected
    { Protected declarations }
    procedure Connect; override;
    //procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditAccidentes_DevEx: TEditAccidentes_DevEx;

implementation

uses
  //ZetaBuscador
  DMedico, ZAccesosTress, ZetaCommonLists, DSistema, FAyudaContexto,
  DCliente, ZetaCommonTools, ZetaDialogo;

{$R *.DFM}

procedure TEditAccidentes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_EMP_KARDEX_ACCIDENTES;
     FirstControl := AX_TIPO;
     TipoValorActivo1 := stExpediente;
     HelpContext:= H00104_Accidentes;

     AX_CAUSA.LookUpDataSet := dmMedico.cdsCausaAcc;
     AX_MOTIVO.LookUpDataSet := dmMedico.cdsMotivoAcc;
     AX_TIP_ACC.LookUpDataset := dmMedico.cdsTAccidente;
end;

procedure TEditAccidentes_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmMedico do
     begin
          cdsCausaAcc.Conectar;
          cdsMotivoAcc.Conectar;
          cdsTAccidente.Conectar;
          DataSource.DataSet:= cdsKardexAccidente;
     end;
end;

procedure TEditAccidentes_DevEx.AX_TIPOChange(Sender: TObject);
begin
  inherited;
  gbAccidente.Caption := ' ' + AX_TIPO.Text + ' ';
end;

procedure TEditAccidentes_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 0;
end;

end.
