unit FEditKAccidentes;

interface

uses
  ///ZBaseEdicion
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaKeyCombo, ZetaEdit, ZetaKeyLookup,
  ZetaHora, Mask, ZetaFecha, ExtCtrls, ComCtrls, Db, Buttons, ZetaDBTextBox,
  ZetaNumero;

type
    //TEditAccidentes = class(TBaseEdicion)
    TEditAccidentes = class(TObject)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    TabSheet3: TTabSheet;
    Label15: TLabel;
    gbTestigo: TGroupBox;
    AX_INF_TES: TDBMemo;
    GroupBox9: TGroupBox;
    AX_INF_SUP: TDBMemo;
    Label17: TLabel;
    AX_TIPO: TZetaDBKeyCombo;
    TabSheet2: TTabSheet;
    Label5: TLabel;
    AX_TIP_LES: TZetaDBKeyCombo;
    AX_RIESGO: TDBRadioGroup;
    AX_INCAPA: TDBRadioGroup;
    AX_DAN_MAT: TDBRadioGroup;
    GroupBox3: TGroupBox;
    AX_DESCRIP: TDBMemo;
    lTipoAx: TLabel;
    AX_TIP_ACC: TZetaDBKeyLookup;
    gbAccidente: TGroupBox;
    lax_fecha: TLabel;
    AX_FECHA: TZetaDBFecha;
    lAx_Hora: TLabel;
    AX_HORA: TZetaDBHora;
    GroupBox4: TGroupBox;
    Label7: TLabel;
    AX_FEC_SUS: TZetaDBFecha;
    Label8: TLabel;
    AX_HOR_SUS: TZetaDBHora;
    GroupBox5: TGroupBox;
    Label10: TLabel;
    AX_FEC_COM: TZetaDBFecha;
    Label11: TLabel;
    AX_HOR_COM: TZetaDBHora;
    Label12: TLabel;
    AX_ATENDIO: TZetaDBKeyCombo;
    Label14: TLabel;
    Label9: TLabel;
    Label16: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    GroupBox6: TGroupBox;
    AX_INF_ACC: TDBMemo;
    Label13: TLabel;
    AX_NUM_TES: TZetaDBKeyLookup;
    Label6: TLabel;
    AX_NUM_INC: TDBEdit;
    AX_PER_REG: TDBEdit;
    AX_NUMERO: TDBEdit;
    AX_OBSERVA: TDBEdit;
    lMotivoAx: TLabel;
    AX_CAUSA: TZetaDBKeyLookup;
    Label1: TLabel;
    AX_MOTIVO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure AX_TIPOChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
 Protected
    { Protected declarations }
    procedure Connect; override;
    //procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditAccidentes: TEditAccidentes;

implementation

uses
  //ZetaBuscador
  DMedico, ZAccesosTress, ZetaCommonLists, DSistema, FAyudaContexto,
  DCliente, ZetaCommonTools, ZetaDialogo;

{$R *.DFM}

procedure TEditAccidentes.FormCreate(Sender: TObject);
begin
     inherited;
     {IndexDerechos := ZAccesosTress.D_EMP_KARDEX_ACCIDENTES;
     FirstControl := AX_TIPO;
     TipoValorActivo1 := stExpediente;
     HelpContext:= H00104_Accidentes;  }

     AX_CAUSA.LookUpDataSet := dmMedico.cdsCausaAcc;
     AX_MOTIVO.LookUpDataSet := dmMedico.cdsMotivoAcc;
     AX_TIP_ACC.LookUpDataset := dmMedico.cdsTAccidente;
end;

procedure TEditAccidentes.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmMedico do
     begin
          cdsCausaAcc.Conectar;
          cdsMotivoAcc.Conectar;
          cdsTAccidente.Conectar;
        //  DataSource.DataSet:= cdsKardexAccidente;
     end;
end;

procedure TEditAccidentes.AX_TIPOChange(Sender: TObject);
begin
  inherited;
  gbAccidente.Caption := ' ' + AX_TIPO.Text + ' ';
end;

procedure TEditAccidentes.FormShow(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 0;
end;

end.
