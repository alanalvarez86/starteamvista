unit FTablas_DevEx;

interface
uses ZBaseTablasConsulta_DevEx, ZBaseGridLectura_DevEx, cxCustomData;

{type
 TTOTConsulta_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
  {  procedure AfterCreate; override;
  end;

//MA:Clase agregada por mi
type
    TTOTEstudio_DevEx = class (TTablaLookup)
    protected
    Protected declarations
    procedure AfterCreate;override;
  end;
     }
type
    TTOTAccidente_DevEx = class (TTablaLookup)
    protected
    {Protected declarations}
    procedure AfterCreate;override;
  end;

type
    TTOTCausaAcc_DevEx = class (TTablaLookup)
    protected
    {Protected declarations}
    procedure AfterCreate;override;
  end;

    TTOMotivoAcc_DevEx = class (TTablaLookup)
    protected
    {Protected declarations}
    procedure AfterCreate;override;
  end;


implementation

uses DMedico, ZetaCXGrid, FAyudaContexto;


{TTOTAccidente}
procedure TTOTAccidente_DevEx.AfterCreate;
begin
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited AfterCreate;
     LookupDataset := dmMedico.cdsTAccidente;
     HelpContext := H00102_Catalogo_de_Tipos_de_Accidente;
     ApplyMinWidth;
     Agrega_Tipo;
end;

{TTOTCausaAcc}
procedure TTOTCausaAcc_DevEx.AfterCreate;
begin
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited AfterCreate;
     LookupDataset := dmMedico.cdsCausaAcc;
     HelpContext := H00109_Catalogo_de_Causas_de_accidente;
     Agrega_Tipo;
end;

{ TTOTMotivoAcc }

procedure TTOMotivoAcc_DevEx.AfterCreate;
begin
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     inherited AfterCreate;
     LookupDataset := dmMedico.cdsMotivoAcc;
     HelpContext := H00103_Catalogo_de_motivos_de_accidente;
     Agrega_Tipo;
end;

end.
