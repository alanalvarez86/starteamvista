inherited Consultas_DevEx: TConsultas_DevEx
  Left = 319
  Top = 168
  Caption = 'Consultas M'#233'dicas'
  ClientHeight = 372
  ClientWidth = 619
  ExplicitWidth = 619
  ExplicitHeight = 372
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 619
    ExplicitWidth = 619
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 360
      ExplicitWidth = 360
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 354
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 619
    Height = 353
    ExplicitWidth = 619
    ExplicitHeight = 353
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object CN_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'CN_FECHA'
        Options.Grouping = False
      end
      object CN_HOR_INI: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'CN_HOR_INI'
        Options.Grouping = False
      end
      object CN_HOR_FIN: TcxGridDBColumn
        Caption = 'Fin'
        DataBinding.FieldName = 'CN_HOR_FIN'
        Options.Grouping = False
      end
      object CN_TIPO_CONS: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CN_TIPO_CONS'
      end
      object CN_MOTIVO: TcxGridDBColumn
        Caption = 'Motivo'
        DataBinding.FieldName = 'CN_MOTIVO'
        Options.Grouping = False
      end
      object DA_DESCRIP: TcxGridDBColumn
        Caption = 'Diagn'#243'stico'
        DataBinding.FieldName = 'DA_DESCRIP'
      end
      object CN_IMSS: TcxGridDBColumn
        Caption = #191' Envi'#243' IMSS ?'
        DataBinding.FieldName = 'CN_IMSS'
        Options.Grouping = False
        Width = 90
      end
      object CN_SUB_SEC: TcxGridDBColumn
        Caption = #191' Subsecuente ?'
        DataBinding.FieldName = 'CN_SUB_SEC'
        Options.Grouping = False
      end
      object US_NOMBRE: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_NOMBRE'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
