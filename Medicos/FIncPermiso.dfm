inherited fIncpermisos_DevEx: TfIncpermisos_DevEx
  Left = 251
  Top = 253
  Caption = 'Incapacidades/Permisos'
  PixelsPerInch = 96
  TextHeight = 13
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 429
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'IN_FEC_INI'
        Title.Caption = 'Fecha'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_DIAS'
        Title.Caption = 'D'#237'as'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_FEC_FIN'
        Title.Caption = 'Regresa'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_CLASIFI'
        Title.Caption = 'Clase'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Tipo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_NUMERO'
        Title.Caption = 'N'#250'mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_COMENTA'
        Title.Caption = 'Observaciones'
        Visible = True
      end>
  end
end
