unit FEditConsultaGlobal;

interface

uses
  //ZBaseGridEdicion

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ZetaCommonClasses, ExtCtrls, StdCtrls, ZetaKeyLookup, ZetaHora, Mask,
  ZetaFecha, ZetaEdit;

type
//  TEditConsultaGlobal = class(TBaseGridEdicion)
  TEditConsultaGlobal = class(TObject)
    Panel1: TPanel;
    Label1: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CN_FECHA: TZetaFecha;
    CN_HOR_INI: TZetaHora;
    CN_HOR_FIN: TZetaHora;
    CN_TIPO: TZetaKeyLookup;
    CN_MOTIVO: TZetaEdit;
    CN_OBSERVA: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
//    procedure FormDestroy(Sender: TObject);
  private
   { Private declarations }
    FParamList: TZetaParams;
    //procedure BuscaExpediente;
  protected
   // procedure Connect; override;
    //procedure Buscar; override;
    //function Editing: Boolean; override;
    //procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditConsultaGlobal: TEditConsultaGlobal;

implementation
uses DMedico,
     FBuscaExpedientes,
     //ZetaBuscaEmpleado,
     ZetaCommonTools,
     ZAccesosTress,
     FAyudaContexto;

{$R *.DFM}

{ TEditConsultaGlobal }
procedure TEditConsultaGlobal.FormCreate(Sender: TObject);
begin
  inherited;
  {HelpContext := H00107_Registro_de_Consultas_Globales;
  FParamList := TZetaParams.Create;
  CN_TIPO.LookupDataset := dmMedico.cdsTConsulta;}
end;

procedure TEditConsultaGlobal.FormShow(Sender: TObject);
begin
     inherited;
{     Self.ActiveControl := CN_FECHA;
     with dmMedico do
     begin
          CN_FECHA.Valor := DATE;
          CN_HOR_INI.Valor := FormatDateTime( 'hhmm', Now );
          CN_MOTIVO.Text := VACIO;
          CN_OBSERVA.Text := VACIO;
     end;     }
end;
{
procedure TEditConsultaGlobal.Connect;
begin
     with dmMedico do
     begin
          cdsCodigos.Conectar;
          {CV: Se manda llamar el metodo Refrescar enves del Conectar porque necesitamos
          que siempre este vac�o. El conectar si esta abierto, no hace nada y el dataset se
          quedar�a con lo valores anteriores.}
 {         cdsConsultaGlobal.Refrescar;
          DataSource.DataSet:= cdsConsultaGlobal;
     end;
end;


procedure TEditConsultaGlobal.BuscaExpediente;
var
   iExpediente, iEmpleado: Integer;
   sDescripcion: String;
begin
     if FBuscaExpedientes.BuscaExpedienteDialogo( VACIO, iExpediente, iEmpleado, sDescripcion, TRUE ) then
     begin
          with dmMedico.cdsConsultaGlobal do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'EX_CODIGO' ).AsInteger := iExpediente;
          end;
     end;
end;

procedure TEditConsultaGlobal.Buscar;
begin
  inherited;
  with ZetaDBGrid.SelectedField do
  begin
       if ( FieldName = 'EX_CODIGO' )or(FieldName = 'CB_CODIGO') then
          BuscaExpediente;
  end;
end;

procedure TEditConsultaGlobal.EscribirCambios;
begin
     {Se utiliza DatabaseError para poder levantar una excepci�n y que la
      forma no se cierra al momento de darle Ok, si alguna validaci�n
      se ejecuta}

     {No se requiere el INHERITED porque aqui se esta mandando llamar directamente
     el metodo que graba la informacion.}
   {  if (CN_FECHA.Valor = NULLDATETIME) then
     begin
          CN_FECHA.SetFocus;
          DatabaseError('La Fecha De Consulta No Puede Quedar Vac�a');
     end
     else if strVacio(CN_TIPO.Llave) then
          begin
               CN_TIPO.SetFocus;
               DatabaseError('El Tipo De Consulta No Puede Quedar Vac�o');
          end
          else if strVacio(CN_MOTIVO.Valor) then
               begin
                    CN_MOTIVO.SetFocus;
                    DatabaseError('El Motivo De La Consulta No Puede Quedar Vac�o');
               end
               else
               begin
                    with FParamList do
                    begin
                         AddDate( 'CN_FECHA', CN_FECHA.Valor );
                         AddString( 'CN_HOR_INI', CN_HOR_INI.Valor );
                         AddString( 'CN_HOR_FIN', CN_HOR_FIN.Valor );
                         AddString( 'CN_TIPO', CN_TIPO.Llave );
                         AddString( 'CN_MOTIVO', CN_MOTIVO.Valor );
                         AddString( 'CN_OBSERVA', CN_OBSERVA.Text );
                         AddInteger( 'US_CODIGO', dmMedico.cdsConsultaGlobal.FieldByName('US_CODIGO').AsInteger );
                         dmMedico.EscribeConsultaGlobal( VarValues );
                    end;
               end;
end;

procedure TEditConsultaGlobal.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil( FParamList );
end;  }

end.
