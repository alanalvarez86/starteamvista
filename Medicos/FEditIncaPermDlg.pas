unit FEditIncaPermDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, ExtCtrls, Buttons;

type
  TEditIncaPermDlg = class(TZetaDlgModal)
    rgOperacion: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure rgOperacionClick(Sender: TObject);
  private
    { Private declarations }
    FOperacion: integer;
    procedure GetOperacion;
  public
    { Public declarations }
    property Operacion: integer read FOperacion;
  end;

var
  EditIncaPermDlg: TEditIncaPermDlg;

implementation

{$R *.DFM}

procedure TEditIncaPermDlg.FormShow(Sender: TObject);
begin
     inherited;
     GetOperacion;
end;

procedure TEditIncaPermDlg.GetOperacion;
begin
     FOperacion := rgOperacion.ItemIndex;
end;

procedure TEditIncaPermDlg.OKClick(Sender: TObject);
begin
     inherited;
     GetOperacion;
end;

procedure TEditIncaPermDlg.rgOperacionClick(Sender: TObject);
begin
     inherited;
     GetOperacion;
end;

end.
