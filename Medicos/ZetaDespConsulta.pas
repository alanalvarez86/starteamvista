unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcExpediente,
                     efcContratacion,
                     efcConsMedica,
                     efcEmbarazo,
                     efcAxEnfTrabajo,
                     efcIncapacidades,
                     efcPermisos,
                     efcRegistro,
                     efcProcesos,
                     efcRegConsMedInd,
                     efcRegConsMedxList,
                     efcRegVacunaInd,
                     efcRegVacunaxLista,
                     efcRegEstdLabInd,
                     efcRegEstdLabxList,
                     efcRegIncapac,
                     efcRegPermiso,
                     efcProConsMedGlobal,
                     efcProVacunaGlobal,
                     efcProEstdLabGlobal,
                     efcDepuracion,
                     efcReportes,
                     efcSistProcesos,
                     efcQueryGral,
                     efcMedicamentos,
                     efcDiagnosticos,
                     efcTipoConsulta,
                     efcTipoEstdLab,
                     efcTipoAcc,
                     efcCauAcc,
                     efcMotAcc,
                     efcMedicinaEntr,
                     efcSistGlobales,
                     efcSistBitacora );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     FReportes_DevEx,
     FSistProcesos_DevEx,
     FQueryGral_DevEx,
     FTablas_DevEx,
     FTablas2_DevEx,
     FMedicina_DevEx,
     FDiagnostico_DevEx,
     FExpediente_DevEx,
     FKardexConsultas_DevEx,
     FKardexEmbarazos_DevEx,
     FKardexAccidente_DevEx,
     FMedicinaEntr_DevEx,
     FGlobalesMedico,
     FRegistros,
     FIncPermiso_DevEx;


function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     case Forma of
          efcReportes : Result := Consulta( D_REPORTES_MEDICOS, TReportes_DevEx );

          efcTipoConsulta : Result := Consulta( D_CAT_TIPO_CONSULTA, TTOTConsulta_DevEx );
          efcTipoEstdLab  : Result := Consulta( D_CAT_TIPO_EST_LAB, TTOTEstudio_DevEx );
          efcTipoAcc      : Result := Consulta( D_CAT_TIPO_ACC, TTOTAccidente_DevEx);
          efcCauAcc       : Result := Consulta( D_CAT_CAU_ACC, TTOTCausaAcc_DevEx );
          efcMotAcc       : Result := Consulta( D_CAT_MOT_ACC, TTOMotivoAcc_DevEx );

          efcSistProcesos : Result := Consulta( D_CONSULTA_PROCESOS, TSistProcesos_DevEx );
          efcQueryGral    : Result := Consulta( D_CONSULTA_SQL, TQueryGral_DevEx );

          efcMedicamentos : Result := Consulta( D_CAT_MEDICAMENTOS, TMedicina_DevEx );
          efcDiagnosticos : Result := Consulta( D_CAT_DIAGNOSTICO, TDiagnostico_DevEx );
          efcExpediente   : Result := Consulta( D_EMP_SERV_EXPEDIENTE, TExpediente_DevEx );
          efcConsMedica   : Result := Consulta( D_EMP_KARDEX_CONSULTAS, TConsultas_DevEx );
          efcEmbarazo     : Result := Consulta( D_EMP_KARDEX_EMBARAZOS, TKardexEmbarazo_DevEx );
          efcAxEnfTrabajo : Result := Consulta( D_EMP_KARDEX_ACCIDENTES, TKardexAccidente_DevEx );
          efcMedicinaEntr : Result := Consulta( D_MEDICINA_ENTR, TMedicinaEntr_DevEx );
          efcSistGlobales : Result := Consulta( D_SERV_GLOBALES, TGlobalesMedico );
          efcIncapacidades: Result := Consulta( D_EMP_INCAPACIDADES, TfIncpermisos_DevEx );
          else Result := Consulta( 0, nil );
     end;
end;

end.
