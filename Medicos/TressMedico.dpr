program TressMedico;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  Forms,
  ZetaClientTools,
  MidasLib,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZReportTools in '..\Reportes\ZReportTools.pas',
  DGlobal in '..\DataModules\DGlobal.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DDiccionario in 'DDiccionario.pas' {dmDiccionario: TDataModule},
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  DTablas in 'DTablas.pas' {dmTablas: TDataModule},
  ZetaDespConsulta in 'ZetaDespConsulta.pas',
  ZArbolTress in 'ZArbolTress.pas',
  ZBaseThreads in '..\Tools\ZBaseThreads.pas',
  DConsultas in '..\DataModules\DConsultas.pas' {dmConsultas: TDataModule},
  DCatalogos in 'DCatalogos.pas' {dmCatalogos: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  DSistema in 'DSistema.pas' {dmSistema: TDataModule},
  //ZBaseArbolShell in '..\Tools\ZBaseArbolShell.pas' {BaseArbolShell},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  dBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  FTressShell in 'FTressShell.pas' {TressShell},
  DMedico in 'DMedico.pas' {dmMedico: TDataModule},
  FTablas_DevEx in 'FTablas_DevEx.pas',
  FEditTablas2_DevEx in 'FEditTablas2_DevEx.pas' {EditTablas2},
  Ftablas2_DevEx in 'Ftablas2_DevEx.pas',
  FAyudaContexto in 'FAyudaContexto.pas',
  DConfigPoliza in '..\DataModules\DConfigPoliza.pas' {dmConfigPoliza: TDataModule},
  DExportEngine in '..\DataModules\DExportEngine.pas' {dmExportEngine: TDataModule},
  dBaseTressDiccionario in '..\datamodules\dBaseTressDiccionario.pas' {dmBaseTressDiccionario: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  ZBasicoNavBarShell in '..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  ZBaseNavBarShell in '..\Tools\ZBaseNavBarShell.pas' {BaseNavBarShell};

{$R *.RES}
{$R WindowsXP.res}

{$R ..\..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;

  Application.Title := 'Servicio M�dico';
  Application.HelpFile := 'TressMedico.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin

            Free;
       end;
  end;

end.
