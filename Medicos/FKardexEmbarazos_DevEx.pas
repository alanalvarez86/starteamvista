unit FKardexEmbarazos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid;

type
  TKardexEmbarazo_DevEx = class(TBaseGridLectura_DevEx)
    EM_FEC_UM: TcxGridDBColumn;
    EM_FEC_PP: TcxGridDBColumn;
    EM_PRENAT: TcxGridDBColumn;
    EM_POSNAT: TcxGridDBColumn;
    EM_INC_INI: TcxGridDBColumn;
    EM_INC_FIN: TcxGridDBColumn;
    EM_COMENTA: TcxGridDBColumn;
    US_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  KardexEmbarazo_DevEx: TKardexEmbarazo_DevEx;

implementation

uses
ZetaBuscador_DevEx,
DMedico, ZetaCommonLists, FAyudaContexto;

{$R *.DFM}

procedure TKardexEmbarazo_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
 // CanLookup := True;
  HelpContext := H00105_Embarazos;
  TipoValorActivo1 := stExpediente;
end;

procedure TKardexEmbarazo_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;


end;

procedure TKardexEmbarazo_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsKardexEmbarazo.Conectar;
          DataSource.DataSet:= cdsKardexEmbarazo;
     end;
end;

procedure TKardexEmbarazo_DevEx.Refresh;
begin
     dmMedico.cdsKardexEmbarazo.Refrescar;
end;

procedure TKardexEmbarazo_DevEx.Agregar;
begin
     dmMedico.cdsKardexEmbarazo.Agregar;
end;

procedure TKardexEmbarazo_DevEx.Borrar;
begin
     dmMedico.cdsKardexEmbarazo.Borrar;
end;

procedure TKardexEmbarazo_DevEx.Modificar;
begin
     dmMedico.cdsKardexEmbarazo.Modificar;
end;

procedure TKardexEmbarazo_DevEx.DoLookup;
begin
    { inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Embarazos', 'EX_CODIGO', dmMedico.cdsKardexEmbarazo );}
end;

{Se requiere el PuedeAgregar para que no agregue
registros cuando la base de datos esta vacia.}
function TKardexEmbarazo_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsKardexEmbarazo.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;


end.
