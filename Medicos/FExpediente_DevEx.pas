unit FExpediente_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, ImgList, ZetaDBTextBox, StdCtrls, ComCtrls, ToolWin,
  Grids, DBGrids, Db, ExtCtrls, Menus, ZetaDBGrid, ZetaMessages, EditorDResumen,
  Vcl.Mask, Vcl.DBCtrls, imageenview, ieview, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  TressMorado2013, cxTextEdit, cxMemo, cxRichEdit, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;


type
  TImageEnDBView = class(TImageEnView);
  ePintaRtf = ( epConsultas, epExpediente );
  TExpediente_DevEx = class(TBaseConsulta)
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    gbExpediente: TGroupBox;
    gbConsulta: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    tbImpresion: TToolButton;
    PopupMenu1: TPopupMenu;
    popExpediente: TMenuItem;
    popConsultas: TMenuItem;
    ToolButton1: TToolButton;
    dsKardexConsultas: TDataSource;
    Splitter7: TSplitter;
    reConsulta: TcxRichEdit;
    dsDatosEmpleado: TDataSource;
    Splitter5: TSplitter;
    reExpediente: TcxRichEdit;
    FOTO: TImageEnView;
    Label1: TLabel;
    CB_FEC_ING: TDBEdit;
    Label2: TLabel;
    CONTRATO: TDBEdit;
    Label3: TLabel;
    PU_DESCRIP: TDBEdit;
    Label4: TLabel;
    CLASIFICACION: TDBEdit;
    Label6: TLabel;
    TU_DESCRIP: TDBEdit;
    Label5: TLabel;
    ESTADOCIVIL: TDBEdit;
    zdbGridDBTableView1: TcxGridDBTableView;
    zdbGridLevel1: TcxGridLevel;
    zdbGrid: TZetaCXGrid;
    CN_FECHA: TcxGridDBColumn;
    CN_TIPO_CONS: TcxGridDBColumn;
    DA_DESCRIP: TcxGridDBColumn;
    CN_SUB_SEC: TcxGridDBColumn;
    CN_IMSS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure btnExpedienteClick(Sender: TObject);
    procedure Splitter7Moved(Sender: TObject);
    procedure Splitter6Moved(Sender: TObject);
    procedure reExpedienteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure reConsultaMouseUp(Sender: TObject;Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure popExpedienteClick(Sender: TObject);
    procedure popConsultasClick(Sender: TObject);
    procedure tbImpresionClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure dsKardexConsultasDataChange(Sender: TObject; Field: TField);
    procedure zdbGridDBTableView1DblClick(Sender: TObject);
    procedure zdbGridDBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure zdbGridDBTableView1ColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure zdbGridDBTableView1DataControllerSortingChanged(Sender: TObject);
    procedure zdbGridDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure zdbGridDBTableView1DataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);

  private
    FInicial : TDateTime;
    FIntervalo : TDateTime;
    FEditor : TEditorCliente;
    procedure PintaRTFS(const eRtf: ePintaRtf);
    procedure NOExpediente;
    procedure NOConsulta;
    procedure PintaConsulta;
    procedure PintaExpediente;
    procedure SinDatos( const sTipoRtf: String; oRtf: TcxRichEdit );
    procedure HabilitaControles(const lHabilita: Boolean);
    function esEmpleado:boolean;
    procedure ApplyMinWidth;

    procedure SendExaminar( const eSender: TExaminador );
    function GetExpedienteActual:integer;
    procedure SetExpedienteActual(exp:integer);
    procedure EditaConsulta;




  protected
    { Protected declarations }
    AColumn: TcxGridDBColumn;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function PuedeAgregar(var sMensaje: String): Boolean; override;

  public
    { Public declarations }
    procedure MuestraDatosEmpleado(status:boolean);
    //procedure DoLookup; override;
    property Consultas:TcxRichEdit read reConsulta;
    property Expediente:TcxRichEdit read reExpediente;
    property ExpedienteActual:integer read GetExpedienteActual write SetExpedienteActual;




  end;

var
  Expediente_DevEx: TExpediente_DevEx;

implementation

uses
//     ZetaBuscador,
     DMedico,
     ZetaCommonLists,
     ZAccesosMgr,
     ZetaCommonClasses,
     FAyudaContexto,
     ZAccesosTress,
     ZetaDialogo,
     FTressShell,
     dCliente,
     ZetaClientDataSet,
     FToolsImageEn,
     ZGridModeTools;

{$R *.DFM}

procedure TExpediente_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     zdbGridDBTableView1.DataController.DataModeController.GridMode:= True;
     zdbGridDBTableView1.DataController.Filter.AutoDataSetFilter := TRUE;
     HelpContext := H00002_Expediente;
     FInicial := Now;
     if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CONSULTA ) then
     begin
          gbConsulta.Align := alClient;
          splitter7.Align := alLeft;
          gbExpediente.Align := alLeft;
          FIntervalo := EncodeTime(0,0,0,Windows.GetDoubleClickTime);
          FEditor := TEditorCliente.Create;
     end
     else
     begin
          gbConsulta.Visible := False;
          splitter7.Align := alRight;
          gbExpediente.Align := alClient;
     end;
     popConsultas.Enabled :=  Revisa( D_EMP_KARDEX_CONSULTAS );
     popExpediente.Enabled := Revisa( D_EMP_SERV_EXPEDIENTE );
end;

procedure TExpediente_DevEx.Connect;
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CONSULTA ) then
     begin
          gbConsulta.Align := alClient;
          splitter7.Align := alLeft;
          gbExpediente.Align := alLeft;
          FIntervalo := EncodeTime(0,0,0,Windows.GetDoubleClickTime);
          FEditor := TEditorCliente.Create;
     end
     else
     begin
          gbConsulta.Visible := False;
          splitter7.Align := alRight;
          gbExpediente.Align := alClient;
     end;

     with dmMedico do
     begin
          cdsExpediente.Conectar;
          DataSource.DataSet:= cdsExpediente;
          if not( cdsKardexConsultas.State IN [ dsEdit, dsInsert ] ) then
            dsKardexConsultas.DataSet:=cdsKardexConsultas;
          if esEmpleado then begin
            cdsDatosEmpleado.Conectar;
            dsDatosEmpleado.DataSet := cdsDatosEmpleado;
          end;
          if not( cdsExpediente.State IN [ dsEdit, dsInsert ] ) then
             PintaRTFs( epExpediente );
          if ((esEmpleado) and (FOTO.Visible)) and (not( cdsExpediente.State = dsInsert )) then begin     // Navegando
            cdsDatosEmpleado.refrescar;
            FToolsImageEn.AsignaBlobAImagen( FOTO, cdsDatosEmpleado, 'FOTOGRAFIA' );
        end;

        if not( cdsKardexConsultas.State IN [ dsEdit, dsInsert ] ) then begin
             PintaRTFs( epConsultas );
        end;
     end;
end;

procedure TExpediente_DevEx.Refresh;
begin
     With dmMedico do begin
          cdsExpediente.Refrescar;
          cdsDatosEmpleado.Refrescar;
          FOTO.BorderStyle := bsSingle;

     end;
end;

procedure TExpediente_DevEx.Agregar;
begin
     dmMedico.cdsExpediente.Agregar;
end;

function TExpediente_DevEx.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     {if Result then
        Result := dmMedico.CheckAuto( sMensaje );}
end;

procedure TExpediente_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  zdbGridDBTableView1 do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TExpediente_DevEx.Borrar;
begin
     dmMedico.cdsExpediente.Borrar;
end;

procedure TExpediente_DevEx.Modificar;
begin
     dmMedico.cdsExpediente.Modificar;
end;

procedure TExpediente_DevEx.MuestraDatosEmpleado(status: boolean);
begin
    Splitter5.Visible := status;
    label1.Visible := status;
    label2.Visible := status;
    label3.Visible := status;
    label4.Visible := status;
    label5.Visible := status;
    label6.Visible := status;

    Foto.Visible := status;
    CB_FEC_ING.Visible := status;
    CONTRATO.Visible := status;
    PU_DESCRIP.Visible := status;
    CLASIFICACION.Visible := status;
    TU_DESCRIP.Visible := status;
    ESTADOCIVIL.Visible := status;
end;

function TExpediente_DevEx.esEmpleado: boolean;
begin
    Result := ( eExTipo( dmCliente.cdsExpediente.FieldByName( 'EX_TIPO' ).AsInteger ) = exEmpleado );
end;

procedure TExpediente_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with dmMedico do
     begin
          if EsEmpleado then
            MuestraDatosEmpleado(true)
          else
            MuestraDatosEmpleado(false);
     end;
end;

procedure TExpediente_DevEx.dsKardexConsultasDataChange(Sender: TObject;
  Field: TField);
begin
  with dmMedico.cdsKardexConsultas do
     begin
          if not( State IN [ dsEdit, dsInsert ] ) then
             PintaRTFs( epConsultas );
     end;
end;

procedure TExpediente_DevEx.EditaConsulta;
var
   dAhora : TDateTime;
begin
  inherited;
     if dmMedico.cdsKardexConsultas.RecordCount <= 0 then
        ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Hay Consultas Para Modificar', 0 );
     dAhora := Now;
     {if ( ( dAhora - FInicial ) <= FIntervalo ) then
     begin}
          if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CAMBIO ) then
          begin
               With dmMedico.cdsKardexConsultas do
               begin
                    if dmMedico.ExisteExpediente then
                    begin
                       //MA: Linea para que al dar click sobre el grid de consultas no aparezca sin datos al agregar una cons.nueva
                         if ( FieldByName('EX_CODIGO').AsInteger <> 0 ) then
                             Modificar
                         else
                             Agregar;
                    end;
               end;
          end
          else
              ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Consultas', 0 );
      //end;
     Finicial := dAhora;
end;

procedure TExpediente_DevEx.btnExpedienteClick(Sender: TObject);
begin
      inherited;
      With dmMedico do
      begin
           with TressShell do
           begin
                if ExisteExpediente then
                     _E_Modificar.Execute
                else
                     _E_Agregar.Execute;
           end;
      end;//With
end;


procedure TExpediente_DevEx.WMExaminar(var Message: TMessage);
begin
     //MA: Linea para que al dar click sobre el grid de consultas no aparezca sin datos al agregar una cons.nueva
     if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CAMBIO ) then
     begin
          With dmMedico.cdsKardexConsultas do
          begin
               if ( FieldByName('EX_CODIGO').AsInteger <> 0 ) then
                   Modificar
               else
                   Agregar;
          end;
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Consultas', 0 );
end;

procedure TExpediente_DevEx.zdbGridDBTableView1ColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  if zdbGridDBTableView1.DataController.DataModeController.GridMode then
  begin
      inherited;
      Self.AColumn := TcxGridDBColumn(AColumn);
  end;
end;

procedure TExpediente_DevEx.zdbGridDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
  inherited;
 inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if zdbGridDBTableView1.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( zdbGridDBTableView1, AItemIndex, AValueList );
end;

procedure TExpediente_DevEx.zdbGridDBTableView1DataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  if zdbGridDBTableView1.DataController.DataModeController.GridMode then
  begin
    inherited;
    ZDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
  end;
end;

procedure TExpediente_DevEx.zdbGridDBTableView1DataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
begin
     inherited;
     if zdbGridDBTableView1.DataController.IsGridMode and zdbGridDBTableView1.OptionsView.Footer then      // Hay columnas totalizadas
        ZGridModeTools.AsignaValorColumnaSummary( ASender );
end;

procedure TExpediente_DevEx.zdbGridDBTableView1DblClick(Sender: TObject);
begin
     EditaConsulta;
end;

procedure TExpediente_DevEx.zdbGridDBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then
    EditaConsulta;
end;

procedure TExpediente_DevEx.Splitter7Moved(Sender: TObject);
begin
     reExpediente.Repaint;
     reConsulta.Repaint;
end;

procedure TExpediente_DevEx.Splitter6Moved(Sender: TObject);
begin
     reExpediente.Repaint;
     reConsulta.Repaint;
end;

procedure TExpediente_DevEx.reExpedienteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   dAhora : TDateTime;
begin
  inherited;
       dAhora := Now;
       if ( ( dAhora - FInicial ) <= FIntervalo ) then
       begin
            if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_CAMBIO ) then
            begin
                 With dmMedico do
                 begin
                      if ExisteExpediente then
                         cdsExpediente.Modificar;
                 end;
            end
            else
                ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Expediente', 0 );
       end;
       Finicial := dAhora;
end;



procedure TExpediente_DevEx.reConsultaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
   dAhora : TDateTime;
begin
  inherited;
     dAhora := Now;
     if ( ( dAhora - FInicial ) <= FIntervalo ) then
     begin
          if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_CAMBIO ) then
          begin
               With dmMedico.cdsKardexConsultas do
               begin
                    if dmMedico.ExisteExpediente then
                    begin
                       //MA: Linea para que al dar click sobre el grid de consultas no aparezca sin datos al agregar una cons.nueva
                         if ( FieldByName('EX_CODIGO').AsInteger <> 0 ) then
                             Modificar
                         else
                             Agregar;
                    end;
               end;
          end
          else
              ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Consultas', 0 );
     end;
     Finicial := dAhora;
end;

procedure TExpediente_DevEx.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeAndNil( FEditor );
end;

procedure TExpediente_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth();
   {***(am):Trabaja CON GridMode***}
  if zdbGridDBTableView1.DataController.DataModeController.GridMode then
   // zdbGridDBTableView1.OptionsCustomize.ColumnFiltering := False;

  //Desactiva la posibilidad de agrupar
  zdbGridDBTableView1.OptionsCustomize.ColumnGrouping := False;
  //Esconde la caja de agrupamiento
  zdbGridDBTableView1.OptionsView.GroupByBox := False;
  //Para que nunca muestre el filterbox inferior
  zdbGridDBTableView1.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  zdbGridDBTableView1.FilterBox.CustomizeDialog := False;
  //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
  zdbGridDBTableView1.ApplyBestFit();
  ZGridModeTools.CreaColumnaSumatoria( zdbGridDBTableView1.Columns[0], 0, 'Total:', SkCount );
  inherited;
  tbImpresion.Visible:= false;

end;

function TExpediente_DevEx.GetExpedienteActual: integer;
begin
      result:=ExpedienteActual;
end;

procedure TExpediente_DevEx.HabilitaControles( const lHabilita: Boolean );
begin
     zdbGrid.Enabled := lHabilita;
     
end;

procedure TExpediente_DevEx.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
   tbImpresion.Down:=False;
end;


procedure TExpediente_DevEx.PintaRTFS(const eRtf: ePintaRtf);
begin
     Case eRtf of
          epConsultas :PintaConsulta;
          epExpediente:PintaExpediente;
     end;
end;

procedure TExpediente_DevEx.PintaExpediente;
 var lTieneExpediente: Boolean;
begin
     reExpediente.Properties.ReadOnly := false;
     try
        lTieneExpediente := dmMedico.ExisteExpediente;
        reExpediente.Properties.ScrollBars := ssNone;
        if lTieneExpediente then begin
           dmMedico.CreaResumenExpediente(reExpediente)
        end
        else
            NOExpediente;
        reExpediente.Properties.ScrollBars := ssVertical;

        HabilitaControles( lTieneExpediente );
     finally
        reExpediente.Properties.ReadOnly := true;
     end;
end;

procedure TExpediente_DevEx.PintaConsulta;
begin
     reConsulta.Properties.ReadOnly := false;
     try
        with dmMedico do
        begin
             if ExisteConsulta then
                CreaResumenConsulta(reConsulta)
             else
                 NOConsulta;
        end;
     finally
        reConsulta.Properties.ReadOnly := true;
     end;
end;

procedure TExpediente_DevEx.SendExaminar(const eSender: TExaminador);
var
   Msg: TMessage;
begin
     with Msg do
     begin
          Msg := WM_EXAMINAR;
          LParam := Ord( eSender );
          WParam := 0;
          Result := 0;
     end;
     Owner.Dispatch( Msg );

end;

procedure TExpediente_DevEx.SetExpedienteActual(exp:integer);
begin
    ExpedienteActual := exp;
end;

procedure TExpediente_DevEx.SinDatos( const sTipoRtf: String; oRtf: TcxRichEdit  );
begin
     with oRtf do
     begin
          Lines.BeginUpdate;
          try
             Clear;
             SelAttributes.Color := clRed;
             Paragraph.Alignment:=taCenter;
             SelAttributes.Style:=[fsUnderline,fsBold];
             Lines.Add( Format( '* El Empleado No Tiene %s *', [ sTipoRtf ] ) );
         finally
             Lines.EndUpdate;
         end;
     end;
end;

procedure TExpediente_DevEx.NOConsulta;
begin
     SinDatos( 'Consultas', reConsulta  );
end;

procedure TExpediente_DevEx.NOExpediente;
begin
     SinDatos( 'Expediente', reExpediente );
end;

procedure TExpediente_DevEx.popExpedienteClick(Sender: TObject);
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          reExpediente.Print('Imprimiendo Expediente');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir El Expediente', 0 );
end;

procedure TExpediente_DevEx.popConsultasClick(Sender: TObject);
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_KARDEX_CONSULTAS, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          reConsulta.Print('Imprimiendo Consultas');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir Consultas', 0 );
end;

procedure TExpediente_DevEx.tbImpresionClick(Sender: TObject);
begin
     if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          reExpediente.Print('Imprimiendo Expediente');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir El Expediente', 0 );     
end;


end.
