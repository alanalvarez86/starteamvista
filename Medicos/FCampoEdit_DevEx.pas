unit FCampoEdit_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     FCamposMgr,
     ZBaseDlgModal_DevEx, Mask, ZetaNumero, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, Vcl.ImgList,
  cxButtons;

type
  TCampoEdit_DevEx = class(TZetaDlgModal_DevEx)
    LetreroLBL: TLabel;
    Letrero: TEdit;
    ResumerLBL: TLabel;
    ComoMostrar: TComboBox;
    FormulaLBL: TLabel;
    FormulaTexto: TEdit;
    FormulaNumero: TZetaNumero;
    FormulaBool: TCheckBox;
    lblExcede: TLabel;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FCampo: TCampo;
    FExcedeAlto: Boolean;
    procedure SetCampo(const Value: TCampo);
    procedure SetExcedeAlto(const Value: Boolean);
  public
    { Public declarations }
    property Campo: TCampo read FCampo write SetCampo;
    property ExcedeAlto : Boolean read FExcedeAlto write SetExcedeAlto;
  end;

const
     K_MAX_LENGTH_DESCRIP = 30;
       
var
  CampoEdit_DevEx: TCampoEdit_DevEx;

implementation

uses ZetaCommonLists, ZetaCommonTools;//,FCamposCFG;
{$R *.DFM}

{ TCampoEdit }
procedure TCampoEdit_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Letrero.MaxLength := K_MAX_LENGTH_DESCRIP;
end;

procedure TCampoEdit_DevEx.FormShow(Sender: TObject);
begin
     ActiveControl := Letrero;
     inherited;
end;

procedure TCampoEdit_DevEx.SetCampo(const Value: TCampo);
begin
     FCampo := Value;
     Letrero.Text := FCampo.Letrero;
     with FormulaTexto do
     begin
          Visible := FCampo.Tipo = xctTexto;
          if Visible then
             Text := FCampo.ValorDefault;
     end;

     with FormulaNumero do
     begin
          Visible := FCampo.Tipo = xctNumero;
          if Visible then
             Valor := StrToReal(FCampo.ValorDefault);
     end;

     with FormulaBool do
     begin
          Visible := FCampo.Tipo in [xctBooleano,xctBoolStr];
          if Visible then
             FormulaBool.Checked := zStrToBool( FCampo.ValorDefault );
     end;

     with ComoMostrar do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  Clear;
                  Add( 'Siempre' );
                  Add( 'Nunca' );
                  case FCampo.Tipo of
                       xctTexto:
                       begin
                            Add( 'Cuando Tiene Datos' );
                            Add( 'Cuando Est� Vac�o' );
                       end;
                       xctNumero:
                       begin
                            Add( 'Cuando Es Diferente de Cero' );
                            Add( 'Cuando Es Cero' );
                       end;
                       xctBooleano, xctBoolStr:
                       begin
                            Add( 'Cuando Es Verdadero' );
                            Add( 'Cuando Es Falso' );
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := Ord( FCampo.Mostrar );
     end;
end;

procedure TCampoEdit_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     FCampo.Letrero := Letrero.Text;
     FCampo.Mostrar := eExMostrar( ComoMostrar.ItemIndex );
     case FCampo.Tipo of
          xctTexto: FCampo.ValorDefault := FormulaTexto.Text;
          xctNumero: FCampo.ValorDefault := FormulaNumero.ValorAsText;
          xctBooleano, xctBoolStr: FCampo.ValorDefault := zBoolToStr(FormulaBool.Checked);
     end;
end;

procedure TCampoEdit_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
     FCampo.Letrero := Letrero.Text;
     FCampo.Mostrar := eExMostrar( ComoMostrar.ItemIndex );
     case FCampo.Tipo of
          xctTexto: FCampo.ValorDefault := FormulaTexto.Text;
          xctNumero: FCampo.ValorDefault := FormulaNumero.ValorAsText;
          xctBooleano, xctBoolStr: FCampo.ValorDefault := zBoolToStr(FormulaBool.Checked);
     end;

end;

procedure TCampoEdit_DevEx.SetExcedeAlto(const Value: Boolean);
begin
     FExcedeAlto := Value;
     lblExcede.Visible := Value;

end;

end.
