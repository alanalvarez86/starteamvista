unit FIncPermiso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls;

type
  TfIncpermisos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;    
    function PuedeImprimir( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  fIncpermisos: TfIncpermisos;

implementation

uses DMedico,
//     ZetaBuscador,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     FEditIncaPermDlg,
     FAyudaContexto;

{$R *.DFM}

procedure TfIncpermisos.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext := H00106_Incapacidades_y_permisos;
end;

procedure TfIncpermisos.Connect;
begin
     with dmMedico do
     begin
          cdsHistorial.Conectar;
          cdsHisPermiso.Conectar;
          cdsHisIncapaci.Conectar;
          DataSource.DataSet:= cdsHistorial;
     end;
end;

procedure TfIncpermisos.Refresh;
begin
     dmMedico.cdsHistorial.Refrescar;
end;

procedure TfIncpermisos.Agregar;
var
   oForma: TEditIncaPermDlg;
   lDerechos: boolean;
   sMensaje: string;
begin
     if dmMedico.EsEmpleado then
     begin
          oForma := TEditIncaPermDlg.Create( Self );
          try
             with oForma do
             begin
                  ShowModal;
                  if ModalResult = mrOK then
                  begin
                       lDerechos := TRUE;
                       with dmMedico do
                       begin
                            case Operacion of
                                 0:
                                 begin
                                      TipoIncaPerm := Operacion;
                                      lDerechos := ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_ALTA );
                                      sMensaje := 'No tiene derecho para agregar incapacidades';
                                 end;
                                 1:
                                 begin
                                      TipoIncaPerm := Operacion;
                                      lDerechos := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_ALTA );
                                      sMensaje := 'No tiene derecho para agregar permisos';
                                 end;
                            end;
                            if lDerechos then
                               cdsHistorial.Agregar
                            else
                                ZetaDialogo.ZError( Caption, sMensaje, 0 );
                       end;
                  end;
             end;
          finally
                 FreeAndNil( oForma );
          end;
     end
     else
         ZetaDialogo.ZError( Caption, 'Solamente se pueden agregar registros a empleados', 0 );

end;

procedure TfIncpermisos.Borrar;
begin
     dmMedico.cdsHistorial.Borrar;
end;

procedure TfIncpermisos.Modificar;
begin
     dmMedico.cdsHistorial.Modificar;
end;

procedure TfIncpermisos.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', 'Incapacidades/Permisos', 'CB_CODIGO', dmMedico.cdsHistorial );
end;

function TfIncpermisos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               {
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
               }
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
              //end;
          end
          else
          begin
               if ( cdsHistorial.FieldByname( 'CB_CODIGO' ).AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;


function TfIncpermisos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     with dmMedico do
     begin
          if EsPermiso then
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_BAJA );
               if not Result then
                  sMensaje := 'No tiene derecho para borrar permisos';
          end
          else
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_BAJA );
               if not Result then
                  sMensaje := 'No tiene derecho para borrar incapacidades';
          end;
     end;
end;

function TfIncpermisos.PuedeModificar(var sMensaje: String): Boolean;
begin
     with dmMedico do
     begin
          if EsPermiso then
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_CAMBIO );
               if not Result then
                  sMensaje := 'No tiene derecho para modificar permisos';
          end
          else
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_CAMBIO );
               if not Result then
                  sMensaje := 'No tiene derecho para modificar incapacidades';
          end;
     end;
end;

function TfIncpermisos.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_IMPRESION ) and
               ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_IMPRESION );
     if ( not Result ) then
        sMensaje := 'No tiene derecho para imprimir registros de permisos e incapacidades';
end;

end.
