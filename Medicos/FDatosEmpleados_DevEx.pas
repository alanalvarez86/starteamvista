unit FDatosEmpleados_DevEx;

interface

uses
  ZBaseEdicion_DevEx, //TDMULTIP,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaDBTextBox, StdCtrls, Db, ExtCtrls, DBCtrls,
  Buttons, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  imageenview, ieview;

type
  TDatosEmpleado_DevEx = class(TBaseEdicion_DevEx)
    gbDatosEmpleado: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    TU_DESCRIP: TZetaDBTextBox;
    PU_DESCRIP: TZetaDBTextBox;
    TB_ELEMENT: TZetaDBTextBox;
    CONTRATO: TZetaDBTextBox;
    CB_FEC_ING: TZetaDBTextBox;
    TB_ELEMENT2: TZetaDBTextBox;
    Label17: TLabel;
    CB_CONTRAT: TZetaDBTextBox;
    CB_PUESTO: TZetaDBTextBox;
    CB_CLASIFI: TZetaDBTextBox;
    CB_TURNO: TZetaDBTextBox;
    CB_EDO_CIV: TZetaDBTextBox;
    FotoSwitch: TcxButton;
    FOTO: TImageEnView;
    procedure FotoSwitchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  DatosEmpleado_DevEx: TDatosEmpleado_DevEx;

implementation
uses FAyudaContexto,
     DMedico,
     ZetaCommonClasses,
     ZetaCommonLists,
     DCliente,
     FToolsImageEn;

{$R *.DFM}

{ TDatosEmpleado }

procedure TDatosEmpleado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H00002_Expediente;
end;

procedure TDatosEmpleado_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsDatosEmpleado.Refrescar;
          Datasource.DataSet:= cdsDatosEmpleado;

          FotoSwitch.Down := TRUE;
          FToolsImageEn.AsignaBlobAImagen( FOTO, cdsDatosEmpleado, 'FOTOGRAFIA' );
     end;
end;

procedure TDatosEmpleado_DevEx.FotoSwitchClick(Sender: TObject);
begin
     inherited;
     FOTO.Visible := FotoSwitch.Down;
end;

end.
