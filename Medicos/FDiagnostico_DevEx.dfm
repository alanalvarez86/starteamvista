inherited Diagnostico_DevEx: TDiagnostico_DevEx
  Left = 215
  Top = 453
  Caption = 'Diagn'#243'stico'
  ClientHeight = 272
  ClientWidth = 647
  ExplicitWidth = 647
  ExplicitHeight = 272
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 647
    ExplicitWidth = 647
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 388
      ExplicitWidth = 388
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 382
        ExplicitLeft = 659
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 647
    Height = 253
    ParentFont = False
    ExplicitLeft = 22
    ExplicitTop = 22
    ExplicitWidth = 647
    ExplicitHeight = 253
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object DA_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'DA_CODIGO'
      end
      object DA_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'DA_NOMBRE'
      end
      object DA_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'DA_INGLES'
      end
      object DA_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'DA_NUMERO'
      end
      object DA_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'DA_TEXTO'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
