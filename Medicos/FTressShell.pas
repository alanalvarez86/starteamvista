unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa principal de TressMedicos.exe     ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, Menus, ImgList, ActnList, StdCtrls, Mask, Buttons, ExtCtrls, DB,
     {$ifndef VER130}
     Variants,
     {$endif}     
     //ZBaseArbolShell,
     ZetaFecha,
     ZetaStateComboBox,
     ZetaSmartLists,
     ZBaseConsulta,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaDespConsulta,
     ZetaMessages,
     ZBaseShell,
     FExpediente_DevEx,
     ZetaNumero, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxStatusBar, dxRibbonStatusBar,
  TressMorado2013, System.Actions,
  cxStyles, cxClasses,dxRibbon, ZBaseNavBarShell,
  cxPCdxBarPopupMenu,
  cxLocalization, dxBar, dxNavBar, cxButtons, cxPC,
  ZBasicoNavBarShell, cxContainer, cxEdit, cxImage, dxGDIPlusClasses,
  dxNavBarBase, dxNavBarCollns, HtmlHelpViewer, cxButtonEdit, cxBarEditItem,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, ZetaCXStateComboBox, dxSkinsCore,
  dxSkinscxPCPainter, dxSkinsdxNavBarPainter, dxRibbonSkins,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinsForm, dxBarBuiltInMenu,
  dxRibbonCustomizationForm;

type
  TTressShell = class(TBaseNavBarShell)
    _Exp_Primero: TAction;
    _Exp_Anterior: TAction;
    _Exp_Siguiente: TAction;

    _Exp_Ultimo: TAction;
    _E_BuscarEmpleado: TAction;
    _E_CambiarEmpleado: TAction;
    _R_Consulta: TAction;
    _V_Empleados: TAction;
    _R_Embarazo: TAction;
    _R_Accidente: TAction;
    _R_Medicinas: TAction;
    _R_Cons_Global: TAction;
    _R_Incapacidades: TAction;
    _R_Permisos: TAction;
    TabMedicos: TdxRibbonTab;
    DevEx_BarManagerBar5: TdxBar;
    tabRegistro_btnConsultaMedica: TdxBarLargeButton;
    tabRegistro_btnEmbarazos: TdxBarLargeButton;
    tabRegistro_btnAccidentes: TdxBarLargeButton;
    tabRegistro_btnIncapacidades: TdxBarLargeButton;
    tabRegistro_btnPermisos: TdxBarLargeButton;
    tabRegistro_btnMedicinasEntregadas: TdxBarLargeButton;
    tabRegistro_btnConsultaMedicaGlobal: TdxBarLargeButton;
    Image24_StatusEmpleado: TcxImageList;
    Panel1: TPanel;
    EmpleadoPrettyName: TLabel;
    EmpleadoLBL: TLabel;
    EmpleadoNumeroCB: TcxStateComboBox;
    ImagenStatus_DevEx: TcxImage;
    Panel2: TPanel;
    Label1: TLabel;
    ExpedienteNumeroCB: TcxStateComboBox;
    EmpleadoPrimero: TcxButton;
    EmpleadoAnterior: TcxButton;
    EmpleadoSiguiente: TcxButton;
    EmpleadoUltimo: TcxButton;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    ImprimeConsulta: TdxBarButton;
    ImprimeExpediente: TdxBarButton;
    dxBarLargeButton8: TdxBarLargeButton;
    EOperaciones_btnImprimir: TdxBarSubItem;
    dxBarButton3: TdxBarButton;
    EmpleadoBuscaBtn_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
     procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure _V_EmpleadosExecute(Sender: TObject);
    procedure _E_CambiarEmpleadoExecute(Sender: TObject);
    procedure PGSuperiorChange(Sender: TObject);
    procedure _Exp_PrimeroExecute(Sender: TObject);
    procedure _Exp_AnteriorExecute(Sender: TObject);
    procedure _Exp_SiguienteExecute(Sender: TObject);
    procedure _Exp_UltimoExecute(Sender: TObject);
    procedure EmpleadoBuscaBtn_DevExClick(Sender: TObject);
    procedure ExpedienteNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    procedure ArchivoClick(Sender: TObject);
    procedure _R_ConsultaExecute(Sender: TObject);
    procedure _R_EmbarazoExecute(Sender: TObject);
    procedure _R_AccidenteExecute(Sender: TObject);
    procedure _R_MedicinasExecute(Sender: TObject);
    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure EmpleadoNumeroCBEnter(Sender: TObject);
    procedure _R_Cons_GlobalExecute(Sender: TObject);
    procedure _Exp_UltimoUpdate(Sender: TObject);
    procedure _Exp_PrimeroUpdate(Sender: TObject);
    procedure IncapacidadesPermisosExecute(Sender: TObject);
    procedure ImagenStatus_DevExDblClick(Sender: TObject);
    procedure LLenaOpcionesGrupos;
    procedure FormShow(Sender: TObject);
    procedure tabRegistro_btnConsultaMedicaGlobalClick(Sender: TObject);
    procedure ClientAreaPG_DevExChange(Sender: TObject);
    procedure popConsultasClick(Sender: TObject);
    procedure ImprimeExpedienteClick(Sender: TObject);
    procedure _V_CerrarTodasExecute(Sender: TObject);
    procedure ClientAreaPG_DevExDrawTabEx(AControl: TcxCustomTabControl;
      ATab: TcxTab; Font: TFont);
    procedure _V_CerrarExecute(Sender: TObject);
    procedure _E_Exportar_DevExExecute(Sender: TObject);

  private
    { Private declarations }
    tcValor: TCaption;
    procedure RevisaDerechos;
    procedure CargaExpedienteActivo;

    procedure AbreFormas;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure RefrescaExpedientesActivos;
  protected
    { Protected declarations }
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    procedure CargaTraducciones; override;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); override;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); override;
    {$endif}

  public
    { Public declarations }
    procedure ActualizaShell( DataSet: TDataSet );
    procedure CambioExpedienteActivo;
    function GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet; override;



  end;

var
  TressShell: TTressShell;
  //DevEx(by am): Arreglo para guardar los status del Empleado
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');

implementation

{$R *.DFM}

uses DCliente,
     DConsultas,
     DSistema,
     DCatalogos,
     //DReportes,
     DDiccionario,
     DGlobal,
     DTablas,
     DMedico,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     ZetaRegistryServerEditor,
{$endif}
//     ZetaBuscaEmpleado,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaDialogo,
     ZGlobalTress,
     ZArbolTress,
     ZAccesosTress,
     ZAccesosMgr,
     FAyudaContexto,
     FEditConsultaGlobal_DevEx,
     FBuscaExpedientes,
     DBaseCliente,
     ZNavBarTools,
     DReportes;

const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_FECHA = 2;
     K_PANEL_HINT = 4;
     K_EMP_EXPEDIENTE                   = 1001;
     K_EMP_CONSULTAS_MEDICAS            = 1003;
     K_EMP_EMBARAZOS                    = 1004;
     K_EMP_MED_ENTR                     = 1035;
     K_EMP_AX_ENFERMEDADES              = 1005;
     K_EMP_INCAPACIDADES                = 1006;
     K_PRINCIPAL_CONTEXT = H00001_Usando_Servicio_Medico;



function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          {Entidades de Tress}
          enEmpleado: Result := dmCliente.cdsEmpleadoLookUp;
          enNivel1: Result := dmTablas.cdsNivel1;
          enNivel2: Result := dmTablas.cdsNivel2;
          enNivel3: Result := dmTablas.cdsNivel3;
          enNivel4: Result := dmTablas.cdsNivel4;
          enNivel5: Result := dmTablas.cdsNivel5;
          enNivel6: Result := dmTablas.cdsNivel6;
          enNivel7: Result := dmTablas.cdsNivel7;
          enNivel8: Result := dmTablas.cdsNivel8;
          enNivel9: Result := dmTablas.cdsNivel9;
          enEntidad: Result := dmTablas.cdsEstado;
          enPuesto: Result := dmCatalogos.cdsPuestos;
          enTurno: Result := dmCatalogos.cdsTurnos;
          enHorario: Result := dmCatalogos.cdsHorarios;
          {Entidades de Servicios Medicos}
          enMedicina: Result := nil;
          enTAccidente: Result := dmMedico.cdsTAccidente;
          enCausaAccidente: Result := dmMedico.cdsCausaAcc;
          enMotivoAcc: Result := dmMedico.cdsMotivoAcc;
          enTEstudio: Result := dmMedico.cdsTEstudio;
          enTConsulta: Result := dmMedico.cdsTConsulta;
          enDiagnostico: Result := nil;
          enGrupoEx: Result := nil;
     else
         Result := nil;
     end;
end;

{ **************** TTressShell **************** }

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmMedico := TdmMedico.Create(Self);
     inherited;
     HelpContext := K_PRINCIPAL_CONTEXT;
     FHelpGlosario := 'TressMedico.chm';
     //EmpleadoTab.TabVisible := FALSE;
     HContenido_btnUsoTeclado.HelpContext:=21;
     HContenido_Glosario.HelpContext := 19;

    { Procesos.Visible := FALSE;
     Arbol.HelpContext                     := K_PRINCIPAL_CONTEXT;
     ArchivoOtraEmpresa.HelpContext        := K_PRINCIPAL_CONTEXT;
     ArchivoImprimir.HelpContext           := K_PRINCIPAL_CONTEXT;
     ArchivoImprimirForma.HelpContext      := K_PRINCIPAL_CONTEXT;
     ArchivoImpresora.HelpContext          := K_PRINCIPAL_CONTEXT;
     EscogeImpresora.HelpContext           := K_PRINCIPAL_CONTEXT;  }
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     //FFormManager.Free;
     inherited;
     //Expediente.Free;
     dmMedico.Free;
     dmDiccionario.Free;
     //dmReportes.Free;
     dmTablas.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmConsultas.Free;
     //inherited;                                                                \
     dmCliente.Free;
     Global.Free;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
  CargaTraducciones;
  TabArchivo_btnCatalogoUsuarios.Visible := ivNever;
  TabArchivo_btnSQL.Visible := ivNever;
  ClientAreaPopup.Items[0].ImageIndex := 36;
  ClientAreaPopup.Items[1].ImageIndex := 37;
  ClientAreaPopup.Items[3].ImageIndex := 38;
  //btnEmpAnterior_DevEx.Enabled:=false;
  EmpleadoPrimero.Enabled:=false;
//  HelpContext := H_CONCIL_COMENZAR;
end;

procedure TTressShell.AbreFormas;
begin
    //if CheckDerecho( D_EMP_SERV_EXPEDIENTE, K_EMP_EXPEDIENTE ) then
     if ZAccesosMgr.CheckDerecho( D_EMP_SERV_EXPEDIENTE, K_DERECHO_CONSULTA ) then
          AbreFormaConsulta(efcExpediente);
     //if ZAccesosMgr.CheckDerecho( D_EMP_KARDEX_CONSULTAS, K_EMP_CONSULTAS_MEDICAS ) then
     if ZAccesosMgr.CheckDerecho( D_EMP_KARDEX_CONSULTAS, K_DERECHO_CONSULTA ) then
        AbreFormaConsulta(efcConsMedica);
     //if CheckDerecho( D_EMP_KARDEX_EMBARAZOS, K_EMP_EMBARAZOS ) then
     if ZAccesosMgr.CheckDerecho( D_EMP_KARDEX_EMBARAZOS, K_DERECHO_CONSULTA ) then
        AbreFormaConsulta(efcEmbarazo);
     //if CheckDerecho( D_EMP_KARDEX_ACCIDENTES, K_EMP_AX_ENFERMEDADES ) then
     if ZAccesosMgr.CheckDerecho( D_EMP_KARDEX_ACCIDENTES, K_DERECHO_CONSULTA ) then
        AbreFormaConsulta(efcAxEnfTrabajo);
     //if CheckDerecho( D_EMP_INCAPACIDADES, K_EMP_INCAPACIDADES ) then
     if ZAccesosMgr.CheckDerecho( D_EMP_INCAPACIDADES, K_DERECHO_CONSULTA ) then
        AbreFormaConsulta(efcIncapacidades);
     //if CheckDerecho(  D_MEDICINA_ENTR, K_EMP_MED_ENTR ) then
     if ZAccesosMgr.CheckDerecho(  D_MEDICINA_ENTR, K_DERECHO_CONSULTA ) then
        AbreFormaConsulta(efcMedicinaEntr);

     ClientAreaPG_DevEx.ActivePageIndex := 0;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosExpediente;
        end;
        dmMedico.Conectar;
        CargaExpedienteActivo;
        //CreaArbol( True );
        CreaArbol( Revisa( D_EMP_SERV_EXPEDIENTE ) );
        RevisaDerechos;
        _V_Empleados.Execute;
        AbreFormas;
        {With  ClientAreaPG do
        begin
             if (PageCount > 0)then
                ActivePageIndex := 0;
        end;}

        SetArbolitosLength(12);
        CreaNavBar;
        LLenaOpcionesGrupos;

        ZetaClientDataSet.GlobalSoloKeysActivos := Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS );
        // 2013-Feb-05: Mejora en Confidencialidad
        ZetaClientDataSet.GlobalListaConfidencialidad := dmCliente.Confidencialidad;

     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          CierraDatasets( dmMedico );
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmReportes );
          CierraDatasets( dmTablas );
          CierraDatasets( dmCatalogos );
          CierraDatasets( dmSistema );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     //ZetaClientTools.SetEnabled( EmpleadoPanel, EmpresaAbierta );

     if not EmpresaAbierta then
     begin
          StatusBarMsg( VACIO, K_PANEL_EMPLEADO );
     end;
end;

procedure TTressShell.RevisaDerechos;
begin
    _R_Consulta.Enabled := Revisa( D_EMP_KARDEX_CONSULTAS ) and ( CheckDerecho( D_EMP_KARDEX_CONSULTAS, K_DERECHO_ALTA ) );
    _R_Embarazo.Enabled := Revisa( D_EMP_KARDEX_EMBARAZOS ) and ( CheckDerecho( D_EMP_KARDEX_EMBARAZOS, K_DERECHO_ALTA ) );
    _R_Accidente.Enabled := Revisa( D_EMP_KARDEX_ACCIDENTES ) and ( CheckDerecho( D_EMP_KARDEX_ACCIDENTES, K_DERECHO_ALTA ) );
    _R_Medicinas.Enabled := Revisa( D_MEDICINA_ENTR ) and ( CheckDerecho( D_MEDICINA_ENTR, K_DERECHO_ALTA ) );
    _R_Permisos.Tag := D_PACIENTE_PERMISOS;
    _R_Permisos.Enabled := Revisa( D_EMP_INCAPACIDADES ) and  ( CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_ALTA ) );
    _R_Incapacidades.Tag := D_PACIENTE_INCAPACIDADES;
    _R_Incapacidades.Enabled := Revisa( D_EMP_INCAPACIDADES ) and  ( CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_ALTA ) );
    _R_Cons_Global.Enabled := Revisa( D_REG_CONS_MED_GLOBAL );
    EmpleadoBuscaBtn_DevEx.Enabled := ( Revisa( D_EMP_SERV_EXPEDIENTE ) )
                                or ( Revisa( D_EMP_INCAPACIDADES ) )
                                or ( Revisa( D_EMP_KARDEX_CONSULTAS ) )
                                or ( Revisa( D_EMP_KARDEX_EMBARAZOS ) )
                                or ( Revisa( D_EMP_KARDEX_ACCIDENTES ) )
                                or ( Revisa( D_MEDICINA_ENTR ) );
    //EditarBuscar.Enabled := EmpleadoBuscaBtn.Enabled;
end;


procedure TTressShell.tabRegistro_btnConsultaMedicaGlobalClick(Sender: TObject);
begin
  inherited;
  _R_Cons_GlobalExecute( Sender );
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          //dmLabor.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell._E_CambiarEmpleadoExecute(Sender: TObject);
begin
     inherited;
    // PGSuperior.ActivePage := EmpleadoTab;
     ExpedienteNumeroCB.SetFocus;
     //VentanaEmpleados.Checked := True;
end;

procedure TTressShell._E_Exportar_DevExExecute(Sender: TObject);
begin
  if FormaActiva.Caption = 'Expediente' then
      FormaActiva.Caption:='Consultas de Expediente';
  inherited;

end;

procedure TTressShell._V_CerrarExecute(Sender: TObject);
begin
  inherited;
  if (FormaActiva.Caption = 'Expediente') or (FormaActiva.Caption = '')  then
  if EOperaciones_btnImprimir.Visible = ivAlways then
      EOperaciones_btnImprimir.Visible := ivNever;
end;

procedure TTressShell._V_CerrarTodasExecute(Sender: TObject);
begin
  inherited;
  if EOperaciones_btnImprimir.Visible <> ivNever then
    EOperaciones_btnImprimir.Visible := ivNever;
end;

procedure TTressShell._V_EmpleadosExecute(Sender: TObject);
begin
     _E_CambiarEmpleadoExecute( Sender );
end;


procedure TTressShell.PGSuperiorChange(Sender: TObject);
begin
     inherited;
     //MA:14/02/2002 Se mantiene la llamada para un valor activo posterior...
     {case PGSuperior.ActivePage.PageIndex of
          0: _V_Empleados.Execute;
     end;}
end;

procedure TTressShell.popConsultasClick(Sender: TObject);
begin
    if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          TExpediente_DevEx(FormaActiva).reConsulta.Print('Imprimiendo Expediente');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir El Expediente', 0 );
end;

procedure TTressShell._Exp_PrimeroExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetExpedientePrimero then
          begin
               ExpedienteNumeroCB.ValorEntero := Expediente;
               CambioExpedienteActivo;
          end;
     end;
end;

procedure TTressShell._Exp_AnteriorExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetExpedienteAnterior then
          begin
               ExpedienteNumeroCB.ValorEntero := Expediente;
               CambioExpedienteActivo;
          end;
     end;
end;

procedure TTressShell._Exp_SiguienteExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetExpedienteSiguiente then
          begin
               ExpedienteNumeroCB.ValorEntero := Expediente;
               CambioExpedienteActivo;
          end;
     end;
end;

procedure TTressShell._Exp_UltimoExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if GetExpedienteUltimo then
          begin
               ExpedienteNumeroCB.ValorEntero := Expediente;
               CambioExpedienteActivo;
          end;
     end;
end;

procedure TTressShell.CargaExpedienteActivo;
begin
     with dmCliente do
     begin
          ExpedienteNumeroCB.ValorEntero := Expediente;
     end;
     RefrescaExpedientesActivos;
end;

procedure TTressShell.CargaTraducciones;
begin
  inherited;
  DevEx_cxLocalizer.Active := True;
  DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)
end;

procedure TTressShell.ClientAreaPG_DevExChange(Sender: TObject);
begin
  inherited;
  if FormaActiva.Caption = 'Expediente' then
     EOperaciones_btnImprimir.Visible := ivAlways
  else EOperaciones_btnImprimir.Visible := ivNever;
end;

procedure TTressShell.ClientAreaPG_DevExDrawTabEx(AControl: TcxCustomTabControl;
  ATab: TcxTab; Font: TFont);
begin
  inherited;
    if (FormaActiva.Caption = 'Expediente')then
      EOperaciones_btnImprimir.Visible := ivAlways
    else EOperaciones_btnImprimir.Visible := ivNever;

end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
    _V_Cerrar.Execute;
end;

procedure TTressShell.RefrescaExpedientesActivos;
var
   oColor : TColor;
   sCaption, sHint : String;
   iTipo: Integer;
   i: Integer;
   oInfoEmpleado: TInfoEmpleado;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
     with dmCliente do
     begin
          StatusBarMsg( 'Expediente: '+ IntToStr( Expediente ), K_PANEL_EMPLEADO );
          with GetDatosExpedienteActivo do
          begin
               EmpleadoPrettyName.Caption := Nombre;
               iTipo := dmCliente.cdsExpediente.FieldByName('EX_TIPO').AsInteger;
               if( iTipo <> 0 )then
               begin
                    EmpleadoNumeroCB.Text := ZetaCommonLists.ObtieneElemento( lfExTipo, iTipo );
                    {with EmpleadoActivoPanel do
                    begin
                         Caption := VACIO;
                         Hint := VACIO;
                    end;}
               end
               else
               begin
                    EmpleadoNumeroCB.ValorEntero := Empleado;
                    oInfoEmpleado.Activo := Activo;
                    oInfoEmpleado.Baja := Baja;
                    oInfoEmpleado.Ingreso := Ingreso;
                    oInfoEmpleado.Recontratable := True; //no mostrar hint de no recontratable
                   { with EmpleadoActivoPanel do
                    begin
                         dmCliente.GetStatusEmpleado(oInfoEmpleado, sCaption, sHint, oColor);
                         Font.Color := oColor;
                         Caption := sCaption;
                         Hint := sHint;
                   // end;}
               end;
          end;
     end;

     //
     with dmCliente do
     begin
          StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          //MuestraDatosUsuarioActivo; //DevEx(by am): Se refresca el usuario y el grupo en el Panel de valores activos
          with GetDatosExpedienteActivo do
          begin
               //DevEx (by am): Se despliega el valor activo del empleado de distinta forma en cada vista

                EmpleadoPrettyName.Caption := Nombre;

                GetStatusEmpleado(GetDatosExpedienteActivo, sCaption, sHint, oColor);
                    //DevEx (by am): Inicializando Bitmat y PNG
                ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
                APngImage := nil;
                //sCaption := ;
                for i := Low(StatusEmpleado) to High(StatusEmpleado) do
                begin

                     if (sCaption = StatusEmpleado[i]) then
                     begin
                        try
                           //DevEx (by am): Creando el Bitmap
                           ABitmap.Clear;
                           Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                           //DevEx (by am): Creando el PNG
                           APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                           ImagenStatus_DevEx.Picture.Graphic := APngImage;
                           ImagenStatus_DevEx.Hint := sHint;
                        finally
                               APngImage.Free;
                               ABitmap.Free;
                        end;
                        Break;
                     end;

          end;

        end;
     // end antonio
  end;
end;

procedure TTressShell.CambioExpedienteActivo;
begin
     RefrescaExpedientesActivos;
     CambioValoresActivos( stExpediente );
end;
{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     dmMedico.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell.EmpleadoBuscaBtn_DevExClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
    { if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ) then
          ExpedienteNumeroCB.AsignaValorEntero( StrToIntDef( sKey, 0 ) ); }
end;

procedure TTressShell.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
begin
  inherited;
     with dmMedico.cdsCodigos do
     begin
          Conectar;
          if Locate('CB_CODIGOC', EmpleadoNumeroCB.ValorEntero, []) then
          begin
               if Locate('CB_CODIGOC;EX_TIPO', VarArrayOf([EmpleadoNumeroCB.ValorEntero,Ord(exEmpleado)]), []) AND
                  ( FieldByName('EX_CODIGO').AsInteger <> 0 ) then
               begin
                    lOk := dmCliente.SetExpedienteNumero( FieldByName('EX_CODIGO').AsInteger );
                    if lOk then
                    begin
                         ExpedienteNumeroCB.ValorEntero := FieldByName('EX_CODIGO').AsInteger;
                         CambioExpedienteActivo;
                    end;{else Nunca entra al else porque si no tiene expediente es = 0 y entra al siguiente else}
               end
               else
               begin
                    if ZConfirm('Informaci�n','El Empleado No Tiene Expediente' + CR_LF + '�Desea Crear Expediente?',0,mbYes ) then
                    begin
                         if ( CheckDerecho( D_EMP_SERV_EXPEDIENTE, K_DERECHO_ALTA ) ) then
                         begin
                              with dmMedico do
                              begin
                                   EmpleadoNuevo := EmpleadoNumeroCB.ValorEntero;
                                   with cdsExpediente do
                                   begin
                                        Conectar;
                                        Agregar;
                                   end;     
                                   EmpleadoNuevo := 0;
                              end;
                         end
                         else
                             ZInformation( 'Operaci�n No V�lida','No Tiene Permiso Para Agregar Expediente',0 );
                    end
                    else
                        EmpleadoNumeroCB.Text := tcValor;
               end;
          end
          else
          begin
               ZError(Caption, 'Empleado No Existe',0);
          end;
     end;
end;

procedure TTressShell.ExpedienteNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     lOk := dmCliente.SetExpedienteNumero( ExpedienteNumeroCB.ValorEntero );
     if lOk then
        CambioExpedienteActivo
     else
         ZetaDialogo.zInformation( 'Error', '! Expediente No Encontrado !', 0 );
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean;
begin
     Result := ( eEntidad = enEmpleado );
     {if Result then
        lEncontrado := ZetaBuscaEmpleado.BuscaEmpleadoDialogo( VACIO, sKey, sDescription )}
end;

procedure TTressShell._E_BuscarEmpleadoExecute(Sender: TObject);
var
   iEmpleado, iExpediente : integer;
   sDescription: String;
begin
     inherited;
     tcValor := EmpleadoNumeroCB.Text;
     if FBuscaExpedientes.BuscaExpedienteDialogo( VACIO, iExpediente, iEmpleado, sDescription, FALSE ) then
     begin
          if iExpediente <> 0 then
             ExpedienteNumeroCB.AsignaValorEntero( iExpediente )
          else
              EmpleadoNumeroCB.AsignaValorEntero( iEmpleado );
     end;
end;

procedure TTressShell.ArchivoClick(Sender: TObject);
begin
  inherited;
//MA:Directiva para que cuando se ejecute en 2 capas no se activen los archivos temporales
{$ifdef DOS_CAPAS}
     ArchivoCache.Enabled:=False;
{$endif}
end;

//MA:Acciones para el menu del Shell <Registro>
procedure TTressShell._R_ConsultaExecute(Sender: TObject);
var
   oCursor : TCursor;
begin
  inherited;
  oCursor := Screen.Cursor;
  try
     Screen.Cursor := crHourGlass;
     if ZAccesosMgr.CheckDerecho( ZAccesosTress.D_EMP_KARDEX_CONSULTAS, K_DERECHO_ALTA ) then
     begin
          With dmMedico do
          begin
               if ExisteExpediente then
               begin
                    With cdsKardexConsultas do
                    begin
                         Conectar;
                         Agregar;
                    end;
               end
               else
                   ZInformation( 'Operaci�n No V�lida','El Empleado No Tiene Expediente',0 );
          end;
     end
     else
     begin
          ZInformation( 'Operaci�n No V�lida','No Tiene Permiso Para Agregar Consultas',0 );
     end;
  finally
         Screen.Cursor := oCursor;
  end;
end;

procedure TTressShell._R_EmbarazoExecute(Sender: TObject);
var
   oCursor : TCursor;
begin
  inherited;
  oCursor := Screen.Cursor;
  try
     Screen.Cursor := crHourGlass;
     if ZAccesosMgr.CheckDerecho( ZAccesosTress.D_EMP_KARDEX_EMBARAZOS, K_DERECHO_ALTA ) then
     begin
          With dmMedico do
          begin
               if ExisteExpediente then
               begin
                    With cdsKardexEmbarazo do
                    begin
                         Conectar;
                         Agregar;
                    end;
               end
               else
                   ZInformation( 'Operaci�n No V�lida','El Empleado No Tiene Expediente',0 );
          end;
     end
     else
          ZInformation( 'Operaci�n No V�lida','No Tiene Permiso Para Agregar Embarazo',0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._R_AccidenteExecute(Sender: TObject);
var
   oCursor : TCursor;
begin
  inherited;
  oCursor := Screen.Cursor;
  try
     Screen.Cursor := crHourGlass;
     if ZAccesosMgr.CheckDerecho( ZAccesosTress.D_EMP_KARDEX_ACCIDENTES, K_DERECHO_ALTA ) then
     begin
          With dmMedico do
          begin
               if ExisteExpediente then
               begin
                    With cdsKardexAccidente do
                    begin
                         Conectar;
                         Agregar;
                    end;
               end
               else
                   ZInformation( 'Operaci�n No V�lida','El Empleado No Tiene Expediente',0 );
          end;
     end
     else
          ZInformation( 'Operaci�n No V�lida','No Tiene Permiso Para Agregar Accidente',0 );
  finally
         Screen.Cursor := oCursor;
  end;
end;

procedure TTressShell._R_MedicinasExecute(Sender: TObject);
var
   oCursor : TCursor;
begin
  inherited;
  oCursor := Screen.Cursor;
  try
     Screen.Cursor := crHourGlass;
     if ZAccesosMgr.CheckDerecho( ZAccesosTress.D_MEDICINA_ENTR, K_DERECHO_ALTA ) then
     begin
          With dmMedico do
          begin
               if ExisteExpediente then
               begin
                    With cdsMedicinaEntr do
                    begin
                         Conectar;
                         ActivakCons:=false;
                         Agregar;
                    end;
               end
               else
                   ZInformation( 'Operaci�n No V�lida','El Empleado No Tiene Expediente',0 );
          end;
     end
     else
          ZInformation( 'Operaci�n No V�lida','No Tiene Permiso Para Agregar Medicina',0 );
  finally
         Screen.Cursor := oCursor;
  end;
end;

procedure TTressShell.ActualizaShell( DataSet: TDataSet );
begin
     if dmCliente.SetExpedienteNumero( DataSet.FieldByName('EX_CODIGO').AsInteger  ) then
     begin
          CambioExpedienteActivo;
          ExpedienteNumeroCB.ValorEntero := DataSet.FieldByName('EX_CODIGO').AsInteger;
     end;
end;

procedure TTressShell.EmpleadoNumeroCBEnter(Sender: TObject);
begin
  inherited;
  tcValor := EmpleadoNumeroCB.Text;
end;

procedure TTressShell._R_Cons_GlobalExecute(Sender: TObject);
var
   oCursor : TCursor;
begin
  inherited;
     dmMedico.RegistraConsultaGlobal;
end;

procedure TTressShell._Exp_UltimoUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.ExpedienteUpEnabled;
end;

procedure TTressShell._Exp_PrimeroUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.ExpedienteDownEnabled;
end;


procedure TTressShell.ImagenStatus_DevExDblClick(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcExpediente );
end;

procedure TTressShell.ImprimeExpedienteClick(Sender: TObject);
begin
  if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_IMPRESION ) then
     begin
          inherited;
          TExpediente_DevEx(FormaActiva).reExpediente.Print('Imprimiendo Expediente');
     end
     else
         ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Imprimir El Expediente', 0 );
end;

procedure TTressShell.IncapacidadesPermisosExecute(Sender: TObject);
var
   lPermisos: boolean;
begin
     inherited;
     lPermisos := ( TAction( Sender ).Tag = D_PACIENTE_PERMISOS );
     if ZAccesosMgr.CheckDerecho(TAction( Sender ).Tag, K_DERECHO_ALTA ) then
     begin
          with dmMedico do
          begin
               cdsHistorial.Conectar;
               if ExisteExpediente then
               begin
                    if lPermisos then //Se ejecuto el action de Permisos
                       TipoIncaPerm := 1
                    else                      //Incapacidades
                        TipoIncaPerm := 0;
                    cdsHistorial.Agregar;
               end
               else
                   ZInformation( 'Operaci�n No V�lida', 'El Empleado No Tiene Expediente', 0 );
          end;
     end
     else
          ZInformation( 'Operaci�n No V�lida', 'No tiene derecho para agregar registro', 0 );
end;

procedure TTressShell.LLenaOpcionesGrupos;
var
   iIndice : Integer;

begin
     if ( dmCliente.EmpresaAbierta ) then
     begin
          // @DChavez : Si no tiene derecho asignar los iconos que corresponden al navbar  cuando no tiene derechos
          if( (DevEx_NavBar.Groups.Count = 1 )and ( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos'  ) )   then
          begin
                 DevEx_NavBar.Groups[0].LargeImageIndex := 3;
                 DevEx_NavBar.Groups[0].SmallImageIndex := 3;
                 //para que use las imagenes grandes
                 DevEx_NavBar.Groups[0].UseSmallImages := False;
          end
          else
              //recorre los grupos del navbar, le asigna su imagen por su nombre
              for iIndice := 0 to (DevEx_NavBar.Groups.Count - 1)  do
              begin
                   DevEx_NavBar.Groups[iIndice].LargeImageIndex := iIndice;
                   DevEx_NavBar.Groups[iIndice].SmallImageIndex := iIndice;
                   //para que use las imagenes grandes
                   DevEx_NavBar.Groups[iIndice].UseSmallImages := False;
              end;
     end;
end;

end.
