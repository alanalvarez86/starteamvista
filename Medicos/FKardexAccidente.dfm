inherited KardexAccidente: TKardexAccidente
  Left = 38
  Top = 152
  Caption = 'Accidentes'
  ClientWidth = 868
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 868
    ExplicitWidth = 868
    inherited ValorActivo2: TPanel
      Width = 609
      ExplicitWidth = 609
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 868
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'AX_FECHA'
        Title.Caption = 'Fecha'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AX_HORA'
        Title.Caption = 'Hora'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AX_TIP_LES'
        Title.Caption = 'Circunstancias'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AX_NUM_INC'
        Title.Caption = '# Incapacidad'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_DESCRIP'
        Title.Caption = 'Causa de Accidente'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_DESCRIP2'
        Title.Caption = 'Tipo de Accidente'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AX_RIESGO'
        Title.Caption = 'Riesgo'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AX_INCAPA'
        Title.Caption = #191'Incapacitado?'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AX_DAN_MAT'
        Title.Caption = #191'Da'#241'o Material?'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Modific'#243
        Width = 200
        Visible = True
      end>
  end
end
