unit FEditDiagnostico_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaNumero,
  ZetaKeyCombo, Mask, ZetaEdit, ZetaKeyLookup, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TfDiagnosticomed_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    DA_NOMBRE: TDBEdit;
    Label3: TLabel;
    DA_INGLES: TDBEdit;
    Label5: TLabel;
    DA_NUMERO: TZetaDBNumero;
    Label6: TLabel;
    DA_TEXTO: TDBEdit;
    DA_CODIGO: TZetaDBEdit;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  fDiagnosticomed_DevEx: TfDiagnosticomed_DevEx;

implementation

uses
//ZetaBuscador
DMedico, ZAccesosTress, FAyudaContexto;

{$R *.DFM}

procedure TfDiagnosticomed_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  //MA:Pendiente
  IndexDerechos := ZAccesosTress.D_CAT_DIAGNOSTICO;
  FirstControl := DA_CODIGO;
  HelpContext:= H00006_Catalogo_de_Diagnosticos;
end;

procedure TfDiagnosticomed_DevEx.Connect;
begin
     with dmMedico do
     begin
          DataSource.DataSet:= cdsDiagnostico;
     end;
end;

procedure TfDiagnosticomed_DevEx.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', 'Diagn�stico', 'DA_CODIGO', dmMedico.cdsDiagnostico );
end;


end.
