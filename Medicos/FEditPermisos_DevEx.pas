unit FEditPermisos_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   StdCtrls, Mask, DBCtrls, Db, Buttons,
  ExtCtrls,ZetaDBTextBox, ZetaKeyCombo, ZetaNumero,
  ZetaFecha, ZetaKeyLookup, ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
    TEditPermisos_DevEx = class(TBaseEdicion_DevEx)
    LIN_FEC_INI: TLabel;
    LIN_DIAS: TLabel;
    LIN_FEC_FIN: TLabel;
    Label13: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    UsuarioLbl: TLabel;
    PM_COMENTA: TDBEdit;
    PM_TIPO: TZetaDBKeyLookup_DevEx;
    PM_FEC_INI: TZetaDBFecha;
    PM_DIAS: TZetaDBNumero;
    PM_NUMERO: TDBEdit;
    PM_CAPTURA: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    PM_FEC_FIN: TZetaDBTextBox;
    US_DESCRIP: TZetaDBTextBox;
    PM_CLASIFI: TZetaDBKeyCombo;
    PM_GLOBAL: TDBCheckBox;
    Label1: TLabel;
    PM_FEC_REG: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure PM_CLASIFIChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FiltroPermiso : string;
    procedure BorraElementosClasifi;
    procedure SetFiltros;
  protected
    procedure Connect;override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  EditPermisos_DevEx: TEditPermisos_DevEx;

implementation

uses  dMedico, DTablas, DSistema, ZAccesosTress, ZetaCommonLists, ZetaCommonClasses,
  DCliente, ZetaCommonTools, FAyudaContexto, ZGlobalTress, DGlobal;



{$R *.DFM}


procedure TEditPermisos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FiltroPermiso := 'TB_INCIDEN ='+ IntToStr( Ord( eiPermiso ) );
     PM_TIPO.Filtro := FiltroPermiso ;
     HelpContext:= H00106_Incapacidades_y_permisos;
     TipoValorActivo1 := ZetaCommonLists.stExpediente;
     IndexDerechos := ZAccesosTress.D_PACIENTE_PERMISOS;
     FirstControl := PM_FEC_INI;
     BorraElementosClasifi;
     PM_TIPO.LookupDataset := dmTablas.cdsIncidencias;

end;

procedure TEditPermisos_DevEx.SetFiltros;
begin
     PM_TIPO.Filtro := ConcatFiltros( FiltroPermiso,Format('TB_PERMISO = -1 OR TB_PERMISO = %d',[ PM_CLASIFI.Valor ] ) );
     //PM_TIPO.SetLlaveDescripcion(VACIO,VACIO);
end;

procedure TEditPermisos_DevEx.BorraElementosClasifi;
 var i: eTipoPermiso;
begin
     for i := High(eTipoPermiso) downto tpFaltaJustificada do
     begin
          PM_CLASIFI.Items.Delete( ord( i ) );
     end;
end;

procedure TEditPermisos_DevEx.Connect;
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmMedico do
     begin
          Datasource.DataSet := cdsHisPermiso;
     end;
end;

function TEditPermisos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
end;

function TEditPermisos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
end;

function TEditPermisos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
end;

procedure TEditPermisos_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     if ( Field = Nil )then
        SetFiltros;
end;

procedure TEditPermisos_DevEx.PM_CLASIFIChange(Sender: TObject);
begin
     inherited;
     SetFiltros;
     PM_TIPO.SetLlaveDescripcion(VACIO,VACIO);
end;

procedure TEditPermisos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
     if Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
     begin
          PM_FEC_FIN.Visible := false;
          PM_FEC_REG.Visible := true;
          PM_FEC_REG.Left := 104;    
          LIN_FEC_FIN.Caption := 'Fecha Regreso:';
          LIN_FEC_FIN.Left := 23;
     end
     else
     begin
          PM_FEC_FIN.Visible := true;
          PM_FEC_REG.Visible := false;
          LIN_FEC_FIN.Caption := 'Regresa:';
          LIN_FEC_FIN.Left := 56;
     end;
end;

end.



