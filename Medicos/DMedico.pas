unit DMedico;

interface

{$DEFINE MULTIPLES_ENTIDADES}

{$INCLUDE DEFINES.INC}


uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,comctrls, cxRichEdit,Mask,

{$ifdef DOS_CAPAS}
     DServerMedico,
     DServerRecursos,
     DServerAsistencia,
{$else}
     Recursos_TLB,
     ServerMedico_TLB,
     Asistencia_TLB,
{$endif}
     {$ifndef VER130}
     Variants,MaskUtils,
     {$endif}
     FCamposMGR,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     EditorDResumen;

type
  TdmMedico = class(TDataModule)       
    cdsTConsulta: TZetaLookupDataSet;
    cdsTEstudio: TZetaLookupDataSet;
    cdsTAccidente: TZetaLookupDataSet;
    cdsCausaAcc: TZetaLookupDataSet;
    cdsMedicina: TZetaLookupDataSet;
    cdsDiagnostico: TZetaLookupDataSet;
    cdsExpediente: TZetaClientDataSet;
    cdsKardexConsultas: TZetaClientDataSet;
    cdsKardexAccidente: TZetaClientDataSet;
    cdsKardexEmbarazo: TZetaClientDataSet;
    cdsMedicinaEntr: TZetaClientDataSet;
    cdsGruposEx: TZetaLookupDataSet;
    cdsCamposEx: TZetaClientDataSet;
    cdsHistorial: TZetaClientDataSet;
    cdsHisPermiso: TZetaClientDataSet;
    cdsSoloExpediente: TZetaClientDataSet;
    cdsDatosEmpleado: TZetaClientDataSet;
    cdsCodigos: TZetaClientDataSet;
    cdsBusquedas: TZetaLookupDataSet;
    cdsConsultaGlobal: TZetaClientDataSet;
    cdsMotivoAcc: TZetaLookupDataSet;
    cdsHisIncapaci: TZetaClientDataSet;
    cdsListaConflicto: TZetaClientDataSet;
    procedure cdsTablasAfterPost(DataSet: TDataSet);
    procedure cdsTablasAlAdquirirDatos(Sender: TObject);
    procedure cdsTablasAlModificar(Sender: TObject);
    procedure cdsTablasBeforePost(DataSet: TDataSet);
    procedure cdsTablasGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    {$ifdef VER130}
    procedure cdsTablasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisPermisoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsConsultaGlobalReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisIncapaciReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsTablasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisPermisoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsConsultaGlobalReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsHisIncapaciReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure ShowEdicion(const iTabla : Integer);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsKardexConsultasAlAdquirirDatos(Sender: TObject);
    function  GetIndexRights( const iTabla: Integer ): Integer;
    procedure cdsKardexConsultasAlCrearCampos(Sender: TObject);
    procedure cdsKardexEmbarazoAlCrearCampos(Sender: TObject);
    procedure cdsKardexAccidenteAlCrearCampos(Sender: TObject);
    procedure cdsExpedienteAlCrearCampos(Sender: TObject);
    procedure cdsTAccidenteAlCrearCampos(Sender: TObject);
    procedure cdsCausaAccAlCrearCampos(Sender: TObject);
    procedure cdsKardexEmbarazoAlAdquirirDatos(Sender: TObject);
    procedure cdsKardexAccidenteAlAdquirirDatos(Sender: TObject);
    procedure cdsMedicinaEntrAlAdquirirDatos(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsExpedienteAlAdquirirDatos(Sender: TObject);
    procedure cdsHistorialAlAdquirirDatos(Sender: TObject);
    procedure cdsHistorialAlCrearCampos(Sender: TObject);
    procedure cdsMedicinaEntrAlCrearCampos(Sender: TObject);
    procedure cdsMedicinaEntrAlEnviarDatos(Sender: TObject);
    procedure cdsKardexEmbarazoAlEnviarDatos(Sender: TObject);
    procedure cdsKardexConsultasAlEnviarDatos(Sender: TObject);
    procedure cdsKardexAccidenteAlEnviarDatos(Sender: TObject);
    procedure cdsMedicinaEntrNewRecord(DataSet: TDataSet);
    procedure cdsMedicinaEntrBeforePost(DataSet: TDataSet);
    procedure cdsKardexEmbarazoNewRecord(DataSet: TDataSet);
    procedure cdsKardexEmbarazoCalcFields(DataSet: TDataSet);
    procedure cdsExpedienteAlEnviarDatos(Sender: TObject);
    procedure cdsKardexConsultasBeforePost(DataSet: TDataSet);
    procedure cdsKardexConsultasNewRecord(DataSet: TDataSet);
    procedure cdsKardexAccidenteBeforePost(DataSet: TDataSet);
    procedure cdsKardexAccidenteNewRecord(DataSet: TDataSet);
    procedure cdsHistorialAlModificar(Sender: TObject);
    procedure cdsHisPermisoAlAdquirirDatos(Sender: TObject);
    procedure cdsHisPermisoAlCrearCampos(Sender: TObject);
    procedure cdsHisPermisoAlEnviarDatos(Sender: TObject);
    procedure cdsHisPermisoAfterDelete(DataSet: TDataSet);
    procedure cdsHisPermisoBeforePost(DataSet: TDataSet);
    procedure cdsHisPermisoNewRecord(DataSet: TDataSet);
    procedure cdsExpedienteNewRecord(DataSet: TDataSet);
    procedure cdsKardexAccidenteAfterDelete(DataSet: TDataSet);
    procedure cdsKardexEmbarazoAfterDelete(DataSet: TDataSet);
    procedure cdsKardexConsultasAfterDelete(DataSet: TDataSet);
    procedure cdsExpedienteAfterDelete(DataSet: TDataSet);
    procedure cdsExpedienteBeforePost(DataSet: TDataSet);
    procedure cdsKardexEmbarazoBeforePost(DataSet: TDataSet);
    procedure cdsKardexEmbarazoAlAgregar(Sender: TObject);
    procedure cdsSoloExpedienteAlAdquirirDatos(Sender: TObject);
    procedure cdsMedicinaEntrAfterDelete(DataSet: TDataSet);
    procedure cdsKardexConsultasAfterOpen(DataSet: TDataSet);
    procedure cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
    procedure cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsCodigosAlAdquirirDatos(Sender: TObject);
    procedure cdsBusquedasLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsBusquedasLookupKey(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter, sKey: String;
      var sDescription: String);
    procedure cdsConsultaGlobalAlCrearCampos(Sender: TObject);
    procedure cdsConsultaGlobalAlAdquirirDatos(Sender: TObject);
    procedure cdsHistorialAlAgregar(Sender: TObject);
    procedure cdsBusquedasAlCrearCampos(Sender: TObject);
    procedure cdsKardexAccidenteAlAgregar(Sender: TObject);
    procedure cdsExpedienteAlBorrar(Sender: TObject);
    procedure cdsGruposExAlEnviarDatos(Sender: TObject);
    procedure cdsExpedienteAlAgregar(Sender: TObject);
    procedure cdsHisIncapaciAfterDelete(DataSet: TDataSet);
    procedure cdsHisIncapaciAlAdquirirDatos(Sender: TObject);
    procedure cdsHisIncapaciAlCrearCampos(Sender: TObject);
    procedure cdsHisIncapaciAlEnviarDatos(Sender: TObject);
    procedure cdsHisIncapaciBeforePost(DataSet: TDataSet);
    procedure cdsHisIncapaciNewRecord(DataSet: TDataSet);
    procedure cdsHistorialAlBorrar(Sender: TObject);
    procedure OnPM_CLASIFIChange(Sender: TField);
  private
    { Private declarations }
    Activa:Boolean;
    FUpdating: Boolean;
    FCancelaAjuste: Boolean;
    FClasificaciones : TListaClasificaciones; {Es la lista de Registros de GRUPO_EX}
    FCampos : TListaCampos; {Es la lista de Registros de CAMPO_EX}
    fEditor : TEditorCliente;
    FEmpleadoNuevo: TNumEmp;
    FTipoIncaPerm: integer;    
    lCambiandoCB_CODIGO, lCambiandoEX_CODIGO: Boolean;
    FValidaGrabado: Boolean;
  {$ifdef DOS_CAPAS}
    function GetServerMedico: TdmServerMedico;
    property ServerMedico: TdmServerMedico read GetServerMedico;
    function GetServerRecursos : TdmServerRecursos;
    property ServerRecursos :TdmServerRecursos read GetServerRecursos;
    function GetServerAsistencia : TdmServerAsistencia;
    property ServerAsistencia :TdmServerAsistencia read GetServerAsistencia;
 {$else}
    FServidor: IdmServerMedicoDisp;
    FServidorRecursos : IdmServerRecursosDisp;
    FServidorAsistencia : IdmServerAsistenciaDisp;
    function GetServerMedico: IdmServerMedicoDisp;
    property ServerMedico: IdmServerMedicoDisp read GetServerMedico;
    function GetServerRecursos : IdmServerRecursosDisp ;
    property ServerRecursos : IdmServerRecursosDisp read GetServerRecursos;
    function GetServerAsistencia : IdmServerAsistenciaDisp ;
    property ServerAsistencia : IdmServerAsistenciaDisp read GetServerAsistencia;
  {$endif}
    procedure CargarCampos;
    procedure DescargarCampos;
    procedure DescargarCamposEx;
    procedure DescargarGruposEx;
    procedure OnPM_FEC_INIChange(Sender: TField);
    procedure OnPM_FEC_FINChange(Sender: TField);
    procedure OnIN_FEC_INIChange(Sender: TField);
    procedure OnIN_DIASChange(Sender: TField);
    procedure OnIN_SUA_INIChange(Sender: TField);
    procedure CN_LIBRASChange(Sender: TField);
    procedure CN_PESOChange(Sender: TField);
    procedure CN_FAHRENChange(Sender: TField);
    procedure CN_TEMPERAChange(Sender: TField);
    procedure CN_ALTURAChange(Sender: TField);
    procedure CN_PIESChange(Sender: TField);
    procedure OnPM_CLASIFIValidate(Sender: TField);
    procedure ObtieneClasifi(Sender: TField; var Text: String;DisplayText: Boolean);
    function  DerechoTipoPermiso( const Tipo : Integer ): Boolean;
  public
    { Public declarations }
    //BandAgregaExpediente:Boolean;
    //Total_lineas: Integer;
    FRefrescaEntidades:Boolean;
    property Clasificaciones: TListaClasificaciones read FClasificaciones;
    property Campos: TListaCampos read FCampos;
    property ActivaKcons: Boolean read Activa write Activa;
    property EmpleadoNuevo: TNumEmp read FEmpleadoNuevo write FEmpleadoNuevo;
    property TipoIncaPerm: integer read FTipoIncaPerm write FTipoIncaPerm;  
    procedure Conectar;
    procedure AbrirConfiguracion;
    procedure CreaResumenConsulta(oRichText: TcxRichEdit);
    procedure CreaResumenExpediente(oRichText: TcxRichEdit);
    procedure HacerBusqueda( oParamList: TZetaParams );
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure RegistraConsultaGlobal;
    procedure EX_CODIGOConsultaGlobalChange(Sender: TField);
    procedure EX_CODIGOConsultaGlobalValidate(Sender: TField);
    procedure CB_CODIGOConsultaGlobalChange(Sender: TField);
    procedure CB_CODIGOConsultaGlobalValidate(Sender: TField);
    function ExisteExpediente : Boolean;
    function ExisteConsulta : Boolean;
    function CargaDocumento( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
    function CheckAuto: Boolean;
    function EsPermiso: boolean;
    function EsEmpleado: boolean;
    procedure AbreDocumento;
    procedure BorraDocumento;
    procedure SincronizaFechas( const iControl: Integer );
    procedure EscribeConsultaGlobal( const oParams: OleVariant );
    function ValidarMotivoPermisos(const sCodigo :string; const iTipo:Integer ):Boolean;
    function GetPermisoFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const Dias: integer; var rDiasRango: Double ): TDate;
    function GetPermisoDiasHabiles( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
  end;

var
  dmMedico: TdmMedico;

implementation

uses ZAccesosMgr,
     ZReconcile,
     ZBaseEdicion_DevEx,
     ZAccesosTress,
     ZetaMsgDlg,
     ZetaDialogo,
     ZetaCommontools,
     ZBaseGridEdicion_DevEx,
     ZetaClientTools,
     ZetaFilesTools,
     ZetaMedicoTools,
     ZGlobalTress,
     FToolsRH,
     DCliente,
     DSistema,
     DTablas,
     DCatalogos,
     DGlobal,
     FEditTablas_DevEx,
     FEditTablas2_DevEx,
     FEditMedicina_DevEx,
     FCamposCFG_DevEx,
     FEditDiagnostico_DevEx,
     FTressShell,
     FAutoClasses,
     FEditEmbarazo_DevEx,
     FEditMedicinaEntr_DevEx,
     FEditKardexConsulta_DevEx,
     FEditKAccidentes_DevEx,
     FEditExpediente_DevEx,
     FEditPermisos_DevEx,
     FEditHisIncapaci_DevEx,
     FEditConsultaGlobal_DevEx;
     {FExpediente,
     FBuscaExpedientes,
     ,

     ZBaseEdicion;}

Const K_EDIT_CONSULTA=0;
      K_EDIT_ESTUDIO=1;
      K_EDIT_ACCIDENTE=2;
      K_EDIT_CAUSACC=3;
      K_EDIT_DIAGNOSTICO=4;
      K_EDIT_MEDICINA=5;
      K_EDIT_KCONSULTAS=7;
      K_EDIT_EMBARAZOS=8;
      K_EDIT_ACCIDENTES=9;
      K_EDIT_MED_ENTR=10;
      K_EDIT_EXPEDIENTE=11;
      K_EDIT_PERMISOS=12;
      K_EMP_DATOS_CONTRATA=13;
      K_EDIT_MOTIVOACC = 16;
      K_DIAS_INCAPACIDAD = 42;
      K_TIPO_PERMISO = 'PER';

      TP_CON_GOCE   = 1;
      TP_SIN_GOCE   = 2;

      { Constantes que se utilizan para las conversiones en los signos vitales }
      K_LIBRAS_KILOS = 2.2;
      K_FAHREN_CENT_VAL1 = 32;
      K_FAHREN_CENT_VAL2 = 1.8;
      K_METROS_PIES_VAL1 = 12;
      K_METROS_PIES_VAL2 = 2.54;
      K_POR_CIEN = 100;

{$R *.DFM}

function GetKilos( const rLibras: Extended ): Extended;
begin
     Result := rLibras / K_LIBRAS_KILOS;
end;

function GetLibras( const rKilos: Extended ): Extended;
begin
     Result := rKilos * K_LIBRAS_KILOS;
end;

function GetCentigrados( const rFahrenheit: Extended ): Extended;
begin
     Result := ( rFahrenheit - K_FAHREN_CENT_VAL1 ) / K_FAHREN_CENT_VAL2;
end;

function GetFahrenheit( const rCentigrados: Extended ): Extended;
begin
     Result := ( rCentigrados * K_FAHREN_CENT_VAL2 ) + K_FAHREN_CENT_VAL1;
end;

function GetMetros( const iPies, iPulgadas: Integer ): Extended;
begin
     Result :=  ( ( iPies * K_METROS_PIES_VAL1 ) + iPulgadas ) * K_METROS_PIES_VAL2 / K_POR_CIEN;
end;

function GetPies( const rAltura: Extended ): Integer;
begin
     Result := Trunc( ( K_POR_CIEN * rAltura / K_METROS_PIES_VAL2 ) ) div K_METROS_PIES_VAL1;
end;

function GetPulgadas( const rAltura: Extended ): Integer;
begin
     Result := Trunc( ( K_POR_CIEN * rAltura / K_METROS_PIES_VAL2 ) ) mod K_METROS_PIES_VAL1;
end;

{ ************************** TdmMedico ******************************* }

procedure TdmMedico.DataModuleCreate(Sender: TObject);
begin
     cdsTConsulta.Tag := K_EDIT_CONSULTA;
     cdsTEstudio.Tag := K_EDIT_ESTUDIO;
     cdsTAccidente.Tag := K_EDIT_ACCIDENTE;
     cdsCausaAcc.Tag := K_EDIT_CAUSACC;
     cdsMotivoAcc.Tag := K_EDIT_MOTIVOACC;
     cdsMedicina.Tag := K_EDIT_MEDICINA;
     cdsDiagnostico.Tag := K_EDIT_DIAGNOSTICO;
     cdsKardexEmbarazo.Tag:=K_EDIT_EMBARAZOS;
     cdsMedicinaEntr.Tag := K_EDIT_MED_ENTR;
     cdsKardexConsultas.Tag := K_EDIT_KCONSULTAS;
     cdsKardexAccidente.Tag := K_EDIT_ACCIDENTES;
     cdsExpediente.Tag := K_EDIT_EXPEDIENTE;
     cdsHistorial.Tag := K_EDIT_PERMISOS;
     FClasificaciones := TListaClasificaciones.Create;
     FCampos := TListaCampos.Create;
     FEditor := TEditorCliente.Create;
     lCambiandoCB_CODIGO := False;
     lCambiandoEX_CODIGO := False;
     FValidaGrabado:= True;
     FRefrescaEntidades := True;
end;

procedure TdmMedico.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FEditor );
     FreeAndNil( FCampos );
     FreeAndNil( FClasificaciones );
end;

{$ifdef DOS_CAPAS}
function TdmMedico.GetServerMedico: TdmServerMedico;
begin
     Result := DCliente.dmCliente.ServerMedico;
end;
function TdmMedico.GetServerRecursos: TdmServerRecursos;
begin
     Result:= DCliente.dmCliente.ServerRecursos;
end;
function TdmMedico.GetServerAsistencia: TdmServerAsistencia;
begin
     Result:= DCliente.dmCliente.ServerAsistencia;
end;
{$else}
function TdmMedico.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidorRecursos ) );
end;
function TdmMedico.GetServerMedico: IdmServerMedicoDisp;
begin
     Result := IdmServerMedicoDisp( dmCliente.CreaServidor( CLASS_dmServerMedico, FServidor ) );
end;
function TdmMedico.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( dmCliente.CreaServidor( CLASS_dmServerAsistencia, FServidorAsistencia ) );
end;
{$endif}

procedure TdmMedico.Conectar;
 var oCamposEx : OleVariant;
begin
     cdsGruposEx.Data := ServerMedico.GetGruposEx(dmCliente.Empresa, oCamposEx);
     cdsCamposEx.Data := oCamposEx;
     CargarCampos;
end;

procedure TdmMedico.CargarCampos;
begin
     with FClasificaciones do
     begin
          Clear;
          with cdsGruposEx do
          begin
               First;
               while not Eof do
               begin
                    Add( FieldByName( 'GX_CODIGO' ).AsString,
                         FieldByName( 'GX_TITULO' ).AsString,
                         FieldByName( 'GX_POSICIO' ).AsInteger );
                    Next;
               end;
          end;
     end;

     with FCampos do
     begin
          Clear;
          with cdsCamposEx do
          begin
               First;
               while not Eof do
               begin
                    Add( eExCampoTipo( FieldByName( 'CX_TIPO' ).AsInteger ),
                         FieldByName( 'CX_TITULO' ).AsString,
                         FieldByName( 'CX_NOMBRE' ).AsString,
                         FieldByName( 'CX_DEFAULT' ).AsString,
                         FieldByName( 'GX_CODIGO' ).AsString,
                         FieldByName( 'CX_POSICIO' ).AsInteger,
                         eExMostrar( FieldByName( 'CX_MOSTRAR' ).AsInteger ) );
                    Next;
               end;
          end;
          SetReDraw;
     end;
end;

procedure TdmMedico.DescargarCamposEx;
 var i: integer;
begin
     with cdsCamposEx do
     begin
          EmptyDataSet;
          with FCampos do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Append;
                    try
                       with Campo[ i ] do
                       begin
                            FieldByName( 'CX_NOMBRE' ).AsString := Campo;
                            FieldByName( 'GX_CODIGO' ).AsString := Clasificacion;
                            FieldByName( 'CX_TITULO' ).AsString := Letrero;
                            FieldByName( 'CX_POSICIO' ).AsInteger := Posicion;
                            FieldByName( 'CX_DEFAULT' ).AsString := ValorDefault;
                            FieldByName( 'CX_MOSTRAR' ).AsInteger := Ord( Mostrar );
                            FieldByName( 'CX_TIPO' ).AsInteger := Ord( Tipo );
                       end;
                       Post;
                    except
                          Cancel;
                    end;
               end; //for
          end;//with
     end;//with
end;

procedure TdmMedico.DescargarGruposEx;
 var i: integer;
begin
     cdsGruposEx.EmptyDataSet;
     with FClasificaciones do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Clasificacion[ i ] do
               begin
                    with cdsGruposEx do
                    begin
                         Append;
                         try
                            FieldByName( 'GX_CODIGO' ).AsString := Codigo;
                            FieldByName( 'GX_TITULO' ).AsString := Nombre;
                            FieldByName( 'GX_POSICIO' ).AsInteger := Posicion;
                            Post;
                         except
                               Cancel;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TdmMedico.cdsGruposExAlEnviarDatos(Sender: TObject);
 var
    ErrorCount: Integer;
    oCamposExResult : OleVariant;
begin
     ErrorCount := 0;
     with cdsGruposEx do
     begin
          if Reconcile( ServerMedico.GrabaGruposEx( dmCliente.Empresa, DeltaNull, cdsCamposEx.Delta, oCamposExResult, ErrorCount ) ) AND
             cdsCamposEx.Reconcile( oCamposExResult ) then
          begin
               MergeChangelog;
               cdsCamposEx.MergeChangeLog;
          end;
     end;
end;

procedure TdmMedico.DescargarCampos;
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;

     try
        DescargarGruposEx;
        DescargarCamposEx;

        cdsGruposEx.Enviar;

        // --->>> OJO <<--------
        //Se destruye la forma de expediente para que la siguiente vez que se abra,
        //se redibujen los componentes.
        FreeAndNil(EditExpediente_DevEx);

     finally
            Screen.Cursor := oCursor;
     end;
end;

function tdmMedico.CheckAuto: Boolean;
begin
     {$ifdef CAROLINA}
     Result := TRUE;
     {$else}
     Result := Autorizacion.OkModulo(OkEnfermeria,True);
     if not Result then
         ZInformation( 'Versi�n Demo', 'Operaci�n No Permitida En Versi�n Demo' , 0);
     {$endif}

end;

procedure TdmMedico.AbrirConfiguracion;
 var oCamposCFG : TCamposCFG_DevEx;
begin
     oCamposCFG := TCamposCFG_DevEx.Create( Self );
     try
        with oCamposCFG do
        begin
             Campos.Assign( Self.FCampos );
             Clasificaciones.Assign( Self.FClasificaciones );
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  Self.FCampos.Assign( Campos );
                  Self.FClasificaciones.Assign( Clasificaciones );
                  DescargarCampos;
             end;
        end;
     finally
            FreeAndNil( oCamposCFG );
     end;
end;


procedure TdmMedico.cdsTablasAfterPost(DataSet: TDataSet);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( DataSet ) do
     begin
          if ( Changecount > 0 ) then
             if Reconcile( ServerMedico.GrabaTabla( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) then
             begin
                  if FRefrescaEntidades then
                     TressShell.SetDataChange( [ enExpediente ] );
             end;
     end;
end;

procedure TdmMedico.cdsTablasAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerMedico.GetTabla( dmCliente.Empresa, Tag );
     end;
end;

procedure TdmMedico.ShowEdicion(const iTabla : Integer);
begin
     case iTabla of
          K_EDIT_CONSULTA:    ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoConsulta, TTOEditTipoConsulta );
          K_EDIT_ESTUDIO:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoEstudio, TTOEditTipoEstudio );
          K_EDIT_ACCIDENTE:   ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoAccident, TTOEditTipoAccident );
          K_EDIT_CAUSACC:     ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoCausaAcc, TTOEditTipoCausaAcc );
          K_EDIT_MOTIVOACC:   ZBaseEdicion_DevEx.ShowFormaEdicion( EditMotivoAcc, TTOEditMotivoAcc );
          K_EDIT_DIAGNOSTICO: ZBaseEdicion_DevEx.ShowFormaEdicion( fDiagnosticomed_DevEx, TfDiagnosticomed_DevEx );
          K_EDIT_MEDICINA:    ZBaseEdicion_DevEx.ShowFormaEdicion( EditMedicina_DevEx, TEditMedicina_DevEx );
          K_EDIT_EMBARAZOS:   ZBaseEdicion_DevEx.ShowFormaEdicion( EditEmbarazos_DevEx, TEditEmbarazos_DevEx );
          K_EDIT_MED_ENTR:    ZBaseEdicion_DevEx.ShowFormaEdicion( EditMedEntr_DevEx, TEditMedEntr_DevEx );
          K_EDIT_KCONSULTAS:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditKConsulta_DevEx, TEditKConsulta_DevEx );
          K_EDIT_ACCIDENTES:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditAccidentes_DevEx, TEditAccidentes_DevEx );
          K_EDIT_EXPEDIENTE:  ZBaseEdicion_DevEx.ShowFormaEdicion( EditExpediente_DevEx, TEditExpediente_DevEx );
          K_EDIT_PERMISOS:    ZBaseEdicion_DevEx.ShowFormaEdicion( EditPermisos_DevEx, TEditPermisos_DevEx );
     end;
end;

procedure TdmMedico.cdsTablasAlModificar(Sender: TObject);
begin
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;


{ ********************cdsExpediente**********************************}
procedure TdmMedico.cdsExpedienteNewRecord(DataSet: TDataSet);
var
   oCampo : TCampo;
   i:Integer;
begin
     With DataSet do
     begin
          FieldByName('EX_CODIGO').AsInteger := dmCliente.Expediente;
          FieldByName('CB_CODIGO').AsInteger := EmpleadoNuevo;
          FieldByName('EX_FEC_INI').AsDateTime := dmCliente.FechaDefault;
          FieldByName('EX_SEXO').AsInteger := Ord(esFemenino);
          FieldByName('EX_FEC_NAC').AsDateTime := dmCliente.FechaDefault;
     end;
     for i:=0 to ( FCampos.Count - 1 ) do
     begin
          oCampo := FCampos.Campo[i];
          if StrLleno(oCampo.Clasificacion)then
          begin
               With cdsExpediente.FieldByName(oCampo.Campo) do
               begin
                    if(oCampo.Tipo IN [xctBooleano,xctBoolStr])then
                        AsString:= StrDef(oCampo.ValorDefault,K_GLOBAL_NO)
                    else
                        AsString:= oCampo.ValorDefault;
               end;//with
          end;//if
     end;//for
     FRefrescaEntidades := False;
end;

procedure TdmMedico.cdsExpedienteAlBorrar(Sender: TObject);
begin
     if ZetaMsgDlg.ConfirmaCambio( Format( '� Desea Borrar El Expediente del %s ? ',
                                           [dmCliente.GetExpedienteDescripcion] ) ) then
     begin
          cdsExpediente.Delete;
     end;
end;

procedure TdmMedico.cdsExpedienteAfterDelete(DataSet: TDataSet);
begin
     cdsExpediente.Enviar;
     dmCliente.PosicionaSiguienteExpediente;
end;

procedure TdmMedico.cdsExpedienteAlAdquirirDatos(Sender: TObject);
var
   oConsultas : OleVariant;
   oParams: TZetaParams;
begin
     oParams := TZetaParams.Create;
     try
        dmCliente.CargaActivosTodos( oParams );
        cdsExpediente.Data:=ServerMedico.GetExpediente(dmCliente.Empresa,dmCliente.Expediente, oParams.VarValues,
                                                     oConsultas );
        cdsKardexConsultas.Data:=oConsultas;
     finally
            FreeAndNil( oParams );
     end;
end;

procedure TdmMedico.cdsExpedienteAlCrearCampos(Sender: TObject);
const
     K_CAMPOS_NUMERICOS = 30;
var
   i: Integer;
   sCampo: String;
begin
     With cdsExpediente do
      begin
           dmSistema.cdsUsuarios.Conectar;
           MaskFecha( 'EX_FEC_INI' );
           MaskFecha( 'EX_EMB_INI' );
           MaskFecha( 'EX_EMB_FIN' );
           MaskFecha( 'EX_FEC_NAC' );
           for i:= 1 to K_CAMPOS_NUMERICOS do
           begin
                sCampo := Format( 'EX_G_NUM%s' ,[ strZero( IntToStr( i ), 2 ) ] );
                MaskPesos( sCampo );
           end;
           CreateSimpleLookup(dmSistema.cdsUsuariosLookup,'US_NOMBRE','US_CODIGO');
      end;
end;

procedure TdmMedico.cdsExpedienteAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iExpediente: Integer;
   lHayQueRefrescar: Boolean;
begin
     ErrorCount := 0;
     FRefrescaEntidades := True;
     with cdsExpediente do
     begin
          {if State in [ dsEdit, dsInsert ] then
             Post;}
          lHayQueRefrescar := ( State = dsInsert );
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               iExpediente := FieldByName('EX_CODIGO').AsInteger ;
               if Reconcile( ServerMedico.GrabaExpediente( dmCliente.Empresa, Delta, ErrorCount, iExpediente ) ) then
               begin
                    //if ErrorCount = 1 then Edit;
                    if ( ErrorCount = 0 ) and ( FieldByName( 'EX_CODIGO' ).AsInteger <> iExpediente ) then
                    begin
                         Edit;
                         FieldByName( 'EX_CODIGO' ).AsInteger := iExpediente;
                         Post;
                         MergeChangeLog;
                    end;
                    TressShell.SetDataChange( [ enExpediente, enConsulta ] );
                    if( lHayQueRefrescar )then
                        cdsCodigos.SetDataChange;
               end;
          end;
    end;
end;

procedure TdmMedico.cdsExpedienteBeforePost(DataSet: TDataSet);
begin
     With cdsExpediente do
      begin                       
           if strLleno( FieldByName('EX_NOMBRES').asString ) and strLleno( FieldByName('EX_APE_PAT').asString ) then
           begin
                case eExTipo(Fieldbyname('EX_TIPO').asInteger) of
                     exEmpleado, exPariente:
                     begin
                          if Fieldbyname('CB_CODIGO').AsInteger = 0 then
                          begin
                               DatabaseError('El N�mero de Empleado no Puede Quedar Vac�o');
                          end;
                     end;
                     exCandidato, exOtro:
                     begin
                          {Se asigna el valor cero al CB_CODIGO, para que posteriormente,
                          al hacer el join con COLABORA no se traiga ningun valor.
                          Tambien se asigna cero, para que al navegar en el Shell no aparezca
                          ning�n n�mero de empleado}
                          Fieldbyname('CB_CODIGO').AsInteger := 0;
                     end;
                end;
                Fieldbyname('US_CODIGO').AsInteger := dmCliente.Usuario;
           end
           else
               DB.DatabaseError( 'El Apellido Paterno y Nombre No Pueden Quedar Vac�os' );
      end;
end;

procedure TdmMedico.cdsTablasBeforePost(DataSet: TDataSet);
begin
     if ( strVacio(DataSet.Fields[ 0 ].asString) ) then
        DB.DatabaseError( 'C�digo No Puede Quedar Vac�o' );
end;

procedure TdmMedico.cdsTablasGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( GetIndexRights( Sender.Tag ), iRight );
end;

{$ifdef VER130}
procedure TdmMedico.cdsTablasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$else}
procedure TdmMedico.cdsTablasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$endif}

 //MA:Agregue el cdsTEstudio a los mismos eventos del cdsTconsulta 16/02/2002

function TdmMedico.GetIndexRights( const iTabla: Integer ): Integer;
begin
     case iTabla of
          K_EDIT_CONSULTA:   Result := D_CAT_TIPO_CONSULTA;
          K_EDIT_ESTUDIO:    Result := D_CAT_TIPO_EST_LAB;
          K_EDIT_ACCIDENTE:  Result := D_CAT_TIPO_ACC;
          K_EDIT_CAUSACC:    Result := D_CAT_CAU_ACC;
          K_EDIT_MOTIVOACC:  Result := D_CAT_MOT_ACC;
          K_EDIT_MEDICINA:   Result := D_CAT_MEDICAMENTOS;
          K_EDIT_DIAGNOSTICO:Result := D_CAT_DIAGNOSTICO;
          K_EDIT_EMBARAZOS : Result := D_EMP_KARDEX_EMBARAZOS;
          K_EDIT_MED_ENTR :  Result := D_MEDICINA_ENTR;
          K_EDIT_KCONSULTAS: Result := D_EMP_KARDEX_CONSULTAS;
          K_EDIT_ACCIDENTES: Result := D_EMP_KARDEX_ACCIDENTES;
     else
         Result := 0;
     end;
end;

procedure TdmMedico.CN_TEMPERAChange(Sender: TField );
begin
     if not FUpdating and Assigned( Sender ) then
     begin
          FUpdating := True;
          try
             with Sender do
             begin
                  Dataset.FieldByName( 'CN_FAHREN' ).AsFloat := GetFahrenheit( AsFloat );
             end;
          finally
                 FUpdating := False;
          end;
     end;
end;

procedure TdmMedico.CN_FAHRENChange(Sender: TField );
begin
     if not FUpdating and Assigned( Sender ) then
     begin
          FUpdating := True;
          try
             with Sender do
             begin
                  Dataset.FieldByName( 'CN_TEMPERA' ).AsFloat := GetCentigrados( AsFloat );
             end;
          finally
                 FUpdating := False;
          end;
     end;
end;

procedure TdmMedico.CN_PESOChange(Sender: TField );
begin
     if not FUpdating and Assigned( Sender ) then
     begin
          FUpdating := True;
          try
             with Sender do
             begin
                  Dataset.FieldByName( 'CN_LIBRAS' ).AsFloat := GetLibras( AsFloat );
             end;
          finally
                 FUpdating := False;
          end;
     end;
end;

procedure TdmMedico.CN_LIBRASChange(Sender: TField );
begin
     if not FUpdating and Assigned( Sender ) then
     begin
          FUpdating := True;
          try
             with Sender do
             begin
                  Dataset.FieldByName( 'CN_PESO' ).AsFloat := GetKilos( AsFloat );
             end;
          finally
                 FUpdating := False;
          end;
     end;
end;

procedure TdmMedico.CN_ALTURAChange(Sender: TField );
begin
     if not FUpdating and Assigned( Sender ) then
     begin
          FUpdating := True;
          try
             with Sender do
             begin
                  Dataset.FieldByName( 'CN_PIES' ).AsFloat := GetPies( AsFloat );
                  Dataset.FieldByName( 'CN_PULGADAS' ).AsFloat := GetPulgadas( AsFloat );
             end;
          finally
                 FUpdating := False;
          end;
     end;
end;

procedure TdmMedico.CN_PIESChange(Sender: TField );
begin
     if not FUpdating and Assigned( Sender ) then
     begin
          FUpdating := True;
          try
             with Sender.DataSet do
             begin
                  FieldByName( 'CN_ALTURA' ).AsFloat := GetMetros( FieldByName('CN_PIES').AsInteger, FieldByName('CN_PULGADAS').AsInteger );
             end;
          finally
                 FUpdating := False;
          end;
     end;
end;

{ ******************* cdsTAccidente **************************}
procedure TdmMedico.cdsTAccidenteAlCrearCampos(Sender: TObject);
begin
     cdsTAccidente.ListaFija('TB_TIPO',lfTipoAccidente);
end;

{ ******************** cdsTCausaAcc **************************}
procedure TdmMedico.cdsCausaAccAlCrearCampos(Sender: TObject);
begin
     TZetaClientDataset( Sender ).ListaFija('TB_TIPO',lfMotivoAcc);
end;

procedure TdmMedico.cdsSoloExpedienteAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerMedico.GetSoloExpediente( dmCliente.Empresa,dmCliente.Expediente );
     end;
end;

{ ******************* cdsHistorial *******************************}
procedure TdmMedico.cdsHistorialAlAdquirirDatos(Sender: TObject);
begin
     if EsEmpleado then
        cdsHistorial.Data:=ServerMedico.GetHistorial(dmCliente.Empresa,dmCliente.Empleado)
     else
         cdsHistorial.Close;
end;

procedure TdmMedico.cdsHistorialAlAgregar(Sender: TObject);
begin
     if EsEmpleado then
     begin
          case FTipoIncaPerm of
               0:
               begin
                    cdsHisIncapaci.Conectar;
                    cdsHisIncapaci.Append;
                    ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisIncapaci_DevEx, TEditHisIncapaci_DevEx );
               end;
               1:
               begin
                    cdsHisPermiso.Conectar;
                    cdsHisPermiso.Append;
                    ShowEdicion(cdsHistorial.Tag);
               end;
          end;
     end
     else
         ZetaDialogo.ZError( 'Historial de Permisos/Incapacidades', 'Solamente se pueden agregar registros a empleados', 0 );

     {if ( eExTipo( dmCliente.cdsExpediente.FieldByName('EX_TIPO').AsInteger ) = exEmpleado )  then
     begin
          cdsHisPermiso.Conectar;
          cdsHisPermiso.Append;
          ShowEdicion(cdsHistorial.Tag);
     end
     else
         ZetaDialogo.ZError('Historial de Permisos', 'Solamente se Pueden Agregar Permisos a Empleados', 0);
            }
end;

procedure TdmMedico.cdsHistorialAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsIncidencias.Conectar;
     with cdsHistorial do
     begin
          MaskFecha( 'IN_FEC_INI' );
          MaskFecha( 'IN_FEC_FIN' );
          with FieldByName('IN_CLASIFI') do
          begin
               OnGetText := ObtieneClasifi;
               Alignment := taLeftJustify;
          end;
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'TB_ELEMENT', 'IN_TIPO' );
     end;
end;

procedure TdmMedico.cdsHistorialAlModificar(Sender: TObject);
begin
     if EsPermiso then
     begin
          cdsHisPermiso.Refrescar;
          ShowEdicion( cdsHistorial.Tag );
     end
     else
     begin
          cdsHisIncapaci.Refrescar;
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisIncapaci_DevEx, TEditHisIncapaci_DevEx );
     end;
    {
     if(cdsHistorial.FieldByName('TIPO').asString = K_TIPO_PERMISO)then
     begin
          cdsHisPermiso.Refrescar;
          ShowEdicion(cdsHistorial.Tag);
     end;}

end;

procedure TdmMedico.cdsHistorialAlBorrar(Sender: TObject);
begin
     if EsPermiso then
     begin
          with cdsHisPermiso do
          begin
               Refrescar;
               if ZetaMsgDlg.ConfirmaCambio( '�Desea borrar este permiso?' ) then
               begin
                    Delete;
                    Enviar;
               end;
          end;
     end
     else
     begin
          with cdsHisIncapaci do
          begin
               Refrescar;
               if ZetaMsgDlg.ConfirmaCambio( '�Desea borrar esta incapacidad?' ) then
               begin
                    Delete;
                    Enviar;
               end;
          end;
     end;
end;

function TdmMedico.EsPermiso: boolean;
begin
     Result := ( cdsHistorial.FieldByName( 'TIPO' ).asString = K_TIPO_PERMISO );
end;

function TdmMedico.EsEmpleado: boolean;
begin
     Result := ( eExTipo( dmCliente.cdsExpediente.FieldByName( 'EX_TIPO' ).AsInteger ) = exEmpleado );
end;

procedure TdmMedico.ObtieneClasifi(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with cdsHistorial do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    if (FieldByName('TIPO').AsString = K_TIPO_PERMISO) then
                       Text := ObtieneElemento( lfTipoPermiso, Sender.AsInteger )
                    else
                        Text := VACIO;
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;

//*********************Resumen de Consulta ********************************//
procedure TdmMedico.CreaResumenConsulta(oRichText: TcxRichEdit);
const
     K_TAMANO_LETRA = 8;
var
   sSubsecuente, sResultados_IMSS: String;

        procedure Exploracion_Fisica;
        const aExpFisica : Array[ 0..5 ] of String =
             ( 'CN_PRE_SIS',
               'CN_PULSO',
               'CN_ALTURA',
               'CN_PESO',
               'CN_TEMPERA',
               'CN_AV_IZQ');
        var
           i: Integer;
           lEscribeCampos: Boolean;
        begin
             With cdsKardexConsultas do
             begin
                  lEscribeCampos := False;
                       for i:= Low( aExpFisica ) to High( aExpFisica ) do
                       begin
                            if ( FieldByName( aExpFisica[ i ] ).AsInteger > 0 ) then
                            begin
                                 lEscribeCampos := True;
                                 Break;
                            end;
                       end;
                  if lEscribeCampos or strLleno( FieldByName('CN_SINTOMA').asString ) or
                     strLleno( FieldByName('CN_OBSERVA').asString ) then
                  begin
                       With FEditor do
                       begin
                            RenglonVacio;
                            Clasificaciones('Exploraci�n F�sica');
                            if (FieldByName('CN_PRE_SIS').asInteger > 0 )then
                            begin
                                 EscribeCampos('Presi�n Arterial:');
                                 CamposConsulta(inttostr(FieldByName('CN_PRE_SIS').asInteger)+' / '+inttostr(FieldByName('CN_PRE_DIA').asInteger));
                            end;
                            Condicion_Integer('Pulso:',FieldByName('CN_PULSO').asInteger, VACIO);
                            Condicion_Integer('Temperatura:',FieldByName('CN_Tempera').asInteger,' �C.');
                            Condicion_Float('Altura:',FieldByName('CN_ALTURA').asFloat,'Mts.');
                            Condicion_Float('Peso:',FieldByName('CN_PESO').asFloat,'Kgs.');
                            if (FieldByName('CN_AV_IZQ').asInteger > 0 )then
                            begin
                                 EscribeCampos('Agudeza Visual:');
                                 CamposConsulta(InttoStr(FieldByName('CN_AV_IZQ').AsInteger)+' / '+InttoStr(FieldByName('CN_AV_DER').AsInteger));
                            end;
                            RenglonVacio;
                            Condicion_String('S�ntoma:',FieldByName('CN_SINTOMA').AsString, VACIO);
                            Condicion_String('Observaciones:',FieldByName('CN_OBSERVA').AsString, VACIO);
                       end;
                  end;//if Exploracion Fisica
             end;
        end;

        procedure Diagnostico;
         begin
          With cdsKardexConsultas do
            begin
                 if( strLleno( FieldByName('CN_EXPLORA').AsString )or
                     strLleno( FieldByName('DA_CODIGO').AsString )or
                     strLleno( FieldByName('CN_DIAGNOS').AsString )or
                     strLleno( FieldByName('CN_RECETA').AsString )or
                     strLleno( FieldByName('CN_CUIDADO').AsString ) )then
                     begin
                          With FEditor do
                          begin
                               RenglonVacio;
                               Clasificaciones('Diagn�stico');
                               Condicion_String('Exploraci�n F�sica:',FieldByName('CN_EXPLORA').AsString, VACIO );
                               Condicion_String('Diagn�stico:',FieldByName('DA_DESCRIP').AsString, VACIO );
                               Condicion_String('Detalle:',FieldByName('CN_DIAGNOS').AsString, VACIO );
                               RenglonVacio;
                               Condicion_String('Cuidados Generales:',FieldByName('CN_CUIDADO').AsString, VACIO );
                               RenglonVacio;
                               Condicion_String('Receta:',FieldByName('CN_RECETA').AsString, VACIO );
                          end;
                       end;
                end;
         end;//if Diagnostico

         procedure Estudio_Laboratorio;
         begin
              With cdsKardexConsultas do
              begin
                   if ( strLleno( FieldByName('CN_EST_TIP').AsString )or
                        strLleno( FieldByName('CN_EST_DES').AsString )or
                        zStrToBool( FieldByName('CN_EST_HAY').AsString )or // = 'S'
                        zStrToBool( FieldByName('CN_IMSS').AsString )or
                        strLleno( FieldByName('CN_EST_OBS').AsString ) ) then
                   begin
                        With FEditor do
                        begin
                             RenglonVacio;
                             Clasificaciones('Estudio de Laboratorio');
                             Condicion_String('Tipo de Estudio:',FieldByName('TB_ELEMENT').AsString, VACIO );
                             Condicion_String('Descripci�n:',FieldByName('CN_EST_DES').AsString, VACIO );
                             EscribeCampos('Resultados Entregados al Paciente: ');
                             if zStrToBool( FieldByName('CN_EST_HAY').AsString )then // = 'N'
                             begin
                                  CamposConsulta( K_MEDICO_SI );
                                  EscribeCampos('Fecha de Realizaci�n:');
                                  CamposConsulta(Datetostr(FieldByName('CN_EST_FEC').AsDateTime));
                                  Condicion_String('Resultados:',FieldByName('CN_EST_RES').AsString, VACIO );
                             end
                             else
                                 CamposConsulta( K_MEDICO_NO );
                            RenglonVacio;
                            Condicion_String('Observaciones:',FieldByName('CN_EST_OBS').AsString, VACIO );
                        end;
                   end;
              end;
         end;//Estudio_laboratorio

begin
     oRichText.Lines.BeginUpdate;
     try
        With cdsKardexConsultas do
        begin
             With FEditor do
             begin
                  Inicializa(oRichText);
                  Titulo('Resumen de la Consulta');
                  RenglonVacio;
                  CamposDefault('Paciente: ' ,dmCliente.GetDatosExpedienteActivo.Nombre );
                  CamposDefault('Fecha de la Consulta:',DateToStr(FieldByName('CN_FECHA').asDateTime) );//'',9,taLeftJustify,'Courier New',[]);
                  CamposDefault('Inici� a las:', FormatMaskText( '00:00;0', FieldByName('CN_HOR_INI').asString ) +' Hrs.' );
                  CamposDefault('Termin� a las:', FormatMaskText( '00:00;0', FieldByName('CN_HOR_FIN').asString ) +' Hrs.' );
                  CamposDefault('Tipo:', FieldByName('CN_TIPO_CONS').asString );
                  if strLleno( FieldByName('CN_MOTIVO').asString )then
                     CamposDefault('Motivo:', FieldByName('CN_MOTIVO').asString );
                  if zStrToBool(FieldByName('CN_IMSS').AsString )then
                       sResultados_IMSS := K_MEDICO_SI
                  else
                       sResultados_IMSS := K_MEDICO_NO;
                  if zStrToBool( FieldByName('CN_SUB_SEC').asString ) then
                      sSubsecuente := K_MEDICO_SI
                  else
                      sSubsecuente := K_MEDICO_NO;
                  CamposDefault('�Se Refiri� al IMSS? ', sResultados_IMSS );
                  CamposDefault('�Consulta Subsecuente? ', sSubsecuente );
             end;
             Exploracion_Fisica;
             Estudio_Laboratorio;
             Diagnostico;
             FEditor.Pos_Inicio(oRichText);
        end;
     finally
            oRichText.Lines.EndUpdate;
     end;
end;

//*****************CreaResumendeExpediente*********************************//
procedure TdmMedico.CreaResumenExpediente(oRichText: TcxRichEdit);
var
   i,j:Integer;
   oClasificacion:TClasificacion;
   oCampo:TCampo;
   oField:TField;
   sLetrero,sCampo:String;
   lLetrero:Boolean;
begin
    oRichText.Lines.BeginUpdate;
    try
       With FEditor do
       begin
            Inicializa(oRichText);
            Titulo('Historial Cl�nico');
            RenglonVacio;
            with cdsExpediente do
            begin
                 if zStrToBool( FieldByName('EX_STATUS_ACC').AsString ) then
                    CamposExpediente( 'Empleado(a) Accidentado(a)' );
                 EscribeCampos('Paciente: ');
                 CamposExpediente( dmCliente.GetDatosExpedienteActivo.Nombre );
                 EscribeCampos('Tipo:');
                 CamposExpediente(ZetaCommonLists.ObtieneElemento( lfExTipo, FieldByName('EX_TIPO').AsInteger ) );

                 if zStrToBool( FieldByName('EX_STATUS_EMB').AsString ) then
                 begin
                      EscribeCampos('Estado Actual:');
                      CamposExpediente( 'Embarazada' );
                      EscribeCampos('Semanas de Gestaci�n:');
                      CamposExpediente( GestacionSemanas( dmCliente.FechaDefault, FieldByName('EX_FEC_UM').ASDateTime ) );
                      EscribeCampos('Fecha Probable de Incapacidad:');
                      CamposExpediente(DatetoStr(FieldByName('EX_PRENAT').AsDateTime));
                      EscribeCampos('A:');
                      CamposExpediente(DatetoStr(FieldByName('EX_POSNAT').AsDateTime));
                      if zStrToBool( FieldByName('EX_RIESGO').AsString ) then
                      begin
                           EscribeCampos('El Embarazo es de Riesgo');
                           RenglonVacio;
                           EscribeCampos('Inicio de la Incapacidad:');
                           CamposExpediente( DatetoStr(FieldByName('EX_INC_INI').AsDateTime) );
                           EscribeCampos('Fin de la Incapacidad:');
                           CamposExpediente( DatetoStr(FieldByName('EX_INC_FIN').AsDateTime) );
                      end;
                 end;
            end;
       end;
       For i:=0 to FClasificaciones.Count-1 do
       begin
            oClasificacion:=FClasificaciones.Clasificacion[i];
            FEditor.RenglonVacio;
            FEditor.Clasificaciones(oClasificacion.Nombre);
            For j:=0 to FCampos.Count-1 do
            begin
                 Application.ProcessMessages;
                 oCampo:=FCampos.Campo[j];
                 With oCampo do
                 begin
                       if( Clasificacion = oClasificacion.Codigo)then
                       begin
                            if( Mostrar <> arNunca)then
                            begin
                                 with cdsExpediente do
                                 begin
                                      if ( FindField( oCampo.Campo ) <> nil ) then
                                         oField := FieldByName( oCampo.Campo )
                                      else
                                      begin
                                           oField := nil;
                                           Databaseerror( Format( 'El Campo %s no Existe en la Tabla Expediente' , [oCampo.Campo ] ) );
                                      end;
                                 end;
                                  //arSiempre, arNunca, arVacio, arLleno
                                  sLetrero := Letrero+':';
                                  if(Tipo IN [xctBooleano,xctBoolStr])then
                                  begin
                                       if(zStrToBool( oField.AsString ))then
                                       begin
                                            sCampo:=K_BOOLEANO_SI;
                                       end
                                       else
                                            sCampo:=K_BOOLEANO_NO;
                                       if(Tipo = xctBoolStr)then
                                            sCampo:=sCampo+'-'+cdsExpediente.FieldByName( oCampo.CampoTexto ).AsString;
                                  end
                                  else
                                       sCampo:=oField.AsString;
                                  lLetrero := ( Mostrar = arSiempre );
                                  if not lLetrero then
                                  begin
                                       Case Tipo of
                                           xctTexto: lLetrero:= strVacio(oField.AsString);
                                           xctNumero:  lLetrero:= ( oField.AsFloat = 0 );
                                           xctBooleano,xctBoolStr: lLetrero:= not zStrToBool( oField.AsString );
                                       end;//case
                                       if ( Mostrar = arLleno ) then
                                          lLetrero := not lLetrero;
                                   end;//if
                                   if lLetrero then
                                   begin
                                        with FEditor do
                                        begin
                                             EscribeCampos( sLetrero );
                                             CamposExpediente( sCampo );
                                        end;
                                   end;//if
                             end;//if
                       end;//if
                 end;//With
            end;//for
       end;//for
       FEditor.Pos_Inicio(oRichText);
    finally
           oRichText.Lines.EndUpdate;
    end;
end;

{******************* cdsHisPermiso ********************}
procedure TdmMedico.cdsHisPermisoAfterDelete(DataSet: TDataSet);
begin
     cdsHisPermiso.Enviar;
end;

procedure TdmMedico.cdsHisPermisoAlAdquirirDatos(Sender: TObject);
begin
     if EsEmpleado then
     begin
          with cdsHisPermiso do
          begin
               with dmCliente do
                    Data := ServerRecursos.GetHisPermiso( Empresa, Empleado );

               Locate( 'PM_FEC_INI', cdsHistorial.FieldByName( 'IN_FEC_INI' ).AsDAteTime, [] );
          end;
     end
     else
         cdsHisPermiso.Close;
end;

procedure TdmMedico.cdsHisPermisoAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisPermiso do
     begin
          ListaFija( 'PM_CLASIFI', lfTipoPermiso );
          MaskFecha( 'PM_FEC_INI' );
          MaskFecha( 'PM_FEC_FIN' );
          FieldByName('PM_FEC_INI').OnChange := OnPM_FEC_INIChange;
          FieldByName('PM_DIAS').OnChange := OnPM_FEC_INIChange; 
          FieldByName('PM_FEC_FIN').OnChange := OnPM_FEC_FINChange;
          FieldByName( 'PM_CLASIFI' ).OnValidate := OnPM_CLASIFIValidate;
          FieldByName( 'PM_CLASIFI' ).OnChange := OnPM_CLASIFIChange;
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'TB_ELEMENT', 'PM_TIPO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmMedico.OnPM_CLASIFIChange(Sender: TField);
begin
{      if( not ValidarMotivoPermisos(cdsHisPermiso.FieldByName('PM_TIPO').AsString,cdsHisPermiso.FieldByName('PM_CLASIFI').AsInteger )then
          cdsHisPermiso.FieldByName('PM_TIPO').AsString := VACIO;}
end;


function TdmMedico.ValidarMotivoPermisos(const sCodigo :string; const iTipo:Integer ):Boolean;
begin
     Result := StrVacio(sCodigo);
     if  not Result  then
     begin
          with dmTablas.cdsIncidencias do
          begin
               Result := (  Locate('PM_TIPO',UpperCase(sCodigo),[]) ) and
                         ( ( FieldByName('TB_PERMISO').AsInteger = K_MOTIVO_PERMISOS_OFFSET ) or
                           ( FieldByName('TB_PERMISO').AsInteger = iTipo  )
                         );
          end;
     end;
end;



{$ifdef VER130}
procedure TdmMedico.cdsHisPermisoReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccion( DataSet, FCancelaAjuste, K_SUB_PERMISO, 'Error al Registrar Permiso', E.Message );
end;
{$else}
procedure TdmMedico.cdsHisPermisoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccionGetDias( DataSet, FCancelaAjuste, K_SUB_PERMISO, 'Error al Registrar Permiso', E.Message,  GetPermisoDiasHabiles );
end;
{$endif}


procedure TdmMedico.cdsHisPermisoAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisPermiso do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               FCancelaAjuste := FALSE;
               Repeat
               Until ( FCancelaAjuste or Reconcile( ServerRecursos.GrabaHistorial( dmCliente.Empresa, Tag, Delta, ErrorCount ) ) );
               if ErrorCount = 0 then
                  TressShell.SetDataChange( [ enPermiso ] );
          end;
     end;
end;


procedure TdmMedico.cdsHisPermisoBeforePost(DataSet: TDataSet);
begin
     with cdsHisPermiso do
     begin
          if ( FieldByName( 'PM_DIAS').AsInteger <= 0 ) then
              DataBaseError( 'Dias de Permiso debe ser mayor a 0');
          if not StrLleno(FieldByName( 'PM_TIPO').AsString ) then
              DataBaseError( 'Tipo de Permiso no puede quedar vac�o') ;
          if (FieldByName( 'PM_FEC_FIN').AsDateTime < (FieldByName( 'PM_FEC_INI').AsDateTime + 1)) then
             DataBaseError('Fecha de regreso debe ser mayor/igual a Fecha Inicio m�s 1 d�a');
          FieldByName( 'US_CODIGO').AsInteger   := dmCliente.Usuario;
          FieldByName( 'PM_CAPTURA').AsDateTime := dmCliente.FechaDefault;
     end;
end;

procedure TdmMedico.cdsHisPermisoNewRecord(DataSet: TDataSet);
const
     K_TIPO_CON_GOCE = 0;
     K_TIPO_SIN_GOCE = 1;
begin
     with cdsHisPermiso do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('PM_FEC_INI').AsDateTime := dmCliente.FechaDefault;
          FieldByName('PM_DIAS').AsInteger := 1;
          if DerechoTipoPermiso( K_TIPO_CON_GOCE ) then
              FieldByName('PM_CLASIFI').AsInteger := Ord( tpConGoce)
          else
          begin
               if ( not DerechoTipoPermiso( K_TIPO_CON_GOCE ) )
               and( not DerechoTipoPermiso( K_TIPO_SIN_GOCE ) )  then
                    DataBaseError( 'No Tiene Derecho Para Agregar Ninguna Clase de Permiso' )
               else
                   FieldByName('PM_CLASIFI').AsInteger := Ord( tpSinGoce);
          end;
          FieldByName( 'PM_GLOBAL' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmMedico.OnPM_FEC_INIChange(Sender: TField);
var
   DiasRango : Double; // ??
   oCursor: TCursor;
begin 
     cdsHisPermiso.FieldByName( 'PM_FEC_FIN').OnChange := nil;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     with cdsHisPermiso do
     begin
          if Global.GetGlobalBooleano( K_GLOBAL_PERMISOS_DIAS_HABILES ) then
          begin
               FieldByName( 'PM_FEC_FIN').AsDateTime := GetPermisoFechaRegreso(dmCliente.Empleado,FieldByName( 'PM_FEC_INI').AsDateTime,FieldByName( 'PM_DIAS').AsInteger, DiasRango);
          end
          else
          begin
               FieldByName( 'PM_FEC_FIN').AsDateTime := FieldByName( 'PM_FEC_INI').AsDateTime + FieldByName( 'PM_DIAS').AsInteger;
          end;
     end;
     Screen.Cursor := oCursor;
     cdsHisPermiso.FieldByName( 'PM_FEC_FIN').OnChange := OnPM_FEC_FINChange;
end;

procedure TdmMedico.OnPM_FEC_FINChange(Sender : TField);
var
   oCursor: TCursor;
begin
     cdsHisPermiso.FieldByName( 'PM_DIAS').OnChange := nil;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     cdsHisPermiso.FieldByName( 'PM_DIAS').AsFloat := GetPermisoDiasHabiles( dmCliente.Empleado,cdsHisPermiso.FieldByName( 'PM_FEC_INI').AsDateTime,cdsHisPermiso.FieldByName( 'PM_FEC_FIN').AsDateTime);
     Screen.Cursor := oCursor;
     cdsHisPermiso.FieldByName( 'PM_DIAS').OnChange := OnPM_FEC_INIChange;
end;

procedure TdmMedico.OnIN_FEC_INIChange(Sender : TField );
begin
      with Sender.DataSet do
      begin
          FieldByName( 'IN_FEC_FIN').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime +
                                                   FieldByName( 'IN_DIAS').AsInteger;
          FieldByName( 'IN_SUA_INI').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime ;
          FieldByName( 'IN_FEC_RH').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime;
      end;
end;

procedure TdmMedico.OnIN_DIASChange(Sender : TField );
begin
      with Sender.DataSet do
      begin
           FieldByName( 'IN_FEC_FIN').AsDateTime := FieldByName( 'IN_FEC_INI').AsDateTime +
                                                   FieldByName( 'IN_DIAS').AsInteger;
           FieldByName( 'IN_SUA_FIN').AsDateTime := FieldByName( 'IN_SUA_INI').AsDateTime +
                                                   FieldByName( 'IN_DIAS').AsInteger;
      end;
end;

procedure TdmMedico.OnIN_SUA_INIChange(Sender : TField );
begin
      with Sender.DataSet do
         FieldByName( 'IN_SUA_FIN').AsDateTime := FieldByName( 'IN_SUA_INI').AsDateTime +
                                                  FieldByName( 'IN_DIAS').AsInteger;
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmMedico.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmMedico.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enExpediente, Entidades ) OR ( Estado = stExpediente ) then
     {$else}
     if ( enExpediente in Entidades ) OR ( Estado = stExpediente ) then
     {$endif}

     begin
          cdsExpediente.SetDataChange;
          cdsKardexConsultas.SetDataChange;
          cdsKardexEmbarazo.SetDataChange;
          cdsKardexAccidente.SetDataChange;
          cdsMedicinaEntr.SetDataChange;
          cdsHistorial.SetDataChange;
          cdsDatosEmpleado.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPermiso, Entidades ) then
     {$else}
     if ( enPermiso in Entidades ) then
     {$endif}

          cdsHistorial.SetDataChange;
end;

procedure TdmMedico.OnPM_CLASIFIValidate(Sender: TField);
begin
     with Sender do
          if not DerechoTipoPermiso( AsInteger ) then
             DataBaseError( 'No Tiene Derecho Para Permisos de Clase = ' + ObtieneElemento( lfTipoPermiso, AsInteger ) );
end;

function TdmMedico.DerechoTipoPermiso( const Tipo : Integer ): Boolean;
begin
     case eTipoPermiso( Tipo ) of
          tpConGoce : Result := ZAccesosMgr.CheckDerecho( D_EMP_CLASE_PERMISO, TP_CON_GOCE );
          tpSinGoce : Result := ZAccesosMgr.CheckDerecho( D_EMP_CLASE_PERMISO, TP_SIN_GOCE );
     else
          Result := TRUE;
     end;
end;

{***************** cdsKardexAccidente *************************}
procedure TdmMedico.cdsKardexAccidenteAfterDelete(DataSet: TDataSet);
begin
     cdsKardexAccidente.Enviar;
end;

procedure TdmMedico.cdsKardexAccidenteAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerMedico.GetKardexAccidente( dmCliente.Empresa,dmCliente.Expediente );
     end;
end;

procedure TdmMedico.cdsKardexAccidenteAlCrearCampos(Sender: TObject);
begin
     With cdsKardexAccidente do
      begin
           cdsCausaAcc.Conectar;
           cdsTAccidente.Conectar;
           dmSistema.cdsUsuarios.Conectar;
           MaskFecha( 'AX_FECHA' );
           MaskFecha( 'AX_FEC_REG' );
           MaskTime( 'AX_HORA' );
           MaskTime( 'AX_HOR_SUS' );
           MaskFecha( 'AX_FEC_SUS' );
           MaskFecha( 'AX_FEC_COM' );
           MaskTime( 'AX_HOR_COM' );
           cdsKardexAccidente.ListaFija('AX_TIP_LES',lfTipoLesion);
           CreateSimpleLookup(cdsCausaAcc,'TB_DESCRIP','AX_CAUSA');
           CreateSimpleLookup(cdsTAccidente,'TB_DESCRIP2','AX_TIP_ACC');
           CreateSimpleLookup(dmSistema.cdsUsuariosLookup,'US_NOMBRE','US_CODIGO');
      end;
end;

procedure TdmMedico.cdsKardexAccidenteAlAgregar(Sender: TObject);
begin
      with dmCliente do
     begin
          if ( cdsExpediente.FieldByName('EX_TIPO').AsInteger = Ord( exEmpleado ) ) then
          begin
               if CheckAuto then
                  With cdsKardexAccidente do
                  begin
                       Append;
                       Modificar;
                  end;
          end
          else
              ZInformation('Operaci�n No V�lida', 'Solo Se Pueden Registrar Accidentes a Empleados', 0 );
     end;
end;

procedure TdmMedico.cdsKardexAccidenteAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     FRefrescaEntidades := False;
     ErrorCount := 0;
     with cdsKardexAccidente do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerMedico.GrabaKardexAccidente( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enExpediente ] );
               end;
          end;
    end;
end;

procedure TdmMedico.cdsKardexAccidenteBeforePost(DataSet: TDataSet);
const aCamposAccidente : Array[ 0..3 ] of String =
  ( 'AX_DESCRIP',
    'AX_INF_ACC',
    'AX_INF_TES',
    'AX_INF_SUP');
var
   i: Integer;
begin
    With cdsKardexAccidente do
    begin
         if (Fieldbyname('AX_FECHA').AsDateTime = NULLDATETIME)then
            DB.DatabaseError( 'La Fecha Del Accidente/Enfermedad No Puede Quedar Vac�a');
         if (FieldByName('AX_FEC_SUS').AsDateTime < FieldByName('AX_FECHA').AsDateTime )then
            DB.DatabaseError( 'La Fecha De Suspensi�n No Puede Ser Menor Que La Fecha Del Accidente/Enfermedad');
         if (FieldByName('AX_FEC_COM').AsDateTime < FieldByName('AX_FECHA').AsDateTime )then
            DB.DatabaseError( 'La Fecha Del Comunicado No Puede Ser Menor Que La Fecha Del Accidente/Enfermedad');
         Fieldbyname('US_CODIGO').asInteger := dmCliente.Usuario;
         for i:= Low( aCamposAccidente ) to High( aCamposAccidente ) do
         begin
              if strVacio( FieldByName( aCamposAccidente[ i ] ).AsString ) then
                 FieldByName( aCamposAccidente[ i ] ).AsString := ' ';
         end;
    end;
end;

procedure TdmMedico.cdsKardexAccidenteNewRecord(DataSet: TDataSet);
begin
     With cdsKardexAccidente do
      begin
           FieldByName('EX_CODIGO').AsInteger := dmCliente.Expediente;
           FieldbyName('AX_FECHA').AsDateTime := dmCliente.FechaDefault;
           FieldbyName('AX_FEC_SUS').AsDateTime := dmCliente.FechaDefault;
           FieldByName('AX_HORA').AsString := FormatDateTime( 'hhmm', Now );
           FieldByName('AX_HOR_SUS').AsString := FormatDateTime( 'hhmm', Now );
           FieldByName('AX_HOR_COM').AsString := FormatDateTime( 'hhmm', Now );
           FieldByName('AX_FEC_COM').AsDateTime := dmCliente.FechaDefault;
           FieldByName('AX_RIESGO').AsString := K_GLOBAL_NO;
           FieldByName('AX_INCAPA').AsString := K_GLOBAL_NO;
           FieldByName('AX_DAN_MAT').AsString := K_GLOBAL_NO;
      end;
      FRefrescaEntidades := False;
end;

{********************* cdsKardexEmbarazo **************************}
procedure TdmMedico.cdsKardexEmbarazoAfterDelete(DataSet: TDataSet);
begin
     cdsKardexEmbarazo.Enviar;
end;

procedure TdmMedico.cdsKardexEmbarazoAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerMedico.GetKardexEmbarazo( dmCliente.Empresa,dmCliente.Expediente );
     end;
end;

procedure TdmMedico.cdsKardexEmbarazoAlAgregar(Sender: TObject);
const
     K_SEXO = 'F';
begin
     with dmCliente do
     begin
          if ( cdsExpediente.FieldByName('EX_TIPO').AsInteger = Ord( exEmpleado ) ) then
          begin
               if (cdsExpediente.FieldByName('EX_SEXO').AsString <> K_SEXO ) then
                    ZInformation( 'Operaci�n No V�lida Al Agregar',
                                  'Solo Se Pueden Registrar Embarazos a Empleadas',0 )
               else
               begin
                    if CheckAuto then
                       With cdsKardexEmbarazo do
                       begin
                            Append;
                            Modificar;
                       end;
               end;
          end
          else
              ZInformation('Operaci�n No V�lida', 'Solo Se Pueden Registrar Embarazos a Empleadas', 0 );
     end;
end;

procedure TdmMedico.cdsKardexEmbarazoAlCrearCampos(Sender: TObject);
begin
     With cdsKardexEmbarazo do
      begin
           dmSistema.cdsUsuarios.Conectar;
           MaskFecha( 'EM_FEC_UM' );
           MaskFecha( 'EM_FEC_PP' );
           MaskFecha( 'EM_POSNAT' );
           MaskFecha( 'EM_PRENAT' );
           MaskFecha( 'EM_INC_INI' );
           MaskFecha( 'EM_INC_FIN' );
           CreateCalculated('EM_GESTA', ftInteger, 0);
           CreateSimpleLookup(dmSistema.cdsUsuariosLookup,'US_NOMBRE','US_CODIGO');
      end;
end;

procedure TdmMedico.SincronizaFechas( const iControl: Integer );
const
     K_FUM = 0;
     K_FPP = 1;
     K_EMB_EN_DIAS = 280;
begin
     with cdsKardexEmbarazo do
     begin
          if ( iControl = K_FUM )then
             FieldByName('EM_FEC_PP').AsDateTime := FieldbyName('EM_FEC_UM').asDateTime + K_EMB_EN_DIAS;
          if ( iControl = K_FPP )then
             FieldByName('EM_FEC_FIN').AsDateTime := FieldByName('EM_FEC_PP').AsDateTime;
          FieldbyName('EM_PRENAT').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime - K_DIAS_INCAPACIDAD;
          FieldbyName('EM_POSNAT').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime + K_DIAS_INCAPACIDAD;
          FieldbyName('EM_INC_INI').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime - K_DIAS_INCAPACIDAD;
          FieldbyName('EM_INC_FIN').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime + K_DIAS_INCAPACIDAD;
     end;
end;

procedure TdmMedico.cdsKardexEmbarazoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsKardexEmbarazo do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerMedico.GrabaKardexEmbarazo( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enExpediente ] );
               end;
          end;
     end;
end;

procedure TdmMedico.cdsKardexEmbarazoBeforePost(DataSet: TDataSet);
begin
     With cdsKardexEmbarazo do
     begin
          if ( FieldByName('EM_FEC_PP').AsDateTime < FieldByName('EM_FEC_UM').AsDateTime )then
             DB.DatabaseError( 'La Fecha Probable De Parto No Puede Ser Menor A La Fecha De Ultima Menstruaci�n');
          if ( FieldByName('EM_FEC_FIN').AsDateTime < FieldByName('EM_FEC_UM').AsDateTime )then
             DB.DatabaseError( 'La Fecha Final De Embarazo No Puede Ser Menor A La Fecha De Ultima Menstruaci�n');
          if ( FieldByName('EM_INC_INI').AsDateTime < FieldByName('EM_FEC_UM').AsDateTime )then
             DB.DatabaseError( 'El Inicio De La Incapacidad Del Riesgo No Puede Ser Menor A La Fecha De Ultima Menstruaci�n');
          if ( FieldByName('EM_INC_FIN').AsDateTime < FieldByName('EM_INC_INI').AsDateTime )then
             DB.DatabaseError( 'El Fin De La Incapacidad Del Riesgo No Puede Ser Menor A La Fecha De Inicio De La Incapacidad Del Riesgo');
          if ( FieldByName('EM_PRENAT').AsDateTime < FieldByName('EM_FEC_UM').AsDateTime )then
             DB.DatabaseError( 'El Inicio De La Incapacidad No Puede Ser Menor A La Fecha De Ultima Menstruaci�n');
          if ( FieldByName('EM_POSNAT').AsDateTime < FieldByName('EM_PRENAT').AsDateTime )then
             DB.DatabaseError( 'El Fin De La Incapacidad No Puede Ser Menor A La Fecha De Inicio De La Incapacidad');
          Fieldbyname('US_CODIGO').asInteger := dmCliente.Usuario;
     end;
end;

procedure TdmMedico.cdsKardexEmbarazoCalcFields(DataSet: TDataSet);
const
     K_DIAS_SEMANA = 7;
begin
   With cdsKardexEmbarazo do
   begin
        FieldByName('EM_GESTA').AsInteger := Trunc((dmCliente.FechaDefault - FieldByName('EM_FEC_UM').ASDateTime) / K_DIAS_SEMANA) + 1;
   end;
end;

procedure TdmMedico.cdsKardexEmbarazoNewRecord(DataSet: TDataSet);
begin
     With cdsKardexEmbarazo do
     begin
          FieldByName('EX_CODIGO').AsInteger := dmCliente.Expediente;
          FieldbyName('EM_FEC_UM').asDateTime := dmCliente.FechaDefault;
          FieldByName('EM_FEC_FIN').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime;
          FieldbyName('EM_PRENAT').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime - K_DIAS_INCAPACIDAD;
          FieldbyName('EM_POSNAT').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime + K_DIAS_INCAPACIDAD;
          FieldbyName('EM_INC_INI').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime - K_DIAS_INCAPACIDAD;
          FieldbyName('EM_INC_FIN').AsDateTime := FieldbyName('EM_FEC_PP').AsDateTime + K_DIAS_INCAPACIDAD;
          FieldByName('EM_RIESGO').AsString := K_GLOBAL_NO;
          FieldByName('EM_FINAL').AsString := K_GLOBAL_NO;
          FieldByName('EM_NORMAL').AsString := K_GLOBAL_SI;
          FieldByName('EM_MORTAL').AsString := K_GLOBAL_NO;
     end;//with
end;


{********************** cdsKardexConsultas *********************}
procedure TdmMedico.cdsKardexConsultasAfterDelete(DataSet: TDataSet);
begin
     cdsKardexConsultas.Enviar;
end;

procedure TdmMedico.cdsKardexConsultasAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerMedico.GetKardexConsulta( dmCliente.Empresa,dmCliente.Expediente );
     end;
end;

procedure TdmMedico.cdsKardexConsultasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsKardexConsultas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerMedico.GrabaKardexConsultas( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    //TressShell.SetDataChange( [ enConsulta ] );
               end;
          end;
     end;
end;

procedure TdmMedico.cdsKardexConsultasBeforePost(DataSet: TDataSet);
const aCamposConsulta : Array[ 0..7 ] of String =
  ( 'CN_SINTOMA',
    'CN_OBSERVA',
    'CN_EST_RES',
    'CN_EST_OBS',
    'CN_EXPLORA',
    'CN_DIAGNOS',
    'CN_CUIDADO',
    'CN_RECETA');
var
   i: Integer;
begin
     if ( FValidaGrabado ) then
     begin
          With DataSet do
          begin
               Fieldbyname('US_CODIGO').asInteger := dmCliente.Usuario;
               if (strVacio(Fieldbyname('CN_TIPO').asString))then
                    DB.DatabaseError( 'El Tipo De Consulta No Puede Quedar Vac�o');
               if(strVacio(FieldByName('CN_HOR_FIN').asString))then
                    FieldByName('CN_HOR_FIN').AsString := FormatDateTime( 'hhmm', Now );
               if( FieldByName('CN_FECHA').asDateTime =  NULLDATETIME )then
                    DB.DatabaseError('La Fecha De Consulta No Puede Quedar Vac�a');
               FieldByName('CN_D_OBS').AsString := ExtractFileName(FieldByName('CN_D_OBS').AsString);
               for i:= Low( aCamposConsulta ) to High( aCamposConsulta ) do
               begin
                    if strVacio( FieldByName( aCamposConsulta[ i ] ).AsString ) then
                       FieldByName( aCamposConsulta[ i ] ).AsString := ' ';
               end;
          end;
     end;
end;

procedure TdmMedico.cdsKardexConsultasNewRecord(DataSet: TDataSet);
const
     K_CN_AV_DER = 20;
begin
     With cdsKardexConsultas do
     begin
          FieldbyName('CN_FECHA').AsDateTime := dmCliente.FechaDefault;
          FieldbyName('CN_EST_FEC').AsDateTime := dmCliente.FechaDefault;
          FieldByName('CN_HOR_INI').AsString := FormatDateTime( 'hhmm', Now );
          FieldByName('CN_HOR_FIN').AsString := FormatDateTime( 'hhmm', Now );
          FieldByName('CN_IMSS').AsString := K_GLOBAL_NO;
          FieldByName('CN_SUB_SEC').AsString := K_GLOBAL_NO;
          FieldbyName('CN_EST_HAY').AsString := K_GLOBAL_NO;
          FieldByName('EX_CODIGO').AsInteger := dmCliente.Expediente;
          FieldByName('CN_AV_DER').AsInteger := K_CN_AV_DER;
     end;//with
     FRefrescaEntidades := False;
end;

procedure TdmMedico.cdsKardexConsultasAlCrearCampos(Sender: TObject);
begin
     With cdsKardexConsultas do
     begin
          cdsDiagnostico.Conectar;
          cdsTConsulta.Conectar;
          cdsTEstudio.Conectar;
          dmSistema.cdsUsuarios.Conectar;
          MaskFecha( 'CN_FECHA' );
          MaskFecha( 'CN_FEC_MOD' );
          MaskTime( 'CN_HOR_INI' );
          MaskTime( 'CN_HOR_FIN' );
          MaskPesos( 'CN_PESO' );
          MaskPesos( 'CN_ALTURA' );
          MaskFecha( 'CN_EST_FEC' );
          CreateSimpleLookup(cdsDiagnostico,'DA_DESCRIP','DA_CODIGO');
          CreateSimpleLookup(cdsTEstudio,'TB_ELEMENT','CN_EST_TIP');
          CreateSimpleLookup(cdsTConsulta,'CN_TIPO_CONS','CN_TIPO');
          CreateSimpleLookup(dmSistema.cdsUsuariosLookup,'US_NOMBRE','US_CODIGO');
     end;
end;

procedure TdmMedico.cdsKardexConsultasAfterOpen(DataSet: TDataSet);
begin
     FUpdating := False;
     with TClientDataset( Dataset ) do
     begin
          DisableControls;
          try
             FValidaGrabado:= False; //AP(12/10/2007): Se agreg� para no validar los eventos del beforepost
             while not Eof do
             begin
                  Edit;
                  FieldByName( 'CN_PIES' ).AsInteger := GetPies( FieldByName( 'CN_ALTURA' ).AsFloat );
                  FieldByName( 'CN_PULGADAS' ).AsInteger := GetPulgadas( FieldByName( 'CN_ALTURA' ).AsFloat );
                  Post;
                  Next;
             end;
             MergeChangeLog;
             First;
             FieldByName( 'CN_PESO' ).OnChange := CN_PESOChange;
             FieldByName( 'CN_LIBRAS' ).OnChange := CN_LIBRASChange;
             FieldByName( 'CN_TEMPERA' ).OnChange := CN_TEMPERAChange;
             FieldByName( 'CN_FAHREN' ).OnChange := CN_FAHRENChange;
             FieldByName( 'CN_ALTURA' ).OnChange := CN_ALTURAChange;
             FieldByName( 'CN_PIES' ).OnChange := CN_PIESChange;
             FieldByName( 'CN_PULGADAS' ).OnChange := CN_PIESChange;
          finally
                 FValidaGrabado:= True; 
                 EnableControls;
          end;
     end;
end;

function TdmMedico.ExisteExpediente: Boolean;
begin
     With cdsSoloExpediente do
     begin
          if Active and not IsEmpty and ( FieldByName('EX_CODIGO').AsInteger = dmCliente.Expediente )then
             Result := True
          else
          begin
               //MA: Si se deshabilita el refrescar al trabajar con otros kardex diferentes a expediente
               //    el puntero no se sincroniza y se pierde el empleado activo.
               Refrescar;
               Result := not IsEmpty and ( FieldByName('EX_CODIGO').AsInteger = dmCliente.Expediente );
          end;
     end;
end;

function TdmMedico.ExisteConsulta: Boolean;
begin
     With cdsKardexConsultas do
     begin
          Result := Active and not IsEmpty and ( FieldByName('EX_CODIGO').AsInteger = dmCliente.Expediente );
          if Active and not IsEmpty and ( FieldByName('EX_CODIGO').AsInteger = dmCliente.Expediente ) then
             Result := True
          else
          begin
               //Refrescar;
               Result := ( FieldByName('EX_CODIGO').AsInteger = dmCliente.Expediente )
          end;
     end;
end;


{ ***************** cdsMedicinaEntr **********************}
procedure TdmMedico.cdsMedicinaEntrAfterDelete(DataSet: TDataSet);
begin
     cdsMedicinaEntr.Enviar;
end;

procedure TdmMedico.cdsMedicinaEntrAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerMedico.GetMedicinaEntr( dmCliente.Empresa,dmCliente.Expediente );
     end;
end;

procedure TdmMedico.cdsMedicinaEntrAlCrearCampos(Sender: TObject);
begin
    dmSistema.cdsUsuarios.Conectar;
    with cdsMedicinaEntr do
    begin
         cdsMedicina.Conectar;
         MaskPesos( 'MT_CANTIDA' );
         MaskFecha( 'MT_FECHA' );
         CreateSimpleLookup(cdsMedicina,'ME_NOMBRE','ME_CODIGO');
         CreateSimpleLookup(dmSistema.cdsUsuariosLookup,'US_NOMBRE','US_CODIGO');
    end;
end;

procedure TdmMedico.cdsMedicinaEntrAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsMedicinaEntr do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerMedico.GrabaMedicinaEntr( dmCliente.Empresa, Delta, ErrorCount ) ) then
               begin
                    TressShell.SetDataChange( [ enNinguno ] );
               end;
          end;
     end;
end;

procedure TdmMedico.cdsMedicinaEntrNewRecord(DataSet: TDataSet);
begin
   with cdsMedicinaEntr do
   begin
        FieldbyName('EX_CODIGO').asInteger := dmCliente.Expediente;
        if(dmMedico.ActivaKcons = false) then
        begin
             FieldbyName('MT_FECHA').AsDateTime := dmCliente.FechaDefault;
        end
        else
             FieldbyName('MT_FECHA').AsDateTime := cdsKardexConsultas.FieldByName('CN_FECHA').asDateTime;
   end;
end;

procedure TdmMedico.cdsMedicinaEntrBeforePost(DataSet: TDataSet);
begin
     With cdsMedicinaEntr do
     begin
          Fieldbyname('US_CODIGO').asInteger := dmCliente.Usuario;
          if ( strVacio(Fieldbyname('ME_CODIGO').asString)) then
               DB.DatabaseError( 'El C�digo de la Medicina no Puede Quedar Vac�o');
          if (Fieldbyname('MT_CANTIDA').asfloat = 0 )then
               DB.DatabaseError( 'La Cantidad no Puede Quedar en Ceros');
     end;
end;

{-********************* cdsDatosEmpleado ***************************-}
procedure TdmMedico.cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
begin
     with cdsDatosEmpleado do
     begin
          Data := ServerMedico.GetDatosEmpleado(dmCliente.Empresa, cdsExpediente.FieldByName('CB_CODIGO').AsInteger );          
          if IsEmpty then
          begin
               //ZetaDialogo.ZError('Datos del Empleado', 'No se Encontr� Informaci�n', 0 );
          end;
     end;
end;


procedure TdmMedico.cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
begin
     with cdsDatosEmpleado do
     begin
          with dmCatalogos do
          begin
               cdsTurnos.Conectar;
               cdsPuestos.Conectar;
               cdsClasifi.Conectar;
               cdsContratos.Conectar;
               CreateSimpleLookup(cdsTurnos,'TU_DESCRIP','CB_TURNO');
               CreateSimpleLookup(cdsPuestos,'PU_DESCRIP','CB_PUESTO');
               CreateSimpleLookup(cdsContratos,'CONTRATO','CB_CONTRAT');
               CreateSimpleLookup(cdsClasifi,'CLASIFICACION','CB_CLASIFI');
          end;
          with dmTablas do
          begin
               cdsEstadoCivil.Conectar;
               CreateSimpleLookup(cdsEstadoCivil,'ESTADOCIVIL','CB_EDO_CIV');
          end;
     end;

end;

//****************** cdsBusquedas **********************//
procedure TdmMedico.HacerBusqueda( oParamList: TZetaParams );
begin
     cdsBusquedas.Data := ServerMedico.BuscarEnTablas( dmCliente.Empresa, oParamList.VarValues );
end;

procedure TdmMedico.cdsBusquedasLookupSearch(Sender: TZetaLookupDataSet;
  var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
 var iEmpleado, iExpediente : integer;
begin
     iEmpleado := StrToIntDef( sKey, 0 );
     lOk := True;//TODO :FBuscaExpedientes.BuscaExpedienteDialogo( sFilter, iExpediente, iEmpleado, sDescription, FALSE );

     if lOk then
        sKey := IntToStr( iEmpleado );
end;

procedure TdmMedico.cdsBusquedasLookupKey(Sender: TZetaLookupDataSet;
  var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
  var Datos: OleVariant;
begin
     with DCliente.dmCliente do
          lOk := ServerMedico.ExpedienteActivo( Empresa, StrToIntDef( sKey, 0 ), Datos );
            {if EditConsultaGlobal.OldCreateOrder then
                lOk := ServerMedico.ExpedienteActivo( Empresa, StrToIntDef( sKey, 0 ), Datos )
            else
                lOk := Servidor.GetLookupEmpleado( Empresa, StrToIntDef( sKey, 0 ), Datos, Ord( TipoLookup ) );}
     if lOk then
     begin
          with cdsBusquedas do
          begin
               Data := Datos;
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmMedico.cdsBusquedasAlCrearCampos(Sender: TObject);
begin
     cdsBusquedas.ListaFija( 'EX_TIPO', lfExTipo );
end;

//****************** cdsCodigos **********************//
procedure TdmMedico.cdsCodigosAlAdquirirDatos(Sender: TObject);
begin
     cdsCodigos.Data := ServerMedico.GetCodigos( dmCliente.Empresa );
end;

//****************** cdsConsultaGlobal **********************//
procedure TdmMedico.RegistraConsultaGlobal;
begin
     ZBaseGridEdicion_DevEx.ShowGridEdicion( EditConsultaGlobal_DevEx, TEditConsultaGlobal_DevEx, TRUE );
end;

procedure TdmMedico.cdsConsultaGlobalAlCrearCampos(Sender: TObject);
begin
     With cdsConsultaGlobal do
     begin
          with FieldByName( 'EX_CODIGO' ) do
          begin
               OnValidate := EX_CODIGOConsultaGlobalValidate;
               OnChange := EX_CODIGOConsultaGlobalChange;
          end;
          with FieldByName( 'CB_CODIGO' ) do
          begin
               OnValidate := CB_CODIGOConsultaGlobalValidate;
               OnChange := CB_CODIGOConsultaGlobalChange;
          end;
          cdsTConsulta.Conectar;
          dmSistema.cdsUsuarios.Conectar;
          MaskFecha( 'CN_FECHA' );
          MaskTime( 'CN_HOR_INI' );
          MaskTime( 'CN_HOR_FIN' );
          MaskNumerico('CB_CODIGO', '#0;-#0;#' );
          ListaFija( 'EX_TIPO', lfExTipo );
          CreateSimpleLookup(cdsTConsulta,'CN_TIPO_CONS','CN_TIPO');
          CreateSimpleLookup(dmSistema.cdsUsuariosLookup,'US_NOMBRE','US_CODIGO');
     end;
end;

procedure TdmMedico.EX_CODIGOConsultaGlobalValidate( Sender: TField );
var
   iExpediente: Integer;
   sNombre, sExpediente: String;
begin
     if not lCambiandoCB_CODIGO then
     begin
          iExpediente := Sender.AsInteger;
          sExpediente := IntToStr( iExpediente );
          if ( iExpediente <= 0 ) then
             DataBaseError( 'N�mero de Expediente Debe Ser Mayor a Cero' )
          else
          begin
               sNombre := cdsBusquedas.GetDescripcion( sExpediente );
               if StrVacio( sNombre ) then    // Posiciona el empleado en cdsBusquedas
                  DataBaseError( 'No Existe el Expediente #' + sExpediente );
          end;
     end;
end;

procedure TdmMedico.EX_CODIGOConsultaGlobalChange( Sender: TField );
begin
     lCambiandoEX_CODIGO := True;
     try
        try
           with Sender do
           begin
                if ( AsInteger = dmCliente.Expediente ) then
                begin
                     DataSet.FieldByName( 'PRETTYNAME' ).AsString := dmCliente.GetDatosExpedienteActivo.Nombre;
                     DataSet.FieldByName( 'CB_CODIGO' ).AsInteger := dmCliente.Empleado;
                     DataSet.FieldByName( 'EX_TIPO' ).AsInteger := Ord( dmCliente.GetDatosExpedienteActivo.Tipo );
                end
                else
                begin
                     cdsBusquedas.GetDescripcion( Sender.AsString );
                     if ( AsInteger = cdsBusquedas.FieldByName( 'EX_CODIGO' ).AsInteger ) then
                     begin
                          DataSet.FieldByName( 'PRETTYNAME' ).AsString := cdsBusquedas.GetDescription;
                          DataSet.FieldByName( 'CB_CODIGO' ).AsInteger := cdsBusquedas.FieldByName('CB_CODIGO').AsInteger;
                          DataSet.FieldByName( 'EX_TIPO' ).AsInteger := cdsBusquedas.FieldByName('EX_TIPO').AsInteger;
                     end;
                end;
           end;
        except
              DatabaseError(' Error Al Asignar Datos Del Paciente ');
        end;
    finally
           lCambiandoEX_CODIGO := False;
    end;
end;

procedure TdmMedico.CB_CODIGOConsultaGlobalValidate( Sender: TField );
var
   iEmpleado: Integer;
begin
     if not lCambiandoEX_CODIGO then
     begin
          iEmpleado := Sender.AsInteger;
          if cdsCodigos.Locate( 'CB_CODIGOC', iEmpleado, [ ] ) then
          begin
               if not cdsCodigos.Locate( 'CB_CODIGO', iEmpleado, [ ] ) then
               begin
                    DataBaseError( 'El Empleado ' + IntToStr( iEmpleado ) + ' No Tiene Expediente' );
               end;
          end
          else
              if ( iEmpleado = 0 ) then
                  DataBaseError( 'El Numero de Empleado Debe Ser Mayor a Cero' )
              else
                  DataBaseError( 'No Existe el Empleado ' + IntToStr( iEmpleado ) );
     end;
end;

procedure TdmMedico.CB_CODIGOConsultaGlobalChange( Sender: TField );
begin
     lCambiandoCB_CODIGO := True;
     try
        if not lCambiandoEX_CODIGO then
           cdsConsultaGlobal.FieldByName('EX_CODIGO').AsInteger := cdsCodigos.FieldByName('EX_CODIGO').AsInteger;
     finally
            lCambiandoCB_CODIGO := False;
     end;
end;

procedure TdmMedico.cdsConsultaGlobalAlAdquirirDatos(Sender: TObject);
begin
     cdsConsultaGlobal.Data :=  ServerMedico.GetConsultaGlobal( dmCliente.Empresa );
end;

procedure TdmMedico.EscribeConsultaGlobal( const oParams: OleVariant );
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsConsultaGlobal do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerMedico.GrabaConsultaGlobal( dmCliente.Empresa, Delta, oParams, ErrorCount ) );

               //CV: Necesitamos que siempre se refresque el Expediente y la Consulta por que no sabemos
               //cuantos registros si se pudieron agregar.
               TressShell.SetDataChange( [ enExpediente, enConsulta ] );

          end;
     end;
end;

{$ifdef VER130}
procedure TdmMedico.cdsConsultaGlobalReconcileError( DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     if PK_Violation( E ) then
        with DataSet do
             sError := Format( 'El Paciente Con Expediente: %d' + CR_LF + 'Ya Cuenta Con Un Registro Para Esa Fecha',
                    [ FieldByName( 'EX_CODIGO' ).AsInteger] )
     else
         sError := GetErrorDescription( E );
     Action := cdsConsultaGlobal.ReconciliaError(DataSet,UpdateKind,E, 'EX_CODIGO', sError );
end;
{$else}
procedure TdmMedico.cdsConsultaGlobalReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     if PK_Violation( E ) then
        with DataSet do
             sError := Format( 'El Paciente Con Expediente: %d' + CR_LF + 'Ya Cuenta Con Un Registro Para Esa Fecha',
                    [ FieldByName( 'EX_CODIGO' ).AsInteger] )
     else
         sError := GetErrorDescription( E );
     Action := cdsConsultaGlobal.ReconciliaError(DataSet,UpdateKind,E, 'EX_CODIGO', sError );
end;
{$endif}

procedure TdmMedico.AbreDocumento;
begin
     ZetaFilesTools.AbreDocumento( cdsKardexConsultas, 'CN_D_BLOB', 'CN_D_EXT' );
end;


procedure TdmMedico.BorraDocumento;
begin
     with cdsKardexConsultas do
     begin
          if State = dsBrowse then
             Edit;

          FieldByName('CN_D_BLOB').AsString := VACIO;
          FieldByName('CN_D_EXT').AsString := VACIO;
          FieldByName('CN_D_OBS').AsString := VACIO;
     end;
end;

function TdmMedico.CargaDocumento( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean;
begin
     Result := StrLleno( sObservaciones ) ;
     if Result then
        with cdsKardexConsultas do
        begin
             if State = dsBrowse then
                Edit;

             if lEncimarArchivo then
                Result := ZetaFilesTools.CargaDocumento( cdsKardexConsultas,
                                                         sPath,
                                                         'CN_D_BLOB',
                                                         'CN_D_EXT' );
             if Result then
                FieldByName('CN_D_OBS').AsString := sObservaciones;
        end
     else
         ZetaDialogo.ZError( 'Error en el Documento...', 'El nombre no puede quedar vac�o', 0 );

end;

procedure TdmMedico.cdsExpedienteAlAgregar(Sender: TObject);
begin
     if CheckAuto then
     begin
          with cdsExpediente do
          begin
               Append;
               Modificar;
          end;
     end;
end;

procedure TdmMedico.cdsHisIncapaciAfterDelete(DataSet: TDataSet);
begin
     cdsHisIncapaci.Enviar; 
end;

procedure TdmMedico.cdsHisIncapaciAlAdquirirDatos(Sender: TObject);
begin
     if EsEmpleado then
     begin
          with cdsHisIncapaci do
          begin
               with dmCliente do
                    Data := ServerRecursos.GetHisIncapaci( Empresa, Empleado );
               Locate( 'IN_FEC_INI', cdsHistorial.FieldByName( 'IN_FEC_INI' ).AsDateTime, [] )
          end;
     end
     else
         cdsHisIncapaci.Close;
end;

procedure TdmMedico.cdsHisIncapaciAlCrearCampos(Sender: TObject);
begin
     dmTablas.cdsIncidencias.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisIncapaci do    //cdsHisIncapaci o cdsGridIncapaci
     begin
          MaskFecha( 'IN_FEC_INI' );
          MaskFecha( 'IN_FEC_FIN' );
          MaskFecha( 'IN_SUA_INI' );
          MaskFecha( 'IN_SUA_FIN' );
          MaskFecha( 'IN_FEC_RH' );

          FieldByName('IN_FEC_INI').OnChange := OnIN_FEC_INIChange;
          FieldByName('IN_DIAS').OnChange := OnIN_DIASChange;

          FieldByName('IN_SUA_INI').OnChange := OnIN_SUA_INIChange;
          
          CreateSimpleLookup( dmTablas.cdsIncidencias, 'TB_ELEMENT', 'IN_TIPO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          FieldByName( 'IN_NUMERO' ).EditMask := 'll999999;1;';
     end;
end;

procedure TdmMedico.cdsHisIncapaciAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;

   procedure CrearDatosConflictos;
   begin
        with cdsListaConflicto do
        begin
             if Fields.Count = 0 then
             begin
                  Init;
                  AddIntegerField( 'CB_CODIGO');
                  AddDateTimeField('IN_FEC_INI' );
                  AddIntegerField( 'IN_DIAS');
                  AddDateTimeField('IN_FEC_FIN');
                  AddDateTimeField('IN_SUA_INI' );
                  AddDateTimeField('IN_SUA_FIN' );
                  AddDateTimeField('IN_FEC_INI' + '_BUS' );
                  CreateTempDataset;
             end
             else
                 EmptyDataSet;
        end;
   end;
begin
     ErrorCount := 0;
     with Sender as TZetaClientDataSet do              // cdsHisIncapaci o cdsGridIncapaci
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ChangeCount > 0 then
          begin
               FCancelaAjuste := FALSE;
               CrearDatosConflictos;
               Repeat
               Until ( FCancelaAjuste or Reconciliar(ServerRecursos.GrabaIncapacidad( dmCliente.Empresa, Delta, ErrorCount, cdsListaConflicto.Data  ) ) );
               if ErrorCount = 0 then
                  TressShell.SetDataChange( [ enPermiso ] );
                  if cdsListaConflicto.RecordCount > 0 then
                  begin
                       cdsHisIncapaci.Refrescar;
                       cdsListaConflicto.EmptyDataSet;
                  end;
          end;
     end;

end;

procedure TdmMedico.cdsHisIncapaciBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if FieldByName( 'IN_FEC_INI').AsDateTime = 0 then
             DatabaseError( 'Fecha de Inicio de Incapacidad no puede quedar Vac�a');

          if ( FieldByName( 'IN_DIAS').AsInteger <= 0 ) then
	     DataBaseError( 'Dias de Incapacidad debe ser mayor a 0');

          if NOT StrLleno(FieldByName( 'IN_TIPO').AsString ) then
             DataBaseError( 'Tipo de Incapacidad no puede quedar vac�o') ;

          if eFinIncapacidad( FieldByName( 'IN_FIN').AsInteger ) <>  fiPermanente then
             FieldByName( 'IN_TASA_IP').AsFloat := 0;
          with dmCliente do
          begin
               FieldByName( 'US_CODIGO').AsInteger   := Usuario;
               FieldByName( 'IN_CAPTURA').AsDateTime := FechaDefault;
          end;
     end;
end;

procedure TdmMedico.cdsHisIncapaciNewRecord(DataSet: TDataSet);
begin
     with cdsHisIncapaci do
     begin
          FieldByName('CB_CODIGO').AsInteger  := dmCliente.Empleado;
          FieldByName('IN_FEC_INI').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('IN_DIAS').AsInteger    := 1;
          FieldByName('IN_MOTIVO').AsInteger  := Ord( miInicial );
          FieldByName('IN_FIN').AsInteger     := Ord( fiNoTermina );
     end;
end;
{$ifdef VER130}
procedure TdmMedico.cdsHisIncapaciReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoAccion( DataSet, FCancelaAjuste, K_SUB_INCAPACIDAD, 'Error al Registrar Incapacidad', E.Message );
end;
{$else}
procedure TdmMedico.cdsHisIncapaciReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ConflictoIncapacidadAccion( DataSet, FCancelaAjuste, K_SUB_INCAPACIDAD, 'Error al Registrar Incapacidad', E.Message, cdsListaConflicto );
end;
{$endif}


function TdmMedico.GetPermisoDiasHabiles( const iEmpleado: Integer; const dInicio, dFinal: TDate ): TPesos;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result := ServerAsistencia.GetPermisoDiasHabiles( dmCliente.Empresa,iEmpleado,dInicio,dFinal);
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TdmMedico.GetPermisoFechaRegreso( const iEmpleado: Integer; const dInicio: TDate; const Dias: integer; var rDiasRango: Double ): TDate;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Result :=   ServerAsistencia.GetPermisoFechaRegreso( dmCliente.Empresa, iEmpleado, dInicio, Dias, rDiasRango );
          finally
             Cursor := oCursor;
          end;
     end;
end;

end.
