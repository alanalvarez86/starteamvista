unit FEditMedicinaEntr_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaEdit, Mask,
  ZetaNumero, ZetaKeyLookup, ZetaDBTextBox, ZetaFecha, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
    TEditMedEntr_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    ME_CODIGO: TZetaDBKeyLookup_DevEx;
    MT_CANTIDA: TZetaDBNumero;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    Label5: TLabel;
    MT_FECHA: TZetaDBFecha;
    MT_COMENTA: TDBEdit;
    ME_MEDIDAlb: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ME_CODIGOExit(Sender: TObject);
  Protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditMedEntr_DevEx: TEditMedEntr_DevEx;

implementation

uses
     //ZetaBuscador
     DMedico, ZAccesosTress, ZetaCommonLists, DSistema, FAyudaContexto,
     ZetaCommonTools;

{$R *.DFM}

procedure TEditMedEntr_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_MEDICINA_ENTR;
     FirstControl := ME_CODIGO;
     TipoValorActivo1 := stExpediente;
     HelpContext:= H00004_Medicina_entregada;
     ME_CODIGO.LookupDataset := dmMedico.cdsMedicina;
end;

procedure TEditMedEntr_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsMedicina.Conectar;
          DataSource.DataSet:= cdsMedicinaEntr;
     end;
end;

procedure TEditMedEntr_DevEx.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', 'Medicina Entregada', 'ME_CODIGO', dmMedico.cdsMedicinaEntr );
end;

procedure TEditMedEntr_DevEx.ME_CODIGOExit(Sender: TObject);
begin
  inherited;
     with ME_MEDIDAlb do
     begin
          Caption := dmMedico.cdsMedicina.FieldByName('ME_MEDIDA').AsString;
          Visible := strLleno(ME_CODIGO.Llave);
     end;     
end;

end.


