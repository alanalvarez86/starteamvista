unit FKardexAccidente_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid;

type
  TKardexAccidente_DevEx = class(TBaseGridLectura_DevEx)
    AX_FECHA: TcxGridDBColumn;
    AX_HORA: TcxGridDBColumn;
    AX_TIP_LES: TcxGridDBColumn;
    AX_NUM_INC: TcxGridDBColumn;
    TB_DESCRIP: TcxGridDBColumn;
    TB_DESCRIP2: TcxGridDBColumn;
    AX_RIESGO: TcxGridDBColumn;
    AX_INCAPA: TcxGridDBColumn;
    AX_DAN_MAT: TcxGridDBColumn;
    US_NOMBRE: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function  PuedeAgregar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  KardexAccidente_DevEx: TKardexAccidente_DevEx;

implementation

uses
//ZetaBuscador
DMedico,ZetaCommonLists, FAyudaContexto;

{$R *.DFM}

procedure TKardexAccidente_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  //CanLookup := True;
  HelpContext := H00104_Accidentes;
  TipoValorActivo1 := stExpediente;
end;

procedure TKardexAccidente_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;


end;

procedure TKardexAccidente_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsExpediente.Conectar;
          cdsKardexAccidente.Conectar;
          DataSource.DataSet:= cdsKardexAccidente;
     end;
end;

procedure TKardexAccidente_DevEx.Refresh;
begin
     dmMedico.cdsKardexAccidente.Refrescar;
end;

procedure TKardexAccidente_DevEx.Agregar;
begin
     dmMedico.cdsKardexAccidente.Agregar;
end;

procedure TKardexAccidente_DevEx.Borrar;
begin
     dmMedico.cdsKardexAccidente.Borrar;
end;

procedure TKardexAccidente_DevEx.Modificar;
begin
     dmMedico.cdsKardexAccidente.Modificar;
end;


{procedure TKardexAccidente.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Accidentes/Enfermedades', 'EX_CODIGO', dmMedico.cdsKardexAccidente );
end;}

function TKardexAccidente_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := FALSE;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsKardexAccidente.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end; 

end.
