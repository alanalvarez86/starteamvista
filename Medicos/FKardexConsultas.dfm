inherited Consultas: TConsultas
  Left = 319
  Top = 168
  Caption = 'Consultas M'#233'dicas'
  ClientWidth = 619
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 619
    ExplicitWidth = 619
    inherited ValorActivo2: TPanel
      Width = 360
      ExplicitWidth = 360
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 619
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CN_FECHA'
        Title.Caption = 'Fecha'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CN_HOR_INI'
        Title.Caption = 'Inicio'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CN_HOR_FIN'
        Title.Caption = 'Fin'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CN_TIPO_CONS'
        Title.Caption = 'Tipo'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CN_MOTIVO'
        Title.Caption = 'Motivo'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DA_DESCRIP'
        Title.Caption = 'Diagn'#243'stico'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CN_IMSS'
        Title.Caption = #191'Envi'#243' IMSS?'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CN_SUB_SEC'
        Title.Caption = #191'Subsecuente?'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Modific'#243
        Width = 120
        Visible = True
      end>
  end
end
