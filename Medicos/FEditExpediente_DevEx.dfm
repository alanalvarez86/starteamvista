inherited EditExpediente_DevEx: TEditExpediente_DevEx
  Left = 248
  Top = 135
  BorderStyle = bsSingle
  Caption = 'Expediente'
  ClientHeight = 587
  ClientWidth = 879
  Color = clWindow
  OnDestroy = FormDestroy
  ExplicitWidth = 885
  ExplicitHeight = 615
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 551
    Width = 879
    ParentFont = False
    TabOrder = 3
    ExplicitTop = 551
    ExplicitWidth = 879
    inherited OK_DevEx: TcxButton
      Left = 709
      Top = 6
      ExplicitLeft = 709
      ExplicitTop = 6
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 789
      Top = 6
      ExplicitLeft = 789
      ExplicitTop = 6
    end
  end
  inherited PanelIdentifica: TPanel
    Top = 0
    Width = 879
    TabOrder = 0
    ExplicitTop = 0
    ExplicitWidth = 879
    inherited ValorActivo2: TPanel
      Width = 553
      ExplicitWidth = 553
      inherited textoValorActivo2: TLabel
        Width = 547
        ExplicitLeft = 467
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 272
    Top = 98
    ExplicitLeft = 272
    ExplicitTop = 98
  end
  object PageControl: TPageControl [3]
    Left = 305
    Top = 17
    Width = 576
    Height = 536
    Margins.Left = 1
    Margins.Top = 1
    Margins.Right = 1
    Margins.Bottom = 1
    ActivePage = tsGenerales
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object tsGenerales: TTabSheet
      Caption = 'Generales'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabVisible = False
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 568
        Height = 234
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          568
          234)
        object GroupBox1: TGroupBox
          Left = 8
          Top = -1
          Width = 550
          Height = 233
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = ' Datos Personales '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object Label4: TLabel
            Left = 86
            Top = 59
            Width = 86
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Apellido Paterno:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label5: TLabel
            Left = 86
            Top = 80
            Width = 86
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Apellido Materno:'
          end
          object Label6: TLabel
            Left = 86
            Top = 100
            Width = 86
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Nombres:'
          end
          object Label13: TLabel
            Left = 143
            Top = 124
            Width = 27
            Height = 13
            Caption = 'Sexo:'
          end
          object LCB_FEC_NAC: TLabel
            Left = 83
            Top = 144
            Width = 89
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fecha Nacimiento:'
          end
          object ZEdad: TZetaTextBox
            Left = 299
            Top = 139
            Width = 121
            Height = 22
            AutoSize = False
            Caption = '99 a'#241'os 99 meses'
            ShowAccelChar = False
            Layout = tlCenter
            Brush.Color = clBtnFace
            Border = True
          end
          object Label7: TLabel
            Left = 85
            Top = 17
            Width = 86
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Tipo de Paciente:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object TipoPacientelbl: TLabel
            Left = 125
            Top = 42
            Width = 50
            Height = 13
            Alignment = taRightJustify
            Caption = 'Empleado:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object btnSincroniza: TcxButton
            Left = 452
            Top = 34
            Width = 21
            Height = 21
            Hint = 'Sincronizar Datos del Paciente con el Empleado'
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A40500000000000000000000000000000000000050D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF51D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF51D293FF50D293FF50D293FF50D2
              93FF50D293FF8FE2BAFFB2EBD0FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FFB8EDD3FFFFFFFFFFFFFFFFFFE4F8EEFF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FFCEF2E1FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFE4F8EEFF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF52D394FFDEF7
              EBFFFFFFFFFFFFFFFFFFF4FCF8FFDBF6E9FFFFFFFFFFFFFFFFFFE4F8EEFF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF91E3BBFFFCFEFDFFFFFFFFFFE9F9F1FF50D293FF4FD091FCD6F4
              E6FFFFFFFFFFFFFFFFFFE4F8EEFF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF6DDAA5FFE1F7ECFFCEF2E1FF4ECE
              90FA50D293FF50D293FF50D293FFD6F4E6FFFFFFFFFFFFFFFFFFE4F8EEFF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF51D2
              94FF50D293FF50D293FF50D192FE51D293FF52D395FF50D293FF4FD092FDD9F5
              E7FFFFFFFFFFFFFFFFFFE4F8EEFF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF58D498FF50D293FF4FD092FDE4F8EEFFFFFFFFFFFFFFFFFFE4F8EEFF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF57D498FF50D192FE50D192FEE4F8
              EEFFFFFFFFFFFFFFFFFFD0F3E2FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF52D294FF4FD091FC50D192FEE4F8EEFFFFFFFFFFF1FBF7FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF5AD499FF4FD092FD50D192FE92D8
              B6EF74CFA3EF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF51D293FF4FD091FC4FD091FC4FD091FC50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
              93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 9
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = btnSincronizaClick
          end
          object EX_APE_PAT: TDBEdit
            Left = 176
            Top = 55
            Width = 249
            Height = 21
            AutoSize = False
            DataField = 'EX_APE_PAT'
            DataSource = DataSource
            TabOrder = 2
            OnChange = CambioEmpleadoChange
          end
          object EX_APE_MAT: TDBEdit
            Left = 176
            Top = 76
            Width = 249
            Height = 21
            AutoSize = False
            DataField = 'EX_APE_MAT'
            DataSource = DataSource
            TabOrder = 3
            OnChange = CambioEmpleadoChange
          end
          object EX_NOMBRES: TDBEdit
            Left = 176
            Top = 97
            Width = 249
            Height = 21
            AutoSize = False
            DataField = 'EX_NOMBRES'
            DataSource = DataSource
            TabOrder = 4
            OnChange = CambioEmpleadoChange
          end
          object EX_FEC_NAC: TZetaDBFecha
            Left = 176
            Top = 139
            Width = 121
            Height = 22
            Cursor = crArrow
            Opcional = True
            TabOrder = 6
            Text = '18-Dec-97'
            Valor = 35782.000000000000000000
            DataField = 'EX_FEC_NAC'
            DataSource = DataSource
          end
          object EX_TIPO: TZetaDBKeyCombo
            Left = 176
            Top = 13
            Width = 121
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            OnChange = EX_TIPOChange
            ListaFija = lfExTipo
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
            DataField = 'EX_TIPO'
            DataSource = DataSource
            LlaveNumerica = True
          end
          object CB_CODIGO: TZetaDBKeyLookup_DevEx
            Left = 176
            Top = 34
            Width = 274
            Height = 21
            LookupDataset = dmCliente.cdsEmpleadoLookUp
            Opcional = False
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 1
            TabStop = True
            WidthLlave = 60
            OnValidKey = CB_CODIGOValidKey
            DataField = 'CB_CODIGO'
            DataSource = DataSource
          end
          object EX_SEXO: TZetaKeyCombo
            Left = 176
            Top = 118
            Width = 121
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 5
            OnChange = EX_SEXOChange
            ListaFija = lfSexoDesc
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
          object btnDatosContrata: TcxButton
            Left = 477
            Top = 34
            Width = 21
            Height = 21
            Hint = 'Ver Datos de Contrataci'#243'n'
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFF5E8DDFFFFFFFFFFFFFFFFFFFFFFFFFFFBF7F3FFF7EEE5FFFCF8
              F5FFF8F0E9FFFFFFFFFFFFFFFFFFFFFFFFFFFCF8F5FFD9AB81FFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE2BF9FFFFFFFFFFFFFFFFFFFFFFF
              FFFFEFDCCBFFE0BB99FFEAD1B9FFE7CAAFFFFFFFFFFFFFFFFFFFFFFFFFFFEEDB
              C9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFECD5BFFFFFFFFFFFFFFFFFFFE5C7ABFFDAAF87FFE2BF9FFFDDB58FFFFFFF
              FFFFFFFFFFFFF9F2EBFFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE5C7ABFFF6ECE3FFDDB58FFFDEB7
              93FFE2BF9FFFD8AA7FFFF8F0E9FFF0DFCFFFDCB28BFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFDCB38DFFE5C6A9FFE5C6A9FFDCB38DFFD9AD83FFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFE2BF9FFFFDFAF7FFFFFFFFFFFFFFFFFFFDFB
              F9FFE2C1A1FFDAAE85FFE4C5A7FFE9CFB7FFE2BF9FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAF87FFFCF8F5FFFFFF
              FFFFFFFFFFFFFFFFFFFFFDFAF7FFE4C3A5FFFAF4EFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF5EADFFFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFE1BD9BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8CDB3FFF6ECE3FFFFFF
              FFFFFDFBF9FFE2C1A1FFFEFCFBFFFFFFFFFFEEDBC9FFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFBA97FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFE2BF9FFFFFFFFFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFFFFFFFFFFCF8
              F5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF8EF
              E7FFFFFFFFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFFFFFFFFFFCF8F5FFE2BF
              9FFFFFFFFFFFFFFFFFFFFEFCFBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFDCB38DFFF7EEE5FFFFFFFFFFFFFFFFFFE3C2A3FFFDFA
              F7FFFFFFFFFFFFFFFFFFF6ECE3FFFFFFFFFFFFFFFFFFF5EADFFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDBB1
              89FFDBB189FFD8AA7FFFE6C9ADFFFEFEFDFFFEFCFBFFF0DFCFFFFFFFFFFFFDFB
              F9FFDFBA97FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE2C1A1FFF3E4
              D7FFF8EFE7FFF0DFCFFFDDB58FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
              7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            OnClick = btnDatosContrataClick
          end
          object gbProteccionCivil: TGroupBox
            Left = 84
            Top = 166
            Width = 343
            Height = 60
            Caption = 'Protecci'#243'n Civil :'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            object lblTSangre: TLabel
              Left = 104
              Top = 16
              Width = 76
              Height = 13
              Alignment = taRightJustify
              Caption = 'Tipo de Sangre:'
            end
            object lblAlergia: TLabel
              Left = 64
              Top = 37
              Width = 116
              Height = 13
              Alignment = taRightJustify
              Caption = 'Al'#233'rgico / Padecimiento:'
            end
            object EX_ALERGIA: TDBEdit
              Left = 186
              Top = 35
              Width = 150
              Height = 21
              DataField = 'EX_ALERGIA'
              DataSource = DataSource
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object EX_TSANGRE: TDBComboBox
              Left = 186
              Top = 11
              Width = 150
              Height = 21
              DataField = 'EX_TSANGRE'
              DataSource = DataSource
              Items.Strings = (
                ''
                'O +'
                'O -'
                'A +'
                'A -'
                'B +'
                'B -'
                'AB +'
                'AB -')
              TabOrder = 0
            end
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 234
        Width = 568
        Height = 173
        Align = alTop
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = cl3DLight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        DesignSize = (
          568
          173)
        object GroupBox4: TGroupBox
          Left = 8
          Top = 0
          Width = 550
          Height = 174
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = ' Domicilio '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object Label19: TLabel
            Left = 124
            Top = 144
            Width = 45
            Height = 13
            Alignment = taRightJustify
            Caption = 'Tel'#233'fono:'
          end
          object LEstado: TLabel
            Left = 133
            Top = 123
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Estado:'
          end
          object LCP: TLabel
            Left = 101
            Top = 102
            Width = 68
            Height = 13
            Alignment = taRightJustify
            Caption = 'C'#243'digo Postal:'
          end
          object LCiudad: TLabel
            Left = 133
            Top = 81
            Width = 36
            Height = 13
            Alignment = taRightJustify
            Caption = 'Ciudad:'
          end
          object LColonia: TLabel
            Left = 131
            Top = 60
            Width = 38
            Height = 13
            Alignment = taRightJustify
            Caption = 'Colonia:'
          end
          object LDireccion: TLabel
            Left = 143
            Top = 18
            Width = 26
            Height = 13
            Alignment = taRightJustify
            Caption = 'Calle:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 122
            Top = 39
            Width = 48
            Height = 13
            Caption = '# Exterior:'
          end
          object Label9: TLabel
            Left = 259
            Top = 39
            Width = 45
            Height = 13
            Caption = '# Interior:'
          end
          object EX_CALLE: TDBEdit
            Left = 175
            Top = 14
            Width = 310
            Height = 21
            DataField = 'EX_CALLE'
            DataSource = DataSource
            TabOrder = 0
          end
          object EX_COLONIA: TDBEdit
            Left = 175
            Top = 56
            Width = 310
            Height = 21
            DataField = 'EX_COLONIA'
            DataSource = DataSource
            TabOrder = 3
          end
          object EX_CIUDAD: TDBEdit
            Left = 175
            Top = 77
            Width = 150
            Height = 21
            DataField = 'EX_CIUDAD'
            DataSource = DataSource
            TabOrder = 4
          end
          object EX_CODPOST: TDBEdit
            Left = 175
            Top = 98
            Width = 57
            Height = 21
            DataField = 'EX_CODPOST'
            DataSource = DataSource
            TabOrder = 5
          end
          object EX_ESTADO: TZetaDBKeyLookup_DevEx
            Left = 175
            Top = 119
            Width = 310
            Height = 21
            LookupDataset = dmCliente.cdsEmpleadoLookUp
            Opcional = False
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 6
            TabStop = True
            WidthLlave = 60
            DataField = 'EX_ESTADO'
            DataSource = DataSource
          end
          object EX_TEL: TDBEdit
            Left = 175
            Top = 140
            Width = 150
            Height = 21
            DataField = 'EX_TEL'
            DataSource = DataSource
            TabOrder = 7
          end
          object EX_NUM_EXT: TDBEdit
            Left = 175
            Top = 35
            Width = 80
            Height = 21
            DataField = 'EX_NUM_EXT'
            DataSource = DataSource
            TabOrder = 1
          end
          object EX_NUM_INT: TDBEdit
            Left = 305
            Top = 35
            Width = 80
            Height = 21
            DataField = 'EX_NUM_INT'
            DataSource = DataSource
            TabOrder = 2
          end
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 407
        Width = 568
        Height = 119
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        DesignSize = (
          568
          119)
        object GroupBox3: TGroupBox
          Left = 8
          Top = 8
          Width = 550
          Height = 110
          Margins.Left = 1
          Margins.Top = 1
          Margins.Right = 1
          Margins.Bottom = 1
          Anchors = [akLeft, akTop, akRight, akBottom]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          DesignSize = (
            550
            110)
          object Label1: TLabel
            Left = 55
            Top = 18
            Width = 52
            Height = 13
            Alignment = taRightJustify
            Caption = 'Capturado:'
          end
          object Label2: TLabel
            Left = 33
            Top = 38
            Width = 74
            Height = 13
            Alignment = taRightJustify
            Caption = 'Observaciones:'
          end
          object Label3: TLabel
            Left = 283
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Modific'#243':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object US_NOMBRE: TZetaDBTextBox
            Left = 329
            Top = 10
            Width = 153
            Height = 22
            AutoSize = False
            Caption = 'US_NOMBRE'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ShowAccelChar = False
            Layout = tlCenter
            Brush.Color = clBtnFace
            Border = True
            DataField = 'US_NOMBRE'
            DataSource = DataSource
            FormatFloat = '%14.2n'
            FormatCurrency = '%m'
          end
          object EX_FEC_INI: TZetaDBFecha
            Left = 111
            Top = 13
            Width = 115
            Height = 22
            Cursor = crArrow
            TabOrder = 0
            Text = '28-Feb-02'
            Valor = 37315.000000000000000000
            DataField = 'EX_FEC_INI'
            DataSource = DataSource
          end
          object EX_OBSERVA: TDBMemo
            Left = 111
            Top = 38
            Width = 371
            Height = 65
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataField = 'EX_OBSERVA'
            DataSource = DataSource
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 92
    Top = 297
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 524472
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF98A1E2FFF6F7FDFFB7BEEBFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFB1B8
          E9FFF6F7FDFFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF8792DEFFDFE2F6FFA1A9E5FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8
          A1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF8CE2B8FFE9F9
          F1FF54D396FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF52D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF52D395FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF5AD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF62D79FFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4FD293FF54D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF8AE1B7FFE6F9F0FF6AD9A4FF4FD293FF4FD293FF4FD2
          93FF5FD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF52D395FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF65D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF68D8A2FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF6AD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF52D395FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF70DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF57D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF81DFB1FF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF5261CFFF6C79D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6F7CD7FF7582D9FF6471D4FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFF969FE2FF6471D4FF969FE2FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4757CCFF4757CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4757CCFF4757CCFF6F7CD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF6976D6FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF5564D0FF7582D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    AutoAlignBars = True
    UseSystemFont = False
    Top = 184
    DockControlHeights = (
      200
      0
      0
      0)
    inherited BarSuperior: TdxBar
      UseOwnFont = True
      Visible = False
    end
    object BarManagerClasificaciones: TdxBar [1]
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Clasificaciones'
      CaptionButtons = <>
      Color = clBtnHighlight
      DockedDockingStyle = dsLeft
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsLeft
      FloatLeft = 234
      FloatTop = 115
      FloatClientWidth = 300
      FloatClientHeight = 260
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Orientation = 1
      Font.Pitch = fpFixed
      Font.Style = []
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      RotateWhenVertical = False
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = True
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    inherited dxBarButton_CortarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_CopiarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_PegarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_UndoBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      HelpContext = 2
      Hint = 'New Button'
      Visible = ivAlways
      Glyph.Data = {
        6A000000424D6A000000000000003600000028000000010000000D0000000100
        2000000000003400000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      PaintStyle = psCaptionInMenu
      OnClick = dxBarLargeButton9Click
      GlyphLayout = glLeft
      HotGlyph.Data = {
        6A000000424D6A000000000000003600000028000000010000000D0000000100
        2000000000003400000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Width = 200
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 7340136
  end
  object dxSkinController1: TdxSkinController
    Kind = lfFlat
    NativeStyle = False
    SkinName = 'TressMorado2013'
    Left = 120
    Top = 520
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 136
    Top = 416
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svBitmap, svFont]
      Bitmap.Data = {
        6A000000424D6A000000000000003600000028000000010000000D0000000100
        2000000000003400000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGrayText
      Font.Height = 13
      Font.Name = 'SEGOE UI'
      Font.Style = []
    end
  end
end
