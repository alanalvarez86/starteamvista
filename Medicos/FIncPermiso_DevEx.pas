unit FIncPermiso_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TfIncpermisos_DevEx = class(TBaseGridLectura_DevEx)
    IN_FEC_INI: TcxGridDBColumn;
    IN_DIAS: TcxGridDBColumn;
    IN_FEC_FIN: TcxGridDBColumn;
    IN_CLASIFI: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    IN_NUMERO: TcxGridDBColumn;
    IN_COMENTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeImprimir( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  fIncpermisos_DevEx: TfIncpermisos_DevEx;

implementation

uses DMedico,
     ZetaBuscador_DevEx,
     ZetaDialogo,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     FEditIncaPermDlg_DevEx,
     FAyudaContexto;

{$R *.DFM}

procedure TfIncpermisos_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  HelpContext := H00106_Incapacidades_y_permisos;
end;

procedure TfIncpermisos_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  inherited;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

end;

procedure TfIncpermisos_DevEx.Connect;
begin
     with dmMedico do
     begin
          cdsHistorial.Conectar;
          cdsHisPermiso.Conectar;
          cdsHisIncapaci.Conectar;
          DataSource.DataSet:= cdsHistorial;
     end;
end;

procedure TfIncpermisos_DevEx.Refresh;
begin
     dmMedico.cdsHistorial.Refrescar;
     ApplyMinWidth;
end;

procedure TfIncpermisos_DevEx.Agregar;
var
   oForma: TEditIncaPermDlg_DevEx;
   lDerechos: boolean;
   sMensaje: string;
begin
     if dmMedico.EsEmpleado then
     begin
          oForma := TEditIncaPermDlg_DevEx.Create( Self );
          try
             with oForma do
             begin
                  ShowModal;
                  if ModalResult = mrOK then
                  begin
                       lDerechos := TRUE;
                       with dmMedico do
                       begin
                            case Operacion of
                                 0:
                                 begin
                                      TipoIncaPerm := Operacion;
                                      lDerechos := ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_ALTA );
                                      sMensaje := 'No tiene derecho para agregar incapacidades';
                                 end;
                                 1:
                                 begin
                                      TipoIncaPerm := Operacion;
                                      lDerechos := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_ALTA );
                                      sMensaje := 'No tiene derecho para agregar permisos';
                                 end;
                            end;
                            if lDerechos then
                               cdsHistorial.Agregar
                            else
                                ZetaDialogo.ZError( Caption, sMensaje, 0 );
                       end;
                  end;
             end;
          finally
                 FreeAndNil( oForma );
          end;
     end
     else
         ZetaDialogo.ZError( Caption, 'Solamente se pueden agregar registros a empleados', 0 );

end;

procedure TfIncpermisos_DevEx.Borrar;
begin
     dmMedico.cdsHistorial.Borrar;
end;

procedure TfIncpermisos_DevEx.Modificar;
begin
     dmMedico.cdsHistorial.Modificar;
end;

procedure TfIncpermisos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Incapacidades/Permisos', 'CB_CODIGO', dmMedico.cdsHistorial );
end;

function TfIncpermisos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
    Result := FALSE;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               {
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
               }
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
              //end;
          end
          else
          begin
               if ( cdsHistorial.FieldByname( 'CB_CODIGO' ).AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;


function TfIncpermisos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     with dmMedico do
     begin
          if EsPermiso then
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_BAJA );
               if not Result then
                  sMensaje := 'No tiene derecho para borrar permisos';
          end
          else
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_BAJA );
               if not Result then
                  sMensaje := 'No tiene derecho para borrar incapacidades';
          end;
     end;
end;

function TfIncpermisos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     with dmMedico do
     begin
          if EsPermiso then
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_CAMBIO );
               if not Result then
                  sMensaje := 'No tiene derecho para modificar permisos';
          end
          else
          begin
               Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_CAMBIO );
               if not Result then
                  sMensaje := 'No tiene derecho para modificar incapacidades';
          end;
     end;
end;

function TfIncpermisos_DevEx.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_PACIENTE_PERMISOS, K_DERECHO_IMPRESION ) and
               ZAccesosMgr.CheckDerecho( D_PACIENTE_INCAPACIDADES, K_DERECHO_IMPRESION );
     if ( not Result ) then
        sMensaje := 'No tiene derecho para imprimir registros de permisos e incapacidades';
end;

end.
