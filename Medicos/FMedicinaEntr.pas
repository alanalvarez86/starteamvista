unit FMedicinaEntr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Vcl.StdCtrls;

type
  TMedicinaEntr = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  MedicinaEntr: TMedicinaEntr;

implementation

uses
//ZetaBuscador
DMedico, ZetaCommonLists, FAyudaContexto;

{$R *.DFM}

procedure TMedicinaEntr.FormCreate(Sender: TObject);
begin
  inherited;
  //CanLookup := True;
  HelpContext := H00004_Medicina_entregada;
  TipoValorActivo1 := stExpediente;
end;


procedure TMedicinaEntr.Connect;
begin
     with dmMedico do
     begin
          cdsMedicinaEntr.Conectar;
          DataSource.DataSet:= cdsMedicinaEntr;
     end;
end;

procedure TMedicinaEntr.Refresh;
begin
     dmMedico.cdsMedicinaEntr.Refrescar;
end;

procedure TMedicinaEntr.Agregar;
begin
     dmMedico.cdsMedicinaEntr.Agregar;
end;

procedure TMedicinaEntr.Borrar;
begin
     dmMedico.cdsMedicinaEntr.Borrar;
end;

procedure TMedicinaEntr.Modificar;
begin
     dmMedico.cdsMedicinaEntr.Modificar;
end;

{Se requiere el PuedeAgregar para que no agregue
registros cuando la base de datos esta vacia.}
function TMedicinaEntr.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     With dmMedico do
     begin
          if cdsExpediente.Active then
          begin
               Result := inherited PuedeAgregar( sMensaje );
               if Result then
               begin
                    Result := ExisteExpediente;
                    if not Result then
                       sMensaje := 'El Expediente No Existe';
               end;
          end
          else
          begin
               if ( cdsMedicinaEntr.FieldByname('EX_CODIGO').AsInteger <> 0 ) then
                  Result := True;
          end;
     end;
end;


end.
