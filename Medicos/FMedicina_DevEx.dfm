inherited Medicina_DevEx: TMedicina_DevEx
  Left = 307
  Top = 230
  Caption = 'Medicinas'
  ClientHeight = 289
  ClientWidth = 665
  ExplicitWidth = 665
  ExplicitHeight = 289
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 665
    ExplicitWidth = 665
    inherited ValorActivo2: TPanel
      Width = 406
      ExplicitWidth = 406
      inherited textoValorActivo2: TLabel
        Width = 400
        ExplicitLeft = 3
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 665
    Height = 270
    ExplicitWidth = 665
    ExplicitHeight = 270
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object ME_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'ME_CODIGO'
        Options.Grouping = False
      end
      object ME_NOMBRE: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'ME_NOMBRE'
        Options.Grouping = False
      end
      object ME_MEDIDA: TcxGridDBColumn
        Caption = 'Medida'
        DataBinding.FieldName = 'ME_MEDIDA'
      end
      object ME_DESCRIP: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'ME_DESCRIP'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
