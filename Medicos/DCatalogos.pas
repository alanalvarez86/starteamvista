unit DCatalogos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
{$else}
     Catalogos_TLB,
{$endif}
     ZetaClientDataSet;

type
  TdmCatalogos = class(TDataModule)
    cdsNomParamLookUp: TZetaLookupDataSet;
    cdsConceptosLookup: TZetaLookupDataSet;
    cdsCondiciones: TZetaLookupDataSet;
    cdsPuestos: TZetaLookupDataSet;
    cdsTurnos: TZetaLookupDataSet;
    cdsClasifi: TZetaLookupDataSet;
    cdsHorarios: TZetaLookupDataSet;
    cdsContratos: TZetaLookupDataSet;
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosAlAdquirirDatos(Sender: TObject);
    procedure cdsTurnosAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiAlAdquirirDatos(Sender: TObject);
    procedure cdsHorariosAlAdquirirDatos(Sender: TObject);
    procedure cdsContratosAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
{$else}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
{$endif}
  public
    { Public declarations }
  end;

var
  dmCatalogos: TdmCatalogos;

implementation

uses DCliente;

{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsPuestosAlAdquirirDatos(Sender: TObject);
begin
     cdsPuestos.Data := ServerCatalogo.GetPuestos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsTurnosAlAdquirirDatos(Sender: TObject);
begin
     cdsTurnos.Data := ServerCatalogo.GetTurnos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsClasifiAlAdquirirDatos(Sender: TObject);
begin
     cdsClasifi.Data := ServerCatalogo.GetClasifi( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsHorariosAlAdquirirDatos(Sender: TObject);
begin
     cdsHorarios.Data := ServerCatalogo.GetHorarios( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsContratosAlAdquirirDatos(Sender: TObject);
begin
     cdsContratos.Data := ServerCatalogo.GetContratos( dmCliente.Empresa );
end;

end.
