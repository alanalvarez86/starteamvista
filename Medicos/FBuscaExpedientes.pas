unit FBuscaExpedientes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Grids, DBGrids, Db, ZetaDBGrid, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, cxButtons,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, Vcl.ImgList, TressMorado2013;

type
  TBuscarExpediente = class(TForm)
    Panel1: TPanel;
    ApePatLBL: TLabel;
    EX_APE_PAT: TEdit;
    EX_APE_MAT: TEdit;
    ApeMatLBL: TLabel;
    NombreLBL: TLabel;
    EX_NOMBRES: TEdit;
    EsEmpleadoLBL: TLabel;
    rbSi: TRadioButton;
    rbNo: TRadioButton;
    DataSource: TDataSource;
    AceptarBTN: TcxButton;
    BuscarBTN: TcxButton;
    CancelarBTN: TcxButton;
    ExpedienteGrid: TZetaCXGrid;
    ExpedienteGridDBTableView: TcxGridDBTableView;
    EX_CODIGO: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    EX_TIPO: TcxGridDBColumn;
    ExpedienteGridLevel: TcxGridLevel;
    cxImageList24_PanelBotones: TcxImageList;
    procedure CancelarBTNClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BuscarBTNClick(Sender: TObject);
    procedure ExpedienteGridDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ExpedienteGridDBTableViewDblClick(Sender: TObject);
    procedure ExpedienteGridDBTableViewColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure ExpedienteGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
  private
    { Private declarations }
    FNumeroExpediente: Integer;
    FNumeroEmpleado: Integer;
    FSoloExpedientes: Boolean;
    function GetNombrePaciente: string;
  protected
    AColumn: TcxGridDBColumn;
  public
    { Public declarations }
    property NumeroExpediente: Integer read FNumeroExpediente write FNumeroExpediente;
    property NumeroEmpleado: Integer read FNumeroEmpleado write FNumeroEmpleado;
    property NombrePaciente: string read GetNombrePaciente;
    property SoloExpedientes: Boolean read FSoloExpedientes write FSoloExpedientes;
  end;

const
     K_ALTURA_SGRID = 140;
     K_ALTURA_CGRID = 310;

var
  BuscarExpediente: TBuscarExpediente;

function BuscaExpedienteDialogo( const sFilter: String; var iExpediente, iEmpleado: integer; var sDescription: String; lSoloExp: Boolean ): Boolean;

implementation
uses DMedico,
     ZetaCommonClasses,
     ZetaCommonTools,
     DCliente,
     ZetaDialogo,
     ZetaClientDataSet,
     FAyudaContexto;

{$R *.DFM}

procedure TBuscarExpediente.FormCreate(Sender: TObject);
begin
   ExpedienteGridDBTableView.DataController.DataModeController.GridMode:= True;
   HelpContext := H00108_Busqueda_de_Expediente;
   rbSi.Checked := True;
   Height := K_ALTURA_SGRID;
end;

procedure TBuscarExpediente.FormShow(Sender: TObject);
begin
    PRETTYNAME.Width := 230;
    {***(am):Trabaja CON GridMode***}
    if ExpedienteGridDBTableView.DataController.DataModeController.GridMode then
      ExpedienteGridDBTableView.OptionsCustomize.ColumnFiltering := False;

    //Desactiva la posibilidad de agrupar
    ExpedienteGridDBTableView.OptionsCustomize.ColumnGrouping := False;
    //Esconde la caja de agrupamiento
    ExpedienteGridDBTableView.OptionsView.GroupByBox := False;
    //Para que nunca muestre el filterbox inferior
    ExpedienteGridDBTableView.FilterBox.Visible := fvNever;
    //Para que no aparezca el Custom Dialog
    ExpedienteGridDBTableView.FilterBox.CustomizeDialog := False;
    //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
    ExpedienteGridDBTableView.ApplyBestFit();
end;

function BuscaExpedienteDialogo( const sFilter: String; var iExpediente, iEmpleado: integer; var sDescription: String; lSoloExp: Boolean ): Boolean;
begin
     Result := False;
     if ( BuscarExpediente = nil ) then
        BuscarExpediente := TBuscarExpediente.Create( Application );
     try
        if ( BuscarExpediente <> nil ) then
        begin
             with BuscarExpediente do
             begin
                  SoloExpedientes := lSoloExp;
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       iExpediente := NumeroExpediente;
                       iEmpleado := NumeroEmpleado;
                       sDescription := NombrePaciente;
                       Result := True;
                  end;
             end;
        end;
     finally
            FreeAndNil( BuscarExpediente );
     end;
end;

procedure TBuscarExpediente.CancelarBTNClick(Sender: TObject);
begin
     Close;
end;

procedure TBuscarExpediente.BuscarBTNClick(Sender: TObject);
var
   FParamList: TZetaParams;
begin
     FParamList := TZetaParams.Create;
     try
        FParamList.AddString( 'APE_PAT', UPPERCASE(EX_APE_PAT.Text) );
        FParamList.AddString( 'APE_MAT', UPPERCASE(EX_APE_MAT.Text) );
        FParamList.AddString( 'NOMBRES', UPPERCASE(EX_NOMBRES.Text) );
        FParamList.AddBoolean( 'TIPO', rbSi.Checked );
        FParamList.AddBoolean( 'SOLOEXP', SoloExpedientes );
        with dmMedico do
        begin
             HacerBusqueda( FParamList );
             DataSource.DataSet := cdsBusquedas;
             if cdsBusquedas.IsEmpty then
             begin
                  Height := K_ALTURA_SGRID;
                  ExpedienteGrid.Visible := False;
                  ZetaDialogo.ZInformation('Información','No Existe Información Con Los Datos Requeridos', 0);
             end
             else
             begin
                  Height := K_ALTURA_CGRID;
                  with ExpedienteGrid do
                  begin
                       Visible := True;
                       ExpedienteGridDBTableView.Columns[0].Visible := not rbSi.Checked;
                       ExpedienteGridDBTableView.Columns[1].Visible := rbSi.Checked;
                       ExpedienteGridDBTableView.Columns[3].Visible := not rbSi.Checked;
                  end;
             end;
             AceptarBTN.Enabled := ExpedienteGrid.Visible;
        end;
     finally

            FreeAndNil( FParamList );
     end;
end;

function TBuscarExpediente.GetNombrePaciente: string;
begin
     with dmMedico.cdsBusquedas do
          Result := FieldByName('PRETTYNAME').AsString;
end;

procedure TBuscarExpediente.ExpedienteGridDblClick(Sender: TObject);
begin
     with dmMedico.cdsBusquedas do
     begin
          NumeroEmpleado := FieldByName('CB_CODIGO').AsInteger;
          NumeroExpediente := FieldByName('EX_CODIGO').AsInteger;
          ModalResult := mrOk;
     end;
end;

procedure TBuscarExpediente.ExpedienteGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
   if ExpedienteGridDBTableView.DataController.DataModeController.GridMode then
  begin
      inherited;
      Self.AColumn := TcxGridDBColumn(AColumn);
  end;
end;

procedure TBuscarExpediente.ExpedienteGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  if ExpedienteGridDBTableView.DataController.DataModeController.GridMode then
  begin
    inherited;
    ExpedienteGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
  end;
end;

procedure TBuscarExpediente.ExpedienteGridDBTableViewDblClick(Sender: TObject);
begin
    with dmMedico.cdsBusquedas do
     begin
          NumeroEmpleado := FieldByName('CB_CODIGO').AsInteger;
          NumeroExpediente := FieldByName('EX_CODIGO').AsInteger;
          ModalResult := mrOk;
     end;
end;

end.
