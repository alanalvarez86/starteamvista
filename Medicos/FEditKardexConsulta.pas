unit FEditKardexConsulta;

interface

uses
  //ZBaseEdicion,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaNumero, ComCtrls, ZetaFecha, Mask,
  ZetaHora, ZetaEdit, ToolWin, ZetaKeyLookup, Db, ExtCtrls, Buttons,
  ImgList, ZetaDBTextBox, ZetaSmartLists;

type
    //TEditKConsulta = class(TBaseEdicion)
    TEditKConsulta = class(TObject)

    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    CN_TIPO: TZetaDBKeyLookup;
    ToolBar1: TToolBar;
    Expedientetb: TToolButton;
    CN_HOR_INI: TZetaDBHora;
    CN_HOR_FIN: TZetaDBHora;
    CN_FECHA: TZetaDBFecha;
    PageControl: TPageControl;
    tsConsulta: TTabSheet;
    Splitter2: TSplitter;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    CN_ALTURA: TZetaDBNumero;
    CN_PULSO: TZetaDBNumero;
    CN_PESO: TZetaDBNumero;
    CN_TEMPERA: TZetaDBNumero;
    tsDiagnostico: TTabSheet;
    GroupBox4: TGroupBox;
    CN_CUIDADO: TDBMemo;
    GroupBox6: TGroupBox;
    CN_RECETA: TDBMemo;
    GroupBox5: TGroupBox;
    CN_DIAGNOS: TDBMemo;
    DA_CODIGO: TZetaDBKeyLookup;
    ToolBar3: TToolBar;
    MedEntrBtn: TToolButton;
    tsEstudios: TTabSheet;
    Label16: TLabel;
    Label17: TLabel;
    TB_CODIGOEST: TZetaDBKeyLookup;
    GroupBox8: TGroupBox;
    Realiza_LB: TLabel;
    CN_EST_FEC: TZetaDBFecha;
    ImageList1: TImageList;
    CN_EST_HAY: TDBCheckBox;
    CN_PRE_SIS: TZetaDBNumero;
    CN_PRE_DIA: TZetaDBNumero;
    CN_AV_IZQ: TZetaDBNumero;
    CN_AV_DER: TZetaDBNumero;
    US_CODIGO: TZetaDBTextBox;
    Label21: TLabel;
    GroupBox7: TGroupBox;
    CN_OBSERVA: TDBMemo;
    Resultados_GB: TGroupBox;
    CN_EST_RES: TDBMemo;
    GroupBox9: TGroupBox;
    CN_EST_OBS: TDBMemo;
    Label19: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel; 
    CN_IMSS: TDBCheckBox;
    Label27: TLabel;
    GroupBox2: TGroupBox;
    CN_EXPLORA: TDBMemo;
    GroupBox3: TGroupBox;
    CN_SINTOMA: TDBMemo;
    CN_MOTIVO: TDBEdit;
    CN_EST_DES: TDBEdit;
    CN_LIBRAS: TZetaDBNumero;
    CN_FAHREN: TZetaDBNumero;
    CN_PIES: TZetaDBNumero;
    CN_PULGADAS: TZetaDBNumero;
    CN_SUB_SEC: TDBCheckBox;
    OpenDialog: TOpenDialog;
    GroupBox10: TGroupBox;
    btnAgregarDocumento: TSpeedButton;
    btnBorraDocumento: TSpeedButton;
    btnVerDocumento: TSpeedButton;
    lblDescripcionArchivo: TLabel;
    btnEditarDocumento: TSpeedButton;
    lblTipoArchivo: TLabel;
    {procedure FormCreate(Sender: TObject);
    procedure MedEntrBtnClick(Sender: TObject);
    procedure CN_EST_HAYClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ExpedientetbClick(Sender: TObject);
    procedure btnBuscaDoctoClick(Sender: TObject);
    procedure btnVerDocumentoClick(Sender: TObject);
    procedure CN_D_OBSExit(Sender: TObject);
    procedure btnBorraDocumentoClick(Sender: TObject);
    procedure btnAgregarDocumentoClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnEditarDocumentoClick(Sender: TObject); }
  private
   { procedure EnabledControlDocumento;
    procedure AgregaDocumento;
    procedure DialogoAgregaDocumento(const lAgregando: Boolean);   }
  Protected
    { Protected declarations }
    //procedure Connect; override;
    //procedure ImprimirForma;override;
    //procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditKConsulta: TEditKConsulta;

implementation

uses DMedico,
     DSistema,
     FAyudaContexto,
     //FEditDocumento,
     ZAccesosTress,
     ZetaFilesTools,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
//     ZetaBuscador,
     ZAccesosMgr,
     ZImprimeForma,
     DCliente;

{$R *.DFM}

Const K_PESO = 2.2;
      K_LONGITUD = 3.2808;
      K_EMP_MED_ENTR = 1035;

 {
procedure TEditKConsulta.FormCreate(Sender: TObject);
begin
  inherited;
  IndexDerechos := ZAccesosTress.D_EMP_KARDEX_CONSULTAS;
  FirstControl := CN_FECHA;
  TipoValorActivo1 := stExpediente;
  HelpContext:= H00003_Consulta;
  with dmMedico do
  begin
       CN_TIPO.LookupDataset := cdsTConsulta;
       TB_CODIGOEST.LookupDataset := cdsTEstudio;
       DA_CODIGO.LookupDataset := cdsDiagnostico;
  end;
end;

procedure TEditKConsulta.FormShow(Sender: TObject);
begin
  inherited;
  dmMedico.ActivaKcons:=true;
  PageControl.ActivePageIndex := 0;
end;

procedure TEditKConsulta.Connect;
begin
     with dmMedico do
     begin
          cdsDiagnostico.Conectar;
          cdsTConsulta.Conectar;
          cdsTEstudio.Conectar;
          dmSistema.cdsUsuarios.Conectar;
          DataSource.DataSet:= cdsKardexConsultas;
          CN_TIPO.LookupDataset := cdsTConsulta;
     end;
     EnabledControlDocumento;
end;

procedure TEditKConsulta.MedEntrBtnClick(Sender: TObject);
begin
  inherited;
  if ZAccesosMgr.CheckDerecho(  D_MEDICINA_ENTR, K_EMP_MED_ENTR ) and ZAccesosMgr.CheckDerecho(  D_MEDICINA_ENTR, K_DERECHO_ALTA ) then
  begin
       With dmMedico.cdsMedicinaEntr do
       begin
            Conectar;
            Agregar;
       end;
  end
  else
      ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Agregar Medicina', 0 );
end;

procedure TEditKConsulta.CN_EST_HAYClick(Sender: TObject);
begin
  inherited;
  with CN_EST_HAY do
  begin
       CN_EST_FEC.Enabled := Checked;
       CN_EST_RES.Enabled := Checked;
       Realiza_LB.Enabled := Checked;
       Resultados_GB.Enabled := Checked;
  end;
end;

procedure TEditKConsulta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  dmMedico.ActivaKcons:=false;
end;

procedure TEditKConsulta.ExpedientetbClick(Sender: TObject);
begin
  inherited;
  if ZAccesosMgr.CheckDerecho(  D_EMP_SERV_EXPEDIENTE, K_DERECHO_CAMBIO ) then
  begin
       with dmMedico.cdsExpediente do
       begin
            Conectar;
            Modificar;
       end;
       AsignaValoresActivos;
  end
  else
      ZetaDialogo.ZInformation( 'Operaci�n No V�lida', 'No Tiene Permiso Para Modificar Expediente', 0 );
end;

procedure TEditKConsulta.btnBuscaDoctoClick(Sender: TObject);
begin
     inherited;
     with dmMedico.cdsKardexConsultas do
     begin
          OpenDialog.FileName := FieldByName('CN_D_OBS').AsString;
          if OpenDialog.Execute then
          begin
               if Modo = dsBrowse then Edit;
               FieldByName('CN_D_OBS').AsString := OpenDialog.FileName;
               EnabledControlDocumento;
          end;
     end;
end;

procedure TEditKConsulta.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     dmMedico.AbreDocumento;
end;

procedure TEditKConsulta.EnabledControlDocumento;
 var lEnabled : Boolean;
     sDescripcion, sTipo : string;
     oColor : TColor;
begin
     with dmMedico.cdsKardexConsultas do
     begin
          lEnabled := StrLleno( FieldByName('CN_D_OBS').AsString );

          if lEnabled then
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: '+ FieldByName('CN_D_OBS').AsString;
               sTipo := ZetaFilesTools.GetTipoDocumento( FieldByName('CN_D_EXT').AsString );
          end
          else
          begin
               with lblDescripcionArchivo do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay Ning�n Documento Almacenado';
                    sTipo := VACIO;
               end;
          end;
     end;

     btnVerDocumento.Enabled := lEnabled ;
     btnBorraDocumento.Enabled := btnVerDocumento.Enabled;
     btnEditarDocumento.Enabled := btnVerDocumento.Enabled;;

     with lblDescripcionArchivo do
     begin
          Font.Color:= oColor;
          Caption := sDescripcion;
     end;

     lblTipoArchivo.Caption := sTipo;

end;

procedure TEditKConsulta.CN_D_OBSExit(Sender: TObject);
begin
     inherited;
     EnabledControlDocumento
end;

procedure TEditKConsulta.btnBorraDocumentoClick(Sender: TObject);
begin
     inherited;
     with dmMedico do
     begin
          if ZetaDialogo.ZConfirm(Caption, '� Desea Borrar el Documento ' + cdsKardexConsultas.FieldByName('CN_D_OBS').AsString + ' ?', 0, mbNo ) then
          begin
               dmMedico.BorraDocumento;
               EnabledControlDocumento;
          end;
     end;
end;

procedure TEditKConsulta.btnAgregarDocumentoClick(Sender: TObject);
begin
     inherited;
     AgregaDocumento;
end;

procedure TEditKConsulta.AgregaDocumento;
 var sDocumento : string;
begin
     sDocumento := dmMedico.cdsKardexConsultas.FieldByName('CN_D_OBS').AsString;
     if StrVacio( sDocumento ) or ZetaDialogo.ZConfirm( Caption, '� Desea Sustituir el Documento: ' + sDocumento + ' por uno Nuevo ?', 0, mbNo ) then
     begin
          DialogoAgregaDocumento( TRUE );
     end;
end;

procedure TEditKConsulta.DialogoAgregaDocumento( const lAgregando : Boolean );
begin
     with dmMedico.cdsKardexConsultas do
     begin
          if FEditDocumento.EditarDocumento( lAgregando, FieldByName('CN_D_OBS').AsString, H00003_Consulta, dmMedico.CargaDocumento ) then
          begin
               EnabledControlDocumento;
               if state in [dsedit,dsinsert] then
                  Modo:= dsEdit;
          end;
     end;
end;

procedure TEditKConsulta.FormDestroy(Sender: TObject);
begin
     inherited;
     ZetaFilesTools.BorraArchivosTemporales;
end;

procedure TEditKConsulta.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     EnabledControlDocumento;
end;

procedure TEditKConsulta.btnEditarDocumentoClick(Sender: TObject);
begin
     inherited;
     if StrLleno( dmMedico.cdsKardexConsultas.FieldByName('CN_D_OBS').AsString ) then
     begin
          DialogoAgregaDocumento( FALSE );
     end
     else ZetaDialogo.ZError( Caption, 'El Documento No Contiene Informaci�n', 0 );
end;

procedure TEditKConsulta.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enConsulta, dmMedico.cdsKardexConsultas );
end;
    }
end.
