unit FEditEmbarazo_DevEx;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaEdit, Mask,
  ZetaFecha, ZetaDBTextBox, ZetaKeyCombo, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, TressMorado2013, cxControls,
  dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit, cxTextEdit,
  cxMemo, cxDBEdit, dxSkinsCore, dxSkinsdxBarPainter, cxRadioGroup, cxGroupBox;

type
  TEditEmbarazos_DevEx = class(TBaseEdicion_DevEx)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EM_FEC_UM: TZetaDBFecha;
    EM_PRENAT: TZetaDBFecha;
    EM_FEC_PP: TZetaDBFecha;
    EM_POSNAT: TZetaDBFecha;
    Label5: TLabel;
    US_NOMBRE: TZetaDBTextBox;
    Label6: TLabel;
    gbTermino: TGroupBox;
    LBLEM_FEC_FIN: TLabel;
    EM_FEC_FIN: TZetaDBFecha;
    EM_NORMAL: TcxDBRadioGroup;
    EM_FINAL: TDBCheckBox;
    EM_MORTAL: TcxDBRadioGroup;
    gbEmbRiesgo: TGroupBox;
    lblEM_INC_INI: TLabel;
    lblEM_INC_FIN: TLabel;
    EM_INC_INI: TZetaDBFecha;
    EM_INC_FIN: TZetaDBFecha;
    lblEM_OBS_RIE: TLabel;
    EM_OBS_RIE: TcxDBMemo;
    LBLEM_TERMINO: TLabel;
    EM_RIESGO: TDBCheckBox;
    EM_TERMINO: TZetaDBKeyCombo;
    EM_COMENTA: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure EM_FINALClick(Sender: TObject);
    procedure EM_RIESGOClick(Sender: TObject);
    procedure EM_FEC_UMChange(Sender: TObject);
    procedure EM_FEC_PPChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditEmbarazos_DevEx: TEditEmbarazos_DevEx;

implementation

uses
//ZetaBuscador
DMedico, ZAccesosTress, ZetaCommonLists, DSistema, FAyudaContexto;

{$R *.DFM}

procedure TEditEmbarazos_DevEx.FormCreate(Sender: TObject);
begin

     inherited;
     IndexDerechos := ZAccesosTress.D_EMP_KARDEX_EMBARAZOS;
     FirstControl := EM_FEC_UM;
     TipoValorActivo1 := stExpediente;
     HelpContext:= H00105_Embarazos;
end;

procedure TEditEmbarazos_DevEx.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = VK_ESCAPE)then
      close;
end;

procedure TEditEmbarazos_DevEx.Connect;
begin
     with dmMedico do
     begin
          dmSistema.cdsUsuarios.Conectar;
          DataSource.DataSet:= cdsKardexEmbarazo;
     end;
end;

procedure TEditEmbarazos_DevEx.EM_FINALClick(Sender: TObject);
begin
     inherited;
     with EM_FINAL do
     begin
          EM_FEC_FIN.Enabled := Checked;
          EM_TERMINO.Enabled := Checked;
          EM_MORTAL.Enabled := Checked;
          EM_NORMAL.Enabled := Checked;
          LBLEM_FEC_FIN.Enabled := Checked;
          LBLEM_TERMINO.Enabled := Checked;
     end;
end;

procedure TEditEmbarazos_DevEx.EM_RIESGOClick(Sender: TObject);
begin
     inherited;
     with EM_RIESGO do
     begin
          lblEM_INC_INI.Enabled := Checked;
          lblEM_INC_FIN.Enabled := Checked;
          lblEM_OBS_RIE.Enabled := Checked;
          EM_INC_INI.Enabled := Checked;
          EM_INC_FIN.Enabled := Checked;
          EM_OBS_RIE.Enabled := Checked;
     end;
end;

procedure TEditEmbarazos_DevEx.EM_FEC_UMChange(Sender: TObject);
const
     K_FUM = 0;
begin
     inherited;
        with dmMedico do
        begin
             if ( cdsKardexEmbarazo.State in [ dsInsert, dsEdit] ) then
                SincronizaFechas( K_FUM );
        end;
end;

procedure TEditEmbarazos_DevEx.EM_FEC_PPChange(Sender: TObject);
const
     K_FPP = 1;
begin
     inherited;
     with dmMedico do
     begin
          if ( cdsKardexEmbarazo.State in [ dsInsert, dsEdit] ) then
             SincronizaFechas( K_FPP );
     end;
end;


end.
