unit FEditTipoDepositoRetiro_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db, ExtCtrls, ZetaEdit, ZetaNumero,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters,  cxControls,
  dxSkinsdxBarPainter, ImgList, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TEditTipoDepositoRetiro_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_SISTEMA: TDBCheckBox;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditTipoDepositoRetiro_DevEx: TEditTipoDepositoRetiro_DevEx;

implementation

{$R *.DFM}

uses ZAccesosTress, ZetaCommonClasses, ZetaBuscador_DevEx,
     DCajaAhorro;

procedure TEditTipoDepositoRetiro_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_REG_TIPO_DEP_RET;
     IndexDerechos := D_AHORRO_CAT_TIPO_DEPOSITO;
     FirstControl := TB_CODIGO;
end;

procedure TEditTipoDepositoRetiro_DevEx.Connect;
begin
     DataSource.DataSet := dmCajaAhorro.cdsTiposDeposito;
end;

procedure TEditTipoDepositoRetiro_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmCajaAhorro.cdsTiposDeposito );
end;

procedure TEditTipoDepositoRetiro_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     TB_CODIGO.Enabled:= not TB_SISTEMA.Checked; // No lleva if porque TB_SISTEMA solo cambia desde fabrica
     DBCodigoLBL.Enabled:= not TB_SISTEMA.Checked;
end;

end.
