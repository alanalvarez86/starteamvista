inherited RegistroPrestamo: TRegistroPrestamo
  Left = 328
  Top = 117
  Caption = 'Registrar pr'#233'stamo'
  ClientHeight = 603
  ClientWidth = 474
  PixelsPerInch = 96
  TextHeight = 13
  object AH_TIPOLbl: TLabel [0]
    Left = 75
    Top = 79
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pr'#233'stamo:'
  end
  object Label8: TLabel [1]
    Left = 6
    Top = 123
    Width = 116
    Height = 13
    Alignment = taRightJustify
    Caption = 'Referencia de pr'#233'stamo:'
  end
  object Label1: TLabel [2]
    Left = 47
    Top = 147
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de inicio:'
  end
  object EmpleadoLbl: TLabel [3]
    Left = 73
    Top = 100
    Width = 50
    Height = 13
    Caption = 'Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label15: TLabel [4]
    Left = 37
    Top = 170
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de pr'#233'stamo:'
  end
  object TIPO_PRESTAMO: TZetaTextBox [5]
    Left = 168
    Top = 77
    Width = 266
    Height = 17
    AutoSize = False
    Caption = 'TIPO_PRESTAMO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object PR_TIPO: TZetaDBTextBox [6]
    Left = 126
    Top = 77
    Width = 41
    Height = 17
    AutoSize = False
    Caption = 'PR_TIPO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'PR_TIPO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 567
    Width = 474
    TabOrder = 7
    inherited OK: TBitBtn
      Left = 306
    end
    inherited Cancelar: TBitBtn
      Left = 391
    end
  end
  inherited PanelSuperior: TPanel
    Width = 474
    TabOrder = 8
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 474
    TabOrder = 9
    inherited ValorActivo2: TPanel
      Width = 148
    end
  end
  object PR_REFEREN: TZetaDBEdit [10]
    Left = 126
    Top = 119
    Width = 80
    Height = 21
    TabOrder = 1
    DataField = 'PR_REFEREN'
    DataSource = DataSource
  end
  object PR_FECHA: TZetaDBFecha [11]
    Left = 126
    Top = 142
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '17/Dic/97'
    Valor = 35781.000000000000000000
    DataField = 'PR_FECHA'
    DataSource = DataSource
  end
  object CB_CODIGO: TZetaDBKeyLookup [12]
    Left = 126
    Top = 96
    Width = 333
    Height = 21
    TabOrder = 0
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object GBCondiciones: TGroupBox [13]
    Left = 8
    Top = 189
    Width = 457
    Height = 260
    Caption = ' Condiciones del pr'#233'stamo '
    TabOrder = 4
    object LBL_Formula: TLabel
      Left = 74
      Top = 183
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object SBCO_FORMULA: TSpeedButton
      Left = 424
      Top = 179
      Width = 25
      Height = 25
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      OnClick = SBCO_FORMULAClick
    end
    object Label7: TLabel
      Left = 34
      Top = 22
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto solicitado:'
    end
    object Label3: TLabel
      Left = 38
      Top = 45
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa de inter'#233's:'
    end
    object Label4: TLabel
      Left = 68
      Top = 68
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Duraci'#243'n:'
    end
    object Label5: TLabel
      Left = 48
      Top = 114
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total a pagar:'
    end
    object LBL_Cuantos_pagos: TLabel
      Left = 40
      Top = 137
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cu'#225'ntos pagos:'
    end
    object LBL_Cada_Pago: TLabel
      Left = 59
      Top = 160
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cada pago:'
    end
    object Label10: TLabel
      Left = 26
      Top = 234
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Abonos anteriores:'
    end
    object Label14: TLabel
      Left = 68
      Top = 91
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Intereses:'
    end
    object PR_FORMULA: TDBMemo
      Left = 118
      Top = 179
      Width = 305
      Height = 49
      DataField = 'PR_FORMULA'
      DataSource = DataSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 255
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 7
    end
    object PR_MONTO_S: TZetaDBNumero
      Left = 118
      Top = 18
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 0
      Text = '0.00'
      DataField = 'PR_MONTO_S'
      DataSource = DataSource
    end
    object PR_TASA: TZetaDBNumero
      Left = 118
      Top = 41
      Width = 115
      Height = 21
      Mascara = mnTasa
      TabOrder = 1
      Text = '0.0 %'
      DataField = 'PR_TASA'
      DataSource = DataSource
    end
    object PR_MESES: TZetaDBNumero
      Left = 118
      Top = 64
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
      DataField = 'PR_MESES'
      DataSource = DataSource
    end
    object PR_MONTO: TZetaDBNumero
      Left = 118
      Top = 110
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 4
      Text = '0.00'
      DataField = 'PR_MONTO'
      DataSource = DataSource
    end
    object PR_PAGOS: TZetaDBNumero
      Left = 118
      Top = 133
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
      DataField = 'PR_PAGOS'
      DataSource = DataSource
    end
    object PR_PAG_PER: TZetaDBNumero
      Left = 118
      Top = 156
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 6
      Text = '0.00'
      DataField = 'PR_PAG_PER'
      DataSource = DataSource
    end
    object PR_SALDO_I: TZetaDBNumero
      Left = 118
      Top = 230
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 8
      Text = '0.00'
      DataField = 'PR_SALDO_I'
      DataSource = DataSource
    end
    object PR_INTERES: TZetaDBNumero
      Left = 118
      Top = 87
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 3
      Text = '0.00'
      DataField = 'PR_INTERES'
      DataSource = DataSource
    end
  end
  object GBCheque: TGroupBox [14]
    Left = 8
    Top = 453
    Width = 457
    Height = 110
    TabOrder = 6
    object Label11: TLabel
      Left = 33
      Top = 21
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cuenta bancaria:'
    end
    object Label12: TLabel
      Left = 64
      Top = 44
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheque #:'
    end
    object BtnSiguiente: TSpeedButton
      Left = 236
      Top = 39
      Width = 25
      Height = 25
      Hint = 'Sugiere siguiente n'#250'mero de cheque'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333FF3333333333333003333333333333F77F33333333333009033
        333333333F7737F333333333009990333333333F773337FFFFFF330099999000
        00003F773333377777770099999999999990773FF33333FFFFF7330099999000
        000033773FF33777777733330099903333333333773FF7F33333333333009033
        33333333337737F3333333333333003333333333333377333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BtnSiguienteClick
    end
    object Label13: TLabel
      Left = 55
      Top = 67
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object CT_CODIGO: TZetaKeyLookup
      Left = 118
      Top = 18
      Width = 333
      Height = 21
      LookupDataset = dmCajaAhorro.cdsCtasBancarias
      Opcional = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = CT_CODIGOValidKey
    end
    object CM_CHEQUE: TZetaNumero
      Left = 118
      Top = 41
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
    end
    object CM_DESCRIP: TEdit
      Left = 118
      Top = 64
      Width = 331
      Height = 21
      TabOrder = 2
    end
    object ImprimeCheque: TCheckBox
      Left = 35
      Top = 86
      Width = 96
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Imprimir cheque:'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
  end
  object RegistrarCheque: TCheckBox [15]
    Left = 19
    Top = 451
    Width = 105
    Height = 17
    Caption = 'Registrar cheque'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = RegistrarChequeClick
  end
  object CM_PRESTA: TZetaKeyCombo [16]
    Left = 126
    Top = 166
    Width = 115
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    OnChange = CM_PRESTAChange
    ListaFija = lfTiposdePrestamo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object gbNomina: TGroupBox [17]
    Left = 0
    Top = 51
    Width = 474
    Height = 46
    Align = alTop
    Caption = ' N'#243'mina Activa '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    Visible = False
    object PeriodoTipoLbl: TLabel
      Left = 50
      Top = 19
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object TipoNomina: TZetaTextBox
      Tag = 1
      Left = 75
      Top = 17
      Width = 74
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PeriodoNumeroLBL: TLabel
      Left = 159
      Top = 19
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object NumeroNomina: TZetaTextBox
      Tag = 1
      Left = 199
      Top = 17
      Width = 42
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PeriodoDelLBL: TLabel
      Left = 249
      Top = 19
      Width = 14
      Height = 13
      Caption = 'del'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object PeriodoFechaInicial: TZetaTextBox
      Tag = 1
      Left = 264
      Top = 17
      Width = 74
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PeriodoAlLBL: TLabel
      Left = 343
      Top = 19
      Width = 8
      Height = 13
      Caption = 'al'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object PeriodoFechaFinal: TZetaTextBox
      Tag = 1
      Left = 353
      Top = 17
      Width = 74
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  inherited DataSource: TDataSource
    Left = 348
    Top = 128
  end
end
