unit FTipoAhorro_DevEx;

interface

uses Controls, ExtCtrls, Dialogs,
     ZBaseTablasConsulta_DevEx,
     ZAccesosTress;

type
  TipoTAhorro_DevEx = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;


implementation

uses DTablas,
     DGlobal,
     ZGlobalTress,
     DCajaAhorro,
     ZetaCommonClasses;


{ TipoTAhorro }
procedure TipoTAhorro_DevEx.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCajaAhorro.cdsTAhorro;
     HelpContext := H_CAT_TIPO_AHORRO;
     IndexDerechos:= D_TAB_NOM_TIPO_AHORRO;
end;

end.
