inherited CtasBancarias_DevEx: TCtasBancarias_DevEx
  Left = 575
  Top = 179
  Caption = 'Cuentas bancarias'
  ClientHeight = 291
  ClientWidth = 643
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 643
    inherited ValorActivo2: TPanel
      Width = 384
      inherited textoValorActivo2: TLabel
        Width = 378
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 643
    Height = 272
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object CT_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CT_CODIGO'
        MinWidth = 70
      end
      object CT_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CT_NOMBRE'
        MinWidth = 70
      end
      object CT_NUM_CTA: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CT_NUM_CTA'
        MinWidth = 70
      end
      object CT_BANCO: TcxGridDBColumn
        Caption = 'Banco'
        DataBinding.FieldName = 'CT_BANCO'
        MinWidth = 70
      end
      object CT_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'CT_STATUS'
        MinWidth = 70
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Ahorro'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 70
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
