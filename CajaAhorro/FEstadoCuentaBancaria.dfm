inherited EstadoCuentaBancaria: TEstadoCuentaBancaria
  Left = 257
  Top = 289
  Caption = 'Estado de cuenta bancaria'
  ClientHeight = 511
  ClientWidth = 611
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 611
    inherited ValorActivo2: TPanel
      Width = 352
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 611
    Height = 130
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 88
      Top = 12
      Width = 37
      Height = 13
      Caption = 'Cuenta:'
    end
    object Label2: TLabel
      Left = 63
      Top = 36
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha inicial:'
    end
    object Label3: TLabel
      Left = 70
      Top = 60
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha final:'
    end
    object Label8: TLabel
      Left = 30
      Top = 83
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de movimiento:'
    end
    object Label9: TLabel
      Left = 21
      Top = 106
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status de movimiento:'
    end
    object bbtnCalcular: TBitBtn
      Left = 439
      Top = 28
      Width = 56
      Height = 56
      Caption = 'Calcular'
      TabOrder = 5
      OnClick = bbtnCalcularClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object zstcmbMovimientos: TStateComboBox
      Left = 128
      Top = 102
      Width = 115
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      DropDownCount = 0
      ItemHeight = 13
      TabOrder = 4
      ListaFija = lfStatusAhorro
      ListaVariable = lvPuesto
      LlaveNumerica = True
      MaxItems = 10
      Offset = 0
    end
    object zfchFechaIni: TZetaFecha
      Left = 128
      Top = 31
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '28/Feb/06'
      Valor = 38776.000000000000000000
    end
    object zfchFechaFin: TZetaFecha
      Left = 128
      Top = 55
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 2
      Text = '28/Feb/06'
      Valor = 38776.000000000000000000
    end
    object zlkCuentas: TZetaKeyLookup
      Left = 128
      Top = 8
      Width = 300
      Height = 21
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
    end
    object zlkTipoMovimiento: TZetaKeyLookup
      Left = 128
      Top = 79
      Width = 300
      Height = 21
      TabOrder = 3
      TabStop = True
      WidthLlave = 60
    end
  end
  object grdMovimientos: TZetaDBGrid [2]
    Left = 0
    Top = 149
    Width = 611
    Height = 240
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CM_FECHA'
        Title.Caption = 'Fecha'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Tipo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DESCRIPCION'
        Title.Caption = 'Descripci'#243'n'
        Width = 229
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_CHEQUE'
        Title.Caption = 'Cheque'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_DEPOSIT'
        Title.Caption = 'Dep'#243'sito'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_RETIRO'
        Title.Caption = 'Retiro'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SALDO'
        Title.Caption = 'Saldo'
        Width = 92
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DEPOSITOS'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'RETIROS'
        Visible = False
      end>
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 389
    Width = 611
    Height = 122
    Align = alBottom
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 609
      Height = 120
      Align = alClient
      Caption = 'Totales'
      TabOrder = 0
      DesignSize = (
        609
        120)
      object Label4: TLabel
        Left = 405
        Top = 14
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = 'Saldo inicial:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object SALDO_INICIAL: TZetaTextBox
        Left = 482
        Top = 10
        Width = 115
        Height = 21
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object DEPOSITOS: TZetaTextBox
        Left = 482
        Top = 34
        Width = 115
        Height = 21
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object RETIROS: TZetaTextBox
        Left = 482
        Top = 58
        Width = 115
        Height = 20
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label5: TLabel
        Left = 405
        Top = 38
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = '(+) Dep'#243'sito:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 418
        Top = 62
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = '(-) Retiros:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object SALDO_FINAL: TZetaTextBox
        Left = 482
        Top = 90
        Width = 115
        Height = 21
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 10485760
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label7: TLabel
        Left = 395
        Top = 94
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Anchors = [akRight, akBottom]
        Caption = '(=) Saldo final:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Shape1: TShape
        Left = 392
        Top = 84
        Width = 211
        Height = 1
        Anchors = [akRight, akBottom]
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 560
    Top = 80
  end
end
