inherited EditCtasBancarias: TEditCtasBancarias
  Left = 395
  Top = 192
  Caption = 'Cuenta bancaria'
  ClientHeight = 364
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 73
    Top = 41
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 69
    Top = 63
    Width = 40
    Height = 13
    Caption = 'Nombre:'
  end
  object Label3: TLabel [2]
    Left = 18
    Top = 86
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero de cuenta:'
  end
  object Label4: TLabel [3]
    Left = 75
    Top = 110
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Banco:'
  end
  object AH_TIPOLbl: TLabel [4]
    Left = 75
    Top = 134
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ahorro:'
  end
  object Label8: TLabel [5]
    Left = 286
    Top = 41
    Width = 33
    Height = 13
    Caption = 'Status:'
  end
  inherited PanelBotones: TPanel
    Top = 328
  end
  object CT_CODIGO: TZetaDBEdit [9]
    Left = 113
    Top = 37
    Width = 75
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    ConfirmEdit = True
    DataField = 'CT_CODIGO'
    DataSource = DataSource
  end
  object CT_NOMBRE: TDBEdit [10]
    Left = 113
    Top = 59
    Width = 315
    Height = 21
    DataField = 'CT_NOMBRE'
    DataSource = DataSource
    TabOrder = 5
  end
  object CT_NUM_CTA: TDBEdit [11]
    Left = 113
    Top = 82
    Width = 315
    Height = 21
    DataField = 'CT_NUM_CTA'
    DataSource = DataSource
    TabOrder = 6
  end
  object CT_BANCO: TDBEdit [12]
    Left = 113
    Top = 106
    Width = 315
    Height = 21
    DataField = 'CT_BANCO'
    DataSource = DataSource
    TabOrder = 7
  end
  object AH_TIPO: TZetaDBKeyLookup [13]
    Left = 113
    Top = 130
    Width = 340
    Height = 21
    Opcional = False
    IgnorarConfidencialidad = False
    EditarSoloActivos = True
    TabOrder = 8
    TabStop = True
    WidthLlave = 60
    DataField = 'AH_TIPO'
    DataSource = DataSource
  end
  object GBFormatos: TGroupBox [14]
    Left = 8
    Top = 159
    Width = 457
    Height = 74
    Caption = ' Formatos de impresi'#243'n '
    TabOrder = 9
    object FormatoLBL: TLabel
      Left = 56
      Top = 22
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheques:'
    end
    object Label5: TLabel
      Left = 44
      Top = 46
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Liquidaci'#243'n:'
    end
    object CT_REP_CHK: TZetaDBKeyLookup
      Left = 104
      Top = 18
      Width = 340
      Height = 21
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CT_REP_CHK'
      DataSource = DataSource
    end
    object CT_REP_LIQ: TZetaDBKeyLookup
      Left = 104
      Top = 42
      Width = 340
      Height = 21
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
      DataField = 'CT_REP_LIQ'
      DataSource = DataSource
    end
  end
  object GBAdicionales: TGroupBox [15]
    Left = 8
    Top = 239
    Width = 457
    Height = 74
    Caption = ' Adicionales '
    TabOrder = 10
    object Label6: TLabel
      Left = 61
      Top = 20
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
    end
    object Label7: TLabel
      Left = 71
      Top = 44
      Width = 30
      Height = 13
      Caption = 'Texto:'
    end
    object CT_NUMERO: TZetaDBNumero
      Left = 105
      Top = 16
      Width = 121
      Height = 21
      Mascara = mnNumeroGlobal
      TabOrder = 0
      Text = '0.00'
      UseEnterKey = True
      DataField = 'CT_NUMERO'
      DataSource = DataSource
    end
    object CT_TEXTO: TDBEdit
      Left = 105
      Top = 40
      Width = 274
      Height = 21
      DataField = 'CT_TEXTO'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object CT_STATUS: TZetaDBKeyCombo [16]
    Left = 323
    Top = 37
    Width = 105
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 4
    ListaFija = lfStatusAhorro
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'CT_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 105
  end
end
