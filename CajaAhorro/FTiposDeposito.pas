unit FTiposDeposito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TTiposDeposito = class(TBaseConsulta)
    DBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
  end;

var
  TiposDeposito: TTiposDeposito;

implementation

uses dCajaAhorro,
     ZetaCommonClasses,
     ZetaBuscador,
     ZAccesosTress;

{$R *.dfm}

{ TCtasBancarias }

procedure TTiposDeposito.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CAT_TIPOS_DEP_RET;
     IndexDerechos:= D_AHORRO_CAT_TIPO_DEPOSITO;
end;

procedure TTiposDeposito.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsTiposDeposito.Conectar;
          DataSource.DataSet:= cdsTiposDeposito;
     end;
end;

procedure TTiposDeposito.Refresh;
begin
     dmCajaAhorro.cdsTiposDeposito.Refrescar;
end;

procedure TTiposDeposito.Agregar;
begin
     dmCajaAhorro.cdsTiposDeposito.Agregar;
end;

procedure TTiposDeposito.Modificar;
begin
     dmCajaAhorro.cdsTiposDeposito.Modificar;
end;

procedure TTiposDeposito.Borrar;
begin
     dmCajaAhorro.cdsTiposDeposito.Borrar;
end;

procedure TTiposDeposito.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Tipos de Dep�sito/Retiro', 'Tipos de Dep�sito/Retiro', 'TB_CODIGO', dmCajaAhorro.cdsTiposDeposito );
end;

end.
