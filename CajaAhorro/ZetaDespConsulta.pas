unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcSaldosEmp,
                     efcTotalCaja,
                     efcEstadoCuenta,
                     efcReportes,
                     efcQueryGral,
                     efcTOTAhorro,
                     efcTOTPresta,
                     efcCtasBancarias,
                     efcTiposDeposito,
                     efcCatCondiciones,
                     efcSistGlobales,
                     efcSistBitacora,
                     efcDiccion,
                     efcSistProcesos
                      );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonLists,
     FReportes_DevEx,
     FQueryGral_DevEx,
     FSistBitacora_DevEx,
     FSistProcesos_DevEx,
     FDiccion_DevEx,
     FTablas_DevEx,
     FTipoAhorro_DevEx,
     FCtasBancarias_DevEx,
     FTiposDeposito_DevEx,
     FSaldosEmpleado_DevEx,
     FTotalesCajaFondo_DevEx,
     FEstadoCuentaBancaria_DevEx,
     DCliente;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin

          case Forma of
          efcSaldosEmp              : Result := Consulta( D_AHORRO_SALDOS_X_EMP, TSaldosEmpleado_DevEx );
          efcTotalCaja              : Result := Consulta( D_AHORRO_TOT_CAJA_FONDO, TTotalesCajaFondo_DevEx );
          efcTOTAhorro              : Result := Consulta( D_TAB_NOM_TIPO_AHORRO, TipoTAhorro_DevEx );
          efcTOTPresta              : Result := Consulta( D_TAB_NOM_TIPO_PRESTA, TTOTPresta_DevEx );
          efcCtasBancarias          : Result := Consulta( D_AHORRO_CAT_CUENTAS_BANC, TCtasBancarias_DevEx );
          efcEstadoCuenta           : Result := Consulta( D_AHORRO_EDO_CUENTA_BANC, TEstadoCuentaBancaria_DevEx );
          efcTiposDeposito          : Result := Consulta( D_AHORRO_CAT_TIPO_DEPOSITO, TTiposDeposito_DevEx );
          efcReportes               : Result := Consulta( D_REPORTES_CAJAAHORRO, TReportes_DevEx );
          efcQueryGral              : Result := Consulta( D_AHORRO_SQL, TQueryGral_DevEx );
          efcSistBitacora           : Result := Consulta( D_AHORRO_BITACORA, TSistBitacora_DevEx );
          efcSistProcesos           : Result := Consulta( D_AHORRO_BITACORA, TSistProcesos_DevEx ); 
          efcDiccion                : Result := Consulta( D_CAT_CONFI_DICCIONARIO, TDiccion_DevEx ); //Donde esta?

          else
              Result := Consulta( 0, nil );
          end;

end;

end.
