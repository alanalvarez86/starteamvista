unit FRegistroDepositoRetiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, StdCtrls, DBCtrls, Mask, ZetaFecha, ZetaEdit,
  ZetaKeyLookup, DB, ExtCtrls, ZetaSmartLists, Buttons, ZetaNumero,
  ZetaKeyCombo;

type
  TRegistroDepositoRetiro = class(TBaseEdicion)
    EmpleadoLbl: TLabel;
    Label8: TLabel;
    CM_FECHA: TZetaDBFecha;
    Label11: TLabel;
    CT_CODIGO: TZetaDBKeyLookup;
    CM_DEP_RET: TDBRadioGroup;
    Label3: TLabel;
    CM_TIPO: TZetaDBKeyLookup;
    lbMonto: TLabel;
    CM_MONTO: TZetaDBNumero;
    lbCheque: TLabel;
    CM_CHEQUE: TZetaDBNumero;
    BtnSiguiente: TSpeedButton;
    Label13: TLabel;
    CM_DESCRIP: TDBEdit;
    CM_BENEFI: TDBEdit;
    LBL_STATUS_MOV: TLabel;
    CM_STATUS: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure BtnSiguienteClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CM_DEP_RETClick(Sender: TObject);
  private
    { Private declarations }
    FAgregarNuevo: Boolean;
  protected
    { Protected declarations }
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    property AgregarNuevo: Boolean read FAgregarNuevo write FAgregarNuevo;
  end;

var
  RegistroDepositoRetiro: TRegistroDepositoRetiro;

implementation

uses dCajaAhorro,
     dCliente,
     ZetaTipoEntidad,
     ZImprimeForma,
     ZConstruyeFormula,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaClientDataSet, DBasicoCliente;

{$R *.dfm}

procedure TRegistroDepositoRetiro.FormCreate(Sender: TObject);
const
     K_FILTRO_CTAS = '( AH_TIPO = %s )';
begin
     inherited;
     TipoValorActivo1 := stTipoAhorro;
     IndexDerechos := 0; // PENDIENTE
     HelpContext := H_REG_MOVIMIENTOS; 
     FirstControl := CM_MONTO;
     //CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     CM_TIPO.LookupDataset := dmCajaAhorro.cdsTiposDeposito;
     //CM_STATUS.ListaFija := lfStatusAhorro;
     with CT_CODIGO do
     begin
          LookupDataset := dmCajaAhorro.cdsCtasBancarias;
          Filtro := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
     end;
     //PE_NUMERO.LookupDataset := dmCajaAhorro.cdsPeriodo;
end;

procedure TRegistroDepositoRetiro.FormShow(Sender: TObject);
begin
     inherited;
     DBNavigator.Visible := NOT FAgregarNuevo;
     AgregarBtn.Visible := NOT FAgregarNuevo;
     BorrarBtn.Visible := NOT FAgregarNuevo;
     ModificarBtn.Visible := NOT FAgregarNuevo;
     CM_STATUS.Enabled := NOT FAgregarNuevo;
     LBL_STATUS_MOV.Enabled := Not FAgregarNuevo;
     CM_DEP_RETClick(nil);
end;

procedure TRegistroDepositoRetiro.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsCtasBancarias.Conectar;
          cdsTiposDeposito.Conectar;
          cdsPeriodo.Conectar;


          if FAgregarNuevo then
          begin
               cdsCtasMovs.Refrescar;
               cdsCtasMovs.Append;
          end;

          Datasource.Dataset := cdsCtasMovs;

     end;
end;

procedure TRegistroDepositoRetiro.EscribirCambios;
begin
     inherited;
end;

procedure TRegistroDepositoRetiro.Agregar;
begin
     { No hace nada }
end;

procedure TRegistroDepositoRetiro.Borrar;
begin
     { No hace nada }
end;


{ Eventos de controles }

procedure TRegistroDepositoRetiro.BtnSiguienteClick(Sender: TObject);
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
        CM_CHEQUE.Valor := dmCajaAhorro.SiguienteCheque( CT_CODIGO.Llave );
end;

procedure TRegistroDepositoRetiro.OKClick(Sender: TObject);
begin
     inherited;
     if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
end;

procedure TRegistroDepositoRetiro.ImprimirForma;
begin
     inherited;
     //ZImprimeForma.ImprimeUnaForma( enPrestamo, dmCajaAhorro.cdsRegPrestamos );
end;

function TRegistroDepositoRetiro.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TRegistroDepositoRetiro.CM_DEP_RETClick(Sender: TObject);
begin
     inherited;
     CM_CHEQUE.Enabled := ( CM_DEP_RET.ItemIndex = 1 );
     lbCheque.Enabled := CM_CHEQUE.Enabled;
     BtnSiguiente.Enabled := CM_CHEQUE.Enabled;
end;

end.
