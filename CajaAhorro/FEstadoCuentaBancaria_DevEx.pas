unit FEstadoCuentaBancaria_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, ZetaKeyLookup_DevEx,
  StdCtrls, ZetaDBTextBox, Mask, ZetaFecha, ZetaStateComboBox,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters,  dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid,  TressMorado2013, Menus, cxButtons,
  ActnList, ImgList, System.Actions;

type
  TEstadoCuentaBancaria_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SALDO_INICIAL: TZetaTextBox;
    DEPOSITOS: TZetaTextBox;
    RETIROS: TZetaTextBox;
    Label5: TLabel;
    Label6: TLabel;
    SALDO_FINAL: TZetaTextBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    zstcmbMovimientos: TStateComboBox;
    zfchFechaFin: TZetaFecha;
    zfchFechaIni: TZetaFecha;
    zlkCuentas: TZetaKeyLookup_DevEx;
    zlkTipoMovimiento: TZetaKeyLookup_DevEx;
    Shape1: TShape;
    CM_FECHA: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    CM_CHEQUE: TcxGridDBColumn;
    CM_DEPOSIT: TcxGridDBColumn;
    CM_RETIRO: TcxGridDBColumn;
    SALDO: TcxGridDBColumn;
    DEPOSITOS_GRID: TcxGridDBColumn;
    RETIROS_GRID: TcxGridDBColumn;
    bbtnCalcular_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbtnCalcularClick(Sender: TObject);
    procedure bbtnCalcular_DevExClick(Sender: TObject);
  protected
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;

  private
    procedure GetCuentasBancarias;
    { Private declarations }
  public
    function primerDiaDMes(Date: TDateTime):TDateTime;
    function ultimoDiaDMes(Date: TDateTime):TDateTime;
  end;

var
  EstadoCuentaBancaria_DevEx: TEstadoCuentaBancaria_DevEx;

implementation

uses DCajaAhorro,
     DCliente,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosTress;

{$R *.dfm}

//funcion del primer dia..
function TEstadoCuentaBancaria_DevEx.primerDiaDMes(Date: TDateTime): TDateTime;
var
  Anio, Mes, Dia: Word;
begin
     DecodeDate(Date, Anio, Mes, Dia);
     Result := EncodeDate(Anio, Mes, 1);
end;

//funcion del ultimo dia
function TEstadoCuentaBancaria_DevEx.ultimoDiaDMes(Date: TDateTime): TDateTime;
var
  Anio, Mes, Dia: Word;
begin
     DecodeDate(Date, Anio, Mes, Dia);
     Result := EncodeDate(Anio, Mes,MonthDays[IsLeapYear(Anio), Mes]);
end;

procedure TEstadoCuentaBancaria_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     zlkCuentas.LookupDataset := dmCajaAhorro.cdsCtasBancarias;
     zlkTipoMovimiento.LookupDataset := dmCajaAhorro.cdsTiposDeposito;

     zstcmbMovimientos.ItemIndex := 0;
  
     zfchFechaIni.Valor := FirstDayOfMonth(dmCliente.FechaDefault);
     zfchFechaFin.Valor := LastDayOfMonth(dmCliente.FechaDefault);

     DataSource.DataSet := dmCajaAhorro.cdsCtasMovs;
     //grdMovimientos.DataSource := DataSource;
     ZetaDBGridDBTableView.DataController.DataSource := DataSource;
end;

procedure TEstadoCuentaBancaria_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth; //DevEx: Se invoca despues de que todas las columnas del grid han sido creadas.
     inherited;
     //grdMovimientos.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= H_ESTADO_CTA_BANCARIA;
     IndexDerechos:= D_AHORRO_EDO_CUENTA_BANC;
     TipoValorActivo1 := stTipoAhorro;
end;

procedure TEstadoCuentaBancaria_DevEx.Connect;
begin
     inherited;
     with dmCajaAhorro do
     begin
          cdsCtasBancarias.Conectar;
          if( ( ( not cdsCtasMovs.Active ) or cdsCtasMovs.HayQueRefrescar ) ) then
          begin
               with cdsCtasBancarias do
               begin
                    try
                       Filtered := False;
                       Filter := Format( 'AH_TIPO = %s', [ EntreComillas( dmCliente.TipoAhorro ) ] );
                       Filtered := True;
                       First;
                       zlkCuentas.Llave := FieldByName( 'CT_CODIGO' ).AsString;
                    finally
                           Filtered := False;
                           Filter := VACIO;
                    end;
               end;
               zlkCuentas.Filtro := Format( 'AH_TIPO = %s', [ EntreComillas( dmCliente.TipoAhorro ) ] );
          end;
          DataSource.DataSet := cdsCtasMovs;
     end;
     DoRefresh;
end;

procedure TEstadoCuentaBancaria_DevEx.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsCtasMovs.Agregar;
     DoRefresh;
end;

procedure TEstadoCuentaBancaria_DevEx.Borrar;
//var sCuenta:string;
begin
     inherited;
     if ZetaDialogo.ZWarningConfirm( 'Estado de cuenta', '�Desea borrar el registro?', 0, mbCancel ) then
     begin
          //sCuenta := zlkCuentas.Llave;
          dmCajaAhorro.cdsCtasMovs.Borrar;
          //Aplica el BestFit para las columnas despues de que se ha eliminado el registro
          DoBestFit;
          //zlkCuentas.Llave := sCuenta;
          //Refresh;
     end;
end;

procedure TEstadoCuentaBancaria_DevEx.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsCtasMovs.Modificar;
     DoRefresh;
end;

procedure TEstadoCuentaBancaria_DevEx.Refresh;
 var
    oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        inherited;
        GetCuentasBancarias;
        with dmCajaAhorro do
        begin
             SALDO_INICIAL.Caption := FormatFloat( '#,0.00', SaldoInicial );
             DEPOSITOS.Caption := FormatFloat( '#,0.00', cdsCtasMovs.FieldByName('DEPOSITOS').AsFloat );
             RETIROS.Caption := FormatFloat( '#,0.00', cdsCtasMovs.FieldByName('RETIROS').AsFloat );
             SALDO_FINAL.Caption := FormatFloat( '#,0.00', cdsCtasMovs.FieldByName('SALDO').AsFloat );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     //Aplica el BestFit para las columnas despues de que se ha eliminado el registro
     DoBestFit;
end;

procedure TEstadoCuentaBancaria_DevEx.bbtnCalcularClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TEstadoCuentaBancaria_DevEx.GetCuentasBancarias;
var
   oParams:TZetaParams;
begin
     oParams := TZetaParams.Create;
     with oParams do
     begin
          AddString('Cuenta',zlkCuentas.Llave);
          AddDate('FechaIni',zfchFechaIni.Valor);
          AddDate('FechaFin',zfchFechaFin.Valor);
          AddInteger('Status',zstcmbMovimientos.ItemIndex);
          AddString('Tipo',zlkTipoMovimiento.Llave);
     end;
     dmCajaAhorro.GetCuentasBancarias( oParams );
end;


procedure TEstadoCuentaBancaria_DevEx.bbtnCalcular_DevExClick(Sender: TObject);
begin
  inherited;
  DoRefresh;
end;

end.
