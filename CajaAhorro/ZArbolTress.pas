unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB, Controls,
     ZArbolTools, ZNavBarTools, ZetaCommonLists,cxTreeView;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
procedure MuestraArbolOriginal( oArbol: TcxTreeView ); 
//DevEx: Este metodo creara los arbolitos que se asignaran a la NavBar

//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaNavBarUsuario ( oNavBarMgr: TNavBarMgr; const sFileName: String ; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
//procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String; iAccesoGlobal: Integer );
procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses DGlobal,
     DSistema,
     DCliente,
     ZetaDespConsulta,
     ZGlobalTress,
     ZetaCommonTools,
     ZAccesosMgr,
     ZetaTipoEntidad,
     ZBaseNavBarShell;

const
     {******** Caja de Ahorro *****}
     K_ADMINISTRACION                   = 539;
     K_SALDOS_EMPLEADO                  = 540;
     K_TOTALES_CAJA                     = 541;
     K_ESTADO_CUENTA                    = 542;
     K_CAJA_REGISTRO                    = 543;
     K_CAJA_PROCESOS                    = 547;
     K_CAT_CUENTAS_BANCARIAS            = 553;
     K_CAT_TIPO_DEPOSITO                = 554;
     {******** Consultas **********}
     K_CONSULTAS                        = 549;
     K_CONS_REPORTES                    = 518;
     K_CONS_SQL                         = 550;
     {******** Cat�logos **********}
     K_CATALOGOS                        = 552;
     K_TAB_NOM_TIPO_AHORRO              = 193;
     K_TAB_NOM_TIPO_PRESTA              = 194;
     K_CAT_CONFIGURACION                = 159;
     K_CONS_BITACORA                    = 551;
     K_CAT_CONFI_DICCIONARIO            = 162;
     K_CONS_PROCESOS                    = 163; { GA 22/Feb/2000:ANTES ERA UN HUECO 163 }
     {********** Reportes ***********}
     K_REPORTES_EMPLEADOS               = 217;
     K_REPORTES_ASISTENCIA              = 218;
     K_REPORTES_NOMINA                  = 219;
     K_REPORTES_IMSS                    = 220;
     K_REPORTES_CONSULTAS               = 221;
     K_REPORTES_CATALOGOS               = 222;
     K_REPORTES_TABLAS                  = 223;

     //MA( Ver 2.7 ) NOTA: Cuando se agreguen constantes que corresponden a nodos tipo FOLDER dentro de el
     //�rbol de formas en Tress, es necesario que el valor de la constante sea igual al valor del derecho
     //de acceso en ZAccesosTress.pas.
     //                   Ej.- K_CAPACITACION = D_CAPACITACION        ( Sugerencia para versiones futuras )

function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
//DevEx
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     case iNodo of
          // Administraci�n
          K_ADMINISTRACION          : Result := Folder( 'Administraci�n', iNodo );
          K_SALDOS_EMPLEADO         : Result := Forma( 'Saldos de empleado', efcSaldosEmp );
          K_TOTALES_CAJA            : Result := Forma( 'Totales de caja/fondo', efcTotalCaja );
          K_ESTADO_CUENTA           : Result := Forma( 'Estado de cuenta bancaria', efcEstadoCuenta );

          // Consultas
          K_CONSULTAS               : Result := Folder( 'Consultas', iNodo );
          K_CONS_REPORTES           : Result := Forma( 'Reportes', efcReportes );
          K_CONS_SQL                : Result := Forma( 'SQL', efcQueryGral );

          // Cat�logos
          K_CATALOGOS               : Result := Folder( 'Cat�logos', iNodo );
          K_TAB_NOM_TIPO_AHORRO     : Result := Forma( 'Tipo de ahorro', efcTOTAhorro );
          K_TAB_NOM_TIPO_PRESTA     : Result := Forma( 'Tipo de pr�stamo', efcTOTPresta );
          K_CAT_CUENTAS_BANCARIAS   : Result := Forma( 'Cuentas bancarias', efcCtasBancarias );
          K_CAT_TIPO_DEPOSITO       : Result := Forma( 'Tipo de dep�sito/retiro', efcTiposDeposito );

          K_CAT_CONFIGURACION       : Result := Folder( 'Configuraci�n', iNodo );
          K_CONS_BITACORA           : Result := Forma( 'Bit�cora', efcSistBitacora );
          K_CAT_CONFI_DICCIONARIO   : Result := Forma( 'Diccionario de datos', efcDiccion );
          K_CONS_PROCESOS           : Result := Forma( 'Procesos', efcSistProcesos );

       else
           Result := Folder( '', 0 );
     end;
end;
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
     case iNodo of
         // Administraci�n
          K_ADMINISTRACION          : Result := Grupo( 'Administraci�n', iNodo );
         // Consultas
          K_CONSULTAS               : Result := Grupo( 'Consultas', iNodo );
          // Cat�logos
          K_CATALOGOS               : Result := Grupo( 'Cat�logos', iNodo );

          K_CAT_CONFIGURACION       : Result := Grupo( 'Configuraci�n', iNodo );
     else
          Result := Grupo( '', 0 );
     end;
end;

procedure CreaArbolDefault( oArbolMgr : TArbolMgr );

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

begin  // CreaArbolDefault

     // ************ Presupuestos *****************
     NodoNivel( 0, K_ADMINISTRACION );
      NodoNivel( 1, K_SALDOS_EMPLEADO );
      NodoNivel( 1, K_TOTALES_CAJA );
      NodoNivel( 1, K_ESTADO_CUENTA );

     // ******* Cat�logos *************
    NodoNivel( 0, K_CATALOGOS );
      NodoNivel( 1, K_TAB_NOM_TIPO_AHORRO );
      NodoNivel( 1, K_TAB_NOM_TIPO_PRESTA );
      NodoNivel( 1, K_CAT_CUENTAS_BANCARIAS );
      NodoNivel( 1, K_CAT_TIPO_DEPOSITO );
      {$IFDEF DOS_CAPAS}
      NodoNivel( 1, K_CAT_CONFIGURACION );
        NodoNivel( 2, K_CAT_CONFI_DICCIONARIO );
      {$ENDIF}

end;
//DevEx(by am)
procedure CreaArbolitoDefault( oArbolMgr : TArbolMgr; iAccesoGlobal: Integer );
function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo); //Siempre mandar 0 en iNodo
end;
begin
     case iAccesoGlobal of
          // Administraci�n
          K_ADMINISTRACION          :
          begin
               NodoNivel( 0, K_SALDOS_EMPLEADO);
               NodoNivel( 0, K_TOTALES_CAJA );
               NodoNivel( 0, K_ESTADO_CUENTA );
               //NodoNivel( 0, K_CAJA_REGISTRO );
               //NodoNivel( 0, K_CAJA_PROCESOS );
          end;
         // Consultas
          {K_CONSULTAS               :
          begin
               NodoNivel( 0, K_CONS_REPORTES );
               NodoNivel( 0, K_CONS_SQL );
               NodoNivel( 0, K_CONS_BITACORA );
               NodoNivel( 0, K_CONS_PROCESOS );
          end;}
          // Cat�logos
          K_CATALOGOS               :
          begin
               NodoNivel( 0, K_TAB_NOM_TIPO_AHORRO );
               NodoNivel( 0, K_TAB_NOM_TIPO_PRESTA );
               NodoNivel( 0, K_CAT_CUENTAS_BANCARIAS );
               NodoNivel( 0, K_CAT_TIPO_DEPOSITO );
               {$IFDEF DOS_CAPAS}
                       NodoNivel( 0, K_CAT_CONFIGURACION );
                                  NodoNivel( 1, K_CAT_CONFI_DICCIONARIO );
               {$ENDIF}
          end;
     end;
end;
//DevEx
//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
{***DevEx(by am)***}
//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     //CreaArbolitoUsuario( oArbolMgr, '',K_GLOBAL );
                     //Antes metodo: CreaArbolUsuario
                     with oArbolMgr do
                     begin
                           //Configuracion := TRUE;
                           //oArbolMgr.NumDefault := K_GLOBAL ;
                           CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                           Arbol_DevEx.FullCollapse;//edit by mp: el arbol dentro del navbar es el arbol_DevEx
                     end;
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario}
     // ************ Presupuestos *****************
     if( NodoNivel( K_ADMINISTRACION, 1 )) then
                CreaArbolito ( K_ADMINISTRACION, Arbolitos[0]);
     // ******** Consultas ***********
     //if( NodoNivel( K_CONSULTAS, 2 )) then
                //CreaArbolito ( K_CONSULTAS, Arbolitos[1]);
     // ******* Cat�logos *************
     if( NodoNivel( K_CATALOGOS, 2 ) ) then
                CreaArbolito ( K_CATALOGOS, Arbolitos[1]);

end;
//DevEx(by am)
procedure GetDatosNavBarUsuario( oNavBarMgr: TNavBarMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
   oDatosUsuario: TDatosArbolUsuario;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oDatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
     end;
     lImportando := StrLleno( sFileName );
     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oDatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );

        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_ADMINISTRACION; //K_EMP_REGISTRO;
     end;
     oNavBarMgr.DatosUsuario := oDatosUsuario;
end;
{***DevEx(by am)***}
//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
procedure CreaNavBarUsuario (  oNavBarMgr: TNavBarMgr; const sFileName: String; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);
var
   DataSet: TDataSet;
   NodoRaiz: TGrupoInfo;
procedure CreaArbolito( Arbolito: TcxTreeView);//edit by mp: Se modificaron los metodos para Utilizar los TcxTreeView
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                     CreaArbolitoUsuario( oArbolMgr, '' );
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
     if (  oNavBarMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
     else
         DataSet := dmCliente.cdsUsuario;

     GetDatosNavBarUsuario(oNavBarMgr, DataSet, sFileName );
     //CreaNavBarUsuario
     with oNavBarMgr.DatosUsuario do
     begin
          oNavBarMgr.NumDefault := NodoDefault;
          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oNavBarMgr.Configuracion ) or ( TieneArbol ) then
             with NodoRaiz do
             begin
                   Caption := NombreNodoInicial;
                   //EsForma := FALSE;
                   IndexDerechos := K_SIN_RESTRICCION;
                   if oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ) then
                   begin
                        CreaArbolito( ArbolitoUsuario ); //Pendiente cambiar el indice
                   end;
             end;

          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oNavBarMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
             begin
                  if ( oNavBarMgr.NumDefault = 0 ) then
                         oNavBarMgr.NumDefault :=  K_ADMINISTRACION; //Por si solo se agregara el arbol del sistema
                  CreaNavBarDefault(oNavBarMgr, Arbolitos );
                  //PENDIENTE: Implementar FullColapse de la NavBar
             end;

             // Si por derechos no tiene nada, agregar grupo
             if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
             begin
                  with NodoRaiz do
                  begin
                     Caption := 'No tiene derechos';
                     //EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
                  end;
             end;
          end;
     end;
end;
procedure GetDatosArbolUsuario( oArbolMgr: TArbolMgr; DataSet: TDataSet; const sFileName: String );
var
   iUsuario: Integer;
   lImportando: Boolean;
begin
     // SUPONE que 'cdsUsuarios' est� posicionado en el usuario correcto
     with Dataset, oArbolMgr.DatosUsuario do
     begin
          iUsuario          := FieldByName( 'US_CODIGO' ).AsInteger;
          NombreNodoInicial := FieldByName( 'US_NOMBRE' ).AsString;
          IncluirSistema    := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
          NodoDefault       := FieldByName( 'US_FORMA' ).AsInteger;
          TieneArbol        := FALSE;
     end;
     lImportando := StrLleno( sFileName );
     // Se trae los registros de Arbol del Servidor
     with dmSistema, cdsArbol, oArbolMgr.DatosUsuario do
     begin
            if ( lImportando ) then
              cdsArbol.LoadFromFile( sFileName )
            else
               GetArbolUsuario( iUsuario );

        if not IsEmpty then
        begin
             // Busca el Primer registro para obtener nombre del Nodo
             if ( FieldByName( 'AB_ORDEN' ).AsInteger <= 0 ) then
             begin
               // En caso de IMPORTACION
               // En el primer registro va 'IncluirSistema' y 'Forma Default'
               if ( lImportando ) then
               begin
                    IncluirSistema := FieldByName( 'AB_NIVEL' ).AsInteger > 0;
                    NodoDefault    := FieldByName( 'AB_NODO' ).AsInteger;
               end
               else // Si viene de Base de Datos, permite renombrar RAIZ
               begin
                    if StrLleno( FieldByName( 'AB_DESCRIP' ).AsString ) then
                       NombreNodoInicial:= FieldByName('AB_DESCRIP').AsString;
               end;
               Next;
             end;
             TieneArbol := Not Eof;
        end;
        if ( NodoDefault = 0 ) then
           NodoDefault     := K_ADMINISTRACION; //K_EMP_REGISTRO;
     end;
end;
{procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin
     // CreaArbolUsuario
     with oArbolMgr do
     begin
          Configuracion := TRUE;
          oArbolMgr.NumDefault := K_ADMINISTRACION;
          CreaArbolDefault( oArbolMgr );
          Arbol.FullCollapse;
     end;
end;}
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
var
   NodoRaiz: TNodoInfo;
   DataSet: TDataSet;

  procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
  var
     oNodoInfo : TNodoInfo;
  begin
       oNodoInfo := GetNodoInfo( iNodo );
       oNodoInfo.Caption := sCaption;
       oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
  end;
begin
      // CreaArbolUsuario
       if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

       GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );

       with oArbolMgr.DatosUsuario do
       begin
          oArbolMgr.NumDefault := NodoDefault;

          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oArbolMgr.Configuracion ) or ( TieneArbol ) then
              with NodoRaiz do
              begin
                   Caption := NombreNodoInicial;
                   EsForma := FALSE;
                   IndexDerechos := K_SIN_RESTRICCION;
                   oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
              end;

          if TieneArbol then
            with dmSistema.cdsArbol do
              while not Eof do
              begin
                   {Validacion agregada para la creacion del arbol de preferencias
                   -Para la Vista Clasica aplicara el metodo normal
                   -Para la Vista Actual no agregara las opciones de las constantes.}
                        {Checamos que no se de los nodos eliminados para la vista actual
                        porque ya se encuentran en el ribbon.}
                        if ( ( FieldByName( 'AB_NODO' ).AsInteger <> K_CAJA_REGISTRO ) and
                           ( FieldByName( 'AB_NODO' ).AsInteger <> K_CAJA_PROCESOS ) and
                           ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_REPORTES ) and
                           ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_SQL      ) and
                           ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_BITACORA ) and
                           ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_PROCESOS ) and
                           ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONSULTAS     )) then
                        begin
                             NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger,
                                        FieldByName( 'AB_NODO' ).AsInteger,
                                        FieldByName( 'AB_DESCRIP' ).AsString );

                        end; //End if 1
              Next;
              end;

          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oArbolMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
                ZArbolTress.CreaArbolDefault( oArbolMgr );

             // Si por derechos no tiene nada, agregar nodo
             if ( oArbolMgr.Arbol.Items.Count = 0 ) then
             begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oArbolMgr.NodoNivel( 0, NodoRaiz, K_ARBOL_NODO_RAIZ );
                end;
             end;
          end;
        end;

end;

{***DevEx(by am)***}
procedure CreaArbolitoUsuario( oArbolMgr: TArbolMgr; const sFileName: String);
var
   //NodoRaiz: TNodoInfo;
   DataSet: TDataSet;
procedure NodoNivel( const iNivel, iNodo : Integer; const sCaption : String );
var
   oNodoInfo : TNodoInfo;
begin
     oNodoInfo := GetNodoInfo( iNodo );
     oNodoInfo.Caption := sCaption;
     oArbolMgr.NodoNivel( iNivel, oNodoInfo, iNodo );
end;
begin
     if ( oArbolMgr.Configuracion ) then
          DataSet := dmSistema.cdsUsuarios
       else
           DataSet := dmCliente.cdsUsuario;

     GetDatosArbolUsuario( oArbolMgr, DataSet, sFileName );
     with oArbolMgr.DatosUsuario do
     begin
           if TieneArbol then
            with dmSistema.cdsArbol do
              while not Eof do
              begin
                   //Checamos que no se de los nodos eliminados para la vista actual porque ya se encuentran
                   //en el ribbon.
                   if ( ( FieldByName( 'AB_NODO' ).AsInteger <> K_CAJA_REGISTRO ) and
                        ( FieldByName( 'AB_NODO' ).AsInteger <> K_CAJA_PROCESOS ) and
                        ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_REPORTES ) and
                        ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_SQL      ) and
                        ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_BITACORA ) and
                        ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONS_PROCESOS ) and
                        ( FieldByName( 'AB_NODO' ).AsInteger <> K_CONSULTAS     )) then
                   begin
                        //A AB_NIVEL LE RESTAMOS UNO POR QUE EL GRUPO SERA E NODO PADRE
                        NodoNivel( FieldByName( 'AB_NIVEL' ).AsInteger -1,
                              FieldByName( 'AB_NODO' ).AsInteger,
                              FieldByName( 'AB_DESCRIP' ).AsString );
                   end;
                   Next;
              end;
     end;
     // Antes: CreaArbolUsuario
     {with oArbolMgr do
     begin
          //Configuracion := TRUE;
          oArbolMgr.NumDefault := iAccesoGlobal;
          CreaArbolitoDefault( oArbolMgr, iAccesoGlobal );
          Arbol.FullCollapse;
     end;}
end;


//DevEx(@am): Agregado para ver el nombre original de los nodos del arbol de usuario.
procedure MuestraArbolOriginal( oArbol: TcxTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;

end.
