unit FCtasBancarias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, 
  TressMorado2013, Menus, ActnList, ImgList, StdCtrls;

type
  TCtasBancarias_DevEx = class(TBaseGridLectura_DevEx)
    CT_CODIGO: TcxGridDBColumn;
    CT_NOMBRE: TcxGridDBColumn;
    CT_NUM_CTA: TcxGridDBColumn;
    CT_BANCO: TcxGridDBColumn;
    CT_STATUS: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
  end;

var
  CtasBancarias_DevEx: TCtasBancarias_DevEx;

implementation

uses dCajaAhorro,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosTress;

{$R *.dfm}

{ TCtasBancarias }

procedure TCtasBancarias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CAT_CTAS_BANCARIAS;  // PENDIENTE
     IndexDerechos:=  D_AHORRO_CAT_CUENTAS_BANC;
end;

procedure TCtasBancarias_DevEx.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsCtasBancarias.Conectar;
          DataSource.DataSet:= cdsCtasBancarias;
     end;
end;

procedure TCtasBancarias_DevEx.Refresh;
begin
     dmCajaAhorro.cdsCtasBancarias.Refrescar;
end;

procedure TCtasBancarias_DevEx.Agregar;
begin
     dmCajaAhorro.cdsCtasBancarias.Agregar;
end;

procedure TCtasBancarias_DevEx.Modificar;
begin
     dmCajaAhorro.cdsCtasBancarias.Modificar;
end;

procedure TCtasBancarias_DevEx.Borrar;
begin
     dmCajaAhorro.cdsCtasBancarias.Borrar;
     //Aplica el BestFit para las columnas despues de que se ha eliminado el registro
     DoBestFit;
end;

procedure TCtasBancarias_DevEx.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'Cuentas bancarias', 'Cuentas bancarias', 'CT_CODIGO', dmCajaAhorro.cdsCtasBancarias );
     ZetaBuscador_DevEx.BuscarCodigo( 'Cuentas bancarias', 'Cuentas bancarias', 'CT_CODIGO', dmCajaAhorro.cdsCtasBancarias );
end;

procedure TCtasBancarias_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth; //DevEx: Se invoca despues de que todas las columnas del grid han sido creadas.
  inherited;
end;

end.
