inherited SaldosEmpleado: TSaldosEmpleado
  Left = 324
  Top = 248
  Caption = 'Saldos de empleado'
  ClientHeight = 458
  ClientWidth = 607
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 607
    inherited ValorActivo2: TPanel
      Width = 348
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 607
    Height = 142
    Align = alTop
    TabOrder = 1
    object Label18: TLabel
      Left = 38
      Top = 70
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total ahorrado:'
    end
    object Label19: TLabel
      Left = 29
      Top = 93
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo pr'#233'stamos:'
    end
    object Label25: TLabel
      Left = 84
      Top = 116
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Neto:'
    end
    object Label8: TLabel
      Left = 9
      Top = 14
      Width = 101
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha de inscripci'#243'n:'
    end
    object Label2: TLabel
      Left = 77
      Top = 37
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object AH_SALDO: TZetaDBTextBox
      Left = 114
      Top = 66
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'AH_SALDO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AH_SALDO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PR_SALDO: TZetaDBTextBox
      Left = 114
      Top = 89
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'PR_SALDO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'PR_SALDO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AH_NETO: TZetaDBTextBox
      Left = 114
      Top = 112
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'AH_NETO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AH_NETO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AH_FECHA: TZetaDBTextBox
      Left = 114
      Top = 9
      Width = 115
      Height = 22
      Cursor = crArrow
      AutoSize = False
      Caption = 'AH_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AH_FECHA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AH_STATUS: TZetaDBTextBox
      Left = 114
      Top = 33
      Width = 115
      Height = 21
      AutoSize = False
      Caption = 'AH_STATUS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AH_STATUS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 161
    Width = 607
    Height = 297
    Align = alClient
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 605
      Height = 29
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object BBAgregar: TBitBtn
        Left = 8
        Top = 1
        Width = 122
        Height = 25
        Hint = 'Registrar un pr'#233'stamo'
        Caption = 'Prestar'
        TabOrder = 0
        OnClick = BBAgregarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BBModificar: TBitBtn
        Left = 131
        Top = 1
        Width = 122
        Height = 25
        Hint = 'Modificar el pr'#233'stamo seleccionado'
        Caption = 'Modificar pr'#233'stamo'
        TabOrder = 1
        OnClick = BBModificarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
      end
      object BBBorrar: TBitBtn
        Left = 254
        Top = 1
        Width = 122
        Height = 25
        Hint = 'Borrar el pr'#233'stamo seleccionado'
        Caption = 'Borrar pr'#233'stamo'
        TabOrder = 2
        OnClick = BBBorrarClick
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55558888888585599958555555555550305555555550055BB0555555550FB000
          0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
          B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
          B05500000E055550705550000055555505555500055555550555}
      end
    end
    object GridRenglones: TZetaDBGrid
      Left = 1
      Top = 30
      Width = 605
      Height = 266
      Align = alClient
      DataSource = dsRenglon
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'PR_REFEREN'
          Title.Caption = 'Referencia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TB_ELEMENT'
          Title.Caption = 'Descripci'#243'n'
          Width = 128
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_FECHA'
          Title.Caption = 'Fecha'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_MONTO'
          Title.Caption = 'Monto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_PAGOS'
          Title.Caption = 'Pagos'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_SALDO'
          Title.Caption = 'Saldo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CM_PRESTA'
          Title.Caption = 'Tipo de pr'#233'stamo'
          Width = 120
          Visible = True
        end>
    end
  end
  inherited DataSource: TDataSource
    Left = 320
    Top = 32
  end
  object dsRenglon: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 352
    Top = 32
  end
end
