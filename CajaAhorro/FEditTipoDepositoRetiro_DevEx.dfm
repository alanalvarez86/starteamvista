inherited EditTipoDepositoRetiro_DevEx: TEditTipoDepositoRetiro_DevEx
  Left = 565
  Top = 335
  Caption = 'Tipo de dep'#243'sito / retiro'
  ClientHeight = 199
  ClientWidth = 395
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 46
    Top = 82
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 18
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 41
    Top = 40
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 47
    Top = 123
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 37
    Top = 102
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  inherited PanelBotones: TPanel
    Top = 163
    Width = 395
    TabOrder = 8
    inherited OK_DevEx: TcxButton
      Left = 232
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 312
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 395
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 69
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 11
  end
  object TB_SISTEMA: TDBCheckBox [8]
    Left = 35
    Top = 141
    Width = 59
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Sistema:'
    DataField = 'TB_SISTEMA'
    DataSource = DataSource
    Enabled = False
    TabOrder = 12
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object TB_CODIGO: TZetaDBEdit [9]
    Left = 81
    Top = 36
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [10]
    Left = 81
    Top = 57
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [11]
    Left = 81
    Top = 78
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [12]
    Left = 81
    Top = 119
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_NUMERO: TZetaDBNumero [13]
    Left = 81
    Top = 98
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 284
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
