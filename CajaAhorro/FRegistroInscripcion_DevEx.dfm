inherited RegistroInscripcion_DevEx: TRegistroInscripcion_DevEx
  Left = 398
  Top = 283
  Caption = 'Inscribir en ahorro'
  ClientHeight = 227
  ClientWidth = 436
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLbl: TLabel [0]
    Left = 34
    Top = 60
    Width = 50
    Height = 13
    Caption = '&Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 85
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de Inicio:'
  end
  object Label18: TLabel [2]
    Left = 25
    Top = 108
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo inicial:'
  end
  inherited PanelBotones: TPanel
    Top = 191
    Width = 436
    inherited OK_DevEx: TcxButton
      Left = 272
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 352
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 436
    inherited ValorActivo2: TPanel
      Width = 110
      inherited textoValorActivo2: TLabel
        Width = 104
      end
    end
  end
  object CB_CODIGO: TZetaDBKeyLookup_DevEx [5]
    Left = 86
    Top = 56
    Width = 340
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object AH_FECHA: TZetaDBFecha [6]
    Left = 86
    Top = 80
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '17/Dec/97'
    Valor = 35781.000000000000000000
    DataField = 'AH_FECHA'
    DataSource = DataSource
  end
  object AH_SALDO_I: TZetaDBNumero [7]
    Left = 86
    Top = 104
    Width = 115
    Height = 21
    Mascara = mnPesos
    TabOrder = 4
    Text = '0.00'
    DataField = 'AH_SALDO_I'
    DataSource = DataSource
  end
  object GroupBox1: TGroupBox [8]
    Left = 8
    Top = 128
    Width = 417
    Height = 57
    Caption = '                           '
    TabOrder = 6
    object lbFormula: TLabel
      Left = 32
      Top = 28
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object AH_FORMULA: TDBEdit
      Left = 78
      Top = 24
      Width = 320
      Height = 21
      DataField = 'AH_FORMULA'
      DataSource = DataSource
      TabOrder = 1
    end
    object nPorcentaje: TZetaNumero
      Left = 78
      Top = 24
      Width = 121
      Height = 21
      Mascara = mnTasa
      TabOrder = 0
      Text = '0.0 %'
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 240
    Top = 88
    TabOrder = 7
  end
  object cbPorcentaje: TCheckBox [10]
    Left = 18
    Top = 125
    Width = 73
    Height = 17
    Caption = 'Porcentaje'
    TabOrder = 5
    OnClick = cbPorcentajeClick
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
