inherited RegistroDepositoRetiro_DevEx: TRegistroDepositoRetiro_DevEx
  Left = 438
  Top = 216
  Caption = 'Tipo de dep'#243'sito / retiro'
  ClientHeight = 327
  ClientWidth = 471
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLbl: TLabel [0]
    Left = 68
    Top = 220
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Beneficiario:'
  end
  object Label8: TLabel [1]
    Left = 93
    Top = 124
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
  end
  object Label11: TLabel [2]
    Left = 45
    Top = 100
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cuenta bancaria:'
  end
  object Label3: TLabel [3]
    Left = 102
    Top = 172
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  object lbMonto: TLabel [4]
    Left = 93
    Top = 148
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Monto:'
  end
  object lbCheque: TLabel [5]
    Left = 76
    Top = 196
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cheque #:'
    Enabled = False
  end
  object Label13: TLabel [6]
    Left = 67
    Top = 244
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object LBL_STATUS_MOV: TLabel [7]
    Left = 22
    Top = 268
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status de movimiento:'
  end
  inherited PanelBotones: TPanel
    Top = 291
    Width = 471
    TabOrder = 11
    inherited OK_DevEx: TcxButton
      Left = 302
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 381
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 471
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 145
      inherited textoValorActivo2: TLabel
        Width = 139
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 14
  end
  object CM_FECHA: TZetaDBFecha [11]
    Left = 130
    Top = 118
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '17/dic/97'
    Valor = 35781.000000000000000000
    DataField = 'CM_FECHA'
    DataSource = DataSource
  end
  object CT_CODIGO: TZetaDBKeyLookup_DevEx [12]
    Left = 130
    Top = 94
    Width = 333
    Height = 21
    LookupDataset = dmCajaAhorro.cdsCtasBancarias
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 80
    DataField = 'CT_CODIGO'
    DataSource = DataSource
  end
  object CM_DEP_RET: TDBRadioGroup [13]
    Left = 130
    Top = 50
    Width = 185
    Height = 41
    Caption = ' Movimiento '
    Columns = 2
    DataField = 'CM_DEP_RET'
    DataSource = DataSource
    Items.Strings = (
      'Dep'#243'sito'
      'Retiro')
    TabOrder = 1
    Values.Strings = (
      'D'
      'R')
    OnClick = CM_DEP_RETClick
  end
  object CM_TIPO: TZetaDBKeyLookup_DevEx [14]
    Left = 130
    Top = 168
    Width = 333
    Height = 21
    LookupDataset = dmCajaAhorro.cdsCtasBancarias
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 5
    TabStop = True
    WidthLlave = 80
    DataField = 'CM_TIPO'
    DataSource = DataSource
  end
  object CM_MONTO: TZetaDBNumero [15]
    Left = 130
    Top = 143
    Width = 115
    Height = 21
    Mascara = mnPesos
    TabOrder = 4
    Text = '0.00'
    DataField = 'CM_MONTO'
    DataSource = DataSource
  end
  object CM_CHEQUE: TZetaDBNumero [16]
    Left = 130
    Top = 193
    Width = 115
    Height = 21
    Enabled = False
    Mascara = mnDias
    TabOrder = 15
    Text = '0'
    DataField = 'CM_CHEQUE'
    DataSource = DataSource
  end
  object CM_DESCRIP: TDBEdit [17]
    Left = 130
    Top = 242
    Width = 331
    Height = 21
    DataField = 'CM_DESCRIP'
    DataSource = DataSource
    TabOrder = 7
  end
  object CM_BENEFI: TDBEdit [18]
    Left = 130
    Top = 218
    Width = 331
    Height = 21
    DataField = 'CM_BENEFI'
    DataSource = DataSource
    TabOrder = 6
  end
  object CM_STATUS: TZetaDBKeyCombo [19]
    Left = 130
    Top = 265
    Width = 115
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 8
    ListaFija = lfStatusAhorro
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'CM_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object BtnSiguiente_DevEx: TcxButton [20]
    Left = 248
    Top = 193
    Width = 21
    Height = 21
    Hint = 'Sugiere siguiente n'#250'mero de cheque'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 16
    OnClick = BtnSiguienteClick
    OptionsImage.Glyph.Data = {
      DA050000424DDA05000000000000360000002800000013000000130000000100
      200000000000A4050000000000000000000000000000000000008B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFBAB4BBFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA69F
      A7FFF6F5F6FFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFFACA5ACFFFAF9FAFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFFFFFFC5C0
      C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D95
      9EFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFBFBFBFFFFFFFFFFFFFFFFFFFFFF
      FFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA69F
      A7FFFAF9FAFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFFA199A2FFF4F3F4FFFFFFFFFFC5C0C6FF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D95
      9EFFEFEDEFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF968D97FFB3ACB4FF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
      8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
  end
  inherited DataSource: TDataSource
    Left = 8
    Top = 97
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
