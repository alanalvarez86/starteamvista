unit FEditTipoDepositoRetiro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaEdit, ZetaNumero, ZetaSmartLists;

type
  TEditTipoDepositoRetiro = class(TBaseEdicion)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_SISTEMA: TDBCheckBox;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditTipoDepositoRetiro: TEditTipoDepositoRetiro;

implementation

{$R *.DFM}

uses ZAccesosTress, ZetaCommonClasses, ZetaBuscador,
     DCajaAhorro;

procedure TEditTipoDepositoRetiro.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_REG_TIPO_DEP_RET;
     IndexDerechos := D_AHORRO_CAT_TIPO_DEPOSITO;
     FirstControl := TB_CODIGO;
end;

procedure TEditTipoDepositoRetiro.Connect;
begin
     DataSource.DataSet := dmCajaAhorro.cdsTiposDeposito;
end;

procedure TEditTipoDepositoRetiro.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmCajaAhorro.cdsTiposDeposito );
end;

procedure TEditTipoDepositoRetiro.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     TB_CODIGO.Enabled:= not TB_SISTEMA.Checked; // No lleva if porque TB_SISTEMA solo cambia desde fabrica
     DBCodigoLBL.Enabled:= not TB_SISTEMA.Checked;
end;

end.
