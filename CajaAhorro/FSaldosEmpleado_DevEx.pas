unit FSaldosEmpleado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, ComCtrls, DBCtrls,
  StdCtrls, Mask, ZetaNumero, ZetaKeyCombo, ZetaFecha,
  ZetaDBTextBox, ZetaMessages,
  ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, Menus,
  ActnList, ImgList, TressMorado2013, cxButtons, System.Actions;

type
  TSaldosEmpleado_DevEx = class(TBaseConsulta)
    Panel1: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    Label25: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    AH_SALDO: TZetaDBTextBox;
    PR_SALDO: TZetaDBTextBox;
    AH_NETO: TZetaDBTextBox;
    AH_FECHA: TZetaDBTextBox;
    AH_STATUS: TZetaDBTextBox;
    Panel2: TPanel;
    Panel3: TPanel;
    dsRenglon: TDataSource;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    PR_REFEREN: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    PR_FECHA: TcxGridDBColumn;
    PR_MONTO: TcxGridDBColumn;
    PR_PAGOS: TcxGridDBColumn;
    PR_SALDO_GRID: TcxGridDBColumn;
    CM_PRESTA: TcxGridDBColumn;
    cxImage16: TcxImageList;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    PopupMenu1: TPopupMenu;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    mImprimir: TMenuItem;
    BBAgregar_DevEx: TcxButton;
    BBModificar_DevEx: TcxButton;
    BBBorrar_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);

  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function PuedeModificarPresta: Boolean;
    function PuedeBorrarPresta: Boolean;
    function PuedeAgregarPresta: Boolean;
    procedure ModificaPrestamo;
    //DevEx (by ame): Proceso Agregado para asignar el MinWidth Ideal
    procedure ApplyMinWidth;
  protected
    AColumn: TcxGridDBColumn;
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  end;

var
  SaldosEmpleado_DevEx: TSaldosEmpleado_DevEx;

implementation

uses
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaDialogo,
    DCajaAhorro,
    ZAccesosTress,
    ZAccesosMgr,
    ZetaClientDataSet,
    ZGridModeTools;


{$R *.dfm}

procedure TSaldosEmpleado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //GridRenglones.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= H_SALDOS_EMPLEADO;
     IndexDerechos := D_AHORRO_SALDOS_X_EMP;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stTipoAhorro;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     //ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
end;

procedure TSaldosEmpleado_DevEx.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsHisAhorros.Refrescar;
          Datasource.Dataset := cdsHisAhorros;
          dsRenglon.DataSet := cdsHisPrestamos;
     end;
     {***Aplicar Best Fit es Necesario al Reconectar, pues para esta forma la informacion
         del grid puede ser cambiada desde otros botones del ribbon (no los botones comunes
         de Agregar, Borrar y Modificar)***}
     DoBestFit;
end;

procedure TSaldosEmpleado_DevEx.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Agregar;
end;

procedure TSaldosEmpleado_DevEx.Borrar;
begin
     dmCajaAhorro.BorrarIncripcion;
end;

procedure TSaldosEmpleado_DevEx.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Modificar;
end;

procedure TSaldosEmpleado_DevEx.Refresh;
begin
     dmCajaAhorro.cdsHisAhorros.Refrescar;
end;

procedure TSaldosEmpleado_DevEx.BBAgregarClick(Sender: TObject);
begin
     inherited;
     if PuedeAgregarPresta then
     begin
          dmCajaAhorro.cdsHisPrestamos.Agregar;
          DoBestFit;
     end;

end;

procedure TSaldosEmpleado_DevEx.BBModificarClick(Sender: TObject);
begin
     inherited;
     ModificaPrestamo;
end;

procedure TSaldosEmpleado_DevEx.FormDblClick(Sender: TObject);
begin
     if ( ActiveControl = ZetaDBGrid )  then
         ModificaPrestamo
     else
         inherited;
end;

procedure TSaldosEmpleado_DevEx.WMExaminar(var Message: TMessage);
begin
     ModificaPrestamo;
end;

procedure TSaldosEmpleado_DevEx.BBBorrarClick(Sender: TObject);
begin
     inherited;
     if ( PuedeBorrarPresta ) then
     begin
          with dmCajaAhorro.cdsHisPrestamos do
          begin
               if RecordCount = 0 then
                  ZInformation(Self.Caption,' No existen préstamos para borrar ',0)
               else
               begin
                    Borrar;
                    //DevEx: Si borra el registro que Aplique BestFit
                    DoBestFit;
               end;
          end;
     end;
end;

procedure TSaldosEmpleado_DevEx.ModificaPrestamo;
begin
     if PuedeModificarPresta then
     begin
          dmCajaAhorro.cdsHisPrestamos.Modificar;
          DoBestFit;
     end;

end;

function TSaldosEmpleado_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= Revisa(D_AHORRO_REG_INSCRIP_EMP);
     if not Result then
        sMensaje:= 'No tiene permiso para inscribir a empleados';
end;

function TSaldosEmpleado_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= Revisa(D_AHORRO_REG_LIQ_RET);
     if not Result then
        sMensaje:= 'No tiene permiso para dar de baja inscripciones';
end;


function TSaldosEmpleado_DevEx.PuedeAgregarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS, K_DERECHO_ALTA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para registrar préstamos',0);
end;

function TSaldosEmpleado_DevEx.PuedeModificarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_CAMBIO);
     if not Result then
        ZInformation('Préstamos','No tiene permiso para modificar préstamos',0);
end;

function TSaldosEmpleado_DevEx.PuedeBorrarPresta: Boolean;
begin
     Result:= CheckDerecho( D_EMP_NOM_PRESTAMOS, K_DERECHO_BAJA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para borrar préstamos',0);
end;



procedure TSaldosEmpleado_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  //Desactiva la posibilidad de agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := False;

  //Para que nunca muestre el filterbox inferior
  ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;

  //DevEx: Para que ponga el MinWidth ideal al titulo de las columnas. Posteriormente se aplica el BestFit.
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSaldosEmpleado_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     inherited;
     self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TSaldosEmpleado_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);

   ZGridModeTools.BorrarItemGenericoAll( AValueList );
   if ZetaDBGridDBTableView.DataController.IsGridMode then
      ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
end;

procedure TSaldosEmpleado_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
     inherited;
     ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TSaldosEmpleado_DevEx.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
 {inherited;
  DoEdit;}  //Con el codigo comentado al dar doble clic sobre el Grid sale la forma de ahorros
  {*** El Grid maneja la informacion del cdsPrestamos, por lo tanto al dar doble clic sobre el se debe abrir
       la forma de edicion de prestamos no la de ahorros ***}
       ModificaPrestamo
end;

procedure TSaldosEmpleado_DevEx._A_ImprimirExecute(Sender: TObject);
begin
  inherited;
  DoPrint;
end;

procedure TSaldosEmpleado_DevEx._E_AgregarExecute(Sender: TObject);
begin
  inherited;
  DoInsert;
end;

procedure TSaldosEmpleado_DevEx._E_BorrarExecute(Sender: TObject);
begin
  inherited;
  DoDelete;
end;

procedure TSaldosEmpleado_DevEx._E_ModificarExecute(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TSaldosEmpleado_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;
end.
