unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa principal de Tress                ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, ToolWin, Menus, Mask, DBCtrls, Db, ImgList,
     StdCtrls, ActnList, StrUtils,
     ZBaseConsulta,
     ZBaseShell,
     ZetaClientDataset,
     ZetaCommonLists,
     ZetaDBTextBox,
     ZetaDespConsulta,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaMessages,
     ZetaStateComboBox,
     ZetaTipoEntidad,
     ZBaseNavBarShell,
     dxSkinsCore,cxLookAndFeels, dxSkinsForm, dxSkinsdxBarPainter, cxGraphics, cxControls,
     cxLookAndFeelPainters, dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbon,
     dxBar, cxClasses, dxSkinsdxNavBarPainter, dxNavBar, dxNavBarCollns,
     dxNavBarBase, dxSkinsDefaultPainters,  cxLabel,
     cxBarEditItem, cxDropDownEdit, dxBarExtItems, cxLocalization,
     dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, TressMorado2013,
     cxButtons, dxGDIPlusClasses, dxImageSlider, cxContainer, cxEdit, cxImage,
     cxTextEdit, cxMaskEdit,
     ZetaCXStateComboBox,
     dxStatusBar,
     dxRibbonStatusBar,
     cxtreeview, cxStyles, System.Actions, dxBarBuiltInMenu,
  dxRibbonCustomizationForm;

type
  TTressShell = class(TBaseNavBarShell)
    _Emp_Primero: TAction;
    _Emp_Anterior: TAction;
    _Emp_Siguiente: TAction;
    _Emp_Ultimo: TAction;
    _E_BuscarEmpleado: TAction;
    _E_CambiarEmpleado: TAction;
    _V_ArbolOriginal: TAction;
    _V_Empleados: TAction;
    _V_TipoAhorro: TAction;
    _V_Sistema: TAction;
    _Adm_Inscripcion: TAction;
    _Adm_RegPrestamo: TAction;
    _Adm_RegLiquidacion: TAction;
    _Adm_RegDeposito: TAction;
    _Adm_DepositarRetenciones: TAction;
    _Adm_CalcularIntereses: TAction;
    _Adm_RepartoAnual: TAction;
    _Adm_CierreAnual: TAction;
    _A_Preferencias: TAction;
    TabArchivo_btnPreferencias: TdxBarLargeButton;
    EExportar_btnExportar: TdxBarLargeButton;
    TabCajaAhorro: TdxRibbonTab;
    PProcesos: TdxBar;
    PProcesos_btnCalcularIntereses: TdxBarLargeButton;
    PProcesos_btnCierreAnual: TdxBarLargeButton;
    PProcesos_btnRetenciones: TdxBarLargeButton;
    PProcesos_btnRepartoAnual: TdxBarLargeButton;
    RRegistro: TdxBar;
    RRegistro_btnDepositoRetiro: TdxBarLargeButton;
    RRegistro_btnInscribirEmpleado: TdxBarLargeButton;
    RRegistro_btnLiquidacion: TdxBarLargeButton;
    RRegistro_btnRegistrarPrestamo: TdxBarLargeButton;
    Image24_StatusEmpleado: TcxImageList;
    PanelFecha: TPanel;
    SistemaFechaLBL_DevEx: TLabel;
    SistemaFechaDia_DevEx: TLabel;
    SistemaFechaZF_DevEx: TZetaFecha;
    EFechaSistema: TdxBar;
    dxBarControlContainerItem_FechaSistema: TdxBarControlContainerItem;
    CajaAhorroRibbonSmall: TcxImageList;
    CajaAhorroRibbonLarge: TcxImageList;
    PanelValorAEmpleado: TPanel;
    PanelValorAEmpleado_2: TPanel;
    EmpleadoPrettyName_DevEx: TLabel;
    EmpleadoActivoPanel_DevEx: TPanel;
    Panel1: TPanel;
    Panel3: TPanel;
    PanelImg: TPanel;
    ImagenStatus_DevEx: TcxImage;
    PanelValorEmpleado_1: TPanel;
    EmpleadoLBL_DevEx: TLabel;
    btnBuscarEmp_DevEx: TcxButton;
    btnEmplPrimero_DevEx: TcxButton;
    btnEmpAnterior_DevEx: TcxButton;
    btnEmpSiguiente_DevEx: TcxButton;
    btnEmpUltimo_DevEx: TcxButton;
    PanelValorTipoAhorro: TPanel;
    EmpleadoNumeroCB_DevEx: TcxStateComboBox;
    PanelContenedor: TPanel;
    TipoAhorroCB_DevEx: TcxStateComboBox;
    AH_TIPOLbl_DevEx: TLabel;
    dxStaticFechaSistemaValor: TdxBarStatic;
    dxStaticSistemaFechaDia: TdxBarStatic;
    dxStaticFechaSistema: TdxBarStatic;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure _A_CatalogoUsuariosExecute(Sender: TObject);
    procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    procedure _E_RefrescarUpdate(Sender: TObject);
    procedure _V_ArbolOriginalExecute(Sender: TObject);
    procedure _Emp_PrimeroExecute(Sender: TObject);
    procedure _Emp_PrimeroUpdate(Sender: TObject);
    procedure _Emp_AnteriorExecute(Sender: TObject);
    procedure _Emp_SiguienteExecute(Sender: TObject);
    procedure _Emp_UltimoExecute(Sender: TObject);
    procedure _Emp_UltimoUpdate(Sender: TObject);
    procedure SistemaFechaZFValidDate(Sender: TObject);
    procedure TipoAhorroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure _Adm_RegPrestamoExecute(Sender: TObject);
    procedure _Adm_RegLiquidacionExecute(Sender: TObject);
    procedure _Adm_RegDepositoExecute(Sender: TObject);
    procedure _Adm_InscripcionExecute(Sender: TObject);
    procedure _Adm_DepositarRetencionesExecute(Sender: TObject);
    procedure _A_PreferenciasExecute(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);

  private
    { Private declarations }
    procedure CambioEmpleadoActivo;
    procedure CargaEmpleadoActivos;
    procedure CargaTipoAhorroActivos;
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    procedure RefrescaEmpleadoActivos;
    procedure RefrescaTipoAhorroActivo;
    procedure RevisaDerechos;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure AsignacionTabOrder_DevEx;
  protected
    { Protected declarations }
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; override;
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    //DevEx
    procedure CargaTraducciones; override;
    procedure MuestraDatosUsuarioActivo; override;
    //procedure WndProc(var Message: TMessage); override;
    //(@am): Se remueve para vs 2015. Por que se requirio mostrar la bienvenida unicamente en TRESS.
    //function CheckVersionDatos:Boolean; override;
  public
    { Public declarations }
    procedure LLenaOpcionesGrupos;
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
    procedure CambiaEmpleadoActivos;
    procedure CambiaSistemaActivos;
    //Agregado para DevEx
    procedure CambiaTipoAhorroActivos;

    procedure SetDataChange(const Entidades: array of TipoEntidad); override;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure SetAsistencia( const dValue: TDate );
    procedure RefrescaEmpleado;
    //DevEx
    procedure CargaVista; override;
    //procedure HabilitaBtn; override;
    //procedure CreaRibbon; override;
    //procedure DestruyeRibbon; override;
  end;

const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_CAJAAHORRO = 2;
     K_PANEL_SISTEMA = 3;
var
  TressShell: TTressShell;
  //DevEx(by am): Arreglo para guardar los status del Empleado
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');

implementation

{$R *.DFM}

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaDialogo,
     ZetaBuscaEmpleado_DevEx,
     ZetaRegistryCliente,
     ZAccesosMgr,
     ZArbolTress,
     ZBaseDlgModal_DevEx,
     ZGlobalTress,
     ZcxWizardBasico,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
{$endif}
     TressHelp,
     DCajaAhorro,
     DCatalogos,
     DTablas,
     dSistema,
     DCliente,
     DConsultas,
     DDiccionario,
     DGlobal,
     DReportes,
     FAutoClasses,
     FWizDepositarRetenciones_DevEx,
     FCalendario_DevEx,
     ZAccesosTress,
     FArbolConfigura_DevEx;

{ ************** TTressShell ****************** }

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmCajaAhorro := TdmCajaAhorro.Create( self );

     inherited;
     HelpContext                           := H00001_Pantalla_principal;
     //** ArchivoConfigurarEmpresa.HelpContext  := H60651_Globales_empresa;
     TabArchivo_btnConfigurarEmpresa.HelpContext  := H60651_Globales_empresa;
     //**EditarBuscar.HelpContext              := H00012_Busqueda_empleados;
     btnBuscarEmp_DevEx.HelpContext              := H00012_Busqueda_empleados;
     FHelpGlosario := 'TressCajadeAhorro.chm';

     ZAccesosMgr.SetValidacion( TRUE );           // AP(04/07/2007): Se modific� esta linea para que si se validen los derechos de acceso.
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     //FFormManager.Free;
     inherited;
     dmCajaAhorro.Free;
     dmDiccionario.Free;
     dmReportes.Free;                                                             
     dmTablas.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     dmCliente.Free;
     Global.Free;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     BuscarFormaBtn_DevEx.Caption:='';
     //DevEx
     CargaVista;
     //DevEx: Se movio al Source despues del evento Update para que tome en cuenta que el shell no tiene menu
     //WindowState := wsMaximized;
     //DevEx: Se carga el archivo de recursos para la traduccion de textos
     CargaTraducciones;
end;

{procedure HabilitaBtn;
begin
     inherited;
end;}

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     //DevEx(by am): Se activan o desactivan panel de valores activos para la nueva imagen
     ZetaClientTools.SetEnabled( DevEx_BandaValActivos, EmpresaAbierta );

     if not EmpresaAbierta then
     begin
          StatusBarMsg( '', K_PANEL_EMPLEADO );
          StatusBarMsg( '', K_PANEL_CAJAAHORRO );
          //lblDatosUsuario.Caption:=''; //DevEx(by am): Agregado para desplegar el nombre y grupo del usuario en sesion.
     end;
     //DevEx(by am): Habilitar botones de Caja de Ahorro solo cuando hay empresa Abierta. No se llama directamente con el ACTION pues no se puede validar la vista en este punto
     DevEx_BarManager.GetItemByName('RRegistro_btnDepositoRetiro').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('RRegistro_btnInscribirEmpleado').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('RRegistro_btnLiquidacion').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('RRegistro_btnRegistrarPrestamo').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('PProcesos_btnCalcularIntereses').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('PProcesos_btnCierreAnual').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('PProcesos_btnRetenciones').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('PProcesos_btnRepartoAnual').Enabled := EmpresaAbierta;

end;

procedure TTressShell.DoOpenAll;
const
     K_TAMANO_ARBOLITOS = 2;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;

        {AV: 2010-11-16 Asigna El Global Usar Validacion Status Activo de Tablas y Cat�logos al ZetaClienteDataSet }
        ZetaClientDataSet.GlobalSoloKeysActivos := Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS );
        ZetaClientDataSet.GlobalListaConfidencialidad := dmCliente.Confidencialidad;
        with dmCliente do
        begin
             InitActivosSistema;
             InitArrayTPeriodo;//acl
             InitActivosTipoAhorro;
             InitActivosEmpleado;
             //Definir el exe en el que se esta, para saber que arbol de usuario cargar
             dmSistema.FTipo_Exe_DevEx := Ord( taCajaAhorro );
             //Definir el tipod e vista para saber que mensajes desplegar
             //DevEx: Vista Actual
             LeeTiposAhorro( Self.TipoAhorroCB_DevEx.Lista);
             with self.TipoAhorroCB_DevEx.Properties do
                  DropDownRows := iMin( Items.Count, 12 );   // M�ximo muestra 12 sin que aparezca el Scroll

        end;
        CargaEmpleadoActivos;
        CargaTipoAhorroActivos;
        CargaSistemaActivos;
        SetArbolitosLength( K_TAMANO_ARBOLITOS );
        CreaNavBar;

        RevisaDerechos;
        //DevEx: Se valida el vencimiento para ambas vistas
        if Autorizacion.TieneVencimiento then
        begin
             SistemaFechaZF_DevEx.Enabled := FALSE;
        end;
        EmpleadoNumeroCB_DevEx.SetFocus;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
     llenaopcionesGrupos;
end;

procedure TTressShell.RevisaDerechos;
begin
     // Listar los nodos cuandos se implementen derechos de acceso
     _Adm_Inscripcion.Enabled:= Revisa(D_AHORRO_REG_INSCRIP_EMP);
     _Adm_RegPrestamo.Enabled:= CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_ALTA );
     _Adm_RegLiquidacion.Enabled:= Revisa(D_AHORRO_REG_LIQ_RET);
     _Adm_RegDeposito.Enabled:= CheckDerecho(D_AHORRO_EDO_CUENTA_BANC,K_DERECHO_ALTA);
     _Adm_DepositarRetenciones.Enabled:= Revisa(D_AHORRO_DEPOSITAR_RET);

     //DevEx: Botones movidos del arbol al Ribbon
     begin
          _A_Bitacora_DevEx.Enabled:= Revisa(D_AHORRO_BITACORA);
          _A_SQL_DevEx.Enabled := Revisa(D_AHORRO_SQL);
          {****Lista Procesos y el Explorador de Reportes tienen asociado OnUpdate para la
          vista clasica, asi que para la Actual quitamos este evento.****}

          //Se le puso el mismo pues derecho de bitacora pues asi lo tiene en ZetaDespConsulta
          _A_ListaProcesos.OnUpdate := nil;
          _A_ListaProcesos.Enabled := Revisa(D_AHORRO_BITACORA);
          _A_ExploradorReportes.OnUpdate := nil;
          _A_ExploradorReportes.Enabled := Revisa(D_REPORTES_CAJAAHORRO);
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmDiccionario.CierraEmpresa;
          dmCliente.CierraEmpresa;
          CierraDatasets( dmCajaAhorro );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmReportes );
          CierraDatasets( dmCatalogos );
          CierraDatasets( dmTablas );
          CierraDatasets( dmSistema );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean): Boolean;
begin
     Result := ( eEntidad in [enEmpleado] );
     if Result then
     begin
          case eEntidad of
          enEmpleado:
          begin
               lEncontrado := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ); //DevEx: Se agrego el if de la vista para llamar ala forma de busqueda indicada.
          end;
          end;
     end;
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          enEmpleado: Result := dmCliente.cdsEmpleadoLookUp;
          enQuerys: Result := dmCatalogos.cdsCondiciones;
          enTAhorro: Result := dmCajaAhorro.cdsTAhorro;
          enTPresta: Result := dmCajaAhorro.cdsTPresta;
          enUsuarios: Result := dmSistema.cdsUsuarios;
          enReporte:  Result := dmReportes.cdsLookupReportes;
          enTCtaMovs: Result := dmCajaAhorro.cdsTiposDeposito;
          enCtaBanco: Result := dmCajaAhorro.cdsCtasBancarias;
     else
         Result := nil;
     end;
end;

procedure TTressShell.ChangeTimerInfo;
begin
     SetTimerInfo;
end;

{ ********** Manejo de Valores Activos ************ }

procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     dmCajaAhorro.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     { Cambios a Valores Activos }
     if Dentro( enTAhorro, Entidades ) then
     begin
          RefrescaEmpleadoActivos;
     end;
     { Cambios a otros datasets }
     inherited SetDataChange( Entidades );
end;

procedure TTressShell.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        //DevEx
        lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB_DevEx.ValorEntero );
        if lOk then
           CambioEmpleadoActivo
        else
            ZetaDialogo.zInformation( 'Error', '! Empleado no encontrado !', 0 );
        finally
               Screen.Cursor := oCursor;
        end;// end try
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     //Este metodo solo se ejecuta en el DoOpenAll asi que cargamos los el combo de ambas vistas.
     with dmCliente do
     begin
          EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
     end;
     RefrescaEmpleadoActivos;
end;

procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;

procedure TTressShell.RefrescaEmpleadoActivos;
var
   oColor : TColor;
   i:Integer;
   sCaption, sHint : String;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
     with dmCliente do
     begin
          StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          //MuestraDatosUsuarioActivo; //DevEx(by am): Se refresca el usuario y el grupo en el Panel de valores activos
          with GetDatosEmpleadoActivo do
          begin
               EmpleadoPrettyName_DevEx.Caption := Nombre;
               GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);

               //DevEx (by am): Inicializando Bitmat y PNG
               ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
               APngImage := nil;
               for i := Low(StatusEmpleado) to High(StatusEmpleado) do
               begin
                    if (sCaption = StatusEmpleado[i]) then
                    begin
                         try
                            //DevEx (by am): Creando el Bitmap
                            ABitmap.Clear;
                            Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                            //DevEx (by am): Creando el PNG
                            APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                            ImagenStatus_DevEx.Picture.Graphic := APngImage;
                            ImagenStatus_DevEx.Hint := sHint;
                         finally
                                APngImage.Free;
                                ABitmap.Free;
                         end;
                         Break;
                    end;
               end;

{
               with EmpleadoActivoPanel do
               begin
                    if Activo then
                    begin
                         if ( Baja = NullDateTime ) then
                         begin
                              Caption := 'Activo';
                              Hint := 'Ingres� ' + FechaCorta( Ingreso );
                              Font.Color := clBlack;
                         end
                         else
                         begin
                              Caption := 'Reingreso';
                              Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                              Font.Color := clGreen;
                         end;
                    end
                    else
                    begin
                         Caption := 'Baja';
                         Hint := 'Baja desde ' + FechaCorta( Baja );
                         Font.Color := clRed;
                    end;
               end;
}
          end;
     end;
end;

procedure TTressShell.TipoAhorroCBLookUp(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     dmCliente.TipoAhorro := TipoAhorroCB_DevEx.Llave;
     RefrescaTipoAhorroActivo;
end;

procedure TTressShell.CargaTipoAhorroActivos;
begin
     //DevEx
     with dmCliente do
     begin
          TipoAhorroCB_DevEx.Llave := TipoAhorro;
          StatusBarMsg( GetTipoAhorroDescripcion, K_PANEL_CAJAAHORRO );
     end;
end;

procedure TTressShell.RefrescaTipoAhorroActivo;
begin
     CambioValoresActivos( stTipoAhorro );
     StatusBarMsg( dmCliente.GetTipoAhorroDescripcion, K_PANEL_CAJAAHORRO );
end;

procedure TTressShell.SistemaFechaZFValidDate(Sender: TObject);
begin
     inherited;
     dmCliente.FechaDefault := SistemaFechaZF_DevEx.Valor;
     RefrescaSistemaActivos;
     CambioValoresActivos( stSistema );
end;

procedure TTressShell.CargaSistemaActivos;
begin
     {La fecha default es asignada a ambas vistas}
     with dmCliente do
     begin
          dxStaticFechaSistemaValor.Caption := DateToStr(dmCliente.FechaDefault);
          SistemaFechaZF_DevEx.Valor := FechaDefault;
     end;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     with SistemaFechaZF_DevEx do
     begin
          SistemaFechaDia_DevEx.Caption := DiaSemana( Valor );
          { Refresca el Default del Lookup de ZetaFecha }
          //FCalendario.SetDefaultDlgFecha( Valor ); //OLD
          FCalendario_DevEx.SetDefaultDlgFecha( Valor ); //Se realizo una nueva forma de Calendario, esta es la que llama el componente ZetaFecha
          dxStaticSistemaFechaDia.Caption := DiaSemana( Valor );
     end;
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     CargaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     CargaSistemaActivos;
     CambioValoresActivos( stSistema );
end;

procedure TTressShell.CambiaTipoAhorroActivos;
begin
     CargaTipoAhorroActivos;
     CambioValoresActivos( stTipoAhorro );
end;
{ ********* Funciones de Menus *********************}


{ ************ Eventos del ActionList *********** }

procedure TTressShell._E_RefrescarUpdate(Sender: TObject);
begin
     _E_Refrescar.Enabled := HayFormaAbierta;
end;

procedure TTressShell._A_CatalogoUsuariosExecute(Sender: TObject);
begin
     inherited;
     //AbreFormaConsulta( efcSistUsuarios );
end;

procedure TTressShell._E_BuscarEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        EmpleadoNumeroCB_DevEx.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell._V_ArbolOriginalExecute(Sender: TObject);
begin
     inherited;
     if ZConfirm( 'Edici�n Del Arbol De Formas', 'Este Cambio Es S�lo Temporal' + CR_LF + '� Desea Continuar ? ', 0, mbYes ) then
     begin
          {***DevEx(@am): El metodo ZArbolTress.MuestraArbolOriginal, renombra todo el arbol de sistema TRESS en vista la vista clasica,
                          lo cual no es necesario, pues los nombres del arbol del sistema no pueden ser cambiados, los unicos que puden cambiarse
                          son los del arbol del usuario, sin embargo como es Legacy se dejo tal y como esta ese codigo.
                          Para la nueva imagens olo se cambiaran los nombres para el primer grupo, que es donde estaria ubicado el arbol del usuario,
                          en caso de contar con uno***}
          if DevEx_NavBar.Groups.Count>0 then //Renoombrara el arbol del Grupo Cero, el arbol del usuario seimpre sea el cero.
             ZArbolTress.MuestraArbolOriginal( DevEx_NavBar.Groups[0].Control.Controls[0] as TcxTreeView );
     end;
end;

procedure TTressShell._Emp_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoPrimero then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_PrimeroUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoDownEnabled;
end;

procedure TTressShell._Emp_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoAnterior then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoSiguiente then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoUltimo then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_UltimoUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoUpEnabled;
end;

procedure TTressShell._Adm_RegPrestamoExecute(Sender: TObject);
{const
K_NOMBRE_FORMA = 'SaldosEmpleado_DevEx';}
begin
     inherited;
     dmCajaAhorro.RegistrarPrestamos;
     //InvocaBestFit( K_NOMBRE_FORMA );
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmCajaAhorro.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell.GetFechaLimiteShell;
begin
//
end;

procedure TTressShell.SetBloqueo;
begin
//
end;

procedure TTressShell.SetAsistencia( const dValue: TDate );
begin
//
end;

procedure TTressShell._Adm_RegLiquidacionExecute(Sender: TObject);
begin
     inherited;
     dmCajaAhorro.LiquidarAhorro;
end;

procedure TTressShell._Adm_RegDepositoExecute(Sender: TObject);
begin
     inherited;
     dmCajaAhorro.RegistrarDepositoRetiro;
end;

procedure TTressShell._Adm_InscripcionExecute(Sender: TObject);
begin
     inherited;
     dmCajaAhorro.RegistrarInscripcion;
end;

procedure TTressShell._Adm_DepositarRetencionesExecute(Sender: TObject);
begin
     inherited;
     ZcxWizardBasico.ShowWizard(TWizDepositarRetenciones_DevEx);
end;

procedure TTressShell.RefrescaEmpleado;
begin
end;

{***Ribbon***}
{procedure TTressShell.CreaRibbon;
var
    RibbonTab: TdxRibbonTab;
    IRibbonTabs, IdxBars: Integer;
begin
     inherited;
     if ( Ribbon <> nil) then
     begin
          Ribbon.BeginUpdate;
          for IRibbonTabs := Low(RibbonTabItemsCaption) to High(RibbonTabItemsCaption) do
          begin
               //Inicializa variable RibbonTab con los tabs creados en la clase padre
               RibbonTab := Ribbon.Tabs.Find(RibbonTabItemsCaption[IRibbonTabs]);
               //Recorrer el BarManager para buscar los dxBar correspondientes al Tab
               for IdxBars:=0 to BarManager.Bars.Count-1 do
               begin
                    //Si dxBar pertenece a RibbonTab
                    if(AnsiContainsStr( BarManager.Bars[IdxBars].Name, RibbonTab.Name )) then
                    begin
                         //Se crean los grupos de cada Tab
                         with RibbonTab.Groups.Add do
                         begin
                              Toolbar := BarManager.Bars[IdxBars];
                              Toolbar.Visible := True;
                         end;
                    end;
               end;
          end;
          Ribbon.EndUpdate;
     end;
end;
procedure TTressShell.DestruyeRibbon;
var
   IdxBars: Integer;
begin

     if ( Ribbon <> nil) then
     begin
          with BarManager do
          begin
               for IdxBars:=0 to Bars.Count-1 do
               begin
                    Bars[IdxBars].Visible := False;
               end;
          end;
     end;
     inherited;
end;}
{ Estilos }
{procedure TTressShell.WndProc(var Message: TMessage);
begin
  inherited;
  case Message.Msg of
    WM_EXITMENULOOP:
      SetMenu(Handle, 0);
  end;
end;}
//Carga los textos a traducir
procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;

procedure TTressShell.AsignacionTabOrder_DevEx;
begin
     DevEx_ShellRibbon.TabOrder:= 1;
     PanelNavBar.TabOrder := 2;
     DevEx_BandaValActivos.TabOrder :=3;
     PanelConsulta.TabOrder :=4;
end;


procedure TTressShell.CargaVista;
begin
     inherited;
     //Mostar Banda Valores activos
     DevEx_BandaValActivos.Visible := True;
     //Asignacion de TabOrders para el shell
     AsignacionTabOrder_DevEx;
     //Para avisar a los controles que hubo movimientos
     Application.ProcessMessages;
end;


//DevEx(by am): Metodo agregado para mostrar la informacion del usuario en el Panel de valores activos aun cuando no se elija una empresa.
procedure TTressShell.MuestraDatosUsuarioActivo;
begin
     //lblDatosUsuario.Caption := dmCliente.GetDatosUsuarioActivo.NombreCorto +'|'+ dmCliente.GetDatosUsuarioActivo.GrupoDescrip;
end;

procedure TTressShell._A_PreferenciasExecute(Sender: TObject);
begin
  inherited;
     // Ser�a mejor llamar a 'cdsArbol.Modificar'
     // y que ese m�todo se encargue de lo dem�s.
     dmSistema.PosicionaUsuarioActivo;
     ZBaseDlgModal_DevEx.ShowDlgModal( ArbolConfigura_DevEx, TArbolConfigura_DevEx );

end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
  inherited;
  _V_Cerrar.Execute;
end;

Procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta) then
  begin
        for I:=0 to DevEx_NavBar.Groups.Count-1 do
        begin
              if (DevEx_NavBar.Groups[I].Caption = 'Administraci�n')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=1;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=1;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Cat�logos')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=2;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=2;
              end;
              if (I = 0) then
              begin
                    if(DevEx_NavBar.Groups.Count = 1) then
                    begin
                          // @DChavez : Cuando no tiene derecho se asigna los iconos que corresponden al navbar por el index fijo en la lista
                          if( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos' )   then
                          begin
                               DevEx_NavBar.Groups[0].LargeImageIndex := 3;
                               DevEx_NavBar.Groups[0].SmallImageIndex := 3;
                          end
                          else
                          begin
                               DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                               DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                          end;
                    end
                    else
                    begin
                          if (DevEx_NavBar.Groups[1].Caption = 'Administraci�n')  then
                          begin
                                DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                                DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                          end;
                    end;
              end;
              DevEx_NavBar.Groups[I].UseSmallImages:=false;
        end;
  end
  else
  begin
        if(DevEx_NavBar.Groups.Count >0) then
        begin
              DevEx_NavBar.Groups[0].UseSmallImages:=false;
              DevEx_NavBar.Groups[0].LargeImageIndex:=9;
              DevEx_NavBar.Groups[0].SmallImageIndex:=9;
        end;
  end;
end;

//DevEx(@am)
 //(@am): Se remueve para vs 2015. Por que se requirio mostrar la bienvenida unicamente en TRESS.
 {
function TTressShell.CheckVersionDatos:Boolean;
begin
     //DevEx (@am): Se guarda el resultado de CheckVersion para utlizarlo despues en la validacion para mostrar la presentacion de cambios en version.
     with dmCliente do
          EsVersionActualBD := CheckVersion;
     Result := TRUE; //Conecta a la base de datos sin importar la version. Esta solo puede ser actualizada desde TRESS.
end;  }


end.




