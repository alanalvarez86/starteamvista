unit FRegistroPrestamo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, StdCtrls, DBCtrls, Mask, ZetaFecha, ZetaEdit,
  ZetaKeyLookup, DB, ExtCtrls, ZetaSmartLists, Buttons, ZetaNumero,
  ZetaDBTextBox, ZetaKeyCombo;

type
  TRegistroPrestamo = class(TBaseEdicion)
    AH_TIPOLbl: TLabel;
    Label8: TLabel;
    PR_REFEREN: TZetaDBEdit;
    Label1: TLabel;
    PR_FECHA: TZetaDBFecha;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup;
    GBCondiciones: TGroupBox;
    LBL_Formula: TLabel;
    PR_FORMULA: TDBMemo;
    SBCO_FORMULA: TSpeedButton;
    Label7: TLabel;
    PR_MONTO_S: TZetaDBNumero;
    Label3: TLabel;
    PR_TASA: TZetaDBNumero;
    Label4: TLabel;
    PR_MESES: TZetaDBNumero;
    Label5: TLabel;
    PR_MONTO: TZetaDBNumero;
    LBL_Cuantos_pagos: TLabel;
    PR_PAGOS: TZetaDBNumero;
    LBL_Cada_Pago: TLabel;
    PR_PAG_PER: TZetaDBNumero;
    Label10: TLabel;
    PR_SALDO_I: TZetaDBNumero;
    GBCheque: TGroupBox;
    RegistrarCheque: TCheckBox;
    Label11: TLabel;
    CT_CODIGO: TZetaKeyLookup;
    Label12: TLabel;
    CM_CHEQUE: TZetaNumero;
    BtnSiguiente: TSpeedButton;
    Label13: TLabel;
    CM_DESCRIP: TEdit;
    ImprimeCheque: TCheckBox;
    Label14: TLabel;
    PR_INTERES: TZetaDBNumero;
    Label15: TLabel;
    CM_PRESTA: TZetaKeyCombo;
    TIPO_PRESTAMO: TZetaTextBox;
    PR_TIPO: TZetaDBTextBox;
    gbNomina: TGroupBox;
    PeriodoTipoLbl: TLabel;
    TipoNomina: TZetaTextBox;
    PeriodoNumeroLBL: TLabel;
    NumeroNomina: TZetaTextBox;
    PeriodoDelLBL: TLabel;
    PeriodoFechaInicial: TZetaTextBox;
    PeriodoAlLBL: TLabel;
    PeriodoFechaFinal: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure BtnSiguienteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RegistrarChequeClick(Sender: TObject);
    procedure CT_CODIGOValidKey(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure SBCO_FORMULAClick(Sender: TObject);
    procedure CM_PRESTAChange(Sender: TObject);
  private
    procedure InitRegistrarCheque;
    procedure LlenaDatosPeriodo;
    { Private declarations }
  protected
    { Protected declarations }
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  RegistroPrestamo: TRegistroPrestamo;

implementation

uses dCajaAhorro, dCliente,
     ZEtaDialogo,
     ZetaTipoEntidad,
     ZImprimeForma,
     ZConstruyeFormula,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaClientDataSet;

{$R *.dfm}

procedure TRegistroPrestamo.FormCreate(Sender: TObject);
const
     K_FILTRO_CTAS = '( AH_TIPO = %s and CT_STATUS = 0 )'; //S�lo cuentas activas
begin
     inherited;
     TipoValorActivo1 := stTipoAhorro;
     IndexDerechos := 0; // PENDIENTE
     HelpContext := H_REG_PRESTAMO;
     FirstControl := CB_CODIGO;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     CM_PRESTA.ItemIndex := 0;
     with CT_CODIGO do
     begin
          LookupDataset := dmCajaAhorro.cdsCtasBancarias;
          Filtro := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
     end;
end;

procedure TRegistroPrestamo.FormShow(Sender: TObject);
begin
     inherited;
     InitRegistrarCheque;
     LlenaDatosPeriodo;
     with dmCajaAhorro do
     begin
          //cdsRegPrestamos.FieldByName('PR_TIPO').AsString := cdsTPresta.FieldByName('TB_CODIGO').AsString ;
          TIPO_PRESTAMO.Caption := cdsTPresta.FieldByName('TB_ELEMENT').AsString;
     end;
end;

procedure TRegistroPrestamo.Connect;
begin
     CB_CODIGO.ResetMemory;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCajaAhorro do
     begin
          cdsCtasBancarias.Conectar;
          Datasource.Dataset := cdsRegPrestamos;    // Ya debe estar agregado el registro al llegar a la forma
     end;
end;

procedure TRegistroPrestamo.EscribirCambios;
begin
     if RegistrarCheque.Checked then
     begin
          if ( CM_CHEQUE.ValorEntero > 0 ) then
          begin
               if not dmCajaAhorro.ValidarCheque( CT_CODIGO.Llave, CM_CHEQUE.ValorEntero ) then
               begin
                    with dmCajaAhorro do
                    begin
                         with cdsRegCtasMovs do
                         begin
                              PostData;
                              if ( not IsEmpty ) then
                              EmptyDataSet;  // Se asegura que est� vacio aunque se invoque varias veces el Escribircambios
                              Append;
                              FieldByName( 'CT_CODIGO' ).AsString := CT_CODIGO.Llave;
                              FieldByName( 'CM_TIPO' ).AsString := 'PRESTA';
                              FieldByName( 'CM_DEP_RET' ).AsString := 'R';
                              FieldByName( 'CM_DESCRIP' ).AsString := CM_DESCRIP.Text;
                              FieldByName( 'CM_CHEQUE' ).AsInteger := CM_CHEQUE.ValorEntero;
                              FieldByName( 'CM_STATUS' ).AsInteger := Ord( saActivo );
                              FieldByName( 'CM_PRESTA' ).AsInteger := CM_PRESTA.ItemIndex;
                              Post;  // Los datos que toma de otros clientdataset se asignan en BeforePost
                         end;
                         RegImprimirCheque := self.ImprimeCheque.Checked;
                    end;
                    inherited EscribirCambios;
               end
               else
                   ZetaDialogo.ZError( 'Prestamos', 'N�mero de cheque repetido. Es necesario cambiarlo.', 0 );
          end
          else
          ZetaDialogo.ZError( 'Prestamos', 'Capturar n�mero de cheque', 0 );
     end
     else
     inherited EscribirCambios;
end;

procedure TRegistroPrestamo.LlenaDatosPeriodo;
begin
     with dmCliente.GetDatosPeriodoActivo do
     begin
          Self.TipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
          Self.NumeroNomina.Caption := IntToStr( Numero );
          Self.PeriodoFechaInicial.Caption := FechaCorta( Inicio );
          Self.PeriodoFechaFinal.Caption := FechaCorta( Fin );
     end;
end;

procedure TRegistroPrestamo.Agregar;
begin
     { No hace nada }
end;

procedure TRegistroPrestamo.Borrar;
begin
     { No hace nada }
end;

procedure TRegistroPrestamo.InitRegistrarCheque;
var
   lPuedeRegistrarCheque, lPuedeImprimir: Boolean;
   sCtaBancaria : String;
begin
     sCtaBancaria := VACIO;
     lPuedeImprimir := FALSE;
     with dmCajaAhorro.cdsCtasBancarias do
     begin
          Filter := CT_CODIGO.Filtro;    // Mismo filtro del control lookup
          Filtered := TRUE;
          try
             lPuedeRegistrarCheque := ( not IsEmpty );
             if lPuedeRegistrarCheque then
             begin
                  First;
                  sCtaBancaria := FieldByName( 'CT_CODIGO' ).AsString;
                  lPuedeImprimir := ( FieldByName( 'CT_REP_CHK' ).AsInteger > 0 );
             end;
          finally
                 Filtered := FALSE;
                 Filter := VACIO;
          end;
     end;
     with RegistrarCheque do
     begin
          Enabled := lPuedeRegistrarCheque;
          Checked := lPuedeRegistrarCheque;
     end;
     CT_CODIGO.Llave := sCtaBancaria;
     CM_CHEQUE.Valor := 0;
     CM_DESCRIP.Text := VACIO;
     ImprimeCheque.Checked := lPuedeImprimir;
end;

{ Eventos de controles }

procedure TRegistroPrestamo.RegistrarChequeClick(Sender: TObject);
begin
     inherited;
     ZetaClientTools.SetEnabledControl( GBCheque, RegistrarCheque.Checked );
end;

procedure TRegistroPrestamo.BtnSiguienteClick(Sender: TObject);
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
        CM_CHEQUE.Valor := dmCajaAhorro.SiguienteCheque( CT_CODIGO.Llave );
end;

procedure TRegistroPrestamo.CT_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
     begin
          with dmCajaAhorro.cdsCtasBancarias do
          begin
               Locate( 'CT_CODIGO', CT_CODIGO.Llave, [] );
               self.ImprimeCheque.Checked := ( FieldByName( 'CT_REP_CHK' ).AsInteger > 0 );
          end;
     end;
end;

procedure TRegistroPrestamo.SBCO_FORMULAClick(Sender: TObject);
begin
     inherited;
     with dmCajaAhorro.cdsRegPrestamos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'PR_FORMULA').AsString := GetFormulaConst( enNomina, PR_FORMULA.Lines.Text, PR_FORMULA.SelStart, evBase );
     end;
end;

procedure TRegistroPrestamo.OKClick(Sender: TObject);
begin
     inherited;
     if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
end;

procedure TRegistroPrestamo.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enPrestamo, dmCajaAhorro.cdsRegPrestamos );
end;

function TRegistroPrestamo.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TRegistroPrestamo.CM_PRESTAChange(Sender: TObject);
var lPago:Boolean;
begin
     inherited;
     lPago := CM_PRESTA.ItemIndex <> Ord( tdpDiferidos );
     LBL_Cuantos_pagos.Enabled := lPago;
     LBL_Cada_Pago.Enabled := lPago;
     LBL_Formula.Enabled := lPago;
     PR_PAGOS.Enabled := lPago;
     if not lPago then
        dmCajaAhorro.cdsRegPrestamos.FieldByName('PR_PAGOS').AsInteger := 0;
     PR_PAG_PER.Enabled := lPago;
     PR_FORMULA.Enabled := lPago;
     SBCO_FORMULA.Enabled := lPago;
end;

end.
