program TressCajaAhorro;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}


uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {TBaseConsulta},
  DDiccionario in 'DDiccionario.pas' {dmDiccionario: TDataModule},
  DGlobal in '..\DataModules\DGlobal.pas',
  DReportes in '..\DataModules\DReportes.pas' {dmReportes: TDataModule},
  ZBaseDlgModal_DevEx in '..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  ZBaseEdicion_DevEx in '..\Tools\ZBaseEdicion_DevEx.pas' {BaseEdicion_DevEx},
  ZBaseGlobal_DevEx in '..\Tools\ZBaseGlobal_DevEx.pas' {BaseGlobal_DevEx},
  ZBaseEscogeGrid_DevEx in '..\Tools\ZBaseEscogeGrid_DevEx.pas' {BaseEscogeGrid_DevEx},
  ZBaseGridEdicion_DevEx in '..\Tools\ZBaseGridEdicion_DevEx.pas' {BaseGridEdicion_DevEx},
  ZBaseSelectGrid_DevEx in '..\Tools\ZBaseSelectGrid_DevEx.pas' {BaseGridSelect_DevEx},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZetaWizardFeedBack_DevEx in '..\Tools\ZetaWizardFeedBack_DevEx.pas' {WizardFeedback_DevEx},
  ZetaBuscaEntero_DevEx in '..\Tools\ZetaBuscaEntero_DevEx.pas' {BuscaEntero_DevEx},
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  FBaseReportes_DevEx in '..\Reportes\FBaseReportes_DevEx.pas' {BaseReportes_DevEx},
  FEditBaseReportes_DevEx in '..\Reportes\FEditBaseReportes_DevEx.pas' {EditBaseReportes_DevEx},
  FEditReportes_DevEx in '..\Reportes\FEditReportes_DevEx.pas' {EditReportes_DevEx},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DCajaAhorro in 'DCajaAhorro.pas' {dmCajaAhorro: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  FRegistroDepositoRetiro_DevEx in 'FRegistroDepositoRetiro_DevEx.pas' {RegistroDepositoRetiro_DevEx},
  FRegistroInscripcion_DevEx in 'FRegistroInscripcion_DevEx.pas' {RegistroInscripcion_DevEx},
  FEditHisAhorros_DevEx in '..\Empleados\FEditHisAhorros_DevEx.pas' {EditHisAhorros_DevEx},
  FEditHisPrestamos_DevEx in '..\Empleados\FEditHisPrestamos_DevEx.pas' {EditHisPrestamos_DevEx},
  ZBaseEdicionRenglon_DevEx in '..\Tools\ZBaseEdicionRenglon_DevEx.pas' {BaseEdicionRenglon_DevEx},
  DSistema in '..\DataModules\dSistema.pas' {dmSistema: TDataModule},
  DTablas in '..\DataModules\dTablas.pas' {dmTablas: TDataModule},
  DCatalogos in 'DCatalogos.pas' {dmCatalogos: TDataModule},
  ZNavBarTools in '..\Tools\ZNavBarTools.pas',
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  TressMorado2013 in '..\Skin\TressMorado2013.pas',
  ZBasicoNavBarShell in '..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  ZBaseNavBarShell in '..\Tools\ZBaseNavBarShell.pas' {BaseNavBarShell};

{$R *.RES}
{$R WindowsXP.res}

{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'TRESS Caja de Ahorro';
  Application.HelpFile := 'TressCajadeAhorro.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       //Inicializamos la varibale de maximizacion del Shell
       if Login( False ) then
       begin
            Show;
            Update;
            WindowState := wsMaximized;  //Maximizar despues del update para que tome en cuenta que se escondio el Menu
            BeforeRun;
            Application.Run;
       end;
       Free;
  end;
end.
