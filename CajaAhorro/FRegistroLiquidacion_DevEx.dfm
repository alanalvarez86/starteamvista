inherited RegistroLiquidacion_DevEx: TRegistroLiquidacion_DevEx
  Left = 427
  Top = 72
  Caption = 'Liquidaci'#243'n / retiro de ahorro'
  ClientHeight = 612
  ClientWidth = 488
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 63
    Top = 85
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de inicio:'
  end
  object EmpleadoLbl: TLabel [1]
    Left = 90
    Top = 60
    Width = 50
    Height = 13
    Caption = 'Empleado:'
    FocusControl = ZEmpleado
  end
  object Label8: TLabel [2]
    Left = 64
    Top = 109
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de retiro:'
  end
  object Label15: TLabel [3]
    Left = 62
    Top = 132
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tasa de inter'#233's:'
  end
  object Label16: TLabel [4]
    Left = 92
    Top = 155
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Duraci'#243'n:'
  end
  object Label2: TLabel [5]
    Left = 35
    Top = 420
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo final disponible:'
  end
  object Label3: TLabel [6]
    Left = 260
    Top = 420
    Width = 134
    Height = 13
    Caption = '(Posterior al monto retirado)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object AH_FECHA: TZetaDBTextBox [7]
    Left = 142
    Top = 82
    Width = 115
    Height = 17
    AutoSize = False
    Caption = 'AH_FECHA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'AH_FECHA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Disponible_Pos: TZetaTextBox [8]
    Left = 142
    Top = 418
    Width = 115
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0.00'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  inherited PanelBotones: TPanel
    Top = 576
    Width = 488
    TabOrder = 9
  end
  inherited PanelIdentifica: TPanel
    Width = 488
    TabOrder = 7
    inherited ValorActivo2: TPanel
      Width = 162
      inherited textoValorActivo2: TLabel
        Width = 156
      end
    end
  end
  object ZEmpleado: TZetaKeyLookup_DevEx [11]
    Left = 142
    Top = 56
    Width = 333
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 80
    OnValidKey = ZEmpleadoValidKey
  end
  object GBCheque: TGroupBox [12]
    Left = 8
    Top = 440
    Width = 473
    Height = 132
    Caption = ' Cheque '
    TabOrder = 13
    object Label11: TLabel
      Left = 49
      Top = 21
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cuenta bancaria:'
    end
    object Label12: TLabel
      Left = 80
      Top = 43
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheque #:'
    end
    object Label13: TLabel
      Left = 71
      Top = 69
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object CT_CODIGO: TZetaDBKeyLookup_DevEx
      Left = 134
      Top = 13
      Width = 333
      Height = 21
      LookupDataset = dmCajaAhorro.cdsCtasBancarias
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = CT_CODIGOValidKey
      DataField = 'CT_CODIGO'
      DataSource = dsCuentas
    end
    object CM_CHEQUE: TZetaDBNumero
      Left = 134
      Top = 41
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
      DataField = 'CM_CHEQUE'
      DataSource = dsCuentas
    end
    object CM_DESCRIP: TDBEdit
      Left = 134
      Top = 68
      Width = 331
      Height = 21
      DataField = 'CM_DESCRIP'
      DataSource = dsCuentas
      TabOrder = 2
    end
    object ImprimeCheque: TCheckBox
      Left = 51
      Top = 90
      Width = 96
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Imprimir cheque:'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object ImprimeLiquidacion: TCheckBox
      Left = 5
      Top = 107
      Width = 142
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Imprimir recibo liquidaci'#243'n:'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object BtnSiguiente_DevEx: TcxButton
      Left = 252
      Top = 41
      Width = 21
      Height = 21
      Hint = 'Sugiere siguiente n'#250'mero de cheque'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = BtnSiguienteClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFBAB4BBFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA69F
        A7FFF6F5F6FFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFFACA5ACFFFAF9FAFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFFFFFFC5C0
        C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D95
        9EFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFBFBFBFFFFFFFFFFFFFFFFFFFFFF
        FFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA69F
        A7FFFAF9FAFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFA199A2FFF4F3F4FFFFFFFFFFC5C0C6FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D95
        9EFFEFEDEFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF968D97FFB3ACB4FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
    end
  end
  object CM_FECHA: TZetaDBFecha [13]
    Left = 142
    Top = 104
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 1
    Text = '17-Dec-97'
    Valor = 35781.000000000000000000
    DataField = 'CM_FECHA'
    DataSource = dsCuentas
  end
  object Tasa: TZetaDBNumero [14]
    Left = 142
    Top = 128
    Width = 115
    Height = 21
    Mascara = mnTasa
    TabOrder = 2
    Text = '0.0 %'
    DataField = 'TASA'
    DataSource = DataSource
  end
  object Duracion: TZetaDBNumero [15]
    Left = 142
    Top = 151
    Width = 115
    Height = 21
    Mascara = mnPesos
    TabOrder = 3
    Text = '0.00'
    DataField = 'DURACION'
    DataSource = DataSource
  end
  object gbCondiciones: TGroupBox [16]
    Left = 8
    Top = 177
    Width = 473
    Height = 152
    Caption = ' Condiciones del ahorro  '
    TabOrder = 4
    object SpeedButton1: TSpeedButton
      Left = 424
      Top = 179
      Width = 25
      Height = 25
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
    object Label18: TLabel
      Left = 58
      Top = 29
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total ahorrado:'
    end
    object Label19: TLabel
      Left = 25
      Top = 49
      Width = 105
      Height = 13
      Alignment = taRightJustify
      Caption = '(+) Intereses ganados:'
    end
    object Label20: TLabel
      Left = 78
      Top = 69
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = '(=) A favor:'
    end
    object Label21: TLabel
      Left = 35
      Top = 109
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = '(=) Saldo disponible:'
    end
    object Label25: TLabel
      Left = 37
      Top = 89
      Width = 93
      Height = 13
      Alignment = taRightJustify
      Caption = '(-) Saldo pr'#233'stamos:'
    end
    object CM_TOT_AHO: TZetaDBTextBox
      Left = 134
      Top = 27
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_TOT_AHO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_TOT_AHO'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_INTERES: TZetaDBTextBox
      Left = 134
      Top = 47
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_INTERES'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_INTERES'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_AFAVOR: TZetaDBTextBox
      Left = 134
      Top = 67
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_AFAVOR'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_AFAVOR'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_SAL_PRE: TZetaDBTextBox
      Left = 134
      Top = 87
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_SAL_PRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_SAL_PRE'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_DISPONIBLE: TZetaDBTextBox
      Left = 134
      Top = 107
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_DISPONIBLE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_DISPONIBLE'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_TOT_AHO_EMP: TZetaDBTextBox
      Left = 262
      Top = 27
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_TOT_AHO_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_TOT_AHO_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_INTERES_EMP: TZetaDBTextBox
      Left = 262
      Top = 47
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_INTERES_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_INTERES_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_AFAVOR_EMP: TZetaDBTextBox
      Left = 262
      Top = 67
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_AFAVOR_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_AFAVOR_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_DISPONIBLE_EMP: TZetaDBTextBox
      Left = 262
      Top = 107
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_DISPONIBLE_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_DISPONIBLE_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbEmpleado: TLabel
      Left = 168
      Top = 9
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado'
    end
    object lbEmpresa: TLabel
      Left = 299
      Top = 9
      Width = 41
      Height = 13
      Alignment = taCenter
      Caption = 'Empresa'
    end
    object CM_TOTAL_DISPONIBLE: TZetaDBTextBox
      Left = 262
      Top = 127
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_TOTAL_DISPONIBLE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_TOTAL_DISPONIBLE'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbSaldo: TLabel
      Left = 146
      Top = 129
      Width = 103
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo total disponible:'
    end
  end
  object gbTipoRetiro: TGroupBox [17]
    Left = 8
    Top = 337
    Width = 473
    Height = 73
    Caption = 
      ' Tipo de retiro:                                                ' +
      ' '
    TabOrder = 5
    object lbMonto: TLabel
      Left = 58
      Top = 47
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto a retirar:'
    end
    object CM_MONTO: TZetaDBNumero
      Left = 134
      Top = 43
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 3
      Text = '0.00'
      OnExit = CM_MONTOExit
      DataField = 'CM_MONTO'
      DataSource = dsCuentas
    end
    object DarBaja: TCheckBox
      Left = 35
      Top = 24
      Width = 112
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Dar baja de ahorro:'
      TabOrder = 2
    end
    object RetiroLiquidacion: TRadioButton
      Left = 148
      Top = -1
      Width = 80
      Height = 17
      Caption = 'Liquidaci'#243'n'
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = RetiroLiquidacionClick
    end
    object RetiroParcial: TRadioButton
      Left = 84
      Top = -1
      Width = 56
      Height = 17
      Caption = 'Parcial'
      TabOrder = 0
      OnClick = RetiroParcialClick
    end
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  object dsCuentas: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 44
    Top = 57
  end
end
