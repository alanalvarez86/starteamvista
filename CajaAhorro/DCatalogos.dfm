object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 432
  Top = 315
  Height = 268
  Width = 509
  object cdsCondiciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    OnGetRights = cdsCondicionesGetRights
    Left = 300
    Top = 12
  end
  object cdsPuestos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsPuestosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    LookupActivoField = 'PU_ACTIVO'
    LookupConfidenField = 'PU_NIVEL0'
    OnGetRights = cdsPuestosGetRights
    Left = 16
    Top = 16
  end
  object cdsTurnos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TU_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsTurnosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Turnos'
    LookupDescriptionField = 'TU_DESCRIP'
    LookupKeyField = 'TU_CODIGO'
    LookupActivoField = 'TU_ACTIVO'
    LookupConfidenField = 'TU_NIVEL0'
    OnGetRights = cdsTurnosGetRights
    Left = 88
    Top = 16
  end
  object cdsClasifi: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsClasifiAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Clasificaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    LookupConfidenField = 'TB_NIVEL0'
    OnGetRights = cdsClasifiGetRights
    Left = 152
    Top = 16
  end
  object cdsHorarios: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'HO_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsHorariosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Horarios'
    LookupDescriptionField = 'HO_DESCRIP'
    LookupKeyField = 'HO_CODIGO'
    LookupActivoField = 'HO_ACTIVO'
    OnGetRights = cdsHorariosGetRights
    Left = 224
    Top = 16
  end
  object cdsConceptos: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsConceptosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    OnGetRights = cdsConceptosGetRights
    Left = 16
    Top = 72
  end
  object cdsNomParamLookUp: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    AlAdquirirDatos = cdsNomParamLookUpAlAdquirirDatos
    LookupName = 'Par'#225'metros de N'#243'mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    LookupActivoField = 'NP_ACTIVO'
    OnGetRights = cdsNomParamGetRights
    Left = 36
    Top = 128
  end
  object cdsConceptosLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsConceptosLookupAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    OnGetRights = cdsConceptosGetRights
    Left = 144
    Top = 144
  end
  object cdsNomParam: TZetaLookupDataSet
    Tag = 21
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    AlAdquirirDatos = cdsNomParamAlAdquirirDatos
    UsaCache = True
    LookupName = 'Par'#225'metros de N'#243'mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    LookupActivoField = 'NP_ACTIVO'
    OnGetRights = cdsNomParamGetRights
    Left = 104
    Top = 80
  end
end
