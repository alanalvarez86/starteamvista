unit FCtasBancarias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TCtasBancarias = class(TBaseConsulta)
    GridCuentas: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
  end;

var
  CtasBancarias: TCtasBancarias;

implementation

uses dCajaAhorro,
     ZetaCommonClasses,
     ZetaBuscador,
     ZAccesosTress;

{$R *.dfm}

{ TCtasBancarias }

procedure TCtasBancarias.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CAT_CTAS_BANCARIAS;  // PENDIENTE
     IndexDerechos:=  D_AHORRO_CAT_CUENTAS_BANC;
end;

procedure TCtasBancarias.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsCtasBancarias.Conectar;
          DataSource.DataSet:= cdsCtasBancarias;
     end;
end;

procedure TCtasBancarias.Refresh;
begin
     dmCajaAhorro.cdsCtasBancarias.Refrescar;
end;

procedure TCtasBancarias.Agregar;
begin
     dmCajaAhorro.cdsCtasBancarias.Agregar;
end;

procedure TCtasBancarias.Modificar;
begin
     dmCajaAhorro.cdsCtasBancarias.Modificar;
end;

procedure TCtasBancarias.Borrar;
begin
     dmCajaAhorro.cdsCtasBancarias.Borrar;
end;

procedure TCtasBancarias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Cuentas bancarias', 'Cuentas bancarias', 'CT_CODIGO', dmCajaAhorro.cdsCtasBancarias );
end;

end.
