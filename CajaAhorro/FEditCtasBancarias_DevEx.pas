unit FEditCtasBancarias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls,
  StdCtrls, ZetaNumero, ZetaKeyLookup_DevEx, Mask, ZetaEdit, ZetaKeyCombo,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,  cxButtons,
  dxSkinsdxBarPainter, ImgList, dxBar, cxClasses,
  dxBarExtItems, cxImage, cxBarEditItem, cxControls, cxNavigator,
  cxDBNavigator;

type
  TEditCtasBancarias_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    CT_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    CT_NOMBRE: TDBEdit;
    Label3: TLabel;
    CT_NUM_CTA: TDBEdit;
    CT_BANCO: TDBEdit;
    Label4: TLabel;
    AH_TIPOLbl: TLabel;
    AH_TIPO: TZetaDBKeyLookup_DevEx;
    GBFormatos: TGroupBox;
    FormatoLBL: TLabel;
    Label5: TLabel;
    GBAdicionales: TGroupBox;
    Label6: TLabel;
    CT_NUMERO: TZetaDBNumero;
    Label7: TLabel;
    CT_TEXTO: TDBEdit;
    Label8: TLabel;
    CT_STATUS: TZetaDBKeyCombo;
    CT_REP_CHK: TZetaDBKeyLookup_DevEx;
    CT_REP_LIQ: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCtasBancarias_DevEx: TEditCtasBancarias_DevEx;

implementation

uses dCajaAhorro,
     dReportes,
     ZetaBuscador_DevEx,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.dfm}

procedure TEditCtasBancarias_DevEx.FormCreate(Sender: TObject);
const
     K_FILTRO_REPORTES_CTA = '( RE_ENTIDAD = %d ) and ( ( RE_TIPO = %d ) or ( RE_TIPO = %d ) or ( RE_TIPO = %d ) )';
var
   sFiltroCta: String;
begin
     inherited;
     IndexDerechos := D_AHORRO_CAT_CUENTAS_BANC; 
     HelpContext := H_REG_CTAS_BANCARIAS;
     FirstControl := CT_CODIGO;
     AH_TIPO.LookupDataset := dmCajaAhorro.cdsTAhorro;
     sFiltroCta := Format( K_FILTRO_REPORTES_CTA, [ Ord( enCtaMovs ), Ord( trListado ), Ord( trForma ), Ord( trImpresionRapida ) ] );
     with CT_REP_CHK do
     begin
          LookupDataset := dmReportes.cdsLookupReportes;
          Filtro := sFiltroCta;
     end;
     with CT_REP_LIQ do
     begin
          LookupDataset := dmReportes.cdsLookupReportes;
          Filtro := sFiltroCta;
     end;
     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditCtasBancarias_DevEx.Connect;
begin
     dmReportes.cdsLookupReportes.Conectar;
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsCtasBancarias.Conectar;
          Datasource.Dataset := cdsCtasBancarias;
     end;
end;

procedure TEditCtasBancarias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Cuentas bancarias', 'Cuentas bancarias', 'CT_CODIGO', dmCajaAhorro.cdsTAhorro );
end;


procedure TEditCtasBancarias_DevEx.SetEditarSoloActivos;
begin
     AH_TIPO.EditarSoloActivos := TRUE;
end;


end.
