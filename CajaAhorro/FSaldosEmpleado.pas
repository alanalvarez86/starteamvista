unit FSaldosEmpleado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, ZBaseEdicionRenglon, Grids,
  DBGrids, ZetaDBGrid, ComCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls,
  Mask, ZetaNumero, ZetaKeyCombo, ZetaFecha, ZetaDBTextBox, ZetaMessages;

type
  TSaldosEmpleado = class(TBaseConsulta)
    Panel1: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    Label25: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    AH_SALDO: TZetaDBTextBox;
    PR_SALDO: TZetaDBTextBox;
    AH_NETO: TZetaDBTextBox;
    AH_FECHA: TZetaDBTextBox;
    AH_STATUS: TZetaDBTextBox;
    Panel2: TPanel;
    Panel3: TPanel;
    BBAgregar: TBitBtn;
    BBModificar: TBitBtn;
    GridRenglones: TZetaDBGrid;
    dsRenglon: TDataSource;
    BBBorrar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);

  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function PuedeModificarPresta: Boolean;
    function PuedeBorrarPresta: Boolean;
    function PuedeAgregarPresta: Boolean;
    procedure ModificaPrestamo;

  protected
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  end;

var
  SaldosEmpleado: TSaldosEmpleado;

implementation

uses
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaDialogo,
    DCajaAhorro,
    ZAccesosTress,
    ZAccesosMgr;


{$R *.dfm}

procedure TSaldosEmpleado.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= H_SALDOS_EMPLEADO;
     IndexDerechos := D_AHORRO_SALDOS_X_EMP;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stTipoAhorro;
end;

procedure TSaldosEmpleado.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsHisAhorros.Refrescar;
          Datasource.Dataset := cdsHisAhorros;
          dsRenglon.DataSet := cdsHisPrestamos;
     end;
end;

procedure TSaldosEmpleado.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Agregar;
end;

procedure TSaldosEmpleado.Borrar;
begin
     dmCajaAhorro.BorrarIncripcion;
end;

procedure TSaldosEmpleado.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Modificar;
end;

procedure TSaldosEmpleado.Refresh;
begin
     dmCajaAhorro.cdsHisAhorros.Refrescar;
end;

procedure TSaldosEmpleado.BBAgregarClick(Sender: TObject);
begin
     inherited;
     if PuedeAgregarPresta then
        dmCajaAhorro.cdsHisPrestamos.Agregar;
end;

procedure TSaldosEmpleado.BBModificarClick(Sender: TObject);
begin
     inherited;
     ModificaPrestamo;
end;

procedure TSaldosEmpleado.FormDblClick(Sender: TObject);
begin
     if ( ActiveControl = GridRenglones )  then
         ModificaPrestamo
     else
         inherited;
end;

procedure TSaldosEmpleado.WMExaminar(var Message: TMessage);
begin
     ModificaPrestamo;
end;

procedure TSaldosEmpleado.BBBorrarClick(Sender: TObject);
begin
     inherited;
     if ( PuedeBorrarPresta ) then
     begin
          with dmCajaAhorro.cdsHisPrestamos do
          begin
               if RecordCount = 0 then
                  ZInformation(Self.Caption,' No existen préstamos para borrar ',0)
               else
                   Borrar;
          end;
     end;
end;

procedure TSaldosEmpleado.ModificaPrestamo;
begin
     if PuedeModificarPresta then
        dmCajaAhorro.cdsHisPrestamos.Modificar;
end;

function TSaldosEmpleado.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= Revisa(D_AHORRO_REG_INSCRIP_EMP);
     if not Result then
        sMensaje:= 'No tiene permiso para inscribir a empleados';
end;

function TSaldosEmpleado.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= Revisa(D_AHORRO_REG_LIQ_RET);
     if not Result then
        sMensaje:= 'No tiene permiso para dar de baja inscripciones';
end;


function TSaldosEmpleado.PuedeAgregarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS, K_DERECHO_ALTA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para registrar préstamos',0);
end;

function TSaldosEmpleado.PuedeModificarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_CAMBIO);
     if not Result then
        ZInformation('Préstamos','No tiene permiso para modificar préstamos',0);
end;

function TSaldosEmpleado.PuedeBorrarPresta: Boolean;
begin
     Result:= CheckDerecho( D_EMP_NOM_PRESTAMOS, K_DERECHO_BAJA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para borrar préstamos',0);
end;



end.
