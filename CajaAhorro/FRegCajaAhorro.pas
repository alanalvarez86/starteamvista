unit FRegCajaAhorro;
                
interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, ToolWin, Db, ExtCtrls, ImgList,
     ZBaseConsulta,
     ZBaseConsultaBotones;

type
  TRegCajaAhorro = class(TBaseBotones)
    InscribirEmpleado: TToolButton;
    RegistrarPrestamo: TToolButton;
    RegistrarLiquidacion: TToolButton;
    RegistroDeposito: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RegCajaAhorro: TRegCajaAhorro;

implementation

uses FTressShell;

{$R *.DFM}

procedure TRegCajaAhorro.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= 0;
end;

end.
 