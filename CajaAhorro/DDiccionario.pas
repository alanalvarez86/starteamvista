unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  {$ifndef VER130}Variants,{$endif}  
  DBaseDiccionario,
  DBaseTressDiccionario,
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaClientDataSet  ;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
    procedure cdsDiccionAlModificar(Sender: TObject);
    procedure cdsDiccionAfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    {$ifdef RDD}
    {$else}
    function GetNOClasificacion(const lFavoritos, lMigracion, lSuscripciones: Boolean): ListaClasificaciones;override;
    {$endif}
  public
    {procedure CamposPorEntidad( const oEntidad: TipoEntidad;
                                const lTodos: Boolean; oLista: TStrings);}
    procedure SetLookupNames; override;
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
    function GetValorActivo({$ifdef RDD}const eValor: eRDDValorActivo{$else}const sValor: string{$endif}): string;override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation

uses
     ZBaseEdicion_DevEx,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZReportTools,
     FEditDiccion_DevEx,
     DGlobal,
     DCliente,
     DTablas;

{$R *.DFM}

procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     inherited;
     {FClasificacion :=   VarArrayOf( [crFavoritos,
                          crEmpleados ,
                          crCursos ,
                          crAsistencia ,
                          crNominas ,
                          crPagosIMSS ,
                          crConsultas ,
                          crCatalogos ,
                          crTablas ,
                          crSupervisor ,
                          crCafeteria ,
                          crLabor ,
                          crMedico ,
                          crCarrera ,
                          crKiosco ,
                          crAccesos ,
                          crEvaluacion ,
                          crMigracion ]
                         );}
end;
{
procedure TdmDiccionario.CamposPorEntidad( const oEntidad : TipoEntidad;
                                           const lTodos: Boolean;
                                                 oLista : TStrings );
 var oCampo : TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
          while NOT EOF do
          begin
               oCampo := TObjetoString.Create;
               oCampo.Campo := FieldByName('DI_NOMBRE').AsString;
               oLista.AddObject( FieldByName('DI_TITULO').AsString,
                                 oCampo );
               Next;
          end;
end;
}
procedure TdmDiccionario.cdsDiccionAlModificar(Sender: TObject);
begin
     inherited;
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditDiccion_DevEx, TEditDiccion_DevEx );
end;

procedure TdmDiccionario.cdsDiccionAfterPost(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     inherited;
     ErrorCount := 0;
     with DataSet as TZetaClientDataSet do
     begin
          if ChangeCount > 0 then
             Reconcile ( ServerDiccionario.GrabaDiccion( dmCliente.Empresa,Delta, ErrorCount ) );
     end;
end;

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     dmTablas.SetLookupNames;
end;

procedure TdmDiccionario.GetListaClasifiModulo( oLista : TStrings; const lMigracion : Boolean );
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
     {$ifdef RDD}
     {$else}
     AgregaClasifi( oLista, crCajaAhorro );
     {$endif}
end;

{$ifdef RDD}
{$else}
function TdmDiccionario.GetNOClasificacion( const lFavoritos, lMigracion, lSuscripciones : Boolean ): ListaClasificaciones;
begin
     Result := inherited GetNOClasificacion( lFavoritos, lMigracion, lSuscripciones );
end;
{$endif}

function TdmDiccionario.GetValorActivo({$ifdef RDD}const eValor: eRDDValorActivo{$else}const sValor: string{$endif}): string;
begin
     {$ifdef RDD}
     with dmCliente do
     begin
          case eValor of
               vaCajaAhorro: Result := Comillas( TipoAhorro );
          else
              Result := inherited GetValorActivo( eValor )
          end;
     end;
     {$else}
     with dmCliente do
     begin
          if ( sValor = K_CAJA_AHORRO ) then
             Result := Comillas( TipoAhorro )
          else
              Result := inherited GetValorActivo(  sValor );
     end;
     {$endif}

end;


end.
