unit FEditCtasBancarias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ZetaNumero, ZetaKeyLookup, Mask, ZetaEdit, ZetaKeyCombo;

type
  TEditCtasBancarias = class(TBaseEdicion)
    Label1: TLabel;
    CT_CODIGO: TZetaDBEdit;
    Label2: TLabel;
    CT_NOMBRE: TDBEdit;
    Label3: TLabel;
    CT_NUM_CTA: TDBEdit;
    CT_BANCO: TDBEdit;
    Label4: TLabel;
    AH_TIPOLbl: TLabel;
    AH_TIPO: TZetaDBKeyLookup;
    GBFormatos: TGroupBox;
    FormatoLBL: TLabel;
    Label5: TLabel;
    GBAdicionales: TGroupBox;
    Label6: TLabel;
    CT_NUMERO: TZetaDBNumero;
    Label7: TLabel;
    CT_TEXTO: TDBEdit;
    Label8: TLabel;
    CT_STATUS: TZetaDBKeyCombo;
    CT_REP_CHK: TZetaDBKeyLookup;
    CT_REP_LIQ: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditCtasBancarias: TEditCtasBancarias;

implementation

uses dCajaAhorro,
     dReportes,
     ZetaBuscador,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.dfm}

procedure TEditCtasBancarias.FormCreate(Sender: TObject);
const
     K_FILTRO_REPORTES_CTA = '( RE_ENTIDAD = %d ) and ( ( RE_TIPO = %d ) or ( RE_TIPO = %d ) or ( RE_TIPO = %d ) )';
var
   sFiltroCta: String;
begin
     inherited;
     IndexDerechos := D_AHORRO_CAT_CUENTAS_BANC; 
     HelpContext := H_REG_CTAS_BANCARIAS;
     FirstControl := CT_CODIGO;
     AH_TIPO.LookupDataset := dmCajaAhorro.cdsTAhorro;
     sFiltroCta := Format( K_FILTRO_REPORTES_CTA, [ Ord( enCtaMovs ), Ord( trListado ), Ord( trForma ), Ord( trImpresionRapida ) ] );
     with CT_REP_CHK do
     begin
          LookupDataset := dmReportes.cdsLookupReportes;
          Filtro := sFiltroCta;
     end;
     with CT_REP_LIQ do
     begin
          LookupDataset := dmReportes.cdsLookupReportes;
          Filtro := sFiltroCta;
     end;
end;

procedure TEditCtasBancarias.Connect;
begin
     dmReportes.cdsLookupReportes.Conectar;
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsCtasBancarias.Conectar;
          Datasource.Dataset := cdsCtasBancarias;
     end;
end;

procedure TEditCtasBancarias.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Cuentas bancarias', 'Cuentas bancarias', 'CT_CODIGO', dmCajaAhorro.cdsTAhorro );
end;


end.
