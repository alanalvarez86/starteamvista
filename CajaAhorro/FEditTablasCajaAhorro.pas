unit FEditTablasCajaAhorro;

interface

uses ZBaseTablas;

type
  TTOEditTiposDeposito = class( TEditLookup )
  protected
    {Protected declarations}
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure AfterCreate; override;
  end;

var
  EditTablasCajaAhorro: TEditTablas;

implementation

uses dCajaAhorro,
     DGlobal,
     ZetaCommonClasses,
     ZGlobalTress,
     ZAccesosTress;

{ TTOEditEstadoCivil }

procedure TTOEditTiposDeposito.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCajaAhorro.cdsTiposDeposito;
     HelpContext:= H_REG_TIPO_DEP_RET;
     IndexDerechos := 0; // PENDIENTE: ZAccesosTress.D_TAB_PER_EDOCIVIL;
end;

function TTOEditTiposDeposito.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

end.
