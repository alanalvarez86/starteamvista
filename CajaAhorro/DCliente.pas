unit DCliente;

interface

{$DEFINE REPORTING}
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBClient, dxSkinsForm,
     DBaseCliente,
{$ifdef DOS_CAPAS}
     DServerCajaAhorro,
     DServerCatalogos,
     DServerConsultas,
     DServerGlobal,
     DServerSistema,
     DServerTablas,
     DServerReportes,
     {$IFDEF REPORTING}
      DServerReporting,
     {$endif}
{$else}
     CajaAhorro_TLB,
{$endif}
     ZetaClientDataSet,
     ZetaCommonClasses,
     ZetaCommonLists;

type
  TInfoAhorroActivo = record
    Codigo: String;
    Descripcion: String;
    TipoPrestamo: String;
    Concepto: Integer;
    Relativo: Integer;
  end;
  TdmCliente = class(TBaseCliente)
    cdsEmpleado: TZetaClientDataSet;
    cdsTipoAhorro: TZetaClientDataSet;
    cdsPeriodo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpleadoAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FEmpleado: TNumEmp;
    FEmpleadoBOF: Boolean;
    FEmpleadoEOF: Boolean;
    FTipoAhorro: String;
{$ifdef DOS_CAPAS}
    FServerCajaAhorro: TdmServerCajaAhorro;
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    FServerTablas: TdmServerTablas;
    FServerReportes : TdmServerReportes;
    {$ifdef REPORTING}
    FServerReporteador: TdmServerReporting;
    {$endif}
    function GetServerCajaAhorro: TdmServerCajaAhorro;
{$else}
    FServerCajaAhorro: IdmServerCajaAhorroDisp;
    function GetServerCajaAhorro: IdmServerCajaAhorroDisp;
{$endif}
    function FetchEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchNextEmployee(const iEmpleado: TNumEmp): Boolean;
    function FetchPreviousEmployee(const iEmpleado: TNumEmp): Boolean;
    function GetAsistenciaDescripcion: String;
    procedure SetTipoAhorro(const Value: String);
    function GetPeriodoInicial: Boolean;
    function GetComputerName: String;     
  protected
    procedure InitCacheModules; override;
    {***DevEx(@am): Metodos virtuales para poder compilar unidades utilizadas en varios proyectos ***}
    function GetSkinController: TdxSkinController; override;
    {***}
  public
    { Public declarations }
{$ifdef DOS_CAPAS}
    property ServerCajaAhorro: TdmServerCajaAhorro read GetServerCajaAhorro;
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    property ServerTablas: TdmServerTablas read FServerTablas;
    property ServerReportes: TdmServerReportes read FServerReportes;
    {$ifdef REPORTING}
    property ServerReporteador: TdmServerReporting read FServerReporteador;
    {$endif}
{$else}
    property ServerCajaAhorro: IdmServerCajaAhorroDisp read GetServerCajaAhorro;
{$endif}
    property ComputerName: String read GetComputerName;
    property Empleado: TNumEmp read FEmpleado;
    property TipoAhorro: String read FTipoAhorro write SetTipoAhorro;
    function GetValorActivoStr(const eTipo: TipoEstado): String; override;
    function GetEmpleadoAnterior: Boolean;
    function GetEmpleadoInicial: Boolean;
    function GetEmpleadoPrimero: Boolean;
    function GetEmpleadoSiguiente: Boolean;
    function GetEmpleadoUltimo: Boolean;
    function GetDatosEmpleadoActivo: TInfoEmpleado;
    function SetEmpleadoNumero(const Value: TNumEmp): Boolean;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetEmpleadoDescripcion: String;
    function GetTipoAhorroDescripcion: String;
    function EmpleadoDownEnabled: Boolean;
    function EmpleadoUpEnabled: Boolean;
    function GetDatosAhorroActivo: TInfoAhorroActivo;
    procedure CargaActivosTodos(Parametros: TZetaParams); override;
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema(Parametros: TZetaParams);
    procedure InitActivosEmpleado;
    procedure InitActivosTipoAhorro;
    procedure LeeTiposAhorro(Lista: TStrings);
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses dCajaAhorro,
     DTablas,
     DCatalogos,
     DDiccionario,
     ZetaRegistryCliente,
     ZetaCommonTools, FTressShell;

{ ************** TdmCliente *************** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     ModoCajaAhorro := TRUE;
{$ifdef DOS_CAPAS}
     FServerCajaAhorro := TdmServerCajaAhorro.Create( Self );
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     FServerTablas := TdmServerTablas.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     {$ifdef REPORTING}
     FServerReporteador := TdmServerReporting.Create( self );
     {$endif}
{$endif}
     SetTipoLookupEmpleado( eLookEmpMedicos );   // Permite traer campos: CB_NOMBRES, CB_APE_PAT y CB_APE_MAT que se ocupan en lookups
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerTablas );
     FreeAndNil( FServerSistema );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerCajaAhorro );
     {$ifdef REPORTING}
     FreeAndNil( FServerReporteador );
     {$endif}
{$endif}
     inherited;
end;

{$ifdef DOS_CAPAS}
function TdmCliente.GetServerCajaAhorro: TdmServerCajaAhorro;
begin
     Result := FServerCajaAhorro;
end;
{$else}
function TdmCliente.GetServerCajaAhorro: IdmServerCajaAhorroDisp;
begin
     Result := IdmServerCajaAhorroDisp( dmCliente.CreaServidor( CLASS_dmServerCajaAhorro, FServerCajaAhorro ) );
end;
{$endif}

procedure TdmCliente.InitCacheModules;
begin
     inherited;
     DataCache.AddObject( 'Tabla', dmTablas );
     DataCache.AddObject( 'Cat�logo', dmCatalogos );
     DataCache.AddObject( 'Diccionario', dmDiccionario );
end;

procedure TdmCliente.CargaActivosIMSS( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', '' );
          AddInteger( 'IMSSYear', 0 );
          AddInteger( 'IMSSMes', 0 );
          AddInteger( 'IMSSTipo', 0 );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddInteger( 'Year', TheYear( FechaDefault ) );
          AddInteger( 'Tipo', 0 );
          AddInteger( 'Numero', 0 );
     end;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', Date );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( Date ) );
          AddInteger( 'EmpleadoActivo', Empleado );
          AddString( 'NombreUsuario', GetDatosUsuarioActivo.Nombre );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.InitActivosEmpleado;
begin
     FEmpleadoBOF := False;
     FEmpleadoEOF := False;
     FEmpleado := 0;
     GetEmpleadoInicial;
end;

procedure TdmCliente.InitActivosTipoAhorro;
begin
     with cdsTipoAhorro do
     begin
          Data := ServerCajaAhorro.GetTiposAhorro( Empresa );
          if IsEmpty then
             FTipoAhorro := ''
          else
              FTipoAhorro := FieldByName( 'TB_CODIGO' ).AsString;
     end;
end;

procedure TdmCliente.LeeTiposAhorro( Lista: TStrings );
begin
     with Lista do
     begin
          BeginUpdate;
          Clear;
          with cdsTipoAhorro do
          begin
               while not Eof do
               begin
                    if zStrToBool( FieldByName( 'TB_ACTIVO' ).AsString ) and ListaIntersectaConfidencialidad(FieldByName( 'TB_NIVEL0' ).AsString, Confidencialidad )   then
                       Add( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString );
                    Next;
               end;
               cdsTipoAhorro.Locate( 'TB_CODIGO', FTipoAhorro, [] );
          end;
          EndUpdate;
     end;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

procedure TdmCliente.SetTipoAhorro(const Value: String);
begin
     if ( FTipoAhorro <> Value ) then
     begin
          cdsTipoAhorro.Locate( 'TB_CODIGO', Value, [] );
          FTipoAhorro := Value;
     end;
end;

function TdmCliente.GetDatosAhorroActivo: TInfoAhorroActivo;
begin
     with Result do
     begin
          Codigo := self.FTipoAhorro;
          with cdsTipoAhorro do
          begin
               Descripcion := FieldByName( 'TB_ELEMENT' ).AsString;
               TipoPrestamo := FieldByName( 'TB_PRESTA' ).AsString;
               Concepto := FieldByName( 'TB_CONCEPT' ).AsInteger;
               Relativo := FieldByName( 'TB_RELATIV' ).AsInteger;
          end;
     end;
end;

{function TdmCliente.GetFechaAsistencia: TDate;
begin
     Result := Trunc( Date );
end;}

function TdmCliente.GetEmpleadoDescripcion: String;
begin
     Result := IntToStr( FEmpleado ) + ': '+ cdsEmpleado.FieldByName( 'PRETTYNAME' ).AsString;
end;

function TdmCliente.GetTipoAhorroDescripcion: String;
begin
     Result := FTipoAhorro + ': '+ cdsTipoAhorro.FieldByName( 'TB_ELEMENT' ).AsString;
end;

function TdmCliente.GetAsistenciaDescripcion: String;
begin
     Result := FechaCorta( FechaDefault );
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
     case eTipo of
          stEmpleado: Result := GetEmpleadoDescripcion;
          stTipoAhorro: Result := GetTipoAhorroDescripcion;
          stFecha: Result := GetAsistenciaDescripcion;
     end;
end;

function TdmCliente.EmpleadoDownEnabled: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoBOF;
     end;
end;

function TdmCliente.EmpleadoUpEnabled: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := Active and not IsEmpty and not FEmpleadoEOF;
     end;
end;

function TdmCliente.GetEmpleadoAnterior: Boolean;
begin
     Result := FetchPreviousEmployee( FEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoEOF := False;
     end
     else
         FEmpleadoBOF := True;
end;

function TdmCliente.GetEmpleadoSiguiente: Boolean;
begin
     Result := FetchNextEmployee( FEmpleado );
     if Result then
     begin
          with cdsEmpleado do
          begin
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
          FEmpleadoBOF := False;
     end
     else
         FEmpleadoEOF := True;
end;

function TdmCliente.GetEmpleadoUltimo: Boolean;
var
   iNewEmp: TNumEmp;
begin
     Result := FetchPreviousEmployee( MAXINT );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleadoBOF := False;
               FEmpleado := iNewEmp;
               FEmpleadoEOF := ( iNewEmp = FEmpleado );
          end;
     end;
end;

function TdmCliente.FetchEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchPreviousEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoAnterior( Empresa, iEmpleado, VACIO, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.FetchNextEmployee( const iEmpleado: TNumEmp ): Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoSiguiente( Empresa, iEmpleado, VACIO, Datos );
     if Result then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
          end;
     end;
end;

function TdmCliente.GetEmpleadoPrimero: Boolean;
var
   iNewEmp: TNumEmp;
begin
     Result := FetchNextEmployee( 0 );
     if Result then
     begin
          with cdsEmpleado do
          begin
               iNewEmp := FieldByName( 'CB_CODIGO' ).AsInteger;
               FEmpleado := iNewEmp;
               FEmpleadoBOF := ( iNewEmp = FEmpleado );
               FEmpleadoEOF := False;
          end;
     end;
end;

function TdmCliente.GetEmpleadoInicial: Boolean;
var
   Datos: OleVariant;
begin
     Result := Servidor.GetEmpleadoAnterior( Empresa, MAXINT, VACIO, Datos );
     with cdsEmpleado do
     begin
          Data := Datos;
          FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
     end;
     FEmpleadoEOF := True;
end;

function TdmCliente.GetDatosEmpleadoActivo: TInfoEmpleado;
begin
     with Result do
     begin
          Numero := Self.Empleado;
          with cdsEmpleado do
          begin
               Nombre := FieldByName( 'PRETTYNAME' ).AsString;
               Activo := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
               Baja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
               Ingreso := FieldByName( 'CB_FEC_ING' ).AsDateTime;
          end;
     end;
end;

procedure TdmCliente.cdsEmpleadoAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     FetchEmployee( FEmpleado );
end;

function TdmCliente.SetEmpleadoNumero( const Value: TNumEmp ): Boolean;
begin
     if ( FEmpleado <> Value ) then
     begin
          Result := FetchEmployee( Value );
          if Result then
          begin
               with cdsEmpleado do
               begin
                    FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               end;
               FEmpleadoBOF := False;
               FEmpleadoEOF := False;
          end;
     end
     else
         Result := True;
end;

function TdmCliente.GetPeriodoInicial: Boolean;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, YearDefault, ClientRegistry.TipoNomina );
          Result := not IsEmpty;
     end;
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          with cdsPeriodo do
          begin
               if NOT Active then
               begin
                    if NOT GetPeriodoInicial then
                       Raise Exception.Create('No existen per�odos definidos');
               end;
                  
               Year := FieldByName( 'PE_YEAR' ).AsInteger;
               Tipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );
               Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
{$ifdef QUINCENALES}
               InicioAsis := FieldByName( 'PE_ASI_INI' ).AsDateTime;
               FinAsis := FieldByName( 'PE_ASI_FIN' ).AsDateTime;
{$endif}
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;

function TdmCliente.GetComputerName: String;
begin
    // Result := ZetaWinAPITools.GetComputerName;
end;

function TdmCliente.GetSkinController: TdxSkinController;
begin
     Result:= TressShell.DevEx_SkinController;
end;

end.

