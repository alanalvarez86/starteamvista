unit FProcCajaAhorro;
                 
interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, ToolWin, Db, ExtCtrls, ImgList,
     ZBaseConsulta,
     ZBaseConsultaBotones;

type
  TProcCajaAhorro = class(TBaseBotones)
    DepositarRetenciones: TToolButton;
    CalcularIntereses: TToolButton;
    RepartoAnual: TToolButton;
    CierreAnual: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProcCajaAhorro: TProcCajaAhorro;

implementation

uses FTressShell,
     ZetaCommonClasses;

{$R *.DFM}

procedure TProcCajaAhorro.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H30034_Procesos_nomina;
end;

end.
