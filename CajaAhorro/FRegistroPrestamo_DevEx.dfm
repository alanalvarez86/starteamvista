inherited RegistroPrestamo_DevEx: TRegistroPrestamo_DevEx
  Left = 269
  Top = 67
  Caption = 'Registrar pr'#233'stamo'
  ClientHeight = 613
  PixelsPerInch = 96
  TextHeight = 13
  object AH_TIPOLbl: TLabel [0]
    Left = 75
    Top = 79
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pr'#233'stamo:'
  end
  object Label8: TLabel [1]
    Left = 6
    Top = 123
    Width = 116
    Height = 13
    Alignment = taRightJustify
    Caption = 'Referencia de pr'#233'stamo:'
  end
  object Label1: TLabel [2]
    Left = 47
    Top = 147
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de inicio:'
  end
  object EmpleadoLbl: TLabel [3]
    Left = 73
    Top = 100
    Width = 50
    Height = 13
    Caption = 'Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label15: TLabel [4]
    Left = 37
    Top = 170
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de pr'#233'stamo:'
  end
  object TIPO_PRESTAMO: TZetaTextBox [5]
    Left = 168
    Top = 77
    Width = 266
    Height = 17
    AutoSize = False
    Caption = 'TIPO_PRESTAMO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object PR_TIPO: TZetaDBTextBox [6]
    Left = 126
    Top = 77
    Width = 41
    Height = 17
    AutoSize = False
    Caption = 'PR_TIPO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'PR_TIPO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 577
    TabOrder = 7
  end
  inherited PanelIdentifica: TPanel
    TabOrder = 9
  end
  object PR_REFEREN: TZetaDBEdit [9]
    Left = 126
    Top = 119
    Width = 80
    Height = 21
    TabOrder = 1
    DataField = 'PR_REFEREN'
    DataSource = DataSource
  end
  object PR_FECHA: TZetaDBFecha [10]
    Left = 126
    Top = 142
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '17/Dec/97'
    Valor = 35781.000000000000000000
    DataField = 'PR_FECHA'
    DataSource = DataSource
  end
  object CB_CODIGO: TZetaDBKeyLookup_DevEx [11]
    Left = 126
    Top = 96
    Width = 333
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object GBCondiciones: TGroupBox [12]
    Left = 0
    Top = 196
    Width = 474
    Height = 260
    Caption = ' Condiciones del pr'#233'stamo '
    TabOrder = 4
    object LBL_Formula: TLabel
      Left = 74
      Top = 183
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object Label7: TLabel
      Left = 34
      Top = 22
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto solicitado:'
    end
    object Label3: TLabel
      Left = 38
      Top = 45
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa de inter'#233's:'
    end
    object Label4: TLabel
      Left = 68
      Top = 68
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Duraci'#243'n:'
    end
    object Label5: TLabel
      Left = 48
      Top = 114
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total a pagar:'
    end
    object LBL_Cuantos_pagos: TLabel
      Left = 40
      Top = 137
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cu'#225'ntos pagos:'
    end
    object LBL_Cada_Pago: TLabel
      Left = 59
      Top = 160
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cada pago:'
    end
    object Label10: TLabel
      Left = 26
      Top = 234
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Abonos anteriores:'
    end
    object Label14: TLabel
      Left = 68
      Top = 91
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Intereses:'
    end
    object PR_FORMULA: TDBMemo
      Left = 118
      Top = 179
      Width = 305
      Height = 49
      DataField = 'PR_FORMULA'
      DataSource = DataSource
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 255
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 7
    end
    object PR_MONTO_S: TZetaDBNumero
      Left = 118
      Top = 18
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 0
      Text = '0.00'
      DataField = 'PR_MONTO_S'
      DataSource = DataSource
    end
    object PR_TASA: TZetaDBNumero
      Left = 118
      Top = 41
      Width = 115
      Height = 21
      Mascara = mnTasa
      TabOrder = 1
      Text = '0.0 %'
      DataField = 'PR_TASA'
      DataSource = DataSource
    end
    object PR_MESES: TZetaDBNumero
      Left = 118
      Top = 64
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
      OnChange = PR_MESESChange
      DataField = 'PR_MESES'
      DataSource = DataSource
    end
    object PR_MONTO: TZetaDBNumero
      Left = 118
      Top = 110
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 4
      Text = '0.00'
      DataField = 'PR_MONTO'
      DataSource = DataSource
    end
    object PR_PAGOS: TZetaDBNumero
      Left = 118
      Top = 133
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
      DataField = 'PR_PAGOS'
      DataSource = DataSource
    end
    object PR_PAG_PER: TZetaDBNumero
      Left = 118
      Top = 156
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 6
      Text = '0.00'
      DataField = 'PR_PAG_PER'
      DataSource = DataSource
    end
    object PR_SALDO_I: TZetaDBNumero
      Left = 118
      Top = 230
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 9
      Text = '0.00'
      DataField = 'PR_SALDO_I'
      DataSource = DataSource
    end
    object PR_INTERES: TZetaDBNumero
      Left = 118
      Top = 87
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 3
      Text = '0.00'
      DataField = 'PR_INTERES'
      DataSource = DataSource
    end
    object SBCO_FORMULA_DevEx: TcxButton
      Left = 424
      Top = 179
      Width = 25
      Height = 25
      Hint = 'Abrir Constructor de F'#243'rmulas'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = SBCO_FORMULAClick
      LookAndFeel.SkinName = 'TressMorado2013'
      OptionsImage.Glyph.Data = {
        7A080000424D7A08000000000000360000002800000017000000170000000100
        2000000000004408000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFF000000000000000000000000D9AB81FFD8AA7FFF0000000000000000D8AA
        7FFFD8AA7FFF000000002E241B36D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
        0000DCB38DFF0000000000000000D8AA7FFFDAAF87FF00000000000000000000
        00000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD9AD83FF00000000DAAE85FFD8AA7FFFD8AA7FFF00000000000000000302
        0203D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFF00000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000
        000000000000D8AA7FFFD8AA7FFF010000010000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF000000000000
        0000D8AA7FFF0000000000000000D8AA7FFF0000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000DCB3
        8DFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFF0000000000000000000000000000000000000000D8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFF0000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFDCB28BFF00000000D9AD83FF0000000000000000D8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFF00000000000000000000000000000000D8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
        7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
    end
  end
  object GBCheque: TGroupBox [13]
    Left = 0
    Top = 464
    Width = 474
    Height = 110
    Caption = '                                 '
    TabOrder = 6
    object Label11: TLabel
      Left = 33
      Top = 21
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cuenta bancaria:'
    end
    object Label12: TLabel
      Left = 64
      Top = 44
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheque #:'
    end
    object Label13: TLabel
      Left = 55
      Top = 67
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object CT_CODIGO: TZetaKeyLookup_DevEx
      Left = 118
      Top = 18
      Width = 333
      Height = 21
      LookupDataset = dmCajaAhorro.cdsCtasBancarias
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = CT_CODIGOValidKey
    end
    object CM_CHEQUE: TZetaNumero
      Left = 118
      Top = 41
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
    end
    object CM_DESCRIP: TEdit
      Left = 118
      Top = 64
      Width = 331
      Height = 21
      TabOrder = 3
    end
    object ImprimeCheque: TCheckBox
      Left = 35
      Top = 86
      Width = 96
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Imprimir cheque:'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object BtnSiguiente_DevEx: TcxButton
      Left = 236
      Top = 41
      Width = 21
      Height = 21
      Hint = 'Sugiere siguiente n'#250'mero de cheque'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtnSiguienteClick
      OptionsImage.Glyph.Data = {
        DA050000424DDA05000000000000360000002800000013000000130000000100
        200000000000A4050000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFBAB4BBFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA69F
        A7FFF6F5F6FFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFFACA5ACFFFAF9FAFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFFFFFFC5C0
        C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D95
        9EFFFDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFBFBFBFFFFFFFFFFFFFFFFFFFFFF
        FFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA69F
        A7FFFAF9FAFFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFA199A2FFF4F3F4FFFFFFFFFFC5C0C6FF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9D95
        9EFFEFEDEFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF968D97FFB3ACB4FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
    end
  end
  object CM_PRESTA: TZetaKeyCombo [14]
    Left = 126
    Top = 166
    Width = 115
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 3
    OnChange = CM_PRESTAChange
    ListaFija = lfTiposdePrestamo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object gbNomina: TGroupBox [15]
    Left = 0
    Top = 45
    Width = 474
    Height = 46
    Align = alTop
    Caption = ' N'#243'mina Activa '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
    Visible = False
    object PeriodoTipoLbl: TLabel
      Left = 50
      Top = 19
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object TipoNomina: TZetaTextBox
      Tag = 1
      Left = 75
      Top = 17
      Width = 74
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PeriodoNumeroLBL: TLabel
      Left = 159
      Top = 19
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object NumeroNomina: TZetaTextBox
      Tag = 1
      Left = 199
      Top = 17
      Width = 42
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PeriodoDelLBL: TLabel
      Left = 249
      Top = 19
      Width = 14
      Height = 13
      Caption = 'del'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object PeriodoFechaInicial: TZetaTextBox
      Tag = 1
      Left = 264
      Top = 17
      Width = 74
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PeriodoAlLBL: TLabel
      Left = 343
      Top = 19
      Width = 8
      Height = 13
      Caption = 'al'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object PeriodoFechaFinal: TZetaTextBox
      Tag = 1
      Left = 353
      Top = 17
      Width = 74
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 8
  end
  object RegistrarCheque: TCheckBox [17]
    Left = 11
    Top = 461
    Width = 105
    Height = 17
    Caption = 'Registrar cheque'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = RegistrarChequeClick
  end
  inherited DataSource: TDataSource
    Left = 348
    Top = 128
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
