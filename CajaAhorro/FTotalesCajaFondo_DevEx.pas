unit FTotalesCajaFondo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls,
  ZetaDBTextBox, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid,  Menus, ActnList, ImgList,
  TressMorado2013;

type
  TTotalesCajaFondo_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SOCIOS_ACTIVOS: TZetaDBTextBox;
    TOTAL_AHORRADO: TZetaDBTextBox;
    PRESTAMOS_ACTIVOS: TZetaDBTextBox;
    SALDO_PRESTAMOS: TZetaDBTextBox;
    labels: TLabel;
    SALDO_BANCOS: TZetaDBTextBox;
    dsCuentas: TDataSource;
    CT_CODIGO: TcxGridDBColumn;
    CT_NUM_CTA: TcxGridDBColumn;
    CT_NOMBRE: TcxGridDBColumn;
    CT_BANCO: TcxGridDBColumn;
    SALDO_HOY: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;
  private
    { Private declarations }
  public
    { Public declarations }
    function PuedeImprimir( var sMensaje: String ): Boolean; override;
  end;

var
  TotalesCajaFondo_DevEx: TTotalesCajaFondo_DevEx;

implementation


uses ZetaCommonLists,
     ZetaCommonClasses,
     dCajaAhorro,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaDialogo;

{$R *.dfm}

procedure TTotalesCajaFondo_DevEx.Connect;
begin
     inherited;
     with dmCajaAhorro do
     begin
          cdsTotalesFondoCaja.Refrescar;
          Datasource.Dataset := cdsTotalesFondoCaja;
          dsCuentas.DataSet := cdsTotalesCuentas;
     end;
end;

procedure TTotalesCajaFondo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     //grdCuentas.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= H_SALDO_CAJA_AHORRO;
     TipoValorActivo1 := stTipoAhorro;
end;

procedure TTotalesCajaFondo_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth; //DevEx: Se invoca despues de que todas las columnas del grid han sido creadas.
     inherited;
     IndexDerechos := D_AHORRO_CAT_CUENTAS_BANC;
end;

procedure TTotalesCajaFondo_DevEx.Refresh;
begin
     inherited;
     dmCajaAhorro.cdsTotalesFondoCaja.Refrescar;
     //Aplica el BestFit para las columnas despues de cualquier operacion
     DoBestFit;
end;

procedure TTotalesCajaFondo_DevEx.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsTotalesCuentas.Agregar;
     Refresh;
end;

procedure TTotalesCajaFondo_DevEx.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsTotalesCuentas.Modificar;
     Refresh;
end;

procedure TTotalesCajaFondo_DevEx.Borrar;
begin
     inherited;
     dmCajaAhorro.cdsTotalesCuentas.Borrar;
     Refresh;
end;

function TTotalesCajaFondo_DevEx.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result:= CheckDerecho( D_AHORRO_TOT_CAJA_FONDO, K_DERECHO_IMPRESION);
     if not Result then
        sMensaje:='No tiene Permiso para Imprimir Registros';
end;



end.
