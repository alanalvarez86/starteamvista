unit FWizDepositarRetenciones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizard, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaKeyLookup, Mask, ZetaNumero, ZetaKeyCombo, ZetaDBTextBox, ZetaFecha,
  DBCtrls, ZetaCommonClasses,ZetaCommonLists;

type
  TWizDepositarRetenciones = class(TBaseWizard)
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    PE_TIPO: TZetaKeyCombo;
    PE_YEAR: TZetaNumero;
    PE_NUMERO: TZetaKeyLookup;
    cbRetencionesPrestamos: TCheckBox;
    cbRetencionesAhorro: TCheckBox;
    Retenciones: TTabSheet;
    Label1: TLabel;
    Num_ahorro: TZetaTextBox;
    Suma_ahorro: TZetaTextBox;
    Num_Prestamos: TZetaTextBox;
    Label2: TLabel;
    Suma_prestamos: TZetaTextBox;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    CT_CODIGO: TZetaKeyLookup;
    CM_FECHA: TZetaFecha;
    CM_DESCRIP: TEdit;
    Label13: TLabel;
    CM_MONTO: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure PE_YEARExit(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
    FChangedPeriodo : Boolean;
    FParams : TZetaParams;
    FMonto: Double;
    procedure SetFiltroPeriodo(const iYear: Integer;const TipoPeriodo: eTipoPeriodo);
  public
    { Public declarations }
  end;

var
  WizDepositarRetenciones: TWizDepositarRetenciones;

implementation
uses
    DCliente,
    DCajaAhorro,
    ZetaDialogo,
    ZetaCommonTools ;

{$R *.dfm}

procedure TWizDepositarRetenciones.FormCreate(Sender: TObject);
begin
     inherited;
     FChangedPeriodo := FALSE;
     FParams := TZetaParams.Create;
     HelpContext := H_DEP_RETENCIONES;
     PE_NUMERO.LookupDataSet := dmCajaAhorro.cdsPeriodo;
     CT_CODIGO.LookupDataset := dmCajaAhorro.cdsCtasBancarias;

     dmCajaAhorro.cdsCtasBancarias.Conectar;
     dmCajaAhorro.cdsPeriodo.Conectar;

     with dmCliente.GetDatosPeriodoActivo do
     begin
          PE_YEAR.Valor := Year;
          //PE_TIPO.Valor := Ord( Tipo );
          PE_NUMERO.Valor := Numero;
          SetFiltroPeriodo( Year, Tipo );
     end;


     cbRetencionesPrestamos.Checked := TRUE;
     cbRetencionesAhorro.Checked := TRUE;

     CM_FECHA.Valor := dmCliente.FechaDefault;
end;

procedure TWizDepositarRetenciones.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
 var
    oParams : TZetaParams;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               FParams.AddInteger('Year', PE_YEAR.ValorEntero );
               FParams.AddInteger('Numero', PE_NUMERO.Valor );
               FParams.AddInteger('Tipo', PE_TIPO.LlaveEntero );//acl               
               if( dmCliente.GetDatosAhorroActivo.Concepto = 0 ) then
               begin
                    ZError(Caption, 'El ahorro activo no tiene relacionado un concepto', 0);
                    CanMove := FALSE;
               end
               else
                   FParams.AddInteger('ConceptoAhorro', dmCliente.GetDatosAhorroActivo.Concepto );

               FParams.AddInteger('ConceptoRelativo', dmCliente.GetDatosAhorroActivo.Relativo );

               with dmCajaAhorro.cdsTPresta do
               begin
                    Conectar;
                    if Locate( 'TB_CODIGO', dmCliente.GetDatosAhorroActivo.TipoPrestamo, [] ) then
                    begin
                         FParams.AddInteger('ConceptoPrestamo', FieldByName('TB_CONCEPT').AsInteger );
                    end
                    else
                    begin
                         ZError(Caption, 'El ahorro activo no tiene relacionado un pr�stamo', 0);
                         CanMove := FALSE;
                    end;
               end;
               if CanMove then
               begin
                    oParams := TZetaParams.Create;
                    try
                       oParams.VarValues := dmCajaAhorro.GetTotalesRetenciones( FParams );
                       while oParams.Count > 0 do
                           FParams.AddParam( oParams.Items[0] );
                    finally
                           FreeAndNil(oParams)
                    end;

                    with FParams do
                    begin
                         CM_DESCRIP.Text := StrLeft( 'Retenciones de ' + ShowNomina( PE_YEAR.ValorEntero, PE_TIPO.LlaveEntero, PE_NUMERO.Valor ), 50 ); //acl

                         Num_ahorro.Caption := IntToStr( ParamBYName('Num_Ahorro').AsInteger );
                         Num_Prestamos.Caption := IntToStr( ParamBYName('Num_Prestamos').AsInteger );

                         Suma_ahorro.Caption := FormatFloat( '$#,0.00', ParamBYName('Suma_Ahorro').AsFloat );
                         Suma_prestamos.Caption := FormatFloat( '$#,0.00', ParamBYName('Suma_Prestamos').AsFloat );
                         FMonto := 0;

                         if cbRetencionesAhorro.Checked then
                            FMonto := FMonto + ParamBYName('Suma_Ahorro').AsFloat;

                         if cbRetencionesPrestamos.Checked then
                            FMonto := FMonto + ParamBYName('Suma_Prestamos').AsFloat;

                         CM_MONTO.Caption := FormatFloat( '#,0.00', FMonto );
                    end;
               end;
          end
          else if Wizard.EsPaginaActual( Retenciones ) then
          begin
               if FMonto = 0 then
               begin
                    ZError(Caption, 'El monto debe ser mayor a cero', 0);
                    CanMove := FALSE;
               end;
               if StrVacio( CT_CODIGO.Llave ) then
               begin
                    ZError(Caption, 'La cuenta no debe quedar vac�a', 0);
                    CT_CODIGO.SetFocus;
                    CanMove := FALSE;
               end;
          end;
     end;
end;

procedure TWizDepositarRetenciones.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     with FParams do
     begin
          AddString('Cuenta', CT_CODIGO.Llave );
          AddFloat('Monto', FMonto );
          AddString('Descripcion', CM_DESCRIP.Text );
          AddDate('Fecha', CM_FECHA.Valor );
     end;

     lOk := dmCajaAhorro.GrabaRetenciones( FParams );
     if lOK then
     begin
          ZInformation(Caption, Format( 'El dep�sito de %f fue realizado', [FMonto] ), 0 );
          Close;
     end;
end;

procedure TWizDepositarRetenciones.FormDestroy(Sender: TObject);
begin
     FreeAndNil(FParams);
     inherited;

end;

procedure TWizDepositarRetenciones.PE_YEARExit(Sender: TObject);
begin
     inherited;
     if ( PE_TIPO.Llave <> VACIO ) then
        SetFiltroPeriodo( PE_YEAR.ValorEntero, eTipoPeriodo( PE_TIPO.LlaveEntero  ) );  //acl
end;

procedure TWizDepositarRetenciones.SetFiltroPeriodo( const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCajaAhorro do
     begin
          if ( not cdsPeriodo.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodo.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodo.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  ConectarPeriodo( iYear, TipoPeriodo );
          end
          else
              ConectarPeriodo( iYear, TipoPeriodo );

          with PE_NUMERO do
          begin
               SetLlaveDescripcion( VACIO, VACIO );
               if ( dmCliente.GetDatosPeriodoActivo.Tipo = TipoPeriodo ) then
                  Valor := dmCliente.GetDatosPeriodoActivo.Numero
               else
                   Valor := 1;
          end;
     end;
end;



procedure TWizDepositarRetenciones.FormShow(Sender: TObject);
begin
     inherited;
     with PE_TIPO do
     begin
          ListaFija:=lfTipoPeriodoConfidencial; //acl
          LlaveEntero:= Ord( dmCliente.GetDatosPeriodoActivo.Tipo );
     end;
end;

end.
