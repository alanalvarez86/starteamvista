unit FRegistroDepositoRetiro_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, Mask, ZetaFecha, ZetaEdit,
  ZetaKeyLookup_DevEx, DB, ExtCtrls, ZetaNumero,
  ZetaKeyCombo, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ImgList,
  dxBarExtItems, dxBar, cxClasses, cxNavigator, cxDBNavigator, cxButtons;

type
  TRegistroDepositoRetiro_DevEx = class(TBaseEdicion_DevEx )
    EmpleadoLbl: TLabel;
    Label8: TLabel;
    CM_FECHA: TZetaDBFecha;
    Label11: TLabel;
    CT_CODIGO: TZetaDBKeyLookup_DevEx;
    CM_DEP_RET: TDBRadioGroup;
    Label3: TLabel;
    CM_TIPO: TZetaDBKeyLookup_DevEx;
    lbMonto: TLabel;
    CM_MONTO: TZetaDBNumero;
    lbCheque: TLabel;
    CM_CHEQUE: TZetaDBNumero;
    Label13: TLabel;
    CM_DESCRIP: TDBEdit;
    CM_BENEFI: TDBEdit;
    LBL_STATUS_MOV: TLabel;
    CM_STATUS: TZetaDBKeyCombo;
    BtnSiguiente_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnSiguienteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CM_DEP_RETClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FAgregarNuevo: Boolean;
  protected
    { Protected declarations }
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
    property AgregarNuevo: Boolean read FAgregarNuevo write FAgregarNuevo;
  end;

var
  RegistroDepositoRetiro_DevEx: TRegistroDepositoRetiro_DevEx;

implementation

uses dCajaAhorro,
     dCliente,
     ZetaTipoEntidad,
     ZImprimeForma,
     ZConstruyeFormula,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaClientDataSet, DBasicoCliente;

{$R *.dfm}

procedure TRegistroDepositoRetiro_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stTipoAhorro;
     IndexDerechos := 0; // PENDIENTE
     HelpContext := H_REG_MOVIMIENTOS;
     FirstControl := CM_MONTO;
     CT_CODIGO.LookupDataset := dmCajaAhorro.cdsCtasBancarias;
     //CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     CM_TIPO.LookupDataset := dmCajaAhorro.cdsTiposDeposito;
     //CM_STATUS.ListaFija := lfStatusAhorro;
     //PE_NUMERO.LookupDataset := dmCajaAhorro.cdsPeriodo;
end;

procedure TRegistroDepositoRetiro_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //DevEx:
     if ( FAgregarNuevo ) then
     begin
        dxBarControlContainerItem_DBNavigator.Visible := ivNever;
        dxBarButton_AgregarBtn.Visible := ivNever;
        dxBarButton_BorrarBtn.Visible := ivNever;
        dxBarButton_ModificarBtn.Visible := ivNever;
     end
     else
     begin
        dxBarControlContainerItem_DBNavigator.Visible := ivAlways;
        dxBarButton_AgregarBtn.Visible := ivAlways;
        dxBarButton_BorrarBtn.Visible := ivAlways;
        dxBarButton_ModificarBtn.Visible := ivAlways;
     end;

     CM_STATUS.Enabled := NOT FAgregarNuevo;
     LBL_STATUS_MOV.Enabled := Not FAgregarNuevo;
     CM_DEP_RETClick(nil);
end;

procedure TRegistroDepositoRetiro_DevEx.Connect;
const
     K_FILTRO_CTAS = '( AH_TIPO = %s )';
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsCtasBancarias.Conectar;
          cdsTiposDeposito.Conectar;
          cdsPeriodo.Conectar;

          if FAgregarNuevo then
          begin
               cdsCtasMovs.Refrescar;
               cdsCtasMovs.Append;
          end;
          Datasource.Dataset := cdsCtasMovs;
          
          CT_CODIGO.Filtro := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
     end;
end;

procedure TRegistroDepositoRetiro_DevEx.EscribirCambios;
begin
     inherited;
end;

procedure TRegistroDepositoRetiro_DevEx.Agregar;
begin
     { No hace nada }
end;

procedure TRegistroDepositoRetiro_DevEx.Borrar;
begin
     { No hace nada }
end;


{ Eventos de controles }

procedure TRegistroDepositoRetiro_DevEx.BtnSiguienteClick(Sender: TObject);
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
        CM_CHEQUE.Valor := dmCajaAhorro.SiguienteCheque( CT_CODIGO.Llave );
end;

procedure TRegistroDepositoRetiro_DevEx.ImprimirForma;
begin
     inherited;
     //ZImprimeForma.ImprimeUnaForma( enPrestamo, dmCajaAhorro.cdsRegPrestamos );
end;

function TRegistroDepositoRetiro_DevEx.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TRegistroDepositoRetiro_DevEx.CM_DEP_RETClick(Sender: TObject);
begin
     inherited;
     CM_CHEQUE.Enabled := ( CM_DEP_RET.ItemIndex = 1 );
     lbCheque.Enabled := CM_CHEQUE.Enabled;
     BtnSiguiente_DevEx.Enabled := CM_CHEQUE.Enabled;
end;

procedure TRegistroDepositoRetiro_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
end;

end.
