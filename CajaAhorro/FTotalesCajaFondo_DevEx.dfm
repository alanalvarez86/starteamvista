inherited TotalesCajaFondo_DevEx: TTotalesCajaFondo_DevEx
  Left = 374
  Top = 153
  Caption = 'Totales de caja / fondo'
  ClientHeight = 517
  ClientWidth = 652
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 652
    inherited ValorActivo2: TPanel
      Width = 393
      inherited textoValorActivo2: TLabel
        Width = 387
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 652
    Height = 130
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 40
      Top = 11
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Socios activos:'
    end
    object Label2: TLabel
      Left = 40
      Top = 35
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total ahorrado:'
    end
    object Label3: TLabel
      Left = 23
      Top = 59
      Width = 89
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pr'#233'stamos activos:'
    end
    object Label4: TLabel
      Left = 16
      Top = 83
      Width = 96
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo de prestamos:'
    end
    object SOCIOS_ACTIVOS: TZetaDBTextBox
      Left = 115
      Top = 7
      Width = 151
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SOCIOS_ACTIVOS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CUANTOS_SOCIOS'
      DataSource = DataSource
      FormatFloat = '%14n'
      FormatCurrency = '%m'
    end
    object TOTAL_AHORRADO: TZetaDBTextBox
      Left = 115
      Top = 31
      Width = 151
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'TOTAL_AHORRADO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TOTAL_AHORRADO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PRESTAMOS_ACTIVOS: TZetaDBTextBox
      Left = 115
      Top = 55
      Width = 151
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'PRESTAMOS_ACTIVOS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'NUM_PRESTAMOS'
      DataSource = DataSource
      FormatFloat = '%14n'
      FormatCurrency = '%m'
    end
    object SALDO_PRESTAMOS: TZetaDBTextBox
      Left = 115
      Top = 79
      Width = 151
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SALDO_PRESTAMOS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SALDO_PRESTAMOS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object labels: TLabel
      Left = 44
      Top = 107
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo bancos:'
    end
    object SALDO_BANCOS: TZetaDBTextBox
      Left = 115
      Top = 103
      Width = 151
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SALDO_BANCOS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SALDO_BANCOS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 149
    Width = 652
    Height = 368
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsCuentas
      object CT_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CT_CODIGO'
        MinWidth = 70
        Width = 70
      end
      object CT_NUM_CTA: TcxGridDBColumn
        Caption = 'N'#250'mero de cuenta'
        DataBinding.FieldName = 'CT_NUM_CTA'
        MinWidth = 120
        Width = 120
      end
      object CT_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre de cuenta'
        DataBinding.FieldName = 'CT_NOMBRE'
        MinWidth = 125
        Width = 125
      end
      object CT_BANCO: TcxGridDBColumn
        Caption = 'Banco'
        DataBinding.FieldName = 'CT_BANCO'
        MinWidth = 70
      end
      object SALDO_HOY: TcxGridDBColumn
        Caption = 'Saldo al d'#237'a'
        DataBinding.FieldName = 'SALDO_HOY'
        MinWidth = 90
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 568
    Top = 24
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsCuentas: TDataSource
    Left = 600
    Top = 27
  end
end
