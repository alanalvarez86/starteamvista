inherited CtasBancarias: TCtasBancarias
  Left = 358
  Top = 279
  Caption = 'Cuentas bancarias'
  ClientWidth = 624
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 624
    inherited ValorActivo2: TPanel
      Width = 365
    end
  end
  object GridCuentas: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 624
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CT_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 70
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CT_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 180
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CT_NUM_CTA'
        Title.Caption = 'N'#250'mero'
        Width = 120
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CT_BANCO'
        Title.Caption = 'Banco'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CT_STATUS'
        Title.Caption = 'Status'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Ahorro'
        Width = 180
        Visible = True
      end>
  end
end
