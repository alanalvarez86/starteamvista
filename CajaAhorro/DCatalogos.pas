unit DCatalogos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerCatalogos,
{$else}
     Catalogos_TLB,
{$endif}
     ZetaClientDataSet;

type
  TdmCatalogos = class(TDataModule)
    cdsCondiciones: TZetaLookupDataSet;
    cdsPuestos: TZetaLookupDataSet;
    cdsTurnos: TZetaLookupDataSet;
    cdsClasifi: TZetaLookupDataSet;
    cdsHorarios: TZetaLookupDataSet;
    cdsConceptos: TZetaLookupDataSet;
    cdsNomParamLookUp: TZetaLookupDataSet;
    cdsConceptosLookup: TZetaLookupDataSet;
    cdsNomParam: TZetaLookupDataSet;
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosAlAdquirirDatos(Sender: TObject);
    procedure cdsTurnosAlAdquirirDatos(Sender: TObject);
    procedure cdsClasifiAlAdquirirDatos(Sender: TObject);
    procedure cdsHorariosAlAdquirirDatos(Sender: TObject);
    procedure cdsConceptosAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTurnosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsClasifiGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsHorariosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsCondicionesGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsConceptosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    //DevEx: Se agrego este metodo al cds pues en la vista 2013 este grid no carga Conceptos
    procedure cdsConceptosLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
{$else}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
{$endif}
  public
    { Public declarations }
  end;

var
  dmCatalogos: TdmCatalogos;

implementation

uses DCliente, zAccesosTress, ZAccesosMgr;

{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsCondicionesGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights:= FALSE;
end;

procedure TdmCatalogos.cdsPuestosAlAdquirirDatos(Sender: TObject);
begin
     cdsPuestos.Data := ServerCatalogo.GetPuestos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsPuestosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights:= FALSE;
end;

procedure TdmCatalogos.cdsTurnosAlAdquirirDatos(Sender: TObject);
begin
     cdsTurnos.Data := ServerCatalogo.GetTurnos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsTurnosGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights:= FALSE;
end;

procedure TdmCatalogos.cdsClasifiAlAdquirirDatos(Sender: TObject);
begin
     cdsClasifi.Data := ServerCatalogo.GetClasifi( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsClasifiGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights:= FALSE;
end;

procedure TdmCatalogos.cdsHorariosAlAdquirirDatos(Sender: TObject);
begin
     cdsHorarios.Data := ServerCatalogo.GetHorarios( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsHorariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights:= FALSE;
end;

procedure TdmCatalogos.cdsConceptosAlAdquirirDatos(Sender: TObject);
begin
     cdsConceptos.Data := ServerCatalogo.GetConceptos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsConceptosGetRights(Sender: TZetaClientDataSet;   const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights:= FALSE;
end;


//DevEx: Se agrego este metodo al cds pues en la vista 2013 este cds no carga Conceptos. Este cambio se dejo para ambas vistas
procedure TdmCatalogos.cdsConceptosLookupAlAdquirirDatos(Sender: TObject);
begin
      with cdsConceptos do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque la transferencia
          begin                          // de Data regresa el cds a dsBrowse
               Conectar;
               cdsConceptosLookUp.Data := Data;
          end;
     end;

end;

//DevEx: Se agrego este metodo al cds pues en la vista 2013 cdsno carga Parametros. Este cambio se dejo para ambas vistas
procedure TdmCatalogos.cdsNomParamAlAdquirirDatos(Sender: TObject);
begin
     cdsNomParam.Data := ServerCatalogo.GetNomParam( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsNomParam do
     begin
          if ( State <> dsInsert ) then // se condiciona porque la transferencia
          begin                         // de Data regresa el cds a dsBrowse
               Conectar;
               cdsNomParamLookUp.Data := Data;
          end;
     end;
end;

procedure TdmCatalogos.cdsNomParamGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_CAT_NOMINA_PARAMETROS, iRight );
end;

end.
