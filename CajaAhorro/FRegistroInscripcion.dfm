inherited RegistroInscripcion: TRegistroInscripcion
  Left = 394
  Top = 399
  Caption = 'Inscribir en ahorro'
  ClientHeight = 227
  ClientWidth = 436
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLbl: TLabel [0]
    Left = 34
    Top = 60
    Width = 50
    Height = 13
    Caption = '&Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 85
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de Inicio:'
  end
  object Label18: TLabel [2]
    Left = 25
    Top = 108
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo inicial:'
  end
  inherited PanelBotones: TPanel
    Top = 191
    Width = 436
    TabOrder = 6
    inherited OK: TBitBtn
      Left = 268
    end
    inherited Cancelar: TBitBtn
      Left = 353
    end
  end
  inherited PanelSuperior: TPanel
    Width = 436
    TabOrder = 0
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 436
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 110
    end
  end
  object CB_CODIGO: TZetaDBKeyLookup [6]
    Left = 86
    Top = 56
    Width = 340
    Height = 21
    TabOrder = 2
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object AH_FECHA: TZetaDBFecha [7]
    Left = 86
    Top = 80
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '17/Dic/97'
    Valor = 35781.000000000000000000
    DataField = 'AH_FECHA'
    DataSource = DataSource
  end
  object AH_SALDO_I: TZetaDBNumero [8]
    Left = 86
    Top = 104
    Width = 115
    Height = 21
    Mascara = mnPesos
    TabOrder = 4
    Text = '0.00'
    DataField = 'AH_SALDO_I'
    DataSource = DataSource
  end
  object GroupBox1: TGroupBox [9]
    Left = 8
    Top = 128
    Width = 417
    Height = 57
    Caption = ' F'#243'rmula: '
    TabOrder = 5
    object lbFormula: TLabel
      Left = 32
      Top = 28
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object AH_FORMULA: TDBEdit
      Left = 78
      Top = 24
      Width = 320
      Height = 21
      DataField = 'AH_FORMULA'
      DataSource = DataSource
      TabOrder = 2
    end
    object cbPorcentaje: TCheckBox
      Left = 54
      Top = -1
      Width = 73
      Height = 17
      Caption = 'Porcentaje'
      TabOrder = 0
      OnClick = cbPorcentajeClick
    end
    object nPorcentaje: TZetaNumero
      Left = 78
      Top = 24
      Width = 121
      Height = 21
      Mascara = mnTasa
      TabOrder = 1
      Text = '0.0 %'
    end
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 57
  end
end
