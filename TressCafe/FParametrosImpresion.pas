unit FParametrosImpresion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ZetaEdit, ZetaKeyCombo, Mask,
  ZetaFecha, ComCtrls, ZetaNumero, CheckLst, ZBaseDlgModal, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCheckListBox, Vcl.ImgList,
  cxButtons;

type
  TParametrosImpresion = class(TZetaDlgModal_DevEx)
    Label1: TLabel;
    lbFechaInicial: TLabel;
    EFechaInicial: TZetaFecha;
    EFechaFinal: TZetaFecha;
    lbFechaFinal: TLabel;
    ComidasLBL: TLabel;
    ListaTipos_DevEx: TcxCheckListBox;
    CBTipoImpresion_DevEx: TcxComboBox;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ListaTiposClick(Sender: TObject);
  private
    procedure SetValoresDefault;
    procedure ImprimeListado;
    procedure LlenaLista;{OP: 21.Mayo.08}
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ParametrosImpresion: TParametrosImpresion;

implementation

uses FReportesCafe, DCafeCliente, ZetaDialogo, ZetaRegistryCafe, ZetaCommonTools,
     ZetaCommonClasses, ZetaHelpCafe;

{$R *.DFM}

procedure TParametrosImpresion.SetValoresDefault;
var
   i: integer;
begin
     // CBTipoImpresion.ItemIndex := 0;
     CBTipoImpresion_DevEx.ItemIndex := 0;
     EFechaInicial.Valor := DATE;
     EFechaFinal.Valor := DATE;
     for i:= 0 to ListaTipos_DevEx.Items.Count -1 do
         ListaTipos_DevEx.Items[i].Checked := TRUE;
end;

procedure TParametrosImpresion.FormCreate(Sender: TObject);
begin
     inherited;
     SetValoresDefault;
     HelpContext := H00010_Parametros_de_impresion;
     LlenaLista; {OP: 21.Mayo.08}
end;

procedure TParametrosImpresion.ImprimeListado;

 function GetTipoComida : string;
 var
    i: integer;
 begin
      Result := '';
      with ListaTipos_DevEx do
      begin
           for i := 0 to ( Items.Count - 1 ) do
           begin
                if Items[i].Checked then
                   Result := Result + IntToStr( i + 1 )+',';
           end;
      end;
      Result := ZetaCommonTools.CortaUltimo( Result );
      if StrVacio( Result ) then
      begin
           ZError(Caption, 'No se Especific� Ning�n Tipo de Comida', 0);
           ListaTipos_DevEx.SetFocus;
           Abort;
      end;
 end;

begin
     with dmCafeCliente do
     begin
          ImprimeListado( CafeRegistry.Identificacion,
                          GetTipoComida,
                          EFechaInicial.Valor,
                          EFechaFinal.Valor,
                          CBTipoImpresion_DevEx.ItemIndex=1 );
          with TReportesCafe.Create do
               Imprime(dsDetalle.DataSet,dsTotales.DataSet);
     end;
end;

procedure TParametrosImpresion.OKClick(Sender: TObject);
begin
     inherited;
     ImprimeListado;
end;

procedure TParametrosImpresion.ListaTiposClick(Sender: TObject);
begin
  inherited;

end;

{OP: 21.Mayo.08}
procedure TParametrosImpresion.LlenaLista;
var
   iContador : Integer;
begin
     for iContador := 0 to ListaTipos_DevEx.Items.Count - 1 do
     begin
          with ListaTipos_DevEx do
               Items[ iContador ].Text := ZetaRegistryCafe.CafeRegistry.TextoTipoComida( iContador + 1 );
     end;
end;

end.
