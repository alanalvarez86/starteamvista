unit FConfigura;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, Registry,
  Grids, DBGrids, Db, FileCtrl, ZBaseDlgModal_DevEx, ZetaDBGrid, ZetaKeyCombo, ZetaMessages, Mask, ZetaNumero, ZetaEdit, OleCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxBarBuiltInMenu, cxControls, cxContainer, cxEdit, cxGroupBox,
  dxCheckGroupBox, cxTrackBar, cxSpinEdit, cxSpinButton, cxCheckBox, cxMaskEdit,
  cxDropDownEdit, cxTextEdit, cxPC, Vcl.ImgList, cxButtons, SFE, ZetaHora, ZetaDBTextBox, ZetaCommonClasses;

type
  TFormaConfigura = class(TZetaDlgModal_DevEx)
    Configuracion_DevEx: TcxPageControl;
    General_DevEx: TcxTabSheet;
    Calendario_DevEx: TcxTabSheet;
    TipoComidaTS_DevEx: TcxTabSheet;
    Server_DevEx: TcxTabSheet;
    Seguridad_DevEx: TcxTabSheet;
    Impresora_DevEx: TcxTabSheet;
    Sonidos_DevEx: TcxTabSheet;
    Serial_DevEx: TcxTabSheet;
    Biometrico_DevEx: TcxTabSheet;
    PanelGeneral: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    lblTipoComida: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    editID: TcxTextEdit;
    cbTipoGafete: TZetaKeyCombo;
    cbLectorProximidad: TZetaKeyCombo;
    DefaultGB: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    Letrero: TGroupBox;
    Label3: TLabel;
    GridRenglones: TZetaDBGrid;
    pCalendario: TPanel;
    Label8: TLabel;
    pBotones: TPanel;
    pEventos: TPanel;
    Label29: TLabel;
    cbSemana: TZetaKeyCombo;
    ZCombo: TZetaDBKeyCombo;
    Label22: TLabel;
    Label23: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    FueraLinea: TGroupBox;
    LblArchivo: TLabel;
    LblMessFuera: TLabel;
    EditArchivo: TcxTextEdit;
    EditMessOK: TcxTextEdit;
    Servidor: TGroupBox;
    Label6: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    lblSegundos: TLabel;
    editDireccion: TcxTextEdit;
    editPuerto: TZetaNumero;
    editIntervalo: TZetaNumero;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    cbConcesionImprimir: TcxCheckBox;
    cbConcesionCancelar: TcxCheckBox;
    LblPuerto: TLabel;
    EditImpresora: TcxTextEdit;
    Codigos: TGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    EditCodigoInicial: TcxTextEdit;
    EditCodigoFinal: TcxTextEdit;
    EditEject: TcxTextEdit;
    GroupBox1: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    EAcepta: TcxTextEdit;
    ERechazo: TcxTextEdit;
    CBPuertoB: TZetaKeyCombo;
    CBPuertoA: TZetaKeyCombo;
    GBCodigosApagados: TGroupBox;
    LblCodigoApagadoRechazo: TLabel;
    LblCodigoApagadoAceptado: TLabel;
    LblEsperaAceptado: TLabel;
    LblEsperaRechazado: TLabel;
    EditApagadoRechazo: TZetaEdit;
    EditApagadoAceptado: TZetaEdit;
    EditEsperaAceptado: TZetaNumero;
    EditEsperaRechazo: TZetaNumero;
    GBCodigosSalida: TGroupBox;
    LblCodigoAcepta: TLabel;
    LblCodigoRechazo: TLabel;
    EditSerialAcepta: TcxTextEdit;
    EditSerialRechazo: TcxTextEdit;
    Label14: TLabel;
    Label13: TLabel;
    GroupBox3: TGroupBox;
    Label33: TLabel;
    PathTemplates: TcxTextEdit;
    OpenDialogSonidos: TOpenDialog;
    OpenDialog: TOpenDialog;
    tbVelocidad_DevEx: TcxTrackBar;
    Agregar: TcxButton;
    Borrar: TcxButton;
    Modificar: TcxButton;
    ArchivoSeekCalendario_DevEx: TcxButton;
    EditCalendario_DevEx: TcxTextEdit;
    ArchivoSeek_DevEx: TcxButton;
    bSonidoAceptacion: TcxButton;
    cxButton1: TcxButton;
    btnBrowseDirectoryBiometrico_DevEx: TcxButton;
    BtnConfigA_DevEx: TcxButton;
    BtnConfigB_DevEx: TcxButton;
    bPUERTO_DevEx: TcxButton;
    cxImageList19_PanelBotones: TcxImageList;
    Label32: TLabel;
    Label36: TLabel;
    GroupBox4: TGroupBox;
    Label37: TLabel;
    cbChecadaManual_BK: TZetaKeyCombo;
    numChecadaManualTimer: TZetaNumero;
    Label38: TLabel;
    Label39: TLabel;
    cbChecadaManual: TcxComboBox;
    gbRegenerarArchivoHuellas: TcxGroupBox;
    RecargaHuellas_DevEx: TcxButton;
    lblAutomatico: TLabel;
    hrRegenerarArchivoHuellas: TZetaHora;
    lblFormato24: TLabel;
    lblManual: TLabel;
    lblHoraRegenerarArchivoHuellas: TLabel;
    GroupBox5: TGroupBox;
    SincronizarConfiguracion: TcxButton;
    lblHora: TLabel;
    editSincronizacion: TZetaHora;
    lblFormato: TLabel;
    zTipoComida: TZetaTextBox;
    zSimultaneas: TZetaTextBox;
    zTipoComida1: TZetaTextBox;
    zTipoComida2: TZetaTextBox;
    zTipoComida3: TZetaTextBox;
    zTipoComida4: TZetaTextBox;
    zTipoComida5: TZetaTextBox;
    zTipoComida6: TZetaTextBox;
    zTipoComida7: TZetaTextBox;
    zTipoComida8: TZetaTextBox;
    zTipoComida9: TZetaTextBox;
    zBanner: TZetaTextBox;
    zTiempoEspera: TZetaTextBox;
    zGafeteAdmon: TZetaTextBox;
    UsaTeclado: TPanel;
    zEmpresa: TZetaTextBox;
    zCredencial: TZetaTextBox;
    cbUsaTeclado: TdxCheckGroupBox;
    cbTipoComida: TcxCheckBox;
    cbCantidad: TcxCheckBox;
    Vista: TGroupBox;
    cbMostrarFoto: TcxCheckBox;
    cbMostrarComidas: TcxCheckBox;
    cbMostrarSigComida: TcxCheckBox;
    chkReiniciaMediaNoche: TcxCheckBox;
    rbUnaVez: TRadioButton;
    rbFrecuencia: TRadioButton;
    editMinutos: TZetaNumero;
    lblCada: TLabel;
    lblMinutos: TLabel;

    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure bPUERTOClick(Sender: TObject);
    procedure ConfiguracionChange(Sender: TObject);
    procedure CBPuertoAChange(Sender: TObject);
    procedure BtnConfigAClick(Sender: TObject);
    procedure editTipoChange(Sender: TObject);
    procedure cbSemanaChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    {$IFDEF BOSE}
    procedure BBAgregarContadorClick(Sender: TObject);
    procedure BBBorrarContadorClick(Sender: TObject);
    procedure BBModificarContadorClick(Sender: TObject);
    procedure sbReiniciarContadorClick(Sender: TObject);
    {$ENDIF}
    // BIOMETRICO
    procedure cbTipoGafeteChange(Sender: TObject);
    procedure RecargaHuellasClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SincronizarConfiguracionClick(Sender: TObject);
    procedure rbUnaVezClick(Sender: TObject);
    procedure rbFrecuenciaClick(Sender: TObject);

  private
    { Private declarations }
    FComEntrada : String;
    FComSalida  : String;
    FDrivers    : boolean;
    //SFEStandard1: TSFEStandard; // BIOMETRICO
    function GridEnfocado: Boolean;
    function ExisteDirLecturas(const sArchivo: String): Boolean;
    function EscribeCalendario(const sArchivo: String): Boolean;
    {$IFDEF BOSE}
    function EscribeContador(const sArchivo: String): Boolean;
    {$ENDIF}
    procedure EscribeRegistry;
    procedure SeleccionaSiguienteRenglon;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure HabilitaBtnSeriales;
    procedure CambiaConfigSerial(const iPuerto: Integer; var sConfig: String);
    procedure SetControlesTeclado(const lEnabled: Boolean);
    procedure TextosTipoComida;
    procedure ActualizaLabelTipoComida;              { OP: 22.Mayo.08 }
    procedure CalendarioDeSemana;                    { OP: 23.Mayo.08 }
    procedure ToggleBiometric(lBiometrico: Boolean); // BIOMETRICO
    function HayDispositivo: Boolean;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override; { TWinControl }
  public
    { Public declarations }
    procedure SetControlesServidor;
    procedure SincronizaConfiguracion(sEstacion: String);
    procedure Sincronizar;
    procedure LeeRegistry;
  end;

const
  COL_HORA       = 0;
  COL_ACCION     = 1;
  TAB_CALENDARIO = 1;
  TAB_COMIDAS    = 2;
  TAB_SERVIDOR   = 3;
  TAB_SEGURIDAD  = 4;
  TAB_IMPRESORA  = 5;
  TAB_SONIDOS    = 6;
  TAB_SERIAL     = 7;
  TAB_CONTADOR   = 8;
  K_ADVERTENCIA  = 'El identificador asignado no ha sido agregado a la lista de dispositivos' + CR_LF + 'de sistema TRESS o no ha sido configurado.';

var
  FormaConfigura: TFormaConfigura;

implementation

uses
  FCafeteria, FConfigSerial, DCafeCliente, ZetaRegistryCafe, ZetaWinApiTools,
  ZetaNetworkBrowser_DevEx, ZetaCommonTools, ZetaDialogo,
  ZetaHelpCafe, ZetaCommonLists, DBClient, CafeteraConsts, DateUtils;

{$R *.DFM}

procedure TFormaConfigura.FormCreate(Sender: TObject);
var
  eValor: eChecadaSimultanea;
begin
  inherited;
  {$IFDEF BOSE}
  Contador.TabVisible         := True;
  sbReiniciarContador.OnClick := sbReiniciarContadorClick;
  BBAgregarContador.OnClick   := BBAgregarContadorClick;
  BBBorrarContador.OnClick    := BBBorrarContadorClick;
  BBModificarContador.OnClick := BBModificarContadorClick;
  {$ENDIF}
  Self.HelpContext := H00001_Configuracion_General;
  {with Simultaneas.Properties.Items do
  begin
    BeginUpdate;
    Clear;
    try
      for eValor := Low(eChecadaSimultanea) to High(eChecadaSimultanea) do
      begin
        Add(ZetaRegistryCafe.aSimultaneas[eValor]);
      end;
    finally
      EndUpdate;
    end;
  end;  }
  TextosTipoComida;                              { OP: 21.Mayo.08 }
  cbSemana.ItemIndex       := Ord(dcTodaSemana); { OP: 23.Mayo.08 }
  // Configuracion.ActivePage := General;
  Configuracion_DevEx.ActivePage := General_DevEx;
  dmCafeCliente.adCalendario.Open;
  FDrivers := LoadDLL;
  cbTipoComida.Enabled := False;
  cbCantidad.Enabled := False;
end;

procedure TFormaConfigura.FormShow(Sender: TObject);
begin
     inherited;
     LeeRegistry;
     HabilitaBtnSeriales;
     //SetControlesTeclado(cbUsaTeclado.CheckBox.Checked);
     ActualizaLabelTipoComida; { OP: 22.Mayo.08 }
     CalendarioDeSemana;       { OP: 23.Mayo.08 }

     // En caso de ser Lector de Proximidad Activa o Desactiva el combo de Lector de Proximidad
     // cbLectorProximidad.Enabled := (cbTipoGafete.ItemIndex <> Ord(egBiometrico));
     // Solo se activa si el tipo de gafete seleccionado es de proximidad.
     cbLectorProximidad.Enabled := (cbTipoGafete.ItemIndex = Ord(egProximidad));

     // En caso de ser Lector Biometrico Activa Controles de Configuracion de Biom�trico
     ToggleBiometric((cbTipoGafete.ItemIndex = Ord(egBiometrico)));
     Configuracion_DevEx.ActivePage := General_DevEx;

     cbChecadaManual.ItemIndex := CafeRegistry.ChecadaManual;
     numChecadaManualTimer.Valor := CafeRegistry.ChecadaManualTimer;

     EditCalendario_DevEx.Text := ExtractFilePath( Application.ExeName ) + 'CALENDARIO.DAT';
     EditArchivo.Text := ExtractFilePath( Application.ExeName ) + 'LECTURAS.DAT';
end;

procedure TFormaConfigura.LeeRegistry;
begin
     with CafeRegistry do
     begin
          zTipoComida.Caption          := IntToStr(TipoComida);
          editID.Text                  := Identificacion;
          cbMostrarFoto.Checked        := MostrarFoto;
          cbMostrarComidas.Checked     := MostrarComidas;
          cbMostrarSigComida.Checked   := MostrarSigComida;
          cbUsaTeclado.CheckBox.Checked:= UsaTeclado;
          cbTipoGafete.ItemIndex       := Ord(TipoGafete);
          cbLectorProximidad.ItemIndex := Ord(LectorProximidad);
          zEmpresa.Caption             := DefaultEmpresa;
          zCredencial.Caption          := DefaultCredencial;
          cbTipoComida.Checked         := PreguntaTipo;
          cbCantidad.Checked           := PreguntaQty;
          zBanner.Caption              := Banner;
          tbVelocidad_DevEx.Position   := Velocidad;
          EditCalendario_DevEx.Text          := Calendario;

          cbChecadaManual.ItemIndex    := ChecadaManual;
          numChecadaManualTimer.Valor  := ChecadaManualTimer;

          {$IFDEF BOSE}
          EditContador.Text := ArchivoContador;
          {$ENDIF}
          editDireccion.Text          := Servidor;
          editPuerto.Valor            := Puerto;
          editIntervalo.Valor         := Intervalo;
          zTiempoEspera.Caption       := IntToStr(TiempoEspera);
          EditArchivo.Text            := Archivo;
          EditMessOK.Text             := MensajeOk;
          EditImpresora.Text          := Impresora;
          EditCodigoInicial.Text      := CodigoInicial;
          EditCodigoFinal.Text        := CodigoFinal;
          EditEject.Text              := Eject;
          EAcepta.Text                := SonidoAcepta;
          ERechazo.Text               := SonidoRechazo;
          zGafeteAdmon.Caption        := GafeteAdmon;
          //zClaveAdmon.Caption         := ClaveAdmon;
          cbConcesionImprimir.Checked := ConcesionImprimir;
          cbConcesionCancelar.Checked := ConcesionCancelar;
          CBPuertoA.Valor             := SerialA;
          CBPuertoB.Valor             := SerialB;
          EditSerialAcepta.Text       := CodigoAcepta;
          EditSerialRechazo.Text      := CodigoRechazo;
          FComEntrada                 := ComEntrada;
          FComSalida                  := ComSalida;

          EditApagadoAceptado.Text := ApagadoAcepta;
          EditEsperaAceptado.Valor := EsperaAcepta;
          EditApagadoRechazo.Text  := ApagadoRechazo;
          EditEsperaRechazo.Valor  := EsperaRechazo;
          editSincronizacion.Valor := Sincronizacion;
          editMinutos.Valor        := FrecuenciaMinutos;

          if TipoSincronizacion = K_FRECUENCIA_DIARIA then
             rbUnaVez.Checked := True
          else if TipoSincronizacion = K_FRECUENCIA_MINUTOS then
               rbFrecuencia.Checked := True;

          { OP: 21.Mayo.08 - Nuevos campos de tipo de comida }
          zTipoComida1.Caption          := TipoComida1;
          zTipoComida2.Caption          := TipoComida2;
          zTipoComida3.Caption          := TipoComida3;
          zTipoComida4.Caption          := TipoComida4;
          zTipoComida5.Caption          := TipoComida5;
          zTipoComida6.Caption          := TipoComida6;
          zTipoComida7.Caption          := TipoComida7;
          zTipoComida8.Caption          := TipoComida8;
          zTipoComida9.Caption          := TipoComida9;
          chkReiniciaMediaNoche.Checked := ReiniciaMediaNoche;
          // DES SP: 13301EP III - Recarga de huellas
          hrRegenerarArchivoHuellas.Text := RegenerarArchivoHuellasHora;

          zSimultaneas.Caption {Simultaneas.ItemIndex} :=  aSimultaneas[ChecadaSimultanea];
          if strVacio(SonidoRechazo) and strVacio(Servidor) then
            ERechazo.Text := ZetaWinApiTools.GetWindowsDir + '\MEDIA\CHORD.WAV';

          // ------------------------Biometrico
          PathTemplates.Text := FileTemplates;
     end;
end;

procedure TFormaConfigura.EscribeRegistry;
var
   eValor: eChecadaSimultanea;
   iFrec : Integer;
begin
     with CafeRegistry do
     begin
          iFrec             := CafeRegistry.FrecuenciaMinutos;
          TipoComida        := StrToInt(zTipoComida.Caption);
          Identificacion    := editID.Text;
          MostrarFoto       := cbMostrarFoto.Checked;
          MostrarComidas    := cbMostrarComidas.Checked;
          MostrarSigComida  := cbMostrarSigComida.Checked;
          UsaTeclado        := cbUsaTeclado.CheckBox.Checked;
          TipoGafete        := eTipoGafete(cbTipoGafete.ItemIndex);
          LectorProximidad  := eTipoProximidad(cbLectorProximidad.ItemIndex);
          DefaultEmpresa    := zEmpresa.Caption;
          DefaultCredencial := zCredencial.Caption;
          PreguntaTipo      := cbTipoComida.Checked;
          PreguntaQty       := cbCantidad.Checked;
          Banner            := zBanner.Caption;
          Velocidad         := tbVelocidad_DevEx.Position;
          Servidor          := editDireccion.Text;
          Puerto            := editPuerto.ValorEntero;
          Intervalo         := EditIntervalo.ValorEntero;
          Sincronizacion    := editSincronizacion.Text;
          FrecuenciaMinutos := editMinutos.ValorEntero;

          if rbUnaVez.Checked then
             TipoSincronizacion := K_FRECUENCIA_DIARIA
          else if rbFrecuencia.Checked then
               TipoSincronizacion := K_FRECUENCIA_MINUTOS;

          // Cumplir requisito de valor para editTiempoEspera de m�ximo = 999
          if StrToInt (zTiempoEspera.Caption) > 999 then
              TiempoEspera  := 999
          else
            TiempoEspera    := StrToInt ( zTiempoEspera.Caption );

          ChecadaManual     := cbChecadaManual.ItemIndex;
          ChecadaManualTimer:= numChecadaManualTimer.ValorEntero;

          Calendario        := EditCalendario_DevEx.Text;
          {$IFDEF BOSE}
          ArchivoContador := EditContador.Text;
          {$ENDIF}
          Archivo           := EditArchivo.Text;
          MensajeOk         := EditMessOK.Text;
          Impresora         := EditImpresora.Text;
          CodigoInicial     := EditCodigoInicial.Text;
          CodigoFinal       := EditCodigoFinal.Text;
          Eject             := EditEject.Text;
          SonidoAcepta      := EAcepta.Text;
          SonidoRechazo     := ERechazo.Text;
          GafeteAdmon       := zGafeteAdmon.Caption;
          //ClaveAdmon        := zClaveAdmon.Caption;
          ConcesionImprimir := cbConcesionImprimir.Checked;
          ConcesionCancelar := cbConcesionCancelar.Checked;
          SerialA           := CBPuertoA.Valor;
          SerialB           := CBPuertoB.Valor;
          CodigoAcepta      := EditSerialAcepta.Text;
          CodigoRechazo     := EditSerialRechazo.Text;
          ComEntrada        := FComEntrada;
          ComSalida         := FComSalida;

          ApagadoAcepta  := EditApagadoAceptado.Text;
          EsperaAcepta   := EditEsperaAceptado.ValorEntero;
          ApagadoRechazo := EditApagadoRechazo.Text;
          EsperaRechazo  := EditEsperaRechazo.ValorEntero;

          { OP: 21.Mayo.08 - Nuevos campos de tipo de comida }
          TipoComida1        := zTipoComida1.Caption;
          TipoComida2        := zTipoComida2.Caption;
          TipoComida3        := zTipoComida3.Caption;
          TipoComida4        := zTipoComida4.Caption;
          TipoComida5        := zTipoComida5.Caption;
          TipoComida6        := zTipoComida6.Caption;
          TipoComida7        := zTipoComida7.Caption;
          TipoComida8        := zTipoComida8.Caption;
          TipoComida9        := zTipoComida9.Caption;
          ReiniciaMediaNoche := chkReiniciaMediaNoche.Checked;
          for eValor := Low(eChecadaSimultanea) to High(eChecadaSimultanea) do
          begin
               if aSimultaneas[eValor] = zSimultaneas.Caption then
               begin
                    ChecadaSimultanea := eValor;
                    break;
               end;
          end;
          // DES SP: 13301EP III - Recarga de huellas
          RegenerarArchivoHuellasHora := hrRegenerarArchivoHuellas.Text;

          {
            with dmCafeCliente do
            begin
            ProhibirAcceso := FALSE;
            ValidarEmpleado := TRUE;
            end;
          }

          // Biometrico
          FileTemplates := PathTemplates.Text;

          if TipoSincronizacion = K_FRECUENCIA_MINUTOS then
          begin
               if editMinutos.ValorEntero <> iFrec then
                  dmCafeCliente.HoraSincroniza := IncMinute( Now, editMinutos.ValorEntero);
          end;
     end;
end;

procedure TFormaConfigura.OK_DevExClick(Sender: TObject);
const
K_MIN_LIMPIAR_PANTALLA = 3;
var
  sHora, sEstacion: String;

  procedure Success;
  begin
       EscribeRegistry;
       if Not dmCafeCliente.BuscaCafeteria(sEstacion) and dmCafeCliente.Active then
       begin
            ZWarning( '�Advertencia!', K_ADVERTENCIA, 0, mbOk);
            FormaCafe.ShowTipoConexion(K_CAFETERA_PENDIENTE_TEXT);
       end;
       ModalResult := mrOk;
  end;

begin
     inherited;
     sEstacion := editID.Text;
     sHora := editSincronizacion.Text;

     if ( rbFrecuencia.Checked ) then
     begin
          if ( editMinutos.Valor > 1440 ) then
          begin
               ZetaDialogo.ZError(Self.Caption,'El valor m�ximo permitido en minutos es 1440!', 0);
               ActiveControl := editMinutos;
               ModalResult := mrNone;
          end
          else if ( editMinutos.Valor < 1 ) then
          begin
               ZetaDialogo.ZError(Self.Caption,'El valor m�nimo permitido en minutos es 1', 0);
               ActiveControl := editMinutos;
               ModalResult := mrNone;
          end;
     end
     else if ( rbUnaVez.Checked ) and (Not ZetaCommonTools.ValidaHora(sHora, '48')) then
     begin
          ZetaDialogo.ZError(Self.Caption, 'Hora de sincronizaci�n Inv�lida', 0);
          ActiveControl := editSincronizacion;
          ModalResult := mrNone;
     end;

     if ModalResult <> mrNone then
     begin

          if strVacio(editDireccion.Text) then
          begin
               ZetaDialogo.ZError(Self.Caption,'�No puede quedar vaci� el servidor!', 0);
               ModalResult := mrNone;
               SetControlesServidor;
          end
          else if StrToInt (zTiempoEspera.Caption) < K_MIN_LIMPIAR_PANTALLA then
          begin
               ZetaDialogo.ZError(Self.Caption, Format('El valor para limpiar pantalla no debe ser menor a %d segundos', [K_MIN_LIMPIAR_PANTALLA]), 0);
               // SetControlesServidor;
          end
          else if EscribeCalendario(EditCalendario_DevEx.Text) and
          {$IFDEF BOSE}
          EscribeContador(EditContador.Text) and
          {$ENDIF}
          ExisteDirLecturas(EditArchivo.Text) then
          begin
               if (cbTipoGafete.ItemIndex = Ord(egBiometrico)) then
               begin
                    if (not HayDispositivo) then
                    begin
                         ZetaDialogo.ZError(Self.Caption,'�Es necesario que tenga un dispositivo biom�trico conectado!', 0);
                         cbTipoGafete.ItemIndex := 0;
                    end
                    else if (strVacio(Trim(PathTemplates.Text))) then
                    begin
                         ZetaDialogo.ZError(Self.Caption, '�Es necesario que tenga configurado el almacenamiento local de las huellas!', 0);
                    end
                    else if numChecadaManualTimer.Enabled and ((numChecadaManualTimer.ValorEntero < 3) or (numChecadaManualTimer.ValorEntero > 59)) then
                    begin
                         ZetaDialogo.ZError(Self.Caption, 'Tiempo l�mite para checada adicional debe ser un n�mero entre 3 y 59', 0);
                    end
                    else
                    begin
                         Success;
                    end;
               end
               else
               begin
                    Success;
               end;
          end;
     end;
end;

procedure TFormaConfigura.rbFrecuenciaClick(Sender: TObject);
begin
     inherited;
     lblHora.Enabled := rbUnaVez.Checked;
     editSincronizacion.Enabled := rbUnaVez.Checked;
     lblFormato.Enabled := rbUnaVez.Checked;
     lblCada.Enabled := rbFrecuencia.Checked;
     editMinutos.Enabled := rbFrecuencia.Checked;
     lblMinutos.Enabled := rbFrecuencia.Checked;
end;

procedure TFormaConfigura.rbUnaVezClick(Sender: TObject);
begin
     inherited;
     lblHora.Enabled := rbUnaVez.Checked;
     editSincronizacion.Enabled := rbUnaVez.Checked;
     lblFormato.Enabled := rbUnaVez.Checked;
     lblCada.Enabled := rbFrecuencia.Checked;
     editMinutos.Enabled := rbFrecuencia.Checked;
     lblMinutos.Enabled := rbFrecuencia.Checked;
end;

function TFormaConfigura.ExisteDirLecturas(const sArchivo: String): Boolean;
var
  sFolder: String;
begin
  sFolder := dmCafeCliente.GetFolderLecturas(sArchivo);
  Result  := (strVacio(sFolder)) or (DirectoryExists(sFolder));
  if (not Result) then begin
    ZetaDialogo.ZError(Self.Caption, '�Folder no v�lido para almacenar archivo ' + 'de lecturas!' + CR_LF + CR_LF +
        'Verifique el disco/folder seleccionado', 0);
    Configuracion_DevEx.ActivePage := Self.Server_DevEx;
    Self.ActiveControl       := EditArchivo;
  end;
end;

function TFormaConfigura.EscribeCalendario(const sArchivo: String): Boolean;
var
  sError: String;
begin
  Result := True;
  with dmCafeCliente.adCalendario do begin
    if State in [dsInsert, dsEdit] then
      Post;
    if strLleno(sArchivo) then begin
      try
        SaveToFile(sArchivo, dfXMLUTF8);
      except
        on Error: Exception do begin
          sError := '�No se pudo grabar el calendario!' + CR_LF + CR_LF + 'Verifique que se tengan derechos de lectura/escritura'
            + CR_LF + 'sobre el disco/folder seleccionado';
          Result := FALSE;
        end;
      end;
    end else if (RecordCount > 0) then begin
      sError := 'Falta especificar el archivo en que se almacenar� el calendario';
      Result := FALSE;
    end;
  end;
  if (not Result) then begin
    ZetaDialogo.ZError(Self.Caption, sError, 0);
    Configuracion_DevEx.ActivePage := Self.Calendario_DevEx;
    Self.ActiveControl       := EditCalendario_DevEx;
  end;
end;

{$IFDEF BOSE}

function TFormaConfigura.EscribeContador(const sArchivo: String): Boolean;
var
  sError: String;
begin
  Result := True;
  with dmCafeCliente.adContador do begin
    if State in [dsInsert, dsEdit] then
      Post;
    if strLleno(sArchivo) then begin
      try
        SaveToFile(sArchivo, dfXMLUTF8);
      except
        on Error: Exception do begin
          sError := '�No se pudo grabar el archivo de para reiniciar contador!' + CR_LF + CR_LF +
            'Verifique que se tengan derechos de lectura/escritura' + CR_LF + 'sobre el disco/folder seleccionado';
          Result := FALSE;
        end;
      end;
    end else if (RecordCount > 0) then begin
      sError := 'Falta especificar el archivo en que se almacenaran los tiempos de reiniciar el contador';
      Result := FALSE;
    end;
  end;
  if (not Result) then begin
    ZetaDialogo.ZError(Self.Caption, sError, 0);
    Configuracion.ActivePage := Self.Contador;
    Self.ActiveControl       := EditContador;
  end;
end;
{$ENDIF}
{ Seleccion de Archivos }

procedure TFormaConfigura.ArchivoSeekClick(Sender: TObject);
var
  sArchivo: String;
  oEdit   : TcxTextEdit;
  oDialog : TOpenDialog;
begin
  case TControl(Sender).Tag of
    0: begin
        oEdit   := EditArchivo;
        oDialog := OpenDialog;
      end;
    1: begin
        oEdit   := EditCalendario_DevEx;
        oDialog := OpenDialog;
      end;
    2: begin
        oEdit   := EAcepta;
        oDialog := OpenDialogSonidos;
      end;
    3: begin
        oEdit   := ERechazo;
        oDialog := OpenDialogSonidos;
      end;
    5: begin
        oEdit   := PathTemplates;
        oDialog := OpenDialog;
      end;
    {$IFDEF BOSE}
    4: begin
        oEdit   := EditContador;
        oDialog := OpenDialog;
      end;
    {$ENDIF}
    else begin
        oEdit   := nil;
        oDialog := nil;
      end;

  end;
  sArchivo := oEdit.Text;
  with oDialog do begin
    FileName   := ExtractFileName(sArchivo);
    InitialDir := ExtractFilePath(sArchivo);
    if (oEdit.Tag in [2, 3]) and strVacio(InitialDir) then
      InitialDir := ZetaWinApiTools.GetWinSysDir;

    if Execute then
      oEdit.Text := FileName;
  end;
end;

{ Control de Calendario }

procedure TFormaConfigura.BBAgregarClick(Sender: TObject);
begin
  dmCafeCliente.adCalendario.Append;
  GridRenglones.SetFocus;
end;

procedure TFormaConfigura.BBModificarClick(Sender: TObject);
begin
  with dmCafeCliente.adCalendario do
    if not IsEmpty then
      Edit;
  GridRenglones.SetFocus;
end;

procedure TFormaConfigura.BBBorrarClick(Sender: TObject);
begin
  with dmCafeCliente.adCalendario do
    if not IsEmpty then
      Delete;
end;

{ Control de Grid de Calendario }

function TFormaConfigura.GridEnfocado: Boolean;
begin
  Result := (ActiveControl = GridRenglones);
end;

procedure TFormaConfigura.GridRenglonesDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if (gdFocused in State) then begin
    if (Column.FieldName = 'ACCION') then begin
      with ZCombo do begin
        Left    := Rect.Left + GridRenglones.Left;
        Top     := Rect.Top + GridRenglones.Top;
        Width   := Column.Width + 2;
        Visible := True;
      end;
    end;
  end;
end;

procedure TFormaConfigura.GridRenglonesColExit(Sender: TObject);
begin
  inherited;
  with GridRenglones do begin
    if (SelectedField <> nil) and (SelectedField.FieldName = 'ACCION') and (ZCombo.Visible) then
      ZCombo.Visible := FALSE;
  end;
end;

procedure TFormaConfigura.WMExaminar(var Message: TMessage);
begin
  if (TExaminador(message.LParam) = exEnter) then
    if GridEnfocado then begin
      if (GridRenglones.SelectedIndex = COL_HORA) then
        GridRenglones.SelectedIndex := COL_ACCION
      else
        SeleccionaSiguienteRenglon;
    end;
end;

procedure TFormaConfigura.KeyDown(var Key: Word; Shift: TShiftState);
begin
  if (GridEnfocado) and (Key = VK_RETURN) then
    Key := 0;
  inherited KeyDown(Key, Shift);
end;

procedure TFormaConfigura.KeyPress(var Key: Char);
begin
  if (GridEnfocado) then begin
    if (Key = Chr(VK_RETURN)) then begin
      Key := #0;
      if (GridRenglones.SelectedIndex = COL_HORA) then
        GridRenglones.SelectedIndex := COL_ACCION
      else
        SeleccionaSiguienteRenglon;
    end else if (Key = Chr(VK_ESCAPE)) then begin
      GridRenglones.SelectedIndex := COL_HORA;
    end else if ((Key <> Chr(9)) and (Key <> #0) and (GridRenglones.SelectedIndex = COL_ACCION)) then begin
      ZCombo.SetFocus;
      SendMessage(ZCombo.Handle, WM_Char, Word(Key), 0);
      Key := #0;
    end
  end else if (ActiveControl = ZCombo) and ((Key = Chr(VK_RETURN)) or (Key = Chr(9))) then begin
    Key := #0;
    with GridRenglones do begin
      GridRenglones.SelectedIndex := COL_HORA;
      SeleccionaSiguienteRenglon;
    end;
  end
  else
    inherited KeyPress(Key);
end;

procedure TFormaConfigura.SeleccionaSiguienteRenglon;
begin
  with dmCafeCliente.adCalendario do begin
    Next;
    if EOF then
      Append;
  end;
  GridRenglones.SelectedIndex := COL_HORA;
end;

procedure TFormaConfigura.bPUERTOClick(Sender: TObject);
begin
  inherited;
  EditImpresora.Text := ZetaNetworkBrowser_DevEx.GetPrinterPort(EditImpresora.Text);
end;

procedure TFormaConfigura.SincronizarConfiguracionClick(Sender: TObject);
var sEstacion, sHora : String;

begin
     sEstacion := editID.Text;
     if Not dmCafeCliente.BuscaCafeteria(sEstacion) and CafeRegistry.ConfiguracionEnviada then
     begin
          ZWarning( '�Advertencia!', K_ADVERTENCIA, 0, mbOk);
          FormaCafe.ShowTipoConexion(K_CAFETERA_PENDIENTE_TEXT);
     end
     else
     begin
          Sincronizar;
          ActualizaLabelTipoComida;
     end;
end;

procedure TFormaConfigura.Sincronizar;
begin
     CafeRegistry.Identificacion := editID.Text;
     FormaCafe.WizardConfiguracion;
     if FormaCafe.LblConectado.Caption = ESTADO_CONEXION[K_CAFETERA_EN_LINEA_TEXT] then
          CafeRegistry.ConfiguracionEnviada := True;

     LeeRegistry;
     FormaCafe.ActualizaParametros;
end;

procedure TFormaConfigura.SincronizaConfiguracion(sEstacion: String);
var sCalend : string;
begin
     sCalend := ExtractFilePath( Application.ExeName ) + 'CALENDARIO.DAT';
     FormaCafe.ShowTipoConexion(K_CAFETERA_FUERA_LINEA_TEXT);
     if Not dmCafeCliente.SincronizarCafeteria(sEstacion) then
     begin
          if dmCafeCliente.Active then
             FormaCafe.ShowTipoConexion(K_CAFETERA_PENDIENTE_TEXT)
     end
     else
     begin
          FormaCafe.ShowTipoConexion(K_CAFETERA_EN_LINEA_TEXT);
          dmCafeCliente.adCalendario.SaveToFile(sCalend, dfXMLUTF8);
     end;
end;

procedure TFormaConfigura.ConfiguracionChange(Sender: TObject);
begin
  inherited;
  case Configuracion_DevEx.ActivePageIndex of
    TAB_CALENDARIO:
      Self.HelpContext := H00002_Configuracion_Calendario;
    TAB_COMIDAS:
      Self.HelpContext := H00024_Configuracion_TipoComida;
    TAB_SERVIDOR:
      Self.HelpContext := H00003_Configuracion_Servidor;
    TAB_SEGURIDAD:
      Self.HelpContext := H00004_Configuracion_Seguridad;
    TAB_IMPRESORA:
      Self.HelpContext := H00005_Configuracion_Impresora;
    TAB_SONIDOS:
      Self.HelpContext := H00006_Configuracion_Sonidos;
    TAB_SERIAL:
      Self.HelpContext := H00012_Configuracion_Interfase_Serial;
    TAB_CONTADOR:
      Self.HelpContext := H00030_Configuracion_Biometrico;
    else
      Self.HelpContext := H00001_Configuracion_General;
  end;
end;

procedure TFormaConfigura.CBPuertoAChange(Sender: TObject);
begin
  inherited;
  HabilitaBtnSeriales;
end;

procedure TFormaConfigura.HabilitaBtnSeriales;
begin
  BtnConfigA_DevEx.Enabled        := (CBPuertoA.Valor > 0);
  BtnConfigB_DevEx.Enabled        := (CBPuertoB.Valor > 0) and (CBPuertoA.Valor <> CBPuertoB.Valor);
  EditSerialAcepta.Enabled  := (CBPuertoB.Valor > 0);
  EditSerialRechazo.Enabled := EditSerialAcepta.Enabled;
  LblCodigoAcepta.Enabled   := EditSerialAcepta.Enabled;
  LblCodigoRechazo.Enabled  := EditSerialAcepta.Enabled;
  GBCodigosSalida.Enabled   := EditSerialAcepta.Enabled;

  LblCodigoApagadoRechazo.Enabled  := EditSerialAcepta.Enabled;
  LblCodigoApagadoAceptado.Enabled := EditSerialAcepta.Enabled;
  EditApagadoRechazo.Enabled       := EditSerialAcepta.Enabled;
  EditApagadoAceptado.Enabled      := EditSerialAcepta.Enabled;
  LblEsperaAceptado.Enabled        := EditSerialAcepta.Enabled;
  LblEsperaRechazado.Enabled       := EditSerialAcepta.Enabled;
  EditEsperaRechazo.Enabled        := EditSerialAcepta.Enabled;
  EditEsperaAceptado.Enabled       := EditSerialAcepta.Enabled;
  GBCodigosApagados.Enabled        := EditSerialAcepta.Enabled;

end;

procedure TFormaConfigura.BtnConfigAClick(Sender: TObject);
begin
  inherited;
  if (TControl(Sender).Tag = 0) then
    CambiaConfigSerial(CBPuertoA.Valor, FComEntrada)
  else
    CambiaConfigSerial(CBPuertoB.Valor, FComSalida);
end;

procedure TFormaConfigura.CambiaConfigSerial(const iPuerto: Integer; var sConfig: String);
begin
  if (ConfigSerial = nil) then
    ConfigSerial := TConfigSerial.Create(Application);
  with ConfigSerial do begin
    InfoPuerto := dmCafeCliente.GetInfoPuerto(iPuerto, sConfig);
    ShowModal;
    if (ModalResult = mrOk) then
      sConfig := dmCafeCliente.SetInfoPuerto(InfoPuerto);
  end;
end;

procedure TFormaConfigura.SetControlesTeclado(const lEnabled: Boolean);
begin
  //cbTipoComida.Enabled := lEnabled;
  //cbCantidad.Enabled   := lEnabled;
end;

procedure TFormaConfigura.SetControlesServidor;
begin
  Configuracion_DevEx.ActivePage := Server_DevEx;
  Self.ActiveControl       := editDireccion;
end;

procedure TFormaConfigura.editTipoChange(Sender: TObject);
begin
  inherited;
  ActualizaLabelTipoComida; { OP: 22.Mayo.08 }
end;

{ OP: 22.Mayo.08 }
procedure TFormaConfigura.TextosTipoComida;
var
  iContador: Integer;
begin
  with ZCombo do begin
    if (Items.Count >= 8) then
      for iContador      := 0 to 8 do begin
        Items[iContador] := ZetaRegistryCafe.CafeRegistry.TextoTipoComida(iContador + 1);
      end;
    ItemIndex := 0;
  end;
end;

{ OP: 22.Mayo.08 }
procedure TFormaConfigura.ActualizaLabelTipoComida;
begin
  lblTipoComida.Caption := ZetaRegistryCafe.CafeRegistry.TextoTipoComida(StrToInt(zTipoComida.Caption));
end;

{ OP: 23.Mayo.08 }
procedure TFormaConfigura.CalendarioDeSemana;
begin
  dmCafeCliente.SemanaCalendario := eDiasCafeteria(cbSemana.Valor);
end;

{ OP: 23.Mayo.08 }
procedure TFormaConfigura.cbSemanaChange(Sender: TObject);
begin
  inherited;
  dmCafeCliente.SemanaCalendario := eDiasCafeteria(cbSemana.Valor);
end;

procedure TFormaConfigura.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dmCafeCliente.QuitaFiltro;
end;

{$IFDEF BOSE}

procedure TFormaConfigura.BBAgregarContadorClick(Sender: TObject);
begin
  dmCafeCliente.adContador.Append;
  GridContador.SetFocus;
end;

procedure TFormaConfigura.BBBorrarContadorClick(Sender: TObject);
begin
  with dmCafeCliente.adContador do
    if not IsEmpty then
      Delete;
end;

procedure TFormaConfigura.BBModificarContadorClick(Sender: TObject);
begin
  with dmCafeCliente.adContador do
    if not IsEmpty then
      Edit;
  GridContador.SetFocus;
end;

procedure TFormaConfigura.sbReiniciarContadorClick(Sender: TObject);
begin
  inherited;
  FormaCafe.ReiniciaConsumo(0);
end;
{$ENDIF}
// ------------------------------------------------------------------- Biometrico

procedure TFormaConfigura.ToggleBiometric(lBiometrico: Boolean);
begin
     PathTemplates.Enabled                := lBiometrico;
     btnBrowseDirectoryBiometrico_DevEx.Enabled := lBiometrico;
     RecargaHuellas_DevEx.Enabled               := lBiometrico;
     GridRenglones.Columns[3].Visible     := lBiometrico;

     cbChecadaManual.Enabled              := lBiometrico;
     numChecadaManualTimer.Enabled        := lBiometrico;

     GroupBox3.Enabled                    := lBiometrico;
     // DES SP: 13301EP III - Recarga de huellas
     hrRegenerarArchivoHuellas.Enabled    := lBiometrico;
end;

procedure TFormaConfigura.cbTipoGafeteChange(Sender: TObject);
var
  lSeleccionBioemtrico: Boolean;
begin
  inherited;
  lSeleccionBioemtrico := (cbTipoGafete.ItemIndex = Ord(egBiometrico));

  // Activar cbLectorProximidad solo si el tipo de gafete es de proximidad
  // cbLectorProximidad.Enabled := not lSeleccionBioemtrico;
  cbLectorProximidad.Enabled := cbTipoGafete.ItemIndex = Ord(egProximidad);

  // En caso de ser Lector de Proximidad Activa o Desactiva el combo de Lector de Proximidad
  ToggleBiometric(lSeleccionBioemtrico); // En caso de ser Lector Biometrico Activa Controles de Configuracion de Biometrico
  // ZInformation(self.Caption,'Dispositivo biom�trico deber� estar configurado al salir de la pantalla de configuraci�n.',0);
end;

function TFormaConfigura.HayDispositivo: Boolean;
var
  nRet: Integer;

  function InicializaControl: Boolean;
  begin
       try
          Result := FDrivers;
       except
         on E: Exception do
           ZetaDialogo.ZError(Self.Caption, 'Driver de dispositivo biom�trico no detectado. ', 0);
       end;
  end;

begin
     nRet := 1;
     if ( InicializaControl ) then
     begin
          {$ifdef FALSE}
          nRet := SFE.Open( PathTemplates.Text, 4, 0);
          {$else}
          nRet := SFE.Open( PChar( PathTemplates.Text ), K_TIPO_BIOMETRICO, 0);
          {$endif}
          //DotNET();
          SFE.Close;
     end;
     Result := ( ( nRet = 0 ) or ( nRet = -101 ) );
end;

procedure TFormaConfigura.RecargaHuellasClick(Sender: TObject);
begin
     inherited;
     // Hace que la cafeteria recargue TODAS las huellas nuevamente la siguiente vez que se active el sensor.
     if (cbTipoGafete.ItemIndex = Ord(egBiometrico)) then
     begin
          if (HayDispositivo) then
          begin
               if (ZConfirm(Self.Caption, '�Est� seguro de solicitar nuevamente todas las huellas? ' + CR_LF +
                     'Este proceso puede tomar unos minutos.', 0, mbNo)) then
               begin
                    CafeRegistry.FileTemplates := PathTemplates.Text;
                    DeleteFile(CafeRegistry.FileTemplates);
                    with dmCafeCliente do
                    begin
                      PrimeraVez := True;
                      ReiniciaHuellas;
                    end;
                    ZInformation(Self.Caption,
                      'Al salir de la pantalla de la configuraci�n de cafeter�a, se iniciar� la descarga de todas las huellas.', 0);
               end;
          end
          else
              ZetaDialogo.ZError(Self.Caption, 'No hay dispositivo biom�trico conectado.', 0);
     end
     else
         ZetaDialogo.ZError(Self.Caption,
           'Funcionalidad disponible �nicamente cuando est� seleccionado el Tipo de Gafete: Biom�trico.', 0);
end;

end.
