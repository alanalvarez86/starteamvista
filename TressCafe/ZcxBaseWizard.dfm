inherited cxBaseWizard: TcxBaseWizard
  Left = 574
  Top = 197
  BorderStyle = bsDialog
  Caption = 'cxBaseWizard'
  ClientHeight = 476
  ClientWidth = 633
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 633
    Height = 476
    ExplicitWidth = 633
    ExplicitHeight = 476
    inherited Parametros: TdxWizardControlPage
      ExplicitWidth = 611
      ExplicitHeight = 336
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 611
      ExplicitHeight = 336
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 611
        ExplicitHeight = 241
        Height = 241
        Width = 611
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 611
        Width = 611
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 543
          AnchorY = 57
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    AlEjecutar = WizardAlEjecutar
    AfterMove = WizardAfterMove
  end
end
