unit FBitacoraErrores;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls;

type
  TBitacoraErrores = class(TZetaDlgModal)
    mLog: TMemo;
  private
    FArchivo: string;
    procedure SetArchivo(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    property Archivo: string read FArchivo write SetArchivo;
  end;

var
  BitacoraErrores: TBitacoraErrores;

procedure ShowBitacoraErrores( const sArchivo : string );

implementation
uses ZetaDialogo;

{$R *.DFM}

procedure ShowBitacoraErrores( const sArchivo : string );
begin
     if ( BitacoraErrores = NIL ) then
        BitacoraErrores := TBitacoraErrores.Create( Application );
     with BitacoraErrores do
     begin
          Archivo := sArchivo;
          ShowModal;
     end;
end;

{ TBitacoraErrores }

procedure TBitacoraErrores.SetArchivo(const Value: string);
begin
     FArchivo := Value;
     if FileExists( FArchivo ) then
     begin
          mLog.Lines.LoadFromFile( FArchivo );
     end
     else
         ZetaDialogo.ZError( Caption, Format( 'El Archivo %s no Existe', [FArchivo] ), 0 );
end;

end.
