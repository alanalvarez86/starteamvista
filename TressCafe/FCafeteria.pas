unit FCafeteria;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi XE3/XE5                             ::
  :: Unidad:      FCafeteria.pas                             ::
  :: Descripci�n: Programa principal de TressCafeteria.exe   ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
     Variants, SysUtils, Dialogs, Messages, Windows, MMSystem, DB, Forms, VaClasses, VaComm, VaConst, ExtCtrls,
     StdCtrls, Grids, DBGrids, jpeg, Graphics, Controls, Classes, SFE, ZetaCommonLists,
     ZetaRegistryCafe,
     FParametrosImpresion,
     IniFiles,
     FImportaChecadas,
     ZetaAsciiFile,
     DCafeCliente,
     FHelpManager,
     OleCtrls,
     {SFEStandardLib_TLB,} //Biometrico
     SZCodeBaseX, //Biometrico Base64 Serializer
     ZetaBanner, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, cxTextEdit, FireDAC.UI.Intf, FireDAC.VCLUI.Login,
  FireDAC.Stan.Intf, FireDAC.Comp.UI, cxCalendar, cxClasses, dxSkinsForm,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, cxImage, dxGDIPlusClasses, Vcl.Menus, cxButtons,
  Vcl.ImgList, ZetaCXGrid, ZcxWizardBasico, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter;

{$define MULT_TIPO_COMIDA} //Permitir multiples registros de cafeteria de diferente tipo en un mismo minuto

type
  TTArchivosIni = class( TObject )
  private
    { Private declarations }
    FiniFile: TiniFile;
    function GetConsumos: Integer;
    function GetFechaConsumos: TDatetime;
    procedure SetConsumos(const Value: Integer);
    procedure SetFechaConsumos(const Value: TDateTime);
  public
    { Public declarations }
    Constructor Create;
    Destructor Destroy; override;
    property Consumos: integer read GetConsumos write SetConsumos;
    property FechaConsumos: TDateTime read GetFechaConsumos write SetFechaConsumos;
  end;

  TFormaCafe = class(TForm)
    DevEx_SkinController: TdxSkinController;
    cxStyleRepository1: TcxStyleRepository;
    cxStyleStatusBarLabel: TcxStyle;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    panelEncabezado: TPanel;
    TimerReloj: TTimer;
    TimerPregunta: TTimer;
    TimerContador: TTimer;
    TimerSensorBiometrico: TTimer;
    VaCommA: TVaComm;
    VaCommB: TVaComm;
    lblMsg: TStaticText;
    Foto: TImage;
    PanelFoto: TPanel;
    panelDEMOBanner: TPanel;
    Desconectado: TTimer;
    TimerValidar: TTimer;
    FDGUIxLoginDialog1: TFDGUIxLoginDialog;
    panelEmpleado: TPanel;
    lblTurno: TLabel;
    Label1: TLabel;
    lblNombre: TLabel;
    editNumero: TEdit;
    panelRespuesta: TPanel;
    panelDEMO: TPanel;
    lblDEMO: TLabel;
    Banner: TZetaBanner;
    DataSource: TDataSource;
    panelInferior: TPanel;
    gridComidas_DevEx: TZetaCXGrid;
    gridComidas_DevExDBTableView: TcxGridDBTableView;
    CF_FECHA: TcxGridDBColumn;
    CF_HORA: TcxGridDBColumn;
    CF_TIPO: TcxGridDBColumn;
    CF_COMIDAS: TcxGridDBColumn;
    gridComidas_DevExLevel: TcxGridLevel;
    panelDatosEmpresa: TPanel;
    panelDer1lTiempo: TPanel;
    Panel2: TPanel;
    imgReloj: TcxImage;
    Panel3: TPanel;
    lblFecha: TLabel;
    lblHora: TLabel;
    panelDer2Comida: TPanel;
    Panel4: TPanel;
    imgTaza: TcxImage;
    Panel6: TPanel;
    lblTipoComida: TLabel;
    panelDer3SigComida: TPanel;
    Panel7: TPanel;
    imgSigComida: TcxImage;
    Panel8: TPanel;
    lblSigComida: TLabel;
    panelDer4Consumos: TPanel;
    Panel9: TPanel;
    imgConsumos: TcxImage;
    Panel10: TPanel;
    lblConsumos: TLabel;
    cxsColumnas: TcxStyle;
    lblConsumosPeriodo: TLabel;
    lblSirviendo: TLabel;
    lblHrSigComida: TLabel;
    lblNumConsEstacion: TLabel;
    TimerSiguienteComida: TTimer;
    TimerPaseSuGafete: TTimer;
    imagenGenerica: TImage;
    imgEnLinea: TcxImage;
    imgFueraLinea: TcxImage;
    Checada: TButton;
    imgTazaFondo: TcxImage;
    imgEnFueraLinea: TcxImage;
    LblConectado: TLabel;
    btnChecadaManual: TcxButton;
    TimerChecadaAlterna: TTimer;
    imgListTipoDispositivo: TcxImageList; // BIOMETRICO
    procedure ChecadaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerRelojTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerPreguntaTimer(Sender: TObject);
    procedure VaCommABreak(Sender: TObject);
    procedure VaCommAError(Sender: TObject; Errors: Integer);
    procedure VaCommARx80Full(Sender: TObject);
    procedure VaCommATxEmpty(Sender: TObject);
    procedure VaCommARxFlag(Sender: TObject);
    procedure ControlesEnter(Sender: TObject);
    {$ifdef BOSE}
    procedure TimerContadorTimer(Sender: TObject);
    {$endif}
    Procedure ClearKeyBoardBuffer(Handle:THandle);
    //BIOMETRICO
    function CaptureFinger : Integer;
    function LeeArchivo:Boolean;
    procedure InicializaControles(lBiometrico: Boolean);
    procedure InicializaBiometrico;
    procedure InicializaSensorBiometrico;
    procedure switchWorking(bWorking : Boolean);
    procedure PrintError(error : Integer);
    function GetBioMessage(error: Integer) : String; // BIOMETRICO

    // BIOMETRICO
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerSensorBiometricoTimer(Sender: TObject);
    procedure LogPantalla(sMensaje : String);
    procedure DesconectadoTimer(Sender: TObject);
    procedure ResetTimerConnect;
    procedure TimerValidarTimer(Sender: TObject);
    procedure gridComidas_DevExDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure CF_TIPOGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure CF_HORAGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure TimerSiguienteComidaTimer(Sender: TObject);
    procedure TimerPaseSuGafeteTimer(Sender: TObject);
    procedure PanelFotoResize(Sender: TObject);
    procedure panelEmpleadoResize(Sender: TObject);
    procedure panelRespuestaResize(Sender: TObject);

    // US #7328: Implementar Control de la ayuda en XE5
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );

    procedure btnChecadaManualClick(Sender: TObject);
    procedure TimerChecadaAlternaTimer(Sender: TObject);
    procedure editNumeroKeyPress(Sender: TObject; var Key: Char);
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean); 
  protected
    { Private declarations }
  private
    { Private declarations }
    FUltimoFoco: TShape;
    FUltimaCredencial: String;
    FHoraCal: String;
    FTimerOn: Boolean;
    FDrivers: boolean;
    {$ifdef BOSE}
    FHoraContador : string;
    {$endif}
    FAceptaCancelacion: Boolean;
    FSegunda: Boolean;
    FImpresion: TParametrosImpresion;
    FImportaChecadas : TImportaChecadas;
    FTotalConsumos: Integer;
    FWasOnline : Boolean;
    FVaCommSalida: TVaComm;
    FChecadaSimultanea: eChecadaSimultanea;
    aSerialSalida: array [ FALSE..TRUE ] of String;
    FOldHelpEvent: THelpEvent;
    lChecadaManual: Boolean;
    iTiempo: Integer;
    tgAnterior: eTipoGafete;
    procedure CancelarChecadaManual;

    //SFEStandard1: TSFEStandard; // BIOMETRICO
    // procedure Pinta(Foco: TShape; eColor: TColor);
    procedure Pinta(eColor: TColor);
    procedure ShowResultado(const sResultado: String; const eColor : TColor);
    function properCase(sBuffer: string):string;
    procedure AceptaChecada( const sLetrero : String );
    procedure RechazaChecada( const sLetrero : String; bTimerPaseSuGafete: Boolean = TRUE );
    {$ifdef MULT_TIPO_COMIDA}
    function RevisaChecada( const lCancelacion: Boolean; const iTipoComida, iQty: Integer): integer; overload;
    function RevisaChecada( const lCancelacion: Boolean; const iTipoComida, iQty: Integer; var sLetrero: String) : integer; overload;
    function ConfirmaChecada(var sLetrero: String; const iTipo: Integer = 0):Boolean;
    {$else}
    function RevisaChecada( const lCancelacion: Boolean; const iTipoComida, iQty: Integer): integer;
    function ConfirmaChecada : Boolean;
    {$endif}
    procedure MuestraDatosEmpleado( const iTipo, iCantidad: Integer );
    procedure LimpiaDatos;
    procedure FocusNumero;
    procedure ChecadaEmpleado;
    procedure ChecadaOperador;
    procedure IdleCafe(Sender: TObject; var Done: Boolean);
    procedure CafeOnMessage(var Msg: TMsg; var Handled: Boolean);
    procedure ConfiguraEstacion( const lInicial: Boolean = FALSE );
    procedure CancelaChecadaAnterior;
    procedure ImprimeListado;
    procedure IncrementaConsumos(const Incremento : integer);
    procedure ProcesaChecada;
    procedure SonidoAcepta;
    procedure SonidoRechaza;
    function GetNombreEmpleado: string;
    {$ifdef MULT_TIPO_COMIDA}
    function GetTurnoEmpleado: string;
    function PideTipoComida:Boolean;
    procedure RecuperaDatosEmpleado;
    {$endif}
    function EsInvitador: Boolean;
    procedure AbrePuerto(VaCommControl: TVaComm; PuertoInfo: TInfoPuerto; const lEntrada: Boolean);
    procedure InitSerialControl;
    procedure InitSerial(const lEntrada, lIguales: Boolean; const iPuerto: Integer);
    procedure SerialResultado(const lOk: Boolean);
    procedure InitConsumos;
    procedure ConsumosPeriodo;
    procedure SetArchivoBak;
    procedure ImportaChecadas;
    function HTMLHelpHook( Command: Word; Data: THelpEventData; var CallHelp: Boolean ): Boolean;
    procedure SincronizaHuellas; // BIOMETRICO
    procedure SeHaConectado(Sender: TObject);
    procedure SeHaDesConectado(Sender: TObject);
    procedure ErrorDeCliente(Sender: TObject);
    procedure ajustarPanelRespuesta;
    procedure repaint;
  public
    { Public declarations }
    FActualCredencial: String;
    procedure CheckServidorDesconectado;
    procedure ShowTipoConexion( const lConectado: Integer );
    procedure ShowProhibicion( const lProhibe: Boolean );
    procedure CheckDemo( const sMensaje: string );
    procedure WarningMessage(const sLetrero: String);
    procedure OkMessage(const sLetrero: String);
    procedure ErrorMessage(const sLetrero: String);
    function  LeeParametros : Boolean;
    procedure ActualizaParametros;

    {OP: Se cambi� el m�todo de uno privado a privado, ya que se va invocar de otras pantallas}
    procedure ReiniciaConsumo( const Incremento: integer );{OP: 22.Mayo.08}
    procedure paseSuGafete;
    procedure DetieneLector;
    procedure WizardConfiguracion;

  end;

const
     MESS_PROHIBE_ACCESO = '� Prohibido el Acceso !';
     {$ifdef MULT_TIPO_COMIDA}
     K_SIN_TCOMIDA = 0;
     K_SIN_CONFIRMAR_CONSUMO = 'No se confirm� otra Comida';
     {$endif}
     K_1000 = 1000;
     K_2000 = 2000;

     MIN_SIZE_FONT = 22;
     MED_SIZE_FONT = 30;
     MAX_SIZE_FONT = 40; // �46?
     MIN_SIZE_PANEL = 650; // 700;
     MAX_SIZE_PANEL = 1020;
     CANT_CARAC_PANEL = 27; // 25

var
  FormaCafe: TFormaCafe;
  TIni: TTArchivosIni;

implementation

uses FDlgOperador,
     FConfigura,
     FPreguntaComida,
     FConfirmaComida,
     {$ifdef CALSONIC}
     FReportesCafe,
     {$endif}
     FPreguntaPassWord,
     FServidorDesconectado,
     ZetaMessages,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonClasses,
     GraphicField,
     CafeteraConsts,
     CafeteraUtils,
     MaskUtils,
     StrUtils,
     FWizConfigurarCafeteria_DevEx;

const
     SECCION = 'CONSUMOS';
     CONSUMO = 'Consumos';
     ANIO = 'A�o';
     MES = 'Mes';
     DIA = 'Dia';
     K_MENSAJE_GENERAL_ERROR_CLIENTE_SERVIDOR = 'No fue posible comunicarse con el servidor';

{$R *.DFM}

// BIOMETRICO
var
  gTemplate         : array[0..1403] of Integer; //351*4 = 1404
  gImageBytes       : array[0..65535] of Byte; // 256 * 256
  addrOfImageBytes  : Pbyte;
  addrOfTemplate    : PByte;
  gWorking          : Boolean;

// BIOMETRICO
const
  FINGER_BITMAP_WIDTH   : Integer = 256;
  FINGER_BITMAP_HEIGHT  : Integer = 256;
  ColorNColor           : Integer = 3;

procedure TFormaCafe.FormCreate(Sender: TObject);
begin
     Checada.Width := 0;
     Environment;
     InitCafeRegistry;
     InitConsumos;
     // FUltimoFoco := Amarillo;
     FWasOnline := False;
     LimpiaDatos;
     WindowState:= wsMaximized;
     with Application do
     begin
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
          OnIdle := IdleCafe;
          OnMessage := CafeOnMessage;
          UpdateFormatSettings := FALSE;
          // US #7328: Implementar Control de la ayuda en XE5.
          OnException := ManejaExcepcion;
     end;

     // US #7328: Implementar Control de la ayuda en XE5.
     HelpContext := 1;

     dmCafeCliente := TdmCafeCliente.Create(nil);
     with dmCafeCliente do begin
       OnClientConnect    := SeHaConectado;
       OnClientDisConnect := SeHaDesConectado;
       OnClientError      := ErrorDeCliente;
     end;
     {$ifdef BOSE}
     with TimerContador do
     begin
          OnTimer := TimerContadorTimer;
          Enabled := TRUE;
     end;
     {$endif}

     {Cargar los Drivers dependiendo de la plataforma (32 o 64 Bits)}
     FDrivers := SFE.LoadDLL;
     // Apariencia.
     {Banner.Color := RGB (227, 221, 234);
     Banner.Font.Color := RGB (85, 85, 85);}
     // Banner.Color := TColor ($8C818B);
     Banner.Color := RGB (227, 221, 234);
     Banner.Font.Color := TColor ($4D3B4B);

     lblNombre.Font.Color := TColor ($4D3B4B);
     lblTurno.Font.Color := TColor ($4D3B4B);
     lblConsumosPeriodo.Font.Color := TColor ($4D3B4B);
     lblFecha.Font.Color := TColor ($4D3B4B);
     lblHora.Font.Color := TColor ($4D3B4B);
     lblSirviendo.Font.Color := RGB (204,88,72);
     // lblSirviendo.Font.Color := TColor ($4D3B4B);
     lblTipoComida.Font.Color := RGB (204,88,72);
     // lblConsumos.Font.Color := RGB (0,214,209);
     lblConsumos.Font.Color := TColor ($4D3B4B);
     lblNumConsEstacion.Font.Color := TColor ($4D3B4B);
     // lblSigComida.Font.Color := RGB (255,153,51);
     // lblSigComida.Font.Color := TColor ($4D3B4B);
     lblSigComida.Font.Color := TColor ($4D3B4B);
     // lblHrSigComida.Font.Color := RGB (255,153,51);
     lblHrSigComida.Font.Color := TColor ($4D3B4B);
     panelRespuesta.Color := RGB (195,195,195);
     lblDEMO.Font.Color := RGB (196,0,0);

     // Timer PaseSuGafete
     TimerPaseSuGafete.Interval := CafeRegistry.TiempoEspera*K_1000;
end;


// US #7328: Implementar Control de la ayuda en XE5
procedure TFormaCafe.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TFormaCafe.FormShortCut(var Msg: TWMKey; var Handled: Boolean);
begin
     if (Msg.CharCode = VK_F2) and ( CafeRegistry.TipoGafete = egBiometrico ) then
     begin
          Handled := true;
          ActiveControl := editNumero;
          btnChecadaManualClick(Self);
     end;
end;

procedure TFormaCafe.FormShow(Sender: TObject);
var
   lBiometrico : Boolean;
begin
     FTimerOn := False;
     FAceptaCancelacion := FALSE;
     FSegunda := FALSE;
     dmCafeCliente.PrimeraVez:= True; // BIOMETRICO: Primera vez que sincroniza huellas
     if not LeeParametros then
     begin
          if CafeRegistry.CanWrite then
             ConfiguraEstacion( TRUE )
          else
          begin
               ZetaDialogo.ZError( 'Acceso al Programa', 'No Tiene Configurado Acceso' + CR_LF +
                                                         'Requiere Derechos de Escritura en Registry' + CR_LF +
                                                         'Para Realizar Esta Configuraci�n', 0 );
               Close;
          end;
     end;

     // Checada adicional
     lChecadaManual := FALSE;
     iTiempo := CafeRegistry.ChecadaManualTimer;
     btnChecadaManual.Visible := FALSE;
     TimerChecadaAlterna.Enabled := FALSE;

     // BIOMETRICO
     lBiometrico := ( CafeRegistry.TipoGafete = egBiometrico );
     if ( lBiometrico ) then
     begin
          InicializaControles( lBiometrico ); //Inicializa Timer!
          InicializaBiometrico;
          // Checada adicional
          btnChecadaManual.Visible := TRUE;
     end;

    gridComidas_DevExDBTableView.DataController.DataSource := dmCafeCliente.dsComidas;
    //Desactiva la posibilidad de agrupar
    gridComidas_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
    //Esconde la caja de agrupamiento
    gridComidas_DevExDBTableView.OptionsView.GroupByBox := False;
    //Para que nunca muestre el filterbox inferior
    gridComidas_DevExDBTableView.FilterBox.Visible := fvNever;
    //Para que no aparezca el Custom Dialog
    gridComidas_DevExDBTableView.FilterBox.CustomizeDialog := TRUE;
end;

// BIOMETRICO
procedure TFormaCafe.InicializaControles( lBiometrico : Boolean );
begin
     LogPantalla('Activa Pantalla de Verbose');
     lblMsg.Visible := False;
     LogPantalla('Activa Timer de Sensor');
     TimerSensorBiometrico.Enabled := lBiometrico;
     TimerValidar.Enabled := TimerSensorBiometrico.Enabled;
end;

// BIOMETRICO
procedure TFormaCafe.InicializaBiometrico;
begin
     //Inicializa Biometrico
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
end;

procedure TFormaCafe.FormDestroy(Sender: TObject);
begin
     TimerSensorBiometrico.Enabled := False;
     try
       FreeAndNil(dmCafeCliente);
     except

     end;
     ZetaRegistryCafe.ClearCafeRegistry;
     FreeAndNil( TIni );
end;

function TFormaCafe.HTMLHelpHook(Command: Word; Data: THelpEventData; var CallHelp: Boolean): Boolean;
begin
     Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

{ Control de Timers }

procedure TFormaCafe.IdleCafe(Sender: TObject; var Done: Boolean);
var
   FActual: String;
begin
     FActual := FormatDateTime( 'hhmm', Now );
     if ( FActual <> FHoraCal ) then
     begin
          FHoraCal := FActual;
          with dmCafeCliente do
          begin
               // No revisar calendario s� se est� mostrando la forma de configuraci�n.
               if (not Assigned (FormaConfigura)) or (not FormaConfigura.Showing) then
                  ScanCalendario( FHoraCal );

               // Correcci�n de Bug #12036: NELLCOR: No se recargan las huellas automaticamente en cafeteria.
               // No intentar sincronizar huellas en IdleCafe. No sirve hacerlo aqu�.
               {if ( ( CafeRegistry.TipoGafete = egBiometrico ) and ( Sync ) ) then
               begin
                    WarningMessage( 'Espere a la sincronizacion de Huellas' );
                    Sync := False;
                    DetieneLector;
                    TimerSensorBiometrico.Enabled := True;
               end}
          end;
     end;
     TimerPregunta.Enabled := FTimerOn;
end;

procedure TFormaCafe.CafeOnMessage(var Msg: TMsg; var Handled: Boolean);
begin
     if FTimerOn then
     begin
          with Msg do
          begin
               if ( ( Message >= WM_KEYFIRST ) and ( Message <= WM_KEYLAST ) ) or ( ( Message >= WM_MOUSEFIRST ) and ( Message <= WM_MOUSELAST ) ) then
                  TimerPregunta.Enabled := False;
          end;
     end;
end;

procedure TFormaCafe.TimerPaseSuGafeteTimer(Sender: TObject);
begin
    paseSuGafete;
end;

procedure TFormaCafe.TimerChecadaAlternaTimer(Sender: TObject);
var sMensaje: String;
begin
     if (CafeRegistry.ChecadaManual = ord (egTress)) or (CafeRegistry.ChecadaManual = ord (egProximidad)) then
        sMensaje := 'Pase su gafete o credencial'
     else
         sMensaje :=  'Puede ingresar su n�mero biom�trico';

     if lChecadaManual then
     begin
          if iTiempo > 0 then
          begin
               Pinta (RGB (195,195,195));
               ShowResultado(Format (sMensaje +' %d segundos', [iTiempo]), clGray);
               Dec (iTiempo);
          end
          else
          begin
               Pinta (RGB (195,195,195));
               ShowResultado(Format (sMensaje +' %d segundos', [iTiempo]), clGray);
               TimerChecadaAlterna.Enabled := FALSE;
               CancelarChecadaManual;
               {CafeRegistry.TipoGafete := egBiometrico;
               dmCafeCliente.TipoGafete := egBiometrico;}
               {inicializaControles (TRUE);
               InicializaBiometrico;}
          end;
     end;
end;

procedure TFormaCafe.TimerPreguntaTimer(Sender: TObject);
begin
     //
     // No correr timer si es invitador y no usa teclado.
     // if (not EsInvitador) and (not CafeRegistry.UsaTeclado) then
     {if (not CafeRegistry.UsaTeclado) then
     begin
          if ( CafeRegistry.TipoGafete = egBiometrico ) then // BIOMETRICO
               DetieneLector;

          if ( PreguntaComida <> nil ) then
          begin
               with PreguntaComida do
               begin
                    Validando := TRUE;
                    try
                       if Visible and Invitador and ( QTY.Valor > 0 ) and
                          ( not dmCafeCliente.UsaTeclado ) then           // Si no usa teclado s� graba
                          PreguntaComida.EscribirCambios
                       else
                          PreguntaComida.CancelarComida
                    finally
                       Validando := FALSE;
                    end;
               end;
          end;

          if ( CafeRegistry.TipoGafete = egBiometrico ) then // BIOMETRICO
               TimerSensorBiometrico.Enabled := True;
     end;}
     // ----- -----
end;

procedure TFormaCafe.TimerRelojTimer(Sender: TObject);
begin
     lblFecha.Caption := FormatDateTime( 'd/mmm/yyyy', Date );
     // lblFecha.Caption := FormatDateTime( 'd/mmm/yyyy', Date ) + ' ' + FormatDateTime( 'hh:nn AM/PM', Now );
     // lblFecha.Caption := FormatDateTime( 'hh:nn AM/PM', Now ) + ' ' + FormatDateTime( 'd/mmm/yyyy', Date );
     lblHora.Caption := FormatDateTime( 'hh:nn AM/PM', Now );
end;

procedure TFormaCafe.CheckDemo( const sMensaje: string );
{
const
     K_MENSAJE = 'El M�dulo de Cafeter�a Trabajar� en Modo %s' + CR_LF +
                 '� Las Checadas que se Realicen NO Ser�n Grabadas !' + CR_LF +
                 '� Desea Continuar ?';
var
   lResultado: boolean;
}
begin
     if strLleno( sMensaje ) then
     begin
          {
          lResultado := ZetaDialogo.ZWarningConfirm( 'Advertencia', Format( K_MENSAJE, [ sMensaje ] ), 0, mbOk );
          if lResultado then
          }
          lblDemo.Caption := sMensaje;
          lblDEMO.Visible := TRUE;
          panelDEMO.Visible := TRUE;
          {
          else
              Close;
          }
     end
     else
     begin
         lblDemo.Caption := VACIO;
         lblDEMO.Visible := FALSE;
         panelDEMO.Visible := FALSE;
     end;
end;

{ Inicializa Parametros }

function TFormaCafe.LeeParametros: Boolean;
var
   oCursor: TCursor;
   lIguales: Boolean;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Banner.Caption := CafeRegistry.Banner;
        Banner.StepsPerSecond := CafeRegistry.Velocidad * 3;
        Banner.Active := True;

        FChecadaSimultanea := CafeRegistry.ChecadaSimultanea;
        with dmCafeCliente do
        begin
             TipoComida     := CafeRegistry.TipoComida;
             MostrarComidas := CafeRegistry.MostrarComidas;
             MostrarSigComida := CafeRegistry.MostrarSigComida;
             MostrarFoto    := CafeRegistry.MostrarFoto;
             Estacion       := CafeRegistry.Identificacion;
             UsaTeclado     := CafeRegistry.UsaTeclado;         // Se usan Constantemente, para evitar leer el registry cada vez
             PreguntaTipo   := CafeRegistry.PreguntaTipo;
             PreguntaQty    := CafeRegistry.PreguntaQty;
             TipoGafete     := CafeRegistry.TipoGafete;
             InicializaCalendario( CafeRegistry.Calendario );   // Se requiere Leer independientemente de que no est� configurado el Servidor
             SiguienteComida;
             paseSuGafete;
             // Timer PaseSuGafete
             TimerPaseSuGafete.Interval := CafeRegistry.TiempoEspera*K_1000;

             // Checada adicional.
             iTiempo := CafeRegistry.ChecadaManualTimer;
             btnChecadaManual.OptionsImage.ImageIndex := CafeRegistry.ChecadaManual;

             {$ifdef BOSE}
             //Inicializar Contador
             InicializaContador( CafeRegistry.ArchivoContador );
             ScanContador( FormatDateTime( 'hhmm', Now ) );
             {$endif}
             Result := StrLleno( CafeRegistry.Servidor ) and ( CafeRegistry.Puerto > 0 );
             if Result then
             begin
                  // Configura Entrada y Salida Serial
                  InitSerialControl;
                  with CafeRegistry do
                  begin
                       lIguales := ( SerialA = SerialB );
                       if ( SerialA > 0 ) then  // Entrada Serial
                       begin
                            InitSerial( True, lIguales, SerialA );
                       end;
                       if ( SerialB > 0 ) then  // Salida Serial
                       begin
                            InitSerial( False, lIguales, SerialB );
                       end;
                  end;
                  // GA: Aqu� se Conectar� el puerto TCP/IP. Es importante que sea despu�s
                  // de inicializar los puertos seriales ya que en WIN95/98 si se falla
                  // al conectarse al puerto TCP/IP, no se inicializa la interfase serial.
                  SetCalendarioInicial( FormatDateTime( 'hhmm', Now ) );
             end;
             Init; { GA: Inicializa bit�cora de checadas }
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TFormaCafe.ActualizaParametros;
var
   oCursor: TCursor;
   lIguales: Boolean;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Banner.Caption := CafeRegistry.Banner;

        FChecadaSimultanea := CafeRegistry.ChecadaSimultanea;
        with dmCafeCliente do
        begin
             MostrarComidas   := CafeRegistry.MostrarComidas;
             MostrarSigComida := CafeRegistry.MostrarSigComida;
             MostrarFoto      := CafeRegistry.MostrarFoto;
             Estacion         := CafeRegistry.Identificacion;
             UsaTeclado       := CafeRegistry.UsaTeclado; 
             PreguntaTipo     := CafeRegistry.PreguntaTipo;
             PreguntaQty      := CafeRegistry.PreguntaQty;
             TipoGafete       := CafeRegistry.TipoGafete;

             SiguienteComida;
             paseSuGafete;
             TimerPaseSuGafete.Interval := CafeRegistry.TiempoEspera*K_1000;
             iTiempo := CafeRegistry.ChecadaManualTimer;
             btnChecadaManual.OptionsImage.ImageIndex := CafeRegistry.ChecadaManual;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

{ Evento de Checada }

procedure TFormaCafe.ChecadaClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     TimerPaseSuGafete.Enabled := FALSE;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        // Agregar condici�n por cambios para Checada adicional
        if ( CafeRegistry.TipoGafete <> egBiometrico ) or ( UpperCase( editNumero.Text ) = CafeRegistry.GafeteAdmon )
        or (lChecadaManual) then
        begin
             ProcesaChecada;

             // Checada adicional.
             if lChecadaManual then
             begin
                  CancelarChecadaManual;
             end;

             if ( FUltimaCredencial <> FActualCredencial ) then // BIOMETRICO
             begin
                  Sleep( K_2000 ); //Este Tiempo, es para que el Invitador pueda ver la respuesta del Servidor;
                  editNumero.Text := FUltimaCredencial;
                  ProcesaChecada;
             end;
             FSegunda := TRUE;
            TimerPaseSuGafete.Enabled := TRUE;
        end
        else
        begin
             editNumero.Text := VACIO;
             RechazaChecada('�No se permiten checadas con teclado!', FALSE);
             panelRespuesta.Color := RGB (227,66,51);
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

// CancelarChecadaManual.
procedure TFormaCafe.CancelarChecadaManual;
begin
     if lChecadaManual then
     begin
          lChecadaManual := FALSE;
          CafeRegistry.TipoGafete := egBiometrico;
          dmCafeCliente.TipoGafete := egBiometrico;
          iTiempo := CafeRegistry.ChecadaManualTimer;
          inicializaControles (TRUE);
     end;
end;

procedure TFormaCafe.ProcesaChecada;
var
   sLetrero: String;
begin
     if ( CafeRegistry.TipoGafete = egBiometrico ) then // BIOMETRICO
          DetieneLector;

     //ARCELIA FlushKeyboard;
     ClearKeyBoardBuffer(Self.Handle);
     FUltimaCredencial := editNumero.Text;
     FActualCredencial := FUltimaCredencial;
     {$ifdef MULT_TIPO_COMIDA}
     if PideTipoComida or ConfirmaChecada(sLetrero) then
     {$else}
     if ConfirmaChecada then
     {$endif}
     begin
          LimpiaDatos;
          if ( UpperCase( FUltimaCredencial ) = CafeRegistry.GafeteAdmon ) then
          begin
               ChecadaOperador;
          end
          else
              ChecadaEmpleado;
     end
     else
         {$ifdef MULT_TIPO_COMIDA}
         WarningMessage( sLetrero );
         {$else}
         WarningMessage( 'NO Se Confirm� Otra Comida' );
         {$endif}

     //ARCELIA FlushKeyboard;
     ClearKeyBoardBuffer(Self.Handle);
     FocusNumero;

     ajustarPanelRespuesta;
     repaint;

     // Agregar condici�n por cambios para Checada adicional.
     if ( CafeRegistry.TipoGafete = egBiometrico ) and (not lChecadaManual) then // BIOMETRICO
         TimerSensorBiometrico.Enabled := True;
end;

procedure TFormaCafe.WizardConfiguracion;
begin
     if (Not CafeRegistry.ConfiguracionEnviada) then
     begin
          if (CafeRegistry.CanWrite) and (dmCafeCliente.Active) then //El wizard solo aparece si tienes permisos de administrador
          begin
               ZcxWizardBasico.ShowWizard(TWizConfigurarCafeteria_DevEx);
               ActualizaParametros;
          end;
     end;

     if CafeRegistry.ConfiguracionEnviada then
         FormaConfigura.SincronizaConfiguracion(CafeRegistry.Identificacion);
end;

Procedure TFormaCafe.ClearKeyBoardBuffer(Handle:THandle);
var
  MyMgs:TMsg;
  NoMessages:Boolean;
begin
    while PeekMessage(MyMgs,Handle,WM_KEYFIRST,WM_KEYLAST,PM_REMOVE) do ;
end;

{$ifdef MULT_TIPO_COMIDA}
function TFormaCafe.ConfirmaChecada(var sLetrero: String; const iTipo: Integer): Boolean;
{$else}
function TFormaCafe.ConfirmaChecada:Boolean;
{$endif}
var
   dUltima, dAhora: TDateTime;
begin
     dAhora := Now;
     dUltima := dmCafeCliente.LastHora;
     if not EsInvitador and FSegunda and ( dmCafeCliente.CompletaGafete(FUltimaCredencial) = dmCafeCliente.LastGafete ) and
        ( FormatDateTime( 'HHMM', dAhora ) = FormatDateTime( 'HHMM', dUltima ) )
        {$ifdef MULT_TIPO_COMIDA}and ( ( iTipo = K_SIN_TCOMIDA ) or ( dmCafeCliente.LastTipo = iTipo ) ){$endif}then
     begin
          case FChecadaSimultanea of
               csNoPreguntar: Result := True;
               csNoAcumular: Result := False;
               csPreguntar:
               begin
                    // Checada adicional.
                    if lChecadaManual then
                       TimerChecadaAlterna.Enabled := FALSE;

                    //ARCELIA FlushKeyboard  y Sleep( 500 ) ;
                    ClearKeyBoardBuffer(Self.Handle);
                    Sleep( 500 ) ;
                    if ( ConfirmaComida = NIL ) then
                       ConfirmaComida := TConfirmaComida.Create( Self );
                    with ConfirmaComida do
                    begin
                         Empleado := GetNombreEmpleado;
                         Credencial := editNumero.Text;
                         ShowModal;
                         FUltimaCredencial := eCredencial.Text;
                         Result := ( ModalResult = mrOk );

                         if ( Result ) then
                              // ShowResultado( 'Confirmado', clWebSeaGreen );
                              ShowResultado( 'Confirmado', RGB(147,210,80));
                    end;

                     //ARCELIA FlushKeyboard  y Sleep( 250 ) ;
                     ClearKeyBoardBuffer(Self.Handle);
                     Sleep( 250 ) ;
               end;
          else
              Result := False;
          end;
     end
     else
         Result := TRUE;
     {$ifdef MULT_TIPO_COMIDA}
     if not Result then
     begin
          sLetrero:= K_SIN_CONFIRMAR_CONSUMO;
          if ( iTipo <> K_SIN_TCOMIDA ) then
             RecuperaDatosEmpleado;
     end;
     {$endif}
end;

procedure TFormaCafe.ChecadaOperador;
const
     K_OPERA_IMPRIMIR = 1;
     K_OPERA_CANCELAR = 2;
     K_OPERA_CONFIGURAR = 3;
     K_OPERA_RESET_CONSUMOS = 4;
     K_OPERA_IMPORTA_CHECADAS = 5;
begin
     //Se agrega para que no haya operaciones de biometrico mientras se esta en la configuracion
     editNumero.Enabled := false;

     CancelarChecadaManual;

     editNumero.Text := '';
     if ( DlgOperador = nil ) then
        DlgOperador := TDlgOperador.Create( Application );
     with DlgOperador do
     begin
          Cancelar_DevEx.Enabled := FAceptaCancelacion AND CafeRegistry.ConcesionCancelar;
          Imprimir_DevEx.Enabled := CafeRegistry.ConcesionImprimir;
          Configurar_DevEx.Enabled := CafeRegistry.CanWrite;
          ImportarChecadas_DevEx.Enabled := dmCafeCliente.Active;

          ShowModal;
         
          {if ( CafeRegistry.TipoGafete = egBiometrico ) then //Si es biometrico Inicializa el lector
             dmCafeCliente.PrimeraVez := True;}

          case Operacion of
               K_OPERA_IMPRIMIR : ImprimeListado;
               K_OPERA_CANCELAR :  CancelaChecadaAnterior;
               K_OPERA_CONFIGURAR : ConfiguraEstacion;
               K_OPERA_RESET_CONSUMOS : IncrementaConsumos( FTotalConsumos * -1 );
               K_OPERA_IMPORTA_CHECADAS : ImportaChecadas;
          end;
     end;

     //Se reactiva para poder notificar a los demas procesos que esta disponible para operacion
     editNumero.Enabled := True;
end;

procedure TFormaCafe.ChecadaEmpleado;
var
    sResultado : String;
    iCantidad : integer;
   {$ifdef CALSONIC}
   FTicket: TReportesCafe;
   {$endif}
begin
    try
       if not dmCafeCliente.ProhibirAcceso then
       begin
            if ( CafeRegistry.TipoGafete = egBiometrico ) then
               dmCafeCliente.ScanCalendario( FormatDateTime( 'hhmm', Now ) );
            {$ifdef MULT_TIPO_COMIDA}
            iCantidad := RevisaChecada( FALSE, dmCafeCliente.TipoComida, 1, sResultado );
            {$else}
            iCantidad := RevisaChecada( FALSE, dmCafeCliente.TipoComida, 1 );
            {$endif}
            if dmCafeCliente.ValidacionOK( sResultado ) then
            begin
                 IncrementaConsumos(iCantidad);
                 AceptaChecada( sResultado );
                 FAceptaCancelacion := TRUE;
                 {$ifdef CALSONIC}
                 FTicket := TReportesCafe.Create();
                 try
                    FTicket.ImprimeTicket( editNumero.Text, dmCafeCliente.LastHora, FormatDateTime( 'hh:mm', dmCafeCliente.LastHora  ), lblNombre.Caption, not dmCafeCliente.Active );
                 finally
                        FreeAndNil( FTicket );
                 end;
                 {$endif}
            end
            else
            begin
                 RechazaChecada( sResultado );
                 FAceptaCancelacion :=  FALSE;
            end;
       end
       else
            RechazaChecada( MESS_PROHIBE_ACCESO, FALSE );
    except
        on E : Exception do
           RechazaChecada( E.Message );
    end;
end;

function TFormaCafe.RevisaChecada( const lCancelacion: Boolean; const iTipoComida, iQty: Integer ): integer;
var
   sLetrero: String;
begin
     Result:= RevisaChecada( lCancelacion, iTipoComida, iQty, sLetrero);
end;


function TFormaCafe.RevisaChecada( const lCancelacion: Boolean; const iTipoComida, iQty: Integer; var sLetrero: String ): integer;
var
   iTipo, iCantidad : Integer;
   lOk: Boolean;

   function GetTipoQty: Boolean;
   begin
        if ( PreguntaComida = nil ) then
           PreguntaComida := TPreguntaComida.Create( Application );

        with PreguntaComida do
        begin
             CREDENCIAL.Text := FActualCredencial;        // editNumero.Text; Se manda llamar despues de la Asignaci�n de la Variable
             COMIDA.Valor := iTipo;
             QTY.Valor := iCantidad;
             Invitador := EsInvitador;
             try
                FTimerOn := TRUE;
                ShowModal;
             finally
                FTimerOn := FALSE;
             end;
             FUltimaCredencial := CREDENCIAL.Text;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  iTipo := COMIDA.ValorEntero;
                  iCantidad := QTY.ValorEntero;
             end;
        end;
   end;

begin
     Result := 0;
     lOk := TRUE;
     iTipo := iTipoComida; // Defaults
     iCantidad := iQty;

     // Checada adicional
     if not lCancelacion then
        tgAnterior := dmCafeCliente.TipoGafete;

     with dmCafeCliente do
          if ( not lCancelacion ) and ( EsInvitador or ( UsaTeclado and ( PreguntaTipo or PreguntaQty ) ) ) then
          begin
               // Checada adicional
               if lChecadaManual then
                  TimerChecadaAlterna.Enabled := FALSE;
               lOk := GetTipoQty;
          end;
     if lOk {$ifdef MULT_TIPO_COMIDA} and ( ( not PideTipoComida ) or  ConfirmaChecada(sLetrero,iTipo) ){$endif} then
     begin
          // Pinta( Amarillo, RGB(251,223,30) );
          Pinta( RGB(233,178,0) );
          ShowResultado( 'Revisando...', clBlack );
          with dmCafeCliente do
          begin
               if dmCafeCliente.Active then
                  MuestraDatosEmpleado( iTipo, iCantidad )
               else
                  AgregaChecadaFueraLinea( editNumero.Text, IntToStr( iTipo ), iCantidad );
          end;
          Result := iCantidad;
     end
     else
         dmCafeCliente.CancelaOperacion:= TRUE;
end;

procedure TFormaCafe.MuestraDatosEmpleado( const iTipo, iCantidad: Integer );
var
   sGafete : String;
begin
     sGafete := editNumero.Text;
     if ZetaCommonTools.StrLleno( sGafete ) then
        sGafete := dmCafeCliente.CompletaGafete( sGafete );
     with dmCafeCliente do
     begin
         if ExisteEmpleado( sGafete, IntToStr( iTipo ), iCantidad ) then
         begin
              editNumero.Text := sGafete;
              lblNombre.Caption := GetNombreEmpleado;
              with cdsEmpleado do
              begin
                   if strLleno( FieldByName( 'CB_TURNO' ).AsString ) then
                      lblTurno.Caption := 'Turno: ' + FieldByName( 'CB_TURNO' ).AsString + ' - ' +
                                          FieldByName( 'TU_DESCRIP' ).AsString
                   else
                      lblTurno.Caption := '';

                   PanelFoto.Visible:= dmCafeCliente.MostrarFoto;

                   if PanelFoto.Visible then
                   begin
                       Foto.Picture := nil;
                       if  Assigned(dsFotoEmp.DataSet.FindField('FOTO')) then
                          LoadPictureFromBlobField(dsFotoEmp.DataSet.FieldByName('Foto') as TBlobField, Foto.Picture);
                       try
                           if not StrLleno(dsFotoEmp.DataSet.FieldByName('Foto').AsString) then
                              Foto.Picture := imagenGenerica.Picture;
                       except
                          on e: Exception do
                          begin
                            Foto.Picture := imagenGenerica.Picture;
                          end;
                       end;
                   end;
              end;

              gridComidas_DevEx.Visible := False;
              if HayComidas then
              begin
                   gridComidas_DevEx.Visible := True;
                   ConsumosPeriodo;
              end;
         end
         else
            lblConsumosPeriodo.Caption := '';
     end;
end;

procedure TFormaCafe.CancelaChecadaAnterior;
var
   sResultado : String;
   iCantidad : Integer;
begin
     with dmCafeCliente do
     begin
          iCantidad := LastCantidad;
          if ( iCantidad <= 0 ) or ( FTotalConsumos <= 0 ) then       // No Hay Checada Anterior o Fu� Cancelaci�n {OP: 07/06/08}
             ZetaDialogo.ZError( self.Caption, '�No se tiene checada anterior!', 0 )
          else
          begin
               try
                  editNumero.Text := DesCompletaGafete(LastGafete);

                  // Cambios por Checada adicional
                  dmCafeCliente.TipoGafete := tgAnterior;
                  iCantidad := RevisaChecada( TRUE, LastTipo, iCantidad * -1 );
                  dmCafeCliente.TipoGafete := CafeRegistry.TipoGafete;

                  if ValidacionOK( sResultado ) then
                  begin
                       IncrementaConsumos(iCantidad);
                       AceptaChecada( 'Cancel� Comida Anterior' );
                       ZetaDialogo.ZInformation( self.Caption, 'Se Cancel� Comida(s)' + CR_LF +
                                                               'Gafete : ' + LastGafete + CR_LF +
                                                               'Hora : ' + FormatDateTime( 'hh:nn', LastHora ) + CR_LF +
                                                               'Tipo de Comida : ' + IntToStr( LastTipo ) + CR_LF +
                                                               'Cantidad : ' + IntToStr( iCantidad ), 0 );
                       FAceptaCancelacion := FALSE;
                  end
                  else
                      RechazaChecada( sResultado );
               except
                   on E : Exception do
                      RechazaChecada( 'EXCEPTION ' + E.Message );
               end;
          end;
     end;
end;

{ Funciones y Metodos de Uso General }

function TFormaCafe.EsInvitador : Boolean;
begin
//     Result := Copy(editNumero.Text,2,2) = 'IV';
     with dmCafeCliente do
     begin
          Result := EsGafeteInvitador( CompletaGafete( FActualCredencial ) );   // Se manda llamar despues de la asignaci�n a la variable
     end;
end;

function TFormaCafe.GetNombreEmpleado : string;
begin
     with dmCafeCliente do
     begin
          if dmCafeCliente.Active then
             Result := cdsEmpleado.FieldByName( 'PrettyName' ).AsString
          else
              Result := '';
     end;
end;

{$ifdef MULT_TIPO_COMIDA}
{Obtiene el turno del empleado en el formato de Turno = Descripci�n}
function TFormaCafe.GetTurnoEmpleado: string;
begin
     with dmCafeCliente.cdsEmpleado do
     begin
          if dmCafeCliente.Active and StrLleno(FieldByName( 'CB_TURNO' ).AsString) then
             Result := FieldByName( 'CB_TURNO' ).AsString + ' - ' +
                       FieldByName( 'TU_DESCRIP' ).AsString
          else
              Result := VACIO;
     end;
end;

procedure TFormaCafe.gridComidas_DevExDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if Odd(AViewInfo.GridRecord.Index) then
  begin
    ACanvas.Brush.Color := RGB (247,247,247);
  end
  else
  begin
    ACanvas.Brush.Color := clWhite;
  end;

end;

{ procedure TFormaCafe.gridComidas_DevExDBTableViewCustomDrawColumnHeader(
  Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridColumnHeaderViewInfo; var ADone: Boolean);
begin
    ACanvas.Font.Style := [fsBold];
    ACanvas.Font.Size := 14;
    ACanvas.Brush.Color := RGB (226,226,226);
end; }

{ Regresa 'TRUE' si se tiene configuraci�n de 'Usar Teclado' y 'Preguntar Tipo de Comida'}
function TFormaCafe.PideTipoComida: Boolean;
begin
     Result:=( dmCafeCliente.UsaTeclado and dmCafeCliente.PreguntaTipo);
end;
{$endif}

procedure TFormaCafe.InitConsumos;
begin
     TIni := TTArchivosIni.Create;
     with Tini do
     begin
          if ( Date = FechaConsumos ) or ( NOT ZetaRegistryCafe.CafeRegistry.ReiniciaMediaNoche ) then{OP: 22.mayo.08}
              FTotalConsumos := Consumos
          else
              ReiniciaConsumo( 0 );

     end;
     // eTotalConsumos.Text := IntToStr( FTotalConsumos );
     // lblConsumos.Caption := 'Consumos de estaci�n: ' + IntToStr( FTotalConsumos );
     lblNumConsEstacion.Caption := IntToStr( FTotalConsumos );
end;

procedure TFormaCafe.IncrementaConsumos( const Incremento : integer );
begin
     Inc( FTotalConsumos, Incremento );
     with TIni do
     begin
          if ( Date = FechaConsumos ) or ( NOT ZetaRegistryCafe.CafeRegistry.ReiniciaMediaNoche ) then{OP: 22.mayo.08}
             Consumos := FTotalConsumos
          else
              ReiniciaConsumo( Incremento );
     end;
     // ETotalConsumos.Text := IntToStr( FTotalConsumos );
     // lConsumos.Caption := 'Consumos de estaci�n: ' + IntToStr( FTotalConsumos );
     lblNumConsEstacion.Caption := IntToStr( FTotalConsumos );
end;

procedure TFormaCafe.ReiniciaConsumo( const Incremento: integer );
begin
     SetArchivoBak;
     with TIni do
     begin
          {$ifdef BOSE}
          if ( Incremento = 0 ) then
          begin
               with dmCafeCliente do
               begin
                    AgregaChecadaContador( FTotalConsumos, 'Reinicio de Contador' );
               end;
          end;
          {$endif}
          FTotalConsumos := Incremento;
          Consumos := FTotalConsumos;
          FechaConsumos := Date;
          {OP: 07/06/08}
          // ETotalConsumos.Text := IntToStr( FTotalConsumos );
          // lblConsumos.Caption := 'Consumos de estaci�n: ' + IntToStr( FTotalConsumos );
          lblNumConsEstacion.Caption := IntToStr( FTotalConsumos );
     end;
end;

procedure TFormaCafe.ResetTimerConnect;
begin
   Desconectado.Enabled := not dmCafeCliente.ServerRunning;
end;

procedure TFormaCafe.SetArchivoBak;
var
   sFileBak, sFileIni: string;

begin
     sFileIni := ChangeFileExt( Application.ExeName, '.Ini' );
     sFileBak := ChangeFileExt( Application.ExeName, '.Log' );
     if FileExists( sFileBak ) then
        SysUtils.DeleteFile( sFileBak );
     RenameFile( sFileIni, sFileBak );
end;

procedure TFormaCafe.LimpiaDatos;
begin
     ShowResultado( '', clBlack );
     gridComidas_DevEx.Visible := FALSE;
     lblNombre.Caption := '';
     lblConsumosPeriodo.Caption := '';
     lblTurno.Caption := '';
     PanelFoto.Visible := FALSE;
     Foto.Picture := imagenGenerica.Picture;
end;

{$ifdef MULT_TIPO_COMIDA}
{Recupera los datos del empleado en caso de que se haya llamado a LimpiaDatos}
procedure TFormaCafe.RecuperaDatosEmpleado;
begin
     gridComidas_DevEx.Visible := TRUE;
     PanelFoto.Visible := TRUE;
     lblNombre.Caption := GetNombreEmpleado;
     lblTurno.Caption := 'Turno: ' + GetTurnoEmpleado;
     if strLleno (lblNombre.Caption) then
        ConsumosPeriodo
     else
        lblConsumosPeriodo.Caption := '';
end;
{$endif}

procedure TFormaCafe.AceptaChecada( const sLetrero : String );
begin
     SerialResultado( TRUE );
     // Pinta( Verde, clGreen );
     // Pinta( Verde, RGB(147,210,80) );
     Pinta( RGB(147,210,80) );
     ShowResultado( sLetrero, RGB(147,210,80) );
     SonidoAcepta;
end;

procedure TFormaCafe.btnChecadaManualClick(Sender: TObject);
begin
     if lChecadaManual then
     begin
          CancelarChecadaManual;
          lChecadaManual := FALSE;
     end
     else
     begin
          DetieneLector;
          CafeRegistry.TipoGafete := eTipoGafete (CafeRegistry.ChecadaManual);
          dmCafeCliente.TipoGafete := eTipoGafete (CafeRegistry.ChecadaManual);
          lChecadaManual := TRUE;
          FocusNumero;
          timerChecadaAlterna.Enabled := TRUE;
     end;
end;

procedure TFormaCafe.RechazaChecada( const sLetrero : String; bTimerPaseSuGafete: Boolean = TRUE );

  function FiltraMensaje( sMensaje : string ) : string;
  begin
    if StrPosI( sMensaje , 'EXCEPTION' ) > 0  then
      result := 'No fue posible procesar el consumo en el servidor'
    else
      result := sMensaje;
  end;

begin
     SerialResultado( FALSE );
     // Pinta( Rojo, RGB (227,66,51) );
     Pinta( RGB (227,66,51) );
     ShowResultado( FiltraMensaje( sLetrero ) , RGB (227,66,51) );
     SonidoRechaza;
     TimerPaseSuGafete.Enabled := bTimerPaseSuGafete;
end;

procedure TFormaCafe.WarningMessage( const sLetrero: String );
begin
     // Pinta( Amarillo, RGB(251,223,30) );
     Pinta( RGB(233,178,0) );
     ShowResultado( sLetrero, RGB (227,66,51) );
     //Sonido ... Si se quiere sonido para warning de comunicaci�n serial
end;

procedure TFormaCafe.OkMessage( const sLetrero: String );
begin
     // Pinta( Verde, RGB(147,210,80) );
     Pinta( RGB(147,210,80) );
     ShowResultado( sLetrero, RGB(147,210,80) );
end;

procedure TFormaCafe.panelEmpleadoResize(Sender: TObject);
const
    K_MIN_TOP_NOMBRE = 126;
    K_MAX_TOP_NOMBRE = 160; // ??
    K_MAX_SEP_LABELS = 65;
    K_MIN_SEP_LABELS = 40;
    K_PANEL_LIMITE = 400;
var
  iSeparacion: Integer;
  iTop: Integer;

begin
  inherited;
  imgTazaFondo.Width := imgTazaFondo.Height;

  iTop := K_MIN_TOP_NOMBRE;
  iSeparacion := K_MIN_SEP_LABELS;
  if panelEmpleado.Height >= K_PANEL_LIMITE then
  begin
    iTop :=  K_MAX_TOP_NOMBRE;
    iSeparacion := K_MAX_SEP_LABELS;
  end;

  lblNombre.Top := iTop;
  lblTurno.Top := lblNombre.Top + (iSeparacion*2);
  lblConsumosPeriodo.Top := lblTurno.Top + iSeparacion;

end;

procedure TFormaCafe.PanelFotoResize(Sender: TObject);
begin
    PanelFoto.Width := PanelFoto.Height;
end;

procedure TFormaCafe.panelRespuestaResize(Sender: TObject);
begin
  ajustarPanelRespuesta;
end;

procedure TFormaCafe.ajustarPanelRespuesta;
begin
    // Cambiar el tama�o de la letra.
    // Evaluar tama�o del texto.
    panelRespuesta.Font.Size := MAX_SIZE_FONT;

    if Length (panelRespuesta.Caption) > CANT_CARAC_PANEL then
    begin
      if panelRespuesta.Width < MIN_SIZE_PANEL then
          panelRespuesta.Font.Size := MIN_SIZE_FONT
      else if panelRespuesta.Width <= MAX_SIZE_PANEL then
          panelRespuesta.Font.Size := MED_SIZE_FONT;
    end;
end;

procedure TFormaCafe.repaint;
begin
    imgTazaFondo.Repaint;
    lblNombre.Repaint;
    lblTurno.Repaint;
    lblConsumosPeriodo.Repaint;
    LblConectado.Repaint;
    Application.ProcessMessages;
end;

procedure TFormaCafe.ErrorMessage( const sLetrero: String );
begin
     // Pinta( Rojo, RGB (227,66,51) );
     Pinta( RGB (227,66,51) );
     ShowResultado( sLetrero, RGB (227,66,51) );
end;

procedure TFormaCafe.Pinta(eColor : TColor);
begin
     panelRespuesta.Color := eColor;
     panelRespuesta.Font.Color := clWhite;
     Application.ProcessMessages;
end;

procedure TFormaCafe.ShowResultado( const sResultado : String; const eColor : TColor );
begin
     with panelRespuesta do
     begin
          if ZetaCommonTools.StrLleno( sResultado ) then
          begin
               Font.Size := ZetaCommonTools.iMin( MAX_SIZE_FONT, ZetaCommonTools.iMax( MIN_SIZE_FONT, Trunc( Width / Length( sResultado ) ) + 1 ) );
          end;
          Caption := ' '  + sResultado;
     end;
     Application.ProcessMessages;
     TimerPaseSuGafete.Enabled := TRUE;
     ajustarPanelRespuesta;
end;

function TFormaCafe.properCase(sBuffer: string):string;
var
    iLen, iIndex: integer;
begin
    iLen := Length(sBuffer);
    sBuffer:= Uppercase(MidStr(sBuffer, 1, 1)) + Lowercase(MidStr(sBuffer,2, iLen));
    {for iIndex := 0 to iLen do
    begin
      if MidStr(sBuffer, iIndex, 1) = ' ' then
          sBuffer := MidStr(sBuffer, 0, iIndex) + Uppercase(MidStr(sBuffer, iIndex + 1, 1)) + Lowercase(MidStr(sBuffer, iIndex + 2, iLen));
    end;}
    Result := sBuffer;
end;

procedure TFormaCafe.FocusNumero;
begin
     with editNumero do
     begin
          SelStart := 0;
          SelLength := Length( editNumero.Text );
          if ( Enabled and Visible ) then
              SetFocus;
     end;
end;

procedure TFormaCafe.ControlesEnter(Sender: TObject);
begin
     with editNumero do
     begin
          if ( Enabled and Visible ) then
              SetFocus;
     end;
end;

procedure TFormaCafe.SonidoAcepta;
begin
     SndPlaySound(Pchar(CafeRegistry.SonidoAcepta),SND_SYNC);
end;

procedure TFormaCafe.SonidoRechaza;
begin
     SndPlaySound(PChar(CafeRegistry.SonidoRechazo),SND_SYNC);
end;

{ Impresiones }

procedure TFormaCafe.ImprimeListado;
begin
     if FImpresion = NIL then
        FImpresion := TParametrosImpresion.Create(Self);
     FImpresion.ShowModal;
end;

procedure TFormaCafe.ImportaChecadas;
begin
     if FImportaChecadas = NIL then
        FImportaChecadas := TImportaChecadas.Create(Self);
     FImportaChecadas.ShowModal;
end;

{ Configurar Estaci�n }

procedure TFormaCafe.ConfiguraEstacion( const lInicial: Boolean = FALSE );
begin
     if ValidaPasswordOperador then
     begin
          editNumero.Text := '';
          if ( FormaConfigura = nil ) then
             FormaConfigura := TFormaConfigura.Create( Application );
          if lInicial then
             FormaConfigura.SetControlesServidor;
          if ( FormaConfigura.ShowModal = mrOK ) then
             LeeParametros
          else if ZetaCommonTools.StrVacio( CafeRegistry.Servidor ) then
             Close;
     end;

     // Checada adicional
     btnChecadaManual.Visible := FALSE;
     if CafeRegistry.TipoGafete = egBiometrico then
     begin
          btnChecadaManual.Visible := TRUE;
          // Correcci�n de Bug #12036: NELLCOR: No se recargan las huellas automaticamente en cafeteria.
          TimerValidar.Enabled := TRUE;
     end
     // Correcci�n de Bug #12036: NELLCOR: No se recargan las huellas automaticamente en cafeteria.
     else
     begin
          TimerValidar.Enabled := FALSE;
     end;
end;

{ M�todos P�blicos llamados de dmCafeCliente }

procedure TFormaCafe.ShowTipoConexion(const lConectado: Integer);
begin
     LblConectado.Caption := ESTADO_CONEXION[lConectado];
     //LblConectado.Width := 178;

     if lConectado > 0 then
     begin
          //LblConectado.AutoSize:= TRUE;
          LblConectado.Font.Color := RGB(147,210,80);
          imgEnFueraLinea.Picture := imgEnLinea.Picture;
     end
     else
     begin
          //LblConectado.AutoSize:= FALSE;
          LblConectado.Font.Color := RGB (227,66,51);
          imgEnFueraLinea.Picture := imgFueraLinea.Picture;
     end;

     repaint;
end;

procedure TFormaCafe.ShowProhibicion(const lProhibe: Boolean);
begin
     LimpiaDatos;
     if lProhibe then
        RechazaChecada( MESS_PROHIBE_ACCESO, FALSE )
     else
        AceptaChecada( '' );
end;

procedure TFormaCafe.CheckServidorDesconectado;
begin
     // BIOMETRICO
     TimerSensorBiometrico.Enabled := False;
     if ( CafeRegistry.TipoGafete = egBiometrico ) then
          DetieneLector;

     with CafeRegistry do
     begin
          // Si no se est� validando empleado, no mostrar forma de Servidor desconectado.
          if dmCafeCliente.ValidarEmpleado then
          begin
            case FServidorDesconectado.ShowDlgDesconectado( Servidor, Puerto ) of
                 osReintentar: LeeParametros;
                 osConfigurar: ConfiguraEstacion;
                 osSalir: Close;
            end;
          end;
     end;

     TimerSensorBiometrico.Enabled := True; // BIOMETRICO
end;

// Comunicaci�n con dispositivos seriales

procedure TFormaCafe.InitSerialControl;

   procedure InitCommControl( CommControl: TVaComm );
   begin
        with CommControl do
        begin
             if Active then
                Close;
             PortNum  := 0;
             OnRxFlag := nil;
        end;
        Sleep( 500 );  // Requiere de un timeout para que se libere el puerto de la computadora
   end;

begin
     FVaCommSalida := nil;
     InitCommControl( VaCommA );
     InitCommControl( VaCommB );
end;

procedure TFormaCafe.InitSerial( const lEntrada, lIguales: Boolean; const iPuerto: Integer );
begin
     with dmCafeCliente do
     begin
          if ( VaCommA.PortNum = 0 ) or ( VaCommA.PortNum = iPuerto ) then
             AbrePuerto( VaCommA, GetInfoPuerto( iPuerto, GetConfiguracion( lEntrada, lIguales ) ), lEntrada )
          else
              AbrePuerto( VaCommB, GetInfoPuerto( iPuerto, GetConfiguracion( lEntrada, lIguales ) ), lEntrada );
     end;
end;

procedure TFormaCafe.AbrePuerto( VaCommControl: TVaComm; PuertoInfo: TInfoPuerto; const lEntrada: Boolean );
begin
     with VaCommControl do
     begin
          if not Active then
          begin
               PortNum := PuertoInfo.PortNum;
               BaudRate := PuertoInfo.BaudRate;
               Databits := PuertoInfo.Databits;
               Parity := PuertoInfo.Parity;
               StopBits := PuertoInfo.StopBits;
               FlowControl.ControlDTR := PuertoInfo.DTRControl;
               FlowControl.ControlRTS := PuertoInfo.RTSControl;

               // TVaFlowControl = (fcNone, fcXonXoff, fcRtsCts, fcDtrDsr);
               FlowControl.XonXoffIn  := PuertoInfo.FlowControl = 1;  // fcXonXoff
               FlowControl.XonXoffOut := PuertoInfo.FlowControl = 1;  // fcXonXoff
               FlowControl.OutCtsFlow := PuertoInfo.FlowControl = 2;  // fcRtsCts
               //ToDo: Verificar el mapeo entre el FlowContol enumerado y el FlowControl Nuevo
               //FlowControl.ControlDtr := PuertoInfo.FlowControl = 3;  // fcDtrDsr

               if lEntrada then
               begin
                    EventChars.EventChar := PuertoInfo.EventChar;
                    OnRxFlag := VaCommARxFlag;
               end;
               try
                  Open;
               except
                     on Error: Exception do
                     begin
                          ShowMessage( VaCommControl.Name );
                          Application.HandleException( Error );
                          VaCommControl.Close;
                     end;
               end;
          end;
          if Active and not lEntrada then
          begin
               FVaCommSalida := VaCommControl;          // Direcciona para el Destino de las Salidas
               aSerialSalida[ FALSE ] := GetCodigosASCII( CafeRegistry.CodigoRechazo );
               aSerialSalida[ TRUE ] := GetCodigosASCII( CafeRegistry.CodigoAcepta );
          end;
     end;
end;

procedure TFormaCafe.VaCommABreak(Sender: TObject);
begin
     WarningMessage( 'Break Signal Detected...' );
end;

procedure TFormaCafe.VaCommAError(Sender: TObject; Errors: Integer);
begin
     case Errors of
          CE_BREAK: WarningMessage( sCE_BREAK );
          CE_DNS: WarningMessage( sCE_DNS );
          CE_FRAME: WarningMessage( sCE_FRAME );
          CE_IOE: WarningMessage( sCE_IOE );
          CE_MODE: WarningMessage( sCE_MODE );
          CE_OOP: WarningMessage( sCE_OOP );
          CE_OVERRUN: WarningMessage( sCE_OVERRUN);
          CE_PTO: WarningMessage( sCE_PTO);
          CE_RXOVER: WarningMessage( sCE_RXOVER);
          CE_RXPARITY: WarningMessage( sCE_RXPARITY);
          CE_TXFULL: WarningMessage( sCE_TXFULL);
     else
         WarningMessage( Format( 'Error # %d', [ Errors ] ) );
     end;
end;

procedure TFormaCafe.VaCommARx80Full(Sender: TObject);
begin
//     WarningMessage( 'Receiver Buffer Is 80% Full' );
end;

procedure TFormaCafe.VaCommATxEmpty(Sender: TObject);
begin
//     WarningMessage( 'TxEmpty Signal Detected...' );
end;

procedure TFormaCafe.VaCommARxFlag(Sender: TObject);
var
   sTexto : String;

   function EsFormaActiva( oForma: TCustomForm ): Boolean;
   begin
        Result := ( oForma <> nil ) and ( oForma.Visible ) and ( oForma.Active );
   end;

begin
     sTexto := TVaComm( Sender ).ReadText;
     sTexto := Trim( Copy( sTexto, 1, Length( sTexto ) - 1 ) );
     if ZetaCommonTools.StrLleno( sTexto ) then
     begin
          with CafeRegistry do
          begin
               case TipoGafete of
                    egProximidad:
                    begin
                         case LectorProximidad of
                              epSerial: sTexto := ZetaCommonTools.SerialToWiegand( sTexto );
                              epClockData: sTexto := ZetaCommonTools.ClockAndDataToWiegand( sTexto );
                         end;
                    end;
               end;
          end;
          if EsFormaActiva( PreguntaComida ) then
          begin
               ZetaClientTools.EnviaMensaje( PreguntaComida.ActiveControl, sTexto );
//               PreguntaComida.CREDENCIAL.Text := sTexto;
//               PreguntaComida.EscribirCambios;
          end
          else if EsFormaActiva( ConfirmaComida ) then
          begin
               ConfirmaComida.eCredencial.Text := sTexto;
               ConfirmaComida.Checada.Click;
//               EnviaMensaje( ConfirmaComida.ActiveControl, sTexto );
//               ConfirmaComida.SetChecada;
          end
          else if EsFormaActiva( self ) then
          begin
//               EditNumero.Text := sTexto;
               ZetaClientTools.EnviaMensaje( self.ActiveControl, sTexto );
//               ChecadaClick( self );
          end;
     end;
end;

procedure TFormaCafe.SeHaConectado(Sender: TObject);
var
   I, iConexion: Integer;
begin
     iConexion := K_CAFETERA_FUERA_LINEA_TEXT;
     with dmCafeCliente do
     begin
          if Active then
             iConexion := K_CAFETERA_EN_LINEA_TEXT;

          ShowTipoConexion(iConexion);

          for I := 0 to Screen.FormCount - 1 do
              PostMessage(Screen.Forms[I].Handle, WM_KILLWINDOW, 0, 0);
          AgregaChecadaServidor(ESTADO_CONEXION[iConexion]);
          RevisaLecturas;
          RevisaModulo;
          RevisaInvitadores;
     end;

     ResetTimerConnect;
     WizardConfiguracion;
     ActualizaParametros;
end;

procedure TFormaCafe.SeHaDesConectado(Sender: TObject);
var
   I, iConexion: Integer;
begin
     iConexion := K_CAFETERA_FUERA_LINEA_TEXT;
     with dmCafeCliente do
     begin
          if Active then
             iConexion := K_CAFETERA_EN_LINEA_TEXT;

          ShowTipoConexion(iConexion);
          AgregaChecadaServidor(ESTADO_CONEXION[iConexion]);
     end;

     ResetTimerConnect;
     CheckServidorDesconectado;
end;

procedure TFormaCafe.ErrorDeCliente(Sender: TObject);
var
  sMensaje : string;
begin
  // ToDo: Crear rutina de error

  sMensaje := Exception(Sender).Message;

  ShowResultado( K_MENSAJE_GENERAL_ERROR_CLIENTE_SERVIDOR, RGB (227,66,51) );
  //guardar el mensaje en alguna parte sMensaje

end;

procedure TFormaCafe.SerialResultado( const lOk: Boolean );
var
   lRes : Boolean;
begin
     if Assigned( FVaCommSalida ) then
     begin
          lRes := FVaCommSalida.WriteText( aSerialSalida[lOk] );
          if not lRes then
          WarningMessage( 'Error al Transmitir a Puerto Serial' )
          else
          begin
               lRes := True;
               if ( lOK ) and strLleno( CafeRegistry.ApagadoAcepta ) and ( CafeRegistry.EsperaAcepta <> 0 ) then
               begin
                    Sleep( CafeRegistry.EsperaAcepta );
                    lRes := FVaCommSalida.WriteText( GetCodigosASCII( CafeRegistry.ApagadoAcepta ) );
               end
               else
                   if strLleno( CafeRegistry.ApagadoRechazo ) and ( CafeRegistry.EsperaRechazo <> 0 ) then
                   begin
                        Sleep( CafeRegistry.EsperaAcepta );
                        lRes := FVaCommSalida.WriteText( GetCodigosASCII( CafeRegistry.ApagadoRechazo ) );
                   end;
               if not lRes then
               WarningMessage( 'Error al Transmitir a Puerto Serial' );
          end;
     end;
end;

{ -------------- Inicia TTArchivosIni ------------------ }

constructor TTArchivosIni.Create;
begin
     FIniFile := TIniFile.Create( ChangeFileExt( Application.ExeName, '.Ini' ) );
end;

destructor TTArchivosIni.Destroy;
begin
     FreeAndNil( FIniFile );
     inherited;
end;

function TTArchivosIni.GetConsumos: Integer;
begin
     Result := FiniFile.ReadInteger( SECCION, CONSUMO, 0 );
end;

procedure TTArchivosIni.SetConsumos(const Value: Integer);
begin
     FIniFile.WriteInteger( SECCION, CONSUMO, Value );
end;

function TTArchivosIni.GetFechaConsumos: TDatetime;
var
   wAnio, wMes, wDia: Word;
begin
     with FIniFile do
     begin
          wAnio := ReadInteger( SECCION, ANIO, 1 );
          wMes := ReadInteger( SECCION, MES, 1 );
          wDia := ReadInteger( SECCION, DIA, 1 );
          Result := EncodeDate( wAnio, wMes, wDia );
     end;
end;

procedure TTArchivosIni.SetFechaConsumos(const Value: TDatetime);
var
   wAnio, wMes, wDia: Word;
begin
     DecodeDate( Value, wAnio, wMes, wDia );
     with FIniFile do
     begin
          WriteInteger( SECCION, ANIO, wAnio );
          WriteInteger( SECCION, MES, wMes );
          WriteInteger( SECCION, DIA, wDia );
     end;
end;

{$ifdef BOSE}
procedure TFormaCafe.TimerContadorTimer(Sender: TObject);
var
   FActual: String;

begin
     FActual := FormatDateTime( 'hhmm', Now );
     if ( FActual <> FHoraContador ) then
     begin
          FHoraContador := FActual;
          with dmCafeCliente do
          begin
               ScanContador( FHoraContador );
               // ETotalConsumos.Text := IntToStr( FTotalConsumos );
               // lblConsumos.Caption := 'Consumos de estaci�n: ' + IntToStr( FTotalConsumos );
               lblNumConsEstacion.Caption := IntToStr( FTotalConsumos );
          end;
     end;
     TimerPregunta.Enabled := FTimerOn;
end;
{$endif}

procedure TFormaCafe.ConsumosPeriodo;
var i, iConsumosPeriodo: Integer;
begin
    iConsumosPeriodo := 0;
    for i := 0 to dmCafeCliente.dsComidas.DataSet.RecordCount - 1 do
    begin
        iConsumosPeriodo := iConsumosPeriodo + gridComidas_DevExDBTableView.DataController.Values[i, 3];
    end;
    lblConsumosPeriodo.Caption := 'Consumos: ' + IntToStr (iConsumosPeriodo);

    // Ajustar grid para mostrar hasta 12 registros.
    if dmCafeCliente.dsComidas.DataSet.RecordCount > 10 then
    begin
        gridComidas_DevEx.Font.Size := 13;
        gridComidas_DevExDBTableView.OptionsView.DataRowHeight := 26;
        gridComidas_DevEx.Font.Size := 15;
    end
    else
    begin
        gridComidas_DevEx.Font.Size := 16;
        gridComidas_DevExDBTableView.OptionsView.DataRowHeight := 31;
    end;
end;

// BIOMETRICO
procedure TFormaCafe.DesconectadoTimer(Sender: TObject);
var
  i : integer;
begin
    with Screen do
    begin
        for I := 0 to FormCount - 1 do
          PostMessage( Forms[I].Handle, WM_KILLWINDOW, 0, 0);
    end;
end;

procedure TFormaCafe.DetieneLector;
begin
     LogPantalla('Hace Close del Lector');
     if ( FDrivers ) then
        SFE.Close();

     switchWorking(False);
     TimerSensorBiometrico.Enabled := False;
end;

// BIOMETRICO
procedure TFormaCafe.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     // Checada adicional
     if lChecadaManual then
     begin
          CancelarChecadaManual;
     end;

     //Finaliza Lector Biometrico
     if ( CafeRegistry.TipoGafete = egBiometrico ) then
        DetieneLector;
end;

// BIOMETRICO
procedure TFormaCafe.TimerSensorBiometricoTimer(Sender: TObject);
var
   lPv : Boolean;

     function InicializaControl : Boolean;
     begin
          Result := False;
          try
             {if ( SFEStandard1 = nil ) then
             begin
                 SFEStandard1 := TSFEStandard.Create(self);
                 SFEStandard1.TabStop := False;
             end;}
            Result := True;
          except
                on E : Exception do
                     RechazaChecada( 'Driver de dispositivo biom�trico no detectado. ', FALSE );
          end;
     end;

begin
     TimerSensorBiometrico.Enabled := False; //Si no es biometrico, se apaga el timer
     LogPantalla('Termina ciclo de Timer');

     //IniciaLector solo si es biometrico y si no hay alguna ventana abierta de configuracion
     if ( CafeRegistry.TipoGafete = egBiometrico )
        and ( editNumero.Enabled ) then
     begin
          lPv := dmCafeCliente.PrimeraVez;
          dmCafeCliente.PrimeraVez := False;

          DetieneLector;

          if ( InicializaControl ) and ( LeeArchivo ) then
          begin
               if ( ( lPv ) and ( dmCafeCliente.Active ) ) then
               begin
                    ShowResultado( 'Lector biom�trico activo', RGB(147,210,80) );
                    SincronizaHuellas;
               end;

               InicializaSensorBiometrico;
          end;
     end;
end;

procedure TFormaCafe.TimerSiguienteComidaTimer(Sender: TObject);
var bConsultarSiguienteComida: Boolean;
begin
      bConsultarSiguienteComida := FALSE;
      if not Assigned (FormaConfigura) then
        bConsultarSiguienteComida := TRUE
      else if not FormaConfigura.Showing then
        bConsultarSiguienteComida := TRUE;
      if bConsultarSiguienteComida then
      begin
          dmCafeCliente.SiguienteComida;
      end;
end;

procedure TFormaCafe.paseSuGafete;
begin
    if not dmCafeCliente.ProhibirAcceso  then
    begin
      editNumero.Text := VACIO;
      PanelFoto.Visible := FALSE;
      lblNombre.Caption := VACIO;
      lblTurno.Caption := VACIO;
      lblConsumosPeriodo.Caption := VACIO;
      gridComidas_DevEx.Visible := FALSE;
      panelRespuesta.Color := RGB (195,195,195);

      if (CafeRegistry.TipoGafete = egBiometrico) then
        panelRespuesta.Caption := ' Puede poner su huella en el lector'
      else
        panelRespuesta.Caption := ' Pase su gafete o credencial';

      Foto.Picture := imagenGenerica.Picture;
    end;

    repaint;
end;

// BIOMETRICO
function TFormaCafe.LeeArchivo:Boolean;
var
   strDbFileName : String;
   nRet : Integer;
begin
     try
        strDbFileName := CafeRegistry.FileTemplates; //JB: Archivo por default Biometrico

        //Inicia con el archivo.
        LogPantalla( 'Abre Archivo: ' + strDbFileName);
        //if ( SFEStandard1 <> nil ) then
           {$ifdef ANTES}
           nRet := SFE.Open( strDbFileName, 4, 0)
           {$else}
           if ( FDrivers ) then
              nRet := SFE.Open( PChar( strDbFileName ), K_TIPO_BIOMETRICO, 0 );
           {$endif}

        if nRet = 0 then
        begin
             LogPantalla('Huellas: ' + IntToStr(SFE.GetEnrollCount()));
             //SFEStandard1.DotNET();
             Result := True;
        end
        else
        begin
             Raise Exception.Create( '�No hay dispositivo biom�trico!' );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                switchWorking(False);
                LogPantalla('Lector detenido: ' + Error.Message);
                RechazaChecada( Error.Message, FALSE );
                Result := False;
           end;
     end;
end;

// BIOMETRICO
procedure TFormaCafe.InicializaSensorBiometrico;
var
   nRet, userId, fingerNum: Integer;

   function EsFormaActiva( oForma: TCustomForm ): Boolean;
   begin
        Result := ( oForma <> nil ) and ( oForma.Visible ) and ( oForma.Active );
   end;

   procedure ObtieneLectura;
   label
        stopWorking;
   begin
        if ( LeeArchivo ) then
        begin
             try
                switchWorking(True);
                LogPantalla('Detectando');
                while gWorking do
                begin
                     Application.ProcessMessages();
                     // Resetear TimerPaseSuGafete
                     TimerPaseSuGafete.Enabled := FALSE;
                     TimerPaseSuGafete.Enabled := TRUE;
                     // Siempre en el caso de pedir poner la huella en el lector,
                     // pintar de gris el panel.
                     ShowResultado( 'Puede poner su huella en el lector', RGB(147,210,80) );
                     panelRespuesta.Color := RGB (195,195,195);
                     userId := 0;

                     nRet := CaptureFinger; //Devuelve algun mensaje de lectura o error.

                     if nRet <> 1 then
                        goto stopWorking;

                     nRet := SFE.Identify(userId, fingerNum); //Obtiene el Identificador de la huella y el dedo
                     LogPantalla(Format('Id: %d Dedo %d',[userId, fingerNum]));

                     if nRet < 0 then
                     begin
                          PrintError(nRet);
                          RechazaChecada( 'Huella no identificada' );
                          Sleep( K_1000 );
                     end
                     else
                     begin
                          editNumero.Text := IntToStr(userId);
                          WarningMessage( 'Identificando Huella...' );
                          goto stopWorking;
                     end;

                end;
stopWorking:
                switchWorking(False);
             except
                   on Error: Exception do
                   begin
                        Application.HandleException( Error );
                        switchWorking(False);
                        WarningMessage('Reinicie Cafeter�a');
                   end;
             end;
         end;
     end;

begin
     switchWorking(True);
     while gWorking do
     begin
          ObtieneLectura;
          if ( userId > 0 ) then
          begin
               ProcesaChecada;
               if ( FUltimaCredencial <> FActualCredencial ) then
               begin
                    Sleep( K_1000 ); //Este Tiempo, es para que el Invitador pueda ver la respuesta del Servidor;
                    editNumero.Text := FUltimaCredencial;
                    ProcesaChecada;
               end;
               FSegunda := TRUE;
          end;
          FocusNumero;
     end;
end;

// BIOMETRICO
procedure TFormaCafe.SincronizaHuellas;
var
   sArchivo, sArchivoTemplate : String;
   nRet, iHuellas, iId, iDedo : Integer;
   ss           :TStringStream;
   ms           :TMemoryStream;
   sLista : String;
   oCursor: TCursor;

const
     K_EXT_TEMPLATE = '.tem';

     Procedure AnexaHuella( iHuella : Integer );
     begin
          if ( StrLleno( sLista ) ) then
               sLista := sLista + ',' + intToStr(iHuella)
          else
               sLista := intToStr(iHuella);
     end;

begin
     // Desactivar timerPaseSuGafete cuando se sincroniza huellas.
     TimerPaseSuGafete.Enabled := FALSE;

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     editNumero.Enabled := False;
     try

        WarningMessage( 'Sincronizando espere un momento' );
        iId := 0;
        iDedo := 1;
        sLista := VACIO;
        //Proceso de Sincronizacion de Huellas
        sArchivo := CafeRegistry.FileTemplates;

        try
           ShowResultado( 'Sincronizando huellas, espere un momento', RGB(251,223,30) );
           dmCafeCliente.SincronizaHuellas; //Proceso para traer las huellas
           ms := TMemoryStream.Create;

           if ( not LeeArchivo ) then
              Exit;


           with dmCafeCliente.cdsHuellas do
           begin
                iHuellas := Recordcount;
                ShowResultado( 'Sincronizando ' + intToStr(iHuellas) + ' huellas, espere un momento', RGB(251,223,30) );

                iHuellas := 0;
                First;
                while not EOF do
                begin
                     inc( iHuellas );
                     try
                        iId := FieldByName( 'ID_NUMERO' ).AsInteger;
                        iDedo := FieldByName( 'HU_INDICE' ).AsInteger;
                        ss := TStringStream.Create( FieldByName( 'HU_HUELLA' ).AsString );

                        ms.Position := 0;
                        SZDecodeBase64( ss, ms );

                        sArchivoTemplate := ExtractFilePath( Application.ExeName ) + IntToStr( iId ) + '_' + IntToStr( iDedo ) +  K_EXT_TEMPLATE;
                        ms.SaveToFile( sArchivoTemplate);

                        nRet := SFE.LoadTemplateFromFile( PChar( sArchivoTemplate ), addrOfTemplate);

                        if nRet < 0 then
                        begin
                             raise Exception.Create(GetBioMessage(nRet));
                        end
                        else
                        begin
                             nRet := SFE.CheckFingerNum(iId, iDedo);
                             if nRet <> 0 then
                             begin
                                  nRet := SFE.TemplateEnroll( iId , iDedo, 0, addrOfTemplate);
                                  if ( ( nRet < 0 ) and ( nRet <> -102 ) ) then
                                  begin
                                       raise Exception.Create(GetBioMessage(nRet));
                                  end;

                                  AnexaHuella( FieldByName( 'HU_ID' ).AsInteger );
                             end;
                        end;

                     except
                        on Error: Exception do
                        begin
                             LogPantalla( Format( 'ERROR [%d_%d]: ' + Error.Message, [ iId, iDedo ] ) );
                        end;
                     end;

                     SysUtils.DeleteFile( sArchivoTemplate );

                     ShowResultado( 'Huella ' + intToStr(iHuellas) + '/' + intToStr(dmCafeCliente.cdsHuellas.Recordcount), RGB(251,223,30) );

                     Next;
                end;
           end;

           ms.Clear;
           Application.ProcessMessages();

           with dmCafeCliente do
           begin
                PrimeraVez := False;
                ReportaHuella( sLista );
           end;

           ShowResultado( 'Huellas Sincronizadas', RGB(147,210,80) );

           // Activar timerPaseSuGafete.
           TimerPaseSuGafete.Enabled := TRUE;
           Application.ProcessMessages();
        except
              on Error: Exception do
              begin
                   switchWorking(False);
                   LogPantalla( 'Lector inactivo. Error: ' + Error.Message );
                   WarningMessage('Reinicie Cafeteria. Error: ' + Error.Message);
              end;
        end;
     finally
            Screen.Cursor := oCursor;
            editNumero.Enabled := True;
     end;
end;

// BIOMETRICO
function TFormaCafe.CaptureFinger() : Integer;
var
  nRet, nCapArea, nMaxCapArea, nResult : Integer;
begin
     try
        Result := 0;
        nResult := 1;
        nMaxCapArea := 0;

        while True do
        begin
             try
                Application.ProcessMessages();
                if Not gWorking then
                begin
                     nResult := 0;
                     break;
                end;

                if ( FDrivers ) then
                   nRet := SFE.Capture();
                if nRet < 0 then
                begin
                     PrintError(nRet);
                     nResult := 0;
                     break;
                end;
                Application.ProcessMessages();
                if ( FDrivers ) then
                   nCapArea := SFE.IsFinger();
                if nCapArea >= 0 then
                begin
                     if nCapArea < nMaxCapArea + 2 then break;
                     if nCapArea > nMaxCapArea then nMaxCapArea := nCapArea;
                     if nCapArea > 45 then break;
                end;
                Application.ProcessMessages();
             except
                   Result := 0;
                   Exit;
             end;
        end;
        Application.ProcessMessages();

        if nResult = 1 then
        begin
             if ( FDrivers ) then
                nRet := SFE.GetImage(addrOfImageBytes);
             if nRet < 0 then
             begin
                  PrintError(nRet);
                  nResult := 0;
             end
        end;
        Application.ProcessMessages();
        CaptureFinger := nResult;
     except
           on E : Exception do
           begin
                DetieneLector;
                InicializaControles(true);
                InicializaBiometrico;
                Result := 0;
           end;
     end;
end;

procedure TFormaCafe.CF_HORAGetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  if StrVacio(AText) then
    AText := ''
  else
    AText := FormatMaskText('99:99;0', AText);
end;

procedure TFormaCafe.CF_TIPOGetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  if StrLleno(AText) then
  begin
      AText := AText + ': ' + ZetaRegistryCafe.CafeRegistry.TextoTipoComida(StrToInt (AText));
  end
  { else
    Text := Sender.AsString; }
end;

// BIOMETRICO
function TFormaCafe.GetBioMessage(error: Integer) : String;
begin
    case error of
        0: Result := 'Exito!';
        -1: Result := 'Error de imagen de huella!';
        -2: Result := 'Huella inv�lida!';
        -3: Result := 'Error de n�mero biom�trico!';
        -4: Result := 'Error de archivo de dispositivo!';
        -6: Result := 'Error de almacenamiento de dispositivo!';
        -7: Result := 'Error de sensor de dispositivo!';
        -8: Result := 'Orden de enrolamiento incorrecto!';
        -9: Result := 'No puede analizar las tres capturas, intente de nuevo!';
        -11: Result := 'La imagen no es un dedo!';
        -100: Result := 'No hay dispositivo biom�trico!';
        -101: Result := 'No puede abrir archivo de dispositivo local!';
        -102: Result := 'Ya ha enrolado esa huella!';
        -103: Result := 'Error de identificaci�n de huella!';
        -104: Result := 'Error de verificaci�n de huella!';
        -105: Result := 'No puede abrir la imagen de huella!';
        -106: Result := 'No puede crear imagen de huella!';
        -107: Result := 'No puede abrir huella!';
        -108: Result := 'No puede crear huella!';
        else Result := 'Error desconocido! (Numero =' + IntToStr(error) + ')';
    end;
end;

// BIOMETRICO
procedure TFormaCafe.LogPantalla(sMensaje : String);
begin
     lblMsg.Caption := sMensaje + CR_LF + lblMsg.Caption;
end;

// BIOMETRICO
procedure TFormaCafe.PrintError(error: Integer);
begin
     LogPantalla(GetBioMessage(error));
end;

// BIOMETRICO
procedure TFormaCafe.switchWorking(bWorking : Boolean);
begin
     gWorking := bWorking;
end;

// BIOMETRICO
procedure TFormaCafe.editNumeroKeyPress(Sender: TObject; var Key: Char);
begin
     // Cancelar checada con dispositivo adicional
     if (ord (Key) = 27 ) and (lChecadaManual) then
     begin
          CancelarChecadaManual;
     end
end;

// BIOMETRICO
procedure TFormaCafe.TimerValidarTimer(Sender: TObject);   var
   FActual: String;
begin
     // Correcci�n de Bug #12036: NELLCOR: No se recargan las huellas automaticamente en cafeteria.
     // TimerValidar.Enabled := False;
     FActual := FormatDateTime( 'hhmm', Now );
     if ( FActual <> FHoraCal ) then
     begin
          FHoraCal := FActual;
     end;

     try
        if ( CafeRegistry.TipoGafete = egBiometrico ) then
        begin
           // No revisar calendario s� se est� mostrando la forma de configuraci�n.
           // dmCafeCliente.ScanCalendario( FormatDateTime( 'hhmm', Now ) );
           if (not Assigned (FormaConfigura)) or (not FormaConfigura.Showing) then
              dmCafeCliente.ScanCalendario( FHoraCal );


           // Correcci�n de Bug #12036: NELLCOR: No se recargan las huellas automaticamente en cafeteria.
           if ( ( CafeRegistry.TipoGafete = egBiometrico ) and ( dmCafeCliente.Sync ) )
           and (dmCafeCliente.PrimeraVez) then
           begin
                // WarningMessage( 'Espere a la sincronizacion de Huellas' );
                dmCafeCliente.Sync := FALSE;     
                dmCafeCliente.PrimeraVez := FALSE;
                DetieneLector;
                Self.SincronizaHuellas;                
                // TimerSensorBiometrico.Enabled := True; 
                InicializaSensorBiometrico;           
           end;
        end;
     finally
            // Correcci�n de Bug #12036: NELLCOR: No se recargan las huellas automaticamente en cafeteria.
            // TimerValidar.Enabled := ( CafeRegistry.TipoGafete = egBiometrico );
     end;
end;

end.
