inherited FormaConfigura: TFormaConfigura
  Left = 467
  Top = 342
  Caption = 'Configuraci'#243'n de Cafeter'#237'a'
  ClientHeight = 447
  ClientWidth = 658
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 664
  ExplicitHeight = 476
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 411
    Width = 658
    ExplicitTop = 411
    ExplicitWidth = 658
    inherited OK_DevEx: TcxButton
      Left = 489
      Top = 6
      ParentFont = False
      OnClick = OK_DevExClick
      ExplicitLeft = 489
      ExplicitTop = 6
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 570
      Top = 6
      ExplicitLeft = 570
      ExplicitTop = 6
    end
  end
  object Configuracion_DevEx: TcxPageControl [1]
    Left = 0
    Top = 0
    Width = 658
    Height = 411
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Properties.ActivePage = General_DevEx
    Properties.CustomButtons.Buttons = <>
    OnChange = ConfiguracionChange
    ClientRectBottom = 409
    ClientRectLeft = 2
    ClientRectRight = 656
    ClientRectTop = 27
    object General_DevEx: TcxTabSheet
      Caption = 'General'
      ImageIndex = 0
      object PanelGeneral: TPanel
        Left = 3
        Top = 3
        Width = 435
        Height = 279
        BevelOuter = bvNone
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 0
        object Label1: TLabel
          Left = 39
          Top = 12
          Width = 76
          Height = 13
          Caption = 'Tipo de comida:'
        end
        object Label4: TLabel
          Left = 71
          Top = 64
          Width = 44
          Height = 13
          Caption = 'Estaci'#243'n:'
        end
        object Label15: TLabel
          Left = 6
          Top = 91
          Width = 109
          Height = 13
          Alignment = taRightJustify
          Caption = 'Checadas simult'#225'neas:'
        end
        object Label16: TLabel
          Left = 43
          Top = 142
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de gafete:'
        end
        object Label17: TLabel
          Left = 14
          Top = 168
          Width = 101
          Height = 13
          Alignment = taRightJustify
          Caption = 'Lector de proximidad:'
        end
        object lblTipoComida: TLabel
          Left = 145
          Top = 12
          Width = 66
          Height = 13
          Caption = 'lblTipoComida'
        end
        object Label30: TLabel
          Left = 42
          Top = 44
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'a media noche:'
        end
        object Label31: TLabel
          Left = 21
          Top = 31
          Width = 92
          Height = 13
          Alignment = taRightJustify
          Caption = 'Reiniciar consumos'
        end
        object Label32: TLabel
          Left = 24
          Top = 115
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Limpiar pantalla en:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label36: TLabel
          Left = 151
          Top = 115
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'segundos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object zTipoComida: TZetaTextBox
          Left = 118
          Top = 11
          Width = 20
          Height = 17
          AutoSize = False
          Caption = '1'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object zSimultaneas: TZetaTextBox
          Left = 118
          Top = 89
          Width = 147
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object zTiempoEspera: TZetaTextBox
          Left = 118
          Top = 114
          Width = 30
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object editID: TcxTextEdit
          Left = 117
          Top = 60
          Properties.MaxLength = 4
          TabOrder = 0
          Width = 149
        end
        object cbTipoGafete: TZetaKeyCombo
          Left = 118
          Top = 138
          Width = 147
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 1
          OnChange = cbTipoGafeteChange
          ListaFija = lfTipoGafete
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object cbLectorProximidad: TZetaKeyCombo
          Left = 118
          Top = 164
          Width = 147
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Enabled = False
          ParentCtl3D = False
          TabOrder = 2
          ListaFija = lfTipoProximidad
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object DefaultGB: TGroupBox
          Left = 0
          Top = 188
          Width = 435
          Height = 65
          Align = alCustom
          Caption = ' Defaults: '
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          object Label18: TLabel
            Left = 71
            Top = 17
            Width = 44
            Height = 13
            Alignment = taRightJustify
            Caption = 'Empresa:'
          end
          object Label19: TLabel
            Left = 62
            Top = 42
            Width = 53
            Height = 13
            Alignment = taRightJustify
            Caption = 'Credencial:'
          end
          object zEmpresa: TZetaTextBox
            Left = 118
            Top = 16
            Width = 27
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
          object zCredencial: TZetaTextBox
            Left = 118
            Top = 40
            Width = 27
            Height = 17
            AutoSize = False
            ShowAccelChar = False
            Brush.Color = clBtnFace
            Border = True
          end
        end
        object chkReiniciaMediaNoche: TcxCheckBox
          Left = 115
          Top = 34
          Enabled = False
          State = cbsChecked
          TabOrder = 4
          Transparent = True
          Width = 18
        end
      end
      object Letrero: TGroupBox
        Left = 3
        Top = 262
        Width = 435
        Height = 107
        Align = alCustom
        Caption = ' Letrero: '
        Ctl3D = False
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        object Label3: TLabel
          Left = 20
          Top = 72
          Width = 50
          Height = 13
          Caption = 'Velocidad:'
        end
        object zBanner: TZetaTextBox
          Left = 14
          Top = 26
          Width = 409
          Height = 17
          AutoSize = False
          Caption = 'zBanner'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object tbVelocidad_DevEx: TcxTrackBar
          Left = 76
          Top = 63
          TabOrder = 0
          Height = 35
          Width = 189
        end
      end
      object GroupBox5: TGroupBox
        Left = 441
        Top = 191
        Width = 208
        Height = 178
        Caption = 'Sincronizar configuraci'#243'n '
        Ctl3D = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        object lblHora: TLabel
          Left = 34
          Top = 52
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblFormato: TLabel
          Left = 110
          Top = 50
          Width = 86
          Height = 15
          Alignment = taRightJustify
          Caption = 'Formato de 24 horas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial Narrow'
          Font.Style = []
          ParentFont = False
        end
        object lblCada: TLabel
          Left = 32
          Top = 103
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cada:'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lblMinutos: TLabel
          Left = 114
          Top = 103
          Width = 32
          Height = 15
          Alignment = taRightJustify
          Caption = 'minutos'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial Narrow'
          Font.Style = []
          ParentFont = False
        end
        object SincronizarConfiguracion: TcxButton
          Left = 54
          Top = 141
          Width = 99
          Height = 26
          Hint = 'Sincronizar configuraci'#243'n de cafeter'#237'a'
          Caption = 'Sincronizar'
          OptionsImage.ImageIndex = 8
          OptionsImage.Images = cxImageList24_PanelBotones
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = SincronizarConfiguracionClick
        end
        object editSincronizacion: TZetaHora
          Left = 66
          Top = 50
          Width = 38
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 1
          Text = ''
          Tope = 24
        end
        object rbFrecuencia: TRadioButton
          Left = 17
          Top = 77
          Width = 113
          Height = 17
          Caption = 'Frecuencia'
          TabOrder = 2
          TabStop = True
          OnClick = rbFrecuenciaClick
        end
        object rbUnaVez: TRadioButton
          Left = 17
          Top = 27
          Width = 113
          Height = 17
          Caption = 'Una vez al d'#237'a'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TabStop = True
          OnClick = rbUnaVezClick
        end
        object editMinutos: TZetaNumero
          Left = 66
          Top = 100
          Width = 38
          Height = 21
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Mascara = mnDias
          ParentFont = False
          TabOrder = 3
          Text = '0'
        end
      end
      object UsaTeclado: TPanel
        Left = 519
        Top = 6
        Width = 39
        Height = 20
        BevelOuter = bvNone
        TabOrder = 3
        Visible = False
      end
      object cbUsaTeclado: TdxCheckGroupBox
        Left = 441
        Top = 5
        Caption = 'Usa teclado: '
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        Style.BorderStyle = ebsFlat
        Style.LookAndFeel.NativeStyle = True
        Style.LookAndFeel.SkinName = 'TressMorado2013'
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.SkinName = 'TressMorado2013'
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.SkinName = 'TressMorado2013'
        StyleHot.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.SkinName = 'TressMorado2013'
        TabOrder = 4
        Height = 81
        Width = 208
        object cbTipoComida: TcxCheckBox
          Left = 24
          Top = 17
          Caption = 'Preguntar tipo de comida'
          Enabled = False
          TabOrder = 0
          Transparent = True
          Width = 158
        end
        object cbCantidad: TcxCheckBox
          Left = 24
          Top = 42
          Caption = 'Preguntar cantidad'
          Enabled = False
          TabOrder = 1
          Transparent = True
          Width = 158
        end
      end
      object Vista: TGroupBox
        Left = 441
        Top = 92
        Width = 208
        Height = 93
        Caption = ' Vista de informaci'#243'n: '
        Ctl3D = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 5
        object cbMostrarFoto: TcxCheckBox
          Left = 24
          Top = 20
          Caption = 'Mostrar foto'
          Enabled = False
          ParentFont = False
          State = cbsChecked
          TabOrder = 0
          Transparent = True
          Width = 97
        end
        object cbMostrarComidas: TcxCheckBox
          Left = 24
          Top = 45
          Caption = 'Mostrar comidas'
          Enabled = False
          ParentFont = False
          State = cbsChecked
          TabOrder = 1
          Transparent = True
          Width = 97
        end
        object cbMostrarSigComida: TcxCheckBox
          Left = 24
          Top = 70
          Caption = 'Mostrar siguiente comida'
          Enabled = False
          ParentFont = False
          TabOrder = 2
          Transparent = True
          Width = 158
        end
      end
    end
    object Calendario_DevEx: TcxTabSheet
      Caption = 'Calendario'
      ImageIndex = 1
      object GridRenglones: TZetaDBGrid
        Left = 0
        Top = 82
        Width = 654
        Height = 259
        Align = alClient
        Ctl3D = False
        DataSource = dmCafeCliente.dsCalendario
        Enabled = False
        Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        ParentCtl3D = False
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = GridRenglonesColExit
        OnDrawColumnCell = GridRenglonesDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'HORA'
            Title.Caption = 'Hora'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ACCION'
            Title.Caption = 'Acci'#243'n'
            Width = 225
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONTADOR'
            Title.Alignment = taCenter
            Title.Caption = 'Iniciar contador'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SYNC'
            Title.Alignment = taCenter
            Title.Caption = 'Sinc. Huellas'
            Width = 80
            Visible = True
          end>
      end
      object pCalendario: TPanel
        Left = 0
        Top = 341
        Width = 654
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 3
        object Label8: TLabel
          Left = 8
          Top = 16
          Width = 106
          Height = 13
          Caption = 'Archivo de calendario:'
        end
        object ArchivoSeekCalendario_DevEx: TcxButton
          Tag = 1
          Left = 596
          Top = 12
          Width = 21
          Height = 21
          Hint = 'Indicar Archivo para Almacenar Calendario'
          OptionsImage.ImageIndex = 0
          OptionsImage.Images = cxImageList19_PanelBotones
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = ArchivoSeekClick
        end
        object EditCalendario_DevEx: TcxTextEdit
          Left = 118
          Top = 12
          Enabled = False
          Style.Color = clInfoBk
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clWindowText
          TabOrder = 0
          Width = 531
        end
      end
      object pBotones: TPanel
        Left = 0
        Top = 41
        Width = 654
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Agregar: TcxButton
          Left = 8
          Top = 9
          Width = 129
          Height = 26
          Caption = 'Agregar rengl'#243'n'
          Enabled = False
          OptionsImage.ImageIndex = 3
          OptionsImage.Images = cxImageList24_PanelBotones
          OptionsImage.Margin = 1
          TabOrder = 0
          OnClick = BBAgregarClick
        end
        object Borrar: TcxButton
          Left = 147
          Top = 9
          Width = 129
          Height = 26
          Caption = 'Borrar rengl'#243'n'
          Enabled = False
          OptionsImage.ImageIndex = 4
          OptionsImage.Images = cxImageList24_PanelBotones
          OptionsImage.Margin = 1
          TabOrder = 1
          OnClick = BBBorrarClick
        end
        object Modificar: TcxButton
          Left = 289
          Top = 9
          Width = 129
          Height = 26
          Caption = 'Modificar rengl'#243'n'
          Enabled = False
          OptionsImage.ImageIndex = 5
          OptionsImage.Images = cxImageList24_PanelBotones
          OptionsImage.Margin = 1
          TabOrder = 2
          OnClick = BBModificarClick
        end
      end
      object pEventos: TPanel
        Left = 0
        Top = 0
        Width = 654
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label29: TLabel
          Left = 19
          Top = 12
          Width = 57
          Height = 13
          Caption = 'Eventos de:'
        end
        object cbSemana: TZetaKeyCombo
          Left = 80
          Top = 8
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          OnChange = cbSemanaChange
          ListaFija = lfDiasCafeteria
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
      end
      object ZCombo: TZetaDBKeyCombo
        Left = 220
        Top = 128
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 4
        TabStop = False
        Visible = False
        ListaFija = lfCafeCalendario
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'ACCION'
        LlaveNumerica = True
      end
    end
    object TipoComidaTS_DevEx: TcxTabSheet
      Caption = 'Tipos de comida'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      object Label22: TLabel
        Left = 49
        Top = 68
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #3:'
      end
      object Label23: TLabel
        Left = 49
        Top = 92
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #4:'
      end
      object Label20: TLabel
        Left = 49
        Top = 20
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #1:'
      end
      object Label21: TLabel
        Left = 49
        Top = 44
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #2:'
      end
      object Label24: TLabel
        Left = 49
        Top = 116
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #5:'
      end
      object Label27: TLabel
        Left = 49
        Top = 188
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #8:'
      end
      object Label28: TLabel
        Left = 49
        Top = 212
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #9:'
      end
      object Label25: TLabel
        Left = 49
        Top = 140
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #6:'
      end
      object Label26: TLabel
        Left = 49
        Top = 164
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de comida #7:'
      end
      object zTipoComida1: TZetaTextBox
        Left = 147
        Top = 18
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida2: TZetaTextBox
        Left = 147
        Top = 42
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida3: TZetaTextBox
        Left = 147
        Top = 65
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida4: TZetaTextBox
        Left = 147
        Top = 89
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida5: TZetaTextBox
        Left = 147
        Top = 113
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida6: TZetaTextBox
        Left = 147
        Top = 137
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida7: TZetaTextBox
        Left = 147
        Top = 161
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida8: TZetaTextBox
        Left = 147
        Top = 185
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object zTipoComida9: TZetaTextBox
        Left = 147
        Top = 209
        Width = 389
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
    end
    object Server_DevEx: TcxTabSheet
      Caption = 'Servidor'
      ImageIndex = 3
      DesignSize = (
        654
        382)
      object FueraLinea: TGroupBox
        Left = 10
        Top = 98
        Width = 628
        Height = 87
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Fuera de l'#237'nea: '
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        DesignSize = (
          628
          87)
        object LblArchivo: TLabel
          Left = 28
          Top = 30
          Width = 94
          Height = 13
          Caption = 'Archivo de lecturas:'
        end
        object LblMessFuera: TLabel
          Left = 8
          Top = 53
          Width = 114
          Height = 13
          Caption = 'Mensaje de aprobaci'#243'n:'
        end
        object EditArchivo: TcxTextEdit
          Left = 128
          Top = 25
          Anchors = [akLeft, akTop, akRight]
          Enabled = False
          Style.Color = clInfoBk
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clWindowText
          TabOrder = 0
          Width = 487
        end
        object EditMessOK: TcxTextEdit
          Left = 128
          Top = 49
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
          Width = 487
        end
        object ArchivoSeek_DevEx: TcxButton
          Left = 586
          Top = 25
          Width = 21
          Height = 21
          Hint = 'Indicar Archivo de Lecturas Fuera de L'#237'nea'
          Anchors = [akLeft, akTop, akBottom]
          OptionsImage.ImageIndex = 0
          OptionsImage.Images = cxImageList19_PanelBotones
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = ArchivoSeekClick
        end
      end
      object Servidor: TGroupBox
        Left = 10
        Top = 8
        Width = 628
        Height = 84
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Servidor: '
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 0
        DesignSize = (
          628
          84)
        object Label6: TLabel
          Left = 521
          Top = 24
          Width = 34
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Puerto:'
          ExplicitLeft = 487
        end
        object Label34: TLabel
          Left = 29
          Top = 51
          Width = 44
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Intervalo:'
          ExplicitLeft = 17
        end
        object Label35: TLabel
          Left = 25
          Top = 23
          Width = 48
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Direcci'#243'n:'
          ExplicitLeft = 13
        end
        object lblSegundos: TLabel
          Left = 126
          Top = 51
          Width = 46
          Height = 13
          Caption = 'segundos'
        end
        object editDireccion: TcxTextEdit
          Left = 79
          Top = 20
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          Width = 433
        end
        object editPuerto: TZetaNumero
          Left = 561
          Top = 20
          Width = 54
          Height = 21
          Anchors = [akTop, akRight]
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
        object editIntervalo: TZetaNumero
          Left = 80
          Top = 47
          Width = 40
          Height = 21
          Anchors = [akTop, akRight]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Mascara = mnDias
          ParentFont = False
          TabOrder = 2
          Text = '0'
        end
      end
    end
    object Seguridad_DevEx: TcxTabSheet
      Caption = 'Seguridad'
      ImageIndex = 4
      DesignSize = (
        654
        382)
      object GroupBox2: TGroupBox
        Left = 11
        Top = 11
        Width = 634
        Height = 102
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Datos del administrador: '
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        object Label11: TLabel
          Left = 76
          Top = 24
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Gafete:'
        end
        object zGafeteAdmon: TZetaTextBox
          Left = 118
          Top = 21
          Width = 496
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object cbConcesionImprimir: TcxCheckBox
          Left = 115
          Top = 43
          Caption = 'Puede imprimir'
          Enabled = False
          TabOrder = 0
          Transparent = True
          Width = 97
        end
        object cbConcesionCancelar: TcxCheckBox
          Left = 115
          Top = 60
          Caption = 'Puede cancelar comidas'
          Enabled = False
          TabOrder = 1
          Transparent = True
          Width = 147
        end
      end
    end
    object Impresora_DevEx: TcxTabSheet
      Caption = 'Impresora'
      ImageIndex = 5
      DesignSize = (
        654
        382)
      object LblPuerto: TLabel
        Left = 56
        Top = 24
        Width = 34
        Height = 13
        Caption = 'Puerto:'
      end
      object EditImpresora: TcxTextEdit
        Left = 96
        Top = 20
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Width = 513
      end
      object Codigos: TGroupBox
        Left = 16
        Top = 56
        Width = 620
        Height = 105
        Anchors = [akLeft, akTop, akRight]
        Caption = ' C'#243'digos de impresi'#243'n: '
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 2
        DesignSize = (
          620
          105)
        object Label2: TLabel
          Left = 72
          Top = 28
          Width = 30
          Height = 13
          Caption = 'Inicial:'
        end
        object Label5: TLabel
          Left = 77
          Top = 52
          Width = 25
          Height = 13
          Caption = 'Final:'
        end
        object Label7: TLabel
          Left = 25
          Top = 76
          Width = 77
          Height = 13
          Caption = 'Salto de p'#225'gina:'
        end
        object EditCodigoInicial: TcxTextEdit
          Left = 107
          Top = 24
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          Width = 494
        end
        object EditCodigoFinal: TcxTextEdit
          Left = 107
          Top = 48
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
          Width = 494
        end
        object EditEject: TcxTextEdit
          Left = 107
          Top = 72
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
          Width = 494
        end
      end
      object bPUERTO_DevEx: TcxButton
        Left = 604
        Top = 20
        Width = 22
        Height = 21
        Hint = 'Impresoras de Red'
        OptionsImage.ImageIndex = 1
        OptionsImage.Images = cxImageList19_PanelBotones
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = bPUERTOClick
      end
    end
    object Sonidos_DevEx: TcxTabSheet
      Caption = 'Sonidos'
      ImageIndex = 6
      DesignSize = (
        654
        382)
      object GroupBox1: TGroupBox
        Left = 14
        Top = 17
        Width = 627
        Height = 81
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Sonidos: '
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        DesignSize = (
          627
          81)
        object Label9: TLabel
          Left = 14
          Top = 28
          Width = 57
          Height = 13
          Caption = 'Aceptaci'#243'n:'
        end
        object Label10: TLabel
          Left = 25
          Top = 52
          Width = 46
          Height = 13
          Caption = 'Rechazo:'
        end
        object EAcepta: TcxTextEdit
          Left = 77
          Top = 24
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          Width = 508
        end
        object ERechazo: TcxTextEdit
          Left = 77
          Top = 48
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
          Text = 'Chord.wav'
          Width = 508
        end
        object bSonidoAceptacion: TcxButton
          Tag = 2
          Left = 585
          Top = 23
          Width = 21
          Height = 21
          Hint = 'Buscar Archivo de Sonidos'
          OptionsImage.ImageIndex = 0
          OptionsImage.Images = cxImageList19_PanelBotones
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = ArchivoSeekClick
        end
        object cxButton1: TcxButton
          Tag = 3
          Left = 585
          Top = 48
          Width = 21
          Height = 21
          Hint = 'Buscar Archivo de Sonidos'
          OptionsImage.ImageIndex = 0
          OptionsImage.Images = cxImageList19_PanelBotones
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = ArchivoSeekClick
        end
      end
    end
    object Serial_DevEx: TcxTabSheet
      Caption = 'Interfase Serial'
      ImageIndex = 7
      DesignSize = (
        654
        382)
      object Label14: TLabel
        Left = 33
        Top = 51
        Width = 79
        Height = 13
        Caption = 'Puerto de &salida:'
        FocusControl = CBPuertoB
      end
      object Label13: TLabel
        Left = 24
        Top = 24
        Width = 88
        Height = 13
        Caption = 'Puerto de &entrada:'
        FocusControl = CBPuertoA
      end
      object CBPuertoB: TZetaKeyCombo
        Left = 116
        Top = 47
        Width = 100
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 2
        OnChange = CBPuertoAChange
        ListaFija = lfPuertoSerial
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object CBPuertoA: TZetaKeyCombo
        Left = 116
        Top = 20
        Width = 100
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        OnChange = CBPuertoAChange
        ListaFija = lfPuertoSerial
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object GBCodigosApagados: TGroupBox
        Left = 13
        Top = 175
        Width = 628
        Height = 82
        Anchors = [akLeft, akTop, akRight]
        Caption = ' C'#243'digos de apagado: '
        Ctl3D = True
        ParentCtl3D = False
        TabOrder = 5
        DesignSize = (
          628
          82)
        object LblCodigoApagadoRechazo: TLabel
          Left = 24
          Top = 49
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Rechazo:'
        end
        object LblCodigoApagadoAceptado: TLabel
          Left = 13
          Top = 24
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aceptaci'#243'n:'
        end
        object LblEsperaAceptado: TLabel
          Left = 500
          Top = 24
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Espera:'
          ExplicitLeft = 464
        end
        object LblEsperaRechazado: TLabel
          Left = 500
          Top = 49
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Espera:'
          ExplicitLeft = 464
        end
        object EditApagadoRechazo: TZetaEdit
          Left = 77
          Top = 44
          Width = 413
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
        end
        object EditApagadoAceptado: TZetaEdit
          Left = 77
          Top = 20
          Width = 413
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
        end
        object EditEsperaAceptado: TZetaNumero
          Left = 542
          Top = 20
          Width = 73
          Height = 21
          Anchors = [akTop, akRight]
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
        object EditEsperaRechazo: TZetaNumero
          Left = 542
          Top = 44
          Width = 73
          Height = 21
          Anchors = [akTop, akRight]
          Mascara = mnDias
          TabOrder = 3
          Text = '0'
        end
      end
      object GBCodigosSalida: TGroupBox
        Left = 13
        Top = 80
        Width = 628
        Height = 81
        Anchors = [akLeft, akTop, akRight]
        Caption = ' C'#243'digos de mensajes de salida: '
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 4
        DesignSize = (
          628
          81)
        object LblCodigoAcepta: TLabel
          Left = 13
          Top = 28
          Width = 57
          Height = 13
          Caption = '&Aceptaci'#243'n:'
          FocusControl = EditSerialAcepta
        end
        object LblCodigoRechazo: TLabel
          Left = 24
          Top = 52
          Width = 46
          Height = 13
          Caption = '&Rechazo:'
          FocusControl = EditSerialRechazo
        end
        object EditSerialAcepta: TcxTextEdit
          Left = 76
          Top = 24
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 0
          Width = 539
        end
        object EditSerialRechazo: TcxTextEdit
          Left = 76
          Top = 48
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
          Width = 539
        end
      end
      object BtnConfigA_DevEx: TcxButton
        Left = 222
        Top = 20
        Width = 22
        Height = 21
        Hint = 'Configurar Puerto Serial'
        OptionsImage.ImageIndex = 2
        OptionsImage.Images = cxImageList19_PanelBotones
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtnConfigAClick
      end
      object BtnConfigB_DevEx: TcxButton
        Tag = 1
        Left = 222
        Top = 47
        Width = 22
        Height = 21
        Hint = 'Configurar Puerto Serial'
        OptionsImage.ImageIndex = 2
        OptionsImage.Images = cxImageList19_PanelBotones
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtnConfigAClick
      end
    end
    object Biometrico_DevEx: TcxTabSheet
      Caption = 'Biom'#233'trico'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 8
      ParentFont = False
      DesignSize = (
        654
        382)
      object GroupBox3: TGroupBox
        Left = 15
        Top = 11
        Width = 624
        Height = 150
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Huellas: '
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        DesignSize = (
          624
          150)
        object Label33: TLabel
          Left = 39
          Top = 16
          Width = 109
          Height = 13
          Caption = 'Almacenamiento local: '
        end
        object PathTemplates: TcxTextEdit
          Tag = 5
          Left = 156
          Top = 12
          Hint = 'Ruta de Archivo de Almacenamiento de Huellas'
          Anchors = []
          Enabled = False
          TabOrder = 0
          Width = 426
        end
        object btnBrowseDirectoryBiometrico_DevEx: TcxButton
          Tag = 5
          Left = 585
          Top = 12
          Width = 21
          Height = 21
          Hint = 'Indicar Archivo de Almacenamiento de Huellas'
          OptionsImage.ImageIndex = 0
          OptionsImage.Images = cxImageList19_PanelBotones
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = ArchivoSeekClick
        end
        object gbRegenerarArchivoHuellas: TcxGroupBox
          Left = 39
          Top = 39
          Caption = 'Regenerar archivo de huellas'
          TabOrder = 2
          Height = 98
          Width = 514
          object lblAutomatico: TLabel
            Left = 250
            Top = 25
            Width = 56
            Height = 13
            Alignment = taRightJustify
            Caption = 'Autom'#225'tico:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblFormato24: TLabel
            Left = 326
            Top = 48
            Width = 97
            Height = 13
            Alignment = taRightJustify
            Caption = 'Formato de 24 horas'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblManual: TLabel
            Left = 27
            Top = 25
            Width = 38
            Height = 13
            Alignment = taRightJustify
            Caption = 'Manual:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lblHoraRegenerarArchivoHuellas: TLabel
            Left = 250
            Top = 48
            Width = 26
            Height = 13
            Alignment = taRightJustify
            Caption = 'Hora:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object RecargaHuellas_DevEx: TcxButton
            Left = 27
            Top = 44
            Width = 115
            Height = 26
            Hint = 'Recargar Huellas de Servidor'
            Caption = 'Recargar huellas'
            OptionsImage.ImageIndex = 7
            OptionsImage.Images = cxImageList24_PanelBotones
            OptionsImage.Margin = 1
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = RecargaHuellasClick
          end
          object hrRegenerarArchivoHuellas: TZetaHora
            Left = 282
            Top = 47
            Width = 38
            Height = 19
            EditMask = '99:99;0'
            TabOrder = 1
            Text = ''
            Tope = 24
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 15
        Top = 179
        Width = 624
        Height = 110
        Caption = ' Lector adicional al dispositivo biom'#233'trico: '
        TabOrder = 1
        object Label37: TLabel
          Left = 47
          Top = 32
          Width = 24
          Height = 13
          Caption = 'Tipo:'
        end
        object Label38: TLabel
          Left = 5
          Top = 59
          Width = 66
          Height = 13
          Caption = 'Tiempo l'#237'mite:'
        end
        object Label39: TLabel
          Left = 113
          Top = 59
          Width = 46
          Height = 13
          Caption = 'segundos'
        end
        object cbChecadaManual_BK: TZetaKeyCombo
          Left = 204
          Top = 28
          Width = 147
          Height = 21
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 2
          Visible = False
          Items.Strings = (
            'C'#243'digo de barra'
            'Proximidad'
            'N'#250'mero biom'#233'trico')
          ListaFija = lfTipoGafete
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object numChecadaManualTimer: TZetaNumero
          Left = 78
          Top = 55
          Width = 30
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 1
          Text = ''
        end
        object cbChecadaManual: TcxComboBox
          Left = 77
          Top = 28
          Properties.DropDownListStyle = lsFixedList
          Properties.Items.Strings = (
            'C'#243'digo de barra'
            'Proximidad'
            'N'#250'mero biom'#233'trico')
          Properties.ReadOnly = False
          TabOrder = 0
          Width = 121
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 6947159
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF98A1E2FFF6F7FDFFB7BEEBFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFB1B8
          E9FFF6F7FDFFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF8792DEFFDFE2F6FFA1A9E5FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8
          A1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF8CE2B8FFE9F9
          F1FF54D396FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF52D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF52D395FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF5AD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF62D79FFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4FD293FF54D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF8AE1B7FFE6F9F0FF6AD9A4FF4FD293FF4FD293FF4FD2
          93FF5FD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF52D395FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF65D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF68D8A2FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF6AD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF52D395FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF70DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF57D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF81DFB1FF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF5261CFFF6C79D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6F7CD7FF7582D9FF6471D4FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFF969FE2FF6471D4FF969FE2FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4757CCFF4757CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4757CCFF4757CCFF6F7CD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF6976D6FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF5564D0FF7582D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF53D395FFCBF2DFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD9F5
          E7FF5BD59AFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF84DFB3FFFFFFFFFF95E4BDFF92E3BCFF92E3
          BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF95E4BDFFF7FD
          FAFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FFE9F9
          F1FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF9DE6C2FFF7FDFAFFFFFFFFFFFFFFFFFFF7FD
          FAFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFAFEFCFFFFFFFFFFFFFFFFFFFCFEFDFF8AE1
          B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFFFFFFFFFFCFEFDFF8AE1B7FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF92E3BCFFFFFFFFFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFFFFFFFFFFCFEFDFF8AE1B7FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF7FDEB0FFFFFFFFFF9AE5C1FF92E3BCFF92E3
          BCFF92E3BCFF92E3BCFF95E4BDFFFFFFFFFF8FE2BAFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFB5ECD1FFFAFEFCFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF9DE6C2FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFE8EAF9FFA4AC
          E6FFC6CBEFFFFFFFFFFFFFFFFFFFD1D5F2FFA4ACE6FFDDE0F5FFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
          CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
          CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
          CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
          CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
          CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
          CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
          CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF8D97DFFF8D97DFFF8D97DFFF8D97
          DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97
          DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
          FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
          FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
          EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
          E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
          F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
          F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
          EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
          F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
          F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF64D0F2FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
          F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
          F7FF83DAF4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FFC7EEFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF3FBFEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FFF3FBFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF20BCECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF20BCECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF4CC9F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF50CAF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF74D5F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF9FE2F7FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FFABE6F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFCBEFFBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FFD3F2FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF3FBFEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF20BC
          ECFF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF20BC
          ECFF20BCECFF20BCECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFBFECF9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF64D0F2FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
          F7FF78D6F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFB5CDFFFFB1CBFFFF5B92FFFFAFCAFFFFADC8FFFF5D93FFFF5C93
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFFBAD1FFFFA9C7FFFF79A6FFFFCEDFFFFFB5CDFFFF689AFFFFB8CFFFFFA9C5
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFCADCFFFF6095FFFFCFDF
          FFFFA4C2FFFF95B8FFFFC9DBFFFF6599FFFFADC9FFFFC1D6FFFF9EBEFFFF8CB2
          FFFF72A1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF7FAAFFFFD6E4FFFF7EA9FFFFDFEA
          FFFF72A1FFFFC4D8FFFF6397FFFFBAD1FFFFB8CFFFFF6C9DFFFFDBE7FFFFD9E6
          FFFF72A1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFD7E4FFFF689BFFFFDEE9FFFF6A9C
          FFFFC4D8FFFF6397FFFFB4CDFFFFA9C6FFFFA0BFFFFFCCDDFFFF74A3FFFF82AC
          FFFFBAD1FFFF70A0FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFE6EEFFFF6397FFFFE4EDFFFF72A1
          FFFFB2CCFFFF87AFFFFFA8C5FFFF7FAAFFFFC9DBFFFF689BFFFFDDE8FFFFD6E3
          FFFF75A3FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF6095FFFFD5E3FFFFBAD1FFFFD6E3FFFFB4CE
          FFFFB4CDFFFFB4CDFFFF6599FFFFC6D9FFFF689BFFFFC9DBFFFF77A4FFFF7AA7
          FFFFC4D8FFFF7AA7FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5D93FFFFDCE8FFFFBBD2FFFFCEDEFFFFB2CC
          FFFFA9C6FFFFBAD1FFFF689BFFFF6196FFFFB8CFFFFFADC8FFFFB2CCFFFFBCD3
          FFFF6599FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFE0EAFFFF679BFFFFE9F1FFFF7AA7
          FFFFA8C5FFFF72A1FFFFD1E0FFFFCCDDFFFFC0D5FFFF72A1FFFFC6D9FFFF5B92
          FFFFB7CFFFFF6D9EFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFDCE8FFFF6598FFFFCEDEFFFF699C
          FFFFC1D6FFFF82ACFFFF5E94FFFF689BFFFF89B1FFFFCEDFFFFF6A9CFFFFB8D0
          FFFFB8D0FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF6599FFFFD1E1FFFF86AFFFFFCEDEFFFF79A6FFFFC1D6
          FFFF679AFFFFBAD1FFFFC4D7FFFFCADBFFFFB2CCFFFF6397FFFFBDD3FFFFB5CE
          FFFF5C93FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFFDEE9FFFF699CFFFFE6EEFFFF79A6FFFF82AC
          FFFFCCDDFFFFB0CAFFFF6297FFFF5F95FFFFA5C3FFFFC4D8FFFF92B7FFFF5F95
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF75A3FFFFDFEAFFFF6A9DFFFFC9DBFFFF8EB4
          FFFF6095FFFFB7CFFFFFBED3FFFFC5D8FFFFB5CDFFFF6598FFFF84ADFFFFCCDD
          FFFF6599FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC6D9FFFFB5CEFFFF5B92FFFFA0C0
          FFFFC4D8FFFFB3CCFFFF7FAAFFFF7CA9FFFFBAD1FFFFC7D9FFFFA3C2FFFF6095
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC6D9FFFFE5EDFFFF75A3
          FFFF5C93FFFFA9C6FFFFB9D1FFFFB6CEFFFF9BBDFFFF6397FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF77A4FFFFD7E5FFFF6C9DFFFFD8E5
          FFFFE1EBFFFFE1EBFFFFD8E5FFFFD7E4FFFFDAE6FFFFDEE9FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5D93FFFFBFD4FFFFC9DBFFFF85AE
          FFFF6095FFFF70A0FFFF7FAAFFFF689BFFFF5D93FFFF5D93FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFFD5E3
          FFFFE1EBFFFFE3ECFFFFDBE7FFFF75A3FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94
          FFFF5D93FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF91B6FFFF82ACFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF91B6FFFFFCFDFFFF84ADFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF91B6FFFFFCFDFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFFA3C2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFA3C2FFFFFFFFFFFFA3C2FFFF84AD
          FFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF84AD
          FFFF94B8FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFA3C2FFFF84ADFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF70A0FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF70A0FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF70A0FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF6599FFFFADC9FFFFADC9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6A9CFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFE5EEFFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFFFFFFFFFB2CCFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFFFFFFFFFFE5EE
          FFFF6A9CFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5EEFFFF6599FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1
          FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFFFFFFFFFFFFF
          FFFFB7CFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFFFFFFFFFB7CF
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFB7CFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      end>
  end
  object OpenDialogSonidos: TOpenDialog
    DefaultExt = 'dat'
    Filter = 'Archivos de Sonidos (*.wav)|*.wav|Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo'
    Left = 397
    Top = 210
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'dat'
    Filter = 
      'Archivos de Datos (*.dat)|*.dat|Archivos de Texto (*.txt)|*.txt|' +
      'Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo'
    Left = 349
    Top = 154
  end
  object cxImageList19_PanelBotones: TcxImageList
    Height = 24
    Width = 24
    FormatVersion = 1
    DesignInfo = 3801431
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFDDF3FAFFDDF3FAFFDDF3
          FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFF14B8
          EBFF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF10B7EAFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF3FC4
          EEFF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF3FC4EEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF6AD1
          F0FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF6AD1F0FFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF92DD
          F5FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF96DFF5FFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFBDEA
          F7FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FFC9EDF9FFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFE8F6
          FAFF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF53CAEFFF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
          F0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
          F0FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFC
          FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFC
          FCFFBDEAF7FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FFC1EBF8FFDDF3FAFFDDF3FAFFDDF3
          FAFF6ED2F1FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          000000B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFF99BBFFFF99BBFFFF99BBFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFFAFCFFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4
          FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4
          FFFFD6E4FFFFF0F5FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFEAF1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFC1D6FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFEAF1FFFF5B92FFFF5B92FFFFD8E5FFFFEAF1FFFFEAF1
          FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1FFFF5B92
          FFFF5B92FFFFC1D6FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFEAF1FFFFD8E5FFFF5B92FFFF5B92FFFFEAF1FFFFE5EEFFFF99BB
          FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFFD8E5FFFFFFFFFFFF5B92
          FFFF5B92FFFFB4CDFFFFEAF1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFE5EEFFFF99BB
          FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFFD8E5FFFFFFFFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFD6E4FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC1D6FFFFFFFFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF5F8FFFFD6E4
          FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFF0F5FFFFFFFFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFF5F8FFFFD6E4
          FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFF0F5FFFFFFFFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFDBE7FFFF70A0
          FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFFC9DBFFFFFFFFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF7FAAFFFF84ADFFFF84AD
          FFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF84ADFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA69FA7FFFDFDFDFFFFFFFFFFB7B0B7FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFB7B0B7FFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFB7B0B7FFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFB7B0B7FFFFFFFFFFFFFFFFFFC5C0C6FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF948B
          95FFD2CED2FFFFFFFFFFFFFFFFFFD9D5D9FF968D97FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF908791FFD7D4D7FFFDFD
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDFFD9D5D9FF968D97FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF988F99FFEBE9EBFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F3F4FFA39B
          A3FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8D838EFFE6E3E6FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F3
          F4FF928993FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFBCB6BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFCAC6CBFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFE9E7E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF8F7F8FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF928993FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFA199A2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF968D97FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0
          B7FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0B7FFB7B0
          B7FFB7B0B7FF9B939CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFF0EFF1FFE4E1
          E4FF8B818CFF8B818CFF8B818CFF8B818CFFD7D4D7FFF0EFF1FFBEB8BFFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EF
          F1FF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFC5C0C6FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EF
          F1FF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFC5C0C6FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EF
          F1FF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFC5C0C6FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EF
          F1FF8B818CFF8B818CFF8B818CFF8B818CFFE2DFE2FFFFFFFFFFC5C0C6FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFAF9FAFFDBD7
          DBFF8B818CFF8B818CFF8B818CFF8B818CFFC8C4C9FFFBFBFBFFB3ACB4FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      end>
  end
end
