object dmASTA: TdmASTA
  OldCreateOrder = False
  Left = 516
  Top = 106
  Height = 296
  Width = 217
  object adCalendario: TAstaDataSet
    StreamOptions = [ssIndexes, ssAggregates]
    Indexes = <>
    Aggregates = <>
    Constraints = <>
    Left = 24
    Top = 16
    FastFields = ()
    object adCalendarioHORA: TStringField
      Alignment = taRightJustify
      FieldName = 'HORA'
      EditMask = '!90:00;0'
      Size = 4
    end
    object adCalendarioACCION: TSmallintField
      Alignment = taLeftJustify
      FieldName = 'ACCION'
    end
    object adCalendarioCONTADOR: TStringField
      Alignment = taCenter
      FieldName = 'CONTADOR'
      Size = 1
    end
    object adCalendarioSEMANA: TSmallintField
      FieldName = 'SEMANA'
    end
    object adCalendarioSYNC: TStringField
      Alignment = taCenter
      FieldName = 'SYNC'
      Size = 1
    end
  end
  object cdsADO: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 104
    Top = 16
  end
  object adIdInvita: TAstaDataSet
    StreamOptions = [ssIndexes, ssAggregates]
    Indexes = <>
    Aggregates = <>
    Constraints = <>
    Left = 24
    Top = 72
    FastFields = ()
  end
  object adContador: TAstaDataSet
    StreamOptions = [ssIndexes, ssAggregates]
    Indexes = <>
    Aggregates = <>
    Constraints = <>
    Left = 24
    Top = 184
    FastFields = ()
    object adContadorHORA: TStringField
      FieldName = 'HORA'
      EditMask = '!90:00;0'
      Size = 4
    end
  end
  object adHuellas: TAstaDataSet
    StreamOptions = [ssIndexes, ssAggregates]
    Indexes = <>
    Aggregates = <>
    Constraints = <>
    Left = 24
    Top = 128
    FastFields = ()
  end
end
