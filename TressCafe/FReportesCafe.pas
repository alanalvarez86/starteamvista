unit FReportesCafe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls,
  MaskUtils,
  DB;

type
  eStatusImpresora = (siLista, siOffLine, siSinPapel, siApagada, siDesconocido );
  TReportesCafe = class
  private
    { Private declarations }
    FImpresora  : TextFile;
    FTotales,FDataSet : TDataSet;
    FPagina,FLineas : integer;
    FPuerto: string;
    FSaltoPagina: string;
    FCodigoInicial: string;
    FCodigoFinal: string;
    FEstacion: string;
    procedure AbreImpresora;
    procedure CierraImpresora;
    procedure ImprimeEncabezado;
    procedure ImprimeDetalle;
    procedure ImprimeTotales;
    procedure ImprimeFueraLinea;
    procedure EscribeDato(sTexto: string);
    procedure EscribeDatoOEM(sTexto: string);
    procedure BajaRenglon;
    procedure Raya(const iCuantos: integer);
    procedure MandaCodigosFinales;
    procedure Eject;
    function GetStatusImpresora: eStatusImpresora;
    procedure ErrorDeImpresion(const eStatus: eStatusImpresora);
    procedure ExcepcionImpresion(const ErrorCode: integer);
    function HayDetalle: Boolean;
  public
    property SaltoPagina : string read FSaltoPagina write FSaltoPagina;
    property CodigoInicial : string read FCodigoInicial write FCodigoInicial;
    property CodigoFinal : string read FCodigoFinal write FCodigoFinal;
    property Puerto : string read FPuerto write FPuerto;
    property Estacion : string read FEstacion write FEstacion;
    property Pagina : integer read FPagina write FPagina;
    procedure Imprime( oDetalle, oTotales : TDataSet );
    {$ifdef CALSONIC}
    procedure ImprimeTicket( sEmpleado: string; dFecha: TDate; sHora, sNombre: string; lFueraLinea: boolean = FALSE );
    {$endif}
  end;


implementation
uses
     DBClient,
     ZetaCommonTools,
     ZetaRegistryCafe,
     ZetaDialogo,
     WinSpool;

const K_TOTAL_LINEAS = 60;

function PadLCar( const sValor: String; iLength: Integer; const cCar: Char ): String;
var
   iLongitud: Integer;
begin
     Result := sValor;
     iLongitud := Length( Result );
     if ( iLongitud < iLength ) then
     begin
          while ( Length( Result ) < iLength ) do
          begin
                Result := cCar + Result;
          end;
     end
     else
         Result := Copy( Result, ( iLongitud - iLength + 1 ), iLength );
end;

function PadL( const sValor: String; iLength: Integer ): String;
begin
     Result := PadLCar( sValor, iLength, ' ' );
end;

function PadRCar( const sValor: String; iLength: Integer; const cCar: Char ): String;
begin
     Result := sValor;
     if ( Length( Result ) < iLength ) then
     begin
          while ( Length( Result ) < iLength ) do
          begin
               Result := Result + cCar;
          end;
     end
     else
         Result := Copy( Result, 1, iLength );
end;

function PadR( const sValor: String; iLength: Integer ): String;
begin
     Result := PadRCar( sValor, iLength, ' ' );
end;

function PadCCar( const sValor: String; iLength: Integer; const cCar: Char ): String;
var
   iLengthValor: Integer;
begin
     iLengthValor := Length( sValor );
     Result := PadRCar( PadLCar( sValor, iLengthValor + Trunc( ( iLength - iLengthValor ) / 2 ), cCar ), iLength, cCar );
end;

function PadC( const sValor: String; iLength: Integer ): String;
begin
     Result := PadCCar( sValor, iLength, ' ' );
end;

// RCM Fuente: http://www.todoexpertos.com/categorias/tecnologia-e-internet/programacion/delphi/respuestas/577931/obtener-el-estado-de-la-impresora
function TReportesCafe.GetStatusImpresora: eStatusImpresora;
type
  JOB_INFO_1_ARRAY = Array of JOB_INFO_1;
var
  Rdo       : Byte;
  TextoError: string;

  PrinterInfo  : PPrinterInfo2;
  PrinterHandle: THandle;
  Stat         : LongBool;
  requiredSize : Cardinal;

begin
  Result := siDesconocido;
  Stat   := OpenPrinter(PChar(FPuerto), PrinterHandle, Nil);
  if (Stat) then
  begin
      try
          try
            GetMem(PrinterInfo, 1024);
            Stat := GetPrinter(PrinterHandle, 2, PrinterInfo, 1024, @requiredSize);
            case PrinterInfo^.Status of
              0:
                Result := siLista;
              PRINTER_STATUS_PAPER_OUT:
                Result := siSinPapel;
              PRINTER_STATUS_OFFLINE:
                Result := siOffLine;
              PRINTER_STATUS_POWER_SAVE:
                Result := siApagada
              else begin
                  Result     := siDesconocido;
                  TextoError := 'La impresora tiene un problema desconocido (' + IntToStr(PrinterInfo^.Status) +
                    '). Solucione el problema y reintente.';
                  ShowMessage(TextoError);
                end;
            end;
          finally
            FreeMem(PrinterInfo, 1024);
          end;
      except

      end;
  end;
end;

{function TReportesCafe.GetStatusImpresora: eStatusImpresora;
var
  Pto       : Word;
  Rdo       : Byte;
  Confirmado: Boolean;
  TextoError: string;
begin
  if (Win32Platform = VER_PLATFORM_WIN32_WINDOWS) and (UpperCase(Copy(FPuerto, 1, 3)) = 'LPT') then begin // ES WINDOWS 95
    Result     := siDesconocido;
    Confirmado := False;
    while (Result <> siLista) and (not Confirmado) do begin
      Pto := StrToInt(FPuerto[Length(FPuerto)]) - 1;
      asm
        MOV  DX,Pto
        MOV  AX,$0200  // AH := $02 : Leer el estado de la impresora
        INT  $17
        MOV  Rdo,AH     // Guarda el estado en AL
      end;
      if Rdo = 144 then
        Result := siLista
      else begin
        Result     := siDesconocido;
        TextoError := 'La impresora tiene un problema desconocido (' + IntToStr(Rdo) + '). Solucione el problema y reintente.';
      end;
      if (Result <> siLista) then begin
        ShowMessage(TextoError);
        Confirmado := True;
      end;
    end;
  end else // WINDOWS NT , � Como se averigua si esta lista ?
    Result := siLista; // Aunque puede no estar lista
end;}

procedure TReportesCafe.ErrorDeImpresion(const eStatus: eStatusImpresora);
var
    sError : String;
begin
    sError := '';
    case eStatus of
        siOffLine  : sError := 'La impresora est� fuera de l�nea';
        siSinPapel : sError := 'La impresora se ha quedado sin papel';
        siApagada  : sError := 'La impresora est� apagada o desconectada';
        else
                     sError := 'Error desconocido en impresora'
    end;
    raise Exception.Create( sError );
end;

procedure TReportesCafe.ExcepcionImpresion(const ErrorCode:integer);
begin
     case ErrorCode of
          100: raise Exception.Create('Error Al Leer Disco');
          101: raise Exception.Create('Error Al Escribir Disco. Disco sin Espacio');
          102: raise Exception.Create('Impresora no Asignada al Puerto');
          103: raise Exception.Create('Puerto No V�lido');
          104: raise Exception.Create('Impresora no est� Lista para Imprimir');
          105: raise Exception.Create('Impresora no est� Lista para Salida');
          106: raise Exception.Create('Invalid numeric format');
     end;
end;

procedure TReportesCafe.AbreImpresora;
var
    eStatus : eStatusImpresora;
begin
     FLineas := 0;
     FPagina := 1;

     eStatus := siLista;
     if not StrLleno (ExtractFileExt(FPuerto)) then
     begin
         eStatus := GetStatusImpresora;
         if ( eStatus <> siLista ) then
            ErrordeImpresion( eStatus );
     end;

     try
        AssignFile( FImpresora, FPuerto );
        ReWrite( FImpresora );
        Write( FImpresora, FCodigoInicial );
     except
           On E:EInOutError do
              ExcepcionImpresion(E.ErrorCode);
           On E:Exception do
              ErrorDeImpresion( siOffLine );
     end;

end;

procedure TReportesCafe.CierraImpresora;
begin
     try
        MandaCodigosFinales;
        CloseFile( FImpresora );
     except
           On E:EInOutError do
              ExcepcionImpresion(E.ErrorCode);
           On E:Exception do
              ErrorDeImpresion( siOffLine );
     end;
end;

procedure TReportesCafe.ImprimeTotales;
 var iRenglones : integer;
begin
     iRenglones := FTotales.RecordCount + 4;

     if (( (FLineas MOD K_TOTAL_LINEAS) + iRenglones ) > K_TOTAL_LINEAS) then
        Eject;


     BajaRenglon;Raya(80);
     BajaRenglon;Raya(29);
     EscribeDato( 'Total de Consumos' );
     Raya(30);BajaRenglon;
     Raya(80);BajaRenglon;

     with FTotales do
          while NOT EOF do
          begin
               EscribeDato( 'Total de Consumos del Tipo: ' +
                            Fields[0].AsString +'    --> '+
                            PadL(Fields[1].AsString,9) + ' Comidas');
               BajaRenglon;
               Next;
          end;
end;
procedure TReportesCafe.Eject;//Salto de PAgina
begin
     FPagina := FPagina +1;
     Write(FImpresora,CHR(12));
end;

procedure TReportesCafe.MandaCodigosFinales;
begin
     Write(FImpresora,CHR(27)+CHR(69));//Reset
end;

procedure TReportesCafe.ImprimeEncabezado;
begin
     EscribeDatoOEM( 'Estaci�n: ' + FEstacion );
     EscribeDatoOEM( PadC('P�gina: ' + IntToStr(FPagina),25) );
     EscribeDato( PadL('Fecha y Hora: ' + FormatDateTime('dd/mmm/yyyy  hh:nn', Now()),35));
     BajaRenglon;
     if HayDetalle then
     begin
          BajaRenglon;
          EscribeDatoOEM( Padr( 'N�mero',10 ));
          EscribeDato( Padr( 'Nombre',30 ));
          EscribeDato( Padr( 'Fecha',11 ) );
          EscribeDato( Padr( 'Hora',5 ));
          EscribeDato( Padr( 'Tipo',4 ));
          EscribeDato( Padr( 'Comidas',7) );
          BajaRenglon;

          Raya( 10 ); Raya( 30 ); Raya( 11 );
          Raya( 5 );  Raya( 4 );  Raya( 7 );
          BajaRenglon;
     end;
end;

procedure TReportesCafe.EscribeDatoOEM(sTexto: string);
begin
     if ( Length( sTexto ) > 0 ) and ( Length( Trim( sTexto ) ) > 0 ) then
        try
           CharToOEM( PChar( sTexto ), PAnsiChar( sTexto ) );
        except
        end;
     EscribeDato(sTexto);
end;

procedure TReportesCafe.EscribeDato( sTexto : string );
begin
     Write( FImpresora, sTexto );
     Write( FImpresora, '  ' );
end;

procedure TReportesCafe.BajaRenglon;
begin
     FLineas := FLineas + 1;
     WriteLn( FImpresora, '' );
end;

procedure TReportesCafe.Raya( const iCuantos : integer );
 var sTexto : string;
     i : integer;
begin
     sTexto := '';
     for i:= 1 to iCuantos do
     begin
          sTexto := sTexto + '-';
     end;
     EscribeDato( sTexto );
end;

procedure TReportesCafe.ImprimeDetalle;
begin
     with FDataSet do
          while NOT EOF do
          begin
               if (FLineas mod K_TOTAL_LINEAS ) = 0 then
               begin
                    Eject;
                    ImprimeEncabezado;
               end;
               EscribeDato( PadL( FieldByName('CB_CODIGO').AsString,10 ) );
               EscribeDatoOEM( Padr( FieldByName('PRETTYNAME').AsString,30 ) );
               EscribeDato( Padr( FormatDateTime('dd/mmm/yyyy',FieldByName('CF_FECHA').AsDateTime),11 ) );
               EscribeDato( Padr( FormatMaskText('00:00;0',FieldByName('CF_HORA').AsString),5 ) );
               EscribeDato( PadL( FieldByName('CF_TIPO').AsString,4 ) );
               EscribeDato( PadL( FieldByName('CF_COMIDAS').AsString,7 ) );
               BajaRenglon;
               Next;
          end;
end;

function TReportesCafe.HayDetalle:Boolean;
begin
     Result := NOT FDataSet.IsEmpty;
end;

procedure TReportesCafe.ImprimeFueraLinea;
var
   FLista: TStrings;
   sArchivoDat,sArchivoBak,sArchivo : string;
   i : integer;
begin
     sArchivo := GetArchivoCafeTemporal(FALSE);
     sArchivoDat := sArchivo+'.DAT';
     sArchivoBak := sArchivo+'.BAK';

     if FileExists(sArchivoDat) then
     begin

          FLista := TStringList.Create;
          try
             FLista.LoadFromFile(sArchivoDat);
             if FLista.Count > 0 then
             begin
                  BajaRenglon;
                  BajaRenglon;
                  Raya(80);BajaRenglon;
                  Raya(14);
                  EscribeDatoOEM( Padr( 'Listado de Consumos fuera de L�nea ( Inv�lidos )',48 ) );
                  Raya(14);BajaRenglon;
                  for i:= 0 to FLista.Count -1 do
                  begin
                       EscribeDatoOEM(FLista[i]);
                       BajaRenglon;
                  end;

                  Raya(80);BajaRenglon;
                  EscribeDatoOEM( 'Total de Consumos Inv�lidos    --> '+
                               Padl(IntToStr(FLista.Count),9) + ' Comidas');
                  BajaRenglon;
                  Raya(80);BajaRenglon;
             end;
          finally
                 if FileExists(sArchivoBAK) then
                    DeleteFile(sArchivoBAK);
                 SysUtils.RenameFile(sArchivoDat,sArchivoBak);
                 FLista.Free;
          end;
     end;
end;
{$ifdef CALSONIC}
procedure TReportesCafe.ImprimeTicket( sEmpleado: string; dFecha: TDate; sHora, sNombre: string; lFueraLinea: boolean = FALSE );
begin
     FSaltoPagina:= CafeRegistry.Eject;
     FCodigoInicial:= CafeRegistry.CodigoInicial;
     FCodigoFinal:=CafeRegistry.CodigoFinal;
     FPuerto := CafeRegistry.Impresora;
     FEstacion := CafeRegistry.Identificacion;
     try
        AbreImpresora;
        BajaRenglon;
        Write( FImpresora, '      CalsonicKansei Mexicana      ' +  CHR(13)+CHR(10) );
        WriteLn(FImpresora,'');
        Write( FImpresora, 'Empleado: ' + sEmpleado + CHR(13)+CHR(10) );
        Write( FImpresora, 'Nombre: ' + Copy( sNombre, 1, 27 ) + CHR(13)+CHR(10) );
        Write( FImpresora, 'Fecha: ' + FechaCorta( dFecha ) + CHR(13)+CHR(10) );
        Write( FImpresora, 'Hora: ' + sHora + CHR(13)+CHR(10) );
        WriteLn(FImpresora,'');
        if lFueraLinea then
           Write( FImpresora, '   ** Consumo FUERA DE LINEA **    ' +  CHR(13)+CHR(10) );
        WriteLn(FImpresora,'');
        WriteLn(FImpresora,'');
        WriteLn(FImpresora,'');
        WriteLn(FImpresora,'');
        WriteLn(FImpresora,'');
        WriteLn(FImpresora,'');
        Write(FImpresora,CHR(27)+CHR(105));
     finally
            CierraImpresora;
     end;
end;
{$endif}
procedure TReportesCafe.Imprime( oDetalle, oTotales : TDataSet );
begin
     FSaltoPagina:= CafeRegistry.Eject;//SaltoPagina
     FCodigoInicial:= CafeRegistry.CodigoInicial;
     FCodigoFinal:=CafeRegistry.CodigoFinal;//Reset
     FPuerto := CafeRegistry.Impresora;
     //FPuerto := 'D:\TEMP\REPORTE.TXT';
     FEstacion := CafeRegistry.Identificacion;
     //FPuerto := '\\Tress\hplaser4';
     if oDetalle = NIL then fDataset := TClientDataset.Create( NIL )
     else fDataSet := oDetalle;

     if oTotales = NIL then FTotales := TClientDataset.Create( Nil )
     else FTotales := oTotales;

     if HayDetalle OR NOT FTotales.IsEmpty then
     begin
          try
             AbreImpresora;
             ImprimeEncabezado;
             ImprimeDetalle;
             ImprimeTotales;
             ImprimeFueraLinea;
          finally
                 CierraImpresora;
          end;
     end
     else ZetaDialogo.ZError( 'Impresi�n de Listado', 'No Hay Datos que Cumplan con los Filtros Especificados',0 );
end;
end.
