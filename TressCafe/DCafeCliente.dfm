inherited dmCafeCliente: TdmCafeCliente
  OldCreateOrder = True
  Height = 586
  Width = 532
  inherited cdsEmpleado: TClientDataSet
    Params = <
      item
        DataType = ftString
        Name = 'Gafete'
        ParamType = ptInputOutput
        Value = #39#39
      end
      item
        DataType = ftDateTime
        Name = 'FechaHora'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'TipoComida'
        ParamType = ptInput
        Value = #39#39
      end
      item
        DataType = ftInteger
        Name = 'NumComidas'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftString
        Name = 'Letrero'
        ParamType = ptInputOutput
        Value = #39#39
      end
      item
        DataType = ftMemo
        Name = 'Comidas'
        ParamType = ptInputOutput
        Value = #39#39
      end
      item
        DataType = ftInteger
        Name = 'Status'
        ParamType = ptOutput
        Value = 0
      end
      item
        DataType = ftInteger
        Name = 'TipoGafete'
        ParamType = ptInput
        Value = 0
      end
      item
        DataType = ftMemo
        Name = 'Foto'
        ParamType = ptInputOutput
        Value = #39#39
      end>
    ReadOnly = True
  end
  object dsEmpleado: TDataSource
    DataSet = cdsEmpleado
    Left = 192
    Top = 24
  end
  object dsComidas: TDataSource
    Left = 256
    Top = 24
  end
  object adCalendario: TClientDataSet
    Aggregates = <>
    Params = <>
    OnNewRecord = adCalendarioNewRecord
    Left = 184
    Top = 288
    object adCalendarioHORA: TStringField
      Alignment = taRightJustify
      FieldName = 'HORA'
      OnGetText = adCalendarioHORAGetText
      OnSetText = adCalendarioHORASetText
      OnValidate = adCalendarioHORAValidate
      EditMask = '!90:00;0'
      Size = 4
    end
    object adCalendarioACCION: TSmallintField
      Alignment = taLeftJustify
      FieldName = 'ACCION'
      OnGetText = adCalendarioACCIONGetText
    end
    object adCalendarioCONTADOR: TStringField
      Alignment = taCenter
      FieldName = 'CONTADOR'
      OnGetText = adCalendarioCONTADORGetText
      OnSetText = adCalendarioCONTADORSetText
      Size = 1
    end
    object adCalendarioSEMANA: TSmallintField
      FieldName = 'SEMANA'
      OnGetText = adCalendarioSEMANAGetText
    end
    object adCalendarioSYNC: TStringField
      Alignment = taCenter
      FieldName = 'SYNC'
      OnGetText = adCalendarioSYNCGetText
      OnSetText = adCalendarioSYNCSetText
      Size = 1
    end
  end
  object dsCalendario: TDataSource
    DataSet = adCalendario
    Left = 256
    Top = 288
  end
  object cdsListado: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDateTime
        Name = 'FechaInicial'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'FechaFinal'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftString
        Name = 'TipoComidas'
        ParamType = ptInput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Reloj'
        ParamType = ptInput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Detalle'
        ParamType = ptInputOutput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Totales'
        ParamType = ptInputOutput
        Value = #39#39
      end>
    Left = 112
    Top = 80
  end
  object dsDetalle: TDataSource
    Left = 312
    Top = 24
  end
  object dsTotales: TDataSource
    Left = 368
    Top = 24
  end
  object cdsIdInvita: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Letrero'
        ParamType = ptOutput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'IdInvita'
        ParamType = ptOutput
        Value = #39#39
      end>
    Left = 112
    Top = 136
  end
  object adIdInvita: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 136
  end
  object dsFotoEmp: TDataSource
    Left = 424
    Top = 24
  end
  object adCalendarioAnterior: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 336
    object StringField1: TStringField
      Alignment = taRightJustify
      FieldName = 'HORA'
      OnGetText = adCalendarioHORAGetText
      OnSetText = adCalendarioHORASetText
      OnValidate = adCalendarioHORAValidate
      EditMask = '!90:00;0'
      Size = 4
    end
    object SmallintField1: TSmallintField
      Alignment = taLeftJustify
      FieldName = 'ACCION'
      OnGetText = adCalendarioACCIONGetText
    end
  end
  object adCalendarioTmp: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 384
    object StringField2: TStringField
      Alignment = taRightJustify
      FieldName = 'HORA'
      OnGetText = adCalendarioHORAGetText
      OnSetText = adCalendarioHORASetText
      OnValidate = adCalendarioHORAValidate
      EditMask = '!90:00;0'
      Size = 4
    end
    object SmallintField2: TSmallintField
      Alignment = taLeftJustify
      FieldName = 'ACCION'
      OnGetText = adCalendarioACCIONGetText
    end
    object StringField3: TStringField
      FieldName = 'CONTADOR'
      Size = 1
    end
    object adCalendarioTmpSYNC: TStringField
      FieldName = 'SYNC'
      Size = 1
    end
  end
  object adContador: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 184
    Top = 240
    object adContadorHORA: TStringField
      FieldName = 'HORA'
      EditMask = '!90:00;0'
      Size = 4
    end
  end
  object dsContador: TDataSource
    DataSet = adContador
    Left = 256
    Top = 240
  end
  object cdsHuellas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Huellas'
        ParamType = ptOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'Letrero'
        ParamType = ptOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'Computer'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'IdCafeteria'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftInteger
        Name = 'TipoEstacion'
        ParamType = ptInputOutput
        Value = 0
      end>
    ReadOnly = True
    Left = 116
    Top = 192
  end
  object dsHuellas: TDataSource
    DataSet = cdsHuellas
    Left = 256
    Top = 192
  end
  object cdsReiniciaHuellas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Computer'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end>
    ReadOnly = True
    Left = 112
    Top = 432
  end
  object cdsReportaHuellas: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftString
        Name = 'Computer'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end
      item
        DataType = ftString
        Name = 'Lista'
        ParamType = ptInputOutput
        Size = 50
        Value = Null
      end>
    ReadOnly = True
    Left = 112
    Top = 488
  end
  object dsReiniciaHuellas: TDataSource
    DataSet = cdsReiniciaHuellas
    Left = 208
    Top = 432
  end
  object dsReportaHuellas: TDataSource
    DataSet = cdsReportaHuellas
    Left = 208
    Top = 488
  end
  object dsConfiguracion: TDataSource
    Left = 344
    Top = 160
  end
  object cdsConfiguracion: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'DI_TIPO'
        ParamType = ptInputOutput
        Value = 0
      end
      item
        DataType = ftString
        Name = 'CF_NOMBRE'
        ParamType = ptOutput
        Size = 50
        Value = 'Null'
      end
      item
        DataType = ftString
        Name = 'Detalle'
        ParamType = ptInputOutput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Totales'
        ParamType = ptInputOutput
        Value = #39#39
      end>
    Left = 96
    Top = 256
  end
  object dsCalend: TDataSource
    Left = 344
    Top = 272
  end
  object cdsActualizarConfig: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'DI_TIPO'
        ParamType = ptInputOutput
        Value = 0
      end
      item
        DataType = ftString
        Name = 'CF_NOMBRE'
        ParamType = ptOutput
        Size = 50
        Value = 'Null'
      end
      item
        DataType = ftBoolean
        Name = 'STATUS'
        ParamType = ptInputOutput
        Value = False
      end>
    Left = 96
    Top = 312
  end
  object cdsImportarConfig: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'DI_TIPO'
        ParamType = ptInputOutput
        Value = 0
      end
      item
        DataType = ftString
        Name = 'CF_NOMBRE'
        ParamType = ptOutput
        Size = 50
        Value = 'Null'
      end
      item
        DataType = ftString
        Name = 'Detalle'
        ParamType = ptInputOutput
        Value = #39#39
      end
      item
        DataType = ftString
        Name = 'Totales'
        ParamType = ptInputOutput
        Value = #39#39
      end>
    Left = 64
    Top = 368
  end
end
