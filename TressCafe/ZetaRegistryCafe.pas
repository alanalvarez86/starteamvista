unit ZetaRegistryCafe;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Registry,
     ZetaCommonLists;

type
  eChecadaSimultanea = ( csPreguntar, csNoPreguntar, csNoAcumular );
  TZetaCafeRegistry = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    FCanWrite: Boolean;
    {$ifdef TRESS_DELPHIXE5_UP}
    FRegistryUser: TRegIniFile;
    FCanWriteUser: Boolean;
    {$endif}
    function GetTipoComida: Integer;
    procedure SetTipoComida( const Value: Integer );
    function GetIdentificacion: String;
    procedure SetIdentificacion( const Value: String );
    function GetMostrarFoto: Boolean;
    procedure SetMostrarFoto( const Value: Boolean );
    function GetMostrarComidas: Boolean;
    procedure SetMostrarComidas( const Value: Boolean );
    function GetMostrarSigComida: Boolean;
    procedure SetMostrarSigComida( const Value: Boolean );
    function GetUsaTeclado: Boolean;
    procedure SetUsaTeclado( const Value: Boolean );
    function GetPreguntaTipo: Boolean;
    procedure SetPreguntaTipo( const Value: Boolean );
    function GetPreguntaQty: Boolean;
    procedure SetPreguntaQty( const Value: Boolean );
    function GetBanner: String;
    procedure SetBanner( const Value: String );
    function GetVelocidad: Integer;
    procedure SetVelocidad( const Value: Integer );
    function GetChecadaManual: Integer;
    procedure SetChecadaManual( const Value: Integer );
    function GetChecadaManualTimer: Integer;
    procedure SetChecadaManualTimer( const Value: Integer );
    function GetServidor: String;
    procedure SetServidor( const Value: String );
    function GetPuerto: Integer;
    procedure SetPuerto( const Value: Integer );
    function GetArchivo: String;
    procedure SetArchivo( const Value: String );
    function GetMensajeOK: String;
    procedure SetMensajeOK( const Value: String );
    function GetImpresora: String;
    procedure SetImpresora( const Value: String );
    function GetCodigoInicial: String;
    procedure SetCodigoInicial( const Value: String );
    function GetCodigoFinal: String;
    procedure SetCodigoFinal( const Value: String );
    function GetEject: String;
    procedure SetEject( const Value: String );
    function GetCalendario: String;
    procedure SetCalendario( const Value: String );
    {$ifdef BOSE}
    function GetArchivoContador: String;
    procedure SetArchivoContador( const Value: string);
    {$endif}
    function GetSonidoAcepta: String;
    function GetSonidoRechazo: String;
    procedure SetSonidoAcepta(const Value: String);
    procedure SetSonidoRechazo(const Value: String);
    function GetGafeteAdmon: String;
    procedure SetGafeteAdmon(const Value: String);
    function GetClaveAdmon: String;
    procedure SetClaveAdmon(const Value: String);
    function GetConcesionCancelar: Boolean;
    procedure SetConcesionCancelar(const Value: Boolean);
    function GetConcesionImprimir: Boolean;
    procedure SetConcesionImprimir(const Value: Boolean);
    function GetSerialA: Integer;
    procedure SetSerialA( const Value: Integer );
    function GetSerialB: Integer;
    procedure SetSerialB( const Value: Integer );
    function GetCodigoAcepta: String;
    procedure SetCodigoAcepta( const Value: String );
    function GetCodigoRechazo: String;
    procedure SetCodigoRechazo( const Value: String );
    function GetApagadoAcepta: String;
    procedure SetApagadoAcepta(const Value: String);
    function GetApagadoRechazo: String;
    procedure SetApagadoRechazo(const Value: String);
    function GetEsperaAcepta: integer;
    procedure SetEsperaAcepta(const Value: integer);
    function GetEsperaRechazo: integer;
    procedure SetEsperaRechazo(const Value: integer);
    function GetComEntrada: String;
    procedure SetComEntrada( const Value: String );
    function GetComSalida: String;
    procedure SetComSalida( const Value: String );
    function GetChecadaSimultanea: eChecadaSimultanea;
    procedure SetChecadaSimultanea(const Value: eChecadaSimultanea);
    function GetTipoGafete: eTipoGafete;
    procedure SetTipoGafete( const Value: eTipoGafete );
    function GetLectorProximidad: eTipoProximidad;
    procedure SetLectorProximidad(const Value: eTipoProximidad);
    function GetDefaultCredencial: String;
    function GetDefaultEmpresa: String;
    procedure SetDefaultCredencial(const Value: String);
    procedure SetDefaultEmpresa(const Value: String);
    {OP: 21.Mayo.08 - Funciones para grabar y obtener los tipos de comidas}
    function GetTipoComida1: String;
    procedure SetTipoComida1( const Value: String );
    function GetTipoComida2: String;
    procedure SetTipoComida2( const Value: String );
    function GetTipoComida3: String;
    procedure SetTipoComida3( const Value: String );
    function GetTipoComida4: String;
    procedure SetTipoComida4( const Value: String );
    function GetTipoComida5: String;
    procedure SetTipoComida5( const Value: String );
    function GetTipoComida6: String;
    procedure SetTipoComida6( const Value: String );
    function GetTipoComida7: String;
    procedure SetTipoComida7( const Value: String );
    function GetTipoComida8: String;
    procedure SetTipoComida8( const Value: String );
    function GetTipoComida9: String;
    procedure SetTipoComida9( const Value: String );
    function GetReiniciaMediaNoche: Boolean;
    procedure SetReiniciaMediaNoche( const Value: Boolean );
    function GetConfiguracionEnviada: Boolean;
    procedure SetConfiguracionEnviada( const Value: Boolean );

{$ifndef DOS_CAPAS}
    //Biometrico
    function GetUsaBiometrico: Boolean;
    procedure SetUsaBiometrico( const Value: Boolean );
    function GetFileTemplates: String;
    procedure SetFileTemplates( const Value: String );
    function GetIntervalo: Integer;
    procedure SetIntervalo(const Value: Integer);
    function GetTiempoEspera: Integer;
    procedure SetTiempoEspera(const Value: Integer);
    // DES SP: 13301 EP III - Recarga de huellas
    function GetRegenerarArchivoHuellasHora: String;
    procedure SetRegenerarArchivoHuellasHora( const Value: String );
{$endif}
    function GetSincronizacion: String;
    procedure SetSincronizacion(const Value: String);
    function GetFrecuenciaMinutos: Integer;
    procedure SetFrecuenciaMinutos(const Value: Integer);
    function GetTipoSincronizacion: Integer;
    procedure SetTipoSincronizacion(Const Value: Integer);

  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property CanWrite: Boolean read FCanWrite;
    property TipoComida: Integer read GetTipoComida write SetTipoComida;
    property Identificacion: String read GetIdentificacion write SetIdentificacion;
    property MostrarFoto: Boolean read GetMostrarFoto write SetMostrarFoto;
    property MostrarComidas: Boolean read GetMostrarComidas write SetMostrarComidas;
    property MostrarSigComida: Boolean read GetMostrarSigComida write SetMostrarSigComida;
    property UsaTeclado: Boolean read GetUsaTeclado write SetUsaTeclado;
    property PreguntaTipo: Boolean read GetPreguntaTipo write SetPreguntaTipo;
    property PreguntaQty: Boolean read GetPreguntaQty write SetPreguntaQty;
    property ChecadaSimultanea: eChecadaSimultanea read GetChecadaSimultanea write SetChecadaSimultanea;
    property Banner: String read GetBanner write SetBanner;
    property Velocidad: Integer read GetVelocidad write SetVelocidad;
    property ChecadaManual: Integer read GetChecadaManual write SetChecadaManual;
    property ChecadaManualTimer: Integer read GetChecadaManualTimer write SetChecadaManualTimer;
    property Servidor: String read GetServidor write SetServidor;
    property Puerto: Integer read GetPuerto write SetPuerto;
    property Intervalo: Integer read GetIntervalo write SetIntervalo;
    property TiempoEspera: Integer read GetTiempoEspera write SetTiempoEspera;
    property Archivo: String read GetArchivo write SetArchivo;
    property MensajeOk: String read GetMensajeOk write SetMensajeOk;
    property Impresora: String read GetImpresora write SetImpresora;
    property CodigoInicial: String read GetCodigoInicial write SetCodigoInicial;
    property CodigoFinal: String read GetCodigoFinal write SetCodigoFinal;
    property Eject: String read GetEject write SetEject;
    property Calendario: String read GetCalendario write SetCalendario;
    property Sincronizacion: String read GetSincronizacion write SetSincronizacion;
    property FrecuenciaMinutos: Integer read GetFrecuenciaMinutos write SetFrecuenciaMinutos;
    property TipoSincronizacion: Integer read GetTipoSincronizacion write SetTipoSincronizacion;
    {$ifdef BOSE}
    property ArchivoContador: string read GetArchivoContador write SetArchivoContador;
    {$endif}
    property SonidoAcepta: String read GetSonidoAcepta write SetSonidoAcepta;
    property SonidoRechazo: String read GetSonidoRechazo write SetSonidoRechazo;
    property GafeteAdmon: String read GetGafeteAdmon write SetGafeteAdmon;
    property ClaveAdmon: String read GetClaveAdmon write SetClaveAdmon;
    property ConcesionImprimir: Boolean read GetConcesionImprimir write SetConcesionImprimir;
    property ConcesionCancelar: Boolean read GetConcesionCancelar write SetConcesionCancelar;
    property SerialA: Integer read GetSerialA write SetSerialA;
    property SerialB: Integer read GetSerialB write SetSerialB;
    property CodigoAcepta: String read GetCodigoAcepta write SetCodigoAcepta;
    property CodigoRechazo: String read GetCodigoRechazo write SetCodigoRechazo;
    property ApagadoAcepta: String read GetApagadoAcepta write SetApagadoAcepta;
    property ApagadoRechazo: String read GetApagadoRechazo write SetApagadoRechazo;
    property EsperaAcepta: integer read GetEsperaAcepta write SetEsperaAcepta;
    property EsperaRechazo: integer read GetEsperaRechazo write SetEsperaRechazo;
    property ComEntrada: String read GetComEntrada write SetComEntrada;
    property ComSalida: String read GetComSalida write SetComSalida;
    property TipoGafete: eTipoGafete read GetTipoGafete write SetTipoGafete;
    property LectorProximidad: eTipoProximidad read GetLectorProximidad write SetLectorProximidad;
    property DefaultEmpresa: String read GetDefaultEmpresa write SetDefaultEmpresa;
    property DefaultCredencial: String read GetDefaultCredencial write SetDefaultCredencial;
    property TipoComida1: String read GetTipoComida1 write SetTipoComida1;{OP: 21.Mayo.08}
    property TipoComida2: String read GetTipoComida2 write SetTipoComida2;{OP: 21.Mayo.08}
    property TipoComida3: String read GetTipoComida3 write SetTipoComida3;{OP: 21.Mayo.08}
    property TipoComida4: String read GetTipoComida4 write SetTipoComida4;{OP: 21.Mayo.08}
    property TipoComida5: String read GetTipoComida5 write SetTipoComida5;{OP: 21.Mayo.08}
    property TipoComida6: String read GetTipoComida6 write SetTipoComida6;{OP: 21.Mayo.08}
    property TipoComida7: String read GetTipoComida7 write SetTipoComida7;{OP: 21.Mayo.08}
    property TipoComida8: String read GetTipoComida8 write SetTipoComida8;{OP: 21.Mayo.08}
    property TipoComida9: String read GetTipoComida9 write SetTipoComida9;{OP: 21.Mayo.08}
    property ReiniciaMediaNoche: Boolean read GetReiniciaMediaNoche write SetReiniciaMediaNoche;{OP: 21.Mayo.08}
    function TextoTipoComida( const iTipoComida: Integer ): String; {OP: 21.Mayo.08}
    // DES SP: 13301 EP III - Recarga de huellas
    property RegenerarArchivoHuellasHora: String read GetRegenerarArchivoHuellasHora write SetRegenerarArchivoHuellasHora;
    property ConfiguracionEnviada: Boolean read GetConfiguracionEnviada write SetConfiguracionEnviada;

{$ifndef DOS_CAPAS}
    //Biometrico
    property UsaBiometrico: Boolean read GetUsaBiometrico write SetUsaBiometrico;
    property FileTemplates: String read GetFileTemplates write SetFileTemplates;
{$endif}

  end;

const
     aSimultaneas: array[ eChecadaSimultanea ] of PChar = ( 'Agregar Preguntando', 'Agregar Sin Preguntar', 'No Agregar' );
var
   CafeRegistry: TZetaCafeRegistry;

procedure InitCafeRegistry;
procedure ClearCafeRegistry;

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZetaMessages,
     ZetaCommonClasses,
     CafeteraConsts;

const
     CAFETERIA_REGISTRY_SECTION = '';
     DEFAULT_CONFIG_SERIAL = '6,4,0,0,0,1,0,10';

procedure InitCafeRegistry;
begin
     CafeRegistry := TZetaCafeRegistry.Create;
end;

procedure ClearCafeRegistry;
begin
     FreeAndNil( CafeRegistry );
end;

{ ************ TZetaRegistry *********** }

constructor TZetaCafeRegistry.Create;
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          FCanWrite := OpenKey( REG_KEY, TRUE );
          if not FCanWrite then
             OpenKeyReadOnly( REG_KEY );
          LazyWrite := False;
     end;
     {$ifdef TRESS_DELPHIXE5_UP}
     // Registry del usuario
     FRegistryUser := TRegIniFile.Create;
     with FRegistryUser do
     begin
          //RootKey := HKEY_LOCAL_MACHINE;    // Al migrar las aplicaciones a Delphi XE5 se cambia a usar el registry del usuario: HKEY_CURRENT_USER que es el default
          LazyWrite := False;
          FCanWriteUser := OpenKey( REG_KEY, TRUE );
          if not FCanWriteUser then
             OpenKeyReadOnly( REG_KEY );
     end;
     {$endif}
end;

destructor TZetaCafeRegistry.Destroy;
begin
     FRegistry.Free;
     {$ifdef TRESS_DELPHIXE5_UP}
     FRegistryUser.Free;
     {$endif}
     inherited Destroy;
end;

function TZetaCafeRegistry.GetTipoComida: Integer;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'TipoComida', 1 );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'TipoComida', 1 );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetTipoComida(const Value: Integer);
begin
     with FRegistryUser do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'TipoComida' , Value );
     end;
end;

function TZetaCafeRegistry.GetIdentificacion: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Identificacion', 'CAF1' );
     end;
end;

procedure TZetaCafeRegistry.SetIdentificacion(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Identificacion' , Value );
     end;
end;

function TZetaCafeRegistry.GetMostrarFoto: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'MostrarFoto', TRUE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'MostrarFoto', TRUE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetMostrarFoto(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'MostrarFoto' , Value );
     end;
end;

function TZetaCafeRegistry.GetMostrarComidas: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'MostrarComidas', TRUE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'MostrarComidas', TRUE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetMostrarComidas(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'MostrarComidas' , Value );
     end;
end;

function TZetaCafeRegistry.GetMostrarSigComida: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'MostrarSigComida', TRUE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'MostrarSigComida', TRUE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetMostrarSigComida(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'MostrarSigComida' , Value );
     end;
end;

function TZetaCafeRegistry.GetUsaTeclado: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'UsaTeclado', FALSE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'UsaTeclado', FALSE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetUsaTeclado(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'UsaTeclado' , Value );
     end;
end;

function TZetaCafeRegistry.GetPreguntaTipo: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'PreguntaTipo', FALSE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'PreguntaTipo', FALSE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetPreguntaTipo(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'PreguntaTipo' , Value );
     end;
end;

function TZetaCafeRegistry.GetPreguntaQty: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'PreguntaQty', FALSE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'PreguntaQty', FALSE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetPreguntaQty(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'PreguntaQty' , Value );
     end;
end;

function TZetaCafeRegistry.GetBanner: String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Banner', 'Grupo Tress Internacional, S.A. de C.V.' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Banner', 'Grupo Tress Internacional, S.A. de C.V.' );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetBanner(const Value: String);
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Banner' , Value );
     end;
end;

function TZetaCafeRegistry.GetVelocidad: Integer;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'Velocidad', 5 );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'Velocidad', 5 );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetVelocidad(const Value: Integer);
begin
     with FRegistryUser do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'Velocidad' , Value );
     end;
end;

function TZetaCafeRegistry.GetChecadaManual: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'ChecadaManual', 0 );
     end;
end;

procedure TZetaCafeRegistry.SetChecadaManual(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'ChecadaManual' , Value );
     end;
end;

function TZetaCafeRegistry.GetChecadaManualTimer: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'ChecadaManualTimer', 5 );
     end;
end;

procedure TZetaCafeRegistry.SetChecadaManualTimer(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'ChecadaManualTimer' , Value );
     end;
end;

function TZetaCafeRegistry.GetServidor: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Servidor', '' );
     end;
end;

procedure TZetaCafeRegistry.SetServidor(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Servidor' , Value );
     end;
end;

function TZetaCafeRegistry.GetPuerto: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'Puerto', 33100 );
     end;
end;

procedure TZetaCafeRegistry.SetPuerto(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'Puerto' , Value );
     end;
end;

function TZetaCafeRegistry.GetArchivo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Archivo', '' );
          if ( Result = '' ) then
             Result := ZetaMessages.SetFileNameDefaultPath( 'LECTURAS.DAT' );
     end;
end;

procedure TZetaCafeRegistry.SetArchivo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Archivo' , Value );
     end;
end;

function TZetaCafeRegistry.GetMensajeOK: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'MensajeOk', 'Buen Provecho !' );
     end;
end;

procedure TZetaCafeRegistry.SetMensajeOK(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'MensajeOk' , Value );
     end;
end;

function TZetaCafeRegistry.GetImpresora: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Impresora', 'LPT1' );
     end;
end;

procedure TZetaCafeRegistry.SetImpresora(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Impresora' , Value );
     end;
end;

function TZetaCafeRegistry.GetIntervalo: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'Intervalo', 30 );
     end;
end;

procedure TZetaCafeRegistry.SetIntervalo(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'Intervalo', Value );
     end;
end;

function TZetaCafeRegistry.GetSincronizacion: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Sincronizacion', '0000' );
     end;
end;

procedure TZetaCafeRegistry.SetSincronizacion(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Sincronizacion', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoSincronizacion: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'TipoSincronizacion', 1 );
     end;
end;

procedure TZetaCafeRegistry.SetTipoSincronizacion(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'TipoSincronizacion', Value );
     end;
end;

function TZetaCafeRegistry.GetFrecuenciaMinutos: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'FrecuenciaMinutos', 10 );
     end;
end;

procedure TZetaCafeRegistry.SetFrecuenciaMinutos(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'FrecuenciaMinutos', Value );
     end;
end;

function TZetaCafeRegistry.GetTiempoEspera: Integer;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'TiempoEspera', 60 );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'TiempoEspera', 60 );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetTiempoEspera(const Value: Integer);
begin
     with FRegistryUser do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'TiempoEspera', Value );
     end;
end;

function TZetaCafeRegistry.GetCodigoInicial: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'CodigoInicial', '' );
     end;
end;

procedure TZetaCafeRegistry.SetCodigoInicial(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'CodigoInicial' , Value );
     end;
end;

function TZetaCafeRegistry.GetCodigoFinal: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'CodigoFinal', '27,69' );
     end;
end;

procedure TZetaCafeRegistry.SetCodigoFinal(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'CodigoFinal' , Value );
     end;
end;

function TZetaCafeRegistry.GetEject: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Eject', '12' );
     end;
end;

procedure TZetaCafeRegistry.SetEject(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Eject' , Value );
     end;
end;

function TZetaCafeRegistry.GetCalendario: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'Calendario', '' );
          if ( Result = '' ) then
             Result := ZetaMessages.SetFileNameDefaultPath( 'CALENDARIO.DAT' );
     end;
end;

procedure TZetaCafeRegistry.SetCalendario(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'Calendario' , Value );
     end;
end;

{$ifdef BOSE}
function TZetaCafeRegistry.GetArchivoContador: string;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'ArchivoContador', '' );

          if ( Result = '' ) then Result := ZetaMessages.SetFileNameDefaultPath( 'CONTADOR.DAT' );
     end;
end;

procedure TZetaCafeRegistry.SetArchivoContador(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'ArchivoContador' , Value );
     end;
end;
{$endif}

function TZetaCafeRegistry.GetSonidoAcepta: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'SonidoAcepta', '' );
     end;
end;

procedure TZetaCafeRegistry.SetSonidoAcepta(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'SonidoAcepta' , Value );
     end;
end;

function TZetaCafeRegistry.GetSonidoRechazo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'SonidoRechazo', '' );
     end;
end;

procedure TZetaCafeRegistry.SetSonidoRechazo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'SonidoRechazo' , Value );
     end;
end;

function TZetaCafeRegistry.GetGafeteAdmon: String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'GafeteAdmon', 'CAFE' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'GafeteAdmon', 'CAFE' );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetGafeteAdmon(const Value: String);
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'GafeteAdmon' , Value );
     end;
end;

function TZetaCafeRegistry.GetClaveAdmon: String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ZetaServerTools.Decrypt( ReadString( CAFETERIA_REGISTRY_SECTION, 'ClaveAdmon', '' ) );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ZetaServerTools.Decrypt( ReadString( CAFETERIA_REGISTRY_SECTION, 'ClaveAdmon', '' ) );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetClaveAdmon(const Value: String);
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'ClaveAdmon' , ZetaServerTools.Encrypt( Value ) );
     end;
end;

function TZetaCafeRegistry.GetConcesionCancelar: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'ConcesionCancelar', TRUE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'ConcesionCancelar', TRUE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetConcesionCancelar(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'ConcesionCancelar' , Value );
     end;
end;

function TZetaCafeRegistry.GetConcesionImprimir: Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'ConcesionImprimir', TRUE );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'ConcesionImprimir', TRUE );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetConcesionImprimir(const Value: Boolean);
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'ConcesionImprimir' , Value );
     end;
end;

function TZetaCafeRegistry.GetSerialA: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'SerialA', 0 );
     end;
end;

procedure TZetaCafeRegistry.SetSerialA(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'SerialA' , Value );
     end;
end;

function TZetaCafeRegistry.GetSerialB: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'SerialB', 0 );
     end;
end;

procedure TZetaCafeRegistry.SetSerialB(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'SerialB' , Value );
     end;
end;

function TZetaCafeRegistry.GetCodigoAcepta: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'CodigoAcepta', '' );
     end;
end;

procedure TZetaCafeRegistry.SetCodigoAcepta(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'CodigoAcepta' , Value );
     end;
end;

function TZetaCafeRegistry.GetCodigoRechazo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'CodigoRechazo', '' );
     end;
end;

procedure TZetaCafeRegistry.SetCodigoRechazo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'CodigoRechazo' , Value );
     end;
end;

function TZetaCafeRegistry.GetComEntrada: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'ComEntrada', DEFAULT_CONFIG_SERIAL );
     end;
end;

procedure TZetaCafeRegistry.SetComEntrada(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'ComEntrada' , Value );
     end;
end;

function TZetaCafeRegistry.GetComSalida: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'ComSalida', DEFAULT_CONFIG_SERIAL );
     end;
end;

procedure TZetaCafeRegistry.SetComSalida(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'ComSalida' , Value );
     end;
end;

function TZetaCafeRegistry.GetChecadaSimultanea: eChecadaSimultanea;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := eChecadaSimultanea( ReadInteger( CAFETERIA_REGISTRY_SECTION, 'ChecadaSimultanea', 0 ) );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := eChecadaSimultanea( ReadInteger( CAFETERIA_REGISTRY_SECTION, 'ChecadaSimultanea', 0 ) );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetChecadaSimultanea(const Value: eChecadaSimultanea);
begin
     with FRegistryUser do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'ChecadaSimultanea', Ord( Value ) );
     end;
end;

function TZetaCafeRegistry.GetTipoGafete: eTipoGafete;
begin
     with FRegistry do
     begin
          Result := eTipoGafete( ReadInteger( CAFETERIA_REGISTRY_SECTION, 'TipoGafete', 0 ) );
     end;
end;

procedure TZetaCafeRegistry.SetTipoGafete(const Value: eTipoGafete);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'TipoGafete', Ord( Value ) );
     end;
end;

function TZetaCafeRegistry.GetLectorProximidad: eTipoProximidad;
begin
     with FRegistry do
     begin
          Result := eTipoProximidad( ReadInteger( CAFETERIA_REGISTRY_SECTION, 'LectorProximidad', 0 ) );
     end;
end;

procedure TZetaCafeRegistry.SetLectorProximidad( const Value: eTipoProximidad);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'LectorProximidad', Ord( Value ) );
     end;
end;

function TZetaCafeRegistry.GetDefaultCredencial: String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := Copy( ReadString( CAFETERIA_REGISTRY_SECTION, 'DefaultCredencial', '' ), 1, 1 );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := Copy( ReadString( CAFETERIA_REGISTRY_SECTION, 'DefaultCredencial', '' ), 1, 1 );
          end;
     end;
end;

function TZetaCafeRegistry.GetDefaultEmpresa: String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               Result := Copy( ReadString( CAFETERIA_REGISTRY_SECTION, 'DefaultEmpresa', '' ), 1, 1 );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               Result := Copy( ReadString( CAFETERIA_REGISTRY_SECTION, 'DefaultEmpresa', '' ), 1, 1 );
          end;
     end;
end;

procedure TZetaCafeRegistry.SetDefaultCredencial(const Value: String);
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'DefaultCredencial', Value );
     end;
end;

procedure TZetaCafeRegistry.SetDefaultEmpresa(const Value: String);
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'DefaultEmpresa', Value );
     end;
end;


///////////////////////

function TZetaCafeRegistry.GetApagadoAcepta: String;
begin
     with FRegistry do
     begin
     Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'ApagadoAcepta', '' );
     end;
end;

procedure TZetaCafeRegistry.SetApagadoAcepta(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'ApagadoAcepta' , Value );
     end;
end;

function TZetaCafeRegistry.GetApagadoRechazo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'ApagadoRechazo', '' );
     end;
end;

procedure TZetaCafeRegistry.SetApagadoRechazo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'ApagadoRechazo' , Value );
     end;
end;

function TZetaCafeRegistry.GetEsperaAcepta: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'EsperaAcepta', 0 );
     end;
end;

procedure TZetaCafeRegistry.SetEsperaAcepta(const Value: integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'EsperaAcepta' , Value );
     end;
end;

function TZetaCafeRegistry.GetEsperaRechazo: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( CAFETERIA_REGISTRY_SECTION, 'EsperaRechazo', 0 );
     end;
end;

procedure TZetaCafeRegistry.SetEsperaRechazo(const Value: integer);
begin
     with FRegistry do
     begin
          WriteInteger( CAFETERIA_REGISTRY_SECTION, 'EsperaRechazo' , Value );
     end;
end;

{OP: 21.Mayo.08}
function TZetaCafeRegistry.GetTipoComida1: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida1', 'Tipo de Comida #1' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida1', 'Tipo de Comida #1' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida1( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida1', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida2: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida2', 'Tipo de Comida #2' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida2', 'Tipo de Comida #2' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida2( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida2', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida3: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida3', 'Tipo de Comida #3' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida3', 'Tipo de Comida #3' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida3( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida3', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida4: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida4', 'Tipo de Comida #4' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida4', 'Tipo de Comida #4' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida4( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida4', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida5: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida5', 'Tipo de Comida #5' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida5', 'Tipo de Comida #5' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida5( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida5', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida6: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida6', 'Tipo de Comida #6' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida6', 'Tipo de Comida #6' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida6( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida6', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida7: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida7', 'Tipo de Comida #7' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida7', 'Tipo de Comida #7' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida7( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida7', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida8: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida8', 'Tipo de Comida #8' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida8', 'Tipo de Comida #8' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida8( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida8', Value );
     end;
end;

function TZetaCafeRegistry.GetTipoComida9: String;
var
   sRes : String;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida9', 'Tipo de Comida #9' );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'TipoComida9', 'Tipo de Comida #9' );
          end;
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetTipoComida9( const Value: String );
begin
     with FRegistryUser do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'TipoComida9', Value );
     end;
end;

function TZetaCafeRegistry.GetReiniciaMediaNoche: Boolean;
var
   lRes : Boolean;
begin
     if ConfiguracionEnviada then
     begin
          with FRegistryUser do
          begin
               lRes := ReadBool( CAFETERIA_REGISTRY_SECTION, 'ReiniciarCon', True );
          end;
     end
     else
     begin
          with FRegistry do
          begin
               lRes := ReadBool( CAFETERIA_REGISTRY_SECTION, 'ReiniciarCon', True );
          end;
     end;
     Result := lRes;
end;

procedure TZetaCafeRegistry.SetReiniciaMediaNoche( const Value: Boolean );
begin
     with FRegistryUser do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'ReiniciarCon', Value );
     end;
end;

{OP: 21.Mayo.08 - Obtiene el texto que pertenezca al tipo de comida indicado}
function TZetaCafeRegistry.TextoTipoComida( const iTipoComida: Integer ): String;
var
   sTipoComida : String;
begin
     sTipoComida := VACIO;
     {Verifica que est� en el rango de 1 a 9, ya que solo se manejar� esa cantidad de tipos de comida}
     if( iTipoComida >= 1 ) and ( iTipoComida <= 9 )  then
     begin
          with FRegistryUser do
              sTipoComida := ReadString( CAFETERIA_REGISTRY_SECTION, Format( 'TipoComida%d', [ iTipoComida ] ),
                                         Format( 'Tipo de Comida #%d', [ iTipoComida ] ) );
     end
     else
     begin
          with FRegistry do
              sTipoComida := ReadString( CAFETERIA_REGISTRY_SECTION, Format( 'TipoComida%d', [ iTipoComida ] ),
                                         Format( 'Tipo de Comida #%d', [ iTipoComida ] ) );
     end;
     {Revisa en el caso que si encuentre el registry pero venga vacio}
     if StrVacio( sTipoComida ) then
        sTipoComida := Format( 'Tipo de Comida #%d', [ iTipoComida ] );

     Result := sTipoComida;
end;

function TZetaCafeRegistry.GetConfiguracionEnviada: Boolean;
var
   lRes : Boolean;
begin
     with FRegistry do
     begin
          lRes := ReadBool( CAFETERIA_REGISTRY_SECTION, 'ConfiguracionEnviada', False );
     end;
     Result := lRes;
end;

procedure TZetaCafeRegistry.SetConfiguracionEnviada( const Value: Boolean );
begin
     with FRegistry do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'ConfiguracionEnviada', Value );
     end;
end;

//------------------------------------------------------------------- Biometrico
{$ifndef DOS_CAPAS}

function TZetaCafeRegistry.GetUsaBiometrico: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( CAFETERIA_REGISTRY_SECTION, 'UsaBiometrico', FALSE );
     end;
end;

procedure TZetaCafeRegistry.SetUsaBiometrico( const Value: Boolean );
begin
     with FRegistry do
     begin
          WriteBool( CAFETERIA_REGISTRY_SECTION, 'UsaBiometrico', Value );
     end;
end;

function TZetaCafeRegistry.GetFileTemplates: String;
var
   sRes : String;
begin
     with FRegistry do
     begin
          sRes := ReadString( CAFETERIA_REGISTRY_SECTION, 'FileTemplates', VACIO );
     end;
     Result := sRes;
end;

procedure TZetaCafeRegistry.SetFileTemplates( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'FileTemplates', Value );
     end;
end;

// DES SP: 13301 EP III - Recarga de huellas
function TZetaCafeRegistry.GetRegenerarArchivoHuellasHora: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CAFETERIA_REGISTRY_SECTION, 'RegenerarArchivoHuellasHora', VACIO );
     end;
end;

// DES SP: 13301 EP III - Recarga de huellas
procedure TZetaCafeRegistry.SetRegenerarArchivoHuellasHora(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CAFETERIA_REGISTRY_SECTION, 'RegenerarArchivoHuellasHora' , Value );
     end;
end;

{$ENDIF}
end.
