inherited OperadorDlg: TOperadorDlg
  Left = 453
  Top = 253
  Caption = 'Diálogo de Operador'
  ClientHeight = 177
  ClientWidth = 227
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 141
    Width = 227
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 59
      Default = True
    end
    inherited Cancelar: TBitBtn
      Left = 144
    end
  end
  object TipoOperacion: TRadioGroup
    Left = 9
    Top = 3
    Width = 209
    Height = 133
    Caption = ' Operación '
    ItemIndex = 0
    Items.Strings = (
      '&Impresión de Listados'
      'Cancelar Comida &Anterior'
      'Configurar &Estación')
    TabOrder = 0
  end
end
