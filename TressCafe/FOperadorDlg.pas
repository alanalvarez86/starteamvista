unit FOperadorDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, ExtCtrls, Buttons;

type
  TOperadorDlg = class(TZetaDlgModal)
    TipoOperacion: TRadioGroup;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OperadorDlg: TOperadorDlg;

implementation

{$R *.DFM}

procedure TOperadorDlg.FormShow(Sender: TObject);
begin
     inherited;
     TipoOperacion.SetFocus;
end;

end.
