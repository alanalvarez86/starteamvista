inherited CafPeriodo_DevEx: TCafPeriodo_DevEx
  Left = 255
  Top = 269
  Caption = 'Comidas Per'#237'odo'
  ClientHeight = 273
  ClientWidth = 775
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 19
    Width = 775
    Height = 130
    Align = alClient
  end
  inherited PanelIdentifica: TPanel
    Width = 775
    inherited Slider: TSplitter
      Left = 289
      Width = 0
    end
    inherited ValorActivo1: TPanel
      Width = 273
      inherited textoValorActivo1: TLabel
        Width = 267
      end
    end
    inherited ValorActivo2: TPanel
      Left = 289
      Width = 486
      inherited textoValorActivo2: TLabel
        Width = 480
      end
    end
  end
  object DBCtrlGrid: TDBCtrlGrid [2]
    Left = 0
    Top = 149
    Width = 775
    Height = 124
    Align = alBottom
    DataSource = dsComida
    PanelHeight = 31
    PanelWidth = 758
    TabOrder = 2
    RowCount = 4
    object TipoLbl: TLabel
      Left = 28
      Top = 12
      Width = 24
      Height = 13
      Caption = 'Tipo:'
    end
    object CF_TIPO: TZetaDBTextBox
      Left = 65
      Top = 10
      Width = 65
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CF_TIPO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CF_TIPO'
      DataSource = dsComida
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ComidasLbl: TLabel
      Left = 175
      Top = 12
      Width = 43
      Height = 13
      Caption = 'Comidas:'
    end
    object CF_COMIDAS: TZetaDBTextBox
      Left = 232
      Top = 10
      Width = 65
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CF_COMIDAS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CF_COMIDAS'
      DataSource = dsComida
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object ExtrasLbl: TLabel
      Left = 341
      Top = 12
      Width = 32
      Height = 13
      Caption = 'Extras:'
    end
    object CF_EXTRAS: TZetaDBTextBox
      Left = 384
      Top = 10
      Width = 65
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CF_EXTRAS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CF_EXTRAS'
      DataSource = dsComida
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 775
    Height = 130
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CF_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'CF_FECHA'
        MinWidth = 60
        Options.Grouping = False
      end
      object CF_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'CF_HORA'
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object CF_TIPOField: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CF_TIPO'
        MinWidth = 50
        Width = 50
      end
      object CF_COMIDASField: TcxGridDBColumn
        Caption = 'Comidas'
        DataBinding.FieldName = 'CF_COMIDAS'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 70
        Options.Grouping = False
        Width = 70
      end
      object CF_EXTRASField: TcxGridDBColumn
        Caption = 'Extras'
        DataBinding.FieldName = 'CF_EXTRAS'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object CF_RELOJ: TcxGridDBColumn
        Caption = 'Reloj'
        DataBinding.FieldName = 'CF_RELOJ'
        MinWidth = 60
        Width = 60
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_CODIGO'
        MinWidth = 70
        Options.Grouping = False
        Width = 70
      end
      object CL_CODIGO: TcxGridDBColumn
        Caption = 'Regla'
        DataBinding.FieldName = 'CL_CODIGO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object CF_REG_EXT: TcxGridDBColumn
        Caption = 'T.Extra'
        DataBinding.FieldName = 'CF_REG_EXT'
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsComida: TDataSource
    Left = 112
    Top = 216
  end
end