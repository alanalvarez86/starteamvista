unit FGridComidas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls,
  StdCtrls, ZetaKeyCombo, Mask, ZetaFecha, ZetaDBGrid, ZetaSmartLists,
  ZBaseGridEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TGridComidas_DevEx = class(TBaseGridEdicion_DevEx)
    Panel1: TPanel;
    RGComida: TRadioGroup;
    ZFecha: TZetaDBFecha;
    zComboReloj: TDBComboBox;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    procedure BuscaEmpleado;
    procedure Cargar;
  protected
    procedure Connect; override;
    procedure Buscar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
  end;

var
  GridComidas_DevEx: TGridComidas_DevEx;

implementation

uses dCafeteria, dCliente, ZetaCommonClasses, ZetaBuscaEmpleado_DevEx, dSistema;

{$R *.DFM}

{ TGridComidas }

procedure TGridComidas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H11612_Registro_de_comidas;
end;

procedure TGridComidas_DevEx.Connect;
begin
     Cargar;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCafeteria do
     begin
          cdsComidas.Refrescar;
          DataSource.DataSet:= cdsComidas;
     end;
end;

procedure TGridComidas_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado;
     end;
end;

procedure TGridComidas_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmCafeteria.cdsComidas do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;
end;

procedure TGridComidas_DevEx.KeyPress(var Key: Char);
begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> chr(9) ) and ( key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'CF_FECHA' ) then
               begin
                    zFecha.SetFocus;
                    SendMessage(zFecha.Handle, WM_Char, word(Key), 0);
                    Key:= #0;
               end
               else if ( ZetaDBGrid.SelectedField.FieldName = 'CF_RELOJ' ) then
               begin
                    zComboReloj.SetFocus;
                    SendMessage(zComboReloj.Handle, WM_Char, word(Key), 0);
                    Key:= #0;
               end
          end;
     end
     else if ( ActiveControl = zFecha ) and ( ( Key = chr( VK_RETURN ) ) or ( Key = chr( 9 ) ) ) then
     begin
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;
               SelectedField := dmCafeteria.cdsComidas.FieldByName( 'CF_HORA' );
          end;
     end
     else if ( ActiveControl = zComboReloj ) and ( ( Key = chr( VK_RETURN ) ) or ( Key = chr( 9 ) ) ) then
     begin
          Key := #0;
          with ZetaDBGrid do
          begin
               SetFocus;
               SelectedField := dmCafeteria.cdsComidas.FieldByName( 'CF_RELOJ' );
          end;
     end
end;

procedure TGridComidas_DevEx.OKClick(Sender: TObject);
begin
     dmCafeteria.SetClaseComidas( ( RGComida.ItemIndex = 0 ) );
     inherited;
end;

procedure TGridComidas_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject;
          const Rect: TRect; DataCol: Integer; Column: TColumn;
          State: TGridDrawState);

   procedure AjustaControlEnCelda( oControl: TControl );
   begin
        with oControl do
        begin
             Left := Rect.Left + ZetaDBGrid.Left;
             Top := Rect.Top + ZetaDBGrid.Top;
             Width := Column.Width + 2;
             Visible := True;
        end;
   end;

begin
     inherited;
     if (gdFocused in State) then
     begin
        if ( Column.FieldName = 'CF_FECHA' ) then
           AjustaControlEnCelda( ZFecha )
        else if ( Column.FieldName = 'CF_RELOJ' ) then
             AjustaControlEnCelda( zComboReloj );
     end;
end;

procedure TGridComidas_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) and ( SelectedField.FieldName = 'CF_FECHA' ) and
             ( zFecha.Visible ) then
             zFecha.Visible:= False
          else if ( SelectedField <> nil ) and ( SelectedField.FieldName = 'CF_RELOJ' ) and ( zComboReloj.Visible ) then
              zComboReloj.Visible := FALSE;
     end;
end;


procedure TGridComidas_DevEx.OK_DevExClick(Sender: TObject);
begin
     dmCafeteria.SetClaseComidas( ( RGComida.ItemIndex = 0 ) );
     inherited;
end;

procedure TGridComidas_DevEx.Cargar;
var
   i: integer;
   lstEstacionesCafe : TStrings;
begin
     lstEstacionesCafe := TStringList.Create;
     try
        dmSistema.CargaEstacionesCafeteria(lstEstacionesCafe );
        with zComboReloj do
        begin
             Items.BeginUpdate;
             try
                Clear;
                with lstEstacionesCafe do
                begin
                     for i := 0 to ( Count - 1 ) do
                         Items.Add( lstEstacionesCafe[i]);
                end;
             finally
                    Items.EndUpdate;
             end;
        end;
     finally
            lstEstacionesCafe.Free;
     end;
end;

end.
