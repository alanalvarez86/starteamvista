unit FCafPeriodo_DevEx
;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, DBCGrids, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TCafPeriodo_DevEx = class(TBaseGridLectura_DevEx)
    DBCtrlGrid: TDBCtrlGrid;
    Splitter1: TSplitter;
    dsComida: TDataSource;
    TipoLbl: TLabel;
    CF_TIPO: TZetaDBTextBox;
    ComidasLbl: TLabel;
    CF_COMIDAS: TZetaDBTextBox;
    ExtrasLbl: TLabel;
    CF_EXTRAS: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

var
  CafPeriodo_DevEx: TCafPeriodo_DevEx;

implementation

uses DCafeteria,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

{ TCafPeriodo }

procedure TCafPeriodo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := ZetaCommonClasses.H10172_Comidas_periodo;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
end;

procedure TCafPeriodo_DevEx.Connect;
begin
     with dmCafeteria do
     begin
          cdsCafPeriodo.Conectar;
          DataSource.DataSet := cdsCafPeriodo;
          dsComida.DataSet := cdsCafPeriodoTotales;
     end;
end;

procedure TCafPeriodo_DevEx.Refresh;
begin
     dmCafeteria.cdsCafPeriodo.Refrescar;
end;

function TCafPeriodo_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar En Comidas Del Per�odo';
end;

function TCafPeriodo_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Comidas Del Per�odo';
end;

function TCafPeriodo_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar En Comidas Del Per�odo';
end;

procedure TCafPeriodo_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := true;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := true;
  //Para que nunca muestre el filterbox inferior
  //Para que no aparezca el Custom Dialog
  ZetaDBGridDBTableView.FilterBox.CustomizeDialog := true;
end;

end.
