unit FEditCafInvita_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, Buttons,
  DBCtrls, StdCtrls, ZetaDBTextBox, Mask, ZetaNumero, 
  ZetaHora, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditCafInvita_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    InvitaLbl: TLabel;
    GroupBox2: TGroupBox;
    HoraLbl: TLabel;
    TipoLbl: TLabel;
    ComidaLbl: TLabel;
    Extraslbl: TLabel;
    CF_TIPO: TDBComboBox;
    CF_COMIDAS: TZetaDBNumero;
    CF_EXTRAS: TZetaDBNumero;
    GroupBox1: TGroupBox;
    RelojLbl: TLabel;
    ModificoLbl: TLabel;
    ReglaLbl: TLabel;
    CF_RELOJ: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    CL_CODIGO: TZetaDBTextBox;
    CF_REG_EXT: TDBCheckBox;
    CF_HORA: TZetaDBHora;
    ZInvitador: TZetaTextBox;
    ExtraLbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;           
  public
    { Public declarations }
  end;

var
  EditCafInvita_DevEx: TEditCafInvita_DevEx;

implementation

uses ZetaCommonClasses, ZAccesosTress, DCafeteria, dCatalogos;

{$R *.DFM}

procedure TEditCafInvita_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext   := H10173_Invitaciones;
     IndexDerechos := ZAccesosTress.D_EMP_CAFE_INVITACIONES;
     FirstControl  := CF_HORA;
end;

procedure TEditCafInvita_DevEx.Connect;
begin
     with dmCafeteria do
     begin
          cdsCafInvita.Conectar;
          DataSource.DataSet := cdsCafInvita;
          with cdsCafInvita do
               ZInvitador.Caption := FieldByName('IV_CODIGO').AsString + ' = '  +
                                     FieldByName('IV_DESCRIP').AsString;
     end;
end;

procedure TEditCafInvita_DevEx.FormShow(Sender: TObject);
begin
  inherited;
     ZInvitador.Caption := Datasource.DataSet.FieldByName( 'IV_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsInvitadores.FieldByName( 'IV_NOMBRE' ).AsString;
end;

end.
