unit FCafDiarias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids,ZBaseGridLectura_DevEx,
  DBGrids, ZetaDBGrid, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,dxSkinscxPCPainter, 
  cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, Mask,
  ZetaFecha,ftressShell, TressMorado2013, System.Actions;

type
  TCafDiarias_DevEx = class(TBaseGridLectura_DevEx)
    CF_HORA: TcxGridDBColumn;
    CF_COMIDAS: TcxGridDBColumn;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    AsistenciaFechaZF: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AsistenciaFechaZFValidDate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  protected
    //function PuedeAgregar( var sMensaje: String ): Boolean; override;
    //function PuedeModificar( var sMensaje: String ): Boolean; override;
    //function PuedeBorrar( var sMensaje: String ): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Connect; override;
    procedure Refresh; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CafDiarias_DevEx: TCafDiarias_DevEx;

implementation

uses dRecursos, dCafeteria, ZetaCommonLists, ZetaCommonClasses, DCliente;


{$R *.DFM}
{ TCafDiarias }

procedure TCafDiarias_DevEx.Connect;
begin
     inherited;
     with dmCafeteria do
     begin
          cdsCafDiarias.Conectar;
          DataSource.DataSet:= cdsCafDiarias;
     end;
    // CreaColumaSumatoria('CF_EXTRAS',skCount);
   //  CreaColumaSumatoria('CF_COMIDAS',skSum);

end;

procedure TCafDiarias_DevEx.Refresh;
begin
       AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
     dmCafeteria.cdsCafDiarias.Refrescar;
end;

procedure TCafDiarias_DevEx.FormCreate(Sender: TObject);
begin
     AsistenciaFechaZF.Valor := dmCliente.FechaAsistencia;
     inherited;
     HelpContext   := ZetaCommonClasses.H10171_Comidas_diarias;
     TipoValorActivo1 := stEmpleado;
end;

{
function TCafDiarias.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.CheckBajaEmpleado( 'No se Puede Agregar Comidas', sMensaje )
end;
}

procedure TCafDiarias_DevEx.Agregar;
begin
     inherited;
     if dmRecursos.CheckEmpleadoActivoBaja then
        dmCafeteria.cdsCafDiarias.Agregar;
end;

{
function TCafDiarias.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.CheckBajaEmpleado( 'No se Puede Borrar Comidas', sMensaje )
end;
}

procedure TCafDiarias_DevEx.Borrar;
begin
     inherited;
     if dmRecursos.CheckEmpleadoActivoBaja then
        dmCafeteria.cdsCafDiarias.Borrar;
end;

{
function TCafDiarias.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := dmRecursos.CheckBajaEmpleado( 'No se Puede Modificar Comidas', sMensaje )
end;
}

procedure TCafDiarias_DevEx.Modificar;
begin
     inherited;
     if dmRecursos.CheckEmpleadoActivoBaja then
        dmCafeteria.cdsCafDiarias.Modificar;
end;

procedure TCafDiarias_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
   ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  //Para que nunca muestre el filterbox inferior
end;

procedure TCafDiarias_DevEx.AsistenciaFechaZFValidDate(Sender: TObject);
begin
  inherited;
   TressShell.AsistenciaFechavalid(AsistenciaFechaZF.Valor);
end;

procedure TCafDiarias_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
 // inherited;
// AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

end.
