inherited CafDiarias_DevEx: TCafDiarias_DevEx
  Left = 818
  Top = 320
  Caption = 'Comidas Diarias'
  ClientHeight = 228
  ClientWidth = 580
  ExplicitWidth = 580
  ExplicitHeight = 228
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 580
    ExplicitWidth = 576
    inherited Slider: TSplitter
      Left = 266
      ExplicitLeft = 266
    end
    inherited ValorActivo1: TPanel
      Width = 250
      ExplicitWidth = 250
      inherited textoValorActivo1: TLabel
        Width = 244
      end
    end
    inherited ValorActivo2: TPanel
      Left = 269
      Width = 311
      ExplicitLeft = 269
      ExplicitWidth = 307
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 305
        ExplicitLeft = 497
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 580
    Height = 168
    ExplicitTop = 60
    ExplicitWidth = 576
    ExplicitHeight = 164
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CF_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'CF_HORA'
        MinWidth = 65
        Options.Grouping = False
      end
      object CF_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CF_TIPO'
        MinWidth = 40
        Width = 40
      end
      object CF_COMIDAS: TcxGridDBColumn
        Caption = 'Comidas'
        DataBinding.FieldName = 'CF_COMIDAS'
        MinWidth = 85
        Options.Grouping = False
      end
      object CF_EXTRAS: TcxGridDBColumn
        Caption = 'Extras'
        DataBinding.FieldName = 'CF_EXTRAS'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 50
        Options.Grouping = False
        Width = 50
      end
      object CF_RELOJ: TcxGridDBColumn
        Caption = 'Estaci'#243'n'
        DataBinding.FieldName = 'CF_RELOJ'
        MinWidth = 40
        Options.Grouping = False
        Width = 40
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_CODIGO'
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object CL_CODIGO: TcxGridDBColumn
        Caption = 'Regla'
        DataBinding.FieldName = 'CL_CODIGO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 50
        Options.Grouping = False
        Width = 50
      end
      object CF_REG_EXT: TcxGridDBColumn
        Caption = 'T.Extra'
        DataBinding.FieldName = 'CF_REG_EXT'
        MinWidth = 55
        Options.Grouping = False
        Width = 55
      end
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 580
    Height = 41
    Align = alTop
    TabOrder = 2
    ExplicitWidth = 576
    DesignSize = (
      580
      41)
    object Panel2: TPanel
      Left = 450
      Top = 8
      Width = 125
      Height = 27
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      ExplicitLeft = 446
    end
    object Panel3: TPanel
      Left = 273
      Top = 4
      Width = 307
      Height = 33
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 269
      DesignSize = (
        307
        33)
      object Label1: TLabel
        Left = 102
        Top = 12
        Width = 69
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Fecha Activa :'
      end
      object AsistenciaFechaZF: TZetaFecha
        Left = 182
        Top = 6
        Width = 115
        Height = 22
        Cursor = crArrow
        Hint = 'Fecha Default para Tarjetas de Asistencia'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '10/Dec/97'
        UseEnterKey = True
        Valor = 35774.000000000000000000
        OnValidDate = AsistenciaFechaZFValidDate
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 226
    Top = 12
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 917824
  end
  inherited ActionList: TActionList
    Left = 292
    Top = 12
  end
  inherited PopupMenu1: TPopupMenu
    Left = 260
    Top = 10
  end
end
