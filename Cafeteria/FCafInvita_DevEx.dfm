inherited CafInvita_DevEx: TCafInvita_DevEx
  Left = 652
  Top = 152
  Caption = 'Invitaciones'
  ClientWidth = 674
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 674
    inherited Slider: TSplitter
      Left = 268
    end
    inherited ValorActivo1: TPanel
      Width = 252
      inherited textoValorActivo1: TLabel
        Width = 246
      end
    end
    inherited ValorActivo2: TPanel
      Left = 271
      Width = 403
      inherited textoValorActivo2: TLabel
        Width = 397
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 674
    Height = 41
    Align = alTop
    TabOrder = 2
    DesignSize = (
      674
      41)
    object Label1: TLabel
      Left = 12
      Top = 14
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Invitador:'
    end
    object LookUpInvita: TZetaKeyLookup_DevEx
      Left = 64
      Top = 10
      Width = 320
      Height = 21
      LookupDataset = dmCatalogos.cdsInvitadores
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = LookUpInvitaValidKey
    end
    object Panel2: TPanel
      Left = 442
      Top = 0
      Width = 231
      Height = 39
      Anchors = [akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        231
        39)
      object Label2: TLabel
        Left = 32
        Top = 14
        Width = 69
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Fecha Activa :'
      end
      object AsistenciaFechaZF: TZetaFecha
        Left = 108
        Top = 10
        Width = 115
        Height = 22
        Cursor = crArrow
        Hint = 'Fecha Default para Tarjetas de Asistencia'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '10/dic/97'
        UseEnterKey = True
        Valor = 35774.000000000000000000
        OnValidDate = AsistenciaFechaZFValidDate
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 674
    Height = 164
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CF_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'CF_HORA'
        MinWidth = 65
        Options.Grouping = False
      end
      object CF_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CF_TIPO'
        MinWidth = 40
        Width = 40
      end
      object CF_COMIDAS: TcxGridDBColumn
        Caption = 'Comidas'
        DataBinding.FieldName = 'CF_COMIDAS'
        MinWidth = 85
        Options.Grouping = False
      end
      object CF_EXTRAS: TcxGridDBColumn
        Caption = 'Extras'
        DataBinding.FieldName = 'CF_EXTRAS'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 50
        Options.Grouping = False
        Width = 50
      end
      object CF_RELOJ: TcxGridDBColumn
        Caption = 'Reloj'
        DataBinding.FieldName = 'CF_RELOJ'
        MinWidth = 40
        Width = 40
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_CODIGO'
        MinWidth = 60
        Options.Grouping = False
        Width = 60
      end
      object CL_CODIGO: TcxGridDBColumn
        Caption = 'Regla'
        DataBinding.FieldName = 'CL_CODIGO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 50
        Options.Grouping = False
        Width = 50
      end
      object CF_REG_EXT: TcxGridDBColumn
        Caption = 'T.Extra'
        DataBinding.FieldName = 'CF_REG_EXT'
        MinWidth = 55
        Options.Grouping = False
        Width = 55
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 56
    Top = 128
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end