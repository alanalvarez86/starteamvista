unit FEditCafDiarias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, Buttons,
  DBCtrls, StdCtrls, Mask, ZetaNumero, ZetaDBTextBox, ZetaHora,ZBaseEdicion_DevEx,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  TressMorado2013, dxSkinsDefaultPainters;

type
  TEditCafDiarias_DevEx = class(TBaseEdicion_DevEx)
    GroupBox1: TGroupBox;
    RelojLbl: TLabel;
    ModificoLbl: TLabel;
    ReglaLbl: TLabel;
    GroupBox2: TGroupBox;
    HoraLbl: TLabel;
    TipoLbl: TLabel;
    CF_TIPO: TDBComboBox;
    US_CODIGO: TZetaDBTextBox;
    CL_CODIGO: TZetaDBTextBox;
    CF_HORA: TZetaDBHora;
    CF_REG_EXT: TDBCheckBox;
    RGComidas: TRadioGroup;
    CF_COMIDAS: TZetaDBNumero;
    CF_EXTRAS: TZetaDBNumero;
    CF_RELOJ: TDBComboBox;
    procedure FormCreate(Sender: TObject);
    procedure RGComidasClick(Sender: TObject);
  private
    FConectar: Boolean;
    procedure EnabledControles;
    procedure Cargar;
  protected
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditCafDiarias_DevEx: TEditCafDiarias_DevEx;

implementation

uses ZetaCommonClasses, ZetaCommonLists, ZAccesosTress, DCafeteria, DSistema;

{$R *.DFM}


procedure TEditCafDiarias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext   := H10171_Comidas_diarias;
     FirstControl := CF_HORA;
     IndexDerechos := ZAccesosTress.D_EMP_CAFE_COMIDAS_DIA;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
end;

procedure TEditCafDiarias_DevEx.EnabledControles;
begin
     if RGComidas.ItemIndex = 0 then
     begin
          CF_COMIDAS.Enabled :=TRUE;
          CF_EXTRAS.Enabled := FALSE;
          CF_REG_EXT.Enabled := TRUE;
     end
     else
     begin
          CF_COMIDAS.Enabled :=FALSE;
          CF_EXTRAS.Enabled := TRUE;
          CF_REG_EXT.Enabled := FALSE;
          CF_REG_EXT.Checked := FALSE;
     end;
end;

procedure TEditCafDiarias_DevEx.RGComidasClick(Sender: TObject);
begin
     inherited;
     if not FConectar then
     begin
           with dmCafeteria.cdsCafDiarias do
           begin
               if State = dsBrowse then
                  Edit;
           end;

           if RGComidas.ItemIndex = 0 then
              CF_EXTRAS.Valor  := 0
           else
               CF_COMIDAS.Valor := 0;
           EnabledControles;
     end;
end;

procedure TEditCafDiarias_DevEx.Connect;
begin
     inherited;
     Cargar;
     with dmCafeteria do
     begin
          cdsCafDiarias.Conectar;
          DataSource.DataSet := cdsCafDiarias;
     end;
end;

procedure TEditCafDiarias_DevEx.Cargar;
var
   i: integer;
   lstEstacionesCafe : TStrings;
begin
     lstEstacionesCafe := TStringList.Create;
     try
        dmSistema.CargaEstacionesCafeteria( lstEstacionesCafe );
        with CF_RELOJ do
        begin
             Items.BeginUpdate;
             try
                Clear;
                with lstEstacionesCafe do
                begin
                     for i := 0 to ( Count - 1 ) do
                         Items.Add( lstEstacionesCafe[i]);
                end;
            finally
                   Items.EndUpdate;
            end;
       end;
     finally
            lstEstacionesCafe.Free;
     end;
end;

end.
