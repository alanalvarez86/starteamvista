unit DCatalogo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet;

type
  TdmCatalogo = class(TDataModule)
    cdsNomParamLookUp: TZetaLookupDataSet;
    cdsConceptosLookUp: TZetaLookupDataSet;
    cdsConceptos: TZetaLookupDataSet;
    procedure cdsConceptosAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;

  public
    { Public declarations }
  end;

var
  dmCatalogo: TdmCatalogo;

implementation

{$R *.DFM}

procedure TdmCatalogo.cdsConceptosAlAdquirirDatos(Sender: TObject);
begin
     cdsConceptos.Data := ServerCatalogo.GetConceptos( dmCliente.Empresa );
end;

procedure TdmCatalogo.cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsNomParam do
     begin
          if ( State <> dsInsert ) then // se condiciona porque la transferencia
          begin                         // de Data regresa el cds a dsBrowse
               Conectar;
               cdsNomParamLookUp.Data := Data;
          end;
     end;
end;

function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;

end.
