object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 233
  Top = 104
  Height = 375
  Width = 544
  object cdsNomParamLookUp: TZetaLookupDataSet
    Tag = 21
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    AlAdquirirDatos = cdsNomParamAlAdquirirDatos
    LookupName = 'Par�metros de N�mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    Left = 48
    Top = 16
  end
  object cdsConceptosLookUp: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsConceptosAlAdquirirDatos
    LookupName = 'Cat�logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    Left = 48
    Top = 72
  end
  object cdsConceptos: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsConceptosAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat�logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    Left = 48
    Top = 128
  end
  object cdsNomParam: TZetaLookupDataSet
    Tag = 21
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    AlAdquirirDatos = cdsNomParamAlAdquirirDatos
    UsaCache = True
    LookupName = 'Par�metros de N�mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    Left = 272
    Top = 128
  end
end
