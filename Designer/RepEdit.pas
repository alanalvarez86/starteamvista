{$I QRDESIGN.INC}
unit RepEdit;
                               
interface

uses
  WinProcs, WinTypes, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DB, DBTables, Qrdesign, Menus, Buttons, IniFiles,
  DBCtrls, Grids, DBGrids, QRDOpt, QRDProcs,
  LabProp, DBTxProp, ShapProp, MemoProp, SysDProp, DBImProp, ImgProp, BandProp,
  ChldProp, SubProp, GrpProp, ExprProp, FramePro, QRDConst,
  {$IFDEF WIN32}
  {$IFNDEF WPTools}
  RichProp, DbRtfPro,
  {$ENDIF}
  {$ENDIF}
  {$IFDEF WPTools}
  RichPrWP, DbRtfPWP,
  {$ENDIF}
  qrdtools, qrdexprt, Printers,
  csProp, Quickrpt, Qrctrls, QRPrntr,
  {$IFDEF Teechart}
  TeeProcs, TeEngine, Chart, DBChart, QrTee, TeeProp,
  {$ENDIF}
  {$IFDEF IncludeCustomComponents}
  QRDAddon,
  {$ENDIF}
  {$IFDEF QR2PP}
  GridProp, CBoxProp,
  {$ENDIF}
  Mask, QRDCtrls, QRDLDR, SupComps;

type
  TReportEditor = class(TForm)
    OpenDialog1: TOpenDialog;                        
    SaveDialog1: TSaveDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    StatusPanel: TPanel;
    PropertyPanel: TPanel;
    BoldSpeedButton: TSpeedButton;
    UnderlineSpeedButton: TSpeedButton;
    ItalicSpeedButton: TSpeedButton;
    FontComboBox: TComboBox;
    FontSizeComboBox: TComboBox;
    MainMenu: TMainMenu;
    NewMI: TMenuItem;
    LoadMI: TMenuItem;
    SaveMI: TMenuItem;
    SaveAsMI: TMenuItem;
    MenuItem5: TMenuItem;
    PrintMI: TMenuItem;
    PrinterSetupMI: TMenuItem;
    MenuItem8: TMenuItem;
    ExitMI: TMenuItem;
    NewElementMI: TMenuItem;
    NewBandMI: TMenuItem;
    NewLabelMI: TMenuItem;
    NewDatafieldMI: TMenuItem;
    NewCalculatedfieldMI: TMenuItem;
    NewSystemdataMI: TMenuItem;
    NewShapeMI: TMenuItem;
    NewImageMI: TMenuItem;
    NewImagefromTableMI: TMenuItem;
    NewMemoMI: TMenuItem;
    N3: TMenuItem;
    CutMI: TMenuItem;
    CopyMI: TMenuItem;
    PasteMI: TMenuItem;
    N4: TMenuItem;
    AlignMI: TMenuItem;
    AlignLeftMI: TMenuItem;
    AlignRightMI: TMenuItem;
    AlignTopMI: TMenuItem;
    AlignBottomMI: TMenuItem;
    CenterMI: TMenuItem;
    AlignCenterHorizontalMI: TMenuItem;
    AlignCenterVerticalMI: TMenuItem;
    ParentCenterMI: TMenuItem;
    AlignCenterBandHorizontalMI: TMenuItem;
    AlignCenterBandVerticalMI: TMenuItem;
    EqualspaceMI: TMenuItem;
    AlignEqualHorizontalMI: TMenuItem;
    AlignEqualVerticalMI: TMenuItem;
    N2: TMenuItem;
    SendtobackMI: TMenuItem;
    Bringtofront1: TMenuItem;
    Options1: TMenuItem;
    MenuItem12: TMenuItem;
    Databases1: TMenuItem;
    MenuItem19: TMenuItem;
    DeleteMI: TMenuItem;
    PropertyPanelPopupMenu: TPopupMenu;
    PropMenuTop: TMenuItem;
    PropMenuBottom: TMenuItem;
    AllComboBox: TComboBox;
    AllEdit: TEdit;
    ElementPanel: TPanel;
    NormalSpeedButton: TSpeedButton;
    ToolbarPanel: TPanel;
    PrintSpeedButton: TSpeedButton;
    PreviewSpeedButton: TSpeedButton;
    SendToBackButton: TSpeedButton;
    SaveSpeedButton: TSpeedButton;
    LoadSpeedButton: TSpeedButton;
    BandSpeedButton: TSpeedButton;
    LabelSpeedButton: TSpeedButton;
    DBTextSpeedButton: TSpeedButton;
    MemoSpeedButton: TSpeedButton;
    DBImageSpeedButton: TSpeedButton;
    ImageSpeedButton: TSpeedButton;
    ShapeSpeedButton: TSpeedButton;
    SysdataSpeedButton: TSpeedButton;
    ExpressionSpeedButton: TSpeedButton;
    NewSpeedButton: TSpeedButton;
    CutSpeedButton: TSpeedButton;
    CopySpeedButton: TSpeedButton;
    PasteSpeedButton: TSpeedButton;
    BringToFrontButton: TSpeedButton;
    TextLeftAlignButton: TSpeedButton;
    TextCenterAlignButton: TSpeedButton;
    TextRightAlignButton: TSpeedButton;
    Visible1: TMenuItem;
    N5: TMenuItem;
    LeftButton: TSpeedButton;
    CenterHorizontalButton: TSpeedButton;
    RightButton: TSpeedButton;
    HorizontalEqualButton: TSpeedButton;
    ParentCenterHorizontalButton: TSpeedButton;
    TopButton: TSpeedButton;
    CenterVerticalButton: TSpeedButton;
    BottomButton: TSpeedButton;
    VerticalEqualButton: TSpeedButton;
    ParentCenterVerticalButton: TSpeedButton;
    Bevel1: TBevel;
    SpeedButton7: TSpeedButton;
    TypeLabel: TLabel;
    Bevel2: TBevel;
    Toolbar1: TMenuItem;
    Fonttoolbar1: TMenuItem;
    Elementtoolbar1: TMenuItem;
    Statusline1: TMenuItem;
    N6: TMenuItem;
    Options2: TMenuItem;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    XPosLabel: TLabel;
    YPosLabel: TLabel;
    Label1: TLabel;
    N7: TMenuItem;
    Preview1: TMenuItem;
    ElementPanelPopupMenu: TPopupMenu;
    TopElementPanelMI: TMenuItem;
    LeftElementPanelMI: TMenuItem;
    N1: TMenuItem;
    VisibleElementPanelMI: TMenuItem;
    SubdetailSpeedButton: TSpeedButton;
    ChildSpeedbutton: TSpeedButton;
    GroupSpeedButton: TSpeedButton;
    RichtextSpeedButton: TSpeedButton;
    DBRichtextSpeedButton: TSpeedButton;
    QRepDesigner1: TQRepDesigner;
    N8: TMenuItem;
    Zoomfaktor1: TMenuItem;
    Normalgre1: TMenuItem;
    Vergrern1: TMenuItem;
    Verkleinern1: TMenuItem;
    Childband1: TMenuItem;
    Subdetailband1: TMenuItem;
    Groupband1: TMenuItem;
    FrameSpeedButton: TSpeedButton;
    QRDPanel: TPanel;
    Scrollbox1: TEventScrollbox;
    QuickReport: TDesignQuickReport;
    ScrollBox2: TScrollBox;
    RulerPanel1: TRulerPanel;
    TeechartSpeedButton: TSpeedButton;
    N9: TMenuItem;
    Help1: TMenuItem;
    ElementsComboBox: TComboBox;
    UndoMI: TMenuItem;
    N10: TMenuItem;
    Rotatebands1: TMenuItem;
    Hidebands1: TMenuItem;
    N11: TMenuItem;
    Resetbands1: TMenuItem;
    DBBarcodeSpeedButton: TSpeedButton;
    BarcodeSpeedButton: TSpeedButton;
    FileHistory0: TMenuItem;
    FileHistory1: TMenuItem;
    FileHistory2: TMenuItem;
    FileHistory3: TMenuItem;
    FileHistory4: TMenuItem;
    FileHistory5: TMenuItem;
    CheckboxSpeedButton: TSpeedButton;
    GridSpeedButton: TSpeedButton;
    FontColorSpeedbutton: TSpeedButton;
    DatafieldsListbox: TMenuItem;
    procedure BandSpeedButtonClick(Sender: TObject);
    procedure LabelSpeedButtonClick(Sender: TObject);
    procedure DBTextSpeedButtonClick(Sender: TObject);
    procedure ExpressionSpeedButtonClick(Sender: TObject);
    procedure SysDataSpeedButtonClick(Sender: TObject);
    procedure ShapeSpeedButtonClick(Sender: TObject);
    procedure PreviewSpeedButtonClick(Sender: TObject);
    procedure PrintSpeedButtonClick(Sender: TObject);
    procedure OptionsSpeedButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ImageSpeedButtonClick(Sender: TObject);
    procedure DBImageSpeedButtonClick(Sender: TObject);
    procedure MemoSpeedButtonClick(Sender: TObject);
    procedure LoadSpeedButtonClick(Sender: TObject);
    procedure SaveSpeedButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure NewMIClick(Sender: TObject);
    procedure SaveAsMIClick(Sender: TObject);
    procedure PrinterSetupMIClick(Sender: TObject);
    procedure ExitMIClick(Sender: TObject);
    procedure Databases1Click(Sender: TObject);
    procedure QRepDesigner1EditProperties(QRD: TQRepDesigner;
      Component: TComponent; var EditResult: Boolean);
    procedure QRepDesigner1SelectionChanged(QRD: TQRepDesigner;
      NumberOfSelections: Integer);
    procedure Scrollbox1ScrollHorz(Sender: TObject);
    procedure PropMenuTopClick(Sender: TObject);
    procedure PropMenuBottomClick(Sender: TObject);
    procedure AllEditChange(Sender: TObject);
    procedure AllComboBoxChange(Sender: TObject);
    procedure Visible1Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure Toolbar1Click(Sender: TObject);
    procedure Fonttoolbar1Click(Sender: TObject);
    procedure Elementtoolbar1Click(Sender: TObject);
    procedure Options2Click(Sender: TObject);
    procedure QRepDesigner1ShowInfo(Component: TComponent;
      var Info: QRDShowInfoString);
    procedure MenuItem19Click(Sender: TObject);
    procedure Statusline1Click(Sender: TObject);
    procedure TopElementPanelMIClick(Sender: TObject);
    procedure LeftElementPanelMIClick(Sender: TObject);
    procedure LabelSpeedButtonMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SubdetailSpeedButtonClick(Sender: TObject);
    procedure ChildSpeedbuttonClick(Sender: TObject);
    procedure GroupSpeedButtonClick(Sender: TObject);
    procedure RichtextSpeedButtonClick(Sender: TObject);
    procedure DBRichtextSpeedButtonClick(Sender: TObject);
    procedure Normalgre1Click(Sender: TObject);
    procedure Vergrern1Click(Sender: TObject);
    procedure Verkleinern1Click(Sender: TObject);
    procedure FrameSpeedButtonClick(Sender: TObject);
    procedure TeechartSpeedButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Help1Click(Sender: TObject);
    procedure Panel2DblClick(Sender: TObject);
    procedure ElementsComboBoxDropDown(Sender: TObject);
    procedure ElementsComboBoxChange(Sender: TObject);
    procedure UndoMIClick(Sender: TObject);
    procedure Rotatebands1Click(Sender: TObject);
    procedure Resetbands1Click(Sender: TObject);
    procedure Hidebands1Click(Sender: TObject);
    procedure DBBarcodeSpeedButtonClick(Sender: TObject);
    procedure BarcodeSpeedButtonClick(Sender: TObject);
    procedure FileMenuClick(Sender: TObject);
    procedure FileHistory1Click(Sender: TObject);
    procedure CheckboxSpeedButtonClick(Sender: TObject);
    procedure GridSpeedButtonClick(Sender: TObject);
    procedure FontComboBoxChange(Sender: TObject);
    procedure FontColorSpeedbuttonClick(Sender: TObject);
    procedure DatafieldsListboxClick(Sender: TObject);
    procedure ViewMenuClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    ActiveBlobField: TBlobField;
    ActiveStream: TStream;
    ReportFileName: String;
    QRDHelpFile, AppHelpFile: String[255];
    OpenFileHistory: TStringList;
    procedure RefreshFontCombobox;
  end;

var
  ReportEditor: TReportEditor;

implementation

{$R *.DFM}


procedure TReportEditor.BandSpeedButtonClick(Sender: TObject);
begin
  BandSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddBand;
end;
                 
procedure TReportEditor.LabelSpeedButtonClick(Sender: TObject);
begin
  LabelSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddLabel;
end;

procedure TReportEditor.DBTextSpeedButtonClick(Sender: TObject);
begin
  DBTextSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddDBText;
end;

procedure TReportEditor.ExpressionSpeedButtonClick(Sender: TObject);
begin
  ExpressionSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddExpression;
end;

procedure TReportEditor.SysDataSpeedButtonClick(Sender: TObject);
begin
  SysDataSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddSysData;
end;

procedure TReportEditor.ShapeSpeedButtonClick(Sender: TObject);
begin
  ShapeSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddShape;
end;

procedure TReportEditor.ImageSpeedButtonClick(Sender: TObject);
begin
  ImageSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddImage;
end;

procedure TReportEditor.DBImageSpeedButtonClick(Sender: TObject);
begin
  DBImageSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddDBImage;
end;

procedure TReportEditor.MemoSpeedButtonClick(Sender: TObject);
begin
  MemoSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddMemo;
end;

procedure TReportEditor.PreviewSpeedButtonClick(Sender: TObject);
begin
  QuickReport.Preview;
end;

procedure TReportEditor.PrintSpeedButtonClick(Sender: TObject);
var
  savedWindowState: TWindowState;
begin
  savedWindowState := WindowState;
  WindowState:=wsMinimized;
  QuickReport.Print;
  WindowState:=savedWindowState;
  BringToFront;
end;

procedure TReportEditor.OptionsSpeedButtonClick(Sender: TObject);
begin
  EditReportOptions(QRepDesigner1, QuickReport);
end;

procedure TReportEditor.FormCreate(Sender: TObject);
var
  TI: TIniFile;
  I,X,Y: Integer;
  S: String[100];
  DefaultExt: String[10];
begin
  ActiveBlobField:=NIL;
  ActiveStream:=NIL;
  OpenFileHistory:=TStringList.Create;
                                               
  {$IFDEF DefaultWithFlatSpeedbuttons_Form}
  For I:=0 to ComponentCount-1 Do
    If (Components[I] is TSpeedButton) Then TSpeedButton(Components[I]).Flat:=True;
  {$ENDIF}
  ReadQRDesignSetup(QRepDesigner1,QRDHelpFile);
  try
    TI:=TIniFile.Create(QRDesignINIFile);
    DefaultExt:=TI.ReadString('General','ReportFileExtension','.QR2');
    For I:=5 Downto 1 Do
      begin
        S:=TI.ReadString('History','History'+IntToStr(I),'');
        If S<>'' Then OpenFileHistory.Insert(0,S);
      end;
    TI.Free;
  except
  end;

  AppHelpFile:=#0;
  TypeLabel.Caption:='';
  OpenDialog1.InitialDir:=ExtractFilePath(ExpandFileName(ParamStr(0)));
  OpenDialog1.FileName:='*'+DefaultExt;
  OpenDialog1.Filter:='Reports|*'+DefaultExt;
  SaveDialog1.InitialDir:=ExtractFilePath(ExpandFileName(ParamStr(0)));
  SaveDialog1.Filter:='Reports|*'+DefaultExt;
  VertScrollBar.Visible:=FALSE;
  HorzScrollBar.Visible:=FALSE;
  VertScrollBar.Range:=0;
  HorzScrollBar.Range:=0;
  XPosLabel.Caption:='';
  YPosLabel.Caption:='';
  ElementsComboBox.Items.Clear;
  RulerPanel1.Zoom:=Quickreport.Zoom;
  RulerPanel1.Units:=Quickreport.Units;
  UndoMI.Visible:=QRepDesigner1.UndoEnabled;
  N3.Visible:=QRepDesigner1.UndoEnabled;

  {--- hide element speed buttons for unavailable components ---}
  {$IFNDEF WIN32}
  {$IFNDEF WPTools}
  RichTextSpeedButton.Visible:=FALSE;
  DBRichTextSpeedButton.Visible:=FALSE;
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF Teechart}
  TeechartSpeedButton.Visible:=FALSE;
  {$ENDIF}
  {$IFDEF Zorn_Barcode}
  DBBarcodeSpeedbutton.Visible:=True;
  BarcodeSpeedbutton.Visible:=True;
  {$ENDIF}
  {$IFDEF Schlottke_Barcode}
  DBBarcodeSpeedbutton.Visible:=True;
  {$ENDIF}
  {$IFDEF Poulsen_Barcode}
  DBBarcodeSpeedbutton.Visible:=True;
  {$ENDIF}
  {$IFDEF Mountaintop_Barcode}
  DBBarcodeSpeedbutton.Visible:=True;
  {$ENDIF}
  {$IFNDEF QR2PP}                            
  GridSpeedbutton.Visible:=False;
  CheckboxSpeedbutton.Visible:=False;
  {$ENDIF}

  {--- move element toolbar from left side to top if there is not enough room ---}
  Y:=20;
  For X:=0 to ElementPanel.ControlCount-1 Do
    If ElementPanel.Controls[X].Visible Then Y:=Y+29;
  If ((WindowState<>wsMaximized) and (Height<Y+120)) or
     ((WindowState=wsMaximized) and (Screen.Height<Y+120)) Then
    TopElementPanelMIClick(Self)
  Else
    LeftElementPanelMIClick(Self);

  {--- Datafield listbox ---}
  CreateDatafieldListbox(QRepDesigner1);
  DatafieldsListbox.Checked:=QRepDesigner1.ShowDatafieldListbox;
end;

procedure TReportEditor.LoadSpeedButtonClick(Sender: TObject);
begin
  If QRepDesigner1.HasChanged Then
    begin
      If MessageDlg(QRepDesigner1.RuntimeMessages.Values['Report not saved'],
                    mtconfirmation,[mbok,mbcancel],0)<>mrOk Then Exit;
    end;
  If ActiveBlobField<>NIL Then
    begin
      QRepDesigner1.LoadReport_FromBlobField(ActiveBlobField);
      RefreshFontCombobox;
      RulerPanel1.Zoom:=Quickreport.Zoom;
      RulerPanel1.Units:=Quickreport.Units;
    end
  Else
  If ActiveStream<>NIL Then
    begin
      ActiveStream.Seek(0,soFromBeginning);
      QRepDesigner1.LoadReport_FromStream(ActiveStream);
      RefreshFontCombobox;
      RulerPanel1.Zoom:=Quickreport.Zoom;
      RulerPanel1.Units:=Quickreport.Units;
    end
  Else
    begin
      OpenDialog1.HistoryList:=OpenFileHistory;
      OpenDialog1.FileName:='*'+ExtractFileExt(OpenDialog1.FileName);
      If OpenDialog1.Execute Then
        begin
          QRepDesigner1.LoadReport(OpenDialog1.FileName);
          RefreshFontCombobox;
          If OpenFileHistory.IndexOf(OpenDialog1.FileName)<>-1 Then
            OpenFileHistory.Delete(OpenFileHistory.IndexOf(OpenDialog1.FileName));
          OpenFileHistory.Insert(0,OpenDialog1.FileName);
          If OpenFileHistory.Count>20 Then OpenFileHistory.Delete(OpenFileHistory.Count-1);
          ReportFileName:=OpenDialog1.FileName;
          Caption:=ReportFileName;
          RulerPanel1.Zoom:=Quickreport.Zoom;
          RulerPanel1.Units:=Quickreport.Units;
          OpenDialog1.InitialDir:=ExtractFilePath(OpenDialog1.FileName);
          OpenDialog1.FileName:=ExtractFileName(OpenDialog1.FileName);
        end;
    end;
end;

procedure TReportEditor.SaveSpeedButtonClick(Sender: TObject);
begin
  If ActiveBlobField<>NIL Then
    begin
      QRepDesigner1.SaveReport_ToBlobField(ActiveBlobField);
    end
  Else
  If ActiveStream<>NIL Then
    begin
      QRepDesigner1.SaveReport_ToStream(ActiveStream);
    end
  Else
    begin
      If (ReportFileName='') Then
        begin
          If SaveDialog1.Execute Then
            begin
              ReportFileName:=SaveDialog1.FileName;
              QRepDesigner1.SaveReport(ReportFileName);
              Caption:=ReportFileName;
            end;
        end
      Else
        QRepDesigner1.SaveReport(ReportFileName);
    end;
end;

procedure TReportEditor.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  C: Integer;
begin
  If QRepDesigner1.HasChanged Then
    begin
      C:=MessageDlg(Format(QRepDesigner1.RuntimeMessages.Values['Save report'],[ReportFileName]),
                    mtconfirmation,[mbyes,mbno,mbcancel],0);
      If C=mrCancel Then
        begin
          CanClose:=FALSE;
          Exit;
        end;
      If C<>mrNo Then
        begin
          If ActiveBlobField<>NIL Then
            begin
              QRepDesigner1.SaveReport_ToBlobField(ActiveBlobField);
            end
          Else
          If ActiveStream<>NIL Then
            begin
              QRepDesigner1.SaveReport_ToStream(ActiveStream);
            end
          Else
            begin
              If (ReportFileName='') Then
                begin
                  If SaveDialog1.Execute Then
                    begin
                      ReportFileName:=SaveDialog1.FileName;
                      QRepDesigner1.SaveReport(ReportFileName);
                    end
                  Else
                    CanClose:=FALSE;
                end
              Else
                QRepDesigner1.SaveReport(ReportFileName);
            end;
        end;
    end;
end;

procedure TReportEditor.NewMIClick(Sender: TObject);
begin
  If QRepDesigner1.HasChanged and
     (MessageDlg(QRepDesigner1.RuntimeMessages.Values['Report not saved'],
                             mtconfirmation,[mbok,mbcancel],0)<>mrOk) Then Exit;
  ReportFileName:='';  SaveDialog1.FileName:='';
  If (ActiveBlobField=NIL) and (ActiveStream=NIL) Then Caption:=''; 
  QRDesignReportExpert(QRepDesigner1);
end;

procedure TReportEditor.SaveAsMIClick(Sender: TObject);
begin
  If SaveDialog1.Execute Then
    begin
      ReportFileName:=SaveDialog1.FileName;
      QRepDesigner1.SaveReport(ReportFileName);
      Caption:=ReportFileName;
    end;
end;

procedure TReportEditor.RefreshFontCombobox;
var
  X: Integer;
begin
  FontCombobox.Items.Clear;
  FontComboBox.Items:=Screen.Fonts;
  For X:=1 to Printer.Fonts.Count Do
    If FontComboBox.Items.IndexOf(Printer.Fonts[X-1])=-1 Then
      FontComboBox.Items.Add(Printer.Fonts[X-1]);
end;

procedure TReportEditor.PrinterSetupMIClick(Sender: TObject);
begin
  Quickreport.PrinterSetup;
  Printer.PrinterIndex:=Quickreport.PrinterSettings.PrinterIndex;
  RefreshFontCombobox;
  QRepDesigner1.AssignComponentEvents;
end;

procedure TReportEditor.ExitMIClick(Sender: TObject);
begin
  Close;
end;

procedure TReportEditor.Databases1Click(Sender: TObject);
begin
  EditReportDatabases(QRepDesigner1,QuickReport);
end;

procedure TReportEditor.QRepDesigner1EditProperties(QRD: TQRepDesigner;
  Component: TComponent; var EditResult: Boolean);
{$IFDEF IncludeCustomComponents}
var
  IsCustomComponent: Boolean;
{$ENDIF}
begin
  {$IFDEF IncludeCustomComponents}
  If (Component is TQRPrintable) Then
    begin
      EditResult:=CustomEditProperties(QRD,Self,TQRPrintable(Component), IsCustomComponent);
      If IsCustomComponent Then Exit;
    end;
  {$ENDIF}
  If (Component is TDesignQuickReport)  Then EditResult:=EditReportOptions  (QRD, TQuickRep(Component)) Else
  If (Component is TQRDesignDBText)     Then EditResult:=EditDBTextProps    (QRD, Self, TQRDesignDBText(Component)) Else
  If (Component is TQRDesignShape)      Then EditResult:=EditShapeProps     (QRD, Self, TQRDesignShape(Component)) Else
  If (Component is TQRDesignExpr)       Then EditResult:=EditExpressionProps(QRD, Self, TQRDesignExpr(Component)) Else
  If (Component is TQRDesignChildband)  Then EditResult:=EditChildProps     (QRD, Self, TQRDesignChildBand(Component)) Else
  If (Component is TQRDesignGroup)      Then EditResult:=EditGroupProps     (QRD, Self, TQRDesignGroup(Component)) Else
  If (Component is TQRDesignSubdetail)  Then EditResult:=EditSubdetailProps (QRD, Self, TQRDesignSubdetail(Component)) Else
  {$IFDEF WPTools}
  If (Component is TQRDesignRichtext)   Then EditResult:=EditRichtextProps  (QRD, Self, TQRDesignRichtext(Component)) Else
  If (Component is TQRDesignDBRichtext) Then EditResult:=EditDBRichtextProps(QRD, Self, TQRDesignDBRichtext(Component)) Else
  {$ELSE}
  {$IFDEF WIN32}
  If (Component is TQRDesignRichtext)   Then EditResult:=EditRichtextProps  (QRD, Self, TQRDesignRichtext(Component)) Else
  If (Component is TQRDesignDBRichtext) Then EditResult:=EditDBRichtextProps(QRD, Self, TQRDesignDBRichtext(Component)) Else
  {$ENDIF}
  {$ENDIF}
  If (Component is TQRDesignSysData)    Then EditResult:=EditSysDataProps   (QRD, Self, TQRDesignSysData(Component)) Else
  If (Component is TQRDesignImage)      Then EditResult:=EditImageProps     (QRD, Self, TQRDesignImage(Component)) Else
  If (Component is TQRDesignDBImage)    Then EditResult:=EditDBImageProps   (QRD, Self, TQRDesignDBImage(Component)) Else
  If (Component is TQRDesignLabel)      Then EditResult:=EditLabelProps     (QRD, Self, TQRDesignLabel(Component)) Else
  If (Component is TQRDesignMemo)       Then EditResult:=EditMemoProps      (QRD, Self, TQRDesignMemo(Component)) Else
  If (Component is TQRDesignBand)       Then EditResult:=EditBandProps      (QRD, Self, TQRDesignBand(Component)) Else
  {$IFDEF Teechart}
  If (Component is TQRDesignTeechart)   Then EditResult:=EditTeechartProps  (QRD, Self, TQRDesignTeechart(Component)) Else
  {$ENDIF}
  {$IFDEF QR2PP}
  If (Component is TQRDesignGrid)       Then EditResult:=EditGridProps      (QRD, Self, TQRDesignGrid(Component)) Else
  If (Component is TQRDesignCheckbox)   Then EditResult:=EditCheckboxProps  (QRD, Self, TQRDesignCheckbox(Component)) Else
  {$ENDIF}
end;

procedure TReportEditor.QRepDesigner1SelectionChanged(QRD: TQRepDesigner;
  NumberOfSelections: Integer);

  procedure DatafieldsToCombobox(C: TComponent; Dataset: TDataset);
  var
    L, FName: String[80];
    X: Integer;
  begin
    AllCombobox.Sorted:=True;
    AllCombobox.Items.Clear;
    If DataSet<>NIL Then
    try
      FName:=GetStringProperty(C,'Datafield');
      L:=FName;
      For X:=1 to DataSet.FieldCount Do
        begin
          If DataSet.Fields[X-1].Visible Then
            begin
              AllComboBox.Items.Add(DataSet.Fields[X-1].DisplayLabel);
              If DataSet.Fields[X-1].FieldName=FName Then
                L:=DataSet.Fields[X-1].DisplayLabel;
            end;
        end;
      For X:=1 to AllCombobox.Items.Count Do
        If AllCombobox.Items[X-1]=L Then
          begin
            AllComboBox.ItemIndex:=X-1;
            Break;
          end;
    except
    end;
    If AllComboBox.ItemIndex<0 Then AllComboBox.ItemIndex:=0;
    AllComboBox.Enabled:=Not(GetBooleanProperty(C,'BlockChange') OR
                             GetBooleanProperty(C,'BlockEdit'));
  end;


var
  C: TControl;
  A: TAlignment;
begin
  AlignMI.Enabled:=AlignLeftMI.Enabled;
  CenterMI.Enabled:=AlignCenterHorizontalMI.Enabled;
  ParentCenterMI.Enabled:=AlignCenterBandHorizontalMI.Enabled;
  EqualSpaceMI.Enabled:=AlignEqualHorizontalMI.Enabled;

  TextCenterAlignButton.Down:=FALSE;
  TextLeftAlignButton.Down:=FALSE;
  TextRightAlignButton.Down:=FALSE;

  If NumberOfSelections=1 Then
    begin
      C:=TControl(QRD.GetSelectedElement);
      If PropertyExists(C,'Alignment') Then
        begin
          A:=TAlignment(GetIntegerProperty(C,'Alignment'));
          If A=taCenter Then TextCenterAlignButton.Down:=TRUE
          Else
          If A=taRightJustify Then TextRightAlignButton.Down:=TRUE
          Else
            TextLeftAlignButton.Down:=TRUE;
        end;

      If C is TQRDesignLabel Then
        begin
          TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Label'];
          AllEdit.Visible:=TRUE;
          AllComboBox.Visible:=FALSE;
          AllEdit.Text:=TQRLabel(C).Caption;
          AllEdit.Enabled:=Not(GetBooleanProperty(C,'BlockChange') OR
                               GetBooleanProperty(C,'BlockEdit'));
        end
      Else
      If C is TQRExpr Then
        begin
          TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Expression'];
          AllEdit.Visible:=TRUE;
          AllComboBox.Visible:=FALSE;
          AllEdit.Text:=TQRExpr(C).Expression;
          AllEdit.Enabled:=Not(GetBooleanProperty(C,'BlockChange') OR
                               GetBooleanProperty(C,'BlockEdit'));
        end
      Else
        begin
          AllComboBox.Items.Clear;
          AllComboBox.Visible:=TRUE;
          AllCombobox.Sorted:=FALSE;
          AllEdit.Visible:=FALSE;
          With QRepDesigner1 Do
            begin
              {---- Band ----}
              If C is TQRChildBand Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Childband'];
                  AllCombobox.Visible:=FALSE;
                end
              Else
              If C is TQRSubDetail Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Subdetailband'];
                  AllCombobox.Visible:=FALSE;
                end
              Else
              If C is TQRGroup Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Groupband'];
                  AllCombobox.Visible:=FALSE;
                end
              Else
              If C is TQRDesignBand Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Band'];;
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[8]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[2]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[6]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[5]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[1]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[3]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[4]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[0]]);
                  Case TQRDesignBand(C).BandType of
                    rbColumnHeader: AllComboBox.ItemIndex:=0;
                    rbDetail:       AllComboBox.ItemIndex:=1;
                    rbGroupFooter:  AllComboBox.ItemIndex:=2;
                    rbGroupHeader:  AllComboBox.ItemIndex:=3;
                    rbPageHeader:   AllComboBox.ItemIndex:=4;
                    rbPageFooter:   AllComboBox.ItemIndex:=5;
                    rbSummary:      AllComboBox.ItemIndex:=6;
                    rbTitle:        AllComboBox.ItemIndex:=7;
                  end;
                  AllComboBox.Enabled:=Not(GetBooleanProperty(C,'BlockChange') OR
                                           GetBooleanProperty(C,'BlockEdit'));
                end
              Else
              {---- DBText ----}
              If C is TQRDesignDBText Then
                begin
                  DatafieldsToCombobox(C,TQRDesignDBText(C).DataSet);
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Datafield'];
                end
              Else
              {---- Memo ----}
              If C is TQRDesignMemo Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Memo'];
                  AllComboBox.Visible:=FALSE;
                end
              Else
              {---- DBImage ----}
              If C is TQRDesignDBImage Then
                begin
                  DatafieldsToCombobox(C,TQRDesignDBImage(C).DataSet);
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Image datafield'];
                end
              Else
              {---- Image ----}
              If C is TQRDesignImage Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Image'];
                  AllComboBox.Visible:=FALSE;
                end
              Else
              {---- Shape ----}
              If C is TQRDesignShape Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Shape'];
                  AllComboBox.Items.Add(RuntimeMessages.Values['ShapeCircle']);
                  AllComboBox.Items.Add(RuntimeMessages.Values['ShapeHorizontalLine']);
                  AllComboBox.Items.Add(RuntimeMessages.Values['ShapeRectangle']);
                  AllComboBox.Items.Add(RuntimeMessages.Values['ShapeLeftRightLines']);
                  AllComboBox.Items.Add(RuntimeMessages.Values['ShapeTopBottomLines']);
                  AllComboBox.Items.Add(RuntimeMessages.Values['ShapeVerticalLine']);
                  Case TQRDesignShape(C).Shape of
                    qrsCircle: AllComboBox.ItemIndex:=0;
                    qrsHorLine: AllComboBox.ItemIndex:=1;
                    qrsRectangle: AllComboBox.ItemIndex:=2;
                    qrsRightAndLeft: AllComboBox.ItemIndex:=3;
                    qrsTopAndBottom: AllComboBox.ItemIndex:=4;
                    qrsVertLine: AllComboBox.ItemIndex:=5;
                  end;
                  AllComboBox.Enabled:=Not(GetBooleanProperty(C,'BlockChange') OR
                                           GetBooleanProperty(C,'BlockEdit'));
                end
              Else
              {---- Sysdata ----}
              If C is TQRDesignSysdata Then
                begin
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Systemfield'];
                  AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[1]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[0]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[2]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[5]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[6]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[3]]);
                  AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[4]]);
                  Case TQRDesignSysdata(C).Data of
                    qrsDate: AllCombobox.ItemIndex:=0;
                    qrsTime: AllCombobox.ItemIndex:=1;
                    qrsDateTime: AllCombobox.ItemIndex:=2;
                    qrsDetailCount: AllCombobox.ItemIndex:=3;
                    qrsDetailNo: AllCombobox.ItemIndex:=4;
                    qrsPageNumber: AllCombobox.ItemIndex:=5;
                    qrsReportTitle: AllCombobox.ItemIndex:=6;
                  end;
                  AllComboBox.Enabled:=Not(GetBooleanProperty(C,'BlockChange') OR
                                           GetBooleanProperty(C,'BlockEdit'));
                end
              {$IFDEF WIN32}
              {$IFNDEF WPTools}
              Else
              If C is TQRDesignRichtext Then
                begin
                  AllEdit.Visible:=FALSE;
                  AllComboBox.Visible:=FALSE;
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Richtext'];
                end
              Else
              If C is TQRDesignDBRichtext Then
                begin
                  DatafieldsToCombobox(C,TQRDesignDBRichtext(C).DataSet);
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Richtext datafield'];
                end
              {$ENDIF}
              {$ENDIF}
              {$IFDEF WPTools}
              Else
              If C is TQRDesignRichtext Then
                begin
                  AllEdit.Visible:=FALSE;
                  AllComboBox.Visible:=FALSE;
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Richtext'];
                end
              Else
              If C is TQRDesignDBRichtext Then
                begin
                  DatafieldsToCombobox(C,TQRDesignDBRichtext(C).Datasource.DataSet);
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Richtext datafield'];
                end
              {$ENDIF}
              {$IFDEF Teechart}
              Else
              If C is TQRDesignTeechart Then
                begin
                  AllEdit.Visible:=FALSE;
                  AllComboBox.Visible:=FALSE;
                  TypeLabel.Caption:=QRepDesigner1.RuntimeMessages.Values['Chart'];
                end
              {$ENDIF}
              Else
                begin
                  AllEdit.Visible:=FALSE;
                  AllComboBox.Visible:=FALSE;
                  TypeLabel.Caption:='';
                end;
            end { with QRDesigner }
        end { NOT C is TQRDesignLabel }
    end { NumberOfSelections = 1 }
  Else
    begin
      AllEdit.Visible:=FALSE;
      AllComboBox.Visible:=FALSE;
      If NumberOfSelections=0 Then TypeLabel.Caption:='Report' Else TypeLabel.Caption:='';
    end;
  Scrollbox2.HorzScrollbar.Position:=ScrollBox1.HorzScrollbar.Position;
end;

procedure TReportEditor.Scrollbox1ScrollHorz(Sender: TObject);
begin
  Scrollbox2.HorzScrollbar.Position:=ScrollBox1.HorzScrollbar.Position;
end;

procedure TReportEditor.PropMenuTopClick(Sender: TObject);
begin
  PropMenuTop.Checked:=TRUE;
  PropMenuBottom.Checked:=FALSE;
  PropertyPanel.Align:=alTop;
end;

procedure TReportEditor.PropMenuBottomClick(Sender: TObject);
begin
  PropMenuTop.Checked:=FALSE;
  PropMenuBottom.Checked:=TRUE;
  PropertyPanel.Align:=alBottom;
  If StatusPanel.Visible Then
    PropertyPanel.Top:=StatusPanel.Top+1;
end;

procedure TReportEditor.AllEditChange(Sender: TObject);
var
  TC: TControl;
begin
  TC:=TControl(QRepDesigner1.GetSelectedElement);
  If TC=NIL Then Exit;
  If TC is TQRExpr Then
    begin
      If GetStringProperty(TC,'Expression')<>AllEdit.Text Then
       begin
         SetStringProperty(TC,'Expression',AllEdit.Text);
         QRepDesigner1.RedrawSelection;
       end;
    end
  Else
    begin
      If GetStringProperty(TC,'Caption')<>AllEdit.Text Then
       begin             
         SetStringProperty(TC,'Caption',AllEdit.Text);
         QRepDesigner1.RedrawSelection;
       end;
    end
end;


procedure TReportEditor.AllComboBoxChange(Sender: TObject);
var
  C: TControl;
  X: Integer;
begin
  C:=TControl(QRepDesigner1.GetSelectedElement);
  If C=NIL Then Exit;
  If C is TQRDesignDBText Then
    begin
      For X:=1 to TQRDesignDBText(C).DataSet.FieldCount Do
        begin
          If AllComboBox.Items[AllComboBox.ItemIndex]
             =TQRDesignDBText(C).DataSet.Fields[X-1].DisplayLabel Then
            begin
              TQRDesignDBText(C).DataField:=TQRDesignDBText(C).DataSet.Fields[X-1].FieldName;
              TQRDesignDBText(C).Caption:=TQRDesignDBText(C).DataSet.Fields[X-1].DisplayLabel;
            end;
        end;
    end
  Else
  If C is TQRDesignBand Then
    begin
      Case AllComboBox.ItemIndex of
        0: TQRDesignband(C).BandType:=rbColumnHeader;
        1: TQRDesignband(C).BandType:=rbDetail;
        2: TQRDesignband(C).BandType:=rbGroupFooter;
        3: TQRDesignband(C).BandType:=rbGroupHeader;
        4: TQRDesignband(C).BandType:=rbPageHeader;
        5: TQRDesignband(C).BandType:=rbPageFooter;
        6: TQRDesignband(C).BandType:=rbSummary;
        7: TQRDesignband(C).BandType:=rbTitle;
      end;
    end
  Else
  If C is TQRDesignDBImage Then
    begin
      For X:=1 to TQRDesignDBImage(C).DataSet.FieldCount Do
        begin
          If AllComboBox.Items[AllComboBox.ItemIndex]
             =TQRDesignDBImage(C).DataSet.Fields[X-1].DisplayLabel Then
            begin
              TQRDesignDBImage(C).DataField:=TQRDesignDBImage(C).DataSet.Fields[X-1].FieldName;
            end;
        end;
    end
  Else
  {$IFDEF WIN32}
  {$IFNDEF WPTools}
  If C is TQRDesignDBRichtext Then
    begin
      For X:=1 to TQRDesignDBRichtext(C).DataSet.FieldCount Do
        begin
          If AllComboBox.Items[AllComboBox.ItemIndex]
             =TQRDesignDBRichtext(C).DataSet.Fields[X-1].DisplayLabel Then
            begin
              TQRDesignDBRichtext(C).DataField:=TQRDesignDBRichtext(C).DataSet.Fields[X-1].FieldName;
            end;
        end;
    end
  Else
  {$ENDIF}
  {$ENDIF}
  {$IFDEF WPTools}
  If C is TQRDesignDBRichtext Then
    begin
      For X:=1 to TQRDesignDBRichtext(C).Datasource.DataSet.FieldCount Do
        begin
          If AllComboBox.Items[AllComboBox.ItemIndex]
             =TQRDesignDBRichtext(C).Datasource.DataSet.Fields[X-1].DisplayLabel Then
            begin
              TQRDesignDBRichtext(C).DataField:=
                TQRDesignDBRichtext(C).Datasource.DataSet.Fields[X-1].FieldName;
            end;
        end;
    end
  Else
  {$ENDIF}
  If C is TQRDesignShape Then
    begin
      Case AllComboBox.ItemIndex of
        0: TQRDesignShape(C).Shape:=qrsCircle;
        1: TQRDesignShape(C).Shape:=qrsHorLine;
        2: TQRDesignShape(C).Shape:=qrsRectangle;
        3: TQRDesignShape(C).Shape:=qrsRightAndLeft;
        4: TQRDesignShape(C).Shape:=qrsTopAndBottom;
        5: TQRDesignShape(C).Shape:=qrsVertLine;
      end;
    end
  Else
  If C is TQRDesignSysdata Then
    begin
      Case AllComboBox.ItemIndex of
        0: TQRDesignSysdata(C).Data:=qrsDate;
        1: TQRDesignSysdata(C).Data:=qrsTime;
        2: TQRDesignSysdata(C).Data:=qrsDateTime;
        3: TQRDesignSysdata(C).Data:=qrsDetailCount;
        4: TQRDesignSysdata(C).Data:=qrsDetailNo;
        5: TQRDesignSysdata(C).Data:=qrsPageNumber;
        6: TQRDesignSysdata(C).Data:=qrsReportTitle;
      end;
    end;
  QRepDesigner1.SelectElement(True, C);
end;

procedure TReportEditor.Visible1Click(Sender: TObject);
begin
  PropertyPanel.Visible:=FALSE;
  FontToolbar1.Checked:=FALSE;
end;

procedure TReportEditor.SpeedButton7Click(Sender: TObject);
begin
  Close;
end;

procedure TReportEditor.Toolbar1Click(Sender: TObject);
begin
  Toolbar1.Checked:=NOT Toolbar1.Checked;
  ToolbarPanel.Visible:=Toolbar1.Checked;
  ToolbarPanel.Top:=-1;
end;

procedure TReportEditor.Fonttoolbar1Click(Sender: TObject);
begin
  FontToolbar1.Checked:=NOT FontToolbar1.Checked;
  PropertyPanel.Visible:=FontToolbar1.Checked;
  PropertyPanel.Top:=-1;
  If ToolbarPanel.Visible Then ToolbarPanel.Top:=-1;
end;

procedure TReportEditor.Elementtoolbar1Click(Sender: TObject);
begin
  ElementToolbar1.Checked:=NOT ElementToolbar1.Checked;
  ElementPanel.Visible:=ElementToolbar1.Checked;
  If ToolbarPanel.Visible Then ToolbarPanel.Top:=-1;
end;

procedure TReportEditor.Options2Click(Sender: TObject);
begin
  EditViewOptions(Quickreport, QRepDesigner1);
  RulerPanel1.Units:=Quickreport.Units;
end;

procedure TReportEditor.QRepDesigner1ShowInfo(Component: TComponent;
  var Info: QRDShowInfoString);
var
  U: String[15];
begin
  Info:='';
  If Component=NIL Then
    begin
      XPosLabel.Caption:='';
      YPosLabel.Caption:='';
      ElementsComboBox.Items.Clear;
    end
  Else
    begin
      ElementsComboBox.Items.Clear;
      If Component.Name<>'' Then
        begin
          ElementsComboBox.Items.Add(Component.Name);
          ElementsComboBox.ItemIndex:=0;
        end;
      Case Quickreport.Units of
        MM: U:=' mm';
        Inches: U:=' Inches';
        Characters: U:=' Chars';
        Else U:='';
      end;
      If Component is TQRPrintable Then
        begin
          Label1.Visible:=True;
          XPosLabel.Caption:=IntToStr(TControl(Component).Left)
            +' ('+FloatToStrF(TQRPrintable(Component).Size.Left,ffFixed,18,2)+U+')';
          YPosLabel.Caption:=IntToStr(TControl(Component).Top)
            +' ('+FloatToStrF(TQRPrintable(Component).Size.Top,ffFixed,18,2)+U+')';
        end
      else
        begin
          Label1.Visible:=False;
          XPosLabel.Caption:='';
          YPosLabel.Caption:='';
        end;
    end;
end;

procedure TReportEditor.MenuItem19Click(Sender: TObject);
begin
  MessageDlg('QRDesign (C) Copyright 1998 by'#13+
             'THSD, Timo Hartmann, Oststr. 38, 57074 Siegen, GERMANY'#13+
             'Email: thsd@thsd.de - http://www.thsd.de'#13#13+
             'QRDesign v'+QRepDesigner1.Version+' / '+cQRName,mtinformation,[mbok],0);
end;

procedure TReportEditor.Statusline1Click(Sender: TObject);
begin
  StatusLine1.Checked:=NOT StatusLine1.Checked;
  StatusPanel.Visible:=StatusLine1.Checked;
  StatusPanel.Top:=Height+1;
end;

procedure TReportEditor.TopElementPanelMIClick(Sender: TObject);
var
  X,Y: Integer;                     
begin
  ElementPanel.Align:=alTop;
  ElementPanel.Height:=38;
  If PropertyPanel.Visible AND (PropertyPanel.Align=alTop) then ElementPanel.Top:=PropertyPanel.Top-1;
  Y:=0;
  For X:=0 to ElementPanel.ControlCount-1 Do
    If ElementPanel.Controls[X].Visible Then
      begin
        ElementPanel.Controls[X].Left:=8+Y*29;
        ElementPanel.Controls[X].Top:=6;
        Inc(Y);
      end;
  LeftElementPanelMI.Checked:=FALSE;
  TopElementPanelMI.Checked:=TRUE;
end;

procedure TReportEditor.LeftElementPanelMIClick(Sender: TObject);
var
  X,Y: Integer;                                    
begin
  If ElementPanel.Align=alLeft Then Exit;
  ElementPanel.Align:=alLeft;
  ElementPanel.Width:=39;
  Y:=0;
  For X:=0 to ElementPanel.ControlCount-1 Do
    If ElementPanel.Controls[X].Visible Then
      begin
        ElementPanel.Controls[X].Left:=6;
        ElementPanel.Controls[X].Top:=8+Y*28;
        Inc(Y);
      end;
  LeftElementPanelMI.Checked:=TRUE;
  TopElementPanelMI.Checked:=FALSE;
end;

procedure TReportEditor.LabelSpeedButtonMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  QRepDesigner1.AddMultipleElements:=ssShift in Shift;
end;

procedure TReportEditor.SubdetailSpeedButtonClick(Sender: TObject);
begin
  SubDetailSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddSubDetail;
end;

procedure TReportEditor.ChildSpeedbuttonClick(Sender: TObject);
begin
  ChildSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddChildband;
end;

procedure TReportEditor.GroupSpeedButtonClick(Sender: TObject);
begin
  GroupSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddGroup;
end;

procedure TReportEditor.RichtextSpeedButtonClick(Sender: TObject);
begin
  RichtextSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddRichtext;
end;

procedure TReportEditor.DBRichtextSpeedButtonClick(Sender: TObject);
begin
  DBRichtextSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddDBRichtext;
end;

procedure TReportEditor.Normalgre1Click(Sender: TObject);
begin
  QuickReport.Zoom:=100;
  QRepDesigner1.RedrawSelection;
  RulerPanel1.Zoom:=Quickreport.Zoom;
end;

procedure TReportEditor.Vergrern1Click(Sender: TObject);
begin
  If QuickReport.Zoom*2>300 Then
    QuickReport.Zoom:=300
  Else
    QuickReport.Zoom:=QuickReport.Zoom*2;
  QRepDesigner1.RedrawSelection;
  RulerPanel1.Zoom:=Quickreport.Zoom;
end;

procedure TReportEditor.Verkleinern1Click(Sender: TObject);
begin
  If QuickReport.Zoom/2>10 Then QuickReport.Zoom:=Round(QuickReport.Zoom/2);
  QRepDesigner1.RedrawSelection;
  RulerPanel1.Zoom:=Quickreport.Zoom;
end;

procedure TReportEditor.FrameSpeedButtonClick(Sender: TObject);
begin
  EditFrameProperty(QRepDesigner1);
end;

procedure TReportEditor.TeechartSpeedButtonClick(Sender: TObject);
begin
  TeechartSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddTeechart;
end;

procedure TReportEditor.FormDestroy(Sender: TObject);
begin
  OpenFileHistory.Free;
end;

procedure TReportEditor.FormShow(Sender: TObject);
begin
  SaveAsMI.Visible:=(ActiveBlobField=NIL) AND (ActiveStream=NIL);
  AppHelpFile:=Application.HelpFile;
  If QRDHelpFile<>'' Then Application.HelpFile:=QRDHelpFile
  Else
  If FileExists(ExtractFilePath(ParamStr(0))+'QRDUSER.HLP') Then
    Application.HelpFile:=ExtractFilePath(ParamStr(0))+'QRDUSER.HLP';
  If ParamCount>0 Then
    begin
      QRepDesigner1.LoadReport(ParamStr(1));
      RefreshFontCombobox;
      If OpenFileHistory.IndexOf(ParamStr(1))<>-1 Then
        OpenFileHistory.Delete(OpenFileHistory.IndexOf(ParamStr(1)));
      OpenFileHistory.Insert(0,ParamStr(1));
      If OpenFileHistory.Count>20 Then OpenFileHistory.Delete(OpenFileHistory.Count-1);
      ReportFileName:=ParamStr(1);
      Caption:=ReportFileName;
      RulerPanel1.Zoom:=Quickreport.Zoom;
      RulerPanel1.Units:=Quickreport.Units;
      OpenDialog1.InitialDir:=ExtractFilePath(ParamStr(1));
      OpenDialog1.FileName:=ExtractFileName(ParamStr(1));
    end;
end;

procedure TReportEditor.FormClose(Sender: TObject; var Action: TCloseAction);
{$IFDEF AutoSaveSetup}
var
  TI: TIniFile;
  X: Integer;
{$ENDIF}
begin
  If AppHelpFile<>#0 Then Application.HelpFile:=AppHelpFile;
  {$IFDEF AutoSaveSetup}
  SaveQRDesignUserSetup(QRepDesigner1);
  try
    TI:=TIniFile.Create(QRDesignINIFile);
    For X:=1 to 5 Do
      begin
        If X>OpenFileHistory.Count Then Break;
        TI.WriteString('History','History'+IntToStr(X),OpenFileHistory[X-1]);
      end;
    TI.Free;
  except
  end;
  {$ENDIF}
end;

procedure TReportEditor.Help1Click(Sender: TObject);
begin
  Application.HelpContext(90001);
end;

procedure TReportEditor.Panel2DblClick(Sender: TObject);
begin
  QRepDesigner1.EditProperties(QRepDesigner1.GetSelectedElement);
end;

procedure TReportEditor.ElementsComboBoxDropDown(Sender: TObject);
var
  I: Integer;
begin
  ElementsCombobox.Items.Clear;
  For I:=0 to ComponentCount-1 Do
    begin
      If (PropertyExists(Components[I],'BlockDelete')) and (Components[I].Name<>'')
       Then ElementsCombobox.Items.Add(Components[I].Name);
    end;
end;

procedure TReportEditor.ElementsComboBoxChange(Sender: TObject);
var
  C: TControl;
begin
  C:=TControl(FindComponent(ElementsCombobox.Text));
  If (C<>NIL) Then QRepDesigner1.SelectElement(True, C);
end;

procedure TReportEditor.UndoMIClick(Sender: TObject);
begin
  QRepDesigner1.DoUndo;
end;

procedure TReportEditor.Rotatebands1Click(Sender: TObject);
begin
  Quickreport.RotateBands:=Quickreport.RotateBands+1;
end;

procedure TReportEditor.Resetbands1Click(Sender: TObject);
begin
  Quickreport.RotateBands:=0;
  Quickreport.HideBands:=False;
  HideBands1.Checked:=False;
end;

procedure TReportEditor.Hidebands1Click(Sender: TObject);
begin
  HideBands1.Checked:=Not HideBands1.Checked;
  Quickreport.HideBands:=HideBands1.Checked;
end;

procedure TReportEditor.DBBarcodeSpeedButtonClick(Sender: TObject);
begin
  {$IFDEF IncludeCustomComponents}
  DBBarcodeSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddDBBarcode;
  {$ENDIF}
end;

procedure TReportEditor.BarcodeSpeedButtonClick(Sender: TObject);
begin
  {$IFDEF IncludeCustomComponents}
  BarcodeSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddBarcode;
  {$ENDIF}
end;

procedure TReportEditor.FileMenuClick(Sender: TObject);
var
  MI: TMenuItem;
  X: Integer;
begin
  If (ActiveBlobField<>NIL) or (ActiveStream<>NIL) Then Exit;
  For X:=1 to 5 Do
    begin
      MI:=TMenuItem(FindComponent('FileHistory'+IntToStr(X)));
      MI.Visible:=OpenFileHistory.Count>=X;
      If MI.Visible Then MI.Caption:='&'+IntToStr(X)+' '+OpenFileHistory[X-1];
    end;
  FileHistory0.Visible:=OpenFileHistory.Count>0;
end;

procedure TReportEditor.FileHistory1Click(Sender: TObject);
var
  N: String;
begin
  If QRepDesigner1.HasChanged Then
    begin
      If MessageDlg(QRepDesigner1.RuntimeMessages.Values['Report not saved'],
                    mtconfirmation,[mbok,mbcancel],0)<>mrOk Then Exit;
    end;
  N:=Copy(TMenuItem(Sender).Caption,4,255);
  QRepDesigner1.LoadReport(N);
  RefreshFontCombobox;
  If OpenFileHistory.IndexOf(N)<>-1 Then OpenFileHistory.Delete(OpenFileHistory.IndexOf(N));
  OpenFileHistory.Insert(0,N);
  If OpenFileHistory.Count>20 Then OpenFileHistory.Delete(OpenFileHistory.Count-1);
  ReportFileName:=N;
  Caption:=ReportFileName;
  RulerPanel1.Zoom:=Quickreport.Zoom;
  RulerPanel1.Units:=Quickreport.Units;
  OpenDialog1.InitialDir:=ExtractFilePath(N);
  OpenDialog1.FileName:=ExtractFileName(N);
end;

procedure TReportEditor.CheckboxSpeedButtonClick(Sender: TObject);
begin
  {$IFDEF QR2PP}                           
  CheckboxSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddCheckbox;
  {$ENDIF}
end;

procedure TReportEditor.GridSpeedButtonClick(Sender: TObject);
begin
  {$IFDEF QR2PP}                           
  GridSpeedButton.Down:=TRUE;
  QRepDesigner1.QRDesignState:=qrds_AddGrid;
  {$ENDIF}
end;

procedure TReportEditor.FontComboBoxChange(Sender: TObject);
begin
  Panel1.Caption:=FontCombobox.Text;
end;

procedure TReportEditor.FontColorSpeedbuttonClick(Sender: TObject);
begin
  With TColorDialog.Create(Self) Do
    begin
      If Execute Then QRepDesigner1.SetSelectedElementsFontColor(Color);
      Free;
    end;
end;

procedure TReportEditor.DatafieldsListboxClick(Sender: TObject);
begin
  DatafieldsListbox.Checked:=NOT DatafieldsListbox.Checked;
  QRepDesigner1.ShowDatafieldListbox:=DatafieldsListbox.Checked;
end;

procedure TReportEditor.ViewMenuClick(Sender: TObject);
begin
  DatafieldsListbox.Checked:=(QRepDesigner1.DatafieldListbox<>NIL) and
                             (TForm(QRepDesigner1.DatafieldListbox.Owner).Visible);
  QRepDesigner1.ShowDatafieldListbox:=DatafieldsListbox.Checked;
end;

end.



