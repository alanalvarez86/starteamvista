unit FSeleccion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, ExtCtrls, StdCtrls, Buttons;

type
  TSeleccion = class(TZetaDlgModal)
    RGSeleccion: TRadioGroup;
    Panel1: TPanel;
    PlantillaHecha: TImage;
    PlantillaNueva: TImage;
    procedure RGSeleccionClick(Sender: TObject);
  private
    function GetAbrirPlantilla: Boolean;
    function GetCerrar: Boolean;
    { Private declarations }
  public
    property AbrirPlantilla : Boolean read GetAbrirPlantilla;
    property Cerrar : Boolean read GetCerrar;
  end;

var
  Seleccion: TSeleccion;

implementation

{$R *.DFM}

function TSeleccion.GetAbrirPlantilla: Boolean;
begin
     Result := RGSeleccion.ItemIndex = 0;
end;

procedure TSeleccion.RGSeleccionClick(Sender: TObject);
 var lAbrir : Boolean;
begin
     inherited;
     lAbrir := GetAbrirPlantilla;
     PlantillaHecha.Visible := lAbrir;
     PlantillaNueva.Visible := NOT lAbrir;
end;

function TSeleccion.GetCerrar: Boolean;
begin
     Result := ModalResult = mrCancel;
end;

end.
