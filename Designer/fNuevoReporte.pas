unit FNuevoReporte;
{$INCLUDE DEFINES.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo,
  ZetaCommonLists, ZetaTipoEntidad;

type
  TNuevoRep = class(TForm)
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Panel1: TPanel;
    gbTablaPrincipal: TGroupBox;
    LBTablas: TListBox;
    RGClasificacion: TListBox;
    procedure RGClasificacionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LBTablasDblClick(Sender: TObject);
  private
         function GetNombreReporte : string;
         function GetTablaPrincipal : TipoEntidad;
         procedure LlenaListBox;
  public
        property TablaPrincipal : TipoEntidad read GetTablaPrincipal;
        property NombreReporte : string read GetNombreReporte;
  end;

var
  NuevoRep: TNuevoRep;

implementation

uses ZetaCommonClasses,
     DCliente,
     DDiccionario;

{$R *.DFM}

function TNuevoRep.GetTablaPrincipal : TipoEntidad;
begin
     with LbTablas do
          Result := TipoEntidad(Items.Objects[ItemIndex]);
end;

function TNuevoRep.GetNombreReporte : string;
begin
     Result := '<PLANTILLA NUEVA>';
end;

procedure TNuevoRep.RGClasificacionClick(Sender: TObject);
begin
     LlenaListBox;
end;

procedure TNuevoRep.LlenaListBox;
 var iOffset: integer;
begin
     {$ifdef RDD}
     iOffset :=0;
     {$else}
     iOffset :=1;
     {$endif}

     LBTablas.Items.Clear;
     with dmDiccionario, RgClasificacion do
          TablasPorClasificacion( Integer(Items.Objects[ItemIndex])+iOffset,
                                  LBTablas.Items );

     LBTablas.ItemIndex:= 0;
end;

procedure TNuevoRep.FormCreate(Sender: TObject);
begin
     dmDiccionario.GetListaClasifi(RGClasificacion.Items,FALSE);

     RGClasificacion.ItemIndex := dmDiccionario.GetPosClasifi( RGClasificacion.Items,
                                                               dmDiccionario.ClasifiDefault );

     LlenaListBox;
     {.$ifndef TRESS}//{.ifdef SELECCION}{.ifdef VISITANTES}
     if NOT dmCliente.ModoTress then
     begin
          RgClasificacion.Visible := FALSE;
          gbTablaPrincipal.Left := 87;
     end;
     {.$endif}
end;

procedure TNuevoRep.LBTablasDblClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

end.
