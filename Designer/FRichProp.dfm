object RichtextForm: TRichtextForm
  Left = 154
  Top = 111
  HelpContext = 90037
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Richtext'
  ClientHeight = 373
  ClientWidth = 496
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 16
    Top = 240
    Width = 465
    Height = 89
    TabOrder = 1
    object Label1: TLabel
      Tag = 1000
      Left = 16
      Top = 27
      Width = 81
      Height = 21
      AutoSize = False
      Caption = '&Alignment'
      FocusControl = AlignmentComboBox
    end
    object StretchCheckbox: TCheckBox
      Tag = 1001
      Left = 16
      Top = 56
      Width = 233
      Height = 17
      Caption = 'Autostretch height'
      TabOrder = 1
    end
    object AlignmentComboBox: TComboBox
      Tag = 1007
      Left = 104
      Top = 24
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Left'
        'Right'
        'Centered')
    end
    object Button1: TButton
      Left = 360
      Top = 24
      Width = 89
      Height = 25
      Caption = '&Color...'
      TabOrder = 3
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 264
      Top = 24
      Width = 89
      Height = 25
      Caption = '&Font...'
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object Button3: TButton
    Left = 170
    Top = 340
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Button4: TButton
    Left = 250
    Top = 340
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object GroupBox2: TGroupBox
    Left = 16
    Top = 8
    Width = 465
    Height = 225
    TabOrder = 0
    object RichEdit1: TRichEdit
      Left = 16
      Top = 22
      Width = 433
      Height = 153
      TabOrder = 0
    end
    object Button5: TButton
      Left = 17
      Top = 186
      Width = 140
      Height = 25
      Caption = 'Editor...'
      TabOrder = 1
      OnClick = Button5Click
    end
    object ExpressionButton: TButton
      Left = 162
      Top = 186
      Width = 140
      Height = 25
      Caption = 'Expression...'
      TabOrder = 2
      OnClick = ExpressionButtonClick
    end
    object RemoveEmptyLineButton: TButton
      Left = 307
      Top = 186
      Width = 140
      Height = 25
      Caption = 'Remove line if empty'
      TabOrder = 3
      OnClick = RemoveEmptyLineButtonClick
    end
  end
  object ColorDialog1: TColorDialog
    Left = 344
    Top = 232
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = []
    Left = 424
    Top = 232
  end
end
