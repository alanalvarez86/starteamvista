object EditImageDir: TEditImageDir
  Left = 184
  Top = 187
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Directorio de Imagen'
  ClientHeight = 128
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 23
    Top = 6
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Directorio:'
  end
  object SpeedButton1: TSpeedButton
    Left = 447
    Top = 0
    Width = 25
    Height = 25
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333FFF333333333333000333333333
      3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
      3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
      0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
      BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
      33337777773FF733333333333300033333333333337773333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    OnClick = SpeedButton1Click
  end
  object Label2: TLabel
    Left = 6
    Top = 30
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Rotar Imagen:'
  end
  object EDirectorio: TEdit
    Left = 79
    Top = 2
    Width = 363
    Height = 21
    TabOrder = 0
    Text = 'EDirectorio'
  end
  object Panel1: TPanel
    Left = 0
    Top = 98
    Width = 478
    Height = 30
    Align = alBottom
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 303
      Top = 3
      Width = 75
      Height = 25
      Caption = '&OK'
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 383
      Top = 3
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
    end
  end
  object CBRotacion: TComboBox
    Left = 79
    Top = 26
    Width = 145
    Height = 21
    Style = csDropDownList
    TabOrder = 2
    Items.Strings = (
      'No Rotar'
      '90 Grados'
      '180 Grados'
      '270 Grados')
  end
  object CBAutoSize: TCheckBox
    Left = 80
    Top = 51
    Width = 145
    Height = 17
    Caption = 'Tama'#241'o Autom'#225'tico'
    TabOrder = 3
  end
  object CBStretch: TCheckBox
    Left = 80
    Top = 72
    Width = 145
    Height = 17
    Caption = 'Ajuste Autom'#225'tico'
    TabOrder = 4
  end
  object OpenPictureDialog: TOpenPictureDialog
    DefaultExt = 'BMP'
    Filter = 'Bitmaps (*.bmp)|*.bmp'
    Left = 312
    Top = 25
  end
end
