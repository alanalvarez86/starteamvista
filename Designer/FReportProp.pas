unit FReportProp;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Buttons, quickrpt, QRDesign;

type
  TReportPropForm = class(TForm)
    OrientationRadioGroup: TRadioGroup;
    Button3: TButton;
    Button4: TButton;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    BitBtn7: TBitBtn;
    PenStyleComboBox: TComboBox;
    TopCheckBox: TCheckBox;
    BottomCheckBox: TCheckBox;
    LeftCheckBox: TCheckBox;
    RightCheckBox: TCheckBox;
    PaperSizeGB: TGroupBox;
    PaperSize: TComboBox;
    MarginsGB: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    UnitLabel1: TLabel;
    UnitLabel4: TLabel;
    UnitLabel5: TLabel;
    UnitLabel6: TLabel;
    UnitLabel7: TLabel;
    GroupBox1: TGroupBox;
    TitleEdit: TEdit;
    ColorDialog1: TColorDialog;
    GroupBox3: TGroupBox;
    Button2: TButton;
    FontDialog1: TFontDialog;
    TopMargin: TEdit;
    BottomMargin: TEdit;
    LeftMargin: TEdit;
    RightMargin: TEdit;
    ColumnSpace: TEdit;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton12: TSpeedButton;
    SpeedButton14: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton11: TSpeedButton;
    SpeedButton13: TSpeedButton;
    SpeedButton15: TSpeedButton;
    SpeedButton16: TSpeedButton;
    Columns: TEdit;
    SpeedButton17: TSpeedButton;
    SpeedButton18: TSpeedButton;
    PenWidthSpinEdit: TEdit;
    SpeedButton19: TSpeedButton;
    SpeedButton20: TSpeedButton;
    Panel1: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    UnitLabel2: TLabel;
    UnitLabel3: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton3: TSpeedButton;
    WidthSpinEdit: TEdit;
    LengthSpinEdit: TEdit;
    procedure BitBtn7Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure TopMarginKeyPress(Sender: TObject; var Key: Char);
    procedure WidthSpinEditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure ColumnsKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton14Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton17Click(Sender: TObject);
    procedure SpeedButton18Click(Sender: TObject);
    procedure SpeedButton19Click(Sender: TObject);
    procedure SpeedButton20Click(Sender: TObject);
    procedure PaperSizeChange(Sender: TObject);
  private
  public
    FrameColor: TColor;
    TF: TFont;
    procedure SpinButtonUpClick(Sender: TObject);
    procedure SpinButtonDownClick(Sender: TObject);
  end;

function _EditReportOptions(QRD: TComponent; Rep: TQuickRep;sCaption:string): Boolean;

{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}

implementation

{$R *.DFM}

uses
  Printers, QRPrntr, QRCtrls;

procedure TReportPropForm.BitBtn7Click(Sender: TObject);
begin
  ColorDialog1.Color:=FrameColor;
  If ColorDialog1.Execute Then FrameColor:=ColorDialog1.Color;
end;

procedure TReportPropForm.Button2Click(Sender: TObject);
begin
  FontDialog1.Font.Assign(TF);
  If FontDialog1.Execute Then TF.Assign(FontDialog1.Font);
end;

procedure TReportPropForm.TopMarginKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Not (Key in [#32..#255]) Then Exit;
  If (Key='.') or (Key=',') Then Key:= {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator;
  If Not (Key in ['0'..'9',{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator]) Then Key:=#0;
end;

procedure TReportPropForm.SpinButtonDownClick(Sender: TObject);
var
  F: Real;
begin
  Try
    F:=StrToFloat(TEdit(Sender).Text)-1;
    If F>=0 Then
      TEdit(Sender).Text:=FormatFloat('',F);
  Except
    Exit;
  End;
end;

procedure TReportPropForm.SpinButtonUpClick(Sender: TObject);
var
  F: Real;
begin
  Try
    F:=StrToFloat(TEdit(Sender).Text)+1;
    TEdit(Sender).Text:=FormatFloat('',F);
  Except
    Exit;
  End;
end;


procedure TReportPropForm.WidthSpinEditChange(Sender: TObject);
begin
  PaperSize.ItemIndex:=PaperSize.Items.IndexOf(QRPaperName(Custom));
end;

procedure TReportPropForm.FormCreate(Sender: TObject);
begin
  TranslateForm(Self, 'ReportPropertyForm');
end;

procedure TReportPropForm.SpeedButton1Click(Sender: TObject);
begin
  SpinButtonUpClick(WidthSpinEdit);
end;

procedure TReportPropForm.SpeedButton2Click(Sender: TObject);
begin
  SpinButtonDownClick(WidthSpinEdit);
end;

procedure TReportPropForm.SpeedButton4Click(Sender: TObject);
begin
  SpinButtonUpClick(LengthSpinEdit);
end;

procedure TReportPropForm.SpeedButton3Click(Sender: TObject);
begin
  SpinButtonDownClick(LengthSpinEdit);
end;

procedure TReportPropForm.ColumnsKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Not (Key in ['1'..'9']) Then Key:=#0;
end;

procedure TReportPropForm.SpeedButton5Click(Sender: TObject);
begin
  SpinButtonUpClick(TopMargin);
end;

procedure TReportPropForm.SpeedButton6Click(Sender: TObject);
begin
  SpinButtonDownClick(TopMargin);
end;

procedure TReportPropForm.SpeedButton14Click(Sender: TObject);
begin
  SpinButtonUpClick(BottomMargin);
end;

procedure TReportPropForm.SpeedButton13Click(Sender: TObject);
begin
  SpinButtonDownClick(BottomMargin);
end;

procedure TReportPropForm.SpeedButton10Click(Sender: TObject);
begin
  SpinButtonUpClick(LeftMargin);
end;

procedure TReportPropForm.SpeedButton9Click(Sender: TObject);
begin
  SpinButtonDownClick(LeftMargin);
end;

procedure TReportPropForm.SpeedButton15Click(Sender: TObject);
begin
  SpinButtonUpClick(RightMargin);
end;

procedure TReportPropForm.SpeedButton16Click(Sender: TObject);
begin
  SpinButtonDownClick(RightMargin);
end;

procedure TReportPropForm.SpeedButton12Click(Sender: TObject);
begin
  SpinButtonUpClick(ColumnSpace);
end;

procedure TReportPropForm.SpeedButton11Click(Sender: TObject);
begin
  SpinButtonDownClick(ColumnSpace);
end;

procedure TReportPropForm.SpeedButton17Click(Sender: TObject);
begin
  Try
    Columns.Text:=IntToStr(StrToInt(Columns.Text)+1);
  Except
  end;
end;

procedure TReportPropForm.SpeedButton18Click(Sender: TObject);
begin
  Try
    If StrToInt(Columns.Text)>1 Then
      Columns.Text:=IntToStr(StrToInt(Columns.Text)-1);
  Except
  end;
end;

procedure TReportPropForm.SpeedButton19Click(Sender: TObject);
begin
  Try
    PenWidthSpinEdit.Text:=IntToStr(StrToInt(PenWidthSpinEdit.Text)+1);
  Except
  end;
end;

procedure TReportPropForm.SpeedButton20Click(Sender: TObject);
begin
  Try
    If StrToInt(PenWidthSpinEdit.Text)>1 Then
      PenWidthSpinEdit.Text:=IntToStr(StrToInt(PenWidthSpinEdit.Text)-1);
  Except
  end;
end;

procedure TReportPropForm.PaperSizeChange(Sender: TObject);
begin
  Panel1.Visible:=PaperSize.Items[PaperSize.ItemIndex]=QRPaperName(Custom);
end;

{------------------------------------------------------------------------------}
{------------------------------------------------------------------------------}

function _EditReportOptions(QRD: TComponent; Rep: TQuickRep;sCaption:string): Boolean;
var
  PS : TQRPaperSize;
  AU : String[20];
  X  : Integer;
  ReportPropertyForm: TReportPropForm;
begin
  ReportPropertyForm:=TReportPropForm.Create(Rep.Owner);
  With ReportPropertyForm Do
    begin
      case Rep.Units of
        Inches: AU:='Inches';
        Pixels: AU:='Pixels';
        MM    : AU:='mm';
        Characters: AU:='Chars'
      else AU:='';
      end;
      For X:=1 to 7 Do
        begin
          try
            TLabel(FindComponent('UnitLabel'+IntToStr(X))).Caption:=AU;
          except
          end;
        end;
      TF:=TFont.Create;
      TF.Assign(Rep.Font);
      TitleEdit.Text:=sCaption;
      If Rep.Page.Orientation=poLandscape Then
        OrientationRadioGroup.ItemIndex:=1
      Else
        OrientationRadioGroup.ItemIndex:=0;
      for PS:=Default to Custom do PaperSize.Items.Add(QRPaperName(PS));
      PaperSize.Sorted:=true;
      WidthSpinEdit.Text:=FloatToStr(Round(Rep.Page.Width*100)/100);
      LengthSpinEdit.Text:=FloatToStr(Round(Rep.Page.Length*100)/100);
      PaperSize.ItemIndex:=PaperSize.Items.IndexOf(QRPaperName(Rep.Page.PaperSize));
      TopMargin.Text:=FloatToStr(Round(Rep.Page.TopMargin*100)/100);
      LeftMargin.Text:=FloatToStr(Round(Rep.Page.LeftMargin*100)/100);
      RightMargin.Text:=FloatToStr(Round(Rep.Page.RightMargin*100)/100);
      BottomMargin.Text:=FloatToStr(Round(Rep.Page.BottomMargin*100)/100);
      ColumnSpace.Text:=FloatToStr(Round(Rep.Page.ColumnSpace*100)/100);
      Columns.Text:=IntToStr(Rep.Page.Columns);
      TopCheckbox.Checked:=Rep.Frame.DrawTop;
      LeftCheckbox.Checked:=Rep.Frame.DrawLeft;
      RightCheckbox.Checked:=Rep.Frame.DrawRight;
      BottomCheckbox.Checked:=Rep.Frame.DrawBottom;
      PenWidthSpinEdit.Text:=IntToStr(Rep.Frame.Width);
      PenStyleComboBox.Items.Clear;
      PenStyleComboBox.Items.Add(TQRepDesigner(QRD).RuntimeMessages.Values['PenStyleSolid']);
      PenStyleComboBox.Items.Add(TQRepDesigner(QRD).RuntimeMessages.Values['PenStyleDashed']);
      PenStyleComboBox.Items.Add(TQRepDesigner(QRD).RuntimeMessages.Values['PenStyleDot']);
      PenStyleComboBox.Items.Add(TQRepDesigner(QRD).RuntimeMessages.Values['PenStyleDashDot']);
      PenStyleComboBox.Items.Add(TQRepDesigner(QRD).RuntimeMessages.Values['PenStyleDashDotDot']);
      PenStyleComboBox.Items.Add(TQRepDesigner(QRD).RuntimeMessages.Values['PenStyleHiddenMark']);
      PenStyleComboBox.Items.Add(TQRepDesigner(QRD).RuntimeMessages.Values['PenStyleInsideFrame']);
      Case Rep.Frame.Style of
        psSolid:         PenStyleComboBox.ItemIndex:=0;
        psDash:          PenStyleComboBox.ItemIndex:=1;
        psDot:           PenStyleComboBox.ItemIndex:=2;
        psDashDot:       PenStyleComboBox.ItemIndex:=3;
        psDashDotDot:    PenStyleComboBox.ItemIndex:=4;
        psClear:         PenStyleComboBox.ItemIndex:=5;
        psInsideFrame:   PenStyleComboBox.ItemIndex:=6;
      end;
      FrameColor:=Rep.Frame.Color;
      Panel1.Visible:=PaperSize.Items[PaperSize.ItemIndex]=QRPaperName(Custom);
    end;
  Result:=ReportPropertyForm.ShowModal=mrOk;
  If Result Then
  With ReportPropertyForm Do
    begin
      TQRepDesigner(QRD).HasChanged:=True;
      Rep.Font.Assign(TF);
      TF.Free;
      //Rep.ReportTitle:=TitleEdit.Text;
      If OrientationRadioGroup.ItemIndex=1 Then
        Rep.Page.Orientation:=poLandscape
      Else
        Rep.Page.Orientation:=poPortrait;
      For PS:=Default To Custom Do
        If QRPaperName(PS)=PaperSize.Text Then
          begin
            Rep.Page.PaperSize:=PS;
            Break;
          end;
      If Rep.Page.PaperSize=Custom then
        begin
          try Rep.Page.Width:=StrToFloat(WidthSpinEdit.Text); except end;
          try Rep.Page.Length:=StrToFloat(LengthSpinEdit.Text); except end;
        end;

      try Rep.Page.TopMargin:=StrToFloat(TopMargin.Text); except end;
      try Rep.Page.LeftMargin:=StrToFloat(LeftMargin.Text); except end;
      try Rep.Page.RightMargin:=StrToFloat(RightMargin.Text); except end;
      try Rep.Page.BottomMargin:=StrToFloat(BottomMargin.Text); except end;
      try Rep.Page.ColumnSpace:=StrToFloat(ColumnSpace.Text); except end;
      try Rep.Page.Columns:=StrToInt(Columns.Text); except end;
      Rep.Frame.DrawTop:=TopCheckbox.Checked;
      Rep.Frame.DrawLeft:=LeftCheckbox.Checked;
      Rep.Frame.DrawRight:=RightCheckbox.Checked;
      Rep.Frame.DrawBottom:=BottomCheckbox.Checked;
      try Rep.Frame.Width:=StrToInt(PenWidthSpinEdit.Text); except end;
      Rep.Frame.Color:=FrameColor;
      Case PenStyleComboBox.ItemIndex of
        0: Rep.Frame.Style:=psSolid;
        1: Rep.Frame.Style:=psDash;
        2: Rep.Frame.Style:=psDot;
        3: Rep.Frame.Style:=psDashDot;
        4: Rep.Frame.Style:=psDashDotDot;
        5: Rep.Frame.Style:=psClear;
        6: Rep.Frame.Style:=psInsideFrame;
      end;
    end;
  ReportPropertyForm.Free;
end;

end.
