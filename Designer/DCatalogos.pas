unit DCatalogos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
{$ifdef DOS_CAPAS}
     DServerCatalogos
{$else}
       Catalogos_TLB
{$endif};

type
  TdmCatalogos = class(TDataModule)
    cdsNomParamLookUp: TZetaLookupDataSet;
    cdsConceptosLookUp: TZetaLookupDataSet;
    cdsConceptos: TZetaLookupDataSet;
    cdsNomParam: TZetaLookupDataSet;
    procedure cdsConceptosAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsNomParamAlAdquirirDatos(Sender: TObject);
  private
{$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
{$else}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
{$endif}
  public
    { Public declarations }
  end;

var
  dmCatalogos: TdmCatalogos;

implementation
uses DCliente;
{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}

procedure TdmCatalogos.cdsConceptosAlAdquirirDatos(Sender: TObject);
begin
     cdsConceptos.Data := ServerCatalogo.GetConceptos( dmCliente.Empresa );
end;

procedure TdmCatalogos.cdsNomParamLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsNomParam do
     begin
          if ( State <> dsInsert ) then // se condiciona porque la transferencia
          begin                         // de Data regresa el cds a dsBrowse
               Conectar;
               cdsNomParamLookUp.Data := Data;
          end;
     end;
end;

procedure TdmCatalogos.cdsNomParamAlAdquirirDatos(Sender: TObject);
begin
     cdsNomParam.Data := ServerCatalogo.GetNomParam( dmCliente.Empresa );
end;

end.
