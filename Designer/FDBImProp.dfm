object DBImageForm: TDBImageForm
  Left = 346
  Top = 370
  HelpContext = 90032
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Picture from database'
  ClientHeight = 299
  ClientWidth = 459
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Button3: TButton
    Left = 152
    Top = 256
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button4: TButton
    Left = 232
    Top = 256
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 441
    Height = 241
    TabOrder = 0
    object Label1: TLabel
      Tag = 1000
      Left = 16
      Top = 184
      Width = 65
      Height = 21
      AutoSize = False
      Caption = '&Alignment'
      FocusControl = AlignmentComboBox
    end
    object lbDataField: TLabel
      Tag = 1001
      Left = 16
      Top = 92
      Width = 97
      Height = 21
      AutoSize = False
      Caption = '&Data field'
    end
    object Label3: TLabel
      Tag = 1001
      Left = 16
      Top = 124
      Width = 97
      Height = 21
      AutoSize = False
      Caption = '&Band List'
    end
    object Label4: TLabel
      Tag = 1000
      Left = 16
      Top = 208
      Width = 65
      Height = 21
      AutoSize = False
      Caption = 'Rotation'
      FocusControl = CBRotacion
    end
    object StretchCheckbox: TCheckBox
      Tag = 1002
      Left = 16
      Top = 152
      Width = 233
      Height = 17
      Caption = '&Stretch picture automatically'
      TabOrder = 3
    end
    object AlignmentComboBox: TComboBox
      Tag = 1006
      Left = 80
      Top = 184
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 5
    end
    object CenterCheckBox: TCheckBox
      Tag = 1005
      Left = 256
      Top = 152
      Width = 169
      Height = 17
      Caption = '&Center picture'
      TabOrder = 4
      Visible = False
    end
    object DataFieldComboBox: TComboBox
      Tag = 1007
      Left = 112
      Top = 88
      Width = 313
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'FOTO'
        'FIRMA'
        'HUELLA')
    end
    object BandsComboBox: TComboBox
      Tag = 1007
      Left = 112
      Top = 120
      Width = 313
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
    end
    object CBRotacion: TComboBox
      Tag = 1006
      Left = 80
      Top = 208
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 6
      Items.Strings = (
        'No Rotar'
        '90 Grados'
        '180 Grados'
        '270 Grados')
    end
    object RGMuestraImagen: TRadioGroup
      Left = 208
      Top = 8
      Width = 215
      Height = 65
      Caption = 'Mostrar Imagen de:'
      Columns = 2
      Items.Strings = (
        'Empleados'
        'Maestros')
      TabOrder = 0
      OnClick = RGMuestraImagenClick
    end
  end
end
