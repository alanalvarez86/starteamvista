
{$I QRDESIGN.INC}
{$define MULTIPLES_ENTIDADES}
unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa principal de TressDisena.exe      ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
  WinProcs, WinTypes, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DB, Qrdesign, Menus, Buttons,
  DBCtrls, Grids, DBGrids, QRDOpt,
  LabProp, DBTxProp, ShapProp, MemoProp, SysDProp,
  FDBImProp,
  BandProp,
  ChldProp, SubProp, GrpProp, FExprProp, FReportProp, FramePro, QRDConst,
  FRichProp, DbRtfPro,
  qrdtools, qrdexprt, Printers,
  csProp, Quickrpt, Qrctrls, QRPrntr,
  Mask, QRDCtrls,
  ExtDlgs, ComCtrls,
  QRDLDR,
  fNuevoReporte,
  ZBaseShell,
  ZetaTipoEntidad,
  ZetaCommonLists, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
  thsdRuntimeLoader, thsdRuntimeEditor, qrdBaseCtrls, qrdQuickrep,
  dxStatusBar, dxRibbonStatusBar, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxRibbonCustomizationForm, cxStyles, cxClasses, dxSkinsForm, dxRibbon,
  qrdSupComps;

const PANEL_POS = 4;
type
  TTressShell = class(TBaseShell)
    {Dise�ador}
    QRepDesigner: TQRepDesigner;
    QuickReport: TDesignQuickReport;
    Titulo: TQRDesignBand;
    Detalle: TQRDesignBand;
    PiePagina: TQRDesignBand;
    Scrollbox1: TqrdScrollbox;

    {Dialogos}
    OpenDialog: TOpenDialog;
    PrinterSetupDialog: TPrinterSetupDialog;
    PropertyPanel: TPanel;
    ElementPanel: TPanel;
    ToolbarPanel: TPanel;
    Bevel2: TBevel;
    {Botones}
    FrameSpeedButton: TSpeedButton;
    TeechartSpeedButton: TSpeedButton;
    ChildSpeedbutton: TSpeedButton;
    RichtextSpeedButton: TSpeedButton;
    NormalSpeedButton: TSpeedButton;
    LeftButton: TSpeedButton;
    CenterHorizontalButton: TSpeedButton;
    RightButton: TSpeedButton;
    HorizontalEqualButton: TSpeedButton;
    ParentCenterHorizontalButton: TSpeedButton;
    TopButton: TSpeedButton;
    CenterVerticalButton: TSpeedButton;
    BottomButton: TSpeedButton;
    VerticalEqualButton: TSpeedButton;
    ParentCenterVerticalButton: TSpeedButton;
    BoldSpeedButton: TSpeedButton;
    UnderlineSpeedButton: TSpeedButton;
    ItalicSpeedButton: TSpeedButton;
    SendToBackButton: TSpeedButton;
    SaveSpeedButton: TSpeedButton;
    LoadSpeedButton: TSpeedButton;
    BandSpeedButton: TSpeedButton;
    LabelSpeedButton: TSpeedButton;
    MemoSpeedButton: TSpeedButton;
    DBImageSpeedButton: TSpeedButton;
    ImageSpeedButton: TSpeedButton;
    ShapeSpeedButton: TSpeedButton;
    SysdataSpeedButton: TSpeedButton;
    ExpressionSpeedButton: TSpeedButton;
    NewSpeedButton: TSpeedButton;
    CutSpeedButton: TSpeedButton;
    CopySpeedButton: TSpeedButton;
    PasteSpeedButton: TSpeedButton;
    BringToFrontButton: TSpeedButton;
    TextLeftAlignButton: TSpeedButton;
    TextCenterAlignButton: TSpeedButton;
    TextRightAlignButton: TSpeedButton;

    {Combo Box}
    FontComboBox: TComboBox;
    FontSizeComboBox: TComboBox;
    AllComboBox: TComboBox;

    {Menus}
    CutMI: TMenuItem;
    N9: TMenuItem;
    Help1: TMenuItem;
    UndoMI: TMenuItem;
    N10: TMenuItem;
    Rotatebands1: TMenuItem;
    Hidebands1: TMenuItem;
    N11: TMenuItem;
    Resetbands1: TMenuItem;
    N7: TMenuItem;
    Preview1: TMenuItem;
    ElementPanelPopupMenu: TPopupMenu;
    TopElementPanelMI: TMenuItem;
    LeftElementPanelMI: TMenuItem;
    N1: TMenuItem;
    VisibleElementPanelMI: TMenuItem;
    N8: TMenuItem;
    Zoomfaktor1: TMenuItem;
    Normalgre1: TMenuItem;
    Vergrern1: TMenuItem;
    Verkleinern1: TMenuItem;
    Childband1: TMenuItem;
    Subdetailband1: TMenuItem;
    Groupband1: TMenuItem;
    Toolbar1: TMenuItem;
    Fonttoolbar1: TMenuItem;
    Elementtoolbar1: TMenuItem;
    N6: TMenuItem;
    Options2: TMenuItem;
    MainMenu: TMainMenu;
    NewMI: TMenuItem;
    LoadMI: TMenuItem;
    SaveMI: TMenuItem;
    SaveAsMI: TMenuItem;
    MenuItem5: TMenuItem;
    PrintMI: TMenuItem;
    PrinterSetupMI: TMenuItem;
    MenuItem8: TMenuItem;
    ExitMI: TMenuItem;
    NewElementMI: TMenuItem;
    NewBandMI: TMenuItem;
    NewLabelMI: TMenuItem;
    NewDatafieldMI: TMenuItem;
    NewCalculatedfieldMI: TMenuItem;
    NewSystemdataMI: TMenuItem;
    NewShapeMI: TMenuItem;
    NewImageMI: TMenuItem;
    NewImagefromTableMI: TMenuItem;
    NewMemoMI: TMenuItem;
    N3: TMenuItem;
    CopyMI: TMenuItem;
    PasteMI: TMenuItem;
    N4: TMenuItem;
    AlignMI: TMenuItem;
    AlignLeftMI: TMenuItem;
    AlignRightMI: TMenuItem;
    AlignTopMI: TMenuItem;
    AlignBottomMI: TMenuItem;
    CenterMI: TMenuItem;
    AlignCenterHorizontalMI: TMenuItem;
    AlignCenterVerticalMI: TMenuItem;
    ParentCenterMI: TMenuItem;
    AlignCenterBandHorizontalMI: TMenuItem;
    AlignCenterBandVerticalMI: TMenuItem;
    EqualspaceMI: TMenuItem;
    AlignEqualHorizontalMI: TMenuItem;
    AlignEqualVerticalMI: TMenuItem;
    N2: TMenuItem;
    SendtobackMI: TMenuItem;
    Bringtofront1: TMenuItem;
    Options1: TMenuItem;
    MenuItem19: TMenuItem;
    DeleteMI: TMenuItem;
    PropertyPanelPopupMenu: TPopupMenu;
    PropMenuTop: TMenuItem;
    PropMenuBottom: TMenuItem;
    Visible1: TMenuItem;
    N5: TMenuItem;
    {Labels}
    TypeLabel: TLabel;
    AllEdit: TEdit;
    BBandaImagen: TSpeedButton;
    TextoEnriquecidoTabla: TMenuItem;
    TextoEnriquecido: TMenuItem;
    OpenPictureDialog: TOpenPictureDialog;
    SaveDialog: TSaveDialog;
    N12: TMenuItem;
    CambiarTablaPrincipal1: TMenuItem;
    BRichTextToMemo: TSpeedButton;
    oRichTemp: TRichEdit;
    RulerPanel: TqrdRulerPanel;

    procedure BandSpeedButtonClick(Sender: TObject);
    procedure LabelSpeedButtonClick(Sender: TObject);
    procedure ExpressionSpeedButtonClick(Sender: TObject);
    procedure SysDataSpeedButtonClick(Sender: TObject);
    procedure ShapeSpeedButtonClick(Sender: TObject);
    procedure OptionsSpeedButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ImageSpeedButtonClick(Sender: TObject);
    procedure DBImageSpeedButtonClick(Sender: TObject);
    procedure MemoSpeedButtonClick(Sender: TObject);
    procedure LoadSpeedButtonClick(Sender: TObject);
    procedure SaveSpeedButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure NewMIClick(Sender: TObject);
    procedure SaveAsMIClick(Sender: TObject);
    procedure PrinterSetupMIClick(Sender: TObject);
    procedure ExitMIClick(Sender: TObject);
    {procedure QRepDesignerEditProperties(QRD: TQRepDesigner;
      Component: TComponent; var EditResult: Boolean); } //am: CAMBIO TEMPORAL
    procedure QRepDesignerEditProperties(QRD: TQRepDesigner;
      var Component: TControl; var EditResult: Boolean);
    procedure QRepDesignerSelectionChanged(Sender: TObject);
    procedure PropMenuTopClick(Sender: TObject);
    procedure PropMenuBottomClick(Sender: TObject);
    procedure AllEditChange(Sender: TObject);
    procedure AllComboBoxChange(Sender: TObject);
    procedure Visible1Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure Toolbar1Click(Sender: TObject);
    procedure Fonttoolbar1Click(Sender: TObject);
    procedure Elementtoolbar1Click(Sender: TObject);
    procedure Options2Click(Sender: TObject);
    procedure QRepDesignerShowInfo(Component: TComponent;
      var Info: String);
    procedure MenuItem19Click(Sender: TObject);
    procedure TopElementPanelMIClick(Sender: TObject);
    procedure LeftElementPanelMIClick(Sender: TObject);
    procedure LabelSpeedButtonMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure SubdetailSpeedButtonClick(Sender: TObject);
    procedure ChildSpeedbuttonClick(Sender: TObject);
    procedure GroupSpeedButtonClick(Sender: TObject);
    procedure RichtextSpeedButtonClick(Sender: TObject);
    procedure Normalgre1Click(Sender: TObject);
    procedure Vergrern1Click(Sender: TObject);
    procedure Verkleinern1Click(Sender: TObject);
    procedure FrameSpeedButtonClick(Sender: TObject);
    procedure TeechartSpeedButtonClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Help1Click(Sender: TObject);
    //procedure Panel2DblClick(Sender: TObject);
    procedure UndoMIClick(Sender: TObject);
    procedure Rotatebands1Click(Sender: TObject);
    procedure Resetbands1Click(Sender: TObject);
    procedure Hidebands1Click(Sender: TObject);
    procedure QRepDesignerDeleteElement(QRD: TQRepDesigner;
      Component: TControl; var DoDelete: Boolean);
    procedure BBandaImagenClick(Sender: TObject);
    procedure TextoEnriquecidoClick(Sender: TObject);
    procedure TextoEnriquecidoTablaClick(Sender: TObject);
    procedure CambiarTablaPrincipal1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BRichTextToMemoClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);

  private
    fNuevoRep : TNuevoRep;
    fDirDefault : String;
    function AbrePlantilla: Boolean;
  public
    sFoto : string;
    ActiveBlobField: TBlobField;
    ActiveStream: TStream;
    ReportFileName: String;
    QRDHelpFile, AppHelpFile: String; //String[255];     OLD
    OpenFileHistory: TStringList;
    FechaDefault : TDate;
    procedure RefreshFontCombobox;
    procedure CargaReporte;
    procedure DesignerRun;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades);
    {$endif}

    procedure DesignerRunBatch;

  end;

  function BuscaDialogo( const eEntidad: TipoEntidad; const sFilter: String; sKey, sDescription: String ): Boolean;

var
  TressShell: TTressShell;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaClientDataSet,
     zReportTools,
     ZetaDialogo,
     ZetaAcercaDe_DevEx,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     //ZetaSQLBroker,
{$endif}
     FEditImageDir,
     FSeleccion,
     DGlobal,
     DCliente,
     DCatalogos,
     DDiccionario;

{$R *.DFM}


procedure TTressShell.BandSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  BandSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddBand;
end;

procedure TTressShell.LabelSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  LabelSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddLabel;
end;

procedure TTressShell.ExpressionSpeedButtonClick(Sender: TObject);
begin
{REVISADO}
  ExpressionSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddExpression;
end;

procedure TTressShell.SysDataSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  SysDataSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddSysData;
end;

procedure TTressShell.ShapeSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  ShapeSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddShape;
end;

procedure TTressShell.ImageSpeedButtonClick(Sender: TObject);
begin
     {REVISADO}
     ImageSpeedButton.Down:=TRUE;
     QRepDesigner.QRDesignState:=qrds_AddImage;
end;

procedure TTressShell.DBImageSpeedButtonClick(Sender: TObject);
 var
 DesignDBImage:TQRDesignDBImage; //(am):Se debe declarar como var porque ahora asi se recibe este parametro
 lEdit : Boolean;
begin
{REVISADO}
     QRepDesigner.QRDesignState:=qrds_User;
     DesignDBImage :=  TQRDesignDBImage.Create(QRepDesigner.Owner);
     QRepDesignerEditProperties( QRepDesigner, TControl(DesignDBImage) , lEdit );
     DBImageSpeedButton.Down:=FALSE;
end;

procedure TTressShell.MemoSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  MemoSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddMemo;
end;

procedure TTressShell.OptionsSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

     {FReportProp._EditReportOptions( QRepDesigner, QuickReport,
                                     StrDef( ExtractFileName(ReportFileName), Caption )); }//OLD
     //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
     FReportProp._EditReportOptions( QRepDesigner, QRepDesigner.QReport,
                                     StrDef( ExtractFileName(ReportFileName), Caption ));
end;
procedure TTressShell.FormDblClick(Sender: TObject);
begin
  inherited;
  QRepDesigner.UpdateSelections;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     OpenFileHistory.Free;
     FreeAndNil(dmCatalogos);
     FreeAndNil(dmDiccionario);

     inherited;

     FreeAndNil(Global);
     FreeAndNil(dmCliente);

{$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
{$endif}

end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}

{$ifdef TRESS}
     Application.Title := 'Dise�ador Tress';
{$endif}
{$ifdef SELECCION}
     Application.Title := 'Dise�ador Selecci�n';
{$endif}
{$ifdef VISITANTES}
     Application.Title := 'Dise�ador Visitantes';
{$endif}


     ActiveBlobField:=NIL;
     ActiveStream:=NIL;

     ReadQRDesignSetup(QRepDesigner,QRDHelpFile);
     AppHelpFile:=#0;
     OpenFileHistory:=TStringList.Create;
     TypeLabel.Caption:='';
     VertScrollBar.Visible:=FALSE;
     HorzScrollBar.Visible:=FALSE;
     VertScrollBar.Range:=0;
     HorzScrollBar.Range:=0;
     {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
                la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
                Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
                ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
                Asi que estaran sincronizados automaticamente.***}
     //RulerPanel.Zoom:=Quickreport.Zoom;
     //Quickreport.Units := Inches; //Old
     QRepDesigner.QReport.Units := Inches; //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
     {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
                la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
                Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
                ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
                Asi que estaran sincronizados automaticamente.***}
     //RulerPanel.Units:=Quickreport.Units;
     UndoMI.Visible:=QRepDesigner.UndoEnabled;
     N3.Visible:=QRepDesigner.UndoEnabled;
     //QRDesignLanguageIniFile:=''; //am: CAMBIO TEMPORAL

     dmCliente := TdmCliente.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     Global := TdmGlobal.Create;
     inherited;

end;

function TTressShell.AbrePlantilla : Boolean;
var oCursor: TCursor;
begin
     Result := FALSE;
     with QRepDesigner do
     begin
          if HasChanged AND
             NOT ZWarningConfirm( Caption, RuntimeMessages.Values['Report not saved'],
                                  0, mbCancel ) then Exit;
          if ActiveBlobField<>NIL then
          begin
               LoadReport_FromBlobField(ActiveBlobField);
               RefreshFontCombobox;
               {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
                la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
                Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
                ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
                Asi que estaran sincronizados automaticamente.***}
               {RulerPanel.Zoom:=Quickreport.Zoom;
               RulerPanel.Units:=Quickreport.Units;}
          end
          else if ActiveStream<>NIL then
          begin
               ActiveStream.Seek(0,soFromBeginning);
               LoadReport_FromStream(ActiveStream);
               RefreshFontCombobox;
                {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
                la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
                Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
                ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
                Asi que estaran sincronizados automaticamente.***}
               {RulerPanel.Zoom:=Quickreport.Zoom;
               RulerPanel.Units:=Quickreport.Units;}
          end
          else
          begin
               with OpenDialog do
               begin
                    FileName := VACIO;
                    InitialDir := fDirDefault;
                    HistoryList:=OpenFileHistory;
                    if Execute then
                    begin
                         {***(@am): Se pone al cursor como waitcursor pues existen reportes pesados que tardan en cargarse***}
                         oCursor := Screen.Cursor;
                         Screen.Cursor := crHourglass;
                         try
                             Result := TRUE;
                             LoadReport(FileName);
                             RefreshFontCombobox;
                             if OpenFileHistory.IndexOf(FileName)<>-1 then
                               OpenFileHistory.Delete(OpenFileHistory.IndexOf(FileName));
                             OpenFileHistory.Insert(0,FileName);
                             if OpenFileHistory.Count>20 then OpenFileHistory.Delete(OpenFileHistory.Count-1);
                             ReportFileName:=FileName;
                             Caption:=ReportFileName;
                            {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
                            la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
                            Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
                            ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
                            Asi que estaran sincronizados automaticamente.***}
                             {RulerPanel.Zoom:=Quickreport.Zoom;
                             RulerPanel.Units:=Quickreport.Units; }
                             InitialDir:=ExtractFilePath(FileName);
                             FileName:=ExtractFileName(FileName);
                         finally
                              Screen.Cursor := oCursor;
                         end;
                    end
                    else Result := FALSE;
               end;
          end;
          CargaReporte;
     end;
end;

procedure TTressShell.LoadSpeedButtonClick(Sender: TObject);
begin
     AbrePlantilla;
end;

procedure TTressShell.CargaReporte;
 var i : integer;
     C : TComponent;
begin

    if QRepDesigner.QReport <> NIL then
    begin
         //QRepDesigner.QReport.QRDesigner := QRepDesigner; //TEMPORAL
         for i := 0 to ComponentCount - 1 do
         begin
              C := Components[ i ];
              if C is TQRDesignDBImage then
                 with TQRDesignDBImage( C ) do
                 begin
                      DataSet := QRepDesigner.QReport.Dataset;
                      //QrDesigner := QRepDesigner; //am: CAMBIO TEMPORAL
                 end;
         end;
    end;
end;

procedure TTressShell.SaveSpeedButtonClick(Sender: TObject);
begin
     {REVISADO}
     if ActiveBlobField<>NIL then
     begin
          QRepDesigner.SaveReport_ToBlobField(ActiveBlobField);
     end
     else if ActiveStream<>NIL then
     begin
          QRepDesigner.SaveReport_ToStream(ActiveStream);
     end
     else
     begin
          if (ReportFileName='') OR (ReportFileName='<PLANTILLA NUEVA>') then
          begin
               SaveDialog.FileName := '';
               if SaveDialog.Execute then
               begin
                    ReportFileName:=SaveDialog.FileName;
                    QRepDesigner.SaveReport(ReportFileName);
                    Caption:=ReportFileName;
               end;
           end
           else
               QRepDesigner.SaveReport(ReportFileName);
       end;
end;

procedure TTressShell.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  C: Integer;
begin
  if QRepDesigner.HasChanged then
    begin
      C:=MessageDlg(Format(QRepDesigner.RuntimeMessages.Values['Save report'],[ReportFileName]),
                    mtconfirmation,[mbyes,mbno,mbcancel],0);
      if C=mrCancel then
        begin
          CanClose:=FALSE;
          Exit;
        end;
      if C<>mrNo then
        begin
          if ActiveBlobField<>NIL then
            begin
              QRepDesigner.SaveReport_ToBlobField(ActiveBlobField);
            end
          else
          if ActiveStream<>NIL then
            begin
              QRepDesigner.SaveReport_ToStream(ActiveStream);
            end
          else
            begin
              if (ReportFileName='') OR (ReportFileName='<PLANTILLA NUEVA>') then
                begin
                  SaveDialog.FileName := '';
                  if SaveDialog.Execute then
                    begin
                      ReportFileName:=SaveDialog.FileName;
                      QRepDesigner.SaveReport(ReportFileName);
                    end
                  else
                    CanClose:=FALSE;
                end
              else
                QRepDesigner.SaveReport(ReportFileName);
            end;
        end;
    end;
end;

procedure TTressShell.NewMIClick(Sender: TObject);
begin
{REVISADO}
     if QRepDesigner.HasChanged and
        (MessageDlg(QRepDesigner.RuntimeMessages.Values['Report not saved'],
        mtconfirmation,[mbok,mbcancel],0)<>mrOk) then Exit;

     if fNuevoRep = NIL then fNuevoRep := TNuevoRep.Create( self );

     fNuevoRep.ShowModal;
     if fNuevoRep.ModalResult=mrOk then
     begin
              ReportFileName:= fNuevoRep.NombreReporte;
              SaveDialog.FileName:=ReportFileName;
              Caption := ReportFileName;
              if (ActiveBlobField<>NIL) and (ActiveStream<>NIL) then Caption:='';

              //with QuickReport do  //old
              with QRepDesigner.QReport do //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
              begin
                   while BandList.Count > 0 do
                   begin
                        QRepDesigner.SelectElement( FALSE, BandList[ 0 ] );
                        QRepDesigner.DeleteSelectedElements;
                   end;
              end;
              //with QuickReport do  //old
              with QRepDesigner.QReport do //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
              begin
                   ReportTitle := IntToStr( Ord( fNuevoRep.TablaPrincipal ) );
                   Page.BottomMargin := 0.5;
                   Page.TopMargin := 0.5;
                   Page.LeftMargin := 0.5;
                   Page.RightMargin := 0.5;
                   Page.PaperSize := Letter;
                   Page.Orientation := poPortrait;
                   Page.Columns := 1;
                   Page.ColumnSpace := 0;
              end;
              with TQRDesignBand.Create( self ) do
              begin
                   Name := 'Titulo';
                   BandType := rbTitle;
                   Parent := QuickReport;
                   Height := 40;
              end;
              with TQRDesignBand.Create( self ) do
              begin
                   Name := 'Detalle';
                   BandType := rbDetail;
                   Parent := QuickReport;
                   Height := 40;
              end;
              with TQRDesignBand.Create( self ) do
              begin
                   Name := 'PiePagina';
                   BandType := rbPageFooter;
                   Parent := QuickReport;
                   Height := 40;
              end;
              {with dmDiccionario do
                   Entidad := fNuevoRep.TablaPrincipal;}
     end;
end;

procedure TTressShell.SaveAsMIClick(Sender: TObject);
begin
     {REVISADO}
     SaveDialog.FileName := VACIO;
     if SaveDialog.Execute then
     begin
          ReportFileName:=SaveDialog.FileName;
          QRepDesigner.SaveReport(ReportFileName);
          Caption:=ReportFileName;
     end;
end;

procedure TTressShell.RefreshFontCombobox;
var
  X: Integer;
begin
  FontCombobox.Items.Clear;
  FontComboBox.Items:=Screen.Fonts;
  For X:=1 to Printer.Fonts.Count Do
    if FontComboBox.Items.IndexOf(Printer.Fonts[X-1])=-1 then
      FontComboBox.Items.Add(Printer.Fonts[X-1]);
end;

procedure TTressShell.PrinterSetupMIClick(Sender: TObject);
var
  Device, Driver, Port: Array[0..79] of Char;
  DeviceMode: THandle;
  I: Integer;
  TPS: TPrinterSettings;
begin
{REVISADO}

  PrinterSetupDialog.Execute;
  TPS:=TPrinterSettings.Create;
  TPS.Printer:=Printer;
  Printer.GetPrinter(Device, Driver, Port, DeviceMode);
  I:=Printer.Printers.IndexOf(StrPas(Device));

  //if (I<>-1) AND (I<>Quickreport.PrinterSettings.PrinterIndex) then  //old
  if (I<>-1) AND (I<>QRepDesigner.QReport.PrinterSettings.PrinterIndex) then //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
    begin
      //Quickreport.PrinterSettings.PrinterIndex:=I;  //old
      QRepDesigner.QReport.PrinterSettings.PrinterIndex:=I; //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
      RefreshFontCombobox;
    end;
  {Quickreport.Page.Orientation:=TPS.Orientation;
  Quickreport.PrinterSettings.Copies:=TPS.Copies;
  Quickreport.PrinterSettings.Duplex:=TPS.Duplex;
  Quickreport.Page.PaperSize:=TPS.PaperSize; }//OLD
  //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
  with  QRepDesigner.QReport do
  begin
      Page.Orientation:=TPS.Orientation;
      PrinterSettings.Copies:=TPS.Copies;
      PrinterSettings.Duplex:=TPS.Duplex;
      Page.PaperSize:=TPS.PaperSize;
  end;

  if TPS.PaperSize=Custom then
    begin
      {Quickreport.Page.Length:=TPS.PaperLength;
      Quickreport.Page.Width:=TPS.PaperWidth; }//OLD
      //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
      QRepDesigner.QReport.Page.Length:=TPS.PaperLength;
      QRepDesigner.QReport.Page.Width:=TPS.PaperWidth;
    end;
  QRepDesigner.AssignComponentEvents;
end;

procedure TTressShell.ExitMIClick(Sender: TObject);
begin
     {REVISADO}
     Close;
end;

procedure TTressShell.QRepDesignerEditProperties(QRD: TQRepDesigner;
  var Component: TControl; var EditResult: Boolean);
var
QRC: TQRCustomBand;
lUsaImagenDefault:Boolean;
begin
     lUsaImagenDefault := False;
     if (Component is TDesignQuickReport) then
        EditResult:=EditReportOptions(QRD, TQuickRep(Component))
     else if (Component is TQRDesignDBText) then
          EditResult:=EditDBTextProps(QRD, Self, TQRDesignDBText(Component))
     else if (Component is TQRDesignShape)then
          EditResult:=EditShapeProps(QRD, Self, TQRDesignShape(Component))
     else if (Component is TQRDesignExpr) then
          EditResult:=EditExpressionProps(QRD, Self, TQRDesignExpr(Component))
     else if (Component is TQRDesignChildband)then
          EditResult:=EditChildProps     (QRD, Self, TQRDesignChildBand(Component))
     else if (Component is TQRDesignGroup) then
          EditResult:=EditGroupProps     (QRD, Self, TQRDesignGroup(Component))
     else if (Component is TQRDesignSubdetail) then
          EditResult:=EditSubdetailProps (QRD, Self, TQRDesignSubdetail(Component))
     else if (Component is TQRDesignRichtext) then
          EditResult:=EditRichtextProps  (QRD, Self, TQRDesignRichtext(Component))
     else if (Component is TQRDesignDBRichtext) then
          EditResult:=EditDBRichtextProps(QRD, Self, TQRDesignDBRichtext(Component))
     else if (Component is TQRDesignSysData)    then
          EditResult:=EditSysDataProps   (QRD, Self, TQRDesignSysData(Component))
     else if (Component is TQRDesignImage) then
     begin
          if QRepDesigner.QRDesignState = qrds_AddImage then
            lUsaImagenDefault := True;
             //TQRDesignImage(Component).FileName := VACIO;
          EditResult:=EditImageProps(TQRDesignImage(Component), FDirDefault+'LOGO.BMP', lUsaImagenDefault)
     end
     else if (Component is TQRDesignDBImage) then
          EditResult:=EditDBImageProps (QRD, Self, TQRDesignDBImage(Component))
     else if (Component is TQRDesignLabel)then
          EditResult:=EditLabelProps(QRD, Self, TQRDesignLabel(Component))
     else if (Component is TQRDesignMemo) then
          EditResult:=EditMemoProps(QRD, Self, TQRDesignMemo(Component))
     else if (Component is TQRDesignBand) then
     begin
          {***(@am): Como se cambio a var el ultimo parametro del metodo EditBandProps
                     tenemos que declarar uno y hacer la asignacion, no podemos mandar el
                     Component con un Cast nadamas.***}
          QRC := (Component as TQRDesignBand);
          EditResult:=EditBandProps(QRD, Self, QRC); //NEW
          //EditResult:=EditBandProps(QRD, Self, TQRDesignBand(Component)) //OLD
     end
     else if (Component is TQRDesignTeechart) then
          EditResult:=TRUE;//EditTeechartProps (QRD, Self, TQRDesignTeechart(Component))
end;

procedure TTressShell.QRepDesignerSelectionChanged(Sender: TObject);
var
  C: TControl;
  X: Integer;
  A: TAlignment;
  lBandera: Boolean;
begin
     lBandera := False;
     AlignMI.Enabled:=AlignLeftMI.Enabled;
     CenterMI.Enabled:=AlignCenterHorizontalMI.Enabled;
     ParentCenterMI.Enabled:=AlignCenterBandHorizontalMI.Enabled;
     EqualSpaceMI.Enabled:=AlignEqualHorizontalMI.Enabled;

     TextCenterAlignButton.Down:=FALSE;
     TextLeftAlignButton.Down:=FALSE;
     TextRightAlignButton.Down:=FALSE;

     BBandaImagen.Enabled := FALSE;
     BRichTextToMemo.Enabled := FALSE;

     if sender <> nil then
     begin
         C:=TControl(sender);
         if PropertyExists(C,'Alignment') then
           begin
             A:=TAlignment(GetIntegerProperty(C,'Alignment'));
             if A=taCenter then TextCenterAlignButton.Down:=TRUE
             else
             if A=taRightJustify then TextRightAlignButton.Down:=TRUE
             else
               TextLeftAlignButton.Down:=TRUE;
           end;

         if C is TQRDesignLabel then
           begin
             TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Label'];
             AllEdit.Visible:=TRUE;
             AllComboBox.Visible:=FALSE;
             {*** US 14491: Net Payroll- En TressDisena 2016 al seleccionar un campo y utilizando la tecla �Shift� querer seleccionar otro campo se duplica la informaci�n del siguiente campo y se pierde la informaci�n del campo original. ***}
             if ( not lBandera ) and ( GetKeyState(VK_SHIFT) < 0 ) then
               lBandera := True;
             if ( not lBandera ) and ( GetKeyState(VK_CONTROL) < 0 ) then
               lBandera := True;
             if not lBandera then
             begin
                  AllEdit.Visible:=True;
                  AllEdit.Text:=TQRLabel(C).Caption;
             end
             else
                 AllEdit.Visible:=FALSE;
             AllEdit.Enabled:=NOT(GetBooleanProperty(C,'BlockChange') OR
                                  GetBooleanProperty(C,'BlockEdit'));
           end
         else
         if C is TQRExpr then
           begin
             TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Expression'];
             AllEdit.Visible:=TRUE;
             AllComboBox.Visible:=FALSE;
             {*** US 14491: Net Payroll- En TressDisena 2016 al seleccionar un campo y utilizando la tecla �Shift� querer seleccionar otro campo se duplica la informaci�n del siguiente campo y se pierde la informaci�n del campo original. ***}
             if ( not lBandera ) and ( GetKeyState(VK_SHIFT) < 0 ) then
               lBandera := True;
             if ( not lBandera ) and ( GetKeyState(VK_CONTROL) < 0 ) then
               lBandera := True;
             if not lBandera then
             begin
                  AllEdit.Visible:=True;
                  AllEdit.Text:=TQRLabel(C).Caption;
             end
             else
                 AllEdit.Visible:=FALSE;

             AllEdit.Enabled:=NOT(GetBooleanProperty(C,'BlockChange') OR
                                  GetBooleanProperty(C,'BlockEdit'));
           end
         else
           begin
             AllComboBox.Items.Clear;
             AllComboBox.Visible:=TRUE;
             AllEdit.Visible:=FALSE;
             With QRepDesigner Do
               begin
                 //---- Band ----
                 if C is TQrCustomBand then
                 begin
                      BBandaImagen.Enabled := TRUE;
                 end;
                 if C is TQRDesignRichtext then
                 begin
                      BRichTextToMemo.Enabled := TRUE;
                 end;
                 if C is TQRChildBand then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Childband'];
                     AllCombobox.Visible:=FALSE;
                   end
                 else
                 if C is TQRSubDetail then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Subdetailband'];
                     AllCombobox.Visible:=FALSE;
                   end
                 else
                 if C is TQRGroup then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Groupband'];
                     AllCombobox.Visible:=FALSE;
                   end
                 else
                 if C is TQRDesignBand then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Band'];;
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[8]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[2]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[6]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[5]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[1]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[3]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[4]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[BandTypes[0]]);
                     Case TQRDesignBand(C).BandType of
                       rbColumnHeader: AllComboBox.ItemIndex:=0;
                       rbDetail:       AllComboBox.ItemIndex:=1;
                       rbGroupFooter:  AllComboBox.ItemIndex:=2;
                       rbGroupHeader:  AllComboBox.ItemIndex:=3;
                       rbPageHeader:   AllComboBox.ItemIndex:=4;
                       rbPageFooter:   AllComboBox.ItemIndex:=5;
                       rbSummary:      AllComboBox.ItemIndex:=6;
                       rbTitle:        AllComboBox.ItemIndex:=7;
                     end;
                     AllComboBox.Enabled:=NOT(GetBooleanProperty(C,'BlockChange') OR
                                              GetBooleanProperty(C,'BlockEdit'));
                   end
                 else
                 //---- DBText ----
                 if C is TQRDesignDBText then
                   begin
                     if TQRDesignDBText(C).DataSet<>NIL then
                     try
                       TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Datafield'];
                       For X:=1 to TQRDesignDBText(C).DataSet.FieldCount Do
                         begin
                           if TQRDesignDBText(C).DataSet.Fields[X-1].Visible then
                             begin
                               AllComboBox.Items.Add(TQRDesignDBText(C).DataSet.Fields[X-1].DisplayLabel);
                               if TQRDesignDBText(C).DataSet.Fields[X-1].FieldName=TQRDesignDBText(C).DataField then
                                 AllComboBox.ItemIndex:=AllComboBox.Items.Count-1;
                             end;
                         end;
                     except
                     end;
                     if AllComboBox.ItemIndex<0 then AllComboBox.ItemIndex:=0;
                     AllComboBox.Enabled:=NOT(GetBooleanProperty(C,'BlockChange') OR
                                              GetBooleanProperty(C,'BlockEdit'));
                   end
                 else
                 //---- Memo ----
                 if C is TQRDesignMemo then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Memo'];
                     AllComboBox.Visible:=FALSE;
                   end
                 else
                 //---- DBImage ----
                 if C is TQRDesignDBImage then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Image datafield'];
                     with AllComboBox,Items do
                     begin
                          Clear;
                          Add('FOTO');
                          Add('FIRMA');
                          Add('HUELLA');
                          Text := TQRDesignDBImage(C).DataField;

                          if ItemIndex < 0 then ItemIndex:=0;
                          Enabled:=NOT( GetBooleanProperty(C,'BlockChange' ) OR
                                        GetBooleanProperty(C,'BlockEdit' ) );
                     end;
                   end
                 else
                 //---- Image ----
                 if C is TQRDesignImage then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Image'];
                     AllComboBox.Visible:=FALSE;
                   end
                 else
                 //---- Shape ----
                 if C is TQRDesignShape then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Shape'];
                     AllComboBox.Items.Add(RuntimeMessages.Values['ShapeCircle']);
                     AllComboBox.Items.Add(RuntimeMessages.Values['ShapeHorizontalLine']);
                     AllComboBox.Items.Add(RuntimeMessages.Values['ShapeRectangle']);
                     AllComboBox.Items.Add(RuntimeMessages.Values['ShapeLeftRightLines']);
                     AllComboBox.Items.Add(RuntimeMessages.Values['ShapeTopBottomLines']);
                     AllComboBox.Items.Add(RuntimeMessages.Values['ShapeVerticalLine']);
                     Case TQRDesignShape(C).Shape of
                       qrsCircle: AllComboBox.ItemIndex:=0;
                       qrsHorLine: AllComboBox.ItemIndex:=1;
                       qrsRectangle: AllComboBox.ItemIndex:=2;
                       qrsRightAndLeft: AllComboBox.ItemIndex:=3;
                       qrsTopAndBottom: AllComboBox.ItemIndex:=4;
                       qrsVertLine: AllComboBox.ItemIndex:=5;
                     end;
                     AllComboBox.Enabled:=NOT(GetBooleanProperty(C,'BlockChange') OR
                                              GetBooleanProperty(C,'BlockEdit'));
                   end
                 else
                 //---- Sysdata ----
                 if C is TQRDesignSysdata then
                   begin
                     TypeLabel.Caption:=QRepDesigner.RuntimeMessages.Values['Systemfield'];
                     AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[1]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[0]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[2]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[5]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[6]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[3]]);
                     AllComboBox.Items.Add(RuntimeMessages.Values[SysDataTypes[4]]);
                     Case TQRDesignSysdata(C).Data of
                       qrsDate: AllCombobox.ItemIndex:=0;
                       qrsTime: AllCombobox.ItemIndex:=1;
                       qrsDateTime: AllCombobox.ItemIndex:=2;
                       qrsDetailCount: AllCombobox.ItemIndex:=3;
                       qrsDetailNo: AllCombobox.ItemIndex:=4;
                       qrsPageNumber: AllCombobox.ItemIndex:=5;
                       qrsReportTitle: AllCombobox.ItemIndex:=6;
                     end;
                     AllComboBox.Enabled:=NOT(GetBooleanProperty(C,'BlockChange') OR
                                              GetBooleanProperty(C,'BlockEdit'));
                   end
                 else
                   AllComboBox.Visible:=FALSE;
               end // with QRDesigner
           end // NOT C is TQRDesignLabel
       end // sender <> nil
       else
       begin
         AllEdit.Visible:=FALSE;
         AllComboBox.Visible:=FALSE;
         //if NumberOfSelections=0 then TypeLabel.Caption:='Report' else TypeLabel.Caption:=''; //OLD
         TypeLabel.Caption:='Report'; //Nuevo
       end;
end;

procedure TTressShell.PropMenuTopClick(Sender: TObject);
begin
  PropMenuTop.Checked:=TRUE;
  PropMenuBottom.Checked:=FALSE;
  PropertyPanel.Align:=alTop;
end;

procedure TTressShell.PropMenuBottomClick(Sender: TObject);
begin
  PropMenuTop.Checked:=FALSE;
  PropMenuBottom.Checked:=TRUE;
  PropertyPanel.Align:=alBottom;
end;

procedure TTressShell.AllEditChange(Sender: TObject);
var
  TC: TControl;
begin
  TC:=TControl(QRepDesigner.GetSelectedElement);
  if TC=NIL then Exit;
  if TC is TQRExpr then
    begin
      if GetStringProperty(TC,'Expression')<>AllEdit.Text then
       begin
         SetStringProperty(TC,'Expression',AllEdit.Text);
         QRepDesigner.RedrawSelection;
       end;
    end
  else
    begin
      if GetStringProperty(TC,'Caption')<>AllEdit.Text then
       begin
         SetStringProperty(TC,'Caption',AllEdit.Text);
         QRepDesigner.RedrawSelection;
       end;
    end
end;


procedure TTressShell.AllComboBoxChange(Sender: TObject);
var
  C: TControl;
  X: Integer;
begin
{REVISADO}


  C:=TControl(QRepDesigner.GetSelectedElement);
  if C=NIL then Exit;
  if C is TQRDesignDBText then
    begin
      For X:=1 to TQRDesignDBText(C).DataSet.FieldCount Do
        begin
          if AllComboBox.Items[AllComboBox.ItemIndex]
             =TQRDesignDBText(C).DataSet.Fields[X-1].DisplayLabel then
            begin
              TQRDesignDBText(C).DataField:=TQRDesignDBText(C).DataSet.Fields[X-1].FieldName;
              TQRDesignDBText(C).Caption:=TQRDesignDBText(C).DataSet.Fields[X-1].DisplayLabel;
            end;
        end;
    end
  else
  if C is TQRDesignBand then
    begin
      Case AllComboBox.ItemIndex of
        0: TQRDesignband(C).BandType:=rbColumnHeader;
        1: TQRDesignband(C).BandType:=rbDetail;
        2: TQRDesignband(C).BandType:=rbGroupFooter;
        3: TQRDesignband(C).BandType:=rbGroupHeader;
        4: TQRDesignband(C).BandType:=rbPageHeader;
        5: TQRDesignband(C).BandType:=rbPageFooter;
        6: TQRDesignband(C).BandType:=rbSummary;
        7: TQRDesignband(C).BandType:=rbTitle;
      end;
    end
  else
  if C is TQRDesignDBImage then
    begin
      For X:=1 to TQRDesignDBImage(C).DataSet.FieldCount Do
        begin
          if AllComboBox.Items[AllComboBox.ItemIndex]
             =TQRDesignDBImage(C).DataSet.Fields[X-1].DisplayLabel then
            begin
              TQRDesignDBImage(C).DataField:=TQRDesignDBImage(C).DataSet.Fields[X-1].FieldName;
            end;
        end;
    end
  else
  if C is TQRDesignShape then
    begin
      Case AllComboBox.ItemIndex of
        0: TQRDesignShape(C).Shape:=qrsCircle;
        1: TQRDesignShape(C).Shape:=qrsHorLine;
        2: TQRDesignShape(C).Shape:=qrsRectangle;
        3: TQRDesignShape(C).Shape:=qrsRightAndLeft;
        4: TQRDesignShape(C).Shape:=qrsTopAndBottom;
        5: TQRDesignShape(C).Shape:=qrsVertLine;
      end;
    end
  else
  if C is TQRDesignSysdata then
    begin
      Case AllComboBox.ItemIndex of
        0: TQRDesignSysdata(C).Data:=qrsDate;
        1: TQRDesignSysdata(C).Data:=qrsTime;
        2: TQRDesignSysdata(C).Data:=qrsDateTime;
        3: TQRDesignSysdata(C).Data:=qrsDetailCount;
        4: TQRDesignSysdata(C).Data:=qrsDetailNo;
        5: TQRDesignSysdata(C).Data:=qrsPageNumber;
        6: TQRDesignSysdata(C).Data:=qrsReportTitle;
      end;
    end;
  QRepDesigner.SelectElement(True, C);
end;

procedure TTressShell.Visible1Click(Sender: TObject);
begin
  PropertyPanel.Visible:=FALSE;
  FontToolbar1.Checked:=FALSE;
end;

procedure TTressShell.SpeedButton7Click(Sender: TObject);
begin
{REVISADO}

  Close;
end;

procedure TTressShell.Toolbar1Click(Sender: TObject);
begin
{REVISADO}

  Toolbar1.Checked:=NOT Toolbar1.Checked;
  ToolbarPanel.Visible:=Toolbar1.Checked;
  ToolbarPanel.Top:=-1;
end;

procedure TTressShell.Fonttoolbar1Click(Sender: TObject);
begin
{REVISADO}

  FontToolbar1.Checked:=NOT FontToolbar1.Checked;
  PropertyPanel.Visible:=FontToolbar1.Checked;
  PropertyPanel.Top:=-1;
  if ToolbarPanel.Visible then ToolbarPanel.Top:=-1;
end;

procedure TTressShell.Elementtoolbar1Click(Sender: TObject);
begin
{REVISADO}

  ElementToolbar1.Checked:=NOT ElementToolbar1.Checked;
  ElementPanel.Visible:=ElementToolbar1.Checked;
  if ToolbarPanel.Visible then ToolbarPanel.Top:=-1;
end;

procedure TTressShell.Options2Click(Sender: TObject);
begin
{REVISADO}

  //EditViewOptions(Quickreport, QRepDesigner); //OLD
  EditViewOptions(QRepDesigner.QReport, QRepDesigner); //(@am): Se necesita cambiar porque ahora todo debe hacerse desde el QRepDesigner

  {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
  la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
  Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
  ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
  Asi que estaran sincronizados automaticamente.***}
  {RulerPanel.Units:=Quickreport.Units;}
end;

procedure TTressShell.QRepDesignerShowInfo(Component: TComponent;
  var Info: String);
var
  U: String[15];
begin
     //REVISADO
     Info:='';
     if Component=NIL then
     begin
          StatusBarMsg( VACIO, PANEL_POS );
     end
     else
     begin
          //Case Quickreport.Units of  //old
          case QRepDesigner.QReport.Units of //(@am): Se necesita cambiar porque ahora todo debe hacerse desde el QRepDesigner
               MM: U:=' mm';
               Inches: U:=' Inches';
               Characters: U:=' Chars';
               else U:='';
          end;
          if Component is TQRPrintable then
          begin
               StatusBarMsg( IntToStr(TControl(Component).Left) +' ('+
                             FloatToStrF(TQRPrintable(Component).Size.Left,ffFixed,18,2)+U+') / '+
                             IntToStr(TControl(Component).Top)+
                             ' ('+FloatToStrF(TQRPrintable(Component).Size.Top,ffFixed,18,2)+U+')',
                             PANEL_POS );
          end
          else
          begin
               StatusBarMsg( VACIO, PANEL_POS );
          end;
     end;
end;

procedure TTressShell.MenuItem19Click(Sender: TObject);
begin
     inherited;
     try
        ZAcercaDe_DevEx := TZAcercaDe_DevEx.Create( Self );
        ZAcercaDe_DevEx.ShowModal;
     finally
            ZAcercaDe_DevEx.Free;
     end;
end;

procedure TTressShell.TopElementPanelMIClick(Sender: TObject);
var
  X,L: Integer;
begin
  if ElementPanel.Align=alTop then Exit;
  ElementPanel.Align:=alTop;
  if PropertyPanel.Visible AND (PropertyPanel.Align=alTop) then ElementPanel.Top:=PropertyPanel.Top-1;
  For X:=0 to ElementPanel.ControlCount-1 Do
    begin
      L:=ElementPanel.Controls[X].Left;
      ElementPanel.Controls[X].Left:=ElementPanel.Controls[X].Top;
      ElementPanel.Controls[X].Top:=L;
    end;
  LeftElementPanelMI.Checked:=FALSE;
  TopElementPanelMI.Checked:=TRUE;
end;

procedure TTressShell.LeftElementPanelMIClick(Sender: TObject);
var
  X,L: Integer;
begin
  if ElementPanel.Align=alLeft then Exit;
  ElementPanel.Align:=alLeft;
  For X:=0 to ElementPanel.ControlCount-1 Do
    begin
      L:=ElementPanel.Controls[X].Left;
      ElementPanel.Controls[X].Left:=ElementPanel.Controls[X].Top;
      ElementPanel.Controls[X].Top:=L;
    end;
  LeftElementPanelMI.Checked:=TRUE;
  TopElementPanelMI.Checked:=FALSE;
end;

procedure TTressShell.LabelSpeedButtonMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
{REVISADO}
  QRepDesigner.AddMultipleElements:=ssShift in Shift;
end;

procedure TTressShell.SubdetailSpeedButtonClick(Sender: TObject);
begin
     {REVISADO}
     QRepDesigner.QRDesignState:=qrds_AddSubDetail;
end;

procedure TTressShell.ChildSpeedbuttonClick(Sender: TObject);
begin
{REVISADO}

  ChildSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddChildband;
end;

procedure TTressShell.GroupSpeedButtonClick(Sender: TObject);
begin
     {REVISADO}
     QRepDesigner.QRDesignState:=qrds_AddGroup;
end;

procedure TTressShell.RichtextSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  RichtextSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddRichtext;
end;

procedure TTressShell.Normalgre1Click(Sender: TObject);
begin
{REVISADO}

  {QuickReport.Zoom:=100;}//OLD
  //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
  QRepDesigner.QReport.Zoom:=100;
  QRepDesigner.RedrawSelection;
  {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
  la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
  Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
  ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
  Asi que estaran sincronizados automaticamente.***}
  //RulerPanel.Zoom:=Quickreport.Zoom;
end;

procedure TTressShell.Vergrern1Click(Sender: TObject);
begin
{REVISADO}

  {if QuickReport.Zoom*2>300 then
    QuickReport.Zoom:=300
  else
    QuickReport.Zoom:=QuickReport.Zoom*2;} //OLD
  //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
  if QRepDesigner.QReport.Zoom*2>300 then
      QRepDesigner.QReport.Zoom := 300
  else
      QRepDesigner.QReport.Zoom:=QRepDesigner.QReport.Zoom*2;
  QRepDesigner.RedrawSelection;
  {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
  la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
  Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
  ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
  Asi que estaran sincronizados automaticamente.***}
  //RulerPanel.Zoom:=Quickreport.Zoom;
end;

procedure TTressShell.Verkleinern1Click(Sender: TObject);
begin
{REVISADO}

  {if QuickReport.Zoom/2>10 then QuickReport.Zoom:=Round(QuickReport.Zoom/2);} //OLD
  if  QRepDesigner.QReport.Zoom/2>10 then  QRepDesigner.QReport.Zoom:=Round( QRepDesigner.QReport.Zoom/2);  //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
  QRepDesigner.RedrawSelection;
  {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
  la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
  Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
  ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
  Asi que estaran sincronizados automaticamente.***}
  //RulerPanel.Zoom:=Quickreport.Zoom;
end;

procedure TTressShell.FrameSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  EditFrameProperty(QRepDesigner);
end;

procedure TTressShell.TeechartSpeedButtonClick(Sender: TObject);
begin
{REVISADO}

  TeechartSpeedButton.Down:=TRUE;
  QRepDesigner.QRDesignState:=qrds_AddTeechart;
end;



procedure TTressShell.FormShow(Sender: TObject);
begin
     {REVISADO}
     SaveAsMI.Visible:=(ActiveBlobField=NIL) AND (ActiveStream=NIL);
     AppHelpFile:=Application.HelpFile;
     if QRDHelpFile<>'' then
        Application.HelpFile:=QRDHelpFile
     else if FileExists(ExtractFilePath(Application.ExeName)+'QRDUSER.HLP') then
          Application.HelpFile:=ExtractFilePath(Application.ExeName)+'QRDUSER.HLP';
     {***@am: La propiedad Enable del objeto QRepDesigner, no esta publicada. Pero su valor default es false
              para que el evento QRepDesignerSelectionChanged funcione es necesario que este en TRUE.***}
     QRepDesigner.Enabled := True;
end;



procedure TTressShell.Help1Click(Sender: TObject);
begin
{REVISADO}

  Application.HelpContext(90001);
end;

{***(@am):Se comenta porque no se usa en ningun lado este codigo***}
{procedure TTressShell.Panel2DblClick(Sender: TObject);
begin
  QRepDesigner.EditProperties(QRepDesigner.GetSelectedElement);
end;}

procedure TTressShell.UndoMIClick(Sender: TObject);
begin
  QRepDesigner.Undo; //(@am): Nueva sintaxis para undo
  //QRepDesigner.DoUndo; //OLD
end;

procedure TTressShell.Rotatebands1Click(Sender: TObject);
begin
{REVISADO}

  //Quickreport.RotateBands:=Quickreport.RotateBands+1; //OLD
  //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
  QRepDesigner.QReport.RotateBands:=QRepDesigner.QReport.RotateBands+1;
end;

procedure TTressShell.Resetbands1Click(Sender: TObject);
begin
{REVISADO}

  {Quickreport.RotateBands:=0;
  Quickreport.HideBands:=False;}
  //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
  QRepDesigner.QReport.RotateBands:=0;
  QRepDesigner.QReport.HideBands:=False;
  HideBands1.Checked:=False;
end;

procedure TTressShell.Hidebands1Click(Sender: TObject);
begin
{REVISADO}

  HideBands1.Checked:=Not HideBands1.Checked;
  {Quickreport.HideBands:=HideBands1.Checked;}//OLD
  //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
  QRepDesigner.QReport.HideBands:=HideBands1.Checked;
end;

procedure TTressShell.QRepDesignerDeleteElement(QRD: TQRepDesigner;
  Component: TControl; var DoDelete: Boolean);
begin
  DoDelete := TRUE;
end;

{****************************************************}
procedure TTressShell.BBandaImagenClick(Sender: TObject);
begin
{REVISADO}

     if ( QRepDesigner.GetSelectedElement is TQrDesignBand ) AND
        ( OpenPictureDialog.Execute ) then
        TQrDesignBand( QRepDesigner.GetSelectedElement ).DesignBackground.LoadFromFile( OpenPictureDialog.FileName );

     if ( QRepDesigner.GetSelectedElement is TQrDesignGroup ) AND
        ( OpenPictureDialog.Execute ) then
        TQrDesignGroup( QRepDesigner.GetSelectedElement ).DesignBackground.LoadFromFile( OpenPictureDialog.FileName );
end;

procedure TTressShell.TextoEnriquecidoClick(Sender: TObject);
begin
     inherited;
     RichtextSpeedButton.Down:=TRUE;
     QRepDesigner.QRDesignState:=qrds_AddRichtext;
end;

procedure TTressShell.TextoEnriquecidoTablaClick(Sender: TObject);
begin
     inherited;
     QRepDesigner.QRDesignState:=qrds_AddDBRichtext;
end;

procedure TTressShell.CambiarTablaPrincipal1Click(Sender: TObject);
begin
     inherited;
     if fNuevoRep = NIL then fNuevoRep := TNuevoRep.Create( self );

     fNuevoRep.ShowModal;
     if fNuevoRep.ModalResult=mrOk then
     begin
          //QuickReport.ReportTitle := IntToStr( Ord( fNuevoRep.TablaPrincipal ) ); //OLD
          //(@am): Ahora debe accederse el objeto QuickReport de esta forma.
          QRepDesigner.QReport.ReportTitle := IntToStr( Ord( fNuevoRep.TablaPrincipal ) );
     end;
end;

procedure TTressShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     {REVISADO}
     if AppHelpFile<>#0 then Application.HelpFile:=AppHelpFile;
end;

function BuscaDialogo( const eEntidad: TipoEntidad; const sFilter: String; sKey, sDescription: String ): Boolean;
begin
     REsult := True;
end;

procedure TTressShell.DesignerRun;
 var lCiclo : Boolean;
begin
     if dmCliente.EmpresaAbierta then
     begin
          Global.Conectar;

          fDirDefault := zReportTools.DirPlantilla;
          OpenDialog.InitialDir := fDirDefault;
          SaveDialog.InitialDir := fDirDefault;


          lCiclo := FALSE;
          if Seleccion = NIL then Seleccion := TSeleccion.Create( self );

          while NOT lCiclo do
          begin
               lCiclo := TRUE;
               Seleccion.ShowModal;
               if Seleccion.Cerrar then
                  Close
               else
               begin
                    if Seleccion.AbrirPlantilla then
                       lCiclo := AbrePlantilla
                    else
                        NewMIClick( Self );
               end;
          end;
     end;
end;

procedure TTressShell.BRichTextToMemoClick(Sender: TObject);
 var oRichText : TQRDesignRichtext;
     oMemo : TQRDesignMemo;
     MS:TMemoryStream;
begin
     inherited;
     if ( QRepDesigner.GetSelectedElement is TQRDesignRichtext ) then
        if ZetaDialogo.ZConfirm( Caption, '�Desea Cambiar este Objeto Tipo Texto Enriquecido a Tipo Memo?', 0, mbYes )  then
        begin
             oRichText := TQRDesignRichtext(QRepDesigner.GetSelectedElement);
             oMemo := TQRDesignMemo.Create( SELF );
             oMemo.Parent := oRichText.Parent;
             oMemo.Left := oRichText.Left;
             oMemo.Top := oRichText.Top;
             oMemo.Height := oRichText.Height;
             oMemo.Width := oRichText.Width;
             //oMemo.Blockchange := oRichText.Blockchange;  //am: CAMBIO TEMPORAL
             //oMemo.BlockDelete := oRichText.BlockDelete; //am: CAMBIO TEMPORAL
             //oMemo.BlockMove := oRichText.BlockMove; //am: CAMBIO TEMPORAL
             //oMemo.BlockResize := oRichText.BlockResize;  //am: CAMBIO TEMPORAL
             oMemo.Color := oRichText.Color;
             oMemo.Autosize := FALSE;
             oMemo.AutoStretch := oRichText.AutoStretch;
             oMemo.Font := oRichText.Font;
             //oMemo.QrDesigner := oRichText.QrDesigner;  //am: CAMBIO TEMPORAL
             oMemo.StretchHeightWithBand := oRichText.StretchHeightWithBand;
             oMemo.Lines.Clear;
             {oMemo.Lines.AddStrings(oRichText.Lines);
             oMemo.Lines.Text := StrTransAll(oMemo.Lines.Text,CHR(9),'      ');}

             oRichTemp.Lines.Clear;
             MS:=TMemoryStream.Create;
             try
                oRichText.Lines.SaveToStream(MS);
                MS.Seek(0,0);
                oRichTemp.PlainText := FALSE;
                oRichTemp.Lines.LoadFromStream(MS);
                oRichTemp.PlainText := TRUE;
                oMemo.Lines.AddStrings( oRichTemp.Lines );
                oMemo.Lines.Text := StrTransAll(oMemo.Lines.Text,CHR(9),'      ');
             finally
                    MS.Free;
             end;

             QRepDesigner.DeleteSelectedElements;
        end;

end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TTressShell.SetDataChange( const Entidades: ListaEntidades );
{$endif}
begin

end;

procedure TTressShell.DesignerRunBatch;
const
     K_CMD_PLANTILLA = 4;
var
   lCiclo : Boolean;
   sPlantilla: string;
   iPos: integer;
begin
     if dmCliente.EmpresaAbierta then
     begin
          Global.Conectar;
          fDirDefault := zReportTools.DirPlantilla;
          OpenDialog.InitialDir := fDirDefault;
          SaveDialog.InitialDir := fDirDefault;
          sPlantilla := ParamStr( K_CMD_PLANTILLA );
          iPos := Pos( '=', sPlantilla );
          if ( iPos <> 0 ) then
             sPlantilla := Copy( sPlantilla, iPOs + 1, Length(sPlantilla) )
          else
              sPlantilla := VACIO;
          if StrLleno( sPlantilla ) and FileExists(sPlantilla) then
          begin
               QRepDesigner.LoadReport(sPlantilla);
               RefreshFontCombobox;
               ReportFileName:=sPlantilla;
               Caption:=ReportFileName;
                 {***(@am): En XE5 el componente de TRulerPanel, cambio a TqrdRulerPanel, fue agregada
                            la propiedad QReport. La cual relaciona el componete con un objeto TDesingQuickReport.
                            Por lo tanto ya no es necesario sincronizar el tamao de este componente con el TqrdRulerPanel,
                            ahora el TRulerPanel toma los valores de las propiedades units y Zoom del TDesingQuickReport.
                            Asi que estaran sincronizados automaticamente.***}
               {RulerPanel.Zoom:=Quickreport.Zoom;
               RulerPanel.Units:=Quickreport.Units;}
               CargaReporte;
          end
          else
          begin
               lCiclo := FALSE;
               if Seleccion = NIL then
                  Seleccion := TSeleccion.Create( self );
               while NOT lCiclo do
               begin
                    lCiclo := TRUE;
                    Seleccion.ShowModal;
                    if Seleccion.Cerrar then
                       Close
                    else
                    begin
                         if Seleccion.AbrirPlantilla then
                            lCiclo := AbrePlantilla
                         else
                             NewMIClick( Self );
                    end;
               end;
          end;
     end;
end;

end.
