object ExpressionForm: TExpressionForm
  Left = 237
  Top = 216
  HelpContext = 90033
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Expression field'
  ClientHeight = 350
  ClientWidth = 484
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button3: TButton
    Left = 164
    Top = 316
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 6
  end
  object Button4: TButton
    Left = 244
    Top = 316
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object FrameGroupBox: TGroupBox
    Left = 9
    Top = 224
    Width = 465
    Height = 81
    TabOrder = 5
    object AutoSizeCheckbox: TCheckBox
      Tag = 1001
      Left = 16
      Top = 16
      Width = 233
      Height = 17
      Caption = 'Autosize width'
      TabOrder = 0
    end
    object TransparentCheckBox: TCheckBox
      Tag = 1002
      Left = 256
      Top = 16
      Width = 193
      Height = 17
      Caption = 'Transparent'
      TabOrder = 3
    end
    object WordWrapCheckBox: TCheckBox
      Left = 16
      Top = 48
      Width = 233
      Height = 17
      Caption = 'Automatic wordwrap'
      TabOrder = 2
    end
    object StretchCheckBox: TCheckBox
      Tag = 1001
      Left = 16
      Top = 32
      Width = 233
      Height = 17
      Caption = 'Autostretch height'
      TabOrder = 1
    end
    object ResetCheckBox: TCheckBox
      Left = 256
      Top = 32
      Width = 201
      Height = 17
      Caption = 'Reset after print'
      TabOrder = 4
    end
  end
  object ExpressionGroupBox: TGroupBox
    Left = 9
    Top = 8
    Width = 465
    Height = 81
    TabOrder = 0
    object Label2: TLabel
      Left = 14
      Top = 26
      Width = 67
      Height = 23
      AutoSize = False
      Caption = 'Expression'
    end
    object Label6: TLabel
      Left = 14
      Top = 50
      Width = 67
      Height = 23
      AutoSize = False
      Caption = 'Nombre'
    end
    object lbTipCampo: TLabel
      Left = 206
      Top = 50
      Width = 67
      Height = 23
      AutoSize = False
      Caption = 'Tipo de Dato'
    end
    object Button5: TButton
      Left = 424
      Top = 22
      Width = 25
      Height = 21
      Caption = '...'
      TabOrder = 1
      OnClick = Button5Click
    end
    object ExpressionEdit: TEdit
      Left = 80
      Top = 22
      Width = 345
      Height = 21
      TabOrder = 0
      OnExit = ExpressionEditExit
    end
    object EAlias: TEdit
      Left = 80
      Top = 46
      Width = 97
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 10
      TabOrder = 2
    end
    object cbTipoGlobal: TComboBox
      Left = 280
      Top = 46
      Width = 145
      Height = 21
      Style = csDropDownList
      TabOrder = 3
      Items.Strings = (
        'Autom'#225'tico'
        'L'#243'gico'
        'Numero con Decimales'
        'Numero Entero'
        'Fecha'
        'Texto')
    end
  end
  object ColorGroupBox: TGroupBox
    Left = 249
    Top = 96
    Width = 225
    Height = 57
    TabOrder = 2
    object Button1: TButton
      Left = 120
      Top = 20
      Width = 89
      Height = 25
      Caption = '&Color...'
      TabOrder = 1
      OnClick = ColorButtonClick
    end
    object Button2: TButton
      Left = 16
      Top = 20
      Width = 89
      Height = 25
      Caption = '&Font...'
      TabOrder = 0
      OnClick = FontButtonClick
    end
  end
  object FormatGroupBox: TGroupBox
    Left = 9
    Top = 96
    Width = 233
    Height = 57
    TabOrder = 1
    object Label3: TLabel
      Left = 16
      Top = 26
      Width = 65
      Height = 15
      AutoSize = False
      Caption = 'Format'
      FocusControl = FormatEdit
    end
    object FormatEdit: TEdit
      Left = 80
      Top = 22
      Width = 137
      Height = 21
      TabOrder = 0
    end
  end
  object GroupBox4: TGroupBox
    Left = 249
    Top = 160
    Width = 225
    Height = 57
    TabOrder = 4
    object Label5: TLabel
      Left = 16
      Top = 26
      Width = 57
      Height = 23
      AutoSize = False
      Caption = 'Rotation'
    end
    object UnitLabel2: TLabel
      Tag = 1004
      Left = 157
      Top = 26
      Width = 60
      Height = 23
      AutoSize = False
      Caption = 'Degrees'
    end
    object SpeedButton19: TSpeedButton
      Left = 128
      Top = 24
      Width = 13
      Height = 12
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton19Click
    end
    object SpeedButton20: TSpeedButton
      Left = 128
      Top = 32
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton20Click
    end
    object DegreeEdit: TEdit
      Left = 72
      Top = 23
      Width = 57
      Height = 21
      MaxLength = 4
      TabOrder = 0
      OnKeyPress = DegreeEditKeyPress
    end
  end
  object GroupBox1: TGroupBox
    Left = 9
    Top = 160
    Width = 233
    Height = 57
    TabOrder = 3
    object Label1: TLabel
      Tag = 1000
      Left = 16
      Top = 28
      Width = 65
      Height = 21
      AutoSize = False
      Caption = '&Alignment'
      FocusControl = AlignmentComboBox
    end
    object AlignmentComboBox: TComboBox
      Tag = 1007
      Left = 80
      Top = 24
      Width = 137
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      Items.Strings = (
        'Left'
        'Right'
        'Centered')
    end
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = []
    Device = fdBoth
    Left = 64
    Top = 16
  end
  object ColorDialog1: TColorDialog
    Left = 8
    Top = 65528
  end
end
