unit FEditImageDir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, ExtCtrls, ExtDlgs, QRDCtrls;

type
  TEditImageDir = class(TForm)
    EDirectorio: TEdit;
    Label1: TLabel;
    OpenPictureDialog: TOpenPictureDialog;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    SpeedButton1: TSpeedButton;
    CBRotacion: TComboBox;
    Label2: TLabel;
    CBAutoSize: TCheckBox;
    CBStretch: TCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
  private
    function GetDirDefault : string;
    procedure SetDirDefault( const sDir : string );
  public
    property DirDefault : string read GetDirDefault write SetDirDefault;
  end;

var
  EditImageDir: TEditImageDir;
  function EditImageProps( QRC: TQRDesignImage; const sDefault : string; lUsarImagenDefault:boolean  ) : Boolean;


implementation

uses ZetaCommonTools;
{$R *.DFM}

function EditImageProps( QRC: TQRDesignImage; const sDefault : string; lUsarImagenDefault:boolean ) : Boolean;
begin
     if EditImageDir = NIL then
        EditImageDir := TEditImageDir.Create( Application.MainForm );
     with EditImageDir do
     begin
          //if StrLleno( QRC.FileName ) then DirDefault := QRC.FileName  //OLD
          {***(@am): Ya no podemos validar ocn FileName porque no podemos asignarle un valor VACIO para limpiarlo.***}
          if not lUsarImagenDefault then DirDefault := QRC.FileName
          else DirDefault := sDefault;
          CBRotacion.ItemIndex := QRC.Tag;
          CBAutosize.Checked := QRC.AutoSize;
          CBStretch.Checked := QRC.Stretch;
          ShowModal;
          Result := ModalResult = mrOk;
          if Result then
          begin
               {QRC.FileName:=DirDefault;
               QRC.AutoSize := CBAutosize.Checked;
               QRC.Stretch := CBStretch.Checked;
               QRC.Tag := CBRotacion.ItemIndex;
               try
                  QRC.Picture := NIL;
                  if FileExists(DirDefault) then
                     QRC.Picture.LoadFromFile(DirDefault);
               except
                     QRC.Picture := NIL;
               end;}//OLD
               {***(@am): Ahora al asignar la ruta a la propiedad FileName, el componente carga la imagen.***}
               QRC.AutoSize := CBAutosize.Checked;
               QRC.Stretch := CBStretch.Checked;
               QRC.Tag := CBRotacion.ItemIndex;
               try
                  QRC.Picture := NIL;
                  if FileExists(DirDefault) then
                     QRC.FileName:=DirDefault;
               except
                     QRC.Picture := NIL;
               end;
          end;
     end;
end;

procedure TEditImageDir.SpeedButton1Click(Sender: TObject);
begin
     with OpenPictureDialog do
     begin
          FileName := ExtractFileName( EDirectorio.Text );
          InitialDir := ExtractFileDir( EDirectorio.Text );
          if Execute then
             EDirectorio.Text := FileName;
     end;
end;

function TEditImageDir.GetDirDefault : string;
begin
     Result := EDirectorio.Text;
end;

procedure TEditImageDir.SetDirDefault( const sDir : string );
begin
     EDirectorio.Text := sDir;
end;

end.
