unit FRichProp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, QRDesign, QRDCtrls;

type
  TRichtextForm = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    StretchCheckbox: TCheckBox;
    AlignmentComboBox: TComboBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    GroupBox2: TGroupBox;
    RichEdit1: TRichEdit;
    Button5: TButton;
    ColorDialog1: TColorDialog;
    FontDialog1: TFontDialog;
    ExpressionButton: TButton;
    RemoveEmptyLineButton: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure ExpressionButtonClick(Sender: TObject);
    procedure RemoveEmptyLineButtonClick(Sender: TObject);
  private
  public
    TF: TFont;
    AColor: Longint;
    QRD: TQRepDesigner;
  end;

var
  RichtextForm: TRichtextForm;

function EditRichtextProps(QRD: TQRepDesigner; Owner: TWinControl; QRC: TQRDesignRichtext): Boolean;

implementation

uses
    FRichEd, ClipBrd, QRExpBld, {QRDsgn3,//am: CAMBIO TEMPORAL} QRDProcs,
    ZetaTipoEntidad,
    ZetaCommonLists,
    ZConstruyeFormula, ZetaDialogo ;
  
{$R *.DFM}

procedure TRichtextForm.Button2Click(Sender: TObject);
begin
  FontDialog1.Font.Assign(TF);
  If FontDialog1.Execute Then TF.Assign(FontDialog1.Font);
end;

procedure TRichtextForm.Button1Click(Sender: TObject);
begin
  ColorDialog1.Color:=AColor;
  If ColorDialog1.Execute Then AColor:=ColorDialog1.Color;
end;

procedure TRichtextForm.Button5Click(Sender: TObject);
var
  MS: TMemoryStream;
begin
  RichEditForm:=TRichEditForm.Create(Self);
  RichEditForm.QRD:=QRD;
  MS:=TMemoryStream.Create;
  RichEdit1.Lines.SaveToStream(MS);
  MS.Seek(0,0);
  RichEditForm.RichEdit1.Lines.LoadFromStream(MS);
  RichEditForm.ShowModal;
  MS.Seek(0,0);
  RichEditForm.RichEdit1.Lines.SaveToStream(MS);
  MS.Seek(0,0);
  RichEdit1.Lines.LoadFromStream(MS);
  RicheditForm.Free;
  MS.Free;
end;

procedure TRichtextForm.ExpressionButtonClick(Sender: TObject);
 var
     S: String;
     iEntidad: SmallInt;

begin
     Clipboard.AsText:='';
     RichEdit1.CopyToClipboard;
     S:= Clipboard.AsText;

     //@(am): Ahora se validara la entidad, para que en reportes sin entidad asignada salgo un mensaje de error de Tress y no uno generado por la propia excepcion.
     iEntidad := -1;
     try
        iEntidad := StrToInt( TQRepDesigner(Qrd).QReport.ReportTitle );
     Except
         ZError( 'Error en ' + Application.Title, '� Asigne una Tabla Principal a la plantilla !', 0 );
     end;
     if iEntidad >= 0 then
            if GetFormulaConstruye( TipoEntidad( iEntidad ),
                             S, RichEdit1.SelStart,
                             evReporte ) then
             begin
                  if (S<>'') then
                  begin
                       if RichEdit1.SelLength = 0 then S := '\'+S+'\';
                       Clipboard.AsText:=S;
                       RichEdit1.PasteFromClipboard;
                  end;
             end;

     RichEdit1.SetFocus;
end;

procedure TRichtextForm.RemoveEmptyLineButtonClick(
  Sender: TObject);
var
  CPos, LPos: Integer;
begin
  Lpos := SendMessage(RichEdit1.Handle,EM_LINEFROMCHAR,RichEdit1.SelStart,0);
  Cpos := SendMessage(RichEdit1.Handle,EM_LINEINDEX,Lpos,0);
  CPos := RichEdit1.SelStart-CPos;
  RichEdit1.SelStart:=RichEdit1.SelStart-CPos;
  RichEdit1.SelLength:=0;
  Clipboard.AsText:='%DEL';
  RichEdit1.PasteFromClipboard;
end;

function EditRichtextProps(QRD: TQRepDesigner; Owner: TWinControl; QRC: TQRDesignRichtext): Boolean;
var
  MS: TMemoryStream;
begin
  MS:=TMemoryStream.Create;
  RichtextForm:=TRichtextForm.Create(Owner);
  RichtextForm.QRD:=QRD;
  QRC.Lines.SaveToStream(MS);
  MS.Seek(0,0);
  RichtextForm.RichEdit1.Lines.LoadFromStream(MS);
  RichtextForm.StretchCheckBox.Checked:=QRC.AutoStretch;
  Case QRC.Alignment of
    taRightJustify  : RichtextForm.AlignmentComboBox.ItemIndex:=1;
    taLeftJustify   : RichtextForm.AlignmentComboBox.ItemIndex:=0;
    taCenter        : RichtextForm.AlignmentComboBox.ItemIndex:=2;
  end;
  RichtextForm.TF:=TFont.Create;
  RichtextForm.TF.Assign(QRC.Font);
  RichtextForm.AColor:=QRC.Color;
  Result:=RichtextForm.ShowModal=mrOk;
  If Result Then
    begin
      MS.Seek(0,0);
      RichtextForm.RichEdit1.Lines.SaveToStream(MS);
      MS.Seek(0,0);
      QRC.Lines.LoadFromStream(MS);
      QRC.AutoStretch:=RichtextForm.StretchCheckBox.Checked;
      Case RichtextForm.AlignmentComboBox.ItemIndex of
        1: QRC.Alignment:=taRightJustify;
        0: QRC.Alignment:=taLeftJustify;
        2: QRC.Alignment:=taCenter;
      end;
      QRC.Font.Assign(RichtextForm.TF);
      QRC.Color:=RichtextForm.AColor;
      QRC.Invalidate;
    end;
  RichtextForm.TF.Free;
  RichtextForm.Free;
end;


end.
