program SeleccionDisena;

uses
  Forms,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZReportTools in '..\Reportes\ZReportTools.pas',
  DBaseCliente in '..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DGlobal in '..\DataModules\DGlobal.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DBaseSeleccionDiccionario in '..\Seleccion\DBaseSeleccionDiccionario.pas' {dmBaseSeleccionDiccionario: TDataModule},
  DDiccionario in 'Seleccion\DDiccionario.pas' {dmDiccionario: TDataModule};

{$R *.RES}
procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
     Application.Initialize;
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
     Application.Title := 'Dise�ador';
  Application.HelpFile := 'Tress.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if Login( TRUE ) then
          begin
               CierraSplash;
               Show;
               Update;
               BeforeRun;
               if ( ParamCount > 0 ) then
                  DesignerRunBatch
               else
                   DesignerRun;
          end
          else
          begin
               CierraSplash;
               Free
          end;
     end;
     Application.Run;
end.
