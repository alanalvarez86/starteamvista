object ReportPropForm: TReportPropForm
  Left = 290
  Top = 272
  HelpContext = 90044
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Report Options'
  ClientHeight = 380
  ClientWidth = 539
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object OrientationRadioGroup: TRadioGroup
    Tag = 1006
    Left = 272
    Top = 8
    Width = 113
    Height = 57
    Items.Strings = (
      'Portrait'
      'Landscape')
    TabOrder = 1
  end
  object Button3: TButton
    Left = 192
    Top = 345
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 6
  end
  object Button4: TButton
    Left = 272
    Top = 345
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object GroupBox2: TGroupBox
    Tag = 1006
    Left = 16
    Top = 239
    Width = 505
    Height = 89
    Caption = 'Frame'
    TabOrder = 5
    object Label7: TLabel
      Tag = 1008
      Left = 192
      Top = 59
      Width = 41
      Height = 21
      AutoSize = False
      Caption = '&Style'
      FocusControl = PenStyleComboBox
    end
    object Label8: TLabel
      Tag = 1009
      Left = 16
      Top = 59
      Width = 76
      Height = 20
      AutoSize = False
      Caption = '&Width'
    end
    object Label9: TLabel
      Tag = 1010
      Left = 16
      Top = 24
      Width = 41
      Height = 17
      AutoSize = False
      Caption = 'Li&nes'
    end
    object SpeedButton19: TSpeedButton
      Left = 136
      Top = 56
      Width = 13
      Height = 13
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton19Click
    end
    object SpeedButton20: TSpeedButton
      Left = 136
      Top = 66
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton20Click
    end
    object BitBtn7: TBitBtn
      Tag = 1011
      Left = 398
      Top = 18
      Width = 89
      Height = 25
      Caption = '&Color...'
      TabOrder = 4
      OnClick = BitBtn7Click
    end
    object PenStyleComboBox: TComboBox
      Tag = 1022
      Left = 238
      Top = 56
      Width = 249
      Height = 21
      Style = csDropDownList
      TabOrder = 5
    end
    object TopCheckBox: TCheckBox
      Tag = 1012
      Left = 80
      Top = 24
      Width = 67
      Height = 17
      Caption = 'Top'
      TabOrder = 0
    end
    object BottomCheckBox: TCheckBox
      Tag = 1013
      Left = 152
      Top = 24
      Width = 67
      Height = 17
      Caption = 'Bottom'
      TabOrder = 1
    end
    object LeftCheckBox: TCheckBox
      Tag = 1014
      Left = 224
      Top = 24
      Width = 67
      Height = 17
      Caption = 'Left'
      TabOrder = 2
    end
    object RightCheckBox: TCheckBox
      Tag = 1015
      Left = 296
      Top = 24
      Width = 67
      Height = 17
      Caption = 'Right'
      TabOrder = 3
    end
    object PenWidthSpinEdit: TEdit
      Left = 96
      Top = 56
      Width = 41
      Height = 21
      MaxLength = 2
      TabOrder = 6
    end
  end
  object PaperSizeGB: TGroupBox
    Left = 16
    Top = 71
    Width = 505
    Height = 57
    Caption = 'Paper size'
    TabOrder = 3
    object PaperSize: TComboBox
      Left = 16
      Top = 22
      Width = 145
      Height = 21
      Style = csDropDownList
      TabOrder = 0
      OnChange = PaperSizeChange
    end
    object Panel1: TPanel
      Left = 168
      Top = 16
      Width = 313
      Height = 33
      BevelOuter = bvNone
      TabOrder = 1
      object Label10: TLabel
        Left = 8
        Top = 10
        Width = 49
        Height = 15
        AutoSize = False
        Caption = 'Width'
      end
      object Label11: TLabel
        Left = 168
        Top = 10
        Width = 57
        Height = 15
        AutoSize = False
        Caption = 'Length'
      end
      object UnitLabel2: TLabel
        Tag = 1004
        Left = 117
        Top = 10
        Width = 16
        Height = 13
        Caption = 'mm'
      end
      object UnitLabel3: TLabel
        Tag = 1004
        Left = 285
        Top = 10
        Width = 36
        Height = 15
        AutoSize = False
        Caption = 'mm'
      end
      object SpeedButton1: TSpeedButton
        Left = 96
        Top = 8
        Width = 13
        Height = 12
        Glyph.Data = {
          8A000000424D8A00000000000000760000002800000007000000050000000100
          0400000000001400000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
          0000F00000F0FF000FF0FFF0FFF0}
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 96
        Top = 16
        Width = 13
        Height = 12
        Glyph.Data = {
          86000000424D8600000000000000760000002800000007000000040000000100
          0400000000001000000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
          0FF0F00000F000000000}
        OnClick = SpeedButton2Click
      end
      object SpeedButton4: TSpeedButton
        Left = 264
        Top = 8
        Width = 13
        Height = 12
        Glyph.Data = {
          8A000000424D8A00000000000000760000002800000007000000050000000100
          0400000000001400000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
          0000F00000F0FF000FF0FFF0FFF0}
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TSpeedButton
        Left = 264
        Top = 16
        Width = 13
        Height = 12
        Glyph.Data = {
          86000000424D8600000000000000760000002800000007000000040000000100
          0400000000001000000000000000000000001000000010000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
          0FF0F00000F000000000}
        OnClick = SpeedButton3Click
      end
      object WidthSpinEdit: TEdit
        Left = 56
        Top = 7
        Width = 41
        Height = 21
        TabOrder = 0
        OnChange = WidthSpinEditChange
        OnKeyPress = TopMarginKeyPress
      end
      object LengthSpinEdit: TEdit
        Left = 224
        Top = 7
        Width = 41
        Height = 21
        TabOrder = 1
        OnKeyPress = TopMarginKeyPress
      end
    end
  end
  object MarginsGB: TGroupBox
    Left = 16
    Top = 139
    Width = 505
    Height = 89
    Caption = 'Margins'
    TabOrder = 4
    object Label12: TLabel
      Left = 16
      Top = 23
      Width = 49
      Height = 17
      AutoSize = False
      Caption = 'Top'
    end
    object Label13: TLabel
      Left = 16
      Top = 59
      Width = 49
      Height = 20
      AutoSize = False
      Caption = 'Bottom'
    end
    object Label14: TLabel
      Left = 172
      Top = 23
      Width = 37
      Height = 17
      AutoSize = False
      Caption = 'Left'
    end
    object Label15: TLabel
      Left = 172
      Top = 59
      Width = 37
      Height = 20
      AutoSize = False
      Caption = 'Right'
    end
    object Label16: TLabel
      Left = 320
      Top = 23
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'Column space'
    end
    object Label17: TLabel
      Left = 320
      Top = 59
      Width = 121
      Height = 20
      AutoSize = False
      Caption = 'Number of columns'
    end
    object UnitLabel1: TLabel
      Tag = 1004
      Left = 117
      Top = 23
      Width = 36
      Height = 13
      AutoSize = False
      Caption = 'mm'
    end
    object UnitLabel4: TLabel
      Tag = 1004
      Left = 117
      Top = 59
      Width = 36
      Height = 13
      AutoSize = False
      Caption = 'mm'
    end
    object UnitLabel5: TLabel
      Tag = 1004
      Left = 269
      Top = 23
      Width = 36
      Height = 13
      AutoSize = False
      Caption = 'mm'
    end
    object UnitLabel6: TLabel
      Tag = 1004
      Left = 269
      Top = 59
      Width = 36
      Height = 13
      AutoSize = False
      Caption = 'mm'
    end
    object UnitLabel7: TLabel
      Tag = 1004
      Left = 461
      Top = 23
      Width = 36
      Height = 13
      AutoSize = False
      Caption = 'mm'
    end
    object SpeedButton5: TSpeedButton
      Left = 96
      Top = 21
      Width = 13
      Height = 12
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton5Click
    end
    object SpeedButton6: TSpeedButton
      Left = 96
      Top = 29
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton6Click
    end
    object SpeedButton10: TSpeedButton
      Left = 248
      Top = 21
      Width = 13
      Height = 9
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton10Click
    end
    object SpeedButton12: TSpeedButton
      Left = 440
      Top = 21
      Width = 13
      Height = 12
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton12Click
    end
    object SpeedButton14: TSpeedButton
      Left = 96
      Top = 57
      Width = 13
      Height = 12
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton14Click
    end
    object SpeedButton9: TSpeedButton
      Left = 248
      Top = 29
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton9Click
    end
    object SpeedButton11: TSpeedButton
      Left = 440
      Top = 29
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton11Click
    end
    object SpeedButton13: TSpeedButton
      Left = 96
      Top = 66
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton13Click
    end
    object SpeedButton15: TSpeedButton
      Left = 248
      Top = 57
      Width = 13
      Height = 12
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton15Click
    end
    object SpeedButton16: TSpeedButton
      Left = 248
      Top = 66
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton16Click
    end
    object SpeedButton17: TSpeedButton
      Left = 464
      Top = 56
      Width = 13
      Height = 12
      Glyph.Data = {
        8A000000424D8A00000000000000760000002800000007000000050000000100
        0400000000001400000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFF00000
        0000F00000F0FF000FF0FFF0FFF0}
      OnClick = SpeedButton17Click
    end
    object SpeedButton18: TSpeedButton
      Left = 464
      Top = 65
      Width = 13
      Height = 12
      Glyph.Data = {
        86000000424D8600000000000000760000002800000007000000040000000100
        0400000000001000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFF0FFF0FF00
        0FF0F00000F000000000}
      OnClick = SpeedButton18Click
    end
    object TopMargin: TEdit
      Left = 64
      Top = 20
      Width = 33
      Height = 21
      TabOrder = 0
      OnKeyPress = TopMarginKeyPress
    end
    object BottomMargin: TEdit
      Left = 64
      Top = 56
      Width = 33
      Height = 21
      TabOrder = 1
      OnKeyPress = TopMarginKeyPress
    end
    object LeftMargin: TEdit
      Left = 216
      Top = 20
      Width = 33
      Height = 21
      TabOrder = 2
      OnKeyPress = TopMarginKeyPress
    end
    object RightMargin: TEdit
      Left = 216
      Top = 56
      Width = 33
      Height = 21
      TabOrder = 3
      OnKeyPress = TopMarginKeyPress
    end
    object ColumnSpace: TEdit
      Left = 408
      Top = 20
      Width = 33
      Height = 21
      TabOrder = 4
      OnKeyPress = TopMarginKeyPress
    end
    object Columns: TEdit
      Left = 440
      Top = 56
      Width = 25
      Height = 21
      MaxLength = 2
      TabOrder = 5
      OnKeyPress = ColumnsKeyPress
    end
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 8
    Width = 241
    Height = 57
    Caption = 'Report Title'
    TabOrder = 0
    object TitleEdit: TEdit
      Tag = 1010
      Left = 16
      Top = 22
      Width = 209
      Height = 21
      Enabled = False
      TabOrder = 0
    end
  end
  object GroupBox3: TGroupBox
    Left = 400
    Top = 8
    Width = 121
    Height = 57
    TabOrder = 2
    object Button2: TButton
      Left = 16
      Top = 19
      Width = 89
      Height = 25
      Caption = 'Font...'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
  object ColorDialog1: TColorDialog
    Left = 65528
    Top = 115
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = []
    Device = fdBoth
    Left = 65528
    Top = 139
  end
end
