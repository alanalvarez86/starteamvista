unit DDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseDiccionario,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists;

type
  TdmDiccionario = class(TdmBaseDiccionario)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function ChecaDerechoConfidencial: Boolean;override;
    function ValidaModulo( const eClasifi: eClasifiReporte ): Boolean;override;
  public
    { Public declarations }
    procedure SetLookupNames;override;
    function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;

  end;

var
  dmDiccionario: TdmDiccionario;

implementation
uses
     FAutoClasses,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZAccesosTress;


{$R *.DFM}

procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     inherited;
     ClasifiDefault := crVisitantes;
end;

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
end;

procedure TdmDiccionario.GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
     AgregaClasifi( oLista, crVisitantes );
end;

function TdmDiccionario.ChecaDerechoConfidencial: Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_CONS_REPORTES, K_DERECHO_SIST_KARDEX );
end;

function TdmDiccionario.ValidaModulo( const eClasifi: eClasifiReporte): Boolean;
begin
     with Autorizacion do
          case eClasifi of
               crVisitantes: Result := OKModulo( OkVisitantes )
               else
                   Result := inherited ValidaModulo( eClasifi );
          end;
end;

function TdmDiccionario.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     Result := D_CONS_REPORTES;
end;


end.
