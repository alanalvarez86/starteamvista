program VisitantesDisena;

uses
  Forms,
  MidasLib,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZReportTools in '..\Reportes\ZReportTools.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  ZetaSplash in '..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DGlobal in '..\VisitantesMGR\DGlobal.pas',
  DCliente in 'DCliente.pas',
  EditorDResumen in '..\Medicos\EditorDResumen.pas',
  DDiccionario in 'Visitantes\DDiccionario.pas' {dmDiccionario: TDataModule};

{$R *.RES}

begin
     Application.Initialize;
     Application.Title := 'Dise�ador';
  Application.HelpFile := 'Tress.chm';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if Login( TRUE ) then
          begin
               Show;
               Update;
               BeforeRun;
               if ( ParamCount > 0 ) then
                  DesignerRunBatch
               else
                   DesignerRun;
          end
          else
          begin
               Free
          end;
     end;
     Application.Run;
end.
