unit DAutomatiza;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerCalcNomina,
     DServerConsultas,
     DServerRecursos,
     DServerSistema,
     {$else}
     DCalcNomina_TLB,
     Consultas_TLB,
     Recursos_TLB,
     Sistema_TLB,
     {$endif}
     {$ifndef VER130}
     Variants,
     {$endif}
     FAutoClasses,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaASCIIFile,
     ZetaClientDataSet;

const
     P_EMPRESA        = 'EMPRESA';
     P_OPERACION      = 'OPERACION';
     P_USUARIO        = 'USUARIO';
     P_CLAVE          = 'CLAVE';
     P_EMAILS         = 'EMAILS';

     P_OP_PRENOMINA   = 'P';
     P_TIPO_CALCULO   = 'TIPO';
     P_FECHA_CALCULO  = 'FECHA';
     P_TIPO_NOMINA    = 'NOMINA';

     P_OP_SAL_INTEG   = 'S';
     P_FECHA_REFEREN  = 'FECHA';
     P_FECHA_CAMBIO   = 'FECHACAMBIO';
     P_FEC_AVISO_IMSS = 'AVISOIMSS';
     P_FEC_ESPEC_IMSS = 'FECHAIMSS';
     P_INCLUIR_INCAPA = 'INCAPACITA';

     P_OP_RECALCTARJE = 'T';
     P_FECHA_INI      = 'FECHA';
     P_FECHA_FIN      = 'FECHAFINAL';
     P_REV_HORARIOS   = 'REVHORARIO';
     P_REV_PUESTO     = 'REVPUESTO';
     P_CAN_HORARIO    = 'CANHORARIO';
     P_CAN_EXTRAS     = 'CANEXTRAS';
     P_CAN_DESCANSO   = 'CANDESCANSO';
     P_CAN_PERMCGOCE  = 'CANPERMCG';
     P_CAN_PERMSGOCE  = 'CANPERMSG';
     P_CAN_EXTPREP    = 'CANEXTPREP';
     P_CAN_REPROCESA = 'REPROCESA';

     P_OP_CIERREVAC = 'V';
     P_VAC_FECHAREF = 'FECHAREF';
     P_VAC_FECHACIERRE = 'FECHACIERRE';
     P_VAC_CIERREDIASGOZO = 'CIERREDIASGOZO';
     P_VAC_CIERREDIASPAGO = 'CIERREDIASPAGO';
     P_VAC_CIERREDIASPRIMA = 'CIERREDIASPRIMA';
     P_VAC_OBSERVACIONES = 'OBSERVACIONES';

type
  eOperacion = ( eoCalculaPrenomina,eoCalcularSalarioIntegrado,eoRecalcularTarjetas, eoCierreVacaciones );
  TdmAutomatiza = class(TDataModule)
    cdsTemporal: TZetaClientDataSet;
    cdsPeriodo: TZetaClientDataSet;
    cdsUsuarios: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FUsuario: string;
    FClave: string;
    FPrenomina: Boolean;
    FTipoCalculo: Integer;

    //agrege FFechaPreNomina
    FFechaPrenomina:TDate;
    FBitacora: TAsciiLog;

    FEmpresa: String;
    FYear: integer;
    FTipoPeriodo: integer;
    FNumero: integer;

    //variables para salario integrado
    FSalIntegrado: Boolean;
    FFechaReferen: TDate;
    FFechaEspecImss: TDate;
    FNoIncluirIncapa: Boolean;
    FAvisoImss: Boolean;
    FReferencia: Boolean;

    //variables para rec�lculo de tarjetas
    FRecalcTarjetas :Boolean;
    FRecFechaInicial :TDate; //Fecha Inicial del recalculo de tarjetas
    FRecFechaFin: TDate; //Fecha Final del recalculo de tarjetas
    FRecRevisarHorarios: Boolean; //Revisar horarios Si , No
    FRecRevisarPuestos: Boolean; //Revisar Puesto Si , No
    FRecCancelarHorarios: Boolean; //Cancelar horarios temporales Si , No
    FRecCancelarAutExtras: Boolean; //Cancelar Autorizaci�n de horas extras Si , No
    FRecCancelarAutDescan: Boolean; //Cancelar Autorizaci�n de descanso trabajado Si , No
    FRecCancelarAutPerCGoce: Boolean; //Cancelar Autorizaci�n de permiso con goce Si , No
    FRecCancelarAutPerSGoce: Boolean; //Cancelar Autorizaci�n de permiso sin goce Si , No
    FRecCancelarAutExtPrep: Boolean;
    FRecReprocesarTarjetas: Boolean; //Reprocesar Tarjetas Si, No

    //Cierre de Vacaciones
    FCierreVacaciones :Boolean;
    FFechaRef : String; // Fecha Referencia ( Default 0 = Aniversario de Empleado 1 = Fecha de Referencia )
    FFechaCierre : String; //Fecha de cierre ( Default = Aniversario de Empleado )
    FCierreDiasGozo : String; //Cierre de dias Gozo ( Default = Saldo a la Fecha )
    FCierreDiasPago : String; //Cierre de dias Pago ( Default = Saldo a la Fecha )
    FCierreDiasPrima : String; //Cierre de dias Prima ( Default = Saldo a la Fecha )
    FObservaciones : String; // Observaciones  ( Default = VACIO )

    FEmails: string;
    FBitacoraMail: TStrings;

    FListaParametros: TStrings;
    FOpcion: eOperacion;
    {$ifdef DOS_CAPAS}
    FServerCalcNomina: TdmServerCalcNomina;
    FServerConsultas: TdmServerConsultas;
    FServerRecursos: TdmServerRecursos;
    FServerSistema: TdmServerSistema;
    {$else}
    FServerCalcNomina: IdmServerCalcNominaDisp;
    FServerConsultas: IdmServerConsultasDisp;
    FServerRecursos: IdmServerRecursosDisp;
    FServerSistema: IdmServerSistemaDisp;
    function GetServerCalcNomina: IdmServerCalcNominaDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    {$endif}
    function InitAutorizacion: Boolean;
    function GetTipoPeriodos: string;
    function GetModulo: TModulos;
    function LoginBatch: Boolean;
    procedure Log( const sTexto: String );
    procedure LogError( const sTexto: String );
    procedure LogEnd;
    procedure LogInit;
    procedure LogProcessEnd(Resultado: OleVariant);
    procedure ParametrosPrepara;
    procedure GetListaParametros;
    procedure CalcularPrenomina(Parametros: OleVariant);
    function CargaDatosPrenomina: Boolean;
    function GetPosDerecho(const eTipo: eTipoCalculo): Integer;
    procedure BitacoraSalIntegrado;
    procedure BitacoraCierreVacaciones;
    procedure BitacoraRecalcTarjetas;
    procedure EnviarCorreos;
    procedure CalcularSalarioIntegrado(Parametros: OleVariant);
    function CrearListaUsuarios( sEmails: String;oUsuarios :TStrings):TStrings;
    function GetGlobalFormula(const iGlobal:Integer): String;
    procedure RecalcularTarjetas(Parametros: OleVariant);
    procedure CierraVacaciones(Parametros: OleVariant);
    function PuedeCambiarTarjeta( const dInicial, dFinal: TDate; var sMensaje : string ):Boolean;
    function GetUsuarioTressAutomatiza:Boolean; // (JB) Trae el US_CODIGO dela configuracion de TressAutomatiza
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerCalcNomina: TdmServerCalcNomina read FServerCalcNomina;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerRecursos: TdmServerRecursos read FServerRecursos;
    property ServerSistema: TdmServerSistema read FServerSistema;
    {$else}
    property ServerCalcNomina: IdmServerCalcNominaDisp read GetServerCalcNomina;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    {$endif}
  public
    { Public declarations }
    procedure Procesar;
    procedure DespliegaAyuda( sComando: string );
    property ListaParametros: TStrings read FListaParametros;
  end;

var
  dmAutomatiza: TdmAutomatiza;


implementation

uses
     FAutoServer,
     ZetaCommonTools,
     ZGlobalTress,
     ZASCIITools,
     ZBaseThreads,
     ZAccesosMgr,
     ZAccesosTress,
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker,
     DZetaServerProvider,
     {$endif}
     DCliente,
     DBasicoCliente,
     ZetaRegistryCliente;

{$R *.DFM}

const
     K_RELLENO                     = 10;
     K_QRY_PERIODO = 'select PE_YEAR, PE_NUMERO from PERIODO ' +
                     'where ( PE_TIPO = %0:d ) and ( PE_NUMERO < %1:d ) and ( PE_ASI_INI <= %2:s ) and ( PE_ASI_FIN >= %2:s )';
     K_QRY_GLOBAL                  = 'select GL_FORMULA from GLOBAL where GL_CODIGO = %d';

{ ********* TdmAutomatiza ********** }

procedure TdmAutomatiza.DataModuleCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     FBitacora := TAsciiLog.Create;
     FBitacoraMail:= TStringList.Create;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmCliente := TdmCliente.Create( Self );
     {$ifdef DOS_CAPAS}
     FServerCalcNomina := TdmServerCalcNomina.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerRecursos := TdmServerRecursos.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
     {$endif}
     FListaParametros := TStringList.Create;
     GetListaParametros;
end;

procedure TdmAutomatiza.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FListaPArametros );
    {$ifdef DOS_CAPAS}
     FreeAndNil( FServerCalcNomina );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerRecursos );
     FreeAndNil( FServerSistema );
    {$endif}
     FreeAndNil( dmCliente );
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     FreeAndNil( FBitacora );
     FreeAndNil( FBitacoraMail );
end;

{$ifndef DOS_CAPAS}
function TdmAutomatiza.GetServerCalcNomina: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( dmCliente.CreaServidor( CLASS_dmServerCalcNomina, FServerCalcNomina ) );
end;

function TdmAutomatiza.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmAutomatiza.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result := IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;

function TdmAutomatiza.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;
{$endif}

procedure TdmAutomatiza.LogInit;
begin
     FBitacoraMail.Clear;
     with FBitacora do
     begin
          if not Used then
          begin
               with Application do
               begin
                    Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + Title + '.log' );
               end;
          end;
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ) );
          FBitacoraMail.Add( Format( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ), [ FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now )]  ) );
     end;
end;

procedure TdmAutomatiza.LogProcessEnd( Resultado: OleVariant );
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     with TProcessInfo.Create( nil ) do
     begin
          try
             SetResultado( Resultado );
             case Status of
                  epsEjecutando: Log( '� Proceso Incompleto !' );
                  epsOK:
                  begin
                       if ( TotalErrores > 0 ) then
                          Log( Format( 'Termin� Con %d Errores', [ TotalErrores ] ) )
                       else
                           if ( TotalAdvertencias > 0 ) then
                              Log( Format( 'Termin� Con %d Advertencias', [ TotalAdvertencias ] ) )
                           else
                               if ( TotalEventos > 0 ) then
                                  Log( Format( 'Termin� Con %d Mensajes', [ TotalEventos ] ) )
                                else
                                    Log( 'Termin� Con Exito' );
                  end;
                  epsCancelado: Log( '� Proceso Cancelado !' );
                  epsError: Log( '� Proceso Con Errores !' );
             end;
             Log( Format( 'Folio:    %d', [ Folio ] ) );
             Log( Format( 'Inicio:   %s', [ FormatDateTime( K_DIA_HORA, Inicio ) ] ) );
             Log( Format( 'Fin:      %s', [ FormatDateTime( K_DIA_HORA, Fin ) ] ) );
          finally
                 Free;
          end;
     end;
end;

procedure TdmAutomatiza.LogEnd;
begin
     with FBitacora do
     begin
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ) );
          Close;
     end;
end;

function TdmAutomatiza.InitAutorizacion: Boolean;
begin
      Autorizacion.Cargar( dmCliente.GetAuto );
      with Autorizacion do
      begin
           Result := OKModulo( GetModulo );
      end;
end;

// Login batch de automatiza

function TdmAutomatiza.LoginBatch: Boolean;
begin
     Result := GetUsuarioTressAutomatiza;
end;

//grabar datos en bitacora
function TdmAutomatiza.CargaDatosPrenomina: Boolean;
begin
     with cdsPeriodo do
     begin
          Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( K_QRY_PERIODO,
                                                                                [ FTipoPeriodo,
                                                                                //ZetaCommonClasses.K_LIMITE_NOM_NORMAL,
                                                                                StrToIntDef( GetGlobalFormula( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) , 200 ),
                                                                                EntreComillas( FechaAsStr( FFechaPrenomina ) )  ] ) );
          Result := ( not IsEmpty );
          if Result then
          begin
               { Se inicializa en este punto porque hasta aqui se sabe si se conecto la empresa}
               dmCliente.InitArrayTPeriodo;  //acl
               FYear := FieldByName( 'PE_YEAR' ).AsInteger;
               FNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
               with FBitacora do
               begin
                    WriteTexto( '** Par�metros de pren�mina **' );
                    WriteTexto( Format( 'Tipo de c�lculo: %s', [ ZetaCommonLists.ObtieneElemento( lfTipoCalculo, FTipoCalculo ) ] ) );
                    WriteTexto( Format( 'Fecha:           %s', [ FechaCorta( FFechaPrenomina ) ] ) );
                    WriteTexto( Format( 'A�o:             %d', [ FYear ] ) );
                    WriteTexto( Format( 'N�mina:          %s', [ ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, FTipoPeriodo ) ] ) );
                    WriteTexto( Format( 'Per�odo:         %d', [ FNumero ] ) );
                    WriteTexto( StringOfChar( '-', 40 ) );
               end;
          end
          else
              LogError( 'No hay n�minas ordinarias para la fecha: ' + FechaCorta( FFechaPrenomina ) );
     end;
end;

//graba datos de prenomina

procedure TdmAutomatiza.CalcularPrenomina(Parametros: OleVariant);
begin
     Log('');
     Log( 'Inicio c�lculo de pren�mina' );
     try
        LogProcessEnd( ServerCalcNomina.CalculaPreNomina( dmCliente.Empresa, Parametros ) );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'Fin c�lculo de pren�mina' );
     Log('');
end;

///( 10/03/06 ) EZ: agregue GetPosDerechos anteriormente estaba en
// D:\3win_20\Wizards\FWizAsistCalculoPreNomina.pas(29):     function GetPosDerecho(const eTipo: eTipoCalculo): Integer;

function TdmAutomatiza.GetPosDerecho( const eTipo: eTipoCalculo ): Integer;
begin
     if ( eTipo = tcNueva ) then
        Result := K_DERECHO_CONSULTA
     else
        Result := K_DERECHO_ALTA;
end;

////////////////

function TdmAutomatiza.GetModulo: TModulos;
begin
     case FOpcion of
          eoCalculaPrenomina:         Result := okAsistencia;
          eoCalcularSalarioIntegrado: Result := okRecursos;
          eoRecalcularTarjetas :      Result := okAsistencia;
          eoCierreVacaciones :        Result := okRecursos;
     else
         Result := okAsistencia;
     end;
end;

function TdmAutomatiza.PuedeCambiarTarjeta( const dInicial, dFinal: TDate; var sMensaje : string ):Boolean;
begin
     Result := (zStrToBool(GetGlobalFormula( K_GLOBAL_BLOQUEO_X_STATUS ) )
               or ZetaCommonTools.PuedeCambiarTarjeta ( dInicial,dFinal,
                                                        StrToFecha( GetGlobalFormula( K_GLOBAL_FECHA_LIMITE ) ),
                                                        CheckDerecho( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES ),
                                                        sMensaje));
end;



procedure TdmAutomatiza.Procesar;
var
     FParametros: TZetaParams;
     sMensaje, sMensajeError:string;
     lLoop, lSalirTress: Boolean;
     sMensajeErrorHTTP: String;
     procedure GetParametrosRango;
     begin
          with FParametros do
          begin
               { Rangos }
               AddString( 'RangoLista', VACIO );
               AddString( 'Condicion', VACIO );
               AddString( 'Filtro', VACIO );
          end;
     end;

     function ValidaParametrosCierreVacaciones( var sError : String ) : Boolean;
     var
          iValor : Extended;
     begin
          sError := VACIO;
          result := True;
          if StrLleno ( FFechaRef ) and ( not TryStrToFloat( FFechaRef, iValor ) ) and ( ( iValor = 0 ) or ( iValor = 1 ) ) then
          begin
               sError := sError + 'El p�rametro ' + P_VAC_FECHAREF + ' no es v�lido: ' + FFechaRef + ' ';
               result := False;
          end;
          if StrLleno ( FFechaCierre ) and ( not TryStrToFloat( FFechaCierre, iValor ) ) then
          begin
               sError := sError + 'El p�rametro ' + P_VAC_FECHACIERRE + ' no es v�lido: ' + FFechaCierre + ' ';
               result := False;
          end;
          if StrLleno ( FCierreDiasGozo ) and ( ( not TryStrToFloat( FCierreDiasGozo, iValor ) ) or ( iValor < 0 ) ) then
          begin
               sError := sError + 'El p�rametro ' + P_VAC_CIERREDIASGOZO + ' no es v�lido: ' + FCierreDiasGozo  + ' ';
               result := False;
          end;
          if StrLleno ( FCierreDiasPago ) and ( ( not TryStrToFloat( FCierreDiasPago, iValor ) ) or ( iValor < 0 ) ) then
          begin
               sError := sError + 'El p�rametro ' + P_VAC_CIERREDIASPAGO + ' no es v�lido: ' + FCierreDiasPago  + ' ';
               result := False;
          end;
          if StrLleno ( FCierreDiasPrima ) and ( ( not TryStrToFloat( FCierreDiasPrima, iValor ) ) or ( iValor < 0 ) ) then
          begin
               sError := sError + 'El p�rametro ' + P_VAC_CIERREDIASPRIMA + ' no es v�lido: ' + FCierreDiasPrima  + ' ';
               result := False;
          end;
     end;
     procedure validarHTTPEmail( var sMensajeErrorHTTP: String );
     begin
          sMensajeErrorHTTP := VACIO;
          if ClientRegistry.TipoConexion = conxHTTP then
          begin
               dmCliente.ValidarConexionHTTP( lLoop, lSalirTress, sMensajeErrorHTTP, True);
          end;
     end;
begin
     LogInit;
     try
        ParametrosPrepara;
        with FBitacora do
        begin
             WriteTexto( Format( 'Empresa:             %s', [ FEmpresa ] ) );
             WriteTexto( StringOfChar( '-', 40 ) );
        end;
        if dmCliente.BuscaCompany( FEmpresa ) then
        begin
             InitAutorizacion;
             validarHTTPEmail( sMensajeErrorHTTP );
             if strVacio( sMensajeErrorHTTP ) then
             begin
                  if InitAutorizacion then
                  begin
                       FParametros := TZetaParams.Create;
                       try
                          if LoginBatch then
                          begin
                               ZAccesosMgr.LoadDerechos;
                               if( FPreNomina )then
                               begin
                                    if ZAccesosMgr.CheckDerecho( D_ASIS_PROC_CALC_PRENOMINA, GetPosDerecho( eTipoCalculo( FTipoCalculo ) ) ) then
                                    begin
                                         if CargaDatosPrenomina then
                                         begin
                                              with FParametros do
                                              begin
                                                   { Parametros de prenomina }

                                                   // US #12120: SKYWORKS - Problema con Tress Automatiza-Calculo Pre-nomina al cambiar de A�o en Sistema TRESS y no en la Computadora
                                                   // AddInteger( 'Year', TheYear( FFechaPrenomina ) );
                                                   AddInteger( 'Year', FYear );

                                                   AddInteger( 'Tipo', FTipoPeriodo );
                                                   AddInteger( 'Numero', FNumero );
                                                   GetParametrosRango;
                                                   { Parametros finales }
                                                   AddInteger( 'TipoCalculo', FTipoCalculo );
                                                   AddBoolean( 'PuedeCambiarTarjeta', ZAccesosMgr.CheckDerecho( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES ) );
                                                   CalcularPrenomina( VarValues );
                                              end;
                                         end;
                                    end
                                    else
                                        LogError( 'Usuario sin derecho para ejecutar proceso de calcular Pren�mina' );
                               end;
                               if ( FSalIntegrado )then
                               begin
                                    if ZAccesosMgr.Revisa(D_EMP_PROC_RECALC_INT) then
                                    begin
                                         BitacoraSalIntegrado;
                                         dmCliente.CargaActivosTodos(FParametros);
                                         with FParametros do
                                         begin
                                              { Parametros de Salario integrado }
                                              AddDate( 'Fecha', FFechaReferen );
                                              AddBoolean( 'Referencia', FReferencia );
                                              AddBoolean( 'NoIncluirIncapacitados', FNoIncluirIncapa );
                                              AddBoolean( 'AvisoIMSSFechaCambio', FAvisoImss );
                                              AddDate( 'Fecha2', FFechaEspecImss );
                                              GetParametrosRango;
                                              AddString( 'Descripcion', 'Rec�lculo de Salarios Integrados' );
                                              AddMemo( 'Observaciones', VACIO);

                                              CalcularSalarioIntegrado( VarValues );
                                         end;
                                    end
                                    else
                                        LogError( 'Usuario sin derecho para ejecutar proceso de calcular Salario Integrado' );
                               end;
                               if ( FRecalcTarjetas )then
                               begin
                                    // (JB) Se modifica la revision de permisos ya que D_ASIS_PROC_RECALC_TARJETA es un permiso especial
                                    //      Y necesitara revisar tambien los permisos hijos que tiene.
                                    if ZAccesosMgr.RevisaCualquiera( D_ASIS_PROC_RECALC_TARJETA )
                                    then
                                    begin
                                         if PuedeCambiarTarjeta( FRecFechaInicial,FRecFechaFin,sMensaje )then
                                         begin
                                              BitacoraRecalcTarjetas;
                                              dmCliente.CargaActivosTodos( FParametros );
                                              with FParametros do
                                              begin
                                                   { Parametros de Salario integrado }
                                                   AddDate( 'FechaInicial',FRecFechaInicial );
                                                   AddDate( 'FechaFinal', FRecFechaFin );

                                                   // (JB) Se agregan las verificaciones de los derechos de acceso para Revisar Clasificacion de Tarjeta
                                                   //      Se revisa con el primer bit llamado K_DERECHO_CONSULTA
                                                   if ( ZAccesosMgr.CheckDerecho(D_ASIS_PROC_RECALC_TARJETA, K_DERECHO_CONSULTA ) ) then
                                                   begin
                                                        AddBoolean( 'RevisarHorarios', FRecRevisarHorarios );
                                                        AddBoolean( 'Reclasificar', FRecRevisarPuestos );
                                                   end
                                                   else
                                                   begin
                                                        AddBoolean( 'RevisarHorarios', False );
                                                        AddBoolean( 'Reclasificar', False );
                                                        sMensaje := sMensaje + 'Usuario sin derecho para ejecutar : Revisar Clasificaci�n de Tarjeta' + CR_LF;
                                                   end;

                                                   // (JB) Se agregan las verificaciones de los derechos de acceso para Efectuar Cancelaciones
                                                   //      Se revisa con el segundo bit llamado K_DERECHO_ALTA
                                                   if ( ZAccesosMgr.CheckDerecho(D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_ALTA ) ) then
                                                   begin
                                                        AddBoolean( 'CancelarTemporales', FRecCancelarHorarios );
                                                        AddBoolean( 'CancelarExtras', FRecCancelarAutExtras );
                                                        AddBoolean( 'CancelarDescansos', FRecCancelarAutDescan  );
                                                        AddBoolean( 'CancelarPermisosConGoce', FRecCancelarAutPerCGoce );
                                                        AddBoolean( 'CancelarPermisosSinGoce', FRecCancelarAutPerSGoce );
                                                        AddBoolean( 'CancelarHorasPrepagadas', FRecCancelarAutExtPrep );
                                                   end
                                                   else
                                                   begin
                                                        AddBoolean( 'CancelarTemporales', False );
                                                        AddBoolean( 'CancelarExtras', False );
                                                        AddBoolean( 'CancelarDescansos', False  );
                                                        AddBoolean( 'CancelarPermisosConGoce', False );
                                                        AddBoolean( 'CancelarPermisosSinGoce', False );
                                                        AddBoolean( 'CancelarHorasPrepagadas', False );
                                                        sMensaje := sMensaje + 'Usuario sin derecho para ejecutar : Efectuar Cancelaciones' + CR_LF;
                                                   end;

                                                   // (JB) Se agregan las verificaciones de los derechos de acceso para Reprocesar Tarjetas
                                                   //      Se revisa con el tercer bit llamado K_DERECHO_BAJA
                                                   if ( ZAccesosMgr.CheckDerecho(D_ASIS_PROC_RECALC_TARJETA,K_DERECHO_BAJA ) ) then
                                                        AddBoolean( 'ReprocesarTarjetas', FRecReprocesarTarjetas )
                                                   else
                                                   begin
                                                        AddBoolean( 'ReprocesarTarjetas', False ); //3
                                                        sMensaje := sMensaje + 'Usuario sin derecho para ejecutar : Reprocesar Tarjetas' + CR_LF ;
                                                   end;

                                                   { Este par�metro se inicializa porque lo necesita el proceso de tarjetas}
                                                   AddBoolean( 'PuedeCambiarTarjeta', ZAccesosMgr.CheckDerecho( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES ) );
                                                   AddBoolean( 'AprobarAutorizaciones', ZAccesosMgr.CheckDerecho( D_ASIS_DATOS_AUTORIZACIONES, K_DERECHO_SIST_KARDEX ) );
                                                   GetParametrosRango;
                                                   RecalcularTarjetas( VarValues );

                                                   // (JB) Se envia el mensaje a LOG, validando que tenga contenido
                                                   if ( StrLleno( sMensaje ) ) then
                                                        Log(sMensaje);
                                              end;
                                         end
                                         else
                                         begin
                                              LogError(sMensaje);
                                         end;
                                    end
                                    else
                                        LogError( 'Usuario sin derecho para ejecutar proceso recalcular tarjetas de asistencia ' );
                               end;

                               if( FCierreVacaciones )then
                               begin
                                    //JB Proceso de Cierre Vacaciones
                                    if ZAccesosMgr.Revisa( D_EMP_PROC_CIERRE_VACACIONES ) then
                                    begin
                                         if ( ValidaParametrosCierreVacaciones( sMensajeError ) ) then
                                         begin
                                              BitacoraCierreVacaciones;
                                              dmCliente.CargaActivosTodos( FParametros );
                                              with FParametros do
                                              begin
                                                   { Parametros de cierre de Vacaciones }
                                                   AddBoolean( 'PorAniversario', ( StrToFloatDef( FFechaRef, 0 ) = 0 ) );
                                                   AddDate( 'Fecha', Date + StrToIntDef( FFechaCierre, 0 ) );

                                                   AddBoolean( 'GozarSaldo', not StrLleno( FCierreDiasGozo ) );
                                                   AddString( 'GozarDias', FCierreDiasGozo );

                                                   AddBoolean( 'PagarSaldo', not StrLleno( FCierreDiasPago ) );
                                                   AddString( 'PagarDias', FCierreDiasPago );

                                                   AddBoolean( 'PrimaSaldo', not StrLleno( FCierreDiasPrima ) );
                                                   AddString( 'PrimaDias', FCierreDiasPrima );

                                                   AddString( 'Observaciones', FObservaciones );

                                                   GetParametrosRango;
                                                   CierraVacaciones( VarValues );
                                              end;
                                         end
                                         else
                                              LogError( sMensajeError );
                                    end
                                    else
                                        LogError( 'Usuario sin derecho para ejecutar proceso de Cierre de Vacaciones' );
                               end;

                               FBitacoraMail.Add( Format( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ), [ FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now )]  ));
                               EnviarCorreos;
                          end;
                       finally
                              FParametros.Free;
                       end;
                  end
                  else
                  begin
                       LogError( 'El M�dulo De ' + Autorizacion.GetModuloStr( GetModulo ) + ' No Est� Autorizado' + CR_LF + 'Consulte A Su Distribuidor' );
                  end;
             end
             else
             begin
                  LogError( sMensajeErrorHTTP );
             end;
        end
        else
        begin
             LogError( 'La L�nea de Comandos Est� Mal Configurada. Empresa ' + FEmpresa + ' No Existe' );
        end;
     finally
            LogEnd;
     end;

end;

procedure TdmAutomatiza.DespliegaAyuda( sComando: string );
begin
     WriteLn( '' );
     WriteLn( 'Formato para calcular pren�mina' );
     WriteLn( '===============================' );
     WriteLn( Format( '%s%s %s=<W> %s=<A> %s=<B> %s=<C>', [ sComando,
                                                                          P_OP_PRENOMINA,
                                                                          P_EMAILS,
                                                                          P_TIPO_CALCULO,
                                                                          P_FECHA_CALCULO,
                                                                          P_TIPO_NOMINA ] ) );
     WriteLn( '' );
     WriteLn( '*** Par�metros ***' );
     WriteLn( '' );
     WriteLn( '<X>: C�digo de empresa (COMPANY.CM_CODIGO)' );
     WriteLn( '<W>: Lista de usuarios a los que se enviar� el correo ( Valor Default = VACIO )' );
     WriteLn( '<A>: Tipo de c�lculo a realizar: Nueva = 0, Todos = 1' );
     WriteLn( '<B>: Fecha para el c�lculo:      Ayer = -1, Hoy = 0, Ma�ana = 1, etc.' );
     WriteLn( '<C>: Tipo de per�odo: (TPERIODO.TP_TIPO)' );
     WriteLn( '' );
     WriteLn( 'Formato para calcular Salario Integrado' );
     WriteLn( '===============================' );
     WriteLn( Format( '%s%s %s=<W> %s=<A> %s=<B> %s=<C> %s=<D> %s=<E>', [ sComando,
                                                                                P_OP_SAL_INTEG,
                                                                                P_EMAILS,
                                                                                P_FECHA_REFEREN,
                                                                                P_FECHA_CAMBIO,
                                                                                P_FEC_AVISO_IMSS,
                                                                                P_FEC_ESPEC_IMSS,
                                                                                P_INCLUIR_INCAPA ] ) );
     WriteLn( '' );
     WriteLn( '*** Par�metros ***' );
     WriteLn( '' );
     WriteLn( '<X>: C�digo de empresa (COMPANY.CM_CODIGO)' );
     WriteLn( '<W>: Lista de usuarios a los que se enviar� el correo ( Valor Default = VACIO )' );
     WriteLn( '<A>: Fecha de Referencia: Hoy = 0, Ayer = -1,Anteayer = -2,etc. Default = 0 ');
     WriteLn( '<B>: Fecha de Cambio: Misma fecha de Referencia = 0, Aniversario = 1 Default = 0');
     WriteLn( '<C>: Fecha de Aviso a IMSS: Misma fecha del cambio = 0, Fecha espec�fica = 1 Default = 0');
     WriteLn( '<D>: Fecha espec�fica a IMSS: Hoy = 0, Ayer = -1, Anteayer = -2, etc. Default = 0');
     WriteLn( '<E>: Incluir incapacitados: No = 0, S� = 1 Valor Default = 0 ');
     WriteLn( '' );
     WriteLn( '' );
     WriteLn( 'Formato para Rec�lculo Tarjetas de Asistencia' );
     WriteLn( '===============================' );
     WriteLn( Format( '%s%s %s=<W> %s=<A> %s=<B> %s=<C> %s=<D> %s=<E> %s=<F> %s=<G> %s=<H> %s=<I> %s=<J>  %s=<K>', [ sComando,
                                                                                P_OP_RECALCTARJE,
                                                                                P_EMAILS,
                                                                                P_FECHA_INI,
                                                                                P_FECHA_FIN ,
                                                                                P_REV_HORARIOS ,
                                                                                P_REV_PUESTO ,
                                                                                P_CAN_HORARIO,
                                                                                P_CAN_EXTRAS,
                                                                                P_CAN_DESCANSO,
                                                                                P_CAN_PERMCGOCE,
                                                                                P_CAN_PERMSGOCE,
                                                                                P_CAN_EXTPREP,
                                                                                P_CAN_REPROCESA ] ) );
     WriteLn( '' );
     WriteLn( '*** Par�metros ***' );
     WriteLn( '' );
     WriteLn( '<X>: C�digo de empresa (COMPANY.CM_CODIGO)' );
     WriteLn( '<W>: Lista de usuarios a los que se enviar� el correo ( Valor Default = VACIO )' );
     WriteLn( '<A>: Fecha Inicial: Hoy = 0, Ayer = -1, Anteayer = -2, etc. ( Default = 0  )');
     WriteLn( '<B>: Fecha Final: Hoy = 0, Ayer = -1, Anteayer = -2, etc. ( Default = 0  )');
     WriteLn( '<C>: Revisar Horarios: No = 0, S� = 1 ( Default = 1 )');
     WriteLn( '<D>: Revisar Puesto/Area: No = 0, S� = 1 ( Default = 0 )');
     WriteLn( '<E>: Cancelar Horarios Temporales: No = 0, S� = 1 ( Default = 0 )');
     WriteLn( '<F>: Cancelar Autorizaci�n de Horas Extras: No = 0, S� = 1 ( Default = 0 )');
     WriteLn( '<G>: Cancelar Autorizaci�n de Descanso Trabajado: No = 0,S� = 1 ( Default = 0 )');
     WriteLn( '<H>: Cancelar Autorizaci�n de Permisos Con Goce: No = 0, S� = 1 ( Default = 0 )');
     WriteLn( '<I>: Cancelar Autorizaci�n de Permisos Sin Goce: No = 0, S� = 1 ( Default = 0 )');
     WriteLn( '<J>: Cancelar Autorizaci�n de Extras Prepagadas: No = 0, S� = 1 ( Default = 0 )');
     WriteLn( '<K>: Reprocesar Tarjetas: No = 0, S� = 1 ( Default = 0 )');
     WriteLn( '' );
     WriteLn( '' );
     WriteLn( 'Formato para Cierre de Vacaciones' );
     WriteLn( '===============================' );
     WriteLn( Format( '%s%s %s=<W> %s=<A> %s=<B> %s=<C> %s=<D> %s=<E> %s=<F>', [ sComando, P_OP_CIERREVAC, P_EMAILS,
                                                                                 P_VAC_FECHAREF,
                                                                                 P_VAC_FECHACIERRE,
                                                                                 P_VAC_CIERREDIASGOZO,
                                                                                 P_VAC_CIERREDIASPAGO,
                                                                                 P_VAC_CIERREDIASPRIMA,
                                                                                 P_VAC_OBSERVACIONES ] ) );
     WriteLn( '' );
     WriteLn( '*** Parametros ***' );
     WriteLn( '' );
     WriteLn( '<X>: Codigo de empresa (COMPANY.CM_CODIGO)' );
     WriteLn( '<W>: Lista de usuarios a los que se enviara el correo ( Valor Default = VACIO )' );
     WriteLn( '<A>: Fecha de Referencia: Aniversario = 0, Referencia = 1 ( Default = 0  )' );
     WriteLn( '<B>: Fecha de Cierre: Hoy = 0, Ayer = -1, Anteayer = -2, etc. ( Default = 0  )' );
     WriteLn( '<C>: Cierre Dias de Gozo: Dias especificos ( Default: VACIO = Saldo a la Fecha )' );
     WriteLn( '<D>: Cierre Dias de Pago: Dias especificos ( Default: VACIO = Saldo a la Fecha )' );
     WriteLn( '<E>: Cierre Dias Prima: Dias especificos ( Default: VACIO = Saldo a la Fecha )' );
     WriteLn( '<F>: Observaciones: Comentario a anexar al movimiento ( Default = VACIO )' );
     WriteLn( '' );

end;


procedure TdmAutomatiza.Log( const sTexto: String );
begin
     FBitacora.WriteTexto( sTexto );
     WriteLn( sTexto );
     FBitacoraMail.Add( sTexto );
end;

procedure TdmAutomatiza.LogError( const sTexto: String );
begin
     Log( 'ERROR: ' + sTexto );
     Log( '' );
end;

procedure TdmAutomatiza.ParametrosPrepara;
const
	K_SI_REFERENCIA = 0;
var
   sOperacion: String;
begin
     with FListaParametros do
     begin
          FEmpresa   := Values[ P_EMPRESA ];
          sOperacion := Values[ P_OPERACION ];
          FUsuario   := Values[ P_USUARIO ];
          FClave     := Values[ P_CLAVE ];
          FEmails    := Values[ P_EMAILS ];

          FPrenomina     := False;
          FSalIntegrado  := False;
          FRecalcTarjetas := False;
          FCierreVacaciones := False;
          if ( sOperacion = P_OP_PRENOMINA ) then
          begin
               FPrenomina := True;
               FTipoCalculo := StrToIntDef( Values[ P_TIPO_CALCULO ], Ord( tcNueva ) );
               FFechaPrenomina := Date + StrToIntDef( Values[ P_FECHA_CALCULO ], 0 );
               FTipoPeriodo := StrToIntDef( Values[ P_TIPO_NOMINA ], Ord( tpSemanal ) );
               FOpcion := eoCalculaPrenomina;
          end
          else if ( sOperacion = P_OP_SAL_INTEG ) then
          begin
               FSalIntegrado   := True;
               FFechaReferen   := Date + StrToIntDef( Values[ P_FECHA_REFEREN ], 0 );
               FReferencia     := ( StrToIntDef( Values[ P_FECHA_CAMBIO ], 0 )= K_SI_REFERENCIA );
               FAvisoImss      := ( StrToIntDef( Values[ P_FEC_AVISO_IMSS ], 0 ) = K_SI_REFERENCIA );
               FNoIncluirIncapa  := ( StrToIntDef( Values[ P_INCLUIR_INCAPA ], 0 )  = K_SI_REFERENCIA );
               FFechaEspecImss := Date + StrToIntDef( Values[ P_FEC_ESPEC_IMSS ], 0 );
               FOpcion         := eoCalcularSalarioIntegrado;
          end
          else if ( sOperacion = P_OP_RECALCTARJE ) then
          begin
               FRecalcTarjetas     := True;
               FRecFechaInicial    := Date + StrToIntDef( Values[ P_FECHA_INI ], 0 );
               FRecFechaFin        := Date + StrToIntDef( Values[ P_FECHA_FIN ], 0 );
               FRecRevisarHorarios := IntToBool( StrToIntDef(Values[ P_REV_HORARIOS ], 1 ) );
               FRecRevisarPuestos  := IntToBool( StrToIntDef( Values[ P_REV_PUESTO ], 0 ) );
               FRecCancelarHorarios := IntToBool( StrToIntDef( Values[ P_CAN_HORARIO ], 0 ) );
               FRecCancelarAutExtras := IntToBool( StrToIntDef( Values[ P_CAN_EXTRAS ], 0 ) );
               FRecCancelarAutDescan := IntToBool( StrToIntDef( Values[ P_CAN_DESCANSO ], 0 ) );
               FRecCancelarAutPerCGoce := IntToBool( StrToIntDef( Values[ P_CAN_PERMCGOCE ], 0 ) );
               FRecCancelarAutPerSGoce := IntToBool( StrToIntDef( Values[ P_CAN_PERMSGOCE ], 0 ) );
               FRecCancelarAutExtPrep := IntToBool( StrToIntDef( Values[ P_CAN_EXTPREP ], 0 ) );
               FRecReprocesarTarjetas := IntToBool( StrToIntDef( Values[ P_CAN_REPROCESA ], 0 ) );
               FOpcion         := eoRecalcularTarjetas ;
          end
          else if ( sOperacion = P_OP_CIERREVAC ) then
          begin
               FCierreVacaciones := True;
               FFechaRef         := Values[ P_VAC_FECHAREF ]; 
               FFechaCierre      := Values[ P_VAC_FECHACIERRE ];
               FCierreDiasGozo   := Values[ P_VAC_CIERREDIASGOZO ];
               FCierreDiasPago   := Values[ P_VAC_CIERREDIASPAGO ];
               FCierreDiasPrima  := Values[ P_VAC_CIERREDIASPRIMA ];
               FObservaciones    := Values[ P_VAC_OBSERVACIONES ];
               FOpcion           := eoCierreVacaciones ;
          end
     end;
end;



function TdmAutomatiza.GetTipoPeriodos: string;
var
   i: eTipoPeriodo;
begin
     Result := VACIO;
     for i:= 0 to FArregloPeriodo.Count - 1 do
     begin
          Result := ConcatString( Result,ObtieneElemento( lfTipoPeriodo, Ord( i ) ) + ' = ' + IntToStr( Ord( i ) ), ', ' );
     end;
end;

procedure TdmAutomatiza.GetListaParametros;
var
   i: integer;
begin
     with FListaParametros do
     begin
          Clear;
          for i := 1 to ParamCount do
          begin
               // se modifico uppercase los parametros respetan minusculas
               Add( ParamStr( i ) );
          end;
     end;
end;

procedure TdmAutomatiza.BitacoraSalIntegrado;
begin
     with FBitacora do
     begin
          Log( '** Par�metros de Salario integrado **' );
          Log( Format( 'Fecha de referencia: %s', [FechaCorta( FFechaReferen ) ] ) );
          if ( FReferencia  ) then
              Log( 'Fecha de cambio:Misma de Fecha de Referencia' )
          else
              Log( 'Fecha de cambio: Fecha de Aniversario' );

          if (FAvisoImss ) then
              Log( 'Fecha de aviso a IMSS:   Misma fecha del cambio' )
          else
              Log( Format( 'Fecha de aviso a IMSS:        %s', [ FechaCorta( FFechaEspecImss ) ] ) );

          Log( Format( 'Fecha espec�fica: %s', [FechaCorta( FFechaEspecImss ) ] ) );

          if (FNoIncluirIncapa)then
              Log( 'Incluir incapacitados: No')
          else
              Log( 'Incluir incapacitados: Si');

          Log( StringOfChar( '-', 40 ) );
     end;
end;

procedure TdmAutomatiza.BitacoraRecalcTarjetas;
begin
     Log( '** Par�metros de recalcular tarjetas de asistencia **' + CR_LF );

     Log( Format( 'Fecha inicial: %s', [FechaCorta( FRecFechaInicial ) ] )  + CR_LF );

     Log( Format( 'Fecha final: %s', [FechaCorta( FRecFechaFin ) ] )  + CR_LF );

     Log( 'Revisar horarios: '+ BoolAsSiNo( FRecRevisarHorarios )  + CR_LF );

     Log( 'Revisar puesto/area: '+ BoolAsSiNo( FRecRevisarPuestos )  + CR_LF );

     Log( 'Cancelar horarios temporales: '+ BoolAsSiNo( FRecCancelarHorarios )  + CR_LF );

     Log( 'Cancelar autorizaci�n de horas extras: '+ BoolAsSiNo( FRecCancelarAutExtras )  + CR_LF );

     Log( 'Cancelar autorizaci�n de descanso trabajado: '+ BoolAsSiNo( FRecCancelarAutDescan ) + CR_LF );

     Log( 'Cancelar autorizaci�n de permisos con goce: '+ BoolAsSiNo( FRecCancelarAutPerCGoce ) + CR_LF );

     Log( 'Cancelar autorizaci�n de permisos sin goce: '+ BoolAsSiNo( FRecCancelarAutPerSGoce ) + CR_LF );

     Log( 'Cancelar autorizaci�n de extras prepagadas: '+ BoolAsSiNo( FRecCancelarAutExtPrep ) + CR_LF );

     Log( 'Reprocesar tarjetas: '+ BoolAsSiNo( FRecReprocesarTarjetas ) + CR_LF );

     Log( StringOfChar( '-', 40 ) );

end;

procedure TdmAutomatiza.BitacoraCierreVacaciones;
var
   sFechaReferencia, sDiasPorGozar, sDiasPorPagar, sDiasPrima : String;
   iValor : Extended;
begin
     with FBitacora do
     begin
          Log( '** Par�metros de Cierre de Vacaciones **' + CR_LF );
          iValor := StrToFloatDef( FFechaRef, 0 );
          if ( iValor = 0 ) then
               sFechaReferencia := 'Aniversario del Empleado'
          else
               sFechaReferencia := FechaCorta( Date + StrToIntDef( FFechaCierre, 0 ) );

          if not StrLleno( FCierreDiasGozo ) then
               sDiasPorGozar := 'Saldo a la Fecha'
          else
               sDiasPorGozar := 'Estos d�as ' + FCierreDiasGozo ;

          if not StrLleno( FCierreDiasPago ) then
               sDiasPorPagar := 'Saldo a la Fecha'
          else
               sDiasPorPagar := 'Estos d�as ' + FCierreDiasPago ;

          if not StrLleno( FCierreDiasPrima ) then
               sDiasPrima := 'Saldo a la Fecha'
          else
               sDiasPrima := 'Estos d�as ' + FCierreDiasPrima ;

          Log( 'Fecha de Cierre: ' + sFechaReferencia + CR_LF );
          Log( 'Cierre de D�as por Gozar: ' + sDiasPorGozar + CR_LF );
          Log( 'Cierre de D�as por Pagar: ' + sDiasPorPagar + CR_LF );
          Log( 'Cierre de D�as de Prima Vacacional: ' + sDiasPrima + CR_LF );
          Log( 'Observaciones: ' + FObservaciones + CR_LF );
          Log( StringOfChar( '-', 40 ) );
     end;
end;


function TdmAutomatiza.GetGlobalFormula( const iGlobal:Integer ):String;
begin
     cdsTemporal.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( K_QRY_GLOBAL ,[ iGlobal ] ) );
     Result := cdsTemporal.FieldByName('GL_FORMULA').AsString;
end;

procedure TdmAutomatiza.EnviarCorreos;
const
    NO_ENCRIPTADO = 'N';
    ENCRIPTADO = 'S';
    K_NO_REPLY = 'noreply@';
var
   oUsuariosMails: TStrings;
   sErrorMsg: string;
   sUsuarioMail: string;
   sClaveTemporal, sCorreoRemitente, sDescripRemitente, sSubject, sEncriptacion, sUsuario, sBody, sDirecErrores  : string;
   iSubtype: integer;
   oListaCorreos: OleVariant;

        function StreamToVariant(Stream: TStream): OleVariant;
        var
          p: Pointer;
        begin
          Result := VarArrayCreate([0, Stream.Size - 1], varByte);
          p := VarArrayLock(Result);
          try
            Stream.Position := 0;
            Stream.Read(p^, Stream.Size);
          finally
            VarArrayUnlock(Result);
          end;
        end;

        function StringlistToVariant(aStrlist: TStrings): OleVariant;
        var
          hStream: TStream;
        begin
          hStream := TMemoryStream.Create;
          try
            aStrList.SaveToStream(hStream);
            hStream.Seek(0,soFromBeginning);
            Result := StreamToVariant(hStream);
          finally
            hStream.Free;
          end;
        end;

begin
       sClaveTemporal := VACIO;
       sCorreoRemitente := VACIO;
       sDescripRemitente := VACIO;
       sSubject := VACIO;
       sEncriptacion := VACIO;
       sUsuario := VACIO;
       sBody := VACIO;
       sDirecErrores := VACIO;
       sUsuarioMail := VACIO;
       iSubtype := 0;
       SetOLEVariantToNull( oListaCorreos );
       if strLleno( FEmails ) then
       begin
            oUsuariosMails := TStringList.Create;
            try
               CrearListaUsuarios( FEmails, oUsuariosMails );
               if ( oUsuariosMails.Count > 0 ) then
               begin
                    try
                       try
                          // sCorreoRemitente := 'TressAutomatiza@dominio.com';
                          with dmCliente do
                          begin
                               GetNotificacionAdvertencia;
                               with DatosNotificacionAdvertencia do
                               begin
                                    sCorreoRemitente := K_NO_REPLY + ServidorCorreos;
                               end;
                          end;

                          sDescripRemitente := 'Sistema TRESS-Automatiza';
                          sSubject := 'Bit�cora de TRESS-Automatiza';
                          oListaCorreos := StringlistToVariant(oUsuariosMails);
                          sUsuario := VACIO;
                          sBody := FBitacoraMail.Text;
                          sEncriptacion := NO_ENCRIPTADO;
                          sDirecErrores := VACIO;
                          //Si no hay un usuario definido entonces debe mandarse una lista de correos
                          ServerSistema.EnviarCorreo(sUsuario, sBody, sCorreoRemitente, sDescripRemitente, sSubject, sEncriptacion, oListaCorreos, sDirecErrores, iSubtype);
                       finally
                       end;
                    except
                          on Error: Exception do
                          begin
                               LogError( Error.Message );
                          end;
                    end;
               end
               else
                   Writeln( 'No hay correos definidos para estos usuarios' );
            finally
                   FreeAndNil( oUsuariosMails);
            end;
       end;
end;

procedure TdmAutomatiza.CalcularSalarioIntegrado(Parametros: OleVariant);
begin
     Log( 'Inicio c�lculo de salario integrado' );
     try
        LogProcessEnd( ServerRecursos.SalarioIntegrado( dmCliente.Empresa, Parametros ) );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'Fin c�lculo de salario integrado' );
end;


procedure TdmAutomatiza.RecalcularTarjetas(Parametros: OleVariant);
begin
     Log( 'Inicio del rec�lculo de tarjetas de asistencia' );
     try
        LogProcessEnd( ServerCalcNomina.RecalcularTarjetas(dmCliente.Empresa,Parametros))
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'Fin del rec�lculo de tarjetas de asistencia' );
end;


function TdmAutomatiza.CrearListaUsuarios( sEmails: String; oUsuarios:TStrings ):TStrings;
var
   iIndice, iLongitud: Integer;
   oListaUsers: TStrings;
   lBloqueoSistema: WordBool;
begin
     cdsUsuarios.Data := ServerSistema.GetUsuarios(iLongitud,ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBloqueoSistema );
     oUsuarios.Clear;
     oListaUsers := TStringList.Create;
     try
        oListaUsers.CommaText := sEmails;
        for iIndice := 0 to ( oListaUsers.Count - 1 ) do
        begin
             try
                if cdsUsuarios.Locate('US_CODIGO', oListaUsers.Strings[ iIndice ], [] ) then
                   oUsuarios.Add( cdsUsuarios.FieldByName( 'US_EMAIL' ).AsString )
                else
                    LogError( 'Usuario: ' + oListaUsers.Strings[ iIndice ]+' no esta registrado en Sistema Tress' );
             except
                   on Error:Exception do
                   begin
                        LogError( 'C�digo de usuario: ' + oListaUsers.Strings[ iIndice ] + ' es inv�lido' );
                   end;
             end;
        end;
     finally
            FreeAndNil( oListaUsers );
     end;
     Result := oUsuarios;
end;

function TdmAutomatiza.GetUsuarioTressAutomatiza:Boolean; //Trae el US_CODIGO dela configuracion de TressAutomatiza
var
   iUsuario, iLongitud : Integer;
   lBloqueoSistema: WordBool;
begin
     Result := False;
     //Se conecta a usuarios
     cdsUsuarios.Conectar;
     cdsUsuarios.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBloqueoSistema );

     //Trae la clave 11
     dmCliente.GetSeguridad;
     iUsuario :=  dmCliente.DatosSeguridad.UsuarioTareasAutomaticas;

     if not (iUsuario = 0) then
          if cdsUsuarios.Locate('US_CODIGO',iUsuario, []) then
          begin
               //Esta configurado el usuario correctamente
               Result := True;
               dmCliente.Usuario := iUsuario;
          end
          else
               //Esta configurado un usuario que no existe
               LogError( Format ( 'Usuario: %d no est� registrado en Sistema Tress', [ iUsuario ] ) )
     else
          //No esta configurado ningun usuario
          LogError( 'No est� configurado Usuario para Tareas Autom�ticas!!' );
end;

procedure TdmAutomatiza.CierraVacaciones(Parametros: OleVariant);
begin
     Log( 'Inicio del Cierre de Vacaciones' );
     try
        LogProcessEnd( ServerRecursos.CierreGlobalVacaciones(dmCliente.Empresa,Parametros) );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'Fin del Cierre de Vacaciones' );
end;

end.

