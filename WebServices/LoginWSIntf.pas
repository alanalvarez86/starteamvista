{ Invokable interface IProcesosWS }

unit LoginWSIntf;

interface

uses InvokeRegistry, Types, XSBuiltIns;

type

  { Invokable interfaces must derive from IInvokable }
  ILoginWS = interface(IInvokable)
  ['{3A525B48-5FB0-4F8F-B517-2E965E1F09E8}']

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function Echo( const Valores: WideString ): WideString; stdcall;
    function UsuarioLogin(const Parametros: WideString): WideString; stdcall;
    function CambiaClaveUsuario(const Parametros: string): WideString; stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(ILoginWS));

end.
 