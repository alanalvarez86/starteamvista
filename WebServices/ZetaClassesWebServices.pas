unit ZetaClassesWebServices;

interface

uses InvokeRegistry, Types, XSBuiltIns,
     SysUtils,
     dCliente;

type
  { T3Webservice }
  T3Webservice = class(TInvokableClass, IInvokable)
  private
    { Private declarations }
    FCliente: TdmCliente;
  protected
    { Protected declarations }
    property dmCliente: TdmCliente read FCliente;
    procedure ClienteInit;
    procedure ClienteRelease;
  public
    { Public declarations }
  end;

implementation

{ ******** T3Webservice ********* }

procedure T3Webservice.ClienteInit;
begin
     FCliente := TdmCliente.Create( nil );
end;

procedure T3Webservice.ClienteRelease;
begin
     FreeAndNil( FCliente );
end;

{ *********** Valores ************* }

end.
