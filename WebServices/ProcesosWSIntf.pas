{ Invokable interface IProcesosWS }

unit ProcesosWSIntf;

interface

uses InvokeRegistry, Types, XSBuiltIns;

type

  { Invokable interfaces must derive from IInvokable }
  IProcesosWS = interface(IInvokable)
  ['{3A525B48-5FB0-4F8F-B517-2E965E1F09E8}']

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function Echo( const Valores: WideString ): WideString; stdcall;
    function SalarioIntegrado( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaRecalcularTarjetas( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaPrenomina( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaAjusteColectivoDataset( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaAjusteColectivoEscribir( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaProcesarTarjetasSimple( const Parametros: WideString ): WideString; stdcall;
    function NominaCalcular( const Parametros: WideString ): WideString; stdcall;
    function NominaAfectar( const Parametros: WideString ): WideString; stdcall;
    function NominaDesafectar( const Parametros: WideString ): WideString; stdcall;
    function PromediarPercepcionesVariables( const Parametros: WideString ): WideString; stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IProcesosWS));

end.
 