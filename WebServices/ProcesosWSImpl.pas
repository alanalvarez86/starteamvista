{ Invokable implementation File for TProcesosWS which implements IProcesosWS }

unit ProcesosWSImpl;

interface

uses InvokeRegistry, Types, XSBuiltIns, ProcesosWSIntf, SysUtils,
     ZetaCommonLists,
     DProcesos;

type
  { TProcesosWS }
  TProcesosWS = class(TInvokableClass, IProcesosWS)
  private
    { Private declarations }
    dmProcesos : TdmProcesos;
    procedure ProcesosInit;
    procedure ProcesosRelease;
  public
    { Public declarations }
    function Echo( const Valores: WideString ): WideString; stdcall;
    function SalarioIntegrado( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaRecalcularTarjetas( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaPrenomina( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaAjusteColectivoDataset( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaAjusteColectivoEscribir( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaProcesarTarjetasSimple( const Parametros: WideString ): WideString; stdcall;
    function NominaCalcular( const Parametros: WideString ): WideString; stdcall;
    function NominaAfectar( const Parametros: WideString ): WideString; stdcall;
    function NominaDesafectar( const Parametros: WideString ): WideString; stdcall;
    function PromediarPercepcionesVariables( const Parametros: WideString ): WideString; stdcall;
  end;

implementation

uses DXMLTools;

procedure TProcesosWS.ProcesosInit;
begin
     dmProcesos := TdmProcesos.Create( nil );
end;

procedure TProcesosWS.ProcesosRelease;
begin
     FreeAndNil( dmProcesos );
end;

function TProcesosWS.Echo( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
begin
     Result := Format( '%s ( %s )', [ Valores, FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) ] );
end;

{-----------------------------
  Wizard de calcular salario integrado
------------------------------}
function TProcesosWS.SalarioIntegrado( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
           Result := dmProcesos.ProcesaWizard( prRHSalarioIntegrado, Parametros );
        finally
               ProcesosRelease;
        end;
     except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
  Wizard de Recalcular tarjetas
------------------------------ }
function TProcesosWS.AsistenciaRecalcularTarjetas( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
            Result := dmProcesos.ProcesaWizard( prASISRecalculoTarjetas, Parametros );
        finally
               ProcesosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------------------------------------------
    Generaci�n del dataset de los ajustes colectivos de asistencia
------------------------------------------------------------------}
function TProcesosWS.AsistenciaAjusteColectivoDataset( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
            Result := dmProcesos.AjusteColectivoLista( Parametros );
        finally
               ProcesosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{----------------------------------------------------
    Escritura de los ajustes colectivos de asistencia
----------------------------------------------------}
function TProcesosWS.AsistenciaAjusteColectivoEscribir( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
            Result := dmProcesos.AjusteColectivoEscribir( Parametros );
        finally
               ProcesosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Wizard de calcular pren�mina
------------------------------}
function TProcesosWS.AsistenciaPrenomina( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
           Result := dmProcesos.ProcesaWizard( prASISCalculoPreNomina, Parametros );
        finally
               ProcesosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
   Wizard de calcular la n�mina
------------------------------}
function TProcesosWS.NominaCalcular( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
          Result := dmProcesos.ProcesaWizard( prNOCalcular, Parametros );
        finally
               ProcesosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
   Wizard de afectar la n�mina
------------------------------}
function TProcesosWS.NominaAfectar( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
           Result := dmProcesos.ProcesaWizard( prNOAfectar, Parametros );
        finally
               ProcesosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
   Wizard de desafectar la n�mina
------------------------------}
function TProcesosWS.NominaDesafectar( const Parametros: WideString ): WideString; stdcall;
begin
     try
        ProcesosInit;
        try
          Result := dmProcesos.ProcesaWizard( prNODesafectar, Parametros );
        finally
               ProcesosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
   Wizard para promediar percepciones variables
------------------------------}
function TProcesosWS.PromediarPercepcionesVariables( const Parametros: WideString ): WideString;
begin
     try
       ProcesosInit;
       try
         Result := dmProcesos.ProcesaWizard( prRHPromediarVariables, Parametros );
       finally
              ProcesosRelease;
       end;
     except
          On Error: Exception do
          begin
               Result := DXMLTools.ExcepcionAsXML( Error );
          end;
    end;
end;

function TProcesosWS.AsistenciaProcesarTarjetasSimple(const Parametros: WideString): WideString;
begin
      try
       ProcesosInit;
       try
         Result := dmProcesos.ProcesaWizard( prASISProcesarTarjetas , Parametros );
       finally
              ProcesosRelease;
       end;
     except
          On Error: Exception do
          begin
               Result := DXMLTools.ExcepcionAsXML( Error );
          end;
    end;
end;

initialization
  { Invokable classes must be registered }
  InvRegistry.RegisterInvokableClass(TProcesosWS);

end.
