unit DTablas;

interface

uses Windows, Messages, SysUtils, Classes, Controls, Db, DBClient,
     DCliente,
     {$ifdef DOS_CAPAS}
     DServerTablas,
     {$else}
     Tablas_TLB,
     {$endif}
     ZetaClientDataSet;

type
  TdmTablas = class(TDataModule)
    cdsNivel1: TZetaLookupDataSet;
    cdsNivel2: TZetaLookupDataSet;
    cdsNivel3: TZetaLookupDataSet;
    cdsNivel4: TZetaLookupDataSet;
    cdsNivel5: TZetaLookupDataSet;
    cdsNivel6: TZetaLookupDataSet;
    cdsNivel7: TZetaLookupDataSet;
    cdsNivel8: TZetaLookupDataSet;
    cdsNivel9: TZetaLookupDataSet;
    cdsIncidencias: TZetaLookupDataSet;
    cdsMotAuto: TZetaLookupDataSet;
    cdsMovKardex: TZetaLookupDataSet;
    cdsNivel10: TZetaLookupDataSet;
    cdsNivel11: TZetaLookupDataSet;
    cdsNivel12: TZetaLookupDataSet;
    procedure cdsTablaAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FdmCliente: TdmCliente;
    {$ifdef DOS_CAPAS}
    function GetServerTablas: TdmServerTablas;
    {$else}
    FServidor: IdmServerTablasDisp;
    function GetServerTablas: IdmServerTablasDisp;
    {$endif}
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerTablas: TdmServerTablas read GetServerTablas;
    {$else}
    property ServerTablas: IdmServerTablasDisp read GetServerTablas;
    {$endif}
  public
    { Public declarations }
    property dmCliente: TdmCliente read FdmCliente write FdmCliente;
    function EsTipoSistema( const sTipo : String ): Boolean;
  end;

implementation

uses ZetaCommonTools;

{$R *.DFM}

{ TdmTablas }

{$ifdef DOS_CAPAS}
function TdmTablas.GetServerTablas: TdmServerTablas;
begin
     Result := DCliente.dmCliente.ServerTablas;
end;
{$else}
function TdmTablas.GetServerTablas: IdmServerTablasDisp;
begin
     Result := IdmServerTablasDisp( dmCliente.CreaServidor( CLASS_dmServerTablas, FServidor ) );
end;
{$endif}

procedure TdmTablas.cdsTablaAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerTablas.GetTabla( dmCliente.Empresa, Tag );
     end;
end;

function TdmTablas.EsTipoSistema(const sTipo: String): Boolean;
begin
     with cdsMovKardex do
     begin
          if Locate( 'TB_CODIGO', sTipo, [] ) then
             Result := zStrToBool( FieldByName( 'TB_SISTEMA' ).AsString )
          else
             Result := FALSE;
     end;
end;

end.
