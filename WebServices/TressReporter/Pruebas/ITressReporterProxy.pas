// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://dell-apache01/tressreporter/tressreporter.dll/wsdl/ITressReporter
// Encoding : utf-8
// Version  : 1.0
// (6/8/2007 9:25:38 AM - 1.33.2.5)
// ************************************************************************ //

unit ITressReporterProxy;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"


  // ************************************************************************ //
  // Namespace : urn:DTressReporterIntf-ITressReporter
  // soapAction: urn:DTressReporterIntf-ITressReporter#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : ITressReporterbinding
  // service   : ITressReporterservice
  // port      : ITressReporterPort
  // URL       : http://dell-apache01/tressreporter/tressreporter.dll/soap/ITressReporter
  // ************************************************************************ //
  ITressReporter = interface(IInvokable)
  ['{7A89AD25-0E06-7602-DD9F-02AF3F3C4915}']
    function  Echo(const Valores: WideString): WideString; stdcall;
    function  ReportAsASCII(const Valores: WideString): WideString; stdcall;
    function  ReportAsPDF(const Valores: WideString): TByteDynArray; stdcall;
    function  ReportAsXML(const Valores: WideString): WideString; stdcall;
    function  ReportParametersGetDefaults(const Valores: WideString): WideString; stdcall;
    function  ReportDataset( const Valores: WideString ): WideString; stdcall;
    function  ValidaFormula( const Valores: WideString ): WideString; stdcall;

  end;

function GetITressReporter(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): ITressReporter;


implementation

function GetITressReporter(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): ITressReporter;
const
  defWSDL = 'http://dell-apache01/tressreporter/tressreporter.dll/wsdl/ITressReporter';
  defURL  = 'http://dell-apache01/tressreporter/tressreporter.dll/soap/ITressReporter';
  defSvc  = 'ITressReporterservice';
  defPrt  = 'ITressReporterPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as ITressReporter);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(ITressReporter), 'urn:DTressReporterIntf-ITressReporter', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(ITressReporter), 'urn:DTressReporterIntf-ITressReporter#%operationName%');

end.