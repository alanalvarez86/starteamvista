unit FTressReporterTest;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, ExtCtrls, Mask, Types, ShellApi, ComCtrls, SOAPHTTPClient,
     DTressReporterImpl,
     ITressReporterProxy;

type
  TReportCaller = class(TForm)
    Panel1: TPanel;
    URLlbl: TLabel;
    URL: TEdit;
    Panel2: TPanel;
    btnReporteXML: TButton;
    UsarWebService: TCheckBox;
    PageControl: TPageControl;
    TabResultados: TTabSheet;
    TabDatos: TTabSheet;
    XMLResult: TMemo;
    XMLData: TMemo;
    btnReportePDF: TButton;
    btnParametros: TButton;
    btnBorrarDatos: TButton;
    XMLOpenDialog: TOpenDialog;
    btnCargar: TButton;
    btnReporteASCII: TButton;
    btnEcho: TButton;
    btnGetDataSet: TButton;
    btnValidaFormula: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnReporteXMLClick(Sender: TObject);
    procedure btnReportePDFClick(Sender: TObject);
    procedure btnParametrosClick(Sender: TObject);
    procedure btnBorrarDatosClick(Sender: TObject);
    procedure btnCargarClick(Sender: TObject);
    procedure btnReporteASCIIClick(Sender: TObject);
    procedure btnEchoClick(Sender: TObject);
    procedure btnGetDataSetClick(Sender: TObject);
    procedure btnValidaFormulaClick(Sender: TObject);
  private
    { Private declarations }
    FWebServiceProxy: THTTPRIO;
    FServicio: TTressReporter;
    function GetParametros: String;
    function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle; overload;
    function ExecuteFile( const FileName: String ): THandle; overload;
    procedure ServicioInit;
    procedure ServicioClear;
    procedure ShowResult(const XML: String);
    procedure WriteStreamToFile( const sFileName: String; const Datos: TByteDynArray );
    procedure ProcessASCIIOutput( const sFileName: String; const Datos: string );
    procedure ProcessStreamOutput(const sFileName: String; const Datos: TByteDynArray);
  protected
    { Protected declarations }
    function GetReporter: ITressReporter;
  public
    { Public declarations }
    procedure Debug( const MethodName: string; SOAPResponse: TStream );
  end;

var
  ReportCaller: TReportCaller;

implementation

uses ZetaServerTools;

{$R *.dfm}

procedure TReportCaller.FormCreate(Sender: TObject);
begin
     FWebServiceProxy := THTTPRIO.Create( Self );
end;

procedure TReportCaller.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FWebServiceProxy );
     if Assigned( FServicio ) then
     begin
          FreeAndNil( FServicio );
     end;
end;

procedure TReportCaller.ServicioInit;
begin
     if not Assigned( FServicio ) then
     begin
          FServicio := TTressReporter.Create;
     end;
end;

procedure TReportCaller.ServicioClear;
begin
     FreeAndNil(FServicio);
end;

procedure TReportCaller.Debug( const MethodName: string; SOAPResponse: TStream );
var
   Texto: TStrings;
begin
     Texto := TStringList.Create;
     try
        SOAPResponse.Position := 0;
        Texto.LoadFromStream( SOAPResponse );
        ShowMessage( MethodName + ':' + Texto.Text );
     finally
            FreeAndNil( Texto );
     end;
end;

function TReportCaller.GetParametros: String;
begin
     Result := XMLData.Lines.Text;
end;

function TReportCaller.GetReporter: ITressReporter;
begin
     Result := ITressReporterProxy.GetITressReporter( False, Self.URL.Text, Self.FWebServiceProxy );
end;

procedure TReportCaller.ShowResult( const XML: String );
begin
     with XMLResult do
     begin
          with Lines do
          begin
               Clear;
               BeginUpdate;
               try
                  Text := XML;
                  //SaveToFile('d:\temp\Reporte.xml');
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

function TReportCaller.ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;

function TReportCaller.ExecuteFile( const FileName: String ): THandle;
begin
     Result := ExecuteFile( FileName, '', SysUtils.ExtractFilePath( FileName ) );
end;

procedure TReportCaller.WriteStreamToFile( const sFileName: String; const Datos: TByteDynArray );
var
   FStream: TFileStream;
   i: Integer;
begin
     if ( Length( Datos ) > 0 ) then
     begin
          try
             if FileExists( sFileName ) then
             begin
                  DeleteFile( sFileName );
             end;
             FStream := TFileStream.Create( sFileName, fmCreate );
             try
                FStream.Position := 0;
                for i := 0 to ( Length( Datos ) ) do
                begin
                     FStream.WriteBuffer( Datos[ i ], 1 );
                end;
             finally
                    FStream.Free;
             end;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end
     else
     begin
          ShowMessage( 'No Hay Datos' );
     end;
end;

procedure TReportCaller.ProcessStreamOutput( const sFileName: String; const Datos: TByteDynArray );
var
   FWebStream: TWebServiceBinaryStream;
begin
     FWebStream := TWebServiceBinaryStream.Create;
     try
        if FWebStream.ErrorIs( Datos ) then
        begin
             Self.ShowResult( FWebStream.ErrorGet( Datos ) );
        end
        else
        begin
             Self.WriteStreamToFile( sFileName, Datos );
             if FileExists( sFileName ) then
             begin
                  ExecuteFile( sFileName );
             end;
        end;
     finally
            FreeAndNil( FWebStream );
     end;
end;

procedure TReportCaller.ProcessASCIIOutput( const sFileName: String; const Datos: string );
var
   FWebStream: TWebServiceStringReply;
   FStream: TStrings;
begin
     FWebStream := TWebServiceStringReply.Create;
     try
        if FWebStream.ErrorIs( Datos ) then
        begin
             Self.ShowResult( FWebStream.ErrorGet( Datos ) );
        end
        else
        begin
             if ( Length( Datos ) > 0 ) then
             begin
                  try
                     if FileExists( sFileName ) then
                     begin
                          DeleteFile( sFileName );
                     end;
                     FStream := TStringList.Create;
                     try
                        with FStream do
                        begin
                             Text := Datos;
                             SaveToFile( sFileName );
                        end;
                     finally
                            FStream.Free;
                     end;
                  except
                        on Error: Exception do
                        begin
                             Application.HandleException( Error );
                        end;
                  end;
                  if FileExists( sFileName ) then
                  begin
                       ExecuteFile( sFileName );
                  end;
             end
             else
             begin
                  ShowMessage( 'No Hay Datos' );
             end;
        end;
     finally
            FreeAndNil( FWebStream );
     end;
end;

{ ************ Eventos de botones ************* }

procedure TReportCaller.btnCargarClick(Sender: TObject);
var
   oCursor: TCursor;
   sArchivo: String;
begin
     with XMLOpenDialog do
     begin
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               sArchivo := FileName;
               if FileExists( sArchivo ) then
               begin
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       Self.XMLData.Lines.LoadFromFile( sArchivo );
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end;
     end;
end;

procedure TReportCaller.btnBorrarDatosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        XMLData.Lines.Clear;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportCaller.btnReporteXMLClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                ShowResult( GetReporter.ReportAsXML( GetParametros ) );
           end
           else
           begin
                ServicioInit;
                try
                   ShowResult( FServicio.ReportAsXML( GetParametros ) );
                finally
                       ServicioClear;
                end;
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportCaller.btnReportePDFClick(Sender: TObject);
var
   oCursor: TCursor;
   aData: TByteDynArray;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                aData := GetReporter.ReportAsPDF( GetParametros );
           end
           else
           begin
                ServicioInit;
                try
                   aData := FServicio.ReportAsPDF( GetParametros );
                finally
                       ServicioClear;
                end;
           end;
           ProcessStreamOutput( 'D:\Temp\archivo.pdf', aData );
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportCaller.btnParametrosClick(Sender: TObject);
var
   Valores: String;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                Valores := GetReporter.ReportParametersGetDefaults( GetParametros);
           end
           else
           begin
                ServicioInit;
                try
                   Valores := FServicio.ReportParametersGetDefaults( GetParametros );
                finally
                       ServicioClear;
                end;
           end;
           Self.ShowResult(Valores);
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportCaller.btnReporteASCIIClick(Sender: TObject);
var
   oCursor: TCursor;
   aData: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                aData := GetReporter.ReportAsASCII( GetParametros );
           end
           else
           begin
                ServicioInit;
                try
                   aData := FServicio.ReportAsASCII( GetParametros );
                finally
                       ServicioClear;
                end;
           end;
           Self.ShowResult(aData);
           ProcessASCIIOutput( 'D:\Temp\archivo.txt', aData );
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportCaller.btnEchoClick(Sender: TObject);
var
   oCursor: TCursor;
   aData: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                aData := GetReporter.Echo( GetParametros );
           end
           else
           begin
                ServicioInit;
                try
                   aData := FServicio.Echo( GetParametros );
                finally
                       ServicioClear;
                end;
           end;
           ShowMessage(aData);
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportCaller.btnGetDataSetClick(Sender: TObject);
var
   oCursor: TCursor;
   aData: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                aData := GetReporter.ReportDataset( GetParametros );
           end
           else
           begin
                ServicioInit;
                try
                   aData := FServicio.ReportDataset( GetParametros );
                finally
                       ServicioClear;
                end;
           end;
           Self.ShowResult(aData);
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportCaller.btnValidaFormulaClick(Sender: TObject);
var
   oCursor: TCursor;
   aData: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
               aData:= GetReporter.ValidaFormula( GetParametros );
           end
           else
           begin
                ServicioInit;
                try
                   aData := FServicio.ValidaFormula( GetParametros );
                finally
                       ServicioClear;
                end;
           end;
           Self.ShowResult(aData);
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
