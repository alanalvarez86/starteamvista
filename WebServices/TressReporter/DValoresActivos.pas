unit DValoresActivos;

interface

uses Types, SysUtils, Classes, Controls,
     ZetaCommonLists,
     ZetaCommonClasses,
     DXMLTools;

type
  TValoresActivosXMLDocument = class( TdmXMLTools )
  private
    { Private declarations }
    FLista: TStrings;
  protected
    { Protected declarations }
  public
    { Public declarations }
    destructor Destroy; override;
    procedure Load( const sValues: String );
    function ParseString(const sValues, sToken: String): String;
    function ParseInteger(const sValues, sToken: String): Integer;
    function ParseDate(const sValues, sToken: String): TDate;
  end;
  TRangoFechas = class( TObject )
  private
    { Private declarations }
    FFinal: TDate;
    FInicio: TDate;
  public
    { Public declarations }
    property Inicio: TDate read FInicio write FInicio;
    property Final: TDate read FFinal write FFinal;
    procedure LoadFromXML( const XMLDocument: TValoresActivosXMLDocument );
  end;
  TMes = class( TObject )
  private
    { Private declarations }
    FAnio: Integer;
    FNumero: Integer;
  public
    { Public declarations }
    property Numero: Integer read FNumero write FNumero;
    property Anio: Integer read FAnio write FAnio;
    procedure LoadFromXML( const XMLDocument: TValoresActivosXMLDocument );
  end;
  TValoresActivosXML = class( TObject )
  private
    { Private declarations }
    FEmpleado: TNumEmp;
    FPeriodo: TDatosPeriodo;
    FFecha: TDate;
    FRangoFechas: TRangoFechas;
    FMes: TMes;
    FAnio: Integer;
    FEmpresa: String;
    FTressUserName: String;
    FTressPassword: String;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Empresa: String read FEmpresa;
    property TressUserName: String read FTressUserName;
    property TressPassword: String read FTressPassword;
    property Empleado: TNumEmp read FEmpleado;
    property Periodo: TDatosPeriodo read FPeriodo;
    property Fecha: TDate read FFecha;
    property RangoFechas: TRangoFechas read FRangoFechas;
    property Mes: TMes read FMes;
    property Anio: Integer read FAnio;
    procedure LoadFromXML( const sXML: WideString );
  end;

implementation

uses ZetaCommonTools,
     ZetaServerTools;

const
     SECCION_PROPIEDADES = 'P';
     SECCION_EMPRESA = 'CIA';
     SECCION_EMPLEADO = 'EMP';
     SECCION_EMPLEADO_NUMERO = 'Numero';
     SECCION_PERIODO = 'PER';
     SECCION_PERIODO_YEAR = 'PYR';
     SECCION_PERIODO_TIPO = 'PTIPO';
     SECCION_PERIODO_NUMERO = 'PNUMERO';
     SECCION_FECHA = 'FEC';
     SECCION_RANGO_FECHAS = 'RAN';
     SECCION_RANGO_FECHAS_INICIAL = 'FechaInicial';
     SECCION_RANGO_FECHAS_FINAL = 'FechaFinal';
     SECCION_MES = 'MES';
     SECCION_MES_NUMERO = 'Numero';
     SECCION_MES_ANIO = 'Anio';
     SECCION_ANIO = 'YR';

{ ********* TValoresActivosXMLDocument ******** }

destructor TValoresActivosXMLDocument.Destroy;
begin
     if Assigned( FLista ) then
        FreeAndNil( FLista );
     inherited Destroy;
end;

procedure TValoresActivosXMLDocument.Load( const sValues: String );
const
     K_ROOT = 'DATOS';
begin
     Self.XMLAsText := Format( '<%0:s>%1:s</%0:s>', [ K_ROOT, sValues ] );
end;

function TValoresActivosXMLDocument.ParseString( const sValues, sToken: String ): String;
const
     K_DELIMITER = '|';
     K_SEPARADOR = ':';
var
   i, iPos: Integer;
   sValue: String;
begin
     Result := '';
     if not Assigned( FLista ) then
        FLista := TStringList.Create;
     with FLista do
     begin
          Clear;
          Delimiter := K_DELIMITER;
          DelimitedText := sValues;
          for i := 0 to ( Count - 1 ) do
          begin
               sValue := Strings[ i ];
               if ( Pos( sToken + K_SEPARADOR, sValue ) > 0 ) then
               begin
                    iPos := Pos( K_SEPARADOR, sValue );
                    Result := Copy( sValue, iPos + 1, ( Length( sValue ) - iPos ) );
                    Break;
               end;
          end;
     end;
end;

function TValoresActivosXMLDocument.ParseDate( const sValues, sToken: String ): TDate;
begin
     Result := StringToDate( ParseString( sValues, sToken ), NullDateTime );
end;

function TValoresActivosXMLDocument.ParseInteger( const sValues, sToken: String ): Integer;
begin
     Result := StrToIntDef( ParseString( sValues, sToken ), 0 );
end;

{ ********* TRangoFechas ****** }

procedure TRangoFechas.LoadFromXML( const XMLDocument: TValoresActivosXMLDocument );
begin
     with XMLDocument do
     begin
          Self.Inicio := ParseDate( TagAsString( GetNode( SECCION_PROPIEDADES, GetNode( SECCION_RANGO_FECHAS ) ), '' ), SECCION_RANGO_FECHAS_INICIAL );
          Self.Final := ParseDate( TagAsString( GetNode( SECCION_PROPIEDADES, GetNode( SECCION_RANGO_FECHAS ) ), '' ), SECCION_RANGO_FECHAS_FINAL );
     end;
end;

{ ****** TMes ******* }

procedure TMes.LoadFromXML( const XMLDocument: TValoresActivosXMLDocument );
begin
     with XMLDocument do
     begin
          Self.Numero := ParseInteger( TagAsString( GetNode( SECCION_PROPIEDADES, GetNode( SECCION_MES ) ), '' ), SECCION_MES_NUMERO );
          Self.Anio := ParseInteger( TagAsString( GetNode( SECCION_PROPIEDADES, GetNode( SECCION_MES ) ), '' ), SECCION_MES_ANIO );
     end;
end;

{ ********* TValoresActivosXML ********* }

constructor TValoresActivosXML.Create;
begin
     FRangoFechas := TRangoFechas.Create;
     FMes := TMes.Create;
     FEmpresa := '';
     FTressUserName := '';
     FTressPassword := '';
end;

destructor TValoresActivosXML.Destroy;
begin
     FreeAndNil( FMes );
     FreeAndNil( FRangoFechas );
     inherited Destroy;
end;

procedure TValoresActivosXML.LoadFromXML(const sXML: WideString);
var
   FXMLTools: TValoresActivosXMLDocument;
   Nodo: TZetaXMLNode;
   {$ifdef ANTES}
   sTipo: String;
   {$endif}
begin
     FXMLTools := TValoresActivosXMLDocument.Create( nil );
     try
        with FXMLTools do
        begin
             Load( sXML );
             Self.FEmpleado := ParseInteger( TagAsString( GetNode( SECCION_PROPIEDADES, GetNode( SECCION_EMPLEADO ) ), '' ), SECCION_EMPLEADO_NUMERO );
             Nodo := GetNode( SECCION_PERIODO );
             Self.FPeriodo.Year := TagAsInteger( GetNode( SECCION_PERIODO_YEAR, Nodo ), 0 );
             {$ifdef ANTES}
             sTipo := TagAsString( GetNode( SECCION_PERIODO_TIPO, Nodo ), 'tpDiario' );
             if ( sTipo = 'tpDiario' ) then
                Self.FPeriodo.Tipo := tpDiario
             else if ( sTipo = 'tpSemanal' ) then
                  Self.FPeriodo.Tipo := tpSemanal
             else if ( sTipo = 'tpCatorcenal' ) then
                  Self.FPeriodo.Tipo := tpCatorcenal
             else if ( sTipo = 'tpQuincenal' ) then
                  Self.FPeriodo.Tipo := tpQuincenal
             else if ( sTipo = 'tpMensual' ) then
                  Self.FPeriodo.Tipo := tpMensual
             else if ( sTipo = 'tpDecenal' ) then
                  Self.FPeriodo.Tipo := tpDecenal
             else if ( sTipo = 'tpSemanalA' ) then
                  Self.FPeriodo.Tipo := tpSemanalA
             else if ( sTipo = 'tpSemanalB' ) then
                  Self.FPeriodo.Tipo := tpSemanalB
             else if ( sTipo = 'tpQuincenalA' ) then
                  Self.FPeriodo.Tipo := tpQuincenalA
             else
                 Self.FPeriodo.Tipo := tpDiario;
             {$else}
             Self.FPeriodo.Tipo := eTipoPeriodo( TagAsInteger( GetNode( SECCION_PERIODO_TIPO, Nodo ), 0 ) );
             {$endif}
             Self.FPeriodo.Numero := TagAsInteger( GetNode( SECCION_PERIODO_NUMERO, Nodo ), 0 );
             Self.FFecha := TagAsDate( GetNode( SECCION_FECHA ), NullDateTime );
             Self.FAnio := TagAsInteger( GetNode( SECCION_ANIO ), 0 );
        end;
        Self.RangoFechas.LoadFromXML( FXMLTools );
        Self.Mes.LoadFromXML( FXMLTools );
     finally
            FreeAndNil( FXMLTools );
     end;
end;

end.
