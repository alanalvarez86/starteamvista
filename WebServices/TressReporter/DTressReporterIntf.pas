{ Invokable interface ITressReporter }

unit DTressReporterIntf;

interface

uses InvokeRegistry, Types, XSBuiltIns;

type
  { Invokable interfaces must derive from IInvokable }
  ITressReporter = interface(IInvokable)
  ['{E7704B20-6295-40BF-B369-1A21B0D806BF}']
    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function Echo( const Valores: WideString ): WideString; stdcall;
    function ReportAsASCII( const Valores: WideString ): WideString; stdcall;
    function ReportAsPDF( const Valores: WideString ): TByteDynArray; stdcall;
    function ReportAsXML( const Valores: WideString ): WideString; stdcall;
    function ReportDataset( const Valores: WideString ): WideString; stdcall;
    function ReportParametersGetDefaults( const Valores: WideString ): WideString; stdcall;
    function ValidaFormula( const Valores: WideString ): WideString; stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface( TypeInfo( ITressReporter ) );

end.
