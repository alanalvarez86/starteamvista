unit DReportesWebBase;

interface
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Variants, Classes,
     Graphics, Controls, Forms, Dialogs, DB, DBClient,
     DReportesGenerador,
     DXMLTools,
     DElementos,
     ZetaCommonLists,
     ZAgenteSQLClient,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZReportTools;

type
  TdmReportWebGeneratorBase = class(TdmReportGenerator)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FLogger: TStrings;
    FReporteInfo: TReporteData;
    FExpresiones: TStrings;
    function GetLogText: String;
    function GetSQLAgente: TSQLAgenteClient;
  protected
    { Protected declarations }
    FFiltroFormula: String;
    FFiltroDescripcion: String;
    function GetDerechosClasificacion( const eClasificacion: eClasifiReporte): Integer;
    function GetSobreTotales: Boolean;
    function ExisteCondicion: Boolean;
    procedure LogInit;
    procedure LogError(const sTexto: String; const lEnviar: Boolean);override;
    procedure ModificaListaCampos(Lista: TStrings);
    procedure ModificaListas;
    procedure ParametrosReporteadorPreparar;
    procedure AgenteSQLPreparar;
  public
    { Public declarations }
    property LogText: String read GetLogText;
    property ReporteInfo: TReporteData read FReporteInfo;
    property SQLAgente: TSQLAgenteClient read GetSQLAgente;
    function LogHayErrores: Boolean;
    function GenerarReporte( lUsaPlantilla: Boolean ): Boolean;
    procedure EvaluarParametros;
    procedure LoadReportFromXML(Reader: TdmXMLTools);
  end;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     DCliente,
     DCatalogos;

{$R *.dfm}

{ ************ TdmReportWebGeneratorBase ************** }

procedure TdmReportWebGeneratorBase.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FLogger := TStringList.Create;
     FExpresiones := TStringList.Create;
     FReporteInfo := TReporteData.Create( Self.Grupos, Self.Orden, Self.Filtros, Self.Campos,FExpresiones );
end;

procedure TdmReportWebGeneratorBase.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     if Assigned(FReporteInfo)then
        FreeAndNil( FReporteInfo );
     if Assigned(FLogger)then
        FreeAndNil( FLogger );
end;

function TdmReportWebGeneratorBase.GetSQLAgente: TSQLAgenteClient;
begin
     Result := Self.FSQLAgente;
end;

{ ********  Bitacora de erroes ************ }

function TdmReportWebGeneratorBase.LogHayErrores: Boolean;
begin
     Result := ( FLogger.Count > 0 );
end;

procedure TdmReportWebGeneratorBase.LogInit;
begin
     FLogger.Clear;
end;

procedure TdmReportWebGeneratorBase.LogError(const sTexto: String; const lEnviar: Boolean );
begin
     FLogger.Add(sTexto);
end;

function TdmReportWebGeneratorBase.GetLogText: String;
begin
     Result := FLogger.Text;
end;

{ *********** Cargado de datos ************** }

procedure TdmReportWebGeneratorBase.LoadReportFromXML(Reader: DXMLTools.TdmXMLTools);
begin
     Self.ReporteInfo.LoadFromXML(Reader);
end;

{ ******* M�todos para preparar invocaci�n al reporteador ******** }

function TdmReportWebGeneratorBase.GetDerechosClasificacion( const eClasificacion: eClasifiReporte): Integer;
begin
     {$ifdef FALSE}
     Result := dmDiccionario.GetDerechosClasifi(eClasificacion);
     {$else}
     Result := ZetaCommonClasses.K_DERECHO_CONSULTA;
     {$endif}
end;

function TdmReportWebGeneratorBase.ExisteCondicion: Boolean;
begin
     Result := False;
     if StrLleno( ReporteInfo.Condicion ) then
     begin
          with dmCatalogos do
          begin
               cdsCondiciones.Refrescar;
               if cdsCondiciones.Locate( 'QU_CODIGO', VarArrayOf( [ ReporteInfo.Condicion] ), [] ) then
               begin
                    Result := True;
               end;
          end;
     end;
end;

procedure TdmReportWebGeneratorBase.ParametrosReporteadorPreparar;
var
   DatasetCondiciones: TClientDataset;
begin
     dmCliente.CargaActivosTodos(Params);
     DatasetCondiciones := nil;
     if ExisteCondicion then
     begin
          DatasetCondiciones := dmCatalogos.cdsCondiciones;
     end;
     ZReportTools.ParametrosReportes( cdsReporte,
                                      DatasetCondiciones,
                                      Params,
                                      FFiltroFormula,
                                      FFiltroDescripcion,
                                      ReporteInfo.SoloTotales,
                                      TRUE,
                                      TRUE,
                                      ContieneImagenes );
     {28-Octubre: CV: Esta llamada se requiere por que los reportes especiales requieren
     agregar ciertos parametros especificos, al FParams. En el Servidor,
     se espera que lleguen estos parametros.}
     ZReportTools.ParametrosEspeciales( ReporteInfo.EntidadTipo,
                                        Params,
                                        Filtros,
                                        Grupos );
end;

procedure TdmReportWebGeneratorBase.AgenteSQLPreparar;
var
   i, j: Integer;
begin
     with SQLAgente do
     begin
          Clear;
          Parametros := ReporteInfo.ReporteParametros.AsVariant;
          Entidad := ReporteInfo.EntidadTipo;
     end;
     SoloTotales := ReporteInfo.SoloTotales;
     DatosImpresion := GetDatosImpresion;
     FFiltroFormula := VACIO;
     FFiltroDescripcion := VACIO;
     AgregaSQLFiltrosEspeciales( SQLAgente, ReporteInfo.Filtro, False, ReporteInfo.ReporteParametros.Count );
     if ExisteCondicion then
     begin
          AgregaSQLFiltrosEspeciales( SQLAgente, dmCatalogos.cdsCondiciones.FieldByName( 'QU_FILTRO' ).AsString, False, ReporteInfo.ReporteParametros.Count );
     end;
     with ReporteInfo do
     begin
          with ReporteColumnas do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    ZReportTools.AgregaSQLColumnas( SQLAgente, Campo[ i ], DatosImpresion, -1, ReporteParametros.Count );
               end;
          end;
          with ReporteGrupos do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    ZReportTools.AgregaSQLGrupos( SQLAgente, Grupo[ i ] );
                    with Grupo[ i ].ListaEncabezado do
                    begin
                         for j := 0 to ( Count - 1 ) do
                         begin
                              ZReportTools.AgregaSQLColumnas( SQLAgente, TCampoOpciones( Objects[ j ] ), DatosImpresion, ( i + 0 ), ReporteParametros.Count );
                         end;
                    end;
                    with Grupo[ i ].ListaPie do
                    begin
                         for j := 0 to ( Count - 1 ) do
                         begin
                              ZReportTools.AgregaSQLColumnas( SQLAgente, TCampoOpciones( Objects[ j ] ), DatosImpresion, ( i + 0 ), ReporteParametros.Count );
                         end;
                    end;
               end;
          end;
          with ReporteOrdenes do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    ZReportTools.AgregaSQLOrdenes( SQLAgente, Orden[ i ] );
               end;
          end;
          with ReporteFiltros do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    ZReportTools.AgregaSQLFiltros( SQLAgente, Filtro[ i ], FFiltroFormula, FFiltroDescripcion );
               end;
          end;
          {Se agregan las expresiones como columnas para que se evaluen }
          with Expresiones do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                   Expresion[i].PosAgente := SQLAgente.AgregaColumna(   TransfomaParams( Expresion[i].Formula, ReporteParametros.Count, SQLAgente.Parametros ),
                                                                        false,
                                                                        SQLAgente.Entidad,
                                                                        tgAutomatico,
                                                                        255,
                                                                        Expresion[i].Campo);//Nombre del campo alias
               end;
          end;
     end;
     {$ifdef FALSE}
     AgregaFiltroSupervisor( SQLAgente );
     {$endif}
end;

procedure TdmReportWebGeneratorBase.ModificaListaCampos( Lista: TStrings );
var
   i: Integer;
begin
     for i := 0 to ( Lista.Count - 1 ) do
     begin
          with TCampoOpciones( Lista.Objects[ i ] ) do
          begin
               if ( PosAgente >= 0 ) then
               begin
                    SQLColumna := Self.SQLAgente.GetColumna(PosAgente);
                    with Self.SQLAgente.GetColumna(PosAgente) do
                    begin
                         TipoImp := TipoFormula;
                         OpImp := Totalizacion;
                    end;
               end;
          end;
     end;
end;



procedure TdmReportWebGeneratorBase.ModificaListas;
var
   i: Integer;
begin
     ModificaListaCampos( Self.Campos );
     for i := 0 to ( Self.Grupos.Count - 1 ) do
     begin
          with TGrupoOpciones( Self.Grupos.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
end;

function TdmReportWebGeneratorBase.GetSobreTotales: Boolean;
var
   i: Integer;
begin
     Result := FALSE;
     for i := 0 to Campos.Count - 1 do
     begin
          with TCampoListado( Campos.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion = ocSobreTotales);
          end;
     end;
end;

{ ********************** M�todos p�blicos ************************ }

procedure TdmReportWebGeneratorBase.EvaluarParametros;
var
   i: Integer;
   oParametros, oSQLAgente: OleVariant;
   Resultado: Boolean;
   sError: WideString;
   oColumna: TSQLColumna;
begin
     LogInit;
     if ( ReporteInfo.ReporteParametros.Count > 0 ) then
     begin

          SQLAgente.Clear;
          for i := 0 to ( ReporteInfo.ReporteParametros.Count - 1 ) do
          begin
               ZReportTools.AgregaSQLColumnasParam( SQLAgente, ReporteInfo.ReporteParametros.Campo[ i ] );
          end;
          oSQLAgente := SQLAgente.AgenteToVariant;
          dmCliente.CargaActivosTodos(Params);
          Params.AddBoolean( 'RDD_APP', FALSE );
          oParametros := Params.VarValues;
          Resultado := ServerReportesBDE.EvaluaParam( dmCliente.Empresa, oSQLAgente, oParametros, sError );
          if Resultado then
          begin
               SQLAgente.VariantToAgente( oSQLAgente );
               for i := 0 to ( ReporteInfo.ReporteParametros.Count - 1 ) do
               begin
                    with ReporteInfo.ReporteParametros.Campo[ i ] do
                    begin
                         if ( PosAgente >= 0 ) then
                         begin
                              oColumna := SQLAgente.GetColumna( PosAgente );
                              TipoCampo := oColumna.TipoFormula;
                              Formula := oColumna.Formula;
                         end;
                    end;
               end;
          end
          else
          begin
               LogError( sError, True );
          end;
     end;
end;

function TdmReportWebGeneratorBase.GenerarReporte( lUsaPlantilla: Boolean ): Boolean;
const
     ES_REPORTE_ESPECIAL = 'Reporte Especial :';
var
   Error: WideString;
begin
     Result := GetReportes( ReporteInfo.Codigo );
     DoOnGetReportes( Result );
     if Result then
     begin
          AgenteSQLPreparar;
          DoBeforeGetResultado;
          if lUsaPlantilla and ( ReporteInfo.Formato <> tfMailMerge ) then
          begin
               Result := PreparaPlantilla( Error );
          end;
          ParametrosReporteadorPreparar;
          if Result then
          begin
               try
                  Result := GeneraSQL( Error );
                  if Result and ( StrVacio( Error ) or ( Error = ES_REPORTE_ESPECIAL ) )then
                  begin
                       DoOnGetResultado( Result, Error );
                  end
                  else
                  begin
                       if StrLleno( Error ) then
                       begin
                            LogError( Error, TRUE );
                       end;
                  end;
               finally
                      if lUsaPlantilla then
                      begin
                           DespreparaPlantilla;
                      end;
                      DoAfterGetResultado( Result, Error );
               end;
          end
          else
          begin
               LogError( Format( 'Problemas Al Cargar Plantilla del Reporte #%d' + CR_LF + Error, [ ReporteInfo.Codigo ] ), TRUE );
          end;
     end
     else
     begin
          LogError( Format( 'El Reporte #%d No Existe', [ ReporteInfo.Codigo ] ), TRUE );
     end;
end;

end.
