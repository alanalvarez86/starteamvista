library TressReporter;

uses
  MidasLib,
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIThreadPool,
  DTressReporter in 'DTressReporter.pas' {dmTressReporter: TWebModule},
  DTressReporterIntf in 'DTressReporterIntf.pas',
  DTressReporterImpl in 'DTressReporterImpl.pas',
  DReportesGenerador in '..\..\Datamodules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DReportesWebBase in 'DReportesWebBase.pas' {dmReportWebGeneratorBase: TDataModule};

{$R *.res}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  {
  GA: No se puede usar debido a que marca error al invocarse el web-service
  ComObj.OleCheck( ActiveX.CoInitialize( nil ) );
  }
  Application.Initialize;
  Application.CreateForm(TdmTressReporter, dmTressReporter);
  Application.Run;
end.
