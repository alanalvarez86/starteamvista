unit ZetaAsciiFile;

interface

uses SysUtils, Controls, Windows, Classes, DB, DBClient, Dialogs,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TString = String[ 1 ];
  TAsciiExport = class;
  TASCIIExportEvent = procedure( Sender: TAsciiExport; DataSet: TDataset ) of object;
  TAsciiServer = class( TObject )
  private
    { Private declarations }
    FData: String;
    FFileSize: Integer;
    FRecordSize: Integer;
    function GetEof: Boolean;
    function GetRecordCount: Integer;
  protected
    { Protected declarations }
    FBuffer: TextFile;
    FFileName: String;
    FUsed: Boolean;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Bytes: Integer read FFileSize;
    property Data: String read FData;
    property EndOfFile: Boolean read GetEof;
    property FileName: String read FFileName;
    property RecordSize: Integer read FRecordSize;
    property RecordCount: Integer read GetRecordCount;
    property FileSize: Integer read FFileSize;
    property Used: Boolean read FUsed;
    function Open( const sFileName: String ): Boolean;
    function Read: Boolean;
    procedure Close;
    procedure Borrar;
  end;
  TAsciiLog = class( {$ifdef XMLREPORT}TObject{$else}TAsciiServer{$endif} )
  private
    { Private declarations }
    FErrors: Integer;
    FEvents: Integer;
    {$ifndef XMLREPORT}
    function GetTimeStamp: String;
    {$endif}
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Errors: Integer read FErrors;
    property Events: Integer read FEvents;
    procedure Init( const sFileName: String );
    procedure WriteEvent(const iEmpleado: TNumEmp; const sMensaje: String);
    procedure WriteException(const iEmpleado: TNumEmp; const sMensaje: String; Error: Exception);
    procedure WriteTexto(const sTexto: String); dynamic;
    procedure WriteTimeStamp( const sText: String );
  end;
  TAsciiOut = class( TObject )
  private
    { Private declarations }
    FRenglones: Integer;
    FOEMConvert: Boolean;
    FName: String;
    FFile: TextFile;
    FRenglon: String;
    FSeparador: TString;
    FDelimitador: TString;
    FUsed: Boolean;
  {$ifdef XMLREPORT}
  protected
    { Protected declarations }
    property Used: Boolean read FUsed write FUsed;
    procedure RenglonesIncrement;
  {$endif}
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Delimitador: TString read FDelimitador write FDelimitador;
    property Name: String read FName;
    property OEMConvert: Boolean read FOEMConvert write FOEMConvert;
    property Renglones: Integer read FRenglones;
    property Separador: TString read FSeparador write FSeparador;
    function Abre(const sFileName: String; const lCanOverwrite: Boolean): Boolean;{$ifdef XMLREPORT}virtual;{$endif}
    procedure AgregaRenglon( const sRenglon: String );{$ifdef XMLREPORT}virtual;{$endif}
    procedure Cierra;{$ifdef XMLREPORT}virtual;{$endif}
    procedure RenglonBegin;
    procedure RenglonEnd;
    procedure AgregaCampoString( const sTexto: String );
    procedure AgregaCampoInteger( const iNumero: Integer );
    procedure AgregaCampoFloat( const rNumero: TPesos );
    procedure AgregaCampoFecha( const dFecha: TDate );
    procedure AgregaFijoString( const sTexto: String; const iAncho: Integer );
    procedure AgregaFijoInteger( const iNumero, iAncho: Integer );
    procedure AgregaFijoFloat( const rNumero: TPesos; const iAncho: Integer );
    procedure AgregaFijoFecha( const dFecha: TDate; const iAncho: Integer );
    procedure AddCampo( const sTexto: String );
  end;
  {$ifdef XMLREPORT}
  TAsciiStream = class( TAsciiOut )
  private
    { Private declarations }
    FStream: TStrings;
    procedure SetStream( Value: TStrings );
  protected
    { Protected declarations }
    procedure Writeln( const Texto: pChar ); overload;
    procedure Writeln( const Texto: String ); overload;
  public
    { Public declarations }
    property Stream: TStrings read FStream write SetStream;
    constructor Create;
    destructor Destroy; override;
    procedure AgregaRenglon( const sRenglon: String ); override;
    procedure Cierra; override;
  end;
  {$endif}
  TAsciiFile = class( TAsciiOut )
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create( const sFileName: String );
  end;
  TAsciiExportItem = class( TObject )
  private
    { Private declarations }
    FAncho: Word;
    FDatos: String;
    FNombre: String;
    FOwner: TList;
    FTipo: eTipoGlobal;
  public
    { Public declarations }
    constructor Create(const Lista: TList);
    destructor Destroy; override;
    property Ancho: Word read FAncho write FAncho;
    property Datos: String read FDatos write FDatos;
    property Nombre: String read FNombre write FNombre;
    property Tipo: eTipoGlobal read FTipo write FTipo;
  end;
  TAsciiExport = class( TAsciiOut )
  private
    { Private declarations }
    FAlExportarColumnas: TASCIIExportEvent;
    FLista: TList;
    FStatus: String;
    function GetColumn( Index: Integer ): TAsciiExportItem;
    function ColumnCount: Integer;
    procedure SetError( const sMensaje: String; Error: Exception );
    procedure SetStatus( const sValue: String );
  public
    { Public declarations }
    constructor Create; reintroduce; overload;
    destructor Destroy; override;
    property AlExportarColumnas: TASCIIExportEvent read FAlExportarColumnas write FAlExportarColumnas;
    property Column[ Index: Integer ]: TAsciiExportItem read GetColumn;
    property Status: String read FStatus;
    function ColumnByName( const sNombre: String ): TAsciiExportItem;
    function Exporta(const eFormato: eFormatoASCII; Dataset: TDataset): Integer;
    function HayError: Boolean;
    procedure AgregaColumna( const sNombre: String; const eTipo: eTipoGlobal; const iAncho: Integer );
    procedure Clear;
    procedure EscribeRenglon( const eFormato: eFormatoASCII );
  end;

implementation

uses ZetaCommonTools,
     ZetaDialogo;

{ ****************** TAsciiServer ************ }

{$I+}         { Input/Output-Checking Directive }

constructor TAsciiServer.Create;
begin
     FUsed := False;
     FFileName := '';
end;

destructor TAsciiServer.Destroy;
begin
     if Used then
        Close;
     inherited Destroy;
end;

function TAsciiServer.Open( const sFileName: String ): Boolean;
var
   SearchRec: TSearchRec;
begin
     if FileExists( sFileName ) and ( FindFirst( sFileName, faAnyFile, SearchRec ) = 0 ) then
     begin
          try
             AssignFile( FBuffer, sFileName );
             Reset( FBuffer );
             FFileSize := SearchRec.Size;
             FUsed := True;
             FFileName := sFileName;
             Result := Read;
          except
                on Error: Exception do
                begin
                     CloseFile( FBuffer );
                     raise;
                end;
          end;
     end
     else
         raise Exception.Create( 'Archivo No Existe' );
end;

procedure TAsciiServer.Close;
begin
     if Used then
     begin
          CloseFile( FBuffer );
          FUsed := False;
     end;
end;

procedure TAsciiServer.Borrar;
begin
     Close;
     Erase( FBuffer );
end;

function TAsciiServer.GetEof: Boolean;
begin
     Result := Eof( FBuffer );
end;

function TAsciiServer.GetRecordCount: Integer;
begin
     if ( FRecordSize > 0 ) then
        Result := Trunc( FFileSize / FRecordSize )
     else
         Result := 0;
end;

function TAsciiServer.Read: Boolean;
begin
     if GetEof then
     begin
          FData := '';
          Result := False;
     end
     else
     begin
          Readln( FBuffer, FData );
          FRecordSize := Length( FData );
          Result := True;
     end;
end;

{ *********** TAsciiLog ********* }

{$ifdef XMLREPORT}
procedure TAsciiLog.Init(const sFileName: String);
begin
end;

procedure TAsciiLog.WriteEvent(const iEmpleado: TNumEmp; const sMensaje: String);
begin
end;

procedure TAsciiLog.WriteException(const iEmpleado: TNumEmp; const sMensaje: String; Error: Exception);
begin
end;

procedure TAsciiLog.WriteTexto(const sTexto: String);
begin
end;

procedure TAsciiLog.WriteTimeStamp(const sText: String);
begin
end;
{$else}
function TAsciiLog.GetTimeStamp: String;
begin
     Result := FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now );
end;

procedure TAsciiLog.Init( const sFileName: String );
begin
     try
        AssignFile( FBuffer, sFileName );
        if FileExists( sFileName ) then
           Append( FBuffer )
        else
            Rewrite( FBuffer );
        FUsed := True;
     except
           on Error: Exception do
           begin
                FUsed := False;
                CloseFile( FBuffer );
                raise;
           end;
     end;
     FFileName := sFileName;
     FErrors := 0;
     FEvents := 0;
end;

procedure TAsciiLog.WriteTexto( const sTexto: String );
begin
     Writeln( FBuffer, sTexto );
end;

procedure TAsciiLog.WriteEvent( const iEmpleado: TNumEmp; const sMensaje: String );
begin
     try
        if ( iEmpleado > 0 ) then
           WriteTexto( 'Empleado # ' + IntToStr( iEmpleado ) );
        WriteTexto( sMensaje );
        FEvents := FEvents + 1;
     except
           raise Exception.Create( 'Error al Escribir a Bitácora' + CR_LF + 'Verifique Derechos De Acceso Sobre El Directorio' );
     end;
end;

procedure TAsciiLog.WriteException( const iEmpleado: TNumEmp; const sMensaje: String; Error: Exception );
begin
     try
        WriteTexto( sMensaje );
        if Assigned( Error ) then
        begin
             if Error is EReconcileError then
             begin
                  with EReconcileError( Error ) do
                  begin
                       WriteTexto( Message );
                       WriteTexto( 'Error # ' + IntToStr( ErrorCode ) + ': ' + Context )
                  end;
             end
             else
             begin
                  if ( iEmpleado > 0 ) then
                     WriteTexto( 'Empleado # ' + IntToStr( iEmpleado ) );
                  WriteTexto( GetExceptionInfo( Error ) );
             end;
        end;
        FErrors := FErrors + 1;
     except
           raise Exception.Create( 'Error al Escribir a Bitácora' + CR_LF + 'Verifique Derechos De Acceso Sobre El Directorio' );
     end;
end;

procedure TAsciiLog.WriteTimeStamp( const sText: String );
begin
     WriteTexto( Format( sText, [ GetTimeStamp ] ) );
end;
{$endif}

{ ********** TAsciiOut *********** }

constructor TAsciiOut.Create;
begin
     FSeparador := ',';
     FDelimitador := '"';
     FOEMConvert := False;
     FRenglones := 0;
     FUsed := False;
end;

destructor TAsciiOut.Destroy;
begin
     Cierra;
     inherited;
end;

function TAsciiOut.Abre( const sFileName: String; const lCanOverwrite: Boolean ): Boolean;
begin
     FName := sFileName;
     try
        if FileExists( sFileName ) then
        begin
             if lCanOverwrite or ZetaDialogo.zConfirm( '¡ Archivo Ya Existe !', 'El Archivo ' + sFileName + ' Ya Existe' + CR_LF + '¿ Desea Sobreescribirlo ?', 0, mbYes ) then
             begin
                  Result := SysUtils.DeleteFile( sFileName );
             end
             else
                 Result := False;
        end
        else
            Result := True;
        if Result then
        begin
             AssignFile( FFile, FName );
             Rewrite( FFile );
        end;
        FUsed := Result;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '¡ Error Al Crear Archivo ASCII !', 'Error Al Abrir Archivo ' + sFileName, Error, 0 );
                Result := False;
           end;
     end;
end;

procedure TAsciiOut.Cierra;
begin
     if FUsed then
     begin
          CloseFile( FFile );
          FUsed := False;
     end;
end;

{$ifdef XMLREPORT}
procedure TAsciiOut.RenglonesIncrement;
begin
     Inc( FRenglones );
end;
{$endif}

procedure TAsciiOut.AgregaRenglon( const sRenglon: String );
 var pRenglon, pRenglon2 : PChar;
begin
     if OEMConvert then
     begin
          pRenglon := StrAlloc( Length( sRenglon ) + 1 );
          pRenglon2 := StrAlloc( Length( sRenglon ) + 1 );
          try
             pRenglon := StrPCopy( pRenglon, sRenglon );

             Windows.CharToOEM( pRenglon, pRenglon2 );
             Writeln( FFile, pRenglon2 );
          finally
                 StrDispose( pRenglon );
                 StrDispose( pRenglon2 );
          end;
     end
     else
         Writeln( FFile, sRenglon );

     FRenglones := FRenglones + 1;
end;

procedure TAsciiOut.RenglonBegin;
begin
     FRenglon := '';
end;

procedure TAsciiOut.RenglonEnd;
begin
     AgregaRenglon( FRenglon );
end;

procedure TAsciiOut.AgregaCampoString( const sTexto: String );
begin
     AddCampo( Delimitador + sTexto + Delimitador );
end;

procedure TAsciiOut.AgregaCampoInteger( const iNumero: Integer );
begin                          
     AddCampo( IntToStr( iNumero ) );
end;

procedure TAsciiOut.AgregaCampoFloat( const rNumero: TPesos );
begin
     AddCampo( FloatToStr( rNumero ) );
end;

procedure TAsciiOut.AgregaCampoFecha( const dFecha: TDate );
begin
     AddCampo( Delimitador + DateToStr( dFecha ) + Delimitador );
end;

procedure TAsciiOut.AgregaFijoString( const sTexto: String; const iAncho: Integer );
begin
     AddCampo( PadR( sTexto, iAncho ) );
end;

procedure TAsciiOut.AgregaFijoInteger( const iNumero, iAncho: Integer );
begin
     AddCampo( PadL( IntToStr( iNumero ), iAncho ) );
end;

procedure TAsciiOut.AgregaFijoFloat( const rNumero: TPesos; const iAncho: Integer );
begin
     AddCampo( PadL( FloatToStr( rNumero ), iAncho ) );
end;

procedure TAsciiOut.AgregaFijoFecha( const dFecha: TDate; const iAncho: Integer );
begin
     AddCampo( PadR( DateToStr( dFecha ), iAncho ) );
end;

procedure TAsciiOut.AddCampo( const sTexto: String );
begin
     if ( Length( FRenglon ) = 0 ) then
        FRenglon := sTexto
     else
         FRenglon := FRenglon + FSeparador + sTexto;
end;

{ ********** TAsciiFile ********** }

constructor TAsciiFile.Create( const sFileName: String );
begin
     inherited Create;
     Abre( sFileName, True );
end;

{$ifdef XMLREPORT}
{ ********** TAsciiStream ********** }

constructor TAsciiStream.Create;
begin
     inherited Create;
end;

destructor TAsciiStream.Destroy;
begin
     inherited Destroy;
end;

procedure TAsciiStream.Cierra;
begin
     if Used then
     begin
     end;
end;

procedure TAsciiStream.SetStream( Value: TStrings );
begin
     Self.FStream := Value;
     Self.Used := Assigned( Self.Stream );
end;

procedure TAsciiStream.AgregaRenglon( const sRenglon: String );
var
   pRenglon, pRenglon2: PChar;
begin
     if Used then
     begin
          if OEMConvert then
          begin
               pRenglon := StrAlloc( Length( sRenglon ) + 1 );
               try
                  pRenglon2 := StrAlloc( Length( sRenglon ) + 1 );
                  try
                     pRenglon := StrPCopy( pRenglon, sRenglon );
                     Windows.CharToOEM( pRenglon, pRenglon2 );
                     Writeln( pRenglon2 );
                  finally
                         StrDispose( pRenglon2 );
                  end;
               finally
                      StrDispose( pRenglon );
               end;
          end
          else
          begin
               Writeln( sRenglon );
          end;
          RenglonesIncrement;
     end;
end;

procedure TAsciiStream.Writeln( const Texto: pChar );
begin
     Writeln( StrPas( Texto ) );
end;

procedure TAsciiStream.Writeln( const Texto: String );
begin
     Self.Stream.Add( Texto );
end;
{$endif}

{ ********* TAsciiExportItem ********* }

constructor TAsciiExportItem.Create(const Lista: TList);
begin
     FOwner := Lista;
     FOwner.Add( Self );
end;

destructor TAsciiExportItem.Destroy;
begin
     FOwner.Remove( Self );
     inherited Destroy;
end;

{ ********* TAsciiExport ********* }

constructor TAsciiExport.Create;
begin
     inherited Create;
     FLista := TList.Create;
     FOEMConvert := True;
end;

destructor TAsciiExport.Destroy;
begin
     Clear;
     FreeAndNil( FLista );
     inherited Destroy;
end;

procedure TAsciiExport.Clear;
var
   i: Integer;
begin
     for i := ( ColumnCount - 1 ) to 0 do
     begin
          Column[ i ].Free;
     end;
end;

function TAsciiExport.ColumnByName( const sNombre: String ): TAsciiExportItem;
var
   i: Integer;
begin
     Result := nil;
     for i := 0 to ( ColumnCount - 1 ) do
     begin
          if ( Column[ i ].Nombre = sNombre ) then
          begin
               Result := Column[ i ];
               Break;
          end;
     end;
end;

function TAsciiExport.HayError: Boolean;
begin
     Result := ZetaCommonTools.StrLleno( FStatus );
end;

procedure TAsciiExport.SetStatus( const sValue: String );
begin
     FStatus := sValue;
end;

procedure TAsciiExport.SetError( const sMensaje: String; Error: Exception );
begin
     SetStatus( sMensaje + CR_LF + Error.Message );
end;

function TAsciiExport.GetColumn(Index: Integer): TAsciiExportItem;
begin
     if ( Index >= 0 ) and ( Index < ColumnCount ) then
        Result := TAsciiExportItem( FLista.Items[ Index ] )
     else
         Result := nil;
end;

function TAsciiExport.ColumnCount: Integer;
begin
     Result := FLista.Count;
end;

procedure TAsciiExport.AgregaColumna(const sNombre: String; const eTipo: eTipoGlobal; const iAncho: Integer);
begin
     with TAsciiExportItem.Create( FLista ) do
     begin
          Nombre := sNombre;
          Tipo := eTipo;
          Ancho := iAncho;
     end;
end;

function TAsciiExport.Exporta( const eFormato: eFormatoASCII; Dataset: TDataset ): Integer;
var
   lOk, lCanContinue: Boolean;
//   i: Integer;
begin
     SetStatus( '' );
     with Dataset do
     begin
          Active := True;
          First;
          Result := RecordCount;
          if ( Result <= 0 ) then
          begin
               SetStatus( 'No Hay Datos A Vaciar En El Archivo ASCII' );
          end
          else
          begin
               try
                  lCanContinue := True;
                  case eFormato of
                       faASCIIFijo:
                       begin
                            Separador := '';
                       end;
                       faASCIIDel:
                       begin
                            Separador := ',';
                       end;
                  end;
                  while not Eof and lCanContinue do
                  begin
                       lOk := False;
                       try
                          if Assigned( FAlExportarColumnas ) then
                             FAlExportarColumnas( Self, Dataset );
                          lOk := True;
                       except
                             on Error: Exception do
                             begin
                             end;
                       end;
                       if lOk then
                       begin
                            try
                               EscribeRenglon( eFormato );
{                               RenglonBegin;
                               for i := 0 to ( ColumnCount - 1 ) do
                               begin
                                    with Column[ i ] do
                                    begin
                                         case eFormato of
                                              faASCIIFijo:
                                              begin
                                                   case Tipo of
                                                        tgFloat: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                                                        tgNumero: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                                                        tgFecha: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                                                        tgTexto: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                                                   end;
                                              end;
                                              faASCIIDel:
                                              begin
                                                   case Tipo of
                                                        tgFloat: AddCampo( Datos );
                                                        tgNumero: AddCampo( Datos );
                                                        tgFecha: AddCampo( Delimitador + Datos + Delimitador );
                                                        tgTexto: AddCampo( Delimitador + Datos + Delimitador );
                                                   end;
                                              end;
                                         end;
                                    end;
                               end;
                               RenglonEnd; }
                            except
                                  on Error: Exception do
                                  begin
                                  end;
                            end;
                       end;
                       Next;
                  end;
               except
                     on Error: Exception do
                     begin
                          SetError( 'Error Al Exportar Archivo ' + Name, Error );
                     end;
               end;
          end;
          Active := False;
     end;
end;

procedure TAsciiExport.EscribeRenglon(const eFormato: eFormatoASCII);
var
   i : Integer;
begin
     RenglonBegin;
     for i := 0 to ( ColumnCount - 1 ) do
     begin
          with Column[ i ] do
          begin
               case eFormato of
                    faASCIIFijo:
                    begin
                         case Tipo of
                              tgFloat: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                              tgNumero: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                              tgFecha: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                              tgTexto: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                         end;
                    end;
                    faASCIIDel:
                    begin
                         case Tipo of
                              tgFloat: AddCampo( Datos );
                              tgNumero: AddCampo( Datos );
                              tgFecha: AddCampo( Delimitador + Datos + Delimitador );
                              tgTexto: AddCampo( Delimitador + Datos + Delimitador );
                         end;
                    end;
               end;
          end;
     end;
     RenglonEnd;
end;

{$I-}         { Input/Output-Checking Directive }

end.
