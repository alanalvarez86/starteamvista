program GeneraReporte;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  DGeneraReporte in 'DGeneraReporte.pas' {dmGeneraReporte: TDataModule};

begin
     try
        dmGeneraReporte := TdmGeneraReporte.Create(nil);
        with dmGeneraReporte do
        begin
             if ( ParamCount = 1 ) then 
                ReporteAsPDF;
        end;
     finally
            FreeAndNil(dmGeneraReporte);
     end;
end.
