unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
  DBaseTressDiccionario,
  DBaseDiccionario,ZetaCommonLists;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
  private
    { Private declarations }
  public
    { Public declarations }
    function GetValorActivo(const eValor: {$ifdef RDD}eRDDValorActivo{$else} string{$endif}): string;override;
  end;

threadvar
  dmDiccionario: TdmDiccionario;

implementation

uses DCliente,ZReportTools,ZetaCommonClasses;
{$R *.DFM}

function TdmDiccionario.GetValorActivo(const eValor: {$ifdef RDD}eRDDValorActivo{$else} string{$endif}):string;
begin
     {$ifdef RDD}
     with dmCliente do
     begin
          case eValor  of
               vaEmpleado: Result := IntToStr( ValoresActivos.Empleado );
               vaNumeroNomina: Result := IntToStr( ValoresActivos.Periodo.Numero );
               vaTipoNomina: Result := IntToStr( Ord( ValoresActivos.Periodo.Tipo ) );
               vaYearNomina: Result := IntToStr( ValoresActivos.Periodo.Year  )
          else
              Result := VACIO;
          end;
     end;
     {$else}
     with dmCliente do
     begin
          if ( eValor = K_EMPLEADO ) then
             Result := IntToStr( ValoresActivos.Empleado )
          else
              if ( eValor = K_YEAR ) then
                 Result := IntToStr( ValoresActivos.Periodo.Year )
              else
                  if ( eValor = K_TIPO ) then
                     Result := IntToStr( Ord( ValoresActivos.Periodo.Tipo ) )
                  else
                      if ( eValor = K_NUMERO ) then
                         Result := IntToStr( ValoresActivos.Periodo.Numero )
                      else
                          Result := eValor;
      end;
     {$endif}

end;


end.
