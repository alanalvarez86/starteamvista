unit DCliente;

interface
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     Variants,
     Consultas_TLB,
     Reportes_TLB,
     DXMLTools,
     DBasicoCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

type
  TDatabaseConnectionData = record
    DatabaseServer: String;
    UserName: String;
    Password: String;
  end;
  TValoresActivos = class( TObject )
  private
    { Private declarations }
    FAnio: Integer;
    FTressUserName: String;
    FEmpresaActiva: String;
    FTressPassword: String;
    FFecha: TDate;
    FFechaAsistencia: TDate;
    FPeriodo: TDatosPeriodo;
    FEmpleado: TNumEmp;
    FLiquidacionIMSS: TDatosIMSS;
    FUsuario: Integer;
    FDatabase: TDatabaseConnectionData;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property EmpresaActiva: String read FEmpresaActiva write FEmpresaActiva;
    property Database: TDatabaseConnectionData read FDatabase write FDatabase;
    property Usuario: Integer read FUsuario write FUsuario;
    property TressUserName: String read FTressUserName write FTressUserName;
    property TressPassword: String read FTressPassword write FTressPassword;
    property Empleado: TNumEmp read FEmpleado write FEmpleado;
    property Periodo: TDatosPeriodo read FPeriodo write FPeriodo;
    property LiquidacionIMSS: TDatosIMSS read FLiquidacionIMSS write FLiquidacionIMSS;
    property Fecha: TDate read FFecha write FFecha;
    property FechaAsistencia: TDate read FFechaAsistencia write FFechaAsistencia;
    property Anio: Integer read FAnio write FAnio;
    procedure CargarParametros(Parametros: TZetaParams);
  end;
  TdmCliente = class(TBasicoCliente)
    cdsReportes: TZetaClientDataSet;
    cdsQuery: TZetaClientDataSet;
    cdsPeriodo: TZetaClientDataSet;
    cdsLookupReportes: TZetaLookupDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsPatron: TZetaClientDataSet;
    cdsResultado: TZetaClientDataSet;
    procedure cdsReportesAlAdquirirDatos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsReportesAfterOpen(DataSet: TDataSet);
    procedure cdsLookupReportesAfterOpen(DataSet: TDataSet);
    procedure cdsQueryAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FValoresActivos: TValoresActivos;
    FStatus: String;
    FLogHayError: Boolean;
    FXMLReader: TdmXMLTools;
    FXMLResult: TdmXMLTools;
    FColPtr: Integer;
    FRowPtr: Integer;
    FFormatoFecha: String;
    FGrupo: Integer;
    FParametros: TZetaParams;
    FRefrescaActivos: Boolean;
    FServerConsultas: IdmServerConsultasDisp;
    FServerReportes: IdmServerReportesDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerReportes: IdmServerReportesDisp;
    procedure CargaActivosIMSS(Parametros: TZetaParams);
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema( Parametros: TZetaParams );
    procedure ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    function GetEmpleadoActivo: Integer;
  protected
    { Protected declarations }
    procedure GetEmpleadoInicial;
    procedure GetIMSSInicial;
    procedure GetPeriodoInicial;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Empleado: Integer read GetEmpleadoActivo;
    property ValoresActivos: TValoresActivos read FValoresActivos;
    property FormatoFecha: String read FFormatoFecha write FFormatoFecha;
    property Parametros: TZetaParams read FParametros;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    property LogHayError: Boolean read FLogHayError;
    property XMLReader: TdmXMLTools read FXMLReader;
    property XMLResult: TdmXMLTools read FXMLResult;
    property Status: String read FStatus write FStatus;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetReporteNombre: String;
    function GetReporteNumero: Integer;
    function GetSQLData(const sSQL: String ): Integer;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    function GetVarActivo(const sValor: String): OleVariant;
    //function GetValorActivo(const sValor: String): String;override;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;override;
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean; overload;
    function SetEmpresaActiva(): Boolean; overload;
    function UsuarioLogin(): Boolean;
    function EoF: Boolean;
    function EoRow: Boolean;
    function FieldName: String;
    function Valor: OleVariant;
    function ConectaEmpresa: Boolean;
    function LogAsText: String;
    function GetGrupoActivo : Integer; override;
    procedure CargarValoresActivos( const Valores: WideString );
    procedure LogEnd;
    procedure LogError(const sTexto: String; const iEmpleado: Integer = 0);
    procedure LogInfo(const sTexto: String; const iEmpleado: Integer = 0);
    procedure LogInit;
    procedure BuscarReportes(const eClasificacion: eClasifiReporte);
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
    procedure First;
    procedure InitResultDataset;
    procedure Next;
    procedure NextCol;
    procedure SetVarActivo(const sValor: String; const Valor: OleVariant);
  end;

threadvar
   dmCliente: TdmCliente;

implementation

uses FAutoClasses,
     DDiccionario,
     ZetaServerTools,
     ZReportConst,
     ZReportTools,
     ZAccesosMgr,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaBusqueda,
     ZetaTipoEntidadTools,
     ZetaWinAPITools,
     ZetaTipoEntidad;

{$R *.DFM}

const
     XML_UTF_8_ENCODING = 'UTF-8';
     K_EMPLEADO = '@EMPLEADO';
     K_IMSS_PATRON = '@PATRON';
     K_IMSS_YEAR = '@IMSS_YEAR';
     K_IMSS_MES = '@IMSS_MES';
     K_IMSS_TIPO = '@IMSS_TIPO';
     K_YEAR = '@YEAR';
     K_TIPO = '@TIPO';
     K_NUMERO = '@NUMERO';
     K_FECHA = '@FECHA';
     K_TAG_DATOS = 'DATOS';

function ValorActivoIndex( const sValor: String ): eRDDValorActivo;
begin
     if ( sValor = K_EMPLEADO ) then
        Result := vaEmpleado
     else
         if ( sValor = K_FECHA ) then
            Result := vaSinValor
         else
             if ( sValor = K_IMSS_PATRON ) then
                Result := vaImssPatron
             else
                 if ( sValor = K_IMSS_YEAR ) then
                    Result := vaImssYear
                 else
                     if ( sValor = K_IMSS_MES ) then
                        Result := vaImssMes
                     else
                         if ( sValor = K_IMSS_TIPO ) then
                            Result := vaImssTipo
                         else
                             if ( sValor = K_YEAR ) then
                                Result := vaYearNomina
                             else
                                 if ( sValor = K_TIPO ) then
                                    Result := vaTipoNomina
                                 else
                                     if ( sValor = K_NUMERO ) then
                                        Result := vaNumeroNomina
                                     else
                                         Result := vaSinValor;
end;

{ ********** TValoresActivos *********** }

constructor TValoresActivos.Create;
begin
end;

destructor TValoresActivos.Destroy;
begin
     inherited Destroy;
end;

procedure TValoresActivos.CargarParametros( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          // Activos Sistema
          AddDate( 'FechaAsistencia', FechaAsistencia );
          AddDate( 'FechaDefault', Fecha );
          AddInteger( 'YearDefault', Anio );
          AddInteger( 'EmpleadoActivo', Empleado );
          AddString( 'NombreUsuario', VACIO );
          AddString( 'CodigoEmpresa', EmpresaActiva );
          with Periodo do
          begin
               AddInteger( 'Year', Year );
               AddInteger( 'Tipo', Ord( Tipo ) );
               AddInteger( 'Numero', Numero );
          end;
          with LiquidacionIMSS do
          begin
               AddString( 'RegistroPatronal', Patron );
               AddInteger( 'IMSSYear', Year );
               AddInteger( 'IMSSMes', Mes );
               AddInteger( 'IMSSTipo', Ord( Tipo ) );
          end;
     end;
end;

{ ********* TdmCliente ********** }

constructor TdmCliente.Create(AOwner: TComponent);
begin
     FValoresActivos := TValoresActivos.Create;
     FLogHayError := False;
     FFormatoFecha := 'dd/mm/yyyy';
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     inherited Create( AOwner );
end;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     FGrupo := 0;
     FRefrescaActivos := TRUE;
     FXMLReader := TdmXMLTools.Create( nil );
     FXMLResult := TdmXMLTools.Create( nil );
     inherited;
end;

destructor TdmCliente.Destroy;
begin
     ZetaRegistryCliente.ClearClientRegistry;
     FreeAndNil( FValoresActivos );
     inherited Destroy;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FXMLReader );
     FreeAndNil( FXMLResult );
end;

function TdmCliente.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmCliente.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp( CreaServidor( CLASS_dmServerReportes, FServerReportes ) );
end;

{ ********** Bit�cora de mensajes y errores ***************** }

procedure TdmCliente.LogInit;
begin
     FLogHayError := False;
     {
     GA: El encoding se fija a nivel interfase de
     webservice, en los componentes de SOAP
     }
     {$ifdef FALSE}
     FXMLResult.XMLEncoding := XML_UTF_8_ENCODING;
     {$endif}
     FXMLResult.XMLInit( K_TAG_DATOS );
end;

procedure TdmCliente.LogEnd;
begin
     FXMLResult.XMLEnd;
end;

procedure TdmCliente.LogInfo( const sTexto: String; const iEmpleado: Integer = 0 );
const
     K_INFO = 'INFO';
var
   sInfo: String;
begin
     sInfo := sTexto;
     if ( iEmpleado > 0 ) then
     begin
          sInfo:= Format( 'Empleado #%d = %s' + CR_LF, [ iEmpleado, sInfo ] );
     end;
     FXMLResult.WriteValueString( K_INFO, sInfo );
end;

procedure TdmCliente.LogError( const sTexto: String; const iEmpleado: Integer = 0 );
begin
     with FXMLResult do
     begin
          if ( iEmpleado > 0 ) then
          begin
               XMLError( Format( 'Empleado #%d = %s' + CR_LF, [ iEmpleado, sTexto ] ) );
          end
          else
          begin
               XMLError( sTexto );
          end;
     end;
     FLogHayError := true;
end;

function TdmCliente.LogAsText: String;
begin
     with FXMLResult do
     begin
          {$ifdef FALSE}
          if ( XMLEncoding = XML_UTF_8_ENCODING ) then
          begin
               Result := ZetaCommonTools.QuitaAcentos( XMLAsText );
          end
          else
          begin
               Result := XMLAsText;
          end;
          {$else}
          Result := XMLAsText;
          {$endif}
     end;
end;

{ ********** M�todos para Autenticar y Empresas ************* }

function TdmCliente.UsuarioLogin(): Boolean;
begin
     case GetUsuario( Self.ValoresActivos.TressUserName, Self.ValoresActivos.TressPassword,'PORTAL::'+ZetaWinAPITools.GetComputerName, True ) of
          lrOK: Result := True;
     else
         Result := False;
     end;
end;

function TdmCliente.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmCliente.SetEmpresaActiva(): Boolean;
begin
     Result := Self.SetEmpresaActiva( Self.ValoresActivos.EmpresaActiva );
end;

function TdmCliente.SetEmpresaActiva(const sCodigo: String): Boolean;
var
   iGrupo: Integer;
   lCompanyChange: Boolean;
begin
     Result := False;
     lCompanyChange := False;
     InitCompanies;
     with cdsCompany do
     begin
          if ( FieldByName( 'CM_CODIGO' ).AsString = sCodigo ) then
          begin
               Result := True;
               SetCompany;
          end
          else
          begin
               if Locate( 'CM_CODIGO', sCodigo, [] ) then
               begin
                    Result := True;
                    lCompanyChange := True;
                    SetCompany;
               end;
          end;
     end;
     if Result then
     begin
          iGrupo := GetGrupoActivo;
          if ( iGrupo <> FGrupo ) or lCompanyChange then
          begin
               FGrupo := iGrupo;
               ZAccesosMgr.LoadDerechos;
          end;
     end;
end;

function TdmCliente.GetGrupoActivo : Integer;
begin
     result := D_GRUPO_SIN_RESTRICCION;
end;


{ ***** Cargado de XML y conexi�n de empresa }

function TdmCliente.ConectaEmpresa: Boolean;
begin
     Result := FALSE;
     try
        if ZetaCommonTools.StrVacio( ValoresActivos.EmpresaActiva ) then
        begin
             LogError( 'Empresa No Especificada' );
        end
        else
        begin
             if ( ValoresActivos.Usuario = 0 ) then
             begin
                  LogError( 'Usuario no especificado' );
             end
             else
             begin
                  Usuario := ValoresActivos.Usuario;    // Asigna el usuario activo a dmCliente - No se hace login ni se validan derechos de acceso
                  if ( ZetaCommonTools.StrLleno( ValoresActivos.Database.DatabaseServer ) and ZetaCommonTools.StrLleno( ValoresActivos.Database.UserName ) and ZetaCommonTools.StrLleno( ValoresActivos.Database.Password ) ) then
                  begin
                       InitCompanies;
                       with cdsCompany do
                       begin
                            LogChanges := False;
                            EmptyDataset;
                            Insert;
                            FieldByName( 'CM_CODIGO' ).AsString := ValoresActivos.EmpresaActiva;
                            FieldByName( 'CM_DATOS' ).AsString := ValoresActivos.Database.DatabaseServer;
                            FieldByName( 'CM_USRNAME' ).AsString := ValoresActivos.Database.UserName;
                            FieldByName( 'CM_PASSWRD' ).AsString := ValoresActivos.Database.Password;
                            FieldByName( 'CM_NIVEL0' ).AsString := '';
                            Post;
                       end;
                       SetCompany;
                       Result := True;
                  end
                  else
                  begin
                       Result := SetEmpresaActiva( ValoresActivos.EmpresaActiva );
                  end;
                  if ( not Result ) then
                  begin
                       LogError( Format( 'Empresa %s No Existe', [ ValoresActivos.EmpresaActiva ] ) );
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error al conectar empresa: ' + Error.Message );
           end;
     end;
end;

{ ********** Procesamiento del XML recibido *********** }

procedure TdmCliente.CargarValoresActivos( const Valores: WideString );
const
     NODO_VALORES = 'VALORES';
     NODO_VALORES_ACTIVOS = 'VALORESACTIVOS';
var
   oNodo, oItem: TZetaXMLNode;
begin
     if StrLleno( Valores ) then
     begin
          with XMLReader do
          begin
               XMLAsText := Valores;
               oNodo := GetNode( NODO_VALORES );
               if Assigned( oNodo ) then
               begin
                    oItem := GetNode( 'EMPRESA', oNodo );
                    if Assigned( oItem ) then
                    begin
                         with ValoresActivos do
                         begin
                              EmpresaActiva := TagAsString( oItem, VACIO );
                              with Database do
                              begin
                                   DatabaseServer := AttributeAsString( oItem, 'Database', VACIO );
                                   UserName := AttributeAsString( oItem, 'Username', VACIO );
                                   Password := AttributeAsString( oItem, 'Password', VACIO );
                              end;
                         end;
                    end;
                    with ValoresActivos do
                    begin
                         Usuario := TagAsInteger( GetNode( 'USUARIO', oNodo ), 0 );
                         TressUserName := TagAsString( GetNode( 'TRESSUSERNAME', oNodo ), VACIO );
                         TressPassword := TagAsString( GetNode( 'TRESSPASSWORD', oNodo ), VACIO );
                    end;
                    oNodo := GetNode( NODO_VALORES_ACTIVOS, oNodo );
                    if Assigned( oNodo ) then               // Viene seccion de valores activos
                    begin
                         with ValoresActivos do
                         begin
                              Empleado :=  TagAsInteger( GetNode( 'EMPLEADO', oNodo ), 0 );
                              FechaAsistencia := TagAsDate( GetNode( 'FECHAASISTENCIA', oNodo ), NullDateTime );
                              Fecha := TagAsDate( GetNode( 'FECHADEFAULT', oNodo ), NullDateTime );
                              Anio := TagAsInteger( GetNode( 'YEAR', oNodo ), 0 );
                         end;
                         oItem := GetNode( 'PERIODO', oNodo );
                         if Assigned( oItem ) then
                         begin
                              with ValoresActivos.Periodo do
                              begin
                                   Year := TagAsInteger( GetNode( 'YEAR', oItem ), 0 );
                                   Tipo := eTipoPeriodo( TagAsInteger( GetNode( 'TIPO', oItem ), 0 ) );
                                   Numero := TagAsInteger( GetNode( 'NUMERO', oItem ), 0 );
                              end;
                         end;
                         oItem := GetNode( 'IMSS', oNodo );
                         if Assigned( oItem ) then
                         begin
                              with ValoresActivos.LiquidacionIMSS do
                              begin
                                   Patron := TagAsString( GetNode( 'PATRON', oItem ), VACIO );
                                   Year := TagAsInteger( GetNode( 'YEAR', oItem ), 0 );
                                   Mes := TagAsInteger( GetNode( 'TIPO', oItem ), 0 );
                                   Tipo := eTipoLiqIMSS( TagAsInteger( GetNode( 'NUMERO', oItem ), 0 ) );
                              end;
                         end;
                    end;
               end;
          end;
          ConectaEmpresa;
     end
     else
         raise Exception.Create( 'Los par�metros no fueron especificados' );
end;

{ ********** Navegaci�n del ClientDataset ************* }

procedure TdmCliente.InitResultDataset;
begin
     with cdsQuery do
     begin
          if Active then
          begin
               EmptyDataset;
               Active := False;
          end;
          Init;
     end;
end;

function TdmCliente.EoF: Boolean;
begin
     with cdsQuery do
     begin
          if Active then
             Result := Eof or ( ( Autorizacion.EsDemo ) and ( FRowPtr > 3 ) )
          else
              Result := True;
     end;
end;

function TdmCliente.EoRow: Boolean;
begin
     with cdsQuery do
     begin
          if Active then
             Result := ( FColPtr >= FieldCount )
          else
              Result := True;
     end;
end;

procedure TdmCliente.First;
begin
     with cdsQuery do
     begin
          if Active then
             First;
     end;
     FColPtr := 0;
end;

procedure TdmCliente.Next;
begin
     with cdsQuery do
     begin
          if Active then
             Next;
     end;
     FColPtr := 0;
     Inc( FRowPtr );
end;

procedure TdmCliente.NextCol;
begin
     Inc( FColPtr );
end;

function TdmCliente.Valor: OleVariant;
begin
     with cdsQuery do
     begin
          if Active then
          begin
               with cdsQuery.Fields[ FColPtr ] do
               begin
                    {$ifdef FALSE}
                    case DataType of
                         ftString: Result := AsString;
                         ftSmallint: Result := IntToStr( AsInteger );
                         ftInteger: Result := IntToStr( AsInteger );
                         ftWord: Result := IntToStr( AsInteger );
                         ftBoolean: Result := ZetaCommonTools.BoolToSiNo( AsBoolean );
                         ftFloat: Result := Format( '%n', [ AsFloat ] );
                         ftCurrency: Result := Format( '%n', [ AsCurrency ] );
                         ftBCD: Result := Format( '%n', [ AsFloat ] );
                         ftDate: Result := FormatDateTime( FormatoFecha, AsDateTime );
                         ftTime: Result := FormatDateTime( 'hh:nn:ss', AsDateTime );
                         ftDateTime: Result := FormatDateTime( FormatoFecha, AsDateTime );
                         ftBlob: Result := '<BLOB>';
                         ftMemo: Result := AsString;
                         ftGraphic: Result := '<GRAPHIC>';
                         ftWideString: Result := AsString;
                         ftLargeint: Result := IntToStr( AsInteger );
                    else
                        Result := '<???>';
                    end;
                    {$endif}
                    Result := Value;
               end;
          end
          else
              Result := NULL;
     end;
end;

function TdmCliente.FieldName: String;
begin
     with cdsQuery do
     begin
          if Active then
             Result := Fields[ FColPtr ].DisplayLabel
          else
              Result := VACIO;
     end;
end;

{ ********** Valores Activos ************ }

function TdmCliente.GetEmpleadoActivo: Integer;
begin
     Result := Self.ValoresActivos.Empleado;
end;

procedure TdmCliente.GetEmpleadoInicial;
var
   Datos: OleVariant;
   lResult: Boolean;
begin
     lResult := Servidor.GetEmpleadoSiguiente( Empresa, 0, VACIO,Datos );
     if lResult then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
               if not Eof then
               begin
                    ValoresActivos.Empleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               end;
          end;
     end;
end;

procedure TdmCliente.GetPeriodoInicial;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, TheYear(Date), Ord( tpSemanal ) );
          with ValoresActivos.Periodo do
          begin
               Year := FieldByName( 'PE_YEAR' ).AsInteger;
               Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
               Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;

procedure TdmCliente.GetIMSSInicial;
begin
     with ValoresActivos.LiquidacionIMSS do
     begin
          with cdsPatron do
          begin
               Data := Servidor.GetPatrones( Empresa );
               if IsEmpty then
               begin
                    Patron := '';
               end
               else
               begin
                    Patron := FieldByName( 'TB_CODIGO' ).AsString;
               end;
          end;
          Year := ZetaCommonTools.TheYear( Now );
          Tipo := tlOrdinaria;
          Mes := ZetaCommonTools.TheMonth( Date ) - 1;
          if ( Mes = 0 ) then
          begin
               Mes := 12;
               Year := Year - 1;
          end;
     end;
end;


procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          with ValoresActivos.LiquidacionIMSS do
          begin
               AddString( 'RegistroPatronal', Patron );
               AddInteger( 'IMSSYear', Year );
               AddInteger( 'IMSSMes', Mes );
               AddInteger( 'IMSSTipo', Ord( Tipo ) );
          end;
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          with ValoresActivos.Periodo do
          begin
               AddInteger( 'Year', Year );
               AddInteger( 'Tipo', Ord( Tipo ) );
               AddInteger( 'Numero', Numero );
          end;
     end;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          with ValoresActivos do
          begin
               AddDate( 'FechaAsistencia', FechaAsistencia );
               AddDate( 'FechaDefault', FechaDefault );
               AddInteger( 'YearDefault', TheYear( FechaDefault ) );
               AddInteger( 'EmpleadoActivo', Empleado );
               AddString( 'NombreUsuario', 'REP_WEB' );
               AddString( 'CodigoEmpresa', EmpresaActiva );
          end;
     end;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     Result := ValoresActivos.Periodo;
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
     Result := '';
end;

function TdmCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     Result := dmDiccionario.GetValorActivo( eValor );
end;

function TdmCliente.GetVarActivo( const sValor : string ): OleVariant;
begin
     with ValoresActivos do
     begin
          case ValorActivoIndex( sValor ) of
               vaEmpleado: Result := Empleado;
               vaImssPatron: Result := LiquidacionIMSS.Patron;
               vaImssYear: Result := LiquidacionIMSS.Year;
               vaImssMes: Result := LiquidacionIMSS.Mes;
               vaImssTipo: Result := LiquidacionIMSS.Tipo;
               vaYearNomina: Result := Periodo.Year;
               vaTipoNomina: Result := Periodo.Tipo;
               vaNumeroNomina: Result := Periodo.Numero;
          end;
     end;
end;

procedure TdmCliente.SetVarActivo(const sValor: String; const Valor: OleVariant );
var
   eValor: eRDDValorActivo;
begin
     eValor := ValorActivoIndex( sValor );
     with ValoresActivos do
     begin
          case eValor of
               vaEmpleado: Empleado := Valor;
          end;
          with LiquidacionIMSS do
          begin
               case eValor of
                    vaImssPatron: Patron := Valor;
                    vaImssYear: Year := Valor;
                    vaImssMes: Mes := Valor;
                    vaImssTipo: Tipo := Valor;
               end;
          end;
          with Periodo do
          begin
               case eValor of
                    vaYearNomina: Year := Valor;
                    vaTipoNomina: Tipo := Valor;
                    vaNumeroNomina: Numero := Valor;
               end;
          end;
     end;
end;

function TdmCliente.GetSQLData(const sSQL: String ): Integer;
begin
     with cdsQuery do
     begin
          Data := ServerConsultas.GetQueryGral( Empresa, sSQL , True);
          Result := RecordCount;
     end;
end;

function TdmCliente.GetReporteNumero: Integer;
begin
     Result := cdsLookupReportes.FieldByName( 'RE_CODIGO' ).AsInteger;
end;

function TdmCliente.GetReporteNombre: String;
begin
     Result := cdsLookupReportes.FieldByName( 'RE_NOMBRE' ).AsString;
end;

procedure TdmCliente.BuscarReportes( const eClasificacion: eClasifiReporte );
begin
     with cdsLookUpReportes do
     begin
          Data := ServerReportes.GetReportes( Empresa, Ord( eClasificacion ), Ord(crFavoritos), Ord(crSuscripciones), VACIO, True );
     end;
end;

{ ********** Client Datasets ************ }

procedure TdmCliente.cdsReportesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsReportes.Data := ServerReportes.GetEscogeReporte( Empresa, Ord( enNomina ), Ord( trPoliza ), True );
end;

procedure TdmCliente.cdsReportesAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsReportes do
     begin
          MaskFecha( 'RE_FECHA' );
          ListaFija( 'RE_TIPO', lfTipoReporte );
     end;
end;

procedure TdmCliente.cdsQueryAfterOpen(DataSet: TDataSet);
begin
     inherited;
     FRowPtr := 1;
end;

procedure TdmCliente.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
              {$ifdef RDD}
              with Sender.DataSet do
                   if ( FindField('RE_TABLA') <> NIL ) then
                      Text := FieldByName('RE_TABLA').AsString
                   else Text := Format( '< Tabla #%d >', [FieldByName('RE_ENTIDAD').AsInteger] );
              {$else}
              Text := ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
              {$endif}
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmCliente.cdsLookupReportesAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsLookUpReportes do
     begin
          FieldByName( 'RE_CODIGO' ).Alignment := taLeftJustify;
          MaskFecha( 'RE_FECHA' );
          ListaFija( 'RE_TIPO', lfTipoReporte );
          with FieldByName( 'RE_ENTIDAD' ) do
          begin
               OnGetText := ObtieneEntidadGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

end.
