unit DReportesPDF;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Db, DBClient, FileCtrl,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZAgenteSQLClient,
     ZReportTools,
     ZQRReporteListado,
     DXMLTools,
     DReportesGenerador,
     DReportesWebBase;

type
  TdmReportesPDF = class(TdmReportWebGeneratorBase)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    QRReporteListado: TQrReporteListado;
    FDefaultStream: TStream;
  protected
    { Protected declarations }
    function GetExtensionDef : string;override;
    function PreparaPlantilla(var sError: WideString): Boolean;override;
    procedure DoOnGetDatosImpresion;override;
    procedure DoOnGetReportes( const lResultado: Boolean );override;
    procedure DoOnGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DesPreparaPlantilla;override;
    procedure PreparaReporte;
    procedure DesPreparaReporte;
  public
    { Public declarations }
    property DefaultStream: TStream read FDefaultStream write FDefaultStream;
    function Procesar: Integer;
  end;

implementation

uses DCliente,
     DGlobal,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZFuncsCliente,
     ZAccesosTress,
     ZReporteAscii,
     ZetaDialogo,
     DMailMerge,DElementos;

{$R *.DFM}

const
     K_NOMBRE_BITACORA = 'ReportesWeb.LOG';

{ ***************** TdmReportes ******************** }

procedure TdmReportesPDF.DataModuleCreate(Sender: TObject);
begin
     FMostrarError := FALSE;
     {
     SetLogFileName( ExtractFilePath( Application.ExeName ) + K_NOMBRE_BITACORA );
     }
     inherited;
     {
     Global.Conectar;
     }
end;

procedure TdmReportesPDF.DataModuleDestroy(Sender: TObject);
begin
     Tag := 0;
     inherited;
end;

function TdmReportesPDF.PreparaPlantilla( var sError : WideString ) : Boolean;
begin
     PreparaReporte;
     Result := TRUE;
     if Result then
     begin
          DoOnGetDatosImpresion;
     end;
end;

procedure TdmReportesPDF.DesPreparaPlantilla;
begin
     DesPreparaReporte;
end;

procedure TdmReportesPDF.DoOnGetReportes( const lResultado: Boolean );
begin
     if lResultado then
     begin
          with cdsReporte do
          begin
               ReporteInfo.Nombre := FieldByName( 'RE_NOMBRE' ).AsString;
               if not (eTipoReporte( FieldByName( 'RE_TIPO' ).AsInteger ) in [ trForma, trListado ] ) then
               begin
                    raise Exception.Create( 'Solamente se Permite Obtener Listados y/o Formas' );
               end;
          end;
     end;
end;

procedure TdmReportesPDF.DoOnGetResultado( const lResultado : Boolean; const Error : string );

 procedure GeneraMailMerge;
  var
     oReporteAscii: TReporteAscii;
 begin
      oReporteAscii := TReporteAscii.Create;

      try
         with oReporteAscii do
         begin
              if GeneraAscii( cdsResultados,
                              DatosImpresion,
                              Campos,
                              Grupos,
                              ReporteInfo.SoloTotales,
                              FALSE ) then
              begin
                   with TdmMailMerge.Create(self) do
                   begin
                        with DatosImpresion do
                             if FileExists( Archivo ) then
                                Imprime( Archivo,Exportacion )
                             else
                                 ZError('Impresión','El Documento ' + Archivo + ' No Ha Sido Encontrado.', 0);
                        Free;
                   end;
              end;
         end;
      finally
             FreeAndNil( oReporteAscii );
      end;

 end;

begin
     if cdsResultados.Active AND Not cdsResultados.IsEmpty then
     begin

          cdsResultados.First;

          ModificaListas;

          ZFuncsCliente.RegistraFuncionesCliente;
          //Asignacion de Parametros del Reporte,
          //para poder evaluar la funcion PARAM() en el Cliente;
          ZFuncsCliente.ParametrosReporte := Self.SQLAgente.Parametros;

                    case ReporteInfo.Tipo of
               trForma: QRReporteListado.GeneraForma( ReporteInfo.Nombre, FALSE, cdsResultados );
               trListado:

               begin
                    case ReporteInfo.Formato of
                         tfMailMerge: GeneraMailMerge;
                    else
                        QRReporteListado.GeneraListado( ReporteInfo.Nombre,
                                                        FALSE, {Mandarlo a Preview}
                                                        ReporteInfo.ColumnasHorizontales,
                                                        ReporteInfo.SoloTotales,
                                                        GetSobreTotales,
                                                        cdsResultados,
                                                        Campos,
                                                        Grupos );
                    end;
               end;
          end;
     end;
end;

procedure TdmReportesPDF.PreparaReporte;
begin
     if QRReporteListado = NIL then
     begin
          QRReporteListado := TQRReporteListado.Create( nil );
     end;
end;

procedure TdmReportesPDF.DesPreparaReporte;
begin
     try
        QRReporteListado.Free;
     finally
            QRReporteListado := NIL;
     end;
end;

procedure TdmReportesPDF.DoOnGetDatosImpresion;
 var
    sError: string;
    lHayImagenes: Boolean;
begin
     with DatosImpresion do
     begin
          Tipo := tfPDF;
          Exportacion := GetNombreExportacion( DatosImpresion, GetExtensionDef, sError );
          with QRReporteListado do
          begin
               Init( SQLAgente, DatosImpresion, Parametros.Count, lHayImagenes );
               ContieneImagenes := lHayImagenes;
          end;
          QRReporteListado.DefaultStream := FDefaultStream;

     end;
end;

function TdmReportesPDF.GetExtensionDef : string;
begin
     Result := 'PDF';
end;

function TdmReportesPDF.Procesar: Integer;
begin
     Global.Conectar;
     Empresa := dmCliente.ValoresActivos.EmpresaActiva;
    // if GetResultado( ReporteInfo.Codigo, TRUE ) then
     if GenerarReporte( True ) then
     begin
          Result := cdsResultados.RecordCount;
     end
     else
     begin
          Result := 0;
     end;
end;

end.
