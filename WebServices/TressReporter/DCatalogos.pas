unit DCatalogos;

interface

uses Windows, Messages, SysUtils, Classes, Controls, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     {$else}
     Catalogos_TLB,
     {$endif}
     ZetaClientDataSet;

type
  TdmCatalogos = class(TDataModule)
    cdsCondiciones: TZetaLookupDataSet;
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
{$else}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
{$endif}

  public
    { Public declarations }
  end;

threadvar
  dmCatalogos: TdmCatalogos;

implementation

uses DCliente;

{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}
procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

end.
