unit DCatalogos;

interface

uses Windows, Messages, SysUtils, Classes, Controls, Db, DBClient,
     DCliente,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     {$else}
     Catalogos_TLB,
     {$endif}
     ZetaClientDataSet;

type
  TdmCatalogos = class(TDataModule)
    cdsCondiciones: TZetaLookupDataSet;
    cdsHorarios: TZetaLookupDataSet;
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsHorariosAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FdmCliente: TdmCliente;
    {$ifdef DOS_CAPAS}
    function GetServerCatalogos: TdmServerCatalogos;
    {$else}
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    {$endif}
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerCatalogo: TdmServerCatalogos read GetServerCatalogos;
    {$else}
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
    {$endif}
  public
    { Public declarations }
    property dmCliente: TdmCliente read FdmCliente write FdmCliente;
  end;

implementation

{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmCatalogos.GetServerCatalogos: TdmServerCatalogos;
begin
     Result := DCliente.dmCliente.ServerCatalogos;
end;
{$else}
function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;
{$endif}

{ ************** Eventos de client datasets **************** }

{ ********* cdsCondiciones ********** }

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

{ ********* cdsHorarios ********* }

procedure TdmCatalogos.cdsHorariosAlAdquirirDatos(Sender: TObject);
begin
     cdsHorarios.Data := ServerCatalogo.GetHorarios( dmCliente.Empresa );
end;

end.
