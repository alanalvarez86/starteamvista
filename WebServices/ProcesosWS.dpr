library ProcesosWS;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIThreadPool,
  ISAPIApp,
  ProcesosWSImpl in 'ProcesosWSImpl.pas',
  ProcesosWSIntf in 'ProcesosWSIntf.pas',
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DProcesosWS in 'DProcesosWS.pas' {dmProcesosWS: TWebModule};

{$R *.res}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  // Application.CreateForm(TdmProcesos, dmProcesos);
  Application.CreateForm(TdmProcesosWS, dmProcesosWS);
  Application.Run;
end.
