program TressWebServicesProcesosTester;



uses
  Forms,
  ZetaClientTools,
  FWebServicesTest in 'FWebServicesTest.pas' {TestWebServices},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DCliente in '..\DCliente.pas' {dmCliente: TDataModule},
  DProcesos in '..\DProcesos.pas' {dmProcesos: TDataModule};

{$R *.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.CreateForm(TTestWebServices, TestWebServices);
  Application.Run;
end.
