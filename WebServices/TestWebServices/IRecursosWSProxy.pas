// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://dell-apache01/TressRecursos/RecursosWS.dll/wsdl/IRecursosWS
// Encoding : utf-8
// Version  : 1.0
// (6/22/2007 12:02:02 PM - 1.33.2.5)
// ************************************************************************ //

unit IRecursosWSProxy;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"


  // ************************************************************************ //
  // Namespace : urn:RecursosWSIntf-IRecursosWS
  // soapAction: urn:RecursosWSIntf-IRecursosWS#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : IRecursosWSbinding
  // service   : IRecursosWSservice
  // port      : IRecursosWSPort
  // URL       : http://dell-apache01/TressRecursos/RecursosWS.dll/soap/IRecursosWS
  // ************************************************************************ //
  IRecursosWS = interface(IInvokable)
  ['{3C0DF8B5-00C6-5FE8-2ECD-40878FF87447}']
    function  AltaEmpleado(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoReingreso(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoBaja(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoIdentificacion(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoPersonales(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoOtros(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoAdicionales(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoExperiencia(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoParientes(const Parametros: WideString): WideString; stdcall;
    function  KardexCambio(const Parametros: WideString): WideString; stdcall;
    function  KardexVacaciones(const Parametros: WideString): WideString; stdcall;
    function  KardexPermisos(const Parametros: WideString): WideString; stdcall;
    function  KardexIncapacidades(const Parametros: WideString): WideString; stdcall;
    function  AsistenciaTarjetaDiaria(const Parametros: WideString): WideString; stdcall;
    function  AsistenciaTarjetaDiariaDefaults(const Parametros: WideString): WideString; stdcall;
    function  AsistenciaAutorizacionAlta(const Parametros: WideString): WideString; stdcall;
    function  NominaExcepcionMonto(const Parametros: WideString): WideString; stdcall;
    function  NominaExcepcionDias(const Parametros: WideString): WideString; stdcall;
    function  NominaExcepcionHoras(const Parametros: WideString): WideString; stdcall;
    function  Echo(const Parametros: WideString): WideString; stdcall;
    function  CatalogoPuestos(const Parametros: WideString): WideString; stdcall;
    function  CatalogoTexto(const Parametros: WideString): WideString; stdcall;
    function  CatalogoTurnos(const Parametros: WideString): WideString; stdcall;
    function  EmpleadoPrestamos (const Parametros: WideString ): WideString; stdcall;
    function  EmpleadoPrestaAcarAbo(const Parametros: WideString ): WideString; stdcall;
    function GetVacaFechaRegreso(const Parametros: WideString ): WideString; stdcall;
    function GetVacaDiasGozados(const Parametros: WideString ): WideString; stdcall;
    function GetPermisoFechaRegreso(const Parametros: WideString ): WideString; stdcall;
    function GetPermisoDiasHabiles (const Parametros: WideString ): WideString; stdcall;
  end;

function GetIRecursosWS(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IRecursosWS;


implementation

function GetIRecursosWS(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IRecursosWS;
const
  defWSDL = 'http://dell-apache01/TressRecursos/RecursosWS.dll/wsdl/IRecursosWS';
  defURL  = 'http://dell-apache01/TressRecursos/RecursosWS.dll/soap/IRecursosWS';
  defSvc  = 'IRecursosWSservice';
  defPrt  = 'IRecursosWSPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IRecursosWS);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(IRecursosWS), 'urn:RecursosWSIntf-IRecursosWS', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IRecursosWS), 'urn:RecursosWSIntf-IRecursosWS#%operationName%');

end.