program TressWebServicesTester;



uses
  Forms,
  ZetaClientTools,
  FWebServicesTest in 'FWebServicesTest.pas' {TestWebServices},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DCliente in '..\DCliente.pas' {dmCliente: TDataModule};

{$R *.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.CreateForm(TTestWebServices, TestWebServices);
  Application.Run;
end.
