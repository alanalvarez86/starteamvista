{ Invokable interface IRecursosWS }

unit RecursosWSIntf;

interface

uses InvokeRegistry, Types, XSBuiltIns;

type

  { Invokable interfaces must derive from IInvokable }
  IRecursosWS = interface(IInvokable)
  ['{7726CACC-E7EE-49AB-917C-0A6ECFA7C96D}']

    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function AltaEmpleado( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoReingreso( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoBaja( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoIdentificacion( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoPersonales( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoOtros( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoAdicionales( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoExperiencia( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoParientes( const Parametros: WideString ): WideString; stdcall;
    function KardexCambio( const Parametros: WideString ): WideString; stdcall;
    function KardexVacaciones( const Parametros: WideString ): WideString; stdcall;
    function KardexPermisos( const Parametros: WideString ): WideString; stdcall;
    function KardexIncapacidades( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaTarjetaDiaria( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaTarjetaDiariaDefaults( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaAutorizacionAlta( const Parametros: WideString ): WideString; stdcall;
    function NominaExcepcionMonto( const Parametros: WideString ): WideString; stdcall;
    function NominaExcepcionDias( const Parametros: WideString ): WideString; stdcall;
    function NominaExcepcionHoras( const Parametros: WideString ): WideString; stdcall;
    function Echo( const Parametros: WideString ): WideString; stdcall;
    function CatalogoPuestos( const Parametros: WideString ): WideString; stdcall;
    function CatalogoTexto(const Parametros: WideString): WideString; stdcall;
    function CatalogoTurnos( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoPrestamos (const Parametros: WideString ): WideString; stdcall;
    function EmpleadoPrestaAcarAbo(const Parametros: WideString ): WideString; stdcall;
    function EmpleadoSaldoVacaciones(const Parametros: WideString ): WideString; stdcall;
    function GetVacaFechaRegreso(const Parametros: WideString ): WideString; stdcall;
    function GetVacaDiasGozados(const Parametros: WideString ): WideString; stdcall;
    function GetPermisoFechaRegreso(const Parametros: WideString ): WideString; stdcall;
    function GetPermisoDiasHabiles (const Parametros: WideString ): WideString; stdcall;

  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface(TypeInfo(IRecursosWS));

end.
