unit DRecursos;

interface

uses Windows, Messages, SysUtils, Variants, Classes, DB, DBClient, Controls,
     DBasicoCliente,
     DCliente,
     DXMLTools,
     Recursos_TLB,
     Asistencia_TLB,
     ZetaCommonLists,
     ZetaClientDataSet;

type
  TdmRecursos = class(TdmCliente)
    cdsQueryGeneral: TZetaClientDataSet;
  private
    { Private declarations }
    FListaPercepFijas: string;
    FOtrosDatos: Variant;
    FOperacion: string;
    function LlaveNuevaGet( const sQuery: string ): Integer;
    procedure LlaveNuevaKardex( const iEmpleado: Integer; const dFecha: TDate; const sTipo: String );
    procedure LlaveNuevaAutorizacionAsistencia( const iEmpleado: Integer; const dFecha: TDate; const eTipo: eTipoChecadas );
    procedure LlaveNuevaEscribir( const iLlave: Integer );
    procedure TransfiereOtrosDatosAlta( oNodo: TZetaXMLNode; oDataSet: TZetaClientDataSet );
    procedure LeerChecadas( sTag:String; oDataSet: TZetaClientDataSet;Empleado:Integer;FechaAusencia:TDate );
    procedure AntesBorrarKardex( const TipoKardex: String;iEmpleado:Integer; FechaKardex:TDateTime);
    function  GetTagNivel(const sCatalogo:string):Integer;
    procedure GetSaldos( DataSet : TDataSet; const sCargo, sAbono : string; var dCargo, dAbono : Real );
    procedure DespuesDeModificar(cdsPCarAbo:TZetaClientDataSet);
    procedure SetStatusPrestamo(DataSet: TDataSet);

  public
    { Public declarations }
    function ProcesaAlta( const Datos: String ): String;
    function ProcesaEmpleadoReingreso( const Datos: String ): String;
    function ProcesaEmpleadoBaja( const Datos: String ): String;
    function ProcesaDatosEmpleado( const Datos: String ): String;
    function ProcesaCambioKardex( const Datos: String ): String;

    function ProcesaKardexVacaciones( const Datos: String ): String;
    function ProcesaKardexPermisos( const Datos: String ): String;
    function ProcesaKardexIncapacidades( const Datos: String ): String;
    function ProcesaAsistenciaTarjetaDiaria( const Datos: String ): String;
    function ProcesaAsistenciaAutorizacion( const Datos: String): String;
    function ProcesaNominaExcepcionMonto( const Datos: String ): String;
    function ProcesaNominaExcepcionDiasHoras( const Datos: String; const EsTipoHora: Boolean ): String;

    function ObtieneDefaultsAsistenciaTarjetaDiaria( const Datos: String ): String;
    function ProcesaEmpleadoParientes (const Datos: String):string;

    function ProcesaCatalogoTexto (const Datos: string):string;
    function ProcesaCatalogoPuestos (const Datos: String):string;
    function ProcesaCatalogoTurnos (const Datos: String):string;

    function ProcesaPrestamos(const Datos: String):string;
    function ProcesaPrestaAcarAbo(const Datos: String):string;
    function ProcesaSaldosVacaciones(const Datos: String):string;

    function GetVacaFechaRegreso( const Datos: String ): WideString;
    function GetVacaDiasGozados( const Datos: String ): WideString;

    function GetPermisoFechaRegreso(const Datos: String ): WideString;
    function GetPermisoDiasHabiles (const Datos: String ): WideString;

  end;

implementation

{$R *.dfm}

uses ZetaCommonClasses,
     ZetaCommonTools,
     FToolsRH;

const
     K_TAG_PERMISOS = 15; // Tags para el servidor
     K_TAG_INCAPACI = 11;
     K_OP_ALTA = 'A';
     K_OP_BAJA = 'B';
     K_OP_CAMBIO = 'C';
     K_HORA_NULA = '----';
     KARDEX_NIVEL_DEFAULT = 5;
     K_TAG_PUESTOS = 0;
     K_TAG_TURNOS = 1;
     TAG_PRESTAMOS = 16;

{--------------------------------
   M�todos de utiler�a Alta de empleado
   Transfiere los campos que NO son de la tabla de Colabora, est�n  programadas: OTRAS_PER,NOM_PADRE,NOM_MADRE
---------------------------------}

procedure TdmRecursos.TransfiereOtrosDatosAlta( oNodo: TZetaXMLNode; oDataSet: TZetaClientDataSet );
const
     K_OTRAS_PER = 'OTRAS_PER';
     K_NOM_PADRE = 'NOM_PADRE';
     K_NOM_MADRE = 'NOM_MADRE';
var
   sNodo: String;
   oItem: TZetaXMLNode;
begin
     sNodo := oNodo.NodeName;
     if ( sNodo = K_OTRAS_PER ) then
     begin
          with XMLTools do
          begin
               if HasChildren( oNodo ) then               // Viene seccion de la tabla
               begin
                    oItem := GetFirstChild( oNodo );
                    while Assigned( oItem ) do
                    begin
                         FListaPercepFijas := ConcatString( FListaPercepFijas, TagAsString( oItem, VACIO ), ',' );
                         oItem := GetNextSibling( oItem );
                    end;
               end;
          end;
     end
     else
         if ( sNodo = K_NOM_PADRE ) then
         begin
              FOtrosDatos[0] := XMLTools.TagAsString( oNodo, VACIO );
         end
         else
             if ( sNodo = K_NOM_MADRE ) then
             begin
                  FOtrosDatos[1] := XMLTools.TagAsString( oNodo, VACIO );
             end;
end;
{--------------------------------
     --Alta de empleado--
     Procesa la llamada del Servicio Web de EmpleadoAlta , la cual barre el Xml para pasar los campos de la tabla
     Colabora al ClientDataSet, para llamar al servidor de Recursos y grabar los datos del nuevo empleado,
     asign�ndole, el evento de los otros campos del empleado
---------------------------------}
function TdmRecursos.ProcesaAlta( const Datos: String ): String;
var
   oDatos: OleVariant;
   iEmpleado: Integer;
   ErrorCount: Integer;
begin
     Result := VACIO;
     FListaPercepFijas := VACIO;
     FOtrosDatos := VarArrayOf( [ VACIO, VACIO ] );       // Default sin padre y madre
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             Servidor.GetEmpleado( Empresa, 0, oDatos );  // Trae la estructura de COLABORA
             with cdsLista do
             begin
                  Data := oDatos;
                  Append;
             end;
             ProcesaNodoData := TransfiereOtrosDatosAlta;
             TransfiereXMLDataSet( 'EMPLEADO', cdsLista );
             with cdsLista do
             begin
                  PostData;
                  iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                  if ( ChangeCount > 0 ) then
                  begin
                       OnReconcileError := ReconcileError;
                       if Reconciliar( ServerRecursos.GrabaDatosEmpleado( Empresa, Delta, FOtrosDatos, FListaPercepFijas, ErrorCount ) ) then
                       begin
                            LogInfo( Format( 'Se agreg� empleado # %d', [ iEmpleado ] ) );
                       end
                       else
                       begin
                            LogError( Status, iEmpleado );
                       end;
                  end
                  else
                  begin
                       LogInfo( 'No hay cambios', iEmpleado );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{----------------------------------

     Obtiene el valor de la columna KARDEX.LLAVE para el nuevo registro
     en la tabla KARDEX para la llave primaria que se especifica.

     Al ser editado un movimiento de KARDEX, su registro original es
     borrado y sustituido por uno nuevo, por lo que el valor de la
     columna KARDEX.LLAVE se pierde y es necesario averiguar el
     nuevo valor
-----------------------------------}
procedure TdmRecursos.LlaveNuevaKardex( const iEmpleado: Integer; const dFecha: TDate; const sTipo: String );
begin
     LlaveNuevaEscribir( LlaveNuevaGet( Format( 'select LLAVE from KARDEX where ( CB_CODIGO = %d ) and ( CB_FECHA = ''%s'' ) and ( CB_TIPO = ''%s'' )', [ iEmpleado, FormatDateTime( 'dd/mm/yyyy', dFecha), sTipo ] ) ) );
end;

procedure TdmRecursos.LlaveNuevaAutorizacionAsistencia( const iEmpleado: Integer; const dFecha: TDate; const eTipo: eTipoChecadas );
begin
     LlaveNuevaEscribir( LlaveNuevaGet( Format( 'select LLAVE from CHECADAS where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' ) and ( CH_H_REAL = ''%s'') and ( CH_TIPO = %d )', [ iEmpleado, FormatDateTime( 'dd/mm/yyyy', dFecha), K_HORA_NULA, Ord( eTipo ) ] ) ) );
end;

function TdmRecursos.LlaveNuevaGet( const sQuery: string ): Integer;
begin
     with cdsQueryGeneral do
     begin
          try
             Data := ServerConsultas.GetQueryGral( Empresa, sQuery , True);
             if ( RecordCount > 0 ) then
             begin
                  Result := FieldByName( 'LLAVE' ).AsInteger;
             end
             else
             begin
                  Result := 0;
             end;
             Active := False;
          except
                on Error: Exception do
                begin
                     Self.LogError( 'Error al obtener llave: ' + Error.Message, 0 );
                     Result := 0;
                end;
          end;
     end;
end;

procedure TdmRecursos.LlaveNuevaEscribir( const iLlave: Integer );
begin
     XMLResult.WriteValueInteger( 'LLAVE', iLlave );
end;

{--------------------------------
     Empleado Reingreso
     Procesa la llamada del Servicio Web de EmpleadoReingreo,la cual barre el Xml para pasar los campos de la tabla
     Colabora al ClientDataSet, para llamar al servidor de Recursos y grabar en su kardex el movimiento de reingreso
     para ese empleado asign�ndole, el evento de los otros campos del empleado
---------------------------------}
function TdmRecursos.ProcesaEmpleadoReingreso( const Datos: String ): String;
var
   iEmpleado: Integer;
   ErrorCount: Integer;
   FechaKardex: TDate;
   TipoKardex: string;
begin
     Result := VACIO;
     FListaPercepFijas := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos,'KARDEX' );
             FOperacion := GetOperacion();
             TipoKardex := '';
             with cdsLista do
             begin
                  if ( FOperacion = K_OP_ALTA ) then
                  begin
                       Data := ServerRecursos.GetEditHisKardex( Empresa, iEmpleado, NullDateTime, '' );
                       Append;
                       FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                       FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_SI;
                       FieldByName( 'CB_NIVEL').AsInteger  := KARDEX_NIVEL_DEFAULT;
                       FieldByName( 'CB_GLOBAL').AsString  := K_GLOBAL_NO;
                       FieldByName( 'CB_STATUS' ).AsInteger  := Ord( skCapturado );
                       //FieldByName( 'CB_TIPO' ).AsString := K_T_ALTA;
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            FechaKardex := TagAsDate( GetNode( 'CB_FECHA', GetNode( 'LLAVE' ) ), NullDateTime );
                            TipoKardex := TagAsString( GetNode( 'CB_TIPO', GetNode( 'LLAVE' ) ), '' );
                       end;
                       if ( FOperacion = K_OP_BAJA ) then
                       begin
                            Data := ServerRecursos.GetHisKardex(Empresa, iEmpleado);
                            //Obtener el historial del Kardex ,posicionarlo en el registro que se quiere borrar
                            //Borrarlo y enviarlo al servidor
                            if Locate( 'CB_FECHA;CB_TIPO', VarArrayOf( [ ZetaCommonTools.FechaAsStr( FechaKardex ), TipoKardex ] ), [] ) then
                            begin
                                 Delete;
                                 OnReconcileError := ReconcileError;
                                 if Reconciliar( ServerRecursos.GrabaBorrarKardex( Empresa, Delta, ErrorCount ) ) then
                                 begin
                                      LogInfo( Format( 'Se borr� la alta al empleado # %d', [ iEmpleado ] ) );
                                 end
                                 else
                                 begin
                                      LogError( Status, iEmpleado );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay reingreso registrado en %s', [ FormatDateTime( 'dd/mmm/yyyy', FechaKardex ) ] ), iEmpleado );
                            end;
                       end;
                       if ( FOperacion = K_OP_CAMBIO ) then
                       begin
                            Data := ServerRecursos.GetEditHisKardex(Empresa, iEmpleado, FechaKardex, TipoKardex );
                            if ( RecordCount > 0 ) then
                            begin
                                 Edit;
                                 FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay reingreso registrado en %s', [ FormatDateTime( 'dd/mmm/yyyy', FechaKardex ) ] ), iEmpleado );
                            end;
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) and ( FOperacion <> K_OP_BAJA ) then
             begin
                  ProcesaNodoData := TransfiereOtrosDatosAlta;
                  TransfiereXMLDataSet( 'KARDEX', cdsLista );
                  with cdsLista do
                  begin
                       FechaKardex := FieldByName( 'CB_FECHA' ).AsDateTime;
                       TipoKardex := FieldByName( 'CB_TIPO' ).AsString;
                       PostData;
                       if ( ChangeCount > 0 ) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerRecursos.GrabaHisKardex ( Empresa, Delta, FListaPercepFijas, ErrorCount ) ) then
                            begin
                                 LogInfo( Format( 'Se reingres� al empleado # %d', [ iEmpleado ] ) );
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      LlaveNuevaKardex( iEmpleado, FechaKardex, TipoKardex );
                                 end;
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{--------------------------------
     EmpleadoBaja
       Procesa la llamada del Servicio Web de EmpleadoBaja,la cual barre el Xml para pasar los campos de la tabla
     Colabora al ClientDataSet, para llamar al servidor de Recursos y grabar en su kardex el movimiento de BAJA
     para ese empleado
---------------------------------}
function TdmRecursos.ProcesaEmpleadoBaja( const Datos: String ): String;
var
   iEmpleado: Integer;
   ErrorCount: Integer;
   FechaKardex: TDate;
   TipoKardex: string;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos, 'KARDEX' );
             FOperacion := GetOperacion();
             TipoKardex := '';
             with cdsLista do
             begin
                  if ( FOperacion = K_OP_ALTA ) then
                  begin
                       Data := ServerRecursos.GetEditHisKardex(Empresa, iEmpleado, NullDateTime, '' );
                       Append;
                       FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                       FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                       FieldByName( 'CB_NIVEL' ).AsInteger  := KARDEX_NIVEL_DEFAULT;
                       FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                        FieldByName( 'CB_STATUS' ).AsInteger  := Ord( skCapturado );
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            FechaKardex := TagAsDate( GetNode( 'CB_FECHA', GetNode( 'LLAVE' ) ), NullDateTime );
                            TipoKardex := TagAsString( GetNode( 'CB_TIPO', GetNode( 'LLAVE' ) ), '' );
                       end;
                       if ( FOperacion = K_OP_BAJA ) then
                       begin
                            Data := ServerRecursos.GetHisKardex(Empresa, iEmpleado);
                            //Obtener el historial del Kardex ,posicionarlo en el registro que se quiere borrar
                            //Borrarlo y enviarlo al servidor
                            if Locate( 'CB_FECHA;CB_TIPO', VarArrayOf( [ ZetaCommonTools.FechaAsStr( FechaKardex ), TipoKardex ] ), [] ) then
                            begin
                                 Delete;
                                 OnReconcileError := ReconcileError;
                                 if Reconciliar( ServerRecursos.GrabaBorrarKardex( Empresa, Delta, ErrorCount ) ) then
                                 begin
                                      LogInfo( Format( 'Se borr� la baja al empleado # %d', [ iEmpleado ] ) );
                                 end
                                 else
                                 begin
                                      LogError( Status, iEmpleado );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay baja registrada en %s', [ FormatDateTime( 'dd/mmm/yyyy', FechaKardex ) ] ), iEmpleado );
                            end;
                       end;
                       if ( FOperacion = K_OP_CAMBIO ) then
                       begin
                            Data := ServerRecursos.GetEditHisKardex(Empresa, iEmpleado, FechaKardex, TipoKardex );
                            if ( RecordCount > 0 ) then
                            begin
                                 Edit;
                                 FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay baja registrada en %s', [ FormatDateTime( 'dd/mmm/yyyy', FechaKardex ) ] ), iEmpleado );
                            end;
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) and ( FOperacion <> K_OP_BAJA ) then
             begin
                  TransfiereXMLDataSet( 'KARDEX', cdsLista );
                  with cdsLista do
                  begin
                       FechaKardex := FieldByName( 'CB_FECHA' ).AsDateTime;
                       TipoKardex := FieldByName( 'CB_TIPO' ).AsString;
                       PostData;
                       if ( ChangeCount > 0 ) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerRecursos.GrabaHisKardex ( Empresa, Delta, FListaPercepFijas, ErrorCount ) ) then
                            begin
                                 LogInfo( Format( 'Se di� de baja el empleado # %d', [ iEmpleado ] ) );
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      LlaveNuevaKardex( iEmpleado, FechaKardex, TipoKardex );
                                 end;
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{--------------------------------
     --ProcesaDatosEmpleado--
         Procesa la llamada del Servicio Web de [edici�n de datos*] ,la cual barre el Xml para pasar los campos de la
         tabla Colabora al ClientDataSet, para llamar al servidor de Recursos y grabar los datos del empleado
         para ese empleado.

      *--M�todos que usan esta llamada --
			EmpleadoIdentificacion
			EmpleadOPersonales
			EmpleadoOtros
			EmpleadoAdicionales
			EmpleadoExperiencia
---------------------------------}
function TdmRecursos.ProcesaDatosEmpleado( const Datos: String ): String;
var
   oDatos: OleVariant;
   iEmpleado: Integer;
   ErrorCount: Integer;
begin
     Result := VACIO;
     FListaPercepFijas := VACIO;
     FOtrosDatos := VarArrayOf( [ VACIO, VACIO ] );    // Default sin padre y madre
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos ,'EMPLEADO' );
             Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );  // Trae la estructura de COLABORA
             with cdsLista do
             begin
                  Data := oDatos;
                  if ( RecordCount > 0 ) then
                  begin
                       Edit;
                       TransfiereXMLDataSet( 'EMPLEADO',cdsLista );
                       PostData;
                       if ( ChangeCount > 0 ) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerRecursos.GrabaDatosEmpleado( Empresa, Delta, FOtrosDatos, FListaPercepFijas, ErrorCount ) ) then
                            begin
                                 LogInfo( Format( 'Se edit� los datos del empleado # %d', [ iEmpleado ] ) );
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end
                  else
                  begin
                       Self.LogError( 'No hay datos para este empleado', iEmpleado );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{--------------------------------
     Procesa las llamadas del Servicio Web [* Cambios de kardex ] de ,la cual barre el Xml para pasar los campos de
     al ClientDataSet, para llamar al servidor de Recursos y grabar en su kardex el movimiento correspondiente al
     tipo de kardex que se especifica en el XML
     -ProcesaCambioKardex-
       Cambio de salario ABC
       Cambio de puesto ABC
       Cambio de turno ABC
       Cambio de niveles ABC
       Cambio de Tipo de N�mina ABC

       Al borrar kardex se ejecuta el m�todo de AntesBorrarKardex donde verifica si es de tipo VACA,PERM,INCA,
       los borra con sus correspondientes llamadas al servidor.

       Borra Kardex
       Borra Vacaciones
       Borra Permisos
       Borra Incapacidades

   EZ:Falta Optimizar c�digo

---------------------------------}
function TdmRecursos.ProcesaCambioKardex( const Datos: String ): String;
var
   iEmpleado: Integer;
   ErrorCount: Integer;
   TipoKardex: string;
   FechaKardex: TDate;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos,'KARDEX' );
             FOperacion := GetOperacion();
             TipoKardex := '';
             with cdsLista do
             begin
                  if ( FOperacion = K_OP_ALTA ) then
                  begin
                        Data := ServerRecursos.GetEditHisKardex(Empresa, iEmpleado, NullDateTime, '');
                        Append;
                        FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                        FieldByName( 'CB_REINGRE' ).AsString := K_GLOBAL_NO;
                        FieldByName( 'CB_NIVEL' ).AsInteger  := KARDEX_NIVEL_DEFAULT;
                        FieldByName( 'CB_GLOBAL' ).AsString  := K_GLOBAL_NO;
                        FieldByName( 'CB_STATUS' ).AsInteger  := Ord( skCapturado );
                        FieldByName( 'CB_AUTOSAL' ).AsString  := K_GLOBAL_NO;
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            FechaKardex := TagAsDate( GetNode( 'CB_FECHA', GetNode( 'LLAVE' ) ), NullDateTime );
                            TipoKardex := TagAsString( GetNode( 'CB_TIPO', GetNode( 'LLAVE' ) ), '' );
                       end;
                       if ( FOperacion = K_OP_BAJA ) then
                       begin
                            Data := ServerRecursos.GetHisKardex(Empresa, iEmpleado);
                            //Obtener el historial del Kardex ,posicionarlo en el registro que se quiere borrar
                            //Borrarlo y enviarlo al servidor
                            if Locate( 'CB_FECHA;CB_TIPO', VarArrayOf( [ ZetaCommonTools.FechaAsStr( FechaKardex ), TipoKardex ] ), [] ) then
                            begin
                                 {
                                 Antes de borrar preguntar si el tipo es PERM,VACA,INCA
                                 construir el DataSet, borrarlo y enviarlo al servidor
                                 }
                                 AntesBorrarKardex( TipoKardex, iEmpleado, FechaKardex );
                                 Delete;
                                 OnReconcileError := ReconcileError;
                                 if Reconciliar( ServerRecursos.GrabaBorrarKardex( Empresa, Delta, ErrorCount ) ) then
                                 begin
                                      LogInfo( Format( 'Se borr� kardex a empleado # %d', [ iEmpleado ] ) );
                                 end
                                 else
                                 begin
                                      LogError( Status, iEmpleado );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay movimiento de tipo "%s" en %s', [ TipoKardex, FormatDateTime( 'dd/mmm/yyyy', FechaKardex ) ] ), iEmpleado );
                            end;
                       end;
                       if ( FOperacion = K_OP_CAMBIO ) then
                       begin
                            Data := ServerRecursos.GetEditHisKardex(Empresa, iEmpleado, FechaKardex, TipoKardex );
                            if ( RecordCount > 0 ) then
                            begin
                                 Edit;
                                 FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay movimiento de tipo "%s" en %s', [ TipoKardex, FormatDateTime( 'dd/mmm/yyyy', FechaKardex ) ] ), iEmpleado );
                            end;
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) and ( FOperacion <> K_OP_BAJA ) then
             begin
                  ProcesaNodoData := TransfiereOtrosDatosAlta;
                  TransfiereXMLDataSet( 'KARDEX', cdsLista );
                  with cdsLista do
                  begin
                       FechaKardex := FieldByName( 'CB_FECHA' ).AsDateTime;
                       TipoKardex := FieldByName( 'CB_TIPO' ).AsString;
                       PostData;
                       if ( ChangeCount > 0 ) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerRecursos.GrabaHisKardex( Empresa, Delta, FListaPercepFijas, ErrorCount ) ) then
                            begin
                                 LogInfo( Format( 'Se cambio kardex a empleado # %d', [ iEmpleado ] ) );
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      LlaveNuevaKardex( iEmpleado, FechaKardex, TipoKardex );
                                 end;
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{----------------------------------------------------------------------------
     M�todos de utileria del kardex
     Para poder borrar el kardex tipo :  INCA, PERM, VACA
     EZ:Falta Optimizar c�digo
-----------------------------------------------------------------------------}
procedure TdmRecursos.AntesBorrarKardex( const TipoKardex: String;iEmpleado:Integer;FechaKardex:TDateTime);
var
     DatosEmpleado : OleVariant;
     ErrorCount :Integer;
     oDataSet: TZetaClientDataSet;
begin
     oDataSet := TZetaClientDataSet.Create(nil);
     if ( TipoKardex = K_T_VACA ) then
     begin
          with oDataSet do
          begin
               Data := ServerRecursos.GetHisVacacion(Empresa,iEmpleado,FechaDefault,DatosEmpleado);
               if Locate( 'VA_FEC_INI;VA_TIPO',VarArrayOf([ZetaCommonTools.FechaAsStr( FechaKardex ), 1 ]),[] ) then
               begin
                    Delete;    // No se puede indicar Borrar, por que este vuelve a preguntar si se quiere borrar
                    OnReconcileError := ReconcileError;
                    if Reconciliar( ServerRecursos.GrabaHisVacacion( Empresa, Delta,FALSE,ErrorCount ) ) then
                    begin
                         LogInfo( Format( 'Se borr� vacaciones a empleado # %d', [ iEmpleado ] ) );
                    end
                    else
                    begin
                         LogError( Status, iEmpleado );
                    end;
               end;
          end;
     end
     else if ( TipoKardex = K_T_PERM ) then
     begin
          with oDataSet do
          begin
               Data := ServerRecursos.GetHisPermiso(Empresa,iEmpleado);
               if Locate( 'PM_FEC_INI',ZetaCommonTools.FechaAsStr( FechaKardex ),[] ) then
               begin
                    Delete;
                    OnReconcileError := ReconcileError;
                    if Reconciliar( ServerRecursos.GrabaHistorial( Empresa,K_TAG_PERMISOS,Delta,ErrorCount ) ) then
                    begin
                         LogInfo( Format( 'Se borr� permiso a empleado # %d', [ iEmpleado ] ) );
                    end
                    else
                    begin
                         LogError( Status, iEmpleado );
                    end;
               end;
          end;
     end
     else if ( TipoKardex = K_T_INCA ) then
     begin
          with oDataSet do
          begin
               Data := ServerRecursos.GetHisIncapaci(Empresa,iEmpleado);
               if Locate( 'IN_FEC_INI',ZetaCommonTools.FechaAsStr( FechaKardex ),[] ) then
               begin
                    Delete;
                    OnReconcileError := ReconcileError;
                    if Reconciliar( ServerRecursos.GrabaHistorial( Empresa,K_TAG_INCAPACI,Delta,ErrorCount ) ) then
                    begin
                         LogInfo( Format( 'Se borr� la incapacidad a empleado # %d', [ iEmpleado ] ) );
                    end
                    else
                    begin
                         LogError( Status, iEmpleado );
                    end;
               end;
          end;
     end;
     FreeAndNil(oDataSet);
end;


{--------------------------------
     ProcesaKardexVacaciones
      Procesa la llamada del Servicio Web CambioKardexVacaciones de la cual barre el Xml para pasar los campos de
     al ClientDataSet, para llamar al servidor de Recursos y grabar el historial de vacaciones para ese empleado.
---------------------------------}
function TdmRecursos.ProcesaKardexVacaciones( const Datos: String ): String;
var
   iEmpleado: Integer;
   DatosEmpleado: OleVariant;
   ErrorCount: Integer;
   dInicio: TDate;
   eTipo: eTipoVacacion;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos,'VACACION' );
             FOperacion := GetOperacion();
             with cdsLista do
             begin
                  if ( FOperacion = K_OP_ALTA ) then
                  begin
                       Data := ServerRecursos.GetHisVacacion( Empresa, iEmpleado, FechaDefault, DatosEmpleado);
                       Append;
                       FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                       TransfiereXMLDataSet( 'VACACION', cdsLista );
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            dInicio := TagAsDate( GetNode( 'VA_FEC_INI', GetNode( 'LLAVE' ) ), NullDateTime );
                            eTipo := eTipoVacacion( TagAsInteger( GetNode( 'VA_TIPO', GetNode( 'LLAVE' ) ), 0 ) );
                       end;
                       Data := ServerRecursos.GetHisVacacion( Empresa, iEmpleado, dInicio, DatosEmpleado );
                       if ( RecordCount > 0 ) then
                       begin
                            if Locate( 'VA_FEC_INI;VA_TIPO', VarArrayOf( [ ZetaCommonTools.FechaAsStr( dInicio ), Ord( eTipo ) ] ), [] ) then
                            begin
                                 if ( FOperacion = K_OP_BAJA ) then
                                 begin
                                      Delete;
                                 end;
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      Edit;
                                      FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                                      TransfiereXMLDataSet( 'VACACION',cdsLista );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No tiene vacaci�n registrada en %s', [ FormatDateTime( 'dd/mmm/yyyy', dInicio ) ] ), iEmpleado );
                            end;
                       end
                       else
                       begin
                            Self.LogError( 'No hay vacaciones registradas', iEmpleado );
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) then
             begin
                  // Enviar Datos
                  with cdsLista do
                  begin
                       PostData;
                       if (ChangeCount > 0) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerRecursos.GrabaHisVacacion( Empresa, Delta, FALSE, ErrorCount ) ) then
                            begin
                                 LogInfo( Format( 'Se realiz� el cambio en las vacaciones del empleado # %d', [ iEmpleado ] ) );
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{--------------------------------
  --ProcesaKardexPermisos--
      Procesa la llamada del Servicio Web CambioKardexPermisos [ABC] de la cual barre el Xml para pasar
      los campos de al ClientDataSet, para llamar al servidor de Recursos y grabar el historial de permisos
      para ese empleado.
---------------------------------}
function TdmRecursos.ProcesaKardexPermisos( const Datos: String ): String;
var
   iEmpleado: Integer;
   ErrorCount: Integer;
   dInicio: TDate;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos, 'PERMISO' );
             FOperacion := GetOperacion();
             with cdsLista do
             begin
                  if ( FOperacion = K_OP_ALTA ) then
                  begin
                       Data := ServerRecursos.GetHisPermiso( Empresa, 0 );
                       Append;
                       FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                       TransfiereXMLDataSet( 'PERMISO', cdsLista );
                  end
                  else
                  begin
                       Data := ServerRecursos.GetHisPermiso( Empresa, iEmpleado );
                       if ( RecordCount > 0 ) then
                       begin
                            with XMLTools do
                            begin
                                 dInicio := TagAsDate( GetNode( 'PM_FEC_INI', GetNode( 'LLAVE' ) ), NullDateTime );
                            end;
                            if Locate( 'PM_FEC_INI', VarArrayOf( [ ZetaCommonTools.FechaAsStr( dInicio ) ] ), [] ) then
                            begin
                                 if ( FOperacion = K_OP_BAJA ) then
                                 begin
                                      Delete;
                                 end;
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      Edit;
                                      FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                                      TransfiereXMLDataSet( 'PERMISO',cdsLista );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No tiene permiso en %s', [ FormatDateTime( 'dd/mmm/yyyy', dInicio ) ] ), iEmpleado );
                            end;
                       end
                       else
                       begin
                            Self.LogError( 'No hay permisos registrados', iEmpleado );
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) then
             begin
                  //Enviar datos
                  with cdsLista do
                  begin
                       PostData;
                       if (ChangeCount > 0) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerRecursos.GrabaHistorial( Empresa,K_TAG_PERMISOS,Delta,ErrorCount ) ) then
                            begin
                                 LogInfo( Format( 'Se realiz� el cambio en los permisos del empleado # %d', [ iEmpleado ] ) );
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;


{--------------------------------
   --ProcesaKardexIncapacidades--
      Procesa la llamada del Servicio Web CambioKardexIncapacidades [ABC] la cual barre el Xml para pasar
      los campos de al ClientDataSet, para llamar al servidor de Recursos y grabar el historial de Incapacidades
      para ese empleado.
---------------------------------}
function TdmRecursos.ProcesaKardexIncapacidades( const Datos: String ): String;
var
   iEmpleado: Integer;
   ErrorCount: Integer;
   dInicio: TDate;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos,'INCAPACIDAD' );
             FOperacion := GetOperacion();
             with cdsLista do
             begin
                  if ( FOperacion = K_OP_ALTA ) then
                  begin
                       Data := ServerRecursos.GetHisIncapaci( Empresa, 0 );
                       Append;
                       FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                       TransfiereXMLDataSet( 'INCAPACIDAD',cdsLista );
                  end
                  else
                  begin
                       Data := ServerRecursos.GetHisIncapaci( Empresa, iEmpleado );
                       if ( RecordCount > 0 ) then
                       begin
                            with XMLTools do
                            begin
                                 dInicio := TagAsDate( GetNode( 'IN_FEC_INI', GetNode( 'LLAVE' ) ), NullDateTime );
                            end;
                            if Locate( 'IN_FEC_INI', VarArrayOf( [ ZetaCommonTools.FechaAsStr( dInicio ) ] ), [] ) then
                            begin
                                 if ( FOperacion = K_OP_BAJA ) then
                                 begin
                                      Delete;
                                 end;
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      Edit;
                                      FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                                      TransfiereXMLDataSet( 'INCAPACIDAD',cdsLista );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No tiene incapacidad en %s', [ FormatDateTime( 'dd/mmm/yyyy', dInicio ) ] ), iEmpleado );
                            end;
                       end
                       else
                       begin
                            Self.LogError( 'No hay incapacidades registradas', iEmpleado );
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) then
             begin
                  //Enviar datos
                  with cdsLista do
                  begin
                       PostData;
                       if (ChangeCount > 0) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerRecursos.GrabaHistorial( Empresa,K_TAG_INCAPACI,Delta,ErrorCount ) ) then
                            begin
                                 LogInfo( Format( 'Se realiz� el cambio en las incapacidades del empleado # %d', [ iEmpleado ] ) );
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{--------------------------------
  --ProcesaAsistenciaAutorizacion--
     Procesa la llamada del Servicio Web AsistenciaAutorizacion [ABC] la cual barre el Xml para pasar
     los campos de al ClientDataSet, para llamar al servidor de Asistencia y grabar las autorizaciones
     para ese empleado.

     Al agregar dos autorizaciones del mismo d�a, automaticamente suma las horas de autorizaci�,
      sin preguntar al usuario si quiere, sumar, sustituir, o ning�n tipo de operaci�n,
     Al editar cualquier autorizaci�n automaticamente sustituye el valor anterior de las horas de autorizaci�n

     Al borrar no efecuta ninguna operaci�n a las horas y borra las autorizaci�n de esa fecha.

---------------------------------}
function TdmRecursos.ProcesaAsistenciaAutorizacion( const Datos: String ): String;
var
   iEmpleado: Integer;
   eOperacion: eOperacionConflicto;
   ErrorCount: Integer;
   FechaAsistencia: TDate;
   ErrorData:OleVariant;
   eTipo: eTipoChecadas;
const
     K_AUTORIZACIONES_OFFSET = 5; // Para Compensar la diferencia entre eTipoChecadas y eAutorizaChecadas - CHECADAS.CH_TIPO
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos,'AUTORIZACION' );
             FechaAsistencia := GetFechaRegTarjeta(Datos,'AUTORIZACION');
             FOperacion := GetOperacion();
             eOperacion := ocReportar;
             eTipo := eTipoChecadas( 0 );
             with cdsLista do
             begin
                  Data := ServerAsistencia.GetAutorizaciones( Empresa, FechaAsistencia, ( FOperacion = K_OP_ALTA ) );
                  if ( FOperacion = K_OP_ALTA ) then
                  begin
                       Append;
                       FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                       FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
                       FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_NO;
                       FieldByName( 'CH_IGNORAR' ).AsString := K_GLOBAL_NO;
                       FieldByName( 'CH_H_REAL' ).AsString := K_HORA_NULA;
                       FieldByName( 'CH_H_AJUS' ).AsString := K_HORA_NULA;
                       FieldByName( 'HORAS' ).AsFloat := 0.0;
                       //Cada vez que se agregue un registro se inicializa el campo con Cero
                       FieldByName( 'CAMBIO' ).AsInteger := 1;
                       eOperacion := ocSumar;
                       TransfiereXMLDataSet( 'AUTORIZACION',cdsLista );
                  end
                  else
                  begin
                       if ( RecordCount > 0 ) then
                       begin
                            with XMLTools do
                            begin
                                 eTipo := eTipoChecadas( TagAsInteger( GetNode( 'CH_TIPO', GetNode( 'LLAVE' ) ), 0 ) + K_AUTORIZACIONES_OFFSET );
                            end;
                            if Locate( 'CH_H_REAL;CH_TIPO', VarArrayOf( [ K_HORA_NULA, Ord( eTipo ) ] ), [] ) then
                            begin
                                 if ( FOperacion = K_OP_BAJA ) then
                                 begin
                                      Delete;
                                      eOperacion := ocIgnorar;
                                 end;
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      Edit;
                                      FieldByName( 'CAMBIO' ).AsInteger := 1;
                                      FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                                      eOperacion := ocSustituir; //Se sustituye la autorizaci�n anterior
                                      TransfiereXMLDataSet( 'AUTORIZACION',cdsLista );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No tiene autorizaci�n de asistencia en %s', [ FormatDateTime( 'dd/mmm/yyyy', FechaAsistencia ) ] ), iEmpleado );
                            end;
                       end
                       else
                       begin
                            Self.LogError( Format( 'No hay autorizaciones registradas en %s', [ FormatDateTime( 'dd/mmm/yyyy', FechaAsistencia ) ] ), 0 );
                       end;
                  end;
             end;

             //adecuar el tipo de autorizacion con la checada
             with cdsLista do
             begin
                  if State in [ dsEdit, dsInsert ] then
                  begin
                       with FieldByName( 'CH_TIPO' ) do
                       begin
                            AsInteger := AsInteger + K_AUTORIZACIONES_OFFSET;
                            eTipo := eTipoChecadas( AsInteger );
                       end;
                       //horas aprobadas
                       if FieldByName( 'CH_HOR_DES' ).AsInteger > 0 then
                       begin
                            FieldByName( 'US_COD_OK' ).AsInteger := Usuario;
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) then
             begin
                  //Enviar datos
                  with cdsLista do
                  begin
                       PostData;
                       if ( ChangeCount > 0 ) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerAsistencia.GrabaAutorizaciones( Empresa, Delta, ErrorCount, ErrorData, Ord( eOperacion ) ) ) then
                            begin
                                 Data := ErrorData;
                                 LogInfo( Format( 'Se registr� el cambio en las autorizaciones del empleado # %d', [ iEmpleado ] ) );
                                 if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      LlaveNuevaAutorizacionAsistencia( iEmpleado, FechaAsistencia, eTipo );
                                 end;
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{--------------------------------
     ObtieneDefaultAsistenciaTarjetaDiaria
---------------------------------}
function TdmRecursos.ObtieneDefaultsAsistenciaTarjetaDiaria( const Datos: String ): String;
var
   iEmpleado: Integer;
   dTarjeta: TDate;
   Valor: Variant;
   Clasificacion: TDatosClasificacion;
   StatusHorario: TStatusHorario;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado(Datos, 'TARJETA' );
             dTarjeta := GetFechaRegTarjeta(Datos, 'TARJETA' );
             Clasificacion := ZetaCommonTools.DatosClasificacionFromVariant( ServerRecursos.GetDatosClasificacion( Self.Empresa, iEmpleado, dTarjeta ) );
             Valor := ServerAsistencia.GetHorarioStatus( Self.Empresa, VarArrayOf( [ Clasificacion.Turno, dTarjeta ] ) );
             with StatusHorario do
             begin
                  Horario := Valor[ 0 ];
                  Status := eStatusAusencia( Valor[ 1 ] );
             end;
             with XMLResult do
             begin
                  with Clasificacion do
                  begin
                       WriteValueString( 'CB_CLASIFI', Clasificacion );
                       WriteValueString( 'CB_TURNO', Turno );
                       WriteValueString( 'CB_PUESTO', Puesto );
                       WriteValueString( 'CB_NIVEL1', Nivel1 );
                       WriteValueString( 'CB_NIVEL2', Nivel2 );
                       WriteValueString( 'CB_NIVEL3', Nivel3 );
                       WriteValueString( 'CB_NIVEL4', Nivel4 );
                       WriteValueString( 'CB_NIVEL5', Nivel5 );
                       WriteValueString( 'CB_NIVEL6', Nivel6 );
                       WriteValueString( 'CB_NIVEL7', Nivel7 );
                       WriteValueString( 'CB_NIVEL8', Nivel8 );
                       WriteValueString( 'CB_NIVEL9', Nivel9 );
{$ifdef ACS}
                       WriteValueString( 'CB_NIVEL10', Nivel10 );
                       WriteValueString( 'CB_NIVEL11', Nivel11 );
                       WriteValueString( 'CB_NIVEL12', Nivel12 );
{$endif}
                       WriteValueFloat( 'CB_SALARIO', Salario );
                  end;
                  with StatusHorario do
                  begin
                       WriteValueString( 'HO_CODIGO', Horario );
                       WriteValueInteger( 'AU_STATUS', Ord( Status ) );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;


{--------------------------------
     ProcesaAsistenciaTarjetaDiaria
---------------------------------}
function TdmRecursos.ProcesaAsistenciaTarjetaDiaria( const Datos: String ): String;
var
   iEmpleado, iTarjeta, iChecadas, iErrorTarjeta, iErrorChecadas: Integer;
   FChecadasBuffer,ChResults,oChecadas,oTarjeta,FNewTarjeta,FNewChecadas : OleVariant;
   cdsChecadas: TZetaClientDataSet;
   FechaRegTarjeta : TDate;
begin
     Result := VACIO;
     cdsChecadas := TZetaClientDataSet.Create(nil);
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             FOperacion := GetOperacion();
             iEmpleado := GetNumEmpleado( Datos,'TARJETA' );
             FechaRegTarjeta := GetFechaRegTarjeta(Datos,'TARJETA');
             with cdsLista do
             begin
                  Data := ServerAsistencia.GetTarjeta( Empresa, iEmpleado, FechaRegTarjeta, FChecadasBuffer );
             end;
             if ( FOperacion = K_OP_BAJA ) then
             begin
                  cdsLista.Delete;
             end
             else
             begin
                  with cdsLista do
                  begin
                       if ( RecordCount > 0 ) then
                       begin
                            Edit;
                       end
                       else
                       begin
                            Append;
                       end;
                  end;
                  with cdsChecadas do
                  begin
                       Data :=  FChecadasBuffer;
                       First;
                       while not Eof do
                       begin
                            Delete;
                       end;
                  end;
                  TransfiereXMLDataSet( 'TARJETA',cdsLista );
                  with cdsLista do
                  begin
                       PostData;
                       iTarjeta := ChangeCount;
                       if ( iTarjeta = 0 ) then
                       begin
                            { Siempre debe haber un delta en este dataset }
                            { para que la validaci�n de NO Modificar Tarjetas }
                            { de un Per�odo Ya Afectado ( Global 132 ) }
                            { tenga efecto ( la validaci�n se efect�a en un Trigger de Ausencia ) }
                            Edit;
                            with FieldByName( 'AU_HORASCK' ) do
                            begin
                                 AsFloat := AsFloat + 1;
                            end;
                            Post;
                       end;
                  end;
                  if not LogHayError then
                  begin
                       LeerChecadas( 'CHECADAS', cdsChecadas, iEmpleado, FechaRegTarjeta );
                  end;
             end;
             if not LogHayError then
             begin
                  with cdsChecadas do
                  begin
                       PostData;
                       iChecadas := ChangeCount;
                       if ( iChecadas = 0 ) then
                       begin
                            oChecadas := Null;
                       end
                       else
                       begin
                            oChecadas := Delta;
                       end;
                  end;
                  with cdsLista do
                  begin
                       PostData;
                       iTarjeta := ChangeCount;
                       oTarjeta := Delta;
                  end;
                  if ( iTarjeta > 0 ) or ( iChecadas > 0 ) then
                  begin
                       iErrorTarjeta := iTarjeta;
                       iErrorChecadas := iChecadas;
                       cdsLista.OnReconcileError := ReconcileError;
                       cdsChecadas.OnReconcileError := ReconcileError;
                       oTarjeta := ServerAsistencia.GrabaTarjeta( Empresa, oTarjeta, oChecadas, iErrorTarjeta, iErrorChecadas, ChResults, FNewTarjeta, FNewChecadas );
                       if ( ( iErrorTarjeta = 0 ) or cdsLista.Reconcile( oTarjeta ) )then
                       begin
                          LogInfo( Format( 'Se agreg� tarjeta a empleado # %d', [ iEmpleado ] ) );
                          if ( ( iErrorChecadas = 0 ) or cdsChecadas.Reconcile( ChResults ) )then
                          begin
                               LogInfo( Format( 'Se agregaron la(s) checada(s) al empleado # %d', [ iEmpleado ] ) )
                          end
                          else
                          begin
                               LogError( Status , iEmpleado );
                          end;
                       end
                       else
                       begin
                            LogError( Status, iEmpleado );
                       end;
                  end;
             end;
        end;
        finally
               LogEnd;
        end;
        Result := LogAsText;
end;

{--------------------------------
   M�todos de utileria Asistencia Tarjeta Diaria
---------------------------------}

procedure TdmRecursos.LeerChecadas( sTag:String; oDataSet: TZetaClientDataSet;Empleado:Integer;FechaAusencia:TDate );
const
     K_CHECADAS = 'CHECADAS';
     K_CHECADA = 'CHECADA';
var
   oNodo: TZetaXMLNode;
   oItem: TZetaXMLNode;
begin
     oNodo := XMLTools.GetNode(sTag);
     if ( sTag = K_CHECADAS ) then
     begin
          with XMLTools do
          begin
               if HasChildren( oNodo ) then               // Viene seccion de la tabla
               begin
                    oItem := GetFirstChild( oNodo );
                    while Assigned( oItem ) do
                    begin
                          with oDataSet do
                          begin
                               Append;
                               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
                               FieldByName( 'CH_H_REAL' ).AsString := TagAsString( oItem, '' );
                               FieldByName( 'CH_H_AJUS' ).AsString := TagAsString( oItem, '' );
                               FieldByName( 'AU_FECHA' ).AsDateTime := FechaAusencia;
                               FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                               FieldByName( 'CH_SISTEMA' ).AsString := K_GLOBAL_NO;
                               FieldByName( 'CH_GLOBAL' ).AsString := K_GLOBAL_NO;
                               PostData;
                               oItem := GetNextSibling( oItem );
                          end;
                    end;
               end;
          end;
     end
end;


{--------------------------------
   --ProcesaNominaExcepcionMonto--
     Procesa la llamada del Servicio Web NominaExcepcionMonto [ABC] la cual barre el Xml para pasar 
     los campos de al ClientDataSet, para llamar al servidor de Nomina y grabar las excepciones de monto
     para ese empleado. 

     Al agregar dos excepciones del mismo d�a, automaticamente sustituye( petici�n 8/8/2007 AR antes se sumaba ) los montos de
     sin preguntar al usuario si quiere, sumar, sustituir, o ning�n tipo de operaci�n,

     Al editar cualquier excepci�n automaticamente sustituye el valor anterior del monto de la excepci�n

     Al borrar no efecuta ninguna operaci�n a las horas y borra la excepci�n de monto de esa fecha.
---------------------------------}
function TdmRecursos.ProcesaNominaExcepcionMonto( const Datos: String ): String;
var
   iEmpleado: Integer;
   iConcepto: Integer;
   iTipoConcepto :Integer;
   sReferencia: String;
   ErrorCount: Integer;
   Parametros: TZetaParams;
   Periodo: TPeriodo;
   ErrorData :OleVariant;
begin
     Result := VACIO;
     LogInit;
     Parametros := TZetaParams.Create(nil);
     Periodo := TPeriodo.Create();
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos,'EXCEPCION' );
             Periodo := GetPeriodo( Datos,'PERIODO' );
             FOperacion := GetOperacion();
             with Periodo do
             begin
                  with cdsLista do
                  begin
                       Data := ServerNomina.GetDatosExcepMontos(Empresa,Anio,Tipo,Numero, ( FOperacion = K_OP_ALTA ));
                       with XMLTools do
                       begin
                            iConcepto := TagAsInteger( GetNode( 'CO_NUMERO', GetNode( 'EXCEPCION' ) ), 0 );
                       end;
                       with cdsQueryGeneral do
                       begin
                            try
                               Data := ServerConsultas.GetQueryGral( Empresa, Format('select CO_TIPO from CONCEPTO where ( CO_NUMERO = %d)',[iConcepto] ),True );
                               if ( RecordCount > 0 ) then
                               begin
                                     iTipoConcepto := FieldByName( 'CO_TIPO' ).AsInteger;
                               end
                               else
                               begin
                                    iTipoConcepto := 0;

                               end;
                               Active := False;
                            except
                                  on Error: Exception do
                                  begin
                                       iTipoConcepto := 0;
                                  end;
                            end;
                       end;
                       if ( FOperacion = K_OP_ALTA ) then
                       begin
                            Append;
                            FieldByName( 'PE_YEAR' ).AsInteger := Anio;
                            FieldByName( 'PE_TIPO' ).AsInteger := Tipo;
                            FieldByName( 'PE_NUMERO' ).AsInteger := Numero;
                            FieldByName( 'MO_PERCEPC' ).AsFloat := 0.0;
                            FieldByName( 'MO_DEDUCCI' ).AsFloat := 0.0;
                            FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                            FieldByName( 'MO_MONTO' ).AsFloat := 0.0;
                            TransfiereXMLDataSet('EXCEPCION',cdsLista);
                            with Parametros do
                            begin
                                 AddInteger( 'Operacion', Ord(ocSumar) ); //Sumar
                            end;
                       end
                       else
                       begin
                            if ( RecordCount > 0 ) then
                            begin
                                 with XMLTools do
                                 begin
                                      iConcepto := TagAsInteger( GetNode( 'CO_NUMERO', GetNode( 'LLAVE' ) ), 0 );
                                      sReferencia := TagAsString( GetNode( 'MO_REFEREN', GetNode( 'LLAVE' ) ), '' );
                                 end;
                                 if Locate( 'CB_CODIGO;CO_NUMERO;MO_REFEREN', VarArrayOf( [ iEmpleado, iConcepto, sReferencia ] ), [] ) then
                                 begin
                                      if ( FOperacion = K_OP_BAJA ) then
                                      begin
                                           Delete;
                                           with Parametros do
                                           begin
                                                AddInteger( 'Operacion', Ord(ocNinguno) ); //Borrando
                                           end;
                                      end;
                                      if ( FOperacion = K_OP_CAMBIO ) then
                                      begin
                                           Edit;
                                           FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                                           TransfiereXMLDataSet('EXCEPCION',cdsLista);
                                           with Parametros do
                                           begin
                                                AddInteger( 'Operacion', Ord(ocSustituir) ); //Sustituir
                                           end;
                                      end;
                                 end
                                 else
                                 begin
                                      Self.LogError( Format( 'Empleado %d no tiene excepci�n de monto para el concepto %d en este periodo ( A�o=%d Tipo=%d N�mero=%d )', [ iEmpleado, iConcepto, Anio, Tipo, Numero ] ), iEmpleado );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay excepciones de monto en este periodo ( A�o=%d Tipo=%d N�mero=%d )', [ Anio, Tipo, Numero ] ), 0 );
                            end;
                       end;
                  end;
             end;

             //Enviar datos
             with cdsLista do
             begin

                  if State in [ dsEdit, dsInsert ] then
                  begin
                       // Asignar Percepcion o Deduccion
                       if ( eTipoConcepto( iTipoConcepto ) in [ coDeduccion, coObligacion ] ) then
                       begin
                            FieldByName( 'MO_DEDUCCI' ).AsFloat := FieldByName( 'MO_MONTO' ).AsFloat;
                            FieldByName( 'MO_PERCEPC' ).AsFloat := 0.0;
                       end
                       else
                       begin
                            FieldByName( 'MO_PERCEPC' ).AsFloat := FieldByName( 'MO_MONTO' ).AsFloat;
                            FieldByName( 'MO_DEDUCCI' ).AsFloat := 0.0;
                       end;
                       Post;
                  end;
                  if (ChangeCount > 0) then
                  begin
                       OnReconcileError := ReconcileError;
                       with Parametros do
                       begin
                            with Periodo do
                            begin
                                 AddInteger( 'Year', Anio );
                                 AddInteger( 'Tipo', Tipo );
                                 AddInteger( 'Numero', Numero );
                            end;
                       end;
                       if Reconciliar( ServerNomina.GrabaMovMontos( Empresa, Parametros.VarValues, Delta, ErrorCount, ErrorData ) ) then
                       begin
                            LogInfo( Format( 'Se realizo el cambio en las excepciones de monto del empleado # %d', [ iEmpleado ] ) );
                       end
                       else
                       begin
                            LogError( Status, iEmpleado );
                       end;
                  end
                  else
                  begin
                       LogInfo( 'No hay cambios', iEmpleado );
                  end;
             end;
        end;
     finally
            LogEnd;
            FreeAndNil(Periodo);
            FreeAndNil(Parametros);
     end;
     Result := LogAsText;
end;

{--------------------------------
  --ProcesaNominaExcepcionDias ,ProcesaNominaExcepcionHoras --
      Procesa la llamada del Servicio Web NominaExcepcionDias,NominaExcepcionHoras [ABC] la cual barre
      el Xml para pasar los campos de al ClientDataSet, para llamar al servidor de Nomina y grabar
      las excepciones de horas o de d�as segun el parametro para ese empleado.

     Al agregar dos excepciones del mismo d�a, automaticamente suma los montos de
      sin preguntar al usuario si quiere, sumar, sustituir, o ning�n tipo de operaci�n,
     Al editar cualquier excepci�n automaticamente sustituye el valor anterior del monto de la excepci�n

     Al borrar no efecuta ninguna operaci�n a las horas y borra la excepci�n de monto de esa fecha.
---------------------------------}
function TdmRecursos.ProcesaNominaExcepcionDiasHoras( const Datos: String; const EsTipoHora: Boolean ): String;
const
     aTipo: array[ false..true ] of Char = ( ZetaCommonClasses.K_TIPO_DIA, ZetaCommonClasses.K_TIPO_HORA );
var
   iEmpleado: Integer;
   ErrorCount: Integer;
   Parametros: TZetaParams;
   Periodo: TPeriodo;
   ErrorData,oTotales :OleVariant;
   dFechaOriginal:TDate;
   sMotivoOriginal: String;
begin
     Result := VACIO;
     LogInit;
     Parametros := TZetaParams.Create(nil);
     Periodo := TPeriodo.Create();
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos,'EXCEPCION' );
             Periodo := GetPeriodo( Datos,'PERIODO' );
             FOperacion := GetOperacion();
             with Periodo do
             begin
                  with cdsLista do
                  begin
                       if ( FOperacion = K_OP_ALTA ) then
                       begin
                            Data := ServerNomina.GetFaltas( Empresa, iEmpleado,Anio,Tipo,Numero,oTotales );
                            Append;
                            FieldByName( 'PE_YEAR' ).AsInteger := Anio;
                            FieldByName( 'PE_TIPO' ).AsInteger := Tipo;
                            FieldByName( 'PE_NUMERO' ).AsInteger := Numero;
                            FieldByName( 'FA_DIA_HOR' ).AsString := aTipo[ EsTipoHora ];
                            TransfiereXMLDataSet('EXCEPCION',cdsLista);
                            with Parametros do
                            begin
                                 AddInteger( 'Operacion', Ord(ocSumar) ); //Sumando
                            end;
                       end
                       else
                       begin
                            Data := ServerNomina.GetFaltas( Empresa, iEmpleado,Anio,Tipo,Numero,oTotales );
                            if ( RecordCount > 0 ) then
                            begin
                                 with XMLTools do
                                 begin
                                      dFechaOriginal := TagAsDate( GetNode( 'FA_FEC_INI', GetNode( 'LLAVE' ) ), ZetaCommonClasses.NullDateTime );
                                      sMotivoOriginal := TagAsString( GetNode( 'FA_MOTIVO', GetNode( 'LLAVE' ) ), '' );
                                 end;
                                 if Locate( 'FA_DIA_HOR;FA_FEC_INI;FA_MOTIVO', VarArrayOf( [ aTipo[ EsTipoHora ], ZetaCommonTools.FechaAsStr( dFechaOriginal ), sMotivoOriginal ] ),[] ) then
                                 begin
                                      if ( FOperacion = K_OP_BAJA ) then
                                      begin
                                           Delete;
                                           with Parametros do
                                           begin
                                                AddInteger( 'Operacion', Ord(ocNinguno) ); //Borrando
                                           end;
                                      end;
                                      if ( FOperacion = K_OP_CAMBIO ) then
                                      begin
                                           Edit;
                                           // FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                                           TransfiereXMLDataSet('EXCEPCION',cdsLista);
                                           with Parametros do
                                           begin
                                                AddInteger( 'Operacion', Ord(ocSustituir) ); //Sustituir
                                           end;
                                      end;
                                 end
                                 else
                                 begin
                                      Self.LogError( 'Excepcion a borrar no existe', iEmpleado );
                                 end;
                            end
                            else
                            begin
                                 Self.LogError( Format( 'No hay excepciones de dia/hora en este periodo ( A�o=%d Tipo=%d N�mero=%d )', [ Anio, Tipo, Numero ] ), 0 );
                            end;
                       end;
                  end;
             end;
             if not ( Self.LogHayError ) then
             begin
                  //Enviar Datos
                  with cdsLista do
                  begin
                       PostData;
                       if (ChangeCount > 0) then
                       begin
                            OnReconcileError := ReconcileError;
                            with Parametros do
                            begin
                                 with Periodo do
                                 begin
                                      AddInteger( 'Year', Anio );
                                      AddInteger( 'Tipo', Tipo );
                                      AddInteger( 'Numero', Numero );
                                 end;
                            end;
                            if Reconciliar( ServerNomina.GrabaFaltas( Empresa, Parametros.VarValues, Delta, ErrorCount, ErrorData ) ) then
                            begin
                                 LogInfo( Format( 'Se realiz� el cambio en las excepciones de d�a(s)/hora(s) del empleado # %d', [ iEmpleado ] ) );
                            end
                            else
                            begin
                                 LogError( Status, iEmpleado );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios', iEmpleado );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
            FreeAndNil(Periodo);
            FreeAndNil(Parametros);
     end;
     Result := LogAsText;
end;

{------------------------------------------------------------------------------
   Procesa la llamada a ABC de Parientes
-------------------------------------------------------------------------------}
function TdmRecursos.ProcesaEmpleadoParientes( const Datos: String ): String;
var
   iEmpleado: Integer;
   Folio,Relacion : Integer;
   ErrorCount: Integer;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             iEmpleado := GetNumEmpleado( Datos ,'PARIENTE' );
             FOperacion := GetOperacion();
             with cdsLista do
             begin
                  Data := ServerRecursos.GetEmpParientes( Empresa, iEmpleado );
                  if( FOperacion = K_OP_ALTA )then
                  begin
                       Append;
                       FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                       FieldByName( 'PA_RELACIO' ).AsInteger:= 0;
                       FieldByName( 'PA_FOLIO' ).AsInteger  := 1;
                       FieldByName( 'PA_SEXO' ).AsString    := 'M';
                       FieldByName( 'PA_FEC_NAC' ).AsDateTime := FechaDefault;
                       TransfiereXMLDataSet('PARIENTE',cdsLista);
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            Folio := TagAsInteger( GetNode( 'PA_FOLIO', GetNode( 'LLAVE' ) ),1 );
                            Relacion := TagAsInteger( GetNode( 'PA_RELACIO', GetNode( 'LLAVE' ) ), 0 );
                       end;
                       if Locate( 'PA_FOLIO;PA_RELACIO', VarArrayOf( [ Folio ,Relacion ] ), [] ) then
                       begin
                            if ( FOperacion = K_OP_BAJA )then
                            begin
                                 Delete;
                            end
                            else
                                if ( FOperacion = K_OP_CAMBIO ) then
                                begin
                                     Edit;
                                     TransfiereXMLDataSet('PARIENTE',cdsLista);
                                end;
                       end
                       else
                       begin
                             Self.LogError( Format( 'No hay pariente registrado con folio: %d y tiene una relaci�n de %s', [ Folio, ObtieneElemento( lfTipoPariente, Relacion ) ] ), iEmpleado );
                       end;
                  end;
                  PostData;
                  if ( ChangeCount > 0 ) then
                  begin
                       OnReconcileError := ReconcileError;
                       if Reconciliar( ServerRecursos.GrabaEmpParientes(Empresa,Delta,ErrorCount ) ) then
                       begin
                            LogInfo( Format( 'Se realiz� el cambio en los parientes del empleado %d ', [ iEmpleado ] ) );
                       end
                       else
                       begin
                            LogError( Status, iEmpleado );
                       end;
                  end
                  else
                  begin
                       LogInfo( 'No hay cambios', iEmpleado );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{-------------------------------------------------------------------------------
   Obtiene el tag del cat�logo de texto
-------------------------------------------------------------------------------}
function TdmRecursos.GetTagNivel(const sCatalogo:string):Integer;
const
     K_TAG_NIVEL_1 = 18;
     K_TAG_NIVEL_2 = 19;
     K_TAG_NIVEL_3 = 20;
     K_TAG_NIVEL_4 = 21;
     K_TAG_NIVEL_5 = 22;
     K_TAG_NIVEL_6 = 23;
     K_TAG_NIVEL_7 = 24;
     K_TAG_NIVEL_8 = 25;
     K_TAG_NIVEL_9 = 26;
{$ifdef ACS}
     K_TAG_NIVEL_10 = 27;
     K_TAG_NIVEL_11 = 28;
     K_TAG_NIVEL_12 = 29;
{$endif}
begin
      if (sCatalogo = 'NIVEL1')then
      begin
           Result := K_TAG_NIVEL_1;
      end
      else if (sCatalogo = 'NIVEL2')then
      begin
           Result := K_TAG_NIVEL_2;
      end
      else if (sCatalogo = 'NIVEL3')then
      begin
           Result := K_TAG_NIVEL_3;
      end
      else if (sCatalogo = 'NIVEL4')then
      begin
           Result := K_TAG_NIVEL_4;
      end
      else if (sCatalogo = 'NIVEL5')then
      begin
           Result := K_TAG_NIVEL_5;
      end
      else if (sCatalogo = 'NIVEL6')then
      begin
           Result := K_TAG_NIVEL_6;
      end
      else if (sCatalogo = 'NIVEL7')then
      begin
           Result := K_TAG_NIVEL_7;
      end
      else if (sCatalogo = 'NIVEL8')then
      begin
           Result := K_TAG_NIVEL_8;
      end
      else if (sCatalogo = 'NIVEL9')then
      begin
           Result := K_TAG_NIVEL_9;
      end
{$ifdef ACS}
      else if (sCatalogo = 'NIVEL10')then
      begin
           Result := K_TAG_NIVEL_10;
      end
      else if (sCatalogo = 'NIVEL11')then
      begin
           Result := K_TAG_NIVEL_11;
      end
      else if (sCatalogo = 'NIVEL12')then
      begin
           Result := K_TAG_NIVEL_12;
      end
{$endif}
      else
      begin
           Result := 0;
      end;
end;

{-------------------------------------------------------------------------------
   Procesa el cat�logo de Texto
-------------------------------------------------------------------------------}
function TdmRecursos.ProcesaCatalogoTexto (const Datos: string):string;
var
   ErrorCount,i,Tags: Integer;
   Codigo,Catalogo:string;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             FOperacion := GetOperacion();
             with XMLTools do
             begin
                  Catalogo := TagAsString( GetNode('CATALOGO'),'');
             end;
             Tags := GetTagNivel( Catalogo );
             if (Tags = 0 )then
             begin
                  LogError( Format('El cat�logo: %s;no tiene asignado un tag en el servidor ',[Catalogo] ));
             end
             else
             begin
                  with cdsLista do
                  begin
                       Data := ServerTablas.GetTabla( Empresa,Tags);
                       if( FOperacion = K_OP_ALTA )then
                       begin
                            Append;
                            for i := 0 to ( FieldCount - 1 ) do
                            begin
                                 if Fields[ i ] is TNumericField then
                                    Fields[ i ].AsFloat := 0
                                 else
                                     Fields[ i ].AsString := '';
                            end;
                            TransfiereXMLDataSet(Catalogo,cdsLista);
                       end
                       else
                       begin
                            with XMLTools do
                            begin
                                 Codigo := TagAsString( GetNode( 'TB_CODIGO', GetNode( 'LLAVE' ) ),'');
                            end;
                            if Locate( 'TB_CODIGO', VarArrayOf( [ Codigo ] ), [] ) then
                            begin
                                 if( FOperacion = K_OP_BAJA )then
                                 begin
                                      Delete;
                                 end
                                 else if ( FOperacion = K_OP_CAMBIO ) then
                                 begin
                                      Edit;
                                      TransfiereXMLDataSet(Catalogo,cdsLista);
                                 end;
                            end
                            else
                            begin
                                  Self.LogError( Format( 'No existe un �rea registrada con el c�digo: %s ', [ Codigo ] ) );
                            end;
                       end;
                       PostData;
                       if ( ChangeCount > 0 ) then
                       begin
                            OnReconcileError := ReconcileError;
                            if Reconciliar( ServerTablas.GrabaTabla(Empresa,Tags,Delta,ErrorCount) ) then
                            begin
                                 LogInfo(  'Se realiz� el cambio en el cat�logo de '+Catalogo );
                            end
                            else
                            begin
                                 LogError( Status );
                            end;
                       end
                       else
                       begin
                            LogInfo( 'No hay cambios' );
                       end;
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{-------------------------------------------------------------------------------
   Procesa el cat�logo de Puestos
-------------------------------------------------------------------------------}
function TdmRecursos.ProcesaCatalogoPuestos(const Datos: String): string;
var
   ErrorCount: Integer;
   Codigo : string;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             FOperacion := GetOperacion();
             with cdsLista do
             begin
                  Data := ServerCatalogos.GetPuestos( Empresa );
                  if( FOperacion = K_OP_ALTA )then
                  begin
                       Append;
                       FieldByName('PU_TIPO').AsInteger := Ord( ptoEmpleado );
                       FieldByName('PU_PLAZAS').AsInteger := 1;
                       FieldByName('PU_ACTIVO').AsString := K_GLOBAL_SI;
                       FieldByName('PU_AUTOSAL').AsString := K_USAR_GLOBAL;
                       FieldByName('PU_CHECA').AsString := K_USAR_GLOBAL;
                       TransfiereXMLDataSet('PUESTO',cdsLista);
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            Codigo := TagAsString( GetNode( 'PU_CODIGO', GetNode( 'LLAVE' ) ),'' );
                       end;
                       if Locate( 'PU_CODIGO', VarArrayOf( [ Codigo ] ), [] ) then
                       begin
                            if( FOperacion = K_OP_BAJA )then
                            begin
                                 Delete;
                            end
                            else if ( FOperacion = K_OP_CAMBIO ) then
                            begin
                                 Edit;
                                 TransfiereXMLDataSet('PUESTO',cdsLista);
                            end;
                       end
                       else
                       begin
                             Self.LogError( Format( 'No existe un puesto registrado con el c�digo: %s ', [ Codigo ] ) );
                       end;
                  end;
                  PostData;
                  if ( ChangeCount > 0 ) then
                  begin
                       OnReconcileError := ReconcileError;
                       if Reconciliar( ServerCatalogos.GrabaCatalogo(Empresa,K_TAG_PUESTOS,Delta,ErrorCount) ) then
                       begin
                            LogInfo(  'Se realiz� el cambio en el cat�logo de Puestos ' );
                       end
                       else
                       begin
                            LogError( Status );
                       end;
                  end
                  else
                  begin
                       LogInfo( 'No hay cambios' );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{-------------------------------------------------------------------------------
   Procesa el cat�logo de Turnos
-------------------------------------------------------------------------------}
function TdmRecursos.ProcesaCatalogoTurnos(const Datos: String): string;
var
   ErrorCount: Integer;
   Codigo: string;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             FOperacion := GetOperacion();
             with cdsLista do
             begin
                  Data := ServerCatalogos.GetTurnos( Empresa );
                  if( FOperacion = K_OP_ALTA )then
                  begin
                       Append;
                       FieldByName( 'TU_DIAS' ).AsInteger := K_DIAS_SEMANA_HABILES;
                       FieldByName( 'TU_JORNADA' ).AsInteger := 48;
                       FieldByName( 'TU_DOBLES' ).AsInteger := 9;
                       FieldByName( 'TU_HORARIO' ).AsInteger := Ord( ttNormal );
                       FieldByName( 'TU_TIP_JOR' ).AsInteger := Ord( tjSemanaCompleta );
                      // FieldByName( 'TU_NOMINA' ).AsInteger := Ord( tpSemanal );
                       FieldByName( 'TU_TIP_1' ).AsInteger := Ord( auHabil );
                       FieldByName( 'TU_TIP_2' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
                       FieldByName( 'TU_TIP_3' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
                       FieldByName( 'TU_TIP_4' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
                       FieldByName( 'TU_TIP_5' ).AsInteger := FieldByName( 'TU_TIP_1' ).AsInteger;
                       FieldByName( 'TU_TIP_6' ).AsInteger := Ord( auSabado );
                       FieldByName( 'TU_TIP_7' ).AsInteger := Ord( auDescanso );
                       FieldByName( 'TU_DOMINGO' ).AsFloat := 0;
                       FieldByName( 'TU_FESTIVO' ).AsString := K_GLOBAL_NO;
                       FieldByName( 'TU_VACA_HA' ).AsFloat := K_DIAS_VACACION_HABIL;
                       FieldByName( 'TU_VACA_SA' ).AsFloat := 0;
                       FieldByName( 'TU_VACA_DE' ).AsFloat := 0;
                       TransfiereXMLDataSet('TURNO',cdsLista);
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            Codigo := TagAsString( GetNode( 'TU_CODIGO', GetNode( 'LLAVE' ) ),'');
                       end;
                       if Locate( 'TU_CODIGO', VarArrayOf( [ Codigo ] ), [] ) then
                       begin
                            if( FOperacion = K_OP_BAJA )then
                            begin
                                 Delete;
                            end
                            else if ( FOperacion = K_OP_CAMBIO ) then
                            begin
                                 Edit;
                                 TransfiereXMLDataSet('TURNO',cdsLista);
                            end;
                       end
                       else
                       begin
                             Self.LogError( Format( 'No existe un turno registrado con el c�digo: %s ', [ Codigo ] ) );
                       end;
                  end;
                  PostData;
                  if ( ChangeCount > 0 ) then
                  begin
                       OnReconcileError := ReconcileError;
                       if Reconciliar( ServerCatalogos.GrabaCatalogo(Empresa,K_TAG_TURNOS,Delta,ErrorCount) ) then
                       begin
                            LogInfo(  'Se realiz� el cambio en el cat�logo de Turnos ' );
                       end
                       else
                       begin
                            LogError( Status );
                       end;
                  end
                  else
                  begin
                       LogInfo( 'No hay cambios' );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

{-------------------------------------------------------------------------------
   Procesa Pr�stamos
   En este m�todo se procesa las llamadas del ABC de los prestamos
   haciendo las validaciones del servidor.
-------------------------------------------------------------------------------}
function TdmRecursos.ProcesaPrestamos(const Datos: String): string;
var
   ErrorCount,iEmpleado: Integer;
   sReferencia: string;
   iEmpleadoLlave,iTipoPrestamo :Integer;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             FOperacion := GetOperacion();
             iEmpleado := GetNumEmpleado( Datos ,'PRESTAMO' );
             with cdsLista do
             begin
                  Data := ServerRecursos.GetHisPrestamos( Empresa,iEmpleado );
                  if( FOperacion = K_OP_ALTA )then
                  begin
                       Append;
                       FieldByName('CB_CODIGO').AsInteger := iEmpleado;
                       FieldByName('PR_CARGOS').AsFloat := 0;
                       FieldByName('PR_ABONOS').AsFloat := 0;
                       FieldByName('PR_NUMERO').AsInteger := 0;
                       FieldByName('PR_SALDO_I').AsFloat := 0;
                       FieldByName('PR_STATUS').AsInteger := Ord( spActivo );
                       FieldByName('PR_TOTAL').AsFloat  := 0;
                       FieldByName('PR_SALDO').AsFloat  := 0;
                       FieldByName('PR_FECHA').AsDateTime := FechaDefault;
                       FieldByName('PR_REFEREN').AsString := VACIO;
                       TransfiereXMLDataSet('PRESTAMO',cdsLista);
                  end
                  else
                  begin
                       with XMLTools do
                       begin
                            sReferencia := TagAsString( GetNode( 'PR_REFEREN', GetNode( 'LLAVE' ) ),'');
                            iTipoPrestamo := TagAsInteger ( GetNode( 'PR_TIPO', GetNode( 'LLAVE' ) ),0);
                            iEmpleadoLlave  := TagAsInteger( GetNode( 'CB_CODIGO', GetNode( 'LLAVE' ) ),0);
                       end;
                       if Locate( 'PR_REFEREN;PR_TIPO;CB_CODIGO', VarArrayOf( [ sReferencia ,iTipoPrestamo,iEmpleadoLlave ] ), [] ) then
                       begin
                            if( FOperacion = K_OP_BAJA )then
                            begin
                                 Delete;
                            end
                            else if ( FOperacion = K_OP_CAMBIO ) then
                            begin
                                 Edit;
                                 TransfiereXMLDataSet('PRESTAMO',cdsLista);
                            end;
                       end
                       else
                       begin
                             Self.LogError( Format( 'No existe un pr�stamo registrado al empleado:%d con la referencia: %s con el tipo: %d ', [ iEmpleadoLlave,sReferencia , iTipoPrestamo ] ) );
                       end;
                  end;
                  SetStatusPrestamo(cdsLista);
                  PostData;
                  if ( ChangeCount > 0 ) then
                  begin
                       OnReconcileError := ReconcileError;
                       if Reconciliar( ServerRecursos.GrabaHistorial( Empresa, TAG_PRESTAMOS, Delta, ErrorCount )  ) then
                       begin
                            LogInfo( Format( 'Se realiz� el cambio en los pr�stamos del empleado: %d',[iEmpleado] ));
                       end
                       else
                       begin
                            LogError( Status );
                       end;
                  end
                  else
                  begin
                       LogInfo( 'No hay cambios' );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;

end;
{-------------------------------------------------------------------------------
   ProcesaPrestaAcarAbo
   En este m�todo se procesa las llamadas del ABC de los Cargos y Abonos del
   prestamo que viene como llave
-------------------------------------------------------------------------------}
function TdmRecursos.ProcesaPrestaAcarAbo(const Datos: String): string;
var
  sReferencia,sTipoPrestamo: string;
  ErrorCount,iEmpleado :Integer;
  dFecha: TDate;
  cdsPCarAbo : TZetaClientDataSet;
begin
     Result := VACIO;
     LogInit;
     cdsPCarAbo := TZetaClientDataSet.Create(nil);
     try
        if ConectaEmpresa( Datos ) then
        begin
             FOperacion := GetOperacion();
             iEmpleado := GetNumEmpleado( Datos ,'PRESTAMO' );
             with XMLTools do
             begin
                   sTipoPrestamo :=  TagAsString ( GetNode( 'PR_TIPO', GetNode( 'PRESTAMO' ) ),VACIO);
                   sReferencia := TagAsString( GetNode( 'PR_REFEREN', GetNode( 'PRESTAMO' ) ),VACIO);
             end;

             cdsLista.Data := ServerRecursos.GetHisPrestamos( Empresa,iEmpleado );
             with cdsLista do
             begin
                  if Locate( 'PR_REFEREN;PR_TIPO;CB_CODIGO', VarArrayOf( [ sReferencia ,sTipoPrestamo,iEmpleado ] ), [] ) then
                  begin
                       with cdsPCarAbo do
                       begin
                            DataSetField := cdsLista.FieldByName('qryDetail') as TDataSetField;
                            if( FOperacion = K_OP_ALTA )then
                            begin
                                 Append;
                                 FieldByName('CB_CODIGO').AsInteger := iEmpleado;
                                 FieldByName('PR_TIPO').AsString  := sTipoPrestamo;
                                 FieldByName('PR_REFEREN').AsString  := sReferencia;
                                 FieldByName('CR_FECHA').AsDateTime:= FechaDefault;
                                 FieldByName('CR_CAPTURA').AsDateTime:= FechaDefault;
                                 TransfiereXMLDataSet('PCAR_ABO',cdsPCarAbo);
                            end
                            else
                            begin
                                 with XMLTools do
                                 begin
                                      sReferencia := TagAsString( GetNode( 'PR_REFEREN', GetNode( 'LLAVE' ) ),VACIO);
                                      sTipoPrestamo := TagAsString ( GetNode( 'PR_TIPO', GetNode( 'LLAVE' ) ),VACIO);
                                      iEmpleado  := TagAsInteger( GetNode( 'CB_CODIGO', GetNode( 'LLAVE' ) ),0);
                                      dFecha := TagAsDateTime( GetNode( 'CR_FECHA', GetNode( 'LLAVE' ) ),FechaDefault);
                                 end;
                                 if Locate( 'PR_REFEREN;PR_TIPO;CB_CODIGO;CR_FECHA', VarArrayOf( [ sReferencia ,sTipoPrestamo,iEmpleado,ZetaCommonTools.FechaAsStr( dFecha )] ), [] ) then
                                 begin
                                      if( FOperacion = K_OP_BAJA )then
                                      begin
                                           Delete;
                                      end
                                      else if ( FOperacion = K_OP_CAMBIO ) then
                                      begin
                                           Edit;
                                           TransfiereXMLDataSet('PCAR_ABO',cdsPCarAbo);
                                      end;
                                 end
                                 else
                                 begin
                                       Self.LogError( Format( 'No existe un cargo � abono registrado al empleado:%d con la referencia: %s con el tipo: %s en la fecha:%s ', [ iEmpleado,sReferencia , sTipoPrestamo, DateToStr(dFecha) ] ) );
                                 end;
                            end;
                            DespuesDeModificar(cdsPCarAbo);
                            PostData;
                            cdsLista.PostData;
                            if ( ChangeCount > 0 ) then
                            begin
                                 OnReconcileError := ReconcileError;
                                 if Reconciliar( ServerRecursos.GrabaHistorial( Empresa, TAG_PRESTAMOS, cdsLista.Delta, ErrorCount )  ) then
                                 begin
                                      LogInfo( Format( 'Se realiz� el cambio en los cargos y abonos de pr�stamos del empleado: %d',[iEmpleado] ));
                                 end
                                 else
                                 begin
                                      LogError( Status );
                                 end;
                            end
                            else
                            begin
                                 LogInfo( 'No hay cambios' );
                            end;
                       end;
                  end
                  else
                  begin
                        Self.LogError( Format( 'No existe un pr�stamo registrado al empleado:%d con la referencia: %s con el tipo: %s  ', [ iEmpleado,sReferencia , sTipoPrestamo ] ) );
                  end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;

end;
{------------------------------------------------------------------------------
   Se ajusta el status del pr�stamo dependiendo del saldo actualizado
-------------------------------------------------------------------------------}
procedure TdmRecursos.SetStatusPrestamo( DataSet: TDataSet );
var
   OldStatus, NewStatus : eStatusPrestamo;
begin
     with DataSet do
     begin
          OldStatus:= eStatusPrestamo( FieldByName( 'PR_STATUS' ).AsInteger );
          NewStatus:= OldStatus;
          if ( OldStatus <> spCancelado ) then
          begin
               if ( FieldByName( 'PR_MONTO' ).AsFloat >= 0 ) then
               begin
                    if ( FieldByName( 'PR_SALDO' ).AsFloat <= 0 ) then
                       NewStatus:= spSaldado
                    else if ( OldStatus <> spCancelado ) then
                       NewStatus:= spActivo;
               end
               else
               begin
                    if ( FieldByName( 'PR_SALDO' ).AsFloat >= 0 ) then
                       NewStatus:= spSaldado
                    else if ( OldStatus <> spCancelado ) then
                       NewStatus:= spActivo;
               end;
               if ( OldStatus <> NewStatus ) then
               begin
                    FieldByName( 'PR_STATUS' ).AsInteger := Ord( NewStatus );
                    Self.LogInfo(Format('El status del pr�stamo se ajust� a : %s ',[ ObtieneElemento( lfStatusPrestamo, Ord( NewStatus ))]) );
               end;
          end;
     end;
end;

{------------------------------------------------------------------------------
   M�todo para modificar los saldos en el pr�stamo
-------------------------------------------------------------------------------}
procedure TdmRecursos.DespuesDeModificar(cdsPCarAbo:TZetaClientDataSet);
var dCargo, dAbono : Real;
begin
     GetSaldos( cdsPCarAbo, 'CR_CARGO', 'CR_ABONO', dCargo, dAbono );
     with cdsLista do
     begin
          Edit;
          FieldByName('US_CODIGO').AsInteger := Usuario;
          FieldByName('PR_ABONOS').AsFloat := dAbono;
          FieldByName('PR_CARGOS').AsFloat := dCargo;
          FieldByName('PR_SALDO').AsFloat := FieldByName('PR_MONTO').AsFloat -
                                             FieldByName('PR_SALDO_I').AsFloat -
                                             FieldByName('PR_TOTAL').AsFloat -
                                             dAbono + dCargo;
     end;
     SetStatusPrestamo(cdsLista);
end;

{------------------------------------------------------------------------------
 M�todo para obtener la suma de cargos y abonos
-------------------------------------------------------------------------------}
procedure TdmRecursos.GetSaldos( DataSet : TDataSet; const sCargo, sAbono : string;
                                 var dCargo, dAbono : Real );
begin
     dCargo := 0;
     dAbono := 0;

     with DataSet do
     begin
          DisableControls;
          First;
          while NOT EOF do
          begin
               dCargo := dCargo + FieldByName(sCargo).AsFloat;
               dAbono := dAbono + FieldByName(sAbono).AsFloat;
               Next;
          end;
          EnableControls;
     end;
end;

{------------------------------------------------------------------------------
 M�todo para obtener el saldo de vacaciones para un empleado
-------------------------------------------------------------------------------}
function TdmRecursos.ProcesaSaldosVacaciones(const Datos: String): string;
var
   DatosEmpleado:Olevariant;
   dFecha:TDate;
   cdsHisVacacion : TZetaClientDataSet;
   cdsEmpVacacion : TZetaClientDataSet;
   Empleado : Integer;
begin

     Result := VACIO;
     cdsHisVacacion := TZetaClientDataSet.Create(nil);
     cdsEmpVacacion := TZetaClientDataSet.Create(nil);
     LogInit;
     try
        if ConectaEmpresa( Datos ) then
        begin
             with XMLTools do
             begin
                  dFecha := TagAsDate( GetNode( 'FECHA', GetNode( 'VALORES' ) ), NullDateTime );
                  Empleado := GetNumEmpleado(Datos,'VALORES'); 
             end;
             cdsHisVacacion.Data := ServerRecursos.GetHisVacacion(Empresa,Empleado,dFecha,DatosEmpleado);
             cdsEmpVacacion.Data := DatosEmpleado;
             DataSetToXML('SALDO_VACA',cdsEmpVacacion );
        end;
     finally
            LogEnd;
     end;
     Result := XMLTools.XMLDocument.XML.Text;
end;

{------------------------------------------------------------------------------
 M�todo para obtener la fecha de regreso de vacaciones en base a una fecha inicial y cantidad de d�as
-------------------------------------------------------------------------------}
function TdmRecursos.GetVacaFechaRegreso( const Datos: String ): WideString;
var
   dFechaInicio: TDate;
   iEmpleado: Integer;
   rDiasGozo, rDiasRango: Double;
begin
     Result := VACIO;
     try
        if ConectaEmpresa( Datos ) then
        begin
             with XMLTools do
             begin
                  dFechaInicio := TagAsDate( GetNode( 'FECHA_INICIO', GetNode( 'VALORES' ) ), NullDateTime );
                  iEmpleado := GetNumEmpleado( Datos, 'VALORES' );
                  rDiasGozo := TagAsFloat( GetNode( 'DIAS_GOZO', GetNode( 'VALORES' ) ), 0 );
             end;
             // Regresa la fecha en formato de MM/DD/YYYY
             Result := Format( '%s', [ DateToStrSQL( ServerAsistencia.GetVacaFechaRegreso( Empresa, iEmpleado, dFechaInicio, rDiasGozo, rDiasRango ) ) ] );
        end
        else
            Result := 'No se pudo acceder a la empresa especificada';
     except
           on Error : Exception do
              Result := Format( 'Error al consultar RecursosWS: %s', [ Error.Message ] );
     end;
end;

{------------------------------------------------------------------------------
 M�todo para obtener los d�as de vacaciones en base a una fecha inicial y final
-------------------------------------------------------------------------------}
function TdmRecursos.GetVacaDiasGozados( const Datos: String ): WideString;
var
   dFechaInicio, dFechaFin: TDate;
   iEmpleado: Integer;
begin
     Result := VACIO;
     try
        if ConectaEmpresa( Datos ) then
        begin
             with XMLTools do
             begin
                  dFechaInicio := TagAsDate( GetNode( 'FECHA_INICIO', GetNode( 'VALORES' ) ), NullDateTime );
                  dFechaFin := TagAsDate( GetNode( 'FECHA_FIN', GetNode( 'VALORES' ) ), NullDateTime );
                  iEmpleado := GetNumEmpleado( Datos, 'VALORES' );
             end;
             Result := Format( '%f', [ ServerAsistencia.GetVacaDiasGozar( Empresa, iEmpleado, dFechaInicio, dFechaFin ) ] );
        end
        else
             Result := 'No se pudo acceder a la empresa especificada';
     except
           on Error : Exception do
              Result := Format( 'Error al consultar RecursosWS: %s', [ Error.Message ] );
     end;
end;

function TdmRecursos.GetPermisoDiasHabiles(
  const Datos: String): WideString;
var
   dFechaInicio, dFechaFin: TDate;
   iEmpleado: Integer;
begin
     Result := VACIO;
     try
        if ConectaEmpresa( Datos ) then
        begin
             with XMLTools do
             begin
                  dFechaInicio := TagAsDate( GetNode( 'FECHA_INICIO', GetNode( 'VALORES' ) ), NullDateTime );
                  dFechaFin := TagAsDate( GetNode( 'FECHA_FIN', GetNode( 'VALORES' ) ), NullDateTime );
                  iEmpleado := GetNumEmpleado( Datos, 'VALORES' );
             end;
             Result := Format( '%f', [ ServerAsistencia.GetPermisoDiasHabiles(Empresa, iEmpleado, dFechaInicio, dFechaFin) ] );
        end
        else
             Result := 'No se pudo acceder a la empresa especificada';
     except
           on Error : Exception do
              Result := Format( 'Error al consultar RecursosWS: %s', [ Error.Message ] );
     end;
end;

function TdmRecursos.GetPermisoFechaRegreso(
  const Datos: String): WideString;
var
   dFechaInicio: TDate;
   iEmpleado: Integer;
   rDias, rDiasRango: Double;
begin
     Result := VACIO;
     try
        if ConectaEmpresa( Datos ) then
        begin
             with XMLTools do
             begin
                  dFechaInicio := TagAsDate( GetNode( 'FECHA_INICIO', GetNode( 'VALORES' ) ), NullDateTime );
                  iEmpleado    := GetNumEmpleado( Datos, 'VALORES' );
                  rDias        := TagAsFloat( GetNode( 'DIAS', GetNode( 'VALORES' ) ), 0 );
             end;
             // Regresa la fecha en formato de MM/DD/YYYY
             Result := Format( '%s', [ DateToStrSQL( ServerAsistencia.GetPermisoFechaRegreso( Empresa, iEmpleado, dFechaInicio, rDias, rDiasRango ) ) ] );
        end
        else
            Result := 'No se pudo acceder a la empresa especificada';
     except
           on Error : Exception do
              Result := Format( 'Error al consultar RecursosWS: %s', [ Error.Message ] );
     end;
end;

end.
