unit DBasicoCliente;

interface

{$INCLUDE DEFINES.INC}

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

{$define VALIDAEMPLEADOSGLOBAL}

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, DBClient, ComOBJ, FileCtrl,
     {$ifdef HTTP_CONNECTION}SConnect,{$endif}
     {$ifndef VER130}Variants,{$endif}
{$ifdef DOS_CAPAS}
     DServerLogin,
{$else}
     Login_TLB,
{$endif}
     FAutoClasses,
     ZetaLicenseClasses,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TInfoUsuario = record
    Codigo: Integer;
    NombreCorto: String;
    Nombre: String;
    Grupo: Integer;
    Nivel: eNivelUsuario;
    TieneArbol: Boolean;
    LoginAD: Boolean;
  end;
  TBasicoCliente = class(TDataModule)
    cdsUsuario: TZetaClientDataSet;
    cdsCompany: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    {$ifdef HTTP_CONNECTION}
    FWebConnection: TWebConnection;
    {$endif}
    FEmpresaAbierta: Boolean;
    FEmpresa: Variant;
    FUsuario: Integer;
    FPasswordUsuario: String;
    FCaptionLogoff : String;
    FFormsMoved : Boolean;
    FModoSuper: Boolean;
    FModoLabor: Boolean;
    FModoServicioMedico: Boolean;
    FModoPlanCarrera: Boolean;
    FModoSeleccion: Boolean;
    FModoVisitantes: Boolean;
    FModoEvaluacion: Boolean;
    FModoCajaAhorro: Boolean;
    FTipoCompany: eTipoCompany;
    FDataCache: TStringList;
    FServerSupportsCOMPlus: Boolean;
    FDatosSeguridad: TDatosSeguridad;
    FTipoLogin : eTipoLogin;
    FApplicationID: Integer;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    FDatosSeguridadEmpresa : TClientGlobalLicenseValues;
{$endif}

    function GetCompania: String;
    function GetEsModoDemo: Boolean;
    function GetFechaDefault: TDate;
    procedure SetFechaDefault(const Value: TDate);
    procedure SetDatosSeguridad( const Valores: OleVariant );
    function GetModoTress: Boolean;
    function GetApplicationID: Integer;
    procedure SetApplicationID( const Value: Integer );
  protected
    FFechaDefault: TDate;
    {$ifdef DOS_CAPAS}
    FServidor: TdmServerLogin;
    {$else}
    FServidor: IdmServerLoginDisp;
    {$endif}
    property ServerSupportsCOMPlus: Boolean read FServerSupportsCOMPlus;
    {$ifndef DOS_CAPAS}
    function GetServidor: IdmServerLoginDisp;
    function CreateServer(const ClassID: TGUID): IDispatch;
    {$endif}
    function GetConfidencialidad: String;
    function GetConfidencialidadDefault : String;
    function GetConfidencialidadListaIN : string;

    procedure InitCacheModules; virtual;
    procedure SetUsuario( const iValue: Integer );
  public
    { Public declarations }
    property Compania: String read GetCompania;
    property Empresa: Variant read FEmpresa;
    property EmpresaAbierta: Boolean read FEmpresaAbierta write FEmpresaAbierta;
    property EsModoDemo: Boolean read GetEsModoDemo;
    property FechaDefault: TDate read GetFechaDefault write SetFechaDefault;
{$ifdef DOS_CAPAS}
    property Servidor: TdmServerLogin read FServidor;
{$else}
    property Servidor: IdmServerLoginDisp read GetServidor;
{$endif}
    property ModoSuper: Boolean read FModoSuper write FModoSuper default FALSE;
    property ModoLabor: Boolean read FModoLabor write FModoLabor default FALSE;
    property ModoServicioMedico: Boolean read FModoServicioMedico write FModoServicioMedico default FALSE;
    property ModoPlanCarrera: Boolean read FModoPlanCarrera write FModoPlanCarrera default FALSE;
    property ModoSeleccion: Boolean read FModoSeleccion write FModoSeleccion default FALSE;
    property ModoVisitantes: Boolean read FModoVisitantes write FModoVisitantes default FALSE;
    property ModoEvaluacion: Boolean read FModoEvaluacion write FModoEvaluacion default FALSE;
    property ModoCajaAhorro: Boolean read FModoCajaAhorro write FModoCajaAhorro default FALSE;
    property ModoTress: Boolean read GetModoTress;
    property Confidencialidad : String read GetConfidencialidad;
    property ConfidencialidadDefault : String read GetConfidencialidadDefault;
    property ConfidencialidadListaIN : String read GetConfidencialidadListaIN;
    property Usuario: Integer read FUsuario write SetUsuario;
    property PasswordUsuario: String read FPasswordUsuario;
    property CaptionLogoff : String read FCaptionLogoff write FCaptionLogoff;
    property FormsMoved : Boolean read FFormsMoved write FFormsMoved;
    property TipoCompany : eTipoCompany read FTipoCompany write FTipoCompany;
    property DataCache : TStringList read FDataCache write FDataCache;
    property DatosSeguridad: TDatosSeguridad read FDatosSeguridad;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    property DatosSeguridadEmpresa : TClientGlobalLicenseValues read FDatosSeguridadEmpresa;
{$endif}
    property TipoLogin : eTipoLogin read FTipoLogin write FTipoLogin;
    property ApplicationID : Integer read GetApplicationID write SetApplicationID;

{$ifndef DOS_CAPAS}
    function CreaServidor( const ClassID: TGUID; var Servidor; const lStateLess: Boolean = TRUE ): IDispatch;
    procedure CreateFixedServer(const ClassID: TGUID; var Servidor);
    procedure DestroyFixedServer(var Servidor);
{$endif}
    function FindCompany( const Value: String ): Boolean;
    function BuildEmpresa(DataSet: TDataSet): OleVariant;
    function GetAccesos: OleVariant;
    function GetDatosEmpresaActiva: TDatosCompania;
    function GetDatosUsuarioActivo: TInfoUsuario;
    function GetGrupoActivo : Integer; virtual;
    function GetSeguridad: Boolean;
    function EsMultipleConfidencialidad : Boolean;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    function GetSeguridadEmpresa: Boolean;
{$endif}
    function GetSegundosTimeout: Integer; virtual;
    function GetUsuario( const sNombre, sClave: String; const lAllowReentry: Boolean ): eLogReply;overload;
    function GetUsuario( const sNombre, sClave, sComputer: String; const lAllowReentry: Boolean ): eLogReply;overload;
    function InitCompany: Integer;
    function SetCompany : OLEVariant;
    function ModuloAutorizado( const eModulo: TModulos ): Boolean;
    function ModuloAutorizadoConsulta( const eModulo: TModulos; var sMensaje: String ): Boolean;
    function ModuloAutorizadoReportes( const Entidad: TipoEntidad ): Boolean;virtual;
    function ModuloAutorizadoDerechos (const eModulo: TModulos ) : Boolean;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; virtual;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; virtual;
    function GetCacheCount: Integer;
    function ConstruyeListaEmpleados( oDataset : TDataset; const sFiltro : string ) : string;
    function ValidaCaracteresPassword( const sClave: String; var sMensaje: String ): Boolean;

{$ifdef VALIDAEMPLEADOSGLOBAL}
    function EvaluarEmpleadosCliente(operacion : eEvaluaOperacion;  var sMensajeAdvertencia, sMensajeError : string ) : eEvaluaTotalEmpleados;
    function ValidaLicenciaEmpleados(operacion : eEvaluaOperacion) : boolean ;
    function EsTRESSPruebas : boolean;
{$endif}


{$ifdef WS_PROCESOS}
    function SetCompanySinCOM( sEmpresa, sDBServer, sDBUser, sDBPassword  : string): OLEVariant;
{$endif}
    procedure CargaActivosTodos(Parametros: TZetaParams); virtual;
    procedure SetCacheLookup;
    procedure ActualizaClaveUsuario( const sAnterior, sClave: String ); virtual;
    procedure CierraEmpresa; virtual;
    procedure SetSeguridad( const Valores: OleVariant ); virtual;
    procedure SetPasswordLongitud( const iLongitud: Integer );
    procedure UsuarioBloquea( const sNombre: String; const iIntentos: Word );
    procedure UsuarioLogout;
    procedure InitDataCache;
    function LoginActiveDirectory(const lAllowReentry: Boolean;var Tipo:Integer):eLogReply;
    function LoggedOnUserNameEx: string;

    function EsDeConfidencialidad( sNivel0 : string ) : Boolean;
    function Sentinel_EsProfesional_MSSQL:Boolean;

  end;

var
  BasicoCliente: TBasicoCliente;

implementation

uses {DDiccionario,}
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaConnectionProblem_DevEx,
     ZetaDialogo,
     ZetaRegistryCliente;

{$R *.DFM}

{ ***************** TdmCliente ******************* }

procedure TBasicoCliente.DataModuleCreate(Sender: TObject);
begin
     FAutoClasses.InitAuto;
     FCaptionLogoff := VACIO;
     FormsMoved := FALSE;
     FFechaDefault := Date;
     FTipoCompany := tc3Datos;
     FServerSupportsCOMPlus := False;
     SetApplicationID( 0 );
{$ifdef DOS_CAPAS}
     FServidor := TdmServerLogin.Create( Self );
{$endif}
     {$ifdef HTTP_CONNECTION}
     FWebConnection := TWebConnection.Create( Self );
     with FWebConnection do
     begin
          LoginPrompt := False;
          Agent := 'GTI';
     end;
     {$endif}
end;

procedure TBasicoCliente.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef HTTP_CONNECTION}
     FreeAndNil( FWebConnection );
     {$endif}
     FAutoClasses.ClearAuto;
     FreeAndNil( FDataCache );
{$ifdef DOS_CAPAS}
     FreeAndNil( FServidor );
{$endif}
end;

{$ifndef DOS_CAPAS}
function TBasicoCliente.CreateServer( const ClassID: TGUID ): IDispatch;
var
   lLoop: Boolean;
begin
     lLoop := False;
     repeat
           Result := nil;
           try
              {$ifdef HTTP_CONNECTION}
              case ClientRegistry.TipoConexion of
                   conxHTTP:
                   begin
                        with FWebConnection do
                        begin
                             Connected := False;
                             URL := ClientRegistry.URL; //'http://www.tress.com.mx/TressRemoto/httpsrvr.dll';
                             Username := ClientRegistry.UserName;
                             Password := ClientRegistry.Password;
                             ServerGUID := GUIDToString( ClassID );
                             Connected := True;
                             Result := AppServer;
                        end;
                   end
                   else
                   begin
                        Result := CreateRemoteComObject( ClientRegistry.ComputerName, ClassID ) as IDispatch;
                   end;
              end;
              {$else}
              Result := CreateRemoteComObject( ClientRegistry.ComputerName, ClassID ) as IDispatch;
              {$endif}
           except
                 on Error: Exception do
                 begin
                      {$ifndef XMLREPORT}
                      case ZetaConnectionProblem.ConnectionProblemDialog( Error ) of
                           mrRetry: lLoop := True;
                      else
                          lLoop := False;
                      end;
                      {$else}
                         lLoop := False;
                      {$endif}
                 end;
           end;
     until not lLoop;
end;

function TBasicoCliente.CreaServidor( const ClassID: TGUID; var Servidor; const lStateLess: Boolean = TRUE ): IDispatch;
begin
     if lStateless then
        Result := CreateServer( ClassID )
     else
     begin
          if ( IDispatch( Servidor ) = nil ) then
             IDispatch( Servidor ) := CreateServer( ClassID );
          Result := IDispatch( Servidor );
     end;
end;

procedure TBasicoCliente.CreateFixedServer( const ClassID: TGUID; var Servidor );
begin
     if not ServerSupportsCOMPlus then
        CreaServidor( ClassID, Servidor, FALSE );
end;

procedure TBasicoCliente.DestroyFixedServer( var Servidor );
begin
     if not ServerSupportsCOMPlus then
        IDispatch( Servidor ) := Nil;
end;

function TBasicoCliente.GetServidor: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( CreaServidor( CLASS_dmServerLogin, FServidor ) );
end;
{$endif}

procedure TBasicoCliente.CierraEmpresa;
begin
     {$ifdef DOS_CAPAS}
     Servidor.CierraEmpresa;
     {$endif}
end;

procedure TBasicoCliente.InitDataCache;
begin
     if not Assigned( FDataCache ) then
     begin
          FDataCache := TStringList.Create;
          InitCacheModules;
     end;
end;

procedure TBasicoCliente.InitCacheModules;
begin
     // Programar en cada Cliente
end;

function TBasicoCliente.GetCacheCount: Integer;
var
   i : Integer;
begin
     Result := 0;
     if Assigned( FDataCache ) then
        for i := 0 to FDataCache.Count - 1 do
            Result := Result + TDataModule( FDataCache.Objects[i] ).ComponentCount;
end;

{ ********** Asignaci�n de Valores Activos ************* }

function TBasicoCliente.GetFechaDefault: TDate;
begin
     Result := Trunc( FFechaDefault );
end;

procedure TBasicoCliente.SetFechaDefault(const Value: TDate);
begin
     if ( FFechaDefault <> Value ) then
     begin
          FFechaDefault := Value;
     end;
end;

function TBasicoCliente.GetCompania : String;
begin
    Result := cdsCompany.FieldByName( 'CM_CODIGO' ).AsString;
end;

function TBasicoCliente.GetAccesos: OleVariant;
begin
     Result := Servidor.GetAccesos( GetGrupoActivo, Compania );
end;

function TBasicoCliente.GetApplicationID: Integer;
begin
     Result := FApplicationID;
end;

function TBasicoCliente.Sentinel_EsProfesional_MSSQL: Boolean;
begin
     with Autorizacion do
     begin
          Cargar(Servidor.GetAuto);
          Result := ( (not EsDemo) and (Plataforma = ptProfesional) and (SQLEngine = engMSSQL));
     end;
end;

procedure TBasicoCliente.SetApplicationID( const Value: Integer );
begin
     FApplicationID := Value;
end;

{ ************** Usuarios ************* }

function TBasicoCliente.GetUsuario( const sNombre, sClave: String; const lAllowReentry: Boolean ): eLogReply;
begin
     Result := GetUsuario( sNombre, sClave, ZetaWinAPITools.GetComputerName, lAllowReentry );
end;

function TBasicoCliente.GetUsuario( const sNombre, sClave, sComputer: String; const lAllowReentry: Boolean ): eLogReply;
var
   Datos, Sentinel: OleVariant;
   iIntentos : Integer;
   {$ifdef DOS_CAPAS}
   LoginServer: TdmServerLogin;
   lLoop : Boolean;
   {$else}
   LoginServer: IdmServerLoginDisp;
   {$endif}
begin
     try
        LoginServer := Servidor;
        if Assigned( LoginServer ) then
        begin
             {$ifdef DOS_CAPAS}
             repeat
                   lLoop := FALSE;
             {$endif}
                   Result := lrNotFound;
                   try
                      Result := eLogReply( LoginServer.UsuarioLogin( sNombre, Encrypt (sClave), sComputer, lAllowReentry, Datos, Sentinel, iIntentos ) )
                   except
                         on Error: Exception do
                         begin
                              {$ifdef DOS_CAPAS}
                              case ZetaConnectionProblem.ConnectionProblemDialog( Error ) of
                                   mrRetry: lLoop := True;
                              end;
                              {$else}
                              Application.HandleException( Error );
                              {$endif}
                         end;
                   end;
             {$ifdef DOS_CAPAS}
             until not lLoop;
             {$endif}
        end
        else
            Result := lrNotFound;
     finally
            {$ifndef DOS_CAPAS}
            LoginServer := nil;
            {$endif}
     end;
     FDatosSeguridad.PasswordIntentos := iIntentos;
     if ( Result in [ lrOK, lrChangePassword, lrExpiredPassword ] ) then
     begin
          with cdsUsuario do
          begin
               Data := Datos;
               FUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
               with FDatosSeguridad do
               begin
                    TimeOut := FieldByName( 'TIMEOUT' ).AsInteger;
                    PasswordLongitud := FieldByName( 'LIMITEPASS' ).AsInteger;
                    PasswordLetras := FieldByName( 'LETRASPASS' ).AsInteger;
                    PasswordDigitos := FieldByName( 'DIGITOPASS' ).AsInteger;
               end;
          end;
          FPasswordUsuario := sClave;
          Autorizacion.Cargar( Sentinel );

     end
     else
         FUsuario := 0;
end;

procedure TBasicoCliente.SetUsuario( const iValue: Integer );
begin
     FUsuario := iValue;
end;

function TBasicoCliente.GetDatosUsuarioActivo: TInfoUsuario;
begin
     with Result do
     begin
          with cdsUsuario do
          begin
               Codigo := FieldByName( 'US_CODIGO' ).AsInteger;
               NombreCorto := FieldByName( 'US_CORTO' ).AsString;
               Nombre := FieldByName( 'US_NOMBRE' ).AsString;
               Grupo := FieldByName( 'GR_CODIGO' ).AsInteger;
               Nivel := eNivelUsuario( FieldByName( 'US_NIVEL' ).AsInteger );
               TieneArbol := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
               LoginAD := zStrToBool( FieldByName( 'LOGIN_AD' ).AsString )
          end;
     end;
end;

function TBasicoCliente.GetGrupoActivo : Integer;
begin
     {$ifdef KIOSCO2}
     //Dentro de Kiosco no hay usuario firmado.
     //Se supone que los reportes que se muestran ahi, se puede ver cualquier dato.
     Result := D_GRUPO_SIN_RESTRICCION;
     {$else}
        {$ifdef KIOSCO1}
         Result := D_GRUPO_SIN_RESTRICCION;
        {$else}
        {$ifdef TRESSEMAIL}
        Result := D_GRUPO_SIN_RESTRICCION;
        {$else}
        {$ifdef TRESSIMPORTER}
        Result := D_GRUPO_SIN_RESTRICCION;
        {$else}
        if cdsUsuario.Active then
           Result := cdsUsuario.FieldByName( 'GR_CODIGO' ).AsInteger
        else
            Result := D_GRUPO_SIN_RESTRICCION;
        {$endif}
        {$endif}
        {$endif}
     {$endif}
end;

procedure TBasicoCliente.ActualizaClaveUsuario( const sAnterior, sClave: String );
var
   sMensaje: String;
begin
     with FDatosSeguridad do
     begin
          if ( PasswordLongitud > 0 ) and ( Length( sClave ) < PasswordLongitud ) then
             DataBaseError( Format( 'Clave De Acceso Debe Tener Por Lo Menos %d Caracteres', [ PasswordLongitud ] ) )
          else if not ValidaCaracteresPassword( sClave, sMensaje ) then
             DataBaseError( sMensaje );
     end;
     Servidor.UsuarioCambiaPswd( FUsuario, sAnterior, sClave );
     FPasswordUsuario := sClave;
end;

procedure TBasicoCliente.UsuarioBloquea( const sNombre: String; const iIntentos: Word );
begin
     Servidor.UsuarioBloquea( sNombre, ZetaWinAPITools.GetComputerName, iIntentos );
end;

procedure TBasicoCliente.UsuarioLogout;
begin
     if ( FUsuario > 0 ) then
     begin
          Servidor.UsuarioLogout( FUsuario );
          cdsCompany.Close;
          cdsUsuario.Close;
          FUsuario := 0;
     end;
end;

//Metodo para obtener el nombre de usuario logeado al Network
procedure GetUserNameEx(NameFormat: DWORD;lpNameBuffer: LPSTR; nSize: PULONG); stdcall;external 'secur32.dll' Name 'GetUserNameExA';

function TBasicoCliente.LoggedOnUserNameEx: string;
CONST
     NameUnknown = 0;             // Unknown name type.
     NameFullyQualifiedDN = 1;    // Fully qualified distinguished name
     NameSamCompatible = 2;       // Windows NT� 4.0 account name
     NameDisplay = 3;             // A "friendly" display name
     NameUniqueId = 6;            // GUID string that the IIDFromString function returns
     NameCanonical = 7;           // Complete canonical name
     NameUserPrincipal = 8;       // User principal name
     NameCanonicalEx = 9;
     NameServicePrincipal = 10;   // Generalized service principal name
     DNSDomainName = 11;          // DNS domain name, plus the user name
var
     UserName: array[0..250] of char;
     Size: DWORD;
begin
     Size := 250;
     GetUserNameEx(NameSamCompatible, @UserName, @Size);
     Result := UserName;
end;

function TBasicoCliente.LoginActiveDirectory(const lAllowReentry: Boolean;var Tipo:Integer):eLogReply;
var
   Datos, Sentinel: OleVariant;
   {$ifdef DOS_CAPAS}
   LoginServer: TdmServerLogin;
   lLoop : Boolean;
   {$else}
   LoginServer: IdmServerLoginDisp;
   {$endif}
begin
     try
        LoginServer := Servidor;
        if Assigned( LoginServer ) then
        begin
             {$ifdef DOS_CAPAS}
             repeat
                   lLoop := FALSE;
             {$endif}
                   Result := lrNotFound;
                   try
                      Result := eLogReply( LoginServer.LoginAD ( ZetaWinAPITools.GetComputerName, lAllowReentry,Tipo, Datos, Sentinel,LoggedOnUserNameEx ) );
                      FTipoLogin := eTipoLogin(Tipo);
                   except
                         on Error: Exception do
                         begin
                              {$ifdef DOS_CAPAS}
                              case ZetaConnectionProblem.ConnectionProblemDialog( Error ) of
                                   mrRetry: lLoop := True;
                              end;
                              {$else}
                              Application.HandleException( Error );
                              {$endif}
                         end;
                   end;
             {$ifdef DOS_CAPAS}
             until not lLoop;
             {$endif}
        end
        else
            Result := lrNotFound;
     finally
            {$ifndef DOS_CAPAS}
            LoginServer := nil;
            {$endif}
     end;
     if ( Result in [ lrOK  ] ) then
     begin
          with cdsUsuario do
          begin
               Data := Datos;
               FUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
               with FDatosSeguridad do
               begin
                    TimeOut := FieldByName( 'TIMEOUT' ).AsInteger;
                    PasswordLongitud := FieldByName( 'LIMITEPASS' ).AsInteger;
                    PasswordLetras := FieldByName( 'LETRASPASS' ).AsInteger;
                    PasswordDigitos := FieldByName( 'DIGITOPASS' ).AsInteger;
               end;
          end;
          Autorizacion.Cargar( Sentinel );
     end
     else
         FUsuario := 0;
end;



{ *************** Compa��as *************** }

function TBasicoCliente.InitCompany: Integer;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( FUsuario, Ord( FTipoCompany ) );
               ResetDataChange;
          end;
          Result := RecordCount;
     end;
end;

function TBasicoCliente.GetDatosEmpresaActiva: TDatosCompania;
begin
     with Result do
     begin
          with cdsCompany do
          begin
               Codigo := FieldByName( 'CM_CODIGO' ).AsString;
               Nombre := FieldByName( 'CM_NOMBRE' ).AsString;
          end;
          // Ahora se hace desde el Shell FCompania := Codigo;
     end;
end;

function TBasicoCliente.FindCompany( const Value: String ): Boolean;
begin
     Result := cdsCompany.Locate( 'CM_CODIGO', Value, [] );
end;

function TBasicoCliente.BuildEmpresa(DataSet : TDataSet):OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;
P_GRUPO = 6;
P_APPID = 7;
}
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  Self.Usuario,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString,
                                  GetGrupoActivo,
                                  GetApplicationID ] );
     end;
end;



function TBasicoCliente.SetCompany: OLEVariant;
begin
     Result := BuildEmpresa(cdsCompany);
     FEmpresa := Result;
{$ifdef VALIDAEMPLEADOSGLOBAL}
     GetSeguridadEmpresa;
{$endif}
end;

{$ifdef WS_PROCESOS}
function TBasicoCliente.SetCompanySinCOM( sEmpresa, sDBServer, sDBUser, sDBPassword  : string): OLEVariant;
begin
      Result := VarArrayOf(       [sDBServer,
                                  sDBUser,
                                  sDBPassword,
                                  Self.Usuario,
                                  '',
                                  sEmpresa,
                                  D_GRUPO_SIN_RESTRICCION,
                                  0 ] );

     FEmpresa := Result;

end;
{$endif}

{ ********** Cache de Client Datasets *********** }

procedure TBasicoCliente.SetCacheLookup;
var
   sError: String;
   lOk: Boolean;

function VerificaDirectorio( const sFolder: String ): Boolean;
begin
     Result := DirectoryExists( sFolder );
     if not Result then
        sError := 'No Se Encontr� El Directorio: ' + sFolder;
end;

begin
     with ClientRegistry do
     begin
          Compania := GetCompania;
          if CacheActive then
          begin
               lOk := ( not CacheSistemaActive ) or ( VerificaDirectorio( CacheSistemaFolder ) );
               if not lOk then
               begin
                    CacheSistemaActive:= FALSE;
                    ZetaDialogo.zError( 'Tablas Temporales', sError + CR_LF + 'Se Desactivaran Las Tablas Temporales de Sistema', 0 );
               end;
               lOk := VerificaDirectorio( CacheFolder );
               if not lOk then
               begin
                    CacheActive:= FALSE;
                    ZetaDialogo.zError( 'Tablas Temporales', sError + CR_LF + 'Se Desactivaran Las Tablas Temporales', 0 );
               end;
          end;
          ZetaClientDataSet.CacheActive := CacheActive;
          ZetaClientDataSet.CacheFolder := CacheFolder;
          ZetaClientDataSet.CacheSistemaActive := CacheSistemaActive;
          ZetaClientDataSet.CacheSistemaFolder := CacheSistemaFolder;
     end;
end;

{ Autorizacion de Sentinela }

function TBasicoCliente.GetEsModoDemo: Boolean;
begin
     Result := Autorizacion.EsDemo;
     {$ifdef CAROLINA}
     Result := FALSE;
     {$endif}
     if Result then
        ZetaDialogo.zInformation( 'Versi�n Demostraci�n', 'Operaci�n No Permitida en Versi�n Demo', 0 );
end;

function TBasicoCliente.ModuloAutorizadoConsulta( const eModulo: TModulos; var sMensaje: String ): Boolean;
begin
     with Autorizacion do
     begin
          Result := OkModulo( eModulo );
          if not Result then
             sMensaje := 'El M�dulo De ' + GetModuloStr( eModulo ) + ' No Est� Autorizado' + CR_LF + 'Consulte A Su Distribuidor';
     end;
end;

function TBasicoCliente.ModuloAutorizadoDerechos( const eModulo: TModulos ): Boolean;
begin
     with Autorizacion do
     begin
          Result := OkModulo( eModulo );
     end;
end;

function TBasicoCliente.ModuloAutorizado( const eModulo: TModulos ): Boolean;
var
   sMensaje: String;
begin
     Result := ModuloAutorizadoConsulta( eModulo, sMensaje );
     if not Result then
        ZetaDialogo.ZInformation( 'M�dulo No Autorizado', sMensaje, 0 );
end;

function TBasicoCliente.ModuloAutorizadoReportes( const Entidad: TipoEntidad ): Boolean;
begin
     Result := TRUE;
     {CV: El codigo que se encontraba aqui, lo puse en dmCliente,
     ya que son validaciones exclusivas del modulo de
     Reporteador de Tress}
end;

procedure TBasicoCliente.SetDatosSeguridad( const Valores: OleVariant );
begin
     with FDatosSeguridad do
     begin
          PasswordExpiracion := Valores[ SEG_VENCIMIENTO ];
          PasswordLongitud := Valores[ SEG_LIMITE_PASSWORD ];
          PasswordLetras := Valores[ SEG_MIN_LETRAS_PASSWORD ];
          PasswordDigitos := Valores[ SEG_MIN_DIGITOS_PASSWORD ];
          PasswordLog := Valores[ SEG_MAX_LOG_PASSWORD ];
          PasswordIntentos := Valores[ SEG_INTENTOS ];
          Inactividad := Valores[ SEG_DIAS_INACTIVOS ];
          { El timeout se enmasacara en GetSegundosTimeout de acuerdo a lo asignado en LoginAD }
          TimeOut := Valores[ SEG_TIEMPO_INACTIVO ];
          UsuarioTareasAutomaticas := Valores[ SEG_USUARIO_TRESSAUTOMATIZA ]; // (JB) Se agrega Configuracion de Tress Automatiza
     end;
end;

function TBasicoCliente.GetSeguridad: Boolean;
begin
     try
        SetDatosSeguridad( Servidor.GetSeguridad );
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TBasicoCliente.SetSeguridad( const Valores: OleVariant );
begin
     try
        Servidor.SetSeguridad( Empresa, Valores );
        SetDatosSeguridad( Valores );
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TBasicoCliente.SetPasswordLongitud( const iLongitud: Integer );
begin
     FDatosSeguridad.PasswordLongitud := iLongitud;
end;

function TBasicoCliente.GetSegundosTimeout: Integer;
begin
     if( NOT GetDatosUsuarioActivo.LoginAD)then
     begin
         Result := FDatosSeguridad.TimeOut * 60      // El Resultado debe convertirse a Segundos
     end
     else
         Result := 0;
end;

function TBasicoCliente.ValidaCaracteresPassword( const sClave: String; var sMensaje: String ): Boolean;
const
     K_MESS_ERROR = 'Clave De Acceso Debe Incluir Por Lo Menos %d %s';
var
   i, iDigitos, iLetras : Integer;
   lCheckLetras, lCheckDigitos: Boolean;

   procedure RevisaChar( const Caracter: Char );
   begin
        if ZetaCommonTools.EsDigito( Caracter ) then
           Inc( iDigitos )
        else if ZetaCommonTools.EsLetra( Caracter ) then
           Inc( iLetras );
   end;

begin
     with FDatosSeguridad do
     begin
          lCheckLetras := ( PasswordLetras > 0 );
          lCheckDigitos := ( PasswordDigitos > 0 );
          Result := ( not lCheckLetras ) and ( not lCheckDigitos );
          if not Result then
          begin
               iDigitos := 0;
               iLetras  := 0;
               for i := 1 to Length( sClave ) do
               begin
                    RevisaChar( sClave[i] );
               end;
               if lCheckLetras and ( iLetras < PasswordLetras ) then
                  sMensaje := Format( K_MESS_ERROR, [ PasswordLetras, 'Letras' ] )
               else
                   if lCheckDigitos and ( iDigitos < PasswordDigitos ) then
                      sMensaje := Format( K_MESS_ERROR, [ PasswordDigitos, 'D�gitos' ] )
                   else
                       Result := TRUE;
          end;
     end;
end;

function TBasicoCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     Result := Vacio;
end;

function TBasicoCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     Result := '';
end;

procedure TBasicoCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
end;

function TBasicoCliente.GetConfidencialidad: String;
begin
    Result := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;
end;

function TBasicoCliente.GetConfidencialidadListaIN : string;
var
   sConfiden : string;
begin
     Result := VACIO;
     sConfiden := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;

     if strLleno( sConfiden ) then
        Result := ListaComas2InQueryList( sConfiden );

end;

function TBasicoCliente.GetConfidencialidadDefault: String;
var
   lista : TStringList;
   sConfiden : string;
begin
    sConfiden := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;

    Result :=VACIO;

    if strLleno( sConfiden )  then
    begin
         lista :=   TStringList.Create;
         lista.CommaText := sConfiden;
         if lista.Count = 1 then
                  Result := lista[0]
         else
             Result := VACIO;
         FreeAndNil ( lista );
    end;
end;

function TBasicoCliente.EsMultipleConfidencialidad : Boolean;
var
   lista : TStringList;
begin
    Result := FALSE;

    if strLleno( GetConfidencialidad )  then
    begin
         lista :=   TStringList.Create;
         lista.CommaText := GetConfidencialidad;
         Result := ( lista.Count > 1);
         FreeAndNil ( lista );
    end;

end;



function TBasicoCliente.EsDeConfidencialidad( sNivel0 : string ) : Boolean;
var
   lista : TStringList;
   sConfiden : string;
   idx : integer;
begin
    sConfiden := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;

    Result := TRUE ;

    if strLleno( sConfiden )  then
    begin
         lista :=   TStringList.Create;
         lista.CommaText := sConfiden;
         Result := lista.Find(sNivel0, idx);
         FreeAndNil ( lista );
    end;
end;

function TBasicoCliente.ConstruyeListaEmpleados( oDataset: TDataset; const sFiltro : string ): string;

 const K_LIMITE_OBJETOS_FB = 1000; {Corrige defecto 113}
 var
    iCodigo : integer;
    sFiltroTemporal: string;
begin
     Result := '';
     with oDataSet do
     begin
          DisableControls;
          try
             if IsEmpty then
                Raise Exception.Create('Supervisor no Tiene Lista de Empleados');
             iCodigo := FieldByName('CB_CODIGO').AsInteger;
             First;
             while NOT EOF do
             begin
                  if ( RecNo MOD K_LIMITE_OBJETOS_FB ) = 0 then
                  begin
                       sFiltroTemporal := ConcatString( sFiltroTemporal, sFiltro + ' IN ('+Result+')', ' OR ' );
                       Result := VACIO;
                  end;

                  Result := ConcatString( Result, FieldByName('CB_CODIGO').AsString, ',' );
                  Next;
             end;

             if StrLleno(Result) or StrLLeno(sFiltroTemporal) then
             begin
                  Result := sFiltro + ' IN ('+Result+')';
                  Result := ConcatString( sFiltroTemporal, Result, ' OR ')
             end;
             Locate('CB_CODIGO', VarArrayOf([iCodigo]), [] );
          finally
                 EnableControls;
          end;
     end;
end;

function TBasicoCliente.GetModoTress: Boolean;
begin
     Result:=  not (
                     //ModoLabor or
                     //ModoServicioMedico or
                     //ModoPlanCarrera or
                     ModoSuper or
                     ModoSeleccion or
                     ModoVisitantes
                     //ModoEvaluacion or
                     //ModoCajaAhorro
                     );
end;

{$ifdef VALIDAEMPLEADOSGLOBAL}
function TBasicoCliente.GetSeguridadEmpresa: Boolean;
begin
     try
        FDatosSeguridadEmpresa := TClientGlobalLicenseValues.Create(  Servidor.GetSeguridadEmpresa( Empresa ) );

        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TBasicoCliente.EvaluarEmpleadosCliente(  operacion : eEvaluaOperacion; var sMensajeAdvertencia, sMensajeError : string ) : eEvaluaTotalEmpleados;
begin
     sMensajeAdvertencia := VACIO;
     sMensajeError := VACIO;
     Result := evIgnorar;
     if ( DatosSeguridadEmpresa <> nil ) then
     begin
          if DatosSeguridadEmpresa.TotalGlobal <= 0 then
             GetSeguridadEmpresa;
     end
     else
          GetSeguridadEmpresa;

     if ( DatosSeguridadEmpresa <> nil ) then
     begin
           Result := DatosSeguridadEmpresa.EvaluarEmpleadosCliente(  operacion , sMensajeAdvertencia, sMensajeError );
     end;
end;

function  TBasicoCliente.ValidaLicenciaEmpleados( operacion : eEvaluaOperacion) : boolean ;
var
   sMsgAdv, sMsgError : string;
   eEvalua : eEvaluaTotalEmpleados;
begin
      eEvalua := EvaluarEmpleadosCliente( operacion, sMsgAdv, sMsgError );
      if StrLleno(sMsgAdv) then
         ZWarning(  'Advertencia de Uso de Licencia', sMsgAdv, 0, mbOK );
      if StrLleno(sMsgError) then
         ZError(  'Restricci�n de Uso de Licencia', sMsgError, 0 );

      Result := ( eEvalua <> evRebasaRestringe );
end;

function TBasicoCliente.EsTRESSPruebas : boolean;
begin
     Result := TRUE;

     if ( DatosSeguridadEmpresa <> nil ) then
     begin
           Result := ( DatosSeguridadEmpresa.TipoCompany = tc3Prueba )  and DatosSeguridadEmpresa.DebeValidar;
     end;
end;


{$endif}


end.
