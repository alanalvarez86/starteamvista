library RecursosWS;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIThreadPool,
  ISAPIApp,
  DRecursosWS in 'DRecursosWS.pas' {dmRecursosWS: TWebModule},
  RecursosWSImpl in 'RecursosWSImpl.pas',
  RecursosWSIntf in 'RecursosWSIntf.pas',
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  DRecursos in 'DRecursos.pas' {dmRecursos: TDataModule},
  IRecursosWSProxy in 'TestWebServices\IRecursosWSProxy.pas';

{$R *.res}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  Application.CreateForm(TdmRecursosWS, dmRecursosWS);
  //Application.CreateForm(TdmCliente, dmCliente);
  //Application.CreateForm(TBasicoCliente, BasicoCliente);
  //Application.CreateForm(TdmRecursos, dmRecursos);
  Application.Run;
end.
