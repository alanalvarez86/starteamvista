inherited Listados: TListados
  Left = 389
  Top = 261
  Caption = 'Listados'
  ClientHeight = 437
  ClientWidth = 571
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 571
    TabOrder = 3
    inherited CortarBtn: TSpeedButton
      Left = 183
    end
    inherited CopiarBtn: TSpeedButton
      Left = 207
    end
    inherited PegarBtn: TSpeedButton
      Left = 231
    end
    inherited UndoBtn: TSpeedButton
      Left = 255
    end
    inherited BGuardarComo: TSpeedButton
      Left = 88
    end
    inherited bbFavoritos: TSpeedButton
      Left = 117
    end
    inherited bbSuscripcion: TSpeedButton
      Left = 146
    end
    object BGrafica: TSpeedButton
      Left = 55
      Top = 2
      Width = 25
      Height = 25
      Hint = 'Gr'#225'fica (Ctrl + G)'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333300030003
        0003333377737773777333333333333333333FFFFFFFFFFFFFFF770000000000
        0000777777777777777733039993BBB3CCC3337F737F737F737F37039993BBB3
        CCC3377F737F737F737F33039993BBB3CCC33F7F737F737F737F77079997BBB7
        CCC77777737773777377330399930003CCC3337F737F7773737F370399933333
        CCC3377F737F3333737F330399933333CCC33F7F737FFFFF737F770700077777
        CCC77777777777777377330333333333CCC3337F33333333737F370333333333
        0003377F33333333777333033333333333333F7FFFFFFFFFFFFF770777777777
        7777777777777777777733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BGraficaClick
    end
  end
  inherited PageControl: TPageControl
    Width = 571
    Height = 354
    TabOrder = 0
    inherited tsGenerales: TTabSheet
      inherited GBParametrosEncabezado: TGroupBox
        Top = 12
      end
      object GBTotales: TGroupBox
        Left = 26
        Top = 205
        Width = 105
        Height = 73
        TabOrder = 1
        object RE_VERTICA: TDBCheckBox
          Left = 8
          Top = 40
          Width = 81
          Height = 17
          Caption = 'Vertical'
          DataField = 'RE_VERTICA'
          DataSource = DataSource
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object RE_SOLOT: TDBCheckBox
          Left = 8
          Top = 16
          Width = 89
          Height = 17
          Caption = 'S'#243'lo Totales'
          DataField = 'RE_SOLOT'
          DataSource = DataSource
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
          OnClick = RE_SOLOTClick
        end
      end
    end
    inherited tsCampos: TTabSheet
      object BArribaCampos: TZetaSmartListsButton [0]
        Left = 215
        Top = 8
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
          3333333333090333333333333309033333333333330903333333333333090333
          3333333333090333333333300009000033333330999999903333333309999903
          3333333309999903333333333099903333333333309990333333333333090333
          3333333333090333333333333330333333333333333033333333}
        Tipo = bsSubir
        SmartLists = SmartListCampos
      end
      object BAbajoCampos: TZetaSmartListsButton [1]
        Left = 215
        Top = 33
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
          3333333333303333333333333309033333333333330903333333333330999033
          3333333330999033333333330999990333333333099999033333333099999990
          3333333000090000333333333309033333333333330903333333333333090333
          3333333333090333333333333309033333333333330003333333}
        Tipo = bsBajar
        SmartLists = SmartListCampos
      end
      object SpeedButton1: TSpeedButton [2]
        Left = 215
        Top = 64
        Width = 25
        Height = 25
        Hint = 'Rengl'#243'n Nuevo'
        Glyph.Data = {
          66010000424D6601000000000000760000002800000014000000140000000100
          040000000000F000000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
          8888888800008888888888888888888800008888888888888888888800008888
          8888888888888888000088888888888888888888000088888884444888888888
          0000888888844788888888880000888888848078888888880000888888848807
          8888888800008888888888807888888800008888888888880788888800008888
          8888888880788888000088888888888888478888000088888444444444478888
          0000888884444444444888880000888888888888888888880000888888888888
          8888888800008888888888888888888800008888888888888888888800008888
          88888888888888880000}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton1Click
      end
      inherited GroupBox4: TGroupBox
        inherited LBCampos: TZetaSmartListBox
          OnDrawItem = LBCamposDrawItem
        end
      end
      inherited BAgregaCampo: TBitBtn
        OnClick = BFormulaClick
      end
      inherited GBPropiedadesCampo: TGroupBox
        inherited lAnchoCampo: TLabel
          Top = 217
        end
        object lCriterioCampo: TLabel [5]
          Left = 8
          Top = 191
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Criterio Totalizaci'#243'n:'
          Enabled = False
        end
        inherited bFormulaCampo: TBitBtn
          OnClick = bFormulaCampoClick
        end
        inherited CBTipoCampo: TZetaKeyCombo
          OnClick = CBTipoCampoClick
        end
        inherited EAnchoCampo: TEdit
          Top = 213
          TabOrder = 6
        end
        object CBCriterioCampo: TComboBox
          Left = 108
          Top = 187
          Width = 165
          Height = 21
          Style = csDropDownList
          DropDownCount = 9
          Enabled = False
          ItemHeight = 13
          TabOrder = 5
          OnChange = CampoOpcionesClick
          OnClick = CBCriterioCampoClick
          Items.Strings = (
            'Ninguno'
            'Suma'
            'Cuantos'
            'Promedio'
            'Min'
            'Max'
            'Autom'#225'tico')
        end
      end
    end
    inherited tsOrden: TTabSheet
      inherited BArribaOrden: TZetaSmartListsButton
        Left = 233
      end
      inherited BAbajoOrden: TZetaSmartListsButton
        Left = 233
      end
      inherited GroupBox2: TGroupBox
        Left = 29
      end
      inherited BAgregaOrden: TBitBtn
        Left = 29
      end
      inherited BBorraOrden: TBitBtn
        Left = 129
      end
      inherited GroupBoxOrden: TGroupBox
        Left = 263
      end
      inherited BAgregaFormulaOrden: TBitBtn
        Left = 54
      end
    end
    inherited tsGrupos: TTabSheet
      inherited BAbajoGrupo: TZetaSmartListsButton
        Left = 232
      end
      inherited BArribaGrupo: TZetaSmartListsButton
        Left = 232
      end
      inherited GroupBox6: TGroupBox
        Left = 28
      end
      inherited BAgregaGrupo: TBitBtn
        Left = 28
      end
      inherited BBorraGrupo: TBitBtn
        Left = 128
      end
      object GBEncabezado: TGroupBox [5]
        Left = 266
        Top = 84
        Width = 285
        Height = 113
        Caption = 'Datos del Encabezado'
        TabOrder = 5
        object CBSaltoPag: TCheckBox
          Left = 178
          Top = 32
          Width = 82
          Height = 17
          Caption = 'Salto P'#225'gina'
          Enabled = False
          TabOrder = 2
          OnClick = GrupoOpcionesClick
        end
        object CBEncabezado: TCheckBox
          Left = 178
          Top = 16
          Width = 80
          Height = 17
          Caption = 'Mostrar'
          TabOrder = 1
          OnClick = GrupoOpcionesClick
        end
        object BAgregaEncabezado: TBitBtn
          Left = 178
          Top = 81
          Width = 75
          Height = 25
          Caption = 'Modificar'
          TabOrder = 4
          OnClick = BAgregaEncabezadoClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
        object LBGrupoEncabezado: TListBox
          Left = 8
          Top = 16
          Width = 161
          Height = 89
          ItemHeight = 13
          TabOrder = 0
          OnDblClick = BAgregaEncabezadoClick
        end
        object CBMaster: TCheckBox
          Left = 178
          Top = 48
          Width = 71
          Height = 17
          Caption = 'Centrado'
          TabOrder = 3
          OnClick = GrupoOpcionesClick
        end
        object cbTitulos: TCheckBox
          Left = 178
          Top = 64
          Width = 71
          Height = 17
          Caption = 'T'#237'tulos'
          TabOrder = 5
          OnClick = GrupoOpcionesClick
        end
      end
      object GbPie: TGroupBox [6]
        Left = 266
        Top = 198
        Width = 285
        Height = 101
        Caption = 'Propiedades del Pie:'
        TabOrder = 6
        object CbTotalizar: TCheckBox
          Left = 178
          Top = 33
          Width = 61
          Height = 17
          Caption = 'Totalizar'
          Enabled = False
          TabOrder = 2
          OnClick = GrupoOpcionesClick
        end
        object CbPie: TCheckBox
          Left = 178
          Top = 16
          Width = 75
          Height = 17
          Caption = 'Mostrar'
          TabOrder = 1
          OnClick = GrupoOpcionesClick
        end
        object BAgregaPie: TBitBtn
          Left = 178
          Top = 67
          Width = 75
          Height = 25
          Caption = 'Modificar'
          TabOrder = 3
          OnClick = BAgregaPieClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
        object LBGrupoPie: TListBox
          Left = 8
          Top = 16
          Width = 161
          Height = 76
          ItemHeight = 13
          TabOrder = 0
          OnDblClick = BAgregaPieClick
        end
      end
      inherited GroupBoxGrupo: TGroupBox
        Left = 266
        Height = 80
        inherited eFormulaGrupo: TMemo
          Height = 33
        end
        inherited bFormulaGrupo: TBitBtn
          Top = 48
          Enabled = False
        end
      end
      inherited BAgregaFormulaGrupo: TBitBtn
        Left = 53
      end
    end
    inherited tsImpresora: TTabSheet
      inherited GroupBox3: TGroupBox
        Left = 17
        Top = 16
        Width = 529
        Height = 297
        object Label1: TLabel [0]
          Left = 45
          Top = 20
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Salida:'
        end
        object lbSeparador: TLabel [1]
          Left = 387
          Top = 20
          Width = 52
          Height = 13
          Caption = 'Separador:'
          Enabled = False
        end
        object lbImpresora: TLabel [2]
          Left = 67
          Top = 47
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Impresora:'
        end
        object lbCopias: TLabel [3]
          Left = 71
          Top = 74
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = '# Copias:'
        end
        object EPAGINAS: TLabel [4]
          Left = 187
          Top = 74
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Rango de P'#225'ginas:'
        end
        object EPAginasAl: TLabel [5]
          Left = 322
          Top = 74
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al:'
        end
        object BGuardaArchivo: TSpeedButton [6]
          Left = 458
          Top = 94
          Width = 25
          Height = 25
          Hint = 'Buscar Archivo'
          Enabled = False
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333FFF333333333333000333333333
            3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
            3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
            0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
            BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
            33337777773FF733333333333300033333333333337773333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BGuardaArchivoClick
        end
        object lbNombreArchivo: TLabel [7]
          Left = 20
          Top = 100
          Width = 96
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre del Archivo:'
          Enabled = False
        end
        object lbPlantilla: TLabel [8]
          Left = 22
          Top = 167
          Width = 94
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre de Plantilla:'
        end
        object bPlantilla: TSpeedButton [9]
          Left = 458
          Top = 161
          Width = 25
          Height = 25
          Hint = 'Buscar Plantilla'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333FFF333333333333000333333333
            3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
            3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
            0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
            BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
            33337777773FF733333333333300033333333333337773333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bPlantillaClick
        end
        object bExportPreferencias: TSpeedButton [10]
          Left = 333
          Top = 14
          Width = 25
          Height = 25
          Hint = 'Configuraci'#243'n'
          Glyph.Data = {
            06020000424D0602000000000000760000002800000028000000140000000100
            0400000000009001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333333333333333333333333333333FFF3333333333
            FFF33300033333333330003333888FFFFFFFFFF888FF30000000000000000003
            3888888888888888888F30F7777777777777770338F3333333333333338F30F7
            777777777777770338F3333333333333338F30F7777777777799770338F33333
            33333333338F30FFFFFFFFFFFFFFFF0338FFFFFFFFFFFFFFFF8F380088888888
            8888008338888888888888888883333000000000000003333338888888888888
            8F333330888888888888033333388888888888888F3333300000000000000333
            333888888888888883333333333333333333333333333333333333FFFFF33333
            3333333338000833333333333333388888F333333333333330EFE0333333FFFF
            FFFFF83338F33338000000000EF008333338888888888F3888333330E4EFEFEF
            8FE033333338F8FFFFFF8F383FF33338000000000EF0083333388888888883F8
            88F333333333333330EFE03333333333333338FFF8F333333333333338000833
            33333333333338888833}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bExportPreferenciasClick
        end
        inherited bDisenador: TSpeedButton
          Left = 488
          Top = 161
          OnClick = bDisenadorClick
        end
        object TLabel [12]
          Left = 120
          Top = 120
          Width = 9
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsItalic]
          ParentFont = False
        end
        object bEvaluaArchivo: TSpeedButton [13]
          Left = 488
          Top = 94
          Width = 25
          Height = 25
          Hint = 'Evaluar Nombre de Archivo'
          Enabled = False
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777700000007777777777777777700000007777777774F77777700000007777
            7777444F77777000000077777774444F777770000000700000444F44F7777000
            000070FFF444F0744F777000000070F8884FF0774F777000000070FFFFFFF077
            74F77000000070F88888F077774F7000000070FFFFFFF0777774F000000070F8
            8777F07777774000000070FFFF00007777777000000070F88707077777777000
            000070FFFF007777777770000000700000077777777770000000777777777777
            777770000000}
          ParentShowHint = False
          ShowHint = True
          OnClick = bEvaluaArchivoClick
        end
        inherited cbBitacora: TCheckBox
          Left = 113
          Top = 271
          TabOrder = 11
        end
        object RE_PFILE: TZetaDBKeyCombo
          Left = 121
          Top = 16
          Width = 208
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          DropDownCount = 12
          ItemHeight = 13
          TabOrder = 0
          OnChange = RE_PFILEChange
          ListaFija = lfTipoFormato
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'RE_PFILE'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object RE_CFECHA: TDBEdit
          Left = 444
          Top = 16
          Width = 40
          Height = 21
          DataField = 'RE_CFECHA'
          DataSource = DataSource
          MaxLength = 1
          TabOrder = 1
        end
        object RE_PRINTER: TComboBox
          Left = 121
          Top = 43
          Width = 363
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 2
          OnChange = RE_PRINTERChange
        end
        object RE_COPIAS: TZetaDBNumero
          Left = 121
          Top = 70
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 3
          Text = '0'
          DataField = 'RE_COPIAS'
          DataSource = DataSource
        end
        object UpDown: TUpDown
          Left = 154
          Top = 70
          Width = 16
          Height = 21
          Associate = RE_COPIAS
          Min = 1
          Position = 1
          TabOrder = 4
        end
        object EPaginaInicial: TEdit
          Left = 283
          Top = 70
          Width = 33
          Height = 21
          TabOrder = 5
          Text = '1'
        end
        object EPaginaFinal: TEdit
          Left = 338
          Top = 70
          Width = 39
          Height = 21
          TabOrder = 6
          Text = '9999'
        end
        object RE_ARCHIVO: TDBEdit
          Left = 121
          Top = 96
          Width = 334
          Height = 21
          AutoSelect = False
          DataField = 'RE_ARCHIVO'
          DataSource = DataSource
          MaxLength = 255
          TabOrder = 7
          OnChange = RE_ARCHIVOChange
        end
        object RE_REPORTE: TDBEdit
          Left = 121
          Top = 163
          Width = 334
          Height = 21
          AutoSelect = False
          DataField = 'RE_REPORTE'
          DataSource = DataSource
          MaxLength = 255
          TabOrder = 9
        end
        object GroupBox5: TGroupBox
          Left = 64
          Top = 192
          Width = 449
          Height = 73
          Caption = 'En donde se copia la informaci'#243'n:'
          TabOrder = 10
          object lbPagina: TLabel
            Left = 8
            Top = 25
            Width = 73
            Height = 13
            Caption = 'P'#225'gina (Sheet):'
            Enabled = False
          end
          object lbColumna: TLabel
            Left = 254
            Top = 25
            Width = 44
            Height = 13
            Caption = 'Columna:'
            Enabled = False
          end
          object lbRenglon: TLabel
            Left = 352
            Top = 25
            Width = 43
            Height = 13
            Caption = 'Rengl'#243'n:'
            Enabled = False
          end
          object RE_FONTNAM: TDBEdit
            Left = 88
            Top = 21
            Width = 153
            Height = 21
            DataField = 'RE_FONTNAM'
            DataSource = DataSource
            Enabled = False
            TabOrder = 0
          end
          object RE_GENERAL: TDBCheckBox
            Left = 8
            Top = 48
            Width = 153
            Height = 17
            Caption = 'Refrescar Hoja de Trabajo'
            DataField = 'RE_GENERAL'
            DataSource = DataSource
            TabOrder = 3
            ValueChecked = 'S'
            ValueUnchecked = 'N'
          end
          object RE_ALTO: TZetaDBNumero
            Left = 304
            Top = 21
            Width = 33
            Height = 21
            Mascara = mnDias
            TabOrder = 1
            Text = '0'
            DataField = 'RE_ALTO'
            DataSource = DataSource
          end
          object RE_ANCHO: TZetaDBNumero
            Left = 400
            Top = 21
            Width = 33
            Height = 21
            Mascara = mnDias
            TabOrder = 2
            Text = '0'
            DataField = 'RE_ANCHO'
            DataSource = DataSource
          end
        end
        object NombreArchivoFormula: TMemo
          Left = 121
          Top = 120
          Width = 334
          Height = 33
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 8
        end
      end
    end
    inherited tsParametro: TTabSheet
      inherited BArribaParametro: TZetaSmartListsButton
        SmartLists = SmartListParametros
      end
      inherited BAbajoParametro: TZetaSmartListsButton
        SmartLists = SmartListParametros
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 384
    Width = 571
    TabOrder = 1
  end
  inherited StatusBarRep: TStatusBar
    Top = 418
    Width = 571
  end
  inherited DataSource: TDataSource
    Left = 376
  end
  inherited SmartListFiltros: TZetaSmartLists
    Left = 440
  end
  inherited SmartListOrden: TZetaSmartLists
    Left = 408
  end
  inherited SmartListParametros: TZetaSmartLists
    Left = 472
  end
  inherited dsResultados: TDataSource
    Left = 352
  end
  inherited SmartListCampos: TZetaSmartLists
    Left = 536
  end
  inherited SmartListGrupos: TZetaSmartLists
    Left = 504
  end
end
