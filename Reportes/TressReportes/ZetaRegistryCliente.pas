unit ZetaRegistryCliente;

interface

uses Windows, Messages, SysUtils, Classes,
     {$ifndef DOTNET}Graphics, Controls, Forms, Dialogs,{$endif}
     Registry,ZetaCommonClasses;

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

type
  {$ifdef HTTP_CONNECTION}
  TTipoConexion = ( conxDCOM, conxHTTP );
  {$endif}
  TZetaClientRegistry = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    FCompania: String;
    FCanWrite: Boolean;
    {$ifdef TRESS_DELPHIXE5_UP}
    FRegistryUser: TRegIniFile;
    FCanWriteUser: Boolean;
    {$endif}
    function GetRegistry: TRegIniFile;
    function GetCanWrite: Boolean;
    function GetComputerName: String;
    function GetConsolaUsuario: String;
    function GetConsolaContrasena: String;
    function GetInvocaApp: String;
    procedure SetInvocaApp( const Value: String );
    procedure SetComputerName(const Value: String);
    function GetCacheSistemaActive: Boolean;
    procedure SetCacheSistemaActive(const Value: Boolean);
    function GetCacheSistemaFolder: String;
    procedure SetCacheSistemaFolder(const Value: String);
    function GetCacheActive: Boolean;
    procedure SetCacheActive(const Value: Boolean);
    function GetCacheFolder: String;
    procedure SetCacheFolder(const Value: String);
    function GetCacheFecha: String;
    procedure SetCacheFecha(const Value: String);
    procedure SetCompania(const Value: String);
    function GetRegistryKey: String;
    function GetRegistryRoot: HKEY;
    {$ifdef HTTP_CONNECTION}
    function GetPassword: String;
    function GetTipoConexion: TTipoConexion;
    function GetURL: String;
    function GetUserName: String;
    procedure SetPassword(const Value: String);
    procedure SetTipoConexion(const Value: TTipoConexion);
    procedure SetURL(const Value: String);
    procedure SetUserName(const Value: String);
    {$endif}
    function GetRegPatronal: String;
    function GetTipoNomina: Integer;
    procedure SetRegPatronal(const Value: String);
    procedure SetTipoNomina(const Value: integer);
    function GetVSPresentacion:String;                     //DevEx (@am): Lee y Escribe si se desea ver la presentacion de bienvenida
    procedure SetVSPresentacion (Value: String);
  protected
    { Protected declarations }
    property Registry: TRegIniFile read FRegistry;
    {$ifdef TRESS_DELPHIXE5_UP}
    property RegistryUser: TRegIniFile read FRegistryUser;
    {$endif}
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property CanWrite: Boolean read FCanWrite;
    property RegistryKey: String read GetRegistryKey;
    property RegistryRoot: HKEY read GetRegistryRoot;
    property ComputerName: String read GetComputerName write SetComputerName;
    property Compania: String read FCompania write SetCompania;
    property CacheSistemaActive: Boolean read GetCacheSistemaActive write SetCacheSistemaActive;
    property CacheSistemaFolder: String read GetCacheSistemaFolder write SetCacheSistemaFolder;
    property CacheActive: Boolean read GetCacheActive write SetCacheActive;
    property CacheFolder: String read GetCacheFolder write SetCacheFolder;
    property CacheFecha: String read GetCacheFecha write SetCacheFecha;
    {$ifdef HTTP_CONNECTION}
    property TipoConexion: TTipoConexion read GetTipoConexion write SetTipoConexion;
    property URL: String read GetURL write SetURL;
    property UserName: String read GetUserName write SetUserName;
    property Password: String read GetPassword write SetPassword;
    {$endif}
    property TipoNomina:  Integer read GetTipoNomina write SetTipoNomina;
    property RegPatronal: String read GetRegPatronal write SetRegPatronal;
    {$ifdef TRESS_DELPHIXE5_UP}
    {$endif}
    function InitComputerName: Boolean;
    function GetLastEmpresa( const iUsuario: Integer ): String;
    procedure SetLastEmpresa( const iUsuario: Integer; const sEmpresa: String );
    property ConsolaUsuario: string read GetConsolaUsuario;
    property ConsolaContrasena: string read GetConsolaContrasena;
    property InvocaApp: string read GetInvocaApp write SetInvocaApp;
    property VSPresentacion: String read GetVSPresentacion write SetVSPresentacion;  //DevEx (@am): Propiedad para leer y escribir si el usuario desea ver la presentacion de bienvenida al entrar a la aplicacion.
  end;


var
   ClientRegistry: TZetaClientRegistry;

procedure InitClientRegistry;
procedure ClearClientRegistry;

implementation

uses ZetaCommonTools;

const
     {$ifdef HTTP_CONNECTION}
     K_KEY = 't0MacEwsky';
     {$endif}
     CLIENT_REGISTRY_PATH = 'Software\Grupo Tress\TressWin\Client';
     SERVER_COMPUTER_NAME = 'Server Computer Name';
     CLIENT_REGISTRY_SISTEMA_PATH = 'Sistema';
     FILES_CACHE_ACTIVE   = 'Files Cache Active';
     FILES_CACHE_FOLDER   = 'Files Cache Folder';
     FILES_CACHE_FECHA    = 'Files Cache Fecha';
     CLIENT_LAST_COMPANY  = 'Last Company';
     CLIENT_TIPO_NOMINA   = 'Tipo de Nomina';
     CLIENT_REG_PATRONAL  = 'Registro Patronal';
     CONSOLA_USR = 'ConsolaUsuario';
     CONSOLA_PWD = 'ConsolaClave';
     INVOCA_APP = 'InvocaApp';
     VS_PRESENTACION ='VsPresentacion';    //DevEx (@am): Key Agregado para guardar la seleccion del usuario para continuar viendo la presentacion de bienvenida
     {$ifdef HTTP_CONNECTION}
     TIPO_CONEXION = 'TipoConexion';
     SERVER_URL = 'ServerURL';
     SERVER_USERNAME = 'ServerUserName';
     SERVER_PASSWORD = 'ServerPassword';
     {$endif}

procedure InitClientRegistry;
begin
     ClientRegistry := TZetaClientRegistry.Create;
end;

procedure ClearClientRegistry;
begin
     FreeAndNil( ClientRegistry );
end;

{$ifdef HTTP_CONNECTION}
function Decrypt( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iTmpSrcAsc: Integer;
begin
     Result := '';
     if StrLleno( sSource ) then
     begin
          {$ifndef DOTNET}
          try
          {$endif}
             sKey := K_KEY;
             iKeyLen := Length( sKey );
             iKeyPos := 0;
             iOffset := StrToInt( '$' + Copy( sSource, 1, 2 ) );
             iSrcPos := 3;
             repeat
                   iSrcAsc := StrToInt( '$' + Copy( sSource, iSrcPos, 2 ) );
                   if ( iKeyPos < iKeyLen ) then
                      iKeyPos := iKeyPos + 1
                   else
                       iKeyPos := 1;
                   iTmpSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
                   if ( iTmpSrcAsc <= iOffset ) then
                      iTmpSrcAsc := 255 + iTmpSrcAsc - iOffset
                   else
                       iTmpSrcAsc := iTmpSrcAsc - iOffset;
                   Result := Result + Chr( iTmpSrcAsc );
                   iOffset := iSrcAsc;
                   iSrcPos := iSrcPos + 2;
             until ( iSrcPos >= Length( sSource ) );
          {$ifndef DOTNET}
          except
          end;
          {$endif}
     end;
end;

function Encrypt( const sSource: String ): String;
var
   sKey: String;
   iKeyLen, iKeyPos, iOffset, iSrcPos, iSrcAsc, iRange: Integer;
begin
     if StrLleno( sSource ) then
     begin
          sKey := K_KEY;
          iKeyLen := Length( sKey );
          iKeyPos := 0;
          iRange := 256;
          Randomize;
          iOffset := Random( iRange );
          Result := Format( '%1.2x', [ iOffset ] );
          for iSrcPos := 1 to Length( sSource ) do
          begin
               iSrcAsc := ( Ord( sSource[ iSrcPos ] ) + iOffset ) mod 255;
               if ( iKeyPos < iKeyLen ) then
                  iKeyPos := iKeyPos + 1
               else
                   iKeyPos := 1;
               iSrcAsc := iSrcAsc xor Ord( sKey[ iKeyPos ] );
               Result := Result + Format( '%1.2x',[ iSrcAsc ] );
               iOffset := iSrcAsc;
          end;
     end
     else
         Result := '';
end;
{$endif}

{ ************ TZetaRegistry *********** }

constructor TZetaClientRegistry.Create;
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          LazyWrite := False;
          FCanWrite := OpenKey( CLIENT_REGISTRY_PATH, TRUE );
          if not FCanWrite then
             OpenKeyReadOnly( CLIENT_REGISTRY_PATH );
     end;
     {$ifdef TRESS_DELPHIXE5_UP}
     // Registry del usuario
     FRegistryUser := TRegIniFile.Create;
     with FRegistryUser do
     begin
          //RootKey := HKEY_LOCAL_MACHINE;    // Al migrar las aplicaciones a Delphi XE5 se cambia a usar el registry del usuario: HKEY_CURRENT_USER que es el default
          LazyWrite := False;
          FCanWriteUser := OpenKey( CLIENT_REGISTRY_PATH, TRUE );
          if not FCanWriteUser then
             OpenKeyReadOnly( CLIENT_REGISTRY_PATH );
     end;
     {$endif}
end;

destructor TZetaClientRegistry.Destroy;
begin
     FRegistry.Free;
     {$ifdef TRESS_DELPHIXE5_UP}
     FRegistryUser.Free;
     {$endif}
     inherited Destroy;
end;

function TZetaClientRegistry.GetRegistry: TRegIniFile;
begin
     {$ifdef TRESS_DELPHIXE5_UP}
     Result := FRegistryUser;
     {$else}
     Result := FRegistry;
     {$endif}
end;

function TZetaClientRegistry.GetCanWrite: Boolean;
begin
     {$ifdef TRESS_DELPHIXE5_UP}
     Result := FCanWriteUser;
     {$else}
     Result := FCanWrite;
     {$endif}
end;

function TZetaClientRegistry.GetRegistryKey: String;
begin
     Result := FRegistry.CurrentPath;
end;

function TZetaClientRegistry.GetRegistryRoot: HKEY;
begin
     Result := FRegistry.RootKey;
end;

function TZetaClientRegistry.GetComputerName: String;
begin
     with FRegistry do
     begin
          Result := ReadString( '', SERVER_COMPUTER_NAME, '' );
     end;
end;

//DevEx: Leer Si el cliente desea ver la presentacion de bienvenida del Registry
function TZetaClientRegistry.GetVSPresentacion:String;
begin
     with GetRegistry do
     begin
          //Si el usuario no puede escribir en el registry, la presentacion no se mostrara
          if GetCanWrite then
             //El metodo ReadString ya cubre el que la llave no exista, en estos casos devolvera el valor por defecto que es el que mandamos en el ultimo parametro.
             Result := ReadString( '', VS_PRESENTACION, '0' )
          else
              Result := VERSION_COMPILACION; //Devolvera el mismo valor de la compilacion por lo tanto la presentacion no sera mostrada.
     end;
end;

//DevEx: Escribe en el Registry Si el cliente desea continuar viendo la presentacion de bienvenida la prox. vez que entre a la aplicacion.
procedure TZetaClientRegistry.SetVSPresentacion( Value: String);
begin
     with GetRegistry do
     begin
           //Se valida si el usuario puede o no escribir en el registry
           if GetCanWrite then
              WriteString( '', VS_PRESENTACION, Value );
     end;
end;

function TZetaClientRegistry.GetConsolaUsuario: String;
begin
     with FRegistry do
          Result := ReadString( '', CONSOLA_USR, '' );
end;

function TZetaClientRegistry.GetConsolaContrasena: String;
begin
     with FRegistry do
          Result := ReadString( '', CONSOLA_PWD, '' );
end;

function TZetaClientRegistry.GetInvocaApp: String;
begin
     with FRegistry do
          Result := ReadString( '', INVOCA_APP, '' );
end;

procedure TZetaClientRegistry.SetInvocaApp( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( '', INVOCA_APP, Value );
     end;
end;

function TZetaClientRegistry.InitComputerName: Boolean;
begin
     with FRegistry do
     begin
          if ValueExists( SERVER_COMPUTER_NAME ) then
             Result := True
          else
              Result := FALSE;
     end;
end;

procedure TZetaClientRegistry.SetComputerName( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( '', SERVER_COMPUTER_NAME, Value );
     end;
end;

procedure TZetaClientRegistry.SetCompania( const Value: String );
begin
     FCompania := Value;
end;

function TZetaClientRegistry.GetCacheSistemaActive: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_ACTIVE, FALSE );
     end;
end;

procedure TZetaClientRegistry.SetCacheSistemaActive(const Value: Boolean);
begin
     with FRegistry do
     begin
          WriteBool( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_ACTIVE, Value );
     end;
end;

function TZetaClientRegistry.GetCacheSistemaFolder: String;
begin
     with FRegistry do
     begin
          Result := ReadString( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_FOLDER, '' );
     end;
end;

procedure TZetaClientRegistry.SetCacheSistemaFolder(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( CLIENT_REGISTRY_SISTEMA_PATH, FILES_CACHE_FOLDER, Value );
     end;
end;

function TZetaClientRegistry.GetCacheActive: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( FCompania, FILES_CACHE_ACTIVE, FALSE );
     end;
end;

procedure TZetaClientRegistry.SetCacheActive( const Value: Boolean );
begin
     with FRegistry do
     begin
          WriteBool( FCompania, FILES_CACHE_ACTIVE, Value );
     end;
end;

function TZetaClientRegistry.GetCacheFolder: String;
begin
     with FRegistry do
     begin
          Result := ReadString( FCompania, FILES_CACHE_FOLDER, '' );
     end;
end;

procedure TZetaClientRegistry.SetCacheFolder( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( FCompania, FILES_CACHE_FOLDER, Value );
     end;
end;

function TZetaClientRegistry.GetCacheFecha: String;
begin
     with FRegistry do
     begin
          Result := ReadString( FCompania, FILES_CACHE_FECHA, '' );
          if StrVacio( Result ) then
             Result := 'No se Ha Refrescado';
     end;
end;

procedure TZetaClientRegistry.SetCacheFecha( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( FCompania, FILES_CACHE_FECHA, Value );
     end;
end;

function TZetaClientRegistry.GetLastEmpresa( const iUsuario: Integer ): String;
begin
     with GetRegistry do
     begin
          Result := ReadString( CLIENT_LAST_COMPANY, IntToStr( iUsuario ), ZetaCommonClasses.VACIO );
     end;
end;

procedure TZetaClientRegistry.SetLastEmpresa( const iUsuario: Integer; const sEmpresa: String );
begin
     if GetCanWrite then
     begin
 	  with GetRegistry do
          begin
               WriteString( CLIENT_LAST_COMPANY, IntToStr( iUsuario ), sEmpresa );
          end;
     end;
end;

//extraer el registry de tipo de nomina
function TZetaClientRegistry.GetTipoNomina: Integer;
begin
     with GetRegistry do
     begin
          // 1 es semanal, se pone constante para no tener que ligar ZetaCommonLists
          Result := ReadInteger( FCompania, CLIENT_TIPO_NOMINA, 1 );
     end;
end;

procedure TZetaClientRegistry.SetTipoNomina(const Value: integer);
begin
     if GetCanWrite then
     begin
          with GetRegistry do
          begin
               WriteInteger( FCompania, CLIENT_TIPO_NOMINA, Value );
          end;
     end;
end;

// Registro patronal en el registry
function TZetaClientRegistry.GetRegPatronal: String;
begin
     with GetRegistry do
     begin
          Result := ReadString( FCompania, CLIENT_REG_PATRONAL, '' );
     end;
end;

procedure TZetaClientRegistry.SetRegPatronal( const Value: String );
begin
     if GetCanWrite then
     begin
          with GetRegistry do
          begin
               WriteString( FCompania, CLIENT_REG_PATRONAL, Value );
          end;
     end;
end;

{$ifdef HTTP_CONNECTION}
function TZetaClientRegistry.GetPassword: String;
begin
     with FRegistry do
     begin
          Result := Decrypt( ReadString( VACIO, SERVER_PASSWORD, VACIO ) );
     end;
end;

function TZetaClientRegistry.GetTipoConexion: TTipoConexion;
begin
     with FRegistry do
     begin
          Result := TTipoConexion( ReadInteger( VACIO, TIPO_CONEXION, Ord( conxDCOM ) ) );
     end;
end;

function TZetaClientRegistry.GetURL: String;
const
     TRESS_HOST_NAME = 'TressHTTPSrvr.dll';
var
   iPos: integer;
begin
     with FRegistry do
     begin
          {Alternativa # 2 }
          Result := ReadString( VACIO, SERVER_URL, VACIO );
          iPos := Pos( '.dll', Result );
          if ( iPos > 0 ) then
          begin
               Dec( iPos );
               while ( Result[iPos] <> '/' ) and ( iPos > 0 ) do
                     Dec( iPos );

               Result := Copy( Result, 0, iPos );
          end;
          Result := ZetaCommonTools.VerificaDirURL( Result ) + TRESS_HOST_NAME
     end;
end;

function TZetaClientRegistry.GetUserName: String;
begin
     with FRegistry do
     begin
          Result := ReadString( VACIO, SERVER_USERNAME, VACIO );
     end;
end;

procedure TZetaClientRegistry.SetPassword(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( VACIO, SERVER_PASSWORD, Encrypt( Value ) );
     end;
end;

procedure TZetaClientRegistry.SetTipoConexion(const Value: TTipoConexion);
begin
     with FRegistry do
     begin
          WriteInteger( VACIO, TIPO_CONEXION, Ord( Value ) );
     end;
end;

procedure TZetaClientRegistry.SetURL(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( VACIO, SERVER_URL, Value );
     end;
end;

procedure TZetaClientRegistry.SetUserName(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( VACIO, SERVER_USERNAME, Value );
     end;
end;

{$endif}

end.

