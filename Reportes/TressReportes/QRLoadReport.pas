unit QRLoadReport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, thsdRuntimeLoader, thsdRuntimeEditor,
  QRDesign, Vcl.ExtCtrls, QuickRpt, qrdBaseCtrls, qrdQuickrep, StrUtils;

type
  TQRReportLoader = class(TForm)
    DesignQuickReport1: TDesignQuickReport;
    QRepDesigner: TQRepDesigner;
  private
    { Private declarations }
  public
    { Public declarations }
    function Imprimir( sPlantilla: AnsiString ):Integer;
    function Previa( sPlantilla: AnsiString ):Integer;
  end;

const
     K_ERROR_CARGANDO_LEGACY = 2;
     K_PLANTILLA_NO_EXISTE = 4;
     K_ERROR_IMPRIMIENDO = 5;
     K_IMPRESA = 10;

var
  QRReportLoader: TQRReportLoader;

  procedure PreparaReporte;
  procedure DesPreparaReporte;

implementation

{$R *.dfm}

procedure PreparaReporte;
begin
     QRReportLoader := TQRReportLoader.Create( nil );
end;

procedure DesPreparaReporte;
begin
     FreeAndNil( QRReportLoader );
end;

function TQRReportLoader.Imprimir( sPlantilla:AnsiString ):Integer;
var
   iResultado: Integer;
begin
     if FileExists( sPlantilla ) then
     begin
          try
             if not QRepDesigner.LoadReport(sPlantilla) then
             begin
                  iResultado:= K_ERROR_CARGANDO_LEGACY;
             end
             else
             begin
                  try
                     QRepDesigner.QReport.Print;
                     iResultado:= K_IMPRESA;
                  except
                        On Error:Exception do
                           iResultado:= K_ERROR_IMPRIMIENDO;
                  end;
             end;
          finally
          end;
     end
     else
         iResultado:= K_PLANTILLA_NO_EXISTE;
     Result := iResultado;
end;

function TQRReportLoader.Previa( sPlantilla:AnsiString ):Integer;
var
   iResultado: Integer;
begin
     if FileExists( sPlantilla ) then
     begin
          try
             if not QRepDesigner.LoadReport(sPlantilla) then
             begin
                  iResultado:= K_ERROR_CARGANDO_LEGACY;
             end
             else
             begin
                  try
                     QRepDesigner.QReport.Preview;
                     iResultado:= K_IMPRESA;
                  except
                        On Error:Exception do
                           iResultado:= K_ERROR_IMPRIMIENDO;
                  end;
             end;
          finally
          end;
     end
     else
         iResultado:= K_PLANTILLA_NO_EXISTE;
     Result := iResultado;
end;

end.
