unit DCatalogos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,
  Catalogos_TLB;

type
  TdmCatalogos = class(TDataModule)
    cdsCondiciones: TZetaClientDataSet;
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FServidor: IdmServerCatalogosDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
  public
    { Public declarations }
  end;

var
  dmCatalogos: TdmCatalogos;

implementation

{$R *.DFM}

uses DCliente;

function TdmCatalogos.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;

procedure TdmCatalogos.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

end.
