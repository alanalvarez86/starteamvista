unit DReportes;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet, FileCtrl,
  Variants,
  DReportesGenerador,
  ZetaCommonLists,
  ZReportTools,
  Sistema_TLB,
  DZetaServerProvider, ZetaCommonClasses
  {$ifdef PROFILE},ZetaProfiler{$endif};

type
  TdmReportes = class(TdmReportGenerator)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FArchivoHTML: String;
    FEmpresa: String;
    FBorraArchivo: Boolean;
    FFrecuencia: integer;
    FUsuarios: String;
    FReportes: String;
    FFromAddress: String;
    FFromName: String;
    FErrorMail: String;
    FSoloArchivos : String;
    FURLImagenes: String;
    FDIRImagenes: string;
    FUsuariosPlain: TStrings;
    FBitacora: TStrings;
    FListaReportes: TStrings;
    FClasifActivo: eClasifiReporte;
    FClasificaciones: TStrings;
    cdsListaEmpleados : TZetaClientDataSet;
    { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
    FDetenerServicio: Boolean;
    {$ifdef DOS_CAPAS}
    FServerSistema: TdmServerSistema;
    {$else}
    {$endif}

    {$ifdef PROFILE}
    emailProfiler : TZetaProfiler;
    {$endif}

    function ConectaGlobal: Boolean;
    function GetSuscripciones: Boolean;
    procedure BorraArchivos;
    procedure ModificaListaCampos(Lista: TStrings);
    procedure ModificaListas;
    function GetSobreTotales: Boolean;
    function SistemaBloqueado: Boolean;
    procedure actualizaCalendario (iCA_FOLIO: Integer);

  protected
    function GeneraReporte: Boolean;
    function GeneraReportePorSupervisor: Boolean;
    function GeneraUnReporte(const iReporte: Integer): Boolean;
    function GetExtensionDef : string;override;
    function GetSoloTotales: Boolean;
    function GeneraArchivoAscii: Boolean;
    procedure DoOnGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoAfterGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoOnGetReportes( const lResultado: Boolean );override;
    procedure DoOnGetDatosImpresion;override;
    procedure GeneraOutPut;
    procedure GeneraQRReporte;
    procedure GeneraMailError( const Error : string );
    procedure SendEmail (iCA_FOLIO: Integer; sEE_ATTACH, sEE_FROM_NA, sEE_FROM_AD, sEE_ERR_AD, sEE_TO: String;
      iEE_SUBTYPE: integer; sEE_BODY: String; bNoHayRegistros: Boolean = FALSE; sError: String = VACIO; iUsuario: Integer = 0);
    procedure Log(const sTexto: String);
    procedure ModificaImagenHtml;
    procedure ModificaArchivoImagen(const sArchivo: string);
    procedure AgregaAttachments( oLista : TStrings );
    procedure EliminaReporte( const iReporte : integer );

    function PreparaPlantilla( var sError : WideString ) : Boolean;override;
    procedure DesPreparaPlantilla;override;
    function GeneraReportesEmpleados: Boolean;
    procedure DoBeforePreparaAgente( var oParams:Olevariant );override;

    {$ifdef DOS_CAPAS}
    property ServerSistema: TdmServerSistema read FServerSistema;
    {$else}
    {$endif}
    function GrabarCorreosReporte(CA_FOLIO: Integer; const EE_ATTACH, EE_FROM_NA, EE_FROM_AD, EE_ERR_AD, EE_TO: WideString;
          EE_SUBTYPE: integer; const EE_BODY: WideString; bNoHayRegistros: Boolean = FALSE; sError: String = VACIO; iUsuario: Integer = 0): OleVariant;
    procedure AgregarSolicitudEnvioProgramado(Folio, Reporte: Integer);

  public
    { Public declarations }
    property Empresa: String read FEmpresa write FEmpresa;
    property Frecuencia: Integer read FFrecuencia write FFrecuencia;
    property ArchivoHTML: String read FArchivoHTML;
    property DIRImagenes: string read FDIRImagenes write FDIRImagenes;
    property ClasifActivo: eClasifiReporte read FClasifActivo write FClasifActivo;
    property Clasificaciones: TStrings read FClasificaciones write FClasificaciones;

    property DetenerServicio: Boolean read FDetenerServicio write FDetenerServicio;

    procedure AppParamClear;
    procedure AppParamAdd( const sValue: String );

    procedure AgregarSolicitudesCalensoli;

    procedure ProcesaReportes ();
    procedure CalendarizaPendientes;
    procedure MoverImagenes;
    procedure LogError(const sTexto, sMemo: String; eTipo: eTipoBitacora; const lEnviar: Boolean = TRUE);
  end;

var
  dmReportes: TdmReportes;

implementation
uses
    DCliente,
    ZetaCommonTools,
    ZQrReporteListado
    , ZReporteAscii
    , ZFuncsCliente
    , ZReportConst
    , DGlobal
    , ZGlobalTress
    , IOUTils
    , Types
    , FAutoClasses
    , ZetaServerTools
    , ZFiltroSQLTools;

const
     K_NOMBRE_BITACORA = 'TressReportes.LOG';

     K_MENSAJE_ERROR_HTML = '<HTML> '+
                            '<FONT face=Arial color=#800080 size=4> '+
                            '<HEAD> '+
                            '<STRONG> '+
                            '        <H1> %s <!--Titulo del Reporte--> </H1> '+
                            '        <P> Fecha: %s <!--Fecha del Reporte--></P> '+
                            '</STRONG></HEAD></FONT> '+
                            '<BODY> '+
                            '<DIV align=center> '+
                            '<FONT color=#FF0000 size=5>Notificaci�n del m�dulo de reportes email. '+
                            '<P>El Reporte </P> '+
                            '<P><STRONG>  %s <!--Nombre del reporte--> </STRONG></P> '+
                            '<P>%s <!--Error Reportado--></P> '+
                            '</FONT></DIV></BODY></HTML>';

     K_MENSAJE_ERROR_PLAINTEXT = 'Notificaci�n del m�dulo de reportes email. ' +
                                 'El reporte ' +
                                 '%s ' +
                                 '%s ';

     K_LEE_CALENDARIO = 'SELECT CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO, CA_REPORT, CA_REPEMP, CA_FECHA, CA_HORA, ' +
                        ' CA_FREC, CA_RECUR, CA_DOWS, CA_MESES, CA_MESDIAS, CA_MES_ON, CA_MESWEEK, CA_FEC_ESP, CA_TSALIDA, CA_FSALIDA, CA_ULT_FEC, ' +
                        ' CA_NX_FEC, CA_NX_EVA, CA_US_CHG, CA_CAPTURA FROM CALENDARIO ' +
                        ' WHERE CA_FOLIO NOT IN (SELECT CA_FOLIO FROM CALENSOLI WHERE CAL_ESTATUS <= %d)';

     K_INSERTA_CALENSOLI = 'INSERT INTO CALENSOLI (CA_FOLIO, RE_CODIGO, CAL_FECHA, CAL_ESTATUS, CAL_MENSAJ) VALUES ' +
                             '(:CA_FOLIO, :RE_CODIGO, :CAL_FECHA, :CAL_ESTATUS, :CAL_MENSAJ)';

     K_ACTUALIZA_CALENDARIO = 'UPDATE CALENDARIO SET CA_ULT_FEC = %s, CA_NX_EVA = %s WHERE CA_FOLIO = %d';

     K_TAREAS_PENDIENTES = { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
                           'SELECT TOP 50 CAL_FOLIO, CA.CA_FOLIO,RE_CODIGO,CAL_FECHA,CAL_INICIO,CAL_FIN,CAL_ESTATUS,CAL_MENSAJ, ' +

                           'CM_CODIGO, CA_FREC, CA_REPORT, CA_REPEMP, CA_FSALIDA, CA_FSALIDA, CA_NOMBRE ' +
                           ' FROM CALENSOLI AS CAL, CALENDARIO AS CA ' +
                           ' WHERE CAL.CA_FOLIO = CA.CA_FOLIO ' +
                           ' AND CAL.CAL_ESTATUS = %d';

     K_SP_CALENDARIO = 'EXECUTE SP_CALENDARIO %s';

     K_LEE_CUENTAS = ' SELECT US_CODIGO, US_EMAIL, MIN (VACIO) AS VACIO FROM ' +
                   ' (SELECT U.US_CODIGO, US_EMAIL, RU_VACIO AS VACIO, ''USUARIO'' AS INDICADOR FROM USUARIO U, ROLUSUARIO R ' +
                     ' WHERE U.US_CODIGO = R.US_CODIGO ' +
                       ' AND CA_FOLIO = %d ' +
                         '	AND RU_ACTIVO = ''S'' ' +
                          '	AND US_ACTIVO = ''S'' ' +
                     ' UNION ' +
                     ' SELECT DISTINCT U.US_CODIGO, US_EMAIL, RS_VACIO AS VACIO, ''ROL'' AS INDICADOR FROM USUARIO U, USER_ROL UR, ROLSUSCRIP RS ' +
                     '	WHERE ' +
                     '		U.US_CODIGO = UR.US_CODIGO ' +
                     '		AND UR.RO_CODIGO = RS.RO_CODIGO ' +
                     '		AND CA_FOLIO = %d ' +
                     '		AND RS_ACTIVO = ''S'' ' +
                     '		AND US_ACTIVO = ''S'' ' +
                     ' ) AS SUSCRIPCIONES ' +
                     ' GROUP BY US_CODIGO, US_EMAIL ' +
                     ' ORDER BY US_CODIGO, VACIO ';

procedure TdmReportes.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FServerSistema := TdmServerSistema.Create( self );
     {$endif}
     FUsuariosPlain := TStringList.Create;
     FListaReportes := TStringList.Create;
     cdsListaEmpleados := TZetaClientDataSet.Create(Self);

     {$ifdef PROFILE}
     emailProfiler :=  TZetaProfiler.Create;
     emailProfiler.Start;
     emailProfiler.Check('TdmEmailServiceReporte.DataModuleCreate');
     {$endif}
     
     { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
     FDetenerServicio := FALSE;

     inherited;
end;

procedure TdmReportes.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FListaReportes );
     FreeAndNil( FBitacora );
     FreeAndNil( FUsuariosPlain );
     FreeAndNil( cdsListaEmpleados );
     {$ifdef PROFILE} FreeAndNil( emailProfiler ); {$endif}
     inherited;
end;

procedure TdmReportes.AppParamClear;
begin
     FAppParams.Clear;
end;

procedure TdmReportes.AppParamAdd( const sValue: String );
begin
     FAppParams.Add( UpperCase( sValue ) );
end;

procedure TdmReportes.Log(const sTexto: String);
begin
     FBitacora.Add(sTexto)
end;

procedure TdmReportes.LogError(const sTexto, sMemo: String;eTipo: eTipoBitacora; const lEnviar: Boolean );
begin
     if lEnviar then
     begin
          // GrabaBitacoraServicio (sTexto, sMemo, eTipo);
          GrabaBitacoraServicio (FiltrarTextoSQL(sTexto), FiltrarTextoSQL(sMemo), eTipo);
     end;
end;

procedure TdmReportes.EliminaReporte( const iReporte : integer );
 var sReporte : string;
     i : integer;
begin
     if FBorraArchivo then
        DeleteFile( FArchivoHTML );

     sReporte := IntToStr( iReporte );

     for i:= 0 to FListaReportes.Count -1 do
     begin
          if sReporte = FListaReportes[ i ] then
          begin
               FListaReportes.Delete( i );
               Break;
          end;
     end;
end;

function TdmReportes.GeneraReporte: Boolean;
 var i, iReporte: integer;
begin
     try
        Result := ConectaGlobal;
        if Result then
        begin
             Result := GetSuscripciones;

             for i:= 0 to FListaReportes.Count - 1 do
             begin
                  FArchivoHTML := VACIO;
                  iReporte := StrToIntDef( FListaReportes[ i ], 0 );

                  if iReporte > 0 then
                  begin
                       if GeneraUnReporte (iReporte) then
                       begin
                            Log( Format( 'El reporte #%d ha sido generado en: ' + CR_LF + '" %s "',[iReporte, FArchivoHTML]));
                            SendEmail (FFolioCalendario, DatosImpresion.Exportacion, FFromName, FFromAddress, FErrorMail, VACIO, FSubtype, VACIO);
                       end;
                  end;
             end;

             // Actualizar tabla Calendario con la fecha y hora de �ltima ejecuci�n.
             // actualizaCalendario (DatosImpresion.FolioCalendario);
             actualizaCalendario (FFolioCalendario);

             if not FFolioCalensoliCancelado then
                actualizaCalenSoli (FFolioCalensoli, tcSEFinalizado, VACIO, FALSE);
        end;

     finally
     end;
end;

function TdmReportes.GeneraReportePorSupervisor: Boolean;
 var i, iReporte: integer;
     iUsuario: Integer;
begin
     Result := ConectaGlobal;
     if Result then
     begin
          Result := GetSuscripciones;

          // Recorrer suscripciones.
          // Generar reporte para cada suscripci�n.
          // Hacer env�o de correo indicando cuenta de correo electr�nico.
          //   Con esto se utilizar� parte del mecanismo de lista de empleados.
          while not cdsSuscripcion.Eof do
          begin
               FArchivoHTML := VACIO;
               iReporte := StrToIntDef( FListaReportes[ 0 ], 0 );
               if iReporte > 0 then
               begin
                    dmCliente.Usuario := cdsSuscripcion.FieldByName( 'US_CODIGO' ).AsInteger ;
                    iUsuario := cdsSuscripcion.FieldByName( 'US_CODIGO' ).AsInteger ;
                    if GeneraUnReporte (iReporte) then
                    begin
                         Log( Format( 'El reporte #%d de la clasificaci�n de Supervisores ha sido generado para el usuario %d en: ' + CR_LF + '" %s "',
                              [iReporte, iUsuario, FArchivoHTML]));

                         // Correcci�n de bug: #20334 AGNICO EAGLE - Cuando un usuarios suscrito al envio programado no tiene cuenta de correo electronico se comienzan a duplicar reportes a enviar, ocasionando se traten de enviar m�s correos de los esperados.
                         SendEmail (FFolioCalendario, DatosImpresion.Exportacion, FFromName, FFromAddress, FErrorMail,
                                   cdsSuscripcion.FieldByName('US_EMAIL').AsString, FSubtype, VACIO, FALSE, VACIO, iUsuario);
                    end;
               end;

               // Siguiente supervisor.
               cdsSuscripcion.Next;
          end;

          // Actualizar tabla Calendario con la fecha y hora de �ltima ejecuci�n.
          actualizaCalendario (FFolioCalendario);

          if not FFolioCalensoliCancelado then
             actualizaCalenSoli (FFolioCalensoli, tcSEFinalizado, VACIO, FALSE);
     end;
end;

function TdmReportes.ConectaGlobal: Boolean;
begin
     Result := FALSE;

     with Global do
     begin
          Conectar;
          FFromAddress := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMADRESS ), 'ReportesViaEmail@dominio.com' );
          FFromName := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMNAME ), 'Sistema TRESS-M�dulo Reportes Email' );
          FErrorMail := Global.GetGlobalString( K_GLOBAL_EMAIL_MSGERROR );

          Result := TRUE;
     end;
end;

procedure TdmReportes.ModificaArchivoImagen( const sArchivo : string );
 const
      K_IMAGE_DIR1 = 'src="file://';
      K_IMAGE_DIR2 = 'src="';
 var
    oLista : TStrings;
begin
     if FileExists( sArchivo ) then
     begin
          oLista := TStringList.Create;
          try
             oLista.LoadFromFile( sArchivo );

             with dmCliente.DatosServicioReportesCorreos do
             begin
                  FURLImagenes := URLImagenes;
             end;

             if StrLleno( FURLImagenes ) then
                oLista.Text := StrTransAll( oLista.Text,
                                            K_IMAGE_DIR1 + VerificaDir( ExtractFileDir(sArchivo) ),
                                            K_IMAGE_DIR2 + VerificaDirURL( FURLImagenes ) );

             // Si es html, copiar im�genes al directorio global.
             if DatosImpresion.Tipo in [tfImpresora, tfHTML, tfHTM] then
             begin
                  MoverImagenes;
             end;

             oLista.SaveToFile( sArchivo );
          finally
                 FreeAndNil( oLista );
          end;
     end;
end;

procedure TdmReportes.MoverImagenes;
var
   aFiles: TStringDynArray;

  procedure CopyFilesToPath(aFiles: array of string; DestPath: string);
  var
     InFile, OutFile: string;
  begin
       for InFile in aFiles do
       begin
            OutFile := TPath.Combine( DestPath, TPath.GetFileName( InFile ) );
            try
               TFile.Move (InFile, OutFile);
            except
                  on Error: Exception do
                  begin
                       // Eliminar OutFile.
                       // Volver a intentar escribir el archivo.
                       try
                          DeleteFile(OutFile);
                          TFile.Move (InFile, OutFile);
                       except
                             // Escribir error en bit�cora de servicio
                             // informando de error al mover im�genes de reporte en html.
                             LogError('No es posible cargar im�genes en directorio para los reportes HTML generados', Error.Message, tbErrorGrave);
                             // Si hay alg�n problema para mover la imagen, se elimina de su ubicaci�n original.
                             DeleteFile(InFile);
                       end;
                  end;
            end;
       end;
  end;

begin
     with dmCliente.DatosServicioReportesCorreos do
     begin
          aFiles := TDirectory.GetFiles(DirectorioReportes, TPath.GetFileNameWithoutExtension(FArchivoHTML) + '*.jpg', TSearchOption.soTopDirectoryOnly);
          CopyFilesToPath(aFiles, DirectorioImagenes);
     end;
end;

procedure TdmReportes.ModificaImagenHtml;
 var
    iPageCount : integer;
    sArchivo, sDir, sExt : string;
    oFile: TSearchRec;
begin
     if ( DatosImpresion.Tipo in [tfImpresora,tfHtml,tfHtm] ) then
     begin
          sArchivo := FArchivoHTML;
          iPageCount := 0;
          if UnSoloHTML then
          begin
               ModificaArchivoImagen( sArchivo );
          end
          else
          begin
               sExt := ExtractFileExt( sArchivo );
               sDir := ExtractFilePath( sArchivo );
               sArchivo := Copy( sArchivo, 1, Pos( sExt, sArchivo )-1 ) + '*' + sExt;

               if FindFirst( sArchivo, faArchive, oFile) = 0 then
               begin
                    ModificaArchivoImagen( sDir + oFile.Name );
                    Inc( iPageCount );
                    while ( iPageCount <= FPageCount) AND ( FindNext( oFile ) = 0 ) do
                    begin
                         ModificaArchivoImagen( sDir + oFile.Name );
                         Inc( iPageCount );
                    end;
               end;
          end;
     end;
end;

function ReemplazaVariablesCorreo( var sMensaje, sAttach:string):string;
const
     K_IMAGEN = '/LOGOCORREO.JPG';
var
     sURLImagen:String;
begin
        sURLImagen :='http://www.tress.com.mx/imagesgti/'+ K_IMAGEN;
        // Validar con el directorio de im�genes, ya que es la ruta a la cual tiene acceso el servidor.
        if FileExists(dmCliente.DatosServicioReportesCorreos.DirectorioImagenes + K_IMAGEN) then
           sURLImagen := dmCliente.DatosServicioReportesCorreos.URLImagenes + K_IMAGEN;

        sMensaje := StringReplace( sMensaje, '##IMAGEN', sURLImagen , [rfReplaceAll] );
        sMensaje := StringReplace( sMensaje, '##NOMBRE', TPath.GetFileNameWithoutExtension (sAttach), [rfReplaceAll] );
        Result := sMensaje;
end;

function ReemplazaVariablesCorreoError( var sMensaje, sAttach, sFecha, sError, sNombreCalendario:string):string;
const
     K_IMAGEN = '/LOGOCORREO.JPG';
var
     sURLImagen:String;
begin
        sURLImagen :='http://www.tress.com.mx/imagesgti/'+ K_IMAGEN;
        // Validar con el directorio de im�genes, ya que es la ruta a la cual tiene acceso el servidor.
        if FileExists(dmCliente.DatosServicioReportesCorreos.DirectorioImagenes + K_IMAGEN) then
           sURLImagen := dmCliente.DatosServicioReportesCorreos.URLImagenes + K_IMAGEN;

        sMensaje := StringReplace( sMensaje, '##IMAGEN', sURLImagen , [rfReplaceAll] );
        sMensaje := StringReplace( sMensaje, '##ENVIO', sNombreCalendario , [rfReplaceAll] );
        sMensaje := StringReplace( sMensaje, '##MENSAJE', sError , [rfReplaceAll] );

        Result := sMensaje;
end;

procedure TdmReportes.GeneraMailError( const Error : string );
 const K_ARCHIVO = 'Error$Temp';
 var oArchivo : TStrings;
     sMensaje, sArchivo : string;
begin
     {
      REPORTES EMAIL V2017.
      Servicio de reportes.

      Siempre se env�a notificaci�n si no hay datos para el reporte.
      Y siempre se env�a en formato HTML.
      Esto puede cambiar durante la implementaci�n de piloto.
      Se hace as� porque hasta el momento no hay forma de indicar a los usuarios suscritos
      si quiere recibir notificaci�n cuando el reporte no genere datos, sin embargo,
      existe el campo RU_VACIO en RolUsuario y RS_VACIO en RolSuscrip que indica esta configuraci�n/
      Este campo se llenar� correctamente con la herramienta de importaci�n de tareas de TressEmail
      para poder ser usado en un futuro.
     }
     oArchivo := TStringList.Create;
     try
        sMensaje := Format ( K_MENSAJE_ERROR_HTML, [ cdsReporte.FieldByName('RE_TITULO').AsString,
                                                     FormatDateTime('dd/mmm/yyyy', DATE),
                                                     NombreReporte,
                                                     Error] );

        sArchivo := ZReportTools.DirPlantilla + K_ARCHIVO + '.html';
        oArchivo.Text := sMensaje;
        oArchivo.SaveToFile( sArchivo, TEncoding.Unicode );
     finally
            FArchivoHTML := sArchivo;
            FreeAndNil( oArchivo );
     end;
end;

procedure TdmReportes.DoOnGetResultado( const lResultado : Boolean; const Error : string );
var
   sMensaje: String;
   sMensajeHTML: String;
begin
     if lResultado then
     begin
          if ( not GeneraListaEmpleados ) then
             GeneraOutPut;
     end
     else
     begin
          // Se env�a el par�metro bNoHayRegistros en TRUE para enviar notificaci�n de que no hay datos
          // para los usuarios que as� lo indiquen en su suscripci�n,
          // ya sea en RolUsuario o en RolSuscrip.
          FSubtype := 0;

          // Correcci�n de Bug #17357: Interiores a�reos: Envi� de multiples correos vacios en reportes email con nuevo mecanismo v.2017
          if (FReporteSupervisor) and (cdsSuscripcion.FieldByName('VACIO').AsInteger = 0) then
             SendEmail (FFolioCalendario, VACIO, FFromName, FFromAddress, FErrorMail, cdsSuscripcion.FieldByName('US_EMAIL').AsString,
             FSubtype, sMensajeHTML, TRUE, Error)
          else
              SendEmail (FFolioCalendario, VACIO, FFromName, FFromAddress, FErrorMail, VACIO, FSubtype, sMensajeHTML, TRUE, Error)          
     end;
end;

procedure TdmReportes.DoAfterGetResultado( const lResultado : Boolean; const Error : string );
begin
     if lResultado and ( not GeneraListaEmpleados ) then
     begin
          FArchivoHTML := DatosImpresion.Exportacion;
          ModificaImagenHtml;
     end;
end;

procedure TdmReportes.DoOnGetReportes( const lResultado: Boolean );
begin
end;

function TdmReportes.GeneraUnReporte(const iReporte: Integer): Boolean;
begin
     Result := GetResultado( iReporte, TRUE );

     if (not Result) and FFolioCalensoliCancelado then
     begin
          LogError ('Env�o cancelado', 'Cancelado por el usuario', tbNormal);
     end
     else if FFolioCalensoliCancelado then
     begin
          LogError ('Error al realizar consulta', 'No se gener� resultado para la consulta SQL del reporte solicitado.', tbNormal);
     end;

end;

procedure TdmReportes.AgregaAttachments( oLista : TStrings );
 var
    oFile: TSearchRec;
    sExt, sDir : string;
    iPageCount : integer;
begin
     iPageCount := 0;

     if DatosImpresion.Tipo IN [ tfHTML,tfHTM,tfTXT,tfWMF,tfEMF,tfBMP,tfPNG,tfJPEG,tfSVG ] then
     begin
          sExt := ExtractFileExt( FArchivoHTML );
          sDir := ExtractFilePath( FArchivoHTML );
          if FindFirst( Copy( FArchivoHTML, 1, Pos( sExt, FArchivoHTML )-1 ) + '*' + sExt, faArchive, oFile) = 0 then
          begin
               oLista.Add( sDir + oFile.Name );
               Inc( iPageCount );
               while ( iPageCount <= FPageCount) AND ( FindNext( oFile ) = 0 ) do
               begin
                    oLista.Add( sDir + oFile.Name );
                    Inc( iPageCount );
               end;
          end;
     end
     else
         oLista.Add( FArchivoHTML );
end;

procedure TdmReportes.SendEmail (iCA_FOLIO: Integer; sEE_ATTACH, sEE_FROM_NA, sEE_FROM_AD, sEE_ERR_AD, sEE_TO: String;
    iEE_SUBTYPE: integer; sEE_BODY: String; bNoHayRegistros: Boolean = FALSE; sError: String = VACIO; iUsuario: Integer = 0);
const K_FORMATO_GRAFICO = '.BMP,.EMF,.JPG,.PNG,.SVG';
var slEE_BODY: TStringList;
     aFiles: TStringDynArray;
     InFile: String;
     iIndexReportesImagen: Integer;

     function esReporteEnImagen (sExt: String): boolean;
     begin
          Result := FALSE;
          if Pos (UpperCase (sExt), K_FORMATO_GRAFICO) > 0 then
             Result := TRUE;
     end;

begin
     slEE_BODY := TStringList.Create;

     try
        if iEE_SUBTYPE = 1 then
        begin
             slEE_BODY.LoadFromFile(FArchivoHTML);
        end;

        sEE_BODY := slEE_BODY.Text;

        if esReporteEnImagen (SysUtils.ExtractFileExt (sEE_ATTACH)) then
        begin
             aFiles := TDirectory.GetFiles (dmCliente.DatosServicioReportesCorreos.DirectorioReportes,
                    TPath.GetFileNameWithoutExtension (sEE_ATTACH) + '*' + SysUtils.ExtractFileExt(sEE_ATTACH),
                    TSearchOption.soTopDirectoryOnly);

             sEE_ATTACH := VACIO;
             for InFile in aFiles do
             begin
                  sEE_ATTACH := sEE_ATTACH + InFile + ',';
             end;
             sEE_ATTACH := CortaUltimo (sEE_ATTACH);
        end;

        try
           // Utilizar m�todo local para grabar correo.
           // Este servicio puede comunicarse directamente con la base de datos.

           // Correcci�n de bug: #20334 AGNICO EAGLE - Cuando un usuarios suscrito al envio programado no tiene cuenta de correo electronico se comienzan a duplicar reportes a enviar, ocasionando se traten de enviar m�s correos de los esperados.
           // GrabarCorreosReporte (iCA_FOLIO, sEE_ATTACH, sEE_FROM_NA, sEE_FROM_AD, sEE_ERR_AD, sEE_TO, iEE_SUBTYPE, sEE_BODY, bNoHayRegistros, sError);
           GrabarCorreosReporte (iCA_FOLIO, sEE_ATTACH, sEE_FROM_NA, sEE_FROM_AD, sEE_ERR_AD, sEE_TO, iEE_SUBTYPE, sEE_BODY, bNoHayRegistros, sError, iUsuario);
        except
              on Error: Exception do
              begin
                   LogError ('Error al grabar solicitud de env�o de correo.', Error.Message, tbErrorGrave)
              end;
        end;

        // S� el tipo de correo es 1 =  Html, eliminar el archivo generado.
        if iEE_SUBTYPE = 1 then
        begin
             try
                DeleteFile (FArchivoHTML);
             except
                   on Error: Exception do
                   begin
                        // Escribir error en bit�cora de servicios.
                        // Informar de error al intentar eliminar reporte en html
                        LogError (Format ('No es posible eliminar archivo HTML: "%s".', [FArchivoHTML]), Error.Message, tbErrorGrave)
                   end;
             end;
        end;

     finally
            FreeAndNil (slEE_BODY);
     end;
end;

procedure TdmReportes.DoOnGetDatosImpresion;
var oDatosImpresion : TDatosImpresion;
begin
     oDatosImpresion := DatosImpresion;
     // Si es tipo impresora, no borrar el archivo, mantener el reporte en formato HTML.
     with oDatosImpresion do
     begin
          if Tipo in [tfImpresora] then
          begin
               Tipo := tfHtml;
          end;

          DatosImpresion := oDatosImpresion;
     end;
end;

function TdmReportes.GetExtensionDef : string;
 var eTipo : eExtFormato;
begin
     eTipo := tfHtml;
     Result := ObtieneElemento( lfExtFormato, Ord( eTipo ) );
end;

procedure TdmReportes.BorraArchivos;
 var
    iPage : integer;
    sExt, sNombre, sArchivo : string;
begin
     if FileExists( DatosImpresion.Exportacion ) then
     begin
          DeleteFile( DatosImpresion.Exportacion );
     end
     else
     begin
          iPage := 1;
          with DatosImpresion do
          begin
               sExt := ExtractFileExt( Exportacion );
               sNombre := Copy( Exportacion, 1, Pos( sExt, Exportacion )-1 );
          end;

          sArchivo := sNombre + IntToStr( iPage ) + sExt;

          while FileExists( sArchivo ) do
          begin
               DeleteFile( sArchivo );
               Inc( iPage );
               sArchivo := sNombre + IntToStr( iPage ) + sExt;
          end;
     end;
end;

function TdmReportes.GetSobreTotales: Boolean;
 var i : integer;
begin
     Result := FALSE;
     for i := 0 to Campos.Count - 1 do
     begin
          with TCampoListado( Campos.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion = ocSobreTotales);
          end;
     end;
end;

procedure TdmReportes.GeneraQRReporte;
 var eTipo : eTipoReporte;
begin
     with cdsReporte do
     begin
          eTipo := eTipoReporte( FieldByName('RE_TIPO').AsInteger );
          if ( eTipo = trForma ) then
          begin
               QRReporteListado.GeneraForma (FieldByName('RE_NOMBRE').AsString,
                                             FALSE,
                                             cdsResultados )
          end
          else if ( eTipo = trListado ) then
          begin
               QRReporteListado.GeneraListado (FieldByName('RE_NOMBRE').AsString,
                                               FALSE,
                                               zStrToBool( FieldByName('RE_VERTICA').AsString ),
                                               zStrToBool( FieldByName('RE_SOLOT').AsString ),
                                               GetSobreTotales,
                                               cdsResultados,
                                               Campos,
                                               Grupos );
          end;
     end;
end;

procedure TdmReportes.ModificaListaCampos( Lista : TStrings );
 var i : integer;
begin
     for i:=0 to Lista.Count -1 do
        with TCampoOpciones(Lista.Objects[i]) do
             if PosAgente >= 0 then
             begin
                  SQLColumna := FSQLAgente.GetColumna(PosAgente);
                  with FSQLAgente.GetColumna(PosAgente) do
                  begin
                       TipoImp := TipoFormula;
                       OpImp := Totalizacion;
                  end;
             end;
end;

procedure TdmReportes.ModificaListas;
 var i : integer;
begin
     ModificaListaCampos( Campos );
     for i:= 0 to Grupos.Count - 1 do
     begin
          with TGrupoOpciones( Grupos.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
end;

procedure TdmReportes.GeneraOutPut;
begin
     ModificaListas;
     BorraArchivos;

     with cdsResultados do
     begin
          if Active AND Not IsEmpty then
          begin
               First;

               ZFuncsCliente.RegistraFuncionesCliente;
               //Asignacion de Parametros del Reporte,
               //para poder evaluar la funcion PARAM() en el Cliente;
               ZFuncsCliente.ParametrosReporte := FSQLAgente.Parametros;

               case DatosImpresion.Tipo of
                    tfMailMerge,tfMailMergeXLS:;
                    tfASCIIFijo, tfASCIIDel, tfCSV : GeneraArchivoAscii
                    else GeneraQRReporte;
               end;
          end;
     end;
end;

function TdmReportes.GetSoloTotales: Boolean;
begin
     Result := zStrToBool( cdsReporte.FieldByName('RE_SOLOT').AsString );
end;

function TdmReportes.GeneraArchivoAscii: Boolean;
 var oReporteAscii : TReporteAscii;
begin
     oReporteAscii := TReporteAscii.Create;
     try
        Result := oReporteAscii.GeneraAscii( cdsResultados,
                                             DatosImpresion,
                                             Campos,
                                             Grupos,
                                             GetSoloTotales,
                                             FALSE );
     finally
            FreeAndNil( oReporteAscii );
     end;
end;

function TdmReportes.PreparaPlantilla( var sError : WideString ) : Boolean;
 var
    lHayImagenes : Boolean;
    sDirectorio : string;
begin
     Result := TRUE;
     ZQRReporteListado.PreparaReporte;
     sDirectorio := ExtractFilePath( DatosImpresion.Archivo );
     Result := DirectoryExists( sDirectorio );
     if NOT Result then
     begin
          LogError ('Error al preparar plantilla', Format ('El directorio de plantillas "%s" no existe', [ sDirectorio ]), tbError);
     end;

     // sDirectorio := ExtractFilePath( DatosImpresion.Exportacion );
     {Result := DirectoryExists( sDirectorio );
     if Not Result then
        sError := Format( 'El directorio del archivo " %s " no existe', [ sDirectorio ] );}

     with dmCliente.DatosServicioReportesCorreos do
     begin
          sDirectorio := DirectorioReportes;
          if not StrLleno (sDirectorio) then
          begin
               Result := FALSE;
               LogError ('Error al preparar reporte', 'Global de directorio de reportes no se encuentra configurado.', tbError);
          end
          else
          begin
               if not DirectoryExists( sDirectorio ) then
               begin
                    Result := FALSE;
                    LogError ('Error al preparar reporte',
                             Format ('El directorio para servicio de reportes "%s" no existe', [ sDirectorio ]), tbError);
               end;
          end;

          {sDirectorio := DirectorioImagenes;
          if not StrLleno (sDirectorio) then
          begin
               Result := FALSE;
               LogError ('Error al preparar reporte', 'Global de directorio de im�genes no se encuentra configurado.', tbError)
          end
          else
          begin
               if not DirectoryExists( sDirectorio ) then
               begin
                    Result := FALSE;
                    LogError ('Error al preparar reporte',
                             Format ('No existe directorio de im�genes para servicio de reportes.' + CR_LF + 'Directorio: "%s".', [sDirectorio]), tbError);
               end;
          end;}

          {sDirectorio := URLImagenes;
          if not StrLleno (sDirectorio) then
          begin
               Result := FALSE;
               LogError ('Error al preparar reporte', 'Global de URL de im�genes no se encuentra configurado.', tbError);
          end;}
     end;

     if Result then
     begin
          DoOnGetDatosImpresion;

          with QRReporteListado do
          begin
               Init( FSQLAgente,
                     DatosImpresion,
                     Parametros.Count,
                     lHayImagenes );

               AddNombreImagenes( FNombresImagenes );
          end;
     end;

     ContieneImagenes := lHayImagenes;
end;

procedure TdmReportes.DesPreparaPlantilla;
begin
     with QRReporteListado do
     begin
          FPageCount := PageCount;
          FUnSoloHTML := UnSoloHTML;
     end;

     ZQrReporteListado.DesPreparaReporte;
end;

function TdmReportes.SistemaBloqueado: Boolean;
begin
     Result:= zstrToBool( ServerLogin.GetClave( ZetaServerTools.CLAVE_SISTEMA_BLOQUEADO ) );
end;


function TdmReportes.GeneraReportesEmpleados: Boolean;
var
   i, iReporte,iPosAgente : integer;
const
     K_CONST_EMPLEADO = 'EMPLEADO';
     K_CONST_EMAIL = 'EMAIL';
     K_CONST_NUMPER = 'NUM_PERIODO';
     K_CONST_TIPOPER = 'TIPO_PERIODO';
     K_CONST_YEARPER = 'ANIO_PERIODO';
     K_COLUMNA = 'COLUMNA';

   function GetFieldName:string;
   begin
        with FSQLAgente.GetColumna(iPosAgente) do
        begin
             if Length(Alias) > 0 then
                  Result := Alias
             else
                 Result := K_COLUMNA + IntToStr(iPosAgente);
        end;
   end;

   function GetValorEnteroColumna:Integer;
   begin
        if FSQLAgente.GetColumna(iPosAgente).Esconstante then
           Result := StrToInt( FSQLAgente.GetColumna(iPosAgente).ValorConstante)
        else
        begin
             Result := cdsListaEmpleados.FieldByname( GetFieldName ).AsInteger;
        end;
   end;

   function AsignaEnteroOpcional( const sCampo: String ): Integer;
   begin
        with cdsListaEmpleados do
        begin
             if Assigned( FindField( sCampo ) ) then
                Result := FieldByname( sCampo ).AsInteger
             else
             		 Result := 0;
        end;
   end;

   procedure ActualizaValoresActivos;
   var
      oDatosPeriodo:TDatosPeriodo;
   begin
        with cdsListaEmpleados do
        begin
             try
                FUsuariosPlain.Clear;
                FUsuariosPlain.Add( FieldByname( K_CONST_EMAIL ).AsString);
                dmCliente.Empleado := FieldByname( K_CONST_EMPLEADO ).AsInteger;
				oDatosPeriodo := dmCliente.PeriodoActivo ;
                with oDatosPeriodo do
                begin
                     Numero := AsignaEnteroOpcional( K_CONST_NUMPER );
                     Year := AsignaEnteroOpcional( K_CONST_YEARPER );
                     Tipo := eTipoPeriodo( AsignaEnteroOpcional( K_CONST_TIPOPER ) );

                     if Assigned( FindField( K_CONST_NUMPER ) ) then
                        Numero := FieldByname( K_CONST_NUMPER ).AsInteger;
                     if Assigned( FindField( K_CONST_YEARPER ) ) then
                        Year := FieldByname( K_CONST_YEARPER ).AsInteger;
                     if Assigned( FindField( K_CONST_TIPOPER ) ) then
                        Tipo := eTipoPeriodo( FieldByname( K_CONST_TIPOPER ).AsInteger );
                end;
                dmCliente.PeriodoActivo := oDatosPeriodo
             except
                  on Error:Exception do
                  begin
                       LogError('Error al leer valores activos y par�metros', Error.Message, tbErrorGrave);
                  end;
            end;
        end;
   end;

begin
     try
        Result := ConectaGlobal;

        GeneraListaEmpleados := True;
        if GeneraUnReporte (FReporteEmpleados)then
        begin
             with cdsListaEmpleados do
             begin
                  Data := cdsResultados.Data;
                  dmCliente.GeneraListaEmpleados := True; //Se prende nueva funcionalidad
                  while not Eof do
                  begin
                       ActualizaValoresActivos; //Se cambian valores activos por cada empleado
                       for i:= 0 to FListaReportes.Count - 1 do
                       begin
                            iReporte := StrToIntDef( FListaReportes[ i ], 0 );
                            if iReporte > 0 then
                            begin
                                 GeneraListaEmpleados := False;
                                 Log(Format('Generando reporte: %d, empleado: %d',[iReporte,dmCliente.Empleado]));
                                 // Agregar el GeneraUnReporte a un try.
                                 try
                                    Result := GeneraUnReporte (iReporte);
                                    try
                                       if Result then
                                       begin
                                            if ((FUsuariosPlain.Count > 0) and (FUsuariosPlain.Strings[0] <> VACIO) and (FUsuariosPlain[0] <> VACIO)) then                                            
                                            begin
                                                 SendEmail (FFolioCalendario, DatosImpresion.Exportacion, FFromName, FFromAddress, FErrorMail, FUsuariosPlain[0], FSubtype, VACIO);
                                            end
                                            else
                                            begin
                                                 LogError('Error al generar reporte con lista de empleados', Format('No se configur� el correo electr�nico del empleado #: %d', [dmCliente.Empleado] ), tbError);
                                            end;
                                       end;
                                    except
                                          on Error:Exception do
                                          begin
                                               LogError('Error al generar reporte con lista de empleados', Format('No se logr� enviar correo a: %s Mensaje: %s',[FUsuariosPlain[0],Error.Message] ), tbErrorGrave);
                                          end;
                                    end;
                                 except
                                       on Error:Exception do
                                       begin
                                            LogError('Error al generar reporte con lista de empleados', Format('No se logr� generar el reporte n�mero: %d para el empleado %d Mensaje: %s', [iReporte, dmCliente.Empleado, Error.Message]), tbErrorGrave);
                                       end;
                                 end;
                            end;
                       end;
                       Next;    //Siguiente empleado de la lista
                  end;
                  // Actualizar tabla Calendario con la fecha y hora de �ltima ejecuci�n.
                  // actualizaCalendario (DatosImpresion.FolioCalendario);
                  actualizaCalendario (FFolioCalendario);

                  if not FFolioCalensoliCancelado then
                     actualizaCalenSoli (FFolioCalensoli, tcSEFinalizado, VACIO, FALSE);
             end;
        end
        else
        begin
             actualizaCalenSoli (FFolioCalensoli, tcSEFinalizado, VACIO, FALSE);
        end;

     finally
            dmCliente.GeneraListaEmpleados := False;
     end;
end;

procedure TdmReportes.DoBeforePreparaAgente( var oParams:Olevariant );
const
     P_TITULO = 1;
     P_FORMULA = 2;
     P_TIPO = 3;
     K_PARAM = 'PARAM';
var
   i:Integer;
   sField:string;
   oParam:OleVariant;
   eTipo:eTipoGlobal;

   procedure SetParamValue( const iParametro: Integer;const sValor:string );
   begin
        if ( iParametro > 0 ) and ( iParametro <= K_MAX_PARAM ) then
        begin
             oParam := VarArrayCreate( [ P_TITULO, P_TIPO ], varVariant );
             oParam := oParams[ iParametro ];
             eTipo := oParam[P_TIPO];
             try
                case eTipo of
                     tgBooleano: oParam[ P_FORMULA ] := ZetaCommonTools.zStrToBool( Trim(sValor) );
                     tgFloat: oParam[ P_FORMULA ] := StrToFloat( Trim(sValor)  );
                     tgNumero: oParam[ P_FORMULA ] := StrToFloat( Trim(sValor)  );
                     tgFecha:
                                             begin
                                                              oParam[ P_FORMULA ] := cdsListaEmpleados.FieldByName(sField).AsDateTime;
                             end;
                     tgTexto: oParam[ P_FORMULA ] := Trim(sValor) ;
                end;
             except
                   on Error: Exception do
                   begin
                        LogError('Error al preparar consulta para el reporte', Error.Message, tbErrorGrave);
                   end;
             end;
             oParams[ iParametro ] := oParam;
        end;
   end;

begin
     for i := 0 to Parametros.Count -1  do
     begin
          sField := K_PARAM+IntToStr(i+1);
          if cdsListaEmpleados.FindField(sField) <> nil then
             SetParamValue(i+1,cdsListaEmpleados.FieldByName(sField).AsString);
     end;
end;

procedure TdmReportes.CalendarizaPendientes;
begin
     try
        oZetaProvider.EjecutaAndFree (Format (K_SP_CALENDARIO, [EntreComillas (FormatDateTime( 'yyyymmdd hh:mm', Now))]));
     except
           on Error: Exception do
           begin
                // Escribir en bit�cora de servicio.
                LogError ('No es posible establecer fecha de siguiente ejecuci�n para el env�o programado.', Error.Message, tbErrorGrave);
           end;
     end;
end;

procedure TdmReportes.ProcesaReportes;
const K_CANVAS = 'Canvas';
var
   FTareasPendientes: TZetaCursor;
   index: Integer;
begin
     try
        index := 0;

        if InitAutorizacion( okReportesMail ) then
        begin
             ModuloAutorizado := TRUE;
             try
                if SistemaBloqueado then
                begin
                     LogError ('Sistema bloqueado', 'El administrador ha bloqueado el acceso al sistema', tbAdvertencia, TRUE);
                end
                else
                begin
                     with oZetaProvider do
                     begin
                          FTareasPendientes := CreateQuery(Format (K_TAREAS_PENDIENTES, [ord (tcSEPendiente)]));
                     end;
                     FTareasPendientes.Active := TRUE;

                     FTareasPendientes.First;
                     while not FTareasPendientes.Eof do
                     begin
                          // Iniciar bit�coras por cada tarea a ejecutar de Calendario de reportes.
                          FBitacora := TStringList.Create;
                          try
                             // Lo primero al inciar la ejecuci�n de una tarea, es cambiar su estatus a Procesando.
                             actualizaCalenSoli(FTareasPendientes.FieldByName ('CAL_FOLIO').AsInteger, tcSEProcesando, VACIO, TRUE);

                             Log( 'Proceso: Reportes V�a EMail' );
                             Log( '' );
                             Log( 'Inicio: '+ FormatDateTime('dd/mmm/yyyy hh:mm:ss', Now ) );

                             FEmpresa := FTareasPendientes.FieldByName ('CM_CODIGO').AsString;
                             FFrecuencia := FTareasPendientes.FieldByName ('CA_FREC').AsInteger;

                             FUsuarios := VACIO;

                             FReportes := FTareasPendientes.FieldByName ('CA_REPORT').AsString;
                             FListaReportes.CommaText := FReportes;
                             FSoloArchivos := '';
                             FReporteEmpleados := FTareasPendientes.FieldByName ('CA_REPEMP').AsInteger;

                             FSubtype := 1;
                             //(@am): Siempre envia HTML, ya sea que el contenido sea el reporte o solo una notificacion.

                             dmCliente.GeneraListaEmpleados := False;
                             FCopia := VACIO;
                             if FTareasPendientes.FieldByName ('CA_FSALIDA').AsString <> VACIO then
                                FCopia := FTareasPendientes.FieldByName ('CA_FSALIDA').AsString;

                             FFolioCalensoli := FTareasPendientes.FieldByName ('CAL_FOLIO').AsInteger;
                             FFolioCalensoliCancelado := FALSE;
                             FReporteSupervisor := FALSE;
                             FFolioCalendario := FTareasPendientes.FieldByName ('CA_FOLIO').AsInteger;
                             FNombreCalendario := FTareasPendientes.FieldByName ('CA_NOMBRE').AsString;

                             // Ejecutar reporte.
                             if dmCliente.BuscaCompany( FEmpresa ) then
                             begin
                                  if GetReportes (StrToInt (FReportes)) then
                                  begin
                                       if cdsReporte.Active then
                                       begin
                                            FReporteSupervisor := eClasifiReporte( cdsReporte.FieldByName( 'RE_CLASIFI' ).AsInteger ) = crSupervisor;
                                       end;
                                  end;

                                  // Para evitar el error:
                                  // cdsSuscripcion Field US_CODIGO not found
                                  // el cual se genera cuando se quiere enviar un reporte de la clasificaci�n de supervisores
                                  // por medio del mcanismo de lista de empleados, se revisar� si se cumplen
                                  // las condiciones y, en este caso, se cancela la ejecuci�n.
                                  if (FReporteEmpleados > 0) and (FReporteSupervisor) then
                                  begin
                                       FFolioCalensoliCancelado := TRUE;
                                       CancelarSolicitudesCalensoli (FFolioCalensoli,
                                                                    'No es posible generar reporte por listado de empleados para la clasificaci�n de supervisores.');
                                  end
                                  else
                                  begin
                                       if FReporteEmpleados > 0 then
                                       begin
                                            GeneraReportesEmpleados;
                                       end
                                       else if FReporteSupervisor = FALSE then
                                       begin
                                            GeneraReporte;
                                       end
                                       else if FReporteSupervisor then
                                       begin
                                            GeneraReportePorSupervisor;
                                       end
                                  end;
                             end;

                             Log( '' );
                             Log( 'Final: '+ FormatDateTime('dd/mmm/yyyy hh:mm:ss', Now ) );

                             // Antes de pasar a la siguiente tarea, generar bit�cora.
                             GrabaBitacoraServicio (Format ('Ejecuci�n de env�o programado "%s"', [FNombreCalendario]), FBitacora.Text, tbNormal);

                          finally
                                 FreeAndNil (FBitacora);
                          end;

                          FTareasPendientes.Next;
                     end;

                     FTareasPendientes.Active := FALSE;
                end;
             except
                   on Error:Exception do
                   begin
                        FDetenerServicio := TRUE;
                        { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
                        if Pos (K_CANVAS, Error.Message) > 0 then
                        begin

                             LogError( 'Error al ejecutar reportes email', Error.Message, tbAdvertencia, TRUE);
                             actualizaCalendario(FFolioCalendario);
                             // Ya que se trata del error de "Canvas ...", dejar solicitud como pendiente,
                             // para ser atendida cuando el servicio sea iniciado.
                             actualizaCalenSoli (FFolioCalensoli, tcSEPendiente, Error.Message, FALSE);
                        end
                        else
                        begin
                             LogError( 'Error al ejecutar reportes email', Error.Message, tbError, TRUE);
                             actualizaCalendario(FFolioCalendario);
                             // Marcar la solicitud con error y no ser� procesada cuando se reinicie el servicio.
                             actualizaCalenSoli (FFolioCalensoli, tcSEConerror, Error.Message, FALSE);
                        end;
                   end;
             end;
        end
        else
        begin
             if ModuloAutorizado then
             begin
                  ModuloAutorizado := FALSE;
                  // Escribir error en bit�cora de servicios.
                  GrabaBitacoraServicio('M�dulo no autorizado', 'El m�dulo de ' + Autorizacion.GetModuloStr( okReportesMail ) + ' no est� autorizado.' + CR_LF + 'Consulte a su distribuidor.', tbAdvertencia);
             end;
        end;
     finally
            FreeAndNil (FTareasPendientes);
     end;
end;

procedure TdmReportes.actualizaCalendario (iCA_FOLIO: Integer);
begin
     try
        with oZetaProvider do
        begin
             EjecutaAndFree (Format (K_ACTUALIZA_CALENDARIO,
                            [ EntreComillas (FormatDateTime ('mm/dd/yyyy hh:nn:ss.zzz', Now)), EntreComillas (K_GLOBAL_SI), iCA_FOLIO]));
        end;
     except
           on Error: Exception do
           begin
                // Escribir en bit�cora de servicio.
                // No pudo ser actualizada la tabla Calendario.
                LogError ('No es posible actualizar env�o programado de reportes', Error.Message, tbErrorGrave);
           end;
     end;
end;

function TdmReportes.GrabarCorreosReporte(CA_FOLIO: Integer; const EE_ATTACH, EE_FROM_NA, EE_FROM_AD, EE_ERR_AD, EE_TO: WideString;
      EE_SUBTYPE: integer; const EE_BODY: WideString; bNoHayRegistros: Boolean = FALSE; sError: String = VACIO; iUsuario: Integer = 0): OleVariant;
const K_GRABA_CORREO = 'INSERT INTO ENVIOEMAIL ' +
                     '(EE_TO, EE_SUBJECT, EE_BODY, EE_FROM_NA, EE_FROM_AD, EE_ERR_AD, EE_MOD_ADJ, EE_FEC_IN, EE_ATTACH, EE_BORRAR, EE_ENCRIPT, EE_SUBTYPE) ' +
                     ' VALUES ' +
                     '(%s, %s, %s, %s, %s, %s, ''S'', %s, %s, %s, %s, 1 ) ';

     K_SEPARADOR = '_';
     K_EE_SUBJECT = 'Reporte enviado: ';
     K_EE_BODY = 'Env�o programado: %s. ' + CR_LF +
                 'Reporte enviado: %s.';
     K_FORMATO_FECHA = 'mm/dd/yyyy hh:nn:ss.zzz';
var
   sScriptCorreo: String;
   iIndex: Integer;

   sEE_BODY: String;
   sAttach: String;
   sAttachCampo: String;
   sEE_ENCRIPT: String;
   sEE_BORRAR: String;
   FQuery : TZetaCursor;
begin
     // Determinar si el correo se graba encriptado.
     // Depende del subtipo de correo.
     // Siempre encriptar.
     sEE_ENCRIPT := K_GLOBAL_SI;

     sEE_BORRAR := K_GLOBAL_NO;

     // Grabar registros en tabla ENVIOEMAIL para los reportes generados para la tarea
     // indicada por medio de CA_FOLIO.

     // if EE_TO = VACIO then
     // Correcci�n de bug: #20334 AGNICO EAGLE - Cuando un usuarios suscrito al envio programado no tiene cuenta de correo electronico se comienzan a duplicar reportes a enviar, ocasionando se traten de enviar m�s correos de los esperados.
     // if (EE_TO = VACIO) AND (FReporteEmpleados = 0) then
     if (EE_TO = VACIO) AND (FReporteEmpleados = 0) AND (FReporteSupervisor = FALSE) then
     // ----- -----
     begin
          // MECANISMO NORMAL.
          // Buscar los usuarios y roles para esta tarea as� como los reportes generados.
          // De los usuarios obtener los correos electr�nicos y para cada uno
          // generar un registro en ENVIOEMAIL.

          // Indicar que se debe modificar el archivo.

          with oZetaProvider do
          begin
               FQuery := CreateQuery(Format (K_LEE_CUENTAS, [CA_FOLIO, CA_FOLIO]));
          end;
          FQuery.Active := TRUE;

          // sAttach.
          if (SysUtils.ExtractFileName(EE_ATTACH).LastDelimiter (K_SEPARADOR) > 0) and (not bNoHayRegistros)then
          begin
               sAttach := Copy (SysUtils.ExtractFileName(EE_ATTACH), 0, SysUtils.ExtractFileName(EE_ATTACH).LastDelimiter (K_SEPARADOR) ) + SysUtils.ExtractFileExt (EE_ATTACH);
               sAttach := TPath.GetFileNameWithoutExtension (sAttach);
          end
          else if bNoHayRegistros then
          begin
               sAttach := Copy (SysUtils.ExtractFileName(DatosImpresion.Exportacion), 0, SysUtils.ExtractFileName(DatosImpresion.Exportacion).LastDelimiter (K_SEPARADOR) ) + SysUtils.ExtractFileExt (DatosImpresion.Exportacion);
               sAttach := TPath.GetFileNameWithoutExtension (sAttach);
          end;

          if EE_SUBTYPE > 0 then
          begin
               sAttachCampo := VACIO;
          end
          else
              sAttachCampo := EE_ATTACH;

          if EE_BODY = VACIO then
          begin
               if sError = VACIO then
                  sEE_BODY := GeneraMensajeHTML(sAttach, ReemplazaVariablesCorreo)
               else
                  sEE_BODY := GeneraMensajeHTMLError(sAttach, sError, FNombreCalendario, ReemplazaVariablesCorreoError);
          end
          else
          begin
               sEE_BODY := EE_BODY;
          end;

          if sEE_ENCRIPT = K_GLOBAL_SI then
             sEE_BODY := Encrypt (sEE_BODY);

          try
             with FQuery do
             begin
                  iIndex := 0;

                  if FQuery.RecordCount <> 0 then
                  begin
                       while not EOF do
                       begin
                            try
                               // DES #15716: Servicio de reportes - Notificaci�n si no hay datos
                               if (FQuery.FieldByName('VACIO').AsInteger = 0) OR (bNoHayRegistros = FALSE) then
                               begin
                                    if iIndex = FQuery.RecordCount -1  then
                                       sEE_BORRAR := K_GLOBAL_SI;

                                    if FieldByName ('US_EMAIL').AsString <> VACIO then
                                    begin
                                         sScriptCorreo := Format (K_GRABA_CORREO, [EntreComillas (FieldByName ('US_EMAIL').AsString),
                                                     EntreComillas (K_EE_SUBJECT + ExtractFileName (sAttach)),
                                                     EntreComillas (sEE_BODY),
                                                     EntreComillas (EE_FROM_NA), EntreComillas (EE_FROM_AD), EntreComillas (EE_ERR_AD), EntreComillas (FormatDateTime (K_FORMATO_FECHA, Now )), EntreComillas (sAttachCampo), EntreComillas (sEE_BORRAR), EntreComillas (sEE_ENCRIPT)]);

                                         // Grabar solicitud de correo en tabla COMPARTE.ENVIOEMAIL.
                                         oZetaProvider.EjecutaAndFree (sScriptCorreo);
                                    end
                                    else
                                    begin
                                         GrabaBitacoraServicio(Format ('Usuario %d sin correo electr�nico', [FieldByName ('US_CODIGO').AsInteger]), Format ('Usuario %d no tiene correo electr�nico configurado.', [FieldByName ('US_CODIGO').AsInteger]), tbAdvertencia);
                                    end;

                                    // Correcci�n de bug: #20334 AGNICO EAGLE - Cuando un usuarios suscrito al envio programado no tiene cuenta de correo electronico se comienzan a duplicar reportes a enviar, ocasionando se traten de enviar m�s correos de los esperados f.
                            		    iIndex := iIndex + 1;
                                    // ----- -----
                               end;

                               // Correcci�n de bug: #20334 AGNICO EAGLE - Cuando un usuarios suscrito al envio programado no tiene cuenta de correo electronico se comienzan a duplicar reportes a enviar, ocasionando se traten de enviar m�s correos de los esperados f.
                               // iIndex := iIndex + 1;
                               // ----- -----

                            except
                                  on Error: Exception do
                                  begin
                                       LogError(EntreComillas ('No es posible generar solicitud de env�o de correo.'), Error.Message, tbErrorGrave);
                                  end;
                            end;

                            Next;
                       end;
                  end
                  else
                  begin
                       GrabaBitacoraServicio('No existe suscripci�n', 'No se encontraron suscripciones para enviar reporte por correo', tbAdvertencia);
                  end;
             end;

             FQuery.Active := FALSE;

          finally
                 FreeAndNil (FQuery);
          end;
     end

     // Correcci�n de bug: #20334 AGNICO EAGLE - Cuando un usuarios suscrito al envio programado no tiene cuenta de correo electronico se comienzan a duplicar reportes a enviar, ocasionando se traten de enviar m�s correos de los esperados.
     else if (EE_TO = VACIO) AND (FReporteSupervisor) AND (bNoHayRegistros = FALSE)  then
     begin
          GrabaBitacoraServicio(Format ('Usuario %d sin correo electr�nico', [iUsuario]),
            Format ('Usuario supervisor %d no tiene correo electr�nico configurado.', [iUsuario]), tbAdvertencia);
     end
     // ----- -----

     else if EE_TO <> VACIO then
     begin
          // MECANISMO DE LISTA DE EMPLEADOS
          // O CUALQUIER OTRO QUE INDIQUE DIRECTAMENTE
          // A QU� CORREO ENVIAR EL REPORTE.
          // sAttach.

          sEE_BORRAR := K_GLOBAL_SI;

          if (SysUtils.ExtractFileName(EE_ATTACH).LastDelimiter (K_SEPARADOR) > 0) then
          begin
               sAttach := Copy (SysUtils.ExtractFileName(EE_ATTACH), 0, SysUtils.ExtractFileName(EE_ATTACH).LastDelimiter (K_SEPARADOR) ) + SysUtils.ExtractFileExt (EE_ATTACH);
               sAttach := TPath.GetFileNameWithoutExtension (sAttach);
          end;

          if EE_SUBTYPE > 0 then
          begin
               sAttachCampo := VACIO;
          end
          else
              sAttachCampo := EE_ATTACH;

          if EE_BODY = VACIO then
          begin
               if sError = VACIO then
                  sEE_BODY := GeneraMensajeHTML(sAttach, ReemplazaVariablesCorreo)
               else
                  sEE_BODY := GeneraMensajeHTMLError(sAttach, sError, FNombreCalendario, ReemplazaVariablesCorreoError)
          end
          else
          begin
               sEE_BODY := EE_BODY;
          end;

          try
             if sEE_ENCRIPT = K_GLOBAL_SI then
                sEE_BODY := Encrypt (sEE_BODY);

             sScriptCorreo := Format (K_GRABA_CORREO, [EntreComillas (EE_TO),
                         EntreComillas (K_EE_SUBJECT + ExtractFileName (sAttach)),
                         EntreComillas (sEE_BODY),
                         EntreComillas (EE_FROM_NA), EntreComillas (EE_FROM_AD), EntreComillas (EE_ERR_AD), EntreComillas (FormatDateTime (K_FORMATO_FECHA, Now )), EntreComillas (sAttachCampo), EntreComillas (sEE_BORRAR), EntreComillas (sEE_ENCRIPT)]);

             with oZetaProvider do
             begin
                  // Grabar solicitud de correo en tabla COMPARTE.ENVIOEMAIL.
                  EjecutaAndFree (sScriptCorreo);
             end;
          except
                on Error: Exception do
                begin
                     LogError('No es posible generar solicitud de env�o de correo.', Error.Message, tbErrorGrave);
                end;
          end;
     end;
end;

procedure TdmReportes.AgregarSolicitudesCalensoli();
const
     K_ESPECIAL = 4;
var
   FTareasCalendario: TZetaCursor;
   sPruebaArchivo: String;
begin
     try
        {$ifdef PROFILE}
        emailProfiler.Clear;
        emailProfiler.Check('Agregar_Solicitudes_Calensoli Inicio');
        emailProfiler.Check('Lee Calendario.');
        {$endif}
        FTareasCalendario := oZetaProvider.CreateQuery(Format (K_LEE_CALENDARIO, [ord(tcSEProcesando)]));

        {$ifdef PROFILE}
        emailProfiler.Check('Se prepar� consulta.');
        {$endif}

        FTareasCalendario.Active := TRUE;
        {$ifdef PROFILE}
        emailProfiler.Check('Activando consulta.');
        {$endif}
        try
           {$ifdef PROFILE}
           emailProfiler.Check(Format ('---> 1 REGISTROS  DE CALENDARIO A REVISAR: %d', [FTareasCalendario.RecordCount]));
           {$endif}
           try
              while not FTareasCalendario.Eof do
              begin
                   {$ifdef PROFILE}
                   emailProfiler.Check(Format('Leyendo tarea Folio: %d.',
                                                       [FTareasCalendario.FieldByName ('CA_FOLIO').AsInteger,
                                                       FTareasCalendario.FieldByName ('CA_NOMBRE').AsString]));
                   {$endif}
                   if (FTareasCalendario.FieldByName ('CA_NX_FEC').AsDateTime <= Now)
                      and (FTareasCalendario.FieldByName ('CA_ACTIVO').AsString = K_GLOBAL_SI) then
                   begin
                        {$ifdef PROFILE}
                        emailProfiler.Check('---> 2 Tarea le�da ha pasado su fecha de siguiente ejecuci�n. Preparar solicitud en tabla Calensoli.');
                        {$endif}

                        // Si es momento de ejecutar una tarea, entonces, hacer solicitud sobre la tabla
                        // CALENSOLI.
                        // AgregarSolicitudEnvioProgramado.
                        // Si no es tarea especial, grabar solicitud.
                        if FTareasCalendario.FieldByName ('CA_FREC').AsInteger <> K_ESPECIAL then
                        begin
                             {$ifdef PROFILE}emailProfiler.Check('---> 3 Solicitando inserci�n de solicitud de env�o programado.');{$endif}
                             AgregarSolicitudEnvioProgramado (FTareasCalendario.FieldByName ('CA_FOLIO').AsInteger,
                                                        StrToInt (FTareasCalendario.FieldByName ('CA_REPORT').AsString));
                        end
                        else
                        begin
                             {$ifdef PROFILE}emailProfiler.Check('--->3 (b) Tarea es especial. Grabar solicitud si no se ha atendido.');{$endif}
                             // Si es tarea especial, grabar solicitud solo si la fecha de �ltima ejecuci�n es menor a la fecha de siguiente ejecuci�n.
                             if FTareasCalendario.FieldByName ('CA_ULT_FEC').AsDateTime < FTareasCalendario.FieldByName ('CA_NX_FEC').AsDateTime then
                             begin
                                  AgregarSolicitudEnvioProgramado (FTareasCalendario.FieldByName ('CA_FOLIO').AsInteger,
                                                        StrToInt (FTareasCalendario.FieldByName ('CA_REPORT').AsString));
                             end;
                        end;
                   end;

                   FTareasCalendario.Next;
              end;

           except
                 on ErrorWhile: Exception do
                 begin
                       {$ifdef PROFILE}emailProfiler.Check(Format ('** ERROR al leer tareas y/o intentar grabar solicitud de env�o en Calensoli. Error: %s',
                               [ErrorWhile.Message]));{$endif}
                       LogError('No es posible agregar solicitud de env�o programado.', ErrorWhile.Message, tbError);
                       if Assigned (FTareasCalendario) then
                          FreeAndNil (FTareasCalendario);
                 end;
           end;

           FTareasCalendario.Active := FALSE;
        finally
               FreeAndNil (FTareasCalendario);
        end;
     except
           on Error: Exception do
           begin
                {$ifdef PROFILE}emailProfiler.Check(Format ('** ERROR AgregarSolicitudesCalensoli. Error: %s', [Error.Message]));{$endif}
                LogError('No es posible agregar solicitud de env�o programado.', Error.Message, tbError);
                if Assigned (FTareasCalendario) then
                   FreeAndNil (FTareasCalendario);
           end;
     end;


    {$ifdef PROFILE}emailProfiler.Check('Agregar_Solicitudes_Calensoli Fin');{$endif}
    {$ifdef PROFILE}
    DateTimeToString(sPruebaArchivo, 'yyyymmddhhmmss', Now);
    dmCliente.GetServicioReportesCorreos;
    emailProfiler.Report( Format(dmCliente.DatosServicioReportesCorreos.DirectorioReportes +
                          '\ReportesEmail_Profile_%s.LOG', [sPruebaArchivo]) );
    {$endif}

end;

procedure TdmReportes.AgregarSolicitudEnvioProgramado(Folio, Reporte: Integer);
var
   FInsertaSolicitudEnvio: TZetaCursor;
begin
     {$ifdef PROFILE}emailProfiler.Check('****** 1 AgregarSolicitudEnvioProgramado ****');{$endif}
     {$ifdef PROFILE}emailProfiler.Check('****** 2 Preparando query de inserci�n ****');{$endif}

     FInsertaSolicitudEnvio := oZetaProvider.CreateQuery (K_INSERTA_CALENSOLI);
     try
        with oZetaProvider do
        begin
             EmpiezaTransaccion;
             try
                {$ifdef PROFILE}emailProfiler.Check('****** 3 Preparando par�metros ****');{$endif}

                ParamAsInteger (FInsertaSolicitudEnvio, 'CA_FOLIO', Folio );
                ParamAsInteger (FInsertaSolicitudEnvio, 'RE_CODIGO', Reporte );
                ParamAsInteger (FInsertaSolicitudEnvio, 'CAL_ESTATUS', ord(tcSEPendiente) );
                ParamAsDateTime( FInsertaSolicitudEnvio, 'CAL_FECHA', Now );
                ParamAsString (FInsertaSolicitudEnvio, 'CAL_MENSAJ', VACIO );

                {$ifdef PROFILE}emailProfiler.Check('****** 4 Par�metros listos. Ejecutar inserci�n de registro en Calensoli ****');{$endif}

                Ejecuta (FInsertaSolicitudEnvio );

                {$ifdef PROFILE}emailProfiler.Check('****** 5 Termina transacci�n ****');{$endif}

                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        {$ifdef PROFILE}emailProfiler.Check(Format ('******* ERROR al intentar grabar en Calensoli. Error: %s *****',
                                [Error.Message]));{$endif}

                        TerminaTransaccion( FALSE );
                        LogError ('Error al agregar solicitud de env�o programado.', Error.Message, tbError );

                        if Assigned (FInsertaSolicitudEnvio) then
                           FreeAndNil(FInsertaSolicitudEnvio);
                   end;
             end;
        end;
     finally
            FreeAndNil(FInsertaSolicitudEnvio);
     end;
end;

function TdmReportes.GetSuscripciones: Boolean;
begin
     try
        cdsSuscripcion.Data := oZetaProvider.OpenSQL (oZetaProvider.EmpresaActiva, Format (K_LEE_CUENTAS, [FFolioCalendario, FFolioCalendario]), TRUE);
        Result := not cdsSuscripcion.IsEmpty;
     except
           on Error: Exception do
           begin
                LogError ('Error al obtener suscripciones.', Error.Message, tbError );
           end;
     end;
end;

end.
