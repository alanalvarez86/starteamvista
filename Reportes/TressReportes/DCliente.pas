unit DCliente;

interface

{$DEFINE REPORTING}
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
     ZetaClientDataSet,
     DBasicoCliente,
     ZetaCommonLists,
     ZetaCommonClasses;

type
  TdmCliente = class(TBasicoCliente)
    cdsPeriodo: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    FEmpleado: integer;
    FPeriodoActivo :TDatosPeriodo;
    FGeneraListaEmpleados:Boolean;
    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;
    {$ifdef TRESS}
    function GetPeriodoInicial(Parametros:TZetaParams): Boolean;
    procedure CargaActivosIMSS( Parametros: TZetaParams );
    procedure CargaActivosPeriodo( Parametros: TZetaParams );
    procedure CargaActivosPeriodoLista(Parametros: TZetaParams);
    {$endif}
    procedure CargaActivosSistema( Parametros: TZetaParams );
  protected
  public
    { Public declarations }
    property Parametros: TZetaParams read FParametros;
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    property Empleado: integer read FEmpleado write FEmpleado default 0;
    property PeriodoActivo: TDatosPeriodo read FPeriodoActivo  write FPeriodoActivo;
    property GeneraListaEmpleados: Boolean read FGeneraListaEmpleados  write FGeneraListaEmpleados default false;

    function BuscaCompany( const Codigo: String ): Boolean;
    function Empresa: OleVariant;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; override;
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
  end;

// {$ifdef XMLREPORT}
// threadvar
// {$else}
var
// {$endif}
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses ZetaRegistryCliente,
     ZetaCommonTools;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FParametros := TZetaParams.Create( Self );

     {$ifdef TRESS}
     TipoCompany := tc3Datos;
     {$endif}
    {$ifdef VISITANTES}
     TipoCompany := tcVisitas;
     {$endif}

end;

{$ifdef TRESS}
function TdmCliente.GetPeriodoInicial(Parametros:TZetaParams): Boolean;
 function GetParametro( const sName:string; const iValor: integer ):integer;
 begin
      if ( Parametros.FindParam(sName) <> NIL) then
      begin
           Result := Parametros.ParamByName(sName).AsInteger;
           if ( Result = -1 ) then
              Result := iValor;
      end
      else
          Result := iValor;
 end;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, GetParametro( 'YEAR', TheYear(Date) ), GetParametro( 'TIPO', ClientRegistry.TipoNomina )  );
          Result := not IsEmpty;
     end;
end;
{$endif}

function TdmCliente.BuscaCompany( const Codigo: String ): Boolean;
var
   ocdsDataSet: TZetaClientDataSet;
begin
{
     ocdsDataSet := TZetaClientDataSet.Create( self );
     ocdsDataSet.Data := Servidor.GetCompanys( 0, Ord(TipoCompany) );
     ocdsDataSet.Free;
     Result := FALSE;
}
     cdsCompany.Data := Servidor.GetCompanys( 0, Ord(TipoCompany) );

     Result := cdsCompany.Locate( 'CM_CODIGO', Codigo, [] );
     if Result then
     begin
          ClientRegistry.Compania := Codigo;
     end;
end;

function TdmCliente.Empresa: OleVariant; {Creo que este metodo repite logica que ya se esta haciendo en DBaseCliente}
begin
     Result := BuildEmpresa( cdsCompany );
end;

procedure TdmCliente.CargaActivosSistema;
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          if GeneraListaEmpleados then
             AddInteger( 'EmpleadoActivo', FEmpleado )
          else
              AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', 'REP_EMAIL' );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosTodos( Parametros: TZetaParams );
begin
     {$ifdef TRESS}
     CargaActivosIMSS( Parametros );
     if FGeneraListaEmpleados then
       CargaActivosPeriodoLista (Parametros)
     else
     	 CargaActivosPeriodo( Parametros );
     {$endif}
     CargaActivosSistema( Parametros );
end;

{$ifdef TRESS}
procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', ImssPatron );
          AddInteger( 'IMSSYear', ImssYear );
          AddInteger( 'IMSSMes', ImssMes );
          AddInteger( 'IMSSTipo', Ord( ImssTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     with cdsPeriodo do
     begin
          if IsEmpty then
             GetPeriodoInicial(Parametros);
          with Parametros do
          begin
               AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
               AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
               AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
          end;
     end;
end;

procedure TdmCliente.CargaActivosPeriodoLista(Parametros: TZetaParams);
begin
     with cdsPeriodo do
     begin
          if IsEmpty then
             GetPeriodoInicial(Parametros);
          with Parametros do
          begin
               AddInteger( 'Year', FPeriodoActivo.Year );
               AddInteger( 'Tipo', Ord(FPeriodoActivo.Tipo ));
               AddInteger( 'Numero', FPeriodoActivo.Numero );
          end;
     end;
end;
{$endif}

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     with Result do
     begin
          with cdsPeriodo do
          begin
               if FGeneraListaEmpleados then
               begin
                    Year := FPeriodoActivo.Year;
               		Tipo := FPeriodoActivo.Tipo;
                    Numero := FPeriodoActivo.Numero;  
               end
               else
               begin
               	    Year := FieldByName( 'PE_YEAR' ).AsInteger;
                    Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
                    Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               end;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;


function TdmCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := TheMonth(Date);
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := '';
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := tlOrdinaria;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := TheYear( Date );
end;

function TdmCliente.GetValorActivo( const eValor: {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin

     {case eValor of
          vaImssPatron: Result := Comillas( IMSSPatron );
          vaImssMes: Result := IntToStr( IMSSMes );
          vaImssTipo: Result := IntToStr( Ord( IMSSTipo ) );
          vaImssYear: Result := IntToStr( IMSSYear );
     else
         Result := inherited GetValorActivo( eValor )
     end;}

     with dmCliente do
     begin
          case eValor  of
               vaEmpleado: Result := IntToStr( Empleado );
               vaNumeroNomina: Result := IntToStr( GetDatosPeriodoActivo.Numero );
               vaTipoNomina: Result := IntToStr( Ord( GetDatosPeriodoActivo.Tipo ) );
               vaYearNomina: Result := IntToStr( GetDatosPeriodoActivo.Year )
          else
              Result := VACIO;
          end;
     end;
end;

end.
