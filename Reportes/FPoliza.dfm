inherited Poliza: TPoliza
  Left = 489
  Top = 152
  Caption = 'Poliza'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    object btnCreaPoliza: TSpeedButton
      Left = 472
      Top = 3
      Width = 135
      Height = 24
      Caption = 'Configurar Cuentas'
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00000F00FFFFFFFF00000000F0FFFFFF0FFFFF
        FFF0FFFFFF0FFFFFFFF0FFFFFF0FFFFFFFF0FFFFFF00CCCCC0F0F000000FC000
        0000F000000F00000000F000000FEE3EEE0FFFFFFFFFEEE3EE0FFFF00FFFEEEE
        3E0FFF0000FFEE3EEE0FFF0000FFEEE3EE0FFFF00FFFEEEEEE0F}
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = btnCreaPolizaClick
    end
  end
  inherited PageControl: TPageControl
    inherited tsCuentas: TTabSheet
      inherited Panel2: TPanel
        object Label7: TLabel
          Left = 344
          Top = 1
          Width = 147
          Height = 28
          Align = alRight
          AutoSize = False
          Caption = 'Total de Cuentas:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbNumCuentas: TLabel
          Left = 491
          Top = 1
          Width = 107
          Height = 28
          Align = alRight
          Alignment = taRightJustify
          AutoSize = False
          Caption = '0,000,000'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
      inherited Grid: TZetaDBGrid
        Top = 97
        Height = 234
      end
      object Panel3: TPanel
        Left = 0
        Top = 30
        Width = 604
        Height = 67
        Align = alTop
        TabOrder = 3
        object GroupBox3: TGroupBox
          Left = 23
          Top = 3
          Width = 457
          Height = 60
          Caption = 'Filtros'
          TabOrder = 0
          object Label1: TLabel
            Left = 9
            Top = 15
            Width = 68
            Height = 13
            Caption = 'Que la Cuenta'
          end
          object Label4: TLabel
            Left = 6
            Top = 38
            Width = 71
            Height = 13
            Caption = 'Que la F'#243'rmula'
          end
          object CBCuentas: TCheckBox
            Left = 406
            Top = 13
            Width = 46
            Height = 17
            Caption = 'Filtrar'
            TabOrder = 2
            OnClick = CBCuentasClick
          end
          object CBBCuentas: TComboBox
            Left = 80
            Top = 11
            Width = 120
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnChange = CBBCuentasChange
            Items.Strings = (
              'Contenga:'
              'NO Contenga:'
              'Empieze Con:'
              'NO Empieze Con:'
              'Termine En:'
              'NO Termine En:'
              'Sea Igual A:'
              'Sea Diferente A:'
              'Sea Mayor Que'
              'Sea Mayor e Igual A:'
              'Sea Menor Que'
              'Sea Menor e Igual A:'
              'Est'#233' Vac'#237'a'
              'NO Est'#233' Vac'#237'a')
          end
          object ECuentas: TEdit
            Left = 200
            Top = 11
            Width = 200
            Height = 21
            TabOrder = 1
            OnChange = ECuentasChange
          end
          object EFormula: TEdit
            Left = 200
            Top = 34
            Width = 200
            Height = 21
            TabOrder = 4
            OnChange = EFormulaChange
          end
          object CBBFormula: TComboBox
            Left = 80
            Top = 34
            Width = 120
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 3
            OnChange = CBBFormulaChange
            Items.Strings = (
              'Contenga:'
              'NO Contenga:'
              'Empieze Con:'
              'NO Empieze Con:'
              'Termine En:'
              'NO Termine En:'
              'Sea Igual A:'
              'Sea Diferente A:'
              'Sea Mayor Que'
              'Sea Mayor e Igual A:'
              'Sea Menor Que'
              'Sea Menor e Igual A:'
              'Est'#233' Vac'#237'a'
              'NO Est'#233' Vac'#237'a')
          end
          object CBFormula: TCheckBox
            Left = 406
            Top = 36
            Width = 46
            Height = 17
            Caption = 'Filtrar'
            TabOrder = 5
            OnClick = CBFormulaClick
          end
        end
        object rgTipoCuenta: TRadioGroup
          Left = 484
          Top = 3
          Width = 97
          Height = 60
          Caption = 'Tipo de Cuenta'
          Items.Strings = (
            'Todos'
            'Cargo'
            'Abono')
          TabOrder = 1
          OnClick = rgTipoCuentaClick
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 336
    Top = 0
  end
  inherited dsCuentas: TDataSource
    Left = 368
    Top = 0
  end
end
