unit DExcelAppender;

interface

uses
  SysUtils, Classes, Excel2000, OleServer, DB, DBClient, OleCtrls, ZetaClientDataSet,
   Variants, ZetaClientTools, ExcelXP, FOLEMessageFilter, Windows, ComObj, ActiveX,
   ZetaCommonTools,ZetaWinAPITools,StdCtrls,Messages, Controls, FileCtrl, Dialogs,
   ExtCtrls, ZetaDialogo,ZetaCommonClasses,Forms, Graphics;

type
  TExcelAppender = class(TDataModule)
    ExcelApp: TExcelApplication;
    ExcelSheet: TExcelWorksheet;
    ExcelWorkbook1: TExcelWorkbook;
    TimerExcel: TTimer;
    procedure TimerExcelTimer(Sender: TObject);
  private
    { Private declarations }
      FFileName : string;
      nSheets : integer;
      bOk : boolean ;
  protected
      function SetHojaEncabezado(oListaCampos: TStrings; const iRow, iCol, iMaximo: Integer; const lFormatCelda: Boolean ): Boolean; virtual;
      procedure AgregaSheets; virtual;
      {procedure InsertFormatoCelda( iRenglon, iColumna: Integer ); virtual;}
  public
    { Public declarations }
      FErrorMessage : string;
      function  GetStatus : boolean;
      function  GetErrorMessage : string;
      function  AbreArchivoBase( sFileName : string ) : boolean;
      function  VaciaDSWorkSheet( cdsDataSet : TClientDataSet; sSheet : string; iRow, iCol : integer; oListaCampos: TStrings; const lFormatCelda: Boolean = FALSE ) : boolean;
      procedure HideActiveSheet;
      procedure ShowExcelApp;
      procedure RefrescarWorkBook;
  end;

var
  ExcelAppender: TExcelAppender;

const
     K_MAX_RENGLONES = 65536;  //Max. n�mero de renglones que acepta excel
     K_MAX_COLUMNAS = 256;   //Max. n�mero de columnas que acepta excel


implementation


{$R *.dfm}

{ TExcelAppender }


function TExcelAppender.AbreArchivoBase(sFileName: string) : boolean;

procedure ProcesaMensajes;
begin
     Application.ProcessMessages;
end;

const
     K_APLICACION_EXCEL = 'Microsoft Excel';
var
   kSheet : integer;
begin
     FFileName :=  sFileName;
     bOk := FileExists( FFileName );
     if not bOk then
        FErrorMessage:= Format('El Documento %s No Ha Sido Encontrado',[FFileName])
     else
         FErrorMessage := '';

     with ExcelApp do
     begin
          try
             ConnectKind := ckRunningOrNew;       // Para que no interfiera con otros Excel's abiertos
             Connect;
             if ( Visible [0] ) and
                ( ActiveCell <> Nil  ) and
                ( FindWindow( nil, K_APLICACION_EXCEL ) <> 0 ) then
             begin
                  PostMessage(Hwnd,WM_KEYDOWN,VK_ESCAPE,0);   //En caso de cambios env�a 2 ESC
                  ProcesaMensajes;
                  PostMessage(Hwnd,WM_KEYDOWN,VK_ESCAPE,0);
                  ProcesaMensajes;
             end;
             Visible[ 0 ] := False; // Se conecta, pero invisible
          except
                on Error: Exception do
                begin
                     if ( Pos( 'OLE', Error.Message ) <= 0 ) then
                        FErrorMessage := Error.Message
                     else
                         FErrorMessage:= 'Favor de cerrar cualquier dialogo de excel antes de exportar registros';

                     bOk := False;
                end;
          end;

        if ( bOk ) then
        begin
             try
                WorkBooks._Open(sFilename,EmptyParam,FALSE,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,EmptyParam,FALSE,EmptyParam,EmptyParam,0); //se cambio para que fuera compatible con office 2000
                //WorkBooks.Open( sFileName, EmptyParam, FALSE, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, 0 );
                bOk := True;

             except
                   on Error: Exception do
                   begin
                        FErrorMessage := 'Error al abrir archivo Excel';
                        Visible[ 0 ] := True;
                        Quit;
                        bOk := False;
                   end;
             end;
        end;


        if ( bOk ) then
        begin
             AgregaSheets;
             nSheets := Sheets.Count;
             for kSheet := 1 to nSheets do
             begin
                  try
                     ExcelSheet.ConnectTo( ExcelApp.Sheets.Item[ kSheet ] as _Worksheet );
                     ExcelSheet.Activate;
                  except
                        on Error: Exception do
                        begin
                             FErrorMessage := Error.Message;
                             ExcelApp.Visible[ 0 ] := True;
                             ExcelApp.Quit;
                             bOk := False;
                        end;
                  end;
             end;
        end;

      end;
      Result := bOk;
end;

function TExcelAppender.GetErrorMessage: string;
begin
     Result := FErrorMessage;
end;

function TExcelAppender.GetStatus: boolean;
begin
     Result := bOk;
end;

procedure TExcelAppender.HideActiveSheet;
begin
     if ( bOk ) then
        ExcelSheet.Visible[1] := 0;

     try
        ExcelSheet.ConnectTo( ExcelApp.Sheets.Item[ 1 ] as _Worksheet );
        ExcelSheet.Activate;
     except
         on Error: Exception do
         begin
              FErrorMessage := Error.Message;
              bOk := False;
         end;
     end;
end;


procedure TExcelAppender.RefrescarWorkBook;
begin
     ExcelApp.ActiveWorkbook.RefreshAll;
end;

procedure TExcelAppender.ShowExcelApp;
begin
     if ( bOk ) then
        ExcelApp.Visible[ 0 ] := TRUE;
end;

function TExcelAppender.SetHojaEncabezado(oListaCampos: TStrings; const iRow, iCol, iMaximo: Integer; const lFormatCelda: Boolean ): Boolean;
var
   iField: Integer;
begin
     Result:= True;
     for iField := 0 to iMin( K_MAX_COLUMNAS, oListaCampos.Count - 1 )  do
     begin
          try
             ExcelSheet.Cells.Item[ iRow, iCol + iField ].Value := oListaCampos[iField] ;
          except
                on Error: Exception do
                begin
                     FErrorMessage := Error.Message;
                     Result := False;
                end;
          end;
     end;
end;


function TExcelAppender.VaciaDSWorkSheet(cdsDataSet: TClientDataSet; sSheet : string;
  iRow, iCol: integer; oListaCampos: TStrings; const lFormatCelda: Boolean ) : boolean;
var
   iReg, iField: integer;
   miWorkSheet : _Worksheet;
   lInserta: Boolean;
begin
     lInserta:= TRUE;
     if bOk then
     begin
          try
             miWorkSheet := ExcelApp.Sheets.Item[ sSheet ] as _Worksheet;
          except
                on Error: Exception do
                begin
                     FErrorMessage := Format( 'La hoja "%s" no existe', [sSheet] );
                     bOk := False;
                end;
          end;
          if bOk then
          begin
               try
                  ExcelSheet.ConnectTo( miWorkSheet );
                  ExcelSheet.Activate;
                  miWorkSheet.Cells.ClearContents;
               except
                     on Error: Exception do
                     begin
                          FErrorMessage := Error.Message;
                          bOk := False;
                     end;
               end;
          end;
          with cdsDataSet do
          begin
               if ( Active ) and ( bOk ) then
               begin
                    if ( ( RecordCount < K_MAX_RENGLONES ) or
                       ( ZetaDialogo.ZWarningConfirm('Mail Merge - Microsoft Excel', 'El numero de registros excede el l�mite de renglones, �Desea continuar con la exportaci�n?',0, mbCancel ) )  ) and
                       ( ( oListaCampos.Count < K_MAX_COLUMNAS ) or
                       ( ZetaDialogo.ZWarningConfirm('Mail Merge - Microsoft Excel', 'El numero de columnas excede el l�mite, �Desea continuar con la exportaci�n?',0, mbCancel ) ) ) then
                    begin
                         if not IsEmpty then
                         begin
                              MessageFilter.RegisterFilter;
                              try
                                 DisableControls;
                                 First;
                                 iReg := 1;
                                 bOk:= SetHojaEncabezado( oListaCampos, iRow, iCol, RecordCount, lFormatCelda  );
                                 { Se cambi� al evento  SetHojaEncabezado
                                 for iField := 0 to iMin( K_MAX_COLUMNAS, oListaCampos.Count - 1 )  do
                                 begin
                                      try
                                         ExcelSheet.Cells.Item[ iRow, iCol + iField ].Value := oListaCampos[iField] ;
                                      except
                                            on Error: Exception do
                                            begin
                                                 FErrorMessage := Error.Message;
                                                 bOk := False;
                                            end;
                                      end;
                                 end;}
                                 while ( not Eof ) and ( bOk ) and ( iReg <= K_MAX_RENGLONES ) do
                                 begin
                                      for iField := 0 to iMin( K_MAX_COLUMNAS, oListaCampos.Count - 1 )  do
                                      begin
                                           try
                                              lInserta:= ( ExcelApp.ActiveCell <> NIL );
                                              if lInserta then
                                              begin
                                                   TimerExcel.Enabled:= False;
                                                   {***(@am):Anteriormente se le pasaba un resultado tipo variant a la propiedad value de cada celda,
                                                             pues en D7 el componente de excel funcionaba igual tanto con variant y OLEVariant.
                                                             En Delphi XE5 es necesario realizar esta distincion, por lo tanto se agrego el metodo
                                                             ValorCampoOLE a ZetaClientTools para devolver un OLEVariant en lugar de un Variant.***}
                                                   ExcelSheet.Cells.Item[ iRow + iReg, iCol + iField ].Value := ValorCampoOLE ( Fields[iField] ) ;
                                                   //ExcelSheet.Cells.Item[ iRow + iReg, iCol + iField ].Value := ValorCampo ( Fields[iField] ) ; //OLD
                                                   {if lFormatCelda then
                                                      InsertFormatoCelda( iRow + iReg, iCol + iField );}
                                              end
                                              else
                                                  TimerExcel.Enabled:= True;
                                           except
                                                 on Error: Exception do
                                                 begin
                                                      if ( Pos( 'OLE', Error.Message ) <= 0 ) then  //Si llega  un error OLE, es porque se interrumpi� el proceso.
                                                         FErrorMessage := Error.Message
                                                      else
                                                          FErrorMessage:= 'El proceso de importaci�n fue interrumpido';
                                                      bOk := False;
                                                 end;
                                           end;
                                      end;
                                      if lInserta then
                                      begin
                                           Inc(iReg);
                                           Next;
                                      end;
                                 end;
                              finally
                                     MessageFilter.RevokeFilter;
                              end;
                         end;
                    end;
               end;
          end;
     end;
     Result := bOk;
end;



procedure TExcelAppender.TimerExcelTimer(Sender: TObject);
begin
     Raise Exception.Create( 'El proceso de importaci�n no se pudo completar' );
end;



procedure TExcelAppender.AgregaSheets;
begin
     { No hace nada }
end;

{procedure TExcelAppender.InsertFormatoCelda( iRenglon, iColumna: Integer );
begin
     { No hace nada

end;    }

end.
