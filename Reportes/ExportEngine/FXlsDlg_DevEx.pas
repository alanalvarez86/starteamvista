unit FXlsDlg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls, Mask, ZetaNumero,
  ZDocumentDlg_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxControls, cxPC;

type
  TXlsDlg_DevEx = class(TDocumentDlg_DevEx)
    GroupBox1: TGroupBox;
    zColumnSpacing: TZetaNumero;
    Label1: TLabel;
    Label2: TLabel;
    zRowSpacing: TZetaNumero;
    Label3: TLabel;
    zScaleX: TZetaNumero;
    zScaleY: TZetaNumero;
    Label4: TLabel;
    lbPagesPerWorksheet: TLabel;
    zPagesPerWorksheet: TZetaNumero;
    cbAutoFormat: TCheckBox;
    cbMargin: TCheckBox;
    cbPrintGridLines: TCheckBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure CargaArchivo; override;
    procedure GrabaArchivo; override;
  public
    { Public declarations }
  end;

var
  XlsDlg_DevEx: TXlsDlg_DevEx;

implementation
uses ZetaCommonLists;

{$R *.DFM}

{ TXlsDlg }

procedure TXlsDlg_DevEx.FormShow(Sender: TObject);
 var
    lVisible : Boolean;
begin
     inherited;
     lVisible := NOT ( Formato in [tfDIF,tfWK2,tfWB1,tfSLK] );
     lbPagesPerWorksheet.Visible := lVisible;
     zPagesPerWorksheet.Visible := lVisible;
     cbMargin.Visible := lVisible;
     cbPrintGridLines.Visible := lVisible;
end;

procedure TXlsDlg_DevEx.CargaArchivo;
begin
     inherited;
     with FIniFile do
     begin
          zColumnSpacing.Valor     := LeeEntero( 'Column Spacing', 0 );
          zRowSpacing.Valor        := LeeEntero( 'Row Spacing', -1 );
          zScaleX.Valor            := LeeEntero( 'Scale X', 1 );
          zScaleY.Valor            := LeeEntero( 'Scale Y', 1 );
          zPagesPerWorksheet.Valor := LeeEntero( 'Pages Per Worksheet', 0 );
          cbAutoFormat.Checked     := LeeLogico( 'Auto Format Cells', FALSE );
          cbMargin.Checked         := LeeLogico( 'Page Margins', FALSE );
          cbPrintGridLines.Checked := LeeLogico( 'Print Grid Lines', FALSE );
     end;
end;

procedure TXlsDlg_DevEx.GrabaArchivo;
begin
     with FIniFile do
     begin
          EscribeEntero( 'Column Spacing', zColumnSpacing.ValorEntero );
          EscribeEntero( 'Row Spacing', zRowSpacing.ValorEntero );
          EscribeEntero( 'Scale X', zScaleX.ValorEntero );
          EscribeEntero( 'Scale Y', zScaleY.ValorEntero );
          EscribeEntero( 'Pages Per Worksheet', zPagesPerWorksheet.ValorEntero );
          EscribeLogico( 'Auto Format Cells', cbAutoFormat.Checked );
          EscribeLogico( 'Page Margins', cbMargin.Checked );
          EscribeLogico( 'Print Grid Lines', cbPrintGridLines.Checked );
     end;
     inherited;

end;



end.
