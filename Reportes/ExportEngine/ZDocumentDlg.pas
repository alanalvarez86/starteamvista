unit ZDocumentDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ComCtrls, gtCstDocEng, IniFiles,
  ZetaCommonLists,
  ZetaCommonClasses;

const
     K_EXPORT_INI_FILE = '3Formatos.INI';
     K_PagesToRender = 'Pages To Render';
     K_RenderText = 'Render Text';
     K_RenderShapes = 'Render Shapes';
     K_RenderImage  = 'Render Image';

     K_ActiveLinks = 'Active Links';
     K_PageEndLines = 'Page End Lines';
     K_OptimizeforIE = 'Optimize for IE';
     K_SingleFile = 'Single File';
     K_InsertPageBreaks = 'Insert Page Breaks';
     K_ShowNavigator = 'Show Navigator';
     K_ColorBackGround = 'Color BackGround';
     K_ColorBackGroundHover = 'Color BackGround Hover';
     K_ColorForeGroundHover = 'Color ForeGround Hover';
     K_NavigatorType = 'Navigator Type';
     K_NavigatorOrientation = 'Navigator Orientation';
     K_NavigatorPosition = 'Navigator Position';
     K_NavLinksUseText = 'NavLinks - Use Text';
     K_NavLinksUseGraphic = 'NavLinks - Use Graphic';
     K_NavLinksFirstText = 'NavLinks - First Text';
     K_NavLinksLastText = 'NavLinks - Last Text';
     K_NavLinksPriorText = 'NavLinks - Prior Text';
     K_NavLinksNextText = 'NavLinks - Next Text';
     K_NavLinksFirstGraphic = 'NavLinks - First Graphic';
     K_NavLinksLastGraphic = 'NavLinks - Last Graphic';
     K_NavLinksPriorGraphic = 'NavLinks - Prior Graphic';
     K_NavLinksNextGraphic = 'NavLinks - Next Graphic';
     K_FontName = 'Font Name';
     K_FontColor = 'Font Color';
     K_FontSize = 'Font Size';
     K_FontNameDefault = 'Wingdings';
     K_FontSizeNumber = 14;
     K_FontBold = 'Font Bold';
     K_FontItalic = 'Font Italic';
     K_FontUnderline = 'Font Underline';
     K_FontStrikeOut = 'Font StrikeOut';
     K_ColumnSpacing = 'Column Spacing';
     K_RowSpacing    = 'Row Spacing';
     K_ScaleX        = 'Scale X';
     K_ScaleY        = 'Scale Y';
     K_PagesPerWorksheet = 'Pages Per Worksheet';
     K_AutoFormat    = 'Auto Format Cells';
     K_Margin        = 'Page Margins';
     K_PrintGridLines = 'Print Grid Lines';
     K_GraphicDataInBinary = 'Graphic Data In Binary';
     K_DocumentEncondingType = 'Document Enconding Type';
     K_TrueTypeFont = 'True Type Font';
     K_CompressionEnabled = 'Compression Enabled';
     K_CompressionLevel = 'Compression Level';
     K_EncryptionEnabled = 'Encryption Enabled';
     K_EncryptionLevel = 'Encryption  Level';
     K_EncryptionOwner = 'Encryption Pass Owner';
     K_EncryptionUser = 'Encryption Pass User';

     K_UserPermissionModify = 'UserPermissionModify';
     K_UserPermissionPrint = 'UserPermissionPrint';
     K_UserPermissionCopy = 'UserPermissionCopy';
     K_UserPermissionAnnotation = 'UserPermissionAnnotation';
     K_UserPermissionFormFill = 'UserPermissionFormFill';
     K_UserPermissionAccessibility = 'UserPermissionAccessibility';
     K_UserPermissionDocumentAssembly = 'UserPermissionDocumentAssembly';
     K_UserPermissionHighResolutionPrint = 'UserPermissionHighResolutionPrint';


     K_PageLayout = 'Page Layout';
     K_PageMode = 'Page Mode';
     K_MenuBar = 'Hide Menu Bar';
     K_ToolBar = 'Hide Tool Bar';
     K_NavigationControls = 'Hide Windows UI';
     K_PageTransitionDuration = 'Page Transition Duration';
     K_PageTransitionEffect = 'Page Transition Effect';
     K_EncodingFont = 'Encoding Font';

     K_Primero =   '�';
     K_Anterior =  '�';
     K_Siguiente = '�';
     K_Ultimo =    '�';
type
  TExportIniFile = class( TIniFile )
  private
    FFormato: eTipoFormato;
    FDescripcionFormato: string;
    procedure SetFormato(const eTipo: eTipoFormato);
  public
    constructor Create;
    procedure EscribeLogico(const sIdent: string; const lValor: Boolean);
    procedure EscribeTexto(const sIdent, sValor: string);
    procedure EscribeEntero(const sIdent: string; const iValor: integer);

    function LeeLogico(const sIdent: string; const lDefault : Boolean = FALSE ): Boolean;
    function LeeTexto(const sIdent: string; const sDefault: string = VACIO ): string;
    function LeeEntero(const sIdent: string; const iDefault : integer = 0 ): integer;

    property Formato: eTipoFormato read FFormato write SetFormato;
  end;

  TDocumentDlg = class(TZetaDlgModal)
    PageControl: TPageControl;
    tsPreferencias: TTabSheet;
    gbPageRange: TGroupBox;
    rbTodas: TRadioButton;
    rbRango: TRadioButton;
    gbItemsToRender: TGroupBox;
    cbTextos: TCheckBox;
    cbFiguras: TCheckBox;
    cbImagen: TCheckBox;
    ePaginas: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure rbTodasClick(Sender: TObject);
    procedure rbRangoEnter(Sender: TObject);
    procedure ePaginasEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FPagesToRender: String;
    FFormato: eTipoFormato;
  protected
    FIniFile : TExportIniFile;
    procedure CargaArchivo; virtual;
    procedure GrabaArchivo; virtual;

    procedure SetProperties; virtual;
    procedure GetProperties; virtual;

    procedure MostrarControles(const lMostrar: Boolean; oControl: TWinControl);

  public
    { Public declarations }
    property Formato: eTipoFormato read FFormato write FFormato;

  end;

var
  DocumentDlg: TDocumentDlg;

implementation
uses
    ZetaCommonTools;

{$R *.DFM}

{ ******************   TExportIniFile   ***************** }
constructor TExportIniFile.Create;
begin
     inherited Create( VerificaDir( ExtractFilePath( Application.ExeName ) ) + K_EXPORT_INI_FILE );
end;

procedure TExportIniFile.SetFormato( const eTipo: eTipoFormato );
begin
     FFormato := eTipo;
     FDescripcionFormato := ObtieneElemento( lfTipoFormato, Ord(FFormato) );
end;

function TExportIniFile.LeeTexto( const sIdent: string; const sDefault: string = VACIO ): string;
begin
     Result := ReadString( FDescripcionFormato, sIdent, sDefault );
end;

procedure TExportIniFile.EscribeTexto( const sIdent, sValor: string );
begin
     WriteString( FDescripcionFormato, sIdent, sValor  );
end;

function TExportIniFile.LeeLogico( const sIdent: string; const lDefault : Boolean ): Boolean ;
begin
     Result := ReadBool( FDescripcionFormato, sIdent, lDefault );
end;

procedure TExportIniFile.EscribeLogico( const sIdent: string; const lValor: Boolean );
begin
     WriteBool( FDescripcionFormato, sIdent, lValor );
end;

function TExportIniFile.LeeEntero(const sIdent: string; const iDefault: integer): integer;
begin
     Result := ReadInteger( FDescripcionFormato, sIdent, iDefault );
end;

procedure TExportIniFile.EscribeEntero(const sIdent: string; const iValor: integer);
begin
     WriteInteger( FDescripcionFormato, sIdent, iValor );
end;

{******************************  TDocumentDlg ********************************}
procedure TDocumentDlg.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Document_Dialog;

     tsPreferencias.TabVisible := FALSE;
     Pagecontrol.ActivePage :=  tsPreferencias;

     rbTodas.Checked := TRUE;
     rbTodas.OnClick( rbTodas );
     cbTextos.Checked := TRUE;
     cbFiguras.Checked := TRUE;
     cbImagen.Checked := TRUE;
     Caption := 'Configuraci�n';
end;

procedure TDocumentDlg.FormShow(Sender: TObject);
begin
     inherited;
     gbItemsToRender.Visible := NOT(FFormato in [tfDIF,tfWB1,tfWK2,tfSLK] );

     CargaArchivo;
     SetProperties;
end;

procedure TDocumentDlg.SetProperties;
begin
     rbTodas.Checked := StrVacio( FPagesToRender );
     rbRango.Checked := not rbTodas.Checked;
     ePaginas.Text := FPagesToRender;
end;

procedure TDocumentDlg.GetProperties;
begin
     if rbRango.Checked then
        FPagesToRender := ePaginas.Text
     else
         FPagesToRender := VACIO;
end;

procedure TDocumentDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     if ModalResult = mrOk then
        GrabaArchivo;
end;

procedure TDocumentDlg.CargaArchivo;
begin
     if FIniFile = NIL then
        FIniFile := TExportIniFile.Create;

     FIniFile.Formato := FFormato;
     with FIniFile do
     begin
          FPagesToRender := LeeTexto( K_PagesToRender );
          cbTextos.Checked := LeeLogico( K_RenderText, TRUE );
          cbFiguras.Checked := LeeLogico( K_RenderShapes, NOT(FFormato in [tfXLS]) );
          cbImagen.Checked := LeeLogico( K_RenderImage, TRUE );
     end;
end;

procedure TDocumentDlg.GrabaArchivo;
begin
     GetProperties;

     with FIniFile do
     begin
          EscribeTexto( K_PagesToRender, FPagesToRender );
          EscribeLogico( K_RenderText, cbTextos.Checked );
          EscribeLogico( K_RenderShapes, cbFiguras.Checked );
          EscribeLogico( K_RenderImage, cbImagen.Checked );
     end;

     FreeAndNil( FIniFile );
end;

procedure TDocumentDlg.rbTodasClick(Sender: TObject);
begin
     inherited;
     rbRango.Checked := NOT rbTodas.Checked;
end;

procedure TDocumentDlg.rbRangoEnter(Sender: TObject);
begin
     inherited;
     ePaginas.SetFocus;
end;

procedure TDocumentDlg.ePaginasEnter(Sender: TObject);
begin
     inherited;
     rbRango.Checked := TRUE;
end;


procedure TDocumentDlg.MostrarControles( const lMostrar: Boolean; oControl: TWinControl );
 var
    i: integer;
    oControlActivo: TWinControl;
begin
     for i := 0 to oControl.ControlCount -1 do
     begin
          if ( oControl.Controls[ i ] is TWinControl ) then
          begin
               oControlActivo := TWinControl(oControl.Controls[ i ]);
               if oControlActivo.ControlCount > 0 then
                  MostrarControles( lMostrar, oControlActivo )
               else
                   oControlActivo.Enabled := lMostrar;
          end
          else
              oControl.Controls[ i ].Enabled := lMostrar;
     end;
end;




end.
