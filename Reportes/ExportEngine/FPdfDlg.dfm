inherited PdfDlg: TPdfDlg
  Left = 406
  Top = 154
  Caption = 'PdfDlg'
  ClientHeight = 360
  ClientWidth = 351
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 324
    Width = 351
    inherited OK: TBitBtn
      Left = 183
    end
    inherited Cancelar: TBitBtn
      Left = 268
    end
  end
  inherited PageControl: TPageControl
    Width = 351
    Height = 324
    ActivePage = tsAvanzado
    inherited tsPreferencias: TTabSheet
      inherited gbPageRange: TGroupBox
        Width = 343
      end
      inherited gbItemsToRender: TGroupBox
        Width = 343
        Height = 83
        Align = alTop
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 152
        Width = 343
        Height = 144
        Align = alClient
        Caption = ' Opciones '
        TabOrder = 2
        object lblFontEncoding: TLabel
          Left = 16
          Top = 102
          Width = 123
          Height = 13
          Caption = 'Codificaci�n de la Fuente:'
        end
        object cbActiveHyperLinks: TCheckBox
          Left = 16
          Top = 16
          Width = 105
          Height = 17
          Caption = 'Ligas Activas'
          TabOrder = 0
        end
        object rgTrueType: TRadioGroup
          Left = 16
          Top = 40
          Width = 273
          Height = 49
          Caption = ' Informaci�n Fuentes '#39'TrueType'#39' '
          Columns = 3
          Items.Strings = (
            'No Incluir'
            'Todo'
            'SubConjunto')
          TabOrder = 1
        end
        object cbEncoding: TZetaKeyCombo
          Left = 144
          Top = 98
          Width = 133
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
          ListaFija = lfFontEncoding
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
      end
    end
    object tsAvanzado: TTabSheet
      Caption = 'Avanzado'
      ImageIndex = 1
      object gbUseCompression: TGroupBox
        Left = 0
        Top = 0
        Width = 343
        Height = 41
        Align = alTop
        TabOrder = 1
        object lblCompressionLevel: TLabel
          Left = 15
          Top = 20
          Width = 100
          Height = 13
          Caption = 'Nivel de Compresi�n:'
        end
        object cbCompressionLevel: TZetaKeyCombo
          Left = 120
          Top = 16
          Width = 133
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          ListaFija = lfCompressionLevel
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
      end
      object chkCompressDocument: TCheckBox
        Left = 15
        Top = -1
        Width = 146
        Height = 17
        Caption = 'Comprimir Documento'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object gbEncryption: TGroupBox
        Left = 0
        Top = 41
        Width = 343
        Height = 255
        Align = alClient
        TabOrder = 3
        object lblOwnerPassword: TLabel
          Left = 14
          Top = 23
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clave del Due�o:'
        end
        object lblConfirmOwnerPassword: TLabel
          Left = 32
          Top = 47
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confirmaci�n:'
        end
        object lblUserPassword: TLabel
          Left = 10
          Top = 71
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clave del Usuario:'
        end
        object lblConfirmUserPassword: TLabel
          Left = 32
          Top = 95
          Width = 64
          Height = 13
          Alignment = taRightJustify
          Caption = 'Confirmaci�n:'
        end
        object lblEncryptionLevel: TLabel
          Left = 69
          Top = 119
          Width = 27
          Height = 13
          Caption = 'Nivel:'
        end
        object edOwnerPassword: TEdit
          Left = 98
          Top = 19
          Width = 133
          Height = 21
          PasswordChar = '*'
          TabOrder = 0
        end
        object edConfirmOwnerPassword: TEdit
          Left = 98
          Top = 43
          Width = 133
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
          OnExit = edConfirmOwnerPasswordExit
        end
        object gbUserPermissions: TGroupBox
          Left = 8
          Top = 144
          Width = 293
          Height = 105
          Caption = ' Permisos del Usuario '
          TabOrder = 5
          TabStop = True
          object chkPrint: TCheckBox
            Left = 7
            Top = 57
            Width = 106
            Height = 17
            Caption = 'Imprimir'
            TabOrder = 2
          end
          object chkModify: TCheckBox
            Left = 7
            Top = 37
            Width = 64
            Height = 17
            Caption = 'Modificar'
            TabOrder = 1
          end
          object chkCopy: TCheckBox
            Left = 7
            Top = 17
            Width = 58
            Height = 17
            Caption = 'Copiar'
            TabOrder = 0
          end
          object chkAnnotation: TCheckBox
            Left = 120
            Top = 16
            Width = 97
            Height = 17
            Caption = 'Anotar'
            TabOrder = 4
          end
          object chkFormFill: TCheckBox
            Left = 7
            Top = 77
            Width = 114
            Height = 17
            Caption = 'Llenado de Forma'
            TabOrder = 3
          end
          object chkAccessibility: TCheckBox
            Left = 120
            Top = 37
            Width = 97
            Height = 17
            Caption = 'Accesibilidad'
            TabOrder = 5
          end
          object chkDocumentAssembly: TCheckBox
            Left = 120
            Top = 57
            Width = 145
            Height = 17
            Caption = 'Ensamble'
            TabOrder = 6
          end
          object chkHighResolutionPrint: TCheckBox
            Left = 120
            Top = 77
            Width = 169
            Height = 17
            Caption = 'Impresi�n de Alta Resoluci�n'
            TabOrder = 7
          end
        end
        object edUserPassword: TEdit
          Left = 98
          Top = 67
          Width = 133
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object edConfirmUserPassword: TEdit
          Left = 98
          Top = 91
          Width = 133
          Height = 21
          PasswordChar = '*'
          TabOrder = 3
          OnExit = edConfirmUserPasswordExit
        end
        object cbEncryptionLevel: TZetaKeyCombo
          Left = 98
          Top = 115
          Width = 133
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 4
          ListaFija = lfEncryptionLevel
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
      end
      object cbEnableEncryption: TCheckBox
        Left = 16
        Top = 40
        Width = 137
        Height = 17
        Caption = 'Encriptar Documento'
        TabOrder = 2
        OnClick = cbEnableEncryptionClick
      end
    end
    object tsUsuario: TTabSheet
      Caption = 'Preferencias del Usuario'
      ImageIndex = 2
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 343
        Height = 73
        Align = alTop
        Caption = ' P�gina '
        TabOrder = 0
        object lblPageLayout: TLabel
          Left = 30
          Top = 21
          Width = 58
          Height = 13
          Caption = 'Distribuci�n:'
        end
        object lblPageMode: TLabel
          Left = 58
          Top = 45
          Width = 30
          Height = 13
          Caption = 'Modo:'
        end
        object cbPageLayout: TZetaKeyCombo
          Left = 90
          Top = 17
          Width = 170
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 0
          ListaFija = lfPageLayout
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
        object cbPageMode: TZetaKeyCombo
          Left = 90
          Top = 41
          Width = 170
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 1
          ListaFija = lfPageMode
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
      end
      object gbHideUIElements: TGroupBox
        Left = 0
        Top = 73
        Width = 343
        Height = 62
        Align = alTop
        Caption = ' Esconder Elementos '
        TabOrder = 1
        object chkMenuBar: TCheckBox
          Left = 8
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Barra del Men�'
          TabOrder = 0
        end
        object chkToolBar: TCheckBox
          Left = 161
          Top = 16
          Width = 152
          Height = 17
          Caption = 'Barras de Herramientas'
          TabOrder = 2
        end
        object chkNavigationControls: TCheckBox
          Left = 8
          Top = 36
          Width = 145
          Height = 17
          Caption = 'Controles de Navegaci�n'
          TabOrder = 1
        end
      end
      object gbPresentationMode: TGroupBox
        Left = 0
        Top = 135
        Width = 343
        Height = 161
        Align = alClient
        Caption = ' Modo de Presentaci�n - Transici�n '
        TabOrder = 2
        object lblPageTransitionEffect: TLabel
          Left = 8
          Top = 24
          Width = 75
          Height = 13
          Caption = 'Efecto P�ginas:'
        end
        object lblPageTransitionDuration: TLabel
          Left = 37
          Top = 46
          Width = 46
          Height = 13
          Caption = 'Duraci�n:'
        end
        object lblSecs: TLabel
          Left = 202
          Top = 48
          Width = 48
          Height = 13
          Caption = 'Segundos'
        end
        object edPageTransitionDuration: TZetaNumero
          Left = 90
          Top = 44
          Width = 109
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 1
        end
        object cbPageTransitionEffect: TZetaKeyCombo
          Left = 90
          Top = 20
          Width = 247
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 0
          ListaFija = lfTransitionEffect
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
      end
    end
  end
end
