unit FRtfDlg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls, ZDocumentDlg_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, dxSkinscxPCPainter,
  dxBarBuiltInMenu, cxControls, cxPC;

type
  TRtfDlg_DevEx = class(TDocumentDlg_DevEx)
    GroupBox1: TGroupBox;
    cbActiveHiperLinks: TCheckBox;
    cbGraphicDataBinary: TCheckBox;
    cbEncondingType: TRadioGroup;
  private
    { Private declarations }
  protected
    procedure CargaArchivo; override;
    procedure GrabaArchivo; override;

  public
    { Public declarations }
  end;

var
  RtfDlg_DevEx: TRtfDlg_DevEx;

implementation

{$R *.DFM}

{ TDocumentDlg1 }

procedure TRtfDlg_DevEx.CargaArchivo;
begin
     inherited;
     with FIniFile do
     begin
          cbActiveHiperLinks.Checked := LeeLogico( K_ActiveLinks, TRUE );
          cbGraphicDataBinary.Checked := LeeLogico( K_GraphicDataInBinary, TRUE );
          cbEncondingType.ItemIndex := LeeEntero( K_DocumentEncondingType, 0 );
     end;
end;

procedure TRtfDlg_DevEx.GrabaArchivo;
begin
     with FIniFile do
     begin
          EscribeLogico( K_ActiveLinks, cbActiveHiperLinks.Checked );
          EscribeLogico( K_GraphicDataInBinary, cbGraphicDataBinary.Checked );
          EscribeEntero( K_DocumentEncondingType, cbEncondingType.ItemIndex  );
     end;

     inherited;
end;

end.
