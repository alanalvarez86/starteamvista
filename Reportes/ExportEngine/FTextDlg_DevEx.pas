unit FTextDlg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Buttons, ExtCtrls, ZDocumentDlg_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,dxSkinsDefaultPainters, ImgList, cxButtons,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxControls, cxPC;

type
  TTextDlg_DevEx = class(TDocumentDlg_DevEx)
    GroupBox1: TGroupBox;
    cbSeparador: TCheckBox;
    cbBreak: TCheckBox;
    cbUnDocumento: TCheckBox;
  private
    { Private declarations }
  protected
    procedure CargaArchivo; override;
    procedure GrabaArchivo; override;
  public
    { Public declarations }
  end;

var
  TextDlg_DevEx: TTextDlg_DevEx;

implementation

{$R *.DFM}

{ TTextDlg }

procedure TTextDlg_DevEx.CargaArchivo;
begin
     inherited;
     with FIniFile do
     begin
          cbSeparador.Checked    := LeeLogico( K_PageEndLines, TRUE );
          cbBreak.Checked        := LeeLogico( K_InsertPageBreaks, TRUE );
          cbUnDocumento.Checked  := LeeLogico( K_SingleFile, TRUE );
     end;
end;

procedure TTextDlg_DevEx.GrabaArchivo;
begin
     with FIniFile do
     begin
          EscribeLogico( K_PageEndLines, cbSeparador.Checked );
          EscribeLogico( K_InsertPageBreaks, cbBreak.Checked );
          EscribeLogico( K_SingleFile, cbUnDocumento.Checked );
     end;
     inherited;
end;

end.
