inherited HtmlDlg: THtmlDlg
  Left = 520
  Top = 171
  ClientHeight = 333
  ClientWidth = 405
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 297
    Width = 405
    inherited OK: TBitBtn
      Left = 237
    end
    inherited Cancelar: TBitBtn
      Left = 322
    end
  end
  inherited PageControl: TPageControl
    Width = 405
    Height = 297
    ActivePage = tsNavegador
    inherited tsPreferencias: TTabSheet
      inherited gbPageRange: TGroupBox
        Width = 397
      end
      inherited gbItemsToRender: TGroupBox
        Width = 397
        Height = 84
        Align = alTop
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 153
        Width = 397
        Height = 116
        Align = alClient
        Caption = ' Opciones '
        TabOrder = 2
        object cbLigasActivas: TCheckBox
          Left = 16
          Top = 16
          Width = 129
          Height = 17
          Caption = 'Ligas Activas'
          TabOrder = 0
        end
        object cbSeparador: TCheckBox
          Left = 16
          Top = 36
          Width = 129
          Height = 17
          Caption = 'Separador de P'#225'ginas'
          TabOrder = 1
        end
        object cbOptimizar: TCheckBox
          Left = 16
          Top = 56
          Width = 189
          Height = 17
          Caption = 'Optimizar para Internet Explorer'
          TabOrder = 2
        end
      end
    end
    object tsNavegador: TTabSheet
      Caption = 'Navegador'
      ImageIndex = 1
      object cbUnaPagina: TCheckBox
        Left = 8
        Top = 6
        Width = 113
        Height = 17
        Caption = 'Una Sola P'#225'gina'
        TabOrder = 0
        OnClick = cbUnaPaginaClick
      end
      object gbMostrarNavegador: TGroupBox
        Left = 2
        Top = 27
        Width = 393
        Height = 238
        TabOrder = 1
        object gbUseLinks: TGroupBox
          Left = 8
          Top = 112
          Width = 377
          Height = 121
          TabOrder = 2
          object pcShowNavigator: TPageControl
            Left = 10
            Top = 15
            Width = 359
            Height = 98
            ActivePage = tsUseGraphicLinks
            TabOrder = 0
            object tsUseTextLinks: TTabSheet
              BorderWidth = 1
              Caption = 'tsUseTextLinks'
              TabVisible = False
              object lblFirst: TLabel
                Left = 20
                Top = 30
                Width = 38
                Height = 13
                Caption = 'Primero:'
              end
              object lblNext: TLabel
                Left = 11
                Top = 55
                Width = 47
                Height = 13
                Caption = 'Siguiente:'
              end
              object lblPrevious: TLabel
                Left = 175
                Top = 30
                Width = 39
                Height = 13
                Caption = 'Anterior:'
              end
              object lblLast: TLabel
                Left = 182
                Top = 55
                Width = 32
                Height = 13
                Caption = 'Ultimo:'
              end
              object lblLinkCaptions: TLabel
                Left = 4
                Top = 1
                Width = 165
                Height = 13
                Caption = 'Texto/Imagenes de las Ligas'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object btnSetFont: TButton
                Left = 252
                Top = 1
                Width = 68
                Height = 21
                Caption = 'Fuente'
                TabOrder = 0
                OnClick = btnSetFontClick
              end
              object ePrimerTexto: TEdit
                Left = 60
                Top = 26
                Width = 106
                Height = 21
                AutoSize = False
                TabOrder = 1
              end
              object eAnteriorTexto: TEdit
                Left = 216
                Top = 26
                Width = 104
                Height = 21
                AutoSize = False
                TabOrder = 2
              end
              object eSiguienteTexto: TEdit
                Left = 60
                Top = 51
                Width = 106
                Height = 21
                AutoSize = False
                TabOrder = 3
              end
              object eUltimoTexto: TEdit
                Left = 216
                Top = 51
                Width = 104
                Height = 21
                AutoSize = False
                TabOrder = 4
              end
            end
            object tsUseGraphicLinks: TTabSheet
              Tag = 1
              BorderWidth = 1
              Caption = 'tsUseGraphicLinks'
              TabVisible = False
              object bPrimero: TSpeedButton
                Left = 146
                Top = 26
                Width = 23
                Height = 21
                Caption = '...'
                OnClick = bPrimeroClick
              end
              object bAnterior: TSpeedButton
                Left = 300
                Top = 26
                Width = 23
                Height = 21
                Caption = '...'
                OnClick = bAnteriorClick
              end
              object bSiguiente: TSpeedButton
                Left = 146
                Top = 51
                Width = 23
                Height = 21
                Caption = '...'
                OnClick = bSiguienteClick
              end
              object bUltimo: TSpeedButton
                Left = 300
                Top = 51
                Width = 23
                Height = 21
                Caption = '...'
                OnClick = bUltimoClick
              end
              object lblUseGraphicLinksFirst: TLabel
                Left = 20
                Top = 30
                Width = 38
                Height = 13
                Caption = 'Primero:'
              end
              object lblUseGraphicLinksNext: TLabel
                Left = 11
                Top = 55
                Width = 47
                Height = 13
                Caption = 'Siguiente:'
              end
              object lblUseGraphicLinksPrevious: TLabel
                Left = 175
                Top = 30
                Width = 39
                Height = 13
                Caption = 'Anterior:'
              end
              object lblUseGraphicLinksLast: TLabel
                Left = 182
                Top = 55
                Width = 32
                Height = 13
                Caption = 'Ultimo:'
              end
              object lblImageSource: TLabel
                Left = 4
                Top = 1
                Width = 124
                Height = 13
                Caption = 'Ruta de las Im'#225'genes'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object ePrimeroImagen: TEdit
                Left = 60
                Top = 26
                Width = 84
                Height = 21
                TabOrder = 0
              end
              object eAnteriorImagen: TEdit
                Left = 216
                Top = 26
                Width = 82
                Height = 21
                TabOrder = 1
              end
              object eUltimoImagen: TEdit
                Left = 216
                Top = 51
                Width = 82
                Height = 21
                TabOrder = 3
              end
              object eSiguienteImagen: TEdit
                Left = 60
                Top = 51
                Width = 84
                Height = 21
                TabOrder = 2
              end
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 8
          Top = 24
          Width = 137
          Height = 85
          Caption = ' Colores '
          TabOrder = 0
          object shBackgroundColor: TShape
            Tag = 2
            Left = 94
            Top = 12
            Width = 31
            Height = 19
            ParentShowHint = False
            ShowHint = True
            OnMouseDown = shBackgroundColorMouseDown
          end
          object shpHoverBackColor: TShape
            Tag = 1
            Left = 94
            Top = 34
            Width = 31
            Height = 19
            ParentShowHint = False
            ShowHint = True
            OnMouseDown = shBackgroundColorMouseDown
          end
          object shpHoverForeColor: TShape
            Left = 94
            Top = 56
            Width = 31
            Height = 19
            ParentShowHint = False
            ShowHint = True
            OnMouseDown = shBackgroundColorMouseDown
          end
          object lblNavigatorBackgroundColor: TLabel
            Left = 56
            Top = 15
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fondo:'
          end
          object lblHoverBackColor: TLabel
            Left = 8
            Top = 37
            Width = 81
            Height = 13
            Alignment = taRightJustify
            Caption = 'Fondo Se'#241'alado:'
          end
          object lblHoverForeColor: TLabel
            Left = 11
            Top = 59
            Width = 78
            Height = 13
            Alignment = taRightJustify
            Caption = 'Texto Se'#241'alado:'
          end
        end
        object GroupBox4: TGroupBox
          Left = 152
          Top = 24
          Width = 233
          Height = 85
          Caption = ' Navegador '
          TabOrder = 1
          object lblNavigatorType: TLabel
            Left = 45
            Top = 16
            Width = 24
            Height = 13
            Caption = 'Tipo:'
          end
          object lblNavigatorOrientation: TLabel
            Left = 12
            Top = 38
            Width = 57
            Height = 13
            Caption = 'Orientaci'#243'n:'
          end
          object lblNavigatorPosition: TLabel
            Left = 26
            Top = 60
            Width = 43
            Height = 13
            Caption = 'Posici'#243'n:'
          end
          object cbNavigatorType: TZetaKeyCombo
            Left = 74
            Top = 12
            Width = 130
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ItemHeight = 13
            ParentCtl3D = False
            TabOrder = 0
            ListaFija = lfTipoNavegador
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
          object cbNavigatorOrientation: TZetaKeyCombo
            Left = 74
            Top = 34
            Width = 130
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ItemHeight = 13
            ParentCtl3D = False
            TabOrder = 1
            ListaFija = lfOrientacionNavegador
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
          object cbNavigatorPosition: TZetaKeyCombo
            Left = 74
            Top = 56
            Width = 130
            Height = 21
            AutoComplete = False
            BevelKind = bkFlat
            Style = csDropDownList
            Ctl3D = False
            ItemHeight = 13
            ParentCtl3D = False
            TabOrder = 2
            ListaFija = lfPosicionNavegador
            ListaVariable = lvPuesto
            Offset = 0
            Opcional = False
            EsconderVacios = False
          end
        end
        object rbtnUseTextLinks: TRadioButton
          Left = 16
          Top = 111
          Width = 157
          Height = 17
          Caption = 'Usar Texto'
          Checked = True
          TabOrder = 3
          TabStop = True
          OnClick = rbtnUseTextLinksClick
        end
        object rbtnUseGraphicLinks: TRadioButton
          Tag = 1
          Left = 173
          Top = 111
          Width = 160
          Height = 17
          Caption = 'Usar Imagenes'
          TabOrder = 4
          OnClick = rbtnUseGraphicLinksClick
        end
      end
      object cbMostrarNavegador: TCheckBox
        Left = 8
        Top = 24
        Width = 113
        Height = 17
        Caption = 'Mostrar Navegador'
        TabOrder = 2
        OnClick = cbMostrarNavegadorClick
      end
    end
  end
  object ColorDialog: TColorDialog
    Left = 224
    Top = 24
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Wingdings'
    Font.Style = []
    Left = 252
    Top = 24
  end
  object OpenPictureDialog: TOpenPictureDialog
    Left = 284
    Top = 24
  end
end
