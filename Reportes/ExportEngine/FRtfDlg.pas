unit FRtfDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZDocumentDlg, StdCtrls, ComCtrls, Buttons, ExtCtrls;

type
  TRtfDlg = class(TDocumentDlg)
    GroupBox1: TGroupBox;
    cbActiveHiperLinks: TCheckBox;
    cbGraphicDataBinary: TCheckBox;
    cbEncondingType: TRadioGroup;
  private
    { Private declarations }
  protected
    procedure CargaArchivo; override;
    procedure GrabaArchivo; override;

  public
    { Public declarations }
  end;

var
  RtfDlg: TRtfDlg;

implementation

{$R *.DFM}

{ TDocumentDlg1 }

procedure TRtfDlg.CargaArchivo;
begin
     inherited;
     with FIniFile do
     begin
          cbActiveHiperLinks.Checked := LeeLogico( K_ActiveLinks, TRUE );
          cbGraphicDataBinary.Checked := LeeLogico( K_GraphicDataInBinary, TRUE );
          cbEncondingType.ItemIndex := LeeEntero( K_DocumentEncondingType, 0 );
     end;
end;

procedure TRtfDlg.GrabaArchivo;
begin
     with FIniFile do
     begin
          EscribeLogico( K_ActiveLinks, cbActiveHiperLinks.Checked );
          EscribeLogico( K_GraphicDataInBinary, cbGraphicDataBinary.Checked );
          EscribeEntero( K_DocumentEncondingType, cbEncondingType.ItemIndex  );
     end;

     inherited;
end;

end.
