inherited TextDlg: TTextDlg
  Left = 397
  Top = 230
  Caption = ''
  ClientHeight = 297
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 261
  end
  inherited PageControl: TPageControl
    Height = 261
    inherited tsPreferencias: TTabSheet
      inherited gbItemsToRender: TGroupBox
        Height = 84
        Align = alTop
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 153
        Width = 309
        Height = 80
        Align = alClient
        Caption = ' Opciones '
        TabOrder = 2
        object cbSeparador: TCheckBox
          Left = 16
          Top = 16
          Width = 137
          Height = 17
          Caption = 'Separador de P�ginas'
          TabOrder = 0
        end
        object cbBreak: TCheckBox
          Left = 16
          Top = 36
          Width = 137
          Height = 17
          Caption = 'Salto de P�gina'
          TabOrder = 1
        end
        object cbUnDocumento: TCheckBox
          Left = 16
          Top = 56
          Width = 129
          Height = 17
          Caption = 'Un Solo Documento'
          TabOrder = 2
        end
      end
    end
  end
end
