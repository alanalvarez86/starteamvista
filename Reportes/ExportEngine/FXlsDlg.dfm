inherited XlsDlg: TXlsDlg
  Caption = 'XlsDlg'
  ClientHeight = 367
  ClientWidth = 312
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 331
    Width = 312
    inherited OK: TBitBtn
      Left = 144
    end
    inherited Cancelar: TBitBtn
      Left = 229
    end
  end
  inherited PageControl: TPageControl
    Width = 312
    Height = 331
    inherited tsPreferencias: TTabSheet
      inherited gbPageRange: TGroupBox
        Width = 304
      end
      inherited gbItemsToRender: TGroupBox
        Width = 304
        Align = alTop
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 147
        Width = 304
        Height = 156
        Align = alClient
        Caption = ' Formateo '
        TabOrder = 2
        object Label1: TLabel
          Left = 45
          Top = 20
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Espacios Columnas:'
        end
        object Label2: TLabel
          Left = 40
          Top = 44
          Width = 100
          Height = 13
          Alignment = taRightJustify
          Caption = 'Espacios Renglones:'
        end
        object Label3: TLabel
          Left = 197
          Top = 20
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Escala X:'
        end
        object Label4: TLabel
          Left = 197
          Top = 44
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = 'Escala Y:'
        end
        object lbPagesPerWorksheet: TLabel
          Left = 10
          Top = 68
          Width = 130
          Height = 13
          Alignment = taRightJustify
          Caption = 'P�ginas X Hoja de Trabajo:'
        end
        object zColumnSpacing: TZetaNumero
          Left = 142
          Top = 16
          Width = 50
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object zRowSpacing: TZetaNumero
          Left = 142
          Top = 40
          Width = 50
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
        end
        object zScaleX: TZetaNumero
          Left = 244
          Top = 16
          Width = 50
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 3
        end
        object zScaleY: TZetaNumero
          Left = 244
          Top = 40
          Width = 50
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 4
        end
        object zPagesPerWorksheet: TZetaNumero
          Left = 142
          Top = 64
          Width = 50
          Height = 21
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
        end
        object cbAutoFormat: TCheckBox
          Left = 16
          Top = 96
          Width = 217
          Height = 17
          Caption = 'Formatear Celdas Autom�ticamente'
          TabOrder = 5
        end
        object cbMargin: TCheckBox
          Left = 16
          Top = 136
          Width = 121
          Height = 17
          Caption = 'M�rgenes'
          TabOrder = 7
        end
        object cbPrintGridLines: TCheckBox
          Left = 16
          Top = 116
          Width = 185
          Height = 17
          Caption = 'Mostrar L�neas de la Cuadr�cula'
          TabOrder = 6
        end
      end
    end
  end
end
