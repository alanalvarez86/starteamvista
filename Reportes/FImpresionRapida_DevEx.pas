{$HINTS OFF}
{$WARNINGS OFF}
unit FImpresionRapida_DevEx;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,    ZAgenteSQLClient,
  ZetaSmartLists, ZetaKeyLookup, ZetaEdit, ZetaFecha, CheckLst, Db,
  ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls, ZetaNumero,
  Grids, DBGrids, ZetaDBGrid,ZReportTools,ZReportToolsConsts, DBClient,
  ZetaTipoEntidad,ZetacommonLists, Spin, jpeg,
  FEditReportes_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  
  TressMorado2013, dxSkinsDefaultPainters, 
  dxSkinsdxBarPainter, dxBar, cxClasses, ImgList, ZetaKeyLookup_DevEx,
  cxButtons, cxStyles, cxControls, cxContainer, cxEdit,
  ZetaSmartLists_DevEx, cxTextEdit, cxMemo, cxListBox, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, cxCheckListBox, dxBarBuiltInMenu, cxDBEdit;

type
  eStatusImpresora = (siLista, siOffLine, siSinPapel, siApagada, siDesconocido );
  EPrinterError = class( Exception );
  TCodigosImpresora = record
    Nombre      : String; 	// Nombre de la Impresora
    Eject       : String;	// Eject. Salto de P�gina y Fin de Documento
    Ancho10     : String;	// Ancho normal, 10 caracteres/pulgada
    Ancho12     : String;	// Semi-Comprimido, 12 caracteres/pulgada
    Comprimido  : String;	// Comprimido, 17 caracteres/pulgada
    Extra1      : String;	// Para uso futuro
    Extra2      : String;	// Para uso futuro
    Reset       : String;	// Reset de Impresora. Al final de la impresi�n
    SubrayadoOn : String;	// Inicio de Subrayado
    SubrayadoOff: String;	// Fin de Subrayado
    NegritaOn  	: String;       // Inicio de Bold
    NegritaOff  : String;	// Fin de Bold
    Lineas6  	: String;       // Imprimir 6 renglones / pulgada
    Lineas8  	: String;       // Imprimir 8 renglones / pulgada
    ItalicaOn  	: String;       // Inicio de Italic
    ItalicaOff  : String;	// Fin de Italic
    Landscape  	: String;       // Impresi�n Landscape
  end;
  TFastForm = class(TObject)
  private
     FEsPreview  : Boolean;
     FFastPuerto : String;
     FImpresora  : TextFile;
     FCodigoIni: String;
     FCodigoFin: String;
     FCodigosImpresora : TCodigosImpresora;
     FFuente : TFuente;           // Tipo de Letra General
     FDataset : TDataset;
     FEncabezado : TStringList;
     FDetalle : TStringList;
     FPiePagina : TStringList;
     FLineasForma : Integer;
     FLineasEncabezado: Integer;
     FLineasDetalle: Integer;
     FCompensaCada: Integer;
     FLineasCompensa: Integer;
     //FPagina : Integer;
     FNumCopias : Integer;
     //FRenglones : Array of String; //OLD
     FRenglones : Array of AnsiString; //@(am): Se modifica para que en las impresiones se sigan escribiendo los caracteres latinos correctamente.
     FColumnas  : Array of Integer;
     FNumGrupos : Integer;
     FLlaves    : Array of String;
     FLlavesPos : Array of Integer;
     FRowDetalle : Integer;
     FColumnasForma : Integer;
     FNavUltimo : Boolean;
     FPagInicial: Integer;
     FPagFinal: Integer;
     FCampoCiclo,FCampoCLetra : TCampoOpciones;
     FPaginaCopias : integer;
     procedure EscribeEncabezado;
     function  EscribeDetalle : Boolean;
     procedure EscribePiePagina;
     procedure ImprimeRenglones;
     procedure AbreImpresora;
     procedure CierraImpresora;
     function  GetStatusImpresora : eStatusImpresora;
     procedure MandaCodigo( const sCodigo: String );
     procedure MandaFuenteInicial;
     procedure MandaFuenteFinal;
     function  PrendeFuente( oFuente : TFuente ) : String;
     function  ApagaFuente( oFuente : TFuente ) : String;
     procedure ErrorDeImpresion( const eStatus : eStatusImpresora );
     procedure ExcepcionImpresion(const ErrorCode: Integer);
     procedure EscribeDato( oColumna : TCampoIRapida; const nOffset : Integer );
     procedure OrdenaLista( FLista: TStringList; oLista : TStrings );
     procedure AsignaObjetos(FLista, oLista: TStrings);
     //procedure AsignaTopLista(oLista: TStrings);
     procedure AsignaDetalle(FLista,oLista : TStrings);
     procedure PreparaGrupos( oGrupos : TStrings );
     procedure AsignaLlaves;
     function  MismoDetalle : Boolean;
     procedure PreparaRenglones;
     procedure LimpiaRenglones;
     procedure GetCodigosImpresora;
     procedure EscribeUnaForma;
     procedure EscribeUnPreliminar;
     function GetCicloCopias: integer;
     function ConvierteFCiclo(const sTexto: string; const iPagina : integer): string;

  public
     constructor Create;
     destructor  Destroy; override;
     procedure  PreparaReporte( oDataSet : TDataset; oEncabezado, oDetalle, oPiePagina, oGrupos : TStrings );
     procedure  GeneraReporte;
     property   LineasForma : Integer read FLineasForma;
     property   ColumnasForma : Integer read FColumnasForma;
     property   PagInicial : Integer read FPagInicial write FPagInicial;
     property   PagFinal : Integer read FPagFinal write FPagFinal;
     procedure  IniciaPreliminar;
     function   UltimoPreliminar : integer;
     function   SiguientePreliminar: Boolean;

     //Funciones para las impresiones subsecuentes
     procedure SubsecuentesAbreImpresora;
     procedure SubsecuentesGeneraReporte( subDataset: TDataset );
     procedure SubsecuentesCierraImpresora;

  end;

  LineaModificada = ( liFormas,liEncabezado,liDetalle);

  TImpresionRapida_DevEx = class(TEditReportes_DevEx)
    lRenglon: TLabel;
    ERenglon: TZetaNumero;
    lColumna: TLabel;
    EColumna: TZetaNumero;
    lCriterioCampo: TLabel;
    CBAlineacion: TZetaKeyCombo;
    lBanda: TLabel;
    CbBanda: TZetaKeyCombo;
    CBNegrita: TCheckBox;
    CBSubrayado: TCheckBox;
    CBItalica: TCheckBox;
    CBComprimido: TCheckBox;
    CBBandaSel: TZetaKeyCombo;
    SmartListEncabezado: TZetaSmartLists_DevEx;
    SmartListPie: TZetaSmartLists_DevEx;
    LBEncabezado: TZetaSmartListBox_DevEx;
    LBPie: TZetaSmartListBox_DevEx;
    dsImpresoras: TDataSource;
    Label1: TLabel;
    RE_REPORTE: TDBComboBox;
    RE_PRINTER: TDBLookupComboBox;
    lbImpresora: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    RE_PIE: TZetaTextBox;
    RE_COLESPA: TZetaDBNumero;
    RE_RENESPA: TZetaDBNumero;
    RE_ANCHO: TZetaDBNumero;
    UDLineasForma: TUpDown;
    UDEncabezado: TUpDown;
    UDDetalle: TUpDown;
    EPAGINAS: TLabel;
    lbCopias: TLabel;
    Label9: TLabel;
    Label6: TLabel;
    RE_HOJA: TZetaDBNumero;
    RE_COPIAS: TZetaDBNumero;
    EPaginaInicial: TZetaNumero;
    UDCopias: TUpDown;
    UDCompensacion: TUpDown;
    UDCompensarCada: TUpDown;
    lTamano: TLabel;
    Label10: TLabel;
    EPaginaFinal: TZetaNumero;
    EPAginasAl: TLabel;
    Bevel1: TBevel;
    eFuentes: TZetaTextBox;
    Label11: TLabel;
    lbPlantilla: TLabel;
    EInicial: TEdit;
    ESeparador: TEdit;
    lbSeparador: TLabel;
    RE_ALTO: TZetaDBNumero;
    TabSheet1: TcxTabSheet;
    GroupBox5: TGroupBox;
    Label12: TLabel;
    eListaReportesSubsecuentes: TDBEdit;
    GroupBox7: TGroupBox;
    Label13: TLabel;
    RE_REPORTE_IS: TDBComboBox;
    RE_PRINTER_IS: TDBLookupComboBox;
    Label14: TLabel;
    RE_FONTSIZ: TDBRadioGroup;
    Panel2: TPanel;
    Image1: TImage;
    Panel3: TPanel;
    Image2: TImage;
    bBusccaReporte_IS: TcxButton;
    bPUERTO: TcxButton;
    BGuardaArchivo: TcxButton;
    bPuerto_IS: TcxButton;
    procedure CBBandaSelChange(Sender: TObject);
    procedure SmartListCamposAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure bFormulaCampoClick(Sender: TObject);
    procedure BBorraCampoClick(Sender: TObject);
    procedure OnCalculaLineas(Sender: TObject);
    procedure OnCalculaLineasUD(Sender: TObject; Button: TUDBtnType);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure bPUERTOClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure UDCompensarCadaClick(Sender: TObject; Button: TUDBtnType);
    procedure CbBandaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure UDCompensacionClick(Sender: TObject; Button: TUDBtnType);
    procedure UDCopiasClick(Sender: TObject; Button: TUDBtnType);
    procedure bBusccaReporte_ISClick(Sender: TObject);
    procedure BPreeliminarClick(Sender: TObject);
    procedure eListaReportesSubsecuentesExit(Sender: TObject);
  private
    FListaSeleccionada : TZetaSmartListBox_DevEx;
    FFuente : TFuente;
    FFastForm : TFastForm;
    FLastEncabezado, FLastDetalle : integer;
    FNumeroConcepto: integer;
    FFCampoCiclo, FFCampoCLetra : TCampoOpciones;
    function GetSmartList: TZetaSmartLists_DevEx;
    function GetCaracPulgada: integer;
    function GetCodigosGenerales: integer;
    function GetCodigosImpresion: string;
    procedure SetCaracPulgada(const iCodigo: integer);
    procedure SetCodigosGenerales(const iCodigo: integer);
    procedure SetCodigosImpresion(const sCodigo: string);
    procedure CalculaLineas(const Modificado : LineaModificada);
    function GetLineas: integer;
    function GetDesFuente: string;
    procedure CalculaRenglonColumna(oCampo: TCampoIRapida);
  protected

    procedure LlenaListas;override;
    procedure LimpiaListas;override;
    procedure PosicionaListas;override;
    procedure IniciaListas;override;

    procedure AgregaCampo;override;
    procedure AgregaListaCampo;override;
    procedure AgregaFormula;override;
    procedure GrabaCampos;override;
    procedure ActualizaCamposObjeto;override;
    procedure ActualizaCampos(const i : integer);override;
    procedure AsignaProcCampos(oProc: TNotifyEvent);override;
    procedure EnabledCampos(const bEnable, bFormula, bAuto: Boolean);override;
    function GetListaCampos : TZetaSmartListBox_DevEx;override;
    procedure AlAgregarCampo( const oDiccion: TDiccionRecord;
                              var oCampo: TCampoOpciones);override;
    procedure AlBorrarDatos(const oEntidad : TipoEntidad);override;
    procedure AlRevisarDatos(const oEntidad : TipoEntidad);override;
    procedure AntesDeEscribirCambios;override;

    property CodigosImpresion : string read GetCodigosImpresion write SetCodigosImpresion;
    property CodigosGenerales : integer read GetCodigosGenerales write SetCodigosGenerales;
    property CaracPulgada : integer read GetCaracPulgada write SetCaracPulgada;

    procedure AntesDeConstruyeSQL; override;
    function GeneraReporte: Boolean;override;
    procedure ImpresionReporte( const Tipo : TipoPantalla );override;
    procedure AgregaSQLCampoRep;override;
    function EsIRSubsecuente: Boolean;
  public
    { Public declarations }
    function PreparaReporte( const lImpresionesSubsecuentes: Boolean = FALSE ): Boolean;
    procedure Connect;override;
    procedure SubsecuentesAbreImpresora;
    procedure SubsecuentesGeneraReporte( subDataset: TDataset );
    procedure SubsecuentesCierraImpresora;
    procedure AgregaSQLCampoRepClear(oAgente: TSQLAgenteClient);
    procedure DespuesDeConstruyeSQL;override;
    function SQLAgenteIR: TSQLAgenteClient;
  end;

var
  ImpresionRapida_DevEx: TImpresionRapida_DevEx;

implementation


uses
    ZetaNetworkBrowser_DevEx,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaDialogo,
    DSistema,
    DReportes,
    ZFuncsCliente,
    ZDiccionTools,
    //FLookupReporte,
    FLookupReporte_DevEx,
    FTressShell,
    FTipoFuentes_DevEx,
    FIRPreliminar_DevEx;

{$R *.DFM}

{ TImpresionRapida }


procedure TImpresionRapida_DevEx.AgregaListaCampo;
 var oCampo : TCampoIRapida;
begin
     oCampo := TCampoIRapida.Create;
     LeeCampoGeneral( TCampoOpciones(oCampo) );
     with oCampo do
     begin
          {Para reportes de Impresion Rapida}
          TCorto := Titulo;
          Fuente := [];
          if CampoRep.FieldByName('CR_BOLD').AsString = 'S' then
             Fuente :=  [ eNegrita ];
          if CampoRep.FieldByName('CR_ITALIC').AsString = 'S' then
             Fuente :=  Fuente + [ eItalica ];
          if CampoRep.FieldByName('CR_SUBRAYA').AsString = 'S' then
             Fuente :=  Fuente + [ eSubrayado ];
          if CampoRep.FieldByName('CR_STRIKE').AsString = 'S' then
             Fuente :=  Fuente + [ eComprimido ];

          Justificacion := TAlignment( CampoRep.FieldByName('CR_ALINEA').AsInteger );

          Columna := CampoRep.FieldByName('CR_SHOW').AsInteger;
          Renglon := CampoRep.FieldByName('CR_SUBPOS').AsInteger;
          Banda := eTipoBanda( CampoRep.FieldByName('CR_COLOR').AsInteger);

          case Banda of
               tbEncabezado : LBEncabezado.Items.AddObject(oCampo.Titulo, oCampo);
               tbDetalle: LBCampos.Items.AddObject(oCampo.Titulo, oCampo);
               tbPiePagina: LBPie.Items.AddObject(oCampo.Titulo, oCampo);
          end;
     end;

end;

procedure TImpresionRapida_DevEx.GrabaCampos;
 procedure Agrega( oLista : TStrings; const iCuantos : integer );
   var i: integer;
 begin
      with oLista do
           for i:= 0 to Count -1 do
           begin
                CampoRep.Append;
                GrabaCampoOpciones( TCampoOpciones(Objects[i]),
                                   tcCampos, i+iCuantos, -1 );
                with TCampoIRapida(Objects[i]) do
                begin
                     with CampoRep do
                     begin
                          FieldByName('CR_BOLD').AsString := zBoolToStr( eNegrita in Fuente );
                          FieldByName('CR_ITALIC').AsString := zBoolToStr( eItalica in Fuente );
                          FieldByName('CR_SUBRAYA').AsString := zBoolToStr( eSubrayado in Fuente );
                          FieldByName('CR_STRIKE').AsString := zBoolToStr( eComprimido in Fuente );
                          FieldByName('CR_ALINEA').AsInteger := Ord( Justificacion );

                          FieldByName('CR_SHOW').AsInteger := Columna;
                          FieldByName('CR_SUBPOS').AsInteger := Renglon;
                          FieldByName('CR_COLOR').AsInteger := Ord(Banda);
                     end;
                end;
                CampoRep.Post;
           end;
 end;

begin
     Agrega( LBEncabezado.Items, 0 );
     Agrega( LBCampos.Items, LBEncabezado.Items.Count );
     Agrega( LBPie.Items, LBEncabezado.Items.Count+LBCampos.Items.Count );
end;

procedure TImpresionRapida_DevEx.ActualizaCamposObjeto;
 var  i : integer;
begin
     inherited;
     i := FListaSeleccionada.ItemIndex;
     if i >= 0 then
     begin
          with TCampoIRapida( FListaSeleccionada.Items.Objects[ i ] ) do
          begin
               Columna := EColumna.ValorEntero;
               Renglon := ERenglon.ValorEntero;

               Fuente := [];
               if CBNegrita.Checked then Fuente := Fuente + [ eNegrita ];
               if CBItalica.Checked then Fuente := Fuente + [ eItalica ];
               if CBSubrayado.Checked then Fuente := Fuente + [ eSubrayado ];
               if CBComprimido.Checked then Fuente := Fuente + [ eComprimido ];

               Justificacion := TAlignment( CBAlineacion.ItemIndex );
               Banda := eTipoBanda(CBBanda.ItemIndex);
          end;
     end;
end;

procedure TImpresionRapida_DevEx.ActualizaCampos(const i : integer);
begin
     inherited;
     AsignaProcCampos( NIL );
     with TCampoIRapida(FListaSeleccionada.Items.Objects[i]) do
     begin
          EColumna.Valor := Columna;
          ERenglon.Valor := Renglon;
          CBNegrita.Checked := eNegrita in Fuente;
          CBItalica.Checked := eItalica in Fuente;
          CBSubrayado.Checked := eSubrayado in Fuente;
          CBComprimido.Checked := eComprimido in Fuente;
          CBAlineacion.ItemIndex := Ord( Justificacion );
          CBBanda.ItemIndex := Ord(Banda);
     end;
     AsignaProcCampos( CampoOpcionesClick );
end;

procedure TImpresionRapida_DevEx.AsignaProcCampos(oProc: TNotifyEvent);
begin
     inherited;
     EColumna.OnExit := oProc;
     ERenglon.OnExit := oProc;

     CBNegrita.OnClick := oProc;
     CBItalica.OnClick := oProc;
     CBSubrayado.OnClick := oProc;
     CBComprimido.OnClick := oProc;
     CBAlineacion.OnClick := oProc;
     CBBanda.OnClick := oProc;
end;

procedure TImpresionRapida_DevEx.EnabledCampos(const bEnable, bFormula,
  bAuto: Boolean);
begin
     inherited;
     lColumna.Enabled := bEnable;
     EColumna.Enabled := bEnable;

     lRenglon.Enabled := bEnable;
     ERenglon.Enabled := bEnable;

     CBNegrita.Enabled := bEnable;
     CBItalica.Enabled := bEnable;
     CBSubrayado.Enabled := bEnable;
     CBComprimido.Enabled := bEnable;

     lCriterioCampo.Enabled := bEnable;
     CBAlineacion.Enabled := bEnable;

     lBanda.Enabled := bEnable;
     CBBanda.Enabled := bEnable;

     if NOT bEnable then
     begin
          AsignaProcCampos( NIL );
          EColumna.Valor := 0;
          ERenglon.Valor := 0;
          CBNegrita.Checked := FALSE;
          CBItalica.Checked := FALSE;
          CBSubrayado.Checked := FALSE;
          CBComprimido.Checked := FALSE;
          CBAlineacion.ItemIndex := 0;
          CBBanda.ItemIndex := 0;
          ETituloCampo.Text := '';
          EFormulaCampo.Text := '';
          EAnchoCampo.Text := '0';
          CBMascaraCampo.Text := '';
          AsignaProcCampos( CampoOpcionesClick );
     end;
end;

procedure TImpresionRapida_DevEx.CBBandaSelChange(Sender: TObject);
begin
     EnabledCampos( FALSE, FALSE, FALSE );
     LBCampos.Visible := eTipoBanda(CbBandaSel.ItemIndex)=tbDetalle;
     LBEncabezado.Visible := eTipoBanda(CbBandaSel.ItemIndex)=tbEncabezado;
     LBPie.Visible := eTipoBanda(CbBandaSel.ItemIndex)=tbPiePagina;

     case eTipoBanda(CbBandaSel.ItemIndex) of
          tbDetalle:
          begin
               FListaSeleccionada := LBCampos;
               SmartListCampos.SelectEscogido( 0 );
          end;
          tbEncabezado:
          begin
               FListaSeleccionada := LbEncabezado;
               SmartListEncabezado.SelectEscogido( 0 );
          end;
          tbPiePagina:
          begin
               FListaSeleccionada := LBPie;
               SmartListPie.SelectEscogido( 0 );
          end;
     end;
end;

procedure TImpresionRapida_DevEx.SmartListCamposAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     FListaSeleccionada := TZetaSmartLists_DevEx(Sender).ListaDisponibles;
     if Objeto <> NIL then
     begin
          ActualizaCampos( FListaSeleccionada.ItemIndex );
          TZetaSmartLists(Sender).Control := ETituloCampo;
     end
     else EnabledCampos( FALSE, FALSE, FALSE );
end;

function TImpresionRapida_DevEx.GetListaCampos: TZetaSmartListBox_DevEx;
begin
     Result := FListaSeleccionada
end;

procedure TImpresionRapida_DevEx.LimpiaListas;
begin
     if fListaSeleccionada <> NIL then
     begin
          fListaSeleccionada.Clear;
          fListaSeleccionada := NIL;
     end;

     FListaSeleccionada := LBEncabezado;

     inherited;
     LBEncabezado.Items.Clear;
     LBPie.Items.Clear;
end;

procedure TImpresionRapida_DevEx.PosicionaListas;
begin
     inherited;
     CBBandaSel.Itemindex := 0;
     CBBandaSelChange(nil);
     SmartListEncabezado.SelectEscogido( 0 );
end;


procedure TImpresionRapida_DevEx.bFormulaCampoClick(Sender: TObject);
begin
     with FListaSeleccionada do
          with TCampoIRapida( Items.Objects[ ItemIndex ] ) do
          begin
               if ConstruyeFormula( TcxCustomMemo(eFormulaCampo) ) then
               begin
                    if Calculado >= 0 then
                    begin
                         Formula := eFormulaCampo.Text;
                         Calculado := -1;
                         TipoCampo := tgAutomatico;
                         Mascara := '';
                         SmartListCampos.SelectEscogido( ItemIndex );
                    end;
                    CampoOpcionesClick( Sender );
               end;
          end;
end;

procedure TImpresionRapida_DevEx.AlAgregarCampo( const oDiccion: TDiccionRecord;
                                           var oCampo: TCampoOpciones);
begin
     oCampo := TCampoIRapida.Create;
     DiccionToCampoMaster(oDiccion,oCampo);
     with TCampoIRapida(oCampo), oDiccion do
     begin
          Titulo := TCorto;
          Mascara := DI_MASCARA;
          Ancho := DI_ANCHO;
          Columna := 1;
          Renglon := 1;
          Banda := eTipoBanda(CBBandaSel.ItemIndex);
          if TipoCampo in [tgFloat, tgNumero] then
             Justificacion := taRightJustify;
     end;
     CalculaRenglonColumna( TCampoIRapida(oCampo) );
end;

procedure TImpresionRapida_DevEx.AlBorrarDatos(const oEntidad : TipoEntidad);
begin
     Inherited;
     BorraDatos( LBEncabezado.Items, oEntidad, 0 );
     BorraDatos( LBPie.Items, oEntidad, 0 );
end;

procedure TImpresionRapida_DevEx.AlRevisarDatos(const oEntidad : TipoEntidad);
begin
     Revisa( LBEncabezado.Items, oEntidad, 'Campos-Encabezado', 0 );
     Revisa( FListaSeleccionada.Items, oEntidad, 'Campos-Detalle', 0 );
     Revisa( LBPie.Items, oEntidad, 'Campos-Pie', 0 );
     inherited;
end;

procedure TImpresionRapida_DevEx.IniciaListas;
begin
     inherited;
     LBEncabezado.OnDblClick := LbListaDblClick;
     LBPie.OnDblClick := LbListaDblClick;
end;

function TImpresionRapida_DevEx.GetSmartList : TZetaSmartLists_DevEx;
begin
     Result := NIL;
     case eTipoBanda(CBBandaSel.ItemIndex) of
          tbEncabezado : Result :=SmartListEncabezado;
          tbDetalle: Result :=SmartListCampos;
          tbPiePagina: Result :=SmartListPie;
     end;
end;

procedure TImpresionRapida_DevEx.AgregaCampo;
 var oCampo : TCampoIRapida;
     oDiccion : TDiccionRecord;
begin
     oCampo := NIL;
      if AntesDeAgregarDato( Entidad, TRUE, TRUE, oDiccion, GetSmartList ) then
     begin
          AlAgregarCampo(oDiccion,TCampoOpciones(oCampo) );
          if oCampo <> NIL then
             DespuesDeAgregarDato( oCampo,
                                   GetSmartList,
                                   BBorraCampo,
                                   ETituloCampo );
          Modo := dsEdit;
     end;
end;

procedure TImpresionRapida_DevEx.BBorraCampoClick(Sender: TObject);
begin
     //NO SE DEBE MANDAR LLAMAR EL INHERITED
     //POR QUE SE BORRARIA DOS VECES LOS DATOS
     //inherited;
     BorraDato( GetSmartList, bAgregaCampo, bBorraCampo, ETituloCampo );
     if GetListaCampos.Items.Count = 0 then
        EnabledCampos( FALSE, FALSE, FALSE );
end;

procedure TImpresionRapida_DevEx.CalculaRenglonColumna( oCampo : TCampoIRapida );
 var i: integer;
begin
     i := FListaSeleccionada.Items.Count;
     if i >= 0 then
     begin
          with oCampo do
               if i=0 then
               begin
                    if eTipoBanda( CbBandaSel.ItemIndex ) = tbPiePagina then
                       Renglon := UDEncabezado.Position + UDDetalle.Position + 1
                    else  Renglon := 1;
                    Columna := 1;
               end
               else
               begin
                    Columna := TCampoIRapida( FListaSeleccionada.Items.Objects[ i-1 ] ).Columna +
                               TCampoIRapida( FListaSeleccionada.Items.Objects[ i-1 ] ).Ancho + 1;
                    Renglon := TCampoIRapida( FListaSeleccionada.Items.Objects[ i-1 ] ).Renglon;
               end;
      end;

end;

procedure TImpresionRapida_DevEx.AgregaFormula;
 var oFormula : TCampoIRapida;
begin
     oFormula := TCampoIRapida.Create;
     with oFormula do
     begin
         Entidad := enFormula;
         Formula := '';
         Titulo := K_TITULO;
         Ancho := 10;
         TipoCampo := tgAutomatico;
         Calculado := -1;
         Columna := 1;
         Renglon := 1;

         CalculaRenglonColumna(oFormula);

     end;

     with GetListaCampos do
     begin
          Items.AddObject( oFormula.Titulo, oFormula );
          GetSmartList.SelectEscogido( Items.Count - 1 );
          Modo := dsEdit;
     end;
     ETituloCampo.SetFocus
end;

procedure TImpresionRapida_DevEx.BGuardaArchivoClick(Sender: TObject);
begin
     inherited;
     if DialogoTipoFuentes( fFuente ) then
     begin
          eFuentes.Caption := GetDesFuente;
          Modo := dsEdit;
     end;
end;

procedure TImpresionRapida_DevEx.AntesDeEscribirCambios;
begin
     inherited;
     with Reporte do
     begin                       
          FieldByName('RE_GENERAL').AsString := CodigosImpresion;
          FieldByName('RE_COLNUM').AsInteger := CodigosGenerales;
          FieldByName('RE_MAR_SUP').AsInteger := CaracPulgada;
     end;
end;

procedure TImpresionRapida_DevEx.LlenaListas;
begin
     inherited;

     if Modo <> dsInsert then
     begin
          CodigosImpresion := Reporte['RE_GENERAL'];
          CodigosGenerales := Reporte['RE_COLNUM'];
          CaracPulgada := Reporte['RE_MAR_SUP'];
     end
     else fFuente := [eLineas6,eAncho10];

     Reporte.Edit;
     if Reporte.FieldByName('RE_REPORTE').AsString = K_TEMPLATE then
        Reporte.FieldByName('RE_REPORTE').AsString := 'LPT1';

     if ( Reporte.FieldByName('RE_PRINTER').AsString = '') OR
        ( Reporte.FieldByName('RE_PRINTER').AsString = K_IMP_DEFAULT ) then
        with dmSistema.cdsImpresoras do
        begin
             First;
             Reporte.FieldByName('RE_PRINTER').AsString := FieldByName('PI_NOMBRE').AsString;
        end;

     Reporte.Post;

     eFuentes.Caption := GetDesFuente;

     UDLineasForma.Position := Reporte.FieldByName('RE_ANCHO').AsInteger;
     UDEncabezado.Position := Reporte.FieldByName('RE_RENESPA').AsInteger;
     UDDetalle.Position := Reporte.FieldByName('RE_COLESPA').AsInteger;
     UDCompensarCada.Position := Reporte.FieldByName('RE_ALTO').AsInteger;
     UDCompensacion.Position := Reporte.FieldByName('RE_HOJA').AsInteger;
     UDCopias.Position := Reporte.FieldByName('RE_COPIAS').AsInteger;

     CalculaLineas(liFormas);
end;

function TImpresionRapida_DevEx.GetCaracPulgada: integer;
begin
     Result := 0;
     if eAncho10 in fFuente then Result := 0;
     if eAncho12 in fFuente then Result := 1;
     if eComprimido in fFuente then Result := 2;
end;

function TImpresionRapida_DevEx.GetCodigosGenerales: integer;
begin
     Result := 0;
     if eNegrita    in fFuente then Result := Result + 1;
     if eItalica    in fFuente then Result := Result + 2;
     if eSubrayado  in fFuente then Result := Result + 4;
     if eLandscape  in fFuente then Result := Result + 8;
     if eEject      in fFuente then Result := Result + 16;
     if eReset      in fFuente then Result := Result + 32;
     if eLineas6    in fFuente then Result := Result + 64;
     if eLineas8    in fFuente then Result := Result + 128;
end;

function TImpresionRapida_DevEx.GetCodigosImpresion: string;
 procedure GetAscii( sCodigo : string );
  var i : integer;
      sChar : string;
 begin
      i := Pos( ',', sCodigo );
      if i > 0 then
      begin
           while i > 0 do
           begin
                sChar := Copy( sCodigo, 1, i - 1 );
                if StrLleno( sChar ) then
                begin
                     Result := Result + CHR( StrToInt( Trim( sChar ) ) );
                     Delete( sCodigo, 1, i );
                     i := Pos( ',', sCodigo );
                     if ( i = 0 ) AND ( StrLleno( sCodigo ) ) then  Result := Result + CHR( StrToInt( Trim( sCodigo ) ) );
                end;
           end;
      end
      else if StrLleno( sCodigo ) then
      begin
           Result := Result + CHR( StrToInt( Trim( sCodigo ) ) );
      end;
 end;
 begin
     Result := '';
     GetAscii( EInicial.Text );
     Result := PadRCar( Result, 20, CHR(255) );
     GetAscii( ESeparador.Text );
     Result := PadRCar( Result, 30, CHR(255)  );
end;

procedure TImpresionRapida_DevEx.SetCaracPulgada(const iCodigo: integer);
begin
     case iCodigo of
          0 : fFuente := fFuente + [ eAncho10 ];
          1 : fFuente := fFuente + [ eAncho12 ];
          2 : fFuente := fFuente + [ eComprimido ];
     end;
end;

procedure TImpresionRapida_DevEx.SetCodigosGenerales(const iCodigo: integer);
begin
     fFuente := [];
     if ( iCodigo and 1 ) > 0 then fFuente := fFuente +  [ eNegrita ];
     if ( iCodigo and 2 ) > 0 then fFuente := fFuente +  [ eItalica ];
     if ( iCodigo and 4 ) > 0 then fFuente := fFuente +  [ eSubrayado ];
     if ( iCodigo and 8 ) > 0 then fFuente := fFuente +  [ eLandscape ];
     if ( iCodigo and 16 ) > 0 then fFuente := fFuente +  [ eEject ];
     if ( iCodigo and 32 ) > 0 then fFuente := fFuente +  [ eReset ];
     if ( iCodigo and 64 ) > 0 then fFuente := fFuente +  [ eLineas6 ];
     if ( iCodigo and 128 ) > 0 then fFuente := fFuente +  [ eLineas8 ];
end;

procedure TImpresionRapida_DevEx.SetCodigosImpresion(const sCodigo: string);
 var iLength : integer;
 function GetAscii( const iInicio, iFin : integer ) : String;
  var sTexto : string;
      i : integer;
 begin
      sTexto := '';

      for i := iInicio to iFin do
      begin
           if ( i > iLength ) then Break
           else if ( sCodigo[ i ] <> CHR(255) ) AND
                   ( sCodigo[ i ] <> CHR(160) ) then
                   sTexto := sTexto + ',' + IntToStr( Ord( sCodigo[ i ] ) );
      end;
      if StrVacio( sTexto ) then Result := ''
      else Result := Copy( sTexto, 2, Length( sTexto ) );
 end;

begin
     EInicial.Text := '';
     ESeparador.Text := '';
     if StrLleno( sCodigo ) then
     begin
          iLength := Length( sCodigo );
          EInicial.Text := GetAscii( 1, 20 );
          ESeparador.Text := GetAscii( 21, 30 );
     end;
end;

function TImpresionRapida_DevEx.GetLineas : integer;
begin
     Result := UDLineasForma.Position;
end;


function TImpresionRapida_DevEx.GetDesFuente: string;
begin
     Result := '';
     if eNegrita in fFuente then Result := Result + 'Negrita';
     if eItalica in fFuente then Result := Result + ' | It�lica';
     if eSubrayado in fFuente then Result := Result + ' | Subrayado';
     if eLandscape in fFuente then Result := Result + ' | Hoja Horizontal';
     if eEject in fFuente then Result := Result + ' | Salto de P�gina';
     if eReset in fFuente then Result := Result + ' | Reset de Impresora';

     if eLineas6 in fFuente then Result := Result + ' | 6 L�neas';
     if eLineas8 in fFuente then Result := Result + ' | 8 L�neas';

     if eAncho10 in fFuente then Result := Result + ' | 10 Caracteres';
     if eAncho12 in fFuente then Result := Result + ' | 12 Caracteres';
     if eComprimido in fFuente then Result := Result + ' | 17 Caracteres';
end;

procedure TImpresionRapida_DevEx.Connect;
begin
     dmSistema.cdsImpresoras.Conectar;
     inherited;
end;


procedure TImpresionRapida_DevEx.bPUERTOClick(Sender: TObject);
begin
     inherited;
     with Reporte do
     begin
          Edit;
          FieldByName('RE_REPORTE').AsString := ZetaNetworkBrowser_DevEx.GetPrinterPort( FieldByName('RE_REPORTE').AsString );
     end;
end;

{*************************************************}
{*************** GENERACION DE SQL ***************}
{*************************************************}
procedure TImpresionRapida_DevEx.AgregaSQLCampoRepClear(oAgente: TSQLAgenteClient);
 var
    oVariant: OleVariant;
begin
     SQLAgente.Clear;
     AgregaSQLCampoRep;
     oVariant := oAgente.AgenteToVariant;
     SQLAgente.VariantToAgente( oVariant );
end;

procedure TImpresionRapida_DevEx.AgregaSQLCampoRep;
 var i: integer;

 procedure AgregaSQLColumnasIR(oCampo: TCampoOpciones);
  function EsFormulaPagina(sFormula:string):Boolean;
  begin
       sFormula := UpperCase(sFormula);
       Result := (sFormula = 'PAGINA') OR
                 (sFormula = 'PAGINA()') OR
                 (sFormula = 'PAGINA( )');
  end;

  function EsFormulaCicloLetra(sFormula:string):Boolean;
  begin
       {la funcion CICLOLETRA hace lo que hace la funcion PROT, en una
       primera version NO  va a tener parametros, en un futuro, si
       va a poder recibir parametros.}
       Result := Pos( 'CICLOLETRA', UpperCase(sFormula) ) > 0;
  end;

  function EsFormulaCiclo(sFormula:string):Boolean;
   var iPos: integer;
  begin
       {la funcion CICLO en DOS recibe CICLO(NumeroConcepto, Monto a Dividir)
       despues se le pega la funcion 'C('}
       sFormula := UpperCase(Trim(sFormula));
       Result := Copy(sFormula, 1, 6 ) = 'CICLO(';
       if Result then
       begin
            //CICLO( 44, 50 )
            iPos := Pos(',',sFormula);
            if iPos > 0 then
            begin
                 FNumeroConcepto := StrToIntDef( Trim(Copy( sFormula, 7, iPos - 7 )), 0 );
                 FMontoCiclo := StrToIntDef( Trim(Copy( sFormula, iPos + 1, Length(sFormula)- iPos -1 )), 0 );
                 if (FNumeroConcepto <= 0 ) or ( FMontoCiclo <=0 ) then
                    Raise Exception.Create('Los Par�metros de la Funci�n CICLO(Concepto,Monto), NO son V�lidos');
            end
            else
                Raise Exception.Create('No se Especificaron los Par�metros Correctos para la Funci�n CICLO(Concepto,Monto)');
       end;
  end;

 begin
      with oCampo do
      begin
           if EsFormulaPagina(Formula) then
              oCampo.PosAgente := SQLAgente.AgregaColumna( EntreComillas(K_PAGINA),FALSE, Entidad,tgTexto,
                                                           5,'',ocNinguno,FALSE,0)
           else if EsFormulaCiclo(Formula) then
           begin
                oCampo.PosAgente := SQLAgente.AgregaColumna( EntreComillas(K_CICLO),FALSE, Entidad,tgTexto,
                                                             5,'',ocNinguno,FALSE,0);
                FPosCiclo := SQLAgente.AgregaColumna( Format('C(%d)', [FNumeroConcepto] ), FALSE, Entidad,tgFloat,
                                                      5,'',ocNinguno,FALSE,0);
                FFCampoCiclo := oCampo;
           end
           else if EsFormulaCicloLetra(Formula) then
           begin
                oCampo.PosAgente := SQLAgente.AgregaColumna( EntreComillas(K_CICLOLETRA),FALSE, Entidad,tgTexto,
                                                             5,'',ocNinguno,FALSE,0);
                FFCampoCLetra := oCampo;
           end
           else
               AgregaSQLColumnas(oCampo);
      end;
 end;

begin
     FMontoCiclo := 0;

     for i:=0 to LBEncabezado.Items.Count -1 do
         AgregaSQLColumnasIR(TCampoOpciones(LBEncabezado.Items.Objects[i]));
     for i:=0 to LBPie.Items.Count -1 do
         AgregaSQLColumnasIR(TCampoOpciones(LBPie.Items.Objects[i]));
     for i:=0 to LBCampos.Items.Count -1 do
         AgregaSQLColumnasIR(TCampoOpciones(LBCampos.Items.Objects[i]));
     for i:=0 to LBGrupos.Items.Count -1 do
     begin
         { EZM: El Grupo de Empresa no tiene FORMULA }
         if ( i = 0 ) then
            TGrupoOpciones(LBGrupos.Items.Objects[i]).Formula := '';
         AgregaSQLGrupos(TGrupoOpciones(LBGrupos.Items.Objects[i]));
     end;
     for i:=0 to LBOrden.Items.Count -1 do
         AgregaSQLOrdenes(TOrdenOpciones(LBOrden.Items.Objects[i]));
     
     for i:=0 to LBFiltros.Items.Count -1 do
         AgregaSQLFiltros(TFiltroOpciones(LBFiltros.Items.Objects[i]));
end;

procedure TImpresionRapida_DevEx.FormDestroy(Sender: TObject);
begin
     with LBCampos do
          while Items.Count > 0 do
          begin
                TCampoOpciones(Items.Objects[0]).Free;
                Items.Delete(0);
          end;
     with LBEncabezado do
          while Items.Count > 0 do
          begin
                TCampoOpciones(Items.Objects[0]).Free;
                Items.Delete(0);
          end;
     with LBPie do
          while Items.Count > 0 do
          begin
                TCampoOpciones(Items.Objects[0]).Free;
                Items.Delete(0);
          end;

     FFastForm.Free;
     //FreeAndNil( FormaLookupReporte );
     FreeAndNil( FormaLookupReporte_DevEx );
     inherited;
end;

function TImpresionRapida_DevEx.PreparaReporte( const lImpresionesSubsecuentes: Boolean = FALSE ): Boolean;
begin
     //Result := TRUE;
     if FFastForm = NIL then
        FFastForm := TFastForm.Create;

     IF FormaIRPreliminar_DevEx = NIL then
     begin
        FormaIRPreliminar_DevEx := TFormaIRPreliminar_DevEx.Create( Self );
        FormaIRPreliminar_DevEx.FastForm := FFastForm;
     end;

     with FFastForm do
     begin
        FEsPreview        := FTipoPantalla = tgPreview;
        if NOT lImpresionesSubsecuentes then
           FFastPuerto       := RE_REPORTE.Text;
        { Tama�o de Forma }

        FLineasForma      := RE_ANCHO.ValorEntero;
        FLineasEncabezado := RE_RENESPA.ValorEntero;
        FLineasDetalle    := RE_COLESPA.ValorEntero;
        FCompensaCada     := RE_ALTO.ValorEntero;
        FLineasCompensa   := RE_HOJA.ValorEntero;
        FNumCopias        := iMax( RE_COPIAS.ValorEntero, 1 );
        FPagInicial       := EPaginaInicial.ValorEntero;
        FPagFinal         := EPaginaFinal.ValorEntero;
        FCampoCiclo       := FFCampoCiclo;
        FCampoCLetra      := FFCampoCLetra;

        { C�digos de Control de Impresora }
        FCodigoIni        := EInicial.Text;
        FCodigoFin        := ESeparador.Text;
        FFuente           := self.FFuente;

        PreparaReporte( dsResultados.Dataset, LBEncabezado.Items, LBCampos.Items, LBPie.Items, LBGrupos.Items );
     end;

     if lImpresionesSubsecuentes then
     begin
          Result := TRUE;
     end
     else
     begin
          if FTipoPantalla = tgPreview then
          begin
             FormaIRPreliminar_DevEx.ShowModal;
             Result := ( FormaIRPReliminar_DevEx.ModalResult = mrOK );
             FFastForm.FEsPreview := FALSE;
          end
          else
             Result := TRUE;
     end;
end;

function TImpresionRapida_DevEx.GeneraReporte: Boolean;
var
    lImprime : Boolean;
begin
     Result := TRUE;
     lImprime := PreparaReporte;

     if ( lImprime ) then
        FFastForm.GeneraReporte;
end;

procedure TImpresionRapida_DevEx.AntesDeConstruyeSQL;
begin
     { Se necesita porque el m�todo es Virtual Abstract }
end;


procedure TImpresionRapida_DevEx.DespuesDeConstruyeSQL;
begin
     inherited;
     ModificaListaCampos( LBEncabezado.Items );
     ModificaListaCampos( LBCampos.Items );
     ModificaListaCampos( LBPie.Items );
end;

function TImpresionRapida_DevEx.SQLAgenteIR: TSQLAgenteClient;
begin
     Result := SQLAgente;
end;


{ TFastForm }

procedure TFastForm.AbreImpresora;
var
    eStatus : eStatusImpresora;
begin
     eStatus := GetStatusImpresora;
     if ( eStatus <> siLista ) then
        ErrordeImpresion( eStatus );

     try
        AssignFile( FImpresora, FFastPuerto );
        ReWrite( FImpresora );
        MandaCodigo( GetCodigosASCII( FCodigoIni )); // Manda Codigos iniciales definidos por el usuario
        MandaFuenteInicial;                         // Manda Fuente inicial definida por el usuario
     except
           On E:EInOutError do
              ExcepcionImpresion(E.ErrorCode);
           On E:Exception do
              ErrorDeImpresion( siOffLine );
     end;

end;
procedure TFastForm.ExcepcionImpresion(const ErrorCode:integer);
begin
     case ErrorCode of
          100: raise EPrinterError.Create('Error Al Leer Disco');
          101: raise EPrinterError.Create('Error Al Escribir Disco. Disco sin Espacio');
          102: raise EPrinterError.Create('Impresora no Asignada al Puerto');
          103: raise EPrinterError.Create('Puerto No V�lido');
          104: raise EPrinterError.Create('Impresora no est� Lista para Imprimir');
          105: raise EPrinterError.Create('Impresora no est� Lista para Salida');
          106: raise EPrinterError.Create('Invalid numeric format');
     end;
end;

procedure TFastForm.CierraImpresora;
begin
     try
        MandaFuenteFinal;
        CloseFile( FImpresora );
     except
           On E:EInOutError do
              ExcepcionImpresion(E.ErrorCode);
           On E:Exception do
              ErrorDeImpresion( siOffLine );
     end;
end;

constructor TFastForm.Create;
begin
    FEncabezado := TStringList.Create;
    FDetalle    := TStringList.Create;
    FPiePagina  := TStringList.Create;
    fNavUltimo := TRUE;
end;

destructor TFastForm.Destroy;
begin
  FEncabezado.Free;
  FDetalle.Free;
  FPiePagina.Free;
  FRenglones := NIL;
  FColumnas  := NIL;
  FLlaves    := NIL;
  FLlavesPos := NIL;
  inherited;
end;

function TFastForm.EscribeDetalle : Boolean;

  procedure EscribeUnDetalle;
  var
    i : Integer;
  begin
    with FDetalle do
      if ( Count > 0 ) then
      begin
        for i := 0 to Count-1 do
            if (TCampoIRapida( Objects[i] ).Banda = tbDetalle) or
               (TCampoIRapida( Objects[i] ).Renglon = FLineasEncabezado + FRowDetalle +1 ) then
            EscribeDato( TCampoIRapida( Objects[i] ), FLineasEncabezado + FRowDetalle );

        // En lugar de sumar 1 a FRowDetalle, se le suma el Rengl�n del �ltimo elemento
        // del detalle. Esto permite tener m�s de 1 rengl�n por cada registro de detalle.
        FRowDetalle := FRowDetalle + TCampoIRapida( Objects[ Count-1 ] ).Top;
      end;
  end;
  var i: integer;
begin  { EscribeDetalle }
    Result := TRUE;
    FRowDetalle := 0;

    if ( FLineasDetalle > 0 ) and ( FDetalle.Count > 0 ) then
      with FDataset do
      begin
          AsignaLlaves;
          while ( NOT EOF ) AND MismoDetalle do
          begin
               if ( FRowDetalle > ( FLineasDetalle - 1 )) then
               begin
                    if FEsPreview then
                    begin
                         ZReportTools.IncrementaPagina;
                         Result := FALSE; // Para que no imprima el pie de P�gina
                         //Exit;            // Para que lo muestre en el siguiente Memo
                    end
                    else
                    begin
                         ImprimeRenglones;
                         EscribeEncabezado;
                         FRowDetalle := 0;
                    end;
               end;
               if Result then
                  EscribeUnDetalle;
               Next;
          end;

          if ( NOT EOF ) then Prior;  { El Pie de P�gina lo necesita en el mismo Grupo }

          {En este punto, se revisan los datos que no alcanzaron a imprimirse
          despues de que el ultimo detalle se imprimio}
          with FDetalle do
          if ( Count > 0 ) then
          begin
               for i := 0 to Count-1 do
                   with TCampoIRapida( Objects[i] ) do
                        if (Banda <> tbDetalle) AND
                           (Renglon > FLineasEncabezado + FRowDetalle ) then
                           EscribeDato( TCampoIRapida( Objects[i] ), Renglon-1 );
          end;
      end;
end;

procedure TFastForm.EscribeEncabezado;
var
    i : Integer;
begin
     ZReportTools.IncrementaPagina;
     LimpiaRenglones;
     with FEncabezado do
         for i := 0 to Count-1 do
             EscribeDato( TCampoIRapida( Objects[i] ), 0 );
end;

procedure TFastForm.ErrorDeImpresion(const eStatus: eStatusImpresora);
var
    sError : String;
begin
    sError := '';
    case eStatus of
        siOffLine  : sError := 'La impresora est� fuera de l�nea';
        siSinPapel : sError := 'La impresora se ha quedado sin papel';
        siApagada  : sError := 'La impresora est� apagada o desconectada';
        else
                     sError := 'Error desconocido en impresora'
    end;
    raise EPrinterError.Create( sError );
end;

procedure TFastForm.EscribeDato(oColumna: TCampoIRapida; const nOffset : Integer );
var
    nRenglon : Integer;
    //sRenglon : String; //OLD
    sRenglon: AnsiString;
    nColumna : Integer;
    nAnchoDato : Integer;
    sDato : {$ifdef TRESS_DELPHIXE5_UP}AnsiString{$else}String{$endif};

{***(@am): Funcion necesaria para poder seguir utilizando CharToOEM,
            esta funcion permite que los caracteres latinos salgan bien
            al imprimir***}
function StringToOem(const S: String): AnsiString;
var temp:boolean;
begin
  SetLength(Result, Length(S));
  if S <> '' then begin
    temp:= CharToOem({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(S), PAnsiChar(Result));
  end;
end;
{***}

begin
    with oColumna do
    begin
      nRenglon := Top + nOffset;
      if ( nRenglon >= 1 ) and ( nRenglon <= FLineasForma ) then
      begin
        sRenglon      := FRenglones[ nRenglon ];
        nColumna      := FColumnas[ nRenglon ];
        sDato         := AsTexto( FDataSet );
        if NOT FEsPreview and StrLleno(sDato) then
           //CharToOEM( PChar( sDato ),PChar( sDato ) ); //OLD
           sDato := StringToOem(sDato); {***La funcion CharToOEM utiliza un AnsiChar***}
        nAnchoDato    := Length( sDato );

        if ( Columna > nColumna ) then
            // Rellena hueco con Espacios a la izquierda
            sDato := Space( Columna-nColumna ) + sDato
        else if ( Columna < nColumna ) then
            // Corta los primeros caracteres del dato pues el campo a su izquierda lo 'invadi�'
            sDato := Copy( sDato, nColumna-Columna+1, MAXINT );

        if not FEsPreview then
            sDato := PrendeFuente( Fuente ) + sDato + ApagaFuente( Fuente );
        FRenglones[ nRenglon ] := sRenglon + sDato;
        FColumnas[ nRenglon ] := Columna + nAnchoDato
      end;
    end;
end;

{procedure TFastForm.AsignaTopLista( oLista : TStrings );
 var i : integer;
begin
     with oLista do
          for i:=0 to Count - 1 do
              TCampoIRapida(Objects[i]).Top := TCampoIRapida(Objects[i]).Renglon;
end;}

procedure TFastForm.AsignaObjetos(FLista,oLista : TStrings);
 var i: integer;
begin
     with oLista do
          for i:=0 to Count - 1 do
              with TCampoIRapida(Objects[i]) do
              begin
                   Top := Renglon;
                   if (Renglon > FLineasEncabezado) then
                   begin
                        if Renglon < (FLineasEncabezado + FLineasDetalle) then
                        begin
                             Top := 1;
                             FDetalle.AddObject('',TCampoIRapida(Objects[i]))
                        end
                        else FPiePagina.AddObject('',TCampoIRapida(Objects[i]));
                   end
                   else
                       FLista.AddObject('',TCampoIRapida(Objects[i]))
              end;
end;

procedure TFastForm.AsignaDetalle(FLista,oLista : TStrings);
var i: integer;
begin
     with oLista do
          for i:=0 to Count - 1 do
          begin
               with TCampoIRapida(Objects[i]) do
               begin
                    Top := Renglon;
                    FLista.AddObject('',TCampoIRapida(Objects[i]));
               end;
          end;
end;

procedure TFastForm.PreparaReporte( oDataSet : TDataset; oEncabezado, oDetalle, oPiePagina, oGrupos : TStrings );
begin
     FDataset := oDataSet;
     FColumnasForma := 0;

     FEncabezado.Clear;
     FDetalle.Clear;
     FPiePagina.Clear;
     {
     AsignaTopLista(oEncabezado);
     AsignaTopLista(oDetalle);
     AsignaTopLista(oPiePagina);
     }
     AsignaObjetos(FEncabezado,oEncabezado);
     AsignaDetalle(FDetalle,oDetalle);
     AsignaObjetos(FPiePagina,oPiePagina);

     OrdenaLista( FEncabezado, oEncabezado );
     OrdenaLista( FDetalle, oDetalle );
     OrdenaLista( FPiePagina, oPiePagina );

     PreparaGrupos( oGrupos );
     PreparaRenglones;
     GetCodigosImpresora;
end;

procedure TFastForm.EscribeUnaForma;
begin
    EscribeEncabezado;
    if EscribeDetalle then
        EscribePiePagina;

    if FEsPreview OR ((GetPagina>=FPagInicial) AND (GetPagina<=FPagFinal)) then
       ImprimeRenglones;
end;

procedure TFastForm.EscribeUnPreliminar;
begin
    with FormaIRPreliminar_DevEx do
    begin
        MemoPreliminar.Visible := FALSE;
        MemoPreliminar.Lines.Clear;
        EscribeUnaForma;       
        FDataSet.Next;
        MemoPreliminar.Visible := TRUE AND fNavUltimo;
    end;
end;

procedure TFastForm.IniciaPreliminar;
begin
     ZReportTools.InicializaPagina;
     //FPagina := 0;
     FDataSet.First;
     EscribeUnPreliminar;
end;

function TFastForm.UltimoPreliminar : integer;
begin
     {with FDataSet do
     begin
          Result := RecordCount;
          Last;
          EscribeUnPreliminar;
     end;}
     fNavUltimo := FALSE;
     while SiguientePreliminar do
           SiguientePreliminar;
     Result := GetPagina;
     fNavUltimo := TRUE;
     FormaIRPreliminar_DevEx.MemoPreliminar.Visible := TRUE;
end;

function TFastForm.SiguientePreliminar : Boolean;
begin
    with FDataSet do
    begin
        Result := not Eof;
        
        if ( Result ) then
            EscribeUnPreliminar;
    end;
end;

procedure TFastForm.GeneraReporte;
begin
     ZReportTools.InicializaPagina;
     FPaginaCopias := 0;
     try
        AbreImpresora;
          with FDataset do
          begin
               First;
               while ( NOT EOF ) do
               begin
                    EscribeUnaForma;
                    Next;
               end;
          end;
          MandaFuenteFinal;
     finally
            CierraImpresora;
     end;
end;

procedure TFastForm.SubsecuentesAbreImpresora;
begin
     ZReportTools.InicializaPagina;
     FPaginaCopias := 0;
     AbreImpresora;
end;

procedure TFastForm.SubsecuentesGeneraReporte( subDataset: TDataset );
begin
     FDataset := subDataset;
     EscribeUnaForma;
end;

procedure TFastForm.SubsecuentesCierraImpresora;
begin
     MandaFuenteFinal;
     CierraImpresora;
end;

function TFastForm.GetStatusImpresora: eStatusImpresora;
var
   Pto : Word;
   Rdo : Byte;
   Confirmado : Boolean;
   TextoError : string;

   function PrinterStatus( Pto: Word ): integer;
   asm
     mov ah, 2    // function 2 - returns status of port
     mov dx, Pto    // lpt1 = 0, lpt2 = 1 etc
     int $17      // status in ah
     mov al, ah
     and eax, $FF // status now in eax with top 24 bits cleared
   end;

begin
     if ( Win32Platform = VER_PLATFORM_WIN32_WINDOWS  ) and
        (UpperCase(Copy(FFastPuerto,1,3))='LPT') then
     begin // ES WINDOWS 95
          Result := siDesconocido;
          Confirmado := False;
          while (Result <> siLista) and (not Confirmado) do
          begin
               Pto := StrToInt(FFastPuerto[Length(FFastPuerto)]) - 1;
               Rdo := PrinterStatus( Pto );
{
               asm
                  MOV  DX,Pto
                  MOV  AX,$0200  //AH := $02 : Leer el estado de la impresora
                  INT  $17
                  MOV  Rdo,AH    // Guarda el estado en AL
               end;
}
               if Rdo = 144 then
                  Result := siLista
               else
               begin
                    Result := siDesconocido;
                    TextoError := 'La impresora tiene un problema desconocido (' + IntToStr(Rdo) + '). Solucione el problema y reintente.';
               end;
               if  ( Result <> siLista ) then
               begin
                    if not zRetry('', TextoError,0, mbRetry) then
                           Confirmado := True;
               end;
          end;
     end
     else // WINDOWS NT , � Como se averigua si esta lista ?
          Result := siLista; // Aunque puede no estar lista
end;

procedure TFastForm.MandaCodigo( const sCodigo: String );
begin
     if not FEsPreview then
       Write( FImpresora, sCodigo );
end;

procedure TFastForm.MandaFuenteFinal;
begin
     MandaCodigo( FCodigosImpresora.Reset ); // Resetea Impresora
     MandaCodigo( FCodigosImpresora.Ancho10 );
     MandaCodigo( GetCodigosASCII( FCodigoFin )); // Manda codigos iniciales definidos por el usuario
end;

procedure TFastForm.MandaFuenteInicial;
begin                                   
     MandaCodigo( PrendeFuente( FFuente ));
     with FCodigosImpresora do
     begin
          if eEject     in FFuente then MandaCodigo( Eject );
          if eAncho10   in FFuente then MandaCodigo( Ancho10 );
          if eAncho12   in FFuente then MandaCodigo( Ancho12 );
          if eExtra1    in FFuente then MandaCodigo( Extra1 );
          if eExtra2    in FFuente then MandaCodigo( Extra2 );
          if eReset     in FFuente then MandaCodigo( Reset );
          if eLineas6   in FFuente then MandaCodigo( Lineas6 );
          if eLineas8   in FFuente then MandaCodigo( Lineas8 );
          if eLandscape in FFuente then MandaCodigo( Landscape );
     end;
end;

function OrdenaRowCol(List: TStringList; Index1, Index2: Integer): Integer;
var
    oCol1, oCol2 : TCampoIRapida;
begin
    oCol1 := TCampoIRapida( List.Objects[ Index1 ] );
    oCol2 := TCampoIRapida( List.Objects[ Index2 ] );
    { a value less than 0 if the string identified by Index1 comes before the string identified by Index2 };
    // Tiene m�s peso el rengl�n que la columna
    Result := ( oCol1.Top - oCol2.Top ) * 1000 + ( oCol1.Columna - oCol2.Columna ) + ( oCol1.Renglon - oCol2.Renglon );
end;


procedure TFastForm.OrdenaLista(FLista: TStringList; oLista: TStrings);
var
    i, nMaxColumna, nAnchoDato : Integer;
begin  { OrdenaLista }
    //FLista.Assign( oLista ); {La asignacion se hace afuera del Ordena Lista}
    with FLista do
      if ( Count > 0 ) then
      begin
          CustomSort( OrdenaRowCol );
          for i := 0 to Count-1 do
            with TCampoIRapida( Objects[ i ] ) do
            begin
                if ( Ancho <= 0 ) then
                    nAnchoDato := 10    // Default para anchos vac�os
                else
                    nAnchoDato := Ancho;
                nMaxColumna := Columna + nAnchoDato ;
                FColumnasForma := iMax( FColumnasForma, nMaxColumna );
            end;
      end;
end;

procedure TFastForm.EscribePiePagina;
var
    i : Integer;
begin
    with FPiePagina do
        for i := 0 to Count-1 do
            EscribeDato( TCampoIRapida( Objects[i] ), 0 );
end;

procedure TFastForm.PreparaRenglones;
begin
     SetLength( FRenglones, FLineasForma+1 );
     SetLength( FColumnas, FLineasForma+1 );
     LimpiaRenglones;
end;

function TFastForm.ApagaFuente(oFuente: TFuente): String;
begin
     Result := '';
     with FCodigosImpresora do
     begin
          if eNegrita    in oFuente then Result := Result + NegritaOff;
          if eItalica    in oFuente then Result := Result + ItalicaOff;
          if eComprimido in oFuente then Result := Result + Ancho10;
          if eSubrayado  in oFuente then Result := Result + SubrayadoOff;
     end;
end;

function TFastForm.PrendeFuente(oFuente: TFuente): String;
begin
     Result := '';
     with FCodigosImpresora do
     begin
          if eNegrita    in oFuente then Result := Result + NegritaOn;
          if eItalica    in oFuente then Result := Result + ItalicaOn;
          if eComprimido in oFuente then Result := Result + Comprimido;
          if eSubrayado  in oFuente then Result := Result + SubrayadoOn;
     end;
end;

procedure TFastForm.LimpiaRenglones;
var
    i : Integer;
begin
     for i := 1 to FLineasForma do
     begin
        FRenglones[ i ] := '';
        FColumnas[ i ] := 1;
     end;
end;

function TFastForm.GetCicloCopias: integer;
begin
     with FDataset.FieldByName(GetNColumna(FPosCiclo)) do
     begin
          if AsFloat > FMontoCiclo then
          begin
               Result := iMax( 1, Trunc(AsFloat / FMontoCiclo)); //<-- por lo menos debe imprimir una vez cada registro
               if ( ModReal(AsFloat,FMontoCiclo) >= 0.01 ) then
                  Inc(Result);
          end
          else Result := 1;
     end;
end;

function TFastForm.ConvierteFCiclo( const sTexto : string; const iPagina : integer ) : string;
 function GetMonto : TPesos;
 begin
      Result := rMin(FMontoCiclo , FDataset.FieldByName(GetNColumna(FPosCiclo)).AsFloat - ((iPagina-1)*FMontoCiclo) )
 end;
 var iPos : integer;
     sMonto : string;
begin
     Result := sTexto;
     if FMontoCiclo = 0 then Exit;
     iPos := Pos(K_CICLO,Result);
     if (iPos > 0 ) then
     begin
          sMonto := FloatToStr(GetMonto);

          with FCampoCiclo do
          begin
               //Todo esto esta repetido, se copio de AsTexto.
               //Pues la funcion Ciclo requiere que por cada registro
               //se evalue.
               sMonto := FormateaValor( sMonto,
                         StrDef( Mascara, GetMascaraDefault(tgFloat)),
                                 tgFloat, FALSE );

               if ( Ancho > 0 ) then
               begin
                    sMonto := Copy( sMonto, 1, Ancho );
                    case Justificacion of
                         taRightJustify : sMonto := PadL( sMonto, Ancho );
                         taCenter       : sMonto := PadC( sMonto, Ancho )
                         else             sMonto := PadR( sMonto, Ancho );
                    end;
               end;

               Result := StrTransAll( Result, PadR(K_CICLO,Ancho), sMonto );
          end;
     end;
     iPos := Pos(K_CICLOLETRA,Result);
     if ( iPos > 0 ) then
     begin
          sMonto := '*** '+MontoConLetra( GetMonto, '', TRUE ) + ' ***';
          with FCampoCLetra do
          begin
               sMonto := FormateaValor( sMonto,
                         StrDef( Mascara, GetMascaraDefault(tgTexto)),
                         tgTexto, FALSE );

               if ( Ancho > 0 ) then
               begin
                    sMonto := Copy( sMonto, 1, Ancho );
                    case Justificacion of
                         taRightJustify : sMonto := PadL( sMonto, Ancho );
                         taCenter       : sMonto := PadC( sMonto, Ancho );
                         else             sMonto := PadR( sMonto, Ancho );
                    end;
               end;
               Result := StrTransAll( Result, PadR(K_CICLOLETRA,Ancho), sMonto );
          end;
     end;
end;

procedure TFastForm.ImprimeRenglones;
var
    j,i,  nMaxLineas : Integer;
begin
     if (FMontoCiclo > 0) then
        FNumCopias := GetCicloCopias; {<-- cuantos recibos voy a imprimir}

     for j:= 1 to FNumCopias do
     begin
          Inc(FPaginaCopias); {<--FPaginaCopias: anteriormente estaba local, pero no calculaba correctamente la compensacion
                                                 se requiere saber cuantos 'vales' en total se han impreso, para que con ese
                                                 n�mero se calcule la compensacion}
          if ( FCompensaCada > 0 ) and
             ((( FPaginaCopias mod FCompensaCada ) = 0 )) then
              nMaxLineas := FLineasForma + FLineasCompensa
          else
              nMaxLineas := FLineasForma;

          if ( FEsPreview ) then
          begin
            with FormaIRPreliminar_DevEx.MemoPreliminar, Lines do
            begin
              for i := 1 to nMaxLineas do
                if ( i <= FLineasForma ) then
                    Add( ConvierteFCiclo( FRenglones[ i ], j ) )
                else
                    Add( '' );
            end
          end
          else
            for i := 1 to nMaxLineas do
                if ( i <= FLineasForma ) then
                    Writeln( FImpresora, ConvierteFCiclo(FRenglones[ i ], j ) )
                else
                    Writeln( FImpresora );
     end;
end;

procedure TFastForm.GetCodigosImpresora;
begin

    with FCodigosImpresora, dmSistema.cdsImpresoras do
      if ( Nombre <> FieldByName( 'PI_NOMBRE' ).AsString ) then
      begin
        Nombre      := FieldByName( 'PI_NOMBRE' ).AsString;
        Eject       := GetCodigosASCII(FieldByName('PI_EJECT').AsString);
        Ancho10     := GetCodigosASCII(FieldByName('PI_CHAR_10').AsString);
        Ancho12     := GetCodigosASCII(FieldByName('PI_CHAR_12').AsString);
        Comprimido  := GetCodigosASCII(FieldByName('PI_CHAR_17').AsString);
        Extra1      := GetCodigosASCII(FieldByName('PI_EXTRA_1').AsString);
        Extra2      := GetCodigosASCII(FieldByName('PI_EXTRA_2').AsString);
        Reset       := GetCodigosASCII(FieldByName('PI_RESET').AsString);
        SubrayadoOn := GetCodigosASCII(FieldByName('PI_UNDE_ON').AsString);
        SubrayadoOff:= GetCodigosASCII(FieldByName('PI_UNDE_OF').AsString);
        NegritaOn   := GetCodigosASCII(FieldByName('PI_BOLD_ON').AsString);
        NegritaOff  := GetCodigosASCII(FieldByName('PI_BOLD_OF').AsString);
        Lineas6     := GetCodigosASCII(FieldByName('PI_6_LINES').AsString);
        Lineas8     := GetCodigosASCII(FieldByName('PI_8_LINES').AsString);
        ItalicaOn   := GetCodigosASCII(FieldByName('PI_ITAL_ON').AsString);
        ItalicaOff  := GetCodigosASCII(FieldByName('PI_ITAL_OF').AsString);
        Landscape   := GetCodigosASCII(FieldByName('PI_LANDSCA').AsString);
      end;
end;

procedure TFastForm.PreparaGrupos(oGrupos: TStrings);
var
    i : Integer;
    oField : TField;
begin
    FNumGrupos := oGrupos.Count-1; // Ignora Grupo de Empresa
    SetLength( FLlaves, FNumGrupos+1 );
    SetLength( FLlavesPos, FNumGrupos+1 );
    with oGrupos do
        for i := 1 to FNumGrupos do
        begin
             //FLLavesPos[ i ] := TGrupoOpciones( Objects[ i ] ).PosAgente;
             oField := FDataSet.FindField(GetNColumna(TGrupoOpciones( Objects[ i ] ).PosAgente));
             if oField <> NIL then
                FLLavesPos[ i ] := oField.Index
             else Raise Exception.Create('Grupo no Encontrado : ' + GetNColumna(TGrupoOpciones( Objects[ i ] ).PosAgente));
        end;
end;

procedure TFastForm.AsignaLlaves;
var
    i : Integer;
begin
    for i := 1 to FNumGrupos do
        FLlaves[ i ] := FDataSet.Fields[ FLlavesPos[ i ] ].AsString;
end;

function TFastForm.MismoDetalle : Boolean;
var
    i : Integer;
begin
    Result:= True;
    for i := 1 to FNumGrupos do
        if ( FDataSet.Fields[ FLlavesPos[ i ]].AsString <> FLlaves[ i ] ) then
        begin
            Result := False;
            break;
        end;
end;


procedure TImpresionRapida_DevEx.CbBandaChange(Sender: TObject);

 var oCampo : TCampoIRapida;
begin
     inherited;
     with GetSmartList.ListaDisponibles do
     begin
          oCampo := TCampoIRapida( Items.Objects[ ItemIndex ] );
          Items.Objects[ ItemIndex ] := NIL;
          Items.Delete( ItemIndex );
     end;
     case oCampo.Banda of
          tbEncabezado : LBEncabezado.Items.AddObject(oCampo.Titulo, oCampo);
          tbDetalle: LBCampos.Items.AddObject(oCampo.Titulo, oCampo);
          tbPiePagina: LBPie.Items.AddObject(oCampo.Titulo, oCampo);
     end;
     //BorraDato( GetSmartList, bAgregaCampo, bBorraCampo, ETituloCampo );
     CBBandaSelChange(Sender);
end;

procedure TImpresionRapida_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     EPaginaInicial.Valor := 1;
     EPaginaFinal.Valor := 9999;
     bDisenador.Visible := FALSE;
     bbSuscripcion.Enabled:= FALSE;
     tsSuscripciones.TabVisible:= FALSE;
     BPreeliminar.Enabled :=  NOT EsIRSubsecuente;
     TabSheet1.HelpContext := H_REP_IMP_SUB;
end;


procedure TImpresionRapida_DevEx.OnCalculaLineas(Sender: TObject);
begin
     inherited;
     CalculaLineas(LineaModificada(TZetaDBNumero(Sender).Tag));
end;

procedure TImpresionRapida_DevEx.OnCalculaLineasUD(Sender: TObject;
  Button: TUDBtnType);
begin
     inherited;
     CalculaLineas(LineaModificada(TUpDown(Sender).Tag));
end;

procedure TImpresionRapida_DevEx.CalculaLineas(const Modificado : LineaModificada);

 var iEncabezado, iLineasForma, iDetalle,iLineas : integer;
 procedure SetLineasDetalle;
 begin
      UDDetalle.Position := iDetalle;
      Reporte.FieldByname('RE_COLESPA').AsInteger := iDetalle;
 end;
 procedure SetLineasEncabezado;
 begin
      UDEncabezado.Position := iEncabezado;
      Reporte.FieldByname('RE_RENESPA').AsInteger := iEncabezado;
 end;
 procedure SetLineasFormas;
 begin
      UDLineasForma.Position := iLineasForma;
      Reporte.FieldByname('RE_ANCHO').AsInteger := iLineasForma;
 end;
begin
     iLineasForma := GetLineas;

     iEncabezado := StrAsInteger( RE_RENESPA.Text );
     if iEncabezado <= iLineasForma then
     begin
          iDetalle := StrAsInteger( RE_COLESPA.Text );

          iLineas := iLineasForma - iEncabezado - iDetalle;

          if iLineas >= 0 then
          begin
               RE_PIE.Caption := IntToStr( iLineas );
               Reporte.Edit;
               SetLineasFormas;
               SetLineasEncabezado;
               SetLineasDetalle;
               Reporte.Post;
          end
          else
          begin
               case Modificado of
                    liFormas:
                    begin
                         RE_PIE.Caption := '0';
                         iDetalle := iLineasForma - iEncabezado;
                         Reporte.Edit;
                         SetLineasFormas;
                         if iDetalle >= 0 then SetLineasDetalle
                         else
                         begin
                              iDetalle := 0;
                              iEncabezado := iLineasForma;
                              SetLineasEncabezado;
                         end;
                         Reporte.Post;
                    end;
                    liEncabezado:
                    begin
                         RE_PIE.Caption := '0';
                         iDetalle := iLineasForma - iEncabezado;
                         Reporte.Edit;
                         SetLineasEncabezado;
                         SetLineasDetalle;
                         Reporte.Post;
                    end;
                    liDetalle:
                    begin
                         iDetalle := FLastDetalle;
                         Reporte.Edit;
                         SetLineasDetalle;
                         Reporte.Post;
                         ZError( Caption, 'L�neas de Detalle Debe Ser Menor o Igual a: '+ IntToStr(iLineasForma - iEncabezado) + ' L�neas', 0 );
                    end;
               end;
          end
     end
     else
     begin
          if Modificado = liEncabezado then
          begin
               iEncabezado := FLastEncabezado;
               Reporte.Edit;
               SetLineasEncabezado;
               Reporte.Post;
               ZError( Caption, 'L�neas de Encabezado Debe Ser Menor o Igual a: '+ IntToStr(iLineasForma) + ' L�neas', 0 )
          end
          else
          begin
               iEncabezado := iLineasForma;
               iDetalle := 0;
               RE_PIE.Caption := '0';
               Reporte.Edit;
               SetLineasDetalle;
               SetLineasFormas;
               SetLineasEncabezado;
               Reporte.Post;
          end;
     end;
     FLastEncabezado := UDEncabezado.Position;
     FLastDetalle := UDDetalle.Position;
     Modo := dsEdit;
end;


procedure TImpresionRapida_DevEx.BAgregaCampoClick(Sender: TObject);
begin
     inherited;
     with TBitBtn(Sender) do
     begin
          SetFocus;
          if Tag = 0 then AgregaCampo
          else AgregaFormula;
     end;
end;

procedure TImpresionRapida_DevEx.UDCompensacionClick(Sender: TObject;
  Button: TUDBtnType);
begin
     inherited;
     Reporte.Edit;
     Reporte.FieldByName('RE_HOJA').AsInteger := UdCompensacion.Position;
end;
procedure TImpresionRapida_DevEx.UDCompensarCadaClick(Sender: TObject;
  Button: TUDBtnType);
begin
     inherited;
     Reporte.Edit;
     Reporte.FieldByName('RE_ALTO').AsInteger := UdCompensarCada.Position;
end;

procedure TImpresionRapida_DevEx.UDCopiasClick(Sender: TObject;
  Button: TUDBtnType);
begin
     inherited;
     Reporte.Edit;
     Reporte.FieldByName('RE_COPIAS').AsInteger := UdCopias.Position;
end;

procedure TImpresionRapida_DevEx.bBusccaReporte_ISClick(Sender: TObject);
begin
     inherited;

     if (FormaLookupReporte_DevEx = NIL) then
        FormaLookupReporte_DevEx := TFormaLookupReporte_DevEx.Create( self );

     Panel2.SetFocus;
     with FormaLookupReporte_DevEx do
     begin

          Filtro := 'RE_TIPO = 5';
          if ShowModal = mrOk then
          begin
               if Reporte.State = dsBrowse then
                  Reporte.Edit;
               Reporte.FieldByName( 'RE_FONTNAM' ).AsString := ConcatString( Reporte.FieldByName( 'RE_FONTNAM' ).AsString , IntToStr( NumReporte ), ',' );
          end;
          Reporte.FieldByName( 'RE_FONTNAM' ).FocusControl;
     end;
end;

procedure TImpresionRapida_DevEx.ImpresionReporte(const Tipo: TipoPantalla);
begin

     if StrVacio( eListaReportesSubsecuentes.Text ) OR dmReportes.ForzarImpresion then
        inherited
     else
     begin
          Cancelar_DevEx.SetFocus;
          FTipoPantalla := Tipo;
          Close;
          dmReportes.ImprimeSubsecuentes := TRUE;
     end;
end;

procedure TImpresionRapida_DevEx.SubsecuentesAbreImpresora;
begin
     FFastForm.SubsecuentesAbreImpresora;
end;

procedure TImpresionRapida_DevEx.SubsecuentesGeneraReporte( subDataset: TDataset );
begin
     FFastForm.SubsecuentesGeneraReporte( SubDataset );
end;

procedure TImpresionRapida_DevEx.SubsecuentesCierraImpresora;
begin
     if FFastForm <> NIL then
        FFastForm.SubsecuentesCierraImpresora;
end;

procedure TImpresionRapida_DevEx.BPreeliminarClick(Sender: TObject);
begin
     if EsIRSubsecuente or StrLleno(eListaReportesSubsecuentes.text) then
        ZetaDialogo.ZInformation( Caption, 'El Preview no se puede mostrar cuando hay reportes subsecuentes',0 )
     else
     begin
          inherited;
     end;
end;

procedure TImpresionRapida_DevEx.eListaReportesSubsecuentesExit(Sender: TObject);
begin
     inherited;
     BPreeliminar.Enabled := NOT EsIRSubsecuente;
end;

function TImpresionRapida_DevEx.EsIRSubsecuente: Boolean;
begin
     Result := StrLleno( Reporte.FieldByName('RE_FONTNAM').AsString );
end;

end.

