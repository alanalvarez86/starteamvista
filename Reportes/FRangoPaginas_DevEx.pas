unit FRangoPaginas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
   TressMorado2013, dxSkinsDefaultPainters,
   cxButtons;

type
  TRangoPaginas_DevEx = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    EPAGINAS: TLabel;
    EPaginaInicial: TEdit;
    EPAginasAl: TLabel;
    EPaginaFinal: TEdit;
    lbCopias: TLabel;
    ECopias: TEdit;
    UDCopias: TUpDown;
    Cancelar_DevEx: TcxButton;
    OK_DevEx: TcxButton;
  private
         function GetInicial : integer;
         procedure SetInicial( iInicial : integer );
         function GetFinal : integer;
         procedure SetFinal( iFinal : integer );
         function GetCopias : integer;
         procedure SetCopias( iCopias : integer );
  public
    property Inicial : Integer read GetInicial write SetInicial;
    property Final : Integer read GetFinal write SetFinal;
    property Copias : Integer read GetCopias write SetCopias;
  end;

var
  RangoPaginas_DevEx: TRangoPaginas_DevEx;

  function DialogoPaginas( var iInicial, iFinal, iCopias : integer ) : Boolean;

implementation

{$R *.DFM}

function DialogoPaginas( var iInicial, iFinal, iCopias : integer ) : Boolean;
begin
     if RangoPaginas_DevEx = NIL then RangoPaginas_DevEx := TRangoPaginas_DevEx.Create( Application.MainForm );
     with RangoPaginas_DevEx do
     begin
          Inicial := iInicial;
          Final := iFinal;
          Copias := iCopias;
          ShowModal;
          Result := ModalResult = mrOk;
          if Result  then
          begin
               iInicial := Inicial;
               iFinal := Final;
               iCopias := Copias;
          end;
     end;
end;

function TRangoPaginas_DevEx.GetInicial : integer;
begin
     Result := StrToInt( EPaginaInicial.Text );
end;

procedure TRangoPaginas_DevEx.SetInicial( iInicial : integer );
begin
     EPaginaInicial.Text := IntToStr( iInicial );
end;

function TRangoPaginas_DevEx.GetFinal : integer;
begin
     Result := StrToInt( EPaginaFinal.Text );
end;

procedure TRangoPaginas_DevEx.SetFinal( iFinal : integer );
begin
     EPaginaFinal.Text := IntToStr( iFinal );
end;

function TRangoPaginas_DevEx.GetCopias : integer;
begin
     Result := StrToInt( ECopias.Text );
end;

procedure TRangoPaginas_DevEx.SetCopias( iCopias : integer );
begin
     ECopias.Text := IntToStr( iCopias );
end;


end.
