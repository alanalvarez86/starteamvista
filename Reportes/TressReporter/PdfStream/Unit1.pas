unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, gtCstDocEng, gtCstPlnEng, gtCstPDFEng, gtExPDFEng,
  gtPDFEng, gtClasses, gtXportIntf, gtQRXportIntf, QuickRpt, QRCtrls,
  ExtCtrls, Grids, DBGrids, DB, DBClient, Types, ZetaServerDataSet;

type
  TForm1 = class(TForm)
    gtPDFEngine1: TgtPDFEngine;
    Button1: TButton;
    Button2: TButton;
    Memo1: TMemo;
    cdsTemporal: TServerDataSet;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
 const
     CR_LF = Chr(13) + Chr( 10 );

 var LStream: TFileStream;
begin
     with gtPDFEngine1 do
     begin
          LStream := TFileStream.Create('D:\TEMP\UserStream' + '.' + FileExtension , fmCreate);
          Preferences.OutputToUserStream := True;
          UserStream := LStream;
          BeginDoc;
          TextOut(2, 2,'HOOOOOOLA! DE PARTE DE CAROLINA <8-) ' );
          TextOut(2, 3,Format( ' Hoy es %s, y son las %s', [FormatDateTime('dd/mmm/yyyy', Date), FormatDateTime('hh:nn:ss', Now) ]) );
          EndDoc;
          LStream.Free;
     end;




end;

procedure TForm1.Button2Click(Sender: TObject);
 const
     CR_LF = Chr(13) + Chr( 10 );

 var mStream: TMemoryStream;
     sStream : TStringStream;
     fStream: TFileStream;
     Datos: string;
     gtPDFEngine: TgtPDFEngine;
     FBlob : TBlobField;
     aData : TByteDynArray;
     Data: Byte;
     i,iSize : integer;
begin
     memo1.Lines.Clear;
     gtPDFEngine:= TgtPDFEngine.Create(Self);

     with gtPDFEngine do
     begin
          mStream := TMemoryStream.Create;
          with Preferences do
          begin
               OutputToUserStream := True;
               ShowSetupDialog := FALSE;
               OpenAfterCreate := FALSE;
          end;
          UserStream := mStream;
          BeginDoc;
          TextOut(2, 2,'HOOOOOOLA! DE PARTE DE CAROLINA <8-) ' );
          TextOut(2, 3,Format( ' Hoy es %s, y son las %s', [FormatDateTime('dd/mmm/yyyy', Date), FormatDateTime('hh:nn:ss', Now) ]) );
          EndDoc;


          SetLength( aData, mStream.Size );
          iSize := mStream.Size;
          mStream.Position :=0;
          for i:=0 to iSize do
          begin
              mStream.ReadBuffer( Data, 1 );
              memo1.lines.Add(IntToStr(i));
              aData[mStream.Position] := Data;
              mStream.Position :=i;
          end;
          mStream.Free;

          mStream := TMemoryStream.Create;
          try
             mStream.Position :=0;
             for i:=0 to iSize do
             begin
                  mStream.WriteBuffer( aData[i], 1);
             end;

               fStream := TFileStream.Create('d:\temp\archivo.pdf', fmCreate );
               try
                  //mStream.SaveToStream( fStream );
                  fStream.Position :=0;
                  for i:=0 to iSize do
                  begin
                       fStream.WriteBuffer( aData[i], 1);
                  end;



               finally
                      fStream.Free;
               end;
            finally
                 mStream.Free;
          end;
     end;
end;

procedure TForm1.Button3Click(Sender: TObject);
var mStream: TMemoryStream;
     sStream : TStringStream;
     fStream: TFileStream;
     Datos: string;
     gtPDFEngine: TgtPDFEngine;
     FBlob : TBlobField;
begin
     with cdsTemporal do
     begin
          InitTempDataset;
          AddBlobField('CampoBlob', 0 );
          CreateTempDataset;
          FBlob := TBlobField( FieldByName('CampoBlob') );
     end;

     FBlob.DataSet.Edit;
     FBlob.AsString := memo1.Lines.Text;
     FBlob.DataSet.Post;

     fStream := TFileStream.Create('d:\temp\archivo2.pdf', fmCreate);
     FBlob.SaveToStream( fStream );
     fStream.Free;
end;

end.
