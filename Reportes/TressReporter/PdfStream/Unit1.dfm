object Form1: TForm1
  Left = 195
  Top = 195
  Width = 698
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 272
    Top = 40
    Width = 209
    Height = 25
    Caption = 'Generar PDF-Stream'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -16
    Font.Name = 'Neuropol'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 272
    Top = 72
    Width = 209
    Height = 25
    Caption = 'Generar PDF-String'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -16
    Font.Name = 'Neuropol'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 16
    Top = 176
    Width = 657
    Height = 313
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
  object Button3: TButton
    Left = 272
    Top = 104
    Width = 209
    Height = 25
    Caption = 'Guarda PDF-String'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -16
    Font.Name = 'Neuropol'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = Button3Click
  end
  object gtPDFEngine1: TgtPDFEngine
    FileExtension = 'pdf'
    FileDescription = 'Adobe PDF Files'
    InputXRes = 96
    InputYRes = 96
    ReferencePoint = rpBand
    FileName = 'd:\temp\Archivo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ImageSettings.SourceDPI = 96
    ImageSettings.OutputImageFormat = ifBMP
    Page.Width = 8.267700000000000000
    Page.Height = 11.692900000000000000
    Page.BinNumber = 0
    Preferences.ShowSetupDialog = False
    FontEncoding = feWinAnsiEncoding
    Left = 8
    Top = 8
  end
  object cdsTemporal: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 88
    Top = 24
  end
end
