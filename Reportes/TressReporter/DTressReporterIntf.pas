{ Invokable interface ITressReporter }

unit DTressReporterIntf;

interface

uses InvokeRegistry, Types, XSBuiltIns;

type
  { Invokable interfaces must derive from IInvokable }
  ITressReporter = interface(IInvokable)
  ['{E7704B20-6295-40BF-B369-1A21B0D806BF}']
    { Methods of Invokable interface must not use the default }
    { calling convention; stdcall is recommended }
    function Clasificaciones( const Empresa: WideString ): WideString; stdcall;
    function Reportes( const Companys, UserName, Password: WideString; const Clasificacion: Integer ): WideString; stdcall;
    function ReportAsXML( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): WideString; stdcall;
    function ReportAsPDF( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): TByteDynArray; stdcall;
    function ReportAsPDFStream( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): TByteDynArray;stdcall;
  end;

implementation

initialization
  { Invokable interfaces must be registered }
  InvRegistry.RegisterInterface( TypeInfo( ITressReporter ) );

end.
 