unit DReportesPDF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient, FileCtrl,
  ZetaCommonLists,
  ZQRReporteListado,
  DReportesGenerador;

type
  TdmReportesPDF = class(TdmReportGenerator)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    QRReporteListado : TQrReporteListado;
    FMensaje: TStrings;
    FReportName: String;
    FDefaultStream: TStream;

    function GetSobreTotales: Boolean;
    procedure ModificaListaCampos(Lista: TStrings);
    procedure ModificaListas;
    function GetMensaje: string ;

  protected
    procedure DoOnGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoOnGetReportes( const lResultado: Boolean );override;

    function PreparaPlantilla(var sError: WideString): Boolean;override;
    procedure DesPreparaPlantilla;override;

    procedure PreparaReporte;
    procedure DesPreparaReporte;
    procedure LogError(const sTexto: String; const lEnviar: Boolean);override;

    function GetExtensionDef : string;override;
    procedure DoOnGetDatosImpresion;override;


  public
    { Public declarations }
    function Procesar(const sEmpresa: String; const iReporte: Integer; var sNombre: String): Integer;
    property Mensaje: string read GetMensaje;
    property DefaultStream: TStream read FDefaultStream write FDefaultStream;

  end;

var
  dmReportesPDF: TdmReportesPDF;

implementation

uses DCliente,
     DGlobal,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZReportTools,
     ZFuncsCliente,
     ZAccesosTress,
     ZReporteAscii,
     ZetaDialogo,
     DMailMerge;
{$R *.DFM}

const
     K_NOMBRE_BITACORA = 'ReportesKiosko.LOG';

{ TdmReportes }

procedure TdmReportesPDF.DataModuleCreate(Sender: TObject);
begin
     FMostrarError := FALSE;
     //SetLogFileName( ExtractFilePath( Application.ExeName ) + K_NOMBRE_BITACORA );
     inherited;
     //Global.Conectar;
     FMensaje := TStringList.Create;
end;

procedure TdmReportesPDF.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FMensaje );
     inherited;
end;

function TdmReportesPDF.GetSobreTotales: Boolean;
 var i : integer;
begin
     Result := FALSE;
     for i := 0 to Campos.Count - 1 do
     begin
          with TCampoListado( Campos.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion = ocSobreTotales);
          end;
     end;
end;

function TdmReportesPDF.PreparaPlantilla( var sError : WideString ) : Boolean;
 var
    lHayImagenes : Boolean;
    sDirectorio : string;
begin

     PreparaReporte;

     sDirectorio := ExtractFilePath( DatosImpresion.Archivo );
     ///Result := DirectoryExists( sDirectorio );
     //if NOT Result then
     //   sError := Format( 'El Directorio de Plantillas " %s " No Existe', [ sDirectorio ] );

     //if Result then
     begin
          DoOnGetDatosImpresion;

          with QRReporteListado do
          begin
               Init( FSQLAgente,
                     DatosImpresion,
                     Parametros.Count,
                     lHayImagenes );
               ContieneImagenes := lHayImagenes;
          end;
          QRReporteListado.DefaultStream := FDefaultStream
     end;
     Result := TRUE;
end;

procedure TdmReportesPDF.DesPreparaPlantilla;
begin
     DesPreparaReporte;
end;

procedure TdmReportesPDF.DoOnGetReportes( const lResultado: Boolean );
begin
     if lResultado then
     begin
          FReportName := cdsReporte.FieldByName( 'RE_NOMBRE' ).AsString;
          if NOT(eTipoReporte( cdsReporte.FieldByName('RE_TIPO').AsInteger ) in [trForma, trListado] ) then
          begin
               Raise Exception.Create( 'Solamente se Permite Obtener Listados y/o Formas' );
          end;
     end;
end;

procedure TdmReportesPDF.DoOnGetResultado( const lResultado : Boolean; const Error : string );

 procedure GeneraMailMerge;
  var
     oReporteAscii: TReporteAscii;
 begin
      oReporteAscii := TReporteAscii.Create;

      try
         with oReporteAscii do
         begin
              if GeneraAscii( cdsResultados,
                              DatosImpresion,
                              Campos,
                              Grupos,
                              zStrToBool( cdsReporte.FieldByName('RE_SOLOT').AsString ),
                              FALSE ) then
              begin
                   with TdmMailMerge.Create(self) do
                   begin
                        with DatosImpresion do
                             if FileExists( Archivo ) then
                                Imprime( Archivo,Exportacion )
                             else
                                 ZError('Impresión','El Documento ' + Archivo + ' No Ha Sido Encontrado.', 0);
                        Free;
                   end;
              end;
         end;
      finally
             FreeAndNil( oReporteAscii );
      end;

 end;

 var eTipo : eTipoReporte;
begin
     if cdsResultados.Active AND Not cdsResultados.IsEmpty then
     begin

          cdsResultados.First;

          ModificaListas;

          ZFuncsCliente.RegistraFuncionesCliente;
          //Asignacion de Parametros del Reporte,
          //para poder evaluar la funcion PARAM() en el Cliente;
          ZFuncsCliente.ParametrosReporte := FSQLAgente.Parametros;

          with cdsReporte do
          begin
               eTipo := eTipoReporte( FieldByName('RE_TIPO').AsInteger );
               if ( eTipo = trForma ) then
               begin
                    QRReporteListado.GeneraForma( FieldByName('RE_NOMBRE').AsString,
                                                  FALSE, {Mandarlo a Preview}
                                                  cdsResultados )
               end
               else if ( eTipo = trListado ) then
               begin
                    if ( eTipoFormato( FieldByName('RE_PFILE').AsInteger ) = tfMailMerge ) then
                    begin
                         GeneraMailMerge;
                    end
                    else
                        QRReporteListado.GeneraListado( FieldByName('RE_NOMBRE').AsString,
                                                    FALSE, {Mandarlo a Preview}
                                                    zStrToBool( FieldByName('RE_VERTICA').AsString ),
                                                    zStrToBool( FieldByName('RE_SOLOT').AsString ),
                                                    GetSobreTotales,
                                                    cdsResultados,
                                                    Campos,
                                                    Grupos );
               end;
          end;
     end;
end;

procedure TdmReportesPDF.ModificaListaCampos( Lista : TStrings );
 var i : integer;
begin
     for i:=0 to Lista.Count -1 do
        with TCampoOpciones(Lista.Objects[i]) do
             if PosAgente >= 0 then
             begin
                  SQLColumna := FSQLAgente.GetColumna(PosAgente);
                  with FSQLAgente.GetColumna(PosAgente) do
                  begin
                       TipoImp := TipoFormula;
                       OpImp := Totalizacion;
                  end;
             end;
end;

procedure TdmReportesPDF.ModificaListas;
 var i : integer;
begin
     ModificaListaCampos( Campos );
     for i:= 0 to Grupos.Count - 1 do
     begin
          with TGrupoOpciones( Grupos.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
end;

procedure TdmReportesPDF.PreparaReporte;
begin
     if QRReporteListado = NIL then
        QRReporteListado := TQRReporteListado.Create( Application );
end;

procedure TdmReportesPDF.DesPreparaReporte;
begin
     try
        QRReporteListado.Free;
     finally
            QRReporteListado := NIL;
     end;
end;


procedure TdmReportesPDF.LogError(const sTexto: String; const lEnviar: Boolean );
begin
     FMensaje.Add(sTexto);
end;

procedure TdmReportesPDF.DoOnGetDatosImpresion;
 var
    sError: string;
begin
     with DatosImpresion do
     begin
          Tipo := tfPDF;
          {with dmCliente.DatosReporte do
          if StrLleno( Path ) and strLleno( Nombre ) then
             Exportacion := VerificaDir( Path ) + Nombre
          else}
              Exportacion := GetNombreExportacion( DatosImpresion,
                                                   GetExtensionDef,
                                                   sError );
     end;
end;

function TdmReportesPDF.GetExtensionDef : string;
begin
     Result := 'PDF';
end;

function TdmReportesPDF.GetMensaje: String;
begin
     Result := FMensaje.Text;
end;

function TdmReportesPDF.Procesar(const sEmpresa: String; const iReporte: Integer; var sNombre: String): Integer;
begin
     Global.Conectar;
     Empresa := sEmpresa;
     FReportName := VACIO;
     if GetResultado( iReporte, TRUE ) then
     begin
          Result := cdsResultados.RecordCount;
     end
     else
     begin
          Result := 0;
     end;
     sNombre := FReportName;
end;

end.

