unit FTressReporterTest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Mask, Types,
  SOAPHTTPClient,
  DTressReporterImpl,
  ITressReporterProxy,
  ZetaNumero, ComCtrls;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    URLlbl: TLabel;
    URL: TEdit;
    Panel2: TPanel;
    Clasificaciones: TButton;
    Reportes: TButton;
    EmpresaLBL: TLabel;
    Empresa: TEdit;
    UserNameLBL: TLabel;
    UserName: TEdit;
    PasswordLBL: TLabel;
    Password: TEdit;
    GenerarReportes: TButton;
    Reporte: TZetaNumero;
    ReporteLBL: TLabel;
    UsarWebService: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Memo1: TMemo;
    Memo2: TMemo;
    Button1: TButton;
    Button2: TButton;
    procedure ClasificacionesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ReportesClick(Sender: TObject);
    procedure GenerarReportesClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    FWebServiceProxy: THTTPRIO;
    FServicio: TTressReporter;
    function GetReporter: ITressReporter;
    function GetEmpresa: String;
    function GetPassword: String;
    function GetUserName: String;
    function GetReporte: Integer;
    procedure ServicioInit;
    procedure ShowResult(const XML: String);
    procedure ServicioClear;
    procedure WriteStreamToFile( const sFileName: String; const Datos: TByteDynArray );
  public
    { Public declarations }
    procedure Debug( const MethodName: string; SOAPResponse: TStream );
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
     FWebServiceProxy := THTTPRIO.Create( Self );
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FWebServiceProxy );
     if Assigned( FServicio ) then
        FreeAndNil( FServicio );
end;

procedure TForm1.ServicioInit;
begin
     if not Assigned( FServicio ) then
        FServicio := TTressReporter.Create;
end;

procedure TForm1.ServicioClear;
begin
     FreeAndNil(FServicio);
end;

procedure TForm1.Debug( const MethodName: string; SOAPResponse: TStream );
var
   Texto: TStrings;
begin
     Texto := TStringList.Create;
     try
        SOAPResponse.Position := 0;
        Texto.LoadFromStream( SOAPResponse );
        ShowMessage( MethodName + ':' + Texto.Text );
     finally
            FreeAndNil( Texto );
     end;
end;

function TForm1.GetReporter: ITressReporter;
begin
     Result := ITressReporterProxy.GetITressReporter( False, Self.URL.Text, Self.FWebServiceProxy );
end;

function TForm1.GetEmpresa: String;
begin
     Result := Empresa.Text;
end;

function TForm1.GetUserName: String;
begin
     Result := UserName.Text;
end;

function TForm1.GetReporte: Integer;
begin
     Result := Self.Reporte.ValorEntero;
end;

function TForm1.GetPassword: String;
begin
     Result := Password.Text;
end;

procedure TForm1.ShowResult( const XML: String );
begin
     with Memo1 do
     begin
          with Lines do
          begin
               Clear;
               BeginUpdate;
               try
                  Text := XML;
                  //SaveToFile('d:\temp\Reporte.xml');
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TForm1.ClasificacionesClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                ShowResult( GetReporter.Clasificaciones( GetEmpresa ) );
           end
           else
           begin
                ServicioInit;
                ShowResult( FServicio.Clasificaciones( GetEmpresa ) );
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TForm1.ReportesClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                ShowResult( GetReporter.Reportes( GetEmpresa, GetUserName, GetPassword, 1 ) );
           end
           else
           begin
                ServicioInit;
                ShowResult( FServicio.Reportes( GetEmpresa, GetUserName, GetPassword, 1 ) );
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TForm1.GenerarReportesClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                ShowResult( GetReporter.ReportAsXML( GetEmpresa, GetUserName, GetPassword, GetReporte, Memo2.Lines.Text ) );
           end
           else
           begin
                ServicioInit;
                try
                   ShowResult( FServicio.ReportAsXML( GetEmpresa, GetUserName, GetPassword, GetReporte, Memo2.Lines.Text ) );
                finally
                       ServicioClear;
                end;
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TForm1.WriteStreamToFile( const sFileName: String; const Datos: TByteDynArray );
var
   FStream: TFileStream;
   {$ifndef FALSE}
   i: integer;
   {$endif}
begin
     if ( Length( Datos ) > 0 ) then
     begin
          try
             if FileExists( sFileName ) then
             begin
                  DeleteFile( sFileName );
             end;
             FStream := TFileStream.Create( sFileName, fmCreate );
             try
                FStream.Position := 0;
                {$ifndef FALSE}
                for i := 0 to ( Length( Datos ) ) do
                begin
                     FStream.WriteBuffer( Datos[ i ], 1 );
                end;
                {$else}
                FStream.Write( Datos, Length( Datos ) );
                {$endif}
             finally
                    FStream.Free;
             end;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end
     else
     begin
          ShowMessage( 'No Hay Datos' );
     end;
end;

procedure TForm1.Button1Click(Sender: TObject);
 const K_ES_ERROR:array[0..4] of Byte = (1,1,2,3,5);
var
   oCursor: TCursor;
   aData: TByteDynArray;

function EsError: boolean;
var
 i: integer;
begin
     Result := Length( aData ) > Length( K_ES_ERROR );
     if Result then
        for i:=0 to Length( K_ES_ERROR )-1 do
        begin
            Result := ( aData[i+1] = K_ES_ERROR[i] );
            if not Result then Break;
        end;
end;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                aData := GetReporter.ReportAsPDF( GetEmpresa, GetUserName, GetPassword, GetReporte, Memo2.Lines.Text );
           end
           else
           begin
                ServicioInit;
                try
                   aData := FServicio.ReportAsPDF( GetEmpresa, GetUserName, GetPassword, GetReporte, Memo2.Lines.Text );
                finally
                       ServicioClear;
                end;
           end;
           if EsError then
              WriteStreamToFile( 'D:\Temp\archivo.txt', aData )
           else
           WriteStreamToFile( 'D:\Temp\archivo.pdf', aData );
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;

end;

procedure TForm1.Button2Click(Sender: TObject);
var
   oCursor: TCursor;
   aData: TByteDynArray;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           if UsarWebService.Checked then
           begin
                aData := GetReporter.ReportAsPDFStream( GetEmpresa, GetUserName, GetPassword, GetReporte, Memo2.Lines.Text );
           end
           else
           begin
                ServicioInit;
                try
                   aData:= FServicio.ReportAsPDFStream( GetEmpresa, GetUserName, GetPassword, GetReporte, Memo2.Lines.Text );
                finally
                       ServicioClear;
                end;
           end;
           WriteStreamToFile( 'D:\Temp\archivo2.pdf', aData );
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;

end;

end.
