unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, DB, DBClient, ExtCtrls;

type
  TForm3 = class(TForm)
    DataSource1: TDataSource;
    ClientDataSet1: TClientDataSet;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Edit1: TEdit;
    carga: TButton;
    Splitter1: TSplitter;
    procedure cargaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.cargaClick(Sender: TObject);
begin
     ClientDataSet1.LoadFromFile(Edit1.Text);
end;

end.
