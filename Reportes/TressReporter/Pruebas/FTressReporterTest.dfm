object Form1: TForm1
  Left = 191
  Top = 142
  Width = 696
  Height = 480
  Caption = 'TressReporter Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 98
    Align = alTop
    TabOrder = 0
    object URLlbl: TLabel
      Left = 39
      Top = 8
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'URL:'
    end
    object EmpresaLBL: TLabel
      Left = 20
      Top = 30
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa:'
    end
    object UserNameLBL: TLabel
      Left = 25
      Top = 53
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usuario:'
    end
    object PasswordLBL: TLabel
      Left = 15
      Top = 76
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'Password:'
    end
    object ReporteLBL: TLabel
      Left = 235
      Top = 35
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reporte:'
    end
    object URL: TEdit
      Left = 67
      Top = 4
      Width = 617
      Height = 21
      TabOrder = 0
      Text = 
        'http://localhost/TressReporter/TressReporter.dll/Reportes/ITress' +
        'Reporter'
    end
    object Empresa: TEdit
      Left = 67
      Top = 26
      Width = 126
      Height = 21
      TabOrder = 1
      Text = 'CAROLINA39'
    end
    object UserName: TEdit
      Left = 67
      Top = 50
      Width = 126
      Height = 21
      TabOrder = 2
      Text = 'c'
    end
    object Password: TEdit
      Left = 67
      Top = 73
      Width = 126
      Height = 21
      TabOrder = 3
      Text = 'c'
    end
    object Reporte: TZetaNumero
      Left = 280
      Top = 32
      Width = 49
      Height = 21
      Mascara = mnDias
      TabOrder = 4
      Text = '0'
    end
    object UsarWebService: TCheckBox
      Left = 576
      Top = 72
      Width = 97
      Height = 17
      Caption = 'Usar WebService'
      TabOrder = 5
    end
  end
  object Panel2: TPanel
    Left = 560
    Top = 98
    Width = 128
    Height = 348
    Align = alRight
    TabOrder = 1
    object Clasificaciones: TButton
      Left = 8
      Top = 8
      Width = 113
      Height = 41
      Caption = 'Clasificaciones'
      TabOrder = 0
      OnClick = ClasificacionesClick
    end
    object Reportes: TButton
      Left = 8
      Top = 56
      Width = 113
      Height = 41
      Caption = 'Reportes'
      TabOrder = 1
      OnClick = ReportesClick
    end
    object GenerarReportes: TButton
      Left = 8
      Top = 105
      Width = 113
      Height = 41
      Caption = 'Generar Reporte XML'
      TabOrder = 2
      OnClick = GenerarReportesClick
    end
    object Button1: TButton
      Left = 8
      Top = 153
      Width = 113
      Height = 41
      Caption = 'Generar Reporte PDF'
      TabOrder = 3
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 8
      Top = 201
      Width = 113
      Height = 41
      Caption = 'Generar Reporte Stream'
      TabOrder = 4
      OnClick = Button2Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 98
    Width = 560
    Height = 348
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 552
        Height = 320
        Align = alClient
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object Memo2: TMemo
        Left = 0
        Top = 0
        Width = 552
        Height = 320
        Align = alClient
        Lines.Strings = (
          '<CIA>DATOS</CIA>'
          '<EMP>'
          
            #9'<P>Numero:54|Nombre:DAFFY DUCK|TipoNomina:tpDiario|FechaIngreso' +
            ':0001/01/0100:00:00:000|FechaBaja:0001/01/01 00:00:00:000|Activo' +
            ':N|Reingreso:N</P>'
          '</EMP>'
          '<PER>'
          '<PYR>2005</PYR>'
          '<PTIPO>1</PTIPO>'
          '<PNUMERO>16</PNUMERO>'
          '<PSTATUS>0</PSTATUS>'
          '<PUSO>0</PUSO>'
          '<PPAGO>'
          
            '<P>FechaInicial:2006/07/26 00:00:00:000|FechaFinal:2006/07/26 00' +
            ':00:00:000</P>'
          '</PPAGO>'
          '<PASIS>'
          
            '<P>FechaInicial:2006/07/26 00:00:00:000|FechaFinal:2006/07/26 00' +
            ':00:00:000</P>'
          '</PASIS>'
          '</PER>'
          '<FEC>2006/08/09</FEC>'
          '<RAN>'
          
            '<P>FechaInicial:2006/07/19 00:00:00:000|FechaFinal:2006/08/02 00' +
            ':00:00:000</P>'
          '</RAN>'
          '<MES>'
          '<P>Numero:1|Anio:2004</P>'
          '</MES>'
          '<YR>2003</YR>'
          '<PRI>N</PRI>'
          '<ULT>N</ULT>'
          '<NIL>N</NIL>'
          '<ERR>N</ERR>'
          '<MSG></MSG>')
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
end
