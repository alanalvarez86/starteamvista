unit FConfigurarPoliza;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBCtrls, StdCtrls, Grids, DBGrids, ComCtrls, Buttons, ExtCtrls, Registry,
     ZBaseDlgModal, ZetaDBTextBox, DConfigPoliza;

type
  TConfigurarPoliza = class(TZetaDlgModal)
    PageControl: TPageControl;
    TabConceptos: TTabSheet;
    gridConceptos: TDBGrid;
    Panel2: TPanel;
    btnLlenaConceptos: TBitBtn;
    dbnConceptos: TDBNavigator;
    btnAbreConceptos: TBitBtn;
    btnExportaConceptos: TBitBtn;
    TabGrupos: TTabSheet;
    Panel4: TPanel;
    cbGrupoLBL: TLabel;
    cbGrupo: TComboBox;
    btnImportaGrupo: TBitBtn;
    dbnGrupo: TDBNavigator;
    btnAbreGrupo: TBitBtn;
    btnExportaGrupo: TBitBtn;
    btnBorraTodos: TBitBtn;
    gridGrupo: TDBGrid;
    TabAsientos: TTabSheet;
    Panel3: TPanel;
    editCuentaLBL: TLabel;
    btnGenerar: TBitBtn;
    editCuenta: TEdit;
    dbnPoliza: TDBNavigator;
    gridPoliza: TDBGrid;
    Formulas: TTabSheet;
    Panel5: TPanel;
    PolizaLBL: TLabel;
    btnBorrarTodos: TBitBtn;
    btnAgregar: TBitBtn;
    gridCampoRep: TDBGrid;
    OpenCDS: TOpenDialog;
    SaveCDS: TSaveDialog;
    dsGrupo: TDataSource;
    dsCtaConceptos: TDataSource;
    dsPoliza: TDataSource;
    dsCampoRep: TDataSource;
    Poliza: TZetaTextBox;
    pbAgregar: TProgressBar;
    Label1: TLabel;
    Edit1: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnLlenaConceptosClick(Sender: TObject);
    procedure btnAbreConceptosClick(Sender: TObject);
    procedure btnExportaConceptosClick(Sender: TObject);
    procedure btnImportaGrupoClick(Sender: TObject);
    procedure btnAbreGrupoClick(Sender: TObject);
    procedure btnExportaGrupoClick(Sender: TObject);
    procedure btnBorraTodosClick(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnBorrarTodosClick(Sender: TObject);
    procedure cbGrupoClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FRegistry: TRegistry;
    FListGrupos: TStrings;
    function GetExportFileName(var sFile: String): Boolean;
    function GetGroupType: eGroupType;
    function GetImportFileName(var sFile: String): Boolean;
    procedure ConectarGrupo;
    procedure SetControls;
    procedure MoveProgressBar(Sender: TObject);
    procedure DisConnect;
  public
    { Public declarations }
    procedure Connect;
    property Grupos: TStrings read FListGrupos write FListGrupos;
  end;

var
  ConfigurarPoliza: TConfigurarPoliza;

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo,
     FListaConceptos,
     DReportes;

const
     K_KEY_ESTRUCTURA = 'Estructura';

{$R *.DFM}

{ ********** TFormaCreaPoliza ************ }

procedure TConfigurarPoliza.FormCreate(Sender: TObject);
var
   sEstructura: String;
begin
     inherited;
     HelpContext := H50519_Generador_Cuentas_contables;
     FRegistry := TRegistry.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          if OpenKey( 'Software\Grupo Tress\CreaPoliza', TRUE ) then
          begin
               sEstructura := ReadString( K_KEY_ESTRUCTURA );
               if ZetaCommonTools.StrLleno( sEstructura ) then
                  editCuenta.Text := sEstructura;
          end;
     end;
end;


procedure TConfigurarPoliza.FormShow(Sender: TObject);
begin
     inherited;
     Connect;
     PageControl.ActivePage := TabConceptos;
end;

procedure TConfigurarPoliza.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     Disconnect;
end;

procedure TConfigurarPoliza.FormDestroy(Sender: TObject);
begin
     inherited;
     if ( FRegistry <> NIL ) then
     begin
          with FRegistry do
          begin
               WriteString( K_KEY_ESTRUCTURA, editCuenta.Text );
               CloseKey;
          end;
          FreeAndNil( FRegistry );
     end;

     FreeAndNil( ListaConceptos );
end;

procedure TConfigurarPoliza.DisConnect;
begin
     dmConfigPoliza.DesconectaConfiguraPoliza;
end;

procedure TConfigurarPoliza.Connect;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if dmConfigPoliza = NIL then
           dmConfigPoliza := TdmConfigPoliza.Create( self );


        with dmConfigPoliza do
        begin
             if ConectaConfiguraPoliza then
             begin
                  Caption := Format( 'Configurar P�liza Contable # %d %s', [ PolizaActiva, PolizaNombre ] );
                  Poliza.Caption := Format( '# %d %s', [ PolizaActiva, PolizaNombre ] );

                  with cbGrupo do
                  begin
                       LlenaPolizaGrupos( Items, FListGrupos );
                       if ( Items.Count > 0 ) then
                          ItemIndex := 0;
                  end;
                  ConectarGrupo;
                  dsCtaConceptos.Dataset := cdsCtaConcepto;
                  dsPoliza.Dataset := cdsPoliza;
                  dsCampoRep.Dataset := dmReportes.cdsCampoRep;
                  SetControls;
             end
             else
                 ZetaDialogo.zError( '� Atenci�n !', 'No Hay Reportes Tipo POLIZA En Esta Base De Datos', 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TConfigurarPoliza.GetGroupType: eGroupType;
begin
     Result := eGroupType( cbGrupo.ItemIndex );
end;

procedure TConfigurarPoliza.ConectarGrupo;
begin
     dsGrupo.Dataset := dmConfigPoliza.GetDatasetGrupo( GetGroupType );
end;

function TConfigurarPoliza.GetImportFileName( var sFile: String ): Boolean;
begin
     Result := False;
     with OpenCDS do
     begin
          FileName := sFile;
          InitialDir := ExtractFilePath( sFile );
          if Execute then
          begin
               Result := True;
               sFile := FileName;
          end;
     end;
end;

function TConfigurarPoliza.GetExportFileName( var sFile: String ): Boolean;
begin
     Result := False;
     with SaveCDS do
     begin
          FileName := sFile;
          //InitialDir := ExtractFilePath( sFile );
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               Result := True;
               sFile := FileName;
          end;
     end;
end;

procedure TConfigurarPoliza.SetControls;
begin
     with dsPoliza do
     begin
          if Assigned( Dataset ) then
          begin
               btnAgregar.Enabled := True;
          end
          else
              btnAgregar.Enabled := False;
     end;
     with dsCampoRep do
     begin
          if Assigned( Dataset ) then
          begin
               btnBorrarTodos.Enabled := not Dataset.IsEmpty;
          end
          else
              btnBorrarTodos.Enabled := False;
     end;
end;

{ ******* Eventos de Botones ********** }

procedure TConfigurarPoliza.cbGrupoClick(Sender: TObject);
begin
     inherited;
     ConectarGrupo;
end;

procedure TConfigurarPoliza.btnLlenaConceptosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ListaConceptos = NIL then
           ListaConceptos := TListaConceptos.Create( self );
           
        with ListaConceptos do
        begin
             ShowModal;
             if ModalResult = mrOK then
                dmConfigPoliza.LlenaConceptos( Conceptos );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza.btnAbreConceptosClick(Sender: TObject);
var
   sFile: String;
begin
     inherited;
     sFile := 'CtaConcepto.cds';
     if GetImportFileName( sFile ) then
        dmConfigPoliza.ImportaCtaConcepto( sFile );
end;

procedure TConfigurarPoliza.btnExportaConceptosClick(Sender: TObject);
var
   sFile: String;
begin
     inherited;
     sFile := 'CtaConcepto.cds';
     if GetExportFileName( sFile ) then
        dmConfigPoliza.ExportaCtaConcepto( sFile );
end;

procedure TConfigurarPoliza.btnImportaGrupoClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmConfigPoliza.ImportaGrupoTress( GetGroupType );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza.btnAbreGrupoClick(Sender: TObject);
var
   sFile: String;
   eGrupo: eGroupType;
begin
     inherited;
     eGrupo := GetGroupType;
     sFile := Format( 'CtaGrupo%d', [ Ord( eGrupo ) ] );
     if GetImportFileName( sFile ) then
        dmConfigPoliza.ImportaGrupo( eGrupo, sFile );
end;

procedure TConfigurarPoliza.btnExportaGrupoClick(Sender: TObject);
var
   sFile: String;
   eGrupo: eGroupType;
begin
     inherited;
     eGrupo := GetGroupType;
     sFile := Format( 'CtaGrupo%d', [ Ord( eGrupo ) ] );
     if GetExportFileName( sFile ) then
        dmConfigPoliza.ExportaGrupo( eGrupo, sFile );
end;

procedure TConfigurarPoliza.btnBorraTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm('� Atenci�n !', '� Desea Borrar Todos Estos Grupos ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmConfigPoliza.BorraGruposTodos( GetGroupType );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TConfigurarPoliza.btnGenerarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmConfigPoliza do
        begin
             GenerarPoliza( editCuenta.Text );
        end;
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza.btnConsultarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza.MoveProgressBar(Sender: TObject);
begin
     pbAgregar.StepIt;
end;

procedure TConfigurarPoliza.btnAgregarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     with dmConfigPoliza do
     begin
          if ZetaDialogo.zConfirm( '� Atenci�n !', Format( '� Desea Agregar Los %d Nuevos Asientos A La P�liza ?', [ CuantosAsientos ] ), 0, mbNo ) then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  with pbAgregar do
                  begin
                       Max := CuantosAsientos;
                       Visible := True;
                       try
                          AgregaAsientos( MoveProgressBar );
                          SetControls;
                       finally
                              Visible := False;
                       end;
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end;
end;

procedure TConfigurarPoliza.btnBorrarTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm('� Atenci�n !', '� Desea Borrar Todas Las F�rmulas De Esta P�liza ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmConfigPoliza.BorraFormulasPoliza;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TConfigurarPoliza.OKClick(Sender: TObject);
 var
    oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm('� Atenci�n !', '� Desea Grabar Esta P�liza ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             ModalResult := mrOk
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
         ModalResult := mrNone;
end;

end.

