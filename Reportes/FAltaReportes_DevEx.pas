unit FAltaReportes_DevEx;

interface
{$INCLUDE DEFINES.INC}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  CheckLst, Db, DBCtrls, Mask,Variants,ZReportToolsConsts,
  ZetaCommonLists, ZetaTipoEntidad, ZetaKeyCombo,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxContainer,
  cxEdit, ImgList, cxImage, cxButtons, ZetaWizard_DevEx, cxListBox,
  ZcxWizardBasico, ZetaCXWizard,
  cxGroupBox, cxLabel, dxGDIPlusClasses, dxCustomWizardControl,
  dxWizardControl, ZcxBaseWizard, cxCheckListBox, cxRadioGroup;

type
  TAltaReportes_DevEx = class(TcxBaseWizard)
    DataSource: TDataSource;
    tsTipoReporte: TdxWizardControlPage;
    Forma: TImage;
    Etiqueta: TImage;
    Listado: TImage;
    RE_TIPO: TcxRadioGroup;
    Poliza: TImage;
    IRapida: TImage;
    GbClasificacion: TGroupBox;
    rgClasificacion: TcxListBox;
    gbTablaPrincipal: TGroupBox;
    LBTablas: TcxListBox;
    tsCampos: TdxWizardControlPage;
    GroupBox2: TGroupBox;
    CBCampos: TcxCheckListBox;
    RE_NOMBRE: TDBEdit;
    Consultar: TcxButton;
    cbChecaTodos: TCheckBox;
    RE_NOMBRE2: TDBEdit;
    Label2: TLabel;
    lbPlantilla: TLabel;
    cbTipoPoliza: TZetaKeyCombo;
    RE_REPORTE: TDBEdit;
    bPlantilla: TcxButton;
    RE_ENTIDAD: TZetaKeyCombo;
    lbTablaPrincipal: TLabel;
    Label1: TLabel;
    Apagar: TcxButton;
    Prender: TcxButton;
    procedure RE_TIPOClick(Sender: TObject);
    procedure rgClasificacionClick(Sender: TObject);
    procedure LBTablasDblClick(Sender: TObject);
    procedure ConsultarClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;var CanMove: Boolean);
    procedure bPlantillaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbChecaTodosClick(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
  private
  {$ifdef RDD}
  {$else}
    FRequeridos : string;
  {$endif}

    UltimaEntidad : TipoEntidad;
    UltimaTipo : eTipoReporte;
    //Ninguna tabla tiene mas de 11 campos por defaults,
    //esto esta definido en el campo DI_REQUIER de la tabla DICCION
    {$ifdef RDD}
    {$else}
    Arreglo : Array[0..10] of String;
    {$endif}

    procedure DoConnect;
    procedure DoDisconnect;
    procedure Connect;
    procedure Disconnect;
    procedure LlenaListBox;
    procedure GrabaCampoRep;
    function CamposPorEntidad: Boolean;
    function GetTipoReporte: eTipoReporte;
    function GetEntidad: TipoEntidad;
    procedure LimpiaListas;

  public
    { Public declarations }
  end;

var
  AltaReportes_DevEx: TAltaReportes_DevEx;

implementation
uses DCliente,
     DReportes,
     DDiccionario,
     DBaseDiccionario,
     DGlobal,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaDialogo,
     ZReportTools,
     ZReportConst,
     ZetaCommonClasses, dSistema;

{$R *.DFM}


procedure TAltaReportes_DevEx.DoConnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
             except
                   on Error: Exception do
                      zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TAltaReportes_DevEx.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TAltaReportes_DevEx.Disconnect;
begin
     Datasource.Dataset := nil;
     LimpiaListas;
end;

procedure TAltaReportes_DevEx.LimpiaListas;
 var i : integer;
begin
     for i:= 0 to CBCampos.Items.Count - 1 do
         CBCampos.Items[i].Checked := FALSE;
end;

procedure TAltaReportes_DevEx.Connect;
begin
     with dmReportes do
     begin
          cdsEditReporte.Conectar;
          DataSource.DataSet := cdsEditReporte;
          cdsCampoRep.Conectar;
     end;

     {$ifdef RDD}
     dmDiccionario.LlenaArregloEntidades;
     {$else}
     {$endif}
     LlenaTipoPoliza(RE_ENTIDAD.Lista);
     RE_ENTIDAD.ItemIndex := 0;
     RGClasificacion.ItemIndex := dmDiccionario.GetPosClasifi(RGClasificacion.Items, dmReportes.ClasifActivo);
     RGClasificacionClick(Self);
     RE_TIPOClick(Self);
end;

procedure TAltaReportes_DevEx.RE_TIPOClick(Sender: TObject);
 var lGenerales, lPlantilla, lTipoReporte, lCampos, lSoloNombre,lPoliza: Boolean;
     oImagen: TImage;
begin
     inherited;
     lTipoReporte := TRUE;
     lGenerales := TRUE;
     lCampos := TRUE;
     lPlantilla := TRUE;
     lSoloNombre := FALSE;
     lPoliza := FALSE;

     oImagen := nil;

     case GetTipoReporte of
          trListado:
          begin
               lPlantilla := FALSE;
               oImagen := Listado;
          end;
          trForma:
          begin
               lCampos := FALSE;
               oImagen := Forma;
          end;
          trEtiqueta:
          begin
               lCampos := FALSE;
               oImagen := Etiqueta;
          end;
          trPoliza,trPolizaConcepto:
          begin
               lPoliza := TRUE;
               lGenerales := FALSE;
               lCampos := FALSE;
               lSoloNombre := TRUE;
               oImagen := Poliza;
          end;
          trImpresionRapida:
          begin
               lCampos := FALSE;
               lSoloNombre := TRUE;
               oImagen := iRapida;
          end;
     end;

     if ( oImagen <> NIL ) then
     begin
          oImagen.Left := 24;
          oImagen.Top := 31;
          oImagen.Visible := TRUE;
          oImagen.BringToFront;
     end;

     if GetTipoReporte = trListado then
     begin
          ejecucion.PageIndex := 2;
          tsCampos.PageIndex := 3;
     end
     else
     begin
          ejecucion.PageIndex := 3;
          tsCampos.PageIndex := 2;
     end;
     Parametros.Pagevisible := lGenerales;
     ejecucion.Pagevisible := lPlantilla;
     tsTipoReporte.Pagevisible := lTipoReporte;
     tsCampos.Pagevisible := lCampos;

     lbPlantilla.Visible := NOT lSoloNombre or lPoliza;
     RE_REPORTE.Visible := NOT lSoloNombre;
     bPlantilla.Visible := NOT lSoloNombre;

     lbTablaPrincipal.Visible := (Not lGenerales) and lSoloNombre;
     RE_ENTIDAD.Visible := (Not lGenerales) and lSoloNombre;
     cbTipoPoliza.Visible := lPoliza;

     if lPoliza then
       lbPlantilla.Caption := 'Tipo de p�liza:'
     else
         lbPlantilla.Caption := 'Nombre plantilla:';
end;

procedure TAltaReportes_DevEx.rgClasificacionClick(Sender: TObject);
begin
     inherited;
     LlenaListBox;
end;

procedure TAltaReportes_DevEx.LBTablasDblClick(Sender: TObject);
begin
     inherited;
     Wizard.Siguiente;
end;

procedure TAltaReportes_DevEx.ConsultarClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizadoReportes( GetEntidad ) then
     begin
        with dmReportes.cdsEditReporte do
        begin
             FieldByName('RE_TIPO').AsInteger := Ord(GetTipoReporte);
             FieldByName('RE_ENTIDAD').AsInteger := Ord(GetEntidad);
             {$ifdef RDD}
             if dmDiccionario.ExisteEntidadEnTablasPorClasificacion( FieldByName('RE_ENTIDAD').AsInteger ) then
             begin
                  FieldByName('EN_NIVEL0').AsString := dmDiccionario.GetEsTablaNivel0;
                  FieldByName('EN_TABLA').AsString := dmDiccionario.GetNombreTabla(FieldByName('RE_ENTIDAD').AsInteger);

                  GrabaCampoRep;
             end
             else
                 ZError( Caption, Format( 'Error al buscar la Tabla #%d.' + CR_LF + 'El reporte no pudo ser dado de alta.' , [ Ord( FieldByName('RE_ENTIDAD').AsInteger ) ] ),0 );
             {$else}
             GrabaCampoRep;
             {$endif}
        end;
        dmReportes.GetPreviewReporte( trListado );
     end;
end;

procedure TAltaReportes_DevEx.CancelarClick(Sender: TObject);
begin
     inherited;
     Wizard.Cancelar
end;

procedure TAltaReportes_DevEx.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     //inherited;
     if ZetaDialogo.ZConfirm(Caption, '� Desea Cancelar la Creaci�n de Reportes ?', 0, mbNo ) then
     begin
          dmReportes.cdsEditReporte.Cancel;
          lOk := TRUE;
          Close;
     end
     else lOk := FALSE;
end;

function TAltaReportes_DevEx.GetEntidad : TipoEntidad;
begin                                      
     if GetTipoReporte in  [trPoliza,trPolizaConcepto ]then
        Result := GetEntidadPoliza(RE_ENTIDAD.ItemIndex)
     else
         with LBTablas do
              Result := TipoEntidad(Items.Objects[ItemIndex]);
end;

{$ifdef RDD}
procedure TAltaReportes_DevEx.GrabaCampoRep;

 procedure AgregaRegistro( const iTipo, iPos, iSubPos, iTabla, iCalc, iAncho, iOper, iTField : integer;
                           const sNombre, sTitulo, sMascara, sRequier : string;
                           const diNumero: integer = 0 );
 begin
      with dmReportes.cdsCampoRep do
      begin
           FieldByName('CR_TIPO').AsInteger := iTipo;
           FieldByName('CR_POSICIO').AsInteger := iPos;
           FieldByName('CR_SUBPOS').AsInteger := iSubPos;
           FieldByName('CR_TABLA').AsInteger := iTabla;
           FieldByName('CR_CLASIFI').AsInteger := iTabla;
           FieldByName('CR_FORMULA').AsString := sNombre;
           FieldByName('CR_CALC').AsInteger := iCalc;
           FieldByName('CR_ANCHO').AsInteger := iAncho;
           FieldByName('CR_MASCARA').AsString := sMascara;
           FieldByName('CR_TFIELD').AsInteger := iTField;
           FieldByName('CR_OPER').AsInteger := iOper;
           FieldByName('CR_REQUIER').AsString := sRequier;
           FieldByName('CR_TITULO').AsString := sTitulo;
      end;
 end;

 function GetFiltro( const eTipo: eTipoGlobal; const eFiltro: integer ): string;
 begin
      case eTipo of
           tgTexto: Result := ObtieneElemento( lfRDDDefaultsTexto, eFiltro );
           tgFloat: Result := ObtieneElemento( lfRDDDefaultsFloat, eFiltro )
           else Result := VACIO;
      end;
 end;


 var  i: integer;
      sCampo : string;
      eTipo : eTipoReporte;
begin
     dmReportes.cdsCampoRep.EmptyDataSet;
     dmSistema.cdsSuscrip.EmptyDataSet;
     eTipo := GetTipoReporte;

     //ORDEN
     if not (eTipo in [trPoliza,trPolizaConcepto]) then
     begin
          with dmDiccionario,cdsDatosDefault do
          begin
               cdsBuscaPorTabla.Filtered := FALSE;
               dmDiccionario.SetFiltroCamposOrden;
               try
                  while NOT EOF do
                  begin
                       if cdsBuscaPorTabla.Locate( 'EN_CODIGO;AT_CAMPO',
                                                   VarArrayOf([ FieldByName('EN_CODIGO').AsInteger, FieldByName('AT_CAMPO').AsString ]),[]) then
                       begin
                            dmReportes.cdsCampoRep.Append;
                            AgregaRegistro( Ord( tcOrden ),
                                            0,
                                            -1,
                                            cdsBuscaPorTabla.FieldByName('EN_CODIGO').AsInteger,
                                            0, 0, 0,
                                            cdsBuscaPorTabla.FieldByName('AT_TIPO').AsInteger,
                                            cdsBuscaPorTabla.FieldByName('EN_TABLA').AsString + '.'+ cdsBuscaPorTabla.FieldByName('AT_CAMPO').AsString,
                                            cdsBuscaPorTabla.FieldByName('AT_TCORTO').AsString,
                                            '', '' );
                            dmReportes.cdsCampoRep.Post;
                       end;
                       Next;
                  end;
               finally
                      dmDiccionario.SetFiltroCamposOrden(FALSE);
               end;

               //CAMPOS
               for i := 0 to CBCampos.Items.Count - 1 do
                   if CBCampos.items[i].Checked then
                   begin
                        sCampo := TObjetoString(CBCampos.Items[i].tag).Campo;
                        if cdsBuscaPorTabla.Locate( 'EN_CODIGO;AT_CAMPO', VarArrayOf([TObjetoString(CBCampos.Items[i].tag).Entidad,sCampo]),[]) then
                        begin
                             dmReportes.cdsCampoRep.Append;
                             AgregaRegistro( Ord( tcCampos ),
                                                     i, -1,
                                                     cdsBuscaPorTabla.FieldByName('EN_CODIGO').AsInteger,
                                                     0,
                                                     cdsBuscaPorTabla.FieldByName('AT_ANCHO').AsInteger,
                                                     cdsBuscaPorTabla.FieldByName('AT_TOTAL').AsInteger,
                                                     cdsBuscaPorTabla.FieldByName('AT_TIPO').AsInteger,
                                                     cdsBuscaPorTabla.FieldByName('EN_TABLA').AsString + '.'+ cdsBuscaPorTabla.FieldByName('AT_CAMPO').AsString,
                                                     cdsBuscaPorTabla.FieldByName('AT_TCORTO').AsString,
                                                     cdsBuscaPorTabla.FieldByName('AT_MASCARA').AsString,
                                                     VACIO,
                                                     0 );
                             dmReportes.cdsCampoRep.Post;
                        end;
                   end;
         //Grupo Nivel Empresa
         if GetTipoReporte in [ trListado, trImpresionRapida ] then
         begin
              with dmReportes.cdsCampoRep do
              begin
                   Append;
                   AgregaRegistro( Ord(tcGrupos),
                                   0,-1,0,0,
                                   0, 1, 1,
                                   '', K_EMPRESA, '', '' );
                   FieldByName('CR_SHOW').AsInteger := 1;
                   Post;
              end;
         end;

         //FILTROS

         {$ifdef RDD}
         i := 0;
         with dmDiccionario do
         begin
              SetFiltroCamposFiltro;
              try
                 while NOT cdsDatosDefault.EOF do
                 begin
                      {$ifdef TRESS}
                      if
                      {$ifndef RDDAPP}
                         ( Global.GetGlobalBooleano ( K_GLOBAL_CB_ACTIVO_AL_DIA ) ) AND
                         ( strLLeno( Global.GetGlobalString( K_GLOBAL_FORMULA_STATUS_ACTIVO ) ) ) AND
                      {$endif}
                         ( dmReportes.cdsEditReporte.FieldByName('RE_ENTIDAD').AsInteger = Ord( enEmpleado ) ) AND
                         ( cdsDatosDefault.FieldByName('AT_CAMPO').ASString = 'CB_ACTIVO' ) then
                      begin
                           dmReportes.cdsEditReporte.FieldByName('RE_FILTRO').AsString := Global.GetGlobalString( K_GLOBAL_FORMULA_STATUS_ACTIVO );
                      end
                      else
                      {$endif}
                      begin
                           with cdsBuscaPorTabla do
                           begin
                                if Locate('EN_CODIGO;AT_CAMPO',VarArrayOf([cdsDatosDefault.FieldByName('EN_CODIGO').AsInteger,cdsDatosDefault.FieldByName('AT_CAMPO').AsString]),[]) then
                                begin
                                     dmReportes.cdsCampoRep.Append;
                                     AgregaRegistro( Ord( tcFiltro ),
                                                     i, -1,
                                                     FieldByName('EN_CODIGO').AsInteger,
                                                     0,
                                                     FieldByName('AT_FILTRO').AsInteger,
                                                     FieldByName('AT_ENTIDAD').AsInteger + FieldByName('LV_CODIGO').AsInteger,
                                                     FieldByName('AT_TIPO').AsInteger,
                                                     FieldByName('EN_TABLA').AsString + '.'+ FieldByName('AT_CAMPO').AsString,
                                                     FieldByName('AT_TITULO').AsString,
                                                     '', GetFiltro(eTipoGlobal( FieldByName('AT_TIPO').AsInteger ), FieldByName('AT_FILTRO').AsInteger)  );

                                          dmReportes.cdsCampoRep.FieldByName('CR_CALC').AsInteger := FieldByName('AT_TRANGO').AsInteger;
                                          dmReportes.cdsCampoRep.FieldByName('CR_DESCRIP').AsInteger := FieldByName('AT_VALORAC').AsInteger;
                                          if eTipoGlobal(FieldByName('AT_TIPO').AsInteger) = tgBooleano then
                                             dmReportes.cdsCampoRep.FieldByName('CR_REQUIER').AsInteger:=0;
                                          {case eTipoRango(FieldByName('DI_TRANGO').AsInteger) of
                                               rNinguno : cdsCampoRep.FieldByName('CR_REQUIER').AsString:=FieldByName('DI_VALORAC').AsString;
                                               rBool :
                                               begin
                                                    if FieldByName('DI_VALORAC').AsString = 'S' then cdsCampoRep.FieldByName('CR_REQUIER').AsInteger:=0
                                                    else cdsCampoRep.FieldByName('CR_REQUIER').AsInteger := 1;
                                               end;
                                          end; }
                                     dmReportes.cdsCampoRep.Post;
                                end;
                                Inc(i);
                           end;
                      end;
                      cdsDatosDefault.Next;
                 end;
              finally
                     SetFiltroCamposFiltro(FALSE);
              end;
         end;
         {$endif}
         end;
     end
end;
{$else}
procedure TAltaReportes_DevEx.GrabaCampoRep;
 procedure AgregaRegistro( const iTipo, iPos, iSubPos, iTabla, iCalc, iAncho, iOper, iTField : integer;
                           const sNombre, sTitulo, sMascara, sRequier : string;
                           const diNumero: integer = 0 );
 begin
      with dmReportes.cdsCampoRep do
      begin
           FieldByName('CR_TIPO').AsInteger := iTipo;
           FieldByName('CR_POSICIO').AsInteger := iPos;
           FieldByName('CR_SUBPOS').AsInteger := iSubPos;
           FieldByName('CR_TABLA').AsInteger := iTabla;
           FieldByName('CR_CLASIFI').AsInteger := iTabla;
           FieldByName('CR_FORMULA').AsString := sNombre;
           FieldByName('CR_CALC').AsInteger := iCalc;
           FieldByName('CR_ANCHO').AsInteger := iAncho;
           FieldByName('CR_MASCARA').AsString := sMascara;
           FieldByName('CR_TFIELD').AsInteger := iTField;
           if (eTipoCampo(iTipo)=tcCampos) and
              (eTipoGlobal(iTField) in [tgFloat,tgNumero]) and
              (diNumero = 0) then
              FieldByName('CR_OPER').AsInteger := Ord(ocSuma)
           else FieldByName('CR_OPER').AsInteger := iOper;
           FieldByName('CR_REQUIER').AsString := sRequier;
           FieldByName('CR_TITULO').AsString := sTitulo;
      end;
 end;

 var  i: integer;
      sCampo : string;
      eTipo : eTipoReporte;
begin
     dmReportes.cdsCampoRep.EmptyDataSet;
     dmSistema.cdsSuscrip.EmptyDataSet;
     eTipo := GetTipoReporte;

     //ORDEN
     if not (eTipo in [trPoliza,trPolizaConcepto]) then
     begin
          sCampo := Arreglo[0];
          if NOT dmDiccionario.cdsBuscaPorTabla.Active then
             CamposPorEntidad;

          if NOT(GetEntidad in ReportesSinOrdenDef) then
          begin
               if dmDiccionario.cdsBuscaPorTabla.Locate( 'DI_NOMBRE', sCampo,[]) then
               begin
                    dmReportes.cdsCampoRep.Append;
                    with dmDiccionario, cdsBuscaPorTabla do
                         AgregaRegistro( Ord( tcOrden ),
                                         0,
                                         -1,
                                         FieldByName('DI_CLASIFI').AsInteger,
                                          0, 0, 0,
                                         FieldByName('DI_TFIELD').AsInteger,
                                         FieldByName('DI_TABLA').AsString + '.'+ FieldByName('DI_NOMBRE').AsString,
                                         FieldByName('DI_TCORTO').AsString,
                                         '', '' );
                    dmReportes.cdsCampoRep.Post;
               end;
          end;
         //CAMPOS
         for i := 0 to CBCampos.Items.Count - 1 do
             if CBCampos.items[i].Checked then
             begin
                  sCampo := TObjetoString(CBCampos.Items[i].tag).Campo;
                  if dmDiccionario.cdsBuscaPorTabla.Locate( 'DI_NOMBRE', sCampo,[]) then
                     with dmReportes.cdsCampoRep, dmDiccionario do
                     begin
                          Append;
                          with cdsBuscaPorTabla do
                          begin
                               AgregaRegistro( Ord( tcCampos ),
                                               i, -1,
                                               FieldByName('DI_CLASIFI').AsInteger,
                                               FieldByName('DI_CALC').AsInteger,
                                               FieldByName('DI_ANCHO').AsInteger,
                                               0,
                                               FieldByName('DI_TFIELD').AsInteger,
                                               FieldByName('DI_TABLA').AsString + '.'+ FieldByName('DI_NOMBRE').AsString,
                                               FieldByName('DI_TCORTO').AsString,
                                               FieldByName('DI_MASCARA').AsString,
                                               FieldByName('DI_REQUIER').AsString,
                                               FieldByName('DI_NUMERO').AsInteger );

                               if eTipoGlobal(FieldByName('DI_TFIELD').AsInteger) = tgFloat then
                                  dmReportes.cdsCampoRep.FieldByName('CR_OPER').AsInteger := Ord(ocSuma);
                          end;
                          Post;
                     end;
             end;
             {$ifdef TRESS}
             if CBCampos.Items.Count > 0 then
             with CBCampos do
                  if Items[i].Checked AND
                     ( ( TObjetoString(Items[i].tag).Campo = PRETTY ) or
                       ( TObjetoString(Items[i].tag).Campo = Q_PRETTY_EXP ) ) then
                     with dmReportes.cdsCampoRep do
                     begin
                          Append;
                          AgregaRegistro( Ord( tcCampos ),
                                          1, -1,
                                          Ord(enEmpleado),
                                          -1,
                                          30,
                                          0,
                                          Ord(tgTexto),
                                          TObjetoString(Items[i].tag).Campo,
                                          'Nombre', '', '' );
                          Post;
                     end;
             {$endif}
         //Grupo Nivel Empresa
         if GetTipoReporte in [ trListado, trImpresionRapida ] then
         begin
              with dmReportes.cdsCampoRep do
              begin
                   Append;
                   AgregaRegistro( Ord(tcGrupos),
                                   0,-1,0,0,
                                   0, 1, 1,
                                   '', K_EMPRESA, '', '' );
                   FieldByName('CR_SHOW').AsInteger := 1;
                   Post;
              end;
         end;

         //FILTROS
         i := 0;
         with dmDiccionario, dmReportes do
         begin
              cdsBuscaPorTabla.First;
              while NOT cdsBuscaPorTabla.EOF do
              begin
                   if cdsBuscaPorTabla.FieldByName('DI_RANGOAC').AsInteger = 1 then
                   begin
                        {$ifdef TRESS}
                        if ( Global.GetGlobalBooleano ( K_GLOBAL_CB_ACTIVO_AL_DIA ) ) AND
                           ( strLLeno( Global.GetGlobalString( K_GLOBAL_FORMULA_STATUS_ACTIVO ) ) ) AND
                           ( dmReportes.cdsEditReporte.FieldByName('RE_ENTIDAD').AsInteger = Ord( enEmpleado ) ) AND
                           ( cdsBuscaPorTabla.FieldByName('DI_NOMBRE').ASString = 'CB_ACTIVO' ) then
                        begin
                              dmReportes.cdsEditReporte.FieldByName('RE_FILTRO').AsString := Global.GetGlobalString( K_GLOBAL_FORMULA_STATUS_ACTIVO );
                        end
                        else
                        {$ENDIF}
                        begin
                             cdsCampoRep.Append;
                             with cdsBuscaPorTabla do
                             begin
                                  AgregaRegistro( Ord( tcFiltro ),
                                                  i, -1,
                                                  FieldByName('DI_CLASIFI').AsInteger,
                                                  0, FieldByName('DI_RANGOAC').AsInteger,
                                                  FieldByName('DI_NUMERO').AsInteger,
                                                  FieldByName('DI_TFIELD').AsInteger,
                                                  FieldByName('DI_TABLA').AsString + '.'+ FieldByName('DI_NOMBRE').AsString,
                                                  FieldByName('DI_TITULO').AsString,
                                                  '', '' );

                                  cdsCampoRep.FieldByName('CR_CALC').AsInteger := FieldByName('DI_TRANGO').AsInteger;
                                  cdsCampoRep.FieldByName('CR_DESCRIP').AsString := FieldByName('DI_VALORAC').AsString;

                                  case eTipoRango(FieldByName('DI_TRANGO').AsInteger) of
                                       rNinguno : cdsCampoRep.FieldByName('CR_REQUIER').AsString:=FieldByName('DI_VALORAC').AsString;
                                       rBool :
                                       begin
                                            if FieldByName('DI_VALORAC').AsString = 'S' then cdsCampoRep.FieldByName('CR_REQUIER').AsInteger:=0
                                            else cdsCampoRep.FieldByName('CR_REQUIER').AsInteger := 1;
                                       end;
                                  end;
                             end;
                             cdsCampoRep.Post;
                             Inc(i);
                        end;
                   end;
                   cdsBuscaPorTabla.Next;
              end;
         end;
     end
 
end;
{$endif}


procedure TAltaReportes_DevEx.WizardAlEjecutar(Sender: TObject;
  var lOk: Boolean);
begin
   //  inherited;
     with dmReportes.cdsEditReporte do
     begin
          FieldByName('RE_TIPO').AsInteger := Ord(GetTipoReporte);
          FieldByName('RE_ENTIDAD').AsInteger := Ord(GetEntidad);
          {$ifdef RDD}
          if ( FindField('RE_TABLA') <> NIL ) then
             FieldByName('RE_TABLA').AsString := dmReportes.ObtieneEntidad( FieldByName('RE_ENTIDAD').AsInteger );
          {$endif}
     {$ifndef RDD}
          GrabaCampoRep;
          Modificar;
     end;
     lOk := True;
     Close;
     {$else}
          lOK:= dmDiccionario.ExisteEntidadEnTablasPorClasificacion( FieldByName('RE_ENTIDAD').AsInteger ) ;
          if lOK then
          begin
               FieldByName('EN_NIVEL0').AsString := dmDiccionario.GetEsTablaNivel0;
               FieldByName('EN_TABLA').AsString := dmDiccionario.GetNombreTabla(FieldByName('RE_ENTIDAD').AsInteger);

               GrabaCampoRep;
               Modificar;
          end
          else
              ZError( Caption, Format( 'Error al buscar la Tabla #%d.' + CR_LF + 'El reporte no pudo ser dado de alta.' , [ Ord( FieldByName('RE_ENTIDAD').AsInteger ) ] ),0 );
     end;
     Close;
     {$endif}
end;

function TAltaReportes_DevEx.GetTipoReporte : eTipoReporte;
begin
     //eTipoReporte = (trListado,trForma,trEtiqueta,trPoliza, trPolizaConcepto, trImpresionRapida, trFuturo);
     {Lista en RE_TIPO:
             0= trListado,
             1=trForma,
             2=trEtiqueta,
             3=trPoliza,
             4=trImpresionRapida}

     if RE_TIPO.ItemIndex = RE_TIPO.Properties.Items.Count - 1 then
        Result := trImpresionRapida
     else
     begin
          Result := eTipoReporte( RE_TIPO.ItemIndex );
          if ( Result = trPoliza ) then
          begin
               if ( cbTipoPoliza.ItemIndex = 0 ) then
                  Result := trPolizaConcepto;
          end
     end;
end;

function TAltaReportes_DevEx.CamposPorEntidad: Boolean;
  var oEntidadTemp, EntidadActual : TipoEntidad;
  {$ifdef RDD}
  procedure SetCamposDefault;
   var
       h,k : integer;
       lEncontrado: Boolean;
       oCampo : TObjetoString;
  begin
       if GetTipoReporte = trListado then
       begin
            h:=0;
            with CBCampos do
            begin
                 with dmDiccionario.cdsDatosDefault do
                 begin
                      dmDiccionario.SetFiltroCamposDefault;
                      try
                         while NOT EOF do
                         begin
                              lEncontrado := FALSE;
                              for k:=0 to Items.Count -1 do
                              begin
                                   if TObjetoString(Items[k].tag).Campo = FieldByName('AT_CAMPO').AsString then
                                   begin
                                        Items[k].index := h;
                                        Items[h].Checked := TRUE;
                                        Inc(h);
                                        lEncontrado := TRUE;
                                        Break;
                                   end;
                              end;
                              if NOT lEncontrado then
                              begin
                                   dmDiccionario.cdsBuscaPorTabla.Filtered := FALSE;
                                   if dmDiccionario.cdsBuscaPorTabla.Locate('EN_CODIGO;AT_CAMPO', VarArrayof([FieldByName('EN_CODIGO').AsInteger,FieldByName('AT_CAMPO').AsString]),[])  then
                                   begin
                                   if h < k then
                                   begin
                                        oCampo := TObjetoString.Create;
                                        oCampo.Campo := FieldByName('AT_CAMPO').AsString;
                                        oCampo.Entidad := TipoEntidad( FieldByName('EN_CODIGO').AsInteger );
                                        Items[h].text :=  dmDiccionario.cdsBuscaPorTabla.FieldByName('AT_TITULO').AsString ;
                                        Items[h].tag := integer(oCampo);
                                        Items[h].Checked := TRUE;
                                        Inc(h);
                                   end
                                   else
                                   begin
                                   oCampo := TObjetoString.Create;
                                   oCampo.Campo := FieldByName('AT_CAMPO').AsString;
                                   oCampo.Entidad := TipoEntidad( FieldByName('EN_CODIGO').AsInteger );
                                   with Items.add do
                                   begin
                                    text :=  dmDiccionario.cdsBuscaPorTabla.FieldByName('AT_TITULO').AsString ;
                                    tag := integer(oCampo);
                                    Checked := TRUE;
                                   end;
                                   inc(h);
                                   end;
                              end;
                              end;
                              Next;
                         end;
                    finally
                           dmDiccionario.SetFiltroCamposDefault(FALSE);
                    end;
                 end;
            end;
       end
       else LimpiaListas;
  end;
{$else}
  procedure SetCamposDefault;

   var sRequeridos, sTabla : string;
       h, i,j,k,iPos : integer;
       oCampo : TObjetoString;
       sPretty : string;
  begin
       i := 0;
       sRequeridos := FRequeridos;
       while Pos( ';', sRequeridos ) > 0 do
       begin
            iPos := Pos( ';', sRequeridos );
            Arreglo[i] := Copy( sRequeridos, 1, iPos-1 );
            Arreglo[i] := Copy( Arreglo[i], Pos('.',Arreglo[i])+1, MAXINT );
            sRequeridos := Copy( sRequeridos, iPos+1, MaxInt );
            inc(i);
       end;
       Arreglo[i] := sRequeridos;
       Arreglo[i] := Copy( Arreglo[i], Pos('.',Arreglo[i])+1, MAXINT );


       if GetTipoReporte = trListado then
       begin
            h:=0;
            sTabla := dmDiccionario.cdsDiccion.FieldByName('DI_NOMBRE').AsString;
            with CBCampos do
            begin
                 for j:=0 to i do
                     for k:=0 to Items.Count -1 do
                     begin
                          if TObjetoString(Items[k].tag).Campo = Arreglo[j] then
                          begin
                               Items[k].index := h;
                               Items[h].Checked := TRUE;
                               Inc(h);                         
                               Break;
                          end;
                     end;

                     if ( High( Arreglo ) >= 1) then
                     begin
                          if ( Arreglo[1] = Q_PRETTY_EMP ) or
                             ( Arreglo[1] = Q_PRETTY_EXP ) then
                          begin
                               oCampo := TObjetoString.Create;

                               if ( Arreglo[1] = Q_PRETTY_EMP ) then
                                  sPretty := PRETTY
                               else if ( Arreglo[1] = Q_PRETTY_EXP ) then
                                    sPretty := Q_PRETTY_EXP;

                               oCampo.Campo := sPretty;
                               items[1].text :=  'Nombre Completo';
                               Items[1].tag := integer(oCampo) ;
                               items[1].Checked := TRUE;
                          end;
                     end;
            end;
       end
       else LimpiaListas;
  end;
{$endif}

begin
     Result := False;
     if ( LBTablas.Items.Count > 0 ) then
     begin
          oEntidadTemp := GetEntidad ;
          Result := TRUE;

          EntidadActual := oEntidadTemp;

          if (UltimaEntidad <> EntidadActual) OR
             (UltimaTipo <> GetTipoReporte ) then
          begin
               UltimaEntidad := EntidadActual;
               UltimaTipo := GetTipoReporte;

               //Campos
               CBCampos.Sorted := FALSE;

               dmDiccionario.CamposPorEntidad( EntidadActual, TRUE, CBCampos);


               //Requeridos
               {$ifdef RDD}
               dmDiccionario.ConectarDatosDefault( EntidadActual );
               //dmDiccionario.CamposDefault( EntidadActual );
               {$else}
               FRequeridos := dmDiccionario.GetRequeridos( EntidadActual );
               {$endif}

               //CBCampos.Sorted := TRUE;
               SetCamposDefault;
          end;
     end
     else
         ZInformation(Caption, 'La Clasificaci�n no Contiene Ninguna Tabla', 0 );

end;

procedure TAltaReportes_DevEx.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     with Wizard do
          if Adelante AND
             EsPaginaActual( Parametros ) then
             CanMove := CamposPorEntidad;
end;

procedure TAltaReportes_DevEx.bPlantillaClick(Sender: TObject);
begin
     inherited;
     RE_REPORTE.Text :=  DialogoPlantilla( RE_REPORTE.Text );
     DataSource.DataSet.FieldByName('RE_REPORTE').AsString := RE_REPORTE.Text;
end;

procedure TAltaReportes_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

procedure TAltaReportes_DevEx.FormShow(Sender: TObject);
begin
      tsTipoReporte.pageindex := 0;
      Parametros.pageindex := 1;
      ejecucion.pageindex := 2;
      tsCampos.pageindex := 3;

     inherited;
     DoConnect;
     UltimaEntidad := enNinguno;
     UltimaTipo := trFuturo;
     {$IFDEF COMPARTE_MGR}
     RGClasificacion.Enabled := TRUE;
     {$ELSE}
       {$ifdef ADUANAS}
       RGClasificacion.Enabled := TRUE;
       {$ELSE}
       RGClasificacion.Enabled := dmCliente.ModoTress;
       {$ENDIF}
     {$ENDIF}
end;

procedure TAltaReportes_DevEx.LlenaListBox;
 var iOffset: integer;
begin
     {$ifdef RDD}
     iOffset :=0;
     {$else}
     iOffset :=1;
     {$endif}

     with dmDiccionario, RGClasificacion do
     begin
          if ItemIndex < 0 then ItemIndex := 0;
          TablasPorClasificacion( Integer(Items.Objects[ItemIndex]) + iOffset,
                                  LBTablas.Items );
     end;

     LbTablas.ItemIndex := 0;
end;

procedure TAltaReportes_DevEx.FormCreate(Sender: TObject);
var
Strings : Tstrings;
i:integer;
begin
     inherited;
     Strings := TStringList.Create;
     with RE_TIPO do
     begin
          LlenaLista( lfTipoReporte, Strings);
          for i:=0 to Strings.count -1 do
          begin
           RE_TIPO.Properties.Items.Add();
           RE_TIPO.Properties.Items[i].caption := Strings[i];
          end;
          {Borrar el elemento de trPolizaConcepto}
          Properties.Items.Delete( Properties.Items.Count - 3 );
          {Borrar el elemente de trFuturo (es el ultimo)}
          Properties.Items.Delete( Properties.Items.Count -1 );


          {$ifdef RDDAPP}
          Properties.Items.Delete( Properties.Items.Count - 2 );
          {$else}
          {$IFDEF PRESUPUESTOS}
		  // (JB) En caso de que sea presupuestos no se muestra la P�liza
          Properties.Items.Delete( Properties.Items.Count - 2 );
          {$endif}
          if not dmCliente.ModoTress then
          begin
               {$ifndef TRESS} //{.ifdef SELECCION} {.ifdef VISITANTES}
               {Se borra el tipo de reporte POLIZA}
               Properties.Items.Delete( Properties.Items.Count - 2 );
               {$endif}

               {$IFDEF COMPARTE_MGR}
               GBClasificacion.Visible := TRUE;
               GBClasificacion.Height := 150;
               GBClasificacion.Top := 57;
               {$ELSE}
                 {$IFDEF ADUANAS}
                 GBClasificacion.Visible := TRUE;
                 GBClasificacion.Height := 150;
                 GBClasificacion.Top := 57;
                 {$else}
                 GBClasificacion.Visible := FALSE;
                 GBTablaPrincipal.Left := 115;
                 {$endif}
               {$ENDIF}
          end;
          {$endif}
          ItemIndex := 0;
     end;

     with cbTipoPoliza,Items do
     begin
          Add( 'P�liza por concepto' );
          Add( 'P�liza por grupo' );
          ItemIndex := 0;
     end;

     HelpContext := H50001_Creacion_reporte;

     dmDiccionario.GetListaClasifi(RGClasificacion.Items,FALSE);

     {$IFDEF RDD}
     if ( RGClasificacion.Items.Count = 1 ) then
     begin
          GBClasificacion.Visible := FALSE;
          GBTablaPrincipal.Left := 115;
     end;
     {$ENDIF}

     cbChecaTodos.Visible := FALSE;
     {$ifdef CAROLINA}
     cbChecaTodos.Visible := TRUE;
     {$endif}
end;

procedure TAltaReportes_DevEx.cbChecaTodosClick(Sender: TObject);
 var i: integer;
begin
     inherited;
     for i:= 0 to CBCampos.Items.Count - 1 do
         CBCampos.items[i].Checked := cbChecaTodos.Checked;
end;
procedure MoveItems(ItemA,ItemB: TcxCheckListBoxItems);
begin



end;

procedure TAltaReportes_DevEx.PrenderClick(Sender: TObject);
var
i :integer;
begin
  inherited;
  for i:=0 to CbCampos.Items.Count -1 do
      Cbcampos.items[i].checked := True;
end;

procedure TAltaReportes_DevEx.ApagarClick(Sender: TObject);
var
i :integer;
begin
  inherited;
  for i:=0 to CbCampos.Items.Count -1 do
      Cbcampos.items[i].checked := False;
end;

end.


