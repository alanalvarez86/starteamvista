{$WARNINGS OFF}
unit FListados_DevEx;

interface
{$INCLUDE DEFINES.INC}
{$ifndef DOS_CAPAS}
 {$define HTTP_CONNECTION}
{$endif}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaSmartLists, Db, StdCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaKeyLookup, ZetaFecha, CheckLst, ZetaEdit, ExtCtrls, ZetaKeyCombo,
  DBCtrls, Mask, ZetaDBTextBox, ComCtrls, Buttons,FileCtrl,
  ZetaCommonLists,
  ZetaCommonClasses,
  ZQRReporteListado,
  ZReporteAscii,
  ZReportTools,ZReportToolsConsts,
  ZetaNumero,
  FEditReportes_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
   TressMorado2013, dxSkinsDefaultPainters,
   dxSkinsdxBarPainter, dxBar, cxClasses, ImgList,
  ZetaKeyLookup_DevEx, cxButtons, cxStyles, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxListBox, ZetaSmartLists_DevEx,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, cxCheckListBox, dxBarBuiltInMenu,
  cxDBEdit, cxTreeView;

type
  TListados_DevEx = class(TEditReportes_DevEx)
    GBTotales: TGroupBox;
    lCriterioCampo: TLabel;
    CBCriterioCampo: TComboBox;
    RE_VERTICA: TDBCheckBox;
    RE_SOLOT: TDBCheckBox;
    GBEncabezado: TGroupBox;
    CBSaltoPag: TCheckBox;
    CBEncabezado: TCheckBox;
    LBGrupoEncabezado: TcxListBox;
    CBMaster: TCheckBox;
    GbPie: TGroupBox;
    CbTotalizar: TCheckBox;
    CbPie: TCheckBox;
    LBGrupoPie: TcxListBox;
    Label1: TLabel;
    RE_PFILE: TZetaDBKeyCombo;
    lbSeparador: TLabel;
    RE_CFECHA: TDBEdit;
    RE_PRINTER: TComboBox;
    lbImpresora: TLabel;
    lbCopias: TLabel;
    RE_COPIAS: TZetaDBNumero;
    UpDown: TUpDown;
    EPAGINAS: TLabel;
    EPaginaInicial: TEdit;
    EPAginasAl: TLabel;
    EPaginaFinal: TEdit;
    RE_ARCHIVO: TDBEdit;
    lbNombreArchivo: TLabel;
    lbPlantilla: TLabel;
    RE_REPORTE: TDBEdit;
    cbTitulos: TCheckBox;
    GroupBox5: TGroupBox;
    RE_FONTNAM: TDBEdit;
    lbPagina: TLabel;
    lbColumna: TLabel;
    lbRenglon: TLabel;
    RE_GENERAL: TDBCheckBox;
    RE_ALTO: TZetaDBNumero;
    RE_ANCHO: TZetaDBNumero;
    NombreArchivoFormula: TcxMemo;
    BGrafica: TdxBarButton;
    BAgregaPie: TcxButton;
    BAgregaEncabezado: TcxButton;
    bPlantilla: TcxButton;
    bEvaluaArchivo: TcxButton;
    BGuardaArchivo: TcxButton;
    BArribaCampos: TZetaSmartListsButton_DevEx;
    BAbajoCampos: TZetaSmartListsButton_DevEx;
    SpeedButton1: TcxButton;
    bExportPreferencias: TcxButton;
    {procedure LBCamposDrawItem(AControl: TcxListBox;
  ACanvas: TcxCanvas; AIndex: Integer; ARect: TRect;
  AState: TOwnerDrawState);   }
    procedure CBCriterioCampoClick(Sender: TObject);
    procedure bFormulaCampoClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure bPlantillaClick(Sender: TObject);
    procedure RE_PRINTERChange(Sender: TObject);
    procedure BAgregaEncabezadoClick(Sender: TObject);
    procedure BAgregaPieClick(Sender: TObject);
    procedure BBorraGrupoClick(Sender: TObject);
    procedure RE_PFILEChange(Sender: TObject);
    procedure SmartListCamposAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure BBorraCampoClick(Sender: TObject);
    procedure BFormulaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RE_SOLOTClick(Sender: TObject);
    procedure CBTipoCampoClick(Sender: TObject);
    procedure BGraficaClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure bExportPreferenciasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RE_ARCHIVOChange(Sender: TObject);
    procedure bDisenadorClick(Sender: TObject);
    procedure bEvaluaArchivoClick(Sender: TObject);
    procedure LbCamposDrawItem(AControl: TcxListBox; ACanvas: TcxCanvas;
      AIndex: Integer; ARect: TRect; AState: TOwnerDrawState);
  private
    FImpresoraDefault : integer;
    FDirDefault : string;
    FSobreTotales : Boolean;
    ReporteAscii : TReporteAscii;
    FTipoFormato: eTipoFormato;
    procedure GetListaOper( oLista: TStrings; const eTipo: eTipoGlobal );
    procedure AgregaSeparador;
    procedure EnabledArchivo( const lEnabled: Boolean; const eTipo : eTipoFormato );
    procedure EnabledImpresora(const lEnabled: Boolean);
    procedure EnabledPlantilla(const lEnabled: Boolean;const eTipo : eTipoFormato);
    procedure EnabledMailMerge(const lMailMerge: Boolean);
    procedure EnabledMailMergeXLS(const lMailMergeXLS: Boolean);
    procedure EnabledSeparador(const lSeparador: Boolean);
    procedure EnabledPrintSetting( const eTipo : eTipoFormato );
    function GetDatosImpresion: TDatosImpresion;
    function HayTotales: Boolean;
    function GetPosOperacion(const eTipo: eTipoGlobal;const eOperacion: eTipoOperacionCampo): Integer;
    function GetTipoOperacion(const eTipo: eTipoGlobal;const Posicion: integer): eTipoOperacionCampo;
    function GeneraArchivoAscii(const oTipoPantalla:TipoPantalla;const lDialogoConfirmacion: Boolean; oDatosImpresion: TDatosImpresion): Boolean;
    procedure GeneraMailMerge;
    procedure GeneraGrafica;
    procedure GeneraListado;
    procedure GeneraMailMergeXLS;
  protected
    procedure AgregaCampo;override;
    procedure AgregaListaCampo;override;
    procedure ActualizaCamposObjeto;override;
    procedure ActualizaCampos(const i : integer);override;
    procedure AsignaProcCampos(oProc: TNotifyEvent);override;
    procedure AlAgregarCampo( const oDiccion: TDiccionRecord;
                              var oCampo: TCampoOpciones);override;
    procedure EnabledCampos(const bEnable, bFormula, bAuto: Boolean);override;
    procedure AgregaFormula;override;

    //Lista de Grupos;
    procedure AgregaGrupo;override;
    procedure ActualizaGrupos(Objeto: TGrupoOpciones);override;
    procedure ActualizaGruposObjeto;override;
    procedure AsignaProcGrupos(oProc: TNotifyEvent);override;
    procedure EnabledGrupos(const bEnable, bFormula: Boolean);override;
    procedure GrupoNivelEmpresa(i: integer);override;

    procedure GrabaCampos;override;
    procedure AntesDeEscribirCambios;override;

    procedure LLenaListas;override;

    procedure AgregaSQLCampoRep;override;
    procedure AntesDeConstruyeSQL;override;
    procedure DespuesDeConstruyeSQL;override;
    function GeneraReporte: Boolean;override;
    function GetSoloTotales : Boolean;override;
  public
    { Public declarations }
  end;

var
  Listados_DevEx: TListados_DevEx;

implementation
uses Printers,
     DDiccionario,
     DCliente,
     DMailMerge,
     DExportEngine,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaWinApiTools,
{$ifdef HTTP_CONNECTION}
     ZetaRegistryCliente,
{$endif}
     ZQRReporteGrafica,
     ZFuncsCliente,
     ZetaDialogo,
     ZDiccionTools,
     FGrafica_DevEx,
     FPropGrupos_DevEx,     
     DExcelAppender,
     DReportes;

const K_OP_CUANTOS = 'Cuantos';
      K_OP_SUMA = 'Suma';
      K_OP_PROMEDIO = 'Promedio';
      K_OP_MINIMO = 'Valor M�nimo';
      K_OP_MAXIMO = 'Valor M�ximo';
      K_OP_STOTALES = 'Sobre Totales';
      K_OP_REPITE = 'Repite';
      K_OP_AUTO = 'Autom�tico';

{$R *.DFM}
{ TEditReportes1 }

procedure TListados_DevEx.AgregaListaCampo;
 var oCampo : TCampoListado;
     i : integer;
begin
     oCampo := TCampoListado.Create;
     LeeCampoGeneral( TCampoOpciones(oCampo) );

     i:= CampoRep.FieldByName('CR_OPER').AsInteger;
     if (i<Ord(Low(eTipoOperacionCampo))) OR
        (i>Ord(High(eTipoOperacionCampo))) then
        i := 0;

     with oCampo do
          Operacion := eTipoOperacionCampo( i );

     LbCampos.Items.AddObject(oCampo.Titulo, oCampo );
end;

procedure TListados_DevEx.GrabaCampos;
 var i: integer;
begin
     with LBCampos.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;
               GrabaCampoOpciones( TCampoOpciones(Objects[i]),
                                   tcCampos, i, -1 );
               CampoRep['CR_OPER'] := Ord( TCampoListado(Objects[i]).Operacion );
               CampoRep.Post;
          end;
end;

{procedure TListados_DevEx.LBCamposDrawItem(AControl: TcxListBox;
  ACanvas: TcxCanvas; AIndex: Integer; ARect: TRect;
  AState: TOwnerDrawState);
var
   sIndex : String;
begin
     inherited;   }
     //with (AControl as TcxListBox).Canvas do  { draw on control canvas, not on the form }
    // begin
          //FillRect(ARect);       { clear the rectangle }
          {sIndex := IntToStr( AIndex + 1  );
          TextOut(ARect.Left + 18 - TextWidth( sIndex ), ARect.Top, sIndex );
          TextOut(ARect.Left + 20, ARect.Top, ': ' }
             // + (AControl as TcxListBox).Items[AIndex])  { display the text }
    { end;
end;  }

procedure TListados_DevEx.EnabledCampos( const bEnable, bFormula, bAuto : Boolean );
begin
     inherited;
     lCriterioCampo.Enabled := bEnable;
     CBCriterioCampo.Enabled := bEnable;
     if bEnable then
     begin
          AsignaProcCampos( NIL );
          CBCriterioCampo.ItemIndex := 0;
          AsignaProcCampos( CampoOpcionesClick );
     end;
end;

procedure TListados_DevEx.AsignaProcCampos( oProc : TNotifyEvent );
begin
     inherited;
     CBCriterioCampo.Onclick := oProc;
end;

procedure TListados_DevEx.ActualizaCampos( const i : integer );
 var Objeto : TCampoListado;
begin
     AsignaProcCampos( NIL );
     Objeto := TCampoListado(LBCampos.Items.Objects[i]);

     EFormulaCampo.Text := Objeto.Formula;
     ETituloCampo.Text := Objeto.Titulo;
     GBPropiedadesCampo.Caption := K_PROPIEDAD_CAMPO + ' #' + IntToStr( GetListaCampos.ItemIndex  + 1 );
     GetListaMascaras( CBMascaraCampo.Items, Objeto.TipoCampo );
     CBMascaraCampo.Text := Objeto.Mascara;
     EAnchoCampo.Text := IntToStr( Objeto.Ancho );
     CBTipoCampo.ItemIndex := Ord( Objeto.TipoCampo );
     with GetListaCampos do
          Items[ ItemIndex ] := Objeto.Titulo;

     EnabledCampos( Objeto.Calculado <> K_RENGLON_NUEVO,
                    Objeto.Calculado=-1,
                    Objeto.TipoCampo = tgAutomatico );

     GetListaOper( CBCriterioCampo.Items, Objeto.TipoCampo );
     if (Ord( Objeto.Operacion )< 0) or
        (Ord( Objeto.Operacion ) > Ord(High(eTipoOperacionCampo))) then
        CBCriterioCampo.ItemIndex := 0
     else CBCriterioCampo.ItemIndex := GetPosOperacion( Objeto.TipoCampo, Objeto.Operacion );
     AsignaProcCampos( CampoOpcionesClick );
end;

function  TListados_DevEx.GetPosOperacion( const eTipo : eTipoGlobal;
                                     const eOperacion : eTipoOperacionCampo ): Integer;
begin
     Result := 0;
     case eTipo of
          tgFecha:
          begin
               case eOperacion of
                    ocAutomatico,ocSuma,ocSobreTotales : Result := 6;
                    ocPromedio : Result := 2;
                    ocMin : Result := 3;
                    ocMax : Result := 4;
                    ocRepite : Result := 5;
                    else Result := Ord( eOperacion );
               end;
          end;
          tgBooleano,tgTexto,tgMemo:
          begin
               case eOperacion of
                    ocNinguno,ocCuantos : Result := Ord( eOperacion );
                    ocRepite : Result := 2
                    else Result := 3;
               end;
          end
          else
          begin
              case eOperacion of
                   ocNinguno,ocCuantos,ocSuma,ocPromedio,ocMin,ocMax: Result := Ord( eOperacion );
                   ocSobreTotales: Result := 6;
                   ocRepite: Result := 7;
                   ocAutomatico: Result := 8;
              end;
          end;
     end;
end;

procedure TListados_DevEx.ActualizaCamposObjeto;
 var  i : integer;
      oCampo : TCampoListado;
      sCampo :string;
      sMascara : string;
begin
     AsignaProcCampos( NIL );
     with LBCampos.Items do
     begin
          if Count > 0 then
          begin
               i := LBCampos.ItemIndex;
               oCampo := TCampoListado( Objects[ i ] );
               if StrLleno( ETituloCampo.Text ) then
               begin
                    if (oCampo.Titulo <> ETituloCampo.Text) then
                    begin
                         oCampo.Titulo := ETituloCampo.Text;
                         LBCampos.Items[i] := oCampo.Titulo;
                    end;
               end
               else oCampo.Titulo := SINTITULO;

               sCampo := EFormulaCampo.Text;
               if sCampo <> oCampo.Formula then
                  oCampo.Formula := sCampo;

               if eTipoGlobal( CBTipoCampo.ItemIndex ) <> oCampo.TipoCampo then
               begin
                    oCampo.TipoCampo := eTipoGlobal( CBTipoCampo.ItemIndex );
                    GetListaMascaras( CBMascaraCampo.Items, oCampo.TipoCampo );

                    CBMascaraCampo.Text := GetMascaraDefault(oCampo.TipoCampo);
                    {case oCampo.TipoCampo of
                         tgBooleano,tgTexto,tgMemo,tgNumero,tgAutomatico : CBMascaraCampo.Text := '';
                         tgFecha: CBMascaraCampo.Text := K_MASCARA_DATE;
                         tgFloat: CBMascaraCampo.Text := K_MASCARA_FLOAT;
                    end;}
               end;

               sMascara := CBMascaraCampo.Text;
               oCampo.Mascara := sMascara;
               oCampo.Ancho := StrAsInteger( EAnchoCampo.Text );

               oCampo.Operacion := GetTipoOperacion(oCampo.TipoCampo, CBCriterioCampo.ItemIndex);
               Modo := dsEdit;
          end;
     end;
     AsignaProcCampos( CampoOpcionesClick );
end;


function TListados_DevEx.GetTipoOperacion( const eTipo : eTipoGlobal;
                                     const Posicion : integer ) : eTipoOperacionCampo;
begin
     with CBCriterioCampo do
     begin
          if Items[Posicion] = K_OP_CUANTOS then
             Result := ocCuantos
          else if Items[Posicion] = K_OP_SUMA then
             Result := ocSuma
          else if Items[Posicion] = K_OP_PROMEDIO then
             Result := ocPromedio
          else if Items[Posicion] = K_OP_MINIMO then
             Result := ocMin
          else if Items[Posicion] = K_OP_MAXIMO then
             Result := ocMax
          else if Items[Posicion] = K_OP_REPITE then
             Result := ocRepite
          else if Items[Posicion] = K_OP_STOTALES then
             Result := ocSobreTotales
          else if Items[Posicion] = K_OP_AUTO then
             Result := ocAutomatico
          else Result := ocNinguno;
     end;
end;

procedure TListados_DevEx.CBCriterioCampoClick(Sender: TObject);
begin
     inherited;
     {AsignaProcCampos( NIL );
     if CBTipoCampo.Enabled then
        GetListaOper( CBCriterioCampo.Items, eTipoGlobal( CBTipoCampo.ItemIndex ) );
     AsignaProcCampos( CampoOpcionesClick );
     }
end;

procedure TListados_DevEx.GetListaOper( oLista : TStrings; const eTipo : eTipoGlobal );
{kNinguno+'=' );
'Cuantos=COUNT' );
'Suma=SUM' );
'Promedio=AVERAGE' );
'Valor M�nimo=MINIMO' );
'Valor Maximo=MAXIMO' );
'Autom�tico=' );}

{K_OP_CUANTOS = 'Cuantos';
 K_OP_SUMA = 'Suma';
 K_OP_PROMEDIO = 'Promedio';
 K_OP_MINIMO = 'Valor M�nimo';
 K_OP_MAXIMO = 'Valor Maximo';
 K_OP_STOTALES = 'Sobre Totales';
 K_OP_REPITE = 'Repite';
 K_OP_AUTO = 'Autom�tico';}

begin
     with oLista do
     begin
          Clear;
          Add( K_NINGUNO );
          Add( K_OP_CUANTOS );
          if eTipo in  [ tgFloat, tgNumero, tgAutomatico ] then
          begin
               Add( K_OP_SUMA );
               Add( K_OP_PROMEDIO );
               Add( K_OP_MINIMO );
               Add( K_OP_MAXIMO );
               Add( K_OP_STOTALES );
          end
          else if eTipo = tgFecha then
          begin
               Add( K_OP_PROMEDIO );
               Add( K_OP_MINIMO );
               Add( K_OP_MAXIMO );
          end;
          Add( K_OP_REPITE );
          Add( K_OP_AUTO );
     end;
end;

{procedure TListados.LimpiaCampos;
begin
     inherited;
     AsignaProcCampos( NIL );
     CBCriterioCampo.ItemIndex := 0;
     AsignaProcCampos( CampoOpcionesClick );

end;}

procedure TListados_DevEx.bFormulaCampoClick(Sender: TObject);
begin
     with LBCampos do
          with TCampoListado( Items.Objects[ ItemIndex ] ) do
          begin
               if ConstruyeFormula( TcxCustomMemo(eFormulaCampo) ) then
               begin
                    Formula := eFormulaCampo.Text;
                    if Calculado >= 0 then
                    begin
                         Calculado := -1;
                         Mascara := '';
                         TipoCampo := tgAutomatico;
                         Operacion := ocAutomatico;
                    end;
                    SmartListCampos.SelectEscogido( ItemIndex );
               end;
          end;
end;


procedure TListados_DevEx.AlAgregarCampo( const oDiccion: TDiccionRecord;
                                    var oCampo: TCampoOpciones);
begin
     oCampo := TCampoListado.Create;
     DiccionToCampoMaster(oDiccion,oCampo);
     with TCampoListado(oCampo), oDiccion do
     begin
          Titulo := TCorto;
          Mascara := DI_MASCARA;
          Ancho := DI_ANCHO;
          {$ifdef RDD}
          Operacion := DI_OPER;
          {$else}
          if ( TipoCampo in [ tgFloat, tgNumero ] ) AND   
             ( DI_NUMERO = 0 ) then
             Operacion := ocSuma
          else if TipoCampo = tgBooleano then
               Operacion := ocCuantos
          else if TipoCampo = tgAutomatico then
               Operacion := ocAutomatico
          else Operacion := ocNinguno;
          {$endif}
     end;
end;

procedure TListados_DevEx.SpeedButton1Click(Sender: TObject);
begin
     inherited;
     AgregaSeparador;
end;

procedure TListados_DevEx.AgregaSeparador;
 var oSeparador : TCampoListado;
begin
     oSeparador := TCampoListado.Create;
     GetSeparador( TCampoopciones(oSeparador) );

     with LbCampos do
     begin
          Items.InsertObject( ItemIndex+1, oSeparador.Titulo, oSeparador );
          SmartListCampos.SelectEscogido( ItemIndex+1 );
     end;
     BBorraCampo.Enabled := TRUE;
     BAgregaCampo.SetFocus;
     Modo := dsEdit;
end;

procedure TListados_DevEx.BGuardaArchivoClick(Sender: TObject);
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
    begin
       ListOfStrings.Clear;
       ListOfStrings.Delimiter       := Delimiter;
       ListOfStrings.StrictDelimiter := True;
       ListOfStrings.DelimitedText   := Str;
    end;

 var sTexto, sArchivo : string;
     RutaArchivo  : TStringList;

begin
     inherited;
     //sTexto := DialogoAscii( RE_ARCHIVO.Text );

     if eTipoFormato(RE_PFILE.ItemIndex) in [tfASCIIFijo, tfASCIIDel,tfCSV, tfTXT] then
        sTexto := DialogoAscii( RE_ARCHIVO.Text )
     else
     begin
          sArchivo := RE_ARCHIVO.Text;
          try
             RutaArchivo := TStringList.Create;
             Split('\', RE_ARCHIVO.Text, RutaArchivo);
             if RutaArchivo.Count > 0 then
                sArchivo := RutaArchivo.Strings[RutaArchivo.Count-1];
          finally
                 RutaArchivo.Free;
          end;

          sTexto := DialogoListado( sArchivo, eTipoFormato(RE_PFILE.ItemIndex) );
     end;

     if sTexto <> sArchivo then
     begin
          Modo := dsEdit;
          Reporte.Edit;
          Reporte.FieldByName( 'RE_ARCHIVO').AsString := sTexto;
     end;
end;

procedure TListados_DevEx.bPlantillaClick(Sender: TObject);
 var sTexto : string;
begin
     inherited;
     case  eTipoFormato(RE_PFILE.ItemIndex) of
            tfMailMerge: sTexto:= DialogoWord( RE_REPORTE.Text );
            tfMailMergeXLS: sTexto:= DialogoExcel( RE_REPORTE.Text );
     else
          sTexto:= ZReportTools.DialogoPlantilla( RE_REPORTE.Text );
     end;

     if sTexto <> RE_REPORTE.Text then
     begin
          Modo := dsEdit;
          Reporte.Edit;
          Reporte.FieldByName( 'RE_REPORTE').AsString := sTexto;
     end;
end;

procedure TListados_DevEx.LLenaListas;
 var fPrinter : TPrinter;
     i: integer;
begin
     inherited LLenaListas;
     FImpresoraDefault := -1;
     RE_PRINTER.Items.Clear;

     fPrinter := TPrinter.Create;

     with fPrinter do
     begin
          if Printers.Count > 0 then
          begin
               FImpresoraDefault := PrinterIndex;
               RE_PRINTER.Items.Add( K_IMP_DEFAULT  + '('+ Printers[ PrinterIndex ] +')' );
               RE_PRINTER.Items.AddStrings( Printers );
          end;
     end;

     i := RE_PRINTER.Items.IndexOf(Reporte['RE_PRINTER']);
     if i<0 then RE_PRINTER.ItemIndex := 0
     else RE_PRINTER.ItemIndex := i;

     RE_PFILEChange( NIL );
end;

procedure TListados_DevEx.RE_PRINTERChange(Sender: TObject);
begin
     inherited;
     Modo := dsEdit;
end;

procedure TListados_DevEx.AgregaGrupo;

 var oGrupo : TGrupoOpciones;
     oDiccion: TDiccionRecord;
     oAdicional : TCampoOpciones;
     sCampo : string;
     oEntidadActiva : TipoEntidad;
begin
     if AntesDeAgregarDato( Entidad, FALSE, FALSE, oDiccion, SmartListGrupos ) then
     begin
          oGrupo := TGrupoOpciones.Create;
          DiccionToCampoMaster(oDiccion,oGrupo);
          oEntidadActiva := Entidad;

          with oGrupo do
          begin
               SaltoPagina := FALSE;
               Totalizar := TRUE;
               Encabezado := TRUE;
               PieGrupo := TRUE;

               //AQUI ESTAMOS AGREGANDO EL CODIGO DEL GRUPO
               oAdicional := TCampoOpciones.Create;
               DiccionToCampoMaster( oDiccion, oAdicional );
               with oAdicional do
               begin
                    Titulo := TCorto;
                    Mascara := oDiccion.DI_MASCARA;
                    Ancho := oDiccion.DI_ANCHO;
                    oGrupo.ListaEncabezado.AddObject( oAdicional.Titulo, oAdicional );
               end;
               //Aqui se esta agregando la descripci�n de
               sCampo := ZReportTools.GetCampoDescripcionGrupos(oEntidadActiva,Entidad,oDiccion);
               if sCampo > '' then
               begin
                    oAdicional := TCampoOpciones.Create;
                    with oAdicional do
                    begin
                         Entidad := enFunciones;
                         Formula := sCampo;
                         Titulo := ':';
                         TCorto := ':';
                         TipoCampo := tgTexto;
                         Calculado := -1;
                         Mascara := '';
                         Ancho := 25;
                         oGrupo.ListaEncabezado.AddObject( oAdicional.Titulo, oAdicional );
                    end;
               end;
          end;
          DespuesDeAgregarDato( oGrupo,
                                SmartListGrupos,
                                BBorraGrupo,
                                BAgregaEncabezado );
          Modo := dsEdit;
     end;
end;

procedure TListados_DevEx.ActualizaGrupos(Objeto: TGrupoOpciones);
  procedure AgregaListaAdicional( oLista : TStrings; lEncabezado : Boolean );
   var LBLista : TcxListBox;
  begin
       if lEncabezado then LBLista := LBGrupoEncabezado
       else LBLista := LBGrupoPie;
       LBLista.Items.Assign( oLista );
  end;

begin
     inherited;
     AsignaProcGrupos( NIL );

     GbEncabezado.Caption := 'Encabezado: ' + LBGrupos.Items[ LBGrupos.ItemIndex ];
     GbPie.Caption := 'Pie: ' + LBGrupos.Items[ LBGrupos.ItemIndex ];
     CBSaltoPag.Checked := Objeto.SaltoPagina;
     CBTotalizar.Checked := Objeto.Totalizar;
     CBEncabezado.Checked := Objeto.Encabezado;
     CBPie.Checked := Objeto.PieGrupo;
     CbMaster.Checked := Objeto.Master;
     {$ifdef ADUANAS}
     cbTitulos.Checked := Objeto.Titulos;
     {$endif} 
     AgregaListaAdicional( Objeto.ListaEncabezado, TRUE );
     AgregaListaAdicional( Objeto.ListaPie, FALSE );
     LBGrupoEncabezado.ItemIndex := 0;
     LBGrupoPie.ItemIndex := 0;
     BAgregaEncabezado.Enabled := NOT SoloImpresion;
     BAgregaPie.Enabled := NOT SoloImpresion;

     AsignaProcGrupos( GrupoOpcionesClick );
end;

procedure TListados_DevEx.ActualizaGruposObjeto;
 var i :integer;
begin
     inherited;
     i := LBGrupos.ItemIndex;
     with TGrupoOpciones( LBGrupos.Items.Objects[ i ] ) do
     begin
          SaltoPagina := CBSaltoPag.Checked;
          Totalizar := CBTotalizar.Checked;
          Encabezado := CbEncabezado.Checked;
          PieGrupo := CBPie.Checked;
          Master := CBMaster.Checked;
          {$ifdef ADUANAS}
          Titulos := cbTitulos.Checked;
          {$endif}

          ListaEncabezado.Assign( LBGrupoEncabezado.Items );
          ListaPie.Assign( LBGrupoPie.Items );
     end;
end;

procedure TListados_DevEx.AsignaProcGrupos(oProc: TNotifyEvent);
begin
     inherited;
     CBSaltoPag.Onclick := oProc;
     CBTotalizar.OnClick := oProc;
     CBEncabezado.Onclick :=  oProc;
     CbPie.OnClick := oProc;
     CbMaster.OnClick := oProc;
end;

procedure TListados_DevEx.EnabledGrupos(const bEnable, bFormula: Boolean);
 var lEnabled : Boolean;
begin
     inherited;
     lEnabled := NOT SoloImpresion AND bEnable;
     CBSaltoPag.Enabled := lEnabled;
     CBTotalizar.Enabled := lEnabled;
     CBEncabezado.Enabled := lEnabled;
     CBPie.Enabled := lEnabled;
     CBMaster.Enabled := lEnabled;
     LBGrupoEncabezado.Enabled := lEnabled;
     LBGrupoPie.Enabled := lEnabled;
     BAgregaEncabezado.Enabled := lEnabled;
     BAgregaPie.Enabled := lEnabled;
end;

procedure TListados_DevEx.GrupoNivelEmpresa(i: integer);
begin
     inherited;
     CbSaltoPag.Enabled := NOT SoloImpresion AND NOT ( i = 0 );
     GroupBoxGrupo.Enabled := NOT SoloImpresion AND NOT ( i = 0 );
     bFormulaGrupo.Enabled := NOT SoloImpresion AND NOT ( i = 0 );
     //eFormulaGrupo.Enabled := NOT ( i = 0 );
end;

procedure TListados_DevEx.BAgregaEncabezadoClick(Sender: TObject);
begin
     inherited;
     if PropGrupos( Entidad, GbEncabezado.Caption, LBGrupoEncabezado.Items, LbParametros.Items ) then
        ActualizaGruposObjeto;
end;

procedure TListados_DevEx.BAgregaPieClick(Sender: TObject);
begin
     inherited;
     if PropGrupos( Entidad, GbPie.Caption, LBGrupoPie.Items, LbParametros.Items ) then
        ActualizaGruposObjeto;
end;

procedure TListados_DevEx.BBorraGrupoClick(Sender: TObject);
 var oControl : TWinControl;
begin
     if GBEncabezado.Visible then oControl := BAgregaEncabezado
     else oControl := BBorraGrupo;
     BorraDato( SmartListGrupos, bAgregaGrupo, bBorraGrupo, oControl );
end;

procedure TListados_DevEx.AntesDeEscribirCambios;
begin
     inherited;
     if RE_PRINTER.ItemIndex = 0 then
        Reporte['RE_PRINTER'] := K_IMP_DEFAULT
     else Reporte['RE_PRINTER'] := RE_PRINTER.Text;
end;

procedure TListados_DevEx.AgregaCampo;
 var oCampo : TCampoListado;
     oDiccion : TDiccionRecord;
begin
     oCampo := NIL;
     if AntesDeAgregarDato( Entidad, TRUE, TRUE, oDiccion, SmartListCampos ) then
     begin
          AlAgregarCampo(oDiccion,TCampoOpciones(oCampo) );
          if oCampo <> NIL then
             DespuesDeAgregarDato( oCampo,
                                   SmartListCampos,
                                   BBorraCampo,
                                   ETituloCampo );
          Modo := dsEdit;
     end;
end;

procedure TListados_DevEx.EnabledPlantilla( const lEnabled : Boolean;const eTipo : eTipoFormato );
begin
     lbPlantilla.Enabled := lEnabled;
     RE_REPORTE.Enabled := lEnabled;
     bPlantilla.Enabled := lEnabled;
     bDisenador.Enabled := lEnabled and not(( eTipo in TiposMailMerge ) );
end;

procedure TListados_DevEx.EnabledImpresora( const lEnabled : Boolean );
begin
     lbImpresora.Enabled := lEnabled;
     RE_PRINTER.Enabled := lEnabled;
     lbCopias.Enabled := lEnabled;
     RE_COPIAS.Enabled := lEnabled;
     UPDown.Enabled := lEnabled;
     EPaginas.Enabled := lEnabled;
     EPaginasAl.Enabled := lEnabled;
     EPaginaInicial.Enabled := lEnabled;
     EPaginaFinal.Enabled := lEnabled;
end;

procedure TListados_DevEx.EnabledArchivo( const lEnabled : Boolean;
                                    const eTipo : eTipoFormato );
begin
     lbNombreArchivo.Enabled := lEnabled;
     RE_ARCHIVO.Enabled := lEnabled;
     BGuardaArchivo.Enabled := lEnabled;
     BEvaluaArchivo.Enabled := lEnabled;

     if lEnabled then
     begin
          if StrVacio(RE_ARCHIVO.Text) then
          begin
               Reporte.Edit;
               Reporte.FieldByName('RE_ARCHIVO').AsString := Reporte['RE_TITULO'] + '.' +
                                                             ObtieneElemento( lfExtFormato, Ord( eTipo ) );
          end;
     end;
end;

procedure TListados_DevEx.EnabledMailMerge( const lMailMerge: Boolean);
begin
     if lMailMerge then
     begin
          //sTexto := K_DOC_DEFAULT;
          {FConectando = TRUE cuando se esta abriendo la forma de edicion
          del reporte.}
          with Reporte do
          begin
               if NOT FConectando then
               begin
                    Edit;
                    FieldByName('RE_REPORTE').AsString := K_DOC_DEFAULT;
               end;
          lbPlantilla.Caption := 'Documento - MS Word:';
          end;
     end
     else
     begin
          if ( FTipoFormato <> tfMailMergeXLS ) then
             lbPlantilla.Caption:= 'Nombre de Plantilla:';
     end;
end;

procedure TListados_DevEx.EnabledMailMergeXLS( const lMailMergeXLS : Boolean);
const
     K_HOJA = 'Sheet1';
     K_UNO = 1;
begin
     if lMailMergeXLS then
     begin
          with Reporte do
          begin
               if NOT FConectando then
               begin
                    Edit;
                    FieldByName('RE_REPORTE').AsString := K_XLS_DEFAULT;
                    FieldByName('RE_FONTNAM').AsString:= K_HOJA;
                    FieldByName('RE_ALTO').AsInteger:= K_UNO;
                    FieldByName('RE_ANCHO').AsInteger:= K_UNO;
                    FieldByName('RE_GENERAL').AsString:= K_GLOBAL_SI;
               end;
               lbPlantilla.Caption:= 'Documento - MS Excel:';
          end;
     end;
     lbPagina.Enabled:= lMailMergeXLS;
     RE_FONTNAM.Enabled:= lMailMergeXLS;
     lbColumna.Enabled:= lMailMergeXLS;
     RE_ALTO.Enabled:= lMailMergeXLS;
     lbRenglon.Enabled:= lMailMergeXLS;
     RE_ANCHO.Enabled:= lMailMergeXLS;
     RE_GENERAL.Enabled:= lMailMergeXLS;
end;

 	

procedure TListados_DevEx.EnabledPrintSetting( const eTipo : eTipoFormato );
begin
     bExportPreferencias.Enabled := ( eTipo in [ tfHTML,tfTXT,tfXLS,tfRTF,tfWMF,
                                                 tfPDF,tfBMP,tfTIF,tfPNG,tfJPEG,
                                                 tfEMF,tfSVG,tfHTM,tfWB1,
                                                 tfWK2,tfDIF,tfSLK ] );

end;

procedure TListados_DevEx.EnabledSeparador( const lSeparador : Boolean );
begin
     lbSeparador.Enabled := lSeparador;
     RE_CFECHA.Enabled := lSeparador;
     if StrVacio( RE_CFECHA.Text ) then
     begin
          RE_CFECHA.Text := ',';
     end;
end;

procedure TListados_DevEx.RE_PFILEChange(Sender: TObject);
 {var fTipoFormato : eTipoFormato;}
begin
     inherited;

     FTipoFormato := eTipoFormato( RE_PFILE.ItemIndex );
     EnabledImpresora( FTipoFormato in [tfImpresora] );
     EnabledArchivo( not( FTipoFormato in [ tfImpresora, tfMailMergeXLS] ), FTipoFormato );
     EnabledPlantilla( not ( fTipoFormato in [tfASCIIFijo,
                                              tfASCIIDel,
                                              tfCSV] ),
                       FTipoFormato );
     EnabledMailMergeXLS( FTipoFormato = tfMailMergeXLS);
     EnabledMailMerge( fTipoFormato in [tfMailMerge] );
     EnabledSeparador( fTipoFormato in [tfASCIIDel, tfCSV,tfMailMerge] );
     EnabledPrintSetting( fTipoFormato );
end;


{*************************************************}
{*************** GENERACION DE SQL ***************}
{*************************************************}
procedure TListados_DevEx.AntesDeConstruyeSQL;
begin
     //CV: Se movio hacia AgregaSQLCampoRep
     //fDatosImpresion := GetDatosImpresion;
     if not( FDatosImpresion.Tipo in TiposMailMerge ) then {Cuando es Mail Merge, el preview se hace dentro del Word}
     begin
          ZQRReporteListado.PreparaReporte;
          with QRReporteListado do
               Init(SQLAgente,fDatosImpresion,LBParametros.Items.Count, FHayImagenes);
          ZQRReporteGrafica.PreparaGrafica;
          with QRReporteGrafica do //Se preparan las graficas, por si acaso se ocupan
               Init(SQLAgente,fDatosImpresion,LbParametros.Items.Count,FHayImagenes);
     end;
end;

procedure TListados_DevEx.DespuesDeConstruyeSQL;
 var i : integer;
begin
     ModificaListaCampos( LbCampos.Items );
     for i:= 0 to LBGrupos.Items.Count -1 do
     begin
          with TGrupoOpciones(LBGrupos.Items.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
     inherited;
end;

procedure TListados_DevEx.AgregaSQLCampoRep;
 var {iAncho,} i,j: integer;                            
begin
     FDatosImpresion := GetDatosImpresion;

     if LBCampos.Items.Count =0 then //raise Exception.Create('No hay Columnas por Agregar');
        Error( tsCampos, LBCampos, 'No se agreg� ninguna Columna' );

     if ( NOT HayTotales ) AND
        ( RE_SOLOT.Checked ) then
        Error( tsCampos, LBCampos, 'Ningun campo tiene criterio de totalizaci�n' );

     {Siempre se deben agregar los Campos PRIMERO}
     for i:=0 to LBCampos.Items.Count -1 do
     begin
          //DETALLE DE LISTADO
          AgregaSQLColumnas(TCampoOpciones(LBCampos.Items.Objects[i]));
     end;
     for i:=0 to LBGrupos.Items.Count -1 do
     begin
          if i > 0 then //El grupo Nivel de Empresa no se Manda al Servidor
             AgregaSQLGrupos(TGrupoOpciones(LBGrupos.Items.Objects[i]));
          with TGrupoOpciones(LBGrupos.Items.Objects[i]) do
          begin
               for j := 0 to ListaEncabezado.Count -1 do
                   AgregaSQLColumnas(TCampoOpciones(ListaEncabezado.Objects[j]),i);
               for j := 0 to ListaPie.Count -1 do
                   AgregaSQLColumnas(TCampoOpciones(ListaPie.Objects[j]),i);
          end;
     end;
     for i:=0 to LBOrden.Items.Count -1 do
         AgregaSQLOrdenes(TOrdenOpciones(LBOrden.Items.Objects[i]));
     for i:=0 to LBFiltros.Items.Count -1 do
         AgregaSQLFiltros(TFiltroOpciones(LBFiltros.Items.Objects[i]));
end;

function TListados_DevEx.GetDatosImpresion : TDatosImpresion;
var
   sError: string;
begin
    {***(@am): Por el cambio de utilizar objetos en vez de records para XE5, en este caso como TDatosImpresion no esta declarada
               en un datamodule el objeto no estara creado. Por lo tanto se debe crear antes de utilizarlo, el campo FDatosImpresion
               es parte de esta forma y sera el objeto que utilizaremos para inicializar funciones que devuelvan TDatosImpresion o
			   alguna otra clase que se use como tipo de dato y que anteriormente se declaraba como record. El campo utilizado (FDatosImpresion
               en este caso) debe ser creado en el evento Oncreate de la forma o datamodule  y destruirlo en el evento OnDestroy de la
               forma o datamodule.***}
    {$IFDEF TRESS_DELPHIXE5_UP}
        Result := FDatosImpresion;
    {$ENDIF}

     with Result do
     begin
          Tipo := eTipoFormato(RE_PFILE.ItemIndex);

          if (Tipo = tfImpresora) AND ( FImpresoraDefault = -1 ) then
             Error( tsImpresora, RE_PRINTER, 'La Computadora No tiene Impresoras Instaladas')
          else
          begin
               if RE_PRINTER.Items[ RE_PRINTER.ItemIndex ] = K_IMP_DEFAULT then
                  Impresora := FImpresoraDefault
               else with RE_PRINTER do
                    Impresora := ItemIndex - 1;
          end;

          Copias := RE_COPIAS.ValorEntero;
          PagInicial := StrToIntDef(EPaginaInicial.Text,1);
          PagFinal := StrToIntDef(EPaginaFinal.Text,9999);

          Archivo := RE_REPORTE.Text;

          Exportacion:= TransParamNomConfig( RE_ARCHIVO.Text, LbParametros.Count, SQLAgente.Parametros, TRUE, FALSE );
          ExcelOpciones.Row:= RE_ANCHO.ValorEntero;
          ExcelOpciones.Col:= RE_ALTO.ValorEntero;
          ExcelOpciones.Hoja:= RE_FONTNAM.Text;
          ExcelOpciones.Refrescar:= RE_GENERAL.Checked;

          if (StrLLeno(ExtractFilePath(Exportacion))) then
             ExportaUser := Exportacion
          else if(CambioArchivo) then
          begin
               ExportaUser := VACIO;
               CambioArchivo := False;
          end;

          Grafica := 'GRAFICA.QR2';
          FDirDefault := zReportTools.DirPlantilla;
          if Archivo = '' then
             Archivo := FDirDefault+'DEFAULT.QR2'
          else
          begin
               if ExtractFileExt( Archivo ) = '' then
                  Archivo := Archivo + '.QR2';
               if ExtractFilePath( Archivo ) = '' then
                  Archivo := FDirDefault + Archivo;

               if NOT (Tipo in [tfASCIIFijo,tfASCIIDel,tfCSV,tfMailMerge]) AND
                  NOT FileExists( Archivo ) then
                  {$ifdef HTTP_CONNECTION}
                  if ( ClientRegistry.TipoConexion = conxDCOM ) then
                  begin
                  {$endif}
                       if ZConfirm(Caption, 'La Plantilla "'+ Archivo +'" no Existe, �Desea que el Listado se Genere sin Plantilla?',0,mbNo) then
                          Archivo :=''
                       else Abort;
                  {$ifdef HTTP_CONNECTION}
                  end;
                  {$endif}
          end;

          if ( ( ( FTipoPantalla = tgImpresora ) and ( Tipo <> tfImpresora) ) or
             ( ( FTipoPantalla = tgPreview ) and ( Tipo = tfMailMerge ) ) )
             and ( Tipo <> tfMailMergeXLS ) then
          begin
               if StrVacio(ExportaUser) then
               begin
                    Exportacion := GetNombreExportacion( Result , ObtieneElemento( lfExtFormato, Ord( Tipo ) ), sError );
                    if StrLleno(sError) then
                       Error( tsImpresora, RE_ARCHIVO, sError )
                    else
                    begin
                         if GetMyDocumentsDir <> VACIO then
                             Exportacion := VerificaDir( GetMyDocumentsDir ) + ExtractFileName(Exportacion);
                         if not DialogoExporta( Exportacion, Tipo ) then
                            raise Exception.Create('Listado cancelado por el usuario');

                         ExportaUser := Exportacion;
                    end;
               end
               else
               begin
                    if DirectoryExists( ExtractFilePath( ExportaUser ) ) then
                    begin
                         if ( StrLleno ( ExtractFileName( ExportaUser ) )  ) then
                            Exportacion := ExportaUser
                         else
                             raise Exception.Create( 'No se especific� un nombre de archivo' );
                    end
                    else
                        raise Exception.Create( 'El Directorio No Existe' );
               end;
          end;
          Separador := Copy(RE_CFECHA.Text,1,1);
          {***(@am): Solo aplica para reportes tipo forma pero se le asigna un valor para que no quede en null la propiedad. ***}
          ImprimirAmbosLados := False;
     end;
end;

function TListados_DevEx.HayTotales : Boolean;
 var i : integer;
begin
     Result := FALSE;
     FSobreTotales := FALSE;
     for i := 0 to LBCampos.Items.Count - 1 do
     begin
          with TCampoListado( LBCampos.Items.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion <> ocNinguno);
               if Result then
                  FSobreTotales := FSobreTotales OR (Operacion = ocSobreTotales);
          end;
     end;
end;

function TListados_DevEx.GeneraReporte: Boolean;
 {$ifdef TRESS}
 var lVertical : Boolean;
 {$endif}
begin
     Result := TRUE;

     {$ifdef TRESS}
     lVertical := FALSE;
     {$endif}

     try
        {$ifdef TRESS}
        if Entidad in  [enMovimienLista, enListadoNomina] then
        begin
             ZFuncsCliente.EsSoloTotales := GetSoloTotales;
             RE_SOLOT.Checked := FALSE;
             lVertical := RE_VERTICA.Checked;
             RE_VERTICA.Checked := FALSE;
        end;
        {$endif}
        if FTipoPantalla in [tgPreview,tgImpresora] then
        begin
             case fDatosImpresion.Tipo of
                  tfASCIIFijo, tfASCIIDel, tfCSV : GeneraArchivoAscii(fTipoPantalla,TRUE,fDatosImpresion);
                  tfMailMerge : GeneraMailMerge;
                  tfMailMergeXLS : GeneraMailMergeXLS;
                  else
                      GeneraListado;
             end;
        end
        else GeneraGrafica;

     finally
            //ZQRReporteListado.DesPreparaReporte;
            {$ifdef TRESS}
            if Entidad in [enMovimienLista,enListadoNomina] then
            begin
                 RE_SOLOT.Checked := ZFuncsCliente.EsSoloTotales;
                 RE_VERTICA.Checked := lVertical;
            end;
            {$endif}

            //FreeAndNil( QRReporteListado );
            ZQrReporteListado.DesPreparaReporte;
            ZQRReporteGrafica.DesPreparaGrafica;
     end;
end;

procedure TListados_DevEx.GeneraListado;
begin
     FGrafica_DevEx.PreparaGrafica( RE_NOMBRE.Text,RE_TITULO.Text,
                              RE_SOLOT.Checked,
                              dsResultados.DataSet,
                              LBCampos.Items,LBGrupos.Items);
     QRReporteListado.GeneraListado( RE_NOMBRE.Text,
                                     FTipoPantalla = tgPreview,
                                     RE_VERTICA.Checked,RE_SOLOT.Checked,FSobreTotales,
                                     dsResultados.DataSet,
                                     LBCampos.Items,LBGrupos.Items );
     FGrafica_DevEx.DesPreparaGrafica;
end;

procedure TListados_DevEx.GeneraGrafica;
begin
     FGrafica_DevEx.PreparaGrafica(RE_NOMBRE.Text,
                            RE_TITULO.Text,
                            RE_SOLOT.Checked,
                            dsResultados.DataSet,
                            LBCampos.Items,
                            LBGrupos.Items);
     Grafica_DevEx.GeneraGrafica;
     FGrafica_DevEx.DesPreparaGrafica;
end;

function TListados_DevEx.GeneraArchivoAscii( const oTipoPantalla:TipoPantalla;
                                       const lDialogoConfirmacion : Boolean;
                                       oDatosImpresion : TDatosImpresion ): Boolean;
begin
     Result := TRUE;
     if oTipoPantalla = tgPreview then
     begin
          GeneraListado
     end
     else
     begin
          if ReporteAscii = NIL then
             ReporteAscii := TReporteAscii.Create;

          Result := ReporteAscii.GeneraAscii( dsResultados.DataSet,
                                              oDatosImpresion,
                                              LBCampos.Items,
                                              LBGrupos.Items,
                                              GetSoloTotales,
                                              lDialogoConfirmacion );
     end;
end;


procedure TListados_DevEx.GeneraMailMerge;
 var oDatosImpresion : TDatosImpresion;
begin
     oDatosImpresion := FDatosImpresion;
     {CV: 06-Oct-2003}
     {El tipo de Salida cuando es Mail Merge se cambia dentro de la clase
     TReporteAscii, en el m�todo  InitAsciiFile() }
     //oDatosImpresion.Tipo := tfASCIIDel;

     if GeneraArchivoAscii( tgImpresora, FALSE, oDatosImpresion ) then
     begin
          with TdmMailMerge.Create(self) do
          begin
               with oDatosImpresion do
                    if FileExists( Archivo ) then
                       Ejecuta(Archivo,Exportacion)
                    else
                        ZError(Caption,'El Documento ' + Archivo + ' No Ha Sido Encontrado.', 0);
               Free;
          end;
     end;
end;

 	

procedure TListados_DevEx.GeneraMailMergeXLS;
 var
    oExcelAppender: TExcelAppender;
begin
     oExcelAppender := TExcelAppender.Create( Self );
     try
        with FDatosImpresion do
        begin
             if oExcelAppender.AbreArchivoBase( Archivo ) then
             begin
                  try
                     if not oExcelAppender.VaciaDSWorkSheet(dmReportes.cdsResultados, ExcelOpciones.Hoja, ExcelOpciones.Row, ExcelOpciones.Col, LBCampos.Items ) then
                        ZError(Caption,oExcelAppender.GetErrorMessage,0);
                  except on Error: Exception do zError( Caption, 'No fue posible generar reporte en Excel' + Error.Message, 0);

                  end;
                  if ExcelOpciones.Refrescar then
                     oExcelAppender.RefrescarWorkBook;
                  oExcelAppender.ShowExcelApp;

             end
             else
                 ZError('Error',oExcelAppender.GetErrorMessage,0);
        end;
     finally
           FreeAndNil( oExcelAppender );
     end;
end;

procedure TListados_DevEx.SmartListCamposAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     if Objeto <> NIL then
     begin
          ActualizaCampos( LbCampos.ItemIndex );
          SmartListCampos.Control := ETituloCampo;
     end
     else EnabledCampos( FALSE, FALSE, FALSE );
end;

procedure TListados_DevEx.BBorraCampoClick(Sender: TObject);
begin
     BorraDato( SmartListCampos, bAgregaCampo, bBorraCampo, ETituloCampo );
     EnabledCampos( LBCampos.Items.Count > 0, FALSE, FALSE );
     SmartListCampos.SelectEscogido( LBCampos.ItemIndex );
end;

procedure TListados_DevEx.BFormulaClick(Sender: TObject);
begin
    with TBitBtn(Sender) do
    begin
         SetFocus;
         if Tag = 0 then AgregaCampo
         else AgregaFormula;
    end;
end;

procedure TListados_DevEx.AgregaFormula;
 var oFormula : TCampoOpciones;
begin
     oFormula := TCampoListado.Create;
     with oFormula do
     begin
         Entidad := enFormula;
         Formula := '';
         Titulo := K_TITULO;
         Ancho := 10;
         TipoCampo := tgAutomatico;
         Calculado := -1;
         Operacion := ocAutomatico;
     end;
     with LBCampos do
     begin
          Items.AddObject( oFormula.Titulo, oFormula );
          SmartListCampos.SelectEscogido( Items.Count - 1 );
          Modo := dsEdit;
     end;
     ETituloCampo.SelectAll;
     ETituloCampo.SetFocus;
end;



procedure TListados_DevEx.FormDestroy(Sender: TObject);
begin
     with LBCampos do
          while Items.Count > 0 do
          begin
                TCampoOpciones(Items.Objects[0]).Free;
                Items.Delete(0);
          end;
     FreeAndNil(ReporteAscii);
     inherited;
end;



procedure TListados_DevEx.RE_SOLOTClick(Sender: TObject);
begin
     inherited;
     if LBGrupos.Items.Count = 1 then
        RE_VERTICA.Checked := RE_SOLOT.Checked;
end;

procedure TListados_DevEx.CBTipoCampoClick(Sender: TObject);
 var oCampo : TCampoOpciones;

begin
     //CampoOpcionesClick(NIL);
     CampoOpcionesClick(Sender);
     with LBCampos do
          oCampo := TCampoOpciones(Items.Objects[ItemIndex]);

     GetListaOper( CBCriterioCampo.Items, oCampo.TipoCampo );
     if oCampo.TipoCampo = tgAutomatico then
        CBCriterioCampo.ItemIndex := GetPosOperacion( oCampo.TipoCampo, ocAutomatico )
     else
     begin
          {if oCampo.TipoCampo = tgFloat then
             CBCriterioCampo.ItemIndex := GetPosOperacion( oCampo.TipoCampo, ocSuma )
          else }
          CBCriterioCampo.ItemIndex := GetPosOperacion( oCampo.TipoCampo, oCampo.Operacion );
     end;
     CampoOpcionesClick(Sender);
end;

procedure TListados_DevEx.BGraficaClick(Sender: TObject);
begin
     inherited;
     ImpresionReporte(tgGrafica);
end;

procedure TListados_DevEx.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
     if ( Shift = [ssCtrl] ) AND ( Chr(Key) = 'G' ) then
     begin
          Key := 0;
          ImpresionReporte( tgGrafica );
     end

end;

function TListados_DevEx.GetSoloTotales: Boolean;
begin
     Result := RE_SOLOT.Checked;
end;

procedure TListados_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     EPaginaInicial.Text := '1';
     EPaginaFinal.Text := '9999';
     NombreArchivoFormula.Text:=VACIO;
end;

procedure TListados_DevEx.bExportPreferenciasClick(Sender: TObject);
begin
     inherited;
     DExportEngine.Preferencias( eTipoFormato( RE_PFILE.ItemIndex ) );
end;

procedure TListados_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     cbTitulos.Visible := {$ifdef ADUANAS}TRUE{$ELSE}FALSE{$endif};
end;

procedure TListados_DevEx.RE_ARCHIVOChange(Sender: TObject);
begin
     inherited;
     CambioArchivo := True;
end;

procedure TListados_DevEx.bDisenadorClick(Sender: TObject);
begin
     inherited;
     AbreDisenador( RE_REPORTE.Text );
end;

procedure TListados_DevEx.bEvaluaArchivoClick(Sender: TObject);
var
   sNomArchivoFormula: String;
begin
     sNomArchivoFormula:= GeneraNombreArchivo(RE_ARCHIVO.Text);

     if ( StrLleno( ExtractFilePath( sNomArchivoFormula ) )  and
         not ( DirectoryExists( ExtractFilePath( sNomArchivoFormula ) ) ) ) then
           ZInformation( '� Advertencia !', 'La ruta especificada no existe en �sta computadora', 0 )
     else if StrVacio( ExtractFileName( sNomArchivoFormula ) ) then
           ZInformation( '� Advertencia !', 'No se ha especificado un nombre de archivo', 0 );

     NombreArchivoFormula.Text:= sNomArchivoFormula;
end;

{***DevEx(@am): Se modifica este metodos para corregir el bug 5359 del proyecto V 2014 - Nueva imagen Fase #2.
                No se pintaban correctamente los indices para los campos del ListBox de la pesta�a Lista Campos***}
procedure TListados_DevEx.LbCamposDrawItem(AControl: TcxListBox;
  ACanvas: TcxCanvas; AIndex: Integer; ARect: TRect;
  AState: TOwnerDrawState);
var
   sIndex : String;
   ATextRect: TRect;
   ABkGColor, ATextColor: TColor;
begin
  inherited;

  if odSelected in AState then
  begin
       ABkGColor := $00F19F3A; //Azul rey 
       ATextColor := clWhite;
  end
  else
  begin
       ABkGColor := clWhite;
       ATextColor := clWindowText;
  end;

  with ACanvas do
  begin
       Brush.Color := ABkGColor;
       Font.Color := ATextColor;
       FillRect(ARect);       { clear the rectangle }
  end;

  sIndex := IntToStr( AIndex + 1  ) + ': ' +AControl.Items[AIndex];
  ATextRect := ARect;
  ATextRect.Left := ATextRect.Left + 18;
  ACanvas.DrawTexT(sIndex, ATextRect,0);  { display the text }

end;

end.






