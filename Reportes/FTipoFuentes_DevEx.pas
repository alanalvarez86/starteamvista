unit FTipoFuentes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, checklst, ZetaCommonLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
    TressMorado2013,
  dxSkinsDefaultPainters,  cxButtons;

type
  TTipoFuentes_DevEx = class(TForm)
    Panel: TPanel;
    RGCaracteres: TRadioGroup;
    RGLineas: TRadioGroup;
    GBGenerales: TGroupBox;
    CBNegrita: TCheckBox;
    CBSubrayado: TCheckBox;
    CBItalica: TCheckBox;
    CBLandscape: TCheckBox;
    CBSaltoPag: TCheckBox;
    CBReset: TCheckBox;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
  private
    function GetFuente : TFuente;
    procedure SetFuente( oFuente : TFuente );
  public
    property Fuente : TFuente read GetFuente write SetFuente;
  end;

  function DialogoTipoFuentes( var oFuente : TFuente ) : Boolean;

var
  TipoFuentes_DevEx: TTipoFuentes_DevEx;

implementation

uses ZetaCommonClasses;

{$R *.DFM}

function DialogoTipoFuentes( var oFuente : TFuente ) : Boolean;
begin
     if TipoFuentes_DevEx = NIL then
        TipoFuentes_DevEx := TTipoFuentes_DevEx.Create( Application.MainForm );

     with TipoFuentes_DevEx do
     begin
          Fuente := oFuente;
          ShowModal;
          Result := ModalResult = mrOk;
          if Result then
             oFuente := Fuente;
     end;
end;

function TTipoFuentes_DevEx.GetFuente : TFuente;
begin
     Result := [];
     if CBNegrita.Checked then Result := Result + [eNegrita];
     if CBItalica.Checked then Result := Result + [eItalica];
     if CbSubrayado.Checked then Result := Result + [eSubrayado];
     if CBLandscape.Checked then Result := Result + [eLandscape];
     if CBSaltoPag.Checked then Result := Result + [eEject];
     if CBReset.Checked then Result := Result + [eReset];
     case RgCaracteres.ItemIndex of
          0: Result := Result + [eAncho10];
          1: Result := Result + [eAncho12];
          2: Result := Result + [eComprimido];
     end;
     case RgLineas.ItemIndex of
          0: Result := Result + [eLineas6];
          1: Result := Result + [eLineas8];
     end;
end;

procedure TTipoFuentes_DevEx.SetFuente( oFuente : TFuente );
begin
     CBNegrita.Checked := eNegrita in oFuente;
     CBItalica.Checked := eItalica in oFuente;
     CbSubrayado.Checked := eSubrayado in oFuente;
     CBLandscape.Checked := eLandscape in oFuente;
     CBSaltoPag.Checked := eEject in oFuente;
     CBReset.Checked := eReset in oFuente;

     if eAncho12 in oFuente then RgCaracteres.ItemIndex := 1
     else if eComprimido in oFuente then RgCaracteres.ItemIndex := 2
     else RgCaracteres.ItemIndex := 0;

     if eLineas8 in oFuente then RgLineas.ItemIndex := 1
     else RgLineas.ItemIndex := 0
end;

end.
