unit FParametros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$ifndef VER130}
  Variants,
  {$endif}
  StdCtrls, ComCtrls, Buttons, ExtCtrls, Mask;

type
  TParametros = class(TForm)
    Panel1: TPanel;
    OKBtn: TBitBtn;
    CancelarBtn: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FCountParam: integer;
    FParams: Variant;
    procedure GetResultados;
    procedure CreaControles;
    procedure DestruyeControles;

  protected
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    property Params : Variant read FParams write FParams;
    property CountParam : integer read FCountParam write FCountParam;
  end;

var
  Parametros: TParametros;

function MuestraParametros( var oParams : OleVariant;
                            const iCountParam : integer ) : Boolean;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZReportConst,
     ZetaFecha;

const K_INICIO_LABEL = 10;
      K_ANCHO_LABEL = 200;
      K_INICIO_TOP = 10;
      K_INICIO_CONTROL = 225;

var aControles : array[ 1 ..K_MAX_PARAM ] of TWinControl;
    aLabels : array[ 1 ..K_MAX_PARAM ] of TLabel;

{$R *.DFM}

function MuestraParametros( var oParams : OleVariant;
                            const iCountParam : integer ) : Boolean;
begin
     if Parametros = NIL then Parametros := TParametros.Create( Application );

     with Parametros do
     begin
          Params := oParams;
          CountParam := iCountParam;
          ShowModal;
          Result := ModalResult = mrOk;
          if Result then
             oParams := Params;
     end;
end;

procedure TParametros.GetResultados;
 var i : integer;
     
     oParam : oleVariant;
begin
     for i := 1 to FCountParam do
     begin

          oParam := VarArrayCreate([1,3],VarVariant);
          case eTipoGlobal(Params[i][3]) of
               tgBooleano: oParam[2] := TCheckBox( aControles[i] ).Checked;
               tgFloat,tgNumero : oParam[2] := StrToReal( TEdit( aControles[i] ).Text );
               tgTexto : oParam[2] := TEdit( aControles[i] ).Text;
               tgFecha : oParam[2] := TZetaFecha( aControles[i] ).Valor;
               tgAutomatico: oParam[2] := TEdit( aControles[i] ).Text;
          end;
          oParam[1] := Params[i][1];
          oParam[3] := Params[i][3];
          FParams[i] := oParam;
     end;
end;

procedure TParametros.DestruyeControles;
 var i : integer;
begin
     for i := 1 to FCountParam do
     begin
          aControles[ i ].Free;
          aLabels[ i ].Free;
     end;
end;

procedure TParametros.CreaControles;
var oControl : TWinControl;
    oLabel : TLabel;
    nTop : Integer;
    nTipo : eTipoGlobal;
    i : Integer;
begin
     try
        nTop := K_INICIO_TOP;
        for i:= 1 to FCountParam do
        begin
             oLabel := TLabel.Create( Self );
             with oLabel do
             begin
                  Parent := Self;
                  Autosize := FALSE;
                  Caption := Params[i][1] + ':';
                  Top := nTop;
                  Left := K_INICIO_LABEL;
                  Width := K_ANCHO_LABEL;
                  Alignment := taRightJustify;
             end;

             oControl := NIL;
             nTipo := Params[i][3];
             case nTipo of
                  tgBooleano :
                  begin

                       oControl := TCheckBox.Create( Self );
                       with oControl as TCheckBox do
                       begin
                            Parent := Self;
                            Top := nTop-2;
                            Left := K_INICIO_CONTROL;
                            Caption := ' ';
                            Checked := zStrToBool( Params[i][2] );
                       end;
                  end;
                  tgFloat, tgNumero, tgTexto, tgAutomatico :
                  begin
                       oControl := TEdit.Create( Self );
                       with oControl as TEdit do
                       begin
                            Parent := Self;
                            Top  := nTop-4;
                            Left := K_INICIO_CONTROL;
                            Text := Params[i][2];
                            if nTipo = tgTexto then Width := 200
                            else Width := 100;
                       end;
                  end;
                  tgFecha :
                  begin
                       oControl := TZetaFecha.Create( Self );
                       with oControl as TZetaFecha do
                       begin
                            Parent := Self;
                            Top := nTop-4;
                            Left := K_INICIO_CONTROL;
                            Height := 21;
                            if Params[i][2]>'' then
                               Valor := StrAsFecha(Params[i][2]);
                       end;
                  end;
             end;

             aControles[ i ] := oControl;
             aLabels[ i ] := oLabel;
             if i = 1 then ActiveControl := oControl;

             nTop := nTop + 20;
        end; //For

     Height := nTop + 80;

     except
     end;
end;

procedure TParametros.FormShow(Sender: TObject);
begin
     CreaControles;
end;

procedure TParametros.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if ModalResult=mrOk then
        GetResultados;
     DestruyeControles;
end;

{procedure TParametros.EnterTab(Sender: TObject; var Key: Char);
begin
     //EnterAsTab( Sender, Self, Key );
end;}

procedure TParametros.FormCreate(Sender: TObject);
begin
     HelpContext:= H55180_Parametros;
end;


procedure TParametros.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

end.
