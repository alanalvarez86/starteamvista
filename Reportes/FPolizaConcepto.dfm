inherited PolizaConcepto: TPolizaConcepto
  Left = 409
  Top = 257
  Width = 640
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'PolizaConcepto'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Width = 632
    inherited OK: TBitBtn
      Left = 471
    end
    inherited Cancelar: TBitBtn
      Left = 549
    end
  end
  inherited Panel1: TPanel
    Width = 632
  end
  inherited PageControl: TPageControl
    Width = 632
    inherited tsCuentas: TTabSheet
      inherited Grid: TZetaDBGrid [0]
        Width = 624
        Columns = <
          item
            Expanded = False
            FieldName = 'CR_CALC'
            PickList.Strings = ()
            Title.Alignment = taCenter
            Title.Caption = 'Concepto'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CO_DESCRIP'
            PickList.Strings = ()
            ReadOnly = True
            Title.Caption = 'Descripci�n'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_FORMULA'
            PickList.Strings = ()
            Title.Alignment = taCenter
            Title.Caption = 'F�rmula de la cuenta'
            Width = 296
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_OPER'
            PickList.Strings = ()
            Title.Alignment = taCenter
            Title.Caption = 'Tipo Cuenta'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_SHOW'
            PickList.Strings = ()
            Title.Alignment = taRightJustify
            Title.Caption = 'Prorrateo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_MASCARA'
            PickList.Strings = ()
            Title.Alignment = taCenter
            Title.Caption = 'Comentario'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_DESCRIP'
            PickList.Strings = ()
            Title.Alignment = taCenter
            Title.Caption = 'Texto'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_ANCHO'
            PickList.Strings = ()
            Title.Alignment = taCenter
            Title.Caption = 'N�mero'
            Width = 42
            Visible = True
          end>
      end
      inherited Panel2: TPanel [1]
        Width = 624
        inherited Label6: TLabel
          Left = 618
        end
        object Label8: TLabel
          Left = 385
          Top = 9
          Width = 143
          Height = 13
          Caption = 'Longitud m�nima de la cuenta:'
        end
        object BuscarBtn: TSpeedButton
          Left = 82
          Top = 4
          Width = 24
          Height = 24
          Hint = 'Buscar (Ctrl+B)'
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F000000000000000000000001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDD707DDDDDDDDDDDDDD0000DD73
            307DDDDDDDDDDDDD0000DD783307DDDDDDDDDDDD0000DDD383307DDDDDDDDDDD
            0000DDDD38330777777DDDDD0000DDDDD38330000077DDDD0000DDDDDD383378
            88707DDD0000DDDDDDD037F8F88707DD0000DDDDDDD07FCCCCC8777D0000DDDD
            DCD0F8C8F8C880CD0000DDDDDCC08FCCCCCF80DD0000DDDDDCD0F8CFFCF880DD
            0000DDDDDDC77FCCCC8F77CD0000DDDDDDDD07F8F8F70DDD0000DDDDDDDDD07F
            8F70DDDD0000DDDDDDDDDD70007DDDDD0000DDDDDDDDDDDDDDDDDDDD0000DDDD
            DDDDDDDDDDDDDDDD0000}
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = BuscarBtnClick
        end
        object btnLlenaConceptos: TSpeedButton
          Left = 117
          Top = 4
          Width = 24
          Height = 24
          Hint = 'Importar Cat�logo de Conceptos de Tress'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
            FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
            00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
            F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
            00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
            F033777777777337F73309999990FFF0033377777777FFF77333099999000000
            3333777777777777333333399033333333333337773333333333333903333333
            3333333773333333333333303333333333333337333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnLlenaConceptosClick
        end
        object RE_HOJA: TZetaDBNumero
          Left = 534
          Top = 5
          Width = 65
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'RE_HOJA'
          DataSource = DataSource
        end
      end
      inherited CBTipoCuenta: TComboBox
        Left = 400
        Top = 48
      end
      object bFormula: TBitBtn
        Left = 349
        Top = 79
        Width = 20
        Height = 20
        Hint = 'Editor de F�rmulas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Visible = False
        OnClick = bFormulaClick
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
          DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
          0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
          D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
          D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
          0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
          DDDDD0000000}
      end
    end
  end
end
