inherited EditPoliza: TEditPoliza
  Left = 259
  Top = 104
  Caption = 'EditPoliza'
  ClientHeight = 362
  ClientWidth = 328
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel [0]
    Left = 40
    Top = 244
    Width = 56
    Height = 13
    Caption = 'Comentario:'
  end
  object Label6: TLabel [1]
    Left = 69
    Top = 268
    Width = 27
    Height = 13
    Caption = 'Texto'
  end
  object Label7: TLabel [2]
    Left = 59
    Top = 292
    Width = 37
    Height = 13
    Caption = 'N�mero'
  end
  inherited PanelBotones: TPanel
    Top = 326
    Width = 328
    inherited OK: TBitBtn
      Left = 160
    end
    inherited Cancelar: TBitBtn
      Left = 245
    end
  end
  object GroupBox1: TGroupBox
    Left = 40
    Top = 16
    Width = 265
    Height = 70
    Caption = 'En las Cuentas'
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 77
      Height = 13
      Caption = 'Buscar el Texto:'
    end
    object Label2: TLabel
      Left = 26
      Top = 44
      Width = 67
      Height = 13
      Caption = 'Sustituirlo Por:'
    end
    object Edit1: TEdit
      Left = 96
      Top = 16
      Width = 150
      Height = 21
      TabOrder = 0
    end
    object Edit2: TEdit
      Left = 96
      Top = 40
      Width = 150
      Height = 21
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 40
    Top = 96
    Width = 265
    Height = 70
    Caption = 'En las F�rmulas'
    TabOrder = 2
    object Label3: TLabel
      Left = 16
      Top = 20
      Width = 77
      Height = 13
      Caption = 'Buscar el Texto:'
    end
    object Label4: TLabel
      Left = 26
      Top = 44
      Width = 67
      Height = 13
      Caption = 'Sustituirlo Por:'
    end
    object Edit3: TEdit
      Left = 96
      Top = 16
      Width = 150
      Height = 21
      TabOrder = 0
    end
    object Edit4: TEdit
      Left = 96
      Top = 40
      Width = 150
      Height = 21
      TabOrder = 1
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 40
    Top = 168
    Width = 185
    Height = 65
    Caption = 'Tipo de Cuenta'
    Items.Strings = (
      'Sin Cambio'
      'Cargo'
      'Abono')
    TabOrder = 3
  end
  object Edit5: TEdit
    Left = 104
    Top = 264
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object Edit6: TEdit
    Left = 104
    Top = 288
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object Edit7: TEdit
    Left = 104
    Top = 240
    Width = 121
    Height = 21
    TabOrder = 6
  end
end
