unit DReportes;

interface
uses Windows, SysUtils, Classes, db;

 {Se declara un Reporteador DUMMY para que el Dise�ador no tenga que hacer un USES
 de DBaseReportes y de DReportes.
 Si la implementacion de DBaseReportes Cambia, tambien debe cambiar la implementacion
 de TDummyReportes.EvaluaParametros}
 type 
 TdmReportes = Class(TObject)
  private
 public
    cdsResultados : TDataSet;
    function DirectorioPlantillas: string;
    function EvaluaParametros( oSQLAgente : TObject; Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;
 end;

 var dmReportes : TdmReportes;

implementation

function TdmReportes.EvaluaParametros( oSQLAgente : TObject; Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;
begin
     Result := TRUE;
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     Result := '';
end;

end.
