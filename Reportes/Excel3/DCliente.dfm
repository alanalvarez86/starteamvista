inherited dmCliente: TdmCliente
  OldCreateOrder = True
  Left = 283
  object cdsReportes: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    StoreDefs = True
    AfterOpen = cdsReportesAfterOpen
    AlAdquirirDatos = cdsReportesAlAdquirirDatos
    Left = 144
    Top = 56
  end
  object cdsQuery: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AfterOpen = cdsQueryAfterOpen
    Left = 144
    Top = 120
  end
  object cdsPeriodo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 136
  end
  object cdsLookupReportes: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    AfterOpen = cdsLookupReportesAfterOpen
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    LookupKeyField = 'RE_CODIGO'
    Left = 232
    Top = 64
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 200
  end
  object cdsPatron: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 256
  end
end
