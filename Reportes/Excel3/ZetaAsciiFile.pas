unit ZetaAsciiFile;

interface

uses SysUtils, Controls, Windows, Classes, DB, DBClient, Dialogs,
     ZetaCommonClasses;

type
  TAsciiLog = class( TObject )
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure Init( const sFileName: String );
    procedure WriteEvent(const iEmpleado: TNumEmp; const sMensaje: String);
    procedure WriteException(const iEmpleado: TNumEmp; const sMensaje: String; Error: Exception);
    procedure WriteTexto(const sTexto: String); dynamic;
    procedure WriteTimeStamp( const sText: String );
  end;

implementation

{ *********** TAsciiLog ********* }

procedure TAsciiLog.Init( const sFileName: String );
begin
end;

procedure TAsciiLog.WriteTexto( const sTexto: String );
begin
end;

procedure TAsciiLog.WriteEvent( const iEmpleado: TNumEmp; const sMensaje: String );
begin
end;

procedure TAsciiLog.WriteException( const iEmpleado: TNumEmp; const sMensaje: String; Error: Exception );
begin
end;

procedure TAsciiLog.WriteTimeStamp( const sText: String );
begin
end;

end.
