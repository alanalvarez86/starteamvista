unit TressExcel_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 6/22/2018 10:01:06 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2018\Reportes\Excel3\TressExcel.tlb (1)
// LIBID: {CE689FFF-FA39-4DD3-8D77-AB1B7AD65050}
// LCID: 0
// Helpfile: 
// HelpString: TressExcel Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\System32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TressExcelMajorVersion = 1;
  TressExcelMinorVersion = 0;

  LIBID_TressExcel: TGUID = '{CE689FFF-FA39-4DD3-8D77-AB1B7AD65050}';

  IID_ITress2Excel: TGUID = '{830BC8E5-EA79-44DC-BA30-33D554F0D03C}';
  CLASS_Tress2Excel: TGUID = '{422CEA51-2B62-4AEF-B183-8B89E0D871C3}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ITress2Excel = interface;
  ITress2ExcelDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Tress2Excel = ITress2Excel;


// *********************************************************************//
// Interface: ITress2Excel
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {830BC8E5-EA79-44DC-BA30-33D554F0D03C}
// *********************************************************************//
  ITress2Excel = interface(IDispatch)
    ['{830BC8E5-EA79-44DC-BA30-33D554F0D03C}']
    function LoginCheck(const Usuario: WideString; const Clave: WideString): SYSINT; safecall;
    function EscogeEmpresa(const Usuario: WideString; const Clave: WideString; 
                           out Codigo: WideString; out Nombre: WideString): WordBool; safecall;
    function ProcesaQuery(const Usuario: WideString; const Password: WideString; 
                          const Empresa: WideString; const Query: WideString): SYSINT; safecall;
    procedure First; safecall;
    function EoF: WordBool; safecall;
    procedure Next; safecall;
    function EoRow: WordBool; safecall;
    procedure NextCol; safecall;
    function Valor: OleVariant; safecall;
    function FieldName: WideString; safecall;
    function Status: WideString; safecall;
    procedure ParamAsString(const Nombre: WideString; const Valor: WideString); safecall;
    procedure ParamAsInteger(const Nombre: WideString; Valor: Integer); safecall;
    procedure ParamAsFloat(const Nombre: WideString; Valor: Single); safecall;
    procedure ParamAsDate(const Nombre: WideString; Valor: TDateTime); safecall;
    procedure SetDateFormat(const Valor: WideString); safecall;
    function GetValorActivo(const Usuario: WideString; const Password: WideString; 
                            const Empresa: WideString; const Nombre: WideString; Default: OleVariant): OleVariant; safecall;
    procedure SetValorActivo(const Usuario: WideString; const Password: WideString; 
                             const Empresa: WideString; const Nombre: WideString; Valor: OleVariant); safecall;
    function GeneraReporte(const Usuario: WideString; const Password: WideString; 
                           const Empresa: WideString; Reporte: Integer; out Nombre: WideString): SYSINT; safecall;
    function EscogerUnReporte(const Usuario: WideString; const Password: WideString; 
                              const Empresa: WideString; out Nombre: WideString): SYSINT; safecall;
    procedure SetParametro(Numero: SYSINT; Valor: OleVariant); safecall;
    function EscogeEmpresaDef(const Usuario: WideString; const Clave: WideString; 
                              const EmpresaDef: WideString; out Codigo: WideString; 
                              out Nombre: WideString): WordBool; safecall;
    function GetLoggedUser: WideString; safecall;
    function LoginCheckAD(const Usuario: WideString; const Clave: WideString; TipoLogin: Integer): SYSINT; safecall;
    function EscogeEmpresaAD(const Usuario: WideString; const Clave: WideString; 
                             out Codigo: WideString; out Nombre: WideString; TipoLogin: Integer): WordBool; safecall;
    function EscogeEmpresaDefAD(const Usuario: WideString; const Clave: WideString; 
                                const EmpresaDef: WideString; out Codigo: WideString; 
                                out Nombre: WideString; TipoLogin: Integer): WordBool; safecall;
    function EscogerUnReporteAD(const Usuario: WideString; const Password: WideString; 
                                const Empresa: WideString; out Nombre: WideString; 
                                TipoLogin: Integer): SYSINT; safecall;
    function GeneraReporteAD(const Usuario: WideString; const Password: WideString; 
                             const Empresa: WideString; Reporte: Integer; out Nombre: WideString; 
                             TipoLogin: Integer): SYSINT; safecall;
    function GetValorActivoAD(const Usuario: WideString; const Password: WideString; 
                              const Empresa: WideString; const Nombre: WideString; 
                              Default: OleVariant; TipoLogin: Integer): OleVariant; safecall;
    procedure SetValorActivoAD(const Usuario: WideString; const Password: WideString; 
                               const Empresa: WideString; const Nombre: WideString; 
                               Valor: OleVariant; TipoLogin: Integer); safecall;
    function ProcesaQueryAD(const Usuario: WideString; const Password: WideString; 
                            const Empresa: WideString; const Query: WideString; TipoLogin: Integer): SYSINT; safecall;
  end;

// *********************************************************************//
// DispIntf:  ITress2ExcelDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {830BC8E5-EA79-44DC-BA30-33D554F0D03C}
// *********************************************************************//
  ITress2ExcelDisp = dispinterface
    ['{830BC8E5-EA79-44DC-BA30-33D554F0D03C}']
    function LoginCheck(const Usuario: WideString; const Clave: WideString): SYSINT; dispid 1;
    function EscogeEmpresa(const Usuario: WideString; const Clave: WideString; 
                           out Codigo: WideString; out Nombre: WideString): WordBool; dispid 2;
    function ProcesaQuery(const Usuario: WideString; const Password: WideString; 
                          const Empresa: WideString; const Query: WideString): SYSINT; dispid 3;
    procedure First; dispid 4;
    function EoF: WordBool; dispid 5;
    procedure Next; dispid 6;
    function EoRow: WordBool; dispid 7;
    procedure NextCol; dispid 8;
    function Valor: OleVariant; dispid 9;
    function FieldName: WideString; dispid 10;
    function Status: WideString; dispid 11;
    procedure ParamAsString(const Nombre: WideString; const Valor: WideString); dispid 12;
    procedure ParamAsInteger(const Nombre: WideString; Valor: Integer); dispid 13;
    procedure ParamAsFloat(const Nombre: WideString; Valor: Single); dispid 14;
    procedure ParamAsDate(const Nombre: WideString; Valor: TDateTime); dispid 15;
    procedure SetDateFormat(const Valor: WideString); dispid 16;
    function GetValorActivo(const Usuario: WideString; const Password: WideString; 
                            const Empresa: WideString; const Nombre: WideString; Default: OleVariant): OleVariant; dispid 18;
    procedure SetValorActivo(const Usuario: WideString; const Password: WideString; 
                             const Empresa: WideString; const Nombre: WideString; Valor: OleVariant); dispid 19;
    function GeneraReporte(const Usuario: WideString; const Password: WideString; 
                           const Empresa: WideString; Reporte: Integer; out Nombre: WideString): SYSINT; dispid 17;
    function EscogerUnReporte(const Usuario: WideString; const Password: WideString; 
                              const Empresa: WideString; out Nombre: WideString): SYSINT; dispid 20;
    procedure SetParametro(Numero: SYSINT; Valor: OleVariant); dispid 21;
    function EscogeEmpresaDef(const Usuario: WideString; const Clave: WideString; 
                              const EmpresaDef: WideString; out Codigo: WideString; 
                              out Nombre: WideString): WordBool; dispid 22;
    function GetLoggedUser: WideString; dispid 201;
    function LoginCheckAD(const Usuario: WideString; const Clave: WideString; TipoLogin: Integer): SYSINT; dispid 202;
    function EscogeEmpresaAD(const Usuario: WideString; const Clave: WideString; 
                             out Codigo: WideString; out Nombre: WideString; TipoLogin: Integer): WordBool; dispid 203;
    function EscogeEmpresaDefAD(const Usuario: WideString; const Clave: WideString; 
                                const EmpresaDef: WideString; out Codigo: WideString; 
                                out Nombre: WideString; TipoLogin: Integer): WordBool; dispid 204;
    function EscogerUnReporteAD(const Usuario: WideString; const Password: WideString; 
                                const Empresa: WideString; out Nombre: WideString; 
                                TipoLogin: Integer): SYSINT; dispid 205;
    function GeneraReporteAD(const Usuario: WideString; const Password: WideString; 
                             const Empresa: WideString; Reporte: Integer; out Nombre: WideString; 
                             TipoLogin: Integer): SYSINT; dispid 206;
    function GetValorActivoAD(const Usuario: WideString; const Password: WideString; 
                              const Empresa: WideString; const Nombre: WideString; 
                              Default: OleVariant; TipoLogin: Integer): OleVariant; dispid 207;
    procedure SetValorActivoAD(const Usuario: WideString; const Password: WideString; 
                               const Empresa: WideString; const Nombre: WideString; 
                               Valor: OleVariant; TipoLogin: Integer); dispid 208;
    function ProcesaQueryAD(const Usuario: WideString; const Password: WideString; 
                            const Empresa: WideString; const Query: WideString; TipoLogin: Integer): SYSINT; dispid 209;
  end;

// *********************************************************************//
// The Class CoTress2Excel provides a Create and CreateRemote method to          
// create instances of the default interface ITress2Excel exposed by              
// the CoClass Tress2Excel. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTress2Excel = class
    class function Create: ITress2Excel;
    class function CreateRemote(const MachineName: string): ITress2Excel;
  end;

implementation

uses ComObj;

class function CoTress2Excel.Create: ITress2Excel;
begin
  Result := CreateComObject(CLASS_Tress2Excel) as ITress2Excel;
end;

class function CoTress2Excel.CreateRemote(const MachineName: string): ITress2Excel;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Tress2Excel) as ITress2Excel;
end;

end.
