inherited EscogerReporte: TEscogerReporte
  ActiveControl = Clasificacion
  Caption = 'Escoja El Reporte Deseado'
  ClientHeight = 300
  ClientWidth = 561
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 264
    Width = 561
    inherited OK: TBitBtn
      Left = 396
    end
    inherited Cancelar: TBitBtn
      Left = 478
    end
  end
  object ZetaDBGrid: TZetaDBGrid
    Left = 0
    Top = 41
    Width = 561
    Height = 223
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'RE_CODIGO'
        Title.Caption = 'N�mero'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RE_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 319
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RE_ENTIDAD'
        Title.Caption = 'Tabla Principal'
        Width = 154
        Visible = True
      end>
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 561
    Height = 41
    Align = alTop
    TabOrder = 2
    object ClasificacionLBL: TLabel
      Left = 6
      Top = 12
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Clasificaci�n:'
    end
    object lbBusca: TLabel
      Left = 225
      Top = 13
      Width = 33
      Height = 13
      Caption = 'Busca:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BBusca: TSpeedButton
      Left = 385
      Top = 6
      Width = 27
      Height = 27
      Hint = 'Buscar Reporte'
      AllowAllUp = True
      GroupIndex = 1
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      OnClick = BBuscaClick
    end
    object Clasificacion: TZetaKeyCombo
      Left = 72
      Top = 9
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnClick = ClasificacionClick
      ListaFija = lfClasifiReporte
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
    object EBuscaNombre: TEdit
      Left = 260
      Top = 9
      Width = 121
      Height = 21
      TabOrder = 1
      OnChange = EBuscaNombreChange
    end
  end
  object DataSource: TDataSource
    Left = 56
    Top = 56
  end
end
