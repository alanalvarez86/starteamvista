unit FTress2Excel;

{$INCLUDE DEFINES.INC}

interface

uses SysUtils, Classes, ComObj, ComServ, ActiveX, StdVcl, Dialogs, ZetaEscogeCia_DevEx,
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaCommonClasses,
     ZetaCommonLists,
     FEscogerReporte,
     dxCore,
     TressExcel_TLB;

type
  TTress2Excel = class(TAutoObject, ITress2Excel)
  private
    { Private declarations }
    FStatus: String;
    FParamList: TStrings;
    FEscoger: TEscogerReporte;
    function DoLogin( const sUserName, sPassword: String;TipoLogin:Integer ): Boolean;
    function Parametriza(const sQuery: String): String;
    procedure AgregaParametro(const sNombre, sValor: String);
    procedure ManejaError( Error: Exception ); overload;
    procedure ManejaError( Error: Exception; const sMensaje: String ); overload;
    procedure Start;
    procedure Stop;
    procedure SetStatusProceso(const iResultado: Integer);
    function LoginActiveDirectory: Boolean;
  public
    { Public declarations }
    destructor Destroy; override;
  protected
    { Protected declarations }
    function EoF: WordBool; safecall;
    function EoRow: WordBool; safecall;
    function EscogeEmpresaDef(const Usuario, Clave, EmpresaDef: WideString; out Codigo, Nombre: WideString): WordBool; safecall;
    function EscogeEmpresa(const Usuario, Clave: WideString; out Codigo, Nombre: WideString): WordBool; safecall;
    function EscogerUnReporte(const Usuario, Password, Empresa: WideString; out Nombre: WideString ): SYSINT; safecall;
    function FieldName: WideString; safecall;
    function GeneraReporte(const Usuario, Password, Empresa: WideString; Reporte: Integer; out Nombre: WideString ): SYSINT; safecall;
    function GetValorActivo(const Usuario, Password, Empresa, Nombre: WideString; Default: OleVariant ): OleVariant; safecall;
    function LoginCheck(const Usuario, Clave: WideString ): SYSINT; safecall;
    function ProcesaQuery(const Usuario, Password, Empresa, Query: WideString ): SYSINT; safecall;
    function Status: WideString; safecall;
    function Valor: OleVariant; safecall;
    procedure First; safecall;
    procedure Next; safecall;
    procedure NextCol; safecall;
    procedure ParamAsString(const Nombre, Valor: WideString); safecall;
    procedure ParamAsInteger(const Nombre: WideString; Valor: Integer); safecall;
    procedure ParamAsFloat(const Nombre: WideString; Valor: Single); safecall;
    procedure ParamAsDate(const Nombre: WideString; Valor: TDateTime); safecall;
    procedure SetDateFormat(const Valor: WideString); safecall;
    procedure SetParametro(Numero: SYSINT; Valor: OleVariant); safecall;
    procedure SetValorActivo(const Usuario, Password, Empresa, Nombre: WideString; Valor: OleVariant); safecall;
    function GetLoggedUser: WideString; safecall;
    function LoginCheckAD(const Usuario, Clave: WideString;TipoLogin: Integer): SYSINT; safecall;
    function EscogeEmpresaAD(const Usuario, Clave: WideString; out Codigo, Nombre: WideString; TipoLogin: Integer): WordBool; safecall;
    function EscogeEmpresaDefAD(const Usuario, Clave, EmpresaDef: WideString; out Codigo, Nombre: WideString; TipoLogin: Integer): WordBool; safecall;
    function EscogerUnReporteAD(const Usuario, Password, Empresa: WideString; out Nombre: WideString; TipoLogin: Integer): SYSINT; safecall;
    function GeneraReporteAD(const Usuario, Password, Empresa: WideString; Reporte: Integer; out Nombre: WideString; TipoLogin: Integer): SYSINT; safecall;
    function GetValorActivoAD(const Usuario, Password, Empresa, Nombre: WideString; Default: OleVariant; TipoLogin: Integer): OleVariant; safecall;
    procedure SetValorActivoAD(const Usuario, Password, Empresa, Nombre: WideString; Valor: OleVariant; TipoLogin: Integer); safecall;
    function ProcesaQueryAD(const Usuario, Password, Empresa, Query: WideString; TipoLogin: Integer): SYSINT; safecall;
  end;

implementation

uses DCliente,
     DReportesExcel,
     FAutoClasses,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaClientTools,
     DBasicoCliente,
     {$IFDEF DOS_CAPAS}
     ZetaRegistryServerEditor,
     {$ENDIF}
     ZetaRegistryCliente;

const
     K_EMPTY_DATASET = 0;
     K_LOGIN_ERROR = -1;
     K_COMPANY_ERROR = -2;
     K_EXCEPCION = -3;
     K_TIPO_LOGINTRESS = 0;
     //Cambiar en cada version 
     K_VERSION_APP ='4.2';

{ *********** TTress2Excel ************ }

destructor TTress2Excel.Destroy;
begin
     Stop;
     inherited Destroy;
end;

procedure TTress2Excel.Start;
begin
     FStatus := VACIO;
     {
     if not Assigned( dmCliente ) then
        dmCliente := TdmCliente.Create( nil );
     }
     if not Assigned( dmReportesExcel ) then
     begin
          dmReportesExcel := TdmReportesExcel.Create( nil );
          dmReportesExcel.Resultado := dmCliente.cdsQuery;
     end;
     if not Assigned( FParamList ) then
        FParamList := TStringList.Create;
end;

procedure TTress2Excel.Stop;
begin
     FreeAndNil( FEscoger );
     FreeAndNil( FParamList );
     FreeAndNil( dmCliente );
     FreeAndNil( dmReportesExcel );
end;

function TTress2Excel.DoLogin( const sUserName, sPassword: String;TipoLogin:Integer  ): Boolean;
var
   sVersionDLL: String;

   function GetVersionDLL( const sVersion: String ): String;
   var
      iPos: Integer;
   begin
        iPos := Pos( '.', sVersion );   // Ubica el primer punto de la version
        iPos := Pos( '.', sVersion, iPos + 1 );   //Ubica el segundo punto de la version
        Result := Copy( sVersion, 1, iPos - 1 );
   end;

begin
     if TipoLogin = 1 then
        Result := LoginActiveDirectory
     else
         Result := dmCliente.UsuarioLogin( sUserName, sPassword );
     if Result then
     begin
          with Autorizacion do
          begin
               { Verifica Versi�n del Cliente vs Versi�n del Servidor }
               sVersionDLL := GetVersionDLL( Autorizacion.VersionArchivo );
               if ( sVersionDLL <> K_VERSION_APP ) then
               begin
                    FStatus := Format( 'La Versi�n Del Servidor De Procesos De Tress ( %s ) Es Diferente De La Versi�n De Este Programa ( %s )', [ sVersionDLL, K_VERSION_APP ] );
                    Result := False;
               end;
          end;
     end
     else
     begin
          if FStatus = VACIO then
             FStatus := Format( 'Usuario %s � Clave Err�nea', [ sUserName, sPassword ] );
     end;
end;

procedure TTress2Excel.ManejaError( Error: Exception );
begin
     ManejaError( Error, 'Se Encontr� Un Error' );
end;

procedure TTress2Excel.ManejaError( Error: Exception; const sMensaje: String );
begin
     FStatus := Error.Message;
     ZetaDialogo.zExcepcion( '� Atenci�n !', sMensaje, Error, 0 );
end;

procedure TTress2Excel.AgregaParametro( const sNombre, sValor: String);
const
     K_FORMATO = '%s=%s';
var
   iPos: Integer;
   sTexto: String;
begin
     Start;
     sTexto := Format( K_FORMATO, [ sNombre, sValor ] );
     with FParamList do
     begin
          iPos := IndexOfName( sNombre );
          if ( iPos < 0 ) then
             Add( sTexto )
          else
              Strings[ iPos ] := sTexto;
     end;
end;

function TTress2Excel.Parametriza( const sQuery: String): String;
var
   i: Integer;
   sParametro, sValor: String;
begin
     Result := sQuery;
     with FParamList do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sParametro := Names[ i ];
               sValor := Values[ sParametro ];
               Result := ZetaCommonTools.StrTransAll( Result, sParametro, sValor );
          end;
     end;
end;

{ ********** LLamadas COM ********** }

function TTress2Excel.LoginCheck(const Usuario, Clave: WideString ): SYSINT;
begin
     Result := LoginCheckAD(Usuario,Clave,K_TIPO_LOGINTRESS);
end;

function TTress2Excel.EscogeEmpresaDef(const Usuario, Clave, EmpresaDef: WideString; out Codigo, Nombre: WideString ): WordBool;
begin
     Result := EscogeEmpresaDefAD(Usuario,Clave,EmpresaDef,Codigo,Nombre,K_TIPO_LOGINTRESS);
end;

function TTress2Excel.EscogeEmpresa(const Usuario, Clave: WideString; out Codigo, Nombre: WideString): WordBool;
begin
     Result := EscogeEmpresaDefAD( Usuario, Clave, ZetaCommonClasses.VACIO, Codigo, Nombre,K_TIPO_LOGINTRESS);
end;

function TTress2Excel.ProcesaQuery(const Usuario, Password, Empresa,
  Query: WideString ): SYSINT;
begin
     Result := ProcesaQueryAD(Usuario,Password,Empresa,Query,K_TIPO_LOGINTRESS);
end;

function TTress2Excel.EoF: WordBool;
begin
     Result := dmCliente.EoF;
end;

function TTress2Excel.EoRow: WordBool;
begin
     Result := dmCliente.EoRow;
end;

procedure TTress2Excel.First;
begin
     dmCliente.First;
end;

procedure TTress2Excel.Next;
begin
     dmCliente.Next;
end;

procedure TTress2Excel.NextCol;
begin
     dmCliente.NextCol;
end;

function TTress2Excel.Valor: OleVariant;
begin
     Result := dmCliente.Valor;
end;

function TTress2Excel.FieldName: WideString;
begin
     Result := dmCliente.FieldName;
end;

function TTress2Excel.Status: WideString;
begin
     Result := FStatus;
end;

procedure TTress2Excel.ParamAsString(const Nombre, Valor: WideString);
begin
     AgregaParametro( Nombre, Valor );
end;

procedure TTress2Excel.ParamAsInteger(const Nombre: WideString; Valor: Integer);
begin
     AgregaParametro( Nombre, IntToStr( Valor ) );
end;

procedure TTress2Excel.ParamAsFloat(const Nombre: WideString;  Valor: Single);
begin
     AgregaParametro( Nombre, Format( '%n', [ Valor ] ) );
end;

procedure TTress2Excel.ParamAsDate(const Nombre: WideString; Valor: TDateTime);
begin
     AgregaParametro( Nombre, Format( '''%s''', [ FormatDateTime( 'dd/mm/yyyy', Valor ) ] ) );
end;

procedure TTress2Excel.SetDateFormat(const Valor: WideString);
begin
     Start;
     dmCliente.FormatoFecha := Valor;
end;

function TTress2Excel.GetValorActivo(const Usuario, Password, Empresa,
  Nombre: WideString; Default: OleVariant ): OleVariant;
begin
     Result := GetValorActivoAD(Usuario,Password,Empresa,Nombre,Default,K_TIPO_LOGINTRESS);
end;

procedure TTress2Excel.SetValorActivo(const Usuario, Password, Empresa,
  Nombre: WideString; Valor: OleVariant);
begin
     SetValorActivoAD(Usuario ,Password ,Empresa ,Nombre,Valor,K_TIPO_LOGINTRESS);
end;

procedure TTress2Excel.SetStatusProceso( const iResultado: Integer );
begin
     if ( iResultado = 0 ) then
        FStatus := 'No Hay Datos'
     else
         if Autorizacion.EsDemo then
            FStatus := 'DEMO'
         else
             FStatus := 'OK';
end;

function TTress2Excel.GeneraReporte(const Usuario, Password,
  Empresa: WideString; Reporte: Integer; out Nombre: WideString ): SYSINT;
begin
     Result := GeneraReporteAD(Usuario,Password,Empresa,Reporte,Nombre,K_TIPO_LOGINTRESS);
end;

function TTress2Excel.EscogerUnReporte(const Usuario, Password,
  Empresa: WideString; out Nombre: WideString ): SYSINT;
begin
     Result := EscogerUnReporteAD(Usuario,Password,Empresa,Nombre,K_TIPO_LOGINTRESS);
end;

procedure TTress2Excel.SetParametro(Numero: SYSINT; Valor: OleVariant);
begin
     Start;
     with dmReportesExcel do
     begin
          case VarType( Valor ) of
               varSmallint: SetParamValue( Numero, IntToStr( Valor ) );
               varInteger: SetParamValue( Numero, IntToStr( Valor ) );
               varSingle: SetParamValue( Numero, FloatToStr( Valor ) );
               varDouble: SetParamValue( Numero, FloatToStr( Valor ) );
               varCurrency: SetParamValue( Numero, FloatToStr( Valor ) );
               varDate: SetParamValue( Numero, ZetaCommonTools.FechaAsStr( Valor ) );
               varOleStr: SetParamValue( Numero, Valor );
               varBoolean: SetParamValue( Numero, ZetaCommonTools.zBoolToStr( Valor ) );
               varByte: SetParamValue( Numero, IntToStr( Valor ) );
               varString: SetParamValue( Numero, Valor );
          end;
     end;
end;

function TTress2Excel.LoginActiveDirectory(): Boolean;
var
   eReply: eLogReply;
   iTipo : Integer;
   eTipo : eTipoLogin;
   lRegistry: Boolean;
begin
     Result := False;
     try
        {$ifdef DOS_CAPAS}
        lRegistry := ZetaRegistryServerEditor.InitComparte;
        {$else}
        lRegistry := ClientRegistry.InitComputerName;
        {$endif}
        if lRegistry then
        begin
             eReply := dmCliente.LoginActiveDirectory(false,iTipo);
             eTipo := eTipoLogin(iTipo);
             case eTipo of
                  tlAD,tlADLoginTress:
                     begin
                          if (eReply = lrNotFound )then
                          begin
                               Result := False;
                               FStatus := Format('El usuario: %s no tiene acceso al Sistema TRESS',[dmCliente.LoggedOnUserNameEx] );
                          end;
                          if (eReply = lrUserInactive )then
                          begin
                               Result := False;
                               FStatus := Format('El usuario: %s No est� Activo en Sistema TRESS',[dmCliente.LoggedOnUserNameEx] );
                          end;
                          if (eReply = lrOk )then
                          begin
                               Result := True;
                          end;
                     end;
             else
                 Result:= False;

             end;
        end;
     except
           on Error: Exception do
           begin
                ManejaError( Error );
           end;
     end;

end;



function TTress2Excel.GetLoggedUser: WideString;
begin
     Result := dmCliente.LoggedOnUserNameEx; 
end;

function TTress2Excel.LoginCheckAD(const Usuario, Clave: WideString;TipoLogin: Integer): SYSINT;
begin
     try
           CoInitialize(nil);
           Result := 0;
           Start;
           try
              if DoLogin( Usuario, Clave, TipoLogin ) then
              begin
      {
                   if Autorizacion.EsDemo then
                      ZetaDialogo.zWarning( '� Modo DEMO !', Format( '� Bienvenido A Tress ! %0:s %0:s Versi�n %1:s DEMO', [ CR_LF, VERSION_PRODUCTO ] ), 0, mbOK )
                   else
                       ZetaDialogo.zInformation( '� Exito !', Format( '� Bienvenido A Tress ! %0:s %0:s Versi�n %1:s', [ CR_LF, VERSION_PRODUCTO ] ), 0 );
      }
                   if Autorizacion.EsDemo then
                      ZetaDialogo.zWarning( '� Modo DEMO !', Format( '� Bienvenido A Tress ! %0:s %0:s Versi�n %1:s DEMO %0:s %0:s Build %2:s', [ CR_LF, VERSION_PRODUCTO, VERSION_ARCHIVO ] ), 0, mbOK )
                   else
                       ZetaDialogo.zInformation( '� Exito !', Format( '� Bienvenido A Tress ! %0:s %0:s Versi�n %1:s %0:s %0:s Build %2:s', [ CR_LF, VERSION_PRODUCTO, VERSION_ARCHIVO ] ), 0 );

                   Result := 1;
              end
              else
                  ZetaDialogo.zError( '� Acceso Negado !', FStatus, 0 );
           except
                 on Error: Exception do
                 begin
                      ManejaError( Error );
                 end;
           end;
           Stop;
     finally
        CoUninitialize;
     end;
end;

function TTress2Excel.EscogeEmpresaAD(const Usuario, Clave: WideString;
  out Codigo, Nombre: WideString; TipoLogin: Integer): WordBool;
begin

end;

function TTress2Excel.EscogeEmpresaDefAD(const Usuario, Clave, EmpresaDef: WideString; out Codigo, Nombre: WideString; TipoLogin: Integer): WordBool;
begin
      Result := False;
     Codigo := VACIO;
     Nombre := VACIO;
     try
       CoInitialize(nil);
       try
          Start;
          if DoLogin( Usuario, Clave ,TipoLogin) then
          begin
               with dmCliente do
               begin

                    if InitCompanies then
                    begin
                         InitCompany;
                         with Autorizacion do
                         begin
                               if ZetaEscogeCia_DevEx.EscogeUnaEmpresa_DevEx( EsDemo, EsKit, EmpresaDef ) then
                               begin
                                    Codigo := GetDatosEmpresaActiva.Codigo;
                                    Nombre := GetDatosEmpresaActiva.Nombre;
                                    Result := True;
                               end;
                         end;
                    end
                    else
                        ZetaDialogo.zError( '� Atenci�n !', 'No Hay Empresas', 0 );
               end;
          end
          else
              ZetaDialogo.zError( '� Acceso Negado !', FStatus, 0 );
       except
             on Error: Exception do
             begin
                  ManejaError( Error );
             end;
       end;
     finally
        Stop;
        CoUninitialize;
     end;
end;

function TTress2Excel.EscogerUnReporteAD(const Usuario, Password,
  Empresa: WideString; out Nombre: WideString; TipoLogin: Integer): SYSINT;
begin
     try
        Result := -1;
        Nombre := VACIO;
        Start;
        if ( dmCliente.Usuario > 0 ) or DoLogin( Usuario, Password ,TipoLogin) then
        begin
             if dmCliente.SetEmpresaActiva( Empresa ) then
             begin
                  if not Assigned( FEscoger ) then
                     FEscoger := TEscogerReporte.Create( nil );
                  with FEscoger do
                  begin
                       ShowModal;
                       if OKPressed then
                       begin
                            Result := GetReporteNumero;
                            Nombre := GetReporteNombre;
                       end;
                  end;
             end
             else
                 ZetaDialogo.zError( '� Acceso Negado !', Format( 'Empresa %s Inv�lida', [ Empresa ] ), 0 );
        end
        else
            ZetaDialogo.zError( '� Acceso Negado !', FStatus, 0 );
     except
           on Error: Exception do
           begin
                ManejaError( Error );
           end;
     end;
end;

function TTress2Excel.GeneraReporteAD(const Usuario, Password,Empresa: WideString; Reporte: Integer; out Nombre: WideString;TipoLogin: Integer): SYSINT;
var
   sNombre: String;
begin
     Result := K_EMPTY_DATASET;
     Nombre := VACIO;
     try
        Start;
        if ( dmCliente.Usuario > 0 ) or DoLogin( Usuario, Password,TipoLogin ) then
        begin
             if dmCliente.SetEmpresaActiva( Empresa ) then
             begin
                  dmCliente.InitResultDataset;
                  with dmReportesExcel do
                  begin
                       Result := Procesar( Empresa, Reporte, sNombre );
                       Nombre := sNombre;
                       SetStatusProceso( Result );
                  end;
             end
             else
             begin
                  FStatus := Format( 'Empresa %s Inv�lida', [ Empresa ] );
                  ZetaDialogo.zError( '� Acceso Negado !', FStatus, 0 );
                  Result := K_COMPANY_ERROR;
             end;
        end
        else
        begin
             ZetaDialogo.zError( '� Acceso Negado !', FStatus, 0 );
             Result := K_LOGIN_ERROR;
        end;
     except
           on Error: Exception do
           begin
                ManejaError( Error );
                Result := K_EXCEPCION;
           end;
     end;

end;

function TTress2Excel.GetValorActivoAD(const Usuario, Password, Empresa,
  Nombre: WideString; Default: OleVariant; TipoLogin: Integer): OleVariant;
begin
     Start;
     Result := Default;
     if ( dmCliente.Usuario > 0 ) or DoLogin( Usuario, Password ,TipoLogin) then
     begin
          if dmCliente.SetEmpresaActiva( Empresa ) then
          begin
               Result := dmCliente.GetVarActivo( Nombre );
          end;
     end;
end;

procedure TTress2Excel.SetValorActivoAD(const Usuario, Password, Empresa,
  Nombre: WideString; Valor: OleVariant; TipoLogin: Integer);
begin
     Start;
     if ( dmCliente.Usuario > 0 ) or DoLogin( Usuario, Password,TipoLogin ) then
     begin
          if dmCliente.SetEmpresaActiva( Empresa ) then
          begin
               dmCliente.SetVarActivo( Nombre, Valor );
          end;
     end;
end;

function TTress2Excel.ProcesaQueryAD(const Usuario, Password, Empresa,
  Query: WideString; TipoLogin: Integer): SYSINT;
begin
      Result := K_EMPTY_DATASET;
     try
        Start;
        if ( dmCliente.Usuario > 0 ) or DoLogin( Usuario, Password ,TipoLogin) then
        begin
             if dmCliente.SetEmpresaActiva( Empresa ) then
             begin
                  if ZAccesosMgr.CheckDerecho( D_CONS_SQL, K_DERECHO_CONSULTA ) then
                  begin
                       dmCliente.InitResultDataset;
                       Result := dmCliente.GetSQLData( Parametriza( Query ) );
                       SetStatusProceso( Result );
                  end
                  else
                      FStatus := 'No Tiene Derecho De Acceso A Consultas De SQL';
             end
             else
             begin
                  FStatus := Format( 'Empresa %s Inv�lida', [ Empresa ] );
                  ZetaDialogo.zError( '� Acceso Negado !', FStatus, 0 );
                  Result := K_COMPANY_ERROR;
             end;
        end
        else
        begin
             ZetaDialogo.zError( '� Acceso Negado !', FStatus, 0 );
             Result := K_LOGIN_ERROR;
        end;
     except
           on Error: Exception do
           begin
                ManejaError( Error );
                Result := K_EXCEPCION;
           end;
     end;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TTress2Excel, Class_Tress2Excel, ciMultiInstance, tmSingle);
  dxInitialize;

Finalization
  dxFinalize;
end.
