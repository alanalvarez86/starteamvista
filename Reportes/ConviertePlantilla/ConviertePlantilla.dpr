program ConviertePlantilla;

uses
  MidasLib,
  Forms,
  FWizConviertePlantilla in 'FWizConviertePlantilla.pas' {WizConviertePlantilla},
  ZcxWizardBasico in '..\..\Tools\ZcxWizardBasico.pas' {CXWizardBasico};

{$R *.res}
{$R ..\..\Traducciones\Spanish.RES}
begin
  Application.Initialize;
  Application.Title := 'Convierte Plantilla';
  Application.CreateForm(TWizConviertePlantilla, WizConviertePlantilla);
  with WizConviertePlantilla do
  begin
        Application.Run;
  end;
end.
