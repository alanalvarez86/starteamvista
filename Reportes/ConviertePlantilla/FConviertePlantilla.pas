unit FConviertePlantilla;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, FileCtrl, Buttons, StdCtrls,
  QRDesign, ExtCtrls, QuickRpt,
  ComCtrls, QRDLDR, thsdRuntimeLoader, thsdRuntimeEditor, qrdBaseCtrls,
  qrdQuickrep, QRPrntr, StrUtils;

type
  TTConviertePlantilla = class(TForm)
    QRepDesigner: TQRepDesigner;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    lbPlantillas: TLabel;
    Buscar: TSpeedButton;
    Plantillas: TEdit;
    BitBtn1: TBitBtn;
    GroupBox2: TGroupBox;
    Bitacora: TMemo;
    DesignQuickReport1: TDesignQuickReport;
    procedure BuscarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    function BuscarDirectorio(const Value: String): String;
    procedure Convertir(const sDirectorio: string);
    procedure GrabaNuevaPlantilla(const sPlantilla: string);
    procedure Log(const sMensaje: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TConviertePlantilla: TTConviertePlantilla;

implementation
uses
    ZetaCommonClasses,
    ZetaCommonTools;

{$R *.dfm}

procedure TTConviertePlantilla.BuscarClick(Sender: TObject);
begin
     Plantillas.Text := BuscarDirectorio(Plantillas.Text);
end;

function TTConviertePlantilla.BuscarDirectorio( const Value: String ): String;
var
   sDirectory: String;
begin
     sDirectory := Value;
     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        Result := sDirectory
     else
         Result := Value;
end;
procedure TTConviertePlantilla.BitBtn1Click(Sender: TObject);
begin
     Convertir(VerificaDir( Plantillas.Text ));
end;

procedure TTConviertePlantilla.GrabaNuevaPlantilla(const sPlantilla: string);
function StrFixTitle( sOrigen: String ): String;
var
    sFind:String;
    i:Integer;
begin
    sFind := LeftStr(sOrigen,1);
    i := AnsiPos( AnsiUpperCase( sFind ),AnsiUpperCase( sOrigen ) );
        if ( i > 0 ) then
            Delete( sOrigen, i, Length( sFind ) );
    Result := sOrigen;
end;

begin
     if FileExists( sPlantilla ) then
     begin
          //sPlantillaAnt := StrTransForm( sPlantilla, '.QR2', '.QRBak' );

          //RenameFile(sPlantilla, sPlantillaAnt );
          try
             if not QRepDesigner.LoadReport(sPlantilla)then
             begin
                Log(Format( 'Error al Cargar Plantilla %s', [sPlantilla]));
             end
             else
             begin
                 try
                      //(@am): Corregir Titulo del reporte, el cual se carga el caracter #2 en plantillas generadas con otra version de QR.
                      QRepDesigner.QReport.ReportTitle := StrFixTitle(QRepDesigner.QReport.ReportTitle);
                      QRepDesigner.SaveReport(sPlantilla);
                      Log(Format( 'Plantilla %s ha sido actualizada al nuevo formato' ,[sPlantilla]));
                 except
                         On Error:Exception do
                            Log(Format( 'Error al grabar la plantilla %s' +CR_LF + Error.Message, [sPlantilla]));
                 end;
             end;
          finally
          end;

     end
     else
         Log(Format('La plantilla %s no existe', [sPlantilla]));
end;

procedure TTConviertePlantilla.Convertir( const sDirectorio: string );
 var
    oFile: TSearchRec;
    sArchivo: string;
begin
     Bitacora.Lines.Clear;
     Log( '* * * * INICIA CONVERSION DE PLANTILLAS  * * * *');
     Log('');
     sArchivo := '*.QR2';

     if DirectoryExists(sDirectorio) then
     begin
          if FindFirst( sDirectorio + sArchivo, faArchive, oFile) = 0 then
          begin
               GrabaNuevaPlantilla( sDirectorio + oFile.Name );
               while ( FindNext( oFile ) = 0 ) do
               begin
                    GrabaNuevaPlantilla( sDirectorio + oFile.Name );
               end;
          end;

     end
     else
     begin
          Log(Format( 'El directorio %s no existe',[sDirectorio]));
     end;
     Log('');
     Log( '* * * * CONVERSION DE PLANTILLAS TERMINADA * * * *');
end;

procedure TTConviertePlantilla.Log( const sMensaje : string );
begin
     Bitacora.Lines.Add(sMensaje);     
end;

end.
