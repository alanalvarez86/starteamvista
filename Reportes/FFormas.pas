unit FFormas;

interface
{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FEditBaseReportes, StdCtrls, Buttons, ExtCtrls, ZetaSmartLists,
  ZetaKeyLookup, ZetaEdit, ZetaFecha, CheckLst, Db, ZetaKeyCombo, DBCtrls,
  Mask, ZetaDBTextBox, ComCtrls, ZetaNumero, FEditReportes, Grids, DBGrids,
  ZetaDBGrid,
  ZetaCommonLists,
  ZetaCommonClasses,
  ZReportTools,
  //(@am): CAMBIO TEMPORAL
  //TDMULTIP,  //(@am): Se necesita cambiar comp. para imagenes.
  ZReportToolsConsts;

type
  TFormas = class(TEditBaseReportes)
    lbPlantilla: TLabel;
    RE_REPORTE: TZetaDBEdit;
    bPlantilla: TSpeedButton;
    BGuardaArchivo: TSpeedButton;
    RE_ARCHIVO: TZetaDBEdit;
    lbNombreArchivo: TLabel;
    lbCopias: TLabel;
    RE_COPIAS: TZetaDBNumero;
    UpDown: TUpDown;
    EPAGINAS: TLabel;
    EPaginaInicial: TEdit;
    EPAginasAl: TLabel;
    EPaginaFinal: TEdit;
    RE_PRINTER: TComboBox;
    lbImpresora: TLabel;
    Label1: TLabel;
    RE_PFILE: TZetaDBKeyCombo;
    bExportPreferencias: TSpeedButton;
    bEvaluaArchivo: TSpeedButton;
    NombreArchivoFormula: TMemo;
    procedure bPlantillaClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure RE_PRINTERChange(Sender: TObject);
    procedure RE_PFILEChange(Sender: TObject);
    procedure BGuardaArchivoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bExportPreferenciasClick(Sender: TObject);
    procedure RE_ARCHIVOChange(Sender: TObject);
    procedure bDisenadorClick(Sender: TObject);
    procedure bEvaluaArchivoClick(Sender: TObject);
  private
    FImpresoraDefault : integer;
    function GetDatosImpresion : TDatosImpresion;
    procedure EnabledArchivo(const lEnabled: Boolean;
      const eTipo: eTipoFormato);
    procedure EnabledImpresora(const lEnabled: Boolean);
    procedure EnabledPlantilla(const lEnabled: Boolean);
    procedure EnabledPrintSetting( const eTipo : eTipoFormato );
    function GetTipoFormato: eTipoFormato;
  protected
    procedure AntesDeEscribirCambios;override;
    procedure LLenaListas;override;
    procedure AgregaSQLCampoRep;override;

    procedure AntesDeConstruyeSQL;override;
    function GeneraReporte: Boolean;override;
    function GetFirstPestana: TTabSheet;override;

  public
    { Public declarations }
  end;

var
  Formas: TFormas;

implementation
uses Printers,
     DExportEngine,
     ZetaCommonTools,
     ZetaWinApiTools,
{$ifdef HTTP_CONNECTION}
     ZetaRegistryCliente,
{$endif}     
     ZQRReporte,
     ZetaDialogo;

{$R *.DFM}

procedure TFormas.bPlantillaClick(Sender: TObject);
 var sTexto : string;
begin
     inherited;
     sTexto := DialogoPlantilla( RE_REPORTE.Text );
     if sTexto <> RE_REPORTE.Text then
     begin
          Modo := dsEdit;
          Reporte.Edit;
          Reporte.FieldByName( 'RE_REPORTE').AsString := sTexto;
     end;
end;

procedure TFormas.LLenaListas;
 var fPrinter : TPrinter;
     i: integer;
begin
     inherited;
     FImpresoraDefault := -1;
     RE_PRINTER.Items.Clear;

     fPrinter := TPrinter.Create;

     with fPrinter do
     begin
          if Printers.Count > 0 then
          begin
               FImpresoraDefault := PrinterIndex;
               RE_PRINTER.Items.Add( K_IMP_DEFAULT  + '('+ Printers[ PrinterIndex ] +')' );
               RE_PRINTER.Items.AddStrings( Printers );
          end;
     end;

     i := RE_PRINTER.Items.IndexOf(Reporte.FieldByName('RE_PRINTER').AsString);
     if i<0 then RE_PRINTER.ItemIndex := 0
     else RE_PRINTER.ItemIndex := i;
end;

procedure TFormas.OKClick(Sender: TObject);
begin
     if Reporte.State <> dsEdit then
       Reporte.Edit;
     if RE_PRINTER.ItemIndex = 0 then
        Reporte.FieldByName('RE_PRINTER').AsString := K_IMP_DEFAULT
     else Reporte.FieldByName('RE_PRINTER').AsString := RE_PRINTER.Text;
     inherited;
end;

procedure TFormas.RE_PRINTERChange(Sender: TObject);
begin
     inherited;
     Modo := dsEdit;
end;

procedure TFormas.AntesDeEscribirCambios;
begin
     if RE_PRINTER.ItemIndex = 0 then
        Reporte['RE_PRINTER'] := K_IMP_DEFAULT
     else Reporte['RE_PRINTER'] := RE_PRINTER.Text;
end;

{*************************************************}
{*************** GENERACION DE SQL ***************}
{*************************************************}
procedure TFormas.AgregaSQLCampoRep;
 var i: integer;
begin
     for i:=0 to LBOrden.Items.Count -1 do
         AgregaSQLOrdenes(TOrdenOpciones(LBOrden.Items.Objects[i]));
     for i:=0 to LBFiltros.Items.Count -1 do
         AgregaSQLFiltros(TFiltroOpciones(LBFiltros.Items.Objects[i]));
end;

procedure TFormas.AntesDeConstruyeSQL;
begin
     fDatosImpresion := GetDatosImpresion;
     ZQrReporte.PreparaReporte;

     with QRReporte do
          Init(SQLAgente,fDatosImpresion,LbParametros.Items.Count,FHayImagenes);
end;


function TFormas.GeneraReporte: Boolean;
begin
     Result := TRUE;
     QRReporte.GeneraForma( RE_NOMBRE.tEXT,
                            FTipoPantalla = tgPreview,
                            dsResultados.DataSet );
     ZQrReporte.DesPreparaReporte;
end;

function TFormas.GetTipoFormato: eTipoFormato;
begin
     Result := ZReportTools.GetTipoFormatoForma(eFormatoFormas( RE_PFILE.ItemIndex ) );
end;

function TFormas.GetDatosImpresion : TDatosImpresion;

 var sDirDefault : string;
     sError: string;

begin
     with Result do
     begin
          Tipo := GetTipoFormato;

          if (Tipo = tfImpresora) AND ( FImpresoraDefault = -1 ) then
             Error( tsImpresora, RE_PRINTER, 'La Computadora No tiene Impresoras Instaladas')
          else
          begin
               if RE_PRINTER.Items[ RE_PRINTER.ItemIndex ] = K_IMP_DEFAULT then
                  Impresora := FImpresoraDefault
               else with RE_PRINTER do
                    Impresora := ItemIndex - 1;
          end;

          Copias := RE_COPIAS.ValorEntero;
          PagInicial := StrToIntDef(EPaginaInicial.Text,1);
          PagFinal := StrToIntDef(EPaginaFinal.Text,9999);

          Archivo := Trim(RE_REPORTE.Text);
          Exportacion:= TransParamNomConfig( RE_ARCHIVO.Text, LbParametros.Count, SQLAgente.Parametros, TRUE, FALSE );
          if (StrLLeno(ExtractFilePath(Exportacion))) then
             ExportaUser := Exportacion
          else if(CambioArchivo) then
          begin
               ExportaUser := VACIO;
               CambioArchivo := False;
          end;
          if Archivo = '' then
             Error( tsImpresora, RE_REPORTE, 'La Forma no se Puede Generar si no se Indica una Plantilla' );

          sDirDefault := zReportTools.DirPlantilla;
          if ExtractFileExt( Archivo ) = '' then
             Archivo := Archivo + '.QR2';
          if ExtractFilePath( Archivo ) = '' then
             Archivo := sDirDefault + Archivo;
          if NOT FileExists( Archivo ) then
             {$ifdef HTTP_CONNECTION}
             if ( ClientRegistry.TipoConexion = conxDCOM ) then
             begin
             {$endif}
                  Error( tsImpresora, RE_REPORTE, 'La Plantilla "'+ Archivo +'" no Existe' );
             {$ifdef HTTP_CONNECTION}
             end;
             {$endif}

          {if ( Tipo <> tfImpresora ) then
          begin
               Exportacion := Trim(RE_ARCHIVO.Text);
               if Exportacion = '' then
                  Error( tsImpresora, RE_ARCHIVO, 'La Forma no se Puede Generar si no se Indica un Nombre de Archivo' );
               if ExtractFileExt( Exportacion ) = '' then
                  Exportacion := Exportacion + '.' +
                                 ObtieneElemento( lfExtFormato, Ord( Tipo ) );
               if ExtractFilePath( Exportacion ) = '' then
                  Exportacion := zReportTools.DirPlantilla + Exportacion;
               ExisteDirectorio(Exportacion,RE_ARCHIVO);
          end;}
          if ( FTipoPantalla = tgImpresora ) and
             ( Tipo <> tfImpresora) then
          begin
               if StrVacio(ExportaUser) then
               begin

                    Exportacion := GetNombreExportacion( Result , ObtieneElemento( lfExtFormato, Ord( Tipo ) ), sError );
                    if StrLleno(sError) then
                       Error( tsImpresora, RE_ARCHIVO, sError )
                    else
                    begin
                         if GetMyDocumentsDir <> VACIO then
                             Exportacion := VerificaDir( GetMyDocumentsDir ) + ExtractFileName(Exportacion);
                         if not DialogoExporta( Exportacion, Tipo ) then
                            raise Exception.Create('Forma cancelada por el usuario');
                         ExportaUser := Exportacion;
                    end;
               end
               else
               begin
                    if DirectoryExists( ExtractFilePath( ExportaUser ) ) then
                    begin
                         if ( StrLleno ( ExtractFileName( ExportaUser ) )  ) then
                            Exportacion := ExportaUser
                         else
                             raise Exception.Create( 'No se especific� un nombre de archivo' );
                    end
                    else
                        raise Exception.Create( 'El Directorio No Existe' );
               end;
          end;
     end;
end;

procedure TFormas.RE_PFILEChange(Sender: TObject);
 var fTipoFormato : eTipoFormato;
begin
     inherited;
     fTipoFormato := GetTipoFormato;

     EnabledImpresora( fTipoFormato = tfImpresora );
     EnabledArchivo( ( fTipoFormato <> tfImpresora ), fTipoFormato );
     EnabledPlantilla( NOT( fTipoFormato in [ tfASCIIFijo,
                                              tfASCIIDel,
                                              tfCSV ] ) );
     EnabledPrintSetting( fTipoFormato );
end;

procedure TFormas.EnabledPrintSetting( const eTipo : eTipoFormato );
begin
     bExportPreferencias.Enabled := ( eTipo in [ tfHTML,tfTXT,tfXLS,tfRTF,tfWMF,
                                                 tfPDF,tfBMP,tfTIF,tfPNG,tfJPEG,
                                                 tfEMF,tfSVG,tfHTM,tfWB1,
                                                 tfWK2,tfDIF,tfSLK ] );
end;

procedure TFormas.EnabledPlantilla( const lEnabled : Boolean );
begin
     lbPlantilla.Enabled := lEnabled;
     RE_REPORTE.Enabled := lEnabled;
     bPlantilla.Enabled := lEnabled;
     bDisenador.Enabled := lEnabled;
end;

procedure TFormas.EnabledImpresora( const lEnabled : Boolean );
begin
     lbImpresora.Enabled := lEnabled;
     RE_PRINTER.Enabled := lEnabled;
     lbCopias.Enabled := lEnabled;
     RE_COPIAS.Enabled := lEnabled;
     UPDown.Enabled := lEnabled;
     EPaginas.Enabled := lEnabled;
     EPaginasAl.Enabled := lEnabled;
     EPaginaInicial.Enabled := lEnabled;
     EPaginaFinal.Enabled := lEnabled;
end;

procedure TFormas.EnabledArchivo( const lEnabled : Boolean;
                                  const eTipo : eTipoFormato );
begin
     lbNombreArchivo.Enabled := lEnabled;
     RE_ARCHIVO.Enabled := lEnabled;
     BGuardaArchivo.Enabled := lEnabled;
     bEvaluaArchivo.Enabled := lEnabled;
     if lEnabled then
     begin
          if StrVacio(RE_ARCHIVO.Text) then
             RE_ARCHIVO.Text := Reporte['RE_TITULO'] + '.' +
                                ObtieneElemento( lfExtFormato, Ord( eTipo ) );
     end;
end;

function TFormas.GetFirstPestana: TTabSheet;
begin
     Result := inherited GetFirstPestana;
     if tsFiltros.TabVisible then
        Result := tsFiltros;
end;

procedure TFormas.BGuardaArchivoClick(Sender: TObject);
 var sTexto : string;
begin
     inherited;
     //sTexto := DialogoAscii( RE_ARCHIVO.Text );
     sTexto := DialogoFormas( RE_ARCHIVO.Text, eFormatoFormas(RE_PFILE.ItemIndex) );
     if sTexto <> RE_ARCHIVO.Text then
     begin
          Modo := dsEdit;
          Reporte.Edit;
          Reporte.FieldByName( 'RE_ARCHIVO').AsString := sTexto;
     end;
end;

procedure TFormas.FormShow(Sender: TObject);
begin
     inherited;
     tsSuscripciones.TabVisible:= FALSE;
     bbSuscripcion.Visible:= FALSE;
     EPaginaInicial.Text := '1';
     EPaginaFinal.Text := '9999';
     RE_PFILEChange( Nil );
     NombreArchivoFormula.Text:=VACIO;
end;

procedure TFormas.bExportPreferenciasClick(Sender: TObject);
begin
     inherited;
     DExportEngine.Preferencias( GetTipoFormato );
end;

procedure TFormas.RE_ARCHIVOChange(Sender: TObject);
begin
     inherited;
     CambioArchivo := True;
end;

procedure TFormas.bDisenadorClick(Sender: TObject);
begin
     inherited;
     AbreDisenador( RE_REPORTE.Text ); 
end;

procedure TFormas.bEvaluaArchivoClick(Sender: TObject);
var
   sNomArchivoFormula: String;
begin
     inherited;
     sNomArchivoFormula:= GeneraNombreArchivo(RE_ARCHIVO.Text);

     if ( StrLleno( ExtractFilePath( sNomArchivoFormula ) )  and
         not ( DirectoryExists( ExtractFilePath( sNomArchivoFormula ) ) ) ) then
           ZInformation( '� Advertencia !', 'La ruta especificada no existe en �sta computadora', 0 )
     else if StrVacio( ExtractFileName( sNomArchivoFormula ) ) then
           ZInformation( '� Advertencia !', 'No se ha especificado un nombre de archivo', 0 );

     NombreArchivoFormula.Text:= sNomArchivoFormula;

end;

end.
