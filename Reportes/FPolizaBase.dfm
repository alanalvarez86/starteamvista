inherited PolizaBase: TPolizaBase
  Left = 462
  Top = 187
  Caption = 'PolizaBase'
  ClientHeight = 419
  ClientWidth = 612
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 389
    Width = 612
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 451
    end
    inherited Cancelar: TBitBtn
      Left = 529
    end
  end
  inherited Panel1: TPanel
    Width = 612
    TabOrder = 0
    inherited BGuardarComo: TSpeedButton
      Visible = False
    end
    inherited BImprimir: TSpeedButton
      OnClick = BImprimirClick
    end
  end
  inherited PageControl: TPageControl
    Width = 612
    Height = 359
    ActivePage = tsCuentas
    TabOrder = 1
    inherited tsGenerales: TTabSheet
      inherited GBParametrosEncabezado: TGroupBox
        Left = 59
        Top = 77
        Height = 177
        inherited Label3: TLabel
          Left = 61
          Width = 73
          Caption = 'Tabla Principal:'
        end
        object Label5: TLabel [5]
          Left = 16
          Top = 125
          Width = 118
          Height = 13
          Alignment = taRightJustify
          Caption = 'Guardar en Clasificaci�n:'
        end
        inherited RE_CANDADOlbl: TLabel
          Top = 149
        end
        inherited RE_CLASIFI: TComboBox
          Top = 121
          Width = 147
          TabOrder = 3
        end
        inherited RE_CANDADO: TDBCheckBox
          Top = 148
          Width = 147
          TabOrder = 4
        end
        object RE_ENTIDAD: TZetaKeyCombo
          Left = 140
          Top = 97
          Width = 147
          Height = 21
          Style = csDropDownList
          DropDownCount = 10
          ItemHeight = 13
          TabOrder = 2
          OnChange = RE_ENTIDADChange
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
      end
    end
    object tsCuentas: TTabSheet [1]
      Caption = 'Cuentas'
      ImageIndex = 3
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 604
        Height = 30
        Align = alTop
        TabOrder = 0
        object ModificarBtn: TSpeedButton
          Left = 50
          Top = 3
          Width = 24
          Height = 24
          Hint = 'Modificar Cuenta (Ctrl+Ins)'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = ModificarBtnClick
        end
        object AgregarBtn: TSpeedButton
          Left = 2
          Top = 3
          Width = 24
          Height = 24
          Hint = 'Agregar Cuenta (Shift+Ins)'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarBtnClick
        end
        object BorrarBtn: TSpeedButton
          Left = 26
          Top = 3
          Width = 24
          Height = 24
          Hint = 'Borrar Cuenta (Shift+Del)'
          Flat = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarBtnClick
        end
        object Label6: TLabel
          Left = 598
          Top = 1
          Width = 5
          Height = 28
          Align = alRight
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
      object Grid: TZetaDBGrid
        Left = 0
        Top = 30
        Width = 604
        Height = 301
        Align = alClient
        DataSource = dsCuentas
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnColExit = GridColExit
        OnDrawColumnCell = GridDrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'CR_TITULO'
            Title.Alignment = taCenter
            Title.Caption = 'Cuenta'
            Width = 170
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_FORMULA'
            Title.Alignment = taCenter
            Title.Caption = 'F�rmula'
            Width = 195
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_OPER'
            Title.Alignment = taCenter
            Title.Caption = 'Tipo Cuenta'
            Width = 62
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_MASCARA'
            Title.Alignment = taCenter
            Title.Caption = 'Comentario'
            Width = 59
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_DESCRIP'
            Title.Alignment = taCenter
            Title.Caption = 'Texto'
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_ANCHO'
            Title.Alignment = taCenter
            Title.Caption = 'N�mero'
            Width = 42
            Visible = True
          end>
      end
      object CBTipoCuenta: TComboBox
        Left = 368
        Top = 176
        Width = 89
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        Visible = False
        OnChange = CBTipoCuentaChange
        Items.Strings = (
          'Cargo'
          'Abono')
      end
    end
    inherited tsOrden: TTabSheet
      Caption = 'Grupos'
      inherited BArribaOrden: TZetaSmartListsButton
        Left = 261
        Top = 34
      end
      inherited BAbajoOrden: TZetaSmartListsButton
        Left = 261
        Top = 59
      end
      inherited GroupBox2: TGroupBox
        Left = 57
        Top = 30
        Caption = 'Agrupar Por'
      end
      inherited BAgregaOrden: TBitBtn
        Left = 57
        Top = 276
      end
      inherited BBorraOrden: TBitBtn
        Left = 157
        Top = 276
      end
      inherited GroupBoxOrden: TGroupBox
        Left = 292
        Top = 30
        Width = 254
        inherited bFormulaOrden: TBitBtn
          Visible = False
        end
      end
    end
    inherited tsFiltros: TTabSheet
      inherited PageControlFiltros: TPageControl
        Left = 57
        Top = 38
        ActivePage = tsFiltrosEspeciales
      end
    end
  end
  inherited DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 400
    Top = 32
  end
  inherited SmartListFiltros: TZetaSmartLists
    Left = 432
  end
  inherited SmartListOrden: TZetaSmartLists
    Left = 400
  end
  object dsCuentas: TDataSource
    OnStateChange = dsCuentasStateChange
    Left = 432
    Top = 32
  end
end
