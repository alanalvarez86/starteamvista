unit ZQRReporteListado;

interface   

uses
  Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, ComCtrls, Mask,
  //(@am):FormatMaskText no compila en XE5 si no se invoca a esta unidad.
  {$IFDEF TRESS_DELPHIXE5_UP}
  MaskUtils,thsdRuntimeLoader, thsdRuntimeEditor, qrdBaseCtrls, qrdQuickrep,
  {$ENDIF}
  Quickrpt,
  Qrdctrls,
  QrExport,
  Qrprntr,
  QRDLDR,
  QRDesign,
  Qrctrls,
  StdCtrls,
  {$ifdef QR362}
  qrpBaseCtrls,
  {$endif}
  {$ifdef VER150}
  thsdRuntimeLoader, thsdRuntimeEditor, qrdBaseCtrls,
  qrdQuickrep,
  MaskUtils,
  {$ENDIF}
  ZReportTools,
  ZReportToolsConsts,
  ZetaCommonLists,
  ZetaCommonClasses,
  ZAgenteSQLClient,
  ZQrReporte, imageenview, ieview, dbimageen, dxGDIPlusClasses;

type
  TQRReporteListado = class(TQrReporte)
    procedure FQReportPreview(Sender: TObject);
  private
    FEsPreview : Boolean;
    FBandaTotales : TList;
    FBandaTitulos : TList;
    FCentraPagina,FAnchoGrupos : integer;
    FSummary, EsVertical, EsSoloTotales,SobreTotales : Boolean;
    FAjustaColumnHeader:Boolean;
    function AltoTitulo(oLetra: TFont; const sTexto: string): integer;
    procedure CreaColumnas( oCampos, oGrupos : TStrings);
    procedure CreaRenglones(oCampos : TStrings );
    procedure CreaListado( oCampos, oGrupos: TStrings );
    procedure OnPrintExprDate(Sender: TObject; var Value: String);
    procedure OnPrintExprString(Sender: TObject; var Value: String);
    procedure AgregaQRDBField(oBanda: TQRCustomBand; oCampo: TCampoOpciones );
    procedure AgregaQRExpr(oBanda: TQRCustomBand; oCampo: TCampoOpciones; const lTotaliza, lGranTotal : Boolean );
    procedure AgregaQRLabel( oBanda: TQRCustomBand; oCampo: TCampoOpciones; const lValidaTitulo : Boolean = FALSE );
    procedure AgregaQRConstante(oBanda: TQRCustomBand;oCampo: TCampoOpciones);

    function AnchoTitulo(oLetra: TFont; sTexto: string): integer;
    function AnchoCampo(oLetra: TFont; iAncho: integer): integer;
    function GetJustificacion(eTipo: eTipoGlobal): TAlignment;
    procedure SetAnchosCampo(oCampo: TCampoListado; const iLeft: integer);

    procedure CreaBandaHijo(oBanda: TQrDesignBand);

    procedure CreaGrupos( oGrupos : TStrings );
    procedure CreaGrupo( oCampo: TGrupoOpciones; var oBandaCabeza: TQRDesignGroup; var oBandaPie: TQRDesignBand; const iPos: integer; const lEspecialTotal : Boolean);
    procedure AgregaColumnasMaster(oBanda: TQRCustomBand; oLista: TStrings; const Pos, Top: integer);
    procedure AgregaColumnas( oBanda: TQRCustomBand; oLista: TStrings; const Pos, Top: integer; const lEspecialTotal: Boolean; const lUltimoGrupo : Boolean = FALSE; const lDatosPie : Boolean = FALSE );
    procedure AgregaTotales(oCampo: TCampoOpciones);
    procedure SetAltoTotales(const iAlto: integer);
    procedure CreaBandaGrupos( var oBandaCabeza: TQRDesignGroup;
                               var oBandaPie: TQRDesignBand;
                               const i: integer);
    function GetAjustaColumnHeader( oCampos,oGrupos : TStrings ): Boolean;
    procedure OnPrintExprDateGrid(Sender: TObject; var Value: String);
  protected
    procedure Termina;override;
  public
    procedure Init( oAgente : TSQLAgenteClient;
                    oDatosImpresion : TDatosImpresion;
                    iCountParametros: Integer;
                    var lHayImagenes : Boolean );override;
    procedure InitGrid( oDatosImpresion : TDatosImpresion;
                        const sValor1, sValor2, sCaption : string;
                        const eValor1, eValor2 : TipoEstado );
    procedure InitExportacion( oDatosImpresion : TDatosImpresion;
                                             const sValor1, sValor2, sCaption : string;
                                             const eValor1, eValor2 : TipoEstado );

    function GeneraListado( const sNombre: string;
                            const EsPreview, lEsVertical, lEsSoloTotales,lSobreTotales : Boolean;
                            oDataSet: TDataSet;
                            oCampos, oGrupos : TStrings ): Boolean;
  end;

{$ifdef XMLREPORT}
threadvar
{$else}
var
{$endif}
  { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
  QRReporteListado: TQRReporteListado = NIL;

procedure PreparaReporte;
procedure DesPreparaReporte;

implementation
uses ZetaWinAPITools,
     ZetaCommonTools,
     ZetaTipoEntidad,
     ZFuncsCliente,
     ZDiccionTools,
     DCliente;

{$R *.DFM}

const K_LEFT_INICIAL = 2;
      K_TOP_INICIAL = 1;
      K_ESPACIO = 5;
      KRenglon = 4;

procedure PreparaReporte;
begin
     if QRReporteListado = NIL then
        QRReporteListado := TQRReporteListado.Create( {$ifdef TRESS_REPORTES}nil{$else}{$ifdef XMLREPORT}nil{$else}Application{$endif}{$ENDIF} );
end;

procedure DesPreparaReporte;
begin
     if ( QRReporteListado <> NIL ) then
          QRReporteListado.Free;
     QRReporteListado := NIL;
end;

function TQRReporteListado.GeneraListado( const sNombre : string;
                                          const EsPreview, lEsVertical, lEsSoloTotales,lSobreTotales : Boolean;
                                          oDataSet : TDataSet;
                                          oCampos, oGrupos : TStrings ):Boolean;
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crDefault;

      {***(@am): Anteriormente se deshabilitaba y habilitaba el dataset desde que se invoca GeneraReporte. Lo cual
                no nos permitia mostrar imagene en los gafettes pues el nuevo control (ImageEn) necesita la notificacion
                de que hubo un cambio.***}
     oDataSet.DisableControls;
     {***************************}

     fNombre := sNombre;
     FEsPreview := EsPreview;
     with fQReport do
     begin
          Description.Text := fNombre;
          ReportTitle := fNombre;
          Dataset := oDataset;
     end;

     Result := TRUE;
     FSummary := FALSE;
     FAnchoGrupos := 0;
     FCentraPagina := 0;
     if ( QRDataSource <> nil ) then
        QRDataSource.DataSet := oDataSet;

     EsVertical := lEsVertical;
     EsSoloTotales := lEsSoloTotales;
     SobreTotales := lSobreTotales;

     if SobreTotales then RegistraFuncionesTotales;

     if (FAgente <> NIL) then {El Agente es NIL si es Impresion de Grid}
        GetExpresion;

     ParametrosImpresion;

     CreaListado(oCampos,oGrupos);

       {***(@am): Anteriormente se deshabilitaba y habilitaba el dataset desde que se invoca GeneraReporte. Lo cual
                no nos permitia mostrar imagene en los gafettes pues el nuevo control (ImageEn) necesita la notificacion
                de que hubo un cambio.***}
     oDataset.EnableControls;
     {***************************}

     if FEsPreview then Preview
     else Imprime;
     Termina;

     Screen.Cursor := oCursor;
end;

procedure TQRReporteListado.AgregaColumnas( oBanda: TQRCustomBand; oLista: TStrings; const Pos, Top: integer; const lEspecialTotal: Boolean; const lUltimoGrupo : Boolean = FALSE; const lDatosPie : Boolean = FALSE );
var i, iLeft, iTop, iAltoBanda : integer;
     oCampo : TCampoOpciones;
begin
     iLeft := Pos;
     iTop := Top;
     iAltoBanda := AltoTitulo( oBanda.Font, 'W' );
     for i:= 0 to oLista.Count - 1 do
     begin
          oCampo := TCampoOpciones( oLista.Objects[ i ] );
          oCampo.AnchoTitulo := 0;
          oCampo.AnchoCampo := 0;

          if oCampo.Calculado = K_RENGLON_NUEVO then
          begin
               iLeft := Pos;
               iTop := K_TOP_INICIAL + iTop + AltoTitulo( oBanda.Font, 'W' );
          end
          else
          begin
               oCampo.Top := iTop;
               oCampo.Alto := AltoTitulo( fQReport.Font, oCampo.Titulo );
               if lEspecialTotal then
               begin
                    oCampo.AnchoTitulo := AnchoCampo( oBanda.Font, oCampo.Ancho );
                    oCampo.AnchoCampo := oCampo.AnchoTitulo;
                    oCampo.LeftTitulo := iLeft;
                    oCampo.LeftCampo := iLeft;

                    oCampo.Justificacion := GetJustificacion( oCampo.TipoImp );
                    if NOT EsSoloTotales OR lDatosPie then
                       AgregaQRDBField( oBanda, oCampo );
                    iLeft := iLeft + oCampo.AnchoCampo + K_ESPACIO;
                    if lUltimoGrupo then FAnchoGrupos := iLeft;
                    oBanda.Height := oCampo.Top+iAltoBanda;
               end
               else
               begin
                    oCampo.LeftTitulo := iLeft;
                    oCampo.AnchoTitulo := AnchoTitulo( fQReport.Font, oCampo.Titulo );

                    iLeft := iLeft + oCampo.AnchoTitulo + K_ESPACIO;
                    oCampo.LeftCampo := iLeft;
                    oCampo.AnchoCampo := AnchoCampo( fQReport.Font, oCampo.Ancho );

                    oCampo.Justificacion := taLeftJustify;
                    AgregaQRLabel( oBanda, oCampo );
                    oCampo.Justificacion := GetJustificacion( oCampo.TipoImp );
                    AgregaQRDBField( oBanda, oCampo );
                    iLeft := iLeft + oCampo.AnchoCampo + K_ESPACIO;
                    oBanda.Height := oCampo.Top+iAltoBanda;
               end;
          end;
     end;//for
end;

procedure TQRReporteListado.CreaGrupo( oCampo: TGrupoOpciones;
                                var oBandaCabeza: TQRDesignGroup;
                                var oBandaPie: TQRDesignBand;
                                const iPos: integer;
                                const lEspecialTotal : Boolean);
begin
     oBandaCabeza.Expression := oCampo.NombreCampo;
     oBandaCabeza.ForceNewPage := oCampo.SaltoPagina;

     {$ifdef TRESS}
     if FAgente.Entidad = enListadoNomina then
        oBandaCabeza.PrintExpression := oCampo.NombreCampo + '<>''''';
     {$endif}

     if ( oCampo.Encabezado ) then
     begin
          if ( oCampo.ListaEncabezado.Count > 0 )then
          begin
               if NOT lEspecialTotal AND oCampo.Master then
                  AgregaColumnasMaster( oBandaCabeza, oCampo.ListaEncabezado, Round( oBandaCabeza.Width/3), K_TOP_INICIAL * 2 )
               else
               begin
                    if lEspecialTotal then AgregaColumnas( oBandaPie, oCampo.ListaEncabezado, iPos + FCentraPagina, K_TOP_INICIAL * 2, lEspecialTotal, oCampo.Ultimo )
                    else AgregaColumnas( oBandaCabeza, oCampo.ListaEncabezado, iPos, K_TOP_INICIAL * 2, lEspecialTotal )
               end;
          end;
     end
     else
     begin
          oBandaCabeza.Height := 0;
          oBandaCabeza.Visible := FALSE;
     end;

     if ( oCampo.PieGrupo ) then
        if ( oCampo.ListaPie.Count > 0 ) then
        with oBandaPie do
        begin
             if HasChild then
             begin
                  CreaBandaHijo( TQrDesignBand( ChildBand ) );
                  ChildBand.ChildBand.Height := ChildBand.Height;
                  ChildBand.ChildBand.Frame.DrawTop := ChildBand.Frame.DrawTop;
                  ChildBand.ChildBand.Color := ChildBand.Color;

                  ChildBand.Color := oBandaPie.Color;
                  ChildBand.Frame.DrawTop := Frame.DrawTop;
                  Frame.DrawBottom := FALSE;
                  Frame.DrawTop := FALSE;
                  AgregaColumnas( ChildBand.ChildBand, oCampo.ListaPie, K_LEFT_INICIAL, K_TOP_INICIAL, lEspecialTotal );
             end
             else
             begin
                  CreaBandaHijo( oBandaPie );
                  oBandaPie.ChildBand.Height := 5;
                  oBandaPie.ChildBand.Color := oBandaPie.Color;
                  AgregaColumnas( oBandaPie.ChildBand, oCampo.ListaPie, K_LEFT_INICIAL, K_TOP_INICIAL, lEspecialTotal, FALSE, TRUE );
             end;
     end;

     if NOT oCampo.Totalizar then
     begin
          oBandaPie.Height := 0;
          oBandaPie.Visible := FALSE;
     end;
end;

procedure TQRReporteListado.AgregaColumnasMaster( oBanda: TQRCustomBand; oLista: TStrings; const Pos, Top: integer);
 var i, iLeft, iTop, iAltoBanda : integer;
     oCampo : TCampoOpciones;
     lPrimero : Boolean;
begin
     iLeft := Pos;
     iTop := Top;
     lPrimero := TRUE;
     iAltoBanda := AltoTitulo( oBanda.Font, 'W' );
     for i:= 0 to oLista.Count - 1 do
     begin
          oCampo := TCampoOpciones( oLista.Objects[ i ] );
          oCampo.AnchoTitulo := 0;
          oCampo.AnchoCampo := 0;

          if oCampo.Calculado = K_RENGLON_NUEVO then
          begin
               iLeft := Pos;
               iTop := K_TOP_INICIAL + iTop + iAltoBanda;
               lPrimero := TRUE;
          end
          else
          begin
               oCampo.Top := iTop;
               oCampo.Alto := iAltoBanda;
               if lPrimero then
               begin
                    oCampo.LeftTitulo := 0;
                    oCampo.AnchoTitulo := iLeft;
                    lPrimero := FALSE;
               end
               else
               begin
                    oCampo.LeftTitulo := iLeft;
                    oCampo.AnchoTitulo := AnchoTitulo( fQReport.Font, oCampo.Titulo );
               end;
               iLeft := oCampo.LeftTitulo + oCampo.AnchoTitulo + K_ESPACIO;
               oCampo.LeftCampo := iLeft;
               oCampo.AnchoCampo := AnchoCampo( fQReport.Font, oCampo.Ancho );

               oCampo.Justificacion := taRightJustify;
               AgregaQrLabel( oBanda, oCampo );
               AgregaQRDBField( oBanda, oCampo );
               iLeft := iLeft + oCampo.AnchoCampo + K_ESPACIO;

          end;
     end;//for
     oBanda.Height := iTop+iAltoBanda;
end;

procedure TQRReporteListado.CreaBandaGrupos( var oBandaCabeza: TQRDesignGroup; var oBandaPie : TQRDesignBand; const i : integer );
begin
     oBandaCabeza := TQRDesignGroup.Create( self );
     oBandaCabeza.Parent := fQReport;
     oBandaCabeza.ParentReport := fQReport;
     oBandaCabeza.BandType := rbGroupHeader;
     oBandaCabeza.Master := fQReport.DataSet;
     oBandaCabeza.Height := K_ESPACIO;
     oBandaCabeza.BandNum := i;

     oBandaPie := TQRDesignBand.Create( self );
     oBandaPie.Parent := fQReport;
     oBandaPie.ParentReport:= fQReport;
     oBandaPie.BandType := rbGroupFooter;
     oBandaPie.Height := K_ESPACIO;
     oBandaPie.BandNum := i;

     with fQReport.Bands do
     begin
          if HasColumnHeader then oBandaCabeza.Font := ColumnHeaderBand.Font
          else if HasTitle then oBandaCabeza.Font := TitleBand.Font;
          if HasDetail then oBandaPie.Font := DetailBand.Font;
      end;

      oBandaCabeza.FooterBand := oBandaPie;
end;

procedure TQRReporteListado.CreaGrupos( oGrupos : TStrings );
 var i : integer;
     oBandaCabeza : TQRDesignGroup;
     oBandaPie : TQRDesignBand;
     oGrupo : TGrupoOpciones;
     lEspecialTotal : Boolean;
begin
     //GRUPO NIVEL EMPRESA
     if oGrupos <> NIL then
     begin
          oGrupo := TGrupoOpciones( oGrupos.Objects[ 0 ] );
          if ( oGrupo.Encabezado ) then
             with fQReport.Bands do
                  if HasTitle then
                  begin
                       if ( oGrupo.ListaEncabezado.Count > 0 )then
                       begin
                            CreaBandaHijo( TQrDesignBand( TitleBand ) );
                            AgregaColumnas( TitleBand.ChildBand, oGrupo.ListaEncabezado, K_LEFT_INICIAL, K_TOP_INICIAL, FALSE )
                       end;
                  end;

          with fQReport.Bands do
          begin
               if oGrupo.Totalizar then
               begin
                    HasSummary := TRUE;
                    SummaryBand.Enabled := TRUE;
               end
               else HasSummary := FALSE;
               FSummary := HasSummary;
          end;

          with fQReport.Bands do
               if ( oGrupo.PieGrupo ) AND
                  //( HasSummary ) AND
                  ( oGrupo.ListaPie.Count > 0 ) then
               begin
                    HasSummary :=  TRUE;
                    if NOT FSummary then
                       SummaryBand.Height := 0;
                    CreaBandaHijo( TQrDesignBand( SummaryBand ) );
                    SummaryBand.ChildBand.Height := K_TOP_INICIAL;
                    AgregaColumnas( SummaryBand.ChildBand, oGrupo.ListaPie, K_LEFT_INICIAL, K_TOP_INICIAL, FALSE );
               end;

          lEspecialTotal := FALSE;
          for i := 1 to oGrupos.Count - 1 do
          begin
               oGrupo := TGrupoOpciones( oGrupos.Objects[ i ] );
               if StrLleno(oGrupo.Formula) then
               begin
                    CreaBandaGrupos( oBandaCabeza, oBandaPie, i );

                    if oGrupo.Totalizar AND oGrupo.PieGrupo then
                       fBandaTotales.Add(oBandaPie);

                    if ( i = oGrupos.Count -1 ) then lEspecialTotal := EsSoloTotales AND NOT EsVertical;

                    if NOT lEspecialTotal then
                    begin
                         CreaBandaHijo( oBandaPie );
                         oBandaPie.ChildBand.Font := oBandaCabeza.Font;
                         oBandaPie.ChildBand.Height := KRenglon;
                    end;
                    if (i = 1) AND NOT EsSoloTotales then
                    begin
                         //oBandaCabeza.Frame.DrawTop := TRUE;
                         if oBandaPie.HasChild then
                            oBandaPie.ChildBand.Frame.DrawTop := TRUE;
                    end;

                    oGrupo.Ultimo := i = oGrupos.Count-1;
                    CreaGrupo( oGrupo, oBandaCabeza, oBandaPie, (i-1) * 24, lEspecialTotal{,FALSE} );

                    {$ifdef ADUANAS}
                    if ( NOT EsVertical ) AND ( oGrupo.SaltoPagina or oGrupo.Titulos) then
                    {$else}
                    if ( NOT EsVertical ) AND oGrupo.SaltoPagina then
                    {$endif}
                    begin
                         FAjustaColumnHeader := FALSE;
                         CreaBandaHijo( TQrDesignBand( oBandaCabeza ) );
                         if oBandaPie.HasChild then
                            oBandaPie.ChildBand.Height := 2;
                         FBandaTitulos.Add( oBandaCabeza.ChildBand );
                    end;
               end;
          end; //for
     end;
end;

function TQRReporteListado.AltoTitulo( oLetra : TFont;
                                const sTexto : string ) : integer;
begin
     Result := fQReport.TextHeight( oLetra, sTexto );
end;

function TQRReporteListado.AnchoTitulo( oLetra  : TFont; sTexto : string ) : integer;
  function GetTextSize( oLetra : TFont; sTexto : String; iAncho : Integer ) : TSize;
   var  ReferenceDC, oHandle : THandle;
  begin
       ReferenceDC := CreateCompatibleDC( 0 );
       oHandle := SelectObject(ReferenceDC, oLetra.Handle );
       if NOT GetTextExtentPoint32( ReferenceDC,
                                    PChar( sTexto ),
                                    iAncho,
                                    Result ) then
       begin
            Result.Cx := 0;
            Result.Cy := 0;
       end;
       ReferenceDC := SelectObject(ReferenceDC,oHandle);
       DeleteDC( ReferenceDC );
  end;
begin
     if StrLleno( sTexto ) AND ( sTexto <> SINTITULO ) then
        Result := GetTextSize( oLetra, sTexto, Length( sTexto ) ).Cx + 1
     else Result := 0;
end;

function TQRReporteListado.AnchoCampo( oLetra : TFont; iAncho : integer ) : integer;
 var sTexto : string;
     i : integer;
begin                 
     for i := 0 to iAncho do sTexto := sTexto + 'H';
     Result := AnchoTitulo( oLetra, sTexto );
end;

procedure TQRReporteListado.SetAnchosCampo( oCampo : TCampoListado;
                                     const iLeft : integer );
begin
     with fQReport.Bands do
     begin
          if HasColumnHeader then
             oCampo.AnchoTitulo := AnchoTitulo( ColumnHeaderBand.Font, oCampo.Titulo )
          else oCampo.AnchoTitulo := AnchoTitulo( fQReport.Font, oCampo.Titulo );

          if HasDetail then
             oCampo.AnchoCampo := AnchoCampo( DetailBand.Font, oCampo.Ancho )
          else oCampo.AnchoCampo := AnchoCampo( fQReport.Font, oCampo.Ancho );
     end;

     if oCampo.TipoImp = tgTexto then
     begin
          if oCampo.AnchoTitulo > oCampo.AnchoCampo then
          begin
               oCampo.LeftTitulo := iLeft;
               oCampo.LeftCampo := iLeft + Round(( oCampo.AnchoTitulo - oCampo.AnchoCampo )/2 )
          end
          else
          begin
               oCampo.AnchoTitulo := iMax( oCampo.AnchoTitulo, oCampo.AnchoCampo );
               oCampo.AnchoCampo := oCampo.AnchoTitulo;
               oCampo.LeftTitulo := iLeft;
               oCampo.LeftCampo := iLeft;
          end;
     end
     else
     begin
          oCampo.AnchoTitulo := iMax( oCampo.AnchoTitulo, oCampo.AnchoCampo );
          oCampo.AnchoCampo := oCampo.AnchoTitulo;
          oCampo.LeftTitulo := iLeft;
          oCampo.LeftCampo := iLeft;
     end;
end;

function TQRReporteListado.GetJustificacion( eTipo : eTipoGlobal ) : TAlignment;
begin
     case eTipo of
          tgFloat, tgNumero : Result := taRightJustify;
          tgFecha :
          begin
               if NOT FEsPreview AND (FDatosImpresion.Tipo = tfTXT) then
                  Result := taRightJustify
               else Result := taCenter;
          end
          else Result := taLeftJustify;
     end;
end;

procedure TQRReporteListado.AgregaQRExpr( oBanda: TQRCustomBand;
                                          oCampo: TCampoOpciones;
                                          Const lTotaliza, lGranTotal : Boolean );

 function StrTransTotal(const sFormula: string ) : string;
  const K_MIN = 'MIN(' ;
        K_MAX = 'MAX(' ;
        K_MINIMO = 'zMINIMO(' ;
        K_MAXIMO = 'zMAXIMO(' ;        
 begin
      Result := sFormula;
      Result := StrTransAll( Result, K_MIN, K_MINIMO );
      Result := StrTransAll( Result, K_MAX, K_MAXIMO );
 end;

 var oExpr : TQRDesignExpr;

begin
     oExpr := TQRDesignExpr( oBanda.AddPrintable( TQRDesignExpr ) );
     with oExpr do
     begin
          AutoSize := FALSE;
          Left := oCampo.LeftCampo;
          Top := oCampo.Top;
          Width := oCampo.AnchoCampo;
          GroupID := oCampo.Ancho;
          Transparent := TRUE;
          Mask := oCampo.Mascara;
          Tag := -1;

          if StrVacio( Mask ) then Mask := GetMascaraDefault(oCampo.TipoImp);
          Alignment := GetJustificacion( oCampo.TipoImp );

          if( ( ZFuncsCliente.ImprimeGrid ) and ( oCampo.TipoImp = tgFecha ) ) then
             oExpr.OnPrint := OnPrintExprDateGrid;

          if lTotaliza then
          begin
               if EsSoloTotales then
               begin
                    Expression := 'TOTAL_'+IntToStr(oCampo.PosAgente);
                    if lGranTotal then
                    begin
                         case oCampo.OpImp of
                              ocCuantos :
                              begin
                                   {$ifndef CAROLINA}
                                   if oCampo.TipoImp = tgBooleano then
                                      Expression := 'SUM(IF( '+ Expression +'= "S",1,0))'
                                   else {$ENDIF}if FBandaTotales.Count > 0 then
                                        Expression := 'SUM('+Expression+')';
                              end;
                              ocSuma : Expression := 'SUM('+Expression+')';
                              ocPromedio : Expression := 'AVERAGE('+Expression+')';
                              ocMin : Expression := 'MIN('+Expression+')';
                              ocMax : Expression := 'MAX('+Expression+')';
                              ocSobreTotales :
                              begin
                                   Expression := StrTransAll( StrTransAll(oCampo.Formula,'SUMA_RESULT','RESULT'), CR_LF, ' ' );
                                   Expression := StrTransTotal(Expression);
                              end;
                         end;
                    end;
               end
               else
               begin
                   case oCampo.OpImp of
                        ocCuantos :
                        begin
                             {$IFNDEF CAROLINA}
                             if oCampo.TipoImp = tgBooleano then
                                Expression := 'SUM(IF( '+ oCampo.NombreCampo + '= "S" ,1,0))'
                             else {$ENDIF}Expression := 'COUNT()';
                        end;
                        ocSuma : Expression := 'SUM('+oCampo.NombreCampo+')';
                        ocPromedio : Expression := 'AVERAGE('+oCampo.NombreCampo+')';
                        ocMin : Expression := 'MIN('+oCampo.NombreCampo+')';
                        ocMax : Expression := 'MAX('+oCampo.NombreCampo+')';
                        ocSobreTotales :
                        begin
                             Expression := StrTransAll( oCampo.Formula, CR_LF, ' ');
                             Expression := StrTransTotal(Expression);
                        end;
                        ocRepite : Expression := oCampo.NombreCampo
                        else Expression := '';
                   end;
               end;
               if SobreTotales AND (Expression <> '' ) then
                  Expression := 'SAVE_RES('+IntToStr( oCampo.PosAgente +1 )  + ',' + Expression+')';
          end
          else Expression := oCampo.NombreCampo;

          ResetAfterPrint := TRUE;
     end;
end;
procedure TQRReporteListado.OnPrintExprDate(Sender: TObject; var Value: String);
  //var Fecha : TDateTime;
begin
     with TQRDBText(Sender) do
          Value := GetFormatFecha( FQReport.DataSet.FieldByName(DataField).AsDateTime,
                                  Mask );
     {
     Fecha := FQReport.DataSet.FieldByName(TQRDBText(Sender).DataField).AsDateTime;
     if Fecha <= 0 then Value := ''
     else Value := FormatDateTime( TQRDBText(Sender).Mask, Fecha );}
end;

//Evento para exportar Grid con fecha vacia
procedure TQRReporteListado.OnPrintExprDateGrid(Sender: TObject; var Value: String);
  //var Fecha : TDateTime;
begin
     Value := GetFormatFecha( FQReport.DataSet.FieldByName(TQRDesignExpr(Sender).Expression).AsDateTime,TQRDesignExpr(Sender).Mask );
     {
     Fecha := FQReport.DataSet.FieldByName(TQRDBText(Sender).DataField).AsDateTime;
     if Fecha <= 0 then Value := ''
     else Value := FormatDateTime( TQRDBText(Sender).Mask, Fecha );}
end;


procedure TQRReporteListado.OnPrintExprString(Sender: TObject; var Value: String);
 var Texto: string;
begin
     if (Value <> '') then     // Est� validaci�n estaba en QRCtrls.TQRCustomLabel.PrintToCanvas, se removi� en nuevo QR, se traspasa aqu� para que no aplique formato si el caption viene vacio
     begin
          Texto := FQReport.DataSet.FieldByName(TQRDBText(Sender).DataField).AsString;
          Value := FormatMaskText( TQRDBText(Sender).Mask, Texto );
          Value := Copy( Value, 1, TQRDesignDBText(Sender).GroupID );
     end;
end;

{procedure TQRReporteListado.AgregaQRDBField( oBanda : TQRCustomBand;
                                      oCampo : TCampoOpciones );
 var oExpr : TQRDesignDBText;
begin
     if oCampo.PosAgente >= 0 then
     begin
          oExpr := TQRDesignDBText( oBanda.AddPrintable( TQRDesignDBText ) );
          with oExpr do
          begin
               Color := clYellow;
               Dataset := fQReport.DataSet;
               DataField := oCampo.NombreCampo;
               AutoSize := FALSE;
               Left := oCampo.LeftCampo;
               Top := oCampo.Top;
               Width := oCampo.AnchoCampo;
               GroupID := oCampo.Ancho;
               Transparent := TRUE;
               Mask := oCampo.Mascara;

               if StrVacio( Mask ) then Mask := GetMascaraDefault(oCampo.TipoImp);
               Alignment := GetJustificacion( oCampo.TipoImp );
          end;
          if (oCampo.TipoImp in [tgTexto,tgMemo]) AND (oExpr.Mask<> '')then
             oExpr.OnPrint := OnPrintExprString
          else if oCampo.TipoImp = tgFecha then
               oExpr.OnPrint := OnPrintExprDate;
     end
     else AgregaQRExpr(oBanda,oCampo,FALSE,FALSE);
end;}

procedure TQRReporteListado.AgregaQRConstante( oBanda : TQRCustomBand;
                                               oCampo : TCampoOpciones );
begin
     with TQRDesignLabel( oBanda.AddPrintable( TQRDesignLabel ) ) do
     begin
          with oCampo do
               Caption := Copy(FormateaValor( SQLColumna.ValorConstante,
                                              Mascara,
                                              TipoImp, FALSE ),1, Ancho);
          AutoSize := FALSE;
          Left := oCampo.LeftCampo;
          Top := oCampo.Top;
          Width := oCampo.AnchoCampo;
          Transparent := TRUE;
          Alignment := GetJustificacion( oCampo.TipoImp );
     end;
end;

procedure TQRReporteListado.AgregaQRDBField( oBanda : TQRCustomBand;
                                             oCampo : TCampoOpciones );
 var oExpr : TQRDesignDBText;
begin
     if oCampo.PosAgente >= 0 then
     begin
          if oCampo.SQLColumna.EsConstante then
             AgregaQRConstante(oBanda,oCampo)
          else
          begin
               oExpr := TQRDesignDBText( oBanda.AddPrintable( TQRDesignDBText ) );
               with oExpr do
               begin
                    Dataset := fQReport.DataSet;
                    DataField := oCampo.NombreCampo;
                    AutoSize := FALSE;
                    Left := oCampo.LeftCampo;
                    Top := oCampo.Top;
                    Width := oCampo.AnchoCampo;
                    GroupID := oCampo.Ancho;
                    Transparent := TRUE;
                    Mask := oCampo.Mascara;

                    if StrVacio( Mask ) then Mask := GetMascaraDefault(oCampo.TipoImp);
                    Alignment := GetJustificacion( oCampo.TipoImp );
               end;
               if (oCampo.TipoImp in [tgTexto,tgMemo]) AND (oExpr.Mask<> '')then
                  oExpr.OnPrint := OnPrintExprString
               else if oCampo.TipoImp = tgFecha then
                    oExpr.OnPrint := OnPrintExprDate;
          end;
     end
     else {Llega a este punto cuando es impresion de Grids}
         AgregaQRExpr(oBanda,oCampo,FALSE,FALSE);
end;


procedure TQRReporteListado.AgregaQRLabel( oBanda : TQRCustomBand;
                                           oCampo : TCampoOpciones;
                                           const lValidaTitulo : Boolean = FALSE );
begin
     if ( oCampo.Titulo <> SINTITULO ) then
        with TQRDesignLabel( oBanda.AddPrintable( TQRDesignLabel ) ) do
        begin
             if lValidaTitulo AND EsSoloTotales AND (oCampo.Titulo =':') then
                Caption := ''
             else Caption := oCampo.Titulo;
             if EsVertical AND ( oBanda.BandType in [ rbDetail, rbSummary ] ) then
                Caption := Caption +':';
             AutoSize := EsVertical;
             Left := oCampo.LeftTitulo;
             Top := oCampo.Top;
             Width := oCampo.AnchoTitulo;
             Transparent := TRUE;
             Alignment := oCampo.Justificacion;
        end;
end;

procedure TQRReporteListado.SetAltoTotales( const iAlto : integer );
 var j: integer;
begin
     with FQReport.Bands do
     begin
          if FSummary then
          begin
               if HasDetail then
                  SummaryBand.Font := DetailBand.Font
               else SummaryBand.Font := fQReport.Font;

               if SummaryBand.Height < iAlto then
                  SummaryBand.Height := iAlto;
               CreaBandaHijo(TQrDesignBand(SummaryBand));
               SummaryBand.ChildBand.Height := 1;
          end;
          for j := 0 to FBandaTotales.Count - 1 do
          begin
               TQrDesignBand(FBandaTotales[j]).Font := DetailBand.Font;
               TQrDesignBand(FBandaTotales[j]).Height := iAlto;
          end;
     end;
end;

procedure TQRReporteListado.AgregaTotales( oCampo : TCampoOpciones );
 var j : integer;
begin
     if oCampo.OpImp <> ocNinguno then
     begin
          {with FQReport.Bands do
          begin
               if NOT HasSummary then
               begin
                    HasSummary := TRUE;
                    SummaryBand.Font := DetailBand.Font;
                    SummaryBand.Height := DetailBand.Height;
               end;
               //SummaryBand.Color := clYellow;
          end;}
          with FQReport.Bands do
          begin
               if FSummary then
               begin
                    AgregaQRExpr( SummaryBand, oCampo, TRUE, TRUE );
                    if EsVertical then
                       AgregaQrLabel( SummaryBand, oCampo );
               end;
          end;
          for j := 0 to FBandaTotales.Count - 1 do
          begin
               AgregaQRExpr(TQrDesignBand(FBandaTotales[j]), oCampo, TRUE, j<>(FBandaTotales.Count-1) );
               if EsVertical then
                  AgregaQrLabel( TQrDesignBand(FBandaTotales[j]), oCampo );
          end;
     end;
end;

procedure TQRReporteListado.CreaColumnas( oCampos, oGrupos : TStrings );

 var i, j,iLeft, iLeftInicial, iPagina,
     iAltoBanda,
     iAltoDetail, iAltoHeader : integer;
     oCampo : TCampoListado;
     oGrupo : TCampoOpciones;
     lAjusta, lHeader : Boolean;
     oEncabezado : TStrings;

 procedure BajaRenglon;
 begin
      iLeft := iLeftInicial;
      with fQReport.Bands do
      begin
           iAltoBanda := iAltoBanda + iAltoDetail;
           DetailBand.Height := DetailBand.Height+iAltoDetail;
           if HasColumnHeader and FAjustaColumnHeader then
              ColumnHeaderBand.Height := ColumnHeaderBand.Height + iAltoHeader;
      end;
      lAjusta := FALSE;
 end;
begin
     if FAnchoGrupos = 0 then
        iLeft := K_LEFT_INICIAL
     else iLeft := FAnchoGrupos;
     iLeftInicial := iLeft;
     iAltoBanda := K_TOP_INICIAL;
     lHeader := FALSE;
     oCampo := NIL;

     with fQReport.Bands do
     begin
          if HasDetail then
          begin
               iAltoDetail := AltoTitulo( DetailBand.Font, 'W' );
               DetailBand.Height := iAltoDetail;
               DetailBand.Enabled := NOT EsSoloTotales;
               iPagina := DetailBand.Width;
          end
          else
          begin
               iAltoDetail := AltoTitulo( fQReport.Font, 'W' );
               HasDetail := TRUE;
               iPagina := DetailBand.Width;
               if EsSoloTotales then
                  HasDetail := FALSE;
          end;
          if HasColumnHeader AND FAjustaColumnHeader then
          begin
               iAltoHeader := AltoTitulo( ColumnHeaderBand.Font, 'W' );
               ColumnHeaderBand.Height := iAltoHeader;
          end;

          //lAjusta := TRUE;
          lAjusta := FesPreview or (FDatosImpresion.Tipo <> tfXLS);
          for i:= 0 to oCampos.Count - 1 do
          begin
               oCampo := TCampoListado( oCampos.Objects[ i ] );
               oCampo.AnchoTitulo := 0;
               oCampo.AnchoCampo := 0;

               if oCampo.Calculado = K_RENGLON_NUEVO then
               begin
                    BajaRenglon;
               end
               else 
               begin
                    if ( oCampo.Ancho > 0 ) AND
                       ((NOT EsSoloTotales) OR
                        (EsSoloTotales AND (oCampo.OpImp<> ocNinguno))) then
                    begin
                         oCampo.Top := iAltoBanda;
                         SetAnchosCampo( oCampo, iLeft );

                         if ( ( iPagina ) -
                            ( iLeft + iMax( oCampo.AnchoTitulo, oCampo.AnchoCampo ) + K_LEFT_INICIAL ) ) < 0 then
                         begin
                              BajaRenglon;
                              oCampo.Top := iAltoBanda;
                              SetAnchosCampo( oCampo, iLeft );
                         end;

                         iLeft := iLeft + iMax( oCampo.AnchoTitulo, oCampo.AnchoCampo ) + K_ESPACIO;
                         if ( i = 0 ) AND ( FAnchoGrupos = 0 ) then iLeftInicial := iLeft;
                    end;
               end
          end; //for
          //iAltoBanda := DetailBand.Height;

          if lAjusta  AND (oCampo<>NIL) then
          begin
               FCentraPagina := iPagina - ( iLeft );
               if FCentraPagina > 0 then
               begin
                    FCentraPagina := Round( FCentraPagina/2 );
                    for i := 0 to oCampos.Count - 1 do
                    begin
                         if oCampo.Calculado <> K_RENGLON_NUEVO then
                         begin
                              oCampo := TCampoListado( oCampos.Objects[ i ] );
                              oCampo.LeftCampo := oCampo.LeftCampo + (FCentraPagina);
                              oCampo.LeftTitulo := oCampo.LeftTitulo + (FCentraPagina);
                         end;
                    end;
               end;
          end;

          if EsSoloTotales and (oGrupos.Count > 1) then
          begin
               with oGrupos do
                    oEncabezado := TGrupoOpciones(Objects[ Count -1 ]).ListaEncabezado;
               for i := 0 to oEncabezado.Count - 1 do
               begin
                    oGrupo := TCampoOpciones(oEncabezado.Objects[i]);
                    with oGrupo do
                    begin
                         LeftCampo := LeftCampo + (FCentraPagina);
                         LeftTitulo := LeftTitulo + (FCentraPagina);
                    end;
                    if FBandaTotales.Count > 0 then
                       AgregaQRDBField( FBandaTotales[FBandaTotales.Count-1], oGrupo );
                    if HasColumnHeader and (FBandaTitulos.Count = 0) then
                       AgregaQRLabel( ColumnHeaderBand, oGrupo, TRUE )
                    else if (FBandaTitulos.Count > 0) then
                         for j:=0 to FBandaTitulos.Count-1 do
                             AgregaQRLabel( FBandaTitulos[j], oGrupo )
               end;
          end;
          for i := 0 to oCampos.Count - 1 do
          begin
               oCampo := TCampoListado( oCampos.Objects[ i ] );
               if ( oCampo.Calculado <> K_RENGLON_NUEVO ) then
               begin
                    if ( oCampo.Ancho > 0 ) then
                    begin
                         if HasColumnHeader then
                         begin
                              oCampo.Justificacion := GetJustificacion( oCampo.TipoImp );
                              oCampo.Alto := AltoTitulo( ColumnHeaderBand.Font, oCampo.Titulo );
                              if (FBandaTitulos.Count=0) then
                              AgregaQRLabel( ColumnHeaderBand, oCampo );
                         end;
                         if NOT lHeader then
                         begin
                              for j := 0 to FBandaTitulos.Count - 1 do
                                  TQRDesignBand(FBandaTitulos[j]).Height := iAltoHeader;
                              lHeader := TRUE;
                         end;

                         for j := 0 to FBandaTitulos.Count - 1 do
                         begin
                              AgregaQRLabel( TQRDesignBand(FBandaTitulos[j]), oCampo );
                              with TQRDesignBand(FBandaTitulos[j]) do
                                   Height := AltoTitulo( Font, 'W' )+oCampo.Top+1;
                         end;

                         if HasDetail then
                            oCampo.Alto := AltoTitulo( DetailBand.Font, 'W' )
                         else
                             oCampo.Alto := AltoTitulo( fQReport.Font, 'W' );

                         if NOT EsSoloTotales then AgregaQRDBField( DetailBand, oCampo );
                    end
                    else if NOT ESSoloTotales then AgregaQRDBField( DetailBand, oCampo );

                    AgregaTotales(oCampo);
               end;
          end;
          if HasDetail then
             SetAltoTotales(DetailBand.Height)
          else
             SetAltoTotales(16);
     end;//with fQrReporte.Bands
end;

procedure TQRReporteListado.CreaRenglones( oCampos : TStrings );
 const K_RENGLON = 2;

 var i,j, iTop, iPagina : integer;
     oCampo : TCampoListado;
begin
     with fQReport.Bands do
     begin
          iTop := 3;

          if NOT HasDetail then
          begin
               HasDetail := TRUE;
               DetailBand.Height := 0;
          end;

          iPagina := Round( DetailBand.Width / 3 );

          for i:= 0 to oCampos.Count-1 do
          begin
               oCampo := TCampoListado( oCampos.Objects[ i ] );
               oCampo.AnchoCampo := 0;
               if oCampo.Ancho > 0 then
               begin
                    if oCampo.Calculado <> K_RENGLON_NUEVO then
                    begin
                         oCampo.LeftCampo := iPagina + K_RENGLON;
                         oCampo.AnchoCampo := AnchoCampo( fQReport.Font, oCampo.Ancho );
                         oCampo.Alto := AltoTitulo( fQReport.Font, oCampo.Titulo );
                         oCampo.Top := iTop;
                         if NOT EsSoloTotales then AgregaQRDBField( DetailBand, oCampo );

                         oCampo.AnchoTitulo := AnchoTitulo( fQReport.Font, oCampo.Titulo );
                         oCampo.LeftTitulo := iPagina - oCampo.AnchoTitulo - 5;
                         oCampo.Justificacion := taRightJustify;

                         if NOT EsSoloTotales then
                            AgregaQRLabel( DetailBand, oCampo );

                         if ( NOT EsSoloTotales ) OR ( EsSoloTotales AND ( oCampo.OpImp <> ocNinguno ) ) then
                             iTop := iTop + oCampo.Alto + K_RENGLON;

                         if oCampo.OpImp <> ocNinguno then
                            for j:=0 to FBandaTotales.Count -1 do
                                AgregaQRLabel( TQrDesignBand(FBandaTotales[j]),oCampo );
                    end;
               end
               else if NOT EsSoloTotales then AgregaQRDBField( DetailBand, oCampo );
               AgregaTotales( oCampo );
          end; //for

          if NOT EsSoloTotales then
          begin
               DetailBand.Height := iTop;
               CreaBandaHijo( TQrDesignBand( DetailBand ) );
               DetailBand.ChildBand.Height := 5;
          end;
          SetAltoTotales(iTop);
          if EsSoloTotales then DetailBand.Enabled := FALSE;
     end;//with
end;

procedure TQRReporteListado.CreaBandaHijo( oBanda : TQrDesignBand );
begin
     with TQRDesignChildBand.Create( SELF ) do
     begin
          ParentReport := fQReport;
          ParentBand := oBanda;
          Parent := fQReport;
     end;
end;

function TQRReporteListado.GetAjustaColumnHeader( oCampos,oGrupos : TStrings ) : Boolean;
 var i : integer;
begin
     Result := TRUE;
     if EsVertical then Exit;

     with oCampos do
          for i:= 0 to Count - 1 do
          begin
               Result :=  (TCampoListado( Objects[ i ] ).Titulo <> SINTITULO) AND
                          (TCampoListado( Objects[ i ] ).Calculado <> K_RENGLON_NUEVO) ;
               if Result then
                  Break;
          end;
end;

procedure TQRReporteListado.CreaListado( oCampos,oGrupos : TStrings );
begin
     with fQReport do
     begin
          Bands.HasColumnHeader := TRUE;
          Bands.ColumnHeaderband.Frame.DrawTop := TRUE;
     end;
     FAjustaColumnHeader := GetAjustaColumnHeader(oCampos, oGrupos);
     CreaGrupos(oGrupos);

     if EsVertical then CreaRenglones(oCampos)
     else CreaColumnas(oCampos, oGrupos);
end;



procedure TQRReporteListado.Termina;
begin
     inherited;
     FBandaTotales.Free;
     FBandaTotales := NIL;
     FBandaTitulos.Free;
     FBandaTitulos := NIL;
end;


procedure TQRReporteListado.InitGrid( oDatosImpresion : TDatosImpresion;
                                      const sValor1, sValor2, sCaption : string;
                                      const eValor1, eValor2 : TipoEstado );
begin
     if FBandaTotales = NIL then FBandaTotales := TList.Create;
     if FBandaTitulos = NIL then FBandaTitulos := TList.Create;
     //Solamente se registra cuando es Impresion de Grid.
     ZFuncsCliente.RegistraFunciones;
     ZFuncsCliente.ImprimeGrid := TRUE;
     ZFuncsCliente.TipoValor1 := eValor1;
     ZFuncsCliente.TipoValor2:= eValor2;
     ZFuncsCliente.Valor1 := sValor1;
     ZFuncsCliente.Valor2:= sValor2;
     ZFuncsCliente.FActiveFormCaption := sCaption ;

     FDatosImpresion := oDatosImpresion;
     fDirDefault := zReportTools.DirPlantilla;
     if FileExists(FDirDefault + 'LOGO.BMP') then
        QrImagen.Picture.LoadFromFile( FDirDefault + 'LOGO.BMP' );
     //CargaReporte( FDatosImpresion.Archivo );
end;

procedure TQRReporteListado.InitExportacion( oDatosImpresion : TDatosImpresion;
                                             const sValor1, sValor2, sCaption : string;
                                             const eValor1, eValor2 : TipoEstado );
 var
    i: SmallInt;
begin
     Randomize;
     i := Random(High(SmallInt));
     oDatosImpresion.Exportacion :=  Format( ZetaWinAPITools.GetTempDir + 'Archivo_%d', [i] );
     InitGrid( oDatosImpresion, sValor1, sValor2, sCaption, eValor1, eValor2 );
     with FQReport.Page do
     begin
          PaperSize := Custom;
          Width := 50;
     end;

end;

procedure TQRReporteListado.Init( oAgente : TSQLAgenteClient;
                                  oDatosImpresion : TDatosImpresion;
                                  iCountParametros: Integer;
                                  var lHayImagenes : Boolean );
begin
     if FBandaTotales = NIL then FBandaTotales := TList.Create;
     if FBandaTitulos = NIL then FBandaTitulos := TList.Create;

     inherited;
end;

procedure TQRReporteListado.FQReportPreview(Sender: TObject);
begin
     //PENDIENTE - aqui falta la validacion de Sentinel
     //ShowPreview(ZReportTools.ModuloGraficas);
     ShowPreview(NOT ZFuncsCliente.ImprimeGrid);
end;

end.
