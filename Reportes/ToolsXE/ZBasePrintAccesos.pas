unit ZBasePrintAccesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, DB, Quickrpt, QrPrntr, Qrctrls, DBClient;

type
  TRepDerechos = class(TForm)
    QRReporte: TQuickRep;
    PageHeaderBand: TQRBand;
    QRTitulo: TQRLabel;
    FechaImpresionLBL: TQRLabel;
    FechaImpresion: TQRSysData;
    QRShape4: TQRShape;
    Company: TQRLabel;
    CompanyLBL: TQRLabel;
    GroupLBL: TQRLabel;
    Group: TQRLabel;
    DetailBand: TQRBand;
    ChildBand: TQRChildBand;
    RI_NOMBRE: TQRExpr;
    RI_ITEM_00: TQRExpr;
    RI_NOMBRElbl: TQRExpr;
    RI_ITEM_01: TQRExpr;
    Explicacion1LBL: TQRExpr;
    Titulo1LBL: TQRExpr;
    Explicacion2LBL: TQRExpr;
    Titulo2LBL: TQRExpr;
    RI_ITEM_02: TQRExpr;
    RI_ITEM_03: TQRExpr;
    RI_ITEM_07: TQRExpr;
    RI_ITEM_06: TQRExpr;
    RI_ITEM_05: TQRExpr;
    RI_ITEM_04: TQRExpr;
    Titulo3LBL: TQRExpr;
    Explicacion3LBL: TQRExpr;
    Explicacion4LBL: TQRExpr;
    Titulo4LBL: TQRExpr;
    Titulo5LBL: TQRExpr;
    Titulo6LBL: TQRExpr;
    Titulo7LBL: TQRExpr;
    Titulo8LBL: TQRExpr;
    Explicacion5LBL: TQRExpr;
    ExplicacionLBL: TQRExpr;
    RI_COMENTA: TQRExprMemo;
    QRSysData1: TQRSysData;
    Filtro: TQRLabel;
    Titulo9LBL: TQRExpr;
    Titulo10LBL: TQRExpr;
    Titulo11LBL: TQRExpr;
    RI_ITEM_08: TQRExpr;
    RI_ITEM_09: TQRExpr;
    RI_ITEM_10: TQRExpr;
    procedure FechaImpresionPrint(sender: TObject; var Value: String);
    procedure RI_ITEM_00Print(sender: TObject; var Value: String);
    procedure QRReportePreview(Sender: TObject);
  private
    { Private declarations }
    function GetDatos: TDataset;
    procedure SetDatos(const Value: TDataset);
    procedure SetEmpresa(const Value: String);
    procedure SetGrupo(const Value: String);
    procedure SetTodos(const Value: Boolean);
  public
    { Public declarations }
    property Todos: Boolean write SetTodos;
    property Datos: TDataset read GetDatos write SetDatos;
    property Empresa: String write SetEmpresa;
    property Grupo: String write SetGrupo;
    procedure Preview;
    procedure Exporta;
  end;

var
  RepDerechos: TRepDerechos;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZReportTools,
     DExportEngine,
     FPreview_DevEx;
     
{$R *.DFM}

procedure TRepDerechos.FechaImpresionPrint(Sender: TObject; var Value: String);
begin
     Value := FormatDateTime( 'dd/mmm/yyyy', Now );
end;

function TRepDerechos.GetDatos: TDataset;
begin
     Result := QRReporte.Dataset;
end;

procedure TRepDerechos.Preview;
begin
     QRReporte.Preview;
end;

procedure TRepDerechos.SetDatos(const Value: TDataset);
begin
     QRReporte.Dataset := Value;
end;

procedure TRepDerechos.SetEmpresa(const Value: String);
begin
     Company.Caption := Value;
end;

procedure TRepDerechos.SetGrupo(const Value: String);
begin
     Group.Caption := Value;
end;

procedure TRepDerechos.SetTodos(const Value: Boolean);
begin
     with Filtro do
     begin
          if Value then
             Caption := 'Todos Los Derechos'
          else
              Caption := 'Unicamente Los Derechos Asignados';
     end;
end;

procedure TRepDerechos.RI_ITEM_00Print(sender: TObject; var Value: String);
begin
     if ZetaCommonTools.StrVacio( Value ) or ( Value = K_GLOBAL_NO ) then
        Value := '';
end;

procedure TRepDerechos.QRReportePreview(Sender: TObject);
begin
     QRReporte.Zoom := 100;

     with TPreview_DevEx.Create( self ) do
     begin
          QRPreview.Zoom := 100;
          Caption := Self.Caption;
          QRPrinter := QRReporte.QRPrinter;
          Reporte := QRReporte;
          QRPreview.QRPrinter := QRReporte.QRPrinter;
          WindowState := wsMaximized;
          ShowGrafica := FALSE;
     end;
end;

procedure TRepDerechos.Exporta;
const
     K_FILTER = 'XLS (Excel - Hoja de C�lculo)|*.XLS'+
                '|RTF (Texto Enriquecido)|*.RTF'+
                '|HTML (P�ginas Internet)|*.HTML'+
                '|PDF (Documento de Acrobat)|*.PDF|';
var
   sNombreArchivo: string;
   iFilter: Integer;
   eTipo: eTipoFormato;
begin
      sNombreArchivo := ZReportTools.ReportSaveDialogFilter( 'Derechos',
                                                             '',
                                                              K_FILTER,
                                                              iFilter );
      case iFilter of
           2: eTipo := tfRTF;
           3: eTipo := tfHTML;
           4: eTipo := tfPDF;
      else
          eTipo := tfXLS;
      end;

      try
         QRReporte.Prepare;

         dmExportEngine := TdmExportEngine.Create( Self );
         dmExportEngine.Exporta( QRReporte, eTipo, sNombreArchivo );
      finally
             FreeAndNil( dmExportEngine );
      end;
end;

end.
