{$HINTS OFF}
unit ZQrReporte;
{$ifndef DOS_CAPAS}
{$ifndef CONCILIA}
  {$ifndef XMLREPORT}                                                                              
   {$define HTTP_CONNECTION}
  {$endif}
 {$endif}
{$endif}

{$INCLUDE DEFINES.INC}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, comctrls, DB, StrUtils,
  {$ifndef ver130}
  qrdQuickRep,
  thsdRuntimeLoader,
  thsdRuntimeEditor,
  qrdBaseCtrls,
  Variants,
  MaskUtils,
  {$endif}
  QuickRpt,
  qrprntr,
  Qrctrls,
  QrExport,
  Qrdctrls,
  QRDesign,
  QRDLDR,
  qrdProcs,
  QRExpr,
  Mask,
  StdCtrls,
  FileCtrl,
  {$ifndef TRESS_REPORTES}
  FPreview_DevEx,
  {$endif}
  {$ifdef QR362}
  qrpBaseCtrls,
  {$endif}
  ZAgenteSQLClient,
  ZReportTools,
  ZReportToolsConsts,
  ZetaCommonClasses,
  ZetaCommonLists, jpeg, {ieview,} dxGDIPlusClasses, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  TressMorado2013, cxImage, cxDBEdit, imageenview, ieview, dbimageen, imageen;


type
{ ~*~*~*~*~*~*~*~*~*~* TJustificadorRTF ~*~*~*~*~*~*~*~*~*~*~*~*~*~*}

    TJustificadorRTF = Class
     private
        FLineasMemo, FLineasTempo : TStringList;
        FAnchoControl : Integer;
        FHuecos : Variant;
        procedure SetAnchoControl( const nAncho : Integer );
     public
        constructor Create;
        destructor Destroy; override;
        procedure Justifica(oRich : TRichEdit);
        property  AnchoControl : Integer read FAnchoControl write SetAnchoControl;
        property  Justificadas : TStringList read FLineasMemo;
     end;
{ ~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*}

  TQrReporte = class(TForm)
    FQRDesigner: TQRepDesigner;
    QrImagen: TQRDesignImage;
    RichEdit3: TRichEdit;
    RichParent: TRichEdit;
    Memo1: TMemo;
    Label1: TLabel;
    FQReport: TDesignQuickReport;
    ImageVaciaBMP: TImage;
    DBImage: TImageEnView;
    procedure FQReportPreview(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FQReportAfterPreview(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FListaImagenes: TList;
    FListaImagenesEn: TList;
    FCountParams : integer;
    FAsignaMedidas : Boolean;
    FHayImagenes, FHayRichText, FHayMemo, FMostrandoPrewiew : Boolean;
    FJustifica : TJustificadorRTF;
    FParentRichText : TRichEdit;
    FRichText: TQRRichText;
    {.$ifdef TRESSEMAIL}
    FPageCount: integer;
    {.$endif }
    {.$ifdef QR362}
    FQRDataSource : TDataSource;
    {.$endif}
    FEmpleadoAgregado: Boolean;
    FMaestroAgregado: Boolean;
    {$ifdef VISITANTES}
    FVisitanteAgregado: Boolean;
    {$endif}
    {$ifdef XMLREPORT}
    FDefaultStream: TStream;
    {$endif}
    procedure EncuentraHijos(oBanda: TQrCustomBand);
    procedure OnPrintExprString(Sender: TObject; var Value: String);
    procedure OnPrintExprDate(Sender: TObject; var Value: String);
    function AddCampo(const sFormula: string;const iBanda: integer; const eTipo : eTipoGlobal = tgAutomatico; sAlias : string=''): integer;
    function AddGrupo(const sFormula: string): integer;
    procedure RotateImage(oPicture: TImageEnView; iRotacion: integer);
    {.$ifdef TRESSEMAIL}
    {$ifdef FALSE}
    {procedure GeneraQRHtml;
    procedure GeneraQRPDF;}
    {$endif}
    function GetTieneMultiplesHojas : Boolean;
    procedure CreaPlantillaInterna;
    {function ResizeImageBestFit(PImageEn: TImageEn; iHeight,iWidth: integer): Boolean;
    function ResizeImageKeepProportions(PImageEn: TImageEn;  NewDimension: integer; IsWidth: Boolean): Boolean;  }

    //function GetExpressionBanda(oBanda: TQRCustomBand): string;
    {.$endif}
    procedure ZError( const sCaption, sMsg: String; const iHelpCtx: Longint );
  protected
    fNombre : string;
    fDirDefault : string;
    FDatosImpresion : TDatosImpresion;
    FAgente : TSQLAgenteClient;
    FCountImagen : integer;
    FEsPreview: Boolean;
    procedure CreaNuevoReporte;
    procedure CargaReporte(sFileName: string);
    procedure CreaTodasBandas;

    procedure Preview;
    procedure ShowPreview(const lGrafica: Boolean);
    procedure Imprime;
    procedure Termina;virtual;
    procedure RevisaControles;
    procedure AgregaFormulas;
    procedure GetExpresion;
    function AgregaGrupo(const sFormula: string): string;
    procedure AgregaImagen(oExpr: TQRDesignImage);
    procedure AgregaDBImagen( oBanda : TQrCustomBand;
                              oExpr : TQRDesignDBImage );
    procedure AsignaOnPrint(oExpr: TQRDesignExpr; const eTipo: eTipoGlobal);
    procedure DecodeMemo(oBanda : TQrCustomBand; oMemo : TQrMemo; const sFind : string; const lAgrega : Boolean;  const iBanda : integer );
    procedure DecodeRichText( oBanda : TQrCustomBand;
                              oExpr : TQRDesignRichtext;
                              const sFind, sReplace : string;
                              const lAgrega : Boolean;
                              const iAncho,iBanda : integer );

    procedure BeforePrintImage;
    procedure BeforePrintBanda(Sender: TQRCustomBand;var PrintBand: Boolean);
    procedure ParametrosImpresion;

    procedure BeforePrintRichText(oRichEdit : TRichEdit);
    procedure BeforePrintMemo(oMemo : TQRDesignMemo);
    procedure Justifica1;
    procedure JustificaRichText( const iSize, iWidth : integer;
                                 oRichEdit : TRichEdit;
                                 oQrRich : TQRRichtext;
                                 const lRich : Boolean);
    function AnchoTexto( const iSize, iWidth : integer ) : Integer;
    procedure JustificaMemo( oMemo : TQrMemo );
    {.$ifdef QR362}
    property QrDataSource : TDataSource read FQrDataSource;
    {.$endif}
  public
    procedure Init( oAgente : TSQLAgenteClient;
                    oDatosImpresion : TDatosImpresion;
                    iCountParametros: Integer;
                    var lHayImagenes : Boolean  ); virtual;
    function GeneraForma( const sNombre : string;
                          const EsPreview : Boolean;
                          oDataSet : TDataSet ):Boolean;

    {.$ifdef TRESSEMAIL}
    function UnSoloHTML: Boolean;
    procedure AddNombreImagenes(oLista: TStrings);
    property PageCount : integer read FPageCount;
    {.$endif}
    {$ifdef XMLREPORT}
    property DefaultStream: TStream read FDefaultStream write FDefaultStream;
    {$endif}
  end;

{$ifdef XMLREPORT}
threadvar
{$else}
var
{$endif}
  { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
  QrReporte: TQrReporte = NIL;

procedure PreparaReporte;
procedure DesPreparaReporte;


implementation


uses //dll95v1,
    {$ifdef KIOSCO2}
     DCliente,
    {$ENDIF}
     {$ifdef HTTP_CONNECTION}
     DReportes,
     {$endif}
     ZetaRegistryCliente,
     DExportEngine,
     ZReportDialog,
     ZFuncsCliente,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZetaCommonTools,
     FToolsImageEn;


{$R *.DFM}

const K_RESULT_ALIAS = '#RESULT';
      K_ALIAS = 'RESULT';
      
procedure PreparaReporte;
begin
     if QRReporte = NIL then        
        { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
        QRReporte := TQRReporte.Create(  {$ifdef TRESS_REPORTES}nil{$else}{$ifdef XMLREPORT}nil{$else}Application{$endif}{$endif} );

end;

procedure DesPreparaReporte;
begin
     { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
     if ( QRReporte <> NIL )  then
          QRReporte.Free;
     QRReporte := NIL;
end;

procedure TQRReporte.CreaNuevoReporte;
begin
     qrdProcs.ClearReportForm( Self, TRUE );
     CreaTodasBandas;
end;

procedure TQRReporte.CargaReporte( sFileName : string );
{$ifdef HTTP_CONNECTION}
var
   BlobPlantilla: TBlobField;
{$endif}

   function CargaPlantilla: Boolean;
   begin
        Result := FALSE;
        if FileExists( sFileName ) then
        begin
             Result := FQRDesigner.LoadReport( sFileName );
             if ( NOT Result ) then
                ZError(Caption, 'Error al Cargar Plantilla ' + sFileName , 0)
        end
        {$ifdef HTTP_CONNECTION}
        else if ( ClientRegistry.TipoConexion = conxHTTP ) then
        begin
             BlobPlantilla := dmReportes.GetPlantilla( sFileName );
             Result := FQRDesigner.LoadReport_FromBlobField(BlobPlantilla);
        end;
        {$else}
        ;
        {$endif}
   end;

   {$IFDEF TRESS_DELPHIXE5_UP}
   function ConviertePlantilla: Boolean;
   var
      aDir: array[0..255] of char;
      sTempFile: String;
   begin
        try
           GetTempPath(255, aDir);
           sTempFile:= ZetaCommonTools.SetFileNamePath( aDir, ExtractFileName( sFileName ) );
           FQRDesigner.SaveReport( sTempFile );  // Crear la plantilla temporal convertida
           Result := FileExists( sTempFile ) and FQRDesigner.LoadReport( sTempFile );
           DeleteFile( sTempFile );   // Borra la plantilla temporal
        except
              Result := FALSE;
        end;
   end;
   {$ENDIF}

begin
     if CargaPlantilla then
     begin
          {$IFDEF TRESS_DELPHIXE5_UP}
          if ( NOT ConviertePlantilla ) then
             CargaPlantilla;    // Si no la pudo convertir vuelve a cargar la plantilla original
          {$ELSE}
          //No se hace nada, ya est� cargada la plantilla
          {$ENDIF}
     end;
     {$IFDEF TEST_QUICKREPORT}
          FQRDesigner.SaveToFile('C:\logPlantilla.txt');
     {$ENDIF}
     fQRDesigner.QReport.Zoom := 100;
      {***(@am): Para plantillas compiladas en D7, no era necesario asingar el fQRDesigner.QReport AL FQReport,
                el refresh de informacion se hacia automaticamente. Ahora en XE5 se tiene que reasignar.***}
     FQReport:= fQRDesigner.QReport;
      {***}
end;

procedure TQRReporte.CreaTodasBandas;
begin
     with FQReport.Bands do
     begin
          if NOT HasColumnHeader then
          begin
               with TQRDesignBand.Create( self ) do
               begin
                    BandType := rbColumnHeader;
                    Parent := FQReport;
                    Height := 1;
                    Font.Name := 'ARIAL';
                    Font.Size := 8;
               end;
          end;
          if NOT HasDetail then
          begin
               with TQRDesignBand.Create( self ) do
               begin
                    BandType := rbDetail;
                    Parent := FQReport;
                    Height := 1;
                    Font.Name := 'COURIER NEW';
                    Font.Size := 7;
               end;
          end;
          if NOT HasPageFooter then
          begin
               with TQRDesignBand.Create( self ) do
               begin
                    BandType := rbPageFooter;
                    Parent := FQReport;
                    Height := 1;
               end;
          end;
          if NOT HasPageHeader then
          begin
               with TQRDesignBand.Create( self ) do
               begin
                    BandType := rbPageHeader;
                    Parent := FQReport;
                    Height := 1;
                    Font.Name := 'ARIAL';
                    Font.Size := 8;
               end;
          end;
          if NOT HasSummary then
          begin
               with TQRDesignBand.Create( self ) do
               begin
                    BandType := rbSummary;
                    Parent := FQReport;
                    Height := 1;
               end;
          end;
          if NOT HasTitle then
          begin
               with TQRDesignBand.Create( self ) do
               begin
                    BandType := rbTitle;
                    Parent := FQReport;
                    Height := 1;
                    Font.Name := 'ARIAL';
                    Font.Size := 8;
               end;
          end;
     end;
end;

procedure TQRReporte.Preview;
begin
     try
        FQReport.Preview;
     except
           On E: Exception do
              ZError(Caption, 'Error al Generar el Preview' +CR_LF+
                              E.Message,0);
     end;
end;

procedure TQRReporte.Termina;
 var i : integer;
begin
     FQReport.DataSet := NIL;
     {.$ifdef QR362}
     FreeAndNil( FQRDataSource );
     {.$endif}
     with FQReport do
     begin
          for i := 0 to BandList.Count - 1 do
              while TQrCustomBand(BandList[ i ]).ControlCount > 0 do
                    TQrCustomBand( BandList[ i ] ).Controls[ 0 ].Free;
     end;
      {***ANTERIOR***}
     {with DBImageTemp.DataBinding do
     begin
          DataSource := NIL;
          DataField := '';
     end;}
end;

procedure TQRReporte.EncuentraHijos(oBanda : TQrCustomBand);

begin
     with oBanda do
          if HasChild then
          begin
               ChildBand.Tag := Tag;
               EncuentraHijos( ChildBand );
          end;
end;


procedure TQRReporte.RevisaControles;
 var
    oControl : TControl;
    sControl : string;
    i : integer;
begin
     for i := 0 to FQReport.ControlCount - 1 do
     begin
          oControl := FQReport.Controls[i];

          if ( oControl.Parent = NIL ) OR
             ( NOT ( oControl is  TQrCustomBand ) AND
               ( oControl is  TQrPrintable ) AND
               ( oControl.Parent is TDesignQuickReport ) ) then
          begin
               if ( oControl is TQRDesignExpr ) then
                    sControl := 'La F�rmula: ' + TQRDesignExpr( oControl ).Expression
               else if ( oControl is TQRDesignImage ) then
                    sControl := 'La Im�gen ' + TQRDesignImage( oControl ).FileName
               else if ( oControl is TQRDesignDBImage ) then
                    sControl := 'La Im�gen ' + TQRDesignDBImage( oControl ).DataField
               else if ( oControl is TQRDesignRichtext ) then
                    sControl := 'Uno de los Textos Enriquecidos'
               else if (( oControl is TQrDesignMemo ) OR ( oControl is TQrMemo )) then
                    sControl := 'Uno de los Memos'
               else sControl := 'Uno de los Textos';
               //raise Exception.Create( sControl + CR_LF + 'NO se Encuentra dentro de una Banda' );

               // raise Exception.Create( sControl + CR_LF + 'No est� Dentro de una Banda.' + CR_LF + 'Por Favor de Revisar la Plantilla del Reporte' );
               ZError (Caption, sControl + CR_LF + 'No est� Dentro de una Banda.' + CR_LF + 'Por Favor de Revisar la Plantilla del Reporte', 0);
          end;
     end;
end;

{function TQrReporte.GetExpressionBanda( oBanda: TQRCustomBand ): string;
begin
     Result := VACIO;
     if ( oBanda is TQrDesignBand ) then
     begin
          Result := TQrDesignBand(oBanda).Expression;
     end

     else if ( oBanda is TQrDesignChildBand ) then
     begin
          Result := TQrDesignChildBand(oBanda).Expression;
     end;
end;  }

procedure TQRReporte.AgregaFormulas;

 function GetTipoGlobal( const iTag :  integer ) : eTipoGlobal;
 begin
      if iTag = 0 then Result := tgAutomatico
      else Result := eTipoGlobal( iTag - 1 );
 end;

 var i, j : integer;
     oBanda : TQrCustomBand;
     oExpr : TControl;
     FCountGrupos: integer;
begin
     FCountGrupos := 0;
     for i := 0 to FQReport.BandList.Count - 1 do
     begin
          oBanda := TQrCustomBand(FQReport.BandList[i]);
          case oBanda.BandType of
               rbGroupHeader :
               begin
                    Inc(FCountGrupos);
                    oBanda.Tag := FCountGrupos;
                    with TQrDesignGroup( oBanda ) do
                    begin
                         Tag := AddGrupo(Expression);
                         EncuentraHijos(oBanda);
                         if FooterBand <> NIL then
                         begin
                              FooterBand.Tag := FCountGrupos;
                              EncuentraHijos(FooterBand);
                         end;
                    end;
               end;
               rbTitle,rbPageHeader,rbPageFooter,
               rbColumnHeader,rbSummary:
               begin
                    oBanda.Tag := 0;
                    EncuentraHijos(oBanda);
               end;
               rbDetail,rbSubDetail,rbOverlay:
               begin
                    oBanda.Tag := -1;
                    EncuentraHijos(oBanda);
               end;
          end;
     end;

     for i := 0 to FQReport.BandList.Count - 1 do
     begin
          oBanda := TQrCustomBand(FQReport.BandList[i]);

          if ( oBanda is TQrDesignBand ) then
          begin
               with TQrDesignBand(oBanda) do
               begin
                    if StrLleno( Expression ) then
                    begin
                         BandNum := AddCampo( Expression, oBanda.Tag, tgBooleano );
                    end;
               end
          end
          else if ( oBanda is TQrDesignChildBand ) then
          begin
               with TQrDesignChildBand(oBanda) do
               begin
                    if StrLleno( Expression ) then
                    begin
                         BandNum := AddCampo( Expression, oBanda.Tag, tgBooleano );
                    end;
               end;
          end;

          for j := 0 to oBanda.ControlCount - 1 do
          begin
               oExpr := oBanda.Controls[ j ];
               if oExpr is TQRDesignExpr then
               begin
                    oExpr.Tag := AddCampo( TQRDesignExpr( oExpr ).Expression,
                                           oBanda.Tag,
                                           GetTipoGlobal(oExpr.Tag),
                                           oExpr.Name );
               end
               else if oExpr is TQRDesignImage then
                    AgregaImagen(TQRDesignImage( oExpr ))
               else if oExpr is TQRDesignDBImage then
                    AgregaDBImagen( oBanda,TQRDesignDBImage(oExpr))
               else if oExpr is TQRDesignRichtext then
                    DecodeRichText( oBanda, TQRDesignRichtext( oExpr ),'\','&&',TRUE,2,oBanda.Tag )
               else if oExpr is TQrDesignMemo then
                    DecodeMemo( oBanda, TQrDesignMemo(oExpr),'\',TRUE, oBanda.Tag );

          end;
     end;
end;

procedure TQrReporte.AsignaOnPrint( oExpr : TQRDesignExpr;
                                    const eTipo : eTipoGlobal);
begin
     with oExpr do
     begin
          if (eTipo = tgTexto) AND (Mask <> '') then
             OnPrint := OnPrintExprString
          else if (eTipo = tgFecha) then
             OnPrint := OnPrintExprDate;
     end;
end;

procedure TQRReporte.GetExpresion;
 var i, j : integer;
     oBanda : TQrCustomBand;
     oExpr : TControl;
     oColumna : TSQLColumna;
begin
     for i := 0 to FQReport.BandList.Count - 1 do
     begin
          oBanda := TQrCustomBand(FQReport.BandList[i]);

          if ( oBanda is TQrDesignBand ) then
          begin
               with TQrDesignBand(oBanda) do
               begin
                    if StrLleno( Expression ) then
                    begin
                         Expression := GetNColumna(BandNum) + '=''SI''';
                    end;
               end
          end
          else if ( oBanda is TQrDesignChildBand ) then
          begin
               with TQrDesignChildBand(oBanda) do
               begin
                    if StrLleno( Expression ) then
                    begin
                         Expression := GetNColumna(BandNum) + '=''SI''';
                    end;
               end;
          end;

          for j := 0 to oBanda.ControlCount - 1 do
          begin
               oExpr := oBanda.Controls[ j ];
               if oExpr is TQRDesignExpr then
               begin
                    with TQRDesignExpr(oExpr) do
                    begin
                         if Tag >= 0 then
                         begin
                              oColumna := FAgente.GetColumna(Tag);
                              with oColumna do
                              begin
                                   if Mask = '' then
                                      Mask := GetMascaraDefault(TipoFormula);
                                   if oColumna.EsConstante then
                                   begin
                                        Expression := FormateaValor( ValorConstante, Mask, TipoFormula);
                                        Mask := '';
                                   end
                                   else
                                   begin
                                        if StrVacio(oColumna.Alias) then
                                           Expression := GetNColumna( Tag )
                                        else Expression := oColumna.Alias ;
                                        AsignaOnPrint(TQRDesignExpr(oExpr),TipoFormula);
                                   end;
                              end
                         end
                         else
                             Expression := StrTransAll(Expression, K_RESULT_ALIAS, K_ALIAS );
                    end;
               end
               else if oExpr is TQRDesignDBText then
               begin
                    with TQRDesignDBText(oExpr) do
                    begin
                         if Tag >= 0 then
                         begin
                              oColumna := FAgente.GetColumna(Tag);
                              with oColumna do
                              begin
                                   DataSet := FQReport.Dataset;
                                   if StrVacio(oColumna.Alias) then
                                      DataField := GetNColumna( Tag )
                                   else DataField := oColumna.Alias ;
                              end;
                         end
                         else
                             DataField := '';
                    end;
               end
               else if oExpr is TQRDesignRichtext then
                    DecodeRichText( oBanda, TQRDesignRichtext( oExpr ),'&&','%%',FALSE,2,oBanda.Tag )
               else if oExpr is TQrDesignMemo then
                    DecodeMemo( oBanda, TQrDesignMemo(oExpr),'&&',FALSE, oBanda.Tag );
          end;
          if oBanda.BandType = rbGroupHeader then
          begin
               with TQRGroup(oBanda) do
                    if Tag >= 0 then
                       with FAgente.GetColumna(Tag) do
                            if EsConstante then
                               Expression := FormateaValor(ValorConstante,'',TipoFormula)
                            else
                                Expression := GetNColumna( Tag );
          end
          {else
          begin
               with TQRDesignBand(FQReport.BandList[i]) do
                    if Tag >= 0 then
                       with FAgente.GetColumna(Tag) do
                            if EsConstante then
                               Expression := FormateaValor(ValorConstante,'',TipoFormula)
                            else
                                Expression := GetNColumna( Tag );
          end;}
     end;
end;

procedure TQRReporte.OnPrintExprString(Sender: TObject; var Value: String);
  var sMask : string;
begin
     if (Value <> '') then     // Est� validaci�n estaba en QRCtrls.TQRCustomLabel.PrintToCanvas, se removi� en nuevo QR, se traspasa aqu� para que no aplique formato si el caption viene vacio
     begin
          sMask := Trim(TQRDesignExpr( Sender ).Mask);
          if (sMask > '') AND
             (TQRDesignExpr(Sender).Value.Kind = resString) then
             Value := FormatMaskText( sMask, Value );
     end;
end;

procedure TQrReporte.OnPrintExprDate(Sender: TObject; var Value: String);
var oExpr : TQRDesignExpr;
    Resultado : TQrEvResult;
    dFecha : TDate;
    sField : string;
begin
     oExpr := TQRDesignExpr( Sender );
     Resultado := oExpr.Value;
     if Resultado.Kind = resDouble then
        dFecha := Resultado.dblResult
     else
     begin
          with FAgente.GetColumna(oExpr.Tag) do
          begin
               if StrVacio(Alias) then
                  sField := GetNColumna( oExpr.Tag )
               else sField := Alias ;
          end;
          dFecha := FQReport.DataSet.FieldByName(sField).AsDateTime;
     end;

     Value := GetFormatFecha(dFecha,oExpr.Mask)
end;

function TQRReporte.AddGrupo( const sFormula : string ) : integer;
begin
     Result := -1;
     if sFormula>'' then
     begin
          {PENDIENTE, SE AGREGARA CUANDO EL AGENTE
           PUEDA DETERMINAR CUALES GRUPOS SON SQLVALIDO}
          //Result := FAgente.AgregaOrden( sFormula,TRUE,enNinguno,FALSE );
           Result := FAgente.AgregaColumna( TransfomaParams( sFormula,  FCountParams, FAgente.Parametros ),
                                           FALSE,
                                           enNinguno,
                                           tgAutomatico,
                                           30,
                                           '',
                                           ocAutomatico,
                                           TRUE );
     end;
end;

function TQRReporte.AddCampo( const sFormula : string;const iBanda : integer;const eTipo : eTipoGlobal = tgAutomatico; sAlias : string='' ) : integer;
begin
     Result := -1;
     if sFormula>'' then
     begin
          if ( Pos(K_RESULT_ALIAS,UpperCase(sFormula)) = 0 ) then
          begin
               if Pos( K_ALIAS, sAlias) = 0 then
                  sAlias := '';
               Result :=  FAgente.AgregaColumna( TransfomaParams( sFormula,  FCountParams, FAgente.Parametros ),
                                                 FALSE,
                                                 enNinguno,
                                                 eTipo,
                                                 255,
                                                 sAlias,
                                                 ocAutomatico,
                                                 FALSE,
                                                 iBanda );
          end;
     end
end;

function TQRReporte.AgregaGrupo( const sFormula : string ) : string;
 var i: integer;
begin
     Result := sFormula;
     if sFormula>'' then
     begin
          i := FAgente.AgregaColumna( TransfomaParams( sFormula, FCountParams,FAgente.Parametros ),
                                      FALSE,
                                      enNinguno,
                                      tgAutomatico,
                                      30,
                                      '',
                                      ocAutomatico,
                                      TRUE );
          if i >=0 then Result := GetNColumna( i )
     end;
end;

procedure TQRReporte.RotateImage( oPicture : TImageEnView;
                                  iRotacion : integer );
var
   j: integer;
begin
     if ( oPicture <> Nil ) AND ( iRotacion <> 0 ) then
     begin
          for j:=1 to 4-iRotacion do
              oPicture.Proc.Rotate(-90);

     end;
end;

function FindUniqueComponentName(Owner: TComponent; Name: String): String;
 var
    i: Integer;
begin
     i := 0;
     Repeat Inc(i);
     Until Owner.FindComponent( Name + IntToStr(i) ) = NIL;
     Result:= Name + IntToStr( i );
end;

procedure TQRReporte.AgregaImagen( oExpr : TQRDesignImage );
var
   sArchivo : string;
{$ifdef HTTP_CONNECTION}
   BlobLogo: TBlobField;
{$endif}
begin
     with oExpr do
     begin
          try
             if StrVacio( Name ) then
                Name := FindUniqueComponentName( Owner, 'qrImagen' );

             sArchivo := Trim(FileName);
             if sArchivo > ''  then
             begin
                  if ExtractFileDir( sArchivo ) = '' then
                  begin
                       oExpr.Picture := NIL;
                       sArchivo := fDirDefault + sArchivo;
                  end;
             end;

             {$ifdef HTTP_CONNECTION}
             if ( ClientRegistry.TipoConexion = conxHTTP ) then
             begin
                  BlobLogo := dmReportes.GetLogo( sArchivo );
                  oExpr.Picture.Assign(BlobLogo);
             end
             else
             {$endif}
             begin
                  if SysUtils.FileExists( sArchivo ) then   // Si no la encuentra no carga el archivo y se deja lo que traiga la plantilla
                     oExpr.Picture.LoadFromFile(sArchivo);
             end;
             // Revisar si procede rotar la imagen en cuyo caso se requiere que sea tipo Bitmap
             if ( oExpr.Tag <> 0 ) then
             begin
                  if ( oExpr.Picture.Bitmap.Empty ) then
                  begin
                       oExpr.Tag := 0;
                       oExpr.Picture := ImageVaciaBMP.Picture;
                  end
                  else
                  begin
                       dbImage.IO.IEBitmap.Assign( oExpr.Picture.Bitmap );
                       RotateImage( dbImage, oExpr.Tag );
                       oExpr.Picture.Bitmap.Assign( DBImage.IO.IEBitmap.VclBitmap );
                  end;
             end;
             {
             //Con ayuda de un componente TImage convertimos la imagen JPG a Bitmap.
             ImageJPGtoBitmap.Picture.Bitmap.Assign(oExpr.Picture.Graphic);
             if not (ImageJPGtoBitmap.Picture.Bitmap.Empty) then
             begin
                dbImage.Bitmap.Assign(ImageJPGtoBitmap.Picture.Bitmap);
                RotateImage( dbImage, oExpr.Tag );
                oExpr.Picture := NIL;
                oExpr.Picture.Bitmap.Assign(dbImage.Bitmap);
             end
             else
             begin
                oExpr.Picture := ImageVaciaBMP.Picture;
             end;
             }
          except
                oExpr.Picture := NIL;
          end;
     end;
end;

procedure TQRReporte.DecodeMemo( oBanda : TQrCustomBand; oMemo : TQrMemo; const sFind : string; const lAgrega : Boolean;  const iBanda : integer );
var
  X     : Integer;
  E,S, sValor   : String;
  P,P2, j  : Integer;
  oExpr : TQRDesignDBText;
begin
     sValor := StrTransAll( Trim(oMemo.Lines.Text), '\', '' );
     if StrLleno( sValor ) then
     begin
          if EsFormulaSQLTexto(Copy( sValor,1,Length(K_SELECT_TEXTO)) ) then
          begin
               sValor := K_SELECT + Copy( sValor,Length(K_SELECT_TEXTO)+1,Length(sValor)-1);
               oExpr := TQRDesignDBText( oBanda.AddPrintable( TQRDesignDBText ) );
               with oExpr do
               begin
                    Dataset := fQReport.DataSet;
                    Tag := FAgente.AgregaColumna( TransfomaParams( sValor, FCountParams, FAgente.Parametros ),
                                                                        FALSE,
                                                                        enNinguno,
                                                                        tgMemo,
                                                                        255,
                                                                        '',
                                                                        ocAutomatico,
                                                                        FALSE,
                                                                        iBanda );
                    AutoSize := FALSE;
                    Left := oMemo.Left;
                    Top := oMemo.Top;
                    Width := oMemo.Width;
                    GroupID := oMemo.Width;
                    Transparent := TRUE;
                    Mask := '';
                    AutoStretch := oMemo.AutoStretch;
                    Alignment := oMemo.Alignment;
                    AlignToBand := oMemo.AlignToBand;
                    Font := oMemo.Font;
                    Color := oMemo.Color;
                    ParentFont := oMemo.ParentFont;
                    WordWrap := oMemo.WordWrap;
                    oMemo.Lines.Text := VACIO;
                    oMemo.Visible := FALSE;
                    //oMemo.Free;
               end
          end
          else
          begin
               FHayMemo:= TRUE;
               oBanda.BeforePrint:= BeforePrintBanda;
               if (sFind = '&&') AND (oMemo.Alignment = taRightJustify) then
               begin
                    oMemo.Alignment := taLeftJustify;
                    oMemo.Tag := 1;
                    //FListaMemo.Add(oMemo);
                    oMemo.Font.Name := 'Courier New';
                    oBanda.Font := oMemo.Font;
               end;
               with oMemo do
                    If Lines.Count>0 Then
                       For X:=0 to Lines.Count-1 Do
                       begin
                            S:=Lines[X];
                            Repeat
                                  P:=Pos(sFind,S);
                                  If P<>0 Then
                                  begin
                                       System.Delete(S,P,length(sFind));
                                       P2:=Pos(sFind,S);
                                       If P2=0 Then P2:=Length(S)+1
                                       Else System.Delete(S,P2,length(sFind));
                                       E:=Copy(S,P,P2-P);
                                       System.Delete(S,P,P2-P);
                                       if lAgrega then
                                       begin
                                            j := FAgente.AgregaColumna( TransfomaParams( E, FCountParams, FAgente.Parametros ),
                                                                        FALSE,
                                                                        enNinguno,
                                                                        tgAutomatico,
                                                                        255,
                                                                        '',
                                                                        ocAutomatico,
                                                                        FALSE,
                                                                        iBanda );
                                            sValor := '&&'+IntToStr(j)+'&&';
                                       end
                                       else
                                       begin
                                            with FAgente.GetColumna(StrToInt(E)) do
                                            begin
                                                 if EsConstante then
                                                    sValor := FormateaValor( ValorConstante, '', TipoFormula)
                                                 else sValor := GetNColumna( StrToInt(E) );
                                                 sValor := '%%'+sValor+'%%';
                                            end;
                                       end;
                                       System.Insert(sValor,S,P);
                                       Lines[X]:=S;
                                  end;
                            Until P=0;
                       end;
          end;
     end;
end;

{Function TQRReporte.ResizeImageKeepProportions( PImageEn: TImageEn; NewDimension : integer; IsWidth : Boolean): Boolean;
Var
  inWidthOld   : integer;
  inWidthNew   : integer;
  inHeightOld  : integer;
  inHeightNew  : integer;
  Bitmap       : TBitmap;
  boStretch    : Boolean;
begin
  boStretch:= PMultiImage.StretchRatio;
  Bitmap       := TBitmap.Create;
  Try
    Try
      //Set Stretch Off
      PMultiImage.StretchRatio := False;
      //Create a new bitmap and set its size
      inWidthOld  := PMultiImage.Picture.Bitmap.Width;
      inHeightOld := PMultiImage.Picture.Bitmap.Height;
      If IsWidth Then
      Begin
           inWidthNew  := NewDimension;
           inHeightNew := (inHeightOld * inWidthNew) div inWidthOld;
      End
      Else
      Begin
           inHeightNew := NewDimension;
           inWidthNew  := (inWidthOld * inHeightNew) div inHeightOld;
      End;
      Bitmap.Width  := inWidthNew;
      Bitmap.Height := inHeightNew;
      //Copy the palette
      Bitmap.Palette:=PMultiImage.Picture.Bitmap.Palette;
      //Delete the lines needed to shrink
      SetStretchBltMode(Bitmap.Canvas.Handle,STRETCH_DELETESCANS);
      //Resize it
      Bitmap.Canvas.Copyrect(Rect(0,
                                 0,
                                 inWidthNew,
                                 inHeightNew),
                            PMultiImage.Picture.Bitmap.Canvas,
                            Rect(0,
                                 0,
                                 PMultiImage.Picture.Bitmap.Width,
                                 PMultiImage.Picture.Bitmap.Height));
      //Copy the palette
      Bitmap.Palette:=PMultiImage.Picture.Bitmap.Palette;
      //Assign the new smaller bitmap
      PMultiImage.Picture.Bitmap.Assign(Bitmap);
      //Free the bitmap

      Result := True;
    Except
      Result := False;
    End;
  Finally
    Bitmap.Free;
    PMultiImage.StretchRatio := boStretch;
  End;
end;

function TQRReporte.ResizeImageBestFit( PMultiImage  : TPDBMultiImage; iHeight, iWidth : integer): Boolean;
Var
  inWidthOld   : Integer;
  inHeightOld  : Integer;
  boStretch    : Boolean;
  IsWidth      : Boolean;
  NewDimension : Integer;
begin
     boStretch := PMultiImage.StretchRatio;
     Try
        Try
           PMultiImage.StretchRatio := False;
           inWidthOld  := PMultiImage.Picture.Bitmap.Width;
           inHeightOld := PMultiImage.Picture.Bitmap.Height;
           IsWidth     := (((inHeightOld * Width) div inWidthOld)<=Height);
           If IsWidth Then
           Begin
                NewDimension := iWidth;
           End
           Else
           Begin
                NewDimension := iHeight;
           End;
           Result := ResizeImageKeepProportions(  PMultiImage  ,  //PMultiImage  : TPMultiImage;
                                                  NewDimension ,  //NewDimension : Integer;
                                                  IsWidth      ); //IsWidth      : Boolean): Boolean;
         Except
               Result := False;
         End;
     finally
            PMultiImage.StretchRatio := boStretch;
     end;
end; }

{$warnings OFF}
procedure TQRReporte.AgregaDBImagen( oBanda : TQrCustomBand;
                                     oExpr : TQRDesignDBImage );
var
   i : integer;
   oImage : TQRDesignImage;
   oImageEn : TImageEnView;
begin
     FHayImagenes := TRUE;
     oBanda.BeforePrint:= BeforePrintBanda;
     // TQRDesignImage
     oImage := TQRDesignImage.Create( SELF );
     oImage.Parent := oBanda;
     FListaImagenes.Add(oImage);
     // TImageEnView
     oImageEn := TImageEnView.Create(self);
     oImageEn.Visible := FALSE;
     oImageEn.LegacyBitmap := FALSE;
     FListaImagenesEn.Add( oImageEn );

     {$ifdef TRESS}
     {Esta columna, es la que va a regresar la foto. En el Servidor se regresa en el
     evento OnEvaluaColumna}
     If ( TQRDesignDBImage( oExpr ).DataField <> K_IMAGEN_MAESTRO ) then
     begin
          i := FAgente.AgregaColumna( '@0', FALSE, enNinguno, tgMemo, 0, K_IMAGEN_EMPLEADO_X + IntToStr(FListaImagenes.Count) );
          with FAgente.GetColumna(i) do
               ValorConstante := TQRDesignDBImage( oExpr ).DataField;
          {Esta segunda columna es para que en el servidor se sepa que tipo de Foto se esta pidiendo}
          //FAgente.AgregaColumna( EntreComillas(TQRDesignDBImage( oExpr ).DataField), FALSE, enNinguno, tgTexto, 10 );
          if not FEmpleadoAgregado then
          begin
               FAgente.AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 10, Q_EMPLEADO_FOTO );
               FEmpleadoAgregado:= TRUE;
          end;
     end
     else
     begin
          i := FAgente.AgregaColumna( '@0', FALSE, enNinguno, tgMemo, 0, K_IMAGEN_MAESTRO_X + IntToStr(FListaImagenes.Count)  );
          if not FMaestroAgregado then
          begin
               FAgente.AgregaColumna( 'MAESTRO.MA_CODIGO', TRUE, enMaestros, tgTexto, 6, K_MAESTRO_MA_CODIGO );
               FMaestroAgregado:= TRUE;
          end;
     end;
     {$endif}

     {$ifdef SELECCION}
     i := FAgente.AgregaColumna( '@SOLICITA.SO_FOTO', FALSE, enNinguno, tgMemo, 0 );
     {$endif}
     {$ifdef VISITANTES}
     i := FAgente.AgregaColumna( '@VISITA.VI_FOTO', FALSE, enNinguno, tgMemo, 0, K_VISITA_VI_FOTO );
     if not FVisitanteAgregado then
     begin
          FAgente.AgregaColumna( 'VISITA.VI_NUMERO', TRUE, enVisita, tgTexto, 6, K_VISITA_VI_NUMERO );
          FVisitanteAgregado:= TRUE;
     end;
     {$endif}
     {***ANTERIOR***}
     {with DBImageTemp.DataBinding do
     begin
          DataSource := QRDataSource;
     end;}
     oImage.Left := oExpr.Left;
     oImage.Top := oExpr.Top;
     oImage.GroupId := oExpr.Tag;
     if oImage.GroupId in [1,3] then
     begin
          oImage.Width := oExpr.Height;
          oImage.Height := oExpr.Width;
     end
     else
     begin
          oImage.Width := oExpr.Width;
          oImage.Height := oExpr.Height;
     end;
     oImage.Hint := PadL(IntToStr(oImage.Left),5)+','+
                    PadL(IntToStr(oImage.Top),5)+','+
                    PadL(IntToStr(oImage.Width),5)+','+
                    PadL(IntToStr(oImage.Height),5);
     oImage.Stretch := TQRDesignDBImage( oExpr ).Stretch;
     oImage.AutoSize := FALSE;

     oExpr.Left := 0;
     oExpr.Width := 0;
     oExpr.Height := 0;
     oImage.Tag := i;

     TQRDesignDBImage( oExpr ).DataField := '';
     TQRDesignDBImage( oExpr ).DataSet := NIL;
end;

{$warnings ON}

procedure TQRReporte.DecodeRichText( oBanda : TQrCustomBand;
                                     oExpr : TQRDesignRichtext;
                                     const sFind, sReplace : string;
                                     const lAgrega : Boolean;
                                     const iAncho,iBanda : integer );
 var i,i2,j{$IFDEF TRESS_DELPHIXE5_UP},iLenBuffer {$ENDIF}: Integer;
     sTexto : string;//String[255];
     E {$IFDEF TRESS_DELPHIXE5_UP}: PChar{$ELSE}String[255]{$ENDIF};
     RichEdit : TRichEdit;
begin
     RichEdit := NIL;
     if oExpr.ParentRichEdit <> NIL Then
        RichEdit := oExpr.ParentRichEdit
     else
     begin
          for i:= 0 to oExpr.ControlCount - 1 do
              if oExpr.Controls[ i ] is TQRRichEdit Then
              begin
                   RichEdit := TRichEdit( oExpr.Controls[ i ] ); // ParentRichEdit
                   Break;
              end;
     end;

     if RichEdit <> NIL then
     begin
          try
             repeat
                   I:=RichEdit.FindText(sFind,0,Length(RichEdit.Text)-1,[stMatchCase]);
                   If I>=0 Then
                   begin
                        RichEdit.SelStart:=I;
                        RichEdit.SelLength:=length(sFind);
                        RichEdit.SelText := sReplace;

                        I2:=RichEdit.FindText(sFind,0,Length(RichEdit.Text),[stMatchCase]);
                        If I2>=0 Then
                        begin
                             RichEdit.SelStart:=I2;
                             RichEdit.SelLength:=length(sFind);
                             RichEdit.SelText := sReplace;
                             RichEdit.SelStart:=I+iAncho;
                             RichEdit.SelLength:=I2-I-iAncho;
                             {***(@am):Esta linea aseguraba que el nombre no tenga mas de 256 caracteres. Y almacenaba el numero de caracteres
                                       copiados en la primera posicion del buffer. Sin embargo, por cambios en sintaxis y en tipos de dato ya no es
                                       posible realizarlo de la misma forma. El codigo que sustituye esta linea se integro dentro de la condicion
                                       que valida que solo se ejecute cuando se agregan columnas.***}
                             {$IFNDEF TRESS_DELPHIXE5_UP}
                                E[0]:=Chr(RichEdit.GetSelTextBuf(@E[1],256));
                             {$ENDIF}
                             // BINGO !!!!!
                             //FALTA SUSTITUIR LAS COLUMNAS
                             {El diccionario puede hacer el trabajo}

                             if lAgrega then
                             begin
                                  {$IFDEF TRESS_DELPHIXE5_UP}
                                    sTexto := RichEdit.SelText;
                                  {$ENDIF}
                                  j := FAgente.AgregaColumna( TransfomaParams( {$IFDEF TRESS_DELPHIXE5_UP}string(sTexto){$ELSE}E{$ENDIF},FCountParams,FAgente.Parametros ),
                                                              FALSE,
                                                              enNinguno,
                                                              tgAutomatico,
                                                              255,
                                                              '',
                                                              ocAutomatico,
                                                              FALSE,
                                                              iBanda );
                                  RichEdit.SelText := IntToStr(j);
                             end
                             else
                             begin
                                 {$IFDEF TRESS_DELPHIXE5_UP}
                                    sTexto := RichEdit.SelText;
                                 {$ENDIF}
                                  with FAgente.GetColumna(StrToInt(string(sTexto))) do
                                  begin
                                       if EsConstante then
                                          RichEdit.SelText := FormateaValor( ValorConstante, '', TipoFormula)
                                       else
                                           RichEdit.SelText := GetNColumna( StrToInt(string(sTexto)) );
                                  end;
                             end;

                        end;
                   end;
             until I<0;
          except
          end;

          {if (sFind ='&&') AND (oExpr.Alignment = taRightJustify) then
          begin
               FHayRichText := True;
               oBanda.BeforePrint:= BeforePrintBanda;
               oExpr.Alignment := taLeftJustify;
               oBanda.Font.Size := oExpr.Font.Size;
               FRichText := oExpr;
          end;}
     end;
end;


function TQRReporte.AnchoTexto(const iSize, iWidth : integer) : Integer;
begin
    with FParentRichText do
    begin
         WordWrap := FALSE;
         Width := iWidth;
         SelStart := 0;
         SelLength := 0;
         Font.Size := iSize;
    end;
    with RichEdit3 do
    begin
         WordWrap := FALSE;
         Width := iWidth;
         Clear;
         Font.Size := iSize;
         WordWrap := TRUE;
         // Agrega caracteres hasta que se pase a la siguiente l�nea
         while Lines.Count <= 1 do
             Text := Text + '@';
         // El ancho es un caracter menos
         Result := Length( Text )-1;
         WordWrap := FALSE;
         Clear;
    end;
end;

procedure TQRReporte.Justifica1;
begin
    with FParentRichText do
    begin
         WordWrap := FALSE;
         SelectAll;
         Font.Name := 'Courier New';
         SelAttributes.Name := 'Courier New';
         WordWrap := TRUE;
         SelStart := 0;
         SelLength := 0;
    end;

    with FJustifica do
    begin
        AnchoControl := FParentRichText.Width; // Caracteres que caben en el control, con el Font dado.
        Justifica( FParentRichText );
    end;
end;


procedure TQRReporte.JustificaRichText( const iSize, iWidth : integer;
                                        oRichEdit : TRichEdit;
                                        oQrRich : TQRRichtext;
                                        const lRich : Boolean);
var
    i, nAncho, nMaximo, nPos, nInicial, nPosRow : Integer;
    nEspacios, nQuedan, nVueltas, nAnchoOriginal : Integer;
    sLinea : String;
    MS: TMemoryStream;
begin
     if lRich then
     begin
          MS:=TMemoryStream.Create;

          with oRichEdit do
          begin
               Lines.SaveToStream(MS);
               {SelectAll;
               CopyToClipBoard;}
          end;

          with RichParent do
          begin
               Clear;
               MS.Seek(0,0);
               Lines.LoadFromStream(MS);
               {PasteFromClipBoard;}
          end;
          Ms.Free;
          TQrDesignRichText(oQrRich).ParentRichEdit := RichParent;
     end;

     FParentRichText := RichParent;
     nMaximo := AnchoTexto(iSize,iWidth);    // Siempre dejan un espacio al final
     Justifica1;
     with FParentRichText do
     begin
         nInicial := 0;
         for i := 0 to Lines.Count-1 do
         begin
             sLinea := Lines[ i ];
             nAnchoOriginal := Length( sLinea );
             nAncho := Length( Trim( sLinea ));
             // Si termina con CR_LF, no se justifica
             if FindText( Chr(13)+Chr(10), nInicial, nAnchoOriginal+2, [] ) > 0 then
                 nEspacios := 2
             else
             begin
                 nEspacios := 0;
                 nVueltas  := 1;
                 // Justifica hasta que quede del ancho m�ximo
                 while ( nAncho < nMaximo ) do
                 begin
                     nQuedan := nAncho + nEspacios;
                     // Si ya no hay espacios, termina
                     if FindText( ' ', nInicial, nQuedan, [] ) < 0 then
                         break;

                     nPosRow := nInicial;
                     while ( nAncho < nMaximo ) do
                     begin
                       nPos    := FindText( ' ', nPosRow, nQuedan, [] );
                       if ( nPos >= 0 ) then
                       begin
                           SelStart := nPos;
                           SelLength := 1;
                           SelText := '  ';
                           nAncho := nAncho + 1;
                           nQuedan := nQuedan - ( nPos - nPosRow ) - 2;
                           nPosRow := nPos + nVueltas + 1;
                           nEspacios := nEspacios + 1;
                       end
                       else
                         break;
                     end;
                 end;
             end;
             nInicial := nInicial + nAnchoOriginal + nEspacios;
         end;
      end;
end;

procedure TQRReporte.JustificaMemo( oMemo : TQrMemo );
     function AjustaLinea(sTexto: string; const nMaximo : integer ) : String;
      var iPos : integer;
     begin
          Result := Trim(sTexto);
          if Length(Result)< nMaximo then
          begin
               sTexto := Trim(sTexto);
               Result := '';
               if Pos(' ', sTexto) = 0 then
               begin
                    Result := sTexto;
                    Exit;
               end;
               while (Length(Result)+ Length(sTexto))< nMaximo do
               begin
                    iPos := Pos(' ', sTexto);
                    if iPos > 0 then
                    begin
                         Result := Result + Copy(sTexto, 1, iPos ) + ' ';
                         sTexto := Copy(sTexto, iPos+1, Length(sTexto) );
                    end
                    else
                    begin
                         sTexto := Result + sTexto;
                         Result := '';
                    end;
               end;
               if sTexto <> '' then
                  Result := Result + sTexto;
          end;
     end;

var
    iPos, nMaximo : Integer;
    sTexto, sTexto2,sTexto3 : string;
begin
     with Memo1 do
     begin
          Width := oMemo.Width;
          Font.Size := oMemo.Font.Size;
          Font.Name := oMemo.Font.Name;
          nMaximo := Length(lines[0]);
     end;

     sTexto := oMemo.Lines.Text;
     oMemo.Lines.Clear;

     iPos := Pos( CHR(13)+ CHR(10), sTexto );
     while iPos > 0  do
     begin
          sTexto2 := WrapText(Copy(sTexto, 1, iPos +1 ),nMaximo);
          sTexto := Copy(sTexto, iPos+2 , MaxInt );


          iPos := Pos( CHR(13)+ CHR(10), Trim(sTexto2) );
          if iPos = 0 then
             oMemo.lines.Add( Trim(sTexto2) )
          else
          begin
               while iPos > 0  do
               begin
                    sTexto3 := Copy(sTexto2, 1, iPos +1 );
                    sTexto2 := Copy(sTexto2, iPos+2 , MaxInt );
                    if sTexto2 <> '' then
                       oMemo.lines.Add(AjustaLinea(sTexto3,nMaximo))
                    else oMemo.lines.Add(Trim(sTexto3));
                    iPos := Pos( CHR(13)+ CHR(10), sTexto2 );
               end;
          end;
          iPos := Pos( CHR(13)+ CHR(10), sTexto );
     end;
end;


procedure TQRReporte.BeforePrintMemo(oMemo : TQRDesignMemo);
begin
     try
        with oMemo do
        begin
             if Tag = 1 then
             begin
                  JustificaMemo( oMemo );
             end;
        end;
     finally
     end;
end;

procedure TQRReporte.BeforePrintRichText(oRichEdit : TRichEdit);
begin
     JustificaRichText( FRichText.Font.Size,
                        FRichText.Width,
                        oRichEdit,FRichText, True);
end;

procedure TQRReporte.BeforePrintBanda(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
     PrintBand := TRUE;

     if FHayImagenes then BeforePrintImage;

     if FHayRichText then
     begin
          if FParentRichText <> NIL then
             FRichText.ParentRichEdit := NIL;
     end;
end;

procedure TQRReporte.BeforePrintImage;
var
   oImage : TQRDesignImage;
   oImageEn : TImageEnView;
   iAnchoImage, iAltoImage, iTopImage, iLeftImage, i : integer;
   oColumna: TSQLColumna;
   sFieldName: String;
   //oFotoStream: TStream;
   sMsgError: String;

   procedure BestFit;
   var
      iTemp : integer;
      iFactor,iFactor2: Real;
   begin
        iFactor2 := ( iAnchoImage / iAltoImage );
        iFactor := ( oImageEn.IO.IEBitmap.VclBitmap.width / oImageEn.IO.IEBitmap.VclBitmap.Height );
        if ( oImage.GroupId in [1,3] )  then
        begin
             if ( iFactor2 < 1 ) then
                iFactor2 := ( iAltoImage / iAnchoImage );
             iTemp := iAnchoImage;
             iAnchoImage := iAltoImage;
             iAltoImage:= iTemp;
        end;
        if ( iFactor > iFactor2 ) then
        begin
             with oImage do
             begin
                  //El Ancho queda Igual
                  Width := iAnchoImage;
                  Height := Trunc(iAnchoImage/iFactor);
                  Left := iLeftImage;
                  Top := iTopImage + Trunc((iAltoImage - Height)/2);
             end;
        end
        else
        begin
             with oImage do
             begin
                  //El Alto queda Igual
                  Height := iAltoImage;
                  Width := Trunc(iAltoImage*iFactor);
                  Top := iTopImage;
                  Left := iLeftImage + Trunc((iAnchoImage - Width)/2);
             end;
        end;
   end;

begin
     for i:=0 to FListaImagenes.Count - 1 do
     begin
          oImage := TQRDesignImage(FListaImagenes[i]);
          if ( not oImage.Picture.Bitmap.Empty ) then
          begin
               //oImage.Picture.Bitmap.Dormant;
               oImage.Picture.Bitmap.FreeImage;
               oImage.Picture.Bitmap.ReleaseHandle;
          end;
          oImage.Picture := nil;

          oImageEn := TImageEnView( FListaImagenesEn[i] );
          oImageEn.Clear;
          oImageEn.Blank;

          oColumna := FAgente.GetColumna(oImage.Tag);

          if oColumna <> NIL then
             sFieldName := StrDef( oColumna.Alias, GetNColumna(oImage.Tag) );

          if ( not FQReport.DataSet.FieldByName(sFieldName).IsNull ) then
          begin
               iLeftImage := StrToInt(Copy(oImage.Hint,1,5));
               iTopImage := StrToInt(Copy(oImage.Hint,7,5));
               iAnchoImage := StrToInt(Copy(oImage.Hint,13,5));
               iAltoImage := StrToInt(Copy(oImage.Hint,19,5));

               FToolsImageEn.AsignaBlobAImagen( oImageEn, QRDataSource.DataSet, sFieldName );

               if ( oImage.GroupId <> 0 ) then
                  RotateImage( oImageEn, oImage.GroupId );

               FToolsImageEn.ResizeImagen( oImageEn, sMsgError );
               oImage.Picture.Assign( oImageEn.IO.IEBitmap.VclBitmap );
               BestFit;
{
               FToolsImageEn.AsignaBlobAImagen( ImageEnView1, QRDataSource.DataSet, sFieldName );
               oImage.Picture.Assign( ImageEnView1.Bitmap );
               FreeAndNil( ImageEnView1 );
               oFotoStream := QRDataSource.DataSet.CreateBlobStream( QRDataSource.DataSet.FieldByName( sFieldName ), bmRead );
               try
                  DBImage.IO.LoadFromStream( oFotoStream );
               finally
                      FreeAndNil( oFotoStream );
               end;
               //Rotacion de imagen del empleado
               try
                  RotateImage( DBImage, oImage.GroupId );
                  oImage.Picture.Assign( DBImage.Bitmap );
                  BestFit;
               finally
                      DBImage.Bitmap.FreeImage;
               end;
}
          end;
     end;
end;

procedure TQRReporte.ShowPreview( const lGrafica : Boolean );
begin
     {$IFDEF KIOSCO2}
     dmCliente.QrPreview.QrPrinter := FQReport.QRPrinter;
     dmCliente.QrPreview.QrPrinter.ShowingPreview := FALSE;
     {$ELSE}
     FQReport.Zoom := 100;
     {$ifndef TRESS_REPORTES}
     with TPreview_DevEx.Create( self ) do
     begin
        QRPreview.Zoom := 100;
        Caption := fNombre;
        QRPrinter := FQReport.QRPrinter;
        Reporte := FQReport;
        QRPreview.QRPrinter := FQReport.QRPrinter;
        WindowState := wsMaximized;
        ShowGrafica := lGrafica;
     end;
     {$endif}
     {$ENDIF}
end;

procedure TQRReporte.FQReportPreview(Sender: TObject);
begin
     ShowPreview(FALSE);
end;

procedure TQRReporte.Init( oAgente : TSQLAgenteClient;
                           oDatosImpresion : TDatosImpresion;
                           iCountParametros: Integer;
                           var lHayImagenes : Boolean  );
begin
     ZFuncsCliente.ImprimeGrid := FALSE;

     if FListaImagenes = NIL then FListaImagenes := TList.Create
     else FListaImagenes.Clear;

     if FListaImagenesEn = NIL then FListaImagenesEn := TList.Create
     else FListaImagenesEn.Clear;

     FJustifica := TJustificadorRTF.Create;

     FCountParams := iCountParametros;
     FCountImagen := 0;
     FAsignaMedidas := TRUE;
     FHayImagenes := FALSE;
     FHayRichText := FALSE;
     FMostrandoPrewiew := TRUE;
     FEmpleadoAgregado := FALSE;
     FMaestroAgregado := FALSE;
     FDatosImpresion := oDatosImpresion;
     fDirDefault := zReportTools.DirPlantilla;
     CargaReporte( FDatosImpresion.Archivo );
     {.$ifdef QR362}
     FQRDataSource := TDataSource.Create( self );
     {.$endif}
     FAgente := oAgente;
     RevisaControles;
     AgregaFormulas;

     lHayImagenes := FHayImagenes;

end;

function TQRReporte.GeneraForma( const sNombre : string;
                                 const EsPreview : Boolean;
                                 oDataSet : TDataSet ):Boolean;
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crDefault;

     {***(@am): Anteriormente se deshabilitaba y habilitaba el dataset desde que se invoca GeneraReporte. Lo cual
                no nos permitia mostrar imagene en los gafettes pues el nuevo control (ImageEn) necesita la notificacion
                de que hubo un cambio.***}
     //oDataSet.DisableControls;
     {***************************}

     Result := TRUE;

     fNombre := sNombre;
     with fQReport do
     begin
          Description.Text := fNombre;
          Dataset := oDataset;

          ReportTitle := fNombre;
     end;
     QRDataSource.DataSet := oDataSet;

     if (FAgente <> NIL) then {El Agente es NIL si es Impresion de Grid}
        GetExpresion;

     ParametrosImpresion;

      {***(@am): Anteriormente se deshabilitaba y habilitaba el dataset desde que se invoca GeneraReporte. Lo cual
                no nos permitia mostrar imagene en los gafettes pues el nuevo control (ImageEn) necesita la notificacion
                de que hubo un cambio.***}
     //oDataset.EnableControls;
      {***************************}

     FEsPreview := EsPreview;
     if FEsPreview then Preview
     else Imprime;
     Termina;

     Screen.Cursor := oCursor;
end;

procedure TQRReporte.ParametrosImpresion;
//const ES_DUPLEX = 1;
begin
     with fQReport.PrinterSettings, FDatosImpresion do
     begin
          FirstPage := PagInicial;
          LastPage := PagFinal;
          Copies := iMax(Copias,1);
          PrinterIndex := Impresora;
          {if fQReport.Tag = ES_DUPLEX then
            Duplex := TRUE;  }
     end;
end;

procedure TQRReporte.Imprime;
 function EstaEnUso : Boolean;
 begin
      with FDatosImpresion do
      begin
           Result := FileExists(Exportacion);
           if Result then
              Result := NOT SysUtils.DeleteFile(Exportacion);

           if Result then
           begin
                ZError( 'Exportando...', Format( 'El archivo " %s " no pudo ser exportado .' + CR_LF + CR_LF +
                                         'Probablemente alguna aplicaci�n lo est� usando.', [ ExtractFileName(Exportacion) ] ), 0 );
           end
      end;
 end;
begin
     try
        FQReport.DataSet.DisableControls;
        try
           if FDatosImpresion.Tipo <> tfImpresora then
              FQReport.Prepare;

           case FDatosImpresion.Tipo of
                tfImpresora: fQReport.Print;
                tfQRP: fQReport.QRPrinter.Save( FDatosImpresion.Exportacion );
                tfXLSQR:
                begin
                     if NOT EstaEnUso then
                     begin
                          with FDatosImpresion do
                          begin
                               {$IFDEF TRESS_REPORTES}
                               fQReport.ShowProgress := FALSE;
                               {$ENDIF}

                               fQReport.ExportToFilter( TQRXLSFilter.Create( Exportacion ) );

                               {$IFDEF TRESS_REPORTES}
                               if TRIM (Copia) <> VACIO then
                               begin
                                    // fQReport.ExportToFilter( TQRXLSFilter.Create( Copia ) );
                                    CopyFile(PChar (Exportacion), PChar (Copia), TRUE);
                               end;
                               {$ENDIF}

                               {$ifndef TRESSEMAIL}
                               {$ifndef TRESSWEB}
                               ZReportDialog.MuestraDialogo( 'Reporte Exportado', Format( 'El Archivo " %s " fue Exportado con Exito' + CR_LF +
                                                             '� Desea Verlo ?', [ ExtractFileName( Exportacion ) ] ), Exportacion );
                               {$endif}
                               {$endif}
                          end;
                     end;
                end
                else
                begin
                     if NOT EstaEnUso then
                     begin
                          try
                             dmExportEngine := TdmExportEngine.Create( Self );

                             with FDatosImpresion do
                             begin
                                  dmExportEngine.Exporta( FQReport, Tipo, Exportacion{$ifdef XMLREPORT}, FDefaultStream{$endif});
                                  // dmExportEngine.Exporta( FQReport, Tipo, Exportacion, FDefaultStream);

                                  {$IFDEF TRESS_REPORTES}
                                  if TRIM (Copia) <> VACIO then
                                  begin
                                       // dmExportEngine.Exporta( FQReport, FDatosImpresion.Tipo, Copia);
                                       CopyFile(PChar (Exportacion), PChar (Copia), TRUE);
                                  end;
                                  {$ENDIF}
                             end;

                          finally
                                 FreeAndNil( dmExportEngine );
                          end;
                     end;
                end;
           end;
        finally
               {$ifdef TressEmail}
               with FQReport do
               begin
                    //Assert(FQReport.qrPrinter = NIL, 'Objeto qrPrinter es NIL. Unidad ZQRReporte');
                    if FQReport.qrPrinter <> NIL then
                       FPageCount := FQReport.qrPrinter.PageCount;
                    QRPrinter.Free;
                    QRPrinter := NIL;
               end;
               {$else}
               fQReport.QRPrinter.Free;
               fQReport.QRPrinter := nil;
               {$endif}


        end;
     finally
            FQReport.DataSet.EnableControls;
     end;

end;

procedure TQrReporte.FormDestroy(Sender: TObject);
var
   aDir : array[0..255] of char;
   FFile: TSearchRec;
begin
     //Se borran los archivos temporales creados por el QuickReport y QrDesign
     try
        GetTempPath(255, adir);
        if FindFirst(aDir+'QRP*.TMP', faArchive, FFile) = 0 then
        begin
             DeleteFile(aDir+Ffile.Name);
             while FindNext(FFile) = 0 do
                   DeleteFile(aDir+Ffile.Name)
        end;
        FindClose(FFile);
     finally
            if Assigned( FListaImagenes ) then
               FListaImagenes.Clear;
            FListaImagenes.Free;
            if Assigned( FListaImagenesEn ) then
               FListaImagenesEn.Clear;
            FListaImagenesEn.Free;
            FJustifica.Free;
            FQReport.Free;
     end;
end;

{~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*}
{~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*}
{~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*}
                        { TJustificadorRTF }

constructor TJustificadorRTF.Create;
begin
    FLineasMemo  := TStringList.Create;
    FLineasTempo := TStringList.Create;
end;

destructor TJustificadorRTF.Destroy;
begin
    FLineasMemo.Free;
    FLineasTempo.Free;
end;

procedure TJustificadorRTF.Justifica(oRich : TRichEdit);
var
    i, j : Integer;
    LineFinished : Boolean;
    NewLine : string;

  function Justifica( sLinea : String ) : String;
  var
    nDiferencia, nPointer, nPos, nUltimo, nHuecos : Integer;
    nDelta, nSobran, i, nEspacios, nIndex : Integer;
  begin
    sLinea  := Trim( sLinea );
    nUltimo := Length( sLinea );
    nDiferencia := ( FAnchoControl - nUltimo );
    // Si mide el Ancho exacto, no hay nada que hacer
    if ( nDiferencia > 0 ) then
    begin
        nPointer := 1;
        nHuecos  := 0;
        // Coloca sobre primer caracter que no sea espcio
        while ( nPointer <= nUltimo ) and ( sLinea[ nPointer ] = ' ' ) do
            Inc( nPointer );
        while ( nPointer <= nUltimo ) do
        begin
            nPos := Pos( ' ', Copy( sLinea, nPointer, MAXINT ));
            if ( nPos = 0 ) then    // Si ya no hay m�s espacios, termina
                break;
            // Se encontr� un hueco
            nHuecos := nHuecos + 1;
            nPointer := nPointer + nPos;
            FHuecos[ nHuecos ] := nPointer;
            // Coloca sobre primer caracter que no sea espcio
            while ( nPointer <= nUltimo ) and ( sLinea[ nPointer ] = ' ' ) do
                Inc( nPointer );
        end;
        if ( nHuecos > 0 ) then
        begin
            nDelta := nDiferencia div nHuecos;
            nSobran :=  nDiferencia mod nHuecos;
            nUltimo := 0;
            for i := 1 to nHuecos-1 do
            begin
                nEspacios := nDelta;
                // El sobrante de la divisi�n, lo va repartiendo en los huecos
                // hasta que se acabe.
                if ( nSobran > 0 ) then
                begin
                    Inc( nEspacios );
                    Dec( nSobran );
                end;
                nIndex := FHuecos[ i ] + nUltimo;
                Insert( Space( nEspacios ), sLinea, nIndex );
                nUltimo := nUltimo + nEspacios;
            end;
            // El Ultimo hueco toma el resto del sobrante.
            nIndex := FHuecos[ nHuecos ] + nUltimo;
            Insert( Space( nDiferencia-nUltimo ), sLinea, nIndex );
        end;
    end;
    Result := Padr( sLinea, FAnchoControl );
  end;

  procedure FlushLine( const lJustifica : Boolean );
  begin
    if ( lJustifica ) then
        NewLine := Justifica( NewLine );
    FLineasMemo.Add( NewLine );
    NewLine := '';
  end;

  procedure AddWord(aWord : string);
  begin
    if Length( NewLine + aWord ) > FAnchoControl then
    begin
      if NewLine = '' then
      begin
        while ( Length( NewLine ) + 1 ) < FAnchoControl do
        begin
          NewLine := NewLine + copy(aWord, 1, 1);
          Delete(aWord, 1, 1);
        end;
        aWord := '';
      end;
      FlushLine( True );
      if Length(aWord) > FAnchoControl then
      begin
        if NewLine = '' then
            aWord := Copy( aWord, 1, FAnchoControl );

        NewLine := aWord;
        FlushLine( True );
        aWord := '';
      end;
    end;
    NewLine := NewLine + aWord;
  end;

{$WARNINGS OFF}
  procedure AgregaLinea(sLinea : String );
  const BreakChars : set of Char = [' ', #13, '-'];
  var aPos : Integer;
  begin
      while pos(#10, sLinea) > 0 do
        Delete(sLinea, Pos(#10, sLinea), 1);
      aPos := pos(#13, sLinea);
      if aPos > 0 then
      begin
        repeat
          AgregaLinea(copy(sLinea, 1, aPos - 1));
          Delete(sLinea, 1 , aPos);
          aPos := pos(#13, sLinea);
        until aPos = 0;
        AgregaLinea(sLinea);
      end
      else
      begin
        j := 0;
        NewLine := '';
        LineFinished := False;
        while (J < Length(sLinea)) and (Length(sLinea) > 0) do
        begin
          repeat
            inc(J)
          until (CharInSet (sLinea[J], BreakChars)) or (J >= Length(sLinea));
          AddWord(copy(sLinea, 1, J));
          Delete(sLinea, 1, J);
          J := 0;
        end;
        if not LineFinished then
          FlushLine( False );
      end;
  end;

begin   // Justifica
    FLineasMemo.Clear;
    FLineasTempo.Text := oRich.Text;
    LineFinished := False;
    with FLineasTempo do
        for i := 0 to Count-1 do
            AgregaLinea( Trim( Strings[ i ] ));
end;
{$WARNINGS ON}

procedure TJustificadorRTF.SetAnchoControl(const nAncho: Integer);
begin
    FAnchoControl := nAncho;
    FHuecos := VarArrayCreate( [ 1, FAnchoControl ], varInteger );
end;

procedure TQrReporte.FQReportAfterPreview(Sender: TObject);
begin
     if ( fQReport <> NIL ) AND ( fQReport.Dataset <> NIL ) AND ( NOT fQReport.Dataset.Eof ) then
     begin
          if FHayRichText then
             BeforePrintRichText(TRichEdit(Sender));
          if FHayMemo then
             BeforePrintMemo(TQRDesignMemo(Sender));
     end;
end;

{.$ifdef TRESSEMAIL}
function TQRReporte.GetTieneMultiplesHojas : Boolean;
 var oBanda : TQrCustomBand;
     i : integer;
begin
     Result := FALSE;
     for i := 0 to FQReport.BandList.Count - 1 do
     begin
          oBanda := TQrCustomBand(FQReport.BandList[i]);
          //if oBanda.BandType = rbGroupHeader then
          Result := oBanda.ForceNewPage;
          if Result then
             Break;
     end;
end;

{procedure TQRReporte.GeneraQRHtml;
 var oHTML : TQRGHTMLDocumentFilter;
begin
     oHTML := TQRGHTMLDocumentFilter.Create( FDatosImpresion.Exportacion );
     try
        oHTML.TransparentTextBG := FALSE;
        if DirectoryExists( FDirDefault ) then
           oHTML.PictureDir := FDirDefault;
        oHTML.MultiPage := GetTieneMultiplesHojas;
        oHTML.PageLinks := TRUE;
        oHTML.FirstLastLinks := FALSE; // El link hacia la ultima pagina tiene un bug...

        FQReport.ExportToFilter( oHTML );
     finally
            if oHTML.MultiPage then
            begin
                 oHTML.MultiPage := FALSE;
                 oHTML.Finish;
            end;
            oHTML.free;
     end;
end;

procedure TQRReporte.GeneraQRPDF;
 var oPDF : TQRPDFDocumentFilter;
begin
     oPDF := TQRPDFDocumentFilter.Create( FDatosImpresion.Exportacion );
     try
        with oPDF do
        begin
             TextOnTop := True;
             LeftMargin := -10;
             CompressionOn := TRUE;
             AddFontMap( 'Bookman-Old-Style:Times New Roman' );
        end;
        FQReport.ExportToFilter( oPDF );
     finally
            FreeAndNil( oPDF );
     end;
end;}

procedure TQrReporte.AddNombreImagenes( oLista : TStrings );
 var i: integer;
begin
     oLista.Clear;

     for i := 0 to ComponentCount - 1 do
         if Components[ i ].ClassNameIs( 'TQRDESIGNIMAGE' ) OR
            Components[ i ].ClassNameIs( 'TQRIMAGE' ) then
         begin
              oLista.Add( Components[ i ].Name + '.jpg=' + TQrDesignImage( Components[ i ] ).FileName );
         end;
end;


function TQrReporte.UnSoloHTML : Boolean;
begin
     Result := NOT GetTieneMultiplesHojas;
end;

{.$endif}


procedure TQrReporte.FormCreate(Sender: TObject);
begin
     CreaPlantillaInterna;
end;     

procedure TQrReporte.CreaPlantillaInterna;
 var
    oExpr : TQRDesignExpr;
    sArchivo: string;
 const
    K_NOMBRE_LOGO ='LOGO.BMP';
begin
     CreaTodasBandas;
     FQReport.Bands.TitleBand.Height := 81;
     FEmpleadoAgregado:= FALSE;
     FMaestroAgregado:= FALSE;
     with QrImagen do
     begin
          ParentReport := FQReport;
          Parent := FQReport.Bands.TitleBand;
          Height := 50;
          Width := 100;
          Left := 600;
          Top := 4;
          {***DevEx(@am): Anteriormente se inicializaba QrImage.FileName sin problemas pues la propiedad no cargaba realmente la imagen.
                          apartir del 2011, Delphi modifica esta propiedad para que si se cargue  la imagen. Por lo que sera necesario
                          definir correctamente la ruta del archivo en la propiedad File Name para que al cargarlo no marque un error.***}
          {sArchivo := Trim(K_NOMBRE_LOGO);
          if sArchivo > ''  then
          begin
                  if ExtractFileDir( sArchivo ) = '' then
                  begin
                       fDirDefault := zReportTools.DirPlantilla;
                       sArchivo := fDirDefault + sArchivo;
                  end;
                  FileName := sArchivo;
          end; }
     end;

     oExpr := TQRDesignExpr( FQReport.Bands.TitleBand.AddPrintable( TQRDesignExpr ) );
     with oExpr do
     begin
          Left := 8;
          Top := 5;
          Width := 585;
          Height := 23;
          Alignment := taLeftJustify;
          AlignToBand := False;
          AutoSize := False;
          AutoStretch := False;
          Font.Size := 12;
          Font.Name := 'Arial';
          Font.Style := [fsBold];
          ParentFont := False;
          ResetAfterPrint := False;
          Transparent := False;
          WordWrap := False;
          Expression := 'GLOBAL(1)';

      end;

      oExpr := TQRDesignExpr( FQReport.Bands.TitleBand.AddPrintable( TQRDesignExpr ) );
      with oExpr do
      begin
           Left := 8;
           Top := 29;
           Width := 585;
           Height := 31;
           Alignment := taLeftJustify;
           AlignToBand := False;
           AutoSize := False;
           AutoStretch := True;
           Font.Size := 10;
           Font.Name := 'Arial';
           ParentFont := False;
           ResetAfterPrint := False;
           Transparent := False;
           WordWrap := True;
           Expression := 'Reporte(6)';
           FixBottomPosition := False;
           StretchHeightWithBand := True;
      end;
end;

procedure TQrReporte.ZError( const sCaption, sMsg: String; const iHelpCtx: Longint );
begin
     {$IFNDEF TRESS_REPORTES}
     ZetaDialogo.ZError(sCaption, sMsg, iHelpCtx);
     {$ELSE}
     dmReportes.LogError('Error al generar reporte', sMsg, tbError);
     {$ENDIF}
end;

end.



