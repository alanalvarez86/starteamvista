object RepDerechos: TRepDerechos
  Left = 189
  Top = 200
  Width = 896
  Height = 553
  Caption = 'Asignaci�n De Derechos de Acceso'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object QRReporte: TQuickRep
    Left = 8
    Top = 8
    Width = 845
    Height = 653
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      '''''')
    OnPreview = QRReportePreview
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poLandscape
    Page.PaperSize = Letter
    Page.Values = (
      127.000000000000000000
      2159.000000000000000000
      127.000000000000000000
      2794.000000000000000000
      127.000000000000000000
      127.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = First
    PrintIfEmpty = False
    SnapToGrid = True
    Units = Inches
    Zoom = 80
    object PageHeaderBand: TQRBand
      Left = 38
      Top = 38
      Width = 768
      Height = 67
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        221.588541666666700000
        2540.000000000000000000)
      BandType = rbPageHeader
      object QRTitulo: TQRLabel
        Left = 8
        Top = 6
        Width = 326
        Height = 18
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          59.531250000000000000
          26.458333333333330000
          19.843750000000000000
          1078.177083333333000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Asignaci�n De Derechos de Acceso a Tress'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        WordWrap = True
        FontSize = 14
      end
      object FechaImpresionLBL: TQRLabel
        Left = 446
        Top = 8
        Width = 95
        Height = 14
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          46.302083333333340000
          1475.052083333333000000
          26.458333333333330000
          314.192708333333400000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Fecha de Impresi�n:'
        Color = clWhite
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object FechaImpresion: TQRSysData
        Left = 548
        Top = 8
        Width = 54
        Height = 14
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          46.302083333333340000
          1812.395833333333000000
          26.458333333333330000
          178.593750000000000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        Color = clWhite
        Data = qrsDateTime
        OnPrint = FechaImpresionPrint
        Transparent = False
        FontSize = 10
      end
      object QRShape4: TQRShape
        Left = 7
        Top = 22
        Width = 751
        Height = 10
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          33.072916666666700000
          23.151041666666700000
          72.760416666666700000
          2483.776041666670000000)
        Shape = qrsHorLine
      end
      object Company: TQRLabel
        Left = 59
        Top = 33
        Width = 45
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666670000
          195.130208333333400000
          109.140625000000000000
          148.828125000000000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Company'
        Color = clWhite
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object CompanyLBL: TQRLabel
        Left = 8
        Top = 33
        Width = 46
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666670000
          26.458333333333330000
          109.140625000000000000
          152.135416666666700000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Empresa:'
        Color = clWhite
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object GroupLBL: TQRLabel
        Left = 22
        Top = 48
        Width = 32
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666670000
          72.760416666666680000
          158.750000000000000000
          105.833333333333300000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Grupo:'
        Color = clWhite
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object Group: TQRLabel
        Left = 59
        Top = 48
        Width = 29
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666670000
          195.130208333333400000
          158.750000000000000000
          95.911458333333340000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Group'
        Color = clWhite
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
      object QRSysData1: TQRSysData
        Left = 664
        Top = 8
        Width = 81
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666670000
          2196.041666666667000000
          26.458333333333330000
          267.890625000000000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = True
        Color = clWhite
        Data = qrsPageNumber
        Text = 'P�gina # '
        Transparent = False
        FontSize = 10
      end
      object Filtro: TQRLabel
        Left = 661
        Top = 39
        Width = 96
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666670000
          2186.119791666667000000
          128.984375000000000000
          317.500000000000000000)
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = True
        AutoStretch = False
        Caption = 'Todos Los Derechos'
        Color = clWhite
        Transparent = False
        WordWrap = True
        FontSize = 10
      end
    end
    object DetailBand: TQRBand
      Left = 38
      Top = 136
      Width = 768
      Height = 14
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = True
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        46.302083333333340000
        2540.000000000000000000)
      BandType = rbDetail
      object RI_NOMBRE: TQRExpr
        Left = 5
        Top = 1
        Width = 250
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          16.536458333333300000
          3.307291666666670000
          826.822916666667000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = True
        Expression = 'RI_NOMBRE'
        FontSize = 8
      end
      object RI_ITEM_00: TQRExpr
        Left = 256
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          846.666666666667000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_00'
        FontSize = 8
      end
      object RI_ITEM_01: TQRExpr
        Left = 296
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          978.958333333333000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_01'
        FontSize = 8
      end
      object RI_ITEM_02: TQRExpr
        Left = 336
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1111.250000000000000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_02'
        FontSize = 8
      end
      object RI_ITEM_03: TQRExpr
        Left = 376
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1243.541666666670000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_03'
        FontSize = 8
      end
      object RI_ITEM_07: TQRExpr
        Left = 496
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1640.416666666670000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_07'
        FontSize = 8
      end
      object RI_ITEM_06: TQRExpr
        Left = 476
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1574.270833333330000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_06'
        FontSize = 8
      end
      object RI_ITEM_05: TQRExpr
        Left = 456
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1508.125000000000000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_05'
        FontSize = 8
      end
      object RI_ITEM_04: TQRExpr
        Left = 416
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1375.833333333330000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_04'
        FontSize = 8
      end
      object RI_COMENTA: TQRExprMemo
        Left = 584
        Top = 0
        Width = 177
        Height = 12
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          39.687500000000000000
          1931.458333333330000000
          0.000000000000000000
          585.390625000000000000)
        RemoveBlankLines = False
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Color = clWhite
        Lines.Strings = (
          '{RI_COMENTA}')
        Transparent = True
        WordWrap = True
        FontSize = 8
      end
      object RI_ITEM_08: TQRExpr
        Left = 516
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1706.562500000000000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_08'
        FontSize = 8
      end
      object RI_ITEM_09: TQRExpr
        Left = 536
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1772.708333333330000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_09'
        FontSize = 8
      end
      object RI_ITEM_10: TQRExpr
        Left = 556
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1838.854166666670000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        OnPrint = RI_ITEM_00Print
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = 'RI_ITEM_10'
        FontSize = 8
      end
    end
    object ChildBand: TQRChildBand
      Left = 38
      Top = 105
      Width = 768
      Height = 31
      Frame.Color = clBlack
      Frame.DrawTop = True
      Frame.DrawBottom = True
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clSilver
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        102.526041666666700000
        2540.000000000000000000)
      ParentBand = PageHeaderBand
      object RI_NOMBRElbl: TQRExpr
        Left = 5
        Top = 1
        Width = 250
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          16.536458333333300000
          3.307291666666670000
          826.822916666667000000)
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = True
        Expression = '''Derechos de Acceso'''
        FontSize = 8
      end
      object Explicacion1LBL: TQRExpr
        Left = 256
        Top = 15
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          846.666666666667000000
          49.609375000000000000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''Consultar'''
        FontSize = 8
      end
      object Titulo1LBL: TQRExpr
        Left = 256
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          846.666666666667000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 1'''
        FontSize = 8
      end
      object Explicacion2LBL: TQRExpr
        Left = 296
        Top = 15
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          978.958333333333000000
          49.609375000000000000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''Agregar'''
        FontSize = 8
      end
      object Titulo2LBL: TQRExpr
        Left = 296
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          978.958333333333000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 2'''
        FontSize = 8
      end
      object Titulo3LBL: TQRExpr
        Left = 336
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1111.250000000000000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 3'''
        FontSize = 8
      end
      object Explicacion3LBL: TQRExpr
        Left = 336
        Top = 15
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1111.250000000000000000
          49.609375000000000000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''Borrar'''
        FontSize = 8
      end
      object Explicacion4LBL: TQRExpr
        Left = 376
        Top = 15
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1243.541666666670000000
          49.609375000000000000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''Cambiar'''
        FontSize = 8
      end
      object Titulo4LBL: TQRExpr
        Left = 376
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1243.541666666670000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 4'''
        FontSize = 8
      end
      object Titulo5LBL: TQRExpr
        Left = 416
        Top = 1
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1375.833333333330000000
          3.307291666666670000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 5'''
        FontSize = 8
      end
      object Titulo6LBL: TQRExpr
        Left = 456
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1508.125000000000000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 6'''
        FontSize = 8
      end
      object Titulo7LBL: TQRExpr
        Left = 476
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1574.270833333330000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 7'''
        FontSize = 8
      end
      object Titulo8LBL: TQRExpr
        Left = 496
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1640.416666666670000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 8'''
        FontSize = 8
      end
      object Explicacion5LBL: TQRExpr
        Left = 416
        Top = 15
        Width = 40
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1375.833333333330000000
          49.609375000000000000
          132.291666666667000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''Imprimir'''
        FontSize = 8
      end
      object ExplicacionLBL: TQRExpr
        Left = 584
        Top = 1
        Width = 177
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1931.458333333330000000
          3.307291666666670000
          585.390625000000000000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''Explicaci�n'''
        FontSize = 8
      end
      object Titulo9LBL: TQRExpr
        Left = 516
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1706.562500000000000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''# 9'''
        FontSize = 8
      end
      object Titulo10LBL: TQRExpr
        Left = 536
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1772.708333333330000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''#10'''
        FontSize = 8
      end
      object Titulo11LBL: TQRExpr
        Left = 556
        Top = 1
        Width = 20
        Height = 13
        Frame.Color = clBlack
        Frame.DrawTop = False
        Frame.DrawBottom = False
        Frame.DrawLeft = False
        Frame.DrawRight = False
        Size.Values = (
          42.994791666666700000
          1838.854166666670000000
          3.307291666666670000
          66.145833333333300000)
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        AutoStretch = False
        Color = clWhite
        ResetAfterPrint = False
        Transparent = True
        WordWrap = False
        Expression = '''#11'''
        FontSize = 8
      end
    end
  end
end
