unit QRLoadReport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, thsdRuntimeLoader, thsdRuntimeEditor,
  QRDesign, Vcl.ExtCtrls, QuickRpt, qrdBaseCtrls, qrdQuickrep, StrUtils;

type
  TQRReportLoader = class(TForm)
    DesignQuickReport1: TDesignQuickReport;
    QRepDesigner: TQRepDesigner;
  private
    { Private declarations }
  public
    { Public declarations }
    function Conversion(sPlantilla: String):Integer;
  end;

var
  QRReportLoader: TQRReportLoader;

  procedure PreparaReporte;
  procedure DesPreparaReporte;

implementation

{$R *.dfm}

uses
    ZetaCommonTools,
    ZetaCommonClasses;

procedure PreparaReporte;
begin
     if QRReportLoader = NIL then
        QRReportLoader := TQRReportLoader.Create( {$ifdef XMLREPORT}nil{$else}Application{$endif} );
end;

procedure DesPreparaReporte;
begin
     {if QRReportLoader <> nil then
     begin
        QRReportLoader.Free;
        QRReportLoader := NIL;
     end;}
     FreeAndNil(QRReportLoader);
end;

function TQRReportLoader.Conversion(sPlantilla:String):Integer;
var
    iResultado: Integer;
    //sTempFileName: String;
const
    CONVERTIDA = 1;
    ERROR_CARGANDO_LEGACY = 2;
    ERROR_GRABANDO =3;
    PLANTILLA_NO_EXISTE = 4;
    K_FIND = 'QR2';
    K_REPLACE ='QRT';
begin
    iResultado := 0;
    if FileExists( sPlantilla ) then
     begin
          //raise Exception.Create( 'path QR2 valido' );
          try
             if not QRepDesigner.LoadReport(sPlantilla)then
             begin
                iResultado:= ERROR_CARGANDO_LEGACY;
             end
             else
             begin
                 try
                      //sTempFileName := sPlantilla;
                      //sTempFileName := StrTransform(sTempFileName,K_FIND,K_REPLACE);
                      QRepDesigner.SaveReport('D:\Temp\prueba.QR2'); //PENDIENTE: CAMBIAR PATH
                      iResultado:= CONVERTIDA;
                 except
                         On Error:Exception do
                            iResultado:= ERROR_GRABANDO;
                 end;
             end;
          finally
          end;
     end
     else
         iResultado:= PLANTILLA_NO_EXISTE;
     result := iResultado;
end;
end.
