library ConviertePlantillaDLL;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

{------------------------------------------------------------------------------------------------
 Unidad para importar los datos de formato propietario ASTA a formato XML de Midas
 Este DLL se incluye como un recurso dentro del ejecutable de Tress Cafeter�a.

 Instrucciones para integrarlo al exe de Tress Cafeter�a:
 1. Compilar y generar el DLL con sus funciones exportadas correspondientes
 2. Compilar el archivo AstaDLL.rc (existe un archivo AstaDLL.bat que tiene las instrucciones)
 3. Una vez generado el archivo de recurso AstaDLL.RES, compilar Tress Cafeter�a y ejecutar de
    manera normal, todo es transparente para el usuario.
 ------------------------------------------------------------------------------------------------}

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  SysUtils,
  MidasLib,
  QrdQuickrep,
  QRDesign,
  QRLoadReport in 'QRLoadReport.pas' {QRReportLoader};

{$R *.res}

function ConviertePlantilla( sPlantilla:String ):Integer; stdcall;
var iResultado:Integer;
begin
  iResultado := 0;
  //Inicializanco objetos QR y QRD
  QRLoadReport.PreparaReporte;
  iResultado := QRLoadReport.QRReportLoader.Conversion(sPlantilla);
  QRLoadReport.DesPreparaReporte;
  result := iResultado;
end;

exports
  ConviertePlantilla;
end.
