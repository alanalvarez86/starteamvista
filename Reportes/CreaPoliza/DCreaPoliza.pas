unit DCreaPoliza;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     ZetaTipoEntidad,
     ZetaClientDataSet;

const
     K_MAX_GRUPOS = 5;
type
  eGroupType = ( eGrupo1, eGrupo2, eGrupo3, eGrupo4, eGrupo5 );
  TGroupList = class;
  TGroupData = class( TObject )
  private
    { Private declarations }
    FLista: TGroupList;
    FNombre: String;
    FFormula: String;
    function GetEntidad: TipoEntidad;
  public
    { Public declarations }
    constructor Create( Lista: TGroupList );
    property Nombre: String read FNombre write FNombre;
    property Formula: String read FFormula write FFormula;
    property Entidad: TipoEntidad read GetEntidad;
  end;
  TGroupList = class( TObject )
  private
    { Private declarations }
    FItems: TList;
    function GetItem(Index: Integer): TGroupData;
    procedure Delete(const Index: Integer);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Items[ Index: Integer ]: TGroupData read GetItem;
    function AddGroup(const sNombre, sExpresion: String ): TGroupData;
    function Count: Integer;
    procedure Clear;
  end;
  TdmCreaPoliza = class(TDataModule)
    cdsCtaConcepto: TZetaClientDataSet;
    cdsGrupo1: TZetaClientDataSet;
    cdsGrupo2: TZetaClientDataSet;
    cdsGrupo3: TZetaClientDataSet;
    cdsGrupo4: TZetaClientDataSet;
    cdsGrupo5: TZetaClientDataSet;
    cdsPoliza: TZetaClientDataSet;
    cdsEditReporte: TZetaClientDataSet;
    cdsCampoRep: TZetaClientDataSet;
    cdsTemporal: TZetaClientDataSet;
    cdsConceptos: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsPolizaNewRecord(DataSet: TDataSet);
    procedure cdsPolizaAfterOpen(DataSet: TDataSet);
    procedure CR_OPERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsEditReporteAlAdquirirDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsEditReporteReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsEditReporteReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsCampoRepAfterOpen(DataSet: TDataSet);
    procedure cdsCampoRepNewRecord(DataSet: TDataSet);
    procedure cdsCtaConceptoAfterOpen(DataSet: TDataSet);
    procedure cdsCtaConceptoAlCrearCampos(Sender: TObject);
    procedure cdsGrupo1AfterOpen(DataSet: TDataSet);
    procedure cdsGrupo2AfterOpen(DataSet: TDataSet);
    procedure cdsGrupo3AfterOpen(DataSet: TDataSet);
    procedure cdsGrupo4AfterOpen(DataSet: TDataSet);
    procedure cdsGrupo5AfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FGrupos: TGroupList;
    FListaCodigo: array[ 0..K_MAX_GRUPOS ] of TStrings;
    FListaCuenta: array[ 0..K_MAX_GRUPOS ] of TStrings;
    FListaAsiento: TStrings;
    function GetCuantosAsientos: Integer;
    function GetNumGrupos: Integer;
    procedure BorraFormulas(Dataset: TClientDataset);
    procedure BorraTodoExceptoFormulas(Dataset: TClientDataset);
    procedure ExportaArchivo(Dataset: TClientDataSet; const sFile: String);
    procedure FillGrupos(Dataset: TClientDataset; Datos: OleVariant; const sCodigo, sDescripcion, sSubcuenta: String);
    procedure FillNivel(Dataset: TClientDataset; const iNivel: Integer );
    procedure FilterCampoRepOff;
    procedure FilterCampoRepOn;
    procedure ImportaArchivo(Dataset: TClientDataSet; const sFile: String);
    procedure LimpiaListas;
  public
    { Public declarations }
    property CuantosAsientos: Integer read GetCuantosAsientos;
    property NumGrupos: Integer read GetNumGrupos;
    function Conectar: Boolean;
    function GetDatasetGrupo(const eGrupo: eGroupType): TClientDataset;
    function GrabaPoliza( var sMensaje: String ): Boolean;
    function ReporteActivo: Integer;
    function UltimoAsiento: Integer;
    procedure AgregaAsientos( Callback: TNotifyEvent );
    procedure BorraFormulasPoliza;
    procedure BorraGruposTodos(const eGrupo: eGroupType);
    procedure ExportaCtaConcepto(const sFile: String);
    procedure ExportaGrupo(const eGrupo: eGroupType; const sFile: String);
    procedure GenerarPoliza(const sTemplate: String);
    procedure ImportaCtaConcepto(const sFile: String);
    procedure ImportaGrupo(const eGrupo: eGroupType; const sFile: String);
    procedure ImportaGrupoTress(const eGrupo: eGroupType);
    procedure LlenaConceptos;
    procedure LlenaGrupos( Lista: TStrings );
  end;

var
  dmCreaPoliza: TdmCreaPoliza;

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZReconcile,
     FConfigurarPoliza,
     DCliente;

{$R *.DFM}

const
     K_CARGO = 'C';
     K_CARGO_INT = 0;
     K_ABONO = 'A';
     K_ABONO_INT = 1;
     K_MASCARA_ASIENTO = '>L';
     K_FLD_CUENTA = 'CUENTA';
     K_FLD_CONCEPTO = 'CONCEPTO';
     K_FLD_GRUPO = 'GRUPO%D';
     K_FLD_GRUPO1 = 'GRUPO1';
     K_FLD_GRUPO2 = 'GRUPO2';
     K_FLD_GRUPO3 = 'GRUPO3';
     K_FLD_GRUPO4 = 'GRUPO4';
     K_FLD_GRUPO5 = 'GRUPO5';
     K_FLD_TIPO = 'TIPO';
     K_FLD_ASIENTO = 'ASIENTO';
     K_FLD_CODIGO = 'CODIGO';
     K_FLD_DESCRIPCION = 'DESCRIPCION';
     K_FLD_SUBCUENTA = 'SUBCUENTA';

type
    eTipoScript = ( Q_POLIZA, Q_GRUPO, Q_CUENTA_CONCEPTO );

function GetSQLScript( const eScript: eTipoScript ): String;
begin
     case eScript of
          Q_POLIZA: Result := 'select CB_APE_MAT CUENTA, CB_CODIGO CONCEPTO, CB_NIVEL1 GRUPO1, CB_NIVEL2 GRUPO2, CB_NIVEL3 GRUPO3, CB_NIVEL4 GRUPO4, CB_NIVEL5 GRUPO5, CB_ACTIVO TIPO '+
                              'from COLABORA where ( CB_CODIGO is NULL )';
          Q_GRUPO: Result := 'select TB_CODIGO CODIGO, TB_ELEMENT DESCRIPCION, TB_TEXTO SUBCUENTA from NIVEL1 where ( TB_CODIGO is NULL )';
          Q_CUENTA_CONCEPTO: Result := 'select CO_NUMERO CONCEPTO, CO_DESCRIP SUBCUENTA, CO_ACTIVO ASIENTO from CONCEPTO where ( CO_NUMERO is NULL )';
     else
         Result := VACIO;
     end;
end;

{ *********** TGroupData ************ }

constructor TGroupData.Create(Lista: TGroupList);
begin
     FLista := Lista;
end;

function TGroupData.GetEntidad: TipoEntidad;
const
     K_LEN_NIVEL = {$ifdef ACS}11{$else}8{$endif};
var
   sFormula: String;
begin
     { FFormulas almacena strings en formato VALOR=TABLA.CAMPO}
     sFormula := FFormula;
     { Elimina la parte VALOR= }
     sFormula := Copy( sFormula, Pos( '=', sFormula ) + 1, MAXINT );
     { Elimina la parte TABLA. }
     sFormula := Copy( sFormula, Pos( '.', sFormula ) + 1, MAXINT );
     if ( Copy( sFormula, 1, K_LEN_NIVEL ) = 'CB_NIVEL' ) then
     begin
          case StrToIntDef( Copy( sFormula, ( K_LEN_NIVEL + 1 ), ( Length( sFormula ) - K_LEN_NIVEL ) ), 0 ) of
               1: Result := enNivel1;
               2: Result := enNivel2;
               3: Result := enNivel3;
               4: Result := enNivel4;
               5: Result := enNivel5;
               6: Result := enNivel6;
               7: Result := enNivel7;
               8: Result := enNivel8;
               9: Result := enNivel9;
{$ifdef ACS}
               10: Result := enNivel10;
               11: Result := enNivel11;
               12: Result := enNivel12;
{$endif}
          else
              Result := enNinguno;
          end;
     end
     else
         if ( sFormula = 'CB_PUESTO' ) then
            Result := enPuesto
         else
             if ( sFormula = 'CB_CLASIFI' ) then
                Result := enClasifi
             else
                 if ( sFormula = 'CB_TURNO' ) then
                    Result := enTurno
                 else
                     Result := enNinguno;
end;

{ ********* TGroupList ******** }

constructor TGroupList.Create;
begin
     FItems := TList.Create;
end;

destructor TGroupList.Destroy;
begin
     Clear;
     FreeAndNil( FItems );
     inherited Destroy;
end;

function TGroupList.GetItem(Index: Integer): TGroupData;
begin
     with FItems do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TGroupData( Items[ Index ] )
          else
              Result := nil;
     end;
end;

function TGroupList.AddGroup(const sNombre, sExpresion: String ): TGroupData;
begin
     Result := TGroupData.Create( Self );
     try
        FItems.Add( Result );
        with Result do
        begin
             Nombre := sNombre;
             Formula := sExpresion;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TGroupList.Clear;
var
   i: Integer;
begin
     with FItems do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TGroupList.Count: Integer;
begin
     Result := FItems.Count;
end;

procedure TGroupList.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;

{ ************ TdmCreaPoliza ************ }

procedure TdmCreaPoliza.DataModuleCreate(Sender: TObject);
var
   i: Integer;
begin
     for i := 0 to K_MAX_GRUPOS do
     begin
          FListaCodigo[ i ] := TStringList.Create;
          FListaCuenta[ i ] := TStringList.Create;
     end;
     FListaAsiento := TStringList.Create;
     FGrupos := TGroupList.Create;
end;

procedure TdmCreaPoliza.DataModuleDestroy(Sender: TObject);
var
   i: Integer;
begin
     FreeAndNil( FGrupos );
     FreeAndNil( FListaAsiento );
     for i := K_MAX_GRUPOS downto 0 do
     begin
          FListaCodigo[ i ].Free;
          FListaCuenta[ i ].Free;
     end;
end;

function TdmCreaPoliza.Conectar: Boolean;
var
   Datos: OleVariant;
begin
     Result := False;
     try
        with dmCliente do
        begin
             { Cargar Reporte que almacena la p�liza }
             with cdsEditReporte do
             begin
                  Refrescar;
             end;
             { Cargar Cat�logo de Conceptos }
             cdsConceptos.Data := ServerCatalogos.GetConceptos( Empresa );
             { Inicializa ClientDataset de P�liza }
             cdsPoliza.Data := GetSQLData( GetSQLScript( Q_POLIZA ) );
             { Inicializar ClientDatasets de Grupos }
             Datos := GetSQLData( GetSQLScript( Q_GRUPO ) );
             cdsGrupo1.Data := Datos;
             cdsGrupo2.Data := Datos;
             cdsGrupo3.Data := Datos;
             cdsGrupo4.Data := Datos;
             cdsGrupo5.Data := Datos;
             { Inicializa ClientDataset de P�liza }
             cdsCtaConcepto.Data := GetSQLData( GetSQLScript( Q_CUENTA_CONCEPTO ) );
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function TdmCreaPoliza.GetNumGrupos: Integer;
begin
     Result := FGrupos.Count;
end;

procedure TdmCreaPoliza.LimpiaListas;
var
   i: Integer;
begin
     for i := 0 to K_MAX_GRUPOS do
     begin
          FListaCodigo[ i ].Clear;
          FListaCuenta[ i ].Clear;
     end;
     FListaAsiento.Clear;
end;

function TdmCreaPoliza.ReporteActivo: Integer;
begin
     Result := dmCliente.PolizaActiva;
end;

procedure TdmCreaPoliza.ImportaArchivo( Dataset: TClientDataSet; const sFile: String );
begin
     with Dataset do
     begin
          LoadFromFile( sFile );
          IndexFieldNames := Fields[ 0 ].FieldName;
     end;
end;

procedure TdmCreaPoliza.ExportaArchivo( Dataset: TClientDataSet; const sFile: String);
begin
     with Dataset do
     begin
          SaveToFile( sFile );
     end;
end;

procedure TdmCreaPoliza.ImportaCtaConcepto(const sFile: String);
begin
     ImportaArchivo( cdsCtaConcepto, sFile );
end;

procedure TdmCreaPoliza.ExportaCtaConcepto(const sFile: String);
begin
     ExportaArchivo( cdsCtaConcepto, sFile );
end;

function TdmCreaPoliza.GetCuantosAsientos: Integer;
begin
     with cdsPoliza do
     begin
          if Active then
             Result := RecordCount
          else
              Result := 0;
     end;
end;

function TdmCreaPoliza.GetDatasetGrupo(const eGrupo: eGroupType): TClientDataset;
begin
     case eGrupo of
          eGrupo1: Result := cdsGrupo1;
          eGrupo2: Result := cdsGrupo2;
          eGrupo3: Result := cdsGrupo3;
          eGrupo4: Result := cdsGrupo4;
          eGrupo5: Result := cdsGrupo5;
     else
         Result := nil;
     end;
end;

procedure TdmCreaPoliza.ExportaGrupo(const eGrupo: eGroupType; const sFile: String);
begin
     ExportaArchivo( GetDatasetGrupo( eGrupo ), sFile );
end;

procedure TdmCreaPoliza.ImportaGrupo(const eGrupo: eGroupType; const sFile: String);
begin
     ImportaArchivo( GetDatasetGrupo( eGrupo ), sFile );
end;

procedure TdmCreaPoliza.FillGrupos( Dataset: TClientDataset; Datos: OleVariant; const sCodigo, sDescripcion, sSubcuenta: String );
begin
     with Dataset do
     begin
          DisableControls;
          EmptyDataSet;
          with cdsTemporal do
          begin
               Data := Datos;
               if Active then
               begin
                    First;
                    while not Eof do
                    begin
                         Dataset.Append;
                         Dataset.FieldByName( K_FLD_CODIGO ).AsString := FieldByName( sCodigo ).AsString;
                         Dataset.FieldByName( K_FLD_DESCRIPCION ).AsString := FieldByName( sDescripcion ).AsString;
                         Dataset.FieldByName( K_FLD_SUBCUENTA ).AsString := FieldByName( sSubCuenta ).AsString;
                         Dataset.Post;
                         Next;
                    end;
               end;
               Active := False;
          end;
          First;
          EnableControls;
     end;
end;

procedure TdmCreaPoliza.FillNivel(Dataset: TClientDataset; const iNivel: Integer );
const
     K_NIVEL_BASE = 17;
begin
     with dmCliente do
     begin
          FillGrupos( Dataset, ServerTablas.GetTabla( Empresa, K_NIVEL_BASE + iNivel ), 'TB_CODIGO', 'TB_ELEMENT', 'TB_TEXTO' );
     end;
end;

procedure TdmCreaPoliza.ImportaGrupoTress( const eGrupo: eGroupType );
var
   FDataset: TClientDataset;
begin
     FDataset := GetDatasetGrupo( eGrupo );
     if Assigned( FDataset ) then
     begin
          with FGrupos.Items[ Ord( eGrupo ) ] do
          begin
               with dmCliente do
               begin
                    case Entidad of
                         enClasifi: FillGrupos( FDataset, ServerCatalogos.GetClasifi( Empresa ), 'TB_CODIGO', 'TB_ELEMENT', 'TB_TEXTO' );
                         enNivel1: FillNivel( FDataset, 1  );
                         enNivel2: FillNivel( FDataset, 2  );
                         enNivel3: FillNivel( FDataset, 3  );
                         enNivel4: FillNivel( FDataset, 4  );
                         enNivel5: FillNivel( FDataset, 5  );
                         enNivel6: FillNivel( FDataset, 6  );
                         enNivel7: FillNivel( FDataset, 7  );
                         enNivel8: FillNivel( FDataset, 8  );
                         enNivel9: FillNivel( FDataset, 9  );
{$ifdef ACS}
                         enNivel10: FillNivel( FDataset, 10  );
                         enNivel11: FillNivel( FDataset, 11  );
                         enNivel12: FillNivel( FDataset, 12  );
{$endif}
                         enPuesto: FillGrupos( FDataset, ServerCatalogos.GetPuestos( Empresa ), 'PU_CODIGO', 'PU_DESCRIP', 'PU_TEXTO' );
                         enTurno: FillGrupos( FDataset, ServerCatalogos.GetTurnos( Empresa ), 'TU_CODIGO', 'TU_DESCRIP', 'TU_TEXTO' );
                    else
                        DataBaseError( Format( 'El Campo %s No Est� Preparado', [ Formula ] ) );
                    end;
               end;
          end;
     end
     else
         DataBaseError( 'No Hay Grupos Asignados' );
end;

procedure TdmCreaPoliza.LlenaGrupos( Lista: TStrings );
var
   i: Integer;
begin
     FGrupos.Clear;
     with cdsCampoRep do
     begin
          if Active then
          begin
               DisableControls;
               try
                  FilterCampoRepOff;
                  First;
                  while not Eof do
                  begin
                       if ( eTipoCampo( FieldByName( 'CR_TIPO' ).AsInteger ) = tcOrden ) then
                       begin
                            FGrupos.AddGroup( FieldByName( 'CR_TITULO' ).AsString,
                                              FieldByName( 'CR_FORMULA' ).AsString );
                       end;
                       Next;
                  end;
                  First;
               finally
                      FilterCampoRepOn;
                      EnableControls;
               end;
          end;
     end;
     { Columnas del DataSet de P�liza }
     with cdsPoliza do
     begin
          for i := 1 to K_MAX_GRUPOS do
          begin
               FieldByName( Format( K_FLD_GRUPO, [ i ] ) ).Visible := ( NumGrupos >= i );
          end;
     end;
     { Llenar la Lista del ComboBox }
     with Lista do
     begin
          Clear;
          BeginUpdate;
          try
             for i := 0 to ( NumGrupos - 1 ) do
             begin
                  with FGrupos.Items[ i ] do
                  begin
                       Add( Format( '%s=%s', [ Nombre, Formula ] ) );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TdmCreaPoliza.BorraTodoExceptoFormulas( Dataset: TClientDataset );
begin
     with Dataset do
     begin
          DisableControls;
          try
             First;
             while not Eof do
             begin
                  if ( eTipoCampo( FieldByName( 'CR_TIPO' ).AsInteger ) <> tcCampos ) then
                  begin
                       Delete;
                  end
                  else
                      Next;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmCreaPoliza.BorraFormulas( Dataset: TClientDataset );
begin
     with Dataset do
     begin
          DisableControls;
          try
             First;
             while not Eof do
             begin
                  if ( eTipoCampo( FieldByName( 'CR_TIPO' ).AsInteger ) = tcCampos ) then
                  begin
                       Delete;
                  end
                  else
                      Next;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmCreaPoliza.BorraFormulasPoliza;
begin
     BorraFormulas( cdsCampoRep );
end;

procedure TdmCreaPoliza.cdsEditReporteAlAdquirirDatos(Sender: TObject);
var
   CampoRep: OleVariant;
begin
     with dmCliente do
     begin
          cdsEditReporte.Data := ServerReportes.GetEditReportes( Empresa, ReporteActivo, CampoRep );
     end;
     { Se esta simulando el Master-Detail }
     cdsCampoRep.Data := CampoRep;
     { Forzar el cambio de usuario para que siempre haya un DELTA }
     with cdsEditReporte do
     begin
          Edit;
          FieldByName( 'US_CODIGO' ).AsInteger := 0;
          Post;
          MergeChangeLog;
     end;
end;

{$ifdef VER130}
procedure TdmCreaPoliza.cdsEditReporteReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( E.ErrorCode = 1 ) and ( Pos( 'DUPLICATE', UpperCase( E.Message) ) > 0 ) then
        E.Message := 'El Nombre del Reporte est� Repetido';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmCreaPoliza.cdsEditReporteReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( E.ErrorCode = 1 ) and ( Pos( 'DUPLICATE', UpperCase( E.Message) ) > 0 ) then
        E.Message := 'El Nombre del Reporte est� Repetido';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

procedure TdmCreaPoliza.cdsCampoRepAfterOpen(DataSet: TDataSet);
begin
     with cdsCampoRep do
     begin
          FieldByName( 'CR_OPER' ).OnGetText := CR_OPERGetText;
     end;
     FilterCampoRepOn;
end;

procedure TdmCreaPoliza.FilterCampoRepOn;
begin
     with cdsCampoRep do
     begin
          Filter := Format( 'CR_TIPO = %d', [ Ord( tcCampos ) ] );
          Filtered := True;
     end;
end;

procedure TdmCreaPoliza.FilterCampoRepOff;
begin
     with cdsCampoRep do
     begin
          Filtered := False;
          Filter := VACIO;
     end;
end;

procedure TdmCreaPoliza.CR_OPERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = K_CARGO_INT ) then
        Text := K_CARGO
     else
         Text := K_ABONO;
end;

procedure TdmCreaPoliza.cdsCampoRepNewRecord(DataSet: TDataSet);
begin
     with cdsCampoRep do
     begin
          FieldByName( 'RE_CODIGO' ).AsInteger := ReporteActivo;
          FieldByName( 'CR_TIPO' ).AsInteger := Ord( tcCampos );
          FieldByName ( 'CR_SUBPOS' ).AsInteger := -1;
     end;
end;

procedure TdmCreaPoliza.BorraGruposTodos( const eGrupo: eGroupType );
var
   FDataset: TClientDataset;
begin
     FDataset := GetDatasetGrupo( eGrupo );
     if Assigned( FDataset ) then
     begin
          FDataset.EmptyDataSet;
     end;
end;

procedure TdmCreaPoliza.cdsCtaConceptoAlCrearCampos(Sender: TObject);
begin
     with cdsCtaConcepto do
     begin
          CreateSimpleLookup( cdsConceptos, K_FLD_DESCRIPCION, K_FLD_CONCEPTO );
     end;
end;

procedure TdmCreaPoliza.cdsCtaConceptoAfterOpen(DataSet: TDataSet);
begin
     with cdsCtaConcepto do
     begin
          with FieldByName( K_FLD_CONCEPTO ) do
          begin
               DisplayWidth := 10;
          end;
          with FieldByName( K_FLD_ASIENTO ) do
          begin
               EditMask := K_MASCARA_ASIENTO;
               DisplayWidth := 1;
          end;
          LogChanges := False;
     end;
end;

procedure TdmCreaPoliza.cdsGrupo1AfterOpen(DataSet: TDataSet);
begin
     with cdsGrupo1 do
     begin
          LogChanges := False;
     end;
end;

procedure TdmCreaPoliza.cdsGrupo2AfterOpen(DataSet: TDataSet);
begin
     with cdsGrupo2 do
     begin
          LogChanges := False;
     end;
end;

procedure TdmCreaPoliza.cdsGrupo3AfterOpen(DataSet: TDataSet);
begin
     with cdsGrupo3 do
     begin
          LogChanges := False;
     end;
end;

procedure TdmCreaPoliza.cdsGrupo4AfterOpen(DataSet: TDataSet);
begin
     with cdsGrupo4 do
     begin
          LogChanges := False;
     end;
end;

procedure TdmCreaPoliza.cdsGrupo5AfterOpen(DataSet: TDataSet);
begin
     with cdsGrupo5 do
     begin
          LogChanges := False;
     end;
end;

procedure TdmCreaPoliza.cdsPolizaAfterOpen(DataSet: TDataSet);
var
   i: Integer;
begin
     with cdsPoliza do
     begin
          with FieldByName( K_FLD_CUENTA ) do
          begin
               DisplayWidth := 30;
          end;
          with FieldByName( K_FLD_CONCEPTO ) do
          begin
               DisplayWidth := 10;
          end;
          for i := 1 to K_MAX_GRUPOS do
          begin
               with FieldByName( Format( K_FLD_GRUPO, [ i ] ) ) do
               begin
                    DisplayWidth := 10;
                    Visible := False;
               end;
          end;
          with FieldByName( K_FLD_TIPO ) do
          begin
               EditMask := K_MASCARA_ASIENTO;
               DisplayWidth := 1;
          end;
          LogChanges := False;
     end;
end;

procedure TdmCreaPoliza.cdsPolizaNewRecord(DataSet: TDataSet);
begin
     with cdsPoliza do
     begin
          FieldByName( K_FLD_TIPO ).AsString := K_CARGO;
     end;
end;

procedure TdmCreaPoliza.LlenaConceptos;
const
     aAsiento: array[ False..True ] of pChar = ( K_ABONO, K_CARGO );
var
   eTipo: eTipoConcepto;
begin
     with cdsCtaConcepto do
     begin
          DisableControls;
          EmptyDataSet;
     end;
     try
        with cdsConceptos do
        begin
             if Active then
             begin
                  First;
                  while not Eof do
                  begin
                       eTipo := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                       if ( eTipo in [ coPercepcion, coDeduccion ] ) then
                       begin
                            cdsCtaConcepto.Append;
                            cdsCtaConcepto.FieldByName( K_FLD_CONCEPTO ).AsInteger := FieldByName( 'CO_NUMERO' ).AsInteger;
                            cdsCtaConcepto.FieldByName( K_FLD_ASIENTO ).AsString := aAsiento[ ( eTipo = coPercepcion ) ];
                            cdsCtaConcepto.Post;
                       end;
                       Next;
                  end;
             end;
        end;
     finally
            with cdsCtaConcepto do
            begin
                 if Active then
                    First;
                 EnableControls;
            end;
     end;
end;

function TdmCreaPoliza.UltimoAsiento: Integer;
begin
     with cdsCampoRep do
     begin
          if Active then
          begin
               DisableControls;
               try
                  Last;
                  Result := FieldByName( 'CR_POSICIO' ).AsInteger;
                  First;
               finally
                      EnableControls;
               end;
          end
          else
              Result := 0;
     end;
end;

procedure TdmCreaPoliza.AgregaAsientos( Callback: TNotifyEvent );
var
   iUltimo: Integer;
   sCuenta, sUltimaCuenta, sTipo: String;

    function GetDatasetGrupo( const iPos: Integer ): String;
    begin
         with FListaCodigo[ iPos ] do
         begin
            if ( Count > 0 ) then
               Result := '"' + FListaCodigo[ iPos ].CommaText + '"'
            else
                Result := VACIO;
         end;
    end;

    function CreaFormula: String;
    var
       i: Integer;
       sGrupo: String;
    begin
        Result := 'SUMA( ' + GetDatasetGrupo( 0 );
        for i := 1 to NumGrupos do
        begin
            sGrupo := GetDatasetGrupo( i );
            { Si no hay grupo y es el �ltimo grupo, entonces no agrega la ',' }
            if ZetaCommonTools.StrLleno( sGrupo ) or ( i < NumGrupos ) then
                Result := Result + ', ' + sGrupo;
        end;
        Result := Result + ' )';
    end;

    procedure AgregaUnaCuenta;
    var
       iOperacion: Integer;
    begin
         Inc( iUltimo );
         if ( sTipo = K_CARGO ) then
            iOperacion := K_CARGO_INT
         else
             iOperacion := K_ABONO_INT;
         with cdsCampoRep do
         begin
              Append;
              FieldByName( 'CR_POSICIO' ).AsInteger := iUltimo;
              FieldByName( 'CR_FORMULA' ).AsString := CreaFormula;
              FieldByName( 'CR_TITULO' ).AsString := sUltimaCuenta;
              FieldByName( 'CR_OPER' ).AsInteger := iOperacion;
              Post;
         end;
         LimpiaListas;
    end;

    procedure AgregaUnCodigo( const iPos: Integer; const sCampo: String );
    var
       sCodigo: String;
    begin
        sCodigo := cdsPoliza.FieldByName( sCampo ).AsString;
        if ZetaCommonTools.StrLleno( Trim( sCodigo ) ) then
        begin
             with FListaCodigo[ iPos ] do
             begin
                 if ( IndexOf( sCodigo ) < 0 ) then
                    Add( sCodigo );
             end;
        end;
    end;

    procedure AgregaCodigos;
    var
       i: Integer;
    begin
         AgregaUnCodigo( 0, K_FLD_CONCEPTO );
         for i := 1 to NumGrupos do
         begin
              AgregaUnCodigo( i, Format( K_FLD_GRUPO, [ i ] ) );
         end;
    end;

begin
     sUltimaCuenta := VACIO;
     LimpiaListas;
     iUltimo := UltimoAsiento;
     with cdsCampoRep do
     begin
          DisableControls;
     end;
     with cdsPoliza do
     begin
          DisableControls;
          try
             IndexFieldNames := K_FLD_CUENTA;
             First;
             while not Eof do
             begin
                  if Assigned( Callback ) then
                     CallBack( Self );
                  Application.ProcessMessages;
                  sCuenta := FieldByName( K_FLD_CUENTA ).AsString;
                  { Agrupa SUMA de todas las que van a la misma cuenta }
                  if ( sCuenta <> sUltimaCuenta ) then
                  begin
                       if ZetaCommonTools.StrLleno( sUltimaCuenta ) then { Que no sea la primera }
                          AgregaUnaCuenta;
                       sUltimaCuenta := sCuenta;
                       sTipo := FieldByName( K_FLD_TIPO ).AsString;
                  end;
                  AgregaCodigos;
                  Next;
             end;
             AgregaUnaCuenta;
          finally
                 EnableControls;
          end;
     end;
     with cdsCampoRep do
     begin
          Last;
          EnableControls;
     end;
end;

procedure TdmCreaPoliza.GenerarPoliza( const sTemplate: String );
var
   iConcepto, iG1, iG2, iG3, iG4, iG5 : Integer;
   sCuenta: String;

    procedure LlenaLista( const iPos: Integer; DataSet: TClientDataset );
    const
         K_CODIGO = 0;
    var
       sSubCuenta: String;
    begin
         with DataSet do
         begin
              DisableControls;
              First;
              while not Eof do
              begin
                   sSubCuenta := FieldByName( 'SubCuenta' ).AsString;
                   if ZetaCommonTools.StrLleno( Trim( sSubCuenta ) ) then
                   begin
                        FListaCodigo[ iPos ].Add( Fields[ K_CODIGO ].AsString );
                        FListaCuenta[ iPos ].Add( sSubCuenta );
                        if ( iPos = 0 ) then
                           FListaAsiento.Add( FieldByName( K_FLD_ASIENTO ).AsString );
                   end;
                   Next;
              end;
              First;
              EnableControls;
              // Se necesita al menos uno para que funcionen los 'for' anidados
              if ( FListaCodigo[ iPos ].Count = 0 ) then
              begin
                   FListaCodigo[ iPos ].Add( VACIO );
                   FListaCuenta[ iPos ].Add( VACIO );
                   if ( iPos = 0 ) then
                       FListaAsiento.Add( VACIO );
              end
         end;
    end;

    function Sustituye( const sValor, sPatron, sNuevo: String ): String;
    begin
         Result := StringReplace( sValor, sPatron, sNuevo, [ rfReplaceAll, rfIgnoreCase ] );
    end;

begin
     LimpiaListas;
     LlenaLista( 0, cdsCtaConcepto );
     LlenaLista( 1, cdsGrupo1 );
     LlenaLista( 2, cdsGrupo2 );
     LlenaLista( 3, cdsGrupo3 );
     LlenaLista( 4, cdsGrupo4 );
     LlenaLista( 5, cdsGrupo5 );
     with cdsPoliza do
     begin
          EmptyDataSet;
          DisableControls;
          for iConcepto := 0 to ( FListaCodigo[ 0 ].Count - 1 ) do
          begin
               for iG1 := 0 to ( FListaCodigo[ 1 ].Count - 1 ) do
               begin
                    for iG2 := 0 to ( FListaCodigo[ 2 ].Count - 1 ) do
                    begin
                         for iG3 := 0 to ( FListaCodigo[ 3 ].Count - 1 ) do
                         begin
                              for iG4 := 0 to ( FListaCodigo[ 4 ].Count - 1 ) do
                              begin
                                   for iG5 := 0 to ( FListaCodigo[ 5 ].Count - 1 ) do
                                   begin
                                        sCuenta := sTemplate;
                                        sCuenta := Sustituye( sCuenta, '#CO', FListaCuenta[ 0 ][ iConcepto ] );
                                        sCuenta := Sustituye( sCuenta, '#G1', FListaCuenta[ 1 ][ iG1 ] );
                                        sCuenta := Sustituye( sCuenta, '#G2', FListaCuenta[ 2 ][ iG2 ] );
                                        sCuenta := Sustituye( sCuenta, '#G3', FListaCuenta[ 3 ][ iG3 ] );
                                        sCuenta := Sustituye( sCuenta, '#G4', FListaCuenta[ 4 ][ iG4 ] );
                                        sCuenta := Sustituye( sCuenta, '#G5', FListaCuenta[ 5 ][ iG5 ] );
                                        Append;
                                        FieldByName( K_FLD_CUENTA ).AsString := sCuenta;
                                        FieldByName( K_FLD_CONCEPTO ).AsInteger := StrToIntDef( FListaCodigo[ 0 ][ iConcepto ], 0 );
                                        FieldByName( K_FLD_GRUPO1 ).AsString := FListaCodigo[ 1 ][ iG1 ];
                                        FieldByName( K_FLD_GRUPO2 ).AsString := FListaCodigo[ 2 ][ iG2 ];
                                        FieldByName( K_FLD_GRUPO3 ).AsString := FListaCodigo[ 3 ][ iG3 ];
                                        FieldByName( K_FLD_GRUPO4 ).AsString := FListaCodigo[ 4 ][ iG4 ];
                                        FieldByName( K_FLD_GRUPO5 ).AsString := FListaCodigo[ 5 ][ iG5 ];
                                        FieldByName( K_FLD_TIPO ).AsString := FListaAsiento[ iConcepto ];
                                        Post;
                                   end;
                              end;
                         end;
                    end;
               end;
          end;
          First;
          EnableControls;
     end;
end;

function TdmCreaPoliza.GrabaPoliza( var sMensaje: String ): Boolean;
var
   i, iPoliza, iReporte, iErrorCount: Integer;
begin
     Result := False;
     if ( cdsCampoRep.ChangeCount > 0 ) then
     begin
          { Copia el contenido de CampoRep hacia un temporal }
          cdsTemporal.Data := cdsCampoRep.Data;
          { Borra todas las f�rmulas del temporal }
          BorraFormulas( cdsTemporal );
          { Borra Todo Excepto las f�rmulas de CampoRep }
          BorraTodoExceptoFormulas( cdsCampoRep );
          with cdsTemporal do
          begin
               { Elimina el delta del Temporal }
               MergeChangeLog;
               { Copiar del Temporal hacia CampoRep los grupos, filtros, etc. }
               First;
               while not Eof do
               begin
                    cdsCampoRep.Append;
                    for i := 0 to ( FieldCount - 1 ) do
                    begin
                         cdsCampoRep.Fields[ i ].Assign( Fields[ i ] );
                    end;
                    cdsCampoRep.Post;
                    Next;
               end;
               { Borra Todo del Temporal }
               EmptyDataset;
          end;
          iPoliza := ReporteActivo;
          iReporte := iPoliza;
          iErrorCount := 0;
          with cdsEditReporte do
          begin
               Edit;
               FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
               FieldByName( 'RE_FECHA' ).AsDateTime := Date;
               Post;
               if ( ChangeCount = 0 ) then
                  sMensaje := 'La P�liza No Fu� Modificada'
               else
                   if ( cdsCampoRep.ChangeCount = 0 ) then
                      sMensaje := 'No Hay Cambios A Los Asientos De La P�liza'
                   else
                   begin
                        Reconcile( dmCliente.ServerReportes.GrabaPoliza( dmCliente.Empresa,
                                                                         Delta,
                                                                         cdsCampoRep.Delta,
                                                                         iErrorCount,
                                                                         iReporte ) );
                        Result := ( iErrorCount = 0 );
                   end;
          end;
     end
     else
         sMensaje := 'No Se Hicieron Cambios A Los Asientos De La P�liza';
end;

end.
