inherited dmCliente: TdmCliente
  OldCreateOrder = True
  Left = 285
  Top = 161
  object cdsReportes: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'RE_CODIGOOIndex'
        Fields = 'RE_CODIGO'
      end
      item
        Name = 'RE_NOMBREIndex'
        Fields = 'RE_NOMBRE'
      end
      item
        Name = 'RE_TIPOIndex'
        Fields = 'RE_TIPO'
      end>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    StoreDefs = True
    AfterOpen = cdsReportesAfterOpen
    AlAdquirirDatos = cdsReportesAlAdquirirDatos
    Left = 144
    Top = 56
  end
end
