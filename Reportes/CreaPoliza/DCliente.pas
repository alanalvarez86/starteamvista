unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerCatalogos,
     DServerConsultas,
     DServerReportes,
     DServerTablas,
     {$else}
     Catalogos_TLB,
     Consultas_TLB,
     Reportes_TLB,
     Tablas_TLB,
     {$endif}
     DBasicoCliente,
     ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    cdsReportes: TZetaClientDataSet;
    procedure cdsReportesAlAdquirirDatos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsReportesAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    FServerCatalogos: TdmServerCatalogos;
    FServerConsultas: TdmServerConsultas;
    FServerReportes: TdmServerReportes;
    FServerTablas: TdmServerTablas;
    {$else}
    FServerCatalogos: IdmServerCatalogosDisp;
    FServerConsultas: IdmServerConsultasDisp;
    FServerReportes: IdmServerReportesDisp;
    FServerTablas: IdmServerTablasDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerReportes: IdmServerReportesDisp;
    function GetServerTablas: IdmServerTablasDisp;
    {$endif}
    function GetPolizaActiva: Integer;
    function GetPolizaNombre: String;
    function GetEsPoliza: Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
    property ServerConsultas: TdmServerConsultas read FServerConsultas;
    property ServerReportes: TdmServerReportes read FServerReportes;
    property ServerTablas: TdmServerTablas read FServerTablas;
    {$else}
    property ServerCatalogos: IdmServerCatalogosDisp read GetServerCatalogos;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    property ServerTablas: IdmServerTablasDisp read GetServerTablas;
    {$endif}
    property EsPoliza: Boolean read GetEsPoliza;
    property PolizaActiva: Integer read GetPolizaActiva;
    property PolizaNombre: String read GetPolizaNombre;
    function GetPolizas: Boolean;
    function GetSQLData( const sSQL: String ): OleVariant;
  end;

var
  dmCliente: TdmCliente;

implementation

uses ZetaCommonLists,
     ZetaCommonClasses,
     ZetaTipoEntidad;

{$R *.DFM}

{ ********* TdmCliente ********** }

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerConsultas := TdmServerConsultas.Create( Self );
     FServerReportes := TdmServerReportes.Create( Self );
     FServerTablas := TdmServerTablas.Create( Self );
     {$endif}
     inherited;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerTablas );
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerConsultas );
     FreeAndNil( FServerReportes );
     {$endif}
end;

{$ifndef DOS_CAPAS}
function TdmCliente.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( CreaServidor( CLASS_dmServerCatalogos, FServerCatalogos ) );
end;

function TdmCliente.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmCliente.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp( CreaServidor( CLASS_dmServerReportes, FServerReportes ) );
end;

function TdmCliente.GetServerTablas: IdmServerTablasDisp;
begin
     Result := IdmServerTablasDisp( CreaServidor( CLASS_dmServerTablas, FServerTablas ) );
end;
{$endif}

function TdmCliente.GetPolizas: Boolean;
begin
     with cdsReportes do
     begin
          Refrescar;
          Result := not IsEmpty;
     end;
end;

function TdmCliente.GetEsPoliza: Boolean;
begin
     Result := ( eTipoReporte( cdsReportes.FieldByName( 'RE_TIPO' ).AsInteger ) = trPoliza );
end;

function TdmCliente.GetPolizaActiva: Integer;
begin
     Result := cdsReportes.FieldByName( 'RE_CODIGO' ).AsInteger;
end;

function TdmCliente.GetPolizaNombre: String;
begin
     Result := cdsReportes.FieldByName( 'RE_NOMBRE' ).AsString;
end;

procedure TdmCliente.cdsReportesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsReportes.Data := ServerReportes.GetEscogeReporte( Empresa, Ord( enNomina ), Ord( trPoliza ), True );
end;

procedure TdmCliente.cdsReportesAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsReportes do
     begin
          MaskFecha( 'RE_FECHA' );
          ListaFija( 'RE_TIPO', lfTipoReporte );
     end;
end;

function TdmCliente.GetSQLData(const sSQL: String): OleVariant;
begin
     Result := ServerConsultas.GetQueryGral( Empresa, sSQL );
end;


end.
