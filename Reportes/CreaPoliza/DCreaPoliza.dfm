object dmCreaPoliza: TdmCreaPoliza
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 222
  Top = 147
  Height = 495
  Width = 693
  object cdsCtaConcepto: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'Concepto'
    Params = <>
    AfterOpen = cdsCtaConceptoAfterOpen
    AlCrearCampos = cdsCtaConceptoAlCrearCampos
    Left = 43
    Top = 61
  end
  object cdsGrupo1: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsGrupo1AfterOpen
    Left = 44
    Top = 108
  end
  object cdsGrupo2: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsGrupo2AfterOpen
    Left = 43
    Top = 152
  end
  object cdsGrupo3: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsGrupo3AfterOpen
    Left = 43
    Top = 204
  end
  object cdsGrupo4: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsGrupo4AfterOpen
    Left = 43
    Top = 253
  end
  object cdsGrupo5: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsGrupo5AfterOpen
    Left = 43
    Top = 300
  end
  object cdsPoliza: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsPolizaAfterOpen
    OnNewRecord = cdsPolizaNewRecord
    Left = 42
    Top = 12
  end
  object cdsEditReporte: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_CODIGO'
    Params = <>
    OnReconcileError = cdsEditReporteReconcileError
    AlAdquirirDatos = cdsEditReporteAlAdquirirDatos
    Left = 136
    Top = 13
  end
  object cdsCampoRep: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CR_TIPO;CR_POSICIO;CR_SUBPOS'
    Params = <>
    AfterOpen = cdsCampoRepAfterOpen
    OnNewRecord = cdsCampoRepNewRecord
    Left = 136
    Top = 61
  end
  object cdsTemporal: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 108
  end
  object cdsConceptos: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat�logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    Left = 136
    Top = 152
  end
end
