inherited Formas: TFormas
  Left = 503
  Top = 366
  Caption = 'Formas'
  ClientHeight = 393
  ClientWidth = 559
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 559
  end
  inherited PageControl: TPageControl
    Width = 559
    Height = 310
    inherited tsGenerales: TTabSheet
      inherited GBParametrosEncabezado: TGroupBox
        Left = 33
      end
    end
    inherited tsOrden: TTabSheet
      inherited BArribaOrden: TZetaSmartListsButton
        Left = 227
      end
      inherited BAbajoOrden: TZetaSmartListsButton
        Left = 227
      end
      inherited GroupBox2: TGroupBox
        Left = 23
      end
      inherited BAgregaOrden: TBitBtn
        Left = 23
      end
      inherited BBorraOrden: TBitBtn
        Left = 123
      end
      inherited GroupBoxOrden: TGroupBox
        Left = 259
      end
      inherited BAgregaFormulaOrden: TBitBtn
        Left = 48
      end
    end
    inherited tsFiltros: TTabSheet
      inherited PageControlFiltros: TPageControl
        Left = 31
      end
    end
    inherited tsImpresora: TTabSheet
      inherited GroupBox3: TGroupBox
        Left = 16
        Top = 21
        Width = 521
        Height = 228
        object lbPlantilla: TLabel [0]
          Left = 25
          Top = 167
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre Plantilla:'
        end
        object bPlantilla: TSpeedButton [1]
          Left = 449
          Top = 161
          Width = 25
          Height = 25
          Hint = 'Buscar Plantilla'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333FFF333333333333000333333333
            3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
            3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
            0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
            BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
            33337777773FF733333333333300033333333333337773333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bPlantillaClick
        end
        object BGuardaArchivo: TSpeedButton [2]
          Left = 449
          Top = 94
          Width = 25
          Height = 25
          Hint = 'Buscar Archivo'
          Enabled = False
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333FFF333333333333000333333333
            3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
            3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
            0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
            BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
            33337777773FF733333333333300033333333333337773333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BGuardaArchivoClick
        end
        object lbNombreArchivo: TLabel [3]
          Left = 10
          Top = 100
          Width = 96
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre del Archivo:'
          Enabled = False
        end
        object lbCopias: TLabel [4]
          Left = 59
          Top = 74
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = '# Copias:'
        end
        object EPAGINAS: TLabel [5]
          Left = 175
          Top = 74
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Rango de P'#225'ginas:'
        end
        object EPAginasAl: TLabel [6]
          Left = 310
          Top = 74
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al:'
        end
        object lbImpresora: TLabel [7]
          Left = 55
          Top = 47
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Impresora:'
        end
        object Label1: TLabel [8]
          Left = 35
          Top = 20
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Salida:'
        end
        object bExportPreferencias: TSpeedButton [9]
          Left = 325
          Top = 14
          Width = 25
          Height = 25
          Hint = 'Configuraci'#243'n'
          Glyph.Data = {
            06020000424D0602000000000000760000002800000028000000140000000100
            0400000000009001000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333333333333333333333333333333FFF3333333333
            FFF33300033333333330003333888FFFFFFFFFF888FF30000000000000000003
            3888888888888888888F30F7777777777777770338F3333333333333338F30F7
            777777777777770338F3333333333333338F30F7777777777799770338F33333
            33333333338F30FFFFFFFFFFFFFFFF0338FFFFFFFFFFFFFFFF8F380088888888
            8888008338888888888888888883333000000000000003333338888888888888
            8F333330888888888888033333388888888888888F3333300000000000000333
            333888888888888883333333333333333333333333333333333333FFFFF33333
            3333333338000833333333333333388888F333333333333330EFE0333333FFFF
            FFFFF83338F33338000000000EF008333338888888888F3888333330E4EFEFEF
            8FE033333338F8FFFFFF8F383FF33338000000000EF0083333388888888883F8
            88F333333333333330EFE03333333333333338FFF8F333333333333338000833
            33333333333338888833}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bExportPreferenciasClick
        end
        inherited bDisenador: TSpeedButton
          Left = 480
          Top = 161
          OnClick = bDisenadorClick
        end
        object bEvaluaArchivo: TSpeedButton [11]
          Left = 480
          Top = 94
          Width = 25
          Height = 25
          Hint = 'Evaluar Nombre de Archivo'
          Enabled = False
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777700000007777777777777777700000007777777774F77777700000007777
            7777444F77777000000077777774444F777770000000700000444F44F7777000
            000070FFF444F0744F777000000070F8884FF0774F777000000070FFFFFFF077
            74F77000000070F88888F077774F7000000070FFFFFFF0777774F000000070F8
            8777F07777774000000070FFFF00007777777000000070F88707077777777000
            000070FFFF007777777770000000700000077777777770000000777777777777
            777770000000}
          ParentShowHint = False
          ShowHint = True
          OnClick = bEvaluaArchivoClick
        end
        inherited cbBitacora: TCheckBox
          Left = 111
          Top = 190
          TabOrder = 9
        end
        object RE_REPORTE: TZetaDBEdit
          Left = 111
          Top = 163
          Width = 334
          Height = 21
          TabOrder = 8
          Text = 'RE_REPORTE'
          DataField = 'RE_REPORTE'
          DataSource = DataSource
        end
        object RE_ARCHIVO: TZetaDBEdit
          Left = 111
          Top = 96
          Width = 334
          Height = 21
          Enabled = False
          TabOrder = 6
          Text = 'RE_ARCHIVO'
          OnChange = RE_ARCHIVOChange
          DataField = 'RE_ARCHIVO'
          DataSource = DataSource
        end
        object RE_COPIAS: TZetaDBNumero
          Left = 111
          Top = 70
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 2
          Text = '0'
          DataField = 'RE_COPIAS'
          DataSource = DataSource
        end
        object UpDown: TUpDown
          Left = 144
          Top = 70
          Width = 16
          Height = 21
          Associate = RE_COPIAS
          Min = 1
          Position = 1
          TabOrder = 3
        end
        object EPaginaInicial: TEdit
          Left = 271
          Top = 70
          Width = 33
          Height = 21
          TabOrder = 4
          Text = '1'
        end
        object EPaginaFinal: TEdit
          Left = 326
          Top = 70
          Width = 39
          Height = 21
          TabOrder = 5
          Text = '9999'
        end
        object RE_PRINTER: TComboBox
          Left = 111
          Top = 43
          Width = 363
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 1
          OnChange = RE_PRINTERChange
        end
        object RE_PFILE: TZetaDBKeyCombo
          Left = 111
          Top = 16
          Width = 208
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          DropDownCount = 10
          ItemHeight = 13
          TabOrder = 0
          OnChange = RE_PFILEChange
          ListaFija = lfFormatoFormas
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
          DataField = 'RE_PFILE'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object NombreArchivoFormula: TMemo
          Left = 111
          Top = 120
          Width = 334
          Height = 33
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 7
        end
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 340
    Width = 559
    DesignSize = (
      559
      34)
    inherited OK: TBitBtn
      Left = 398
    end
    inherited Cancelar: TBitBtn
      Left = 476
    end
  end
  inherited StatusBarRep: TStatusBar
    Top = 374
    Width = 559
  end
end
