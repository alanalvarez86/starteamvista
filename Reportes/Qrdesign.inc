{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{          THE UNREGISTERED VERSION OF QRDESIGN IS                    }
{          NOT AFFECTED BY CHANGES TO THIS FILE!                      }
{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}

{---------------------------------------------------------------------}
{--- Check for active compiler (do not change)                    ----}
{---------------------------------------------------------------------}
{$IFDEF VER130} {$DEFINE VER120} {$DEFINE CK} {$ENDIF}  {Delphi 5}  {-}
{                                                         + BCB 5}  {-}
{$IFDEF VER125} {$DEFINE VER120} {$DEFINE CK} {$ENDIF}  {   BCB 4}  {-}
{$IFDEF VER120} {$DEFINE VER100} {$DEFINE CK} {$ENDIF}  {Delphi 4}  {-}
{$IFDEF VER110} {$DEFINE VER100} {$DEFINE CK} {$ENDIF}  {   BCB 3}  {-}
{$IFDEF VER100}                  {$DEFINE CK} {$ENDIF}  {Delphi 3}  {-}
{$IFDEF VER90}  {$DEFINE VER80}  {$DEFINE CK} {$ENDIF}  {Delphi 2}  {-}
{$IFDEF VER93}                   {$DEFINE CK} {$ENDIF}  {   BCB 1}  {-}
{$IFDEF VER80}                   {$DEFINE CK} {$ENDIF}  {Delphi 1}  {-}
{$IFNDEF CK}    { if unknown compiler, try D5 compatibility :-}     {-}
  {$DEFINE VER130}  {$DEFINE VER120} {$DEFINE VER100}               {-}
{$ENDIF}                                                            {-}
{$UNDEF CK}                                                         {-}
{---------------------------------------------------------------------}
{---------------------------------------------------------------------}

{---------------------------------------------------------------------}
{----- Quickreport 3 support                                     -----}
{----- enabled for Delphi 4+5 and BCB 4 by default               -----}
{---------------------------------------------------------------------}
{$IFDEF VER120}
  {$DEFINE QR3}     { use Quickreport 3 with Delphi 4, Delphi 5, BCB 4}
{$ELSE}
  {.$DEFINE QR3}     { use Quickreport 3 with D1..D3, BCB 1+3 }
{$ENDIF}

{---------------------------------------------------------------------}
{----- registered version of QRDesign                            -----}
{---------------------------------------------------------------------}
{$DEFINE REGISTERED}

{---------------------------------------------------------------------}
{----- enables QRDesign's script language for event handlers     -----}
{---------------------------------------------------------------------}
{$DEFINE Scripts}

{---------------------------------------------------------------------}
{----- Enables JPEG support                                      -----}
{-----                                                           -----}
{----- Enable the compiler symbol for your compiler if you want  -----}
{----- QRDesign to be able to use JPEG images                    -----}
{----- Note: You need the JPEG unit in your library search path! -----}
{-----       This unit is included with all 32-bit versions of   -----}
{-----       Delphi and BCB but it might not be in the search    -----}
{-----       path by default.                                    -----}
{---------------------------------------------------------------------}
{$IFDEF VER130}
  {.$DEFINE JPEG}          { JPEG support with Delphi 5 }
{$ELSE}
{$IFDEF VER125}
  {.$DEFINE JPEG}         { JPEG support with BCB 4 }
{$ELSE}
{$IFDEF VER120}
  {.$DEFINE JPEG}         { JPEG support with Delphi 4 }
{$ELSE}
{$IFDEF VER110}
  {.$DEFINE JPEG}         { JPEG support with BCB 3 }
{$ELSE}
{$IFDEF VER100}
  {.$DEFINE JPEG}          { JPEG support with Delphi 3 }
{$ELSE}
{$IFDEF VER90}
  {.$DEFINE JPEG}          { JPEG support with Delphi 2 }
{$ELSE}
{$IFDEF VER80}
  {.$DEFINE JPEG}          { JPEG support with Delphi 1 }
{$ENDIF}
{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}


{---------------------------------------------------------------------}
{----- activate to set Inches as default unit instead of MM         --}
{---------------------------------------------------------------------}
{.$DEFINE Default_Inch}

{---------------------------------------------------------------------}
{----- activate to set "Letter" paper size as default instead of A4 --}
{---------------------------------------------------------------------}
{.$DEFINE Default_Letter}

{---------------------------------------------------------------------}
{----- The following compiler symbols define the SQL delimiters  -----}
{----- ( [...], "...", '...' etc.) used                          -----}
{----- See also the "SQLSettings" property !!                    -----}
{---------------------------------------------------------------------}
{.$DEFINE UseAccessSQL}
{.$DEFINE UseOracleSQL}
{.$DEFINE UseSingleQuotesSQL}
{.$DEFINE UseDoubleQuotesSQL}
{$DEFINE UseNoQuotesSQL}

{---------------------------------------------------------------------}
{----- set the default report editor form (see QRDESIGN.HLP)     -----}
{---------------------------------------------------------------------}
{.$DEFINE Default_Form}
{$DEFINE DefaultWithFlatSpeedbuttons_Form}
{.$DEFINE RXControls_Form}                   {uses QRDFRMRX.PAS}
{.$DEFINE Toolbar97_Form}                    {uses QRDFRM97.PAS}
{.$DEFINE Custom_Form}                       {uses QRDFRMCU.PAS}

{---------------------------------------------------------------------}
{----- include the report expert                                 -----}
{---------------------------------------------------------------------}
{.$DEFINE ExcludeReportExpert}

{---------------------------------------------------------------------}
{----- runtime dataset edit/create                               -----}
{---------------------------------------------------------------------}
{.$DEFINE DisableRuntimeDatasetEdit}

{---------------------------------------------------------------------}
{----- include Teechart component                                -----}
{----- IMPORTANT: IF YOU ARE USING TEECHART 4, YOU MUST HAVE THE -----}
{-----            THE LICENSED TEECHART 4 PRO VERSION !!!!       -----}
{-----            ELSE SEE QRDESIGN.HLP FOR MORE INFORMATION     -----}
{---------------------------------------------------------------------}
{$IFDEF REGISTERED}
  {$IFDEF VER120}
    {$DEFINE Teechart}      {--- use Teechart in Delphi 4 and 5 ---}
      {$DEFINE Teechart4}    {--- installed Teechart is version 4 Pro ---}
  {$ELSE}
  {$IFDEF VER110}
    {.$DEFINE Teechart}      {--- use Teechart in C++ Builder 3 ---}
      {.$DEFINE Teechart4}   {--- installed Teechart is version 4 Pro ---}
  {$ELSE}
  {$IFDEF VER100}
    {$DEFINE Teechart}       {--- use Teechart in Delphi 3 ---}
      {.$DEFINE Teechart4}   {--- installed Teechart is version 4 Pro---}
  {$ELSE}
    {...}
    {.$DEFINE Teechart}      {--- use Teechart in Delphi 1, 2 and C++ Builder 1 ---}
      {.$DEFINE Teechart4}   {--- installed Teechart is version 4 Pro ---}
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
{$ENDIF}


{---------------------------------------------------------------------}
{----- include support for "QuickReport 2 PowerPack" components  -----}
{----- TQRPExprCheckbox and TQRPGrid                             -----}
{----- (QR2PP can be found on our website at http://www.thsd.de) -----}
{---------------------------------------------------------------------}
{.$DEFINE QR2PP}

{---------------------------------------------------------------------}
{----- include support for third-party RTF components            -----}
{----- (this components are NOT included in QRDesign, so you     -----}
{----- need to download and install them first, before you can   -----}
{----- compile QRDesign)                                         -----}
{---------------------------------------------------------------------}
{.$DEFINE WPTools}        { use WPTools richtext component,
                            http://www.wptools.com }
{.$DEFINE Richtext98}     { use Richtext98 component,
                            http://www.qusoft.com }
{.$DEFINE RichtextIP2000} { use InfoPower 2000 richtext component,
                            http://www.qusoft.com and
                            http://www.woll2woll.com }
{.$DEFINE RichtextRX}     { use Rx Lib, http://www.qusoft.com and
                            http://rx.demo.ru }


{---------------------------------------------------------------------}
{----- include QRDADDON.PAS with user-defined report components  -----}
{---------------------------------------------------------------------}
{.$DEFINE IncludeCustomComponents}

  {$IFDEF IncludeCustomComponents}

    {----------------------------------------------------------------------}
    {----- The following user-defined report components are available -----}
    {----- by default. You can use them just by activating the        -----}
    {----- compiler symbols below. QRDesign takes care of the rest.   -----}
    {----- (remember to also activate "IncludeCustomComponents" above)-----}
    {----------------------------------------------------------------------}

    {.$DEFINE Zorn_Barcode}        { include Zorn Barcode support }
    {.$DEFINE Schlottke_Barcode}   { include Schlottke Barcode support }
    {.$DEFINE Poulsen_Barcode}     { include support for Allan Poulsen's component }
    {.$DEFINE Mountaintop_Barcode} { include support for Mountaintop's barcode component }

  {$ENDIF}

{---------------------------------------------------------------------}
{----- Don't use TQuery components (makes sense when using Apollo) ---}
{---------------------------------------------------------------------}
{.$DEFINE NoTQueries}

{---------------------------------------------------------------------}
{----- language settings for QRDesign forms                      -----}
{---------------------------------------------------------------------}
{$DEFINE MultiLanguageSupport}
            
{---------------------------------------------------------------------}
{----- load files saved with QRDesign 1.07...1.08                -----}
{---------------------------------------------------------------------}
{.$DEFINE QRDesign108compatibility}

{---------------------------------------------------------------------}
{----- load files saved with QRDesign 0.94...1.06                -----}
{---------------------------------------------------------------------}
{$DEFINE QRDesign106compatibility}

{---------------------------------------------------------------------}
{----- load files saved with QRDesign version 0.93 or prior      -----}
{---------------------------------------------------------------------}
{.$DEFINE QRDesign093compatibility}

{---------------------------------------------------------------------}
{----- use ADOExpress' TAdoTable and TAdoQuery                   -----}
{---------------------------------------------------------------------}
{.$DEFINE USE_ADOEXPRESS}

{.$DEFINE ADO_CLIENTCURSOR}  { activate to use ADO client-side cursor }
{.$DEFINE ADO_LOCKTYPEREADONLY} { open ADO datasets with ltReadOnly   }
{$DEFINE ADO_TABLEDIRECT} { set TAdoTable.TableDirect property to TRUE}
{---------------------------------------------------------------------}
{----- use DBIsam's TDBIsamTable and TDBIsamQuery                -----}
{---------------------------------------------------------------------}
{.$DEFINE USE_DBISAM}

{---------------------------------------------------------------------}
{----- use Infopower's TwwTable and TwwQuery instead of TTable...-----}
{---------------------------------------------------------------------}
{.$DEFINE USE_INFOPOWER}
{---------------------------------------------------------------------}
{----- use Infopower's TwwFilterDialog to allow Table filtering  -----}
{---------------------------------------------------------------------}
{.$DEFINE USE_INFOPOWER_FILTERDIALOG}

{---------------------------------------------------------------------}
{----- shows X/Y position of selected reportelement in infopanel -----}
{---------------------------------------------------------------------}
{$DEFINE ShowElementPosition}

{---------------------------------------------------------------------}
{----- Select a report element's parent band after the element    ----}
{----- is deleted (this may scroll the current report view)       ----}
{---------------------------------------------------------------------}
{.$DEFINE Select_parent_after_delete}

{---------------------------------------------------------------------}
{----- Autoscroll if reportelement is dragged out of visible area ----}
{---------------------------------------------------------------------}
{.$DEFINE AutoScrollAfterMove}

{---------------------------------------------------------------------}
{----- Don't show popup menu when report element is right-clicked ----}
{---------------------------------------------------------------------}
{.$DEFINE NoPopupMenu}

{---------------------------------------------------------------------}
{----- automatically save QRDESIGN.INI with report designer setup ----}
{----- to the application directory when closing the report editor----}
{---------------------------------------------------------------------}
{$DEFINE AutoSaveSetup}

{---------------------------------------------------------------------}
{----- deactivate if you don't want a dataset selection combobox -----}
{----- in report element property forms                          -----}
{---------------------------------------------------------------------}
{$DEFINE AllowDatasetSelection}

{---------------------------------------------------------------------}
{----- Activate if you want a dataset selection dialog to appear -----}
{----- when a new report element is inserted                     -----}
{---------------------------------------------------------------------}
{.$DEFINE ShowDatasetSelectionDialog}

{---------------------------------------------------------------------}
{----- Deactivate if you don't want the end user to be able to   -----}
{----- change filter conditions for TTable and/or TQuery         -----}
{----- (only supported under 32 bit)                             -----}
{---------------------------------------------------------------------}
{$DEFINE AllowTableFilters}
{$DEFINE AllowQueryFilters}

{---------------------------------------------------------------------}
{----- Open all tables of the report in ReadOnly mode            -----}
{---------------------------------------------------------------------}
{$DEFINE TablesReadonly}

{---------------------------------------------------------------------}
{----- Open all TQueries in UniDirectional mode                  -----}
{---------------------------------------------------------------------}
{.$DEFINE UniDirectionalQueries}




{----------------------------------------------------------------------------------}
{----------------------------------------------------------------------------------}
{--------------------- DO NOT CHANGE ANYTHING BELOW THIS LINE ---------------------}
{----------------------------------------------------------------------------------}
{----------------------------------------------------------------------------------}

{$B-}
{$IFDEF WIN32}
  {$IFOPT H+}
    {$DEFINE HugeStrings}
  {$ENDIF}
  {$A-,H-,J+}
{$ENDIF}

{----------------------------------------------------------------------------------}

{$IFNDEF REGISTERED}
  {$UNDEF IncludeCustomComponents}
  {$UNDEF DefaultWithFlatSpeedbuttons_Form}
  {$UNDEF RXControls_Form}
  {$UNDEF Toolbar97_Form}
  {$UNDEF Default_Form}
  {$UNDEF WPTools}
  {$UNDEF Richtext98}
  {$UNDEF RichtextIP2000}
  {$UNDEF RichtextRX}
{$ENDIF}

{$IFDEF DefaultWithFlatSpeedbuttons_Form}
  {$IFNDEF VER100}
    {$UNDEF DefaultWithFlatSpeedbuttons_Form}
  {$ENDIF}
{$ENDIF}

{$IFNDEF USE_INFOPOWER}
  {$UNDEF USE_INFOPOWER_FILTERDIALOG}
{$ENDIF}

{$IFDEF QRDesign106Compatibility}
  {$DEFINE QRDesign108Compatibility}
{$ENDIF}
{$IFDEF QRDesign093Compatibility}
  {$DEFINE QRDesign108Compatibility}
{$ENDIF}

{$IFDEF QR3}
  {$DEFINE LinkBand}
{$ENDIF}

{$IFNDEF Teechart}
  {$UNDEF Teechart4}
{$ENDIF}

{$IFDEF USE_DBISAM}
  {$UNDEF QRDesign108compatibility}
  {$UNDEF QRDesign106compatibility}
  {$UNDEF QRDesign093compatibility}
{$ENDIF}

{$IFDEF USE_ADOEXPRESS}
  {$UNDEF QRDesign108compatibility}
  {$UNDEF QRDesign106compatibility}
  {$UNDEF QRDesign093compatibility}
{$ENDIF}

{$DEFINE RichtextDefault}

{$IFDEF WPTools}
  {$UNDEF Richtext98}
  {$UNDEF RichtextIP2000}
  {$UNDEF RichtextRX}
  {$UNDEF RichtextDefault}
{$ENDIF}

{$IFDEF Richtext98}
  {$UNDEF WPTools}
  {$UNDEF RichtextIP2000}
  {$UNDEF RichtextRX}
  {$UNDEF RichtextDefault}
{$ENDIF}

{$IFDEF RichtextIP2000}
  {$UNDEF WPTools}
  {$UNDEF Richtext98}
  {$UNDEF RichtextRX}
  {$UNDEF RichtextDefault}
{$ENDIF}

{$IFDEF RichtextRX}
  {$UNDEF WPTools}
  {$UNDEF Richtext98}
  {$UNDEF RichtextIP2000}
  {$UNDEF RichtextDefault}
{$ENDIF}

{$IFNDEF WIN32}
  {$UNDEF RichtextDefault}
{$ENDIF}
