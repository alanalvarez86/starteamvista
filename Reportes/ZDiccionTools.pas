unit ZDiccionTools;
{$INCLUDE DEFINES.INC}

interface

uses
  SysUtils, Classes, Controls, DB, Mask, Dialogs, Forms, FileCtrl, stdCtrls, DBClient, Variants, MaskUtils,
  {$IFDEF DOS_CAPAS}
  DZetaServerProvider,
  {$ENDIF}
  ZetaCommonClasses, ZetaCommonTools, ZetaCommonLists;

function GetMascaraDefault(const eTipo: eTipoGlobal): string;

{$IFDEF RDD}
procedure DefaultDato(cdsDataset: TClientDataset; Tipo: eTipoGlobal; AnchoField: integer); overload;
{$IFDEF DOS_CAPAS}
procedure DefaultDato(cdsDataset: TClientDataset; oField: TField); overload;
{$ELSE}
procedure DefaultDato(cdsDataset: TClientDataset; oField: TField); overload;
{$ENDIF}
{$ELSE}
{$ENDIF}

const
  K_MASCARA_INT  = '#0;-#0';
  K_MASCARA_DATE = 'dd/mmm/yy';

implementation

function GetMascaraDefault(const eTipo: eTipoGlobal): string;
begin
  case eTipo of
    tgFloat:
      Result := K_MASCARA_FLOAT;
    tgNumero:
      Result := K_MASCARA_INT;
    tgFecha:
      Result := K_MASCARA_DATE
    else
      Result := '';
  end;
end;

{$IFDEF DOS_CAPAS}
function GetTipoDefault(oField: TZetaField): eTipoGlobal;
{$ELSE}
function GetTipoDefault(oField: TField): eTipoGlobal;
{$ENDIF}
begin
  {$IFDEF DOS_CAPAS}
  if oField.IsText then begin
    if oField.SQLLen = 1 then
      Result := tgBooleano
    else
      Result := tgTexto
  end else if oField.IsNumeric then begin
    if oField.SQLScale = 0 then
      Result := tgNumero
    else
      Result := tgFloat;
  end else if oField.IsDateTime then
    Result := tgFecha
  else
    Result := tgAutomatico;
  {$ELSE}
  case oField.DataType of
    ftString, ftMemo, ftBlob, ftFmtMemo:
      Result := tgTexto;
    ftSmallint, ftInteger, ftWord, ftAutoInc:
      Result := tgNumero;
    ftBoolean:
      Result := tgBooleano;
    ftFloat, ftCurrency, ftBCD, ftFMTBcd, ftTime:
      Result := tgFloat;
    ftDate, ftDateTime:
      Result := tgFecha;
    else
      Result := tgAutomatico;
  end;
  {$ENDIF}
end;

{$IFDEF RDD}
procedure DefaultDato(cdsDataset: TClientDataset; Tipo: eTipoGlobal; AnchoField: integer); overload;
var
  TipoRango       : eTipoRango;
  iAncho, AtFiltro: integer;
  AtTotal         : eTipoOperacionCampo;

begin
  iAncho    := 0;
  AtFiltro  := 0;
  TipoRango := eTipoRango(0);

  with cdsDataset do begin
    AtTotal := ocNinguno;

    case Tipo of
      tgBooleano: begin
          TipoRango := rBool;
          iAncho    := 1;
          AtFiltro  := 0;
        end;
      tgFloat: begin
          TipoRango := rNinguno;
          iAncho    := 15;
          AtFiltro  := Ord(dfDiferentes);
          AtTotal   := ocSuma;
        end;
      tgNumero: begin
          TipoRango := rNinguno;
          iAncho    := 7;
          AtFiltro  := Ord(dfDiferentes);
        end;
      tgFecha: begin
          TipoRango := rFechas;
          iAncho    := 9;
          AtFiltro  := Ord(rfHoy);
        end;
      tgTexto: begin
          TipoRango := rNinguno;
          AtFiltro  := Ord(dfDiferentes);
          case AnchoField of
            1: begin
                Tipo      := tgBooleano;
                iAncho    := 1;
                TipoRango := rBool;
              end;
            6: begin
                TipoRango := rRangoEntidad;
                AtFiltro  := Ord(raTodos);
                iAncho    := AnchoField;
              end
            else
              iAncho := iMin(AnchoField, 30);
          end;
        end;
    end;

    cdsDataset.FieldByName('AT_ANCHO').AsInteger   := iAncho;
    cdsDataset.FieldByName('AT_TIPO').AsInteger    := Ord(Tipo);
    cdsDataset.FieldByName('AT_MASCARA').AsString  := GetMascaraDefault(Tipo);
    cdsDataset.FieldByName('AT_FILTRO').AsInteger  := AtFiltro;
    cdsDataset.FieldByName('AT_TRANGO').AsInteger  := Ord(TipoRango);
    cdsDataset.FieldByName('AT_ENTIDAD').AsInteger := 0;
    cdsDataset.FieldByName('LV_CODIGO').AsInteger  := 0;
    cdsDataset.FieldByName('AT_SISTEMA').AsString  := K_GLOBAL_SI;
    cdsDataset.FieldByName('AT_VALORAC').AsInteger := 0;
    cdsDataset.FieldByName('AT_TOTAL').AsInteger   := Ord(AtTotal);

  end;
end;

{$IFDEF DOS_CAPAS}
procedure DefaultDato(cdsDataset: TClientDataset; oField: TField); overload;
{$ELSE}
procedure DefaultDato(cdsDataset: TClientDataset; oField: TField); overload;
{$ENDIF}
var
  iAncho: integer;
begin
  {$IFDEF DOS_CAPAS}
  iAncho := oField.SQLLen;
  {$ELSE}
  iAncho := oField.Size;
  {$ENDIF}
  if oField.DataType in [ftSmallint, ftWord] then begin
    iAncho := iMin(2, iAncho);
  end;

  cdsDataset.FieldByName('AT_TIPO').AsInteger := Ord(GetTipoDefault(oField));
  DefaultDato(cdsDataset, GetTipoDefault(oField), iAncho);

end;
{$ELSE}
{$ENDIF}

end.
