unit FListaConceptos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls,
  ZBaseDlgModal,
  ZReportTools;

type
  TListaConceptos = class(TZetaDlgModal)
    cbListaConceptos: TCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function GetTiposConceptos: TiposConcepto;
    { Private declarations }
  public
    { Public declarations }
    property Conceptos : TiposConcepto read GetTiposConceptos;
  end;

var
  ListaConceptos: TListaConceptos;

implementation

uses
ZetaCommonLists;

{$R *.DFM}

procedure TListaConceptos.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaCommonLists.LlenaLista( lfTipoConcepto,  cbListaConceptos.Items );
end;

procedure TListaConceptos.FormShow(Sender: TObject);
 var
    i : integer;
begin
     inherited;
     for i:= 0 to Ord( High(eTipoConcepto) ) do
     begin
          cbListaConceptos.Checked[ i ] := ( i in [Ord(coPercepcion), Ord(coDeduccion)] );
     end;
end;

function TListaConceptos.GetTiposConceptos: TiposConcepto;
 var
    i : integer;
begin
     Result := [];
     for i:= 0 to Ord( High(eTipoConcepto) ) do
     begin
          if cbListaConceptos.Checked[ i ] then
             Result := Result + [eTipoConcepto( i )];
     end;
end;

end.
