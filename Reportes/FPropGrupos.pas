unit FPropGrupos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  DBCtrls, ComCtrls, StdCtrls, checklst, ZetaFecha, Mask, Buttons,
  ZetaSmartLists,
  ZetaKeyCombo,
  ZetaTipoEntidad,
  ZetaCommonClasses,
  ZetaCommonLists;

type
  TPropGrupos = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ZetaSmartList: TZetaSmartLists;
    GroupBox4: TGroupBox;
    LBCampos: TZetaSmartListBox;
    BAgregaCampo: TBitBtn;
    BBorraCampo: TBitBtn;
    BArribaCampos: TZetaSmartListsButton;
    BAbajoCampos: TZetaSmartListsButton;
    BFormula: TBitBtn;
    SpeedButton1: TSpeedButton;
    GBPropiedadesCampo: TGroupBox;
    LTituloCampo: TLabel;
    lFormulaCampo: TLabel;
    lMascaraCampo: TLabel;
    lAnchoCampo: TLabel;
    lTipoCampo: TLabel;
    CBMascaraCampo: TComboBox;
    ETituloCampo: TEdit;
    EAnchoCampo: TEdit;
    bFormulaCampo: TBitBtn;
    EFormulaCampo: TMemo;
    CBTipoCampo: TZetaKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure ZetaSmartListAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure CampoOpcionesClick(Sender: TObject);
    procedure bFormulaCampoClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure BBorraCampoClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BFormulaClick(Sender: TObject);
    procedure CBTipoCampoClick(Sender: TObject);
    procedure LBCamposDblClick(Sender: TObject);
  private
    FEntidad : TipoEntidad;
    FParametros : TStrings;
    procedure AsignaProcCampos( oProc : TNotifyEvent );
    procedure SetLista( oLista : TStrings );
    function GetLista : TStrings;
    procedure EnabledCampos( const bEnable, bFormula, bAuto : Boolean );
    procedure LimpiaCampos;
  public
    property Lista : TStrings read GetLista write SetLista;
    property Entidad : TipoEntidad read FEntidad write FEntidad;
    property Parametros: TStrings read FParametros write FParametros;
  end;

function PropGrupos( const iEntidad : TipoEntidad; sCaption : String; oLista, oParametros : TStrings ) : Boolean;

implementation

uses ZetaCommonTools,
     ZReportTools,
     ZReportToolsConsts,
     ZDiccionTools,
     ZConstruyeFormula;

{$R *.DFM}

var fGrupos: TPropGrupos;

function PropGrupos( const iEntidad : TipoEntidad; sCaption : String; oLista, oParametros : TStrings ) : Boolean;
begin
     if fGrupos = NIL then fGrupos := TPropGrupos.Create( Application.MainForm );
     with fGrupos do
     begin
          Caption := sCaption;
          Lista := oLista;
          Entidad := iEntidad;
          Parametros := oParametros;
          ShowModal;
          Result := fGrupos.ModalResult = mrOK;
          if Result then oLista.Assign( Lista );
     end
end;

procedure TPropGrupos.FormShow(Sender: TObject);
begin
     LimpiaCampos;
     ZetaSmartList.SelectEscogido( 0 );
     BBorraCampo.Enabled := LbCampos.Items.Count > 0;
end;

procedure TPropGrupos.EnabledCampos( const bEnable, bFormula, bAuto : Boolean );
begin
     lTituloCampo.Enabled := bEnable;
     ETituloCampo.Enabled := bEnable;

     lFormulaCampo.Enabled := bFormula;
     EFormulaCampo.Enabled := bFormula;
     bFormulaCampo.Enabled := bEnable;

     lMascaraCampo.Enabled := bEnable ;
     CBMascaraCampo.Enabled := bEnable ;

     lAnchoCampo.Enabled := bEnable;
     EAnchoCampo.Enabled := bEnable;

     lTipoCampo.Enabled := bFormula;
     CBTipoCampo.Enabled := bFormula;
end;

procedure TPropGrupos.ZetaSmartListAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     if Objeto <> NIL then
     begin
          AsignaProcCampos( NIL );

          EFormulaCampo.Text := TCampoOpciones( Objeto ).Formula;
          ETituloCampo.Text := TCampoOpciones( Objeto ).Titulo;
          GetListaMascaras( CBMascaraCampo.Items, TCampoOpciones( Objeto ).TipoCampo );
          CBMascaraCampo.Text := TCampoOpciones( Objeto ).Mascara;
          EAnchoCampo.Text := IntToStr( TCampoOpciones( Objeto ).Ancho );
          CBTipoCampo.Enabled := TCampoOpciones( Objeto ).Calculado = -1;
          CBTipoCampo.ItemIndex := Ord( TCampoOpciones( Objeto ).TipoCampo );
          AsignaProcCampos( CampoOpcionesClick );
          EnabledCampos( TCampoOpciones( Objeto ).Calculado <> K_RENGLON_NUEVO,
                         TCampoOpciones( Objeto ).Calculado = -1,
                         TCampoOpciones( Objeto ).TipoCampo = tgAutomatico );
          if TCampoOpciones( Objeto ).Calculado <> K_RENGLON_NUEVO then
             ZetaSmartList.Control := ETituloCampo
          else ZetaSmartList.Control := LbCampos;
     end
     else
     begin
          EFormulaCampo.Text := VACIO;
          ETituloCampo.Text := VACIO;

          CBMascaraCampo.Text := VACIO;
          EAnchoCampo.Text := '0';
          CBTipoCampo.ItemIndex := 0;
     end;
end;


procedure TPropGrupos.AsignaProcCampos( oProc : TNotifyEvent );
begin
     ETituloCampo.OnExit := oProc;
     EFormulaCampo.OnExit := oProc;
     CBMascaraCampo.OnExit :=  oProc;
     EAnchoCampo.OnExit := oProc;
     CBTipoCampo.OnExit := oProc;
end;

function TPropGrupos.GetLista : TStrings;
begin
     Result := TStrings( LBCampos.Items );
end;

procedure TPropGrupos.SetLista( oLista : TStrings );
begin
     LBCampos.Items.Assign( oLista );
end;

procedure TPropGrupos.LimpiaCampos;
begin
     AsignaProcCampos( NIL );
     EnabledCampos( FALSE, FALSE, FALSE );
     GBPropiedadesCampo.Caption := 'Datos de los Campos';
     EFormulaCampo.Text := VACIO;
     ETituloCampo.Text := VACIO;
     CBMascaraCampo.Text := VACIO;
     EAnchoCampo.Text := '0';
     CBTipoCampo.ItemIndex := 0;
     AsignaProcCampos( CampoOpcionesClick );
end;

procedure TPropGrupos.CampoOpcionesClick(Sender: TObject);
 var  i : integer;
      Objeto : TCampoOpciones;
begin
     i := LBCampos.ItemIndex;
     Objeto := TCampoOpciones( LBCampos.Items.Objects[ i ] );
     if Objeto <> NIL then
        with Objeto do
        begin
             if StrLleno( ETituloCampo.Text ) then Titulo := ETituloCampo.Text
             else Titulo := SINTITULO;
             Formula := EFormulaCampo.Text;
             TipoCampo := eTipoGlobal( CBTipoCampo.ItemIndex );
             Ancho := StrAsInteger( EAnchoCampo.Text );

             EnabledCampos( TRUE, Calculado < 0, TipoCampo = tgAutomatico );
             Mascara := CBMascaraCampo.Text;
             LBCampos.Items[ i ] := Titulo;
        end;
end;

procedure TPropGrupos.bFormulaCampoClick(Sender: TObject);
 var sFormula : string;
begin
     with LBCampos, Items do
          with TCampoOpciones( Objects[ ItemIndex ] ) do
          begin
               sFormula := eFormulaCampo.Text;
               if GetFormulaConstruyeParams( FEntidad , sFormula, eFormulaCampo.SelStart, evReporte, FParametros ) then
               begin
                    eFormulaCampo.Text := sFormula;
                    Calculado := -1;
               end;
               CampoOpcionesClick( self );
               ZetaSmartList.SelectEscogido( ItemIndex );
          end;
end;

procedure TPropGrupos.BAgregaCampoClick(Sender: TObject);
 var oCampo : TCampoOpciones;
     oDiccion : TDiccionRecord;
begin
     inherited;
     if AntesDeAgregarDato( Entidad, TRUE, TRUE, oDiccion, ZetaSmartList ) then
     begin
          oCampo := TCampoOpciones.Create;
          DiccionToCampoMaster(oDiccion,oCampo);
          with oCampo do
          begin
               Mascara := oDiccion.DI_MASCARA;
               Ancho := oDiccion.DI_ANCHO;
          end;
          DespuesDeAgregarDato( oCampo,
                                ZetaSmartList,
                                BBorraCampo,
                                ETituloCampo );
     end;
end;

procedure TPropGrupos.BBorraCampoClick(Sender: TObject);
 var i : integer;
begin
     inherited;
     i := LBCampos.ItemIndex;
     if i >= 0 then
     begin
          LBCampos.Items.Objects[ i ].Free;
          LBCampos.Items.Delete( i );
     end;

     BBorraCampo.Enabled := LBCampos.Items.Count > 0;
     if BBorraCampo.Enabled then
     begin
          LBCampos.ItemIndex := LBCampos.Items.Count - 1;
          LBCampos.OnClick( Self );
     end
     else LimpiaCampos;
end;

procedure TPropGrupos.SpeedButton1Click(Sender: TObject);
 var oSeparador : TCampoListado;
begin
     oSeparador := TCampoListado.Create;
     GetSeparador( TCampoopciones(oSeparador) );

     with LbCampos do
     begin
          Items.InsertObject( ItemIndex+1, oSeparador.Titulo, oSeparador );
          ZetaSmartList.SelectEscogido( ItemIndex+1 );
     end;
     BBorraCampo.Enabled := TRUE;
     BAgregaCampo.SetFocus;
end;


procedure TPropGrupos.FormCreate(Sender: TObject);
begin
     HelpContext := H55152_Grupos_Modificar_Encabezado;
end;

procedure TPropGrupos.BFormulaClick(Sender: TObject);
  var oFormula : TCampoOpciones;
begin
     oFormula := TCampoOpciones.Create;
     with oFormula do
     begin
         Entidad := enFormula;
         Formula := '';
         Titulo := K_TITULO;
         Ancho := 10;
         TipoCampo := tgAutomatico;
         Calculado := -1;
     end;
     with LbCampos do
     begin
          Items.AddObject( oFormula.Titulo, oFormula );
          ZetaSmartList.SelectEscogido( Items.Count - 1 );
     end;
     BBorraCampo.Enabled := TRUE;
     ETituloCampo.SelectAll;
     ETituloCampo.SetFocus;
end;


procedure TPropGrupos.CBTipoCampoClick(Sender: TObject);
begin
     with TCampoOpciones( LBCampos.Items.Objects[ LBCampos.ItemIndex ] ) do
     begin
          GetListaMascaras( CBMascaraCampo.Items, eTipoGlobal( CBTipoCampo.ItemIndex ) );
          CBMascaraCampo.Text := GetMascaraDefault(eTipoGlobal( CBTipoCampo.ItemIndex ));
          {case eTipoGlobal( CBTipoCampo.ItemIndex ) of
               tgBooleano,tgTexto,tgMemo, tgAutomatico : CBMascaraCampo.Text := VACIO;
               tgFecha: CBMascaraCampo.Text := K_MASCARA_DATE;
               tgFloat: CBMascaraCampo.Text := K_MASCARA_FLOAT;
               tgNumero: CBMascaraCampo.Text := K_MASCARA_INT;
          end;}
     end;
     CampoOpcionesClick( Sender );
end;

procedure TPropGrupos.LBCamposDblClick(Sender: TObject);
begin
     //No Hace Nada - NO QUITAR!!!!!
end;

end.
