unit FReportes;

interface

{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ComCtrls, ExtCtrls,FileCtrl,
     StdCtrls, Buttons, Db, Menus, ImgList, Grids, DBGrids,
     ZBaseConsulta,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaKeyCombo,
     ZetaDBGrid,
     ZReportConst,
     DReportes;

type
  TReportes = class(TBaseConsulta)
    PanelSuperior: TPanel;
    ListaSmallImages: TImageList;
    lbClasificacion: TLabel;
    CbTablas: TZetaKeyCombo;
    DBGrid: TZetaDBGrid;
    Panel1: TPanel;
    BImportar: TSpeedButton;
    BExportar: TSpeedButton;
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
    EBuscaNombre: TEdit;
    BBusca: TSpeedButton;
    lbBusca: TLabel;
    PopupMenu1: TPopupMenu;
    mSeleccion: TMenuItem;
    BFavoritos: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure CBTablasChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGridTitleClick(Column: TColumn);
    procedure BExportarClick(Sender: TObject);
    procedure BImportarClick(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure EBuscaNombreChange(Sender: TObject);
    procedure mSeleccionClick(Sender: TObject);
    procedure BFavoritosClick(Sender: TObject);
  private
    //lBorrando : Boolean;
    FClasifiReporte : eClasifiReporte;
    FClasifiTemp : integer;
    procedure ExportaArchivo;
    procedure ImportaArchivo;
    function GetDerechosClasifi: Integer;
    function PuedeConsultar: Boolean;
    function ChecaFavoritos: Boolean;
    function ChecaSuscripciones: Boolean;
    procedure LimpiaGrid;
    procedure FiltraReporte;
    procedure AgregaFavoritos;
    procedure ActualizaForma( const eClasificacion: eClasifiReporte );

  protected

    {********* NUEVA IMPLEMENTACION ********}
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Imprimir;override;

    function PuedeImprimir(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;

    function CheckUnDerecho( const NumeroDerecho, iDerecho : Integer ): Boolean;
  public
    property  ClasifiReporte : eClasifiReporte read FClasifiReporte write FClasifiReporte default crFavoritos;
    procedure DoLookup;override;
  end;

const
     K_COL_IMAGE = 0;
     K_COL_TIPO  = 3;
     K_COL_TABLA = 4;
     K_COL_FECHA = 5;
     K_COL_ESPACIO = 6;
     K_COL_USUARIO = 7;
var
  Reportes: TReportes;

implementation

uses ZetaDialogo,
     ZReportTools,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaBuscador,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaWinAPITools,
     {$IFDEF ADUANAS}
     ZHelpContext,
     {$endif}
     FTressShell,
     DCliente,
     DBaseReportes,
     DDiccionario;

{$R *.DFM}


{************** Forma de Reportes **************}
procedure TReportes.Connect;
begin
     with dmReportes do
     begin
          DataSource.DataSet := cdsReportes;
          // No se necesita. Se hace un Refresh por el COMBO. cdsReportes.Conectar;
     end;
end;

procedure TReportes.Refresh;
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        if PuedeConsultar then
        begin
             with CbTablas do
             begin
                  with dmReportes do
                  begin
                       ClasifActivo := eClasifiReporte( Items.Objects[ ItemIndex ] );
                       Clasificaciones := Items;
                       cdsReportes.Refrescar;
                  end;
             end;

             BExportar.Enabled := not ( dmCliente.ModoSuper or
                                        dmReportes.ChecaFavoritos or
                                        dmReportes.ChecaSuscripciones );

             BImportar.Enabled := BExportar.Enabled;
             BFavoritos.Enabled := not ( dmReportes.ChecaFavoritos or
                                        dmReportes.ChecaSuscripciones );
                                        
             ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'RE_NOMBRE');
             FClasifiTemp:= CbTablas.ItemIndex;
        end
        else
        begin
             if ( FClasifiTemp > -1 ) then CbTablas.ItemIndex:= FClasifiTemp;
        end;
        LimpiaGrid;
        FiltraReporte;

     finally
            Screen.Cursor := oCursor;
     end;

end;

procedure TReportes.LimpiaGrid;
begin
     dbGrid.SelectedRows.Clear;
     if dmReportes.cdsReportes.Active then dbGrid.SelectedRows.Refresh;
end;

procedure TReportes.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     {$ifdef ADUANAS}
     HelpContext := H_EXPLORADOR_DE_REPORTES;
     {$ELSE}
     if dmCliente.ModoSuper then
        HelpContext := 50001
     else
         HelpContext := H5005101_Explorador_reportes_Clasica;
         //HelpContext := H50051_Explorador_reportes; //old
     {$ENDIF}
     ListaSmallImages.ResourceLoad(rtBitmap, 'EXPLORERSMALL', clOlive);

     dmDiccionario.GetListaClasifiFavoritos( CBTablas.Items );

     {$ifdef RDD}
     with CBTablas do
          DropDownCount := Items.Count + 1;
     DBGrid.Columns[K_COL_TABLA].FieldName := 'RE_TABLA';
     {$else}
     CBTablas.DropDownCount := Ord( High( eClasifiReporte ) ) + 1;
     DBGrid.Columns[K_COL_TABLA].FieldName := 'RE_ENTIDAD';
     {$endif}

     {$ifdef SELECCION}
     {
     lbClasificacion.Visible := FALSE;
     CBTablas.Visible := FALSE;
     lbBusca.Left := 5;
     EBuscaNombre.Left := 40;
     BBusca.Left := 165;
     }
     {$endif}

     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_REPORTES;
     {$endif}

     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_REPORTES;
     {$endif}

end;

procedure TReportes.FormShow(Sender: TObject);
const
     K_SIGUIENTE_CLASIFICACION = 2;
begin
     inherited;
     ActualizaForma( crFavoritos );
     with dmReportes do
     begin
          with cbTablas do
          begin
               if ( Items.Count > K_SIGUIENTE_CLASIFICACION ) AND ( cdsReportes.RecordCount = 0 ) and ( ClasifActivo = crFavoritos  ) then
                  ActualizaForma( eClasifiReporte( Items.Objects[ ItemIndex + K_SIGUIENTE_CLASIFICACION ] ) );
          end;
     end;
end;

procedure TReportes.ActualizaForma( const eClasificacion: eClasifiReporte );
begin
     FClasifiReporte := eClasificacion;
     CBTablas.ItemIndex:= dmDiccionario.GetPosClasifi( CBTablas.Items, FClasifiReporte, TRUE );
     CBTablasChange(self);
end;

procedure TReportes.CBTablasChange(Sender: TObject);
begin
     inherited;
     Refresh;
end;

{*********** Eventos de la Lista ********************}
procedure TReportes.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if ( DataCol = 0 ) then
        ListaSmallImages.Draw( DBGrid.Canvas, Rect.Left, Rect.Top + 1,
                               DataSource.DataSet.FieldByName('RE_TIPO').AsInteger )
end;

procedure TReportes.Agregar;
begin
     dmReportes.cdsReportes.Agregar;
end;

procedure TReportes.Borrar;
const
     aMensaje: array[ FALSE..TRUE ] of PChar = ('Borrar','Quitar');
     aVariosReportes: array[ FALSE..TRUE ] of PChar=( '�Desea Borrar los %d Reportes Seleccionados?',
                                                      '�Desea Quitar los %d Reportes Seleccionados de Mis Favoritos?' );
     aUnReporte: array[ FALSE..TRUE ] of PChar=( '�Desea Borrar el Reporte: ' + CR_LF + ' %s?',
                                                 '�Desea Quitar el Reporte: ' + CR_LF + ' %s de Mis Favoritos?' );
var
   i: integer;
    function GetMensaje: String;
    begin
         if ChecaFavoritos then
            Result:= ' de '+ dmReportes.ObtieneClasificacion(crFavoritos)
         else if ChecaSuscripciones then
                Result:= ' de '+ dmReportes.ObtieneClasificacion(crSuscripciones)
             else
                 Result:=VACIO;
    end;
begin
     if ( DBGrid.SelectedRows.Count <= 1 ) then
     begin
          if  dmReportes.ChecaFavoritos or dmReportes.PuedeAbrirCandado then
          begin
               if ZetaDialogo.ZConfirm( Caption, Format( aUnReporte[ dmReportes.ChecaFavoritos ], [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0, mbNo) then
                  dmReportes.BorraReporte;
          end
          else
              ZetaDialogo.zInformation( '� Atenci�n !', 'Este Reporte S�lo Puede Ser Borrado Por Su Autor', 0 );
     end
     else
     begin
          if ZetaDialogo.ZConfirm( Caption, Format( aVariosReportes[ dmReportes.ChecaFavoritos ], [ DBGrid.SelectedRows.Count ] ), 0, mbNo) then
          begin
               with DBGrid do
               begin
                    for i := 0 to SelectedRows.Count -1 do
                    begin
                         DataSource.DataSet.GotoBookmark(pointer(SelectedRows.Items[i]));
                         if dmReportes.PuedeAbrirCandado then
                            dmReportes.BorraReporte
                         else
                             ZetaDialogo.zInformation( '� Atenci�n !', Format( 'El Reporte %s' + CR_LF + 'S�lo Puede Ser Borrado Por Su Autor', [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0 );
                    end;
               end;
          end;
     end;
     LimpiaGrid;
end;

procedure TReportes.Modificar;
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmReportes do
        begin
             if dmCliente.ModoSuper then
             begin
                  SoloLectura := TRUE;
                  SoloImpresion := TRUE;
             end
             else
             begin
                  if not SoloLectura then
                     SoloLectura := not dmReportes.PuedeAbrirCandado;
                  SoloImpresion := SoloLectura;
             end;
             cdsReportes.Modificar;
        end;
     finally
            Screen.Cursor := oCursor;
            with dmReportes do
            begin
                 SoloImpresion := FALSE;
                 SoloLectura := FALSE;
            end;

     end;
end;

procedure TReportes.DBGridTitleClick(Column: TColumn);
begin
     inherited;
     {if Column.Index = 0 then
        DBGrid.OrdenarPor( DBGrid.Columns[2] )
     else if Column.Index = 5 then
          DBGrid.OrdenarPor( DBGrid.Columns[4] )
     else DBGrid.OrdenarPor( Column );}
     with dmReportes do
          case Column.Index of
               K_COL_IMAGE:ZetaClientTools.OrdenarPor(cdsReportes,DBGrid.Columns[K_COL_TIPO].FieldName);
               K_COL_ESPACIO:ZetaClientTools.OrdenarPor(cdsReportes,DBGrid.Columns[K_COL_FECHA].FieldName);
               K_COL_USUARIO:ZetaClientTools.OrdenarPor(cdsReportes,'US_CODIGO')
               else ZetaClientTools.OrdenarPor(cdsReportes,Column.FieldName )
          end;
end;

procedure TReportes.BExportarClick(Sender: TObject);
begin
     inherited;
     ExportaArchivo;
end;

procedure TReportes.BImportarClick(Sender: TObject);
begin
     inherited;
     ImportaArchivo;

end;

procedure TReportes.ExportaArchivo;
 var sError, sNombre, sDirPlantilla : string;
     i{, iCount} : integer;
     FLista : TStrings;
  function RevisaArchivo : string;
  begin
       Result := sNombre;
       Result := StrTransAll( Result, '/', ' ' );
       Result := StrTransAll( Result, '\', ' ' );
       Result := StrTransAll( Result, ':', ' ' );
       Result := StrTransAll( Result, '*', ' ' );
       Result := StrTransAll( Result, '?', ' ' );
       Result := StrTransAll( Result, '"', ' ' );
       Result := StrTransAll( Result, '<', ' ' );
       Result := StrTransAll( Result, '>', ' ' );
       Result := StrTransAll( Result, '|', ' ' );
       Result := StrTransAll( Result, '-', ' ' );
  end;
  function EjecutaSaveDialog : Boolean;
  begin
       Result := TRUE;
       with SaveDialog do
       begin
            if ( i >0 ) then
                 FileName := InitialDir +'\'+RevisaArchivo + '.RP1'
            else
            begin
                if InitialDir = '' then
                   InitialDir := sDirPlantilla;
                FileName := RevisaArchivo;
                Result := Execute;
                if Result then
                   InitialDir := ExtractFileDir(FileName);
            end;
       end;
  end;
  function ExportaReporte : Boolean;
  begin
       sNombre := dmReportes.cdsReportes.FieldByName('RE_NOMBRE').AsString;

       Result := EjecutaSaveDialog;
       if Result then
       begin
            if dmReportes.Exporta( SaveDialog.FileName, sError ) then
               FLista.Add( 'El Reporte:  ' + Comillas(sNombre) + '  fue exportado con �xito')
            else
                if sError > '' then
                   FLista.Add( 'El Reporte:  ' + Comillas(sNombre) + '  no se export� debido al siguiente error: '+ CR_LF+sError );
            {cv: 25-JULIO-2002
            Hay m�dulos en los que el combo de CBTablas (Clasificaci�n de Reportes),
            no est� visible y hay m�dulos en los que el combo esta deshabilitado.}
            if  CBTablas.Visible AND CBTablas.Enabled then
            begin
                 CBTablas.SetFocus;
            end;
       end
  end;
begin
     sDirPlantilla := zReportTools.DirPlantilla;

     if NOT DirectoryExists(sDirPlantilla) then
     begin
          sDirPlantilla := ExtractFileDir( ZetaWinAPITools.GetTempDir );
     end;

     FLista := TStringList.Create;
     try
        dmReportes.cdsReportes.DisableControls;

        if DBGrid.SelectedRows.Count <= 1 then
        begin
             ExportaReporte;
        end
        else
        begin
             if (DBGrid.SelectedRows.Count>0) then
             with dmReportes.cdsReportes do
                  for i:=0 to DBGrid.SelectedRows.Count-1 do
                  begin
                       GotoBookmark(pointer(DBGrid.SelectedRows.Items[i]));
                       if NOT ExportaReporte then
                          Break;
                  end;
        end;
        if FLista.Count >0 then
           ZetaDialogo.ZInformation( Caption, FLista.Text, 0);
     finally
            FreeAndNil(FLista);
            dmReportes.cdsReportes.EnableControls;
     end;
end;

procedure TReportes.ImportaArchivo;
 var sDirPlantilla : string;

 function EjecutaOpenDialog : Boolean;
 begin
      with OpenDialog do
      begin
           if InitialDir = '' then
           begin
                InitialDir := sDirPlantilla;
           end;
           FileName := '';
           Result := Execute;
           if Result then
           begin
                InitialDir := ExtractFileDir(FileName);
           end;
      end;
 end;

 var sError : string;
     lExito :Boolean;
     i : integer;
     FLista : TStrings;
begin
     if dmCliente.EsModoDemo then Exit;

     sDirPlantilla := zReportTools.DirPlantilla;
     if NOT DirectoryExists(sDirPlantilla) then
     begin
          sDirPlantilla := ExtractFileDir( ZetaWinAPITools.GetTempDir );
     end;
     lExito := FALSE;
     FLista := TStringList.Create;
     try
        if EjecutaOpenDialog then
        begin
             for i:= 0 to OpenDialog.Files.Count -1 do
                 if dmReportes.Importa( OpenDialog.Files[i], sError ) then
                 begin
                      FLista.Add('El Reporte:  ' + Comillas( sError ) + '  fue importado con �xito');
                      lExito := TRUE;
                 end
                 else
                 begin
                      if sError > '' then
                         FLista.Add('El Archivo:  ' + Comillas( ExtractFileName( OpenDialog.Files[i] ) ) + '  no se import� debido al siguiente error: '+ CR_LF+sError);
                 end;
             if lExito then CBTablasChange(NIL);
             if CbTablas.Visible AND CBTablas.Enabled then
             begin
                  CBTablas.SetFocus;
             end;
             if FLista.Text > '' then
                ZetaDialogo.ZInformation( Caption, FLista.Text, 0);
        end;
     finally
            FLista.Free;
     end;
end;

procedure TReportes.AgregaFavoritos;
var
   i, iReportes: integer;
begin
     if NoHayDatos then
     begin
          ZetaDialogo.ZInformation( Caption,
                                    'No Hay Ning�n Reporte para Agregar a Mis Favoritos',
                                    0 )
     end
     else
     begin
          if ( DBGrid.SelectedRows.Count <= 1 ) then
          begin
               if ( dmReportes.AgregaFavoritos( DataSource.DataSet ) ) then 
                  ZetaDialogo.ZInformation( Caption,
                                            Format( 'Reporte "%s" Agregado a Mis Favoritos', [ DataSource.DataSet.FieldByName( 'RE_NOMBRE' ).AsString ] ),
                                            0 )
          end
          else
          begin
               iReportes := 0;
               with DBGrid do
               begin
                    for i := 0 to SelectedRows.Count -1 do
                    begin
                         DataSource.DataSet.GotoBookmark( pointer( SelectedRows.Items[ i ] ) );
                         if ( dmReportes.AgregaFavoritos( DataSource.DataSet ) ) then
                            Inc( iReportes );
                    end;
               end;
               if ( iReportes > 0 ) then
                  ZetaDialogo.ZInformation( Caption,
                                            Format('%d Reportes Agregados a Mis Favoritos', [ iReportes ] ),
                                            0 );
          end;
     end;
end;

procedure TReportes.Imprimir;
begin
     dmReportes.SoloLectura := TRUE;
     dmReportes.SoloImpresion := TRUE;
     Modificar;
     dmReportes.SoloLectura := FALSE;
     dmReportes.SoloImpresion := FALSE;
end;

function TReportes.GetDerechosClasifi: Integer;
var
   eClasificacion: eClasifiReporte;
begin
     if ChecaFavoritos or ChecaSuscripciones then
     begin
           with dmReportes do
                eClasificacion := eClasifiReporte( cdsReportes.FieldByName( 'RE_CLASIFI' ).AsInteger )
     end
     else
         with CbTablas do
              eClasificacion := eClasifiReporte( Integer( Items.Objects[ ItemIndex ] ) );

     Result := dmDiccionario.GetDerechosClasifi( eClasificacion );
end;

function TReportes.CheckUnDerecho( const NumeroDerecho, iDerecho : Integer ): Boolean;
begin
     {$ifdef RDD}
     Result:= ZAccesosMgr.CheckUnDerecho( NumeroDerecho, iDerecho );
     {$else}
     Result:= ZAccesosMgr.CheckDerecho( NumeroDerecho, iDerecho );
     {$endif}
end;

function TReportes.PuedeAgregar( var sMensaje: String ): Boolean;
var
     aClasifi: array[ FALSE..TRUE ] of String;
begin
     aClasifi[FALSE] := dmReportes.ObtieneClasificacion(crSuscripciones);
     aClasifi[TRUE] := dmReportes.ObtieneClasificacion(crFavoritos);

     Result := not ( ChecaFavoritos or ChecaSuscripciones );

     if Result then
     begin
          Result := NOT dmCliente.ModoSuper;
          if Result then
          begin
               Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_ALTA );
               if not Result then
                  sMensaje := 'Usuario no Posee Derecho para Agregar en esta Clasificaci�n'
          end
          else sMensaje := 'No se Pueden Agregar Reportes en M�dulo Supervisores';
     end
     else
         sMensaje := Format( '� No se Pueden Agregar Reportes a %s !', [ aClasifi[ ChecaFavoritos ] ] )
end;

function TReportes.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := ChecaFavoritos;
     if not Result then
     begin
          Result := NOT dmCliente.ModoSuper;
          if Result then
          begin
               Result := ChecaSuscripciones or CheckUnDerecho( GetDerechosClasifi, K_DERECHO_BAJA );
               if not Result then
                  sMensaje := 'Usuario no Posee Derecho para Borrar en esta Clasificaci�n';
          end
          else
              sMensaje := 'No se Pueden Borrar Reportes en M�dulo Supervisores';
     end;
end;

function TReportes.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_CAMBIO );

     if not Result then
     begin
          Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_CONSULTA );
          if Result then
          begin
               dmReportes.SoloImpresion := TRUE;
               dmReportes.SoloLectura := TRUE;
          end
          else
              sMensaje := 'Usuario no Posee Derecho para Modificar en esta Clasificaci�n';
     end;
end;

function TReportes.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_IMPRESION );
     if Result then
     begin
          if NoHayDatos then
          begin
               Result := FALSE;
               sMensaje := 'No Hay Ning�n Reporte para Imprimir';
          end;
     end
     else
        sMensaje := 'Usuario no Posee Derecho para Imprimir en esta Clasificaci�n';
end;

function TReportes.PuedeConsultar: Boolean;
begin
     Result := ChecaFavoritos or ChecaSuscripciones;

     if not Result then
     begin
          Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_CONSULTA );
          if not Result then
             ZInformation( Caption, 'Usuario no Posee Derecho para Consultar en esta Clasificaci�n', 0 );
     end;
end;

function TReportes.ChecaFavoritos: Boolean;
begin
     with cbTablas do
          Result := ( eClasifiReporte( Items.Objects[ ItemIndex ] ) = crFavoritos );
end;

function TReportes.ChecaSuscripciones: boolean;
begin
     with cbTablas do
          Result := ( eClasifiReporte( Items.Objects[ ItemIndex ] ) = crSuscripciones );
end;

procedure TReportes.BBuscaClick(Sender: TObject);
begin
     inherited;
     FiltraReporte;
end;

procedure TReportes.FiltraReporte;
begin
     with dmReportes.cdsReportes do
     begin
          Filtered := BBusca.Down;
          Filter := 'UPPER(RE_NOMBRE) LIKE '+ chr(39)+'%'+ UpperCase(EBuscaNombre.Text) + '%' + chr(39);
     end;
     LimpiaGrid;
end;

procedure TReportes.EBuscaNombreChange(Sender: TObject);
begin
     inherited;
     BBusca.Down := Length(EBuscaNombre.Text)>0;
     FiltraReporte;
end;

procedure TReportes.DoLookup;
begin
     LimpiaGrid;
     if EBuscaNombre.Focused then
     begin
          BBusca.Down := NOT BBusca.Down;
          FiltraReporte;
     end
     else EBuscaNombre.SetFocus;
end;

procedure TReportes.mSeleccionClick(Sender: TObject);
begin
     inherited;
     mSeleccion.Checked := NOT mSeleccion.Checked;
     if mSeleccion.Checked then
        DBGrid.Options := DBGrid.Options + [dgMultiSelect]
     else
         DBGrid.Options := DBGrid.Options - [dgMultiSelect]
end;

procedure TReportes.BFavoritosClick(Sender: TObject);
begin
     inherited;
     AgregaFavoritos;
end;

end.


