unit ZImprimeForma;

interface
uses Windows, db, SysUtils,
     {$ifndef VER130}
     Variants,
     {$endif}
     ZReportTools,
     ZetaDialogo,
     ZetaTipoEntidad,
     ZetaCommonLists;

procedure ImprimeUnaForma( const oEntidad : TipoEntidad;
                           oDataSet : TDataset ); overload;

procedure ImprimeUnaForma( const oEntidad : TipoEntidad;
                           oDataSet : TDataSet;
                           const iReporte: integer;
                           const eSalida : TipoPantalla = tgPreview ); overload;

{$ifdef ADUANAS}
procedure ImprimeUnaForma( const EntidadPrincipal: TipoEntidad;
                           const Entity: ArregloEntidades;
                           const ReportType: ArregloReportes;
                           oDataset: TDataset );overload;
{$ENDIF}



implementation

uses FEscogeReporte_DevEx,
     ZetaCommonTools;

procedure PreparaUnaForma( const oEntidad : TipoEntidad;
                           oDataSet : TDataSet;
                           var vCampos,vValores : Variant );
begin
     with oDataSet do
     begin
          case oEntidad of
               {$ifdef TRESS}
               enEmpleado:
               begin
                    vCampos := VarArrayOf(['COLABORA.CB_CODIGO']);
                    vValores := VarArrayOf([ FieldByName('CB_CODIGO').AsString ]);
               end;
               enKardex :
               begin
                    vCampos := VarArrayOf(['KARDEX.CB_CODIGO','KARDEX.CB_FECHA','KARDEX.CB_TIPO']);
                    vValores := VarArrayOf([ FieldByName('CB_CODIGO').AsString,
                                             DateToStrSQLC(FieldByName('CB_FECHA').AsDateTime),
                                             Comillas(FieldByName('CB_TIPO').AsString)]);

               end;
               enIncapacidad:
               begin
                    vCampos := VarArrayOf(['INCAPACI.CB_CODIGO','INCAPACI.IN_FEC_INI']);
                    vValores := VarArrayOf([ FieldByName('CB_CODIGO').AsString,
                                             DateToStrSQLC(FieldByName('IN_FEC_INI').AsDateTime)]);

               end;
               enPermiso:
               begin
                    vCampos := VarArrayOf(['PERMISO.CB_CODIGO','PERMISO.PM_FEC_INI']);
                    vValores := VarArrayOf([ FieldByName('CB_CODIGO').AsString,
                                             DateToStrSQLC(FieldByName('PM_FEC_INI').AsDateTime)]);

               end;
               enVacacion:
               begin
                    vCampos := VarArrayOf(['VACACION.CB_CODIGO','VACACION.VA_FEC_INI','VACACION.VA_TIPO']);
                    vValores := VarArrayOf([ FieldByName('CB_CODIGO').AsString,
                                             Comillas(FormatDateTime('mm/dd/yyyy',FieldByName('VA_FEC_INI').AsDateTime)),
                                             FieldByName('VA_TIPO').AsString]);

               end;
               enConsulta:
               begin
                    vCampos := VarArrayOf(['CONSULTA.EX_CODIGO','CONSULTA.CN_FECHA','CONSULTA.CN_TIPO']);
                    vValores := VarArrayOf([ FieldByName('EX_CODIGO').AsString,
                                             Comillas(FormatDateTime('mm/dd/yyyy',FieldByName('CN_FECHA').AsDateTime)),
                                             Comillas(FieldByName('CN_TIPO').AsString)]);

               end;
               enConcepto:
               begin
                    vCampos := VarArrayOf(['CONCEPTO.CO_NUMERO']);
                    vValores := VarArrayOf([ FieldByName('CO_NUMERO').AsString]);
               end;
               enMovimienBalanza:
               begin
                    vCampos := VarArrayOf(['TMPBALAN.PE_YEAR','TMPBALAN.PE_TIPO','TMPBALAN.PE_NUMERO','TMPBALAN.CB_CODIGO']);
                    vValores := VarArrayOf([ FieldByName('PE_YEAR').AsString,
                                             FieldByName('PE_TIPO').AsString,
                                             FieldByName('PE_NUMERO').AsString,
                                             FieldByName('CB_CODIGO').AsString]);
               end;

               {$endif}
               {$ifdef VISITANTES}
               enLibro:
               begin
                    vCampos := VarArrayOf(['LIBRO.LI_FOLIO']);
                    vValores := VarArrayOf([ FieldByName('LI_FOLIO').AsString]);

               end;
               {$endif}
               {$ifdef ADUANAS}
               enFactura:
               begin
                    vCampos := VarArrayOf(['FACTURA.CB_CODIGO','FACTURA.FA_CODIGO']);
                    vValores := VarArrayOf([ Comillas(FieldByName('CB_CODIGO').AsString),FieldByName('FA_CODIGO').AsString]);
               end;
               enPedimento:
               begin
                    vCampos := VarArrayOf(['PEDIMEN.CB_CODIGO','PEDIMEN.PE_CODIGO']);
                    vValores := VarArrayOf([ Comillas(FieldByName('CB_CODIGO').AsString),Comillas(FieldByName('PE_CODIGO').AsString) ]);
               end;
               {$endif}

          end;
     end;
end;

procedure ImprimeUnaForma( const oEntidad : TipoEntidad;
                           oDataSet : TDataSet );
 var
    vCampos,vValores : Variant;
begin
     if oDataSet.IsEmpty then
     begin
          raise Exception.Create('No Hay Datos para Imprimir');
     end;

     PreparaUnaForma( oEntidad, oDataSet, vCampos, vValores );

     FEscogeReporte_DevEx.ImprimeUnaForma( oEntidad,
                                     trForma,
                                     vCampos,
                                     vValores );
end;

procedure ImprimeUnaForma( const oEntidad : TipoEntidad;
                           oDataSet : TDataSet;
                           const iReporte: integer;
                           const eSalida : TipoPantalla );

 var
    vCampos,vValores : Variant;
begin
     PreparaUnaForma( oEntidad, oDataSet, vCampos,vValores  );
     FEscogeReporte_DevEx.ImprimeUnaForma( iReporte, vCampos, vValores, eSalida );
end;

{$ifdef ADUANAS}
procedure ImprimeUnaForma( const EntidadPrincipal: TipoEntidad;
                           const Entity: ArregloEntidades;
                           const ReportType: ArregloReportes;
                           oDataset: TDataset );overload;
 var
    vCampos,vValores : Variant;
begin
     PreparaUnaForma( EntidadPrincipal, oDataSet, vCampos,vValores  );
     FEscogeReporte.ImprimeUnaForma( Entity, ReportType, vCampos, vValores );
end;
{$ENDIF}

end.
