inherited ImpresionRapida: TImpresionRapida
  Left = 743
  Top = 115
  Caption = 'ImpresionRapida'
  ClientHeight = 417
  ClientWidth = 651
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 651
    TabOrder = 0
  end
  inherited PageControl: TPageControl
    Width = 651
    Height = 334
    TabOrder = 1
    inherited tsGenerales: TTabSheet
      inherited GBParametrosEncabezado: TGroupBox
        Left = 24
        Top = 58
      end
    end
    inherited tsCampos: TTabSheet
      inherited GroupBox4: TGroupBox
        Left = 7
        Top = 2
        Height = 241
        inherited LBCampos: TZetaSmartListBox
          Top = 40
          Height = 196
        end
        object CBBandaSel: TZetaKeyCombo
          Left = 5
          Top = 15
          Width = 188
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          OnChange = CBBandaSelChange
          ListaFija = lfTipoBanda
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object LBEncabezado: TZetaSmartListBox
          Left = 5
          Top = 40
          Width = 188
          Height = 196
          Style = lbOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 2
        end
        object LBPie: TZetaSmartListBox
          Left = 5
          Top = 40
          Width = 188
          Height = 196
          Style = lbOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 3
        end
      end
      inherited BAgregaCampo: TBitBtn
        Left = 7
        Top = 247
        OnClick = BAgregaCampoClick
      end
      inherited BBorraCampo: TBitBtn
        Left = 107
        Top = 247
      end
      inherited BFormula: TBitBtn
        Left = 32
        Top = 276
      end
      inherited GBPropiedadesCampo: TGroupBox
        Left = 221
        Top = 2
        Height = 301
        inherited lFormulaCampo: TLabel
          Top = 43
        end
        inherited lMascaraCampo: TLabel
          Top = 176
        end
        inherited lAnchoCampo: TLabel
          Top = 199
        end
        inherited lTipoCampo: TLabel
          Top = 153
        end
        object lRenglon: TLabel [5]
          Left = 60
          Top = 130
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Rengl'#243'n:'
          Enabled = False
        end
        object lColumna: TLabel [6]
          Left = 161
          Top = 130
          Width = 44
          Height = 13
          Alignment = taRightJustify
          Caption = 'Columna:'
          Enabled = False
        end
        object lCriterioCampo: TLabel [7]
          Left = 51
          Top = 222
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Alineaci'#243'n:'
          Enabled = False
        end
        object lBanda: TLabel [8]
          Left = 69
          Top = 245
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'Banda:'
          Enabled = False
        end
        inherited CBMascaraCampo: TComboBox
          Top = 172
          TabOrder = 6
        end
        inherited bFormulaCampo: TBitBtn
          Top = 99
          OnClick = bFormulaCampoClick
        end
        inherited EFormulaCampo: TMemo
          Top = 43
        end
        inherited CBTipoCampo: TZetaKeyCombo
          Top = 149
          TabOrder = 5
        end
        inherited EAnchoCampo: TEdit
          Top = 195
          TabOrder = 7
        end
        object ERenglon: TZetaNumero
          Left = 108
          Top = 126
          Width = 50
          Height = 21
          Enabled = False
          Mascara = mnDias
          TabOrder = 3
          Text = '0'
          UseEnterKey = True
        end
        object EColumna: TZetaNumero
          Left = 208
          Top = 126
          Width = 50
          Height = 21
          Enabled = False
          Mascara = mnDias
          TabOrder = 4
          Text = '0'
          UseEnterKey = True
        end
        object CBAlineacion: TZetaKeyCombo
          Left = 108
          Top = 218
          Width = 165
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 8
          ListaFija = lfJustificacion
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object CbBanda: TZetaKeyCombo
          Left = 108
          Top = 241
          Width = 165
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 9
          OnChange = CbBandaChange
          ListaFija = lfTipoBanda
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object CBNegrita: TCheckBox
          Left = 108
          Top = 264
          Width = 81
          Height = 17
          Caption = 'Negrita'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
        end
        object CBSubrayado: TCheckBox
          Left = 108
          Top = 280
          Width = 81
          Height = 17
          Caption = 'Subrayado'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          TabOrder = 11
        end
        object CBItalica: TCheckBox
          Left = 188
          Top = 264
          Width = 81
          Height = 17
          Caption = 'It'#225'lica'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsItalic]
          ParentFont = False
          TabOrder = 12
        end
        object CBComprimido: TCheckBox
          Left = 188
          Top = 280
          Width = 85
          Height = 17
          Caption = 'Comprimido'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = 5
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
        end
      end
    end
    inherited tsOrden: TTabSheet
      inherited BArribaOrden: TZetaSmartListsButton
        Left = 211
        Top = 21
      end
      inherited BAbajoOrden: TZetaSmartListsButton
        Left = 211
        Top = 46
      end
      inherited GroupBox2: TGroupBox
        Left = 7
        Top = 17
      end
      inherited BAgregaOrden: TBitBtn
        Top = 230
      end
      inherited BBorraOrden: TBitBtn
        Top = 230
      end
      inherited GroupBoxOrden: TGroupBox
        Left = 241
        Top = 17
        inherited RGOrden: TRadioGroup
          Top = 86
        end
      end
      inherited BAgregaFormulaOrden: TBitBtn
        Top = 259
      end
    end
    inherited tsGrupos: TTabSheet
      inherited BAbajoGrupo: TZetaSmartListsButton
        Left = 211
        Top = 46
      end
      inherited BArribaGrupo: TZetaSmartListsButton
        Left = 211
        Top = 21
      end
      inherited GroupBox6: TGroupBox
        Left = 4
        Top = 17
      end
      inherited BAgregaGrupo: TBitBtn
        Left = 4
        Top = 228
      end
      inherited BBorraGrupo: TBitBtn
        Left = 104
        Top = 228
      end
      inherited GroupBoxGrupo: TGroupBox
        Left = 243
        Top = 17
        inherited bFormulaGrupo: TBitBtn
          Visible = False
        end
      end
      inherited BAgregaFormulaGrupo: TBitBtn
        Left = 29
        Top = 257
      end
    end
    inherited tsFiltros: TTabSheet
      inherited PageControlFiltros: TPageControl
        Left = 22
        Top = 25
        inherited tsFiltroFechas: TTabSheet
          inherited GBRangoEntidad: TGroupBox
            inherited bListaRangoEntidad: TSpeedButton
              Left = 202
              Top = 135
            end
          end
        end
      end
    end
    inherited tsImpresora: TTabSheet
      inherited GroupBox3: TGroupBox
        Left = 29
        Top = 7
        Width = 484
        Height = 291
        object Label1: TLabel [0]
          Left = 74
          Top = 20
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puerto:'
        end
        object bPUERTO: TSpeedButton [1]
          Left = 448
          Top = 14
          Width = 25
          Height = 25
          Hint = 'Impresoras de Red'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000333300
            0000377777F3337777770FFFF099990FFFF07FFFF7FFFF7FFFF7000000999900
            00007777777777777777307703399330770337FF7F37F337FF7F300003399330
            000337777337F337777333333339933333333FFFFFF7F33FFFFF000000399300
            0000777777F7F37777770FFFF099990FFFF07FFFF7F7FF7FFFF7000000999900
            00007777777777777777307703399330770337FF7F37F337FF7F300003399330
            0003377773F7FFF77773333330000003333333333777777F3333333330FFFF03
            3333333337FFFF7F333333333000000333333333377777733333333333077033
            33333333337FF7F3333333333300003333333333337777333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bPUERTOClick
        end
        object lbImpresora: TLabel [2]
          Left = 59
          Top = 47
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Impresora:'
        end
        object Label5: TLabel [3]
          Left = 22
          Top = 74
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'L'#237'neas por Forma:'
        end
        object Label4: TLabel [4]
          Left = 9
          Top = 98
          Width = 99
          Height = 13
          Alignment = taRightJustify
          Caption = 'L'#237'neas Encabezado:'
        end
        object Label7: TLabel [5]
          Left = 36
          Top = 122
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'L'#237'neas Detalle:'
        end
        object Label8: TLabel [6]
          Left = 54
          Top = 146
          Width = 54
          Height = 13
          Alignment = taRightJustify
          Caption = 'L'#237'neas Pie:'
        end
        object RE_PIE: TZetaTextBox [7]
          Left = 113
          Top = 144
          Width = 33
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object EPAGINAS: TLabel [8]
          Left = 224
          Top = 146
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Rango de P'#225'ginas:'
        end
        object lbCopias: TLabel [9]
          Left = 271
          Top = 122
          Width = 45
          Height = 13
          Alignment = taRightJustify
          Caption = '# Copias:'
        end
        object Label9: TLabel [10]
          Left = 243
          Top = 98
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'Compensaci'#243'n:'
        end
        object Label6: TLabel [11]
          Left = 232
          Top = 74
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Compensar Cada:'
        end
        object lTamano: TLabel [12]
          Left = 373
          Top = 74
          Width = 34
          Height = 13
          Caption = 'Formas'
        end
        object Label10: TLabel [13]
          Left = 373
          Top = 98
          Width = 33
          Height = 13
          Caption = 'L'#237'neas'
        end
        object EPAginasAl: TLabel [14]
          Left = 373
          Top = 146
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al:'
        end
        object Bevel1: TBevel [15]
          Left = 9
          Top = 168
          Width = 467
          Height = 98
        end
        object eFuentes: TZetaTextBox [16]
          Left = 61
          Top = 175
          Width = 383
          Height = 36
          AutoSize = False
          ShowAccelChar = False
          WordWrap = True
          Brush.Color = clBtnFace
          Border = True
        end
        object Label11: TLabel [17]
          Left = 15
          Top = 175
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'C'#243'digos:'
        end
        object lbPlantilla: TLabel [18]
          Left = 26
          Top = 220
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Inicial:'
        end
        object lbSeparador: TLabel [19]
          Left = 31
          Top = 244
          Width = 25
          Height = 13
          Caption = 'Final:'
        end
        object BGuardaArchivo: TSpeedButton [20]
          Left = 448
          Top = 175
          Width = 25
          Height = 25
          Hint = 'C'#243'digos de Impresi'#243'n'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333333FFF33FFFFF33333300033000
            00333337773377777333333330333300033333337FF33777F333333330733300
            0333333377FFF777F33333333700000073333333777777773333333333033000
            3333333337FF777F333333333307300033333333377F777F3333333333703007
            33333333377F7773333333333330000333333333337777F33333333333300003
            33333333337777F3333333333337007333333333337777333333333333330033
            3333333333377333333333333333033333333333333733333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BGuardaArchivoClick
        end
        inherited cbBitacora: TCheckBox
          Left = 61
          Top = 269
          TabOrder = 18
        end
        object RE_REPORTE: TDBComboBox
          Left = 113
          Top = 16
          Width = 334
          Height = 21
          DataField = 'RE_REPORTE'
          DataSource = DataSource
          ItemHeight = 13
          Items.Strings = (
            'LPT1'
            'LPT2'
            'LPT3'
            'COM1'
            'COM2'
            'COM3'
            'FILE')
          TabOrder = 0
        end
        object RE_PRINTER: TDBLookupComboBox
          Left = 113
          Top = 43
          Width = 360
          Height = 21
          DataField = 'RE_PRINTER'
          DataSource = DataSource
          KeyField = 'PI_NOMBRE'
          ListField = 'PI_NOMBRE'
          ListSource = dsImpresoras
          TabOrder = 1
        end
        object RE_COLESPA: TZetaDBNumero
          Tag = 2
          Left = 113
          Top = 118
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 6
          Text = '0'
          OnExit = OnCalculaLineas
          DataField = 'RE_COLESPA'
          DataSource = DataSource
        end
        object RE_RENESPA: TZetaDBNumero
          Tag = 1
          Left = 113
          Top = 94
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 4
          Text = '0'
          OnExit = OnCalculaLineas
          DataField = 'RE_RENESPA'
          DataSource = DataSource
        end
        object RE_ANCHO: TZetaDBNumero
          Left = 113
          Top = 70
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 2
          Text = '1000'
          OnExit = OnCalculaLineas
          DataField = 'RE_ANCHO'
          DataSource = DataSource
        end
        object UDLineasForma: TUpDown
          Left = 146
          Top = 70
          Width = 16
          Height = 21
          Associate = RE_ANCHO
          Min = 1
          Max = 1000
          Position = 1000
          TabOrder = 3
          Thousands = False
          Wrap = True
          OnClick = OnCalculaLineasUD
        end
        object UDEncabezado: TUpDown
          Tag = 1
          Left = 146
          Top = 94
          Width = 16
          Height = 21
          Associate = RE_RENESPA
          Max = 1000
          TabOrder = 5
          Thousands = False
          Wrap = True
          OnClick = OnCalculaLineasUD
        end
        object UDDetalle: TUpDown
          Tag = 2
          Left = 146
          Top = 118
          Width = 16
          Height = 21
          Associate = RE_COLESPA
          Max = 1000
          TabOrder = 7
          Thousands = False
          Wrap = True
          OnClick = OnCalculaLineasUD
        end
        object RE_HOJA: TZetaDBNumero
          Left = 320
          Top = 94
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 10
          Text = '0'
          DataField = 'RE_HOJA'
          DataSource = DataSource
        end
        object RE_COPIAS: TZetaDBNumero
          Left = 320
          Top = 118
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 12
          Text = '0'
          DataField = 'RE_COPIAS'
          DataSource = DataSource
        end
        object EPaginaInicial: TZetaNumero
          Left = 320
          Top = 142
          Width = 33
          Height = 21
          Mascara = mnDias
          TabOrder = 14
          Text = '0'
          UseEnterKey = True
        end
        object UDCopias: TUpDown
          Left = 353
          Top = 118
          Width = 13
          Height = 21
          Min = 1
          Position = 100
          TabOrder = 13
          Thousands = False
          Wrap = True
          OnClick = UDCopiasClick
        end
        object UDCompensacion: TUpDown
          Left = 353
          Top = 94
          Width = 13
          Height = 21
          Min = -1000
          Max = 1000
          TabOrder = 11
          Thousands = False
          Wrap = True
          OnClick = UDCompensacionClick
        end
        object UDCompensarCada: TUpDown
          Left = 353
          Top = 70
          Width = 13
          Height = 21
          Max = 1000
          TabOrder = 9
          Thousands = False
          Wrap = True
          OnClick = UDCompensarCadaClick
        end
        object EPaginaFinal: TZetaNumero
          Left = 388
          Top = 142
          Width = 33
          Height = 21
          Mascara = mnDias
          TabOrder = 15
          Text = '0'
          UseEnterKey = True
        end
        object EInicial: TEdit
          Left = 61
          Top = 216
          Width = 383
          Height = 21
          MaxLength = 79
          TabOrder = 16
        end
        object ESeparador: TEdit
          Left = 61
          Top = 240
          Width = 383
          Height = 21
          MaxLength = 39
          TabOrder = 17
        end
        object RE_ALTO: TZetaDBNumero
          Left = 320
          Top = 70
          Width = 33
          Height = 21
          Mascara = mnMinutos
          TabOrder = 8
          Text = '0'
          DataField = 'RE_ALTO'
          DataSource = DataSource
        end
      end
    end
    object TabSheet1: TTabSheet [6]
      Caption = 'Impresiones Subsecuentes'
      ImageIndex = 4
      object GroupBox5: TGroupBox
        Left = 22
        Top = 20
        Width = 497
        Height = 265
        TabOrder = 0
        object Label12: TLabel
          Left = 10
          Top = 25
          Width = 88
          Height = 13
          Alignment = taRightJustify
          Caption = ' IR Subsecuentes:'
        end
        object bBusccaReporte_IS: TSpeedButton
          Left = 438
          Top = 19
          Width = 25
          Height = 25
          Hint = 'Impresoras de Red'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bBusccaReporte_ISClick
        end
        object RE_FONTSIZ: TDBRadioGroup
          Left = 14
          Top = 144
          Width = 473
          Height = 105
          Caption = ' Organizaci'#243'n de las hojas '
          Columns = 2
          DataField = 'RE_FONTSIZ'
          DataSource = DataSource
          Items.Strings = (
            'Por Hoja'
            'Por Reporte')
          TabOrder = 2
          Values.Strings = (
            '0'
            '1')
        end
        object eListaReportesSubsecuentes: TDBEdit
          Left = 103
          Top = 21
          Width = 334
          Height = 21
          DataField = 'RE_FONTNAM'
          DataSource = DataSource
          MaxLength = 79
          TabOrder = 0
          OnExit = eListaReportesSubsecuentesExit
        end
        object GroupBox7: TGroupBox
          Left = 14
          Top = 53
          Width = 473
          Height = 81
          Caption = ' Impresora de Salida (Para todos los reportes) '
          TabOrder = 1
          object Label13: TLabel
            Left = 48
            Top = 24
            Width = 34
            Height = 13
            Alignment = taRightJustify
            Caption = 'Puerto:'
          end
          object Label14: TLabel
            Left = 33
            Top = 48
            Width = 49
            Height = 13
            Alignment = taRightJustify
            Caption = 'Impresora:'
          end
          object bPuerto_IS: TSpeedButton
            Left = 424
            Top = 18
            Width = 25
            Height = 25
            Hint = 'Impresoras de Red'
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000333300
              0000377777F3337777770FFFF099990FFFF07FFFF7FFFF7FFFF7000000999900
              00007777777777777777307703399330770337FF7F37F337FF7F300003399330
              000337777337F337777333333339933333333FFFFFF7F33FFFFF000000399300
              0000777777F7F37777770FFFF099990FFFF07FFFF7F7FF7FFFF7000000999900
              00007777777777777777307703399330770337FF7F37F337FF7F300003399330
              0003377773F7FFF77773333330000003333333333777777F3333333330FFFF03
              3333333337FFFF7F333333333000000333333333377777733333333333077033
              33333333337FF7F3333333333300003333333333337777333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = bPUERTOClick
          end
          object RE_REPORTE_IS: TDBComboBox
            Left = 89
            Top = 20
            Width = 334
            Height = 21
            DataField = 'RE_REPORTE'
            DataSource = DataSource
            ItemHeight = 13
            Items.Strings = (
              'LPT1'
              'LPT2'
              'LPT3'
              'COM1'
              'COM2'
              'COM3'
              'FILE')
            TabOrder = 0
          end
          object RE_PRINTER_IS: TDBLookupComboBox
            Left = 89
            Top = 44
            Width = 360
            Height = 21
            DataField = 'RE_PRINTER'
            DataSource = DataSource
            KeyField = 'PI_NOMBRE'
            ListField = 'PI_NOMBRE'
            ListSource = dsImpresoras
            TabOrder = 1
          end
        end
        object Panel2: TPanel
          Left = 96
          Top = 168
          Width = 113
          Height = 70
          BevelOuter = bvNone
          TabOrder = 3
          object Image1: TImage
            Left = 0
            Top = 0
            Width = 113
            Height = 70
            Align = alClient
            Picture.Data = {
              0A544A504547496D616765470A0000FFD8FFE000104A46494600010001006000
              600000FFFE001F4C45414420546563686E6F6C6F6769657320496E632E205631
              2E303100FFDB00840008050607060508070607090808090C140D0C0B0B0C1811
              120E141D191E1E1C191C1B20242E2720222B221B1C2836282B2F313334331F26
              383C38323C2E323331010809090C0A0C170D0D1731211C213131313131313131
              3131313131313131313131313131313131313131313131313131313131313131
              31313131313131313131FFC401A2000001050101010101010000000000000000
              0102030405060708090A0B010003010101010101010101000000000000010203
              0405060708090A0B100002010303020403050504040000017D01020300041105
              122131410613516107227114328191A1082342B1C11552D1F02433627282090A
              161718191A25262728292A3435363738393A434445464748494A535455565758
              595A636465666768696A737475767778797A838485868788898A929394959697
              98999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3
              D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FA1100020102
              0404030407050404000102770001020311040521310612415107617113223281
              08144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A
              35363738393A434445464748494A535455565758595A636465666768696A7374
              75767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9
              AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5
              E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFC000110800400071030111000211010311
              01FFDA000C03010002110311003F00F68AE62C2800A002800A002800A002800A
              002800A002800A002800A002802A6ABA8DB69364D777AD22C2AE89FBB89E562C
              EC11542A02C4966030077A695C0A1FF09458FF00CF96B9FF00823BDFFE354F91
              8AE1FF0009458FFCF96B9FF823BDFF00E3547230B97348D5AD35649DACFCF06D
              A5F26549EDA481D1F6AB60A48AADF75D4F4EF49A680BD48663DC78974F82F6E2
              D3CBD4A79AD5C24DF65D32E675462AAE14B47195CED7538CF7154A2D8AE37FE1
              28B1FF009F2D73FF000477BFFC6A8E461713FE12AD355E25961D56DC4B2A42AF
              3E9177126F760AA0BB461465980E48EB472B0B9B75232A6ABA8DB69364D777AD
              22C2AE89FBB89E562CEC11542A02C4966030077A695C0A1FF09458FF00CF96B9
              FF00823BDFFE354F918AE1FF0009458FFCF96B9FF823BDFF00E3547230B97348
              D5AD35649DACFCF06DA5F26549EDA481D1F6AB60A48AADF75D4F4EF49A680BD4
              8614018BE30FF9065A7FD8574EFF00D2D86AA1B899D8D6E48500723A37FC8C5E
              2BFF00B09C7FFA456B58CF7291AF50328782FF00E427E2AFFB0AA7FE915AD6F0
              D89674D54239CF885FF22EC5FF00613D3BFF004B60A4F602E5739662F8C3FE41
              969FF615D3BFF4B61AA86E267635B921401C8E8DFF0023178AFF00EC271FFE91
              5AD633DCA46BD40CCDF146A12E93E19D5752B65469ACACE69E359012A5910B00
              7041C647AD35B815B52F0C789B50823866F10690AB1DC43703668F283BA29564
              51FF001F3D3720CFB67A75AD541264DCD2FB1F8C3FE83BA1FF00E09A6FFE4AAB
              1152CFC4375A56B97BA778AB53D30A24368F6D2C16EF6DBE499E74F2F6B48FB8
              FEE4118C753C7142D5A8ADD85B46FA219A37FC8C5E2BFF00B09C7FFA456B58CF
              72911CB26B77FE26BBD374ABDD3ECA1B4B3B79D9AE6C9EE19DA57997036CA800
              0221D8F534E314D03649A5F87BC53A64FA84D06BFA3B35FDC0B8943E8F290AC2
              28E3C0C5CF4DB1AF5CF24FD2B44ACAC48DF11CFE30D13C3DA9EADFDAFA1CFF00
              60B496E7CAFEC89977EC42DB73F6938CE319C1A6033C65AD596A1A549676E675
              B8B5D4F4EF3239EDA584E0DF44032EF51BD72A7E65C8F7A5BC6EBFAF974FEBB0
              3D1D8B5E28D425D27C33AAEA56CA8D35959CD3C6B20254B221600E0838C8F5AC
              16E595B52F0C789B50823866F10690AB1DC43703668F283BA2956451FF001F3D
              3720CFB67A75AD541264DCD2FB1F8C3FE83BA1FF00E09A6FFE4AAB11461F125E
              E8DAC6A163E279E2BA8ADEDADAE239B4ED2EE06DF31A70C1D55A5C01E483B8E0
              73CD2BA5BE9F97DFB21D9F41BA1BABEBFE297460CADA9464107823EC56B59D44
              D4ACC22D35746CD665185F103FE442F117FD832E7FF453535B88ED2BA090A00F
              37F1BEA505978AF53B76D4347B39E7B1D31D06A97E2D15D63B9BA76DA76B1278
              51D38DD9F62B5528BECEE356B49774D7DEAC68784EFED753D4FC4D7961756F75
              6F2EA49B65B6944B1B11676C0E187070411F856752DCDA046F6D4BFA0FFC8FBA
              D7FD832C3FF46DDD54360675356239FF0088FF00F24F3C4BFF0060ABAFFD14D4
              01C4EA9E23B3D41665B8D63C3B757775AA69EB6C2C35713C8D1ADEC455045B06
              DC2E589058939EC0009694ECF7EBFD79745FADDB1FC5A6DFD7E7D4E8BE207FC8
              85E22FFB065CFF00E8A6AC16E51DA574121401E6DE37D46DADBC57AC58DCDEE9
              36BF6ED26C907F68EAAD600812DDE4A95525F04AE578041C13835324DE8B4F3D
              FF000D9FA3293E5D7FE07E3BAF55A97BC19776D7B79E229ECAFADF5084EA11A8
              B9B76568E42B656CA482BC750781D3A54D669CDB44D34E31499D25645985F103
              FE442F117FD832E7FF00453535B88ED2BA0933BC497B75A6E817D7D610C33DC5
              B42D2AC7348510E064E4804F4CF18E7A64751327CAAE5D38A949459830B997C7
              7A8C8D805F47B0638F796EE8ACB965633A72E68A6CD8AC0D0CDD07FE47DD6BFE
              C1961FFA36EEB686C4B3A9AB11C37C44BBD49346F1359B4D677162FA1DE4BE5C
              703A4B6D88885DEFBCAB6E3BF036AFDD3D70688EA9DFEFFD3EED7CBB6A825A5A
              DFD79FDFF9F91D157316617C40FF00910BC45FF60CB9FF00D14D4D6E23B4AE82
              4A5AD3DDC5A64F2D84D1C1346BB83C968F74303920468CACC71D0039CF63D2A6
              4EDA8D2BE8723A0DECBA8F89EE6F2E151669B45B069163E81BCDBBC8C64E39EC
              4923A76A5555ADFF000FF8F5F5EBB8A0EFFD5BF0E9E9D363A4AC4B0A00CDF146
              9F2EADE19D574DB66459AF6CE682369090A19D0A827009C64FA535A300FED9F1
              5FFD00745FFC1C4BFF00C8D5AF3A26C55D56E3C47AAE9F3585EE81A51B79D76C
              822D7AE22623D3725B8619EF83C8E2A5CA2F72A2DC5DD0CF0F69DA8C1AB5D5E6
              A16D6D6B13595B59C31457D2DDB6226998B33C88A4E7CD03B9E0F344E7CEEE4C
              62A2AC8DFACCA31258F5BD3FC4D77A969565A7DE4377676F032DCDEBDBB2346F
              33646D89C1044A3B8E86B48C924268B3FDB3E2BFFA00E8BFF83897FF0091AAB9
              D0AC73BAD68DAEEA169AD2C1A3D85ADCEAF6D2C12BFF00C24576F102F1F97BFC
              930EC240C761D3A8A23351565FD6B7FCC1ABBB9DA562519BE28D3E5D5BC33AAE
              9B6CC8B35ED9CD046D2121433A1504E0138C9F4A6B4601FDB3E2BFFA00E8BFF8
              3897FF0091AB5E744D8ADA95D789751B46B6B8D134E48D8824DBF886E607E3FD
              B8EDD587E749C90F622F0E6997D69AADD5D5DD8D958406CADAD2082D6EDEE302
              37998B333C68727CE1EA4E09279A99CB9AD614558E82A0A0A002800A002800A0
              02800A002800A002800A002800A002800A002803FFD9}
          end
        end
        object Panel3: TPanel
          Left = 344
          Top = 168
          Width = 115
          Height = 70
          BevelOuter = bvNone
          TabOrder = 4
          object Image2: TImage
            Left = 0
            Top = 0
            Width = 115
            Height = 70
            Align = alClient
            Picture.Data = {
              0A544A504547496D6167650B0A0000FFD8FFE000104A46494600010001006000
              600000FFFE001F4C45414420546563686E6F6C6F6769657320496E632E205631
              2E303100FFDB00840008050607060508070607090808090C140D0C0B0B0C1811
              120E141D191E1E1C191C1B20242E2720222B221B1C2836282B2F313334331F26
              383C38323C2E323331010809090C0A0C170D0D1731211C213131313131313131
              3131313131313131313131313131313131313131313131313131313131313131
              31313131313131313131FFC401A2000001050101010101010000000000000000
              0102030405060708090A0B010003010101010101010101000000000000010203
              0405060708090A0B100002010303020403050504040000017D01020300041105
              122131410613516107227114328191A1082342B1C11552D1F02433627282090A
              161718191A25262728292A3435363738393A434445464748494A535455565758
              595A636465666768696A737475767778797A838485868788898A929394959697
              98999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3
              D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FA1100020102
              0404030407050404000102770001020311040521310612415107617113223281
              08144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A
              35363738393A434445464748494A535455565758595A636465666768696A7374
              75767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9
              AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5
              E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFC000110800470066030111000211010311
              01FFDA000C03010002110311003F00F68AE62C2800A002800A002800A002800A
              002800A002800A002800A00C236D7DABF8B6FEC62D6EFB4DB6B4B1B59952D238
              0EE7924B80C4992373D225C631DEB484535A92CBDFF08A5F7FD0E1AE7FDFAB2F
              FE47ABE441732FC59A2EA9A3785757D52D7C5BACBCF636535C46B2436654B221
              600810038C8F5147220B9D1D60505001400500626A71DEDFF8AB4DD2EDB56BBD
              32096CAEAE246B548599D91EDD541F311C01895BA01DAAE093DC4CB9FF0008A5
              F7FD0E1AE7FDFAB2FF00E47AD3910AE1FF0008A5F7FD0E1AE7FDFAB2FF00E47A
              39105C83C1F7971A8F84B45BEBC93CCB9BAB182695F006E768D4B1C0E0724F4A
              C5EE335A90CA1AAEB7A4E8DE57F6BEA765A7F9D9F2FED570916FC6338DC46719
              1F98A690185A2F8CBC2F1F8D757B893C49A3A43269D6489235F44159964BA2C0
              1DD824065C8EDB87AD6B05644B3A3FF84EFC1FFF00435E87FF0083187FF8AAB1
              11F8FA786EBE1A7886E2D658E6826D1EE5E392360CAEA616208238208E734017
              6B98B0A002802A6A7AA69FA45BACFAADFDAD8C2CDB164B9996252D82700B1033
              8078F634580E6D7C63E181E3AD32E3FE124D1FC84D32F11A4FB745B559A5B52A
              A4EEC0242B103BED3E95AC1589674DFF0009DF83FF00E86BD0FF00F0630FFF00
              155A08D8D3EFECF53B38EF34DBB82F2D64CEC9A090488D8241C30E0E0823F0A0
              0E4FE1FF00FC885E1DFF00B065B7FE8A5AE77B946ED219969FF250F4AFFB055F
              7FE8DB4AD6992CEAEB41050078FBCF7D69F0BA181EF6E9ECAF3C1CECB14CB0F9
              41D6D17E58F6A8932002497257918E780A1BB8BED7F3DD7CACAF6EFB7984F4B3
              5DEDF83FC5DAEBA6F7E87A4D73961400500646B3FF002317853FEC2727FE915D
              55C37133AEAD890A00F3FF000FCF7D652D94B15EDD2D9DCEB5A8C12C256136E0
              9BCB8C7F0F9BBD8E3183B460E71DD47E3E57D6FF0093765F75DDFA5D2D6C13D2
              375D2DF9F5FBEDA75B74B973E1FF00FC885E1DFF00B065B7FE8A5AC1EE51675D
              D46F2CA5D36DB4DB482EAEB50BA36E8B3DC18517114929258239E91118C77A71
              5702A0B0F180F10DA6ADFD95A1FF00A35A4F6DE57F6B4DF3798F0B6ECFD9B8C7
              938C639DDDB1CEB18F289B35BED9E30FFA01687FF8399BFF00916A841E1BF15D
              9EAAA96D7B3D8D96AE65B888E9E9782473E54D24459410ACCA7CB273B47E942D
              6F6E80FDDB5FA9C0F85FC31A8DFF00802D12DA7F0FD9FF006B68915ACD711E8A
              DF69313C0AB86944E373018E48C6474ED59F3F2A715B0DABBBB3D1EB22828033
              BC43A8CDA5E9A2E2D6DE3B99DEE20B78E39253129696648812C158800BE7A1E9
              4D2BBB08A179A7F8C2E751D2AEFF00B2F434FECEBA6B8DBFDAD31F3330CB1633
              F66E3FD6E73CF4C77C8D631B0AE6BFDB3C61FF00402D0FFF0007337FF22D5888
              F47F164325EDD69DAFBE9BA4EA50DE0B48EDD6FC49F682628A4063DCA8CDC4C0
              602F5FAD0B5765B83F755DEC72DE1CD27516B9BBD52C0F87E29D353D41229EE3
              4769AE517ED936479A26524649E001C1C73D4C73F2369751B5CD6BF43A7F0FE9
              BFD8FA0E9DA5F9BE77D86D62B7F336EDDFB142E71938CE3A66B26515F59FF918
              BC29FF0061393FF48AEAAA1B899D756C4850079FF858DF5CDDC3049A45CC9A75
              B6AFA8CEB771BC3B3CD37972BF30670E0283FC2A492472002090B5DB7BADBEEF
              D76FE9589691B2EBBFDFFD32E7C3FF00F910BC3BFF0060CB6FFD14B5CEF728DD
              A430A00C5F187FC832D3FEC2BA77FE96C3550DC4CEC6B7242803CFD8DF4FE23D
              7EC61D22E6F6C24D66292E6581E1CA6CB4B32AA448EBC12324804E0600C9C823
              67357E966BD7F3D37F3F934C969176EB75F2FF00825FF07FFC832EFF00EC2BA8
              FF00E96CD584F7291B552332359FF918BC29FF0061393FF48AEAAE1B899D756C
              499DAAEAADA7DF69B6C2CE5996FE73099959424242161B813939DA71807DC8E3
              22D65CA36AD172ED6FC5D8E27C3DE17F0FEA36F7F77A8685A65DDCC9AAEA1BE6
              9ED23776C5E4C06588C9E001F856326D31A3ACB7822B6B78EDEDA24861894247
              1C6A155140C0000E0003B540C92800A00C5F187FC832D3FEC2BA77FE96C3550D
              C4CEC6B724CED7F556D1ECE3B85B396E83CF142C23655F2C3B05DED9238048E9
              93CF4EA40B5928F7FEBFAFD076F75BEC9BFBBFAFEB63908BC3BA26ADE29F14DC
              6ABA3E9F7D32EA31A2C9736C92305FB1DB1C02C09C649E3DCD6536D304745616
              569A75A25A69F6B0DA5B479D90C118445C9C9C28E07249FC6B3289E8031BC46B
              771DEE857D67A7CFA80B0BE69A586078D5F61B69E3C8F31954FCD22F7AB8349E
              A265AFF84AEFBFE84FD73FEFED97FF0024569CE8563335AD4EFF0054B8B09868
              1E28B3FB0CDE7AADBC9A6E246C630DBE5638C161C11D4FB60524A4A40FE171EF
              FF000FF996BC2505CC1A449F6DB592D269AF6F2E3C9919199164B9924504A12B
              9DAE3A135949DD8D1B15230A0028031FC5B05CCFA447F62B592EE682F6D2E3C9
              8D9159D63B98E4600B90B9DAA7A915517662659FF84AEFBFE84FD73FEFED97FF
              002456BCE856337C43AA5F6B7A78B3FF00847BC4F6004A92192DA4D3B712A770
              1F3CAC31900F4EDE99A5CCAE9DF61AD1356DD589BC32978D77ADDEDED8DC58FD
              BAF9658A3B9689A42AB6D047B8F96CCA32D1B719FCAA26D377428AB236EA0A0A
              002800A002800A002800A002800A002800A002800A002803FFD9}
          end
        end
      end
    end
    inherited tsParametro: TTabSheet
      inherited BArribaParametro: TZetaSmartListsButton
        Left = 214
        Top = 36
      end
      inherited BAbajoParametro: TZetaSmartListsButton
        Left = 214
        Top = 61
      end
      inherited GroupBox13: TGroupBox
        Left = 8
        Top = 32
      end
      inherited BAgregaParametro: TBitBtn
        Left = 8
        Top = 247
      end
      inherited BBorraParametro: TBitBtn
        Left = 123
        Top = 247
      end
      inherited GBPropiedadesParametro: TGroupBox
        Left = 247
        Top = 32
        Width = 277
        inherited LTituloParametro: TLabel
          Left = 8
          Top = 21
        end
        inherited lFormulaParametro: TLabel
          Left = 12
          Top = 45
        end
        inherited ETituloParametro: TEdit
          Left = 44
          Top = 17
        end
        inherited EFormulaParametro: TMemo
          Left = 44
          Top = 45
        end
        inherited bFormulaParametro: TBitBtn
          Left = 244
          Top = 101
        end
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 364
    Width = 651
    TabOrder = 3
    inherited OK: TBitBtn
      Left = 496
    end
    inherited Cancelar: TBitBtn
      Left = 574
    end
  end
  inherited StatusBarRep: TStatusBar
    Top = 398
    Width = 651
  end
  inherited SmartListCampos: TZetaSmartLists
    Left = 384
  end
  object SmartListEncabezado: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LBEncabezado
    ListaEscogidos = LBEncabezado
    AlBajar = Intercambia
    AlSeleccionar = SmartListCamposAlSeleccionar
    AlSubir = Intercambia
    Left = 411
    Top = 2
  end
  object SmartListPie: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LBPie
    ListaEscogidos = LBPie
    AlBajar = Intercambia
    AlSeleccionar = SmartListCamposAlSeleccionar
    AlSubir = Intercambia
    Left = 440
  end
  object dsImpresoras: TDataSource
    DataSet = dmSistema.cdsImpresoras
    Left = 512
    Top = 8
  end
end
