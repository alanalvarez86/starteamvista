unit FPolizaBase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FBaseReportes, Grids,
  DBGrids, ZetaDBGrid, ZetaSmartLists, ZetaKeyLookup, ZetaEdit, ZetaFecha,
  CheckLst, Db, ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls,
  ZetaMessages,
  ZetaTipoEntidad,
  ZetaCommonLists;

type eFiltroPoliza = ( fpContenga,fpNOContenga,
                       fpEmpiezeCon,fpNOEmpiezeCon,
                       fpTermineEn,fpNoTermineEn,
                       fpIgual,fpDiferente,
                       fpMayor,fpMayorIgual,
                       fpMenor,fpMenorIgual,
                       fpVacio,fpNoVacio );

type
  TPolizaBase = class(TBaseReportes)
    tsCuentas: TTabSheet;
    dsCuentas: TDataSource;
    Panel2: TPanel;
    Grid: TZetaDBGrid;
    ModificarBtn: TSpeedButton;
    AgregarBtn: TSpeedButton;
    BorrarBtn: TSpeedButton;
    Label6: TLabel;
    CBTipoCuenta: TComboBox;
    Label5: TLabel;
    RE_ENTIDAD: TZetaKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AgregarBtnClick(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure ModificarBtnClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BImprimirClick(Sender: TObject);
    procedure dsCuentasStateChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure GridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GridColExit(Sender: TObject);
    procedure CBTipoCuentaChange(Sender: TObject);
    procedure RE_ENTIDADChange(Sender: TObject);
  private
    FIndex : string;
    FEntidadActual: integer;
    procedure SeleccionaPrimerColumna;
    procedure ShowEditPoliza;
    procedure WMExaminar(var Message: TMessage);message WM_EXAMINAR;
    procedure Agregar;
    procedure Modificar;
    procedure SeleccionaSiguienteRenglon;
    procedure ImprimirGrid;
    procedure CambiaTipo(const iValor: integer);
    procedure CambiaTablaPrincipal;
    function GetEntidadPoliza:TipoEntidad;
  protected
    procedure Borrar;virtual;
    procedure Connect;override;
    procedure EscribirCambios;override;
    procedure KeyPress(var Key: Char);override;
    procedure KeyDown(var Key: Word; Shift: TShiftState);override;

  public
    { Public declarations }
  end;

var
  PolizaBase: TPolizaBase;

const
     Q_FILTRO = 'CR_TIPO =0';

implementation

uses FAutoClasses,
     FEditPoliza,
{$ifndef TRESSCFG}
     FTressShell,
{$endif}
     DCatalogos,
     DSistema,
     DReportes,
     DDiccionario,
     DCliente,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZReportTools,
     ZReportModulo;

{$R *.DFM}

{ TPoliza }


procedure TPolizaBase.Connect;
begin
     dmCatalogos.cdsCondiciones.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmReportes do
     begin
          cdsEditReporte.Conectar;
          DataSource.DataSet := cdsEditReporte;
          dsCuentas.DataSet := cdsCampoRep;
          fConectando := cdsEditReporte.State <> dsInsert;

     end;
     CampoRep.Filter := 'CR_TIPO <>0';
     CampoRep.Filtered := TRUE;

     LlenaListas;

     CampoRep.First;
     while not CampoRep.EOF do {El emptyDataSet, borrar todo, aunque este filtrado}
           CampoRep.Delete;

     CampoRep.Filter := Q_FILTRO;

     FIndex := CampoRep.IndexFieldNames;
     CampoRep.IndexFieldNames := 'CR_TITULO';


     LlenaTipoPoliza(RE_ENTIDAD.Lista);
     RE_CLASIFI.ItemIndex := dmDiccionario.GetPosClasifi( RE_CLASIFI.Items,eClasifiReporte(Reporte.FieldByName('RE_CLASIFI').AsInteger) );

     RE_ENTIDAD.ItemIndex := ZReportModulo.GetItemPoliza(TipoEntidad(Reporte.FieldByName('RE_ENTIDAD').AsInteger));
     {ifdef TRESS}
     {case TipoEntidad(Reporte.FieldByName('RE_ENTIDAD').AsInteger) of
          enAusencia: RE_ENTIDAD.ItemIndex := 1;
          enWorks: RE_ENTIDAD.ItemIndex := 2
          else RE_ENTIDAD.ItemIndex := 0;
     end;}
     {endif}
     
     FEntidadActual := RE_ENTIDAD.ItemIndex;

     
     Modo := dmReportes.cdsEditReporte.State;
     fConectando := FALSE;

     SetLookupCondicion;

end;

procedure TPolizaBase.FormShow(Sender: TObject);
 var lEsDemo : Boolean;
begin
     inherited;
     PageControl.ActivePage := tsCuentas;
     CbTipoCuenta.Visible := FALSE;
     SeleccionaPrimerColumna;

     lEsDemo := Autorizacion.EsDemo;

     AgregarBtn.Enabled := Not lEsDemo;
     BorrarBtn.Enabled := Not lEsDemo;
     ModificarBtn.Enabled := Not lEsDemo;
     Grid.ReadOnly := lEsDemo;

     if lEsDemo then
     begin
          ZInformation('','En Versi�n Demo, las Cuentas Contables no Pueden Ser Editadas.',0);
     end;
end;


procedure TPolizaBase.EscribirCambios;
begin
     if dmCliente.EsModoDemo then Exit;

     if ( Modo in [ dsEdit, dsInsert ] ) AND ( Reporte.State <> Modo ) then
        Reporte.Edit;

     with RE_CLASIFI do
          Reporte.FieldByName('RE_CLASIFI').AsInteger := Integer(Items.Objects[ItemIndex]);

     Reporte.FieldByName('RE_ENTIDAD').AsInteger := Ord(GetEntidadPoliza);

     CampoRep.Filtered := FALSE;
     CampoRep.Filter := 'CR_TIPO<>0';
     CampoRep.Filtered := TRUE;

     CampoRep.First;
     while not CampoRep.EOF do {El emptyDataSet, borrar todo, aunque este filtrado}
           CampoRep.Delete;

     CampoRep.Filter := '';
     CampoRep.Filtered := FALSE;

     GrabaFiltros;
     GrabaOrden;

     ClientDataset.Enviar;

     CampoRep.Filtered := FALSE;
     CampoRep.Filter := Q_FILTRO;
     CampoRep.Filtered := TRUE;

end;





procedure TPolizaBase.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H50519_Generador_Cuentas_Contables;
     Grid.Options := [dgEditing,dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgCancelOnExit];
end;


procedure TPolizaBase.AgregarBtnClick(Sender: TObject);
begin
     inherited;
     Agregar;
end;

procedure TPolizaBase.BorrarBtnClick(Sender: TObject);
begin
     inherited;
     Borrar;
end;

procedure TPolizaBase.ModificarBtnClick(Sender: TObject);
begin
     inherited;
     Modificar;
end;

procedure TPolizaBase.SeleccionaPrimerColumna;
begin
     Self.ActiveControl:= Grid;
     Grid.SelectedField:= Grid.Columns[ 0 ].Field;   // Posicionar en la primer columna
end;

procedure TPolizaBase.SpeedButton1Click(Sender: TObject);
begin
     inherited;
     ShowEditPoliza;
end;

procedure TPolizaBase.ShowEditPoliza;
begin
     if EditPoliza = NIL then
        EditPoliza := TEditPoliza.Create( Self );
     EditPoliza.ShowModal;
     //ProcesaCuentas;
end;

procedure TPolizaBase.SpeedButton3Click(Sender: TObject);
begin
     inherited;
     ShowEditPoliza;
end;

procedure TPolizaBase.WMExaminar(var Message: TMessage);
begin
     if ( ActiveControl = Grid ) then
     begin
          with Grid do
          begin
               if SelectedIndex = 1 then
               begin
                    SeleccionaSiguienteRenglon;
                    SelectedIndex := 0;
               end
               else SelectedIndex := SelectedIndex + 1
          end;
     end
end;

procedure TPolizaBase.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( ActiveControl = Grid ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         Agregar;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         Borrar;
                    end;
               end;
          end
          else
          begin
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             Modificar;
                        end;
                        80: { Letra P = Imprimir }
                        begin
                             Key := 0;
                             ImprimirGrid;
                        end;
                   end;
              end;
          end;
          if ( Key = VK_RETURN ) then
          begin
               Key:= 0;
          end;
          if ( Key = VK_DOWN ) then
          begin
               Key:= 0;
               SeleccionaSiguienteRenglon;
          end;
     end;
end;

procedure TPolizaBase.Agregar;
begin
     CampoRep.Append;
end;

procedure TPolizaBase.Borrar;
begin
     if CampoRep.IsEmpty then
        ZetaDialogo.ZInformation( Caption, 'No hay Cuentas para Borrar', 0 )
     else if ZetaDialogo.ZConfirm( Caption, '�Desea Borrar la Cuenta  '+ Camporep.FieldByname('CR_TITULO').AsString+'?',0, mbYes ) then
     begin

          CampoRep.Delete;
          Modo := dsEdit;
     end;
end;

procedure TPolizaBase.Modificar;
begin
     CampoRep.Edit;
end;

procedure TPolizaBase.SeleccionaSiguienteRenglon;
begin
     with CampoRep do
     begin
          Next;
          if EOF then
             Self.Agregar;
     end;
end;

procedure TPolizaBase.CambiaTipo( const iValor : integer );
begin
     with dmReportes.cdsCampoRep do
     begin
          if( iValor <> FieldByName( 'CR_OPER' ).AsInteger ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CR_OPER' ).AsInteger := iValor;
          end;
     end;
     Grid.SetFocus;
     Grid.SelectedField := dmReportes.cdsCampoRep.FieldByName('CR_OPER');
end;

procedure TPolizaBase.KeyPress(var Key: Char);
 var iSel : integer;
begin
     if ActiveControl = Grid then
     begin
          if ( ( Key <> chr(9) ) and ( key <> #0 ) ) then
          begin
               if ( Grid.SelectedField.FieldName = 'CR_OPER' ) then
               begin
                    case Key of
                        'C', 'c' : iSel := 0;
                        'A', 'a' : iSel := 1
                        else iSel:= -1;
                    end;
                    if iSel > -1 then
                    begin
                         CambiaTipo( iSel );
                         CBTipoCuenta.ItemIndex := iSel;
                    end;
                    Key:= #0;
               end;
          end;
          if Key = CHR(13) then
             with Grid do
             begin
                  if SelectedIndex = 1 then
                  begin
                       SeleccionaSiguienteRenglon;
                       SelectedIndex := 0;
                  end
                  else SelectedIndex := SelectedIndex + 1;
                  Key := #0;
             end;
     end;
end;


procedure TPolizaBase.BImprimirClick(Sender: TObject);
begin
     ImprimirGrid;
{$ifndef TRESSCFG}
     TressShell.ReconectaMenu;
{$endif}
end;

procedure TPolizaBase.ImprimirGrid;
begin
     if PageControl.ActivePage = tsCuentas then
     begin
          if zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De Las Cuentas Contables ?', 0, mbYes ) then
             FBaseReportes.ImprimirGrid( Grid, dsCuentas.DataSet, Caption, 'IM',
                                         '','',stNinguno,stNinguno );

     end
     else if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
          Print;
end;

procedure TPolizaBase.dsCuentasStateChange(Sender: TObject);
begin
     inherited;
     with dsCuentas do
     begin
          if Dataset = nil then
             Modo := dsInactive
          else
          begin
               if Dataset.State in [ dsInsert, dsEdit ] then
               begin
                    Modo := Dataset.State;
               end;
          end;
     end;
end;

procedure TPolizaBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     with CampoRep do
     begin
          IndexFieldNames := FIndex;
          Filtered := FALSE;
     end;

end;

procedure TPolizaBase.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Modo := dsInactive
          else
              Modo := Dataset.State;
     end;

end;

procedure TPolizaBase.GridDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CR_OPER' ) then
          begin
               with CBTipoCuenta do
               begin
                    ItemIndex := CampoRep.FieldByName('CR_OPER').AsInteger;
                    Left := Rect.Left + Grid.Left;
                    Top := Rect.Top + Grid.Top;
                    Width := Column.Width + 2;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TPolizaBase.GridColExit(Sender: TObject);
begin
     inherited;
     with Grid do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'CR_OPER' ) and
             ( CBTipoCuenta.Visible ) then
          begin
               CBTipoCuenta.Visible:= False;
          end;
     end;
end;

procedure TPolizaBase.CBTipoCuentaChange(Sender: TObject);
begin
     inherited;
     CambiaTipo(CBTipoCuenta.ItemIndex);
end;



procedure TPolizaBase.RE_ENTIDADChange(Sender: TObject);

begin
     inherited;
     CambiaTablaPrincipal;
end;

procedure TPolizaBase.CambiaTablaPrincipal;

 procedure CambiaTabla;
 begin
      Modo := dsEdit;
      FEntidadActual := RE_ENTIDAD.ItemIndex;
      if ( Modo in [ dsEdit, dsInsert ] ) AND ( Reporte.State <> Modo ) then
        Reporte.Edit;
      Reporte.FieldByName('RE_ENTIDAD').AsInteger := Ord(GetEntidadPoliza);
 end;

begin


     if ZetaDialogo.ZWarningConfirm( Caption,
                                     'Los Grupos y Filtros se basan en la Nueva Tabla Principal,'+CR_LF+
                                     'los Existentes Seran Borrados.'+CR_LF+
                                     '�Desea Continuar?',0, mbNO ) then
     begin
          CambiaTabla;

          LBOrden.Clear;
          LBFiltros.Clear;

          EnabledOrden( NOT LbOrden.Items.Count = 0, FALSE );
          SmartListOrden.SelectEscogido( 0 );
          EnabledFiltro( NOT LbFiltros.Items.Count = 0 );
          SmartListFiltros.SelectEscogido( 0 );
    end
     else
         RE_ENTIDAD.ItemIndex := FEntidadActual;

end;

function TPolizaBase.GetEntidadPoliza:TipoEntidad;
begin
     Result := ZReportTools.GetEntidadPoliza(RE_ENTIDAD.ItemIndex);
end;


end.




