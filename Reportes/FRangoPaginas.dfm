object RangoPaginas: TRangoPaginas
  Left = 280
  Top = 288
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Imprimir'
  ClientHeight = 88
  ClientWidth = 196
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 58
    Width = 196
    Height = 30
    Align = alBottom
    TabOrder = 0
    object OK: TBitBtn
      Left = 21
      Top = 3
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&OK'
      TabOrder = 0
      Kind = bkOK
    end
    object Cancelar: TBitBtn
      Left = 101
      Top = 3
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = '&Cancelar'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 196
    Height = 58
    Align = alClient
    TabOrder = 1
    object EPAGINAS: TLabel
      Left = 3
      Top = 10
      Width = 91
      Height = 13
      Alignment = taRightJustify
      Caption = 'Rango de P�ginas:'
    end
    object EPAginasAl: TLabel
      Left = 138
      Top = 10
      Width = 12
      Height = 13
      Alignment = taRightJustify
      Caption = 'Al:'
    end
    object lbCopias: TLabel
      Left = 49
      Top = 34
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = '# Copias:'
    end
    object EPaginaInicial: TEdit
      Left = 99
      Top = 6
      Width = 33
      Height = 21
      TabOrder = 0
      Text = '1'
    end
    object EPaginaFinal: TEdit
      Left = 154
      Top = 6
      Width = 39
      Height = 21
      TabOrder = 1
      Text = '9999'
    end
    object ECopias: TEdit
      Left = 99
      Top = 30
      Width = 33
      Height = 21
      TabOrder = 2
      Text = '1'
    end
    object UDCopias: TUpDown
      Left = 132
      Top = 30
      Width = 12
      Height = 21
      Associate = ECopias
      Min = 1
      Position = 1
      TabOrder = 3
      Thousands = False
      Wrap = True
    end
  end
end
