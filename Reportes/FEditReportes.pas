unit FEditReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FEditBaseReportes, StdCtrls, Buttons, ExtCtrls, ZetaSmartLists,
  ZetaKeyLookup, ZetaEdit, ZetaFecha, CheckLst, Db, ZetaKeyCombo, DBCtrls,
  Mask, ZetaDBTextBox, ComCtrls, ZetaNumero, Grids, DBGrids, ZetaDBGrid,
  ZReportTools,
  ZReportToolsConsts,
  ZetaTipoEntidad,
  ZetaCommonLists,
  //(@am): CAMBIO TEMPORAL
  //TDMULTIP,  //(@am): Se necesita cambiar comp. para imagenes.
  ZetaCommonClasses;

type
  TEditReportes = class(TEditBaseReportes)
    tsCampos: TTabSheet;
    GroupBox4: TGroupBox;
    LBCampos: TZetaSmartListBox;
    BAgregaCampo: TBitBtn;
    BBorraCampo: TBitBtn;
    BFormula: TBitBtn;
    GBPropiedadesCampo: TGroupBox;
    LTituloCampo: TLabel;
    lFormulaCampo: TLabel;
    lMascaraCampo: TLabel;
    lAnchoCampo: TLabel;
    lTipoCampo: TLabel;
    CBMascaraCampo: TComboBox;
    ETituloCampo: TEdit;
    bFormulaCampo: TBitBtn;
    EFormulaCampo: TMemo;
    CBTipoCampo: TZetaKeyCombo;
    SmartListCampos: TZetaSmartLists;
    tsGrupos: TTabSheet;
    SmartListGrupos: TZetaSmartLists;
    GroupBox6: TGroupBox;
    LbGrupos: TZetaSmartListBox;
    BAgregaGrupo: TBitBtn;
    BBorraGrupo: TBitBtn;
    BAbajoGrupo: TZetaSmartListsButton;
    BArribaGrupo: TZetaSmartListsButton;
    GroupBoxGrupo: TGroupBox;
    eFormulaGrupo: TMemo;
    bFormulaGrupo: TBitBtn;
    EAnchoCampo: TEdit;
    ETituloGrupo: TEdit;
    lTituloGrupo: TLabel;
    lFormulaGrupo: TLabel;
    BAgregaFormulaGrupo: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure SmartListGruposAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure GrupoOpcionesClick(Sender: TObject);
    procedure SmartListCamposAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure CampoOpcionesClick(Sender: TObject);
    procedure BBorraGrupoClick(Sender: TObject);
    procedure BBorraCampoClick(Sender: TObject);
    procedure BFormulaClick(Sender: TObject);
    procedure SmartListGruposAlBajar(Sender: TObject; var Objeto: TObject;
      Texto: String);
    procedure SmartListGruposAlSubir(Sender: TObject; var Objeto: TObject;
      Texto: String);
    procedure FormDestroy(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure bFormulaGrupoClick(Sender: TObject);
    procedure BAgregaFormulaGrupoClick(Sender: TObject);
  private
    oTempGrupos : TStrings;
    procedure ValidaGrupoEmpresa;
    procedure AsignaGrupos(oDestino, oFuente: TStrings);
  protected
    procedure AgregaListaGrupo;override;
    procedure AgregaListaEncabezado;override;
    procedure AgregaListaPieGrupo;override;
    procedure LeeCampoGeneral( oCampo: TCampoOpciones );

    procedure LimpiaListas;override;
    procedure PosicionaListas;override;
    procedure IniciaListas;override;

    procedure GrabaListas;override;
    procedure GrabaGrupos;virtual;
    procedure GrabaCampos;virtual;
    procedure GrabaCampoOpciones( oCampo: TCampoOpciones;
                                  const eTipo: eTipoCampo;
                                  const iPosicion, iSubPos: integer);

    procedure AgregaCampoDefault;
    procedure AgregaFormula;virtual;

    //LISTA DE GRUPOS
    procedure AgregaGrupo;virtual;
    procedure ActualizaGrupos(Objeto: TGrupoOpciones);virtual;
    procedure ActualizaGruposObjeto;virtual;
    procedure AsignaProcGrupos(oProc: TNotifyEvent);virtual;
    procedure EnabledGrupos(const bEnable, bFormula: Boolean);override;
    procedure GrupoNivelEmpresa(i: integer);virtual;
    procedure AgregaFormulaGrupo;

    //Lista de CAMPOS
    procedure ModificaListaCampos(Lista : TStrings);
    procedure ActualizaCampos(const i : integer);virtual;
    procedure ActualizaCamposObjeto;virtual;
    procedure AsignaProcCampos(oProc: TNotifyEvent);virtual;
    procedure EnabledCampos(const bEnable, bFormula, bAuto: Boolean);virtual;
    function GetListaCampos : TZetaSmartListBox;virtual;


    function GetFirstPestana: TTabSheet;override;

    procedure AgregaCampo;virtual;abstract;
    procedure AlAgregarCampo( const oDiccion: TDiccionRecord;
                              var oCampo: TCampoOpciones);dynamic;

    //CAMBIO DE TABLA PRINCIPAL
    procedure AlBorrarDatos(const oEntidad : TipoEntidad);override;
    procedure AlRevisarDatos(const oEntidad : TipoEntidad);override;
    procedure DespuesDeBorrarDatos;override;

    procedure ParametrosMovimienLista(FParamList : TZetaParams);override;
    procedure ParametrosListadoNomina(FParamList : TZetaParams);override;
    function GetGruposStrings: TStrings;override;
    procedure GruposMovimienLista;
    function GetSoloTotales : Boolean;virtual;
    procedure DespuesDeConstruyeSQL;override;
    procedure DespuesDeGeneraReporte;override;


  public
    { Public declarations }
  end;

const K_PROPIEDAD_CAMPO = 'Propiedades del Campo';

var
  EditReportes: TEditReportes;

implementation
uses ZetaCommonTools,
     ZReportConst,
     ZFuncsCliente,
     ZDiccionTools,
     ZetaDialogo;

{$R *.DFM}

{ TEditReportes }


procedure TEditReportes.AgregaListaGrupo;
 var oGrupo : TGrupoOpciones;
begin
     oGrupo := TGrupoOpciones.Create;
     with oGrupo do
     begin
          Encabezado := CampoRep['CR_TFIELD'] = 1;
          PieGrupo := CampoRep['CR_SHOW'] = 1;
          SaltoPagina := CampoRep['CR_CALC'] = 1;
          Totalizar := CampoRep['CR_OPER'] = 1;
          Master := CampoRep['CR_ANCHO'] = 1;
          Calculado := CampoRep['CR_ALINEA'];
          {$ifdef ADUANAS}
          Titulos := Camporep['CR_COLOR']=1;
          {$ENDIF}
     end;
     GetDatosGenerales( oGrupo );
     LBGrupos.Items.AddObject(oGrupo.Titulo,oGrupo);     
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_REPORTES_ASIST;
     {$else}
     HelpContext := H50511_Generador_reportes;
     tsGenerales.HelpContext := H50512_Generador_reportes_Generales;
     tsCampos.HelpContext := H50513_Generador_reportes_Lista_campos;
     tsOrden.HelpContext := H50514_Generador_reportes_Orden;
     tsGrupos.HelpContext := H50515_Generador_reportes_Grupos;
     tsFiltros.HelpContext := H50516_Generador_reportes_Filtros;
     tsImpresora.HelpContext := H50517_Generador_reportes_Impresora;
     tsParametro.HelpContext := H50518_Generador_reportes_Parametros;
     {$endif}
end;


procedure TEditReportes.LeeCampoGeneral( oCampo : TCampoOpciones );
begin
     with oCampo do
     begin
          Requiere := CampoRep.FieldByName('CR_REQUIER').AsString;
          Calculado := CampoRep.FieldByName('CR_CALC').AsInteger;
          Mascara := CampoRep.FieldByName('CR_MASCARA').AsString;
          Ancho := CampoRep.FieldByName('CR_ANCHO').AsInteger;
          TipoCampo := eTipoGlobal( CampoRep.FieldByName('CR_TFIELD').AsInteger );
     end;
     GetDatosGenerales( oCampo );
end;

procedure TEditReportes.AgregaListaEncabezado;
 var oCampo : TCampoOpciones;
begin
     oCampo := TCampoOpciones.Create;
     LeeCampoGeneral( oCampo );
     with LBGrupos.Items do
          with TGrupoOpciones(Objects[CampoRep['CR_POSICIO']]).ListaEncabezado do
               AddObject( oCampo.Titulo, oCampo );
end;

procedure TEditReportes.AgregaListaPieGrupo;
 var oCampo : TCampoOpciones;
begin
     oCampo := TCampoOpciones.Create;
     LeeCampoGeneral( oCampo );
     with LBGrupos.Items do
          with TGrupoOpciones(Objects[CampoRep['CR_POSICIO']]).ListaPie do
               AddObject( oCampo.Titulo, oCampo );
end;

procedure TEditReportes.FormShow(Sender: TObject);
begin
     inherited;
     ValidaGrupoEmpresa;
end;

procedure TEditReportes.PosicionaListas;
begin
     inherited;
     SmartListCampos.SelectEscogido( 0 );
     SmartListGrupos.SelectEscogido( 0 );
end;

procedure TEditReportes.LimpiaListas;
  var i : integer;
begin
     Inherited;
     LBCampos.Items.Clear;
     EnabledCampos(FALSE, FALSE,FALSE);

     for i:=0 to LBGrupos.Items.Count -1 do
     begin
          TGrupoOpciones(LBGrupos.Items.Objects[i]).ListaEncabezado.Clear;
          TGrupoOpciones(LBGrupos.Items.Objects[i]).ListaPie.Clear;
     end;
     LBGrupos.Items.Clear;
     EnabledGrupos(FALSE, FALSE);
end;

procedure TEditReportes.GrabaListas;
begin
     inherited;
     GrabaGrupos;
     GrabaCampos;
end;

procedure TEditReportes.GrabaCampoOpciones( oCampo:TCampoOpciones;
                                            const eTipo : eTipoCampo;
                                            const iPosicion, iSubPos : integer );
begin
     GrabaCampoGeneral( oCampo, eTipo, iPosicion, iSubPos );
     with oCampo do
     begin
          CampoRep['CR_REQUIER'] := Requiere;
          CampoRep['CR_CALC'] := Calculado;
          CampoRep['CR_MASCARA'] := Mascara;
          CampoRep['CR_ANCHO'] := Ancho;

     end;
end;

procedure TEditReportes.GrabaGrupos;
 var i,j: integer;
begin
     with LBGrupos.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;
               GrabaCampoGeneral( TCampoMaster(Objects[i]),
                                  tcGrupos, i, -1 );
               with TGrupoOpciones(Objects[i]) do
               begin
                    CampoRep['CR_OPER'] := BoolToInt( Totalizar );
                    CampoRep['CR_CALC'] := BoolToInt( SaltoPagina );
                    CampoRep['CR_TFIELD'] := BoolToInt( Encabezado );
                    CampoRep['CR_SHOW'] := BoolToInt( PieGrupo );
                    CampoRep['CR_ANCHO'] := BoolToInt( Master );
                    CampoRep['CR_ALINEA'] := Calculado;
                    {$ifdef ADUANAS}
                    CampoRep['CR_COLOR'] := BoolToInt( Titulos );
                    {$endif}
               end;
               CampoRep.Post;

               for j := 0 to TGrupoOpciones(Objects[i]).ListaEncabezado.Count - 1 do
               begin
                    CampoRep.Append;
                    GrabaCampoOpciones( TCampoOpciones( TGrupoOpciones(Objects[i]).ListaEncabezado.Objects[ j ] ),
                                        tcEncabezado, i,j );
                    CampoRep.Post;

               end;

               for j := 0 to TGrupoOpciones(Objects[i]).ListaPie.Count - 1 do
               begin
                    CampoRep.Append;
                    GrabaCampoOpciones( TCampoOpciones( TGrupoOpciones(Objects[i]).ListaPie.Objects[ j ] ),
                                        tcPieGrupo, i,j );
                    CampoRep.Post;
               end;
          end;
end;

procedure TEditReportes.GrabaCampos;
begin
     //Los hijos son los que van a grabar;
end;

//Lista de Grupos


procedure TEditReportes.SmartListGruposAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaGrupos( TGrupoOpciones( Objeto ) );
          GrupoNivelEmpresa( LBGrupos.ItemIndex );
     end
     else EnabledGrupos( FALSE, FALSE );
end;

procedure TEditReportes.ActualizaGrupos( Objeto : TGrupoOpciones );
begin
     AsignaProcGrupos( NIL );

     eTituloGrupo.Text := Objeto.Titulo;
     eFormulaGrupo.Text := Objeto.Formula;

     AsignaProcGrupos( GrupoOpcionesClick );
     EnabledGrupos( TRUE, Objeto.Calculado<>0 );
end;

procedure TEditReportes.GrupoNivelEmpresa( i : integer );
begin
     case i of
          0:
          begin
               BArribaGrupo.SmartLists := NIL;
               BAbajoGrupo.SmartLists := NIL;
          end;
          1:
          begin
               BArribaGrupo.SmartLists := NIL;
               BAbajoGrupo.SmartLists := SmartListGrupos;
          end;
          else
          begin
               BArribaGrupo.SmartLists := SmartListGrupos;
               BAbajoGrupo.SmartLists := SmartListGrupos;
          end;
     end;
     BArribaGrupo.Enabled := NOT ( i <= 1 );
     BAbajoGrupo.Enabled := NOT ( i = 0 );
     BBorraGrupo.Enabled := NOT ( i = 0 );
end;

procedure TEditReportes.EnabledGrupos( const bEnable, bFormula : Boolean );
 var lFormula : Boolean;
begin
     BAgregaFormulaGrupo.Enabled := NOT SoloImpresion;
     lFormula := NOT SoloImpresion AND bFormula;
     lTituloGrupo.Enabled := lFormula;
     eTituloGrupo.Enabled := lFormula;
     lFormulaGrupo.Enabled := lFormula;
     eFormulaGrupo.Enabled := lFormula;
end;

procedure TEditReportes.AsignaProcGrupos( oProc : TNotifyEvent );
begin
     eFormulaGrupo.OnExit := oProc;
end;

procedure TEditReportes.GrupoOpcionesClick(Sender: TObject);
begin
     inherited;
     ActualizaGruposObjeto;
end;

procedure TEditReportes.ActualizaGruposObjeto;
 var i :integer;
begin
     with LBGrupos,Items do
     begin
          i := ItemIndex;
          with TGrupoOpciones( Objects[ i ] ) do
          begin
               Titulo := eTituloGrupo.Text;
               Items[ItemIndex] := Titulo;
               Formula := eFormulaGrupo.Text;
          end;
          Modo := dsEdit;
     end;
end;

procedure TEditReportes.SmartListCamposAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaCampos( GetListaCampos.ItemIndex );
          SmartListCampos.Control := ETituloCampo;
     end
     else EnabledCampos( FALSE, FALSE, FALSE );

end;

procedure TEditReportes.ActualizaCampos( const i : integer );
 var Objeto : TCampoOpciones;
begin
     AsignaProcCampos( NIL );
     Objeto := GetListaCampos.Items.Objects[i] as TCampoOpciones;

     EFormulaCampo.Text := Objeto.Formula;
     ETituloCampo.Text := Objeto.Titulo;
     GBPropiedadesCampo.Caption := K_PROPIEDAD_CAMPO + ' #' + IntToStr( GetListaCampos.ItemIndex  + 1 );
     GetListaMascaras( CBMascaraCampo.Items, Objeto.TipoCampo );
     CBMascaraCampo.Text := Objeto.Mascara;
     EAnchoCampo.Text := IntToStr( Objeto.Ancho );
     CBTipoCampo.ItemIndex := Ord( Objeto.TipoCampo );
     with GetListaCampos do
          Items[ ItemIndex ] := Objeto.Titulo;

     EnabledCampos( Objeto.Calculado <> K_RENGLON_NUEVO,
                    Objeto.Calculado=-1,
                    Objeto.TipoCampo = tgAutomatico );
     AsignaProcCampos( CampoOpcionesClick );
end;

function TEditReportes.GetListaCampos: TZetaSmartListBox;
begin
     Result := LBCampos;
end;

procedure TEditReportes.EnabledCampos( const bEnable, bFormula, bAuto : Boolean );
begin
     lTituloCampo.Enabled := bEnable;
     ETituloCampo.Enabled := bEnable;

     lFormulaCampo.Enabled := bFormula;
     EFormulaCampo.Enabled := bFormula;
     bFormulaCampo.Enabled := bEnable;

     lMascaraCampo.Enabled := bEnable;
     CBMascaraCampo.Enabled := bEnable;

     lAnchoCampo.Enabled := bEnable;
     EAnchoCampo.Enabled := bEnable;

     lTipoCampo.Enabled := bFormula;
     CBTipoCampo.Enabled := bFormula;

     BBorraCampo.Enabled := GetListaCampos.Items.Count > 0;

     if GetListaCampos.Items.Count = 0 then
     begin
          AsignaProcCampos( NIL );
          ETituloCampo.Text := '';
          EFormulaCampo.Text := '';
          CBMascaraCampo.Text := '';
          EAnchoCampo.Text := '0';
          CBTipoCampo.ItemIndex := 0;
          AsignaProcCampos( CampoOpcionesClick );
     end;
end;

procedure TEditReportes.AsignaProcCampos( oProc : TNotifyEvent );
begin
     ETituloCampo.OnExit := oProc;
     EFormulaCampo.OnExit := oProc;
     CBMascaraCampo.OnExit :=  oProc;
     EanchoCampo.OnExit := oProc;
end;

procedure TEditReportes.CampoOpcionesClick(Sender: TObject);
begin
     inherited;
     ActualizaCamposObjeto;
end;

procedure TEditReportes.ActualizaCamposObjeto;
 var  i : integer;
      sCampo : string;
      oLista : TZetaSmartListBox;
begin
     inherited;
     oLista := GetListaCampos;
     with oLista.Items do
          if Count > 0 then
          begin
               i := oLista.ItemIndex;
               if StrLleno( ETituloCampo.Text ) then
                  TCampoOpciones( Objects[ i ] ).Titulo := ETituloCampo.Text
               else TCampoOpciones( Objects[ i ] ).Titulo := SINTITULO;

               sCampo := EFormulaCampo.Text;
               if sCampo <> TCampoOpciones( Objects[ i ] ).Formula then
                  TCampoOpciones( Objects[ i ] ).Formula := sCampo;

               if eTipoGlobal( CBTipoCampo.ItemIndex ) <> TCampoOpciones( Objects[ i ] ).TipoCampo then
               begin
                    TCampoOpciones( Objects[ i ] ).TipoCampo := eTipoGlobal( CBTipoCampo.ItemIndex );
                    GetListaMascaras( CBMascaraCampo.Items, TCampoOpciones( Objects[ i ] ).TipoCampo );

                    CBMascaraCampo.Text := GetMascaraDefault(TCampoOpciones( Objects[ i ] ).TipoCampo);
               end;

               TCampoOpciones( Objects[ i ] ).Mascara := CBMascaraCampo.Text;
               TCampoOpciones( Objects[ i ] ).Ancho := StrAsInteger( EAnchoCampo.Text );
               GetListaCampos.Items[ i ] := TCampoOpciones( Objects[ i ] ).Titulo;
          end;
          Modo := dsEdit;
end;


procedure TEditReportes.BBorraGrupoClick(Sender: TObject);
 var oControl : TWinControl;
begin
     inherited;
     oControl := BBorraGrupo;
     BorraDato( SmartListGrupos, bAgregaGrupo, bBorraGrupo, oControl );
end;

procedure TEditReportes.BBorraCampoClick(Sender: TObject);
begin
     inherited;
     BorraDato( SmartListCampos, bAgregaCampo, bBorraCampo, ETituloCampo );
     EnabledCampos( GetListaCampos.Items.Count > 0, FALSE, FALSE );
     SmartListCampos.SelectEscogido( GetListaCampos.ItemIndex );
end;

{procedure TEditReportes.LimpiaCampos;
begin
     AsignaProcCampos( NIL );
     EnabledCampos( FALSE, FALSE, FALSE );
     GBPropiedadesCampo.Caption := K_PROPIEDAD_CAMPO;
     EFormulaCampo.Text := '';
     ETituloCampo.Text := '';
     CBMascaraCampo.Text := '';
     EAnchoCampo.Text := '0';
     CBTipoCampo.ItemIndex := 0;
     AsignaProcCampos( CampoOpcionesClick );
end;
 }
procedure TEditReportes.BFormulaClick(Sender: TObject);
begin
     inherited;
     with TBitBtn(Sender) do
     begin
          SetFocus;
          if Tag = 0 then AgregaCampo
          else AgregaFormula;
     end;
end;

procedure TEditReportes.AgregaFormula;
 var oFormula : TCampoOpciones;
begin
     ETituloCampo.SelectAll;
     ETituloCampo.SetFocus;
     oFormula := TCampoOpciones.Create;
     with oFormula do
     begin
         Entidad := enFormula;
         Formula := '';
         Titulo := K_TITULO;
         Ancho := 10;
         TipoCampo := tgAutomatico;
         Calculado := -1;
     end;
     with GetListaCampos do
     begin
          Items.AddObject( oFormula.Titulo, oFormula );
          SmartListCampos.SelectEscogido( Items.Count - 1 );
          Modo := dsEdit;
     end;

end;

procedure TEditReportes.AgregaCampoDefault;
begin
     AsignaProcCampos( NIL );
     GBPropiedadesCampo.Caption := K_PROPIEDAD_CAMPO;
     EFormulaCampo.Text := '';
     ETituloCampo.Text := '';
     CBMascaraCampo.Text := '';
     EAnchoCampo.Text := '0';
     CBTipoCampo.ItemIndex := 0;
     AsignaProcCampos( CampoOpcionesClick );
end;

function TEditReportes.GetFirstPestana: TTabSheet;
begin
     Result := inherited GetFirstPestana;
     if tsCampos.TabVisible then
        Result := tsCampos;
end;

procedure TEditReportes.AlAgregarCampo( const oDiccion: TDiccionRecord;
                                        var oCampo: TCampoOpciones);
begin

end;

procedure TEditReportes.AgregaGrupo;

 var oGrupo : TGrupoOpciones;
     oDiccion: TDiccionRecord;
begin
     if AntesDeAgregarDato( Entidad, FALSE, FALSE, oDiccion, SmartListGrupos ) then
     begin
          oGrupo := TGrupoOpciones.Create;
          DiccionToCampoMaster(oDiccion,oGrupo);

          with oGrupo do
          begin
               SaltoPagina := FALSE;
               Totalizar := TRUE;
               Encabezado := TRUE;
               PieGrupo := TRUE;
          end;

          DespuesDeAgregarDato( oGrupo,
                                SmartListGrupos,
                                BBorraGrupo,
                                BAgregaGrupo );
          Modo := dsEdit;
     end;
end;

procedure TEditReportes.SmartListGruposAlBajar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     GrupoNivelEmpresa( LBGrupos.ItemIndex + 1);
     Modo := dsEdit;
end;

procedure TEditReportes.SmartListGruposAlSubir(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     GrupoNivelEmpresa( LBGrupos.ItemIndex - 1);
     Modo := dsEdit;
end;

procedure TEditReportes.AlBorrarDatos(const oEntidad : TipoEntidad);
begin
     inherited;
     BorraDatos( LBCampos.Items, oEntidad, 0 );
     BorraDatos( LBGrupos.Items, oEntidad, 1 );
end;

procedure TEditReportes.AlRevisarDatos(const oEntidad : TipoEntidad);
begin
     inherited;
     Revisa( LBCampos.Items, oEntidad, 'Campo', 0 );
     Revisa( LBGrupos.Items, oEntidad, 'Grupo', 1 );
end;

procedure TEditReportes.DespuesDeBorrarDatos;
begin
     inherited;
     EnabledCampos( NOT GetListaCampos.Items.Count = 0, FALSE, FALSE );
     EnabledGrupos( NOT LbGrupos.Items.Count = 0, FALSE );
     SmartListGrupos.SelectEscogido( 0 );
end;

procedure TEditReportes.IniciaListas;
begin
     inherited;
     LBCampos.OnDblClick := LbListaDblClick;
     LBGrupos.OnDblClick := LbListaDblClick;
end;
      
procedure TEditReportes.FormDestroy(Sender: TObject);
 var oGrupo : TGrupoOpciones;
begin
     with LBGrupos do
          while Items.Count > 0 do
          begin
               oGrupo := TGrupoOpciones(Items.Objects[0]);
               while oGrupo.ListaEncabezado.Count > 0 do
               begin
                    TCampoOpciones(oGrupo.ListaEncabezado.Objects[0]).Free;
                    oGrupo.ListaEncabezado.Delete(0);
               end;
               while oGrupo.ListaPie.Count > 0 do
               begin
                    TCampoOpciones(oGrupo.ListaPie.Objects[0]).Free;
                    oGrupo.ListaPie.Delete(0);
               end;
               TGrupoOpciones(Items.Objects[0]).Free;
               Items.Delete(0);
          end;
     inherited;
end;

procedure TEditReportes.PageControlChange(Sender: TObject);
begin
  inherited;
  with PageControl do
  begin
       HelpContext := ActivePage.HelpContext;
  end;
end;

procedure TEditReportes.ModificaListaCampos(Lista : TStrings);
 var i: integer;
begin
     for i:=0 to Lista.Count -1 do
         with TCampoOpciones(Lista.Objects[i]) do
              if PosAgente >= 0 then
              begin
                   SQLColumna := SQLAgente.GetColumna(PosAgente);
                   if (SQLColumna <> NIL ) then
                       with SQLAgente.GetColumna(PosAgente) do
                       begin
                            TipoImp := TipoFormula;
                            OpImp := Totalizacion;
                       end;
              end;
end;

procedure TEditReportes.ValidaGrupoEmpresa;
begin
     if (LBGrupos.Items.Count = 0) OR
        (LBGrupos.Items[0] <> K_EMPRESA) then
        ZetaDialogo.ZError(Caption, 'El Reporte no Tiene "Grupo Nivel Empresa"', 0)
end;

procedure TEditReportes.bFormulaGrupoClick(Sender: TObject);
begin
     inherited;
     if ConstruyeFormula( TCustomMemo(eFormulaGrupo) ) then
     begin
          with LBGrupos,TGrupoOpciones( Items.Objects[ ItemIndex] ) do
          begin
               Formula := eFormulaGrupo.Text;
               if Calculado >= 0 then
                  Calculado := -1;
               SmartListGrupos.SelectEscogido( ItemIndex );
          end;
     end;
end;


{****************}
procedure TEditReportes.GruposMovimienLista;

 var oGrupo : TGrupoOpciones;
     lEsGrupoValido : Boolean;
     FTablaPrincipal : string;

 procedure LimpiaCampo( const Posicion,iAncho : integer;
                        const sFormula : string );
 begin
      if ( oGrupo.ListaEncabezado.Count > Posicion ) then
         with TCampoListado(oGrupo.ListaEncabezado.Objects[Posicion]) do
         begin
              TCorto := '';
              Titulo := SINTITULO;
              Formula := sFormula;
              Ancho := iAncho;
              NombreColumna := '';
              TipoImp := tgTexto;
              SQLColumna.TipoFormula := tgTexto;
              Mascara := '';
              PosAgente := -1;
              Calculado := -1;
         end;
 end;
 procedure AgregaDescripcionEmpresa(oGrupo : TGrupoOpciones);
  var oContainer : TCampoListado;
 begin
      oContainer := TCampoListado.Create;
      oContainer.Entidad := enNinguno;
      oContainer.Formula := 'TITULO(1)';
      oContainer.Calculado := -1;
      oContainer.Titulo := '';
      oContainer.TCorto := '';
      oContainer.Tabla := '';
      oContainer.Ancho := 100;
      oContainer.PosAgente := -1;
      oGrupo.ListaEncabezado.AddObject('', oContainer );
 end;
 procedure AgregaGrupoEmpleado;
  var oContainer : TCampoListado;
 begin
      oGrupo := TGrupoOpciones.Create;
      oGrupo.Formula := 'CB_CODIGO';
      oGrupo.Titulo := '';
      oGrupo.Entidad := Entidad;
      oGrupo.Tabla := FTablaPrincipal;
      oGrupo.SaltoPagina := FALSE;
      oGrupo.Totalizar := FALSE;
      oGrupo.Encabezado := TRUE;
      oGrupo.PieGrupo := FALSE;
      oGrupo.PosAgente := -1;

      oContainer := TCampoListado.Create;
      oContainer.Entidad := enNinguno;
      oContainer.Formula := Format('TITULO(%d)',[nGruposListado+1]);
      oContainer.Calculado := -1;
      oContainer.Titulo := '';
      oContainer.TCorto := '';
      oContainer.Tabla := '';
      oContainer.Ancho := 100;
      oContainer.PosAgente := -1;
      oGrupo.ListaEncabezado.AddObject('', oContainer );

      LBGrupos.Items.AddObject('',oGrupo);
 end;

 function EsGrupoValido( oGrupo : TGrupoOpciones ) : Boolean;
 begin
      with oGrupo do
           Result := (Calculado = 0) AND
                     (Pos('CB_CODIGO',Formula) = 0) AND
                     (Pos('CO_NUMERO',Formula) = 0);
 end;
 var i: integer;

     lHayEmpleado, lSoloTotales : Boolean;
begin
     {$ifdef TRESS}
     if Entidad = enMovimienLista then
        FTablaPrincipal := 'TMPLISTA'
     else FTablaPrincipal := 'TMPNOMTOT';
     {$endif}

     if oTempGrupos = NIL then
        oTempGrupos := TStringList.Create;
     AsignaGrupos(oTempGrupos,LBGrupos.Items);

     with LBGrupos do
     begin

          ZFuncsCliente.nGruposListado := iMin( Items.Count - 1 , K_MAXGRUPOS );
          lEsGrupoValido := nGruposListado = 0;
          lHayEmpleado := FALSE;
          lSoloTotales := GetSoloTotales;
          if Items.Count > 0 then
          begin

               oGrupo := TGrupoOpciones( Items.Objects[0] );
               oGrupo.Totalizar := FALSE;
               if (Items.Count = 1) AND
                  lSoloTotales then
                  AgregaDescripcionEmpresa(oGrupo);

               for i := 1 to Items.Count - 1 do
               begin
                    oGrupo := TGrupoOpciones( Items.Objects[i] );
                    oGrupo.Totalizar := FALSE;

                    if lEsGrupoValido OR EsGrupoValido(oGrupo) then
                    begin
                         with oGrupo do
                         begin
                              Grupos[i][1] := Formula;
                              Formula := FTablaPrincipal+'.TA_NIVEL'+IntToStr(i);
                              NombreColumna := '';
                              if (oGrupo.ListaEncabezado.Count>= 1) then
                                 Grupos[i][2] := TCampoListado(oGrupo.ListaEncabezado.Objects[0]).Titulo;
                              LimpiaCampo( 0,100,Format('TITULO(%d)',[i]) );
                              //PARA QUE SE UTILIZA GRUPOS[I][3]?
                              //Para poder hacer el FieldByName de la descripcion de los grupos.
                              Grupos[i][3]:='';
                              if oGrupo.ListaEncabezado.Count > 1 then
                              begin
                                   Grupos[i][3] := TCampoListado(oGrupo.ListaEncabezado.Objects[1]).NombreCampo;
                                   LimpiaCampo( 1,0,'');
                              end;
                         end;
                    end
                    else
                    begin
                         lHayEmpleado := TRUE;
                         Dec(nGruposListado);
                         LimpiaCampo( 0,100,Format('TITULO(%d)',[i]) );
                         LimpiaCampo( 1,0,'');
                    end;
               end;
          end;
     end;

     if NOT lSoloTotales AND (lEsGrupoValido OR NOT lHayEmpleado) then
     begin
          AgregaGrupoEmpleado;
     end;
end;

procedure TEditReportes.AsignaGrupos(oDestino,oFuente : TStrings);
 procedure AsignaCampo(oDestino,oFuente : TCampoOpciones);
 begin
      with oFuente do
      begin
           oDestino.NombreColumna:=NombreColumna;
           oDestino.Entidad:=Entidad;
           oDestino.Tabla:=Tabla;
           oDestino.Formula:=Formula;
           oDestino.Titulo:=Titulo;
           oDestino.TCorto:=TCorto;
           oDestino.TipoCampo:=TipoCampo;
           oDestino.Calculado:=Calculado;
           oDestino.PosAgente:=PosAgente;
           oDestino.Justificacion:=Justificacion;
           oDestino.Requiere:=Requiere;
           oDestino.Mascara:=Mascara;
           oDestino.Ancho:=Ancho;
           oDestino.LeftTitulo:=LeftTitulo;
           oDestino.LeftCampo:=LeftCampo;
           oDestino.AnchoTitulo:=AnchoTitulo;
           oDestino.AnchoCampo:=AnchoCampo;
           oDestino.Top:=Top;
           oDestino.Alto:=Alto;
           oDestino.Posicion:=Posicion;
           //oDestino.TipoImp:=TipoImp;
           oDestino.Operacion:=Operacion;
           //oDestino.OpImp:=OpImp;
      end;
 end;

 var oCampo : TCampoOpciones;
     oGrupo : TGrupoOpciones;
     i,j : integer;
begin
     with oFuente do
          for i:= 0 to Count - 1do
          begin
               oGrupo := TGrupoOpciones.Create;
               with TGrupoOpciones(Objects[i]) do
               begin
                    oGrupo.SaltoPagina:=SaltoPagina;
                    oGrupo.Totalizar:=Totalizar;
                    oGrupo.Encabezado:=Encabezado;
                    oGrupo.PieGrupo:=PieGrupo;
                    oGrupo.Master:=Master;
                    oGrupo.Ultimo:=Ultimo;
                    oGrupo.NombreColumna:=NombreColumna;
                    oGrupo.Entidad:=Entidad;
                    oGrupo.Tabla:=Tabla;
                    oGrupo.Formula:=Formula;
                    oGrupo.Titulo:=Titulo;
                    oGrupo.TCorto:=TCorto;
                    oGrupo.TipoCampo:=TipoCampo;
                    oGrupo.Calculado:=Calculado;
                    oGrupo.PosAgente:=PosAgente;

                    for j := 0 to ListaEncabezado.Count -1 do
                    begin
                         oCampo := TCampoOpciones.Create;
                         AsignaCampo(oCampo,TCampoOpciones(ListaEncabezado.Objects[j]));
                         oGrupo.ListaEncabezado.AddObject(oCampo.Titulo,oCampo);
                    end;
                    for j := 0 to ListaPie.Count -1 do
                    begin
                         oCampo := TCampoOpciones.Create;
                         AsignaCampo(oCampo,TCampoOpciones(ListaPie.Objects[j]));
                         oGrupo.ListaPie.AddObject(oCampo.Titulo,oCampo);
                    end;
               end;
               oDestino.AddObject(oGrupo.Titulo,oGrupo);
          end;
end;

procedure TEditReportes.ParametrosListadoNomina(FParamList : TZetaParams);
begin
     //ZReportTools.ParametrosListadoNomina( FParamList, LBGrupos.Items );
end;

procedure TEditReportes.ParametrosMovimienLista(FParamList : TZetaParams);
begin
     //ZReportTools.ParametrosMovimienLista( FParamList, LBGrupos.Items );
end;


{procedure TEditReportes.ParametrosListadoNomina(FParamList : TZetaParams);
 var j, i : integer;
begin
     ZFuncsCliente.RegistraFuncListadoTotales;
     FParamList.AddInteger('CountDatosEmpleado',0);
     with LBGrupos do
     begin
          for i:=1 to iMin(K_MAXGRUPOS,Items.Count-1) do
          begin
               with TGrupoOpciones(Items.Objects[i]) do
                    if (Pos('CB_CODIGO',Formula) <> 0) then
                    begin
                         FParamList.AddInteger('CountDatosEmpleado',ListaEncabezado.Count);
                         for j:=0 to ListaEncabezado.Count - 1 do
                             FParamList.AddString( Format('DatosEmpl%d',[j]),
                                                   TCampoOpciones(ListaEncabezado.Objects[j]).Formula );
                    end;
          end;
     end;
end;

procedure TEditReportes.ParametrosMovimienLista(FParamList : TZetaParams);
 var i,j:integer;
begin
     j:= 1;
     ZFuncsCliente.RegistraFuncListadoTotales;
     FParamList.AddBoolean( 'TotalesNeto', TRUE );

     for i:= 1 to K_MAXGRUPOS do
     begin
          FParamList.AddString( 'TA_NIVEL'+IntToStr(i),'');
          FParamList.AddString( 'TIT_NIVEL'+IntToStr(i), '' );
          FParamList.AddString( 'DES_NIVEL'+IntToStr(i), '' );
     end;

     with LBGrupos do
     begin
          for i:=1 to iMin(K_MAXGRUPOS,Items.Count-1) do
          begin
               with TGrupoOpciones(Items.Objects[i]) do
                    if (Pos('CB_CODIGO',Formula) = 0) AND
                       (Pos('CO_NUMERO',Formula) = 0) then
                    begin
                         FParamList.AddString( 'TA_NIVEL'+IntToStr(j),Formula );//Codigo del Grupo
                         if ListaEncabezado.Count > 0 then //Quiere decir que tpor lo menos tiene Codigo
                         begin
                              FParamList.AddString( 'TIT_NIVEL'+IntToStr(j),TCampoOpciones( ListaEncabezado.Objects[0] ).Titulo );//Codigo del Grupo

                              if ListaEncabezado.Count > 1 then //Quiere decir que tiene Codigo y Descripcion
                                 FParamList.AddString( 'DES_NIVEL'+IntToStr(j),
                                                       TCampoOpciones( ListaEncabezado.Objects[1] ).Formula )//Descripcion del Grupo
                              else
                                  FParamList.AddString( 'DES_NIVEL'+IntToStr(j), '' )
                         end
                         else
                              FParamList.AddString( 'TIT_NIVEL'+IntToStr(i),Titulo );//Codigo del Grupo
                         inc(j)
                    end
                    else if (Pos('CB_CODIGO',Formula) > 0) then
                    begin
                         FParamList.AddBoolean( 'TotalesNeto', Totalizar );
                    end;
          end;
     end;
     FParamList.AddInteger('CountGrupos',j-1);
end;
}

function TEditReportes.GetSoloTotales: Boolean;
begin
     Result := FALSE;
end;

procedure TEditReportes.DespuesDeConstruyeSQL;
begin
     {$ifdef TRESS}
     if Entidad in [enMovimienLista] then
        GruposMovimienLista;
     {$endif}
end;

procedure TEditReportes.DespuesDeGeneraReporte;
begin
     {$ifdef TRESS}
     if (FEntidadActual in [enMovimienLista]) AND
        (oTempGrupos <> NIL) then
     begin
          LimpiaLista(LbGrupos.Items);
          AsignaGrupos(LbGrupos.Items,oTempGrupos);
          LimpiaLista(oTempGrupos);
          oTempGrupos.Free;
          oTempGrupos:= NIL;
          SmartListGrupos.SelectEscogido( 0 );
     end;
     {$endif}
end;


procedure TEditReportes.BAgregaFormulaGrupoClick(Sender: TObject);
begin
     inherited;
     with TBitBtn(Sender) do
     begin
          SetFocus;
          if Tag = 0  then AgregaGrupo
          else AgregaFormulaGrupo;
     end;
end;

procedure TEditReportes.AgregaFormulaGrupo;
 var oFormula : TGrupoOpciones;
begin
     BAgregaFormulaGrupo.SetFocus;
     oFormula := TGrupoOpciones.Create;
     with oFormula do
     begin
          Entidad := enFormula;
          Formula := '';
          Titulo := K_TITULO;
          TipoCampo := tgAutomatico;
          Calculado := -1;
          Encabezado := TRUE;
          PieGrupo := TRUE;
          Totalizar := TRUE;
     end;
     with LBGrupos do
     begin
          Items.AddObject( oFormula.Titulo, oFormula );
          SmartListGrupos.SelectEscogido( Items.Count - 1 );
          Modo := dsEdit;
     end;
     ETituloGrupo.SelectAll;
     ETituloGrupo.SetFocus;
end;


function TEditReportes.GetGruposStrings: TStrings;
begin
     Result := LBGrupos.Items;
end;

end.


