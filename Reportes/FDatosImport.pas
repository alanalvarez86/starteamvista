unit FDatosImport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaCommonLists, ZReportTools, ZReportToolsConsts;

type
  TDatosImport = class(TForm)
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    lbEmpresa: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbUsuario: TLabel;
    lbFecha: TLabel;
    lbNombre: TLabel;
    lbTipo: TLabel;
    lbTabla: TLabel;
    Panel1: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Label12: TLabel;
    eNombre: TEdit;
    Label2: TLabel;
    lbPlantilla: TLabel;
    cbImportar: TCheckBox;
    Label3: TLabel;
    lbClasificacion: TLabel;
  private
    FPlantilla : string;
    FClasificacion : eClasifiReporte;
    function GetNombreNuevo: string;
    procedure SetEmpresa(const Value: string);
    procedure SetFecha(const Value: string);
    procedure SetNombre(const Value: string);
    procedure SetNombreNuevo(const Value: string);
    procedure SetTabla(const Value: string);
    procedure SetTipo(const Value: string);
    procedure SetUsuario(const Value: string);
    procedure SetPlantilla(const Value: string);
    procedure SetClasificacion(const Value: eClasifiReporte);
    function GetImportarPlantilla : Boolean;
  public
    property Empresa : string write SetEmpresa;
    property Usuario : string write SetUsuario;
    property Fecha : string write SetFecha;
    property Nombre : string write SetNombre;
    property Tipo : string write SetTipo;
    property Tabla : string write SetTabla;
    property Clasificacion : eClasifiReporte read FClasificacion write SetClasificacion;
    property Plantilla : string read FPlantilla write SetPlantilla;
    property ImportarPlantilla : Boolean read GetImportarPlantilla;
    property NombreNuevo : string read GetNombreNuevo write SetNombreNuevo;

  end;

var
  DatosImport: TDatosImport;

implementation
uses ZetaCommonClasses, DReportes;
{$R *.DFM}

{ TDatosImport }

{ TDatosImport }


function TDatosImport.GetNombreNuevo: string;
begin
     Result := eNombre.Text;
end;

procedure TDatosImport.SetNombreNuevo(const Value: string);
begin
     eNombre.Text := Copy(Value,1,30);
end;

procedure TDatosImport.SetEmpresa(const Value: string);
begin
     lbEmpresa.Caption := Value;
end;

procedure TDatosImport.SetFecha(const Value: string);
begin
     lbFecha.Caption := Value;
end;

procedure TDatosImport.SetNombre(const Value: string);
begin
     lbNombre.Caption := Value;
end;

procedure TDatosImport.SetTabla(const Value: string);
begin
     lbTabla.Caption := Value;
end;

procedure TDatosImport.SetTipo(const Value: string);
begin
     lbTipo.Caption := Value;
end;

procedure TDatosImport.SetUsuario(const Value: string);
begin
     lbUsuario.Caption := Value;
end;

procedure TDatosImport.SetPlantilla(const Value: string);
begin

     if ( Value = '' ) OR (Value=(K_TEMPLATE+'.QR2')) then
     begin
          CBImportar.Checked:= FALSE;
          CBImportar.Enabled:= FALSE;
          lbPlantilla.Caption := K_TEMPLATE+'.QR2';
     end
     else if Value = K_NO_REQUIERE then
     begin
          CBImportar.Checked:= FALSE;
          CBImportar.Enabled:= FALSE;
          lbPlantilla.Caption := Value;
     end
     else
     begin
          CBImportar.Checked:= TRUE;
          CBImportar.Enabled:= TRUE;
          lbPlantilla.Caption := Value;
     end;
     FPlantilla := lbPlantilla.Caption;
end;


function TDatosImport.GetImportarPlantilla : Boolean;
begin
     Result := CBImportar.Checked;
end;

procedure TDatosImport.SetClasificacion(const Value: eClasifiReporte);
begin
     lbClasificacion.Caption := dmReportes.ObtieneClasificacion( Value );
     fClasificacion := Value;
end;


end.
