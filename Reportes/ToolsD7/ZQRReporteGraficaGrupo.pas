unit ZQRReporteGraficaGrupo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QRDLDR, QRDesign, QuickRpt, Qrdctrls, ExtCtrls, thsdRuntimeLoader,
  thsdRuntimeEditor, qrdBaseCtrls, qrdQuickrep;

type
  TQRGraficaGrupo = class(TForm)
    FQReportGrupos: TDesignQuickReport;
    FQrDesignerGroups: TQRepDesigner;
    procedure FQReportGruposPreview(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  QRGraficaGrupo: TQRGraficaGrupo;

implementation

uses FPreview;
{$R *.DFM}

procedure TQRGraficaGrupo.FQReportGruposPreview(Sender: TObject);
begin
     FQReportGrupos.Zoom := 100;

     with TPreview.Create( self ) do
     begin
          QRPreview.Zoom := 100;
          Caption := '';//PENDIENTE
          QRPrinter := FQReportGrupos.QRPrinter;
          Reporte := FQReportGrupos;
          QRPreview.QRPrinter := FQReportGrupos.QRPrinter;
          WindowState := wsMaximized;
     end;
end;

end.


    object QRDesignBand7: TQRDesignBand
      Left = 48
      Top = 53
      Width = 720
      Height = 5
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        13.229166666666670000
        1905.000000000000000000)
      BandType = rbTitle
      EditorEnabled = True
      BandNum = 0
    end
    object QRDesignBand8: TQRDesignBand
      Left = 48
      Top = 73
      Width = 720
      Height = 8
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        21.166666666666670000
        1905.000000000000000000)
      BandType = rbPageFooter
      EditorEnabled = True
      BandNum = 0
    end
    object QRDesignBand9: TQRDesignBand
      Left = 48
      Top = 48
      Width = 720
      Height = 5
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        13.229166666666670000
        1905.000000000000000000)
      BandType = rbPageHeader
      EditorEnabled = True
      BandNum = 0
    end
    object QRDesignBand10: TQRDesignBand
      Left = 48
      Top = 63
      Width = 720
      Height = 5
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ForceNewColumn = False
      ForceNewPage = False
      ParentFont = False
      Size.Values = (
        13.229166666666670000
        1905.000000000000000000)
      BandType = rbDetail
      EditorEnabled = True
      BandNum = 0
    end
    object QRDesignBand11: TQRDesignBand
      Left = 48
      Top = 68
      Width = 720
      Height = 5
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False           
      Size.Values = (
        13.229166666666670000
        1905.000000000000000000)
      BandType = rbSummary
      EditorEnabled = True
      BandNum = 0
    end
    object QRDesignBand12: TQRDesignBand
      Left = 48
      Top = 58
      Width = 720
      Height = 5
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      AlignToBottom = False
      Color = clWhite
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        13.229166666666670000
        1905.000000000000000000)
      BandType = rbColumnHeader
      EditorEnabled = True
      BandNum = 0
    end
