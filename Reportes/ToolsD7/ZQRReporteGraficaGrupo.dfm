object QRGraficaGrupo: TQRGraficaGrupo
  Left = 224
  Top = 181
  Width = 696
  Height = 480
  Caption = 'QRGraficaGrupo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object FQReportGrupos: TDesignQuickReport
    Left = 0
    Top = 0
    Width = 816
    Height = 1056
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnPreview = FQReportGruposPreview
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = Letter
    Page.Values = (
      127
      2794
      127
      2159
      127
      127
      0)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrintIfEmpty = True
    SnapToGrid = True
    Units = Inches
    Zoom = 100
    DesignBackgroundDX = 0
    DesignBackgroundDY = 0
  end
  object FQrDesignerGroups: TQRepDesigner
    SaveLoadPrinterSetup = False
    UseDataModules = False
    Version = '1.22'
    QReport = FQReportGrupos
    ScriptsEnabled = True
    AddCursor = crDefault
    AllowBlockEdit = False
    AllowNewCalcFields = False
    AllowSQLEdit = True
    AutoEditAfterInsert = True
    BackupFileExtension = '.BAK'
    BandNameFont.Charset = DEFAULT_CHARSET
    BandNameFont.Color = clSilver
    BandNameFont.Height = 20
    BandNameFont.Name = 'MS Sans Serif'
    BandNameFont.Style = []
    ComponentFrameColor = clGray
    ComponentFrameStyle = psDot
    DefaultDatabaseOnly = False
    GridSizeX = 1
    GridSizeY = 1
    KeyboardMoveX = 1
    KeyboardMoveY = 1
    MinElementWidth = 5
    MinElementHeight = 5
    QueriesOnlyInExpert = False
    QueriesOnlyInDBSetup = False
    SaveBackupFiles = False
    SaveImagesInReportfile = False
    ScriptEditByUser = False
    ShowBandNames = True
    ShowComponentFrames = False
    ShowDatafieldListbox = True
    UndoEnabled = False
    DatafieldsSorted = False
    SQLSettings.DelimiterType = delNoQuotes
    SQLSettings.SQLStringDelimiterLeft = #39
    SQLSettings.SQLStringDelimiterRight = #39
    SQLSettings.SQLTableDelimiterLeft = #39
    SQLSettings.SQLTableDelimiterRight = #39
    SQLSettings.SQLDateDelimiterLeft = #39
    SQLSettings.SQLDateDelimiterRight = #39
    SQLSettings.SQLWildcard = '%'
    Left = 32
    Top = 16
  end
end
