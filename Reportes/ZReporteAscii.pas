unit ZReporteAscii;

interface
uses DB,Classes,SysUtils, Forms, Controls, ZetaAsciiFile, ZReportTools, ZReportToolsConsts,
     Variants,
     ZetaCommonLists, Windows;

type
    TReporteAscii = class
    private
       {$ifdef XMLREPORT}
       FStream: TStrings;
       fAsciiFile : TAsciiStream;
       {$else}
       fAsciiFile : TAsciiFile;
       {$endif}
       fDataSet : TDataSet;
       FSoloTotales : Boolean;
       FCampos : TStrings;
       FCountCorte,FAnchoAscii,FAnchoAsciiDes, FGrupoCount : integer;
       FAsciiSoloTotales : Boolean;
       FGrupoTotales : string;
       FDescrip, FCodigos,FAlias : TStringList;
       FTipoAscii : eTipoFormato;
       aGrupos : oleVariant;
       procedure AgregaDelimitado(oCampos: TStrings; const lTotal: Boolean);
       procedure AgregaFijo( oCampos: TStrings; const lTotal: Boolean;  const nCampo: integer = 0 );
       procedure AgregaGrupo( oGrupos: TStrings; const lEncabezado, lDelimitado: Boolean);
       procedure AgregaTitulo(oCampos: TStrings );
       procedure AgregaTituloSoloTotales(oCampos : TStrings; const nCampo: integer = 0 );
       procedure BajaRenglon;
       function AgregaDatosTotales: Boolean;
       function HayTitulos(oCampos: TStrings; const nCampo: integer = 0): Boolean;
    public
       {$ifdef XMLREPORT}
       property Stream: TStrings read FStream write FStream;
       {$endif}
         function GeneraAscii( oDataSet: TDataSet;
                               oDatosImpresion : TDatosImpresion;
                               oCampos,oGrupos: TStrings;
                               const lSoloTotales,lDialogoConfirmacion : Boolean ) : Boolean;
    end;

implementation

uses
     ZetaCommonClasses,
     ZetaCommonTools,
     {$ifdef AUTOLIV}
     dReportes,
     dGlobal,
     zGlobalTress,
     {$endif}
     ZetaDialogo;

{ TReporteAscii }

function TReporteAscii.GeneraAscii( oDataSet: TDataSet;
                                    oDatosImpresion : TDatosImpresion;
                                    oCampos,oGrupos: TStrings;
                                    const lSoloTotales, lDialogoConfirmacion : Boolean ): Boolean;
 procedure InitAsciiFile(const sFile : string);
 begin
     {$ifdef XMLREPORT}
      fAsciiFile := TAsciiStream.Create;
      fAsciiFile.Stream := Self.Stream;
      {$else}
      fAsciiFile := TAsciiFile.Create( sFile );
      {$endif}
      {CV: 06-Oct-2003}
      {Se realiz� este cambio para que los acentos aparezcan correctamente dentro de Word}
      {fAsciiFile.OEMConvert := TRUE;
      FTipoAscii := oDatosImpresion.Tipo;}

      {***(@am):La bandera OEMConvert determina si se realiza una conversion del juego de caracters ANSI(Windows) hacia el juego de caracteres OEM (Dos).
                Hasta la vs. 2013 del sistema se seguia utilizando esta conversion. Sin embargo, llegamos a la conclusion de que realizar esta conversion no
                es necesario, pues el ambiente en el que opera el sistema actualmente es Windows. Asi que se inicializa la bander en False para todos los
                archivos tipo ASCII y CSV. Se sigue dejando la opcion del else pues se desconoce si se utiliza para algun otro tipo salida.***}
      {$IFDEF TRESS_DELPHIXE5_UP}
      if oDatosImpresion.Tipo in [ tfMailMerge, tfASCIIDel, tfASCIIFijo, tfCSV] then
      begin
          if oDatosImpresion.Tipo = tfMailMerge then
              FTipoAscii := tfASCIIDel
          else
              FTipoAscii := oDatosImpresion.Tipo;
           fAsciiFile.OEMConvert := FALSE;
      end
      else
      begin
           FTipoAscii := oDatosImpresion.Tipo;
           fAsciiFile.OEMConvert := TRUE;
      end;
      {$ELSE}
      if oDatosImpresion.Tipo = tfMailMerge then
      begin
           FTipoAscii := tfASCIIDel;
           fAsciiFile.OEMConvert := FALSE;
      end
      else
      begin
           FTipoAscii := oDatosImpresion.Tipo;
           fAsciiFile.OEMConvert := TRUE;
      end;
      {$ENDIF}
      case FTipoAscii of
           tfASCIIDel, tfCSV :
           begin
                fAsciiFile.Separador:= {$ifdef TRESS_DELPHIXE5_UP}String({$endif} oDatosImpresion.Separador {$ifdef TRESS_DELPHIXE5_UP}){$endif};
                if oDatosImpresion.Separador = '|' then
                   fAsciiFile.Delimitador := ''
           end;
           tfASCIIFijo : fAsciiFile.Separador := '';
      end;
 end;
 procedure InitListaCampos;
  var i :integer;
 begin
      FCampos := TStringList.Create;
      if FSoloTotales then
      begin
           if (oGrupos.Count > 1) then
           begin
                if FTipoAscii = tfASCIIFijo then
                begin
                     FGrupoCount := oGrupos.Count-1;
                     FAsciiSoloTotales := TRUE;
                     with oGrupos do
                     begin
                          for i:=1 to FGrupoCount do
                          begin
                               with TGrupoOpciones(Objects[i]),ListaEncabezado do
                               begin
                                    if ListaEncabezado.Count > 0 then
                                    begin
                                         FAnchoAscii := FAnchoAscii + TCampoOpciones(Objects[0]).Ancho;
                                         FCampos.AddObject(Strings[0],Objects[0]);
                                         if ListaEncabezado.Count > 1 then
                                            FAlias.Add(TCampoOpciones(Objects[1]).NombreCampo);
                                    end;
                               end;
                          end;
                          FAnchoAsciiDes :=FAnchoAscii;
                          with TGrupoOpciones(Objects[FGrupoCount]),ListaEncabezado do
                               if ListaEncabezado.Count > 1 then
                               begin
                                    FAnchoAscii := FAnchoAscii + TCampoOpciones(Objects[1]).Ancho;
                                    FCampos.AddObject(Strings[1],Objects[1]);

                               end;

                     end;
                end
                else
                begin
                     with TGrupoOpciones(oGrupos.Objects[oGrupos.Count-1]).ListaEncabezado do
                          for i:=0 to Count - 1 do
                              FCampos.AddObject(Strings[i],Objects[i]);
                end;
           end;
           for i:=0 to oCampos.Count - 1 do
               with TCampoListado(oCampos.Objects[i]) do
                    if not (OpImp in [ocNinguno,ocAutomatico]) then
                       FCampos.AddObject(oCampos[i],oCampos.Objects[i]);
      end
      else
      begin
           with oCampos do
                for i:=0 to oCampos.Count - 1 do
                    with TCampoListado(oCampos.Objects[i]) do
                         if (FTipoAscii <> tfASCIIFijo) or (Ancho <> 0) then
                            FCampos.AddObject(Strings[i],Objects[i]);

      end;
      if FCampos.Count = 0 then
         raise Exception.Create( 'Todas las Columnas Tienen Ancho CERO.'+CR_LF+
                                 'No Hay Informaci�n a Exportar' );
 end;
 procedure InitArregloGrupos;
  var i : integer;
      aGrupo : oleVariant;
 begin
      aGrupos := VarArrayCreate([1, oGrupos.Count-1], varVariant);
      with oGrupos do
           for i:=1 to Count-1 do
           begin
                aGrupo := VarArrayCreate([1,2],varVariant);
                with TGrupoOpciones(Objects[i]) do
                begin
                     aGrupo[1] := NombreCampo;
                     aGrupo[2] := '';
                end;
                aGrupos[i] := aGrupo;
           end;
 end;

 function CorteGrupo(const lActualiza : Boolean) : integer;
  var i, j: integer;
      aGrupo : OleVariant;
 begin
      Result := -1 ;
      if not fSoloTotales then
      begin
           for i:= VarArrayLowBound(aGrupos,1) to VarArrayHighBound(aGrupos,1) do
               if FDataSet.FieldByName(aGrupos[i][1]).AsString <> aGrupos[i][2] then
               begin
                    Result := i;
                    if lActualiza then
                    begin
                         for j:= i to VarArrayHighBound(aGrupos,1) do
                         begin
                              aGrupo := aGrupos[j];
                              aGrupo[2] := FDataSet.FieldByName(aGrupos[j][1]).AsString;
                              aGrupos[j] := aGrupo;
                         end;
                    end;
                    Break;
               end;
      end;
 end;

 procedure AgregaEncabezadosGrupo( const iCorteGrupo : integer; const lDelimitado : Boolean );
  var i : integer;
 begin
      if iCorteGrupo > 0 then
      begin
           for i := iCorteGrupo to oGrupos.Count - 1 do
               with TGrupoOpciones(oGrupos.Objects[i]) do
                    if Encabezado then
                    begin
                         if lDelimitado then AgregaDelimitado( ListaEncabezado, TRUE )
                         else AgregaFijo( ListaEncabezado, TRUE, 0 );
                    end;
      end;
 end;

 procedure AgregaPieGrupo( const iCorteGrupo : integer; const lDelimitado,lFinArchivo : Boolean );
  var i : integer;
 begin
      if (iCorteGrupo > 0) AND (not lFinArchivo) then
      begin
           if NOT ( FDataSet.EOF ) then FDataset.Prior;
           for i := oGrupos.Count - 1 downTo iCorteGrupo do
               with TGrupoOpciones(oGrupos.Objects[i]) do
               begin
                    if PieGrupo then
                    begin
                         if lDelimitado then AgregaDelimitado( ListaPie, TRUE )
                         else AgregaFijo( ListaPie, TRUE, 0 );
                    end;
               end;
            if NOT ( FDataset.Bof ) then FDataset.Next;
      end;
 end;

 var
   {$ifndef XMLREPORT}
   oCursor : TCursor;
   {$endif}
   sArchivoTemp, sArchivo : string;
   {$IFDEF TRESS_REPORTES}
   sArchivoCopia: String;
   {$ENDIF}
   {$ifndef XMLREPORT}
   oArchivoTemp, oArchivo :TStrings;
   {$IFDEF TRESS_REPORTES}
   oArchivoCopia: TStrings;
   {$ENDIF}
   {$endif}
   nCampo : integer;
   {$ifdef AUTOLIV}
   sListaReportes, sLlave, sLinea, sLineaEncriptada: string;
   oListaReportes, oEncriptado: TStringList;
   iReporte, iContador: Integer;
   lErrorEncriptado: Boolean;
  {$endif}

begin

     {$ifndef XMLREPORT}
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     Result := TRUE;
     {$endif}

     {$ifdef AUTOLIV}
     lErrorEncriptado := false;
     {$endif}

     FAsciiSoloTotales := FALSE;
     FGrupoCount := 0;
     FAnchoAscii := 0;
     FGrupoTotales := '';
     FDescrip := TStringList.Create;
     FCodigos := TStringList.Create;
     FAlias  := TStringList.Create;
     try
        fDataSet := oDataSet;
        try
           FSoloTotales := lSoloTotales;
           sArchivo := oDatosImpresion.Exportacion;
           {$IFDEF TRESS_REPORTES}
           if oDatosImpresion.Copia <> VACIO then
           begin
                sArchivoCopia := oDatosImpresion.Copia;
           end;
           {$ENDIF}
           sArchivoTemp := sArchivo+'_TEMP_.txt';
           {$ifndef XMLREPORT}
           SysUtils.DeleteFile( sArchivo );
           SysUtils.DeleteFile( sArchivoTemp );
           {$IFDEF TRESS_REPORTES}
           if oDatosImpresion.Copia <> VACIO then
           begin
                SysUtils.DeleteFile( sArchivoCopia );
           end;
           {$ENDIF}
           {$endif}
           InitAsciiFile(sArchivoTemp);
           InitListaCampos;
           InitArregloGrupos;

           if FSoloTotales AND NOT TGrupoOpciones(oGrupos.Objects[0]).Totalizar then
           begin
                FDataSet.Last;
                FDataset.Delete;
           end;

           FDataSet.First;

           if ( FTipoAscii in [tfASCIIDel,tfCSV] ) then
           begin
                if FTipoAscii = tfASCIIDel then
                   AgregaTitulo(FCampos);
                with fDataSet do
                     while NOT EOF do
                     begin
                          AgregaEncabezadosGrupo(CorteGrupo(TRUE), TRUE);

                          AgregaDelimitado( FCampos, TRUE );
                          Next;

                          AgregaPieGrupo(CorteGrupo(FALSE), TRUE, EOF );
                     end;
                if NOT FSoloTotales then AgregaPieGrupo(1, TRUE, FALSE);
                AgregaGrupo( oGrupos,FALSE,TRUE);
           end
           else
           begin
                if FAsciiSoloTotales AND (FCampos.Count>0) then
                   AgregaTituloSoloTotales(FCampos,FGrupoCount);

                with fDataSet do
                     while NOT EOF do
                     begin

                          AgregaEncabezadosGrupo(CorteGrupo(TRUE),FALSE);

                          if AgregaDatosTotales then
                             nCampo := 0
                          else nCampo := FGrupoCount+1;

                          AgregaFijo( FCampos, TRUE, nCampo );
                             
                          Next;

                          AgregaPieGrupo(CorteGrupo(FALSE),FALSE, EOF);
                     end;

                if NOT FSoloTotales then AgregaPieGrupo(1,FALSE, FALSE);//Para que se imprime el ultimo pie de grupo;
                AgregaGrupo( oGrupos,FALSE, FALSE );
           end;
        finally
               Result := fAsciiFile.Renglones > 0;
               {$ifndef XMLREPORT}
               fAsciiFile.Free;
               FAsciiFile := NIL;
               {$endif}

               if Result then
                  try
                     {$ifndef XMLREPORT}
                     InitAsciiFile(sArchivo);
                     {$IFDEF TRESS_REPORTES}
                     if oDatosImpresion.Copia <> VACIO then
                     begin
                          // InitAsciiFile(sArchivoCopia);
                     end;
                     {$ENDIF}
                     {$endif}
                     AgregaGrupo( oGrupos,TRUE, FTipoAscii<>tfASCIIFijo); //Encabezado
                     {$ifdef XMLREPORT}
                     {$endif}
                  finally
                         fAsciiFile.Free;
                         FAsciiFile := NIL;
                  end;
        end;

        {$ifndef XMLREPORT}
        if Result AND FileExists(sArchivo) then
        begin
             oArchivo := TStringList.Create;
             {$IFDEF TRESS_REPORTES}
             if oDatosImpresion.Copia <> VACIO then
             begin
                  oArchivoCopia := TStringList.Create;
             end;
             {$ENDIF}
             try
                try
                   oArchivo.LoadFromFile(sArchivo);
                   {$IFDEF TRESS_REPORTES}
                   if oDatosImpresion.Copia <> VACIO then
                   begin
                        // oArchivoCopia.LoadFromFile(sArchivoCopia);
                        oArchivoCopia.LoadFromFile(sArchivo);
                   end;
                   {$ENDIF}
                except
                      on Error: Exception do
                      begin
                           Result := FALSE;
                           {$IFNDEF TRESS_REPORTES}
                           ZetaDialogo.ZError( 'Error al Abrir Archivo',
                                               'El Archivo:' + CR_LF + sArchivo + CR_LF + 'No Pudo ser Abierto. ' + CR_LF +
                                               'Revisar Que No Est� Siendo Usado Por Otra Aplicaci�n', 0);
                           {$ELSE}
                           // Colocar env�o de error a bit�cora de servicio de reportes.
                           {$ENDIF}
                      end;
                end;
                if Result then
                begin
                     oArchivoTemp := TStringList.Create;
                     try
                        oArchivoTemp.LoadFromFile(sArchivoTemp);
                        oArchivo.AddStrings(oArchivoTemp);

                        {$ifdef AUTOLIV}
                        oArchivoTemp.Clear;
                        oArchivoTemp.Assign( oArchivo );
                        // Obtiene el # reporte
                        oListaReportes := TStringList.Create;
                        try
                           oEncriptado := TStringList.Create;
                           try
                              oEncriptado.Clear;
                              iReporte := dmReportes.cdsReportes.FieldByName( 'RE_CODIGO' ).AsInteger;
                              sListaReportes := Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL2 );
                              sLlave := Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL3 );

                              if StrLleno( sListaReportes )then
                                 oListaReportes.CommaText := sListaReportes;

                              if (oListaReportes.Count > 0 ) and (oListaReportes.IndexOf(Format( '%d', [ iReporte ] )) > -1 ) and StrLleno( sLlave )then
                              begin
                                   if (oArchivoTemp.Count > 0)then
                                   begin
                                        for iContador := 0 to (oArchivoTemp.Count - 1) do
                                        begin
                                             sLinea := oArchivoTemp.Strings[ iContador ];

                                             sLineaEncriptada := ZetaCommonTools.EncriptaWMLOCK( sLinea, sLlave, lErrorEncriptado );
                                             oEncriptado.Add( sLineaEncriptada );
                                             sLineaEncriptada := '';
                                             if lErrorEncriptado then
                                             begin
                                                  ZetaDialogo.zError( 'Encriptado de archivo', Format( 'Error al encriptar la l�nea #%d', [ iContador + 1 ] ), 0 );
                                                  break;
                                             end;
                                        end;

                                        if ( oEncriptado.Count <> 0 ) then
                                        begin
                                             oArchivoTemp.Clear;
                                             oArchivoTemp.Assign( oEncriptado );
                                             oArchivo.Clear;
                                             oArchivo.Assign( oArchivoTemp );
                                        end;
                                   end;
                              end;
                           finally
                                  FreeAndNil( oEncriptado );
                           end;
                        finally
                               FreeAndNil( oListaReportes );
                        end;
                        {$endif}

                        oArchivo.SaveToFile(sArchivo);
                        // SysUtils.DeleteFile( sArchivoTemp );
                        {$IFDEF TRESS_REPORTES}
                        if oDatosImpresion.Copia <> VACIO then
                        begin
                             oArchivoCopia.AddStrings(oArchivoTemp);
                             oArchivoCopia.SaveToFile(sArchivoCopia);
                        end;
                        {$ENDIF}
                        SysUtils.DeleteFile( sArchivoTemp );
                     finally
                            FreeAndNil(oArchivoTemp);
                     end;
                end;
             finally
                    FreeAndNil(oArchivo);
                    {$IFDEF TRESS_REPORTES}
                    if oDatosImpresion.Copia <> VACIO then
                    begin
                         FreeAndNil(oArchivoCopia);
                    end;
                    {$ENDIF}
             end;
        end
        else
        begin
             SysUtils.RenameFile(sArchivoTemp, sArchivo );
             {$IFDEF TRESS_REPORTES}
             if oDatosImpresion.Copia <> VACIO then
             begin
                  CopyFile(PChar (sArchivo), PChar (sArchivoCopia), TRUE);
             end;
             {$ENDIF}
        end;
        {$endif}
     finally
            FAlias.Free;
            FDescrip.Free;
            FCodigos.Free;
            FCampos.Free;
            {$ifndef XMLREPORT}
            Screen.Cursor := oCursor;
            if lDialogoConfirmacion AND Result {$ifdef AUTOLIV} AND not lErrorEncriptado{$endif} then
               ZetaDialogo.ZInformation('Archivos Ascii', 'El Archivo: '+ oDatosImpresion.Exportacion + CR_LF + 'fue Exportado con Exito' , 0);
            {$endif}
     end;
end;

procedure TReporteAscii.AgregaGrupo( oGrupos : TStrings;
                                     const lEncabezado, lDelimitado : Boolean );
  var oGrupo : TGrupoOpciones;
      oLista : TStrings;
begin
     oLista := NIL;
     oGrupo := TGrupoOpciones( oGrupos.Objects[ 0 ] );//Nivel Empresa
     if ( oGrupo <> NIL ) then
     begin
          if lEncabezado then
          begin
               if oGrupo.Encabezado then
                  oLista := oGrupo.ListaEncabezado
          end
          else
          begin
               if oGrupo.PieGrupo then
                  oLista := oGrupo.ListaPie;
          end;

          if oLista <> NIL then
          begin
               if lDelimitado then
                  AgregaDelimitado( oLista, TRUE )
               else AgregaFijo( oLista, TRUE )
          end;
     end;
end;

function TReporteAscii.HayTitulos( oCampos : TStrings; const nCampo: integer = 0 ) : Boolean;
 var i : integer;
begin
     Result := FALSE;
     for i := nCampo+1 to oCampos.Count - 1 do
     begin
          with TCampoListado(oCampos.Objects[i]) do
               Result := ( Ancho > 0 ) and ( Titulo <> SINTITULO );
          if Result then
             Break;
     end;

end;

procedure TReporteAscii.AgregaTituloSoloTotales(oCampos : TStrings; const nCampo: integer = 0 );
 var i : integer;
begin
     with fAsciiFile do
     begin
          if HayTitulos( oCampos, nCampo ) then
          begin
               RenglonBegin;
               if ( ( FAnchoAscii-FAnchoAsciiDes) > 0 ) then
               begin
                    AgregaFijoString(' ',FAnchoAsciiDes);
                    AgregaFijoString('Area',FAnchoAscii-FAnchoAsciiDes);
               end;
               for i := nCampo+1 to oCampos.Count - 1 do
               begin
                    with TCampoListado(oCampos.Objects[i]) do
                    begin
                         if ( Calculado = K_RENGLON_NUEVO ) then
                         begin
                              BajaRenglon;
                         end
                         else
                         begin
                              if ( oCampos[ i ] <> SINTITULO ) and
                                 ( TCampoListado(oCampos.Objects[i]).Ancho > 0 ) then
                                 AgregaFijoString( oCampos[ i ], Ancho );
                         end;
                    end;
               end;
               RenglonEnd;
          end;
     end;
end;

procedure TReporteAscii.AgregaTitulo( oCampos : TStrings );
 var i : integer;
     sSeparador : string;
begin
     fAsciiFile.RenglonBegin;

     for i := 0 to oCampos.Count - 1 do
     begin
          with TCampoListado(oCampos.Objects[i]) do
          begin
               if ( Calculado = K_RENGLON_NUEVO ) then
               begin
                    BajaRenglon;
               end
               else
               begin
                    sSeparador := FAsciiFile.Separador;
                    if ( i > 0 ) AND
                       ( TCampoListado(oCampos.Objects[i-1]).Calculado = K_RENGLON_NUEVO ) then
                    begin
                         FAsciiFile.Separador := '';
                    end;

                    fAsciiFile.AgregaCampoString( oCampos[ i ] );
                    
                    FAsciiFile.Separador := sSeparador; 
               end;
          end;
     end;
     
     fAsciiFile.RenglonEnd;
end;

procedure TReporteAscii.BajaRenglon;
 var sSeparador : string;
begin
     sSeparador := fAsciiFile.Separador;
     try
        fAsciiFile.Separador := '';
        fAsciiFile.AddCampo(CR_LF);
     finally
            fAsciiFile.Separador := sSeparador;
     end;
end;

procedure TReporteAscii.AgregaDelimitado( oCampos : TStrings;
                                          const lTotal : Boolean );
 var i : integer;
     oCampo : TCampoListado;
     sTemp, sSeparador : string;
begin
     if oCampos.Count > 0 then
     begin
          fAsciiFile.RenglonBegin;
          for i:= 0 to oCampos.Count -1 do
          begin
               oCampo := TCampoListado(oCampos.Objects[i]);
               if (oCampo.Ancho > 0) then
               begin
                    if ( oCampo.Calculado = K_RENGLON_NUEVO ) then
                    begin
                         BajaRenglon;
                    end
                    else if oCampo.PosAgente >= 0 then
                    begin
                         sSeparador := FAsciiFile.Separador;
                         if ( i > 0 ) AND
                            ( TCampoListado(oCampos.Objects[i-1]).Calculado = K_RENGLON_NUEVO ) then
                         begin
                              FAsciiFile.Separador := '';
                         end;

                         case oCampo.TipoImp of
                              tgBooleano, tgTexto, tgMemo :
                              begin
                                   sTemp := oCampo.AsTextoTotal(FDataSet,FSoloTotales);
                                   if (fAsciiFile.Separador <> '|') then
                                   begin
                                        if (FTipoAscii = tfCSV) AND (Pos(fAsciiFile.Separador, sTemp)=0) then
                                           fAsciiFile.Delimitador := ''
                                        else fAsciiFile.Delimitador := '"';
                                   end;

                                   fAsciiFile.AgregaCampoString(sTemp);
                              end;
                              tgFloat, tgNumero, tgFecha : fAsciiFile.AddCampo( oCampo.AsTextoTotal(FDataSet,FSoloTotales));
                         end;

                         FAsciiFile.Separador := sSeparador;
                    end;
               end;
          end;
          fAsciiFile.RenglonEnd;
     end;
end;

function TReporteAscii.AgregaDatosTotales: Boolean;
 var sTemp : string;
     oCampo : TCampoListado;
     i: integer;
begin
     Result := True;
     if FAsciiSoloTotales AND (FCampos.Count>0) then
     begin
          oCampo := TCampoListado(FCampos.Objects[0]);

          if ( oCampo.Calculado <> K_RENGLON_NUEVO ) and
             ( oCampo.PosAgente >= 0 ) then
          begin
               sTemp := oCampo.AsTextoTotal(fDataSet,FSoloTotales);

               if StrVacio(sTemp) then
               begin
                    fAsciiFile.RenglonBegin;
                    Dec(FCountCorte);
                    for i:=0 to FCountCorte-1 do
                        sTemp := sTemp + FCodigos[i];
                    i:=FCountCorte;
                    if i=0 then
                       fAsciiFile.AgregaFijoString('- Totales de Empresa -',FAnchoAscii )
                    else
                        fAsciiFile.AgregaFijoString( sTemp +
                                                     Replicate('-',FAnchoAsciiDes-Length(sTemp)-1)+ ' '+FDescrip[i-1] + ' -', FAnchoAscii );
                    Result := FALSE;

               end
               else
               begin
                    if FCountCorte <> FGrupoCount then
                    begin
                         FCountCorte := FGrupoCount;
                         FGrupoTotales :='';
                         FDescrip.Clear;
                         FCodigos.Clear;
                         for i:= 0 to FCountCorte-1 do
                             with TCampoListado(FCampos.Objects[i]) do
                                  FCodigos.Add(PadR(AsTextoTotal(fDataSet,FSoloTotales),Ancho));
                         //FDescrip.Add('-- Totales de Empresa --');
                         for i:=0 to FAlias.Count -1 do
                             FDescrip.Add(fDataSet.FieldByName(FAlias[i]).AsString);
                    end;
               end;
          end;
     end;
end;

procedure TReporteAscii.AgregaFijo( oCampos : TStrings;
                                    const lTotal : Boolean;
                                    const nCampo: integer );
  var sTemp : string;
      i{,j} : integer;
      oCampo : TCampoListado;
begin
     if oCampos.Count > 0 then
     begin
          if nCampo = 0 then fAsciiFile.RenglonBegin;

          for i:= nCampo to oCampos.Count -1 do
          begin
               oCampo := TCampoListado(oCampos.Objects[i]);
               sTemp := '';

               if ( oCampo.Calculado = K_RENGLON_NUEVO ) then
               begin
                    BajaRenglon;
               end
               else if oCampo.PosAgente >= 0 then
               begin
                    sTemp := oCampo.AsTextoTotal(fDataSet,FSoloTotales);

                    if (oCampo.Ancho = -1)  then
                       fAsciiFile.AddCampo( sTemp )
                    else
                        case oCampo.TipoImp of
                             tgBooleano, tgTexto, tgMemo : fAsciiFile.AgregaFijoString( StrRight( sTemp, oCampo.Ancho ), oCampo.Ancho );
                             tgFloat, tgNumero :
                             begin
                                  if Length( sTemp ) > oCampo.Ancho then
                                     fAsciiFile.AddCampo( StrLeft( '**************', oCampo.Ancho) )
                                  else
                                      fAsciiFile.AddCampo( PadL( sTemp, oCampo.Ancho ) );
                             end;
                             tgFecha : fAsciiFile.AddCampo( StrLeft( PadL( sTemp, oCampo.Ancho ), oCampo.Ancho ) );
                        end;
               end
               else
               begin
                    with oCampo do
                         if (Formula = '') AND (Ancho > 0) then
                            fAsciiFile.AddCampo( PadL( sTemp, Ancho ) );
               end;
          end;
          fAsciiFile.RenglonEnd;
     end;
end;

end.
