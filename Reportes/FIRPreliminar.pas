unit FIRPreliminar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FImpresionRapida, StdCtrls, ExtCtrls;

type
  TFormaIRPreliminar = class(TForm)
    Panel1: TPanel;
    ScrollBox1: TScrollBox;
    MemoPreliminar: TMemo;
    btnSiguiente: TButton;
    btnPrimero: TButton;
    btnImprimir: TButton;
    btnSalir: TButton;
    Panel2: TPanel;
    lblPagina: TLabel;
    btnUltimo: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnSiguienteClick(Sender: TObject);
    procedure btnPrimeroClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure btnUltimoClick(Sender: TObject);
  private
    { Private declarations }
    FFastForm : TFastForm;
    procedure CentraMemo;
    procedure Primero;
    procedure Siguiente;
    procedure Ultimo;
    procedure CursorAlInicio;
    procedure MuestraPagina;
  public
    { Public declarations }
    property FastForm : TFastForm read FFastForm write FFastForm;
  end;

var
  FormaIRPreliminar: TFormaIRPreliminar;

implementation

{$R *.DFM}

procedure TFormaIRPreliminar.CentraMemo;
begin
    with FastForm do
    begin
        ScrollBox1.VertScrollBar.Position := 0;
        ScrollBox1.HorzScrollBar.Position := 0;
        MemoPreliminar.Left   := 0;
        MemoPreliminar.Top    := 0;
        MemoPreliminar.Width  := ( ColumnasForma + 1 ) * 7;
        MemoPreliminar.Height := LineasForma * 14 + 25;
        if ( MemoPreliminar.Width < ScrollBox1.ClientWidth ) then
            MemoPreliminar.Left   := ( ScrollBox1.ClientWidth - MemoPreliminar.Width ) div 2 - 1;
        if ( MemoPreliminar.Height < ScrollBox1.ClientHeight ) then
            MemoPreliminar.Top := ( ScrollBox1.ClientHeight - MemoPreliminar.Height ) div 2 - 1;
    end;
end;

procedure TFormaIRPreliminar.FormShow(Sender: TObject);
begin
    Primero;
end;

procedure TFormaIRPreliminar.btnUltimoClick(Sender: TObject);
begin
     Ultimo;
end;

procedure TFormaIRPreliminar.btnSiguienteClick(Sender: TObject);
begin
    Siguiente;
end;

procedure TFormaIRPreliminar.Primero;
begin
  FastForm.IniciaPreliminar;
  CentraMemo;
  CursorAlInicio;
  btnSiguiente.Enabled := TRUE;
  btnPrimero.Enabled   := FALSE;
  btnUltimo.Enabled := TRUE;
  lblPagina.Tag := 1;
  MuestraPagina;
end;

procedure TFormaIRPreliminar.Siguiente;
begin
    btnSiguiente.Enabled := FastForm.SiguientePreliminar;
    btnUltimo.Enabled := btnSiguiente.Enabled;
    btnPrimero.Enabled := TRUE;
    if btnSiguiente.Enabled then
    begin
         CursorAlInicio;
         lblPagina.Tag := lblPagina.Tag + 1;
         MuestraPagina;
    end;
end;

procedure TFormaIRPreliminar.Ultimo;
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        btnSiguiente.Enabled := FALSE;
        btnUltimo.Enabled := FALSE;
        btnPrimero.Enabled := TRUE;
        CursorAlInicio;
        lblPagina.Tag := FastForm.UltimoPreliminar;
        MuestraPagina;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TFormaIRPreliminar.btnPrimeroClick(Sender: TObject);
begin
     Primero;
end;

procedure TFormaIRPreliminar.FormResize(Sender: TObject);
begin
     CentraMemo;
end;

procedure TFormaIRPreliminar.CursorAlInicio;
begin
    with MemoPreliminar do
    begin
        SelStart := 1;
        SelLength := 1;
    end;
end;

procedure TFormaIRPreliminar.MuestraPagina;
begin
    lblPagina.Caption := IntToStr( lblPagina.Tag );
end;


end.
