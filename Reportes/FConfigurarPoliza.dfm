inherited ConfigurarPoliza: TConfigurarPoliza
  Left = 263
  Top = 141
  Width = 716
  Height = 396
  BorderIcons = [biSystemMenu, biMinimize, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Configurar P�liza Contable: '
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 326
    Width = 708
    inherited OK: TBitBtn
      Left = 540
      Hint = 'Grabar Esta P�liza'
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 625
      Hint = 'Cancelar Cambios y Salir'
    end
    object pbAgregar: TProgressBar
      Left = 7
      Top = 10
      Width = 221
      Height = 16
      Min = 0
      Max = 100
      TabOrder = 2
      Visible = False
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 708
    Height = 326
    ActivePage = Formulas
    Align = alClient
    TabOrder = 1
    object TabConceptos: TTabSheet
      Caption = 'Conceptos'
      ImageIndex = 2
      object gridConceptos: TDBGrid
        Left = 0
        Top = 33
        Width = 700
        Height = 265
        Align = alClient
        DataSource = dsCtaConceptos
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CONCEPTO'
            Title.Caption = 'Concepto'
            Width = 61
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DESCRIPCION'
            ReadOnly = True
            Title.Caption = 'Descripci�n'
            Width = 247
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPO'
            ReadOnly = True
            Title.Caption = 'Tipo'
            Width = 119
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ASIENTO'
            Title.Caption = 'Asiento'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SUBCUENTA'
            Title.Caption = 'SubCuenta'
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object btnLlenaConceptos: TBitBtn
          Left = 5
          Top = 4
          Width = 118
          Height = 25
          Hint = 'Importar Cat�logo de Conceptos de Tress'
          Caption = 'Importar de &Tress'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnLlenaConceptosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
            FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
            00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
            F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
            00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
            F033777777777337F73309999990FFF0033377777777FFF77333099999000000
            3333777777777777333333399033333333333337773333333333333903333333
            3333333773333333333333303333333333333337333333333333}
          NumGlyphs = 2
        end
        object dbnConceptos: TDBNavigator
          Left = 566
          Top = 5
          Width = 130
          Height = 25
          DataSource = dsCtaConceptos
          VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
          Anchors = [akTop, akRight]
          Flat = True
          Hints.Strings = (
            'Primero'
            'Anterior'
            'Siguiente'
            'Ultimo'
            'Agregar'
            'Borrar'
            'Cambiar'
            'Grabar'
            'Cancelar'
            'Refrescar')
          ParentShowHint = False
          ConfirmDelete = False
          ShowHint = True
          TabOrder = 3
        end
        object btnAbreConceptos: TBitBtn
          Left = 128
          Top = 4
          Width = 110
          Height = 25
          Hint = 'Importar Cat�logo De Conceptos De Un Archivo Externo'
          Caption = '&Importar Archivo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnAbreConceptosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
            B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
            B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
            0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
            55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
            55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
            55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
            5555575FFF755555555557000075555555555577775555555555}
          NumGlyphs = 2
        end
        object btnExportaConceptos: TBitBtn
          Left = 244
          Top = 4
          Width = 110
          Height = 25
          Hint = 'Exportar Estos Conceptos Hacia Un Archivo Externo'
          Caption = '&Exportar Archivo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnExportaConceptosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
            7700333333337777777733333333008088003333333377F73377333333330088
            88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
            000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
            FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
            99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
            99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
            99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
            93337FFFF7737777733300000033333333337777773333333333}
          NumGlyphs = 2
        end
      end
    end
    object TabGrupos: TTabSheet
      Caption = 'Grupos'
      ImageIndex = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 69
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object cbGrupoLBL: TLabel
          Left = 17
          Top = 12
          Width = 32
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grupo:'
        end
        object cbGrupo: TComboBox
          Left = 52
          Top = 8
          Width = 354
          Height = 21
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 0
          OnClick = cbGrupoClick
        end
        object btnImportaGrupo: TBitBtn
          Left = 52
          Top = 38
          Width = 115
          Height = 25
          Hint = 'Importar Criterios De Agrupaci�n Desde Tress'
          Caption = 'Importar de &Tress'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnImportaGrupoClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
            FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
            00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
            F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
            00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
            F033777777777337F73309999990FFF0033377777777FFF77333099999000000
            3333777777777777333333399033333333333337773333333333333903333333
            3333333773333333333333303333333333333337333333333333}
          NumGlyphs = 2
        end
        object dbnGrupo: TDBNavigator
          Left = 412
          Top = 6
          Width = 115
          Height = 25
          DataSource = dsGrupo
          VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
          Flat = True
          Hints.Strings = (
            'Primero'
            'Anterior'
            'Siguiente'
            'Ultimo'
            'Agregar'
            'Borrar'
            'Cambiar'
            'Grabar'
            'Cancelar'
            'Refrescar')
          ParentShowHint = False
          ConfirmDelete = False
          ShowHint = True
          TabOrder = 5
        end
        object btnAbreGrupo: TBitBtn
          Left = 172
          Top = 38
          Width = 115
          Height = 25
          Hint = 'Importar Criterios de Agrupaci�n Desde Archivo Externo'
          Caption = '&Importar Archivo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnAbreGrupoClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
            B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
            B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
            0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
            55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
            55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
            55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
            5555575FFF755555555557000075555555555577775555555555}
          NumGlyphs = 2
        end
        object btnExportaGrupo: TBitBtn
          Left = 292
          Top = 38
          Width = 115
          Height = 25
          Hint = 'Exportar Criterios De Agrupaci�n Hacia Archivo Externo'
          Caption = '&Exportar Archivo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btnExportaGrupoClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
            7700333333337777777733333333008088003333333377F73377333333330088
            88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
            000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
            FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
            99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
            99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
            99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
            93337FFFF7737777733300000033333333337777773333333333}
          NumGlyphs = 2
        end
        object btnBorraTodos: TBitBtn
          Left = 412
          Top = 38
          Width = 115
          Height = 25
          Hint = 'Borrar Estos Criterios De Agrupaci�n'
          Caption = '&Borrar Todos'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = btnBorraTodosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
        end
      end
      object gridGrupo: TDBGrid
        Left = 0
        Top = 69
        Width = 700
        Height = 229
        Align = alClient
        DataSource = dsGrupo
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C�digo'
            Width = 91
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descripcion'
            Title.Caption = 'Descripci�n'
            Width = 205
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SubCuenta'
            Width = 80
            Visible = True
          end>
      end
    end
    object TabAsientos: TTabSheet
      Caption = 'Asientos Contables'
      ImageIndex = 3
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object editCuentaLBL: TLabel
          Left = 17
          Top = 10
          Width = 114
          Height = 13
          Alignment = taRightJustify
          Caption = 'Estructura de la Cuenta:'
        end
        object Label1: TLabel
          Left = 471
          Top = 20
          Width = 124
          Height = 13
          Alignment = taRightJustify
          Caption = 'Estructura del Comentario:'
          Visible = False
        end
        object btnGenerar: TBitBtn
          Left = 338
          Top = 4
          Width = 120
          Height = 25
          Hint = 'Generar Los Asientos Contables De Esta P�liza'
          Caption = 'Generar Asientos'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnGenerarClick
          Glyph.Data = {
            66010000424D6601000000000000760000002800000014000000140000000100
            040000000000F000000000000000000000001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            8888888800008888888888888888888800008888888888888888888800008888
            8884444488888888000088888884FFF488888888000088888804444488888888
            00008888808888888888888800004444488444448844444800004FFF4004BBB4
            004FFF4800004444488444448844444800008888808888888888888800008888
            8804444488444448000088888884FFF4004FFF48000088888884444488444448
            0000888888888888088888880000888888888888804444480000888888888888
            884FFF4800008888888888888844444800008888888888888888888800008888
            88888888888888880000}
        end
        object editCuenta: TEdit
          Left = 134
          Top = 6
          Width = 185
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 0
          Text = '#CO-#G1-000'
        end
        object dbnPoliza: TDBNavigator
          Left = 563
          Top = 4
          Width = 130
          Height = 25
          DataSource = dsPoliza
          VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
          Anchors = [akTop, akRight]
          Flat = True
          Hints.Strings = (
            'Primero'
            'Anterior'
            'Siguiente'
            'Ultimo'
            'Agregar'
            'Borrar'
            'Cambiar'
            'Grabar'
            'Cancelar'
            'Refrescar')
          ParentShowHint = False
          ConfirmDelete = False
          ShowHint = True
          TabOrder = 2
        end
        object Edit1: TEdit
          Left = 510
          Top = 16
          Width = 185
          Height = 21
          CharCase = ecUpperCase
          TabOrder = 3
          Text = '#CO-#G1-000'
          Visible = False
        end
      end
      object gridPoliza: TDBGrid
        Left = 0
        Top = 33
        Width = 700
        Height = 265
        Align = alClient
        DataSource = dsPoliza
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CUENTA'
            Title.Caption = 'Cuenta'
            Width = 92
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONCEPTO'
            Title.Caption = 'Concepto'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO1'
            Title.Caption = 'Grupo 1'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO2'
            Title.Caption = 'Grupo 2'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO3'
            Title.Caption = 'Grupo 3'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO4'
            Title.Caption = 'Grupo 4'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GRUPO5'
            Title.Caption = 'Grupo 5'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIPO'
            Title.Caption = 'Asiento'
            Width = 45
            Visible = True
          end>
      end
    end
    object Formulas: TTabSheet
      Caption = 'F�rmulas de P�liza'
      ImageIndex = 4
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 700
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object PolizaLBL: TLabel
          Left = 6
          Top = 9
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'P�liza:'
        end
        object Poliza: TZetaTextBox
          Left = 40
          Top = 8
          Width = 251
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object btnBorrarTodos: TBitBtn
          Left = 413
          Top = 4
          Width = 96
          Height = 25
          Hint = 'Borrar Todos Los Asientos De Esta P�liza'
          Caption = 'Borrar Todos'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnBorrarTodosClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
        end
        object btnAgregar: TBitBtn
          Left = 296
          Top = 4
          Width = 114
          Height = 25
          Hint = 'Agregar Los Asientos Generados A La P�liza'
          Caption = 'Agregar Asientos'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnAgregarClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
        end
      end
      object gridCampoRep: TDBGrid
        Left = 0
        Top = 33
        Width = 700
        Height = 265
        Align = alClient
        DataSource = dsCampoRep
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CR_POSICIO'
            Title.Alignment = taCenter
            Title.Caption = '#'
            Width = 31
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_TITULO'
            Title.Caption = 'Cuenta'
            Width = 142
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_FORMULA'
            Title.Caption = 'F�rmula'
            Width = 298
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CR_OPER'
            Title.Caption = 'Asiento'
            Width = 40
            Visible = True
          end>
      end
    end
  end
  object OpenCDS: TOpenDialog
    DefaultExt = 'cds'
    Filter = 'Archivos Temporales (*.cds)|*.cds|Todos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Importar Archivo Temporal'
    Left = 148
    Top = 328
  end
  object SaveCDS: TSaveDialog
    DefaultExt = 'cds'
    Filter = 'Archivos Temporales (*.cds)|*.cds|Todos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Exportar Archivo Temporal'
    Left = 180
    Top = 329
  end
  object dsGrupo: TDataSource
    Left = 5
    Top = 328
  end
  object dsCtaConceptos: TDataSource
    Left = 37
    Top = 328
  end
  object dsPoliza: TDataSource
    Left = 69
    Top = 328
  end
  object dsCampoRep: TDataSource
    Left = 101
    Top = 328
  end
end
