object DatosImport: TDatosImport
  Left = 198
  Top = 108
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Importaci'#243'n de Reportes'
  ClientHeight = 260
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 77
    Width = 313
    Height = 140
    Caption = 'Reporte'
    TabOrder = 0
    object Label4: TLabel
      Left = 7
      Top = 64
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre Original:'
    end
    object Label5: TLabel
      Left = 61
      Top = 17
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object Label6: TLabel
      Left = 12
      Top = 33
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tabla Principal:'
    end
    object lbNombre: TLabel
      Left = 89
      Top = 64
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object lbTipo: TLabel
      Left = 89
      Top = 17
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object lbTabla: TLabel
      Left = 89
      Top = 33
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object Label12: TLabel
      Left = 10
      Top = 116
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre Nuevo:'
    end
    object Label2: TLabel
      Left = 46
      Top = 80
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Plantilla:'
    end
    object lbPlantilla: TLabel
      Left = 89
      Top = 80
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object Label3: TLabel
      Left = 23
      Top = 49
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Clasificacion:'
    end
    object lbClasificacion: TLabel
      Left = 89
      Top = 49
      Width = 67
      Height = 13
      Caption = 'lbClasificacion'
    end
    object eNombre: TEdit
      Left = 89
      Top = 112
      Width = 217
      Height = 21
      MaxLength = 30
      TabOrder = 0
      Text = 'eNombre'
    end
    object cbImportar: TCheckBox
      Left = 89
      Top = 94
      Width = 104
      Height = 17
      Caption = 'Importar Plantilla'
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 3
    Width = 313
    Height = 73
    Caption = 'Procedencia'
    TabOrder = 1
    object Label1: TLabel
      Left = 41
      Top = 18
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa:'
    end
    object lbEmpresa: TLabel
      Left = 89
      Top = 18
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object Label8: TLabel
      Left = 15
      Top = 34
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'Exportado Por:'
    end
    object Label9: TLabel
      Left = 18
      Top = 50
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha y Hora:'
    end
    object lbUsuario: TLabel
      Left = 89
      Top = 34
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object lbFecha: TLabel
      Left = 89
      Top = 50
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 224
    Width = 328
    Height = 36
    Align = alBottom
    TabOrder = 2
    object OK: TBitBtn
      Left = 168
      Top = 6
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 0
      Kind = bkOK
    end
    object Cancelar: TBitBtn
      Left = 248
      Top = 6
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
