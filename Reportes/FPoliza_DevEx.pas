unit FPoliza_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZetaEdit,
  ZetaFecha, CheckLst, StdCtrls, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls, Buttons,
  FPolizaBase_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  
  TressMorado2013, dxSkinsDefaultPainters, 
  dxSkinsdxBarPainter, dxBar, cxClasses, ImgList, ZetaKeyLookup_DevEx,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls,
  cxContainer, cxEdit, ZetaSmartLists_DevEx, cxTextEdit, cxMemo, cxListBox,
  cxPC, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, cxCheckListBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, ZetaCXGrid, dxBarBuiltInMenu;

type
  TPoliza_DevEx = class(TPolizaBase_DevEx)
    Label7: TLabel;
    lbNumCuentas: TLabel;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    CBCuentas: TCheckBox;
    CBBCuentas: TComboBox;
    ECuentas: TEdit;
    EFormula: TEdit;
    CBBFormula: TComboBox;
    CBFormula: TCheckBox;
    rgTipoCuenta: TRadioGroup;
    dxBarLargeButton1: TdxBarLargeButton;
    btnCreaPoliza: TdxBarLargeButton;
    procedure btnCreaPolizaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBCuentasClick(Sender: TObject);
    procedure CBFormulaClick(Sender: TObject);
    procedure CBBCuentasChange(Sender: TObject);
    procedure CBBFormulaChange(Sender: TObject);
    procedure EFormulaChange(Sender: TObject);
    procedure ECuentasChange(Sender: TObject);
    procedure rgTipoCuentaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure dsCuentasStateChange(Sender: TObject);
  private
    { Private declarations }
    FTipoCuenta : string;
    FFiltroCuenta : string;
    FFiltroFormula : string;

    procedure FiltraCampoRep;
    procedure CuentaCuentas;
    procedure FiltraPoliza;
    procedure FiltraTipoCuenta;
    function GetFiltro(const eFiltro: eFiltroPoliza; const sCampo: string; sFiltro: string): string;

  protected
    procedure EscribirCambios;override;

  public
    { Public declarations }
    procedure Connect;override;

  end;

var
  Poliza_DevEx: TPoliza_DevEx;

implementation

uses
    DReportes,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    FConfigurarPoliza_DevEx, ZetaCommonLists;


{$R *.dfm}


procedure TPoliza_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     btnCreaPoliza.Hint := 'Configurar Cuentas Automáticamente' + CR_LF +
                           '( Antes: Utilería CreaPóliza )  ';

end;

procedure TPoliza_DevEx.btnCreaPolizaClick(Sender: TObject);
begin
     inherited;
     { 10-Feb-2003:
       CV: Este boton manda llamar lo que inicialmente era la utileria CreaPoliza }

     if ( LBOrden.Items.Count > 0 ) then
     begin
          if ConfigurarPoliza_DevEx = NIL then
             ConfigurarPoliza_DevEx := TConfigurarPoliza_DevEx.Create( Self );

          with ConfigurarPoliza_DevEx do
          begin
               Grupos := LbOrden.Items;
               ShowModal;

               if ModalResult = mrOk then
               begin
                    Modo := dsEdit;
                    EscribirCambios;
               end
               else
                   dmReportes.CancelaCuentasPoliza;
          end;
     end
     else
     begin
          PageControl.ActivePage := tsOrden;
          ZError(Caption, 'La Póliza Debe de Tener por lo Menos un Criterio de Agrupación', 0 );
     end;
end;


procedure TPoliza_DevEx.Connect;
begin
     inherited;
     FTipoCuenta := '';
     FFiltroCuenta := '';
     FFiltroFormula := '';

     FiltraCampoRep;

     CBCuentas.Checked := FALSE;
     ECuentas.Text := '';
     CBBCuentas.ItemIndex := 0;
     CBFormula.Checked := FALSE;
     EFormula.Text := '';
     CBBFormula.ItemIndex := 0;

end;

procedure TPoliza_DevEx.FiltraTipoCuenta;
begin
     case rgTipoCuenta.ItemIndex of
          0: FTipoCuenta := '';
          1: FTipoCuenta := 'CR_OPER=0';
          2: FTipoCuenta := 'CR_OPER=1';
     end;
     FiltraCampoRep;
end;

procedure TPoliza_DevEx.FiltraCampoRep;
 var sFiltro : string;
begin
     sFiltro := Q_FILTRO;
     sFiltro := ConcatFiltros( sFiltro, FTipoCuenta );
     sFiltro := ConcatFiltros( sFiltro, FFiltroCuenta );
     sFiltro := ConcatFiltros( sFiltro, FFiltroFormula );

     with CampoRep do
     begin
          Filter := sFiltro;
          Filtered := TRUE;
     end;
     
     CuentaCuentas;
end;

procedure TPoliza_DevEx.CuentaCuentas;
begin
     if CampoRep.Active then
        lbNumCuentas.Caption := FormatFloat('0,0', CampoRep.RecordCount);
end;

procedure TPoliza_DevEx.FiltraPoliza;
begin
     if CBCuentas.Checked then
        FFiltroCuenta := GetFiltro(eFiltroPoliza(CBBCuentas.ItemIndex), 'CR_TITULO',ECuentas.Text)
     else FFiltroCuenta := '';

     if CBFormula.Checked then
        FFiltroFormula := GetFiltro(eFiltroPoliza(CBBFormula.ItemIndex), 'CR_FORMULA', EFormula.Text)
     else FFiltroFormula := '';
     
     FiltraCampoRep;
end;

procedure TPoliza_DevEx.CBCuentasClick(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza_DevEx.CBFormulaClick(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza_DevEx.CBBCuentasChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza_DevEx.CBBFormulaChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza_DevEx.EFormulaChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;

end;

procedure TPoliza_DevEx.ECuentasChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;


procedure TPoliza_DevEx.rgTipoCuentaClick(Sender: TObject);
begin
     inherited;
     FiltraTipoCuenta;
end;


procedure TPoliza_DevEx.EscribirCambios;
begin
     inherited;
     FiltraCampoRep;
end;

procedure TPoliza_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     rgTipoCuenta.ItemIndex := 0;

     //{$ifdef PRESUPUESTOS}
     //if ( Reporte.FieldByName('RE_TIPO').AsInteger = Ord( trPoliza ) ) then
          //ReadOnly := True;
     //{$ENDIF}     //Se elimina la valiadacion: Presupuestos tiene visibilidad de reportes tipo poliza

end;

function TPoliza_DevEx.GetFiltro( const eFiltro : eFiltroPoliza;
                            const sCampo: string; sFiltro : string): string;
begin
     Result := sCampo;
     case eFiltro of
          fpIgual: Result := Result + '='+EntreComillas( sFiltro );
          fpDiferente: Result := Result + '<>'+EntreComillas( sFiltro );
          fpEmpiezeCon: Result := Result + ' LIKE '+EntreComillas( sFiltro+'%');
          fpNOEmpiezeCon: Result := ' NOT (' + Result + ' LIKE '+EntreComillas( sFiltro+'%')+')';
          fpTermineEn: Result := Result + ' LIKE '+EntreComillas( '%' + sFiltro);
          fpNoTermineEn: Result := ' NOT (' + Result + ' LIKE '+EntreComillas('%' + sFiltro) + ')';
          fpContenga: Result := Result + ' LIKE '+EntreComillas( '%' +sFiltro+'%');
          fpNOContenga: Result := ' NOT (' + Result + ' LIKE '+EntreComillas( '%'+sFiltro+'%')+')';
          fpMayor: Result := Result + '> '+EntreComillas( sFiltro);
          fpMayorIgual: Result := Result + '>= '+EntreComillas( sFiltro);
          fpMenor: Result := Result + '< '+EntreComillas( sFiltro);
          fpMenorIgual: Result := Result + '<= '+EntreComillas( sFiltro);
          fpVacio: Result := Result + '='+EntreComillas('');
          fpNoVacio: Result := Result + '<>'+EntreComillas('');
     end;
end;
procedure TPoliza_DevEx.BorrarBtnClick(Sender: TObject);
begin
     inherited;
     CuentaCuentas;

end;

procedure TPoliza_DevEx.dsCuentasStateChange(Sender: TObject);
begin
     inherited;
     CuentaCuentas;
end;

end.
