unit FEditBaseReportes;

interface

{$INCLUDE DEFINES.INC}
{$ifdef TRESS}
 {$DEFINE MULTIPLES_ENTIDADES}
{$endif}
{$define CONFIDENCIALIDAD_MULTIPLE}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dbclient,  Buttons, StdCtrls, ExtCtrls,
  FBaseReportes,
  ZetaSmartLists, ZetaKeyLookup, ZetaEdit, ZetaFecha, CheckLst, Db,
  ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls, Grids, DBGrids,
  ZetaDBGrid,
  //(@am): CAMBIO TEMPORAL
  //TDMULTIP,  //(@am): Se necesita cambiar comp. para imagenes.
  FileCtrl,
  ZReportTools,
  ZReportToolsConsts,
  ZetaTipoEntidad,
  ZetaTipoEntidadTools,
  ZetaCommonLists,
  ZetaCommonClasses,
  {$ifdef RDD}
  {$else}
  DEntidadesTress,
  {$endif}
  ZAgenteSQLClient;

type
  TEditBaseReportes = class(TBaseReportes)
    tsParametro: TTabSheet;
    GroupBox13: TGroupBox;
    LBParametros: TZetaSmartListBox;
    BAgregaParametro: TBitBtn;
    BBorraParametro: TBitBtn;
    BArribaParametro: TZetaSmartListsButton;
    BAbajoParametro: TZetaSmartListsButton;
    GBPropiedadesParametro: TGroupBox;
    LTituloParametro: TLabel;
    lFormulaParametro: TLabel;
    ETituloParametro: TEdit;
    EFormulaParametro: TMemo;
    bFormulaParametro: TBitBtn;
    SmartListParametros: TZetaSmartLists;
    tsImpresora: TTabSheet;
    BPreeliminar: TSpeedButton;
    lbTablaPrincipal: TLabel;
    BTablaPrincipal: TSpeedButton;
    RE_ENTIDAD: TZetaDBTextBox;
    dsResultados: TDataSource;
    RGOrden: TRadioGroup;
    BAgregaFormulaOrden: TBitBtn;
    GroupBox3: TGroupBox;
    cbBitacora: TCheckBox;
    bDisenador: TSpeedButton;
    bbSuscripcion: TSpeedButton;
    tsSuscripciones: TTabSheet;
    lbSubordinados: TLabel;
    RE_IFECHA: TDBCheckBox;
    BtnAgregaUsuario: TBitBtn;
    BtnBorraUsuario: TBitBtn;
    GBPropiedadesSus: TGroupBox;
    LbFormatoRep: TLabel;
    LbDireccion: TLabel;
    GBPersonasSus: TGroupBox;
    LBPersonasSus: TZetaSmartListBox;
    US_EMAIL: TEdit;
    LbNoDatos: TLabel;
    lbFrecuencia: TLabel;
    SU_FRECUEN: TZetaKeyCombo;
    SU_VACIO: TZetaKeyCombo;
    SmartListSuscritos: TZetaSmartLists;
    US_FORMATO: TEdit;
    procedure FormKeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure RGOrdenClick(Sender: TObject);
    procedure SmartListParametrosAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ParametrosClick(Sender: TObject);
    procedure BBorraParametroClick(Sender: TObject);
    procedure BAgregaOrdenClick(Sender: TObject);
    procedure bFormulaParametroClick(Sender: TObject);
    procedure BAgregaParametroClick(Sender: TObject);
    procedure BTablaPrincipalClick(Sender: TObject);
    procedure BPreeliminarClick(Sender: TObject);
    procedure BImprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure eFormulaOrdenExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbSuscripcionClick(Sender: TObject);
    procedure BtnAgregaUsuarioClick(Sender: TObject);
    procedure BtnBorraUsuarioClick(Sender: TObject);
    procedure SmartListSuscritosAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure SuscripcionClick(Sender: TObject);
  private
    FExportaUser : String; //NOmbre del archivo a exportar, pero modificado por el usuario
    FCambioArchivo : Boolean; //Bandera para cambio en nombre archivos.
    {$ifdef RDD}
    {$else}
    dmEntidadesShell : TdmEntidadesTress;
    {$endif}
    FRelaciones : TStrings;
    FBitacora : TStrings;
    FClasificacionActiva : eClasifiReporte;
    FPuedeVerSuscrip: Boolean;

    procedure AgregaParametro;
    procedure CambiaTablaPrincipal;
    function EntidadValida(NuevaEntidad: TipoEntidad; oCampo: TCampoMaster;
      lCambiaTabla: Boolean): Boolean;
    function GetDisenaName: string;
    function ValidaPlantilla( var sPlantilla: string ): Boolean;
    procedure GetDatosSuscripcion ( oCampo : TSuscripCampoMaster );
    procedure AgregaSuscripcion(oUsuario : TSuscripCampoMaster);
    procedure AgregaSQLFiltrosEspeciales( const sFiltro: string;const lDeDiccion : Boolean = FALSE );
    function PuedeAgregarSuscripcion( iUsuario:Integer; const lAgregar: Boolean ): Boolean;
    function PuedeSuscribir( const lEsAutoSuscripcion, lEsOtrosUsuarios: Boolean ): Boolean;
    procedure EnabledSuscritos(const lEnable: Boolean);
    procedure EnabledSuscripControls( const lEnable: Boolean );
  protected
    fControl : TWinControl;
    SQLAgente : TSQLAgenteClient;
    FDatosImpresion : TDatosImpresion;
    FTipoPantalla : TipoPantalla;
    FFiltroFormula : string;
    FFiltroDescrip : string;
    FEntidadActual : TipoEntidad;
    FHayImagenes : Boolean;
    Procedure AbreDisenador( const sFileName: string );
    procedure AgregaListaParametro;override;
    procedure LimpiaListas;override;
    procedure LlenaListas;override;
    procedure PosicionaListas;override;
    procedure IniciaListas;override;
    procedure GrabaListas;override;
    procedure GrabaOrden;override;
    procedure GrabaParametros;
    procedure GrabaSuscripciones;
    procedure HabilitaControles;override;

    //LISTA ORDEN
    procedure ActualizaOrden(oOrden: TOrdenOpciones);override;
    procedure ActualizaOrdenObjeto;
    procedure AsignaProcOrden(oProc: TNotifyEvent);
    procedure AgregaFormulaOrden;
    procedure EnabledOrden(const bEnable, bFormula : Boolean );override;
    procedure EnabledGrupos(const bEnable, bFormula: Boolean);virtual;

    //LISTA PARAMETRO
    procedure ActualizaParametro(oParametro: TCampoMaster);
    procedure ActualizaParametroObjeto;
    procedure AsignaProcParametro(oProc: TNotifyEvent);
    procedure EnabledParametro(bEnable: Boolean);

    //LISTA SUSCRITOS
    procedure ActualizaSuscrito(oUsuario: TSuscripCampoMaster);
    procedure ActualizaSuscritoObjeto;
    procedure AsignaProcSuscrito(oProc: TNotifyEvent);

    //CAMBIO DE TABLA PRINCIPAL
    procedure AlBorrarDatos( const oEntidad : TipoEntidad );virtual;
    procedure AlRevisarDatos( const oEntidad : TipoEntidad );virtual;
    procedure DespuesDeBorrarDatos;virtual;
    procedure BorraDatos( oLista: TStrings;
                          const oEntidad: TipoEntidad;
                          const Inicio: integer);
    procedure Revisa( oLista: TStrings;
                      const oEntidad: TipoEntidad;
                      const sTipoCampo: string;
                      const Inicio: integer);
    function GetGruposStrings: TStrings;virtual;

    {GENERACION DE SQL}
    procedure AgregaSQLCampoRep;virtual;abstract;
    procedure AgregaSQLColumnas(oCampo: TCampoOpciones; const Banda : integer = -1);
    procedure AgregaSQLColumnasParam(oParam : TCampoMaster);
    procedure AgregaSQLFiltros(oFiltro: TFiltroOpciones);
    procedure AgregaSQLGrupos(oGrupo: TGrupoOpciones);
    procedure AgregaSQLOrdenes(oOrden: TOrdenOpciones);

    procedure AntesDeConstruyeSQL;virtual;abstract;
    procedure DespuesDeConstruyeSQL;virtual;
    procedure DespuesDeGeneraReporte;virtual;
    function GeneraReporte: Boolean;virtual;abstract;
    {Metodos para la impresion de Reportes}
    procedure ImpresionReporte( const Tipo : TipoPantalla );override;

    function GetFirstPestana: TTabSheet;override;

    procedure CreaEntidadesShell;
    procedure ExisteDirectorio(const sArchivo: string;oControl: TWinControl);
    procedure ParametrosMovimienLista(FParamList : TZetaParams);virtual;
    procedure ParametrosListadoNomina(FParamList : TZetaParams);virtual;
    procedure Connect;override;
    function ConstruyeFormula(var oMemo: TCustomMemo): Boolean;override;
    property ExportaUser : string read FExportaUser write FExportaUser;
    property CambioArchivo : boolean read FCambioArchivo write FCambioArchivo;
    function GeneraNombreArchivo(const sNombreArchivo:string): String;
  public
    procedure Preview;override;
    procedure Imprime;override;
    {GENERACION DE SQL}
    function GeneraSQL: Boolean;
  end;

var
  EditBaseReportes: TEditBaseReportes;

implementation

uses
     FTablaPrincipal,
     FDialogoPrinc,
     ZetaCommonTools,
     ZReportConst,
     ZetaDialogo,
     ZConstruyeFormula,
     ZFuncsCliente,
     DReportes,
     DCliente,
     DSistema,
     ZAccesosMgr,
     ZAccesosTress,
     DDiccionario,
     ZetaBusqueda,
     FTressShell,
     {$ifdef CAROLINA}
     DBaseReportes,
     {$endif}
     ZetaClientTools;

{$R *.DFM}

const K_PROPIEDAD_PARAM = 'Propiedades del Par�metro';

procedure TEditBaseReportes.AgregaListaParametro;
var  oParametro : TCampoMaster;
begin
     oParametro := TCampoMaster.Create;
     oParametro.TipoCampo := eTipoGlobal( CampoRep['CR_TFIELD'] );
     GetDatosGenerales( oParametro );
     LBParametros.Items.AddObject( oParametro.Titulo, oParametro );
end;

procedure TEditBaseReportes.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
     if ( (Shift = [ssCtrl]) OR (Shift  = [ssCtrl,ssShift]) ) then
     begin
          if ( Chr(Key) = 'P' ) OR ( Chr(Key) = 'I' ) then
          begin
               cbBitacora.Checked := cbBitacora.Checked OR (Shift = [ssCtrl,ssShift]);
               if ( Chr(Key) = 'P' ) then ImpresionReporte( tgPreview )
               else ImpresionReporte( tgImpresora ) ;
               Key := 0;
          end;
     end
end;


procedure TEditBaseReportes.PosicionaListas;
begin
     inherited;
     SmartListParametros.SelectEscogido( 0 );
     SmartListSuscritos.SelectEscogido( 0 );
end;

procedure TEditBaseReportes.LimpiaListas;
begin
     Inherited;
     LBParametros.Items.Clear;
     LBPersonasSus.Items.Clear;

     EnabledParametro( FALSE );
     EnabledSuscritos( FALSE );
end;

//MANEJO DE LISTA ORDEN
procedure TEditBaseReportes.ActualizaOrden( oOrden : TOrdenOpciones );
begin
     AsignaProcOrden( NIL );
     ETituloOrden.Text := oOrden.Titulo;
     EFormulaOrden.Text := oOrden.Formula;
     RGOrden.ItemIndex := oOrden.DireccionOrden;
     AsignaProcOrden( RGOrdenClick );
     EnabledOrden( TRUE, oOrden.Calculado<>0 );
end;

procedure TEditBaseReportes.AsignaProcOrden( oProc : TNotifyEvent );
begin
     RgOrden.OnClick := oProc;
end;

procedure TEditBaseReportes.EnabledOrden( const bEnable, bFormula : Boolean  );
begin
     inherited;
     //BBorraOrden.Enabled := bEnable;
     RGOrden.Enabled := bEnable AND NOT bFormula;
     //if NOT bEnable then EFormulaOrden.Text := '';
end;

procedure TEditBaseReportes.RGOrdenClick(Sender: TObject);
begin
     inherited;
     ActualizaOrdenObjeto;
end;

procedure TEditBaseReportes.ActualizaOrdenObjeto;
 var  Objeto : TOrdenOpciones;
begin
     with LBOrden, Items do
     begin
          Objeto := TOrdenOpciones( Objects[ ItemIndex ] );
          if Objeto <> NIL then
          begin
               Objeto.Titulo := eTituloOrden.Text;
               Items[ItemIndex] := Objeto.Titulo;
               Objeto.Formula := eFormulaOrden.Text;
               Objeto.DireccionOrden := RGOrden.ItemIndex;
               Modo := dsEdit;
          end;
     end;
end;

procedure TEditBaseReportes.GrabaOrden;
 var i: integer;
begin
     with LBOrden.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;
               GrabaCampoGeneral( TCampoMaster(Objects[i]),
                                  tcOrden, i, -1 );
               with TOrdenOpciones(Objects[i]) do
               begin
                    CampoRep.FieldByName('CR_CALC').AsInteger := DireccionOrden;
                    CampoRep.FieldByName('CR_COLOR').AsInteger := Calculado;
               end;
               CampoRep.Post;
          end;
end;

procedure TEditBaseReportes.GrabaParametros;
 var i: integer;
begin
     with LBParametros.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;
               GrabaCampoGeneral( TCampoMaster(Objects[i]),
                                  tcParametro, i, -1 );
               CampoRep.Post;
          end;
end;


procedure TEditBaseReportes.GrabaListas;
begin
     inherited;
     GrabaParametros;
     GrabaSuscripciones;
end;



//Lista de PARAMETROS
procedure TEditBaseReportes.SmartListParametrosAlSeleccionar(
  Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaParametro( TCampoMaster( Objeto ) );
          Texto := TCampoMaster( Objeto ).Titulo;
          BAgregaParametro.Enabled := LBParametros.Items.Count < 10;
     end
     else EnabledParametro( FALSE );
end;

procedure TEditBaseReportes.ActualizaParametro( oParametro : TCampoMaster );
begin
     AsignaProcParametro( NIL );
     GBPropiedadesParametro.Caption := K_PROPIEDAD_PARAM + ' #' + IntToStr( LBParametros.ItemIndex  + 1 );
     ETituloParametro.Text := oParametro.Titulo;
     EFormulaParametro.Text := oParametro.Formula;
     AsignaProcParametro( ParametrosClick );
     EnabledParametro( TRUE );
end;

procedure TEditBaseReportes.AsignaProcParametro( oProc : TNotifyEvent );
begin
     ETituloParametro.OnExit := oProc;
     EFormulaParametro.OnExit := oProc;
end;

procedure TEditBaseReportes.EnabledParametro( bEnable : Boolean );
begin
     ETituloParametro.Enabled := bEnable;
     lTituloParametro.Enabled := bEnable;
     EFormulaParametro.Enabled := bEnable;
     lFormulaParametro.Enabled := bEnable;
     bFormulaParametro.Enabled := bEnable;
     BBorraParametro.Enabled := bEnable;
     if NOT bEnable then
     begin
          GBPropiedadesParametro.Caption := K_PROPIEDAD_PARAM;
          ETituloParametro.Text := '';
          EFormulaParametro.Text := '';
     end;
end;


procedure TEditBaseReportes.ActualizaParametroObjeto;
 var Objeto : TCampoMaster;
     i : integer;
begin
     i := LBParametros.ItemIndex;
     Objeto := TCampoMaster( LBParametros.Items.Objects[ i ] );
     if Objeto <> NIL then
     begin
          Objeto.Titulo := ETituloParametro.Text;
          Objeto.Formula := EFormulaParametro.Text;
          Objeto.TipoCampo := tgAutomatico;
          LBParametros.Items[ i ] := Objeto.Titulo;
          Modo := dsEdit;
     end;
end;

procedure TEditBaseReportes.ParametrosClick(Sender: TObject);
begin
     inherited;
     ActualizaParametroObjeto
end;

procedure TEditBaseReportes.BBorraParametroClick(Sender: TObject);
begin
     inherited;
     BorraDato( SmartListParametros, bAgregaParametro, bBorraParametro, LBParametros );
     EnabledParametro(LBParametros.Items.Count > 0);
end;

procedure TEditBaseReportes.BAgregaOrdenClick(Sender: TObject);
begin
     with TBitBtn(Sender) do
     begin
          SetFocus;
          if Tag = 0 then AgregaOrden
          else AgregaFormulaOrden;
     end;
end;


procedure TEditBaseReportes.bFormulaParametroClick(Sender: TObject);
begin
     inherited;
     ConstruyeFormula( TCustomMemo(EFormulaParametro) );
     ActualizaParametroObjeto;
end;


procedure TEditBaseReportes.BAgregaParametroClick(Sender: TObject);
begin
     inherited;
     AgregaParametro;
end;

procedure TEditBaseReportes.AgregaParametro;
 var oParametro : TCampoMaster;
begin
     BAgregaParametro.SetFocus;
     oParametro := TCampoMaster.Create;
     oParametro.TipoCampo := tgAutomatico;
     oParametro.Titulo := K_PARAMETRO + IntToStr( LBParametros.Items.Count + 1 );
     DespuesDeAgregarDato( oParametro,
                           SmartListParametros,
                           BBorraParametro,
                           ETituloParametro );
     Modo := dsEdit;
end;

procedure TEditBaseReportes.BTablaPrincipalClick(Sender: TObject);
begin
     inherited;
     CambiaTablaPrincipal;
     SetLookupCondicion;
end;

procedure TEditBaseReportes.BorraDatos( oLista : TStrings;
                                        const oEntidad : TipoEntidad;
                                        const Inicio : integer );
 var i : integer;
begin
     i := Inicio;
     while i < oLista.Count do
     begin
          with oLista do
               if NOT EntidadValida( oEntidad, TCampoMaster( Objects[ i ] ), TRUE ) then
               begin
                    oLista.Delete( i );
                    i := Inicio;
               end
               else i := i + 1;
     end;
end;

procedure TEditBaseReportes.Revisa( oLista : TStrings;
                                    const oEntidad : TipoEntidad;
                                    const sTipoCampo : string;
                                    const Inicio : integer );
 var i : integer;
begin
     for i := Inicio to oLista.Count -1 do
     begin
          with oLista do
               if NOT EntidadValida( oEntidad, TCampoMaster( Objects[ i ]), FALSE ) then
                  fBitacora.Add( PadR( sTipoCampo + ' # ' + IntToStr(i) +
                                 ' : ' + TCampoMaster( Objects[ i ]).Titulo, 40 ) );
     end;
end;

procedure TEditBaseReportes.AlBorrarDatos(const oEntidad : TipoEntidad);
begin
     BorraDatos( LBOrden.Items, oEntidad, 0 );
     BorraDatos( LBFiltros.Items, oEntidad, 0 );
end;

procedure TEditBaseReportes.AlRevisarDatos(const oEntidad : TipoEntidad);
begin
     Revisa( LBOrden.Items, oEntidad, 'Orden', 0 );
     Revisa( LBFiltros.Items, oEntidad, 'Filtro', 0 );
end;

procedure TEditBaseReportes.CambiaTablaPrincipal;
 var iActual, iNueva : TipoEntidad;


 procedure CambiaTabla;
 begin
      {$ifdef RDD}
      if dmDiccionario.ExisteEntidadEnTablasPorClasificacion( iNueva ) then
      begin
      {$endif}
           AlBorrarDatos(iNueva);
           ZInformation( Caption,
                         'La Tabla Principal fue Cambiada de' + CR_LF +
                         dmReportes.ObtieneEntidad( iActual ) + ' a ' +
                         dmReportes.ObtieneEntidad( iNueva ),
                         0 );
           if (Modo = dsBrowse) or (Reporte.State = dsBrowse) then
           begin
                Modo := dsEdit;
                Reporte.Edit;
           end;

           {$ifdef RDD}
           Reporte.FieldByName('EN_NIVEL0').AsString := dmDiccionario.GetEsTablaNivel0;
           Reporte.FieldByName('EN_TABLA').AsString := dmDiccionario.GetNombreTabla( iNueva );
           {$endif}

           Reporte['RE_ENTIDAD'] := iNueva;
           {$ifdef RDD}
           Reporte['RE_TABLA'] := dmReportes.ObtieneEntidad( iNueva );
           {$endif}
           DespuesDeBorrarDatos;
      {$ifdef RDD}
      end
      else
          ZError( Caption, Format( 'Error al buscar la Tabla #%d.' + CR_LF + 'La Tabla Principal no pudo ser cambiada.' +CR_LF + 'El Reporte permanece sin cambios.' , [ Ord( iNueva ) ] ),0 );
      {$endif}
 end;
begin
     iNueva := Entidad;
     iActual := iNueva;
     if FRelaciones <> NIL then FRelaciones.Clear;
     ShowTablaPrincipal( iNueva, eClasifiReporte(Reporte['RE_CLASIFI']) );
     {$ifndef SELECCION}
     if dmCliente.ModoTress then
     {$endif}
     begin
        if (iNueva <> iActual) then
        begin
             if fBitacora = NIL then
                fBitacora := TStringList.Create
             else fBitacora.Clear;

             AlRevisarDatos(iNueva);

             if fBitacora.Count > 0 then
             begin
                  if ShowDialogoPrinc( fBitacora ) then
                     CambiaTabla;
             end
             else CambiaTabla;
        end;
     end;
end;


procedure TEditBaseReportes.CreaEntidadesShell;
begin
     {$ifdef RDD}
     {$else}
     if dmEntidadesShell = NIL then
        dmEntidadesShell := TdmEntidadesTress.Create( self );
     {$endif}
end;

function TEditBaseReportes.EntidadValida( NuevaEntidad : TipoEntidad;
                                          oCampo : TCampoMaster;
                                          lCambiaTabla : Boolean ) : Boolean;
 procedure GetRelaciones;
 begin
      {$ifdef RDD}
      {$else}
      if FRelaciones = NIL then
         FRelaciones := TStringList.Create;

      if FRelaciones.Count = 0 then
      begin
           with TStringList(fRelaciones) do
           begin
                Sorted := TRUE;
                Duplicates := dupIgnore;
           end;

           CreaEntidadesShell;

           with dmEntidadesShell do
                RelacionesEntidad( NuevaEntidad, TStringList(fRelaciones), TRUE );

      end;
      {$endif}
 end;

begin
     Result := (oCampo.Calculado < 0) OR (oCampo.Entidad = NuevaEntidad);

     if ( NOT Result ) AND (oCampo.Calculado = 0) then
     begin
          {$ifdef RDD}
          Result := dmDiccionario.EntidadValida(NuevaEntidad, oCampo.Entidad ) ;
          {$else}
          GetRelaciones;
          Result := FRelaciones.IndexOf( IntToStr(Ord(oCampo.Entidad)) )>=0;
          {$endif}
     end;
end;

procedure TEditBaseReportes.DespuesDeBorrarDatos;
begin
      EnabledOrden( NOT LbOrden.Items.Count = 0, FALSE );
      SmartListOrden.SelectEscogido( 0 );
      EnabledFiltro( NOT LbFiltros.Items.Count = 0 );
      SmartListFiltros.SelectEscogido( 0 );
end;

procedure TEditBaseReportes.IniciaListas;
begin
     inherited;
     LBParametros.OnDblClick := LbListaDblClick;
     LBPersonasSus.OnDblClick := LbListaDblClick;
end;

procedure TEditBaseReportes.BPreeliminarClick(Sender: TObject);
{$IFDEF PREVIEW_100}
 //var i: integer;
{$ENDIF}
begin
     inherited;
     {$IFDEF PREVIEW_100}
     while TRUE do
     {$ENDIF}
           ImpresionReporte( tgPreview );
end;

procedure TEditBaseReportes.BImprimirClick(Sender: TObject);
begin
     inherited;
     ImpresionReporte( tgImpresora );
end;


{*************************************************}
{*************** GENERACION DE SQL ***************}
{*************************************************}
{function TEditBaseReportes.EvaluaParametros( var sError : wideString;
                                             var oParams : OleVariant;
                                             const lMuestraDialogo : Boolean ) : Boolean;
 var i, iCount: integer;
     oParam : OleVariant;
     oColumna : TSQLColumna;
begin
     Result := TRUE;
     iCount := LBParametros.Items.Count;
     oParams := VarArrayCreate([1, K_MAX_PARAM ], varVariant);

     if iCount > 0 then
     begin
          for i:=0 to iCount-1 do
              AgregaSQLColumnasParam(TCampoMaster(LBParametros.Items.Objects[i]));

          Result := dmReportes.EvaluaParams( SQLAgente, sError );

          if Result then
          begin
               for i := 0 to LBParametros.Items.Count -1do
               begin
                   with TCampoMaster(LBParametros.Items.Objects[i]) do
                        if (PosAgente >= 0) then
                        begin
                             oColumna := SQLAgente.GetColumna( PosAgente );
                             oParam := VarArrayCreate([1, 3], varVariant);
                             oParam[1] := Titulo;
                             oParam[2] := oColumna.Formula;
                             oParam[3] := oColumna.TipoFormula;
                        end
                        else
                        begin
                             oParam := VarArrayCreate([1, 3], varVariant);
                             oParam[1] := Titulo;
                             oParam[2] := Formula;
                             oParam[3] := TipoCampo;
                        end;
                   oParams[i+1] := oParam;
               end;

               if lMuestraDialogo then
                  Result := FParametros.MuestraParametros( oParams, lBParametros.Items.Count );
          end;
     end;
end;
}
function TEditBaseReportes.GeneraSQL : Boolean;
 var sError : WideString;
     oParams : OleVariant;
     FParamList : TZetaParams;

 function GetCampoCB_Codigo( const iEntidad: TipoEntidad ) : string;
  var
     sTabla : string;
 begin
      //Los proyectos de Seleccion y Visitantes no deben de llegar a este punto.
      {$ifndef RDD}
      sTabla := dmDiccionario.GetNombreTabla( iEntidad );
      {$else}
      sTabla := Reporte.FieldByName('EN_TABLA').AsString ;
      {$endif}
      {$ifdef TRESS}
      case iEntidad of
           enEmbarazo,enMedEntregada,enConsulta,enAccidente : Result := 'or EXPEDIEN.CB_CODIGO = 0';
           enPoll: Result := ' or '+ sTabla + '.PO_NUMERO = 0';
           enInvitacion: Result := 'or INVITA.CB_CODIGO = 0';
           enRotacion,enDemografica  : Result := VACIO;
           else
               Result := ' or '+ sTabla + '.CB_CODIGO = 0';
      end;
      {$endif}
 end;
     {$ifdef TRESS}
var     
     i: integer;
     {$endif}
begin
     SQLAgente.Clear;
     FFiltroFormula :=  '';
     FFiltroDescrip := '';
     FHayImagenes := FALSE;

     FParamList := TZetaParams.Create;
     try
        Result := dmReportes.EvaluaParametros( SqlAgente, LbParametros.Items, sError, oParams, TRUE );
        if Result then
        begin
             dsResultados.DataSet.Active := FALSE;
             SQLAgente.Clear;
             SQLAgente.Entidad := TipoEntidad(Reporte.FieldByName('RE_ENTIDAD').AsInteger);
             SQLAgente.Parametros := oParams;

             if (FFiltroEspecial = '') then
             begin
                  if Trim(QU_CODIGO.LLave) > '' then
                  begin
                       with QU_CODIGO.LookUpDataset do
                       begin
                            if QU_CODIGO.LLave = FieldByName('QU_CODIGO').AsString then
                               AgregaSQLFiltrosEspeciales(FieldByName('QU_FILTRO').AsString);
                       end;
                  end;
                  AgregaSQLFiltrosEspeciales(Reporte.FieldByName('RE_FILTRO').AsString);

                  {$ifdef ANTES}
                  if Strlleno(dmCliente.Confidencialidad) and
                  begin
                       {$ifdef MULTIPLES_ENTIDADES}
                       Dentro( Entidad , EntidadConCondiciones ) then
                       {$else}
                       (Entidad in EntidadConCondiciones) then
                       {$endif}
                       AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_NIVEL0=''%s''',[dmCliente.Confidencialidad]),
                                                   FALSE );
                  end;
                  {$else}
                  if Strlleno(dmCliente.Confidencialidad) then
                  begin
                       {$ifdef MULTIPLES_ENTIDADES}
                               if Dentro( Entidad , EntidadConCondiciones )
                         {$IFDEF RDD}
                               or zStrToBool( Reporte.FieldByName('EN_NIVEL0').AsString )
                         {$ENDIF}
                               then
                       {$else}
                              if (Entidad in EntidadConCondiciones) then
                       {$endif}
                       {$ifdef CONFIDENCIALIDAD_MULTIPLE}
                        begin
                        AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_CODIGO > 0 %s ',[GetCampoCB_Codigo(Entidad) ]), FALSE );
                        AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_NIVEL0 in %s %s ',[dmCliente.ConfidencialidadListaIN, GetCampoCB_Codigo(Entidad) ]), TRUE );
                        end;
                       {$else}
                       AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_NIVEL0=''%s'' %s ',[dmCliente.Confidencialidad, GetCampoCB_Codigo(Entidad) ]),
                                                   FALSE );
                       {$endif}
                  end;
                  {$endif}
                  AgregaSQLCampoRep;
                  {$ifdef TRESS}
                  if dmCliente.ModoSuper
                     {a futuro PENDIENTECARO:
                     OR ( Reporte.FieldByName('RE_IFECHA').AsInteger = 1 )} then
                       {$ifdef MULTIPLES_ENTIDADES}
                       if Dentro( SQLAgente.Entidad , EntidadConCondiciones )
                         {$IFDEF RDD}
                               or zStrToBool( Reporte.FieldByName('EN_NIVEL0').AsString )
                         {$ENDIF}
                               then  
                       {$else}
                       if SQLAgente.Entidad in EntidadConCondiciones then
                       {$endif}
                       begin
                            SQLAgente.AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 30, 'EMPLEADO_SUPERVISOR' );
                            AgregaSQLFiltrosEspeciales( dmCliente.GetListaEmpleados('COLABORA.CB_CODIGO'),
                                                        TRUE );

                       end;

                  //Para los recibos mensuales se requiere saber la lista de meses y de a�os que se esta solicitando.
                  if (SQLAgente.Entidad = enMovimienBalanzaMensual) then
                  begin
                       //Si no existe el MES, ListaMesesTodos debe de quedar como falso
                       FParamList.AddBoolean( 'ListaMesesTodos', FALSE );

                       for i:=0 to LBFiltros.Items.Count -1 do
                       begin
                            if ( Pos( 'PE_MES',  TFiltroOpciones(LBFiltros.Items.Objects[i]).Formula ) <> 0 ) then
                            begin
                                 FParamList.AddString( 'ListaMeses', TFiltroOpciones(LBFiltros.Items.Objects[i]).Lista );
                                 FParamList.AddBoolean( 'ListaMesesTodos', TFiltroOpciones(LBFiltros.Items.Objects[i]).TipoRangoActivo = raTodos );
                            end;
                            if ( Pos( 'PE_YEAR',  TFiltroOpciones(LBFiltros.Items.Objects[i]).Formula ) <> 0 ) then
                               FParamList.AddString( 'ListaYears', TFiltroOpciones(LBFiltros.Items.Objects[i]).Lista );
                       end;
                  end;

                  {$endif}
             end
             else
             begin
                  {$ifdef ADUANAS}
                  AgregaSQLCampoRep;
                  {$endif}
                  {$ifdef CAJAAHORRO}
                  AgregaSQLCampoRep;
                  {$endif}
                  AgregaSQLFiltrosEspeciales( FFiltroEspecial, TRUE );
             end;

             FParamList.AddBoolean('GeneraBitacora', cbBitacora.Checked);
             AntesDeConstruyeSQL;
             FParamList.AddBoolean('ContieneImagenes', fHayImagenes);

             {Los Reportes Especiales, podran agregar
             parametros especiales en este metodo}

             {$ifdef TRESS}
             ZReportTools.ParametrosEspeciales( TipoEntidad(Reporte.FieldByName('RE_ENTIDAD').AsInteger),
                                                fParamList,
                                                LBFiltros.Items,
                                                GetGruposStrings );
             {$endif}

             Result := dmReportes.ConstruyeSQL( SQLAgente, sError, FFiltroFormula, FFiltroDescrip, FParamList, FTipoPantalla,
                                                cbBitacora.Checked, FHayImagenes);
             if Result then
             begin
                  ZFuncsCliente.RegistraFuncionesCliente;
                  //Asignacion de Parametros del Reporte,
                  //para poder evaluar la funcion PARAM() en el Cliente;
                  ZFuncsCliente.ParametrosReporte := SQLAgente.Parametros;
                  DespuesDeConstruyeSQL;

                  {$ifdef CAROLINA}
                  dmReportes.cdsResultados.SaveTofile('c:\temp\Resultados.cds');
                  {$endif}
             end
             else
             begin
                  if (StrVacio(sError) AND dsResultados.DataSet.IsEmpty) then
                     sError := K_NoHayRegistros;

                  ZetaDialogo.ZError(Caption,sError,0);
                  if Pos({$IFDEF TRESS_DELPHIXE5_UP}WideString({$ENDIF}K_NoHayRegistros{$IFDEF TRESS_DELPHIXE5_UP}){$ENDIF}, sError ) > 0 then
                     PageControl.ActivePage := tsFiltros;

             end;
        end
        else
        begin
             if sError > '' then
                ZetaDialogo.ZError(Caption,sError,0)
        end;

     finally
            FParamList.Free;
     end;
end;

procedure TEditBaseReportes.ImpresionReporte( const Tipo : TipoPantalla );
 var oCursor : TCursor;
begin
     fControl := ActiveControl;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FEntidadActual := Entidad;
        FTipoPantalla := Tipo;
        if Visible then Cancelar.SetFocus;
        if GeneraSQL then
        begin
              {***(@am): Anteriormente deshabilitar y habilitar el dataset desde este metodo no afectaba pues el control MULTIIMAGE
                          no necesitaba a notificacion para mostrar la imagen o el metodo disableControls en realidad no lo deshabilitaba.
                          Con el nuevo componente, necesitamos habilitar los controles antes de mostrar el PREVIEW, por lo que se movieron
                          estas dos lieneas al metodo GeneraForma de la unidad ZQrReporte.***}
             //dsResultados.DataSet.DisableControls;
             GeneraReporte;
             //dsResultados.DataSet.EnableControls;
        end;
     finally
            DespuesDeGeneraReporte;
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditBaseReportes.AgregaSQLColumnasParam(oParam : TCampoMaster);
begin
     ZReportTools.AgregaSQLColumnasParam( SQLAgente, oParam );
end;

procedure TEditBaseReportes.AgregaSQLColumnas( oCampo : TCampoOpciones; const Banda : integer );
begin
     ZReportTools.AgregaSQLColumnas( SQLAgente, oCampo, FDatosImpresion, Banda, LbParametros.Items.Count );
end;

procedure TEditBaseReportes.AgregaSQLFiltrosEspeciales(const sFiltro : string; const lDeDiccion : Boolean = FALSE);
begin
     ZReportTools.AgregaSQLFiltrosEspeciales( SQLAgente, sFiltro, lDeDiccion, LbParametros.Items.Count );
end;

procedure TEditBaseReportes.AgregaSQLFiltros( oFiltro : TFiltroOpciones );
begin
     ZReportTools.AgregaSQLFiltros( SQLAgente, oFiltro, FFiltroFormula, FFiltroDescrip );
end;


procedure TEditBaseReportes.AgregaSQLGrupos(oGrupo: TGrupoOpciones);
begin
     ZReportTools.AgregaSQLGrupos( SQLAgente, oGrupo, LbParametros.Items.Count );
end;

procedure TEditBaseReportes.AgregaSQLOrdenes(oOrden: TOrdenOpciones);
begin
     ZReportTools.AgregaSQLOrdenes( SQLAgente, oOrden, LbParametros.Items.Count );
end;

procedure TEditBaseReportes.FormCreate(Sender: TObject);
begin
     FPuedeVerSuscrip:= FALSE;
     tsSuscripciones.TabVisible := FPuedeVerSuscrip;
     bbSuscripcion.Visible:= FPuedeVerSuscrip;
     inherited;
     dsResultados.DataSet := dmReportes.cdsResultados;
     SQLAgente := TSQLAgenteClient.Create;
end;

procedure TEditBaseReportes.FormDestroy(Sender: TObject);
begin
     with LBParametros do
          while Items.Count > 0 do
          begin
                TCampoMaster(Items.Objects[0]).Free;
                Items.Delete(0);
          end;

      with  LBPersonasSus do
           while Items.Count > 0 do
           begin
                TSuscripCampoMaster(Items.Objects[0]).Free;
                Items.Delete(0);
           end;

     SQLAgente.Free;
     fBitacora.Free;
     FRelaciones.Free;
     FRelaciones := NIL;

     {$ifdef RDD}
     {$else}
     dmEntidadesShell.Free;
     dmEntidadesShell := NIL;
     {$endif}

     inherited;
end;

procedure TEditBaseReportes.Connect;
begin
     inherited;
     //dmReportes.cdsReportes.Conectar;
     dsResultados.DataSet := dmReportes.cdsResultados;
end;

procedure TEditBaseReportes.DespuesDeConstruyeSQL;
begin
end;

procedure TEditBaseReportes.Preview;
begin
     DataSource.DataSet := dmReportes.cdsEditReporte;
     LlenaListas;
     ImpresionReporte(tgPreview);
end;

procedure TEditBaseReportes.Imprime;
begin
     DataSource.DataSet := dmReportes.cdsEditReporte;
     LlenaListas;
     ImpresionReporte(tgImpresora);
end;


function TEditBaseReportes.GetFirstPestana: TTabSheet;
 var i: integer;
begin
     Result := NIL;
     BGuardarComo.Enabled := NOT SoloImpresion;
     BDisenador.Visible := NOT SoloImpresion;
     with PageControl do
          if SoloImpresion then
          begin
               for i:= 0 to PageControl.PageCount -1 do
                   if (Pages[i] <> tsOrden) and
                      (Pages[i].Name <> 'tsGrupos') and
                      (Pages[i] <> tsFiltros) and
                      (Pages[i] <> tsImpresora) then
                   begin
                        Pages[i].TabVisible := FALSE;
                   end;
               Result := tsFiltros;

          end
          else
          begin
               for i:= 0 to PageControl.PageCount -1 do
                   Pages[i].TabVisible := TRUE;
          end;
end;

procedure TEditBaseReportes.ExisteDirectorio( const sArchivo : string; oControl : TWinControl );
begin
     if NOT DirectoryExists(ExtractFileDir(sArchivo)) then
        Error( tsImpresora, oControl, 'El Directorio : '+ ExtractFileDir(sArchivo) +' No Existe' );
end;

procedure TEditBaseReportes.ParametrosListadoNomina(FParamList : TZetaParams);
begin
end;

procedure TEditBaseReportes.ParametrosMovimienLista(FParamList : TZetaParams);
begin
end;

procedure TEditBaseReportes.DespuesDeGeneraReporte;
begin

end;

procedure TEditBaseReportes.eFormulaOrdenExit(Sender: TObject);
begin
     inherited;
     ActualizaOrdenObjeto;
end;

procedure TEditBaseReportes.EnabledGrupos(const bEnable,
  bFormula: Boolean);
begin

end;

procedure TEditBaseReportes.AgregaFormulaOrden;
 var oFormula : TOrdenOpciones;
begin
     BAgregaFormulaOrden.SetFocus;
     oFormula := TOrdenOpciones.Create;
     with oFormula do
     begin
         Entidad := enFormula;
         Formula := '';
         Titulo := K_TITULO;
         Calculado := -1;
     end;
     with LBOrden do
     begin
          Items.AddObject( oFormula.Titulo, oFormula );
          SmartListOrden.SelectEscogido( Items.Count - 1 );
          Modo := dsEdit;
     end;
     ETituloOrden.SetFocus;
     ETituloOrden.SelectAll;
end;


function TEditBaseReportes.ConstruyeFormula( var oMemo : TCustomMemo ) : Boolean;
 var sFormula : string;
begin
     sFormula := oMemo.Text;
     Result := ZConstruyeFormula.GetFormulaConstruyeParams( Entidad, sFormula, oMemo.SelStart, evReporte, LbParametros.Items );
     if Result then
     begin
          Reporte.Edit;
          Modo := dsEdit;
          oMemo.Text := sFormula;
     end;
end;

function TEditBaseReportes.GetGruposStrings: TStrings;
begin
     Result := NIL;
end;


procedure TEditBaseReportes.FormShow(Sender: TObject);
begin
     inherited;
     FPuedeVerSuscrip:= ( not dmCliente.ModoSuper ) and ( not ReadOnly );
     FExportaUser := VACIO;
     FCambioArchivo := False;

     FClasificacionActiva := eClasifiReporte( dmReportes.cdsEditReporte.FieldByName( 'RE_CLASIFI' ).AsInteger );
     bbSuscripcion.Visible:= FPuedeVerSuscrip and PuedeSuscribir( TRUE, FALSE );
     tsSuscripciones.TabVisible:= FPuedeVerSuscrip and PuedeSuscribir( FALSE, TRUE );
     HabilitaControles;
end;

function TeditBaseReportes.GetDisenaName: string;
begin
{$ifdef ADUANAS}
        Result := 'TressIMXDisena.exe';
{$else}
       {$ifdef SELECCION}
               Result := 'SeleccionDisena.exe';
       {$else}
              {$ifdef VISITANTES}
                      Result := 'VisitantesDisena.exe';
              {$else}
                     Result := 'TressDisena.exe';
              {$endif}
       {$endif}
{$endif}
end;

function TEditBaseReportes.ValidaPlantilla( var sPlantilla: string ): Boolean;
begin
    if StrVacio(sPlantilla) then
       sPlantilla := ZReportTools.DirPlantilla + 'DEFAULT.QR2'
    else
    begin
         if ExtractFileExt( sPlantilla ) = '' then
            sPlantilla := sPlantilla + '.QR2';
         if ExtractFilePath( sPlantilla ) = '' then
            sPlantilla := ZReportTools.DirPlantilla + sPlantilla;
    end;
    Result := FileExists(sPlantilla);
    if NOT Result then
    begin
         Result := ZetaDialogo.ZConfirm( Caption, Format( 'La plantilla %s no existe.' + CR_LF +
                  ' � Desea abrir el dise�ador de formas para crearla ?' , [sPlantilla] ), 0, mbYes );
         sPlantilla := VACIO;
    end;
end;

procedure TEditBaseReportes.AbreDisenador( const sFileName: string );
const
     K_PATH_DISENA = 'EMPRESA=%s USUARIO=%d PASS=%s PLANTILLA="%s"';
var
   sPlantilla, sAppPath, sDisenaName, sParametros: string;
begin
     sPlantilla:= sFileName;
     if ValidaPlantilla(sPlantilla) then
     begin
          sAppPath := VerificaDir( ExtractFilePath(Application.ExeName) );
          sDisenaName := sAppPath + GetDisenaName;
          if FileExists( sDisenaName ) then
          begin
               sParametros := Format( K_PATH_DISENA, [dmCliente.Compania, dmCliente.Usuario, dmCliente.PasswordUsuario, sPlantilla ] );
               //sParametros := Format( K_PATH_DISENA, [dmCliente.Compania, dmCliente.Usuario, dmCliente.PasswordUsuario, 'C:\Progra~1\default.qr2' ] );
               ExecuteFile( sDisenaName, sParametros, '', SW_SHOWDEFAULT );
          end
          else
          begin
               ZetaDialogo.ZError(Caption, Format( 'No se encontr� el Dise�ador de Formas en "%s"', [sDisenaName] ), 0 )
          end
     end;
end;

 	

function TEditBaseReportes.GeneraNombreArchivo(const sNombreArchivo:string ): String;
var  sError : WideString;
     oParams : OleVariant;
begin
     SqlAgente.Clear;
     dmReportes.EvaluaParametros( SqlAgente, LbParametros.Items, sError, oParams, TRUE );
     if ( StrVacio( sError ) )then
     begin
          Result:= TransParamNomConfig( sNombreArchivo, LbParametros.Count, oParams, TRUE, FALSE );
     end
     else
     begin
          ZetaDialogo.zError('� Error !', sError, 0 );
          Result:= VACIO;
     end;
end;

procedure TEditBaseReportes.bbSuscripcionClick(Sender: TObject);
var
   oUsuario: TSuscripCampoMaster;
begin
     inherited;
      dmSistema.cdsSuscrip.DisableControls;
      try
         if ( dmReportes.AgregaSuscripciones( Reporte ) ) then
         begin
              ZetaDialogo.ZInformation( 'Explorador de Reportes',
                              Format( 'Reporte "%s" Agregado a Mis Suscripciones', [ Reporte.FieldByName( 'RE_NOMBRE' ).AsString ] ),
                              0 );
              if PuedeAgregarSuscripcion(dmCliente.Usuario,TRUE) then
              begin
                   oUsuario:= TSuscripCampoMaster.Create;
                   AgregaSuscripcion(oUsuario);
                   RefrescarStatusBar;
              end;
        end;
     finally
            dmSistema.cdsSuscrip.EnableControls;
     end;
end;

//LISTA SUSCRITOS

procedure TEditBaseReportes.SmartListSuscritosAlSeleccionar( Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaSuscrito( TSuscripCampoMaster( Objeto ) );
          Texto := TSuscripCampoMaster( Objeto ).Titulo;
     end
     else
         EnabledSuscritos(FALSE);
end;

procedure TEditBaseReportes.EnabledSuscritos(const lEnable: Boolean);
const
   K_VACIO_COMBO = -1;
begin
     if tsSuscripciones.TabVisible then
     begin
          SU_FRECUEN.Enabled:= lEnable;
          SU_VACIO.Enabled:= lEnable;
          lbFrecuencia.Enabled:= lEnable;
          lbNoDatos.Enabled:= lEnable;
          BtnBorraUsuario.Enabled:= lEnable;

          if not lEnable then
          begin
               SU_FRECUEN.ItemIndex:= K_VACIO_COMBO;
               SU_VACIO.ItemIndex:= K_VACIO_COMBO;
               US_EMAIL.Text:= VACIO;
               US_FORMATO.Text:= VACIO;
          end;
     end;
end;

procedure TEditBaseReportes.EnabledSuscripControls( const lEnable: Boolean );
var
   i: Integer;
begin
     if tsSuscripciones.TabVisible then
     begin
          for i:= 0 to tsSuscripciones.ControlCount - 1 do
          begin
               tsSuscripciones.Controls[i].Enabled:= lEnable;
          end;
     end;
     if bbSuscripcion.Visible then
        bbSuscripcion.Enabled:= ( lEnable and PuedeSuscribir( TRUE, FALSE ) );
     EnabledSuscritos( ( lEnable ) and ( LBPersonasSus.Items.Count > 0 ) );
end;

procedure TEditBaseReportes.BtnAgregaUsuarioClick(Sender: TObject);
var
   oUsuario: TSuscripCampoMaster;
   sKey,sDescripcion: String;
begin
     inherited;
     with dmSistema.cdsUsuarios do
     begin
          BtnAgregaUsuario.SetFocus;
          sKey:= LookupKeyField;
          sDescripcion:=  LookupDescriptionField;
          if( ZetaBusqueda.ShowSearchForm(dmSistema.cdsUsuarios, VACIO, sKey, sDescripcion ) ) then
          begin
               if( PuedeAgregarSuscripcion(FieldByName('US_CODIGO').AsInteger,TRUE) ) then
               begin
                    oUsuario := TSuscripCampoMaster.Create;
                    oUsuario.Usuario:= FieldByName('US_CODIGO').AsInteger;
                    oUsuario.Titulo := FieldByName('US_NOMBRE').AsString;
                    oUsuario.Frecuencia:= eEmailFrecuencia(dmSistema.UltimaFrecuencia);
                    oUsuario.HayDatos:= enEnviar;
                    oUsuario.Direccion:= FieldByName('US_EMAIL').AsString;
                    oUsuario.FormatoRep:= eEmailFormato(FieldByName('US_FORMATO').AsInteger);
                    DespuesDeAgregarDato( TCampoMaster( oUsuario ),
                           SmartListSuscritos,
                           BtnBorraUsuario,
                           SU_FRECUEN);
                    Modo := dsEdit;
               end;
          end;
     end;
end;

procedure TEditBaseReportes.LlenaListas;
var  oUsuario : TSuscripCampoMaster;
begin
     inherited;
     with dmSistema.cdsSuscrip do
     begin
          if Active then
          begin
               First;
               dmSistema.cdsUsuarios.Conectar;
               with dmSistema do
               begin
                    while NOT EOF do
                    begin
                         oUsuario := TSuscripCampoMaster.Create;
                         AgregaSuscripcion(oUsuario);
                         Next;
                    end;
               end;
          end;
     end;
     SmartListSuscritos.SelectEscogido( 0 );
end;

procedure TEditBaseReportes.BtnBorraUsuarioClick(Sender: TObject);
var
   oCampo: TSuscripCampoMaster;
begin
     inherited;
     oCampo:= TSuscripCampoMaster(LBPersonasSus.Items.Objects[LBPersonasSus.ItemIndex]);
     with dmSistema do
     begin
          if ( PuedeAgregarSuscripcion ( oCampo.Usuario,FALSE ) )then
          begin
               if ( ZetaDialogo.ZConfirm(Caption, '� Desea Borrar la Suscripci�n del Usuario ?', 0, mbNo ) ) then
               begin
                    if ( cdsSuscrip.Locate('US_CODIGO', oCampo.Usuario, []) ) then
                       cdsSuscrip.Delete;
                    BorraDato( SmartListSuscritos, BtnAgregaUsuario, BtnBorraUsuario, LBPersonasSus );
                    EnabledSuscritos(LBPersonasSus.Items.Count > 0)
               end;
          end;
     end;

end;

procedure TEditBaseReportes.ActualizaSuscrito( oUsuario : TSuscripCampoMaster );
begin
     AsignaProcSuscrito( NIL );
     SU_FRECUEN.ItemIndex := Ord( oUsuario.Frecuencia );
     SU_VACIO.ItemIndex := Ord( oUsuario.HayDatos )  ;
     US_EMAIL.Text := oUsuario.Direccion;
     US_FORMATO.Text:= ObtieneElemento(lfEmailFormato,Ord(oUsuario.FormatoRep));
     AsignaProcSuscrito( SuscripcionClick );
     EnabledSuscritos(TRUE);
end;

procedure TEditBaseReportes.ActualizaSuscritoObjeto;
var  Objeto : TSuscripCampoMaster;
     i:Integer;
begin
     i := LBPersonasSus.ItemIndex;
     with LBPersonasSus, Items do
     begin
          Objeto := TSuscripCampoMaster( Objects[ i ] );
          if Objeto <> NIL then
          begin
               Objeto.Frecuencia := eEmailFrecuencia( SU_FRECUEN.ItemIndex );
               Items[i] := Objeto.Titulo;
               Objeto.HayDatos := eEmailNotificacion( SU_VACIO.ItemIndex ) ;
               Modo := dsEdit;
          end;
     end;
end;


procedure TEditBaseReportes.SuscripcionClick(Sender: TObject);
begin
     inherited;
     ActualizaSuscritoObjeto;
end;

procedure TEditBaseReportes.AsignaProcSuscrito(oProc: TNotifyEvent);
begin
     SU_FRECUEN.OnClick := oProc;
     SU_VACIO.OnClick := oProc;
     US_EMAIL.OnExit := oProc;
     US_FORMATO.OnExit := oProc;
end;

procedure TEditBaseReportes.GetDatosSuscripcion( oCampo: TSuscripCampoMaster );
begin
     with  dmSistema do
     begin
          if cdsUsuariosLookup.Locate( 'US_CODIGO', cdsSuscrip.FieldByName('US_CODIGO').AsInteger, [] ) then
          begin
               oCampo.Titulo:= cdsUsuariosLookup.FieldByName('US_NOMBRE').AsString;
               oCampo.Direccion := cdsUsuariosLookup.FieldByName('US_EMAIL').AsString;
               oCampo.FormatoRep := eEmailFormato( cdsUsuariosLookup.FieldByName('US_FORMATO').AsInteger );
          end
          else
          begin
               oCampo.Titulo:= '<Usuario desconocido>';
               oCampo.Direccion := VACIO;
               oCampo.FormatoRep := efHTML;
          end;

          oCampo.Usuario:= cdsSuscrip.FieldByName('US_CODIGO').AsInteger;
          oCampo.Frecuencia :=  eEmailFrecuencia( cdsSuscrip.FieldByName('SU_FRECUEN').AsInteger );
          oCampo.HayDatos := eEmailNotificacion( cdsSuscrip.FieldByName('SU_VACIO').AsInteger );
     end;
end;

procedure TEditBaseReportes.GrabaSuscripciones;
var
   i: Integer;
begin
     with LBPersonasSus.Items do
     begin
          for i:= 0 to Count - 1 do
          begin
               with TSuscripCampoMaster(Objects[i]) do
               begin
                    with dmSistema.cdsSuscrip do
                    begin
                         if ( Locate('US_CODIGO', Usuario,[]) ) then
                         begin
                              Edit;
                              FieldByName('SU_FRECUEN').AsInteger:= Ord(Frecuencia);
                              FieldByName('SU_VACIO').AsInteger:= Ord(HayDatos);
                              Post;
                         end
                         else
                         begin
                              if ( dmSistema.AgregaSuscripciones( Usuario, Reporte.FieldByName('RE_CODIGO').AsInteger, Ord(Frecuencia), Ord(HayDatos) ) ) then
                              begin
                                   dmReportes.ModificaCampoReporte('US_SUSCRITO', Usuario);
                                   if( dmReportes.EstaEnClasifi( dmReportes.cdsReportes.FieldByName('RE_CODIGO').AsInteger, 'US_SUSCRITO' )  ) then
                                       RefrescarStatusBar;
                              end;
                         end;
                    end;
               end;
          end;

     end;
end;

procedure TEditBaseReportes.AgregaSuscripcion(oUsuario: TSuscripCampoMaster );
begin
     GetDatosSuscripcion( oUsuario );
     LBPersonasSus.Items.AddObject( oUsuario.Titulo, oUsuario );
end;

function TEditBaseReportes.PuedeAgregarSuscripcion(iUsuario: Integer; const lAgregar: Boolean): Boolean;
var
   i: Integer;
   oCampo: TSuscripCampoMaster;
begin
     Result:= TRUE;
     with LBPersonasSus.Items do
     begin
          if ( ( dmReportes.PuedeSuscribirse ) or
                  not ( dmCliente.Usuario = iUsuario ) )  then
          begin
               for i:= 0 to Count - 1 do
               begin
                    if ( lAgregar ) then
                    begin
                         oCampo:= TSuscripCampoMaster(Objects[i]);
                         Result:= not ( oCampo.Usuario = iUsuario );
                         if not Result then
                            Exit;
                    end
                    else
                        Result:= TRUE;
               end;
          end
          else
          begin
               ZError('Error','El Usuario no tiene los suficientes derechos para suscribirse o borrar su sucripci�n',0);
               Result:= FALSE;
          end;
     end;
end;

function TEditBaseReportes.PuedeSuscribir(const lEsAutoSuscripcion, lEsOtrosUsuarios: Boolean): Boolean;

 function CheckaUnDerecho( const NumeroDerecho, iDerecho : Integer ): Boolean;
 begin
      {$ifdef RDD}
      Result:= ZAccesosMgr.CheckUnDerecho( NumeroDerecho, iDerecho );
      {$else}
      Result:= ZAccesosMgr.CheckDerecho( NumeroDerecho, iDerecho );
      {$endif}
 end;
begin
      Result :=  ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION );

     if NOT Result then
     begin
          with dmReportes, dmDiccionario do
          begin
               if lEsAutoSuscripcion and lEsOtrosUsuarios then
               begin
                    Result:= PuedeAbrirCandado and
                             PuedeSuscribirse and
                             PuedeSuscribirUsuarios and
                             CheckaUnDerecho( GetDerechosClasifi(FClasificacionActiva), K_DERECHO_CAMBIO );
               end
               else if lEsAutoSuscripcion then
                    Result:= PuedeAbrirCandado and
                             PuedeSuscribirse and
                             CheckaUnDerecho( GetDerechosClasifi(FClasificacionActiva),K_DERECHO_CAMBIO)
               else
                   Result:= PuedeAbrirCandado and
                            PuedeSuscribirUsuarios and
                            CheckaUnDerecho(GetDerechosClasifi(FClasificacionActiva),K_DERECHO_CAMBIO);
          end;
     end;
end;

procedure TEditBaseReportes.HabilitaControles;
begin
     inherited;
     EnabledSuscripControls( dmReportes.cdsEditReporte.State <> dsInsert );
end;

end.
