unit fgrafica;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs,Chart, DBChart, StdCtrls, Buttons,
  ExtCtrls, db, Series, ZBaseDlgModal, ComCtrls, ToolWin, ImgList,
  DBClient,ZetaCommonLists, DBCtrls, Menus,
  TeeFunci,
  {$ifdef VER130}
  IEditCha,
  {$else}
  TeeEditCha,
  Variants,
  {$endif}
  TeeProcs, TeEngine,
  TeeComma, TeeEdit,
  ZReportTools,
  ZetaClientDataSet;


type eTipoGrafica = ( tgBarraVertical,tgBarraHorizontal,tgPie,tgLinea,tgArea,tgPuntos);

type
  TGrafica = class(TForm)
    ToolBar1: TToolBar;
    BImprimir: TToolButton;
    BAcercar: TToolButton;
    ImageList: TImageList;
    dsDataset: TDataSource;
    BTipoGrafica: TToolButton;
    MenuTipoGrafica: TPopupMenu;
    meBarraVertical: TMenuItem;
    meBarraHorizontal: TMenuItem;
    mePie: TMenuItem;
    meLinea: TMenuItem;
    BSeries: TToolButton;
    MenuImpresion: TPopupMenu;
    meGraficaActual: TMenuItem;
    meTodas: TMenuItem;
    Arbol: TTreeView;
    Splitter: TSplitter;
    cdsGrupos: TZetaClientDataSet;
    Panel1: TPanel;
    ScrollGrafica: TScrollBar;
    MenuSeries: TPopupMenu;
    meArea: TMenuItem;
    mePuntos: TMenuItem;
    meGrupoActual: TMenuItem;
    meTodoslosGrupos: TMenuItem;
    BOpciones: TToolButton;
    MenuOpciones: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem3: TMenuItem;
    BDescripcion: TToolButton;
    MenuDescripcion: TPopupMenu;
    NoMostrarEtiqueta1: TMenuItem;
    Valor1: TMenuItem;
    Porcentaje2: TMenuItem;
    LabelyPorcentaje1: TMenuItem;
    LabelyValor2: TMenuItem;
    Leyenda2: TMenuItem;
    PorcentajeTotal2: TMenuItem;
    LabelyPorcentajeTotal2: TMenuItem;
    BAlejar: TToolButton;
    ChartEditor: TChartEditor;
    Chart: TDBChart;
    lbDemo: TLabel;
    LblNoGraficar: TLabel;
    procedure BAcercarClick(Sender: TObject);
    procedure BAnteriorClick(Sender: TObject);
    procedure ScrollGraficaChange(Sender: TObject);
    procedure BEditarClick(Sender: TObject);
    procedure BSeriesClick(Sender: TObject);
    procedure BImprimirClick(Sender: TObject);
    procedure meGraficaActualClick(Sender: TObject);
    procedure ArbolChange(Sender: TObject; Node: TTreeNode);
    procedure DrawItemMenu(Sender: TObject; ACanvas: TCanvas; ARect: TRect;
      Selected: Boolean);
    procedure ClickItemMenu(Sender: TObject);
    procedure ClickItemDesc(Sender: TObject);
    procedure TipoGraficaClick(Sender: TObject);
    procedure BTipoGraficaClick(Sender: TObject);
    procedure meTodasClick(Sender: TObject);
    procedure meTodoslosGruposClick(Sender: TObject);
    procedure meGrupoActualClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure NoMostrarEtiqueta1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FCampos,FGrupos : TStrings;
    FDataSet : TDataSet;
    FNombre, FXSeries : string;
    FGroupCount, FColumnCount,FRecordCount,FMaxPoints : Integer;
    FLastSeries : integer;
    FAgregaDesc,FSoloTotales : Boolean;
    FTipo : eTipoGrafica;
    FMenuDescripcion : String;
    function CreaGrafica(const lInit: Boolean=TRUE) : Boolean;
    function GetDescripcion(const sTitulo : string ) : string;
    procedure CreaSerie( const sTitulo, sX, sY : String;
                         const Tipo : Integer);
    procedure SetDefaultsGrafica;
    procedure AgregaSeries;
    procedure AgregaPie( var oSerie : TPieSeries );
    procedure DefineFXSeries;
    procedure AgregaGrupos;
    procedure DefineGrupos;
    procedure FiltraDataSet( const sFiltro : string );
    procedure SetChartEditorOptions( const sTitulo : string;
                                     const Tabs: TChartEditorHiddenTabs;

                                     const Options: TChartEditorOptions);
    procedure ImprimirGrafica(const lTodas: Boolean; const Paginas : integer);
    procedure ConstruyeArbol;

    function UnaSerie: Boolean;
    procedure SetMaxPoints(const iSeriesCount: integer);
    procedure SetTipoGrafica( const eTipo : eTipoGrafica );

    procedure MuestraSerie(const Posicion: integer);
    procedure MuestraDesc(const Posicion: integer);

    procedure MuestraTodasSeries;

    procedure ClickItemMenuTodos(Sender: TObject);

    procedure CreateTempDataset;
    procedure InitTempDataSet;
    function GetFiltroGrupos(Nodo: TTreeNode): String;
    procedure SetMenuChecked(oMenu : TMenu; const Posicion: integer);
    procedure SetDefView3d;
    procedure AgregaMenuSeries(eTipo: eTipoGrafica);
    function CampoEsSerie(oCampo: TCampoListado): Boolean;
    procedure SetMaxScroll(const iPuntos: integer);
    function ValidaPieSeries( const iPos: Integer ): Boolean;
    procedure SetLblDemo;
    procedure Desprepara;
  public
    { Public declarations }
    function GeneraGrafica: Boolean;
    function Prepara( const sTitulo,sNombre: string;
                      const lSoloTotales : Boolean;
                      oDataSet: TDataSet;
                      oCampos, oGrupos: TStrings): Boolean;
  end;


var
  Grafica: TGrafica;

  procedure PreparaGrafica( const sTitulo,sNombre: string;
                            const lSoloTotales : Boolean;
                            oDataSet: TDataSet;
                            oCampos, oGrupos: TStrings);
  procedure DesPreparaGrafica;


implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZQRReporteGrafica;

{$R *.DFM}

const K_MAX_POINTS = 30;
      Off_SET = 3;
      K_STR_TOTAL_EMPRESA = 'Total de Empresa';


procedure PreparaGrafica( const sTitulo,sNombre: string; const lSoloTotales : Boolean;oDataSet: TDataSet;oCampos, oGrupos: TStrings);
begin
     if Grafica = NIL then
        Grafica := TGrafica.Create( Application.MainForm );
     Grafica.Prepara( sTitulo,sNombre,lSoloTotales,
                      oDataSet,oCampos,oGrupos);
end;

procedure DesPreparaGrafica;
begin
     Grafica.Desprepara;
     FreeAndNil( Grafica );
end;

procedure TGrafica.AgregaPie( var oSerie : TPieSeries );
begin
     with oSerie do
     begin
          with Marks do
          begin
               Visible := TRUE;
               Style := smsLabelPercent;
          end;
          ColorEachPoint := TRUE;
          ExplodeBiggest := 25;
          with OtherSlice do
          begin
               Style := poBelowPercent;
               Text := 'Otros';
               Value := 5;
          end;
     end;
end;

procedure TGrafica.CreaSerie( const sTitulo, sX, sY : String;
                              const Tipo : Integer );
var
   oSeries : TBarSeries;
begin
     oSeries := TBarSeries.Create( Chart );
     with oSeries do
     begin
          Title := sTitulo;
          Marks.Visible := FALSE;
          Marks.Style := smsValue;
          BarStyle := bsRectGradient;
          ParentChart := Chart;
          DataSource := FDataSet;
          //YValues.Value := 133.5;//esto no se puede
          YValues.ValueSource := sy;  { <-- the Field for Bar Values }
          XLabelsSource := FXSeries;          { <-- the Field for Bar Labels }
          //ColorEachPoint := FColumnCount = 1;
     end;
     Chart.AddSeries( oSeries );
end;

procedure TGrafica.DefineFXSeries;
 var
    oGrupo : TGrupoOpciones;
    sTitulo : string;
    i : integer;

 procedure SetCamposFXSeries(oLista : TStrings);
 begin
      if oLista.Count > 0 then
      begin
           i:=0;
           {if oLista.Count = 1 then i := 0
           else i := 1;}
           with TCampoOpciones(oLista.Objects[i]) do
           begin
                FXSeries := NombreCampo;
                sTitulo := Titulo;
           end
      end
      else FXSeries := '< Indefinido >';
 end;
begin
     if NOT FSoloTotales then
     begin
          SetCamposFXSeries(FCampos);
          {if FCampos.Count > 0 then
          begin
               if FCampos.Count = 1 then i := 0
               else i := 1;
               with TCampoListado(FCampos.Objects[i]) do
               begin
                    FXSeries := NombreCampo;
                    sTitulo := Titulo;
               end;
          end
          else FXSeries := '< Indefinido >';}
     end
     else
     begin
          {Solo Totales de Empresa}
          if FGrupos.Count = 1 then
          begin
               //FXSeries := K_STR_TOTAL_EMPRESA; {Como se le indica una constante?}
               SetCamposFXSeries(FCampos);
               {with TCampoListado(FCampos.Objects[0]) do
               begin
                    FXSeries := NombreCampo;
                    sTitulo := Titulo;
               end;}
          end
          else {Solo Totales, con Grupos}
          begin
               {Es el Grupo mas Interno}
               oGrupo := TGrupoOpciones(FGrupos.Objects[FGrupos.Count - 1]);
               if oGrupo.ListaEncabezado.Count = 0 then
               begin
                    FXSeries := oGrupo.NombreCampo;
                    sTitulo := oGrupo.Titulo;
               end
               else
               begin
                    SetCamposFXSeries(oGrupo.ListaEncabezado);
                    {with TCampoOpciones(oGrupo.ListaEncabezado.Objects[0]) do
                    begin
                         FXSeries := NombreCampo;
                         sTitulo := Titulo;
                    end;}
               end;
          end;
     end;

     Chart.Foot.Text.Text:= FMenuDescripcion;
end;

procedure TGrafica.AgregaSeries;
 var oCampo : TCampoListado;
     i : integer;
     sCampo : string;
begin
     Chart.FreeAllSeries;
     DefineFXSeries;
     {----------------------------}
     {Series a Detalle, Sin Grupos}
     with FCampos do
     begin
          if FCampos.Count = 1 then
             Chart.Legend.LegendStyle := lsSeries;
          for i:= 0 to Count -1 do
          begin
               oCampo := TCampoListado(Objects[i]);
               if CampoEsSerie(oCampo) then
               begin
                    if FSoloTotales then
                       sCampo := oCampo.NombreTotal
                    else sCampo := oCampo.NombreCampo;
                    CreaSerie( oCampo.Titulo,'',sCampo,0 );
               end;
          end;
     end;
     {------------------------------}
end;

procedure TGrafica.InitTempDataSet;
begin
     with cdsGrupos do
     begin
          Active := False;
          while FieldCount > 0 do
          begin
               Fields[ FieldCount - 1 ].DataSet := nil;
          end;
          FieldDefs.Clear;
     end;
end;

procedure TGrafica.CreateTempDataset;
var
   i: Integer;
begin
     with cdsGrupos do
     begin
          for i := 0 to ( FieldDefs.Count - 1 ) do
          begin
               FieldDefs[ i ].CreateField( Self );
          end;
          CreateDataset;
     end;
end;

procedure TGrafica.AgregaGrupos;
begin
     InitTempDataSet;
     DefineGrupos;
end;

procedure TGrafica.DefineGrupos;
 var oGrupo : TGrupoOpciones;
     j: integer;

 function EsCorteGrupo : Boolean;
  var i : integer;
 begin
      Result := FALSE;
      for i := 1 to FGrupos.Count -1 do
      begin
           oGrupo := TGrupoOpciones(FGrupos.Objects[i]);
           Result := Result OR
                     (FDataSet.FieldByName(oGrupo.NombreCampo).AsString <>
                     cdsGrupos.FieldByName(oGrupo.NombreCampo).AsString);
           if Result then Break;
      end;
 end;

 procedure AgregaRegistroGrupo;
  var i: integer;
  procedure SetRecord(const sField : string);
  begin
       with FDataSet.FieldByName(sField) do
       begin
            if DataType in [ftDate,ftTime,ftDateTime] then
               cdsGrupos.FieldByName(sField).AsDateTime := AsDateTime
            else cdsGrupos.FieldByName(sField).AsString := AsString;
       end;
  end;
 begin
      cdsGrupos.Append;
      for i := 1 to FGrupos.Count -1 do
      begin
           oGrupo := TGrupoOpciones(FGrupos.Objects[i]);
           SetRecord(oGrupo.NombreCampo);
           with oGrupo.ListaEncabezado do
           begin
                if Count>0 then
                   SetRecord(TCampoListado(Objects[0]).NombreCampo);
                if Count>1 then
                   SetRecord(TCampoListado(Objects[1]).NombreCampo);
           end;
      end;
      cdsGrupos.Post;
 end;

 procedure AgregaGruposDataSet( const sFieldName : string );

   procedure AgregaDefField( const Tipo : TFieldType;
                             const iSize : Integer );
   begin
        with cdsGrupos.FieldDefs.AddFieldDef do
        begin
             Name := sFieldName;
             DisplayName := sFieldName;
             DataType := Tipo;
             Size := iSize;
        end;
   end;
 begin
      if sFieldName = '' then
         with cdsGrupos.FieldDefs.AddFieldDef do
         begin
              Name := '_'+IntToStr(j);
              DataType := ftString;
              Size := 1;
              Inc(j);
         end
      else
          with FDataSet.FieldByName(sFieldName) do
             AgregaDefField( DataType, Size );
 end;

 var i : integer;
     sIndexName : string;
begin
     j:=0;
     for i := 1 to FGrupos.Count -1 do
     begin
          oGrupo := TGrupoOpciones(FGrupos.Objects[i]);
          AgregaGruposDataset( oGrupo.NombreCampo );
          with oGrupo.ListaEncabezado do
          begin
               if Count > 0 then
                  AgregaGruposDataset( TCampoListado(Objects[0]).NombreCampo )
               else AgregaGruposDataset( oGrupo.NombreCampo+IntToStr(i) );
               if Count > 1 then
                  AgregaGruposDataset( TCampoListado(Objects[1]).NombreCampo )
               else AgregaGruposDataset( '' );
          end;
          sIndexName := ConcatString( sIndexName,oGrupo.NombreCampo, ',' );
     end;

     CreateTempDataset;

     with FDataSet do
     begin
          DisableControls;

          {Se Agrega el primer registro al DataSet Temporal}
          First;
          AgregaRegistroGrupo;
          Next;
          while NOT EOF do
          begin
               if EsCorteGrupo then
                  AgregaRegistroGrupo;
               Next;
          end;
          EnableControls;
     end;
     {$ifdef CAROLINA}
     cdsGrupos.SaveToFile(ExtractFileDir((Application.ExeName )) + '\Grupos.cds',dfBinary);
     {$endif}
     {SoloTotales con dos o mas Grupos}
end;

procedure TGrafica.SetDefaultsGrafica;
 var I : integer;
begin
     FGroupCount := FGrupos.Count - 1;
     FColumnCount := 0;

     for i:= 0 to FCampos.Count -1 do
         if NOT (TCampoListado(FCampos.Objects[i]).OpImp in [ocNinguno,ocAutomatico]) then
            Inc(FColumnCount);

     with Chart do
     begin
          with Title do
          begin
               Text.Text := FNombre;
               Font.Color := clBlack;
          end;
          Legend.LegendStyle := lsSeries;
          Legend.Alignment := laBottom;
     end;
     SetDefView3d;

     AgregaMenuSeries(tgBarraVertical);
end;

procedure TGrafica.AgregaMenuSeries(eTipo : eTipoGrafica);
 var i,j: integer;
 procedure AgregaPopupDescr(const sTitulo : string);
  var Popup : TMenuItem;
 begin
      Popup := TMenuItem.Create(Self);
      with Popup do
      begin
           Caption := GetDescripcion(sTitulo);
           OnClick := ClickItemDesc;
           RadioItem := TRUE;
           Checked := i=0;
           if Checked then
              FMenuDescripcion := Caption;
           Tag := i;
      end;
      MenuDescripcion.Items.Add(Popup);
 end;

 procedure AgregaPopupMenu(const sTitulo : string; const lEnabled : Boolean = TRUE);
  var Popup : TMenuItem;
 begin
      Popup := TMenuItem.Create(Self);
      with Popup do
      begin
           Caption := sTitulo;
           OnClick := ClickItemMenu;
           RadioItem := TRUE;
           Checked := FALSE;
           Tag := j;
           Enabled := lEnabled;
      end;
      MenuSeries.Items.Add(Popup);
 end;
 var oCampo : TCampoListado;
     Popup : TMenuItem;
begin
     MenuSeries.Items.Clear;
     MenuDescripcion.Items.Clear;
     j:=0;
     if (FColumnCount > 1) then
     begin
          if (eTipo <> tgPie) then
          begin
               Popup := TMenuItem.Create(Self);
               with Popup do
               begin
                    Caption := 'Todos';
                    //OnDrawItem := DrawItemMenu;
                    OnClick := ClickItemMenuTodos;
                    RadioItem := TRUE;
                    Checked := TRUE;
               end;
               MenuSeries.Items.Add(Popup);
          end;
          FAgregaDesc := NOT FSoloTotales AND (FGrupos.Count>0);

          for i := 0 to FCampos.Count -1 do
          begin
               oCampo := TCampoListado(FCampos.Objects[i]);
               if FAgregaDesc AND (i<=1) then
                  AgregaPopupDescr(oCampo.Titulo);
               if CampoEsSerie(oCampo) then
               begin
                    AgregaPopupMenu(oCampo.Titulo);
                    Inc(j);
               end;
          end;
          BDescripcion.Enabled := NOT (FSoloTotales AND (FGrupos.Count=0));
          if NOT FAgregaDesc then
          begin
               with FGrupos,TGrupoOpciones(Objects[Count-1]) do
                    for i := 0 to ListaEncabezado.Count - 1 do
                        if (i<=1) then
                           AgregaPopupDescr(TCampoOpciones(ListaEncabezado.Objects[i]).Titulo)
                        else Break;
          end;
     end
     else
     begin
          for i := 0 to FCampos.Count -1 do
          begin
               oCampo := TCampoListado(FCampos.Objects[i]);
               if CampoEsSerie(oCampo) then
               begin
                    AgregaPopupMenu(TCampoListado(FCampos.Objects[i]).Titulo, FALSE);
                    Break;
               end;
          end;
     end;
end;

function TGrafica.CampoEsSerie(oCampo : TCampoListado) : Boolean;
begin
     with oCampo do
          Result := (OpImp <> ocNinguno) AND
                    (TipoImp in [tgNumero,tgFloat]) AND
                    (FSoloTotales  OR NOT SQLColumna.EsConstante );
end;

procedure TGrafica.SetMaxScroll( const iPuntos : integer );
begin
     with ScrollGrafica do
     begin
          Max := Trunc(FRecordCount/iPuntos);
          //if (FRecordCount mod iPuntos) < iPuntos then Max := Max + 1;
          Enabled := Max>=1;
     end;
end;

procedure TGrafica.SetMaxPoints(const iSeriesCount : integer);
begin
     FMaxPoints := iMin(Trunc(K_MAX_POINTS/iSeriesCount-1)+1,FDataset.RecordCount);
     Chart.MaxPointsPerPage := FMaxPoints;
     SetMaxScroll(FMaxPoints)
end;

function TGrafica.CreaGrafica(const lInit: Boolean=TRUE) : Boolean;
begin
     Result := TRUE;
     SetDefaultsGrafica;

     if UnaSerie then
     begin {Barras - N Series}
          Arbol.Visible := FALSE;
          Splitter.Visible := FALSE;
          AgregaSeries;
     end
     else
     begin
          if lInit then
             AgregaGrupos;
          AgregaSeries;
          ConstruyeArbol;
          cdsGrupos.First;
          //FiltraDataSet('');
          Splitter.Visible := TRUE;
     end;
     ClickItemMenu(MenuSeries.Items[0]);
     //SetMaxPoints(FColumnCount);
end;

function TGrafica.Prepara( const sTitulo,sNombre: string; const lSoloTotales : Boolean;oDataSet: TDataSet;oCampos, oGrupos: TStrings): Boolean;
begin
     Result := TRUE;
     FCampos := oCampos;
     FGrupos := oGrupos;
     FDataSet := oDataSet;
     FNombre := sTitulo;
     FSoloTotales := lSoloTotales;

     FRecordCount := FDataSet.RecordCount;
     //ScrollGrafica.Enabled := NOT FSoloTotales;

     Caption := 'Gr�fica: '+BorraCReturn(sNombre);
     WindowState := wsMaximized;

end;

procedure TGrafica.Desprepara;
begin
     Chart.FreeAllSeries;
end;

function TGrafica.GeneraGrafica: Boolean;
 var i: integer;
begin
     Result := FALSE;
     for i:=0 to FCampos.Count-1 do
     begin
          Result := CampoEsSerie(TCampoListado(FCampos.Objects[i]));
          if Result then
             Break;
     end;

     if NOT Result then
        Raise Exception.Create('No Hay Ninguna Columna para Graficar');

     Result := TRUE;
     if CreaGrafica then
     begin
          ShowModal;
          FDataSet.Filter := '';
          FDataSet.Filtered :=FALSE;
     end;
end;

procedure TGrafica.BAcercarClick(Sender: TObject);
begin
     inherited;
     with Chart do
     begin
          if MaxPointsPerPage > 1 then
          begin
               MaxPointsPerPage := MaxPointsPerPage - 1;
               //ScrollGrafica.Max := FRecordCount-MaxPointsPerPage;
               SetMaxScroll(MaxPointsPerPage);
          end;
          BAcercar.Enabled := MaxPointsPerPage > 1;
          BAlejar.Enabled := TRUE;
     end;

end;

procedure TGrafica.BAnteriorClick(Sender: TObject);
begin
     inherited;
     Chart.Page := ScrollGrafica.Position+1;
end;

procedure TGrafica.ScrollGraficaChange(Sender: TObject);
begin
     inherited;
     Chart.Page := ScrollGrafica.Position+1;
end;

procedure TGrafica.FiltraDataSet( const sFiltro : string );
begin
     FDataSet.Filter := sFiltro;
     FDataSet.Filtered := TRUE;
     if ( FLastSeries > 0 ) then
        MuestraSerie( FLastSeries - 1 );     // Reevalua la serie activa, para casos de Graficas de PIE
     //Chart.MaxPointsPerPage := iMin(FMaxPoints,FDataset.RecordCount);
     if FLastSeries > 0 then SetMaxPoints(1)
     else SetMaxPoints(FColumnCount);
     Chart.RefreshData;
end;

procedure TGrafica.BEditarClick(Sender: TObject);
begin
     SetChartEditorOptions( 'Editar Gr�fica',
                            [],
                            [ceAdd, ceDelete, ceChange, ceClone, ceTitle] );
end;

procedure TGrafica.SetChartEditorOptions( const sTitulo : string;
                                          const Tabs : TChartEditorHiddenTabs;
                                          const Options : TChartEditorOptions );
begin
     with ChartEditor do
     begin
          Title := sTitulo;
          //HideTabs := Tabs;
          //Options := Options;
          Execute;
     end;
end;

procedure TGrafica.BSeriesClick(Sender: TObject);
begin
     SetChartEditorOptions( 'Editar Series',
                            [cetGeneral, cetAxis, cetTitles, cetLegend, cetPanel, cetPaging, cetWalls, cet3D],
                            [ceAdd, ceDelete, ceChange, ceClone, ceTitle] );

end;

procedure TGrafica.BImprimirClick(Sender: TObject);
begin
     ImprimirGrafica(FALSE,1);
end;

function TGrafica.UnaSerie : Boolean;
begin
     Result := (FGroupCount = 0) OR
               ((FGroupCount = 1) AND (FColumnCount= 1) AND FSoloTotales )
end;

procedure TGrafica.ImprimirGrafica( const lTodas : Boolean; const Paginas : integer );
 var oDataSet : TDataSet;
begin
     if ZReportTools.ModuloGraficas then
     begin

          try
             if lTodas then
             begin
                  if UnaSerie then oDataSet := NIL
                  else oDataSet := cdsGrupos;
                  QrReporteGrafica.GeneraVariasGraficas(Chart,FDataSet,oDataset,Paginas);
             end
             else {Imprime la Grafica que se esta viendo en ese momento}
                  QrReporteGrafica.GeneraUnaGrafica(Chart,FDataSet,Paginas);
          finally
                 CreaGrafica(FALSE);
          end;
     end;
end;
procedure TGrafica.meGraficaActualClick(Sender: TObject);
begin
     ImprimirGrafica(FALSE,1);
end;

procedure TGrafica.meTodasClick(Sender: TObject);
begin
     ImprimirGrafica(TRUE,ScrollGrafica.Max+1);
end;

procedure TGrafica.meTodoslosGruposClick(Sender: TObject);
begin
     ImprimirGrafica(TRUE,ScrollGrafica.Max);
end;

procedure TGrafica.meGrupoActualClick(Sender: TObject);
begin
     ImprimirGrafica(FALSE,ScrollGrafica.Max);
end;

{Arbol}
procedure TGrafica.ConstruyeArbol;
var
    oPadre, oHijo : TTreeNode;
    sActual : String;
    aGrupos : Array of String;
    nCorte, nNivel : Integer;


    procedure InicializaArbol;
    var
        i : Integer;
    begin
        aGrupos := VarArrayCreate( [ 0, FGroupCount-1 ], varOleStr );
        for i := 0 to FGroupCount-1 do
            aGrupos[ i ] := '';
        Arbol.Items.BeginUpdate;
        Arbol.Items.Clear;
        oPadre := Arbol.Items.AddChild( NIL, 'Empresa' );
        with oPadre do
        begin
            ImageIndex := 19;
            SelectedIndex := 19;
            Data := Pointer( 0 );
        end;
    end;

    function GrupoCorte : Integer;
    var
        i : Integer;
    begin
        Result := -1;
        for i := 0 to FGroupCount-1 do
        begin
            if ( cdsGrupos.Fields[ i ].AsString <> aGrupos[ i ] ) then
            begin
                Result := i;
                Exit;
            end;
        end;
    end;

begin   // ConstruyeArbol
     Arbol.Visible := not (FSoloTotales and (FGroupCount = 1));
     if Arbol.Visible then
     begin
          InicializaArbol;
          with cdsGrupos do
          begin
              First;
              while not Eof do
              begin
                  nCorte := GrupoCorte;
                  if ( nCorte >= 0 ) then
                  begin
                      while oPadre.Level > nCorte do
                          oPadre := oPadre.Parent;

                      for nNivel := nCorte to FGroupCount-1 do
                      begin
                        sActual := Fields[ nNivel*Off_SET ].AsString;
                        aGrupos[ nNivel ] := sActual;
                        //pendiente, para cuando haya descripcion:
                        //oHijo := Arbol.Items.AddChild( oPadre, sActual + Fields[ nNivel + nGrupos ].AsString );
                        sActual := Fields[ (nNivel*Off_SET)+1 ].AsString;
                        if StrLleno(Fields[ (nNivel*Off_SET)+2 ].AsString) then
                           sActual := sActual + ': ' + Fields[ (nNivel*Off_SET)+2 ].AsString;
                        oHijo := Arbol.Items.AddChild( oPadre, sActual );
                        with oHijo do
                        begin
                            ImageIndex := 14;
                            //pendiente, si se quiere cambiar el bitmap cuando
                            //esta seleccionado
                            SelectedIndex := 15;
                            Data := Pointer( RecNo );
                        end;
                        oPadre := oHijo;
                      end;
                  end;
                  Next;
              end;
          end;

          with Arbol do
          begin
              Items[ 0 ].Expand( FALSE );
              Items.EndUpdate;
              Selected := Items[ 1 ];
              FiltraDataset(GetFiltroGrupos( Selected ));
          end;
     end;
end;

function TGrafica.GetFiltroGrupos( Nodo : TTreeNode ) : String;
var
    nRecord : Integer;
    i : Integer;

    function FiltroUnCampo( const nPos : Integer ) : String;
    begin //PENDIENTE SI SE AGRUPA POR TIPO <> STRING
        // Supone que todos los Grupos son tipo String
        with cdsGrupos.Fields[ nPos*Off_SET] do
             if DataType in [ftDate,ftTime,ftDateTime] then
                Result := FieldName  + ' = ' + EntreComillas(FormatDateTime('dd/mm/yyyy',AsDateTime))
             else Result := FieldName  + ' = ' + EntreComillas(AsString);
    end;
    function GetDescripcion(const iPos : integer): string;
    begin
         with cdsGrupos do
         begin
              Result := Fields[(iPos*Off_SET)+1].AsString;
              if StrLleno(Fields[(iPos*Off_SET)+2].AsString) then
                 Result := Result +': '+Fields[(iPos*Off_SET)+2].AsString;
         end;
    end;
    function GetGrupoDescripcion(const Posicion : integer) : string;
    begin
         with TGrupoOpciones(FGrupos.Objects[Posicion]) do
         begin
              if ListaEncabezado.Count > 0 then
                 Result := ListaEncabezado[0]
              else Result := Titulo;
         end;
    end;
begin   // GetFiltroGrupos
     Chart.Title.Text.Text := '';
     nRecord := Integer( Nodo.Data );
     if ( nRecord = 0 ) then
         Result := ''
     else
     begin
         cdsGrupos.Recno := nRecord;
         Result := FiltroUnCampo( 0 );
         Chart.Title.Text.Add( GetGrupoDescripcion(1) + ': ' + GetDescripcion(0) );
         for i := 1 to Nodo.Level-1 do
         begin
              Result := ConcatFiltros(Result, FiltroUnCampo( i ));
              Chart.Title.Text.Add( GetGrupoDescripcion(i+1) + ':' + GetDescripcion(i) );
         end;
     end;
end;

procedure TGrafica.ArbolChange(Sender: TObject; Node: TTreeNode);
begin
    if Node <> NIL then
       FiltraDataset(GetFiltroGrupos( Node ));
end;

procedure TGrafica.DrawItemMenu(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; Selected: Boolean);
begin
     {aCanvas.Pen.Color := clRed;
     with (Sender as TMenuItem).Canvas do  { draw on control canvas, not on the form }
     {begin
          Pen.Color := clRed;
          {FillRect(Rect);       { clear the rectangle }
          {sIndex := IntToStr( Index + 1  );
          TextOut(Rect.Left + 18 - TextWidth( sIndex ), Rect.Top, sIndex );
          TextOut(Rect.Left + 20, Rect.Top, ': '
              + (Control as TListBox).Items[Index])  { display the text }
     //end;
end;

procedure TGrafica.SetMenuChecked( oMenu : TMenu; const Posicion : integer );
 var i: integer;
begin
     with oMenu do
     begin
          if Items.Count = 1 then
             Items[0].Checked := True
          else
          begin
               for i:=0 to Items.Count - 1 do
                   Items[i].Checked := FALSE;
               Items[Posicion].Checked := True;
          end;
     end;
end;

procedure TGrafica.ClickItemDesc(Sender: TObject);
begin
     with TMenuItem(Sender) do
     begin
          MuestraDesc(Tag);
          SetMenuChecked( MenuDescripcion, Tag );
     end;
end;

procedure TGrafica.ClickItemMenuTodos(Sender: TObject);
begin
     MuestraTodasSeries;
     SetMenuChecked( MenuSeries, 0 );
end;

procedure TGrafica.ClickItemMenu(Sender: TObject);
begin
     with TMenuItem(Sender) do
     begin
          MuestraSerie(Tag);
          if FTipo <> tgPie then
             SetMenuChecked( MenuSeries, Tag + 1 )
          else SetMenuChecked( MenuSeries, Tag );
     end;
end;

procedure TGrafica.MuestraTodasSeries;
 var i: integer;
begin
     for i := 0 to FColumnCount-1 do
     begin
          Chart.Series[i].Active := TRUE;
     end;
     SetMaxPoints(FColumnCount);
     FLastSeries := 0;
end;

procedure TGrafica.MuestraDesc(const Posicion: integer);
 var i: integer;
     sTitulo : string;
begin
     if FAgregaDesc then
     begin
          with TCampoListado(FCampos.Objects[Posicion]) do
          begin
               FXSeries := NombreCampo;
               sTitulo := Titulo;
          end;
     end
     else
     begin
          with FGrupos,TGrupoOpciones(Objects[Count-1]) do
               with TCampoOpciones(ListaEncabezado.Objects[Posicion]) do
               begin
                    FXSeries := NombreCampo;
                    sTitulo := Titulo;
               end;
     end;
     for i := 0 to FColumnCount-1 do
         Chart.Series[i].XLabelsSource := FXSeries;

     with Chart.Foot.Text do
     begin
          Clear;
          Add(GetDescripcion(sTitulo));
     end;

end;

procedure TGrafica.MuestraSerie( const Posicion : integer );
 var i: integer;
begin
     LblNoGraficar.Visible := FALSE;
     SetLblDemo;
     if Chart.SeriesCount > 0 then
     begin
          for i := 0 to FColumnCount-1 do
              Chart.Series[i].Active := ( i = Posicion ) and
                                        ( ( not ( Chart.Series[i] is TPieSeries ) ) or
                                          ValidaPieSeries(i) );
     end;
     SetMaxPoints(1);
     FLastSeries := Posicion+1;
end;

function TGrafica.ValidaPieSeries( const iPos: Integer ): Boolean;
const
     K_MESS_NO_GRAFICAR = 'El Campo <%s> No tiene Datos Para Graficar';
var
   oBookMark : TBookMark;
   sCampo : String;
begin
     Result := FALSE;
     with Chart.Series[iPos] do
     begin
          sCampo := YValues.ValueSource;
          with TDataSet( Datasource ) do                                     // Se esta suponiendo que el datasource siempre es un DataSet
               if Active and ( not IsEmpty ) then
               begin
                    oBookMark := GetBookMark;
                    try
                       First;
                       while ( not EOF ) and ( not Result ) do
                       begin
                            Result := ( FieldByName( sCampo ).AsFloat > 0 );    // Es un valor n�merico, con uno solo que no sea cero no habr� problema
                            Next;
                       end;
                       GotoBookMark( oBookMark );
                    finally
                       FreeBookmark( oBookMark );
                    end;
               end;
          LblNoGraficar.Visible := ( not Result );
          if LblNoGraficar.Visible then
          begin
               LbDemo.Visible := FALSE;
               LblNoGraficar.Caption := Format( K_MESS_NO_GRAFICAR, [ Title ] );
               LblNoGraficar.Align := alClient;
          end;
     end;
end;

procedure TGrafica.TipoGraficaClick(Sender: TObject);
begin
     SetTipoGrafica(eTipoGrafica(TMenuItem(Sender).MenuIndex));
end;

procedure TGrafica.SetTipoGrafica( const eTipo : eTipoGrafica );
 var oSerie : TChartSeries;
     SerieClass:TChartSeriesClass;
     i: integer;
begin
     FTipo := eTipo;
     case eTipo of
          tgBarraVertical: SerieClass := TBarSeries;
          tgBarraHorizontal:SerieClass := THorizBarSeries;
          tgPie:SerieClass := TPieSeries;
          tgLinea:SerieClass := TLineSeries;
          tgArea:SerieClass := TAreaSeries;
          tgPuntos:SerieClass := TPointSeries
          else SerieClass := TBarSeries;
     end;

     for i := 0 to FColumnCount -1 do
     begin
          oSerie := Chart.Series[i];
          ChangeSeriesType(oSerie,SerieClass);
          with oSerie do
          begin
               with Marks do
               begin
                    Visible := FALSE;
                    Style := smsValue;
               end;
               ColorEachPoint := FALSE;
          end;

          if (SerieClass = TBarSeries) then
               TBarSeries(oSerie).BarStyle := bsRectGradient
          else if (SerieClass = THorizBarSeries) then
               THorizBarSeries(oSerie).BarStyle := bsRectGradient
          else if SerieClass = TPieSeries then
               AgregaPie(TPieSeries(oSerie));
     end;

     if SerieClass = TPieSeries then
     begin
          MuestraSerie( 0 ); //Nada mas va a mostrar la primera;
          AgregaMenuSeries(eTipo);
     end
     else
     begin
          SetMaxPoints( FColumnCount );
          SetDefView3d;
          if FLastSeries > 0 then
             MuestraSerie(FLastSeries-1)
          else MuestraTodasSeries;
     end;

     BAcercar.Enabled := eTipo <> tgPie;
     BAlejar.Enabled := eTipo <> tgPie;
     ScrollGrafica.Enabled := eTipo <> tgPie;

     //MenuSeries.Items[0].Checked := TRUE;
end;

procedure TGrafica.SetDefView3d;
begin
     with Chart.View3dOptions do
     begin
          Elevation := 0;
          Perspective := 0;
          Tilt := 0;
          Rotation := 350;
          Orthogonal := FALSE;
     end;
end;

procedure TGrafica.BTipoGraficaClick(Sender: TObject);
begin
     MenuTipoGrafica.Popup( BTipoGrafica.Left,
                            BTipoGrafica.Top+BTipoGrafica.Height);
end;






procedure TGrafica.MenuItem1Click(Sender: TObject);
begin
     with TMenuItem(Sender) do
     begin
          Chart.View3d := NOT Checked;
          Checked := NOT Checked;
     end;
end;

procedure TGrafica.MenuItem2Click(Sender: TObject);
begin
     with TMenuItem(Sender) do
     begin
          Chart.AnimatedZoom := Checked;
          Checked := NOT Checked;
     end;
end;

procedure TGrafica.NoMostrarEtiqueta1Click(Sender: TObject);
 var i: integer;
     lVisible : Boolean;
     oStyle : TSeriesMarksStyle;
begin
     with TMenuItem(Sender) do
     begin
          lVisible := MenuIndex > 0 ;
          oStyle := TSeriesMarksStyle(MenuIndex - 1);
          Checked := NOT Checked;
     end;
     for i := 0 to FColumnCount-1 do
     begin
          with Chart.Series[i].Marks do
          begin
               Visible := lVisible;
               Style :=  oStyle;
          end;
     end;
end;

procedure TGrafica.ToolButton2Click(Sender: TObject);
begin
     with Chart do
     begin
          if MaxPointsPerPage < FRecordCount then
          begin
               MaxPointsPerPage := MaxPointsPerPage + 1;
               SetMaxScroll(MaxPointsPerPage);
          end;
          BAlejar.Enabled := MaxPointsPerPage < FRecordCount;
          BAcercar.Enabled := TRUE;
     end;
end;

procedure TGrafica.FormShow(Sender: TObject);
begin
     lbDemo.Align := AlClient;
     SetLblDemo;
end;

procedure TGrafica.SetLblDemo;
begin
     lbDemo.Visible := not ZReportTools.ModuloGraficas;
end;

function TGrafica.GetDescripcion(const sTitulo: string): string;
begin
     if Trim(sTitulo) = ':' then
        Result := 'Descripci�n'
     else Result := sTitulo;
end;

procedure TGrafica.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key = 27  then
     begin
          Key := 0;
          Close;
     end;
end;

end.

