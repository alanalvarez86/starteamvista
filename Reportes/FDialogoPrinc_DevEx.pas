unit FDialogoPrinc_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
   TressMorado2013, dxSkinsDefaultPainters,
   cxButtons;

type
  TDialogoPrinc_DevEx = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Image1: TImage;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    BitBtn3_DevEx: TcxButton;
    procedure BitBtn3Click(Sender: TObject);
  private
    FBitacora : TStrings;
    procedure Informacion;
  public
    property Bitacora : TStrings read FBitacora write FBitacora;
  end;

var
  DialogoPrinc_DevEx: TDialogoPrinc_DevEx;

function ShowDialogoPrinc( const oBitacora : TStrings ) : Boolean;

implementation

uses ZetaCommonClasses, ZetaDialogo;
{$R *.DFM}

function ShowDialogoPrinc( const oBitacora : TStrings ) : Boolean;
begin
     if DialogoPrinc_DevEx = NIL then
        DialogoPrinc_DevEx := TDialogoPrinc_DevEx.Create(Application.Mainform);

     with DialogoPrinc_DevEx do
     begin
          Bitacora := oBitacora;
          ShowModal;
          Result := ModalResult = mrOk;
     end;
end;

procedure TDialogoPrinc_DevEx.Informacion;
 const MSG1 = 'Los siguientes datos NO son COMPATIBLES con la nueva Tabla Principal:';
begin
     ZInformation( Caption, MSG1 + CR_LF + FBitacora.Text, 0 );
end;

procedure TDialogoPrinc_DevEx.BitBtn3Click(Sender: TObject);
begin
     Informacion;
end;


end.
