unit ZReportModulo;

interface
uses Controls,SysUtils,
     ZReportTools,
     ZetaTipoEntidad,
     ZetaCommonLists;

{$define QUINCENALES}
{.$undefine QUINCENALES}
     
function GetCampoDescripcionGrupos( const FEntidadActiva, FEntidadGrupo : TipoEntidad;
                                    const oDiccion : TDiccionRecord ) : string;

procedure GetFechasRango( const eTipo: eRangoFechas;
                          var dInicial,dFinal : TDate );

function GetEntidadPoliza(const i:integer ) :TipoEntidad;
function GetItemPoliza(const Entidad : TipoEntidad ) : integer;

implementation

uses ZetaCommonClasses,
     DCliente;

function GetItemPoliza(const Entidad : TipoEntidad ) : integer;
begin
     case Entidad of
          enAusencia: Result := 1;
          enWorks: Result := 2
          else Result := 0;
     end;
end;

function GetEntidadPoliza(const i:integer ) :TipoEntidad;
begin
     case i of
          1: Result := enAusencia;
          2: Result := enWorks
          else Result := enNomina
     end;
end;

procedure GetFechasRango( const eTipo: eRangoFechas;
                          var dInicial,dFinal : TDate );
begin
     if eTipo = rfNominaActiva then
     begin
          with dmCliente.GetDatosPeriodoActivo do
          begin
               if (Trunc(Inicio) <= NullDateTime) then
               begin
                    dInicial := Date;
                    dFinal := Date;
               end
               else
               begin
                    dInicial := Inicio;
                    dFinal := Fin;
               end;
          end;
     {$ifndef QUINCENALES}
     end;
     {$else}
     end
     else if eTipo = rfAsistenciaActiva then
          begin
               with dmCliente.GetDatosPeriodoActivo do
               begin
                    if ( Trunc( InicioAsis ) <= NullDateTime) then
                    begin
                         dInicial := Date;
                         dFinal := Date;
                    end
                    else
                    begin
                         dInicial := InicioAsis;
                         dFinal := FinAsis;
                    end;
               end;
          end;
     {$endif}
end;

function GetCampoDescripcionGrupos( const FEntidadActiva, FEntidadGrupo : TipoEntidad;
                                    const oDiccion : TDiccionRecord ) : string;

 const aDesGrupos : array[1..{$ifdef ACS}15{$else}12{$endif},1..2] of string =
        (('COLABORA.CB_PUESTO','TPUESTO(1)'),
        ('COLABORA.CB_TURNO','TTURNO(1)'),
        ('COLABORA.CB_CLASIFI','T(13, COLABORA.CB_CLASIFI,1)'),
        ('COLABORA.CB_NIVEL1','TNIVEL(1,1)'),
        ('COLABORA.CB_NIVEL2','TNIVEL(2,1)'),
        ('COLABORA.CB_NIVEL3','TNIVEL(3,1)'),
        ('COLABORA.CB_NIVEL4','TNIVEL(4,1)'),
        ('COLABORA.CB_NIVEL5','TNIVEL(5,1)'),
        ('COLABORA.CB_NIVEL6','TNIVEL(6,1)'),
        ('COLABORA.CB_NIVEL7','TNIVEL(7,1)'),
        ('COLABORA.CB_NIVEL8','TNIVEL(8,1)'),
{$ifdef ACS}
        ('COLABORA.CB_NIVEL10','TNIVEL(10,1)'),
        ('COLABORA.CB_NIVEL11','TNIVEL(11,1)'),
        ('COLABORA.CB_NIVEL12','TNIVEL(12,1)'),
{$endif}
        ('COLABORA.CB_NIVEL9','TNIVEL(9,1)'));
 var lEncontrado : Boolean;
     sCampo : string;
     i : integer;
begin
     sCampo := oDiccion.DI_TABLA +'.'+ oDiccion.DI_NOMBRE;
     case FEntidadActiva of
          enNomina, enAusencia, enKarCurso, enKardex :
          begin
               lEncontrado := FALSE;
               if FEntidadGrupo = enEmpleado then
               begin
                    for i:=1 to 12 do
                        if (NOT lEncontrado) and (sCampo = aDesGrupos[i][1]) then
                        begin
                             Result := aDesGrupos[i][2];
                             lEncontrado := TRUE;
                        end;

                    if ( not lEncontrado ) AND
                       ( FEntidadActiva in [enKardex, enNomina] ) then
                    begin
                         if sCampo = 'COLABORA.CB_PATRON' then
                         begin
                              Result := 'REG_PATRONAL(COLABORA.CB_PATRON,1)';
                              lEncontrado := TRUE;
                         end;

                         if ( NOT lEncontrado ) AND
                            ( FEntidadActiva = enKardex ) then
                         begin
                              lEncontrado := TRUE;
                              if sCampo = 'COLABORA.CB_CONTRAT' then
                                 Result := 'T(9,COLABORA.CB_CONTRAT,1)'
                              else if sCampo = 'COLABORA.CB_MOT_BAJ' then
                                   Result := 'T(10,COLABORA.CB_MOT_BAJ,1)'
                              else if sCampo = 'COLABORA.CB_TABLASS' then
                                   Result := 'T(17,COLABORA.CB_TABLASS,1)'
                              else Result := GetCampoDescripcion( oDiccion );
                         end;
                    end;
               end;
               if NOT lEncontrado then
                  Result := GetCampoDescripcion( oDiccion );
          end;
          enVacacion :
          begin
               if sCampo = 'COLABORA.CB_TABLASS' then
                  Result := 'T(17,COLABORA.CB_TABLASS,1)'
               else Result := GetCampoDescripcion( oDiccion );
          end
          else Result := GetCampoDescripcion( oDiccion );
     end;
end;
end.
