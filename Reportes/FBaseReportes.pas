unit FBaseReportes;

interface
{$INCLUDE DEFINES.INC}
{$ifdef TRESS}
{$DEFINE MULTIPLES_ENTIDADES}
{$ENDIF}
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, Db, ZetaKeyCombo, DBCtrls,
  Printers,
  Mask, ZetaDBTextBox, ComCtrls, ZetaSmartLists, ZetaKeyLookup, ZetaEdit,
  ZetaFecha, ZetaClientDataset, CheckLst, Grids, DBGrids, ZetaDBGrid,
  ZReportTools,
  ZReportToolsConsts,
  ZetaTipoEntidad,
  ZetaCommonClasses,
  ZetaCommonLists;

type
  TBaseReportes = class(TZetaDlgModal)
    Panel1: TPanel;
    CortarBtn: TSpeedButton;
    CopiarBtn: TSpeedButton;
    PegarBtn: TSpeedButton;
    UndoBtn: TSpeedButton;
    BGuardarComo: TSpeedButton;
    BImprimir: TSpeedButton;
    PageControl: TPageControl;
    tsGenerales: TTabSheet;
    GBParametrosEncabezado: TGroupBox;
    lbNombreReporte: TLabel;
    Label17: TLabel;
    Label3: TLabel;
    lbCodigoReporte: TLabel;
    RE_CODIGO: TZetaDBTextBox;
    RE_NOMBRE: TDBEdit;
    RE_TITULO: TDBMemo;
    DataSource: TDataSource;
    tsFiltros: TTabSheet;
    PageControlFiltros: TPageControl;
    tsFiltroFechas: TTabSheet;
    ZetaSmartListsButton1: TZetaSmartListsButton;
    ZetaSmartListsButton2: TZetaSmartListsButton;
    RGBooleano: TGroupBox;
    RGSeleccionValores: TRadioGroup;
    GBRangoLista: TGroupBox;
    RBTodosRangoValores: TRadioButton;
    RBActivoRangoValores: TRadioButton;
    RBListaRangoValores: TRadioButton;
    CLBSeleccionValores: TCheckListBox;
    GBAbierto: TGroupBox;
    Label2: TLabel;
    EAbiertoFiltro: TMemo;
    GBCampoFecha: TGroupBox;
    lbFechaInicial: TLabel;
    lbFechaFinal: TLabel;
    EFechaInicial: TZetaFecha;
    EFechaFinal: TZetaFecha;
    GroupBox1: TGroupBox;
    LbFiltros: TZetaSmartListBox;
    BAgregaFiltro: TBitBtn;
    BBorraFiltro: TBitBtn;
    EFormulaFiltro: TEdit;
    GBRangoEntidad: TGroupBox;
    lbInicialRangoEntidad: TLabel;
    lbFinalRangoEntidad: TLabel;
    bFinalRangoEntidad: TSpeedButton;
    bInicialRangoEntidad: TSpeedButton;
    bListaRangoEntidad: TSpeedButton;
    RBTodosRangoEntidad: TRadioButton;
    RBActivoRangoEntidad: TRadioButton;
    RBRangoRangoEntidad: TRadioButton;
    RBListaRangoEntidad: TRadioButton;
    EInicialRangoEntidad: TZetaEdit;
    EFinalRangoEntidad: TZetaEdit;
    EListaRangoEntidad: TZetaEdit;
    tsFiltrosEspeciales: TTabSheet;
    lbCondicion: TLabel;
    lbFiltroUsuario: TLabel;
    BitBtn1: TBitBtn;
    QU_CODIGO: TZetaDBKeyLookup;
    SmartListFiltros: TZetaSmartLists;
    tsOrden: TTabSheet;
    GroupBox2: TGroupBox;
    LbOrden: TZetaSmartListBox;
    BAgregaOrden: TBitBtn;
    BBorraOrden: TBitBtn;
    BArribaOrden: TZetaSmartListsButton;
    BAbajoOrden: TZetaSmartListsButton;
    SmartListOrden: TZetaSmartLists;
    RE_FILTRO: TDBMemo;
    RE_CLASIFI: TComboBox;
    GroupBoxOrden: TGroupBox;
    eFormulaOrden: TMemo;
    bFormulaOrden: TBitBtn;
    CBRangoFechas: TZetaKeyCombo;
    ETituloOrden: TEdit;
    lTituloOrden: TLabel;
    lFormulaorden: TLabel;
    RE_CANDADOlbl: TLabel;
    RE_CANDADO: TDBCheckBox;
    bbFavoritos: TSpeedButton;
    StatusBarRep: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OKClick(Sender: TObject);
    procedure SmartListOrdenAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure Intercambia(Sender: TObject; var Objeto: TObject;
      Texto: String);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SmartListFiltrosAlSeleccionar(Sender: TObject;
      var Objeto: TObject; Texto: String);
    procedure RBTodosRangoEntidadClick(Sender: TObject);
    procedure RBActivoRangoEntidadClick(Sender: TObject);
    procedure RBRangoRangoEntidadClick(Sender: TObject);
    procedure RBListaRangoEntidadClick(Sender: TObject);
    procedure EFechaInicialExit(Sender: TObject);
    procedure EAbiertoFiltroExit(Sender: TObject);
    procedure RBTodosRangoValoresClick(Sender: TObject);
    procedure RBActivoRangoValoresClick(Sender: TObject);
    procedure RBListaRangoValoresClick(Sender: TObject);
    procedure CLBSeleccionValoresExit(Sender: TObject);
    procedure RGSeleccionValoresClick(Sender: TObject);
    procedure bInicialRangoEntidadClick(Sender: TObject);
    procedure bFinalRangoEntidadClick(Sender: TObject);
    procedure bListaRangoEntidadClick(Sender: TObject);
    procedure BBorraFiltroClick(Sender: TObject);
    procedure BBorraOrdenClick(Sender: TObject);
    procedure BAgregaOrdenClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure LbListaDblClick(Sender: TObject);
    procedure BGuardarComoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RE_NOMBREChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PageControlFiltrosChange(Sender: TObject);
    procedure RE_CLASIFIChange(Sender: TObject);
    procedure bFormulaOrdenClick(Sender: TObject);
    procedure CBRangoFechasChange(Sender: TObject);
    procedure CortarBtnClick(Sender: TObject);
    procedure CopiarBtnClick(Sender: TObject);
    procedure PegarBtnClick(Sender: TObject);
    procedure UndoBtnClick(Sender: TObject);
    procedure bbFavoritosClick(Sender: TObject);
  private
    FFirstControl: TWinControl;
    FIndexDerechos: Integer;
    FModo: TDatasetState;
    FReadOnly: Boolean;
    FSoloImpresion: Boolean;

    FSustituyeFiltro: Boolean;

    function DoConnect: Boolean;
    procedure DoDisconnect;
    procedure CancelarCambios;
    procedure SetModo( const Value: TDatasetState );
    procedure FocusFirstControl;
    function GetCampoRep: TZetaClientDataSet;
    function GetReporte: TZetaClientDataSet;
    procedure GuardarComo;
    function GetEntidad: TipoEntidad;
    procedure SetCaption;
    function GetPanelBar( lEstaEnClasifi: Boolean; const Clasifi: eClasifiReporte ): String;
  protected
    FFiltroEspecial: string;
    FConectando : Boolean;
    property IndexDerechos: Integer read FIndexDerechos write FIndexDerechos;
    property Modo: TDatasetState read FModo write SetModo;
    property FirstControl: TWinControl read FFirstControl write FFirstControl;
    property CampoRep : TZetaClientDataSet read GetCampoRep;
    property Reporte : TZetaClientDataSet read GetReporte;
    property Entidad : TipoEntidad read GetEntidad;

    function ClientDataset: TZetaClientDataset;
    procedure Connect; virtual;
    procedure Disconnect; virtual;
    procedure HabilitaControles; dynamic;

    procedure LLenaListas;virtual;
    procedure LimpiaListas;virtual;
    procedure IniciaListas;virtual;
    procedure PosicionaListas;virtual;
    procedure GrabaListas;virtual;
    procedure GrabaFiltros;
    procedure GrabaOrden;virtual;
    procedure GrabaCampoGeneral( const oCampo: TCampoMaster;
                                 const eTipo: eTipoCampo;
                                 const iPos, iSubPos: integer);
    procedure EscribirCambios;virtual;
    procedure AntesDeEscribirCambios;virtual;
    procedure AgregaListaCampo;virtual;
    procedure AgregaListaGrupo;virtual;
    procedure AgregaListaOrden;virtual;
    procedure AgregaListaEncabezado;virtual;
    procedure AgregaListaPieGrupo;virtual;
    procedure AgregaListaFiltro;virtual;
    procedure AgregaListaParametro;virtual;
    procedure GetDatosGenerales( oCampo : TCampoMaster );

    procedure AgregaOrden;
    procedure AgregaFiltro;
    procedure BorraDato( oLista: TZetaSmartLists;
                         oAgregar, oBorrar: TBitBtn;
                         oControl: TWinControl);
    //Metodos para ORDEN
    procedure ActualizaOrden(oOrden: TOrdenOpciones);virtual;
    procedure EnabledOrden(const bEnable, bFormula : Boolean );virtual;

    //Metodos para FILTROS
    procedure ActualizaFiltro(oFiltro: TFiltroOpciones);
    procedure EnabledFiltro(bEnable: Boolean);
    procedure ActualizaObjetoRangoEntidad(iTipoRango: eTipoRangoActivo);
    procedure ActualizaRangoEntidad(iTipoRango: eTipoRangoActivo; i: integer);
    procedure ActualizaListaRangoValores(i: integer);
    procedure ActualizaRangoValores(iTipoRango: eTipoRangoActivo;i: integer);
    procedure ActualizaObjetoRangoValores(iTipoRango: eTipoRangoActivo);
    procedure ActualizaFechasRango;

    function BuscaDialogoEntidad(iEntidad: integer; oEdit: TCustomEdit): Boolean;
    function ConstruyeFormula(var oMemo: TCustomMemo): Boolean;virtual;
    function GetFirstPestana: TTabSheet;virtual;

    procedure Error( oPagina: TTabSheet; oControl: TWinControl;
                     const sMensaje: string);

    procedure ImpresionReporte( const Tipo : TipoPantalla );virtual;
    procedure SetLookupCondicion;
    procedure RefrescarStatusBar;
  public
        property ReadOnly : Boolean read FReadOnly write FReadOnly ;
        property SoloImpresion: Boolean read FSoloImpresion write FSoloImpresion default FALSE;
        property FiltroEspecial : string read FFiltroEspecial write FFiltroEspecial;
        property SustituyeFiltro : Boolean read FSustituyeFiltro write FSustituyeFiltro;
        procedure Preview;virtual;
        procedure Imprime;virtual;
        procedure GeneraResultado; virtual;

  end;
  TBaseReportesClass = class of TBaseReportes;

var
  BaseReportes: TBaseReportes;

procedure ImprimirGrid( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado );
procedure ImprimirGridParams( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo: string; oParametros: TZetaParams );
procedure ExportarGrid( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado; const eSalida: eTipoFormato = tfXLS );
procedure ExportarGridParams( oGrid : TDBGrid; oDataSet : TDataSet; oParametros: TZetaParams; const sTitulo, sCodigo: string; const eSalida: eTipoFormato = tfXLS );


implementation

uses DCatalogos,
     DReportes,
     DCliente,
     DGlobal,
     DDiccionario,
     ZCerrarEdicion,
     ZetaDialogo,
     ZAccesosMgr,
     ZConstruyeFormula,
     ZGlobalTress,
     ZDiccionTools,
     ZReportConst,
     ZetaClientTools,
     ZetaCommonTools,
     ZQRReporteListado,
     FGuardarComo,
     FTressShell,
     dSistema;

{$R *.DFM}

{**************************************************}
//procedure ImprimirGrid( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado );
procedure ImprimirGridGeneral( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado; eSalida: eTipoFormato; lMostrarPreview: Boolean );
 var i : integer;
     oCampos : TStringList;
     oCampo : TCampoListado;
     oDatosImpresion : TDatosImpresion;
     sCaption : String;

     function GetMascara( const sDefault  : string) : string;
     begin
          Result := oCampo.Mascara;
          if StrVacio( Result ) then Result := sDefault;
     end;

begin
     if oDataSet.IsEmpty then
        raise Exception.Create('El Grid ' + sTitulo + ' est� Vac�o' );

     oCampos := TStringList.Create;
     try
        for i := 0 to oGrid.Columns.Count - 1 do
        begin
             if (oGrid.Fields[ i ] <> NIL) AND
                (oGrid.Columns[i].Visible) then
             begin
                  oCampo := TCampoListado.Create;
                  with oCampo do
                  begin
                       PosAgente := -1;
                       Formula     := oGrid.Columns[ i ].FieldName;
                       Titulo      := oGrid.Columns[ i ].Title.Caption;
                       Ancho       := oGrid.Fields[ i ].DisplayWidth;
                       Mascara     := oGrid.Fields[ i ].EditMask;
                       Operacion  := ocNinguno;

                       if (oGrid.Fields[ i ] is TNumericField) then
                       begin
                            if oGrid.Fields[ i ].DataType in [ ftSmallint,
                                                               ftInteger,
                                                               ftWord,
                                                               ftAutoInc ] then
                            begin
                                 Ancho := iMin( Ancho, 10 );
                                 TipoCampo := tgNumero;
                                 Mascara := GetMascara( K_MASCARA_INT );
                            end
                            else
                            begin
                                 Ancho := iMin( Ancho, 15 );
                                 TipoCampo := tgFloat;
                                 Operacion := ocSuma;
                                 Mascara := GetMascara( K_MASCARA_FLOAT );
                            end;
                       end
                       else if oGrid.Fields[ i ] is TDateTimeField then
                       begin
                            TipoCampo := tgFecha;
                            { Se usa ShortDateFormat dado que esto es lo que se
                            usa en TZetaClientDataset.MaskFecha }
                            Ancho := iMin( Ancho, 11 );
                            Mascara := GetMascara(  {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat );
                       end
                       else if oGrid.Fields[ i ] is TStringField then
                       begin
                            Ancho := iMin( Ancho, 50 );
                            TipoCampo := tgTexto;
                       end;
                       OPImp := Operacion;
                       TipoImp := TipoCampo;
                  end;

                  oCampos.AddObject( '', oCampo );
             end;
        end;
        try
           with oDatosImpresion do
           begin
                Tipo := eSalida;
                with Printer do
                begin
                     if Printers.Count = 0 then
                        Exception.Create('No Hay Impresoras Instaladas')
                     else
                     begin
                          PrinterIndex := -1;
                          Impresora := -1;
                     end;
                end;
                Copias := 1;
                PagInicial := 1;
                PagFinal := 999;
                //Ya no se usa la plantilla default, se
                // va a utilizar la plantilla interna
                //ver defecto 1107
                //Archivo := zReportTools.DirPlantilla+'DEFAULT.QR2';
                Separador := '';
           end;
           sCaption := 'Pantalla: ' + sTitulo;
           ZQRReporteListado.PreparaReporte;

           if (oDatosImpresion.Tipo = tfImpresora) then
           begin
                QRReporteListado.InitGrid( oDatosImpresion,
                                           sValor1, sValor2, sCaption, eValor1, eValor2 );
           end
           else
           begin
                QRReporteListado.InitExportacion( oDatosImpresion,
                                                  sValor1, sValor2, sCaption, eValor1, eValor2 );
           end;

           QRReporteListado.GeneraListado( sCaption,
                                           lMostrarPreview,
                                           FALSE,
                                           FALSE,
                                           FALSE,
                                           oDataSet,
                                           oCampos,
                                           NIL );

        finally
               ZQRReporteListado.DesPreparaReporte;
        end;
     finally
            if oCampos <> NIL then
            begin
                 for i := 0 to oCampos.Count -1 do
                     oCampos.Objects[i].Free;
                 oCampos.Free;
            end;
            TressShell.ReconectaMenu;
     end;
end;

function ParametroValido( oParam :TParam ): Boolean;
 //Son todos los valores activos del Sistema
 const Arreglo: array[1..13] of string = ( 'RegistroPatronal', 'IMSSYear',
                                    'IMSSMes', 'IMSSTipo', 'Year',
                                    'Tipo', 'Numero', 'FechaAsistencia',
                                    'FechaDefault', 'YearDefault', 'EmpleadoActivo',
                                    'NombreUsuario', 'CodigoEmpresa' );
 var
    i: integer;
begin
     Result := TRUE;
     for i:= Low(Arreglo) to High(Arreglo) do
     begin
          if Arreglo[i] = oParam.Name then
          begin
               Result := FALSE;
               Break;
          end;
     end;
end;

function GetDescripParametros( oParametros: TZetaParams ): string;
 var
    i: integer;
    oParam: TParam;
    sValor: string;
begin
     Result := VACIO;
     for i:= 0 to ( oParametros.Count - 1 ) do
     begin
          oParam := oParametros[i];
          with oParametros do
          begin
               case ParamByName( oParam.Name ).DataType of
                    ftInteger ,ftFloat:
                    begin
                         if (ParamByName( oParam.Name ).AsFloat <> 0) then
                            sValor := ParamByName( oParam.Name ).AsString
                         else
                             sValor := VACIO;
                    end;
                    ftBoolean: sValor := BoolAsSiNo( ParamByName( oParam.Name ).AsBoolean );
                    ftDate,ftDateTime: sValor := FechaCorta( ParamByName( oParam.Name ).AsDateTime )
                    else sValor := ParamByName( oParam.Name ).AsString;
               end;
          end;
          if StrLleno( sValor ) then
             Result := ConcatString( Result, oParam.Name + '=' + sValor, CR_LF );
     end;
end;

procedure ImprimirGridParams( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo: string; oParametros: TZetaParams );
 var
    sValor: string;
begin
     sValor := GetDescripParametros(oParametros);
     ImprimirGrid( oGrid, oDataSet, sTitulo, sCodigo, sValor, VACIO, stNinguno, stNinguno );
end;

procedure ImprimirGrid( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado );
begin
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor1, sValor2 , eValor1, eValor2 ,
                          tfImpresora, TRUE );
end;

procedure ExportarGrid( oGrid : TDBGrid; oDataSet : TDataSet;
                        const sTitulo, sCodigo,sValor1, sValor2 : string;
                        const eValor1, eValor2 : TipoEstado;
                        const eSalida: eTipoFormato = tfXLS );
begin
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor1, sValor2 , eValor1, eValor2 ,eSalida, FALSE );
end;

procedure ExportarGridParams( oGrid : TDBGrid; oDataSet : TDataSet; oParametros: TZetaParams; const sTitulo, sCodigo: string; const eSalida: eTipoFormato = tfXLS );
 var
    sValor : string;
begin
     sValor := GetDescripParametros(oParametros);
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor, VACIO , stNinguno, stNinguno, eSalida, FALSE );
end;

{TBaseReportes}
procedure TBaseReportes.FormShow(Sender: TObject);
begin
     inherited;
     dmDiccionario.GetListaClasifi(RE_CLASIFI.Items);

     {$ifdef RDD}
     with RE_CLASIFI do
          DropDownCount := Items.Count + 1;
     {$else}
     RE_CLASIFI.DropDownCount := Ord( High( eClasifiReporte ) ) + 1;
     {$endif}

     PageControl.ActivePage := GetFirstPestana;
     PageControlFiltros.ActivePage := tsFiltroFechas;
     RE_CLASIFI.Enabled := dmCliente.ModoTress;
     if DoConnect then FocusFirstControl;
     begin
          FocusFirstControl;
          RefrescarStatusBar;
     end;

end;

function TBaseReportes.GetFirstPestana : TTabSheet;
begin
     Result := tsFiltros;
end;

function TBaseReportes.DoConnect: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        try
           Connect;
           Result := True;
        except
              on Error: Exception do
              begin
                   ZetaDialogo.zExcepcion( '� Error Al Conectar Forma !', '� Ventana No Pudo Ser Conectada A Sus Datos !', Error, 0 );
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseReportes.DoDisconnect;
var
   oCursor: TCursor;
begin
     oCursor := Cursor;
     Cursor := crHourglass;
     try
        Disconnect;
     finally
            Cursor := oCursor;
     end;
end;

procedure TBaseReportes.Connect;
begin
     dmCatalogos.cdsCondiciones.Conectar;

     with dmReportes do
     begin
          cdsEditReporte.Conectar;
          DataSource.DataSet := cdsEditReporte;
          fConectando := cdsEditReporte.State <> dsInsert;
     end;

     SetLookupCondicion;

     LlenaListas;
     SetCaption;

     RE_CLASIFI.ItemIndex := dmDiccionario.GetPosClasifi( RE_CLASIFI.Items,
                                                          eClasifiReporte( Reporte.FieldByName( 'RE_CLASIFI' ).AsInteger ) );
     fConectando := FALSE;
end;

procedure TBaseReportes.SetLookupCondicion;
var
   lEnabled: Boolean;
begin
     {$ifdef MULTIPLES_ENTIDADES}
     lEnabled := Dentro( GetEntidad, EntidadConCondiciones );
     {$else}
     lEnabled := GetEntidad in EntidadConCondiciones;
     {$endif}

     {$IFDEF RDD}
     lEnabled := lEnabled or zStrToBool( Reporte.FieldByName('EN_NIVEL0').AsString );
     {$ENDIF}

     lbCondicion.Enabled := lEnabled;
     QU_CODIGO.Enabled := lEnabled;
end;

procedure TBaseReportes.SetCaption;
 var sNombre : string;
begin
     sNombre := RE_NOMBRE.Text;
     with Reporte do
     begin
          Caption := ObtieneElemento( lfTipoReporte, FieldByName('RE_TIPO').AsInteger ) + ': ' +
                  sNombre;
          if State in [dsEdit, dsInsert] then
             FieldByName('RE_TITULO').AsString := sNombre;
     end;
end;

procedure TBaseReportes.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBaseReportes.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Modo := dsInactive
          else
              Modo := Dataset.State;
     end;
end;

function TBaseReportes.ClientDataset: TZetaClientDataset;
begin
     Result := TZetaClientDataset( Datasource.Dataset );
end;

procedure TBaseReportes.HabilitaControles;
begin
     OK.Enabled := Modo in [ dsInsert, dsEdit ];
end;


procedure TBaseReportes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

procedure TBaseReportes.EscribirCambios;
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        if dmCliente.EsModoDemo then Exit;

        if Reporte.State <> dsEdit then
           Reporte.Edit;

        with RE_CLASIFI do
             Reporte.FieldByName('RE_CLASIFI').AsInteger := Integer(Items.Objects[ItemIndex]);
        AntesDeEscribirCambios;
        //Es el unico que puede hacer un EmptyDataSet
        CampoRep.EmptyDataSet;

        GrabaListas;

        with ClientDataset do
        begin
             Enviar;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseReportes.CancelarCambios;
begin
     with ClientDataset do
     begin
          Cancel;
     end;
end;

procedure TBaseReportes.SetModo(const Value: TDatasetState);
begin
     if ( FModo <> Value ) then
     begin
          if ( not ReadOnly ) AND( not FConectando ) then
          begin
               FModo := Value;
               HabilitaControles;
          end;
          bbFavoritos.Enabled := not ( dmReportes.ChecaFavoritos ) and ( dmReportes.cdsEditReporte.State <> dsInsert );
     end;
end;

procedure TBaseReportes.FocusFirstControl;
begin
     if Visible and ( FirstControl <> nil ) then
     begin
          with FirstControl do
          begin
               if Visible and Enabled then
                  SetFocus;
          end;
     end;
end;

procedure TBaseReportes.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
     if ( Shift = [ssCtrl] ) AND ( Chr(Key) = 'I' ) then
     begin
          Key := 0;
          ImpresionReporte(tgImpresora);
     end;
end;

procedure TBaseReportes.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
end;

procedure TBaseReportes.IniciaListas;
begin
     LBOrden.OnDblClick := LbListaDblClick;
     LBFiltros.OnDblClick := LbListaDblClick;
end;

procedure TBaseReportes.PosicionaListas;
begin
     SmartListOrden.SelectEscogido( 0 );
     SmartListFiltros.SelectEscogido( 0 );
end;

procedure TBaseReportes.LimpiaListas;
begin
     LBFiltros.Items.Clear;
     EnabledFiltro(FALSE);
     LBOrden.Items.Clear;
     EnabledOrden(FALSE, FALSE);
end;

procedure TBaseReportes.LLenaListas;
begin
     LimpiaListas;
     with CampoRep do
     begin
          First;
          while NOT eof do
          begin
               case eTipoCampo( CampoRep.FieldByName('CR_TIPO').AsInteger ) of
                    tcCampos : AgregaListaCampo;
                    tcGrupos : AgregaListaGrupo;
                    tcOrden : AgregaListaOrden;
                    tcEncabezado : AgregaListaEncabezado;
                    tcPieGrupo : AgregaListaPieGrupo;
                    tcFiltro: AgregaListaFiltro;
                    tcParametro: AgregaListaParametro;

               end;//case
               Next;
          end;
     end;
     PosicionaListas;
end;


procedure TBaseReportes.AgregaListaCampo;
begin

end;

procedure TBaseReportes.AgregaListaEncabezado;
begin

end;

procedure TBaseReportes.AgregaListaFiltro;
 var oFiltro : TFiltroOpciones;
begin
     oFiltro := GetFiltroOpciones( Camporep );
     GetDatosGenerales( oFiltro );
     LBFiltros.Items.AddObject( oFiltro.Titulo, oFiltro );
end;

procedure TBaseReportes.AgregaListaGrupo;
begin

end;

procedure TBaseReportes.AgregaListaOrden;
 var oOrden : TOrdenOpciones;
begin
     oOrden := TOrdenOpciones.Create;
     with oOrden do
     begin
          Calculado := CampoRep.FieldByName('CR_COLOR').AsInteger;
          DireccionOrden := CampoRep.FieldByName('CR_CALC').AsInteger;
     end;
     GetDatosGenerales( oOrden );
     LBOrden.Items.AddObject( oOrden.Titulo, oOrden );
end;

procedure TBaseReportes.AgregaListaParametro;
begin

end;

procedure TBaseReportes.AgregaListaPieGrupo;
begin
end;

procedure TBaseReportes.GetDatosGenerales( oCampo : TCampoMaster );
begin
     with oCampo do
     begin
          Entidad := TipoEntidad( CampoRep.FieldByName('CR_TABLA').AsInteger );
          Titulo := CampoRep.FieldByName('CR_TITULO').AsString;
          if Titulo = '' then Titulo := SINTITULO;
             Formula := CampoRep.FieldByName('CR_FORMULA').AsString;
     end;
end;

function TBaseReportes.GetCampoRep: TZetaClientDataSet;
begin
     Result := dmReportes.cdsCampoRep;
end;

function TBaseReportes.GetReporte: TZetaClientDataSet;
begin
     Result := dmReportes.cdsEditReporte;
end;

//Eventos y Manejo de las Listas

//Lista de Orden
procedure TBaseReportes.SmartListOrdenAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaOrden( TOrdenOpciones( Objeto ) );
          SmartListOrden.Control := LBOrden;
     end
     else EnabledOrden( FALSE, FALSE );

end;

procedure TBaseReportes.Intercambia(Sender: TObject; var Objeto: TObject;
  Texto: String);
begin
     inherited;
     Modo := dsEdit;
end;

procedure TBaseReportes.ActualizaOrden( oOrden : TOrdenOpciones );
begin
     ETituloOrden.Text := oOrden.Titulo;
     EFormulaOrden.Text := oOrden.Formula;
     EnabledOrden( TRUE, oOrden.Calculado <> 0 );
end;

procedure TBaseReportes.EnabledOrden( const bEnable, bFormula : Boolean  );
begin
     lTituloOrden.Enabled := bFormula;
     ETituloOrden.Enabled := bFormula;
     lFormulaOrden.Enabled := bFormula;
     EFormulaOrden.Enabled := bFormula;
     BBorraOrden.Enabled := bEnable;
     BFormulaOrden.Enabled := bEnable;
     if NOT bEnable then EFormulaOrden.Text := '';
end;


procedure TBaseReportes.GrabaCampoGeneral( const oCampo: TCampoMaster;
                                           const eTipo: eTipoCampo;
                                           const iPos, iSubPos: integer);
begin
     with oCampo, CampoRep do
     begin
          FieldByName('RE_CODIGO').AsInteger   := Reporte.FieldByName('RE_CODIGO').AsInteger;
          FieldByName('CR_TIPO').AsInteger     := Ord( eTipo );
          FieldByName('CR_POSICIO').AsInteger  := iPos;
          FieldByName('CR_SUBPOS').AsInteger   := iSubPos;
          FieldByName('CR_TABLA').AsInteger    := Ord( Entidad );
          FieldByName('CR_TITULO').AsString   := Copy( Titulo, 1, 30 );//El campo de CR_TITULO, deberia ser de 40
          FieldByName('CR_FORMULA').AsString  := Formula;
          FieldByName('CR_TFIELD').AsInteger   := Ord(TipoCampo);
     end;
end;

procedure TBaseReportes.GrabaFiltros;
 var i : integer;
begin
     with LBFiltros.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;
               GrabaCampoGeneral( TCampoMaster(Objects[i]),
               tcFiltro, i, -1 );

               with TFiltroOpciones(Objects[i]), CampoRep do
               begin
                    FieldByName('CR_CALC').AsInteger := Ord( TipoRango );
                    FieldByName('CR_ANCHO').AsInteger := Ord( TipoRangoActivo );
                    {$ifdef RDD}
                    FieldByName('CR_DESCRIP').Asinteger := Ord( ValorActivo );
                    {$else}
                    FieldByName('CR_DESCRIP').AsString := ValorActivo;
                    {$endif}
                    FieldByName('CR_OPER').AsInteger:= Numero;

                    case TipoRango of
                         rNinguno : FieldByName('CR_REQUIER').AsString := RangoInicial;
                         rFechas ://No se guardan las fechas
                                  FieldByName('CR_COLOR').AsInteger:= Ord(RangoFechas);

                         rBool : FieldByName('CR_REQUIER').AsInteger:= Posicion
                         else
                         begin
                              case TipoRangoActivo of
                                   raRango :
                                   begin
                                        FieldByName('CR_REQUIER').AsString := RangoInicial;
                                        FieldByName('CR_MASCARA').AsString := RangoFinal;
                                   end;
                                   raLista : FieldByName('CR_REQUIER').AsString := Lista;
                              end; //case
                         end//else
                    end;//Case
               end;//with
               CampoRep.Post;
          end;//for
end;

procedure TBaseReportes.GrabaOrden;
 var i: integer;
begin
     with LBOrden.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;

               GrabaCampoGeneral( TCampoMaster(Objects[i]),
                                  tcOrden, i, -1 );
               CampoRep.FieldByName('CR_COLOR').AsInteger := TOrdenOpciones(Objects[i]).Calculado;
               CampoRep.Post;
          end;
end;

procedure TBaseReportes.GrabaListas;
begin
     GrabaFiltros;
     GrabaOrden;
end;

procedure TBaseReportes.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     inherited;
     CanClose := True;
     if Modo in [ dsEdit, dsInsert ] then
     begin
          case ZCerrarEdicion.CierraEdicion of
               mrOk: EscribirCambios;
               mrIgnore: CancelarCambios;
          else
              CanClose := False;
          end;
     end
     else if dmReportes.cdsEditReporte.State = dsEdit then
          dmReportes.cdsEditReporte.Cancel;

end;

//Lista de FILTROS
procedure TBaseReportes.SmartListFiltrosAlSeleccionar(Sender: TObject;
  var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaFiltro( TFiltroOpciones( Objeto ) );
     end
     else EnabledFiltro( FALSE );

end;

procedure TBaseReportes.ActualizaFiltro( oFiltro : TFiltroOpciones );
 procedure ActualizaAbierto;
 begin
      with oFiltro do
      begin
           GbAbierto.Caption := oFiltro.Titulo;
           EAbiertoFiltro.Text := RangoInicial;
           if PageControl.ActivePage = tsFiltros then
              EAbiertoFiltro.SetFocus;
      end;
 end;

 procedure ActualizaLlaves;
 begin
      with oFiltro do
      begin
           GbRangoEntidad.Caption := oFiltro.Titulo;
           EInicialRangoEntidad.Text := RangoInicial;
           EFinalRangoEntidad.Text := RangoFinal;
           EListaRangoEntidad.Text := Lista;
      end;
      ActualizaRangoEntidad( oFiltro.TipoRangoActivo, lbFiltros.ItemIndex );
 end;

 procedure ActualizaStatus;
 begin
      ActualizaListaRangoValores( lbFiltros.ItemIndex );
 end;

 procedure ActualizaFechas;
 begin
      with oFiltro do
      begin
           GbCampoFecha.Caption := oFiltro.Titulo;
           CBRangoFechas.ItemIndex := Ord(RangoFechas);
           EFechaInicial.Valor := FechaInicial;
           EFechaFinal.Valor := FechaFinal;
      end;
 end;

 procedure ActualizaBool;
 begin
      with oFiltro do
      begin
           RGBooleano.Caption := oFiltro.Titulo;
           RGSeleccionValores.ItemIndex := Posicion;
      end;
 end;

 procedure FiltroActivo;
 begin
      with LbFiltros.Items do
           //with TFiltroOpciones( Objects[ LBFiltros.ItemIndex ] ) do
           with oFiltro do
           begin
                GBRangoEntidad.Visible := TipoRango = rRangoEntidad;
                GBRangoLista.Visible := TipoRango = rRangoListas;
                GBCampoFecha.Visible := TipoRango = rFechas;
                GbAbierto.Visible := TipoRango = rNinguno;
                RgBooleano.Visible := TipoRango = rBool;
           end;
 end;

begin
     FiltroActivo;
     BBorraFiltro.Enabled := TRUE;

     EFormulaFiltro.Text := oFiltro.Formula;
     with LbFiltros do
          case oFiltro.TipoRango of
               rNinguno : ActualizaAbierto;
               rRangoEntidad: ActualizaLlaves;
               rRangoListas: ActualizaStatus;
               rFechas: ActualizaFechas;
               rBool: ActualizaBool;
          end;
end;

procedure TBaseReportes.EnabledFiltro( bEnable : Boolean );
begin
     GBRangoEntidad.Visible := bEnable;
     GBRangoLista.Visible := bEnable;
     GBCampoFecha.Visible := bEnable;
     GbAbierto.Visible := bEnable;
     RgBooleano.Visible := bEnable;

     if NOT bEnable then EFormulaFiltro.Text := '';
end;

procedure TBaseReportes.RBTodosRangoEntidadClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoEntidad( raTodos, LbFiltros.ItemIndex );
     ActualizaObjetoRangoEntidad( raTodos );
end;

procedure TBaseReportes.RBActivoRangoEntidadClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoEntidad( raActivo, lbFiltros.ItemIndex );
     ActualizaObjetoRangoEntidad( raActivo );
end;

procedure TBaseReportes.RBRangoRangoEntidadClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoEntidad( raRango, lbFiltros.ItemIndex );
     ActualizaObjetoRangoEntidad( raRango );
end;

procedure TBaseReportes.RBListaRangoEntidadClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoEntidad( raLista, lbFiltros.ItemIndex );
     ActualizaObjetoRangoEntidad( raLista );
end;

procedure TBaseReportes.ActualizaRangoEntidad(iTipoRango : eTipoRangoActivo; i : integer );
 var oCampo : TFiltroOpciones;
begin
     //Todos
     RBTodosRangoEntidad.Enabled := TRUE;
     RBTodosRangoEntidad.Checked := iTipoRango = raTodos;

     //Activo
     oCampo := TFiltroOpciones( LbFiltros.Items.Objects[ i ] );
     {$ifdef RDD}
     RBActivoRangoEntidad.Enabled := oCampo.ValorActivo <> vaSinValor;
     {$else}
     RBActivoRangoEntidad.Enabled := StrLleno( oCampo.ValorActivo ) ;
     {$endif}
     RBActivoRangoEntidad.Checked := iTipoRango = raActivo;

     //Rango
     RBRangoRangoEntidad.Enabled := TRUE;
     RBRangoRangoEntidad.Checked := iTipoRango = raRango;
     lbInicialRangoEntidad.Enabled := iTipoRango = raRango;
     EInicialRangoEntidad.Enabled := iTipoRango = raRango;
     bInicialRangoEntidad.Enabled := ( iTipoRango = raRango ) AND ( oCampo.Numero >= 0 );
     lbFinalRangoEntidad.Enabled := iTipoRango = raRango;
     EFinalRangoEntidad.Enabled := iTipoRango = raRango;
     bFinalRangoEntidad.Enabled := ( iTipoRango = raRango ) AND ( oCampo.Numero >= 0 );

     //Lista
     RBListaRangoEntidad.Enabled := TRUE;
     RBListaRangoEntidad.Checked := iTipoRango = raLista;
     eListaRangoEntidad.Enabled := iTipoRango = raLista;
     bListaRangoEntidad.Enabled := ( iTipoRango = raLista ) AND ( oCampo.Numero >= 0 );
end;

procedure TBaseReportes.ActualizaObjetoRangoEntidad( iTipoRango : eTipoRangoActivo );
 var i : integer;
     lCambio : Boolean;
begin
     i := lbFiltros.ItemIndex;
     with TFiltroOpciones( lbFiltros.Items.Objects[ i ] ) do
     begin
          lCambio := ( TipoRangoActivo <> iTipoRango ) OR
                     ( RangoInicial <> EInicialRangoEntidad.Text ) OR
                     ( RangoFinal <> EFinalRangoEntidad.Text ) OR
                     ( Lista <> EListaRangoEntidad.Text );

          if lCambio then
          begin
               TipoRangoActivo := iTipoRango;
               RangoInicial := EInicialRangoEntidad.Text;
               RangoFinal := EFinalRangoEntidad.Text;
               Lista := EListaRangoEntidad.Text;
               Modo := dsEdit;
          end;
     end;
end;

procedure TBaseReportes.ActualizaListaRangoValores(i : integer);

  function DesactivarElementosListaClasificacionPeriodo ( oListBox: TCheckListBox; const iLista: Integer ): String;
  var
     i : Integer;
  begin
       if StrLleno( GlobalListaConfidencialidad ) then
           with oListBox do
           begin
                for i := 0 to ( Items.Count - 1 ) do
                begin
                     oListBox.ItemEnabled[i] := False;
                end;
                for i := 0 to FArregloPeriodoConfidencial.Count - 1 do
                begin
                     case GetClasificacionPeriodo( FListaTiposPeriodoConfidencialidad , StrToInt( FArregloPeriodoConfidencial.Names[i] ) ) of
                        tpDiario: oListBox.ItemEnabled[ Ord( tpDiario ) ] := True;
                        tpSemanal: oListBox.ItemEnabled[ Ord( tpSemanal ) ] := True;
                        tpCatorcenal: oListBox.ItemEnabled[ Ord( tpCatorcenal ) ] := True;
                        tpQuincenal: oListBox.ItemEnabled[ Ord( tpQuincenal ) ] := True;
                        tpMensual: oListBox.ItemEnabled[ Ord( tpMensual ) ] := True;
                        tpDecenal: oListBox.ItemEnabled[ Ord( tpDecenal ) ] := True;
                     end;
                end;
           end;
  end;


begin
     with TFiltroOpciones( lbFiltros.Items.Objects[ i ] ) do
     begin
          GbRangoLista.Caption := Titulo;
          ActualizaRangoValores( TipoRangoActivo, i );
          dmDiccionario.LlenaLista( Numero,  CLBSeleccionValores.Items, {$ifdef RDD} FALSE {$else} TRUE {$endif} );
          DecodeTexto( CLBSeleccionValores, Lista, Numero );
          if Numero = Ord( lfTipoPeriodo ) then
              DesactivarElementosListaClasificacionPeriodo( CLBSeleccionValores,  Numero );
     end;
end;

procedure TBaseReportes.EFechaInicialExit(Sender: TObject);
begin
     inherited;
     with lbFiltros do
          with TFiltroOpciones( Items.Objects[ ItemIndex ] ) do
          begin
               FechaInicial := EFechaInicial.Valor;
               FechaFinal := EFechaFinal.Valor;
          end;
end;

procedure TBaseReportes.EAbiertoFiltroExit(Sender: TObject);
begin
     inherited;
     with TFiltroOpciones( lbFiltros.Items.Objects[ lbFiltros.ItemIndex ] ) do
          if RangoInicial <> EAbiertoFiltro.Text then
          begin
               RangoInicial:= EAbiertoFiltro.Text;
               Modo := dsEdit;
          end;

end;

procedure TBaseReportes.RBTodosRangoValoresClick(Sender: TObject);
begin
  inherited;
  ActualizaRangoValores( raTodos, lbFiltros.ItemIndex );
  ActualizaObjetoRangoValores( raTodos );
  Modo := dsEdit;
end;

procedure TBaseReportes.RBActivoRangoValoresClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoValores( raActivo, lbFiltros.ItemIndex );
     ActualizaObjetoRangoValores( raActivo );
     Modo := dsEdit;
end;

procedure TBaseReportes.RBListaRangoValoresClick(Sender: TObject);
begin
     inherited;
     ActualizaRangoValores( raLista, lbFiltros.ItemIndex );
     ActualizaObjetoRangoValores( raLista );
     Modo := dsEdit;
end;

procedure TBaseReportes.ActualizaObjetoRangoValores( iTipoRango : eTipoRangoActivo );
 var i : integer;
begin
     i := lbFiltros.ItemIndex;
     with TFiltroOpciones( lbFiltros.Items.Objects[ i ] ) do
     begin
          TipoRangoActivo := iTipoRango;
     end;
end;

procedure TBaseReportes.CLBSeleccionValoresExit(Sender: TObject);
begin
     inherited;
     with TFiltroOpciones( lbFiltros.Items.Objects[ lbFiltros.ItemIndex ] ) do
          Lista := DecodeLista( CLBSeleccionValores, Numero );
     Modo := dsEdit;
end;

procedure TBaseReportes.ActualizaRangoValores(iTipoRango : eTipoRangoActivo; i : integer );
begin
     //Todos
     RBTodosRangoValores.Enabled := TRUE;
     RBTodosRangoValores.Checked := iTipoRango = raTodos;

     //Activo
     {$ifdef RDD}
     RBActivoRangoValores.Enabled := TFiltroOpciones( lbFiltros.Items.Objects[ i ] ).ValorActivo <> vaSinValor;
     {$else}
     RBActivoRangoValores.Enabled := TFiltroOpciones( lbFiltros.Items.Objects[ i ] ).ValorActivo <> VACIO;
     {$endif}

     RBActivoRangoValores.Checked := iTipoRango = raActivo;

     //Lista
     RBListaRangoValores.Enabled := TRUE;
     RBListaRangoValores.Checked := iTipoRango = raLista;
     CLBSeleccionValores.Enabled := iTipoRango = raLista;
end;

procedure TBaseReportes.RGSeleccionValoresClick(Sender: TObject);
begin
     inherited;
     with lbFiltros do
          TFiltroOpciones( Items.Objects[ ItemIndex ] ).Posicion := RGSeleccionValores.ItemIndex;
     Modo := dsEdit;
end;

procedure TBaseReportes.bInicialRangoEntidadClick(Sender: TObject);
begin
     inherited;
     with lbFiltros do
          if BuscaDialogoEntidad( TFiltroOpciones(Items.Objects[ ItemIndex ]).Numero, EInicialRangoEntidad ) then
             ActualizaObjetoRangoEntidad( raRango );

end;

procedure TBaseReportes.bFinalRangoEntidadClick(Sender: TObject);
begin
     inherited;
     with lbFiltros do
          if BuscaDialogoEntidad( TFiltroOpciones(Items.Objects[ ItemIndex ]).Numero,
                                  EFinalRangoEntidad ) then
             ActualizaObjetoRangoEntidad( raRango );

end;

function TBaseReportes.BuscaDialogoEntidad( iEntidad : integer; oEdit : TCustomEdit ) : Boolean;
 var sLlave, sDescripcion : string;
begin
     sLlave := oEdit.Text;
     Result := TressShell.BuscaDialogo( TipoEntidad( iEntidad ), '', sLLave, sDescripcion );
     if Result then
     begin
          oEdit.Text := sLlave;
          Modo := dsEdit;
     end;
end;

procedure TBaseReportes.bListaRangoEntidadClick(Sender: TObject);
 var sLlave,sDescripcion : string;
     iNumero : integer;
begin
     inherited;
     with lbFiltros do
          iNumero := TFiltroOpciones(Items.Objects[ ItemIndex ]).Numero;
     if TressShell.BuscaDialogo( TipoEntidad( iNumero ), '', sLlave, sDescripcion ) then
     begin
          if StrLleno( EListaRangoEntidad.Text ) then
             EListaRangoEntidad.Text := EListaRangoEntidad.Text + ','+ sLlave
          else EListaRangoEntidad.Text := sLlave;
          ActualizaObjetoRangoEntidad( raLista );
     end;

end;

procedure TBaseReportes.BorraDato( oLista : TZetaSmartLists;
                                   oAgregar, oBorrar : TBitBtn;
                                   oControl : TWinControl );
 var i : integer;
     oCampo : TCampoMaster;
begin
     i := oLista.ListaDisponibles.ItemIndex;
     if i >= 0 then
     begin
          oCampo := TCampoMaster(oLista.ListaDisponibles.Items.Objects[ i ]);
          oCampo.Free;
          oLista.ListaDisponibles.Items.Delete(i);
     end;

     oBorrar.Enabled := oLista.ListaDisponibles.Items.Count > 0;

     if oBorrar.Enabled then
     begin
          if i >= oLista.ListaDisponibles.Items.Count then i := oLista.ListaDisponibles.Items.Count - 1;
          oLista.SelectEscogido( i );
          if oControl.Enabled then oControl.SetFocus
          else oLista.ListaDisponibles.SetFocus;
     end
     else oAgregar.SetFocus;

     Modo := dsEdit;
end;


procedure TBaseReportes.BBorraFiltroClick(Sender: TObject);
begin
     inherited;
     with TBitBtn(Sender) do
     begin
          SetFocus;
          if Tag = 0 then
          begin
               AgregaFiltro;
          end
          else
          begin
               BorraDato( SmartListFiltros, bAgregaFiltro, bBorraFiltro, LBFiltros );
               with LBFiltros.Items do
               begin
                    EnabledFiltro( Count > 0 );
                    if Count > 0 then
                       ActualizaFiltro( TFiltroOpciones( Objects[LBFiltros.ItemIndex] ) );
               end;
          end;
     end;
end;

procedure TBaseReportes.BBorraOrdenClick(Sender: TObject);
begin
     inherited;
     BorraDato( SmartListOrden, bAgregaOrden, bBorraOrden, bAgregaOrden );
     if LbOrden.Items.Count = 0 then
        EnabledOrden( FALSE, FALSE );
end;


procedure TBaseReportes.BAgregaOrdenClick(Sender: TObject);
begin
     inherited;
     AgregaOrden;
end;

procedure TBaseReportes.AgregaOrden;
 var oOrden : TOrdenOpciones;
     oDiccion : TDiccionRecord;
begin
     if AntesDeAgregarDato( Entidad, FALSE, FALSE, oDiccion, SmartListOrden ) then
     begin
          oOrden := TOrdenOpciones.Create;
          DiccionToCampoMaster(oDiccion,oOrden);
          DespuesDeAgregarDato( oOrden,
                                SmartListOrden,
                                BBorraOrden,
                                BAgregaOrden );
          Modo := dsEdit;
     end;
end;

procedure TBaseReportes.AgregaFiltro;
 var oFiltro : TFiltroOpciones;
     oDiccion : TDiccionRecord;
begin
     if AntesDeAgregarDato( Entidad, FALSE, FALSE, oDiccion, SmartListFiltros ) then
     begin
          oFiltro := TFiltroOpciones.Create;
          DiccionToCampoMaster(oDiccion, oFiltro);
          with oFiltro do
          begin
               TipoRangoActivo := eTipoRangoActivo( oDiccion.DI_RANGOAC );   //eTipoRangoActivo = (raTodos,raActivo,raRango,raLista);
               ValorActivo := oDiccion.DI_VALORAC;
               Numero := oDiccion.DI_NUMERO;
               TipoRango := eTipoRango(oDiccion.DI_TRANGO);  //eTipoRango = (rNinguno,rRangoEntidad,rRangoListas, rFechas, rBool );
               {$ifdef RDD}
               case TipoRango of
                    rNinguno :
                    begin
                         case oFiltro.TipoCampo of
                              tgNumero, tgFloat : RangoInicial := ObtieneElemento( lfRDDDefaultsFloat, iMin( oDiccion.DI_RANGOAC, Ord( High(eRDDDefaultsFloat))) );
                              tgTexto :  RangoInicial := ObtieneElemento( lfRDDDefaultsTexto, iMin( oDiccion.DI_RANGOAC, Ord( High(eRDDDefaultsTexto))) );
                         end;
                    end;
                    rFechas :
                    begin
                         RangoFechas := eRangoFechas(oDiccion.DI_RANGOAC);
                         ZReportTools.GetFechasRango( RangoFechas, FechaInicial,FechaFinal);
                    end;
                    rBool :
                    begin
                         Posicion:= oDiccion.DI_RANGOAC;
                    end;
               end;
               {$else}
               case TipoRango of
                    rNinguno : RangoInicial := ValorActivo;
                    rFechas :
                    begin
                         RangoFechas := rfHoy;
                         FechaInicial := dmCliente.FechaDefault;
                         FechaFinal := FechaInicial;
                    end;
                    rBool :
                    begin
                         if ValorActivo = K_GLOBAL_SI then Posicion:=0
                         else Posicion := 1;
                    end;
               end;
               {$endif}

          end;

          DespuesDeAgregarDato( oFiltro,
                                SmartListFiltros,
                                BBorraFiltro,
                                LBFiltros );
          Modo := dsEdit;
     end;
end;

procedure TBaseReportes.BitBtn1Click(Sender: TObject);
begin
     inherited;
     if ConstruyeFormula( TCustomMemo(RE_FILTRO)) then
     begin
          Reporte.FieldByName('RE_FILTRO').AsString := Re_Filtro.Text;
     end
end;

function TBaseReportes.ConstruyeFormula( var oMemo : TCustomMemo ) : Boolean;
 var sFormula : string;
begin
     sFormula := oMemo.Text;
     Result := ZConstruyeFormula.GetFormulaConstruye( Entidad, sFormula, oMemo.SelStart, evReporte );
     if Result then
     begin
          Reporte.Edit;
          Modo := dsEdit;
          oMemo.Text := sFormula;
     end;
end;

procedure TBaseReportes.LbListaDblClick(Sender: TObject);
begin
     inherited;
     //NO HACE NADA - NO QUITAR!!!
end;

procedure TBaseReportes.BGuardarComoClick(Sender: TObject);
begin
     inherited;
     GuardarComo;
end;

procedure TBaseReportes.GuardarComo;
 var
     Arreglo : array of OleVariant;
     i, iCodigo, iCount : integer;
     sNombre : string;
begin
     if dmCliente.EsModoDemo then Exit;

     if Reporte.FieldByName('RE_NOMBRE').AsString = VACIO then
     begin
          ZetaDialogo.ZError( Caption, 'Nombre del Reporte est� vac�o', 0 );
          PageControl.ActivePage := tsGenerales;
          RE_NOMBRE.SetFocus;
     end
     else
     begin
          sNombre := 'Copia de ' + Reporte.FieldByName('RE_NOMBRE').AsString;
          if FGuardarComo.DialogoGuardar(sNombre) then
          begin
               if Reporte.State = dsBrowse then Reporte.Edit;
               CampoRep.EmptyDataSet;
               dmSistema.cdsSuscrip.EmptyDataset;

               iCount := Reporte.FieldCount;
               SetLength( Arreglo, iCount );

               for i:= 0 to iCount - 1 do
                    Arreglo[i] := Reporte.Fields[i].Value;

               Reporte.Cancel;

               iCodigo := Reporte.FieldByName('RE_CODIGO').Index;
               Reporte.Append;
               for i:= 0 to iCount - 1 do
                   if i <> iCodigo then
                      Reporte.Fields[i].Value:=Arreglo[i];
               Reporte.FieldByName('RE_NOMBRE').AsString := sNombre;
               Reporte.Post;

               try
                  EscribirCambios;
                  SetCaption;
               except
                     raise;
               end;
          end
     end;
end;

procedure TBaseReportes.AntesDeEscribirCambios;
begin

end;

function TBaseReportes.GetEntidad: TipoEntidad;
begin
     Result := TipoEntidad(Reporte.FieldByName('RE_ENTIDAD').AsInteger);
end;

procedure TBaseReportes.FormCreate(Sender: TObject);
begin
     inherited;
     FFiltroEspecial := '';
     IniciaListas;
     PageControl.ActivePage := tsFiltros;

     QU_CODIGO.LookupDataSet := dmCatalogos.cdsCondiciones;

end;

procedure TBaseReportes.RE_NOMBREChange(Sender: TObject);
begin
     inherited;
     SetCaption;
end;

procedure TBaseReportes.ImpresionReporte(const Tipo : TipoPantalla);
begin
     //La Clases Hijas Implementan esta Metodo
end;

procedure TBaseReportes.FormDestroy(Sender: TObject);
begin
     with LBFiltros do
          while Items.Count > 0 do
          begin
                TFiltroOpciones(Items.Objects[0]).Free;
                Items.Delete(0);
          end;

     with LBOrden do
          while Items.Count > 0 do
          begin
                TOrdenOpciones(Items.Objects[0]).Free;
                Items.Delete(0);
          end;
     inherited;

end;

procedure TBaseReportes.Error( oPagina : TTabSheet; oControl : TWinControl;
                               const sMensaje : string );
begin
     PageControl.ActivePage := oPagina;
     ActiveControl := oControl;
     raise Exception.Create( sMensaje );
end;

procedure TBaseReportes.Preview;
begin

end;

procedure TBaseReportes.Imprime;
begin

end;

procedure TBaseReportes.GeneraResultado;
begin

end;



procedure TBaseReportes.PageControlFiltrosChange(Sender: TObject);
begin
     inherited;
     if PageControlFiltros.ActivePage = tsFiltrosEspeciales then
        if QU_CODIGO.Enabled then
           QU_CODIGO.SetFocus
        else RE_FILTRO.SetFocus;
end;

procedure TBaseReportes.RE_CLASIFIChange(Sender: TObject);
begin
     inherited;
     Modo := dsEdit;
end;

procedure TBaseReportes.bFormulaOrdenClick(Sender: TObject);
begin
     inherited;
     if ConstruyeFormula( TCustomMemo(eFormulaOrden) ) then
     begin
          with LBOrden,TOrdenOpciones( Items.Objects[ ItemIndex] ) do
          begin
               Formula := eFormulaOrden.Text;
               if Calculado >= 0 then
                  Calculado := -1;
               SmartListOrden.SelectEscogido( ItemIndex );
          end;
     end;

end;

procedure TBaseReportes.CBRangoFechasChange(Sender: TObject);
begin
     inherited;
     ActualizaFechasRango;
end;
procedure TBaseReportes.ActualizaFechasRango;
 var dInicial, dFinal : TDate;
begin

     ZReportTools.GetFechasRango( eRangoFechas( CBRangoFechas.ItemIndex ),
                                    dInicial,dFinal);
     //EFechaInicial.Valor := dInicial;
     //EFechaFinal.Valor := dFinal;

     with LBFiltros,Items do
     begin
          with TFiltroOpciones( Objects[ItemIndex] ) do
          begin
               FechaInicial := dInicial;
               FechaFinal := dFinal;
               RangoFechas := eRangoFechas( CBRangoFechas.ItemIndex );
          end;
          ActualizaFiltro( TFiltroOpciones( Objects[ItemIndex] ) );
     end;
     Modo := dsEdit;

end;

procedure TBaseReportes.CortarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_CUT, 0, 0 );
end;

procedure TBaseReportes.CopiarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_COPY, 0, 0 );
end;

procedure TBaseReportes.PegarBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_PASTE, 0, 0 );
end;

procedure TBaseReportes.UndoBtnClick(Sender: TObject);
begin
     inherited;
     SendMessage( ActiveControl.Handle, WM_UNDO, 0, 0 );
end;

procedure TBaseReportes.bbFavoritosClick(Sender: TObject);
begin
     inherited;
     if ( dmReportes.AgregaFavoritos( Reporte ) ) then
        ZetaDialogo.ZInformation( Caption,
                                  Format( 'Reporte "%s" Agregado a Mis Favoritos', [ Reporte.FieldByName( 'RE_NOMBRE' ).AsString ] ),
                                  0 );
     RefrescarStatusBar;
end;
                                               
function TBaseReportes.GetPanelBar(lEstaEnClasifi: Boolean; const Clasifi: eClasifiReporte ): String;
begin
     if ( lEstaEnClasifi ) then
        Result:= dmReportes.ObtieneClasificacion(Clasifi);
end;

procedure TBaseReportes.RefrescarStatusBar;
const
     aReporteEsta: array [FALSE..TRUE] of PChar = (VACIO, 'Este Reporte est� en');
begin
     with dmReportes, StatusBarRep do
     begin
          if cdsReportes.Active then
          begin
               Panels[1].Text:= GetPanelBar( EstaEnClasifi( cdsReportes.FieldByName('RE_CODIGO').AsInteger, 'US_FAVORITO' ), crFavoritos );
               Panels[2].Text:= GetPanelBar( EstaEnClasifi( cdsReportes.FieldByName('RE_CODIGO').AsInteger, 'US_SUSCRITO' ), crSuscripciones );
               Panels[0].Text:= aReporteEsta[ StrLleno( Panels[1].Text ) or StrLleno( Panels[2].Text ) ];
          end;
     end;
end;
end.






