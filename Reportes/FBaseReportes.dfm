inherited BaseReportes: TBaseReportes
  Top = 220
  BorderStyle = bsSingle
  Caption = 'BaseReportes'
  ClientHeight = 434
  ClientWidth = 544
  OldCreateOrder = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel [0]
    Left = 0
    Top = 0
    Width = 544
    Height = 30
    Align = alTop
    TabOrder = 1
    object CortarBtn: TSpeedButton
      Left = 95
      Top = 3
      Width = 24
      Height = 24
      Hint = 'Cortar ( Ctrl-X)'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000800000B7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCF800000B7AFCFB7AFCF800000B7AFCFB7AFCF800000800000B7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000B7AFCFB7AFCF80
        0000B7AFCF800000B7AFCFB7AFCF800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCF800000B7AFCFB7AFCF800000B7AFCF800000B7AFCFB7AFCF8000
        00B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80000080000080
        0000B7AFCF800000B7AFCFB7AFCF800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000B7AFCF800000800000800000B7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80
        0000000000800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF00
        0000000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCF000000B7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF00000000
        0000B7AFCF000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCF000000B7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000B7
        AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCF000000B7AFCFB7AFCFB7AFCF000000B7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF}
      ParentShowHint = False
      ShowHint = True
      OnClick = CortarBtnClick
    end
    object CopiarBtn: TSpeedButton
      Left = 119
      Top = 3
      Width = 24
      Height = 24
      Hint = 'Copiar ( Ctrl-C)'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80
        0000800000800000800000800000800000800000800000800000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF800000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80
        0000FFFFFF000000000000000000000000000000FFFFFF800000B7AFCF000000
        000000000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF800000B7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
        0000FFFFFF000000000000000000000000000000FFFFFF800000B7AFCF000000
        FFFFFF000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF800000B7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
        0000FFFFFF000000000000FFFFFF800000800000800000800000B7AFCF000000
        FFFFFF000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFF8000
        00DCD8E7800000B7AFCFB7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
        0000FFFFFFFFFFFFFFFFFFFFFFFF800000800000B7AFCFB7AFCFB7AFCF000000
        FFFFFF000000000000FFFFFF0000008000008000008000008000008000008000
        00B7AFCFB7AFCFB7AFCFB7AFCF000000FFFFFFFFFFFFFFFFFFFFFFFF000000DC
        D8E7000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF000000
        FFFFFFFFFFFFFFFFFFFFFFFF000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCF000000000000000000000000000000000000B7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF}
      ParentShowHint = False
      ShowHint = True
      OnClick = CopiarBtnClick
    end
    object PegarBtn: TSpeedButton
      Left = 143
      Top = 3
      Width = 24
      Height = 24
      Hint = 'Pegar ( Ctrl-V)'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000B7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF80000080
        0000800000800000800000800000800000800000800000800000B7AFCF000000
        000000000000000000000000800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF80000000000070609F00808070609F00808070609F800000FF
        FFFF000000000000000000000000000000000000FFFFFF800000000000008080
        70609F00808070609F008080800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF80000000000070609F00808070609F00808070609F800000FF
        FFFF000000000000000000FFFFFF800000800000800000800000000000008080
        70609F00808070609F008080800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8000
        00DCD8E7800000B7AFCF00000070609F00808070609F00808070609F800000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF800000800000B7AFCFB7AFCF000000008080
        70609F00808070609F0080808000008000008000008000008000008000008000
        00000000B7AFCFB7AFCF00000070609F00808070609F00808070609F00808070
        609F00808070609F00808070609F008080000000B7AFCFB7AFCF000000008080
        70609F00000000000000000000000000000000000000000000000070609F7060
        9F000000B7AFCFB7AFCF00000070609F70609F000000B7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCF00000070609F008080000000B7AFCFB7AFCF000000008080
        70609F00808000000000FFFF00000000000000FFFF00000070609F0080807060
        9F000000B7AFCFB7AFCFB7AFCF00000000000000000000000000000000FFFF00
        FFFF000000000000000000000000000000B7AFCFB7AFCFB7AFCFB7AFCFB7AFCF
        B7AFCFB7AFCFB7AFCF000000000000000000000000B7AFCFB7AFCFB7AFCFB7AF
        CFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7
        AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCFB7AFCF}
      ParentShowHint = False
      ShowHint = True
      OnClick = PegarBtnClick
    end
    object UndoBtn: TSpeedButton
      Left = 167
      Top = 3
      Width = 24
      Height = 24
      Hint = 'Deshacer (Ctrl-Z)'
      Flat = True
      Glyph.Data = {
        76020000424D7602000000000000760000002800000040000000100000000100
        0400000000000002000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00331111133333
        333333FFFFF33333333333222223332222233333333333444443333199933333
        333333388883333333333332AAA333AAA2333333333333CCC433333199933333
        1133333888833333FF333332AAA333AAA2333334433333CCC433339919933333
        99133388F883333388F333AA2AA333AA2A2333CC433333CC4C43339133933333
        3913338F33833333388F33A233A333A33A2333C4333333C33CC4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4391333333333
        339138F333333333338F3A233333333333A23C433333333333C4339133333333
        3991338F33333333388F33A2333333333AA233C4333333333CC4339913333333
        99133388F333333388F333AA23333333AA2333CC43333333CC43333991333339
        913333388F3333388F33333AA233333AA233333CC433333CC433333399111119
        1333333388FFFFF8F3333333AA22222A23333333CC44444C4333333333999993
        33333333338888833333333333AAAAA33333333333CCCCC33333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 4
      ParentShowHint = False
      ShowHint = True
      OnClick = UndoBtnClick
    end
    object BGuardarComo: TSpeedButton
      Left = 35
      Top = 2
      Width = 25
      Height = 25
      Hint = 'Guardar Como...'
      Flat = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888880000008888888888888888880000008888888800000000080000008888
        88880FFFFFFF08000000888777770F44444F08000000880000000FFFFFFF0800
        000080B088F80F44444F0800000080B088F80FFFFFFF0800000080BB00000F44
        F0000800000080BBBBBB0FFFF0F08800000080BB00000FFFF0088800000080B0
        FFFF000000888800000080B0FFFFF0B078888800000080B0FFFFF0B078888800
        000080B0FFFFF0B0788888000000800000000000888888000000888888888888
        888888000000888888888888888888000000}
      ParentShowHint = False
      ShowHint = True
      OnClick = BGuardarComoClick
    end
    object BImprimir: TSpeedButton
      Left = 5
      Top = 2
      Width = 25
      Height = 25
      Hint = 'Impresi'#243'n del Reporte (Ctrl + I)'
      Flat = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDD000000DDD00000000000DDDD000000DD0777777777070DDD000000D000
        000000000070DD000000D0777777FFF77000DD000000D077777799977070DD00
        0000D0000000000000770D000000D0777777777707070D000000DD0000000000
        70700D000000DDD0FFFFFFFF07070D000000DDDD0FCCCCCF0000DD000000DDDD
        0FFFFFFFF0DDDD000000DDDDD0FCCCCCF0DDDD000000DDDDD0FFFFFFFF0DDD00
        0000DDDDDD000000000DDD000000DDDDDDDDDDDDDDDDDD000000DDDDDDDDDDDD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000}
      ParentShowHint = False
      ShowHint = True
    end
    object bbFavoritos: TSpeedButton
      Left = 65
      Top = 2
      Width = 25
      Height = 25
      Hint = 'Agregar a Mis Favoritos'
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000130B0000130B00000000000000000000000000000000
        000000669AB595ADB9000000000000000000000000000000000000ACC6D3598F
        A9000000000000000000000000000000B0CEDD53C6E6069ECE6B91A5F4F4F400
        000000000000000086B0C784BED668CBE2A1BBC8000000000000000000000000
        0000007EB6CF2AD5FE0EB0DE4E869FE8E9EA0000006CA3BD91CDE250DEFE279E
        C300000000000000000000000000000000000072ACC885E5FB0CCFFE2AC4E93B
        82A05899B6ADDFEC4DDFFE0ACBFA5899B6000000000000000000000000000000
        000000C2DAE690CADE10D0FE25D7FE49D8F3AFEBF487F5FE44E1FE0FADDBB6C9
        D400000000000000000000000000000000000000000067AAC765E1FD11D1FE41
        E0FE72F1FF8BF9FF5CE9FE2F93B8000000000000000000000000000000000000
        000000D4DFE5328CB353DBFA09CEFE2CDAFE5DEAFE89F9FF71EEFC2880A4B2BF
        C60000000000000000000000000000007CAAC10D99C828D3F831DBFE0ED0FE18
        D3FE48E3FE79F3FF87F8FF50E2FA179CC65A8BA2000000000000000000278FB7
        1AC2EC70F0FE6BEFFF45E2FE1DD5FE0ACEFE34DCFE65ECFE8CF9FF6AEEFE32DC
        FE08BCEC1E82A90000002B98C170E2FDACF4FEC5F9FCBCF3FAA6E6F48CE6FA0B
        CEFE1FD6FE51E0F9B8EDF5BDF4FA9FEEFC76E4FD6EE3FC2E8EB28AB9D16CADC9
        73AFCA79B2CC84B5CD87B6CE8CC3D919D3FE0ED0FE1CA8D181ADC384B6CE79B2
        CB72AECA72AFCA88B5CB0000000000000000000000000000000000007DB1CA48
        DEFE09CEFE218FB7000000000000000000000000000000000000000000000000
        0000000000000000000000007BB0C983E9FE11CFFC5195B20000000000000000
        0000000000000000000000000000000000000000000000000000000092BDD39F
        E4F51EC4ED8EB0C1000000000000000000000000000000000000000000000000
        000000000000000000000000CBDFE990C9DD24ABD2CCD6DB0000000000000000
        0000000000000000000000000000000000000000000000000000000000000070
        B0CB6EACC5000000000000000000000000000000000000000000}
      ParentShowHint = False
      ShowHint = True
      OnClick = bbFavoritosClick
    end
  end
  object PageControl: TPageControl [1]
    Left = 0
    Top = 30
    Width = 544
    Height = 351
    ActivePage = tsFiltros
    Align = alClient
    TabOrder = 2
    object tsGenerales: TTabSheet
      Caption = 'Generales'
      object GBParametrosEncabezado: TGroupBox
        Left = 26
        Top = 74
        Width = 485
        Height = 159
        Caption = 'Datos Generales'
        TabOrder = 0
        object lbNombreReporte: TLabel
          Left = 94
          Top = 41
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Nombre:'
        end
        object Label17: TLabel
          Left = 103
          Top = 71
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'T'#237'tulo:'
        end
        object Label3: TLabel
          Left = 16
          Top = 101
          Width = 118
          Height = 13
          Alignment = taRightJustify
          Caption = 'Guardar en Clasificaci'#243'n:'
        end
        object lbCodigoReporte: TLabel
          Left = 98
          Top = 16
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'C'#243'digo:'
        end
        object RE_CODIGO: TZetaDBTextBox
          Left = 140
          Top = 12
          Width = 65
          Height = 21
          AutoSize = False
          Caption = 'RE_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RE_CODIGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object RE_CANDADOlbl: TLabel
          Left = 7
          Top = 126
          Width = 127
          Height = 13
          Alignment = taRightJustify
          Caption = 'S'#243'lo Autor Puede Cambiar:'
        end
        object RE_NOMBRE: TDBEdit
          Left = 140
          Top = 37
          Width = 193
          Height = 21
          DataField = 'RE_NOMBRE'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 0
          OnChange = RE_NOMBREChange
        end
        object RE_TITULO: TDBMemo
          Left = 140
          Top = 61
          Width = 329
          Height = 33
          DataField = 'RE_TITULO'
          DataSource = DataSource
          MaxLength = 100
          TabOrder = 1
        end
        object RE_CLASIFI: TComboBox
          Left = 140
          Top = 97
          Width = 145
          Height = 21
          Style = csDropDownList
          DropDownCount = 12
          ItemHeight = 13
          TabOrder = 2
          OnChange = RE_CLASIFIChange
        end
        object RE_CANDADO: TDBCheckBox
          Left = 140
          Top = 124
          Width = 18
          Height = 17
          DataField = 'RE_CANDADO'
          DataSource = DataSource
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
    end
    object tsOrden: TTabSheet
      Caption = 'Orden'
      ImageIndex = 2
      object BArribaOrden: TZetaSmartListsButton
        Left = 212
        Top = 8
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
          3333333333090333333333333309033333333333330903333333333333090333
          3333333333090333333333300009000033333330999999903333333309999903
          3333333309999903333333333099903333333333309990333333333333090333
          3333333333090333333333333330333333333333333033333333}
        Tipo = bsSubir
        SmartLists = SmartListOrden
      end
      object BAbajoOrden: TZetaSmartListsButton
        Left = 212
        Top = 33
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
          3333333333303333333333333309033333333333330903333333333330999033
          3333333330999033333333330999990333333333099999033333333099999990
          3333333000090000333333333309033333333333330903333333333333090333
          3333333333090333333333333309033333333333330003333333}
        Tipo = bsBajar
        SmartLists = SmartListOrden
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 4
        Width = 199
        Height = 243
        Caption = 'Ordenar Por'
        TabOrder = 0
        object LbOrden: TZetaSmartListBox
          Left = 5
          Top = 15
          Width = 188
          Height = 222
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object BAgregaOrden: TBitBtn
        Left = 8
        Top = 250
        Width = 100
        Height = 25
        Caption = '&Agrega Campo'
        TabOrder = 1
        OnClick = BAgregaOrdenClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BBorraOrden: TBitBtn
        Left = 108
        Top = 250
        Width = 100
        Height = 25
        Caption = '&Borra Campo'
        Enabled = False
        TabOrder = 2
        OnClick = BBorraOrdenClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object GroupBoxOrden: TGroupBox
        Left = 242
        Top = 4
        Width = 285
        Height = 93
        Caption = 'Propiedades de Orden'
        TabOrder = 3
        object lTituloOrden: TLabel
          Left = 14
          Top = 20
          Width = 31
          Height = 13
          Caption = 'T'#237'tulo:'
        end
        object lFormulaorden: TLabel
          Left = 5
          Top = 38
          Width = 40
          Height = 13
          Caption = 'F'#243'rmula:'
        end
        object eFormulaOrden: TMemo
          Left = 49
          Top = 38
          Width = 200
          Height = 45
          Enabled = False
          MaxLength = 255
          TabOrder = 1
        end
        object bFormulaOrden: TBitBtn
          Left = 254
          Top = 58
          Width = 25
          Height = 25
          Hint = 'Editor de F'#243'rmulas'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = bFormulaOrdenClick
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
            DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
            0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
            D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
            D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
            0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
            DDDDD0000000}
        end
        object ETituloOrden: TEdit
          Left = 49
          Top = 16
          Width = 200
          Height = 21
          Enabled = False
          TabOrder = 0
        end
      end
    end
    object tsFiltros: TTabSheet
      Caption = 'Filtros'
      ImageIndex = 1
      object PageControlFiltros: TPageControl
        Left = 32
        Top = 16
        Width = 489
        Height = 255
        ActivePage = tsFiltroFechas
        TabOrder = 0
        OnChange = PageControlFiltrosChange
        object tsFiltroFechas: TTabSheet
          Caption = 'Filtros'
          object ZetaSmartListsButton1: TZetaSmartListsButton
            Left = 213
            Top = 15
            Width = 25
            Height = 25
            Enabled = False
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
              3333333333090333333333333309033333333333330903333333333333090333
              3333333333090333333333300009000033333330999999903333333309999903
              3333333309999903333333333099903333333333309990333333333333090333
              3333333333090333333333333330333333333333333033333333}
            Tipo = bsSubir
            SmartLists = SmartListFiltros
          end
          object ZetaSmartListsButton2: TZetaSmartListsButton
            Left = 213
            Top = 40
            Width = 25
            Height = 25
            Enabled = False
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              80000080000000808000800000008000800080800000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
              3333333333303333333333333309033333333333330903333333333330999033
              3333333330999033333333330999990333333333099999033333333099999990
              3333333000090000333333333309033333333333330903333333333333090333
              3333333333090333333333333309033333333333330003333333}
            Tipo = bsBajar
            SmartLists = SmartListFiltros
          end
          object GBRangoLista: TGroupBox
            Left = 240
            Top = 40
            Width = 207
            Height = 165
            TabOrder = 2
            Visible = False
            object RBTodosRangoValores: TRadioButton
              Left = 11
              Top = 11
              Width = 113
              Height = 17
              Caption = '&Todos'
              Checked = True
              TabOrder = 0
              TabStop = True
              OnClick = RBTodosRangoValoresClick
            end
            object RBActivoRangoValores: TRadioButton
              Left = 11
              Top = 27
              Width = 113
              Height = 17
              Caption = 'A&ctivo'
              TabOrder = 1
              OnClick = RBActivoRangoValoresClick
            end
            object RBListaRangoValores: TRadioButton
              Left = 11
              Top = 43
              Width = 134
              Height = 17
              Caption = 'S&elecci'#243'n'
              TabOrder = 2
              OnClick = RBListaRangoValoresClick
            end
            object CLBSeleccionValores: TCheckListBox
              Left = 11
              Top = 59
              Width = 185
              Height = 100
              ItemHeight = 13
              TabOrder = 3
              OnExit = CLBSeleccionValoresExit
            end
          end
          object RGBooleano: TGroupBox
            Left = 242
            Top = 40
            Width = 233
            Height = 97
            TabOrder = 4
            Visible = False
            object RGSeleccionValores: TRadioGroup
              Left = 16
              Top = 17
              Width = 169
              Height = 72
              Ctl3D = True
              ItemIndex = 0
              Items.Strings = (
                'Si'
                'No'
                'Todos')
              ParentCtl3D = False
              TabOrder = 0
              OnClick = RGSeleccionValoresClick
            end
          end
          object GBAbierto: TGroupBox
            Left = 242
            Top = 40
            Width = 233
            Height = 129
            TabOrder = 3
            Visible = False
            object Label2: TLabel
              Left = 12
              Top = 12
              Width = 25
              Height = 13
              Caption = 'Filtro:'
            end
            object EAbiertoFiltro: TMemo
              Left = 12
              Top = 28
              Width = 209
              Height = 89
              MaxLength = 255
              ScrollBars = ssVertical
              TabOrder = 0
              OnExit = EAbiertoFiltroExit
            end
          end
          object GBCampoFecha: TGroupBox
            Left = 242
            Top = 40
            Width = 181
            Height = 105
            Caption = 'GBCampoFecha'
            TabOrder = 1
            Visible = False
            object lbFechaInicial: TLabel
              Left = 17
              Top = 50
              Width = 30
              Height = 13
              Alignment = taRightJustify
              Caption = 'Inicial:'
            end
            object lbFechaFinal: TLabel
              Left = 22
              Top = 76
              Width = 25
              Height = 13
              Alignment = taRightJustify
              Caption = 'Final:'
              Color = clBtnFace
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentColor = False
              ParentFont = False
            end
            object EFechaInicial: TZetaFecha
              Left = 49
              Top = 45
              Width = 115
              Height = 22
              Cursor = crArrow
              TabOrder = 1
              Text = '15/dic/97'
              Valor = 35779.000000000000000000
              OnExit = EFechaInicialExit
            end
            object EFechaFinal: TZetaFecha
              Left = 49
              Top = 71
              Width = 115
              Height = 22
              Cursor = crArrow
              TabOrder = 2
              Text = '09/dic/97'
              Valor = 35773.000000000000000000
              OnExit = EFechaInicialExit
            end
            object CBRangoFechas: TZetaKeyCombo
              Left = 17
              Top = 19
              Width = 147
              Height = 21
              AutoComplete = False
              Style = csDropDownList
              DropDownCount = 10
              ItemHeight = 13
              TabOrder = 0
              OnChange = CBRangoFechasChange
              ListaFija = lfRangoFechas
              ListaVariable = lvPuesto
              Offset = 0
              Opcional = False
              EsconderVacios = False
            end
          end
          object GroupBox1: TGroupBox
            Left = 10
            Top = 8
            Width = 199
            Height = 186
            Caption = 'Filtrar Por'
            TabOrder = 0
            object LbFiltros: TZetaSmartListBox
              Left = 5
              Top = 20
              Width = 188
              Height = 160
              ItemHeight = 13
              TabOrder = 0
            end
          end
          object BAgregaFiltro: TBitBtn
            Left = 10
            Top = 198
            Width = 100
            Height = 25
            Caption = '&Agrega Filtro'
            TabOrder = 7
            OnClick = BBorraFiltroClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
              333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
              0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
              07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
              0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
              33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
              B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
              3BB33773333773333773B333333B3333333B7333333733333337}
            NumGlyphs = 2
          end
          object BBorraFiltro: TBitBtn
            Tag = 1
            Left = 110
            Top = 198
            Width = 100
            Height = 25
            Caption = '&Borra Filtro'
            Enabled = False
            TabOrder = 8
            OnClick = BBorraFiltroClick
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
              55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
              305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
              005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
              B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
              B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
              B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
              B0557777FF577777F7F500000E055550805577777F7555575755500000555555
              05555777775555557F5555000555555505555577755555557555}
            NumGlyphs = 2
          end
          object EFormulaFiltro: TEdit
            Left = 242
            Top = 15
            Width = 207
            Height = 21
            Enabled = False
            TabOrder = 6
          end
          object GBRangoEntidad: TGroupBox
            Left = 242
            Top = 40
            Width = 233
            Height = 165
            TabOrder = 5
            Visible = False
            object lbInicialRangoEntidad: TLabel
              Left = 27
              Top = 68
              Width = 30
              Height = 13
              Alignment = taRightJustify
              Caption = 'Inicial:'
            end
            object lbFinalRangoEntidad: TLabel
              Left = 32
              Top = 96
              Width = 25
              Height = 13
              Alignment = taRightJustify
              Caption = 'Final:'
            end
            object bFinalRangoEntidad: TSpeedButton
              Left = 198
              Top = 90
              Width = 25
              Height = 25
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                33033333333333333F7F3333333333333000333333333333F777333333333333
                000333333333333F777333333333333000333333333333F77733333333333300
                033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
                33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
                3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
                33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
                333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
                333333773FF77333333333370007333333333333777333333333}
              NumGlyphs = 2
              OnClick = bFinalRangoEntidadClick
            end
            object bInicialRangoEntidad: TSpeedButton
              Left = 198
              Top = 62
              Width = 25
              Height = 25
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                33033333333333333F7F3333333333333000333333333333F777333333333333
                000333333333333F777333333333333000333333333333F77733333333333300
                033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
                33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
                3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
                33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
                333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
                333333773FF77333333333370007333333333333777333333333}
              NumGlyphs = 2
              OnClick = bInicialRangoEntidadClick
            end
            object bListaRangoEntidad: TSpeedButton
              Left = 198
              Top = 133
              Width = 25
              Height = 25
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                0400000000000001000000000000000000001000000010000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                33033333333333333F7F3333333333333000333333333333F777333333333333
                000333333333333F777333333333333000333333333333F77733333333333300
                033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
                33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
                3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
                33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
                333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
                333333773FF77333333333370007333333333333777333333333}
              NumGlyphs = 2
              OnClick = bListaRangoEntidadClick
            end
            object RBTodosRangoEntidad: TRadioButton
              Left = 8
              Top = 13
              Width = 113
              Height = 17
              Caption = '&Todos'
              Checked = True
              TabOrder = 0
              TabStop = True
              OnClick = RBTodosRangoEntidadClick
            end
            object RBActivoRangoEntidad: TRadioButton
              Left = 8
              Top = 29
              Width = 57
              Height = 17
              Caption = 'A&ctivo'
              TabOrder = 1
              OnClick = RBActivoRangoEntidadClick
            end
            object RBRangoRangoEntidad: TRadioButton
              Left = 8
              Top = 45
              Width = 57
              Height = 17
              Caption = '&Rango'
              TabOrder = 2
              OnClick = RBRangoRangoEntidadClick
            end
            object RBListaRangoEntidad: TRadioButton
              Left = 8
              Top = 117
              Width = 73
              Height = 17
              Caption = '&Lista'
              TabOrder = 3
              OnClick = RBListaRangoEntidadClick
            end
            object EInicialRangoEntidad: TZetaEdit
              Left = 62
              Top = 64
              Width = 131
              Height = 21
              CharCase = ecUpperCase
              LookUpBtn = bInicialRangoEntidad
              TabOrder = 4
              OnExit = RBRangoRangoEntidadClick
            end
            object EFinalRangoEntidad: TZetaEdit
              Left = 62
              Top = 92
              Width = 131
              Height = 21
              CharCase = ecUpperCase
              LookUpBtn = bFinalRangoEntidad
              TabOrder = 5
              OnExit = RBRangoRangoEntidadClick
            end
            object EListaRangoEntidad: TZetaEdit
              Left = 24
              Top = 135
              Width = 169
              Height = 21
              CharCase = ecUpperCase
              LookUpBtn = bListaRangoEntidad
              TabOrder = 6
              OnExit = RBListaRangoEntidadClick
            end
          end
        end
        object tsFiltrosEspeciales: TTabSheet
          Caption = 'Filtros Especiales'
          object lbCondicion: TLabel
            Left = 16
            Top = 60
            Width = 50
            Height = 13
            Alignment = taRightJustify
            Caption = 'Condici'#243'n:'
          end
          object lbFiltroUsuario: TLabel
            Left = 41
            Top = 83
            Width = 25
            Height = 13
            Alignment = taRightJustify
            Caption = 'Filtro:'
          end
          object BitBtn1: TBitBtn
            Left = 320
            Top = 178
            Width = 145
            Height = 25
            Caption = '&Constructor de Formulas'
            TabOrder = 2
            OnClick = BitBtn1Click
            Glyph.Data = {
              42010000424D4201000000000000760000002800000011000000110000000100
              040000000000CC00000000000000000000001000000010000000000000000000
              BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
              DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
              DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
              0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
              D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
              D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
              0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
              DDDDD0000000}
          end
          object QU_CODIGO: TZetaDBKeyLookup
            Left = 73
            Top = 56
            Width = 392
            Height = 21
            EditarSoloActivos = False
            IgnorarConfidencialidad = False
            TabOrder = 0
            TabStop = True
            WidthLlave = 100
            DataField = 'QU_CODIGO'
            DataSource = DataSource
          end
          object RE_FILTRO: TDBMemo
            Left = 73
            Top = 83
            Width = 392
            Height = 89
            DataField = 'RE_FILTRO'
            DataSource = DataSource
            MaxLength = 255
            TabOrder = 1
          end
        end
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 381
    Width = 544
    Height = 34
    DesignSize = (
      544
      34)
    inherited OK: TBitBtn
      Left = 383
      Top = 3
      Anchors = [akRight, akBottom]
      Caption = '&Guardar'
      Enabled = False
      ModalResult = 0
      OnClick = OKClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888888000088877777777777777778000088000000000000000778000080BF
        B0FFFFFFF0BFB078000080FBF0F00FFFF0FBF078000080BFB0F77FFFF0BFB078
        000080FBF0FFFFFFF0FBF078000080BFBF0000000FBFB078000080FBFBFBFBFB
        FBFBF078000080BFBFBFBFBFBFBFB078000080FBFBFBFBFBFBFBF078000080BF
        BFBFBFBFBFBFB078000080FBF000000000FBF078000080BF0FFFFFFFF0BFB078
        000080FB0FCCCCCCF0FBF078000080BF0FFFFFFFF0BFB078000080FB0FCCCCCC
        F0F0F078000080BF0FFFFFFFF0BFB08800008800000000000000088800008888
        88888888888888880000}
      NumGlyphs = 1
    end
    inherited Cancelar: TBitBtn
      Left = 461
      Top = 3
      Anchors = [akRight, akBottom]
      Caption = '&Salir'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00330000000000
        03333377777777777F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F333333337F333301111111110333337F333333337F33330111111111
        0333337F3333333F7F333301111111B10333337F333333737F33330111111111
        0333337F333333337F333301111111110333337F33FFFFF37F3333011EEEEE11
        0333337F377777F37F3333011EEEEE110333337F37FFF7F37F3333011EEEEE11
        0333337F377777337F333301111111110333337F333333337F33330111111111
        0333337FFFFFFFFF7F3333000000000003333377777777777333}
      Kind = bkCustom
    end
  end
  object StatusBarRep: TStatusBar
    Left = 0
    Top = 415
    Width = 544
    Height = 19
    Panels = <
      item
        Width = 120
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object DataSource: TDataSource
    OnStateChange = DataSourceStateChange
    Left = 472
  end
  object SmartListFiltros: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LbFiltros
    ListaEscogidos = LbFiltros
    AlBajar = Intercambia
    AlSeleccionar = SmartListFiltrosAlSeleccionar
    AlSubir = Intercambia
    Left = 240
  end
  object SmartListOrden: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LbOrden
    ListaEscogidos = LbOrden
    AlBajar = Intercambia
    AlSeleccionar = SmartListOrdenAlSeleccionar
    AlSubir = Intercambia
    Left = 208
  end
end
