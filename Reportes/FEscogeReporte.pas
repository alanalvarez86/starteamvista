unit FEscogeReporte;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, StdCtrls, Buttons, ComCtrls,
     Db, Grids, DBGrids, ImgList,
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaDBGrid,
     ZReportTools,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaKeyCombo, ToolWin;

type
  TEscogeReporte = class(TForm)
    PanelInferior: TPanel;
    Cancelar: TBitBtn;
    OK: TBitBtn;
    DataSource: TDataSource;
    DBGrid: TZetaDBGrid;
    ListaSmallImages: TImageList;
    PanelPrincipal: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    ZetaKeyCombo1: TZetaKeyCombo;
    ToolBar: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    FEntidad: TipoEntidad;
    FTipoReporte: eTipoReporte;
    {$ifdef ADUANAS}
    FListaEntidades: ArregloEntidades;
    FListaReportes: ArregloReportes;
    {$ENDIF}
    FReporteDefault: Integer;
    FImprimeForma : Boolean;
    FCampos : Variant;
    FValores : Variant;
    procedure SeleccionaReporte;
    function GetFiltroEspecial: string;
  public
    { Public declarations }
    property Entidad: TipoEntidad read FEntidad write FEntidad;
    property TipoReporte: eTipoReporte read FTipoReporte write FTipoReporte;
    {$ifdef ADUANAS}
    property ListaEntidad: ArregloEntidades read FListaEntidades write FListaEntidades;
    property ListaReporte: ArregloReportes read FListaReportes write FListaReportes;
    {$ENDIF}
    property ReporteDefault: Integer read FReporteDefault write FReporteDefault;
    property ImprimeForma : Boolean read FImprimeForma write FImprimeForma;
    property Campos : Variant read FCampos write FCampos;
    property Valores : Variant read FValores write FValores;
  end;

var
  EscogeReporte: TEscogeReporte;

procedure EscogeUnReporte( const sCaption: String; const Entity: TipoEntidad; const ReportType: eTipoReporte; const iDefault: Integer );
procedure ImprimeUnaForma( const Entity: TipoEntidad;
                           const ReportType: eTipoReporte;
                           const vCampos, vValores : Variant );overload;
procedure ImprimeUnaForma( const iReporte: integer;
                           const vCampos, vValores : Variant;
                           const eSalida : TipoPantalla = tgPreview );overload;

{$ifdef ADUANAS}
procedure ImprimeUnaForma( const Entity: ArregloEntidades;
                           const ReportType: ArregloReportes;
                           const vCampos, vValores : Variant );overload;
{$ENDIF}

implementation

uses DReportes,
     DSistema,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;


{$R *.DFM}

function GetFiltroEspecialVariant(Campos, Valores: Variant) : string;
 var i : integer;
begin
     Result := '';
     for i := VarArrayLowBound(Campos,1) to VarArrayHighBound(Campos,1) do
     begin
          if Result > '' then Result := Result + ' AND ';
          Result := Result + Campos[i]+'='+Valores[i]
     end;
end;

procedure EscogeUnReporte( const sCaption: String; const Entity: TipoEntidad; const ReportType: eTipoReporte; const iDefault: Integer );
begin
     with TEscogeReporte.Create( Application ) do
     begin
          try
             Caption := sCaption;
             Entidad := Entity;
             TipoReporte := ReportType;
             ReporteDefault := iDefault;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure ImprimeUnaForma( const Entity: TipoEntidad;
                           const ReportType: eTipoReporte;
                           const vCampos, vValores : Variant );
begin
     with TEscogeReporte.Create( Application ) do
     begin
          try
             Caption := 'Impresión de Formas';
             Entidad := Entity;
             TipoReporte := ReportType;
             ImprimeForma := TRUE;
             Campos := vCampos;
             Valores := vValores;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;


procedure ImprimeUnaForma( const iReporte: integer;
                           const vCampos, vValores : Variant;
                           const eSalida : TipoPantalla );
begin
     dmReportes.ImprimeUnaForma( GetFiltroEspecialVariant( vCampos,vValores ), iReporte, eSalida );
end;

{$ifdef ADUANAS}
procedure ImprimeUnaForma( const Entity: ArregloEntidades;
                           const ReportType: ArregloReportes;
                           const vCampos, vValores : Variant );
begin
 with TEscogeReporte.Create( Application ) do
     begin
          try
             Caption := 'Impresión de Formas';
             ListaEntidad := Entity;
             ListaReporte := ReportType;
             ImprimeForma := TRUE;
             Campos := vCampos;
             Valores := vValores;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;
{$ENDIF}


{ ************** TEscogeReporte ************ }

procedure TEscogeReporte.FormCreate(Sender: TObject);
begin
     HelpContext := H00018_Lista_reportes;
     {$ifdef ADUANAS}
     FListaEntidades := [];
     FListaReportes := [];
     {$ENDIF}
     PanelPrincipal.Visible := FALSE;
     ToolBar.Visible := FALSE;
     {$ifdef ADUANAS}
     ToolBar.Visible := TRUE;
     {$ENDIF}

end;


procedure TEscogeReporte.FormShow(Sender: TObject);
 var lVacio : Boolean; 
begin
     dmReportes.TipoReporte := FTipoReporte;
     with dmReportes do
     begin
          EntidadReporte := FEntidad;
          {$ifdef ADUANAS}
          ListaEntidad := FListaEntidades;
          ListaReporte := FListaReportes;
          {$ENDIF}
          dmSistema.cdsUsuarios.Conectar;

          cdsEscogeReporte.Refrescar;
          //cdsEscogeReporte.Conectar;
          DataSource.DataSet := cdsEscogeReporte;

          lVacio := cdsEscogeReporte.IsEmpty;

          Ok.Enabled := NOT lVacio;
          DBGrid.Enabled := NOT lVacio;

          if lVacio then
             ZetaDialogo.ZError(Caption, 'No Hay Reportes Disponibles',0);
     end;

end;

function TEscogeReporte.GetFiltroEspecial : string;
begin
     Result := GetFiltroEspecialVariant( Campos, Valores );
end;

procedure TEscogeReporte.SeleccionaReporte;
begin
     if FImprimeForma then
     begin
          dmReportes.ImprimeUnaForma(GetFiltroEspecial);
     end
     else dmReportes.cdsEscogeReporte.Modificar;
end;

procedure TEscogeReporte.OKClick(Sender: TObject);
begin
     SeleccionaReporte;
end;

procedure TEscogeReporte.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     if ( DataCol = 0 ) then
        ListaSmallImages.Draw( DBGrid.Canvas, Rect.Left, Rect.Top + 1,
                               DataSource.DataSet.FieldByName('RE_TIPO').AsInteger )
end;

procedure TEscogeReporte.ToolButton2Click(Sender: TObject);
begin
     with dmReportes.cdsEscogeReporte do
     begin
          with TToolButton(Sender) do
          begin
               Filtered := ( Tag >= 0 );
               Filter := Format( 'RE_TIPO = %d', [Tag] )
          end;
     end;
end;

end.
