unit FPolizaConcepto;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, FPolizaBase, ZetaSmartLists, DB, ZetaKeyLookup, ZetaEdit,
  ZetaFecha, CheckLst, StdCtrls, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls, Buttons, ZetaNumero;

type
  TPolizaConcepto = class(TPolizaBase)
    Label8: TLabel;
    RE_HOJA: TZetaDBNumero;
    bFormula: TBitBtn;
    BuscarBtn: TSpeedButton;
    btnLlenaConceptos: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure GridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GridColExit(Sender: TObject);
    procedure bFormulaClick(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure btnLlenaConceptosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure BuscaConcepto;
    procedure Buscar;
    procedure DialogoFormulas;

    { Private declarations }
  protected
    procedure Connect;override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override; { TWinControl }
    procedure Borrar;override;

  public
    { Public declarations }
  end;

var
  PolizaConcepto: TPolizaConcepto;

implementation
uses
    FListaConceptos,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZConstruyeFormula,
    ZetaDialogo,
    ZReportTools,
    {$ifdef TRESS}
    DCatalogos,
    {$endif}

    DReportes;

{$R *.dfm}

procedure TPolizaConcepto.FormCreate(Sender: TObject);
begin
     inherited;
     tsOrden.Visible := FALSE;
     tsOrden.TabVisible := FALSE;
end;

procedure TPolizaConcepto.Connect;
begin
     inherited;
     CampoRep.IndexFieldNames := 'CR_CALC';
    {$ifdef TRESS}
     dmCatalogos.cdsConceptosLookUp.Conectar;
    {$endif}
end;


procedure TPolizaConcepto.GridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CR_FORMULA' ) then
          begin
               with bFormula do
               begin
                    //ItemIndex := CampoRep.FieldByName('CR_OPER').AsInteger;
                    Left := Rect.Right - Width + 2;
                    Top := Rect.Top + Grid.Top ;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TPolizaConcepto.GridColExit(Sender: TObject);
begin
     inherited;
     with Grid do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'CR_FORMULA' ) and
             ( BFormula.Visible ) then
          begin
               BFormula.Visible:= False;
          end;
     end;
end;

procedure TPolizaConcepto.bFormulaClick(Sender: TObject);
begin
     inherited;
     DialogoFormulas;
end;

procedure TPolizaConcepto.DialogoFormulas;
 var sFormula : string;
begin
     sFormula := CampoRep.FieldByName('CR_FORMULA').AsString;
     if ZConstruyeFormula.GetFormulaConstruye( Entidad, sFormula, 0, evReporte ) then
     begin
          if (CampoRep.State = dsBrowse ) then
          begin
               CampoRep.Edit;
               Modo := dsEdit;
          end;
          CampoRep.FieldByName('CR_FORMULA').AsString := sFormula;
          Grid.SetFocus;
     end;
end;

procedure TPolizaConcepto.Buscar;
begin

     if ( ActiveControl = Grid ) then
     begin
          with Grid do
          begin
               if ( SelectedField.FieldName = 'CR_CALC' ) then
                  BuscaConcepto;
          end;
     end;
end;

procedure TPolizaConcepto.BuscaConcepto;
begin
     dmReportes.BuscaConcepto;
end;

procedure TPolizaConcepto.BuscarBtnClick(Sender: TObject);
begin
     inherited;
     Buscar;
end;


procedure TPolizaConcepto.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( ActiveControl = Grid ) then
     begin
          if ( Key <> 0 ) and
             ( ssCtrl in Shift ) and { CTRL }
             ( Key = 66 )  then      { Letra B = Buscar }
          begin
               Key := 0;
               Buscar;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;


procedure TPolizaConcepto.btnLlenaConceptosClick(Sender: TObject);
var
   oCursor: TCursor;
   iRecordCount: integer;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ListaConceptos = NIL then
           ListaConceptos := TListaConceptos.Create( self );

        with ListaConceptos do
        begin
             ShowModal;
             if ModalResult = mrOK then
             begin
                  iRecordCount := CampoRep.RecordCount;
                  dmReportes.LlenaConceptos( Conceptos );

                  if ( CampoRep.RecordCount <> iRecordCount ) then
                     Modo := dsEdit;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TPolizaConcepto.Borrar;
begin
     if CampoRep.IsEmpty then
        ZetaDialogo.ZInformation( Caption, 'No hay Cuentas para Borrar', 0 )
     else if ZetaDialogo.ZConfirm( Caption, '�Desea Borrar la Cuenta del Concepto '+ Camporep.FieldByname('CR_CALC').AsString+'?',0, mbYes ) then
     begin
          CampoRep.Delete;
          Modo := dsEdit;
     end;
end;


procedure TPolizaConcepto.FormShow(Sender: TObject);
begin
     inherited;
     bFormula.Visible := FALSE;
end;

end.
