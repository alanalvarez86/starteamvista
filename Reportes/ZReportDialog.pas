unit ZReportDialog;

interface
uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

procedure MuestraDialogo( const sCaption, sMsg, sArchivo: String );

implementation
uses
    ZetaDialogo,
    ZetaClientTools;

procedure MuestraDialogo( const sCaption, sMsg, sArchivo: String );
begin
     if ZetaDialogo.ZConfirm( sCaption, sMsg, 0, mbNo ) then
     begin
          try
             ExecuteFile( sArchivo, '', '', SW_SHOWDEFAULT );
          except
                On E:Exception do
                   ZetaDialogo.ZError('Problemas al Abrir Documento', 'El Documento no Contiene Información', 0 );
          end;
     end;
end;
end.
