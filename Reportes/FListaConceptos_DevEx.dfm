inherited ListaConceptos_DevEx: TListaConceptos_DevEx
  Left = 535
  Top = 273
  Caption = 'Tipos de Concepto'
  ClientHeight = 187
  ClientWidth = 198
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 204
  ExplicitHeight = 215
  PixelsPerInch = 96
  TextHeight = 13
  object cbListaConceptos: TcxCheckListBox [0]
    Left = 0
    Top = 0
    Width = 198
    Height = 154
    Align = alClient
    EditValueFormat = cvfStatesString
    Items = <>
    Style.BorderColor = clWhite
    Style.BorderStyle = cbsNone
    Style.Color = clWhite
    Style.LookAndFeel.Kind = lfFlat
    StyleDisabled.LookAndFeel.Kind = lfFlat
    StyleFocused.LookAndFeel.Kind = lfFlat
    StyleHot.LookAndFeel.Kind = lfFlat
    TabOrder = 0
  end
  inherited PanelBotones: TPanel
    Top = 154
    Width = 198
    Height = 33
    TabOrder = 1
    ExplicitTop = 154
    ExplicitWidth = 198
    ExplicitHeight = 33
    inherited OK_DevEx: TcxButton
      Left = 23
      Top = 3
      ExplicitLeft = 23
      ExplicitTop = 3
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 101
      Top = 3
      ExplicitLeft = 101
      ExplicitTop = 3
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
