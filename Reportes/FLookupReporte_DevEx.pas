unit FLookupReporte_DevEx;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ImgList, Db, Grids, DBGrids,
  ZetaDBGrid, ZetaKeyCombo,
  ZetaCommonClasses,
  ZetaMessages,
  ZetaCommonLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  
  TressMorado2013, dxSkinsDefaultPainters,  cxButtons,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxTextEdit;

const
     K_FILTRO_LOOK = '(RE_TIPO=0 or RE_TIPO=1)';
type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);
type
  TFormaLookupReporte_DevEx = class(TZetaDlgModal_DevEx)
    PanelSuperior: TPanel;
    lbClasificacion: TLabel;
    lbBusca: TLabel;
    CbTablas: TZetaKeyCombo;
    Panel1: TPanel;
    EBuscaNombre: TEdit;
    DataSource: TDataSource;
    BBusca_DevEx: TcxButton;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    RE_TIPO_DESC: TcxGridDBColumn;
    RE_NOMBRE: TcxGridDBColumn;
    RE_TIPO: TcxGridDBColumn;
    RE_FECHA: TcxGridDBColumn;
    RE_ENTIDAD: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    SmallImageList: TcxImageList;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure EBuscaNombreChange(Sender: TObject);
    //procedure DBGridTitleClick(Column: TColumn);
    //procedure OKClick(Sender: TObject);
    procedure CbTablasChange(Sender: TObject);
    //procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      //DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BBusca_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure RE_TIPO_DESCCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure RE_TIPO_DESCHeaderClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FNumReporte : Integer;
    FNomReporte : String;
    FFiltro : string;
    procedure Refresh;
    procedure FiltraReporte;
    procedure LimpiaGrid;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function GetClasificacion: eClasifiReporte;
    function PuedeConsultar: Boolean;
    procedure ApplyMinWidth;
  protected
    property ClasificacionActiva: eClasifiReporte read GetClasificacion;
  public
    { Public declarations }
    property NumReporte : Integer read FNumReporte;
    property NombreReporte : String read FNomReporte;
    property Filtro: string read FFiltro write FFiltro;
  end;

var
  FormaLookupReporte_DevEx: TFormaLookupReporte_DevEx;

implementation

uses DReportes, 
     DDiccionario,
     ZetaDialogo,
     ZAccesosMgr,
     ZReportModulo,
     ZetaClientTools;

{$R *.DFM}

procedure TFormaLookupReporte_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     if DataSource.DataSet = NIL then
     begin
       DataSource.DataSet := dmReportes.cdsSuscripReportes;

       {with dmDiccionario do
            CBTablas.ItemIndex:= GetPosClasifi( CBTablas.Items, ClasifiDefault );}

     end;
     {$ifdef RDD}
     CBTablas.ItemIndex:= 0; //Siempre se posiciona en la primera;
     {$else}
     CBTablas.ItemIndex:= dmDiccionario.GetPosClasifi( CBTablas.Items, dmDiccionario.ClasifiDefault );
     {$endif}


     CBTablasChange(self);

     Refresh;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

{$ifdef COMENTARIOS}
procedure TFormaLookupReporte_DevEx.Refresh;
begin
     Screen.Cursor := crHourGlass;
     try
        with CbTablas do
        dmReportes.ClasifActivo := ClasificacionActiva;
        dmReportes.cdsSuscripReportes.Refrescar;
        dmReportes.cdsSuscripReportes.Filtered := FALSE;
        ZetaClientTools.OrdenarPor(dmReportes.cdsSuscripReportes,'RE_NOMBRE');
        LimpiaGrid;
        FiltraReporte;
     finally
            Screen.Cursor := crDefault;
     end;
end;
{$endif}

procedure TFormaLookupReporte_DevEx.Refresh;
var
   oCursor : TCursor;
   //FClasifiTemp: integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        if PuedeConsultar then
        begin
             with CbTablas do
             begin
                  with dmReportes do
                  begin
                       dmReportes.ClasifActivo := ClasificacionActiva;
                       Clasificaciones := Items; //CBTablas.ITems;
                       try
                          cdsSuscripReportes.Refrescar;
                          cdsSuscripReportes.Filtered := FALSE;
                          ZetaClientTools.OrdenarPor(cdsSuscripReportes,'RE_NOMBRE');
                       except
                             On E:Exception do
                             begin
                                  cdsSuscripReportes.EmptyDataset;
                                  ZError(Caption, E.Message, 0);
                             end;
                       end;
                  end;
             end;
             //FClasifiTemp:= CbTablas.ItemIndex;
        end;
        {else
        begin
             if ( FClasifiTemp > -1 ) then CbTablas.ItemIndex:= FClasifiTemp;
        end;}
        LimpiaGrid;
        FiltraReporte;

     finally
            Screen.Cursor := oCursor;
     end;
end;
procedure TFormaLookupReporte_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions = 35;
begin
     with  ZetaDbGridDbTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TFormaLookupReporte_DevEx.FormCreate(Sender: TObject);
var
totWidth : integer;
begin
     inherited;
     HelpContext := H50051_Explorador_reportes;
     ApplyMinWidth;
     totWidth := RE_TIPO.Width + RE_TIPO_DESC.Width +  RE_NOMBRE.Width + RE_ENTIDAD.Width ;
     if (TotWidth < 405 ) then
     begin
          self.Width := 430;
     end
     else
     begin
          self.Width := totWidth+25;
     end;
     //ListaSmallImages.ResourceLoad(rtBitmap, 'EXPLORERSMALL', clOlive);
     SmallImageList.ResourceLoad(rtBitmap,'EXPLORERSMALL',clOlive);
     dmDiccionario.GetListaClasifi( CBTablas.Items );
     ZetaDbGridDbTableView.ApplyBestFit();
     FFiltro := K_FILTRO_LOOK;
     {$ifdef TRESSEMAIL}
        RE_ENTIDAD.Visible := False;
     {$endif}
end;

//(@am):Para el keypress en DBTableViews funcione
procedure TFormaLookupReporte_DevEx.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
  if ( ActiveControl.parent.classname = 'TZetaCXGrid' )then
  begin
     if Key = Chr( VK_RETURN )then
     begin
          if OK_DevEx.Visible and OK_DevEx.Enabled then
          begin
               Key := #0; //Limpiamos el Key para que no se ejecute el ENTER de la clase padre
               OK_DevEx.Click;
          end;
     end;
  end;
  inherited;  //Se ejecuta KeyPress de la clase padre
end;

procedure TFormaLookupReporte_DevEx.FiltraReporte;
begin
     {with dmReportes.cdsSuscripReportes do
     begin
          Filtered := TRUE;
          if NOT BBusca.Down then
             Filter := FFiltro
          else Filter := FFiltro+ ' AND '+
                         'UPPER(RE_NOMBRE) LIKE '+ chr(39)+'%'+
                         UpperCase(EBuscaNombre.Text) + '%' + chr(39);
     end; }
     with dmReportes.cdsSuscripReportes do
     begin
          Filtered := TRUE;
          if NOT BBusca_DevEx.Down then
             Filter := FFiltro
          else Filter := FFiltro+ ' AND '+
                         'UPPER(RE_NOMBRE) LIKE '+ chr(39)+'%'+
                         UpperCase(EBuscaNombre.Text) + '%' + chr(39);
     end;
     LimpiaGrid;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TFormaLookupReporte_DevEx.BBuscaClick(Sender: TObject);
begin
  inherited;
  FiltraReporte;
end;

procedure TFormaLookupReporte_DevEx.EBuscaNombreChange(Sender: TObject);
begin
  inherited;
  //BBusca.Down := Length(EBuscaNombre.Text)>0;
  BBusca_DevEx.Down := Length(EBuscaNombre.Text)>0;
  FiltraReporte;
end;

procedure TFormaLookupReporte_DevEx.LimpiaGrid;
begin
     //dbGrid.SelectedRows.Clear;
     ZetaDBGridDBTableView.Controller.ClearSelection;
end;

{procedure TFormaLookupReporte_DevEx.DBGridTitleClick(Column: TColumn);
begin
     inherited;
     with dmReportes do
          case Column.Index of
               0:ZetaClientTools.OrdenarPor(cdsSuscripReportes,DBGrid.Columns[2].FieldName);
               5:ZetaClientTools.OrdenarPor(cdsSuscripReportes,DBGrid.Columns[4].FieldName);
               6:ZetaClientTools.OrdenarPor(cdsSuscripReportes,'US_CODIGO')
               else ZetaClientTools.OrdenarPor(cdsSuscripReportes,Column.FieldName )
          end;
end;}

{procedure TFormaLookupReporte_DevEx.OKClick(Sender: TObject);
begin
  inherited;
  with dmReportes.cdsSuscripReportes do
  begin
    FNumReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
    FNomReporte := FieldByName( 'RE_NOMBRE' ).AsString;
    ModalResult := mrOK;
  end;
end;}

procedure TFormaLookupReporte_DevEx.CbTablasChange(Sender: TObject);
begin
     inherited;
     Refresh;
end;

{procedure TFormaLookupReporte_DevEx.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if ( DataCol = 0 ) then
        ListaSmallImages.Draw( DBGrid.Canvas, Rect.Left, Rect.Top + 1,
                               DataSource.DataSet.FieldByName('RE_TIPO').AsInteger )
end;}

procedure TFormaLookupReporte_DevEx.WMExaminar(var Message: TMessage);
begin
    //OKClick( self );
    OK_DevExClick( self );
end;

procedure TFormaLookupReporte_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  // Para que el explorador de reportes vuelva a hacer el Query
  dmReportes.cdsSuscripReportes.SetDataChange;
  inherited;
end;

function TFormaLookupReporte_DevEx.PuedeConsultar: Boolean;
begin
     {$ifdef RDD}
     Result := ZAccesosMgr.CheckUnDerecho( dmDiccionario.GetDerechosClasifi(ClasificacionActiva), K_DERECHO_CONSULTA );
     {$else}
     Result := ZAccesosMgr.CheckDerecho(dmDiccionario.GetDerechosClasifi(ClasificacionActiva), K_DERECHO_CONSULTA );
     {$endif}
     if not Result then
        ZInformation( Caption, 'Usuario no Tiene Derecho para Consultar en esta Clasificación', 0 );
end;

function TFormaLookupReporte_DevEx.GetClasificacion: eClasifiReporte;
begin
     with CBTablas do
          Result := eClasifiReporte(Items.Objects[ItemIndex]);
end;

procedure TFormaLookupReporte_DevEx.BBusca_DevExClick(Sender: TObject);
begin
  inherited;
  FiltraReporte;
end;

procedure TFormaLookupReporte_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
   inherited;
  with dmReportes.cdsSuscripReportes do
  begin
    FNumReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
    FNomReporte := FieldByName( 'RE_NOMBRE' ).AsString;
    ModalResult := mrOK;
  end;
end;

procedure TFormaLookupReporte_DevEx.RE_TIPO_DESCCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   APainter: TcxPainterAccess;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
   with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + SmallImageList.Width + 1;
           DrawContent;
           DrawBorders;
              with AViewInfo.ClientBounds do
                 //  SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, RE_TIPO.DataBinding.DataController.DataSource.DataSet.FieldByName('RE_TIPO').AsInteger );
                     SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, Aviewinfo.GridRecord.Values[RE_TIPO.Index] );

         finally
               Free;
        end;
  end;
  ADone := True;
end;

procedure TFormaLookupReporte_DevEx.RE_TIPO_DESCHeaderClick(
  Sender: TObject);
begin
  inherited;
  ZetaClientTools.OrdenarPor(dmReportes.cdsSuscripReportes,RE_TIPO.DataBinding.FieldName);
  //ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'RE_TIPO');
end;

procedure TFormaLookupReporte_DevEx.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  OK_DevExClick( self );
end;

procedure TFormaLookupReporte_DevEx.ZetaDBGridDBTableViewKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     inherited;
     if Key = VK_RETURN then
     begin
          if OK_DevEx.Visible and OK_DevEx.Enabled then
             OK_DevEx.Click;
     end;
end;

end.
