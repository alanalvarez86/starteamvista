unit ZReportTools;

interface
{$INCLUDE DEFINES.INC}

{$ifndef DOS_CAPAS}
{$ifndef CONCILIA}
 {$define HTTP_CONNECTION}
{$endif}
{$endif}

uses SysUtils,Classes,Controls,DB,Mask,
     Dialogs,Forms,FileCtrl,stdCtrls,
     {$IFDEF TRESS_DELPHIXE5_UP}
     System.IOUtils,
     {$ENDIF}
     Variants,
     MaskUtils,
     ZAgenteSQLClient,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaSmartLists,
     ZetaSmartLists_DevEx,
     ZReportToolsConsts;

const
      SINTITULO  = '< TITULO VACIO >';
      K_TITULO = 'T�tulo';

      K_MAX_TAMANIO_FOTO = 50000; //Es el limite para el tama�o de las fotos, para que no aparezcan negras: 50KB


      aMascaraEntero : array[ 1..8 ] of string =
                    ( '#0;-#0',
                      '#0;-#0;#',
                      '#,0;-#,0',
                      '#,0;-#,0;#',
                      '$#,0;-$#,0',
                      '$#,0;-$#,0;#',
                      '$#,0;($#,0)',
                      '0 %;0 %' );

      aMascaraFloat : array[ 1..13 ] of string =
                   ( '#,0;-#,0',
                     '#,0.00;-#,0.00',
                     '#,0.00;(#,0.00)',
                     '#,0.00;-#,0.00;#',
                     '$#,0.00;($#,0.00)',
                     '$#,0.00;-$#,0.00',
                     '$#,0.00;-$#,0.00;#',
                     '$#,0;-$#,0',
                     '$#,0;($#,0)',
                     '0 %;-0 %',
                     '0.00 %;-0.00 %',
                     '#0.00 %;-#0.00 %',
                     '#0.00000%;-#0.00000%');

      aMascaraFecha : array[ 1..11 ] of string =
                   ( 'd/m',
                     'd/m/yy',
                     'dd/mm/yy',
                     'dd/mm/yyyy',
                     'dd/mmm/yy',
                     'dd/mmm/yyyy',
                     'd-mmm-yy',
                     'dd-mmm-yy',
                     'mmm-yy',
                     'mmmm-yy',
                     'mmmm d, yyyy');

     aMascaraTexto : array[ 1..5 ] of string =
                   ( 'LLLL-999999-AAA;0;',
                     '99-99-99-9999-9;0;',
                     '!\(999\)\000\-0000;',
                     '!99999;0;',
                     '00:00;0' );

type TipoConPlantilla = Set of ETipoFormato;
     TipoPantalla = (tgImpresora,tgPreview,tgGrafica);
     TiposPantalla = Set of TipoPantalla;
     TiposConcepto = set of eTipoConcepto;
     {$ifdef ADUANAS}
     ArregloEntidades = set of TipoEntidad;
     ArregloReportes= set of eTipoReporte;
     {$endif}

     TipoMailMerge = set of eTipoFormato;

const TiposConPlantilla : TipoConPlantilla = [ tfImpresora,tfHTML,tfQRP,tfTXT,tfXLS,tfRTF,tfWMF ];
const TiposMailMerge : TipoMailMerge = [ tfMailMerge,tfMailMergeXLS ];

var FMontoCiclo, FPosCiclo : integer;

type
  TDiccionRecord = record
    DI_CLASIFI : TipoEntidad;
    DI_TABLA   : string;
    DI_NOMBRE  : string;
    DI_TITULO  : string;
    DI_CALC    : integer;
    DI_ANCHO   : integer;
    DI_MASCARA : string;
    DI_TFIELD  : eTipoGlobal;
    DI_ORDEN   : Boolean;
    DI_REQUIER : string;
    DI_TRANGO  : eTipoRango;
    DI_NUMERO  : integer;
    DI_VALORAC : {$ifdef RDD}eRDDValorActivo;{$else}string;{$endif}
    DI_RANGOAC : integer;
    DI_TCORTO  : string;
    DI_OPER    : eTipoOperacionCampo;
    DI_LOOK_DESCRIP : string;
  end;

  TCampoMaster = class(TObject)
    private
      FPosAgente: integer;
    protected
      function GetNombreCampo : string;virtual;
    public
      NombreColumna : string; //'ALIAS' de la columna
      Entidad   : TipoEntidad;//Entidad a la que pertenece
{$IFDEF TRESS_DELPHIXE5_UP}
      Tabla : string;
{$ELSE}
      Tabla : string[ 10 ];
{$ENDIF}
      Formula    : string;//Nombre Campo
      Titulo : string;
      TCorto : string;
      TipoCampo : eTipoGlobal;
      Calculado : Integer;
      property PosAgente : integer read FPosAgente write FPosAgente default -1;
      property NombreCampo : string read GetNombreCampo;
      procedure GetDatosDataSet( cdsDataSet : TDataSet );virtual;
  end;

  TSuscripCampoMaster = class ( TCampoMaster )
     Usuario: integer;
     Frecuencia : eEmailFrecuencia;
     HayDatos : eEmailNotificacion;
     Direccion : String;
     FormatoRep : eEmailFormato;
  end;

  TFiltroOpciones = class( TCampoMaster )
     Numero : integer;
     TipoRangoActivo : eTipoRangoActivo;     //eTipoRangoActivo = (raTodos,raActivo,raRango,raLista);
     TipoRango : eTipoRango;                 //eTipoRango = (rNinguno,rRangoEntidad,rRangoListas, rFechas, rBool );
     ValorActivo : {$ifdef RDD}eRDDValorActivo;{$else}string;{$endif}
     RangoInicial : string;
     RangoFinal : string;
     RangoFechas : eRangoFechas;
     FechaInicial : TDate;
     FechaFinal : TDate;
     Lista : string;
     Posicion : integer;
     procedure GetDatosDataSet( cdsDataSet : TDataSet );override;

  end;

  TCampoOpciones = class(TCampoMaster)
    protected
     function GetNombreTotal: string;
     function GetNombreCampo : string;override;
    public
      Justificacion : TAlignment;
      Requiere   : string;
      Mascara    : string;//mascara de impresion
      Ancho      : integer; //En numero de letras
      LeftTitulo : integer;
      LeftCampo  : integer;
      AnchoTitulo: integer; //ancho del titulo en pixeles
      AnchoCampo : integer; //ancho del cmapo en pixeles
      Top        : integer;
      Alto       : integer;
      Posicion   : integer; //Posicion del Campo dentro de la lista
      Operacion  : eTipoOperacionCampo;//tipo de totalizacion
      TipoImp : eTipoGlobal;
      OpImp : eTipoOperacionCampo;
      {$ifdef ADUANAS}
      AutoStretch: Boolean;
      {$endif}
      SQLColumna : TSQLColumna;
      constructor Create;
      function AsTexto( FDataSet : TDataSet ) : String;virtual;
      function AsTextoTotal(FDataSet: TDataSet; const lSoloTotales, lImportar: Boolean): String; overload;
      function AsTextoTotal(FDataSet: TDataSet; const lSoloTotales:Boolean): String; overload;
      procedure GetDatosDataSet( cdsDataSet : TDataSet );override;
      property NombreTotal : string read GetNombreTotal;
  end;

  TCampoIRapida = class(TCampoOpciones)
      Fuente     : TFuente;
      Columna    : integer; //Posicion Impresion Rapida
      Renglon    : integer;
      Banda      : eTipoBanda;
      function AsTexto( FDataSet : TDataSet ) : String;override;
      procedure GetDatosDataSet( cdsDataSet : TDataSet );override;
  end;

  TCampoListado = class(TCampoOpciones)
      procedure GetDatosDataSet( cdsDataSet : TDataSet );override;
  end;

  TGrupoOpciones = class( TCampoMaster )
    constructor Create;
    destructor Destroy;override;
  protected
   function GetNombreCampo : string;override;
  public
    SaltoPagina : Boolean; //si se quiere pagina nueva po cada grupo
    Totalizar : Boolean;//si se quiere que aparezca totalizaci�n en el grupo
    Encabezado : Boolean;// si se quiere que aparezca el encabezado del grupo
    PieGrupo : Boolean;// // si se quiere que aparezca el pie de grupo
    Master : Boolean;
    ListaEncabezado : TStrings; //Campos del Encabezado
    ListaPie : TStrings;//Campos del Pie del Grupo
    Ultimo : Boolean;
    {$ifdef ADUANAS}
    Titulos: Boolean;
    {$endif}
    procedure GetDatosDataSet( cdsDataSet : TDataSet );override;
  end;

  TOrdenOpciones = class( TCampoMaster )
  public
      DireccionOrden : Integer; //Asecendente = 0 , Descendente = 1
      procedure GetDatosDataSet( cdsDataSet : TDataSet );override;
  end;

  TCampoMasterClass = class of TCampoMaster;

  TExcelOpciones = record
    Col         : Integer;
    Row         : Integer;
    Hoja        : String;
    Refrescar   : Boolean;
  end;

  TDatosImpresion = record
    Tipo        : eTipoFormato; // Tipo de Salida
    Impresora   : Integer;      // Nombre Impresora Seleccionada
    Copias      : Integer;      // Numero de Copias
    PagInicial  : Integer;      // Pagina Inicial de Impresion
    PagFinal    : Integer;      // Pagina Final de Impresion
    Archivo     : String;       // Nombre de la Plantilla
    Exportacion : String;       // Nombre de Exportacion.
    {$IFDEF TRESS_DELPHIXE5_UP}
    Separador   : ShortString;    // Separador para Exportacion de Archivos ASCII
    {$ELSE}
    Separador   : String[1];    // Separador para Exportacion de Archivos ASCII
    {$ENDIF}
    Grafica     : string;       // Plantilla para las graficas
    ExcelOpciones: TExcelOpciones;
    {$IFDEF TRESS_DELPHIXE5_UP}
    {***(@am): Se agrega este campo para la impresion de gafetes por ambos lados. Anteriormente no se necesitaba
                porque la config. de la impresora sobreescribia la especificacion de estos valores. Sin embargo con
                el cambio de componente para QRD vs 159 si es necesario especificar si se necesita o no. El valor
                por defecto del campo sera FALSO. Solo podra ser editado a VERDADERO desde la pantalla de edicion
                de reportes de tipos FORMA.***}
    ImprimirAmbosLados: boolean;
    {$ENDIF}

    {$IFDEF TRESS_REPORTES}
    Copia                  : String;           // Ruta para guardar copia en disco. Solo lo usa el Servicio de Reportes.
    FolioCalendario        : Integer;          // Folio de la tarea en calendario de reportes.
    NombreCalendario       : String;           // Nombre de la tarea en calendario de reportes.
    Nombre                 : String;           // Nombre del reporte. Campo RE_NOMBRE de tabla DATOS.dbo.REPORTE.
    {$ENDIF}
  end;

  procedure GetListaMascaras( oLista : TStrings; const eTipo : eTipoGlobal );
  procedure DiccionToCampoMaster( const oDiccion: TDiccionRecord;
                                  oCampo: TCampoMaster);
  procedure GetSeparador( var oSeparador : TCampoOpciones );
  procedure DespuesDeAgregarDato( oCampo : TCampoMaster;
                                  oLista : TZetaSmartLists;
                                  oBoton, oControl : TWinControl  );overload;
  function AntesDeAgregarDato( const oEntidad : TipoEntidad;
                               const lRepetidos,lTodos : Boolean;
                               var oDiccion : TDiccionRecord;
                               oLista : TZetaSmartLists ;
                               const lConRelaciones: Boolean= TRUE ):Boolean;overload;
  procedure DespuesDeAgregarDato( oCampo : TCampoMaster;
                                  oLista : TZetaSmartLists_DevEx;
                                  oBoton, oControl : TWinControl  );overload;
  function AntesDeAgregarDato( const oEntidad : TipoEntidad;
                               const lRepetidos,lTodos : Boolean;
                               var oDiccion : TDiccionRecord;
                               oLista : TZetaSmartLists_DevEx ;
                               const lConRelaciones: Boolean= TRUE ):Boolean;overload;
  function GetDiccionRecord( cdsDataSet : TDataSet ) : TDiccionRecord;
  function GetCampoDescripcion( const oDiccion : TDiccionRecord ) : string;
  function GetCampoDescripcionGrupos( const FEntidadActiva, FEntidadGrupo : TipoEntidad; const oDiccion : TDiccionRecord ) : string;
  function FiltroFechaSQL( const oCampo : TFiltroOpciones; const lCliente : Boolean ) : string;
  function FiltroBool( const oFiltro : TFiltroOpciones  ) : string;
  function FiltroStatus( const oFiltro : TFiltroOpciones; const lDescrip : Boolean ) : string;
  function FiltroAbierto( const oFiltro : TFiltroOpciones ) : string;
  function FiltroRango( const oFiltro : TFiltroOpciones; const lCliente : Boolean ) : string;
  function GetFiltroOpciones( CampoRep : TDataSet ) : TFiltroOpciones;
  function GetStringFiltro(oFiltro : TFiltroOpciones; var lDiccion : Boolean; const lCliente : Boolean = FALSE) : string;
  function GetDescripFiltro(oFiltro : TFiltroOpciones) : string;
  function GetCampoRepFiltro( DataSet : TDataSet ; var lDiccion : Boolean; var Entidad : TipoEntidad ) : string;
  function ModuloGraficas : Boolean;
  procedure GetFechasRango( const eTipo: eRangoFechas; var dInicial,dFinal : TDate );

  function ReportOpenDialog( sArchivo,
                             sDefaultExt,
                             sFilename,
                             sFilter : string  ) : string;

  function ReportOpenDialogB( var sArchivo:string;
                                  sDefaultExt,
                                  sFilename,
                                  sFilter : string  ) : Boolean;

  function ReportOpenDialogFilter(  sFilename,
                                    sDefaultExt,
                                    sFilter : string;
                                var iFilter: integer  ) : string;

  {$IFDEF TRESS_DELPHIXE5_UP}
  function ReportOpenDialogFilterB_Plantilla( var sFilename: string;
                                      sDefaultExt,
                                      sFilter : string;
                                  var iFilter: integer) : Boolean;
  {$ENDIF}

  function ReportOpenDialogFilterB( var sFilename : string;
                                        sDefaultExt,
                                        sFilter : string;
                                    var iFilter: integer  ) : boolean;

  function ReportSaveDialog( const sFilename,
                                   sDefaultExt,
                                   sFilter : string  ) : string;

  function ReportSaveDialogFilter(  sFilename,
                                    sDefaultExt,
                                    sFilter : string;
                                    var  iFilter: Integer ): string;
  function ReportSaveDialogFilterB( var  sFilename: string;
                                         sDefaultExt,
                                         sFilter : string;
                                     var iFilter: Integer ): boolean;

  function DialogoPlantilla( const sArchivo : string ): string;
  function DialogoAscii( sArchivo : string ): string;
  function DialogoAsciiB( var sArchivo : string ): boolean;
  function DialogoWord( const sArchivo : string ): string;
  function DialogoExcel( const sArchivo : string ): string;
  function DialogoListado( sArchivo : string; const eTipo: eTipoFormato ): string;
  function DialogoListadoB( var sArchivo : string; const eTipo: eTipoFormato ): Boolean;
  function DialogoFormas( const sArchivo : string; const eTipo: eFormatoFormas ): string;
  function DirPlantilla : string;

  function GetMascaraDefault( const eTipo : eTipoGlobal ) : string;
  function GetFormatFecha( const dFecha : TDate; const sMascara : string ): string;
  function FormateaValor( const sValor: string; sMascara : string;
                           const eTipo : eTipoGlobal; const lComillas : Boolean = TRUE ): string;
  function GetNColumna( const i : integer) : string;
  function TransfomaParams( const sFormula: string; const Cuantos : Integer; Parametros: Olevariant ) : string;
  function TransParamNomConfig( const sFormula: string; const Cuantos : Integer; Parametros: Olevariant; lForzaSustituye, lConComillas: Boolean) : string;
  procedure AgregaListaObjeto( cdsDataSet:TDataset; oLista:TStrings );
{$ifndef CONCILIA}
  procedure AgregaSQLColumnas(Agente:TSQLAgenteClient;oCampo: TCampoOpciones; oDatosImpresion : TDatosImpresion; const Banda : integer = -1; const iParametros : integer = 0 );
  procedure AgregaSQLColumnasParam(Agente:TSQLAgenteClient;oParam : TCampoMaster);
  procedure AgregaSQLFiltros( Agente:TSQLAgenteClient; oFiltro: TFiltroOpciones; var FFiltroFormula, FFiltroDescrip : string );
  procedure AgregaSQLGrupos(Agente:TSQLAgenteClient; oGrupo: TGrupoOpciones; const iParametros: integer = 0 );
  procedure AgregaSQLOrdenes(Agente:TSQLAgenteClient;oOrden: TOrdenOpciones; const iParametros: integer = 0 );
  procedure AgregaSQLFiltrosEspeciales(Agente:TSQLAgenteClient; sFiltro : string; const lDeDiccion : Boolean = FALSE; const iParametros : integer = 0 );
  procedure AgregaSQLColumnasAlias( Agente:TSQLAgenteClient;oCampo: TCampoOpciones;oDatosImpresion : TDatosImpresion;const Banda : integer = -1;const iParametros : integer = 0 );
{$endif}
  procedure IncrementaPagina;
  procedure InicializaPagina;
  function GetPagina:integer;
  function GetEntidadPoliza(const i:integer ) :TipoEntidad;
  function EsFormulaSQL( const sTexto: string ) : Boolean;
  function EsFormulaSQLTexto( const sTexto: string ) : Boolean;
  function GetNombreArchivo( oDatosImpresion : TDatosImpresion; var sError: string ): String;
  function GetNombreExportacion( oDatosImpresion : TDatosImpresion;
                               const sExtDefault : string;
                               var sError: string
                               {$IFDEF TRESS_REPORTES}
                               ; const sNombre: String
                               {$ENDIF}
                               ): string;

  procedure ParametrosEspeciales( eEntidad : TipoEntidad ; oParamList : TZetaParams; oFiltros, oGrupos : TStrings );

  procedure ParametrosReportes( DataSet, cdsCondiciones : TDataSet; FParams: TZetaParams;
                                const FFiltroFormula, FFiltroDescrip : string;
                                const lSoloTotales,lVerConfidencial,lGeneraBitacora,lContieneImagenes : Boolean );
  function GetTipoFormatoForma( const eTipo: eFormatoFormas ): eTipoFormato;
  function DialogoExporta( var sArchivo : string; const eTipo: eTipoFormato ): Boolean;

implementation

 	

uses DGlobal,
     ZDiccionTools,
     DCliente,
     DReportes,
     {$ifdef RDD}
     {$else}
     DDiccionario,
     {$endif}
     ZGlobalTress,
     ZReportModulo,
     ZetaCommonTools,
     ZReportConst,
     {$ifndef TRESS_REPORTES}
     FBuscaCampos_DevEx,
     {$endif}
     ZFuncsCliente,
     ZetaRegistryCliente,
     FAutoClasses;

var
  FOrOperador, FANDOperador, FIgual, FInLista, FFormula2, FFormula, FMenor,
  FMayor, FVerdadero, FFalso : string;
  FPagina : integer;


{*************** TCampoMaster ********************}
function TCampoMaster.GetNombreCampo : string;
begin
     if PosAgente >= 0 then
        Result := GetNColumna(PosAgente)
     else Result := Formula;
end;

procedure TCampoMaster.GetDatosDataSet( cdsDataSet : TDataSet );
begin
     with cdsDataSet do
     begin
          Entidad := TipoEntidad( FieldByName('CR_TABLA').AsInteger );
          Titulo := FieldByName('CR_TITULO').AsString;
          if Titulo = '' then Titulo := SINTITULO;
             Formula := FieldByName('CR_FORMULA').AsString;
     end;
end;

constructor TGrupoOpciones.Create;
begin
     ListaEncabezado := TStringList.Create;
     ListaPie := TStringList.Create;
end;

destructor TGrupoOpciones.Destroy;
begin
     inherited;
     ListaEncabezado.Free;
     ListaPie.Free;
end;

function TGrupoOpciones.GetNombreCampo: string;
begin
     Result := inherited GetNombreCampo;
end;

{ TCampoOpciones }
function TCampoOpciones.AsTextoTotal(FDataSet: TDataSet; const lSoloTotales, lImportar: Boolean): String;
var
   FField: TField;
   oValue: eTipoOperacionCampo;
begin
     Result := '';
     with SQLColumna do
     begin
          if EsConstante then
             Result := ValorConstante
          else
          begin
               if lImportar then
                  oValue := OpImp
               else
                   oValue := Operacion;
               if lSoloTotales and not ( oValue in [ ocNinguno, ocAutomatico ] ) then
                  FField := FDataSet.FieldByName( GetNombreTotal )
               else
                   FField := FDataSet.Fields[ PosCampo ];
               if TipoImp = tgFecha then
                  Result := FechaAsStr( FField.AsDateTime )
               else
                   Result := FField.AsString;
          end;
     end;

     Result := FormateaValor( Result,
                              StrDef( Mascara, GetMascaraDefault( TipoImp ) ),
                              TipoImp,
                              FALSE );
     { EZM }
     if ( Ancho > 0 ) then
        Result := Copy( Result, 1, Ancho );
end;

function TCampoOpciones.AsTextoTotal(FDataSet: TDataSet;const lSoloTotales :Boolean): String;
{GA: Esta es la llamada que se hace desde ReportASCII }
begin
     Result := AsTextoTotal( FDataset, lSoloTotales, True );
end;

function TCampoOpciones.AsTexto(FDataSet: TDataSet): String;
begin
     Result := '';
     if StrLleno(Formula) then
     begin
          with SQLColumna do
          begin
               if EsConstante then
                  Result := ValorConstante
               else
               begin
                    if TipoImp = tgFecha then
                       Result := FechaAsStr( fDataSet.Fields[PosCampo].AsDateTime )
                    else Result := fDataSet.Fields[PosCampo].AsString;
               end;
          end;

          Result := FormateaValor( Result,
                                   StrDef( Mascara, GetMascaraDefault(TipoImp)),
                                   TipoImp, FALSE );
          { EZM }
          if ( Ancho > 0 ) then
               Result := Copy( Result, 1, Ancho );
     end;
end;

constructor TCampoOpciones.Create;
begin
     Operacion := ocNinguno;
     {$ifdef ADUANAS}
     AutoStretch := FALSE;
     {$endif}
end;

procedure TCampoOpciones.GetDatosDataSet(cdsDataSet: TDataSet);
begin
     inherited;
     with cdsDataset do
     begin
          Requiere := FieldByName('CR_REQUIER').AsString;
          Calculado := FieldByName('CR_CALC').AsInteger;
          Mascara := FieldByName('CR_MASCARA').AsString;
          Ancho := FieldByName('CR_ANCHO').AsInteger;
          TipoCampo := eTipoGlobal( FieldByName('CR_TFIELD').AsInteger );
     end;

end;

function TCampoOpciones.GetNombreCampo: string;
begin
     if SQLColumna <> NIL then
     begin
          with SQLColumna do
            if EsConstante then
               Result := FormateaValor( ValorConstante,Mascara,TipoImp )
            else Result := Inherited GetNombreCampo;
     end
     else Result := Inherited GetNombreCampo;
end;

procedure TCampoListado.GetDatosDataSet(cdsDataSet: TDataSet);
 var i : integer;
begin
     inherited;
     i:= cdsDataSet.FieldByName('CR_OPER').AsInteger;
     if (i<Ord(Low(eTipoOperacionCampo))) OR
        (i>Ord(High(eTipoOperacionCampo))) then
        i := 0;

     Operacion := eTipoOperacionCampo( i );
     {$ifdef ADUANAS}
     AutoStretch := cdsDataset.FieldByName('CR_SHOW').AsInteger = 1;
     {$endif}

end;

{ TCampoIRapida }
function TCampoIRapida.AsTexto(FDataSet: TDataSet): String;
begin
     if StrLleno(Formula) then
     begin
          if SQLColumna.EsConstante AND
             ((SQLColumna.ValorConstante = K_PAGINA) OR ( SQLColumna.ValorConstante = K_CICLO ) OR (SQLColumna.ValorConstante = K_CICLOLETRA )) then
          begin
               Result := SQLColumna.ValorConstante;
               if (Result = K_PAGINA) then
               begin
                    Result := InttoStr(FPagina);
                    Result := FormateaValor( Result,
                                        StrDef( Mascara, GetMascaraDefault(tgNumero)),
                                        tgNumero, FALSE );
                    if ( Ancho > 0 ) then
                       Result := Copy( Result, 1, Ancho );
               end
               else
               begin
                    //Se asume que son la funcion CICLO o CICLOLETRA
                    if ( Ancho > 0 ) then
                       Result := PadR( Result, Ancho ); //<-- Se Esta usando PadR, para que en el ultimo IF del
                                                        //metodo, no me justifique el texto, porque la justificacion
                                                        //se hace mas adelante en ConvierteFCiclo. El PadR,me respeta el
                                                        //ancho que tiene mi Columna.
               end
          end
          else
              Result := inherited AsTexto( FDataSet );

          if ( Ancho > 0 ) then
             case Justificacion of
                 taRightJustify : Result := PadL( Result, Ancho );
                 taCenter       : Result := PadC( Result, Ancho );
             end;
     end;
end;

procedure TCampoIRapida.GetDatosDataSet(cdsDataSet: TDataSet);
begin
     inherited;
     with cdsDataSet do
     begin
          {Para reportes de Impresion Rapida}
          TCorto := Titulo;
          Fuente := [];
          if FieldByName('CR_BOLD').AsString = 'S' then
             Fuente :=  [ eNegrita ];
          if FieldByName('CR_ITALIC').AsString = 'S' then
             Fuente :=  Fuente + [ eItalica ];
          if FieldByName('CR_SUBRAYA').AsString = 'S' then
             Fuente :=  Fuente + [ eSubrayado ];
          if FieldByName('CR_STRIKE').AsString = 'S' then
             Fuente :=  Fuente + [ eComprimido ];

          Justificacion := TAlignment( FieldByName('CR_ALINEA').AsInteger );

          Columna := FieldByName('CR_SHOW').AsInteger;
          Renglon := FieldByName('CR_SUBPOS').AsInteger;
          Banda := eTipoBanda( FieldByName('CR_COLOR').AsInteger);
     end;
end;

function TCampoOpciones.GetNombreTotal: string;
begin
     Result := 'TOTAL_'+IntToStr(PosAgente);
end;


{ TFiltroOpciones }
procedure TFiltroOpciones.GetDatosDataSet(cdsDataSet: TDataSet);
 var dInicio,dFin : TDate;
     sDescrip: string;
begin
     inherited;
     with cdsDataSet do
     begin
          TipoRango := eTipoRango( FieldByName('CR_CALC').AsInteger );
          TipoCampo := eTipoGlobal( FieldByName('CR_TFIELD').AsInteger );
          sDescrip := FieldByName('CR_DESCRIP').AsString;
          {$ifdef RDD}
          if ( sDescrip = K_EMPLEADO ) then
             ValorActivo := vaEmpleado
          else if ( sDescrip = K_IMSS_PATRON ) then
               ValorActivo := vaImssPatron
          else if ( sDescrip = K_IMSS_YEAR ) then
               ValorActivo := vaImssYear
          else if ( sDescrip = K_IMSS_MES  ) then
               ValorActivo := vaImssMes
          else if ( sDescrip = K_IMSS_TIPO  ) then
               ValorActivo := vaImssTipo
          else if ( sDescrip = K_YEAR  ) then
               ValorActivo := vaYearNomina
          else if ( sDescrip = K_TIPO  ) then
               ValorActivo := vaTipoNomina
          else if ( sDescrip = K_NUMERO  ) then
               ValorActivo := vaNumeroNomina
          {$ifdef CAJAAHORRO}
          else if ( sDescrip = K_CAJA_AHORRO  ) then
               ValorActivo := vaCajaAhorro
          {$endif}
          else
              ValorActivo := eRddValorActivo(StrToIntDef(sDescrip,0));
          {$else}
          ValorActivo := sDescrip;
          {$endif}
          TipoRangoActivo := eTipoRangoActivo( FieldByName('CR_ANCHO').AsInteger );
          if {$ifdef RDD}(ValorActivo=vaSinValor){$else}(ValorActivo=VACIO){$endif}AND
             (TipoRangoActivo = raActivo) then
             TipoRangoActivo := raLista;
          Numero := FieldByName('CR_OPER').AsInteger;
          case TipoRango of
               rNinguno : RangoInicial := FieldByName('CR_REQUIER').AsString;
               rFechas :
               begin
                    RangoFechas := eRangoFechas(FieldByName('CR_COLOR').AsInteger);
                    GetFechasRango( RangoFechas,dInicio,dFin);
                    FechaInicial := dInicio;
                    FechaFinal := dFin;
               end;
               rBool : Posicion := StrToIntDef( FieldByName('CR_REQUIER').AsString, 0 );
               else
                   case TipoRangoActivo of
                        raRango :
                        begin
                             RangoInicial := FieldByName('CR_REQUIER').AsString;
                             RangoFinal := FieldByName('CR_MASCARA').AsString;
                        end;
                        raLista : Lista := FieldByName('CR_REQUIER').AsString;
                   end;
          end;
     end;
end;

procedure TGrupoOpciones.GetDatosDataSet(cdsDataSet: TDataSet);
begin
     inherited;
     with cdsDataSet do
     begin
          Encabezado := FieldByName('CR_TFIELD').AsInteger = 1;
          PieGrupo := FieldByName('CR_SHOW').AsInteger = 1;
          SaltoPagina := FieldByName('CR_CALC').AsInteger = 1;
          Totalizar := FieldByName('CR_OPER').AsInteger = 1;
          Master := FieldByName('CR_ANCHO').AsInteger = 1;
          Calculado := FieldByName('CR_ALINEA').AsInteger;
     end;
end;

procedure TOrdenOpciones.GetDatosDataSet(cdsDataSet: TDataSet);
begin
     inherited;
     Calculado := cdsDataSet.FieldByName('CR_COLOR').AsInteger;
     DireccionOrden := cdsDataSet.FieldByName('CR_CALC').AsInteger;
end;

{$ifdef RDD}
function GetDiccionRecord( cdsDataSet : TDataSet ) : TDiccionRecord;
begin
     with Result, cdsDataSet do
     begin
          DI_CLASIFI := TipoEntidad(FieldByName('EN_CODIGO').AsInteger);
          DI_TABLA   := FieldByName('EN_TABLA').AsString;
          DI_NOMBRE  := FieldByName('AT_CAMPO').AsString;
          DI_TITULO  := FieldByName('AT_TITULO').AsString;
          //V2.10.110 EL USO DE DI_CALC SE DESCONTINUA
          //DI_CALC    := FieldByName('DI_CALC').AsInteger;
          DI_ANCHO   := FieldByName('AT_ANCHO').AsInteger;
          DI_MASCARA := FieldByName('AT_MASCARA').AsString;
          DI_TFIELD  := eTipoGlobal(FieldByName('AT_TIPO').AsInteger);
          //DI_ORDEN   := FieldByName('AT_ORDEN').AsInteger=1;
          //V2.10.110 EL USO DE DI_REQUIER SE DESCONTINUA
          //DI_REQUIER := FieldByName('DI_REQUIER').AsString;
          DI_TRANGO  := eTipoRango(FieldByName('AT_TRANGO').AsInteger);
          {$ifdef RDD}
          DI_VALORAC := eRDDValorActivo( FieldByName('AT_VALORAC').AsInteger );
          {$else}
          DI_VALORAC := FieldByName('AT_VALORAC').AsString;
          {$endif}
          DI_RANGOAC := FieldByName('AT_FILTRO').AsInteger;
          if ( DI_TRANGO = rRangoEntidad ) then
          begin
               DI_NUMERO  := FieldByName('AT_ENTIDAD').AsInteger;
               {*** US 15273: Los tipos de periodo que tienen descripci�n vac�a se siguen mostrando en en lookup/cat�lago ***}
               if DI_NUMERO = enTPeriodo then
                  DI_LOOK_DESCRIP := Format( 'TF(%d,%s)', [Ord( lfTipoPeriodo ), DI_TABLA+'.'+DI_NOMBRE])
               else
               {*** FIN ***}
                   DI_LOOK_DESCRIP := FieldByName('EN_ATDESC').AsString;
          end
          else if ( DI_TRANGO = rRangoListas ) then
          begin
               DI_NUMERO  := FieldByName('LV_CODIGO').AsInteger;
               If DI_NUMERO = Ord(lfSexo) then
                  DI_LOOK_DESCRIP := 'SEXO()'
               else
                   DI_LOOK_DESCRIP := Format( 'TF(%d,%s)', [Ord( DI_NUMERO ), DI_TABLA+'.'+DI_NOMBRE]);
          end;
          DI_TCORTO  := FieldByName('AT_TCORTO').AsString;
          DI_OPER    := eTipoOperacionCampo( FieldByName('AT_TOTAL').AsInteger );
     end;
end;
{$else}

 	

function GetDiccionRecord( cdsDataSet : TDataSet ) : TDiccionRecord;
begin
     with Result, cdsDataSet do
     begin
          DI_CLASIFI := TipoEntidad(FieldByName('DI_CLASIFI').AsInteger);
          DI_NOMBRE  := FieldByName('DI_NOMBRE').AsString;
          DI_TITULO  := FieldByName('DI_TITULO').AsString;
          DI_CALC    := FieldByName('DI_CALC').AsInteger;
          DI_ANCHO   := FieldByName('DI_ANCHO').AsInteger;
          DI_MASCARA := FieldByName('DI_MASCARA').AsString;
          DI_TFIELD  := eTipoGlobal(FieldByName('DI_TFIELD').AsInteger);
          DI_ORDEN   := FieldByName('DI_ORDEN').AsString='S';
          DI_REQUIER := FieldByName('DI_REQUIER').AsString;
          DI_TRANGO  := eTipoRango(FieldByName('DI_TRANGO').AsInteger);
          DI_NUMERO  := FieldByName('DI_NUMERO').AsInteger;
          {$ifdef RDD}
          DI_VALORAC := eRDDValorActivo( FieldByName('DI_VALORAC').AsInteger );
          {$else}
          DI_VALORAC := FieldByName('DI_VALORAC').AsString;
          {$endif}
          DI_RANGOAC := FieldByName('DI_RANGOAC').AsInteger;
          DI_TCORTO  := FieldByName('DI_TCORTO').AsString;
          DI_TABLA   := FieldByName('DI_TABLA').AsString;
     end;
end;
{$endif}

procedure DiccionToCampoMaster( const oDiccion: TDiccionRecord;oCampo: TCampoMaster);
begin
     with oCampo,oDiccion do
     begin
          Entidad := DI_CLASIFI;
          Tabla := DI_TABLA;
          if DI_CLASIFI = enFunciones then
             Formula := DI_REQUIER
          else Formula := DI_TABLA+'.'+DI_NOMBRE;
          Titulo := DI_TITULO;
          TCorto := DI_TCORTO;
          TipoCampo := DI_TFIELD;
          Calculado := DI_CALC;
     end;
end;

function AntesDeAgregarDato( const oEntidad : TipoEntidad;
                             const lRepetidos,lTodos : Boolean;
                             var oDiccion : TDiccionRecord;
                             oLista : TZetaSmartLists;
                             const lConRelaciones: Boolean= TRUE ):Boolean;overload;
begin
     {$ifndef TRESS_REPORTES}
     oDiccion := FBuscaCampos_DevEx.BuscarCampos( oEntidad, lTodos, lConRelaciones );
     with oDiccion do
          Result := (DI_CLASIFI <> enNinguno) AND
                    (lRepetidos OR
                    (oLista.ListaDisponibles.Items.IndexOf( DI_TITULO ) < 0));
     {$endif}
end;

function AntesDeAgregarDato( const oEntidad : TipoEntidad;
                             const lRepetidos,lTodos : Boolean;
                             var oDiccion : TDiccionRecord;
                             oLista : TZetaSmartLists_DevEx;
                             const lConRelaciones: Boolean= TRUE ):Boolean;overload;
begin
     {$ifndef TRESS_REPORTES}
     oDiccion := FBuscaCampos_DevEx.BuscarCampos( oEntidad, lTodos, lConRelaciones );
     with oDiccion do
          Result := (DI_CLASIFI <> enNinguno) AND
                    (lRepetidos OR
                    (oLista.ListaDisponibles.Items.IndexOf( DI_TITULO ) < 0));
     {$endif}
end;

procedure DespuesDeAgregarDato( oCampo : TCampoMaster;
                                oLista : TZetaSmartLists;
                                oBoton, oControl : TWinControl  );overload;
begin
     with oLista.ListaDisponibles do
     begin
          Items.AddObject( oCampo.Titulo, oCampo );
          oLista.SelectEscogido( Items.Count - 1 );
     end;
     oBoton.Enabled := TRUE;
     if oControl.Enabled then
     begin
          if oControl.ClassNameIs('TCustomEdit') then
             TEdit(oControl).SelectAll;
          oControl.SetFocus
     end;
end;

procedure DespuesDeAgregarDato( oCampo : TCampoMaster;
                                oLista : TZetaSmartLists_DevEx;
                                oBoton, oControl : TWinControl  );overload;
begin
     with oLista.ListaDisponibles do
     begin
          Items.AddObject( oCampo.Titulo, oCampo );
          oLista.SelectEscogido( Items.Count - 1 );
     end;
     oBoton.Enabled := TRUE;
     if oControl.Enabled then
     begin
          if oControl.ClassNameIs('TCustomEdit') then
             TEdit(oControl).SelectAll;
          oControl.SetFocus
     end;
end;


procedure GetListaMascaras( oLista : TStrings; const eTipo : eTipoGlobal );
 procedure Llena( Arreglo: array of string );
  var i : integer;
 begin
      with oLista do
      begin
           try
              BeginUpdate;
              Clear;
              for i := Low( Arreglo ) to High( Arreglo ) do
                  Add( Arreglo[ i ] )
           finally
                  EndUpdate;
           end;
      end;
 end;
begin
     case eTipo of
          tgFloat : Llena( aMascaraFloat );
          tgNumero : Llena( aMascaraEntero );
          tgFecha : Llena( aMascaraFecha );
          tgAutomatico : oLista.Clear;
          else Llena( aMascaraTexto );

     end;
end;

procedure GetSeparador( var oSeparador : TCampoOpciones );
begin
     with oSeparador do
     begin
          Entidad := enNinguno;
          Formula := K_RENGLON;
          Titulo := K_RENGLON_TITULO;
          Ancho := 10;
          TipoCampo := tgTexto;
          Calculado := K_RENGLON_NUEVO;
     end;
end;

function ReportOpenDialog( sArchivo,
                           sDefaultExt,
                           sFilename,
                           sFilter : string  ) : string;

begin
     ReportOpenDialogB( sArchivo,
                        sDefaultExt,
                        sFilename,
                        sFilter ) ;
     Result := sArchivo;
end;

{function ReportOpenDialogB( var sArchivo: string;
                                sDefaultExt,
                                sFilename,
                                sFilter : string  ) : Boolean;
var
   iFilter: Integer;
begin
     Result := ReportOpenDialogFilterB( sArchivo,
                                       sDefaultExt,
                                       sFilter,
                                       iFilter  ) ;
end;  }

function ReportOpenDialogB( var sArchivo: string;
                                sDefaultExt,
                                sFilename,
                                sFilter : string  ) : Boolean;
var
   iFilter: Integer;
begin
    {$IFDEF TRESS_DELPHIXE5_UP}
    if sDefaultExt = 'QR2' then
        Result := ReportOpenDialogFilterB_Plantilla(sArchivo, sDefaultExt, sFilter, iFilter )
    else
        Result := ReportOpenDialogFilterB( sArchivo,
                                       sDefaultExt,
                                       sFilter,
                                       iFilter  ) ;
    {$ELSE}
     Result := ReportOpenDialogFilterB( sArchivo,
                                       sDefaultExt,
                                       sFilter,
                                       iFilter  ) ;
    {$ENDIF}
end;

function DialogoPlantilla( const sArchivo : string ): string;
begin
     Result := ReportOpenDialog( sArchivo,
                                 'QR2',
                                 '*.QR2',
                                 'Plantillas (*.QR2)|*.QR2|Todos (*.*)|*.*' );
end;

function DialogoAsciiB( var sArchivo : string ): boolean;
begin
     Result := ReportOpenDialogB( sArchivo,
                                  'TXT',
                                  '*.TXT',
                                  'Archivos Texto (*.TXT)|*.TXT|Todos (*.*)|*.*' );

end;


function DialogoAscii( sArchivo : string ): string;
begin
     DialogoAsciiB( sArchivo );
     Result := sArchivo;
end;

function DialogoWord( const sArchivo : string ): string;
begin
     Result := ReportOpenDialog( sArchivo,
                                 'DOC',
                                 '*.DOC',
                                 'Documentos - Microsoft Word (*.DOC)|*.DOC|Formato RTF (*.RTF)|*.RTF|Todos (*.*)|*.*' );
end;

function DialogoExcel( const sArchivo : string ): string;
begin
     Result := ReportOpenDialog( sArchivo,
                                 'XLSX',
                                 '*.XLSX',
                                 'Todos los Documentos - Microsoft Excel (*.XLSX)|*.XLSX|*.XLS|*.XLS*|*.XL*|*.XLT|*.XLW|*.XLA|*.XLC|*.XLM|Todos (*.*)|*.*' );
end;

function GetCampoDescripcionGrupos( const FEntidadActiva, FEntidadGrupo : TipoEntidad;
                                    const oDiccion : TDiccionRecord ) : string;
begin
     Result := ZReportModulo.GetCampoDescripcionGrupos( FEntidadActiva, FEntidadGrupo,oDiccion );
end;

{$ifdef RDD}
function GetCampoDescripcion( const oDiccion : TDiccionRecord ) : string;
begin
     with oDiccion do
          Result := DI_LOOK_DESCRIP;
end;
{$else}
function GetCampoDescripcion( const oDiccion : TDiccionRecord ) : string;
begin
     case oDiccion.DI_TRANGO OF
          rRangoEntidad:
          begin
               Result :=  dmDiccionario.GetCampoDescripcion(TipoEntidad(oDiccion.DI_NUMERO));
               if Result = 'COLABORA.PRETTYNAME' then
                  Result := PRETTY;
          end;
          rRangoListas:
          begin
               if oDiccion.DI_TFIELD = tgNumero then
                  Result := 'TF('+IntToStr(Ord(oDiccion.DI_NUMERO))+','+oDiccion.DI_TABLA+'.'+oDiccion.DI_NOMBRE+')';
          end;
     end;
end;
{$endif}

{CONSTRUCCION DE FILTROS}
function GetDescripFiltro(oFiltro : TFiltroOpciones) : string;
begin
     FFormula := oFiltro.Titulo;
     FFormula2 := '';
     FMenor := ' Al ';
     FMayor := ' Del ';
     FVerdadero := 'Si';
     FFalso := 'No';
     FInLista := '=';
     FOrOperador := ',';
     FAndOperador := '';
     FIgual := '';

     case oFiltro.TipoRango of
          rNinguno : Result := FiltroAbierto( oFiltro );
          rRangoEntidad: Result := StrTransAll( FiltroRango( oFiltro, FALSE ), UnaCOMILLA, VACIO );
          rRangoListas: Result := FiltroStatus( oFiltro,  TRUE );
          rFechas : Result := FiltroFechaSQL( oFiltro, TRUE );
          rBool : Result := FiltroBool( oFiltro );
     end;
end;

function GetStringFiltro(oFiltro : TFiltroOpciones; var lDiccion : Boolean; const lCliente : Boolean = FALSE) : string;
begin
     FFormula := oFiltro.Formula;
     FFormula2 := FFormula;
     FMenor := ' <= ';
     FMayor := ' >= ';
     FVerdadero := Comillas( K_GLOBAL_SI );
     FFalso := Comillas( K_GLOBAL_NO );
     FInLista := ' in';
     FOrOperador := ' OR ';
     FAndOperador := ' AND ';
     FIgual := ' = ';

     lDiccion := oFiltro.TipoRango <>rNinguno;
     case oFiltro.TipoRango of
          rNinguno : Result := FiltroAbierto( oFiltro );
          rRangoEntidad: Result := FiltroRango( oFiltro, FALSE );
          rRangoListas: Result := FiltroStatus( oFiltro, FALSE );
          rFechas : Result := FiltroFechaSQL( oFiltro, lCliente );
          rBool : Result := FiltroBool( oFiltro );
     end;
end;

function GetCampoRepFiltro( DataSet : TDataSet ; var lDiccion : Boolean; var Entidad : TipoEntidad ) : string;
 var oFiltro : TFiltroOpciones;
begin
     oFiltro := GetFiltroOpciones( DataSet );
     Entidad := oFiltro.Entidad;
     Result := GetStringFiltro( oFiltro, lDiccion );
end;

function GetFiltroOpciones( CampoRep : TDataSet ) : TFiltroOpciones;
begin
     Result := TFiltroOpciones.Create;
     Result.GetDatosDataSet( CampoRep );
end;



function FiltroRango( const oFiltro : TFiltroOpciones;
                      const lCliente : Boolean ) : string;
var i : integer;
    sInicial, sFinal,sValor : string;
    fListaGeneral : TStringList;

    function GetFiltrolista( const sCampo, sValores: String ): String;
     var i: integer;
    begin
         //fListaGeneral := TStringList.Create;
         fListaGeneral.CommaText := Trim( sValores );
         for i := 0 to fListaGeneral.Count - 1 do
             if StrLleno(fListaGeneral[ i ]) then
             begin
                  if StrToIntDef( Trim( fListaGeneral[ i ] ), -1 ) = -1 then
                     raise Exception.Create('Error en Filtro : '+oFiltro.Titulo +'.'+ CR_LF+ 'No Se Aceptan Letras');
                  if StrLleno( Result ) then
                     Result := Result + ',' + fListaGeneral[ i ]
                  else Result := fListaGeneral[ i ];
             end;
         if StrLleno( sCampo ) and StrLleno( Result ) then
            Result := sCampo + FInLista + ' ( ' + Result + ' )'
         else
             Result := '';
    end;

begin
     Result := '';
     fListaGeneral := TStringList.Create;
     try
        case oFiltro.TipoRangoActivo of
             raActivo :
             begin
                  sValor := dmCliente.GetValorActivo(oFiltro.ValorActivo);
                  if StrLleno(sValor) then
                     Result := FFormula + ' = ' + sValor
                  else
                      {$ifdef RDD}
                      raise Exception.Create( Format( 'El valor activo %s no est� definido',
                                                      [ObtieneElemento( lfRDDValorActivo, Ord( oFiltro.ValorActivo ) )] ));
                      {$else}
                      raise Exception.Create( 'El valor activo %s no est� definido' );
                      {$endif}
             end;
             raRango :
             begin
                  sInicial := Trim( oFiltro.RangoInicial );
                  sFinal := Trim( oFiltro.RangoFinal );

                  if oFiltro.TipoCampo in[ tgBooleano, tgFecha, tgTexto, tgMemo ] then
                  begin
                       if StrLleno( sInicial ) then sInicial := Comillas( sInicial );
                       if StrLleno( sFinal ) then sFinal := Comillas( sFinal );
                  end;
                  Result := ZetaCommonTools.GetFilterRango( FFormula, FFormula2, sInicial, sFinal );
             end;
             raLista:
             begin
                  if lCliente then
                  begin
                       Result := FFormula + '$' + EntreComillas( oFiltro.Lista );
                  end
                  else
                  begin
                       if oFiltro.TipoCampo in [ tgFloat,tgNumero ] then
                          Result := GetFiltrolista( FFormula, Trim( oFiltro.Lista ) )
                       else
                       begin
                            //fListaGeneral := TStringList.Create;
                            fListaGeneral.CommaText := Trim( oFiltro.Lista );
                            for i := 0 to fListaGeneral.Count - 1 do
                                if StrLleno(fListaGeneral[ i ]) then
                                begin
                                     if StrLleno( Result ) then
                                        Result := Result + FOrOperador + FFormula2 + FIgual + Comillas( fListaGeneral[ i ] )
                                     else Result := FFormula + ' = ' + Comillas( fListaGeneral[ i ] );
                                end;
                       end;
                  end;
             end;
        end;//case
     finally
            FreeAndNil(FListaGeneral);
     end;
end;

function FiltroAbierto( const oFiltro : TFiltroOpciones ) : string;
begin
     Result := '';
     if StrLleno( oFiltro.RangoInicial ) then
        Result := FFormula + ' ' + oFiltro.RangoInicial
     else Raise Exception.Create( 'El Filtro  "' + oFiltro.Titulo + '" Se encuentra vac�o' );
end;

function FiltroStatus( const oFiltro : TFiltroOpciones; const lDescrip : Boolean ) : string;

   function GetFiltroStatusDescripcion: string;
    var oLista : TStrings;
        i : integer;
   begin
        Result := '';
        oLista := TStringList.Create;
        try
           oLista.CommaText := Trim( oFiltro.Lista );
           for i := 0 to oLista.Count - 1 do
               Result := ConcatString( Result, ObtieneElemento( ListasFijas( oFiltro.Numero ),  StrToInt( oLista[i] )-GetOffSet(ListasFijas( oFiltro.Numero )) ),',' );
           Result := FFormula + ':' + Result;
        finally
               oLista.Free;
        end;
   end;

   function AgregaCampo( const sLista, sCampo: String ): String;
   begin
        Result := sLista + sCampo + ',' + CR_LF;
   end;

 var sDescrip, sInicial, sFinal : string;
     j : integer;
begin
     Result := '';
     case oFiltro.TipoRangoActivo of
          raActivo : Result := FFormula + ' = ' + dmCliente.GetValorActivo(oFiltro.ValorActivo);
          raLista :
          begin
               if lDescrip then
               begin
                    Result := GetFiltroStatusDescripcion;
               end
               else
               begin
                    sFinal := Trim( oFiltro.Lista );
                    while Length( sFinal ) > 0 do
                    begin
                         j := Pos( ',', sFinal );
                         if ( j > 0 ) then sInicial := Copy( sFinal, 1, ( j - 1 ) )
                         else
                         begin
                              sInicial := sFinal;
                              sFinal := '';
                         end;
                         sDescrip := AgregaCampo( sDescrip, sInicial );
                         if StrLleno( Result ) then Result := Result +  FOrOperador;
                         if oFiltro.TipoCampo in [ tgBooleano, tgTexto ] then
                            sInicial := Comillas( ObtieneElemento( ListasFijas( oFiltro.Numero ),  StrToInt( sInicial ) ) );
                         Result := Result + FFormula + ' = ' + sInicial;
                         sFinal := Copy( sFinal, ( j + 1 ), MaxInt );
                    end;
               end;
          end;
     end;//case
end;

function FiltroBool( const oFiltro : TFiltroOpciones ) : string;
begin
     case oFiltro.Posicion of
          0: Result := FFormula + '=' + FVerdadero;
          1: Result := FFormula + '=' + FFalso
          else Result := VACIO;
     end;
end;

function rComillas( const sExpresion: String ): String;
begin
     if ( sExpresion <> VACIO ) then
        Result := COMILLA+ sExpresion +COMILLA;
end;

function FiltroFechaSQL( const oCampo : TFiltroOpciones; const lCliente : Boolean ) : string;
 function TipoRangoFecha( dInicial, dFinal : TDate ) : integer;
 begin
   if ( dInicial <> NullDateTime ) AND ( dFinal <> NullDateTime ) then Result := 0
   else if ( dInicial <> NullDateTime ) then Result := 1
   else if ( dFinal <> NullDateTime ) then Result := 2
   else Result := 3;
 end;

 var dInicial , dFinal : TDate;
     lfinal, lInicial : Boolean;
begin
     Result := '';
     //lInicial := StrVacio( oCampo.RangoInicial ) OR ( StrToDate( oCampo.RangoInicial ) = NullDateTime );
     //lFinal := StrVacio( oCampo.RangoFinal ) OR ( StrToDate( oCampo.RangoFinal ) = NullDateTime ) ;
     lInicial := oCampo.FechaInicial = NullDateTime ;
     lFinal := oCampo.FechaFinal = NullDateTime ;

     if lInicial AND lFinal then
     begin
          dInicial := dmCliente.FechaDefault;
          dFinal := dmCliente.FechaDefault;
     end
     else
     begin
          if lInicial then dInicial := NullDateTime
          else dInicial := oCampo.FechaInicial;

          if lFinal then dFinal := NullDateTime
          else dFinal := oCampo.FechaFinal;
     end;

     if lCliente then
         case TipoRangoFecha( dInicial, dFinal ) of
              0: Result := FFormula + FMayor + rComillas( FormatDateTime('dd/mm/yyyy', dInicial ) )+ FAndOperador +
                           FFormula2 + FMenor + rComillas( FormatDateTime('dd/mm/yyyy',dFinal ) );
              1: Result := FFormula + FMayor + rComillas( FormatDateTime('dd/mm/yyyy',dInicial ) );
              2: Result := FFormula + FMenor + rComillas( FormatDateTime('dd/mm/yyyy',dFinal ) );
         end
     else
         case TipoRangoFecha( dInicial, dFinal ) of
              0: Result := FFormula + FMayor + DateToStrSQLC( dInicial ) + FAndOperador +
                           FFormula2 + FMenor + DateToStrSQLC( dFinal );
              1: Result := FFormula + FMayor + DateToStrSQLC( dInicial );
              2: Result := FFormula + FMenor + DateToStrSQLC( dFinal );
         end;
end;



function DirPlantilla : string;
begin
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
end;

function GetMascaraDefault( const eTipo : eTipoGlobal ) : string;
begin
     case eTipo of
          tgFloat : Result := K_MASCARA_FLOAT;
          tgNumero : Result := K_MASCARA_INT;
          tgFecha : Result := K_MASCARA_DATE
          else Result := '';
     end;
end;

function GetFormatFecha( const dFecha : TDate; const sMascara : string ): string;
begin
     if dFecha = NullDateTime then Result := ''
     else Result := FormatDateTime( sMascara, dFecha );
end;

function FormateaValor( const sValor: string;
                               sMascara : string;
                         const eTipo : eTipoGlobal;
                         const lComillas : Boolean ): string;

 function Comillaz( const sTexto : string ) : string;
 begin
      if lComillas then Result := EntreComillas(sTexto)
      else Result := sTexto;
 end;
begin
     if sMascara = '' then sMascara := GetMascaraDefault(eTipo);
     case eTipo of
          tgFloat    : Result := FormatFloat( sMascara, StrToReal( sValor ));
          tgNumero   : Result := FormatCurr( sMascara,StrToIntDef( sValor, 0 ));
          tgFecha    : Result := Comillaz(GetFormatFecha(StrAsFecha( sValor ),sMascara))
          else
          begin
               if sMascara > '' then
                  Result := Comillaz(FormatMaskText(sMascara,sValor))
               else Result := Comillaz(sValor);
          end;
     end;
end;

function GetNColumna( const i : integer) : string;
begin
     Result := K_COLUMNA+IntToStr(i);
end;

procedure GetFechasRango( const eTipo: eRangoFechas;
                          var dInicial,dFinal : TDate );
 var dValor: TDate;
begin
     dInicial := NullDateTime;
     dFinal := NullDateTime;
     dValor := dmCliente.FechaDefault;
     ZReportModulo.GetFechasRango( eTipo, dInicial,dFinal );
     if dInicial = NullDateTime then
        case eTipo of
          rfSemanaActual :
          begin
               dInicial := LastMonday(dValor);
               dFinal := dInicial+6;
          end;
          rfQuincenaActual:
          begin
               dInicial := FirstDayOfQuincena(dValor);
               dFinal := LastDayOfQuincena(dValor);
          end;
          rfMesActual :
          begin
               dInicial := FirstDayOfMonth(dValor);
               dFinal := LastDayOfMonth(dInicial);
          end;
          rfBimestreActual :
          begin
               dInicial := FirstDayOfBimestre(dValor);
               dFinal := LastDayOfBimestre(dValor);
          end;
          rfYearActual :
          begin
               dInicial := FirstDayOfYear(TheYear(dValor));
               dFinal := LastDayOfYear(TheYear(dInicial));
          end;
          rfAyer :
          begin
               dInicial := dValor-1;
               dFinal := dInicial;
          end;
          rfLunesPasado :
          begin
               dInicial := LastMonday(dValor);
               dFinal := dInicial;
          end;
          rfSemanaPasada :
          begin
               dInicial := LastMonday(dValor)-7;
               dFinal := dInicial+6;
          end;
          rfQuincenaPasada:
          begin
               dInicial := FirstDayOfLastQuincena(dValor);
               dFinal := LastDayOfLastQuincena(dValor);
          end;
          rfMesPasado :
          begin
               dInicial := FirstDayOfMonth(LastMonth(dValor));
               dFinal := LastDayOfMonth(dInicial);
          end;
          rfBimestrePasado:
          begin
               dInicial := FirstDayOfBimestre(FirstDayOfBimestre(dValor)-1);
               dFinal := LastDayOfBimestre(dInicial);
          end;
          rfYearPasado :
          begin
               dInicial := FirstDayOfYear(TheYear(dValor)-1);
               dFinal := LastDayOfYear(TheYear(dInicial));
          end
          else
          begin
               dInicial := dValor;
               dFinal := dInicial;
          end;
     end;
end;

function ModuloGraficas : Boolean;
begin
     Result := dmCliente.ModuloAutorizado( okGraficas );
end;

procedure AgregaListaObjeto( cdsDataSet:TDataset;
                             oLista:TStrings );
 var oParam, oMaster : TCampoMaster;
     oGrupo : TGrupoOpciones;
     oCampo : TCampoListado;
     oFiltro : TFiltroOpciones;
     oOrden : TOrdenOpciones;
begin
     oMaster := NIL;
     case eTipoCampo( cdsDataSet.FieldByName('CR_TIPO').AsInteger ) of
          tcCampos :
          begin
               oCampo := TCampolistado.Create;
               oMaster := oCampo;
          end;
          tcGrupos :
          begin
               oGrupo := TGrupoOpciones.Create;
               oMaster := oGrupo;
          end;
          tcOrden :
          begin
               oOrden := TOrdenOpciones.Create;
               oMaster := oOrden;
          end;
          tcEncabezado, tcPieGrupo :
          begin
               oCampo := TCampoListado.Create;
               oMaster := oCampo;
          end;
          tcFiltro:
          begin
               oFiltro := TFiltroOpciones.Create;
               oMaster := oFiltro;
          end;
          tcParametro:
          begin
               oParam := TCampoMaster.Create;
               oMaster := oParam;
          end;
     end;
     oMaster.GetDatosDataset(cdsDataSet);
     oLista.AddObject(oMaster.Titulo,oMaster);
end;

{$ifndef CONCILIA}
procedure AgregaSQLColumnas( Agente:TSQLAgenteClient;
                             oCampo: TCampoOpciones;
                             oDatosImpresion : TDatosImpresion;
                             const Banda : integer = -1;
                             const iParametros : integer = 0 );
 var sFormula : string;
     lDiccion : Boolean;
     oEntidad : TipoEntidad;
     iAncho : integer;
begin
     sFormula := oCampo.Formula;
     //if (sFormula>'') AND (oCampo.Calculado<>K_RENGLON_NUEVO) then
     {CV: esta validacion se puso, para que cuando haya formulas vacias o
     salto de renglon, no afectara la posicion del RESULT, pues antes, cuando
     habia formulas vacias o salto de renglon no se mandaban al servidor
     y el servidor pensaba que habia menos columnas de las que realmente hay.}
     if (oCampo.Calculado=K_RENGLON_NUEVO) then sFormula := '';

     lDiccion := oCampo.Calculado=0;
     if lDiccion then oEntidad := TipoEntidad(oCampo.Entidad)
     else oEntidad := enNinguno;

     if ( oDatosImpresion.Tipo in [ tfASCIIDel,tfCSV ]) then
        iAncho := 0
     else
     begin
          {$ifdef ADUANAS}
          if oCampo.AutoStretch then
             iAncho := K_ANCHO_FORMULA
          else
          {$endif}
              iAncho := oCampo.Ancho;
     end;

     oCampo.PosAgente := Agente.AgregaColumna( TransfomaParams( sFormula, iParametros, Agente.Parametros ),
                                               lDiccion,
                                               oEntidad,
                                               oCampo.TipoCampo,
                                               iMax(iAncho,0){iMax(oCampo.Ancho,0)},
                                               '',
                                               oCampo.Operacion,
                                               FALSE,
                                               Banda );

end;

procedure AgregaSQLColumnasAlias( Agente:TSQLAgenteClient;
                             oCampo: TCampoOpciones;
                             oDatosImpresion : TDatosImpresion;
                             const Banda : integer = -1;
                             const iParametros : integer = 0 );
 var sFormula : string;
     lDiccion : Boolean;
     oEntidad : TipoEntidad;
     iAncho : integer;
begin
     sFormula := oCampo.Formula;
     //if (sFormula>'') AND (oCampo.Calculado<>K_RENGLON_NUEVO) then
     {CV: esta validacion se puso, para que cuando haya formulas vacias o
     salto de renglon, no afectara la posicion del RESULT, pues antes, cuando
     habia formulas vacias o salto de renglon no se mandaban al servidor
     y el servidor pensaba que habia menos columnas de las que realmente hay.}
     if (oCampo.Calculado=K_RENGLON_NUEVO) then sFormula := '';

     lDiccion := oCampo.Calculado=0;
     if lDiccion then oEntidad := TipoEntidad(oCampo.Entidad)
     else oEntidad := enNinguno;

     if ( oDatosImpresion.Tipo in [ tfASCIIDel,tfCSV ]) then
        iAncho := 0
     else
     begin
          {$ifdef ADUANAS}
          if oCampo.AutoStretch then
             iAncho := K_ANCHO_FORMULA
          else
          {$endif}
              iAncho := oCampo.Ancho;
     end;

     oCampo.PosAgente := Agente.AgregaColumna( TransfomaParams( sFormula, iParametros, Agente.Parametros ),
                                               lDiccion,
                                               oEntidad,
                                               oCampo.TipoCampo,
                                               iMax(iAncho,0){iMax(oCampo.Ancho,0)},
                                               oCampo.Titulo,
                                               oCampo.Operacion,
                                               FALSE,
                                               Banda );

end;


procedure AgregaSQLColumnasParam(Agente:TSQLAgenteClient;oParam : TCampoMaster);
var sFormula : string;
begin
     sFormula := oParam.Formula;
     if sFormula>'' then
     begin
          oParam.PosAgente := Agente.AgregaColumna( sFormula,
                                                    FALSE,
                                                    enNinguno,
                                                    oParam.TipoCampo,
                                                    0 );
     end
     else oParam.PosAgente := -1
end;

procedure AgregaSQLFiltros( Agente:TSQLAgenteClient; oFiltro: TFiltroOpciones;
                            var FFiltroFormula, FFiltroDescrip : string );
 var sFormula : string;
     lDiccion : Boolean;
     oEntidad : TipoEntidad;
begin
     sFormula := oFiltro.Formula;
     if sFormula>'' then
     begin
          sFormula := ZReportTools.GetStringFiltro(oFiltro, lDiccion);

          if lDiccion then oEntidad := oFiltro.Entidad
          else oEntidad := enNinguno;

          if sFormula > '' then
             oFiltro.PosAgente := Agente.AgregaFiltro( sFormula, lDiccion, oEntidad );

          if FFiltroFormula > '' then FFiltroFormula := FFiltroFormula + CR_LF;
          FFiltroFormula := FFiltroFormula + sFormula;
          if FFiltroDescrip > '' then FFiltroDescrip := FFiltroDescrip + CR_LF;
          FFiltroDescrip := FFiltroDescrip + ZReportTools.GetDescripFiltro(oFiltro);
     end
     else oFiltro.PosAgente := -1;
end;

procedure AgregaSQLGrupos(Agente:TSQLAgenteClient; oGrupo: TGrupoOpciones; const iParametros: integer  );
 var sFormula : string;
begin
     with oGrupo do
     begin
          sFormula := Formula;
          if StrLLeno(sFormula) then
          begin
               sFormula := TransfomaParams( sFormula, iParametros, Agente.Parametros );
               Agente.AgregaOrden( sFormula,
                                    TRUE,
                                    Entidad,
                                    Calculado=0 );
               PosAgente := Agente.AgregaColumna( sFormula, FALSE, enNinguno, tgAutomatico, 30,
                                                  '', ocAutomatico, TRUE );
          end
          else PosAgente := -1;
     end;

end;

procedure AgregaSQLOrdenes(Agente:TSQLAgenteClient;oOrden: TOrdenOpciones; const iParametros: integer );
begin
     with oOrden do
          if StrLleno(Formula) then
             PosAgente := Agente.AgregaOrden( TransfomaParams( Formula, iParametros, Agente.Parametros ),
                                              DireccionOrden=0,
                                              Entidad,
                                              Calculado = 0 );
end;

procedure AgregaSQLFiltrosEspeciales(Agente:TSQLAgenteClient; sFiltro : string; const lDeDiccion : Boolean = FALSE; const iParametros : integer = 0);
begin
     sFiltro := Trim( BorraCReturn( sFiltro ) );
     if StrLleno( sFiltro ) then
     begin
          if (EsFormulaSQL(sFiltro)) then
             Agente.AgregaFiltro( TransfomaParams( sFiltro, iParametros, Agente.Parametros ),
                                  FALSE,
                                  enNinguno )
          else
              if lDeDiccion then
                 Agente.AgregaFiltro( sFiltro,
                                      TRUE,
                                      Agente.Entidad )
              else Agente.AgregaFiltro( sFiltro,
                                        FALSE,
                                        enNinguno );
     end;
end;
{$endif}

function TransfomaParams( const sFormula: string;
                          const Cuantos : Integer;
                          Parametros: Olevariant ) : string;
begin
     Result := TransParamNomConfig( sFormula, Cuantos, Parametros, FALSE, TRUE );
end;

function TransParamNomConfig( const sFormula: string;
                              const Cuantos : Integer;
                              Parametros: Olevariant;
                              lForzaSustituye, lConComillas: Boolean ) : string;
 var eTipo : eTipoGlobal;
     uParam : OleVariant;
     i : integer;
     sValor : string;

 function GetValor(const lComillas:Boolean; sValorTemp: string): String;
 begin
      if lComillas and ( eTipo in [tgFecha,tgTexto,tgBooleano] ) then
      begin
           Result:= EntreComillas(sValorTemp)
      end
      else
      begin
           Result:= sValorTemp;
      end;
 end;

begin
     Result := Trim(sFormula);
     If Cuantos > 0 then
     begin
          if EsFormulaSQL( Result ) or lForzaSustituye then
          begin
               if (AnsiPos('#PARAM',UpperCase(Result))>0) then
               begin
                    for i:= Cuantos downto 1 do
                    begin
                         sValor := '';
                         uParam := ZReportConst.GetParametro( Parametros,
                                                              i, eTipo );
                         case eTipo of
                              tgBooleano :
                              begin
                                   if uParam then
                                      sValor := K_GLOBAL_SI
                                   else sValor := K_GLOBAL_NO;
                                   sValor := EntreComillas(sValor);
                              end;
                              tgFloat : sValor := FloatToStr(uParam);
                              tgNumero : sValor := IntToStr(uParam);
                              tgFecha : sValor := FormatDateTime( 'dd/mm/yyyy', uParam);
                              tgTexto : sValor := uParam;
                        end;
                        sValor:= GetValor(lConComillas, sValor);
                        Result := StrTransAll(Result,'#PARAM'+IntToStr(i),sValor);
                    end;
               end;
          end;
     end;
end;

procedure InicializaPagina;
begin
     FPagina := 0;
end;

procedure IncrementaPagina;
begin
     FPagina := FPagina +1;
end;

function GetPagina:integer;
begin
     Result := FPagina;
end;

function GetEntidadPoliza(const i:integer ) :TipoEntidad;
begin
     Result := ZReportModulo.GetEntidadPoliza(i);
end;

function EsFormulaSQL( const sTexto : String ): Boolean;
begin
     // Si Expr es F�rmula, empieza con @ //
     Result := Copy( sTexto, 1, 1 ) = K_SELECT;
end;

function EsFormulaSQLTexto( const sTexto: string ) : Boolean;
begin
      Result := Copy( sTexto, 1, Length(K_SELECT_TEXTO) ) = K_SELECT_TEXTO;
end;

function GetNombreArchivo( oDatosImpresion : TDatosImpresion; var sError: string ): String;
begin
     with oDatosImpresion do
     begin
          if StrVacio(Archivo) then
             Archivo := DirPlantilla + 'DEFAULT.QR2'
          else
          begin
               if ExtractFileExt( Archivo ) = '' then
                  Archivo := Archivo + '.QR2';
               if ExtractFilePath( Archivo ) = '' then
                  Archivo := DirPlantilla + Archivo;

               if NOT (Tipo in [tfASCIIFijo, tfASCIIDel, tfCSV ] )and
                  NOT ( Tipo in TiposMailMerge ) and
                  NOT FileExists( Archivo ) then
               begin
                    {$ifdef HTTP_CONNECTION}
                    if ( ClientRegistry.TipoConexion = conxDCOM ) then
                    {$endif}
                       sError := 'La Plantilla "'+ Archivo +'" no Existe, �Desea que el Listado se Genere sin Plantilla?';
               end;
          end;
          Result := Archivo;
     end;
end;

function GetNombreExportacion( oDatosImpresion : TDatosImpresion;
                               const sExtDefault : string;
                               var sError: string
                               {$IFDEF TRESS_REPORTES}
                               ; const sNombre: String
                               {$ENDIF}
                               ): string;
begin
     with oDatosImpresion do
     begin
          {$ifndef TRESSEMAIL}
          if ( Tipo <> tfImpresora ) then
          {$endif}
          begin
               if ExtractFileName( Exportacion ) = '' then
               begin
                    {$IFDEF TRESS_REPORTES}
                    Exportacion := sNombre;
                    {$ELSE}
                    Exportacion := 'Reporte';
                    {$ENDIF}
               end;

               if ( Tipo in [ tfImpresora, tfHtml, tfPDF ] ) OR
                  ( ExtractFileExt( Exportacion ) = '' ) then
               begin
                    Exportacion := ChangeFileExt( Exportacion, '.' + StrDef( ObtieneElemento( lfExtFormato, Ord( Tipo ) ), sExtDefault ) );
                    //Exportacion := ExtractFileName( Exportacion ) +;
               end;
               if ExtractFilePath( Exportacion ) = '' then
                  Exportacion := DirPlantilla + Exportacion;

               if NOT DirectoryExists( ExtractFileDir( Archivo ) ) then
                  sError := 'El Directorio : '+ ExtractFileDir( Archivo ) +' No Existe';
               Result := Exportacion;
          end;
     end;
end;



{$ifdef TRESS}
procedure FiltrosFechasEspeciales( oParamList : TZetaParams; oFiltros : TStrings; const sParamInicial, sParamFinal, sFiltroFormula : string );
 var i : integer;
begin
     with oParamList do
     begin
          AddDate( sParamInicial, dmCliente.FechaDefault );
          AddDate( sParamFinal, dmCliente.FechaDefault );
     end;
     with oFiltros do
          for i := 0 to Count -1 do
              if TFiltroOpciones(Objects[i]).Formula = sFiltroFormula then
                 with oParamList do
                 begin
                      if TFiltroOpciones(Objects[i]).FechaInicial <> NullDateTime then
                      begin
                         AddDate(sParamInicial, TFiltroOpciones(Objects[i]).FechaInicial );
                      end;
                      if TFiltroOpciones(Objects[i]).FechaFinal <> NullDateTime then
                      begin
                         AddDate(sParamFinal, TFiltroOpciones(Objects[i]).FechaFinal );
                      end;
                      Break;
                 end;
end;

procedure FiltrosEspecialCalen( oParamList : TZetaParams; oFiltros : TStrings; const sTabla : string );
 const sParamInicial = 'dCalenInicial';
       sParamFinal = 'dCalenFinal';
 var i : integer;
begin
     with oParamList do
     begin
          AddDate( sParamInicial, dmCliente.FechaDefault );
          AddDate( sParamFinal, dmCliente.FechaDefault );
          AddString( 'ListaCampos', '0' );
     end;
     with oFiltros do
          for i := 0 to Count -1 do
          begin
               with TFiltroOpciones(Objects[i]) do
                    if Formula = sTabla + '.TL_FEC_MIN' then
                       with oParamList do
                       begin
                            if FechaInicial <> NullDateTime then
                               AddDate(sParamInicial, FechaInicial );
                            if FechaFinal <> NullDateTime then
                               AddDate(sParamFinal, FechaFinal );
                       end
                    else
                    if Formula = sTabla+'.TL_CAMPO' then
                       with oParamList do
                            if TipoRangoActivo = raTodos then
                               AddString('ListaCampos', '' )
                            else AddString('ListaCampos', Lista )
          end;
     //ShowMessage('La Fecha Inicial:'+FechaAsStr(FParamList.ParamByName( sParamInicial).AsDate) +'; Fecha Final '+FechaAsStr(FParamList.ParamByName( sParamFinal).AsDate));
end;

procedure FiltrosConciliacionEspeciales( oParamList : TZetaParams; oFiltros : TStrings; const sParam, sFiltroFormula : string );
 var i, iFolio : integer;
begin
     iFolio := -1;
     oParamList.AddInteger( sParam, 0 );
     with oFiltros do
          for i := 0 to Count -1 do
              with TFiltroOpciones(Objects[i]) do
                   if Formula = sFiltroFormula then
                      with oParamList do
                      begin
                           if TipoRangoActivo = raRango then
                           begin
                                if RangoInicial <> '' then
                                begin
                                     iFolio := StrToIntDef( RangoInicial,0 );
                                     Break;
                                end;
                           end;
                           if TipoRangoActivo = raLista then
                           begin
                                if Lista <> '' then
                                begin
                                     iFolio := StrToIntDef( Lista, 0 );
                                     Break;
                                end;
                           end;
                      end;
   if iFolio = 0 then
      raise Exception.Create('N�mero de Folio a Conciliar, No Puede Ser Cero');

   if NOT(iFolio in [1..5]) then
      raise Exception.Create('N�mero de Folio a Conciliar,  Debe Estar entre 1 y 5');

   if iFolio = -1 then
      raise Exception.Create('No se Especific� N�mero de Folio a Conciliar');

   oParamList.AddInteger( sParam, iFolio );
end;

procedure ParametrosListadoNomina( oParamList : TZetaParams; oGrupos: TStrings );
 var j, i : integer;
begin
     ZFuncsCliente.RegistraFuncListadoTotales;
     oParamList.AddInteger('CountDatosEmpleado',0);
     with oGrupos do
     begin
          for i:=1 to iMin(K_MAXGRUPOS,Count-1) do
          begin
               with TGrupoOpciones(Objects[i]) do
                    if (Pos('CB_CODIGO',Formula) <> 0) then
                    begin
                         oParamList.AddInteger('CountDatosEmpleado',ListaEncabezado.Count);
                         for j:=0 to ListaEncabezado.Count - 1 do
                             oParamList.AddString( Format('DatosEmpl%d',[j]),
                                                   TCampoOpciones(ListaEncabezado.Objects[j]).Formula );
                    end;
          end;
     end;
end;

procedure ParametrosMovimienLista( oParamList : TZetaParams; oGrupos: TStrings );
 var i,j:integer;
begin
     j:= 1;
     ZFuncsCliente.RegistraFuncListadoTotales;
     oParamList.AddBoolean( 'TotalesNeto', TRUE );

     for i:= 1 to K_MAXGRUPOS do
     begin
          oParamList.AddString( 'TA_NIVEL'+IntToStr(i),'');
          oParamList.AddString( 'TIT_NIVEL'+IntToStr(i), '' );
          oParamList.AddString( 'DES_NIVEL'+IntToStr(i), '' );
     end;

     with oGrupos do
     begin
          for i:=1 to iMin(K_MAXGRUPOS,Count-1) do
          begin
               with TGrupoOpciones(Objects[i]) do
                    if (Pos('CB_CODIGO',Formula) = 0) AND
                       (Pos('CO_NUMERO',Formula) = 0) then
                    begin
                         oParamList.AddString( 'TA_NIVEL'+IntToStr(j),Formula );//Codigo del Grupo
                         if ListaEncabezado.Count > 0 then {Quiere decir que tpor lo menos tiene Codigo}
                         begin
                              oParamList.AddString( 'TIT_NIVEL'+IntToStr(j),TCampoOpciones( ListaEncabezado.Objects[0] ).Titulo );//Codigo del Grupo

                              if ListaEncabezado.Count > 1 then {Quiere decir que tiene Codigo y Descripcion}
                                 oParamList.AddString( 'DES_NIVEL'+IntToStr(j),
                                                       TCampoOpciones( ListaEncabezado.Objects[1] ).Formula )//Descripcion del Grupo
                              else
                                  oParamList.AddString( 'DES_NIVEL'+IntToStr(j), '' )
                         end
                         else
                              oParamList.AddString( 'TIT_NIVEL'+IntToStr(i),Titulo );//Codigo del Grupo
                         inc(j)
                    end
                    else if (Pos('CB_CODIGO',Formula) > 0) then
                    begin
                         oParamList.AddBoolean( 'TotalesNeto', Totalizar );
                    end;
          end;
     end;
     oParamList.AddInteger('CountGrupos',j-1);
end;
{$endif}

procedure ParametrosEspeciales( eEntidad : TipoEntidad ; oParamList : TZetaParams; oFiltros, oGrupos : TStrings );
begin
     {$ifdef TRESS}
     case eEntidad of
          enRotacion : FiltrosFechasEspeciales( oParamList, oFiltros, 'dRotaInicial','dRotaFinal','TMPROTA.TR_FECHA');
          enCalendario: FiltrosEspecialCalen( oParamList, oFiltros, 'TMPCALEN');
          enCalendarioHoras : FiltrosEspecialCalen( oParamList, oFiltros, 'TMPCALHR');
          enConcilia : FiltrosConciliacionEspeciales( oParamList, oFiltros, 'iFolio','TMPFOLIO.TO_FOLIO_X');
          enMovimienLista:ParametrosMovimienLista( oParamList, oGrupos );
          enListadoNomina :
          begin
               ParametrosMovimienLista( oParamList, oGrupos );
               ParametrosListadoNomina( oParamList, oGrupos );
          end;
     end;
     {$endif}
end;

procedure ParametrosReportes( DataSet, cdsCondiciones : TDataSet;
                              FParams: TZetaParams;
                              const FFiltroFormula,
                                    FFiltroDescrip : string;
                              const lSoloTotales,
                                    lVerConfidencial,
                                    lGeneraBitacora,
                                    lContieneImagenes : Boolean );
 var sCondicion : string;
begin
     with Dataset do
     begin
          if lSoloTotales then
             sCondicion := 'S�lo Totales'
          else
              sCondicion := 'Detalle por Registro';

          with FParams do
          begin
               AddInteger( 'Codigo', FieldByName( 'RE_CODIGO' ).AsInteger );
               AddInteger( 'TipoReporte', FieldByName( 'RE_TIPO' ).AsInteger );
               AddString( 'Nombre', FieldByName( 'RE_NOMBRE' ).AsString );
               AddString( 'Titulo', FieldByName( 'RE_TITULO' ).AsString );
               AddBoolean( 'GeneraBitacora', lGeneraBitacora );

               {$ifdef CAROLINA}
               AddBoolean( 'GeneraBitacora', TRUE );
               {$endif}

               AddBoolean( 'SoloTotales', lSoloTotales );
               AddString( 'SoloTotalesDes', sCondicion );
               AddInteger( 'TablaPrincipal', FieldByName( 'RE_ENTIDAD' ).AsInteger );

               // Valores FIJOS
               AddBoolean( 'VerConfidencial', lVerConfidencial );
               AddBoolean( 'TotalesGrupos', (eTipoFormato(FieldByName('RE_PFILE').AsInteger)= tfASCIIFijo) );

               sCondicion := VACIO;
               if cdsCondiciones <> NIL then
               begin
                    if StrLleno(FieldByName('QU_CODIGO').AsString) then
                    begin
                         sCondicion := cdsCondiciones.FieldByName('QU_DESCRIP').AsString;
                         if StrLleno( sCondicion ) then
                         begin
                              sCondicion := 'Condici�n : ' + FieldByName('QU_CODIGO').AsString +'='+sCondicion;
                         end;
                    end;
               end;
               AddString( 'Condicion', sCondicion );
               AddString( 'Filtro', FFiltroFormula );
               AddString( 'FiltroDescrip', FFiltroDescrip  );
               AddBoolean( 'ContieneImagenes', lContieneImagenes );

               {$ifdef RDDAPP}
               AddBoolean( 'RDD_APP', TRUE );
               {$else}
               AddBoolean( 'RDD_APP', FALSE );
               {$endif}
          end;
     end;
end;

function GetTipoFormatoForma( const eTipo: eFormatoFormas ): eTipoFormato;
 const

      aPosicion : Array[eFormatoFormas] of eTipoFormato = (
                  tfImpresora,
                  tfHTML,
                  tfQRP,
                  tfTXT,
                  tfXLS,
                  tfRTF,
                  tfWMF,
                  tfPDF,
                  tfBMP,
                  tfTIF,
                  tfPNG,
                  tfJPEG,
                  tfEMF,
                  tfSVG,
                  tfHTM,
                  tfWB1,
                  tfWK2,
                  tfDIF,
                  tfSLK
                  );
begin
     Result := aPosicion[eTipo];

end;

{$IFDEF TRESS_DELPHIXE5_UP}
 function ReportOpenDialogFilterB_Plantilla( var sFilename: string;
                                      sDefaultExt,
                                      sFilter : string;
                                  var iFilter: integer) : Boolean;
var
   OpenDialog: TOpenDialog;
begin
     OpenDialog := TOpenDialog.Create( Application.MainForm );
     OpenDialog.HelpContext := H55172_Impresora_Plantilla;
     try
        with OpenDialog do
        begin
             DefaultExt := sDefaultExt;
             Filename := sFilename;
             Filter := sFilter;
             //Inicializacion del directorio inicial
             InitialDir := zReportTools.DirPlantilla;
             //Si el archivo esta en una ruta distinta a la de plantillas, el directorio inicial sera ese.
             if strLleno( sFileName ) then
                  if strLleno(ExtractFilePath( sFileName )) then
                  begin
                    FileName := ExtractFileName( sFileName );
                    InitialDir := ExtractFilePath( sFileName );
                  end;

             Result := Execute;

             //(@am):Este codigo solo se ejecuta si hubo un cambio de archivo.
             if Result then
             begin
                  //(@am): Asignacion de la ruta que aparecera en el campo NombrePlantilla
                  sFileName := FileName;
                 {***(@am):Si InitialDir y Filename son iguales (Caso en que se busca la plantilla) y
                  la extrancion del path es igual al directorio de reportes del global, entonces el campo NombrePlantilla
                  sera unicamente el nombre del archivo QR2, no toda la ruta.***}
                 if (UpperCase( VerificaDir( ExtractFilePath( FileName ) ) ) = UpperCase(VerificaDir(zReportTools.DirPlantilla))) then
                    sFileName := ExtractFileName( FileName );
                 iFilter := FilterIndex;
             end;

        end;
     finally
            OpenDialog.Free;
     end;
end;
{$ENDIF}

{$IFDEF TRESS_DELPHIXE5_UP}
function ReportOpenDialogFilterB( var sFilename: string;
                                      sDefaultExt,
                                      sFilter : string;
                                  var iFilter: integer) : Boolean;
var
   OpenDialog: TOpenDialog;
   //sFolderInicial: String;
begin
     OpenDialog := TOpenDialog.Create( Application.MainForm );
     OpenDialog.HelpContext := H55172_Impresora_Plantilla;
     try
        with OpenDialog do
        begin
             DefaultExt := sDefaultExt;
             if strLleno( sFileName ) then
             begin
                  FileName := ExtractFileName( sFileName );
                  InitialDir := ExtractFilePath( sFileName );
             end
             else
                 InitialDir := System.IOUtils.TPath.GetDocumentsPath;
             Filter := sFilter;

             Result := Execute;

             if Result then
             begin
                  sFileName := FileName;
                  iFilter := FilterIndex;
             end;
        end;
     finally
            OpenDialog.Free;
     end;
end;

{$ELSE}

function ReportOpenDialogFilterB( var sFilename: string;
                                      sDefaultExt,
                                      sFilter : string;
                                  var iFilter: integer  ) : Boolean;
var
   OpenDialog: TOpenDialog;
begin
     OpenDialog := TOpenDialog.Create( Application.MainForm );
     OpenDialog.HelpContext := H55172_Impresora_Plantilla;

     with OpenDialog do
     begin
          DefaultExt := sDefaultExt;
          Filename := sFilename;
          Filter := sFilter;
          InitialDir := zReportTools.DirPlantilla;
          //FileName := sArchivo;
          Result := Execute;
          if UpperCase( VerificaDir( ExtractFilePath( FileName ) ) ) = UpperCase( VerificaDir( ( InitialDir ) ) ) then
             sFileName := ExtractFileName( FileName )
          else
              sFileName := FileName;
          iFilter := FilterIndex;
     end;
end;
{$ENDIF}

function ReportOpenDialogFilter( sFilename: string;
                                 sDefaultExt,
                                 sFilter : string;
                                 var iFilter: integer  ) : String;
begin
     ReportOpenDialogFilterB( sFilename,
                              sDefaultExt,
                              sFilter,
                              iFilter  );
     Result := sFileName;
end;

function ReportSaveDialog(const sFilename,
                                sDefaultExt,
                                sFilter : string  ) : string;
var
   iFilter: Integer;
begin
     iFilter := 0;
     Result := ReportSaveDialogFilter( sFilename,
                                       sDefaultExt,
                                       sFilter,
                                       iFilter  ) ;
end;


function ReportSaveDialogFilterB( var sFilename: string;
                                      sDefaultExt,
                                      sFilter : string;
                                  var iFilter: Integer ): boolean;
var
   SaveDialog: TSaveDialog;
begin
     SaveDialog := TSaveDialog.Create( Application.MainForm );
     try
        with SaveDialog do
        begin
             DefaultExt := sDefaultExt;
             Filename := sFilename;
             Filter := sFilter;
             FilterIndex := iFilter;
             InitialDir := zReportTools.DirPlantilla;
             Result := Execute;
             if UpperCase( VerificaDir( ExtractFilePath( FileName ) ) ) = UpperCase( VerificaDir( ( InitialDir ) ) ) then
                sFileName := ExtractFileName( FileName )
             else
                 sFileName := FileName;
             iFilter := FilterIndex;
        end;
     finally
            SaveDialog.Free;
     end;
end;

function ReportSaveDialogFilter( sFilename,
                                 sDefaultExt,
                                 sFilter : string;
                             var iFilter: Integer ): string;
begin
     ReportSaveDialogFilterB( sFilename,
                              sDefaultExt,
                              sFilter,
                              iFilter );
     Result := sFileName; 
end;

function DialogoFormas( const sArchivo : string; const eTipo : eFormatoFormas ): string;
  Const K_FILTER =                                                           
                   'HTML (P�ginas Internet)|*.HTML|'                      +  
                   'QRP  (Tipo Preliminar)|*.QRP|'                        +  
                   'TXT  (Archivos Texto)|*.TXT|'                         +
                   'XLSX (Excel - Hoja de C�lculo)|*.XLSX|'                +
                   'RTF  (Texto Enriquecido)|*.RTF|'                      +  
                   'WMF  (Archivos MetaData)|*.WMF|'                      +  
                   'PDF  (Documento de Acrobat)|*.PDF|'                   +  
                   'BMP  (Imagen - Bitmap)|*.BMP|'                        +  
                   'TIF  (Imagen - Tagged Image File)|*.TIF|'             +  
                   'PNG  (Imagen - Portable Network Graphics)|*.PNG|'     +  
                   'JPEG (Imagen - JPEG File Interchange Format)| *.JPG|' +  
                   'EMF  (Imagen - Enhanced Metafiles)|*.EMF|'            +  
                   'SVG  (Scalable Vector Graphics)|*.SVG|'               +  
                   'HTM  (Extended HyperText Markup Language)|*.HTM|'     +  
                   'WB1  (Quattro Pro para Windows)|*.WB1|'               +  
                   'WK2  (Lotus 1-2-3)|*.WK2|'                            +  
                   'DIF  (Excel - Data Interchange Format)|*.DIF|'        +  
                   'SLK  (Excel - Symbolic Link)|*.SLK|'                  ;

 var
    sExt: string;
    iFilter : integer;
begin
     iFilter := Ord(eTipo);
     sExt := ZetaCommonTools.ExtractJustExt(sArchivo);
     Result := ReportSaveDialogFilter( sArchivo,
                                       sExt,
                                       K_FILTER,
                                       iFilter );
end;

function DialogoListadoB( var sArchivo : string; const eTipo: eTipoFormato ): Boolean;
  Const K_FILTER =
                   'HTML (P�ginas Internet)|*.HTML|'                      +
                   'QRP  (Tipo Preliminar)|*.QRP|'                        +
                   'XLSX (Excel con Presentaci�n)|*.XLSX|'                +
                   'RTF  (Texto Enriquecido)|*.RTF|'                      +
                   'WMF  (Archivos MetaData)|*.WMF|'                      +
                   'Mail Merge - Microsoft Word|*.TXT|'                   +
                   'PDF  (Documento de Acrobat)|*.PDF|'                   +
                   'BMP  (Imagen - Bitmap)|*.BMP|'                        +
                   'TIF  (Imagen - Tagged Image File)|*.TIF|'             +
                   'PNG  (Imagen - Portable Network Graphics)|*.PNG|'     +
                   'JPEG (Imagen - JPEG File Interchange Format)| *.JPG|' +
                   'EMF  (Imagen - Enhanced Metafiles)|*.EMF|'            +
                   'SVG  (Scalable Vector Graphics)|*.SVG|'               +
                   'HTM  (Extended HyperText Markup Language)|*.HTM|'     +
                   'WB1  (Quattro Pro para Windows)|*.WB1|'               +
                   'WK2  (Lotus 1-2-3)|*.WK2|'                            +
                   'DIF  (Excel - Data Interchange Format)|*.DIF|'        +
                   'SLK  (Excel - Symbolic Link)|*.SLK|'                  +
                   'XLS  (Excel - Hoja de C�lculo)|*.XLS|';
var
   sExt: string;
   iFilter : integer;
begin
     case eTipo of
          tfHtml : iFilter := 1;
          tfQRP : iFilter := 2;
          else
              iFilter := Ord(eTipo) - 4; {4 es el offSet}
     end;

     sExt := ZetaCommonTools.ExtractJustExt(sArchivo);
     Result := ReportSaveDialogFilterB( sArchivo,
                                        sExt,
                                        K_FILTER,
                                        iFilter );

end;

function DialogoListado( sArchivo : string; const eTipo: eTipoFormato ): string;
begin
     DialogoListadoB( sArchivo, eTipo );
     Result := sArchivo;
end;


function DialogoExporta( var sArchivo : string; const eTipo: eTipoFormato ): Boolean;
begin
     if eTipo in [tfASCIIFijo, tfASCIIDel,tfCSV, tfTXT] then
            Result := DialogoAsciiB( sArchivo )
     else
         Result := DialogoListadoB( sArchivo, eTipo );
end;



end.


