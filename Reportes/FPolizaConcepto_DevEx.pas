unit FPolizaConcepto_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, ZetaEdit,
  ZetaFecha, CheckLst, StdCtrls, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls, Buttons, ZetaNumero,
  FPolizaBase_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  dxSkinsdxBarPainter, dxBar, cxClasses, ImgList, ZetaKeyLookup_DevEx,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls,
  cxContainer, cxEdit, ZetaSmartLists_DevEx, cxTextEdit, cxMemo, cxListBox,
  cxPC, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, cxCheckListBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, ZetaCXGrid,
  cxEditRepositoryItems, dxBarBuiltInMenu;

type
  TPolizaConcepto_DevEx = class(TPolizaBase_DevEx)
    Label8: TLabel;
    RE_HOJA: TZetaDBNumero;
    BuscarBtn: TcxButton;
    btnLlenaConceptos: TcxButton;
    bFormula: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure GridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GridColExit(Sender: TObject);
    procedure bFormulaClick(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure btnLlenaConceptosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxEditRepository1ButtonItem1PropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
   
  private
    procedure BuscaConcepto;
    procedure Buscar;
    procedure DialogoFormulas;

    { Private declarations }
  protected
    procedure Connect;override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override; { TWinControl }
    procedure Borrar;override;

  public
    { Public declarations }
  end;

var
  PolizaConcepto_DevEx: TPolizaConcepto_DevEx;

implementation
uses
    FListaConceptos_DevEx,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZConstruyeFormula,
    ZetaDialogo,
    ZReportTools,
    {$ifdef TRESS}
    DCatalogos,
    {$endif}

    DReportes;

{$R *.dfm}

procedure TPolizaConcepto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     tsOrden.Visible := FALSE;
     tsOrden.TabVisible := FALSE;
end;

procedure TPolizaConcepto_DevEx.Connect;
var
  oCampoRep: OLEVariant;
begin
     inherited;
     CampoRep.IndexFieldNames := 'CR_CALC';
    {$ifdef TRESS}
     dmCatalogos.cdsConceptosLookUp.Conectar;
     {***(@am): La siguiente logica se agrega para que el cds del detalle vuelva a pasar por su metodo OnCreateFields.
                El motivo es que ahi es donde realiza el CreatSimpleLookUp hacia el cdsConceptos. Al crear el reporte,
                si pasa por este evento, pero no la validacion de reporte tipo poliza porque en ese momento el usuario
                aun no elige el reporte que va a realizar. ***}
     with dmReportes, dmCatalogos do
     begin
         if ((cdsEditReporte.State in [dsInsert]) AND (cdsConceptosLookup.Active) AND (cdsCampoRep.FindField('CO_DESCRIP') = nil )) then
         begin
              oCampoRep := cdsCampoRep.Data;
              cdsCampoRep.Close;
              cdsCampoRep.Data := oCampoRep;
         end;
     end;

    {$endif}
end;


procedure TPolizaConcepto_DevEx.GridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'CR_FORMULA' ) then
          begin
               with bFormula do
               begin
                    //ItemIndex := CampoRep.FieldByName('CR_OPER').AsInteger;
                    Left := Rect.Right - Width + 2;
                    Top := Rect.Top + Grid.Top ;
                    Visible := True;
               end;
          end;
     end;
end;

procedure TPolizaConcepto_DevEx.GridColExit(Sender: TObject);
begin
     inherited;
     with Grid do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'CR_FORMULA' ) and
             ( BFormula.Visible ) then
          begin
               BFormula.Visible:= False;
          end;
     end;
end;

procedure TPolizaConcepto_DevEx.bFormulaClick(Sender: TObject);
begin
     inherited;
     DialogoFormulas;
end;

procedure TPolizaConcepto_DevEx.DialogoFormulas;
 var sFormula : string;
begin
     sFormula := CampoRep.FieldByName('CR_FORMULA').AsString;
     if ZConstruyeFormula.GetFormulaConstruye( Entidad, sFormula, 0, evReporte ) then
     begin
          if (CampoRep.State = dsBrowse ) then
          begin
               CampoRep.Edit;
               Modo := dsEdit;
          end;
          CampoRep.FieldByName('CR_FORMULA').AsString := sFormula;
          Grid.SetFocus;
     end;
end;

procedure TPolizaConcepto_DevEx.Buscar;
begin

     if ( ActiveControl = Grid ) then
     begin
          with Grid do
          begin
               if ( SelectedField.FieldName = 'CR_CALC' ) then
                  BuscaConcepto;
          end;
     end;
end;

procedure TPolizaConcepto_DevEx.BuscaConcepto;
begin
     dmReportes.BuscaConcepto;
end;

procedure TPolizaConcepto_DevEx.BuscarBtnClick(Sender: TObject);
begin
     inherited;
     Buscar;
end;


procedure TPolizaConcepto_DevEx.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( ActiveControl = Grid ) then
     begin
          if ( Key <> 0 ) and
             ( ssCtrl in Shift ) and { CTRL }
             ( Key = 66 )  then      { Letra B = Buscar }
          begin
               Key := 0;
               Buscar;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;


procedure TPolizaConcepto_DevEx.btnLlenaConceptosClick(Sender: TObject);
var
   oCursor: TCursor;
   iRecordCount: integer;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ListaConceptos_DevEx = NIL then
           ListaConceptos_DevEx := TListaConceptos_DevEx.Create( self );

        with ListaConceptos_DevEx do
        begin
             ShowModal;
             if ModalResult = mrOK then
             begin
                  iRecordCount := CampoRep.RecordCount;
                  dmReportes.LlenaConceptos( Conceptos );

                  if ( CampoRep.RecordCount <> iRecordCount ) then
                     Modo := dsEdit;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TPolizaConcepto_DevEx.Borrar;
begin
     if CampoRep.IsEmpty then
        ZetaDialogo.ZInformation( Caption, 'No hay Cuentas para Borrar', 0 )
     else if ZetaDialogo.ZConfirm( Caption, '�Desea Borrar la Cuenta del Concepto '+ Camporep.FieldByname('CR_CALC').AsString+'?',0, mbYes ) then
     begin
          CampoRep.Delete;
          Modo := dsEdit;
     end;
end;


procedure TPolizaConcepto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     bFormula.Visible := FALSE;
end;



procedure TPolizaConcepto_DevEx.cxEditRepository1ButtonItem1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;
DialogoFormulas;
end;

end.
