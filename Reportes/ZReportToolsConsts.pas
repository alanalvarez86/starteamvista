{* Constantes utilizadas para los reportes}
unit ZReportToolsConsts;

interface

const
      SINTITULO  = '< TITULO VACIO >';
      K_TITULO = 'T�tulo';

      K_NINGUNO = '< Ninguno >';

      K_RENGLON = '< RENGLON NUEVO >';
      K_RENGLON_TITULO = '==============================';
      K_RENGLON_NUEVO = -2;

      K_PARAMETRO = 'Par�metro';
      K_PROPIEDAD_PARAM = 'Propiedades del Par�metro';

      {$ifdef RDD}
        {$ifdef CONCILIA}
        K_MASCARA_INT = '#0;-#0';
        K_MASCARA_DATE = 'dd/mmm/yy';
        {$endif}
        {$ifdef RDDAPP} //(@gbeltran): Se agrego para usar la constante en reporteadorcfg
        K_MASCARA_INT = '#0;-#0';
        K_MASCARA_DATE = 'dd/mmm/yy';
        {$endif}
      {$else}
      K_MASCARA_INT = '#0;-#0';
      K_MASCARA_DATE = 'dd/mmm/yy';
      {$endif}

      K_SELECT = '@';
      K_SELECT_TEXTO = '@TEXTO:';

      K_TEMPLATE_EXT = '.QR2';
      K_TEMPLATE_LAND = 'DEFAULT LANDSCAPE';
      K_TEMPLATE = 'DEFAULT';
      K_TEMPLATE_HTM = 'DEFAULT.HTM';
      K_IMP_DEFAULT  = 'IMPRESORA DEFAULT:';
      K_DOC_DEFAULT = 'CONTRATO.DOC';
      K_BLOBFIELD = 'IM_BLOB';

      K_TOKEN = 'TOKEN';
      K_EMPRESA = 'Grupo Nivel Empresa';
      K_COLUMNA = 'COLUMNA';

      PRETTY = 'PRETTY_NAM';

      K_NO_REQUIERE = 'NO REQUIERE';
      K_GENERAL = '[GENERALES]';
      K_REPORTE = '[REPORTE]';

      K_FILTROSUPERVISOR = ' ( ( SELECT RESULTADO FROM SP_LISTA_DENTRO( %s, %d, %s ) ) > 0 )';
      K_PAGINA = '#PAG#';
      K_CICLO = '&#';
      K_CICLOLETRA = '&%';

      K_XLS_DEFAULT = '';

      K_EMPLEADO = '@EMPLEADO';
      K_IMSS_PATRON = '@PATRON';
      K_IMSS_YEAR = '@IMSS_YEAR';
      K_IMSS_MES = '@IMSS_MES';
      K_IMSS_TIPO = '@IMSS_TIPO';
      K_YEAR = '@YEAR';
      K_TIPO = '@TIPO';
      K_NUMERO = '@NUMERO';
{$ifdef CAJAAHORRO}
      K_CAJA_AHORRO = '@CAJA_AHORRO';
{$endif}

implementation

end.


