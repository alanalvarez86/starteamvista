object ExcelAppender: TExcelAppender
  OldCreateOrder = False
  Left = 475
  Top = 319
  Height = 246
  Width = 398
  object ExcelApp: TExcelApplication
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    AutoQuit = False
    Left = 336
    Top = 16
  end
  object ExcelSheet: TExcelWorksheet
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 264
    Top = 16
  end
  object ExcelWorkbook1: TExcelWorkbook
    AutoConnect = False
    ConnectKind = ckRunningOrNew
    Left = 184
    Top = 16
  end
  object TimerExcel: TTimer
    Enabled = False
    Interval = 30000
    OnTimer = TimerExcelTimer
    Left = 104
    Top = 24
  end
end
