unit FSentinelVer;
{$ifdef MSSQL}
{$define DEBUGSENTINEL}
{$define SENTINELVIRTUAL}
{$endif}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, CheckLst, ComCtrls,
     FAutoServer,
     FAutoServerLoader,
     ZBaseDlgModal,
     ZetaDBTextBox;

type
  TSentinelView = class(TZetaDlgModal)
    PageControl: TPageControl;
    Datos: TTabSheet;
    Modules: TTabSheet;
    SerialNumberLBL: TLabel;
    SerialNumber: TZetaTextBox;
    EmpresaLBL: TLabel;
    Empresa: TZetaTextBox;
    EmpleadosLBL: TLabel;
    Empleados: TZetaTextBox;
    VersionLBL: TLabel;
    Version: TZetaTextBox;
    Vencimiento: TZetaTextBox;
    VencimientoLBL: TLabel;
    UsuariosLBL: TLabel;
    Usuarios: TZetaTextBox;
    EsKit: TZetaTextBox;
    EsKitLBL: TLabel;
    InstalacionesLBL: TLabel;
    Instalaciones: TZetaTextBox;
    Plataforma: TZetaTextBox;
    PlataformaLBL: TLabel;
    SQLEngineLBL: TLabel;
    SQLEngine: TZetaTextBox;
    EsDemo: TZetaTextBox;
    EsDemoLBL: TLabel;
    RefrescarSentinel: TSpeedButton;
    ModulosGB: TGroupBox;
    Modulos: TCheckListBox;
    Caducidad: TGroupBox;
    Prestamos: TCheckListBox;
    StatusBar: TStatusBar;
    IntentosLBL: TLabel;
    Intentos: TZetaTextBox;
    procedure FormShow(Sender: TObject);
    procedure RefrescarSentinelClick(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    function BoolToFullStr(const lValue: Boolean): String;
    procedure CargaSentinelInfo;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  end;

var
  SentinelView: TSentinelView;

implementation
uses
     FHelpContext,
     {$ifdef SENTINELVIRTUAL}
     FSentinelRegistry,
     {$endif}
     ZetaCommonClasses;

{$R *.DFM}

procedure TSentinelView.FormShow(Sender: TObject);
begin
     inherited;
     HelpContext := H00013_Datos_del_sentinel;
     CargaSentinelInfo;
     PageControl.ActivePage := Datos;
     IntentosLBL.Visible := {$ifdef VALIDA_EMPLEADOS}True{$else}False{$endif};
     Intentos.Visible := {$ifdef VALIDA_EMPLEADOS}True{$else}False{$endif};
end;

procedure TSentinelView.RefrescarSentinelClick(Sender: TObject);
begin
     inherited;
     CargaSentinelInfo;
end;

function TSentinelView.BoolToFullStr( const lValue: Boolean ): String;
begin
     if lValue then
        Result := 'Si'
     else
         Result := 'No';
end;

procedure TSentinelView.CargaSentinelInfo;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;


     try

        {$ifdef SENTINELVIRTUAL}
        FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
        {$endif}

        TAutoServerLoader.LoadSentinelInfo( FAutoServer);
        with FAutoServer do
        begin
             {$ifdef ANTES}Load;{$endif}
             with Self.StatusBar do
             begin
                  if EsDemo then
                     Font.Color := clRed
                  else
                      Font.Color := clBlack;
                  SimpleText := StatusMsg;
             end;
             Self.SerialNumber.Caption := IntToStr( NumeroSerie );
             Self.Empresa.Caption := IntToStr( Empresa );
             Self.Empleados.Caption := GetEmpleadosStr;
             Self.Version.Caption := Version;
             Self.Vencimiento.Caption := GetVencimientoStr;
             Self.Caducidad.Caption := ' Préstamos: ' + GetCaducidadStr + ' ';
             Self.Usuarios.Caption := IntToStr( Usuarios );
             Self.EsKit.Caption := BoolToFullStr( EsKit );
             Self.EsDemo.Caption := BoolToFullStr( EsDemo );
             Self.Instalaciones.Caption := IntToStr( Instalaciones );
             {$ifdef ANTES}
             Self.Intentos.Caption := IntToStr( Intentos );
             {$endif}
             Self.Plataforma.Caption := PlataformaAutoStr;
             Self.SQLEngine.Caption := GetSQLEngineStr;
             GetModulos( Self.Modulos );
             GetPrestamos( Self.Prestamos );
        end;
     finally
            Screen.Cursor := oCursor;
            {$ifdef SENTINELVIRTUAL}
            FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
            {$endif}
     end;
end;

end.
