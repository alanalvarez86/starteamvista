object DesbloqueaAdmon: TDesbloqueaAdmon
  Left = 285
  Top = 189
  AutoScroll = False
  Caption = 'Desbloquear Usuarios Administradores'
  ClientHeight = 261
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Wizard: TZetaWizard
    Left = 0
    Top = 227
    Width = 436
    Height = 34
    Align = alBottom
    TabOrder = 0
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    PageControl = PageControl
    Reejecutar = True
    object Desbloquear: TZetaWizardButton
      Left = 240
      Top = 5
      Width = 92
      Height = 25
      Hint = 'Desbloquear Los Usuarios'
      Caption = '&Desbloquear'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      Tipo = bwEjecutar
      Wizard = Wizard
    end
    object Cancelar: TZetaWizardButton
      Left = 336
      Top = 5
      Width = 94
      Height = 25
      Hint = 'Cancelar y Salir'
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = CancelarClick
      Kind = bkCancel
      Tipo = bwCancelar
      Wizard = Wizard
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 436
    Height = 227
    ActivePage = Comparacion
    Align = alClient
    TabOrder = 1
    object Comparacion: TTabSheet
      Caption = 'Comparacion'
      TabVisible = False
      object Mensaje: TMemo
        Left = 0
        Top = 0
        Width = 428
        Height = 217
        Align = alClient
        Alignment = taCenter
        BorderStyle = bsNone
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          '')
        ParentFont = False
        TabOrder = 0
      end
    end
  end
end
