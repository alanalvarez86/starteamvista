unit FPatchComparte;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, DbTables, Mask,
     FMigrar,
     ZetaWizard,
     ZetaNumero, jpeg;

type
  TPatchComparte = class(TMigrar)
    ArchivoGB: TGroupBox;
    ConfigFileLBL: TLabel;
    ConfigFileSeek: TSpeedButton;
    ConfigFile: TEdit;
    OpenDialog: TOpenDialog;
    Parametros: TTabSheet;
    ParametrosGB: TGroupBox;
    VersionLBL: TLabel;
    Version: TComboBox;
    PatchTriggers: TCheckBox;
    PatchSP: TCheckBox;
    PatchDB: TCheckBox;
    RangoScriptsGB: TGroupBox;
    ScriptInicialLBL: TLabel;
    ScriptFinalLBL: TLabel;
    ScriptInicial: TZetaNumero;
    ScriptFinal: TZetaNumero;
    ScriptsTodos: TRadioButton;
    ScriptsRango: TRadioButton;
    Label1: TLabel;
    VerActual: TEdit;
    UpDown: TUpDown;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ConfigFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure PatchDBClick(Sender: TObject);
  private
    { Private declarations }
    FOK: Boolean;
    function GetConfigFile: String;
    function GetVersion: String;
    procedure SetConfigFile( const sValue: String );
  protected
    { Protected declarations }
    procedure SetControls; override;
  public
    { Public declarations }
    property OK: Boolean read FOK;
    function Init: Boolean;
  end;

var
  PatchComparte: TPatchComparte;

implementation

uses ZetaMigrar,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaTressCFGTools,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TPatchComparte.FormCreate(Sender: TObject);
const
     {$ifdef INTERBASE}
     K_PATCH_FILE = 'SP_Comparte';
     {$endif}
     {$ifdef MSSQL}
     K_PATCH_FILE = 'SP_MSSQL_Comparte';
     {$endif}
begin
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'PatchComparte.log' );
     ConfigFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( Format( 'Fuentes\Patch\%s.db', [ K_PATCH_FILE ] ) );
     Version.ItemIndex := 0;
     dmMigracion := TdmReportes.Create( Self );
     ScriptInicial.Valor := 1;
     ScriptFinal.Valor := 999;
     inherited;
     SetControls;
     HelpContext := H00007_Actualizar_base_de_datos;
     FOk := False;
end;

procedure TPatchComparte.FormDestroy(Sender: TObject);
begin
     TdmReportes( dmMigracion ).Free;
     inherited;
end;

procedure TPatchComparte.FormShow(Sender: TObject);
begin
     inherited;
     SetControls;
end;

function TPatchComparte.Init: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TdmReportes( dmMigracion ) do
        begin
             with Self.UpDown do
             begin
                  Position:= StrToIntDef( GetVerActualComparte, 0 );
                  Enabled:= not Existe_VA;
             end;
             Self.VerActual.Enabled:= not Existe_VA;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Abrir Base De Datos Comparte !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

function TPatchComparte.GetConfigFile: String;
begin
     Result := ConfigFile.Text;
end;

function TPatchComparte.GetVersion: String;
begin
     with Version do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

procedure TPatchComparte.SetConfigFile( const sValue: String );
begin
     ConfigFile.Text := sValue
end;

procedure TPatchComparte.ConfigFileSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          FileName := GetConfigFile;
          InitialDir := ExtractFileDir( FileName );
          if Execute then
             SetConfigFile( FileName );
     end;
end;

procedure TPatchComparte.SetControls;
var
   lEnabled: Boolean;
begin
     lEnabled := PatchDB.Checked;
     RangoScriptsGB.Enabled := lEnabled;
     ScriptsTodos.Enabled := lEnabled;
     ScriptsRango.Enabled := lEnabled;
     lEnabled := lEnabled and ScriptsRango.Checked;
     ScriptInicialLBL.Enabled := lEnabled;
     ScriptInicial.Enabled := lEnabled;
     ScriptFinalLBL.Enabled := lEnabled;
     ScriptFinal.Enabled := lEnabled;
end;

procedure TPatchComparte.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if Wizard.Adelante then
             begin
                  if Wizard.EsPaginaActual( DirectorioDOS ) then
                  begin
                       if not FileExists( GetConfigFile ) then
                       begin
                            zWarning( Caption, 'Archivo de Scripts No Existe', 0, mbOK );
                            ActiveControl := ConfigFile;
                            CanMove := False;
                       end
                       else
                       begin
                            with TdmReportes( dmMigracion ) do
                            begin
                                 SourceSharedPath := ExtractFileDir( GetConfigFile );
                                 try
                                    if AbreArchivoParadox( ExtractFileName( GetConfigFile ) ) then
                                    begin
                                         if GetVersionList( Version.Items, ExtractFileName( GetConfigFile ) ) then
                                         begin
                                              Version.ItemIndex := 0;
                                              CanMove := True;
                                         end
                                         else
                                         begin
                                              zWarning( Caption, 'Archivo de Scripts Es Inv�lido', 0, mbOK );
                                              ActiveControl := ConfigFile;
                                              CanMove := False;
                                         end;
                                    end;
                                 except
                                       on Error: Exception do
                                       begin
                                            CanMove := False;
                                            ShowError( '� Error En Archivo de Scripts !', Error );
                                       end;
                                 end;
                            end;
                       end;
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TPatchComparte.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);

function GetModoPatchEstructuras: TModoPatch;
begin
     with Result do
     begin
          Habilitado := PatchDB.Checked;
          if ScriptsTodos.Checked then
             Filtro := esTodos
          else
              if ScriptsRango.Checked then
                 Filtro := esRango
              else
                  Filtro := esLista;
          Inicial := ScriptInicial.ValorEntero;
          Final := ScriptFinal.ValorEntero;
          Lista := '';
     end;
end;

begin
     FOk := False;
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          InitCounter( GetPatchSteps );
          StartProcess;
          EnableStopOnError;
          lOk := ActualizarComparte( ExtractFileName( GetConfigFile ),
                                     Self.VerActual.Text,
                                     GetVersion,
                                     PatchTriggers.Checked,
                                     PatchSP.Checked,
                                     GetModoPatchEstructuras );
          EndProcess;
     end;
     EndLog;
     inherited;
     FOk := lOk;
end;

procedure TPatchComparte.PatchDBClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

end.
