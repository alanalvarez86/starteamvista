unit FExportaDatosSistLic;

interface

uses
System.SysUtils, System.Variants, System.Classes,Dispatcher3DTProxy, ZetaRegistryServer, AbZipper,
FAutoClasses, FAutoServer, FAutoServerLoader, IdCoderMIME, FSentinelRegistry;

Type
  TExportaDatosSistLic = class(TObject)
    IdEncoderMIME: TIdEncoderMIME;
    AbZipper: TAbZipper;
Private
  FAutoServer: TAutoServer;
  FDatos: TStrings; //AUN NO ESTA CREADO, SE CREA EN EXPORTDATA
  FFileName: String;
  FRespuesta: String;
  function Server: Despachador_x0020_3DTSoap;
  function GetURL: String;
  function GetFileSize( const sFileName : String ): String;
  function FileToBase64( sFilePath : string ) : string;
  procedure WriteLicData( Lineas: TStrings);
  function BoolToAttBoolean( lValue : boolean ) : String;
  function ReportaFecha( const dValue: TDate ): String;
  function ModuloOK(const eModulo: TModulos): String;
  function PrestamoOK(const eModulo: TModulos): String;
Public
  constructor Create;
  destructor Destroy; override;
  property FileName: String read FFileName write FFileName;
  property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  property Respuesta: String read FRespuesta write FRespuesta;
  property Datos: TStrings read FDatos write FDatos;
  procedure ExportRepLic( const sFileName: String);
  procedure EnviarReporteLicencia(FileName:String; sNombreZIP: String);
  procedure EnviarArchivo3dt(FileName:String; sNombreZIP: String);
  function crearNombreArchivoZIP: String;
  function IntentarGuardarArchivo: boolean;
  procedure LoadSentinelData; overload;
end;


implementation

uses
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaServerTools,
    ZetaWinAPITools,
    ZetaLicenseClasses;

const
     K_FECHA = 'yyyy-mm-dd';
     K_LLAVE = '3DTLlavedeAccesoHerramienta';
     K_WS3DTDIR = '3DT/Dispatcher/Dispatcher.asmx';
     {$IFNDEF TEST_SENTINEL}
        K_WS3DTURL = 'http://tress30.tress.com.mx';
     {$ELSE}
        K_WS3DTURL = 'http://10.10.10.26';  // (@am): SERVIDOR DE PRUEBAS
     {$ENDIF}

constructor TExportaDatosSistLic.Create;
begin
     FDatos := TStringList.Create;
end;

destructor TExportaDatosSistLic.Destroy;
begin
     FreeAndNil( FDatos );
     inherited Destroy;
end;

function TExportaDatosSistLic.Server: Despachador_x0020_3DTSoap;
begin
  Result := GetDespachador_x0020_3DTSoap (FALSE, GetURL);
end;

function TExportaDatosSistLic.GetURL: String;
var
   oRegistry : TZetaRegistryServer;
begin
    oRegistry := TZetaRegistryServer.Create();
    try
        if not (StrLleno(oRegistry.WS3DTURL)) then
          oRegistry.WS3DTURL := K_WS3DTURL;
        Result :=  VerificaDir (oRegistry.WS3DTURL, '/') + K_WS3DTDIR;
    finally
      FreeAndNil (oRegistry);
    end;
end;

function TExportaDatosSistLic.GetFileSize( const sFileName : String ): String;
var
   Archivo: TSearchRec;
begin
     if ( StrLleno( SFileName ) ) and ( FindFirst( sFileName, faAnyFile, Archivo) = 0 ) then
     begin
          Result := IntToStr (Archivo.Size);
          FindClose( Archivo );
     end
     else
         Result := '0';
end;

function TExportaDatosSistLic.crearNombreArchivoZIP: String;
begin
    Result := IntToStr (FAutoServer.Empresa) + '_' + IntToStr( FAutoServer.NumeroSerie ) + '_' + FormatDateTime( 'yyyymmddhhnn', now ) + '.zip';
end;

function TExportaDatosSistLic.FileToBase64( sFilePath : string ) : string;
var
  SourceStr: TFileStream;
begin
    SourceStr := TFileStream.Create(sFilePath, fmOpenRead);
    try
       IdEncoderMIME := TIdEncoderMIME.Create(nil);
       try
           Result := IdEncoderMIME.Encode(SourceStr);
       finally
          IdEncoderMIME.Free;
       end;
    finally
      SourceStr.Free;
    end;
end;

function TExportaDatosSistLic.BoolToAttBoolean(lValue: boolean): String;
begin
     if ( lValue ) then
        Result := 'true'
     else
         Result := 'false';
end;

function TExportaDatosSistLic.ReportaFecha( const dValue: TDate ): String;
begin
     if ( dValue > NullDateTime ) then
        Result := FormatDateTime( K_FECHA, dValue )
     else
         Result := '1899-12-30';
end;

function TExportaDatosSistLic.ModuloOK( const eModulo: TModulos ): String;
begin
     Result := BoolToAttBoolean( FAutoClasses.BitSet( FAutoServer.Modulos, Ord( eModulo ) ) );
end;

function TExportaDatosSistLic.PrestamoOK( const eModulo: TModulos ): String;
begin
     Result := BoolToAttBoolean( FAutoClasses.BitSet( FAutoServer.Prestamos, Ord( eModulo ) ) );
end;

procedure TExportaDatosSistLic.LoadSentinelData;
const
     K_MODULO = '%25.25s         %s       %s';
     K_MODULO_XML = '<Modulo Numero="%d" Nombre="%s" Autorizado="%s" Prestado="%s"/>';
var
   eModulo: TModulos;
begin
     with FDatos do
     begin
          {$ifdef SENTINELVIRTUAL}
          FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
          {$endif}

          TAutoServerLoader.LoadSentinelInfo( FAutoServer );

          with FAutoServer do
          begin
               Add( '<Sentinel ' );
               Add( 'Empresa="' + IntToStr( Empresa ) +'" ');
               Add( 'Empleados="' + IntToStr( Empleados ) +'" ');
               Add( 'Licencias="' + IntToStr( Usuarios ) +'" ');
               Add( 'Numero="' + IntToStr( NumeroSerie ) +'" ');
               Add( 'LimiteConfigBD="' + IntToStr( ZetaLicenseClasses.GetIntervaloLimiteBDConfig(LimiteConfigBD) ) +'" ');
               Add( 'IniciaValidacion="' + IntToStr( IniciaValidacion ) +'" ');
               Add( 'Version="' + Version  +'" ' );
               Add( 'EsKitDistribuidor="' + BoolToAttBoolean( EsKit )+'" ' );
               Add( 'Plataforma="' + PlataformaAutoStr +'" ');
               Add( 'BaseDeDatos="' + GetSQLEngineStr +'" ');
               Add( 'Vencimiento="' + ReportaFecha( FAutoServer.Vencimiento ) +'" ');
               Add( 'CaducidadPrestamos="' + ReportaFecha( FAutoServer.Caducidad )  +'" ');
               Add( '>' );
               for eModulo := Low( TModulos ) to High( TModulos ) do
               begin
                    Add( Format( K_MODULO_XML, [ Ord( eModulo ),  FAutoServer.GetModuloStr( eModulo ), ModuloOk( eModulo ), PrestamoOk( eModulo ) ] ) );
               end;
               Add( '</Sentinel>');
          end;
         {$ifdef SENTINELVIRTUAL}
         FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
         {$endif}
     end;
end;

function TExportaDatosSistLic.IntentarGuardarArchivo: boolean;
begin
    try
        FDatos.SaveToFile( FFileName );
        Result := TRUE;
    except
      Result := FALSE;
    end;
end;

//@(am): Genera el reporte de error de los datos leidos de la licencia en forma XML.
procedure TExportaDatosSistLic.WriteLicData( Lineas: TStrings);
begin
  FSentinelRegistry.ObtenDatosServidor;
  with Lineas do
  begin
        Add('<LicData>');
        Add('<ServerInfo>');
        Add('<ServerName>'+ FSentinelRegistry.LicServidor +'</ServerName>');
        Add('<LenStr>'+ IntToStr(Length(FSentinelRegistry.LicServidor)) +'</LenStr>');
        Add('</ServerInfo>');
        Add('<OS>');
        Add('<SerialNumber>'+FSentinelRegistry.LicOSProductId+'</SerialNumber>');
        Add('<LenStr>'+ IntToStr(Length(FSentinelRegistry.LicOSProductId)) +'</LenStr>');
        Add('</OS>');
        Add('<Bios>');
        Add('<SerialNumber>'+FSentinelRegistry.LicBIOSId+'</SerialNumber>');
        Add('<LenStr>'+ IntToStr(Length(FSentinelRegistry.LicBIOSId)) +'</LenStr>');
        Add('</Bios>');
        Add('</LicData>');
  end;
end;

//(@am): Genera el reporte de error en formato XML.
procedure TExportaDatosSistLic.ExportRepLic( const sFileName: String );
var
   i: Integer;
begin
     try
           FDatos.Add('<?xml version="1.0" encoding="ISO-8859-1" ?>');
           FDatos.Add( Format( '<ErrorLicencia Fecha="%s">', [ FormatDateTime( K_FECHA, Now )] ) );
           ZetaWinAPITools.WriteSystemShortInfo(FDatos); //Info directo del servidor.
           WriteLicData(FDatos); //Info que se cargo al registry al leer el archivo de licencia.
           FDatos.Add('</ErrorLicencia>');

           with FDatos do
           begin
          {$ifdef DEBUG_XML_3DT}
                SaveToFile( sFileName  + '.xml' );
          {$endif}
                for i := 0 to ( Count - 1 ) do
                begin
                     Strings[ i ] := ZetaServerTools.Encrypt( Strings[ i ] );
                end;
           end;
     except
           on Error: Exception do
           begin
                //Application.HandleException( Error );
                FRespuesta := 'No se pudo generar el reporte de error.'
           end;
     end;
end;

//@(am): Envia el reporte de error de la licencia
procedure TExportaDatosSistLic.EnviarReporteLicencia(FileName:String; sNombreZIP: String);  //FileName: Aqui solo es el nombre del archivo #NoEmpresa+Sentinel+Fecha
var
   oDespachador_x0020_3DTSoap: Despachador_x0020_3DTSoap;
   sXML3DT: String;
begin
      oDespachador_x0020_3DTSoap := Server;
      try
        FRespuesta := oDespachador_x0020_3DTSoap.Echo;
      except
        on Error: Exception do
        begin
              raise Exception.Create('No es posible establecer conexi�n con el web service');
        end;
      end;
      {$ifdef SENTINELVIRTUAL}
      FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
      {$endif}
      TAutoServerLoader.LoadSentinelInfo( FAutoServer );
      sXML3DT :=
            '<PROCESS>' + CR_LF +
               '<LLAVE>' + Encrypt(K_LLAVE) + '</LLAVE>' + CR_LF +
               '<SISTEMA>' + IntToStr (FAutoServer.Empresa) + '</SISTEMA>' + CR_LF +
               '<SENTINEL>' + IntToStr (FAutoServer.NumeroSerie) + '</SENTINEL>' + CR_LF +
               '<ARCHIVO>' + CR_LF +
                  '<ARCHIVONAME>' + ExtractFileName (FileName) + '</ARCHIVONAME>' + CR_LF +
                  '<ARCHIVOSIZE>' +  GetFileSize(FileName) +'</ARCHIVOSIZE>' + CR_LF +
               '</ARCHIVO>' + CR_LF +
            '</PROCESS>';
      FRespuesta := oDespachador_x0020_3DTSoap.UploadRepLicBase64(FileToBase64 (sNombreZIP), sXML3DT);
      {$ifdef SENTINELVIRTUAL}
      FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
      {$endif}
end;

//@am): Envia el archivo 3dt
procedure TExportaDatosSistLic.EnviarArchivo3DT(FileName:String; sNombreZIP: String);  //FileName: Aqui solo es el nombre del archivo #NoEmpresa+Sentinel+Fecha
const
    K_NO_CONEX = '-1';
var
   oDespachador_x0020_3DTSoap: Despachador_x0020_3DTSoap;
   sXML3DT: String;
begin
      oDespachador_x0020_3DTSoap := Server;
      try
        FRespuesta := oDespachador_x0020_3DTSoap.Echo;
      except
        on Error: Exception do
        begin
              //raise Exception.Create('');
              FRespuesta := '-1';
        end;
      end;
      if FRespuesta <> K_NO_CONEX then
      begin
        {$ifdef SENTINELVIRTUAL}
        FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
        {$endif}
        TAutoServerLoader.LoadSentinelInfo( FAutoServer );
        sXML3DT :=
              '<PROCESS>' + CR_LF +
                 '<LLAVE>' + Encrypt(K_LLAVE) + '</LLAVE>' + CR_LF +
                 '<SISTEMA>' + IntToStr (FAutoServer.Empresa) + '</SISTEMA>' + CR_LF +
                 '<SENTINEL>' + IntToStr (FAutoServer.NumeroSerie) + '</SENTINEL>' + CR_LF +  //REVISAR QUE SI SE ESTE GUARDANDO BIENE N EL WS
                 '<ARCHIVO>' + CR_LF +
                    '<ARCHIVONAME>' + ExtractFileName (FileName) + '</ARCHIVONAME>' + CR_LF +
                    '<ARCHIVOSIZE>' +  GetFileSize(FileName) +'</ARCHIVOSIZE>' + CR_LF +
                 '</ARCHIVO>' + CR_LF +
              '</PROCESS>';
        FRespuesta := oDespachador_x0020_3DTSoap.UploadBase64(FileToBase64 (sNombreZIP), sXML3DT);
        {$ifdef SENTINELVIRTUAL}
        FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
        {$endif}
      end;
end;

end.
