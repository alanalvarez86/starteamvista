unit FHelpContext;
{ Turbo Pascal Unit:  tresscfg.pas                }
{                                                 }
{ This is an interface unit containing integer    }
{ mappings of Topic IDs (names of Help            }
{ Topics) which are located in tress.rtf          }
{                                                 }
{ This file is re-written by RoboHELP             }
{ whenever tress.rtf is saved.   	          }
{                                                 }
{ However, the numeric values stored in           }
{ tress.hh are the 'master values' and if you     }
{ modify the value in tress.hh and then           }
{ save the tress.rtf again, this file will        }
{ reflect the changed values.                     }
{                                                 }
interface

const

     H00001_Usando_el_programa_TressCFG = 1;
     H00002_Revisar_DOS = 2;
     H00003_Migrar_empresa = 3;
     H00004_Importar_y_Exportar_reportes = 4;
     H00006_Consolidar_empresas = 6;
     H00007_Actualizar_base_de_datos = 7;
     H00008_Migrar_comparte = 8;
     H00009_Importar_conceptos = 9;
     H00011_Indicando_la_base_de_datos = 11;
     H00012_Probando_el_acceso = 12;
     H00013_Datos_del_sentinel = 13;
     H00014_Actualizar_sentinel = 14;
     //H00015_Datos_del_sistema = 15; <-- ahorita esta en ZetaCommonClases
     H00016_Servidor_de_procesos = 16;
     H00017_Migrando_Datos_de_IB_a_MS_SQL = 17;
     H00020_Agregar_y_borrar_empresas = 20;
     H00021_Exportando_tablas = 21;
     H00022_Importando_tablas = 22;
     H00023_Poblando_una_base_de_datos = 23;
     H00024_Acerca_de_TressCFG = 24;
     H00025_Comparar_catalogos_y_tablas = 25;
     H00026_Transferir_empleados = 26;
     H00027_Stored_Procedures_Adicionales = 27;
     H52000_Copiado_Base_Datos_para_Presupuestos = 52000;
     H50012_Probando_el_acceso = 50012;
     H50013_Configurando_la_estructura_de_Comparte = 50013;
     H50014_Configurando_la_estructura_de_una_base_de_datos = 50014;
     H50015_Inicializando_los_datos_de_una_empresa = 50015;

implementation

end.
