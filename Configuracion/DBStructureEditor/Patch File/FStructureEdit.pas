unit FStructureEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls,
     Mask, DBCtrls, Grids, DBGrids, FileCtrl, Db,
     DStructure,
     ZetaKeyCombo,
     ZetaNumero;

type
  TDBStructEdit = class(TForm)
    PageControl: TPageControl;
    PanelControles: TPanel;
    Interbase: TTabSheet;
    Oracle: TTabSheet;
    SP_NAMElbl: TLabel;
    SP_SEQ_NUMlbl: TLabel;
    PanelBotones: TPanel;
    Cancelar: TBitBtn;
    SP_INTRBASE: TDBMemo;
    SP_ORACLE: TDBMemo;
    SP_NAME: TDBEdit;
    OK: TBitBtn;
    PanelIB: TPanel;
    PanelORA: TPanel;
    IBLoadFromFile: TSpeedButton;
    MSSQLLoadFromFile: TSpeedButton;
    OpenDialog: TOpenDialog;
    dsSP: TDataSource;
    SP_VERSIONlbl: TLabel;
    SP_VERSION: TDBComboBox;
    SP_TIPO: TZetaDBKeyCombo;
    SP_TIPOlbl: TLabel;
    SP_SEQ_NUM: TZetaDBNumero;
    IBBorrarTodo: TSpeedButton;
    MSSQLBorrarTodo: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IBLoadFromFileClick(Sender: TObject);
    procedure MSSQLLoadFromFileClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure IBBorrarTodoClick(Sender: TObject);
    procedure MSSQLBorrarTodoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CargaMemo( Memo: TDBMemo );
  public
    { Public declarations }
    procedure Editar( const lInsert: Boolean );
  end;

var
  DBStructEdit: TDBStructEdit;

implementation

uses FStructureSeek;

{$R *.DFM}

procedure TDBStructEdit.FormCreate(Sender: TObject);
begin
     with SP_TIPO.Lista do
     begin
          Add( Format( '%d=Trigger', [ K_TRIGGER ] ) );
          Add( Format( '%d=Stored Procedure', [ K_STORED_PROCEDURE ] ) );
          Add( Format( '%d=SQL', [ K_SQL ] ) );
     end;
     with SP_VERSION do
     begin
          DStructure.FillVersionList( Items );
     end;
end;

procedure TDBStructEdit.FormShow(Sender: TObject);
begin
     with PageControl do
     begin
          with dmDBStruct do
          begin
               case DbType of
                    eIBComparte: ActivePage := Interbase;
                    eIBDatos: ActivePage := Interbase;
                    eIBSeleccion: ActivePage := Interbase;
                    eIBPortal: ActivePage := Interbase;
                    eMSSQLComparte: ActivePage := Oracle;
                    eMSSQLDatos: ActivePage := Oracle;
                    eMSSQLSeleccion: ActivePage := Oracle;
                    eMSSQLPortal: ActivePage := Oracle;
               end;
               {
               Interbase.TabVisible := ( PageControl.ActivePage = Interbase );
               Oracle.TabVisible := ( PageControl.ActivePage = Oracle );
               }
          end;
     end;
end;

procedure TDBStructEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     with dmDBStruct do
     begin
          try
             CancelaCambios;
             ModalResult := mrCancel;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

procedure TDBStructEdit.CargaMemo( Memo: TDBMemo );
begin
     with OpenDialog do
     begin
          if Execute then
             Memo.Lines.LoadFromFile( FileName );
     end;
end;

procedure TDBStructEdit.IBLoadFromFileClick(Sender: TObject);
begin
     CargaMemo( SP_INTRBASE );
end;

procedure TDBStructEdit.MSSQLLoadFromFileClick(Sender: TObject);
begin
     CargaMemo( SP_ORACLE );
end;

procedure TDBStructEdit.Editar( const lInsert: Boolean );
begin
     with dmDBStruct do
     begin
          Editar( lInsert );
     end;
end;

procedure TDBStructEdit.OKClick(Sender: TObject);
begin
     with dmDBStruct do
     begin
          try
             if EscribeCambios then
                ModalResult := mrOk;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

procedure TDBStructEdit.IBBorrarTodoClick(Sender: TObject);
begin
     SP_INTRBASE.Clear;
end;

procedure TDBStructEdit.MSSQLBorrarTodoClick(Sender: TObject);
begin
     SP_ORACLE.Clear;
end;

end.
