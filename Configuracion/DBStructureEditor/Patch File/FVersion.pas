unit FVersion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables;

type
  TVersionForm = class(TForm)
    OK: TBitBtn;
    Cancelar: TBitBtn;
    OriginalLBL: TLabel;
    Original: TComboBox;
    NuevaLBL: TLabel;
    Nueva: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function VersionOriginal: String;
    function VersionNueva: String;
  end;

var
  VersionForm: TVersionForm;

function ChooseVersion( var sOriginal, sNueva: String ): Boolean;

implementation

uses DStructure;

{$R *.DFM}

function ChooseVersion( var sOriginal, sNueva: String ): Boolean;
begin
     if not Assigned( VersionForm ) then
        VersionForm := TVersionForm.Create( Application );
     with VersionForm do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               sOriginal := VersionOriginal;
               sNueva := VersionNueva;
               Result := True;
          end
          else
              Result := False;
     end;
end;

procedure TVersionForm.FormCreate(Sender: TObject);
begin
     with Original do
     begin
          DStructure.FillVersionList( Items );
          ItemIndex := 1;
     end;
     with Nueva do
     begin
          DStructure.FillVersionList( Items );
          ItemIndex := 0;
     end;
end;

procedure TVersionForm.FormShow(Sender: TObject);
begin
     ActiveControl := Original;
end;

function TVersionForm.VersionOriginal: String;
begin
     with Original do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

function TVersionForm.VersionNueva: String;
begin
     with Nueva do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

procedure TVersionForm.OKClick(Sender: TObject);
begin
     if ( VersionOriginal <> VersionNueva ) then
        ModalResult := mrOk
     else
     begin
          Beep;
          ActiveControl := Original;
     end;
end;

end.
