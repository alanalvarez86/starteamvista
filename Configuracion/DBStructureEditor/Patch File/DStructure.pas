unit DStructure;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, DBTables, Db,{$ifndef VER130}Variants,{$endif}
     ZetaCommonLists;

const
     K_TRIGGER = 0;
     K_STORED_PROCEDURE = 1;
     K_SQL = 2;
type
  eDbType = ( eIBComparte, eIBDatos, eIBSeleccion, eIBVisitantes, eIBPortal, eMSSQLComparte, eMSSQLDatos, eMSSQLSeleccion, eMSSQLVisitantes, eMSSQLPortal );
  eDBServer = ( dbInterbase, dbSQLServer );
  TScriptKeys = record
   Tipo: eTipoCompany;
   Secuencia: Integer;
  end;
  TdmDBStruct = class(TDataModule)
    dbStruct: TDatabase;
    tqSP: TQuery;
    tqSPSeek: TQuery;
    uqSP: TUpdateSQL;
    tqEdSp: TQuery;
    taExport: TTable;
    BatchMove: TBatchMove;
    tqExport: TQuery;
    dbTress: TDatabase;
    tqImport: TQuery;
    spImport: TStoredProc;
    tqImpDet: TQuery;
    tqIBSpDep: TQuery;
    tqScripts: TQuery;
    tqScript: TQuery;
    upScript: TUpdateSQL;
    ttScripts: TTable;
    tqScriptImport: TQuery;
    procedure tqEdSpBeforePost(DataSet: TDataSet);
    procedure tqSPAfterOpen(DataSet: TDataSet);
    procedure dmDBStructCreate(Sender: TObject);
    procedure dmDBStructDestroy(Sender: TObject);
    procedure tqEdSpNewRecord(DataSet: TDataSet);
    procedure tqScriptsAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FLista: TStrings;
    FLlave: String;
    FPatchVersion: String;
    FVersionValue: String;
    FNewVersion: String;
    FNewTipo: Integer;
    FNewSeqNum: Integer;
    FContador: Integer;
    FSQLType: Integer;
    FDBType: eDbType;
    function Abrir: Boolean;
    function ComparaStrings( const sOriginal, sTemporal: string ): boolean;
    function FormatMetaData(const sOriginal: String): String;
    function GetDBServer: eDbServer;
    function GetBlobFieldName: String;
    function GetTableName: String;
    procedure Borrar( const DataSets: array of TDBDataSet );
    procedure CancelaCambio(BDEDataset: TBDEDataset);
    procedure ConectarBD( Database: TDatabase; const sAlias, sUsuario, sPassword: String );
    procedure DespreparaQuery( Query: TQuery );
    procedure DeterminaDependencias( const sValor: String );
    procedure Grabar( const DataSets: array of TDBDataSet );
    procedure GuardaLlave;
    procedure ImportMSSQLMetadata(const lTriggers: Boolean; const sAlias, sUsuario, sPassword, sVersion: String);
    procedure ListStoredProcedures;
    procedure MuestraTipo( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure Posicionar( const sVersion, sLlave: String; const iTipo: Integer );
    procedure PosicionarEdicion;
    procedure PreparaQuery( Query: TQuery; const sSQL: String );
    procedure Refrescar;
    procedure ResetImportaCodigo;
    procedure ResetStoredProcedure( StoredProc: TStoredProc );
    procedure SetImportaCodigo( const iTipo, iDBType: Integer; const sVersion: String );
    procedure SetStoredProcedure( StoredProc: TStoredProc; const sStoredProcName: String );
    procedure SetUpQueries;
    procedure BorraLog( const sFile: string );
    procedure SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  public
    { Public declarations }
    property DBType: eDbType read FDBType write FDBType;
    property DBServer: eDbServer read GetDBServer;
    property PatchVersion: String read FPatchVersion write FPatchVersion;
    function Conectada: Boolean;
    function Version: String;
    function Llave: String;
    function HayDatos: Boolean;
    function Conectar( const sDatabase, sUsuario, sPassword, sVersion: String ): Boolean;
    function EscribeCambios: Boolean;
    function CompararMSSQL( const sAlias, sUsuario, sPassword, sVersion: String; var sLog: String): Boolean;
    function CompararTriggersIB( const sAlias, sUsuario, sPassword, sVersion: String; var sLog: String): Boolean;
    function CompararSPIB( const sAlias, sUsuario, sPassword, sVersion: String; var sLog: String): Boolean;
    function DBStructConectar(const sDatabase, sUsuario, sPassword: String): Boolean;
    function DBStructDesconectar: Boolean;
    function MSSQLScriptsRefrescar: Boolean;
    function MSSQLScriptBorrar: Boolean;
    function MSSQLScriptEscribirCambios: Boolean;
    function MSSQLScriptExportarParadox(const sArchivo: String): Boolean;
    function MSSQLScriptGetKeys: TScriptKeys;
    function MSSQLScriptPosicionar: Boolean;
    function MSSQLScriptsInit( const sDatabase, sUsuario, sPassword: String): Boolean;
    function ExportarParadox( const sVersion, sArchivo: String ): Boolean;
    function GetExportFile: String;
    function SQLType: Integer;
    procedure ConectarBusqueda;
    procedure PosicionarBusqueda( const sLlave: String );
    procedure PosicionarResultado;
    procedure BorrarRegistro;
    procedure Editar( const lInsert: Boolean );
    procedure CancelaCambios;
    procedure ImportarParadox( const sFileName: String );
    procedure InitBlobFields;
    procedure InitVersion( const sOriginal, sNueva: String );
    procedure ImportInterbaseTriggers( const sAlias, sUsuario, sPassword, sVersion: String );
    procedure ImportInterbaseProcedures( const sAlias, sUsuario, sPassword, sVersion: String );
    procedure ImportMSSQLProcedures(const sAlias, sUsuario, sPassword, sVersion: String);
    procedure ImportMSSQLTriggers(const sAlias, sUsuario, sPassword, sVersion: String);
    procedure ImportAllScripts;
    procedure MSSQLScriptCancelarCambios;
    procedure MSSQLScriptEditar(const lInsert: Boolean);
  end;

var
  dmDBStruct: TdmDBStruct;

procedure FillVersionList( Lista: TStrings );
function GetDBType( const eValor: eDbType ): String;

implementation

uses BDEFunctions,
     ZetaCommonTools,
     ZetaCommonClasses, FStructure;

const
     LF = Chr( 10 );
     K_INTERBASE = 1;
     K_SQL_SERVER = 2;
     Q_LEER = 'select SP_NAME, SP_INTRBASE, SP_ORACLE, SP_VERSION, SP_SEQ_NUM, SP_TIPO, SP_DUMMY ' +
              'from %s where ( SP_VERSION = ''%s'' ) order by SP_VERSION desc, SP_TIPO, SP_SEQ_NUM';
     Q_BUSCAR = 'select SP_VERSION, SP_NAME, SP_TIPO from %s where ( SP_VERSION = ''%s'' ) order by SP_TIPO, SP_NAME, SP_VERSION desc';
     Q_EDIT = 'select SP_VERSION, SP_NAME, SP_TIPO, SP_SEQ_NUM, SP_INTRBASE, SP_ORACLE ' +
              'from %s where ( SP_VERSION = :SP_VERSION ) and ( SP_TIPO = :SP_TIPO ) and ( SP_NAME = :SP_NAME )';
     Q_INSERT = 'insert into %s( '+
                'SP_VERSION, SP_NAME, SP_TIPO, SP_SEQ_NUM, SP_INTRBASE, SP_ORACLE ) values ( '+
                ':SP_VERSION, :SP_NAME, :SP_TIPO, :SP_SEQ_NUM, :SP_INTRBASE, :SP_ORACLE)';
     Q_DELETE = 'delete from %s where ( SP_VERSION = :OLD_SP_VERSION ) and ( SP_TIPO = :OLD_SP_TIPO ) and ( SP_NAME = :OLD_SP_NAME )';
     Q_UPDATE = 'update %s set '+
                'SP_VERSION = :SP_VERSION, '+
                'SP_NAME = :SP_NAME, '+
                'SP_TIPO = :SP_TIPO, '+
                'SP_SEQ_NUM = :SP_SEQ_NUM, '+
                'SP_INTRBASE = :SP_INTRBASE, '+
                'SP_ORACLE = :SP_ORACLE '+
                'where '+
                '( SP_VERSION = :OLD_SP_VERSION ) and '+
                '( SP_TIPO = :OLD_SP_TIPO ) and '+
                '( SP_NAME = :OLD_SP_NAME )';
     Q_INIT_LEER = 'select SP_DUMMY, SP_VERSION, SP_TIPO, SP_NAME from %s '+
                   'where ( %s is NULL ) and ( SP_DUMMY is not NULL ) and ( SP_DUMMY > "" ) '+
                   'order by SP_VERSION, SP_TIPO, SP_NAME';
     Q_INIT_BLOB = 'update %0:s set '+
                   '%1:s = :%1:s '+
                   'where '+
                   '( SP_VERSION = :SP_VERSION ) and '+
                   '( SP_TIPO = :SP_TIPO ) and '+
                   '( SP_NAME = :SP_NAME )';
     Q_EXPORTAR = 'select SP_NAME, SP_INTRBASE, SP_ORACLE, SP_VERSION, SP_SEQ_NUM, SP_TIPO from %s '+
                  'where ( SP_VERSION <= ''%s'' ) order by SP_VERSION, SP_TIPO, SP_SEQ_NUM';
     Q_BORRA_TODO = 'delete from %s';
     Q_INIT_VERSION_DELETE = 'delete from %0:s where ( SP_VERSION = ''%1:s'' )';
     Q_INIT_VERSION = 'insert into %0:s select SP_NAME, SP_INTRBASE, SP_ORACLE, ''%1:s'', SP_SEQ_NUM, SP_TIPO, SP_DUMMY '+
                      'from %0:s where ( SP_VERSION = ''%2:s'' ) and ( SP_TIPO < %3:d )';
     K_SQL_INPUT = 'create procedure %s( %s )' + LF + 'as';
     K_SQL_OUTPUT = 'create procedure %s returns ( %s )' + LF + 'as';
     K_SQL_INPUT_OUTPUT = 'create procedure %s( %s )' + LF + 'returns ( %s )' + LF + 'as';
     K_SQL_NONE = 'create procedure %s' + LF + 'as';
     K_TRIGGER_SQL = 'create trigger %s for %s %s';

type
    eTipoParametro = ( ppInput, ppOutput );

{$R *.DFM}

function GetDBType( const eValor: eDbType ): String;
const
     aDBType: array[ eDbType ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Comparte IB',
                                            'Datos IB',
                                            'Seleccion IB',
                                            'Visitantes IB',
                                            'Portal IB',
                                            'Comparte MS SQL',
                                            'Datos MS SQL',
                                            'Seleccion MS SQL',
                                            'Visitantes MS SQL',
                                            'Portal MS SQL' );
begin
     Result := aDBType[ eValor ];
end;

procedure FillVersionList( Lista: TStrings );
begin
     with Lista do
     begin
          Clear;
          BeginUpdate;
          try
             Add( '435' );
             Add( '430' );
             Add( '425' );
             Add( '420' );
             Add( '415' );
             Add( '410' );
             Add( '405' );
             Add( '401' );
             Add( '400' );
             Add( '398' );
             Add( '397' );
             Add( '396' );
             Add( '395' );
             Add( '390' );
             Add( '385' );
             Add( '380' );
             Add( '376' );
             Add( '375' );
             Add( '370' );
             Add( '365' );
             Add( '360' );
             Add( '355' );
             Add( '350' );
             Add( '345' );
             Add( '340' );
             Add( '335' );
             Add( '330' );
             Add( '325' );
             Add( '1.3' );
             Add( '1.2' );
          finally
                 EndUpdate;
          end;
     end;
end;

{ ************* TdmDBStruct **************** }

procedure TdmDBStruct.dmDBStructCreate(Sender: TObject);
begin
     FLista := TStringList.Create;
     FNewVersion := '';
     FNewTipo := 0;
     FNewSeqNum := 0;
     FDbType := eIBComparte;
end;

procedure TdmDBStruct.dmDBStructDestroy(Sender: TObject);
begin
     FLista.Free;
end;

procedure TdmDBStruct.ConectarBD( Database: TDatabase; const sAlias, sUsuario, sPassword: String );
begin
     with Database do
     begin
          Connected := False;
          AliasName := sAlias;
          with Params do
          begin
               Values[ 'USER NAME' ] := sUsuario;
               Values[ 'PASSWORD' ] := sPassword;
          end;
          Connected := True;
     end;
end;

function TdmDBStruct.DBStructConectar( const sDatabase, sUsuario, sPassword: String ): Boolean;
begin
     try
        with dbStruct do
        begin
             if not Connected or ( Params.Values[ 'SERVER NAME' ] <> sDatabase ) then
             begin
                  Connected := False;
                  with Params do
                  begin
                       Values[ 'SERVER NAME' ] := sDatabase;
                       Values[ 'USER NAME' ] := sUsuario;
                       Values[ 'PASSWORD' ] := sPassword;
                  end;
                  Connected := True;
             end;
             Result := Connected;
        end;
     except
           on Error: Exception do
           begin
                dbStruct.Connected := False;
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TdmDBStruct.DBStructDesconectar: Boolean;
begin
     try
        with dbStruct do
        begin
             Connected := False;
             Result := True;
        end;
     except
           on Error: Exception do
           begin
                dbStruct.Connected := False;
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TdmDBStruct.Conectar( const sDatabase, sUsuario, sPassword, sVersion: String ): Boolean;
begin
     Result := DBStructConectar( sDatabase, sUsuario, sPassword );
     if Result then
     begin
          try
             InitBlobFields;
             Result := Abrir;
             if not Result then
             begin
                  DBStructDesconectar;
             end;
          except
                on Error: Exception do
                begin
                     DBStructDesconectar;
                     Application.HandleException( Error );
                     Result := False;
                end;
          end;
     end;
end;

function TdmDBStruct.GetDBServer: eDbServer;
begin
     case FDBType of
          eMSSQLComparte: Result := dbSQLServer;
          eMSSQLDatos: Result := dbSQLServer;
          eMSSQLSeleccion: Result := dbSQLServer;
          eMSSQLPortal: Result := dbSQLServer;
          eMSSQLVisitantes: Result := dbSQLServer;
     else
         Result := dbInterbase;
     end;
end;

function TdmDBStruct.GetExportFile: String;
const
     K_ARCHIVO = 'D:\3win_20_Vsn_2013\Datos\Patch File\%s.db';
begin
     case FDBType of
          eIBComparte: Result := Format( K_ARCHIVO, [ 'SP_Comparte' ] );
          eIBDatos: Result := Format( K_ARCHIVO, [ 'SP_Datos' ] );
          eIBSeleccion: Result := Format( K_ARCHIVO, [ 'SP_Seleccion' ] );
          eIBVisitantes: Result := Format( K_ARCHIVO, [ 'SP_Visitantes' ] );
          eIBPortal: Result := Format( K_ARCHIVO, [ 'SP_Portal' ] );
          eMSSQLComparte: Result := Format( K_ARCHIVO, [ 'SP_MSSQL_Comparte' ] );
          eMSSQLDatos: Result := Format( K_ARCHIVO, [ 'SP_MSSQL_Datos' ] );
          eMSSQLSeleccion: Result := Format( K_ARCHIVO, [ 'SP_MSSQL_Seleccion' ] );
          eMSSQLVisitantes: Result := Format( K_ARCHIVO, [ 'SP_MSSQL_Visitantes' ] );
          eMSSQLPortal: Result := Format( K_ARCHIVO, [ 'SP_MSSQL_Portal' ] );
     else
         Result := VACIO;
     end;
end;

function TdmDBStruct.GetTableName: String;
begin
     case FDBType of
          eIBComparte: Result := 'COMPARTE_IB';
          eIBDatos: Result := 'DATOS_IB';
          eIBSeleccion: Result := 'SELECCION_IB';
          eIBVisitantes: Result := 'VISITANTES_IB';
          eIBPortal: Result := 'PORTAL_IB';
          eMSSQLComparte: Result := 'COMPARTE_MSSQL';
          eMSSQLDatos: Result := 'DATOS_MSSQL';
          eMSSQLSeleccion: Result := 'SELECCION_MSSQL';
          eMSSQLVisitantes: Result := 'VISITANTES_MSSQL';
          eMSSQLPortal: Result := 'PORTAL_MSSQL';
     else
         Result := VACIO;
     end;
end;

function TdmDBStruct.GetBlobFieldName: String;
begin
     case FDBType of
          eIBComparte: Result := 'SP_INTRBASE';
          eIBDatos: Result := 'SP_INTRBASE';
          eIBSeleccion: Result := 'SP_INTRBASE';
          eIBVisitantes: Result := 'SP_INTRBASE';
          eIBPortal: Result := 'SP_INTRBASE';
          eMSSQLComparte: Result := 'SP_ORACLE';
          eMSSQLDatos: Result := 'SP_ORACLE';
          eMSSQLSeleccion: Result := 'SP_ORACLE';
          eMSSQLVisitantes: Result := 'SP_ORACLE';
          eMSSQLPortal: Result := 'SP_ORACLE';
     else
         Result := VACIO;
     end;
end;

procedure TdmDBStruct.SetUpQueries;
var
   sTable: String;
begin
     sTable := GetTableName;
     PreparaQuery( tqSP, Format( Q_LEER, [ sTable, PatchVersion ] ) );
     PreparaQuery( tqSPSeek, Format( Q_BUSCAR, [ sTable, PatchVersion ] ) );
     PreparaQuery( tqEdSP, Format( Q_EDIT, [ sTable ] ) );
     with tqSP do
     begin
          AfterOpen := tqSPAfterOpen;
     end;
     with uqSP do
     begin
          DeleteSQL.Text := Format( Q_DELETE, [ sTable ] );
          InsertSQL.Text := Format( Q_INSERT, [ sTable ] );
          ModifySQL.Text := Format( Q_UPDATE, [ sTable ] );
     end;
end;

function TdmDBStruct.Abrir: Boolean;
begin
     SetUpQueries;
     try
        with tqSP do
        begin
             Active := False;
             Active := True;
             Result := Active;
        end;
     except
           on Error: Exception do
           begin
                tqSP.Active := False;
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmDBStruct.MuestraTipo( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     case Sender.AsInteger of
          K_TRIGGER: Text := 'TGR';
          K_STORED_PROCEDURE: Text := 'SP';
          K_SQL: Text := 'SQL';
     else
         Text := '?'
     end;
end;

procedure TdmDBStruct.tqSPAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'SP_TIPO' ).OnGetText := MuestraTipo;
     end;
end;

procedure TdmDBStruct.SetImportaCodigo( const iTipo, iDBType: Integer; const sVersion: String );
begin
     SetStoredProcedure( spImport, Format( 'IMPORTA_%s', [ GetTableName ] ) );
     with spImport do
     begin
          ParamByName( 'SP_VERSION' ).AsString := sVersion;
          ParamByName( 'SP_TIPO' ).AsInteger := iTipo;
          {
          ParamByName( 'iClase' ).AsInteger := iDBType;
          }
     end;
end;

procedure TdmDBStruct.ResetImportaCodigo;
begin
     ResetStoredProcedure( spImport );
end;

procedure TdmDBStruct.SetStoredProcedure( StoredProc: TStoredProc; const sStoredProcName: String );
begin
     ResetStoredProcedure( StoredProc );
     with StoredProc do
     begin
          StoredProcName := sStoredProcName;
          Prepare;
     end;
end;

procedure TdmDBStruct.ResetStoredProcedure( StoredProc: TStoredProc );
begin
     with StoredProc do
     begin
          if Active then
             Active := False;
          if Prepared then
             UnPrepare;
     end;
end;

procedure TdmDBStruct.PreparaQuery( Query: TQuery; const sSQL: String );
begin
     DespreparaQuery( Query );
     with Query do
     begin
          with SQL do
          begin
               Clear;
               Text := sSQL;
          end;
          Prepare;
     end;
end;

procedure TdmDBStruct.DespreparaQuery( Query: TQuery );
begin
     with Query do
     begin
          Active := False;
          if Prepared then
             Unprepare;
     end;
end;

function TdmDBStruct.Version: String;
begin
     Result := tqSp.FieldByName( 'SP_VERSION' ).AsString;
end;

function TdmDBStruct.Llave: String;
begin
     Result := tqSp.FieldByName( 'SP_NAME' ).AsString;
end;

function TdmDBStruct.SQLType: Integer;
begin
     Result := tqSp.FieldByName( 'SP_TIPO' ).AsInteger;
end;

function TdmDBStruct.HayDatos: Boolean;
begin
     Result := not tqSP.IsEmpty;
end;

function TdmDBStruct.Conectada: Boolean;
begin
     Result := dbStruct.Connected;
end;

procedure TdmDBStruct.GuardaLlave;
begin
     with tqEdSp do
     begin
          FVersionValue := FieldByName( 'SP_VERSION' ).AsString;
          FSQLType := FieldByName( 'SP_TIPO' ).AsInteger;
          FLlave := FieldByName( 'SP_NAME' ).AsString;
     end;
end;

procedure TdmDBStruct.Refrescar;
begin
     with tqSP do
     begin
          Active := False;
          Active := True;
          Posicionar( FVersionValue, FLlave, FSQLType );
     end;
end;

procedure TdmDBStruct.Borrar( const DataSets: array of TDBDataSet );
var
   i: Integer;
begin
     for i := 0 to High( Datasets ) do
     begin
          with Datasets[ 0 ] do
          begin
               Delete;
          end;
     end;
     Datasets[ 0 ].Database.ApplyUpdates( Datasets );
end;

procedure TdmDBStruct.Grabar( const DataSets: array of TDBDataSet );
var
   Apuntador: TBookMarkStr;
   i: Integer;
begin
     for i := 0 to High( Datasets ) do
     begin
          with Datasets[ i ] do
          begin
               if ( State in [ dsEdit, dsInsert ] ) then
                  Post;
          end;
     end;
     with Datasets[ 0 ] do
     begin
          Apuntador := BookMark;
          try
             Database.ApplyUpdates( Datasets );
             BookMark := Apuntador;
          except
                on Error: Exception do
                begin
                     BookMark := Apuntador; { El Dataset es movido por Dataset.ApplyUpdates }
                     if not ( State in [ dsEdit, dsInsert ] ) then
                        Edit;
                     SysUtils.Abort; { La verdadera Excepcion fu� procesada por Application.HandleException dentro de Dataset.ApplyUpdates }
                end;
          end;
     end;
end;

procedure TdmDBStruct.ConectarBusqueda;
begin
     with tqSPSeek do
     begin
          Active := False;
          if not Active then
          begin
               Active := True;
          end;
     end;
     PosicionarBusqueda( Llave );
end;

procedure TdmDBStruct.Posicionar( const sVersion, sLlave: String; const iTipo: Integer );
begin
     with tqSP do
     begin
          Locate( 'SP_VERSION;SP_TIPO;SP_NAME', VarArrayOf( [ sVersion, iTipo, sLlave ] ), [] );
     end;
end;

procedure TdmDBStruct.PosicionarBusqueda( const sLlave: String );
begin
     with tqSPSeek do
     begin
          Locate( 'SP_NAME', UpperCase( sLlave ), [ loPartialKey ] );
     end;
end;

procedure TdmDBStruct.PosicionarResultado;
var
   sVersion, sLlave: String;
begin
     with tqSPSeek do
     begin
          sVersion := FieldByName( 'SP_VERSION' ).AsString;
          sLlave := FieldByName( 'SP_NAME' ).AsString;
     end;
     with tqSP do
     begin
          Locate( 'SP_VERSION;SP_NAME', VarArrayOf( [ sVersion, sLlave ] ), [] );
     end;
end;

procedure TdmDBStruct.PosicionarEdicion;
begin
     with tqEdSP do
     begin
          Active := False;
          ParamByName( 'SP_VERSION' ).AsString := Version;
          ParamByName( 'SP_TIPO' ).AsInteger := SQLType;
          ParamByName( 'SP_NAME' ).AsString := Llave;
          Active := True;
     end;
     GuardaLlave;
end;

procedure TdmDBStruct.Editar( const lInsert: Boolean );
begin
     PosicionarEdicion;
     with tqEdSP do
     begin
          if lInsert then
             Insert
          else
              Edit;
     end;
end;

function TdmDBStruct.EscribeCambios: Boolean;
begin
     try
        GuardaLlave;
        Grabar( [ tqEdSP ] );
        CancelaCambios;
        Refrescar;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmDBStruct.CancelaCambio( BDEDataset: TBDEDataset );
begin
     with BDEDataset do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
          begin
               CancelUpdates;
          end;
          Active := False;
     end;
end;

procedure TdmDBStruct.CancelaCambios;
begin
     CancelaCambio( tqEdSP );
end;

procedure TdmDBStruct.BorrarRegistro;
begin
     try
        PosicionarEdicion;
        Borrar( [ tqEdSP ] );
        CancelaCambios;
        Refrescar;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TdmDBStruct.InitVersion( const sOriginal, sNueva: String );
begin
     with dbStruct do
     begin
          StartTransaction;
          try
             PreparaQuery( tqExport, Format( Q_INIT_VERSION_DELETE, [ GetTableName, sNueva ] ) );
             tqExport.ExecSQL;
             PreparaQuery( tqExport, Format( Q_INIT_VERSION, [ GetTableName, sNueva, sOriginal, K_SQL ] ) );
             tqExport.ExecSQL;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
     Abrir;
end;

procedure TdmDBStruct.ImportarParadox( const sFileName: String );
var
   lSiHay: Boolean;
   i: Integer;
begin
     try
        try
           with taExport do
           begin
                Active := False;
                DatabaseName := ExtractFilePath( sFileName );
                TableName := ExtractFileName( sFileName );
                Active := True;
                lSiHay := ( RecordCount > 0 );
           end;
           if lSiHay then
           begin
                with dbStruct do
                begin
                     StartTransaction;
                     try
                        PreparaQuery( tqExport, Format( Q_BORRA_TODO, [ GetTableName ] ) );
                        tqExport.ExecSQL;
                        PreparaQuery( tqExport, Format( Q_INSERT, [ GetTableName ] ) );
                        with taExport do
                        begin
                             while not Eof do
                             begin
                                  for i := 0 to ( FieldCount - 1 ) do
                                  begin
                                       with Fields[ i ] do
                                       begin
                                            if ( Fields[ i ] is TBlobField ) and ( TBlobField( Fields[ i ] ).BlobSize = 0 ) then
                                               tqExport.ParamByName( FieldName ).AsBlob := TBlobData( '' )
                                            else
                                                tqExport.ParamByName( FieldName ).AssignField( Fields[ i ] );
                                       end;
                                  end;
                                  {
                                  ParamByName( 'SP_NAME' ).AsString := FieldByName( 'SP_NAME' ).AsString;
                                  ParamByName( 'SP_INTRBASE' ).AsString := FieldByName( 'SP_INTRBASE' ).As
                                  ParamByName( 'SP_ORACLE' ).AsString := FieldByName( 'SP_ORACLE' ).As
                                  ParamByName( 'SP_VERSION' ).AsString := FieldByName( 'SP_VERSION' ).AsString;
                                  ParamByName( 'SP_SEQ_NUM' ).AsInteger := FieldByName( 'SP_SEQ_NUM' ).AsInteger;
                                  ParamByName( 'SP_TIPO' ).AsInteger := FieldByName( 'SP_TIPO' ).AsInteger;
                                  }
                                  tqExport.ExecSQL;
                                  Next;
                             end;
                        end;
                        Commit;
                     except
                           on Error: Exception do
                           begin
                                Rollback;
                                Application.HandleException( Error );
                           end;
                     end;
                end;
                Abrir;
           end
           else
               raise Exception.Create( Format( 'Tabla %s Est� Vac�a', [ sFileName ] ) );
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            with taExport do
            begin
                 Active := False;
            end;
     end;
end;

function TdmDBStruct.ExportarParadox( const sVersion, sArchivo: String ): Boolean;
begin
     Result := False;
     try
        with tqExport do
        begin
             PreparaQuery( tqExport, Format( Q_EXPORTAR, [ GetTableName, sVersion ] ) );
             Active := True;
        end;
        with taExport do
        begin
             Active := False;
             DatabaseName := ExtractFilePath( sArchivo );
             TableName := ExtractFileName( sArchivo );
        end;
        with BatchMove do
        begin
             Execute;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                with tqExport do
                begin
                     Active := False;
                end;
                Application.HandleException( Error );
           end;
     end;
end;

procedure TdmDBStruct.InitBlobFields;
var
   sBlobFieldName: String;
begin
     sBlobFieldName := GetBlobFieldName;
     with dbStruct do
     begin
          StartTransaction;
          try
             PreparaQuery( tqSP, Format( Q_INIT_LEER, [ GetTableName, sBlobFieldName ] ) );
             PreparaQuery( tqExport, Format( Q_INIT_BLOB, [ GetTableName, sBlobFieldName ] ) );
             with tqSP do
             begin
                  DisableControls;
                  Active := True;
                  try
                     while not Eof do
                     begin
                          tqExport.ParamByName( sBlobFieldName ).AsBlob := TBlobData( FieldByName( 'SP_DUMMY' ).AsString );
                          tqExport.ParamByName( 'SP_VERSION' ).AsString := FieldByName( 'SP_VERSION' ).AsString;
                          tqExport.ParamByName( 'SP_TIPO' ).AsInteger := FieldByName( 'SP_TIPO' ).AsInteger;
                          tqExport.ParamByName( 'SP_NAME' ).AsString := FieldByName( 'SP_NAME' ).AsString;
                          tqExport.ExecSQL;
                          Next;
                     end;
                  finally
                         Active := False;
                         EnableControls;
                  end;
             end;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

{ ************ Importar Triggers de Interbase *************** }

procedure TdmDBStruct.ImportInterbaseTriggers( const sAlias, sUsuario, sPassword, sVersion: String );
var
   sTrigger, sTabla, sSource, sTipo: String;
   iSeqNum: Integer;
begin
     FLlave := Llave;
     with dbStruct do
     begin
          StartTransaction;
          try
             SetImportaCodigo( K_TRIGGER, K_INTERBASE, sVersion );
             ConectarBD( dbTress, sAlias, sUsuario, sPassword );
             PreparaQuery( tqImport, 'select RDB$TRIGGER_NAME, RDB$RELATION_NAME, RDB$TRIGGER_TYPE, RDB$TRIGGER_SOURCE from RDB$TRIGGERS where ( RDB$SYSTEM_FLAG is NULL ) order by RDB$RELATION_NAME, RDB$TRIGGER_NAME' );
             with tqImport do
             begin
                  Active := True;
                  iSeqNum := 0;
                  while not Eof do
                  begin
                       iSeqNum := iSeqNum + 1;
                       sTrigger := FieldByName( 'RDB$TRIGGER_NAME' ).AsString;
                       sTabla := FieldByName( 'RDB$RELATION_NAME' ).AsString;
                       case FieldByName( 'RDB$TRIGGER_TYPE' ).AsInteger of
                            1: sTipo := 'BEFORE INSERT';
                            2: sTipo := 'AFTER INSERT';
                            3: sTipo := 'BEFORE UPDATE';
                            4: sTipo := 'AFTER UPDATE';
                            5: sTipo := 'BEFORE DELETE';
                            6: sTipo := 'AFTER DELETE';
                       end;
                       sSource := Format( K_TRIGGER_SQL, [ sTrigger, sTabla, sTipo ] ) + LF + FieldByName( 'RDB$TRIGGER_SOURCE' ).AsString;
                       with spImport do
                       begin
                            ParamByName( 'SP_NAME' ).AsString := sTrigger;
                            ParamByName( 'SP_SEQ_NUM' ).AsInteger := iSeqNum;
                            ParamByName( 'SP_BLOB' ).AsBlob := TBlobData( sSource );
                            ExecProc;
                       end;
                       Next;
                  end;
                  Active := False;
             end;
             Commit;
             Refrescar;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
     ResetImportaCodigo;
     DespreparaQuery( tqImport );
     dbTress.Connected := False;
end;

{ ************ Importar Stored Procedures de Interbase *************** }

procedure TdmDBStruct.DeterminaDependencias( const sValor: String );
var
   FDependencias: TStrings;
   i: Integer;
begin
     FDependencias := TStringList.Create;
     try
        try
           with tqIBSPDep do
           begin
                Active := False;
                ParamByName( 'SP_NAME' ).AsString := sValor;
                Active := True;
                while not Eof do
                begin
                     FDependencias.Add( FieldByName( 'SP_DEPEND' ).AsString );
                     Next;
                end;
                Active := False;
           end;
        except
              on Error: Exception do
              begin
                   FDependencias.Clear;
                   Application.HandleException( Error );
              end;
        end;
        with FDependencias do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  DeterminaDependencias( Strings[ i ] );
             end;
        end;
        with FLista do
        begin
             if ( IndexOf( sValor ) < 0 ) then  { No Existe en la Lista }
                Append( sValor );               { Agrega al Final }
        end;
     finally
            FDependencias.Free;
     end;
end;

procedure TdmDBStruct.ListStoredProcedures;
var
   i: Integer;
   FProcList: TStrings;
begin
     with FLista do
     begin
          BeginUpdate;
          Clear;
     end;
     FProcList := TStringList.Create;
     BDEGetSPList( dbTress.Handle, FProcList );
     with FProcList do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               DeterminaDependencias( Strings[ i ] );
          end;
     end;
     with FLista do
     begin
          EndUpdate;
     end;
     FProcList.Free;
end;

procedure TdmDBStruct.ImportInterbaseProcedures( const sAlias, sUsuario, sPassword, sVersion: String );
var
   sProcedure, sInputParams, sOutputParams, sSource: String;
   lInput, lOutput: Boolean;
   i: Integer;

function GetParamList( const eTipo: eTipoParametro ): String;
var
   sTipo, sLength, sDecimales: String;
begin
     Result := '';
     with tqImpDet do
     begin
          Active := False;
          ParamByName( 'SP_NAME' ).AsString := sProcedure;
          ParamByName( 'iTipo' ).AsInteger := Ord( eTipo );
          Active := True;
          while not Eof do
          begin
               sLength := FieldByName( 'RDB$FIELD_LENGTH' ).AsString;
               sDecimales := IntToStr( Abs( FieldByName( 'RDB$FIELD_SCALE' ).AsInteger ) );
               case FieldByName( 'RDB$FIELD_TYPE' ).AsInteger of
                    7: sTipo := 'SmallInt';
                    8: sTipo := 'Integer';
                    9: sTipo := 'Quad';
                    10: sTipo := 'Float';
                    11: sTipo := 'D_Float';
                    14: sTipo := Format( 'Char( %s )', [ sLength ] );
                    27: sTipo := Format( 'Numeric( 15, %s )', [ sDecimales ] );
                    35: sTipo := 'Date';
                    37: sTipo := Format( 'VarChar( %s )', [ sLength ] );
                    261: sTipo := 'Blob';
               end;
               Result := Result +
                         FieldByName( 'RDB$PARAMETER_NAME' ).AsString +
                         ' ' +
                         sTipo +
                         ',' +
                         LF;
               Next;
          end;
          Result := Copy( Result, 1, ( Length( Result ) - 2 ) );
          Active := False;
     end;
end;

begin
     FLlave := Llave;
     with dbStruct do
     begin
          StartTransaction;
          try
             SetImportaCodigo( K_STORED_PROCEDURE, K_INTERBASE, sVersion );
             ConectarBD( dbTress, sAlias, sUsuario, sPassword );
             ListStoredProcedures;
             PreparaQuery( tqImport, 'select RDB$PROCEDURE_INPUTS, RDB$PROCEDURE_OUTPUTS, RDB$PROCEDURE_SOURCE from RDB$PROCEDURES where ( RDB$PROCEDURE_NAME = :SP_NAME )' );
             PreparaQuery( tqImpDet, 'select RDB$PARAMETER_NAME, RDB$FIELD_TYPE, RDB$FIELD_SUB_TYPE, RDB$FIELD_LENGTH, RDB$FIELD_SCALE ' +
                                     'from RDB$PROCEDURE_PARAMETERS P ' +
                                     'left join RDB$FIELDS F on ( RDB$FIELD_NAME = RDB$FIELD_SOURCE ) ' +
                                     'where ( RDB$PROCEDURE_NAME = :SP_NAME ) and ( RDB$PARAMETER_TYPE = :iTipo )' );

             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       sProcedure := Strings[ i ];
                       with tqImport do
                       begin
                            Active := False;
                            ParamByName( 'SP_NAME' ).AsString := sProcedure;
                            Active := True;
                            if not IsEmpty then
                            begin
                                 lInput := ( FieldByName( 'RDB$PROCEDURE_INPUTS' ).AsInteger > 0 );
                                 lOutput := ( FieldByName( 'RDB$PROCEDURE_OUTPUTS' ).AsInteger > 0 );
                                 if lInput then
                                    sInputParams := GetParamList( ppInput );
                                 if lOutput then
                                    sOutputParams := GetParamList( ppOutput );
                                 if ( lInput and lOutput ) then
                                    sSource := Format( K_SQL_INPUT_OUTPUT, [ sProcedure, sInputParams, sOutputParams ] )
                                 else
                                     if lInput then
                                        sSource := Format( K_SQL_INPUT, [ sProcedure, sInputParams ] )
                                     else
                                         if lOutput then
                                            sSource := Format( K_SQL_OUTPUT, [ sProcedure, sOutputParams ] )
                                         else
                                             sSource := Format( K_SQL_NONE, [ sProcedure ] );
                                 sSource := sSource + LF + Trim( FieldByName( 'RDB$PROCEDURE_SOURCE' ).AsString );
                                 with spImport do
                                 begin
                                      ParamByName( 'SP_NAME' ).AsString := sProcedure;
                                      ParamByName( 'SP_SEQ_NUM' ).AsInteger := i;
                                      ParamByName( 'SP_BLOB' ).AsBlob := TBlobData( sSource );
                                      ExecProc;
                                 end;
                            end;
                       end;
                  end;
             end;
             Commit;
             Refrescar;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
     ResetImportaCodigo;
     DespreparaQuery( tqImport );
     DespreparaQuery( tqImpDet );
     dbTress.Connected := False;
end;

{ *********** Importar SQL-Server Triggers ************** }

function TdmDBStruct.FormatMetaData( const sOriginal: String ): String;
begin
     Result := sOriginal;
     Result := ZetaCommonTools.StrTransAll( Result, ' begin', CR_LF + 'begin' + CR_LF );
     Result := ZetaCommonTools.StrTransAll( Result, ' end', CR_LF + 'end' + CR_LF );
     Result := ZetaCommonTools.StrTransAll( Result, ';', ';' + CR_LF );
end;

procedure TdmDBStruct.ImportMSSQLMetadata( const lTriggers: Boolean; const sAlias, sUsuario, sPassword, sVersion: String );
const
     Q_LIST_METADATA = 'select DBO.SYSOBJECTS.ID SP_ID, '+
                         'CAST( DBO.SYSOBJECTS.NAME as VARCHAR( 50 ) ) SP_NAME, '+
                         'DBO.SYSCOMMENTS.COLID SP_COLID, ' +
                         'CAST( DBO.SYSCOMMENTS.TEXT as VARCHAR( 8000 ) ) SP_TEXT '+
                         'from DBO.SYSOBJECTS left outer join DBO.SYSCOMMENTS on ( DBO.SYSCOMMENTS.ID = DBO.SYSOBJECTS.ID ) '+
                         'where ( DBO.SYSOBJECTS.TYPE in ( %s ) ) '+
                         'order by SP_ID, SP_COLID';
var
   sNombre, sSource, sFiltro: String;
   i, iTipo: Integer;

   procedure AgregaRegistroMetadata;
   begin
        with spImport do
        begin
             ParamByName( 'SP_NAME' ).AsString := sNombre;
             ParamByName( 'SP_SEQ_NUM' ).AsInteger := i;
             ParamByName( 'SP_BLOB' ).AsBlob := TBlobData( FormatMetadata( sSource ) );
             ExecProc;
        end;
   end;

begin
     if lTriggers then
     begin
          iTipo := K_TRIGGER;
          sFiltro := '''TR''' ;
     end
     else
     begin
          iTipo := K_STORED_PROCEDURE;
          sFiltro := '''FN'', ''IF'', ''TF'', ''P''';
     end;
     with dbStruct do
     begin
          StartTransaction;
          try
             SetImportaCodigo( iTipo, K_SQL_SERVER, sVersion );
             ConectarBD( dbTress, sAlias, sUsuario, sPassword );
             PreparaQuery( tqImport, Format( Q_LIST_METADATA, [ sFiltro ] ) );
             i := 1;
             with tqImport do
             begin
                  Active := True;

                  sSource := VACIO;
                  while not Eof do
                  begin
                       if ( FieldByName( 'SP_COLID' ).AsInteger = 1 ) and strLleno( sSource ) then
                       begin
                            AgregaRegistroMetadata;
                            sSource := VACIO;
                            Inc( i );
                       end;
                       sNombre := FieldByName( 'SP_NAME' ).AsString;
                       sSource := sSource + FieldByName( 'SP_TEXT' ).AsString;
                       Next;
                  end;
                  if strLleno( sSource ) then        // Para Procesar el Ultimo Registro
                     AgregaRegistroMetadata;

                  Active := False;
             end;
             Commit;
             Refrescar;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
     ResetImportaCodigo;
     DespreparaQuery( tqImport );
     dbTress.Connected := False;
end;

procedure TdmDBStruct.ImportMSSQLTriggers( const sAlias, sUsuario, sPassword, sVersion: String );
begin
     ImportMSSQLMetadata( True, sAlias, sUsuario, sPassword, sVersion );
end;

{ *********** Importar SQL-Server Stored Procedures ************** }

procedure TdmDBStruct.ImportMSSQLProcedures( const sAlias, sUsuario, sPassword, sVersion: String );
begin
     ImportMSSQLMetadata( False, sAlias, sUsuario, sPassword, sVersion );
end;

{ *************** Eventos ************* }

procedure TdmDBStruct.tqEdSpBeforePost(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FNewVersion := FieldByName( 'SP_VERSION' ).AsString;
          FNewTipo := FieldByName( 'SP_TIPO' ).AsInteger;
          FNewSeqNum := FieldByName( 'SP_SEQ_NUM' ).AsInteger;
          if ( FNewVersion = '' ) then
             raise EDatabaseError.Create( '� N�mero de Versi�n No Puede Estar Vac�o !' );
          if ( FieldByName( 'SP_NAME' ).AsString = '' ) then
             raise EDatabaseError.Create( '� Nombre No Puede Estar Vac�o !' );
          if ( FNewSeqNum = 0 ) then
             raise EDatabaseError.Create( '� # de Sequencia No Puede Estar Vac�o !' );
     end;
end;

procedure TdmDBStruct.tqEdSpNewRecord(DataSet: TDataSet);
begin
     with Dataset do
     begin
          with FieldByName( 'SP_VERSION' ) do
          begin
               if ZetaCommonTools.StrVacio( FNewVersion ) then
                  AsString := Version
               else
                   AsString := FNewVersion;
          end;
          FieldByName( 'SP_TIPO' ).AsInteger := FNewTipo;
          FieldByName( 'SP_SEQ_NUM' ).AsInteger := FNewSeqNum + 1;
     end;
end;

function TdmDBStruct.ComparaStrings( const sOriginal, sTemporal: string ): boolean;
const
     K_ESPACIO = ' ';

var
   i: integer;
   sWorkOriginal, sWorkTemporal: string;

   function QuitaEspacios( sString: string ): string;
   var
      sTemp: string;
      i: integer;
      lComentario: boolean;

   begin
        lComentario := FALSE;
        for i := 1 to length( sString ) do
        begin
             if sString[ i ] = '/' then
                if sString[ i + 1 ] = '*' then
                   lComentario := TRUE;
             if sString[ i ] = '*' then
                if sString[ i + 1 ] = '/' then
                   begin
                        lComentario := FALSE;
                        sString[ i ] := K_ESPACIO;
                        sString[ i + 1 ] := K_ESPACIO;
                   end;
             if ( sString[ i ] <> K_ESPACIO ) and ( ( sString[ i ] <> #0 ) and ( sString[ i ] <> #9 ) and ( sString[ i ] <> #10 ) and ( sString[ i ] <> #13 ) ) then
             begin
                  if not lComentario then
                     sTemp := sTemp + sString [ i ];
             end;
        end;
        Result := sTemp;
   end;

begin
     Result := TRUE;
     { Asignacion de las variables de Trabajo }
     sWorkOriginal := sOriginal;
     sWorkTemporal := sTemporal;

     { Se quitan los espacios que tengan los strings }
     sWorkOriginal := AnsiLowerCase( QuitaAcentos( QuitaEspacios( sWorkOriginal ) ) );
     sWorkTemporal := AnsiLowerCase( QuitaAcentos( QuitaEspacios( sWorkTemporal ) ) );

     { Si la Longitud es diferente los strings son diferentes }
     if Length( sWorkOriginal ) <> Length( sWorkTemporal ) then
        Result := FALSE
     else
     begin
          for i := 1 to Length( sWorkOriginal ) do
          begin
               if sWorkOriginal[ i ] <> sWorkTemporal[ i ] then
               begin
                    Result := FALSE;
                    Break;
               end
          end;
     end;
end;

procedure TdmDBStruct.BorraLog( const sFile: string );
begin
     if FileExists( sFile ) then
        DeleteFile( sFile );
end;

function TdmDBStruct.CompararMSSQL( const sAlias, sUsuario, sPassword, sVersion: String; var sLog: String): Boolean;
const
     K_TGR_SP_MSSQL = '%sDiferenciasMSSQL.log';
     Q_LIST_UN_METADATA = 'select DBO.SYSOBJECTS.ID SP_ID, ' +
                          'CAST( DBO.SYSOBJECTS.NAME as VARCHAR( 50 ) ) SP_NAME, ' +
                          'DBO.SYSCOMMENTS.COLID SP_COLID, ' +
                          'CAST( DBO.SYSCOMMENTS.TEXT as VARCHAR( 8000 ) ) SP_TEXT ' +
                          'from DBO.SYSOBJECTS left outer join DBO.SYSCOMMENTS on ( DBO.SYSCOMMENTS.ID = DBO.SYSOBJECTS.ID ) '+
                          'where ( DBO.SYSOBJECTS.NAME = %s )' +
                          'order by SP_ID, SP_COLID';
var
   sName, sNombre, sVersionMSSQL: string;
   sOriginal, sSource: string;
   fListaErrores: Tstrings;

begin
     sLog := Format( K_TGR_SP_MSSQL, [ ExtractFilePath( Application.ExeName ) ] );
     Result := True;
     FContador := 0;
     with dbStruct do
     begin
          fListaErrores := TStringList.Create;
          try
             ConectarBD( dbTress, sAlias, sUsuario, sPassword );
             with tqSP do
             begin
                  DisableControls;
                  First;
                  while not Eof do
                  begin
                       sName := FieldByName( 'SP_NAME' ).AsString;
                       sOriginal := FieldByName( 'SP_ORACLE' ).AsString;
                       sVersionMSSQL := FieldByName( 'SP_VERSION' ).AsString;
                       if ( sVersionMSSQL = sVersion ) then
                       begin
                            PreparaQuery( tqImport, Format( Q_LIST_UN_METADATA, [ EntreComillas( sName ) ] ) );
                            sSource := VACIO;
                            with tqImport do
                            begin
                                 Active := True;
                                 while not Eof do
                                 begin
                                      sNombre := FieldByName( 'SP_NAME' ).AsString;
                                      sSource := sSource + FieldByName( 'SP_TEXT' ).AsString;
                                      Next;
                                 end;
                                 if strLleno( sSource ) then
                                 begin
                                      if not ( ComparaStrings( sOriginal, sSource ) ) then
                                      begin
                                           with fListaErrores do
                                           begin
                                                Add( '( Patch ) --> ' + sName );
                                                Add( '( DBMSSQL ) --> ' + sNombre );
                                                Add( VACIO );
                                                FContador := FContador + 1;
                                           end;
                                           Result := False;
                                      end;
                                 end;
                                 sName := VACIO;
                                 sNombre := VACIO;
                                 sSource := VACIO;
                                 Active := False;
                            end;
                       end;
                       Next;
                  end;
                  EnableControls;
             end;
             if Not Result then
             begin
                  with fListaErrores do
                  begin
                       Add( 'Registros Totales: ' + IntToStr(  FContador ) );
                       SaveToFile( sLog );
                  end;
             end
             else
                 BorraLog( sLog );
          finally
                 FreeAndNil( fListaErrores );
                 DespreparaQuery( tqImport );
                 dbTress.Connected := False;
          end;
     end;
end;

function TdmDBStruct.CompararTriggersIB( const sAlias, sUsuario, sPassword, sVersion: String; var sLog: String): Boolean;
const
     K_TRIGGER_IB = '%sDiferenciasTriggersIB.log';
var
   sTrigger, sTabla, sSource, sTipo, sVersionIB: string;
   sName, sOriginal: string;
   fListaErrores: TStrings;
begin
     sLog := Format( K_TRIGGER_IB, [ ExtractFilePath( Application.ExeName ) ] );
     Result := True;
     FContador := 0;
     with dbStruct do
     begin
          fListaErrores := TStringList.Create;
          try
             SetImportaCodigo( K_TRIGGER, K_INTERBASE, sVersion );
             ConectarBD( dbTress, sAlias, sUsuario, sPassword );
             with tqSP do
             begin
                  DisableControls;
                  First;
                  while not Eof do
                  begin
                       sName := FieldByName( 'SP_NAME' ).AsString;
                       sOriginal := FieldByName( 'SP_INTRBASE' ).AsString;
                       sVersionIB := FieldByName( 'SP_VERSION' ).AsString;
                       if ( sVersionIB = sVersion ) and ( FieldByName( 'SP_TIPO' ).AsInteger = K_TRIGGER ) then
                       begin
                            PreparaQuery( tqImport, Format( 'select RDB$TRIGGER_NAME, RDB$RELATION_NAME, RDB$TRIGGER_TYPE, RDB$TRIGGER_SOURCE from RDB$TRIGGERS where RDB$TRIGGER_NAME = %s  order by RDB$RELATION_NAME, RDB$TRIGGER_NAME', [ EntreComillas( sName ) ] ) );
                            with tqImport do
                            begin
                                 Active := True;
                                 while not Eof do
                                 begin
                                      sTrigger := FieldByName( 'RDB$TRIGGER_NAME' ).AsString;
                                      sTabla := FieldByName( 'RDB$RELATION_NAME' ).AsString;
                                      case FieldByName( 'RDB$TRIGGER_TYPE' ).AsInteger of
                                           1: sTipo := 'BEFORE INSERT';
                                           2: sTipo := 'AFTER INSERT';
                                           3: sTipo := 'BEFORE UPDATE';
                                           4: sTipo := 'AFTER UPDATE';
                                           5: sTipo := 'BEFORE DELETE';
                                           6: sTipo := 'AFTER DELETE';
                                      end;
                                      sSource := Format( K_TRIGGER_SQL, [ sTrigger, sTabla, sTipo ] ) + LF + FieldByName( 'RDB$TRIGGER_SOURCE' ).AsString;
                                      Next;
                                 end;
                                 if strLleno( sSource ) then
                                 begin
                                      if not ( ComparaStrings( sOriginal, sSource ) ) then
                                      begin
                                           with fListaErrores do
                                           begin
                                                Add( '( Patch ) --> ' + sName );
                                                Add( '( DataBase ) --> ' + sTrigger );
                                                Add( VACIO );
                                                FContador := Fcontador + 1;
                                           end;
                                           Result := False;
                                      end;
                                 end;
                                 sName := VACIO;
                                 sTrigger := VACIO;
                                 sSource := VACIO;
                                 Active := False;
                            end;
                       end;
                       next;
                  end;
                  EnableControls;
            end;
             if Not Result then
             begin
                  with fListaErrores do
                  begin
                       Add( 'Registros Totales: ' + IntToStr(  FContador ) );
                       fListaErrores.SaveToFile( sLog );
                  end;
             end
             else
                 BorraLog( sLog );
          finally
                 FreeAndNil( fListaErrores );
                 DespreparaQuery( tqImport );
                 dbTress.Connected := False;
          end;
     end;
end;

function TdmDBStruct.CompararSPIB( const sAlias, sUsuario, sPassword, sVersion: String; var sLog: String): Boolean;
const
     K_STORED_IB = '%sDiferenciasStoredProcIB.log';
var
   sProcedure, sInputParams, sOutputParams, sSource: String;
   sName, sOriginal, sVersionIB: string;
   lInput, lOutput: Boolean;
   fListaErrores: TStrings;

   function GetParamList( const eTipo: eTipoParametro ): String;
   var
      sTipo, sLength, sDecimales: String;
      iTipo: integer;
   begin
        Result := VACIO;
        iTipo := ord( eTipo );
        PreparaQuery( tqImpDet, Format( 'select RDB$PARAMETER_NAME, RDB$FIELD_TYPE, RDB$FIELD_SUB_TYPE, RDB$FIELD_LENGTH, RDB$FIELD_SCALE ' +
                                        'from RDB$PROCEDURE_PARAMETERS P ' +
                                        'left join RDB$FIELDS F on ( RDB$FIELD_NAME = RDB$FIELD_SOURCE ) ' +
                                        'where ( RDB$PROCEDURE_NAME = %s ) and ( RDB$PARAMETER_TYPE = %d ) order by RDB$PARAMETER_NUMBER', [ EntreComillas( sName ), iTipo ] ) );
        try
           with tqImpDet do
           begin
                Active := False;
                Active := True;
                while not Eof do
                begin
                     sLength := FieldByName( 'RDB$FIELD_LENGTH' ).AsString;
                     sDecimales := IntToStr( Abs( FieldByName( 'RDB$FIELD_SCALE' ).AsInteger ) );
                     case FieldByName( 'RDB$FIELD_TYPE' ).AsInteger of
                          7: sTipo := 'SmallInt';
                          8: sTipo := 'Integer';
                          9: sTipo := 'Quad';
                          10: sTipo := 'Float';
                          11: sTipo := 'D_Float';
                          14: sTipo := Format( 'Char( %s )', [ sLength ] );
                          27: sTipo := Format( 'Numeric( 15, %s )', [ sDecimales ] );
                          35: sTipo := 'Date';
                          37: sTipo := Format( 'VarChar( %s )', [ sLength ] );
                          261: sTipo := 'Blob';
                     end;
                     Result := Result +
                               FieldByName( 'RDB$PARAMETER_NAME' ).AsString +
                               ' ' +
                               sTipo +
                               ',' +
                               LF;
                     Next;
                end;
                Result := Copy( Result, 1, ( Length( Result ) - 2 ) );
                Active := False;
           end;
        finally
               DespreparaQuery( tqImpDet );
        end;
   end;

begin
     sLog := Format( K_STORED_IB, [ ExtractFilePath( Application.ExeName ) ] );
     Result := True;
     FContador := 0;
     with dbStruct do
     begin
          fListaErrores := TStringList.Create;
          try
             SetImportaCodigo( K_STORED_PROCEDURE, K_INTERBASE, sVersion );
             ConectarBD( dbTress, sAlias, sUsuario, sPassword );
             with tqSP do
             begin
                  DisableControls;
                  First;
                  while not Eof do
                  begin
                       sName := FieldByName( 'SP_NAME' ).AsString;
                       sOriginal := FieldByName( 'SP_INTRBASE' ).AsString;
                       sVersionIB := FieldByName( 'SP_VERSION' ).AsString;
                       if ( sVersionIB = sVersion ) and  ( FieldByName( 'SP_TIPO' ).AsInteger = K_STORED_PROCEDURE ) then
                       begin
                            PreparaQuery( tqImport, Format( 'select RDB$PROCEDURE_INPUTS, RDB$PROCEDURE_OUTPUTS, RDB$PROCEDURE_NAME, RDB$PROCEDURE_SOURCE from RDB$PROCEDURES where ( RDB$PROCEDURE_NAME = %s )', [ EntreComillas( sName ) ] ) );
                            with tqImport do
                            begin
                                 Active := True;
                                 while not Eof do
                                 begin
                                      sProcedure := FieldByName( 'RDB$PROCEDURE_NAME' ).AsString;
                                      lInput := ( FieldByName( 'RDB$PROCEDURE_INPUTS' ).AsInteger > 0 );
                                      lOutput := ( FieldByName( 'RDB$PROCEDURE_OUTPUTS' ).AsInteger > 0 );
                                      if lInput then
                                         sInputParams := GetParamList( ppInput );
                                      if lOutput then
                                         sOutputParams := GetParamList( ppOutput );
                                      if ( lInput and lOutput ) then
                                         sSource := Format( K_SQL_INPUT_OUTPUT, [ sProcedure, sInputParams, sOutputParams ] )
                                      else
                                          if lInput then
                                             sSource := Format( K_SQL_INPUT, [ sProcedure, sInputParams ] )
                                          else
                                              if lOutput then
                                                 sSource := Format( K_SQL_OUTPUT, [ sProcedure, sOutputParams ] )
                                              else
                                                  sSource := Format( K_SQL_NONE, [ sProcedure ] );
                                      sSource := sSource + LF + Trim( FieldByName( 'RDB$PROCEDURE_SOURCE' ).AsString );
                                      Next;
                                 end;
                                 if strLleno( sSource ) then
                                 begin
                                      if not ( ComparaStrings( sOriginal, sSource ) ) then
                                      begin
                                           with fListaErrores do
                                           begin
                                                Add( '( Patch ) --> ' + sName );
                                                Add( '( DataBase ) --> ' + sProcedure );
                                                Add( VACIO );
                                                FContador := Fcontador + 1;
                                           end;
                                           Result := False;
                                      end;
                                 end;
                                 sName := VACIO;
                                 sProcedure := VACIO;
                                 sSource := VACIO;
                                 Active := False;
                            end;
                       end;
                       Next;
                  end;
                  EnableControls;
             end;
             if Not Result then
             begin
                  with fListaErrores do
                  begin
                       Add( 'Registros Totales: ' + IntToStr(  FContador ) );
                       fListaErrores.SaveToFile( sLog );
                  end;
             end
             else
                 BorraLog( sLog );
          finally
                 FreeAndNil( fListaErrores );
                 DespreparaQuery( tqImport );
                 dbTress.Connected := False;
          end;
     end;
end;

{ *********** Microsoft MS SQL Scripts ************ }

procedure TdmDBStruct.SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean );
var
   iValue: Integer;
begin
     if DisplayText then
     begin
          iValue := Sender.AsInteger;
          if ( iValue = 0 ) then
             Text := 'Comparte'
          else
              Text := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, iValue - 1 );
     end;
end;


function TdmDBStruct.MSSQLScriptsRefrescar: Boolean;
begin
     Result := False;
     try
        with tqScripts do
        begin
             Active := False;
             Active := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                DBStructDesconectar;
                Application.HandleException( Error );
           end;
     end;
end;

function TdmDBStruct.MSSQLScriptsInit(const sDatabase, sUsuario, sPassword: String): Boolean;
begin
     Result := DBStructConectar( sDatabase, sUsuario, sPassword ) and MSSQLScriptsRefrescar;
end;

function TdmDBStruct.MSSQLScriptGetKeys: TScriptKeys;
begin
     with tqScripts do
     begin
          with Result do
          begin
               Tipo := eTipoCompany( FieldByName( 'SE_TIPO' ).AsInteger );
               Secuencia := FieldByName( 'SE_SEQ_NUM' ).AsInteger;
          end;
     end;
end;

function TdmDBStruct.MSSQLScriptPosicionar: Boolean;
var
   Llave: TScriptKeys;
begin
     Result := False;
     try
        Llave := MSSQLScriptGetKeys;
        with tqScript do
        begin
             Active := False;
             ParamByName( 'SE_TIPO' ).AsInteger := Ord( Llave.Tipo );
             ParamByName( 'SE_SEQ_NUM' ).AsInteger := Llave.Secuencia;
             Active := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function TdmDBStruct.MSSQLScriptBorrar: Boolean;
begin
     Result := False;
     try
        if MSSQLScriptPosicionar then
        begin
             Borrar( [ tqScript ] );
             MSSQLScriptCancelarCambios;
             MSSQLScriptsRefrescar;
             Result := True;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function TdmDBStruct.MSSQLScriptEscribirCambios: Boolean;
begin
     try
        {
        GuardaLlave;
        }
        Grabar( [ tqScript ] );
        MSSQLScriptCancelarCambios;
        MSSQLScriptsRefrescar;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmDBStruct.MSSQLScriptCancelarCambios;
begin
     CancelaCambio( tqScript );
end;

procedure TdmDBStruct.MSSQLScriptEditar( const lInsert: Boolean );
begin
     MSSQLScriptPosicionar;
     with tqScript do
     begin
          if lInsert then
             Insert
          else
              Edit;
     end;
end;

procedure TdmDBStruct.ImportAllScripts;
begin
     with dbStruct do
     begin
          StartTransaction;
          try
             with tqScriptImport do
             begin
                  with SQL do
                  begin
                       Clear;
                       Add( 'delete from METADATA_MSSQL' );
                  end;
                  ExecSQL;
                  with SQL do
                  begin
                       Clear;
                       Add( 'insert into METADATA_MSSQL( SE_TIPO, SE_SEQ_NUM, SE_NOMBRE, SE_DESCRIP, SE_DATA, SE_ARCHIVO )' );
                       Add( 'values ( :SE_TIPO, :SE_SEQ_NUM, :SE_NOMBRE, :SE_DESCRIP, :SE_DATA, :SE_ARCHIVO )' );
                  end;
                  Prepare;
             end;
             with ttScripts do
             begin
                  First;
                  while not Eof do
                  begin
                       tqScriptImport.ParamByName( 'SE_TIPO' ).AsInteger := FieldByName( 'SE_TIPO' ).AsInteger;
                       tqScriptImport.ParamByName( 'SE_SEQ_NUM' ).AsInteger := FieldByName( 'SE_SEQ_NUM' ).AsInteger;
                       tqScriptImport.ParamByName( 'SE_NOMBRE' ).AsString := FieldByName( 'SE_NOMBRE' ).AsString;
                       tqScriptImport.ParamByName( 'SE_DESCRIP' ).AsString := FieldByName( 'SE_DESCRIP' ).AsString;
                       tqScriptImport.ParamByName( 'SE_DATA' ).AsBlob := TBlobData( FieldByName( 'SE_DATA' ).AsString );
                       tqScriptImport.ParamByName( 'SE_ARCHIVO' ).AsString := '';
                       tqScriptImport.ExecSQL;
                       Next;
                  end;
             end;
             Commit;
          except
                on Error: Exception do
                begin
                     Rollback;
                     Application.HandleException( Error );
                end;
          end;
     end;
     MSSQLScriptsRefrescar;
end;

procedure TdmDBStruct.tqScriptsAfterOpen(DataSet: TDataSet);
begin
     with tqScripts do
     begin
          FieldByName( 'SE_TIPO' ).OnGetText := SE_TIPOGetText;
     end;
end;

function TdmDBStruct.MSSQLScriptExportarParadox( const sArchivo: String ): Boolean;
const
     Q_SCRIPTS_EXPORT = 'select SE_TIPO, SE_SEQ_NUM, SE_NOMBRE, SE_DESCRIP, SE_DATA from METADATA_MSSQL order by SE_TIPO, SE_SEQ_NUM';
begin
     Result := False;
     try
        with tqExport do
        begin
             PreparaQuery( tqExport, Q_SCRIPTS_EXPORT );
             Active := True;
        end;
        with taExport do
        begin
             Active := False;
             DatabaseName := ExtractFilePath( sArchivo );
             TableName := ExtractFileName( sArchivo );
        end;
        with BatchMove do
        begin
             Execute;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                with tqExport do
                begin
                     Active := False;
                end;
                Application.HandleException( Error );
           end;
     end;
end;

end.
