unit FSPConsulta;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Db, Grids, DBGrids, ComCtrls,
     FSPEdit,
     ZBaseDlgModal,
     ZetaDBGrid, DBCtrls;

type
  TSPConsulta = class(TZetaDlgModal)
    ZetaDBGrid: TZetaDBGrid;
    DataSource: TDataSource;
    StatusBar: TStatusBar;
    Panel1: TPanel;
    DBNavigator: TDBNavigator;
    Abrir: TBitBtn;
    Agregar: TSpeedButton;
    Borrar: TSpeedButton;
    Modificar: TSpeedButton;
    Exportar: TSpeedButton;
    SaveDialog: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ZetaDBGridDblClick(Sender: TObject);
    procedure AbrirClick(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
  private
    { Private declarations }
    FSPEdit: TSPEdit;
    function Dataset: TDataset;
    function Editar( const lInsert: Boolean ): Boolean;
    procedure SetControls;
    procedure DisplayHint(Sender: TObject);
    procedure ShowErrorInfo(Sender: TObject; Error: Exception);
  public
    { Public declarations }
  end;

var
  SPConsulta: TSPConsulta;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     BDEFunctions,
     FHelpContext,
     FEscogerDB,
     DStructure;

{$R *.DFM}

{ TSPConsulta }

procedure TSPConsulta.FormCreate(Sender: TObject);
begin
     inherited;
     with Application do
     begin
          OnException := ShowErrorInfo;
          OnHint := DisplayHint;
     end;
     dmDBStruct := TdmDBStruct.Create( Self );
     HelpContext := H00027_Stored_Procedures_Adicionales;
end;

procedure TSPConsulta.FormShow(Sender: TObject);
begin
     inherited;
     Datasource.Dataset := Self.Dataset;
     SetControls;
     ActiveControl := Abrir;
end;

procedure TSPConsulta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     with dmDBStruct do
     begin
          try
             MSSQLScriptCancelarCambios;
             ModalResult := mrCancel;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

procedure TSPConsulta.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FSPEdit );
     FreeAndNil( dmDBStruct );
     inherited;
end;

procedure TSPConsulta.DisplayHint(Sender: TObject);
begin
     Statusbar.Panels[ 1 ].Text := GetLongHint( Application.Hint );
end;

procedure TSPConsulta.ShowErrorInfo( Sender: TObject; Error: Exception );
begin
     ShowMessage( GetExceptionInfo( Error ) );
end;

function TSPConsulta.Dataset: TDataset;
begin
     Result := dmDBStruct.tqScripts;
end;

procedure TSPConsulta.SetControls;
var
   lHay, lEdit: Boolean;
begin
     if ( Dataset <> nil ) then
     begin
          with Dataset do
          begin
               lHay := Active and not IsEmpty;
               lEdit := Active and ( State in [ dsEdit, dsInsert ] );
          end;
     end
     else
     begin
          lHay := False;
          lEdit := False;
     end;
     Agregar.Enabled := not lEdit and dmDBStruct.Conectada;
     Borrar.Enabled := lHay and not lEdit;
     Modificar.Enabled := lHay and not lEdit;
     Exportar.Enabled := lHay and not lEdit and dmDBStruct.Conectada;
end;

function TSPConsulta.Editar( const lInsert: Boolean ): Boolean;
begin
     if not Assigned( SPEdit ) then
        FSPEdit := TSPEdit.Create( Self );
     with FSPEdit do
     begin
          Editar( lInsert );
          ShowModal;
          Result := ( ModalResult = mrOk );
     end;
end;

procedure TSPConsulta.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     SetControls;
end;

procedure TSPConsulta.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TSPConsulta.ZetaDBGridDblClick(Sender: TObject);
begin
     inherited;
     Modificar.Click;
end;

procedure TSPConsulta.AbrirClick(Sender: TObject);
var
   oCursor: TCursor;
   sDatabase, sUsuario, sPassword: String;
begin
     inherited;
     if FEscogerDB.EscogerBaseDeDatos( sDatabase, sUsuario, sPassword ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with dmDBStruct do
             begin
                  MSSQLScriptsInit( sDatabase, sUsuario, sPassword );
             end;
             SetControls;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TSPConsulta.AgregarClick(Sender: TObject);
begin
     inherited;
     Editar( True );
end;

procedure TSPConsulta.BorrarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Este Script ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmDBStruct do
             begin
                  MSSQLScriptBorrar;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TSPConsulta.ModificarClick(Sender: TObject);
begin
     inherited;
     Editar( False );
end;

procedure TSPConsulta.ExportarClick(Sender: TObject);
var
   oCursor: TCursor;
   sArchivo: String;
begin
     inherited;
     with SaveDialog do
     begin
          FileName := 'D:\3win_20_Vsn_2013\Datos\Patch File\DB_MSSQL.DB';
          if Execute then
          begin
               sArchivo := FileName;
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourGlass;
               try
                  if dmDBStruct.MSSQLScriptExportarParadox( sArchivo ) then
                     ZetaDialogo.zInformation( '� Exito !', Format( 'El Archivo De Scripts de MS SQL' + CR_LF + '%s' + CR_LF + 'Fu� Creado', [ sArchivo ] ), 0 )
                  else
                      ZetaDialogo.zError( '� Error !', Format( 'El Archivo %s No Pudo Ser Creado', [ sArchivo ] ), 0 );
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end;
end;

end.
