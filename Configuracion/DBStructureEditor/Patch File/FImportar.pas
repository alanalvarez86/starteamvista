unit FImportar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables;

type
  TImportForm = class(TForm)
    OK: TBitBtn;
    Cancelar: TBitBtn;
    AliasLBL: TLabel;
    UsuarioLBL: TLabel;
    PasswordLBL: TLabel;
    Alias: TComboBox;
    Usuario: TEdit;
    Password: TEdit;
    VersionLBL: TLabel;
    Version: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetAlias: String;
    function GetUsuario: String;
    function GetPassword: String;
    function GetVersion: String;
  end;

var
  ImportForm: TImportForm;

function ChooseImportAlias( var sAlias, sUsuario, sPassword, sVersion: String ): Boolean;

implementation

uses DStructure;

{$R *.DFM}

function ChooseImportAlias( var sAlias, sUsuario, sPassword, sVersion: String ): Boolean;
begin
     if not Assigned( ImportForm ) then
        ImportForm := TImportForm.Create( Application );
     with ImportForm do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               sAlias := GetAlias;
               sUsuario := GetUsuario;
               sPassword := GetPassword;
               sVersion := GetVersion;
               Result := True;
          end
          else
              Result := False;
     end;
end;

procedure TImportForm.FormCreate(Sender: TObject);
var
   OldConfigMode: TConfigMode;
begin
     with Alias do
     begin
          with Session do
          begin
               OldConfigMode := ConfigMode;
               ConfigMode := [cfmVirtual, cfmPersistent];
               if not Active then
                  Active := True;
               NetFileDir := ExtractFilePath( Application.ExeName );
               GetAliasNames( Items );
               ConfigMode := OldConfigMode;
          end;
          ItemIndex := 0;
     end;
     with Version do
     begin
          DStructure.FillVersionList( Items );
          ItemIndex := 0;
     end;
end;

procedure TImportForm.FormShow(Sender: TObject);
begin
     ActiveControl := Alias;
end;

function TImportForm.GetAlias: String;
begin
     with Alias do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

function TImportForm.GetUsuario: String;
begin
     Result := Usuario.Text;
end;

function TImportForm.GetPassword: String;
begin
     Result := Password.Text;
end;

function TImportForm.GetVersion: String;
begin
     with Version do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

end.
