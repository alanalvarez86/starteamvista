unit FStructure;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, Db,
     Mask, DBTables, DBCtrls, Grids, DBGrids, FileCtrl, Menus, ShellApi,
     DStructure,
     ZetaCommonLists,
     ZetaKeyCombo,
     ZetaDBTextBox;

type
  TDBStruct = class(TForm)
    PanelControles: TPanel;
    PanelBotones: TPanel;
    DBNavigator: TDBNavigator;
    Agregar: TSpeedButton;
    Borrar: TSpeedButton;
    Modificar: TSpeedButton;
    Buscar: TSpeedButton;
    Exportar: TSpeedButton;
    MainMenu: TMainMenu;
    Archivo: TMenuItem;
    Editar: TMenuItem;
    EditarAgregar: TMenuItem;
    EditarBorrar: TMenuItem;
    EditarModificar: TMenuItem;
    N1: TMenuItem;
    EditarBuscar: TMenuItem;
    ArchivoAbrir: TMenuItem;
    ArchivoExportar: TMenuItem;
    N2: TMenuItem;
    ArchivoSalir: TMenuItem;
    Abrir: TBitBtn;
    StatusBar: TStatusBar;
    InterbaseDB: TMenuItem;
    InterbaseImportarTriggers: TMenuItem;
    InterbaseImportarProcedures: TMenuItem;
    dsSP: TDataSource;
    PageControl: TPageControl;
    Interbase: TTabSheet;
    SP_INTRBASE: TDBMemo;
    MSSQL: TTabSheet;
    SP_ORACLE: TDBMemo;
    PanelGrid: TPanel;
    DBGrid: TDBGrid;
    Paradox: TMenuItem;
    ParadoxExportar: TMenuItem;
    ParadoxImportar: TMenuItem;
    OpenDialog: TOpenDialog;
    SQLServer: TMenuItem;
    SQLServerImportarTriggers: TMenuItem;
    SQLServerImportarProcedures: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    ParadoxInit: TMenuItem;
    N5: TMenuItem;
    CompararMSSQL: TMenuItem;
    N6: TMenuItem;
    ComparaTriggersIB: TMenuItem;
    CompararSPIB: TMenuItem;
    Splitter1: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ArchivoAbrirClick(Sender: TObject);
    procedure ArchivoSalirClick(Sender: TObject);
    procedure EditarAgregarClick(Sender: TObject);
    procedure EditarBorrarClick(Sender: TObject);
    procedure EditarModificarClick(Sender: TObject);
    procedure EditarBuscarClick(Sender: TObject);
    procedure InterbaseImportarTriggersClick(Sender: TObject);
    procedure InterbaseImportarProceduresClick(Sender: TObject);
    procedure ParadoxImportarClick(Sender: TObject);
    procedure ParadoxExportarClick(Sender: TObject);
    procedure SQLServerImportarProceduresClick(Sender: TObject);
    procedure SQLServerImportarTriggersClick(Sender: TObject);
    procedure ParadoxInitClick(Sender: TObject);
    procedure CompararMSSQLClick(Sender: TObject);
    procedure ComparaTriggersIBClick(Sender: TObject);
    procedure CompararSPIBClick(Sender: TObject);
  private
    { Private declarations }
    procedure DisplayHint( Sender: TObject );
    procedure Edit( const lAgregar: Boolean );
    procedure SetControls( const lEnabled: Boolean );
    procedure ShowErrorInfo( Sender: TObject; Error: Exception );
    procedure ShowDiferencias(const sLog: String);
  public
    { Public declarations }
  end;

var
  DBStruct: TDBStruct;

implementation

uses BDEFunctions,
     ZetaDialogo,
     FEscogerAlias,
     FImportar,
     FVersionPatch,
     FVersion,
     FStructureSeek,
     FStructureEdit;

{$R *.DFM}

function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;

procedure CallNotePad( const sFileName: String );
begin
     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ) );
end;

{ ********* TDBStruct *********** }

procedure TDBStruct.FormCreate(Sender: TObject);
begin
     with Application do
     begin
          OnException := ShowErrorInfo;
          OnHint := DisplayHint;
     end;
     dmDBStruct := TdmDBStruct.Create( Self );
     SetControls( False );
end;

procedure TDBStruct.FormDestroy(Sender: TObject);
begin
     dmDBStruct.Free;
end;

procedure TDBStruct.DisplayHint(Sender: TObject);
begin
     Statusbar.Panels[ 1 ].Text := GetLongHint( Application.Hint );
end;

procedure TDBStruct.ShowErrorInfo( Sender: TObject; Error: Exception );
begin
     ShowMessage( GetExceptionInfo( Error ) );
end;

procedure TDBStruct.SetControls( const lEnabled: Boolean );
begin
     PanelControles.Enabled := lEnabled;
     PageControl.Enabled := lEnabled;
     SP_INTRBASE.Enabled := lEnabled;
     SP_ORACLE.Enabled := lEnabled;
     Agregar.Enabled := lEnabled;
     EditarAgregar.Enabled := lEnabled;
     InterbaseImportarTriggers.Enabled := lEnabled;
     InterbaseImportarProcedures.Enabled := lEnabled;
     SQLServerImportarTriggers.Enabled := lEnabled;
     SQLServerImportarProcedures.Enabled := lEnabled;
     ParadoxImportar.Enabled := lEnabled;
     with dmDBStruct do
     begin
          Borrar.Enabled := lEnabled and HayDatos;
          EditarBorrar.Enabled := lEnabled and HayDatos;
          Modificar.Enabled := lEnabled and HayDatos;
          EditarModificar.Enabled := lEnabled and HayDatos;
          Buscar.Enabled := lEnabled and HayDatos;
          EditarBuscar.Enabled := lEnabled and HayDatos;
          Exportar.Enabled := lEnabled and HayDatos;
          ArchivoExportar.Enabled := lEnabled and HayDatos;
          ParadoxExportar.Enabled := lEnabled and HayDatos;
          ParadoxInit.Enabled := lEnabled; // and HayDatos;
          case DBServer of
               dbSQLServer: PageControl.ActivePage := MSSQL;
          else
              PageControl.ActivePage := Interbase;
          end;
          {
          Interbase.TabVisible := ( PageControl.ActivePage = Interbase );
          MSSQL.TabVisible := ( PageControl.ActivePage = MSSQL );
          }
          Statusbar.Panels[ 0 ].Text := Format( '%s : Patch %s', [ DStructure.GetDBType( DBType ), PatchVersion ] );
     end;
end;

procedure TDBStruct.Edit( const lAgregar: Boolean );
begin
     if not Assigned( DBStructEdit ) then
        DBStructEdit := TDBStructEdit.Create( Self );
     with DBStructEdit do
     begin
          try
             Editar( lAgregar );
             ShowModal;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

procedure TDBStruct.ArchivoAbrirClick(Sender: TObject);
var
   oCursor: TCursor;
   eType: eDbType;
   sDatabase, sUsuario, sPassword, sVersion: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmDBStruct do
        begin
             if FEscogerAlias.EscogeUnAlias( sDatabase, sUsuario, sPassword, sVersion, eType ) then
             begin
                  DBType := eType;
                  PatchVersion := sVersion;
                  SetControls( Conectar( sDatabase, sUsuario, sPassword, sVersion ) );
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TDBStruct.ArchivoSalirClick(Sender: TObject);
begin
     Close;
end;

procedure TDBStruct.EditarAgregarClick(Sender: TObject);
begin
     Edit( True );
end;

procedure TDBStruct.EditarBorrarClick(Sender: TObject);
begin
     if ( Application.MessageBox( '� Desea Borrar Este Registro ?', '� Atenci�n !', MB_OKCANCEL ) = IdOK ) then
     begin
          with dmDBStruct do
          begin
               BorrarRegistro;
          end;
     end;
end;

procedure TDBStruct.EditarModificarClick(Sender: TObject);
begin
     Edit( False );
end;

procedure TDBStruct.EditarBuscarClick(Sender: TObject);
begin
     if not Assigned( DBStructSeek ) then
        DBStructSeek := TDBStructSeek.Create( Self );
     with DBStructSeek do
     begin
          ShowModal;
     end;
end;

procedure TDBStruct.InterbaseImportarTriggersClick(Sender: TObject);
var
   oCursor: TCursor;
   sAlias, sUsuario, sPassword, sVersion: String;
begin
     if FImportar.ChooseImportAlias( sAlias, sUsuario, sPassword, sVersion ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with dmDBStruct do
             begin
                  ImportInterbaseTriggers( sAlias, sUsuario, sPassword, sVersion );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TDBStruct.InterbaseImportarProceduresClick(Sender: TObject);
var
   oCursor: TCursor;
   sAlias, sUsuario, sPassword, sVersion: String;
begin
     if FImportar.ChooseImportAlias( sAlias, sUsuario, sPassword, sVersion ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with dmDBStruct do
             begin
                  ImportInterbaseProcedures( sAlias, sUsuario, sPassword, sVersion );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TDBStruct.SQLServerImportarTriggersClick(Sender: TObject);
var
   oCursor: TCursor;
   sAlias, sUsuario, sPassword, sVersion: String;
begin
     if FImportar.ChooseImportAlias( sAlias, sUsuario, sPassword, sVersion ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with dmDBStruct do
             begin
                  ImportMSSQLTriggers( sAlias, sUsuario, sPassword, sVersion );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TDBStruct.SQLServerImportarProceduresClick( Sender: TObject);
var
   oCursor: TCursor;
   sAlias, sUsuario, sPassword, sVersion: String;
begin
     if FImportar.ChooseImportAlias( sAlias, sUsuario, sPassword, sVersion ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with dmDBStruct do
             begin
                  ImportMSSQLProcedures( sAlias, sUsuario, sPassword, sVersion );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TDBStruct.ParadoxImportarClick(Sender: TObject);
var
   oCursor: TCursor;
   sFileName: String;
begin
     with OpenDialog do
     begin
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               sFileName := FileName;
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourGlass;
               try
                  with dmDBStruct do
                  begin
                       ImportarParadox( sFileName );
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end;
end;

procedure TDBStruct.ParadoxExportarClick(Sender: TObject);
var
   oCursor: TCursor;
   sVersion, sArchivo: String;
begin
     if FVersionPatch.ChooseVersion( sVersion ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             sArchivo := dmDBStruct.GetExportFile;
             if dmDBStruct.ExportarParadox( sVersion, sArchivo ) then
                ZetaDialogo.zInformation( '� Exito !', Format( 'El Archivo De Patch Para La Versi�n %s' + CR_LF + '%s' + CR_LF + 'Fu� Creado', [ sVersion, sArchivo ] ), 0 )
             else
                 ZetaDialogo.zError( '� Error !', Format( 'El Archivo %s No Pudo Ser Creado', [ sArchivo ] ), 0 );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TDBStruct.ParadoxInitClick(Sender: TObject);
var
   oCursor: TCursor;
   sOriginal, sNueva: String;
begin
     if FVersion.ChooseVersion( sOriginal, sNueva ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          try
             with dmDBStruct do
             begin
                  InitVersion( sOriginal, sNueva );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TDBStruct.ShowDiferencias( const sLog: String );
begin
     if ZetaDialogo.ZConfirm( '� Hay Diferencias !', 'Las Diferencias Encontradas Se Detallan' + CR_LF + 'En El Archivo: ' + sLog + CR_LF + '� Desea Verlo ?', 0, mbOK ) then
     begin
          CallNotePad( sLog );
     end;
end;

procedure TDBStruct.CompararMSSQLClick(Sender: TObject);
var
   sAlias, sUsuario, sPassword, sVersion, sLog: String;
begin
     if FImportar.ChooseImportAlias( sAlias, sUsuario, sPassword, sVersion ) then
     begin
          if dmDBStruct.CompararMSSQL( sAlias, sUsuario, sPassword, sVersion, sLog ) then
             ZetaDialogo.ZInformation( 'DBStructure', 'Todos Los Archivos Est�n Iguales', 0 )
          else
              ShowDiferencias( sLog );
     end;
end;

procedure TDBStruct.ComparaTriggersIBClick(Sender: TObject);
var
   sAlias, sUsuario, sPassword, sVersion, sLog: String;
begin
     if FImportar.ChooseImportAlias( sAlias, sUsuario, sPassword, sVersion ) then
     begin
          if dmDBStruct.CompararTriggersIB( sAlias, sUsuario, sPassword, sVersion, sLog ) then
             ZetaDialogo.ZInformation( 'DBStructure', 'Todos Los Archivos Est�n Iguales', 0 )
          else
              ShowDiferencias( sLog );
     end;
end;

procedure TDBStruct.CompararSPIBClick(Sender: TObject);
var
   sAlias, sUsuario, sPassword, sVersion, sLog: String;
begin
     if FImportar.ChooseImportAlias( sAlias, sUsuario, sPassword, sVersion ) then
     begin
          if dmDBStruct.CompararSPIB( sAlias, sUsuario, sPassword, sVersion, sLog ) then
             ZetaDialogo.ZInformation( 'DBStructure', 'Todos Los Archivos Est�n Iguales', 0 )
          else
              ShowDiferencias( sLog );
     end;
end;

end.
