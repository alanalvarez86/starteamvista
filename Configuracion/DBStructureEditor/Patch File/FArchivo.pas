unit FArchivo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables, ExtCtrls;

type
  TArchivoForm = class(TForm)
    OK: TBitBtn;
    Cancelar: TBitBtn;
    ArchivoRG: TRadioGroup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Archivo: String;
  end;

var
  ArchivoForm: TArchivoForm;

function EscogerArchivo( var sArchivo: String ): Boolean;

implementation

uses DStructure;

{$R *.DFM}

function EscogerArchivo( var sArchivo: String ): Boolean;
begin
     if not Assigned( ArchivoForm ) then
        ArchivoForm := TArchivoForm.Create( Application );
     with ArchivoForm do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               sArchivo := Archivo;
               Result := True;
          end
          else
              Result := False;
     end;
end;

{ TArchivoForm }

procedure TArchivoForm.FormCreate(Sender: TObject);
begin
     with ArchivoRG do
     begin
          with Items do
          begin
               Add( 'Interbase - Comparte' );
               Add( 'Interbase - Datos' );
               Add( 'SQL Server - Comparte' );
               Add( 'SQL Server - Datos' );
          end;
          ItemIndex := 0;
     end;
end;

function TArchivoForm.Archivo: String;
const
     K_ARCHIVO = 'D:\3win_20_Vsn_2013\Datos\Patch File\%s.db';
begin
     case ArchivoRG.ItemIndex of
          0: Result := Format( K_ARCHIVO, [ 'SP_Comparte' ] );
          1: Result := Format( K_ARCHIVO, [ 'SP_Datos' ] );
          2: Result := Format( K_ARCHIVO, [ 'SP_MSSQL_Comparte' ] );
          3: Result := Format( K_ARCHIVO, [ 'SP_MSSQL_Datos' ] );
     else
         Result := '';
     end;
end;

end.
