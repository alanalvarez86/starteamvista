�
 TDBSTRUCTEDIT 0�  TPF0TDBStructEditDBStructEditLeft� ToplWidthHeightwCaption-Crear Archivos de Estructura de Base de DatosColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPageControlPageControlLeft TopaWidth�Height� 
ActivePageOracleAlignalClientTabOrder 	TTabSheet	InterbaseCaption
&Interbase TDBMemoSP_INTRBASELeft Top WidthMHeight� AlignalClient	DataFieldSP_INTRBASE
DataSourcedsSPFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFont
ScrollBarsssBothTabOrder   TPanelPanelIBLeftMTop Width#Height� AlignalRightTabOrder TSpeedButtonIBLoadFromFileLeftTopWidthHeightHintLeer Archivo
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333333?����333     333wwwww333����3333337333����333?���333 � �33�w7w7303����37�??��39 ��37sw7739�����?�w?���	�� �  wwww7ww	������wwww�7�s	�����3wwwws7�3	�����3wwww��s3	��   33wwwwww3339�3333337w33333393333337s333333033333337333333	NumGlyphsParentShowHintShowHint	OnClickIBLoadFromFileClick  TSpeedButtonIBBorrarTodoLeftTop+WidthHeightHintBorrar Todo El SQL
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUU_���_U��wwwuuY�WwwwuuWw�UUUUUP0UUUUU�W��UUUP[�UUUUWu�w�UUU�  UUUUuWww�UUP���UUUWUuWW�UU���UUUUUuW�UU��� UUUUWUw�UP����UUWUUW��U�� �UUuu�Uw��P�࿰��UW�W_WW�� ���Uw�u����  ��  �Uww�Www��  UP�UwwuUWWUP  UUUUWwwUUUUU UUUUUwuUUUuU	NumGlyphsParentShowHintShowHint	OnClickIBBorrarTodoClick    	TTabSheetOracleCaption&MS SQL TDBMemo	SP_ORACLELeft Top Width�Height� AlignalClient	DataField	SP_ORACLE
DataSourcedsSPFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFont
ScrollBarsssBothTabOrder   TPanelPanelORALeft�Top Width#Height� AlignalRightTabOrder TSpeedButtonMSSQLLoadFromFileLeftTopWidthHeightHintLeer Archivo
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333333?����333     333wwwww333����3333337333����333?���333 � �33�w7w7303����37�??��39 ��37sw7739�����?�w?���	�� �  wwww7ww	������wwww�7�s	�����3wwwws7�3	�����3wwww��s3	��   33wwwwww3339�3333337w33333393333337s333333033333337333333	NumGlyphsParentShowHintShowHint	OnClickMSSQLLoadFromFileClick  TSpeedButtonMSSQLBorrarTodoLeftTop+WidthHeightHintBorrar Todo El SQL
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUU_���_U��wwwuuY�WwwwuuWw�UUUUUP0UUUUU�W��UUUP[�UUUUWu�w�UUU�  UUUUuWww�UUP���UUUWUuWW�UU���UUUUUuW�UU��� UUUUWUw�UP����UUWUUW��U�� �UUuu�Uw��P�࿰��UW�W_WW�� ���Uw�u����  ��  �Uww�Www��  UP�UwwuUWWUP  UUUUWwwUUUUU UUUUUwuUUUuU	NumGlyphsParentShowHintShowHint	OnClickMSSQLBorrarTodoClick     TPanelPanelControlesLeft Top Width�HeightaAlignalTop
BevelOuterbvNoneTabOrder  TLabel
SP_NAMElblLeftTopWidth(Height	AlignmenttaRightJustifyCaptionNombre:  TLabelSP_SEQ_NUMlblLeftTopJWidth3Height	AlignmenttaRightJustifyCaption	Secuencia  TLabelSP_VERSIONlblLeftTopWidth&Height	AlignmenttaRightJustifyCaptionVersi�n:  TLabel
SP_TIPOlblLeft!Top5WidthHeight	AlignmenttaRightJustifyCaptionTipo:  TDBEditSP_NAMELeft>TopWidth+Height	DataFieldSP_NAME
DataSourcedsSPTabOrder  TDBComboBox
SP_VERSIONLeft>TopWidth� Height	DataField
SP_VERSION
DataSourcedsSP
ItemHeightTabOrder   TZetaDBKeyComboSP_TIPOLeft>Top1Width� HeightStylecsDropDownList
ItemHeightTabOrder	ListaFija	lfNingunaListaVariablelvPuestoOffset Opcional	DataFieldSP_TIPO
DataSourcedsSPLlaveNumerica	  TZetaDBNumero
SP_SEQ_NUMLeft>TopHWidth+HeightMascaramnDiasTabOrderText0	DataField
SP_SEQ_NUM
DataSourcedsSP   TPanelPanelBotonesLeft Top3Width�Height"AlignalBottomTabOrder TBitBtnCancelarLeft�TopWidthKHeightAnchorsakTopakRight Caption	&CancelarTabOrder KindbkCancel  TBitBtnOKLeft\TopWidthKHeightAnchorsakTopakRight Caption&OKDefault	TabOrderOnClickOKClick
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs   TOpenDialog
OpenDialog
DefaultExtsqlFilterUArchivos Texto ( *.txt )|*.txt|Scripts de SQL ( *.sql )|*.sql|Todos los Archivos |*.*FilterIndexTitleEscoja El Archivo DeseadoLeft� Top  TDataSourcedsSPDataSetdmDBStruct.tqEdSpLeftTop   