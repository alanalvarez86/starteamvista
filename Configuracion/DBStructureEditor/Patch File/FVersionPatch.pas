unit FVersionPatch;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables;

type
  TVersionPatch = class(TForm)
    OK: TBitBtn;
    Cancelar: TBitBtn;
    VersionNumberLBL: TLabel;
    VersionNumber: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function Version: String;
  end;

var
  VersionPatch: TVersionPatch;

function ChooseVersion( var sVersion: String ): Boolean;

implementation

uses DStructure;

{$R *.DFM}

function ChooseVersion( var sVersion: String ): Boolean;
begin
     if not Assigned( VersionPatch ) then
        VersionPatch := TVersionPatch.Create( Application );
     with VersionPatch do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               sVersion := Version;
               Result := True;
          end
          else
              Result := False;
     end;
end;

procedure TVersionPatch.FormCreate(Sender: TObject);
begin
     with VersionNumber do
     begin
          DStructure.FillVersionList( Items );
          ItemIndex := 0;
     end;
end;

procedure TVersionPatch.FormShow(Sender: TObject);
begin
     ActiveControl := VersionNumber;
end;

function TVersionPatch.Version: String;
begin
     with VersionNumber do
     begin
          if ( ItemIndex < 0 ) then
             Result := Text
          else
              Result := Items.Strings[ ItemIndex ];
     end;
end;

procedure TVersionPatch.OKClick(Sender: TObject);
begin
     if ( Version = '' ) then
     begin
          Beep;
          ActiveControl := VersionNumber;
     end
     else
         ModalResult := mrOk;
end;

end.
