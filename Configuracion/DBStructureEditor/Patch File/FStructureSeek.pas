unit FStructureSeek;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Grids, DBGrids,
     DStructure, Db, DBCtrls;

type
  TDBStructSeek = class(TForm)
    PanelBotones: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    PanelSuperior: TPanel;
    PistaLBL: TLabel;
    Pista: TEdit;
    DBGrid: TDBGrid;
    dsSpSeek: TDataSource;
    DBNavigator: TDBNavigator;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure PistaChange(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DBStructSeek: TDBStructSeek;

implementation

{$R *.DFM}

procedure TDBStructSeek.FormShow(Sender: TObject);
begin
     dmDBStruct.ConectarBusqueda;
     ActiveControl := Pista;
end;

procedure TDBStructSeek.PistaChange(Sender: TObject);
begin
     dmDBStruct.PosicionarBusqueda( Pista.Text );
end;

procedure TDBStructSeek.DBGridDblClick(Sender: TObject);
begin
     OK.Click;
end;

procedure TDBStructSeek.OKClick(Sender: TObject);
begin
     dmDBStruct.PosicionarResultado;
end;

end.
