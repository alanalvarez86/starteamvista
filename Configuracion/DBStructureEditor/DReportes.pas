unit DReportes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBTables;

type
  TdmReportes = class(TDataModule)
    ttSource: TTable;
    dbSourceShared: TDatabase;
    procedure SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ttSourceNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    FSourceSharedPath: String;
    procedure OpenDBSourceShared;
    procedure OpenDBase( Database: TDatabase; const sDirectory: String );
    procedure PreparaTabla( Tabla: TTable; const sTabla: String );
  public
    { Public declarations }
    property SourceSharedPath: String read FSourceSharedPath write FSourceSharedPath;
    function AbreArchivoSPExtras: Boolean;
    function GetSPFileName( const sExtension: String ): String;
  end;

var
  dmReportes: TdmReportes;

implementation

uses ZetaCommonTools,
     ZetaCommonLists;
     
{$R *.DFM}

procedure TdmReportes.OpenDBase( Database: TDatabase; const sDirectory: String );
begin
     if StrLleno( sDirectory ) then
     begin
          with Database do
          begin
               Connected := False;
               with Params do
               begin
                    Values[ 'PATH' ] := sDirectory;
               end;
               Connected := True;
          end;
     end;
end;

procedure TdmReportes.OpenDBSourceShared;
begin
     OpenDBase( dbSourceShared, FSourceSharedPath );
end;

procedure TdmReportes.PreparaTabla( Tabla: TTable; const sTabla: String );
begin
     with Tabla do
     begin
          Active := False;
          DatabaseName := dbSourceShared.DatabaseName;
          TableType := ttDefault;
          TableName := sTabla;
     end;
end;

function TdmReportes.GetSPFileName( const sExtension: String ): String;
begin
     Result := Format( '%sDB_MSSQL.%s', [ 'D:\3Win_13\Datos\Patch File\', sExtension ] );
end;

function TdmReportes.AbreArchivoSPExtras: Boolean;
var
   sFileName: String;
begin
     sFileName := GetSPFileName( 'DB' );
     SourceSharedPath := ExtractFilePath( sFileName );
     OpenDBSourceShared;
     if FileExists( sFileName ) then
        PreparaTabla( ttSource, ExtractFileName( sFileName ) )
     else
     begin
          with ttSource do
          begin
               Active := False;
               Filtered := FALSE;
               Filter := '';
               IndexFieldNames := '';
               while ( FieldCount > 0 ) do
               begin
                    Fields[ FieldCount - 1 ].DataSet := nil;
               end;
               DatabaseName := dbSourceShared.DatabaseName;
               TableType := ttParadox;
               TableName := ExtractFileName( sFileName );
               with FieldDefs do
               begin
                    Clear;
                    Add( 'SE_TIPO', ftInteger, 0, True );
                    Add( 'SE_SEQ_NUM', ftInteger, 0, True );
                    Add( 'SE_NOMBRE', ftString, 10, False );
                    Add( 'SE_DESCRIP', ftString, 50, False );
                    Add( 'SE_DATA', ftBlob, 1, False );
               end;
               CreateTable;
          end;
     end;
     with ttSource do
     begin
          Exclusive := True;
          OnNewRecord := ttSourceNewRecord;
          AddIndex( '', 'SE_TIPO;SE_SEQ_NUM', [ ixUnique, ixPrimary ] );
          Active := True;
          FieldByName( 'SE_TIPO' ).OnGetText := SE_TIPOGetText;
          Result := Active;
     end;
end;

procedure TdmReportes.ttSourceNewRecord(DataSet: TDataSet);
begin
     inherited;
     with Dataset do
     begin
          FieldByName( 'SE_TIPO' ).AsInteger := 0;
          FieldByName( 'SE_SEQ_NUM' ).AsInteger := 0;
     end;
end;

procedure TdmReportes.SE_TIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean );
var
   iValue: Integer;
begin
     if DisplayText then
     begin
          iValue := Sender.AsInteger;
          if ( iValue = 0 ) then
             Text := 'Comparte'
          else
              Text := ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, iValue - 1 );
     end;
end;

end.
