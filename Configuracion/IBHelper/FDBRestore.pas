unit FDBRestore;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ZetaNumero;

type
  TIBRestore = class(TForm)
    SaveDialog: TSaveDialog;
    Opciones: TGroupBox;
    PanelSuperior: TPanel;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    SeekArchivo: TSpeedButton;
    PanelBotones: TPanel;
    Salir: TBitBtn;
    Restaurar: TBitBtn;
    GenerarBatch: TBitBtn;
    Defaults: TBitBtn;
    ReplaceDatabase: TCheckBox;
    SetBuffers: TCheckBox;
    IndexesInactive: TCheckBox;
    NoValidityChecks: TCheckBox;
    VerboseOutput: TCheckBox;
    TargetGB: TGroupBox;
    TargetComputerNameLBL: TLabel;
    TargetUserNameLBL: TLabel;
    TargetPasswordLBL: TLabel;
    TargetLBL: TLabel;
    SeekTargetDB: TSpeedButton;
    TargetComputerName: TEdit;
    TargetUserName: TEdit;
    TargetPassword: TEdit;
    TargetDB: TEdit;
    OpenDialog: TOpenDialog;
    Buffers: TZetaNumero;
    PageSize: TZetaNumero;
    Label1: TLabel;
    CommitAfterEachTable: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure SeekArchivoClick(Sender: TObject);
    procedure GenerarBatchClick(Sender: TObject);
    procedure RestaurarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure DefaultsClick(Sender: TObject);
    procedure SeekTargetDBClick(Sender: TObject);
    procedure SetBuffersClick(Sender: TObject);
  private
    { Private declarations }
    FComputerName: String;
    FDatabaseFile: String;
    FPassword: String;
    FUserName: String;
    function CreateCommand: String;
    function GetLogFile: String;
    function GetParameters: String;
    function GetProgram: String;
  public
    { Public declarations }
    property ComputerName: String read FComputerName write FComputerName;
    property DatabaseFile: String read FDatabaseFile write FDatabaseFile;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
  end;

var
  IBRestore: TIBRestore;

implementation

uses ZetaDialogo,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     FDBRegistry,
     DInterbase;

{$R *.DFM}

procedure TIBRestore.FormShow(Sender: TObject);
begin
     Archivo.Text := ZetaCommonTools.StrTransform( DatabaseFile, ExtractFileExt( DatabaseFile ), '.gbk' );
     ReplaceDatabase.Checked := ZetaRegistry.ReplaceDatabase;
     SetBuffers.Checked := ZetaRegistry.SetDBBuffers;
     Buffers.Valor := ZetaRegistry.Buffers;
     IndexesInactive.Checked := ZetaRegistry.IndexesInactive;
     CommitAfterEachTable.Checked := ZetaRegistry.CommitAfterEachTable;
     PageSize.Valor := ZetaRegistry.PageSize;
     VerboseOutput.Checked := ZetaRegistry.VerboseOutput;
     TargetComputerName.Text := ZetaWinAPITools.GetComputerName;
     TargetUserName.Text := UserName;
     TargetPassword.Text := Password;
     TargetDB.Text := DatabaseFile;
     SetBuffersClick( Self );
end;

function TIBRestore.GetLogFile: String;
begin
     with Archivo do
     begin
          Result := ExtractFilePath( Text ) + 'Restore' + ExtractFileName( Text );
          Result := ZetaCommonTools.StrTransform( Result, ExtractFileExt( Result ), '.log' );
     end;
end;

function TIBRestore.GetProgram: String;
const
     K_COMMAND = '%sgbak.exe';
begin
     Result := Format( K_COMMAND, [ DInterbase.GetServerDirectory ] );
end;

function TIBRestore.GetParameters: String;
const
     K_OPTIONS = '%s -y "%s" -p %d -z -user "%s" -password "%s"';
     K_PARAMETERS = '%s "%s" "%s"';
begin
     if ReplaceDatabase.Checked then
        Result := '-R'
     else
         Result := '-C';
     Result := Format( K_OPTIONS, [ Result, GetLogFile, PageSize.ValorEntero, UserName, Password ] );
     if SetBuffers.Checked then
        Result := Result + Format( ' -buffers %d', [ Buffers.ValorEntero ] );
     if IndexesInactive.Checked then
        Result := Result + ' -i';
     if CommitAfterEachTable.Checked then
        Result := Result + ' -o';
     if VerboseOutput.Checked then
        Result := Result + ' -v';
     Result := Format( K_PARAMETERS, [ Result, Archivo.Text, TargetComputerName.Text + ':' + TargetDB.Text ] );
end;

function TIBRestore.CreateCommand: String;
const
     K_COMMAND = '"%s" %s';
begin
     Result := Format( K_COMMAND, [ GetProgram, GetParameters ] );
end;

procedure TIBRestore.SeekArchivoClick(Sender: TObject);
begin
     with Archivo do
     begin
          with OpenDialog do
          begin
               Filter := 'Respaldo Interbase( *.gbk )|*.gbk|Respaldo ( *.bak )|*.bak|Todos ( *.* )|*.*';
               FilterIndex := 0;
               Title := 'Seleccione El Archivo de Respaldo';
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TIBRestore.SeekTargetDBClick(Sender: TObject);
begin
     with TargetDB do
     begin
          with SaveDialog do
          begin
               Filter := 'Base De Datos Interbase( *.gdb )|*.gdb|Todos ( *.* )|*.*';
               FilterIndex := 0;
               Title := 'Seleccione La Base De Datos A Restaurar';
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TIBRestore.DefaultsClick(Sender: TObject);
begin
     ZetaRegistry.ReplaceDatabase := ReplaceDatabase.Checked;
     ZetaRegistry.SetDBBuffers := SetBuffers.Checked;
     ZetaRegistry.Buffers := Buffers.ValorEntero;
     ZetaRegistry.IndexesInactive := IndexesInactive.Checked;
     ZetaRegistry.CommitAfterEachTable := CommitAfterEachTable.Checked;
     ZetaRegistry.PageSize := PageSize.ValorEntero;
     ZetaRegistry.VerboseOutput := VerboseOutput.Checked;
end;

procedure TIBRestore.SetBuffersClick(Sender: TObject);
begin
     Buffers.Enabled := SetBuffers.Checked;
end;

procedure TIBRestore.GenerarBatchClick(Sender: TObject);
var
   FDatos: TStrings;
begin
     with SaveDialog do
     begin
          Filter := 'Archivo Batch( *.bat )|*.bat|Archivo Texto ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione El Archivo Batch A Crear';
          FileName := 'restore.bat';
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               FDatos := TStringList.Create;
               try
                  with FDatos do
                  begin
                       Add( Format( 'del "%s"', [ GetLogFile ] ) );
                       Add( CreateCommand );
                       SaveToFile( FileName );
                  end;
               finally
                      FDatos.Free;
               end;
               if ZetaDialogo.zConfirm( Caption, 'El Archivo BATCH Para Restaurar Esta Base De Datos Ha Sido Creado' +
                                                 CR_LF +
                                                 CR_LF +
                                                 FileName +
                                                 CR_LF +
                                                 CR_LF +
                                                 '� Desea Verlo ?', 0, mbYes ) then
               begin
                    DInterbase.CallNotePad( FileName );
               end;
          end;
     end;
end;

procedure TIBRestore.RestaurarClick(Sender: TObject);
var
   sLog: String;
   oCursor: TCursor;
   lOk, lViewLog: Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        sLog := GetLogFile;
        if FileExists( sLog ) then
           DeleteFile( sLog );
        lOk := DInterbase.Ejecutar( GetProgram, GetParameters, False );
        if FileExists( sLog ) then
        begin
             if lOk then
                lViewLog := ZetaDialogo.zConfirm( Caption,
                                                  'La Restauraci�n Ha Terminado' +
                                                  CR_LF +
                                                  CR_LF +
                                                  'La Bit�cora Ha Sido Creada En El Archivo' +
                                                  CR_LF +
                                                  CR_LF +
                                                  sLog +
                                                  CR_LF +
                                                  CR_LF +
                                                  '� Desea Verla ?', 0, mbYes )
             else
                 lViewLog := ZetaDialogo.zErrorConfirm( Caption,
                                                        'La Restauraci�n Termin� Con Error' +
                                                        CR_LF +
                                                        CR_LF +
                                                        'La Bit�cora Ha Sido Creada En El Archivo' +
                                                        CR_LF +
                                                        CR_LF +
                                                        sLog +
                                                        CR_LF +
                                                        CR_LF +
                                                        '� Desea Verla ?', 0, mbYes );
             if lViewLog then
                DInterbase.CallNotePad( sLog );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TIBRestore.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
