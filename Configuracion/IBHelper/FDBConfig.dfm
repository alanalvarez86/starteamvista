object IBConfig: TIBConfig
  Left = 192
  Top = 107
  ActiveControl = Buffers
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Configurar Base De Datos'
  ClientHeight = 137
  ClientWidth = 342
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BufferValueLBL: TLabel
    Left = 83
    Top = 12
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&Buffers:'
    FocusControl = Buffers
  end
  object SweepIntervalLBL: TLabel
    Left = 24
    Top = 59
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = '&Intervalo de Sweep:'
    FocusControl = SweepInterval
  end
  object BuffersTotal: TLabel
    Left = 208
    Top = 12
    Width = 57
    Height = 13
    Caption = 'BuffersTotal'
  end
  object PanelBotones: TPanel
    Left = 0
    Top = 104
    Width = 342
    Height = 33
    Align = alBottom
    TabOrder = 4
    object Salir: TBitBtn
      Left = 261
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Salir De Esta Ventana'
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = SalirClick
      Kind = bkClose
    end
    object Ejecutar: TBitBtn
      Left = 172
      Top = 4
      Width = 81
      Height = 25
      Hint = 'Configurar La Base De Datos Seg�n Los Valores Especificados'
      Anchors = [akTop, akRight]
      Caption = '&Configurar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = EjecutarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object GenerarBatch: TBitBtn
      Left = 5
      Top = 4
      Width = 110
      Height = 25
      Hint = 'Generar Archivo Batch Con El Comando De Configuraci�n'
      Caption = '&Generar Batch'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = GenerarBatchClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        888888880000880000788000780007880000801919070CDC003B307800008091
        9190CD0D03B0B30800008090009900C0D0080B080000809078490788C480B088
        0000809078090788050B08880000809078490788C0B088880000809000990070
        0B078078000080999990CD0D0B000B080000809999080CDC40BBB07800008800
        0788700077000888000088070888070870707088000088070808070807880088
        0000880700700707888870880000880707070708700008880000880770807708
        0788887800008807088807080777708800008800888880087000078800008888
        88888888888888880000}
    end
  end
  object Sweeping: TCheckBox
    Left = 24
    Top = 34
    Width = 109
    Height = 17
    Alignment = taLeftJustify
    Caption = 'S&weep Autom�tico:'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnClick = SweepingClick
  end
  object ForcedWrites: TCheckBox
    Left = 2
    Top = 80
    Width = 131
    Height = 17
    Alignment = taLeftJustify
    Caption = '&Escribir Directo A Disco:'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = ForcedWritesClick
  end
  object Buffers: TZetaNumero
    Left = 120
    Top = 8
    Width = 81
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 0
    OnChange = BuffersChange
  end
  object SweepInterval: TZetaNumero
    Left = 120
    Top = 55
    Width = 81
    Height = 21
    Mascara = mnEmpleado
    TabOrder = 2
    OnChange = SweepIntervalChange
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'gbk'
    Filter = 'Respaldo Interbase ( *.gbk )|*.gbk|Todos ( *.* )|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Seleccione El Archivo de Respaldo'
    Left = 16
  end
end
