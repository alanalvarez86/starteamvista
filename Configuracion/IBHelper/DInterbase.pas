unit DInterbase;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBTables, ShellAPI,
     ZetaAliasManager,
     FDBRegistry,
     IBHeader,
     IBDatabase,
     IBDatabaseInfo,
     IBSQL,
     IB_Components,
     IB_Dialogs;

{$define CHECA_NULOS}

type
  TServerType = ( stInterbase, stFirebird );
  TCallMeBack = function( const Mensaje: String; const Valor: Integer; var HayError: Boolean ): Boolean of Object;
  TTableCallBack  = procedure( const Mensaje: String ) of Object;
  TdmInterbase = class(TDataModule)
    IBSourceDB: TIB_Database;
    IBTargetDB: TIB_Database;
    tqQuery: TIB_Query;
    stTarget: TIB_DSQL;
    tqSource: TIB_Cursor;
    tqSource2: TIB_Cursor;
    tqTarget: TIB_Cursor;
    stSource: TIB_DSQL;
    IB_DSQL: TIB_DSQLDialog;
    IB_Script: TIB_ScriptDialog;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    FBitacora: TStrings;
    FAliasMgr: TZetaAliasManager;
    FEmpresas: TList;
    {$ifdef CHECA_NULOS}
    FCheckNulls: Boolean;
    {$endif}
    function GetEmpresas( Index: Integer ): TEmpresaData;
    function GetEmpresasCount: Integer;
    function ConnectDatabase(Database: TIB_Database; const sUserName, sPassword, sFileName: String): Boolean;
    function CopyOneTable(const sTableName: String; Callback: TCallMeback; var lContinue: Boolean ): Boolean;
    function DeleteTables(Callback: TCallMeBack; TableCallBack: TTableCallBack): Boolean;
    function DisconnectDatabase(Database: TIB_Database): Boolean;
    function GetDatabaseName( Database: TIB_Database ):String;
    function GetInsertScript(const sTableName: String; Source: TIB_Cursor): String;
    procedure AddLogMsg(const sMensaje: String);
    procedure AddLogException(const sMensaje: String; Error: Exception);
    procedure AddLogInsertException(const sMensaje: String; Error: Exception);
    procedure AddLogTimeStamp(const sMensaje: String);
    procedure AddLogVerification(const sMensaje: String);
    procedure AddSeparator;
    procedure AttachQuery(Query: TIB_Cursor; const sScript: String);
    procedure AttachStatement(Query: TIB_DSQL; const sScript: String);
    procedure ClearEmpresas;
    procedure CopyTables( Callback: TCallMeback; TableCallBack: TTableCallBack; const lVerificar: Boolean );
    procedure DeterminaDependencias(const sValor: String);
    procedure ExecuteStatement(Query: TIB_DSQL; const sScript: String);
    procedure ExecuteCommand(Query: TIB_DSQL);
    procedure ListTables;
    procedure ReactivaIndice(const sIndice: String);
    procedure VerifyOneTable( const sTableName: String; Callback: TCallMeBack; var lContinue: Boolean );
  public
    { Public declarations }
    property Empresas[ Index: Integer ]: TEmpresaData read GetEmpresas;
    property EmpresasCount: Integer read GetEmpresasCount;
    property Lista: TStrings read FLista;
    {$ifdef CHECA_NULOS}
    property ChecaNulls: Boolean read FCheckNulls write FCheckNulls;
    {$endif}
    function CheckWrongDatabaseStructure(var sMensaje: String): Boolean;
    function ConectarComparte: Boolean;
    function ConnectSource(const sUserName, sPassword, sFileName: String): Boolean;
    function ConnectTarget(const sUserName, sPassword, sFileName: String): Boolean;
    function DisconnectSource: Boolean;
    function DisconnectTarget: Boolean;
    function FindDatabaseBufferSize( const iValue: Integer ): Extended;
    function GetDatabaseBuffers: Integer;
    function GetDatabaseBufferSize: Extended;
    function GetDatabaseFile(const sAlias: String): String;
    function GetDatabaseForcedWrites: Boolean;
    function GetDatabaseSweepInterval: Integer;
    function GetDatabaseTableList( const sFileName: String ): Boolean;
    function RefrescaIndices( const iIndexType: Integer; var sError: String ): Boolean;
    procedure Copiar( Callback: TCallMeback; TableCallBack: TTableCallBack; Bitacora: TStrings; const lVerificar, lBorrar: Boolean );
    procedure GetDatabaseInfo(Texto: TStrings);
    procedure GetUserInfo(Texto: TStrings);
    procedure ExecuteScript(const iEmpresa: Integer);
    procedure InvokeDSQL( const iEmpresa: Integer );
    procedure ReactivaTodos;
    procedure ReactivaUno;
  end;

var
  dmInterbase: TdmInterbase;

procedure CallNotePad( const sFileName: String );
procedure SetServerType( Value: TServerType );
function GetServerType: TServerType;
function GetServerDirectory: String;
function Ejecutar( const sProgram, sParameters: String; const lShow: Boolean ): Boolean; overload;
function Ejecutar( const sProgram, sParameters: String; const lShow: Boolean; const iOkResult: Integer ): Boolean; overload;

implementation

uses ZetaWinAPITools,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaRegistryServer,
     ZetaServerTools;

const
     K_MB = 1048576;
     K_INTERBASE_56_FILE_STRUCTURE = 9;

var
   FServerType: TServerType;

{$R *.DFM}

function GetServerType: TServerType;
begin
     Result := FServerType;
end;

procedure SetServerType( Value: TServerType );
begin
     FServerType := Value;
end;

function GetServerDirectory: String;
begin
     case FServerType of
          stInterbase: Result := ZetaWinAPITools.GetInterbaseServerDirectory;
          stFirebird: Result := ZetaWinAPITools.GetFirebirdServerDirectory;
     else
         Result := '';
     end;
end;

procedure InitServerType;
begin
     FServerType := stInterbase;
     if ZetaWinAPITools.GetFirebirdIsInstalled then
     begin
          FServerType := stFirebird;
     end;
end;

function BoolToYesNo( const lValue: Boolean ): String;
begin
     if lValue then
        Result := 'Si'
     else
         Result := 'No';
end;

function IntToYesNo( const iValue: Integer ): String;
begin
     Result := BoolToYesNo( iValue = 1 );
end;

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      ShowCmd );
end;

function WinExecAndWait32( const Programa, Parametros: String; Visibility: Integer ): Integer;
var
   zAppName: array[ 0..512 ] of Char;
   zCurDir: array[ 0..255 ] of Char;
   WorkDir: String;
   StartupInfo: TStartupInfo;
   ProcessInfo: TProcessInformation;
   lpExitCode: DWORD;
begin
     if FileExists( Programa ) then
     begin
          if StrLleno( Parametros ) then
             StrPCopy( zAppName, Programa + ' ' + Parametros )
          else
              StrPCopy( zAppName, Programa );
          {
          ShowMessage( zAppName );
          }
          GetDir( 0, WorkDir );
          StrPCopy( zCurDir, WorkDir );
          FillChar( StartupInfo, Sizeof( StartupInfo ), #0 );
          with StartupInfo do
          begin
               cb := Sizeof( StartupInfo );
               dwFlags := STARTF_USESHOWWINDOW + STARTF_USEFILLATTRIBUTE;
               wShowWindow := Visibility;
               dwFillAttribute := FOREGROUND_BLUE + BACKGROUND_RED + BACKGROUND_GREEN + BACKGROUND_BLUE;
          end;
          if not CreateProcess( nil,
                                zAppName, { pointer to command line string }
                                nil,      { pointer to process security attributes }
                                nil,      { pointer to thread security attributes }
                                False,    { handle inheritance flag }
                                CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS,          { creation flags }
                                nil,               { pointer to new environment block }
                                nil,               { pointer to current directory name }
                                StartupInfo,       { pointer to STARTUPINFO }
                                ProcessInfo ) then
          begin
               Result := -1; { pointer to PROCESS_INF / FRACASO}
          end
          else
          begin
               with ProcessInfo do
               begin
                    WaitForSingleObject( hProcess, INFINITE );
                    GetExitCodeProcess( hProcess, lpExitCode );
                    Result := lpExitCode; {SE EJECUTO CON EXITO}
               end;
          end;
          Application.ProcessMessages;
     end
     else
         Result := -2;//NO EXISTE EL ARCHIVO
end;

function Ejecutar( const sProgram, sParameters: String; const lShow: Boolean; const iOkResult: Integer ): Boolean;
const
     K_CAPTION = '� Error Al Ejecutar Proceso Externo !';
     aShow: array[ False..True ] of Word = ( SW_HIDE, SW_SHOWDEFAULT );
var
   iResult: Integer;
begin
     Result := False;
     iResult := WinExecAndWait32( sProgram, sParameters, aShow[ lShow ] );
     case iResult of
          0: Result := True;
          -1: ZetaDialogo.zError( K_CAPTION, 'El Programa ' + sProgram + ' No Pudo Ser Invocado', 0 );
          -2: ZetaDialogo.zError( K_CAPTION, 'El Programa ' + sProgram + ' No Existe', 0 );
     else
         if ( iResult > iOKResult ) then
            ZetaDialogo.zError( K_CAPTION, 'El Programa ' + sProgram + ' Termin� Con Error ( ' + IntToStr( iResult ) + ' )', 0 )
         else
             Result := True;
     end;
     {
     case ExecuteFile( sProgram, sParameters, ExtractFilePath( sProgram ), SW_SHOW	 ) of
          0: ZetaDialogo.zError( K_CAPTION, 'The operating system is out of memory or resources', 0 );
          ERROR_FILE_NOT_FOUND: ZetaDialogo.zError( K_CAPTION, 'The specified file was not found', 0 );
          ERROR_PATH_NOT_FOUND: ZetaDialogo.zError( K_CAPTION, 'The specified path was not found', 0 );
          ERROR_BAD_FORMAT: ZetaDialogo.zError( K_CAPTION, 'The .EXE file is invalid (non-Win32 .EXE or error in .EXE image)', 0 );
          SE_ERR_ACCESSDENIED: ZetaDialogo.zError( K_CAPTION, 'The operating system denied access to the specified file', 0 );
          SE_ERR_ASSOCINCOMPLETE: ZetaDialogo.zError( K_CAPTION, 'The filename association is incomplete or invalid', 0 );
          SE_ERR_DDEBUSY: ZetaDialogo.zError( K_CAPTION, 'The DDE transaction could not be completed because other DDE transactions were being processed', 0 );
          SE_ERR_DDEFAIL: ZetaDialogo.zError( K_CAPTION, 'The DDE transaction failed', 0 );
          SE_ERR_DDETIMEOUT: ZetaDialogo.zError( K_CAPTION, 'The DDE transaction could not be completed because the request timed out', 0 );
          SE_ERR_DLLNOTFOUND: ZetaDialogo.zError( K_CAPTION, 'The specified dynamic-link library was not found', 0 );
          SE_ERR_NOASSOC: ZetaDialogo.zError( K_CAPTION, 'There is no application associated with the given filename extension', 0 );
          SE_ERR_OOM: ZetaDialogo.zError( K_CAPTION, 'There was not enough memory to complete the operation', 0 );
          SE_ERR_SHARE: ZetaDialogo.zError( K_CAPTION, 'A sharing violation occurred', 0 );
     else
         Result := True;
     end;
     }
end;

function Ejecutar( const sProgram, sParameters: String; const lShow: Boolean ): Boolean;
begin
     Result := Ejecutar( sProgram, sParameters, lShow, 0 );
end;

procedure CallNotePad( const sFileName: String );
begin
     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ), SW_SHOWDEFAULT );
end;

{ ******** TdmInterbase ********* }

procedure TdmInterbase.DataModuleCreate(Sender: TObject);
begin
     FLista := TStringList.Create;
     FAliasMgr := TZetaAliasManager.Create;
     FEmpresas := TList.Create;
     {$ifdef CHECA_NULOS}
     FCheckNulls := False;
     {$endif}
end;

procedure TdmInterbase.DataModuleDestroy(Sender: TObject);
begin
     ClearEmpresas;
     FEmpresas.Free;
     FAliasMgr.Free;
     FLista.Free;
end;

function TdmInterbase.GetEmpresas( Index: Integer ): TEmpresaData;
begin
     Result := TEmpresaData( FEmpresas.Items[ Index ] );
end;

function TdmInterbase.GetEmpresasCount: Integer;
begin
     Result := FEmpresas.Count;
end;

procedure TdmInterbase.ClearEmpresas;
var
   i: Integer;
begin
     for i := ( EmpresasCount - 1 ) downto 0 do
     begin
          Empresas[ i ].Free;
     end;
end;

procedure TdmInterbase.AttachQuery( Query: TIB_Cursor; const sScript: String );
begin
     with Query do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
          Prepare;
     end;
end;

procedure TdmInterbase.ExecuteCommand( Query: TIB_DSQL );
begin
     with Query do
     begin
          with IB_Transaction do
          begin
               StartTransaction;
               try
                  ExecSQL;
                  Commit;
               except
                     on Error: Exception do
                     begin
                          RollBack;
                          raise;
                     end;
               end;
          end;
     end;
end;

procedure TdmInterbase.ExecuteStatement( Query: TIB_DSQL; const sScript: String );
begin
     AttachStatement( Query, sScript );
     ExecuteCommand( Query );
end;

procedure TdmInterbase.AttachStatement( Query: TIB_DSQL; const sScript: String );
begin
     with Query do
     begin
          Active := False;
          if Prepared then
             UnPrepare;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
          Prepare;
     end;
end;

function TdmInterbase.ConnectDatabase( Database: TIB_Database; const sUserName, sPassword, sFileName: String ): Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with Database do
           begin
                if not Connected then
                begin
                     DatabaseName := sFileName;
                     UserName := sUserName;
                     Password := sPassword;
                     Connected := True;
                end;
                Result := Connected;
           end;
        except
              on Error: Exception do
              begin
                   ZetaDialogo.ZExcepcion( '� Error Al Conectar Base de Datos !', 'La Base De Datos No Pudo Ser Accesada', Error, 0 );
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmInterbase.GetDatabaseName( Database: TIB_Database ):String;
begin
     with Database.Characteristics do
     begin
          Result := Format( '%s:%s', [ dbSite, dbFile ] );
     end;
end;

function TdmInterbase.DisconnectDatabase( Database: TIB_Database ): Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with Database do
           begin
                Connected := False;
                Result := Connected;
           end;
        except
              on Error: Exception do
              begin
                   ZetaDialogo.ZExcepcion( '� Error Al Desconectar Base de Datos !', 'La Base De Datos No Pudo Ser Accesada', Error, 0 );
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmInterbase.ConnectSource(const sUserName, sPassword, sFileName: String): Boolean;
begin
     Result := ConnectDatabase( IBSourceDB, sUserName, sPassword, sFileName );
end;

function TdmInterbase.GetDatabaseFile( const sAlias: String): String;
begin
     try
        Result := FAliasMgr.GetServerName( sAlias );
     except
           on Error: Exception do
           begin
                Error.Message := Error.Message + Format( ' ( Alias %s )', [ sAlias ] );
                raise;
           end;
     end;
end;

function TdmInterbase.ConnectTarget(const sUserName, sPassword, sFileName: String): Boolean;
begin
     Result := ConnectDatabase( IBTargetDB, sUserName, sPassword, sFileName );
end;

function TdmInterbase.DisconnectSource: Boolean;
begin
     Result := DisconnectDatabase( IBSourceDB );
end;

function TdmInterbase.DisconnectTarget: Boolean;
begin
     Result := DisconnectDatabase( IBTargetDB );
end;

procedure TdmInterbase.GetUserInfo( Texto: TStrings );
begin
     with Texto do
     begin
          BeginUpdate;
          try
             Clear;
             Assign( IBSourceDB.Users );
          finally
                 EndUpdate;
          end;
     end;
end;

function TdmInterbase.FindDatabaseBufferSize( const iValue: Integer ): Extended;
begin
     with IBSourceDB.Characteristics do
     begin
          Result := iValue * dbPage_Size / K_MB;
     end;
end;

function TdmInterbase.GetDatabaseBuffers: Integer;
begin
     with IBSourceDB.Characteristics do
     begin
          Result := dbPage_Buffers;
     end;
end;

function TdmInterbase.GetDatabaseBufferSize: Extended;
begin
     with IBSourceDB do
     begin
          Result := FindDatabaseBufferSize( Buffers );
     end;
end;

function TdmInterbase.GetDatabaseSweepInterval: Integer;
begin
     with IBSourceDB do
     begin
          Result := SweepInterval;
     end;
end;

function TdmInterbase.GetDatabaseForcedWrites: Boolean;
begin
     with IBSourceDB do
     begin
          Result := ( Characteristics.dbForced_Writes = 1 );
     end;
end;

function TdmInterbase.CheckWrongDatabaseStructure( var sMensaje: String ): Boolean;
const
     K_INTERBASE_56_VERSION = 'WI-V5.6.0.29';
begin
     with IBSourceDB do
     begin
          with Characteristics do
          begin
               Result := ( ( dbODS_Version > K_INTERBASE_56_FILE_STRUCTURE ) and ( Pos( K_INTERBASE_56_VERSION, dbVersion ) > 0 ) );
               if Result then
               begin
                    sMensaje := Format( 'La Versi�n De La Estructura De La Base De Datos ( %d.%d ) %s No Puede Ser Manejada Por Interbase 5.6', [ dbODS_Version, dbODS_Minor_Version, CR_LF ] );
               end;
          end;
     end;
end;

procedure TdmInterbase.GetDatabaseInfo( Texto: TStrings );

function GetFormato( const iVersion : integer ) : string;
begin
     if (iVersion > K_INTERBASE_56_FILE_STRUCTURE) then
        Result := 'FIREBIRD'
     else
         Result := 'INTERBASE';

end;

procedure Agrega( const sTitulo, sValor: String );
begin
     Texto.Add( ZetaCommonTools.PadR( sTitulo, 30 ) + ': ' + Trim( sValor ) );
end;

begin
     with Texto do
     begin
          BeginUpdate;
          try
             Clear;
             with IBSourceDB do
             begin
                  with Characteristics do
                  begin
                       {
                       dbLevelPrefix: byte;
                       dbBase_Level: byte;
                       dbFilePrefix: byte;
                       }
                       Agrega( 'Formato Archivo', Format( '%s', [ GetFormato( dbODS_Version ) ] ) );
                       Agrega( 'Versi�n Estructura', Format( '%d.%d', [ dbODS_Version, dbODS_Minor_Version ] ) );
                       Agrega( 'Versi�n Servidor', Format( '%s ( %d )', [ dbVersion, dbVersionPrefix ] ) );
                       Agrega( 'Dialecto de SQL', Format( '%d', [ dbSQLDialect ] ) );
                       Agrega( 'Solo Lectura', Format( '%s', [ BoolToYesNo( ReadOnly ) ] ) );
                       Agrega( 'Intervalo de "Sweep"', Format( '%14.0n', [ dbSweep_Interval / 1 ] ) );
                       Agrega( 'Escrituras Directas A Disco', Format( '%s', [ IntToYesNo( dbForced_Writes ) ] ) );
                       Agrega( 'Buffers', Format( '%14.0n', [ dbPage_Buffers / 1 ] ) );
                       Agrega( 'Tama�o de P�gina', Format( '%14.0n', [ dbPage_Size / 1 ] ) );
                       Agrega( 'P�ginas Asignadas', Format( '%14.0n', [ dbSizeInPages / 1 ] ) );
                       Agrega( 'Memoria Asignada', Format( '%14.2n', [ dbSizeInPages * dbPage_Size / K_MB ] ) );
                       Agrega( 'Memoria Usada', Format( '%14.0n MB', [ dbAllocation / K_MB ] ) );
                       Agrega( 'Nivel Base', Format( '%d', [ dbBase_Level ] ) );
                       Agrega( 'Implementaci�n Clase', Format( '%d', [ dbClass ] ) );
                       Agrega( 'Implementaci�n N�mero', Format( '%d:%d', [ dbImplementationPrefix, dbImplementation ] ) );
                       Agrega( 'Sitio', Format( '%s', [ dbSite ] ) );
                       Agrega( 'Archivo', Format( '%s', [ dbFile ] ) );
                       Agrega( 'Espacio Para Reserva', Format( '%s', [ IntToYesNo( dbNo_Reserve ) ] ) );
                       {
                       Agrega( 'M�xima Memoria Usada', Format( '%14.0n MB', [ MaxMemory / K_MB ] ) );
                       }
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

{ ************* Copiado de Tablas ************** }

function TdmInterbase.GetDatabaseTableList(  const sFileName: String ): Boolean;
var
   oCursor: TCursor;
   i: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           ListTables;
           with TStringList.Create do
           begin
                Add( 'Lista De Tablas' );
                Add( '-------------------------' );
                for i := 0 to ( FLista.Count - 1 ) do
                begin
                     Add( FLista.Strings[ i ] );
                end;
                Add( '------------------------------------------' );
                Add( 'SCRIPT de SQL para Borrar Todas Las Tablas' );
                Add( '------------------------------------------' );
                Add( 'connect "SERVER_NAME:<Disco>:<Folder>\<databasefile>" user "SYSDBA" password "masterkey";' );
                for i := ( FLista.Count - 1 ) downto 0 do
                begin
                     Add( Format( 'delete from %s;', [ Trim( FLista.Strings[ i ] ) ] ) );
                end;
                Add( '------------------------------------------' );
                SaveToFile( sFileName );
                Free;
           end;
           Result := True;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmInterbase.Copiar( Callback: TCallMeback; TableCallBack: TTableCallBack; Bitacora: TStrings; const lVerificar, lBorrar: Boolean );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if IBSourceDB.Connected then
        begin
             if IBTargetDB.Connected then
             begin
                  FBitacora := Bitacora;
                  try
                     AddLogMsg( 'Base De Datos Fuente: ' + GetDatabaseName( IBSourceDB ) );
                     AddLogMsg( 'Base De Datos Destino: ' + GetDatabaseName( IBTargetDB ) );
                     AddLogMsg( 'Borrar Tablas: ' + BoolToYesNo( lBorrar ) );
                     AddLogMsg( 'Verificar Tablas: ' + BoolToYesNo( lVerificar ) );
                     AddSeparator;
                     ListTables;
                     if not lBorrar or DeleteTables( CallBack, TableCallBack ) then
                        CopyTables( Callback, TableCallBack, lVerificar );
                  finally
                         FBitacora := nil;
                  end;
             end
             else
                 ZetaDialogo.zError( '� Error En Base De Datos Destino !', '� Base De Datos Destino No Est� Conectada !', 0 );
        end
        else
            ZetaDialogo.zError( '� Error En Base De Datos Fuente !', '� Base De Datos Fuente No Est� Conectada !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmInterbase.AddLogMsg( const sMensaje: String );
begin
     FBitacora.Add( sMensaje );
end;

procedure TdmInterbase.AddSeparator;
begin
     AddLogMsg( StringOfChar( '*', 40 ) );
end;

procedure TdmInterbase.AddLogInsertException(const sMensaje: String; Error: Exception);
var
   i: Integer;
begin
     AddLogException( sMensaje, Error );
     AddLogMsg( '--- Contenido del Registro ---' );
     with tqSource do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               AddLogMsg( Fields[ i ].FieldName + ': ' + Fields[ i ].AsString );
          end;
     end;
end;

procedure TdmInterbase.AddLogVerification(const sMensaje: String);
var
   i: Integer;
begin
     AddLogMsg( '+++ ' + sMensaje + ' +++' );
     AddLogMsg( '--- Contenido de los Registros diferentes ---' );
     with tqSource do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               AddLogMsg( Fields[ i ].FieldName + ': ' + Fields[ i ].AsString + ' | ' + tqTarget.Fields[ i ].AsString );
          end;
     end;
end;

procedure TdmInterbase.AddLogException(const sMensaje: String; Error: Exception);
begin
     AddLogMsg( Format( '***** %s *****', [ sMensaje ] ) );
     AddLogMsg( Error.Message );
end;

procedure TdmInterbase.AddLogTimeStamp( const sMensaje: String );
begin
     AddLogMsg( FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) + ' - ' + sMensaje );
end;

procedure TdmInterbase.DeterminaDependencias( const sValor: String );
var
   FDependencias: TStrings;
   i: Integer;
   sLlaveForanea: String;
begin
     FDependencias := TStringList.Create;
     try
        try
           with tqSource do
           begin
                Active := False;
                ParamByName( 'Tabla' ).AsString := sValor;
                Active := True;
                while not Eof do
                begin
                     sLlaveForanea := FieldByName( 'KEY_NAME' ).AsString;
                     if ZetaCommonTools.StrLleno( sLlaveForanea ) then
                     begin
                          with tqSource2 do
                          begin
                               Active := False;
                               ParamByName( 'Indice' ).AsString := sLlaveForanea;
                               Active := True;
                               FDependencias.Add( FieldByName( 'TABLE_NAME' ).AsString );
                               Active := False;
                          end;
                     end;
                     Next;
                end;
                Active := False;
           end;
        except
              on Error: Exception do
              begin
                   FDependencias.Clear;
                   AddLogException( 'Error Al Determinar Dependencias', Error );
              end;
        end;
        with FDependencias do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  DeterminaDependencias( Strings[ i ] );
             end;
        end;
        with FLista do
        begin
             if ( IndexOf( sValor ) < 0 ) then  { No Existe en la Lista }
             begin
                  Add( sValor );                  { Agrega al Final }
             end;
        end;
     finally
            FDependencias.Free;
     end;
end;

procedure TdmInterbase.ListTables;
var
   i: Integer;
   FTablas: TStrings;
begin
     FTablas := TStringList.Create;
     try
        with FLista do
        begin
             BeginUpdate;
             Clear;
        end;
        try
           if FindCmdLineSwitch( 'TABLES', [ '-', '/' ], True ) then
           begin
                { Lista unicamente las tablas con CB_CODIGO }
                { Util para transferencia de empleados }
                AttachQuery( tqSource, 'select T.RDB$RELATION_NAME TABLE_NAME from RDB$RELATIONS T where '+
                                       '( ( T.RDB$SYSTEM_FLAG = 0 ) or ( T.RDB$SYSTEM_FLAG is NULL ) ) and '+
                                       '( T.RDB$DBKEY_LENGTH = 8 ) and '+
                                       '( ( select COUNT(*) from RDB$VIEW_RELATIONS V where ( V.RDB$VIEW_NAME = T.RDB$RELATION_NAME ) ) = 0 ) and '+
                                       '( ( select COUNT(*) from RDB$RELATION_FIELDS F where '+
                                       '( F.RDB$RELATION_NAME = T.RDB$RELATION_NAME ) and '+
                                       '( F.RDB$FIELD_NAME = ''CB_CODIGO'' ) ) > 0 ) '+
                                       'order by T.RDB$RELATION_NAME' );
           end
           else
           begin
                AttachQuery( tqSource, 'select T.RDB$RELATION_NAME TABLE_NAME from RDB$RELATIONS T where '+
                                       '( ( T.RDB$SYSTEM_FLAG = 0 ) or ( T.RDB$SYSTEM_FLAG is NULL ) ) and '+
                                       '( T.RDB$DBKEY_LENGTH = 8 ) and '+
                                       '( ( select COUNT(*) from RDB$VIEW_RELATIONS V where ( V.RDB$VIEW_NAME = T.RDB$RELATION_NAME ) ) = 0 ) '+
                                       'order by T.RDB$RELATION_NAME' );
           end;
           with tqSource do
           begin
                Active := True;
                while not Eof do
                begin
                     FTablas.Add( FieldByName( 'TABLE_NAME' ).AsString );
                     Next;
                end;
                Active := False;
           end;
           AttachQuery( tqSource, 'select RDB$FOREIGN_KEY KEY_NAME from RDB$INDICES where ( RDB$RELATION_NAME = :Tabla ) and not ( RDB$FOREIGN_KEY is NULL ) or ( RDB$FOREIGN_KEY = "" )' );
           AttachQuery( tqSource2, 'select RDB$RELATION_NAME TABLE_NAME from RDB$INDICES where ( RDB$INDEX_NAME = :Indice )' );
           with FTablas do
           begin
                for i := 0 to ( Count - 1 ) do
                begin
                     DeterminaDependencias( Strings[ i ] );
                end;
           end;
        finally
               with FLista do
               begin
                    EndUpdate;
               end;
        end;
     finally
            FTablas.Free;
     end;
end;

function TdmInterbase.DeleteTables( Callback: TCallMeBack; TableCallBack: TTableCallBack ): Boolean;
const
     K_DELETE = 'delete from %s';
var
   i: Integer;
   sTabla: String;
   lContinue, lErrores: Boolean;
begin
     lErrores := False;
     lContinue := True;
     AddSeparator;
     AddLogTimeStamp( 'Empezando Borrado De Tablas' );
     AddSeparator;
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               TableCallBack( Format( 'Tabla %d de %d', [ i + 1, Count ] ) );
               sTabla := Trim( Strings[ i ] );
               try
                  ExecuteStatement( stTarget, Format( K_DELETE, [ sTabla ] ) );
                  AddLogMsg( sTabla + ': Borrada' );
                  lContinue := CallBack( sTabla + ': Borrada', 0, lErrores );
               except
                     on Error: Exception do
                     begin
                          lErrores := True;
                          AddLogException( 'Error Al Borrar Tabla ' + sTabla, Error );
                          lContinue := CallBack( 'Error Al Borrar Tabla ' + sTabla, 0, lErrores );
                     end;
               end;
               if not lContinue then
                  Break;
          end;
     end;
     TableCallBack( '' );
     AddSeparator;
     AddLogTimeStamp( 'Borrado De Tablas Terminado' );
     AddSeparator;
     Result := not lErrores;
end;

function TdmInterbase.GetInsertScript( const sTableName: String; Source: TIB_Cursor ): String;
var
   Lista: TStrings;
   i: Integer;
   sScript, sCampos: String;
begin
     sScript := 'insert into ' + sTableName + ' ( ';
     sCampos := ' ) values ( ';
     Lista := TStringList.Create;
     with Lista do
     begin
          try
             Source.GetFieldNamesList( Lista );
             for i := 0 to ( Count - 1 ) do
             begin
                  sScript := sScript + Strings[ i ] + ',';
                  sCampos := sCampos + ':' + Strings[ i ] + ',';
             end;
          finally
                 Free;
          end;
     end;
     Result := ZetaCommonTools.CortaUltimo( sScript ) + ZetaCommonTools.CortaUltimo( sCampos ) + ' )';
end;

function TdmInterbase.CopyOneTable( const sTableName: String; Callback: TCallMeBack; var lContinue: Boolean ): Boolean;
const
     K_COUNT = 'select COUNT(*) from %s';
     K_SELECT = 'select * from %s';
     K_INSERT = 'insert into %s ( %s ) values ( %s )';
var
   i, j, iCtr, iTxCtr, iTope, iSourceCount, iTargetCount: Integer;
   lErrores: Boolean;
   {$ifdef CHECA_NULOS}
   oParametro: TIB_Column;
   {$endif}

function GetTope: Integer;
begin
     if ( iCtr < 100 ) then
        Result := 9
     else
         if ( iCtr < 1000 ) then
            Result := 99
         else
             Result := 499;
end;

begin
     lErrores := False;
     try
        AttachQuery( tqSource, Format( K_COUNT, [ sTableName ] ) );
        with tqSource do
        begin
             Active := True;
             if Eof then
                iSourceCount := 0
             else
                 iSourceCount := Fields[ 0 ].AsInteger;
             Active := False;
        end;
        if ( iSourceCount > 0 ) then
        begin
             AttachQuery( tqSource, Format( K_SELECT, [ sTableName ] ) );
             with tqSource do
             begin
                  Active := True;
                  AttachStatement( stTarget, GetInsertScript( sTableName, tqSource ) );
                  i := 501;
                  iCtr := 0;
                  iTxCtr := 0;
                  iTope := GetTope;
                  with stTarget.IB_Transaction do
                  begin
                       StartTransaction;
                  end;
                  while not Eof and lContinue do
                  begin
                       if ( i > iTope ) then
                       begin
                            lContinue := Callback( sTableName, iCtr, lErrores );
                            i := 1;
                            iTope := GetTope;
                       end
                       else
                           Inc( i );
                       { Cada 99 escrituras se hace un Database.Commit }
                       if ( iTxCtr > 99 ) then
                       begin
                            iTxCtr := 1;
                            with stTarget.IB_Transaction do
                            begin
                                 if InTransaction then
                                    Commit;
                                 StartTransaction;
                            end;
                       end
                       else
                           Inc( iTxCtr );
                       Inc( iCtr );
                       try
                          { Asigna Campos Fuente a Parametros Destino }
                          with stTarget do
                          begin
                               with Params do
                               begin
                                    BeginUpdate;
                               end;
                          end;
                          with Fields do
                          begin
                               for j := 0 to ( ColumnCount - 1 ) do
                               begin
                                    {$ifdef CHECA_NULOS}
                                    with Columns[ j ] do
                                    begin
                                         oParametro := stTarget.ParamByName( FieldName );
                                         { Si se checan los nulls y }
                                         { Si la columna destino no permite nulls y }
                                         { la columna fuente trae un null }
                                         if FCheckNulls and not oParametro.IsNullable and IsNull then
                                         begin
                                              if IsBoolean then
                                                 oParametro.AsBoolean := False
                                              else if IsCurrency then
                                                   oParametro.AsCurrency := 0
                                              else if IsDateOnly then
                                                   oParametro.AsDate := 0
                                              else if IsDateTime then
                                                   oParametro.AsDateTime := 0
                                              else if IsNumeric then
                                                   oParametro.AsInteger := 0
                                              else if IsText then
                                                   oParametro.AsString := ''
                                              else if IsTimeOnly then
                                                   oParametro.AsDateTime := 0
                                              else
                                                  oParametro.Assign( Columns[ j ] );
                                         end
                                         else
                                         begin
                                              oParametro.Assign( Columns[ j ] );
                                         end;
                                    end;
                                    {$else}
                                    stTarget.ParamByName( oColumns[ j ].FieldName ).Assign( oColumns[ j ] );
                                    {$endif}
                               end;
                          end;
                          { Inserta Registro Destino }
                          with stTarget do
                          begin
                               with Params do
                               begin
                                    EndUpdate( True );
                               end;
                               ExecSQL;
                          end;
                       except
                             on Error: Exception do
                             begin
                                  lErrores := True;
                                  AddLogInsertException( 'Error Al Copiar Registro ' + IntToStr( iCtr ) + ' De Tabla ' + sTableName, Error );
                             end;
                       end;
                       Next;
                  end;
                  Active := False;
                  with stTarget.IB_Transaction do
                  begin
                       if InTransaction then
                          Commit;
                  end;
             end;
             AttachQuery( tqTarget, Format( K_COUNT, [ sTableName ] ) );
             with tqTarget do
             begin
                  Active := True;
                  if Eof then
                     iTargetCount := 0
                  else
                      iTargetCount := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
             if ( iSourceCount <> iTargetCount ) then
                raise Exception.Create( Format( 'Tabla %s Tiene %d En Fuente y %d En Destino', [ sTableName, iSourceCount, iTargetCount ] ) );
        end;
     except
           on Error: Exception do
           begin
                lErrores := True;
                AddLogException( 'Error Al Copiar Tabla ' + sTableName, Error );
           end;
     end;
     Result := not lErrores;
end;

procedure TdmInterbase.VerifyOneTable( const sTableName: String; Callback: TCallMeBack; var lContinue: Boolean );
const
     K_SELECT = 'select * from %s order by %s';
     K_INDEX_FIELDS = 'select C.RDB$FIELD_NAME from RDB$INDEX_SEGMENTS C where ( C.RDB$INDEX_NAME in ( select I.RDB$INDEX_NAME from RDB$INDICES I where '+
                      '( I.RDB$UNIQUE_FLAG = 1 ) and '+
                      '( I.RDB$INDEX_NAME like "%s" ) and '+
                      '( I.RDB$RELATION_NAME = "%s" ) ) ) '+
                      'order by C.RDB$FIELD_POSITION';
var
   i, j, iCtr, iFieldCount: Integer;
   lOk, lErrores, lNotEqual: Boolean;
   sFieldName, sCampos: String;
   oSource, oTarget: TIB_Column;

function GetTargetField( const sName: String ): TIB_Column;
begin
     with tqTarget do
     begin
          Result := FieldByName( sName );
     end;
end;

begin
     try
        sCampos := '';
        AttachQuery( tqSource, Format( K_INDEX_FIELDS, [ '%PRIMARY%', sTableName ] ) );
        with tqSource do
        begin
             Active := True;
             while not Eof do
             begin
                  sCampos := sCampos + Fields[ 0 ].AsString + ',';
                  Next;
             end;
             Active := False;
        end;
        sCampos := ZetaCommonTools.CortaUltimo( sCampos );
        if ZetaCommonTools.StrLleno( sCampos ) then
        begin
             AttachQuery( tqSource, Format( K_SELECT, [ sTableName, sCampos ] ) );
             AttachQuery( tqTarget, Format( K_SELECT, [ sTableName, sCampos ] ) );
             with tqTarget do
             begin
                  Active := True;
             end;
             with tqSource do
             begin
                  Active := True;
                  iFieldCount := ( Fields.ColumnCount - 1 );
                  lOk := True;
                  i := 1;
                  iCtr := 0;
                  while not Eof and lOk and lContinue do
                  begin
                       if ( i > 99 ) then
                       begin
                            lContinue := Callback( 'Verificando ' + sTableName, iCtr, lErrores );
                            i := 1;
                       end
                       else
                           i := i + 1;
                       for j := 0 to iFieldCount do
                       begin
                            oSource := Fields.Columns[ j ];
                            oTarget := tqTarget.Fields.Columns[ j ];
                            with oSource do
                            begin
                                 sFieldName := FieldName;
                                 if ( sFieldName <> oTarget.FieldName ) then
                                    lNotEqual := True
                                 else
                                    if IsBoolean then
                                       lNotEqual := ( AsBoolean <> oTarget.AsBoolean )
                                    else
                                        if IsCurrency then
                                           lNotEqual := ( AsCurrency <> oTarget.AsCurrency )
                                        else
                                            if IsDateOnly then
                                               lNotEqual := ( AsDate <> oTarget.AsDate )
                                            else
                                                if ( IsDateTime or IsTimeOnly ) then
                                                   lNotEqual := ( AsDateTime <> oTarget.AsDateTime )
                                                else
                                                    if IsNumeric then
                                                       lNotEqual := ( AsFloat <> oTarget.AsFloat )
                                                    else
                                                        if ( IsText or IsBlob ) then
                                                           lNotEqual := ( AsString <> oTarget.AsString )
                                                        else
                                                            lNotEqual := True;
                            end;
                            if lNotEqual then
                            begin
                                 AddLogVerification( 'Tabla ' + sTableName + ' No Fu� Copiada Correctamente ( Registro # ' + IntToStr( iCtr ) + ' Campo ' + sFieldName + ' )' );
                                 lOk := False;
                                 Break;
                            end;
                       end;
                       iCtr := iCtr + 1;
                       tqTarget.Next;
                       Next;
                  end;
             end;
             tqSource.Active := False;
             tqTarget.Active := False;
        end;
        AddLogMsg( sTableName + ': Verificada' );
     except
           on Error: Exception do
           begin
                AddLogException( 'Error Al Verificar Tabla ' + sTableName, Error );
           end;
     end;
end;

procedure TdmInterbase.CopyTables( Callback: TCallMeBack; TableCallBack: TTableCallBack; const lVerificar: Boolean );
var
   i: Integer;
   sTabla: String;
   lContinue, lErrores: Boolean;
begin
     lErrores := False;
     lContinue := True;
     with FLista do
     begin
          AddSeparator;
          AddLogTimeStamp( 'Empezando Copia De Base De Datos' );
          AddSeparator;
          for i := 0 to ( Count - 1 ) do
          begin
               TableCallBack( Format( 'Tabla %d de %d', [ i + 1, Count ] ) );
               sTabla := Trim( Strings[ i ] );
               lContinue := CallBack( sTabla + ': Empezando', 0, lErrores );
               if not lContinue then
                  Break;
               AddLogMsg( sTabla + ': Empezando' );
               if not CopyOneTable( sTabla, CallBack, lContinue ) then
                  lErrores := True;
               if not lContinue then
                  Break;
               if lVerificar then
                  VerifyOneTable( sTabla, CallBack, lContinue );
               if not lContinue then
                  Break;
               AddLogMsg( sTabla + ': Copiada' );
               lContinue := CallBack( sTabla + ': Copiada', 0, lErrores );
               if not lContinue then
                  Break;
          end;
          TableCallBack( '' );
          AddSeparator;
          if lContinue then
          begin
               CallBack( 'Base De Datos Ha Sido Copiada', 0, lErrores );
               AddLogTimeStamp( 'Base De Datos Ha Sido Copiada' );
          end
          else
          begin
               CallBack( 'Copiado De Base De Datos Fu� Interrumpido', 0, lErrores );
               AddLogTimeStamp( 'Copiado De Base De Datos Fu� Interrumpido' );
          end;
          AddSeparator;
          Clear;
     end;
end;

{ ****** Reactivacion de Indices ************ }

function TdmInterbase.RefrescaIndices( const iIndexType: Integer; var sError: String ): Boolean;
var
   sFiltro: String;
begin
     if IBSourceDB.Connected then
     begin
          case iIndexType of
               0: sFiltro := 'and RDB$FOREIGN_KEY is NULL ';
               1: sFiltro := 'and RDB$FOREIGN_KEY is NOT NULL ';
          else
              sFiltro := '';
          end;
          with tqQuery do
          begin
               Active   := False;
               SQL.Text := 'select RDB$RELATION_NAME TABLA, RDB$INDEX_NAME INDICE ' +
                           'from RDB$INDICES ' +
                           'where ( RDB$INDEX_INACTIVE = 1 ) ' + sFiltro  +
                           'order by RDB$RELATION_NAME';
               Active := True;
               if Eof then
               begin
                    sError := 'No Hay Indices Inactivos';
                    Result := False;
               end
               else
                   Result := True;
          end;
     end
     else
     begin
          sError := 'No Hay Conexi�n A La Base De Datos';
          Result := False;
     end;
end;

procedure TdmInterbase.ReactivaIndice( const sIndice: String );
begin
     ExecuteStatement( stSource, Format( 'alter index %s active', [ sIndice ] ) );
end;

procedure TdmInterbase.ReactivaUno;
begin
     with tqQuery do
     begin
          ReactivaIndice( Fields[ 1 ].AsString );
     end;
end;

procedure TdmInterbase.ReactivaTodos;
begin
     with tqQuery do
     begin
          First;
          while not Eof do
          begin
               ReactivaUno;
               Next;
          end;
     end;
end;

{ ********* Creaci�n de Lista de Empresas *********** }

function TdmInterbase.ConectarComparte: Boolean;
var
   sOp, sAlias, sComparte: String;
begin
     sOp := 'Creando TZetaRegistryServer';
     try
        with TZetaRegistryServer.Create do
        begin
             try
                sOp := 'Leyendo ALIAS->SERVER_NAME';
                sComparte := GetDatabaseFile( AliasComparte );
                sOp := 'Conectando Base de Datos';
                Result := ConnectSource( UserName, Password, sComparte );
                if Result then
                begin
                     sOp := 'Borrando Lista De Empresas';
                     ClearEmpresas;
                     try
                        sOp := 'Leyendo Empresas';
                        AttachQuery( tqSource, 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_USRNAME, CM_PASSWRD from COMPANY order by CM_NOMBRE' );
                        with tqSource do
                        begin
                             Active := True;
                             sOp := 'Navengando Lista De Empresas';
                             while not Eof do
                             begin
                                  sAlias := FieldByName( 'CM_ALIAS' ).AsString;
                                  try
                                     TEmpresaData.Create( FEmpresas,
                                                          FieldByName( 'CM_CODIGO' ).AsString,
                                                          FieldByName( 'CM_NOMBRE' ).AsString,
                                                          sAlias,
                                                          FieldByName( 'CM_USRNAME' ).AsString,
                                                          ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString ),
                                                          GetDatabaseFile( sAlias ),
                                                          False );
                                  except
                                        on Error: Exception do
                                        begin
                                             Application.HandleException( Error );
                                        end;
                                  end;
                                  Next;
                             end;
                             sOp := 'Cerrando Lista de Empresas';
                             Active := False;
                        end;
                     except
                           on Error: Exception do
                           begin
                                ZetaDialogo.ZExcepcion( '� Error Al Leer Empresas !', sOp, Error, 0 );
                                Result := False;
                           end;
                     end;
                     sOp := 'Creando Empresa COMPARTE';
                     TEmpresaData.Create( FEmpresas,
                                          'COMPARTE',
                                          'Base de Datos COMPARTE',
                                          AliasComparte,
                                          UserName,
                                          Password,
                                          sComparte,
                                          True );
                end;
             finally
                    Free;
             end;
        end;
        sOp := 'Desconectando Base De Datos';
        DisconnectSource;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZExcepcion( '� Error Al Conectar Comparte !', sOp, Error, 0 );
                Result := False;
           end;
     end;
end;

procedure TdmInterbase.InvokeDSQL(const iEmpresa: Integer);
begin
     with Empresas[ iEmpresa ] do
     begin
          if ConnectSource( UserName, Password, GetDatabaseFile( Alias ) ) then
          begin
               IB_DSQL.Execute;
          end;
          DisconnectSource;
     end;
end;

procedure TdmInterbase.ExecuteScript(const iEmpresa: Integer);
begin
     with Empresas[ iEmpresa ] do
     begin
          if ConnectSource( UserName, Password, GetDatabaseFile( Alias ) ) then
          begin
               IB_Script.Execute;
          end;
          DisconnectSource;
     end;
end;

initialization
begin
     InitServerType;
end;

end.




