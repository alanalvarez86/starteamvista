unit FDBConfig;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ZetaNumero,
     ZetaDBTextBox;

type
  TIBConfig = class(TForm)
    PanelBotones: TPanel;
    Salir: TBitBtn;
    Ejecutar: TBitBtn;
    GenerarBatch: TBitBtn;
    SaveDialog: TSaveDialog;
    Sweeping: TCheckBox;
    ForcedWrites: TCheckBox;
    Buffers: TZetaNumero;
    SweepInterval: TZetaNumero;
    BufferValueLBL: TLabel;
    SweepIntervalLBL: TLabel;
    BuffersTotal: TLabel;
    procedure FormShow(Sender: TObject);
    procedure GenerarBatchClick(Sender: TObject);
    procedure EjecutarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure SweepingClick(Sender: TObject);
    procedure SweepIntervalChange(Sender: TObject);
    procedure ForcedWritesClick(Sender: TObject);
    procedure BuffersChange(Sender: TObject);
  private
    { Private declarations }
    FComputerName: String;
    FDatabaseFile: String;
    FPassword: String;
    FUserName: String;
    FSweepAuto: Boolean;
    FSweepInterval: Integer;
    FBuffers: Integer;
    FForcedWrites: Boolean;
    FSweepChange: Boolean;
    FBuffersChange: Boolean;
    FForcedWritesChange: Boolean;
    function CreateCommand: String;
    function GetParameters: String;
    function GetProgram: String;
    procedure Init;
    procedure SetControls;
  public
    { Public declarations }
    property ComputerName: String read FComputerName write FComputerName;
    property DatabaseFile: String read FDatabaseFile write FDatabaseFile;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
  end;

var
  IBConfig: TIBConfig;

implementation

uses ZetaDialogo,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     FDBRegistry,
     DInterbase;

{$R *.DFM}

procedure TIBConfig.FormShow(Sender: TObject);
begin
     Init;
end;

procedure TIBConfig.Init;
begin
     FSweepChange := False;
     FBuffersChange := False;
     FForcedWritesChange := False;
     with dmInterbase do
     begin
          FSweepInterval := GetDatabaseSweepInterval;
          FSweepAuto := ( FSweepInterval <> 0);
          FBuffers := GetDatabaseBuffers;
          FForcedWrites := GetDatabaseForcedWrites;
     end;
     Buffers.Valor := FBuffers;
     Sweeping.Checked := FSweepAuto;
     SweepInterval.Valor := FSweepInterval;
     ForcedWrites.Checked := FForcedWrites;
     SetControls;
end;

procedure TIBConfig.SetControls;
begin
     with Ejecutar do
     begin
          Enabled := ( FSweepChange or FBuffersChange or FForcedWritesChange );
          GenerarBatch.Enabled := Enabled;
     end;
     with SweepInterval do
     begin
          Enabled := Sweeping.Checked;
          SweepIntervalLBL.Enabled := Enabled;
     end;
     BuffersTotal.Caption := '= ' + Trim( Format( '%14.2n', [ dmInterbase.FindDatabaseBufferSize( Buffers.ValorEntero ) ] ) )  + ' MB';
end;

procedure TIBConfig.BuffersChange(Sender: TObject);
begin
     if ( Buffers.ValorEntero <> FBuffers ) then
     begin
          FBuffersChange := True;
     end;
     SetControls;
end;

procedure TIBConfig.SweepingClick(Sender: TObject);
begin
     if ( Sweeping.Checked <> FSweepAuto ) then
        FSweepChange := True;
     SetControls;
end;

procedure TIBConfig.SweepIntervalChange(Sender: TObject);
begin
     if ( SweepInterval.ValorEntero <> FSweepInterval ) then
        FSweepChange := True;
     SetControls;
end;

procedure TIBConfig.ForcedWritesClick(Sender: TObject);
begin
     if ( ForcedWrites.Checked <> FForcedWrites ) then
        FForcedWritesChange := True;
     SetControls;
end;

function TIBConfig.GetProgram: String;
const
     K_COMMAND = '%sgfix.exe';
begin
     Result := Format( K_COMMAND, [ DInterbase.GetServerDirectory ] );
end;

function TIBConfig.GetParameters: String;
const
     K_OPTIONS = '%s -z -user "%s" -password "%s"';
     K_PARAMETERS = '%s "%s"';
begin
     if FSweepChange then
     begin
          if Sweeping.Checked then
             Result := Result + Format( ' -h %d', [ SweepInterval.ValorEntero ] )
          else
              Result := Result + ' -h 0';
     end;
     if FBuffersChange then
        Result := Result + Format( ' -buffers %d', [ Buffers.ValorEntero ] );
     if FForcedWritesChange then
     begin
          if ForcedWrites.Checked then
             Result := Result + ' -w sync'
          else
              Result := Result + ' -w async';
     end;
     Result := Format( K_OPTIONS, [ Result, UserName, Password ] );
     Result := Format( K_PARAMETERS, [ Result, ComputerName + ':' + DatabaseFile ] );
end;

function TIBConfig.CreateCommand: String;
const
     K_COMMAND = '"%s" %s';
begin
     Result := Format( K_COMMAND, [ GetProgram, GetParameters ] );
end;

procedure TIBConfig.GenerarBatchClick(Sender: TObject);
var
   FDatos: TStrings;
begin
     with SaveDialog do
     begin
          Filter := 'Archivo Batch( *.bat )|*.bat|Archivo Texto ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione El Archivo Batch A Crear';
          FileName := 'IBConfig.bat';
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               FDatos := TStringList.Create;
               try
                  with FDatos do
                  begin
                       Add( CreateCommand );
                       SaveToFile( FileName );
                  end;
               finally
                      FDatos.Free;
               end;
               if ZetaDialogo.zConfirm( Caption, 'El Archivo BATCH Para Configurar Esta Base De Datos Ha Sido Creado' +
                                                 CR_LF +
                                                 CR_LF +
                                                 FileName +
                                                 CR_LF +
                                                 CR_LF +
                                                 '¿ Desea Verlo ?', 0, mbYes ) then
               begin
                    DInterbase.CallNotePad( FileName );
               end;
          end;
     end;
end;

procedure TIBConfig.EjecutarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if DInterbase.Ejecutar( GetProgram, GetParameters, True ) then
        begin
             ZetaDialogo.zInformation( Caption, 'La Operación De Configuración Ha Terminado', 0 );
             Init;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TIBConfig.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
