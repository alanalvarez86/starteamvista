unit FDBStats;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TIBStats = class(TForm)
    SaveDialog: TSaveDialog;
    PanelSuperior: TPanel;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    SeekArchivo: TSpeedButton;
    PanelBotones: TPanel;
    Salir: TBitBtn;
    Generar: TBitBtn;
    GenerarBatch: TBitBtn;
    Opciones: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure SeekArchivoClick(Sender: TObject);
    procedure GenerarBatchClick(Sender: TObject);
    procedure GenerarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
  private
    { Private declarations }
    FComputerName: String;
    FDatabaseFile: String;
    FPassword: String;
    FUserName: String;
    function CreateCommand: String;
    function GetStatsFile: String;
    function GetParameters: String;
    function GetProgram: String;
  public
    { Public declarations }
    property ComputerName: String read FComputerName write FComputerName;
    property DatabaseFile: String read FDatabaseFile write FDatabaseFile;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
  end;

var
  IBStats: TIBStats;

implementation

uses ZetaDialogo,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     FDBRegistry,
     DInterbase;

{$R *.DFM}

procedure TIBStats.FormShow(Sender: TObject);
begin
     Archivo.Text := ZetaCommonTools.StrTransform( DatabaseFile, ExtractFileExt( DatabaseFile ), '.txt' );
end;

function TIBStats.GetStatsFile: String;
begin
     Result := Archivo.Text;
end;

function TIBStats.GetProgram: String;
const
     K_COMMAND = '%sgstat.exe';
begin
     Result := Format( K_COMMAND, [ DInterbase.GetServerDirectory ] );
end;

function TIBStats.GetParameters: String;
const
     K_PARAMETERS = '-z %s "%s" >> "%s"';
begin
     case Opciones.ItemIndex of
          0: Result := Format( K_PARAMETERS, [ '-header', DatabaseFile, GetStatsFile ] );
          1: Result := Format( K_PARAMETERS, [ '-log', DatabaseFile, GetStatsFile ] );
     else
         Result := Format( K_PARAMETERS, [ '-all', DatabaseFile, GetStatsFile ] );
     end;
end;

function TIBStats.CreateCommand: String;
const
     K_COMMAND = '"%s" %s';
begin
     Result := Format( K_COMMAND, [ GetProgram, GetParameters ] );
end;

procedure TIBStats.SeekArchivoClick(Sender: TObject);
begin
     with Archivo do
     begin
          with SaveDialog do
          begin
               Filter := 'Texto ( *.txt )|*.txt|Bit�cora ( *.log )|*.log|Datos ( *.dat )|*.dat|Todos ( *.* )|*.*';
               FilterIndex := 0;
               Title := 'Seleccione El Archivo De Estad�sticas A Generar';
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TIBStats.GenerarBatchClick(Sender: TObject);
var
   FDatos: TStrings;
begin
     with SaveDialog do
     begin
          Filter := 'Archivo Batch( *.bat )|*.bat|Archivo Texto ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione El Archivo Batch A Crear';
          FileName := 'Stats.bat';
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               FDatos := TStringList.Create;
               try
                  with FDatos do
                  begin
                       Add( Format( 'del "%s"', [ GetStatsFile ] ) );
                       Add( CreateCommand );
                       SaveToFile( FileName );
                  end;
               finally
                      FDatos.Free;
               end;
               if ZetaDialogo.zConfirm( Caption, 'El Archivo BATCH Para Crear Este Respaldo Ha Sido Creado' +
                                                 CR_LF +
                                                 CR_LF +
                                                 FileName +
                                                 CR_LF +
                                                 CR_LF +
                                                 '� Desea Verlo ?', 0, mbYes ) then
               begin
                    DInterbase.CallNotePad( FileName );
               end;
          end;
     end;
end;

procedure TIBStats.GenerarClick(Sender: TObject);
var
   sStatsFile: String;
   oCursor: TCursor;
begin
     if ( Win32Platform = VER_PLATFORM_WIN32_NT	) then
     begin
          if ZetaDialogo.zConfirm( 'Proceso No Corre En Windows NT',
                                   'Las Estad�sticas De La Base De Datos' + CR_LF +
                                   'Unicamente Pueden Ser Generadas Mediante' + CR_LF +
                                   'La Ejecuci�n De Un Archivo Batch En Computadoras' + CR_LF +
                                   'Que Corren Windows NT' + CR_LF + CR_LF +
                                   'Tambi�n Se Puede Utilizar el Interbase Server Manager' + CR_LF + CR_LF +
                                   '� Desea Generar El Archivo Batch ?', 0, mbYes ) then
          begin
               GenerarBatch.Click;
          end;
     end
     else
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             sStatsFile := GetStatsFile;
             if FileExists( sStatsFile ) then
                DeleteFile( sStatsFile );
             if DInterbase.Ejecutar( GetProgram, GetParameters, True, 1 ) then
             begin
                  if FileExists( sStatsFile ) then
                  begin
                       if ZetaDialogo.zConfirm( Caption,
                                                'Las Estad�sticas De La Base De Datos' +
                                                CR_LF +
                                                'Fueron Creadas En El Archivo' +
                                                CR_LF +
                                                CR_LF +
                                                sStatsFile +
                                                CR_LF +
                                                CR_LF +
                                                '� Desea Verlas ?', 0, mbYes ) then
                       begin
                            DInterbase.CallNotePad( sStatsFile );
                       end;
                  end
                  else
                      ZetaDialogo.zError( Caption, 'El Archivo De Estad�sticas ' + sStatsFile + ' No Fu� Creado', 0 );
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TIBStats.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
