unit FDBCopier;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Db, ComCtrls,ShellApi,
     DInterbase;

type
  TDBCopier = class(TForm)
    DatabaseFind: TOpenDialog;
    TargetGB: TGroupBox;
    TargetComputerNameLBL: TLabel;
    TargetUserNameLBL: TLabel;
    TargetPasswordLBL: TLabel;
    TargetLBL: TLabel;
    SeekTargetDB: TSpeedButton;
    TargetComputerName: TEdit;
    TargetUserName: TEdit;
    TargetPassword: TEdit;
    TargetDB: TEdit;
    TargetConnect: TBitBtn;
    Copiar: TBitBtn;
    StatusBar: TStatusBar;
    Salir: TBitBtn;
    TargetDisconnect: TBitBtn;
    Interrumpir: TBitBtn;
    Opciones: TGroupBox;
    BorrarTablas: TCheckBox;
    Verificar: TCheckBox;
    DontStopOnError: TCheckBox;
    SourceGB: TGroupBox;
    SourceComputerNameLBL: TLabel;
    SourceUserNameLBL: TLabel;
    SourcePasswordLBL: TLabel;
    SourceLBL: TLabel;
    SourceComputerName: TEdit;
    SourceUserName: TEdit;
    SourcePassword: TEdit;
    SourceDB: TEdit;
    TableList: TBitBtn;
    SaveDialog: TSaveDialog;
    CheckNulls: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure SeekTargetDBClick(Sender: TObject);
    procedure TargetConnectClick(Sender: TObject);
    procedure TargetDisconnectClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure CopiarClick(Sender: TObject);
    procedure InterrumpirClick(Sender: TObject);
    procedure TableListClick(Sender: TObject);
  private
    { Private declarations }
    FContinue: Boolean;
    FSiATodos: Boolean;
    FPassword: String;
    FUserName: String;
    FComputerName: String;
    FDatabaseFile: String;
    function ShowProgress( const Mensaje: String; const Valor: Integer; var HayError: Boolean ): Boolean;
    procedure TableProgress( const Mensaje: String );
    procedure SetControls;
    procedure SetSourceControls(const lConnected: Boolean);
    procedure SetTargetControls(const lConnected: Boolean);
  public
    { Public declarations }
    property ComputerName: String read FComputerName write FComputerName;
    property DatabaseFile: String read FDatabaseFile write FDatabaseFile;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
  end;

var
  DBCopier: TDBCopier;

implementation

uses ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

{ ********** TDBCopier *********** }

procedure TDBCopier.FormShow(Sender: TObject);
begin
     SourceComputerName.Text := ComputerName;
     SourceUserName.Text := UserName;
     SourcePassword.Text := Password;
     SourceDB.Text := DatabaseFile;
     TargetComputerName.Text := ZetaWinAPITools.GetComputerName;
     TargetUserName.Text := UserName;
     TargetPassword.Text := Password;
     TargetDB.Text := ExtractFilePath( DatabaseFile );
     SetSourceControls( True );
     SetTargetControls( False );
end;

procedure TDBCopier.SeekTargetDBClick(Sender: TObject);
begin
     with TargetDB do
     begin
          with DatabaseFind do
          begin
               Title := 'Seleccione La Base De Datos Destino';
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TDBCopier.TargetConnectClick(Sender: TObject);
begin
     SetTargetControls( dmInterbase.ConnectTarget( TargetUserName.Text,
                                                   TargetPassword.Text,
                                                   TargetComputerName.Text + ':' + TargetDB.Text ) );
end;

procedure TDBCopier.TargetDisconnectClick(Sender: TObject);
begin
     SetTargetControls( dmInterbase.DisconnectTarget );
end;

procedure TDBCopier.SetSourceControls( const lConnected: Boolean );
begin
     TableList.Enabled := lConnected;
     SetControls;
end;

procedure TDBCopier.SetTargetControls( const lConnected: Boolean );
begin
     TargetConnect.Enabled := not lConnected;
     TargetDisconnect.Enabled := lConnected;
     TargetComputerNameLBL.Enabled := not lConnected;
     TargetComputerName.Enabled := not lConnected;
     TargetUserName.Enabled := not lConnected;
     TargetUserNameLBL.Enabled := not lConnected;
     TargetPasswordLBL.Enabled := not lConnected;
     TargetPassword.Enabled := not lConnected;
     TargetDB.Enabled := not lConnected;
     TargetLBL.Enabled := not lConnected;
     SeekTargetDB.Enabled := not lConnected;
     SetControls;
end;

procedure TDBCopier.SetControls;
begin
     with dmInterbase do
     begin
          with Self.Copiar do
          begin
               Enabled := IBSourceDB.Connected and IBTargetDB.Connected;
               Opciones.Enabled := Enabled;
               BorrarTablas.Enabled := Enabled;
               Verificar.Enabled := Enabled;
               DontStopOnError.Enabled := Enabled;
               CheckNulls.Enabled := Enabled;
          end;
     end;
end;

function TDBCopier.ShowProgress( const Mensaje: String; const Valor: Integer; var HayError: Boolean ): Boolean;
begin
     with StatusBar.Panels[ 0 ] do
     begin
          if ( Valor > 0 ) then
             Text := Mensaje + ' # ' + Trim( Format( '%15.0n', [ Valor / 1 ] ) )
          else
              Text := Mensaje;
     end;
     Application.ProcessMessages;
     if FContinue then
     begin
          Result := True;
          if HayError and not FSiATodos then
          begin
               case ZetaDialogo.zYesToAll( Caption, '¡ Se Encontraron Errores ! ¿ Desea Continuar ? ', 0, mbYes ) of
                    mrYes: Result := True;
                    mrNo: Result := False;
               else
                   FSiATodos := True;
               end;
          end;
          if Result then
             HayError := False;
     end
     else
         Result := False;
end;

procedure TDBCopier.TableProgress( const Mensaje: String );
begin
     with StatusBar.Panels[ 1 ] do
     begin
          Text := Mensaje;
     end;
end;

procedure TDBCopier.TableListClick(Sender: TObject);
var
   sFileName: String;
begin
     with SaveDialog do
     begin
          with SourceDB do
          begin
               sFileName := ExtractFilePath( Text ) + 'TablasDe' + ExtractFileName( Text );
               sFileName := ZetaCommonTools.StrTransform( sFileName, ExtractFileExt( sFileName ), '.txt' );
          end;
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          Title := 'Escoja El Archivo Donde Guardar La Lista';
          if Execute then
          begin
               sFileName := FileName;
               if dmInterbase.GetDatabaseTableList( sFileName ) then
               begin
                    if ZetaDialogo.zConfirm( Caption, 'La Lista De Tablas De Esta Base De Datos' +
                                                      CR_LF +
                                                      'Ha Sido Creada En El Archivo' +
                                                      CR_LF +
                                                      CR_LF +
                                                      sFileName +
                                                      CR_LF +
                                                      CR_LF +
                                                      '¿ Desea Verlo ?', 0, mbYes ) then
                    begin
                         DInterbase.CallNotePad( FileName );
                    end;
               end;
          end;
     end;
end;

procedure TDBCopier.CopiarClick(Sender: TObject);
var
   FBitacora: TStrings;
   sLogFileName: String;

procedure SetButtons( const lEnabled: Boolean );
begin
     Interrumpir.Enabled := not lEnabled;
     Copiar.Enabled := lEnabled;          
     Salir.Enabled := lEnabled;
     Opciones.Enabled := lEnabled;
     TargetDisconnect.Enabled := lEnabled;
     TableList.Enabled := lEnabled;
end;

begin
     try
        SetButtons( False );
        FContinue := True;
        FSiATodos := DontStopOnError.Checked;
        FBitacora := TStringList.Create;
        try
           with dmInterbase do
           begin
                ChecaNulls := Self.CheckNulls.Checked;
                Copiar( ShowProgress, TableProgress, FBitacora, Verificar.Checked, BorrarTablas.Checked );
           end;
           with FBitacora do
           begin
                sLogFileName := ExtractFilePath( Application.ExeName ) + 'BDCopy.txt';
                SaveToFile( sLogFileName );
                if ZetaDialogo.zConfirm( Caption, '¿ Desea Ver La Bitácora Del Copiado ?', 0, mbYes ) then
                begin
                     CallNotePad( sLogFileName );
                end;
           end;
        finally
               FBitacora.Free;
        end;
     finally
            SetButtons( True );
     end;
end;

procedure TDBCopier.InterrumpirClick(Sender: TObject);
begin
     if ZetaDialogo.zConfirm( Caption, '¿ Desea Interrumpir El Copiado ?', 0, mbNo ) then
        FContinue := False;
end;

procedure TDBCopier.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
