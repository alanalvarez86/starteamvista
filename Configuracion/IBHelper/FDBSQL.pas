unit FDBSQL;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, DB, DBTables, ComCtrls, ExtCtrls,
     Mask, ImgList;

type
  TSQLTools = class(TForm)
    Panel: TPanel;
    Salir: TBitBtn;
    SmallIconList: TImageList;
    Companias: TListView;
    RunStatements: TBitBtn;
    Scripts: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RunStatementsClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure ScriptsClick(Sender: TObject);
  private
    { Private declarations }
    function ConectaEmpresa: Boolean;
    function GetEmpresa: Integer;
  public
    { Public declarations }
  end;

var
  SQLTools: TSQLTools;

implementation

uses DInterbase,
     ZetaDialogo,
     ZetaCommonTools;

{$R *.DFM}

{ ********* TSQLTools *********** }

procedure TSQLTools.FormCreate(Sender: TObject);
begin
     Tag := 0;
end;

procedure TSQLTools.FormShow(Sender: TObject);
var
   i: Integer;
   ListItem: TListItem;
begin
     with dmInterbase do
     begin
          if ConectarComparte then
          begin
               for i := 0 to ( EmpresasCount - 1 ) do
               begin
                    with Companias.Items do
                    begin
                         ListItem := Add;
                         with ListItem do
                         begin
                              StateIndex := i;
                              if Empresas[ i ].EsComparte then
                                 ImageIndex := 1
                              else
                                  ImageIndex := 0;
                         end;
                         Item[ ListItem.Index ].Caption := Empresas[ i ].Nombre;
                    end;
               end;
               with Companias do
               begin
                    if ( Selected = nil ) then
                       Selected := Items[ 0 ];
               end;
          end
          else
              Self.Close;
     end;
end;

procedure TSQLTools.FormDestroy(Sender: TObject);
begin
     Tag := 0;
end;

function TSQLTools.GetEmpresa: Integer;
begin
     Result := Companias.Selected.StateIndex;
end;

function TSQLTools.ConectaEmpresa: Boolean;
begin
     Result := False;
     case Companias.SelCount of
          0: ZetaDialogo.zInformation( '� Atenci�n !', 'Hace Falta Escoger Una Empresa', 0 );
          1: Result := True;
     else
         ZetaDialogo.zError( '� Atenci�n !', 'Hay M�s De Una Empresa Escogida', 0 );
     end;
end;

{ *********** Eventos ************* }

procedure TSQLTools.RunStatementsClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     RunStatements.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ConectaEmpresa then
        begin
             dmInterbase.InvokeDSQL( GetEmpresa );
        end;
     finally
            RunStatements.Enabled := True;
            Screen.Cursor := oCursor;
            Application.ProcessMessages;
     end;
     ActiveControl := Companias;
end;

procedure TSQLTools.ScriptsClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     Scripts.Enabled := False;
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ConectaEmpresa then
        begin
             dmInterbase.ExecuteScript( GetEmpresa );
        end;
     finally
            Scripts.Enabled := True;
            Screen.Cursor := oCursor;
            Application.ProcessMessages;
     end;
     ActiveControl := Companias;
end;

procedure TSQLTools.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
