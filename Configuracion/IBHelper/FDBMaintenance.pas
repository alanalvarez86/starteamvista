unit FDBMaintenance;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZetaASCIIFile, ComCtrls;

type
  TIBMaintenance = class(TForm)
    PanelBotones: TPanel;
    Salir: TBitBtn;
    Ejecutar: TBitBtn;
    GenerarBatch: TBitBtn;
    Operacion: TRadioGroup;
    SaveDialog: TSaveDialog;
    PanelOpciones: TPanel;
    IgnoreCheckSum: TCheckBox;
    ValidateModes: TRadioGroup;
    Defaults: TBitBtn;
    StatusBar: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure GenerarBatchClick(Sender: TObject);
    procedure DefaultsClick(Sender: TObject);
    procedure OperacionClick(Sender: TObject);
    procedure ValidateClick(Sender: TObject);
    procedure EjecutarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
  private
    { Private declarations }
    FAsciiLog: TAsciiLog;
    FComputerName: String;
    FDatabaseFile: String;
    FPassword: String;
    FUserName: String;
    FTodas: Boolean;
    function CreateCommand( const sDatabase, sUserName, sPassword: String ): String;
    function GetBatchFileName(var sFileName: String): Boolean;
    function GetParameters( const sDatabase, sUserName, sPassword: String ): String;
    function GetProgram: String;
    procedure InitControls;
    procedure SetControls;
    procedure ShowBatch(const sBatch: String);
    procedure ShowText(const sMensaje: String);
  public
    { Public declarations }
    property AsciiLog: TAsciiLog read FAsciiLog write FAsciiLog;
    property Todas: Boolean read FTodas write FTodas;
    property ComputerName: String read FComputerName write FComputerName;
    property DatabaseFile: String read FDatabaseFile write FDatabaseFile;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
    procedure SweepAll;
  end;

var
  IBMaintenance: TIBMaintenance;

implementation

uses ZetaDialogo,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     FDBRegistry,
     DInterbase;

{$R *.DFM}

const
     K_SWEEP = 0;
     K_VALIDATE = 1;
     K_MEND = 2;

procedure TIBMaintenance.FormShow(Sender: TObject);
begin
     if Todas then
     begin
          if dmInterbase.ConectarComparte then
          begin
               Caption := 'Mantenimiento A Todas Las Bases De Datos de Tress';
               InitControls;
          end
          else
              Close;
     end
     else
     begin
          InitControls;
          SetControls;
     end;
end;

procedure TIBMaintenance.InitControls;
begin
     with ZetaRegistry do
     begin
          Operacion.ItemIndex := FixMode;
          ValidateModes.ItemIndex := ValidateMode;
          IgnoreCheckSum.Checked := IgnoreCheckSumsFix;
     end;
end;

procedure TIBMaintenance.SetControls;
begin
     ValidateModes.Enabled := ( Operacion.ItemIndex = K_VALIDATE );
end;

procedure TIBMaintenance.OperacionClick(Sender: TObject);
begin
     SetControls;
end;

procedure TIBMaintenance.ValidateClick(Sender: TObject);
begin
     SetControls;
end;

procedure TIBMaintenance.ShowText( const sMensaje: String );
begin
     StatusBar.SimpleText := sMensaje;
end;

procedure TIBMaintenance.ShowBatch( const sBatch: String );
begin
     ShowText( Format( 'Archivo Batch %s Creado', [ sBatch ] ) );
     if ZetaDialogo.zConfirm( Caption, 'El Archivo BATCH Ha Sido Creado' +
                                       CR_LF +
                                       CR_LF +
                                       sBatch +
                                       CR_LF +
                                       CR_LF +
                                       '� Desea Verlo ?', 0, mbYes ) then
     begin
          DInterbase.CallNotePad( sBatch );
     end;
end;

function TIBMaintenance.GetBatchFileName( var sFileName: String ): Boolean;
begin
     with SaveDialog do
     begin
          Filter := 'Archivo Batch( *.bat )|*.bat|Archivo Texto ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione El Archivo Batch A Crear';
          FileName := ExtractFileName( sFileName );
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               sFileName := FileName;
               Result := True;
          end
          else
              Result := False;
     end;
end;

procedure TIBMaintenance.SweepAll;
var
   i: Integer;
   sPrograma: String;
begin
     try
        with FAsciiLog do
        begin
             WriteTimeStamp( '++++ Inicio De Sweep A Bases De Datos De Tress %s ++++' );
        end;
        if dmInterbase.ConectarComparte then
        begin
             InitControls;
             Operacion.ItemIndex := K_SWEEP;
             sPrograma := GetProgram;
             with dmInterbase do
             begin
                  for i := 0 to ( EmpresasCount - 1 ) do
                  begin
                       with Empresas[ i ] do
                       begin
                            try
                               if DInterbase.Ejecutar( sPrograma, GetParameters( Datos, UserName, Password ), False ) then
                                  FAsciiLog.WriteTimeStamp( Nombre + ' %s' );
                            except
                                  on Error: Exception do
                                  begin
                                       FAsciiLog.WriteException( 0, Format( 'Error En Sweep A Empresa %s', [ Nombre ] ), Error );
                                  end;
                            end;
                       end;
                  end;
             end;
        end;
        with FAsciiLog do
        begin
             WriteTimeStamp( '++++ Fin De Sweep A Bases De Datos De Tress %s ++++' );
        end;
     except
           on Error: Exception do
           begin
                FAsciiLog.WriteException( 0, 'Error En Sweep', Error );
           end;
     end;
end;

function TIBMaintenance.GetProgram: String;
const
     K_COMMAND = '%sgfix.exe';
begin
     Result := Format( K_COMMAND, [ DInterbase.GetServerDirectory ] );
end;

function TIBMaintenance.GetParameters( const sDatabase, sUserName, sPassword: String ): String;
const
     K_OPTIONS = '%s -z -user "%s" -password "%s"';
     K_PARAMETERS = '%s "%s"';
     aModes: array[ 0..1 ] of Char = ( 'f', 'n' );
begin
     case Operacion.ItemIndex of
          K_SWEEP: Result := '-sweep';
          K_VALIDATE: Result := Format( '-v -%s', [ aModes[ ValidateModes.ItemIndex ] ] );
          K_MEND: Result := '-m';
     end;
     if IgnoreCheckSum.Checked then
        Result := Result + ' -i';
     Result := Format( K_OPTIONS, [ Result, sUserName, sPassword ] );
     Result := Format( K_PARAMETERS, [ Result, sDatabase ] );
end;

function TIBMaintenance.CreateCommand( const sDatabase, sUserName, sPassword: String ): String;
const
     K_COMMAND = '"%s" %s';
begin
     Result := Format( K_COMMAND, [ GetProgram, GetParameters( sDatabase, sUserName, sPassword ) ] );
end;

procedure TIBMaintenance.DefaultsClick(Sender: TObject);
const
     K_MENSAJE = 'Esta Operaci�n Declara Como Defaults' + CR_LF +
                 'Los Par�metros Capturados En Esta Pantalla' + CR_LF +
                 CR_LF +
                 'Los Defaults Son Utilizados En El Proceso' + CR_LF +
                 'De Mantenimiento Autom�tico';
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', K_MENSAJE, 0, mbNo ) then
     begin
          with ZetaRegistry do
          begin
               FixMode := Operacion.ItemIndex;
               ValidateMode := ValidateModes.ItemIndex;
               IgnoreCheckSumsFix := IgnoreCheckSum.Checked;
          end;
     end;
end;

procedure TIBMaintenance.GenerarBatchClick(Sender: TObject);
var
   i: Integer;
   FDatos: TStrings;
   sBatch: String;
begin
     sBatch := ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) );
     case Operacion.ItemIndex of
          K_SWEEP: sBatch := sBatch + 'Sweep.bat';
          K_VALIDATE: sBatch := sBatch + 'Validate.bat';
          K_MEND: sBatch := sBatch + 'Mend.bat';
     end;
     if GetBatchFileName( sBatch ) then
     begin
          FDatos := TStringList.Create;
          try
             with FDatos do
             begin
                  if Todas then
                  begin
                       Add( '@echo off' );
                       with dmInterbase do
                       begin
                            for i := 0 to ( EmpresasCount - 1 ) do
                            begin
                                 with Empresas[ i ] do
                                 begin
                                      Add( Format( 'REM Mantenimiento De Empresa %s ( Alias %s )', [ Nombre, Alias ] ) );
                                      Add( CreateCommand( Datos, UserName, Password ) );
                                      Add( '' );
                                 end;
                            end;
                       end;
                  end
                  else
                  begin
                       Add( CreateCommand( ComputerName + ':' + DatabaseFile, UserName, Password ) );
                  end;
                  SaveToFile( sBatch );
             end;
          finally
                 FDatos.Free;
          end;
          ShowBatch( sBatch );
     end;
end;

procedure TIBMaintenance.EjecutarClick(Sender: TObject);
var
   i, iCtr: Integer;
   oCursor: TCursor;
   sPrograma, sDatabase: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if Todas then
        begin
             sPrograma := GetProgram;
             with dmInterbase do
             begin
                  iCtr := 0;
                  ShowText( Format( 'Iniciando Mantenimiento De %d Empresas', [ EmpresasCount ] ) );
                  for i := 0 to ( EmpresasCount - 1 ) do
                  begin
                       with Empresas[ i ] do
                       begin
                            sDatabase := Datos;
                            try
                               ShowText( Format( 'Manteniendo %s ( %s )', [ Nombre, sDatabase ] ) );
                               if DInterbase.Ejecutar( sPrograma, GetParameters( sDatabase, UserName, Password ), False ) then
                                  Inc( iCtr )
                               else
                                   ZetaDialogo.zError( '� Error En Mantenimiento !', Format( 'Mantenimiento De %s' + CR_LF + '%s' + CR_LF + 'No Tuvo Exito', [ Nombre, sDatabase ] ), 0 );
                            except
                                  on Error: Exception do
                                  begin
                                       Application.HandleException( Error );
                                  end;
                            end;
                       end;
                  end;
                  ShowText( Format( 'Mantenimiento De %d de %d Empresas Terminado', [ iCtr, EmpresasCount ] ) );
                  ZetaDialogo.zInformation( 'Operaci�n Terminada', Format( '%d de %d Empresas Fueron Procesadas', [ iCtr, EmpresasCount ] ), 0 );
             end;
        end
        else
        begin
             if DInterbase.Ejecutar( GetProgram, GetParameters( ComputerName + ':' + DatabaseFile, UserName, Password ), True ) then
                ZetaDialogo.zInformation( Caption, 'La Operaci�n De Mantenimiento Ha Terminado', 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TIBMaintenance.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
