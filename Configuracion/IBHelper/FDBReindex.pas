unit FDBReindex;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, DBTables, StdCtrls, Db, Grids, DBGrids, ExtCtrls, Buttons,
  ComCtrls, IB_Components, IB_Grid;

type
  TFormaReactiva = class(TForm)
    PanelInferior: TPanel;
    Mostrar: TRadioGroup;
    ReactivarUno: TBitBtn;
    ReactivarTodos: TBitBtn;
    SourceGB: TGroupBox;
    SourceComputerNameLBL: TLabel;
    SourceUserNameLBL: TLabel;
    SourcePasswordLBL: TLabel;
    SourceLBL: TLabel;
    SourceComputerName: TEdit;
    SourceUserName: TEdit;
    SourcePassword: TEdit;
    SourceDB: TEdit;
    Salir: TBitBtn;
    StatusBar: TStatusBar;
    dsIndices: TIB_DataSource;
    IBGrid: TIB_Grid;
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MostrarClick(Sender: TObject);
    procedure ReactivarUnoClick(Sender: TObject);
    procedure ReactivarTodosClick(Sender: TObject);
  private
    { Private declarations }
    FPassword: String;
    FUserName: String;
    FComputerName: String;
    FDatabaseFile: String;
    function HayInactivos: Boolean;
    procedure SetControls(const lConnected: Boolean);
  public
    { Public declarations }
    property ComputerName: String read FComputerName write FComputerName;
    property DatabaseFile: String read FDatabaseFile write FDatabaseFile;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
    function RefrescaIndices: Boolean;
  end;

var
  FormaReactiva: TFormaReactiva;

implementation

uses DInterbase,
     ZetaDialogo;

{$R *.DFM}

procedure TFormaReactiva.FormShow(Sender: TObject);
begin
     SourceComputerName.Text := ComputerName;
     SourceUserName.Text := UserName;
     SourcePassword.Text := Password;
     SourceDB.Text := DatabaseFile;
     SetControls( True );
end;

procedure TFormaReactiva.FormDestroy(Sender: TObject);
begin
     dmInterbase.DisconnectSource;
end;

function TFormaReactiva.HayInactivos: Boolean;
begin
     Result := not dmInterbase.tqSource.Eof;
end;

function TFormaReactiva.RefrescaIndices: Boolean;
var
   sError: String;
begin
     Result := dmInterbase.RefrescaIndices( Mostrar.ItemIndex, sError );
     if Result then
        SetControls( True )
     else
         ZetaDialogo.ZError( Caption, sError, 0 );
end;

procedure TFormaReactiva.SetControls( const lConnected: Boolean );
begin
     Mostrar.Enabled := lConnected;
     with dsIndices do
     begin
          if lConnected then
             Dataset := dmInterbase.tqQuery
          else
              Dataset := nil;
     end;
     with ReactivarUno do
     begin
          Enabled := lConnected and HayInactivos;
          ReactivarTodos.Enabled := Enabled;
     end;
     with StatusBar do
     begin
          if HayInactivos then
             SimpleText := ''
          else
              SimpleText := 'No Hay Indices Inactivos';
     end;
end;

procedure TFormaReactiva.MostrarClick(Sender: TObject);
begin
     RefrescaIndices;
end;

procedure TFormaReactiva.ReactivarUnoClick(Sender: TObject);
begin
     dmInterbase.ReactivaUno;
     RefrescaIndices;
end;

procedure TFormaReactiva.ReactivarTodosClick(Sender: TObject);
begin
     dmInterbase.ReactivaTodos;
     RefrescaIndices;
end;

end.
