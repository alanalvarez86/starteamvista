object dmInterbase: TdmInterbase
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 677
  object IBSourceDB: TIB_Database
    Left = 48
    Top = 24
  end
  object IBTargetDB: TIB_Database
    Left = 48
    Top = 80
  end
  object tqQuery: TIB_Query
    IB_Connection = IBSourceDB
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    ReadOnly = True
    BufferSynchroFlags = []
    FetchWholeRows = True
    Left = 256
    Top = 24
  end
  object stTarget: TIB_DSQL
    IB_Connection = IBTargetDB
    Left = 128
    Top = 80
  end
  object tqSource: TIB_Cursor
    IB_Connection = IBSourceDB
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    ReadOnly = True
    Left = 128
    Top = 24
  end
  object tqSource2: TIB_Cursor
    IB_Connection = IBSourceDB
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    ReadOnly = True
    Left = 192
    Top = 24
  end
  object tqTarget: TIB_Cursor
    IB_Connection = IBTargetDB
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    ReadOnly = True
    Left = 192
    Top = 80
  end
  object stSource: TIB_DSQL
    IB_Connection = IBSourceDB
    Left = 320
    Top = 24
  end
  object IB_DSQL: TIB_DSQLDialog
    Caption = 'Execute DDL Statements'
    BaseConnection = IBSourceDB
    Params = False
    Left = 384
    Top = 24
  end
  object IB_Script: TIB_ScriptDialog
    Caption = 'Execute Interbase SQL Script'
    BaseConnection = IBSourceDB
    Left = 384
    Top = 80
  end
end
