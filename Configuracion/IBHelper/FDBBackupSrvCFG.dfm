object IBBackupSrvCFG: TIBBackupSrvCFG
  Left = 192
  Top = 106
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Configurar Servicio De Respaldo'
  ClientHeight = 296
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBotones: TPanel
    Left = 0
    Top = 265
    Width = 350
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Cancelar: TBitBtn
      Left = 273
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Salir De Esta Ventana Sin Escribir Configuraci�n'
      Anchors = [akTop, akRight]
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = CancelarClick
      Kind = bkCancel
    end
    object OK: TBitBtn
      Left = 187
      Top = 4
      Width = 81
      Height = 25
      Hint = 'Escribir Configuraci�n Y Salir'
      Anchors = [akTop, akRight]
      Caption = '&OK'
      Default = True
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = OKClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object BackupGB: TGroupBox
    Left = 0
    Top = 0
    Width = 350
    Height = 141
    Align = alTop
    Caption = ' Respaldo Autom�tico De Todas Las Bases De Datos '
    TabOrder = 1
    object BkupDaysLBL: TLabel
      Left = 10
      Top = 19
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = '&Efectuar En:'
      FocusControl = BkupDays
    end
    object BkupTimeLBL: TLabel
      Left = 167
      Top = 19
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = '&A Las:'
      FocusControl = BkupTime
    end
    object BkupFolderLBL: TLabel
      Left = 9
      Top = 118
      Width = 60
      Height = 13
      Alignment = taRightJustify
      Caption = 'Al Directorio:'
    end
    object SeekBkupFolder: TSpeedButton
      Left = 322
      Top = 114
      Width = 23
      Height = 22
      Hint = 'Buscar Directorio De Respaldo'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SeekBkupFolderClick
    end
    object BkupDays: TCheckListBox
      Left = 72
      Top = 16
      Width = 84
      Height = 97
      ItemHeight = 13
      Items.Strings = (
        'Domingo'
        'Lunes'
        'Martes'
        'Mi�rcoles'
        'Jueves'
        'Viernes'
        'S�bado')
      TabOrder = 0
    end
    object BkupTime: TZetaHora
      Left = 200
      Top = 16
      Width = 40
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 1
      Tope = 24
    end
    object BkupFolder: TEdit
      Left = 72
      Top = 114
      Width = 246
      Height = 21
      TabOrder = 2
    end
  end
  object SweepGB: TGroupBox
    Left = 0
    Top = 141
    Width = 350
    Height = 124
    Align = alClient
    Caption = ' Sweep  Autom�tico De Todas Las Bases De Datos '
    TabOrder = 2
    object SweepDaysLBL: TLabel
      Left = 6
      Top = 21
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'E&fectuar En:'
      FocusControl = SweepDays
    end
    object SweepTimeLBL: TLabel
      Left = 163
      Top = 21
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'A &Las:'
      FocusControl = SweepTime
    end
    object SweepDays: TCheckListBox
      Left = 68
      Top = 16
      Width = 84
      Height = 97
      ItemHeight = 13
      Items.Strings = (
        'Domingo'
        'Lunes'
        'Martes'
        'Mi�rcoles'
        'Jueves'
        'Viernes'
        'S�bado')
      TabOrder = 0
    end
    object SweepTime: TZetaHora
      Left = 196
      Top = 16
      Width = 40
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 1
      Tope = 24
    end
  end
end
