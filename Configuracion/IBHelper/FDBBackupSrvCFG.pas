unit FDBBackupSrvCFG;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, FileCtrl,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, CheckLst, Mask,
     ZetaHora;

type
  TIBBackupSrvCFG = class(TForm)
    PanelBotones: TPanel;
    Cancelar: TBitBtn;
    OK: TBitBtn;
    BackupGB: TGroupBox;
    SweepGB: TGroupBox;
    BkupDays: TCheckListBox;
    BkupDaysLBL: TLabel;
    SweepDaysLBL: TLabel;
    SweepDays: TCheckListBox;
    BkupTimeLBL: TLabel;
    BkupTime: TZetaHora;
    SweepTimeLBL: TLabel;
    SweepTime: TZetaHora;
    BkupFolderLBL: TLabel;
    BkupFolder: TEdit;
    SeekBkupFolder: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure SeekBkupFolderClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  IBBackupSrvCFG: TIBBackupSrvCFG;

implementation

uses FDBRegistry;

{$R *.DFM}

procedure TIBBackupSrvCFG.FormShow(Sender: TObject);
var
   i: Integer;
begin
     with ZetaRegistry do
     begin
          BkupTime.Valor := BackupTime;
          BkupFolder.Text := BackupFolder;
          Self.SweepTime.Valor := SweepTime;
          for i := 0 to 6 do
          begin
               BkupDays.Checked[ i ] := BackupDay[ i ];
               SweepDays.Checked[ i ] := SweepDay[ i ];
          end;
     end;
end;

procedure TIBBackupSrvCFG.SeekBkupFolderClick(Sender: TObject);
var
   sFolder: String;
begin
     with BkupFolder do
     begin
          sFolder := Text;
          if FileCtrl.SelectDirectory( sFolder, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
             Text := sFolder;
     end;
end;

procedure TIBBackupSrvCFG.OKClick(Sender: TObject);
var
   oCursor: TCursor;
   i: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with ZetaRegistry do
        begin
             BackupTime := BkupTime.Valor;
             BackupFolder := BkupFolder.Text;
             SweepTime := Self.SweepTime.Valor;
             for i := 0 to 6 do
             begin
                  BackupDay[ i ] := BkupDays.Checked[ i ];
                  SweepDay[ i ] := SweepDays.Checked[ i ];
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TIBBackupSrvCFG.CancelarClick(Sender: TObject);
begin
     Close;
end;

end.
