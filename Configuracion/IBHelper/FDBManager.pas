unit FDBManager;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Menus, ComCtrls, ExtCtrls, ActnList, ImgList,
     ZetaSmartLists,
     ZetaASCIIFile;

type
  TIBManager = class(TForm)
    SourceGB: TGroupBox;
    SourceComputerNameLBL: TLabel;
    SourceUserNameLBL: TLabel;
    SourcePasswordLBL: TLabel;
    SourceLBL: TLabel;
    SeekSourceDB: TSpeedButton;
    SourceComputerName: TEdit;
    SourceUserName: TEdit;
    SourcePassword: TEdit;
    SourceDB: TEdit;
    SourceConnect: TBitBtn;
    DatabaseFind: TOpenDialog;
    PageControl: TPageControl;
    DBInfo: TTabSheet;
    DBUsers: TTabSheet;
    DBMainInfo: TMemo;
    DBUserInfo: TMemo;
    StatusBar: TStatusBar;
    SourceDisconnect: TBitBtn;
    MainMenu: TMainMenu;
    Archivo: TMenuItem;
    Operaciones: TMenuItem;
    ArchivoConectar: TMenuItem;
    ArchivoDesconectar: TMenuItem;
    N1: TMenuItem;
    ArchivoSalir: TMenuItem;
    OperacionesRespaldar: TMenuItem;
    OperacionesRestaurar: TMenuItem;
    N2: TMenuItem;
    OperacionesMantenimiento: TMenuItem;
    OperacionesConfigurar: TMenuItem;
    N3: TMenuItem;
    OperacionesCopiar: TMenuItem;
    SystemInfo: TTabSheet;
    SysInfo: TMemo;
    PanelBotones: TPanel;
    N4: TMenuItem;
    ArchivoGrabarDatos: TMenuItem;
    SaveDialog: TSaveDialog;
    ActionList: TActionList;
    ImageList: TImageList;
    _ArchivoConectar: TAction;
    _ArchivoDesconectar: TAction;
    _ArchivoGrabarDatos: TAction;
    _ArchivoSalir: TAction;
    _OperacionesRespaldar: TAction;
    _OperacionesRestaurar: TAction;
    _OperacionesMantenimiento: TAction;
    _OperacionesConfigurar: TAction;
    _OperacionesCopiar: TAction;
    _AyudaContenido: TAction;
    Ayuda: TMenuItem;
    AyudaContenido: TMenuItem;
    _AyudaAcercaDe: TAction;
    AyudaAcercaDe: TMenuItem;
    _OperacionesEstadistica: TAction;
    OperacionesEstadistica: TMenuItem;
    _OperacionesReactivar: TAction;
    OperacionesReindexar: TMenuItem;
    Tress: TMenuItem;
    TressDBLookup: TMenuItem;
    _TressBkupAll: TAction;
    TressRespaldo: TMenuItem;
    _TressSweepAll: TAction;
    N5: TMenuItem;
    TressConfigurarServicioDeRespaldo: TMenuItem;
    _TressLookup: TAction;
    TressLookup: TBitBtn;
    PanelSalir: TPanel;
    Salir: TBitBtn;
    PanelControles: TPanel;
    Grabar: TZetaSpeedButton;
    Respaldar: TZetaSpeedButton;
    Restaurar: TZetaSpeedButton;
    Copiar: TZetaSpeedButton;
    Reactivar: TZetaSpeedButton;
    Estadisticas: TZetaSpeedButton;
    PanelTipo: TPanel;
    _ArchivoInterbase: TAction;
    _ArchivoFirebird: TAction;
    N6: TMenuItem;
    ArchivoInterbase: TMenuItem;
    ArchivoFirebird: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SeekSourceDBClick(Sender: TObject);
    procedure _ArchivoConectarExecute(Sender: TObject);
    procedure _ArchivoDesconectarExecute(Sender: TObject);
    procedure _ArchivoGrabarDatosExecute(Sender: TObject);
    procedure _ArchivoSalirExecute(Sender: TObject);
    procedure _OperacionesRespaldarExecute(Sender: TObject);
    procedure _OperacionesRestaurarExecute(Sender: TObject);
    procedure _OperacionesMantenimientoExecute(Sender: TObject);
    procedure _OperacionesConfigurarExecute(Sender: TObject);
    procedure _OperacionesCopiarExecute(Sender: TObject);
    procedure _OperacionesEstadisticaExecute(Sender: TObject);
    procedure _OperacionesReactivarExecute(Sender: TObject);
    procedure _AyudaContenidoExecute(Sender: TObject);
    procedure _AyudaAcercaDeExecute(Sender: TObject);
    procedure _TressBkupAllExecute(Sender: TObject);
    procedure _TressSweepAllExecute(Sender: TObject);
    procedure _TressLookupExecute(Sender: TObject);
    procedure _ArchivoInterbaseExecute(Sender: TObject);
    procedure _ArchivoFirebirdExecute(Sender: TObject);
  private
    { Private declarations }
    FBatch: Boolean;
    FAsciiLog: TAsciiLog;
    function ValidateShow: Boolean;
    procedure DisplayHint(Sender: TObject);
    procedure ManejaError(Sender: TObject; E: Exception);
    procedure SetControls(const lConnected: Boolean);
    procedure SetControlsServerType;
    procedure InitBatch;
  public
    { Public declarations }
    property ModoBatch: Boolean read FBatch;
    procedure BackupAll;
    procedure SweepAll;
  end;

var
  IBManager: TIBManager;

implementation

uses ZetaWinAPITools,
     ZetaAliasManager,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     DInterbase,
     FDBRestore,
     FDBConfig,
     FDBStats,
     FDBMaintenance,
     FDBRegistry,
     FDBBackup,
     FDBReindex,
     FDBCopier,
     FDBTressLookup;

{$R *.DFM}

function GetApplicationVersionNumber: String;
type
    VersionInfo = record
      MajorVersion: Word;
      MinorVersion: Word;
      Release: Word;
      Build: Word;
    end;

function HighWord( Value: DWord ): Word;
begin
     Result := ( ( Value and $FFFF0000 ) shr $10 );
end;

function LowWord( Value: DWord ): Word;
begin
     Result := ( Value and $0000FFFF );
end;

function GetVersionInfo: VersionInfo;
var
   Size, Value: DWord;
   pFileName: PChar;
   sFileName: String;
   Data, Info: Pointer;
   iLen: UINT;
begin
     sFileName := Application.ExeName;
     pFileName := StrAlloc( Length( sFileName ) + 1 );
     try
        pFileName := StrPCopy( pFileName, sFileName );
        Size := Windows.GetFileVersionInfoSize( pFileName, Value );
        if ( Size > 0 ) then
        begin
             GetMem( Data, Size );
             try
                if Windows.GetFileVersionInfo( pFileName, Value, Size, Data ) then
                begin
                     if Windows.VerQueryValue( Data, '\', Info, iLen ) then
                     begin
                          with Result, PVSFixedFileInfo( Info )^ do
                          begin
	                       MajorVersion := HighWord( dwFileVersionMS );
                               MinorVersion := LowWord( dwFileVersionMS );
                               Release := HighWord( dwFileVersionLS );
                               Build := LowWord( dwFileVersionLS );
                          end;
                     end;
                end;
             finally
                    FreeMem( Data );
             end;
        end;
     finally
            StrDispose( pFileName );
     end;
end;

begin
     with GetVersionInfo do
     begin
          Result := 'Versi�n ' +
                    IntToStr( MajorVersion ) +
                    '.' +
                    IntToStr( MinorVersion ) +
                    '.' +
                    IntToStr( Release );
     end;
end;

procedure TIBManager.FormCreate(Sender: TObject);
begin
     FBatch := False;
     ZetaCommonTools.Environment;
     FAsciiLog := TAsciiLog.Create;
     with Application do
     begin
          OnException := ManejaError;
          OnHint := DisplayHint;
          UpdateFormatSettings := FALSE;
     end;
     dmInterbase := TdmInterbase.Create( Self );
     ZetaRegistry := TZetaIBRegistry.Create;
end;

procedure TIBManager.FormShow(Sender: TObject);
begin
     with SourceComputerName do
     begin
          Text := ZetaWinAPITools.GetComputerName;
     end;
     ZetaWinAPITools.WriteSystemInfo( SysInfo.Lines );
     with ZetaRegistry do
     begin
          SourceDB.Text := DatabaseFile;
          SourcePassword.Text := Password;
          SourceUserName.Text := UserName;
     end;
     SetControls( False );
end;

procedure TIBManager.FormDestroy(Sender: TObject);
begin
     ZetaRegistry.Free;
     dmInterbase.Free;
     FAsciiLog.Free;
end;

procedure TIBManager.DisplayHint(Sender: TObject);
begin
     if FBatch then
     else
     begin
          StatusBar.Panels[ 1 ].Text := GetLongHint( Application.Hint );
     end;
end;

procedure TIBManager.ManejaError(Sender: TObject; E: Exception );
begin
     if FBatch then
     begin
          FAsciiLog.WriteException( 0, 'Se Encontr� Un Error', E );
     end
     else
     begin
          ZetaDialogo.ZExcepcion( '� Atenci�n !', 'Se Encontr� Un Error', E, 0 );
     end;
end;

procedure TIBManager.InitBatch;
begin
     FBatch := True;
     with FAsciiLog do
     begin
          if not Used then
             Init( ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ) + 'IBHelper.log' );
     end;
end;

procedure TIBManager.SetControls( const lConnected: Boolean );
begin
     SourceComputerNameLBL.Enabled := not lConnected;
     SourceComputerName.Enabled := not lConnected;
     SourceUserName.Enabled := not lConnected;
     SourceUserNameLBL.Enabled := not lConnected;
     SourcePasswordLBL.Enabled := not lConnected;
     SourcePassword.Enabled := not lConnected;
     SourceDB.Enabled := not lConnected;
     SourceLBL.Enabled := not lConnected;
     SeekSourceDB.Enabled := not lConnected;
     _ArchivoConectar.Enabled := not lConnected;
     _ArchivoDesconectar.Enabled := lConnected;
     _ArchivoGrabarDatos.Enabled := lConnected;
     _OperacionesRestaurar.Enabled := not lConnected;
     _OperacionesCopiar.Enabled := lConnected;
     _OperacionesReactivar.Enabled := lConnected;
     _OperacionesConfigurar.Enabled := lConnected;
     Tress.Enabled := not lConnected;
     _TressBkupAll.Enabled := not lConnected;
     _TressSweepAll.Enabled := not lConnected;
     _TressLookup.Enabled := not lConnected;
     if lConnected then
     begin
          StatusBar.Panels[ 0 ].Text := 'Base de Datos Conectada';
          with dmInterbase do
          begin
               GetDatabaseInfo( DBMainInfo.Lines );
               GetUserInfo( DBUserInfo.Lines );
          end;
     end
     else
     begin
          StatusBar.Panels[ 0 ].Text := 'No Hay Conexi�n A Una Base de Datos';
          DBMainInfo.Lines.Clear;
          DBUserInfo.Lines.Clear;
     end;
     SetControlsServerType;
end;

procedure TIBManager.SetControlsServerType;
const
     K_CAPTION = 'Mantenimiento de %s';
begin
     case DInterbase.GetServerType of
          stInterbase:
          begin
               Self.Caption := Format( K_CAPTION, [ 'Interbase' ] );
               _ArchivoInterbase.Checked := True;
               _ArchivoFirebird.Checked := False;
          end;
          stFirebird:
          begin
               Self.Caption := Format( K_CAPTION, [ 'Firebird' ] );
               _ArchivoInterbase.Checked := False;
               _ArchivoFirebird.Checked := True;
          end;
     end;
end;

function TIBManager.ValidateShow: Boolean;
begin
     Result := False;
     if ZetaCommonTools.StrVacio( SourceComputerName.Text ) then
        ZetaDialogo.zError( Caption, '� Computadora No Fu� Especificada !', 0 )
     else
         if ZetaCommonTools.StrVacio( SourceUserName.Text ) then
            ZetaDialogo.zError( Caption, '� Usuario No Fu� Especificado !', 0 )
         else
             if ZetaCommonTools.StrVacio( SourcePassword.Text ) then
                ZetaDialogo.zError( Caption, '� Clave De Acceso No Fu� Especificada !', 0 )
             else
                 if ZetaCommonTools.StrVacio( SourceDB.Text ) then
                    ZetaDialogo.zError( Caption, '� Archivo De Base De Datos No Fu� Especificado !', 0 )
                 else
                     Result := True;
end;

procedure TIBManager.SeekSourceDBClick(Sender: TObject);
begin
     with SourceDB do
     begin
          with DatabaseFind do
          begin
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TIBManager.BackupAll;
begin
     InitBatch;
     with TIBBackup.Create( Self ) do
     begin
          try
             AsciiLog := FAsciiLog;
             BackupAll;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager.SweepAll;
begin
     InitBatch;
     with TIBMaintenance.Create( Self ) do
     begin
          try
             AsciiLog := FAsciiLog;
             SweepAll;
          finally
                 Free;
          end;
     end;
end;

{ *********** Eventos del Men� *********** }

procedure TIBManager._ArchivoConectarExecute(Sender: TObject);
var
   sMensaje: string;
begin
     if dmInterbase.ConnectSource( SourceUserName.Text,
                                   SourcePassword.Text,
                                   SourceComputerName.Text + ':' + SourceDB.Text ) then
     begin
          SetControls( True );
          with ZetaRegistry do
          begin
               DatabaseFile := SourceDB.Text;
               Password := SourcePassword.Text;
               UserName := SourceUserName.Text;
          end;
          if dmInterbase.CheckWrongDatabaseStructure( sMensaje ) then
          begin
               ZetaDialogo.zWarning( '� Base De Datos Incorrecta !', sMensaje, 0, mbOk );
          end;
     end
     else
         SetControls( False );
end;

procedure TIBManager._ArchivoDesconectarExecute(Sender: TObject);
begin
     SetControls( dmInterbase.DisconnectSource );
end;

procedure TIBManager._ArchivoGrabarDatosExecute(Sender: TObject);
var
   FDatos: TStrings;
   sFileName: String;

procedure CopiaDatos( const sNombre: String; Lista: TStrings );
var
   i: Integer;
begin
     with FDatos do
     begin
          Add( '' );
          Add( StringOfChar( '>', 10 ) +  ' ' + sNombre + ' ' + StringOfChar( '<', 10 ) );
          Add( '' );
     end;
     with Lista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               if ZetaCommonTools.StrLleno( Strings[ i ] ) then
                  FDatos.Add( Strings[ i ] );
          end;
     end;
end;

begin
     with SaveDialog do
     begin
          with SourceDB do
          begin
               sFileName := ExtractFilePath( Text ) + 'Info' + ExtractFileName( Text );
               sFileName := ZetaCommonTools.StrTransform( sFileName, ExtractFileExt( sFileName ), '.txt' );
          end;
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          Title := 'Escoja El Archivo Donde Guardar Los Datos';
          if Execute then
          begin
               FDatos := TStringList.Create;
               try
                  with FDatos do
                  begin
                       Add( SourceComputerName.Text + ':' + SourceDB.Text );
                  end;
                  CopiaDatos( 'Datos De La Base De Datos', DBMainInfo.Lines );
                  CopiaDatos( 'Usuarios Conectados', DBUserInfo.Lines );
                  CopiaDatos( 'Datos del Sistema', SysInfo.Lines );
                  FDatos.SaveToFile( FileName );
               finally
                      FDatos.Free;
               end;
          end;
     end;
end;

procedure TIBManager._ArchivoSalirExecute(Sender: TObject);
begin
     Close;
end;

procedure TIBManager._OperacionesRespaldarExecute(Sender: TObject);
begin
     if ValidateShow then
     begin
          with TIBBackup.Create( Self ) do
          begin
               try
                  ComputerName := SourceComputerName.Text;
                  Password := SourcePassword.Text;
                  UserName := SourceUserName.Text;
                  DatabaseFile := SourceDB.Text;
                  ShowModal;
               finally
                      Free;
               end;
          end;
     end;
end;

procedure TIBManager._OperacionesRestaurarExecute(Sender: TObject);
begin
     with TIBRestore.Create( Self ) do
     begin
          try
             ComputerName := SourceComputerName.Text;
             Password := SourcePassword.Text;
             UserName := SourceUserName.Text;
             DatabaseFile := SourceDB.Text;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager._OperacionesMantenimientoExecute(Sender: TObject);
begin
     if ValidateShow then
     begin
          with TIBMaintenance.Create( Self ) do
          begin
               try
                  ComputerName := SourceComputerName.Text;
                  Password := SourcePassword.Text;
                  UserName := SourceUserName.Text;
                  DatabaseFile := SourceDB.Text;
                  ShowModal;
               finally
                      Free;
               end;
          end;
     end;
end;

procedure TIBManager._OperacionesConfigurarExecute(Sender: TObject);
begin
     if ValidateShow then
     begin
          with TIBConfig.Create( Self ) do
          begin
               try
                  ComputerName := SourceComputerName.Text;
                  Password := SourcePassword.Text;
                  UserName := SourceUserName.Text;
                  DatabaseFile := SourceDB.Text;
                  ShowModal;
               finally
                      Free;
               end;
          end;
     end;
end;

procedure TIBManager._OperacionesCopiarExecute(Sender: TObject);
begin
     with TDBCopier.Create( Self ) do
     begin
          try
             ComputerName := Self.SourceComputerName.Text;
             Password := Self.SourcePassword.Text;
             UserName := Self.SourceUserName.Text;
             DatabaseFile := Self.SourceDB.Text;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager._OperacionesReactivarExecute(Sender: TObject);
begin
     with TFormaReactiva.Create( Self ) do
     begin
          try
             ComputerName := Self.SourceComputerName.Text;
             Password := Self.SourcePassword.Text;
             UserName := Self.SourceUserName.Text;
             DatabaseFile := Self.SourceDB.Text;
             if RefrescaIndices then
                ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager._OperacionesEstadisticaExecute(Sender: TObject);
begin
     with TIBStats.Create( Self ) do
     begin
          try
             ComputerName := Self.SourceComputerName.Text;
             Password := Self.SourcePassword.Text;
             UserName := Self.SourceUserName.Text;
             DatabaseFile := Self.SourceDB.Text;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager._TressBkupAllExecute(Sender: TObject);
begin
     with TIBBackup.Create( Self ) do
     begin
          try
             Todas := True;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager._TressSweepAllExecute(Sender: TObject);
begin
     with TIBMaintenance.Create( Self ) do
     begin
          try
             Todas := True;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager._TressLookupExecute(Sender: TObject);
begin
     with TTressDBList.Create( Self ) do
     begin
          try
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  with Empresa do
                  begin
                       SourceComputerName.Text := ZetaAliasManager.GetDatabaseComputerName( Datos );
                       SourceDB.Text := ZetaAliasManager.GetDatabaseFileName( Datos );
                       SourceUserName.Text := UserName;
                       SourcePassword.Text := Password;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TIBManager._AyudaContenidoExecute(Sender: TObject);
begin
     Application.HelpCommand( HELP_FINDER, 0 );
end;

procedure TIBManager._AyudaAcercaDeExecute(Sender: TObject);
begin
     ZetaDialogo.zInformation( Caption,
                               'Auxiliar De Interbase ' + CR_LF +
                               GetApplicationVersionNumber + CR_LF +
                               'Grupo Tress Internacional, S.A. de C.V.' + CR_LF +
                               'http://www.tress.com.mx', 0 );
end;

procedure TIBManager._ArchivoInterbaseExecute(Sender: TObject);
begin
     DInterbase.SetServerType( stInterbase );
     SetControlsServerType;
end;

procedure TIBManager._ArchivoFirebirdExecute(Sender: TObject);
begin
     DInterbase.SetServerType( stFirebird );
     SetControlsServerType;
end;

end.
