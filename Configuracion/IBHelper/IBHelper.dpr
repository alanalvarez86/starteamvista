program IBHelper;

uses
  Forms,
  SysUtils,
  FDBManager in 'FDBManager.pas' {IBManager},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FDBTressLookup in 'FDBTressLookup.pas' {TressDBList};

{$R *.RES}
const
     K_SWITCHES = [ '-', '/' ];
var
   lBackup, lSweep: Boolean;
begin
  Application.Initialize;
  Application.HelpFile := 'Ibhelper.hlp>CONTEXT';
  Application.Title := 'Mantenimiento De Interbase';
  Application.CreateForm(TIBManager, IBManager);
  lBackup := FindCmdLineSwitch( 'BACKUP', K_SWITCHES, True ) or
             FindCmdLineSwitch( 'RESPALDAR', K_SWITCHES, True );
  lSweep :=  FindCmdLineSwitch( 'SWEEP', K_SWITCHES, True );
  if lBackup or lSweep then
  begin
       if lBackup then
       begin
            IBManager.BackupAll;
       end;
       if lSweep then
       begin
            IBManager.SweepAll;
       end;
  end
  else
      Application.Run;
end.
