unit FDBTressLookup;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, DB, DBTables, ComCtrls, ExtCtrls, Mask, ImgList,
     FDBRegistry,
     ZBaseDlgModal;

type
  TTressDBList = class(TZetaDlgModal)
    Companias: TListView;
    SmallIconList: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CompaniasDblClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function ConectaEmpresa: Boolean;
    function GetEmpresa: TEmpresaData;
  public
    { Public declarations }
    property Empresa: TEmpresaData read GetEmpresa;
  end;

var
  TressDBList: TTressDBList;

implementation

uses DInterbase,
     ZetaDialogo,
     ZetaCommonTools;

{$R *.DFM}

procedure TTressDBList.FormCreate(Sender: TObject);
begin
     inherited;
     Tag := 0;
end;

procedure TTressDBList.FormShow(Sender: TObject);
var
   i: Integer;
   ListItem: TListItem;
begin
     inherited;
     with dmInterbase do
     begin
          if ConectarComparte then
          begin
               for i := 0 to ( EmpresasCount - 1 ) do
               begin
                    with Companias.Items do
                    begin
                         ListItem := Add;
                         with ListItem do
                         begin
                              StateIndex := i;
                              if Empresas[ i ].EsComparte then
                                 ImageIndex := 1
                              else
                                  ImageIndex := 0;
                         end;
                         Item[ ListItem.Index ].Caption := Empresas[ i ].Nombre;
                    end;
               end;
               with Companias do
               begin
                    if ( Selected = nil ) then
                       Selected := Items[ 0 ];
               end;
          end
          else
              Self.Close;
     end;
end;

procedure TTressDBList.FormDestroy(Sender: TObject);
begin
     inherited;
     Tag := 0;
end;

function TTressDBList.ConectaEmpresa: Boolean;
begin
     Result := False;
     case Companias.SelCount of
          0: ZetaDialogo.zInformation( '� Atenci�n !', 'Hace Falta Escoger Una Empresa', 0 );
          1: Result := True;
     else
         ZetaDialogo.zError( '� Atenci�n !', 'Hay M�s De Una Empresa Escogida', 0 );
     end;
end;

function TTressDBList.GetEmpresa: TEmpresaData;
begin
     Result := dmInterbase.Empresas[ Companias.Selected.StateIndex ];
end;

procedure TTressDBList.CompaniasDblClick(Sender: TObject);
begin
     inherited;
     Ok.Click;
end;

procedure TTressDBList.OKClick(Sender: TObject);
begin
     inherited;
     if ConectaEmpresa then
        ModalResult := mrOK;
end;

end.
