unit FDBRegistry;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Registry;

type
  TEmpresaData = class(TObject)
  private
    { Private declarations }
    FOwner: TList;
    FAlias: String;
    FCodigo: String;
    FNombre: String;
    FPassword: String;
    FUserName: String;
    FDatos: String;
    FEsComparte: Boolean;
  public
    { Public declarations }
    constructor Create( Owner: TList; const sCodigo, sNombre, sAlias, sUserName, sPassword, sDatos: String; const lComparte: Boolean );
    destructor Destroy; override;
    property Alias: String read FAlias;
    property Codigo: String read FCodigo;
    property Nombre: String read FNombre;
    property Password: String read FPassword;
    property UserName: String read FUserName;
    property Datos: String read FDatos;
    property EsComparte: Boolean read FEsComparte;
  end;
  TEmpresaList = class( TObject )
  private
    { Private declarations }
    FEmpresas: TList;
    function GetEmpresas( Index: Integer ): TEmpresaData;
    function GetEmpresasCount: Integer;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Empresas[ Index: Integer ]: TEmpresaData read GetEmpresas;
    property EmpresasCount: Integer read GetEmpresasCount;
    procedure Add(const sCodigo, sNombre, sAlias, sUsuario, sPassword, sDatos: String; const lComparte: Boolean);
    procedure Clear;
  end;
  TZetaIBRegistry = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    function GetBackupFolder: String;
    function GetBuffers: Integer;
    function GetCommitAfterEachTable: Boolean;
    function GetDatabaseFile: String;
    function GetDisableGarbageCollection: Boolean;
    function GetFixMode: Integer;
    function GetIgnoreCheckSumsBkup: Boolean;
    function GetIgnoreCheckSumsFix: Boolean;
    function GetIgnoreLimboTx: Boolean;
    function GetIndexesInactive: Boolean;
    function GetMetadataOnly: Boolean;
    function GetPageSize: Integer;
    function GetPassword: String;
    function GetReplaceDatabase: Boolean;
    function GetSetDBBuffers: Boolean;
    function GetTransportable: Boolean;
    function GetUserName: String;
    function GetValidateMode: Integer;
    function GetVerboseOutput: Boolean;
    procedure SetBackupFolder(const Value: String);
    procedure SetBuffers(const Value: Integer);
    procedure SetCommitAfterEachTable(const Value: Boolean);
    procedure SetDatabaseFile(const Value: String);
    procedure SetDisableGarbageCollection(const Value: Boolean);
    procedure SetFixMode(const Value: Integer);
    procedure SetIgnoreCheckSumsBkup(const Value: Boolean);
    procedure SetIgnoreCheckSumsFix(const Value: Boolean);
    procedure SetIgnoreLimboTx(const Value: Boolean);
    procedure SetIndexesInactive(const Value: Boolean);
    procedure SetMetadataOnly(const Value: Boolean);
    procedure SetPageSize(const Value: Integer);
    procedure SetPassword(const Value: String);
    procedure SetReplaceDatabase(const Value: Boolean);
    procedure SetSetDBBuffers(const Value: Boolean);
    procedure SetTransportable(const Value: Boolean);
    procedure SetUserName(const Value: String);
    procedure SetValidateMode(const Value: Integer);
    procedure SetVerboseOutput(const Value: Boolean);
  protected
    { Protected declarations }
    property Registry: TRegIniFile read FRegistry;
  public
    { Public declarations }
    constructor Create; virtual;
    destructor Destroy; override;
    property BackupFolder: String read GetBackupFolder write SetBackupFolder;
    property Buffers: Integer read GetBuffers write SetBuffers;
    property CommitAfterEachTable: Boolean read GetCommitAfterEachTable write SetCommitAfterEachTable;
    property DatabaseFile: String read GetDatabaseFile write SetDatabaseFile;
    property DisableGarbageCollection: Boolean read GetDisableGarbageCollection write SetDisableGarbageCollection;
    property FixMode: Integer read GetFixMode write SetFixMode;
    property IgnoreCheckSumsBkup: Boolean read GetIgnoreCheckSumsBkup write SetIgnoreCheckSumsBkup;
    property IgnoreCheckSumsFix: Boolean read GetIgnoreCheckSumsFix write SetIgnoreCheckSumsFix;
    property IgnoreLimboTx: Boolean read GetIgnoreLimboTx write SetIgnoreLimboTx;
    property IndexesInactive: Boolean read GetIndexesInactive write SetIndexesInactive;
    property MetadataOnly: Boolean read GetMetadataOnly write SetMetadataOnly;
    property PageSize: Integer read GetPageSize write SetPageSize;
    property Password: String read GetPassword write SetPassword;
    property ReplaceDatabase: Boolean read GetReplaceDatabase write SetReplaceDatabase;
    property SetDBBuffers: Boolean read GetSetDBBuffers write SetSetDBBuffers;
    property Transportable: Boolean read GetTransportable write SetTransportable;
    property UserName: String read GetUserName write SetUserName;
    property ValidateMode: Integer read GetValidateMode write SetValidateMode;
    property VerboseOutput: Boolean read GetVerboseOutput write SetVerboseOutput;
  end;

var
   ZetaRegistry: TZetaIBRegistry;

implementation

const
     IB_REGISTRY_PATH = 'Software\Grupo Tress\Interbase';
     IB_BKUP_TRANSPORTABLE = 'BkupTransportable';
     IB_BKUP_METADATA = 'BkupMetadataOnly';
     IB_BKUP_GARBAGE = 'BkupDisableGarbageCollection';
     IB_BKUP_LIMBO = 'BkupIgnoreLimboTx';
     IB_BKUP_CHECKSUMS = 'BkupIgnoreCheckSums';
     IB_BKUP_VERBOSE = 'BkupVerboseOutput';
     IB_BKUP_FOLDER = 'BkupFolder';
     IB_DATABASE_FILE = 'DatabaseFile';
     IB_PASSWORD = 'Password';
     IB_USER_NAME = 'UserName';
     IB_FIX_CHECKSUMS = 'FixIgnoreCheckSums';
     IB_FIX_MODE = 'FixMode';
     IB_FIX_VALIDATE_MODE = 'FixValidateMode';
     IB_RESTORE_PAGE_SIZE = 'RestorePageSize';
     IB_RESTORE_SET_BUFFERS = 'RestoreSetBuffers';
     IB_RESTORE_BUFFERS = 'RestoreBuffers';
     IB_RESTORE_COMMIT = 'RestoreCommit';
     IB_RESTORE_INDEXES_INACTIVE = 'RestoreIndexInactive';
     IB_RESTORE_OVERWRITE = 'RestoreOverwrite';

{ ******** TEmpresaData ********* }

constructor TEmpresaData.Create( Owner: TList; const sCodigo, sNombre, sAlias, sUserName, sPassword, sDatos: String; const lComparte: Boolean );
begin
     FOwner := Owner;
     FCodigo := sCodigo;
     FNombre := sNombre;
     FAlias := sAlias;
     FPassword := sPassword;
     FUserName := sUserName;
     FDatos := sDatos;
     FEsComparte := lComparte;
     FOwner.Add( Self );
end;

destructor TEmpresaData.Destroy;
begin
     FOwner.Remove( Self );
     inherited Destroy;
end;

{ ************ TEmpresaList ****************** }

constructor TEmpresaList.Create;
begin
     FEmpresas := TList.Create;
end;

destructor TEmpresaList.Destroy;
begin
     Clear;
     FEmpresas.Free;
     inherited Destroy;
end;

function TEmpresaList.GetEmpresas(Index: Integer): TEmpresaData;
begin
     Result := TEmpresaData( FEmpresas.Items[ Index ] );
end;

function TEmpresaList.GetEmpresasCount: Integer;
begin
     Result := FEmpresas.Count;
end;

procedure TEmpresaList.Clear;
var
   i: Integer;
begin
     for i := ( EmpresasCount - 1 ) downto 0 do
     begin
          Empresas[ i ].Free;
     end;
end;

procedure TEmpresaList.Add( const sCodigo, sNombre, sAlias, sUsuario, sPassword, sDatos: String; const lComparte: Boolean );
begin
     TEmpresaData.Create( FEmpresas,
                          sCodigo,
                          sNombre,
                          sAlias,
                          sUsuario,
                          sPassword,
                          sDatos,
                          lComparte );
end;

{ ************ TZetaRegistryServer *********** }

constructor TZetaIBRegistry.Create;
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          OpenKey( IB_REGISTRY_PATH, True );
          LazyWrite := False;
     end;
end;

destructor TZetaIBRegistry.Destroy;
begin
     FRegistry.Free;
     inherited Destroy;
end;

function TZetaIBRegistry.GetBackupFolder: String;
begin
     Result := FRegistry.ReadString( '', IB_BKUP_FOLDER, ExtractFilePath( Application.ExeName ) );
end;

function TZetaIBRegistry.GetBuffers: Integer;
begin
     Result := FRegistry.ReadInteger( '', IB_RESTORE_BUFFERS, 5000 );
end;

function TZetaIBRegistry.GetCommitAfterEachTable: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_RESTORE_COMMIT, True );
end;

function TZetaIBRegistry.GetDatabaseFile: String;
begin
     Result := FRegistry.ReadString( '', IB_DATABASE_FILE, '' );
end;

function TZetaIBRegistry.GetDisableGarbageCollection: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_BKUP_GARBAGE, False );
end;

function TZetaIBRegistry.GetFixMode: Integer;
begin
     Result := FRegistry.ReadInteger( '', IB_FIX_MODE, 0 );
end;

function TZetaIBRegistry.GetIgnoreCheckSumsBkup: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_BKUP_CHECKSUMS, False );
end;

function TZetaIBRegistry.GetIgnoreCheckSumsFix: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_FIX_CHECKSUMS, False );
end;

function TZetaIBRegistry.GetIgnoreLimboTx: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_BKUP_LIMBO, False );
end;

function TZetaIBRegistry.GetIndexesInactive: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_RESTORE_INDEXES_INACTIVE, False );
end;

function TZetaIBRegistry.GetMetadataOnly: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_BKUP_METADATA, False );
end;

function TZetaIBRegistry.GetPageSize: Integer;
begin
     Result := FRegistry.ReadInteger( '', IB_RESTORE_PAGE_SIZE, 4096 );
end;

function TZetaIBRegistry.GetPassword: String;
begin
     Result := FRegistry.ReadString( '', IB_PASSWORD, 'masterkey' );
end;

function TZetaIBRegistry.GetReplaceDatabase: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_RESTORE_OVERWRITE, False );
end;

function TZetaIBRegistry.GetSetDBBuffers: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_RESTORE_SET_BUFFERS, False );
end;

function TZetaIBRegistry.GetTransportable: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_BKUP_TRANSPORTABLE, True );
end;

function TZetaIBRegistry.GetUserName: String;
begin
     Result := FRegistry.ReadString( '', IB_USER_NAME, 'SYSDBA' );
end;

function TZetaIBRegistry.GetValidateMode: Integer;
begin
     Result := FRegistry.ReadInteger( '', IB_FIX_VALIDATE_MODE, 0 );
end;

function TZetaIBRegistry.GetVerboseOutput: Boolean;
begin
     Result := FRegistry.ReadBool( '', IB_BKUP_VERBOSE, False );
end;

procedure TZetaIBRegistry.SetBackupFolder(const Value: String);
begin
     FRegistry.WriteString( '', IB_BKUP_FOLDER, Value );
end;

procedure TZetaIBRegistry.SetBuffers(const Value: Integer);
begin
     FRegistry.WriteInteger( '', IB_RESTORE_BUFFERS, Value );
end;

procedure TZetaIBRegistry.SetCommitAfterEachTable(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_RESTORE_COMMIT, Value );
end;

procedure TZetaIBRegistry.SetDatabaseFile(const Value: String);
begin
     FRegistry.WriteString( '', IB_DATABASE_FILE, Value );
end;

procedure TZetaIBRegistry.SetDisableGarbageCollection(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_BKUP_GARBAGE, Value );
end;

procedure TZetaIBRegistry.SetFixMode(const Value: Integer);
begin
     FRegistry.WriteInteger( '', IB_FIX_MODE, Value );
end;

procedure TZetaIBRegistry.SetIgnoreCheckSumsBkup(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_BKUP_CHECKSUMS, Value );
end;

procedure TZetaIBRegistry.SetIgnoreCheckSumsFix(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_FIX_CHECKSUMS, Value );
end;

procedure TZetaIBRegistry.SetIgnoreLimboTx(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_BKUP_LIMBO, Value );
end;

procedure TZetaIBRegistry.SetIndexesInactive(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_RESTORE_INDEXES_INACTIVE, Value );
end;

procedure TZetaIBRegistry.SetMetadataOnly(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_BKUP_METADATA, Value );
end;

procedure TZetaIBRegistry.SetPageSize(const Value: Integer);
begin
     FRegistry.WriteInteger( '', IB_RESTORE_PAGE_SIZE, Value );
end;

procedure TZetaIBRegistry.SetPassword(const Value: String);
begin
     FRegistry.WriteString( '', IB_PASSWORD, Value );
end;

procedure TZetaIBRegistry.SetReplaceDatabase(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_RESTORE_OVERWRITE, Value );
end;

procedure TZetaIBRegistry.SetSetDBBuffers(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_RESTORE_SET_BUFFERS, Value );
end;

procedure TZetaIBRegistry.SetTransportable(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_BKUP_TRANSPORTABLE, Value );
end;

procedure TZetaIBRegistry.SetUserName(const Value: String);
begin
     FRegistry.WriteString( '', IB_USER_NAME, Value );
end;

procedure TZetaIBRegistry.SetValidateMode(const Value: Integer);
begin
     FRegistry.WriteInteger( '', IB_FIX_VALIDATE_MODE, Value );
end;

procedure TZetaIBRegistry.SetVerboseOutput(const Value: Boolean);
begin
     FRegistry.WriteBool( '', IB_BKUP_VERBOSE, Value );
end;

end.
