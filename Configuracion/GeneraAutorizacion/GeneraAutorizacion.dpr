program GeneraAutorizacion;

uses
  Vcl.Forms,
  FGeneraAutoShell in 'FGeneraAutoShell.pas' {FGeneraAutorizacion},
  FExportaDatosSistLic in '..\FExportaDatosSistLic.pas',
  FAutoServerLoader in '..\FAutoServerLoader.pas',
  FErrorGrabarArchivo3dt in '..\FErrorGrabarArchivo3dt.pas' {ErrorGrabarArchivo3dt},
  Dispatcher3DTProxy in '..\WebServices\Dispatcher3DTProxy.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFGeneraAutorizacion, FGeneraAutorizacion);
  Application.Run;
end.
