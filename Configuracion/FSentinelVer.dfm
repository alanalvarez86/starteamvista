inherited SentinelView: TSentinelView
  Caption = 'Datos del Sentinel'
  ClientHeight = 322
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 267
    inherited OK: TBitBtn
      Visible = False
    end
    inherited Cancelar: TBitBtn
      Caption = '&Salir'
      Kind = bkClose
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 375
    Height = 267
    ActivePage = Datos
    Align = alClient
    TabOrder = 1
    object Datos: TTabSheet
      Caption = 'Datos'
      object SerialNumberLBL: TLabel
        Left = 37
        Top = 4
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = '# de Serie:'
      end
      object SerialNumber: TZetaTextBox
        Left = 92
        Top = 3
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'SerialNumber'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object EmpresaLBL: TLabel
        Left = 35
        Top = 21
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = '# Empresa:'
      end
      object Empresa: TZetaTextBox
        Left = 92
        Top = 20
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Empresa'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object EmpleadosLBL: TLabel
        Left = 34
        Top = 39
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empleados:'
      end
      object Empleados: TZetaTextBox
        Left = 92
        Top = 37
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Empleados'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object VersionLBL: TLabel
        Left = 51
        Top = 56
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Versi'#243'n:'
      end
      object Version: TZetaTextBox
        Left = 92
        Top = 54
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Version'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Vencimiento: TZetaTextBox
        Left = 92
        Top = 71
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Vencimiento'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object VencimientoLBL: TLabel
        Left = 28
        Top = 73
        Width = 61
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vencimiento:'
      end
      object UsuariosLBL: TLabel
        Left = 45
        Top = 90
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Usuarios:'
      end
      object Usuarios: TZetaTextBox
        Left = 92
        Top = 88
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Usuarios'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object EsKit: TZetaTextBox
        Left = 92
        Top = 105
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'EsKit'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object EsKitLBL: TLabel
        Left = 4
        Top = 107
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Caption = 'Kit de Distribuidor:'
      end
      object InstalacionesLBL: TLabel
        Left = 24
        Top = 124
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Instalaciones:'
      end
      object Instalaciones: TZetaTextBox
        Left = 92
        Top = 122
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Instalaciones'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Plataforma: TZetaTextBox
        Left = 92
        Top = 139
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Plataforma'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object PlataformaLBL: TLabel
        Left = 36
        Top = 141
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plataforma:'
      end
      object SQLEngineLBL: TLabel
        Left = 16
        Top = 158
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Base de Datos:'
      end
      object SQLEngine: TZetaTextBox
        Left = 92
        Top = 156
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'SQLEngine'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object EsDemo: TZetaTextBox
        Left = 92
        Top = 173
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'EsDemo'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object EsDemoLBL: TLabel
        Left = 43
        Top = 175
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Es Demo:'
      end
      object RefrescarSentinel: TSpeedButton
        Left = 287
        Top = 4
        Width = 23
        Height = 22
        Hint = 'Volver a Leer Sentinel'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333338083333333333380083333
          3333333300833333333333380833333333333330033333333333333003330000
          0333333008333800033333380083800003333333000000080333333338000833
          0333333333333333333333333333333333333333333333333333}
        ParentShowHint = False
        ShowHint = True
        OnClick = RefrescarSentinelClick
      end
      object IntentosLBL: TLabel
        Left = 48
        Top = 192
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Intentos:'
        Visible = False
      end
      object Intentos: TZetaTextBox
        Left = 92
        Top = 190
        Width = 187
        Height = 17
        AutoSize = False
        Caption = 'Intentos'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
      end
    end
    object Modules: TTabSheet
      Caption = 'M'#243'dulos'
      ImageIndex = 1
      object ModulosGB: TGroupBox
        Left = 0
        Top = 0
        Width = 178
        Height = 239
        Align = alLeft
        Caption = ' Autorizados '
        TabOrder = 0
        object Modulos: TCheckListBox
          Left = 2
          Top = 15
          Width = 174
          Height = 222
          Align = alClient
          BorderStyle = bsNone
          Color = clBtnFace
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object Caducidad: TGroupBox
        Left = 178
        Top = 0
        Width = 189
        Height = 239
        Align = alClient
        Caption = ' Pr'#233'stamos '
        TabOrder = 1
        object Prestamos: TCheckListBox
          Left = 2
          Top = 15
          Width = 185
          Height = 222
          Align = alClient
          BorderStyle = bsNone
          Color = clBtnFace
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 303
    Width = 375
    Height = 19
    Panels = <>
    SimplePanel = True
  end
end
