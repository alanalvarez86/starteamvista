unit FWizActualizaComparte;

interface

uses
 ZetaCommonClasses, Forms, dxWizardControlForm, TressMorado2013, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
 dxSkinsCore, cxContainer, cxEdit, cxProgressBar, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
 dxCustomWizardControl, dxWizardControl, System.Classes, Vcl.Controls, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  TActualizaComparte = class(TdxWizardControlForm)
    dxWizardControl: TdxWizardControl;
    dxWizardControlPageCaptura: TdxWizardControlPage;
    dxWizardControlPageAplicar: TdxWizardControlPage;
    cxLabelVersion: TcxLabel;
    lblVersion: TcxLabel;
    cxLabelActualizar: TcxLabel;
    cbVersion: TcxComboBox;
    chkDiccionario: TcxCheckBox;
    chkEspeciales: TcxCheckBox;
    cxProgressBar1: TcxProgressBar;
    dxWizardControlPageResumen: TdxWizardControlPage;
    lblResumen: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    lblExitos: TcxLabel;
    lblAdvertencias: TcxLabel;
    lblErrores: TcxLabel;
    btnBitacora: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure dxWizardControlPageChanged(Sender: TObject);
    procedure btnBitacoraClick(Sender: TObject);
  private
    { Private declarations }
    Mutex      : THandle;
    fAvances   : TList;
    oParametros: TZetaParams;
    fDataBase  : string;
    function Processing: Boolean;
    procedure HiloFinalizado(Sender: TObject);
    procedure HabilitaBotones;
  public
    { Public declarations }
  end;

var
  ActualizaComparte: TActualizaComparte;

implementation

uses
  SysUtils, Windows, DDBConfig, DMotorPatch, MotorPatchUtils, MotorPatchThreads, DBClient, ZetaCommonLists, ShellAPI,
  ZetaRegistryServer;

{$R *.dfm}

{ TActualizaComparte }

procedure TActualizaComparte.FormCreate(Sender: TObject);
begin
  HelpContext := 12;
  Mutex := CreateMutex(nil, False, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ClassName + '.Execute'));
  if Mutex = 0 then
    RaiseLastOSError;

  fAvances := TList.Create;
end;

procedure TActualizaComparte.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not Processing;
end;

procedure TActualizaComparte.FormDestroy(Sender: TObject);
begin
  FreeAndNil(oParametros);
  FreeAndNil(fAvances);
  CloseHandle(Mutex);
end;

function TActualizaComparte.Processing: Boolean;
begin
  Result := Assigned(fAvances) and (fAvances.Count > 0)
end;

procedure TActualizaComparte.HabilitaBotones;
begin
  with dxWizardControl.Buttons do
  begin
       Next.Enabled := not Processing;
       Back.Enabled := not Processing;
  end;
end;

procedure TActualizaComparte.HiloFinalizado(Sender: TObject);
var
  Hilo: TThreadPatchProceso;
begin
  with fAvances do
    Delete(IndexOf(Sender));

  if (Sender is TThreadPatchProgreso) then begin
    ;
  end else begin
    Hilo := TThreadPatchProceso(Sender);
    with Hilo.Params do begin
      lblExitos.Caption       := ParamByName('PRO_EXITOS').AsString;
      lblAdvertencias.Caption := ParamByName('PRO_ADVERTENCIAS').AsString;
      lblErrores.Caption      := ParamByName('PRO_ERRORES').AsString;

      if ParamByName('PRO_ERRORES').AsInteger > 0 then
        lblResumen.Caption := 'La actualizaci�n termin� con errores, favor de revisar la bit�cora del sistema.'
      else
        lblResumen.Caption := 'La actualizaci�n se aplic� correctamente a la versi�n: ' + cbVersion.Text;
    end;
    dxWizardControl.ActivePage := dxWizardControlPageResumen;
  end;
  cxProgressBar1.Position := 100;
  HabilitaBotones();
end;

{* Generar un archivo de bit�cora. @author Ricardo Carrillo Morales 2014-04-10}
procedure TActualizaComparte.btnBitacoraClick(Sender: TObject);
var
   oTexto: TStringList;
   oCDS: TClientDataSet;
   eTipo: eTipoBitacora;
   sArchivo: string;
begin
  oTexto := TStringList.Create;
  oCDS := TClientDataSet.Create(Self);
  with oParametros do
  try
     oTexto.Add(Format('********** Inicio: %s  **********',  [FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now)]));
     oTexto.Add(Format('      Sentencias: %d', [ParamByName('PRO_EXITOS').AsInteger]));
     oTexto.Add(Format('    Advertencias: %d', [ParamByName('PRO_ADVERTENCIAS').AsInteger]));
     oTexto.Add(Format('         Errores: %d', [ParamByName('PRO_ERRORES').AsInteger]));
     oTexto.Add('');
     // Lista de advertencias y errores
     oCDS.Data := dmDBConfig.GetBitacora(ParamByName('DB_PROCESO').AsInteger);
     while not oCDS.Eof do
     begin
          eTipo := eTipoBitacora(oCDS.FieldByName('BI_TIPO').AsInteger);
          if eTipo in [tbAdvertencia, tbError] then begin
             oTexto.Add(Format('%s: %s', [ObtieneElemento(lfTipoBitacora, Ord(eTipo)), oCDS.FieldByName('BI_TEXTO').AsString]));
             if eTipo = tbError then
                oTexto.Add(StringOfChar(' ', 7) + oCDS.FieldByName('BI_DATA').AsString);
          end;
          oCDS.Next;
     end;
     if not oCDS.IsEmpty then
        oTexto.Add('');

    if ParamByName('PRO_ERRORES').AsInteger > 0 then
       oTexto.Add('La actualizaci�n termin� con errores, favor de revisarlos y ejecutar el proceso de nuevo.')
    else
        oTexto.Add(lblResumen.Caption);
    oTexto.Add(Format('********** Final: %s **********',  [FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now)]));
    sArchivo := ExtractFilePath(ParamStr(0)) + 'COMPARTE_Actualiza_' + ParamByName('DB_APLICAVERSION').AsString + '.log';
    oTexto.SaveToFile(sArchivo);
    ShellExecute(0, nil, 'notepad', {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sArchivo), nil, SW_SHOWNORMAL);
  finally
         FreeAndNil(oTexto)
  end;
end;

procedure TActualizaComparte.dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind;
  var AHandled: Boolean);
var
  iDel, iAl: Integer;
begin
  case AKind of
    wcbkNext:
      begin
        iDel := StrToIntDef(lblVersion.Caption, 0);
        iAl := StrToIntDef(cbVersion.Text, 0);
        if (dxWizardControl.ActivePage = dxWizardControlPageAplicar) and (iDel < iAl) then begin

          oParametros := TZetaParams.Create(Self);
          with oParametros do begin
            AddBoolean('DB_APLICAPATCH'      , True);
            AddString('DB_DATOS'             , fDataBase);
            AddString('DB_DESCRIP'           , 'COMPARTE');
            AddInteger('DB_TIPO'             , -1);
            AddInteger('DB_VERSION'          , StrToInt(lblVersion.Caption));
            AddInteger('DB_APLICAVERSION'    , StrToInt(cbVersion.Text));
            AddBoolean('DB_APLICADICCIONARIO', chkDiccionario.Checked);
            AddBoolean('DB_APLICAREPORTES'   , False);
            AddBoolean('DB_APLICAESPECIALES' , chkEspeciales.Checked);
            AddInteger('DB_PROCESO'          , 0);
          end;
          cxProgressBar1.Position := 0;
          TThreadPatchProceso.Create(oParametros, dmDBConfig.ZetaProvider, HiloFinalizado, fAvances);
          //Sleep(100);
          //TThreadPatchProgreso.Create(0, nil, oParametros, HiloActualiza, HiloFinalizado, fAvances); // Hilo para actualizar avance
          AHandled := True;
        end;
      end;
    wcbkCancel:
      begin
        if not Processing then
          ModalResult := mrCancel;
      end;
    wcbkFinish:
      ModalResult := mrOk
  end;
  HabilitaBotones();
end;

procedure TActualizaComparte.dxWizardControlPageChanged(Sender: TObject);
var
  DetallesBD: TDetallesBD;
  FRegistry : TZetaRegistryServer;
begin
  if (dxWizardControl.ActivePage = dxWizardControlPageCaptura) or (dxWizardControl.ActivePage = dxWizardControlPageAplicar) then begin
    lblVersion.Caption := dmDBConfig.LoadVersion;
    cxLabelVersion.Parent := dxWizardControl.ActivePage;
    cxLabelActualizar.Parent := dxWizardControl.ActivePage;
    lblVersion.Parent := dxWizardControl.ActivePage;
    cbVersion.Parent := dxWizardControl.ActivePage;
    chkDiccionario.Parent := dxWizardControl.ActivePage;
    chkEspeciales.Parent := dxWizardControl.ActivePage;
    cbVersion.Enabled := (dxWizardControl.ActivePage = dxWizardControlPageCaptura);
    chkDiccionario.Enabled := (dxWizardControl.ActivePage = dxWizardControlPageCaptura);
    chkEspeciales.Enabled := (dxWizardControl.ActivePage = dxWizardControlPageCaptura);
  end;
  if (dxWizardControl.ActivePage = dxWizardControlPageCaptura) then begin
    try
      FRegistry := TZetaRegistryServer.Create;
      fDataBase := FRegistry.DataBase;
      DetallesBD := TDetallesBD.Create(nil, fDataBase);
      cbVersion.Properties.Items.Text := DetallesBD.DBVersiones.Text;
      chkEspeciales.Checked := DetallesBD.DBEspecial;
    finally
      FreeAndNil(FRegistry);
      FreeAndNil(DetallesBD);
    end;
    cbVersion.ItemIndex := (cbVersion.Properties.Items.Count - 1);
    dxWizardControl.Buttons.Next.Enabled := cbVersion.Properties.Items.Count > 0;
  end;
end;

procedure TActualizaComparte.dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage;
  var AAllow: Boolean);
begin
  //AAllow := not Processing;
  if ANewPage = dxWizardControlPageAplicar then
    dxWizardControl.Buttons.Next.Caption := 'Aplicar'
  else
    dxWizardControl.Buttons.Next.Caption := 'Siguiente';
end;

end.
