unit ConfigConexion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxTextEdit, cxLabel, cxMaskEdit, cxDropDownEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons,ZetaRegistryServer,dxSkinsCore, dxSkinsForm, TressMorado2013,
  dxSkinsDefaultPainters,ZetaDialogo;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TConfigurarConexion = class(TForm)
    cxListServers: TcxComboBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxUser: TcxTextEdit;
    cxPassword: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxBaseDatos: TcxComboBox;
    cxOK: TcxButton;
    cxCancelar: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxOKClick(Sender: TObject);
    procedure cxCancelarClick(Sender: TObject);
    procedure cxBaseDatosPropertiesInitPopup(Sender: TObject);
    procedure cxPasswordExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    SC : TSQLConnection;
    FRegistry: TZetaRegistryServer;
    function GetConnStr: widestring;
    procedure DatabasesOnServer(Databases : TStrings);
    procedure CargarConexion;
    function ValidaCampos: Boolean;
  public
    { Public declarations }
  end;

var
  ConfigurarConexion: TConfigurarConexion;

implementation

{$R *.dfm}
uses
DB, ADODB, ActiveX, ComObj, AdoInt, OleDB, FConfigShell,ZetaCommonTools, DDBConfig, ZetaCommonClasses, ZetaWinAPITools;

procedure ListAvailableSQLServers(Names : TStrings);
var
RSCon: ADORecordsetConstruction;
Rowset: IRowset;
SourcesRowset: ISourcesRowset;
SourcesRecordset: _Recordset;
SourcesName, SourcesType: TField;
begin
OleCheck(CoCreateInstance(CLASS_Recordset, nil, CLSCTX_INPROC_SERVER or
CLSCTX_LOCAL_SERVER, IUnknown, SourcesRecordset) );
RSCon := SourcesRecordset as ADORecordsetConstruction;
SourcesRowset := CreateComObject(ProgIDToClassID('SQLOLEDB Enumerator'))
as ISourcesRowset;
OleCheck(SourcesRowset.GetSourcesRowset(nil, IRowset, 0, nil,
IUnknown(Rowset)));
RSCon.Rowset := RowSet;
with TADODataSet.Create(nil) do try
Recordset := SourcesRecordset;
SourcesName := FieldByName('SOURCES_NAME'); { do not localize }
SourcesType := FieldByName('SOURCES_TYPE'); { do not localize }
Names.BeginUpdate;
try
while not EOF do begin
if (SourcesType.AsInteger = DBSOURCETYPE_DATASOURCE)
and
(SourcesName.AsString <>'')
then
Names.Add(SourcesName.AsString);
Next;
end;
finally
Names.EndUpdate;
end;
finally
Free;
end;
end;


procedure TConfigurarConexion.cxBaseDatosPropertiesInitPopup(Sender: TObject);
begin
     Screen.Cursor := crSQLWait;
     try
        if ValidaCampos then
        begin
             try
                DatabasesOnServer(cxBaseDatos.Properties.Items );
             except
                   on e:exception do
                      ZetaDialogo.ZWarning(Self.Caption,'No se pudo conectar al servidor de base de datos, revise los par�metros.' + #13#10 + #13#10 + e.Message,0, mbOk );
             end;
        end;
     finally
            Screen.Cursor := crDefault;
     end;
end;

procedure TConfigurarConexion.cxCancelarClick(Sender: TObject);
begin
     Close;
end;

procedure TConfigurarConexion.cxOKClick(Sender: TObject);
var
   Mensaje: string;
begin
     Mensaje := VACIO;
     if StrLleno(cxListServers.Text) then
     begin
          if StrLleno(cxUser.Text) then
          begin
               if StrLleno(cxPassword.Text) then
               begin
                    if StrLleno(cxBaseDatos.Text) then
                    begin
                         Screen.Cursor := crSQLWait;
                         try
                           if dmDBConfig.ConectarServidor(GetConnStr,Mensaje) then
                           begin
                                 try
                                 begin
                                       with FRegistry do
                                       begin
                                            if not (ServerDB = cxListServers.Text ) then
                                            begin
                                                PathMSSQL := dmDBConfig.GetPathDefaultSQLServer(GetConnStr);
                                            end;
                                            UserName := cxUser.Text ;
                                            Password := cxPassword.Text ;
                                            Database := cxListServers.Text+'.'+cxBaseDatos.Text;
                                            ServerDB := cxListServers.Text;
                                            DataBaseADO := cxBaseDatos.Text;
                                            ModalResult := mrOk;
                                       end;
                                       end;
                                       except
                                       on e:exception do
                                       begin
                                            ZetaDialogo.ZError('Error','Hubo un error al tratar de registrar Comparte '+ e.Message,0);
                                            ModalResult := mrNone;
                                       end;
                                 end;
                           end
                           else
                           begin
                                ZetaDialogo.ZError('Error',Mensaje, 0 );
                                ModalResult := mrNone;
                           end;
                         finally
                            Screen.Cursor := crDefault;
                         end;
                    end
                    else
                    begin
                       ZetaDialogo.ZWarning(Self.Caption,'El campo base de datos no puede quedar vac�o.',0, mbOK);
                       ModalResult := mrNone;
                    end;
               end
               else
               begin
                    ZetaDialogo.ZWarning(Self.Caption,'El campo clave no puede quedar vac�o.',0, mbOk);
                    ModalResult := mrNone;
               end;
          end
          else
          begin
              ZetaDialogo.ZWarning(Self.Caption,'El campo usuario no puede quedar vac�o.',0, mbOk);
              ModalResult := mrNone;
          end;
     end
     else
     begin
          ZetaDialogo.ZWarning(Self.Caption,'El campo servidor no puede quedar vac�o.', 0, mbOk );
          ModalResult := mrNone;
     end;
end;

function TConfigurarConexion.ValidaCampos: Boolean;
var
   Mensaje:string;
begin
    if StrLleno(cxListServers.Text) then
    begin
        if StrLleno(cxUser.Text) then
        begin
             if StrLleno(cxPassword.Text) then
             begin
                  Screen.Cursor := crSQLWait;
                  try
                      if dmDBConfig.ConectarServidor(GetConnStr,Mensaje) then
                      begin
                          Result := TRUE;
                      end
                      else
                      begin
                        Result := FALSE;
                        ZetaDialogo.ZError('Error', Mensaje,0 );
                      end;
                  finally
                       Screen.Cursor := crDefault;
                  end;
             end
             else
                 ZetaDialogo.ZError('Error','El campo clave no puede quedar vac�o.',0 );
        end
        else
            ZetaDialogo.ZError('Error','El campo usuario no puede quedar vac�o.',0 );
    end
    else
        ZetaDialogo.ZError('Error','El campo servidor no puede quedar vac�o.',0 );
end;

procedure TConfigurarConexion.cxPasswordExit(Sender: TObject);
begin
     ValidaCampos;
end;

procedure TConfigurarConexion.DatabasesOnServer(Databases : TStrings);
var
   rs : _RecordSet;
begin
     Databases.Clear;
     with TAdoConnection.Create(nil) do
     try
        ConnectionString := GetConnStr;
        LoginPrompt := False;
        try
           Open;
           rs := ConnectionObject.OpenSchema(adSchemaCatalogs, EmptyParam, EmptyParam);
           with rs do
           begin
                try
                   Databases.BeginUpdate;
                   while not Eof do
                   begin
                        Databases.Add(VarToStr(Fields['CATALOG_NAME'].Value));
                        MoveNext;
                   end;
                finally
                       Databases.EndUpdate;
                end;
           end;
           Close;
        except
              on e:exception do
                 ZetaDialogo.ZError('Error',e.Message,0);
        end;
     finally
            Free;
     end;
end;

procedure TConfigurarConexion.FormCreate(Sender: TObject);
begin
     FRegistry := TZetaRegistryServer.Create;
     HelpContext := 11;
end;

procedure TConfigurarConexion.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
end;

procedure TConfigurarConexion.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ListAvailableSQLServers(cxListServers.Properties.Items);
        with FRegistry do
        begin
             if not (IsKeyExists(SERVER_SERVER_DB)) OR (ServerDB = VACIO) then
            begin
                cxListServers.Text := ZetaWinAPITools.GetComputerName;
            end;
            CargarConexion;
        end;
     finally
         Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarConexion.CargarConexion;
begin
     with FRegistry do
     begin
          cxUser.Text := UserName;
          cxPassword.Text := Password;
          cxBaseDatos.Text := DataBaseADO;
          cxListServers.Text := ServerDB;
     end;
end;

function TConfigurarConexion.GetConnStr: widestring;
begin
     SC.ServerName := cxListServers.Text;
     if cxBaseDatos.ItemIndex <> -1 then
        SC.DatabaseName := cxBaseDatos.Properties.Items[cxBaseDatos.ItemIndex]
     else
         SC.DatabaseName := '';

     SC.UserName := cxUser.Text;
     SC.Password := cxPassword.Text;

     Result := Format( 'Provider=%s;', [ FRegistry.SQLNCLIProvider ] );

     Result := Result + 'Data Source=' + Trim(SC.ServerName) + ';';
     if SC.DatabaseName <> '' then
        Result := Result + 'Initial Catalog=' + Trim(SC.DatabaseName) + ';';

     Result := Result + 'uid=' + Trim(SC.UserName) + ';';
     Result := Result + 'pwd=' + Trim(SC.Password) + ';';
     Result := Result + 'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80;Language=english';
end;

end.
