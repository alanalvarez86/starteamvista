unit FExportarDatosSistema;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer,
  cxEdit, cxGroupBox, cxCheckBox, cxTextEdit, cxLabel, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls,
  FAutoClasses, FAutoServer, FAutoServerLoader, cxShellBrowserDialog, Vcl.ExtDlgs, cxMaskEdit, dxSkinsCore, TressMorado2013,
  Vcl.ImgList, cxClasses;
type
  TExportarDatosSistema = class(TForm)
    panelBotones: TPanel;
    btExportar: TcxButton;
    Cancelar: TcxButton;
    txtArchivo3dt: TcxTextEdit;
    btArchivo3dt: TcxButton;
    SaveDialog: TSaveDialog;
    cxOpenDialog: TcxShellBrowserDialog;
    ImageButtons: TcxImageList;
    chbGuardarArchivo: TcxCheckBox;
    procedure btExportarClick(Sender: TObject);
    procedure btArchivo3dtClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chbGuardarArchivoClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  end;

var
  ExportarDatosSistema: TExportarDatosSistema;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaServerTools,
     ZetaLicenseClasses,
{$ifdef SENTINELVIRTUAL}
     FSentinelRegistry,
{$endif}
     ZetaWinAPITools, DDBConfig, DBConfigServer, DZetaServerProvider, ZetaLicenseMgr, ZetaCommonLists, FExportandoDatosSistema;

{$R *.dfm}

const
     K_FECHA = 'yyyy-mm-dd';

procedure TExportarDatosSistema.FormShow(Sender: TObject);
begin
    txtArchivo3dt.Text := Format( '%sDatosSistema.3dt', [ ExtractFilePath( Application.ExeName ) ] );
    txtArchivo3dt.SetFocus;
end;

procedure TExportarDatosSistema.btArchivo3dtClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := StrDef (ExtractFileName(txtArchivo3dt.Text), 'DatosSistema.3dt');
      cxOpenDialog.Path := ExtractFilePath( Application.ExeName );
      if not strLleno (ExtractFileExt(sFileName)) then
          sFileName := sFileName + '.3dt';
      if cxOpenDialog.Execute then
        txtArchivo3dt.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TExportarDatosSistema.btExportarClick(Sender: TObject);
var
   sFileName: String;
   oExportandoDatosSistema: TExportandoDatosSistema;
begin
    sFileName := txtArchivo3dt.Text;
    if  Strvacio( ExtractFileExt( sFileName ) ) then
      sFileName := sFileName + '.3dt';
    oExportandoDatosSistema := TExportandoDatosSistema.Create( Self );
    with oExportandoDatosSistema do
    begin
             FileName := sFileName;
             GuardarArchivo := chbGuardarArchivo.Checked;
             AutoServer := FAutoServer;

             Cancelar.Caption := '&Salir';
             Cancelar.OptionsImage.ImageIndex := 17;
             Cancelar.Hint := 'Cerrar pantalla y salir';
             Cancelar.SetFocus;

             Show;
             ExportarDatos;
    end;
end;

procedure TExportarDatosSistema.CancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TExportarDatosSistema.chbGuardarArchivoClick(Sender: TObject);
begin
    txtArchivo3dt.Enabled := chbGuardarArchivo.Checked;
    btArchivo3dt.Enabled := chbGuardarArchivo.Checked;
end;

end.
