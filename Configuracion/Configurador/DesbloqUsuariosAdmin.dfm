object DesbloqueaUsuariosAdmin: TDesbloqueaUsuariosAdmin
  Left = 509
  Top = 271
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Desbloquear Usuarios Administradores'
  ClientHeight = 171
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    371
    171)
  PixelsPerInch = 96
  TextHeight = 13
  object cxOK: TcxButton
    Left = 128
    Top = 113
    Width = 120
    Height = 26
    Hint = 'Desbloquear usuarios'
    Anchors = [akRight, akBottom]
    Caption = '&Desbloquear'
    ModalResult = 1
    OptionsImage.ImageIndex = 12
    OptionsImage.Images = FConfiguradorTRESS.ImageButtons
    OptionsImage.Margin = 1
    OptionsImage.Spacing = 16
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    OnClick = cxOKClick
  end
  object cxCancelar: TcxButton
    Left = 254
    Top = 113
    Width = 88
    Height = 26
    Hint = 'Cancelar el proceso'
    Anchors = [akRight, akBottom]
    Caption = '&Cancelar'
    ModalResult = 2
    OptionsImage.ImageIndex = 14
    OptionsImage.Images = FConfiguradorTRESS.ImageButtons
    OptionsImage.Margin = 1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    OnClick = cxCancelarClick
  end
  object mensajeDesbloquea: TRichEdit
    Left = 45
    Top = 24
    Width = 297
    Height = 75
    Color = clWhite
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowFrame
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    Lines.Strings = (
      'Este proceso desbloquear'#225' a TODOS los usuarios del '
      'grupo de Administradores de Sistema TRESS.')
    ParentFont = False
    TabOrder = 2
  end
end
