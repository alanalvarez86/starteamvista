object FConfiguradorTRESS: TFConfiguradorTRESS
  Left = 392
  Top = 150
  BorderStyle = bsSingle
  Caption = 'Configurador TRESS'
  ClientHeight = 658
  ClientWidth = 1004
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxBevel1: TdxBevel
    Left = 200
    Top = 192
    Width = 50
    Height = 50
  end
  object OpcionesPaginas: TcxPageControl
    Left = 200
    Top = 0
    Width = 804
    Height = 658
    Align = alClient
    TabOrder = 4
    Properties.ActivePage = cxModulos
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.NativeStyle = False
    ClientRectBottom = 656
    ClientRectLeft = 2
    ClientRectRight = 802
    ClientRectTop = 2
    object cxConfigurar: TcxTabSheet
      Caption = 'Configurar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 0
      ParentFont = False
      TabVisible = False
      object PnlTConfigurar: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 56
        Align = alTop
        TabOrder = 0
        object imgTitleConfigurar: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            0320494441545847B5572D6C1441143E8140201008040281402010888A262090
            080484EECC25679008440582041CA22477BB77496505028140542010084405A2
            A2028140202A2A10151597ECF0BD376F76677667EF66EFB82F796977E6BDF77D
            373F6F66069B80198FAE9A423D2C73B55B166A5C4EF4B09C0CEF48F7E660DEDC
            BF544ED5BBB2D0735368E31B847CA27E718DC3BCCFAE95D39D7BE66074599A92
            51CE86B7407CDC244E223745761B4E4775809EC3F65285F02FCFF50F9FB4CEB5
            8C9C7E75A1CF82A05C5DD860FDDDCC9E5E11D74E985CBFF5E39D250D3B48F683
            A05C1FD02262724EB258048D127CDA739EABD345E4D467A6FA0104A89320B0D0
            BFCD6C749D485344F09AF1E29D21665F5C5AE029C3E8C0E77000A5BF3808C358
            16D90B34CE310A3F5345508C4FEC0C047F6223E0C8AD8F3E83000C3905410039
            E0FB491F1148F6DA91368D893C113E39F763AD11E10D34FEB50DFA2539F61151
            4ED423973066F03FA422C4DB34D79F833EEC1C4E428B011FE7D2D84B04ED223F
            691F43EE29E5609849B6BDAA088CE0373F71B2D12EF0B1AA08337E76D3C5A51A
            FCEB5FEF6355117CE0C0C727E932F81D2FACB23D447005AD44E47AABDAD61D06
            DFB412BF5444913D760224B19D0E16867A32551FB0364E6887C1FF2B11E3EF16
            274F4554C424DB71A4BE39011CF83F118A50BB7C78C961B5717202CD972FA269
            D5D0271EDDC9E0B289AD52114444B83EB4636DA0F6E35E21E1EBA155B32322AA
            36BAFBC9F6C3D49CAE2D427E5150B303421251A82FF47FE54F7DCE6F1D11CD64
            4D7322C4BD4254C458DD95EE3450715944EE2C5504FB4208DA3E2E5DA07C05E3
            E212927519151A090D6045D497DBDA5FEF894B1C20B7971272C61E4740674D87
            6FFC1011D099D08AB1D5335E054975ED886395EABC3DDD5A23C23BC3BBE1C440
            178F661CC752BED854F8974A2AB3D28C0AA85F050948DC1272077BBF8CBD8E22
            5361DF71E240DB8F2A1FCDA5F7D0E0FF230B6F11681780307829754E050F6D45
            462BD7DE11EDB73D7EC5B51768C46824695DF9F9A4BB86BC8E5AEF3A08395A95
            DC87FFF483003E555BB0E597DE057830D0599EEBE7A9739E023B1AD9B67C0A06
            837FEB72ED86590F22620000000049454E44AE426082}
        end
        object lblTConfigurar: TcxLabel
          Left = 44
          Top = 7
          Caption = 'Configurar'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object lblConfigurarDesc: TcxLabel
          Left = 44
          Top = 31
          Caption = 'Configuraci'#243'n del servidor de Sistema TRESS.'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object gbArchivosConfig: TcxGroupBox
        Left = 37
        Top = 62
        Caption = 'Archivos de configuraci'#243'n de Sistema TRESS'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Height = 76
        Width = 702
        object lblArchivoConfiguracion: TcxLabel
          Left = 82
          Top = 23
          Caption = 'Ubicaci'#243'n:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.ShadowedColor = clWhite
        end
        object cxFolderConfiguracion: TcxTextEdit
          Left = 157
          Top = 20
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 1
          OnExit = cxFolderConfiguracionExit
          Width = 396
        end
        object cxBuscar: TcxButton
          Left = 559
          Top = 20
          Width = 98
          Height = 26
          Hint = 'Seleccionar Ruta de Archivos de Configuraci'#243'n'
          Caption = 'Buscar...'
          OptionsImage.ImageIndex = 4
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = cxBuscarClick
        end
      end
      object cxGBConfigCOMPARTE: TcxGroupBox
        Left = 37
        Top = 144
        Caption = 'Acceso a BD de COMPARTE'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Height = 146
        Width = 706
        object btnConfigConexion: TcxButton
          Left = 159
          Top = 23
          Width = 204
          Height = 26
          Hint = 'Seleccionar Conexi'#243'n de COMPARTE ya Creada'
          Caption = 'Configurar BD de COMPARTE'
          OptionsImage.ImageIndex = 6
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 6
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = btnConfigConexionClick
        end
        object cxConexionComparte: TcxTextEdit
          Left = 157
          Top = 60
          Enabled = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 3
          Width = 396
        end
        object lblUbicaBDCompartida: TcxLabel
          Left = 27
          Top = 64
          Caption = 'Ubicaci'#243'n COMPARTE:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 145
        end
        object lblConfUsuario: TcxLabel
          Left = 98
          Top = 99
          Caption = 'Usuario:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleHot.Color = clWhite
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 145
        end
        object cxUsuario: TcxTextEdit
          Left = 157
          Top = 98
          HelpType = htKeyword
          Enabled = False
          ParentFont = False
          Properties.ReadOnly = True
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clBtnFace
          TabOrder = 4
          Width = 133
        end
        object btnCrearBD: TcxButton
          Left = 394
          Top = 23
          Width = 127
          Height = 26
          Hint = 'Crear Base de Datos e Inicializaci'#243'n de COMPARTE'
          Caption = 'Crear COMPARTE'
          OptionsImage.ImageIndex = 1
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 6
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnCrearBDClick
        end
      end
      object GBCreacionArchivosBD: TcxGroupBox
        Left = 37
        Top = 298
        Caption = 'Archivos de bases de datos'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 3
        Height = 65
        Width = 708
        object lblUbicacionBD: TcxLabel
          Left = 91
          Top = 22
          Caption = 'Ubicaci'#243'n:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 149
        end
        object txtRutaArchivosBD: TcxTextEdit
          Left = 157
          Top = 21
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 1
          OnExit = txtRutaArchivosBDExit
          Width = 396
        end
        object btnRutaArchivosBaseDatos: TcxButton
          Left = 559
          Top = 21
          Width = 98
          Height = 26
          Hint = 'Seleccionar Ruta de Archivos de bases de datos'
          Caption = 'Buscar...'
          OptionsImage.ImageIndex = 4
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = btnRutaArchivosBaseDatosClick
        end
      end
      object cxGBVersionDB: TcxGroupBox
        Left = 37
        Top = 374
        Caption = 'Actualizar COMPARTE'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 4
        Height = 72
        Width = 706
        object lblConfVersion: TcxLabel
          Left = 97
          Top = 30
          Caption = 'Versi'#243'n:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleHot.Color = clWhite
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 143
        end
        object cxVersionDatos: TcxTextEdit
          Left = 156
          Top = 27
          Enabled = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clBtnFace
          StyleDisabled.TextColor = clWindowText
          TabOrder = 1
          Width = 133
        end
        object cxbtnActualizar: TcxButton
          Left = 295
          Top = 27
          Width = 98
          Height = 26
          Hint = 'Actualizar Versi'#243'n de Base de Datos de COMPARTE'
          Caption = 'Actualizar'
          Enabled = False
          OptionsImage.ImageIndex = 3
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 6
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = cxbtnActualizarClick
        end
      end
      object cxGBConfigAcceso: TcxGroupBox
        Left = 37
        Top = 456
        Caption = 'Acceso a Sistema TRESS'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        Height = 73
        Width = 706
        object cxTipoLogin: TcxComboBox
          Left = 159
          Top = 24
          ParentFont = False
          Properties.AutoSelect = False
          Properties.DropDownListStyle = lsFixedList
          Properties.Items.Strings = (
            'Usuario de TRESS'
            'Active Directory'
            'Active Directory '#243' Usuario TRESS')
          Properties.OnChange = cxTipoLoginPropertiesChange
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -13
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 1
          Width = 358
        end
        object lblConfTipoAcceso: TcxLabel
          Left = 76
          Top = 27
          Caption = 'Tipo Acceso:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 143
        end
      end
      object cxGBConfigWorkflow: TcxGroupBox
        Left = 37
        Top = 542
        Caption = 'WorkFlow'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        Height = 75
        Width = 706
        object cxbtnPrepararWorkFlow: TcxButton
          Left = 242
          Top = 21
          Width = 89
          Height = 26
          Hint = 
            'Preparar el Sistema para configurar accesos y obtener reportes d' +
            'e Workflow'
          Caption = 'Preparar'
          OptionsImage.ImageIndex = 9
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = cxbtnPrepararWorkFlowClick
        end
        object lblPrepararUsarWorkFlow: TcxLabel
          Left = 13
          Top = 24
          Caption = 'No preparado para reportes de WorkFlow:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
    end
    object cxSentinel: TcxTabSheet
      Caption = 'Sentinel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      TabVisible = False
      object cxGBSentinel: TcxGroupBox
        Left = 0
        Top = 301
        Caption = 'Guardia'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 0
        Height = 296
        Width = 617
        object lblLicenciaSentinel: TcxLabel
          Left = 18
          Top = 24
          Caption = 'Licencia Guardia:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 109
        end
        object cxArchivoLicencia: TcxTextEdit
          Left = 115
          Top = 22
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 3
          Width = 293
        end
        object lblServidor: TcxLabel
          Left = 60
          Top = 64
          Caption = 'Servidor:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 109
        end
        object cxCompNam: TcxTextEdit
          Left = 115
          Top = 63
          Enabled = False
          ParentFont = False
          Properties.ReadOnly = True
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -9
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 0
          Width = 121
        end
        object cxLicenciaSentinel: TcxButton
          Left = 414
          Top = 21
          Width = 155
          Height = 26
          Hint = 'Carga el archivo del guardia'
          Caption = 'Cargar Licencia'
          OptionsImage.ImageIndex = 5
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 6
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = cxLicenciaSentinelClick
        end
        object cxTituloAutorizaVirtual: TcxLabel
          Left = 299
          Top = 64
          Caption = 'NO AUTORIZADO'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clWhite
          Style.TextStyle = [fsBold]
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 394
        end
        object btnBorrarSentinelVirtual: TcxButton
          Left = 414
          Top = 53
          Width = 155
          Height = 26
          Hint = 'Desinstala el guardia'
          Caption = 'Desinstalar Guardia'
          OptionsImage.ImageIndex = 14
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 6
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = btnBorrarSentinelVirtualClick
        end
        object cxGrupoAcciones: TcxGroupBox
          Left = 64
          Top = 89
          Caption = 'Acciones sugeridas'
          TabOrder = 7
          Visible = False
          Height = 151
          Width = 505
          object btnEscribirDisco: TRadioButton
            Left = 34
            Top = 102
            Width = 113
            Height = 17
            Caption = 'Guardar en disco'
            TabOrder = 1
            TabStop = True
          end
          object btnEnviarGTI: TRadioButton
            Left = 34
            Top = 79
            Width = 247
            Height = 17
            Caption = 'Enviar reporte a Grupo Tress Internacional'
            Checked = True
            TabOrder = 0
            TabStop = True
          end
          object btnExportaReporte: TcxButton
            Left = 352
            Top = 93
            Width = 121
            Height = 26
            Hint = 'Exportar reporte de licencia.'
            Caption = 'Exportar reporte'
            OptionsImage.ImageIndex = 7
            OptionsImage.Images = ImageButtons
            OptionsImage.Margin = 1
            OptionsImage.Spacing = 6
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = btnExportaReporteClick
          end
          object cxAccionSugerida: TcxRichEdit
            Left = 34
            Top = 23
            Enabled = False
            ParentFont = False
            Lines.Strings = (
              
                'Exporte el archivo de Datos del Sistema y solicite una nueva lic' +
                'encia. Si persiste la '
              'situaci'#243'n env'#237'e el reporte a Grupo Tress Internacional.')
            Style.Color = clInfoBk
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Segoe UI'
            Style.Font.Style = []
            Style.HotTrack = True
            Style.LookAndFeel.NativeStyle = True
            Style.Shadow = False
            Style.TransparentBorder = True
            Style.IsFontAssigned = True
            StyleDisabled.BorderColor = clBtnShadow
            StyleDisabled.Color = clInfoBk
            StyleDisabled.LookAndFeel.NativeStyle = True
            StyleDisabled.TextColor = clWindowText
            StyleFocused.LookAndFeel.NativeStyle = True
            StyleHot.LookAndFeel.NativeStyle = True
            TabOrder = 3
            Height = 34
            Width = 439
          end
        end
        object cxImgNoAut: TcxImage
          Left = 245
          Top = 62
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            020C494441545847BD57A152C340108D40221095083E0081443053043407D319
            0CE4521020900804028147222A100804128140562010D0249D181C1289A8A840
            20CAEE35CBB4E926B9BB267D33AF9D66F7F6BDCDF42E1B8710B59AB5D077AF23
            5F748071E88BBB50BA9B49D81A2FF5FA42241BCD488ACB408A8BC073B7F05A12
            1EA1EB3704887D83F090617B6A8126BA87BB6BB03E4ED51B42A36F185349EF47
            6219BAEDA7932628DD475313514B6C40DD015B0F29DDAFF8786FC9811FEDA920
            47031385E244296E1D281CB2418E1A26B4C58190F7E9E00717CC648E09137122
            1A78E202B9644CD888AB3B00C54EB86021C74CD8882BE27F00018B9FD9842282
            093C2BECC4935D80F838A82F42A15736B1022AC370D79438615E26587142D526
            72C5095599D01227946DC2489C5096092B71C2AC26661227581F542376741F60
            2CA0837D28F29B2A6A468D07188B52C489A6264A1527EA9AA8449C5864A25271
            629689B98813D32692E9D5581C4C0FA098FE4837461CD195383A810B53A37311
            95381C32D68795143FB17457D434C326E490C45507005B13A16C9C39812FCEB9
            6016D3E2042B133892859E7BCA06196689134C4DA8FF41E06DAF72C1348BC409
            46263CB1AE16E1B6601312EA8A13744CF47C719FA4E358DDACC1457E27C0F46A
            224E5026E00D3BA3E603C693D411D476845768DCD7AA6330D46BEDDCFC8FCE96
            C0DB0C6FC357D8317E4F36E3387F91A0DA8BAB864F300000000049454E44AE42
            6082}
          Properties.ShowFocusRect = False
          Style.BorderColor = clWindow
          TabOrder = 8
          Height = 21
          Width = 26
        end
        object cxImgAut: TcxImage
          Left = 267
          Top = 62
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000040
            000000400806000000AA6971DE0000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            03EB49444154785EE5D9CDAB4D511806700349A11899291928F2313764CC2DF9
            0B1433429292898914F928254329A428A5943FC008DD8C246526138501A3E379
            75F6699DE77DD6DE6BEDB3BF8E3DF80D3CAD779DFDDCED9EB3CEBE6B2693C9A8
            C9704C643826321C13198E890CC7C405775657FE07C7E1397909B7B9EFDC3F0C
            162DB375701F26110FB8EFDC3F0C162DABFDF00554F13F70115C5F17D8A22564
            E5ACA42A6FEC87F36F2DF77541B170496C8047A04A17CEC06C86FBBA205C3C70
            DBE03DA8D2852B3037C77D5DC003037514CAFECB9B4BE066B9AF0BD4D0C01C03
            5538E4EE7C81FBBA400D0DC855508543F2CE17B8AF0BD4D0403C05553864071E
            353BC37D5DA08606E009A8C2217B43DC0C6A7E86FBBA400DF5C84E762977FE03
            D847A2DA630EF775811AEAC93E889DECD876507B38DCD7056AA807BBE017A8B2
            CC3E12D51E12F775811AEA98DDF99FA0CAB213A0F688E2BE2E50431DCAB9F38F
            41ED518AFBBA400D7524E7CE7F07B54725EEEB0235D4819C3B6FE567DFEE7271
            5F17A8A196ED84D43B6F8E80DA2709F775811A6AD95B5045951BA0F648C67D5D
            A0865A9472C22BBC03B54716EEEB0231B457644D4839E1852A8FB929B8AF0B68
            C03E67EDC55F40D2513351CE9D37D99FF731DCD705C162BE43F665C39EC2846B
            EAB806E1BE556A7DDEC7705F174C17DE037531F61426EBE8496C56ED1B53FBF3
            3E86FBBA008BCE82BA98D005709B5738086AAF32B53FEF63B8AF0BB0E815A88B
            61F67BEC5E20C2DE3FBE82DA27C66E84DA6B21DCD705586417FB19D445B155B0
            539C7B2152F5F496D97AB5CFC2B8AF0BA60B77C36F5017C7EC14771AE65E2870
            1DD45C991DA0F65A18F77541B0F824A88B8BB9091B21DC630FA8B565CE43B847
            A3B8AF0B68E032A88B8CF90887C166D7C20F50EB62EC58CCD7D028EEEB023164
            BFE7EA62CBDC828794A5B02F46EA1A1AC37D5D20863641CEB7B5BA5A79D767DC
            D7056A0856405D74535A7BD767DCD7056A68EA19A88B6F426BEFFA8CFBBA400D
            4DD93B7CEE9B5A0A3B76ABD76B05F775811A0AD4F9582BF30DD4EBB486FBBA40
            0D913A079B9843A05EA335DCD7056A48B0A733AA508EBBA0F66E15F775811A12
            ECE98C2A95834F8D9DE0BE2E504311C5D3A23A1A7BC2938BFBBA400D95780DAA
            60994FA0F6EA04F775811A2AB11552BF3516EC2F406AAF4E705F17A8A10A39DF
            1AED30A5F6E80CF775811A4A90F2AB609FF98D3CDA5E04F775811A4AB005AA1E
            79953D34E90CF775811A4A54F6D0F30DA899CE715F17A8A10CB1C7E90740ADEF
            1CF775811ACAC47FECECE5C417C37D5DA08632D9539DF007D0CB892F86FBBA40
            0DD5600F36ADFCA9201B04EEEB023554D339584F59EFB8AF0BC646866322C331
            91E198C8704C64381E93357F010DDCEAF04B3024B80000000049454E44AE4260
            82}
          Properties.ShowFocusRect = False
          Style.BorderColor = clWindow
          TabOrder = 9
          Height = 21
          Width = 26
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 56
        Align = alTop
        TabOrder = 1
        object imgTitleSentinel: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            025C494441545847E5562B4C034110AD40201008040281402010082409080402
            0981DBBDA40681402071902010905C6FAF0E8140221008040281402210480402
            51814020487699991DCAF6BA5B7ABD9690F09249EE6E66DF9B9BD95FE55FC0D4
            D7464C521DE5D7DF058A6B256F752A1F7F3D0953AF8E93B092060D13C184D83D
            58E4C5BF9310571C323818154DC3DF3EE5C56D02F2DEEC2D0C7168FF41E2A978
            F1897F1954E17C2049E86C632E240EA209B464A7F99ECA8C87F507A616CD03E9
            9B2BEA1AF93046C9A3E63778E6E1E56032B9E81307810F5D135BE0A3C9085578
            B5F34324FC5E7E4202F92A0AB9C2440E09E9345EC618775EC0772A3D26567A49
            7612C7727318C12621B3AE44753D9E82E01D2C11083C81DDC3F30D952E11B314
            437E8FB8928DBC78D7C0A5A13371E823760DC4DB3618FB1DCB1C4D335D3100E9
            04FEA98FB81B2B258EC072FB88BB31AA48B23EC954C581FDF4122BF1807DE7B6
            5C065BC3F3A227D80927DE5D4248089690DCE4902650C8DBA69ADCE590E240A1
            3C21AE5176B701CFF4FC768B2D60777140894F5AC894B8635710FEA4E3197617
            03F6B985087ACEAE2070B6BB63C8603B66773160F95C22783F60571074C773C6
            909548E0C225C2777605817BBC3B86C6C151CCEE62C025D642444BADF386824B
            3237A6D1F305C328B1E49259427117BAC16A156DB7C567E28CDDBD21FF4744AA
            C4B3AEC9185A32618EA3313AF33D7164BDF6FF0BB88D8250F036D3C9B0854C53
            0EF4B73F9C8279832ADDF4F572495B6DE0A8758D124DE5BE39AD0EF3D0FE0149
            EDE1D3BA4191B03D23AE4B1DBB4560F7FD7819CE869552C7EDDF45A5F209CB3B
            CE0314402F100000000049454E44AE426082}
        end
        object lblTSentinel: TcxLabel
          Left = 44
          Top = 7
          Caption = 'Guardia'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object lblSentinelDesc: TcxLabel
          Left = 44
          Top = 31
          Caption = 'Configuraci'#243'n y actualizaci'#243'n de la licencia de Guardia.'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object btnRenovaAutorizacion: TcxButton
        Left = 623
        Top = 131
        Width = 153
        Height = 26
        Hint = 'Renovar Autorizaci'#243'n del Sistema'
        Caption = 'Renovar Autorizaci'#243'n'
        OptionsImage.ImageIndex = 1
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnRenovaAutorizacionClick
      end
      object btnActualizaSentinel: TcxButton
        Left = 623
        Top = 99
        Width = 153
        Height = 26
        Hint = 'Cargar nuevas claves '
        Caption = 'Actualizar Guardia'
        OptionsImage.ImageIndex = 2
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnActualizaSentinelClick
      end
      object btnRefreshSentinel: TcxButton
        Left = 623
        Top = 67
        Width = 153
        Height = 26
        Hint = 'Refrescar Lectura Guardia'
        Caption = 'Refrescar Guardia'
        OptionsImage.ImageIndex = 0
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnRefreshSentinelClick
      end
      object cxGBSentinelFisico: TcxGroupBox
        Left = 0
        Top = 67
        TabOrder = 5
        Height = 228
        Width = 617
        object lblSentNumero: TcxLabel
          Left = 36
          Top = 15
          Caption = 'N'#250'mero de Guardia:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 144
        end
        object cxSN_NUMERO: TcxTextEdit
          Left = 150
          Top = 13
          Hint = 'Sentinel Autorizado'
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 1
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblSentNumEmpresa: TcxLabel
          Left = 34
          Top = 44
          Caption = 'N'#250'mero de Empresa:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 144
        end
        object cxSI_FOLIO: TcxTextEdit
          Left = 150
          Top = 43
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 3
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblSentPlataforma: TcxLabel
          Left = 82
          Top = 71
          Caption = 'Plataforma:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 144
        end
        object cxPlataforma: TcxTextEdit
          Left = 150
          Top = 70
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taLeftJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 5
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblSentBaseDatos: TcxLabel
          Left = 64
          Top = 96
          Caption = 'Base de Datos:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 144
        end
        object cxBaseDatos: TcxTextEdit
          Left = 150
          Top = 95
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taLeftJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 7
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblVersion: TcxLabel
          Left = 98
          Top = 125
          Caption = 'Versi'#243'n:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 144
        end
        object cxVersion: TcxTextEdit
          Left = 150
          Top = 124
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 9
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblEmpleados: TcxLabel
          Left = 345
          Top = 15
          Caption = 'Empleados:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 408
        end
        object cxEmpleados: TcxTextEdit
          Left = 416
          Top = 13
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 11
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblUsuarios: TcxLabel
          Left = 356
          Top = 44
          Caption = 'Usuarios:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 408
        end
        object cxUsuarios: TcxTextEdit
          Left = 416
          Top = 43
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 13
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblVencimiento: TcxLabel
          Left = 337
          Top = 71
          Caption = 'Vencimiento:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 408
        end
        object cxVencimiento: TcxTextEdit
          Left = 416
          Top = 70
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 15
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblInstalaciones: TcxLabel
          Left = 334
          Top = 96
          Caption = 'Instalaciones:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 408
        end
        object cxInstalaciones: TcxTextEdit
          Left = 416
          Top = 95
          Enabled = False
          ParentFont = False
          Properties.Alignment.Horz = taRightJustify
          Properties.ReadOnly = True
          Style.Color = clInfoBk
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clInfoBk
          StyleDisabled.TextColor = clBlack
          TabOrder = 17
          Text = 'cxSN_NUMERO'
          Width = 121
        end
        object lblEsDemo: TcxLabel
          Left = 357
          Top = 125
          Caption = 'Es Demo:'
          ParentColor = False
          Style.Color = clWhite
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 408
        end
        object cxEsDemo: TcxCheckBox
          Left = 416
          Top = 125
          Enabled = False
          Properties.Alignment = taRightJustify
          TabOrder = 19
          Width = 17
        end
      end
    end
    object cxModulos: TcxTabSheet
      Caption = 'Modulos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 3
      ParentFont = False
      TabVisible = False
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 56
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object imgTltleModulos: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            01AA494441545847ED55A14E0431145C813C8144221008040249020281447081
            DD6E7208E449040E921308C4EE76374120107C0012814022F900249F804090B4
            CC7BE976BBDDB29290D04926473AD3D7B9D7D723898850953805EF46591F6F91
            57CB6C2FA8F779C8759B7C4DC9EC36A05BEA529C274A8A2F2D851E2399B9A8CC
            9E43BA4BF2905757E232A4FBC4B70A0B2E7F2D00BAF18AC3162852E0F3C3AE07
            02E0EF77F8AF559D5DA92A7B73D60701A07FA2464D6BD05FDA75A20DC0C52F76
            96683341CB74BD0DE107A0C375335B6123A09BE984C2B77578CD04E02B2ED36D
            3602740642DF9346EC3A508B5DE3B140B1828BF81DA0E1F180C007ECF50354E2
            810D0E7471B4CA75C02E40916D1ADD029B17A648BF0378396C70402F84B56100
            DEEB4217B3653E13EC026083D119DCAAB6ADC32B78649303786AA3795740D735
            9DB0C900614FF84CD006A0BBC2B09CD1FBA56ED02156F3AF80FD78E365BEC1B3
            824174D6874388C1A3DF126ABD92E91CF5EC80771D186128C04F0C0518630C10
            03C40031400C1003FC810030D2A65156F93E0728451ED47B4CE7ECC5BF5F047F
            0A7B5A8A1BF246FC6724C93724F46CA2F857405C0000000049454E44AE426082}
        end
        object lblTModulos: TcxLabel
          Left = 44
          Top = 7
          Caption = 'M'#243'dulos'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object lblModulosDesc: TcxLabel
          Left = 44
          Top = 31
          Caption = 
            'Lista de m'#243'dulos que se tienen autorizados o prestados de Sistem' +
            'a TRESS.'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object cxGBModulosAutoriza: TcxGroupBox
        Left = 0
        Top = 56
        Align = alLeft
        Caption = 'M'#243'dulos Autorizados'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Height = 598
        Width = 305
        object cxModulosAut: TcxCheckListBox
          Left = 3
          Top = 15
          Width = 299
          Height = 573
          Align = alClient
          Columns = 2
          EditValueFormat = cvfStatesString
          Items = <>
          ParentFont = False
          ReadOnly = True
          Style.BorderStyle = cbsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.TextColor = clBlack
          TabOrder = 0
          ExplicitWidth = 285
        end
      end
      object cxGBModulos: TcxGroupBox
        Left = 305
        Top = 56
        Align = alClient
        Caption = 'M'#243'dulos Prestados'
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        ExplicitLeft = 291
        ExplicitWidth = 509
        Height = 598
        Width = 495
        object cxModulosPrest: TcxCheckListBox
          Left = 3
          Top = 15
          Width = 489
          Height = 573
          Align = alClient
          Columns = 2
          EditValueFormat = cvfStatesString
          Items = <>
          ParentFont = False
          ReadOnly = True
          Style.BorderStyle = cbsNone
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.TextColor = clBlack
          TabOrder = 0
          ExplicitWidth = 503
        end
      end
    end
    object cxServidorProcesos: TcxTabSheet
      Caption = 'cxServidorProcesos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 4
      ParentFont = False
      TabVisible = False
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 654
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGridServidor: TcxGrid
          Left = 0
          Top = 104
          Width = 673
          Height = 467
          BorderStyle = cxcbsNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TabStop = False
          object cxGridServidorDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            FilterBox.Visible = fvNever
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnGrouping = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object colLibreria: TcxGridDBColumn
              Caption = 'Librer'#237'a'
              DataBinding.FieldName = 'colLibreria'
              SortIndex = 0
              SortOrder = soAscending
              Width = 120
            end
            object colDescripcion: TcxGridDBColumn
              Caption = 'Descripci'#243'n'
              DataBinding.FieldName = 'colDescripcion'
              Width = 250
            end
            object colVersion: TcxGridDBColumn
              Caption = 'Versi'#243'n'
              DataBinding.FieldName = 'colVersion'
              FooterAlignmentHorz = taRightJustify
              Width = 80
            end
            object colBuild: TcxGridDBColumn
              Caption = 'Build'
              DataBinding.FieldName = 'colBuild'
              Width = 80
            end
            object colFecha: TcxGridDBColumn
              Caption = 'Fecha'
              DataBinding.FieldName = 'colFecha'
              Width = 80
            end
          end
          object cxGridServidorLevel1: TcxGridLevel
            GridView = cxGridServidorDBTableView1
          end
        end
        object lblPaqueteTress20: TcxLabel
          Left = 14
          Top = 71
          Caption = 'Paquete TRESS20:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object cxPathServidor: TcxTextEdit
          Left = 114
          Top = 70
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 2
          OnExit = cxPathServidorExit
          Width = 319
        end
        object btnBuscarDlls: TcxButton
          Left = 445
          Top = 66
          Width = 95
          Height = 26
          Hint = 'Ruta de Instalaci'#243'n de Componentes'
          Caption = 'Buscar...'
          OptionsImage.ImageIndex = 4
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = btnBuscarDllsClick
        end
        object pnlServidorProcesos: TPanel
          Left = 1
          Top = 1
          Width = 798
          Height = 56
          Align = alTop
          TabOrder = 4
          object imgTltleServidorProcesos: TImage
            Left = 3
            Top = 2
            Width = 35
            Height = 35
            Picture.Data = {
              0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
              000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
              206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
              98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
              02DC494441545847CD56216C1441143D81402010080402814020100804091508
              04A2A284EECC25672A10880A048204920A042477B77B49454505028140201008
              4405125181405454202A2A1088263BBCF7E7CF3237B3A5DCF5EEE0252FD9F96F
              E6FF3F333B7FA6F3DFC06DF7CEBA4171CBF5CD75358DA11E75AFB8CA2EB9D1FD
              736A6A409B686571554D6308BEEBA1BDA4A68E7B76FB0CDA953428D4A5DD73A5
              7524DAEFD8414400ED8D462BED415DADDE5089DA4DDAA2B11B2A0932DFA5DD94
              E0A579CB76E854850EBF592C8BC699271A9CECC84080DF998E312A77EACABCCE
              F4D2EC866FDFA9B4EFE30E643D348FA9B9D2DCC934CC48060270B69FEADC0E95
              39B98F991E31745A8F8D087014FE05EE2FDACD12ABBE2903017E27DA41FC9FD4
              03F330D6534A27D993CABCC0ACBFC1C11724B42282026DEEF30EB82701A300AE
              DF3B4F9BD7CC67F655A901B4976DC149ED323FB851EF2292FADA169CD46EF3C1
              49C149EDCA65EEDEC5126E817DD77F7059CD0259661C2F70BB1E14AB6A6E409B
              6843BB16B6873E4E0A4EAA03732F36228943662F8EFC996D8E8DE87A420837B0
              4FC635D4101424F4F91EDB8FA3384100290A31EBB278E403A03AA61A66260301
              FEB8B96E7EA6B6E3E89D94F64D2A70394543D5CB34AC880C0430362B4493D03B
              F1C7EC281879A4B8EF220208F8291E540F6C5725BFFFD1D849A96EE8A87B0D81
              9E2299F53838A117C79A1BDAE74C56CD0D606BEE8A49A92EA60782AFCC6405A6
              01CBEC698293E2C81F3596537308EE73B94550688DD8159DB71BEF87E4FE9896
              3E00F63E15C25EBB57C5057CFF18D7F2A3372D43021F321105461290974EA2CD
              9021817E2EFA07095720D7664749409619D77030729F4550B0F4C68366490D81
              24F870C423247E4E11F1FB6D1ED430EDF0AFA1F12A386B6AA8B0D7C5B25C3E98
              B50F7EBA3AFF37F4C17977478F4B9E8A450427250104DC6A1317419F40CBDB7D
              519404E65D6CFE449F40CBAB675194E069AD5F24FD938B5BF04F68977E019308
              896CFDE29B680000000049454E44AE426082}
          end
          object lblTServidorProcesos: TcxLabel
            Left = 44
            Top = 7
            Caption = 'Servidor de Procesos'
            ParentColor = False
            ParentFont = False
            Style.Color = clWhite
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clGrayText
            Style.Font.Height = 21
            Style.Font.Name = 'Segoe UI'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
          end
          object lblServidorProcesosDesc: TcxLabel
            Left = 44
            Top = 31
            Caption = 
              'Lista que muestra las propiedades de las librer'#237'as instaladas de' +
              ' Sistema TRESS.'
            ParentColor = False
            ParentFont = False
            Style.Color = clWhite
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Segoe UI'
            Style.Font.Style = []
            Style.IsFontAssigned = True
          end
        end
        object Panel8: TPanel
          Left = 1
          Top = 612
          Width = 798
          Height = 41
          Align = alBottom
          BevelInner = bvLowered
          BorderWidth = 1
          Color = clHighlightText
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 5
          object Label2: TLabel
            Left = 26
            Top = 14
            Width = 135
            Height = 13
            Cursor = crHandPoint
            Hint = 'Abrir Servicios de Componentes'
            Caption = 'Servicios de Componentes'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clHotLight
            Font.Height = -11
            Font.Name = 'Segoe UI'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            OnClick = Label2Click
          end
          object Image5: TImage
            Left = 7
            Top = 12
            Width = 17
            Height = 17
            Cursor = crHandPoint
            Hint = 'Abrir servicios de componentes'
            Picture.Data = {
              0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
              0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000000
              097048597300000EC300000EC301C76FA8640000016A49444154384F8553A14E
              C44010AD40201048C4090402814492C00760C9B5BB4D8A3881409CC0231127DA
              6EEB4F2210271108041F804022104804028120D9E5CD749A6EEFF67A2F99B47D
              336F3A333B1B11DC3CDB76465DB2DD9E6E3129A06F5BEAA92DD4B950AB40C0C2
              19EDC86C91C44247AED267D6A837E68DFE70F5C58EB83AB87CBCDF8A39B0D413
              D808827B9F675FA5EE44D6814B347A664BF5CB4146BF22C1CFB2587C7FCE2487
              22ED033D5E8544642444D20A36B7557C2C9206D442A85CDF482CE10C1ABABCE2
              CF26B90E897C4382858447AE484ED0EE7B6FA008780A095B9316A624C2A93C33
              E70F144E4C5D7D2F0B876C65A0CD228583D719558EE92731FEFE303448F86652
              7EEE715F4830C110B1611D49BDF676805B93F52681C4542ECF76B9747C746B8C
              13C1846FFC04CC534C9DEDE139B2457AC4C216EC30EA11995FF852F1E5C131F9
              098CFAA4E313C966D842A78D10E5E2B8829768083C30946DEBF440A83588A27F
              BDC0E00E91A0CF5E0000000049454E44AE426082}
            OnClick = Image5Click
          end
        end
      end
    end
    object cxServicios: TcxTabSheet
      Caption = 'cxServicios'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 5
      ParentFont = False
      TabVisible = False
      object PnlGridServicios: TPanel
        Left = -2
        Top = 34
        Width = 627
        Height = 444
        Caption = 'PnlGridServicios'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        object cxGridServicios: TcxGrid
          Left = 3
          Top = 31
          Width = 624
          Height = 404
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGridServiciosDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            FilterBox.Visible = fvNever
            OnFocusedRecordChanged = cxGridServiciosDBTableView1FocusedRecordChanged
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnAddValueItems = False
            OptionsCustomize.ColumnGrouping = False
            OptionsCustomize.ColumnHidingOnGrouping = False
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnSorting = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object colServicios: TcxGridDBColumn
              Caption = 'Servicios'
              DataBinding.FieldName = 'colServiceName'
              MinWidth = 342
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.Moving = False
              Options.Sorting = False
              Width = 475
            end
            object colStatus: TcxGridDBColumn
              Caption = 'Estatus'
              DataBinding.FieldName = 'colStatus'
              MinWidth = 145
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
              Options.Sorting = False
              Width = 145
            end
            object colServiceName: TcxGridDBColumn
              Caption = 'ServiceName'
              DataBinding.FieldName = 'colServiceName'
              Visible = False
              MinWidth = 64
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.Focusing = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
              Options.Sorting = False
            end
            object colServiceType: TcxGridDBColumn
              DataBinding.FieldName = 'colServiceType'
              Visible = False
            end
            object colServiceRealName: TcxGridDBColumn
              DataBinding.FieldName = 'colServiceRealName'
              Visible = False
              MinWidth = 300
            end
          end
          object cxGridServiciosLevel1: TcxGridLevel
            GridView = cxGridServiciosDBTableView1
          end
        end
      end
      object pnlBotones: TPanel
        Left = 0
        Top = 3
        Width = 625
        Height = 56
        TabOrder = 7
        object imgTitleServicios: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            00CF49444154584763F83729FAFEFF49D1FF71E17F13A3E733D0128C3A801207
            FC9F18DD804D0FB11864F71077C0A4C800903CD9785274F7682264005A50008E
            4B5C1818CC50A5A3803600180516FF27473BE0C4932235A04A6903467301250E
            F8DF1FAE8035DA88C4A0E8A7CC01E06C8A5D1F311864F71077407F94C1FF4951
            09E462A0D921433B115205002D18D8826814805222B6140AC713226DA04A6903
            467301450511289720B79E48C440B30B287300C8202C7A88C520BB87B803FAE3
            05C05532991868B6CCD04E849403060600F1BBA23D65F7E76F0000000049454E
            44AE426082}
        end
        object lblTServicios: TcxLabel
          Left = 44
          Top = 7
          Caption = 'Servicios'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object lblServiciosDesc: TcxLabel
          Left = 44
          Top = 31
          Caption = 'Estatus de los servicios instalados de Sistema TRESS.'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object btnReiniciarServicios: TcxButton
        Left = 627
        Top = 129
        Width = 95
        Height = 26
        Hint = 'Reinicia el servicio seleccionado'
        Caption = 'Reiniciar'
        OptionsImage.ImageIndex = 2
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnReiniciarServiciosClick
      end
      object btnPararServicios: TcxButton
        Left = 627
        Top = 97
        Width = 95
        Height = 26
        Hint = 'Detiene el servicio seleccionado'
        Caption = 'Detener'
        OptionsImage.ImageIndex = 15
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnPararServiciosClick
      end
      object btnIniciarServicios: TcxButton
        Left = 627
        Top = 65
        Width = 95
        Height = 26
        Hint = 'Inicia el servicio seleccionado'
        Caption = 'Iniciar'
        OptionsImage.ImageIndex = 12
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        OptionsImage.Spacing = 7
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnIniciarServiciosClick
      end
      object btnDesinstalarServicio: TcxButton
        Left = 627
        Top = 442
        Width = 95
        Height = 26
        Hint = 'Desinstalar servicio'
        Caption = 'Desinstalar'
        Enabled = False
        OptionsImage.ImageIndex = 16
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        OptionsImage.Spacing = 7
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnDesinstalarServicioClick
      end
      object Panel9: TPanel
        Left = 0
        Top = 613
        Width = 800
        Height = 41
        Align = alBottom
        BevelInner = bvLowered
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        object Label1: TLabel
          Left = 30
          Top = 13
          Width = 112
          Height = 13
          Cursor = crHandPoint
          Hint = 'Abrir Servicios de Windows'
          Caption = 'Servicios de Windows'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = Label1Click
        end
        object Image6: TImage
          Left = 11
          Top = 11
          Width = 17
          Height = 17
          Cursor = crHandPoint
          Hint = 'Abrir servicios de windows'
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
            0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000000
            097048597300000EC300000EC301C76FA8640000014349444154384FCD52A14E
            0431105D81409C442010080402814022100804028140221048BEA023F8000402
            B9E24276DBC2375422112CD739CC49C4092402016F66E7C886DC6D70F092A6ED
            CCEBF4CD6B8BFF050ACDAA0B9C28E4530BFD0E54F3858E30DEA6C09F2E7245BE
            D9709E4B141D186D3EA89C2C3B9FB31E0CF95D665D7BFED0B5E703A32E06D5CF
            FB76E851E4BB38BE6A0B7232CA628034C1CD53BD2DE61D0B4BFC4154489E7C3E
            9618A5B4A4C92E94D85FE049DAA05B5EC1FE4DD44A8EEAD1895151B91A1DB62D
            C00B988982C36E0B5A405AEB780455E7987755D65C13676BC8C7AB5C9A9A64AF
            73AD1C78D62A0049247D3FA32AC09F506233406E0BB129E633E5C7BCA7C5EF5F
            D6B4C00CD66789368E2CA4402C62BCCA685B8407AA2067A3F443FE0A5579BD55
            0753E57651FB53411F84AC2D8967816FE88E372DF5A7288A2F0FFF2AE3047595
            6C0000000049454E44AE426082}
          OnClick = Image6Click
        end
      end
      object btnInstalarServicio: TcxButton
        Left = 627
        Top = 410
        Width = 95
        Height = 26
        Hint = 'Instalar servicio'
        Caption = 'Instalar'
        OptionsImage.ImageIndex = 17
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        OptionsImage.Spacing = 7
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnInstalarServicioClick
      end
    end
    object cxDatosSistema: TcxTabSheet
      Caption = 'cxDatosSistema'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 5
      ParentFont = False
      TabVisible = False
      object lblDatosServWindows: TcxLabel
        Left = 67
        Top = 71
        Caption = 'Servidor de Windows:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxServidorWin: TcxTextEdit
        Left = 190
        Top = 71
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Width = 323
      end
      object lblDatosProcesadores: TcxLabel
        Left = 109
        Top = 225
        Caption = 'Procesadores:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxProcessors: TcxTextEdit
        Left = 190
        Top = 225
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 6
        Width = 323
      end
      object lblDatosVersion: TcxLabel
        Left = 138
        Top = 97
        Caption = 'Versi'#243'n:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxVersionSO: TcxTextEdit
        Left = 189
        Top = 97
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Width = 171
      end
      object lblDatosServidorBD: TcxLabel
        Left = 117
        Top = 147
        Caption = 'Servidor BD:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxServidorBD: TcxTextEdit
        Left = 190
        Top = 147
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 4
        Width = 323
      end
      object cxNETVersion: TcxTextEdit
        Left = 190
        Top = 199
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 5
        Width = 171
      end
      object lblDatosNetFramework: TcxLabel
        Left = 95
        Top = 199
        Caption = '.NET Framework:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxUserLogged: TcxTextEdit
        Left = 189
        Top = 122
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 3
        Width = 171
      end
      object lblDatosUsuario: TcxLabel
        Left = 137
        Top = 122
        Caption = 'Usuario:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxMemoriaRAM: TcxTextEdit
        Left = 190
        Top = 277
        ParentFont = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 8
        Width = 171
      end
      object lblDatosRAM: TcxLabel
        Left = 105
        Top = 276
        Caption = 'Memoria RAM:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxMemoriaVirtual: TcxTextEdit
        Left = 190
        Top = 328
        ParentFont = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 10
        Width = 171
      end
      object lblDatosMemVirtual: TcxLabel
        Left = 95
        Top = 328
        Caption = 'Memoria Virtual:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxMemoriaVirtualDisp: TcxTextEdit
        Left = 190
        Top = 353
        ParentFont = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 11
        Width = 171
      end
      object lblDatosMemVirtualDisp: TcxLabel
        Left = 36
        Top = 353
        Caption = 'Memoria Virtual Disponible:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object pnlDatosSistema: TPanel
        Left = 0
        Top = 479
        Width = 800
        Height = 175
        Align = alBottom
        TabOrder = 21
        object cxGridDatos: TcxGrid
          Left = 1
          Top = 1
          Width = 798
          Height = 173
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGridDatosDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            FilterBox.CustomizeDialog = False
            FilterBox.Visible = fvNever
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnGrouping = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object colDisco: TcxGridDBColumn
              Caption = 'Disco'
              DataBinding.FieldName = 'colDisco'
              MinWidth = 64
              Options.Editing = False
              Options.IncSearch = False
              Options.HorzSizing = False
              Options.Moving = False
              SortIndex = 0
              SortOrder = soAscending
              Width = 64
            end
            object colTamanio: TcxGridDBColumn
              Caption = 'Tama'#241'o (GB)'
              DataBinding.FieldName = 'colTamanio'
              MinWidth = 120
              Options.Editing = False
              Options.IncSearch = False
              Options.HorzSizing = False
              Options.Moving = False
              Width = 120
            end
            object colLibre: TcxGridDBColumn
              Caption = 'Libre (GB)'
              DataBinding.FieldName = 'colLibre'
              MinWidth = 120
              Options.Editing = False
              Options.IncSearch = False
              Options.HorzSizing = False
              Options.Moving = False
              Width = 120
            end
            object colPorcOcupado: TcxGridDBColumn
              Caption = 'Ocupaci'#243'n (GB)'
              DataBinding.FieldName = 'colPorcOcupado'
              MinWidth = 109
              Options.Editing = False
              Options.IncSearch = False
              Options.HorzSizing = False
              Options.Moving = False
              Width = 109
            end
          end
          object cxGridDatosLevel1: TcxGridLevel
            GridView = cxGridDatosDBTableView1
          end
          object cxGridDatosLevel2: TcxGridLevel
          end
        end
      end
      object lblDatosRecursos: TcxLabel
        Left = 101
        Top = 251
        Caption = 'Recursos Libres'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxRecursosLibres: TcxTextEdit
        Left = 190
        Top = 251
        ParentFont = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 7
        Width = 171
      end
      object cxMemoriaRAMDisp: TcxTextEdit
        Left = 190
        Top = 302
        ParentFont = False
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 9
        Width = 171
      end
      object lblDatosRAMDisp: TcxLabel
        Left = 46
        Top = 302
        Caption = 'Memoria RAM Disponible:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object cxExportar: TcxButton
        Left = 424
        Top = 350
        Width = 89
        Height = 26
        Hint = 'Exporta la informaci'#243'n del sistema'
        Caption = 'Exportar'
        OptionsImage.ImageIndex = 7
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        OptionsImage.Spacing = 6
        ParentShowHint = False
        ShowHint = True
        TabOrder = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = cxExportarClick
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 56
        Align = alTop
        TabOrder = 24
        object imgTitleDatosSistema: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            0164494441545847631805030EFEF7872B0C2466F83F29FAFF40620C07FC9B14
            FD9A28B18951DF41185D9C540C7700D092DFFF264798FCAFB767F93F31BA01AE
            08C4068AFD9B186D015203511BB5FF7F7FBC000803C50EC3D59281911D701E9A
            2C188096C9C0C5816CA83003480D54AC002A04525B00534B0E460981FF93A25C
            FECF8FE700B2BB91C4BB41626039B858D4F1FF53E2254018E880D33071723066
            1A9818FD992831509441A384128CE1007A63A42888DAFE7F72B403AD3130E73C
            C7EE8089D1F3A1E98AA600186DF7A9E200A04F4A80A1D60FE5120DA8E7804951
            8FC17A7B2245A0424401AA39E07F7F9401284EA15CA201D51C0002E0D2105846
            40B94401AA38E0FF84481B781490180A544C84D1CD60BD03E500788535EA8081
            4E03403A042A4414A08A03FE4D88D101E682D9203DA0D210542D43A50802AA85
            00B900B703C04DABA8045A63A03D28ED4BB803060A0F020700B3D1406268D218
            05030518180075EA96DF036240C00000000049454E44AE426082}
        end
        object lblTDatosSistema: TcxLabel
          Left = 44
          Top = 7
          Caption = 'Datos del Sistema'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object lblDatosSistemaDesc: TcxLabel
          Left = 44
          Top = 31
          Caption = 
            'Informaci'#243'n de los recursos f'#237'sicos del servidor en el que resid' +
            'e Sistema TRESS.'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object lblSQLNativeClient: TcxLabel
        Left = 83
        Top = 173
        Caption = 'SQL Cliente Nativo:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        AnchorX = 184
      end
      object txtSQLNativeClient: TcxTextEdit
        Left = 190
        Top = 173
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 26
        Text = 'No se encuentra versi'#243'n'
        Width = 170
      end
    end
    object cxResumen: TcxTabSheet
      Caption = 'cxResumen'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 0
      ParentFont = False
      TabVisible = False
      object PnlTResumen: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 56
        Align = alTop
        TabOrder = 0
        object imgTitleResumen: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            0133494441545847ED963F6EC23014872974E84099183A7293F6082C55957880
            9BB407402231D9390A63C78E1D73133FDE8B6CC7B59E81E60F41C83FE9130EFC
            6C7F38226204008362C37DF81FFC40F13E85EDC78265BF7AD2B53AA1854CDCCD
            54963E9CEDCB740D5200CB4EBCE95A1D7F41F7DA8C2D523C9A7128FD0A3884D2
            4AC0DD208814D56D08A5B1808FCA127BBF3942E94C00273CB3EF6B42E946808E
            D91350B918D36B35074FA71A30E94AE0EFE6593281CFD7899E72328D05F01BCE
            20AF7F661532B19BE278899D3D076C92B9AE351730F1AF4D50EE8B5D94C0A79C
            AEB51708250AF4267069FA3B8162F5A2647AE0A0C574AD47019CCC9609DC54D7
            A24014880251E00E059C47EE375B46941465DD1325D721680DDBCBC52FD72170
            8D1FD34353BE742D6E4080EEFD80D83F22C300A32345E61EA3E0615238000000
            0049454E44AE426082}
        end
        object lblTResumen: TcxLabel
          Left = 44
          Top = 7
          Caption = 'Resumen'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object lblResumenDesc: TcxLabel
          Left = 44
          Top = 31
          Caption = 'Resumen de los par'#225'metros de la configuraci'#243'n de Sistema TRESS.'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object PnlResumenConfig: TPanel
        Left = 17
        Top = 104
        Width = 352
        Height = 156
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object lblTResumenVersion: TcxLabel
          Left = 15
          Top = 1
          Caption = 'Versi'#243'n:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object lblResumenVersion: TcxLabel
          Left = 15
          Top = 20
          Caption = 'lblResumenVersion'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object lblTResumenConexion: TcxLabel
          Left = 15
          Top = 53
          Caption = 'Conexi'#243'n COMPARTE:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object txtResumenConexion: TcxRichEdit
          Left = 15
          Top = 73
          Enabled = False
          ParentFont = False
          Lines.Strings = (
            '')
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.Color = clWhite
          StyleDisabled.TextColor = clWindowText
          TabOrder = 3
          Height = 60
          Width = 329
        end
      end
      object PnlResumenSentinel: TPanel
        Left = 408
        Top = 104
        Width = 353
        Height = 156
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object lblTResumenPlataforma: TcxLabel
          Left = 16
          Top = 1
          Caption = 'Plataforma:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object lblTResumenSentinelVersion: TcxLabel
          Left = 16
          Top = 53
          Caption = 'Versi'#243'n:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object lblTResumenSentVencimiento: TcxLabel
          Left = 16
          Top = 102
          Caption = 'Vencimiento:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object lblResumenSentPlataforma: TcxLabel
          Left = 16
          Top = 20
          Caption = 'lblResumenSentPlataforma'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object lblResumenSentVersion: TcxLabel
          Left = 16
          Top = 73
          Caption = 'lblResumenSentVersion'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object lblResumenSentVencimiento: TcxLabel
          Left = 16
          Top = 119
          Caption = 'lblResumenSentVencimiento'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object cbResumensentEsDemo: TcxCheckBox
          Left = 250
          Top = 10
          Enabled = False
          Properties.Alignment = taRightJustify
          TabOrder = 6
          Width = 17
        end
        object lblResumenSentEsDemo: TcxLabel
          Left = 194
          Top = 9
          Caption = 'Es Demo:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 245
        end
        object cxTituloSentinelAutorizar: TcxLabel
          Left = 194
          Top = 89
          Caption = 'No Autorizado'
          ParentColor = False
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 275
        end
        object lblResumenSentServidor: TcxLabel
          Left = 194
          Top = 50
          Caption = 'Servidor:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          AnchorX = 243
        end
        object txtResumenSentServidor: TcxLabel
          Left = 194
          Top = 73
          Caption = 'txtResumenSentServidor'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object PnlResumenServicios: TPanel
        Left = 18
        Top = 323
        Width = 351
        Height = 163
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        object lblTResumenServiciosIni: TcxLabel
          Left = 16
          Top = 62
          Caption = 'Servicios Inicializados:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object lblResumenServiciosInst: TcxLabel
          Left = 15
          Top = 30
          Caption = 'lblResumenServiciosInst'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object lblResumenServiciosIni: TcxLabel
          Left = 15
          Top = 80
          Caption = 'lblResumenServiciosIni'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object lblTResumenServiciosInst: TcxLabel
          Left = 15
          Top = 16
          Caption = 'Servicios Instalados:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
      end
      object PnlResumenServidor: TPanel
        Left = 408
        Top = 323
        Width = 353
        Height = 163
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        object lblTResumenCompInst: TcxLabel
          Left = 16
          Top = 16
          Caption = 'Componentes Instalados:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object lblResumenCompInst: TcxLabel
          Left = 16
          Top = 30
          Caption = 'lblResumenCompInst'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
        object lblTResumenRutaServidor: TcxLabel
          Left = 16
          Top = 62
          Caption = 'Ruta Servidor Procesos:'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGradientActiveCaption
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.TextColor = clWindowText
          Style.IsFontAssigned = True
        end
        object txtResumenRutaServidor: TcxRichEdit
          Left = 15
          Top = 80
          Enabled = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.HotTrack = True
          Style.Shadow = False
          Style.TransparentBorder = True
          Style.IsFontAssigned = True
          StyleDisabled.BorderColor = clBtnShadow
          StyleDisabled.Color = clWhite
          StyleDisabled.TextColor = clWindowText
          TabOrder = 3
          Height = 60
          Width = 329
        end
      end
      object imgResumenConfigurar: TcxImage
        Left = 18
        Top = 72
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
          000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
          206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
          98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
          030B494441545847B55721741431103D51518140201008040281402010151548
          0402518144221088B2BBBC3DD14DF68EBE5781A8442010150804027102814054
          549C4054202A2A10080402FECF4E72C95D762F7BC7FDF7E6BDBBCC4CE6679299
          64079BC051595ED5B97E50E7EAC5A8D047BAD04FAA97D51D516F0E65596E2198
          AE73FDA72EF45F5F40E8847A318DE3B03CBC36CA0EEEC1705B8692A1F6D52D04
          3A9D0F4C591A1CE9B9AD0BF5D539610558C9EB5422B2F26F7E502B4B8373D53A
          57978163AE7F1BE7427D81F315316D05820C037F91A4B4C3F038707CA5DE9A43
          84E066922524A0DB8EEFB9BAE80A4E1D08EE92C059E8ACCEC7E5F83A83A690E0
          9909FD9D1C8BC90224F809887F2481EF74601A55A69EC96AA6A9248C4F18D808
          32F08381C4CCC1056F6C2E074C79F3470F695017D5E33E24745E15D4C584817C
          127E7023386B83BAAC6FE0C74F3390A9E734EC434215EAA19B302648339B9029
          D35C7FF075AC1C330958ED42F9CB287A926015D909FB0ABAE41BCE6150E7F5CE
          AA2470702776D23EC285D3DF615512BAD4379D5FA204ABF7B132095C3862130D
          3827A7F069EFB2C924A4835A127556DFC77F53D66D92DCE297911815D523BF85
          5B1294A69FA877183F833D2A4C7D6660123493A7A285C49E0DEA8B25601CFF27
          0212786098B293CB6AE3C109EE5740624EBCD4F77E437402136EB154DC018B90
          703A9C0DF67EBE2BC47D3D30B8DFB36324EC18ECF0F66BCA0F242ED626D1040A
          7BB61FD09028F427FEB6F6D439BB7548CC4F362F9684983BC4488CB383BBA24E
          039B4B57702BA9248C2D88608BDE43D77D40F90483C3D477EE12361A710D2024
          668F5B1136213189C33E4A8CB0C63B7A7AEB25223077C2BC1FE76BEB82643D33
          5613B315BCDD22196165C0BEF3952BDF07819FC834BA15E1A3B2DA936174C06A
          DF7386A8C9B2E016203A8C6531BA15B2FF8D11CA8F2C991518BB0F0DFEE698B8
          24815500DFF04BA96D2B82A683930BC3E68DD888B97EC5B417983193C9F0EE98
          8A7A0679D72D7CD7F144AF1ADC47F0E927B7EA02C896E565B281BB5C67FA29C7
          44BD369A6CD43BF2573018FC03A0161182AEC6A9650000000049454E44AE4260
          82}
        Style.BorderColor = clWhite
        TabOrder = 5
        Height = 30
        Width = 30
      end
      object lblResumenConfigurar: TcxLabel
        Left = 54
        Top = 76
        Caption = 'Configurar'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
      end
      object lblResumenSentinel: TcxLabel
        Left = 447
        Top = 76
        Caption = 'Guardia'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
      end
      object lblResumenServicios: TcxLabel
        Left = 54
        Top = 289
        Caption = 'Servicios'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
      end
      object lblResumenServidorProcesos: TcxLabel
        Left = 447
        Top = 289
        Caption = 'Servidor de Procesos'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -15
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
      end
      object imgResumenSentinel: TcxImage
        Left = 411
        Top = 72
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
          000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
          206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
          98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
          0251494441545847E5562B4C034110AD40201008040281402010082402814020
          91480402812CB7479604BAB32D24884A241281405420100824A2A21281A84020
          2A100898779D92BDEB1EEDDDB50D092F99F4AE333B6F777E7BA57F01ADF5CCA5
          D6B3F23A59809C42F368436A4D7C13355D9B0731CB17041BC186443D5E24C91D
          6988C9F85039AA2CDBD0BC78C821CF1C8529311D3D404ECAB43DC43F428A6EC6
          B2896A70B696465E0DE9D206E6D079AFCBB2D1C02ABB6E15755CD29840C73614
          D279EF3F3CCBF262E0906E78C9157D9AC0ECF373B71815BD23455134BA36C50B
          D286951D10894397BC6394D9828D5B17BDD06363855BF23772845BCC22C8C9EB
          43919AB259928269483B3DF3EF0342570BCE566113E93DE47CD2B724F9D0406B
          706190F75471F10D1890B7715271970D56DB0576C227ED773C8C142207D80987
          DBEF7C086991A64571951DEE804848137997B4DCA5A5A65717B910159CA28F98
          53AE620A684F4C7E0022D6F7A74955CA62921D204A3A448F8ABA0FB8D33DE3B6
          25EAECB0C7E6CA75C677F693A852E1DB3417E08AA8B38117375D47C8B9A85281
          6A77D740308E459D0DBC38D1D3F65454A9C0348BAF29B20145B73167FC2EAA54
          60C6C7D6B0E02A167536442DE63AE3561B3450D8E6CE5D83F19BFB038343B7E9
          3A8B1C7221A67DC172871C24EDAD32D7A2CE87E489207CAA578ECE2E46F485BE
          98933BBFCFAE6B9B33FF3D608CB2F3F4AF995F04291437C5109D76F02D9810F3
          903BF73EC8A8F55EB531E18D72D84F987C5A968E0E702A9D111B5042CC6932F7
          85AEDD2C4027A0E74D68B60B5DB77F17A5D2377771458E234BE5FE0000000049
          454E44AE426082}
        Style.BorderColor = clWhite
        TabOrder = 10
        Height = 30
        Width = 30
      end
      object imgResumenServicios: TcxImage
        Left = 18
        Top = 287
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
          000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
          206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
          98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
          00C849444154584763E8A869BBDF51D3FE1F27AE6D9BCF404B30EA004A1CD05E
          DDDE80550FD1B8EDFED07640674D6B00489E5CDC5ED3DE3D9A08193AAADA0A40
          71890B838219AA7414D006745475580083DA01176EAD6CD5802AA50D18CD0594
          38A0BDBE5D015BB4118B41D14F990380D914AB1EA2F150AF0BBAAA5A0C80E579
          02B9B8A3A63564682742AA80012F8846012825624BA130DC51DD6103554A1B30
          9A0B28710034A7606D49118341AD314A1D30C2EB82FEFA7A0170954C26EEA8EF
          9019DA899072C0C000005F3AFC42B0FA7BA50000000049454E44AE426082}
        Style.BorderColor = clWhite
        TabOrder = 11
        Height = 30
        Width = 30
      end
      object imgResumenServidor: TcxImage
        Left = 411
        Top = 287
        Picture.Data = {
          0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
          000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
          206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
          98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
          02DE494441545847CD5621701431145D81A8402010880A040281402010880A04
          028140209008040281289BED2C337493DCD1990AC4090402814054202A100804
          12515181A840202A2A100804BCF7F377C966F70A77DD3B78336F66EFBF9FFF73
          F9C94FB2FF066559AE78E3AF8DF3CDCB6A6AC1AEDB0BCEB835F89D565303DAA8
          558FAB8B6A6AA18EED4BBFAA26DA4E8D0AF75C7E50F0853DF085FB29346E870E
          2202BEF04F6BCD197B38CA37AFA894F9DC5FA5AD190B5F95049DD8859B303626
          FC86BFC58933891C84A3A2BA458DFF3CD55C613FC84080DFA9CE312A67DED857
          A90EEED5DFEAE4DE4662A0B18FA861A6D73B1AFE910C04F0EFBFA43ACBA13256
          CFBE4BF598C129B70F5B82713FEABD10EA1B2FB1702203017EC71A7D3946E5CC
          E6F67EACA71427A949E11C0C9FC14FBEA86E8BA0903ACB524B2D59C326C17659
          9EA18D1A7C3ED257A50688FD8CC9FAA82E8BC3B81C9F43A2FD34714D755B0CFE
          949C5457D4CAD81B7EC3BEC089D876A53BAF664158661CC50DFB12E5B9A3E606
          B45173B9BB5797873190E0D8E4A404B085BDD9128C3BE2ECA9717FC0D61C9BA0
          8713427853ADB735B7C38684CDF8B5659F420952378598D8BD0F420274B04403
          F76520806F6EDCB66EDCF78E6D0A250826F03A15B89CD4D8F5520DDC9381405F
          239A851284478767FFB7600F58771101FC7E1F0FC2B1BAAB52A87F6BEC6CD430
          5986BA5D72A62AD894E2E484F409AC0856EA49DF398FEF8A59A921E6079BD620
          2B300FA4CD9E20392981F4A84D10EC88974BBD016B488FE05184CEDB8D67BD73
          7FCC494920B54F45ADF556B9751689BF257AF7E8CD4999003E7653810D861AAF
          D68E362065026CBFA9503F486405126D48CA043409AE6115506711146CBD8D36
          3035856CC4153E42E2E714213DA0A7550F454DD30FD9ED49171C9A9A2A948175
          E7E5C37FCDE427EDF37F4349CEBB3B795CEE2E23392913E043A44F5C06C304FA
          DFEE4BA14C60D1CDE638CA04A6BC7A96C290BCDBEB97467972B104FF866EED17
          CEE05E4FD69F28B40000000049454E44AE426082}
        Style.BorderColor = clWhite
        TabOrder = 12
        Height = 30
        Width = 30
      end
    end
    object cxUsuariosSistema: TcxTabSheet
      Caption = 'cxUsuariosSistema'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 5
      ParentFont = False
      TabVisible = False
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 56
        Align = alTop
        TabOrder = 0
        object Image1: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            097048597300000EC300000EC301C76FA86400000257494441545847ED552D6C
            536114AD4020100804028140201008C4040988C90944097BEF6BD24A046202B9
            6424130848FAF65A87989C444C2010080402814020261013880A040241F27D3B
            E77EF7B5EFE7F2DA0D4642F24E72D2E6DDF3EE7F6F7B1D3A9C15613CBC1CF274
            E4F75C3FECDCBBA08FCF1F0C86A0BBE08F90BB40FA3C3DF659FAF89F248260E3
            22709DB06DABEC7C80AAAFF9DCFDB28293B0CD381A959F1EDADE2D387A8FCF8F
            61CF3D0BFBC38B6A6EADBEA0CFDC40E502F8E9836FF1EE67F0158B5053131434
            1CE26535D3D97EDDDE643A5239F4E9D3BA1DCFBE8597C915952CE0B3C1ADBAB8
            20166C831AE988612FD34F36EF88169D43B09FB6267D4E4D056C9D25268BE562
            E6F87E6C69488CEE509C01619CDEB634247CBC51D90268EF9A25267D966CAA0C
            89A61B56657876E4A7831B2AEB85E9C34B754D41C49AA8AC0A64F6CE107F292F
            2211A6C3AB9C2F7722EE453AB26E000335FD31F9E4A64AAA882D760714F1E706
            07AFC1F9D6EAF55B6770D987123942EE914A05B207B97B81C2BE4BF0DC7D82BF
            353557C1967181E4BCE2AAF93C7912BF47A778B10F07B37A45754A17C78FAECB
            3BF017774B4EF696742A4BEE3216ED738489BB0FE75F1BCED82E388BC9546D6D
            64C5EC5C0C6AD9DD8C05C5E098479C8B21C4CF451228DDFD5589208772D88CC2
            E644E1583C77601A49B40BD56C9BB615285D308E5B41D83EF05A1D594621F702
            8B68DA5662F2A06D7CEC3C46F01B235AC711B526B894583AA1658B5C9E40DB0C
            97B24BE0BF48A076520BF288FC9504F8AF68F82FC818ADF8D304D4CDD9C193B9
            68E5E958FE7BEED0C146AF7702085DF9CDE403AC470000000049454E44AE4260
            82}
        end
        object cxLabel1: TcxLabel
          Left = 44
          Top = 7
          Caption = 'Usuarios'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object cxLabel2: TcxLabel
          Left = 44
          Top = 31
          Caption = 
            'Estatus de los usuarios dentro del sistema y desbloqueo del usua' +
            'rio Administrador.'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object cxGroupBox1: TcxGroupBox
        Left = 3
        Top = 62
        Caption = 'Usuarios dentro del sistema:'
        TabOrder = 1
        Height = 251
        Width = 583
        object cxGridUsuariosSistema: TcxGrid
          Left = 8
          Top = 17
          Width = 565
          Height = 217
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGridUsuariosSistemaDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            FilterBox.Visible = fvNever
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnAddValueItems = False
            OptionsCustomize.ColumnGrouping = False
            OptionsCustomize.ColumnHidingOnGrouping = False
            OptionsCustomize.ColumnMoving = False
            OptionsCustomize.ColumnSorting = False
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object cxGridDBColumn1: TcxGridDBColumn
              Caption = 'Usuario'
              DataBinding.FieldName = 'colUsuario'
              MinWidth = 100
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.Moving = False
              Options.Sorting = False
              Width = 100
            end
            object cxGridDBColumn2: TcxGridDBColumn
              Caption = 'Nombre'
              DataBinding.FieldName = 'colName'
              MinWidth = 350
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
              Options.Sorting = False
              Width = 350
            end
            object cxGridDBColumn3: TcxGridDBColumn
              Caption = 'Desde'
              DataBinding.FieldName = 'colDesde'
              MinWidth = 85
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.IncSearch = False
              Options.AutoWidthSizable = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
              Options.Sorting = False
              Width = 85
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGridUsuariosSistemaDBTableView1
          end
        end
      end
      object btnDesbloquearAdmin: TcxGroupBox
        Left = 3
        Top = 319
        Caption = 'Usuario Administrador'
        TabOrder = 2
        Height = 74
        Width = 583
        object btnSacarUsuarios: TcxButton
          Left = 16
          Top = 24
          Width = 153
          Height = 26
          Hint = 'Desbloquear grupo de acceso de administradores'
          Caption = 'Desbloquear Admin.'
          OptionsImage.ImageIndex = 18
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = btnSacarUsuariosClick
        end
      end
      object btnAplicarFueraSistema: TcxButton
        Left = 590
        Top = 73
        Width = 200
        Height = 26
        Hint = 
          'Actualizara el estatus "dentro del sistema" de todos los usuario' +
          's'
        Caption = 'Aplicar fuera del sistema (todos)'
        OptionsImage.ImageIndex = 19
        OptionsImage.Images = ImageButtons
        OptionsImage.Margin = 1
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
        OnClick = btnAplicarFueraSistemaClick
      end
      object Panel2: TPanel
        Left = 0
        Top = 613
        Width = 800
        Height = 41
        Align = alBottom
        BevelInner = bvLowered
        BevelOuter = bvNone
        BorderWidth = 1
        Color = clBtnHighlight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 4
        object Label4: TLabel
          Left = 185
          Top = 14
          Width = 113
          Height = 13
          Cursor = crHandPoint
          Hint = 'Ir a Carpetas Compartidas'
          Caption = 'Carpetas Compartidas'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = Label4Click
        end
        object Label3: TLabel
          Left = 26
          Top = 14
          Width = 113
          Height = 13
          Cursor = crHandPoint
          Hint = 'Ir a Usuarios de Windows'
          Caption = 'Usuarios de Windows'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clHotLight
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          OnClick = Label3Click
        end
        object Image3: TImage
          Left = 7
          Top = 12
          Width = 17
          Height = 17
          Cursor = crHandPoint
          Hint = 'Ir a usuarios de windows'
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
            0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000000
            097048597300000EC300000EC301C76FA8640000012849444154384FCD51A156
            C340103C81402090084405920F402090483E01894022704B629ADE5E5E44EB10
            15082412814022110804028940201015113073D9D2CBD1F7C0C1BC37EF92D9DD
            D9BD3DF7BF1024EC6AA1E7E09988AC9AFC3BA8E87628B4053F3AFA4B0B31760C
            D32B2FFE04C62B26F7C1AE8BE2C8967A107FDAD34B3F8E0539D8254DD4C2BF52
            C7240F99FE1E0B7234D2AC23F8F89528A323EAD09E5203304EB6141399AC0519
            1D701F26B9506A93195C5B68012C679FDBE77DE32430A80B9DF29BA63156E833
            7881DC0D2BEBC08E893B3903F91AB3682CBA67A98E2F504BB5934E8825E98D15
            F629FED03AF3FF5E4B1DE27C99C711BB8BD360496F7331615B4BBDC533D37BE4
            35B1FDE1206725D526C75F569492CDED22DFD119F8DB9F68E97F06E73E011972
            309CB488847E0000000049454E44AE426082}
          OnClick = Image3Click
        end
        object Image4: TImage
          Left = 166
          Top = 14
          Width = 17
          Height = 17
          Cursor = crHandPoint
          Hint = 'Ir a carpetas compartidas'
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000010
            0000001008060000001FF3FF610000000467414D410000B18F0BFC6105000000
            097048597300000EC300000EC301C76FA8640000009849444154384F632006BC
            DCC8D0F07213C37F0C0C146778B99EC1E0F94686047C18A8703D6E0370994E0C
            0619F07C338303D8103230482FD49714008A5D00E660F31F3118A877D4006A18
            F07C3D8302283AB0E1179B18B6BFDACCD08E4D0E8C817AA1A9013B78B589613E
            282F40B9A403A013D7BFD8C8D00DE5E20640851EE839F0F15A0619A0E6C32043
            8058055DFED9068610060606060017E4D5273FBEE3940000000049454E44AE42
            6082}
          OnClick = Image4Click
        end
      end
    end
    object cxFacilitadores: TcxTabSheet
      Caption = 'cxFacilitadores'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ImageIndex = 5
      ParentFont = False
      TabVisible = False
      object cxLabel3: TcxLabel
        Left = 505
        Top = 70
        Caption = 'Servicios de Windows:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        Visible = False
        AnchorX = 624
      end
      object cxTextEdit1: TcxTextEdit
        Left = 630
        Top = 69
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Visible = False
        Width = 171
      end
      object cxLabel5: TcxLabel
        Left = 504
        Top = 93
        Caption = 'Usuarios de Windows:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        Visible = False
        AnchorX = 624
      end
      object cxTextEdit3: TcxTextEdit
        Left = 629
        Top = 95
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 2
        Visible = False
        Width = 171
      end
      object cxLabel6: TcxLabel
        Left = 482
        Top = 145
        Caption = 'Servicios de Componentes:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        Visible = False
        AnchorX = 624
      end
      object cxTextEdit4: TcxTextEdit
        Left = 630
        Top = 145
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 4
        Visible = False
        Width = 170
      end
      object cxTextEdit6: TcxTextEdit
        Left = 629
        Top = 120
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 3
        Visible = False
        Width = 171
      end
      object cxLabel8: TcxLabel
        Left = 510
        Top = 120
        Caption = 'Folders Compartidos:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        Visible = False
        AnchorX = 624
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 56
        Align = alTop
        TabOrder = 8
        object Image2: TImage
          Left = 3
          Top = 2
          Width = 35
          Height = 35
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
            000000200806000000737A7AF40000000467414D410000B18F0BFC6105000000
            206348524D00007A26000080840000FA00000080E8000075300000EA6000003A
            98000017709CBA513C000000097048597300000EA600000EA60187DCA96F0000
            0164494441545847631805030EFEF7872B0C2466F83F29FAFF40620C07FC9B14
            FD9A28B18951DF41185D9C540C7700D092DFFF264798FCAFB767F93F31BA01AE
            08C4068AFD9B186D015203511BB5FF7F7FBC000803C50EC3D59281911D701E9A
            2C188096C9C0C5816CA83003480D54AC002A04525B00534B0E460981FF93A25C
            FECF8FE700B2BB91C4BB41626039B858D4F1FF53E2254018E880D33071723066
            1A9818FD992831509441A384128CE1007A63A42888DAFE7F72B403AD3130E73C
            C7EE8089D1F3A1E98AA600186DF7A9E200A04F4A80A1D60FE5120DA8E7804951
            8FC17A7B2245A0424401AA39E07F7F9401284EA15CA201D51C0002E0D2105846
            40B94401AA38E0FF84481B781490180A544C84D1CD60BD03E500788535EA8081
            4E03403A042A4414A08A03FE4D88D101E682D9203DA0D210542D43A50802AA85
            00B900B703C04DABA8045A63A03D28ED4BB803060A0F020700B3D1406268D218
            05030518180075EA96DF036240C00000000049454E44AE426082}
        end
        object cxLabel14: TcxLabel
          Left = 44
          Top = 7
          Caption = 'Facilitadores de Windows'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clGrayText
          Style.Font.Height = 21
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = [fsBold]
          Style.IsFontAssigned = True
        end
        object cxLabel15: TcxLabel
          Left = 44
          Top = 31
          Caption = 
            'Accesos directos a las principales aplicaciones para el control ' +
            'en Windows'
          ParentColor = False
          ParentFont = False
          Style.Color = clWhite
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Segoe UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
        end
      end
      object cxTextEdit12: TcxTextEdit
        Left = 630
        Top = 171
        ParentFont = False
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Style.Color = clInfoBk
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 9
        Visible = False
        Width = 170
      end
      object cxLabel16: TcxLabel
        Left = 535
        Top = 172
        Caption = '.NET Framework:'
        ParentColor = False
        ParentFont = False
        Style.Color = clWhite
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        Visible = False
        AnchorX = 624
      end
      object cxGroupBox2: TcxGroupBox
        Left = 39
        Top = 70
        Caption = 'Generales'
        TabOrder = 11
        Height = 114
        Width = 437
        object cxButton6: TcxButton
          Left = 211
          Top = 26
          Width = 175
          Height = 26
          Hint = 'Inicia el servicio seleccionado'
          Caption = 'Visor de Eventos'
          OptionsImage.ImageIndex = 12
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = cxButton6Click
        end
        object cxButton5: TcxButton
          Left = 17
          Top = 26
          Width = 175
          Height = 26
          Hint = 'Inicia el servicio seleccionado'
          Caption = 'Editor de Registro'
          OptionsImage.ImageIndex = 12
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = cxButton5Click
        end
      end
      object cxGroupBox5: TcxGroupBox
        Left = 39
        Top = 198
        Caption = 'Servicios'
        TabOrder = 12
        Height = 115
        Width = 437
        object cxButton4: TcxButton
          Left = 17
          Top = 29
          Width = 175
          Height = 26
          Hint = 'Inicia el servicio seleccionado'
          Caption = 'Servicios de Componentes'
          OptionsImage.ImageIndex = 12
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = btnComponentServicesClick
        end
        object cxButton1: TcxButton
          Left = 211
          Top = 29
          Width = 175
          Height = 26
          Hint = 'Inicia el servicio seleccionado'
          Caption = 'Servicios del Sistema'
          OptionsImage.ImageIndex = 12
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = btnServiciosWindowsClick
        end
      end
      object cxGroupBox6: TcxGroupBox
        Left = 39
        Top = 336
        Caption = 'Usuarios'
        TabOrder = 13
        Height = 105
        Width = 437
        object cxButton2: TcxButton
          Left = 17
          Top = 20
          Width = 175
          Height = 26
          Hint = 'Inicia el servicio seleccionado'
          Caption = 'Usuarios locales y grupo'
          OptionsImage.ImageIndex = 12
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = btnUsuarioWindowsClick
        end
        object cxButton3: TcxButton
          Left = 211
          Top = 20
          Width = 175
          Height = 26
          Hint = 'Inicia el servicio seleccionado'
          Caption = 'Folders Compartidos'
          OptionsImage.ImageIndex = 12
          OptionsImage.Images = ImageButtons
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 7
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Segoe UI'
          Font.Style = []
          ParentFont = False
          OnClick = btnSharedFoldersClick
        end
      end
    end
  end
  object dxSkinController1: TdxSkinController
    Kind = lfFlat
    NativeStyle = False
    SkinName = 'TressMorado2013'
    Left = 120
    Top = 520
  end
  object dxBarManager1: TdxBarManager
    AutoAlignBars = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.ImageListBkColor = clGrayText
    ImageOptions.LargeImages = cxLargeImageMenu
    ImageOptions.LargeIcons = True
    LookAndFeel.Kind = lfUltraFlat
    MenusShowRecentItemsFirst = False
    PopupMenuLinks = <>
    ShowFullMenusAfterDelay = False
    ShowShortCutInHint = True
    UseSystemFont = False
    Left = 72
    Top = 416
    DockControlHeights = (
      200
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Config'
      CaptionButtons = <
        item
        end>
      Color = clBtnHighlight
      DockedDockingStyle = dsLeft
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsLeft
      FloatLeft = 234
      FloatTop = 115
      FloatClientWidth = 200
      FloatClientHeight = 260
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Orientation = 1
      Font.Pitch = fpFixed
      Font.Style = []
      Images = cxLargeImageMenu
      IsMainMenu = True
      ItemLinks = <
        item
          Visible = True
          ItemName = 'lgbResumen'
        end
        item
          Visible = True
          ItemName = 'lgbConfigurar'
        end
        item
          Visible = True
          ItemName = 'lgbSentinel'
        end
        item
          Visible = True
          ItemName = 'lgbModulos'
        end
        item
          Visible = True
          ItemName = 'lgbServidor'
        end
        item
          Visible = True
          ItemName = 'lgbServicios'
        end
        item
          Visible = True
          ItemName = 'lgbDatos'
        end
        item
          Visible = True
          ItemName = 'lgbUsuarios'
        end
        item
          Visible = True
          ItemName = 'lgbFacilitadores'
        end
        item
          Visible = True
          ItemName = 'lgbSalir'
        end>
      MultiLine = True
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      RotateWhenVertical = False
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = True
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object lgbResumen: TdxBarLargeButton
      Caption = 'Resumen'
      Category = 0
      HelpContext = 2
      Hint = 'Resumen de la instalaci'#243'n'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 16
      OnClick = lgbResumenClick
      GlyphLayout = glLeft
      Width = 200
    end
    object lgbConfigurar: TdxBarLargeButton
      Caption = 'Configurar'
      Category = 0
      Hint = 'Configuraci'#243'n de acceso y actualizaci'#243'n del sistema'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 13
      OnClick = lgbConfigurarClick
      GlyphLayout = glLeft
      Width = 200
      SyncImageIndex = False
      ImageIndex = 2
    end
    object lgbSentinel: TdxBarLargeButton
      Caption = 'Guardia'
      Category = 0
      Hint = 'Consulta y configuraci'#243'n de datos de Guardia'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 20
      OnClick = lgbSentinelClick
      GlyphLayout = glLeft
      Width = 200
    end
    object lgbModulos: TdxBarLargeButton
      Caption = 'M'#243'dulos'
      Category = 0
      Hint = 'Consulta de m'#243'dulos autorizados y prestados del sistema'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 15
      OnClick = lgbModulosClick
      GlyphLayout = glLeft
      Width = 200
    end
    object lgbServidor: TdxBarLargeButton
      Align = iaRight
      Caption = 'Servidor de Procesos'
      Category = 0
      Hint = 'Informaci'#243'n del Servidor de Procesos'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 18
      OnClick = lgbServidorClick
      GlyphLayout = glLeft
      Width = 200
    end
    object lgbServicios: TdxBarLargeButton
      Caption = 'Servicios'
      Category = 0
      Hint = 'Servicios configurados de Sistema TRESS'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 19
      OnClick = lgbServiciosClick
      GlyphLayout = glLeft
      Width = 200
    end
    object lgbDatos: TdxBarLargeButton
      Caption = 'Datos del Sistema'
      Category = 0
      Hint = 'Consultar datos generales del servidor'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 14
      OnClick = lgbDatosClick
      GlyphLayout = glLeft
      Width = 200
    end
    object lgbSalir: TdxBarLargeButton
      Caption = 'Salir'
      Category = 0
      Hint = 'Salir de la aplicaci'#243'n'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 21
      PaintStyle = psCaptionGlyph
      OnClick = lgbSalirClick
      AutoGrayScale = False
      GlyphLayout = glLeft
      Width = 200
      SyncImageIndex = False
      ImageIndex = -1
    end
    object lgbUsuarios: TdxBarLargeButton
      Caption = 'Usuarios'
      Category = 0
      Hint = 'Consultar usuarios dentro de Sistema TRESS'
      Style = cxStyle1
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 23
      OnClick = lgbUsuariosClick
      GlyphLayout = glLeft
      Width = 200
    end
    object lgbFacilitadores: TdxBarLargeButton
      Caption = 'Facilitadores'
      Category = 0
      Hint = 'Muestra lista de accesos directos para windows.'
      Style = cxStyle1
      Visible = ivNever
      ButtonStyle = bsChecked
      LargeImageIndex = 13
      OnClick = lgbFacilitadoresClick
      GlyphLayout = glLeft
      Width = 200
    end
  end
  object cxOpenDialog: TcxShellBrowserDialog
    FolderLabelCaption = 'Configuracion'
    Options.TrackShellChanges = False
    ShowButtons = False
    Title = 'Buscar folder Configuraci'#243'n'
    Left = 102
    Top = 386
  end
  object cxLargeImageMenu: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 31719569
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001E1C1F382320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          23402320234004040408000000000000000000000000000000005F595FAF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF897F8AFB0706070C000000000000000000000000000000002B282C508B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5F595FAF000000005D575EAB8B818CFF8B818CFF1C1A1C343B363B6C8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF716972CF0000000000000000000000000000000000000000000000006F67
          6FCB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF3D383D700000000029262A4C8B818CFF676169BF00000000161416288B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF433E447C0000000000000000000000000000000000000000000000002320
          23408B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF1E1C1F3800000000020202047E757FE72B282C5000000000000000007A71
          7ADF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8179
          82EF090809100000000000000000000000000000000000000000000000000000
          0000413C42788B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF847B
          84F30202020400000000000000003B363B6C000000000000000000000000544E
          549B8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF3632
          3764000000000000000000000000000000000000000000000000000000000000
          0000000000004E494F8F8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF6761
          69BF000000000000000000000000595259A30D0C0D180000000000000000302C
          30588B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF484348830000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000005A545CA78B818CFF8B818CFF8B818CFF8B818CFF4843
          48830000000000000000232023408B818CFF504B519300000000000000000B0A
          0B148B818CFF8B818CFF8B818CFF8B818CFF897F8AFB413C4278000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002020204363237647E757FE78B818CFF8B818CFF2724
          274800000000000000002320234034303560322E325C00000000000000000000
          00006F676FCB8B818CFF8B818CFF726B74D32320234000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000706070C272427484C474D8B0706
          070C000000000000000000000000000000000000000000000000000000000000
          000048434883665F67BB302C3058020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000202
          02043D383D705F595FAF7C737DE38B818CFF716972CF4E494F8F232023400000
          0000090809102320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234011101220000000000000
          0000000000000000000000000000000000000000000000000000161416287169
          72CF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF3F3A3F740000
          00005D575EAB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF847B84F3000000000000
          0000000000000000000000000000000000000000000016141628847B84F38B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF595259A35A54
          5CA78B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          00000000000000000000000000000000000002020204716972CF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF232023403430
          35608B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000003D383D708B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF232023403430
          35608B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000005F595FAF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF232023403430
          35608B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000007C737DE38B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF6F676FCB020202040D0C
          0D18817982EF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000020202048B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF645D64B7000000000000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          000000000000000000000000000000000000776F77DB8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF534D5397534D
          53978B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000005D575EAB8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          000000000000000000000000000000000000343035608B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF4E494F8F0000
          00004C474D8B8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF726B74D3000000000000
          00000000000000000000000000000000000000000000665F67BB8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7F7780EB0202
          0204000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0E0F1C7E757FE78B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF746D
          75D71C1A1C340B0A0B14343035600706070C0000000000000000000000000000
          00000000000000000000000000001C1A1C342724274800000000000000000000
          00000000000000000000000000000000000000000000000000000B0A0B14625B
          62B38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF897F8AFB4C47
          4D8B04040408090809108B818CFF211E213C0000000000000000000000000000
          00000000000000000000000000005F595FAF544E549B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002B282C504E494F8F6A636AC37A717ADF625B62B3484348831C1A1C340000
          00000000000000000000746D75D7534D53970000000000000000000000000000
          000000000000000000000F0E0F1C817982EF3834396800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000029262A4C8B818CFF2E2A2E5400000000000000000000
          0000000000000706070C645D64B7746D75D70404040800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000484348838B818CFF5750589F2E2A2E542320
          23403B363B6C776F77DB7C737DE31C1A1C340000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000232023406A636AC3847B84F38B81
          8CFF7C737DE3534D539709080910000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000B0A
          0B14000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000001010304222960783945A2CB4554C1F33A47A6CF2932738F05060D100000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000010132D38232B
          637C2D37809F232B637C1115303C000000000000000000000000000000000607
          10143A47A6CF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF3F4DB6E31115
          303C000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000003040A0C2C357B9B4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF2D3882A303040A0C0000000000000000303A
          89AB4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4554
          C1F31115303C0000000000000000000000000000000000000000000000000000
          000000000000000000000000000003040A0C36439BC34858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF2D37809F000000000C0F232C4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4554C1F31115303C00000000000000000000000000000000000000000000
          0000000000000000000003040A0C36439BC34858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF10132D381C2250644858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4554C1F31115303C000000000000000000000000000000000000
          00000000000003040A0C36439BC34858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF21285C74202759704858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4554C1F31115303C0000000000000000000000000000
          000003040A0C36439BC34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF2B3479971115303C4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4554C1F31115303C00000000000000000304
          0A0C36439BC34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF1E25566C010103043A47
          A6CF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4554C1F31115303C03040A0C3643
          9BC34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4654C6F7090B1A20000000000E11
          26304757C9FB4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4554C1F33945A2CB4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF21285C7400000000000000000000
          0000181D43544757C9FB4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF303A89AB0101030400000000000000000000
          000000000000181D43544757C9FB4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF303A89AB010103040000000000000000000000000000
          00000000000000000000181D43544757C9FB4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF303A89AB01010304000000000000000000000000000000000000
          0000000000000000000000000000181D43544757C9FB4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF303A89AB0101030400000000000000000000000000000000000000000000
          000000000000000000000000000000000000181D43544757C9FB4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF303A
          89AB010103040000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000181D43544757C9FB4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF303A89AB0101
          0304000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001B214D604858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF354196BB010103040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000003040A0C36439BC34858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4554C1F31115303C0000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000003040A0C36439BC34858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4554C1F31115
          303C000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000003040A0C36439BC34858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4554
          C1F31115303C0000000000000000000000000000000000000000000000000000
          0000000000000000000003040A0C36439BC34858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4554C1F31115303C00000000000000000000000000000000000000000000
          00000000000003040A0C36439BC34858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4554C1F31115303C000000000000000000000000000000000000
          000003040A0C36439BC34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4554C1F31115303C0000000000000000000000000304
          0A0C36439BC34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4554C1F31115303C0000000000000000333E
          8EB34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF303A89AB121633404554
          C1F34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF3F4DB6E305060D101B214D604858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF303A89AB01010304000000001115
          303C4554C1F34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF2B347997313C8BAF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF303A89AB0101030400000000000000000000
          00001115303C4554C1F34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF3E4CAEDB3E4CAEDB4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF303A89AB010103040000000000000000000000000000
          0000000000001115303C4554C1F34858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF3945A2CB4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF303A89AB01010304000000000000000000000000000000000000
          000000000000000000001115303C4554C1F34858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF3E4CAEDB2A3376934858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF303A89AB0101030400000000000000000000000000000000000000000000
          00000000000000000000000000001115303C4554C1F34858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF2B347997080A161C4352
          BEEF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF303A
          89AB010103040000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001115303C4554C1F34858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF3F4DB6E305060D10000000001419
          39484150BBEB4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF2A3376930101
          0304000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000E1126303A47A6CF4858
          CCFF4858CCFF4858CCFF4858CCFF37459EC70E11263000000000000000000000
          000006071014262F6B87354196BB3A47A6CF2C357B9B12163340000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000001010304151A
          3D4C2229607821285C7413173644010103040000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000020202043B363B6C5D575EAB595259A33632376402020204000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000002020204211E213C00000000000000000000000000000000000000000B0A
          0B14716972CF8B818CFF8B818CFF8B818CFF8B818CFF6C656CC70D0C0D180000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001614
          16286C656CC78B818CFF3D383D70000000000000000000000000000000005D57
          5EAB8B818CFF8B818CFF7E757FE7817982EF8B818CFF8B818CFF746D75D70D0C
          0D18000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000002020204413C4278877D
          88F78B818CFF8B818CFF746D75D7000000000000000000000000090809108B81
          8CFF8B818CFF5F595FAF040404080706070C6F676FCB8B818CFF8B818CFF6F67
          6FCB0706070C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001A181A30716972CF8B818CFF8B81
          8CFF8B818CFF897F8AFB1E1C1F380000000000000000000000001C1A1C348B81
          8CFF8B818CFF302C30580000000000000000413C42788B818CFF8B818CFF8B81
          8CFF676169BF0202020400000000000000000000000000000000000000000000
          000000000000000000000000000046414680897F8AFB8B818CFF8B818CFF8B81
          8CFF8B818CFF4C474D8B000000000000000000000000000000000706070C8B81
          8CFF8B818CFF676169BF0D0C0D1811101220746D75D78B818CFF8B818CFF8B81
          8CFF8B818CFF5D575EAB02020204000000000000000000000000000000000000
          00000000000000000000141214248B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF746D75D70404040800000000000000000000000000000000000000005750
          589F8B818CFF8B818CFF877D88F7897F8AFB8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF504B5193000000000000000000000000000000000000
          00000000000000000000322E325C8B818CFF8B818CFF8B818CFF8B818CFF897F
          8AFB1E1C1F380000000000000000000000000000000000000000000000000908
          0910746D75D78B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF3B363B6C0000000000000000000000000000
          0000000000000706070C6C656CC78B818CFF8B818CFF8B818CFF8B818CFF4C47
          4D8B000000000000000000000000000000000000000000000000000000000000
          00000D0C0D186A636AC38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF6A636AC30706070C0000000000000000000000000000
          00000706070C6A636AC38B818CFF8B818CFF8B818CFF7E757FE75A545CA70404
          0408000000000000000000000000000000000000000000000000000000000000
          0000000000000706070C6A636AC38B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF6A636AC30706070C000000000000000000000000000000000706
          070C6A636AC38B818CFF8B818CFF7E757FE71A181A3000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000706070C5F595FAF8B818CFF8B818CFF8B818CFF8B81
          8CFF6A636AC30706070C211E213C645D64B70706070C000000000706070C6A63
          6AC38B818CFF8B818CFF7E757FE7161416280000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000020202045D575EAB8B818CFF8B818CFF6A63
          6AC30706070C211E213C847B84F38B818CFF6A636AC30706070C25222544897F
          8AFB8B818CFF7E757FE716141628000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000002020204504B51936A636AC30706
          070C211E213C847B84F38B818CFF8B818CFF8B818CFF6A636AC30706070C2E2A
          2E547C737DE31614162800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000004040408211E
          213C847B84F38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF6A636AC30706
          070C040404080000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000211E213C847B
          84F38B818CFF3F3A3F740B0A0B14877D88F78B818CFF8B818CFF8B818CFF6A63
          6AC30706070C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000211E213C847B84F38B81
          8CFF3D383D7000000000302C30588B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF6A636AC30706070C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000211E213C847B84F38B818CFF3D38
          3D70000000002E2A2E54897F8AFB746D75D72B282C507F7780EB8B818CFF8B81
          8CFF6A636AC30706070C0706070C3B363B6C595259A3726B74D3817982EF665F
          67BB4C474D8B1E1C1F3800000000000000000000000000000000000000000000
          0000000000000000000000000000211E213C847B84F38B818CFF3D383D700000
          00002E2A2E54897F8AFB746D75D70D0C0D180706070C7F7780EB8B818CFF6A63
          6AC30706070C1E1C1F38776F77DB8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF4C474D8B040404080000000000000000000000000000
          00000000000000000000211E213C847B84F38B818CFF3D383D70000000002E2A
          2E54897F8AFB746D75D70D0C0D180706070C6A636AC38B818CFF6A636AC30706
          070C322E325C897F8AFB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF716972CF0404040800000000000000000000
          000000000000211E213C847B84F38B818CFF3D383D70000000002E2A2E54897F
          8AFB746D75D70D0C0D180706070C6A636AC38B818CFF6A636AC30706070C1110
          1220847B84F38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF504B519300000000000000000000
          0000211E213C847B84F38B818CFF3D383D70000000002E2A2E54897F8AFB746D
          75D70D0C0D180706070C6A636AC38B818CFF6A636AC30706070C000000006761
          69BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF23202340000000002320
          2340847B84F38B818CFF3D383D70000000002E2A2E54897F8AFB746D75D70D0C
          0D180706070C6A636AC38B818CFF6A636AC30706070C000000000B0A0B148B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF504B51930F0E0F1C8179
          82EF8B818CFF3D383D70000000002E2A2E54897F8AFB746D75D70D0C0D180706
          070C6A636AC38B818CFF6A636AC30706070C000000000000000029262A4C8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF625B62B31816
          182C141214245F595FAF8B818CFF8B818CFF8B818CFF6F676FCB5750589F8B81
          8CFF46414680000000002E2A2E54897F8AFB746D75D70D0C0D180706070C6A63
          6AC38B818CFF6A636AC30706070C000000000000000000000000433E447C8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5D575EAB020202040000
          000000000000020202045D575EAB8B818CFF8B818CFF877D88F7776F77DB8B81
          8CFF464146803B363B6C897F8AFB746D75D70D0C0D180706070C6A636AC38B81
          8CFF6A636AC30706070C000000000000000000000000000000003B363B6C8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF7C737DE302020204000000000000
          00000000000000000000020202045D575EAB8B818CFF7F7780EB726B74D38B81
          8CFF8B818CFF8B818CFF7A717ADF0D0C0D180706070C6A636AC38B818CFF6A63
          6AC30706070C00000000000000000000000000000000000000001C1A1C348B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF534D539700000000000000000000
          0000000000000000000000000000020202045D575EAB625B62B3534D53978B81
          8CFF8B818CFF8B818CFF5750589F090809106A636AC38B818CFF6A636AC30706
          070C00000000000000000000000000000000000000000000000004040408897F
          8AFB8B818CFF8B818CFF8B818CFF8B818CFF7E757FE702020204000000000000
          00000000000000000000000000000000000002020204232023400B0A0B147F77
          80EB8B818CFF8B818CFF8B818CFF877D88F78B818CFF6A636AC30706070C0000
          0000000000000000000000000000000000000000000000000000000000004641
          46808B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5D575EAB020202040000
          0000000000000000000000000000000000000000000000000000000000001614
          16286C656CC78B818CFF8B818CFF897F8AFB4C474D8B04040408000000000000
          0000000000000000000000000000000000000000000000000000000000000202
          02046F676FCB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5D575EAB0202
          0204000000000000000000000000000000000000000000000000000000000000
          0000000000000B0A0B141C1A1C34040404080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000014121424716972CF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5D57
          5EAB020202040000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000020202044A454A87897F8AFB8B818CFF8B818CFF8B818CFF8B81
          8CFF5D575EAB0202020400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000009080910252225443F3A3F744E494F8F322E
          325C1816182C0000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          23402320234023202340232023402320234023202340232023408B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF6A636AC33430356034303560343035603430356034303560343035603430
          3560343035603430356034303560343035603430356034303560343035603430
          3560343035603430356034303560343035603430356034303560343035603430
          356034303560343035607F7780EB8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000001816182C544E549B5750589F4E494F8F0D0C
          0D18000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000484348838B818CFF2E2A2E540000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000464146808B818CFF232023400000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000464146808B818CFF232023400000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000464146808B818CFF232023400000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000484348838B818CFF232023400000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000211E213C7C737DE38B818CFF232023400000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000009080910040404080000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001A181A3023202340000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000007C737DE38B818CFF0B0A0B140000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000003F3A3F744E494F8F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5750589F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007A717ADF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF645D64B72320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          234023202340232023407E757FE78B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF847B84F38B818CFF8B818CFF8B818CFF8B818CFF847B
          84F38B818CFF8B818CFF8B818CFF8B818CFF897F8AFB897F8AFB8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF897F8AFB25222544040404084C474D8B8B818CFF8B818CFF4C474D8B0404
          0408252225448B818CFF8B818CFF6C656CC70D0C0D180D0C0D186F676FCB8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF716972CF00000000000000000F0E0F1C8B818CFF8B818CFF0F0E0F1C0000
          000000000000716972CF8B818CFF3D383D7000000000000000003D383D708B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF897F8AFB1C1A1C3402020204464146808B818CFF8B818CFF464146800202
          02041E1C1F38897F8AFB8B818CFF676169BF09080910090809106A636AC38B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF7F7780EB8B818CFF8B818CFF8B818CFF8B818CFF7F77
          80EB8B818CFF8B818CFF8B818CFF8B818CFF877D88F7877D88F78B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF111012201110
          1220111012201110122011101220111012201110122011101220111012201110
          1220111012201110122011101220111012201110122011101220111012201110
          1220111012201110122011101220111012201110122011101220111012201110
          1220111012201110122011101220111012201110122011101220000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000464146806761
          69BF676169BF676169BF676169BF676169BF676169BF676169BF676169BF2724
          274800000000322E325C676169BF676169BF676169BF676169BF676169BF6761
          69BF676169BF676169BF3B363B6C000000001C1A1C34665F67BB676169BF6761
          69BF676169BF676169BF676169BF676169BF676169BF534D53978B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF897F8AFB3F3A3F74141214241A181A30645D64B78B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF544E549B1816182C161416284A45
          4A878B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF6C65
          6CC71C1A1C34111012203B363B6C847B84F38B818CFF8B818CFF8B818CFF8B81
          8CFF4641468000000000000000000000000002020204716972CF8B818CFF5750
          589F00000000676169BF8B818CFF645D64B70000000000000000000000000000
          0000504B51938B818CFF676169BF00000000464146808B818CFF7C737DE30706
          070C000000000000000000000000343035608B818CFF8B818CFF8B818CFF8B81
          8CFF1A181A3000000000000000000000000000000000484348838B818CFF5750
          589F00000000676169BF8B818CFF363237640000000000000000000000000000
          0000252225448B818CFF676169BF00000000464146808B818CFF5750589F0000
          0000000000000000000000000000090809108B818CFF8B818CFF8B818CFF8B81
          8CFF23202340000000000000000000000000000000004E494F8F8B818CFF5750
          589F00000000676169BF8B818CFF413C42780000000000000000000000000000
          00002B282C508B818CFF676169BF00000000464146808B818CFF5F595FAF0000
          0000000000000000000000000000111012208B818CFF8B818CFF8B818CFF8B81
          8CFF676169BF02020204000000000000000016141628847B84F38B818CFF5750
          589F00000000676169BF8B818CFF7C737DE30D0C0D1800000000000000000706
          070C716972CF8B818CFF676169BF00000000464146808B818CFF897F8AFB211E
          213C000000000000000002020204595259A38B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF716972CF484348834E494F8F847B84F38B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF7C737DE34C474D8B4A454A87776F
          77DB8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF877D
          88F7504B5193464146806C656CC78B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF897F8AFB8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750
          589F00000000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF676169BF00000000464146808B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF363237645750
          589F5750589F5750589F5750589F5750589F5750589F5750589F5750589F1C1A
          1C3400000000252225445750589F5750589F5750589F5750589F5750589F5750
          589F5750589F5750589F2E2A2E540000000014121424544E549B5750589F5750
          589F5750589F5750589F5750589F5750589F5750589F413C4278000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000009080910272427482926
          2A4C111012200000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000040404084A454A87726B74D38B818CFF8B818CFF8B81
          8CFF8B818CFF847B84F3665F67BB161416280000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001A181A30746D75D78B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF897F8AFB4641468004040408000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000322E325C897F8AFB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF676169BF020202040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001110
          1220847B84F38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF4A454A870000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000625B
          62B38B818CFF8B818CFF8B818CFF8B818CFF7C737DE3322E325C0F0E0F1C0B0A
          0B1423202340625B62B38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF2320
          2340000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000252225448B81
          8CFF8B818CFF8B818CFF8B818CFF6C656CC70908091000000000000000000000
          000000000000000000003B363B6C8B818CFF8B818CFF8B818CFF8B818CFF5952
          59A3000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000004A454A878B81
          8CFF8B818CFF8B818CFF897F8AFB161416280000000000000000000000000000
          0000000000000000000000000000625B62B38B818CFF8B818CFF8B818CFF7F77
          80EB020202040000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000005F595FAF8B81
          8CFF8B818CFF8B818CFF5F595FAF000000000000000000000000000000000000
          0000000000000000000000000000272427488B818CFF8B818CFF8B818CFF8B81
          8CFF1C1A1C340000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000746D75D78B81
          8CFF8B818CFF8B818CFF48434883000000000000000000000000000000000000
          00000000000000000000000000000706070C8B818CFF8B818CFF8B818CFF8B81
          8CFF232023400000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000726B74D38B81
          8CFF8B818CFF8B818CFF4E494F8F000000000000000000000000000000000000
          0000000000000000000000000000161416288B818CFF8B818CFF8B818CFF8B81
          8CFF232023400000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000544E549B8B81
          8CFF8B818CFF8B818CFF6C656CC7000000000000000000000000000000000000
          0000000000000000000000000000363237648B818CFF8B818CFF8B818CFF8B81
          8CFF1816182C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000363237648B81
          8CFF8B818CFF8B818CFF8B818CFF383439680000000000000000000000000000
          00000000000000000000090809107E757FE78B818CFF8B818CFF8B818CFF746D
          75D7000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000014121424877D
          88F78B818CFF8B818CFF8B818CFF847B84F33632376400000000000000000000
          0000000000001A181A30716972CF8B818CFF8B818CFF8B818CFF8B818CFF413C
          4278000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003D38
          3D708B818CFF8B818CFF8B818CFF8B818CFF8B818CFF726B74D34E494F8F4A45
          4A87625B62B3897F8AFB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF1A18
          1A30000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000645D64B78B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF746D
          75D7111012200000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000F0E0F1C716972CF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF7E757FE71614162800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000020202044A454A87897F8AFB8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF746D75D78B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF7E757FE716141628000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000141214243D383D70645D64B77A717ADF7169
          72CF676169BF4E494F8F1A181A30000000003D383D70897F8AFB8B818CFF8B81
          8CFF8B818CFF8B818CFF7F7780EB211E213C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000002E2A2E54897F8AFB8B81
          8CFF8B818CFF8B818CFF8B818CFF847B84F3211E213C00000000000000000000
          000016141628504B51930706070C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002E2A2E54897F
          8AFB8B818CFF8B818CFF8B818CFF8B818CFF847B84F327242748000000001614
          16287E757FE78B818CFF504B519300000000383439680706070C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000002320
          2340847B84F38B818CFF8B818CFF8B818CFF8B818CFF897F8AFB38343968746D
          75D78B818CFF817982EF161416284E494F8F8B818CFF645D64B7000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000211E213C847B84F38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF847B84F3211E213C4C474D8B8B818CFF8B818CFF4C474D8B000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001C1A1C347E757FE78B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF4E494F8F3D383D708B818CFF8B818CFF504B519300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000161416287E757FE78B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF5D575EAB0202020400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000161416287A717ADF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF716972CF020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000D0C0D18746D75D78B818CFF8B81
          8CFF8B818CFF8B818CFF645D64B7000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000D0C0D18746D75D78B81
          8CFF8B818CFF6A636AC30706070C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000706070C6A63
          6AC3746D75D70908091000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000706
          070C090809100000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          20000000000000100000000000000000000000000000000000001C1A1C342320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340211E213C847B
          84F38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF00000000211E
          213C8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000002320
          23408B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000005952
          59A38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF00000000544E
          549B8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000001110
          12207C737DE38B818CFF8B818CFF645D64B76A636AC38B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          00000B0A0B142B282C50211E213C020202040706070C6A636AC38B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          000000000000000000000000000000000000000000000706070C676169BF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000040404081A181A3014121424000000000000000000000000020202045D57
          5EAB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000B0A
          0B14716972CF8B818CFF8B818CFF595259A30000000000000000000000003F3A
          3F748B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000004E49
          4F8F8B818CFF8B818CFF8B818CFF8B818CFF2B282C50000000000B0A0B148B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF00000000645D
          64B78B818CFF8B818CFF8B818CFF8B818CFF413C427800000000232023408B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF00000000433E
          447C8B818CFF8B818CFF8B818CFF8B818CFF1E1C1F38000000000B0A0B14847B
          84F38B818CFF8B818CFF8B818CFF776F77DB8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000202
          0204534D5397817982EF7C737DE3363237640000000000000000000000002724
          2748726B74D3817982EF595259A30706070C3D383D708B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000038343968897F8AFB8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002E2A2E54897F
          8AFB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          00001816182C433E447C3D383D70090809100000000000000000000000000202
          020436323764464146801E1C1F38000000000000000000000000000000004843
          48838B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000001E1C
          1F38897F8AFB8B818CFF8B818CFF7A717ADF0908091000000000000000006761
          69BF8B818CFF8B818CFF8B818CFF322E325C0000000000000000302C3058897F
          8AFB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000005A54
          5CA78B818CFF8B818CFF8B818CFF8B818CFF36323764000000001C1A1C348B81
          8CFF8B818CFF8B818CFF8B818CFF726B74D300000000000000006A636AC38B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000005D57
          5EAB8B818CFF8B818CFF8B818CFF8B818CFF3B363B6C00000000211E213C8B81
          8CFF8B818CFF8B818CFF8B818CFF746D75D70000000000000000776F77DB8B81
          8CFF8B818CFF8B818CFF8B818CFF877D88F78B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000002926
          2A4C8B818CFF8B818CFF8B818CFF817982EF0D0C0D180000000002020204726B
          74D38B818CFF8B818CFF8B818CFF3F3A3F740000000000000000413C42788B81
          8CFF8B818CFF8B818CFF5F595FAF161416287E757FE78B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          000027242748544E549B4E494F8F111012200000000000000000000000000908
          0910484348835750589F302C305800000000000000000000000000000000302C
          30585750589F363237640706070C0000000011101220746D75D78B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000D0C0D18746D75D78B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000D0C0D18746D
          75D78B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000202
          02044A454A87776F77DB716972CF2E2A2E540000000000000000000000001E1C
          1F386A636AC37A717ADF534D53970706070C000000000000000009080910534D
          53977A717ADF6C656CC71E1C1F38000000000000000000000000000000004C47
          4D8B8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000003D38
          3D708B818CFF8B818CFF8B818CFF897F8AFB1A181A30000000000706070C7F77
          80EB8B818CFF8B818CFF8B818CFF534D53970000000000000000534D53978B81
          8CFF8B818CFF8B818CFF7F7780EB0706070C0000000000000000383439688B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF00000000625B
          62B38B818CFF8B818CFF8B818CFF8B818CFF3F3A3F7400000000252225448B81
          8CFF8B818CFF8B818CFF8B818CFF7A717ADF00000000000000007C737DE38B81
          8CFF8B818CFF8B818CFF8B818CFF232023400000000000000000665F67BB8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF00000000504B
          51938B818CFF8B818CFF8B818CFF8B818CFF2B282C5000000000141214248B81
          8CFF8B818CFF8B818CFF8B818CFF6A636AC300000000000000006A636AC38B81
          8CFF8B818CFF8B818CFF8B818CFF141214240000000000000000595259A38B81
          8CFF8B818CFF8B818CFF8B818CFF726B74D38B818CFF8B818CFF000000000F0E
          0F1C7A717ADF8B818CFF8B818CFF5F595FAF0202020400000000000000004843
          48838B818CFF8B818CFF7F7780EB1C1A1C3400000000000000001E1C1F387F77
          80EB8B818CFF8B818CFF484348830000000000000000000000000F0E0F1C7A71
          7ADF8B818CFF8B818CFF534D5397020202045D575EAB8B818CFF000000000000
          000004040408211E213C1A181A30000000000000000000000000000000000000
          000014121424232023400706070C000000000000000000000000000000000706
          070C232023401614162800000000000000000000000000000000000000000404
          0408211E213C0F0E0F1C000000000000000002020204504B5193000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001E1C1F382320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          23402320234004040408000000000000000000000000000000005F595FAF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF897F8AFB0706070C000000000000000000000000000000002B282C508B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF5F595FAF000000005D575EAB8B818CFF8B818CFF1C1A1C343B363B6C8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF716972CF0000000000000000000000000000000000000000000000006F67
          6FCB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF3D383D700000000029262A4C8B818CFF676169BF00000000161416288B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF433E447C0000000000000000000000000000000000000000000000002320
          23408B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF1E1C1F3800000000020202047E757FE72B282C5000000000000000007A71
          7ADF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8179
          82EF090809100000000000000000000000000000000000000000000000000000
          0000413C42788B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF847B
          84F30202020400000000000000003B363B6C000000000000000000000000544E
          549B8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF3632
          3764000000000000000000000000000000000000000000000000000000000000
          0000000000004E494F8F8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF6761
          69BF000000000000000000000000595259A30D0C0D180000000000000000302C
          30588B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF484348830000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000005A545CA78B818CFF8B818CFF8B818CFF8B818CFF4843
          48830000000000000000232023408B818CFF504B519300000000000000000B0A
          0B148B818CFF8B818CFF8B818CFF8B818CFF897F8AFB413C4278000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002020204363237647E757FE78B818CFF8B818CFF2724
          274800000000000000002320234034303560322E325C00000000000000000000
          00006F676FCB8B818CFF8B818CFF726B74D32320234000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000706070C272427484C474D8B0706
          070C000000000000000000000000000000000000000000000000000000000000
          000048434883665F67BB302C3058020202040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000202
          02043D383D705F595FAF7C737DE38B818CFF716972CF4E494F8F232023400000
          0000090809102320234023202340232023402320234023202340232023402320
          2340232023402320234023202340232023402320234011101220000000000000
          0000000000000000000000000000000000000000000000000000161416287169
          72CF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF3F3A3F740000
          00005D575EAB8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF847B84F3000000000000
          0000000000000000000000000000000000000000000016141628847B84F38B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF595259A35A54
          5CA78B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          00000000000000000000000000000000000002020204716972CF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF232023403430
          35608B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000003D383D708B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF232023403430
          35608B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000005F595FAF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF232023403430
          35608B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000007C737DE38B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF6F676FCB020202040D0C
          0D18817982EF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000020202048B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF645D64B7000000000000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          000000000000000000000000000000000000776F77DB8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF534D5397534D
          53978B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          0000000000000000000000000000000000005D575EAB8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF343035600000
          0000676169BF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF000000000000
          000000000000000000000000000000000000343035608B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF4E494F8F0000
          00004C474D8B8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF726B74D3000000000000
          00000000000000000000000000000000000000000000665F67BB8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7F7780EB0202
          0204000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000F0E0F1C7E757FE78B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF746D
          75D71C1A1C340B0A0B14343035600706070C0000000000000000000000000000
          00000000000000000000000000001C1A1C342724274800000000000000000000
          00000000000000000000000000000000000000000000000000000B0A0B14625B
          62B38B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF897F8AFB4C47
          4D8B04040408090809108B818CFF211E213C0000000000000000000000000000
          00000000000000000000000000005F595FAF544E549B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002B282C504E494F8F6A636AC37A717ADF625B62B3484348831C1A1C340000
          00000000000000000000746D75D7534D53970000000000000000000000000000
          000000000000000000000F0E0F1C817982EF3834396800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000029262A4C8B818CFF2E2A2E5400000000000000000000
          0000000000000706070C645D64B7746D75D70404040800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000484348838B818CFF5750589F2E2A2E542320
          23403B363B6C776F77DB7C737DE31C1A1C340000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000232023406A636AC3847B84F38B81
          8CFF7C737DE3534D539709080910000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000B0A
          0B14000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          20000000000000100000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000009080910111012201110
          1220111012201110122011101220111012201110122011101220111012201110
          122011101220111012201110122011101220111012200F0E0F1C7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          00000000000000000000000000000000000000000000464146808B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7A717ADF7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          00000000000000000000000000000000000000000000464146808B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7A717ADF7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000023202340464146804641
          4680464146804641468046414680464146804641468046414680464146804641
          468046414680464146804641468046414680464146803D383D707A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002E2A2E543430
          3560343035603430356034303560343035603430356034303560211E213C0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001E1C1F382320
          2340232023402320234023202340232023402320234023202340161416280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          000000000000000000000000000000000000000000001A181A30343035603430
          3560343035603430356034303560343035603430356034303560343035603430
          356034303560343035603430356034303560343035602E2A2E547A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          00000000000000000000000000000000000000000000464146808B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7A717ADF7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          00000000000000000000000000000000000000000000464146808B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7A717ADF7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000011101220232023402320
          2340232023402320234023202340232023402320234023202340232023402320
          234023202340232023402320234023202340232023401E1C1F387A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001E1C1F382320
          2340232023402320234023202340232023402320234023202340161416280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003D383D704641
          46804641468046414680464146804641468046414680464146802B282C500000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          000000000000000000000000000000000000000000002B282C505750589F5750
          589F5750589F5750589F5750589F5750589F5750589F5750589F5750589F5750
          589F5750589F5750589F5750589F5750589F5750589F4C474D8B7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          00000000000000000000000000000000000000000000464146808B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7A717ADF7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          00000000000000000000000000000000000000000000464146808B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF7A717ADF7A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000007A717ADF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF5750589F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000100D1138130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F13400202020800000000000000000000000000000000332835AF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4A394CFB0403040C00000000000000000000000000000000171218504B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF332835AF00000000322834AB4B3B4DFF4B3B4DFF0F0C10342019206C4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF3D303FCF0000000000000000000000000000000000000000000000003C2F
          3DCB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF211A2270000000001612174C4B3B4DFF372B3ABF000000000C090C284B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF241D257C000000000000000000000000000000000000000000000000130F
          13404B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF100D11380000000001010104443546E71712185000000000000000004234
          42DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4536
          47EF050405100000000000000000000000000000000000000000000000000000
          0000231C24784B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4737
          48F30101010400000000000000002019206C0000000000000000000000002E24
          2F9B4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1D17
          1E64000000000000000000000000000000000000000000000000000000000000
          00000000000029212B8F4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF372B
          3ABF000000000000000000000000302631A30706071800000000000000001A14
          1A584B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF271E28830000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000302732A74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF271E
          28830000000000000000130F13404B3B4DFF2B212D9300000000000000000605
          06144B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB231C2478000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000010101041D171E64443546E74B3B4DFF4B3B4DFF1511
          16480000000000000000130F13401C161D601B151C5C00000000000000000000
          00003C2F3DCB4B3B4DFF4B3B4DFF3D3140D3130F134000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000403040C1511164829202A8B0403
          040C000000000000000000000000000000000000000000000000000000000000
          0000271E2883362B38BB1A141A58010101040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          0104211A2270332835AF433545E34B3B4DFF3D303FCF29212B8F130F13400000
          000005040510130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F134009070A20000000000000
          00000000000000000000000000000000000000000000000000000C090C283D30
          3FCF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF221B23740000
          0000322834AB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF473748F3000000000000
          000000000000000000000000000000000000000000000C090C28473748F34B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF302631A33027
          32A74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          000000000000000000000000000000000000010101043D303FCF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13401C16
          1D604B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          000000000000000000000000000000000000211A22704B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13401C16
          1D604B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          000000000000000000000000000000000000332835AF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13401C16
          1D604B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          000000000000000000000000000000000000433545E34B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3C2F3DCB010101040706
          0718453647EF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000000000000000000000000000010101044B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF362A37B7000000000000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          000000000000000000000000000000000000403341DB4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2C232E972C23
          2E974B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          000000000000000000000000000000000000322834AB4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1C161D600000
          0000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF29212B8F0000
          000029202A8B4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D3140D3000000000000
          00000000000000000000000000000000000000000000362B38BB4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF443547EB0101
          0104000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000806081C443546E74B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3E32
          41D70F0C1034060506141C161D600403040C0000000000000000000000000000
          00000000000000000000000000000F0C10341511164800000000000000000000
          0000000000000000000000000000000000000000000000000000060506143529
          35B34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB2920
          2A8B02020208050405104B3B4DFF120E123C0000000000000000000000000000
          0000000000000000000000000000332835AF2E242F9B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001712185029212B8F392C3BC3423442DF352935B3271E28830F0C10340000
          000000000000000000003E3241D72C232E970000000000000000000000000000
          000000000000000000000806081C453647EF1E181F6800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000001612174C4B3B4DFF1913195400000000000000000000
          0000000000000403040C362A37B73E3241D70202020800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000271E28834B3B4DFF2F252F9F19131954130F
          13402019206C403341DB433545E30F0C10340000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000130F1340392C3BC3473748F34B3B
          4DFF433545E32C232E9705040510000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000605
          0614000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFA8E9C9FF6AD9A4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFF7FDFAFF7EDEB0FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFCFEFDFF95E4BDFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFADEACCFF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC8F1DDFF57D4
          98FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDBF6
          E9FF5FD69DFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFECFAF3FF6DDAA6FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FDFAFF81DFB1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFEFFBF5FF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFDEF7EBFF5FD69DFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBF2
          DFFF57D498FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB2EBD0FF52D3
          95FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFFFFFFFFFFCFEFDFF97E4BFFF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFFFFFFFFFF7FDFAFF81DFB1FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FFBDEED6FFECFAF3FF6DDAA6FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF9AE5C1FF5FD69DFF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          000000000000000000004FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          000000000000000000008B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFDCB28BFFE9CEB5FFEEDAC7FFF0DF
          CFFFEAD1B9FFE2BF9FFFDAAE85FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFE8CDB3FFFEFCFBFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFBF6F1FFE4C3A5FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFDEB793FFFEFEFDFFFEFCFBFFF6ECE3FFF2E2
          D3FFF9F2EBFFFFFFFFFFFFFFFFFFFFFFFFFFF0DECDFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFDFB995FFDFBA97FFD8AA7EFFD8AA7EFFE2C1A1FFDBB189FFD8AA7EFFD8AA
          7EFFD8AA7EFFDDB58FFFF5EADFFFFFFFFFFFFFFFFFFFE9CEB5FFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFF6EBE1FFF8EFE7FFD9AB81FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFF1E0D1FFFFFFFFFFFDFBF9FFDDB58FFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFE7CA
          AFFFFFFFFFFFFFFFFFFFEAD1B9FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFDAAF87FFFBF6F1FFFFFFFFFFEDD8C5FFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFDAAF87FFFCF8
          F5FFFFFFFFFFFFFFFFFFFEFCFBFFDEB793FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFEEDAC7FFFFFFFFFFF6EBE1FFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFEFDCCBFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF6ECE3FFD9AB81FFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFE5C6A9FFFFFFFFFFFDFBF9FFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFE1BD9BFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9CEB5FFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFE6C9ADFFFFFFFFFFFDFAF7FFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFE9CFB7FFF0DFCFFFF0DF
          CFFFFEFCFBFFFFFFFFFFF8F0E9FFF0DFCFFFEEDBC9FFD9AB81FFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFEEDBC9FFFFFFFFFFF5E8DDFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFF1E0D1FFFFFFFFFFF9F3EDFFD9AB81FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFDCB38DFFFDFAF7FFFFFFFFFFECD6C1FFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFE0BB99FFFFFFFFFFFFFFFFFFEEDAC7FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD9AD83FFF4E7DBFFFFFFFFFFFCF8F5FFDBB189FFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFEEDBC9FFFFFFFFFFFFFFFFFFF3E6D9FFDEB793FFD8AA7EFFD8AA
          7EFFD9AB81FFE2BF9FFFF9F2EBFFFFFFFFFFFFFFFFFFE5C6A9FFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD9AB81FFF2E3D5FFFFFFFFFFFFFFFFFFFFFFFFFFFBF6F1FFF6EC
          E3FFFDFBF9FFFFFFFFFFFFFFFFFFFEFEFDFFEBD3BDFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFE7CAAFFFFBF6F1FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF8EFE7FFDFBA97FFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD9AD83FFE1BD9BFFE8CDB3FFECD5
          BFFFE5C6A9FFDDB58FFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          00000000000000000000D8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA7EFFD8AA
          7EFFD8AA7EFFD8AA7EFF00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000010101042019206C322834AB302631A31D171E6401010104000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001010104120E123C00000000000000000000000000000000000000000605
          06143D303FCF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3B2D3BC7070607180000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000C09
          0C283B2D3BC74B3B4DFF211A2270000000000000000000000000000000003228
          34AB4B3B4DFF4B3B4DFF443546E7453647EF4B3B4DFF4B3B4DFF3E3241D70706
          0718000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000001010104231C24784938
          4BF74B3B4DFF4B3B4DFF3E3241D7000000000000000000000000050405104B3B
          4DFF4B3B4DFF332835AF020202080403040C3C2F3DCB4B3B4DFF4B3B4DFF3C2F
          3DCB0403040C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E0B0E303D303FCF4B3B4DFF4B3B
          4DFF4B3B4DFF4A394CFB100D11380000000000000000000000000F0C10344B3B
          4DFF4B3B4DFF1A141A580000000000000000231C24784B3B4DFF4B3B4DFF4B3B
          4DFF372B3ABF0101010400000000000000000000000000000000000000000000
          0000000000000000000000000000261E27804A394CFB4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF29202A8B000000000000000000000000000000000403040C4B3B
          4DFF4B3B4DFF372B3ABF0706071809070A203E3241D74B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF322834AB01010104000000000000000000000000000000000000
          000000000000000000000B080B244B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF3E3241D70202020800000000000000000000000000000000000000002F25
          2F9F4B3B4DFF4B3B4DFF49384BF74A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF2B212D93000000000000000000000000000000000000
          000000000000000000001B151C5C4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A39
          4CFB100D11380000000000000000000000000000000000000000000000000504
          05103E3241D74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF2019206C0000000000000000000000000000
          0000000000000403040C3B2D3BC74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2920
          2A8B000000000000000000000000000000000000000000000000000000000000
          000007060718392C3BC34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF392C3BC30403040C0000000000000000000000000000
          00000403040C392C3BC34B3B4DFF4B3B4DFF4B3B4DFF443546E7302732A70202
          0208000000000000000000000000000000000000000000000000000000000000
          0000000000000403040C392C3BC34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF392C3BC30403040C000000000000000000000000000000000403
          040C392C3BC34B3B4DFF4B3B4DFF443546E70E0B0E3000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000403040C332835AF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF392C3BC30403040C120E123C362A37B70403040C000000000403040C392C
          3BC34B3B4DFF4B3B4DFF443546E70C090C280000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000001010104322834AB4B3B4DFF4B3B4DFF392C
          3BC30403040C120E123C473748F34B3B4DFF392C3BC30403040C141014444A39
          4CFB4B3B4DFF443546E70C090C28000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000010101042B212D93392C3BC30403
          040C120E123C473748F34B3B4DFF4B3B4DFF4B3B4DFF392C3BC30403040C1913
          1954433545E30C090C2800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002020208120E
          123C473748F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF392C3BC30403
          040C020202080000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000120E123C4737
          48F34B3B4DFF221B23740605061449384BF74B3B4DFF4B3B4DFF4B3B4DFF392C
          3BC30403040C0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000120E123C473748F34B3B
          4DFF211A2270000000001A141A584B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF392C3BC30403040C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000120E123C473748F34B3B4DFF211A
          227000000000191319544A394CFB3E3241D717121850443547EB4B3B4DFF4B3B
          4DFF392C3BC30403040C0403040C2019206C302631A33D3140D3453647EF362B
          38BB29202A8B100D113800000000000000000000000000000000000000000000
          0000000000000000000000000000120E123C473748F34B3B4DFF211A22700000
          0000191319544A394CFB3E3241D7070607180403040C443547EB4B3B4DFF392C
          3BC30403040C100D1138403341DB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF29202A8B020202080000000000000000000000000000
          00000000000000000000120E123C473748F34B3B4DFF211A2270000000001913
          19544A394CFB3E3241D7070607180403040C392C3BC34B3B4DFF392C3BC30403
          040C1B151C5C4A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D303FCF0202020800000000000000000000
          000000000000120E123C473748F34B3B4DFF211A227000000000191319544A39
          4CFB3E3241D7070607180403040C392C3BC34B3B4DFF392C3BC30403040C0907
          0A20473748F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2B212D9300000000000000000000
          0000120E123C473748F34B3B4DFF211A227000000000191319544A394CFB3E32
          41D7070607180403040C392C3BC34B3B4DFF392C3BC30403040C00000000372B
          3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F134000000000130F
          1340473748F34B3B4DFF211A227000000000191319544A394CFB3E3241D70706
          07180403040C392C3BC34B3B4DFF392C3BC30403040C00000000060506144B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2B212D930806081C4536
          47EF4B3B4DFF211A227000000000191319544A394CFB3E3241D7070607180403
          040C392C3BC34B3B4DFF392C3BC30403040C00000000000000001612174C4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF352935B30D0A
          0D2C0B080B24332835AF4B3B4DFF4B3B4DFF4B3B4DFF3C2F3DCB2F252F9F4B3B
          4DFF261E278000000000191319544A394CFB3E3241D7070607180403040C392C
          3BC34B3B4DFF392C3BC30403040C000000000000000000000000241D257C4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF322834AB010101040000
          00000000000001010104322834AB4B3B4DFF4B3B4DFF49384BF7403341DB4B3B
          4DFF261E27802019206C4A394CFB3E3241D7070607180403040C392C3BC34B3B
          4DFF392C3BC30403040C000000000000000000000000000000002019206C4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF433545E301010104000000000000
          0000000000000000000001010104322834AB4B3B4DFF443547EB3D3140D34B3B
          4DFF4B3B4DFF4B3B4DFF423442DF070607180403040C392C3BC34B3B4DFF392C
          3BC30403040C00000000000000000000000000000000000000000F0C10344B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2C232E9700000000000000000000
          000000000000000000000000000001010104322834AB352935B32C232E974B3B
          4DFF4B3B4DFF4B3B4DFF2F252F9F05040510392C3BC34B3B4DFF392C3BC30403
          040C000000000000000000000000000000000000000000000000020202084A39
          4CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF443546E701010104000000000000
          00000000000000000000000000000000000001010104130F1340060506144435
          47EB4B3B4DFF4B3B4DFF4B3B4DFF49384BF74B3B4DFF392C3BC30403040C0000
          000000000000000000000000000000000000000000000000000000000000261E
          27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF322834AB010101040000
          0000000000000000000000000000000000000000000000000000000000000C09
          0C283B2D3BC74B3B4DFF4B3B4DFF4A394CFB29202A8B02020208000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          01043C2F3DCB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF322834AB0101
          0104000000000000000000000000000000000000000000000000000000000000
          000000000000060506140F0C1034020202080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000B080B243D303FCF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3228
          34AB010101040000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010104281F29874A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF322834AB0101010400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000504051014101444221B237429212B8F1B15
          1C5C0D0A0D2C0000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F13404B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF392C3BC31C161D601C161D601C161D601C161D601C161D601C161D601C16
          1D601C161D601C161D601C161D601C161D601C161D601C161D601C161D601C16
          1D601C161D601C161D601C161D601C161D601C161D601C161D601C161D601C16
          1D601C161D601C161D60443547EB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000D0A0D2C2E242F9B2F252F9F29212B8F0706
          0718000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000271E28834B3B4DFF191319540000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000261E27804B3B4DFF130F13400000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000261E27804B3B4DFF130F13400000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000261E27804B3B4DFF130F13400000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000271E28834B3B4DFF130F13400000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000120E123C433545E34B3B4DFF130F13400000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000005040510020202080000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000E0B0E30130F1340000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000433545E34B3B4DFF060506140000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000221B237429212B8F000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF2F252F9F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF362A37B7130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340443546E74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF473748F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4737
          48F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB4A394CFB4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4A394CFB141014440202020829202A8B4B3B4DFF4B3B4DFF29202A8B0202
          0208141014444B3B4DFF4B3B4DFF3B2D3BC707060718070607183C2F3DCB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF3D303FCF00000000000000000806081C4B3B4DFF4B3B4DFF0806081C0000
          0000000000003D303FCF4B3B4DFF211A22700000000000000000211A22704B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4A394CFB0F0C103401010104261E27804B3B4DFF4B3B4DFF261E27800101
          0104100D11384A394CFB4B3B4DFF372B3ABF0504051005040510392C3BC34B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF443547EB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4435
          47EB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF49384BF749384BF74B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF09070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A2009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A2009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A2009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A20000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000261E2780372B
          3ABF372B3ABF372B3ABF372B3ABF372B3ABF372B3ABF372B3ABF372B3ABF1511
          1648000000001B151C5C372B3ABF372B3ABF372B3ABF372B3ABF372B3ABF372B
          3ABF372B3ABF372B3ABF2019206C000000000F0C1034362B38BB372B3ABF372B
          3ABF372B3ABF372B3ABF372B3ABF372B3ABF372B3ABF2C232E974B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4A394CFB221B23740B080B240E0B0E30362A37B74B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF2E242F9B0D0A0D2C0C090C28281F
          29874B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF3B2D
          3BC70F0C103409070A202019206C473748F34B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF261E2780000000000000000000000000010101043D303FCF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF362A37B70000000000000000000000000000
          00002B212D934B3B4DFF372B3ABF00000000261E27804B3B4DFF433545E30403
          040C0000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF0E0B0E3000000000000000000000000000000000271E28834B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF1D171E640000000000000000000000000000
          0000141014444B3B4DFF372B3ABF00000000261E27804B3B4DFF2F252F9F0000
          0000000000000000000000000000050405104B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF130F13400000000000000000000000000000000029212B8F4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF231C24780000000000000000000000000000
          0000171218504B3B4DFF372B3ABF00000000261E27804B3B4DFF332835AF0000
          000000000000000000000000000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF372B3ABF0101010400000000000000000C090C28473748F34B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF433545E30706071800000000000000000403
          040C3D303FCF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4A394CFB120E
          123C000000000000000001010104302631A34B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF3D303FCF271E288329212B8F473748F34B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF433545E329202A8B281F29874033
          41DB4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4938
          4BF72B212D93261E27803B2D3BC74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F25
          2F9F00000000372B3ABF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF372B3ABF00000000261E27804B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1D171E642F25
          2F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F0F0C
          103400000000141014442F252F9F2F252F9F2F252F9F2F252F9F2F252F9F2F25
          2F9F2F252F9F2F252F9F19131954000000000B080B242E242F9B2F252F9F2F25
          2F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F231C2478000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A2009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A2009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A2009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A204B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF372B3ABF372B
          3ABF3B2D3BC74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4234
          42DF372B3ABF372B3ABF423442DF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF3B2D3BC7372B3ABF372B3ABF3E3241D74B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D303FCF372B3ABF372B3ABF000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000009070A204B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF261E
          27800000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          000007060718372B3ABF372B3ABF372B3ABF372B3ABF372B3ABF372B3ABF1C16
          1D600000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000261E27804B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF09070A2000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000000504
          05100000000000000000171218502F252F9F2F252F9F2F252F9F2F252F9F2F25
          2F9F2F252F9F0605061400000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000403040E01010104000000000000000000000000000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002020207000000000000000000000000000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001C161D604B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000403040C09070A2009070A200907
          0A2009070A2009070A2009070A20020202080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000B1220200B12
          20200B1220200B1220200B1220200B1220200B1220200B1220200B1220200B12
          20200B1220200B1220200B1220200B1220200B1220200B1220200B1220200B12
          20200B1220200B1220200B1220200B1220200B1220200B1220200B1220200B12
          20200B1220200B1220200B1220200B1220200B1220200B1220205B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF436DBEBF436D
          BEBF4672C6C75B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5080
          DEDF436DBEBF436DBEBF5080DEDF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF4672C6C7436DBEBF436DBEBF4D7AD6D75B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF4A77CECF436DBEBF436DBEBF000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          00000B1220205B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2E49
          808000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000090E1818436DBEBF436DBEBF436DBEBF436DBEBF436DBEBF436DBEBF2237
          606000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF0B1220200000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000005080E0E010204040000000000000000000000000102
          030300000000000000001C2E5050395A9E9F395A9E9F395A9E9F395A9E9F395A
          9E9F395A9E9F070B14140000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000005070D0D000000000000
          0000000000000102030301010303000000000000000000000000000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          000000000000000000000000000000000000000000000001010105070D0D0000
          0000000000000000000001010202000000000000000000010101000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          000000000000000000000000000000000000000000000000000005070D0D0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000101010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000001010100000000000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000010204040000000000000000000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000101010000000000000000000000000000
          000000000000000000000000000000000000223760605B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF172540400000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000004070C0C0B1220200B1220200B12
          20200B1220200B1220200B122020030508080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          20000000000000100000000000000000000000000000000000000F0C1034130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340120E123C4737
          48F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF00000000120E
          123C4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF00000000130F
          13404B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000003026
          31A34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000002E24
          2F9B4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000907
          0A20433545E34B3B4DFF4B3B4DFF362A37B7392C3BC34B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          00000605061417121850120E123C010101040403040C392C3BC34B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          000000000000000000000000000000000000000000000403040C372B3ABF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000020202080E0B0E300B080B24000000000000000000000000010101043228
          34AB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000605
          06143D303FCF4B3B4DFF4B3B4DFF302631A3000000000000000000000000221B
          23744B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000002921
          2B8F4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1712185000000000060506144B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF00000000362A
          37B74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF231C247800000000130F13404B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF00000000241D
          257C4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF100D113800000000060506144737
          48F34B3B4DFF4B3B4DFF4B3B4DFF403341DB4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000101
          01042C232E97453647EF433545E31D171E640000000000000000000000001511
          16483D3140D3453647EF302631A30403040C211A22704B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000001E181F684A394CFB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000191319544A39
          4CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          00000D0A0D2C241D257C211A2270050405100000000000000000000000000101
          01041D171E64261E2780100D113800000000000000000000000000000000271E
          28834B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF00000000100D
          11384A394CFB4B3B4DFF4B3B4DFF423442DF050405100000000000000000372B
          3ABF4B3B4DFF4B3B4DFF4B3B4DFF1B151C5C00000000000000001A141A584A39
          4CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000003027
          32A74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF1D171E64000000000F0C10344B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D3140D30000000000000000392C3BC34B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000003228
          34AB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2019206C00000000120E123C4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF3E3241D70000000000000000403341DB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF49384BF74B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000001612
          174C4B3B4DFF4B3B4DFF4B3B4DFF453647EF0706071800000000010101043D31
          40D34B3B4DFF4B3B4DFF4B3B4DFF221B23740000000000000000231C24784B3B
          4DFF4B3B4DFF4B3B4DFF332835AF0C090C28443546E74B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000151116482E242F9B29212B8F09070A200000000000000000000000000504
          0510271E28832F252F9F1A141A58000000000000000000000000000000001A14
          1A582F252F9F1D171E640403040C0000000009070A203E3241D74B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000070607183E3241D74B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000070607183E32
          41D74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000000101
          0104281F2987403341DB3D303FCF19131954000000000000000000000000100D
          1138392C3BC3423442DF2C232E970403040C0000000000000000050405102C23
          2E97423442DF3B2D3BC7100D1138000000000000000000000000000000002920
          2A8B4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF00000000211A
          22704B3B4DFF4B3B4DFF4B3B4DFF4A394CFB0E0B0E30000000000403040C4435
          47EB4B3B4DFF4B3B4DFF4B3B4DFF2C232E9700000000000000002C232E974B3B
          4DFF4B3B4DFF4B3B4DFF443547EB0403040C00000000000000001E181F684B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000003529
          35B34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF221B237400000000141014444B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF423442DF0000000000000000433545E34B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F13400000000000000000362B38BB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF000000002B21
          2D934B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF17121850000000000B080B244B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF392C3BC30000000000000000392C3BC34B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF0B080B240000000000000000302631A34B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D3140D34B3B4DFF4B3B4DFF000000000806
          081C423442DF4B3B4DFF4B3B4DFF332835AF010101040000000000000000271E
          28834B3B4DFF4B3B4DFF443547EB0F0C10340000000000000000100D11384435
          47EB4B3B4DFF4B3B4DFF271E28830000000000000000000000000806081C4234
          42DF4B3B4DFF4B3B4DFF2C232E9701010104322834AB4B3B4DFF000000000000
          000002020208120E123C0E0B0E30000000000000000000000000000000000000
          00000B080B24130F13400403040C000000000000000000000000000000000403
          040C130F13400C090C2800000000000000000000000000000000000000000202
          0208120E123C0806081C0000000000000000010101042B212D93000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          000000000000000000000000000000000000000000000504051009070A200907
          0A2009070A2009070A2009070A2009070A2009070A2009070A2009070A200907
          0A2009070A2009070A2009070A2009070A2009070A200806081C423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000261E27804B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF423442DF423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000261E27804B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF423442DF423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000130F1340261E2780261E
          2780261E2780261E2780261E2780261E2780261E2780261E2780261E2780261E
          2780261E2780261E2780261E2780261E2780261E2780211A2270423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000191319541C16
          1D601C161D601C161D601C161D601C161D601C161D601C161D60120E123C0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000100D1138130F
          1340130F1340130F1340130F1340130F1340130F1340130F13400C090C280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          000000000000000000000000000000000000000000000E0B0E301C161D601C16
          1D601C161D601C161D601C161D601C161D601C161D601C161D601C161D601C16
          1D601C161D601C161D601C161D601C161D601C161D6019131954423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000261E27804B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF423442DF423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000261E27804B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF423442DF423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000009070A20130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340130F1340130F1340130F
          1340130F1340130F1340130F1340130F1340130F1340100D1138423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000100D1138130F
          1340130F1340130F1340130F1340130F1340130F1340130F13400C090C280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000211A2270261E
          2780261E2780261E2780261E2780261E2780261E2780261E2780171218500000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000171218502F252F9F2F25
          2F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F2F25
          2F9F2F252F9F2F252F9F2F252F9F2F252F9F2F252F9F29202A8B423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000261E27804B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF423442DF423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          00000000000000000000000000000000000000000000261E27804B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF423442DF423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000423442DF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF2F252F9F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000005040510151116481612
          174C09070A200000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000002020208281F29873D3140D34B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF473748F3362B38BB0C090C280000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000E0B0E303E3241D74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB261E278002020208000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00001B151C5C4A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF372B3ABF010101040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000907
          0A20473748F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF281F29870000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000003529
          35B34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF433545E31B151C5C0806081C0605
          0614130F1340352935B34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF130F
          1340000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000141014444B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF3B2D3BC70504051000000000000000000000
          000000000000000000002019206C4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3026
          31A3000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000281F29874B3B
          4DFF4B3B4DFF4B3B4DFF4A394CFB0C090C280000000000000000000000000000
          0000000000000000000000000000352935B34B3B4DFF4B3B4DFF4B3B4DFF4435
          47EB010101040000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000332835AF4B3B
          4DFF4B3B4DFF4B3B4DFF332835AF000000000000000000000000000000000000
          0000000000000000000000000000151116484B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF0F0C10340000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003E3241D74B3B
          4DFF4B3B4DFF4B3B4DFF271E2883000000000000000000000000000000000000
          00000000000000000000000000000403040C4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF130F13400000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003D3140D34B3B
          4DFF4B3B4DFF4B3B4DFF29212B8F000000000000000000000000000000000000
          00000000000000000000000000000C090C284B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF130F13400000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002E242F9B4B3B
          4DFF4B3B4DFF4B3B4DFF3B2D3BC7000000000000000000000000000000000000
          00000000000000000000000000001D171E644B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF0D0A0D2C0000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001D171E644B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF1E181F680000000000000000000000000000
          0000000000000000000005040510443546E74B3B4DFF4B3B4DFF4B3B4DFF3E32
          41D7000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000B080B244938
          4BF74B3B4DFF4B3B4DFF4B3B4DFF473748F31D171E6400000000000000000000
          0000000000000E0B0E303D303FCF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF231C
          2478000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000211A
          22704B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3D3140D329212B8F281F
          2987352935B34A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF0E0B
          0E30000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000362A37B74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF3E32
          41D709070A200000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000806081C3D303FCF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF443546E70C090C2800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010104281F29874A394CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF3E3241D74B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF443546E70C090C28000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000B080B24211A2270362A37B7423442DF3D30
          3FCF372B3ABF29212B8F0E0B0E3000000000211A22704A394CFB4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF443547EB120E123C0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000191319544A394CFB4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF473748F3120E123C00000000000000000000
          00000C090C282B212D930403040C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000191319544A39
          4CFB4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF473748F315111648000000000C09
          0C28443546E74B3B4DFF2B212D93000000001E181F680403040C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000130F
          1340473748F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4A394CFB1E181F683E32
          41D74B3B4DFF453647EF0C090C2829212B8F4B3B4DFF362A37B7000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000120E123C473748F34B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF473748F3120E123C29202A8B4B3B4DFF4B3B4DFF29202A8B000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000F0C1034443546E74B3B4DFF4B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF29212B8F211A22704B3B4DFF4B3B4DFF2B212D9300000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000C090C28443546E74B3B4DFF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF4B3B4DFF322834AB0101010400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000C090C28423442DF4B3B4DFF4B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF3D303FCF010101040000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000070607183E3241D74B3B4DFF4B3B
          4DFF4B3B4DFF4B3B4DFF362A37B7000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000070607183E3241D74B3B
          4DFF4B3B4DFF392C3BC30403040C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000000403040C392C
          3BC33E3241D70504051000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000403
          040C050405100000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000001010304131736442A3376933945A2CB3F4DB6E34858CCFF4858CCFF4050
          B9E73A47A6CF2C357B9B171C4050020306080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000002030608232B
          637C4150BBEB4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4554C1F32730708B05060D10000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000191E46584050B9E74858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4352BEEF202759700000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000252D69834858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF2D37
          809F010103040000000000000000000000000000000000000000000000000000
          00000000000000000000000000002D3882A34858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4654C6F73A47A6CF313C8BAF303A89AB3945
          A2CB4554C1F34858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF364299BF0101030400000000000000000000000000000000000000000000
          00000000000000000000232B637C4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF3A47A6CF151A3D4C03040A0C0000000000000000000000000000
          00000203060813173644364299BF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF2C357B9B00000000000000000000000000000000000000000000
          000000000000131736444858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4757
          C9FB252D69830101030400000000000000000000000000000000000000000000
          0000000000000000000000000000202759704654C6F74858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF1C225064000000000000000000000000000000000000
          0000010103043D49ABD74858CCFF4858CCFF4858CCFF4858CCFF3F4DB6E31013
          2D38000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000B0E20283D49ABD74858CCFF4858
          CCFF4858CCFF4858CCFF4150BBEB05060D100000000000000000000000000000
          00001B214D604858CCFF4858CCFF4858CCFF4858CCFF4858CCFF121633400000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B0E20284654C6F74858
          CCFF4858CCFF4858CCFF4858CCFF242C66800000000000000000000000000000
          00003C49A9D34858CCFF4858CCFF4858CCFF4858CCFF2A337693000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000021285C744858
          CCFF4858CCFF4858CCFF4858CCFF4150BBEB0101030400000000000000000A0C
          1D244858CCFF4858CCFF4858CCFF4858CCFF3F4DB2DF03040A0C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000010103043745
          9EC74858CCFF4858CCFF4858CCFF4858CCFF1317364400000000000000002027
          59704858CCFF4858CCFF4858CCFF4858CCFF2027597000000000000000000000
          00000000000000000000070813181B214D601B214D601B214D601B214D601B21
          4D601B214D60070813180000000000000000000000000000000000000000171C
          40504858CCFF4858CCFF4858CCFF4858CCFF2932738F00000000000000002D37
          809F4858CCFF4858CCFF4858CCFF4858CCFF080A161C00000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000203
          06084554C1F34858CCFF4858CCFF4858CCFF364299BF00000000000000003541
          96BB4858CCFF4858CCFF4858CCFF4352BEEF0000000000000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000000
          00003A47A6CF4858CCFF4858CCFF4858CCFF3E4CAEDB00000000000000003D49
          ABD74858CCFF4858CCFF4858CCFF3F4DB2DF0000000000000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000000
          0000364299BF4858CCFF4858CCFF4858CCFF4654C6F700000000000000003945
          A2CB4858CCFF4858CCFF4858CCFF3F4DB6E30000000000000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000000
          000036439BC34858CCFF4858CCFF4858CCFF4050B9E700000000000000002E3A
          86A74858CCFF4858CCFF4858CCFF4858CCFF0101030400000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000000
          00003F4DB6E34858CCFF4858CCFF4858CCFF36439BC30000000000000000252D
          69834858CCFF4858CCFF4858CCFF4858CCFF10132D3800000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000607
          10144858CCFF4858CCFF4858CCFF4858CCFF2C357B9B00000000000000001A20
          495C4858CCFF4858CCFF4858CCFF4858CCFF2B34799700000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000002027
          59704858CCFF4858CCFF4858CCFF4858CCFF222960780000000000000000090B
          1A204757C9FB4858CCFF4858CCFF4858CCFF4554C1F306071014000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000010103043D49
          ABD74858CCFF4858CCFF4858CCFF4858CCFF1115303C00000000000000000000
          00002D37809F4858CCFF4858CCFF4858CCFF4858CCFF303A89AB000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000262F6B874858
          CCFF4858CCFF4858CCFF4858CCFF3945A2CB0000000000000000000000000000
          00000E1126304858CCFF4858CCFF4858CCFF4858CCFF4858CCFF1E25566C0000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF12163340000000000000000000000000181D43544757C9FB4858
          CCFF4858CCFF4858CCFF4858CCFF171C40500000000000000000000000000000
          000000000000313C8BAF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF2027
          59700000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF121633400000000000000000181D43544757C9FB4858CCFF4858
          CCFF4858CCFF4858CCFF3E4CAEDB000000000000000000000000000000000000
          0000000000000A0C1D244050B9E74858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF0000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000003F4DB2DF4858CCFF4858CCFF4858
          CCFF4858CCFF4757C9FB12163340000000000000000000000000000000000000
          000000000000000000001115303C4554C1F34858CCFF4858CCFF4858CCFF4858
          CCFF0000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000003F4DB2DF4858CCFF4858CCFF4858
          CCFF4858CCFF191E465800000000000000000000000000000000000000000000
          0000000000000000000000000000181D43544858CCFF4858CCFF4858CCFF4858
          CCFF0000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000003F4DB2DF4858CCFF4858CCFF4858
          CCFF222960780000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001A20495C4352BEEF4858CCFF4858
          CCFF0000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000003F4DB2DF4858CCFF4654C6F72128
          5C74000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000060710142D3882A34858
          CCFF0000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000003F4DB2DF333E8EB3090B1A200000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001216
          33400000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000001216334000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000121633404858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF1216334000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001E3054542237
          6060223760602237606022376060223760602237606022376060223760602237
          606022376060223760602237606022376060223760600D152424000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000005080DEDF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF22376060000000000000
          00000000000000000000000000000000000000000000000000002E4980802E49
          80802E4980802E4980802E4980802E49808028407070000000005080DEDF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF22376060111B30302E49
          80802E4980802E4980802E4980802E4980802E4980802E4980805B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5080DEDF000000005080DEDF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF22376060223760605B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5080DEDF000000005080DEDF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF22376060223760605B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5080DEDF000000005080DEDF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF22376060223760605B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5080DEDF000000005080DEDF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF22376060223760605B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5080DEDF000000004D7DDADB5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF22376060223760605B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF4D7DDADB5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF04070C0C3E64AEAF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF1C2E5050263E6C6C5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF578CF6F73C60A6A75B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF15223C3C2C477C7C5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF0A101C1C37599A9B5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF4672C6C70E172828578C
          F6F75B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF304E86870B122020578A
          F2F35B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF4066B2B3010204045080DEDF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF17254040000000001725
          40405080DEDF5B92FFFF5B92FFFF5B92FFFF5B92FFFF578CF6F70B122020263E
          6C6C5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5489EEEF0A101C1C284070705B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5487EAEB1E30545400000000000000000000
          000004070C0C121E343422376060253B6868263E6C6C2237606010192C2C0000
          00001E3054545487EAEB5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF436DBEBF0B12202000000000121E343421345C5C2237
          6060284070702237606015223C3C04070C0C0000000000000000000000000000
          000000000000070B14143C60A6A7578CF6F75A90FAFB3E64AEAF0A101C1C0000
          0000000000000609101018274444294274742E4980802E4980802E4980802E49
          808021345C5C10192C2C0000000000000000000000000305080832508A8B5489
          EEEF5B92FFFF4A77CECF14203838000000000000000000000000000000000000
          000000000000436DBEBF5B92FFFF5B92FFFF5B92FFFF5B92FFFF4D7AD6D70305
          0808000000000000000000000000090E181829427474395A9E9F395A9E9F2237
          60600102040400000000000000000000000000000000345492935B92FFFF5B92
          FFFF5B92FFFF5B92FFFF578CF6F70E1728280000000000000000000000000000
          0000142038385B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF1F32
          585800000000000000001B2B4C4C578CF6F75B92FFFF5B92FFFF5B92FFFF5B92
          FFFF4D7DDADB0D152424000000000000000004070C0C5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF345492930000000000000000000000000000
          000021345C5C5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF2C47
          7C7C00000000070B14145082E2E35B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF4066B2B30000000000000000111B30305B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF4169B6B70000000000000000000000000000
          000010192C2C5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF1B2B
          4C4C000000002E4980805B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF121E34340000000003050808578CF6F75B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF304E86870000000000000000000000000000
          00000000000037599A9B5B92FFFF5B92FFFF5B92FFFF5B92FFFF436ABABB0000
          000000000000395A9E9F5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF223760600000000000000000294274745B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5283E6E7070B14140000000000000000000000000000
          00000000000001020404284070704670C2C34873CACB2E498080030508080000
          00000000000032528E8F5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF21345C5C0000000000000000000000001E305454436A
          BABB4A79D2D337599A9B0A101C1C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000A101C1C5A90FAFB5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5487EAEB06091010000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000032528E8F5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF2C477C7C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000010204042F4A8283578AF2F35B92FFFF5B92FFFF5082
          E2E3253B68680102040400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000006091010223760601E3054540102
          0404000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000171717541A1A
          1A601A1A1A601A1A1A601A1A1A601A1A1A601A1A1A601A1A1A601A1A1A601A1A
          1A601A1A1A601A1A1A601A1A1A601A1A1A601A1A1A600A0A0A24000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000003C3C3CDF4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF1A1A1A60000000000000
          0000000000000000000000000000000000000000000000000000232323802323
          2380232323802323238023232380232323801F1F1F70000000003C3C3CDF4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF1A1A1A600D0D0D302323
          2380232323802323238023232380232323802323238023232380464646FF4646
          46FF464646FF464646FF464646FF464646FF3C3C3CDF000000003C3C3CDF4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF1A1A1A601A1A1A604646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF3C3C3CDF000000003C3C3CDF4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF1A1A1A601A1A1A604646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF3C3C3CDF000000003C3C3CDF4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF1A1A1A601A1A1A604646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF3C3C3CDF000000003C3C3CDF4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF1A1A1A601A1A1A604646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF3C3C3CDF000000003B3B3BDB4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF1A1A1A601A1A1A604646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF3B3B3BDB4646
          46FF464646FF464646FF464646FF464646FF464646FF0303030C2F2F2FAF4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF161616501E1E1E6C4646
          46FF464646FF464646FF464646FF464646FF464646FF444444F72E2E2EA74646
          46FF464646FF464646FF464646FF464646FF464646FF1010103C2222227C4646
          46FF464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF464646FF0808081C2B2B2B9B4646
          46FF464646FF464646FF464646FF464646FF464646FF373737C70B0B0B284444
          44F7464646FF464646FF464646FF464646FF464646FF25252587090909204343
          43F3464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF464646FF303030B3010101043C3C3CDF4646
          46FF464646FF464646FF464646FF464646FF464646FF12121240000000001212
          12403C3C3CDF464646FF464646FF464646FF464646FF444444F7090909201E1E
          1E6C464646FF464646FF464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF464646FF424242EF0808081C1F1F1F70464646FF4646
          46FF464646FF464646FF464646FF414141EB1717175400000000000000000000
          00000303030C0E0E0E341A1A1A601C1C1C681E1E1E6C1A1A1A600C0C0C2C0000
          000017171754414141EB464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF343434BF09090920000000000E0E0E341919195C1A1A
          1A601F1F1F701A1A1A601010103C0303030C0000000000000000000000000000
          000000000000050505142E2E2EA7444444F7454545FB2F2F2FAF0808081C0000
          0000000000000404041013131344202020742323238023232380232323802323
          23801919195C0C0C0C2C000000000000000000000000020202082626268B4242
          42EF464646FF393939CF0F0F0F38000000000000000000000000000000000000
          000000000000343434BF464646FF464646FF464646FF464646FF3A3A3AD70202
          020800000000000000000000000007070718202020742C2C2C9F2C2C2C9F1A1A
          1A60010101040000000000000000000000000000000028282893464646FF4646
          46FF464646FF464646FF444444F70B0B0B280000000000000000000000000000
          00000F0F0F38464646FF464646FF464646FF464646FF464646FF464646FF1818
          185800000000000000001515154C444444F7464646FF464646FF464646FF4646
          46FF3B3B3BDB0A0A0A2400000000000000000303030C464646FF464646FF4646
          46FF464646FF464646FF464646FF282828930000000000000000000000000000
          00001919195C464646FF464646FF464646FF464646FF464646FF464646FF2222
          227C00000000050505143D3D3DE3464646FF464646FF464646FF464646FF4646
          46FF464646FF303030B300000000000000000D0D0D30464646FF464646FF4646
          46FF464646FF464646FF464646FF323232B70000000000000000000000000000
          00000C0C0C2C464646FF464646FF464646FF464646FF464646FF464646FF1515
          154C0000000023232380464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF0E0E0E340000000002020208444444F7464646FF4646
          46FF464646FF464646FF464646FF252525870000000000000000000000000000
          0000000000002B2B2B9B464646FF464646FF464646FF464646FF333333BB0000
          0000000000002C2C2C9F464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF1A1A1A60000000000000000020202074464646FF4646
          46FF464646FF464646FF3F3F3FE7050505140000000000000000000000000000
          000000000000010101041F1F1F70363636C3383838CB23232380020202080000
          0000000000002727278F464646FF464646FF464646FF464646FF464646FF4646
          46FF464646FF464646FF1919195C000000000000000000000000171717543333
          33BB3A3A3AD32B2B2B9B0808081C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000808081C454545FB464646FF464646FF464646FF464646FF4646
          46FF464646FF414141EB04040410000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000002727278F464646FF464646FF464646FF464646FF4646
          46FF464646FF2222227C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000101010424242483434343F3464646FF464646FF3D3D
          3DE31C1C1C680101010400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000040404101A1A1A60171717540101
          0104000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
  object ImageButtons: TcxImageList
    Height = 24
    Width = 24
    FormatVersion = 1
    DesignInfo = 33292297
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDCB38DFFEBD3
          BDFFF3E4D7FFFBF6F1FFFDFAF7FFF6ECE3FFF0DFCFFFE2BF9FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFD8AA7FFFE5C7ABFFFDFAF7FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3E4D7FFDEB6
          91FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EADFFFF7EEE5FFFFFFFFFFFFFFFFFFFAF4
          EFFFEAD2BBFFE4C3A5FFE2C1A1FFE9CEB5FFF5EADFFFFFFFFFFFFFFFFFFFF9F3
          EDFFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF9F3EDFFFFFFFFFFFFFFFFFFF5E8DDFFDAAE
          85FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE3C2A3FFFDFBF9FFFFFF
          FFFFF6ECE3FFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFDFAF7FFFFFFFFFFFFFFFFFFFEFEFDFFE2C1
          A1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE2BF9FFFFFFF
          FFFFFFFFFFFFE4C3A5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFFFFFFFFFDFAF7FFEEDAC7FFDCB38DFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF5EA
          DFFFFFFFFFFFF2E2D3FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFDCB28BFFEAD1B9FFDAAF87FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE4C3
          A5FFF5EADFFFF4E7DBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFE1BD9BFFE2BF9FFFDCB38DFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF6EBE1FFFFFFFFFFF2E2D3FFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD
          83FFE7CAAFFFDEB793FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFE7CBB1FFFFFFFFFFFEFCFBFFDBB189FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFB995FFEEDBC9FFFDFA
          F7FFFFFFFFFFDDB58FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFDAAF87FFFEFCFBFFFFFFFFFFF4E7DBFFD9AB81FFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFB995FFFBF6F1FFFFFFFFFFFFFF
          FFFFFFFFFFFFDAAF87FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CBB1FFFFFFFFFFFFFFFFFFF3E4D7FFDDB5
          8FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFF5EADFFFFFFFFFFFFFFF
          FFFFFEFEFDFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEDD8C5FFFFFFFFFFFFFFFFFFFEFC
          FBFFF3E4D7FFEDD7C3FFEDD8C5FFF3E6D9FFFEFCFBFFFFFFFFFFFFFFFFFFF5EA
          DFFFF9F2EBFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE6C9ADFFFBF7F3FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF8F5FFE5C6A9FFD8AA
          7FFFDCB28BFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAE85FFE8CD
          B3FFEFDCCBFFF7EEE5FFF6EBE1FFEEDAC7FFE6C9ADFFDBB189FFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FFC5F0DBFFF7FDFAFFDEF7
          EBFFFAFEFCFF76DCABFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF5BD5
          9AFF7FDEB0FFA0E6C4FFA5E8C8FF8CE2B8FF55D396FFE9F9F1FFE1F7ECFF97E4
          BFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF55D396FFAFEBCEFFFCFE
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF69D8A2FFE6F9F0FFD6F4E6FF8AE1
          B7FFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF66D8A1FFE4F8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9AE5C1FF97E4BFFFDEF7EBFFD3F4
          E4FFDBF6E9FF63D79FFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFD3F4E4FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFDFF76DCABFF81DFB1FF50D2
          93FF95E4BDFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF9AE5C1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE6F9F0FF92E3BCFF92E3BCFFFFFFFFFFFFFFFFFFA0E6C4FF9AE5C1FF9DE6
          C2FF81DFB1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFDEF7EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD3F4E4FF50D293FF50D293FFFFFFFFFFFFFFFFFFF4FCF8FFAFEBCEFFAAE9
          CAFFD3F4E4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF55D396FFFCFEFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD3F4E4FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF58D498FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF74DBA9FFFFFFFFFFFFFFFFFFF1FBF7FF7CDDAEFF7CDD
          AEFF71DAA7FF50D293FF50D293FF7CDDAEFF7CDDAEFF9DE6C2FFFFFFFFFFFFFF
          FFFFFFFFFFFF74DBA9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF5ED69BFFFFFFFFFFFFFFFFFFE9F9F1FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFFFFFFFFFFFFF
          FFFFFFFFFFFF5ED69BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFECFAF3FFFFFFFFFFFCFEFDFFD3F4E4FFD3F4
          E4FFB2EBD0FF50D293FF50D293FFD3F4E4FFD3F4E4FFE1F7ECFFFFFFFFFFFFFF
          FFFFECFAF3FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FFBDEED6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD3F4E4FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFBDEED6FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF5ED69BFFF1FBF7FFFFFFFFFFFFFFFFFFFFFF
          FFFFD3F4E4FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1FB
          F7FF5ED69BFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF8AE1B7FFFFFFFFFFFFFFFFFFFFFF
          FFFFFCFEFDFFE9F9F1FFE9F9F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8AE1
          B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDDAEFFE9F9F1FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9F9F1FF7CDDAEFF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FF9AE5
          C1FFC0EFD8FFE1F7ECFFE1F7ECFFC0EFD8FF9AE5C1FF58D498FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFDCB28BFFE9CEB5FFEEDAC7FFF0DFCFFFEAD1B9FFE2BF9FFFDAAE85FFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE8CD
          B3FFFEFCFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF6F1FFE4C3
          A5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDEB7
          93FFFEFEFDFFFEFCFBFFF6ECE3FFF2E2D3FFF9F2EBFFFFFFFFFFFFFFFFFFFFFF
          FFFFF0DECDFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDFB995FFDFBA97FFD8AA7FFFD8AA
          7FFFE2C1A1FFDBB189FFD8AA7FFFD8AA7FFFD8AA7FFFDDB58FFFF5EADFFFFFFF
          FFFFFFFFFFFFE9CEB5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF6EBE1FFF8EFE7FFD9AB81FFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF1E0
          D1FFFFFFFFFFFDFBF9FFDDB58FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CAAFFFFFFFFFFFFFFFFFFFEAD1B9FFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDAAF
          87FFFBF6F1FFFFFFFFFFEDD8C5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFDAAF87FFFCF8F5FFFFFFFFFFFFFFFFFFFEFCFBFFDEB7
          93FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFEEDAC7FFFFFFFFFFF6EBE1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFEFDCCBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6EC
          E3FFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE5C6A9FFFFFFFFFFFDFBF9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFE1BD9BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE9CEB5FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFE6C9ADFFFFFFFFFFFDFAF7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFE9CFB7FFF0DFCFFFF0DFCFFFFEFCFBFFFFFFFFFFF8F0E9FFF0DF
          CFFFEEDBC9FFD9AB81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFEEDBC9FFFFFFFFFFF5E8DDFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFF1E0D1FFFFFFFFFFF9F3EDFFD9AB
          81FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFDCB3
          8DFFFDFAF7FFFFFFFFFFECD6C1FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE0BB99FFFFFFFFFFFFFFFFFFEEDA
          C7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AD83FFF4E7
          DBFFFFFFFFFFFCF8F5FFDBB189FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFEEDBC9FFFFFFFFFFFFFF
          FFFFF3E6D9FFDEB793FFD8AA7FFFD8AA7FFFD9AB81FFE2BF9FFFF9F2EBFFFFFF
          FFFFFFFFFFFFE5C6A9FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFF2E3D5FFFFFF
          FFFFFFFFFFFFFFFFFFFFFBF6F1FFF6ECE3FFFDFBF9FFFFFFFFFFFFFFFFFFFEFE
          FDFFEBD3BDFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFE7CA
          AFFFFBF6F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8EFE7FFDFBA
          97FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD9AD83FFE1BD9BFFE8CDB3FFECD5BFFFE5C6A9FFDDB58FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF689BFFFFB7CFFFFFF2F6FFFFFFFFFFFFE0EAFFFFA6C4
          FFFF5E94FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5E94FFFFD3E2FFFFFFFFFFFFE0EAFFFFB7CFFFFFDBE7FFFFFFFF
          FFFFD6E4FFFF5E94FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFADC9FFFFFFFFFFFFA3C2FFFF5B92FFFF5B92FFFF5B92FFFFAFCA
          FFFFFFFFFFFFA3C2FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFD6E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF91B6
          FFFF5B92FFFF5B92FFFF6D9EFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94
          FFFFEDF3FFFFD6E4FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF82AC
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFFD6E4FFFFEAF1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFE8F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9BBD
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6095
          FFFFF0F5FFFFD8E5FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF99BBFFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6FFFF94B8
          FFFF5B92FFFF7CA8FFFFFFFFFFFFE8F0FFFF75A3FFFF5B92FFFF6397FFFFC4D8
          FFFFFFFFFFFF99BBFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF7AA7FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFF72A1FFFF6095FFFFFFFFFFFFFFFFFFFFEDF3FFFFD6E4FFFFF5F8FFFFFFFF
          FFFFAFCAFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFE3ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF0F5FFFF75A3FFFF9BBDFFFF9BBDFFFFDBE7FFFFF5F8FFFFD1E0FFFF94B8
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF0F5FFFF82ACFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFE3ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFE5EEFFFFC6D9FFFFC1D6FFFF70A0FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF75A3FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF8CB2FFFF5E94FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFA8C5FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4
          FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFFCEDFFFFF689BFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFFE5EEFFFF96B9FFFFF5F8FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84ADFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFFD6E4FFFF7AA7FFFFF0F5FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF84ADFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFC6D9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F6FFFF72A1FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF968D97FFE9E7
          E9FFA8A1A9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8D838EFFE2DFE2FFFFFF
          FFFFFDFDFDFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCAC6CBFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFFFFFFFFFFFFFFFFFFFF
          FFFFEBE9EBFF908791FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9D959EFFFAF9FAFFFFFFFFFFFFFFFFFFFAF9
          FAFF9D959EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF9A919AFFB1AB
          B2FFB7B0B7FFA49DA5FF928993FFEBE9EBFFFFFFFFFFFFFFFFFFFFFFFFFFAFA9
          B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF9D959EFFDCD9DDFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C2C7FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFA69FA7FFF6F5F6FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE0DDE0FF8D838EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8F8590FFF4F3F4FFFFFFFFFFFFFFFFFFD3D0D4FFA69F
          A7FF9A919AFFB7B0B7FFF0EFF1FFFFFFFFFFFFFFFFFFBEB8BFFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFBEB8BFFFFFFFFFFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF968D97FFF2F1F2FFFFFFFFFFF4F3F4FF8F8590FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFE4E1E4FFFFFFFFFFEDEBEDFF8D838EFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFB8B2B9FFFFFFFFFFFFFFFFFFA49DA5FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFD3D0D4FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF9B939CFFFFFFFFFFFFFFFFFFB7B0B7FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFEFEDEFFFFFFFFFFFDEDBDFFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFAAA3ABFFFFFFFFFFFFFFFFFFB1ABB2FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFD2CED2FFFFFFFFFFFBFBFBFF968D97FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFD3D0D4FFFFFFFFFFFFFFFFFF9D959EFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFA8A1A9FFFFFFFFFFFFFFFFFFE7E5E8FF968D97FF8B81
          8CFF8B818CFF8B818CFFC3BEC4FFFFFFFFFFFFFFFFFFE0DDE0FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFFFFFFFFFFBFBFBFFD7D4
          D7FFD5D2D6FFF2F1F2FFFFFFFFFFFFFFFFFFFAF9FAFF9F97A0FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8D838EFFD3D0D4FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0EFF1FF9D959EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA199A2FFD7D4D7FFE6E3
          E6FFF0EFF1FFDEDBDFFFBAB4BBFF948B95FF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF70D4F3FF9BE1F6FF18B9EBFFA3E3F7FF3CC4EEFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFF7FDFEFFFFFFFFFFF3FBFEFF24BD
          ECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF3CC4EEFFF3FBFEFFFFFFFFFFFFFFFFFFD7F3FCFF10B7
          EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF3CC4EEFFF3FBFEFFFFFFFFFFC3EDFAFF0CB6
          EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF3CC4EEFFF3FBFEFFFFFFFFFFC3ED
          FAFF0CB6EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF3CC4EEFFF3FBFEFFFFFF
          FFFFC3EDFAFF0CB6EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF3CC4EEFFF3FB
          FEFFFFFFFFFFC3EDFAFF0CB6EAFF00B2E9FF20BCECFF78D6F3FF6CD3F2FF2CBF
          EDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF3CC4
          EEFFF3FBFEFFFFFFFFFFC7EEFAFFAFE7F8FFFBFEFFFFCBEFFBFFE3F7FDFFFBFE
          FFFF7CD7F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF3CC4EEFFF3FBFEFFFFFFFFFFD3F2FBFF2CBFEDFF00B2E9FF00B2E9FF64D0
          F2FFFBFEFFFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF9BE1F6FFE3F7FDFF10B7EAFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF97E0F6FFD3F2FBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFDFF5FCFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF50CAF0FFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF1CBAEBFFFFFFFFFF2CBFEDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF78D6F3FFD7F3FCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF08B4EAFFFBFEFFFF54CBF0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF18B9
          EBFFEBF9FDFF83DAF4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF9BE1F6FFE3F7FDFF20BCECFF00B2E9FF00B2E9FF24BDECFFCBEF
          FBFFCFF1FBFF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF0CB6EAFFBFECF9FFF3FBFEFFAFE7F8FFBBEAF9FFFBFEFFFFBBEA
          F9FF18B9EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF04B3E9FF60CFF1FF9BE1F6FF9FE2F7FF50CAF0FF04B3
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF84ADFFFFBCD3FFFFB7CFFFFF72A1FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFFC9DBFFFFFFFFFFFFFFFFFFFFA8C5FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF8EB4FFFFEDF3FFFFE0EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD8E5
          FFFFEAF1FFFF689BFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFC1D6FFFFD6E4FFFFD6E4FFFFD6E4FFFFD6E4FFFF6599
          FFFFEAF1FFFFFFFFFFFFFFFFFFFFFAFCFFFFC4D8FFFFC9DBFFFFFCFDFFFFFFFF
          FFFFFFFFFFFFBFD4FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F6FFFF6599
          FFFFE0EAFFFFFFFFFFFFFAFCFFFF75A3FFFF5B92FFFF5B92FFFF91B6FFFFFFFF
          FFFFFFFFFFFFC6D9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3E2FFFF5B92
          FFFF94B8FFFFFFFFFFFFDEE9FFFF5B92FFFF5B92FFFF5B92FFFF6095FFFFF7FA
          FFFFFFFFFFFF70A0FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFB2CCFFFFD6E4FFFFD6E4FFFFD6E4FFFFB7CFFFFF5B92
          FFFFADC9FFFFFFFFFFFFFCFDFFFF6A9CFFFF5B92FFFF5B92FFFF7CA8FFFFFFFF
          FFFFFFFFFFFF9EBEFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF82ACFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF689B
          FFFFF2F6FFFFFFFFFFFFFFFFFFFFD6E4FFFF8CB2FFFF94B8FFFFE0EAFFFFFFFF
          FFFFFFFFFFFFE0EAFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFFAFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADC9
          FFFF9EBEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FA
          FFFF77A5FFFF89B1FFFF77A5FFFFDEE9FFFFFFFFFFFFFFFFFFFFC9DBFFFF689B
          FFFF7CA8FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFF0F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFF2F6FFFF84ADFFFF5B92FFFF9BBDFFFFF5F8FFFFF2F6FFFF8EB4FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF6397FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF70A0FFFF6D9EFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFE3ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99BBFFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFFFFFFFFFB4CDFFFF77A5FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADC9FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFFCFDFFFFE3ECFFFFC4D8FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFADC9FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF94B8FFFFADC9FFFFADC9FFFFADC9FFFFADC9FFFFADC9
          FFFFADC9FFFFADC9FFFFADC9FFFFADC9FFFFADC9FFFF70A0FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF7682D9FF7682D9FF7682D9FF7682D9FF7682D9FF7682D9FF7682
          D9FF7682D9FF7682D9FF7682D9FF7682D9FF7682D9FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFFFFFFFFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4B5BCDFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4B5BCDFF5F6DD2FF4858CCFF5160CEFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFFA1A9E5FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFC6CBEFFF737F
          D8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFFC6CBEFFFB4BBEAFF5665D0FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD1D5F2FFF9FA
          FDFF848FDDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF6A77D6FFFCFCFEFFFCFCFEFFD7DA
          F4FFBAC0ECFFA4ACE6FFA4ACE6FFAFB6E9FFC6CBEFFFEEEFFAFFFFFFFFFFFFFF
          FFFFFFFFFFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF969FE2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF4858CCFF8A94DEFFF4F5
          FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF4858CCFF4858CCFF5362
          CFFFA1A9E5FFDADDF5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF5968D1FF7985DAFF8D97DFFF98A1E2FFE8EAF9FFFFFF
          FFFF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFD1D5F2FF98A1
          E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4E5DCEFF7682D9FF4858CCFF737FD8FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFF7682D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFFFFFFFFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6D
          D2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FF5F6DD2FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6599FFFFBAD1FFFF99BB
          FFFFD8E5FFFF5E94FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF72A1FFFFF5F8FFFFF7FA
          FFFFF5F8FFFFEAF1FFFF89B1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6D9EFFFFFAFCFFFFEAF1FFFF6095
          FFFF96B9FFFFE8F0FFFF689BFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF99BBFFFFF2F6FFFF75A3
          FFFFA8C5FFFFF5F8FFFFC6D9FFFF5B92FFFF5B92FFFF77A5FFFF689BFFFF6D9E
          FFFF7AA7FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFBCD3FFFFC9DBFFFFFCFD
          FFFFF5F8FFFFB2CCFFFF5B92FFFF5B92FFFF5E94FFFFF2F6FFFFC9DBFFFFCEDF
          FFFFE0EAFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF7AA7FFFFC6D9
          FFFF72A1FFFF89B1FFFF5B92FFFF84ADFFFFADC9FFFFF2F6FFFFFFFFFFFFFFFF
          FFFFE8F0FFFFAFCAFFFF8EB4FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFFA8C5FFFFFCFDFFFFEDF3FFFF6A9CFFFF6A9C
          FFFFEDF3FFFFF5F8FFFF9BBDFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6A9CFFFF84ADFFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF6095FFFFD3E2FFFFD8E5FFFF5B92FFFF5B92
          FFFFD8E5FFFFD6E4FFFF6599FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF7AA7FFFF5B92FFFF99BBFFFFFFFFFFFFA3C2
          FFFF5B92FFFF72A1FFFF5B92FFFFB2CCFFFFFFFFFFFFFFFFFFFFADC9FFFFB2CC
          FFFFFFFFFFFFFFFFFFFFBAD1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFA0C0FFFFFFFFFFFFBAD1FFFFD1E0FFFFFFFFFFFFD8E5
          FFFFBAD1FFFFFCFDFFFF91B6FFFF6D9EFFFF6D9EFFFFD8E5FFFFFFFFFFFFFFFF
          FFFFE3ECFFFF689BFFFF689BFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF82ACFFFFF7FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FAFFFF82ACFFFF5B92FFFF5B92FFFFCEDFFFFFA3C2FFFF8EB4
          FFFFD3E2FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF6D9EFFFFE0EAFFFFFFFFFFFF9BBDFFFF6599FFFF9BBD
          FFFFFFFFFFFFE5EEFFFF6D9EFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFC1D6FFFFFFFFFFFFFFFFFFFFD8E5FFFF5B92FFFF5B92FFFF5B92
          FFFFD8E5FFFFFFFFFFFFFFFFFFFFC1D6FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFA8C5FFFFD6E4FFFFFFFFFFFFF0F5FFFF5E94FFFF5B92FFFF5E94
          FFFFF0F5FFFFFFFFFFFFD6E4FFFFA8C5FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFFE0EAFFFFFFFFFFFFD6E4FFFFA3C2FFFFD6E4
          FFFFFFFFFFFFE0EAFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF99BBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF96B9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF75A3FFFFE0EAFFFF82ACFFFFAFCAFFFFFFFFFFFFBCD3
          FFFF82ACFFFFE0EAFFFF75A3FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF99BBFFFFFFFFFFFF9BBD
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000D1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD5D914FFE1E458FFE2E55CFFD5DA18FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFE1E458FFFBFCEBFFEBED8FFFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFF9FADFFFF9FADFFFF9FADFFFF9FADFFFF9FADFFFD6DB
          1CFFDDE040FFFFFFFFFFFAFAE3FFD1D600FFD1D600FFF9FADFFFF9FADFFFF9FA
          DFFFF9FADFFFF9FADFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFF1F2AFFFE2E560FFE2E560FFE2E560FFD3D8
          0CFFF1F3B3FFFFFFFFFFEEF0A3FFD1D600FFD1D600FFE2E560FFE2E560FFE2E5
          60FFEEF09FFFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFDDE040FFD1D600FFD1D600FFD1D600FFD1D6
          00FFFBFBE7FFFFFFFFFFE9EC87FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFDDE040FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFDDE040FFD1D600FFD1D600FFEDEF9BFFF9FA
          DFFFFFFFFFFFFFFFFFFFFCFCEFFFF9F9DBFFD7DB20FFD1D600FFD1D600FFD1D6
          00FFDDE040FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFDDE040FFD1D600FFD1D600FFD5DA18FFF9F9
          DBFFFFFFFFFFFFFFFFFFFFFFFFFFE4E768FFD1D600FFD1D600FFD1D600FFD1D6
          00FFDDE040FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFDDE040FFD1D600FFD1D600FFD1D600FFD8DC
          28FFFBFBE7FFFFFFFFFFE8EB80FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFDDE040FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFDDE040FFD1D600FFD1D600FFD1D600FFD1D6
          00FFDBDF38FFE9EB83FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFDDE040FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFDDE040FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFDDE040FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFDDE040FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFDDE040FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFF2F3B7FFEEF09FFFEEF09FFFEEF09FFFEEF0
          9FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF0
          9FFFF2F3B7FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDF3FFEBED8FFFFAFA
          E3FFEFF1A7FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAE3FFDDE144FFF2F3
          B7FFE0E454FFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF0
          9FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF09FFFEEF0
          9FFFEEF09FFFEEF09FFFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D6
          00FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FFD1D600FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFFADEA
          CCFFFFFFFFFFFFFFFFFFFFFFFFFFBAEDD5FF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF92E3BCFF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF9DE6
          C2FFFFFFFFFFFFFFFFFFFFFFFFFFADEACCFF7CDDAEFF7CDDAEFF7CDDAEFF7CDD
          AEFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7CDD
          AEFFFFFFFFFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF0000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF00000000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF97E0F6FF97E0F6FF2CBFEDFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFB7E9
          F9FF44C7EFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFBFEFFFF9FE2
          F7FF18B9EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFAFE7F8FF24BDECFF00B2
          E9FF0CB6EAFF68D1F2FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFF00B2E9FF14B8EBFF4CC9
          F0FFFFFFFFFFFFFFFFFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFDFF5FCFFFFFF
          FFFFFFFFFFFFFFFFFFFFEBF9FDFF10B7EAFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFA3E3F7FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF8BDCF5FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF1CBAEBFFF7FDFEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFEFFFF28BEECFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E9F9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF08B4EAFFE3F7
          FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E9F9FF08B4EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCE
          F1FFFFFFFFFFFFFFFFFFD7F3FCFF48C8EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FFB7E9F9FF6CD3F2FF04B3E9FF00B2E9FF1CBAEBFFA3E3F7FF8BDCF5FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF08B4EAFF80D9F4FFF7FDFEFFFFFFFFFFA3E3F7FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF40C5EFFFE3F7FDFFFFFFFFFFF7FDFEFF7CD7F4FF08B4EAFF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF18B9EBFFBFECF9FFA7E4F7FF1CBAEBFF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00000000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
          A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF8CE2B8FFE9F9
          F1FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF53D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF79DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF5BD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF50D293FF55D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF8AE1B7FFE6F9F0FF6BD9A4FF50D293FF50D293FF50D2
          93FF60D69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF53D395FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF66D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF69D8A2FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF6BD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF53D395FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF79DDACFFF7FDFAFFFFFFFFFFB8EDD3FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF71DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF81DFB1FF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          2000000000000009000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFBFECF9FFBFECF9FFBFECF9FFBFEC
          F9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFBFECF9FFB3E8
          F8FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FFD7F3FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF0CB6EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF40C5EFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF34C2EDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF6CD3F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF68D1F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF8FDDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF93DEF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB7E9F9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FFC3EDFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE7F8FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF70D4F3FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
          F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF80D9
          F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF64D0F2FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
          F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2F7FF8BDC
          F5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFF5
          FCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF74D5F3FF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF20BCECFF1CBA
          EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF64D0F2FF9FE2F7FF9FE2F7FF9FE2F7FF9FE2
          F7FF3CC4EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00000000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF98A1E2FFF6F7FDFFB7BEEBFF4B5BCDFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFB1B8
          E9FFF6F7FDFFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
          CDFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4B5BCDFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4B5BCDFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
          CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF8792DEFFDFE2F6FFA1A9E5FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4C5BCDFF4C5BCDFF4858CCFF505FCEFF4858CCFF4858CCFF4858
          CCFF4858CCFF4B5ACDFF4B5ACDFF4858CCFF4858CCFF4858CCFF4B5ACDFF4858
          CCFF4B5ACDFF505FCEFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFADB4E8FFB8BEEBFFB4BAEAFFB3B9E9FFAFB6E8FFB4BBEAFFB4BA
          EAFFAFB6E8FEBCC2ECFFADB5E8FFB5BCEAFFBBC2ECFFA6AEE6FFB8BEEBFFB1B8
          E9FFACB4E8FFAAB1E7FFB1B8E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFF8F8FDFFF9F9FCFFFAFBFEFFFBFBFEFFFDFDFEFFFCFCFEFFFDFD
          FEFFFCFCFEFFFAFBFEFFF9FAFDFFF9FAFDFFFBFBFEFFFBFCFEFFFAFAFEFFF4F5
          FCFFF6F6FCFFF8F9FDFFF3F3FBFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFFAFAFEFFFDFDFEFFFEFEFFFFFCFCFEFFFDFDFFFFFEFEFFFFFDFD
          FFFFFBFCFEFFFCFCFEFFFDFDFEFFFCFDFEFFFEFEFEFFFEFEFFFFFEFEFFFFFBFB
          FEFFF9FAFDFFFCFDFEFFFCFCFEFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4959CCFF818CDCFF858FDDFF858FDDFF858FDDFF868FDFFF858FDDFE7C87
          DAFE7884D8FD838EDDFF7D88DAFE8893DEFF7581D8FF808CDCFF7D89DBFF808B
          DCFF8691DEFF808BDCFF7581D9FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4959CCFF4959CCFF4C5BCDFF4C5BCDFF4959CBFE4E5E
          CDFF4D5DCDFF4959CBFE4959CBFE4959CAFD4858C9FB4858C9FB4959CBFE4858
          CCFF4858CCFF4959CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFAFB6
          E9FF4858CCFF4858CBFE4858CBFE4756C8FA98A1E2FFC3C8EEFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFACB3E8FFFFFFFFFFFFFF
          FFFF959BC3D54757CAFD4858CBFE98A1E2FFFFFFFFFFFFFFFFFFC3C8EEFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFF
          FFFFFFFFFFFFAFB6E9FF98A1E2FFFFFFFFFFFFFFFFFFFFFFFFFF98A1E2FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4756C8FA848FDDFFFCFC
          FEFFFFFFFFFFFDFDFDFDF4F4F4F4FFFFFFFFFFFFFFFF98A1E2FF4858CBFE4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CBFE4858CBFE848F
          DDFFFCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFF98A1E2FF4858CBFE4858CBFE4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CBFE98A1
          E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CBFE4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1E2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFCFCFEFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFF
          FFFFFFFFFFFF98A1E2FF848FDDFFFCFCFEFFFFFFFFFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFF
          FFFF98A1E2FF4858CCFF4858CCFF848FDDFFFCFCFEFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1E2FF98A1
          E2FF4858CCFF4858CCFF4858CCFF4858CCFF848FDDFFACB3E8FF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          200000000000000900000000000000000000000000000000000050D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
          D6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEED6FFBDEE
          D6FFBDEED6FFBDEED6FFBDEED6FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
          BCFF92E3BCFF92E3BCFF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF81DFB1FF92E3BCFF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF9DE6C2FFFCFEFDFFFFFFFFFFADEACCFF53D395FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF53D395FFBAED
          D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCBF2DFFF58D498FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF7FDEB0FFA8E9
          C9FFBDEED6FFFFFFFFFFFFFFFFFFC8F1DDFFA8E9C9FF8AE1B7FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF7CDDAEFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF7CDDAEFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF7CDDAEFFFFFFFFFFFFFFFFFF92E3BCFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF6BD9A4FFBDEED6FFBDEED6FF79DDACFF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
          93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF6D9EFFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF689B
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFCCDDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCDD
          FFFFF0F5FFFFDBE7FFFFE8F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9BBD
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF7AA7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7AA7
          FFFFB4CDFFFF84ADFFFFADC9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFCFFFF6599
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFFA3C2FFFFFFFFFFFFFFFFFFFFF5F8FFFF5B92
          FFFF8CB2FFFF6095FFFF7FAAFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3C2FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF9BBDFFFFEAF1FFFFCCDDFFFF5B92
          FFFFAFCAFFFF89B1FFFF5B92FFFFF0F5FFFFF5F8FFFF9EBEFFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFF6A9CFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF8CB2FFFF6A9CFFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF7AA7FFFFD6E4
          FFFFFAFCFFFFF0F5FFFF84ADFFFFA6C4FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6
          FFFFC1D6FFFFBAD1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF75A3FFFFFAFCFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF84ADFFFFEAF1FFFFFFFFFFFFFFFFFFFFA3C2FFFFFFFF
          FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC9DBFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF84ADFFFFEAF1FFFFFFFFFFFFFFFFFFFF84ADFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFEAF1FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF84ADFFFFEAF1FFFFFFFFFFFFF2F6FFFF6095FFFFF5F8
          FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFDEE9FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF84ADFFFFEAF1FFFFFFFFFFFFFFFFFFFFE5EEFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFA0C0FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFADC9FFFF8CB2FFFFADC9FFFFADC9FFFFADC9FFFFADC9
          FFFFADC9FFFF9EBEFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFBAD1FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFE8F0FFFF6A9CFFFFAFCAFFFF5B92FFFF5B92FFFF5B92
          FFFF8CB2FFFF89B1FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF72A1
          FFFF96B9FFFF89B1FFFF6095FFFF5B92FFFFC9DBFFFF75A3FFFF5B92FFFF6095
          FFFFD6E4FFFF70A0FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFFC1D6FFFFCCDDFFFFD3E2
          FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4E5DCEFF7682D9FFA1A9E5FFC0C5EDFFD1D5F2FFE8EAF9FFF9FA
          FDFFFFFFFFFFFFFFFFFFEBEDF9FFDFE2F6FFD1D5F2FF9BA4E3FF5160CEFFB1B8
          E9FFDFE2F6FFCED2F1FF7C87DAFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFFACB3E8FFF6F7FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F7FDFF6572D4FFDDE0F5FFFFFF
          FFFFEBEDF9FFF4F5FCFFFFFFFFFF8792DEFF4858CCFF4858CCFF4858CCFF4858
          CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D8F3FF818CDCFFE8EAF9FFA4AC
          E6FF8D97DFFF6572D4FFE5E7F8FFEBEDF9FF4858CCFF4858CCFF4858CCFF4858
          CCFF5C6AD2FFFCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7BEEBFF9BA4E3FFD1D5F2FF4858
          CCFF4858CCFF4858CCFF848FDDFFFFFFFFFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFFA6AEE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDDE0F5FF7985DAFFF9FAFDFFE8EA
          F9FFC0C5EDFF8D97DFFFFCFCFEFFDDE0F5FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4E5DCEFFDFE2F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6572D4FFC6CBEFFFFFFF
          FFFFF9FAFDFFFFFFFFFFF9FAFDFF7682D9FF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF5968D1FFE2E5F7FFFFFFFFFFFFFFFFFFFFFFFFFFF4F5
          FCFFC6CBEFFFCBD0F1FFF6F7FDFFFFFFFFFFFFFFFFFFEEEFFAFF6875D5FF9099
          E0FFB7BEEBFFACB3E8FF6572D4FF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF5665D0FFBDC3EDFFFFFFFFFFAFB6E9FF4858
          CCFF4858CCFF4858CCFF5160CEFFD4D8F3FFFCFCFEFFAFB6E9FF5160CEFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF6875D5FF4858CCFF5362
          CFFF7985DAFF7985DAFF5160CEFF5160CEFF6D7AD6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF848FDDFFF4F5
          FCFFFFFFFFFFFFFFFFFFF4F5FCFF8792DEFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5F6DD2FFF6F7FDFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFEFF6572D4FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF98A1E2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA6AEE6FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFB1B8E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7BEEBFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF8A94DEFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF969FE2FF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFDDE0F5FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFDFE2F6FF4E5DCEFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF5160CEFFA9B0
          E7FFDFE2F6FFDFE2F6FFA9B0E7FF5362CFFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
          CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      end>
  end
  object cxOpenDialogLic: TOpenTextFileDialog
    Filter = '*.lic'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Archivo de Licencia'
    Left = 86
    Top = 475
  end
  object cxBrowserServidor: TcxShellBrowserDialog
    FolderLabelCaption = 'f'#243'lder Servidor'
    Options.TrackShellChanges = False
    ShowButtons = False
    Title = 'Buscar F'#243'lder de Servidor'
    Left = 10
    Top = 394
  end
  object DataSourceDlls: TDataSource
    Left = 86
    Top = 570
  end
  object DataSourceServicios: TDataSource
    Left = 6
    Top = 562
  end
  object DSLogicalDrives: TDataSource
    Left = 154
    Top = 584
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 136
    Top = 416
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGrayText
      Font.Height = -15
      Font.Name = 'SEGOE UI'
      Font.Style = []
    end
  end
  object cxOpenDialogArchivosBD: TcxShellBrowserDialog
    FolderLabelCaption = 'Bases de datos'
    Options.TrackShellChanges = False
    ShowButtons = False
    Title = 'Buscar folder de bases de datos'
    Left = 14
    Top = 450
  end
  object DevEx_cxLocalizer: TcxLocalizer
    StorageType = lstResource
    Left = 2
    Top = 602
  end
  object AbZipper: TAbZipper
    AutoSave = False
    DOSMode = False
    Left = 76
    Top = 600
  end
  object cxOpenDialogErrorLic: TcxShellBrowserDialog
    FolderLabelCaption = 'Archivo3dt'
    Options.TrackShellChanges = False
    ShowButtons = False
    Title = 'Buscar folder para guardar archivo 3dt'
    Left = 100
    Top = 600
  end
  object DataSourceUsuariosSistema: TDataSource
    Left = 54
    Top = 498
  end
end
