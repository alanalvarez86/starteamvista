program ConfiguradorTress;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Vcl.Forms,
  FConfigShell in 'FConfigShell.pas' {FConfiguradorTRESS},
  DDBConfig in 'DDBConfig.pas' {dmDBConfig: TDataModule},
  FSentinelWrite in 'FSentinelWrite.pas' {SentinelWrite},
  ConfigConexion in 'ConfigConexion.pas' {ConfigurarConexion},
  DesbloqUsuariosAdmin in 'DesbloqUsuariosAdmin.pas' {DesbloqueaUsuariosAdmin},
  FWizConexionComparte in 'FWizConexionComparte.pas' {ComparteConfig},
  FConfigurarWorkflow in 'FConfigurarWorkflow.pas' {ConfigurarWorkflow},
  FWizActualizaComparte in 'FWizActualizaComparte.pas' {ActualizaComparte},
  FExportaDatosSistLic in '..\FExportaDatosSistLic.pas',
  FErrorGrabarArchivo3dt in '..\FErrorGrabarArchivo3dt.pas' {ErrorGrabarArchivo3dt},
  Dispatcher3DTProxy in '..\WebServices\Dispatcher3DTProxy.pas';

{$R *.res}

{***DevEx (by am): Spanish.RES es el recurso con todas las traducciones de textos para los componentes de
DevExpress. El recruso es controlado por los componentes cxLocalizer que se encuentran en el Shell y en la forma
de la Matriz de habilidades. Puede haber varios cxLocalizer en el proyecto, pero debe haber un solo recurso de traduccion.
Estos recursos son generados desde un proyecto el cual se encuentra en StartTeam (Traducciones\Spanish.ini),. Cuando se
requiera agregar un texto el Spanish.ini debe ser modificado, y el Spanish.RES debe ser construido y cambiado en los folders
de los proyectos.

La siguiente directiva es la que indica que se utilizara este recurso el cual debe estar presente en el folder del proyecto.
***}

{$R ..\Traducciones\Spanish.RES}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.HelpFile := 'ConfiguradorTRESS.chm';
  Application.Title := 'ConfiguradorTRESS';
  Application.CreateForm(TFConfiguradorTRESS, FConfiguradorTRESS);
  Application.Run;
end.
