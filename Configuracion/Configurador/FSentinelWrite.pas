unit FSentinelWrite;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Checklst, Mask,
     FAutoServer,
     ZetaNumero,
     ZetaDBTextBox, ComCtrls, ZetaWinAPITools, ZetaEdit, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, dxSkinsDefaultPainters, cxButtons, TressMorado2013;

type
  TSentinelWrite = class(TForm)
    EmpresaLBL: TLabel;
    Label1: TLabel;
    UsuariosLBL: TLabel;
    EmpleadosLBL: TLabel;
    ModulosLBL: TLabel;
    Clave1LBL: TLabel;
    Clave2LBL: TLabel;
    Clave2: TEdit;
    Clave1: TEdit;
    SentinelLBL: TLabel;
    Sentinel: TZetaTextBox;
    Empresa: TZetaTextBox;
    Version: TComboBox;
    Usuarios: TZetaNumero;
    Empleados: TZetaNumero;
    Parametro: TZetaNumero;
    SQLEngineLBL: TLabel;
    PlataformaLBL: TLabel;
    Plataforma: TComboBox;
    SQLEngine: TComboBox;
    PageControl1: TPageControl;
    tabAutorizacion: TTabSheet;
    panelBotones: TPanel;
    ReadOnlyLBL: TLabel;
    tabAutorizacionVirtual: TTabSheet;
    VirtualUsuariosLBL: TLabel;
    VirtualEmpleadosLBL: TLabel;
    VirtualModulosLBL: TLabel;
    VirtualClave1LBL: TLabel;
    VirtualClave2LBL: TLabel;
    VirtualClave1: TEdit;
    VirtualClave2: TEdit;
    VirtualUsuarios: TZetaNumero;
    VirtualEmpleados: TZetaNumero;
    VirtualParametro: TZetaNumero;
    Label14: TLabel;
    Label15: TLabel;
    lblNombreServer: TLabel;
    OK: TcxButton;
    Cancelar: TcxButton;
    lstPlataformaVirtual: TComboBox;
    lblPlataforma: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ArchivoLicenciaChange(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    EsSentinelFisico : Boolean;
    procedure CargaDatosSentinel;
    {$ifdef MSSQL}
    function SeConfiguroArchivoLicenciaVirtual : boolean;
    procedure CargaDatosSentinelVirtual;
    {$endif}
    procedure CargaDatosSentinelFisico;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  end;

var
  SentinelWrite: TSentinelWrite;

implementation

uses FAutoClasses,
     //FHelpContext,
{$ifdef MSSQL}
     FSentinelRegistry,
     ZetaClientTools,
     ZetaCommonTools,
{$endif}
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TSentinelWrite.FormCreate(Sender: TObject);
begin
{$ifndef MSSQL}
     tabAutorizacion.TabVisible := False;
     tabAutorizacion.Visible := True;
{$endif}
     FAutoClasses.LoadVersions( Self.Version.Items );
     with AutoServer do
     begin
          LoadPlataformas( Self.Plataforma.Items );
          LoadPlataformas( lstPlataformaVirtual.Items );
          LoadSQLEngines( Self.SQLEngine.Items );

     end;
end;

procedure TSentinelWrite.FormShow(Sender: TObject);
begin
     CargaDatosSentinel;
     if ( EsSentinelFisico ) then
        Self.Height := 322
     else
         Self.Height := 315;

     tabAutorizacion.TabVisible := False;
     tabAutorizacionVirtual.TabVisible := False;
     tabAutorizacion.Visible := EsSentinelFisico;
     tabAutorizacionVirtual.Visible := not EsSentinelFisico;


     if (EsSentinelFisico) then
     begin
        if ( Self.Usuarios.ValorEntero <=0 ) then
           Self.Usuarios.SetFocus
        else
            Self.Clave1.SetFocus;
     end
     else
     begin
        if ( Self.VirtualUsuarios.Enabled ) then
        begin
           if ( Self.VirtualUsuarios.ValorEntero <=0 ) then
              Self.VirtualUsuarios.SetFocus
           else
               Self.VirtualClave1.SetFocus;
        end;
     end;

     //HelpContext := H00014_Actualizar_sentinel;
end;

procedure TSentinelWrite.OKClick(Sender: TObject);
const
     CR_LF = Chr( 13 ) + Chr( 10 );
var
   oCursor: TCursor;

{$ifdef MSSQL}
   function  ActualizarSentinelVirtual : boolean;
   var
      FRegistrySent: TSentinelRegistryServer;
      oSentinelServer: TSentinelServer;
      sClave1, sClave2: String;
   begin
        //lEnabled := RegistrarAuto.Enabled;
        Result := False;
        //RegistrarAuto.Enabled := False;
        sClave1 := Self.VirtualClave1.Text;
        sClave2 := Self.VirtualClave2.Text;
        try
           oSentinelServer := TSentinelServer.Create;
           try
              with oSentinelServer do
              begin
                   SentinelLoad( Self.VirtualEmpleados.ValorEntero, Self.VirtualUsuarios.ValorEntero, Self.VirtualParametro.ValorEntero,TPlataforma(lstPlataformaVirtual.ItemIndex) );
                   try
                      Result := SentinelTest( sClave1, sClave2 );
                      if not Result then
                      begin
                           ZetaDialogo.ZError('Error', StatusMsg, 0 );
                      end
                   except
                         on Error: Exception do
                         begin
                              Application.HandleException(Error);
                         end;
                   end;
                   if Result then
                   begin
                        FRegistrySent := TSentinelRegistryServer.Create( True );
                        try
                           try
                              FRegistrySent.Usuarios := VirtualUsuarios.ValorEntero;
                              FRegistrySent.Empleados := VirtualEmpleados.ValorEntero;
                              FRegistrySent.Modulos := VirtualParametro.ValorEntero;
                              FRegistrySent.Clave1 := sClave1;
                              FRegistrySent.Clave2 := sClave2;
                              FRegistrySent.Plataforma := TPlataforma(lstPlataformaVirtual.ItemIndex );
                              SentinelWrite;
                              ZetaDialogo.ZInformation( 'Autorización registrada', 'La autorización ha sido registrada.', 0 );
                           except
                                 on Error: Exception do
                                 begin
                                      Application.HandleException(Error);
                                 end;
                           end;
                        finally
                               FreeAndNil( FRegistrySent );
                        end;
                   end;
              end;
           finally
                  FreeAndNil( oSentinelServer );
           end;
        finally
              // RegistrarAuto.Enabled := lEnabled;
        end;
   end;
{$endif}
begin
     OK.Enabled := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
       if EsSentinelFisico then
       begin
           try
              with AutoServer do
              begin
                   Empleados := Self.Empleados.ValorEntero;
                   Version := Self.Version.Text;
                   Usuarios := Self.Usuarios.ValorEntero;
                   Modulos := Self.Parametro.ValorEntero;
                   Plataforma := TPlataforma( Self.Plataforma.ItemIndex );
                   SQLEngine := TSQLEngine( Self.SQLEngine.ItemIndex );
                   if Actualizar( Clave1.Text, Clave2.Text ) then
                      Self.ModalResult := mrOk
                   else
                       ZetaDialogo.zError('Error', 'Error en claves' + CR_LF + StatusMsg, 0 );
              end;
           except
                 on Error: Exception do
                 begin
                      Application.HandleException( Error );
                 end;
           end;
       end
       else
       begin
{$ifdef MSSQL}
          if ActualizarSentinelVirtual then
             Self.ModalResult := mrOk;
{$endif}
       end;
     finally
            OK.Enabled := True;
            Screen.Cursor := oCursor;
     end;
end;

procedure TSentinelWrite.ArchivoLicenciaChange(Sender: TObject);
begin
end;

{$ifdef MSSQL}
procedure TSentinelWrite.CargaDatosSentinelVirtual;
var
   FRegistry: TSentinelRegistryServer;
   lCanWrite: Boolean;
begin
     // Inicializa valores.
     //lblMensajes.Visible := False;
     Self.VirtualUsuarios.Valor := 0;
     Self.VirtualEmpleados.Valor := 0;
     Self.VirtualParametro.Valor := 0;
     Self.VirtualClave1.Text := VACIO;
     Self.VirtualClave2.Text := VACIO;

     lCanWrite := false;

     FRegistry := TSentinelRegistryServer.Create( True );
     try
        if FSentinelRegistry.CheckComputerInfo then
        begin
             Self.VirtualUsuarios.Valor := FRegistry.Usuarios;
             Self.VirtualEmpleados.Valor := FRegistry.Empleados;
             Self.VirtualParametro.Valor := FRegistry.Modulos;
             Self.VirtualClave1.Text := FRegistry.Clave1;
             Self.VirtualClave2.Text := FRegistry.Clave2;
             Self.lstPlataformaVirtual.ItemIndex := Ord(FRegistry.Plataforma);
             lCanWrite := FRegistry.CanWrite;

        end
        else
        begin
             with Self.ReadOnlyLBL do
             begin
                  Caption := 'Servidor no autorizado';
                  Visible := True;
             end;
        end;

        if not FileExists( FRegistry.ArchivoLicencia ) then
        begin
			//US 1227: Cambiar etiqueta 'Sentinel' por 'Guardia" y en Mensajes de validacion que apliquen
             ZetaDialogo.ZError( 'Error','No se encontró el archivo licencia de Guardia Virtual', 0 );
        end;

        Self.OK.Enabled := lCanWrite;
        Self.VirtualUsuariosLBL.Enabled := lCanWrite;
        Self.VirtualUsuarios.Enabled := lCanWrite;
        Self.VirtualEmpleadosLBL.Enabled := lCanWrite;
        Self.VirtualEmpleados.Enabled := lCanWrite;
        Self.VirtualModulosLBL.Enabled := lCanWrite;

        Self.VirtualParametro.Enabled := lCanWrite;
        Self.VirtualClave1LBL.Enabled := lCanWrite;
        Self.VirtualClave1.Enabled := lCanWrite;
        Self.VirtualClave2LBL.Enabled := lCanWrite;
        Self.VirtualClave2.Enabled := lCanWrite;
        Self.lstPlataformaVirtual.Enabled := lCanWrite;
        Self.lblPlataforma.Enabled := lCanWrite;
        Self.ReadOnlyLBL.Visible := not lCanWrite;

        lblNombreServer.Caption := ZetaWinAPITools.GetComputerName;
     finally
            FreeAndNil( FRegistry );
     end;
end;
{$endif}

procedure TSentinelWrite.CargaDatosSentinelFisico;
begin
     with AutoServer do
     begin
          //Load;
          if NumeroSerie=0 then
          begin
               Load;
          end;
          Self.Sentinel.Caption := IntToStr( NumeroSerie );
          Self.Empresa.Caption := IntToStr( Empresa );
          Self.Version.Text := Version;
          Self.Plataforma.Enabled := True;
          Self.SQLEngine.Enabled  := True;

          with Self.Usuarios do
          begin
               Valor := Usuarios;
               Enabled := not EsKit;
               UsuariosLBL.Enabled := Enabled;
          end;
          with Self.Empleados do
          begin
               Valor := Empleados;
               Enabled := not EsKit;
               EmpleadosLBL.Enabled := Enabled;
          end;
          Self.Plataforma.ItemIndex := Ord( Plataforma );
          Self.SQLEngine.ItemIndex := Ord( SQLEngine );
          Self.Parametro.Valor := Modulos;
     end;
end;




procedure TSentinelWrite.CargaDatosSentinel;
begin
{$ifdef MSSQL}
   CargaDatosSentinelFisico;
   EsSentinelFisico := True;



   if ( AutoServer.SentinelOK )  then
   begin
       EsSentinelFisico := True;
   end
   else
   begin
       if ( SeConfiguroArchivoLicenciaVirtual ) then
       begin
            EsSentinelFisico := False;
            CargaDatosSentinelVirtual;
       end;
   end;
{$else}
    EsSentinelFisico := True;
    CargaDatosSentinelFisico;
{$endif}

end;

{$ifdef MSSQL}
function TSentinelWrite.SeConfiguroArchivoLicenciaVirtual: boolean;
var
   FRegistry: TSentinelRegistryServer;
begin
     FRegistry := TSentinelRegistryServer.Create( True );
     try
        Result :=  StrLleno( FRegistry.ArchivoLicencia );
     finally
            FreeAndNil( FRegistry );
     end;
end;
{$endif}

end.
