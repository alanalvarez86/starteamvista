unit  ZHardwareId;
// {$APPTYPE CONSOLE}
// {$DEFINE Use_Jwscl} //necessary to obtain a hash of the data using md2, md4, md5 or sha1

interface

uses
  {$IFDEF Use_Jwscl}
  JwsclTypes,
  JwsclCryptProvider,
  {$ENDIF}
  Classes,
   SysUtils,
  ActiveX,
  ComObj,
  ZNetworkData,
  Variants,
  Registry, Windows,
  ZetaCommonClasses;
  //JwaIpHlpApi,
  //JwaIpTypes;


type
   TMotherBoardInfo   = (Mb_SerialNumber,Mb_Manufacturer,Mb_Product,Mb_Model);
   TMotherBoardInfoSet= set of TMotherBoardInfo;
   TProcessorInfo     = (Pr_Description,Pr_Manufacturer,Pr_Name,Pr_ProcessorId,Pr_UniqueId);
   TProcessorInfoSet  = set of TProcessorInfo;
   TBIOSInfo          = (Bs_BIOSVersion,Bs_BuildNumber,Bs_Description,Bs_Manufacturer,Bs_Name,Bs_SerialNumber,Bs_Version);
   TBIOSInfoSet       = set of TBIOSInfo;
   TOSInfo            = (Os_BuildNumber,Os_BuildType,Os_Manufacturer,Os_Name,Os_SerialNumber,Os_Version);
   TOSInfoSet         = set of TOSInfo;

   TVideoControllerInfo = (Vc_DriverVersion, Vc_Name);
   TVideoControllerInfoSet = set of TVideoControllerInfo;

   TNetworkInfo = (Nt_DNSHostName);
   TNetworkInfoSet = set of TNetworkInfo;

   TNetworkDevicesInfo = (Nd_DeviceID, Nd_DeviceName);
   TNetworkDevicesInfoSet = set of TNetworkDevicesInfo;

const //properties names to get the data
   MotherBoardInfoArr: array[TMotherBoardInfo] of AnsiString =
                        ('SerialNumber','Manufacturer','Product','Model');

   OsInfoArr         : array[TOSInfo] of AnsiString =
                        ('BuildNumber','BuildType','Manufacturer','Name','SerialNumber','Version');

   BiosInfoArr       : array[TBIOSInfo] of AnsiString =
                        ('BIOSVersion','BuildNumber','Description','Manufacturer','Name','SerialNumber','Version');

   ProcessorInfoArr  : array[TProcessorInfo] of AnsiString =
                        ('Description','Manufacturer','Name','ProcessorId','UniqueId');

   VcInfoArr         : array[TVideoControllerInfo] of AnsiString =
                        ('DriverVersion','Name');

   NtInfoArr         : array[TNetworkInfo] of AnsiString =
                        ('DNSHostName');

   NdInfoArr         : array[TNetworkDevicesInfo] of AnsiString =
                        ('DeviceID', 'Name');

type
   THardwareId  = class
   private
    FOSInfo         : TOSInfoSet;
    FBIOSInfo       : TBIOSInfoSet;
    FProcessorInfo  : TProcessorInfoSet;
    FMotherBoardInfo: TMotherBoardInfoSet;
    FVideoControllerInfo: TVideoControllerInfoSet;
    FNetworkInfo: TNetworkInfoSet;
    FNetworkDevicesInfo: TNetworkDevicesInfoSet;

    FBuffer         : AnsiString;
    FNetworkDevices : AnsiString;
    function GetHardwareIdHex: AnsiString;
  {$IFDEF Use_Jwscl}
    function GetHashString(Algorithm: TJwHashAlgorithm; Buffer : Pointer;Size:Integer) : AnsiString;
    function GetHardwareIdMd5: AnsiString;
    function GetHardwareIdMd2: AnsiString;
    function GetHardwareIdMd4: AnsiString;
    function GetHardwareIdSHA: AnsiString;
  {$ENDIF}
      //MAC ADRESS
    function GetHardwareMACAD: string;
    function GetHardiskSerial : string;
    function  GetWMIstring(const WMIClass, WMIProperty:string): string;
    function GetMachineGUID: string;
   public
     //Set the properties to  be used in the generation of the hardware id
    property  MotherBoardInfo : TMotherBoardInfoSet read FMotherBoardInfo write FMotherBoardInfo;
    property  ProcessorInfo : TProcessorInfoSet read FProcessorInfo write FProcessorInfo;
    property  BIOSInfo: TBIOSInfoSet read FBIOSInfo write FBIOSInfo;
    property  OSInfo  : TOSInfoSet read FOSInfo write FOSInfo;
    property  VideoControllerInfo  : TVideoControllerInfoSet read FVideoControllerInfo write FVideoControllerInfo;
    property  NetworkInfo  : TNetworkInfoSet read FNetworkInfo write FNetworkInfo;
    property  NetworkDevicesInfo  : TNetworkDevicesInfoSet read FNetworkDevicesInfo write FNetworkDevicesInfo;
    property  Buffer : AnsiString read FBuffer; //return the content of the data collected in the system
    property  NetworkDevices : AnsiString read FNetworkDevices;
    property  HardwareIdHex : AnsiString read GetHardwareIdHex; //get a hexadecimal represntation of the data collected
  {$IFDEF Use_Jwscl}
    property  HardwareIdMd2  : AnsiString read GetHardwareIdMd2; //get a Md2 hash of the data collected
    property  HardwareIdMd4  : AnsiString read GetHardwareIdMd4; //get a Md4 hash of the data collected
    property  HardwareIdMd5  : AnsiString read GetHardwareIdMd5; //get a Md5 hash of the data collected
    property  HardwareIdSHA  : AnsiString read GetHardwareIdSHA; //get a SHA1 hash of the data collected

  {$ENDIF}

    property  HardwareMACAD: string read GetHardwareMACAD; //get a MAC ADRESS
    property  MachineGUID: string read GetMachineGUID; //get a MAC ADRESS

    procedure GenerateMotherBoardInfo (slTagErrores: TStringList);
    procedure GenerateOSInfo (slTagErrores: TStringList);
    procedure GenerateBIOSInfo (slTagErrores: TStringList);
    procedure GenerateProcessorInfo (slTagErrores: TStringList);
    procedure GenerateVideoControllerInfo (slTagErrores: TStringList);
    procedure GenerateNetworkInfo (slTagErrores: TStringList);
    procedure GenerarListaDispositivos (slTagErrores: TStringList);
    procedure GenerateHardwareId;
    constructor  Create(Generate:Boolean=True); overload;
    Destructor  Destroy; override;
   end;

      var
      FSWbemLocator : OLEVariant;
      FWMIService   : OLEVariant;


implementation

function VarArrayToStr(const vArray: variant): AnsiString;

  function _VarToStr(const V: variant): AnsiString;
  var
  Vt: integer;
  begin
   Vt := VarType(V);
      case Vt of
        varSmallint,
        varInteger  : Result := AnsiString(IntToStr(integer(V)));
        varSingle,
        varDouble,
        varCurrency : Result := AnsiString(FloatToStr(Double(V)));
        varDate     : Result := AnsiString(VarToStr(V));
        varOleStr   : Result := AnsiString(WideString(V));
        varBoolean  : Result := AnsiString(VarToStr(V));
        varVariant  : Result := AnsiString(VarToStr(Variant(V)));
        varByte     : Result := AnsiChar(byte(V));
        varString   : Result := AnsiString(V);
        varArray    : Result := VarArrayToStr(Variant(V));
      end;
  end;

var
i : integer;
begin
    Result := '[';
    if (VarType(vArray) and VarArray)=0 then
       Result := _VarToStr(vArray)
    else
    for i := VarArrayLowBound(vArray, 1) to VarArrayHighBound(vArray, 1) do
     if i=VarArrayLowBound(vArray, 1)  then
      Result := Result+_VarToStr(vArray[i])
     else
      Result := Result+'|'+_VarToStr(vArray[i]);

    Result:=Result+']';
end;

function VarStrNull(const V:OleVariant):AnsiString; //avoid problems with null strings
begin
  Result:='';
  if not VarIsNull(V) then
  begin
    if VarIsArray(V) then
       Result:=VarArrayToStr(V)
    else
    Result:=AnsiString(VarToStr(V));
  end;
end;

{ THardwareId }

constructor THardwareId.Create(Generate:Boolean=True);
begin
   inherited Create;
   CoInitialize(nil);
   FBuffer          := '';
   FNetworkDevices  := '';
   //Set the propeties to be used in the hardware id generation
   FMotherBoardInfo :=[Mb_SerialNumber,Mb_Manufacturer,Mb_Product,Mb_Model];
   FOSInfo          :=[Os_BuildNumber,Os_BuildType,Os_Manufacturer,Os_Name,Os_SerialNumber,Os_Version];
   FBIOSInfo        :=[Bs_BIOSVersion,Bs_BuildNumber,Bs_Description,Bs_Manufacturer,Bs_Name,Bs_SerialNumber,Bs_Version];
   FProcessorInfo   :=[];//including the processor info is expensive [Pr_Description,Pr_Manufacturer,Pr_Name,Pr_ProcessorId,Pr_UniqueId];
   if Generate then
    GenerateHardwareId;
end;

destructor THardwareId.Destroy;
begin
  CoUninitialize;
  inherited;
end;

 function  THardwareId.GetWMIstring(const WMIClass, WMIProperty:string): string;
    const
      wbemFlagForwardOnly = $00000020;
    var
      FWbemObjectSet: OLEVariant;
      FWbemObject   : OLEVariant;
      oEnum         : IEnumvariant;
      iValue        : LongWord;
    begin;
      Result:='';
      FWbemObjectSet:= FWMIService.ExecQuery(Format('Select %s from %s',[WMIProperty, WMIClass]),'WQL',wbemFlagForwardOnly);
      oEnum         := IUnknown(FWbemObjectSet._NewEnum) as IEnumVariant;
      if oEnum.Next(1, FWbemObject, iValue) = 0 then

  if not VarIsNull(FWbemObject.Properties_.Item(WMIProperty).Value) then

     Result:=FWbemObject.Properties_.Item(WMIProperty).Value;

    FWbemObject:=Unassigned;
    end;


procedure THardwareId.GenerateMotherBoardInfo (slTagErrores: TStringList);
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Mb              : TMotherBoardInfo;
begin;
    objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
    objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');
    FBuffer := '';
    if FMotherBoardInfo<>[] then //MotherBoard info
    begin
      // FBuffer := FBuffer +  char(10)+ char(13)+ '<h3>Motherboard Info:</h3>' +  char(10)+ char(13);
      objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_BaseBoard','WQL',0);
      oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
      while oEnum.Next(1, oWmiObject, iValue) = 0 do
      begin
        for Mb := Low(TMotherBoardInfo) to High(TMotherBoardInfo) do
        begin
            try
             if Mb in FMotherBoardInfo then
             begin
                SDummy:=VarStrNull(oWmiObject.Properties_.Item(MotherBoardInfoArr[Mb]).Value);
                if FBuffer = '' then
                    FBuffer := SDummy
                else
                  FBuffer := FBuffer+ ',' + SDummy;
             end;

          except
            on Error: Exception do
            begin
              FBuffer := FBuffer+ ',';
              slTagErrores.BeginUpdate;
              slTagErrores.Add(Format('<Error Descripcion = "Motherboard. Error al obtener ''%s''" Detalle = ''%s'' />',
                  [MotherBoardInfoArr[Mb], Error.Message]));
              slTagErrores.EndUpdate;
            end;
          end;
        end;
        oWmiObject:=Unassigned;
      end;
    end;
end;

procedure THardwareId.GenerateOSInfo (slTagErrores: TStringList);
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Os              : TOSInfo;
  sOSData : string;
  OsDataParse: TStringList;
  i : integer;
begin;
  objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');

  FBuffer := '';

  if FOSInfo<>[] then //Windows info
  begin

    objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_OperatingSystem','WQL',0);
    oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
    while oEnum.Next(1, oWmiObject, iValue) = 0 do
    begin
        for Os := Low(TOSInfo) to High(TOSInfo) do
        begin
          try
             if Os in FOSInfo then
             begin
                SDummy:=VarStrNull(oWmiObject.Properties_.Item(OsInfoArr[Os]).Value);
                if FBuffer = '' then
                    FBuffer := SDummy
                else
                  FBuffer := FBuffer+ ',' + SDummy;
             end;
          except
            on Error: Exception do
            begin
              FBuffer := FBuffer+ ',';
              slTagErrores.BeginUpdate;
              slTagErrores.Add(Format('<Error Descripcion = "OS. Error al obtener ''%s''" Detalle = ''%s'' />',
                  [OsInfoArr[Os], Error.Message]));
              slTagErrores.EndUpdate;
            end;
          end;
        end;

        try
          SDummy := VarStrNull(oWmiObject.Properties_.Item(OsInfoArr[Os_Name]).Value);
          if FBuffer = '' then
            FBuffer := SDummy
          else
            FBuffer := FBuffer+ ',' + SDummy;
        except
          on Error: Exception do
          begin
            FBuffer := FBuffer+ ',';
            slTagErrores.BeginUpdate;
            slTagErrores.Add(Format('<Error Descripcion = "OS. Error al obtener ''%s''" Detalle = ''%s'' />',
                [OsInfoArr[Os_Name], Error.Message]));
            slTagErrores.EndUpdate;
          end;
        end;

        if ( sDummy <> '' )  then
        begin
           OsDataParse := TStringList.Create;
           sOSData := sDummy;

           for  i:= 1 to  sOSData.Length do
           begin
             if (sOSData[i]='|') then
             begin
                 OsDataParse.Append( sDummy);
                 sDummy := '';
             end
             else if (i= sOsData.Length) then
             begin
                  sDummy := sDummy + sOSData[i];
                  OsDataParse.Append( sDummy);
                  sDummy := '';
             end
             else
               sDummy := sDummy + sOSData[i];
           end;

           sDummy := OsDataParse[2];
           if FBuffer = '' then
              FBuffer := SDummy
           else
              FBuffer := FBuffer+ ',' + SDummy;
           if FBuffer = '' then
              FBuffer := GetHardiskSerial
           else
             FBuffer := FBuffer+ ',' + GetHardiskSerial;
        end;

      oWmiObject:=Unassigned;
    end;
  end;

end;

procedure THardwareId.GenerateBIOSInfo (slTagErrores: TStringList);
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Bs              : TBIOSInfo;
begin;
  objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');
  FBuffer := '';
  if FBIOSInfo<>[] then//BIOS info
  begin
    objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_BIOS','WQL',0);
    oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
    while oEnum.Next(1, oWmiObject, iValue) = 0 do
    begin
      for Bs := Low(TBIOSInfo) to High(TBIOSInfo) do
      begin
          try
             if Bs in FBIOSInfo then
             begin
                SDummy:=VarStrNull(oWmiObject.Properties_.Item(BiosInfoArr[Bs]).Value);
                if FBuffer = '' then
                  FBuffer := SDummy
                else
                  FBuffer := FBuffer + ',' + SDummy;
             end;
          except
            on Error: Exception do
            begin
              FBuffer := FBuffer+ ',';
              slTagErrores.BeginUpdate;
              slTagErrores.Add(Format('<Error Descripcion = "Bios. Error al obtener ''%s''" Detalle = ''%s'' />',
                  [BiosInfoArr[Bs], Error.Message]));
              slTagErrores.EndUpdate;
            end;
          end;
      end;
      oWmiObject:=Unassigned;
    end;
  end;
end;

procedure THardwareId.GenerateProcessorInfo (slTagErrores: TStringList);
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Pr              : TProcessorInfo;
begin;
  objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');

  FBuffer := '';

    objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_Processor','WQL',0);
    oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
    while oEnum.Next(1, oWmiObject, iValue) = 0 do
    begin
      for Pr := Low(TProcessorInfo) to High(TProcessorInfo) do
      begin
        try
          SDummy:=VarStrNull(oWmiObject.Properties_.Item(ProcessorInfoArr[Pr]).Value);
          if FBuffer = '' then
            FBuffer := SDummy
          else
            FBuffer := FBuffer + ',' + SDummy;
        except
          on Error: Exception do
          begin
            FBuffer := FBuffer+ ',';
            slTagErrores.BeginUpdate;
            slTagErrores.Add(Format('<Error Descripcion = "Processor. Error al obtener ''%s''" Detalle = ''%s'' />',
                [ProcessorInfoArr[Pr], Error.Message]));
            slTagErrores.EndUpdate;
          end;
        end;
      end;
      oWmiObject:=Unassigned;
    end;
end;

procedure THardwareId.GenerateVideoControllerInfo (slTagErrores: TStringList);
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Vc              : TVideoControllerInfo;
begin;
  objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');
  FBuffer := '';
  objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_VideoController','WQL',0);
  oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
  while oEnum.Next(1, oWmiObject, iValue) = 0 do
  begin
    for Vc := Low(TVideoControllerInfo) to High(TVideoControllerInfo) do
    begin
      try
        SDummy:=VarStrNull(oWmiObject.Properties_.Item(VcInfoArr[Vc]).Value);
        if FBuffer = '' then
          FBuffer := SDummy
        else
          FBuffer := FBuffer + ',' + SDummy;
      except
        on Error: Exception do
        begin
          FBuffer := FBuffer+ ',';
          slTagErrores.BeginUpdate;
          slTagErrores.Add(Format('<Error Descripcion = "VideoController. Error al obtener ''%s''" Detalle = ''%s'' />',
              [VcInfoArr[Vc], Error.Message]));
          slTagErrores.EndUpdate;
        end;
      end;
     end;
     oWmiObject:=Unassigned;
  end;
end;

procedure THardwareId.GenerateNetworkInfo (slTagErrores: TStringList);
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Nt              : TNetworkInfo;
  Nd              : TNetworkDevicesInfo;
begin;
  objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');
  FBuffer := '';
  objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_NetworkAdapterConfiguration','WQL',0);
  oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
  while oEnum.Next(1, oWmiObject, iValue) = 0 do
  begin
    for Nt := Low(TNetworkInfo) to High(TNetworkInfo) do
    begin
      try
        SDummy:=VarStrNull(oWmiObject.Properties_.Item(NtInfoArr[Nt]).Value);
        if FBuffer = '' then
          FBuffer := SDummy
        else if SDummy <> VACIO then
          FBuffer := FBuffer + ',' + SDummy;
      except
        on Error: Exception do
        begin
          FBuffer := FBuffer+ ',';
          slTagErrores.BeginUpdate;
          slTagErrores.Add(Format('<Error Descripcion = "Network. Error al obtener ''%s''" Detalle = ''%s'' />',
              [NtInfoArr[Nt], Error.Message]));
          slTagErrores.EndUpdate;
        end;
      end;
     end;
     oWmiObject:=Unassigned;
  end;

  // Agregar direcci�n MAC
  FBuffer := FBuffer + ',' + GetHardwareMACAD;
end;

procedure THardwareId.GenerarListaDispositivos (slTagErrores: TStringList);
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Nt              : TNetworkInfo;
  Nd              : TNetworkDevicesInfo;
begin;
  objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');
  FBuffer := '';
  // Obtener lista de dispositivos
  objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_NetworkAdapter','WQL',0);
  oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
  while oEnum.Next(1, oWmiObject, iValue) = 0 do
  begin
    for Nd := Low(TNetworkDevicesInfo) to High(TNetworkDevicesInfo) do
    begin
      try
        SDummy:=VarStrNull(oWmiObject.Properties_.Item(NdInfoArr[Nd]).Value);
        if FNetworkDevices = '' then
          FNetworkDevices := SDummy
        else
          FNetworkDevices := FNetworkDevices + ',' + SDummy;
      except
        on Error: Exception do
        begin
          FBuffer := FBuffer+ ',';
          slTagErrores.BeginUpdate;
          slTagErrores.Add(Format('<Error Descripcion = "Network. Error al obtener ''%s''" Detalle = ''%s'' />',
              [NdInfoArr[Nd], Error.Message]));
          slTagErrores.EndUpdate;
        end;
      end;
    end;
    oWmiObject:=Unassigned;
  end;
end;

//Main function which collect the system data.
procedure THardwareId.GenerateHardwareId;
var
  objSWbemLocator : OLEVariant;
  objWMIService   : OLEVariant;
  objWbemObjectSet: OLEVariant;
  oWmiObject      : OLEVariant;
  oEnum           : IEnumvariant;
  iValue          : LongWord;
  SDummy          : AnsiString;
  Mb              : TMotherBoardInfo;
  Os              : TOSInfo;
  Bs              : TBIOSInfo;
  Pr              : TProcessorInfo;
  sOSData : string;
  OsDataParse: TStringList;
  i : integer;
begin;
  objSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  objWMIService   := objSWbemLocator.ConnectServer('localhost','root\cimv2', '','');

  if FMotherBoardInfo<>[] then //MotherBoard info
  begin
    FBuffer := FBuffer +  char(10)+ char(13)+ '<h3>Motherboard Info:</h3>' +  char(10)+ char(13);

    objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_BaseBoard','WQL',0);
    oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
    while oEnum.Next(1, oWmiObject, iValue) = 0 do
    begin
      for Mb := Low(TMotherBoardInfo) to High(TMotherBoardInfo) do
       if Mb in FMotherBoardInfo then
       begin
          SDummy:=VarStrNull(oWmiObject.Properties_.Item(MotherBoardInfoArr[Mb]).Value);
          FBuffer:=FBuffer+ char(9) + '<p>' + SDummy  + '</p>' +  char(10)+ char(13);
       end;
       oWmiObject:=Unassigned;
    end;
  end;

  if FOSInfo<>[] then//Windows info
  begin
    FBuffer := FBuffer +  char(10)+ char(13)+  '<h3>OS Info:</h3>' ;
    objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_OperatingSystem','WQL',0);
    oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
    while oEnum.Next(1, oWmiObject, iValue) = 0 do
    begin
      for Os := Low(TOSInfo) to High(TOSInfo) do
       if Os in FOSInfo then
       begin
          SDummy:=VarStrNull(oWmiObject.Properties_.Item(OsInfoArr[Os]).Value);
          FBuffer:=FBuffer+ char(9) + '<p>' + SDummy  + '</p>' +  char(10)+ char(13);
       end;

         SDummy := VarStrNull(oWmiObject.Properties_.Item(OsInfoArr[Os_Name]).Value);
         FBuffer:=FBuffer+ char(9) + '<h4>OS Name/HD :</h4><p>' + SDummy  + '</p>' +  char(10)+ char(13);

         if ( sDummy <> '' )  then
         begin
           OsDataParse := TStringList.Create;
           sOSData := sDummy;

           for  i:= 1 to  sOSData.Length do
           begin
               if (sOSData[i]='|') then
               begin
                   OsDataParse.Append( sDummy);
                   sDummy := '';
               end
               else
               if (i= sOsData.Length) then
               begin
                    sDummy := sDummy + sOSData[i];
                    OsDataParse.Append( sDummy);
                    sDummy := '';
               end
               else
                 sDummy := sDummy + sOSData[i];
           end;


           sDummy := OsDataParse[2];
           FBuffer:=FBuffer+ char(9) + '<h4>HD Partition ID:</h4><p>' + SDummy  + '</p>' +  char(10)+ char(13);

           FBuffer := FBuffer + '<H4>HD Serial</H4> <p>' + GetHardiskSerial + '</p>' ;


         end;


       oWmiObject:=Unassigned;
    end;
  end;

  if FBIOSInfo<>[] then//BIOS info
  begin
    FBuffer := FBuffer +  char(10)+ char(13)+  '<h3>BIOS Info:</h3>' ;
    objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_BIOS','WQL',0);
    oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
    while oEnum.Next(1, oWmiObject, iValue) = 0 do
    begin
      for Bs := Low(TBIOSInfo) to High(TBIOSInfo) do
       if Bs in FBIOSInfo then
       begin
          SDummy:=VarStrNull(oWmiObject.Properties_.Item(BiosInfoArr[Bs]).Value);
          FBuffer:=FBuffer+ char(9) +  '<p>' +   SDummy   + '</p>' +  char(10)+ char(13);
       end;
       oWmiObject:=Unassigned;
    end;
  end;

  if FProcessorInfo<>[] then//CPU info
  begin
     FBuffer := FBuffer +  char(10)+ char(13)+  '<h3>CPU Info:</h3>' ;
    objWbemObjectSet:= objWMIService.ExecQuery('SELECT * FROM Win32_Processor','WQL',0);
    oEnum           := IUnknown(objWbemObjectSet._NewEnum) as IEnumVariant;
    while oEnum.Next(1, oWmiObject, iValue) = 0 do
    begin
      for Pr := Low(TProcessorInfo) to High(TProcessorInfo) do
       if Pr in FProcessorInfo then
       begin
          SDummy:=VarStrNull(oWmiObject.Properties_.Item(ProcessorInfoArr[Pr]).Value);
          FBuffer:=FBuffer+ char(9)+ '<p>'  + SDummy + '</p>' +  char(10)+ char(13);
       end;
       oWmiObject:=Unassigned;
    end;
  end;

  // MAC ADRESS - Codificacion
  // SDummy:=VarStrNull(GetHardwareMACAD);
end;

function THardwareId.GetHardiskSerial: string;
begin
  FSWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  FWMIService   := FSWbemLocator.ConnectServer('localhost', 'root\cimv2', '', '');


  Result := GetWMIstring('Win32_PhysicalMedia','SerialNumber');
  Result := Result +  GetWMIstring('Win32_DiskDrive','SerialNumber');

end;

function THardwareId.GetHardwareIdHex: AnsiString;
begin
    SetLength(Result,Length(FBuffer)*2);
    BinToHex(PAnsiChar(FBuffer),PAnsiChar(Result),Length(FBuffer));
end;

//MAC ADRESS
function THardwareId.GetHardwareMACAD: string;
begin
     Result :=  GetMACAdress ;
end;

// Machine GUID
function THardwareId.GetMachineGUID: string;
const
  KEY_WOW64_32KEY = $0200;
  KEY_WOW64_64KEY = $0100;
var
  Reg: TRegIniFile;
  SubKey: String;
begin
    SubKey := '\SOFTWARE\Microsoft\Cryptography';
    Reg := TRegIniFile.Create(KEY_ALL_ACCESS or KEY_WOW64_64KEY);
    try
        Reg.RootKey := HKEY_LOCAL_MACHINE;
        if Reg.OpenKey(SubKey,False) then
        begin
            Result := Reg.ReadString (SubKey,'MachineGuid','');
        end;
    finally
        Reg.CloseKey;
        Reg.Free;
    end;
end;

end.
