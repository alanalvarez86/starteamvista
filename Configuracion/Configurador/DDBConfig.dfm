object dmDBConfig: TdmDBConfig
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 576
  Top = 352
  Height = 248
  Width = 384
  object Connector: TADOConnection
    LoginPrompt = False
    Left = 40
    Top = 24
  end
  object AdoQuery: TADOQuery
    Connection = Connector
    Parameters = <>
    Left = 112
    Top = 24
  end
  object cdsEmpresas: TServerDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsEmpresasAfterOpen
    Left = 96
    Top = 80
  end
end
