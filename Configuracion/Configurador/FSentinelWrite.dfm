object SentinelWrite: TSentinelWrite
  Left = 668
  Top = 227
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Actualizar Guardia'
  ClientHeight = 281
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 314
    Height = 241
    ActivePage = tabAutorizacionVirtual
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object tabAutorizacion: TTabSheet
      Caption = 'Autorizaci'#243'n'
      object EmpresaLBL: TLabel
        Left = 19
        Top = 23
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = '# Empresa:'
      end
      object Label1: TLabel
        Left = 33
        Top = 43
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = '&Versi'#243'n:'
        FocusControl = Version
      end
      object UsuariosLBL: TLabel
        Left = 27
        Top = 65
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = '&Usuarios:'
        FocusControl = Usuarios
      end
      object EmpleadosLBL: TLabel
        Left = 16
        Top = 87
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = '&Empleados:'
        FocusControl = Empleados
      end
      object ModulosLBL: TLabel
        Left = 20
        Top = 153
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = '&Par'#225'metro:'
        FocusControl = Parametro
      end
      object Clave1LBL: TLabel
        Left = 29
        Top = 175
        Width = 46
        Height = 13
        Caption = 'Clave #&1:'
        FocusControl = Clave1
      end
      object Clave2LBL: TLabel
        Left = 29
        Top = 198
        Width = 46
        Height = 13
        Caption = 'Clave #&2:'
        FocusControl = Clave2
      end
      object SentinelLBL: TLabel
        Left = 21
        Top = 4
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Guardia #:'
      end
      object Sentinel: TZetaTextBox
        Left = 77
        Top = 2
        Width = 65
        Height = 18
        AutoSize = False
        Caption = 'Sentinel'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Empresa: TZetaTextBox
        Left = 77
        Top = 21
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'Empresa'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object SQLEngineLBL: TLabel
        Left = -1
        Top = 131
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Base de Datos:'
      end
      object PlataformaLBL: TLabel
        Left = 17
        Top = 109
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plataforma:'
      end
      object Clave1: TEdit
        Left = 77
        Top = 171
        Width = 149
        Height = 21
        TabOrder = 6
      end
      object Clave2: TEdit
        Left = 77
        Top = 194
        Width = 149
        Height = 21
        TabOrder = 7
      end
      object Version: TComboBox
        Left = 77
        Top = 39
        Width = 83
        Height = 21
        TabOrder = 0
        Text = 'Version'
        Items.Strings = (
          '2.0'
          '1.3')
      end
      object Usuarios: TZetaNumero
        Left = 77
        Top = 61
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 1
        Text = ''
      end
      object Empleados: TZetaNumero
        Left = 77
        Top = 83
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 2
        Text = ''
      end
      object Parametro: TZetaNumero
        Left = 77
        Top = 149
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 5
        Text = ''
      end
      object Plataforma: TComboBox
        Left = 77
        Top = 105
        Width = 150
        Height = 21
        Style = csDropDownList
        TabOrder = 3
      end
      object SQLEngine: TComboBox
        Left = 77
        Top = 127
        Width = 150
        Height = 21
        Style = csDropDownList
        TabOrder = 4
      end
    end
    object tabAutorizacionVirtual: TTabSheet
      Caption = 'Autorizaci'#243'n Guardia Virtual'
      ImageIndex = 2
      object VirtualUsuariosLBL: TLabel
        Left = 27
        Top = 29
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = '&Usuarios:'
        FocusControl = VirtualUsuarios
      end
      object VirtualEmpleadosLBL: TLabel
        Left = 16
        Top = 56
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = '&Empleados:'
        FocusControl = VirtualEmpleados
      end
      object VirtualModulosLBL: TLabel
        Left = 20
        Top = 109
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = '&Par'#225'metro:'
        FocusControl = VirtualParametro
      end
      object VirtualClave1LBL: TLabel
        Left = 29
        Top = 138
        Width = 46
        Height = 13
        Caption = 'Clave #&1:'
        FocusControl = VirtualClave1
      end
      object VirtualClave2LBL: TLabel
        Left = 29
        Top = 165
        Width = 46
        Height = 13
        Caption = 'Clave #&2:'
        FocusControl = VirtualClave2
      end
      object Label14: TLabel
        Left = 79
        Top = 188
        Width = 104
        Height = 13
        Alignment = taCenter
        Caption = 'Servidor de Pruebas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object Label15: TLabel
        Left = 31
        Top = 5
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Servidor:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblNombreServer: TLabel
        Left = 79
        Top = 5
        Width = 3
        Height = 13
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblPlataforma: TLabel
        Left = 17
        Top = 83
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Plataforma:'
      end
      object VirtualClave1: TEdit
        Left = 77
        Top = 134
        Width = 149
        Height = 21
        TabOrder = 3
      end
      object VirtualClave2: TEdit
        Left = 77
        Top = 161
        Width = 149
        Height = 21
        TabOrder = 4
      end
      object VirtualUsuarios: TZetaNumero
        Left = 77
        Top = 25
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 0
        Text = ''
      end
      object VirtualEmpleados: TZetaNumero
        Left = 77
        Top = 52
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 1
        Text = ''
      end
      object VirtualParametro: TZetaNumero
        Left = 77
        Top = 106
        Width = 83
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 2
        Text = ''
      end
      object lstPlataformaVirtual: TComboBox
        Left = 77
        Top = 79
        Width = 150
        Height = 21
        Style = csDropDownList
        TabOrder = 5
      end
    end
  end
  object panelBotones: TPanel
    Left = 0
    Top = 241
    Width = 314
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      314
      40)
    object ReadOnlyLBL: TLabel
      Left = 8
      Top = 8
      Width = 123
      Height = 26
      Alignment = taCenter
      AutoSize = False
      Caption = 'No tiene derechos de administrador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
      WordWrap = True
    end
    object OK: TcxButton
      Left = 139
      Top = 6
      Width = 80
      Height = 26
      Hint = 'Grabar Sentinela'
      Anchors = [akRight, akBottom]
      Caption = '&OK'
      ModalResult = 1
      OptionsImage.ImageIndex = 12
      OptionsImage.Images = FConfiguradorTRESS.ImageButtons
      OptionsImage.Margin = 1
      OptionsImage.Spacing = 16
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
      OnClick = OKClick
    end
    object Cancelar: TcxButton
      Left = 225
      Top = 6
      Width = 80
      Height = 26
      Anchors = [akRight, akBottom]
      Caption = '&Cancelar'
      ModalResult = 2
      OptionsImage.ImageIndex = 14
      OptionsImage.Images = FConfiguradorTRESS.ImageButtons
      OptionsImage.Margin = 1
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
  end
end
