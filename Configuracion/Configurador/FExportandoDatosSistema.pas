unit FExportandoDatosSistema;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer,
  cxEdit, cxGroupBox, cxCheckBox, cxTextEdit, cxLabel, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls,
  FAutoClasses, FAutoServer, FAutoServerLoader, cxShellBrowserDialog, Vcl.ExtDlgs, cxProgressBar,
  Dispatcher3DTProxy, SOAPHTTPClient, Soap.InvokeRegistry, Soap.Rio, IdBaseComponent, IdCoder, IdCoder3to4, IdCoderMIME,
  AbZipper, AbBase, AbBrowse, AbZBrows, dxSkinsCore, TressMorado2013, Vcl.ImgList,
  cxClasses, ZetaClientDataSet;


type
  TExportandoDatosSistema = class(TForm)
    panelBotones: TPanel;
    btAbrirFolder: TcxButton;
    chbArchivo: TcxCheckBox;
    gbAvance: TcxGroupBox;
    lblArchivo: TLabel;
    cxOpenDialog3dt: TOpenTextFileDialog;
    cxOpenDialog: TcxShellBrowserDialog;
    lblAvance: TLabel;
    pbAvance: TcxProgressBar;
    lblInfoAvance: TLabel;
    txtArchivo: TcxTextEdit;
    HTTPRIO: THTTPRIO;
    IdEncoderMIME: TIdEncoderMIME;
    AbZipper: TAbZipper;
    ImageButtons: TcxImageList;
    chbRecolectarGuardar: TcxCheckBox;
    lblRecolectarGenerar: TLabel;
    btSalir: TcxButton;
    lblError: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btEjecutarClick(Sender: TObject);
    procedure btSalirClick(Sender: TObject);
    procedure btAbrirFolderClick(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    FDatos: TStrings;
    FFileName: String;
    FGuardarArchivo: Boolean;
    function ExportData( const sFileName: String ): Boolean;
    procedure AddText( const sText : string ) ;
    procedure LoadSentinelData;
    procedure LoadTerminalesData;
    procedure LoadUserData;
    function BoolToAttBoolean( lValue : boolean ) : String;
    function ReportaFecha( const dValue: TDate ): String;
    function ModuloOK(const eModulo: TModulos): String;
    function PrestamoOK(const eModulo: TModulos): String;
    function LoadCompanyData : string;
    procedure LoadCompanyDataCallBack(const sMsg: String; dProgreso: Double);
    procedure LoadCompanyDataCallBackError(const sMsg: String; Error: Exception);
    procedure Avance (sTexto:String; dProgreso: Double);
    function intentarGuardarArchivo: boolean;
    function Server: Despachador_x0020_3DTSoap;
    function GetURL: String;
    function FileToBase64( sFilePath : string ) : string;
    procedure comprimirArchivo (sFileName: String);
    function crearNombreArchivoZIP: String;
    function GetFileSize( const sFileName : String ): String;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
    property FileName: String read FFileName write FFileName;
    property GuardarArchivo: Boolean read FGuardarArchivo write FGuardarArchivo;
    procedure ExportarDatos ();
  end;

var
  ExportandoDatosSistema: TExportandoDatosSistema;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaServerTools,
     ZetaLicenseClasses,
{$ifdef SENTINELVIRTUAL}
     FSentinelRegistry,
{$endif}
     ZetaWinAPITools, DDBConfig, DBConfigServer, DZetaServerProvider, ZetaLicenseMgr, ZetaCommonLists, FErrorGrabarArchivo3dt,
     ZetaRegistryServer, ShellAPI;

{$R *.dfm}

const
     K_FECHA = 'yyyy-mm-dd';
     K_LLAVE = '3DTLlavedeAccesoHerramienta';
     K_WS3DTURL = 'http://tress30.tress.com.mx';
     K_WS3DTDIR = '3DT/Dispatcher/Dispatcher.asmx';

procedure TExportandoDatosSistema.FormShow(Sender: TObject);
begin
     chbArchivo.Enabled := GuardarArchivo;
     txtArchivo.Text := FileName;
end;

procedure TExportandoDatosSistema.ExportarDatos;
var oCursor: TCursor;
begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
      if ExportData( FileName ) then
        ZetaDialogo.zInformation( 'Los datos han sido exportados',
            'Los datos del sistema han sido exportados con �xito', 0 )
      else
      begin
         lblError.Visible := TRUE;
         ZetaDialogo.zInformation( 'Error',
            'No ha sido posible exportar con �xito los datos del sistema', 0 );
      end;

      FocusControl(btSalir);

    finally
      Screen.Cursor := oCursor;
    end;
end;

procedure TExportandoDatosSistema.btAbrirFolderClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open','c:\windows\explorer.exe', {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ExtractFilePath(FileName)), nil, SW_SHOWNORMAL);
end;

procedure TExportandoDatosSistema.btEjecutarClick(Sender: TObject);
begin
    ExportarDatos;
end;

procedure TExportandoDatosSistema.btSalirClick(Sender: TObject);
begin
    Close;
end;

function TExportandoDatosSistema.ExportData( const sFileName: String ): Boolean;
var
   i: Integer;
begin
     Result := False;
     try
        FDatos := TStringList.Create;
        try
           FDatos.Add('<?xml version="1.0" encoding="ISO-8859-1" ?>');
           FDatos.Add( Format( '<TressDT Fecha="%s">', [ FormatDateTime( K_FECHA, Now )] ) );

           Avance ('Obtenci�n de datos del sistema operativo', 0);
           ZetaWinAPITools.WriteSystemInfo( FDatos, True);

           Avance ('Recolectando informaci�n de Sentinel', 10);
           LoadSentinelData;

           Avance ('Recolectando informaci�n del usuario', 5);
           LoadUserData;

           Avance ('Recolectando informaci�n de las compa��as', 5);
           LoadCompanyData;

           Avance ('Recolectando informaci�n de Terminales', 5);
           LoadTerminalesData;

           Avance ('Recolectando informaci�n para archivo 3DT', 0);

           AddText('</TressDT>');

           with FDatos do
           begin
          {$ifdef DEBUG_XML_3DT}
                SaveToFile( sFileName  + '.xml' );
          {$endif}
                for i := 0 to ( Count - 1 ) do
                begin
                     Strings[ i ] := ZetaServerTools.Encrypt( Strings[ i ] );
                end;

                chbRecolectarGuardar.Checked := TRUE;

                if GuardarArchivo then
                begin
                    lblError.Top := 145;
                    while intentarGuardarArchivo do
                      FileName := FErrorGrabarArchivo3dt.EditarArchivo(FileName);

                    chbArchivo.Checked := TRUE;
                    btAbrirFolder.Enabled := TRUE;
                    Avance ('Se guard� archivo 3DT: ' + FileName, 10);
                end
                else
                begin
                    Avance ('Se gener� informaci�n para archivo 3DT', 10);
                end;
           end;

           // Enviar archivo 3DT por medio de WebService
           Avance ('Se gener� informaci�n para archivo 3DT', 10);
           Result := TRUE;

        finally
               FreeAndNil( FDatos );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TExportandoDatosSistema.Avance (sTexto:String; dProgreso: Double);
begin
     lblInfoAvance.Caption := sTexto;
     pbAvance.Position := pbAvance.Position + dProgreso;
     Application.ProcessMessages;
end;

function TExportandoDatosSistema.intentarGuardarArchivo: boolean;
begin
    try
        FDatos.SaveToFile( FileName );
        Result := FALSE;
    except
      Result := TRUE;
    end;
end;

procedure TExportandoDatosSistema.AddText( const sText : string ) ;
begin
     FDatos.Add( sText );
end;

procedure TExportandoDatosSistema.LoadSentinelData;
const
     K_MODULO = '%25.25s         %s       %s';
     K_MODULO_XML = '<Modulo Numero="%d" Nombre="%s" Autorizado="%s" Prestado="%s"/>';
var
   eModulo: TModulos;
begin
     with FDatos do
     begin
          {$ifdef SENTINELVIRTUAL}
          FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
          {$endif}

          TAutoServerLoader.LoadSentinelInfo( FAutoServer );

          with FAutoServer do
          begin
               Add( '<Sentinel ' );
               Add( 'Empresa="' + IntToStr( Empresa ) +'" ');
               Add( 'Empleados="' + IntToStr( Empleados ) +'" ');
               Add( 'Licencias="' + IntToStr( Usuarios ) +'" ');
               {***(@am): Este cambio es temporal. Se detecto un error en los dcus de sentinel. El NumeroSerie no era limpiado al entrar en DEMO.
                       Un cambio en dcus implica un riesgo mayor. Asi que se arreglara el problema para la VS 2017. En VS 2016 la siguiente condicional
                       permitira realizar esa operacion.***}
               if Empresa = 0 then
                  Add( 'Numero="' + '0' +'" ')
               else
                   Add( 'Numero="' + IntToStr( NumeroSerie ) +'" ');
               {***}
               Add( 'LimiteConfigBD="' + IntToStr( ZetaLicenseClasses.GetIntervaloLimiteBDConfig(LimiteConfigBD) ) +'" ');
               Add( 'IniciaValidacion="' + IntToStr( IniciaValidacion ) +'" ');
               Add( 'Version="' + Version  +'" ' );
               Add( 'EsKitDistribuidor="' + BoolToAttBoolean( EsKit )+'" ' );
               Add( 'Plataforma="' + PlataformaAutoStr +'" ');
               Add( 'BaseDeDatos="' + GetSQLEngineStr +'" ');
               Add( 'Vencimiento="' + ReportaFecha( FAutoServer.Vencimiento ) +'" ');
               Add( 'CaducidadPrestamos="' + ReportaFecha( FAutoServer.Caducidad )  +'" ');
               Add( '>' );
               for eModulo := Low( TModulos ) to High( TModulos ) do
               begin
                    Add( Format( K_MODULO_XML, [ Ord( eModulo ),  FAutoServer.GetModuloStr( eModulo ), ModuloOk( eModulo ), PrestamoOk( eModulo ) ] ) );
               end;
               Add( '</Sentinel>');
          end;
         {$ifdef SENTINELVIRTUAL}
         FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
         {$endif}
     end;
end;

procedure TExportandoDatosSistema.LoadTerminalesData;
var
   sXmlterminales : String;
begin
     sXmlterminales := VACIO;
     with dmDBConfig do
     begin
          sXmlterminales := GetTerminales(false);
     end;

     with FDatos do
     begin
          {$ifdef SENTINELVIRTUAL}
          FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
          {$endif}

          TAutoServerLoader.LoadSentinelInfo( FAutoServer );

          with FAutoServer do
          begin
                  if sXmlterminales <> VACIO then
                  begin
                       Add( sXmlterminales );
                  end;
          end;
         {$ifdef SENTINELVIRTUAL}
         FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
         {$endif}
     end;
end;

procedure TExportandoDatosSistema.LoadUserData;
begin
     with FDatos do
     begin
        Add( '<UserData UsuariosTotales="' + Trim( Format( '%9.0n', [ dmDBConfig.UsuariosContar / 1 ] ) ) +'" />');
     end;
end;

function TExportandoDatosSistema.BoolToAttBoolean(lValue: boolean): String;
begin
     if ( lValue ) then
        Result := 'true'
     else
         Result := 'false';
end;

function TExportandoDatosSistema.ReportaFecha( const dValue: TDate ): String;
begin
     if ( dValue > NullDateTime ) then
        Result := FormatDateTime( K_FECHA, dValue )
     else
         Result := '1899-12-30';
end;

function TExportandoDatosSistema.ModuloOK( const eModulo: TModulos ): String;
begin
     Result := BoolToAttBoolean( FAutoClasses.BitSet( FAutoServer.Modulos, Ord( eModulo ) ) );
end;

function TExportandoDatosSistema.PrestamoOK( const eModulo: TModulos ): String;
begin
     Result := BoolToAttBoolean( FAutoClasses.BitSet( FAutoServer.Prestamos, Ord( eModulo ) ) );
end;

procedure TExportandoDatosSistema.LoadCompanyDataCallBack( const sMsg: String; dProgreso: Double);
begin
    Avance (sMsg, dProgreso);
end;

procedure TExportandoDatosSistema.LoadCompanyDataCallBackError( const sMsg: String; Error: Exception );
begin
    AddText( Format( '<Error><Mensaje><![CDATA[%s]]></Mensaje><Excepcion><![CDATA[%s]]></Excepcion></Error>', [ sMsg, Error.Message ] ) );
end;

function TExportandoDatosSistema.LoadCompanyData : string;
const
     K_COMPANY = '%-10.10s %-50.50s %9.0n %9.0n %9.0n %-6.6s %-50.50s %-11.11s %-11.11s %-11.11s %-40.40s %4.4d %4.4d %6.6d %-11.11s %2.2s %6.6d %-11.11s %6.6d %-11.11s';
     K_TOTALES = '%-10.10s %-50.50s %9.0n %9.0n %9.0n';
     K_COMPANY_ATTS = 'CM_3_PROD="%s" '+
                      'CM_CTRL_RL="%s" CM_CODIGO="%s" CM_NOMBRE="%s"  CM_ACTIVOS="%d"  CM_INACTIV="%d" CM_TOTAL="%d" CM_NIVEL0="%s"  CM_DATOS="%s" DB_SIZE="%f" MS_SQL_VER="%s" MS_SQL_LEV="%s" MS_SQL_EDT="%s" CM_ULT_ALT="%s" CM_ULT_BAJ="%s" '+
                      'CM_ULT_TAR="%s" PE_YEAR="%d"  PE_TIPO="%d" PE_NUMERO="%d" PE_FEC_INI="%s" CM_CONNECT="%s"  PC_SI_QTY="%d" PC_SI_FEC="%s" PC_AN_QTY="%d" PC_AN_FEC="%s" '+
                      'PC_NOMLAVG="%f" PC_NOM_AVG="%f" ';
     K_NUEVOS_ATTS = 'PC_FOA_QTY="%d"  PC_FOA_FEC="%s" PC_FON_QTY="%d"  PC_FON_FEC="%s" PC_FOC_QTY="%d"  PC_FOC_FEC="%s" PC_LB_QTY="%d" PC_LB_FEC="%s" PC_EM_QTY="%d" '+
                     'PC_EM_FEC="%s" RE_CU_QTY="%d" RE_CF_QTY="%d" RE_CF_FEC="%s" RE_RG_QTY="%d" RE_RG_FEC="%s" RE_SM_QTY="%d" RE_SM_FEC="%s" RE_PC_QTY="%d" RE_PC_FEC="%s" RE_EV_QTY="%d" RE_EV_FEC="%s" ';
     K_OTROS_ATTS = '<Otros K_MDAT_QTY="%d" K_MDAT_FEC="%s" K_KIOS_QTY="%d" K_KIOS_FEC="%s" K_WF_QTY="%d" K_WF_FEC="%s" K_SEL_BD="%d" K_SEL_QTY="%d" K_SEL_FEC="%s" K_VIS_BD="%d" K_VIS_QTY="%d" K_VIS_FEC="%s" />';
     K_TOTALES_ATTS = '<Totales TotalActivos="%d" ActivosProduccion="%d" ActivosPruebas="%d" TotalBajas="%d" BajasProduccion="%d" BajasPruebas="%d"  CantAltasBD="%d"  UltimaAltaBD="%s"/>';
var
   oZetaProvider: TdmZetaServerProvider;
   FManager: TLicenseMgr;
   rActivos, rInactivos : Integer;
   rActivosTest, rInactivosTest : Integer;
   sBaseDeDatos, sDatabase, sNivel0: String;
   FSeleccionDatos : TDatosSeleccion;
   FVisitantesDatos : TDatosVisitantes;
   FModulosComparteDatos : TDatosModulosComparte;
   FDbConfigManager : TDbConfigManager;
   oConfigServer : TDBConfigServer;

   function EsTressProduccion( sCM_CTRL_RL : string ) : boolean;
   begin
        Result :=  StrVacio(sCM_CTRL_RL) or  ( sCM_CTRL_RL = '3DATOS' );
   end;

   function HTMLEncode(const Data: string): string;
   var
    i: Integer;
   begin

    result := '';
    for i := 1 to length(Data) do
      case Data[i] of
        '<': result := result + '&lt;';
        '>': result := result + '&gt;';
        '&': result := result + '&amp;';
        '"': result := result + '&quot;';
      else
        result := result + Data[i];
      end;
   end;

begin
     oZetaProvider := TdmZetaServerProvider.Create( Self );
     try
        FManager := TLicenseMgr.Create( oZetaProvider );
        try
           with FManager do
           begin
                AutoServer := Self.AutoServer;
           end;

           oConfigServer := TDBConfigServer.Create(oZetaProvider, Self.AutoServer);

           with dmDBConfig do
           begin
                FDbConfigManager := oConfigServer.DBConfigMgr;
                FModulosComparteDatos :=  FManager.RegistrosModulosComparte;
                EmpresasConectar(True, tcRecluta );
                FSeleccionDatos := SeleccionLeer(FManager, oZetaProvider, LoadCompanyDataCallBack, LoadCompanyDataCallBackError );

                EmpresasConectar(True, tcVisitas );
                FVisitantesDatos := VisitantesLeer(FManager, oZetaProvider, LoadCompanyDataCallBack, LoadCompanyDataCallBackError );

                EmpresasConectar( {$ifdef VALIDA_EMPLEADOS}False{$else}True{$endif} );
                LicenciasLeer( FManager, oZetaProvider, LoadCompanyDataCallBack, LoadCompanyDataCallBackError );
                rActivos := 0;
                rInactivos := 0;
                rActivosTest := 0;
                rInactivosTest := 0;
                with FDatos do
                begin
                     with cdsEmpresas do
                     begin
                          IndexFieldNames := 'CM_DATOS';
                          First;
                          sDatabase := VACIO;

                          AddText( Format( '<Companies Cantidad="%d">', [cdsEmpresas.RecordCount] ));
                          while not Eof do
                          begin
                               AddText( '<Company ' );
                               sBaseDeDatos := UpperCase( Trim( FieldByName( 'CM_DATOS' ).AsString ) );
                               sNivel0 := FieldByName( 'CM_NIVEL0' ).AsString;
                               AddText( Format( K_COMPANY_ATTS, [
                                                         BoolToAttBoolean(  EsTressProduccion( FieldByName( 'CM_CTRL_RL' ).AsString ) ),
                                                         FieldByName( 'CM_CTRL_RL' ).AsString,
                                                         HTMLEncode( FieldByName( 'CM_CODIGO' ).AsString ),
                                                         HTMLEncode( FieldByName( 'CM_NOMBRE' ).AsString ),
                                                         FieldByName( 'CM_ACTIVOS' ).AsInteger,
                                                         FieldByName( 'CM_INACTIV' ).AsInteger,
                                                         FieldByName( 'CM_TOTAL' ).AsInteger,
                                                         HTMLEncode( sNivel0 ),
                                                         HTMLEncode( sBaseDeDatos ),
                                                         FieldByName( 'DB_SIZE' ).AsFloat,
                                                         HTMLEncode(FieldByName( 'MS_SQL_VER' ).AsString),
                                                         HTMLEncode(FieldByName( 'MS_SQL_LEV' ).AsString),
                                                         HTMLEncode(FieldByName( 'MS_SQL_EDT' ).AsString),
                                                         ReportaFecha( FieldByName( 'CM_ULT_ALT' ).AsDateTime ),
                                                         ReportaFecha( FieldByName( 'CM_ULT_BAJ' ).AsDateTime ),
                                                         ReportaFecha( FieldByName( 'CM_ULT_TAR' ).AsDateTime ),
                                                         FieldByName( 'PE_YEAR' ).AsInteger,
                                                         FieldByName( 'PE_TIPO' ).AsInteger,
                                                         FieldByName( 'PE_NUMERO' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PE_FEC_INI' ).AsDateTime ),
                                                         HTMLEncode( FieldByName( 'CM_CONNECT' ).AsString ),
                                                         FieldByName( 'PC_SI_QTY' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PC_SI_FEC' ).AsDateTime ),
                                                         FieldByName( 'PC_AN_QTY' ).AsInteger,
                                                         ReportaFecha( FieldByName( 'PC_AN_FEC' ).AsDateTime ),
                                                         FieldByName( 'PC_NOMLAVG' ).AsFloat,
                                                         FieldByName( 'PC_NOM_AVG' ).AsFloat
                                                         ] ) );
                               AddText( Format( K_NUEVOS_ATTS,
                                    [
                                       FieldByName( 'PC_FOA_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_FOA_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_FON_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_FON_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_FOC_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_FOC_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_LB_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_LB_FEC' ).AsDateTime ),
                                       FieldByName( 'PC_EM_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'PC_EM_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_CU_QTY' ).AsInteger,
                                       FieldByName( 'RE_CF_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_CF_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_RG_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_RG_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_SM_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_SM_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_PC_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_PC_FEC' ).AsDateTime ),
                                       FieldByName( 'RE_EV_QTY' ).AsInteger,
                                       ReportaFecha( FieldByName( 'RE_EV_FEC' ).AsDateTime )
                                    ] ) );

                               AddText('>');
                               if ( sDatabase <> sBaseDeDatos )  then
                               begin
                                    if EsTressProduccion( FieldByName( 'CM_CTRL_RL' ).AsString )  then
                                    begin
                                       rActivos := rActivos + FieldByName( 'CM_ACTIVOS' ).AsInteger;
                                       rInactivos := rInactivos + FieldByName( 'CM_INACTIV' ).AsInteger;
                                    end
                                    else
                                    if ( FieldByName( 'CM_CTRL_RL' ).AsString = '3PRUEBA' ) then
                                    begin
                                       rActivosTest := rActivosTest + FieldByName( 'CM_ACTIVOS' ).AsInteger;
                                       rInactivosTest := rInactivosTest + FieldByName( 'CM_INACTIV' ).AsInteger;
                                    end;
                               end;

                               sDatabase := sBaseDeDatos;
                               AddText(FieldByName( 'COMPANYXMLDATA' ).AsString);
                               AddText( FieldByName( 'XMLINFOUSOTRESS' ).AsString);
                               AddText('</Company>');
                               Next;
                          end;
                     end;
                     AddText( '</Companies>' );
                     with  FModulosComparteDatos, FSeleccionDatos, FVisitantesDatos do
                     begin
                         AddText( Format( K_OTROS_ATTS, [MisDatosCuantos, ReportaFecha(MisDatosUltimo),
                                  KioscoCuantos, ReportaFecha(KioscoUltimo),
                                  WorkflowCuantos, ReportaFecha(WorkflowUltimo),
                                  FSeleccionDatos.BDCuantos,  RequisicionCuantos, ReportaFecha(RequisicionUltimo),
                                  FVisitantesDatos.BDCuantos, LibroCuantos, ReportaFecha( LibroUltimo )   ] )  );
                     end;
                     AddText( Format( K_TOTALES_ATTS, [rActivos + rActivosTest, rActivos, rActivosTest, rInactivos+rInactivosTest,rInactivos, rInactivosTest, FDbConfigManager.GetDbConfigRec.Cantidad,  ReportaFecha( FDbConfigManager.GetDbConfigRec.Ultima )  ] ) );
                end;
           end;
        finally
               FreeAndNil( FManager );
        end;
     finally
            FreeAndNil( oZetaProvider );
     end;
end;

function TExportandoDatosSistema.Server: Despachador_x0020_3DTSoap;
begin
  Result := GetDespachador_x0020_3DTSoap (FALSE, GetURL);
end;

function TExportandoDatosSistema.GetURL: String;
var
   oRegistry : TZetaRegistryServer;
begin
    oRegistry := TZetaRegistryServer.Create();
    try
        if not (StrLleno(oRegistry.WS3DTURL)) then
          oRegistry.WS3DTURL := K_WS3DTURL;
        Result :=  VerificaDir (oRegistry.WS3DTURL, '/') + K_WS3DTDIR;
    finally
      FreeAndNil (oRegistry);
    end;
end;

{
function TExportandoDatosSistema.EnviarArchivo3DT: Boolean;
var
   oDespachador_x0020_3DTSoap: Despachador_x0020_3DTSoap;
   sXML3DT: String;
   sRespuesta, sNombreZIP: String;
begin
      oDespachador_x0020_3DTSoap := Server;

      Avance ('Confirmar conexi�n con web service', 2);
      try
        sRespuesta := oDespachador_x0020_3DTSoap.Echo;
      except
        on Error: Exception do
        begin
              raise Exception.Create('No es posible establecer conexi�n con el web service');
        end;
      end;

      Avance ('Respuesta: ' + sRespuesta, 2);

      sXML3DT :=
            '<PROCESS>' + CR_LF +
               '<LLAVE>' + Encrypt(K_LLAVE) + '</LLAVE>' + CR_LF +
               '<SISTEMA>' + IntToStr (FAutoServer.Empresa) + '</SISTEMA>' + CR_LF +
               '<ARCHIVO>' + CR_LF +
                  '<ARCHIVONAME>' + ExtractFileName (FileName) + '</ARCHIVONAME>' + CR_LF +
                  '<ARCHIVOSIZE>' +  GetFileSize(FileName) +'</ARCHIVOSIZE>' + CR_LF +
               '</ARCHIVO>' + CR_LF +
            '</PROCESS>';

      Avance('Comprimir archivo 3DT', 2);
      sNombreZIP := VerificaDir(ExtractFilePath(FileName)) + crearNombreArchivoZIP;
      comprimirArchivo (sNombreZIP);

      Avance ('Enviando archivo 3DT', 2);
      sRespuesta := oDespachador_x0020_3DTSoap.UploadBase64(FileToBase64 (sNombreZIP), sXML3DT);

      if (Pos ('FINAL', UpperCase(sRespuesta)) > 0) then
      begin
        Avance (sRespuesta, 2);

        // Eliminar archivo ZIP
        AbZipper.Free;
        DeleteFile(sNombreZIP);

        Result := TRUE;
      end
      else
      begin
        Avance ('Error al enviar archivo', 0);
        Result := FALSE;
      end;
end; }

function TExportandoDatosSistema.FileToBase64( sFilePath : string ) : string;
var
  SourceStr: TFileStream;
begin
    SourceStr := TFileStream.Create(sFilePath, fmOpenRead);
    try
       IdEncoderMIME := TIdEncoderMIME.Create(nil);
       try
           Result := IdEncoderMIME.Encode(SourceStr);
       finally
          IdEncoderMIME.Free;
       end;
    finally
      SourceStr.Free;
    end;
end;

procedure TExportandoDatosSistema.comprimirArchivo (sFileName: String);
var stDatos: TStringStream;
begin
    stDatos := TStringStream.Create;
    try
        stDatos.WriteString(FDatos.Text);
        AbZipper.FileName := sFileName;
        AbZipper.AddFromStream(FileName, stDatos);
        AbZipper.CloseArchive;
    finally
        FreeAndNil (stDatos);
    end;
end;

function TExportandoDatosSistema.crearNombreArchivoZIP: String;
begin
    Result := IntToStr (FAutoServer.Empresa) + '_' + IntToStr( FAutoServer.NumeroSerie ) + '_' + FormatDateTime( 'yyyymmddhhnn', now ) + '.zip';
end;

function TExportandoDatosSistema.GetFileSize( const sFileName : String ): String;
var
   Archivo: TSearchRec;
begin
     if ( StrLleno( SFileName ) ) and ( FindFirst( sFileName, faAnyFile, Archivo) = 0 ) then
     begin
          Result := IntToStr (Archivo.Size);
          FindClose( Archivo );
     end
     else
         Result := '0';
end;

end.
