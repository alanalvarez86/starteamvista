object ConfigurarWorkflow: TConfigurarWorkflow
  Left = 618
  Top = 322
  BorderStyle = bsDialog
  Caption = 'Preparar Workflow'
  ClientHeight = 217
  ClientWidth = 416
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  DesignSize = (
    416
    217)
  PixelsPerInch = 96
  TextHeight = 13
  object mensajeDesbloquea: TRichEdit
    Left = 24
    Top = 24
    Width = 369
    Height = 113
    Color = clWhite
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowFrame
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    Lines.Strings = (
      'Este proceso prepara la base de datos de Comparte para '
      'obtener Reportes de WF.'
      ''
      'Presione el bot'#243'n Preparar para iniciar.')
    ParentFont = False
    TabOrder = 0
  end
  object btnPreparar: TcxButton
    Left = 194
    Top = 167
    Width = 105
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Preparar'
    ModalResult = 1
    OptionsImage.ImageIndex = 12
    OptionsImage.Images = FConfiguradorTRESS.ImageButtons
    OptionsImage.Margin = 1
    OptionsImage.Spacing = 16
    TabOrder = 1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    OnClick = btnPrepararClick
  end
  object btbCancelar: TcxButton
    Left = 305
    Top = 167
    Width = 88
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Cancelar'
    ModalResult = 2
    OptionsImage.ImageIndex = 14
    OptionsImage.Images = FConfiguradorTRESS.ImageButtons
    OptionsImage.Margin = 1
    TabOrder = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    OnClick = btbCancelarClick
  end
end
