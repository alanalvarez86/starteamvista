object ActualizaComparte: TActualizaComparte
  Left = 685
  Top = 283
  BorderStyle = bsDialog
  Caption = 'Actualizar COMPARTE'
  ClientHeight = 261
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxWizardControl: TdxWizardControl
    Left = 0
    Top = 0
    Width = 431
    Height = 261
    Hint = ''
    AutoSize = True
    Buttons.Back.Caption = 'Atr'#225's'
    Buttons.Cancel.Caption = 'Cancelar'
    Buttons.Cancel.Visible = False
    Buttons.CustomButtons.Buttons = <>
    Buttons.Finish.Caption = 'Terminar'
    Buttons.Help.Caption = 'Ayuda'
    Buttons.Help.Visible = False
    Buttons.Next.Caption = 'Siguiente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    OptionsAnimate.TransitionEffect = wcteSlide
    ParentFont = False
    OnButtonClick = dxWizardControlButtonClick
    OnPageChanged = dxWizardControlPageChanged
    OnPageChanging = dxWizardControlPageChanging
    object dxWizardControlPageCaptura: TdxWizardControlPage
      Hint = ''
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      Header.Description = 
        'Selecciona la versi'#243'n a actualizar de Base de Datos y si requier' +
        'e sentencias especiales.'
      Header.Title = 'Actualizar Base de Datos COMPARTE'
      ParentFont = False
      object cxLabelVersion: TcxLabel
        Left = 16
        Top = 3
        Hint = ''
        Caption = 'Versi'#243'n actual:'
        Transparent = True
      end
      object lblVersion: TcxLabel
        Left = 97
        Top = 3
        Hint = ''
        Caption = '0'
        Transparent = True
      end
      object cxLabelActualizar: TcxLabel
        Left = 27
        Top = 28
        Hint = ''
        Caption = 'Actualizar a:'
        Transparent = True
      end
      object cbVersion: TcxComboBox
        Left = 97
        Top = 26
        Hint = ''
        Properties.DropDownListStyle = lsFixedList
        TabOrder = 3
        Width = 56
      end
      object chkDiccionario: TcxCheckBox
        Left = 97
        Top = 74
        Hint = ''
        Caption = 'Actualizar Diccionario'
        State = cbsChecked
        TabOrder = 5
        Transparent = True
        Visible = False
        Width = 192
      end
      object chkEspeciales: TcxCheckBox
        Left = 97
        Top = 53
        Hint = ''
        Caption = 'Aplicar sentencias especiales'
        State = cbsChecked
        TabOrder = 4
        Transparent = True
        Width = 192
      end
    end
    object dxWizardControlPageAplicar: TdxWizardControlPage
      Hint = ''
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      Header.Description = 
        'Se aplicar'#225'n las sentencias para actualizar la versi'#243'n de COMPAR' +
        'TE, presione Aplicar para iniciar el proceso.'
      Header.Title = 'Aplicando el proceso'
      ParentFont = False
      DesignSize = (
        409
        125)
      object cxProgressBar1: TcxProgressBar
        Left = 16
        Top = 103
        Hint = ''
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        Width = 368
      end
    end
    object dxWizardControlPageResumen: TdxWizardControlPage
      Hint = ''
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = []
      Header.Description = 
        'Se aplicaron las sentencias para actualizar la versi'#243'n de COMPAR' +
        'TE.'
      Header.Title = 'Resultado de actualizar COMPARTE'
      ParentFont = False
      object lblResumen: TcxLabel
        Left = 16
        Top = 0
        Hint = ''
        AutoSize = False
        Caption = 'La actualizaci'#243'n termin'#243
        Transparent = True
        Height = 47
        Width = 390
      end
      object cxLabel5: TcxLabel
        Left = 16
        Top = 53
        Hint = ''
        Caption = 'Sentencias correctas:'
        Transparent = True
      end
      object cxLabel6: TcxLabel
        Left = 53
        Top = 76
        Hint = ''
        Caption = 'Advertencias:'
        Transparent = True
      end
      object cxLabel7: TcxLabel
        Left = 81
        Top = 99
        Hint = ''
        Caption = 'Errores:'
        Transparent = True
      end
      object lblExitos: TcxLabel
        Left = 130
        Top = 53
        Hint = ''
        Caption = '0'
        Transparent = True
      end
      object lblAdvertencias: TcxLabel
        Left = 130
        Top = 76
        Hint = ''
        Caption = '0'
        Transparent = True
      end
      object lblErrores: TcxLabel
        Left = 130
        Top = 99
        Hint = ''
        Caption = '0'
        Transparent = True
      end
      object btnBitacora: TcxButton
        Left = 331
        Top = 97
        Width = 75
        Height = 25
        Caption = 'Ver bit'#225'cora'
        TabOrder = 7
        OnClick = btnBitacoraClick
      end
    end
  end
end
