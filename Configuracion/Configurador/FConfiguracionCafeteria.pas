unit FConfiguracionCafeteria;

interface

uses
  ZBaseDlgModal_DevEx, Vcl.Forms, Vcl.StdCtrls, Vcl.Buttons, System.Classes, Vcl.Controls, Vcl.ExtCtrls, ZetaHora, Vcl.Mask, ZetaNumero,
  ZBaseDlgModal, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, Vcl.ImgList, cxButtons, cxControls, cxContainer,
  cxEdit, cxTextEdit, Vcl.Dialogs, cxClasses, cxShellBrowserDialog, Vcl.ExtDlgs;

type
  TConfiguracionCafeteria = class(TZetaDlgModal_DevEx)
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edtPuerto: TZetaNumero;
    edtHora: TZetaHora;
    Label7: TLabel;
    Label1: TLabel;
    edCafeteraServicio: TcxTextEdit;
    edNombreInstancia: TcxTextEdit;
    Label2: TLabel;
    cxBuscar: TcxButton;
    cxOpenDialog: TOpenTextFileDialog;
    procedure OKClick(Sender: TObject);
    procedure cxBuscarClick(Sender: TObject);
    procedure edNombreInstanciaKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function InstalarServicio : Boolean;
    procedure RegistrarServicio (Path: String; Port: Integer; Schedule: String; ServiceName : string);
    procedure HabilitarControles (lEnabled : Boolean);
  public
    { Public declarations }
    class function Execute: Boolean;
  end;

const
     // Constantes de CafeteraServicio, Tress Caseta y Tress Cafeteria
     SERVICE_BASE_NAME   = 'CafeteraServicio';
     SERVICE_DISPLAYNAME = 'Servidor de Cafeter�a/Caseta';
     SERVICE_DESCRIPTION = 'Provee servicios de validaci�n de reglas para los clientes Tress Cafeter�a y Tress Caseta';
     K_CAFETERA_PUERTO    = 33100;            // Puerto TCP predeterminado del servidor CafeteraServicio
     K_CAFETERA_HORA_REINICIO: TDateTime = 0; // 00:00 Hora predeterminada de reinicio de reglas del servidor CafeteraServicio
var
  ConfiguracionCafeteria: TConfiguracionCafeteria;

implementation

uses
  SysUtils, ZetaDialogo, ZetaCommonTools,ZetaWinAPITools, ZetaRegistryServer, Windows, Registry;
{$R *.dfm}

{ TForm1 }

procedure TConfiguracionCafeteria.cxBuscarClick(Sender: TObject);
begin
     inherited;
     if cxOpenDialog.Execute then
     begin
          edCafeteraServicio.Text := cxOpenDialog.FileName;
     end;
end;

procedure TConfiguracionCafeteria.edNombreInstanciaKeyPress(Sender: TObject;var Key: Char);
begin
     inherited;
     if Key = ' ' then
        Key := #0;
end;

class function TConfiguracionCafeteria.Execute: Boolean;
begin
end;

procedure TConfiguracionCafeteria.FormCreate(Sender: TObject);
begin
     inherited;
     edtPuerto.Valor := K_CAFETERA_PUERTO;
end;

procedure TConfiguracionCafeteria.OKClick(Sender: TObject);
var
    oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     ModalResult := mrNone;
     if FileExists(edCafeteraServicio.Text) then
     begin
          if not((edtPuerto.ValorEntero > 0) and (edtPuerto.ValorEntero < 65536)) then
          begin
               zError(Caption, 'El Puerto TCP no est� en el rango permitido.', 0);
               edtPuerto.SetFocus;
          end
          else
          begin
               HabilitarControles(False);
               try
                  Screen.Cursor := crHourGlass;
                  if InstalarServicio then
                     ModalResult := mrOk
                  else
                  begin
                       if StrLleno(edNombreInstancia.Text) then
                          ZetaDialogo.ZError(Self.Caption, 'Instancia [' + edNombreInstancia.Text + '] de CafeteraServicio ya se encuentra registrada.', 0 )
                       else
                           ZetaDialogo.ZError(Self.Caption, 'Instancia [' + SERVICE_BASE_NAME + '] de CafeteraServicio ya se encuentra registrada.', 0 );

                       edNombreInstancia.SetFocus;
                       HabilitarControles(True);
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end
     else
     begin
          zError(Caption, 'La ruta donde se encuentra el servicio no es valida.', 0);
          cxBuscar.SetFocus;
     end;
end;

procedure TConfiguracionCafeteria.HabilitarControles(lEnabled: Boolean);
begin
     OK_DevEx.Enabled := lEnabled;
     Cancelar_DevEx.Enabled := lEnabled;
end;

function TConfiguracionCafeteria.InstalarServicio : Boolean;
const
     K_INSTANCIA =' /INSTANCIA ';
var
   Name,DisplayName,ServicePath,Port,Schedule:String;
begin
     if StrLleno(edNombreInstancia.Text) then
          Name := SERVICE_BASE_NAME + '_' + edNombreInstancia.Text
     else
          Name := SERVICE_BASE_NAME;
     DisplayName := Format('%s (%s)', [SERVICE_DISPLAYNAME, Name]);
     ServicePath :=  '"' + edCafeteraServicio.Text + '"' + K_INSTANCIA + edNombreInstancia.Text;

     Result := False;
     if (Not ZetaWinAPITools.FindService(Name)) then
     begin
          RegistrarServicio(edCafeteraServicio.Text, StrToInt(edtPuerto.Text), edtHora.Text, Name);

          Result := (ZetaWinAPITools.AddService(Name, DisplayName, ServicePath));
     end;
end;


procedure TConfiguracionCafeteria.RegistrarServicio(Path: String; Port: Integer; Schedule: String; ServiceName: string);
const
     P_PUERTO = 'PUERTO';
     P_HORA   = 'HORA';
var
  Reg: TRegistry;
  sParams, sValor: string;
  Hora: TDateTime;
begin
     if AnsiPos('32', ZetaWinAPITools.GetInfoFileDescription(Path)) > 0 then
        Reg := TRegistry.Create(KEY_READ or KEY_WRITE) //Escribe en Nodo de 32Bits
     else
         Reg := TRegistry.Create (KEY_ALL_ACCESS or KEY_WOW64_64KEY); //Escribe en nodo de 64Bits

     try
        Reg.RootKey := HKEY_LOCAL_MACHINE;
        //Agrega la descripcion para el servicio
        if Reg.OpenKey('\SYSTEM\CurrentControlSet\Services\' + ServiceName, True) then begin
           Reg.WriteInteger('DelayedAutostart', 1);
           Reg.WriteString('Description', SERVICE_DESCRIPTION);
           Reg.CloseKey;
        end;

        //Agregar el puerto del servicio al registry
        if Reg.OpenKey('\Software\Grupo Tress\' + ServiceName, True) then begin
           Reg.WriteString(P_PUERTO, IntToStr(Port));

        // Hora de reinicio de reglas
        Hora := EncodeTime(StrToIntDef(Copy(Schedule, 1, 2), 0), StrToIntDef(Copy(Schedule, 3, 2), 0), 0, 0);
        Reg.WriteDateTime(P_HORA, Hora);
        Reg.CloseKey;
        end;
     Finally
            FreeAndNil(Reg);
     end;
end;

end.
