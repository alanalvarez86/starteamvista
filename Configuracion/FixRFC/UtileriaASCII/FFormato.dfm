object Formato: TFormato
  Left = 245
  Top = 16
  Width = 601
  Height = 422
  Caption = 'Formato Del Archivo ASCII Con Los Datos De Los Empleados'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel: TPanel
    Left = 0
    Top = 347
    Width = 593
    Height = 41
    Align = alBottom
    TabOrder = 0
    object Salir: TBitBtn
      Left = 497
      Top = 5
      Width = 89
      Height = 30
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      TabOrder = 0
      Kind = bkClose
    end
    object LogSave: TBitBtn
      Left = 6
      Top = 5
      Width = 86
      Height = 30
      Hint = 'Exportar Formato A Un Archivo'
      Caption = '&Exportar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = LogSaveClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333330070
        7700333333337777777733333333008088003333333377F73377333333330088
        88003333333377FFFF7733333333000000003FFFFFFF77777777000000000000
        000077777777777777770FFFFFFF0FFFFFF07F3333337F3333370FFFFFFF0FFF
        FFF07F3FF3FF7FFFFFF70F00F0080CCC9CC07F773773777777770FFFFFFFF039
        99337F3FFFF3F7F777F30F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
    end
  end
  object Memo: TMemo
    Left = 0
    Top = 0
    Width = 593
    Height = 347
    Align = alClient
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'Archivo De Datos De ENTRADA'
      '==========================='
      'El archivo que contiene los datos a procesar debe conformarse al'
      'formato ASCII Delimitado:'
      ''
      '1) Los datos deben estar separados por comas ( , )'
      '2) Se requieren los siguientes datos:'
      ''
      'Descripci�n         Requerido    Formato'
      
        '=================== ===========  ===============================' +
        '=============='
      
        'Apellido Paterno    Obligatorio  Texto entre comillas dobles ( "' +
        ' )'
      
        'Apellido Materno    Obligatorio  Texto entre comillas dobles ( "' +
        ' )'
      
        'Nombres             Obligatorio  Texto entre comillas dobles ( "' +
        ' )'
      
        'Fecha De Nacimiento Obligatorio  Texto en formato dd/mm/aa � dd/' +
        'mm/aaaa'
      
        'Referencia          Opcional     Texto que indica el N�mero, C�d' +
        'igo, etc.'
      '                                 de Empleado'
      
        'R.F.C. Original     Opcional     Texto entre comillas dobles ( "' +
        ' ) Sin Guiones'
      '                                 Ni Caracteres de Formateo'
      
        'Fecha De Alta       Opcional     Texto en formato dd/mm/aa � dd/' +
        'mm/aaaa                                 '
      ''
      
        '3) Se requieren por lo menos una de las siguientes combinaciones' +
        ':'
      ''
      'a) Apellido Paterno, Nombres y Fecha de Nacimiento'
      'b) Apellido Materno, Nombres y Fecha de Nacimiento'
      
        'c) Apellido Paterno, Apellido Materno, Nombres y Fecha de Nacimi' +
        'ento'
      ''
      'Archivo De Resultados General ( SALIDA )'
      '========================================'
      ''
      'El archivo de Resultados se genera en formato ASCII Delimitado:'
      ''
      '1) Los datos deben estar separados por comas ( , )'
      '2) Se producen los siguientes datos:'
      ''
      'Descripci�n         Formato'
      
        '=================== ============================================' +
        '=='
      'Apellido Paterno    Texto entre comillas dobles ( " )'
      'Apellido Materno    Texto entre comillas dobles ( " )'
      'Nombres             Texto entre comillas dobles ( " )'
      'Fecha De Nacimiento Texto en formato dd/mm/aa � dd/mm/aaaa'
      
        'R.F.C. Generado     Texto entre comillas dobles ( " ) Sin Guione' +
        's'
      '                    Ni Carecteres de Formateo'
      
        'R.F.C. Original     Texto entre comillas dobles ( " ) Sin Guione' +
        's'
      '                    Ni Carecteres de Formateo'
      'Referencia          Texto que indica el N�mero, C�digo, etc.'
      
        '                    de Empleado indicado en el archivo de Entrad' +
        'a'
      ''
      'Archivo De Resultados Kardex ( SALIDA )'
      '======================================='
      ''
      'El archivo de Resultados se genera en formato ASCII Delimitado:'
      ''
      '1) Los datos deben estar separados por comas ( , )'
      '2) Se producen los siguientes datos:'
      ''
      'Descripci�n         Formato'
      
        '=================== ============================================' +
        '=='
      'Empleado            Texto que indica el N�mero, C�digo, etc.'
      'Operaci�n           Tipo de la Operaci�n a Realizar'
      'Fecha De Movimiento Texto en formato dd/mm/aa � dd/mm/aaaa'
      'Campo               Texto del Campo a Modificar'
      
        'R.F.C.              Texto entre comillas dobles ( " ) Sin Guione' +
        's'
      '                    Ni Caracteres de Formateo'
      ''
      ' ')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object SaveDialog: TSaveDialog
    Filter = 'Textos (*.txt)|*.txt|Todos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Archivo Del Formato ASCII'
    Left = 64
    Top = 96
  end
end
