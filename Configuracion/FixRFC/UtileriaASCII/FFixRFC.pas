unit FFixRFC;

interface

{
Delimitado:
123456789,10,�dd/mm/yyyy�,�CB_RFC�,"AAAA999999XXX"
18,10,23/01/2004,"CB_RFC","RABM6908102C0"

�Derechos Reservados  Grupo Tress Internacional, S.A. de C.V.
}
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ShellApi,
     ZetaAsciiFile;

type
    eTipoErrores = ( etIncompleto, etIncorrecto, etHomoclave );     
type
  TEmpleado = record
    ApellidoPaterno: String;
    ApellidoMaterno: String;
    Nombres: String;
    Natalicio: String;
    FechaNacimiento: TDate;
    Referencia: String;
    Original: string;
    RFC: String;
    Ingreso: string;
    Alta: TDate;
  end;
  TSystemDataReader = class(TForm)
    OpenDialog: TOpenDialog;
    PanelInferior: TPanel;
    Salir: TBitBtn;
    Procesar: TBitBtn;
    SaveDialog: TSaveDialog;
    ArchivosGB: TGroupBox;
    DatosLBL: TLabel;
    ResultadosLBL: TLabel;
    Resultados: TEdit;
    Datos: TEdit;
    ResultadosBuscar: TSpeedButton;
    DatosBuscar: TSpeedButton;
    VerFormato: TSpeedButton;
    BitacoraGB: TGroupBox;
    Output: TMemo;
    LogSave: TBitBtn;
    lblFormato: TLabel;
    rbGeneral: TRadioButton;
    rbKardex: TRadioButton;
    gbModo: TGroupBox;
    rbDiagnosticar: TRadioButton;
    rbArreglar: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure DatosBuscarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure ResultadosBuscarClick(Sender: TObject);
    procedure ProcesarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure VerFormatoClick(Sender: TObject);
    procedure LogSaveClick(Sender: TObject);
    procedure rbGeneralClick(Sender: TObject);
    procedure rbKardexClick(Sender: TObject);
    procedure rbDiagnosticarClick(Sender: TObject);
    procedure rbArreglarClick(Sender: TObject);
  private
    { Private declarations }
    FFuente: TStrings;
    FDestino: TStrings;
    FDato: TStrings;
    FEmpleado: TEmpleado;
    FGeneral: boolean;
    FDiagnosticar: boolean;
    FGenerado: string;
    FError: string;
    FIncompletos: integer;
    FIncorrectos: integer;
    FErrorHomo: integer;
    function Arreglar( const sDatos: String ): String;
    function Diagnosticar( const sDatos: String ): boolean;    
    function GetFileName(const sFile: String): String;
    function GetFileNameSource: String;
    function GetFileNameTarget: String;
    function StrAsFecha( sFecha: String ): TDateTime; // Formato DDMMYYYY � DDMMYY //
    function OEMToChar(const sOriginal: String): String;
    function CharToOEM(const sOriginal: String): String;
    function RevisaRFC: boolean;
    procedure LlenaArreglo( const sDatos: String );    
    procedure Log( const sTexto: String );
    procedure LogInit;
    procedure EscribeLog;
    procedure Habilitacontroles( const lHabilita: boolean );
  public
    { Public declarations }
  end;

procedure CallNotePad( const sFileName: String );

var
  SystemDataReader: TSystemDataReader;

implementation

uses FFormato,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaClientTools;

{$R *.DFM}

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      ShowCmd );
end;

procedure CallNotePad( const sFileName: String );
begin
     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ), SW_SHOWDEFAULT );
end;

{ ***** TSystemDataReader ******* }

procedure TSystemDataReader.FormCreate(Sender: TObject);
begin
     Datos.Text := GetFileName( 'RFC.dat' );
     Resultados.Text := GetFileName( 'Kardex.dat' );
     FFuente := TStringList.Create;
     FDestino := TStringList.Create;
     FDato := TStringList.Create;
     FDiagnosticar := TRUE;
     FGeneral := TRUE;
     FGenerado := VACIO;
     FError := VACIO;
     Habilitacontroles( FALSE );
end;

procedure TSystemDataReader.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FDato );
     FreeAndNil( FDestino );
     FreeAndNil( FFuente );
end;

function TSystemDataReader.GetFileName( const sFile: String ): String;
begin
     Result := Format( '%s%s', [ ExtractFilePath( Application.ExeName ), sFile ] );
end;

function TSystemDataReader.GetFileNameSource: String;
begin
     Result := Datos.Text;
end;

function TSystemDataReader.GetFileNameTarget: String;
begin
     Result := Resultados.Text;
end;

procedure TSystemDataReader.DatosBuscarClick(Sender: TObject);
begin
     with Datos do
     begin
          with OpenDialog do
          begin
               FilterIndex := 0;
               FileName := Text;
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TSystemDataReader.ResultadosBuscarClick(Sender: TObject);
begin
     with Resultados do
     begin
          with SaveDialog do
          begin
               Title := 'Especificar El Archivo De Resultados';
               FilterIndex := 0;
               FileName := Text;
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TSystemDataReader.VerFormatoClick(Sender: TObject);
begin
     Formato := TFormato.Create( Self );
     try
        with Formato do
        begin
             ShowModal;
        end;
     finally
            FreeAndNil( Formato );
     end;
end;

function TSystemDataReader.StrAsFecha( sFecha: String ): TDateTime; // Formato DDMMYYYY � DDMMYY //

function GetSiguiente: String;
var
   iPos: Word;
begin
     iPos := Pos( '/', sFecha );
     Result := Copy( sFecha, 0, ( iPos - 1 ) );
     sFecha := Copy( sFecha, ( iPos + 1 ), MaxInt );
end;

var
   sDia, sMes: String;
begin
     sDia := GetSiguiente;
     sMes := GetSiguiente;
     Result := EncodeDate( CodificaYear( StrToInt( sFecha ) ), StrToInt( sMes ), StrToInt( sDia ) );
end;

function TSystemDataReader.OEMToChar( const sOriginal: String ): String;
var
   pOriginal, pTarget: PChar;
begin
     pOriginal := StrAlloc( Length( sOriginal ) + 1 );
     try
        pTarget := StrAlloc( Length( sOriginal ) + 1 );
        try
           pOriginal := StrPCopy( pOriginal, sOriginal );
           Windows.OEMToChar( pOriginal, pTarget );
           Result := pTarget;
        finally
               StrDispose( pTarget );
        end;
     finally
            StrDispose( pOriginal );
     end;
end;

function TSystemDataReader.CharToOEM( const sOriginal: String ): String;
var
   pOriginal, pTarget: PChar;
begin
     pOriginal := StrAlloc( Length( sOriginal ) + 1 );
     try
        pTarget := StrAlloc( Length( sOriginal ) + 1 );
        try
           pOriginal := StrPCopy( pOriginal, sOriginal );
           Windows.CharToOEM( pOriginal, pTarget );
           Result := pTarget;
        finally
               StrDispose( pTarget );
        end;
     finally
            StrDispose( pOriginal );
     end;
end;

procedure TSystemDataReader.LlenaArreglo( const sDatos: String );
begin
     if StrLleno( sDatos ) then
     begin
          FDato.CommaText := sDatos;
          if ( FDato.Count < 4 ) then
          begin
               raise Exception.Create( 'Formato Inv�lido' );
          end
          else if ( ( not FGeneral ) and ( FDato.Count < 6 ) ) or ( FDato.Count > 7 ) then
               begin
                    raise Exception.Create( 'Formato Inv�lido' );
               end
               else
               begin
                    with FEmpleado do
                    begin
                         with FDato do
                         begin
                              ApellidoPaterno := OEMToChar( Strings[ 0 ] );
                              ApellidoMaterno := OEMToChar( Strings[ 1 ] );
                              Nombres := OEMToChar( Strings[ 2 ] );
                              Natalicio := Strings[ 3 ];
                              FechaNacimiento := StrAsFecha( Natalicio );
                              if ( Count >= 5 ) then
                                 Referencia := Strings[ 4 ]
                              else
                                  Referencia := VACIO;
                              if ( Count >= 6 ) then
                                 Original := Strings[ 5 ]
                              else
                                  Original := VACIO;
                              if ( Count >= 7 ) then
                                 Ingreso := Strings[ 6 ]
                              else
                                  Ingreso := '01/01/1980';
                              Alta := StrAsFecha( Ingreso );
                         end;
                    end;
               end;
     end;
end;

function TSystemDataReader.RevisaRFC: boolean;
const
     K_RFC_CON_HOMOCLAVE = 13;
     K_RFC_SIN_HOMOCLAVE = 10;
     aErrores: array[ eTipoErrores ] of PChar = ( '(Incompleto)', '(Incorrecto)', '(HomoClave) ' );
begin
     with FEmpleado do
     begin
          FGenerado := ZetaCommonTools.CalcRFC( ApellidoPaterno, ApellidoMaterno, Nombres, FechaNacimiento );
          Result := ( Length( Trim( Original ) ) < K_RFC_CON_HOMOCLAVE );
          if Result then
          begin
               Inc( FIncompletos );
               FError := aErrores[ etIncompleto ];
          end
          else
          begin
               Result := ( Copy( Original, 1, K_RFC_SIN_HOMOCLAVE ) <> Copy ( FGenerado, 1, K_RFC_SIN_HOMOCLAVE ) );
               if Result then
               begin
                    Inc( FIncorrectos );
                    FError := aErrores[ etIncorrecto ];
               end
               else
               begin
                    Result := ( FGenerado <> Original );
                    if Result then
                    begin
                         Inc( FErrorHomo );
                         FError := aErrores[ etHomoClave ];
                    end
                    else
                        FError := VACIO;
               end;
          end;
     end;
end;

function TSystemDataReader.Diagnosticar( const sDatos: String ): boolean;
begin
     LlenaArreglo( sDatos );
     if ( RevisaRFC ) then
     begin
          Result := FALSE;
          EscribeLog;
     end
     else
         Result := TRUE;
end;

procedure TSystemDataReader.EscribeLog;
begin
     with FEmpleado do
     begin
          if strLleno( Original ) then
             Log( Format( '%s ', [ FError ] ) + ZetaCommonTools.PadR( Trim( Format( '%s %s %s', [ Referencia, ApellidoPaterno, ApellidoMaterno ] ) ) + Format( ', %s:', [ Nombres ] ), 50 ) + ZetaCommonTools.PadR( Trim( Format( 'Original: %s-%s-%s', [ Copy( Original, 1, 4 ), Copy( Original, 5, 6 ), Copy( Original, 11, 3 ) ] ) ), 26 ) + ZetaCommonTools.PadR( Trim( Format( 'Generado: %s-%s-%s', [ Copy( FGenerado, 1, 4 ), Copy( FGenerado, 5, 6 ), Copy( FGenerado, 11, 3 ) ] ) ), 26 ) )
          else
              Log( Format( '%s ', [ FError ] ) + ZetaCommonTools.PadR( Trim( Format( '%s %s %s', [ Referencia, ApellidoPaterno, ApellidoMaterno ] ) ) + Format( ', %s:', [ Nombres ] ), 50 ) + Format( 'Original:                 Generado: %s-%s-%s', [ Copy( FGenerado, 1, 4 ), Copy( FGenerado, 5, 6 ), Copy( FGenerado, 11, 3 ) ] ) );
     end;
end;

function TSystemDataReader.Arreglar( const sDatos: String ): String;
const
     K_OPERACION = 10;
     K_CAMPO_RFC = 'CB_RFC';
begin
     LlenaArreglo( sDatos );
     Result := VACIO;
     if ( RevisaRFC ) then
     begin
          with FEmpleado do
          begin
               if ( FGeneral ) then
               begin
                    if ( ZetaCommonTools.StrLleno( Referencia ) and ZetaCommonTools.StrLleno( Original ) ) then
                       Result := Format( '"%s","%s","%s",%s,"%s","%s",%s', [ CharToOEM( ApellidoPaterno ), CharToOEM( ApellidoMaterno ), CharToOEM( Nombres ), Natalicio, FGenerado, Original, Referencia ] )
                    else
                        if ( ZetaCommonTools.StrLleno( Referencia ) ) then
                           Result := Format( '"%s","%s","%s",%s,"%s",%s', [ CharToOEM( ApellidoPaterno ), CharToOEM( ApellidoMaterno ), CharToOEM( Nombres ), Natalicio, FGenerado, Referencia ] )
                        else
                            if ( ZetaCommonTools.StrLleno( Original ) ) then
                               Result := Format( '"%s","%s","%s",%s,"%s","%s"', [ CharToOEM( ApellidoPaterno ), CharToOEM( ApellidoMaterno ), CharToOEM( Nombres ), Natalicio, FGenerado, Original ] )
                            else
                                Result := Format( '"%s","%s","%s",%s,"%s"', [ CharToOEM( ApellidoPaterno ), CharToOEM( ApellidoMaterno ), CharToOEM( Nombres ), Natalicio, FGenerado ] );
                    EscribeLog;
               end
               else
               begin
                    Result := Format( '%s,%d,"%s","%s","%s"', [ Referencia, K_OPERACION, FechaAsStr( Alta ), K_CAMPO_RFC, FGenerado ] );
                    EscribeLog;
               end;
          end;
     end;
end;

procedure TSystemDataReader.LogInit;
begin
     Output.Lines.Clear;
     Application.ProcessMessages;
end;

procedure TSystemDataReader.Log( const sTexto: String );
begin
     Output.Lines.Add( sTexto );
     Application.ProcessMessages;
end;

procedure TSystemDataReader.ProcesarClick(Sender: TObject);
var
   i, iOK, iErrores, iSinProblema, iProcesados: integer;
   oCursor: TCursor;
   sFuente, sDestino, sDatos, sRegistro: String;
begin
     sFuente := GetFileNameSource;
     sDestino := GetFileNameTarget;
     if ZetaCommonTools.StrLleno( sFuente ) then
     begin
          if SysUtils.FileExists( sFuente ) then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  try
                     iOK := 0;
                     iErrores := 0;
                     iSinProblema := 0;
                     FIncompletos := 0;
                     FIncorrectos := 0;
                     FErrorHomo := 0;
                     iProcesados := 0;
                     FDiagnosticar := ( rbDiagnosticar.Checked );
                     FGeneral := ( rbGeneral.Checked );
                     FFuente.LoadFromFile( sFuente );
                     LogInit;
                     Log( 'Iniciando Proceso:       ' + FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) );
                     Log( Format( 'Registros Encontrados:   %10.0n', [ FFuente.Count / 1 ] ) );
                     Log( VACIO );
                     FDestino.Clear;
                     with FFuente do
                     begin
                          for i := 0 to ( Count - 1 ) do
                          begin
                               sDatos := Strings[ i ];
                               try
                                  if ( FDiagnosticar ) then
                                  begin
                                       if ( Diagnosticar( sDatos ) ) then
                                          Inc( iOK );
                                  end
                                  else
                                  begin
                                       sRegistro := Arreglar( sDatos );
                                       if strLleno( sRegistro ) then
                                       begin
                                            FDestino.Add( sRegistro );
                                            Inc( iOK );
                                       end
                                       else
                                           Inc( iSinProblema )
                                  end;
                                  Inc( iProcesados );
                               except
                                     on Error: Exception do
                                     begin
                                          Log( Format( 'Error L�nea # %d: %s( Datos: %s )', [ i, Error.Message, sDatos ] ) );
                                          Inc( iErrores );
                                     end;
                               end;
                          end;
                     end;
                     Log( VACIO );
                     if FDiagnosticar then
                        Log( 'Resultados del Diagn�stico' )
                     else
                     begin
                          FDestino.SaveToFile( sDestino );
                          Log( 'Resultados del Proceso' );
                     end;
                     Log( '-----------------------------------' );
                     Log( Format( 'Registros Procesados:    %10.0n', [ ( iProcesados ) / 1 ] ) );
                     Log( Format( 'Errores:                 %10.0n', [ iErrores / 1 ] ) );
                     Log( VACIO );                     
                     if ( FDiagnosticar ) then
                     begin
                          Log( Format( 'R.F.Cs Correctos:        %10.0n', [ iOK / 1 ] ) );
                          Log( Format( 'R.F.Cs Incompletos:      %10.0n', [ FIncompletos / 1 ] ) );
                          Log( Format( 'R.F.Cs Incorrectos:      %10.0n', [ FIncorrectos / 1 ] ) );
                          Log( Format( 'HomoClave Diferente:     %10.0n', [ FErrorHomo / 1 ] ) );
                          Log( '-----------------------------------' );
                          Log( Format( 'Total De Problemas:      %10.0n', [ ( Fincompletos + FIncorrectos + FErrorHomo ) / 1 ] ) );
                     end
                     else
                     begin
                          Log( Format( 'R.F.Cs Correctos:        %10.0n', [ iSinProblema / 1 ] ) );
                          Log( Format( 'R.F.Cs Incompletos:      %10.0n', [ FIncompletos / 1 ] ) );
                          Log( Format( 'R.F.Cs Incorrectos:      %10.0n', [ FIncorrectos / 1 ] ) );
                          Log( Format( 'HomoClave Diferente:     %10.0n', [ FErrorHomo / 1 ] ) );
                          Log( '-----------------------------------' );
                          Log( Format( 'Registros Generados:     %10.0n', [ iOK / 1 ] ) );
                     end;
                     Log( VACIO );
                     Log( 'Proceso Terminado:       ' + FormatDateTime( 'dd/mmm/yyyy hh:nn:ss AM/PM', Now ) );
                     if ( not FDiagnosticar ) then
                     begin
                          if ( FDestino.Count > 0 ) then
                          begin
                               if ZetaDialogo.zConfirm( '� Proceso Terminado !', Format( 'El Archivo De Resultados %0:s %1:s %0:s Ha Sido Generado %0:s � Desea Verlo ?', [ CR_LF, sDestino ] ), 0, mbOK ) then
                               begin
                                    CallNotePad( sDestino );
                               end;
                          end
                          else
                              ZetaDialogo.zError( '� Proceso Terminado Con Errores !', 'No Se Gener� Ning�n Registro de Salida', 0 );
                     end;
                  except
                        on Error: Exception do
                        begin
                             ZetaDialogo.zExcepcion( '� Error Al Procesar !', 'Se Encontr� Un Error Al Procesar Los Datos', Error, 0 );
                        end;
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
          end
          else
          begin
               ZetaDialogo.zError( '� Atenci�n !', Format( 'El Archivo De Datos %s No Existe', [ sFuente ] ), 0 );
               ActiveControl := Datos;
          end;
     end
     else
     begin
          ZetaDialogo.zError( '� Atenci�n !', 'No Se Especific� El Archivo De Datos', 0 );
          ActiveControl := Datos;
     end;
end;

procedure TSystemDataReader.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TSystemDataReader.LogSaveClick(Sender: TObject);
var
   sFileName: String;
begin
     sFileName := GetFileName( 'Bitacora.dat' );
     with SaveDialog do
     begin
          Title := 'Archivo Donde Guardar La Bit�cora';
          FilterIndex := 0;
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if Execute then
          begin
               sFileName := FileName;
               Output.Lines.SaveToFile( sFileName );
               if ZetaDialogo.zConfirm( '� Bit�cora Guardada !', Format( 'La Bit�cora Fu� Guardada En El Archivo %0:s %1:s %0:s � Desea Verlo ?', [ CR_LF, sFileName ] ), 0, mbOK ) then
               begin
                    CallNotePad( sFileName );
               end;
          end;
     end;
end;

procedure TSystemDataReader.rbGeneralClick(Sender: TObject);
begin
     Resultados.Text := GetFileName( 'Resultados.dat' );
end;

procedure TSystemDataReader.rbKardexClick(Sender: TObject);
begin
     Resultados.Text := GetFileName( 'Kardex.dat' );
end;

procedure TSystemDataReader.Habilitacontroles(const lHabilita: boolean);
begin
     ResultadosLBL.Enabled := lHabilita;
     Resultados.Enabled := lHabilita;
     ResultadosBuscar.Enabled := lHabilita;
     lblFormato.Enabled := lHabilita;
     rbGeneral.Enabled := lHabilita;
     rbKardex.Enabled := lHabilita;
end;

procedure TSystemDataReader.rbDiagnosticarClick(Sender: TObject);
begin
     Habilitacontroles( FALSE );
end;

procedure TSystemDataReader.rbArreglarClick(Sender: TObject);
begin
     Habilitacontroles( TRUE );
end;

end.
