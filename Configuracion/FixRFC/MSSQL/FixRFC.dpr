program FixRFC;

uses
  Forms,
  ZetaSplash,
  ZetaClientTools,
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseImportaShell in '..\..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  FTressShell in '..\FTressShell.pas' {TressShell},
  DBaseSistema in '..\..\..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  DBaseCliente in '..\..\..\DataModules\DBaseCliente.pas',
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DCliente in '..\..\..\DataModules\DCliente.pas' {dmCliente: TDataModule};

{$R *.RES}
procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Correción de R.F.C.';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.
