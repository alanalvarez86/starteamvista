unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell, Psock, NMsmtp, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons,
  ZetaClientDataSet,
  ZetaTipoEntidad,
  ComCtrls;


type
  TTressShell = class(TBaseImportaShell)
    PanelOpciones: TPanel;
    rgOpciones: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure _P_ProcesarExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FBitacora: TStrings;
    FErrores: TStrings;
    procedure Procesar;
    procedure GrabaBitacora(oMemo: TMemo; const sArchivo: String);
  protected
    { Protected declarations }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
    procedure EscribeBitacoras; override;
  public
    { Public declarations }
    procedure RefrescaEmpleado;
    procedure BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
    procedure CambiaEmpleadoActivos;
    procedure CargaEmpleadoActivos;
    procedure ChangeTimerInfo;
    procedure CreaArbol(const lEjecutar: Boolean);
    function FormaActivaNomina: Boolean;
    procedure ReconectaFormaActiva;
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure SetAsistencia(const dValue: TDate);
    procedure SetDataChange(const Entidades: ListaEntidades);
  end;

var
  TressShell: TTressShell;

implementation

{$R *.DFM}

uses
    DProcesa,
    ZetaCommonTools,
    ZetaCommonClasses,
    DZetaServerProvider;

{ TTressShell }

{ Procedimientos y Funciones Publicos del Shell de Tress }

procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: ListaEntidades);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.DoCloseAll;
begin
  inherited;

end;

procedure TTressShell.DoOpenAll;
begin
     inherited;
     ActivaControles( FALSE );
end;

procedure TTressShell.DoProcesar;
begin
  inherited;

end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     inherited;
     dmProcesa := TdmProcesa.Create( Self );
     FBitacora := TStringList.Create;
     FErrores := TStringList.Create;
     ArchBitacora.Text := ExtractFilePath( Application.ExeName ) + 'Bitacora.log';
     ArchErrores.Text := ExtractFilePath( Application.ExeName ) + 'Errores.log';
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmProcesa.Free;
     FreeAndNil( FErrores );
     FreeAndNil( FBitacora );
end;

procedure TTressShell._P_ProcesarExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Procesar;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.Procesar;
begin
     ActivaControles( TRUE );
     try
        PageControl.ActivePage := TabBitacora;
        InitBitacora;
        with dmProcesa do
        begin
             Application.ProcessMessages;        
             GetDatosEmpleados;
             if Procesa( rgOpciones.ItemIndex, FBitacora, FErrores ) then
             begin;
                  if Assigned( FBitacora ) then
                     MemBitacora.Lines.Assign( FBitacora );
                  if Assigned( FErrores ) then
                     MemErrores.Lines.Assign( FErrores );
             end;
        end;
     finally
            EscribeBitacoras;
            ActivaControles( FALSE );
     end;
end;

procedure TTressShell.EscribeBitacoras;
begin
     self.GrabaBitacora( MemBitacora, ArchBitacora.Text );
     self.GrabaBitacora( MemErrores, ArchErrores.Text );
end;

procedure TTressShell.GrabaBitacora(oMemo: TMemo; const sArchivo: String);
begin
     try
        if strLleno( sArchivo ) then
           oMemo.Lines.SaveToFile( sArchivo );
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( self.Caption, 'Error al Grabar ' + oMemo.Hint + ' al Archivo ' + CR_LF + sArchivo );
        end;
     end;
end;

end.
