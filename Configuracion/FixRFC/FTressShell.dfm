inherited TressShell: TTressShell
  Left = 291
  Top = 147
  Width = 457
  Height = 476
  Caption = 'Corrección de Registro Federal de Causantes'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 411
    Width = 449
  end
  inherited PageControl: TPageControl
    Top = 192
    Width = 449
    Height = 219
    ActivePage = TabErrores
    TabOrder = 3
    inherited TabBitacora: TTabSheet
      inherited MemBitacora: TMemo
        Width = 441
        Height = 150
        ScrollBars = ssVertical
      end
      inherited PanelBottom: TPanel
        Top = 150
        Width = 441
      end
    end
    inherited TabErrores: TTabSheet
      inherited Panel1: TPanel
        Top = 150
        Width = 441
      end
      inherited MemErrores: TMemo
        Width = 441
        Height = 150
        ScrollBars = ssVertical
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 158
    Width = 449
    inherited BtnProcesar: TBitBtn
      Left = 184
      Anchors = [akRight, akBottom]
      Caption = '&Procesar'
    end
    inherited BtnDetener: TBitBtn
      Left = 4
      TabOrder = 2
      Visible = False
    end
    inherited BtnSalir: TBitBtn
      Left = 316
      TabOrder = 1
    end
  end
  inherited PanelEncabezado: TPanel
    Width = 449
    TabOrder = 0
    Visible = False
  end
  object PanelOpciones: TPanel [4]
    Left = 0
    Top = 59
    Width = 449
    Height = 99
    Align = alTop
    TabOrder = 1
    object rgOpciones: TRadioGroup
      Left = 120
      Top = 4
      Width = 209
      Height = 90
      Caption = ' Opciones de Corrección'
      ItemIndex = 0
      Items.Strings = (
        '&Diagnosticar Información'
        '&ArreglarToda La Información'
        '&Sobreescribir Toda La Información')
      TabOrder = 0
    end
  end
  inherited LogoffTimer: TTimer
    Left = 56
  end
  inherited ActionList: TActionList
    Left = 90
    Top = 28
    inherited _P_Procesar: TAction
      Caption = '&Procesar'
      Hint = 'Iniciar Proceso de Correción'
    end
  end
  inherited ShellMenu: TMainMenu
    Left = 416
    inherited Archivo1: TMenuItem
      inherited CancelarProceso2: TMenuItem
        Visible = False
      end
    end
  end
  inherited OpenDialog: TOpenDialog
    Filter = 'Archivos de Texto (*.log)|*.log|Todos (*.*)|*.*'
    Title = 'Guardar en...'
    Left = 14
    Top = 26
  end
  inherited ArbolImages: TImageList
    Left = 476
    Top = 8
  end
  inherited Email: TNMSMTP
    Left = 416
    Top = 0
  end
end
