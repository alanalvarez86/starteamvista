unit FSentinelInstalacion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Checklst, Mask,
     FAutoServer,
     ZetaNumero,
     ZetaDBTextBox;

type
  TSentinelInstall = class(TForm)
    EmpresaLBL: TLabel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    ClaveLBL: TLabel;
    Clave: TEdit;
    SentinelLBL: TLabel;
    Sentinel: TZetaTextBox;
    Empresa: TZetaTextBox;
    InstalacionLBL: TLabel;
    Instalacion: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  end;

var
  SentinelInstall: TSentinelInstall;

implementation

uses FAutoClasses,
     FHelpContext,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

procedure TSentinelInstall.FormCreate(Sender: TObject);
begin
     HelpContext := H00029_Capturar_clave_de_instalacion;
end;

procedure TSentinelInstall.FormShow(Sender: TObject);
begin
     with AutoServer do
     begin
          Load;
          Self.Sentinel.Caption := IntToStr( NumeroSerie );
          Self.Empresa.Caption := IntToStr( Empresa );
          Self.Instalacion.Caption := IntToStr( Instalaciones );
     end;
     HelpContext := 0;
end;

procedure TSentinelInstall.OKClick(Sender: TObject);
const
     CR_LF = Chr( 13 ) + Chr( 10 );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        try
           with AutoServer do
           begin
            {$ifdef ANTES}
                if AumentarInstalaciones( Clave.Text, True ) then
                begin
                     ZetaDialogo.zInformation( 'Clave de Instalaci�n Registrada', 'La Clave De Instalaci�n Fu� Capturada Correctamente', 0 );
                     ModalResult := mrOk;
                end;
            {$endif}
           end;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
