unit F3DataReader;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl,
     FAutoClasses;

type
  TTressDataLoader = class(TForm)
    PanelControles: TPanel;
    DirectorioLBL: TLabel;
    Directorio: TEdit;
    DirectorioBTN: TSpeedButton;
    PanelInferior: TPanel;
    BitacoraGB: TGroupBox;
    Bitacora: TMemo;
    Procesar: TBitBtn;
    Salir: TBitBtn;
    Verificar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure DirectorioBTNClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure ProcesarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure VerificarClick(Sender: TObject);
  private
    { Private declarations }
    FArchivos: TStrings;
    function GetDirectory: String;
    procedure SetDirectory(const Value: String);
  public
    { Public declarations }
    property Directory: String read GetDirectory write SetDirectory;
  end;

var
  TressDataLoader: TTressDataLoader;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaCommonClasses,
     FVerificar,
     DDatosSistema;

{$R *.dfm}

{ *** TTressDataLoader *** }

procedure TTressDataLoader.FormCreate(Sender: TObject);
begin
     FArchivos := TStringList.Create;
     dmDatosSistema := TdmDatosSistema.Create( Self );
     Directory := Format( '%sDatos', [ ExtractFilePath( Application.ExeName ) ] );
end;

procedure TTressDataLoader.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmDatosSistema );
     FreeAndNil( FArchivos );
end;

function TTressDataLoader.GetDirectory: String;
begin
     Result := ZetaCommonTools.VerificaDir( Directorio.Text );
end;

procedure TTressDataLoader.SetDirectory( const Value: String );
begin
     Directorio.Text := Value;
end;

procedure TTressDataLoader.DirectorioBTNClick(Sender: TObject);
var
   sPath: String;
begin
     sPath := Directory;
     if FileCtrl.SelectDirectory( 'Especifique el directorio deseado', '', sPath ) then
        Directory := sPath;
end;

procedure TTressDataLoader.SalirClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TTressDataLoader.ProcesarClick(Sender: TObject);
const
     K_EXTENSION = '.3dt';
     K_EXTENSION_NEW = '.usd';
var
   oCursor: TCursor;
   sArchivo, sWildCard: String;
   Archivo: TSearchRec;
   i, iOk, iErrores: Integer;
begin
     if DirectoryExists( Directory ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with Bitacora do
             begin
                  with Lines do
                  begin
                       Clear;
                       BeginUpdate;
                  end;
             end;
             with FArchivos do
             begin
                  Clear;
                  sWildCard := Format( '%s*%s', [ Directory, K_EXTENSION ] );
                  if ( FindFirst( sWildCard, faAnyFile, Archivo ) = 0 ) then
                  begin
                       repeat
                             if ( ExtractFileExt( Archivo.Name ) = K_EXTENSION ) then
                                Add( Format( '%s%s', [ Directory, Archivo.Name ] ) );
                       until ( FindNext( Archivo ) <> 0 );
                       FindClose( Archivo );
                  end;
                  if ( Count > 0 ) then
                  begin
                       iOk := 0;
                       iErrores := 0;
                       with dmDatosSistema do
                       begin
                            if Conectar then
                            begin
                                 try
                                    for i := 0 to ( Count - 1 ) do
                                    begin
                                         sArchivo := Strings[ i ];
                                         if Cargar( sArchivo ) then
                                         begin
                                              Inc( iOk );
                                              Bitacora.Lines.Add( Format( 'Archivo %s Procesado', [ ExtractFileName( sArchivo ) ] ) );
                                              RenameFile( sArchivo, ChangeFileExt( sArchivo, K_EXTENSION_NEW ) );
                                         end
                                         else
                                         begin
                                              Bitacora.Lines.Add( Format( 'ERROR: Archivo %s No Fu� Procesado: %s', [ ExtractFileName( sArchivo ), StatusMsg ] ) );
                                              Inc( iErrores );
                                         end;
                                    end;
                                    if ( iErrores > 0 ) then
                                       ZetaDialogo.ZWarning( '� Atenci�n !', Format( 'Se Procesaron %d Archivos Con Exito y Hubo %d Errores', [ iOk, iErrores ] ), 0, mbOk )
                                    else
                                        ZetaDialogo.ZInformation( '� Atenci�n !', Format( 'Se Procesaron %d Archivos Con Exito', [ iOk ] ), 0 );
                                 finally
                                        Desconectar;
                                 end;
                            end
                            else
                                ZetaDialogo.zError( '� Error Al Conectar !', StatusMsg, 0 );
                       end;
                  end
                  else
                      ZetaDialogo.ZError( 'No Hay Archivos', Format( 'No Hay Archivos Que Cumplan Con %s', [ sWildCard ] ), 0 );
             end;
             with Bitacora do
             begin
                  with Lines do
                  begin
                       EndUpdate;
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
     begin
          ZetaDialogo.ZError( 'Directorio No Existe', Format( 'El Directorio %s NoExiste', [ Directory ] ), 0 );
     end;
end;

procedure TTressDataLoader.VerificarClick(Sender: TObject);
begin
     VerificarLicencias := FVerificar.TVerificarLicencias.Create( Self );
     try
        with VerificarLicencias do
        begin
             ShowModal;
        end;
     finally
            FreeAndNil( VerificarLicencias );
     end;
end;

end.
