unit FDataReader;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ShellApi;

type
  TSystemDataReader = class(TForm)
    OpenDialog: TOpenDialog;
    PanelSuperior: TPanel;
    ArchivoLBL: TLabel;
    ArchivoBuscar: TSpeedButton;
    Archivo: TEdit;
    Leer: TBitBtn;
    PanelInferior: TPanel;
    Salir: TBitBtn;
    Datos: TMemo;
    Exportar: TBitBtn;
    SaveDialog: TSaveDialog;
    Excel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ArchivoBuscarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure LeerClick(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
    procedure ExcelClick(Sender: TObject);
  private
    { Private declarations }
    function GetFileName(const sFile: String): String;
    function GetFileNameSource: String;
    function GetData( var sData: String; const iCharacters: Integer ): String;
    function GetFecha( var sData: String ): TDate;
    function GetTotal(var sData: String): Integer;
    procedure GetTotals(var sHeader, sData: String);
    procedure ScanData(const sToken: String; var sHeader, sData: String);
  public
    { Public declarations }
  end;

var
  SystemDataReader: TSystemDataReader;

implementation

uses ZetaDialogo,
     ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaClientTools;

{$R *.DFM}

const
     K_NO_APARECE = 'N/A';

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      ShowCmd );
end;

procedure CallNotePad( const sFileName: String );
begin
     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( sFileName ), ExtractFilePath( sFileName ), SW_SHOWDEFAULT );
end;

{ ***** TSystemDataReader ******* }

procedure TSystemDataReader.FormCreate(Sender: TObject);
begin
     Archivo.Text := GetFileName( 'DatosSistema.3dt' );
end;

function TSystemDataReader.GetFileName( const sFile: String ): String;
begin
     Result := Format( '%s%s', [ ExtractFilePath( Application.ExeName ), sFile ] );
end;

function TSystemDataReader.GetFileNameSource: String;
begin
     Result := Archivo.Text;
end;

procedure TSystemDataReader.ArchivoBuscarClick(Sender: TObject);
begin
     with Archivo do
     begin
          with OpenDialog do
          begin
               Filter := 'Archivo Datos (*.3dt)|*.3dt|Todos (*.*)|*.*';
               FilterIndex := 0;
               FileName := Text;
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TSystemDataReader.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TSystemDataReader.LeerClick(Sender: TObject);
var
   oCursor: TCursor;
   sFileName: String;
   i: Integer;
begin
     sFileName := GetFileNameSource;
     if FileExists( sFileName ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with Datos do
             begin
                  with Lines do
                  begin
                       Clear;
                       BeginUpdate;
                       LoadFromFile( sFileName );
                       for i := ( Count - 1 ) downto 0 do
                       begin
                            Strings[ i ] := ZetaServerTools.Decrypt( Strings[ i ] );
                       end;
                       EndUpdate;
                  end;
             end;
             ActiveControl := Datos;
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
         ZetaDialogo.zError( '� Archivo No Existe !', Format( 'El Archivo %s No Existe', [ sFileName ] ), 0 );
end;

procedure TSystemDataReader.ScanData( const sToken: String; var sHeader, sData: String );
var
   i, iPos: Integer;
   sLine, sValue: String;
begin
     with Datos do
     begin
          with Lines do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    sLine := Strings[ i ];
                    iPos := Pos( sToken, sLine );
                    if ( iPos > 0 ) then
                    begin
                         sValue := Trim( Copy( sLine, ( iPos + Length( sToken ) ), ( Length( sLine ) - Length( sToken ) - iPos + 1 ) ) );
                         sHeader := sHeader + '"' + sToken + '",';
                         sData := sData + '"' + sValue + '",';
                         Break;
                    end;
               end;
          end;
     end;
end;

function TSystemDataReader.GetTotal( var sData: String ): Integer;
const
     K_LEN_TOTAL = 9;
var
   sValue: String;
   {$ifdef FALSE}
   i: Integer;
   {$endif}
begin
     {$ifdef FALSE}
     sValue := '';
     for i := 1 to Length( sData ) do
     begin
          if ( sData[ i ] in [ '0'..'9' ] ) then
             sValue := sValue + sData[ i ]
          else
              if( sData[ i ] <> ',' ) then
                  Break;
     end;
     Result := StrToIntDef( sValue, 0 );
     sData := Trim( Copy( sData, i, Length( sData ) - i + 1 ) );
     {$endif}
     sValue := GetData( sData, K_LEN_TOTAL );
     Result := StrToIntDef( sValue, 0 );
end;

function TSystemDataReader.GetData( var sData: String; const iCharacters: Integer ): String;
var
   iLen, iPos: Integer;
begin
     iLen := Length( sData );
     iPos := 1;
     { Buscar 1er. Caracter Que No Sea Espacio En Blanco }
     {
     for iPos := 1 to iLen do
     begin
          if ( sData[ iPos ] <> ' ' ) then
             Break;
     end;
     if ( iPos > iLen ) then
     begin
          Result := K_NO_APARECE;
          sData := Copy( sData, iPos, ( iLen - iPos ) );
     end
     else
     begin
     }
          Result := Trim( Copy( sData, iPos, iCharacters ) );
          if ( Result = '' ) then
             Result := K_NO_APARECE;
          sData := Copy( sData, ( iCharacters + 2 ), ( iLen - iCharacters + 1 ) );
     {
     end;
     }
end;

function TSystemDataReader.GetFecha( var sData: String ): TDate;
const
     K_LEN_FECHA = 11;
var
   sFecha, sMonth: String;
   iDay, iMonth, iYear: Integer;
begin
     sFecha := GetData( sData, K_LEN_FECHA );
     if ( sFecha = K_NO_APARECE ) then
        Result := ZetaCommonClasses.NullDateTime
     else
     begin
          iDay := StrToIntDef( Copy( sFecha, 1, 2 ), 0 );
          sMonth := UpperCase( Copy( sFecha, 4, 3 ) );
          if ( sMonth = 'ENE' ) or ( sMonth = 'JAN' ) then
             iMonth := 1
          else
          if ( sMonth = 'FEB' ) then
             iMonth := 2
          else
          if ( sMonth = 'MAR' ) then
             iMonth := 3
          else
          if ( sMonth = 'ABR' ) or ( sMonth = 'APR' ) then
             iMonth := 4
          else
          if ( sMonth = 'MAY' ) then
             iMonth := 5
          else
          if ( sMonth = 'JUN' ) then
             iMonth := 6
          else
          if ( sMonth = 'JUL' ) then
             iMonth := 7
          else
          if ( sMonth = 'AGO' ) or ( sMonth = 'AUG' ) then
             iMonth := 8
          else
          if ( sMonth = 'SEP' ) then
             iMonth := 9
          else
          if ( sMonth = 'OCT' ) then
             iMonth := 10
          else
          if ( sMonth = 'NOV' ) then
             iMonth := 11
          else
          if ( sMonth = 'DIC' ) or ( sMonth = 'DEC' ) then
             iMonth := 12
          else
              iMonth := 0;
          iYear := StrToIntDef( Copy( sFecha, 8, 4 ), 0 );
          Result := ZetaCommonTools.CodificaFecha( iYear, iMonth, iDay );
     end;
end;

procedure TSystemDataReader.GetTotals( var sHeader, sData: String );
const
     K_LEN_CODIGO = 10;
     K_LEN_EMPRESA = 50;
     K_LEN_NIVEL0 = 6;
     K_LEN_DATABASE = 50;
     K_LEN_PERIODO = 40;
     K_XLS_DATE = 'dd/mm/yyyy';
var
   iPtr, iFound: Integer;
   sLine: String;
begin
     with Datos do
     begin
          with Lines do
          begin
               iFound := 0;
               for iPtr := ( Count - 1 ) downto 0 do
               begin
                    sLine := Trim( Strings[ iPtr ] );
                    if ZetaCommonTools.StrLleno( sLine ) then
                    begin
                         if ( iFound >= 2 ) then
                            Break
                         else
                             Inc( iFound );
                    end;
               end;
               if ( iPtr >= 0 ) then
               begin
                    sHeader := sHeader + '"CODIGO",';
                    sData := sData + Format( '%s,', [ GetData( sLine, K_LEN_CODIGO ) ] );
                    sHeader := sHeader + '"EMPRESA",';
                    sData := sData + Format( '%s,', [ GetData( sLine, K_LEN_EMPRESA ) ] );
                    sHeader := sHeader + '"Activos",';
                    sData := sData + Format( '%d,', [ GetTotal( sLine ) ] );
                    sHeader := sHeader + '"Inactivos",';
                    sData := sData + Format( '%d,', [ GetTotal( sLine ) ] );
                    sHeader := sHeader + '"Totales",';
                    sData := sData + Format( '%d,', [ GetTotal( sLine ) ] );
                    sHeader := sHeader + '"Nivel0",';
                    sData := sData + Format( '%s,', [ GetData( sLine, K_LEN_NIVEL0 ) ] );
                    sHeader := sHeader + '"Base De Datos",';
                    sData := sData + Format( '%s,', [ GetData( sLine, K_LEN_DATABASE ) ] );
                    sHeader := sHeader + '"Ult. Alta",';
                    sData := sData + Format( '%s,', [ FormatDateTime( K_XLS_DATE, GetFecha( sLine ) ) ] );
                    sHeader := sHeader + '"Ult. Baja",';
                    sData := sData + Format( '%s,', [ FormatDateTime( K_XLS_DATE, GetFecha( sLine ) ) ] );
                    sHeader := sHeader + '"U. Tarjeta",';
                    sData := sData + Format( '%s,', [ FormatDateTime( K_XLS_DATE, GetFecha( sLine ) ) ] );
                    sHeader := sHeader + '"Ultima N�mina Calculada Total",';
                    sData := sData + Format( '%s,', [ GetData( sLine, K_LEN_PERIODO ) ] );
               end;
          end;
     end;
end;

procedure TSystemDataReader.ExportarClick(Sender: TObject);
var
   oCursor: TCursor;
   sFileName: String;
begin
     with SaveDialog do
     begin
          Filter := 'Textos (*.txt)|*.txt|Todos (*.*)|*.*';
          FileName := ChangeFileExt( GetFileNameSource, '.txt' );
          if Execute then
          begin
               sFileName := FileName;
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  with Datos do
                  begin
                       with Lines do
                       begin
                            SaveToFile( sFileName );
                       end;
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
               CallNotePad( sFileName );
          end;
     end;
end;

procedure TSystemDataReader.ExcelClick(Sender: TObject);
var
   oCursor: TCursor;
   sFileName, sHeader, sData: String;
   FOutput: TStrings;
begin
     with SaveDialog do
     begin
          Filter := 'Comma-Separated (*.csv)|*.csv|Todos (*.*)|*.*';
          FileName := ChangeFileExt( GetFileNameSource, '.txt' );
          if Execute then
          begin
               sFileName := FileName;
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  sHeader := '';
                  sData := '';
                  ScanData( 'Empresa #:', sHeader, sData );
                  ScanData( 'Empleados:', sHeader, sData );
                  ScanData( 'Licencias:', sHeader, sData );
                  ScanData( '# de Sentinel', sHeader, sData );
                  ScanData( 'Version:', sHeader, sData );
                  ScanData( 'Kit Distribuidor:', sHeader, sData );
                  ScanData( 'Plataforma:', sHeader, sData );
                  ScanData( 'Base De Datos:', sHeader, sData );
                  GetTotals( sHeader, sData );
                  FOutput := TStringList.Create;
                  try
                     with FOutput do
                     begin
                          Add( ZetaCommonTools.CortaUltimo( sHeader ) );
                          Add( ZetaCommonTools.CortaUltimo( sData ) );
                          SaveToFile( sFileName );
                     end;
                  finally
                         FreeAndNil( FOutput );
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
               CallNotePad( sFileName );
          end;
     end;
end;

end.
