object dmDatosSistema: TdmDatosSistema
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 192
  Top = 114
  Height = 178
  Width = 340
  object dbSentinel: TIB_Database
    LoginUsernamePrefix = 'SYSDBA'
    Params.Strings = (
      'SERVER=TRESS_2001'
      'PATH=E:\Admon\Sentinel\sentinel.gdb'
      'PROTOCOL=TCP/IP'
      'USER NAME=SYSDBA')
    Left = 24
    Top = 16
  end
  object qryInsert: TIB_Query
    DatabaseName = 'TRESS_2001:E:\Admon\Sentinel\sentinel.gdb'
    IB_Connection = dbSentinel
    SQL.Strings = (
      'insert into AUDIT( SI_FOLIO,'
      '                   SN_NUMERO,'
      '                   AD_FOLIO,'
      '                   AD_FECHA,'
      '                   US_CODIGO,'
      '                   AD_VERSION,'
      '                   AD_NUM_EMP,'
      '                   AD_USERS,'
      '                   AD_SQL_BD,'
      '                   AD_PLATFORM,'
      '                   AD_KIT_DIS,'
      '                   AD_ARCHIVO ) values ('
      '                   :SI_FOLIO,'
      '                   :SN_NUMERO,'
      '                   :AD_FOLIO,'
      '                   :AD_FECHA,'
      '                   :US_CODIGO,'
      '                   :AD_VERSION,'
      '                   :AD_NUM_EMP,'
      '                   :AD_USERS,'
      '                   :AD_SQL_BD,'
      '                   :AD_PLATFORM,'
      '                   :AD_KIT_DIS,'
      '                   :AD_ARCHIVO )')
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    BufferSynchroFlags = []
    FetchWholeRows = True
    Left = 96
    Top = 16
  end
  object qryFolio: TIB_Query
    DatabaseName = 'TRESS_2001:E:\Admon\Sentinel\sentinel.gdb'
    IB_Connection = dbSentinel
    SQL.Strings = (
      'select MAX( AD_FOLIO ) from AUDIT where '
      '( SI_FOLIO = :Sistema ) and'
      '( SN_NUMERO = :Sentinel )')
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    BufferSynchroFlags = []
    FetchWholeRows = True
    Left = 237
    Top = 16
  end
  object qryCompany: TIB_Query
    DatabaseName = 'TRESS_2001:E:\Admon\Sentinel\sentinel.gdb'
    IB_Connection = dbSentinel
    SQL.Strings = (
      'insert into AUDIT_CIA( SI_FOLIO,'
      '                       SN_NUMERO,'
      '                       AD_FOLIO,'
      '                       CM_CODIGO,'
      '                       CM_NOMBRE,'
      '                       AC_ACTIVOS,'
      '                       AC_INACTIV,'
      '                       AC_TOTALES,'
      '                       AC_NIVEL0,'
      '                       AC_DB_NAME,'
      '                       AC_ULT_ALT,'
      '                       AC_ULT_BAJ,'
      '                       AC_ULT_TAR,'
      '                       AC_ULT_NOM ) values ('
      '                       :SI_FOLIO,'
      '                       :SN_NUMERO,'
      '                       :AD_FOLIO,'
      '                       :CM_CODIGO,'
      '                       :CM_NOMBRE,'
      '                       :AC_ACTIVOS,'
      '                       :AC_INACTIV,'
      '                       :AC_TOTALES,'
      '                       :AC_NIVEL0,'
      '                       :AC_DB_NAME,'
      '                       :AC_ULT_ALT,'
      '                       :AC_ULT_BAJ,'
      '                       :AC_ULT_TAR,'
      '                       :AC_ULT_NOM )')
    ColorScheme = False
    MasterSearchFlags = [msfOpenMasterOnOpen, msfSearchAppliesToMasterOnly]
    BufferSynchroFlags = []
    FetchWholeRows = True
    Left = 170
    Top = 16
  end
  object qryVerificar: TIBOQuery
    Params = <>
    DatabaseName = 'TRESS_2001:E:\Admon\Sentinel\sentinel.gdb'
    IB_Connection = dbSentinel
    RecordCountAccurate = True
    FieldOptions = []
    Left = 96
    Top = 72
  end
  object qryAudit: TIBOQuery
    Params = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AD_FOLIO'
        ParamType = ptUnknown
      end>
    DatabaseName = 'TRESS_2001:E:\Admon\Sentinel\sentinel.gdb'
    IB_Connection = dbSentinel
    RecordCountAccurate = True
    AfterOpen = qryAuditAfterOpen
    SQL.Strings = (
      'select SI_FOLIO, SN_NUMERO, AD_FOLIO, AD_FECHA, US_CODIGO,'
      'AD_VERSION, AD_NUM_EMP, AD_USERS, AD_SQL_BD, AD_PLATFORM,'
      'AD_KIT_DIS, AD_ARCHIVO from AUDIT where'
      '( SI_FOLIO = :SI_FOLIO ) and'
      '( SN_NUMERO = :SN_NUMERO ) and'
      '( AD_FOLIO = :AD_FOLIO )')
    FieldOptions = []
    Left = 173
    Top = 72
  end
  object qryAuditCIA: TIBOQuery
    Params = <
      item
        DataType = ftUnknown
        Name = 'SI_FOLIO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SN_NUMERO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AD_FOLIO'
        ParamType = ptUnknown
      end>
    DatabaseName = 'TRESS_2001:E:\Admon\Sentinel\sentinel.gdb'
    IB_Connection = dbSentinel
    RecordCountAccurate = True
    AfterOpen = qryAuditCIAAfterOpen
    SQL.Strings = (
      'select SI_FOLIO, SN_NUMERO, AD_FOLIO,'
      'CM_CODIGO, CM_NOMBRE, AC_ACTIVOS, AC_INACTIV, AC_TOTALES,'
      
        'AC_NIVEL0, AC_DB_NAME, AC_ULT_ALT, AC_ULT_BAJ, AC_ULT_TAR, AC_UL' +
        'T_NOM'
      'from AUDIT_CIA where'
      '( SI_FOLIO = :SI_FOLIO ) and'
      '( SN_NUMERO = :SN_NUMERO ) and'
      '( AD_FOLIO = :AD_FOLIO )'
      'order by CM_CODIGO')
    FieldOptions = []
    Left = 237
    Top = 72
  end
end
