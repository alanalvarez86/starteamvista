unit DDatosSistema;

interface

uses SysUtils, Classes, Controls,
     IB_Components, DB, IBODataset,
     FAutoClasses;

{$define TEST}

type
  TDatosCompany = class( TObject )
  private
    { Private declarations }
    FEmpleadosTotales: Integer;
    FEmpleadosActivos: Integer;
    FEmpleadosInactivos: Integer;
    FCodigo: String;
    FConfidencialidad: String;
    FEmpresa: String;
    FBaseDeDatos: String;
    FUltimaNominaCalculadaTotal: String;
    FUltimaTarjeta: TDate;
    FUltimaAlta: TDate;
    FUltimaBaja: TDate;
  public
    { Public declarations }
    property Codigo: String read FCodigo write FCodigo;
    property Nombre: String read FEmpresa write FEmpresa;
    property EmpleadosActivos: Integer read FEmpleadosActivos write FEmpleadosActivos;
    property EmpleadosInactivos: Integer read FEmpleadosInactivos write FEmpleadosInactivos;
    property EmpleadosTotales: Integer read FEmpleadosTotales write FEmpleadosTotales;
    property Confidencialidad: String read FConfidencialidad write FConfidencialidad;
    property BaseDeDatos: String read FBaseDeDatos write FBaseDeDatos;
    property UltimaAlta: TDate read FUltimaAlta write FUltimaAlta;
    property UltimaBaja: TDate read FUltimaBaja write FUltimaBaja;
    property UltimaTarjeta: TDate read FUltimaTarjeta write FUltimaTarjeta;
    property UltimaNominaCalculadaTotal: String read FUltimaNominaCalculadaTotal write FUltimaNominaCalculadaTotal;
  end;
  TDatosSistema = class( TObject )
  private
    { Private declarations }
    FEsKit: Boolean;
    FEmpleados: Integer;
    FEmpresa: Integer;
    FSentinel: Integer;
    FUsuarios: Integer;
    FVersion: String;
    FPlataforma: TPlataforma;
    FBaseDeDatos: TSQLEngine;
    FLista: TList;
    FDatos: TStrings;
    FFolio: Integer;
    FStatusMsg: String;
    FRecoleccion: TDate;
    FArchivo: String;
    function DataAsInteger(const sToken: String): Integer;
    function DataAsString(const sToken: String): String;
    function GetCompany(Index: Integer): TDatosCompany;
    function GetData(var sData: String; const iCharacters: Integer): String;
    function GetFecha(var sData: String): TDate;
    function GetTotal(var sData: String): Integer;
    function LoadCompanies: Boolean;
  protected
    { Protected declarations }
   public
    { Public declarations }
    property Archivo: String read FArchivo write FArchivo;
    property Company[ Index: Integer ]: TDatosCompany read GetCompany;
    property Empresa: Integer read FEmpresa write FEmpresa;
    property Empleados: Integer read FEmpleados write FEmpleados;
    property Folio: Integer read FFolio write FFolio;
    property Usuarios: Integer read FUsuarios write FUsuarios;
    property Sentinel: Integer read FSentinel write FSentinel;
    property Version: String read FVersion write FVersion;
    property EsKit: Boolean read FEsKit write FEsKit;
    property Plataforma: TPlataforma read FPlataforma write FPlataforma;
    property BaseDeDatos: TSQLEngine read FBaseDeDatos write FBaseDeDatos;
    property Recoleccion: TDate read FRecoleccion write FRecoleccion;
    property StatusMsg: String read FStatusMsg;
    constructor Create;
    destructor Destroy; override;
    function AgregarEmpresa: TDatosCompany;
    function Cargar(const sArchivo: String): Boolean;
    function Count: Integer;
    procedure Clear;
  end;
  TdmDatosSistema = class(TDataModule)
    dbSentinel: TIB_Database;
    qryInsert: TIB_Query;
    qryFolio: TIB_Query;
    qryCompany: TIB_Query;
    qryVerificar: TIBOQuery;
    qryAudit: TIBOQuery;
    qryAuditCIA: TIBOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure qryAuditAfterOpen(DataSet: TDataSet);
    procedure qryAuditCIAAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FSistema: TDatosSistema;
    FStatusMsg: String;
    function GetFolio: Boolean;
    procedure FechaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PlatformGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SQLDatabaseGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure EsKitDistribuidorGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  public
    { Public declarations }
    property DatosSistema: TDatosSistema read FSistema;
    property StatusMsg: String read FStatusMsg;
    function Cargar( const sArchivo: String ): Boolean;
    function Conectar: Boolean;
    function Desconectar: Boolean;
    function AbrirQuery( SQLScript: TStrings ): Boolean;
    function OpenAudit: Boolean;
  end;

var
  dmDatosSistema: TdmDatosSistema;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools;

{$R *.dfm}

{ *** TDatosSistema *** }

constructor TDatosSistema.Create;
begin
     FLista := TList.Create;
     FDatos := TStringList.Create;
end;

destructor TDatosSistema.Destroy;
begin
     FreeAndNil( FDatos );
     FreeAndNil( FLista );
     inherited Destroy;
end;

function TDatosSistema.AgregarEmpresa: TDatosCompany;
begin
     Result := TDatosCompany.Create;
     FLista.Add( Result );
end;

function TDatosSistema.GetCompany( Index: Integer ): TDatosCompany;
begin
     with FLista do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TDatosCompany( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TDatosSistema.Clear;
var
   i: Integer;
begin
     with FLista do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Company[ i ].Free;
          end;
          Clear;
     end;
end;

function TDatosSistema.Count: Integer;
begin
     Result := FLista.Count;
end;

function TDatosSistema.DataAsString( const sToken: String ): String;
var
   i, iPos: Integer;
   sLine: String;
begin
     Result := '';
     with FDatos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               sLine := Strings[ i ];
               iPos := Pos( sToken, sLine );
               if ( iPos > 0 ) then
               begin
                    Result := Trim( Copy( sLine, ( iPos + Length( sToken ) ), ( Length( sLine ) - Length( sToken ) - iPos + 1 ) ) );
                    Break;
               end;
          end;
     end;
end;

function TDatosSistema.DataAsInteger( const sToken: String ): Integer;
begin
     Result := StrToIntDef( DataAsString( sToken ), 0 );
end;

function TDatosSistema.GetData( var sData: String; const iCharacters: Integer ): String;
var
   iLen, iPos: Integer;
begin
     iLen := Length( sData );
     iPos := 1;
     { Buscar 1er. Caracter Que No Sea Espacio En Blanco }
     Result := Trim( Copy( sData, iPos, iCharacters ) );
     sData := Copy( sData, ( iCharacters + 2 ), ( iLen - iCharacters + 1 ) );
end;

function TDatosSistema.GetFecha( var sData: String ): TDate;
const
     K_LEN_FECHA = 11;
var
   sFecha, sMonth: String;
   iDay, iMonth, iYear: Integer;
begin
     sFecha := GetData( sData, K_LEN_FECHA );
     if ZetaCommonTools.StrVacio( sFecha ) then
        Result := ZetaCommonClasses.NullDateTime
     else
     begin
          iDay := StrToIntDef( Copy( sFecha, 1, 2 ), 0 );
          sMonth := UpperCase( Copy( sFecha, 4, 3 ) );
          if ( sMonth = 'ENE' ) or ( sMonth = 'JAN' ) then
             iMonth := 1
          else
          if ( sMonth = 'FEB' ) then
             iMonth := 2
          else
          if ( sMonth = 'MAR' ) then
             iMonth := 3
          else
          if ( sMonth = 'ABR' ) or ( sMonth = 'APR' ) then
             iMonth := 4
          else
          if ( sMonth = 'MAY' ) then
             iMonth := 5
          else
          if ( sMonth = 'JUN' ) then
             iMonth := 6
          else
          if ( sMonth = 'JUL' ) then
             iMonth := 7
          else
          if ( sMonth = 'AGO' ) or ( sMonth = 'AUG' ) then
             iMonth := 8
          else
          if ( sMonth = 'SEP' ) then
             iMonth := 9
          else
          if ( sMonth = 'OCT' ) then
             iMonth := 10
          else
          if ( sMonth = 'NOV' ) then
             iMonth := 11
          else
          if ( sMonth = 'DIC' ) or ( sMonth = 'DEC' ) then
             iMonth := 12
          else
              iMonth := 0;
          iYear := StrToIntDef( Copy( sFecha, 8, 4 ), 0 );
          Result := ZetaCommonTools.CodificaFecha( iYear, iMonth, iDay );
     end;
end;

function TDatosSistema.GetTotal( var sData: String ): Integer;
const
     K_LEN_TOTAL = 9;
begin
     Result := StrToIntDef( GetData( sData, K_LEN_TOTAL ), 0 );
end;

function TDatosSistema.LoadCompanies: Boolean;
const
     K_LEN_CODIGO = 10;
     K_LEN_EMPRESA = 50;
     K_LEN_NIVEL0 = 6;
     K_LEN_DATABASE = 50;
     K_LEN_PERIODO = 40;
var
   iPtr, iFound: Integer;
   sLine: String;
begin
     Self.Clear;
     with FDatos do
     begin
          iFound := 0;
          for iPtr := ( Count - 1 ) downto 0 do
          begin
               sLine := Trim( Strings[ iPtr ] );
               if ZetaCommonTools.StrLleno( sLine ) then
               begin
                    if ( iFound >= 2 ) then
                       Break
                    else
                        Inc( iFound );
               end;
          end;
          if ( iPtr >= 0 ) then
          begin
               repeat
                     with AgregarEmpresa do
                     begin
                          Codigo := GetData( sLine, K_LEN_CODIGO );
                          Nombre := GetData( sLine, K_LEN_EMPRESA );
                          EmpleadosActivos := GetTotal( sLine );
                          EmpleadosInactivos := GetTotal( sLine );
                          EmpleadosTotales := GetTotal( sLine );
                          Confidencialidad := GetData( sLine, K_LEN_NIVEL0 );
                          BaseDeDatos := GetData( sLine, K_LEN_DATABASE );
                          UltimaAlta := GetFecha( sLine );
                          UltimaBaja := GetFecha( sLine );
                          UltimaTarjeta := GetFecha( sLine );
                          UltimaNominaCalculadaTotal := GetData( sLine, K_LEN_PERIODO );
                     end;
                     Dec( iPtr );
                     if ( iPtr >= 0 ) then
                        sLine := Trim( Strings[ iPtr ] );
               until ( iPtr < 0 ) or ( Pos( '------', sLine ) > 0 );
          end;
          Result := ( Count > 0 );
          if not Result then
             FStatusMsg := 'No Hay Compa��as';
     end;
end;

function TDatosSistema.Cargar( const sArchivo: String ): Boolean;
var
   i: Integer;
   ePlataforma: TPlataforma;
   eSQLEngine: TSQLEngine;
   sValue: String;
begin
     try
        with FDatos do
        begin
             LoadFromFile( sArchivo );
             for i := ( Count - 1 ) downto 0 do
             begin
                  Strings[ i ] := ZetaServerTools.Decrypt( Strings[ i ] );
             end;
        end;
        Recoleccion := FileDateToDateTime( FileAge( sArchivo ) );
        Archivo := sArchivo;
        Empresa := DataAsInteger( 'Empresa #:' );
        Empleados := DataAsInteger( 'Empleados:' );
        Usuarios := DataAsInteger( 'Licencias:' );
        Sentinel := DataAsInteger( '# de Sentinel' );
        Version := DataAsString( 'Version:' );
        EsKit := ( UpperCase( DataAsString( 'Kit Distribuidor:' ) ) = 'SI' );
        sValue := DataAsString( 'Plataforma:' );
        for ePlataforma := Low( TPlataforma ) to High( TPlataforma ) do
        begin
             if ( FAutoClasses.GetPlatformName( ePlataforma ) = sValue ) then
                Break;
        end;
        Plataforma := ePlataforma;
        sValue := DataAsString( 'Base De Datos:' );
        for eSQLEngine := Low( TSQLEngine ) to High( TSQLEngine ) do
        begin
             if ( FAutoClasses.GetSQLEngineName( eSQLEngine ) = sValue ) then
                Break;
        end;
        BaseDeDatos := eSQLEngine;
        Result := LoadCompanies;
     except
           on Error: Exception do
           begin
                FStatusMsg := 'Error Al Cargar Archivo: ' + Error.Message;
                Result := False;
           end;
     end;
end;

{ *** TdmDatosSistema *** }

procedure TdmDatosSistema.DataModuleCreate(Sender: TObject);
begin
     FSistema := TDatosSistema.Create;
end;

procedure TdmDatosSistema.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FSistema );
end;

procedure TdmDatosSistema.EsKitDistribuidorGetText( Sender: TField; var Text: String; DisplayText: Boolean );
const
     aIntBool: array[ 0..1 ] of Boolean = ( False, True  );
begin
     if DisplayText then
     begin
          if Assigned( Sender.DataSet ) then
          begin
               if Sender.DataSet.IsEmpty then
                  Text := ''
               else
               begin
                    Text := ZetaCommonTools.BoolAsSiNo( Sender.AsInteger = 1 );
               end;
          end;
     end;
end;

procedure TdmDatosSistema.SQLDatabaseGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Assigned( Sender.DataSet ) then
          begin
               if Sender.DataSet.IsEmpty then
                  Text := ''
               else
               begin
                    Text := FAutoClasses.GetSQLEngineName( TSQLEngine( Sender.AsInteger ) );
               end;
          end;
     end;
end;

procedure TdmDatosSistema.PlatformGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Assigned( Sender.DataSet ) then
          begin
               if Sender.DataSet.IsEmpty then
                  Text := ''
               else
               begin
                    Text := FAutoClasses.GetPlatformName( TPlataforma( Sender.AsInteger ) );
               end;
          end;
     end;
end;

procedure TdmDatosSistema.FechaGetText( Sender: TField; var Text: String; DisplayText: Boolean );
var
   dValue: TDate;
begin
     if DisplayText then
     begin
          if Assigned( Sender.DataSet ) then
          begin
               if Sender.DataSet.IsEmpty then
                  Text := ''
               else
               begin
                    dValue := Sender.AsDateTime;
                    if ( dValue <= 0 ) then
                       Text := ''
                    else
                       Text := FormatDateTime( 'dd/mmm/yyyy', dValue );
               end;
          end;
     end;
end;

function TdmDatosSistema.Conectar: Boolean;
begin
     with dbSentinel do
     begin
          try
             Connected := False;
             UserName := 'SYSDBA';
             Password := 'm';
             {$ifdef TEST}
             DatabaseName := 'DELL-ERAMIREZ:E:\IBDatos\SUA\sentinel.gdb';
             {$else}
             DatabaseName := 'TRESS_2001:E:\Admon\Sentinel\sentinel.gdb';
             {$endif}
             Connected := True;
          except
                on Error: Exception do
                begin
                     FStatusMsg := 'Error Al Conectar A Base De Datos: ' + Error.Message;
                end;
          end;
          Result := Connected;
     end;
end;

function TdmDatosSistema.Desconectar: Boolean;
begin
     with dbSentinel do
     begin
          try
             Connected := False;
          except
                on Error: Exception do
                begin
                     FStatusMsg := 'Error Al Conectar A Base De Datos: ' + Error.Message;
                end;
          end;
          Result := Connected;
     end;
end;

function TdmDatosSistema.GetFolio: Boolean;
begin
     with FSistema do
     begin
          try
             with qryFolio do
             begin
                  Active := False;
                  ParamByName( 'Sistema' ).AsInteger := Empresa;
                  ParamByName( 'Sentinel' ).AsInteger := Sentinel;
                  Active := True;
                  Folio := Fields[ 0 ].AsInteger + 1;
                  Active := False;
             end;
          except
                on Error: Exception do
                begin
                     Folio := -1;
                     FStatusMsg := 'Error Al Obtener Folio: ' + Error.Message;
                end;
          end;
          Result := ( Folio > 0 );
     end;
end;

function TdmDatosSistema.Cargar( const sArchivo: String ): Boolean;
const
     aIntBool: array[ False..True ] of Integer = ( 0, 1  );
var
   i: Integer;
begin
     Result := FSistema.Cargar( sArchivo );
     if Result then
     begin
          Result := GetFolio;
          if Result then
          begin
               try
                  with qryInsert do
                  begin
                       with FSistema do
                       begin
                            ParamByName( 'SI_FOLIO' ).AsInteger := Empresa;
                            ParamByName( 'SN_NUMERO' ).AsInteger := Sentinel;
                            ParamByName( 'AD_FOLIO' ).AsInteger := Folio;
                            ParamByName( 'AD_FECHA' ).AsDate := Recoleccion;
                            ParamByName( 'US_CODIGO' ).AsInteger := 0;
                            ParamByName( 'AD_VERSION' ).AsString := Version;
                            ParamByName( 'AD_NUM_EMP' ).AsInteger := Empleados;
                            ParamByName( 'AD_USERS' ).AsInteger := Usuarios;
                            ParamByName( 'AD_SQL_BD' ).AsInteger := Ord( BaseDeDatos );
                            ParamByName( 'AD_PLATFORM' ).AsInteger := Ord( Plataforma );
                            ParamByName( 'AD_KIT_DIS' ).AsInteger := aIntBool[ EsKit ];
                            ParamByName( 'AD_ARCHIVO' ).AsString := Archivo;
                       end;
                       ExecSQL;
                  end;
                  with FSistema do
                  begin
                       with qryCompany do
                       begin
                            ParamByName( 'SI_FOLIO' ).AsInteger := Empresa;
                            ParamByName( 'SN_NUMERO' ).AsInteger := Sentinel;
                            ParamByName( 'AD_FOLIO' ).AsInteger := Folio;
                       end;
                       for i := 0 to ( Count - 1 ) do
                       begin
                            with qryCompany do
                            begin
                                 with Company[ i ] do
                                 begin
                                      ParamByName( 'CM_CODIGO' ).AsString := Codigo;
                                      ParamByName( 'CM_NOMBRE' ).AsString := Nombre;
                                      ParamByName( 'AC_ACTIVOS' ).AsInteger := EmpleadosActivos;
                                      ParamByName( 'AC_INACTIV' ).AsInteger := EmpleadosInactivos;
                                      ParamByName( 'AC_TOTALES' ).AsInteger := EmpleadosTotales;
                                      ParamByName( 'AC_NIVEL0' ).AsString := Confidencialidad;
                                      ParamByName( 'AC_DB_NAME' ).AsString := BaseDeDatos;
                                      ParamByName( 'AC_ULT_ALT' ).AsDate := UltimaAlta;
                                      ParamByName( 'AC_ULT_BAJ' ).AsDate := UltimaBaja;
                                      ParamByName( 'AC_ULT_TAR' ).AsDate := UltimaTarjeta;
                                      ParamByName( 'AC_ULT_NOM' ).AsString := UltimaNominaCalculadaTotal;
                                 end;
                                 ExecSQL;
                            end;
                       end;
                  end;
               except
                     on Error: Exception do
                     begin
                          Result := False;
                          FStatusMsg := 'Error Al Agregar Datos: ' + Error.Message;
                     end;
               end;
          end;
     end;
end;

function TdmDatosSistema.AbrirQuery( SQLScript: TStrings ): Boolean;
begin
     Result := False;
     if Conectar then
     begin
          try
             with qryVerificar do
             begin
                  Active := False;
                  SQL.Assign( SQLScript );
                  Active := True;
                  Result := Active;
             end;
          except
                on Error: Exception do
                begin
                     FStatusMsg := 'Error Al Verificar: ' + Error.Message;
                end;
          end;
     end;
end;


function TdmDatosSistema.OpenAudit: Boolean;
var
   iEmpresa, iSentinel, iFolio: Integer;
begin
     Result := False;
     try
        with qryVerificar do
        begin
             iEmpresa := FieldByName( 'SISTEMA' ).AsInteger;
             iSentinel := FieldByName( 'SENTINEL' ).AsInteger;
             iFolio := FieldByName( 'FOLIO' ).AsInteger;
        end;
        with qryAudit do
        begin
             Active := False;
             ParamByName( 'SI_FOLIO' ).AsInteger := iEmpresa;
             ParamByName( 'SN_NUMERO' ).AsInteger := iSentinel;
             ParamByName( 'AD_FOLIO' ).AsInteger := iFolio;
             Active := True;
        end;
        with qryAuditCIA do
        begin
             Active := False;
             ParamByName( 'SI_FOLIO' ).AsInteger := iEmpresa;
             ParamByName( 'SN_NUMERO' ).AsInteger := iSentinel;
             ParamByName( 'AD_FOLIO' ).AsInteger := iFolio;
             Active := True;
        end;
        Result := qryAudit.Active and qryAuditCIA.Active;
     except
           on Error: Exception do
           begin
                FStatusMsg := 'Error Al Abrir Datos De Empresa: ' + Error.Message;
           end;
     end;
end;

procedure TdmDatosSistema.qryAuditAfterOpen(DataSet: TDataSet);
begin
     with qryAudit do
     begin
          FieldByName( 'AD_SQL_BD' ).OnGetText := SQLDatabaseGetText;
          FieldByName( 'AD_PLATFORM' ).OnGetText := PlatformGetText;
          FieldByName( 'AD_KIT_DIS' ).OnGetText := EsKitDistribuidorGetText;
     end;
end;

procedure TdmDatosSistema.qryAuditCIAAfterOpen(DataSet: TDataSet);
begin
     with qryAuditCIA do
     begin
          FieldByName( 'AC_ULT_ALT' ).OnGetText := FechaGetText;
          FieldByName( 'AC_ULT_BAJ' ).OnGetText := FechaGetText;
          FieldByName( 'AC_ULT_TAR' ).OnGetText := FechaGetText;
     end;
end;

end.
