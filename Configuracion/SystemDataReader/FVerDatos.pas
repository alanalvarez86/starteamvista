unit FVerDatos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, DB, Grids, DBGrids, ZetaDBGrid,
  ZetaDBTextBox;

type
  TVerDatos = class(TForm)
    PanelInferior: TPanel;
    Cerrar: TBitBtn;
    PanelSuperior: TPanel;
    EmpresasGB: TGroupBox;
    ZetaDBGrid: TZetaDBGrid;
    dsAudit: TDataSource;
    dsAuditCIA: TDataSource;
    SI_FOLIOlbl: TLabel;
    SN_NUMEROlbl: TLabel;
    AD_FOLIOlbl: TLabel;
    AD_FECHAlbl: TLabel;
    US_CODIGOlbl: TLabel;
    AD_VERSIONlbl: TLabel;
    AD_NUM_EMPlbl: TLabel;
    AD_USERSlbl: TLabel;
    AD_SQL_BDlbl: TLabel;
    AD_PLATFORMlbl: TLabel;
    AD_KIT_DISlbl: TLabel;
    AD_ARCHIVOlbl: TLabel;
    SI_FOLIO: TZetaDBTextBox;
    SN_NUMERO: TZetaDBTextBox;
    AD_FOLIO: TZetaDBTextBox;
    AD_FECHA: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    AD_VERSION: TZetaDBTextBox;
    AD_NUM_EMP: TZetaDBTextBox;
    AD_PLATFORM: TZetaDBTextBox;
    AD_KIT_DIS: TZetaDBTextBox;
    AD_ARCHIVO: TZetaDBTextBox;
    AD_USERS: TZetaDBTextBox;
    AD_SQL_BD: TZetaDBTextBox;
    procedure CerrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure VerDatosArchivo;

implementation

uses DDatosSistema;

var
  VerDatos: TVerDatos;

procedure VerDatosArchivo;
begin
     if not Assigned( VerDatos ) then
        VerDatos := TVerDatos.Create( Application );
     with VerDatos do
     begin
          ShowModal;
     end;
end;

{$R *.dfm}

procedure TVerDatos.FormShow(Sender: TObject);
begin
     with dmDatosSistema do
     begin
          dsAudit.DataSet := qryAudit;
          dsAuditCIA.DataSet := qryAuditCIA;
     end;
end;

procedure TVerDatos.CerrarClick(Sender: TObject);
begin
     Close;
end;

end.
