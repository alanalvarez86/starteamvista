object VerDatos: TVerDatos
  Left = 227
  Top = 286
  Width = 1015
  Height = 635
  Caption = 'Ver Datos De Un Archivo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelInferior: TPanel
    Left = 0
    Top = 560
    Width = 1007
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      1007
      41)
    object Cerrar: TBitBtn
      Left = 926
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Cerrar Esta Ventana'
      Anchors = [akTop, akRight]
      Caption = '&Cerrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = CerrarClick
      Kind = bkClose
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 1007
    Height = 257
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object SI_FOLIOlbl: TLabel
      Left = 46
      Top = 11
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa #:'
    end
    object SN_NUMEROlbl: TLabel
      Left = 51
      Top = 30
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Sentinel #:'
    end
    object AD_FOLIOlbl: TLabel
      Left = 66
      Top = 51
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio #:'
    end
    object AD_FECHAlbl: TLabel
      Left = 22
      Top = 72
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = 'Recolecci'#243'n en:'
    end
    object US_CODIGOlbl: TLabel
      Left = 27
      Top = 93
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Registrado Por:'
    end
    object AD_VERSIONlbl: TLabel
      Left = 62
      Top = 114
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Versi'#243'n:'
    end
    object AD_NUM_EMPlbl: TLabel
      Left = 45
      Top = 134
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleados:'
    end
    object AD_USERSlbl: TLabel
      Left = 56
      Top = 156
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usuarios:'
    end
    object AD_SQL_BDlbl: TLabel
      Left = 27
      Top = 176
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Base de Datos:'
    end
    object AD_PLATFORMlbl: TLabel
      Left = 48
      Top = 197
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Plataforma:'
    end
    object AD_KIT_DISlbl: TLabel
      Left = 70
      Top = 219
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Es Kit:'
    end
    object AD_ARCHIVOlbl: TLabel
      Left = 60
      Top = 239
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Archivo:'
    end
    object SI_FOLIO: TZetaDBTextBox
      Left = 103
      Top = 8
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'SI_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SI_FOLIO'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object SN_NUMERO: TZetaDBTextBox
      Left = 103
      Top = 28
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'SN_NUMERO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SN_NUMERO'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_FOLIO: TZetaDBTextBox
      Left = 103
      Top = 49
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'AD_FOLIO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_FOLIO'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_FECHA: TZetaDBTextBox
      Left = 103
      Top = 70
      Width = 129
      Height = 17
      AutoSize = False
      Caption = 'AD_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_FECHA'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object US_CODIGO: TZetaDBTextBox
      Left = 103
      Top = 91
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'US_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_CODIGO'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_VERSION: TZetaDBTextBox
      Left = 103
      Top = 112
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'AD_VERSION'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_VERSION'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_NUM_EMP: TZetaDBTextBox
      Left = 103
      Top = 132
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'AD_NUM_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_NUM_EMP'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_PLATFORM: TZetaDBTextBox
      Left = 103
      Top = 195
      Width = 129
      Height = 17
      AutoSize = False
      Caption = 'AD_PLATFORM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_PLATFORM'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_KIT_DIS: TZetaDBTextBox
      Left = 103
      Top = 216
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'AD_KIT_DIS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_KIT_DIS'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_ARCHIVO: TZetaDBTextBox
      Left = 103
      Top = 237
      Width = 842
      Height = 17
      AutoSize = False
      Caption = 'AD_ARCHIVO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_ARCHIVO'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_USERS: TZetaDBTextBox
      Left = 103
      Top = 153
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'AD_USERS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_USERS'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AD_SQL_BD: TZetaDBTextBox
      Left = 103
      Top = 174
      Width = 129
      Height = 17
      AutoSize = False
      Caption = 'AD_SQL_BD'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AD_SQL_BD'
      DataSource = dsAudit
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object EmpresasGB: TGroupBox
    Left = 0
    Top = 257
    Width = 1007
    Height = 303
    Align = alClient
    Caption = ' Empresas '
    TabOrder = 2
    object ZetaDBGrid: TZetaDBGrid
      Left = 2
      Top = 15
      Width = 1003
      Height = 286
      Align = alClient
      DataSource = dsAuditCIA
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CM_CODIGO'
          Title.Caption = 'C'#243'digo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CM_NOMBRE'
          Title.Caption = 'Empresa'
          Width = 150
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AC_ACTIVOS'
          Title.Alignment = taCenter
          Title.Caption = 'Activos'
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AC_INACTIV'
          Title.Alignment = taCenter
          Title.Caption = 'Bajas'
          Visible = True
        end
        item
          Alignment = taRightJustify
          Expanded = False
          FieldName = 'AC_TOTALES'
          Title.Alignment = taCenter
          Title.Caption = 'Tot. Empleados'
          Width = 85
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AC_NIVEL0'
          Title.Caption = 'Confidencialidad'
          Width = 88
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AC_DB_NAME'
          Title.Caption = 'Base de Datos'
          Width = 181
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AC_ULT_ALT'
          Title.Caption = 'Ultima Alta'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AC_ULT_BAJ'
          Title.Caption = 'Ultima Baja'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AC_ULT_TAR'
          Title.Caption = 'Ultima Tarjeta'
          Width = 79
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AC_ULT_NOM'
          Title.Caption = 'Ultima N'#243'mina'
          Width = 150
          Visible = True
        end>
    end
  end
  object dsAudit: TDataSource
    Left = 48
    Top = 64
  end
  object dsAuditCIA: TDataSource
    Left = 16
    Top = 96
  end
end
