unit FVerificar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ZetaDBGrid, DB, StdCtrls, Buttons, ExtCtrls;

type
  TVerificarLicencias = class(TForm)
    PanelInferior: TPanel;
    PanelSuperior: TPanel;
    PanelBotones: TPanel;
    QueryGB: TGroupBox;
    Query: TMemo;
    Abrir: TBitBtn;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaDBGrid;
    Splitter: TSplitter;
    Cerrar: TBitBtn;
    procedure FormDestroy(Sender: TObject);
    procedure AbrirClick(Sender: TObject);
    procedure ZetaDBGridDblClick(Sender: TObject);
    procedure CerrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  VerificarLicencias: TVerificarLicencias;

implementation

uses DDatosSistema,
     FVerDatos,
     ZetaDialogo;

{$R *.dfm}

procedure TVerificarLicencias.FormDestroy(Sender: TObject);
begin
     dmDatosSistema.Desconectar;
end;

procedure TVerificarLicencias.AbrirClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           Datasource.DataSet := nil;
           with dmDatosSistema do
           begin
                if AbrirQuery( Query.Lines ) then
                begin
                     Datasource.DataSet := qryVerificar;
                end
                else
                    ZetaDialogo.ZError( '� Error !', StatusMsg, 0 );
           end;
        except
              on Error: Exception do
              begin
                   ZetaDialogo.ZExcepcion( '� Error Al Abrir Query !', 'Error Al Abrir Query', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TVerificarLicencias.ZetaDBGridDblClick(Sender: TObject);
var
   oCursor: TCursor;
   lOk: Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        lOk := dmDatosSistema.OpenAudit;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
     begin
          FVerDatos.VerDatosArchivo;
     end
     else
         ZetaDialogo.ZError( '� Error !', dmDatosSistema.StatusMsg, 0 );
end;

procedure TVerificarLicencias.CerrarClick(Sender: TObject);
begin
     Close;
end;

end.
