unit BDEFunctions;

interface

uses Windows, SysUtils, Classes, DB, DBTables, BDE;

const
     CR_LF = Chr( 13 ) + Chr( 10 );

function GetExceptionInfo( Error: Exception ): String;
function BDECheckTableExists( hTmpDb: hDBIDb; const sTableName: String ): Boolean;
procedure BDEOpenTableList( hTmpDb: hDBIDb; Tablas: TStrings );
procedure BDEGetSPList( hTmpDb: hDBIDb; Lista: TStrings );
procedure BDEOpenFieldList( hTmpDb: hDBIDb; const sTableName: String; Campos: TStrings{$ifdef INTERBASE}; const lExcluirBlobs: Boolean = FALSE{$endif} );
procedure BDEBuildPrimaryKeyFieldList( hTmpDb: hDBIDb; const sTableName: String; Campos, CamposLlave: TStrings );

implementation

uses
    ZetaServerTools;

function GetExceptionInfo( Error: Exception ): String;
const
     DBIERR_KEYVIOL = 9729;
     DBIERR_FOREIGNKEYERR = 9733;
var
   i: Word;
begin
     Result := '';
     if ( Error is EDBEngineError ) then
     begin
          with Error as EDBEngineError do
          begin
               if ( ErrorCount > 0 ) then
               begin
                    case Errors[ 0 ].ErrorCode of
                         DBIERR_KEYVIOL: Result := '� C�digo Ya Existe !';
                    else
                        begin
                             for i := 0 to ( ErrorCount - 1 ) do
                             begin
                                  with Errors[ i ] do
                                  begin
                                       Result := Result +
                                                 CR_LF +
                                                 '( ' +
                                                 IntToStr( Category ) +
                                                 ':' +
                                                 IntToStr( ErrorCode ) +
                                                 ' ):' +
                                                 Message;
                                  end;
                             end;
                        end;
                    end;
               end
               else
                   Result := 'Error Tipo EDBEngineError Desconocido';
          end;
     end
     else
         Result := Error.Message;
end;

procedure BDEGetSPList( hTmpDb: hDBIDb; Lista: TStrings );
var
   hCur: hDBICur;
   Info: SPDesc;
   Result: DBIResult;
begin
     Check( DbiOpenSPList( hTmpDb, True, True, nil, hCur ) );
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             Check( DbiSetToBegin( hCur ) );
             Result := DBIERR_NONE;
             // While there are no errors, get RI information...
             while ( Result = DBIERR_NONE ) do
             begin
                  // Get the next RI record...
                  Result := DbiGetNextRecord( hCur, dbiNOLOCK, @Info, nil );
                  if ( Result <> DBIERR_EOF ) then
                  begin
                       // Make sure nothing out of the ordinary happened...
                       Check( Result );
                       Add( Info.szName );
                  end;
             end;
          finally
                 Check( DbiCloseCursor( hCur ) );
                 EndUpdate;
          end;
     end;
end;

procedure BDEGetRefIntegrity( hTmpDb: hDBIDb; const sTableName: String; Lines: TStrings );
var
   hCur: hDBICur;
   RIDesc: RINTDesc;
   Result: DBIResult;
   iValor: Byte;
   sValor: String;
begin
     // Get a cursor to the RI information...
     Check( DbiOpenRIntList( hTmpDb, {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}( sTableName ), nil, hCur ) );
     with Lines do
     begin
          try
             BeginUpdate;
             Clear;
             Check( DbiSetToBegin( hCur ) );
             Result := DBIERR_NONE;
             // While there are no errors, get RI information...
             while ( Result = DBIERR_NONE ) do
             begin
                  // Get the next RI record...
                  Result := DbiGetNextRecord( hCur, dbiNOLOCK, @RIDesc, nil );
                  if ( Result <> DBIERR_EOF ) then
                  begin
                       // Make sure nothing out of the ordinary happened...
                       Check( Result );
                       // Display information...
                       with RIDesc do
                       begin
                            Add( 'RI Number: ' + IntToStr( iRintNum ) );
                            Add( 'RI Name: ' + szRintName );
                            case eType of
                                 rintMASTER: Add( 'RI Type: MASTER' );
                                 rintDEPENDENT: Add( 'RI Type: DEPENDENT' );
                            else
                                Add( 'RI Type: UNKNOWN' );
                            end;
                            Add( 'RI Other Table Name: ' + szTblName );
                            case eModOp of
                                 rintRESTRICT: Add( 'RI Modify Qualifier: RESTRICT' );
                                 rintCASCADE: Add( 'RI Modify Qualifier: CASCADE' );
                            else
                                Add( 'RI Modify Qualifier: UNKNOWN' );
                            end;
                            case eDelOp of
                                 rintRESTRICT: Add( 'RI Delete Qualifier: RESTRICT' );
                                 rintCASCADE: Add( 'RI Delete Qualifier: CASCADE' );
                            else
                                Add( 'RI Delete Qualifier: UNKNOWN' );
                            end;
                            Add( 'RI Fields in Linking Key: ' + IntToStr( iFldCount ) );
                            sValor := '';
                            for iValor := 0 to ( iFldCount - 1 ) do
                                sValor := sValor + IntToStr( aiThisTabFld[ iValor ] ) + ', ';
                            SetLength( sValor, Length( sValor ) - 2 );
                            Add( 'RI Key Field Numbers in Table: ' + sValor );
                            sValor := '';
                            for iValor := 0 to ( iFldCount - 1 ) do
                                sValor := sValor + IntToStr( aiOthTabFld[ iValor ] ) + ', ';
                            SetLength( sValor, Length( sValor ) - 2 );
                            Add( 'RI Key Field Numbers in Other Table: ' + sValor );
                            Add( '' );
                       end;
                  end;
             end;
          finally
                 // All information was retrieved, close the in-memory table...
                 Check( DbiCloseCursor( hCur ) );
                 EndUpdate;
          end;
     end;
end;

function BDECheckTableExists( hTmpDb: hDBIDb; const sTableName: String ): Boolean;
var
  TmpCursor: hDBICur;
  ListDesc: TBLBaseDesc;
begin
     Result := False;
     Check( DbiOpenTableList( hTmpDb, False, False, '*.*', TmpCursor ) );
     while not Result and ( DbiGetNextRecord( TmpCursor, dbiNOLOCK, @ListDesc, nil ) = dbiErr_None ) do
     begin
          Result := ( ListDesc.szName = sTableName );
     end;
     Check( DbiCloseCursor( TmpCursor ) );
end;

procedure BDEOpenTableList( hTmpDb: hDBIDb; Tablas: TStrings );
var
  TmpCursor: hDBICur;
  ListDesc: TBLBaseDesc;
begin
     Check( DbiOpenTableList( hTmpDb, False, False, '*.*', TmpCursor ) );
     with Tablas do
     begin
          BeginUpdate;
          try
             Clear;
             while ( DbiGetNextRecord( TmpCursor, dbiNOLOCK, @ListDesc, nil ) = dbiErr_None ) do
             begin
                  {$ifdef MSSQL}
                  Add( Copy(ListDesc.szName, Pos('dbo.',ListDesc.szName)+4,MaxInt ));
                  {$endif}

                  {$ifdef INTERBASE}
                  Add( ListDesc.szName );
                  {$endif}
             end;
          finally
                 EndUpdate;
          end;
     end;
     Check( DbiCloseCursor( TmpCursor ) );
end;

procedure BDEOpenFieldList( hTmpDb: hDBIDb; const sTableName: String; Campos: TStrings{$ifdef INTERBASE}; const lExcluirBlobs: Boolean{$endif} );
var
   TmpCursor: hDBICur;
   Rslt: DBIResult;
   FieldDesc: FLDDesc;
begin
     Check( DbiOpenFieldList( hTmpDb, {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}( sTableName ), nil, False, TmpCursor ) );
     with Campos do
     begin
          BeginUpdate;
          try
             Clear;
             repeat
                   Rslt := DbiGetNextRecord( TmpCursor, dbiNOLOCK, @FieldDesc, nil );
                   if ( Rslt <> DBIERR_EOF) then
                   begin
                        if NoEsCampoLLavePortal(FieldDesc.szName){En interbase se excluyen los blobs ya que no se pueden importar}
                           {$ifdef INTERBASE}and ( not( FieldDesc.iFldType in [fldBLOB] ) or not lExcluirBlobs){$endif} then
                           Add( StrPas( FieldDesc.szName ) )
                   end;
             until ( Rslt <> DBIERR_NONE );
          finally
                 EndUpdate;
          end;
     end;
     Check( DbiCloseCursor( TmpCursor ) );
end;

procedure BDEOpenFieldDescList( hTmpDb: hDBIDb; const sTableName: String; Campos: TStrings );
const
     K_FLD_DESC = '%s: %s, SubTipo %s, Longitud %s, Escala %s';
var
   TmpCursor: hDBICur;
   Result: DBIResult;
   FieldDesc: FLDDesc;

function BDEFieldIntToStr( const FieldType: Word ): String;
begin
     case FieldType of
          fldUNKNOWN: Result := 'Unknown';
          fldZSTRING: Result := 'String';               { Null terminated string }
          fldDATE: Result := 'Date';                    { Date     (32 bit) }
          fldBLOB: Result := 'BLOb';                    { Blob }
          fldBOOL: Result := 'Boolean';                 { Boolean  (16 bit) }
          fldINT16: Result := 'Integer';                { 16 bit signed number }
          fldINT32: Result := 'Long Integer';           { 32 bit signed number }
          fldFLOAT: Result := 'Float';                  { 64 bit floating point }
          fldBCD: Result := 'BCD';                      { BCD }
          fldBYTES: Result := 'Bytes';                  { Fixed number of bytes }
          fldTIME: Result := 'Time';                    { Time        (32 bit) }
          fldTIMESTAMP: Result := 'Timestamp';          { Time-stamp  (64 bit) }
          fldUINT16: Result := 'Unsigned Int';          { Unsigned 16 bit integer }
          fldUINT32: Result := 'Unsigned Long Int';     { Unsigned 32 bit integer }
          fldFLOATIEEE: Result := 'Float IEEE';         { 80-bit IEEE float }
          fldVARBYTES: Result := 'VarBytes';            { Length prefixed var bytes }
          fldLOCKINFO: Result := 'LockInfo';            { Look for LOCKINFO typedef }
          fldCURSOR: Result := 'Oracle Cursor';         { For Oracle Cursor type }
          { Paradox types (Physical) }
          fldPDXCHAR: Result := 'Alpha';                { Alpha    (string) }
          fldPDXNUM: Result := 'Numeric';               { Numeric }
          fldPDXMONEY: Result := 'Money';               { Money }
          fldPDXDATE: Result := 'Date';                 { Date }
          fldPDXSHORT: Result := 'Smallint';            { Short }
          fldPDXMEMO: Result := 'Memo BLOb';            { Text Memo       (blob) }
          fldPDXBINARYBLOB: Result := 'Binary BLOb';    { Binary data     (blob) }
          fldPDXFMTMEMO: Result := 'Formatted BLOb';    { Formatted text  (blob) }
          fldPDXOLEBLOB: Result := 'OLE BLOb';          { OLE object      (blob) }
          fldPDXGRAPHIC: Result := 'Graphic BLOb';      { Graphics object (blob) }
          fldPDXLONG: Result := 'Long Integer';         { Long }
          fldPDXTIME: Result := 'Time';                 { Time }
          fldPDXDATETIME: Result := 'Date Time';        { Time Stamp }
          fldPDXBOOL: Result := 'Boolean';              { Logical }
          fldPDXAUTOINC: Result := 'Auto Increment';    { Auto increment (long) }
          fldPDXBYTES: Result := 'Bytes';               { Fixed number of bytes }
          fldPDXBCD: Result := 'BCD';                   { BCD (32 digits) }
          { xBASE types (Physical) }
          fldDBCHAR: Result := 'Character';             { Char string }
          fldDBNUM: Result := 'Number';                 { Number }
          fldDBMEMO: Result := 'Memo BLOb';             { Memo          (blob) }
          fldDBBOOL: Result := 'Logical';               { Logical }
          fldDBDATE: Result := 'Date';                  { Date }
          fldDBFLOAT: Result := 'Float';                { Float }
          fldDBLOCK: Result := 'LOCKINFO';              { Logical type is LOCKINFO }
          fldDBOLEBLOB: Result := 'OLE BLOb';           { OLE object    (blob) }
          fldDBBINARY: Result := 'Binary BLOb';         { Binary data   (blob) }
          fldDBBYTES: Result := 'Bytes';                { Only for TEMPORARY tables }
          fldDBLONG: Result := 'Long Integer';          { Long (Integer) }
          fldDBDATETIME: Result := 'Date Time';         { Time Stamp }
          fldDBDOUBLE: Result := 'Double';              { Double }
          fldDBAUTOINC: Result := 'Auto Increment';     { Auto increment (long) }
     else
         Result := 'Not Found';
     end;
end;


begin
     Check( DbiOpenFieldList( hTmpDb, {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}( sTableName ), nil, False, TmpCursor ) );
     with Campos do
     begin
          BeginUpdate;
          try
             Clear;
             repeat
                   Result := DbiGetNextRecord( TmpCursor, dbiNOLOCK, @FieldDesc, nil );
                   if ( Result <> DBIERR_EOF) then
                   begin
                        with FieldDesc do
                        begin
                             Add( Format( K_FLD_DESC, [ StrPas( szName ), BDEFieldIntToStr( iFldType ), IntToStr( iSubType ), IntToStr( iUnits1 ), IntToStr( iUnits2 ) ] ) )
                        end;
                   end;
             until ( Result <> DBIERR_NONE );
          finally
                 EndUpdate;
          end;
     end;
     Check( DbiCloseCursor( TmpCursor ) );
end;

procedure BDEBuildPrimaryKeyFieldList( hTmpDb: hDBIDb; const sTableName: String; Campos, CamposLlave: TStrings );
var
   TmpCursor: hDBICur;
   Rslt: DBIResult;
   IndexDesc: IDXDesc;
   i, iCampo: Integer;
begin
     Check( DbiOpenIndexList( hTmpDb, {$ifdef TRESS_DELPHIXE5_UP}PAnsiChar{$else}PChar{$endif}( sTableName ), nil, TmpCursor ) );
     with CamposLlave do
     begin
          BeginUpdate;
          try
             Clear;
             repeat
                   Rslt := DbiGetNextRecord( TmpCursor, dbiNOLOCK, @IndexDesc, nil );
                   if ( Rslt <> DBIERR_EOF) then
                   begin
                        with IndexDesc do
                        begin
                             {$ifdef INTERBASE}
                             if (bPrimary) or ( bUnique and (szname[0]='P') and (szName[1]='K') ) then
                             {$endif}
                             {$ifdef MSSQL}
                             if bUnique then
                             {$endif}
                             begin
                                  for i := 0 to High( aiKeyFld ) do
                                  begin
                                       iCampo := aiKeyFld[ i ];
                                       if ( iCampo > 0 ) then
                                       begin
                                            if (iCampo <= Campos.Count)then
                                            begin
                                                 Add( Campos[ ( iCampo - 1 ) ] );
                                            end
                                            else
                                                Add( Campos[ ( iCampo - 2 ) ] );
                                       end
                                  end;
                                  {$ifdef MSSQL}                             
                                  Break;
                                  {$endif}
                             end;
                        end;
                   end;
             until ( Rslt <> DBIERR_NONE );
          finally
                 EndUpdate;
          end;
     end;
     Check( DbiCloseCursor( TmpCursor ) );
end;

end.
