unit FHayProblemasComparte;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Registry;

type
  THayProblemasComparte = class(TForm)
    Imagen: TImage;
    PanelBotones: TPanel;
    Configurar: TBitBtn;
    Cancelar: TBitBtn;
    Info: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FActualiza: Boolean;
  public
    { Public declarations }
    property Actualiza: Boolean read FActualiza write FActualiza;
    procedure SetError( Value: Exception );
  end;

var
  HayProblemasComparte: THayProblemasComparte;

function MetaDataProblemDialog( Error: Exception; lActualiza: Boolean ): Boolean;

implementation

uses ZetaTressCFGTools,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

function MetaDataProblemDialog( Error: Exception; lActualiza: Boolean ): Boolean;
begin
     with THayProblemasComparte.Create( Application ) do
     begin
          try
             SetError( Error );
             Chicharra;
             Actualiza:= lActualiza;
             ShowModal;
             Result := ( ModalResult = mrOk );
          finally
                 Free;
          end;
     end;
end;

{ ************** THayProblemasComparte *************** }

procedure THayProblemasComparte.FormCreate(Sender: TObject);
begin
     with Imagen do
     begin
          Picture.Icon.Handle := LoadIcon( 0, IDI_HAND );
     end;
end;

procedure THayProblemasComparte.FormShow(Sender: TObject);
begin
     with Configurar do
     begin
          {$ifdef INTERBASE}
          Kind := bkOk;
          Caption := 'Con&tinuar';
          {$endif}
          if Actualiza then
          begin
               Kind := bkOk;
               Caption := 'Ac&tualizar';
          end;
     end;
end;

procedure THayProblemasComparte.SetError( Value: Exception );
begin
     Info.Lines.Text := Value.Message;
end;

end.
