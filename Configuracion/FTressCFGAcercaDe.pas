unit FTressCFGAcercaDe;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, CheckLst, ExtCtrls, ComCtrls,
     FAcercaDe;

type
  TTressCFGAcercaDe = class(TAcercaDe)
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TressCFGAcercaDe: TTressCFGAcercaDe;

implementation

uses FHelpContext;
{$R *.DFM}

procedure TTressCFGAcercaDe.FormShow(Sender: TObject);
begin
     inherited;
     HelpContext := H00024_Acerca_de_TressCFG;
end;

procedure TTressCFGAcercaDe.FormCreate(Sender: TObject);
begin
     inherited;
     ProductName.Caption := Application.Title;
end;

end.
