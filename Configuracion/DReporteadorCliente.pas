unit DReporteadorCliente;

interface

uses
  SysUtils, Classes,
  DCliente,
  DBasicoCliente,
  ReporteadorDD_TLB,
  DBClient,Variants,
  ZetaClientDataSet,
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaCommonTools,
  ZetaCommonClasses,
  DSistema,
  DDBConfig,
  FClienteTRESS,
  ZBaseThreads
     ;

type
   TSQLAgenteClient = class(TObject);
  IdmServerDiccionarioDisp = IdmServerReporteadorDDDisp;


  TdmReporteadorCliente = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private

      FClienteTRESSObject : TClienteTRESS;
      function GetServidor: IdmServerDiccionarioDisp;
      property Servidor: IdmServerDiccionarioDisp read GetServidor;

  private
    FServidor: IdmServerDiccionarioDisp;
    function GetServerDiccionario: IdmServerDiccionarioDisp;

  public
    { Public declarations }
    function ImportarDiccionario( Empresa : Olevariant; varParametros : TZetaParams ) : TProcessInfo;
    function GetListaEmpresasConfidenciales: string;
    property ClienteTRESS : TClienteTRESS read FClienteTRESSObject write FClienteTRESSObject;
  end;

var
  dmReporteadorCliente: TdmReporteadorCliente;

implementation

{$R *.dfm}

{ TdmReporteadorDD }
function TdmReporteadorCliente.GetServerDiccionario: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(dmCliente.CreaServidor( CLASS_dmServerReporteadorDD, FServidor) );
end;

function TdmReporteadorCliente.GetServidor: IdmServerDiccionarioDisp;
begin
     Result := GetServerDiccionario;
end;


procedure TdmReporteadorCliente.DataModuleCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     dmCliente := TdmCliente.Create( Self );
end;

procedure TdmReporteadorCliente.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( dmCliente );
end;

function TdmReporteadorCliente.ImportarDiccionario(Empresa: Olevariant;
  varParametros: TZetaParams): TProcessInfo;
var
   processList : TProcessList;
   info: TProcessInfo;

begin
     processList :=  TProcessList.Create;
     info := TProcessInfo.Create( processList );
     info.SetResultado( Servidor.ImportarDiccionario( Empresa, varParametros.VarValues ) );
     Result := info;
end;

function TdmReporteadorCliente.GetListaEmpresasConfidenciales: string;
begin
     Result := ''; /// por implementar  , esto se puede consultar via directa.
     with dmSistema.cdsEmpresas do
     begin
          Conectar;
          if Active then
          begin
               Filtered := FALSE;
               Filter := Format( 'upper(CM_DATOS) = ''%s''', [Uppercase(dmDBConfig.cdsEmpresasActualizar.FieldbyName('CM_DATOS').AsString)] );
               try
                  Filtered := TRUE;
                  First;
                  if RecordCount > 1 then
                  begin

                       while NOT EOF do
                       begin
                            Result := ConcatString(Result, FieldbyName('CM_CODIGO').AsString, ',');
                            Next;
                       end;
                  end
                  else
                  begin
                       Result := dmCliente.GetDatosEmpresaActiva.Codigo
                  end;
               finally
                      Filtered := FALSE;
               end;
          end
          else
          begin
               raise Exception.Create( 'Error al Obtener la Información de la Empresa Activa' );
          end
     end;
end;



end.
