object dmDBConfig: TdmDBConfig
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 891
  Top = 310
  Height = 457
  Width = 602
  object dbEmpresas: TDatabase
    DatabaseName = 'dbEmpresas'
    LoginPrompt = False
    Params.Strings = (
      'SERVER NAME=DELL-GERMAN:D:\3Win_13\Datos\comparte.gdb'
      'USER NAME=sa'
      'PASSWORD=m')
    SessionName = 'Default'
    Left = 25
    Top = 8
  end
  object tqList: TQuery
    DatabaseName = 'dbEmpresas'
    SQL.Strings = (
      'select CM_CODIGO, CM_NOMBRE, '
      'CM_ALIAS, CM_USRNAME, CM_PASSWRD,'
      'CM_EMPATE, CM_ACUMULA, CM_DATOS from COMPANY')
    UniDirectional = True
    Left = 24
    Top = 142
  end
  object tqCompany: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    AfterOpen = tqCompanyAfterOpen
    BeforeInsert = tqCompanyBeforeInsert
    BeforePost = tqCompanyBeforePost
    OnCalcFields = tqCompanyCalcFields
    OnNewRecord = tqCompanyNewRecord
    OnUpdateRecord = tqCompanyUpdateRecord
    DatabaseName = 'dbEmpresas'
    SQL.Strings = (
      'select CM_CODIGO, CM_NOMBRE,'
      'CM_ALIAS, CM_USRNAME, CM_PASSWRD,'
      'CM_EMPATE, CM_ACUMULA, CM_DATOS, '
      'CM_NIVEL0, CM_USACAFE, CM_ACTIVOS,'
      'CM_CONTROL, CM_DIGITO, CM_KCLASIFI,'
      'CM_CTRL_RL,  CM_CHKSUM'
      'from COMPANY'
      'order by CM_NOMBRE')
    UniDirectional = True
    UpdateObject = upCompany
    Left = 24
    Top = 86
    object tqCompanyCM_CODIGO: TStringField
      FieldName = 'CM_CODIGO'
      Origin = 'DBEMPRESAS.COMPANY.CM_CODIGO'
      FixedChar = True
      Size = 10
    end
    object tqCompanyCM_NOMBRE: TStringField
      FieldName = 'CM_NOMBRE'
      Origin = 'DBEMPRESAS.COMPANY.CM_NOMBRE'
      Size = 50
    end
    object tqCompanyCM_ALIAS: TStringField
      FieldName = 'CM_ALIAS'
      Origin = 'DBEMPRESAS.COMPANY.CM_ALIAS'
      Size = 30
    end
    object tqCompanyCM_USRNAME: TStringField
      FieldName = 'CM_USRNAME'
      Origin = 'DBEMPRESAS.COMPANY.CM_USRNAME'
      Size = 15
    end
    object tqCompanyCM_PASSWRD: TStringField
      FieldName = 'CM_PASSWRD'
      Origin = 'DBEMPRESAS.COMPANY.CM_PASSWRD'
      Size = 30
    end
    object tqCompanyCM_EMPATE: TIntegerField
      FieldName = 'CM_EMPATE'
      Origin = 'DBEMPRESAS.COMPANY.CM_EMPATE'
    end
    object tqCompanyCM_ACUMULA: TStringField
      FieldName = 'CM_ACUMULA'
      Origin = 'DBEMPRESAS.COMPANY.CM_ACUMULA'
      FixedChar = True
      Size = 10
    end
    object tqCompanyCM_DATOS: TStringField
      FieldName = 'CM_DATOS'
      Origin = 'DBEMPRESAS.COMPANY.CM_DATOS'
      Size = 255
    end
    object tqCompanyCM_NIVEL0: TStringField
      DisplayWidth = 255
      FieldName = 'CM_NIVEL0'
      Origin = 'DBEMPRESAS.COMPANY.CM_NIVEL0'
      FixedChar = True
      Size = 255
    end
    object tqCompanyCM_USACAFE: TStringField
      FieldName = 'CM_USACAFE'
      Origin = 'DBEMPRESAS.COMPANY.CM_USACAFE'
      FixedChar = True
      Size = 1
    end
    object tqCompanyCM_ACTIVOS: TIntegerField
      FieldName = 'CM_ACTIVOS'
      Origin = 'DBEMPRESAS.COMPANY.CM_ACTIVOS'
    end
    object tqCompanyCM_CONTROL: TStringField
      FieldName = 'CM_CONTROL'
      Origin = 'DBEMPRESAS.COMPANY.CM_CONTROL'
      FixedChar = True
      Size = 8
    end
    object tqCompanyCM_TIPO_E: TStringField
      FieldKind = fkCalculated
      FieldName = 'CM_TIPO_E'
      Size = 15
      Calculated = True
    end
    object tqCompanyCM_TIPO: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'CM_TIPO'
      Calculated = True
    end
    object tqCompanyCM_DIGITO: TStringField
      FieldName = 'CM_DIGITO'
      Size = 1
    end
    object tqCompanyCM_KCLASIFI: TSmallintField
      FieldName = 'CM_KCLASIFI'
    end
    object tqCompanyCM_CTRL_RL: TStringField
      FieldName = 'CM_CTRL_RL'
      Size = 8
    end
    object tqCompanyCM_CHKSUM: TStringField
      FieldName = 'CM_CHKSUM'
      Size = 255
    end
    object tqCompanyCM_USACASE: TStringField
      FieldName = 'CM_USACASE'
      Origin = 'DBEMPRESAS.COMPANY.CM_USACASE'
      FixedChar = True
      Size = 1
    end
  end
  object upCompany: TUpdateSQL
    ModifySQL.Strings = (
      'update COMPANY set'
      'CM_CODIGO = :CM_CODIGO,'
      'CM_NOMBRE = :CM_NOMBRE,'
      'CM_ALIAS = :CM_ALIAS,'
      'CM_USRNAME = :CM_USRNAME,'
      'CM_PASSWRD = :CM_PASSWRD,'
      'CM_EMPATE = :CM_EMPATE,'
      'CM_ACUMULA = :CM_ACUMULA,'
      'CM_DATOS = :CM_DATOS,'
      'CM_NIVEL0 = :CM_NIVEL0,'
      'CM_USACAFE = :CM_USACAFE,'
      'CM_USACASE = :CM_USACASE,'
      'CM_CONTROL = :CM_CONTROL,'
      'CM_DIGITO = :CM_DIGITO,'
      'CM_KCLASIFI = :CM_KCLASIFI,'
      'CM_CTRL_RL = :CM_CTRL_RL,'
      'CM_CHKSUM = :CM_CHKSUM'
      'where ( CM_CODIGO = :OLD_CM_CODIGO )'
      ' ')
    InsertSQL.Strings = (
      'insert into COMPANY( CM_CODIGO,'
      '                     CM_NOMBRE,'
      '                     CM_ALIAS,'
      '                     CM_USRNAME,'
      '                     CM_PASSWRD,'
      '                     CM_EMPATE,'
      '                     CM_ACUMULA,'
      '                     CM_DATOS,'
      '                     CM_NIVEL0,'
      '                     CM_USACAFE,'
      '                     CM_CONTROL,'
      '                     CM_DIGITO,'
      '                     CM_KCLASIFI,'
      '                     CM_CTRL_RL,'
      '                     CM_CHKSUM) '
      'values ('
      '                     :CM_CODIGO,'
      '                     :CM_NOMBRE,'
      '                     :CM_ALIAS,'
      '                     :CM_USRNAME,'
      '                     :CM_PASSWRD,'
      '                     :CM_EMPATE,'
      '                     :CM_ACUMULA,'
      '                     :CM_DATOS,'
      '                     :CM_NIVEL0,'
      '                     :CM_USACAFE,'
      '                     :CM_CONTROL,'
      '                     :CM_DIGITO,'
      '                     :CM_KCLASIFI,'
      '                     :CM_CTRL_RL,'
      '                     :CM_CHKSUM)'
      ' ')
    DeleteSQL.Strings = (
      'delete from COMPANY where ( CM_CODIGO = :CM_CODIGO )')
    Left = 97
    Top = 86
  end
  object cdsEmpresas: TServerDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsEmpresasAfterOpen
    Left = 92
    Top = 9
  end
  object dbEmpresa: TDatabase
    DatabaseName = 'dbEmpresa'
    LoginPrompt = False
    Params.Strings = (
      'SERVER NAME=DELL-GERMAN:D:\3Win_13\Datos\comparte.gdb'
      'USER NAME=sa'
      'PASSWORD=m')
    SessionName = 'Default'
    Left = 185
    Top = 8
  end
  object cdsEmpresasActualizar: TServerDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsEmpresasActualizarAfterOpen
    Left = 196
    Top = 81
  end
  object tqSPPatch: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    AfterOpen = tqSPPatchAfterOpen
    OnNewRecord = tqSPPatchNewRecord
    OnUpdateRecord = tqSPPatchUpdateRecord
    DatabaseName = 'dbEmpresas'
    SQL.Strings = (
      'select        '
      '       SE_TIPO ,'
      '       SE_SEQ_NUM ,'
      '       SE_APLICA ,'
      '       SE_NOMBRE ,'
      '       SE_DESCRIP ,'
      '       SE_DATA,'
      '       SE_ARCHIVO'
      'from SP_PATCH'
      'order by SE_TIPO, SE_SEQ_NUM, SE_APLICA')
    UniDirectional = True
    UpdateObject = upSPPatch
    Left = 24
    Top = 206
    object tqSPPatchSE_TIPO: TIntegerField
      FieldName = 'SE_TIPO'
      Origin = 'DBEMPRESAS.SP_PATCH.SE_TIPO'
    end
    object tqSPPatchSE_SEQ_NUM: TIntegerField
      FieldName = 'SE_SEQ_NUM'
      Origin = 'DBEMPRESAS.SP_PATCH.SE_SEQ_NUM'
    end
    object tqSPPatchSE_APLICA: TStringField
      FieldName = 'SE_APLICA'
      Origin = 'DBEMPRESAS.SP_PATCH.SE_APLICA'
      Size = 10
    end
    object tqSPPatchSE_NOMBRE: TStringField
      DisplayWidth = 10
      FieldName = 'SE_NOMBRE'
      Origin = 'DBEMPRESAS.SP_PATCH.SE_NOMBRE'
      Size = 10
    end
    object tqSPPatchSE_DESCRIP: TStringField
      DisplayWidth = 50
      FieldName = 'SE_DESCRIP'
      Origin = 'DBEMPRESAS.SP_PATCH.SE_DESCRIP'
      Size = 50
    end
    object tqSPPatchSE_DATA: TMemoField
      FieldName = 'SE_DATA'
      Origin = 'DBEMPRESAS.SP_PATCH.SE_DATA'
      BlobType = ftMemo
    end
    object tqSPPatchSE_ARCHIVO: TStringField
      FieldName = 'SE_ARCHIVO'
      Origin = 'DBEMPRESAS.SP_PATCH.SE_ARCHIVO'
      Size = 255
    end
  end
  object upSPPatch: TUpdateSQL
    ModifySQL.Strings = (
      'update SP_PATCH set'
      'SE_TIPO =:SE_TIPO, '
      'SE_SEQ_NUM =:SE_SEQ_NUM, '
      'SE_APLICA =:SE_APLICA, '
      'SE_NOMBRE =:SE_NOMBRE, '
      'SE_DESCRIP =:SE_DESCRIP,'
      'SE_DATA = :SE_DATA,'
      'SE_ARCHIVO = :SE_ARCHIVO'
      'where '
      '(SE_TIPO = :OLD_SE_TIPO  and SE_SEQ_NUM = :OLD_SE_SEQ_NUM and '
      'SE_APLICA = :OLD_SE_APLICA)')
    InsertSQL.Strings = (
      'insert into SP_PATCH( SE_TIPO ,'
      '       SE_SEQ_NUM ,'
      '       SE_APLICA ,'
      '       SE_NOMBRE ,'
      '       SE_DESCRIP ,'
      '       SE_DATA,'
      '       SE_ARCHIVO) '
      'values ('
      '       :SE_TIPO ,'
      '       :SE_SEQ_NUM ,'
      '       :SE_APLICA ,'
      '       :SE_NOMBRE ,'
      '       :SE_DESCRIP ,'
      '       :SE_DATA,'
      '       :SE_ARCHIVO)')
    DeleteSQL.Strings = (
      
        'delete from SP_PATCH where (SE_TIPO = :SE_TIPO  and SE_SEQ_NUM =' +
        ' '
      ':SE_SEQ_NUM and SE_APLICA = :SE_APLICA)')
    Left = 25
    Top = 262
  end
  object tqActualizaBlob: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    OnUpdateError = tqActualizaBlobUpdateError
    DatabaseName = 'dbEmpresas'
    UniDirectional = True
    Left = 96
    Top = 208
  end
  object tqSPPatchToApply: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    DatabaseName = 'dbEmpresas'
    UniDirectional = True
    Left = 32
    Top = 334
  end
  object tqExisteChecksum: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    DatabaseName = 'dbEmpresas'
    UniDirectional = True
    Left = 152
    Top = 334
  end
  object tqActualizaChecksum: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    DatabaseName = 'dbEmpresas'
    UniDirectional = True
    Left = 264
    Top = 334
  end
  object tqScriptComparteAux: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    DatabaseName = 'dbEmpresas'
    UniDirectional = True
    Left = 392
    Top = 334
  end
  object tqValidaDuplicidadBD: TQuery
    AutoCalcFields = False
    CachedUpdates = True
    DatabaseName = 'dbEmpresas'
    UniDirectional = True
    Left = 504
    Top = 334
  end
end
