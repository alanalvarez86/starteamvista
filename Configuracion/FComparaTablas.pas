unit FComparaTablas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables, ExtCtrls,
     FileCtrl, ComCtrls, Mask, DBCtrls, Grids, DBGrids, Db, Checklst,
     ZetaWizard,
     ZetaNumero,
     ZetaKeyCombo;

type
  TComparaTablas = class(TForm)
    Wizard: TZetaWizard;
    Anterior: TZetaWizardButton;
    Siguiente: TZetaWizardButton;
    Comparar: TZetaWizardButton;
    Cancelar: TZetaWizardButton;
    PageControl: TPageControl;
    Intro: TTabSheet;
    Empresas: TTabSheet;
    Comparacion: TTabSheet;
    Mensaje: TMemo;
    StatusBar: TStatusBar;
    EmpresaFuenteGB: TGroupBox;
    EmpresaFuente: TListBox;
    Parametros: TTabSheet;
    CatalogosGB: TGroupBox;
    Catalogos: TCheckListBox;
    PanelSuperior: TPanel;
    Transferir: TCheckBox;
    LogFileDialog: TSaveDialog;
    AvanceGB: TGroupBox;
    ProgressBar: TProgressBar;
    LogFileLBL: TLabel;
    LogFile: TEdit;
    LogFileSeek: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LogFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure TransferirClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    function CallMeBack( const sMensaje: String ): Boolean;
    function GetEmpresa: Integer;
    function GetLogFileName: String;
    procedure CheckCatalogos;
    procedure InitCatalogos;
    procedure ShowStatus( const sMsg: String );
    procedure SetControls( const lEnabled: Boolean );
    procedure SetLogFileName( const Value: String );
  public
    { Public declarations }
    property LogFileName: String read GetLogFileName write SetLogFileName;
  end;

var
  ComparaTablas: TComparaTablas;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaMessages,
     ZetaDialogo,
     FHelpContext,
     DDBConfig,
     DCompara;

{$R *.DFM}

procedure TComparaTablas.FormCreate(Sender: TObject);
begin
     dmCompara := TdmCompara.Create( Self );
     Wizard.Primero;
     HelpContext := H00025_Comparar_catalogos_y_tablas;
end;

procedure TComparaTablas.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        LogFile.Text := ZetaMessages.SetFileNameDefaultPath( 'ComparaTablas.log' );
        with Mensaje.Lines do
        begin
             BeginUpdate;
             try
                Clear;
                Add( '' );
                with dmDBConfig do
                begin
                     Add( GetCompanyName );
                     Add( StringOfChar( '-', Length( GetCompanyName ) ) );
                end;
                Add( '' );
                Add( 'Este Proceso Compara Los Cat�logos Y Las Tablas' );
                Add( 'De Esta Empresa Contra Los Datos De Otra Empresa' );
                Add( 'Fuente, La Cual Se Especifica Posteriormente' );
                Add( '' );
                Add( 'Se Reportan Los Registros De La Empresa Fuente Que' );
                Add( 'No Existen En Esta Empresa ( Los Cuales Pueden Ser' );
                Add( 'Opcionalmente Transferidos ), As� Como Aquellos Que,' );
                Add( 'Existiendo En Ambas, Tienen Diferencias En Algunos Datos.' );
                Add( '' );
                Add( 'El Prop�sito De Esta Comparaci�n Es Auxiliar' );
                Add( 'Al Proceso De Consolidaci�n De Ambas Empresas' );
                Add( 'En Una Sola Base De Datos' );
             finally
                    EndUpdate;
             end;
        end;
        TransferirClick( Self );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TComparaTablas.FormDestroy(Sender: TObject);
begin
     dmCompara.DesconectarFuente;
     FreeAndNil( dmCompara );
end;

procedure TComparaTablas.InitCatalogos;
var
   i: Integer;
begin
     with dmCompara do
     begin
          with Catalogos do
          begin
               with Items do
               begin
                    BeginUpdate;
                    try
                       Clear;
                       for i := 0 to ( TablasCount - 1 ) do
                       begin
                            with Tablas[ i ] do
                            begin
                                 Add( Descripcion );
                                 Checked[ i ] := Transfer;
                            end;
                       end;
                    finally
                           EndUpdate;
                    end;
               end;
          end;
     end;
end;

procedure TComparaTablas.CheckCatalogos;
var
   i: Integer;
begin
     with dmCompara do
     begin
          for i := 0 to ( TablasCount - 1 ) do
          begin
               Catalogos.Checked[ i ] := Tablas[ i ].Transfer;
          end;
     end;
end;

procedure TComparaTablas.SetControls( const lEnabled: Boolean );
begin
     Catalogos.Enabled := lEnabled;
end;

function TComparaTablas.GetEmpresa: Integer;
begin
     with EmpresaFuente do
     begin
          if ( ItemIndex < 0 ) then
             Result := -1
          else
              Result := Integer( Items.Objects[ ItemIndex ] );
     end;
end;

function TComparaTablas.GetLogFileName: String;
begin
     Result := LogFile.Text;
end;

procedure TComparaTablas.SetLogFileName( const Value: String );
begin
     LogFile.Text := Value;
end;

procedure TComparaTablas.ShowStatus( const sMsg: String );
begin
     StatusBar.SimpleText := sMsg;
end;

function TComparaTablas.CallMeBack( const sMensaje: String ): Boolean;
begin
     ShowStatus( sMensaje );
     ProgressBar.StepIt;
     Application.ProcessMessages;
     Result := not Wizard.Cancelado;
end;

procedure TComparaTablas.LogFileSeekClick(Sender: TObject);
begin
     with LogFileDialog do
     begin
          FileName := LogFileName;
          if Execute then
             LogFileName := FileName;
     end;
end;

procedure TComparaTablas.TransferirClick(Sender: TObject);
begin
     SetControls( Transferir.Checked );
end;

procedure TComparaTablas.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
   i: Integer;
   lChecked: Boolean;
begin
     if CanMove then
     begin
          with Wizard do
          begin
               if Adelante then
               begin
                    if EsPaginaActual( Intro ) then
                    begin
                         with dmCompara do
                         begin
                              if InitEmpresas( Self.EmpresaFuente.Items ) then
                              begin
                                   Self.EmpresaFuente.ItemIndex := 0;
                                   if ConectarDestino then
                                   begin
                                        try
                                           InitTablas;
                                           InitCatalogos;
                                        except
                                              on Error: Exception do
                                              begin
                                                   Application.HandleException( Error );
                                                   CanMove := False;
                                              end;
                                        end;
                                   end
                                   else
                                   begin
                                        CanMove := False;
                                        ZetaDialogo.zInformation( '� No Es Posible Continuar !', 'No Es Posible Abrir La Empresa Destino', 0 );
                                   end;
                              end
                              else
                              begin
                                   CanMove := False;
                                   ZetaDialogo.zInformation( '� No Es Posible Continuar !', 'No Es Posible Leer La Lista De Empresas', 0 );
                              end;
                         end;
                    end
                    else
                         if EsPaginaActual( Empresas ) then
                         begin
                              if ( GetEmpresa < 0 ) then
                              begin
                                   CanMove := False;
                                   ZetaDialogo.zInformation( '� Atenci�n !', 'Es Necesario Escoger Una Empresa Fuente', 0 );
                                   ActiveControl := EmpresaFuente;
                              end
                              else
                              begin
                                   oCursor := Screen.Cursor;
                                   Screen.Cursor := crHourglass;
                                   try
                                      if not dmCompara.ConectarFuente( GetEmpresa ) then
                                      begin
                                           ActiveControl := EmpresaFuente;
                                           CanMove := False;
                                      end;
                                   finally
                                          Screen.Cursor := oCursor;
                                   end;
                              end;
                         end
                         else
                             if EsPaginaActual( Parametros ) and Transferir.Checked then
                             begin
                                  with Catalogos do
                                  begin
                                       lChecked := False;
                                       for i := 0 to ( Items.Count - 1 ) do
                                       begin
                                            if Checked[ i ] then
                                            begin
                                                 lChecked := True;
                                                 Break;
                                            end;
                                       end;
                                  end;
                                  if not lChecked then
                                  begin
                                       CanMove := False;
                                       ZetaDialogo.zInformation( '� Atenci�n !', 'Es Necesario Escoger Una Tabla Por Transferir', 0 );
                                       ActiveControl := Catalogos;
                                  end;
                             end;
               end
               else
                   if EsPaginaActual( Comparacion ) then
                   begin
                        CheckCatalogos;
                   end;
          end;
     end;
end;

procedure TComparaTablas.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Comparacion ) then
     begin
          with Comparar do
          begin
               if Transferir.Checked then
                  Caption := '&Transferir'
               else
                   Caption := 'C&omparar';
          end;
          with ProgressBar do
          begin
               Position := 0;
          end;
     end;
end;

procedure TComparaTablas.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   FLog: TTransferLog;
   i: Integer;
begin
     FLog := TTransferLog.Create;
     try
        FLog.Start( LogFileName );
        ShowStatus( 'Empezando Comparaci�n' );
        with dmCompara do
        begin
             with ProgressBar do
             begin
                  Step := 1;
                  Max := TablasCount;
             end;
             if Transferir.Checked then
             begin
                  with Catalogos do
                  begin
                       for i := 0 to ( Items.Count - 1 ) do
                       begin
                            Tablas[ i ].Transfer := Checked[ i ];
                       end;
                  end;
             end;
             if not CompararTablas( CallMeBack, FLog ) then
                ZetaDialogo.zInformation( '� Buenas Noticias !', 'No Se Encontraron Diferencias', 0 );
        end;
        ShowStatus( 'Comparaci�n Terminada' );
        lOk := True;
     finally
            with FLog do
            begin
                 Close;
                 Free;
            end;
     end;
     if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver La Bit�cora ?', 0, mbOk ) then
        ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( LogFileName ), ExtractFilePath( LogFileName ), SW_SHOWDEFAULT );
end;

procedure TComparaTablas.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     lOk := True;
     Close;
end;

end.
