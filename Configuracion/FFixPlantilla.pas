unit FFixPlantilla;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, QRDesign,
  Qrdctrls,
  QrExport,
  Qrprntr,
  QRDLDR,
  Qrctrls,
  qrdProcs,
  StdCtrls, Db, DBTables ;

type
  TForm1 = class(TForm)
    FDesigner: TQRepDesigner;
    Button1: TButton;
    Bitacora: TListBox;
    Reporte: TDesignQuickReport;
    QRDesignBand1: TQRDesignBand;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses Unit2;

{$R *.DFM}

procedure TForm1.Button1Click(Sender: TObject);
 const sDir = 'd:\Plantillas_Douglas\';
       sBackup = 'BackupOriginales';

 procedure CargaArchivo( const sFile : string );
  var sFileBak : string;
 begin
      if FileExists(sFile) then
      begin
           qrdProcs.ClearReportForm( Self, TRUE );
           if FDesigner.LoadReport( sFile ) then
           begin
                Bitacora.Items.Add('Cargando Archivo: ' + sFile);
                sFileBak := sDir+sBackup+'\'+ExtractFileName(sFile);
                FDesigner.SaveReport(sFileBak);
                if NOT Reporte.Bands.HasDetail then
                begin
                     with TQRDesignBand.Create( self ) do
                     begin
                          Parent := Reporte;
                          BandType := rbDetail;
                          Height := 10;
                          Font.Name := 'COURIER NEW';
                          Font.Size := 7;
                     end;
                end;
                FDesigner.SaveReport( sFile );
           end
           else Bitacora.Items.Add('Error al Cargar Archivo: ' + sFile);
      end
      else
          Bitacora.Items.Add('La Plantilla  ' + sFile + ' No Existe');     
 end;
 var sFile : string;
begin
     CreateDir(sDir+sBackup);

     if NOT dmDatamodule.dbTress.Connected then
     begin
          dmDatamodule.dbTress.Connected := TRUE;
          dmDatamodule.tqQuery.Active := TRUE;
     end;

     while NOT dmDatamodule.tqQuery.EOF do
     begin
          sFile := dmDatamodule.tqQuery.FieldByName('RE_REPORTE').AsString;
          if ( Trim(sFile) > '' ) then
          begin
               if ExtractFileDir(sFile) = '' then
                  sFile := sDir + sFile;
               CargaArchivo( sFile );
          end;
          dmDatamodule.tqQuery.Next;
     end;
end;

end.
