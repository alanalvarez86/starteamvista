inherited dmMigrarComparte: TdmMigrarComparte
  Left = 273
  Top = 244
  Height = 189
  Width = 336
  inherited dbSourceShared: TDatabase
    Left = 29
    Top = 9
  end
  inherited dbSourceDatos: TDatabase
    Left = 110
  end
  inherited dbTarget: TDatabase
    Left = 175
    Top = 9
  end
  inherited tqSource: TQuery
    Left = 28
    Top = 57
  end
  inherited tqTarget: TQuery
    Left = 110
    Top = 57
  end
  inherited tqDelete: TQuery
    Left = 175
    Top = 57
  end
  inherited ttTarget: TTable
    Left = 279
    Top = 9
  end
  inherited BatchMove: TBatchMove
    Left = 279
    Top = 57
  end
  inherited ttSource: TTable
    Left = 228
  end
  object Grupos: TZetaMigrar
    AdditionalRows = 2
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      
        'insert into GRUPO (GR_CODIGO, GR_DESCRIP) values (:GR_CODIGO, :G' +
        'R_DESCRIP)')
    Proceso = prNinguno
    StartMessage = 'Grupos de Usuarios'
    TargetDatabase = dbTarget
    TargetTableName = 'GRUPO'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnAdditionalRows = GruposAdditionalRows
    Left = 27
    Top = 107
  end
  object Usuario: TZetaMigrar
    Tag = 1
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into USUARIO ('
      'US_CODIGO,'
      'US_CORTO,'
      'US_NIVEL,'
      'GR_CODIGO,'
      'US_PASSWRD,'
      'US_BLOQUEA,'
      'US_CAMBIA,'
      'US_NOMBRE,'
      'US_DENTRO,'
      'US_BIT_LST) values ('
      ':US_CODIGO,'
      ':US_CORTO,'
      ':US_NIVEL,'
      ':GR_CODIGO,'
      ':US_PASSWRD,'
      ':US_BLOQUEA,'
      ':US_CAMBIA,'
      ':US_NOMBRE,'
      ':US_DENTRO,'
      ':US_BIT_LST)')
    Proceso = prNinguno
    SourceDatabase = dbSourceShared
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from USUARIOS.DAT')
    StartMessage = 'Usuarios'
    TargetDatabase = dbTarget
    TargetTableName = 'USUARIO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = UsuarioCloseDatasets
    OnMoveExcludedFields = UsuarioMoveExcludedFields
    OnMoveOtherTables = UsuarioMoveOtherTables
    OnOpenDatasets = UsuarioOpenDatasets
    Left = 108
    Top = 107
  end
  object tqUno: TQuery
    Left = 228
    Top = 57
  end
  object Company: TZetaMigrar
    Tag = 1
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into COMPANY( CM_CODIGO,'
      '                     CM_NOMBRE,'
      '                     CM_ACUMULA,'
      '                     CM_ALIAS,'
      '                     CM_PASSWRD,'
      '                     CM_USRNAME,'
      '                     CM_DATOS ) values ('
      '                     :CM_CODIGO,'
      '                     :CM_NOMBRE,'
      '                     :CM_ACUMULA,'
      '                     :CM_ALIAS,'
      '                     :CM_PASSWRD,'
      '                     :CM_USRNAME,'
      '                     :CM_DATOS )'
      '')
    Proceso = prNinguno
    SourceDatabase = dbSourceShared
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from COMPANYS.DBF')
    StartMessage = 'Compa��as'
    TargetDatabase = dbTarget
    TargetTableName = 'COMPANY'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnFilterRecord = CompanyFilterRecord
    OnMoveExcludedFields = CompanyMoveExcludedFields
    Left = 177
    Top = 107
  end
end
