unit FTransferirEmpleados;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables, ExtCtrls,
     FileCtrl, ComCtrls, Mask, DBCtrls, Grids, DBGrids, Db, Checklst,
     ZetaWizard,
     ZetaNumero,
     ZetaKeyCombo;

type
  TTransferirEmpleados = class(TForm)
    Wizard: TZetaWizard;
    Anterior: TZetaWizardButton;
    Siguiente: TZetaWizardButton;
    Transferir: TZetaWizardButton;
    Cancelar: TZetaWizardButton;
    PageControl: TPageControl;
    Intro: TTabSheet;
    Comparacion: TTabSheet;
    Mensaje: TMemo;
    StatusBar: TStatusBar;
    Parametros: TTabSheet;
    PanelSuperior: TPanel;
    LogFileDialog: TSaveDialog;
    AvanceGB: TGroupBox;
    ProgressBar: TProgressBar;
    LogFileLBL: TLabel;
    LogFile: TEdit;
    LogFileSeek: TSpeedButton;
    Empresas: TTabSheet;
    EmpresaFuenteGB: TGroupBox;
    EmpresaFuente: TListBox;
    NumeroBaseLBL: TLabel;
    NumeroBase: TZetaNumero;
    FiltroGB: TGroupBox;
    Filtro: TMemo;
    EmpleadosTransferibles: TLabel;
    ConfidencialidadLBL: TLabel;
    Confidencialidad: TComboBox;
    TransferirNominas: TCheckBox;
    TransferirLiquidaciones: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LogFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    FEmpleadoMaximo: Integer;
    FEmpleadoCount: Integer;
    function CallMeBack( const sMensaje: String ): Boolean;
    function GetEmpleadoBase: Integer;
    function GetSQLFilter: String;
    function GetEmpresa: Integer;
    function GetLogFileName: String;
    function GetNivel0: String;
    function GetTransferirIMSS: Boolean;
    function GetTransferirNominas: Boolean;
    procedure ShowStatus( const sMsg: String );
    procedure SetLogFileName( const Value: String );
    procedure SetEmpleadoMaximo;
  public
    { Public declarations }
    property LogFileName: String read GetLogFileName write SetLogFileName;
    procedure SetControls( const lEnabled: Boolean );
  end;

var
  TransferirEmpleados: TTransferirEmpleados;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaMessages,
     ZetaDialogo,
     FHelpContext,
     DDBConfig,
     DCompara;

{$R *.DFM}

procedure TTransferirEmpleados.FormCreate(Sender: TObject);
begin
     dmCompara := TdmCompara.Create( Self );
     Wizard.Primero;
     HelpContext := H00026_Transferir_empleados;
end;

procedure TTransferirEmpleados.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        LogFile.Text := ZetaMessages.SetFileNameDefaultPath( 'Transferencia.log' );
        with Mensaje.Lines do
        begin
             BeginUpdate;
             try
                Clear;
                Add( '' );
                with dmDBConfig do
                begin
                     Add( GetCompanyName );
                     Add( StringOfChar( '-', Length( GetCompanyName ) ) );
                end;
                Add( '' );
                Add( 'Este Proceso Transfiere Empleados Y Todos Sus Datos' );
                Add( 'De Una Empresa Fuente Hacia Esta Empresa' );
                Add( '' );
                Add( 'La Empresa Fuente Se Especifica Posteriormente' );
                Add( '' );
                Add( 'Se Requiere Especificar Un N�mero De Empleado Base Para' );
                Add( 'Asignar A Todos Los Empleados Transferidos En Esta Empresa' );
                Add( 'Quienes Ya Tengan Su N�mero Asignado A Otro Empleado' );
             finally
                    EndUpdate;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTransferirEmpleados.FormDestroy(Sender: TObject);
begin
     dmCompara.DesconectarFuente;
     FreeAndNil( dmCompara );
end;

procedure TTransferirEmpleados.SetControls( const lEnabled: Boolean );
begin
end;

function TTransferirEmpleados.GetEmpresa: Integer;
begin
     with EmpresaFuente do
     begin
          if ( ItemIndex < 0 ) then
             Result := -1
          else
              Result := Integer( Items.Objects[ ItemIndex ] );
     end;
end;

function TTransferirEmpleados.GetLogFileName: String;
begin
     Result := LogFile.Text;
end;

function TTransferirEmpleados.GetSQLFilter: String;
begin
     Result := ZetaCommonTools.BorraCReturn( Trim( Filtro.Lines.Text ) );
end;

function TTransferirEmpleados.GetNivel0: String;
begin
     with Confidencialidad do
     begin
          if ( ItemIndex < 1 ) then
             Result := ''
          else
              Result := dmCompara.Nivel0.Names[ ( ItemIndex - 1 ) ];
     end;
end;

function TTransferirEmpleados.GetEmpleadoBase: Integer;
begin
     Result := NumeroBase.ValorEntero;
end;

function TTransferirEmpleados.GetTransferirIMSS: Boolean;
begin
     Result := TransferirLiquidaciones.Checked;
end;

function TTransferirEmpleados.GetTransferirNominas: Boolean;
begin
     Result := TransferirNominas.Checked;
end;

procedure TTransferirEmpleados.SetEmpleadoMaximo;
begin
     NumeroBase.Valor := FEmpleadoMaximo + 1;
end;

procedure TTransferirEmpleados.SetLogFileName( const Value: String );
begin
     LogFile.Text := Value;
end;

procedure TTransferirEmpleados.ShowStatus( const sMsg: String );
begin
     StatusBar.SimpleText := sMsg;
end;

function TTransferirEmpleados.CallMeBack( const sMensaje: String ): Boolean;
begin
     ShowStatus( sMensaje );
     ProgressBar.StepIt;
     Application.ProcessMessages;
     Result := not Wizard.Cancelado;
end;

procedure TTransferirEmpleados.LogFileSeekClick(Sender: TObject);
begin
     with LogFileDialog do
     begin
          FileName := LogFileName;
          if Execute then
             LogFileName := FileName;
     end;
end;

procedure TTransferirEmpleados.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
   i: Integer;
begin
     if CanMove then
     begin
          with Wizard do
          begin
               if Adelante then
               begin
                    if EsPaginaActual( Intro ) then
                    begin
                         with dmCompara do
                         begin
                              if InitEmpresas( Self.EmpresaFuente.Items ) then
                              begin
                                   Self.EmpresaFuente.ItemIndex := 0;
                                   if ConectarDestino then
                                   begin
                                        try
                                           FEmpleadoMaximo := GetEmpleadoMaximo;
                                           with Confidencialidad do
                                           begin
                                                with Items do
                                                begin
                                                     try
                                                        BeginUpdate;
                                                        Add( 'Confidencialidad Ya Asignada' );
                                                        for i := 0 to ( Nivel0.Count - 1 ) do
                                                        begin
                                                             Add( Nivel0.Values[ Nivel0.Names[ i ] ] );
                                                        end;
                                                     finally
                                                            EndUpdate;
                                                     end;
                                                end;
                                                ItemIndex := 0;
                                           end;
                                           SetEmpleadoMaximo;
                                        except
                                              on Error: Exception do
                                              begin
                                                   Application.HandleException( Error );
                                                   CanMove := False;
                                              end;
                                        end;
                                   end
                                   else
                                   begin
                                        CanMove := False;
                                        ZetaDialogo.zInformation( '� No Es Posible Continuar !', 'No Es Posible Abrir La Empresa Destino', 0 );
                                   end;
                              end
                              else
                              begin
                                   CanMove := False;
                                   ZetaDialogo.zInformation( '� No Es Posible Continuar !', 'No Es Posible Leer La Lista De Empresas', 0 );
                              end;
                         end;
                    end
                    else
                    if EsPaginaActual( Empresas ) then
                    begin
                         if ( GetEmpresa < 0 ) then
                         begin
                              CanMove := False;
                              ZetaDialogo.zInformation( '� Atenci�n !', 'Es Necesario Escoger Una Empresa Fuente', 0 );
                              ActiveControl := EmpresaFuente;
                         end
                         else
                         begin
                              oCursor := Screen.Cursor;
                              Screen.Cursor := crHourglass;
                              try
                                 if not dmCompara.ConectarFuente( GetEmpresa ) then
                                 begin
                                      ActiveControl := EmpresaFuente;
                                      CanMove := False;
                                 end;
                              finally
                                     Screen.Cursor := oCursor;
                              end;
                         end;
                    end
                    else
                        if EsPaginaActual( Parametros ) then
                        begin
                             if ( GetEmpleadoBase < FEmpleadoMaximo ) then
                             begin
                                  CanMove := False;
                                  ZetaDialogo.zInformation( '� Atenci�n !', Format( 'El N�mero De Empleado Base Debe Ser Mayor A %d', [ FEmpleadoMaximo ] ), 0 );
                                  SetEmpleadoMaximo;
                                  ActiveControl := NumeroBase;
                             end
                             else
                             begin
                                  FEmpleadoCount := dmCompara.GetEmpleadoCount( GetSQLFilter );
                                  if ( FEmpleadoCount = 0 ) then
                                  begin
                                       CanMove := False;
                                       ZetaDialogo.zInformation( '� Atenci�n !', 'No Hay Empleados Por Transferir', 0 );
                                       ActiveControl := Filtro;
                                  end;
                             end;
                        end;
               end;
          end;
     end;
end;

procedure TTransferirEmpleados.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual( Comparacion ) then
     begin
          EmpleadosTransferibles.Caption := Format( 'Empleados Transferibles: %.0n', [ FEmpleadoCount / 1 ] );
          with ProgressBar do
          begin
               Step := 1;
               Max := FEmpleadoCount;
               Position := 0;
          end;
     end;
end;

procedure TTransferirEmpleados.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   FLog: TTransferLog;
begin
     FLog := TTransferLog.Create;
     try
        with FLog do
        begin
             Start( LogFileName );
        end;
        ShowStatus( 'Empezando Transferencia' );
        with dmCompara do
        begin
             TransferirEmpleados( CallMeBack,
                                  FLog,
                                  GetEmpleadoBase,
                                  GetTransferirNominas,
                                  GetTransferirIMSS,
                                  GetSQLFilter,
                                  GetNivel0 );
        end;
        ShowStatus( 'Transferencia Terminada' );
        lOk := True;
     finally
            with FLog do
            begin
                 Close;
                 Free;
            end;
     end;
     if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver La Bit�cora ?', 0, mbOk ) then
        ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( LogFileName ), ExtractFilePath( LogFileName ), SW_SHOWDEFAULT );
end;

procedure TTransferirEmpleados.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     lOk := True;
     Close;
end;

end.
