unit FMigrarDatos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, Checklst, ExtCtrls, Buttons, FileCtrl, Mask,
     FMigrar,
     ZetaWizard,
     ZetaFecha,
     ZetaNumero, jpeg;

type
  TMigrarDatos = class(TMigrar)
    Empresas: TTabSheet;
    Parametros: TTabSheet;
    BaseDatosFuente: TGroupBox;
    DefaultsGB: TGroupBox;
    ListaFuente: TListBox;
    PanelDatos: TPanel;
    NombreLBL: TLabel;
    Nombre: TLabel;
    DirectorioLBL: TLabel;
    Directorio: TLabel;
    PathLBL: TLabel;
    Path: TEdit;
    PathSeek: TSpeedButton;
    ParametrosMigracion: TGroupBox;
    EstadoLBL: TLabel;
    Estados: TComboBox;
    AnioLBL: TLabel;
    Anio: TZetaNumero;
    FechaPrenomina: TZetaFecha;
    FechaPrenominaLBL: TLabel;
    TressDOS: TGroupBox;
    TressDOSPathSeek: TSpeedButton;
    TressDOSPathLBL: TLabel;
    TressDOSPath: TEdit;
    ModoMigrar: TRadioGroup;
    PathSuperLBL: TLabel;
    PathSuper: TEdit;
    PathSuperSeek: TSpeedButton;
    MigrarSuper: TCheckBox;
    VersionDOS: TRadioGroup;
    RecalcularKardex: TCheckBox;
    FechaMinima: TZetaFecha;
    FechaMinimaLBL: TLabel;
    AnioAcumuladosLBL: TLabel;
    AnioAcumulados: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure ListaFuenteClick(Sender: TObject);
    procedure PathSuperSeekClick(Sender: TObject);
    procedure MigrarSuperClick(Sender: TObject);
  private
    { Private declarations }
    function GetAnioDefault: Integer;
    function GetAnioAcumulados: Integer;
    function GetEstadoDefault: String;
    function GetModoMigrar: eModoMigrar;
    function GetPathSuper: String;
    function GetEsVer260: Boolean;
    function GetRecalcularKardex: Boolean;
    function GetVersionDosName: String;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property AnioDefault: Integer read GetAnioDefault;
    property EstadoDefault: String read GetEstadoDefault;
  end;

var
  MigrarDatos: TMigrarDatos;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaMigrar,
     FHelpContext,
     DMigrar,
     DMigrarDatos;

{$R *.DFM}

{ ********** TMigrarDatos ************ }

procedure TMigrarDatos.FormCreate(Sender: TObject);
begin
     FListaFuente := ListaFuente;
     FNombre := Nombre;
     FDirectorio := Directorio;
     FParadoxPath := Path;
     FDOSPath := TressDOSPath;
     dmMigracion := TdmMigrarDatos.Create( Self );
     Anio.Valor := TheYear( Now );
     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Fuentes\Config' );
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Migrar.log' );
     FechaPrenomina.Valor := ( Date - 15 );
     FechaMinima.Valor := ZetaCommonTools.DateTheYear( Date, ZetaCommonTools.TheYear( Date ) - 2 );
     inherited;
     HelpContext := H00003_Migrar_empresa;
end;

procedure TMigrarDatos.FormDestroy(Sender: TObject);
begin
     inherited;
     dmMigracion.Free;
end;

function TMigrarDatos.GetEstadoDefault: String;
begin
     with Estados do
     begin
          if ( ItemIndex < 0 ) then
             Result := ''
          else
              Result := Trim( Items.Names[ ItemIndex ] );
     end;
end;

function TMigrarDatos.GetAnioDefault: Integer;
begin
     Result := Anio.ValorEntero;
end;

function TMigrarDatos.GetAnioAcumulados: Integer;
begin
     with AnioAcumulados do
     begin
          if ( ItemIndex < 0 ) then
             Result := AnioDefault
          else
              Result := StrToIntDef( Items.Strings[ ItemIndex ], AnioDefault );
     end;
end;

function TMigrarDatos.GetRecalcularKardex: Boolean;
begin
     Result := RecalcularKardex.Checked;
end;

function TMigrarDatos.GetModoMigrar: eModoMigrar;
begin
     Result := eModoMigrar( ModoMigrar.ItemIndex );
end;

function TMigrarDatos.GetPathSuper: String;
begin
     if MigrarSuper.Checked then
        Result := PathSuper.Text
     else
         Result := '';
end;

function TMigrarDatos.GetEsVer260: Boolean;
begin
     Result := ( VersionDOS.ItemIndex > 0 );
end;

function TMigrarDatos.GetVersionDosName: String;
begin
     with VersionDOS do
     begin
          Result := Items[ ItemIndex ];
     end;
end;

procedure TMigrarDatos.MigrarSuperClick(Sender: TObject);
begin
     inherited;
     with MigrarSuper do
     begin
          PathSuperLBL.Enabled := Checked;
          PathSuper.Enabled := Checked;
          PathSuperSeek.Enabled := Checked;
     end;
end;

procedure TMigrarDatos.PathSuperSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     sDirectory := PathSuper.Text;
     if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
        PathSuper.Text := sDirectory;
end;

procedure TMigrarDatos.ListaFuenteClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TMigrarDatos.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
   dMinima: TDate;
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante then
          begin
               if Wizard.EsPaginaActual( DirectorioDOS ) then
                  CanMove := ValidaDbaseDriver and
                             ValidaDirectorioParadox and
                             ValidaDirectorioDOS and
                             LlenaListaCompanys
               else
                   if Wizard.EsPaginaActual( Empresas ) then
                   begin
                        CanMove := ValidaEmpresa;
                        if CanMove then
                        begin
                             oCursor := Screen.Cursor;
                             Screen.Cursor := crHourglass;
                             try
                                try
                                   with Estados do
                                   begin
                                        TdmMigrarDatos( dmMigracion ).LlenaListaEstados( Items, SourceDatosPath );
                                        ItemIndex := 0;
                                   end;
                                except
                                      on Error: Exception do
                                      begin
                                           ShowError( 'Error Al Llenar Lista De Estados', Error );
                                           ActiveControl := ListaFuente;
                                           CanMove := False;
                                      end;
                                end;
                             finally
                                    Screen.Cursor := oCursor;
                             end;
                        end;
                   end
                   else
                       if Wizard.EsPaginaActual( Parametros ) then
                       begin
                            dMinima := EncodeDate( 1990, 1, 1 );
                            if ( AnioDefault < 1920 ) then
                            begin
                                 ZetaDialogo.zWarning( Caption, 'A�o De N�mina Inv�lido', 0, mbOK );
                                 ActiveControl := Anio;
                                 CanMove := False;
                            end
                            else
                                if ( FechaMinima.Valor < dMinima ) then
                                begin
                                     ZetaDialogo.zWarning( Caption, Format( 'Fecha M�nima De Tarjetas Debe Ser Mayor Al %s', [ FormatDateTime( 'dd/mmm/yyyy', dMinima ) ] ), 0, mbOK );
                                     ActiveControl := FechaMinima;
                                     CanMove := False;
                                end
                                else
                                    if MigrarSuper.Checked and not DirectoryExists( GetPathSuper ) then
                                    begin
                                         ZetaDialogo.zWarning( Caption, 'Directorio De Supervisores No Existe', 0, mbOK );
                                         ActiveControl := PathSuper;
                                         CanMove := False;
                                    end;
                       end;
          end;
     end;
end;

procedure TMigrarDatos.WizardAfterMove(Sender: TObject);
var
   iYear: Integer;
   sPath, sNewPath: String;
begin
     if Wizard.EsPaginaActual( Empresas ) then
     begin
          with ListaFuente do
          begin
               if ( Items.Count > 0 ) and ( ItemIndex < 0 ) then
                  ItemIndex := 0;
          end;
          SetControls;
     end
     else
         if Wizard.EsPaginaActual( Parametros ) then
         begin
              sPath := SourceDatosPath + '%d\';
              with AnioAcumulados do
              begin
                   with Items do
                   begin
                        BeginUpdate;
                        Clear;
                        for iYear := ( AnioDefault - 1 ) downto 1988 do
                        begin
                             sNewPath := Format( sPath, [ iYear ] );
                             if FileExists( sNewPath + 'ACUMULA.DBF' ) then
                             begin
                                  Add( IntToStr( iYear ) );
                             end;
                        end;
                        EndUpdate;
                        Enabled := ( Count > 0 );
                   end;
                   AnioAcumuladosLBL.Enabled := Enabled;
              end;
              PathSuper.Text := ZetaCommonTools.VerificaDir( SourceSharedPath ) + 'SUPER\';
         end;
end;

procedure TMigrarDatos.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     with FParamMsg do
     begin
          Clear;
          Add( 'Versi�n de Tress DOS:     ' + GetVersionDosName );
          Add( 'Estado Default:           ' + EstadoDefault );
          Add( 'A�o Default:              ' + IntToStr( AnioDefault ) );
          Add( 'Migrar Acumulados Desde:  ' + IntToStr( GetAnioAcumulados ) );
          Add( 'Rescatar Checadas Desde:  ' + FechaCorta( FechaPrenomina.Valor ) );
          Add( 'Fecha M�nima de Tarjetas: ' + FechaCorta( FechaMinima.Valor ) );
          Add( 'Migrar Supervisores:      ' + BoolToYesNo( MigrarSuper.Checked ) );
          Add( 'Directorio Supervisores:  ' + GetPathSuper );
          Add( 'Recalcular Kardex:        ' + BoolToYesNo( GetRecalcularKardex ) );
     end;
     if PrepararArchivos( GetModoMigrar, FechaPreNomina.Valor, FechaMinima.Valor, GetPathSuper, GetEsVer260 ) then
     begin
          StartLog;
          with dmMigracion as TdmMigrarDatos do
          begin
               Estado := EstadoDefault;
               Anio := AnioDefault;
               Acumulados := GetAnioAcumulados;
               DirSupervisores := GetPathSuper;
               EsVer260 := GetEsVer260;
               FechaMinima := Self.FechaMinima.Valor;
               RecalcularKardex := GetRecalcularKardex;
          end;
          lOk := MigrarDatos;
          EndLog;
     end;
     inherited;
end;

end.
