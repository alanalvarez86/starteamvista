exec sp_addtype Imagen, 'image', 'NULL'
go

exec sp_addtype Memo, 'text', 'NULL'
go

exec sp_addtype Fecha, 'datetime', 'NOT NULL'
exec sp_bindefault FechaVacia, Fecha
go

exec sp_addtype Anio, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Anio
go

exec sp_addtype Concepto, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Concepto
go

exec sp_addtype DescINFO, 'numeric(15,4)', 'NOT NULL'
exec sp_bindefault Cero, DescINFO
go

exec sp_addtype Dias, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Dias
go

exec sp_addtype DiasFrac, 'numeric(15,2)', 'NOT NULL'
exec sp_bindefault Cero, DiasFrac
go

exec sp_addtype Empleados, 'int', 'NOT NULL'
exec sp_bindefault Cero, Empleados
go

exec sp_addtype FolioChico, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, FolioChico
go

exec sp_addtype FolioGrande, 'int', 'NOT NULL'
exec sp_bindefault Cero, FolioGrande
go

exec sp_addtype Horas, 'numeric(15,2)', 'NOT NULL'
exec sp_bindefault Cero, Horas
go

exec sp_addtype Mes, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Mes
go

exec sp_addtype Minutos, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Minutos
go

exec sp_addtype NominaNumero, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, NominaNumero
go

exec sp_addtype NominaTipo, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, NominaTipo
go

exec sp_addtype Numerico, 'numeric(15,2)', 'NOT NULL'
exec sp_bindefault Cero, Numerico
go

exec sp_addtype NumeroEmpleado, 'int', 'NOT NULL'
exec sp_bindefault Cero, NumeroEmpleado
go

exec sp_addtype Pesos, 'numeric(15,2)', 'NOT NULL'
exec sp_bindefault Cero, Pesos
go

exec sp_addtype PesosDiario, 'numeric(15,2)', 'NOT NULL'
exec sp_bindefault Cero, PesosDiario
go

exec sp_addtype PesosEmpresa, 'numeric(15,2)', 'NOT NULL'
exec sp_bindefault Cero, PesosEmpresa
go

exec sp_addtype SalariosMinimos, 'numeric(15,2)', 'NOT NULL'
exec sp_bindefault Cero, SalariosMinimos
go

exec sp_addtype Status, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Status
go

exec sp_addtype Tamanio, 'numeric(15,3)', 'NOT NULL'
exec sp_bindefault Cero, Tamanio
go

exec sp_addtype Tasa, 'numeric(15,5)', 'NOT NULL'
exec sp_bindefault Cero, Tasa
go

exec sp_addtype TipoLiq, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, TipoLiq
go

exec sp_addtype Usuario, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Usuario
go

exec sp_addtype Booleano, 'char(1)', 'NOT NULL'
exec sp_bindefault BooleanoNO, Booleano
go

exec sp_addtype CamposRequeridos, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, CamposRequeridos
go

exec sp_addtype Codigo, 'char(6)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Codigo
go

exec sp_addtype Codigo1, 'char(1)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Codigo1
go

exec sp_addtype Codigo2, 'char(2)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Codigo2
go

exec sp_addtype Codigo3, 'char(3)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Codigo3
go

exec sp_addtype Codigo4, 'char(4)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Codigo4
go

exec sp_addtype CodigoEmpresa, 'char(10)', 'NOT NULL'
exec sp_bindefault StringVacio, CodigoEmpresa
go

exec sp_addtype Condicion, 'char(8)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Condicion
go

exec sp_addtype DescLarga, 'varchar(40)', 'NOT NULL'
exec sp_bindefault StringVacio, DescLarga
go

exec sp_addtype Descripcion, 'varchar(30)', 'NOT NULL'
exec sp_bindefault StringVacio, Descripcion
go

exec sp_addtype Evento, 'char(10)', 'NOT NULL'
exec sp_bindefault CodigoVacio, Evento
go

exec sp_addtype Formula, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, Formula
go

exec sp_addtype Hora, 'char(4)', 'NOT NULL'
exec sp_bindefault StringVacio, Hora
go

exec sp_addtype HoraMinutosSegundos, 'varchar(8)', 'NOT NULL'
exec sp_bindefault StringVacio, HoraMinutosSegundos
go

exec sp_addtype Mascara, 'varchar(20)', 'NOT NULL'
exec sp_bindefault StringVacio, Mascara
go

exec sp_addtype NombreCampo, 'varchar(10)', 'NOT NULL'
exec sp_bindefault StringVacio, NombreCampo
go

exec sp_addtype NumeroPatronal, 'varchar(11)', 'NOT NULL'
exec sp_bindefault StringVacio, NumeroPatronal
go

exec sp_addtype Observaciones, 'varchar(50)', 'NOT NULL'
exec sp_bindefault StringVacio, Observaciones
go

exec sp_addtype Passwrd, 'char(8)', 'NOT NULL'
exec sp_bindefault StringVacio, Passwrd
go

exec sp_addtype PathArchivo, 'varchar(150)', 'NOT NULL'
exec sp_bindefault StringVacio, PathArchivo
go

exec sp_addtype Referencia, 'varchar(8)', 'NOT NULL'
exec sp_bindefault StringVacio, Referencia
go

exec sp_addtype RegPatronal, 'char(1)', 'NOT NULL'
exec sp_bindefault CodigoVacio, RegPatronal
go

exec sp_addtype RelojLinx, 'varchar(4)', 'NOT NULL'
exec sp_bindefault StringVacio, RelojLinx
go

exec sp_addtype Titulo, 'varchar(100)', 'NOT NULL'
exec sp_bindefault StringVacio, Titulo
go

exec sp_addtype TituloCampo, 'varchar(30)', 'NOT NULL'
exec sp_bindefault StringVacio, TituloCampo
go

exec sp_addtype WindowsName, 'varchar(32)', 'NOT NULL'
exec sp_bindefault StringVacio, WindowsName
go

exec sp_addtype ZonaGeo, 'char(1)', 'NOT NULL'
exec sp_bindefault StringVacio, ZonaGeo
go
