CREATE TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	declare @NewCodigo Condicion, @OldCodigo Condicion;

	select @NewCodigo = QU_CODIGO from Inserted;
        select @OldCodigo = QU_CODIGO from Deleted;

	IF ( @OldCodigo <> '' ) AND ( @NewCodigo <> '' )
	BEGIN
	     UPDATE REPORTE SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
             UPDATE PUESTO SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
             UPDATE REQUIERE SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
	END
  END
END
GO

CREATE TRIGGER TU_TABLA01 ON TABLA01 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update SOLICITA set SOLICITA.SO_G_TAB_1 = @NewCodigo
        where ( SOLICITA.SO_G_TAB_1 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TABLA02 ON TABLA02 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update SOLICITA set SOLICITA.SO_G_TAB_2 = @NewCodigo
        where ( SOLICITA.SO_G_TAB_2 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TABLA03 ON TABLA03 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update SOLICITA set SOLICITA.SO_G_TAB_3 = @NewCodigo
        where ( SOLICITA.SO_G_TAB_3 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TABLA04 ON TABLA04 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update SOLICITA set SOLICITA.SO_G_TAB_4 = @NewCodigo
        where ( SOLICITA.SO_G_TAB_4 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TABLA05 ON TABLA05 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update SOLICITA set SOLICITA.SO_G_TAB_5 = @NewCodigo
        where ( SOLICITA.SO_G_TAB_5 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_CLIENTE ON CLIENTE AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CL_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = CL_CODIGO from Inserted;
        select @OldCodigo = CL_CODIGO from Deleted;

        update REQUIERE set REQUIERE.RQ_CLIENTE = @NewCodigo
        where ( REQUIERE.RQ_CLIENTE = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_AINTERES ON AINTERES AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update PUESTO set PUESTO.PU_AREA = @NewCodigo
        where ( PUESTO.PU_AREA = @OldCodigo )
        update REQUIERE set REQUIERE.RQ_AREA = @NewCodigo
        where ( REQUIERE.RQ_AREA = @OldCodigo )
        update SOLICITA set SOLICITA.SO_AREA_3 = @NewCodigo
        where ( SOLICITA.SO_AREA_3 = @OldCodigo )
        update SOLICITA set SOLICITA.SO_AREA_2 = @NewCodigo
        where ( SOLICITA.SO_AREA_2 = @OldCodigo )
        update SOLICITA set SOLICITA.SO_AREA_1 = @NewCodigo
        where ( SOLICITA.SO_AREA_1 = @OldCodigo )

  END
END
GO

CREATE TRIGGER TU_TGASTO ON TGASTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update REQGASTO set REQGASTO.RG_TIPO = @NewCodigo
        where ( REQGASTO.RG_TIPO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_PUESTO ON PUESTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( PU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = PU_CODIGO from Inserted;
        select @OldCodigo = PU_CODIGO from Deleted;

        update REQUIERE set REQUIERE.RQ_PUESTO = @NewCodigo
        where ( REQUIERE.RQ_PUESTO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TD_REQUIERE ON REQUIERE AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update ENTREVIS set ENTREVIS.RQ_FOLIO = 0 from ENTREVIS, Deleted where ( ENTREVIS.RQ_FOLIO = Deleted.RQ_FOLIO );
END
GO

CREATE TRIGGER TU_REQUIERE ON REQUIERE AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( RQ_FOLIO )
  BEGIN
	declare @NewCodigo FOLIOGRANDE, @OldCodigo FOLIOGRANDE;

	select @NewCodigo = RQ_FOLIO from Inserted;
        select @OldCodigo = RQ_FOLIO from Deleted;

        update ENTREVIS set ENTREVIS.RQ_FOLIO = @NewCodigo where ( ENTREVIS.RQ_FOLIO = @OldCodigo );
  END
END
GO
