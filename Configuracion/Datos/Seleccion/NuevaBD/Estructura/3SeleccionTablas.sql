CREATE TABLE DICCION (
       DI_CLASIFI           Status,
       DI_NOMBRE            NombreCampo,
       DI_TITULO            TituloCampo,
       DI_CALC              Status,
       DI_ANCHO             Status,
       DI_MASCARA           Descripcion,
       DI_TFIELD            Status,
       DI_ORDEN             Booleano,
       DI_REQUIER           CamposRequeridos,
       DI_TRANGO            Status,
       DI_NUMERO            Status,
       DI_VALORAC           Descripcion,
       DI_RANGOAC           Status,
       DI_CLAVES            TituloCampo,
       DI_TCORTO            TituloCampo,
       DI_CONFI             Booleano
)
GO

CREATE INDEX XIE1DICCION ON DICCION
(
       DI_CLASIFI,
       DI_CALC
)
GO

ALTER TABLE DICCION
       ADD CONSTRAINT PK_DICCION PRIMARY KEY (DI_CLASIFI, DI_NOMBRE)
GO

CREATE TABLE BITACORA (
       US_CODIGO            Usuario,
       BI_FECHA             Fecha,
       BI_HORA              HoraMinutosSegundos,
       BI_PROC_ID           Status,
       BI_TIPO              Status,
       BI_NUMERO            FolioGrande,
       BI_TEXTO             Observaciones,
       CB_CODIGO            NumeroEmpleado,
       BI_DATA              Memo,
       BI_CLASE             Status,
       BI_FEC_MOV           Fecha
)
GO

CREATE INDEX XIE1BITACORA ON BITACORA
(
       BI_NUMERO
)
GO

CREATE INDEX XIE2BITACORA ON BITACORA
(
       BI_FECHA,
       BI_HORA
)
GO

CREATE TABLE REPORTE (
       RE_CODIGO            FolioChico,
       RE_NOMBRE            Descripcion,
       RE_TIPO              Status,
       RE_TITULO            Titulo,
       RE_ENTIDAD           Status,
       RE_SOLOT             Booleano,
       RE_FECHA             Fecha,
       RE_GENERAL           Descripcion,
       RE_REPORTE           PathArchivo,
       RE_CFECHA            NombreCampo,
       RE_IFECHA            Status,
       RE_HOJA              Status,
       RE_ALTO              Tamanio,
       RE_ANCHO             Tamanio,
       RE_PRINTER           Formula,
       RE_COPIAS            Status,
       RE_PFILE             Status,
       RE_ARCHIVO           PathArchivo,
       RE_VERTICA           Booleano,
       US_CODIGO            Usuario,
       RE_FILTRO            Formula,
       RE_COLNUM            Status,
       RE_COLESPA           Tamanio,
       RE_RENESPA           Tamanio,
       RE_MAR_SUP           Tamanio,
       RE_MAR_IZQ           Tamanio,
       RE_MAR_DER           Tamanio,
       RE_MAR_INF           Tamanio,
       RE_NIVEL             Status,
       RE_FONTNAM           WindowsName,
       RE_FONTSIZ           Status,
       QU_CODIGO            Condicion,
       RE_CLASIFI           Status,
       RE_CANDADO           Status
)
GO

CREATE UNIQUE INDEX XAK1REPORTE ON REPORTE
(
       RE_NOMBRE
)
GO

CREATE INDEX XIE1REPORTE ON REPORTE
(
       RE_CLASIFI,
       RE_NOMBRE
)
GO


ALTER TABLE REPORTE
       ADD CONSTRAINT PK_REPORTE PRIMARY KEY (RE_CODIGO)
GO

CREATE TABLE CAMPOREP (
       RE_CODIGO            FolioChico,
       CR_TIPO              Status,
       CR_POSICIO           Status,
       CR_CLASIFI           Status,
       CR_SUBPOS            Status,
       CR_TABLA             Status,
       CR_TITULO            TituloCampo,
       CR_REQUIER           Formula,
       CR_CALC              Status,
       CR_MASCARA           Mascara,
       CR_ANCHO             Status,
       CR_OPER              Status,
       CR_TFIELD            Status,
       CR_SHOW              Status,
       CR_DESCRIP           NombreCampo,
       CR_COLOR             Status,
       CR_FORMULA           Formula,
       CR_BOLD              Booleano,
       CR_ITALIC            Booleano,
       CR_SUBRAYA           Booleano,
       CR_STRIKE            Booleano,
       CR_ALINEA            Status
)
GO

CREATE TABLE MISREPOR (
       US_CODIGO            Usuario,
       RE_CODIGO            FolioChico )
GO

ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

ALTER TABLE CAMPOREP
       ADD CONSTRAINT PK_CAMPOREP PRIMARY KEY (RE_CODIGO, CR_TIPO, CR_POSICIO, 
              CR_SUBPOS)
GO

CREATE TABLE GLOBAL (
       GL_CODIGO            FolioChico,
       GL_DESCRIP           Descripcion,
       GL_FORMULA           Formula,
       GL_TIPO              Status,
       US_CODIGO            Usuario,
       GL_CAPTURA           Fecha,
       GL_NIVEL             Status
)
GO


ALTER TABLE GLOBAL
       ADD CONSTRAINT PK_GLOBAL PRIMARY KEY (GL_CODIGO)
GO

CREATE TABLE PROCESO (
       PC_PROC_ID           Status,
       PC_NUMERO            FolioGrande,
       US_CODIGO            Usuario,
       PC_FEC_INI           Fecha,
       PC_HOR_INI           HoraMinutosSegundos,
       PC_FEC_FIN           Fecha,
       PC_HOR_FIN           HoraMinutosSegundos,
       PC_ERROR             Booleano,
       CB_CODIGO            NumeroEmpleado,
       PC_MAXIMO            FolioGrande,
       PC_PASO              FolioGrande,
       US_CANCELA           Usuario,
       PC_PARAMS            Formula
)
GO

CREATE INDEX XIE1PROCESO ON PROCESO
(
       PC_FEC_INI,
       PC_ERROR
)
GO


ALTER TABLE PROCESO
       ADD CONSTRAINT PK_PROCESO PRIMARY KEY (PC_NUMERO)
GO

CREATE TABLE QUERYS (
       QU_CODIGO            Condicion,
       QU_DESCRIP           Formula,
       QU_FILTRO            Formula,
       QU_NIVEL             Status,
       US_CODIGO            Usuario
)
GO


ALTER TABLE QUERYS
       ADD CONSTRAINT PK_QUERYS PRIMARY KEY (QU_CODIGO)
GO

CREATE TABLE SUSCRIP (
       RE_CODIGO            FolioChico,
       US_CODIGO            Usuario,
       SU_FRECUEN           Status,
       SU_ENVIAR            Status,
       SU_VACIO             Status
)
GO

ALTER TABLE SUSCRIP
       ADD CONSTRAINT PK_SUSCRIP PRIMARY KEY (RE_CODIGO, US_CODIGO)
GO

CREATE TABLE AINTERES (
       TB_CODIGO CODIGO,
       TB_ELEMENT DESCRIPCION,
       TB_INGLES DESCRIPCION,
       TB_NUMERO PESOS,
       TB_TEXTO DESCRIPCION
)
GO

ALTER TABLE AINTERES
      ADD CONSTRAINT PK_AINTERES PRIMARY KEY (TB_CODIGO )
GO

CREATE TABLE CANDIDAT (
       RQ_FOLIO FOLIOGRANDE,
       SO_FOLIO FOLIOGRANDE,
       CD_COMENT DESCLARGA,
       CD_STATUS STATUS
)
GO

ALTER TABLE CANDIDAT
      ADD CONSTRAINT PK_CANDIDAT PRIMARY KEY ( SO_FOLIO, RQ_FOLIO )
GO

CREATE TABLE CLIENTE (
       CL_CODIGO CODIGO,
       CL_NOMBRE OBSERVACIONES,
       CL_DIRECC OBSERVACIONES,
       CL_TEL DESCLARGA,
       CL_FAX DESCLARGA,
       CL_DESCRIP DESCRIPCION,
       CL_EMPLEAD NUMEROEMPLEADO,
       CL_INDUSTR DESCRIPCION,
       CL_NOM_C1 DESCRIPCION,
       CL_PUES_C1 DESCRIPCION,
       CL_MAIL_C1 DESCLARGA,
       CL_DESC_C1 DESCRIPCION,
       CL_NOM_C2 DESCRIPCION,
       CL_PUES_C2 DESCRIPCION,
       CL_MAIL_C2 DESCLARGA,
       CL_DESC_C2 DESCRIPCION,
       CL_OBSERVA MEMO,
       CL_PUES_AU DescLarga,
       CL_NOM_AU DescLarga
)
GO

ALTER TABLE CLIENTE
      ADD CONSTRAINT PK_CLIENTE PRIMARY KEY ( CL_CODIGO )
GO

CREATE TABLE DOCUMENT (
       SO_FOLIO FOLIOGRANDE,
       DO_TIPO CODIGO,
       DO_EXT CODIGO,
       DO_BLOB IMAGEN,
       DO_OBSERVA DESCRIPCION
)
GO

ALTER TABLE DOCUMENT
      ADD CONSTRAINT PK_DOCUMENT PRIMARY KEY (SO_FOLIO, DO_TIPO)
GO

CREATE TABLE ENTREVIS (
       SO_FOLIO FOLIOGRANDE,
       ER_FOLIO FOLIOCHICO,
       RQ_FOLIO FOLIOGRANDE,
       ER_FECHA FECHA,
       ER_NOMBRE DESCLARGA,
       ER_RESUMEN DESCLARGA,
       ER_DETALLE MEMO,
       ER_RESULT STATUS
)
GO

ALTER TABLE ENTREVIS
      ADD CONSTRAINT PK_ENTREVIS PRIMARY KEY (SO_FOLIO, ER_FOLIO)
GO

CREATE TABLE EXAMEN (
       SO_FOLIO FOLIOGRANDE,
       EX_FOLIO FOLIOCHICO,
       EX_FECHA FECHA,
       EX_TIPO STATUS,
       EX_APLICO DESCLARGA,
       EX_RESUMEN DESCLARGA,
       EX_EVALUA1 NUMERICO,
       EX_EVALUA2 NUMERICO,
       EX_EVALUA3 NUMERICO,
       EX_OBSERVA MEMO,
       EX_APROBO BOOLEANO
)
GO

ALTER TABLE EXAMEN
      ADD CONSTRAINT PK_EXAMEN PRIMARY KEY (SO_FOLIO, EX_FOLIO)
GO

CREATE TABLE PUESTO (
       PU_CODIGO CODIGO,
       PU_DESCRIP DESCRIPCION,
       PU_INGLES DESCRIPCION,
       PU_NUMERO PESOS,
       PU_TEXTO DESCRIPCION,
       QU_CODIGO CONDICION,
       PU_HABILID MEMO,
       PU_ACTIVID MEMO,
       PU_OFERTA FORMULA,
       PU_AREA CODIGO,
       PU_EDADMIN FOLIOCHICO,
       PU_EDADMAX FOLIOCHICO,
       PU_SEXO CODIGO1,
       PU_ESTUDIO STATUS,
       PU_DOMINA1 FOLIOCHICO,
       PU_SUELDO DESCRIPCION,
       PU_VACACI DESCRIPCION,
       PU_AGUINAL DESCRIPCION,
       PU_BONO DESCRIPCION,
       PU_VALES DESCRIPCION
)
GO

ALTER TABLE PUESTO
      ADD CONSTRAINT PK_PUESTO PRIMARY KEY (PU_CODIGO)
GO

CREATE TABLE REF_LAB (
       SO_FOLIO FOLIOGRANDE,
       RL_FOLIO FOLIOCHICO,
       RL_CAMPO1 OBSERVACIONES,
       RL_CAMPO2 OBSERVACIONES,
       RL_CAMPO3 OBSERVACIONES,
       RL_CAMPO4 OBSERVACIONES,
       RL_CAMPO5 OBSERVACIONES,
       RL_CAMPO6 OBSERVACIONES,
       RL_CAMPO7 OBSERVACIONES,
       RL_CAMPO8 OBSERVACIONES,
       RL_CAMPO9 OBSERVACIONES,
       RL_CAMPO10 OBSERVACIONES,
       RL_CAMPO11 Observaciones,
	RL_CAMPO12 Observaciones,
	RL_CAMPO13 Observaciones,
	RL_CAMPO14 Observaciones,
	RL_CAMPO15 Observaciones,
	RL_CAMPO16 Observaciones,
	RL_CAMPO17 Observaciones,
	RL_CAMPO18 Observaciones,
	RL_CAMPO19 Observaciones,
	RL_CAMPO20 Observaciones,
	RL_EVAL1 Observaciones,
	RL_EVAL2 Observaciones,
	RL_EVAL3 Observaciones,
	RL_EVAL4 Observaciones,
	RL_EVAL5 Observaciones,
	RL_EVAL6 Observaciones,
	RL_EVAL7 Observaciones,
	RL_EVAL8 Observaciones,
	RL_EVAL9 Observaciones,
	RL_EVAL10 Observaciones,
	RL_EVAL11 Observaciones,
	RL_EVAL12 Observaciones,
	RL_EVAL13 Observaciones,
	RL_EVAL14 Observaciones,
	RL_EVAL15 Observaciones,
	RL_EVAL16 Observaciones,
	RL_EVAL17 Observaciones,
	RL_EVAL18 Observaciones,
	RL_EVAL19 Observaciones,
	RL_EVAL20 Observaciones


)
GO

ALTER TABLE REF_LAB
      ADD CONSTRAINT PK_REF_LAB PRIMARY KEY (SO_FOLIO, RL_FOLIO)
GO

CREATE TABLE REQGASTO (
       RQ_FOLIO FOLIOGRANDE,
       RG_FOLIO FOLIOCHICO,
       RG_TIPO CODIGO,
       RG_FECHA FECHA,
       RG_DESCRIP DESCLARGA,
       RG_COMENTA FORMULA,
       RG_MONTO PESOS
)
GO

ALTER TABLE REQGASTO
      ADD CONSTRAINT PK_REQGASTO PRIMARY KEY (RQ_FOLIO, RG_FOLIO)
GO

CREATE TABLE REQUIERE (
       RQ_FOLIO FOLIOGRANDE,
       RQ_FEC_INI FECHA,
       RQ_PUESTO CODIGO,
       QU_CODIGO CONDICION,
       RQ_CLIENTE CODIGO,
       RQ_AREA CODIGO,
       RQ_TURNO DESCLARGA,
       RQ_SEXO CODIGO1,
       RQ_INGLES FOLIOCHICO,
       RQ_EDADMIN FOLIOCHICO,
       RQ_EDADMAX FOLIOCHICO,
       RQ_STATUS STATUS,
       RQ_GTOPRES PESOS,
       RQ_CONTRAT EMPLEADOS,
       RQ_FEC_PRO FECHA,
       RQ_GTOREAL PESOS,
       RQ_FEC_FIN FECHA,
       RQ_OBSERVA MEMO,
       RQ_HABILID MEMO,
       RQ_ACTIVID MEMO,
       RQ_AUTORIZ DESCLARGA,
       RQ_ANUNCIO MEMO,
       RQ_PRIORID STATUS,
       RQ_OFERTA FORMULA,
       RQ_ESTUDIO STATUS,
       RQ_VACANTE EMPLEADOS,
       RQ_SELECCI EMPLEADOS,
       RQ_CANDIDA EMPLEADOS,
       RQ_MOTIVO STATUS,
       RQ_SUELDO DESCRIPCION,
       RQ_VACACI DESCRIPCION,
       RQ_AGUINAL DESCRIPCION,
       RQ_BONO DESCRIPCION,
       RQ_VALES DESCRIPCION,
       RQ_TIPO_CO     Codigo2,
       RQ_REEMPLA     Titulo,
       RQ_PUES_AU DescLarga
  
)
GO

ALTER TABLE REQUIERE
      ADD CONSTRAINT PK_REQUIERE PRIMARY KEY (RQ_FOLIO)
GO

CREATE TABLE SOLICITA (
       SO_FOLIO FOLIOGRANDE,
       SO_STATUS STATUS,
       SO_APE_MAT DESCRIPCION,
       SO_APE_PAT DESCRIPCION,
       SO_NOMBRES DESCRIPCION,
       SO_AREA_1 CODIGO,
       SO_AREA_2 CODIGO,
       SO_AREA_3 CODIGO,
       SO_EDO_CIV CODIGO1,
       SO_ESTUDIO STATUS,
       SO_FEC_INI FECHA,
       SO_FEC_NAC FECHA,
       SO_PAIS         Descripcion,
       SO_CURP         Descripcion,
       SO_RFC          Descripcion,
       SO_CALLE        Observaciones,
       SO_COLONIA      Descripcion,
       SO_CODPOST      Referencia,
       SO_CIUDAD       Descripcion,
       SO_ESTADO       Codigo2,
       SO_NACION       Descripcion,
       SO_FEC_RES      Fecha,
       SO_ESCUELA      DescLarga,
       SO_G_FEC_1 FECHA,
       SO_G_FEC_2 FECHA,
       SO_G_FEC_3 FECHA,
       SO_G_FEC_4 FECHA,
       SO_G_FEC_5 FECHA,
       SO_G_LOG_1 BOOLEANO,
       SO_G_LOG_2 BOOLEANO,
       SO_G_LOG_3 BOOLEANO,
       SO_G_LOG_4 BOOLEANO,
       SO_G_LOG_5 BOOLEANO,
       SO_G_LOG_6      Booleano,
       SO_G_LOG_7      Booleano,
       SO_G_LOG_8      Booleano,
       SO_G_LOG_9      Booleano,
       SO_G_LOG10      Booleano,
       SO_G_NUM_1 PESOS,
       SO_G_NUM_2 PESOS,
       SO_G_NUM_3 PESOS,
       SO_G_NUM_4 NUMERICO,
       SO_G_NUM_5 NUMERICO,
       SO_G_NUM_6      Numerico,
       SO_G_NUM_7      Numerico,
       SO_G_NUM_8      Numerico,
       SO_G_NUM_9      Pesos,
       SO_G_NUM10      Pesos,   
       SO_G_TAB_1 CODIGO,
       SO_G_TAB_2 CODIGO,
       SO_G_TAB_3 CODIGO,
       SO_G_TAB_4 CODIGO,
       SO_G_TAB_5 CODIGO,
       SO_G_TEX_1 DESCRIPCION,
       SO_G_TEX_2 DESCRIPCION,
       SO_G_TEX_3 DESCRIPCION,
       SO_G_TEX_4 DESCRIPCION,
       SO_G_TEX_5 DESCRIPCION,
       SO_G_TEX_6      Descripcion,
       SO_G_TEX_7      Descripcion,
       SO_G_TEX_8      Descripcion,
       SO_G_TEX_9      Descripcion,
       SO_G_TEX10      Descripcion,
       SO_G_TEX11      Descripcion,
       SO_G_TEX12      Descripcion,
       SO_G_TEX13      Descripcion,
       SO_G_TEX14      Descripcion,
       SO_G_TEX15      Descripcion,
       SO_G_TEX16      Descripcion,
       SO_G_TEX17      Descripcion,
       SO_G_TEX18      Descripcion,
       SO_G_TEX19      Descripcion,
       SO_G_TEX20      Descripcion,
       SO_PUE_SOL DESCLARGA,
       SO_FOTO IMAGEN,
       SO_INGLES FOLIOCHICO,
       SO_DOMINA2 FOLIOCHICO,
       SO_IDIOMA2 REFERENCIA,
       SO_SEXO CODIGO1,
       SO_TEL DESCLARGA,
       SO_EMAIL DESCLARGA,
       SO_PASSWRD DESCLARGA,
       SO_REQUI_1 BOOLEANO,
       SO_REQUI_2 BOOLEANO,
       SO_REQUI_3 BOOLEANO,
       SO_REQUI_4 BOOLEANO,
       SO_REQUI_5 BOOLEANO,
       SO_REQUI_6 BOOLEANO,
       SO_REQUI_7 BOOLEANO,
       SO_REQUI_8 BOOLEANO,
       SO_REQUI_9 BOOLEANO,
       SO_OBSERVA MEMO,
       SO_MED_TRA      Codigo1,
	  SO_NUM_EXT  NombreCampo, 
	  SO_NUM_INT NombreCampo,
       PRETTYNAME AS (SO_APE_PAT + ' ' + SO_APE_MAT + ', ' + SO_NOMBRES)
)
GO

ALTER TABLE SOLICITA
      ADD CONSTRAINT PK_SOLICITA PRIMARY KEY (SO_FOLIO)
GO

CREATE TABLE TABLA01 (
       TB_CODIGO CODIGO,
       TB_ELEMENT DESCRIPCION,
       TB_INGLES DESCRIPCION,
       TB_NUMERO PESOS,
       TB_TEXTO DESCRIPCION
)
GO

ALTER TABLE TABLA01
      ADD CONSTRAINT PK_TABLA01 PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE TABLA02 (
       TB_CODIGO CODIGO,
       TB_ELEMENT DESCRIPCION,
       TB_INGLES DESCRIPCION,
       TB_NUMERO PESOS,
       TB_TEXTO DESCRIPCION
)
GO

ALTER TABLE TABLA02
      ADD CONSTRAINT PK_TABLA02 PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE TABLA03 (
       TB_CODIGO CODIGO,
       TB_ELEMENT DESCRIPCION,
       TB_INGLES DESCRIPCION,
       TB_NUMERO PESOS,
       TB_TEXTO DESCRIPCION
)
GO

ALTER TABLE TABLA03
      ADD CONSTRAINT PK_TABLA03 PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE TABLA04 (
       TB_CODIGO CODIGO,
       TB_ELEMENT DESCRIPCION,
       TB_INGLES DESCRIPCION,
       TB_NUMERO PESOS,
       TB_TEXTO DESCRIPCION
)
GO

ALTER TABLE TABLA04
      ADD CONSTRAINT PK_TABLA04 PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE TABLA05 (
       TB_CODIGO CODIGO,
       TB_ELEMENT DESCRIPCION,
       TB_INGLES DESCRIPCION,
       TB_NUMERO PESOS,
       TB_TEXTO DESCRIPCION
)
GO

ALTER TABLE TABLA05
      ADD CONSTRAINT PK_TABLA05 PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE TGASTO (
       TB_CODIGO CODIGO,
       TB_ELEMENT DESCRIPCION,
       TB_INGLES DESCRIPCION,
       TB_NUMERO PESOS,
       TB_TEXTO DESCRIPCION
)
GO

ALTER TABLE TGASTO
      ADD CONSTRAINT PK_TGASTO PRIMARY KEY (TB_CODIGO)
GO
