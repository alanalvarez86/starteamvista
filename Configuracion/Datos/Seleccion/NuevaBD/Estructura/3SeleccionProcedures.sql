CREATE PROCEDURE DBO.INIT_PROCESS_LOG(
  @Proceso Integer,
  @Usuario Smallint,
  @Fecha   DateTime,
  @Hora    Varchar(8),
  @Maximo  Integer,
  @Params   varchar(255),
  @Folio   Integer OUTPUT )
AS
  SET NOCOUNT ON
  select @Folio = MAX( PC_NUMERO ) from PROCESO

  if ( @Folio is NULL )
    select @Folio = 1
  else
    select @Folio = @Folio + 1

  insert into PROCESO ( PC_PROC_ID,
                        PC_NUMERO,
                        US_CODIGO,
                        PC_FEC_INI,
                        PC_HOR_INI,
                        PC_MAXIMO,
                        PC_ERROR,
			PC_PARAMS ) values
                         ( @Proceso,
                           @Folio,
                           @Usuario,
                           @Fecha,
                           @Hora,
                           @Maximo,
                           'A',
			   @Params )
GO

CREATE PROCEDURE UPDATE_PROCESS_LOG( 
  @Proceso  Integer, 
  @Empleado Integer, 
  @Paso     Integer, 
  @Fecha    DateTime, 
  @Hora     VARCHAR( 8 ),
  @Status   SmallInt OUTPUT ) 
AS
  SET NOCOUNT ON
  declare @Bandera Char( 1 )

  update PROCESO set
         CB_CODIGO = @Empleado,
         PC_PASO = @Paso,
         PC_FEC_FIN = @Fecha,
         PC_HOR_FIN = @Hora
  where ( PC_NUMERO = @Proceso )

  select @Bandera = PC_ERROR
  from   PROCESO
  where ( PC_NUMERO = @Proceso )

  if ( @Bandera = 'A' )
    select @Status = 0
  else
    select @Status = 1

GO

CREATE PROCEDURE DBO.SP_TITULO_CAMPO(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
begin
        SET NOCOUNT ON
  	DECLARE @TITULO VARCHAR(30)

	select @Titulo = GL_FORMULA
  	from GLOBAL
  	where GL_CODIGO = @NumGlobal

  	if (( @Titulo is Null ) or ( @Titulo = '' ))
    		SET @Titulo = @TituloDefault;

   	update DICCION
  	set DI_TITULO = @Titulo, DI_TCORTO= @Titulo, DI_CLAVES = ''
  	where DI_CLASIFI IN (2,27)
	and DI_NOMBRE = @Nombre;
end
GO

CREATE PROCEDURE SP_TITULO_TABLA(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@TABLA SMALLINT)
AS
begin
        SET NOCOUNT ON
  	DECLARE @TITULO VARCHAR(30)

  	select @Titulo=GL_FORMULA
  	from GLOBAL
  	where GL_CODIGO = @NumGlobal

 	if (( @Titulo is Null ) or ( @Titulo = '' ))
    		SET @Titulo = @TituloDefault

  	update DICCION set DI_TITULO = @Titulo, DI_TCORTO= @Titulo, DI_CLAVES = ''
  	where DI_CLASIFI = -1 and DI_CALC = @Tabla
end
GO

CREATE PROCEDURE SP_REFRESH_DICCION
AS
BEGIN
     execute SP_TITULO_TABLA 37, 'Tabla Adicional #1', 12;
     execute SP_TITULO_TABLA 38, 'Tabla Adicional #2', 13;
     execute SP_TITULO_TABLA 39, 'Tabla Adicional #3', 14;
     execute SP_TITULO_TABLA 40, 'Tabla Adicional #4', 15;
     execute SP_TITULO_TABLA 41, 'Tabla Adicional #5', 16;

     execute SP_TITULO_CAMPO 17, 'Texto Adicional #1','SO_G_TEX_1';
     execute SP_TITULO_CAMPO 18, 'Texto Adicional #2','SO_G_TEX_2';
     execute SP_TITULO_CAMPO 19, 'Texto Adicional #3','SO_G_TEX_3';
     execute SP_TITULO_CAMPO 20, 'Texto Adicional #4','SO_G_TEX_4';
     execute SP_TITULO_CAMPO 21, 'Texto Adicional #5','SO_G_TEX_5';

     execute SP_TITULO_CAMPO 22, 'Numero Adicional #1','SO_G_NUM_1';
     execute SP_TITULO_CAMPO 23, 'Numero Adicional #2','SO_G_NUM_2';
     execute SP_TITULO_CAMPO 24, 'Numero Adicional #3','SO_G_NUM_3';
     execute SP_TITULO_CAMPO 25, 'Numero Adicional #4','SO_G_NUM_4';
     execute SP_TITULO_CAMPO 26, 'Numero Adicional #5','SO_G_NUM_5';

     execute SP_TITULO_CAMPO 27, 'Logico Adicional #1','SO_G_LOG_1';
     execute SP_TITULO_CAMPO 28, 'Logico Adicional #2','SO_G_LOG_2';
     execute SP_TITULO_CAMPO 29, 'Logico Adicional #3','SO_G_LOG_3';
     execute SP_TITULO_CAMPO 30, 'Logico Adicional #4','SO_G_LOG_4';
     execute SP_TITULO_CAMPO 31, 'Logico Adicional #5','SO_G_LOG_5';

     execute SP_TITULO_CAMPO 32, 'Fecha Adicional #1','SO_G_FEC_1';
     execute SP_TITULO_CAMPO 33, 'Fecha Adicional #2','SO_G_FEC_2';
     execute SP_TITULO_CAMPO 34, 'Fecha Adicional #3','SO_G_FEC_3';
     execute SP_TITULO_CAMPO 35, 'Fecha Adicional #4','SO_G_FEC_4';
     execute SP_TITULO_CAMPO 36, 'Fecha Adicional #5','SO_G_FEC_5';

     execute SP_TITULO_CAMPO 37, 'Tabla Adicional #1','SO_G_TAB_1';
     execute SP_TITULO_CAMPO 38, 'Tabla Adicional #2','SO_G_TAB_2';
     execute SP_TITULO_CAMPO 39, 'Tabla Adicional #3','SO_G_TAB_3';
     execute SP_TITULO_CAMPO 40, 'Tabla Adicional #4','SO_G_TAB_4';
     execute SP_TITULO_CAMPO 41, 'Tabla Adicional #5','SO_G_TAB_5';

     execute SP_TITULO_CAMPO 43, 'Requisito #1','SO_REQUI_1';
     execute SP_TITULO_CAMPO 44, 'Requisito #2','SO_REQUI_2';
     execute SP_TITULO_CAMPO 45, 'Requisito #3','SO_REQUI_3';
     execute SP_TITULO_CAMPO 46, 'Requisito #4','SO_REQUI_4';
     execute SP_TITULO_CAMPO 47, 'Requisito #5','SO_REQUI_5';
     execute SP_TITULO_CAMPO 48, 'Requisito #6','SO_REQUI_6';
     execute SP_TITULO_CAMPO 49, 'Requisito #7','SO_REQUI_7';
     execute SP_TITULO_CAMPO 50, 'Requisito #8','SO_REQUI_8';
     execute SP_TITULO_CAMPO 51, 'Requisito #9','SO_REQUI_9';
END
GO

CREATE PROCEDURE DBO.RECOMPILA_TODO
as
begin
     set NOCOUNT on
     declare @TABLA sysname
     declare TEMPORAL cursor for
     select TABLE_NAME from INFORMATION_SCHEMA.TABLES
     order by TABLE_NAME
     open TEMPORAL
     fetch next from TEMPORAL into @TABLA
     while ( @@FETCH_STATUS = 0 )
     begin
          exec SP_RECOMPILE @TABLA
          fetch next from TEMPORAL into @TABLA
     end
     close TEMPORAL
     deallocate TEMPORAL
end
GO

