/*****************************************/
/********** SELECCION *****************/
/*****************************************/

/* Tabla SOLICITA: N�mero exterior e Interior */
/* Patch 415 # Seq: X Agregado */
alter table SOLICITA add SO_NUM_EXT NombreCampo, SO_NUM_INT NombreCampo
GO

/* Tabla SOLICITA: Inicializar N�mero exterior */
/* Patch 415 # Seq: X Agregado */
update SOLICITA set SO_NUM_EXT = '' where ( SO_NUM_EXT is null )
GO

/* Tabla SOLICITA: Inicializar N�mero Interior */
/* Patch 415 # Seq: X Agregado */
update SOLICITA set SO_NUM_INT = '' where ( SO_NUM_INT is null )
GO

/* Tabla SOLICITA: Cambiar dominio SO_CALLE */
/* Patch 415 # Seq: X Agregado */
alter table SOLICITA alter column SO_CALLE Observaciones not null
GO

