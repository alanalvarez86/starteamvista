/****************************************/
/****************************************/
/****** Patch para SELECCION ************/
/****************************************/
/****************************************/

/* Tabla REQUIERE: 'Empleado reemplazado'*/
/* Patch 410 # Seq: 1 Agregado */
alter table REQUIERE add RQ_REEMPLA Titulo
GO

/* Tabla REQUIERE: Inicializar Empleado reemplazado */
/* Patch 410 # Seq: 2 Agregado */
update REQUIERE set RQ_REEMPLA = '' where (RQ_REEMPLA is NULL )
GO

/* Tabla CLIENTE: 'Nombre de autorizador'*/
/* Patch 410 # Seq: 3 Agregado */
alter table CLIENTE add CL_NOM_AU DescLarga
GO

/* Tabla CLIENTE: Inicializar Nombre de autorizador */
/* Patch 410 # Seq: 4 Agregado */
update CLIENTE set CL_NOM_AU = '' where ( CL_NOM_AU is NULL )
GO

/* Tabla CLIENTE: 'Puesto de autorizador'*/
/* Patch 410 # Seq: 5 Agregado */
alter table CLIENTE add CL_PUES_AU DescLarga
GO

/* Tabla CLIENTE: Inicializar Puesto de autorizador */
/* Patch 410 # Seq: 6 Agregado */
update CLIENTE set CL_PUES_AU = '' where ( CL_PUES_AU is NULL )
GO

/* Tabla REQUIERE: Puesto de autorizador'*/
/* Patch 410 # Seq: 7 Agregado */
alter table REQUIERE add RQ_PUES_AU DescLarga
GO

/* Tabla REQUIERE: Inicializar Nombre de autorizador */
/* Patch 410 # Seq: 8 Agregado */
update REQUIERE set RQ_PUES_AU = '' where (RQ_PUES_AU is NULL )
GO

/* Tabla REF_LAB: Campo 11 de Referencias laborales */
/* Patch 410 # Seq: 9 Agregado */
alter table REF_LAB add RL_CAMPO11 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 11 de Referencias Laborales */
/* Patch 410 # Seq: 10 Agregado */
update REF_LAB set RL_CAMPO11 = '' where ( RL_CAMPO11 is NULL )
GO

/* Tabla REF_LAB: Campo 12 de Referencias laborales */
/* Patch 410 # Seq: 11 Agregado */
alter table REF_LAB add RL_CAMPO12 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 12 de Referencias Laborales */
/* Patch 410 # Seq: 12 Agregado */
update REF_LAB set RL_CAMPO12 = '' where ( RL_CAMPO12 is NULL )
GO

/* Tabla REF_LAB: Campo 13 de Referencias laborales */
/* Patch 410 # Seq: 13 Agregado */
alter table REF_LAB add RL_CAMPO13 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 13 de Referencias Laborales */
/* Patch 410 # Seq: 14 Agregado */
update REF_LAB set RL_CAMPO13 = '' where ( RL_CAMPO13 is NULL )
GO

/* Tabla REF_LAB: Campo 14 de Referencias laborales */
/* Patch 410 # Seq: 15 Agregado */
alter table REF_LAB add RL_CAMPO14 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 14 de Referencias Laborales */
/* Patch 410 # Seq: 16 Agregado */
update REF_LAB set RL_CAMPO14 = '' where ( RL_CAMPO14 is NULL )
GO

/* Tabla REF_LAB: Campo 15 de Referencias laborales */
/* Patch 410 # Seq: 17 Agregado */
alter table REF_LAB add RL_CAMPO15 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 15 de Referencias Laborales */
/* Patch 410 # Seq: 18 Agregado */
update REF_LAB set RL_CAMPO15 = '' where ( RL_CAMPO15 is NULL )
GO

/* Tabla REF_LAB: Campo 16 de Referencias laborales */
/* Patch 410 # Seq: 19 Agregado */
alter table REF_LAB add RL_CAMPO16 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 16 de Referencias Laborales */
/* Patch 410 # Seq: 20 Agregado */
update REF_LAB set RL_CAMPO16 = '' where ( RL_CAMPO16 is NULL )
GO

/* Tabla REF_LAB: Campo 17 de Referencias laborales */
/* Patch 410 # Seq: 21 Agregado */
alter table REF_LAB add RL_CAMPO17 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 17 de Referencias Laborales */
/* Patch 410 # Seq: 22 Agregado */
update REF_LAB set RL_CAMPO17 = '' where ( RL_CAMPO17 is NULL )
GO

/* Tabla REF_LAB: Campo 18 de Referencias laborales */
/* Patch 410 # Seq: 23 Agregado */
alter table REF_LAB add RL_CAMPO18 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 18 de Referencias Laborales */
/* Patch 410 # Seq: 24 Agregado */
update REF_LAB set RL_CAMPO18 = '' where ( RL_CAMPO18 is NULL )
GO

/* Tabla REF_LAB: Campo 19 de Referencias laborales */
/* Patch 410 # Seq: 25 Agregado */
alter table REF_LAB add RL_CAMPO19 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 19 de Referencias Laborales */
/* Patch 410 # Seq: 26 Agregado */
update REF_LAB set RL_CAMPO19 = '' where ( RL_CAMPO19 is NULL )
GO

/* Tabla REF_LAB: Campo 20 de Referencias laborales */
/* Patch 410 # Seq: 27 Agregado */
alter table REF_LAB add RL_CAMPO20 Observaciones
GO

/* Tabla REF_LAB: Inicializar Campo 20 de Referencias Laborales */
/* Patch 410 # Seq: 28 Agregado */
update REF_LAB set RL_CAMPO20 = '' where ( RL_CAMPO20 is NULL )
GO

/* Tabla REF_LAB: Resultado 1 de Referencias laborales */
/* Patch 410 # Seq: 29 Agregado */
alter table REF_LAB add RL_EVAL1 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 1 de Referencias Laborales */
/* Patch 410 # Seq: 30 Agregado */
update REF_LAB set RL_EVAL1 = '' where ( RL_EVAL1 is NULL )
GO

/* Tabla REF_LAB: Resultado 2 de Referencias laborales */
/* Patch 410 # Seq: 31 Agregado */
alter table REF_LAB add RL_EVAL2 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 2 de Referencias Laborales */
/* Patch 410 # Seq: 32 Agregado */
update REF_LAB set RL_EVAL2 = '' where ( RL_EVAL2 is NULL )
GO

/* Tabla REF_LAB: Resultado 3 de Referencias laborales */
/* Patch 410 # Seq: 33 Agregado */
alter table REF_LAB add RL_EVAL3 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 3 de Referencias Laborales */
/* Patch 410 # Seq: 34 Agregado */
update REF_LAB set RL_EVAL3 = '' where ( RL_EVAL3 is NULL )
GO

/* Tabla REF_LAB: Resultado 4 de Referencias laborales */
/* Patch 410 # Seq: 35 Agregado */
alter table REF_LAB add RL_EVAL4 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 4 de Referencias Laborales */
/* Patch 410 # Seq: 36 Agregado */
update REF_LAB set RL_EVAL4 = '' where ( RL_EVAL4 is NULL )
GO

/* Tabla REF_LAB: Resultado 5 de Referencias laborales */
/* Patch 410 # Seq: 38 Agregado */
alter table REF_LAB add RL_EVAL5 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 5 de Referencias Laborales */
/* Patch 410 # Seq: 39 Agregado */
update REF_LAB set RL_EVAL5 = '' where ( RL_EVAL5 is NULL )
GO

/* Tabla REF_LAB: Resultado 6 de Referencias laborales */
/* Patch 410 # Seq: 40 Agregado */
alter table REF_LAB add RL_EVAL6 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 6 de Referencias Laborales */
/* Patch 410 # Seq: 41 Agregado */
update REF_LAB set RL_EVAL6 = '' where ( RL_EVAL6 is NULL )
GO

/* Tabla REF_LAB: Resultado 7 de Referencias laborales */
/* Patch 410 # Seq: 42 Agregado */
alter table REF_LAB add RL_EVAL7 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 7 de Referencias Laborales */
/* Patch 410 # Seq: 43 Agregado */
update REF_LAB set RL_EVAL7 = '' where ( RL_EVAL7 is NULL )
GO

/* Tabla REF_LAB: Resultado 8 de Referencias laborales */
/* Patch 410 # Seq: 44 Agregado */
alter table REF_LAB add RL_EVAL8 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 8 de Referencias Laborales */
/* Patch 410 # Seq: 45 Agregado */
update REF_LAB set RL_EVAL8 = '' where ( RL_EVAL8 is NULL )
GO

/* Tabla REF_LAB: Resultado 9 de Referencias laborales */
/* Patch 410 # Seq: 46 Agregado */
alter table REF_LAB add RL_EVAL9 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 9 de Referencias Laborales */
/* Patch 410 # Seq: 47 Agregado */
update REF_LAB set RL_EVAL9 = '' where ( RL_EVAL9 is NULL )
GO

/* Tabla REF_LAB: Resultado 10 de Referencias laborales */
/* Patch 410 # Seq: 48 Agregado */
alter table REF_LAB add RL_EVAL10 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 10 de Referencias Laborales */
/* Patch 410 # Seq: 49 Agregado */
update REF_LAB set RL_EVAL10 = '' where ( RL_EVAL10 is NULL )
GO

/* Tabla REF_LAB: Resultado 11 de Referencias laborales */
/* Patch 410 # Seq: 50 Agregado */
alter table REF_LAB add RL_EVAL11 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 11 de Referencias Laborales */
/* Patch 410 # Seq: 51 Agregado */
update REF_LAB set RL_EVAL11 = '' where ( RL_EVAL11 is NULL )
GO

/* Tabla REF_LAB: Resultado 12 de Referencias laborales */
/* Patch 410 # Seq: 53 Agregado */
alter table REF_LAB add RL_EVAL12 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 12 de Referencias Laborales */
/* Patch 410 # Seq: 54 Agregado */
update REF_LAB set RL_EVAL12 = '' where ( RL_EVAL2 is NULL )
GO

/* Tabla REF_LAB: Resultado 13 de Referencias laborales */
/* Patch 410 # Seq: 55 Agregado */
alter table REF_LAB add RL_EVAL13 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 13 de Referencias Laborales */
/* Patch 410 # Seq: 56 Agregado */
update REF_LAB set RL_EVAL13 = '' where ( RL_EVAL13 is NULL )
GO

/* Tabla REF_LAB: Resultado 14 de Referencias laborales */
/* Patch 410 # Seq: 57 Agregado */
alter table REF_LAB add RL_EVAL14 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 14 de Referencias Laborales */
/* Patch 410 # Seq: 58 Agregado */
update REF_LAB set RL_EVAL14 = '' where ( RL_EVAL14 is NULL )
GO

/* Tabla REF_LAB: Resultado 15 de Referencias laborales */
/* Patch 410 # Seq: 59 Agregado */
alter table REF_LAB add RL_EVAL15 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 15 de Referencias Laborales */
/* Patch 410 # Seq: 60 Agregado */
update REF_LAB set RL_EVAL15 = '' where ( RL_EVAL15 is NULL )
GO

/* Tabla REF_LAB: Resultado 16 de Referencias laborales */
/* Patch 410 # Seq: 61 Agregado */
alter table REF_LAB add RL_EVAL16 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 16 de Referencias Laborales */
/* Patch 410 # Seq: 62 Agregado */
update REF_LAB set RL_EVAL16 = '' where ( RL_EVAL16 is NULL )
GO

/* Tabla REF_LAB: Resultado 17 de Referencias laborales */
/* Patch 410 # Seq: 63 Agregado */
alter table REF_LAB add RL_EVAL17 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 17 de Referencias Laborales */
/* Patch 410 # Seq: 64 Agregado */
update REF_LAB set RL_EVAL17 = '' where ( RL_EVAL17 is NULL )
GO

/* Tabla REF_LAB: Resultado 18 de Referencias laborales */
/* Patch 410 # Seq: 65 Agregado */
alter table REF_LAB add RL_EVAL18 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 18 de Referencias Laborales */
/* Patch 410 # Seq: 66 Agregado */
update REF_LAB set RL_EVAL18 = '' where ( RL_EVAL18 is NULL )
GO

/* Tabla REF_LAB: Resultado 19 de Referencias laborales */
/* Patch 410 # Seq: 67 Agregado */
alter table REF_LAB add RL_EVAL19 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 19 de Referencias Laborales */
/* Patch 410 # Seq: 68 Agregado */
update REF_LAB set RL_EVAL19 = '' where ( RL_EVAL19 is NULL )
GO

/* Tabla REF_LAB: Resultado 20 de Referencias laborales */
/* Patch 410 # Seq: 69 Agregado */
alter table REF_LAB add RL_EVAL20 Observaciones
GO

/* Tabla REF_LAB: Inicializar Resultado 20 de Referencias Laborales */
/* Patch 410 # Seq: 70 Agregado */
update REF_LAB set RL_EVAL20 = '' where ( RL_EVAL20 is NULL )
GO

