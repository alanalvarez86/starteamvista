/****************************************/
/****************************************/
/****** Patch para SELECCION ************/
/****************************************/
/****************************************/

/* Tabla MISREPOR: Mis Reportes Favoritos */
/* Patch 385 # Seq: 1 Agregado */
CREATE TABLE MISREPOR (
       US_CODIGO            Usuario,      /* Usuario */
       RE_CODIGO            FolioChico ); /* # de Reporte */
GO

/* PK MISREPOR: Llave Primaria */
/* Patch 385 # Seq: 2 Agregado */
ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO);
GO

/* Agregar REQUIERE.RQ_TIPO_CO: Tipo de Contrato */
/* Patch 385 # Seq: 3 Agregado */
alter table REQUIERE add RQ_TIPO_CO Codigo2
GO

/* Inicializar REQUIERE.RQ_TIPO_CO */
/* Patch 385 # Seq: 4 Agregado */
update REQUIERE set RQ_TIPO_CO = '' where ( RQ_TIPO_CO is null )
GO

/* Agregar SOLICITA.SO_PAIS: Lugar Nacimiento */
/* Patch 385 # Seq: 5 Agregado */
alter table SOLICITA add SO_PAIS Descripcion
GO

/* Agregar SOLICITA.SO_CURP: CURP */
/* Patch 385 # Seq: 6 Agregado */
alter table SOLICITA add SO_CURP Descripcion
GO

/* Agregar SOLICITA.SO_RFC: R.F.C. */
/* Patch 385 # Seq: 7 Agregado */
alter table SOLICITA add SO_RFC Descripcion
GO

/* Agregar SOLICITA.SO_CALLE: Calle */
/* Patch 385 # Seq: 8 Agregado */
alter table SOLICITA add SO_CALLE Descripcion
GO

/* Agregar SOLICITA.SO_COLONIA: Colonia */
/* Patch 385 # Seq: 9 Agregado */
alter table SOLICITA add SO_COLONIA Descripcion
GO

/* Agregar SOLICITA.SO_CODPOST: C�digo Postal */
/* Patch 385 # Seq: 10 Agregado */
alter table SOLICITA add SO_CODPOST Referencia
GO

/* Agregar SOLICITA.SO_CIUDAD: Ciudad */
/* Patch 385 # Seq: 11 Agregado */
alter table SOLICITA add SO_CIUDAD Descripcion
GO

/* Agregar SOLICITA.SO_ESTADO: Estado */
/* Patch 385 # Seq: 12 Agregado */
alter table SOLICITA add SO_ESTADO Codigo2
GO

/* Agregar SOLICITA.SO_NACION: Pais */
/* Patch 385 # Seq: 13 Agregado */
alter table SOLICITA add SO_NACION Descripcion
GO

/* Agregar SOLICITA.SO_FEC_RES: Fecha de Residencia */
/* Patch 385 # Seq: 14 Agregado */
alter table SOLICITA add SO_FEC_RES Fecha
GO

/* Agregar SOLICITA.SO_ESCUELA: Nombre Completo de la Escuela */
/* Patch 385 # Seq: 15 Agregado */
alter table SOLICITA add SO_ESCUELA DescLarga
GO

/* Agregar SOLICITA.SO_G_NUM_6: Num�rico Adicional 6 */
/* Patch 385 # Seq: 16 Agregado */
alter table SOLICITA add SO_G_NUM_6 Numerico
GO

/* Agregar SOLICITA.SO_G_NUM_7: Num�rico Adicional 7 */
/* Patch 385 # Seq: 17 Agregado */
alter table SOLICITA add SO_G_NUM_7 Numerico
GO

/* Agregar SOLICITA.SO_G_NUM_8: Num�rico Adicional 8 */
/* Patch 385 # Seq: 18 Agregado */
alter table SOLICITA add SO_G_NUM_8 Numerico
GO

/* Agregar SOLICITA.SO_G_NUM_9: Num�rico Adicional 9 */
/* Patch 385 # Seq: 19 Agregado */
alter table SOLICITA add SO_G_NUM_9 Pesos
GO

/* Agregar SOLICITA.SO_G_NUM10: Num�rico Adicional 10 */
/* Patch 385 # Seq: 20 Agregado */
alter table SOLICITA add SO_G_NUM10 Pesos
GO

/* Agregar SOLICITA.SO_G_LOG_6: Booleano Adicional 6 */
/* Patch 385 # Seq: 21 Agregado */
alter table SOLICITA add SO_G_LOG_6 Booleano
GO

/* Agregar SOLICITA.SO_G_LOG_7: Booleano Adicional 7 */
/* Patch 385 # Seq: 22 Agregado */
alter table SOLICITA add SO_G_LOG_7 Booleano
GO

/* Agregar SOLICITA.SO_G_LOG_8: Booleano Adicional 8 */
/* Patch 385 # Seq: 23 Agregado */
alter table SOLICITA add SO_G_LOG_8 Booleano
GO

/* Agregar SOLICITA.SO_G_LOG_9: Booleano Adicional 9 */
/* Patch 385 # Seq: 24 Agregado */
alter table SOLICITA add SO_G_LOG_9 Booleano
GO

/* Agregar SOLICITA.SO_G_LOG10: Booleano Adicional 10 */
/* Patch 385 # Seq: 25 Agregado */
alter table SOLICITA add SO_G_LOG10 Booleano
GO

/* Agregar SOLICITA.SO_G_TEX_6: Texto Adicional 6 */
/* Patch 385 # Seq: 26 Agregado */
alter table SOLICITA add SO_G_TEX_6 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX_7: Texto Adicional 7 */
/* Patch 385 # Seq: 27 Agregado */
alter table SOLICITA add SO_G_TEX_7 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX_8: Texto Adicional 8 */
/* Patch 385 # Seq: 28 Agregado */
alter table SOLICITA add SO_G_TEX_8 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX_9: Texto Adicional 9 */
/* Patch 385 # Seq: 29 Agregado */
alter table SOLICITA add SO_G_TEX_9 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX10: Texto Adicional 10 */
/* Patch 385 # Seq: 30 Agregado */
alter table SOLICITA add SO_G_TEX10 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX11: Texto Adicional 11 */
/* Patch 385 # Seq: 31 Agregado */
alter table SOLICITA add SO_G_TEX11 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX12: Texto Adicional 12 */
/* Patch 385 # Seq: 32 Agregado */
alter table SOLICITA add SO_G_TEX12 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX13: Texto Adicional 13 */
/* Patch 385 # Seq: 33 Agregado */
alter table SOLICITA add SO_G_TEX13 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX14: Texto Adicional 14 */
/* Patch 385 # Seq: 34 Agregado */
alter table SOLICITA add SO_G_TEX14 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX15: Texto Adicional 15 */
/* Patch 385 # Seq: 35 Agregado */
alter table SOLICITA add SO_G_TEX15 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX16: Texto Adicional 16 */
/* Patch 385 # Seq: 36 Agregado */
alter table SOLICITA add SO_G_TEX16 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX17: Texto Adicional 17 */
/* Patch 385 # Seq: 37 Agregado */
alter table SOLICITA add SO_G_TEX17 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX18: Texto Adicional 18 */
/* Patch 385 # Seq: 38 Agregado */
alter table SOLICITA add SO_G_TEX18 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX19: Texto Adicional 19 */
/* Patch 385 # Seq: 39 Agregado */
alter table SOLICITA add SO_G_TEX19 Descripcion
GO

/* Agregar SOLICITA.SO_G_TEX20: Texto Adicional 20 */
/* Patch 385 # Seq: 40 Agregado */
alter table SOLICITA add SO_G_TEX20 Descripcion
GO

/* Agregar SOLICITA.SO_MED_TRA: Medio De Transporte */
/* Patch 385 # Seq: 78 Agregado */
alter table SOLICITA add SO_MED_TRA Codigo1
GO

/* Inicializar SOLICITA.SO_PAIS */
/* Patch 385 # Seq: 41 Agregado */
update SOLICITA set SO_PAIS = '' where ( SOLICITA.SO_PAIS is NULL)
GO

/* Inicializar SOLICITA.SO_CURP */
/* Patch 385 # Seq: 42 Agregado */
update SOLICITA set SO_CURP = '' where ( SOLICITA.SO_CURP is NULL)
GO

/* Inicializar SOLICITA.SO_RFC */
/* Patch 385 # Seq: 43 Agregado */
update SOLICITA set SO_RFC = '' where ( SOLICITA.SO_RFC is NULL)
GO

/* Inicializar SOLICITA.SO_CALLE */
/* Patch 385 # Seq: 44 Agregado */
update SOLICITA set SO_CALLE = '' where ( SOLICITA.SO_CALLE is NULL)
GO

/* Inicializar SOLICITA.SO_COLONIA */
/* Patch 385 # Seq: 45 Agregado */
update SOLICITA set SO_COLONIA = '' where ( SOLICITA.SO_COLONIA is NULL)
GO

/* Inicializar SOLICITA.SO_CODPOST */
/* Patch 385 # Seq: 46 Agregado */
update SOLICITA set SO_CODPOST = '' where ( SOLICITA.SO_CODPOST is NULL)
GO

/* Inicializar SOLICITA.SO_CIUDAD */
/* Patch 385 # Seq: 47 Agregado */
update SOLICITA set SO_CIUDAD = '' where ( SOLICITA.SO_CIUDAD is NULL)
GO

/* Inicializar SOLICITA.SO_ESTADO */
/* Patch 385 # Seq: 48 Agregado */
update SOLICITA set SO_ESTADO = '' where ( SOLICITA.SO_ESTADO is NULL)
GO

/* Inicializar SOLICITA.SO_NACION */
/* Patch 385 # Seq: 49 Agregado */
update SOLICITA set SO_NACION = '' where ( SOLICITA.SO_NACION is NULL)
GO

/* Inicializar SOLICITA.SO_ESCUELA */
/* Patch 385 # Seq: 51 Agregado */
update SOLICITA set SO_ESCUELA = '' where ( SOLICITA.SO_ESCUELA is NULL)
GO

/* Inicializar SOLICITA.SO_G_NUM_6 */
/* Patch 385 # Seq: 52 Agregado */
update SOLICITA set SO_G_NUM_6 = 0 where ( SOLICITA.SO_G_NUM_6 is NULL)
GO

/* Inicializar SOLICITA.SO_G_NUM_7 */
/* Patch 385 # Seq: 53 Agregado */
update SOLICITA set SO_G_NUM_7 = 0 where ( SOLICITA.SO_G_NUM_7 is NULL)
GO

/* Inicializar SOLICITA.SO_G_NUM_8 */
/* Patch 385 # Seq: 54 Agregado */
update SOLICITA set SO_G_NUM_8 = 0 where ( SOLICITA.SO_G_NUM_8 is NULL)
GO

/* Inicializar SOLICITA.SO_G_NUM_9 */
/* Patch 385 # Seq: 55 Agregado */
update SOLICITA set SO_G_NUM_9 = 0 where ( SOLICITA.SO_G_NUM_9 is NULL)
GO

/* Inicializar SOLICITA.SO_G_NUM10 */
/* Patch 385 # Seq: 56 Agregado */
update SOLICITA set SO_G_NUM10 = 0 where ( SOLICITA.SO_G_NUM10 is NULL)
GO

/* Inicializar SOLICITA.SO_G_LOG_6 */
/* Patch 385 # Seq: 57 Agregado */
update SOLICITA set SO_G_LOG_6 = 'N' where ( SOLICITA.SO_G_LOG_6 is NULL)
GO

/* Inicializar SOLICITA.SO_G_LOG_7 */
/* Patch 385 # Seq: 58 Agregado */
update SOLICITA set SO_G_LOG_7 = 'N' where ( SOLICITA.SO_G_LOG_7 is NULL)
GO

/* Inicializar SOLICITA.SO_G_LOG_8 */
/* Patch 385 # Seq: 59 Agregado */
update SOLICITA set SO_G_LOG_8 = 'N' where ( SOLICITA.SO_G_LOG_8 is NULL)
GO

/* Inicializar SOLICITA.SO_G_LOG_9 */
/* Patch 385 # Seq: 60 Agregado */
update SOLICITA set SO_G_LOG_9 = 'N' where ( SOLICITA.SO_G_LOG_9 is NULL)
GO

/* Inicializar SOLICITA.SO_G_LOG10 */
/* Patch 385 # Seq: 61 Agregado */
update SOLICITA set SO_G_LOG10 = 'N' where ( SOLICITA.SO_G_LOG10 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX_6 */
/* Patch 385 # Seq: 62 Agregado */
update SOLICITA set SO_G_TEX_6 = '' where ( SOLICITA.SO_G_TEX_6 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX_7 */
/* Patch 385 # Seq: 63 Agregado */
update SOLICITA set SO_G_TEX_7 = '' where ( SOLICITA.SO_G_TEX_7 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX_8 */
/* Patch 385 # Seq: 64 Agregado */
update SOLICITA set SO_G_TEX_8 = '' where ( SOLICITA.SO_G_TEX_8 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX_9 */
/* Patch 385 # Seq: 65 Agregado */
update SOLICITA set SO_G_TEX_9 = '' where ( SOLICITA.SO_G_TEX_9 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX10 */
/* Patch 385 # Seq: 66 Agregado */
update SOLICITA set SO_G_TEX10 = '' where ( SOLICITA.SO_G_TEX10 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX11 */
/* Patch 385 # Seq: 67 Agregado */
update SOLICITA set SO_G_TEX11 = '' where ( SOLICITA.SO_G_TEX11 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX12 */
/* Patch 385 # Seq: 68 Agregado */
update SOLICITA set SO_G_TEX12 = '' where ( SOLICITA.SO_G_TEX12 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX13 */
/* Patch 385 # Seq: 69 Agregado */
update SOLICITA set SO_G_TEX13 = '' where ( SOLICITA.SO_G_TEX13 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX14 */
/* Patch 385 # Seq: 70 Agregado */
update SOLICITA set SO_G_TEX14 = '' where ( SOLICITA.SO_G_TEX14 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX15 */
/* Patch 385 # Seq: 71 Agregado */
update SOLICITA set SO_G_TEX15 = '' where ( SOLICITA.SO_G_TEX15 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX16 */
/* Patch 385 # Seq: 72 Agregado */
update SOLICITA set SO_G_TEX16 = '' where ( SOLICITA.SO_G_TEX16 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX17 */
/* Patch 385 # Seq: 73 Agregado */
update SOLICITA set SO_G_TEX17 = '' where ( SOLICITA.SO_G_TEX17 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX18 */
/* Patch 385 # Seq: 74 Agregado */
update SOLICITA set SO_G_TEX18 = '' where ( SOLICITA.SO_G_TEX18 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX19 */
/* Patch 385 # Seq: 75 Agregado */
update SOLICITA set SO_G_TEX19 = '' where ( SOLICITA.SO_G_TEX19 is NULL)
GO

/* Inicializar SOLICITA.SO_G_TEX20 */
/* Patch 385 # Seq: 76 Agregado */
update SOLICITA set SO_G_TEX20 = '' where ( SOLICITA.SO_G_TEX20 is NULL)
GO

/* Inicializar SOLICITA.SO_MED_TRA */
/* Patch 385 # Seq: 79 Agregado */
update SOLICITA set SO_MED_TRA = '' where ( SOLICITA.SO_MED_TRA is NULL)
GO

/* FK MISREPOR_REPORTE: Llave For�nea */
/* Patch 385 # Seq: 3 Agregado */
ALTER TABLE MISREPOR
       ADD CONSTRAINT FK_MISREPOR_REPORTE
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

