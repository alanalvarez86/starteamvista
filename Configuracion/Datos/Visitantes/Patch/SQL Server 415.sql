/*****************************************/
/********** VISITANTES *****************/
/*****************************************/

/* Tabla: LIBRO: �Devolvi� Gafete? (PRO: Control de Gafetes de Visitantes) */
/* Patch 415 # Seq: X Agregado */
alter table LIBRO add LI_DEVGAF booleano
GO

/* Tabla: LIBRO: Inicializar Devolvi� Gafete (PRO: Control de Gafetes de Visitantes) */
/* Patch 415 # Seq: X Agregado */
update LIBRO set LI_DEVGAF = 'N' where ( LI_DEVGAF is NULL )
GO

