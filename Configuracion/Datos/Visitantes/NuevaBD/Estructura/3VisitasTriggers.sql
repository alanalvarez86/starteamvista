CREATE TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	declare @NewCodigo Condicion, @OldCodigo Condicion;

	select @NewCodigo = QU_CODIGO from Inserted;
        select @OldCodigo = QU_CODIGO from Deleted;

	IF ( @OldCodigo <> '' ) AND ( @NewCodigo <> '' )
	BEGIN
	     update REPORTE set QU_CODIGO = @NewCodigo
             where ( QU_CODIGO = @OldCodigo );
	END
  END
END
GO

CREATE TRIGGER TD_REPORTE ON REPORTE AFTER DELETE
AS
BEGIN
     SET NOCOUNT ON;
     update CASETA set CASETA.RE_CODIGO = 0
     from CASETA, Deleted
     where ( CASETA.RE_CODIGO = Deleted.RE_CODIGO );
END
GO

CREATE TRIGGER TU_REPORTE ON REPORTE AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( RE_CODIGO )
  BEGIN
       declare @NewCodigo FolioChico, @OldCodigo FolioChico;

       select @NewCodigo = RE_CODIGO from Inserted;
       select @OldCodigo = RE_CODIGO from Deleted;

       update CASETA set CASETA.RE_CODIGO = @NewCodigo
       where ( CASETA.RE_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_CASETA ON CASETA AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CA_CODIGO )
  BEGIN
       declare @NewCodigo Codigo, @OldCodigo Codigo;

       select @NewCodigo = CA_CODIGO from Inserted;
       select @OldCodigo = CA_CODIGO from Deleted;

       update LIBRO set LIBRO.LI_SAL_CAS = @NewCodigo
       where ( LIBRO.LI_SAL_CAS = @OldCodigo );
       update LIBRO set LIBRO.LI_ENT_CAS = @NewCodigo
       where ( LIBRO.LI_ENT_CAS = @OldCodigo );
       update CORTE set CORTE.CA_CODIGO = @NewCodigo
       where ( CORTE.CA_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_DEPTO ON DEPTO AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
       declare @NewCodigo Codigo, @OldCodigo Codigo;

       select @NewCodigo = TB_CODIGO from Inserted;
       select @OldCodigo = TB_CODIGO from Deleted;

       update LIBRO set LIBRO.LI_CDEPTO = @NewCodigo
       where ( LIBRO.LI_CDEPTO = @OldCodigo );
       update ANFITRIO set ANFITRIO.AN_DEPTO = @NewCodigo
       where ( ANFITRIO.AN_DEPTO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TIPO_ID ON TIPO_ID AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
       declare @NewCodigo Codigo, @OldCodigo Codigo;

       select @NewCodigo = TB_CODIGO from Inserted;
       select @OldCodigo = TB_CODIGO from Deleted;

       update LIBRO set LIBRO.LI_TIPO_ID = @NewCodigo
       where ( LIBRO.LI_TIPO_ID = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TASUNTO ON TASUNTO AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
       declare @NewCodigo Codigo, @OldCodigo Codigo;

       select @NewCodigo = TB_CODIGO from Inserted;
       select @OldCodigo = TB_CODIGO from Deleted;

       update CITA set CITA.CI_ASUNTO = @NewCodigo
       where ( CITA.CI_ASUNTO = @OldCodigo );
       update LIBRO set LIBRO.LI_ASUNTO = @NewCodigo
       where ( LIBRO.LI_ASUNTO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TCARRO ON TCARRO AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
       declare @NewCodigo Codigo, @OldCodigo Codigo;

       select @NewCodigo = TB_CODIGO from Inserted;
       select @OldCodigo = TB_CODIGO from Deleted;

       update LIBRO set LIBRO.LI_CAR_TIP = @NewCodigo
       where ( LIBRO.LI_CAR_TIP = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TVISITA ON TVISITA AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
       declare @NewCodigo Codigo, @OldCodigo Codigo;

       select @NewCodigo = TB_CODIGO from Inserted;
       select @OldCodigo = TB_CODIGO from Deleted;

       update VISITA set VISITA.VI_TIPO = @NewCodigo
       where ( VISITA.VI_TIPO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EMP_VISI ON EMP_VISI AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( EV_NUMERO )
  BEGIN
       declare @NewCodigo FolioGrande, @OldCodigo FolioGrande;

       select @NewCodigo = EV_NUMERO from Inserted;
       select @OldCodigo = EV_NUMERO from Deleted;

       update LIBRO set LIBRO.EV_NUMERO = @NewCodigo
       where ( LIBRO.EV_NUMERO = @OldCodigo );
       update VISITA set VISITA.EV_NUMERO = @NewCodigo
       where ( VISITA.EV_NUMERO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_VISITA ON VISITA AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( VI_NUMERO )
  BEGIN
       declare @NewCodigo Visitante, @OldCodigo Visitante;

       select @NewCodigo = VI_NUMERO from Inserted;
       select @OldCodigo = VI_NUMERO from Deleted;

       update CITA set CITA.VI_NUMERO = @NewCodigo
       where ( CITA.VI_NUMERO = @OldCodigo );
       update LIBRO set LIBRO.VI_NUMERO = @NewCodigo
       where ( LIBRO.VI_NUMERO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_ANFITRIO ON ANFITRIO AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AN_NUMERO )
  BEGIN

       declare @NewCodigo NumeroEmpleado, @OldCodigo NumeroEmpleado;

       select @NewCodigo = AN_NUMERO from Inserted;
       select @OldCodigo = AN_NUMERO from Deleted;

       update CITA set CITA.AN_NUMERO = @NewCodigo
       where ( CITA.AN_NUMERO = @OldCodigo );
       update LIBRO set LIBRO.AN_NUMERO = @NewCodigo
       where ( LIBRO.AN_NUMERO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_CITA ON CITA AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CI_FOLIO )
  BEGIN
       declare @NewCodigo FolioGrande, @OldCodigo FolioGrande;

       select @NewCodigo = CI_FOLIO from Inserted;
       select @OldCodigo = CI_FOLIO from Deleted;

       update LIBRO set LIBRO.CI_FOLIO = @NewCodigo
       where ( LIBRO.CI_FOLIO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_CORTE ON CORTE AFTER UPDATE
AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CO_FOLIO )
  BEGIN
       declare @NewCodigo FolioGrande, @OldCodigo FolioGrande;

       select @NewCodigo = CO_FOLIO from Inserted;
       select @OldCodigo = CO_FOLIO from Deleted;

       update LIBRO set LIBRO.LI_SAL_COR = @NewCodigo
       where ( LIBRO.LI_SAL_COR = @OldCodigo );
       update LIBRO set LIBRO.LI_ENT_COR = @NewCodigo
       where ( LIBRO.LI_ENT_COR = @OldCodigo );
  END
END
GO


