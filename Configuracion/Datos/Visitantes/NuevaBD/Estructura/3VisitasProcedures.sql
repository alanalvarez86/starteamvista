CREATE PROCEDURE DBO.INIT_PROCESS_LOG(
  @Proceso Integer,
  @Usuario Smallint,
  @Fecha   DateTime,
  @Hora    Varchar(8),
  @Maximo  Integer,
  @Params   varchar(255),
  @Folio   Integer OUTPUT )
AS
  SET NOCOUNT ON
  select @Folio = MAX( PC_NUMERO ) from PROCESO

  if ( @Folio is NULL )
    select @Folio = 1
  else
    select @Folio = @Folio + 1

  insert into PROCESO ( PC_PROC_ID,
                        PC_NUMERO,
                        US_CODIGO,
                        PC_FEC_INI,
                        PC_HOR_INI,
                        PC_MAXIMO,
                        PC_ERROR,
			PC_PARAMS ) values
                         ( @Proceso,
                           @Folio,
                           @Usuario,
                           @Fecha,
                           @Hora,
                           @Maximo,
                           'A',
			   @Params )
GO

CREATE PROCEDURE UPDATE_PROCESS_LOG( 
  @Proceso  Integer, 
  @Empleado Integer, 
  @Paso     Integer, 
  @Fecha    DateTime, 
  @Hora     VARCHAR( 8 ),
  @Status   SmallInt OUTPUT ) 
AS
  SET NOCOUNT ON
  declare @Bandera Char( 1 )

  update PROCESO set
         CB_CODIGO = @Empleado,
         PC_PASO = @Paso,
         PC_FEC_FIN = @Fecha,
         PC_HOR_FIN = @Hora
  where ( PC_NUMERO = @Proceso )

  select @Bandera = PC_ERROR
  from   PROCESO
  where ( PC_NUMERO = @Proceso )

  if ( @Bandera = 'A' )
    select @Status = 0
  else
    select @Status = 1

GO

CREATE PROCEDURE DBO.SP_REFRESH_DICCION AS
GO

CREATE PROCEDURE DBO.RECOMPILA_TODO
as
begin
     set NOCOUNT on
     declare @TABLA sysname
     declare TEMPORAL cursor for
     select TABLE_NAME from INFORMATION_SCHEMA.TABLES
     order by TABLE_NAME
     open TEMPORAL
     fetch next from TEMPORAL into @TABLA
     while ( @@FETCH_STATUS = 0 )
     begin
          exec SP_RECOMPILE @TABLA
          fetch next from TEMPORAL into @TABLA
     end
     close TEMPORAL
     deallocate TEMPORAL
end
GO

