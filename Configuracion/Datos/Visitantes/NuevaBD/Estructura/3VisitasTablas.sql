CREATE TABLE DICCION (
       DI_CLASIFI           Status,
       DI_NOMBRE            NombreCampo,
       DI_TITULO            TituloCampo,
       DI_CALC              Status,
       DI_ANCHO             Status,
       DI_MASCARA           Descripcion,
       DI_TFIELD            Status,
       DI_ORDEN             Booleano,
       DI_REQUIER           CamposRequeridos,
       DI_TRANGO            Status,
       DI_NUMERO            Status,
       DI_VALORAC           Descripcion,
       DI_RANGOAC           Status,
       DI_CLAVES            TituloCampo,
       DI_TCORTO            TituloCampo,
       DI_CONFI             Booleano
)
GO

CREATE INDEX XIE1DICCION ON DICCION
(
       DI_CLASIFI,
       DI_CALC
)
GO

ALTER TABLE DICCION
       ADD CONSTRAINT PK_DICCION PRIMARY KEY (DI_CLASIFI, DI_NOMBRE)
GO

CREATE TABLE BITACORA (
       US_CODIGO            Usuario,
       BI_FECHA             Fecha,
       BI_HORA              HoraMinutosSegundos,
       BI_PROC_ID           Status,
       BI_TIPO              Status,
       BI_NUMERO            FolioGrande,
       BI_TEXTO             Observaciones,
       CB_CODIGO            NumeroEmpleado,
       BI_DATA              Memo,
       BI_CLASE             Status,
       BI_FEC_MOV           Fecha
)
GO

CREATE INDEX XIE1BITACORA ON BITACORA
(
       BI_NUMERO
)
GO

CREATE INDEX XIE2BITACORA ON BITACORA
(
       BI_FECHA,
       BI_HORA
)
GO

CREATE TABLE REPORTE (
       RE_CODIGO            FolioChico,
       RE_NOMBRE            Descripcion,
       RE_TIPO              Status,
       RE_TITULO            Titulo,
       RE_ENTIDAD           Status,
       RE_SOLOT             Booleano,
       RE_FECHA             Fecha,
       RE_GENERAL           Descripcion,
       RE_REPORTE           PathArchivo,
       RE_CFECHA            NombreCampo,
       RE_IFECHA            Status,
       RE_HOJA              Status,
       RE_ALTO              Tamanio,
       RE_ANCHO             Tamanio,
       RE_PRINTER           Formula,
       RE_COPIAS            Status,
       RE_PFILE             Status,
       RE_ARCHIVO           PathArchivo,
       RE_VERTICA           Booleano,
       US_CODIGO            Usuario,
       RE_FILTRO            Formula,
       RE_COLNUM            Status,
       RE_COLESPA           Tamanio,
       RE_RENESPA           Tamanio,
       RE_MAR_SUP           Tamanio,
       RE_MAR_IZQ           Tamanio,
       RE_MAR_DER           Tamanio,
       RE_MAR_INF           Tamanio,
       RE_NIVEL             Status,
       RE_FONTNAM           WindowsName,
       RE_FONTSIZ           Status,
       QU_CODIGO            Condicion,
       RE_CLASIFI           Status,
       RE_CANDADO           Status
)
GO

CREATE UNIQUE INDEX XAK1REPORTE ON REPORTE
(
       RE_NOMBRE
)
GO

CREATE INDEX XIE1REPORTE ON REPORTE
(
       RE_CLASIFI,
       RE_NOMBRE
)
GO


ALTER TABLE REPORTE
       ADD CONSTRAINT PK_REPORTE PRIMARY KEY (RE_CODIGO)
GO

CREATE TABLE CAMPOREP (
       RE_CODIGO            FolioChico,
       CR_TIPO              Status,
       CR_POSICIO           Status,
       CR_CLASIFI           Status,
       CR_SUBPOS            Status,
       CR_TABLA             Status,
       CR_TITULO            TituloCampo,
       CR_REQUIER           Formula,
       CR_CALC              Status,
       CR_MASCARA           Mascara,
       CR_ANCHO             Status,
       CR_OPER              Status,
       CR_TFIELD            Status,
       CR_SHOW              Status,
       CR_DESCRIP           NombreCampo,
       CR_COLOR             Status,
       CR_FORMULA           Formula,
       CR_BOLD              Booleano,
       CR_ITALIC            Booleano,
       CR_SUBRAYA           Booleano,
       CR_STRIKE            Booleano,
       CR_ALINEA            Status
)
GO

CREATE TABLE MISREPOR (
       US_CODIGO            Usuario,
       RE_CODIGO            FolioChico )
GO

ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

ALTER TABLE CAMPOREP
       ADD CONSTRAINT PK_CAMPOREP PRIMARY KEY (RE_CODIGO, CR_TIPO, CR_POSICIO, 
              CR_SUBPOS)
GO

CREATE TABLE GLOBAL (
       GL_CODIGO            FolioChico,
       GL_DESCRIP           Descripcion,
       GL_FORMULA           Formula,
       GL_TIPO              Status,
       US_CODIGO            Usuario,
       GL_CAPTURA           Fecha,
       GL_NIVEL             Status
)
GO


ALTER TABLE GLOBAL
       ADD CONSTRAINT PK_GLOBAL PRIMARY KEY (GL_CODIGO)
GO

CREATE TABLE PROCESO (
       PC_PROC_ID           Status,
       PC_NUMERO            FolioGrande,
       US_CODIGO            Usuario,
       PC_FEC_INI           Fecha,
       PC_HOR_INI           HoraMinutosSegundos,
       PC_FEC_FIN           Fecha,
       PC_HOR_FIN           HoraMinutosSegundos,
       PC_ERROR             Booleano,
       CB_CODIGO            NumeroEmpleado,
       PC_MAXIMO            FolioGrande,
       PC_PASO              FolioGrande,
       US_CANCELA           Usuario,
       PC_PARAMS            Formula
)
GO

CREATE INDEX XIE1PROCESO ON PROCESO
(
       PC_FEC_INI,
       PC_ERROR
)
GO


ALTER TABLE PROCESO
       ADD CONSTRAINT PK_PROCESO PRIMARY KEY (PC_NUMERO)
GO

CREATE TABLE QUERYS (
       QU_CODIGO            Condicion,
       QU_DESCRIP           Formula,
       QU_FILTRO            Formula,
       QU_NIVEL             Status,
       US_CODIGO            Usuario
)
GO


ALTER TABLE QUERYS
       ADD CONSTRAINT PK_QUERYS PRIMARY KEY (QU_CODIGO)
GO

CREATE TABLE SUSCRIP (
       RE_CODIGO            FolioChico,
       US_CODIGO            Usuario,
       SU_FRECUEN           Status,
       SU_ENVIAR            Status,
       SU_VACIO             Status
)
GO

ALTER TABLE SUSCRIP
       ADD CONSTRAINT PK_SUSCRIP PRIMARY KEY (RE_CODIGO, US_CODIGO)
GO

CREATE TABLE TASUNTO (
        TB_CODIGO           Codigo,
        TB_ELEMENT          Descripcion,
        TB_INGLES           Descripcion,
        TB_NUMERO           Pesos,
        TB_TEXTO            Descripcion
)
GO

ALTER TABLE TASUNTO
      ADD CONSTRAINT PK_TASUNTO PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE TCARRO (
        TB_CODIGO           Codigo,
        TB_ELEMENT          Descripcion,
        TB_INGLES           Descripcion,
        TB_NUMERO           Pesos,
        TB_TEXTO            Descripcion
)
GO

ALTER TABLE TCARRO
      ADD CONSTRAINT PK_TCARRO PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE TIPO_ID (
        TB_CODIGO           Codigo,
        TB_ELEMENT          Descripcion,
        TB_INGLES           Descripcion,
        TB_NUMERO           Pesos,
        TB_TEXTO            Descripcion
)
GO

ALTER TABLE TIPO_ID
      ADD CONSTRAINT PK_TIPO_ID PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE TVISITA (
        TB_CODIGO           Codigo,
        TB_ELEMENT          Descripcion,
        TB_INGLES           Descripcion,
        TB_NUMERO           Pesos,
        TB_TEXTO            Descripcion
)
GO

ALTER TABLE TVISITA
      ADD CONSTRAINT PK_TVISITA PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE CASETA (
        CA_CODIGO           Codigo,
        CA_NOMBRE           Descripcion,
        CA_UBICA            Formula,
        CA_STATUS           Status,
        CA_CORTE            FolioGrande,
        CA_ABIERTA          Booleano,
        RE_CODIGO           FolioChico
)
GO

ALTER TABLE CASETA
      ADD CONSTRAINT PK_CASETA PRIMARY KEY(CA_CODIGO)
GO

CREATE TABLE DEPTO (
        TB_CODIGO           Codigo,
        TB_ELEMENT          Descripcion,
        TB_INGLES           Descripcion,
        TB_NUMERO           Pesos,
        TB_TEXTO            Descripcion
)
GO

ALTER TABLE DEPTO
      ADD CONSTRAINT PK_DEPTO PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE ANFITRIO (
        AN_NUMERO           NumeroEmpleado,
        AN_DEPTO            Codigo,
        AN_APE_PAT          Descripcion,
        AN_APE_MAT          Descripcion,
        AN_NOMBRES          Descripcion,
        AN_TEL              Descripcion,
        AN_STATUS           Status
)
GO

ALTER TABLE ANFITRIO
       ADD CONSTRAINT PK_ANFITRIO PRIMARY KEY (AN_NUMERO)
GO

CREATE TABLE CITA (
        CI_FOLIO            FolioGrande,
        CI_ASUNTO           Codigo,
        AN_NUMERO           NumeroEmpleado,
        VI_NUMERO           Visitante,
        CI_FECHA            Fecha,
        CI_HORA             Hora,
        US_CODIGO           Usuario,
        CI_FEC_MOD          Fecha,
        CI_HOR_MOD          Hora,
        CI_STATUS           Status,
        CI_OBSERVA          Memo
)
GO

ALTER TABLE CITA
      ADD CONSTRAINT PK_CITA PRIMARY KEY(CI_FOLIO)
GO

CREATE TABLE EMP_VISI (
        EV_NUMERO           FolioGrande,
        EV_NOMBRE           DescLarga,
        EV_RAZ_SOC          Formula,
        EV_TEXTO1           Descripcion,
        EX_TEXTO2           Descripcion,
        EV_TEXTO3           Descripcion,
        EV_TEXTO4           Descripcion,
        EV_TEXTO5           Descripcion,
        EV_STATUS           Status
)
GO

ALTER TABLE EMP_VISI
      ADD CONSTRAINT PK_EMP_VISI PRIMARY KEY(EV_NUMERO)
GO

CREATE TABLE CORTE (
        CO_FOLIO            FolioGrande,
        CA_CODIGO           Codigo,
        CO_INI_VIG          Usuario,
        CO_INI_FEC          Fecha,
        CO_INI_HOR          Hora,
        CO_FIN_VIG          Usuario,
        CO_FIN_FEC          Fecha,
        CO_FIN_HOR          Hora,
        CO_OBSERVA          Memo
)
GO

ALTER TABLE CORTE
      ADD CONSTRAINT PK_CORTE PRIMARY KEY(CO_FOLIO)
GO

CREATE TABLE VISITA (
        VI_NUMERO           Visitante,
        VI_TIPO             Codigo,
        EV_NUMERO           FolioGrande,
        AN_NUMERO           NumeroEmpleado,
        VI_ASUNTO           Codigo,
        VI_APE_PAT          Descripcion,
        VI_APE_MAT          Descripcion,
        VI_NOMBRES          Descripcion,
        VI_NACION           Descripcion,
        VI_STATUS           Status,
        VI_SEXO             Codigo1,
        VI_FEC_NAC          Fecha,
        VI_MIGRA            DescLarga,
        VI_FOTO             Imagen
)
GO

ALTER TABLE VISITA
      ADD CONSTRAINT PK_VISITA PRIMARY KEY(VI_NUMERO)
GO

CREATE TABLE LIBRO (
        LI_FOLIO            FolioGrande,
        LI_TIPO_ID          Codigo,
        LI_CAR_TIP          Codigo,
        CI_FOLIO            FolioGrande,
        LI_ASUNTO           Codigo,
        LI_CDEPTO           Codigo,
        AN_NUMERO           NumeroEmpleado,
        EV_NUMERO           FolioGrande,
        VI_NUMERO           Visitante,
        LI_ENT_CAS          Codigo,
        LI_ENT_COR          FolioGrande,
        LI_ENT_FEC          Fecha,
        LI_ENT_HOR          Hora,
        LI_ENT_VIG          Usuario,
        LI_SAL_CAS          Codigo,
        LI_SAL_COR          FolioGrande,
        LI_SAL_FEC          Fecha,
        LI_SAL_HOR          Hora,
        LI_SAL_VIG          Usuario,
        LI_NOMBRE           DescLarga,
        LI_EMPRESA          DescLarga,
        LI_ANFITR           DescLarga,
        LI_DEPTO            Descripcion,
        LI_GAFETE           DescCorta,
        LI_ID               DescLarga,
        LI_CAR_PLA          Referencia,
        LI_CAR_DES          DescLarga,
        LI_CAR_EST          Referencia,
        LI_STATUS           Status,
        LI_TEXTO1           DescLarga,
        LI_TEXTO2           DescLarga,
        LI_TEXTO3           DescLarga,
        LI_OBSERVA          Memo,
        LI_DEVGAF 		   Booleano
)
GO

ALTER TABLE LIBRO
      ADD CONSTRAINT PK_LIBRO PRIMARY KEY(LI_FOLIO)
GO

