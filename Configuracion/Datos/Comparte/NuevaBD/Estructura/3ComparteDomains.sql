exec sp_addtype Fecha, 'datetime', 'NOT NULL'
exec sp_bindefault FechaVacia, Fecha
go

exec sp_addtype FolioChico, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, FolioChico
go

exec sp_addtype NumeroEmpleado, 'int', 'NOT NULL'
exec sp_bindefault Cero, NumeroEmpleado
go

exec sp_addtype Status, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Status
go

exec sp_addtype Usuario, 'smallint', 'NOT NULL'
exec sp_bindefault Cero, Usuario
go

exec sp_addtype Accesos, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, Accesos
go

exec sp_addtype Booleano, 'char(1)', 'NOT NULL'
exec sp_bindefault BooleanoNO, Booleano
go

exec sp_addtype Codigo, 'char(6)', 'NOT NULL'
exec sp_bindefault StringVacio, Codigo
go

exec sp_addtype Codigo1, 'char(1)', 'NOT NULL'
exec sp_bindefault StringVacio, Codigo1
go

exec sp_addtype CodigoEmpresa, 'char(10)', 'NOT NULL'
exec sp_bindefault StringVacio, CodigoEmpresa
go

exec sp_addtype CodigosImpresora, 'varchar(50)', 'NOT NULL'
exec sp_bindefault StringVacio, CodigosImpresora
go

exec sp_addtype Descripcion, 'varchar(30)', 'NOT NULL'
exec sp_bindefault StringVacio, Descripcion
go

exec sp_addtype Hora, 'char(4)', 'NOT NULL'
exec sp_bindefault StringVacio, Hora
go

exec sp_addtype LinxID, 'varchar(11)', 'NOT NULL'
exec sp_bindefault StringVacio, LinxID
go

exec sp_addtype NombreImpresora, 'varchar(35)', 'NOT NULL'
exec sp_bindefault StringVacio, NombreImpresora
go

exec sp_addtype Observaciones, 'varchar(50)', 'NOT NULL'
exec sp_bindefault StringVacio, Observaciones
go

exec sp_addtype Passwd, 'varchar(30)', 'NOT NULL'
exec sp_bindefault StringVacio, Passwd
go

exec sp_addtype Referencia, 'char(8)', 'NOT NULL'
exec sp_bindefault StringVacio, Referencia
go

exec sp_addtype UsuarioCorto, 'varchar(15)', 'NOT NULL'
exec sp_bindefault StringVacio, UsuarioCorto
go

exec sp_addtype Memo, 'text', 'NULL'
go

exec sp_addtype Imagen, 'image', 'NULL'
go

exec sp_addtype FolioGrande, 'int', 'NOT NULL'
exec sp_bindefault Cero, FolioGrande
go

exec sp_addtype Pesos, 'NUMERIC(15,2)', 'NOT NULL'
exec sp_bindefault Cero, Pesos
go

exec sp_addtype DatoReporte, 'varchar(50)', 'NOT NULL'
exec sp_bindefault StringVacio, DatoReporte
go

exec sp_addtype Formula, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, Formula
go

exec sp_addtype HoraMinutosSegundos, 'varchar(8)', 'NOT NULL'
exec sp_bindefault StringVacio, HoraMinutosSegundos
go

exec sp_addtype Ruta_URL, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, Ruta_URL
go

exec sp_addtype Titulo, 'varchar(70)', 'NOT NULL'
exec sp_bindefault StringVacio, Titulo
go

exec sp_addtype Color, "varchar(8)", "NOT NULL"
exec sp_bindefault StringVacio, Color
go

exec sp_addtype FontName, "varchar(40)", "NOT NULL"
exec sp_bindefault StringVacio, FontName
go

exec sp_addtype NombreArchivo, "varchar(150)", "NOT NULL"
exec sp_bindefault StringVacio, NombreArchivo
go

exec sp_addtype Texto, "varchar(254)", "NOT NULL"
exec sp_bindefault StringVacio, Texto
go

exec sp_addtype DescLarga, "varchar(40)", "NOT NULL"
exec sp_bindefault StringVacio, DescLarga
go


exec sp_addtype TextoBitacora, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, TextoBitacora
go

exec sp_addtype ComputerName, 'varchar(63)', 'NOT NULL'
exec sp_bindefault StringVacio, ComputerName
go

exec sp_addtype RelojLinx, 'varchar(4)', 'NOT NULL'
exec sp_bindefault StringVacio, RelojLinx
go

EXEC SP_ADDTYPE PATHARCHIVO, 'VARCHAR(150)', 'NOT NULL'
EXEC SP_BINDEFAULT STRINGVACIO, PATHARCHIVO
GO

EXEC SP_ADDTYPE CONDICION, 'CHAR(8)', 'NOT NULL'
EXEC SP_BINDEFAULT CODIGOVACIO, CONDICION
GO

EXEC SP_ADDTYPE NOMBRECAMPO, 'VARCHAR(10)', 'NOT NULL'
EXEC SP_BINDEFAULT STRINGVACIO, NOMBRECAMPO
GO

EXEC SP_ADDTYPE TITULOCAMPO, 'VARCHAR(30)', 'NOT NULL'
EXEC SP_BINDEFAULT STRINGVACIO, TITULOCAMPO
GO

EXEC SP_ADDTYPE CAMPOSREQUERIDOS, 'VARCHAR(255)', 'NOT NULL'
EXEC SP_BINDEFAULT STRINGVACIO, CAMPOSREQUERIDOS
GO

EXEC SP_ADDTYPE TAMANIO, 'NUMERIC(15,3)', 'NOT NULL'
EXEC SP_BINDEFAULT CERO, TAMANIO
GO

EXEC SP_ADDTYPE MASCARA, 'VARCHAR(20)', 'NOT NULL'
EXEC SP_BINDEFAULT STRINGVACIO, MASCARA
GO

EXEC SP_ADDTYPE WINDOWSNAME, 'VARCHAR(32)', 'NOT NULL'
EXEC SP_BINDEFAULT STRINGVACIO, WINDOWSNAME
GO

EXEC SP_ADDTYPE GUID, 'UNIQUEIDENTIFIER', 'NOT NULL'
EXEC SP_BINDEFAULT NEWGUID, GUID
GO

EXEC SP_ADDTYPE NULLGUID, 'UNIQUEIDENTIFIER', 'NULL'
EXEC SP_ADDTYPE BINARIO, 'IMAGE', 'NULL'
GO

EXEC SP_ADDTYPE XMLDATA, 'TEXT', 'NULL'
EXEC SP_ADDTYPE NOTAS, 'TEXT', 'NULL'
GO

exec SP_ADDTYPE MACADDRESS, 'VARCHAR(50)', 'NOT NULL'
exec SP_BINDEFAULT STRINGVACIO, MACADDRESS
GO

exec sp_addtype TextoLargo, 'varchar(1024)', 'NOT NULL'
exec sp_bindefault StringVacio, TextoLargo
GO

exec sp_addtype PasswdLrg, 'varchar(70)', 'NOT NULL'
exec sp_bindefault StringVacio, PasswdLrg
GO

exec sp_addtype EmailAdd, 'varchar(320)', 'NOT NULL'
exec sp_bindefault StringVacio, EmailAdd
GO

