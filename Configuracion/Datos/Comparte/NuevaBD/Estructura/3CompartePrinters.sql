INSERT INTO CLAVES (CL_NUMERO, CL_TEXTO ) VALUES ( 1, 'DEMO' )
GO

INSERT INTO GRUPO (GR_CODIGO, GR_DESCRIP, GR_PADRE) VALUES (1, 'Administradores', 0 )
GO
INSERT INTO GRUPO (GR_CODIGO, GR_DESCRIP, GR_PADRE) VALUES (2, 'Recursos Humanos', 1 )
GO
INSERT INTO GRUPO (GR_CODIGO, GR_DESCRIP, GR_PADRE) VALUES (3, 'Nóminas', 1 )
GO

INSERT INTO USUARIO (US_CODIGO, US_CORTO, GR_CODIGO, US_NIVEL, US_NOMBRE, US_PASSWRD, US_ACTIVO, US_PORTAL )
             VALUES (1, 'Tress', 1, 1, 'Usuario Nuevo', 'C540F525', 'S', 'N' )
GO

INSERT INTO PRINTER VALUES ('Epson E/F/J/L/RX/LQ', '12', '27,80,18', '27,77,18', '15', '', '', '27,64', '27,45,1', '27,45,0', '27,71', '27,72', '27,50', '27,48', '27,52', '27,53', '' )
GO
INSERT INTO PRINTER VALUES ('HP Laser', '12', '27,38,107,48,83,27,40,115,48,84', '27,40,115,49,49,72', '27,38,107,50,83', '', '', '27,69', '27,38,100,68', '27,38,100,64', '27,40,115,51,66', '27,40,115,48,66', '27,38,108,54,68', '27,38,108,56,68', '27,40,115,49,83', '27,40,115,48,83', '27,38,108,49,79' )
GO
INSERT INTO PRINTER VALUES ('IBM Color', '12', '18', '27,58', '15', '', '', '27,63', '27,45,1', '27,45,0', '27,71', '27,72', '27,50', '27,48', '27,45,1', '27,45,0', '' )
GO
INSERT INTO PRINTER VALUES ('IBM Proprinter', '12', '18', '27,58', '15', '', '', '', '27,45,1', '27,45,0', '27,71', '27,72', '27,50', '27,48', '27,18', '27,20', '' )
GO

INSERT INTO WACCION( WA_CODIGO, WA_NOMBRE, WA_DESCRIP ) VALUES ( 'FIRMA', 'Pide Autorización', 'Envía eMail solicitando autorización del documento' )
GO
INSERT INTO WACCION( WA_CODIGO, WA_NOMBRE, WA_DESCRIP ) VALUES ( 'GRABA3', 'Registra en TRESS', 'Registra los datos del documento en Sistema TRESS' )
GO
INSERT INTO WACCION VALUES ('CALLWS', 'Comunica a WebService', 'Envia el XMLData a un Webservice especificado')
GO

insert into MENSAJE values (1,'Actualización software terminal',1,0,'S')
GO

insert into MENSAJE values (4,'Actualización de plantillas',4,1440,'S')
GO

insert into MENSAJE values (5,'Actualización de propiedades de terminal',5,0,'S')
GO

insert into MENSAJE values (6,'Depuracion de plantillas',6,1440,'S')
GO

insert into MENSAJE values (10,'Recolección de lecturas offline',10,1440,'S')
GO
