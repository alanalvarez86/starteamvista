
CREATE TABLE GRUPO (
       GR_CODIGO            FolioChico,
       GR_DESCRIP           Descripcion,
       GR_PADRE             FolioChico
)
go

ALTER TABLE GRUPO
       ADD CONSTRAINT PK_GRUPO PRIMARY KEY (GR_CODIGO)
go

CREATE TABLE USUARIO (
       US_CODIGO            Usuario,
       US_CORTO             UsuarioCorto,
       GR_CODIGO            FolioChico,
       US_NIVEL             Status,
       US_NOMBRE            Descripcion,
       US_PASSWRD           Passwd,
       US_BLOQUEA           Booleano,
       US_CAMBIA            Booleano,
       US_FEC_IN            Fecha,
       US_FEC_OUT           Fecha,
       US_DENTRO            Booleano,
       US_ARBOL             Booleano,
       US_FORMA             FolioChico,
       US_BIT_LST           Booleano,
       US_BIO_ID            Passwd,
       US_FEC_SUS           Fecha,
       US_FALLAS            FolioChico,
       US_MAQUINA           Ruta_URL,
       US_FEC_PWD           Fecha,
       US_EMAIL             Observaciones,
       US_FORMATO           Status,
       US_LUGAR             Referencia,
       US_PAGINAS           FolioChico,
       CM_CODIGO            CodigoEmpresa,
       CB_CODIGO            NumeroEmpleado,
       US_ANFITRI           NumeroEmpleado,
       US_ACTIVO 	          Booleano,
       US_PORTAL            Booleano,
       US_DOMAIN            Observaciones,
       US_JEFE              Usuario,
       US_PAG_DEF           Usuario,
       US_WF_OUT            Booleano,
       US_WF_ALT            Usuario,
       US_CLASICA           Booleano
)
go

CREATE UNIQUE INDEX XAK1USUARIO ON USUARIO
(
       US_CORTO
)
go

ALTER TABLE USUARIO
       ADD CONSTRAINT PK_USUARIO PRIMARY KEY (US_CODIGO)
go

CREATE TABLE USER_BIT (
       US_CODIGO            Usuario,
       UB_CODIGO            Usuario
)
go

ALTER TABLE USER_BIT
       ADD CONSTRAINT PK_USER_BIT PRIMARY KEY (US_CODIGO, UB_CODIGO)
go

CREATE TABLE CORREO (
       US_CODIGO            Usuario,
       CC_FOLIO             FolioChico,
       CC_FECHA             Fecha,
       CC_TEMA              Observaciones,
       CC_COMENTA           Accesos,
       CC_AUTOR             Descripcion,
       CC_AUT_COD           Usuario,
       CC_FEC_AUT           Fecha,
       CC_HOR_AUT           Referencia,
       CC_FEC_LEE           Fecha,
       CC_HOR_LEE           Referencia
)
go

ALTER TABLE CORREO
       ADD CONSTRAINT PK_CORREO PRIMARY KEY (US_CODIGO, CC_FOLIO)
go

CREATE TABLE ARBOL (
       US_CODIGO            Usuario,
       AB_ORDEN             FolioChico,
       AB_NIVEL             FolioChico,
       AB_NODO              FolioChico,
       AB_DESCRIP           Descripcion,
       AB_MODULO            Status
)
go

ALTER TABLE ARBOL
       ADD CONSTRAINT PK_ARBOL PRIMARY KEY (US_CODIGO,AB_ORDEN,AB_MODULO)
go

CREATE TABLE POLL (
       PO_LINX              LinxID,
       PO_EMPRESA           Codigo1,
       PO_NUMERO            NumeroEmpleado,
       PO_FECHA             Fecha,
       PO_HORA              Hora,
       PO_LETRA             Codigo1
)
go

ALTER TABLE POLL
	add CONSTRAINT PK_POLL PRIMARY KEY ( PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA, PO_LINX )
GO

CREATE TABLE COMPANY (
       CM_CODIGO            CodigoEmpresa,
       CM_NOMBRE            Observaciones,
       CM_ACUMULA           CodigoEmpresa,
       CM_CONTROL           Referencia,
       CM_EMPATE            NumeroEmpleado,
       CM_ALIAS             Descripcion,
       CM_PASSWRD           PasswdLrg,
       CM_USRNAME           UsuarioCorto,
       CM_USACAFE           Booleano,
       CM_USACASE           Booleano,
       CM_NIVEL0            Accesos,
       CM_DATOS             Accesos,
       CM_DIGITO            Codigo1,
       CM_KIOSKO            Booleano,
       CM_BACKUP            Booleano,
       CM_ACTIVOS           NumeroEmpleado,
       CM_KCLASIFI		      FolioChico,
       CM_KCONFI            Booleano,
       CM_KUSERS            Descripcion,
       CM_CTRL_RL			      Referencia,
       CM_CHKSUM			      Accesos,
       DB_CODIGO            CodigoEmpresa,
       CM_WFEMPID FolioGrande
)
GO

ALTER TABLE COMPANY
       ADD CONSTRAINT PK_COMPANY PRIMARY KEY (CM_CODIGO)
go

CREATE TABLE ACCESO (
       GR_CODIGO            FolioChico,
       CM_CODIGO            CodigoEmpresa,
       AX_NUMERO            FolioChico,
       AX_DERECHO           FolioChico
)
go

ALTER TABLE ACCESO
       ADD CONSTRAINT PK_ACCESO PRIMARY KEY (GR_CODIGO, CM_CODIGO, AX_NUMERO)
go

CREATE TABLE CLAVES (
       CL_NUMERO            FolioChico,
       CL_TEXTO             Accesos
)
go

ALTER TABLE CLAVES
       ADD CONSTRAINT PK_CLAVES PRIMARY KEY (CL_NUMERO)
go

CREATE TABLE PRINTER (
       PI_NOMBRE            NombreImpresora,
       PI_EJECT             CodigosImpresora,
       PI_CHAR_10           CodigosImpresora,
       PI_CHAR_12           CodigosImpresora,
       PI_CHAR_17           CodigosImpresora,
       PI_EXTRA_1           CodigosImpresora,
       PI_EXTRA_2           CodigosImpresora,
       PI_RESET             CodigosImpresora,
       PI_UNDE_ON           CodigosImpresora,
       PI_UNDE_OF           CodigosImpresora,
       PI_BOLD_ON           CodigosImpresora,
       PI_BOLD_OF           CodigosImpresora,
       PI_6_LINES           CodigosImpresora,
       PI_8_LINES           CodigosImpresora,
       PI_ITAL_ON           CodigosImpresora,
       PI_ITAL_OF           CodigosImpresora,
       PI_LANDSCA           CodigosImpresora
)
go

ALTER TABLE PRINTER
       ADD CONSTRAINT PK_PRINTER PRIMARY KEY (PI_NOMBRE)
go

CREATE TABLE NIVEL0 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion
)
go

ALTER TABLE NIVEL0
       ADD CONSTRAINT PK_NIVEL0 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE PASSLOG (
       US_CODIGO USUARIO,
       PL_FOLIO FOLIOCHICO,
       PL_PASSWRD PASSWD )
GO

ALTER TABLE PASSLOG
       ADD CONSTRAINT PK_PASS_LOG PRIMARY KEY (US_CODIGO, PL_FOLIO)
GO

ALTER TABLE USUARIO
       ADD CONSTRAINT FK_Usuario_Grupo
       FOREIGN KEY (GR_CODIGO)
       REFERENCES GRUPO
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
go

ALTER TABLE USER_BIT
       ADD CONSTRAINT FK_User_Bit_Usario
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE CORREO
       ADD CONSTRAINT FK_Correo_Usuario
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE ARBOL
       ADD CONSTRAINT FK_Arbol_Usuario
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE ACCESO
       ADD CONSTRAINT FK_Acceso_Company
       FOREIGN KEY (CM_CODIGO)
       REFERENCES COMPANY
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE ACCESO
       ADD CONSTRAINT FK_Acceso_Grupo
       FOREIGN KEY (GR_CODIGO)
       REFERENCES GRUPO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

ALTER TABLE PASSLOG
       ADD CONSTRAINT FK_PASSLOG_USUARIO
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TABLE GLOBAL (
       GL_CODIGO            FolioChico,
       GL_DESCRIP           Descripcion,
       GL_FORMULA           Formula,
       GL_TIPO              Status,
       US_CODIGO            Usuario,
       GL_CAPTURA           Fecha,
       GL_NIVEL             Status )
GO

ALTER TABLE GLOBAL ADD CONSTRAINT PK_GLOBAL PRIMARY KEY (GL_CODIGO)
GO

CREATE TABLE PAGINA (
       US_CODIGO            Usuario,
       PA_ORDEN             FolioChico,
       PA_TITULO            Descripcion,
       PA_COLS              FolioChico )
GO

ALTER TABLE PAGINA ADD CONSTRAINT PK_PAGINA PRIMARY KEY (US_CODIGO, PA_ORDEN)
GO

CREATE TABLE CONTIENE (
       US_CODIGO            Usuario,
       PA_ORDEN             FolioChico,
       CT_COLUMNA           FolioChico,
       CT_ORDEN             FolioChico,
       CT_TIPO              Status,
       CT_MOSTRAR           FolioChico,
       CT_REPORTE           FolioGrande,
       CT_CLASIFI           Codigo )
GO

ALTER TABLE CONTIENE ADD CONSTRAINT PK_CONTIENE PRIMARY KEY (US_CODIGO, PA_ORDEN, CT_COLUMNA, CT_ORDEN)
GO

CREATE TABLE REPORTAL (
       RP_FOLIO             FolioGrande,
       RP_TITULO            Titulo,
       RP_DESCRIP           Memo,
       RP_FECHA             Fecha,
       RP_HORA              Hora,
       RP_IMAGEN            Ruta_URL,
       RP_USUARIO           Usuario,
       RP_TIPO              Status,
       RP_CLASIFI           Codigo,
       RP_COLS              FolioChico,
       RP_ROWS              FolioGrande )
GO

ALTER TABLE REPORTAL ADD CONSTRAINT PK_REPORTAL PRIMARY KEY (RP_FOLIO)
GO

CREATE TABLE REP_DATA (
       RP_FOLIO             FolioGrande,
       RD_ORDEN             FolioGrande,
       RD_DATO1             DatoReporte,
       RD_DATO2             DatoReporte,
       RD_DATO3             DatoReporte,
       RD_DATO4             DatoReporte,
       RD_DATO5             DatoReporte,
       RD_DATO6             DatoReporte,
       RD_DATO7             DatoReporte,
       RD_DATO8             DatoReporte,
       RD_DATO9             DatoReporte,
       RD_DATO10            DatoReporte )
GO

ALTER TABLE REP_DATA ADD CONSTRAINT PK_REP_DATA PRIMARY KEY (RP_FOLIO, RD_ORDEN)
GO

CREATE TABLE REP_COLS (
       RP_FOLIO             FolioGrande,
       RC_ORDEN             FolioChico,
       RC_TITULO            Descripcion,
       RC_TIPO              Status,
       RC_ANCHO             FolioChico )
GO

ALTER TABLE REP_COLS ADD CONSTRAINT PK_REP_COLS PRIMARY KEY (RP_FOLIO, RC_ORDEN)
GO

CREATE TABLE BUZONSUG (
       BU_FOLIO             FolioGrande,
       BU_FECHA             Fecha,
       BU_HORA              Hora,
       BU_MEMO              Memo,
       BU_TITULO            Titulo,
       BU_USUARIO           Usuario,
       BU_AUTOR             Descripcion,
       BU_EMAIL             Titulo )
GO

ALTER TABLE BUZONSUG ADD CONSTRAINT PK_BUZONSUG PRIMARY KEY (BU_FOLIO)
GO

CREATE TABLE TDOCUMEN (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

ALTER TABLE TDOCUMEN ADD CONSTRAINT PK_TDOCUMEN PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE TEVENTO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

ALTER TABLE TEVENTO ADD CONSTRAINT PK_TEVENTO PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE TNOTICIA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Formula,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

ALTER TABLE TNOTICIA ADD CONSTRAINT PK_TNOTICIA PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE DOCUMENT (
       DC_FOLIO             FolioGrande,
       DC_FECHA             Fecha,
       DC_HORA              Hora,
       DC_RUTA              Ruta_URL,
       DC_IMAGEN            Ruta_URL,
       DC_TITULO            Titulo,
       DC_MEMO              Memo,
       DC_USUARIO           Usuario,
       DC_AUTOR             Descripcion,
       DC_CLASIFI           Codigo,
       DC_PALABRA           Formula )
GO

ALTER TABLE DOCUMENT ADD CONSTRAINT PK_DOCUMENT PRIMARY KEY (DC_FOLIO)
GO

CREATE TABLE EVENTO (
       EV_FOLIO             FolioGrande,
       EV_FEC_INI           Fecha,
       EV_FEC_FIN           Fecha,
       EV_HORA              Hora,
       EV_LUGAR             Titulo,
       EV_IMAGEN            Ruta_URL,
       EV_MEMO              Memo NOT NULL,
       EV_TITULO            Titulo,
       EV_USUARIO           Usuario,
       EV_CLASIFI           Codigo,
       EV_AUTOR             Descripcion,
       EV_URL               Ruta_URL )
GO

ALTER TABLE EVENTO ADD CONSTRAINT PK_EVENTO PRIMARY KEY (EV_FOLIO)
GO

CREATE TABLE NOTICIA (
       NT_FOLIO             FolioGrande,
       NT_FECHA             Fecha,
       NT_HORA              Hora,
       NT_TITULO            Titulo,
       NT_MEMO              Memo,
       NT_IMAGEN            Ruta_URL,
       NT_USUARIO           Usuario,
       NT_CLASIFI           Codigo,
       NT_AUTOR             Descripcion,
       NT_CORTA             Memo,
       NT_SUB_TIT           Titulo )
GO

ALTER TABLE NOTICIA ADD CONSTRAINT PK_NOTICIA PRIMARY KEY (NT_FOLIO)
GO

CREATE TABLE TARJETA (
       TJ_EMPRESA           DescLarga,
       TJ_NOMBRES           DescLarga,
       TJ_TELEFON           DescLarga,
       TJ_DIRECC1           DescLarga,
       TJ_DIRECC2           DescLarga,
       TJ_COMENTA           DescLarga,
       TJ_EMAIL             DescLarga )
GO

CREATE TABLE EMP_ID(
       ID_NUMERO Descripcion,
       CM_CODIGO CodigoEmpresa,
       CB_CODIGO NumeroEmpleado,
       IV_CODIGO NumeroEmpleado,
	   GP_CODIGO Codigo
	   )
GO

ALTER TABLE EMP_ID ADD CONSTRAINT PK_EMP_ID PRIMARY KEY (ID_NUMERO )
GO

alter table REP_DATA
       add constraint FK_REP_DATA_REPORTAL
       foreign key (RP_FOLIO)
       references REPORTAL
       on delete CASCADE
       on update CASCADE
GO

alter table REP_COLS
       add constraint FK_REP_COLS_REPORTAL
       foreign key (RP_FOLIO)
       references REPORTAL
       on delete CASCADE
       on update CASCADE
GO

ALTER TABLE EMP_ID
       ADD CONSTRAINT FK_EMP_ID_COMPANY
       FOREIGN KEY (CM_CODIGO)
       REFERENCES COMPANY
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

CREATE TABLE BITACORA (
       US_CODIGO            Usuario,
       BI_FECHA             Fecha,
       BI_HORA              HoraMinutosSegundos,
       BI_PROC_ID           Status,
       BI_TIPO              Status,
       BI_NUMERO            FolioGrande,
       BI_TEXTO             Observaciones,
       CB_CODIGO            NumeroEmpleado,
       BI_DATA              Memo,
       BI_CLASE             Status,
       BI_FEC_MOV           Fecha,
	  LLAVE		        Int identity ( 1, 1)
)
GO

CREATE INDEX XIE1BITACORA ON BITACORA
(
       BI_NUMERO
)
GO

CREATE INDEX XIE2BITACORA ON BITACORA
(
       BI_FECHA,
       BI_HORA
)
GO


CREATE TABLE KESTILO (
       KE_CODIGO            Codigo,  
       KE_NOMBRE            Descripcion,
       KE_COLOR             FolioGrande,
       KE_F_NAME            Descripcion,
       KE_F_SIZE            FolioChico,
       KE_F_COLR            FolioGrande,
       KE_F_BOLD            Booleano,
       KE_F_ITAL            Booleano,
       KE_F_SUBR            Booleano,
       KE_TIPO              Status,
       KE_RUTA              Ruta_URL,
	  KE_BITMAP            Ruta_URL
)
GO


ALTER TABLE KESTILO
       ADD CONSTRAINT PK_KESTILO PRIMARY KEY (KE_CODIGO)
GO


CREATE TABLE KSCREEN (
       KS_CODIGO            Codigo,
       KS_NOMBRE            Descripcion,
       KS_PF_SIZE           FolioChico,
       KS_PF_COLR           FolioGrande,
       KS_PF_ALIN           Status,
       KS_PF_URL            Formula,
       KS_PB_SIZE           FolioChico,
       KS_PB_COLR           FolioGrande,
       KS_PB_ALIN           Status,
       KS_BTN_ALT           FolioChico,
       KS_BTN_SEP           FolioChico,
       KS_EST_NOR           Codigo,
       KS_EST_SEL           Codigo,
	  KS_BITMAP            Ruta_Url
)
GO


ALTER TABLE KSCREEN
       ADD CONSTRAINT PK_KSCREEN PRIMARY KEY (KS_CODIGO)
GO

CREATE TABLE KBOTON (
       KS_CODIGO            Codigo,
       KB_ORDEN             FolioChico,
       KB_TEXTO             Observaciones,
       KB_BITMAP            Ruta_URL,
       KB_ACCION            Status,
       KB_URL               Ruta_URL,
       KB_SCREEN            Codigo,
       KB_ALTURA            FolioChico,
       KB_LUGAR             Status,
       KB_SEPARA            FolioChico,
	     KB_REPORTE		        FolioChico,
	     KB_POS_BIT           Status,
	     KB_RESTRIC		        Booleano,
	     KB_USA_EMPL          Booleano,
       KB_IMP_DIR           Booleano
)
GO

ALTER TABLE KBOTON
       ADD CONSTRAINT PK_KBOTON PRIMARY KEY (KS_CODIGO, KB_ORDEN)
GO



CREATE TABLE KSHOW (
       KW_CODIGO            Codigo,
       KW_NOMBRE            Descripcion,
       KW_SCR_DAT           Codigo,
       KW_TIM_DAT           FolioChico,
       KW_GET_NIP           Booleano,
	  CM_CODIGO            CodigoEmpresa
)
GO


ALTER TABLE KSHOW
       ADD CONSTRAINT PK_KSHOW PRIMARY KEY (KW_CODIGO)
GO



CREATE TABLE KSHOWINF (
       KW_CODIGO            Codigo,
       KI_ORDEN             FolioChico,
       KI_TIMEOUT           FolioChico,
       KI_URL               Ruta_URL,
       KI_REFRESH           Booleano
)
GO



ALTER TABLE KSHOWINF
       ADD CONSTRAINT PK_KSHOWINF PRIMARY KEY (KW_CODIGO, KI_ORDEN)
GO


ALTER TABLE KBOTON
       ADD CONSTRAINT FK_KBoton_KScreen
       FOREIGN KEY (KS_CODIGO)
       REFERENCES KSCREEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO


ALTER TABLE KSHOWINF
       ADD CONSTRAINT FK_KShowInf_KShow
       FOREIGN KEY (KW_CODIGO)
       REFERENCES KSHOW
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO


CREATE TABLE BITKIOSCO (
       CB_CODIGO            NumeroEmpleado,      
       CM_CODIGO	        CodigoEmpresa,        
       BI_FECHA             Fecha,       
       BI_HORA              HoraMinutosSegundos,  
       BI_TIPO              Status,      
       BI_ACCION	        Status,      
       BI_NUMERO            FolioGrande,  
       BI_TEXTO             TextoBitacora,     
       BI_UBICA             Observaciones,	
       BI_KIOSCO	        ComputerName,     
	  BI_FEC_MOV           Fecha,            
	  BI_FEC_HORA		   Fecha      
)
go



CREATE INDEX XIE1BITKIOSCO ON BITKIOSCO
(
       BI_NUMERO
)
go



CREATE INDEX XIE2BITKIOSCO ON BITKIOSCO
(
       BI_FECHA,
       BI_HORA
)
go

CREATE TABLE KBOTON_ACC (
       KS_CODIGO    Codigo, 
	  KB_ORDEN	FolioChico, 
	  GR_CODIGO	FolioChico  
)   	
GO

ALTER TABLE KBOTON_ACC
       ADD CONSTRAINT PK_KBOTON_ACC PRIMARY KEY (KS_CODIGO, KB_ORDEN, GR_CODIGO )
GO

ALTER TABLE KBOTON_ACC
       ADD CONSTRAINT FK_KBOTON_ACC
       FOREIGN KEY (KS_CODIGO, KB_ORDEN)
       REFERENCES KBOTON
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE KBOTON_ACC
       ADD CONSTRAINT FK_KBOTON_ACC_GRUPO
       FOREIGN KEY (GR_CODIGO)
       REFERENCES GRUPO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO


CREATE TABLE ROL (
	RO_CODIGO Codigo NOT NULL ,
	RO_NOMBRE Descripcion NOT NULL ,
	RO_DESCRIP Accesos NOT NULL ,
	RO_NIVEL FolioChico NOT NULL,
	LLAVE int identity  ) 
GO


ALTER TABLE ROL
       ADD CONSTRAINT PK_ROL PRIMARY KEY (RO_CODIGO)
go



CREATE TABLE USER_ROL (
	US_CODIGO Usuario NOT NULL ,
	RO_CODIGO Codigo NOT NULL ,
	UR_PPAL Booleano NOT NULL,
     LLAVE int identity 
)
GO



ALTER TABLE USER_ROL 
		ADD CONSTRAINT PK_USER_ROL PRIMARY KEY  (US_CODIGO,RO_CODIGO)  
GO


ALTER TABLE USER_ROL
       ADD CONSTRAINT FK_USER_ROL_ROL 
       FOREIGN KEY (RO_CODIGO)
       REFERENCES ROL
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO



ALTER TABLE USER_ROL
       ADD CONSTRAINT FK_USER_ROL_USUARIO 
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TABLE BITCAFE (
          BC_FOLIO FolioGrande IDENTITY,   
          CB_CODIGO   NumeroEmpleado NOT NULL,  
          IV_CODIGO NumeroEmpleado NOT NULL,  
          BC_EMPRESA Codigo1 NOT NULL,   
          BC_CREDENC  Codigo1 NOT NULL,    
          BC_FECHA    Fecha NOT NULL,   
          BC_HORA     Hora NOT NULL,    
          BC_RELOJ    RelojLinx NOT NULL,  
          BC_TIPO  Codigo1 NOT NULL ,  
          BC_COMIDAS  FolioChico NOT NULL ,  
          BC_EXTRAS  FolioChico NOT NULL ,  
          BC_REG_EXT  Booleano NOT NULL,   
          BC_MANUAL  Booleano NOT NULL,   
          CL_CODIGO  FolioChico NOT NULL ,   
          BC_TGAFETE  Status NOT NULL,    
          BC_TIEMPO   FolioChico NOT NULL,  
          BC_STATUS   Status NOT NULL,
          BC_MENSAJE Observaciones,     
          BC_RESPUES Observaciones,    
          BC_CHECADA Observaciones    
)
GO


ALTER TABLE BITCAFE 
       ADD CONSTRAINT PK_BITCAFE PRIMARY KEY (BC_FOLIO)
GO

CREATE TABLE KEM_CONFI(

	CM_CODIGO CodigoEmpresa, 
	TB_CODIGO Codigo,
	KEM_USERS	Descripcion
)
GO

ALTER TABLE KEM_CONFI
       ADD CONSTRAINT PK_KEM_CONFI PRIMARY KEY (CM_CODIGO, TB_CODIGO)
GO


ALTER TABLE KEM_CONFI
       ADD CONSTRAINT FK_KEM_CONFI_COMPANY
       FOREIGN KEY (CM_CODIGO)
       REFERENCES COMPANY
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO


CREATE TABLE DISPOSIT
(
	DI_NOMBRE RelojLinx not null,
	DI_TIPO FolioChico not null,
	DI_DESCRIP Descripcion null,
	DI_NOTA Formula null,
	DI_IP ComputerName null,
	DI_UPDATE booleano
)
GO


ALTER TABLE DISPOSIT
       add CONSTRAINT PK_DISPOSIT PRIMARY KEY( DI_NOMBRE, DI_TIPO )
GO


CREATE TABLE DISXCOM
(
	CM_CODIGO CodigoEmpresa not null,
	DI_NOMBRE RelojLinx not null,
	DI_TIPO FolioChico not null
)
GO

ALTER TABLE DISXCOM add CONSTRAINT PK_DISXCOM PRIMARY KEY (CM_CODIGO, DI_NOMBRE, DI_TIPO)
GO


ALTER TABLE DISXCOM
      add CONSTRAINT FK_DISPO_X_COMP
      FOREIGN KEY( DI_NOMBRE, DI_TIPO )
      references DISPOSIT
      ON DELETE CASCADE
      ON UPDATE CASCADE
GO

CREATE TABLE ACC_ARBOL (
	AA_SOURCE	 Status NOT NULL,
	AX_NUMERO  FolioChico NOT NULL,
	AA_DESCRIP NombreArchivo NOT NULL,
	AA_VERSION FolioChico NOT NULL,
	AA_POSICIO FolioChico NOT NULL,
	AA_MODULO  Status NOT NULL,
	AA_SCREEN  Status NOT NULL,
	AA_INGLES	 NombreArchivo NOT NULL
)
GO

ALTER TABLE ACC_ARBOL
       ADD CONSTRAINT PK_ACC_ARBOL PRIMARY KEY (AA_SOURCE,AX_NUMERO)
GO

CREATE TABLE ACC_DER (
	AA_SOURCE	 Status NOT NULL,
	AX_NUMERO  FolioChico NOT NULL,
	AD_TIPO  Status NOT NULL,
	AD_ACCION Titulo NOT NULL,
	AD_IMPACTO Status NOT NULL,
	AD_POSICIO Status NOT NULL,
	AD_INGLES	 Titulo NOT NULL
)
GO

ALTER TABLE ACC_DER
       ADD CONSTRAINT PK_ACC_DER PRIMARY KEY (AA_SOURCE,AX_NUMERO,AD_TIPO)
GO

CREATE TABLE AUTOMOD (
	AM_CODIGO	 FolioChico NOT NULL,
	AM_NOMBRE  Titulo NOT NULL,
  AM_FECHA 	Fecha NOT NULL
)
GO

ALTER TABLE AUTOMOD
       ADD CONSTRAINT PK_AUTOMOD PRIMARY KEY (AM_CODIGO)
GO

CREATE TABLE DICCION (
		   DI_CLASIFI           STATUS,
		   DI_NOMBRE            NOMBRECAMPO,
		   DI_TITULO            TITULOCAMPO,
		   DI_CALC              STATUS,
		   DI_ANCHO             STATUS,
		   DI_MASCARA           DESCRIPCION,
		   DI_TFIELD            STATUS,
		   DI_ORDEN             BOOLEANO,
		   DI_REQUIER           CAMPOSREQUERIDOS,
		   DI_TRANGO            STATUS,
		   DI_NUMERO            STATUS,
		   DI_VALORAC           DESCRIPCION,
		   DI_RANGOAC           STATUS,
		   DI_CLAVES            TITULOCAMPO,
		   DI_TCORTO            TITULOCAMPO,
		   DI_CONFI             BOOLEANO
	)
GO

CREATE INDEX XIE1DICCION ON DICCION
	(
		   DI_CLASIFI,
		   DI_CALC
	)
GO

ALTER TABLE DICCION
	   ADD CONSTRAINT PK_DICCION PRIMARY KEY (DI_CLASIFI, DI_NOMBRE)
GO

CREATE TABLE PROCESO (
		   PC_PROC_ID           STATUS,
		   PC_NUMERO            FOLIOGRANDE,
		   US_CODIGO            USUARIO,
		   PC_FEC_INI           FECHA,
		   PC_HOR_INI           HORAMINUTOSSEGUNDOS,
		   PC_FEC_FIN           FECHA,
		   PC_HOR_FIN           HORAMINUTOSSEGUNDOS,
		   PC_ERROR             BOOLEANO,
		   CB_CODIGO            NUMEROEMPLEADO,
		   PC_MAXIMO            FOLIOGRANDE,
		   PC_PASO              FOLIOGRANDE,
		   US_CANCELA           USUARIO,
		   PC_PARAMS            FORMULA
	)
GO

CREATE INDEX XIE1PROCESO ON PROCESO
	(
		   PC_FEC_INI,
		   PC_ERROR
	)
GO

ALTER TABLE PROCESO
		   ADD CONSTRAINT PK_PROCESO PRIMARY KEY (PC_NUMERO)
GO

CREATE TABLE QUERYS (
		   QU_CODIGO            CONDICION,
		   QU_DESCRIP           FORMULA,
		   QU_FILTRO            FORMULA,
		   QU_NIVEL             STATUS,
		   US_CODIGO            USUARIO,
       QU_NAVEGA            BOOLEANO,
       QU_ORDEN             FOLIOCHICO,
       QU_CANDADO           BOOLEANO
	)
GO

ALTER TABLE QUERYS
		   ADD CONSTRAINT PK_QUERYS PRIMARY KEY (QU_CODIGO)
GO

CREATE TABLE REPORTE (
		   RE_CODIGO            FOLIOCHICO,
		   RE_NOMBRE            DESCRIPCION,
		   RE_TIPO              STATUS,
		   RE_TITULO            TITULO,
		   RE_ENTIDAD           STATUS,
		   RE_SOLOT             BOOLEANO,
		   RE_FECHA             FECHA,
		   RE_GENERAL           DESCRIPCION,
		   RE_REPORTE           PATHARCHIVO,
		   RE_CFECHA            NOMBRECAMPO,
		   RE_IFECHA            STATUS,
		   RE_HOJA              STATUS,
		   RE_ALTO              TAMANIO,
		   RE_ANCHO             TAMANIO,
		   RE_PRINTER           FORMULA,
		   RE_COPIAS            STATUS,
		   RE_PFILE             STATUS,
		   RE_ARCHIVO           PATHARCHIVO,
		   RE_VERTICA           BOOLEANO,
		   US_CODIGO            USUARIO,
		   RE_FILTRO            FORMULA,
		   RE_COLNUM            STATUS,
		   RE_COLESPA           TAMANIO,
		   RE_RENESPA           TAMANIO,
		   RE_MAR_SUP           TAMANIO,
		   RE_MAR_IZQ           TAMANIO,
		   RE_MAR_DER           TAMANIO,
		   RE_MAR_INF           TAMANIO,
		   RE_NIVEL             STATUS,
		   RE_FONTNAM           WINDOWSNAME,
		   RE_FONTSIZ           STATUS,
		   QU_CODIGO            CONDICION,
		   RE_CLASIFI           STATUS,
		   RE_CANDADO           STATUS
	)
GO

CREATE UNIQUE INDEX XAK1REPORTE ON REPORTE
	(
		   RE_NOMBRE
	)
GO

CREATE INDEX XIE1REPORTE ON REPORTE
	(
		   RE_CLASIFI,
		   RE_NOMBRE
	)
GO

ALTER TABLE REPORTE
		   ADD CONSTRAINT PK_REPORTE PRIMARY KEY (RE_CODIGO)
GO

CREATE TABLE CAMPOREP (
		   RE_CODIGO            FOLIOCHICO,
		   CR_TIPO              STATUS,
		   CR_POSICIO           STATUS,
		   CR_CLASIFI           STATUS,
		   CR_SUBPOS            STATUS,
		   CR_TABLA             STATUS,
		   CR_TITULO            TITULOCAMPO,
		   CR_REQUIER           FORMULA,
		   CR_CALC              STATUS,
		   CR_MASCARA           MASCARA,
		   CR_ANCHO             STATUS,
		   CR_OPER              STATUS,
		   CR_TFIELD            STATUS,
		   CR_SHOW              STATUS,
		   CR_DESCRIP           NOMBRECAMPO,
		   CR_COLOR             STATUS,
		   CR_FORMULA           FORMULA,
		   CR_BOLD              BOOLEANO,
		   CR_ITALIC            BOOLEANO,
		   CR_SUBRAYA           BOOLEANO,
		   CR_STRIKE            BOOLEANO,
		   CR_ALINEA            STATUS
	)
GO

ALTER TABLE CAMPOREP
		   ADD CONSTRAINT PK_CAMPOREP PRIMARY KEY (RE_CODIGO, CR_TIPO, CR_POSICIO, CR_SUBPOS)
GO

ALTER TABLE CAMPOREP
		   ADD CONSTRAINT FK_CAMPOREP_REPORTE
		   FOREIGN KEY (RE_CODIGO)
		   REFERENCES REPORTE
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

CREATE TABLE MISREPOR (
		   US_CODIGO            USUARIO,
		   RE_CODIGO            FOLIOCHICO )
GO

ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

ALTER TABLE MISREPOR
		   ADD CONSTRAINT FK_MISREPOR_REPORTE
		   FOREIGN KEY (RE_CODIGO)
		   REFERENCES REPORTE
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

CREATE TABLE WACCION(
		WA_CODIGO           CODIGO,
		WA_NOMBRE           OBSERVACIONES,
		WA_DESCRIP          MEMO
	)
GO

ALTER TABLE WACCION
		  ADD CONSTRAINT PK_WACCION PRIMARY KEY( WA_CODIGO )
GO

CREATE TABLE WPROCESO(
		WP_FOLIO           FOLIOGRANDE,
		WM_CODIGO          CODIGO,
		WP_NOMBRE          OBSERVACIONES,
		WP_USR_INI         USUARIO,
		WP_FEC_INI         FECHA,
		WP_FEC_FIN         FECHA,
		WP_STATUS          STATUS,
		WP_PASOS           FOLIOCHICO,
		WP_PASO            FOLIOCHICO,
		WP_XMLDATA         XMLData,
		WP_NOTAS           NOTAS,
		WP_DATA            BINARIO,
    CB_CODIGO          NUMEROEMPLEADO,
    CM_CODIGO          CODIGOEMPRESA,
	  WP_PROCESA         Status,
	  WP_XMLDOC		   xml, 
    WP_MOV3            Descripcion,
    WP_EMPRESA         Descripcion,
	  WP_XMLADD        xml
	)
GO

ALTER TABLE WPROCESO
		  ADD CONSTRAINT PK_WPROCESO PRIMARY KEY( WP_FOLIO )
GO

CREATE INDEX WP_XML_MOV3 ON WPROCESO(WP_MOV3)
GO

CREATE INDEX WP_XML_XEMPRESA ON WPROCESO(WP_EMPRESA)
GO

CREATE PRIMARY XML INDEX WP_XMLDOC_IDX  ON WPROCESO(WP_XMLDOC)

GO

CREATE XML INDEX WP_XMLDOC_IDX_PROPERTY ON WPROCESO(WP_XMLDOC) USING XML INDEX WP_XMLDOC_IDX FOR PROPERTY

GO

CREATE TABLE WARCHIVO(
		WP_FOLIO            FOLIOGRANDE,
		WH_ORDEN            FOLIOCHICO,
		WH_NOMBRE           ACCESOS,
		WH_RUTA             ACCESOS
	)
GO

ALTER TABLE WARCHIVO
		  ADD CONSTRAINT PK_WARCHIVO PRIMARY KEY( WP_FOLIO, WH_ORDEN )
GO

ALTER TABLE WARCHIVO
		   ADD CONSTRAINT FK_WARCHIVO_WPROCESO
		   FOREIGN KEY (WP_FOLIO)
		   REFERENCES WPROCESO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

CREATE TABLE WMENSAJE(
		WN_GUID            GUID,
		WP_FOLIO           FOLIOGRANDE,
		WE_ORDEN           FOLIOCHICO,
		WT_GUID            NULLGUID,
		WN_USR_INI         USUARIO,
		WN_USR_FIN         USUARIO,
		WN_FECHA           FECHA,
		WN_NOTAS           MEMO,
		WN_MENSAJE         DESCRIPCION,
		WN_LEIDO           BOOLEANO,
    WN_VECES           FOLIOCHICO,
    WY_FOLIO           FOLIOGRANDE
	)
GO

ALTER TABLE WMENSAJE
		  ADD CONSTRAINT PK_WMENSAJE PRIMARY KEY( WN_GUID )
GO

CREATE INDEX XIE1WMENSAJE ON WMENSAJE
	(
		  WN_FECHA
	)
GO

CREATE FUNCTION DBO.PASOS_MODELO( @WM_CODIGO CODIGO ) RETURNS INTEGER
AS
BEGIN
     RETURN ( SELECT COUNT(*) FROM WMODSTEP WHERE ( WM_CODIGO = @WM_CODIGO ) )
END
GO

CREATE TABLE WMODELO (
		WM_CODIGO          CODIGO,
		WM_NOMBRE          OBSERVACIONES,
		WM_DESCRIP         MEMO,
		WM_TIPNOT1         STATUS,
		WM_USRNOT1         OBSERVACIONES,
		WM_TIPNOT2         STATUS,
		WM_USRNOT2         OBSERVACIONES,
		WM_TIPNOT3         STATUS,
		WM_USRNOT3         OBSERVACIONES,
		WM_URL             ACCESOS,
		WM_URL_DEL         ACCESOS,
		WM_URL_EXA         ACCESOS,
		WM_URL_OK          ACCESOS,
		WM_STATUS          STATUS,
		WM_PASOS           AS ( DBO.PASOS_MODELO( WM_CODIGO ) ),
		WM_XSL_ACT         NOMBREARCHIVO,
		WM_XSL_CAN         NOMBREARCHIVO,
		WM_XSL_TER         NOMBREARCHIVO,
		WM_FORMATO         NOMBREARCHIVO
	)
GO

ALTER TABLE WMODELO
		  ADD CONSTRAINT PK_WMODELO PRIMARY KEY( WM_CODIGO )
GO

CREATE TABLE WMODSTEP(
		WM_CODIGO          CODIGO,
		WE_ORDEN           FOLIOCHICO,
		WA_CODIGO          CODIGO,
		WE_NOMBRE          OBSERVACIONES,
		WE_DESCRIP         MEMO,
		WE_TIPDEST         STATUS,
		WE_USRDEST         OBSERVACIONES,
		WE_TIPNOT1         STATUS,
		WE_USRNOT1         OBSERVACIONES,
		WE_TIPNOT2         STATUS,
		WE_USRNOT2         OBSERVACIONES,
		WE_TIPNOT3         STATUS,
		WE_USRNOT3         OBSERVACIONES,
		WE_XSL_ACT         NOMBREARCHIVO,
		WE_XSL_CAN         NOMBREARCHIVO,
		WE_XSL_TER         NOMBREARCHIVO,
		WE_XSL_TAR         NOMBREARCHIVO,
    WE_LIMITE          FOLIOGRANDE,
    WE_URL_OK          ACCESOS,
    WX_FOLIO           FOLIOGRANDE
	)
GO

ALTER TABLE WMODSTEP
		  ADD CONSTRAINT PK_WMODSTEP PRIMARY KEY( WM_CODIGO, WE_ORDEN )
GO

ALTER TABLE WMODSTEP
		   ADD CONSTRAINT FK_WMODSTEP_WMODELO
		   FOREIGN KEY (WM_CODIGO)
		   REFERENCES WMODELO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

ALTER TABLE WMODSTEP
		   ADD CONSTRAINT FK_WMODSTEP_WACCION
		   FOREIGN KEY (WA_CODIGO)
		   REFERENCES WACCION
		   ON DELETE NO ACTION
		   ON UPDATE CASCADE;
GO

CREATE TABLE WOUTBOX(
		WO_GUID            GUID,
		WT_GUID            NULLGUID,
		WP_FOLIO           FOLIOGRANDE,
		WO_TO              MEMO,
		WO_CC              MEMO,
		WO_SUBJECT         ACCESOS,
		WO_BODY            MEMO,
		WO_FROM_NA         ACCESOS,
		WO_FROM_AD         ACCESOS,
		WO_SUBTYPE         STATUS,
		WO_STATUS          STATUS,
		WO_ENVIADO         BOOLEANO,
		WO_FEC_IN          FECHA,
		WO_FEC_OUT         FECHA,
		WO_TSTATUS         STATUS,
		WO_TAVANCE         FOLIOCHICO,
		US_ENVIA           USUARIO,
		US_RECIBE          USUARIO,
		WO_LOG             FORMULA
	)
GO

ALTER TABLE WOUTBOX
		  ADD CONSTRAINT PK_WOUTBOX PRIMARY KEY( WO_GUID )
GO

CREATE TABLE WPROSTEP(
		WP_FOLIO           FOLIOGRANDE,
		WE_ORDEN           FOLIOCHICO,
		WA_CODIGO          CODIGO,
		WM_CODIGO          CODIGO,
		WS_NOMBRE          OBSERVACIONES,
		WS_STATUS          STATUS,
		WS_USR_INI         USUARIO,
		WS_FEC_INI         FECHA,
		WS_FEC_FIN         FECHA,
		WS_NEXT            FOLIOCHICO
	)
GO

ALTER TABLE WPROSTEP
		  ADD CONSTRAINT PK_WPROSTEP PRIMARY KEY( WP_FOLIO, WE_ORDEN )
GO

ALTER TABLE WPROSTEP
		   ADD CONSTRAINT FK_WPROSTEP_WPROCESO
		   FOREIGN KEY (WP_FOLIO)
		   REFERENCES WPROCESO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

CREATE TABLE WTAREA(
		WT_GUID            GUID,
		WP_FOLIO           FOLIOGRANDE,
		WE_ORDEN           FOLIOCHICO,
		WT_DESCRIP         OBSERVACIONES,
		WT_STATUS          STATUS,
		WT_FEC_INI         FECHA,
		WT_FEC_FIN         FECHA,
		WT_USR_ORI         USUARIO,
		WT_USR_DES         USUARIO,
		WT_NOTAS           NOTAS,
		WT_AVANCE          FOLIOCHICO
	)
GO

ALTER TABLE WTAREA
		  ADD CONSTRAINT PK_WTAREA PRIMARY KEY( WT_GUID )
GO

ALTER TABLE WTAREA
		   ADD CONSTRAINT FK_WTAREA_WPROSTEP
		   FOREIGN KEY (WP_FOLIO, WE_ORDEN)
		   REFERENCES WPROSTEP
		   ON DELETE NO ACTION
		   ON UPDATE CASCADE;
GO

CREATE TABLE WMOD_ROL(
		   WM_CODIGO           CODIGO,
		   RO_CODIGO           CODIGO
	)
GO

ALTER TABLE WMOD_ROL
		  ADD CONSTRAINT PK_WMOD_ROL PRIMARY KEY( WM_CODIGO, RO_CODIGO )
GO

ALTER TABLE WMOD_ROL
		   ADD CONSTRAINT FK_WMOD_ROL_MODELO
		   FOREIGN KEY (WM_CODIGO)
		   REFERENCES WMODELO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

ALTER TABLE WMOD_ROL
		   ADD CONSTRAINT FK_WMOD_ROL_ROL
		   FOREIGN KEY (RO_CODIGO)
		   REFERENCES ROL
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

CREATE VIEW V_JEFES( US_CODIGO, US_NOMBRE )
AS
SELECT U.US_JEFE,
       ( SELECT U2.US_NOMBRE FROM USUARIO U2 WHERE ( U2.US_CODIGO = U.US_JEFE ) ) AS US_NOMBRE
FROM USUARIO U WHERE ( U.US_JEFE IS NOT NULL ) AND ( U.US_JEFE <> 0 )
GO

CREATE VIEW V_PADRE( GR_CODIGO, GR_DESCRIP )
AS
SELECT G.GR_PADRE,
       ( SELECT G2.GR_DESCRIP FROM GRUPO G2 WHERE ( G2.GR_CODIGO = G.GR_PADRE ) ) AS GR_DESCRIP
FROM GRUPO G WHERE ( G.GR_PADRE IS NOT NULL ) AND ( G.GR_PADRE <> 0 )
GO

CREATE FUNCTION DBO.GETNOMBREUSUARIO( @USUARIO USUARIO ) RETURNS DESCRIPCION
AS
BEGIN
  DECLARE @NOMBRE DESCRIPCION;
  SELECT @NOMBRE = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIO );
  RETURN @NOMBRE;
END
GO

CREATE VIEW V_PASOS( WP_FOLIO,
                     WM_CODIGO,
                     WP_NOMBRE,
                     WP_FEC_INI,
                     WP_FEC_FIN,
                     WP_STATUS,
                     WP_PASO,
                     WP_PASOS,
                     WP_USR_INI,
                     WP_NOM_INI,
                     WP_NOTAS,
                     WE_ORDEN,
                     WS_NOMBRE,
                     WS_STATUS,
                     WS_USR_INI,
                     WS_NOM_INI,
                     WS_FEC_INI,
                     WS_FEC_FIN,
                     WT_GUID,
                     WT_DESCRIP,
                     WT_STATUS,
                     WT_FEC_INI,
                     WT_FEC_FIN,
                     WT_USR_ORI,
                     WT_NOM_ORI,
                     WT_USR_DES,
                     WT_NOM_DES,
                     WT_NOTAS,
                     WT_AVANCE,
                     WM_DESCRIP,
                     WE_DESCRIP )
AS
SELECT WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP
FROM WPROSTEP
JOIN WPROCESO ON ( WPROSTEP.WP_FOLIO = WPROCESO.WP_FOLIO )
JOIN WMODELO ON ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
JOIN WMODSTEP ON ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) AND ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
LEFT OUTER JOIN WTAREA ON ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) AND ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
GO

CREATE VIEW V_PROCESO( WP_FOLIO,
                       WM_CODIGO,
                       WP_NOMBRE,
                       WP_FEC_INI,
                       WP_FEC_FIN,
                       WP_STATUS,
                       WP_PASO,
                       WP_PASOS,
                       WP_USR_INI,
                       WP_NOM_INI,
                       WP_NOTAS,
					             WP_XMLDATA,
                       WE_ORDEN,
                       WS_NOMBRE,
                       WS_STATUS,
                       WS_USR_INI,
                       WS_NOM_INI,
                       WS_FEC_INI,
                       WS_FEC_FIN,
                       WT_GUID,
                       WT_DESCRIP,
                       WT_STATUS,
                       WT_FEC_INI,
                       WT_FEC_FIN,
                       WT_USR_ORI,
                       WT_NOM_ORI,
                       WT_USR_DES,
                       WT_NOM_DES,
                       WT_NOTAS,
                       WT_AVANCE,
                       WM_DESCRIP,
                       WE_DESCRIP,
                       WM_URL_EXA,
                       WM_URL_DEL,
					             WP_MOV3,
					             WP_EMPRESA,
					             WP_XMLADD, WP_XMLDOC  )
AS
SELECT WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,
       WPROCESO.WP_XMLDATA,
       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,
       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,
       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP,
       WMODELO.WM_URL_EXA,
       WMODELO.WM_URL_DEL,
	     WPROCESO.WP_MOV3,
	     WPROCESO.WP_EMPRESA,
	     WPROCESO.WP_XMLADD,
		 WPROCESO.WP_XMLDOC
FROM WPROCESO
JOIN WMODELO ON ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
LEFT OUTER JOIN WPROSTEP ON ( WPROCESO.WP_FOLIO = WPROSTEP.WP_FOLIO ) AND ( WPROCESO.WP_PASO = WPROSTEP.WE_ORDEN )
LEFT OUTER JOIN WTAREA ON ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) AND ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
LEFT OUTER JOIN WMODSTEP ON ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) AND ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
GO

CREATE VIEW V_TAREA( WP_FOLIO,
                     WM_CODIGO,
                     WP_NOMBRE,
                     WP_FEC_INI,
                     WP_FEC_FIN,
                     WP_STATUS,
                     WP_PASO,
                     WP_PASOS,
                     WP_USR_INI,
                     WP_NOM_INI,
                     WP_NOTAS,
                     WE_ORDEN,
                     WS_NOMBRE,
                     WS_STATUS,
                     WS_USR_INI,
                     WS_NOM_INI,
                     WS_FEC_INI,
                     WS_FEC_FIN,
                     WT_GUID,
                     WT_DESCRIP,
                     WT_STATUS,
                     WT_FEC_INI,
                     WT_FEC_FIN,
                     WT_USR_ORI,
                     WT_NOM_ORI,
                     WT_USR_DES,
                     WT_NOM_DES,
                     WT_NOTAS,
                     WT_AVANCE,
                     WM_DESCRIP,
                     WE_DESCRIP,
                     WM_URL,
                     WM_URL_EXA,
                     WM_URL_OK,
                     WM_URL_DEL )
AS
SELECT WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP,
       WMODELO.WM_URL,
       WMODELO.WM_URL_EXA,
       WMODELO.WM_URL_OK,
       WMODELO.WM_URL_DEL

FROM WTAREA
JOIN WPROSTEP ON ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) AND ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
JOIN WPROCESO ON ( WTAREA.WP_FOLIO = WPROCESO.WP_FOLIO )
JOIN WMODELO ON ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
JOIN WMODSTEP ON ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) AND ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
GO

CREATE VIEW V_MODELOS( WM_CODIGO, RO_CODIGO, US_CODIGO ) AS
SELECT WMOD_ROL.WM_CODIGO, WMOD_ROL.RO_CODIGO, USER_ROL.US_CODIGO
FROM WMOD_ROL
JOIN USER_ROL ON ( USER_ROL.RO_CODIGO = WMOD_ROL.RO_CODIGO )
GO

CREATE TABLE WMODNOTI (
		   WM_CODIGO            CODIGO,
		   WE_ORDEN             FOLIOCHICO,
		   WY_FOLIO             FOLIOGRANDE IDENTITY,
		   WY_QUIEN             STATUS,
		   WY_USR_NOT           OBSERVACIONES,
		   WY_XSL               NOMBREARCHIVO,
		   WY_CUANDO            STATUS,
		   WY_TIEMPO            FOLIOGRANDE,
		   WY_QUE               STATUS,
		   WY_MODELO            CODIGO,
		   WY_TEXTO             OBSERVACIONES,
		   WY_VECES             FOLIOGRANDE,
		   WY_CADA              FOLIOGRANDE
	)
GO

ALTER TABLE WMODNOTI
		  ADD CONSTRAINT PK_WMODNOTI PRIMARY KEY( WM_CODIGO, WE_ORDEN, WY_FOLIO )
GO

ALTER TABLE WMODNOTI
		   ADD CONSTRAINT FK_WMODNOTI_WMODSTEP
		   FOREIGN KEY (WM_CODIGO,WE_ORDEN)
		   REFERENCES WMODSTEP
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

CREATE TABLE WCONEXION
	(
		WX_FOLIO FOLIOGRANDE IDENTITY (1, 1) NOT NULL,
		WX_NOMBRE DESCRIPCION NOT NULL,
		WX_URL ACCESOS NOT NULL,
		WX_TIP_AUT STATUS NOT NULL,
		WX_USUARIO USUARIOCORTO,
		WX_PASSWRD PASSWD,
		WX_TIP_ENC STATUS NOT NULL
	)
GO

CREATE VIEW V_PROCESO_EMP
AS
SELECT
  WPROCESO.WP_FOLIO,
  WPROCESO.WM_CODIGO,
  WPROCESO.WP_NOMBRE,
  WPROCESO.WP_FEC_INI,
  WPROCESO.WP_FEC_FIN,
  WPROCESO.WP_STATUS,
  WPROCESO.WP_PASO,
  WPROCESO.WP_PASOS,
  WPROCESO.WP_USR_INI,
  DBO.GETNOMBREUSUARIO(WPROCESO.WP_USR_INI) AS WP_NOM_INI,
  WPROCESO.WP_NOTAS,
  WPROCESO.CB_CODIGO,
  WPROCESO.CM_CODIGO,
  WPROSTEP.WE_ORDEN,
  WPROSTEP.WS_NOMBRE,
  WPROSTEP.WS_STATUS,
  WPROSTEP.WS_USR_INI,
  DBO.GETNOMBREUSUARIO(WPROSTEP.WS_USR_INI) AS WS_NOM_INI,
  WPROSTEP.WS_FEC_INI,
  WPROSTEP.WS_FEC_FIN,
  WTAREA.WT_GUID,
  WTAREA.WT_DESCRIP,
  WTAREA.WT_STATUS,
  WTAREA.WT_FEC_INI,
  WTAREA.WT_FEC_FIN,
  WTAREA.WT_USR_ORI,
  DBO.GETNOMBREUSUARIO(WTAREA.WT_USR_ORI) AS WT_NOM_ORI,
  WTAREA.WT_USR_DES,
  DBO.GETNOMBREUSUARIO(WTAREA.WT_USR_DES) AS WT_NOM_DES,
  WTAREA.WT_NOTAS,
  WTAREA.WT_AVANCE,
  WMODELO.WM_DESCRIP,
  WMODSTEP.WE_DESCRIP,
  WMODELO.WM_URL_EXA,
  WMODELO.WM_URL_DEL AS WM_URL_DEL
  FROM WPROCESO
  INNER JOIN WMODELO
   ON  WPROCESO.WM_CODIGO  = WMODELO.WM_CODIGO
  LEFT OUTER JOIN WPROSTEP
   ON  WPROCESO.WP_FOLIO  = WPROSTEP.WP_FOLIO
   AND WPROCESO.WP_PASO  = WPROSTEP.WE_ORDEN
  LEFT OUTER JOIN WTAREA
   ON  WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO
   AND WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN
  LEFT OUTER JOIN WMODSTEP
   ON WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO
   AND WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN
GO

CREATE TABLE Z_BITPASOS
	(
		ID FOLIOGRANDE IDENTITY,
		CM_CODIGO CODIGOEMPRESA,
		TP_SOLICIT CODIGO,
		NO_PASO FOLIOCHICO,
		DESCRIPCIO NOMBREARCHIVO,
		COMENTARIO TEXTOBITACORA,
		MSG_ERROR TEXTOBITACORA
	);
GO

ALTER TABLE Z_BITPASOS
		ADD CONSTRAINT PK_Z_BITPASOS PRIMARY KEY ( ID );
GO

CREATE TABLE WVALUSO
	(
		MODULO  NOMBRECAMPO,
		USO		DESCRIPCION
	)
GO

ALTER TABLE WVALUSO
		ADD CONSTRAINT WVALUSOPK PRIMARY KEY ( MODULO )
GO

CREATE TABLE SUSCRIP (
       RE_CODIGO            FolioChico,
       US_CODIGO            Usuario,
       SU_FRECUEN           Status,
       SU_ENVIAR            Status,
       SU_VACIO             Status
)
GO

ALTER TABLE SUSCRIP
       ADD CONSTRAINT PK_SUSCRIP PRIMARY KEY (RE_CODIGO, US_CODIGO)
GO

ALTER TABLE SUSCRIP
       ADD CONSTRAINT FK_Suscrip_Reporte
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TABLE SP_PATCH ( 
	SE_TIPO STATUS,  		
	SE_SEQ_NUM STATUS,  	
	SE_APLICA CODIGOEMPRESA,
	SE_NOMBRE DESCRIPCION, 	
	SE_DESCRIP DESCLARGA,
	SE_DATA MEMO, 		
	SE_ARCHIVO RUTA_URL	
)
GO

ALTER TABLE SP_PATCH 
ADD CONSTRAINT PK_SP_PATCH PRIMARY KEY (SE_TIPO, SE_SEQ_NUM, SE_APLICA)

GO 

create table GRUPOTERM(
	GP_CODIGO Codigo, 
	GP_DESCRIP Descripcion,
    GP_NUMERO Pesos,
    GP_TEXTO DescLarga,
    GP_ACTIVO Booleano,
	CM_CODIGO CodigoEmpresa
	)
GO

alter table GRUPOTERM 
add constraint PK_GRUPOTERM primary key ( GP_CODIGO )

GO

create table EMP_BIO(
		ID_NUMERO NumeroEmpleado,
		CM_CODIGO CodigoEmpresa,
		CB_CODIGO NumeroEmpleado,
    GP_CODIGO Codigo,
    IV_CODIGO NumeroEmpleado,
	CB_ACTIVO booleano,
	CB_FEC_BAJ fecha, 
	ES_BAJA_FT Booleano
)
GO

alter table EMP_BIO
	add constraint PK_EMP_BIO primary key ( ID_NUMERO )
GO

alter table EMP_BIO
     add  constraint FK_EMP_BIO_COMPANY foreign key(CM_CODIGO)
     references COMPANY (CM_CODIGO)
     on update cascade
     on delete cascade
GO

alter table EMP_BIO
     add  constraint FK_EMP_BIO_GRUPOTERM foreign key(GP_CODIGO)
     references GRUPOTERM (GP_CODIGO)
     on update cascade
     on delete cascade
GO

CREATE NONCLUSTERED INDEX IDX_EMP_BIO_EMPLEADO     
		ON EMP_BIO (CM_CODIGO, CB_CODIGO)

GO 

CREATE TABLE WS_BITAC(
	     LLAVE int IDENTITY(1,1),
	     WS_FECHA Fecha,
	     CH_RELOJ RelojLinx,
	     WS_CHECADA Descripcion,
	     CB_CODIGO NumeroEmpleado,
	     WS_MENSAJE TextoLargo,
	     CM_CODIGO CodigoEmpresa,
	     WS_TIPO FolioChico,
       WS_MIENT NombreImpresora
)
GO

alter table WS_BITAC
     ADD CONSTRAINT PK_WS_BITAC PRIMARY KEY ( LLAVE )
GO

CREATE TABLE WS_ACCESS(
	     LLAVE int IDENTITY(1,1),
	     WA_FECHA Fecha NOT NULL,
	     CM_CODIGO CodigoEmpresa NOT NULL,
	     CB_CODIGO NumeroEmpleado NOT NULL,
	     CH_RELOJ RelojLinx NOT NULL,
	     WS_CHECADA Descripcion NOT NULL
)
GO

alter table WS_ACCESS
     add constraint PK_WS_ACCESS primary KEY ( LLAVE )
GO

create table KIOS_IMPR
(
	CB_CODIGO NumeroEmpleado,
	CM_CODIGO CodigoEmpresa,
	PE_TIPO Status,
	PE_YEAR Status,
	PE_NUMERO Status,
	KI_FECHA Fecha,
	KI_KIOSCO Titulo,
	KI_HORA HoraMinutosSegundos,
	RE_CODIGO FolioChico
)
GO

create index XIE1KIOS_IMPR on KIOS_IMPR
(
       CB_CODIGO,
       CM_CODIGO,
       PE_TIPO,
       PE_YEAR,
       PE_NUMERO
)
GO

create table KENCUESTAS
(
	ENC_CODIGO FolioGrande,
	ENC_NOMBRE Titulo,
	ENC_PREGUN Formula,
	ENC_CONFID Booleano,
	ENC_VOTOSE Booleano,
	ENC_STATUS Status,
	US_CODIGO  Usuario,
	ENC_FECINI Fecha,
	ENC_FECFIN Fecha,
	ENC_HORINI Hora,
	ENC_HORFIN Hora,
	ENC_MRESUL Booleano,
	ENC_FILTRO Formula,
	ENC_COMPAN Formula,
	ENC_KIOSCS Formula,
	ENC_OPCION Booleano,
	ENC_MSJCON Formula
)
GO

ALTER TABLE KENCUESTAS
       ADD CONSTRAINT PK_ENCUESTAS PRIMARY KEY (ENC_CODIGO)
GO

create table KOPCIONES
(
	OP_ORDEN   Status,
	OP_TITULO  Descripcion,
	OP_DESCRIP Formula,
	ENC_CODIGO FolioGrande,
	US_CODIGO  Usuario
)
GO

ALTER TABLE KOPCIONES
       ADD CONSTRAINT PK_OPCIONES PRIMARY KEY ( OP_ORDEN,ENC_CODIGO )
GO

ALTER TABLE KOPCIONES
       ADD CONSTRAINT FK_Opciones_Encuesta
       FOREIGN KEY (ENC_CODIGO)
       REFERENCES KENCUESTAS
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

create table KVOTOS
(
	ENC_CODIGO FolioGrande,
	CB_CODIGO  FolioGrande,
	CM_CODIGO  CodigoEmpresa,
  CB_CREDENC Codigo1,
	VO_VALOR   Status,
	VO_FECHA   Fecha,
	VO_HORA	   Hora,
	VO_KIOSCO  ComputerName
)
GO

ALTER TABLE KVOTOS
       ADD CONSTRAINT PK_VOTOS PRIMARY KEY ( ENC_CODIGO,CB_CODIGO,CM_CODIGO,CB_CREDENC )
GO

ALTER TABLE KVOTOS
       ADD CONSTRAINT FK_Votos_Encuesta
       FOREIGN KEY (ENC_CODIGO)
       REFERENCES KENCUESTAS
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

create view WVMENSAJEM as
	select
		WN_GUID, WN_FECHA
	from
		WMENSAJE
	where
		WN_FECHA > ( GETDATE() - 30) and ( WN_LEIDO = 'N' )
GO

CREATE TABLE VT_WPAUTO3 (
	WP_FOLIO FolioGrande NOT NULL,
	CM_CODIGO Descripcion NULL,
	WP_MODO varchar(5),
	MV_DESCR Formula,
  MV_OBSERVA Formula,
	CB_CODIGO int,
	AT_FEC_AUT Fecha,
	AT_TIPO Status,
	AT_HORAS numeric(15, 2),
	AT_MOTIVO Codigo
)
GO

create index IX_VT_WPAUTO3_WP_FOLIO on VT_WPAUTO3(WP_FOLIO)
GO

create index IX_VT_WPAUTO3_CM_CODIGO on VT_WPAUTO3(CM_CODIGO)
GO

create index IX_VT_WPAUTO3_AT_FEC_AUT on VT_WPAUTO3(AT_FEC_AUT)
GO

CREATE  VIEW WPROCMOV3
AS
SELECT
	WP_FOLIO,
	WP_EMPRESA,
	WP_MOV3
FROM  dbo.WPROCESO
GO

CREATE VIEW V_WPAUTO3_PRE_PROCESS
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t.c.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t.c.value('FECHAAUTO[1]','datetime') AT_FEC_AUT,
t.c.value('TIPOAUTO[1]','Status') AT_TIPO,
t.c.value('HORAS[1]','Pesos') AT_HORAS,
t.c.value('MOTIVO[1]','Codigo') AT_MOTIVO
from
dbo.WPROCESO WP join dbo.WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'AUTO3' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t2.c2.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t.c.value('FECHAAUTO[1]','datetime') AT_FEC_AUT,
t.c.value('TIPOAUTO[1]','Status') AT_TIPO,
t.c.value('HORAS[1]','Pesos') AT_HORAS,
t.c.value('MOTIVO[1]','Codigo') AT_MOTIVO
from
dbo.WPROCESO WP join dbo.WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'AUTO3' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
UNION ALL
(
select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA
,t2.c2.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t2.c2.value('FECHAAUTO[1]','datetime') AT_FEC_AUT,
t2.c2.value('TIPOAUTO[1]','Status') AT_TIPO,
t2.c2.value('HORAS[1]','Pesos') AT_HORAS,
t2.c2.value('MOTIVO[1]','Codigo') AT_MOTIVO
from
dbo.WPROCESO WP join dbo.WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'AUTO3MULT'
)
GO

CREATE VIEW V_WPCAMBIO
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
t.c.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t.c.value('FECHAREG[1]','datetime') MV_FEC_REG,
t.c.value('FECHAIMSS[1]','datetime') MV_FEC_IMS,
t.c.value('CB_SALARIO[1]','Pesos') CB_SALARIO,
t.c.value('SALARIO_NEW[1]','Pesos') CA_SAL_NEW,
t.c.value('PORCENTAJE_AUMENTO[1]','Pesos') CA_POR_AUM,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'CAMBIO' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
t2.c2.value('EMPLEADO[1]', 'INT' ) CB_CODIGO,
t.c.value('FECHAREG[1]','datetime') MV_FEC_REG,
t.c.value('FECHAIMSS[1]','datetime') MV_FEC_IMS,
t2.c2.value('CB_SALARIO[1]', 'Pesos' ) CB_SALARIO,
t2.c2.value('SALARIO_NEW[1]', 'Pesos' ) CA_SAL_NEW,
t.c.value('PORCENTAJE_AUMENTO[1]','Pesos') CA_POR_AUM,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'CAMBIO' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
GO

CREATE VIEW V_WPAUTO3
as
select VP.WP_FOLIO, VP.CM_CODIGO, MODO=VP.WP_MODO, MV.WP_MOV3, VP.MV_DESCR, VP.MV_OBSERVA, VP.CB_CODIGO, VP.AT_FEC_AUT, VP.AT_TIPO, VP.AT_HORAS, VP.AT_MOTIVO
from VT_WPAUTO3 VP
join dbo.WPROCMOV3 MV on MV.WP_FOLIO = VP.WP_FOLIO
GO

CREATE VIEW V_WPMULTIP
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
t.c.value('FECHAREG[1]', 'Fecha' ) MV_FEC_REG,
coalesce(t.c.value('DESCRIPCION[1]', 'Varchar(255)' ), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t.c.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
coalesce (t.c.value('CB_TURNO[1]', 'Codigo' ), '') as CB_TURNO,
coalesce (t.c.value('CB_NIVEL1[1]', 'Codigo' ), '') as CB_NIVEL1,
coalesce (t.c.value('CB_NIVEL2[1]', 'Codigo' ), '') as CB_NIVEL2,
coalesce (t.c.value('CB_NIVEL3[1]', 'Codigo' ), '') as CB_NIVEL3,
coalesce (t.c.value('CB_NIVEL4[1]', 'Codigo' ), '') as CB_NIVEL4,
coalesce (t.c.value('CB_NIVEL5[1]', 'Codigo' ), '') as CB_NIVEL5,
coalesce (t.c.value('CB_NIVEL6[1]', 'Codigo' ), '') as CB_NIVEL6,
coalesce (t.c.value('CB_NIVEL7[1]', 'Codigo' ), '') as CB_NIVEL7,
coalesce (t.c.value('CB_NIVEL8[1]', 'Codigo' ), '') as CB_NIVEL8,
coalesce (t.c.value('CB_NIVEL9[1]', 'Codigo' ), '') as CB_NIVEL9
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'MULTIP' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
t.c.value('FECHAREG[1]', 'Fecha' ) MV_FEC_REG,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t2.c2.value('EMPLEADO[1]', 'NumeroEmpleado' ) as CB_CODIGO,
coalesce (t.c.value('CB_TURNO[1]', 'Codigo' ), '') as CB_TURNO,
coalesce (t.c.value('CB_NIVEL1[1]', 'Codigo' ), '') as CB_NIVEL1,
coalesce (t.c.value('CB_NIVEL2[1]', 'Codigo' ), '') as CB_NIVEL2,
coalesce (t.c.value('CB_NIVEL3[1]', 'Codigo' ), '') as CB_NIVEL3,
coalesce (t.c.value('CB_NIVEL4[1]', 'Codigo' ), '') as CB_NIVEL4,
coalesce (t.c.value('CB_NIVEL5[1]', 'Codigo' ), '') as CB_NIVEL5,
coalesce (t.c.value('CB_NIVEL6[1]', 'Codigo' ), '') as CB_NIVEL6,
coalesce (t.c.value('CB_NIVEL7[1]', 'Codigo' ), '') as CB_NIVEL7,
coalesce (t.c.value('CB_NIVEL8[1]', 'Codigo' ), '') as CB_NIVEL8,
coalesce (t.c.value('CB_NIVEL9[1]', 'Codigo' ), '') as CB_NIVEL9
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'MULTIP' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
GO

CREATE VIEW V_WPPERM
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t.c.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('INICIO[1]','Fecha') PM_FEC_INI,
t.c.value('RETORNO[1]','Fecha') PM_FEC_RET,
t.c.value('DIAS[1]','Status') PM_DIAS,
t.c.value('CLASE[1]','Status') PM_CLASE,
t.c.value('TIPO[1]','Codigo') PM_TIPO,
t.c.value('REFERENCIA[1]','Descripcion') PM_REFEREN
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'PERM' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t2.c2.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('INICIO[1]','Fecha') PM_FEC_INI,
t.c.value('RETORNO[1]','Fecha') PM_FEC_RET,
t.c.value('DIAS[1]','Status') PM_DIAS,
t.c.value('CLASE[1]','Status') PM_CLASE,
t.c.value('TIPO[1]','Codigo') PM_TIPO,
t.c.value('REFERENCIA[1]','Descripcion') PM_REFEREN
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'PERM' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
GO

CREATE VIEW V_WPVACA
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
t.c.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('VA_FEC_INI[1]','Fecha') VA_FEC_INI, 
t.c.value('VA_FEC_FIN[1]','Fecha') VA_FEC_FIN, 
t.c.value('VA_SALD_GOZO[1]','Pesos') VA_SAL_GOZ,
t.c.value('VA_SALD_PAGO[1]','Pesos') VA_SAL_PAG,
t.c.value('VA_GOZO[1]','Pesos') VA_GOZO,
t.c.value('VA_PAGO[1]','Pesos') VA_PAGO,
t.c.value('VA_P_PRIMA[1]','Pesos') VA_P_PRIMA,
t.c.value('VA_PERIODO[1]','Descripcion') VA_PERIODO,
t.c.value('VA_COMENTA[1]','Descripcion') VA_COMENTA,
t.c.value('VA_NOMYEAR[1]','Int') VA_NOMYEAR,
t.c.value('VA_NOMTIPO[1]','Descripcion') VA_NOMTIPO,
t.c.value('VA_NOMNUME[1]','Int') VA_NOMNUME
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO  
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'VACA' and t.c.value('EMPLEADO[1]', 'INT' ) > 0  
)
UNION ALL 
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
t2.c2.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('VA_FEC_INI[1]','Fecha') VA_FEC_INI,
t.c.value('VA_FEC_FIN[1]','Fecha') VA_FEC_FIN, 
VA_SAL_GOZ=0,
VA_SAL_PAG=0,
t.c.value('VA_GOZO[1]','Pesos') VA_GOZO,
t.c.value('VA_PAGO[1]','Pesos') VA_PAGO,
t2.c2.value('VA_P_PRIMA[1]','Pesos') VA_P_PRIMA,
t.c.value('VA_PERIODO[1]','Descripcion') VA_PERIODO,
t.c.value('VA_COMENTA[1]','Descripcion') VA_COMENTA,
t.c.value('VA_NOMYEAR[1]','Int') VA_NOMYEAR,
t.c.value('VA_NOMTIPO[1]','Descripcion') VA_NOMTIPO,
t.c.value('VA_NOMNUME[1]','Int') VA_NOMNUME
 from 
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO  
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2) 
where MV.WP_MOV3 = 'VACA' and 
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' ) 
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null ) 
)
GO

create table HUELLAGTI (
   HU_ID int IDENTITY,
   CM_CODIGO CodigoEmpresa,
   CB_CODIGO NumeroEmpleado,
   HU_INDICE USUARIO,
   HU_HUELLA Memo,
   IV_CODIGO NumeroEmpleado,
   HU_TIMEST FECHA
)
GO

alter table HUELLAGTI
add constraint PK_HUELLAGTI primary key CLUSTERED (CM_CODIGO ASC, CB_CODIGO ASC, HU_INDICE ASC, IV_CODIGO ASC);
GO

create table MENSAJE (
   DM_CODIGO  FolioChico,
   DM_MENSAJE Formula,
   DM_ORDEN   FolioChico,
   DM_SLEEP   FolioGrande,
   DM_ACTIVO  Booleano
)
GO

alter table MENSAJE add constraint PK_MENSAJE primary key (DM_CODIGO ASC);
GO

CREATE TABLE TERM_GPO (
   TE_CODIGO NumeroEmpleado,
   GP_CODIGO Codigo,
)
GO

alter table TERM_GPO add constraint PK_TERM_GPO primary key (TE_CODIGO ASC, GP_CODIGO ASC);
GO

create table TERM_HUELLA (
   CM_CODIGO CodigoEmpresa,
   CB_CODIGO NumeroEmpleado,
   HU_INDICE USUARIO,
   TE_CODIGO NumeroEmpleado,
   IV_CODIGO NumeroEmpleado,
   BORRAR Booleano,
   TerminalError NumeroEmpleado,
   LLAVE INT IDENTITY
)
GO

ALTER TABLE TERM_HUELLA
ADD CONSTRAINT PK_TERM_HUELLA PRIMARY KEY CLUSTERED (CM_CODIGO ASC, CB_CODIGO ASC, HU_INDICE ASC, IV_CODIGO ASC, TE_CODIGO ASC)
GO

create table TERM_MSG (
   DM_CODIGO FolioChico,
   TE_CODIGO NumeroEmpleado,
   TM_SLEEP   FolioGrande,
   TM_NEXT   Fecha,
   TM_ULTIMA Fecha,
   TM_NEXTXHR booleano,
   TM_NEXTHOR Hora,
   TM_NEXT_RL Fecha
)
GO

alter table TERM_MSG add constraint PK_TERM_MSG primary key (DM_CODIGO ASC, TE_CODIGO ASC);
GO

create table TERMINAL (
   TE_CODIGO NumeroEmpleado identity,
   TE_NOMBRE Descripcion,
   TE_NUMERO Pesos,
   TE_TEXTO  DescLarga,
   TE_MAC    MACADDRESS,
   TE_APP    Status,
   TE_BEAT   Fecha,
   TE_ZONA   DescLarga,
   TE_ACTIVO Booleano,
   TE_XML    Memo,
   TE_LOGO   Memo,
   TE_FONDO  Memo,
   TE_ZONA_ES DescLarga,
   TE_VERSION Descripcion,
   TE_IP Descripcion,
   TE_TER_HOR Fecha,
   TE_HUELLAS FolioGrande,
   TE_ID      FolioGrande
)
GO

alter table TERMINAL add constraint PK_TERMINAL primary key (TE_CODIGO ASC);
GO

CREATE TABLE COM_HUELLA (
CH_CODIGO Accesos,
HU_ID NumeroEmpleado)
GO

alter table COM_HUELLA
add constraint PK_COM_HUELLA primary key (CH_CODIGO, HU_ID)
GO

alter table HUELLAGTI add constraint FK_HUELLAGTI_COMPANY FOREIGN KEY (CM_CODIGO) references COMPANY (CM_CODIGO) on delete cascade on update cascade;
GO

alter table TERM_GPO add constraint FK_TERM_GPO_TERMINAL FOREIGN KEY (TE_CODIGO) references TERMINAL (TE_CODIGO) on delete cascade on update cascade;
GO

alter table TERM_GPO add constraint FK_TERM_GPO_GRUPOTERM FOREIGN KEY (GP_CODIGO) references GRUPOTERM (GP_CODIGO) on delete cascade on update cascade;
GO

alter table TERM_HUELLA add constraint FK_TERM_HUELLA_TERMINAL FOREIGN KEY (TE_CODIGO) references TERMINAL (TE_CODIGO) on delete cascade on update cascade;
GO

alter table TERM_MSG add constraint FK_TERM_MSG_TERMINAL FOREIGN KEY (TE_CODIGO) references TERMINAL (TE_CODIGO) on delete cascade on update cascade;
GO

alter table TERM_MSG add constraint FK_DISPO_MSG_MENSAJE FOREIGN KEY (DM_CODIGO) references MENSAJE (DM_CODIGO) on delete cascade on update cascade;
GO

create view V_HUELLAS
as
     SELECT    H.HU_ID, E.ID_NUMERO, E.CM_CODIGO, E.CB_CODIGO, H.HU_INDICE, convert(varchar(max), H.HU_HUELLA) as HU_HUELLA, H.IV_CODIGO, H.HU_TIMEST, 'HUELLAS' as TIPO
     FROM         HUELLAGTI AS H INNER JOIN
                           EMP_BIO AS E ON H.CM_CODIGO = E.CM_CODIGO AND H.CB_CODIGO = E.CB_CODIGO
     WHERE ( H.IV_CODIGO = 0 )
     union
     SELECT    H.HU_ID, E.ID_NUMERO, E.CM_CODIGO, E.CB_CODIGO, H.HU_INDICE, convert(varchar(max), H.HU_HUELLA) as HU_HUELLA, H.IV_CODIGO, H.HU_TIMEST, 'INVITADORES' as TIPO
     FROM         HUELLAGTI AS H INNER JOIN
                           EMP_BIO AS E ON H.CM_CODIGO = E.CM_CODIGO AND H.IV_CODIGO = E.IV_CODIGO
     WHERE ( H.IV_CODIGO <> 0 )
GO

CREATE VIEW V_INVITADORES
AS
     select convert(varchar(max),ID_NUMERO) as ID_NUMERO, IV_CODIGO, 1 as IV_TIPO from EMP_ID where ( IV_CODIGO > 0 )
     union
     select convert(varchar(max),ID_NUMERO), IV_CODIGO, 2 from EMP_BIO where ( IV_CODIGO > 0 )
GO

CREATE TABLE  DB_INFO (
       DB_CODIGO			CodigoEmpresa,
       DB_DESCRIP     Observaciones,
       DB_CONTROL			Referencia,
       DB_TIPO        Status,
       DB_DATOS       Accesos,
       DB_USRNAME     UsuarioCorto,
       DB_PASSWRD     PasswdLrg,
       DB_CTRL_RL     Referencia,
       DB_CHKSUM      Accesos,
       DB_ESPC        Booleano
)
GO

ALTER TABLE DB_INFO
	ADD CONSTRAINT PK_DB_INFO PRIMARY KEY (DB_CODIGO)
GO

ALTER TABLE DB_INFO
	ADD CONSTRAINT UK_DB_INFO UNIQUE NONCLUSTERED (DB_DATOS)
GO

ALTER TABLE COMPANY
ADD CONSTRAINT FK_DB_INFO_DB_CODIGO
FOREIGN KEY(DB_CODIGO)
REFERENCES DB_INFO(DB_CODIGO)
ON UPDATE CASCADE
GO

CREATE TABLE CuentaTim(
	CT_CODIGO Codigo,
	CT_DESCRIP Descripcion,
	CT_ID FolioGrande,
	CT_PASSWRD Formula,
	CT_ACTIVO Booleano,
	US_CODIGO Usuario,
	US_FEC_MOD Fecha,
 CONSTRAINT PK_CuentaTim  PRIMARY KEY
(
	CT_CODIGO
)
)
GO

CREATE TABLE TRESSFILES(
	TF_PATH varchar(600)  NOT NULL,
	TF_EXT dbo.Descripcion NOT NULL,
	TF_FILE dbo.Ruta_URL NOT NULL,
	TF_DESCR dbo.Formula ,
	TF_VERSION dbo.Descripcion ,
	TF_BUILD dbo.Descripcion ,
	TF_FEC_MOD Fecha,
	TF_CALLS integer ,
	TF_FEC_ACC Fecha,
	TF_HOST dbo.Formula ,
	TF_FEC_UPD Fecha,
	CONSTRAINT PK_TRESSFILES PRIMARY KEY CLUSTERED 
(
	TF_PATH ASC,
	TF_EXT ASC,
	TF_FILE ASC
) 
)

GO

CREATE TABLE CAF_CONFIG(
						CF_NOMBRE            RelojLinx     NOT NULL,
						DI_TIPO              FolioChico    NOT NULL,
						CF_TIP_COM           FolioChico    NOT NULL,
						CF_FOTO              Booleano      NOT NULL,
						CF_COM               Booleano      NOT NULL,
						CF_SIG_COM           Booleano      NOT NULL,
						CF_TECLADO           Booleano      NOT NULL,
						CF_DEF_COM           Texto         NULL,
						CF_DEF_CRE           Texto         NULL,
						CF_PRE_TIP           Booleano      NOT NULL,
						CF_PRE_QTY           Booleano      NOT NULL,
						CF_BANNER            Texto         NULL,
						CF_VELOCI            FolioChico    NULL,
						CF_T_ESP             FolioGrande   NULL,         
						CF_CALEND            Texto         NOT NULL,
						CF_GAFETE            Texto         NOT NULL,
						CF_CLAVE             Texto         NULL,
						CF_A_PRINT           Booleano      NOT NULL,
						CF_A_CANC            Booleano      NOT NULL,
						CF_TCOM_1            Texto         NULL,
						CF_TCOM_2            Texto         NULL,
						CF_TCOM_3            Texto         NULL,
						CF_TCOM_4            Texto         NULL,
						CF_TCOM_5            Texto         NULL,
						CF_TCOM_6            Texto         NULL,
						CF_TCOM_7            Texto         NULL,
						CF_TCOM_8            Texto         NULL,
						CF_TCOM_9            Texto         NULL,
						CF_REINICI           Booleano      NOT NULL,
						CF_CHECK_S           FolioGrande   NOT NULL,
						CF_SINC              FolioGrande   NOT NULL,
						CF_CAMBIOS           Booleano      NOT NULL
)

GO

ALTER TABLE CAF_CONFIG 
ADD CONSTRAINT PK_CAF_CONFIG 
PRIMARY KEY (CF_NOMBRE, DI_TIPO)

GO

ALTER TABLE CAF_CONFIG 
ADD CONSTRAINT FK_Caf_Config_Disposit
FOREIGN KEY (CF_NOMBRE, DI_TIPO) 
REFERENCES DISPOSIT(DI_NOMBRE, DI_TIPO) 
ON DELETE CASCADE
ON UPDATE CASCADE

GO

CREATE TABLE CAF_CALEND (
	CF_NOMBRE	RelojLinx		  NOT NULL,
	DI_TIPO	    FolioChico		  NOT NULL,
	CC_REG_ID	Int IDENTITY(1,1) NOT NULL,
	HORA		Hora			  NOT NULL,
	ACCION		FolioChico	      NULL,
	CONTADOR	Booleano		  NOT NULL,
	SEMANA		FolioChico	      NOT NULL,
	SYNC		Booleano		  NOT NULL
)

GO

ALTER TABLE CAF_CALEND 
ADD CONSTRAINT PK_CAF_CALEND 
PRIMARY KEY (CF_NOMBRE, DI_TIPO, CC_REG_ID)

GO

ALTER TABLE CAF_CALEND 
ADD CONSTRAINT FK_Caf_Calend_Caf_Config
FOREIGN KEY (CF_NOMBRE, DI_TIPO) 
REFERENCES CAF_CONFIG (CF_NOMBRE, DI_TIPO)
ON DELETE CASCADE
ON UPDATE CASCADE

GO


CREATE TABLE Tableros(
	TableroID FolioGrande IDENTITY(1,1) ,	
	TableroNombre Descripcion ,
	TableroDescrip Formula ,
	TableroActivo Booleano ,
 CONSTRAINT PK_Tableros PRIMARY KEY CLUSTERED 
(
	TableroID ASC
) ,
 CONSTRAINT AK_Tableros_Nombre UNIQUE NONCLUSTERED 
(	
	TableroNombre ASC
)
)
GO

CREATE TABLE Dashlets(
	DashletID FolioGrande IDENTITY(1,1) ,	
	DashletNombre Formula  ,
	DashletSubtitulo Formula  ,
	DashletQuery TextoLargo ,	
	DashletTipo   dbo.Status,
	DashletEstilo dbo.Status, 
	DashletFormato		Formula,  	
	DashletFormatoSub	Formula,
	DashletTendenciaDesc	Formula,	
	DashletIconos		dbo.Status,
 CONSTRAINT PK_Dashlets PRIMARY KEY CLUSTERED 
(
	DashletID ASC
), 
 CONSTRAINT AK_Dashlets_Nombre UNIQUE NONCLUSTERED 
(
	DashletNombre ASC
) 
) 
GO

CREATE TABLE TableroDashlets(
	TabDashID FolioGrande IDENTITY(1,1) ,
	TableroID FolioGrande ,
	DashletID FolioGrande ,
	TabDashRow Status ,
	TabDashCol Status ,
	TabDashWidth Status ,
	TabDashHeight Status ,
 CONSTRAINT PK_TableroDashlets PRIMARY KEY CLUSTERED 
(
	TabDashID ASC
) 
) 
GO

ALTER TABLE TableroDashlets  WITH CHECK ADD  CONSTRAINT FK_TableroDashlets_Dashlets FOREIGN KEY(DashletID)
	REFERENCES Dashlets (DashletID)
GO

ALTER TABLE TableroDashlets CHECK CONSTRAINT FK_TableroDashlets_Dashlets
GO

ALTER TABLE TableroDashlets  WITH CHECK ADD  CONSTRAINT FK_TableroDashlets_Tableros FOREIGN KEY(TableroID)
	REFERENCES dbo.Tableros (TableroID)
	ON DELETE CASCADE
GO

ALTER TABLE TableroDashlets CHECK CONSTRAINT FK_TableroDashlets_Tableros
GO

CREATE TABLE CompanyTableros 
(
	CompanyTableroID FolioGrande IDENTITY(1,1), 
	CM_CODIGO CodigoEmpresa, 
	TableroID FolioGrande, 
	CompanyTableroOrden status, 

 CONSTRAINT PK_CompanyTableros PRIMARY KEY 
(
	CompanyTableroID  
) , 
 CONSTRAINT AK_CompanyTableros UNIQUE NONCLUSTERED 
(
	CM_CODIGO, TableroID
) 
)
GO 

ALTER TABLE CompanyTableros  WITH CHECK ADD  CONSTRAINT FK_CompanyTableros_Tablero FOREIGN KEY(TableroID)
	REFERENCES Tableros (TableroID)
	ON DELETE CASCADE
GO 

ALTER TABLE CompanyTableros  WITH CHECK ADD  CONSTRAINT FK_CompanyTableros_Company FOREIGN KEY(CM_CODIGO)
	REFERENCES COMPANY (CM_CODIGO)
	ON DELETE CASCADE
	ON UPDATE CASCADE 
GO 

CREATE TABLE RolTableros 
(
	CompanyTableroID FolioGrande,
	RO_CODIGO Codigo	

CONSTRAINT PK_RolTableros PRIMARY KEY 
(
	CompanyTableroID, RO_CODIGO 
) 

)
GO 

ALTER TABLE RolTableros  WITH CHECK ADD  CONSTRAINT FK_RolTableros_CompanyTableros FOREIGN KEY(CompanyTableroID)
	REFERENCES CompanyTableros (CompanyTableroID)
	ON DELETE CASCADE
GO 

ALTER TABLE RolTableros  WITH CHECK ADD  CONSTRAINT FK_RolTableros_Rol FOREIGN KEY(RO_CODIGO)
	REFERENCES Rol (RO_CODIGO)
	ON DELETE CASCADE
	ON UPDATE CASCADE 
GO

CREATE TABLE Sesion ( 
	SesionID  FolioGrande IDENTITY(1,1) ,	
	US_CODIGO	Usuario,
	CM_CODIGO	CodigoEmpresa,
	CM_NIVEL0	Formula,  
	PE_TIPO		int, 
	PE_YEAR		int, 
	PE_NUMERO	int, 
	PE_MES		int, 
	FechaActiva	Fecha, 
	MesIni		Fecha, 
	MesFin		Fecha,

	CONSTRAINT PK_Sesion PRIMARY KEY CLUSTERED 
	(
	SesionID ASC
	)
) 

GO 

CREATE VIEW V_Dashlets
AS
SELECT	DashletID, DashletNombre, DashletSubtitulo, DashletTipo, DashletEstilo, DashletQuery,  
		case DashletTipo 
			when 1 then 'Listado' 
			when 2 then 'Gr�fica' 
			when 3 then 'Indicador' 
		 else '' 
		end  DashletTipoDesc,
		case DashletEstilo 
			when 1 then 'Columnas' 
			when 2 then 'Barras' 
			when 3 then 'Pastel' 
			else 
				'' 
		end   DashletEstiloDesc, 
		DashletFormato, DashletFormatoSub, DashletTendenciaDesc, 
		DashletIconos, 
		case DashletIconos  
			when 0 then 'Sin �conos' 
			when 1 then 'Enfoque positivo' 
			when 2 then 'Enfoque negativo' 			
			else 
				'' 
		end   DashletIconosDesc

FROM	Dashlets Dashlet
GO 

create table ENVIOEMAIL(
		EE_FOLIO FolioGrande identity,		
		EE_TO TextoLargo,					
		EE_CC EmailAdd,						
		EE_SUBJECT Accesos,					
		EE_BODY Memo,						
		EE_FROM_NA Accesos,					
		EE_FROM_AD EmailAdd,				
		EE_ERR_AD EmailAdd,					
		EE_SUBTYPE Status,					
		EE_STATUS Status,					
		EE_ENVIADO Booleano,				
		EE_MOD_ADJ Booleano,				
		EE_FEC_IN Fecha,					
		EE_FEC_OUT Fecha,					
		EE_ATTACH Memo,						
		EE_BORRAR Booleano,					
		EE_LOG Formula,						
		EE_ENCRIPT Booleano					
	)
go

alter table ENVIOEMAIL
add constraint PK_ENVIOEM primary key (EE_FOLIO)
go


create table CALENDARIO(
    CA_FOLIO FolioGrande IDENTITY,        
    CA_NOMBRE Titulo,                     
    CA_DESCRIP Texto,                     
    CA_ACTIVO Booleano,                   
    CM_CODIGO CodigoEmpresa,              
    CA_REPORT Texto,                      
    CA_REPEMP FolioChico,                 
    CA_FECHA Fecha,                      
    CA_HORA Hora,                         
    CA_FREC Status,                       
    CA_RECUR FolioChico,                 
                                         
    CA_DOWS Descripcion,                 
                                         
    CA_MESES Descripcion,                 
    CA_MESDIAS Titulo,                    
    CA_MES_ON Status,                     
	CA_MESWEEK Descripcion,				  
    CA_FEC_ESP Fecha,                     
    CA_TSALIDA Status,                    
    CA_FSALIDA NombreArchivo,             
                                          
    CA_ULT_FEC Fecha,                     
    CA_NX_FEC Fecha,                      
    CA_NX_EVA Booleano,                   

    CA_US_CHG Usuario,
	CA_CAPTURA Fecha                        
	  )    	  	 
GO

alter table CALENDARIO
add constraint PK_CALEN primary key (CA_FOLIO)
GO

create index XAK1CALENDARIO on CALENDARIO ( CA_NX_FEC )
GO

create index XAK2CALENDARIO on CALENDARIO ( CA_NOMBRE )
GO

create table ROLSUSCRIP (
    RO_CODIGO Codigo,                     
    CA_FOLIO FolioGrande,                 
    RS_ACTIVO Booleano,                   
    RS_VACIO Status,                      
    RS_US_CHG Usuario                     
    )
GO


alter table ROLSUSCRIP
add constraint PK_RSUSCRIP primary key (RO_CODIGO, CA_FOLIO)
GO

create table ROLUSUARIO (
    US_CODIGO Codigo,                    
    CA_FOLIO FolioGrande,                
    RU_ACTIVO Booleano,                  
    RU_VACIO Status,                     
    RU_US_CHG Usuario  
    )                  
GO


alter table ROLUSUARIO
add constraint PK_USUSCRIP primary key (US_CODIGO, CA_FOLIO)
GO

CREATE TABLE BITSERVREP (
		  BI_FOLIO		        Int identity ( 1, 1),
		  CA_FOLIO				FolioGrande,
		  BI_FECHA				Fecha,
		  BI_HORA				HoraMinutosSegundos,
		  BI_TIPO				Status,
		  BI_TEXTO				Texto,
		  BI_DATA				Memo,
		  CAL_FOLIO				FolioGrande
)
GO

ALTER TABLE BITSERVREP  WITH NOCHECK ADD  CONSTRAINT FK_BITSERREP_CALENDARIO FOREIGN KEY(CA_FOLIO)
REFERENCES CALENDARIO (CA_FOLIO)
GO


ALTER TABLE BITSERVREP NOCHECK CONSTRAINT FK_BITSERREP_CALENDARIO
GO

create view V_SUSCRIP as 
select C.CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, C.CM_CODIGO,  C.CA_FREC, C.CA_HORA, C.CA_ULT_FEC, C.CA_REPORT, VS_TIPO='USUARIO', RO_CODIGO='', RO_NOMBRE='', US.US_CODIGO, US.US_NOMBRE, US.US_EMAIL from  CALENDARIO C
left join ROLUSUARIO UC on C.CA_FOLIO = UC.CA_FOLIO and UC.RU_ACTIVO = 'S' 
left join USUARIO US on US.US_CODIGO = UC.US_CODIGO 
union all 
select C.CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, C.CM_CODIGO,  C.CA_FREC, C.CA_HORA, C.CA_ULT_FEC, C.CA_REPORT, VS_TIPO ='ROL', RC.RO_CODIGO, ROL.RO_NOMBRE, US2.US_CODIGO, US2.US_NOMBRE, US2.US_EMAIL from  CALENDARIO C
left join ROLSUSCRIP RC on C.CA_FOLIO = RC.CA_FOLIO and RC.RS_ACTIVO = 'S' 
left join ROL on ROL.RO_CODIGO = RC.RO_CODIGO 
left join USER_ROL UR on UR.RO_CODIGO = ROL.RO_CODIGO 
left join USUARIO US2 on US2.US_CODIGO = UR.US_CODIGO
GO

CREATE TABLE CALENSOLI (
	CAL_FOLIO         Int identity ( 1, 1),
    CA_FOLIO          FolioGrande,
    RE_CODIGO         FolioChico,
    CAL_FECHA         Fecha,
    CAL_INICIO        Fecha,
    CAL_FIN           Fecha,
    CAL_ESTATUS       Status,
    CAL_MENSAJ        Memo
)
GO

ALTER TABLE CALENSOLI  WITH NOCHECK ADD  CONSTRAINT FK_CALENSOLI_CALENDARIO FOREIGN KEY(CA_FOLIO)
REFERENCES CALENDARIO (CA_FOLIO)
GO

ALTER TABLE CALENSOLI NOCHECK CONSTRAINT FK_CALENSOLI_CALENDARIO
GO


CREATE TABLE T_WFConfig ( 
	ConfigTipo status,
	ConfigXML XML, 
	ConfigFecha Fecha,
	ConfigUsuario Usuario
) 
GO

CREATE TABLE T_WFConfigParams ( 
	ClienteID  FolioGrande, 
	ParamID FolioChico,
	ParamValue Formula  
)
GO

CREATE TABLE WS_DIAGNOS (
	LLAVE int IDENTITY(1,1),
	WS_FECHA Fecha,
	CH_RELOJ RelojLinx,
	WS_DATOS XML 
)
GO

ALTER TABLE WS_DIAGNOS ADD CONSTRAINT PK_WS_DIAGNOS PRIMARY KEY ( LLAVE )
GO
