CREATE TRIGGER TD_GRUPO ON GRUPO AFTER DELETE AS
begin
     set NOCOUNT ON;
     declare @NumRows Integer;
     declare @OldCodigo FolioChico;
     select @OldCodigo = GR_CODIGO from Deleted;
     select @NumRows = COUNT(*) from GRUPO where ( GRUPO.GR_PADRE = @OldCodigo );
     if ( @NumRows > 0 )
     begin
	  RAISERROR( 'No Es Posible Borrar Un Grupo Que Tiene Subordinados Otros Grupos', 16, 1 )
     end
end
GO

CREATE TRIGGER TU_GRUPO ON GRUPO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( GR_CODIGO )
  BEGIN
	declare @NewCodigo FolioChico;
        declare @OldCodigo FolioChico;

	select @NewCodigo = GR_CODIGO from Inserted;
        select @OldCodigo = GR_CODIGO from Deleted;

        update GRUPO set GR_PADRE = @NewCodigo
        where ( GRUPO.GR_PADRE = @OldCodigo );
  END
END
GO

CREATE TRIGGER TD_COMPANY ON COMPANY AFTER DELETE AS
begin
     set NOCOUNT ON;
     declare @OldCodigo CodigoEmpresa;
     select @OldCodigo = CM_CODIGO from Deleted;
     update USUARIO set USUARIO.CM_CODIGO = '', USUARIO.CB_CODIGO = 0
     where ( USUARIO.CM_CODIGO = @OldCodigo );
END
GO

CREATE TRIGGER TU_COMPANY ON COMPANY AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CM_CODIGO )
  BEGIN
	declare @NewCodigo CodigoEmpresa;
        declare @OldCodigo CodigoEmpresa;

	select @NewCodigo = CM_CODIGO from Inserted;
        select @OldCodigo = CM_CODIGO from Deleted;

        update USUARIO set USUARIO.CM_CODIGO = @NewCodigo
        where ( USUARIO.CM_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TDOCUMEN ON TDOCUMEN AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
	  declare @NewTB_CODIGO Codigo, @OldTB_CODIGO Codigo;

	  select @NewTB_CODIGO = TB_CODIGO from Inserted;
          select @OldTB_CODIGO = TB_CODIGO from Deleted;

          update DOCUMENT set DOCUMENT.DC_CLASIFI = @NewTB_CODIGO
          where ( DOCUMENT.DC_CLASIFI = @OldTB_CODIGO );
     END
END
GO

CREATE TRIGGER TU_TEVENTO ON TEVENTO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
          declare @NewTB_CODIGO Codigo, @OldTB_CODIGO Codigo;

	  select @NewTB_CODIGO = TB_CODIGO from Inserted;
          select @OldTB_CODIGO = TB_CODIGO from Deleted;

          update EVENTO set EVENTO.EV_CLASIFI = @NewTB_CODIGO
          where ( EVENTO.EV_CLASIFI = @OldTB_CODIGO );
     END
END
GO

CREATE TRIGGER TU_TNOTICIA ON TNOTICIA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
	  declare @NewTB_CODIGO Codigo, @OldTB_CODIGO Codigo;

	  select @NewTB_CODIGO = TB_CODIGO from Inserted;
          select @OldTB_CODIGO = TB_CODIGO from Deleted;

          update NOTICIA set NOTICIA.NT_CLASIFI = @NewTB_CODIGO
          where ( NOTICIA.NT_CLASIFI = @OldTB_CODIGO );
     END
END
GO

CREATE TRIGGER TD_USUARIO ON USUARIO AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
     declare @OldUS_CODIGO Usuario;
     select @OldUS_CODIGO = US_CODIGO from Deleted;
     delete from PAGINA where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
   	 UPDATE USUARIO SET US_JEFE = 0 WHERE ( US_JEFE = @OLDUS_CODIGO );
	   UPDATE USUARIO SET US_PAG_DEF = 0 WHERE ( US_PAG_DEF = @OLDUS_CODIGO );
	 update USUARIO set US_WF_ALT = 0, US_WF_OUT = 'N' where US_WF_ALT = @OldUS_CODIGO;
END
GO

CREATE TRIGGER TU_USUARIO ON USUARIO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
	 declare @US_CODIGO USUARIO;
     IF UPDATE( US_CODIGO )
     BEGIN
	      declare @NewUS_CODIGO Usuario, @OldUS_CODIGO Usuario;
	      select @NewUS_CODIGO = US_CODIGO from Inserted;
        select @OldUS_CODIGO = US_CODIGO from Deleted;
        update PAGINA set PAGINA.US_CODIGO = @NewUS_CODIGO
        where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
		    update USUARIO set US_JEFE = @NEWUS_CODIGO
		    where ( US_JEFE = @OLDUS_CODIGO );
		    update USUARIO set US_PAG_DEF = @NEWUS_CODIGO
		    where ( US_PAG_DEF = @OLDUS_CODIGO );
     END
	 ELSE IF UPDATE( US_WF_OUT )
	 BEGIN
		declare @NEWUS_WF_OUT BOOLEANO;
		select @NEWUS_WF_OUT = US_WF_OUT, @US_CODIGO = US_CODIGO from INSERTED;
		IF( @NEWUS_WF_OUT = 'N' )
			update USUARIO set US_WF_ALT = 0 where US_CODIGO = @US_CODIGO;
	 END
	 ELSE IF UPDATE( US_ACTIVO )
	 BEGIN
		declare @NEWUS_ACTIVO BOOLEANO;
		select @US_CODIGO = US_CODIGO, @NEWUS_ACTIVO = US_ACTIVO from INSERTED;
		IF( @NEWUS_ACTIVO = 'N' )
			update USUARIO set US_WF_ALT = 0, US_WF_OUT = 'N' where US_CODIGO = @US_CODIGO OR US_WF_ALT = @US_CODIGO;
	 END;
END
GO

CREATE TRIGGER TD_PAGINA ON PAGINA AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
     declare @OldUS_CODIGO Usuario;
     declare @OldPA_ORDEN FolioChico;
     select @OldUS_CODIGO = US_CODIGO, @OldPA_ORDEN = PA_ORDEN from Deleted;
     delete from CONTIENE where ( CONTIENE.US_CODIGO = @OldUS_CODIGO ) and
                                ( CONTIENE.PA_ORDEN = @OldPA_ORDEN );
END
GO

CREATE TRIGGER TU_PAGINA ON PAGINA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( US_CODIGO ) or UPDATE( PA_ORDEN )
     BEGIN
	  declare @NewUS_CODIGO Usuario, @OldUS_CODIGO Usuario;
          declare @NewPA_ORDEN FolioChico, @OldPA_ORDEN FolioChico;

	  select @NewUS_CODIGO = US_CODIGO, @NewPA_ORDEN = PA_ORDEN from Inserted;
          select @OldUS_CODIGO = US_CODIGO, @OldPA_ORDEN = PA_ORDEN from Deleted;
          update CONTIENE set
                 CONTIENE.US_CODIGO = @NewUS_CODIGO,
                 CONTIENE.PA_ORDEN = @NewPA_ORDEN
          where ( CONTIENE.US_CODIGO = @OldUS_CODIGO ) and
                ( CONTIENE.PA_ORDEN = @OldPA_ORDEN );
     end
END
GO


CREATE TRIGGER TU_KSCREEN ON KSCREEN AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( KS_CODIGO )
     BEGIN
	  declare @NewCodigo Codigo, @OldCodigo Codigo;

	  select @NewCodigo = KS_CODIGO from   Inserted;
          select @OldCodigo = KS_CODIGO from   Deleted;

	  UPDATE KSHOW	SET KW_SCR_DAT = @NewCodigo WHERE KW_SCR_DAT = @OldCodigo;
      END
END
GO



CREATE TRIGGER TD_KSCREEN ON KSCREEN AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	update KSHOW
	set    KSHOW.KW_SCR_DAT = ''
	from   KSHOW, Deleted
	where  KSHOW.KW_SCR_DAT = Deleted.KS_CODIGO
END
GO


CREATE TRIGGER TU_KESTILO ON KESTILO AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
  	IF UPDATE( KE_CODIGO )
  	BEGIN
		declare @NewCodigo Codigo, @OldCodigo Codigo;

		select @NewCodigo = KE_CODIGO from   Inserted;
        	select @OldCodigo = KE_CODIGO from   Deleted;

		UPDATE KSCREEN	SET KS_EST_NOR = @NewCodigo WHERE KS_EST_NOR = @OldCodigo;
		UPDATE KSCREEN	SET KS_EST_SEL = @NewCodigo WHERE KS_EST_SEL = @OldCodigo;
 	END
END
GO


CREATE TRIGGER TD_KESTILO ON KESTILO AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	update KSCREEN
	set    KSCREEN.KS_EST_NOR = ''
	from   KSCREEN, Deleted
	where  KSCREEN.KS_EST_NOR = Deleted.KE_CODIGO

	update KSCREEN
	set    KSCREEN.KS_EST_SEL = ''
	from   KSCREEN, Deleted
	where  KSCREEN.KS_EST_SEL = Deleted.KE_CODIGO
END
GO

CREATE TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	DECLARE @NEWCODIGO CONDICION, @OLDCODIGO CONDICION;

	SELECT @NEWCODIGO = QU_CODIGO FROM INSERTED;
		SELECT @OLDCODIGO = QU_CODIGO FROM DELETED;

	IF ( @OLDCODIGO <> '' ) AND ( @NEWCODIGO <> '' )
	BEGIN
		 UPDATE REPORTE SET QU_CODIGO = @NEWCODIGO WHERE QU_CODIGO = @OLDCODIGO;
	END
  END
END
GO

CREATE TRIGGER TD_COMPANY_SP_PATCH ON COMPANY AFTER DELETE AS 
begin
	set NOCOUNT ON; 
	declare @OldCodigo CodigoEmpresa; 
	select @OldCodigo = CM_CODIGO from Deleted;
	delete from SP_PATCH where SP_PATCH.SE_APLICA = @OldCodigo ;
END 
GO

CREATE TRIGGER TU_COMPANY_SP_PATCH ON COMPANY AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
	IF UPDATE( CM_CODIGO )
	BEGIN
		declare @NewCodigo CodigoEmpresa;
		declare @OldCodigo CodigoEmpresa;
		select @NewCodigo = CM_CODIGO from Inserted;
		select @OldCodigo = CM_CODIGO from Deleted;
	    update SP_PATCH set SP_PATCH.SE_APLICA = @NewCodigo where ( SP_PATCH.SE_APLICA = @OldCodigo );
	END
END
GO

CREATE TRIGGER TD_WPROCESO_XML  ON WPROCESO AFTER DELETE
AS
BEGIN
	SET NOCOUNT ON;
	delete from VT_WPAUTO3 where WP_FOLIO in ( select WP_FOLIO from deleted where WP_MOV3 in ( 'AUTO3', 'AUTO3MULT') )
END
GO

CREATE TRIGGER TU_WPROCESO  ON  WPROCESO AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	IF UPDATE(WP_XMLDATA)
	begin
		declare @WP_FOLIO FolioGrande
		
		select top 1 @WP_FOLIO = WP_FOLIO from Inserted 

		exec SP_REPLICA_XMLDOC @WP_FOLIO

		update WPROCESO
		set WP_MOV3 = coalesce( cast( WPROCESO.WP_XMLDOC.query('data(//DATOS/MOV3[1])') as varchar(30) ) , '' ) ,
			WP_EMPRESA = coalesce(  cast( WPROCESO.WP_XMLDOC.query('data(//DATOS/EMPRESA[1])') as varchar(30) ), '' ) 
		from Inserted where WPROCESO.WP_FOLIO = Inserted.WP_FOLIO

		/* Actualizacion de la Vista Tabla de Autorizaciones  */
		declare @i int
		declare @n int


		select IDENTITY(int, 1, 1) as RowID, Inserted.WP_FOLIO into #tempFolios 
		from Inserted 
		join WPROCESO on Inserted.WP_FOLIO = WPROCESO.WP_FOLIO  and   WPROCESO.WP_MOV3  in ( 'AUTO3', 'AUTO3MULT')
		select @n = count(*) from #tempFolios 

		if (@n =1)
		begin
			delete from VT_WPAUTO3 where WP_FOLIO in ( select WP_FOLIO from  #tempFolios )
			set @i = 1
			while @i <= @n
			begin
				select @WP_FOLIO = WP_FOLIO from #tempFolios where RowID = @i
				insert into VT_WPAUTO3 (WP_FOLIO, CM_CODIGO, WP_MODO, CB_CODIGO, AT_FEC_AUT, AT_TIPO, AT_HORAS, AT_MOTIVO)
				select V.WP_FOLIO, V.CM_CODIGO, V.WP_MODO, V.CB_CODIGO, V.AT_FEC_AUT, V.AT_TIPO, V.AT_HORAS, V.AT_MOTIVO
				from  V_WPAUTO3_PRE_PROCESS V where WP_FOLIO = @WP_FOLIO

				set @i = @i + 1
			end
		end
		else
		if ( @n > 1 )
		begin
			insert into VT_WPAUTO3 (WP_FOLIO, CM_CODIGO, WP_MODO, CB_CODIGO, AT_FEC_AUT, AT_TIPO, AT_HORAS, AT_MOTIVO)
			select V.WP_FOLIO, V.CM_CODIGO, V.WP_MODO, V.CB_CODIGO, V.AT_FEC_AUT, V.AT_TIPO, V.AT_HORAS, V.AT_MOTIVO
			from  V_WPAUTO3_PRE_PROCESS V where WP_FOLIO in (SELECT WP_FOLIO FROM #tempFolios )
		end
		drop table #tempFolios
	end
END
GO

CREATE trigger TR_ADD_TERMINAL on TERMINAL after insert
as
begin
	set nocount on
	
	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx		
	declare ListaMensajes cursor for
		select DM_CODIGO, DM_SLEEP from MENSAJE where DM_ACTIVO='S' order by DM_ORDEN
		
	select @Terminal = TE_CODIGO from Inserted;
	
	open ListaMensajes
	fetch next from ListaMensajes into @Mensaje, @Sleep
	while (@@Fetch_Status = 0)
	begin
	    if (@Mensaje = 8 )
		begin
            insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP, TM_NEXT) values (@Terminal, @Mensaje, @Sleep, Convert(datetime,'29991231'))
		end
		else
		begin
			insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP) values (@Terminal, @Mensaje, @Sleep)
		end
		
		fetch next from ListaMensajes into @Mensaje, @Sleep
	end
	close ListaMensajes
	deallocate ListaMensajes
	
	update TERMINAL set TE_XML = 
'<?xml version="1.0" encoding="utf-8"?>
<properties>
   <ClockId>'+@Terminal+'</ClockId> 
   <WebService>http://SERVIDORWEB/WSAsistencia/WSAsistencia.asmx</WebService>
   <UseProxy>false</UseProxy>
   <Proxy>192.168.0.30</Proxy>
   <ProxyPort>8080</ProxyPort>
   <UseProxyAuthentication>false</UseProxyAuthentication>
   <ProxyUserName>NULL</ProxyUserName>
   <ProxyPassword>NULL</ProxyPassword>
   <Ip>DHCP</Ip>
   <NetMask></NetMask>
   <DefaultGateWay></DefaultGateWay>
   <DNS></DNS>
   <TimeOut>07</TimeOut>
   <HeartBeatTimer>60</HeartBeatTimer>
   <MsgTime>01</MsgTime>
   <ErrorMsgTime>05</ErrorMsgTime>
   <RelayTime>0</RelayTime>
   <DateTimeFormat>dd/MM hh:mm</DateTimeFormat>
   <Volume>70</Volume>
   <ProxReader>OFF</ProxReader>
   <BarCodeReader>OFF</BarCodeReader>
   <HID>OFF</HID>
   <MenuAccessCode>1234</MenuAccessCode>
   <FpuNotIdetifyMessage>INVALIDO/INTENTE DE NUEVO</FpuNotIdetifyMessage>
   <NotValidByServerMessage>Servidor no pudo validar ID</NotValidByServerMessage>
   <DuplicateMessage>Registro Duplicado</DuplicateMessage>
   <AutenticateMessage>Exito</AutenticateMessage>
   <OfflineValidation>Offline-Almacenada</OfflineValidation>
   <OneMinuteValidate>true</OneMinuteValidate>   
   <FingerVerify>false</FingerVerify>
   <EnableOfflineRelay>false</EnableOfflineRelay>
   <FingerPrint>ON</FingerPrint>
   <EnableKeyPad>true</EnableKeyPad>
   <Version>TRESS</Version>
   <Account>0</Account>
   <ClientId>0</ClientId>
   <Password>0</Password>
   <CajaId>0</CajaId>
   <Identidad>1</Identidad>
   <UseSoapActionHeader>true</UseSoapActionHeader>
   <SoapActionHeader>Synel/Services</SoapActionHeader>
   <UpdateTimeByServer>true</UpdateTimeByServer>
   <ntpServer>SERVIDORNTP</ntpServer>
   <timeZone>Tijuana</timeZone>
   <MifareReader>OFF</MifareReader>
   <MagneticReader>OFF</MagneticReader>
   <ShowCameraPhoto>false</ShowCameraPhoto>
   <ShowPhoto>true</ShowPhoto>
   <Schedules>
   <Function code="1" start="00:00" end="23:59" bg="comida.jpg">Comida</Function>
   <Function code="3" start="12:01" end="14:00" bg="desayuno.jpg">Desayuno</Function>
   <Function code="2" start="14:01" end="23:59" bg="cena.jpg">Cena</Function>
   </Schedules>
   <DetalleBitacora>false</DetalleBitacora>
</properties>' 
	where TE_CODIGO = @Terminal
end
GO

create trigger TR_UPDATE_TERMINAL on TERMINAL after update
as
begin
	set nocount on
	declare @Terminal RelojLinx
	select @Terminal = TE_CODIGO from Inserted

	if update( TE_XML ) or update( TE_APP )
	begin
		UPDATE TERM_MSG SET TM_NEXT = '18991230' WHERE (TE_CODIGO = @Terminal) AND (DM_CODIGO = 5)
	end
end
GO

create trigger TR_UPDATE_EMP_BIO on EMP_BIO after update
as
begin
	set nocount on

	declare @Empleado FolioGrande
	declare @EmpleadoAnterior FolioGrande
	declare @Empresa CodigoEmpresa
	declare @EmpresaAnterior CodigoEmpresa
	select @EmpleadoAnterior = CB_CODIGO, @EmpresaAnterior = CM_CODIGO from Deleted
	select @Empleado = CB_CODIGO, @Empresa  = CM_CODIGO from Inserted

	if( update( CB_CODIGO ) or update( CM_CODIGO ) )
	begin
		UPDATE HUELLAGTI SET CB_CODIGO = @Empleado, CM_CODIGO = @Empresa WHERE (CM_CODIGO = @EmpresaAnterior) AND (CB_CODIGO = @EmpleadoAnterior)
	end
end
GO

Create trigger TR_UPDATE_HUELLAGTI on HUELLAGTI AFTER INSERT, UPDATE 
as 
begin 
	set nocount on 
	 
	declare @HU_ID NumeroEmpleado; 
	declare @Empleado NumeroEmpleado;
	declare @Empresa CodigoEmpresa;
	declare @Indice smallint;
	
	select @HU_ID = HU_ID, @Empleado = CB_CODIGO, @Empresa = CM_CODIGO, @Indice = HU_INDICE from Inserted 
	
	if UPDATE( HU_HUELLA ) 
	begin 
		update HUELLAGTI set HU_TIMEST = GETDATE() where HU_ID = @HU_ID
		
		delete from TERM_HUELLA where ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa ) and ( HU_INDICE = @Indice )
	end 

end
GO

CREATE trigger TR_DELETE_HUELLAGTI
on HUELLAGTI
AFTER DELETE
as
begin
     set nocount on

     declare @HU_ID NumeroEmpleado
	 declare @Empresa CodigoEmpresa
	 declare @Empleado NumeroEmpleado
	 declare @HU_INDICE NumeroEmpleado

     select @HU_ID = HU_ID, @Empresa = CM_CODIGO, @Empleado = CB_CODIGO, @HU_INDICE = HU_INDICE from deleted;

     delete from COM_HUELLA where HU_ID = @HU_ID;

	 if ( @HU_INDICE = 11 )
	 begin
			UPDATE TERM_HUELLA set BORRAR ='S' where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) and ( HU_INDICE = @HU_INDICE );
			UPDATE TERM_MSG SET TM_NEXT = '18991230' WHERE ( DM_CODIGO = 6 ) and ( TE_CODIGO in ( select TE_CODIGO from TERMINAL where ( TE_ID = 5 ) ) )
	 end
	 else
	 begin
			UPDATE TERM_HUELLA set BORRAR ='S' where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) and ( HU_INDICE < 11 );
			UPDATE TERM_MSG SET TM_NEXT = '18991230' WHERE ( DM_CODIGO = 6 ) and ( TE_CODIGO in ( select TE_CODIGO from TERMINAL where ( TE_ID <> 5 ) ) )
	 end
end
GO

CREATE trigger TR_DB_INFO
on DB_INFO for update AS
begin
	set NOCOUNT on;

	declare @DB_CODIGO CodigoEmpresa;
	declare @DB_CONTROL Referencia;
	declare @DB_TIPO Status;
	declare @DB_DATOS Accesos;
	declare @DB_USRNAME UsuarioCorto;
	declare @DB_PASSWRD PasswdLrg;
	declare @DB_CTRL_RL Referencia;
	declare @DB_CHKSUM Accesos;

	declare @OldCodigo CodigoEmpresa;
	declare @NewCodigo CodigoEmpresa;
	select @OldCodigo = DB_CODIGO from Deleted;
	select @NewCodigo = DB_CODIGO from Inserted;

	select @DB_CODIGO = DB_CODIGO, @DB_CONTROL = DB_CONTROL, @DB_TIPO = DB_TIPO, @DB_DATOS= DB_DATOS,
	@DB_USRNAME = DB_USRNAME, @DB_PASSWRD = DB_PASSWRD, @DB_CTRL_RL = DB_CTRL_RL, @DB_CHKSUM = DB_CHKSUM
	from INSERTED;

	update COMPANY
	set DB_CODIGO = @NewCodigo, CM_CONTROL = (
		CASE
			WHEN @DB_TIPO = 0 THEN '3DATOS'
			WHEN @DB_TIPO = 1 THEN '3RECLUTA'
			WHEN @DB_TIPO = 2 THEN '3VISITA'
			WHEN @DB_TIPO = 3 THEN '3OTRO'
			WHEN @DB_TIPO = 4 THEN '3DATOS' -- CM_CONTROL igual a 3DATOS si el TIPO es 4 (3PRUEBA)
			WHEN @DB_TIPO = 5 THEN '3PRESUP'
			ELSE ''
		END
		),
		CM_DATOS = @DB_DATOS,
		CM_USRNAME = @DB_USRNAME,
		CM_PASSWRD = @DB_PASSWRD,
		CM_CTRL_RL  = (
		CASE
			WHEN @DB_TIPO = 0 THEN '3DATOS'
			WHEN @DB_TIPO = 1 THEN '3RECLUTA'
			WHEN @DB_TIPO = 2 THEN '3VISITA'
			WHEN @DB_TIPO = 3 THEN '3OTRO'
			WHEN @DB_TIPO = 4 THEN '3PRUEBA'
			WHEN @DB_TIPO = 5 THEN '3PRESUP'
			ELSE ''
		END
		),
		CM_CHKSUM = @DB_CHKSUM
		where DB_CODIGO = @OldCodigo
end
GO

CREATE trigger TR_COMPANY_DB_CODIGO
on COMPANY for insert, update AS
begin
	 set NOCOUNT on;

	 IF UPDATE( DB_CODIGO )
	 BEGIN
		declare @DB_TIPO Status;
		declare @DB_DATOS Accesos;
		declare @DB_USRNAME UsuarioCorto;
		declare @DB_PASSWRD PasswdLrg;
		declare @DB_CHKSUM Accesos;

		select @DB_TIPO = DB_TIPO, @DB_DATOS= DB_DATOS,
			@DB_USRNAME = DB_USRNAME, @DB_PASSWRD = DB_PASSWRD, -- @DB_CTRL_RL = DB_CTRL_RL,
			@DB_CHKSUM = DB_CHKSUM
			from DB_INFO WHERE DB_CODIGO IN (SELECT DB_CODIGO FROM INSERTED);

		update COMPANY
			set CM_CONTROL = (
			CASE
				WHEN @DB_TIPO = 0 THEN '3DATOS'
				WHEN @DB_TIPO = 1 THEN '3RECLUTA'
				WHEN @DB_TIPO = 2 THEN '3VISITA'
				WHEN @DB_TIPO = 3 THEN '3OTRO'
				WHEN @DB_TIPO = 4 THEN '3DATOS'
				WHEN @DB_TIPO = 5 THEN '3PRESUP'
				ELSE ''
			END
			),
			CM_DATOS = COALESCE (@DB_DATOS, ''),
			CM_USRNAME = COALESCE (@DB_USRNAME, ''),
			CM_PASSWRD = COALESCE (@DB_PASSWRD, ''),
			CM_CTRL_RL = (
			CASE
				WHEN @DB_TIPO = 0 THEN '3DATOS'
				WHEN @DB_TIPO = 1 THEN '3RECLUTA'
				WHEN @DB_TIPO = 2 THEN '3VISITA'
				WHEN @DB_TIPO = 3 THEN '3OTRO'
				WHEN @DB_TIPO = 4 THEN '3PRUEBA'
				WHEN @DB_TIPO = 5 THEN '3PRESUP'
				ELSE ''
			END
			),
			CM_CHKSUM = COALESCE (@DB_CHKSUM, '')
		where CM_CODIGO IN (SELECT CM_CODIGO FROM INSERTED)
	END
end
GO

Create trigger TR_ACTUALIZA_TMNEXT on TERM_MSG
after UPDATE AS
BEGIN
	set nocount on

	declare @nextMsg datetime 
	declare @dmCodigo int
	declare @tecodigo int 


	select @nextMsg = TM_NEXT from Inserted
	select @dmCodigo = DM_CODIGO from inserted
	select @tecodigo = TE_CODIGO from inserted
		
	if( update( TM_NEXT ) )
	begin
		if( @dmCodigo = 8 and @nextmsg > Convert(datetime,'18991230') )
		begin
			EXECUTE('delete from TERM_HUELLA where TE_CODIGO='+@tecodigo)
			EXECUTE('update Term_MSG set tm_next =''18991230'' where TE_CODIGO='+@tecodigo+' and dm_Codigo = 4')
		end 

		if(@nextmsg > Convert(datetime,'18991230'))
		begin
			EXECUTE('update Term_MSG set tm_ultima =getdate() where TE_CODIGO='+@tecodigo+' and dm_Codigo = '+@dmCodigo)
		end
		else if (@nextmsg = Convert(datetime,'18991230'))
		begin
			EXECUTE('update Term_MSG set tm_next_rl =getdate() where TE_CODIGO='+@tecodigo+' and dm_Codigo = '+@dmCodigo)
		end
	end
end
GO

CREATE TRIGGER TR_COMPANY_VEMPL_BAJA ON dbo.COMPANY 
AFTER INSERT, DELETE, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	IF UPDATE( CM_CODIGO ) OR UPDATE (CM_DATOS)
	BEGIN
		EXECUTE SP_CREAR_VEMPL_BAJA;	
		EXECUTE SP_CREAR_VEMPL_ALTA;
	END
	ELSE IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		EXECUTE SP_CREAR_VEMPL_BAJA;
		EXECUTE SP_CREAR_VEMPL_ALTA;	
	END  
END
GO

Create trigger TU_TMNEXT_STATUS on dbo.TERM_MSG
after UPDATE AS
BEGIN
	set nocount on

	declare @nextMsg datetime 
	declare @dmCodigo int
	declare @tecodigo int 


	select @nextMsg = TM_NEXT from Inserted
	select @dmCodigo = DM_CODIGO from inserted
	select @tecodigo = TE_CODIGO from inserted
		
	if( update( TM_NEXT ) )
	begin
		if ( ( @dmCodigo = 4 ) or ( @dmCodigo = 6 ) or( @dmCodigo = 8 ) )
		begin
			if ( @nextMsg = Convert(datetime,'18991230') )
			begin
				EXECUTE WS_ACTUALIZA_STATUS_EMP	
			end
		end
	end
end
GO
