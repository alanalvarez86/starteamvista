
IF EXISTS (select * from syscolumns where id=object_id('WS_DIAGNOS') and name='WS_DATOS')
	alter table WS_DIAGNOS alter column WS_DATOS  XML null
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'SP_CALENDARIO')
BEGIN
	DROP PROCEDURE SP_CALENDARIO
END
GO

create PROCEDURE SP_CALENDARIO
(
	@FechaHoraServidor DateTime
) 
AS 
BEGIN
	 SET NOCOUNT ON 

	 /*
		Parámetros:
		1. ¿Folio de tabla Calendario?.
		Proceso:
		1. Leer tabla calendario.
		2. Solo registros activos.
		3. Clasificar por frecuencia (CA_FREC).
			a. Tipo de Frecuencia: 0=Diario, 1=Semanal,2=Mensual,3=Por hora,4=Especial.
			b. Almacenar el tipo de frecuencia en una variable (@FREC).
		4. Revisar recurrencia (CA_RECUR):
			a. Recurrencia (cada cuantos) para tipos de frecuencias: Diario, Semanal y Por Hora.
            b. Para tipo 'Especial', no se repite solo se aplica una vez en la fecha y hora de inicio.
		5. Si @FREC es semanal o mensual, revisar campo:
			a. CA_DOWS.
			b. Con esto se sabe qué días aplicar para frecuencias semanal o mensual.
			c. Almancena los días de la semana: 0=Domingo,1=Lunes,2=Martes,3=Miercoles,4=Jueves,5=Viernes,6=Sabado.
			d. Almacenar en variable (@DOWS).
		6. Grabar fecha y hora de última ejecución.
			a. @ULT_FEC.
		7. Establecer fecha y hora de siguiente ejecución en base a la frecuencia y demás campos, y en base a la fecha y hora de
			última ejecución.
			a. NOTA: CA_FECHA y CA_HORA son la fecha y hora de inicio de ejecución de la tarea respectivamente.
				Estos valores se tomarán en cuenta si fecha y hora de última ejecución están vacíos.
	 */

	DECLARE @FOLIO_ACTUAL FolioGrande;
	DECLARE @FREC Status;
	DECLARE @DOWS Descripcion;
	DECLARE @FECHA Fecha;
	DECLARE @HORA Hora;
	DECLARE @ULT_FEC Fecha;	

	DECLARE @NX_FEC Fecha;
	DECLARE @NX_HOR Hora;
	DECLARE @RECUR FolioChico;

	-- Para tareas semanales.
	DECLARE @ULTIMO_DIA_SEMANA INT;
	DECLARE @INDEX_DOWS INT;

	-- Tareas mensuales.
	DECLARE @MESES Descripcion;
	DECLARE @MESDIAS Titulo;
	DECLARE @MESWEEK Descripcion;
	DECLARE @INDEX INT;
	DECLARE @FECHA_TMP Fecha;
	
			
	DECLARE DB_CURSOR CURSOR FOR
	SELECT CA_FOLIO, CA_FECHA, CA_HORA, CA_FREC, CA_DOWS, CA_ULT_FEC, CA_RECUR, CA_MESES, CA_MESDIAS, CA_MESWEEK
		FROM CALENDARIO
		WHERE 
			(CA_ACTIVO = 'S' AND CA_NX_EVA = 'S')				
			OR				
			(CA_ACTIVO = 'S' AND CAST (CA_NX_FEC AS date) <= CAST (@FechaHoraServidor AS date) )				

	-- Procesar cada registro activo.
	-- Establecer fecha y hora de siguiente ejecución.
	OPEN DB_CURSOR

		FETCH NEXT FROM DB_CURSOR INTO @FOLIO_ACTUAL, @FECHA, @HORA, @FREC, @DOWS, @ULT_FEC, @RECUR, @MESES, @MESDIAS, @MESWEEK		

		WHILE @@FETCH_STATUS = 0
		BEGIN	
				-- Si fecha y hora de última ejecución son vacíos, tomar fecha y hora de inicio de la tarea.
				-- 1899-12-30 00:00:00.000

				-- IF @ULT_FEC = '18991230'
				-- BEGIN
					SET @ULT_FEC = @FECHA;
				-- END
				-- ----- -----

			    -- Establecer fecha de siguiente ejecución.
				-- Iniciar con la fecha más horas y minutos de siguiente ejecución.
				SET @NX_HOR = @HORA;
				-- Sin importar la hr que tenga @ULT_FEC, sustituir en @NX_FEC con la hr en @NX_HOR
				SET @ULT_FEC = CONVERT (DATE, @ULT_FEC) -- DATEADD(dd, 0, DATEDIFF(dd, 0, @ULT_FEC))
				SET @NX_FEC = DATEADD (HOUR, CAST (SUBSTRING (@NX_HOR, 1, 2) AS int), @ULT_FEC);
				SET @NX_FEC = DATEADD (MINUTE, CAST (SUBSTRING (@NX_HOR, 3, 2) AS int), @NX_FEC);				 
				 
				SET @INDEX_DOWS = 0;

				-- Si es de frecuencia diaria, sumar cantidad de días a repetir a la fecha y hora de última ejecución.
				-- 0=Diario
				IF @FREC = 0
				BEGIN					  
					WHILE @NX_FEC <= @FechaHoraServidor
					BEGIN
						SET @NX_FEC = @NX_FEC + @RECUR 						   
					END		
				END

				-- Si es de frecuencia semanal. Revisar recurrencia y días de la semana.
				-- 1=Semanal.
				ELSE IF @FREC =  1
				BEGIN
					-- Tomar en cuenta que en el campo CA_DOWS se almacenan los días de la semana a ejecutar la tarea.
					-- Se establecen del siguiente modo:
					-- 0=Domingo,1=Lunes,2=Martes,3=Miercoles,4=Jueves,5=Viernes,6=Sabado.

					WHILE @NX_FEC <= @FechaHoraServidor
					BEGIN
						-- De la última fecha de ejecución obtener el número de día de acuerdo a la lista mostrada:
						-- 0=Domingo,1=Lunes,2=Martes,3=Miercoles,4=Jueves,5=Viernes,6=Sabado.
						-- Obtener del campo CA_DOWS el último día de la semana que debe ejecutarse la tarea.
						-- SET @ULTIMO_DIA_SEMANA = RIGHT(@DOWS,CHARINDEX(',',REVERSE(@DOWS))-1);
						SELECT TOP 1 @ULTIMO_DIA_SEMANA = VALOR FROM FN_COMMALIST_TO_ROWS (@DOWS) ORDER BY VALOR DESC;

						-- Obtener diferencia del @DOWS ---> @ULTIMO_DIA_SEMANA con el día de última ejecución.
						-- IF @ULTIMO_DIA_SEMANA - DATEPART(WEEKDAY, @ULT_FEC) < 0
						IF @ULTIMO_DIA_SEMANA - DATEPART(WEEKDAY, @NX_FEC) <= 0
						BEGIN
							-- Significa que ya se ejecutó para el último día de la semana.
							-- La siguiente fecha de ejecución debe establecerse con el campo CA_RECUR
							-- que indica cada cuántas semanas debe repetirse la tarea.
							SET @NX_FEC = DATEADD(wk, @RECUR, @ULT_FEC);
							-- Obtener lunes para la semana de la fecha generada en @NX_FEC.
							SET @NX_FEC = DATEADD(wk, DATEDIFF (wk, 0, @NX_FEC), 0);
							-- Ahora establecer fecha para el primer día de la semana en CA_DOWS
							SET @NX_FEC = DATEADD (D, (CAST ( (  SELECT TOP 1 VALOR FROM FN_COMMALIST_TO_ROWS (@DOWS) ORDER BY VALOR ASC  ) AS int)-2), @NX_FEC);
								
							-- Asignar horas y minutos.
							SET @NX_FEC = DATEADD (HOUR, CAST (SUBSTRING (@NX_HOR, 1, 2) AS int), @NX_FEC);
							SET @NX_FEC = DATEADD (MINUTE, CAST (SUBSTRING (@NX_HOR, 3, 2) AS int), @NX_FEC);
							SET @ULT_FEC = @NX_FEC;
						END
						ELSE
						BEGIN
							-- Si la diferencia es mayor a cero significa que todavía necesita ejecutarse para la
							-- misma semana de la última fecha de ejecución.
							-- Debe sumarse la cantidad de días que se indica en DOWS.								
							-- 
							-- IF @INDEX_DOWS > 0
							-- BEGIN
									SET @NX_FEC = @NX_FEC + 1; ---> ??
							-- END
								
							WHILE CAST (DATEPART(WEEKDAY, @NX_FEC) AS VARCHAR) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@DOWS)) -- AND @INDEX_DOWS < 8								
							BEGIN
								SET @INDEX_DOWS = @INDEX_DOWS + 1; -- ---> ??
								SET @NX_FEC = @NX_FEC + 1;
							END
						END
					END
				END

				-- 2=Mensual
				ELSE IF @FREC =  2
				BEGIN
					WHILE @NX_FEC <= @FechaHoraServidor
					BEGIN
						-- ¿Es de días al mes o de días por semanas?
						IF @MESDIAS <> ''
						BEGIN
							-- Es de días al mes.
							-- Revisar si el mes y el día coinciden. Si no, avanzar en días hasta que la fecha coincida.
							-- Lista de meses almacenada en @MESES.
							-- Lista de días al mes almacenada en @MESDIAS.
							-- Importante, sí el día es 0 indica que se ejecutará el último día del mes.														
							-- SET @NX_FEC = @NX_FEC + 1;
							SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);

							-- Repetir hasta estar en un día de ejecución.
							-- WHILE DATEPART (DAY, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS))
							WHILE DATEPART (MONTH, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESES))
								OR DATEPART (DAY, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS))

								-- O mientras el día sea el último del mes pero no se encuentra en @MESDIAS.
								-- OR (@NX_FEC = EOMONTH (@NX_FEC) AND 0 NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS)))

							BEGIN
									-- Pasar al siguiente día.									 
									-- SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);

									-- Si no coincide el mes.
									IF DATEPART (MONTH, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESES))								
								    BEGIN
										-- Primer día del siguiente mes.
										SET @NX_FEC  = DATEADD(mm, DATEDIFF(mm, 0, DATEADD (MONTH, 1, @NX_FEC)), 0)

										-- Asignar horas y minutos.
										SET @NX_FEC = DATEADD (HOUR, CAST (SUBSTRING (@NX_HOR, 1, 2) AS int), @NX_FEC);
										SET @NX_FEC = DATEADD (MINUTE, CAST (SUBSTRING (@NX_HOR, 3, 2) AS int), @NX_FEC);
										-- ----- -----
									END
									-- Revisar si es el último día del mes y este se encuentre en @MESDIAS
									-- ELSE IF 0 IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS)) AND DATEPART (DAY,@NX_FEC) = DATEPART (DAY,EOMONTH (@NX_FEC))
									ELSE IF 0 IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS)) AND DATEPART (DAY,@NX_FEC) = DATEPART (DAY, (DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@NX_FEC)+1,0))))
									BEGIN
										BREAK;
									END
									-- Si no coincide el día
									ELSE IF DATEPART (DAY, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS))
									BEGIN
										-- SET @NX_FEC = @NX_FEC + 1;
										SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);
									END									 
							END
						END
						ELSE
						BEGIN
							-- Es de días por semana.
							-- Crear tabla con los días que aplicará la tarea.
							Declare @TmpMensual TABLE 
                            (FECHA DATE, NUM_DIA INT, SEMANA INT)

							SET @INDEX = 1;
							SET @FECHA_TMP = DATEADD(mm, DATEDIFF(mm, 0, @FechaHoraServidor), 0)								

							-- Se llena la tabla temporal con los días que aplica de acuerdo a:
							-- @MESES y @DOWS.
							-- La lista en @MESWEEK se utilizará después al ir generando la fecha de siguiente ejecución.								
							WHILE @INDEX <= 400
							BEGIN
									IF DATEPART (MONTH, @FECHA_TMP) IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@MESES))
									BEGIN
										IF DATEPART (WEEKDAY, @FECHA_TMP) IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@DOWS))
										BEGIN
											INSERT INTO @TmpMensual VALUES (@FECHA_TMP, DATEPART (WEEKDAY, @FECHA_TMP), 0)
											-- Establecer número de semana por tipo de día, mes y año.
											UPDATE @TmpMensual SET SEMANA = (SELECT COUNT(NUM_DIA) - 1 FROM @TmpMensual
  												WHERE NUM_DIA = DATEPART (WEEKDAY, @FECHA_TMP) AND DATEPART (MONTH, FECHA) = DATEPART (MONTH, @FECHA_TMP)
												AND DATEPART (YEAR, FECHA) = DATEPART (YEAR, @FECHA_TMP)) WHERE FECHA = @FECHA_TMP
										END
									END

									SET @INDEX = @INDEX + 1;
									SET @FECHA_TMP = DATEADD (DAY, 1, @FECHA_TMP)
							END
							-- =============================================================================================================================															

							-- IMPORTANTE.
							-- Analizar si la siguiente asignación debe hacerse al principio antes de cada caso.
							-- Esto es porque no tiene sentido que @NX_FEC se iguale a la última fecha de ejecución.
							-- Es mejor que inicie en el momento actual.

							-- Cambiar @MESWEEK para incluir la última semana si esta lista contiene el valor 4.
							IF '4' IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@MESWEEK))
							BEGIN
								SET @MESWEEK = @MESWEEK + ',3';
							END

							-- Consulta sobre la tabla temporal de acuerdo al valor en @NX_FEC.
							-- Repetir hasta que exista registro que coincida.
							SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);

							WHILE NOT EXISTS (
									SELECT FECHA, NUM_DIA, SEMANA FROM @TmpMensual
										WHERE SEMANA  IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@MESWEEK))
										AND FECHA = CAST (@NX_FEC AS date)
								)
							BEGIN
									SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);
							END								
						END
					END
				END

				 
				-- 3=Por hora
				ELSE IF @FREC =  3
				BEGIN
					WHILE @NX_FEC <= @FechaHoraServidor
					BEGIN
						SET @NX_FEC = DATEADD (HOUR, @RECUR, @NX_FEC);
						SET @NX_HOR = SUBSTRING ((CONVERT (VARCHAR(23), @NX_FEC, 121)), 12, 2) + SUBSTRING ((CONVERT (VARCHAR(23), @NX_FEC, 121)), 15, 2)
					END
				END

				-- Realizar actualización del registro.
				-- UPDATE CALENDARIO SET CA_NX_FEC = @NX_FEC, CA_NX_EVA = 'N' WHERE CA_FOLIO = @FOLIO_ACTUAL				 
				UPDATE CALENDARIO SET CA_NX_FEC = @NX_FEC, CA_NX_EVA = 'N' WHERE CA_FOLIO = @FOLIO_ACTUAL				 
				-- Pasar al siguiente registro.
				FETCH NEXT FROM DB_CURSOR INTO @FOLIO_ACTUAL, @FECHA, @HORA, @FREC, @DOWS, @ULT_FEC, @RECUR, @MESES, @MESDIAS, @MESWEEK
		END
		
	-- Cerrar DB_CURSOR.		
	CLOSE DB_CURSOR;
	DEALLOCATE DB_CURSOR;

END
GO