/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 450: Crear tabla TRESSFILES 1/4 */
IF not exists (SELECT * FROM sysobjects where id = object_id(N'TRESSFILES'))
begin
	CREATE TABLE TRESSFILES(
		TF_PATH varchar(600)  NOT NULL,
		TF_EXT dbo.Descripcion NOT NULL,
		TF_FILE dbo.Ruta_URL NOT NULL,
		TF_DESCR dbo.Formula ,
		TF_VERSION dbo.Descripcion ,
		TF_BUILD dbo.Descripcion ,
		TF_FEC_MOD Fecha ,
		TF_CALLS integer ,
		TF_FEC_ACC Fecha ,
		TF_HOST dbo.Formula ,
		TF_FEC_UPD Fecha ,
		CONSTRAINT PK_TRESSFILES PRIMARY KEY CLUSTERED 
	(
		TF_PATH ASC,
		TF_EXT ASC,
		TF_FILE ASC
	) 
	)
end
GO

/* Patch 446: Crear procedure */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'SP_SET_TRESSFILE')
BEGIN
  DROP PROCEDURE SP_SET_TRESSFILE
END
GO
create PROCEDURE dbo.SP_SET_TRESSFILE 
( 
	@PATH varchar(600),  
	@TF_EXT Descripcion,
	@TF_FILE Ruta_URL,
	@TF_DESCR  Formula,
	@TF_VERSION Descripcion,
	@TF_BUILD Descripcion,
	@TF_FEC_MOD Fecha,
	@TF_CALLS integer,
	@TF_FEC_ACC Fecha,
	@TF_HOST Formula
)
AS begin SET NOCOUNT ON 

	Declare	@TF_FEC_UPD Fecha
	SET @TF_FEC_UPD = GETDATE()

      IF EXISTS (SELECT 1   FROM   TRESSFILES    WHERE  ((TF_PATH = @PATH and TF_EXT = @TF_EXT and TF_FILE = @TF_FILE) ))
	  begin
		IF EXISTS (SELECT 2   FROM   TRESSFILES    WHERE  (TF_PATH = @PATH and TF_EXT = @TF_EXT and TF_FILE = @TF_FILE)  and (@TF_FEC_MOD > TF_FEC_MOD))
			begin
			UPDATE TRESSFILES set 
				TF_DESCR = @TF_DESCR,
				TF_VERSION = @TF_VERSION,
				TF_BUILD = @TF_BUILD,
				TF_FEC_MOD = @TF_FEC_MOD,
				TF_CALLS = @TF_CALLS,
				TF_FEC_ACC = @TF_FEC_ACC,
				TF_HOST = @TF_HOST,
				TF_FEC_UPD = @TF_FEC_UPD
				where TF_PATH = @PATH and TF_EXT = @TF_EXT and TF_FILE = @TF_FILE
			end
	  end
      ELSE
	  begin
		INSERT INTO TRESSFILES
		(
			TF_PATH,
			TF_EXT,
			TF_FILE,
			TF_DESCR,
			TF_VERSION,
			TF_BUILD,
			TF_FEC_MOD,
			TF_CALLS,
			TF_FEC_ACC,
			TF_HOST,
			TF_FEC_UPD
		) 
		VALUES
		(
			@PATH,
			@TF_EXT,
			@TF_FILE,
			@TF_DESCR,
			@TF_VERSION,
			@TF_BUILD,
			@TF_FEC_MOD,
			@TF_CALLS,
			@TF_FEC_ACC,
			@TF_HOST,
			@TF_FEC_UPD
		)
	end
END
GO

/* #11366 Contraseña usuario SQL mayor a 14 caracteres */

exec sp_addtype PasswdLrg, 'varchar(70)', 'NOT NULL'
exec sp_bindefault StringVacio, PasswdLrg
GO

alter table COMPANY alter column CM_PASSWRD PasswdLrg
GO

alter table DB_INFO alter column DB_PASSWRD PasswdLrg
GO

alter trigger TR_DB_INFO
on DB_INFO for update AS
begin
	set NOCOUNT on;

	declare @DB_CODIGO CodigoEmpresa;
	declare @DB_CONTROL Referencia;
	declare @DB_TIPO Status;
	declare @DB_DATOS Accesos;
	declare @DB_USRNAME UsuarioCorto;
	declare @DB_PASSWRD PasswdLrg;
	declare @DB_CTRL_RL Referencia;
	declare @DB_CHKSUM Accesos;

	declare @OldCodigo CodigoEmpresa;
	declare @NewCodigo CodigoEmpresa;
	select @OldCodigo = DB_CODIGO from Deleted;
	select @NewCodigo = DB_CODIGO from Inserted;

	select @DB_CODIGO = DB_CODIGO, @DB_CONTROL = DB_CONTROL, @DB_TIPO = DB_TIPO, @DB_DATOS= DB_DATOS,
	@DB_USRNAME = DB_USRNAME, @DB_PASSWRD = DB_PASSWRD, @DB_CTRL_RL = DB_CTRL_RL, @DB_CHKSUM = DB_CHKSUM
	from INSERTED;

	update COMPANY
	set DB_CODIGO = @NewCodigo, CM_CONTROL = (
		CASE
			WHEN @DB_TIPO = 0 THEN '3DATOS'
			WHEN @DB_TIPO = 1 THEN '3RECLUTA'
			WHEN @DB_TIPO = 2 THEN '3VISITA'
			WHEN @DB_TIPO = 3 THEN '3OTRO'
			WHEN @DB_TIPO = 4 THEN '3DATOS' -- CM_CONTROL igual a 3DATOS si el TIPO es 4 (3PRUEBA)
			WHEN @DB_TIPO = 5 THEN '3PRESUP'
			ELSE ''
		END
		),
		CM_DATOS = @DB_DATOS,
		CM_USRNAME = @DB_USRNAME,
		CM_PASSWRD = @DB_PASSWRD,
		CM_CTRL_RL  = (
		CASE
			WHEN @DB_TIPO = 0 THEN '3DATOS'
			WHEN @DB_TIPO = 1 THEN '3RECLUTA'
			WHEN @DB_TIPO = 2 THEN '3VISITA'
			WHEN @DB_TIPO = 3 THEN '3OTRO'
			WHEN @DB_TIPO = 4 THEN '3PRUEBA'
			WHEN @DB_TIPO = 5 THEN '3PRESUP'
			ELSE ''
		END
		),
		CM_CHKSUM = @DB_CHKSUM
		where DB_CODIGO = @OldCodigo
end
GO

alter trigger TR_COMPANY_DB_CODIGO
on COMPANY for insert, update AS
begin
	 set NOCOUNT on;

	 IF UPDATE( DB_CODIGO )
	 BEGIN
		declare @DB_TIPO Status;
		declare @DB_DATOS Accesos;
		declare @DB_USRNAME UsuarioCorto;
		declare @DB_PASSWRD PasswdLrg;
		declare @DB_CHKSUM Accesos;

		select @DB_TIPO = DB_TIPO, @DB_DATOS= DB_DATOS,
			@DB_USRNAME = DB_USRNAME, @DB_PASSWRD = DB_PASSWRD, -- @DB_CTRL_RL = DB_CTRL_RL,
			@DB_CHKSUM = DB_CHKSUM
			from DB_INFO WHERE DB_CODIGO IN (SELECT DB_CODIGO FROM INSERTED);

		update COMPANY
			set CM_CONTROL = (
			CASE
				WHEN @DB_TIPO = 0 THEN '3DATOS'
				WHEN @DB_TIPO = 1 THEN '3RECLUTA'
				WHEN @DB_TIPO = 2 THEN '3VISITA'
				WHEN @DB_TIPO = 3 THEN '3OTRO'
				WHEN @DB_TIPO = 4 THEN '3DATOS'
				WHEN @DB_TIPO = 5 THEN '3PRESUP'
				ELSE ''
			END
			),
			CM_DATOS = COALESCE (@DB_DATOS, ''),
			CM_USRNAME = COALESCE (@DB_USRNAME, ''),
			CM_PASSWRD = COALESCE (@DB_PASSWRD, ''),
			CM_CTRL_RL = (
			CASE
				WHEN @DB_TIPO = 0 THEN '3DATOS'
				WHEN @DB_TIPO = 1 THEN '3RECLUTA'
				WHEN @DB_TIPO = 2 THEN '3VISITA'
				WHEN @DB_TIPO = 3 THEN '3OTRO'
				WHEN @DB_TIPO = 4 THEN '3PRUEBA'
				WHEN @DB_TIPO = 5 THEN '3PRESUP'
				ELSE ''
			END
			),
			CM_CHKSUM = COALESCE (@DB_CHKSUM, '')
		where CM_CODIGO IN (SELECT CM_CODIGO FROM INSERTED)
	END
end
GO

exec SP_RECOMPILE DB_INFO
GO

exec SP_RECOMPILE COMPANY
GO

/* Patch 450: Crear SP SP_CREAR_VEMPL_BAJA 1/3 */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[SP_CREAR_VEMPL_BAJA]') AND xtype in (N'P'))
BEGIN
	DROP PROCEDURE SP_CREAR_VEMPL_BAJA
END
GO

/* Patch 450: Crear SP SP_CREAR_VEMPL_BAJA 2/3 */
CREATE PROCEDURE SP_CREAR_VEMPL_BAJA
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[VEMPL_BAJA]') AND xtype in (N'V'))
		DROP VIEW VEMPL_BAJA

	DECLARE @CM_DATOS Accesos;
	DECLARE @BD Accesos;
	DECLARE @QUERY NVARCHAR(MAX);
	DECLARE @CM_CODIGO Accesos;

	SET @BD = '';

	DECLARE DB_CURSOR CURSOR FOR 
	SELECT CM_DATOS, SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) AS BD,
	CM_CODIGO
	FROM COMPANY 
		WHERE CM_CONTROL IN ('3DATOS')
			AND SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) IN (SELECT Name COLLATE DATABASE_DEFAULT FROM sys.databases WHERE STATE = 0)

		OPEN DB_CURSOR

			FETCH NEXT FROM DB_CURSOR INTO @CM_DATOS, @BD, @CM_CODIGO
			IF @@FETCH_STATUS = 0 
			BEGIN
				SET @QUERY = 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ
							FROM [' + @BD + '].DBO.COLABORA where ( CB_ACTIVO = ''N'' )  
							and (  CB_FEC_BAJ < getdate() - 1 )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END

			WHILE @@FETCH_STATUS = 0
			BEGIN	
				SET @QUERY =  @QUERY + ' UNION ALL ' + 						 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ
							FROM [' + @BD + '].DBO.COLABORA where ( CB_ACTIVO = ''N'' )  
							and (  CB_FEC_BAJ < getdate() - 1 )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END 

	CLOSE DB_CURSOR;

	DEALLOCATE DB_CURSOR;

	SET @QUERY  = 'CREATE VIEW VEMPL_BAJA AS ' + @QUERY

	EXECUTE (@QUERY)
END
GO

/* Patch 450: Crear SP SP_CREAR_VEMPL_BAJA 3/3 */
EXECUTE SP_CREAR_VEMPL_BAJA
GO

/* Patch 450: Modificar SP WS_TERMINAL_HUELLASACTUALES 1/2 */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WS_TERMINAL_HUELLASACTUALES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
GO

/* Patch 450: Modificar SP WS_TERMINAL_HUELLASACTUALES 2/2 */
create PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
(
     @TE_CODIGO NumeroEmpleado
) as
begin
	SET NOCOUNT ON

	declare @Empleado integer;
	declare @Empresa CodigoEmpresa;


		Declare TmpEmpleadosBaja CURSOR for
        SELECT	TH.CM_CODIGO, TH.CB_CODIGO
		FROM	TERM_HUELLA AS TH 
		INNER JOIN EMP_BIO AS EB ON TH.CB_CODIGO = EB.CB_CODIGO AND TH.CM_CODIGO = EB.CM_CODIGO
		INNER JOIN VEMPL_BAJA AS VE ON VE.CB_CODIGO = TH.CB_CODIGO AND VE.CM_CODIGO = TH.CM_CODIGO 
		WHERE	(TH.TE_CODIGO = @TE_CODIGO)
		GROUP BY TH.CM_CODIGO, TH.CB_CODIGO, TH.TE_CODIGO

		Open TmpEmpleadosBaja
		fetch NEXT FROM TmpEmpleadosBaja 
		into @Empresa, @Empleado

		while @@FETCH_STATUS = 0
       	begin
			update TERM_HUELLA set BORRAR = 'S' where ( TE_CODIGO = @TE_CODIGO ) and ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa )
			fetch NEXT FROM TmpEmpleadosBaja 
			into @Empresa, @Empleado
        end 

		close TmpEmpleadosBaja 
    	deallocate TmpEmpleadosBaja 
    
end
GO

/* Patch : Agregar trigger sobre tabla COMPANY: TR_COMPANY_VEMPL_BAJA (1/2) */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[TR_COMPANY_VEMPL_BAJA]') AND xtype in (N'TR'))
	DROP TRIGGER TR_COMPANY_VEMPL_BAJA
GO

/* Patch : Agregar trigger sobre tabla COMPANY: TR_COMPANY_VEMPL_BAJA (2/2) */
CREATE TRIGGER [dbo].[TR_COMPANY_VEMPL_BAJA] ON [dbo].[COMPANY] AFTER INSERT, DELETE, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	IF UPDATE( CM_CODIGO ) OR UPDATE (CM_DATOS)
	BEGIN
		EXECUTE SP_CREAR_VEMPL_BAJA	
	END
	ELSE IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		EXECUTE SP_CREAR_VEMPL_BAJA	
	END
  
END
GO

/* Agrega Mensaje 10 */
IF (SELECT count(*) FROM MENSAJE where ( DM_CODIGO = 10 ) ) = 0 
begin
	insert into MENSAJE( DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO) values ( 10, 'Recolección de lecturas Offline', 10, 1440, 'S' )
end
GO

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (1/3) */
IF EXISTS (select * from sysobjects where id = object_id('WS_AGREGA_NUEVO_MSG') )
 DROP PROCEDURE WS_AGREGA_NUEVO_MSG
GO

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (2/3) */
CREATE PROCEDURE WS_AGREGA_NUEVO_MSG
AS
BEGIN
	set nocount on
	
	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx	
	
	declare ListaMensajes cursor for
	select distinct( TE_CODIGO ) from TERM_MSG
		
	open ListaMensajes
	fetch next from ListaMensajes into @Terminal
	while (@@Fetch_Status = 0)
	begin
		if ( select count(*) from TERM_MSG where ( DM_CODIGO = 10 ) and ( TE_CODIGO = @Terminal ) ) = 0
		begin 
             insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP, TM_NEXT) values (@Terminal, 10, 1440, Convert(datetime,'29991231'))
		end
		
		fetch next from ListaMensajes into @Terminal
	end
	close ListaMensajes
	deallocate ListaMensajes
end
GO 

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (3/3) */
EXEC Dbo.WS_AGREGA_NUEVO_MSG
go



/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 1/3 */
/* Patch 450 #Seq: 1*/
create procedure ACTUALIZA_DERECHOS
as
begin
    SET NOCOUNT ON;
    declare @Cuantos Integer;

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 732);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 732, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 144 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 733);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 733, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 734);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 734, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 735);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 735, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 145 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 736);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 736, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 566 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 737);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 737, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 566 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 738);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 738, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 566 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 739);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 739, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 566 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 740);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 740, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 566 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 741);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 741, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 742);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 742, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 743);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 743, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 744);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 744, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 745);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 745, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 746);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 746, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 

    select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 747);
    if ( @Cuantos = 0 )
       INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
        SELECT GR_CODIGO, CM_CODIGO, 747, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 87 and AX_DERECHO > 0 
end

GO
/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 2/3 */
/* Patch 450 #Seq: 1*/
execute ACTUALIZA_DERECHOS
GO
/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 3/3 */
/* Patch 450 #Seq: 1*/
DROP PROCEDURE ACTUALIZA_DERECHOS
GO

/* Timbrado de Nomina Notificaciones y advertencias */
if exists (select * from sysobjects where id = object_id(N'FN_NotificacionTimbrado_GetListaEmpresas') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_NotificacionTimbrado_GetListaEmpresas
go

create function FN_NotificacionTimbrado_GetListaEmpresas() 
returns  TABLE 
as
RETURN 
(
select distinct CM.CM_CODIGO, CM_NOMBRE, CM_PASSWRD, CM_USRNAME, CM_DATOS from COMPANY CM
join  ACCESO on  ACCESO.CM_CODIGO = CM.CM_CODIGO 
where CM_CTRL_RL in ( '3DATOS' , '' ) and  AX_NUMERO = 741 and AX_DERECHO > 0 
)

go

if exists (select * from sysobjects where id = object_id(N'FN_NotificacionTimbrado_GetListaContacto') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_NotificacionTimbrado_GetListaContacto
go
create function FN_NotificacionTimbrado_GetListaContacto(@CM_CODIGO CodigoEmpresa) 
returns TABLE 
as 
return ( 
	select US_CODIGO,US_NOMBRE, US_EMAIL from USUARIO  
		join GRUPO on GRUPO.GR_CODIGO  = USUARIO.GR_CODIGO 
		join ACCESO on ACCESO.GR_CODIGO = GRUPO.GR_CODIGO  
		where US_EMAIL <> ''  and US_ACTIVO = 'S'  and  AX_NUMERO = 741  and AX_DERECHO > 0  and ACCESO.CM_CODIGO = @CM_CODIGO 
 ) 

go
