/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 462 */
--
/* #16930 - Aplicar para actualizar estatus de empleados - WS_ACTUALIZA_STATUS_EMP  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_ACTUALIZA_STATUS_EMP' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP]
GO

/* #16930 - Aplicar para actualizar estatus de empleados - WS_ACTUALIZA_STATUS_EMP  */
CREATE  PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP] 
AS
BEGIN
	set nocount on
	
	declare @Empleado NumeroEmpleado
	declare @Empresa CodigoEmpresa
	declare @Activo booleano
	declare @FechaBaja fecha
	declare @IdNumero Integer
	
	declare EmpleadosHuellas cursor for
	select CM_CODIGO, CB_CODIGO, ID_NUMERO from EMP_BIO
		
	open EmpleadosHuellas
	fetch next from EmpleadosHuellas into @Empresa, @Empleado, @IdNumero
	while (@@Fetch_Status = 0)
	begin
		if exists( select * from VEMPL_BAJA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) )
		begin
			select @Activo = Coalesce( CB_ACTIVO, 'S' ), @FechaBaja = coalesce( CB_FEC_BAJ, '1899-12-30' ) from VEMPL_BAJA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado );
			update EMP_BIO set CB_ACTIVO = @Activo, CB_FEC_BAJ = @FechaBaja where ( ID_NUMERO = @IdNumero );		
		end
		fetch next from EmpleadosHuellas into @Empresa, @Empleado, @IdNumero
	end
	close EmpleadosHuellas
	deallocate EmpleadosHuellas
END		
go

/* #16930 Aplicar para actualizar estatus de empleados - WS_ACTUALIZA_STATUS_EMP  */
EXEC WS_ACTUALIZA_STATUS_EMP 
GO

/* #16930 Aplicar para actualizar estatus de empleados - WS_ACTUALIZA_STATUS_EMP  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_ACTUALIZA_STATUS_EMP' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP]
GO

/* #16929 Asignaci�n de n�meros biom�tricos congela el Sistema */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'SP_CREAR_VEMPL_BAJA' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_CREAR_VEMPL_BAJA]
GO

/* #16929 Asignaci�n de n�meros biom�tricos congela el Sistema */
CREATE PROCEDURE SP_CREAR_VEMPL_BAJA
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[VEMPL_BAJA]') AND xtype in (N'V'))
		DROP VIEW VEMPL_BAJA

	DECLARE @CM_DATOS Accesos;
	DECLARE @BD Accesos;
	DECLARE @QUERY NVARCHAR(MAX);
	DECLARE @CM_CODIGO Accesos;

	SET @BD = '';

	DECLARE DB_CURSOR CURSOR FOR 
	SELECT CM_DATOS, SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) AS BD,
	CM_CODIGO
	FROM COMPANY 
		WHERE CM_CONTROL IN ('3DATOS') 	
			AND SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) IN (SELECT Name COLLATE DATABASE_DEFAULT FROM sys.databases WHERE STATE = 0)

		OPEN DB_CURSOR

			FETCH NEXT FROM DB_CURSOR INTO @CM_DATOS, @BD, @CM_CODIGO
			IF @@FETCH_STATUS = 0 
			BEGIN
				SET @QUERY = 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ
							FROM [' + @BD + '].DBO.COLABORA WITH (NOLOCK) where ( CB_ACTIVO = ''N'' )  
							and (  CB_FEC_BAJ < getdate() - 1 )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END

			WHILE @@FETCH_STATUS = 0
			BEGIN	
				SET @QUERY =  @QUERY + ' UNION ALL ' + 						 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ
							FROM [' + @BD + '].DBO.COLABORA WITH (NOLOCK) where ( CB_ACTIVO = ''N'' )  
							and (  CB_FEC_BAJ < getdate() - 1 )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END 

	CLOSE DB_CURSOR;

	DEALLOCATE DB_CURSOR;

	SET @QUERY  = 'CREATE VIEW VEMPL_BAJA AS ' + @QUERY

	EXECUTE (@QUERY)
END
GO

/* #16929 Asignaci�n de n�meros biom�tricos congela el Sistema */
EXECUTE SP_CREAR_VEMPL_BAJA	
GO
