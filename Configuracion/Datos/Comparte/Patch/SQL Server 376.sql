/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Creaci�n de la Tabla PASSLOG */
/* Patch 377 # Seq: 1 Agregado */
CREATE TABLE PASSLOG (
       US_CODIGO USUARIO,
       PL_FOLIO FOLIOCHICO,
       PL_PASSWRD PASSWD )

/* Llave Primaria de la Tabla PASSLOG */
/* Patch 376 # Seq: 2 Agregado */
ALTER TABLE PASSLOG
       ADD CONSTRAINT PK_PASS_LOG PRIMARY KEY (US_CODIGO, PL_FOLIO)

/* Llave for�nea de PASSLOG hacia USUARIO*/
/* Patch 380 # Seq: 3 Agregado */
ALTER TABLE PASSLOG
       ADD CONSTRAINT FK_PASSLOG_USUARIO
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;

/****************************************/
/****************************************/
/****** Fin de Patch para COMPARTE ******/
/****************************************/
/****************************************/

