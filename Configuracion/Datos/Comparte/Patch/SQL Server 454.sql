/****************************************/
/****************************************/
/****** Patch para COMPARTE  ****************/
/****************************************/
/****************************************/

 

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('Tableros')   )
CREATE TABLE Tableros(
	TableroID FolioGrande IDENTITY(1,1) ,	
	TableroNombre Descripcion ,
	TableroDescrip Formula ,
	TableroActivo Booleano ,
 CONSTRAINT PK_Tableros PRIMARY KEY CLUSTERED 
(
	TableroID ASC
) ,
 CONSTRAINT AK_Tableros_Nombre UNIQUE NONCLUSTERED 
(	
	TableroNombre ASC
)
)

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('Dashlets')   )
CREATE TABLE Dashlets(
	DashletID FolioGrande IDENTITY(1,1) ,	
	DashletNombre Formula  ,
	DashletSubtitulo Formula  ,
	DashletQuery TextoLargo ,	
	DashletTipo   dbo.Status,
	DashletEstilo dbo.Status, 
	DashletFormato		Formula,  	
	DashletFormatoSub	Formula,
	DashletTendenciaDesc	Formula,	
	DashletIconos		dbo.Status,
 CONSTRAINT PK_Dashlets PRIMARY KEY CLUSTERED 
(
	DashletID ASC
), 
 CONSTRAINT AK_Dashlets_Nombre UNIQUE NONCLUSTERED 
(
	DashletNombre ASC
) 
) 

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('TableroDashlets')   )
CREATE TABLE TableroDashlets(
	TabDashID FolioGrande IDENTITY(1,1) ,
	TableroID FolioGrande ,
	DashletID FolioGrande ,
	TabDashRow Status ,
	TabDashCol Status ,
	TabDashWidth Status ,
	TabDashHeight Status ,
 CONSTRAINT PK_TableroDashlets PRIMARY KEY CLUSTERED 
(
	TabDashID ASC
) 
) 

GO

IF OBJECT_ID('FK_TableroDashlets_Dashlets' ) is NULL 
	ALTER TABLE dbo.TableroDashlets  WITH CHECK ADD  CONSTRAINT FK_TableroDashlets_Dashlets FOREIGN KEY(DashletID)
	REFERENCES dbo.Dashlets (DashletID)

GO

ALTER TABLE dbo.TableroDashlets CHECK CONSTRAINT FK_TableroDashlets_Dashlets
GO

IF OBJECT_ID('FK_TableroDashlets_Tableros' ) is NULL 
	ALTER TABLE dbo.TableroDashlets  WITH CHECK ADD  CONSTRAINT FK_TableroDashlets_Tableros FOREIGN KEY(TableroID)
	REFERENCES dbo.Tableros (TableroID)
	ON DELETE CASCADE
GO

ALTER TABLE dbo.TableroDashlets CHECK CONSTRAINT FK_TableroDashlets_Tableros
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('CompanyTableros')   )
CREATE TABLE CompanyTableros 
(
	CompanyTableroID FolioGrande IDENTITY(1,1), 
	CM_CODIGO CodigoEmpresa, 
	TableroID FolioGrande, 
	CompanyTableroOrden status, 

 CONSTRAINT PK_CompanyTableros PRIMARY KEY 
(
	CompanyTableroID  
) , 
 CONSTRAINT AK_CompanyTableros UNIQUE NONCLUSTERED 
(
	CM_CODIGO, TableroID
) 
)

GO 

IF OBJECT_ID('FK_CompanyTableros_Tablero' ) is NULL 
	ALTER TABLE dbo.CompanyTableros  WITH CHECK ADD  CONSTRAINT FK_CompanyTableros_Tablero FOREIGN KEY(TableroID)
	REFERENCES dbo.Tableros (TableroID)
	ON DELETE CASCADE

GO 

IF OBJECT_ID('FK_CompanyTableros_Company' ) is NULL 
	ALTER TABLE dbo.CompanyTableros  WITH CHECK ADD  CONSTRAINT FK_CompanyTableros_Company FOREIGN KEY(CM_CODIGO)
	REFERENCES dbo.COMPANY (CM_CODIGO)
	ON DELETE CASCADE
	ON UPDATE CASCADE 

GO 

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('RolTableros')   )
CREATE TABLE RolTableros 
(
	CompanyTableroID FolioGrande,
	RO_CODIGO Codigo	

CONSTRAINT PK_RolTableros PRIMARY KEY 
(
	CompanyTableroID, RO_CODIGO 
) 

)
GO 

IF OBJECT_ID('FK_RolTableros_CompanyTableros' ) is NULL 
	ALTER TABLE dbo.RolTableros  WITH CHECK ADD  CONSTRAINT FK_RolTableros_CompanyTableros FOREIGN KEY(CompanyTableroID)
	REFERENCES dbo.CompanyTableros (CompanyTableroID)
	ON DELETE CASCADE

GO 

IF OBJECT_ID('FK_RolTableros_Rol' ) is NULL 
	ALTER TABLE dbo.RolTableros  WITH CHECK ADD  CONSTRAINT FK_RolTableros_Rol FOREIGN KEY(RO_CODIGO)
	REFERENCES dbo.Rol (RO_CODIGO)
	ON DELETE CASCADE
	ON UPDATE CASCADE 

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id=object_id('Sesion')   )
CREATE TABLE Sesion ( 
	SesionID  FolioGrande IDENTITY(1,1) ,	
	US_CODIGO	Usuario,
	CM_CODIGO	CodigoEmpresa,
	CM_NIVEL0	Formula,  
	PE_TIPO		int, 
	PE_YEAR		int, 
	PE_NUMERO	int, 
	PE_MES		int, 
	FechaActiva	Fecha, 
	MesIni		Fecha, 
	MesFin		Fecha,

	CONSTRAINT PK_Sesion PRIMARY KEY CLUSTERED 
	(
	SesionID ASC
	)
) 

GO 


IF exists (SELECT * FROM sysobjects where id = object_id('V_Dashlets') )
	DROP VIEW V_Dashlets
GO

CREATE VIEW V_Dashlets
AS
SELECT	DashletID, DashletNombre, DashletSubtitulo, DashletTipo, DashletEstilo, DashletQuery,  
		case DashletTipo 
			when 1 then 'Listado' 
			when 2 then 'Gr�fica' 
			when 3 then 'Indicador' 
		 else '' 
		end  DashletTipoDesc,
		case DashletEstilo 
			when 1 then 'Columnas' 
			when 2 then 'Barras' 
			when 3 then 'Pastel' 
			else 
				'' 
		end   DashletEstiloDesc, 
		DashletFormato, DashletFormatoSub, DashletTendenciaDesc, 
		DashletIconos, 
		case DashletIconos  
			when 0 then 'Sin �conos' 
			when 1 then 'Enfoque positivo' 
			when 2 then 'Enfoque negativo' 			
			else 
				'' 
		end   DashletIconosDesc

FROM	Dashlets Dashlet


GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Tablero_Create') AND type IN ( N'P', N'PC' ))
	drop procedure Tablero_Create

go


CREATE PROCEDURE Tablero_Create(
	@TableroID 	FolioGrande OUTPUT,	
	@Nombre 	Descripcion,
	@Descrip 	Formula,
	@Activo 	Booleano )
AS
BEGIN
	INSERT INTO Tableros ( TableroNombre, TableroDescrip, TableroActivo )
	VALUES ( @Nombre, @Descrip, @Activo )
	
	SET @TableroID	= SCOPE_IDENTITY()
END

GO


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Tablero_Delete') AND type IN ( N'P', N'PC' ))
	drop procedure Tablero_Delete

go



CREATE PROCEDURE Tablero_Delete(
	@TableroID	FolioGrande )
AS
BEGIN
	DELETE	Tableros
	WHERE	TableroID = @TableroID
END

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Dashlet_Create') AND type IN ( N'P', N'PC' ))
	drop procedure Dashlet_Create

go


CREATE PROCEDURE Dashlet_Create (
	@DashletID	FolioGrande OUTPUT,	
	@Nombre 	Formula,
	@Subtitulo 	Formula,
	@Query 		TextoLargo,	
	@Tipo		Status,
	@Estilo		Status = 0 , 
	@Formato	Formula = '', 
	@FormatoSub Formula = '' , 
	@TendenciaDesc Formula = '' , 
	@Iconos		Status = 0 )
AS
BEGIN
	INSERT INTO Dashlets ( DashletNombre, DashletSubtitulo, DashletQuery, DashletTipo, DashletEstilo, DashletFormato, DashletFormatoSub , DashletIconos, DashletTendenciaDesc )
	VALUES (  @Nombre, @Subtitulo,  @Query, @Tipo, @Estilo,@Formato , @FormatoSub, @Iconos, @TendenciaDesc )
	
	SET @DashletID	= SCOPE_IDENTITY()
END


GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Dashlet_Update') AND type IN ( N'P', N'PC' ))
	drop procedure Dashlet_Update

go


CREATE PROCEDURE Dashlet_Update (
	@DashletID	FolioGrande,	
	@Nombre 	Formula,
	@Subtitulo 	Formula,
	@Query 		TextoLargo,	
	@Tipo		Status,
	@Estilo		Status = 0, 
	@Formato	Formula = '', 
	@FormatoSub Formula = '' , 
	@TendenciaDesc Formula = '' , 
	@Iconos		Status = 0  )
AS
BEGIN
	UPDATE  Dashlets 
	set DashletNombre = @Nombre, DashletQuery = @Query, DashletTipo= @Tipo, DashletEstilo =@Estilo,  DashletFormato = @Formato, DashletFormatoSub= @FormatoSub, DashletIconos = @Iconos, 
	DashletTendenciaDesc = @TendenciaDesc
	where DashletID = @DashletID	
END

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Dashlet_Delete') AND type IN ( N'P', N'PC' ))
	drop procedure Dashlet_Delete

go


CREATE PROCEDURE Dashlet_Delete (
	@DashletID	FolioGrande
 )
AS
BEGIN
	DELETE FROM  Dashlets where DashletID = @DashletID	
END

GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TableroDashlet_Create') AND type IN ( N'P', N'PC' ))
	drop procedure TableroDashlet_Create

go


CREATE PROCEDURE TableroDashlet_Create (
	@TabDashID 	FolioGrande OUTPUT,
	@TableroID 	FolioGrande,
	@Row 		FolioChico,
	@Col 		FolioChico,
	@Width 		FolioChico,
	@Height 	FolioChico,
	@DashletID 	FolioGrande )
AS
BEGIN
	INSERT INTO TableroDashlets ( TableroID, TabDashRow, TabDashCol, TabDashWidth, TabDashHeight, DashletID )
	VALUES ( @TableroID, @Row, @Col, @Width, @Height,  @DashletID )

	SET @TabDashID	= SCOPE_IDENTITY()
END

GO


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TableroDashlet_Update') AND type IN ( N'P', N'PC' ))
	drop procedure TableroDashlet_Update

go
CREATE PROCEDURE TableroDashlet_Update(
	@TabDashID 	FolioGrande,
	@Row 		FolioChico,
	@Col 		FolioChico,
	@DashletID 	FolioGrande )
AS
BEGIN
	UPDATE	TableroDashlets
	SET		TabDashRow	= @Row,
			TabDashCol	= @Col,
			DashletID	= @DashletID
	WHERE	TabDashID = @TabDashID
END

GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Tableros_ConsultaCombo') AND type IN ( N'P', N'PC' ))
	drop procedure Tableros_ConsultaCombo
go


CREATE PROCEDURE Tableros_ConsultaCombo(	@SesionID	FolioGrande , @lSoloListado Booleano = 'N'  )
AS
BEGIN

	SET NOCOUNT ON 

	if @lSoloListado = 'N'  
	begin 
		select Llave, Descripcion  from ( 
		SELECT 0 as Orden, '' as Llave, 'Todos' as Descripcion 
		UNION 
		select 1 as Orden, TableroID as Llave, TableroNombre as Descripcion from Tableros where TableroActivo = 'S' 
		) ComboTableros 
		order by Orden, Llave 
	end
	else
	begin 
		select TableroID as Llave, TableroNombre as Descripcion from Tableros where TableroActivo = 'S' 		
		order by Llave 
	end 

	
END 

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Tableros_GetListaTableros') AND type IN ( N'P', N'PC' ))
	drop procedure Tableros_GetListaTableros
go


CREATE PROCEDURE Tableros_GetListaTableros(   @SesionID    FolioGrande  )
AS
BEGIN

       declare @US_CODIGO Usuario 
       declare @CM_CODIGO CodigoEmpresa
       select @US_CODIGO = US_CODIGO, @CM_CODIGO = CM_CODIGO from Sesion where SesionID = @SesionID 

       set @US_CODIGO = coalesce( @US_CODIGO, 0 ) 
       set @CM_CODIGO = coalesce( @CM_CODIGO, '' ) 

       SELECT t.TableroID, t.TableroNombre, t.TableroDescrip, ct.CompanyTableroOrden as Orden FROM CompanyTableros ct  
       JOIN Tableros t on t.TableroID = ct.TableroID
       JOIN RolTableros rt on rt.CompanyTableroID = ct.CompanyTableroID
       JOIN ROL on ROL.RO_CODIGO = rt.RO_CODIGO 
       JOIN USER_ROL UR on UR.US_CODIGO = @US_CODIGO and UR.RO_CODIGO = ROL.RO_CODIGO 
       WHERE ct.CM_CODIGO = @CM_CODIGO and t.TableroActivo = 'S'
       GROUP BY t.TableroID, t.TableroNombre, t.TableroDescrip, ct.CompanyTableroOrden
       ORDER BY Orden 
       
END

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Tablero_ConsultaDashlets') AND type IN ( N'P', N'PC' ))
	drop procedure Tablero_ConsultaDashlets
go

CREATE PROCEDURE Tablero_ConsultaDashlets(
	@SesionID	FolioGrande,
	@TableroID	FolioGrande )
AS
BEGIN
	SELECT	TabDashID,  TabDashRow, TabDashCol, TabDashWidth, TabDashHeight,  TabDash.DashletID, DashletNombre, DashletSubtitulo, DashletTipo, DashletTipoDesc, DashletEstilo,  DashletEstiloDesc, DashletFormato, DashletFormatoSub, 
	DashletIconos, DashletIconosDesc, DashletTendenciaDesc
	FROM	TableroDashlets TabDash
				LEFT JOIN V_Dashlets Dashlet ON TabDash.DashletID = Dashlet.DashletID
	WHERE	TableroID = @TableroID
	ORDER BY TabDashRow, TabDashCol
END

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Sesion_Update') AND type IN ( N'P', N'PC' ))
	drop procedure Sesion_Update
go


/*** Procedimiento que siempre se ejecutara al cargar un tablero en Tress.exe ***/ 
CREATE PROCEDURE Sesion_Update (
	@SesionID	FolioGrande OUTPUT,
	@US_CODIGO	Usuario, 
	@CM_CODIGO	CodigoEmpresa, 	
	@PE_TIPO	FolioChico, 
	@PE_YEAR	FolioChico, 
	@PE_NUMERO	FolioChico, 
	@PE_MES		FolioChico, 
	@FechaActiva	Fecha, 
	@MesIni			Fecha, 	
	@MesFin			Fecha
	 )
AS
BEGIN 
	set nocount on 

	if ( @SesionID <= 0 ) 
	begin 
		insert into Sesion ( US_CODIGO, CM_CODIGO, PE_TIPO, PE_YEAR, PE_NUMERO, PE_MES,  FechaActiva, MesIni, MesFin, CM_NIVEL0 ) 	
		select @US_CODIGO, @CM_CODIGO, @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES,  @FechaActiva, @MesIni, @MesFin,  CM_NIVEL0 from 
		COMPANY where CM_CODIGO = @CM_CODIGO  

		SET @SesionID	= SCOPE_IDENTITY()
	end 
	else
	begin 
		update Sesion set 
			US_CODIGO = @US_CODIGO, 
			CM_CODIGO = @CM_CODIGO, 
			PE_TIPO	  = @PE_TIPO, 
			PE_YEAR   = @PE_YEAR, 
			PE_NUMERO = @PE_NUMERO,
			PE_MES    = @PE_MES, 
			FechaActiva = @FechaActiva, 
			MesIni = @MesIni, 
			MesFin = @MesFin, 
			CM_NIVEL0  = COMPANY.CM_NIVEL0 
		from Sesion, COMPANY 
		where COMPANY.CM_CODIGO = @CM_CODIGO and Sesion.SesionID = @SesionID 
		
	end 


END 

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Sesion_Delete') AND type IN ( N'P', N'PC' ))
	drop procedure Sesion_Delete
go


/*** Procedimiento que siempre se ejecutara al cerrar Tress.exe ***/ 
CREATE PROCEDURE Sesion_Delete (
	@SesionID	FolioGrande  ) 
as
BEGIN 
	SET NOCOUNT ON 
	delete from Sesion where SesionID = @SesionID 	
END 


GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'CompanyTableros_Create') AND type IN ( N'P', N'PC' ))
	drop procedure CompanyTableros_Create
go

CREATE PROCEDURE CompanyTableros_Create (
	@CompanyTableroID	FolioGrande OUTPUT,
	@CM_CODIGO CodigoEmpresa, 
	@TableroID FolioGrande, 
	@CompanyTableroOrden Status )
as 
begin 
	SET NOCOUNT ON 

	select @CompanyTableroID = CompanyTableroID from CompanyTableros where CM_CODIGO = @CM_CODIGO and TableroID = @TableroID 

	set @CompanyTableroID = coalesce( @CompanyTableroID , 0 ) 

	if ( @CompanyTableroID = 0 ) 
	begin 		
		INSERT INTO CompanyTableros ( CM_CODIGO, TableroID, CompanyTableroOrden  )
		VALUES ( @CM_CODIGO, @TableroID, @CompanyTableroOrden  )

		SET @CompanyTableroID	= SCOPE_IDENTITY()
	end 
end 

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'CompanyTableros_Update') AND type IN ( N'P', N'PC' ))
	drop procedure CompanyTableros_Update
go

CREATE PROCEDURE CompanyTableros_Update (
	@CompanyTableroID	FolioGrande,
	@CM_CODIGO CodigoEmpresa, 
	@TableroID FolioGrande, 
	@CompanyTableroOrden Status )
as 
begin 
	SET NOCOUNT ON 
	UPDATE CompanyTableros 
	set  CM_CODIGO = @CM_CODIGO, 
		 TableroID = @TableroID, 
		 CompanyTableroOrden = @CompanyTableroOrden
	where CompanytableroID = @CompanyTableroID
	
end 


GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'CompanyTableros_Delete') AND type IN ( N'P', N'PC' ))
	drop procedure CompanyTableros_Delete
go


CREATE PROCEDURE CompanyTableros_Delete (
	@CompanyTableroID	FolioGrande,
	@CM_CODIGO CodigoEmpresa, 
	@TableroID FolioGrande, 
	@CompanyTableroOrden Status )
as 
begin 
	SET NOCOUNT ON 
	DELETE FROM CompanyTableros  where CompanytableroID = @CompanyTableroID	
end 

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'CompanyTableros_Consulta') AND type IN ( N'P', N'PC' ))
	drop procedure CompanyTableros_Consulta
go


CREATE PROCEDURE CompanyTableros_Consulta (
	@CompanyTableroID	FolioGrande,
	@CM_CODIGO CodigoEmpresa, 
	@TableroID FolioGrande, 
	@CompanyTableroOrden Status )
as 
begin 
	SET NOCOUNT ON 
	SELECT CM_CODIGO, CM_NOMBRE, t.TableroID, t.TableroNombre FROM COMPANY , Tableros t 
	where t.TableroActivo = 'S' 
	order by CM_NOMBRE 
end 

GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'RolTableros_Set') AND type IN ( N'P', N'PC' ))
	drop procedure RolTableros_Set
go


CREATE PROCEDURE RolTableros_Set (
	@CM_CODIGO CodigoEmpresa, 
	@TableroID FolioGrande, 
	@RO_CODIGO Codigo, 
	@Activo Booleano  )
as 
begin 
	SET NOCOUNT ON 

	declare @CompanyTableroID FolioGrande 
	set @CompanyTableroID = 0 
	exec CompanyTableros_Create @CompanyTableroID output, @CM_CODIGO, @TableroID, 0


	IF @Activo = 'S' 
	begin 
		if (select count(*) from RolTableros where CompanyTableroID = @CompanyTableroID and RO_CODIGO = @RO_CODIGO ) = 0 
			insert into RolTableros (CompanyTableroID, RO_CODIGO) values ( @CompanyTableroID, @RO_CODIGO ) 		
		else		
			RAISERROR( 'Relaci�n ya existe', 16, 1 )
	end 
	else
	begin 
		delete from RolTableros where CompanyTableroID = @CompanyTableroID and RO_CODIGO = @RO_CODIGO
	end 
	
end 

GO
 
 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'RolTableros_Consulta') AND type IN ( N'P', N'PC' ))
	drop procedure RolTableros_Consulta
go


CREATE PROCEDURE RolTableros_Consulta ( @SesionID FolioGrande, @CM_CODIGO CodigoEmpresa, @TableroID FolioGrande,  @RO_CODIGO Codigo, @Pista Formula = '' )
as 
begin 
	SET NOCOUNT ON 
	
	SELECT ct.CM_CODIGO, cm.CM_NOMBRE, t.TableroNombre,  rt.RO_CODIGO, r.RO_NOMBRE  from RolTableros rt
	join CompanyTableros ct on rt.CompanyTableroID = ct.CompanyTableroID 
	join Tableros t on ct.TableroID = t.TableroID
	join COMPANY cm on ct.CM_CODIGO = cm.CM_CODIGO 
	join ROL r on rt.RO_CODIGO = r.RO_CODIGO 
	where cm.CM_CONTROL in ( '', '3DATOS')  
	and ( ct.CM_CODIGO = @CM_CODIGO or @CM_CODIGO = '' ) 
	and ( ct.TableroID = @TableroID or @TableroID = 0 ) 
	and ( rt.RO_CODIGO = @RO_CODIGO or @RO_CODIGO = '' )
	and ( r.RO_NOMBRE like '%'+@Pista+'%'  or t.TableroNombre like '%'+@Pista+'%' )  
	
end 

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'CompanyTableros_ConsultaEmpresasCombo') AND type IN ( N'P', N'PC' ))
	drop procedure CompanyTableros_ConsultaEmpresasCombo
go


CREATE PROCEDURE CompanyTableros_ConsultaEmpresasCombo (
	@SesionID FolioGrande, @lSololistado Booleano ='N' )
as 
begin 
	SET NOCOUNT ON 

	if ( @lSoloListado = 'N' ) 
	begin 
		select Llave, Descripcion  from ( 
		SELECT 0 as Orden, '0' as Llave, 'Todas' as Descripcion 
		UNION 
		SELECT 1 as Orden, CM_CODIGO as Llave, CM_NOMBRE as Descripcion FROM COMPANY	
			where CM_CONTROL in ( '', '3DATOS' ) 
		) ComboEmpresas 
		order by Orden, Descripcion 
	end 
	else
	begin 
		SELECT CM_CODIGO as Llave, CM_NOMBRE as Descripcion FROM COMPANY	
		where CM_CONTROL in ( '', '3DATOS' ) 
	end
end


GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TablerosInit') AND type IN ( N'P', N'PC' ))
	drop procedure TablerosInit 
go


create procedure TablerosInit
as 
begin 
set nocount on 
declare @DashletID 	FolioGrande	
declare @Ind_EmpAct	FolioGrande	
declare @Ind_HeadC 	FolioGrande	
declare @Ind_Bajas 	FolioGrande	
declare @Ind_Rotac 	FolioGrande	
declare @Ind_Perm 	FolioGrande	
declare @Ind_Inca 	FolioGrande	
declare @Ind_Vaca 	FolioGrande	

declare @Ind_Calculados 	FolioGrande	
declare @Ind_NetoPagar 		FolioGrande	
declare @Ind_PendTimbrar 	FolioGrande	

declare @Ind_Asist_1 	FolioGrande	
declare @Ind_Asist_2 	FolioGrande	
declare @Ind_Asist_3 	FolioGrande	


if ( select count(*) from Dashlets  ) > 0 
	return 

-- Dashlets indicadores 
exec Dashlet_Create @Ind_EmpAct output, 'Empleados activos', '', 'exec SP_INDICADOR_ROTACION_MES @MesIni, @MesFin, @CM_NIVEL0 , 1', 3, 0, '%.0n', '%.2f%%', 'al mes anterior', 1
exec Dashlet_Create @Ind_HeadC output, 'Headcount promedio', 'en el mes', 'exec SP_INDICADOR_ROTACION_MES @MesIni, @MesFin, @CM_NIVEL0 , 2', 3, 0, '%.0n', '%.2f%%'  , 'al mes anterior', 1
exec Dashlet_Create @Ind_Bajas output, 'Bajas', 'en el mes', 'exec SP_INDICADOR_ROTACION_MES @MesIni, @MesFin, @CM_NIVEL0 , 3', 3, 0 , '%.0n', '%.2f%%', 'al mes anterior', 2
exec Dashlet_Create @Ind_Rotac output, 'Rotaci�n', 'en el mes', 'exec SP_INDICADOR_ROTACION_MES @MesIni, @MesFin, @CM_NIVEL0 , 4', 3, 0, '%.2f%%', '%.2f%%' , 'al mes anterior' , 2

exec Dashlet_Create @Ind_Calculados output, 'Empleados calculados', 'en la n�mina', 'exec SP_INDICADOR_NOMINA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @CM_NIVEL0 , 1', 3, 0, '%.0n', '%.2f%%', 'vs n�mina anterior', 1
exec Dashlet_Create @Ind_NetoPagar output, 'Neto a pagar', 'en la n�mina',			'exec SP_INDICADOR_NOMINA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @CM_NIVEL0 , 2', 3, 0, '$%.2n', '%.2f%%', 'vs n�mina anterior', 1
exec Dashlet_Create @Ind_PendTimbrar output, 'Pendientes por timbrar', 'del mes',	'exec SP_INDICADOR_NOMINA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @CM_NIVEL0 , 3', 3, 0, '%.0n', '', 'Solo n�minas afectadas', 1


exec Dashlet_Create @Ind_Asist_1  output, 'Checaron asistencia (empleados)', 'al d�a', 'exec SP_INDICADOR_ASISTENCIA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @FechaActiva, @CM_NIVEL0, 1', 3, 0, '%.0n', '', '', 1
exec Dashlet_Create @Ind_Asist_2  output, 'Tiempo Extra autorizado', 'en la n�mina', 'exec SP_INDICADOR_ASISTENCIA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @FechaActiva, @CM_NIVEL0, 2', 3, 0, '%.2n hrs', '%.2f%%', 'vs n�mina anterior', 1
exec Dashlet_Create @Ind_Asist_3  output, 'Tiempo Extra por autorizar', 'en la n�mina', 'exec SP_INDICADOR_ASISTENCIA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @FechaActiva, @CM_NIVEL0, 3', 3, 0, '%.2n hrs', '%.2f%%', 'vs n�mina anterior', 1


-- Dashlets Graficas 
declare @Graf_Rotacion		FolioGrande 
declare @Graf_Incidencias	FolioGrande 
declare @Graf_Dif_Perceps	FolioGrande 
declare @Graf_Dif_TiempoExtra	FolioGrande 

declare @Graf_Asist_1	FolioGrande 
declare @Graf_Asist_2	FolioGrande 


exec Dashlet_Create @Graf_Rotacion output, 'Rotaci�n mensual', '', 'exec SP_DASHLET_ROTACION_COMPARACION_MES @MesIni, @MesFin, @CM_NIVEL0', 2, 1 
exec Dashlet_Create @Graf_Incidencias output, 'Incidencias de hoy','', 'EXEC SP_DASHLET_INCIDENCIAS_RH @FechaActiva, @CM_NIVEL0 ', 2, 3
exec Dashlet_Create @Graf_Dif_Perceps output, 'Top 5 diferencias entre n�mina actual y anterior (pesos)','', 'EXEC SP_DASHLET_NOMINA_DIFERENCIAS_PERCEPCIONES @PE_TIPO, @PE_YEAR, @PE_NUMERO, @CM_NIVEL0', 2, 1
exec Dashlet_Create @Graf_Dif_TiempoExtra output, 'Comparativo de pago a tiempo extra (pesos)','', 'EXEC SP_DASHLET_NOMINA_DIFERENCIAS_TIEMPOEXTRA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @CM_NIVEL0', 2, 2

exec Dashlet_Create @Graf_Asist_1 output, 'Asistencia al d�a (pren�mina)', '', 'EXEC SP_DASHLET_INCIDENCIAS_PRENOMINA @FechaActiva, @CM_NIVEL0 ', 2, 3
exec Dashlet_Create @Graf_Asist_2 output, 'Comparativo de tiempo extra en la pren�mina (horas)', '', 'EXEC SP_DASHLET_TIEMPOEXTRA_PRENOMINA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @CM_NIVEL0 ', 2, 1


-- Dashlet listados 
declare @Lista_MotivosBaja	FolioGrande 
declare @Lista_Incidencias	FolioGrande 
declare @Lista_SupervisoresTE	FolioGrande 
declare @Lista_Asist_1 FolioGrande
declare @Lista_Asist_2 FolioGrande

exec Dashlet_Create @Lista_MotivosBaja output, 'Motivos de baja','', 'EXEC SP_DASHLET_MOTIVOSBAJAS_MES @MesIni, @MesFin, @CM_NIVEL0 ', 1, 0
exec Dashlet_Create @Lista_Incidencias output, 'Incidencias','', 'EXEC SP_DASHLET_INCIDENCIAS_LISTA_RH @FechaActiva, @CM_NIVEL0 ', 1, 0
exec Dashlet_Create @Lista_SupervisoresTE output, 'Tiempo extra pagado por supervisor (pesos)','', 'EXEC SP_DASHLET_NOMINA_SUPERVISORES_TIEMPOEXTRA @PE_TIPO, @PE_YEAR, @PE_NUMERO, @CM_NIVEL0', 1, 0

exec Dashlet_Create @Lista_Asist_1 output, '% Asistencia por supervisor (al d�a)','', 'EXEC SP_DASHLET_ASISTENCIA_SUPERVISORES_INCIDENCIAS  @FechaActiva, @CM_NIVEL0', 1, 0
exec Dashlet_Create @Lista_Asist_2 output, 'Tiempo extra autorizado por supervisor en la pren�mina (horas)','', 'EXEC SP_DASHLET_ASISTENCIA_SUPERVISORES_TIEMPOEXTRA  @PE_TIPO, @PE_YEAR, @PE_NUMERO, @CM_NIVEL0', 1, 0



declare @TableroID_RH 	FolioGrande	
declare @TableroID_Nom	FolioGrande	
declare @TableroID_Asis	FolioGrande	
declare @TabDashID 	FolioGrande	

exec Tablero_Create @TableroID_RH output, 'Recursos Humanos' , 'Tablero de Recursos Humanos de versi�n oficial' , 'S' 
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 1, 1, 1, 1, @Ind_EmpAct
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 1, 2, 1, 1, @Ind_HeadC
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 1, 3, 1, 1, @Ind_Bajas 
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 1, 4, 1, 1, @Ind_Rotac 
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 2, 1, 2, 2, @Graf_Rotacion 
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 2, 3, 2, 2, @Graf_Incidencias 
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 4, 1, 2, 2, @Lista_MotivosBaja 
exec TableroDashlet_Create @TabDashID output, @TableroID_RH, 4, 3, 2, 2, @Lista_Incidencias 

exec Tablero_Create @TableroID_Asis output, 'Asistencia'   , 'Tablero de asistencia de versi�n oficial' , 'S' 
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 1, 1, 1, 1, @Ind_EmpAct
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 1, 2, 1, 1,  @Ind_Asist_1
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 1, 3, 1, 1,  @Ind_Asist_2 
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 1, 4, 1, 1,  @Ind_Asist_3 
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 2, 1, 2, 2, @Graf_Asist_1 
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 2, 3, 2, 2, @Graf_Asist_2 
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 4, 1, 2, 2, @Lista_Asist_1 
exec TableroDashlet_Create @TabDashID output, @TableroID_Asis, 4, 3, 2, 2, @Lista_Asist_2

exec Tablero_Create @TableroID_Nom output, 'N�mina' , 'Tablero de n�mina de versi�n oficial' , 'S' 
exec TableroDashlet_Create @TabDashID output, @TableroID_Nom, 1, 1, 1, 1, @Ind_EmpAct
exec TableroDashlet_Create @TabDashID output, @TableroID_Nom, 1, 2, 1, 1, @Ind_Calculados
exec TableroDashlet_Create @TabDashID output, @TableroID_Nom, 1, 3, 1, 1, @Ind_NetoPagar 
exec TableroDashlet_Create @TabDashID output, @TableroID_Nom, 1, 4, 1, 1, @Ind_PendTimbrar 
exec TableroDashlet_Create @TabDashID output, @TableroID_Nom, 2, 1, 4, 2, @Graf_Dif_Perceps 
exec TableroDashlet_Create @TabDashID output, @TableroID_Nom, 4, 1, 2, 2, @Graf_Dif_TiempoExtra 
exec TableroDashlet_Create @TabDashID output, @TableroID_Nom, 4, 3, 2, 2, @Lista_SupervisoresTE 

end 

go 

exec TablerosInit

go 

drop procedure TablerosInit

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TablerosGeneraRolDemo') AND type IN ( N'P', N'PC' ))
	drop procedure TablerosGeneraRolDemo
go



create procedure TablerosGeneraRolDemo ( @TableroID_Demo int ) 
as
begin 
	if ( select count(*) from ROL where RO_CODIGO = 'MIEMP' ) = 0 
	begin 
		insert into ROL (RO_CODIGO, RO_NOMBRE, RO_DESCRIP) values ( 'MIEMP', 'Tablero Mi Empresa', 'Usuarios de Tablero Mi Empresa' ) 

		insert into USER_ROL  (US_CODIGO, RO_CODIGO ) 
			select US_CODIGO,'MIEMP'  from Usuario where US_ACTIVO = 'S'  


		select row_number() over (order by CM_CODIGO ) Id,  CM_CODIGO  into #TmpEmpresas from COMPANY where CM_CONTROL = '3DATOS' or CM_CONTROL = '' 
		declare @n int 
		declare @i int 
		declare @sCM_CODIGO CodigoEmpresa 
	
		select @n = count(*) from #TmpEmpresas
		set @i=1 
	
		while @i <= @n 
		begin 
			select @sCM_CODIGO = CM_CODIGO from #TmpEmpresas where Id = @i 
			exec RolTableros_Set @sCM_CODIGO, @TableroID_Demo, 'MIEMP', 'S' 
			set @i= @i + 1 
		end 

		drop table #TmpEmpresas 
	
	end 

end 

GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TablerosInitDemo') AND type IN ( N'P', N'PC' ))
	drop procedure TablerosInitDemo
go


create procedure TablerosInitDemo as
begin 
if ( select count(*)  from Tableros where TableroNombre = 'Mi Empresa' ) > 0 
	return 

declare @Lista_Demo_1 FolioGrande
declare @Lista_Demo_2 FolioGrande
declare @Graf_Demo_1	FolioGrande 
declare @Graf_Demo_2	FolioGrande 

exec Dashlet_Create @Lista_Demo_1 output, 'Cumplea�eros de la semana','', 'EXEC SP_DASHLET_GENERAL_CUMPLEANOS   @CM_NIVEL0', 1, 0
exec Dashlet_Create @Lista_Demo_2 output, 'Aniversarios en la semana','', 'EXEC SP_DASHLET_GENERAL_ANIVERSARIOS @CM_NIVEL0', 1, 0

exec Dashlet_Create @Graf_Demo_1 output, 'Empleados por g�nero', '', 'EXEC SP_DASHLET_GENERAL_DEMOGRAFICO_GENERO @CM_NIVEL0 ', 2, 3
exec Dashlet_Create @Graf_Demo_2 output, 'Distribuci�n por edad','', 'EXEC SP_DASHLET_GENERAL_DEMOGRAFICO_EDAD   @CM_NIVEL0 ', 2, 4


declare @TableroID_Demo	FolioGrande	
declare @TabDashID 	FolioGrande	

exec Tablero_Create @TableroID_Demo output, 'Mi Empresa' , 'Tablero demogr�fico de versi�n' , 'S' 

exec TableroDashlet_Create @TabDashID output, @TableroID_Demo, 1, 1, 2, 3, @Graf_Demo_1 
exec TableroDashlet_Create @TabDashID output, @TableroID_Demo, 1, 3, 2, 3, @Graf_Demo_2 
exec TableroDashlet_Create @TabDashID output, @TableroID_Demo, 4, 1, 2, 2, @Lista_Demo_1 
exec TableroDashlet_Create @TabDashID output, @TableroID_Demo, 4, 3, 2, 2, @Lista_Demo_2
exec TablerosGeneraRolDemo @TableroID_Demo

end 

go 

exec TablerosInitDemo

go 

drop procedure TablerosInitDemo

go 

drop procedure TablerosGeneraRolDemo 

go 

