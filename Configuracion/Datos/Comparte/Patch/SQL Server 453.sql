/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/*	Patch 453: Tabla para registrar solicitudes de env�o de correo (1/3).
	Definici�n de nuevo dominio EmailAdd */
IF not exists (SELECT * FROM systypes where name like N'EmailAdd')
begin
	exec sp_addtype EmailAdd, 'varchar(320)', 'NOT NULL'
	exec sp_bindefault StringVacio, EmailAdd
end
go

/*	Patch 453: Tabla para registrar solicitudes de env�o de correo (2/3).
	Definici�n */
IF not exists (SELECT * FROM sysobjects where id = object_id(N'ENVIOEMAIL'))
begin
	create table ENVIOEMAIL(
		EE_FOLIO FolioGrande identity,		/* Folio */
		EE_TO TextoLargo,					/* Direcci�n de correo a quien va dirigido */
		EE_CC EmailAdd,						/* Con copia */
		EE_SUBJECT Accesos,					/* Asunto del correo */
		EE_BODY Memo,						/* Cuerpo del correo */
		EE_FROM_NA Accesos,					/* Nombre del remitente */
		EE_FROM_AD EmailAdd,				/* Direcci�n de correo del remitente */
		EE_ERR_AD EmailAdd,					/* Direcci�n de correo para errores */
		EE_SUBTYPE Status,					/* Subtipo de correo. 0: Mensaje de texto. 1: Formato html */
		EE_STATUS Status,					/* Estatus del registro */
		EE_ENVIADO Booleano,				/* Booleano para indicar si el correo fue enviado */
		EE_MOD_ADJ Booleano,				/* Booleano para indicar si el servicio modificar� el nombre del archivo adjunto */
		EE_FEC_IN Fecha,					/* Fecha de creaci�n de registro */
		EE_FEC_OUT Fecha,					/* Fecha de env�o de correo */
		EE_ATTACH Memo,						/* Archivo(s) adjunto(s) */
		EE_BORRAR Booleano,					/* Borrar archivo(s) adjunto(s) despu�s de enviar correo */
		EE_LOG Formula,						/* Bit�cora */
		EE_ENCRIPT Booleano					/* Booleano que indica si el cuerpo del mensaje est� encriptado */
	)
end
go

/*	Patch 453: Tabla para registrar solicitudes de env�o de correo (3/3).
	Llava primaria */
if exists (select name from sysobjects where name = 'ENVIOEMAIL')
begin
	if not exists (select name from sysobjects where name = 'PK_ENVIOEM')
	begin
		 alter table ENVIOEMAIL
         add constraint PK_ENVIOEM primary key (EE_FOLIO)
	end
end
go


/* Patch 453: Crear tabla para Calendario de Tareas 1/6 */
if not exists (select * from sysobjects where id = object_id(N'CALENDARIO'))
begin
 	  create table CALENDARIO(
    CA_FOLIO FolioGrande IDENTITY,        /* Folio del registro de la tarea */
    CA_NOMBRE Titulo,                     /* Nombre de la tarea */
    CA_DESCRIP Texto,                     /* Descripci�n larga de la tarea */
    CA_ACTIVO Booleano,                   /* Tarea activa */
    CM_CODIGO CodigoEmpresa,              /* C�digo de empresa */
    CA_REPORT Texto,                      /* C�digos de los reportes a generar */
    CA_REPEMP FolioChico,                 /* Reporte de empleados a los que se envian los reportes (OPCIONAL) */
    CA_FECHA Fecha,                       /* Fecha de inicio de la tarea */
    CA_HORA Hora,                         /* Hora en que se dispara la tarea */
    CA_FREC Status,                       /* Tipo de Frecuencia: 0=Diario, 1=Semanal,2=Mensual,3=Por hora,4=Especial */
    CA_RECUR FolioChico,                  /* Recurrencia (cada cuantos) para tipos de frecuencias: Diario, Semanal y Por Hora */
                                          /* Para tipo 'Especial', no se repite solo se aplica una vez en la fecha y hora de inicio */
    CA_DOWS Descripcion,                  /* Lista de d�as de la semana: 0=Domingo,1=Lunes,2=Martes,3=Miercoles,4=Jueves,5=Viernes,6=Sabado */
                                          /* ( Se captura una lista como de Lunes a Viernes: 1,2,3,4,5 ) Se usa con tipos Semanal y Mensual para recurrencia en el mes */
    CA_MESES Descripcion,                 /* Lista de meses: 1=Enero,2=Febrero,3=Marzo,4=Abril,5=Mayo,6=Junio,7=Julio,8=Agosto,9=Septiembre,10=Octubre,11=Noviembre,12=Diciembre */
    CA_MESDIAS Titulo,                    /* Si se indica lista de d�as del mes en que se aplica la tarea: 1,2,3,4,5,6,7....31 */
    CA_MES_ON Status,                     /* Se usa en tipo Mensual, en combinaci�n con CA_DOWS y CA_MESWEEK para indicar el d�a de la semana en que aplica: 0=Primero,1=Segundo,2=Tercero,3=Cuarto,4=Ultimo */
	CA_MESWEEK Descripcion,				  /* Se usa en tipo Mensual, en combinaci�n con CA_DOWS para indicar semanas del mes que se aplica la tarea: 0=Primero,1=Segundo,2=Tercero,3=Cuarto,4=Ultimo  */
    CA_FEC_ESP Fecha,                     /* Se usa en tipo Especial, para indicar una fecha default a utilizarse en reportes (se utiliza para calendarizar reportes retroactivos que se ejecutan una sola vez) */
    CA_TSALIDA Status,                    /* Default 0=Email, 1=Carpeta en disco */
    CA_FSALIDA NombreArchivo,             /* Si se especifica Carpeta en disco se indica la ruta de salida del archivo generado */
                                          /* Si no se especifica el archivo se genera en donde est� configurada la salida del reporte */
    CA_ULT_FEC Fecha,                     /* Contiene la �ltima fecha en la que se disparo la tarea */		
    CA_NX_FEC Fecha,                      /* Contiene la pr�xima fecha en que se dispara la tarea(1) */
    CA_NX_EVA Booleano,                   /* Reevaluar: Cuando la tarea ha sido aplicada o modificada se marca esta opci�n para que en el siguiente proceso de evaluaci�n se actualice la pr�xima fecha y hora en que se aplicar� */

    CA_US_CHG Usuario,                     /* Usuario que modific� la tarea la �ltima vez */
	CA_CAPTURA Fecha                       /* Fecha en la que se modifico la tarea */
	  )    	  
	  
	                                      /* (1) Existir� un proceso que a la media noche revisar� todas las tareas activas para evaluar cuando ser� su pr�xima ejecuci�n */
end
GO

/* Patch 453: Crear tabla para Calendario de Tareas 2/6 */
if exists (select name from sysobjects where name = 'CALENDARIO')
begin
	if not exists (select name from sysobjects where name = 'PK_CALEN')
	begin
		 alter table CALENDARIO
		 add constraint PK_CALEN primary key (CA_FOLIO)
	end
end
GO

/* Patch 453: Crear tabla para Calendario de Tareas 3/6 */
if exists (SELECT * FROM sys.indexes WHERE name='XAK1CALENDARIO' AND object_id = OBJECT_ID('CALENDARIO'))
begin
	DROP index XAK1CALENDARIO ON CALENDARIO
end
GO


/* Patch 453: Crear tabla para Calendario de Tareas 4/6 */
if exists (select name from sysobjects where name = 'CALENDARIO')
begin
	create index XAK1CALENDARIO on CALENDARIO                /*  Ordenamiento para facilitar la busqueda al ejecutar la tarea */
	( CA_NX_FEC )
end
GO

/* Patch 453: Crear tabla para Calendario de Tareas 5/6 */
if exists (SELECT * FROM sys.indexes WHERE name='XAK2CALENDARIO' AND object_id = OBJECT_ID('CALENDARIO'))
begin
	drop index XAK2CALENDARIO on CALENDARIO
end
GO

/* Patch 453: Crear tabla para Calendario de Tareas 6/6 */
if exists (select name from sysobjects where name = 'CALENDARIO')
begin
	create index XAK2CALENDARIO on CALENDARIO                /*  Indice �nico para impedir que agreguen tareas con el mismo nombre */
	( CA_NOMBRE )
end
GO

/* Patch 453: Crear tabla para suscripci�n de roles 1/2 */
if not exists (select * from sysobjects where id = object_id(N'ROLSUSCRIP'))
begin
    create table ROLSUSCRIP (
    RO_CODIGO Codigo,                     /* C�digo del Rol */
    CA_FOLIO FolioGrande,                 /* Folio del registro de la tarea */
    RS_ACTIVO Booleano,                   /* Suscripci�n activa del rol a la tarea */
    RS_VACIO Status,                      /* Que enviar cuando no hay datos: 0=Enviar Notificaci�n,1=No enviar Notificaci�n */
    RS_US_CHG Usuario                     /* Usuario que modific� la suscripci�n del rol */
    )
end
GO

/* Patch 453: Crear tabla para suscripci�n de roles 2/2 */
if exists (select name from sysobjects where name = 'ROLSUSCRIP')
begin
	if not exists (select name from sysobjects where name = 'PK_RSUSCRIP')
	begin
		alter table ROLSUSCRIP
		add constraint PK_RSUSCRIP primary key (RO_CODIGO, CA_FOLIO)
	end
end
GO

/* Patch 453: Crear tabla para suscripci�n de usuarios 1/2 */
if not exists (select * from sysobjects where id = object_id(N'ROLUSUARIO'))
begin
    create table ROLUSUARIO (
    US_CODIGO Codigo,                     /* C�digo del usuario */
    CA_FOLIO FolioGrande,                 /* Folio del registro de la tarea */
    RU_ACTIVO Booleano,                   /* Suscripci�n activa del usuario a la tarea */
    RU_VACIO Status,                      /* Que enviar cuando no hay datos: 0=Enviar Notificaci�n,1=No enviar Notificaci�n */
    RU_US_CHG Usuario                     /* Usuario que modific� la suscripci�n del rol */
    )
end
GO

/* Patch 453: Crear tabla para suscripci�n de usuarios 2/2 */
if exists (select name from sysobjects where name = 'ROLUSUARIO')
begin
	if not exists (select name from sysobjects where name = 'PK_USUSCRIP')
	begin
	        alter table ROLUSUARIO
		add constraint PK_USUSCRIP primary key (US_CODIGO, CA_FOLIO)
	end
end
GO

/* Patch 453: Crear funci�n de tabla FN_COMMALIST_TO_ROWS 1/2. */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'FN_COMMALIST_TO_ROWS')
BEGIN
	DROP FUNCTION FN_COMMALIST_TO_ROWS
END
GO

/* Patch 453: Crear funci�n de tabla FN_COMMALIST_TO_ROWS 2/2. */
CREATE FUNCTION FN_COMMALIST_TO_ROWS(@input AS Varchar(4000) )
RETURNS
      @Result TABLE(VALOR BIGINT)
AS
BEGIN
      DECLARE @str VARCHAR(20)
      DECLARE @ind Int
      IF(@input is not null)
      BEGIN
            SET @ind = CharIndex(',',@input)
            WHILE @ind > 0
            BEGIN
                  SET @str = SUBSTRING(@input,1,@ind-1)
                  SET @input = SUBSTRING(@input,@ind+1,LEN(@input)-@ind)
                  INSERT INTO @Result values (@str)
                  SET @ind = CharIndex(',',@input)
            END
            SET @str = @input
            INSERT INTO @Result values (@str)
      END
      RETURN
END
GO

/* Patch 453: Crear procedure SP_CALENDARIO para calendarizar tareas de ejecuci�n de reportes 1/2 */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'SP_CALENDARIO')
BEGIN
	DROP PROCEDURE SP_CALENDARIO
END
GO

/* Patch 453: Crear procedure SP_CALENDARIO para calendarizar tareas de ejecuci�n de reportes 2/2 */
create PROCEDURE SP_CALENDARIO
(
	@Folio FolioGrande = 0
) 
AS 
BEGIN
	 SET NOCOUNT ON 

	 /*
		Par�metros:
		1. �Folio de tabla Calendario?.
		Proceso:
		1. Leer tabla calendario.
		2. Solo registros activos.
		3. Clasificar por frecuencia (CA_FREC).
			a. Tipo de Frecuencia: 0=Diario, 1=Semanal,2=Mensual,3=Por hora,4=Especial.
			b. Almacenar el tipo de frecuencia en una variable (@FREC).
		4. Revisar recurrencia (CA_RECUR):
			a. Recurrencia (cada cuantos) para tipos de frecuencias: Diario, Semanal y Por Hora.
            b. Para tipo 'Especial', no se repite solo se aplica una vez en la fecha y hora de inicio.
		5. Si @FREC es semanal o mensual, revisar campo:
			a. CA_DOWS.
			b. Con esto se sabe qu� d�as aplicar para frecuencias semanal o mensual.
			c. Almancena los d�as de la semana: 0=Domingo,1=Lunes,2=Martes,3=Miercoles,4=Jueves,5=Viernes,6=Sabado.
			d. Almacenar en variable (@DOWS).
		6. Grabar fecha y hora de �ltima ejecuci�n.
			a. @ULT_FEC.
		7. Establecer fecha y hora de siguiente ejecuci�n en base a la frecuencia y dem�s campos, y en base a la fecha y hora de
			�ltima ejecuci�n.
			a. NOTA: CA_FECHA y CA_HORA son la fecha y hora de inicio de ejecuci�n de la tarea respectivamente.
				Estos valores se tomar�n en cuenta si fecha y hora de �ltima ejecuci�n est�n vac�os.
	 */

	DECLARE @FOLIO_ACTUAL FolioGrande;
	DECLARE @FREC Status;
	DECLARE @DOWS Descripcion;
	DECLARE @FECHA Fecha;
	DECLARE @HORA Hora;
	DECLARE @ULT_FEC Fecha;	

	DECLARE @NX_FEC Fecha;
	DECLARE @NX_HOR Hora;
	DECLARE @RECUR FolioChico;

	-- Para tareas semanales.
	DECLARE @ULTIMO_DIA_SEMANA INT;
	DECLARE @INDEX_DOWS INT;

	-- Tareas mensuales.
	DECLARE @MESES Descripcion;
	DECLARE @MESDIAS Titulo;
	DECLARE @MESWEEK Descripcion;
	DECLARE @INDEX INT;
	DECLARE @FECHA_TMP Fecha;
	
	IF @Folio = 0
	BEGIN
		 /*
			Si @Folio = 0 (valor default) procesar todos los registros activos y que necesiten evaluarse de nuevo (CA_NX_EVA = 'S').
		 */
		
		DECLARE DB_CURSOR CURSOR FOR
		SELECT CA_FOLIO, CA_FECHA, CA_HORA, CA_FREC, CA_DOWS, CA_ULT_FEC, CA_RECUR, CA_MESES, CA_MESDIAS, CA_MESWEEK
			FROM CALENDARIO
			WHERE 
				(CA_ACTIVO = 'S' AND CA_NX_EVA = 'S')				
				OR				
				(CA_ACTIVO = 'S' AND CAST (CA_NX_FEC AS date) <= CAST (CURRENT_TIMESTAMP AS date) )				

		-- Procesar cada registro activo.
		-- Establecer fecha y hora de siguiente ejecuci�n.
		OPEN DB_CURSOR

			FETCH NEXT FROM DB_CURSOR INTO @FOLIO_ACTUAL, @FECHA, @HORA, @FREC, @DOWS, @ULT_FEC, @RECUR, @MESES, @MESDIAS, @MESWEEK		

			WHILE @@FETCH_STATUS = 0
			BEGIN	
				 -- Si fecha y hora de �ltima ejecuci�n son vac�os, tomar fecha y hora de inicio de la tarea.
				 -- 1899-12-30 00:00:00.000

				 -- IF @ULT_FEC = '18991230'
				 -- BEGIN
					  SET @ULT_FEC = @FECHA;
				 -- END
				 -- ----- -----

			     -- Establecer fecha de siguiente ejecuci�n.
				 -- Iniciar con la fecha m�s horas y minutos de siguiente ejecuci�n.
				 SET @NX_HOR = @HORA;
				 -- Sin importar la hr que tenga @ULT_FEC, sustituir en @NX_FEC con la hr en @NX_HOR
				 SET @ULT_FEC = CONVERT (DATE, @ULT_FEC) -- DATEADD(dd, 0, DATEDIFF(dd, 0, @ULT_FEC))
				 SET @NX_FEC = DATEADD (HOUR, CAST (SUBSTRING (@NX_HOR, 1, 2) AS int), @ULT_FEC);
				 SET @NX_FEC = DATEADD (MINUTE, CAST (SUBSTRING (@NX_HOR, 3, 2) AS int), @NX_FEC);				 
				 
				 SET @INDEX_DOWS = 0;

				 -- Si es de frecuencia diaria, sumar cantidad de d�as a repetir a la fecha y hora de �ltima ejecuci�n.
				 -- 0=Diario
				 IF @FREC = 0
				 BEGIN					  
					  WHILE @NX_FEC < CURRENT_TIMESTAMP
					  BEGIN
						   SET @NX_FEC = @NX_FEC + @RECUR 						   
					  END		
				 END

				 -- Si es de frecuencia semanal. Revisar recurrencia y d�as de la semana.
				 -- 1=Semanal.
				 ELSE IF @FREC =  1
				 BEGIN
					  -- Tomar en cuenta que en el campo CA_DOWS se almacenan los d�as de la semana a ejecutar la tarea.
					  -- Se establecen del siguiente modo:
					  -- 0=Domingo,1=Lunes,2=Martes,3=Miercoles,4=Jueves,5=Viernes,6=Sabado.

					  WHILE @NX_FEC < CURRENT_TIMESTAMP
					  BEGIN
						   -- De la �ltima fecha de ejecuci�n obtener el n�mero de d�a de acuerdo a la lista mostrada:
						   -- 0=Domingo,1=Lunes,2=Martes,3=Miercoles,4=Jueves,5=Viernes,6=Sabado.
						   -- Obtener del campo CA_DOWS el �ltimo d�a de la semana que debe ejecutarse la tarea.
						   -- SET @ULTIMO_DIA_SEMANA = RIGHT(@DOWS,CHARINDEX(',',REVERSE(@DOWS))-1);
						   SELECT TOP 1 @ULTIMO_DIA_SEMANA = VALOR FROM FN_COMMALIST_TO_ROWS (@DOWS) ORDER BY VALOR DESC;

						   -- Obtener diferencia del @DOWS ---> @ULTIMO_DIA_SEMANA con el d�a de �ltima ejecuci�n.
						   -- IF @ULTIMO_DIA_SEMANA - DATEPART(WEEKDAY, @ULT_FEC) < 0
						   IF @ULTIMO_DIA_SEMANA - DATEPART(WEEKDAY, @NX_FEC) <= 0
						   BEGIN
								-- Significa que ya se ejecut� para el �ltimo d�a de la semana.
								-- La siguiente fecha de ejecuci�n debe establecerse con el campo CA_RECUR
								-- que indica cada cu�ntas semanas debe repetirse la tarea.
								SET @NX_FEC = DATEADD(wk, @RECUR, @ULT_FEC);
								-- Obtener lunes para la semana de la fecha generada en @NX_FEC.
								SET @NX_FEC = DATEADD(wk, DATEDIFF (wk, 0, @NX_FEC), 0);
								-- Ahora establecer fecha para el primer d�a de la semana en CA_DOWS
								SET @NX_FEC = DATEADD (D, (CAST ( (  SELECT TOP 1 VALOR FROM FN_COMMALIST_TO_ROWS (@DOWS) ORDER BY VALOR ASC  ) AS int)-2), @NX_FEC);
								
								-- Asignar horas y minutos.
								SET @NX_FEC = DATEADD (HOUR, CAST (SUBSTRING (@NX_HOR, 1, 2) AS int), @NX_FEC);
								SET @NX_FEC = DATEADD (MINUTE, CAST (SUBSTRING (@NX_HOR, 3, 2) AS int), @NX_FEC);
								SET @ULT_FEC = @NX_FEC;
						   END
						   ELSE
						   BEGIN
								-- Si la diferencia es mayor a cero significa que todav�a necesita ejecutarse para la
								-- misma semana de la �ltima fecha de ejecuci�n.
								-- Debe sumarse la cantidad de d�as que se indica en DOWS.								
								-- 
								-- IF @INDEX_DOWS > 0
								-- BEGIN
									 SET @NX_FEC = @NX_FEC + 1; ---> ??
								-- END
								
								WHILE CAST (DATEPART(WEEKDAY, @NX_FEC) AS VARCHAR) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@DOWS)) -- AND @INDEX_DOWS < 8								
								BEGIN
									SET @INDEX_DOWS = @INDEX_DOWS + 1; -- ---> ??
									SET @NX_FEC = @NX_FEC + 1;
								END
						   END
					  END
				 END

				 -- 2=Mensual
				 ELSE IF @FREC =  2
				 BEGIN
					  WHILE @NX_FEC < CURRENT_TIMESTAMP
					  BEGIN
						   -- �Es de d�as al mes o de d�as por semanas?
						   IF @MESDIAS <> ''
						   BEGIN
							    -- Es de d�as al mes.
							    -- Revisar si el mes y el d�a coinciden. Si no, avanzar en d�as hasta que la fecha coincida.
								-- Lista de meses almacenada en @MESES.
								-- Lista de d�as al mes almacenada en @MESDIAS.
								-- Importante, s� el d�a es 0 indica que se ejecutar� el �ltimo d�a del mes.														
								-- SET @NX_FEC = @NX_FEC + 1;
								SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);

								-- Repetir hasta estar en un d�a de ejecuci�n.
								-- WHILE DATEPART (DAY, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS))
								WHILE DATEPART (MONTH, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESES))
									OR DATEPART (DAY, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS))

									-- O mientras el d�a sea el �ltimo del mes pero no se encuentra en @MESDIAS.
									-- OR (@NX_FEC = EOMONTH (@NX_FEC) AND 0 NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS)))

								BEGIN
									 -- Pasar al siguiente d�a.									 
									 -- SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);

									 -- Si no coincide el mes.
									 IF DATEPART (MONTH, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESES))								
								     BEGIN
										  -- Primer d�a del siguiente mes.
										  SET @NX_FEC  = DATEADD(mm, DATEDIFF(mm, 0, DATEADD (MONTH, 1, @NX_FEC)), 0)

										  -- Asignar horas y minutos.
										  SET @NX_FEC = DATEADD (HOUR, CAST (SUBSTRING (@NX_HOR, 1, 2) AS int), @NX_FEC);
										  SET @NX_FEC = DATEADD (MINUTE, CAST (SUBSTRING (@NX_HOR, 3, 2) AS int), @NX_FEC);
										  -- ----- -----
									 END
									 -- Revisar si es el �ltimo d�a del mes y este se encuentre en @MESDIAS
									 -- ELSE IF 0 IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS)) AND DATEPART (DAY,@NX_FEC) = DATEPART (DAY,EOMONTH (@NX_FEC))
									 ELSE IF 0 IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS)) AND DATEPART (DAY,@NX_FEC) = DATEPART (DAY, (DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@NX_FEC)+1,0))))
									 BEGIN
										  BREAK;
									 END
									 -- Si no coincide el d�a
									 ELSE IF DATEPART (DAY, @NX_FEC) NOT IN (SELECT * FROM FN_COMMALIST_TO_ROWS(@MESDIAS))
									 BEGIN
										  -- SET @NX_FEC = @NX_FEC + 1;
										  SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);
									 END									 
								END
						   END
						   ELSE
						   BEGIN
								-- Es de d�as por semana.
								-- Crear tabla con los d�as que aplicar� la tarea.
								If (OBJECT_ID('tempdb..#TMP') Is Null)
								Begin
									 -- Drop Table #Temp
									 CREATE TABLE #TMP (FECHA DATE, NUM_DIA INT, SEMANA INT)

									 SET @INDEX = 1;
									 SET @FECHA_TMP = DATEADD(mm, DATEDIFF(mm, 0, CURRENT_TIMESTAMP), 0)								

									 -- Se llena la tabla temporal con los d�as que aplica de acuerdo a:
									 -- @MESES y @DOWS.
									 -- La lista en @MESWEEK se utilizar� despu�s al ir generando la fecha de siguiente ejecuci�n.
								
									 WHILE @INDEX <= 400
									 -- WHILE @INDEX <= 1000
									 BEGIN
											 IF DATEPART (MONTH, @FECHA_TMP) IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@MESES))
											 BEGIN
												 IF DATEPART (WEEKDAY, @FECHA_TMP) IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@DOWS))
												 BEGIN
													  INSERT INTO #TMP VALUES (@FECHA_TMP, DATEPART (WEEKDAY, @FECHA_TMP), 0)
													  -- Establecer n�mero de semana por tipo de d�a, mes y a�o.
													  UPDATE #TMP SET SEMANA = (SELECT COUNT(NUM_DIA) - 1 FROM #TMP
  														  WHERE NUM_DIA = DATEPART (WEEKDAY, @FECHA_TMP) AND DATEPART (MONTH, FECHA) = DATEPART (MONTH, @FECHA_TMP)
														  AND DATEPART (YEAR, FECHA) = DATEPART (YEAR, @FECHA_TMP)) WHERE FECHA = @FECHA_TMP
												  END
											 END

											 SET @INDEX = @INDEX + 1;
											 SET @FECHA_TMP = DATEADD (DAY, 1, @FECHA_TMP)
									 END
									 -- =============================================================================================================================
								End								

								-- IMPORTANTE.
								-- Analizar si la siguiente asignaci�n debe hacerse al principio antes de cada caso.
								-- Esto es porque no tiene sentido que @NX_FEC se iguale a la �ltima fecha de ejecuci�n.
								-- Es mejor que inicie en el momento actual.

								-- Cambiar @MESWEEK para incluir la �ltima semana si esta lista contiene el valor 4.
								IF '4' IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@MESWEEK))
								BEGIN
									SET @MESWEEK = @MESWEEK + ',3';
								END

								-- Consulta sobre la tabla temporal de acuerdo al valor en @NX_FEC.
								-- Repetir hasta que exista registro que coincida.
								SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);

								WHILE NOT EXISTS (
										SELECT FECHA, NUM_DIA, SEMANA FROM #TMP
											WHERE SEMANA  IN (SELECT VALOR FROM FN_COMMALIST_TO_ROWS(@MESWEEK))
											AND FECHA = CAST (@NX_FEC AS date)
									)
								BEGIN
									 SET @NX_FEC = DATEADD (DAY, 1, @NX_FEC);
								END								
						   END
					  END
				 END

				 
				 -- 3=Por hora
				 ELSE IF @FREC =  3
				 BEGIN
					  WHILE @NX_FEC < CURRENT_TIMESTAMP
					  BEGIN
						   SET @NX_FEC = DATEADD (HOUR, @RECUR, @NX_FEC);
						   SET @NX_HOR = SUBSTRING ((CONVERT (VARCHAR(23), @NX_FEC, 121)), 12, 2) + SUBSTRING ((CONVERT (VARCHAR(23), @NX_FEC, 121)), 15, 2)
					  END
				 END

				 -- Realizar actualizaci�n del registro.
				 -- UPDATE CALENDARIO SET CA_NX_FEC = @NX_FEC, CA_NX_EVA = 'N' WHERE CA_FOLIO = @FOLIO_ACTUAL				 
				 UPDATE CALENDARIO SET CA_NX_FEC = @NX_FEC, CA_NX_EVA = 'N' WHERE CA_FOLIO = @FOLIO_ACTUAL				 
				 -- Pasar al siguiente registro.
				 FETCH NEXT FROM DB_CURSOR INTO @FOLIO_ACTUAL, @FECHA, @HORA, @FREC, @DOWS, @ULT_FEC, @RECUR, @MESES, @MESDIAS, @MESWEEK
			END
		
		-- Cerrar DB_CURSOR.		
		CLOSE DB_CURSOR;
		DEALLOCATE DB_CURSOR;

	END
	ELSE
	BEGIN
		/*
			Si @Folio <> 0 procesar solamente ese registro
		*/
		SELECT @FECHA = CA_FECHA, @HORA = CA_HORA, @FREC = CA_FREC, @DOWS = CA_DOWS, @ULT_FEC = CA_ULT_FEC
		FROM CALENDARIO WHERE CA_FOLIO = @Folio
	END	
END
GO


/*	Patch 453: Tabla para registrar bit�cora de servicio de reportes (1/3).
	Definici�n */
if not exists (select * from sysobjects where id = object_id(N'BITSERVREP'))
begin
	CREATE TABLE BITSERVREP (
		  BI_FOLIO		        Int identity ( 1, 1),
		  CA_FOLIO				FolioGrande,
		  BI_FECHA				Fecha,
		  BI_HORA				HoraMinutosSegundos,
		  BI_TIPO				Status,
		  BI_TEXTO				Texto,
		  BI_DATA				Memo,
		  CAL_FOLIO				FolioGrande
	)
end
GO

/*	Patch 453: Tabla para registrar bit�cora de servicio de reportes (2/3).
	Llava for�nea hacia la tabla Calendario */
IF not EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_BITSERREP_CALENDARIO') AND parent_object_id = OBJECT_ID(N'BITSERVREP')
)
begin
	ALTER TABLE BITSERVREP  WITH NOCHECK ADD  CONSTRAINT FK_BITSERREP_CALENDARIO FOREIGN KEY(CA_FOLIO)
		REFERENCES CALENDARIO (CA_FOLIO)
end
GO

/*	Patch 453: Tabla para registrar bit�cora de servicio de reportes (3/3).
	Llava for�nea hacia la tabla Calendario */
IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_BITSERREP_CALENDARIO') AND parent_object_id = OBJECT_ID(N'BITSERVREP')
)
begin
	ALTER TABLE BITSERVREP NOCHECK CONSTRAINT FK_BITSERREP_CALENDARIO
end
GO

/* Agregar Tarea al Calendario */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_AGREGAR_CALENDARIO') AND type IN ( N'P', N'PC' ))
    drop procedure SP_AGREGAR_CALENDARIO
go

create procedure SP_AGREGAR_CALENDARIO( 
  @CA_NOMBRE Titulo
, @CM_CODIGO CodigoEmpresa
, @CA_DESCRIP Texto
, @CA_ACTIVO Booleano
, @CA_FREC   Status
, @CA_RECUR FolioChico
, @CA_HORA   Hora
, @CA_FECHA  Fecha
, @CA_DOWS Descripcion 
, @CA_MESES  Descripcion
, @CA_MESDIAS Titulo
, @CA_MESWEEK Descripcion
, @CA_MES_ON  Status 
, @CA_US_CHG  Usuario
, @CA_REPORT FolioGrande
, @CA_NX_FEC  Fecha 
, @CA_FSALIDA NombreArchivo
, @CA_REPEMP FolioChico
, @CA_TSALIDA Status
, @CA_FEC_ESP Fecha
, @CA_NX_EVA  Booleano 
, @CA_FOLIO      FolioGrande output 
, @SuscribirUsuario Usuario = 0 
, @CA_CAPTURA Fecha
) 
as
begin 
    set nocount on      

    INSERT INTO CALENDARIO
           (CA_NOMBRE
           ,CA_DESCRIP
           ,CA_ACTIVO
           ,CM_CODIGO
           ,CA_REPORT
           ,CA_REPEMP
           ,CA_FECHA
           ,CA_HORA
           ,CA_FREC
           ,CA_RECUR
           ,CA_DOWS
           ,CA_MESES
           ,CA_MESDIAS
           ,CA_MES_ON
           ,CA_MESWEEK
           ,CA_FEC_ESP
           ,CA_TSALIDA
           ,CA_FSALIDA
           ,CA_NX_FEC
           ,CA_NX_EVA
           ,CA_US_CHG
		   ,CA_CAPTURA)
     VALUES     
           ( @CA_NOMBRE
           , @CA_DESCRIP
           , @CA_ACTIVO
           , @CM_CODIGO
           , @CA_REPORT
           , @CA_REPEMP
           , @CA_FECHA
           , @CA_HORA
           , @CA_FREC
           , @CA_RECUR
           , @CA_DOWS
           , @CA_MESES
           , @CA_MESDIAS
           , @CA_MES_ON
           , @CA_MESWEEK
           , @CA_FEC_ESP
           , @CA_TSALIDA
           , @CA_FSALIDA
           , @CA_NX_FEC
           , @CA_NX_EVA
           , @CA_US_CHG
		   , @CA_CAPTURA)

    set @CA_FOLIO = @@IDENTITY 
     
    if ( @SuscribirUsuario > 0 )  and ( @CA_FOLIO > 0 ) 
    begin 
        INSERT INTO ROLUSUARIO ( CA_FOLIO, US_CODIGO, RU_ACTIVO, RU_VACIO, RU_US_CHG ) values ( @CA_FOLIO, @SuscribirUsuario, 'S' , 0,  @CA_US_CHG ) 
    end 

end

GO

/* Patch 453: #14793: Suscripci�n de reportes - Poder conectar el proceso de suscripci�n en un reporte (F7) con el nuevo mecanismo para lograr un proceso completo (1/2) */
IF EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('V_SUSCRIP') )
	DROP VIEW V_SUSCRIP
GO

/* Patch 453: #14793: Suscripci�n de reportes - Poder conectar el proceso de suscripci�n en un reporte (F7) con el nuevo mecanismo para lograr un proceso completo (2/2) */
create view V_SUSCRIP as 
select C.CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, C.CM_CODIGO,  C.CA_FREC, C.CA_HORA, C.CA_ULT_FEC, C.CA_REPORT, VS_TIPO='USUARIO', RO_CODIGO='', RO_NOMBRE='', US.US_CODIGO, US.US_NOMBRE, US.US_EMAIL from  CALENDARIO C
left join ROLUSUARIO UC on C.CA_FOLIO = UC.CA_FOLIO and UC.RU_ACTIVO = 'S' 
left join USUARIO US on US.US_CODIGO = UC.US_CODIGO 
union all 
select C.CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, C.CM_CODIGO,  C.CA_FREC, C.CA_HORA, C.CA_ULT_FEC, C.CA_REPORT, VS_TIPO ='ROL', RC.RO_CODIGO, ROL.RO_NOMBRE, US2.US_CODIGO, US2.US_NOMBRE, US2.US_EMAIL from  CALENDARIO C
left join ROLSUSCRIP RC on C.CA_FOLIO = RC.CA_FOLIO and RC.RS_ACTIVO = 'S' 
left join ROL on ROL.RO_CODIGO = RC.RO_CODIGO 
left join USER_ROL UR on UR.RO_CODIGO = ROL.RO_CODIGO 
left join USUARIO US2 on US2.US_CODIGO = UR.US_CODIGO
GO


if not exists (select * from sysobjects where id = object_id(N'CALENSOLI'))
begin
     CREATE TABLE CALENSOLI (
	    CAL_FOLIO         Int identity ( 1, 1),
        CA_FOLIO          FolioGrande,
        RE_CODIGO         FolioChico,
        CAL_FECHA         Fecha,
        CAL_INICIO        Fecha,
        CAL_FIN           Fecha,
        CAL_ESTATUS       Status,
        CAL_MENSAJ        Memo
    )
end
GO

IF not EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_CALENSOLI_CALENDARIO') AND parent_object_id = OBJECT_ID(N'CALENSOLI'))
begin
     ALTER TABLE CALENSOLI  WITH NOCHECK ADD  CONSTRAINT FK_CALENSOLI_CALENDARIO FOREIGN KEY(CA_FOLIO) REFERENCES CALENDARIO (CA_FOLIO)
end
GO

IF EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'FK_CALENSOLI_CALENDARIO') AND parent_object_id = OBJECT_ID(N'CALENSOLI'))
begin
     ALTER TABLE CALENSOLI NOCHECK CONSTRAINT FK_CALENSOLI_CALENDARIO
end
GO
