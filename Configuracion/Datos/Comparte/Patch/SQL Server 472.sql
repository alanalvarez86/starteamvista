/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 472 */

/* #16872 - Nuevo campo en tabla TERMINAL para indicar el tipo terminal 1/3 */
if not exists (select * from syscolumns where id=object_id('TERMINAL') and name='TE_ID')
	alter table TERMINAL add TE_ID FolioGrande null
GO

/* #16872 - Nuevo campo en tabla TERMINAL para indicar el tipo terminal 2/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERMINAL') and name='TE_ID')
	update TERMINAL set TE_ID = 1 where TE_ID is null
GO

/* #16872 - Nuevo campo en tabla TERMINAL para indicar el tipo terminal 3/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERMINAL') and name='TE_ID')
	alter table TERMINAL alter column TE_ID FolioGrande not null
GO

/* #16961 - Incidencia de palabra Huella por Plantilla 1/5 */
IF (SELECT count(*) FROM MENSAJE where ( DM_MENSAJE = 'Actualización Software Terminal' ) AND ( DM_CODIGO = 1 ) ) = 1 
begin
     update MENSAJE set  DM_MENSAJE = 'Actualización software terminal' where ( DM_MENSAJE = 'Actualización Software Terminal' ) AND ( DM_CODIGO = 1 )
end
GO

/* #16961 - Incidencia de palabra Huella por Plantilla 2/5 */
IF (SELECT count(*) FROM MENSAJE where ( DM_MENSAJE = 'Actualización de Propiedades de Terminal' ) AND ( DM_CODIGO = 5 ) ) = 1 
begin
     update MENSAJE set  DM_MENSAJE = 'Actualización de propiedades de terminal' where ( DM_MENSAJE = 'Actualización de Propiedades de Terminal' ) AND ( DM_CODIGO = 5 )
end
GO

/* #16961 - Incidencia de palabra Huella por Plantilla 3/5 */
IF (SELECT count(*) FROM MENSAJE where ( DM_MENSAJE = 'Recolección de lecturas Offline' ) AND ( DM_CODIGO = 10 ) ) = 1 
begin
     update MENSAJE set  DM_MENSAJE = 'Recolección de lecturas offline' where ( DM_MENSAJE = 'Recolección de lecturas Offline' ) AND ( DM_CODIGO = 10 )
end
GO

/* #16961 - Incidencia de palabra Huella por Plantilla 4/5 */
IF (SELECT count(*) FROM MENSAJE where ( DM_MENSAJE = 'Depuración de Huellas' or DM_MENSAJE = 'Depuracion de Huellas' ) AND ( DM_CODIGO = 6 ) ) = 1 
begin
     update MENSAJE set  DM_MENSAJE = 'Depuración de plantillas' where ( DM_MENSAJE = 'Depuración de Huellas' or DM_MENSAJE = 'Depuracion de Huellas'  ) AND ( DM_CODIGO = 6 )
end
GO

/* #16961 - Incidencia de palabra Huella por Plantilla 5/5 */
IF (SELECT count(*) FROM MENSAJE where ( DM_MENSAJE = 'Actualización de Huellas') AND ( DM_CODIGO = 4 ) ) = 1 
begin     
     update MENSAJE set  DM_MENSAJE = 'Actualización de plantillas' where ( DM_MENSAJE = 'Actualización de Huellas' ) AND ( DM_CODIGO = 4 )
end
GO

/* #16961 - Incidencia de palabra Huella por Plantilla 3/3 */
IF (SELECT count(*) FROM MENSAJE where ( DM_MENSAJE = 'Sincronización total de huellas') AND ( DM_CODIGO = 8 ) ) = 1 
begin     
     update MENSAJE set  DM_MENSAJE = 'Sincronización total de plantillas' where ( DM_MENSAJE = 'Sincronización total de huellas' ) AND ( DM_CODIGO = 8 )
end
GO

/* 16959 - Migrar derechos de Otros/Asistencia a Gafete y biométrico 1/3 */
create procedure ACTUALIZA_DERECHOS
as
begin
	   SET NOCOUNT ON;	
     declare @Cuantos Integer;
	
	   select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 770);
     if ( @Cuantos = 0 )
		    INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO) 
		       SELECT GR_CODIGO, CM_CODIGO, 770, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 686
end
GO

/* 16959 - Actualización de derecho de Otros/Asistencia a Gafete y biométrico 2/3 */
execute ACTUALIZA_DERECHOS
GO

/* 16959 - Actualización de derecho de Otros/Asistencia a Gafete y biométrico 3/3 */
DROP PROCEDURE ACTUALIZA_DERECHOS
GO


/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_IDENTIFICA' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_IDENTIFICA]
GO

create procedure WS_TERMINAL_IDENTIFICA
as
begin
     SELECT TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA, TE_ACTIVO, TE_XML, TE_LOGO, TE_FONDO, TE_ZONA_ES, TE_ID 
     FROM TERMINAL
end
GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HEARTBEAT' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HEARTBEAT]
GO
Create procedure WS_TERMINAL_HEARTBEAT ( @TerminalID NumeroEmpleado, @Version varchar(30), @IpAddress varchar(30), @TER_HOR datetime ) 
as 
begin 
	 SELECT TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA,TE_ZONA_ES, TE_ACTIVO, TE_XML, TE_LOGO, TE_FONDO, TE_ID 
	 FROM TERMINAL 
	 WHERE (TE_CODIGO = @TerminalID) 
end 
GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_SETHEARTBEAT' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_SETHEARTBEAT]
GO
Create procedure WS_TERMINAL_SETHEARTBEAT( @TerminalID NumeroEmpleado, @Version varchar(30), @IpAddress varchar(30), @TER_HOR datetime, @FPCOUNT int=0 )
as 
begin 
     UPDATE TERMINAL SET TE_BEAT = GETDATE(),TE_Version=@Version, TE_IP=@IpAddress, TE_TER_HOR = @TER_HOR, TE_HUELLAS = @FPCOUNT 
     WHERE (TE_CODIGO = @TerminalID) 
end

GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_GETPENDIENTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_GETPENDIENTE]
GO
CREATE PROCEDURE WS_TERMINAL_GETPENDIENTE( @TE_CODIGO NumeroEmpleado )
as
begin
	SET NOCOUNT ON
	DECLARE @TipoTerminal integer

	SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

	if ( @TipoTerminal = 5 )
	begin
		SELECT  TOP (1) M.DM_CODIGO
		FROM    TERMINAL AS T INNER JOIN
				TERM_MSG AS R ON T.TE_CODIGO = R.TE_CODIGO INNER JOIN
				MENSAJE AS M ON R.DM_CODIGO = M.DM_CODIGO
		WHERE     (T.TE_ACTIVO = 'S') AND (M.DM_ACTIVO = 'S') AND (R.TM_NEXT <= GETDATE()) AND (T.TE_CODIGO = @TE_CODIGO)
		and ( R.DM_CODIGO in ( 1, 4, 5, 6, 8, 10 ) )
		ORDER BY R.TM_NEXT, M.DM_ORDEN
	end
	else
	begin
		SELECT  TOP (1) M.DM_CODIGO
		FROM    TERMINAL AS T INNER JOIN
				TERM_MSG AS R ON T.TE_CODIGO = R.TE_CODIGO INNER JOIN
				MENSAJE AS M ON R.DM_CODIGO = M.DM_CODIGO
		WHERE     (T.TE_ACTIVO = 'S') AND (M.DM_ACTIVO = 'S') AND (R.TM_NEXT <= GETDATE()) AND (T.TE_CODIGO = @TE_CODIGO)
		ORDER BY R.TM_NEXT, M.DM_ORDEN
	end
end
GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLAFALTANTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLAFALTANTE]
GO
CREATE PROCEDURE WS_TERMINAL_HUELLAFALTANTE ( @TE_CODIGO NumeroEmpleado )
as
begin
	SET NOCOUNT ON
		DECLARE @Ayer DATETIME
		DECLARE @TipoTerminal integer
		SET @Ayer = GETDATE() - 1

		SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )
		
		IF @TipoTerminal = 5 
		BEGIN
			SELECT TOP 1 E.ID_NUMERO, H.HU_ID, R.TE_CODIGO, R.GP_CODIGO, E.CM_CODIGO, E.CB_CODIGO, H.HU_HUELLA, H.HU_INDICE
			FROM   TERM_GPO AS R
			INNER JOIN EMP_BIO AS E ON R.GP_CODIGO = E.GP_CODIGO AND E.IV_CODIGO = 0 
			INNER JOIN HUELLAGTI AS H ON E.CM_CODIGO = H.CM_CODIGO AND E.CB_CODIGO = H.CB_CODIGO AND E.IV_CODIGO = H.IV_CODIGO
			LEFT OUTER JOIN TERM_HUELLA AS TH ON TH.TE_CODIGO = @TE_CODIGO AND H.CM_CODIGO = TH.CM_CODIGO AND H.CB_CODIGO = TH.CB_CODIGO 
			AND H.HU_INDICE = TH.HU_INDICE AND H.IV_CODIGO = TH.IV_CODIGO
			WHERE  R.TE_CODIGO = @TE_CODIGO AND TH.TE_CODIGO IS NULL AND ( E.CB_ACTIVO = 'S' OR E.CB_FEC_BAJ >= @Ayer )
			AND H.HU_INDICE = 11
			ORDER BY E.CM_CODIGO, E.CB_CODIGO
		END
		ELSE
		BEGIN		
			SELECT TOP 1 E.ID_NUMERO, H.HU_ID, R.TE_CODIGO, R.GP_CODIGO, E.CM_CODIGO, E.CB_CODIGO, H.HU_HUELLA, H.HU_INDICE
			FROM   TERM_GPO AS R
			INNER JOIN EMP_BIO AS E ON R.GP_CODIGO = E.GP_CODIGO AND E.IV_CODIGO = 0 
			INNER JOIN HUELLAGTI AS H ON E.CM_CODIGO = H.CM_CODIGO AND E.CB_CODIGO = H.CB_CODIGO AND E.IV_CODIGO = H.IV_CODIGO
			LEFT OUTER JOIN TERM_HUELLA AS TH ON TH.TE_CODIGO = @TE_CODIGO AND H.CM_CODIGO = TH.CM_CODIGO AND H.CB_CODIGO = TH.CB_CODIGO 
			AND H.HU_INDICE = TH.HU_INDICE AND H.IV_CODIGO = TH.IV_CODIGO
			WHERE  R.TE_CODIGO = @TE_CODIGO AND TH.TE_CODIGO IS NULL AND ( E.CB_ACTIVO = 'S' OR E.CB_FEC_BAJ >= @Ayer )
			AND ( ( H.HU_INDICE > 0 ) and ( H.HU_INDICE < 11 ) )
			ORDER BY E.CM_CODIGO, E.CB_CODIGO
		END
end
GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLASOBRANTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLASOBRANTE]
GO
Create PROCEDURE WS_TERMINAL_HUELLASOBRANTE( @TE_CODIGO NumeroEmpleado ) 
as 
begin 
	SET NOCOUNT ON
	DECLARE @TipoTerminal integer

	SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

	IF @TipoTerminal = 5 
	BEGIN
		select * 
		from EMP_BIO 
		inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
		where TERM_HUELLA.BORRAR = 'S' 
		and ( TERM_HUELLA.TE_CODIGO = @TE_CODIGO ) and ( TERM_HUELLA.HU_INDICE = 11 ) 
	END
	ELSE
	BEGIN
		select * 
		from EMP_BIO 
		inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
		where TERM_HUELLA.BORRAR = 'S' 
		and TERM_HUELLA.TE_CODIGO =@TE_CODIGO and ( TERM_HUELLA.HU_INDICE > 0 and TERM_HUELLA.HU_INDICE < 11 ) 
	end
end
GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_BORRAHUELLA' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_BORRAHUELLA]
GO
create PROCEDURE WS_TERMINAL_BORRAHUELLA ( @TE_CODIGO NumeroEmpleado, @ID_NUMERO int, @BORRAR char(1) )
as 
begin 
	SET NOCOUNT ON
	DECLARE @TipoTerminal integer
	DECLARE @CM_CODIGO Codigoempresa 
	DECLARE @CB_CODIGO NumeroEmpleado 
	DECLARE @HU_INDICE usuario 
 
	SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

	SELECT @CM_CODIGO = Coalesce( CM_CODIGO, '' ), @CB_CODIGO = Coalesce( CB_CODIGO, 0 )
	FROM EMP_BIO 
	where ID_NUMERO = @ID_NUMERO;
 
 	IF @TipoTerminal = 5 
	BEGIN
		DELETE FROM TERM_HUELLA 
		WHERE  ( (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (TE_CODIGO = @TE_CODIGO) and ( BORRAR = @BORRAR ) and ( HU_INDICE = 11 ) )
	END
	ELSE
	BEGIN
		DELETE FROM TERM_HUELLA 
		WHERE  ( (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (TE_CODIGO = @TE_CODIGO) and ( BORRAR = @BORRAR ) and ( HU_INDICE > 0 ) and ( HU_INDICE < 11 ) )
	END
	
	select @CM_CODIGO as Empresa, @CB_CODIGO as Empleado
end
GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLASACTUALES' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
GO
create PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
(
     @TE_CODIGO NumeroEmpleado
) as
begin
	SET NOCOUNT ON

	declare @Empleado integer;
	declare @Empresa CodigoEmpresa;
	declare @Ayer datetime
	DECLARE @TipoTerminal integer
	SET @Ayer = GETDATE() - 1

	SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

 	IF @TipoTerminal = 5 
	BEGIN
		Declare TmpEmpleadosBaja CURSOR for
        SELECT	TH.CM_CODIGO, TH.CB_CODIGO
		FROM	TERM_HUELLA AS TH 
		INNER JOIN EMP_BIO AS EB ON TH.CB_CODIGO = EB.CB_CODIGO AND TH.CM_CODIGO = EB.CM_CODIGO
		WHERE	(TH.TE_CODIGO = @TE_CODIGO) AND ( EB.CB_ACTIVO = 'N' AND EB.CB_FEC_BAJ >= @Ayer ) 
		and ( TH.HU_INDICE = 11 )
		GROUP BY TH.CM_CODIGO, TH.CB_CODIGO, TH.TE_CODIGO

		Open TmpEmpleadosBaja
		fetch NEXT FROM TmpEmpleadosBaja 
		into @Empresa, @Empleado

		while @@FETCH_STATUS = 0
       	begin
			update TERM_HUELLA set BORRAR = 'S' where ( TE_CODIGO = @TE_CODIGO ) and ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa )
			fetch NEXT FROM TmpEmpleadosBaja 
			into @Empresa, @Empleado
        end 

		close TmpEmpleadosBaja 
    	deallocate TmpEmpleadosBaja 
	END
	ELSE
	BEGIN
		Declare TmpEmpleadosBaja CURSOR for
        SELECT	TH.CM_CODIGO, TH.CB_CODIGO
		FROM	TERM_HUELLA AS TH 
		INNER JOIN EMP_BIO AS EB ON TH.CB_CODIGO = EB.CB_CODIGO AND TH.CM_CODIGO = EB.CM_CODIGO
		WHERE	(TH.TE_CODIGO = @TE_CODIGO) AND ( EB.CB_ACTIVO = 'N' AND EB.CB_FEC_BAJ >= @Ayer ) 
		and ( ( TH.HU_INDICE > 0 ) and ( TH.HU_INDICE < 11 ) )
		GROUP BY TH.CM_CODIGO, TH.CB_CODIGO, TH.TE_CODIGO

		Open TmpEmpleadosBaja
		fetch NEXT FROM TmpEmpleadosBaja 
		into @Empresa, @Empleado

		while @@FETCH_STATUS = 0
       	begin
			update TERM_HUELLA set BORRAR = 'S' where ( TE_CODIGO = @TE_CODIGO ) and ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa )
			fetch NEXT FROM TmpEmpleadosBaja 
			into @Empresa, @Empleado
        end 

		close TmpEmpleadosBaja 
    	deallocate TmpEmpleadosBaja 
	END
end
GO

/* #16873 - Se requiere que el WebService de Asistencia no envié plantillas faciales a una terminal de huellas y viceversa  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_SETAGREGAHUELLA' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_SETAGREGAHUELLA]
GO
CREATE PROCEDURE WS_TERMINAL_SETAGREGAHUELLA ( @TE_CODIGO NumeroEmpleado, @HU_ID INT )
AS
BEGIN
	SET NOCOUNT ON   
   DECLARE @P1 CodigoEmpresa;
   DECLARE @P2 NumeroEmpleado;
   DECLARE @P3 integer;
   DECLARE @LlaveAUpdate integer;
   DECLARE @TipoTerminal integer

    SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

 	IF @TipoTerminal = 5 
	BEGIN
		select @P1 = CM_CODIGO, @P2 = CB_CODIGO, @p3 = HU_INDICE from HUELLAGTI where ( HU_ID = @HU_ID ) and ( HU_INDICE = 11 )
	END
	ELSE
	BEGIN   
		select @P1 = CM_CODIGO, @P2 = CB_CODIGO, @p3 = HU_INDICE from HUELLAGTI where ( HU_ID = @HU_ID ) and ( ( HU_INDICE > 0 ) and ( HU_INDICE < 11 ) )
	END

   IF EXISTS(SELECT *
             FROM TERM_HUELLA
         WHERE CM_CODIGO = @P1
         AND CB_CODIGO = @P2
         AND HU_INDICE = @P3
         AND TE_CODIGO = @TE_CODIGO)
  BEGIN
    SELECT @LlaveAUpdate = LLAVE FROM TERM_HUELLA WHERE ( CM_CODIGO = @P1 ) AND ( CB_CODIGO = @P2 ) AND ( HU_INDICE = @P3 ) AND ( TE_CODIGO = @TE_CODIGO )
    UPDATE TERM_HUELLA
    SET CM_CODIGO = @P1, CB_CODIGO = @P2, HU_INDICE = @P3, TE_CODIGO = @TE_CODIGO
    WHERE LLAVE = @LlaveAUpdate
  END
  ELSE
  BEGIN
    INSERT INTO TERM_HUELLA(CM_CODIGO, CB_CODIGO, HU_INDICE, TE_CODIGO)
    SELECT CM_CODIGO, CB_CODIGO, HU_INDICE, @TE_CODIGO
    FROM HUELLAGTI
    WHERE HU_ID = @HU_ID
  END
END
GO

/* #16959 - Se requiere identificar si el empleado tiene plantilla de huella y/o facial */
if exists (select * from sysobjects where id = object_id(N'TR_DELETE_HUELLAGTI') and xtype in (N'TR')) 
	DROP TRIGGER [dbo].[TR_DELETE_HUELLAGTI]
GO

CREATE TRIGGER [dbo].[TR_DELETE_HUELLAGTI]
on [dbo].[HUELLAGTI]
AFTER DELETE
as
begin
     set nocount on

     declare @HU_ID NumeroEmpleado
	 declare @Empresa CodigoEmpresa
	 declare @Empleado NumeroEmpleado
	 declare @HU_INDICE NumeroEmpleado

     select @HU_ID = HU_ID, @Empresa = CM_CODIGO, @Empleado = CB_CODIGO, @HU_INDICE = HU_INDICE from deleted;

     delete from COM_HUELLA where HU_ID = @HU_ID;

	 if ( @HU_INDICE = 11 )
	 begin
			UPDATE TERM_HUELLA set BORRAR ='S' where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) and ( HU_INDICE = @HU_INDICE );
			UPDATE TERM_MSG SET TM_NEXT = '18991230' WHERE ( DM_CODIGO = 6 ) and ( TE_CODIGO in ( select TE_CODIGO from TERMINAL where ( TE_ID = 5 ) ) )
	 end
	 else
	 begin
			UPDATE TERM_HUELLA set BORRAR ='S' where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) and ( HU_INDICE < 11 );
			UPDATE TERM_MSG SET TM_NEXT = '18991230' WHERE ( DM_CODIGO = 6 ) and ( TE_CODIGO in ( select TE_CODIGO from TERMINAL where ( TE_ID <> 5 ) ) )
	 end
	 
end
GO
