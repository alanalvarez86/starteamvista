/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Agregar tabla BITACORA: No es reporteable ( Sug. 839 ) */
/* Patch 405 # Seq: 1 Agregado */
CREATE TABLE BITACORA (
       US_CODIGO            Usuario,
       BI_FECHA             Fecha,
       BI_HORA              HoraMinutosSegundos,
       BI_PROC_ID           Status,
       BI_TIPO              Status,
       BI_NUMERO            FolioGrande,
       BI_TEXTO             Observaciones,
       CB_CODIGO            NumeroEmpleado,
       BI_DATA              Memo,
       BI_CLASE             Status,
       BI_FEC_MOV           Fecha
)
GO

/* BITACORA: Indice por # de bit�cora ( Sug. 839 ) */
/* Patch 405 # Seq: 2 Agregado */
CREATE INDEX XIE1BITACORA ON BITACORA
(
       BI_NUMERO
)
GO

/* BITACORA: Indice por campos Fecha y hora ( Sug. 839 ) */
/* Patch 405 # Seq: 3 Agregado */
CREATE INDEX XIE2BITACORA ON BITACORA
(
       BI_FECHA,
       BI_HORA
)
GO

/* ACCESO: Inicializar derechos sobre Wizard de Importar Movimientos( Sug. 876 ) */
/* Patch 405 # Seq: 4 Agregado */
update ACCESO set AX_DERECHO = 3 where AX_NUMERO = 98 and AX_DERECHO = 1
GO

/* Patch 405 # Seq: 8 Modificado */
ALTER TRIGGER TD_USUARIO ON USUARIO AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
     declare @OldUS_CODIGO Usuario;
     select @OldUS_CODIGO = US_CODIGO from Deleted;
     delete from PAGINA where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
END
GO

/* Patch 405 # Seq: 10 Modificado */
ALTER TRIGGER TD_PAGINA ON PAGINA AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
     declare @OldUS_CODIGO Usuario;
     declare @OldPA_ORDEN FolioChico;
     select @OldUS_CODIGO = US_CODIGO, @OldPA_ORDEN = PA_ORDEN from Deleted;
     delete from CONTIENE where ( CONTIENE.US_CODIGO = @OldUS_CODIGO ) and
                                ( CONTIENE.PA_ORDEN = @OldPA_ORDEN );
END
GO

/****************************************/
/***** Actualizar versi�n de Datos ******/
/****************************************/

update CLAVES set CL_TEXTO = '405' where ( CL_NUMERO = 100 )
GO

/****************************************/
/****************************************/
/****** Fin de Patch para COMPARTE ******/
/****************************************/
/****************************************/

