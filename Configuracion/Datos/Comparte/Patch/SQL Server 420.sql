/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   Base De Datos COMPARTE   $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* Tabla ACC_ARBOL: es la 'Foto' del Arbol de Derechos, debe de contener los datos de los Arboles de Tress, Visitantes, Seleccion, WorkFlow*/
/* Patch 420 # Seq: 1 Agregado */
CREATE TABLE ACC_ARBOL (			--Descripcion: Arbol de Accesos
	AA_SOURCE	 Status NOT NULL,		--Fuente de donde proviene el derecho de acceso, va en orden de prioridad:
						--0=Tress,1=Clasificacion Adicional, 2=Clasificación de Reportes,3=Entidades de Reportes,
						--4=Seleccion de Personal,5=Visitantes, 6=WorkFlow
	AX_NUMERO  FolioChico NOT NULL ,	--Numero de derecho de acceso:
						--debe de coincidir con el derecho de acceso del arbol (Nodo.ImageIndex, unidad  TZetaEditAccesos.AddElemento( ))
						--y el tambien coincide con el almacenado en COMPARTE.ACCESO.AX_NUMERO
	AA_DESCRIP NombreArchivo NOT NULL ,		--Descripcion del derecho de acceso, debe de coincidir con el nombre de la rama
						--(Nodo.Text, unidad  TZetaEditAccesos.AddElemento( ))
	AA_VERSION FolioChico NOT NULL ,	--Version en que se agregó el derecho de acceso
	AA_POSICIO FolioChico NOT NULL ,	--Posicion del derecho de acceso dentro del arbol.
						--(Sirve para que en el reporte se pueda ordenar de la misma manera en que se ve en IU)
	AA_MODULO  Status NOT NULL ,		--Modulo al que pertence el derecho de acceso
	AA_SCREEN  Status NOT NULL ,		--Tipo de Pantalla a la que se refiere el derecho de acceso:
					--0=Pantalla,1=Registro,2=Proceso,3=Proceso Anual,4=Clasificación Adicional,5=Clasificación de Reportes,6=Entidades de Reportes
	AA_INGLES	 NombreArchivo NOT NULL		--Descripcion en ingles. En caso de que algun cliente quiera usar esta descripcion, tiene que capturarla directamente en el Query Analizer.
			
)
GO

/* Tabla ACC_ARBOL: es la 'Foto' del Arbol de Derechos, debe de contener los datos de los Arboles de Tress, Visitantes, Seleccion, WorkFlow*/
/* Patch 420 # Seq: 2 Agregado */
ALTER TABLE ACC_ARBOL
       ADD CONSTRAINT PK_ACC_ARBOL PRIMARY KEY (AA_SOURCE,AX_NUMERO)
GO

/* Tabla ACC_DER: Esta tabla contiene los derechos que se pueden asignar a cada rama, debe de contener los datos de los Arboles de Tress, Visitantes, Seleccion, WorkFlow*/
/* Patch 420 # Seq: 3 Agregado */
CREATE TABLE ACC_DER (			--Descripcion: Ramas del arbol de derechos
	AA_SOURCE	 Status NOT NULL,		--Fuente de donde proviene el derecho de acceso, va en orden de prioridad:
						--0=Tress,1=Clasificacion Adicional, 2=Clasificación de Reportes,3=Entidades de Reportes,
						--4=Seleccion de Personal,5=Visitantes, 6=WorkFlow
	AX_NUMERO  FolioChico NOT NULL ,	--Numero de derecho de acceso:
						--debe de coincidir con el derecho de acceso del arbol (Nodo.ImageIndex, unidad  TZetaEditAccesos.AddElemento( ))
						--y el tambien coincide con el almacenado en COMPARTE.ACCESO.AX_NUMERO
	AD_TIPO  Status NOT NULL ,		--Tipo de Derecho, indica la posicion que tiene dentro del Nodo padre el derecho, tiene el valor del numerado:
						--  eDerecho = ( {0}edConsulta, {1}edAlta, {2}edBaja, {3}edModificacion, {4}edImpresion,
						--{5}edBorraSistKardex, {6}edBanca, {7}edNivel0, {8}edConfigurar,
						--{9}edAdicional9, {10}edAdicional10, {11}edAdicional11,
						--{12}edAdicional12, {13}edAdicional13, {14}edAdicional14, {15}edAdicional15 );
	AD_ACCION Titulo NOT NULL ,		--Es la descripcion que se muestra en la rama, se requiere tenerla para el caso de los derechos especiales.
	
	AD_IMPACTO Status NOT NULL ,		--Impacto que tiene el derecho con referencia a otros derechos. Valores:
						--0=Impacto Alto: Derechos de Agregar o modificar
						--1=Impacto medio: Derechos de Borrar
						--2=Impacto bajo: Derechos de Consultar o imprimir
						--Los derechos que no caen en las clasificaciones anteriores se definen por el Cliente.
	AD_POSICIO Status NOT NULL, 		--Posicion dentro de la rama que tiene el derecho.
	AD_INGLES	 Titulo NOT NULL		--Descripcion en ingles. En caso de que algun cliente quiera usar esta descripcion, tiene que capturarla directamente en el Query Analizer.						--(Sirve para que en el reporte se pueda ordenar de la misma manera en que se ve en IU)
)
GO

/* Tabla ACC_DER: Esta tabla contiene los derechos que se pueden asignar a cada rama, debe de contener los datos de los Arboles de Tress, Visitantes, Seleccion, WorkFlow*/
/* Patch 420 # Seq: 4 Agregado */
ALTER TABLE ACC_DER
       ADD CONSTRAINT PK_ACC_DER PRIMARY KEY (AA_SOURCE,AX_NUMERO,AD_TIPO)
GO

/* Tabla AUTOMOD: Esta tabla contiene los modulos que tiene autorizados en el sentinel el Cliente.
Esta tabla se actualiza cada vez que se registran las claves de sentinel en el TressCFG.
Esta tabla no tiene consecuencias en la forma en que lee el sentinel los DLLS.
Esta tabla solo se utiliza en los views de derechos de acceso
*/
/* Patch 420 # Seq: 5 Agregado */
CREATE TABLE AUTOMOD (			--Descripcion: Tabla de Modulos autorizados
	AM_CODIGO	 FolioChico NOT NULL,	--Numero de modulo dentro del sentinel
	AM_NOMBRE  Titulo NOT NULL ,		--Nombre del modulo
	AM_FECHA 	Fecha NOT NULL 		--Fecha en que se registró la autorizacion,
)
GO

/* Tabla AUTOMOD: Esta tabla contiene los modulos que tiene autorizados en el sentinel el Cliente.*/
/* Patch 420 # Seq: 6 Agregado */
ALTER TABLE AUTOMOD
       ADD CONSTRAINT PK_AUTOMOD PRIMARY KEY (AM_CODIGO)
GO

/* TABLA ACCESO */
/*CR 1465: Navegacion Supervisores / Confidencialidad */
/* Insert para copiar los derechos de acceso de simulacion al nodo nuevo ( 1/3)*/
/* Patch 420 # Seq: 7 Agregado*/
create procedure ACTUALIZA_DERECHOS
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 614); /* Derecho en Supervisores */
     if ( @Cuantos = 0 )
     begin
          INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		  SELECT GR_CODIGO, CM_CODIGO, 614, AX_DERECHO FROM ACCESO
		  WHERE AX_NUMERO = 163  /* Es el folder de \Supervisores\Ver Status Empleado) */
     end
end
GO

/* Insert para copiar los derechos de acceso de simulacion al nodo nuevo ( 2/3)*/
/* Patch 1 # Seq: 8 Agregado */
execute ACTUALIZA_DERECHOS
GO

/* Insert para copiar los derechos de acceso de simulacion al nodo nuevo ( 3/3)*/
/* Patch 1 # Seq: 9 Agregado */
DROP PROCEDURE ACTUALIZA_DERECHOS
GO

