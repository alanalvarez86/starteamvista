/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/**************************************************/
/******** MODULO DE KIOSCO ************************/
/**************************************************/

/* Agregar tabla : Agregar un dominio nuevo (M�dulo de Kiosco)*/
/* Patch 410 # Seq: 1 Agregado */
if not exists ( select * from dbo.systypes where name = 'Ruta_URL')
BEGIN
exec sp_addtype Ruta_URL, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, Ruta_URL
END
go

/* Agregar tabla KESTILO: (M�dulo de Kiosco)*/
/* Patch 410 # Seq: 2 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[KESTILO]'))
BEGIN
CREATE TABLE KESTILO (
       KE_CODIGO            Codigo,  
       KE_NOMBRE            Descripcion,
       KE_COLOR             FolioGrande,
       KE_F_NAME            Descripcion,
       KE_F_SIZE            FolioChico,
       KE_F_COLR            FolioGrande,
       KE_F_BOLD            Booleano,
       KE_F_ITAL            Booleano,
       KE_F_SUBR            Booleano,
       KE_TIPO              Status,
       KE_RUTA              Ruta_URL,
	  KE_BITMAP            Ruta_URL
)
END
GO

/* TABLA KESTILO: Agregar llave primaria (M�dulo de Kiosco) */
/* Patch 410: # Seq: 3 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_KESTILO]'))
BEGIN
ALTER TABLE KESTILO
       ADD CONSTRAINT PK_KESTILO PRIMARY KEY (KE_CODIGO)
END
GO

/* Agregar tabla KSCREEN: (M�dulo de Kiosco)*/
/* Patch 410 # Seq: 4 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[KSCREEN]'))
BEGIN
CREATE TABLE KSCREEN (
       KS_CODIGO            Codigo,
       KS_NOMBRE            Descripcion,
       KS_PF_SIZE           FolioChico,
       KS_PF_COLR           FolioGrande,
       KS_PF_ALIN           Status,
       KS_PF_URL            Formula,
       KS_PB_SIZE           FolioChico,
       KS_PB_COLR           FolioGrande,
       KS_PB_ALIN           Status,
       KS_BTN_ALT           FolioChico,
       KS_BTN_SEP           FolioChico,
       KS_EST_NOR           Codigo,
       KS_EST_SEL           Codigo,
	  KS_BITMAP            Ruta_Url
)
END
GO

/* TABLA KSCREEN: Agregar llave primaria (M�dulo de Kiosco) */
/* Patch 410: # Seq: 5 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_KSCREEN]'))
BEGIN
ALTER TABLE KSCREEN
       ADD CONSTRAINT PK_KSCREEN PRIMARY KEY (KS_CODIGO)
END
GO

/* Agregar tabla KBOTON: (M�dulo de Kiosco)*/
/* Patch 410 # Seq: 6 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[KBOTON]'))
BEGIN
CREATE TABLE KBOTON (
       KS_CODIGO            Codigo,
       KB_ORDEN             FolioChico,
       KB_TEXTO             Observaciones,
       KB_BITMAP            Ruta_URL,
       KB_ACCION            Status,
       KB_URL               Ruta_URL,
       KB_SCREEN            Codigo,
       KB_ALTURA            FolioChico,
       KB_LUGAR             Status,
       KB_SEPARA            FolioChico,
	  KB_REPORTE		   FolioChico,
	  KB_POS_BIT           Status
)
END
GO

/* TABLA KBOTON: Agregar llave primaria (M�dulo de Kiosco) */
/* Patch 410: # Seq: 7 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_KBOTON]'))
BEGIN
ALTER TABLE KBOTON
       ADD CONSTRAINT PK_KBOTON PRIMARY KEY (KS_CODIGO, KB_ORDEN)
END
GO

/* Agregar tabla KSHOW: (M�dulo de Kiosco)*/
/* Patch 410 # Seq: 8 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[KSHOW]'))
BEGIN
CREATE TABLE KSHOW (
       KW_CODIGO            Codigo,
       KW_NOMBRE            Descripcion,
       KW_SCR_DAT           Codigo,
       KW_TIM_DAT           FolioChico,
       KW_GET_NIP           Booleano,
	  CM_CODIGO            CodigoEmpresa
)
END
GO

/* TABLA KSHOW: Agregar llave primaria (M�dulo de Kiosco) */
/* Patch 410: # Seq: 9 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_KSHOW]'))
BEGIN
ALTER TABLE KSHOW
       ADD CONSTRAINT PK_KSHOW PRIMARY KEY (KW_CODIGO)
END
GO

/* Agregar tabla KSHOWINF: (M�dulo de Kiosco)*/
/* Patch 410 # Seq: 10 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[KSHOWINF]'))
BEGIN
CREATE TABLE KSHOWINF (
       KW_CODIGO            Codigo,
       KI_ORDEN             FolioChico,
       KI_TIMEOUT           FolioChico,
       KI_URL               Ruta_URL,
       KI_REFRESH           Booleano
)
END
GO

/* TABLA KSHOWINF: Agregar llave primaria (M�dulo de Kiosco) */
/* Patch 410: # Seq: 11 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_KSHOWINF]'))
BEGIN
ALTER TABLE KSHOWINF
       ADD CONSTRAINT PK_KSHOWINF PRIMARY KEY (KW_CODIGO, KI_ORDEN)
END
GO

/* Agregar llave foranea de tabla KBOTON hacia KSCREEN (M�dulo de Kiosco) */
/* Patch 410 # Seq: 12 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_KBoton_KScreen]'))
BEGIN
ALTER TABLE KBOTON
       ADD CONSTRAINT FK_KBoton_KScreen
       FOREIGN KEY (KS_CODIGO)
       REFERENCES KSCREEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
END
GO

/* Agregar llave foranea de tabla KSHOWINF hacia KSHOW (M�dulo de Kiosco) */
/* Patch 410 # Seq: 13 Agregado */
if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_KShowInf_KShow]'))
BEGIN
ALTER TABLE KSHOWINF
       ADD CONSTRAINT FK_KShowInf_KShow
       FOREIGN KEY (KW_CODIGO)
       REFERENCES KSHOW
       ON DELETE CASCADE
       ON UPDATE CASCADE;
END
GO

/**************************************************/
/*********** PRO: BITACORA DE KIOSCO **************/
/**************************************************/

/* Agregar tabla : Agregar un dominio nuevo (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 14 Agregado */
exec sp_addtype TextoBitacora, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, TextoBitacora
go

/* Agregar tabla : Agregar un dominio nuevo (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 15 Agregado */
exec sp_addtype ComputerName, 'varchar(63)', 'NOT NULL'
exec sp_bindefault StringVacio, ComputerName
go

/* Agregar tabla BITKIOSCO: (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 16 Agregado */
CREATE TABLE BITKIOSCO (
       CB_CODIGO            NumeroEmpleado,       /* Empleado */
       CM_CODIGO	        CodigoEmpresa,        /* C�digo de Empresa */
       BI_FECHA             Fecha,        /* Fecha */
       BI_HORA              HoraMinutosSegundos,  /* Hora de movimiento */
       BI_TIPO              Status,       /* Tipo de bit�cora */
       BI_ACCION	        Status,       /*  Tipo de Acci�n */
       BI_NUMERO            FolioGrande,  /* N�mero de bit�cora */
       BI_TEXTO             TextoBitacora,     /* Descripci�n de bit�cora*/
       BI_UBICA             Observaciones,	/* Ubicaci�n (Marco)*/
       BI_KIOSCO	        ComputerName,      /* C�digo de Kiosco */
	  BI_FEC_MOV           Fecha,            /* Fecha de Movimiento */
	  BI_FEC_HORA		   Fecha             /* Fecha + Hora */
)
go

/* BITKIOSCO: Indice por # de bit�cora (PRO: Bit�cora de Kiosco) */
/* Patch 410 # Seq: 17 Agregado */
CREATE INDEX XIE1BITKIOSCO ON BITKIOSCO
(
       BI_NUMERO
)
go

/* BITKIOSCO: Indice por campos Fecha y hora (PRO: Bit�cora de Kiosco) */
/* Patch 410 # Seq: 18 Agregado */
CREATE INDEX XIE2BITKIOSCO ON BITKIOSCO
(
       BI_FECHA,
       BI_HORA
)
go

/* Tabla: KBOTON: �Restringir? (PRO: Bit�cora de Kiosco) */
/* Patch 410 # Seq: 19 Agregado */
alter table KBOTON add KB_RESTRIC booleano
GO

/* Tabla: KBOTON: Inicializar Restricci�n de bot�n (PRO: Bit�cora de Kiosco) */
/* Patch 410 # Seq: 20 Modificado */
update KBOTON set KB_RESTRIC = 'N' where ( KB_RESTRIC is NULL )
GO

/* Tabla: KBOTON: �Usa empresa de empleado? (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 21 Agregado */
alter table KBOTON add KB_USA_EMPL Booleano
GO

/* Tabla: KBOTON: Inicializar KB_USA_EMPL en Falso (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 22 Modificado */
update KBOTON set KB_USA_EMPL = 'N' where ( KB_USA_EMPL is NULL )
GO

/* Tabla: COMPANY: Clasificaci�n de Kiosco (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 23 Agregado */
alter table COMPANY add CM_KCLASIFI FolioChico
GO

/* Tabla: COMPANY: Inicializar clasificaci�n para Kiosco (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 24 Agregado */
update COMPANY set CM_KCLASIFI = -1 where ( CM_KCLASIFI is NULL )
GO

/* Global #24: � Graba en bit�cora ? (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 25 Modificado */
if not EXISTS ( select * from global where gl_codigo = 24 )
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) values ( 24,'Movimientos en Bit�cora','C8FF' )

/* Tabla: KBOTON_ACC: Accesos en bot�n (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 26 Agregado */
CREATE TABLE KBOTON_ACC (
       KS_CODIGO    Codigo,
	  KB_ORDEN	FolioChico,
	  GR_CODIGO	FolioChico
)
GO
/* Tabla: KBOTON_ACC: Llave Primaria (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 27 Agregado */
ALTER TABLE KBOTON_ACC
       ADD CONSTRAINT PK_KBOTON_ACC PRIMARY KEY (KS_CODIGO, KB_ORDEN, GR_CODIGO )
GO

/* Tabla: KBOTON_ACC: Llave for�nea (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 28 Agregado */
ALTER TABLE KBOTON_ACC
       ADD CONSTRAINT FK_KBOTON_ACC
       FOREIGN KEY (KS_CODIGO, KB_ORDEN)
       REFERENCES KBOTON
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/**************************************************/
/*********** PRO: ENROLAMIENTO****** **************/
/**************************************************/

/* Tabla Usuario: � Usuario Activo?  a la tabla USUARIO (PRO Enrolamiento) */
/* Patch 410 # Seq: 29 Agregado */
ALTER TABLE USUARIO ADD US_ACTIVO Booleano default 'S';
GO

/* Inicializar US_ACTIVO (PRO Enrolamiento) */
/* Patch 410 # Seq: 30 Agregado */
UPDATE USUARIO SET US_ACTIVO ='S' WHERE US_ACTIVO IS NULL;
GO

/* Tabla USUARIO: �Puede firmarse a portal? (PRO Enrolamiento) */
/* Patch 410 # Seq: 31 Agregado */
ALTER TABLE USUARIO ADD US_PORTAL Booleano default 'S';
GO

/* USUARIO.US_PORTAL: Inicializaci�n (PRO Enrolamiento) */
/* Patch 410 # Seq: 32 Agregado */
UPDATE USUARIO SET US_PORTAL ='N' WHERE US_PORTAL IS NULL

/* Tabla USUARIO: Dominio (PRO Enrolamiento) */
/* Patch 410 # Seq: 33 Agregado */
ALTER TABLE USUARIO ADD US_DOMAIN Observaciones;
GO

/* Tabla USUARIO: Jefe inmediato (PRO Enrolamiento) */
/* Patch 410 # Seq: 34 Agregado */
ALTER TABLE USUARIO ADD US_JEFE Usuario;
GO

/* Tabla USUARIO: Jefe inmediato (PRO Enrolamiento) */
/* Patch 410 # Seq: 35 Agregado */
CREATE TABLE ROL (
	RO_CODIGO Codigo NOT NULL ,
	RO_NOMBRE Descripcion NOT NULL ,
	RO_DESCRIP Accesos NOT NULL ,
	RO_NIVEL FolioChico NOT NULL )
GO

/* Tabla ROL: Agregar Llave primaria  (PRO Enrolamiento) */
/* Patch 410 # Seq: 36 Agregado */
ALTER TABLE ROL
       ADD CONSTRAINT PK_ROL PRIMARY KEY (RO_CODIGO)
go

/* Agregar Tabla ROL (PRO Enrolamiento) */
/* Patch 410 # Seq: 37 Agregado */
CREATE TABLE USER_ROL (
	US_CODIGO Usuario NOT NULL ,
	RO_CODIGO Codigo NOT NULL ,
	UR_PPAL Booleano NOT NULL
)
GO

/* Tabla ROL: Agregar Llave Primaria (PRO Enrolamiento) */
/* Patch 410 # Seq: 38 Agregado */
ALTER TABLE USER_ROL
		ADD CONSTRAINT PK_USER_ROL PRIMARY KEY  (US_CODIGO,RO_CODIGO)
GO

/* Tabla ROL: Agregar llave for�nea hacia la tabla ROL (PRO Enrolamiento) */
/* Patch 410 # Seq: 39 Agregado */
ALTER TABLE USER_ROL
       ADD CONSTRAINT FK_USER_ROL_ROL
       FOREIGN KEY (RO_CODIGO)
       REFERENCES ROL
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Tabla USER_ROL: Agregar llave for�nea hacia la tabla USUARIO (PRO Enrolamiento) */
/* Patch 410 # Seq: 40 Agregado */
ALTER TABLE USER_ROL
       ADD CONSTRAINT FK_USER_ROL_USUARIO
       FOREIGN KEY (US_CODIGO)
       REFERENCES USUARIO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* RelojLinx: Dominio Reloj Linx (SUG 1028 Bitacora de cafeteria en la Base de Datos  ) */
/* Patch 410 # Seq: 41 Agregado */
exec sp_addtype RelojLinx, 'varchar(4)', 'NOT NULL'
exec sp_bindefault StringVacio, RelojLinx
go

/* Create BITCAFE: Bit�cora de cafeter�a (SUG 1028 Bitacora de cafeteria en la Base de Datos  ) */
/* Patch 410 # Seq: 42 Agregado */
CREATE TABLE BITCAFE (
          BC_FOLIO FolioGrande IDENTITY,   /*Identificador/Llave primaria*/
          CB_CODIGO   NumeroEmpleado NOT NULL,  /*#Empleado que registr� la checada*/
          IV_CODIGO NumeroEmpleado NOT NULL,  /* C�digo de Invitador*/
          BC_EMPRESA Codigo1 NOT NULL,   /*D�gito de empresa de la checada*/
          BC_CREDENC  Codigo1 NOT NULL,    /*Letra de la credencial de la checada*/
          BC_FECHA    Fecha NOT NULL,    /*Fecha de la checada*/
          BC_HORA     Hora NOT NULL,    /*Hora de la checada*/
          BC_RELOJ    RelojLinx NOT NULL,  /*Estaci�n que registr� la checada*/
          BC_TIPO  Codigo1 NOT NULL ,   /*Tipo de comida*/
          BC_COMIDAS  FolioChico NOT NULL ,  /*Cantidad de comidas*/
          BC_EXTRAS  FolioChico NOT NULL ,  /*Comidas despues de tiempo extra*/
          BC_REG_EXT  Booleano NOT NULL,   /*�Es comida extra?*/
          BC_MANUAL  Booleano NOT NULL,   /*�Es comida manual?*/
          CL_CODIGO  FolioChico NOT NULL ,   /*Regla por la que NO pas� la checada*/
          BC_TGAFETE  Status NOT NULL,    /*Tipo de gafete: Tress o Proximidad */
          BC_TIEMPO   FolioChico NOT NULL,  /*Tiempo fuera de l�nea*/
          BC_STATUS   Status NOT NULL,
          BC_MENSAJE Observaciones,     /* Mensaje */
          BC_RESPUES Observaciones,    /* Respuesta*/
          BC_CHECADA Observaciones    /* Checada original */
)
GO

/* BITCAFE: Llave Primaria (SUG 1028 Bitacora de cafeteria en la Base de Datos  ) */
/* Patch 410 # Seq: 43 Agregado */
ALTER TABLE BITCAFE
       ADD CONSTRAINT PK_BITCAFE PRIMARY KEY (BC_FOLIO)
GO

/* WOUTBOX: Campo Llave Scripts tablas Evaluaci�n de desempe�o ( Portal ) */
/* Patch 410 # Seq: 44 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WOUTBOX]'))
begin
     alter table WOUTBOX add LLAVE int identity ( 1, 1)
end
go

/* DICCION: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 45 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DICCION]'))
begin
     alter table DICCION add LLAVE int identity ( 1, 1)
end
go

/* BITACORA: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 46 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BITACORA]'))
begin
     alter table BITACORA add LLAVE int identity ( 1, 1)
end
go

/* PROCESO: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 47 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCESO]'))
begin
     alter table PROCESO add LLAVE int identity ( 1, 1)
end
go

/* QUERYS: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 48 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[QUERYS]'))
begin
     alter table QUERYS add LLAVE int identity ( 1, 1)
end
go

/* REPORTE: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 49 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REPORTE]'))
begin
     alter table REPORTE add LLAVE int identity ( 1, 1)
end
go

/* CAMPOREP: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 50 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CAMPOREP]'))
begin
     alter table CAMPOREP add LLAVE int identity ( 1, 1)
end
go

/* MISREPOR: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 51 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MISREPOR]'))
begin
     alter table MISREPOR add LLAVE int identity ( 1, 1)
end
go

/* ROL: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 52 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ROL]'))
begin
     alter table ROL add LLAVE int identity
end
go

/* USER_ROL: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 53 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USER_ROL]'))
begin
     alter table USER_ROL add LLAVE int identity
end
go

/* WACCION: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 54 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WACCION]'))
begin
     alter table WACCION add LLAVE int identity
end
go

/* WARCHIVO: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 55 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WARCHIVO]'))
begin
     alter table WARCHIVO add LLAVE int identity
end
go

/* WMENSAJE: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 56 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WMENSAJE]'))
begin
     alter table WMENSAJE add LLAVE int identity
end
go

/* WMODSTEP: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 57 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WMODSTEP]'))
begin
     alter table WMODSTEP add LLAVE int identity
end
go

/* WMODELO: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 58 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WMODELO]'))
begin
     alter table WMODELO add LLAVE int identity
end
go

/* Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 59 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WPROCESO]'))
begin
     alter table WPROCESO add LLAVE int identity
end
go

/* WPROSTEP: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 60 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WPROSTEP]'))
begin
     alter table WPROSTEP add LLAVE int identity
end
go

/* WTAREA: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 61 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WTAREA]'))
begin
     alter table WTAREA add LLAVE int identity
end
go

/* WMOD_ROL: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 62 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WMOD_ROL]'))
begin
     alter table WMOD_ROL add LLAVE int identity
end
go

/* SUSCRIP: Campo Llave Scripts tablas Instalacion WorkFlow ( Portal ) */
/* Patch 410 # Seq: 63 Agregado */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SUSCRIP]'))
begin
     alter table SUSCRIP add LLAVE int identity
end
go

/* Tabla Usuario: Cambio de dominio de US_MAQUINA ( Portal ) */
/* Patch 410 # Seq: 64 Agregado */
alter table USUARIO alter column US_MAQUINA Ruta_URL;
GO

/* INIT_ACC_29: Inicializa derecho de acceso de Alta - Modificar # de Empleado */
/* Patch 410 # Seq: 65 Agregado */
update ACCESO set AX_DERECHO = ( AX_DERECHO + 8 )
where ( AX_NUMERO = 29 ) AND ( AX_DERECHO >= 1 ) AND ( AX_DERECHO <= 7 )
GO

/* INIT_DERECHO_INFONAVIT: Inicializa derecho de acceso de Expediente - Infonavit (1/3)*/
/* Patch 410 # Seq: 66 Agregado*/
CREATE PROCEDURE INIT_DERECHO_INFONAVIT
AS
BEGIN
	SET NOCOUNT ON
	declare @DerechoOtros FolioChico
	declare @Empresa CodigoEmpresa
	declare @Grupo FolioChico
	declare @DerechosInfonavit FolioChico

	declare CursorDerechoOtros CURSOR FOR
    	select AX_DERECHO, CM_CODIGO, GR_CODIGO from ACCESO where
	( AX_NUMERO = 7 )
	OPEN CursorDerechoOtros
   	FETCH NEXT FROM CursorDerechoOtros INTO @DerechoOtros, @Empresa, @Grupo

	WHILE @@FETCH_STATUS = 0
    	BEGIN
		set @DerechosInfonavit = 0

		if  ( @DerechoOtros & 0x1 ) > 0
		begin
			set @DerechosInfonavit = 1
		end

	        if ( @DerechoOtros & 0x10 ) > 0
		begin
			set @DerechosInfonavit = @DerechosInfonavit + 16
		end

		if ( ( @DerechoOtros & 0x20 ) > 0 )  and ( ( @DerechoOtros & 0x8 ) > 0 )
		begin
			set @DerechosInfonavit = @DerechosInfonavit + 14
		end

		if ( @DerechosInfonavit > 0 )
		begin

			if exists ( select * from ACCESO where AX_NUMERO = 595 and ( CM_CODIGO = @Empresa ) and ( GR_CODIGO = @Grupo ) )
			begin
				UPDATE ACCESO set AX_DERECHO = @DerechosInfonavit from ACCESO where
				( AX_NUMERO = 595 ) and ( CM_CODIGO = @Empresa ) and ( GR_CODIGO = @Grupo )
			end
			else
			begin
				insert into ACCESO( GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO ) values ( @Grupo,@Empresa, 595,@DerechosInfonavit )
			end
		end
		FETCH NEXT FROM CursorDerechoOtros INTO @DerechoOtros, @Empresa, @Grupo
    	END
	close CursorDerechoOtros
     deallocate CursorDerechoOtros

END
GO

/* INIT_DERECHO_INFONAVIT: Inicializa derecho de acceso de Expediente - Infonavit (2/3)*/
/* Patch 410 # Seq: 67 Agregado*/
exec INIT_DERECHO_INFONAVIT
GO

/* INIT_DERECHO_INFONAVIT: Inicializa derecho de acceso de Expediente - Infonavit (3/3)*/
/* Patch 410 # Seq: 68 Agregado*/
drop procedure INIT_DERECHO_INFONAVIT
GO

/**************************************************/
/********** TRIGGERS DE COMPARTE ******************/
/**************************************************/

/* Tabla KSCREEN: Agregar Trigger (M�dulo de Kiosco) */
/* Patch 410 # Seq: 12 Agregado */
CREATE TRIGGER TU_KSCREEN ON KSCREEN AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
  IF UPDATE( KS_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = KS_CODIGO from   Inserted;
        select @OldCodigo = KS_CODIGO from   Deleted;

	UPDATE KSHOW	SET KW_SCR_DAT = @NewCodigo WHERE KW_SCR_DAT = @OldCodigo;
  END
END
GO

/* Tabla KSCREEN: Agregar Trigger (M�dulo de Kiosco) */
/* Patch 410 # Seq: 13 Agregado */
CREATE TRIGGER TD_KSCREEN ON KSCREEN AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	update KSHOW
	set    KSHOW.KW_SCR_DAT = ''
	from   KSHOW, Deleted
	where  KSHOW.KW_SCR_DAT = Deleted.KS_CODIGO
END
GO

/* Tabla KESTILO: Agregar Trigger (M�dulo de Kiosco) */
/* Patch 410 # Seq: 14 Agregado */
CREATE TRIGGER TU_KESTILO ON KESTILO AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
  	IF UPDATE( KE_CODIGO )
  	BEGIN
		declare @NewCodigo Codigo, @OldCodigo Codigo;

		select @NewCodigo = KE_CODIGO from   Inserted;
        	select @OldCodigo = KE_CODIGO from   Deleted;

		UPDATE KSCREEN	SET KS_EST_NOR = @NewCodigo WHERE KS_EST_NOR = @OldCodigo;
		UPDATE KSCREEN	SET KS_EST_SEL = @NewCodigo WHERE KS_EST_SEL = @OldCodigo;
 	END
END
GO

/* Tabla KESTILO: Agregar Trigger (M�dulo de Kiosco) */
/* Patch 410 # Seq: 15 Agregado */
CREATE TRIGGER TD_KESTILO ON KESTILO AFTER DELETE AS
BEGIN
	SET NOCOUNT ON;
	update KSCREEN
	set    KSCREEN.KS_EST_NOR = ''
	from   KSCREEN, Deleted
	where  KSCREEN.KS_EST_NOR = Deleted.KE_CODIGO

	update KSCREEN
	set    KSCREEN.KS_EST_SEL = ''
	from   KSCREEN, Deleted
	where  KSCREEN.KS_EST_SEL = Deleted.KE_CODIGO
END
GO

/**************************************************/
/********** STORED PROCEDURES DE COMPARTE *********/
/**************************************************/

/* SP_AGREGA_BITACORA_KIOSCO: Agrega Bit�cora ( PRO: Bit�cora de Kiosco )*/
/* Patch 410 # Seq: 7 Agregado*/
CREATE PROCEDURE SP_AGREGA_BITACORA_KIOSCO
					  ( @CB_CODIGO NumeroEmpleado,
					  @CM_CODIGO CodigoEmpresa,
       				  @BI_TIPO  Status,
       				  @BI_ACCION Status,
       				  @BI_NUMERO FolioGrande,
       				  @BI_TEXTO  TextoBitacora,
       				  @BI_UBICA  Observaciones,
       				  @BI_KIOSCO  ComputerName,
	  				  @BI_FEC_MOV Fecha )

AS
	BEGIN
		SET NOCOUNT ON
		declare @BI_HORA HoraMinutosSegundos
		declare @BI_FECHA Fecha

		/* Horas: 108: hh:mm:ss */
		set @BI_HORA = convert(varchar,GetDate(),108)
		/* Fecha: 101: mm/dd/yy */
		set @BI_FECHA = convert(varchar,GetDate(),101)

		/* NullDateTime equivale a 12/30/1899 */
		if ( @BI_FEC_MOV = '12/30/1899' )
			set @BI_FEC_MOV = @BI_FECHA

		set @BI_FEC_MOV = IsNull(@BI_FEC_MOV,@BI_FECHA )

		insert into BITKIOSCO( CB_CODIGO, CM_CODIGO, BI_FECHA, BI_HORA, BI_TIPO,
						   BI_ACCION, BI_NUMERO, BI_TEXTO, BI_UBICA, BI_KIOSCO,
						   BI_FEC_MOV )
		values ( @CB_CODIGO, @CM_CODIGO, @BI_FECHA, @BI_HORA, @BI_TIPO,

		   @BI_ACCION, @BI_NUMERO, @BI_TEXTO, @BI_UBICA, @BI_KIOSCO,
						   @BI_FEC_MOV )
	END
GO

/* GET_MINUTOS_LOGS: Regresa Minutos Transcurridos (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 8 Agregado*/
CREATE FUNCTION DBO.GET_MINUTOS_LOGS( @CM_CODIGO CodigoEmpresa,
							   @CB_CODIGO NumeroEmpleado,
							   @FECHA Fecha,
							   @Hora HoraMinutosSegundos,
							   @Accion Status ) RETURNS Integer

AS
BEGIN
	declare @Minutos Integer
	declare @FechaCompleta Fecha

	/* Reconstruye la fecha + horas */
	set @FechaCompleta = @Fecha + ' ' + @Hora
	set @FechaCompleta = CAST ( @FechaCompleta AS datetime )


	select @Minutos = ( select DATEDIFF( mi, ( select CAST ( MAX(BI_FECHA) + ' ' + MAX(BI_HORA) AS datetime ) from BITKIOSCO
	where ( ( CM_CODIGO = @CM_CODIGO ) and ( CB_CODIGO = @CB_CODIGO )
			and (  BI_ACCION = @Accion ) and ( BI_FECHA <=  @Fecha ) and ( BI_HORA < @Hora ) ) ),  @FechaCompleta ) )

	return COALESCE ( @Minutos, 0 )
END
GO

/* GET_MINUTOS_LOGIN: Regresa Minutos Transcurridos de acci�n login (PRO: Bit�cora de Kiosco)*/
/* Patch 410 # Seq: 9 Agregado*/
CREATE FUNCTION DBO.GET_MINUTOS_LOGIN( @CM_CODIGO CodigoEmpresa,
							   @CB_CODIGO NumeroEmpleado,
							   @FECHA Fecha,
							   @Hora HoraMinutosSegundos ) RETURNS Integer

AS
BEGIN
	return dbo.GET_MINUTOS_LOGS ( @CM_CODIGO, @CB_CODIGO, @Fecha, @Hora, 1  )
END
GO

/****************************************/
/****************************************/
/****** Fin de Patch para COMPARTE ******/
/****************************************/
/****************************************/

