if exists (SELECT * FROM sys.indexes WHERE name='IDX_EMP_BIO_EMPLEADO' AND object_id = OBJECT_ID('EMP_BIO'))
begin
	CREATE NONCLUSTERED INDEX IDX_EMP_BIO_EMPLEADO     
		ON EMP_BIO (CM_CODIGO, CB_CODIGO)
end
GO

IF not EXISTS (select * from syscolumns where id=object_id('EMP_BIO') and name='ES_BAJA_FT') 
begin
ALTER TABLE EMP_BIO ADD ES_BAJA_FT Booleano  NULL 
end 
go 

UPDATE EMP_BIO set ES_BAJA_FT = 'N' where ES_BAJA_FT is null 

go 

ALTER TABLE EMP_BIO ALTER COLUMN ES_BAJA_FT Booleano NOT NULL 

GO 

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_ACTUALIZA_STATUS_EMP' AND type in (N'P', N'PC'))
	DROP PROCEDURE WS_ACTUALIZA_STATUS_EMP
GO

CREATE  PROCEDURE WS_ACTUALIZA_STATUS_EMP
AS
BEGIN 
    declare @Hoy datetime
    SET @Hoy = CONVERT(datetime , CONVERT (char(10), getdate(), 103), 103 );

	update EMP_BIO set CB_ACTIVO = Coalesce( VISTA.CB_ACTIVO, 'S' ), CB_FEC_BAJ = coalesce( VISTA.CB_FEC_BAJ, '1899-12-30' ), ES_BAJA_FT= case when VISTA.CB_ACTIVO = 'N' and VISTA.CB_FEC_BAJ >= @Hoy  then 'S' else 'N' end 
	from EMP_BIO, VEMPL_BAJA VISTA 
	where EMP_BIO.CM_CODIGO = VISTA.CM_CODIGO and EMP_BIO.CB_CODIGO = VISTA.CB_CODIGO 

	update EMP_BIO set CB_ACTIVO = 'S' , CB_FEC_BAJ =  '1899-12-30', ES_BAJA_FT = case when VISTA.CB_ACTIVO = 'N' and VISTA.CB_FEC_BAJ >= @Hoy  then 'S' else 'N' end 
	from EMP_BIO, VEMPL_ALTA VISTA 
	where EMP_BIO.CM_CODIGO = VISTA.CM_CODIGO and EMP_BIO.CB_CODIGO = VISTA.CB_CODIGO 

END

GO 


IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLASOBRANTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE WS_TERMINAL_HUELLASOBRANTE
GO

CREATE PROCEDURE WS_TERMINAL_HUELLASOBRANTE( @TE_CODIGO NumeroEmpleado ) 
as 
begin 
       SET NOCOUNT ON
       DECLARE @TipoTerminal integer
       declare @Hoy datetime
       SET @Hoy = CONVERT(datetime , CONVERT (char(10), getdate(), 103), 103 );

       SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

       IF @TipoTerminal = 5 
       BEGIN
             select * 
             from EMP_BIO 
             inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
			 left outer join TERM_GPO on TERM_GPO.TE_CODIGO = TERM_HUELLA.TE_CODIGO 
             where TERM_HUELLA.BORRAR = 'S' 
             and ( TERM_HUELLA.TE_CODIGO = @TE_CODIGO ) and ( TERM_HUELLA.HU_INDICE = 11 )      
			 and ( ( EMP_BIO.CB_ACTIVO = 'N' AND EMP_BIO.CB_FEC_BAJ < @Hoy ) or ( EMP_BIO.CB_ACTIVO = 'S'  and EMP_BIO.ES_BAJA_FT='N'  )  or ( EMP_BIO.GP_CODIGO <> COALESCE(TERM_GPO.GP_CODIGO, EMP_BIO.GP_CODIGO)  ) )       
       END
       ELSE
       BEGIN
             select * 
             from EMP_BIO 
             inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
			 left outer join TERM_GPO on TERM_GPO.TE_CODIGO = TERM_HUELLA.TE_CODIGO 
             where TERM_HUELLA.BORRAR = 'S' 
             and TERM_HUELLA.TE_CODIGO =@TE_CODIGO and ( TERM_HUELLA.HU_INDICE > 0 and TERM_HUELLA.HU_INDICE < 11 )              
			 and ( ( EMP_BIO.CB_ACTIVO = 'N' AND EMP_BIO.CB_FEC_BAJ < @Hoy ) or ( EMP_BIO.CB_ACTIVO = 'S'  and EMP_BIO.ES_BAJA_FT='N'  )  or ( EMP_BIO.GP_CODIGO <> COALESCE(TERM_GPO.GP_CODIGO, EMP_BIO.GP_CODIGO)  ) )    
       end
end
GO

exec WS_ACTUALIZA_STATUS_EMP

GO 


