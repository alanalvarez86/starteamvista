/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* CR:2691, Protecci�n Civil Actualizar Derechos Datos\Otros 1/3 */
/* Patch 435 #Seq: 1*/
create procedure ACTUALIZA_DERECHOS
as
begin
	SET NOCOUNT ON;

	declare @Cuantos Integer;

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 684);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 684, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 685);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 685, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 686);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 686, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 687);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 687, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 688);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 688, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 689);
	if ( @Cuantos = 0 )
	begin 
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 689, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7;
		

        UPDATE ACCESO SET AX_DERECHO = ACCESO.AX_DERECHO & ~8 from ACCESO, ACCESO A2 where ACCESO.AX_NUMERO = 689 and A2.AX_DERECHO&128 <= 0
		 and ACCESO.GR_CODIGO = A2.GR_CODIGO and ACCESO.CM_CODIGO = A2.CM_CODIGO and A2.AX_NUMERO = 7 ;
	 	; 
	  
        UPDATE ACCESO SET AX_DERECHO = ACCESO.AX_DERECHO | 8 from ACCESO, ACCESO A2 where ACCESO.AX_NUMERO = 689 and A2.AX_DERECHO&128 > 0 
  	     and ACCESO.GR_CODIGO = A2.GR_CODIGO and ACCESO.CM_CODIGO = A2.CM_CODIGO and A2.AX_NUMERO = 7 ;

	end

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 692);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 692, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7

	/* Derecho de mis Datos */
	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 693);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 693, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 7
end
GO

/* CR:2691, Protecci�n Civil Actualizar Derechos Datos\Otros 2/3 */
/* Patch 435 #Seq: 1*/
execute ACTUALIZA_DERECHOS
GO

/* CR:2691, Protecci�n Civil Actualizar Derechos Datos\Otros 3/3 */
/* Patch 435 #Seq: 1*/
DROP PROCEDURE ACTUALIZA_DERECHOS
GO

/* CR:2653, Mejora Confidencialidad - Cambiar dominio COMPANY.CM_NIVEL0 1/3 */
/* Patch 435 #Seq: 1*/
EXECUTE sp_unbindefault N'COMPANY.CM_NIVEL0'
GO

/* CR:2653, Mejora Confidencialidad - Cambiar dominio COMPANY.CM_NIVEL0 2/3 */
/* Patch 435 #Seq: 1*/
ALTER TABLE COMPANY
	ALTER COLUMN CM_NIVEL0 Accesos
GO

/* CR:2653, Mejora Confidencialidad - Cambiar dominio COMPANY.CM_NIVEL0 3/3 */
/* Patch 435 #Seq: 1*/
EXECUTE sp_bindefault 'dbo.StringVacio',  N'COMPANY.CM_NIVEL0'

GO


/* CR:2681, Mejoras WF 2013 - Agrega Campo WPROCESO.WP_MOV3 */
/* Patch 435 #Seq: 1*/
if not exists (select * from syscolumns where id=object_id('WPROCESO') and name='WP_MOV3')
	alter table WPROCESO add WP_MOV3 Descripcion null
GO

/* CR:2681, Mejoras WF 2013 - Agrega Campo WPROCESO.WP_EMPRESA */
/* Patch 435 #Seq: 1*/
if not exists (select * from syscolumns where id=object_id('WPROCESO') and name='WP_EMPRESA')
	alter table WPROCESO add WP_EMPRESA Descripcion  null
GO

/* CR:2681, Mejoras WF 2013 - Inicializa campos WPROCESO.WP_MOV3, WPROCESO.WP_EMPRESA */
/* Patch 435 #Seq: 1*/
update WPROCESO  set  WP_MOV3 = '', WP_EMPRESA = ''  where WP_MOV3 is null and WP_EMPRESA is null
GO

/* CR:2681, Mejoras WF 2013 - WPROCESO.WP_MOV3 - not null */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'WP_XML_MOV3') 
alter table WPROCESO alter column   WP_MOV3 Descripcion not null
GO

/* CR:2681, Mejoras WF 2013 - WPROCESO.WP_EMPRESA - not null */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'WP_XML_XEMPRESA')
alter table WPROCESO alter column   WP_EMPRESA Descripcion not null
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice WP_XML_MOV3 */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'WP_XML_MOV3')
CREATE INDEX WP_XML_MOV3 ON WPROCESO(WP_MOV3)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice WP_XML_XEMPRESA */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'WP_XML_XEMPRESA')
CREATE INDEX WP_XML_XEMPRESA ON WPROCESO(WP_EMPRESA)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Campo WPROCESO.WP_EMPRESA */
/* Patch 435 #Seq: 1*/
if not exists (select * from syscolumns where id=object_id('WPROCESO') and name='WP_XMLDOC')
	alter table WPROCESO add WP_XMLDOC Xml  null
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice WP_XMLDOC_IDX */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'WP_XMLDOC_IDX')
	CREATE PRIMARY XML INDEX WP_XMLDOC_IDX  ON WPROCESO(WP_XMLDOC)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice WP_XMLDOC_IDX_PROPERTY */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'WP_XMLDOC_IDX_PROPERTY')
	CREATE XML INDEX WP_XMLDOC_IDX_PROPERTY ON WPROCESO(WP_XMLDOC) USING XML INDEX WP_XMLDOC_IDX FOR PROPERTY
GO



/* CR:2681, Mejoras WF 2013 - Tabla sincronizada con los documentos XML del modelo de Autorizacion */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'VT_WPAUTO3' and XTYPE = 'U' )
CREATE TABLE VT_WPAUTO3 (
	WP_FOLIO FolioGrande NOT NULL,
	CM_CODIGO Descripcion NULL,
	WP_MODO varchar(5),
	MV_DESCR Formula,
  MV_OBSERVA Formula,
	CB_CODIGO int,
	AT_FEC_AUT Fecha,
	AT_TIPO Status,
	AT_HORAS numeric(15, 2),
	AT_MOTIVO Codigo
)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice IX_VT_WPAUTO3_WP_FOLIO */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_VT_WPAUTO3_WP_FOLIO')
create index IX_VT_WPAUTO3_WP_FOLIO on VT_WPAUTO3(WP_FOLIO)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice IX_VT_WPAUTO3_CM_CODIGO */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_VT_WPAUTO3_CM_CODIGO')
create index IX_VT_WPAUTO3_CM_CODIGO on VT_WPAUTO3(CM_CODIGO)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice IX_VT_WPAUTO3_AT_FEC_AUT */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_VT_WPAUTO3_AT_FEC_AUT')
create index IX_VT_WPAUTO3_AT_FEC_AUT on VT_WPAUTO3(AT_FEC_AUT)
GO

/* CR:2681, Mejoras WF 2013 - VIEW WPROCMOV3 */
/* Patch 435 #Seq: 1*/
CREATE  VIEW WPROCMOV3
AS
SELECT
	WP_FOLIO,
	WP_EMPRESA,
	WP_MOV3
FROM  dbo.WPROCESO
GO


/* CR:2681, Mejoras WF 2013 - Tabla sincronizada con los documentos XML del modelo de Autorizacion */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'VT_WPAUTO3' and XTYPE = 'U' )
CREATE TABLE VT_WPAUTO3 (
	WP_FOLIO FolioGrande NOT NULL,
	CM_CODIGO Descripcion NULL,
	WP_MODO varchar(5),
	MV_DESCR Formula,
  MV_OBSERVA Formula,
	CB_CODIGO int,
	AT_FEC_AUT Fecha,
	AT_TIPO Status,
	AT_HORAS numeric(15, 2),
	AT_MOTIVO Codigo
)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice IX_VT_WPAUTO3_WP_FOLIO */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_VT_WPAUTO3_WP_FOLIO')
create index IX_VT_WPAUTO3_WP_FOLIO on VT_WPAUTO3(WP_FOLIO)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice IX_VT_WPAUTO3_CM_CODIGO */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_VT_WPAUTO3_CM_CODIGO')
create index IX_VT_WPAUTO3_CM_CODIGO on VT_WPAUTO3(CM_CODIGO)
GO

/* CR:2681, Mejoras WF 2013 - Agrega Indice IX_VT_WPAUTO3_AT_FEC_AUT */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_VT_WPAUTO3_AT_FEC_AUT')
create index IX_VT_WPAUTO3_AT_FEC_AUT on VT_WPAUTO3(AT_FEC_AUT)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'WPROCMOV3') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view WPROCMOV3
GO 

/* CR:2681, Mejoras WF 2013 - VIEW WPROCMOV3 */
/* Patch 435 #Seq: 1*/
CREATE  VIEW WPROCMOV3
AS
SELECT
	WP_FOLIO,
	WP_EMPRESA,
	WP_MOV3
FROM  dbo.WPROCESO
GO


if exists (select * from dbo.sysobjects where id = object_id(N'V_WPAUTO3_PRE_PROCESS') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_WPAUTO3_PRE_PROCESS

GO 

/* CR:2681, Mejoras WF 2013 - VIEW V_WPAUTO3_PRE_PROCESS */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPAUTO3_PRE_PROCESS
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t.c.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t.c.value('FECHAAUTO[1]','datetime') AT_FEC_AUT,
t.c.value('TIPOAUTO[1]','Status') AT_TIPO,
t.c.value('HORAS[1]','Pesos') AT_HORAS,
t.c.value('MOTIVO[1]','Codigo') AT_MOTIVO
from
dbo.WPROCESO WP join dbo.WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'AUTO3' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t2.c2.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t.c.value('FECHAAUTO[1]','datetime') AT_FEC_AUT,
t.c.value('TIPOAUTO[1]','Status') AT_TIPO,
t.c.value('HORAS[1]','Pesos') AT_HORAS,
t.c.value('MOTIVO[1]','Codigo') AT_MOTIVO
from
dbo.WPROCESO WP join dbo.WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'AUTO3' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
UNION ALL
(
select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA
,t2.c2.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t2.c2.value('FECHAAUTO[1]','datetime') AT_FEC_AUT,
t2.c2.value('TIPOAUTO[1]','Status') AT_TIPO,
t2.c2.value('HORAS[1]','Pesos') AT_HORAS,
t2.c2.value('MOTIVO[1]','Codigo') AT_MOTIVO
from
dbo.WPROCESO WP join dbo.WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'AUTO3MULT'
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'SP_REPLICA_XMLDOC') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
		drop procedure SP_REPLICA_XMLDOC
GO 
create procedure SP_REPLICA_XMLDOC( @WP_FOLIO FolioGrande ) 
as 
begin 
	set NOCOUNT ON

	declare @Text varchar(max);
	declare @Xml  Xml 

	set @Text = null 
	set @Xml = null 

	select  @Text = cast( WP_XMLDATA as varchar(max))  from WPROCESO where WP_FOLIO = @WP_FOLIO 
	
	if @Text is null 
		return 
	else
	begin 
		if LEFT( @Text, 5) <> '<?xml' 
			set @Text  =  '<?xml version="1.0" encoding="ISO-8859-1" ?>'  + @Text; 			

		begin try
			set @Xml = Cast(@Text as xml)
		end try
		begin catch
			set @Xml = cast(replace(@Text, '&', '&amp;') as xml)
		end catch
	end

	update WPROCESO set WP_XMLDOC = @Xml where WP_FOLIO = @WP_FOLIO 

end 

GO 

if exists (select * from dbo.sysobjects where id = object_id(N'REPLICA_TODOS_XMLDOC') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
		drop procedure REPLICA_TODOS_XMLDOC
GO 

create procedure REPLICA_TODOS_XMLDOC 
as 
begin 
	set NOCOUNT ON

	create table #Workflows ( 
		i int identity(1,1), 
		WP_FOLIO int 
	)
	
	insert into #Workflows  (WP_FOLIO) select WP_FOLIO from WPROCESO

	declare @i int 
	declare @max int 
	declare @WP_FOLIO FolioGrande

	select @max = MAX(i) from #Workflows 
	set @i = 1
	while @i <= @max 
	begin	
		select @WP_FOLIO = WP_FOLIO from #Workflows where i = @i 
		exec SP_REPLICA_XMLDOC @WP_FOLIO
		set @i = @i + 1 
	end 
	 
	truncate table #Workflows 
end 

GO 

exec REPLICA_TODOS_XMLDOC 

Go 

drop procedure REPLICA_TODOS_XMLDOC

GO 

/* CR:2681, Mejoras WF 2013 - Actualiza tabla WPROCESO 1/3 */
/* Patch 435 #Seq: 1*/
create procedure ACTUALIZA_WPROCESO
as
begin
  set nocount on
	update WPROCESO	set
	WP_MOV3 = coalesce( cast( WP_XMLDOC.query('data(//DATOS/MOV3[1])') as varchar(30) ), ''),
	WP_EMPRESA = coalesce(  cast( WP_XMLDOC.query('data(//DATOS/EMPRESA[1])') as varchar(30) ), '' ) 

	TRUNCATE TABLE VT_WPAUTO3

	insert into VT_WPAUTO3 (WP_FOLIO, CM_CODIGO, WP_MODO, CB_CODIGO, AT_FEC_AUT, AT_TIPO, AT_HORAS, AT_MOTIVO)
	select V.WP_FOLIO, V.CM_CODIGO, V.WP_MODO, V.CB_CODIGO, V.AT_FEC_AUT, V.AT_TIPO, V.AT_HORAS, V.AT_MOTIVO
	from  V_WPAUTO3_PRE_PROCESS V
end
GO

/* CR:2681, Mejoras WF 2013 - Actualiza tabla WPROCESO 2/3 */
/* Patch 435 #Seq: 1*/
exec ACTUALIZA_WPROCESO
GO

/* CR:2681, Mejoras WF 2013 - Actualiza tabla WPROCESO 3/3 */
/* Patch 435 #Seq: 1*/
drop procedure ACTUALIZA_WPROCESO
GO

/* CR:2681, Mejoras WF 2013 - Agrega WPROCESO.WP_XMLADD */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'WP_XMLADD' and id = object_id(N'WPROCESO') )
ALTER TABLE WPROCESO
	ADD  WP_XMLADD xml
GO

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   VISTAS COMPARTE      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/


if exists (select * from dbo.sysobjects where id = object_id(N'V_PROCESO') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_PROCESO

GO 

/* CR:2681, Mejoras WF 2013 - Modificar VIEW V_PROCESO */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_PROCESO( WP_FOLIO,
                       WM_CODIGO,
                       WP_NOMBRE,
                       WP_FEC_INI,
                       WP_FEC_FIN,
                       WP_STATUS,
                       WP_PASO,
                       WP_PASOS,
                       WP_USR_INI,
                       WP_NOM_INI,
                       WP_NOTAS,
					             WP_XMLDATA,
                       WE_ORDEN,
                       WS_NOMBRE,
                       WS_STATUS,
                       WS_USR_INI,
                       WS_NOM_INI,
                       WS_FEC_INI,
                       WS_FEC_FIN,
                       WT_GUID,
                       WT_DESCRIP,
                       WT_STATUS,
                       WT_FEC_INI,
                       WT_FEC_FIN,
                       WT_USR_ORI,
                       WT_NOM_ORI,
                       WT_USR_DES,
                       WT_NOM_DES,
                       WT_NOTAS,
                       WT_AVANCE,
                       WM_DESCRIP,
                       WE_DESCRIP,
                       WM_URL_EXA,
                       WM_URL_DEL,
					             WP_MOV3,
					             WP_EMPRESA,
					             WP_XMLADD, 
								 WP_XMLDOC )
AS
SELECT WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,
       WPROCESO.WP_XMLDATA,
       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,
       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,
       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP,
       WMODELO.WM_URL_EXA,
       WMODELO.WM_URL_DEL,
	   WPROCESO.WP_MOV3,
	   WPROCESO.WP_EMPRESA,
	   WPROCESO.WP_XMLADD, 
	   WPROCESO.WP_XMLDOC
FROM WPROCESO
JOIN WMODELO ON ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
LEFT OUTER JOIN WPROSTEP ON ( WPROCESO.WP_FOLIO = WPROSTEP.WP_FOLIO ) AND ( WPROCESO.WP_PASO = WPROSTEP.WE_ORDEN )
LEFT OUTER JOIN WTAREA ON ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) AND ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
LEFT OUTER JOIN WMODSTEP ON ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) AND ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
GO

if exists (select * from dbo.sysobjects where id = object_id(N'V_WPCAMBIO') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_WPCAMBIO

GO 

/* CR:2681, Mejoras WF 2013 - VIEW V_WPCAMBIO - Cambios de salario individual */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPCAMBIO
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
t.c.value('EMPLEADO[1]', 'INT' ) as CB_CODIGO,
t.c.value('FECHAREG[1]','datetime') MV_FEC_REG,
t.c.value('FECHAIMSS[1]','datetime') MV_FEC_IMS,
t.c.value('CB_SALARIO[1]','Pesos') CB_SALARIO,
t.c.value('SALARIO_NEW[1]','Pesos') CA_SAL_NEW,
t.c.value('PORCENTAJE_AUMENTO[1]','Pesos') CA_POR_AUM,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'CAMBIO' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
t2.c2.value('EMPLEADO[1]', 'INT' ) CB_CODIGO,
t.c.value('FECHAREG[1]','datetime') MV_FEC_REG,
t.c.value('FECHAIMSS[1]','datetime') MV_FEC_IMS,
t2.c2.value('CB_SALARIO[1]', 'Pesos' ) CB_SALARIO,
t2.c2.value('SALARIO_NEW[1]', 'Pesos' ) CA_SAL_NEW,
t.c.value('PORCENTAJE_AUMENTO[1]','Pesos') CA_POR_AUM,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'CAMBIO' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'V_WPAUTO3') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_WPAUTO3

GO 

/* CR:2681, Mejoras WF 2013 - VIEW V_WPAUTO3 */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPAUTO3
as
select VP.WP_FOLIO, VP.CM_CODIGO, MODO=VP.WP_MODO, MV.WP_MOV3, VP.MV_DESCR, VP.MV_OBSERVA, VP.CB_CODIGO, VP.AT_FEC_AUT, VP.AT_TIPO, VP.AT_HORAS, VP.AT_MOTIVO
from VT_WPAUTO3 VP
join dbo.WPROCMOV3 MV on MV.WP_FOLIO = VP.WP_FOLIO
GO


if exists (select * from dbo.sysobjects where id = object_id(N'V_WPMULTIP') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_WPMULTIP

GO 

/* CR:2681, Mejoras WF 2013 - VIEW V_WPMULTIP - Modelo de Cambios M�ltiples */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPMULTIP
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
t.c.value('FECHAREG[1]', 'Fecha' ) MV_FEC_REG,
coalesce(t.c.value('DESCRIPCION[1]', 'Varchar(255)' ), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t.c.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
coalesce (t.c.value('CB_TURNO[1]', 'Codigo' ), '') as CB_TURNO,
coalesce (t.c.value('CB_NIVEL1[1]', 'Codigo' ), '') as CB_NIVEL1,
coalesce (t.c.value('CB_NIVEL2[1]', 'Codigo' ), '') as CB_NIVEL2,
coalesce (t.c.value('CB_NIVEL3[1]', 'Codigo' ), '') as CB_NIVEL3,
coalesce (t.c.value('CB_NIVEL4[1]', 'Codigo' ), '') as CB_NIVEL4,
coalesce (t.c.value('CB_NIVEL5[1]', 'Codigo' ), '') as CB_NIVEL5,
coalesce (t.c.value('CB_NIVEL6[1]', 'Codigo' ), '') as CB_NIVEL6,
coalesce (t.c.value('CB_NIVEL7[1]', 'Codigo' ), '') as CB_NIVEL7,
coalesce (t.c.value('CB_NIVEL8[1]', 'Codigo' ), '') as CB_NIVEL8,
coalesce (t.c.value('CB_NIVEL9[1]', 'Codigo' ), '') as CB_NIVEL9
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'MULTIP' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
t.c.value('FECHAREG[1]', 'Fecha' ) MV_FEC_REG,
coalesce( t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce( t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t2.c2.value('EMPLEADO[1]', 'NumeroEmpleado' ) as CB_CODIGO,
coalesce (t.c.value('CB_TURNO[1]', 'Codigo' ), '') as CB_TURNO,
coalesce (t.c.value('CB_NIVEL1[1]', 'Codigo' ), '') as CB_NIVEL1,
coalesce (t.c.value('CB_NIVEL2[1]', 'Codigo' ), '') as CB_NIVEL2,
coalesce (t.c.value('CB_NIVEL3[1]', 'Codigo' ), '') as CB_NIVEL3,
coalesce (t.c.value('CB_NIVEL4[1]', 'Codigo' ), '') as CB_NIVEL4,
coalesce (t.c.value('CB_NIVEL5[1]', 'Codigo' ), '') as CB_NIVEL5,
coalesce (t.c.value('CB_NIVEL6[1]', 'Codigo' ), '') as CB_NIVEL6,
coalesce (t.c.value('CB_NIVEL7[1]', 'Codigo' ), '') as CB_NIVEL7,
coalesce (t.c.value('CB_NIVEL8[1]', 'Codigo' ), '') as CB_NIVEL8,
coalesce (t.c.value('CB_NIVEL9[1]', 'Codigo' ), '') as CB_NIVEL9
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'MULTIP' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'V_WPPERM') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_WPPERM

GO 


/* CR:2681, Mejoras WF 2013 - VIEW V_WPPERM - Modelo de Permisos */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPPERM
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t.c.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('INICIO[1]','Fecha') PM_FEC_INI,
t.c.value('RETORNO[1]','Fecha') PM_FEC_RET,
t.c.value('DIAS[1]','Status') PM_DIAS,
t.c.value('CLASE[1]','Status') PM_CLASE,
t.c.value('TIPO[1]','Codigo') PM_TIPO,
t.c.value('REFERENCIA[1]','Descripcion') PM_REFEREN
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'PERM' and t.c.value('EMPLEADO[1]', 'INT' ) > 0
)
UNION ALL
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
coalesce (t.c.value('DESCRIPCION[1]','varchar(255)'), '') MV_DESCR,
coalesce (t.c.value('OBSERVACIONES[1]','varchar(255)'), '') MV_OBSERVA,
t2.c2.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('INICIO[1]','Fecha') PM_FEC_INI,
t.c.value('RETORNO[1]','Fecha') PM_FEC_RET,
t.c.value('DIAS[1]','Status') PM_DIAS,
t.c.value('CLASE[1]','Status') PM_CLASE,
t.c.value('TIPO[1]','Codigo') PM_TIPO,
t.c.value('REFERENCIA[1]','Descripcion') PM_REFEREN
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2)
where MV.WP_MOV3 = 'PERM' and
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' )
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null )
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'V_WPVACA') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_WPVACA

GO 


/* CR:2681, Mejoras WF 2013 - VIEW V_WPVACA - Modelo de Vacaciones */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPVACA
AS
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='INDIV', MV.WP_MOV3,
t.c.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('VA_FEC_INI[1]','Fecha') VA_FEC_INI, 
t.c.value('VA_FEC_FIN[1]','Fecha') VA_FEC_FIN, 
t.c.value('VA_SALD_GOZO[1]','Pesos') VA_SAL_GOZ,
t.c.value('VA_SALD_PAGO[1]','Pesos') VA_SAL_PAG,
t.c.value('VA_GOZO[1]','Pesos') VA_GOZO,
t.c.value('VA_PAGO[1]','Pesos') VA_PAGO,
t.c.value('VA_P_PRIMA[1]','Pesos') VA_P_PRIMA,
t.c.value('VA_PERIODO[1]','Descripcion') VA_PERIODO,
t.c.value('VA_COMENTA[1]','Descripcion') VA_COMENTA,
t.c.value('VA_NOMYEAR[1]','Int') VA_NOMYEAR,
t.c.value('VA_NOMTIPO[1]','Descripcion') VA_NOMTIPO,
t.c.value('VA_NOMNUME[1]','Int') VA_NOMNUME
from
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO  
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
where MV.WP_MOV3 = 'VACA' and t.c.value('EMPLEADO[1]', 'INT' ) > 0  
)
UNION ALL 
( select WP.WP_FOLIO, MV.WP_EMPRESA as CM_CODIGO, WP_MODO='MULT', MV.WP_MOV3,
t2.c2.value('EMPLEADO[1]', 'NumeroEmpleado' ) CB_CODIGO,
t.c.value('VA_FEC_INI[1]','Fecha') VA_FEC_INI, 
t.c.value('VA_FEC_FIN[1]','Fecha') VA_FEC_FIN, 
VA_SAL_GOZ=0,
VA_SAL_PAG=0,
t.c.value('VA_GOZO[1]','Pesos') VA_GOZO,
t.c.value('VA_PAGO[1]','Pesos') VA_PAGO,
t2.c2.value('VA_P_PRIMA[1]','Pesos') VA_P_PRIMA,
t.c.value('VA_PERIODO[1]','Descripcion') VA_PERIODO,
t.c.value('VA_COMENTA[1]','Descripcion') VA_COMENTA,
t.c.value('VA_NOMYEAR[1]','Int') VA_NOMYEAR,
t.c.value('VA_NOMTIPO[1]','Descripcion') VA_NOMTIPO,
t.c.value('VA_NOMNUME[1]','Int') VA_NOMNUME
 from 
WPROCESO WP join WPROCMOV3 MV on MV.WP_FOLIO = WP.WP_FOLIO  
CROSS APPLY WP_XMLDOC.nodes('//DATOS') as t(c)
cross apply t.c.nodes('//EMPLEADO') as t1(c1)
cross apply t.c.nodes('//INFO') as t2(c2) 
where MV.WP_MOV3 = 'VACA' and 
 ( t1.c1.value('.', 'INT' ) = t2.c2.value('EMPLEADO[1]', 'INT' ) 
	or t2.c2.value('EMPLEADO[1]', 'INT' ) is null ) 
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'SP_GET_XML_INFO') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
		drop procedure SP_GET_XML_INFO
GO 


/* CR:2681, Mejoras WF 2013 - procedure SP_GET_XML_INFO */
/* Patch 435 #Seq: 1*/
create procedure SP_GET_XML_INFO(
 @tabla formula,
 @campo formula ,  
 @campoLlave formula ,  
 @llave FolioGrande ,
 @path varchar(1024),
 @outValue varchar(1024) output,  
-- opcionales 
 @atributo formula = '',
 @pos status = 1 
)
as
begin 
	declare @value varchar(max)
	declare @sql nvarchar(max),
	@paramDefinition nvarchar(255),
	@paramValue varchar(max)

	if (@campo = 'WP_XMLDATA') 
		set @campo = 'WP_XMLDOC'

	if ( @pos <= 0 )  
		set @pos = 1 

	if @atributo = '' 
		set @path = '('+ @path+')['+str(@pos)+']' 
    else
		set @path = '('+ @path+'/@'+@atributo+')['+str(@pos)+']' 
		 
	set @sql = ' select top 1 @value='+@campo+'.value('''+@path+''', ''Varchar(max)'') from '+@tabla+' where '+@campoLlave+'='+str(@llave)
	set @paramDefinition = '@value Varchar(max) OUTPUT'

	begin try
		EXEC sp_executesql @sql, @paramDefinition, @value out
	end try
	begin  catch
		set @value = ''
	end catch

	set @outValue =  @value;
end
GO
GO

/* CR:2534, Terminales GTI 2013 - Creacion de Dominio MACADDRESS */
/* Patch 435 #Seq: 1*/
if not EXISTS ( select 1 from sys.types where is_user_defined = 1 and name = 'MACADDRESS' )
begin
     exec SP_ADDTYPE MACADDRESS, 'VARCHAR(50)', 'NOT NULL'
     exec SP_BINDEFAULT STRINGVACIO, MACADDRESS
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE HUELLAGTI */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'HUELLAGTI' and xtype = 'U' )
begin
     create table HUELLAGTI (
         HU_ID int IDENTITY,
         CM_CODIGO CodigoEmpresa,
         CB_CODIGO NumeroEmpleado,
         HU_INDICE USUARIO,
         HU_HUELLA Memo
     )
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE PK_HUELLAGTI */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_HUELLAGTI' and XTYPE = 'PK' )
begin
     alter table HUELLAGTI add constraint PK_HUELLAGTI primary key (CM_CODIGO ASC, CB_CODIGO ASC, HU_INDICE ASC);
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE MENSAJE */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'MENSAJE' and xtype = 'U' )
begin
     create table MENSAJE (
         DM_CODIGO  FolioChico,
         DM_MENSAJE Formula,
         DM_ORDEN   FolioChico,
         DM_SLEEP   FolioGrande,
         DM_ACTIVO  Booleano
     )
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE PK_MENSAJE */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_MENSAJE' and XTYPE = 'PK' )
begin
     alter table MENSAJE add constraint PK_MENSAJE primary key (DM_CODIGO ASC);
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE TERM_GPO */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'TERM_GPO' and xtype = 'U' )
begin
     CREATE TABLE TERM_GPO (
         TE_CODIGO NumeroEmpleado,
         GP_CODIGO Codigo,
     );
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE PK_TERM_GPO */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_TERM_GPO' and XTYPE = 'PK' )
begin
     alter table TERM_GPO add constraint PK_TERM_GPO primary key (TE_CODIGO ASC, GP_CODIGO ASC);
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE TERM_HUELLA */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'TERM_HUELLA' and xtype = 'U' )
begin
     create table TERM_HUELLA (
         CM_CODIGO CodigoEmpresa,
         CB_CODIGO NumeroEmpleado,
         HU_INDICE USUARIO,
         TE_CODIGO NumeroEmpleado
     )
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE PK_TERM_HUELLA */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_TERM_HUELLA' and XTYPE = 'PK' )
begin
     alter table TERM_HUELLA add constraint PK_TERM_HUELLA primary key (CM_CODIGO ASC, CB_CODIGO ASC, HU_INDICE ASC, TE_CODIGO ASC);
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE TERM_MSG */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'TERM_MSG' and xtype = 'U' )
begin
     create table TERM_MSG (
         DM_CODIGO FolioChico,
         TE_CODIGO NumeroEmpleado,
         TM_SLEEP   FolioGrande,
         TM_NEXT   Fecha
     )
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE PK_TERM_MSG */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_TERM_MSG' and XTYPE = 'PK' )
begin
     alter table TERM_MSG add constraint PK_TERM_MSG primary key (DM_CODIGO ASC, TE_CODIGO ASC);
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE TERMINAL */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'TERMINAL' and xtype = 'U' )
begin
     create table TERMINAL (
         TE_CODIGO NumeroEmpleado identity,
         TE_NOMBRE Descripcion,
         TE_NUMERO Pesos,
         TE_TEXTO  DescLarga,
         TE_MAC    MACADDRESS,
         TE_APP    Status,
         TE_BEAT   Fecha,
         TE_ZONA   DescLarga,
         TE_ACTIVO Booleano,
         TE_XML    Memo,
         TE_LOGO   Memo,
         TE_FONDO  Memo
     )
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE PK_TERMINAL */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_TERMINAL' and XTYPE = 'PK' )
begin
     alter table TERMINAL add constraint PK_TERMINAL primary key (TE_CODIGO ASC);
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE FK_HUELLAGTI_COMPANY */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_HUELLAGTI_COMPANY' and XTYPE = 'F')
begin
     alter table HUELLAGTI add constraint FK_HUELLAGTI_COMPANY FOREIGN KEY (CM_CODIGO) references COMPANY (CM_CODIGO) on delete cascade on update cascade;
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE FK_TERM_GPO_TERMINAL */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_TERM_GPO_TERMINAL' and XTYPE = 'F')
begin
     alter table TERM_GPO add constraint FK_TERM_GPO_TERMINAL FOREIGN KEY (TE_CODIGO) references TERMINAL (TE_CODIGO) on delete cascade on update cascade;
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE FK_TERM_GPO_GRUPOTERM */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_TERM_GPO_GRUPOTERM' and XTYPE = 'F')
begin
     alter table TERM_GPO add constraint FK_TERM_GPO_GRUPOTERM FOREIGN KEY (GP_CODIGO) references GRUPOTERM (GP_CODIGO) on delete cascade on update cascade;
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE FK_TERM_HUELLA_TERMINAL */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_TERM_HUELLA_TERMINAL' and XTYPE = 'F')
begin
     alter table TERM_HUELLA add constraint FK_TERM_HUELLA_TERMINAL FOREIGN KEY (TE_CODIGO) references TERMINAL (TE_CODIGO) on delete cascade on update cascade;
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE FK_TERM_HUELLA_HUELLAGTI */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_TERM_HUELLA_HUELLAGTI' and XTYPE = 'F')
begin
     alter table TERM_HUELLA add constraint FK_TERM_HUELLA_HUELLAGTI FOREIGN KEY (CM_CODIGO, CB_CODIGO, HU_INDICE) references HUELLAGTI (CM_CODIGO, CB_CODIGO, HU_INDICE) on delete cascade on update cascade;
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE FK_TERM_MSG_TERMINAL */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_TERM_MSG_TERMINAL' and XTYPE = 'F')
begin
     alter table TERM_MSG add constraint FK_TERM_MSG_TERMINAL FOREIGN KEY (TE_CODIGO) references TERMINAL (TE_CODIGO) on delete cascade on update cascade;
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE FK_DISPO_MSG_MENSAJE */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_DISPO_MSG_MENSAJE' and XTYPE = 'F')
begin
     alter table TERM_MSG add constraint FK_DISPO_MSG_MENSAJE FOREIGN KEY (DM_CODIGO) references MENSAJE (DM_CODIGO) on delete cascade on update cascade;
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE Columna de GRUPOTERM.GP_NUMERO */
/* Patch 435 #Seq: 1*/
if not exists( select * from syscolumns where NAME = 'GP_NUMERO' and id = object_id('GRUPOTERM'))
begin
     ALTER TABLE GRUPOTERM ADD GP_NUMERO Pesos null;
end
GO

/* CR:2534, Terminales GTI 2013 - Actualizacion de nuevos campo de GRUPOTERM.GP_NUMERO */
/* Patch 435 #Seq: 1*/
if exists( select * from syscolumns where NAME = 'GP_NUMERO' and id = object_id('GRUPOTERM'))
begin
     UPDATE GRUPOTERM set GP_NUMERO = 0
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE Columna de GRUPOTERM GP_NUMERO */
/* Patch 435 #Seq: 1*/
alter table GRUPOTERM alter column GP_NUMERO Pesos not null;
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE Columna de GRUPOTERM.GP_TEXTO */
/* Patch 435 #Seq: 1*/
if not exists( select * from syscolumns where NAME = 'GP_TEXTO' and id = object_id('GRUPOTERM'))
begin
     ALTER TABLE GRUPOTERM ADD GP_TEXTO DescLarga null;
end
GO

/* CR:2534, Terminales GTI 2013 - Actualizacion de nuevos campo de GRUPOTERM.GP_TEXTO */
/* Patch 435 #Seq: 1*/
if exists( select * from syscolumns where NAME = 'GP_TEXTO' and id = object_id('GRUPOTERM'))
begin
     UPDATE GRUPOTERM set GP_TEXTO = ''
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE Columna de GRUPOTERM.GP_TEXTO */
/* Patch 435 #Seq: 1*/
alter table GRUPOTERM alter column GP_TEXTO DescLarga not null;
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE Columna de GRUPOTERM.GP_ACTIVO */
/* Patch 435 #Seq: 1*/
if not exists( select * from syscolumns where NAME = 'GP_ACTIVO' and id = object_id('GRUPOTERM'))
begin
     ALTER TABLE GRUPOTERM ADD GP_ACTIVO Booleano null;
end
GO

/* CR:2534, Terminales GTI 2013 - Actualizacion de nuevos campo de GRUPOTERM.GP_ACTIVO */
/* Patch 435 #Seq: 1*/
if exists( select * from syscolumns where NAME = 'GP_ACTIVO' and id = object_id('GRUPOTERM'))
begin
     UPDATE GRUPOTERM set GP_ACTIVO = 'S'
end
GO

/* CR:2534, Terminales GTI 2013 - CREACION DE Columna de GRUPOTERM GP_ACTIVO */
/* Patch 435 #Seq: 1*/
alter table GRUPOTERM alter column GP_ACTIVO Booleano not null;
GO

/* CR:2534, Terminales GTI 2013 - Eliminacion de ID_NUMERO en EMP_BIO 1/2 */
/* Patch 435 #Seq: 1*/
if exists( select * from syscolumns where NAME = 'IV_CODIGO' and id = object_id('EMP_BIO'))
begin
     EXECUTE sp_unbindefault N'EMP_BIO.IV_CODIGO';
end
GO

/* CR:2534, Terminales GTI 2013 - Eliminacion de Usuario en EMP_BIO */
/* Patch 435 #Seq: 1*/
if exists( select * from syscolumns where NAME = 'IV_CODIGO' and id = object_id('EMP_BIO'))
begin
     ALTER TABLE EMP_BIO DROP COLUMN IV_CODIGO;
end
GO

/* CR:2534, Terminales GTI 2013 - Eliminacion de la tabla DISPOGRUPO */
/* Patch 435 #Seq: 1*/
if exists( select NAME from sysobjects where NAME = 'DISPOGRUPO' and XTYPE = 'U' )
begin
	DROP TABLE DISPOGRUPO
end
GO

/* CR:2534, Terminales GTI 2013 - Eliminacion de la columna DI_SYNC */
/* Patch 435 #Seq: 1*/
if exists( select * from syscolumns where NAME = 'DI_SYNC' and id = object_id('DISPOSIT'))
begin
     EXECUTE sp_unbindefault N'DISPOSIT.DI_SYNC';
end
GO

/* CR:2534, Terminales GTI 2013 - Eliminacion de la columna DI_SYNC */
/* Patch 435 #Seq: 1*/
if exists( select NAME from SYSCOLUMNS where NAME = 'DI_SYNC' and ID = object_id(N'DISPOSIT') )
begin
	ALTER TABLE DISPOSIT DROP COLUMN DI_SYNC;
end
GO

/* CR:2534, Terminales GTI 2013 - Inicializacion de datos en Tabla MENSAJE */
/* Patch 435 #Seq: 1*/
if not exists (select 1 from MENSAJE where DM_CODIGO = 1)
begin
     insert into MENSAJE values (1,'Actualizaci�n Software Terminal',1,0,'S')
end
GO

/* CR:2534, Terminales GTI 2013 - Mensaje de Actualizaci�n de Huellas */
/* Patch 435 #Seq: 1*/
if not exists (select 1 from MENSAJE where DM_CODIGO = 4)
begin
     insert into MENSAJE values (4,'Actualizaci�n de Huellas',4,1440,'S')
end
GO

/* CR:2534, Terminales GTI 2013 - Mensaje Actualizaci�n de Propiedades de Terminal */
/* Patch 435 #Seq: 1*/
if not exists (select 1 from MENSAJE where DM_CODIGO = 5)
begin
     insert into MENSAJE values (5,'Actualizaci�n de Propiedades de Terminal',5,0,'S')
end
GO

/* CR:2534, Terminales GTI 2013 - Mensaje de Depuracion de Huellas */
/* Patch 435 #Seq: 1*/
if not exists (select 1 from MENSAJE where DM_CODIGO = 6)
begin
     insert into MENSAJE values (6,'Depuracion de Huellas',6,1440,'S')
end
GO

/* CR:2534, Terminales GTI 2013 - Estructura de bitacora mejorada */
/* Patch 435 #Seq: 1*/
if not exists (select * from sys.columns where name = 'WS_MIENT' and object_id = object_id('WS_BITAC'))
begin
     ALTER TABLE WS_BITAC
     ADD WS_MIENT NombreImpresora null;
end
GO

/* CR:2534, Terminales GTI 2013 - Estructura de bitacora mejorada */
/* Patch 435 #Seq: 1*/
update WS_BITAC set WS_MIENT = '';
GO

/* CR:2534, Terminales GTI 2013 - Estructura de bitacora mejorada */
/* Patch 435 #Seq: 1*/
alter table WS_BITAC alter column WS_MIENT NombreImpresora not null;
GO

/* CR:2675, Cafeter�a con Biometrico - EMP_BIO.IV_CODIGO */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'IV_CODIGO' AND OBJECT_ID = OBJECT_ID('EMP_BIO'))
BEGIN
     ALTER TABLE EMP_BIO
     ADD IV_CODIGO NUMEROEMPLEADO null;
END
GO

/* CR:2675, Cafeter�a con Biometrico - EMP_BIO.IV_CODIGO */
/* Patch 435 #Seq: 1*/
update EMP_BIO set IV_CODIGO = 0;
GO

/* CR:2675, Cafeter�a con Biometrico - EMP_BIO.IV_CODIGO */
/* Patch 435 #Seq: 1*/
alter table EMP_BIO alter column IV_CODIGO NUMEROEMPLEADO not null;
GO

/* CR:2675, Cafeter�a con Biometrico - Drop PK_TERM_HUELLA */
/* Patch 435 #Seq: 1*/
IF (OBJECT_ID('PK_TERM_HUELLA') is not null)
BEGIN
	ALTER TABLE TERM_HUELLA
	DROP CONSTRAINT PK_TERM_HUELLA
END
GO

/* CR:2675, Cafeter�a con Biometrico - Drop FK_TERM_HUELLA_HUELLAGTI */
/* Patch 435 #Seq: 1*/
IF (OBJECT_ID('FK_TERM_HUELLA_HUELLAGTI') is not null)
BEGIN
	ALTER TABLE TERM_HUELLA
	DROP CONSTRAINT FK_TERM_HUELLA_HUELLAGTI
END
GO

/* CR:2675, Cafeter�a con Biometrico - Drop PK_HUELLAGTI */
/* Patch 435 #Seq: 1*/
IF (OBJECT_ID('PK_HUELLAGTI') is not null)
BEGIN
	alter table HUELLAGTI
	DROP CONSTRAINT PK_HUELLAGTI
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega TERM_HUELLA.LLAVE */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE NAME = 'LLAVE' AND OBJECT_ID = OBJECT_ID('TERM_HUELLA'))
BEGIN
	ALTER TABLE TERM_HUELLA
	ADD LLAVE INT IDENTITY not null
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega HUELLAGTI.IV_CODIGO */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'IV_CODIGO' AND OBJECT_ID = OBJECT_ID('HUELLAGTI'))
BEGIN
	alter table HUELLAGTI
	add IV_CODIGO NumeroEmpleado null;
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega HUELLAGTI.IV_CODIGO */
/* Patch 435 #Seq: 1*/
update HUELLAGTI set IV_CODIGO = 0;
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega HUELLAGTI.IV_CODIGO */
/* Patch 435 #Seq: 1*/
alter table HUELLAGTI alter column IV_CODIGO NumeroEmpleado not null;
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega TERM_HUELLA.IV_CODIGO */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'IV_CODIGO' AND OBJECT_ID = OBJECT_ID('TERM_HUELLA'))
BEGIN
	alter table TERM_HUELLA
	add IV_CODIGO NumeroEmpleado null;
END
GO

/* CR:2675, Cafeter�a con Biometrico - Inicializa TERM_HUELLA.IV_CODIGO */
/* Patch 435 #Seq: 1*/
IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'IV_CODIGO' AND OBJECT_ID = OBJECT_ID('TERM_HUELLA'))
BEGIN
	update TERM_HUELLA
	set IV_CODIGO = 0
END
GO

/* CR:2675, Cafeter�a con Biometrico - Not null TERM_HUELLA.IV_CODIGO */
/* Patch 435 #Seq: 1*/
IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'IV_CODIGO' AND OBJECT_ID = OBJECT_ID('TERM_HUELLA'))
BEGIN
	alter table TERM_HUELLA
	alter column IV_CODIGO numeroempleado not null
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega HUELLAGTI.HU_TIMEST */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'HU_TIMEST' AND OBJECT_ID = OBJECT_ID('HUELLAGTI'))
BEGIN
	ALTER TABLE HUELLAGTI
	ADD HU_TIMEST FECHA
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega PK_HUELLAGTI */
/* Patch 435 #Seq: 1*/
IF (OBJECT_ID('PK_HUELLAGTI') is null)
BEGIN
	alter table HUELLAGTI
	add constraint PK_HUELLAGTI primary key CLUSTERED (CM_CODIGO ASC, CB_CODIGO ASC, HU_INDICE ASC, IV_CODIGO ASC);
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega PK_TERM_HUELLA */
/* Patch 435 #Seq: 1*/
IF (OBJECT_ID('PK_TERM_HUELLA') is null)
BEGIN
	ALTER TABLE TERM_HUELLA
	ADD CONSTRAINT PK_TERM_HUELLA PRIMARY KEY CLUSTERED (CM_CODIGO ASC, CB_CODIGO ASC, HU_INDICE ASC, IV_CODIGO ASC, TE_CODIGO ASC)
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega FK_TERM_HUELLA_HUELLAGTI */
/* Patch 435 #Seq: 1*/
IF (OBJECT_ID('FK_TERM_HUELLA_HUELLAGTI') is null)
BEGIN
	ALTER TABLE TERM_HUELLA
	ADD CONSTRAINT FK_TERM_HUELLA_HUELLAGTI
	FOREIGN KEY (CM_CODIGO, CB_CODIGO, HU_INDICE, IV_CODIGO)
	REFERENCES HUELLAGTI(CM_CODIGO, CB_CODIGO, HU_INDICE, IV_CODIGO)
	ON DELETE CASCADE ON UPDATE CASCADE
END
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega tabla COM_HUELLA */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'COM_HUELLA' and xtype = 'u' )
begin
     CREATE TABLE COM_HUELLA (
     CH_CODIGO Accesos,
	HU_ID NumeroEmpleado)
end
GO

/* CR:2675, Cafeter�a con Biometrico - Agrega tabla PK_COM_HUELLA */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from sysobjects where name = 'PK_COM_HUELLA' and xtype = 'PK' )
begin
     alter table COM_HUELLA
     add constraint PK_COM_HUELLA primary key (CH_CODIGO, HU_ID)
end
GO

/* CR:2675, Cafeter�a con Biometrico - view V_HUELLAS */
/* Patch 435 #Seq: 1*/
create view V_HUELLAS
as
	/* Es importante el varchar max porque si no la huella se trunca */
     SELECT    H.HU_ID, E.ID_NUMERO, E.CM_CODIGO, E.CB_CODIGO, H.HU_INDICE, convert(varchar(max), H.HU_HUELLA) as HU_HUELLA, H.IV_CODIGO, H.HU_TIMEST, 'HUELLAS' as TIPO
     FROM         HUELLAGTI AS H INNER JOIN
                           EMP_BIO AS E ON H.CM_CODIGO = E.CM_CODIGO AND H.CB_CODIGO = E.CB_CODIGO
     WHERE ( H.IV_CODIGO = 0 )
     union
     SELECT    H.HU_ID, E.ID_NUMERO, E.CM_CODIGO, E.CB_CODIGO, H.HU_INDICE, convert(varchar(max), H.HU_HUELLA) as HU_HUELLA, H.IV_CODIGO, H.HU_TIMEST, 'INVITADORES' as TIPO
     FROM         HUELLAGTI AS H INNER JOIN
                           EMP_BIO AS E ON H.CM_CODIGO = E.CM_CODIGO AND H.IV_CODIGO = E.IV_CODIGO
     WHERE ( H.IV_CODIGO <> 0 )
GO

/* CR:2675, Cafeter�a con Biometrico - view V_INVITADORES */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_INVITADORES
AS
     select convert(varchar(max),ID_NUMERO) as ID_NUMERO, IV_CODIGO, 1 as IV_TIPO from EMP_ID where ( IV_CODIGO > 0 )
     union
     select convert(varchar(max),ID_NUMERO), IV_CODIGO, 2 from EMP_BIO where ( IV_CODIGO > 0 )
GO


/* CR:2682: Especificacion de Usa Caseta en tabla COMPANY   */
/* Patch 435 #Seq: X */
alter table COMPANY add CM_USACASE Booleano null
GO

/* CR:2682: Se inicializan campo segun si tienen digito de Empresa en tabla COMPANY   */
/* Patch 435 #Seq: X */
update COMPANY
set
	CM_USACASE = case
					  when CM_DIGITO <> '' then 'S'
					  else 'N'
				 end
where CM_USACASE is null
GO

/* CR:2704: Quitar propiedad null al campo Usa Caseta de tabla COMPANY */
/* Patch 435 #Seq: X */
alter table COMPANY alter column CM_USACASE Booleano not null
GO

/* BN4596:: Derecho de acceso se visualizan incorrectamente */
/* Patch 435 #Seq: X */
update ACC_DER
set AD_ACCION = 'Agregar'
where ( AA_SOURCE = 0 ) and ( AX_NUMERO = 29 ) and ( AD_TIPO = 0 )
go
/* BN4596:: Derecho de acceso se visualizan incorrectamente */
/* Patch 435 #Seq: X */
update ACC_DER
set AD_ACCION = 'Permitir Excedentes en Plazas'
where ( AA_SOURCE = 0 ) and ( AX_NUMERO = 29 ) and ( AD_TIPO = 1 )
go
/* BN4596:: Derecho de acceso se visualizan incorrectamente */
/* Patch 435 #Seq: X */
update ACC_DER
set AD_TIPO = AD_POSICIO 
where ( AA_SOURCE = 0 ) and ( AX_NUMERO = 589 ) and ( AD_TIPO = 2 )
go
/* BN4596:: Derecho de acceso se visualizan incorrectamente */
/* Patch 435 #Seq: X */
update ACC_DER
set AD_TIPO = AD_POSICIO 
where ( AA_SOURCE = 0 ) and ( AX_NUMERO = 589 ) and ( AD_TIPO = 4 )
go
/* BN4596:: Derecho de acceso se visualizan incorrectamente */
/* Patch 435 #Seq: X */
update ACC_DER
set AD_TIPO = AD_POSICIO 
where ( AA_SOURCE = 0 ) and ( AX_NUMERO = 589 ) and ( AD_TIPO = 6 )
go
/* BN4596:: Derecho de acceso se visualizan incorrectamente */
/* Patch 435 #Seq: X */
update ACC_DER
set AD_TIPO = AD_POSICIO 
where ( AA_SOURCE = 0 ) and ( AX_NUMERO = 606 ) and ( AD_TIPO = 4 )
go
/* BN4596:: Derecho de acceso se visualizan incorrectamente */
/* Patch 435 #Seq: X */
update ACC_DER
set AD_TIPO = AD_POSICIO 
where ( AA_SOURCE = 0 ) and ( AX_NUMERO = 606 ) and ( AD_TIPO = 7 )
go
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   TRIGGERS COMPARTE      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* CR:2681, Mejoras WF 2013 - Trigger TD_WPROCESO_XML */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_WPROCESO_XML  ON WPROCESO AFTER DELETE
AS
BEGIN
	SET NOCOUNT ON;
  /* Borrar Autorizaciones de la Vista Tabla */
	delete from VT_WPAUTO3 where WP_FOLIO in ( select WP_FOLIO from deleted where WP_MOV3 in ( 'AUTO3', 'AUTO3MULT') )
END

GO

/* CR:2681, Mejoras WF 2013 - Trigger TU_WPROCESO */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_WPROCESO  ON  WPROCESO AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	IF UPDATE(WP_XMLDATA)
	begin
		declare @WP_FOLIO FolioGrande
		
		select top 1 @WP_FOLIO = WP_FOLIO from Inserted 

		exec SP_REPLICA_XMLDOC @WP_FOLIO

		update WPROCESO
		set WP_MOV3 = coalesce( cast( WPROCESO.WP_XMLDOC.query('data(//DATOS/MOV3[1])') as varchar(30) ) , '' ) ,
			WP_EMPRESA = coalesce(  cast( WPROCESO.WP_XMLDOC.query('data(//DATOS/EMPRESA[1])') as varchar(30) ), '' ) 
		from Inserted where WPROCESO.WP_FOLIO = Inserted.WP_FOLIO

		/* Actualizacion de la Vista Tabla de Autorizaciones  */
		declare @i int
		declare @n int


		select IDENTITY(int, 1, 1) as RowID, Inserted.WP_FOLIO into #tempFolios 
		from Inserted 
		join WPROCESO on Inserted.WP_FOLIO = WPROCESO.WP_FOLIO  and   WPROCESO.WP_MOV3  in ( 'AUTO3', 'AUTO3MULT')
		select @n = count(*) from #tempFolios 

		if (@n =1)
		begin
			delete from VT_WPAUTO3 where WP_FOLIO in ( select WP_FOLIO from  #tempFolios )
			set @i = 1
			while @i <= @n
			begin
				select @WP_FOLIO = WP_FOLIO from #tempFolios where RowID = @i
				insert into VT_WPAUTO3 (WP_FOLIO, CM_CODIGO, WP_MODO, CB_CODIGO, AT_FEC_AUT, AT_TIPO, AT_HORAS, AT_MOTIVO)
				select V.WP_FOLIO, V.CM_CODIGO, V.WP_MODO, V.CB_CODIGO, V.AT_FEC_AUT, V.AT_TIPO, V.AT_HORAS, V.AT_MOTIVO
				from  V_WPAUTO3_PRE_PROCESS V where WP_FOLIO = @WP_FOLIO

				set @i = @i + 1
			end
		end
		else
		if ( @n > 1 )
		begin
			insert into VT_WPAUTO3 (WP_FOLIO, CM_CODIGO, WP_MODO, CB_CODIGO, AT_FEC_AUT, AT_TIPO, AT_HORAS, AT_MOTIVO)
			select V.WP_FOLIO, V.CM_CODIGO, V.WP_MODO, V.CB_CODIGO, V.AT_FEC_AUT, V.AT_TIPO, V.AT_HORAS, V.AT_MOTIVO
			from  V_WPAUTO3_PRE_PROCESS V where WP_FOLIO in (SELECT WP_FOLIO FROM #tempFolios )
		end
		drop table #tempFolios
	end
END

GO 

/* CR:2534, Terminales GTI 2013 - trigger TR_ADD_TERMINAL */
/* Patch 435 #Seq: 1*/
create trigger TR_ADD_TERMINAL on TERMINAL after insert
as
begin
	set nocount on

	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx
	declare ListaMensajes cursor for
		select DM_CODIGO, DM_SLEEP from MENSAJE where DM_ACTIVO='S' order by DM_ORDEN
		
	select @Terminal = TE_CODIGO from Inserted;
	
	open ListaMensajes
	fetch next from ListaMensajes into @Mensaje, @Sleep
	while (@@Fetch_Status = 0)
	begin
		insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP) values (@Terminal, @Mensaje, @Sleep)

		fetch next from ListaMensajes into @Mensaje, @Sleep
	end
	close ListaMensajes
	deallocate ListaMensajes
	
	update TERMINAL set TE_XML = 
'<?xml version="1.0" encoding="utf-8"?>
<properties>
   <ClockId>'+@Terminal+'</ClockId> 
   <WebService>http://SERVIDORWEB/WSAsistencia/WSAsistencia.asmx</WebService>
   <UseProxy>false</UseProxy>
   <Proxy>192.168.0.30</Proxy>
   <ProxyPort>8080</ProxyPort>
   <UseProxyAuthentication>false</UseProxyAuthentication>
   <ProxyUserName>NULL</ProxyUserName>
   <ProxyPassword>NULL</ProxyPassword>
   <Ip>DHCP</Ip>
   <NetMask></NetMask>
   <DefaultGateWay></DefaultGateWay>
   <DNS></DNS>
   <TimeOut>07</TimeOut>
   <HeartBeatTimer>60</HeartBeatTimer>
   <MsgTime>03</MsgTime>
   <ErrorMsgTime>05</ErrorMsgTime>
   <RelayTime>02</RelayTime>
   <DateTimeFormat>dd/MM hh:mm:ss</DateTimeFormat>
   <Volume>70</Volume>
   <ProxReader>ON</ProxReader>
   <BarCodeReader>ON</BarCodeReader>
   <HID>ON</HID>
   <MenuAccessCode>1234</MenuAccessCode>
   <FpuNotIdetifyMessage>INVALIDO/INTENTE DE NUEVO</FpuNotIdetifyMessage>
   <NotValidByServerMessage>Servidor no pudo validar ID</NotValidByServerMessage>
   <DuplicateMessage>Registro Duplicado</DuplicateMessage>
   <AutenticateMessage>Exito</AutenticateMessage>
   <OfflineValidation>Offline-Almacenada</OfflineValidation>
   <OneMinuteValidate>true</OneMinuteValidate>
   <FingerVerify>false</FingerVerify>
   <EnableOfflineRelay>false</EnableOfflineRelay>
   <FingerPrint>ON</FingerPrint>
   <EnableKeyPad>true</EnableKeyPad>
   <!-- Etiquetas default -->
   <Version>TRESS</Version>
   <Account>0</Account>
   <ClientId>0</ClientId>
   <Password>0</Password>
   <CajaId>0</CajaId>
   <Identidad>1</Identidad>
   <UseSoapActionHeader>true</UseSoapActionHeader>
   <SoapActionHeader>Synel/Services</SoapActionHeader>
   <UpdateTimeByServer>true</UpdateTimeByServer>
   <ntpServer>SERVIDORNTP</ntpServer>
   <timeZone>Tijuana</timeZone>
   <MifareReader>OFF</MifareReader>
   <MagneticReader>OFF</MagneticReader>
   <ShowCameraPhoto>false</ShowCameraPhoto>
   <ShowPhoto>true</ShowPhoto>
   <Schedules>
   <Function code="1" start="00:00" end="23:59" bg="comida.jpg">Comida</Function>
   <Function code="3" start="12:01" end="14:00" bg="desayuno.jpg">Desayuno</Function>
   <Function code="2" start="14:01" end="23:59" bg="cena.jpg">Cena</Function>
   </Schedules>
</properties>' 
	where TE_CODIGO = @Terminal
end
GO

/* CR:2534, Terminales GTI 2013 - trigger TR_UPDATE_TERMINAL */
/* Patch 435 #Seq: 1*/
create trigger TR_UPDATE_TERMINAL on TERMINAL after update
as
begin
	set nocount on
	declare @Terminal RelojLinx
	select @Terminal = TE_CODIGO from Inserted

	if update( TE_XML ) or update( TE_APP )
	begin
		UPDATE TERM_MSG SET TM_NEXT = '18991230' WHERE (TE_CODIGO = @Terminal) AND (DM_CODIGO = 5)
	end
end
GO

/* CR:2534, Terminales GTI 2013 - Crear trigger TR_UPDATE_EMP_BIO */
/* Patch 435 #Seq: 1*/
create trigger TR_UPDATE_EMP_BIO on EMP_BIO after update
as
begin
	set nocount on

	declare @Empleado FolioGrande
	declare @EmpleadoAnterior FolioGrande
	declare @Empresa CodigoEmpresa
	declare @EmpresaAnterior CodigoEmpresa
	select @EmpleadoAnterior = CB_CODIGO, @EmpresaAnterior = CM_CODIGO from Deleted
	select @Empleado = CB_CODIGO, @Empresa  = CM_CODIGO from Inserted

	if( update( CB_CODIGO ) or update( CM_CODIGO ) )
	begin
		UPDATE HUELLAGTI SET CB_CODIGO = @Empleado, CM_CODIGO = @Empresa WHERE (CM_CODIGO = @EmpresaAnterior) AND (CB_CODIGO = @EmpleadoAnterior)
	end
end
GO

/* CR:2675, Cafeter�a con Biometrico - trigger TR_UPDATE_HUELLAGTI */
/* Patch 435 #Seq: 1*/
create trigger TR_UPDATE_HUELLAGTI
on HUELLAGTI
AFTER INSERT, UPDATE
as
begin
     set nocount on

     /* Trigger de actualizacion de Timestamp de huella gti */
     declare @HU_ID NumeroEmpleado

     select @HU_ID = HU_ID from Inserted

     if UPDATE( HU_HUELLA )
     begin
          update HUELLAGTI set HU_TIMEST = GETDATE() where HU_ID = @HU_ID
     end
end
GO

/* CR:2675, Cafeter�a con Biometrico - trigger TR_DELETE_HUELLAGTI */
/* Patch 435 #Seq: 1*/
CREATE trigger TR_DELETE_HUELLAGTI
on HUELLAGTI
AFTER DELETE
as
begin
     set nocount on

     declare @HU_ID NumeroEmpleado

     select @HU_ID = HU_ID from deleted

     delete from COM_HUELLA where HU_ID = @HU_ID
end
GO



/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   STORE PROCEDURES - FUNCIONES  $$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* CR:2651, CR:2652 */
/* Patch 435 #Seq: 1*/
if exists( select NAME from SYSOBJECTS where NAME = 'PE_GET_OBSERVACIONES' )
	drop function PE_GET_OBSERVACIONES
GO

CREATE FUNCTION PE_GET_OBSERVACIONES
(	@FOLIO INTEGER )
RETURNS @WFOBSERVACIONES
TABLE(	USR_NXT VARCHAR( 30 ),  NOTA VARCHAR( 8000 ), USR_ORG VARCHAR( 30 ), FECHA VARCHAR(10), WN_MENSAJE VARCHAR(30), WE_ORDEN INT, USR_INI VARCHAR( 30 ))
AS BEGIN
DECLARE @FECHA DATETIME;
DECLARE	@USUARIOINICIAL INTEGER;
DECLARE @USUARIOFINAL INTEGER;
DECLARE @USUARIOPASO INTEGER;
DECLARE @NOTAS VARCHAR( 8000 );
DECLARE	@NOMBREUSUARIOINICIAL VARCHAR(30);
DECLARE @NOMBREUSUARIOFINAL VARCHAR(30);
DECLARE @NOMBREUSUARIOPASO VARCHAR(30);
DECLARE	@WN_MENSAJE VARCHAR(30);
DECLARE @WE_ORDEN INT
DECLARE TOTALOBSERVACIONES
CURSOR FOR
SELECT WM.WN_USR_INI, WM.WN_USR_FIN, WM.WN_NOTAS, WM.WN_FECHA, WM.WN_MENSAJE, WM.WE_ORDEN
FROM WMENSAJE WM, USUARIO U
	WHERE ( WM.WN_USR_INI = U.US_CODIGO )
	AND ( WM.WN_NOTAS IS NOT NULL )
	AND ( WM.WP_FOLIO = @FOLIO AND WM.WY_FOLIO = '0' )
	AND ( WM.WE_ORDEN IN
		( SELECT WPS.WE_ORDEN  FROM WPROSTEP WPS  WHERE ( WPS.WP_FOLIO = WM.WP_FOLIO )
			AND ( WPS.WA_CODIGO <> 'GRABA3' ) ) )
	ORDER BY WM.WN_FECHA

OPEN TOTALOBSERVACIONES
	FETCH NEXT
	FROM TOTALOBSERVACIONES
		INTO @USUARIOINICIAL, @USUARIOFINAL, @NOTAS, @FECHA, @WN_MENSAJE, @WE_ORDEN;

	SELECT @USUARIOPASO = @USUARIOINICIAL;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @NOMBREUSUARIOINICIAL = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIOINICIAL );
		SELECT @NOMBREUSUARIOFINAL = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIOFINAL );
		SELECT @NOMBREUSUARIOPASO = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIOPASO );

		INSERT INTO @WFOBSERVACIONES( USR_NXT, NOTA , USR_ORG ,FECHA, WN_MENSAJE , WE_ORDEN, USR_INI)
		VALUES( @NOMBREUSUARIOFINAL, @NOTAS, @NOMBREUSUARIOINICIAL,
			CONVERT(VARCHAR,CONVERT(DATETIME,@FECHA,103),103), @WN_MENSAJE, @WE_ORDEN, @NOMBREUSUARIOPASO );

		SELECT @USUARIOPASO = @USUARIOFINAL;

		FETCH NEXT FROM TOTALOBSERVACIONES
			INTO @USUARIOINICIAL, @USUARIOFINAL, @NOTAS, @FECHA, @WN_MENSAJE, @WE_ORDEN
	END
CLOSE TOTALOBSERVACIONES
DEALLOCATE TOTALOBSERVACIONES
RETURN
END
GO

/* CR:2681, Mejoras WF 2013 - Nueva Funci�n FN_USUARIO_ALTERNO */
/* Patch 435 #Seq: 1*/
CREATE  FUNCTION dbo.FN_USUARIO_ALTERNO (@US_CODIGO SMALLINT)
RETURNS SMALLINT
AS
BEGIN
	DECLARE @US_ALTERNO SMALLINT;	
	DECLARE @US_WF_OUT booleano;
	
	-- FUERA DE OFICINA DEL USUARIO ALTERNO
	SELECT @US_ALTERNO = US_CODIGO, @US_WF_OUT = ISNULL( US_WF_OUT ,'N' )
		FROM USUARIO 
		WHERE US_CODIGO = (SELECT US_WF_ALT FROM USUARIO WHERE US_CODIGO = @US_CODIGO);
	
	WHILE @US_WF_OUT = 'S'
	BEGIN
		SET @US_CODIGO = @US_ALTERNO;
		SELECT @US_ALTERNO = US_CODIGO, @US_WF_OUT = ISNULL( US_WF_OUT ,'N' )
			FROM USUARIO 
			WHERE US_CODIGO = (SELECT US_WF_ALT FROM USUARIO WHERE US_CODIGO = @US_CODIGO);		
	END	
	
	RETURN @US_ALTERNO
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'SP_GET_XML_INFO') and OBJECTPROPERTY(id, N'IsProcedure') = 1) 
		drop procedure SP_GET_XML_INFO
GO 

/* CR:2681, Mejoras WF 2013 - procedure SP_GET_XML_INFO */
/* Patch 435 #Seq: 1*/
create procedure SP_GET_XML_INFO(
 @tabla formula,
 @campo formula ,  
 @campoLlave formula ,  
 @llave FolioGrande ,
 @path varchar(1024),
 @outValue varchar(1024) output,  
-- opcionales 
 @atributo formula = '',
 @pos status = 1 
)
as
begin 
	declare @value varchar(max)
	declare @sql nvarchar(max),
	@paramDefinition nvarchar(255),
	@paramValue varchar(max)

	if ( @pos <= 0 )  
		set @pos = 1 

	if @atributo = '' 
		set @path = '('+ @path+')['+str(@pos)+']' 
    else
		set @path = '('+ @path+'/@'+@atributo+')['+str(@pos)+']' 
		 
	set @sql = ' select top 1 @value='+@campo+'.value('''+@path+''', ''Varchar(max)'') from '+@tabla+' where '+@campoLlave+'='+str(@llave)
	set @paramDefinition = '@value Varchar(max) OUTPUT'

	begin try
		EXEC sp_executesql @sql, @paramDefinition, @value out
	end try
	begin  catch
		set @value = ''
	end catch

	set @outValue =  @value;
end
GO

/* CR:2681, Mejoras WF 2013 - Cambios a procedure SP_CREA_WORKFLOW */
/* Patch 435 #Seq: 1*/
if exists( select NAME from SYSOBJECTS where NAME = 'SP_CREA_WORKFLOW' )
	drop PROCEDURE SP_CREA_WORKFLOW
GO

CREATE PROCEDURE DBO.SP_CREA_WORKFLOW(
  @MODELO    CODIGO,
  @USUARIO   USUARIO,
  @DESTINO   USUARIO,
  @DATA      XMLDATA,
  @NOTAS     NOTAS,
  @XMLADD      XML, 
  @FOLIO     FOLIOGRANDE OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @AF_ID   CHAR(6)
  DECLARE @FECHA   DATETIME
  SET     @FECHA = GETDATE()

  DECLARE @PASOS  FOLIOCHICO
  DECLARE @NOMBRE OBSERVACIONES

  SELECT  @PASOS = WM_PASOS, @NOMBRE = WM_NOMBRE
  FROM    WMODELO
  WHERE   WM_CODIGO = @MODELO

  SELECT @FOLIO = MAX( WP_FOLIO )+1
  FROM   WPROCESO
  IF (@FOLIO IS NULL) SELECT @FOLIO = 1

  -- CREA REGISTRO PROCESO, PADRE DE WORKFLOW
  INSERT INTO WPROCESO ( WP_FOLIO, WM_CODIGO, WP_NOMBRE, WP_USR_INI, WP_FEC_INI, WP_FEC_FIN, WP_STATUS, WP_PASOS, WP_PASO, WP_XMLDATA, WP_NOTAS, WP_XMLADD )
  VALUES ( @FOLIO, @MODELO, @NOMBRE, @USUARIO, @FECHA, @FECHA, 0, @PASOS, 0, @DATA, @NOTAS, @XMLADD )

  INSERT INTO WPROSTEP ( WP_FOLIO, WE_ORDEN, WA_CODIGO, WM_CODIGO, WS_NOMBRE, WS_STATUS )
  SELECT @FOLIO, WE_ORDEN, WA_CODIGO, WM_CODIGO, WE_NOMBRE, 0
  FROM   WMODSTEP
  WHERE  WM_CODIGO = @MODELO

  UPDATE WPROSTEP
  SET    WS_NEXT = WE_ORDEN + 1
  WHERE  WP_FOLIO = @FOLIO AND WE_ORDEN < @PASOS

  EXEC SP_ACTIVA_WORKFLOW @FOLIO, @USUARIO, @DESTINO, @NOTAS
  RETURN
END
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_IDENTIFICA */
/* Patch 435 #Seq: 1*/
create procedure WS_TERMINAL_IDENTIFICA
as
begin
     --Terminales Activas con la mac proporcionada
     SELECT TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA, TE_ACTIVO, TE_XML, TE_LOGO, TE_FONDO
     FROM TERMINAL
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_HEARTBEAT */
/* Patch 435 #Seq: 1*/
create procedure WS_TERMINAL_HEARTBEAT
(
     @TerminalID NumeroEmpleado
)
as
begin
     --Terminales Activas con la mac proporcionada
     SELECT TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA, TE_ACTIVO, TE_XML, TE_LOGO, TE_FONDO
     FROM TERMINAL
     WHERE (TE_CODIGO = @TerminalID)
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_SETHEARTBEAT */
/* Patch 435 #Seq: 1*/
create procedure WS_TERMINAL_SETHEARTBEAT
(
     @TerminalID NumeroEmpleado
)
as
begin
     --Terminales Activas con la mac proporcionada
     UPDATE TERMINAL
     SET TE_BEAT = GETDATE()
     WHERE (TE_CODIGO = @TerminalID)
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_GETPENDIENTE */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE WS_TERMINAL_GETPENDIENTE
(
     @TE_CODIGO NumeroEmpleado
)
as
begin
     SELECT     TOP (1) M.DM_CODIGO
     FROM         TERMINAL AS T INNER JOIN
                           TERM_MSG AS R ON T.TE_CODIGO = R.TE_CODIGO INNER JOIN
                           MENSAJE AS M ON R.DM_CODIGO = M.DM_CODIGO
     WHERE     (T.TE_ACTIVO = 'S') AND (M.DM_ACTIVO = 'S') AND (R.TM_NEXT <= GETDATE()) AND (T.TE_CODIGO = @TE_CODIGO)
     ORDER BY R.TM_NEXT, M.DM_ORDEN
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_SETPENDIENTE */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE WS_TERMINAL_SETPENDIENTE
(
     @TE_CODIGO NumeroEmpleado,
     @DM_CODIGO Codigo
)
as
begin
     update TERM_MSG
     set TM_NEXT = case when TM_SLEEP = 0 then '2999-12-31' else DATEADD(mi, TM_SLEEP, GETDATE()) end
     where DM_CODIGO = @DM_CODIGO and TE_CODIGO =@TE_CODIGO
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_BORRAHUELLA */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE WS_TERMINAL_BORRAHUELLA
(
     @TE_CODIGO NumeroEmpleado,
     @ID_NUMERO int
)
as
begin
     DECLARE @CM_CODIGO Codigoempresa
     DECLARE @CB_CODIGO NumeroEmpleado
     DECLARE @HU_INDICE usuario

     SELECT @CM_CODIGO = CM_CODIGO, @CB_CODIGO = CB_CODIGO FROM EMP_BIO where ID_NUMERO = @ID_NUMERO

     DELETE FROM TERM_HUELLA
     WHERE     (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (TE_CODIGO = @TE_CODIGO)
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_AGREGAHUELLA */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE WS_TERMINAL_AGREGAHUELLA
(
     @CM_CODIGO CodigoEmpresa,
     @CB_CODIGO NumeroEmpleado,
     @HU_INDICE Usuario,
     @HU_HUELLA Memo
)
as
begin
     if not exists (SELECT     CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA FROM         HUELLAGTI
                    WHERE     (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (HU_INDICE = @HU_INDICE))
     begin
          INSERT INTO HUELLAGTI (CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA)
          VALUES (@CM_CODIGO,@CB_CODIGO,@HU_INDICE,@HU_HUELLA)
     end
     else
     begin
          UPDATE HUELLAGTI
          SET HU_HUELLA = @HU_HUELLA
          WHERE CM_CODIGO = @CM_CODIGO and CB_CODIGO = @CB_CODIGO and HU_INDICE = @HU_INDICE
     end
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_BUSCAGAFETE */
/* Patch 435 #Seq: 1*/
create procedure WS_TERMINAL_BUSCAGAFETE  ( @ID_NUMERO NumeroEmpleado )
as
begin
     SELECT     CM_CODIGO, CB_CODIGO
     FROM         EMP_BIO
     WHERE     (ID_NUMERO = @ID_NUMERO)
end
GO

/* CR:2534, Terminales GTI 2013 - procedure WS_TERMINAL_HUELLASACTUALES */
/* Patch 435 #Seq: 1*/
create PROCEDURE WS_TERMINAL_HUELLASACTUALES
(
     @TE_CODIGO NumeroEmpleado --Terminal a verificar
) as
begin
     SELECT     TH.CM_CODIGO, TH.CB_CODIGO, EB.ID_NUMERO
     FROM         TERM_HUELLA AS TH INNER JOIN
                      EMP_BIO AS EB ON TH.CB_CODIGO = EB.CB_CODIGO AND TH.CM_CODIGO = EB.CM_CODIGO
     WHERE     (TH.TE_CODIGO = @TE_CODIGO)
     GROUP BY TH.CM_CODIGO, TH.CB_CODIGO, TH.TE_CODIGO, EB.ID_NUMERO
end
GO

/* CR:2675, Cafeter�a con Biometrico - Stored Procedure para insercion / Actualizacion de la huella de un empleado */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE SP_REPORTAHUELLA
(
     @CH_CODIGO Accesos,
     @HU_ID Numeroempleado
)
as
BEGIN
     if not exists (select 1 from COM_HUELLA where CH_CODIGO = @CH_CODIGO and HU_ID = @HU_ID)
     begin
          insert into COM_HUELLA values (@CH_CODIGO, @HU_ID)
     end
END
GO

/* CR:2675, Cafeter�a con Biometrico - procedure SP_HUELLAGTI_INSERTA */
/* Patch 435 #Seq: 1*/
create procedure SP_HUELLAGTI_INSERTA
(
     @CM_CODIGO CodigoEmpresa,
     @CB_CODIGO NumeroEmpleado,
     @HU_INDICE numeroempleado,
     @HU_HUELLA VARCHAR(MAX),
     @IV_CODIGO NumeroEmpleado,
     @CH_CODIGO Accesos
)
as
Begin

     if ( @IV_CODIGO = 0 ) and ( @CB_CODIGO <> 0 )
     begin
          if exists ( select * from HUELLAGTI where CB_CODIGO = @CB_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE )
          begin
               update HUELLAGTI
               set HU_HUELLA = @HU_HUELLA
               where CB_CODIGO = @CB_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE
          end
          else
          begin
               insert into HUELLAGTI ( CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA, IV_CODIGO )
               values ( @CM_CODIGO, @CB_CODIGO, @HU_INDICE, @HU_HUELLA, @IV_CODIGO )
          end
     end
     else
     begin
          if exists ( select * from HUELLAGTI where IV_CODIGO = @IV_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE )
          begin
               update HUELLAGTI
               set HU_HUELLA = @HU_HUELLA, CB_CODIGO = @CB_CODIGO
               where IV_CODIGO = @IV_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE
          end
          else
          begin
               insert into HUELLAGTI ( CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA, IV_CODIGO )
               values ( @CM_CODIGO, @CB_CODIGO, @HU_INDICE, @HU_HUELLA, @IV_CODIGO )
          end
     end

     exec SP_REPORTAHUELLA @CH_CODIGO, @@IDENTITY
end
GO

/* CR:2675, Cafeter�a con Biometrico, CR:2534, Terminales GTI 2013 - PROCEDURE WS_TERMINAL_SETAGREGAHUELLA */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE WS_TERMINAL_SETAGREGAHUELLA
(
     @TE_CODIGO NumeroEmpleado,
     @HU_ID int
)
as
begin
     insert into TERM_HUELLA(CM_CODIGO, CB_CODIGO, HU_INDICE, TE_CODIGO)
     SELECT     CM_CODIGO, CB_CODIGO, HU_INDICE, @TE_CODIGO
     FROM         HUELLAGTI
     WHERE     (HU_ID = @HU_ID)
end
GO

/* CR:2675, Cafeter�a con Biometrico - PROCEDURE WS_TERMINAL_HUELLAFALTANTE */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE WS_TERMINAL_HUELLAFALTANTE
(
     @TE_CODIGO NumeroEmpleado
)
as
begin
     --Se realiza un cruce de informacion entre dos tablas
 SELECT DEBERIA.ID_NUMERO, DEBERIA.HU_ID, DEBERIA.TE_CODIGO, DEBERIA.GP_CODIGO, DEBERIA.CM_CODIGO, DEBERIA.CB_CODIGO, DEBERIA.HU_HUELLA, DEBERIA.HU_INDICE
     FROM         (
          /*La primera tabla es las huellas que deberia tener la terminal*/
          SELECT     R.TE_CODIGO, R.GP_CODIGO, E.CM_CODIGO, E.CB_CODIGO, H.HU_HUELLA, H.HU_INDICE, E.ID_NUMERO, H.HU_ID, H.IV_CODIGO
          FROM         TERMINAL AS T INNER JOIN
                                TERM_GPO AS R ON T.TE_CODIGO = R.TE_CODIGO INNER JOIN
                                EMP_BIO AS E ON R.GP_CODIGO = E.GP_CODIGO INNER JOIN
                                HUELLAGTI AS H ON E.CM_CODIGO = H.CM_CODIGO AND E.CB_CODIGO = H.CB_CODIGO
          WHERE     (T.TE_CODIGO = @TE_CODIGO)

                    ) AS DEBERIA LEFT OUTER JOIN (
          /*La segunda tabla es las huellas que tiene la terminal*/
          SELECT     T.TE_CODIGO, H.CM_CODIGO, H.CB_CODIGO, H.HU_INDICE--, H.IV_CODIGO
          FROM         TERMINAL AS T INNER JOIN
                                TERM_HUELLA AS R ON T.TE_CODIGO = R.TE_CODIGO INNER JOIN
                                HUELLAGTI AS H ON R.CM_CODIGO = H.CM_CODIGO AND R.CB_CODIGO = H.CB_CODIGO AND R.HU_INDICE = H.HU_INDICE
          WHERE     (T.TE_CODIGO = @TE_CODIGO)
                    ) AS HAY ON
                           DEBERIA.TE_CODIGO = HAY.TE_CODIGO AND DEBERIA.CM_CODIGO = HAY.CM_CODIGO AND DEBERIA.CB_CODIGO = HAY.CB_CODIGO AND
                           DEBERIA.HU_INDICE = HAY.HU_INDICE
     /*La condicion es que traiga las huellas de la primer tabla que no haya dentro de la segunda tabla.*/
     WHERE     (HAY.HU_INDICE IS NULL) and ( DEBERIA.IV_CODIGO = 0 )
end
GO

/* CR:2675, Cafeter�a con Biometrico - PROCEDURE WS_TERMINAL_HUELLASOBRANTE */
/* Patch 435 #Seq: 1*/
CREATE PROCEDURE WS_TERMINAL_HUELLASOBRANTE
(
     @TE_CODIGO NumeroEmpleado
)
as
begin
     /*Se realiza un cruce de informacion entre dos tablas*/
     SELECT     TOP (1) HAY.HU_ID, HAY.TE_CODIGO, HAY.CM_CODIGO, HAY.CB_CODIGO, HAY.HU_INDICE, HAY.ID_NUMERO
     FROM         (
          /*La primera tabla es las huellas que deberia tener la terminal*/
          SELECT     E.ID_NUMERO, R.TE_CODIGO, R.GP_CODIGO, E.CM_CODIGO, E.CB_CODIGO, H.HU_HUELLA, H.HU_INDICE, H.HU_ID, H.IV_CODIGO
          FROM         TERMINAL AS T INNER JOIN
                                TERM_GPO AS R ON T.TE_CODIGO = R.TE_CODIGO INNER JOIN
                                EMP_BIO AS E ON R.GP_CODIGO = E.GP_CODIGO INNER JOIN
                                HUELLAGTI AS H ON E.CM_CODIGO = H.CM_CODIGO AND E.CB_CODIGO = H.CB_CODIGO
          WHERE     (T.TE_CODIGO = @TE_CODIGO)

                    ) AS DEBERIA RIGHT OUTER JOIN (
          /*La segunda tabla es las huellas que tiene la terminal*/
          SELECT     T.TE_CODIGO, H.CM_CODIGO, H.CB_CODIGO, H.HU_INDICE, H.HU_ID, E.ID_NUMERO
          FROM         TERMINAL AS T INNER JOIN
                                TERM_HUELLA AS R ON T.TE_CODIGO = R.TE_CODIGO INNER JOIN
                                HUELLAGTI AS H ON R.CM_CODIGO = H.CM_CODIGO AND R.CB_CODIGO = H.CB_CODIGO AND R.HU_INDICE = H.HU_INDICE INNER JOIN
                                EMP_BIO AS E ON H.CM_CODIGO = E.CM_CODIGO AND H.CB_CODIGO = E.CB_CODIGO                                                                                  
		  WHERE     (T.TE_CODIGO = @TE_CODIGO)
                    ) AS HAY ON                     
                           DEBERIA.TE_CODIGO = HAY.TE_CODIGO AND DEBERIA.CM_CODIGO = HAY.CM_CODIGO AND DEBERIA.CB_CODIGO = HAY.CB_CODIGO AND 
                           DEBERIA.HU_INDICE = HAY.HU_INDICE
          /*La condicion es que traiga la informacion de la segunda tabla que no exista en la primera*/
          WHERE     (DEBERIA.HU_INDICE IS NULL) and (DEBERIA.IV_CODIGO = 0)
end
GO

