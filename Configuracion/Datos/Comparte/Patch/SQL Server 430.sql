/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* SP_PATCH : CR:2420  , Mejora en proceso de actualización de Bases de Datos */
/* Patch 430 # Seq:  1*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'SP_PATCH' AND XTYPE = 'U' )
	CREATE TABLE SP_PATCH ( 
		SE_TIPO STATUS,  		/* Tipo de Base de datos*/
		SE_SEQ_NUM STATUS,  	/* Secuencia de aplicacion del stored procedure*/
		SE_APLICA CODIGOEMPRESA,/* Empresa donde aplica el stored procedure */
		SE_NOMBRE DESCRIPCION, 	/* Nombre o etiqueta del stored procedure */
		SE_DESCRIP DESCLARGA,	/* Descripcion del stored procedure */
		SE_DATA MEMO, 		/* Texto del stored procedure */
		SE_ARCHIVO RUTA_URL	/* Ruta del Script Especial */
	)
GO 
   
/* SP_PATCH : CR:2420  , Mejora en proceso de actualización de Bases de Datos */
/* Patch 430 # Seq: 2*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_PROCESO' AND XTYPE = 'PK' )
   ALTER TABLE SP_PATCH 
       ADD CONSTRAINT PK_SP_PATCH PRIMARY KEY (SE_TIPO, SE_SEQ_NUM, SE_APLICA)
GO  


/* SP_PATCH : CR:2460, Modificaciones de tabla de dispositivos */
/* Patch 430 # Seq: 3*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'DI_SYNC' and ID = object_id(N'DISPOSIT') )
begin
	alter table DISPOSIT add DI_SYNC Fecha
end
go

/* SP_PATCH : CR:2460, Creacion de tabla de grupos de terminales */
/* Patch 430 # Seq: 4*/
if not exists( select NAME from sysobjects where NAME = 'GRUPOTERM' and XTYPE = 'U' )
begin
	create table GRUPOTERM(
		GP_CODIGO Codigo, 
		GP_DESCRIP Descripcion)
end
go

/* SP_PATCH : CR:2460, Creacion de Llave Primaria de Grupos de Terminales */
/* Patch 430 # Seq: 5*/
if not exists( select NAME from sysobjects where NAME = 'PK_GRUPOTERM' and XTYPE = 'PK' )
begin
     alter table GRUPOTERM 
     add constraint PK_GRUPOTERM primary key ( GP_CODIGO )
end

go

/* SP_PATCH : CR:2460, Creacion de Tabla de Dispositivos por Grupo */
/* Patch 430 # Seq: 6*/
if not exists( select NAME from sysobjects where NAME = 'DISPOGRUPO' and XTYPE = 'U' )
begin
	create table DISPOGRUPO(
	DI_NOMBRE RelojLinx,
	DI_TIPO FolioChico,
	GP_CODIGO Codigo)
end
go

/* SP_PATCH : CR:2460, Llave Primaria de Tabla de Dispositivos por Grupo */
/* Patch 430 # Seq: 7*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_DISPOGRUPO' and XTYPE = 'PK' )
begin
	alter table DISPOGRUPO
	add constraint PK_DISPOGRUPO primary key ( DI_NOMBRE,DI_TIPO,GP_CODIGO )
 end
 go
 
/* SP_PATCH : CR:2460, Llave Foranea de Tabla de Dispositivos por Grupo y Dispositivos */
/* Patch 430 # Seq: 8*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_DISPOGRUPO_DISPOSIT' and XTYPE = 'F' )
begin
     alter table DISPOGRUPO  
     add  constraint FK_DISPOGRUPO_DISPOSIT foreign key(DI_NOMBRE, DI_TIPO)
	 references DISPOSIT (DI_NOMBRE, DI_TIPO)
	 on update cascade
	 on delete cascade
 end
 go

/* SP_PATCH : CR:2460, Llave Foreanea de Tabla de Dispositivos por Grupo y Grupos de Terminales */
/* Patch 430 # Seq: 9*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_DISPOGRUPO_GRUPOTERM' and XTYPE = 'F' )
begin
     alter table DISPOGRUPO  
	 add  constraint FK_DISPOGRUPO_GRUPOTERM foreign key(GP_CODIGO)
	 references GRUPOTERM (GP_CODIGO)
	 on update cascade
	 on delete cascade
 end
 go

/* SP_PATCH : CR:2460, Creacion de Tabla de Relacion de Empleados y Empresas */
/* Patch 430 # Seq: 10*/
if not exists( select NAME from SYSOBJECTS where NAME = 'EMP_BIO' and XTYPE = 'U' )
begin
	create table EMP_BIO(
		ID_NUMERO NumeroEmpleado,
		CM_CODIGO CodigoEmpresa,
		CB_CODIGO NumeroEmpleado,
	    GP_CODIGO Codigo,
	    IV_CODIGO NumeroEmpleado)
end
go

/* SP_PATCH : CR:2460, Llave Primaria de Tabla de Relacion de Empleados y Empresas */
/* Patch 430 # Seq: 11*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_EMP_BIO' and XTYPE = 'PK')
begin
	alter table EMP_BIO 
	add constraint PK_EMP_BIO primary key ( ID_NUMERO )
 end
 go
 
/* SP_PATCH : CR:2460, Llave Foranea de Tabla de Relacion de Empleados y Empresas con Tabla de Empresas */
/* Patch 430 # Seq: 12*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_EMP_BIO_COMPANY' and XTYPE = 'F')
begin
     alter table EMP_BIO
     add  constraint FK_EMP_BIO_COMPANY foreign key(CM_CODIGO)
     references COMPANY (CM_CODIGO)
     on update cascade
     on delete cascade
 end
 go
 
/* SP_PATCH : CR:2460, Llave Foranea de Tabla de Relacion de Empleados y Empresas con Tabla de Grupos de Terminales */
/* Patch 430 # Seq: 13*/
if not exists( select NAME from SYSOBJECTS where NAME = 'FK_EMP_BIO_GRUPOTERM' and XTYPE = 'F')
begin
     alter table EMP_BIO
     add  constraint FK_EMP_BIO_GRUPOTERM foreign key(GP_CODIGO)
     references GRUPOTERM (GP_CODIGO)
     on update cascade
     on delete cascade
end
go

/* SP_PATCH : CR:2460, Creacion de la tabla de Bitacora de Asistencia */
/* Patch 430 # Seq: 14*/
if not exists ( select 1 from sysobjects where name = 'WS_BITAC' and xtype = 'U' )
begin
     CREATE TABLE WS_BITAC(
	     LLAVE int IDENTITY(1,1),
	     WS_FECHA Fecha,
	     CH_RELOJ RelojLinx,
	     WS_CHECADA Descripcion,
	     CB_CODIGO NumeroEmpleado,
	     WS_MENSAJE Texto,
	     CM_CODIGO CodigoEmpresa,
	     WS_TIPO FolioChico)
end
go

/* SP_PATCH : CR:2460, Creacion de Llave Primaria de la tabla de Bitacora de Asistencia */
/* Patch 430 # Seq: 15*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_WS_BITAC' )
begin
     alter table WS_BITAC
     ADD CONSTRAINT PK_WS_BITAC PRIMARY KEY ( LLAVE )
end
go

/* SP_PATCH : CR:2460, Modificaciones a tabla de Bitacora de Asistencia, se agrega campo de Ultima Actualizacion de terminal*/
/* Patch 430 # Seq: 16*/
if not exists (select NAME from SYSCOLUMNS where NAME = 'DI_UPDATE' and id=OBJECT_ID('DISPOSIT') )
begin
     alter table disposit
     add DI_UPDATE booleano
end
go

/* SP_PATCH : CR:2460, Actualizacion de Campo de Ultima actualizacion de tabla de DISPOSITIVOS */
/* Patch 430 # Seq: 17*/
update DISPOSIT set DI_UPDATE = 'N' where DI_UPDATE is null
GO

/* SP_PATCH : CR:2460, Creacion de Tabla de Log de accesos */
/* Patch 430  # Seq: 18*/
if not exists ( select 1 from sysobjects where name = 'WS_ACCESS' and xtype = 'U' )
begin
     CREATE TABLE WS_ACCESS(
	     LLAVE int IDENTITY(1,1), 
	     WA_FECHA Fecha NOT NULL,  
	     CM_CODIGO CodigoEmpresa NOT NULL, 
	     CB_CODIGO NumeroEmpleado NOT NULL, 
	     CH_RELOJ RelojLinx NOT NULL,     
	     WS_CHECADA Descripcion NOT NULL) 
end
go

/* SP_PATCH : CR:2460, Llave Primaria de log de accesos */
/* Patch 430  # Seq: 19*/
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_WS_ACCESS' )
begin
     alter table WS_ACCESS
     add constraint PK_WS_ACCESS primary KEY ( LLAVE ) 
end
go

/* SP_PATCH : Se agrega columna a tabla de procesos de WorkFlow para nueva lógica de consulta */
/* Patch 430  # Seq: 20*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'WP_PROCESA' and id=object_id(N'WPROCESO') )
	alter table WPROCESO add WP_PROCESA Status
go

/* Patch 430  # Seq: 21*/
update WPROCESO set WP_PROCESA = 1 where coalesce(WP_PROCESA,2) = 2
go

/* 2303 Limite de Impresiones en Kiosco */
/* Patch 430 Seq: 22*/
create table KIOS_IMPR
(
	CB_CODIGO NumeroEmpleado,
	CM_CODIGO CodigoEmpresa,
	PE_TIPO Status,
	PE_YEAR Status,
	PE_NUMERO Status,
	KI_FECHA Fecha,
	KI_KIOSCO Titulo,
	KI_HORA HoraMinutosSegundos,
	RE_CODIGO FolioChico
);
GO
/* 2303 Limite de Impresiones en Kiosco */
/* Patch 430 Seq: 23*/
create index XIE1KIOS_IMPR on KIOS_IMPR
(
       CB_CODIGO,
       CM_CODIGO,		
       PE_TIPO,
       PE_YEAR,
       PE_NUMERO	
)
GO
/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 24*/
create table KENCUESTAS 
(
	ENC_CODIGO FolioGrande,
	ENC_NOMBRE Titulo,
	ENC_PREGUN Formula,
	ENC_CONFID Booleano,
	ENC_VOTOSE Booleano,
	ENC_STATUS Status,
	US_CODIGO  Usuario,
	ENC_FECINI Fecha,
	ENC_FECFIN Fecha,
	ENC_HORINI Hora,
	ENC_HORFIN Hora,
	ENC_MRESUL Booleano,
	ENC_FILTRO Formula,
	ENC_COMPAN Formula,
	ENC_KIOSCS Formula,
	ENC_OPCION Booleano,
	ENC_MSJCON Formula
);

GO 

/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 25*/
ALTER TABLE KENCUESTAS
       ADD CONSTRAINT PK_ENCUESTAS PRIMARY KEY (ENC_CODIGO);

GO

/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 26*/
create table KOPCIONES
(
	OP_ORDEN   Status,
	OP_TITULO  Descripcion,
	OP_DESCRIP Formula,
	ENC_CODIGO FolioGrande,
	US_CODIGO  Usuario
);

GO
/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 27*/

ALTER TABLE KOPCIONES
       ADD CONSTRAINT PK_OPCIONES PRIMARY KEY ( OP_ORDEN,ENC_CODIGO );
	   
GO 
/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 28*/

ALTER TABLE KOPCIONES
       ADD CONSTRAINT FK_Opciones_Encuesta
       FOREIGN KEY (ENC_CODIGO)
       REFERENCES KENCUESTAS
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO
/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 29*/
create table KVOTOS
(
	ENC_CODIGO FolioGrande,
	CB_CODIGO  FolioGrande,
	CM_CODIGO  CodigoEmpresa,
    CB_CREDENC Codigo1,
	VO_VALOR   Status,
	VO_FECHA   Fecha,
	VO_HORA	   Hora,
	VO_KIOSCO  ComputerName
);
/*LLave Primaria de Votos*/

/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 30*/

ALTER TABLE KVOTOS
       ADD CONSTRAINT PK_VOTOS PRIMARY KEY ( ENC_CODIGO,CB_CODIGO,CM_CODIGO,CB_CREDENC )

/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 31*/

ALTER TABLE KVOTOS
       ADD CONSTRAINT FK_Votos_Encuesta
       FOREIGN KEY (ENC_CODIGO)
       REFERENCES KENCUESTAS
       ON DELETE CASCADE
       ON UPDATE CASCADE;

GO




/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   VISTAS COMPARTE      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* SP_PATCH : Vista temporal para consulta de consola */
/* Patch 430 Seq: 32*/
if exists( select NAME from SYSOBJECTS where NAME = 'WVMENSAJEM' and XTYPE = 'V' )
	drop view WVMENSAJEM
go

create view WVMENSAJEM as
	select 
		WN_GUID, WN_FECHA 
	from 
		WMENSAJE 
	where 
		WN_FECHA > ( GETDATE() - 30) and ( WN_LEIDO = 'N' );

go

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   TRIGGERS COMPARTE      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/


/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/*    Comienza: Cambios para  CR:2420  , Mejora en proceso de actualización de Bases de Datos     */
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */

/* TD_COMPANY_SP_PATCH : CR 2420 */
/* Patch 430 # Seq:  17*/
CREATE TRIGGER TD_COMPANY_SP_PATCH ON COMPANY AFTER DELETE AS 
begin 
	set NOCOUNT ON; 
	declare @OldCodigo CodigoEmpresa; 
	select @OldCodigo = CM_CODIGO from Deleted; 
	delete from SP_PATCH where SP_PATCH.SE_APLICA = @OldCodigo ;
END 

GO 
/* TU_COMPANY_SP_PATCH : CR 2420 */
/* Patch 430 # Seq: 18*/
CREATE TRIGGER TU_COMPANY_SP_PATCH ON COMPANY AFTER UPDATE AS 
BEGIN 
	SET NOCOUNT ON; 
	IF UPDATE( CM_CODIGO ) 
	BEGIN 
		declare @NewCodigo CodigoEmpresa; 
		declare @OldCodigo CodigoEmpresa; 
		select @NewCodigo = CM_CODIGO from Inserted; 
		select @OldCodigo = CM_CODIGO from Deleted;
	    update SP_PATCH set SP_PATCH.SE_APLICA = @NewCodigo where ( SP_PATCH.SE_APLICA = @OldCodigo ); 
	END 
END 
GO

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   STORE PRCEDURES - FUNCIONES  $$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/


/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 51*/
CREATE FUNCTION SP_GET_VOTOS( @Encuesta Int )
RETURNS @Votos TABLE (   
	ENC_CODIGO int,					 
	VO_VALOR int,		
	Opcion varchar(50),
	Total Int )
AS
begin
     insert into @Votos
     select V.ENC_CODIGO,VO_VALOR, O.OP_TITULO +'(' +CONVERT( VARCHAR, ( CONVERT( decimal(4,1), Round((CAST( Count(vo_valor)* 100 AS dECIMAL(6,2)) / CAST( (Select Count(*) From KVOTOS where Enc_Codigo = @Encuesta ) as Decimal (6,2) ) ),1) ) ) )+'% )' as Opcion, Count(VO_VALOR)as Total from
			KVOTOS V left outer join KOPCIONES O on O.OP_ORDEN = V.VO_VALOR and O.ENC_CODIGO = @Encuesta
			where V.ENC_CODIGO = @Encuesta
			Group by O.OP_TITULO,V.ENC_CODIGO,VO_VALOR
			order by VO_VALOR 
     RETURN
end
GO

/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 52*/
CREATE FUNCTION SP_MAX_VOTOS_ENCUESTA(
  @Encuesta INTEGER,
  @Tipo INTEGER
) RETURNS INTEGER
AS
BEGIN
  	 declare @Valor INTEGER;
     declare @Votos INTEGER;
     declare @Regresa INTEGER;
     select TOP 1 @Valor = VO_VALOR, @Votos = count(*)
     from KVOTOS
     where ENC_CODIGO = @Encuesta
     group by VO_VALOR
     order by count(*) desc, VO_VALOR
     if @Tipo = 1
        set @Regresa = @Valor
     else
         set @Regresa = @Votos
     return COALESCE( @Regresa, 0 )
END
GO
/* 2377 Encuestas en Kiosco */
/* Patch 430 Seq: 53*/
CREATE FUNCTION SP_VOTOS_ENCUESTA(
  @Encuesta INTEGER,
  @Opcion INTEGER
) RETURNS INTEGER
AS
BEGIN
     declare @Regresa INTEGER;
     select @Regresa = count(*)
     from KVOTOS
     where ENC_CODIGO = @Encuesta and VO_VALOR = @Opcion
     return COALESCE( @Regresa, 0 )
END
GO

