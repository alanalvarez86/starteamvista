/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/


/* Patch 445: Mejoras terminales GTI */

/* Modificar Trigger TR_ADD_TERMINAL (1/2) */
IF exists (SELECT * FROM sysobjects where id = object_id('TR_ADD_TERMINAL')) 
	DROP TRIGGER TR_ADD_TERMINAL
GO

/* Modificar Trigger TR_ADD_TERMINAL (2/2) */
CREATE trigger TR_ADD_TERMINAL on TERMINAL after insert
as
begin
	set nocount on
	
	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx		
	declare ListaMensajes cursor for
		select DM_CODIGO, DM_SLEEP from MENSAJE where DM_ACTIVO='S' order by DM_ORDEN
		
	select @Terminal = TE_CODIGO from Inserted;
	
	open ListaMensajes
	fetch next from ListaMensajes into @Mensaje, @Sleep
	while (@@Fetch_Status = 0)
	begin		
	    if (@Mensaje = 8 )
		begin
            insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP, TM_NEXT) values (@Terminal, @Mensaje, @Sleep, Convert(datetime,'29991231'))
		end
		else
		begin
			insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP) values (@Terminal, @Mensaje, @Sleep)
		end
		
		fetch next from ListaMensajes into @Mensaje, @Sleep
	end
	close ListaMensajes
	deallocate ListaMensajes
	
	update TERMINAL set TE_XML = 
'<?xml version="1.0" encoding="utf-8"?>
<properties>
   <ClockId>'+@Terminal+'</ClockId> 
   <WebService>http://SERVIDORWEB/WSAsistencia/WSAsistencia.asmx</WebService>
   <UseProxy>false</UseProxy>
   <Proxy>192.168.0.30</Proxy>
   <ProxyPort>8080</ProxyPort>
   <UseProxyAuthentication>false</UseProxyAuthentication>
   <ProxyUserName>NULL</ProxyUserName>
   <ProxyPassword>NULL</ProxyPassword>
   <Ip>DHCP</Ip>
   <NetMask></NetMask>
   <DefaultGateWay></DefaultGateWay>
   <DNS></DNS>
   <TimeOut>07</TimeOut>
   <HeartBeatTimer>60</HeartBeatTimer>
   <MsgTime>01</MsgTime>
   <ErrorMsgTime>05</ErrorMsgTime>
   <RelayTime>0</RelayTime>
   <DateTimeFormat>dd/MM hh:mm</DateTimeFormat>
   <Volume>70</Volume>
   <ProxReader>OFF</ProxReader>
   <BarCodeReader>OFF</BarCodeReader>
   <HID>OFF</HID>
   <MenuAccessCode>1234</MenuAccessCode>
   <FpuNotIdetifyMessage>INVALIDO/INTENTE DE NUEVO</FpuNotIdetifyMessage>
   <NotValidByServerMessage>Servidor no pudo validar ID</NotValidByServerMessage>
   <DuplicateMessage>Registro Duplicado</DuplicateMessage>
   <AutenticateMessage>Exito</AutenticateMessage>
   <OfflineValidation>Offline-Almacenada</OfflineValidation>
   <OneMinuteValidate>true</OneMinuteValidate>   
   <FingerVerify>false</FingerVerify>
   <EnableOfflineRelay>false</EnableOfflineRelay>
   <FingerPrint>ON</FingerPrint>
   <EnableKeyPad>true</EnableKeyPad>
   <Version>TRESS</Version>
   <Account>0</Account>
   <ClientId>0</ClientId>
   <Password>0</Password>
   <CajaId>0</CajaId>
   <Identidad>1</Identidad>
   <UseSoapActionHeader>true</UseSoapActionHeader>
   <SoapActionHeader>Synel/Services</SoapActionHeader>
   <UpdateTimeByServer>true</UpdateTimeByServer>
   <ntpServer>SERVIDORNTP</ntpServer>
   <timeZone>Tijuana</timeZone>
   <MifareReader>OFF</MifareReader>
   <MagneticReader>OFF</MagneticReader>
   <ShowCameraPhoto>false</ShowCameraPhoto>
   <ShowPhoto>true</ShowPhoto>
   <Schedules>
   <Function code="1" start="00:00" end="23:59" bg="comida.jpg">Comida</Function>
   <Function code="3" start="12:01" end="14:00" bg="desayuno.jpg">Desayuno</Function>
   <Function code="2" start="14:01" end="23:59" bg="cena.jpg">Cena</Function>
   </Schedules>
</properties>' 
	where TE_CODIGO = @Terminal
end
GO

/* Nuevo campo en tabla TERMINAL para indicar la cantidad de Huellas 1/3 */
if not exists (select * from syscolumns where id=object_id('TERMINAL') and name='TE_HUELLAS')
	alter table TERMINAL add TE_HUELLAS FolioGrande null
GO

/* Nuevo campo en tabla TERMINAL para indicar la cantidad de Huellas 2/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERMINAL') and name='TE_HUELLAS')
	update TERMINAL set  TE_HUELLAS = 0 where TE_HUELLAS is null
GO

/* Nuevo campo en tabla TERMINAL para indicar la cantidad de Huellas 3/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERMINAL') and name='TE_HUELLAS')
	alter table TERMINAL alter column TE_HUELLAS FolioGrande not null
GO

/* Nuevo par�metro en WS_TERMINAL_SETHEARTBEAT para indicar cantidad de huellas (@FPCOUNT) (1/2) */
if exists (select * from sysobjects where id = object_id('WS_TERMINAL_SETHEARTBEAT') )
 drop procedure WS_TERMINAL_SETHEARTBEAT
GO

/* Nuevo par�metro en WS_TERMINAL_SETHEARTBEAT para indicar cantidad de huellas (@FPCOUNT) (2/2) */
CREATE procedure WS_TERMINAL_SETHEARTBEAT
( @TerminalID NumeroEmpleado, @Version varchar(30), @IpAddress varchar(30), @TER_HOR datetime, @FPCOUNT int=0)
 as 
 begin 
	UPDATE TERMINAL SET TE_BEAT = GETDATE(),TE_Version=@Version, TE_IP=@IpAddress, TE_TER_HOR = @TER_HOR, TE_HUELLAS = @FPCOUNT 
	WHERE (TE_CODIGO = @TerminalID) 
end 
GO


/* Tabla WS_BITAC. Ampliar campo WS_MENSAJE a TextoLargo (varchar (1024)) (1/2) */
-- Creaci�n del nuevo dominio.
if not exists (SELECT * FROM sys.types WHERE name = 'TextoLargo' )
begin
	exec sp_addtype TextoLargo, 'varchar(1024)', 'NOT NULL'
	exec sp_bindefault StringVacio, TextoLargo
end
go


/* Tabla WS_BITAC. Ampliar campo WS_MENSAJE a TextoLargo (varchar (1024)) (2/2) */
alter table WS_BITAC alter column WS_MENSAJE TextoLargo
GO

/* Nuevo campo en tabla TERM_MSG para el tipo de configuraci�n de siguiente mensaje (por minutos o por hora espec�fica) 1/3 */
IF NOT EXISTS (select * from syscolumns where id=object_id('TERM_MSG') and name='TM_NEXTXHR')
	alter table TERM_MSG add TM_NEXTXHR booleano null
GO

/* Nuevo campo en tabla TERM_MSG para el tipo de configuraci�n de siguiente mensaje (por minutos o por hora espec�fica) 2/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERM_MSG') and name='TM_NEXTXHR')
	update TERM_MSG set TM_NEXTXHR = 'N' where TM_NEXTXHR is null
GO

/* Nuevo campo en tabla TERM_MSG para el tipo de configuraci�n de siguiente mensaje (por minutos o por hora espec�fica) 3/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERM_MSG') and name='TM_NEXTXHR')
	alter table TERM_MSG alter column TM_NEXTXHR booleano not null
GO

/* Nuevo campo en tabla TERM_MSG para indicar hora de siguiente ejecuci�n 1/3 */
if not exists (select * from syscolumns where id=object_id('TERM_MSG') and name='TM_NEXTHOR')
	alter table TERM_MSG add TM_NEXTHOR Hora null 
GO

/* Nuevo campo en tabla TERM_MSG para indicar hora de siguiente ejecuci�n 2/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERM_MSG') and name='TM_NEXTHOR')
	update TERM_MSG set TM_NEXTHOR = '0000' where TM_NEXTHOR is null
GO

/* Nuevo campo en tabla TERM_MSG para indicar hora de siguiente ejecuci�n 3/3 */
IF EXISTS (select * from syscolumns where id=object_id('TERM_MSG') and name='TM_NEXTHOR')
	alter table TERM_MSG alter column TM_NEXTHOR Hora not null
GO

/*	Modificaci�n a procedimiento WS_TERMINAL_SETPENDIENTE para tomar en cuenta
	la configuraci�n de siguiente mensaje: por minutos o por hora espec�fica (1/2) */
IF EXISTS (select * from sysobjects where id = object_id('WS_TERMINAL_SETPENDIENTE') )
 DROP PROCEDURE WS_TERMINAL_SETPENDIENTE
GO

/*	Modificaci�n a procedimiento WS_TERMINAL_SETPENDIENTE para tomar en cuenta
	la configuraci�n de siguiente mensaje: por minutos o por hora espec�fica (2/2) */
CREATE PROCEDURE WS_TERMINAL_SETPENDIENTE ( @TE_CODIGO NUMEROEMPLEADO, @DM_CODIGO CODIGO ) 
AS 
BEGIN
	DECLARE @TIPOCONFIGURACION BOOLEANO;
	DECLARE @HORA CHAR(6)	

	SELECT @TIPOCONFIGURACION = TM_NEXTXHR FROM TERM_MSG WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO;
	 
    IF @TIPOCONFIGURACION = 'N'
	BEGIN
		UPDATE TERM_MSG SET TM_NEXT = CASE WHEN TM_SLEEP = 0 THEN '29991231' 
				ELSE DATEADD(MI, TM_SLEEP, GETDATE()) 
				END 
		WHERE DM_CODIGO = @DM_CODIGO 
		AND TE_CODIGO =@TE_CODIGO 
	END
	ELSE
	BEGIN
		-- Asignar valor a @HORA.
		SELECT @HORA = ' ' + SUBSTRING (TM_NEXTHOR, 1, 2) +':' + SUBSTRING (TM_NEXTHOR, 3,2) FROM TERM_MSG
			WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO;
		-- Actualizar fecha de siguiente ejecuci�n a una hora espec�fica.		
		IF CAST (CONVERT(VARCHAR(10),GETDATE(),111)         + @HORA AS datetime) > GETDATE()
		BEGIN
			UPDATE TERM_MSG SET TM_NEXT =  
				CAST (CONVERT(VARCHAR(10),GETDATE(),111) + @HORA AS datetime)				 
				WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO
		END
		ELSE
		BEGIN
			UPDATE TERM_MSG SET TM_NEXT =			  
				CAST (CONVERT(VARCHAR(10), DATEADD(DAY, 1, GETDATE()) ,111) + @HORA AS datetime)				 
				WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO
		END
	END
END
GO


/* Modificar Trigger TR_UPDATE_HUELLAGTI (1/2) */
IF exists (SELECT * FROM sysobjects where id = object_id('TR_UPDATE_HUELLAGTI') )
	DROP TRIGGER TR_UPDATE_HUELLAGTI
GO

/* Modificar Trigger TR_UPDATE_HUELLAGTI (2/2) */
Create trigger TR_UPDATE_HUELLAGTI on HUELLAGTI AFTER INSERT, UPDATE 
as 
begin 
	set nocount on 
	 
	declare @HU_ID NumeroEmpleado; 
	declare @Empleado NumeroEmpleado;
	declare @Empresa CodigoEmpresa;
	declare @Indice smallint;
	
	select @HU_ID = HU_ID, @Empleado = CB_CODIGO, @Empresa = CM_CODIGO, @Indice = HU_INDICE from Inserted 
	
	if UPDATE( HU_HUELLA ) 
	begin 
		update HUELLAGTI set HU_TIMEST = GETDATE() where HU_ID = @HU_ID
		
		delete from TERM_HUELLA where ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa ) and ( HU_INDICE = @Indice )
	end 

end
GO

/* Modificar Procedure WS_TERMINAL_SETAGREGAHUELLA (1/2) */
IF  EXISTS (SELECT * FROM sysobjects WHERE id = object_id('WS_TERMINAL_SETAGREGAHUELLA') )
	DROP PROCEDURE WS_TERMINAL_SETAGREGAHUELLA
GO
/* Modificar Procedure WS_TERMINAL_SETAGREGAHUELLA (2/2) */
CREATE PROCEDURE WS_TERMINAL_SETAGREGAHUELLA
(
	@TE_CODIGO NumeroEmpleado,
	@HU_ID INT
)
AS
BEGIN

     DECLARE @P1 CodigoEmpresa;
	 DECLARE @P2 NumeroEmpleado;
	 DECLARE @P3 integer;
	 DECLARE @LlaveAUpdate integer;

	 select @P1 = CM_CODIGO, @P2 = CB_CODIGO, @p3 = HU_INDICE from HUELLAGTI where ( HU_ID = @HU_ID ) 

	 IF EXISTS(SELECT *
	           FROM TERM_HUELLA
			   WHERE CM_CODIGO = @P1
			   AND CB_CODIGO = @P2
			   AND HU_INDICE = @P3
			   AND TE_CODIGO = @TE_CODIGO)
	BEGIN
		SELECT @LlaveAUpdate = LLAVE FROM TERM_HUELLA WHERE ( CM_CODIGO = @P1 ) AND ( CB_CODIGO = @P2 ) AND ( HU_INDICE = @P3 ) AND ( TE_CODIGO = @TE_CODIGO )
		UPDATE TERM_HUELLA
		SET CM_CODIGO = @P1, CB_CODIGO = @P2, HU_INDICE = @P3, TE_CODIGO = @TE_CODIGO
		WHERE LLAVE = @LlaveAUpdate
	END
	ELSE
	BEGIN
		-----OLD VERSION (4/12/2014)-----
		INSERT INTO TERM_HUELLA(CM_CODIGO, CB_CODIGO, HU_INDICE, TE_CODIGO)
		SELECT CM_CODIGO, CB_CODIGO, HU_INDICE, @TE_CODIGO
		FROM HUELLAGTI
		WHERE HU_ID = @HU_ID
		---------------------------------
	END
END
GO

/* Agrega Mensaje 8 */
IF (SELECT count(*) FROM MENSAJE where ( DM_CODIGO = 8 ) ) = 0 
begin
	insert into MENSAJE( DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO) values ( 8, 'Sincronizaci�n total de huellas', 8, 0, 'S' )
end
GO

IF (SELECT count(*) FROM MENSAJE where ( DM_MENSAJE = 'Depuracion de Huellas' ) AND ( DM_CODIGO = 6 ) ) = 1 
begin
  update MENSAJE set  DM_MENSAJE = 'Depuraci�n de Huellas' where ( DM_MENSAJE = 'Depuracion de Huellas' ) AND ( DM_CODIGO = 6 )
end
GO

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (1/3) */
IF EXISTS (select * from sysobjects where id = object_id('WS_AGREGA_NUEVO_MSG') )
 DROP PROCEDURE WS_AGREGA_NUEVO_MSG
GO

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (2/3) */
CREATE PROCEDURE WS_AGREGA_NUEVO_MSG
AS
BEGIN
	set nocount on
	
	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx	
	
	declare ListaMensajes cursor for
	select distinct( TE_CODIGO ) from TERM_MSG
		
	open ListaMensajes
	fetch next from ListaMensajes into @Terminal
	while (@@Fetch_Status = 0)
	begin
		if ( select count(*) from TERM_MSG where ( DM_CODIGO = 8 ) and ( TE_CODIGO = @Terminal ) ) = 0
		begin 
             insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP, TM_NEXT) values (@Terminal, 8, 0, Convert(datetime,'29991231'))
		end
		
		fetch next from ListaMensajes into @Terminal
	end
	close ListaMensajes
	deallocate ListaMensajes
end
GO 

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (3/3) */
EXEC Dbo.WS_AGREGA_NUEVO_MSG
go

/* Modificar Trigger TR_ACTUALIZA_TMNEXT (1/2) */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id('TR_ACTUALIZA_TMNEXT') )
	DROP TRIGGER TR_ACTUALIZA_TMNEXT
GO

/* Modificar Trigger TR_ACTUALIZA_TMNEXT (2/2) */
Create trigger TR_ACTUALIZA_TMNEXT on dbo.TERM_MSG
after UPDATE AS
BEGIN
	set nocount on

	declare @nextMsg datetime 
	declare @dmCodigo int
	declare @tecodigo int 


	select @nextMsg = TM_NEXT from Inserted
	select @dmCodigo = DM_CODIGO from inserted
	select @tecodigo = TE_CODIGO from inserted
		
	if( update( TM_NEXT ) )
	begin
		if( @dmCodigo = 8 and @nextmsg > Convert(datetime,'18991230') )
		begin
			EXECUTE('delete from TERM_HUELLA where TE_CODIGO='+@tecodigo)
			EXECUTE('update Term_MSG set tm_next =''18991230'' where TE_CODIGO='+@tecodigo+' and dm_Codigo = 4')
		end 

		if(@nextmsg > Convert(datetime,'18991230'))
		begin
			EXECUTE('update Term_MSG set tm_ultima =getdate() where TE_CODIGO='+@tecodigo+' and dm_Codigo = '+@dmCodigo)
		end 
	end
end
GO

/* Correcci�n de Bug #10933: No est� grabando correctamente la huella del empleado cuando est� asignado a un invitador (1/2) */
if exists (select * from sysobjects where id = object_id('SP_HUELLAGTI_INSERTA') )
 drop procedure SP_HUELLAGTI_INSERTA
GO

/* Correcci�n de Bug #10933: No est� grabando correctamente la huella del empleado cuando est� asignado a un invitador (2/2) */
create procedure SP_HUELLAGTI_INSERTA
(
     @CM_CODIGO CodigoEmpresa,
     @CB_CODIGO NumeroEmpleado,
     @HU_INDICE numeroempleado,
     @HU_HUELLA VARCHAR(MAX),
     @IV_CODIGO NumeroEmpleado,
     @CH_CODIGO Accesos
)
as
Begin
     if ( @IV_CODIGO = 0 ) and ( @CB_CODIGO <> 0 )
     begin
          if exists ( select * from HUELLAGTI where CB_CODIGO = @CB_CODIGO and IV_CODIGO = @IV_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE )
          begin
               update HUELLAGTI
               set HU_HUELLA = @HU_HUELLA
			   where CB_CODIGO = @CB_CODIGO and IV_CODIGO = @IV_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE
          end
          else
          begin
               insert into HUELLAGTI ( CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA, IV_CODIGO )
               values ( @CM_CODIGO, @CB_CODIGO, @HU_INDICE, @HU_HUELLA, @IV_CODIGO )
          end
     end
     else
     begin
		  if exists ( select * from HUELLAGTI where CB_CODIGO = @CB_CODIGO and IV_CODIGO = @IV_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE )
          begin
               update HUELLAGTI
               set HU_HUELLA = @HU_HUELLA, CB_CODIGO = @CB_CODIGO
			   where CB_CODIGO = @CB_CODIGO and IV_CODIGO = @IV_CODIGO and CM_CODIGO = @CM_CODIGO and HU_INDICE = @HU_INDICE
          end
          else
          begin
               insert into HUELLAGTI ( CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA, IV_CODIGO )
               values ( @CM_CODIGO, @CB_CODIGO, @HU_INDICE, @HU_HUELLA, @IV_CODIGO )
          end
     end

     exec SP_REPORTAHUELLA @CH_CODIGO, @@IDENTITY
end
GO