/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 452: Crear tabla CAF_CONFIG 1/2 */
--
IF not exists (SELECT * FROM sysobjects where id = object_id(N'CAF_CONFIG'))
begin
	CREATE TABLE CAF_CONFIG(
						 CF_NOMBRE            RelojLinx     NOT NULL,
						 DI_TIPO              FolioChico    NOT NULL,
						 CF_TIP_COM           FolioChico    NOT NULL,
						 CF_FOTO              Booleano      NOT NULL,
						 CF_COM               Booleano      NOT NULL,
						 CF_SIG_COM           Booleano      NOT NULL,
						 CF_TECLADO           Booleano      NOT NULL,
						 CF_DEF_COM           Texto         NULL,
						 CF_DEF_CRE           Texto         NULL,
						 CF_PRE_TIP           Booleano      NOT NULL,
						 CF_PRE_QTY           Booleano      NOT NULL,
						 CF_BANNER            Texto         NULL,
						 CF_VELOCI            FolioChico    NULL,
						 CF_T_ESP             FolioGrande   NULL,         
						 CF_CALEND            Texto         NOT NULL,
						 CF_GAFETE            Texto         NOT NULL,
						 CF_CLAVE             Texto         NULL,
						 CF_A_PRINT           Booleano      NOT NULL,
						 CF_A_CANC            Booleano      NOT NULL,
						 CF_TCOM_1            Texto         NULL,
						 CF_TCOM_2            Texto         NULL,
						 CF_TCOM_3            Texto         NULL,
						 CF_TCOM_4            Texto         NULL,
						 CF_TCOM_5            Texto         NULL,
						 CF_TCOM_6            Texto         NULL,
						 CF_TCOM_7            Texto         NULL,
						 CF_TCOM_8            Texto         NULL,
						 CF_TCOM_9            Texto         NULL,
						 CF_REINICI           Booleano      NOT NULL,
						 CF_CHECK_S           FolioGrande   NOT NULL,
						 CF_SINC              FolioGrande   NOT NULL,
						 CF_CAMBIOS           Booleano      NOT NULL
	);

	ALTER TABLE CAF_CONFIG 
	ADD CONSTRAINT PK_CAF_CONFIG 
	PRIMARY KEY (CF_NOMBRE, DI_TIPO);

	ALTER TABLE CAF_CONFIG 
	ADD CONSTRAINT FK_Caf_Config_Disposit
	FOREIGN KEY (CF_NOMBRE, DI_TIPO) 
	REFERENCES DISPOSIT(DI_NOMBRE, DI_TIPO) 
	ON DELETE CASCADE
	ON UPDATE CASCADE;

end
go

/* Patch 452: Crear tabla CAF_CONFIG 2/2 */
IF not exists (SELECT * FROM sysobjects where id = object_id(N'CAF_CALEND'))
begin
	CREATE TABLE CAF_CALEND (
		CF_NOMBRE	RelojLinx		  NOT NULL,
		DI_TIPO	    FolioChico		  NOT NULL,
		CC_REG_ID	Int IDENTITY(1,1) NOT NULL,
		HORA		Hora			  NOT NULL,
		ACCION		FolioChico	      NULL,
		CONTADOR	Booleano		  NOT NULL,
		SEMANA		FolioChico	      NOT NULL,
		SYNC		Booleano		  NOT NULL
	);

	ALTER TABLE CAF_CALEND 
	ADD CONSTRAINT PK_CAF_CALEND 
	PRIMARY KEY (CF_NOMBRE, DI_TIPO, CC_REG_ID);

	ALTER TABLE CAF_CALEND 
	ADD CONSTRAINT FK_Caf_Calend_Caf_Config
	FOREIGN KEY (CF_NOMBRE, DI_TIPO) 
	REFERENCES CAF_CONFIG (CF_NOMBRE, DI_TIPO)
	ON DELETE CASCADE
	ON UPDATE CASCADE;
end
go
