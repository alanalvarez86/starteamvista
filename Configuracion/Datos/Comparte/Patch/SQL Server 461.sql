/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 461 */
--
/* #16253 - Nuevo campo CB_ACTIVO tabla EMP_BIO */
if not exists (select * from syscolumns where id=object_id('EMP_BIO') and name='CB_ACTIVO')
	alter table EMP_BIO add CB_ACTIVO booleano null;
go

update EMP_BIO set CB_ACTIVO = 'S' where CB_ACTIVO is null;
go

alter table EMP_BIO alter column CB_ACTIVO booleano not null;
go

/* #16253 - Nuevo campo CB_FEC_BAJ tabla EMP_BIO */
if not exists (select * from syscolumns where id=object_id('EMP_BIO') and name='CB_FEC_BAJ')
	alter table EMP_BIO add CB_FEC_BAJ fecha null;
go

update EMP_BIO set CB_FEC_BAJ = '1899-12-30' where CB_FEC_BAJ is null;
go

alter table EMP_BIO alter column CB_FEC_BAJ fecha not null;
go

/* #16252 - Cambio para ya no utilizar la llave foranea */
if exists (select * from sysobjects where id = object_id(N'FK_TERM_HUELLA_HUELLAGTI') and xtype in (N'F')) 
	ALTER TABLE [dbo].[TERM_HUELLA] DROP CONSTRAINT [FK_TERM_HUELLA_HUELLAGTI]
go

/* #16252 - Cambio del TRIGGER - TR_DELETE_HUELLAGTI  */
if exists (select * from sysobjects where id = object_id(N'TR_DELETE_HUELLAGTI') and xtype in (N'TR')) 
	DROP TRIGGER [dbo].[TR_DELETE_HUELLAGTI]
GO

CREATE TRIGGER [dbo].[TR_DELETE_HUELLAGTI]
on [dbo].[HUELLAGTI]
AFTER DELETE
as
begin
     set nocount on

     declare @HU_ID NumeroEmpleado
	 declare @Empresa CodigoEmpresa
	 declare @Empleado NumeroEmpleado

     select @HU_ID = HU_ID, @Empresa = CM_CODIGO, @Empleado = CB_CODIGO from deleted;

     delete from COM_HUELLA where HU_ID = @HU_ID;

	 UPDATE TERM_HUELLA set BORRAR ='S' where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado );
	 
	 UPDATE TERM_MSG SET TM_NEXT = '18991230' WHERE ( DM_CODIGO = 6 )
end
GO

/* #16252 - Cambio del PROCEDURE - WS_ACTUALIZA_STATUS_EMP  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_ACTUALIZA_STATUS_EMP' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP]
GO

CREATE  PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP] 
AS
BEGIN
	set nocount on
	
	declare @Empleado NumeroEmpleado
	declare @Empresa CodigoEmpresa
	declare @Activo booleano
	declare @FechaBaja fecha
	declare @IdNumero Integer
	
	declare EmpleadosHuellas cursor for
	select CM_CODIGO, CB_CODIGO, ID_NUMERO from EMP_BIO
		
	open EmpleadosHuellas
	fetch next from EmpleadosHuellas into @Empresa, @Empleado, @IdNumero
	while (@@Fetch_Status = 0)
	begin
		if exists( select * from VEMPL_BAJA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) )
		begin
			select @Activo = Coalesce( CB_ACTIVO, 'S' ), @FechaBaja = coalesce( CB_FEC_BAJ, '1899-12-30' ) from VEMPL_BAJA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado );
			update EMP_BIO set CB_ACTIVO = @Activo, CB_FEC_BAJ = @FechaBaja where ( ID_NUMERO = @IdNumero );		
		end
		fetch next from EmpleadosHuellas into @Empresa, @Empleado, @IdNumero
	end
	close EmpleadosHuellas
	deallocate EmpleadosHuellas
END		
go

EXEC WS_ACTUALIZA_STATUS_EMP 
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_ACTUALIZA_STATUS_EMP' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP]
GO

/* #16252 - Cambio del PROCEDURE - WS_TERMINAL_HUELLAFALTANTE  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLAFALTANTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLAFALTANTE]
GO
CREATE PROCEDURE [dbo].[WS_TERMINAL_HUELLAFALTANTE]
(
     @TE_CODIGO NumeroEmpleado
)
AS
BEGIN
	SET NOCOUNT ON

		DECLARE @Ayer DATETIME
		SET @Ayer = GETDATE() - 1

        SELECT TOP 1 E.ID_NUMERO, H.HU_ID, R.TE_CODIGO, R.GP_CODIGO, E.CM_CODIGO, E.CB_CODIGO, H.HU_HUELLA, H.HU_INDICE
		FROM   TERM_GPO AS R
		INNER JOIN EMP_BIO AS E ON R.GP_CODIGO = E.GP_CODIGO AND E.IV_CODIGO = 0 
		INNER JOIN HUELLAGTI AS H ON E.CM_CODIGO = H.CM_CODIGO AND E.CB_CODIGO = H.CB_CODIGO AND E.IV_CODIGO = H.IV_CODIGO
		LEFT OUTER JOIN TERM_HUELLA AS TH ON TH.TE_CODIGO = @TE_CODIGO AND H.CM_CODIGO = TH.CM_CODIGO AND H.CB_CODIGO = TH.CB_CODIGO 
		AND H.HU_INDICE = TH.HU_INDICE AND H.IV_CODIGO = TH.IV_CODIGO
		WHERE  R.TE_CODIGO = @TE_CODIGO AND TH.TE_CODIGO IS NULL AND ( E.CB_ACTIVO = 'S' OR E.CB_FEC_BAJ >= @Ayer )
		ORDER BY E.CM_CODIGO, E.CB_CODIGO
END    
GO

/* #16252 - Cambio del PROCEDURE - WS_TERMINAL_HUELLASACTUALES  */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLASACTUALES' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
GO
create PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
(
     @TE_CODIGO NumeroEmpleado
) as
begin
	SET NOCOUNT ON

	declare @Empleado integer;
	declare @Empresa CodigoEmpresa;
	declare @Ayer datetime
	SET @Ayer = GETDATE() - 1


		Declare TmpEmpleadosBaja CURSOR for
        SELECT	TH.CM_CODIGO, TH.CB_CODIGO
		FROM	TERM_HUELLA AS TH 
		INNER JOIN EMP_BIO AS EB ON TH.CB_CODIGO = EB.CB_CODIGO AND TH.CM_CODIGO = EB.CM_CODIGO
		--INNER JOIN VEMPL_BAJA AS VE ON VE.CB_CODIGO = TH.CB_CODIGO AND VE.CM_CODIGO = TH.CM_CODIGO 
		WHERE	(TH.TE_CODIGO = @TE_CODIGO) AND ( EB.CB_ACTIVO = 'N' AND EB.CB_FEC_BAJ >= @Ayer ) 
		GROUP BY TH.CM_CODIGO, TH.CB_CODIGO, TH.TE_CODIGO

		Open TmpEmpleadosBaja
		fetch NEXT FROM TmpEmpleadosBaja 
		into @Empresa, @Empleado

		while @@FETCH_STATUS = 0
       	begin
			update TERM_HUELLA set BORRAR = 'S' where ( TE_CODIGO = @TE_CODIGO ) and ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa )
			fetch NEXT FROM TmpEmpleadosBaja 
			into @Empresa, @Empleado
        end 

		close TmpEmpleadosBaja 
    	deallocate TmpEmpleadosBaja 
    
end
GO
