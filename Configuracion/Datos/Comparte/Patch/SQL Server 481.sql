/* Agregando mensaje 13 - Obtener datos para diagnůstico */
IF (SELECT count(*) FROM MENSAJE where ( DM_CODIGO = 13 ) ) = 0 
begin
	insert into MENSAJE( DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO) values ( 13, 'Obtener datos para diagnůstico', 13, 0, 'S' )
end
GO

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (1/3) */
IF EXISTS (select * from sysobjects where id = object_id('WS_AGREGA_NUEVO_MSG') )
	DROP PROCEDURE WS_AGREGA_NUEVO_MSG
GO

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (2/3) */
CREATE PROCEDURE WS_AGREGA_NUEVO_MSG
AS
BEGIN
	set nocount on
	
	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx	
	
	declare ListaMensajes cursor for
	select distinct( TE_CODIGO ) from TERM_MSG
		
	open ListaMensajes
	fetch next from ListaMensajes into @Terminal
	while (@@Fetch_Status = 0)
	begin
		if ( select count(*) from TERM_MSG where ( DM_CODIGO = 13 ) and ( TE_CODIGO = @Terminal ) ) = 0
		begin 
             insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP, TM_NEXT) values (@Terminal, 13, 0, Convert(datetime,'29991231'))
		end
		
		fetch next from ListaMensajes into @Terminal
	end
	close ListaMensajes
	deallocate ListaMensajes
end
GO 

/* Modificar Procedure WS_AGREGA_NUEVO_MSG (3/3) */
EXEC Dbo.WS_AGREGA_NUEVO_MSG
go

/* Crear nueva tabla para Obtener datos para diagnůstico (1/2) */
IF not exists (SELECT * FROM sysobjects where id = object_id(N'WS_DIAGNOS'))
begin
	CREATE TABLE WS_DIAGNOS(
	     LLAVE int IDENTITY(1,1),
	     WS_FECHA Fecha,
	     CH_RELOJ RelojLinx,
	     WS_DATOS XMLDATA )
END
GO

/* Crear nueva tabla para Obtener datos para diagnůstico (2/2) */
if not exists( select NAME from SYSOBJECTS where NAME = 'PK_WS_DIAGNOS' and XTYPE = 'PK' )
begin
	alter table WS_DIAGNOS ADD CONSTRAINT PK_WS_DIAGNOS PRIMARY KEY ( LLAVE );
end
GO

/* Modificar Procedure WS_AGREGA_DIAGNOSTICO (1/2) */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_AGREGA_DIAGNOSTICO')
  DROP PROCEDURE WS_AGREGA_DIAGNOSTICO
GO

/* Modificar Procedure WS_AGREGA_DIAGNOSTICO (2/2) */
Create PROCEDURE WS_AGREGA_DIAGNOSTICO ( @terminal RelojLinx, @Datos XMLDATA ) 
as 
begin 
	INSERT INTO WS_DIAGNOS( WS_FECHA, CH_RELOJ, WS_DATOS ) 
	VALUES ( getdate(), @terminal, @Datos ) 
end 
GO

/* Modificar Function FN_TERMINAL_STATUS (1/2) */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'FN_TERMINAL_STATUS')
BEGIN
	DROP FUNCTION FN_TERMINAL_STATUS
END
GO

/* Modificar Function FN_TERMINAL_STATUS (2/2) */
CREATE FUNCTION FN_TERMINAL_STATUS(@status booleano)
returns TABLE 
as	
	return ( 
	Select case
	when                                
		DATEDIFF(MINUTE, TE_BEAT, CURRENT_TIMESTAMP) <=1 then 1
	else 0                                
	END as STATUS, *
	from TERMINAL where ( ( TE_ACTIVO = @status ) or ( @status = '') ) )
GO

/* Modificar procedure SP_CREAR_VEMPL_BAJA (1/3) */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'SP_CREAR_VEMPL_BAJA' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_CREAR_VEMPL_BAJA]
GO

/* Modificar procedure SP_CREAR_VEMPL_BAJA (2/3) */
CREATE PROCEDURE [dbo].[SP_CREAR_VEMPL_BAJA]
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[VEMPL_BAJA]') AND xtype in (N'V'))
		DROP VIEW VEMPL_BAJA

	DECLARE @CM_DATOS Accesos;
	DECLARE @BD Accesos;
	DECLARE @QUERY NVARCHAR(MAX);
	DECLARE @CM_CODIGO Accesos;

	SET @BD = '';

	DECLARE DB_CURSOR CURSOR FOR 
	SELECT CM_DATOS, SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) AS BD,
	CM_CODIGO
	FROM COMPANY 
		WHERE CM_CONTROL IN ('3DATOS') 	
			AND SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) IN (SELECT Name COLLATE DATABASE_DEFAULT FROM sys.databases WHERE STATE = 0)

		OPEN DB_CURSOR

			FETCH NEXT FROM DB_CURSOR INTO @CM_DATOS, @BD, @CM_CODIGO
			IF @@FETCH_STATUS = 0 
			BEGIN
				SET @QUERY = 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ
							FROM [' + @BD + '].DBO.COLABORA WITH (NOLOCK) where ( CB_ACTIVO = ''N'' )  
							and (  CB_FEC_BAJ < getdate() )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END

			WHILE @@FETCH_STATUS = 0
			BEGIN	
				SET @QUERY =  @QUERY + ' UNION ALL ' + 						 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ
							FROM [' + @BD + '].DBO.COLABORA WITH (NOLOCK) where ( CB_ACTIVO = ''N'' )  
							and (  CB_FEC_BAJ < getdate() )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END 

	CLOSE DB_CURSOR;

	DEALLOCATE DB_CURSOR;

	SET @QUERY  = 'CREATE VIEW VEMPL_BAJA AS ' + @QUERY

	EXECUTE (@QUERY)
END
GO

/* Modificar procedure SP_CREAR_VEMPL_BAJA (3/3) */
EXEC SP_CREAR_VEMPL_BAJA
GO

/* Modificar procedure SP_CREAR_VEMPL_ALTA (1/3) */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'SP_CREAR_VEMPL_ALTA' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SP_CREAR_VEMPL_ALTA]
GO

/* Modificar procedure SP_CREAR_VEMPL_ALTA (2/3) */
CREATE PROCEDURE [dbo].[SP_CREAR_VEMPL_ALTA]
AS
BEGIN
	SET NOCOUNT ON;

	IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[VEMPL_ALTA]') AND xtype in (N'V'))
		DROP VIEW VEMPL_ALTA

	DECLARE @CM_DATOS Accesos;
	DECLARE @BD Accesos;
	DECLARE @QUERY NVARCHAR(MAX);
	DECLARE @CM_CODIGO Accesos;

	SET @BD = '';

	DECLARE DB_CURSOR CURSOR FOR 
	SELECT CM_DATOS, SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) AS BD,
	CM_CODIGO
	FROM COMPANY 
		WHERE CM_CONTROL IN ('3DATOS') 	
			AND SUBSTRING(CM_DATOS, CHARINDEX('.', CM_DATOS, 0)+1, LEN(CM_DATOS)) IN (SELECT Name COLLATE DATABASE_DEFAULT FROM sys.databases WHERE STATE = 0)

		OPEN DB_CURSOR

			FETCH NEXT FROM DB_CURSOR INTO @CM_DATOS, @BD, @CM_CODIGO
			IF @@FETCH_STATUS = 0 
			BEGIN
				SET @QUERY = 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ, CB_FEC_ING
							FROM [' + @BD + '].DBO.COLABORA WITH (NOLOCK) where ( CB_ACTIVO = ''S'' ) OR ( CB_FEC_BAJ > getdate() )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END

			WHILE @@FETCH_STATUS = 0
			BEGIN	
				SET @QUERY =  @QUERY + ' UNION ALL ' + 						 
							'SELECT ''' + @CM_CODIGO + ''' AS CM_CODIGO,
							CB_CODIGO, CB_ACTIVO COLLATE DATABASE_DEFAULT AS CB_ACTIVO, CB_FEC_BAJ, CB_FEC_ING
							FROM [' + @BD + '].DBO.COLABORA WITH (NOLOCK) where ( CB_ACTIVO = ''S'' ) OR ( CB_FEC_BAJ > getdate() )';				
				
				FETCH NEXT FROM DB_CURSOR INTO  @CM_DATOS, @BD, @CM_CODIGO
			END 

	CLOSE DB_CURSOR;

	DEALLOCATE DB_CURSOR;

	SET @QUERY  = 'CREATE VIEW VEMPL_ALTA AS ' + @QUERY

	EXECUTE (@QUERY)
END
GO

/* Modificar SP SP_CREAR_VEMPL_ALTA (3/3) */
EXEC SP_CREAR_VEMPL_ALTA
GO

/* Modificar SP WS_ACTUALIZA_STATUS_EMP (1/3) */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_ACTUALIZA_STATUS_EMP' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP]
GO

/* Modificar SP WS_ACTUALIZA_STATUS_EMP (2/3) */
CREATE  PROCEDURE [dbo].[WS_ACTUALIZA_STATUS_EMP] 
AS
BEGIN
	set nocount on
	
	declare @Empleado NumeroEmpleado
	declare @Empresa CodigoEmpresa
	declare @Activo booleano
	declare @FechaBaja fecha
	declare @IdNumero Integer
	
	declare EmpleadosHuellas cursor for
	select CM_CODIGO, CB_CODIGO, ID_NUMERO from EMP_BIO
		
	open EmpleadosHuellas
	fetch next from EmpleadosHuellas into @Empresa, @Empleado, @IdNumero
	while (@@Fetch_Status = 0)
	begin
		if exists( select * from VEMPL_BAJA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) )
		begin
			select @Activo = Coalesce( CB_ACTIVO, 'S' ), @FechaBaja = coalesce( CB_FEC_BAJ, '1899-12-30' ) from VEMPL_BAJA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado );
			update EMP_BIO set CB_ACTIVO = @Activo, CB_FEC_BAJ = @FechaBaja where ( ID_NUMERO = @IdNumero );		
		end
		else
		begin
			if exists( select * from VEMPL_ALTA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado ) )
			begin
				select @Activo = 'S', @FechaBaja = '1899-12-30' from VEMPL_ALTA where ( CM_CODIGO = @Empresa ) and ( CB_CODIGO = @Empleado );
				update EMP_BIO set CB_ACTIVO = @Activo, CB_FEC_BAJ = @FechaBaja where ( ID_NUMERO = @IdNumero );		
			end
		end
		fetch next from EmpleadosHuellas into @Empresa, @Empleado, @IdNumero
	end
	close EmpleadosHuellas
	deallocate EmpleadosHuellas
END		
go

/* Modificar SP WS_ACTUALIZA_STATUS_EMP (3/3) */
EXEC WS_ACTUALIZA_STATUS_EMP
GO

/* Modificar TRIGGER TU_TMNEXT_STATUS (1/2) */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id('TU_TMNEXT_STATUS') )
	DROP TRIGGER TU_TMNEXT_STATUS
GO

/* Modificar TRIGGER TU_TMNEXT_STATUS (2/2) */
Create trigger TU_TMNEXT_STATUS on dbo.TERM_MSG
after UPDATE AS
BEGIN
	set nocount on

	declare @nextMsg datetime 
	declare @dmCodigo int
	declare @tecodigo int 


	select @nextMsg = TM_NEXT from Inserted
	select @dmCodigo = DM_CODIGO from inserted
	select @tecodigo = TE_CODIGO from inserted
		
	if( update( TM_NEXT ) )
	begin
		if ( ( @dmCodigo = 4 ) or ( @dmCodigo = 6 ) or( @dmCodigo = 8 ) )
		begin
			if ( @nextMsg = Convert(datetime,'18991230') )
			begin
				EXECUTE WS_ACTUALIZA_STATUS_EMP	
			end
		end
	end
end
GO

/* Modificar TRIGGER TR_COMPANY_VEMPL_BAJA (1/2) */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id('TR_COMPANY_VEMPL_BAJA') )
	DROP TRIGGER TR_COMPANY_VEMPL_BAJA
GO


/* Modificar TRIGGER TR_COMPANY_VEMPL_BAJA (2/2) */
CREATE TRIGGER [dbo].[TR_COMPANY_VEMPL_BAJA] ON [dbo].[COMPANY] AFTER INSERT, DELETE, UPDATE AS
BEGIN
	SET NOCOUNT ON;
	IF UPDATE( CM_CODIGO ) OR UPDATE (CM_DATOS)
	BEGIN
		EXECUTE SP_CREAR_VEMPL_BAJA;	
		EXECUTE SP_CREAR_VEMPL_ALTA;
	END
	ELSE IF EXISTS (SELECT * FROM DELETED)
	BEGIN
		EXECUTE SP_CREAR_VEMPL_BAJA;
		EXECUTE SP_CREAR_VEMPL_ALTA;	
	END  
END

GO

/* Modificar PROCEDURE WS_TERMINAL_HUELLAFALTANTE (1/2) */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLAFALTANTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLAFALTANTE]
GO

/* Modificar PROCEDURE WS_TERMINAL_HUELLAFALTANTE (2/2) */
CREATE PROCEDURE [dbo].[WS_TERMINAL_HUELLAFALTANTE] ( @TE_CODIGO NumeroEmpleado )
as
begin
	SET NOCOUNT ON
		DECLARE @Ayer DATETIME
		DECLARE @TipoTerminal integer
		SET @Ayer = GETDATE() - 1

		SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )
		
		IF @TipoTerminal = 5 
		BEGIN
			SELECT TOP 1 E.ID_NUMERO, H.HU_ID, R.TE_CODIGO, R.GP_CODIGO, E.CM_CODIGO, E.CB_CODIGO, H.HU_HUELLA, H.HU_INDICE
			FROM   TERM_GPO AS R
			INNER JOIN EMP_BIO AS E ON R.GP_CODIGO = E.GP_CODIGO AND E.IV_CODIGO = 0 
			INNER JOIN HUELLAGTI AS H ON E.CM_CODIGO = H.CM_CODIGO AND E.CB_CODIGO = H.CB_CODIGO AND E.IV_CODIGO = H.IV_CODIGO
			LEFT OUTER JOIN VEMPL_ALTA AS VA ON VA.CB_CODIGO = E.CB_CODIGO AND VA.CM_CODIGO = E.CM_CODIGO
			LEFT OUTER JOIN TERM_HUELLA AS TH ON TH.TE_CODIGO = @TE_CODIGO AND H.CM_CODIGO = TH.CM_CODIGO AND H.CB_CODIGO = TH.CB_CODIGO 
			AND H.HU_INDICE = TH.HU_INDICE AND H.IV_CODIGO = TH.IV_CODIGO
			WHERE  R.TE_CODIGO = @TE_CODIGO AND TH.TE_CODIGO IS NULL AND ( ( E.CB_ACTIVO = 'S' AND VA.CB_FEC_ING <= GETDATE() ) OR E.CB_FEC_BAJ >= @Ayer )
			AND H.HU_INDICE = 11
			ORDER BY E.CM_CODIGO, E.CB_CODIGO
		END
		ELSE
		BEGIN		
			SELECT TOP 1 E.ID_NUMERO, H.HU_ID, R.TE_CODIGO, R.GP_CODIGO, E.CM_CODIGO, E.CB_CODIGO, H.HU_HUELLA, H.HU_INDICE
			FROM   TERM_GPO AS R
			INNER JOIN EMP_BIO AS E ON R.GP_CODIGO = E.GP_CODIGO AND E.IV_CODIGO = 0 
			INNER JOIN HUELLAGTI AS H ON E.CM_CODIGO = H.CM_CODIGO AND E.CB_CODIGO = H.CB_CODIGO AND E.IV_CODIGO = H.IV_CODIGO
			LEFT OUTER JOIN VEMPL_ALTA AS VA ON VA.CB_CODIGO = E.CB_CODIGO AND VA.CM_CODIGO = E.CM_CODIGO
			LEFT OUTER JOIN TERM_HUELLA AS TH ON TH.TE_CODIGO = @TE_CODIGO AND H.CM_CODIGO = TH.CM_CODIGO AND H.CB_CODIGO = TH.CB_CODIGO 
			AND H.HU_INDICE = TH.HU_INDICE AND H.IV_CODIGO = TH.IV_CODIGO
			WHERE  R.TE_CODIGO = @TE_CODIGO AND TH.TE_CODIGO IS NULL AND ( ( E.CB_ACTIVO = 'S' AND VA.CB_FEC_ING <= GETDATE() ) OR E.CB_FEC_BAJ >= @Ayer )
			AND ( ( H.HU_INDICE > 0 ) and ( H.HU_INDICE < 11 ) )
			ORDER BY E.CM_CODIGO, E.CB_CODIGO
		END

end
GO

/* Modificar PROCEDURE WS_TERMINAL_HUELLASACTUALES (1/2) */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLASACTUALES' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
GO

/* Modificar PROCEDURE WS_TERMINAL_HUELLASACTUALES (2/2) */
create PROCEDURE [dbo].[WS_TERMINAL_HUELLASACTUALES]
(
     @TE_CODIGO NumeroEmpleado
) as
begin
	SET NOCOUNT ON

	declare @Empleado integer;
	declare @Empresa CodigoEmpresa;
	declare @Ayer datetime
	DECLARE @TipoTerminal integer
	SET @Ayer = GETDATE() - 1

	SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

 	IF @TipoTerminal = 5 
	BEGIN
		Declare TmpEmpleadosBaja CURSOR for
        SELECT	TH.CM_CODIGO, TH.CB_CODIGO
		FROM	TERM_HUELLA AS TH 
		INNER JOIN EMP_BIO AS EB ON TH.CB_CODIGO = EB.CB_CODIGO AND TH.CM_CODIGO = EB.CM_CODIGO
		WHERE	(TH.TE_CODIGO = @TE_CODIGO) AND (( EB.CB_ACTIVO = 'N' AND EB.CB_FEC_BAJ >= @Ayer ) OR (EB.CB_ACTIVO = 'N' AND EB.CB_FEC_BAJ < @Ayer))
		and ( TH.HU_INDICE = 11 )
		GROUP BY TH.CM_CODIGO, TH.CB_CODIGO, TH.TE_CODIGO

		Open TmpEmpleadosBaja
		fetch NEXT FROM TmpEmpleadosBaja 
		into @Empresa, @Empleado

		while @@FETCH_STATUS = 0
       	begin
			update TERM_HUELLA set BORRAR = 'S' where ( TE_CODIGO = @TE_CODIGO ) and ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa )
			fetch NEXT FROM TmpEmpleadosBaja 
			into @Empresa, @Empleado
        end 

		close TmpEmpleadosBaja 
    	deallocate TmpEmpleadosBaja 
	END
	ELSE
	BEGIN
		Declare TmpEmpleadosBaja CURSOR for
        SELECT	TH.CM_CODIGO, TH.CB_CODIGO
		FROM	TERM_HUELLA AS TH 
		INNER JOIN EMP_BIO AS EB ON TH.CB_CODIGO = EB.CB_CODIGO AND TH.CM_CODIGO = EB.CM_CODIGO
		WHERE	(TH.TE_CODIGO = @TE_CODIGO) AND (( EB.CB_ACTIVO = 'N' AND EB.CB_FEC_BAJ >= @Ayer ) OR (EB.CB_ACTIVO = 'N' AND EB.CB_FEC_BAJ < @Ayer))
		and ( ( TH.HU_INDICE > 0 ) and ( TH.HU_INDICE < 11 ) )
		GROUP BY TH.CM_CODIGO, TH.CB_CODIGO, TH.TE_CODIGO

		Open TmpEmpleadosBaja
		fetch NEXT FROM TmpEmpleadosBaja 
		into @Empresa, @Empleado

		while @@FETCH_STATUS = 0
       	begin
			update TERM_HUELLA set BORRAR = 'S' where ( TE_CODIGO = @TE_CODIGO ) and ( CB_CODIGO = @Empleado ) and ( CM_CODIGO = @Empresa )
			fetch NEXT FROM TmpEmpleadosBaja 
			into @Empresa, @Empleado
        end 

		close TmpEmpleadosBaja 
    	deallocate TmpEmpleadosBaja 
	END
end

GO

/* Modificar PROCEDURE WS_TERMINAL_HUELLASOBRANTE (1/2) */
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLASOBRANTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[WS_TERMINAL_HUELLASOBRANTE]
GO

/* Modificar PROCEDURE WS_TERMINAL_HUELLASOBRANTE (2/2) */
CREATE PROCEDURE [dbo].[WS_TERMINAL_HUELLASOBRANTE]( @TE_CODIGO NumeroEmpleado ) 
as 
begin 
       SET NOCOUNT ON
       DECLARE @TipoTerminal integer
       declare @Hoy datetime
       SET @Hoy = CONVERT(datetime , CONVERT (char(10), getdate(), 103), 103 );

       SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

       IF @TipoTerminal = 5 
       BEGIN
             select * 
             from EMP_BIO 
             inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
             where TERM_HUELLA.BORRAR = 'S' 
             and ( TERM_HUELLA.TE_CODIGO = @TE_CODIGO ) and ( TERM_HUELLA.HU_INDICE = 11 ) 
             and ( EMP_BIO.CB_ACTIVO = 'N' AND EMP_BIO.CB_FEC_BAJ < @Hoy )
       END
       ELSE
       BEGIN
             select * 
             from EMP_BIO 
             inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
             where TERM_HUELLA.BORRAR = 'S' 
             and TERM_HUELLA.TE_CODIGO =@TE_CODIGO and ( TERM_HUELLA.HU_INDICE > 0 and TERM_HUELLA.HU_INDICE < 11 ) 
              and ( EMP_BIO.CB_ACTIVO = 'N' AND EMP_BIO.CB_FEC_BAJ < @Hoy )
       end
end
GO

IF EXISTS (SELECT name FROM sysobjects WHERE name = 'SP_GET_EMPRESAS_ACCESOS')
BEGIN
	DROP PROCEDURE SP_GET_EMPRESAS_ACCESOS
END
GO

CREATE PROCEDURE SP_GET_EMPRESAS_ACCESOS( @GR_CODIGO FolioChico, @FILTRO TextoLargo, @GR_CODIGO_ACTIVO FolioChico, @DERECHOS_OTROS_GRUPOS Booleano, @DERECHOS_GRUPO_PROPIO Booleano )
AS
BEGIN 
	SET NOCOUNT ON
	CREATE TABLE #EMPRESAS_ACCESOS 
	(
	CM_CODIGO char(10), 
	CM_NOMBRE varchar(50), 
	CM_ALIAS varchar(30), 
	CM_USRNAME varchar(15), 
	CM_PASSWRD varchar(70), 
	CM_ACUMULA char(10), 
	CM_CONTROL varchar(8), 
	CM_EMPATE int, 
	CM_USACAFE varchar(1), 
	CM_USACASE varchar(1), 
	CM_NIVEL0 varchar(255), 
	CM_DATOS varchar(255), 
	CM_DIGITO varchar(1), 
	ACCESOS int, 
	DB_CODIGO char(10),
	D_SIST_DATOS_GRUPOS int ,
	K_DERECHO_SIST_KARDEX int ,
	K_DERECHO_BANCA int,
	GR_CODIGO smallint,
	GR_DESCRIP varchar(30)
	)

	declare @Derechos int
	declare @CM_CODIGO char(10)
	declare @CM_NOMBRE varchar(50)
	declare @CM_ALIAS varchar(30)
	declare @CM_USRNAME varchar(15)
	declare @CM_PASSWRD varchar(70)
	declare @CM_ACUMULA char(10)
	declare @CM_CONTROL varchar(8)
	declare @CM_EMPATE int
	declare @CM_USACAFE varchar(1)
	declare @CM_USACASE varchar(1)
	declare @CM_NIVEL0 varchar(255)
	declare @CM_DATOS varchar(255)
	declare @CM_DIGITO varchar(1)
	declare @ACCESOS int
	declare @DB_CODIGO char(10)
	declare @D_SIST_DATOS_GRUPOS int
	declare @K_DERECHO_SIST_KARDEX int
	declare @K_DERECHO_BANCA int		
	declare @GR_DESCRIP varchar(30)
	declare @D_DATOS_GRUPOS int
	declare @sql nvarchar(max)
	declare @sqlCommand nvarchar(max)
	declare @columnList nvarchar(max)
	declare @TableSql nvarchar(max)
	declare @FiltroSql nvarchar(max)
	declare @OrderBy nvarchar(max)
	declare @GR_CODIGO_AUX FolioChico

	set @D_DATOS_GRUPOS = 205
	set @GR_CODIGO_AUX = @GR_CODIGO 
	if ( ( select Count (*) from dbo.GET_GRUPOS_HIJOS(@GR_CODIGO_ACTIVO) where GR_HIJO in (@GR_CODIGO) ) > 0 )
	begin
		set @GR_CODIGO = @GR_CODIGO_ACTIVO	
	end	

	SET @columnList = N'
				C.CM_CODIGO,
				C.CM_NOMBRE, 
				C.CM_ALIAS, 
				C.CM_USRNAME, 
				C.CM_PASSWRD, 
				C.CM_ACUMULA, 
				C.CM_CONTROL, 
				C.CM_EMPATE, 
				C.CM_USACAFE, 
				C.CM_USACASE, 
				C.CM_NIVEL0, 
				C.CM_DATOS, 
				C.CM_DIGITO, 
				( select count(*) from ACCESO A where ( A.GR_CODIGO = @GR_CODIGO_VALUE ) and ( A.CM_CODIGO = C.CM_CODIGO ) and ( A.AX_DERECHO > 0 ) ) as ACCESOS , 
				DB_CODIGO,
				( select A.AX_DERECHO from ACCESO A where (AX_NUMERO = @D_DATOS_GRUPOS_VALUE ) and ( A.GR_CODIGO = @GR_CODIGO_VALUE ) and ( A.CM_CODIGO = C.CM_CODIGO ) ) as D_SIST_DATOS_GRUPOS'
	SET @TableSql = N'company C'
	SET @FiltroSql = @FILTRO	
	SET @OrderBy = N'order by C.CM_CODIGO' 
	SET @sqlCommand = 'DECLARE Temporal cursor for SELECT ' + @columnList + ' FROM ' + @TableSql + ' WHERE ' + @FiltroSql + ' ' + @OrderBy
	
	exec sp_executesql @sqlCommand, N'@GR_CODIGO_VALUE int, @D_DATOS_GRUPOS_VALUE int', @GR_CODIGO_VALUE = @GR_CODIGO, @D_DATOS_GRUPOS_VALUE = @D_DATOS_GRUPOS

	Open Temporal;
	Fetch Next From Temporal Into 
		@CM_CODIGO, 
		@CM_NOMBRE, 
		@CM_ALIAS, 
		@CM_USRNAME, 
		@CM_PASSWRD, 
		@CM_ACUMULA, 
		@CM_CONTROL, 
		@CM_EMPATE, 
		@CM_USACAFE, 
		@CM_USACASE, 
		@CM_NIVEL0, 
		@CM_DATOS, 
		@CM_DIGITO, 
		@ACCESOS, 
		@DB_CODIGO,
		@D_SIST_DATOS_GRUPOS;

		while @@Fetch_Status = 0
		begin
			if @GR_CODIGO_ACTIVO = 1
			begin
				set @D_SIST_DATOS_GRUPOS = 96
				set @K_DERECHO_SIST_KARDEX = 1
				set @K_DERECHO_BANCA = 1
				insert into #EMPRESAS_ACCESOS values( @CM_CODIGO, 
					@CM_NOMBRE, 
					@CM_ALIAS, 
					@CM_USRNAME, 
					@CM_PASSWRD, 
					@CM_ACUMULA, 
					@CM_CONTROL, 
					@CM_EMPATE, 
					@CM_USACAFE, 
					@CM_USACASE, 
					@CM_NIVEL0, 
					@CM_DATOS, 
					@CM_DIGITO, 
					@ACCESOS, 
					@DB_CODIGO,
					@D_SIST_DATOS_GRUPOS,
					@K_DERECHO_SIST_KARDEX,
					@K_DERECHO_BANCA,
					@GR_CODIGO,
					@GR_DESCRIP )
			end 
			else
			begin				
				set @Derechos = 0
				set @Derechos = ( select @D_SIST_DATOS_GRUPOS & 32 ) + ( select @D_SIST_DATOS_GRUPOS & 64 )
				if ( @Derechos is not NULL ) 
				begin
					set @K_DERECHO_SIST_KARDEX = 0
					set @K_DERECHO_BANCA = 0

					if ( @Derechos = 32 )
					begin
						set @K_DERECHO_SIST_KARDEX = 1
						set @K_DERECHO_BANCA = 0
					end

					if ( @Derechos = 64 )
					begin
						set @K_DERECHO_SIST_KARDEX = 0
						set @K_DERECHO_BANCA = 1
					end
					
					if ( @Derechos = 96 )
					begin
						set @K_DERECHO_SIST_KARDEX = 1
						set @K_DERECHO_BANCA = 1
					end

					if ( @DERECHOS_OTROS_GRUPOS = 'S' ) and ( @DERECHOS_GRUPO_PROPIO = 'N' )
					begin
						if ( @GR_CODIGO_ACTIVO != @GR_CODIGO_AUX ) --Otro Grupo
						begin
							if ( @Derechos = 32 ) or  ( @Derechos = 96 )
							begin
								set @ACCESOS = 0
								select @ACCESOS = count(*) from ACCESO A where ( A.GR_CODIGO = @GR_CODIGO_AUX ) and ( A.CM_CODIGO = @CM_CODIGO ) and ( A.AX_DERECHO > 0 )

								select  @GR_DESCRIP = G.GR_DESCRIP from ACCESO A 
                                          left outer join GRUPO G on ( G.GR_CODIGO = A.GR_CODIGO ) 
                                          left outer join COMPANY C on ( C.CM_CODIGO = A.CM_CODIGO ) 
                                          where ( ( A.GR_CODIGO = @GR_CODIGO_AUX ) and ( A.CM_CODIGO = @CM_CODIGO ) ) 
                                          group by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE 
                                          ORDER by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE ;


								insert into #EMPRESAS_ACCESOS values( @CM_CODIGO, 
									@CM_NOMBRE, 
									@CM_ALIAS, 
									@CM_USRNAME, 
									@CM_PASSWRD, 
									@CM_ACUMULA, 
									@CM_CONTROL, 
									@CM_EMPATE, 
									@CM_USACAFE, 
									@CM_USACASE, 
									@CM_NIVEL0, 
									@CM_DATOS, 
									@CM_DIGITO, 
									@ACCESOS, 
									@DB_CODIGO,
									@D_SIST_DATOS_GRUPOS,
									@K_DERECHO_SIST_KARDEX,
									@K_DERECHO_BANCA,
									@GR_CODIGO_AUX,
									@GR_DESCRIP )							
							end
						end

					end

					if ( @DERECHOS_GRUPO_PROPIO = 'S' ) and ( @DERECHOS_OTROS_GRUPOS = 'N' )
					begin
						if ( @GR_CODIGO_ACTIVO = @GR_CODIGO_AUX ) --Propio Grupo
						begin
							if ( @Derechos = 64 ) or  ( @Derechos = 96 )
							begin
								set @ACCESOS = 0
								select @ACCESOS = count(*) from ACCESO A where ( A.GR_CODIGO = @GR_CODIGO_AUX ) and ( A.CM_CODIGO = @CM_CODIGO ) and ( A.AX_DERECHO > 0 )								
								select  @GR_DESCRIP = G.GR_DESCRIP from ACCESO A 
                                          left outer join GRUPO G on ( G.GR_CODIGO = A.GR_CODIGO ) 
                                          left outer join COMPANY C on ( C.CM_CODIGO = A.CM_CODIGO ) 
                                          where ( ( A.GR_CODIGO = @GR_CODIGO_AUX ) and ( A.CM_CODIGO = @CM_CODIGO ) ) 
                                          group by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE 
                                          ORDER by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE ;
								insert into #EMPRESAS_ACCESOS values( @CM_CODIGO, 
									@CM_NOMBRE, 
									@CM_ALIAS, 
									@CM_USRNAME, 
									@CM_PASSWRD, 
									@CM_ACUMULA, 
									@CM_CONTROL, 
									@CM_EMPATE, 
									@CM_USACAFE, 
									@CM_USACASE, 
									@CM_NIVEL0, 
									@CM_DATOS, 
									@CM_DIGITO, 
									@ACCESOS, 
									@DB_CODIGO,
									@D_SIST_DATOS_GRUPOS,
									@K_DERECHO_SIST_KARDEX,
									@K_DERECHO_BANCA,
									@GR_CODIGO_AUX,
									@GR_DESCRIP )
							end

						end
					end

					if ( @DERECHOS_OTROS_GRUPOS = 'S' ) and ( @DERECHOS_GRUPO_PROPIO = 'S' )
					begin
						if ( @Derechos = 32 ) or ( @Derechos = 64 ) or  ( @Derechos = 96 )
						begin
							set @ACCESOS = 0
							select @ACCESOS = count(*) from ACCESO A where ( A.GR_CODIGO = @GR_CODIGO_AUX ) and ( A.CM_CODIGO = @CM_CODIGO ) and ( A.AX_DERECHO > 0 )							
							select  @GR_DESCRIP = G.GR_DESCRIP from ACCESO A 
                                          left outer join GRUPO G on ( G.GR_CODIGO = A.GR_CODIGO ) 
                                          left outer join COMPANY C on ( C.CM_CODIGO = A.CM_CODIGO ) 
                                          where ( ( A.GR_CODIGO = @GR_CODIGO_AUX ) and ( A.CM_CODIGO = @CM_CODIGO ) ) 
                                          group by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE 
                                          ORDER by A.GR_CODIGO, G.GR_DESCRIP, A.CM_CODIGO, C.CM_NOMBRE ;
							insert into #EMPRESAS_ACCESOS values( @CM_CODIGO, 
								@CM_NOMBRE, 
								@CM_ALIAS, 
								@CM_USRNAME, 
								@CM_PASSWRD, 
								@CM_ACUMULA, 
								@CM_CONTROL, 
								@CM_EMPATE, 
								@CM_USACAFE, 
								@CM_USACASE, 
								@CM_NIVEL0, 
								@CM_DATOS, 
								@CM_DIGITO, 
								@ACCESOS, 
								@DB_CODIGO,
								@D_SIST_DATOS_GRUPOS,
								@K_DERECHO_SIST_KARDEX,
								@K_DERECHO_BANCA,
								@GR_CODIGO_AUX,
								@GR_DESCRIP )
						end
					end			
				end
			end			
			Fetch Next From Temporal into @CM_CODIGO, 
			@CM_NOMBRE, 
			@CM_ALIAS, 
			@CM_USRNAME, 
			@CM_PASSWRD, 
			@CM_ACUMULA, 
			@CM_CONTROL, 
			@CM_EMPATE, 
			@CM_USACAFE, 
			@CM_USACASE, 
			@CM_NIVEL0, 
			@CM_DATOS, 
			@CM_DIGITO, 
			@ACCESOS, 
			@DB_CODIGO,
			@D_SIST_DATOS_GRUPOS
		end			

	select * from #EMPRESAS_ACCESOS
	drop table #EMPRESAS_ACCESOS
	CLOSE Temporal;
	DEALLOCATE Temporal;
END
GO

UPDATE ACCESO SET AX_DERECHO = case 
		when (AX_DERECHO & 2) > 0  then AX_DERECHO + 8 
		else AX_DERECHO 
	end
WHERE AX_NUMERO = 68
GO
