
/*  Tabla TERM_MSG. Agregar campo TM_NEXT_RL para guardar el �ltimo valor de TM_NEXT */
if not exists( select * from syscolumns where NAME = 'TM_NEXT_RL' and id = object_id('TERM_MSG'))
begin
     ALTER TABLE TERM_MSG add TM_NEXT_RL Fecha Null;
end
GO

update TERM_MSG set TM_NEXT_RL = TM_NEXT where TM_NEXT_RL is null;
GO

alter table TERM_MSG alter column TM_NEXT_RL Fecha not null;
GO



/* Modificaciones a WS_TERMINAL_SETPENDIENTE para que los mensajes no se recorran (1/2) */
if exists (select * from sysobjects where id = object_id('WS_TERMINAL_SETPENDIENTE') )
 drop procedure WS_TERMINAL_SETPENDIENTE
GO

/* Modificaciones a WS_TERMINAL_SETPENDIENTE para que los mensajes no se recorran (2/2) */
CREATE PROCEDURE WS_TERMINAL_SETPENDIENTE ( @TE_CODIGO NUMEROEMPLEADO, @DM_CODIGO CODIGO ) 
AS 
BEGIN
  DECLARE @TIPOCONFIGURACION BOOLEANO;
  DECLARE @HORA CHAR(6) 
  DECLARE @TM_NEXT Fecha;

  SELECT @TIPOCONFIGURACION = TM_NEXTXHR FROM TERM_MSG WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO;
   
  IF @TIPOCONFIGURACION = 'N'
  BEGIN
		-- Establecer valor para @TM_NEXT mientras no sea mayor a GETDATE()
		SELECT @TM_NEXT = TM_NEXT_RL FROM TERM_MSG WHERE DM_CODIGO = @DM_CODIGO AND TE_CODIGO = @TE_CODIGO		

		WHILE @TM_NEXT < GETDATE()
		BEGIN
				SELECT @TM_NEXT = CASE WHEN TM_SLEEP = 0 THEN '29991231' ELSE DATEADD(MI, TM_SLEEP, @TM_NEXT) END 
					FROM TERM_MSG WHERE DM_CODIGO = @DM_CODIGO AND TE_CODIGO = @TE_CODIGO 
		END		
		
		UPDATE TERM_MSG SET TM_NEXT = @TM_NEXT, TM_NEXT_RL = @TM_NEXT WHERE DM_CODIGO = @DM_CODIGO AND TE_CODIGO = @TE_CODIGO
  END
  ELSE
  BEGIN
		SELECT @HORA = ' ' + SUBSTRING (TM_NEXTHOR, 1, 2) +':' + SUBSTRING (TM_NEXTHOR, 3,2) FROM TERM_MSG
		WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO;

		IF CAST (CONVERT(VARCHAR(10),GETDATE(),111) + @HORA AS datetime) > GETDATE()
		BEGIN
			UPDATE TERM_MSG SET TM_NEXT =  
				CAST (CONVERT(VARCHAR(10),GETDATE(),111) + @HORA AS datetime)				 
				WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO
		END
		ELSE
		BEGIN
			UPDATE TERM_MSG SET TM_NEXT =			  
				CAST (CONVERT(VARCHAR(10), DATEADD(DAY, 1, GETDATE()) ,111) + @HORA AS datetime)				 
				WHERE TE_CODIGO = @TE_CODIGO AND DM_CODIGO = @DM_CODIGO
		END
  END
END
GO

if exists (select * from sysobjects where id = object_id('TR_ACTUALIZA_TMNEXT') )
 drop trigger TR_ACTUALIZA_TMNEXT
GO

CREATE trigger TR_ACTUALIZA_TMNEXT on TERM_MSG
after UPDATE AS
BEGIN
	set nocount on

	declare @nextMsg datetime 
	declare @dmCodigo int
	declare @tecodigo int 


	select @nextMsg = TM_NEXT from Inserted
	select @dmCodigo = DM_CODIGO from inserted
	select @tecodigo = TE_CODIGO from inserted
		
	if( update( TM_NEXT ) )
	begin
		if( @dmCodigo = 8 and @nextmsg > Convert(datetime,'18991230') )
		begin
			EXECUTE('delete from TERM_HUELLA where TE_CODIGO='+@tecodigo)
			EXECUTE('update Term_MSG set tm_next =''18991230'' where TE_CODIGO='+@tecodigo+' and dm_Codigo = 4')
		end 

		if(@nextmsg > Convert(datetime,'18991230'))
		begin
			EXECUTE('update Term_MSG set tm_ultima =getdate() where TE_CODIGO='+@tecodigo+' and dm_Codigo = '+@dmCodigo)
		end
		else if (@nextmsg = Convert(datetime,'18991230'))
		begin
			EXECUTE('update Term_MSG set tm_next_rl =getdate() where TE_CODIGO='+@tecodigo+' and dm_Codigo = '+@dmCodigo)
		end
	end
end
GO

IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'TerminalError' AND OBJECT_ID = OBJECT_ID('TERM_HUELLA'))
BEGIN
	alter table TERM_HUELLA
	add TerminalError NumeroEmpleado null;
END
GO

IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'TerminalError' AND OBJECT_ID = OBJECT_ID('TERM_HUELLA'))
BEGIN
	update TERM_HUELLA set TerminalError = 0;
END
GO

IF EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME = 'TerminalError' AND OBJECT_ID = OBJECT_ID('TERM_HUELLA'))
BEGIN
	alter table TERM_HUELLA alter column TerminalError NumeroEmpleado not null;
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_SETAGREGAHUELLA' AND type in (N'P', N'PC'))
	DROP PROCEDURE WS_TERMINAL_SETAGREGAHUELLA
GO

CREATE PROCEDURE WS_TERMINAL_SETAGREGAHUELLA ( @TE_CODIGO NumeroEmpleado, @HU_ID INT, @Error INT )
AS
BEGIN
	SET NOCOUNT ON   
	DECLARE @P1 CodigoEmpresa;
	DECLARE @P2 NumeroEmpleado;
	DECLARE @P3 integer;
	DECLARE @LlaveAUpdate integer;
	DECLARE @TipoTerminal integer

	SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

	IF @TipoTerminal = 5 
	BEGIN
		select @P1 = CM_CODIGO, @P2 = CB_CODIGO, @p3 = HU_INDICE from HUELLAGTI where ( HU_ID = @HU_ID ) and ( HU_INDICE = 11 )
	END
	ELSE
	BEGIN   
		select @P1 = CM_CODIGO, @P2 = CB_CODIGO, @p3 = HU_INDICE from HUELLAGTI where ( HU_ID = @HU_ID ) and ( ( HU_INDICE > 0 ) and ( HU_INDICE < 11 ) )
	END

	IF EXISTS(SELECT *
				FROM TERM_HUELLA
			WHERE CM_CODIGO = @P1
			AND CB_CODIGO = @P2
			AND HU_INDICE = @P3
			AND TE_CODIGO = @TE_CODIGO)
	BEGIN
		SELECT @LlaveAUpdate = LLAVE FROM TERM_HUELLA WHERE ( CM_CODIGO = @P1 ) AND ( CB_CODIGO = @P2 ) AND ( HU_INDICE = @P3 ) AND ( TE_CODIGO = @TE_CODIGO )
		UPDATE TERM_HUELLA
		SET CM_CODIGO = @P1, CB_CODIGO = @P2, HU_INDICE = @P3, TE_CODIGO = @TE_CODIGO, TerminalError = @Error
		WHERE LLAVE = @LlaveAUpdate
	END
	ELSE
	BEGIN
		INSERT INTO TERM_HUELLA(CM_CODIGO, CB_CODIGO, HU_INDICE, TE_CODIGO, TerminalError)
		SELECT CM_CODIGO, CB_CODIGO, HU_INDICE, @TE_CODIGO, @Error
		FROM HUELLAGTI
		WHERE HU_ID = @HU_ID
	END
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_BORRAHUELLA_TODAS' AND type in (N'P', N'PC'))
	DROP PROCEDURE WS_TERMINAL_BORRAHUELLA_TODAS
GO

CREATE PROCEDURE WS_TERMINAL_BORRAHUELLA_TODAS ( @TE_CODIGO NumeroEmpleado, @ID_NUMERO int )
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @TipoTerminal integer
	DECLARE @CM_CODIGO Codigoempresa 
	DECLARE @CB_CODIGO NumeroEmpleado 
	DECLARE @HU_INDICE usuario 	

	SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

	SELECT @CM_CODIGO = Coalesce( CM_CODIGO, '' ), @CB_CODIGO = Coalesce( CB_CODIGO, 0 )
	FROM EMP_BIO 
	where ID_NUMERO = @ID_NUMERO;

    IF @TipoTerminal = 5 
    BEGIN
		DELETE FROM TERM_HUELLA 
		WHERE  ( (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (TE_CODIGO = @TE_CODIGO) and ( HU_INDICE = 11 ) )
						
		UPDATE TERM_HUELLA SET BORRAR = 'S'
		WHERE  ( (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) and ( HU_INDICE = 11 ) )
						
		DELETE FROM HUELLAGTI WHERE ( CB_CODIGO = @CB_CODIGO and CM_CODIGO = @CM_CODIGO and ( HU_INDICE = 11 ) )		
    END
    ELSE
    BEGIN
        DELETE FROM TERM_HUELLA 
        WHERE  ( (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (TE_CODIGO = @TE_CODIGO) and ( HU_INDICE > 0 ) and ( HU_INDICE < 11 ) )

		UPDATE TERM_HUELLA SET BORRAR = 'S'
        WHERE  ( (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) and ( HU_INDICE > 0 ) and ( HU_INDICE < 11 ) )
			
		DELETE FROM HUELLAGTI WHERE ( CB_CODIGO = @CB_CODIGO and CM_CODIGO = @CM_CODIGO and ( HU_INDICE > 0 ) and ( HU_INDICE < 11 ) )			
    END	
              
    select @CM_CODIGO as Empresa, @CB_CODIGO as Empleado
END
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE name = 'WS_TERMINAL_HUELLASOBRANTE' AND type in (N'P', N'PC'))
	DROP PROCEDURE WS_TERMINAL_HUELLASOBRANTE
GO

CREATE PROCEDURE WS_TERMINAL_HUELLASOBRANTE( @TE_CODIGO NumeroEmpleado ) 
as 
begin 
       SET NOCOUNT ON
       DECLARE @TipoTerminal integer
       declare @Hoy datetime
       SET @Hoy = CONVERT(datetime , CONVERT (char(10), getdate(), 103), 103 );

       SELECT @TipoTerminal = TE_ID from TERMINAL where ( TE_CODIGO = @TE_CODIGO )

       IF @TipoTerminal = 5 
       BEGIN
             select * 
             from EMP_BIO 
             inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
             where TERM_HUELLA.BORRAR = 'S' 
             and ( TERM_HUELLA.TE_CODIGO = @TE_CODIGO ) and ( TERM_HUELLA.HU_INDICE = 11 )             
       END
       ELSE
       BEGIN
             select * 
             from EMP_BIO 
             inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
             where TERM_HUELLA.BORRAR = 'S' 
             and TERM_HUELLA.TE_CODIGO =@TE_CODIGO and ( TERM_HUELLA.HU_INDICE > 0 and TERM_HUELLA.HU_INDICE < 11 )              
       end
end
GO