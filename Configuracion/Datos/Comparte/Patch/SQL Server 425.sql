/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* TABLA ACCESO: JB Se actualiza permiso especial 68 - 1830 :Derechos Acceso Recalcular Tarjetas*/
/* Patch 425 # Seq: 1 */
	UPDATE #COMPARTE..ACCESO SET AX_DERECHO = 7 WHERE (AX_NUMERO = 68) AND (AX_DERECHO <> 0)
GO

/* TABLA ACCESO: EZ Se actualiza permiso especial 475-Autorizacion de Prenomina en Supervisores - 1825 Autorizacion de Prenomina */
/* Patch 425 # Seq:  2*/
	UPDATE #COMPARTE..ACCESO SET AX_DERECHO = 7 WHERE (AX_NUMERO = 475) AND (AX_DERECHO <> 0)
GO
/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq:3 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'PATHARCHIVO' )
	EXEC SP_ADDTYPE PATHARCHIVO, 'VARCHAR(150)', 'NOT NULL'
GO
/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 4 */
EXEC SP_BINDEFAULT STRINGVACIO, PATHARCHIVO
GO
/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 5 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'CONDICION' )
	EXEC SP_ADDTYPE CONDICION, 'CHAR(8)', 'NOT NULL'
GO
/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 6 */
EXEC SP_BINDEFAULT CODIGOVACIO, CONDICION
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 7 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'NOMBRECAMPO' )
	EXEC SP_ADDTYPE NOMBRECAMPO, 'VARCHAR(10)', 'NOT NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 8 */
EXEC SP_BINDEFAULT STRINGVACIO, NOMBRECAMPO
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 9 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'TITULOCAMPO' )
	EXEC SP_ADDTYPE TITULOCAMPO, 'VARCHAR(30)', 'NOT NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 10 */
EXEC SP_BINDEFAULT STRINGVACIO, TITULOCAMPO
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 11 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'CAMPOSREQUERIDOS' )
	EXEC SP_ADDTYPE CAMPOSREQUERIDOS, 'VARCHAR(255)', 'NOT NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 12 */
EXEC SP_BINDEFAULT STRINGVACIO, CAMPOSREQUERIDOS
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 13 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'TAMANIO' )
	EXEC SP_ADDTYPE TAMANIO, 'NUMERIC(15,3)', 'NOT NULL'
GO
/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 14 */
EXEC SP_BINDEFAULT CERO, TAMANIO
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 15 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'MASCARA' )
	EXEC SP_ADDTYPE MASCARA, 'VARCHAR(20)', 'NOT NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 16 */
EXEC SP_BINDEFAULT STRINGVACIO, MASCARA
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 17 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'WINDOWSNAME' )
	EXEC SP_ADDTYPE WINDOWSNAME, 'VARCHAR(32)', 'NOT NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 18 */
EXEC SP_BINDEFAULT STRINGVACIO, WINDOWSNAME
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 19 */
IF NOT EXISTS( SELECT * FROM SYSOBJECTS WHERE NAME='NEWGUID' AND XTYPE = 'D' )
	CREATE DEFAULT NEWGUID AS NEWID()
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 20 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'GUID' )
	EXEC SP_ADDTYPE GUID, 'UNIQUEIDENTIFIER', 'NOT NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 21 */
EXEC SP_BINDEFAULT NEWGUID, GUID
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 22 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'NULLGUID' )
	EXEC SP_ADDTYPE NULLGUID, 'UNIQUEIDENTIFIER', 'NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 23 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'BINARIO' )
	EXEC SP_ADDTYPE BINARIO, 'IMAGE', 'NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 24 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'XMLDATA' )
	EXEC SP_ADDTYPE XMLDATA, 'TEXT', 'NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 25 */
IF NOT EXISTS( SELECT * FROM SYSTYPES WHERE NAME = 'NOTAS' )
	EXEC SP_ADDTYPE NOTAS, 'TEXT', 'NULL'
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 26 */
IF NOT EXISTS( SELECT * FROM SYSCOLUMNS WHERE NAME = 'US_PAG_DEF' AND ID = OBJECT_ID(N'[DBO].[USUARIO]') )
	ALTER TABLE USUARIO ADD US_PAG_DEF USUARIO
GO

/* TIPO DATOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 27 */
UPDATE USUARIO SET US_PAG_DEF = 0 WHERE ( US_PAG_DEF IS NULL )
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 28 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'DICCION' AND XTYPE = 'U' )
	CREATE TABLE DICCION (
		   DI_CLASIFI           STATUS,
		   DI_NOMBRE            NOMBRECAMPO,
		   DI_TITULO            TITULOCAMPO,
		   DI_CALC              STATUS,
		   DI_ANCHO             STATUS,
		   DI_MASCARA           DESCRIPCION,
		   DI_TFIELD            STATUS,
		   DI_ORDEN             BOOLEANO,
		   DI_REQUIER           CAMPOSREQUERIDOS,
		   DI_TRANGO            STATUS,
		   DI_NUMERO            STATUS,
		   DI_VALORAC           DESCRIPCION,
		   DI_RANGOAC           STATUS,
		   DI_CLAVES            TITULOCAMPO,
		   DI_TCORTO            TITULOCAMPO,
		   DI_CONFI             BOOLEANO
	)
GO

/* XIE1DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 29 */
IF NOT EXISTS( SELECT * FROM SYSINDEXES WHERE NAME = 'XIE1DICCION' AND ID = OBJECT_ID(N'DICCION') )
	CREATE INDEX XIE1DICCION ON DICCION
	(
		   DI_CLASIFI,
		   DI_CALC
	)
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 30 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_DICCION' AND XTYPE = 'PK' )
	ALTER TABLE DICCION
		   ADD CONSTRAINT PK_DICCION PRIMARY KEY (DI_CLASIFI, DI_NOMBRE)

GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 31 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PROCESO' AND XTYPE = 'U' )
	CREATE TABLE PROCESO (
		   PC_PROC_ID           STATUS,
		   PC_NUMERO            FOLIOGRANDE,
		   US_CODIGO            USUARIO,
		   PC_FEC_INI           FECHA,
		   PC_HOR_INI           HORAMINUTOSSEGUNDOS,
		   PC_FEC_FIN           FECHA,
		   PC_HOR_FIN           HORAMINUTOSSEGUNDOS,
		   PC_ERROR             BOOLEANO,
		   CB_CODIGO            NUMEROEMPLEADO,
		   PC_MAXIMO            FOLIOGRANDE,
		   PC_PASO              FOLIOGRANDE,
		   US_CANCELA           USUARIO,
		   PC_PARAMS            FORMULA
	)
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 32 */
IF NOT EXISTS( SELECT * FROM SYSINDEXES WHERE NAME = 'XIE1PROCESO' AND ID = OBJECT_ID(N'PROCESO') )
	CREATE INDEX XIE1PROCESO ON PROCESO
	(
		   PC_FEC_INI,
		   PC_ERROR
	)
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 33*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_PROCESO' AND XTYPE = 'PK' )
	ALTER TABLE PROCESO
		   ADD CONSTRAINT PK_PROCESO PRIMARY KEY (PC_NUMERO)
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 34 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'QUERYS' AND XTYPE = 'U' )
	CREATE TABLE QUERYS (
		   QU_CODIGO            CONDICION,
		   QU_DESCRIP           FORMULA,
		   QU_FILTRO            FORMULA,
		   QU_NIVEL             STATUS,
		   US_CODIGO            USUARIO
	)
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 35 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_QUERYS' AND XTYPE = 'PK' )
	ALTER TABLE QUERYS
		   ADD CONSTRAINT PK_QUERYS PRIMARY KEY (QU_CODIGO)
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 36 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'REPORTE' AND XTYPE = 'U' )
	CREATE TABLE REPORTE (
		   RE_CODIGO            FOLIOCHICO,
		   RE_NOMBRE            DESCRIPCION,
		   RE_TIPO              STATUS,
		   RE_TITULO            TITULO,
		   RE_ENTIDAD           STATUS,
		   RE_SOLOT             BOOLEANO,
		   RE_FECHA             FECHA,
		   RE_GENERAL           DESCRIPCION,
		   RE_REPORTE           PATHARCHIVO,
		   RE_CFECHA            NOMBRECAMPO,
		   RE_IFECHA            STATUS,
		   RE_HOJA              STATUS,
		   RE_ALTO              TAMANIO,
		   RE_ANCHO             TAMANIO,
		   RE_PRINTER           FORMULA,
		   RE_COPIAS            STATUS,
		   RE_PFILE             STATUS,
		   RE_ARCHIVO           PATHARCHIVO,
		   RE_VERTICA           BOOLEANO,
		   US_CODIGO            USUARIO,
		   RE_FILTRO            FORMULA,
		   RE_COLNUM            STATUS,
		   RE_COLESPA           TAMANIO,
		   RE_RENESPA           TAMANIO,
		   RE_MAR_SUP           TAMANIO,
		   RE_MAR_IZQ           TAMANIO,
		   RE_MAR_DER           TAMANIO,
		   RE_MAR_INF           TAMANIO,
		   RE_NIVEL             STATUS,
		   RE_FONTNAM           WINDOWSNAME,
		   RE_FONTSIZ           STATUS,
		   QU_CODIGO            CONDICION,
		   RE_CLASIFI           STATUS,
		   RE_CANDADO           STATUS
	)
GO

/* DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 37 */
IF NOT EXISTS( SELECT * FROM SYSINDEXES WHERE NAME = 'XAK1REPORTE' AND ID = OBJECT_ID(N'REPORTE') )
	CREATE UNIQUE INDEX XAK1REPORTE ON REPORTE
	(
		   RE_NOMBRE
	)
GO

/* XIE1REPORTE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 38 */
IF NOT EXISTS( SELECT * FROM SYSINDEXES WHERE NAME = 'XIE1REPORTE' AND ID = OBJECT_ID(N'REPORTE') )
	CREATE INDEX XIE1REPORTE ON REPORTE
	(
		   RE_CLASIFI,
		   RE_NOMBRE
	)
GO

/* PK_REPORTE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 39 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_REPORTE' AND XTYPE = 'PK' )
	ALTER TABLE REPORTE
		   ADD CONSTRAINT PK_REPORTE PRIMARY KEY (RE_CODIGO)
GO

/* CAMPOREP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 40 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'CAMPOREP' AND XTYPE = 'U' )
	CREATE TABLE CAMPOREP (
		   RE_CODIGO            FOLIOCHICO,
		   CR_TIPO              STATUS,
		   CR_POSICIO           STATUS,
		   CR_CLASIFI           STATUS,
		   CR_SUBPOS            STATUS,
		   CR_TABLA             STATUS,
		   CR_TITULO            TITULOCAMPO,
		   CR_REQUIER           FORMULA,
		   CR_CALC              STATUS,
		   CR_MASCARA           MASCARA,
		   CR_ANCHO             STATUS,
		   CR_OPER              STATUS,
		   CR_TFIELD            STATUS,
		   CR_SHOW              STATUS,
		   CR_DESCRIP           NOMBRECAMPO,
		   CR_COLOR             STATUS,
		   CR_FORMULA           FORMULA,
		   CR_BOLD              BOOLEANO,
		   CR_ITALIC            BOOLEANO,
		   CR_SUBRAYA           BOOLEANO,
		   CR_STRIKE            BOOLEANO,
		   CR_ALINEA            STATUS
	)
GO

/* CAMPOREP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 41 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_CAMPOREP' AND XTYPE = 'PK' )
	ALTER TABLE CAMPOREP
		   ADD CONSTRAINT PK_CAMPOREP PRIMARY KEY (RE_CODIGO, CR_TIPO, CR_POSICIO,
				  CR_SUBPOS)
GO

/* CAMPOREP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 42 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_CAMPOREP_REPORTE' AND XTYPE = 'F' )
	ALTER TABLE CAMPOREP
		   ADD CONSTRAINT FK_CAMPOREP_REPORTE
		   FOREIGN KEY (RE_CODIGO)
		   REFERENCES REPORTE
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

/* MISREPOR : CR Integracion WF a Version*/
/* Patch 425 # Seq: 43 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'MISREPOR' AND XTYPE = 'U' )
	CREATE TABLE MISREPOR (
		   US_CODIGO            USUARIO,
		   RE_CODIGO            FOLIOCHICO )
GO

/* MISREPOR : CR Integracion WF a Version*/
/* Patch 425 # Seq: 44*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_MISREPOR' AND XTYPE = 'PK' )
	ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

/* MISREPOR : CR Integracion WF a Version*/
/* Patch 425 # Seq: 45 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_MISREPOR_REPORTE' AND XTYPE = 'F' )
	ALTER TABLE MISREPOR
		   ADD CONSTRAINT FK_MISREPOR_REPORTE
		   FOREIGN KEY (RE_CODIGO)
		   REFERENCES REPORTE
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

/* WACCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 46 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WACCION' AND XTYPE = 'U' )
	CREATE TABLE WACCION(                         /* ACCIONES */
		WA_CODIGO           CODIGO,           /* C�DIGO */
		WA_NOMBRE           OBSERVACIONES,    /* NOMBRE */
		WA_DESCRIP          MEMO              /* DESCRIPCI�N */
	)
GO

/* WACCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 47 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WACCION' AND XTYPE = 'PK' )
	ALTER TABLE WACCION
		  ADD CONSTRAINT PK_WACCION PRIMARY KEY( WA_CODIGO )
GO

/* WACCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 48 */
IF NOT EXISTS( SELECT WA_CODIGO FROM WACCION WHERE WA_CODIGO = 'FIRMA' )
	INSERT INTO WACCION( WA_CODIGO, WA_NOMBRE, WA_DESCRIP ) VALUES ( 'FIRMA', 'Pide Autorizaci�n', 'Env�a eMail solicitando autorizaci�n del documento' )
GO

/* WACCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 49
IF NOT EXISTS( SELECT WA_CODIGO FROM WACCION WHERE WA_CODIGO = 'GRABA3' )
	INSERT INTO WACCION( WA_CODIGO, WA_NOMBRE, WA_DESCRIP ) VALUES ( 'GRABA3', 'Registra en TRESS', 'Registra los datos del documento en Sistema TRESS' )
GO

/* WPROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 50 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WPROCESO' AND XTYPE = 'U' )
	CREATE TABLE WPROCESO(                       /* PROCESOS */
		WP_FOLIO           FOLIOGRANDE,      /* PROCESO ( FK WPROCESO ) */
		WM_CODIGO          CODIGO,           /* MODELO ( FK WMODELO ) */
		WP_NOMBRE          OBSERVACIONES,    /* NOMBRE */
		WP_USR_INI         USUARIO,          /* USUARIO SOLICITANTE ( FK USUARIO ) */
		WP_FEC_INI         FECHA,            /* FECHA DE INICIO */
		WP_FEC_FIN         FECHA,            /* ULTIMA ACTIVIDAD */
		WP_STATUS          STATUS,           /* STATUS ( ZETACOMMONLISTS.EWFPROCESOSTATUS ) */
		WP_PASOS           FOLIOCHICO,       /* PASOS TOTALES */
		WP_PASO            FOLIOCHICO,       /* PASO ACTUAL */
		WP_XMLDATA         XMLDATA,
		WP_NOTAS           NOTAS,            /* OBSERVACIONES */
		WP_DATA            BINARIO
	)
GO

/* WPROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq:51 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WPROCESO' AND XTYPE = 'PK' )
	ALTER TABLE WPROCESO
		  ADD CONSTRAINT PK_WPROCESO PRIMARY KEY( WP_FOLIO )
GO

/* WARCHIVO : CR Integracion WF a Version*/
/* Patch 425 # Seq:52 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WARCHIVO' AND XTYPE = 'U' )
	CREATE TABLE WARCHIVO(                       /* ARCHIVOS DE PROCESOS */
		WP_FOLIO            FOLIOGRANDE,     /* PROCESO ( FK WPROCESO ) */
		WH_ORDEN            FOLIOCHICO,      /* ORDEN */
		WH_NOMBRE           ACCESOS,         /* NOMBRE */
		WH_RUTA             ACCESOS          /* URL */
	)
GO

/* WARCHIVO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 53 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WARCHIVO' AND XTYPE = 'PK' )
	ALTER TABLE WARCHIVO
		  ADD CONSTRAINT PK_WARCHIVO PRIMARY KEY( WP_FOLIO, WH_ORDEN )
GO

/* WARCHIVO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 54 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WARCHIVO_WPROCESO' AND XTYPE = 'F' )
	ALTER TABLE WARCHIVO
		   ADD CONSTRAINT FK_WARCHIVO_WPROCESO
		   FOREIGN KEY (WP_FOLIO)
		   REFERENCES WPROCESO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;

GO

/* WMENSAJE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 55 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WMENSAJE' AND XTYPE = 'U' )
	CREATE TABLE WMENSAJE(                       /* MENSAJES */
		WN_GUID            GUID,             /* IDENTIFICADOR */
		WP_FOLIO           FOLIOGRANDE,      /* PROCESO ( FK WPROCESO, WPROSTEP ) */
		WE_ORDEN           FOLIOCHICO,       /* PASO ( FK WPROSTEP ) */
		WT_GUID            NULLGUID,         /* TAREA ( FK WTAREA ) */
		WN_USR_INI         USUARIO,          /* USUARIO ORIGINAL ( FK USUARIO ) */
		WN_USR_FIN         USUARIO,          /* USUARIO RESPONSABLE ( FK USUARIO ) */
		WN_FECHA           FECHA,            /* FECHA */
		WN_NOTAS           MEMO,             /* OBSERVACIONES */
		WN_MENSAJE         DESCRIPCION,      /* MENSAJE */
		WN_LEIDO           BOOLEANO          /* YA FU� PROCESADO */
	)
GO

/* WMENSAJE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 56 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WMENSAJE' AND XTYPE = 'PK' )
	ALTER TABLE WMENSAJE
		  ADD CONSTRAINT PK_WMENSAJE PRIMARY KEY( WN_GUID )
GO

/* PASOS_MODELO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 57 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PASOS_MODELO' AND XTYPE = 'FN' )
CREATE FUNCTION DBO.PASOS_MODELO( @WM_CODIGO CODIGO ) RETURNS INTEGER
AS
BEGIN
     RETURN ( SELECT COUNT(*) FROM WMODSTEP WHERE ( WM_CODIGO = @WM_CODIGO ) )
END
GO

/* WMODELO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 58 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WMODELO' AND XTYPE = 'U' )
	CREATE TABLE WMODELO (                       /* MODELOS */
		WM_CODIGO          CODIGO,           /* C�DIGO */
		WM_NOMBRE          OBSERVACIONES,    /* NOMBRE */
		WM_DESCRIP         MEMO,             /* DESCRIPCI�N */
		WM_TIPNOT1         STATUS,           /* NOTIFICACI�N INICIAL: TIPO ( ZETACOMMONLISTS.EWFNOTIFICACION ) */
		WM_USRNOT1         OBSERVACIONES,    /* NOTIFICACI�N INICIAL: LISTA */
		WM_TIPNOT2         STATUS,           /* NOTIFICACI�N FINAL: TIPO ( ZETACOMMONLISTS.EWFNOTIFICACION ) */
		WM_USRNOT2         OBSERVACIONES,    /* NOTIFICACI�N FINAL: LISTA */
		WM_TIPNOT3         STATUS,           /* NOTIFICACI�N CANCELACI�N: TIPO ( ZETACOMMONLISTS.EWFNOTIFICACION ) */
		WM_USRNOT3         OBSERVACIONES,    /* NOTIFICACI�N CANCELACI�N: LISTA */
		WM_URL             ACCESOS,          /* URL PARA SOLICITAR */
		WM_URL_DEL         ACCESOS,          /* URL PARA DELEGAR */
		WM_URL_EXA         ACCESOS,          /* URL PARA CONSULTAR */
		WM_URL_OK          ACCESOS,          /* URL PARA APROBAR */
		WM_STATUS          STATUS,           /* STATUS ( ZETACOMMONLISTS.EWFSTATUSMODELO ) */
		WM_PASOS           AS ( DBO.PASOS_MODELO( WM_CODIGO ) ),  /* PASOS TOTALES */
		WM_XSL_ACT         NOMBREARCHIVO,    /* XSL NOTIFICACI�N DE ACTIVACI�N */
		WM_XSL_CAN         NOMBREARCHIVO,    /* XSL NOTIFICACI�N DE CANCELACI�N */
		WM_XSL_TER         NOMBREARCHIVO,    /* XSL NOTIFICACI�N DE TERMINACI�N */
			WM_FORMATO         NOMBREARCHIVO     /* ARCHIVO FUENTE */
	)
GO

/* WMODELO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 59*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WMODELO' AND XTYPE = 'PK' )
	ALTER TABLE WMODELO
		  ADD CONSTRAINT PK_WMODELO PRIMARY KEY( WM_CODIGO )
GO
/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 60*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WMODSTEP' AND XTYPE = 'U' )
	CREATE TABLE WMODSTEP(                       /* PASOS DE MODELO */
		WM_CODIGO          CODIGO,           /* MODELO ( FK WMODELO ) */
		WE_ORDEN           FOLIOCHICO,       /* PASO */
		WA_CODIGO          CODIGO,           /* ACCI�N ( FK WACCION ) */
		WE_NOMBRE          OBSERVACIONES,    /* NOMBRE */
		WE_DESCRIP         MEMO,             /* DESCRIPCI�N */
		WE_TIPDEST         STATUS,           /* RESPONSABILIDAD: TIPO ( ZETACOMMONLISTS.EWFNOTIFICACION ) */
		WE_USRDEST         OBSERVACIONES,    /* RESPONSABILIDAD: LISTA */
		WE_TIPNOT1         STATUS,           /* NOTIFICACI�N INICIAL: TIPO ( ZETACOMMONLISTS.EWFNOTIFICACION ) */
		WE_USRNOT1         OBSERVACIONES,    /* NOTIFICACI�N INICIAL: LISTA */
		WE_TIPNOT2         STATUS,           /* NOTIFICACI�N FINAL: TIPO ( ZETACOMMONLISTS.EWFNOTIFICACION ) */
		WE_USRNOT2         OBSERVACIONES,    /* NOTIFICACI�N FINAL: LISTA */
		WE_TIPNOT3         STATUS,           /* NOTIFICACI�N CANCELACI�N: TIPO ( ZETACOMMONLISTS.EWFNOTIFICACION ) */
		WE_USRNOT3         OBSERVACIONES,    /* NOTIFICACI�N CANCELACI�N: LISTA */
		WE_XSL_ACT         NOMBREARCHIVO,    /* XSL NOTIFICACI�N DE ACTIVACI�N */
		WE_XSL_CAN         NOMBREARCHIVO,    /* XSL NOTIFICACI�N DE CANCELACI�N */
		WE_XSL_TER         NOMBREARCHIVO,    /* XSL NOTIFICACI�N DE TERMINACI�N */
		WE_XSL_TAR         NOMBREARCHIVO     /* XSL AVISO */
	)
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 61 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WMODSTEP' AND XTYPE = 'PK' )
	ALTER TABLE WMODSTEP
		  ADD CONSTRAINT PK_WMODSTEP PRIMARY KEY( WM_CODIGO, WE_ORDEN )
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 62*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WMODSTEP_WMODELO' AND XTYPE = 'F' )
	ALTER TABLE WMODSTEP
		   ADD CONSTRAINT FK_WMODSTEP_WMODELO
		   FOREIGN KEY (WM_CODIGO)
		   REFERENCES WMODELO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 63 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WMODSTEP_WACCION' AND XTYPE = 'F' )
	ALTER TABLE WMODSTEP
		   ADD CONSTRAINT FK_WMODSTEP_WACCION
		   FOREIGN KEY (WA_CODIGO)
		   REFERENCES WACCION
		   ON DELETE NO ACTION
		   ON UPDATE CASCADE;
GO

/* WOUTBOX : CR Integracion WF a Version*/
/* Patch 425 # Seq: 64 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WOUTBOX' AND XTYPE = 'U' )
	CREATE TABLE WOUTBOX(                        /* BUZ�N */
		WO_GUID            GUID,             /* IDENTIFICADOR */
		WT_GUID            NULLGUID,         /* TAREA ( FK WTAREA ) */
		WP_FOLIO           FOLIOGRANDE,      /* PROCESO ( FK WPROCESO ) */
		WO_TO              MEMO,             /* DESTINATARIOS */
		WO_CC              MEMO,             /* CON COPIA A */
		WO_SUBJECT         ACCESOS,          /* TEMA */
		WO_BODY            MEMO,             /* TEXTO */
		WO_FROM_NA         ACCESOS,          /* REMITENTE - NOMBRE */
		WO_FROM_AD         ACCESOS,          /* REMITENTE - DIRECCI�N */
		WO_SUBTYPE         STATUS,           /* FORMATO ( ZETACOMMONLIST.EEMAILTYPE ) */
			WO_STATUS          STATUS,           /* STATUS ( ZETACOMMONLISTS.EWFCORREOSTATUS ) */
		WO_ENVIADO         BOOLEANO,         /* YA FU� ENVIADO */
		WO_FEC_IN          FECHA,            /* FECHA DE CREACI�N */
		WO_FEC_OUT         FECHA,            /* FECHA DE ENV�O */
		WO_TSTATUS         STATUS,
		WO_TAVANCE         FOLIOCHICO,
		US_ENVIA           USUARIO,          /* REMITENTE - USUARIO ( FK USUARIO ) */
			US_RECIBE          USUARIO,          /* DESTINATARIO - USUARIO ( FK USUARIO ) */
			WO_LOG             FORMULA           /* BIT�CORA */
	)
GO

/* WOUTBOX : CR Integracion WF a Version*/
/* Patch 425 # Seq: 65*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WOUTBOX' AND XTYPE = 'PK' )
	ALTER TABLE WOUTBOX
		  ADD CONSTRAINT PK_WOUTBOX PRIMARY KEY( WO_GUID )
GO

/* WPROSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 66 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WPROSTEP' AND XTYPE = 'U' )
	CREATE TABLE WPROSTEP(                       /* PASO DE PROCESO */
		WP_FOLIO           FOLIOGRANDE,      /* PROCESO ( FK WPROCESO ) */
		WE_ORDEN           FOLIOCHICO,       /* PASO */
		WA_CODIGO          CODIGO,           /* ACCI�N ( FK WACCION ) */
		WM_CODIGO          CODIGO,           /* MODELO ( FK WMODELO ) */
		WS_NOMBRE          OBSERVACIONES,    /* NOMBRE  */
		WS_STATUS          STATUS,           /* STATUS ( ZETACOMMONLISTS.EWFPASOPROCESOSTATUS ) */
		WS_USR_INI         USUARIO,          /* USUARIO ORIGINAL ( FK USUARIO ) */
		WS_FEC_INI         FECHA,            /* FECHA DE INICIO */
		WS_FEC_FIN         FECHA,            /* FECHA DE TERMINACI�N */
		WS_NEXT            FOLIOCHICO        /* SIGUIENTE PASO */
	)
GO

/* WPROSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 67 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WPROSTEP' AND XTYPE = 'PK' )
	ALTER TABLE WPROSTEP
		  ADD CONSTRAINT PK_WPROSTEP PRIMARY KEY( WP_FOLIO, WE_ORDEN )
GO

/* WPROSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 68 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WPROSTEP_WPROCESO' AND XTYPE = 'F' )
	ALTER TABLE WPROSTEP
		   ADD CONSTRAINT FK_WPROSTEP_WPROCESO
		   FOREIGN KEY (WP_FOLIO)
		   REFERENCES WPROCESO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

/* WTAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 69 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WTAREA' AND XTYPE = 'U' )
	CREATE TABLE WTAREA(                         /* TAREAS */
		WT_GUID            GUID,             /* IDENTIFICADOR */
		WP_FOLIO           FOLIOGRANDE,      /* PROCESO ( FK WPROCESO, WPROSTEP ) */
		WE_ORDEN           FOLIOCHICO,       /* PASO ( FK WPROSTEP ) */
		WT_DESCRIP         OBSERVACIONES,    /* DESCRIPCI�N */
		WT_STATUS          STATUS,           /* STATUS ( ZETACOMMONLISTS.EWFTAREASTATUS ) */
		WT_FEC_INI         FECHA,            /* FECHA DE INICIO */
		WT_FEC_FIN         FECHA,            /* FECHA DE TERMINACI�N */
		WT_USR_ORI         USUARIO,          /* USUARIO ORIGINAL */
		WT_USR_DES         USUARIO,          /* USUARIO RESPONSABLE */
		WT_NOTAS           NOTAS,            /* OBSERVACIONES */
		WT_AVANCE          FOLIOCHICO        /* AVANCE */
	)
GO

/* WTAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 70*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WTAREA' AND XTYPE = 'PK' )
	ALTER TABLE WTAREA
		  ADD CONSTRAINT PK_WTAREA PRIMARY KEY( WT_GUID )
GO

/* WTAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 71 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WTAREA_WPROSTEP' AND XTYPE = 'F' )
	ALTER TABLE WTAREA
		   ADD CONSTRAINT FK_WTAREA_WPROSTEP
		   FOREIGN KEY (WP_FOLIO, WE_ORDEN)
		   REFERENCES WPROSTEP
		   ON DELETE NO ACTION
		   ON UPDATE CASCADE;
GO

/* WMOD_ROL : CR Integracion WF a Version*/
/* Patch 425 # Seq: 72 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WMOD_ROL' AND XTYPE = 'U' )
	CREATE TABLE WMOD_ROL(                       /* ROLES ASIGNADOS A MODELOS ( Y VICEVERSA ) */
		   WM_CODIGO           CODIGO,           /* C�DIGO DEL MODELO */
		   RO_CODIGO           CODIGO            /* C�DIGO DEL ROL */
	)
GO

/* WMOD_ROL : CR Integracion WF a Version*/
/* Patch 425 # Seq: 73 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WMOD_ROL' AND XTYPE = 'PK' )
	ALTER TABLE WMOD_ROL
		  ADD CONSTRAINT PK_WMOD_ROL PRIMARY KEY( WM_CODIGO, RO_CODIGO )
GO

/* WMOD_ROL : CR Integracion WF a Version*/
/* Patch 425 # Seq: 74*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WMOD_ROL_MODELO' AND XTYPE = 'F' )
	ALTER TABLE WMOD_ROL
		   ADD CONSTRAINT FK_WMOD_ROL_MODELO
		   FOREIGN KEY (WM_CODIGO)
		   REFERENCES WMODELO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

/* WMOD_ROL : CR Integracion WF a Version*/
/* Patch 425 # Seq: 75 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WMOD_ROL_ROL' AND XTYPE = 'F' )
	ALTER TABLE WMOD_ROL
		   ADD CONSTRAINT FK_WMOD_ROL_ROL
		   FOREIGN KEY (RO_CODIGO)
		   REFERENCES ROL
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

/* V_JEFES : CR Integracion WF a Version*/
/* Patch 425 # Seq: 76 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_JEFES' AND XTYPE = 'V' )
	DROP VIEW V_JEFES
GO
	
/* V_JEFES : CR Integracion WF a Version*/
/* Patch 425 # Seq: 77 */	
CREATE VIEW V_JEFES( US_CODIGO, US_NOMBRE )
AS
SELECT U.US_JEFE,
       ( SELECT U2.US_NOMBRE FROM USUARIO U2 WHERE ( U2.US_CODIGO = U.US_JEFE ) ) AS US_NOMBRE
FROM USUARIO U WHERE ( U.US_JEFE IS NOT NULL ) AND ( U.US_JEFE <> 0 )
GO

/* V_PADRE : CR Integracion WF a Version*/
/* Patch 425 # Seq:78 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_PADRE' AND XTYPE = 'V' )
	DROP VIEW V_PADRE
GO

/* V_PADRE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 79 */
CREATE VIEW V_PADRE( GR_CODIGO, GR_DESCRIP )
AS
SELECT G.GR_PADRE,
       ( SELECT G2.GR_DESCRIP FROM GRUPO G2 WHERE ( G2.GR_CODIGO = G.GR_PADRE ) ) AS GR_DESCRIP
FROM GRUPO G WHERE ( G.GR_PADRE IS NOT NULL ) AND ( G.GR_PADRE <> 0 )
GO

/* V_PASOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 80 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_PASOS' AND XTYPE = 'V' )
	DROP VIEW V_PASOS
GO

/* GETNOMBREUSUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 81 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'GETNOMBREUSUARIO' AND XTYPE = 'FN' )
CREATE FUNCTION DBO.GETNOMBREUSUARIO( @USUARIO USUARIO ) RETURNS DESCRIPCION
AS
BEGIN
  DECLARE @NOMBRE DESCRIPCION;
  SELECT @NOMBRE = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIO );
  RETURN @NOMBRE;
END
GO

/* V_PASOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 82 */
CREATE VIEW V_PASOS( WP_FOLIO,               /**/
                     WM_CODIGO,              /**/
                     WP_NOMBRE,              /**/
                     WP_FEC_INI,             /**/
                     WP_FEC_FIN,             /**/
                     WP_STATUS,              /**/
                     WP_PASO,                /**/
                     WP_PASOS,               /**/
                     WP_USR_INI,             /**/
                     WP_NOM_INI,             /**/
                     WP_NOTAS,               /**/
                     WE_ORDEN,               /* PASO ( FK WPROSTEP ) */
                     WS_NOMBRE,              /**/
                     WS_STATUS,              /**/
                     WS_USR_INI,             /**/
                     WS_NOM_INI,             /**/
                     WS_FEC_INI,             /**/
                     WS_FEC_FIN,             /**/
                     WT_GUID,                /* TAREA ( FK WTAREA ) */
                     WT_DESCRIP,             /* TAREA: DESCRIPCI�N */
                     WT_STATUS,              /**/
                     WT_FEC_INI,             /**/
                     WT_FEC_FIN,             /**/
                     WT_USR_ORI,             /**/
                     WT_NOM_ORI,             /**/
                     WT_USR_DES,             /**/
                     WT_NOM_DES,             /**/
                     WT_NOTAS,               /**/
                     WT_AVANCE,              /**/
                     WM_DESCRIP,             /* MODELO: DESCRIPCI�N */
                     WE_DESCRIP )            /* PASO DE MODELO: DESCRIPCI�N */
AS
SELECT WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP
FROM WPROSTEP
JOIN WPROCESO ON ( WPROSTEP.WP_FOLIO = WPROCESO.WP_FOLIO )
JOIN WMODELO ON ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
JOIN WMODSTEP ON ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) AND ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
LEFT OUTER JOIN WTAREA ON ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) AND ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
GO

/* V_PROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 83 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_PROCESO' AND XTYPE = 'V' )
	DROP VIEW V_PROCESO
GO

/* V_PROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 84*/
CREATE VIEW V_PROCESO( WP_FOLIO,             /**/
                       WM_CODIGO,            /**/
                       WP_NOMBRE,            /**/
                       WP_FEC_INI,           /**/
                       WP_FEC_FIN,           /**/
                       WP_STATUS,            /**/
                       WP_PASO,              /**/
                       WP_PASOS,             /**/
                       WP_USR_INI,           /**/
                       WP_NOM_INI,           /**/
                       WP_NOTAS,             /**/
                       WE_ORDEN,             /* PASO ( FK WPROSTEP ) */
                       WS_NOMBRE,            /**/
                       WS_STATUS,            /**/
                       WS_USR_INI,           /**/
                       WS_NOM_INI,           /**/
                       WS_FEC_INI,           /**/
                       WS_FEC_FIN,           /**/
                       WT_GUID,              /* TAREA ( FK WTAREA ) */
                       WT_DESCRIP,           /* TAREA: DESCRIPCI�N */
                       WT_STATUS,            /**/
                       WT_FEC_INI,           /**/
                       WT_FEC_FIN,           /**/
                       WT_USR_ORI,           /**/
                       WT_NOM_ORI,           /**/
                       WT_USR_DES,           /**/
                       WT_NOM_DES,           /**/
                       WT_NOTAS,             /**/
                       WT_AVANCE,            /**/
                       WM_DESCRIP,           /* MODELO: DESCRIPCI�N */
                       WE_DESCRIP,           /* PASO DE MODELO: DESCRIPCI�N */
                       WM_URL_EXA,           /**/
                       WM_URL_DEL )          /**/
AS
SELECT WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP,
       WMODELO.WM_URL_EXA,
       WMODELO.WM_URL_DEL
FROM WPROCESO
JOIN WMODELO ON ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
LEFT OUTER JOIN WPROSTEP ON ( WPROCESO.WP_FOLIO = WPROSTEP.WP_FOLIO ) AND ( WPROCESO.WP_PASO = WPROSTEP.WE_ORDEN )
LEFT OUTER JOIN WTAREA ON ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) AND ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
LEFT OUTER JOIN WMODSTEP ON ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) AND ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
GO

/* V_TAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 85 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_TAREA' AND XTYPE = 'V' )
	DROP VIEW V_TAREA
GO
/* V_TAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 86 */
CREATE VIEW V_TAREA( WP_FOLIO,               /**/
                     WM_CODIGO,              /**/
                     WP_NOMBRE,              /**/
                     WP_FEC_INI,             /**/
                     WP_FEC_FIN,             /**/
                     WP_STATUS,              /**/
                     WP_PASO,                /**/
                     WP_PASOS,               /**/
                     WP_USR_INI,             /**/
                     WP_NOM_INI,             /**/
                     WP_NOTAS,               /**/
                     WE_ORDEN,               /* PASO ( FK WPROSTEP ) */
                     WS_NOMBRE,              /**/
                     WS_STATUS,              /**/
                     WS_USR_INI,             /**/
                     WS_NOM_INI,             /**/
                     WS_FEC_INI,             /**/
                     WS_FEC_FIN,             /**/
                     WT_GUID,                /* TAREA ( FK WTAREA ) */
                     WT_DESCRIP,             /* TAREA: DESCRIPCI�N */
                     WT_STATUS,              /**/
                     WT_FEC_INI,             /**/
                     WT_FEC_FIN,             /**/
                     WT_USR_ORI,             /**/
                     WT_NOM_ORI,             /**/
                     WT_USR_DES,             /**/
                     WT_NOM_DES,             /**/
                     WT_NOTAS,               /**/
                     WT_AVANCE,              /**/
                     WM_DESCRIP,             /**/
                     WE_DESCRIP,             /* PASO DE MODELO: DESCRIPCI�N */
                     WM_URL,                 /**/
                     WM_URL_EXA,             /**/
                     WM_URL_OK,              /**/
                     WM_URL_DEL )            /**/
AS
SELECT WPROCESO.WP_FOLIO,
       WPROCESO.WM_CODIGO,
       WPROCESO.WP_NOMBRE,
       WPROCESO.WP_FEC_INI,
       WPROCESO.WP_FEC_FIN,
       WPROCESO.WP_STATUS,
       WPROCESO.WP_PASO,
       WPROCESO.WP_PASOS,
       WPROCESO.WP_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROCESO.WP_USR_INI ),
       WPROCESO.WP_NOTAS,

       WPROSTEP.WE_ORDEN,
       WPROSTEP.WS_NOMBRE,
       WPROSTEP.WS_STATUS,
       WPROSTEP.WS_USR_INI,
       DBO.GETNOMBREUSUARIO( WPROSTEP.WS_USR_INI ),
       WPROSTEP.WS_FEC_INI,
       WPROSTEP.WS_FEC_FIN,

       WTAREA.WT_GUID,
       WTAREA.WT_DESCRIP,
       WTAREA.WT_STATUS,
       WTAREA.WT_FEC_INI,
       WTAREA.WT_FEC_FIN,
       WTAREA.WT_USR_ORI,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_ORI ),
       WTAREA.WT_USR_DES,
       DBO.GETNOMBREUSUARIO( WTAREA.WT_USR_DES ),
       WTAREA.WT_NOTAS,
       WTAREA.WT_AVANCE,

       WMODELO.WM_DESCRIP,
       WMODSTEP.WE_DESCRIP,
       WMODELO.WM_URL,
       WMODELO.WM_URL_EXA,
       WMODELO.WM_URL_OK,
       WMODELO.WM_URL_DEL

FROM WTAREA
JOIN WPROSTEP ON ( WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO ) AND ( WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN )
JOIN WPROCESO ON ( WTAREA.WP_FOLIO = WPROCESO.WP_FOLIO )
JOIN WMODELO ON ( WPROCESO.WM_CODIGO = WMODELO.WM_CODIGO )
JOIN WMODSTEP ON ( WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO ) AND ( WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN )
GO

/* V_MODELOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 87 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_MODELOS' AND XTYPE = 'V' )
	DROP VIEW V_MODELOS
GO

/* V_MODELOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 88 */
CREATE VIEW V_MODELOS( WM_CODIGO, RO_CODIGO, US_CODIGO ) AS
SELECT WMOD_ROL.WM_CODIGO, WMOD_ROL.RO_CODIGO, USER_ROL.US_CODIGO
FROM WMOD_ROL
JOIN USER_ROL ON ( USER_ROL.RO_CODIGO = WMOD_ROL.RO_CODIGO )
GO

/* WMODNOTI : CR Integracion WF a Version*/
/* Patch 425 # Seq: 89 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WMODNOTI' AND XTYPE = 'U' )
	CREATE TABLE WMODNOTI (
		   WM_CODIGO            CODIGO,
		   WE_ORDEN             FOLIOCHICO,
		   WY_FOLIO             FOLIOGRANDE IDENTITY,
		   WY_QUIEN             STATUS,
		   WY_USR_NOT           OBSERVACIONES,
		   WY_XSL               NOMBREARCHIVO,
		   WY_CUANDO            STATUS,
		   WY_TIEMPO            FOLIOGRANDE,
		   WY_QUE               STATUS,
		   WY_MODELO            CODIGO,
		   WY_TEXTO             OBSERVACIONES,
		   WY_VECES             FOLIOGRANDE,
		   WY_CADA              FOLIOGRANDE
	)
GO

/* WMODNOTI : CR Integracion WF a Version*/
/* Patch 425 # Seq: 90 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_WMODNOTI' AND XTYPE = 'PK' )
	ALTER TABLE WMODNOTI
		  ADD CONSTRAINT PK_WMODNOTI PRIMARY KEY( WM_CODIGO, WE_ORDEN, WY_FOLIO )
GO

/* WMODNOTI : CR Integracion WF a Version*/
/* Patch 425 # Seq: 91 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_WMODNOTI_WMODSTEP' AND XTYPE = 'F' )
	ALTER TABLE WMODNOTI
		   ADD CONSTRAINT FK_WMODNOTI_WMODSTEP
		   FOREIGN KEY (WM_CODIGO,WE_ORDEN)
		   REFERENCES WMODSTEP
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

/* WCONEXION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 92 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'WCONEXION' AND XTYPE = 'U' )
	CREATE TABLE WCONEXION
	(
		WX_FOLIO FOLIOGRANDE IDENTITY (1, 1) NOT NULL,
		WX_NOMBRE DESCRIPCION NOT NULL,
		WX_URL ACCESOS NOT NULL,
		WX_TIP_AUT STATUS NOT NULL,
		WX_USUARIO USUARIOCORTO,
		WX_PASSWRD PASSWD,
		WX_TIP_ENC STATUS NOT NULL
	)
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 93 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'WE_LIMITE' AND ID = OBJECT_ID(N'[DBO].[WMODSTEP]') )
	ALTER TABLE WMODSTEP ADD WE_LIMITE FOLIOGRANDE
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 94 */
UPDATE WMODSTEP
	SET    WE_LIMITE = 0
	WHERE  WE_LIMITE IS NULL
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 95 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'WE_URL_OK' AND ID = OBJECT_ID(N'[DBO].[WMODSTEP]') )
	ALTER TABLE WMODSTEP ADD WE_URL_OK ACCESOS
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 96 */
UPDATE WMODSTEP
	SET    WE_URL_OK = ''
	WHERE  WE_URL_OK IS NULL
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 97 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'WX_FOLIO' AND ID = OBJECT_ID(N'[DBO].[WMODSTEP]') )
	ALTER TABLE WMODSTEP ADD WX_FOLIO FOLIOGRANDE
GO

/* WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 98 */
UPDATE WMODSTEP
	SET    WX_FOLIO = 0
	WHERE  WX_FOLIO IS NULL
GO

/* WMENSAJE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 99 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'WN_VECES' AND ID = OBJECT_ID(N'[DBO].[WMENSAJE]') )
	ALTER TABLE WMENSAJE ADD WN_VECES FOLIOCHICO
GO

/* WMENSAJE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 100 */
UPDATE WMENSAJE
	SET    WN_VECES = 0
	WHERE  WN_VECES IS NULL
GO

/* WMENSAJE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 101 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'WY_FOLIO' AND ID = OBJECT_ID(N'[DBO].[WMENSAJE]') )
	ALTER TABLE WMENSAJE ADD WY_FOLIO FOLIOGRANDE
GO

/* WMENSAJE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 102 */
UPDATE WMENSAJE
	SET    WY_FOLIO = 0
	WHERE  WY_FOLIO IS NULL
GO

/* USUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 103 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'US_WF_OUT' AND ID = OBJECT_ID(N'[DBO].[USUARIO]') )
	ALTER TABLE USUARIO ADD US_WF_OUT BOOLEANO
GO

/* USUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 104 */
UPDATE USUARIO
	SET    US_WF_OUT = 'N'
	WHERE  US_WF_OUT IS NULL
GO

/* USUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 105 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'US_WF_ALT' AND ID = OBJECT_ID(N'[DBO].[USUARIO]') )
	ALTER TABLE USUARIO ADD US_WF_ALT USUARIO
GO

/* USUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 106 */
UPDATE USUARIO
	SET    US_WF_ALT = US_CODIGO
	WHERE  US_WF_ALT IS NULL
GO

/* WMENSAJE : CR Integracion WF a Version*/
/* Patch 425 # Seq: 107 */
IF NOT EXISTS( SELECT * FROM SYSINDEXES WHERE NAME = 'XIE1WMENSAJE' AND ID = OBJECT_ID(N'WMENSAJE') )
	CREATE INDEX XIE1WMENSAJE ON WMENSAJE
	(
		  WN_FECHA
	)
GO

/* WPROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 108 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'CB_CODIGO' AND ID = OBJECT_ID(N'[DBO].[WPROCESO]') )
	ALTER TABLE WPROCESO ADD CB_CODIGO NUMEROEMPLEADO
GO

/* WPROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 109 */
UPDATE WPROCESO
	SET    CB_CODIGO = 0
	WHERE  CB_CODIGO IS NULL
GO

/* WPROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 110 */
IF NOT EXISTS( SELECT NAME FROM SYSCOLUMNS WHERE NAME = 'CM_CODIGO' AND ID = OBJECT_ID(N'[DBO].[WPROCESO]') )
	ALTER TABLE WPROCESO ADD CM_CODIGO CODIGOEMPRESA
GO

/* WPROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 111 */
UPDATE WPROCESO
	SET    CM_CODIGO = ''
	WHERE  CM_CODIGO IS NULL
GO

/* WACCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 112 */
IF NOT EXISTS( SELECT WA_CODIGO FROM WACCION WHERE WA_CODIGO = 'CALLWS' )
	INSERT INTO WACCION VALUES ('CALLWS', 'Comunica a WebService', 'Envia el XMLData a un Webservice especificado')
GO

/* V_PROCESO_EMP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 113 */
IF EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'V_PROCESO_EMP' AND XTYPE = 'V' )
	DROP VIEW V_PROCESO_EMP
GO

/* V_PROCESO_EMP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 114 */
CREATE VIEW V_PROCESO_EMP
AS
SELECT
  WPROCESO.WP_FOLIO,
  WPROCESO.WM_CODIGO,
  WPROCESO.WP_NOMBRE,
  WPROCESO.WP_FEC_INI,
  WPROCESO.WP_FEC_FIN,
  WPROCESO.WP_STATUS,
  WPROCESO.WP_PASO,
  WPROCESO.WP_PASOS,
  WPROCESO.WP_USR_INI,
  DBO.GETNOMBREUSUARIO(WPROCESO.WP_USR_INI) AS WP_NOM_INI,
  WPROCESO.WP_NOTAS,
  WPROCESO.CB_CODIGO,
  WPROCESO.CM_CODIGO,
  WPROSTEP.WE_ORDEN,
  WPROSTEP.WS_NOMBRE,
  WPROSTEP.WS_STATUS,
  WPROSTEP.WS_USR_INI,
  DBO.GETNOMBREUSUARIO(WPROSTEP.WS_USR_INI) AS WS_NOM_INI,
  WPROSTEP.WS_FEC_INI,
  WPROSTEP.WS_FEC_FIN,
  WTAREA.WT_GUID,
  WTAREA.WT_DESCRIP,
  WTAREA.WT_STATUS,
  WTAREA.WT_FEC_INI,
  WTAREA.WT_FEC_FIN,
  WTAREA.WT_USR_ORI,
  DBO.GETNOMBREUSUARIO(WTAREA.WT_USR_ORI) AS WT_NOM_ORI,
  WTAREA.WT_USR_DES,
  DBO.GETNOMBREUSUARIO(WTAREA.WT_USR_DES) AS WT_NOM_DES,
  WTAREA.WT_NOTAS,
  WTAREA.WT_AVANCE,
  WMODELO.WM_DESCRIP,
  WMODSTEP.WE_DESCRIP,
  WMODELO.WM_URL_EXA,
  WMODELO.WM_URL_DEL AS WM_URL_DEL
  FROM WPROCESO
  INNER JOIN WMODELO
   ON  WPROCESO.WM_CODIGO  = WMODELO.WM_CODIGO
  LEFT OUTER JOIN WPROSTEP
   ON  WPROCESO.WP_FOLIO  = WPROSTEP.WP_FOLIO
   AND WPROCESO.WP_PASO  = WPROSTEP.WE_ORDEN
  LEFT OUTER JOIN WTAREA
   ON  WPROSTEP.WP_FOLIO = WTAREA.WP_FOLIO
   AND WPROSTEP.WE_ORDEN = WTAREA.WE_ORDEN
  LEFT OUTER JOIN WMODSTEP
   ON WPROSTEP.WM_CODIGO = WMODSTEP.WM_CODIGO
   AND WPROSTEP.WE_ORDEN = WMODSTEP.WE_ORDEN
GO

/* V_PROCESO_EMP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 115 */
DROP FUNCTION DBO.GETNOMBREUSUARIO
GO

/* SUSCRIP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 116 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'SUSCRIP' AND XTYPE = 'U' )
CREATE TABLE SUSCRIP (
       RE_CODIGO            FolioChico,
       US_CODIGO            Usuario,
       SU_FRECUEN           Status,
       SU_ENVIAR            Status,
       SU_VACIO             Status
)
GO
/* SUSCRIP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 117 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_SUSCRIP' AND XTYPE = 'PK' )
ALTER TABLE SUSCRIP
       ADD CONSTRAINT PK_SUSCRIP PRIMARY KEY (RE_CODIGO, US_CODIGO)
GO
/* SUSCRIP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 118 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'FK_Suscrip_Reporte' AND XTYPE = 'F' )
ALTER TABLE SUSCRIP
       ADD CONSTRAINT FK_Suscrip_Reporte
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* QUERYS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 119 */
IF NOT EXISTS( SELECT * FROM SYSCOLUMNS WHERE NAME = 'QU_NAVEGA' AND ID = OBJECT_ID(N'[DBO].[QUERYS]') )
	ALTER TABLE QUERYS ADD QU_NAVEGA BOOLEANO;
GO

/* QUERYS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 120 */
UPDATE QUERYS SET QU_NAVEGA='N' WHERE ( QU_NAVEGA IS NULL )
GO

/* QUERYS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 121 */
IF NOT EXISTS( SELECT * FROM SYSCOLUMNS WHERE NAME = 'QU_ORDEN' AND ID = OBJECT_ID(N'[DBO].[QUERYS]') )
	ALTER TABLE QUERYS ADD QU_ORDEN FOLIOCHICO;
GO

/* QUERYS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 122 */
UPDATE QUERYS SET QU_ORDEN=0 WHERE ( QU_ORDEN IS NULL )
GO

/* QUERYS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 123 */
IF NOT EXISTS( SELECT * FROM SYSCOLUMNS WHERE NAME = 'QU_CANDADO' AND ID = OBJECT_ID(N'[DBO].[QUERYS]') )
	ALTER TABLE QUERYS ADD QU_CANDADO BOOLEANO
GO

/* QUERYS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 124 */
UPDATE QUERYS SET QU_CANDADO='N' WHERE ( QU_CANDADO IS NULL )
GO

/* Z_BITPASOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 125 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'Z_BITPASOS' AND XTYPE = 'U' )
	CREATE TABLE Z_BITPASOS
	(
		ID FOLIOGRANDE IDENTITY,
		CM_CODIGO CODIGOEMPRESA,
		TP_SOLICIT CODIGO,
		NO_PASO FOLIOCHICO,
		DESCRIPCIO NOMBREARCHIVO,
		COMENTARIO TEXTOBITACORA,
		MSG_ERROR TEXTOBITACORA
	);
GO

/* Z_BITPASOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 126*/
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME = 'PK_Z_BITPASOS' AND XTYPE = 'PK' )
	ALTER TABLE Z_BITPASOS
		ADD CONSTRAINT PK_Z_BITPASOS PRIMARY KEY ( ID );
GO

/* WVALUSO: CR Integraci�n de WF a Version*/
/* Patch 425 # Seq: 127 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME='WVALUSO' AND XTYPE='U' )
	CREATE TABLE WVALUSO
	(
		MODULO  NOMBRECAMPO,
		USO		DESCRIPCION
	)
GO

/* WVALUSOPK: CR Integraci�n de WF a Version*/
/* Patch 425 # Seq: 128 */
IF NOT EXISTS( SELECT NAME FROM SYSOBJECTS WHERE NAME='WVALUSOPK' AND XTYPE='PK' )
	ALTER TABLE WVALUSO
		ADD CONSTRAINT WVALUSOPK PRIMARY KEY ( MODULO )
GO

/* KBOTON: CR Integraci�n de WF a Version*/
/* Patch 425 # Seq: 129 */
alter table KBOTON add KB_IMP_DIR Booleano;
GO

/* KBOTON: CR Integraci�n de WF a Version*/
/* Patch 425 # Seq: 130 */
update KBOTON SET KB_IMP_DIR = 'N' WHERE KB_IMP_DIR IS NULL;
GO

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$  PROCEDURES COMPARTE $$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* SP_CREA_NOTIFICACIONES : CR Integracion WF a Version*/
/* Patch 425 # Seq: 10 */
CREATE PROCEDURE DBO.SP_CREA_NOTIFICACIONES AS
BEGIN
  SET NOCOUNT ON;
        DECLARE @MODELO_ID VARCHAR(6);
        DECLARE @PASO_ORDEN INTEGER;
        DECLARE @TIPO_NOT INTEGER;
        DECLARE @USR_NOT INTEGER;

       /*PROCESO PARA CREAR NOTIFICACIONES DE INICIO POR MODELO*/

        DECLARE NOTIFICACIONES_INICIO_MODELO CURSOR FOR
        SELECT WM_CODIGO, WM_TIPNOT1, WM_USRNOT1
        FROM WMODELO
	WHERE WM_TIPNOT1 > 0  AND WM_CODIGO NOT IN (SELECT WM_CODIGO FROM WMODSTEP WHERE WE_TIPNOT1 > 0
        AND WE_ORDEN = 1 )

	OPEN NOTIFICACIONES_INICIO_MODELO

	FETCH NEXT FROM NOTIFICACIONES_INICIO_MODELO
	INTO @MODELO_ID, @TIPO_NOT, @USR_NOT

        WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO WMODNOTI (WM_CODIGO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_CUANDO, WY_QUE)
                            VALUES ( @MODELO_ID, 1, @TIPO_NOT, @USR_NOT, 0, 0)
		FETCH NEXT FROM NOTIFICACIONES_INICIO_MODELO
		INTO @MODELO_ID, @TIPO_NOT, @USR_NOT
        END

	CLOSE NOTIFICACIONES_INICIO_MODELO
        DEALLOCATE NOTIFICACIONES_INICIO_MODELO

        /*PROCESO PARA CREAR NOTIFICACIONES DE INICIO POR PASO*/
        DECLARE NOTIFICACIONES_INICIO_PASO CURSOR FOR
        SELECT WM_CODIGO, WE_ORDEN, WE_TIPNOT1, WE_USRNOT1
        FROM WMODSTEP
	WHERE WE_TIPNOT1 > 0


	OPEN NOTIFICACIONES_INICIO_PASO

	FETCH NEXT FROM NOTIFICACIONES_INICIO_PASO
	INTO @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT

	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO WMODNOTI (WM_CODIGO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_CUANDO, WY_QUE)
                            VALUES ( @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT, 0, 0)
		FETCH NEXT FROM NOTIFICACIONES_INICIO_PASO
		INTO @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT
        END

	CLOSE NOTIFICACIONES_INICIO_PASO
        DEALLOCATE NOTIFICACIONES_INICIO_PASO

        /*PROCESO PARA CREAR NOTIFICACIONES DE TERMINACI�N POR MODELO*/

	DECLARE NOTIFICACIONES_FIN_MODELO CURSOR FOR
        SELECT WM_CODIGO, WM_TIPNOT2, WM_USRNOT2
        FROM WMODELO WM
	WHERE WM_TIPNOT2 > 0  AND WM_CODIGO NOT IN (SELECT WM_CODIGO FROM WMODSTEP WE WHERE WE_TIPNOT2 > 0
        AND WE_ORDEN = (SELECT MAX(WE_ORDEN) FROM WMODSTEP WE2 WHERE WE2.WM_CODIGO = WM.WM_CODIGO) )

	OPEN NOTIFICACIONES_FIN_MODELO

	FETCH NEXT FROM NOTIFICACIONES_FIN_MODELO
	INTO @MODELO_ID, @TIPO_NOT, @USR_NOT

        WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO WMODNOTI (WM_CODIGO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_CUANDO, WY_QUE)
                SELECT @MODELO_ID, MAX(WE_ORDEN), @TIPO_NOT, @USR_NOT, 1, 0 FROM WMODSTEP WHERE
                WM_CODIGO = @MODELO_ID
		FETCH NEXT FROM NOTIFICACIONES_FIN_MODELO
		INTO @MODELO_ID, @TIPO_NOT, @USR_NOT
        END

	CLOSE NOTIFICACIONES_FIN_MODELO
        DEALLOCATE NOTIFICACIONES_FIN_MODELO

	/*PROCESO PARA CREAR NOTIFICACIONES DE TERMINACI�N POR PASO*/

        DECLARE NOTIFICACIONES_FIN_PASO CURSOR FOR
        SELECT WM_CODIGO, WE_ORDEN, WE_TIPNOT2, WE_USRNOT2 FROM WMODSTEP WHERE WE_TIPNOT2 > 0
	OPEN NOTIFICACIONES_FIN_PASO

	FETCH NEXT FROM NOTIFICACIONES_FIN_PASO
	INTO @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT

	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO WMODNOTI (WM_CODIGO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_CUANDO, WY_QUE)
                            VALUES ( @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT, 1, 0)
		FETCH NEXT FROM NOTIFICACIONES_FIN_PASO
		INTO @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT
        END

	CLOSE NOTIFICACIONES_FIN_PASO
        DEALLOCATE NOTIFICACIONES_FIN_PASO

	/*PROCESO PARA CREAR NOTIFICACIONES DE CANCELACI�N POR MODELO*/

	DECLARE NOTIFICACIONES_CANCELAR_MODELO CURSOR FOR
        SELECT WM_CODIGO, WM_TIPNOT3, WM_USRNOT3
        FROM WMODELO WM
	WHERE WM_TIPNOT3 > 0


	OPEN NOTIFICACIONES_CANCELAR_MODELO

	FETCH NEXT FROM NOTIFICACIONES_CANCELAR_MODELO
	INTO @MODELO_ID, @TIPO_NOT, @USR_NOT

        WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO WMODNOTI (WM_CODIGO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_CUANDO, WY_QUE)
                SELECT @MODELO_ID, WE_ORDEN, @TIPO_NOT, @USR_NOT, 2, 0 FROM WMODSTEP WHERE
                WM_CODIGO = @MODELO_ID AND WE_TIPNOT3 = 0
		FETCH NEXT FROM NOTIFICACIONES_CANCELAR_MODELO
		INTO @MODELO_ID, @TIPO_NOT, @USR_NOT
        END

	CLOSE NOTIFICACIONES_CANCELAR_MODELO
        DEALLOCATE NOTIFICACIONES_CANCELAR_MODELO


	/*PROCESO PARA CREAR NOTIFICACIONES DE CANCELACI�N POR PASO*/

        DECLARE NOTIFICACIONES_CANCELAR_PASO CURSOR FOR
        SELECT WM_CODIGO, WE_ORDEN, WE_TIPNOT3, WE_USRNOT3 FROM WMODSTEP WHERE WE_TIPNOT3 > 0
	OPEN NOTIFICACIONES_CANCELAR_PASO

	FETCH NEXT FROM NOTIFICACIONES_CANCELAR_PASO
	INTO @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT

	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO WMODNOTI (WM_CODIGO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_CUANDO, WY_QUE)
                            VALUES ( @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT, 2, 0)
		FETCH NEXT FROM NOTIFICACIONES_CANCELAR_PASO
		INTO @MODELO_ID, @PASO_ORDEN, @TIPO_NOT, @USR_NOT
        END
        CLOSE NOTIFICACIONES_CANCELAR_PASO
        DEALLOCATE NOTIFICACIONES_CANCELAR_PASO
END
GO

/* INIT_PROCESS_LOG : CR Integracion WF a Version*/
/* Patch 425 # Seq: 11 */
CREATE PROCEDURE DBO.INIT_PROCESS_LOG(
  @PROCESO INTEGER,
  @USUARIO SMALLINT,
  @FECHA   DATETIME,
  @HORA    VARCHAR(8),
  @MAXIMO  INTEGER,
  @PARAMS   VARCHAR(255),
  @FOLIO   INTEGER OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;
  SELECT @FOLIO = MAX( PC_NUMERO ) FROM PROCESO

  IF ( @FOLIO IS NULL )
    SELECT @FOLIO = 1
  ELSE
    SELECT @FOLIO = @FOLIO + 1

  INSERT INTO PROCESO ( PC_PROC_ID,
                        PC_NUMERO,
                        US_CODIGO,
                        PC_FEC_INI,
                        PC_HOR_INI,
                        PC_MAXIMO,
                        PC_ERROR,
			PC_PARAMS ) VALUES
                         ( @PROCESO,
                           @FOLIO,
                           @USUARIO,
                           @FECHA,
                           @HORA,
                           @MAXIMO,
                           'A',
			   @PARAMS )
END
GO

/* UPDATE_PROCESS_LOG : CR Integracion WF a Version*/
/* Patch 425 # Seq: 12 */
CREATE PROCEDURE DBO.UPDATE_PROCESS_LOG(
  @PROCESO  INTEGER,
  @EMPLEADO INTEGER,
  @PASO     INTEGER,
  @FECHA    DATETIME,
  @HORA     VARCHAR( 8 ),
  @STATUS   SMALLINT OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @BANDERA CHAR( 1 )

  UPDATE PROCESO SET
         CB_CODIGO = @EMPLEADO,
         PC_PASO = @PASO,
         PC_FEC_FIN = @FECHA,
         PC_HOR_FIN = @HORA
  WHERE ( PC_NUMERO = @PROCESO )

  SELECT @BANDERA = PC_ERROR
  FROM   PROCESO
  WHERE ( PC_NUMERO = @PROCESO )

  IF ( @BANDERA = 'A' )
    SELECT @STATUS = 0
  ELSE
    SELECT @STATUS = 1
END
GO

/* SP_REFRESH_DICCION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 13 */
CREATE PROCEDURE DBO.SP_REFRESH_DICCION
AS
BEGIN
  SET NOCOUNT ON;
END
GO

/* RECOMPILA_TODO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 14 */
CREATE PROCEDURE DBO.RECOMPILA_TODO
AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @TABLA SYSNAME
     DECLARE TEMPORAL CURSOR FOR
     SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES
     ORDER BY TABLE_NAME
     OPEN TEMPORAL
     FETCH NEXT FROM TEMPORAL INTO @TABLA
     WHILE ( @@FETCH_STATUS = 0 )
     BEGIN
          EXEC SP_RECOMPILE @TABLA
          FETCH NEXT FROM TEMPORAL INTO @TABLA
     END
     CLOSE TEMPORAL
     DEALLOCATE TEMPORAL
END
GO

/* SP_ACTIVA_WORKFLOW : CR Integracion WF a Version*/
/* Patch 425 # Seq: 15 */
CREATE PROCEDURE DBO.SP_ACTIVA_WORKFLOW(
  @FOLIO   FOLIOGRANDE,
  @USUARIO USUARIO,
  @DESTINO USUARIO,
  @NOTAS   NOTAS )
AS
BEGIN
     SET NOCOUNT ON;
     INSERT INTO WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO )
     SELECT NEWID(), WP_FOLIO, WE_ORDEN, NULL, @USUARIO, @DESTINO, GETDATE(), @NOTAS, 'Activar', 'N'
     FROM WPROSTEP WHERE ( WP_FOLIO = @FOLIO ) AND ( WE_ORDEN = 1 );
END
GO

/* SP_ACTUALIZA_TAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 16 */
CREATE PROCEDURE DBO.SP_ACTUALIZA_TAREA(
  @GUID   GUID,
  @STATUS STATUS,
  @AVANCE FOLIOCHICO,
  @FECHA  FECHA )
AS
BEGIN
     SET NOCOUNT ON;
     UPDATE WTAREA SET WT_STATUS  = @STATUS,
                       WT_AVANCE  = @AVANCE,
                       WT_FEC_FIN = @FECHA
     WHERE ( WT_GUID = @GUID );
END
GO

/* SP_BORRA_WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 17 */
CREATE PROCEDURE DBO.SP_BORRA_WMODSTEP( @WM_CODIGO CODIGO, @ACTUAL FOLIOCHICO )
AS
BEGIN
     SET NOCOUNT ON;
     DELETE FROM WMODSTEP WHERE ( WM_CODIGO = @WM_CODIGO ) AND ( WE_ORDEN = @ACTUAL );
     IF ( @@ROWCOUNT > 0 )
     BEGIN
          UPDATE WMODSTEP SET WE_ORDEN = WE_ORDEN - 1
          WHERE ( WM_CODIGO = @WM_CODIGO ) AND ( WE_ORDEN > @ACTUAL )
     END
END
GO

/* SP_CAMBIA_WMODSTEP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 18 */
CREATE PROCEDURE DBO.SP_CAMBIA_WMODSTEP( @WM_CODIGO CODIGO, @ACTUAL FOLIOCHICO, @NUEVO FOLIOCHICO )
AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @MAXIMO FOLIOCHICO

     IF ( @NUEVO < 1 ) OR ( @ACTUAL < 1 ) OR ( @ACTUAL = @NUEVO )
             RETURN

     SELECT @MAXIMO = MAX( WE_ORDEN )
     FROM   WMODSTEP
     WHERE  WM_CODIGO = @WM_CODIGO

     IF ( @NUEVO > @MAXIMO ) OR ( @ACTUAL > @MAXIMO )
             RETURN

     UPDATE WMODSTEP
     SET    WE_ORDEN = 0
     WHERE  WM_CODIGO = @WM_CODIGO AND WE_ORDEN = @ACTUAL

     UPDATE WMODSTEP
     SET    WE_ORDEN = @ACTUAL
     WHERE  WM_CODIGO = @WM_CODIGO AND WE_ORDEN = @NUEVO

     UPDATE WMODSTEP
     SET    WE_ORDEN = @NUEVO
     WHERE  WM_CODIGO = @WM_CODIGO AND WE_ORDEN = 0
END
GO

/* SP_CREA_WORKFLOW : CR Integracion WF a Version*/
/* Patch 425 # Seq: 19 */
CREATE PROCEDURE DBO.SP_CREA_WORKFLOW(
  @MODELO    CODIGO,
  @USUARIO   USUARIO,
  @DESTINO   USUARIO,
  @DATA      XMLDATA,
  @NOTAS     NOTAS,
  @FOLIO     FOLIOGRANDE OUTPUT )
AS
BEGIN
  SET NOCOUNT ON;

  DECLARE @AF_ID   CHAR(6)
  DECLARE @FECHA   DATETIME
  SET     @FECHA = GETDATE()

  DECLARE @PASOS  FOLIOCHICO
  DECLARE @NOMBRE OBSERVACIONES

  -- OBTIENE NOMBRE Y # DE PASOS
  SELECT  @PASOS = WM_PASOS, @NOMBRE = WM_NOMBRE
  FROM    WMODELO
  WHERE   WM_CODIGO = @MODELO

  -- OBTIENE FOLIO DE SIGUIENTE PROCESO
  SELECT @FOLIO = MAX( WP_FOLIO )+1
  FROM   WPROCESO
  IF (@FOLIO IS NULL) SELECT @FOLIO = 1

  -- CREA REGISTRO PROCESO, PADRE DE WORKFLOW
  INSERT INTO WPROCESO ( WP_FOLIO, WM_CODIGO, WP_NOMBRE, WP_USR_INI, WP_FEC_INI, WP_FEC_FIN, WP_STATUS, WP_PASOS, WP_PASO, WP_XMLDATA, WP_NOTAS )
  VALUES ( @FOLIO, @MODELO, @NOMBRE, @USUARIO, @FECHA, @FECHA, 0, @PASOS, 0, @DATA, @NOTAS )

  -- PENDIENTE VALIDAR QUE SE AGREG� UN REGISTRO

  -- INSERTA UN REGISTRO DE INSTANCIA POR CADA PASO
  INSERT INTO WPROSTEP ( WP_FOLIO, WE_ORDEN, WA_CODIGO, WM_CODIGO, WS_NOMBRE, WS_STATUS )
  SELECT @FOLIO, WE_ORDEN, WA_CODIGO, WM_CODIGO, WE_NOMBRE, 0
  FROM   WMODSTEP
  WHERE  WM_CODIGO = @MODELO

  -- ASIGNA EL VALOR DE NEXT PARA TODOS LOS PASOS EXCEPTO EL �LTIMO QUE QUEDA EN CERO
  UPDATE WPROSTEP
  SET    WS_NEXT = WE_ORDEN + 1
  WHERE  WP_FOLIO = @FOLIO AND WE_ORDEN < @PASOS

  -- ENVIA EL MENSAJE DE ACTIVACION
  EXEC SP_ACTIVA_WORKFLOW @FOLIO, @USUARIO, @DESTINO, @NOTAS
  RETURN
END
GO

/* SP_ENVIA_OUTBOX : CR Integracion WF a Version*/
/* Patch 425 # Seq: 20 */
CREATE PROCEDURE DBO.SP_ENVIA_OUTBOX(
  @TOADDRESS   MEMO,
  @CCADDRESS   MEMO,
  @SUBJECT     ACCESOS,
  @BODY        MEMO,
  @FROMNAME    ACCESOS,
  @FROMADDRESS ACCESOS,
  @SUBTYPE     STATUS,
  @GUIDTAREA   UNIQUEIDENTIFIER,
  @IDPROCESO   FOLIOGRANDE,
  @STATUS      STATUS,
  @AVANCE      FOLIOCHICO,
  @ENVIA       USUARIO,
  @RECIBE      USUARIO )
AS
BEGIN
  SET NOCOUNT ON;
  INSERT INTO WOUTBOX( WO_GUID, WT_GUID, WP_FOLIO, WO_TO, WO_CC, WO_SUBJECT, WO_BODY, WO_FROM_NA, WO_FROM_AD, WO_SUBTYPE, WO_ENVIADO, WO_FEC_IN, WO_TSTATUS, WO_TAVANCE, US_ENVIA, US_RECIBE )
  VALUES ( NEWID(), @GUIDTAREA, @IDPROCESO, @TOADDRESS, @CCADDRESS, @SUBJECT, @BODY, @FROMNAME, @FROMADDRESS, @SUBTYPE, 'N', GETDATE(), @STATUS, @AVANCE, @ENVIA, @RECIBE )
END
GO

/* SP_ENVIA_RESPUESTA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 21 */
CREATE PROCEDURE DBO.SP_ENVIA_RESPUESTA(
  @TAREA   GUID,
  @DESTINO USUARIO,
  @ACCION  DESCRIPCION,
  @NOTAS   NOTAS )
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO )
  SELECT NEWID(), WP_FOLIO, WE_ORDEN, WT_GUID, WT_USR_ORI, @DESTINO, GETDATE(), @NOTAS, @ACCION, 'N' FROM WTAREA WHERE ( WT_GUID = @TAREA );

END
GO

/* SP_DELEGA_TAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 22 */
CREATE PROCEDURE DBO.SP_DELEGA_TAREA(
  @TAREA   GUID,
  @DESTINO USUARIO,
  @NOTAS   NOTAS )
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO )
  SELECT NEWID(), WP_FOLIO, WE_ORDEN, WT_GUID, WT_USR_ORI, @DESTINO, GETDATE(), @NOTAS, 'Delegar', 'N' FROM WTAREA WHERE ( WT_GUID = @TAREA );

END
GO

/* GETNOMBREUSUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 23 */
CREATE FUNCTION DBO.GETNOMBREUSUARIO( @USUARIO USUARIO ) RETURNS DESCRIPCION
AS
BEGIN
  DECLARE @NOMBRE DESCRIPCION;
  SELECT @NOMBRE = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIO );
  RETURN @NOMBRE;
END

/* SP_PROCESO_CAMBIA_STATUS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 24 */
CREATE PROCEDURE DBO.SP_PROCESO_CAMBIA_STATUS(
  @IDPROCESO   FOLIOGRANDE,
  @MENSAJE     DESCRIPCION,
  @NOTAS       NOTAS )
AS
BEGIN
  SET NOCOUNT ON;

  INSERT INTO WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO )
  SELECT NEWID(), WP_FOLIO, WE_ORDEN, WT_GUID, WT_USR_ORI, WT_USR_ORI, GETDATE(), @NOTAS, @MENSAJE, 'N' FROM V_PROCESO WHERE ( WP_FOLIO = @IDPROCESO ) ;

END
GO

/* SP_PROCESO_SUSPENDER : CR Integracion WF a Version*/
/* Patch 425 # Seq: 25 */
CREATE PROCEDURE DBO.SP_PROCESO_SUSPENDER( @IDPROCESO FOLIOGRANDE, @NOTAS NOTAS )
AS
BEGIN
  SET NOCOUNT ON;
  EXEC DBO.SP_PROCESO_CAMBIA_STATUS @IDPROCESO, 'Suspender', @NOTAS
END
GO

/* SP_PROCESO_REINICIAR : CR Integracion WF a Version*/
/* Patch 425 # Seq: 26 */
CREATE PROCEDURE DBO.SP_PROCESO_REINICIAR( @IDPROCESO FOLIOGRANDE, @NOTAS NOTAS )
AS
BEGIN
  SET NOCOUNT ON;
  EXEC DBO.SP_PROCESO_CAMBIA_STATUS @IDPROCESO, 'Reiniciar', @NOTAS
END
GO

/* SP_PROCESO_CANCELAR : CR Integracion WF a Version*/
/* Patch 425 # Seq: 27 */
CREATE PROCEDURE DBO.SP_PROCESO_CANCELAR( @IDPROCESO FOLIOGRANDE, @NOTAS NOTAS )
AS
BEGIN
  SET NOCOUNT ON;
  EXEC DBO.SP_PROCESO_CAMBIA_STATUS @IDPROCESO, 'Cancelar', @NOTAS
END
GO

/* SP_ENVIA_NOTIFICACION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 28 */
CREATE PROCEDURE DBO.SP_ENVIA_NOTIFICACION(
  @WT_GUID  GUID,
  @WY_FOLIO FOLIOGRANDE,
  @WN_FECHA FECHA,
  @WN_VECES FOLIOCHICO )
AS
BEGIN
  SET NOCOUNT ON;
  INSERT INTO WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO, WY_FOLIO, WN_VECES )
  SELECT NEWID(), WP_FOLIO, WE_ORDEN, WT_GUID, WT_USR_ORI, WT_USR_ORI, @WN_FECHA, '', 'Notificar', 'N', @WY_FOLIO, @WN_VECES FROM WTAREA WHERE
  ( WT_GUID = @WT_GUID )
END
GO

/* SP_COPIAR_MODELO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 29 */
CREATE PROCEDURE DBO.SP_COPIAR_MODELO (@CODIGOORIGEN CODIGO, @CODIGODESTINO CODIGO, @NOMBRE OBSERVACIONES)
AS
BEGIN
 SET NOCOUNT ON;
        INSERT INTO WMODELO (WM_CODIGO, WM_NOMBRE, WM_DESCRIP, WM_URL, WM_URL_EXA, WM_URL_OK, WM_STATUS, WM_URL_DEL, WM_FORMATO)
        SELECT @CODIGODESTINO, @NOMBRE, WM_DESCRIP, WM_URL, WM_URL_EXA, WM_URL_OK, WM_STATUS, WM_URL_DEL, WM_FORMATO FROM WMODELO
        WHERE WM_CODIGO = @CODIGOORIGEN;
        INSERT INTO WMODSTEP (WM_CODIGO, WE_ORDEN, WA_CODIGO, WE_NOMBRE, WE_DESCRIP, WE_TIPDEST, WE_USRDEST, WE_LIMITE, WE_URL_OK)
        SELECT @CODIGODESTINO, WE_ORDEN, WA_CODIGO, WE_NOMBRE, WE_DESCRIP, WE_TIPDEST, WE_USRDEST, WE_LIMITE, WE_URL_OK FROM WMODSTEP
        WHERE WM_CODIGO = @CODIGOORIGEN;
        INSERT INTO WMODNOTI (WM_CODIGO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_XSL, WY_CUANDO, WY_TIEMPO, WY_QUE, WY_MODELO, WY_TEXTO,
        WY_VECES, WY_CADA)
        SELECT @CODIGODESTINO, WE_ORDEN, WY_QUIEN, WY_USR_NOT, WY_XSL, WY_CUANDO, WY_TIEMPO, WY_QUE, WY_MODELO, WY_TEXTO, WY_VECES,
        WY_CADA FROM WMODNOTI WHERE WM_CODIGO = @CODIGOORIGEN;
END
GO

/* SP_ENVIA_AUTORIZACION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 30 */
CREATE PROCEDURE DBO.SP_ENVIA_AUTORIZACION(
  @WT_GUID  GUID,
  @WY_FOLIO FOLIOGRANDE,
  @WN_FECHA FECHA,
  @WN_VECES FOLIOCHICO )
AS
BEGIN
  SET NOCOUNT ON;
  INSERT INTO WMENSAJE( WN_GUID, WP_FOLIO, WE_ORDEN, WT_GUID, WN_USR_INI, WN_USR_FIN, WN_FECHA, WN_NOTAS, WN_MENSAJE, WN_LEIDO,
  WY_FOLIO, WN_VECES )
  SELECT NEWID(), WP_FOLIO, WE_ORDEN, WT_GUID, WT_USR_ORI, WT_USR_ORI, @WN_FECHA, '', 'Autorizar', 'N', @WY_FOLIO, @WN_VECES FROM
  WTAREA WHERE ( WT_GUID = @WT_GUID );
END
GO

/* SP_AVANZA_TAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 31 */
CREATE PROCEDURE DBO.SP_AVANZA_TAREA(
  @GUID   GUID,
  @STATUS STATUS,
  @AVANCE FOLIOCHICO,
  @FECHA  FECHA )
AS
BEGIN
     SET NOCOUNT ON;
     UPDATE WTAREA SET WT_STATUS  = @STATUS,
                       WT_AVANCE  = @AVANCE,
                       WT_FEC_FIN = @FECHA
     WHERE ( WT_GUID = @GUID  AND  WT_STATUS < @STATUS);
END
GO

/* GETDURACION : CR Integracion WF a Version*/
/* Patch 425 # Seq: 32 */
CREATE FUNCTION DBO.GETDURACION( @FECHAINI FECHA, @FECHAFIN FECHA ) RETURNS DESCRIPCION
AS
BEGIN
     DECLARE @DURACION DESCRIPCION
     DECLARE @DIFERENCIA INTEGER

     SET @DIFERENCIA = DATEDIFF( DD, @FECHAINI, @FECHAFIN )
     IF ( @DIFERENCIA > 1 )
     BEGIN
          IF ( @DIFERENCIA >= 60 )
             SET @DURACION = STR( @DIFERENCIA / 30 ) + ' Meses'
          ELSE
              SET @DURACION = STR( @DIFERENCIA ) + ' D�as'
     END
     ELSE
     BEGIN
          SET @DIFERENCIA = DATEDIFF( HH, @FECHAINI, @FECHAFIN )
          IF ( @DIFERENCIA > 1 )
             SET @DURACION = STR( @DIFERENCIA ) + ' Horas'
          ELSE
          BEGIN
               SET @DIFERENCIA = DATEDIFF( MI, @FECHAINI, @FECHAFIN )
               IF ( @DIFERENCIA > 1 )
                  SET @DURACION = STR( @DIFERENCIA ) +  ' Minutos'
               ELSE
                   IF ( @DIFERENCIA = 1 )
                      SET @DURACION = '1 Minuto'
                   ELSE
                       SET @DURACION = ''
          END
     END
     RETURN LTRIM( @DURACION )
END
GO

/* GETSTATUSPROCESO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 33*/
CREATE FUNCTION DBO.GETSTATUSPROCESO( @STATUS STATUS ) RETURNS DESCRIPCION AS
BEGIN
     DECLARE @DESCSTATUS DESCRIPCION
     SET @DESCSTATUS = CASE @STATUS
                            WHEN 0 THEN 'Inactivo'
                            WHEN 1 THEN 'Activo'
                            WHEN 2 THEN 'Termin�'
                            WHEN 3 THEN 'Cancelado'
                            WHEN 4 THEN 'Suspendido'
                            ELSE ''
                       END;
     RETURN @DESCSTATUS;
END
GO

/* GETSTATUSTAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 34 */
CREATE FUNCTION DBO.GETSTATUSTAREA( @STATUS STATUS ) RETURNS DESCRIPCION AS
BEGIN
     DECLARE @DESCSTATUS DESCRIPCION
     SET @DESCSTATUS = CASE @STATUS
                             WHEN 0 THEN 'Inactiva'
                             WHEN 1 THEN 'Activa'
                             WHEN 2 THEN 'Esperando'
                             WHEN 3 THEN 'Termin�'
                             WHEN 4 THEN 'Cancelada'
                             ELSE ''
                       END
     RETURN @DESCSTATUS
END
GO

/* GETUSUARIOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 35 */
CREATE FUNCTION DBO.GETUSUARIOS( @USUARIO USUARIO, @TIPOLISTA STATUS, @DETALLE   OBSERVACIONES, @TAREA GUID)
RETURNS @RETUSUARIOS TABLE ( US_CODIGO SMALLINT PRIMARY KEY, US_NOMBRE VARCHAR(50) NOT NULL )
AS
BEGIN
   DECLARE @JEFE USUARIO
   /* TIPOLISTA: 1=ROL, 2=USUARIO ESPEC�FICO, 3=JEFE, 4=JEFE DEL JEFE, 5=SUBORDINADOS DIRECTOS, 6=NIVEL, 7=SOLICITANTE, 8=PARTICIPANTES, 9=RESPONSABLE */
   IF ( @TIPOLISTA = 1 )
     INSERT @RETUSUARIOS
     SELECT USUARIO.US_CODIGO, US_NOMBRE
     FROM   USUARIO, USER_ROL
     WHERE  USUARIO.US_CODIGO = USER_ROL.US_CODIGO AND
            USER_ROL.RO_CODIGO = @DETALLE

   ELSE IF ( @TIPOLISTA = 2 )
     INSERT @RETUSUARIOS
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_CODIGO = @DETALLE

   ELSE IF ( @TIPOLISTA = 3 OR @TIPOLISTA = 4 )
   BEGIN
     SELECT @JEFE = US_JEFE
     FROM   USUARIO
     WHERE  US_CODIGO = @USUARIO

     IF ( @TIPOLISTA = 4 )
       SELECT @JEFE = US_JEFE
       FROM   USUARIO
       WHERE  US_CODIGO = @JEFE

     /* SI NO TIENE JEFE, REGRESA EL MISMO
     IF ( @JEFE = 0 )
       SET @JEFE = @USUARIO */

     INSERT @RETUSUARIOS
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_CODIGO = @JEFE
   END
   ELSE IF ( @TIPOLISTA = 5 )
     INSERT @RETUSUARIOS
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_JEFE = @USUARIO

   ELSE IF ( @TIPOLISTA = 6 )
     INSERT @RETUSUARIOS
     SELECT DISTINCT USUARIO.US_CODIGO, US_NOMBRE
     FROM   USUARIO, USER_ROL, ROL
     WHERE  USUARIO.US_CODIGO = USER_ROL.US_CODIGO AND
            USER_ROL.RO_CODIGO = ROL.RO_CODIGO AND
            ROL.RO_NIVEL = @DETALLE

   ELSE IF ( @TIPOLISTA = 7 )
     INSERT @RETUSUARIOS
     SELECT US_CODIGO, US_NOMBRE
     FROM   USUARIO
     WHERE  US_CODIGO = @USUARIO

   ELSE IF ( @TIPOLISTA = 8 )
     INSERT @RETUSUARIOS
     SELECT DISTINCT U.US_CODIGO, U.US_NOMBRE
     FROM   WTAREA T
     LEFT OUTER JOIN USUARIO U ON T.WT_USR_DES = U.US_CODIGO
     WHERE  T.WP_FOLIO = (SELECT WP_FOLIO FROM WTAREA WHERE WT_GUID = @TAREA)

   ELSE IF (@TIPOLISTA = 9 )
     INSERT @RETUSUARIOS
     SELECT T.WT_USR_DES, U.US_NOMBRE FROM WTAREA T
     LEFT OUTER JOIN USUARIO U ON U.US_CODIGO = T.WT_USR_DES
     WHERE WT_GUID = @TAREA
   RETURN
END
GO

/* GETUSUARIOSINICIALES : CR Integracion WF a Version*/
/* Patch 425 # Seq:36 */
CREATE FUNCTION DBO.GETUSUARIOSINICIALES( @MODELO CODIGO, @USUARIO USUARIO )
RETURNS @RETUSUARIOS TABLE ( US_CODIGO SMALLINT PRIMARY KEY, US_NOMBRE  VARCHAR(50) NOT NULL)
AS
BEGIN
   DECLARE @TIPOLISTA STATUS
   DECLARE @DETALLE   OBSERVACIONES

   SELECT  @TIPOLISTA = WE_TIPDEST, @DETALLE = WE_USRDEST
   FROM    WMODSTEP
   WHERE   WM_CODIGO = @MODELO AND WE_ORDEN = 1

   INSERT @RETUSUARIOS
   SELECT US_CODIGO, US_NOMBRE
   FROM   DBO.GETUSUARIOS( @USUARIO, @TIPOLISTA, @DETALLE, NULL )

   RETURN
END
GO

/* GETUSUARIOSTAREA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 37 */
CREATE FUNCTION DBO.GETUSUARIOSTAREA( @TAREA GUID, @USUARIO USUARIO, @OFFSET SMALLINT )
RETURNS @RETUSUARIOSTAREA TABLE ( US_CODIGO SMALLINT PRIMARY KEY, US_NOMBRE VARCHAR(50) NOT NULL)
AS
BEGIN
   DECLARE @USUARIOORIGINAL USUARIO
   DECLARE @TIPOLISTA       STATUS
   DECLARE @DETALLE         OBSERVACIONES

   SELECT @TIPOLISTA = WE_TIPDEST,
          @DETALLE   = WE_USRDEST,
          @USUARIOORIGINAL = WP_USR_INI
   FROM   WTAREA, WPROSTEP, WPROCESO, WMODSTEP
   WHERE  ( WTAREA.WT_GUID = @TAREA ) AND
          ( WTAREA.WP_FOLIO = WPROSTEP.WP_FOLIO ) AND ( WTAREA.WE_ORDEN = WPROSTEP.WE_ORDEN ) AND
          ( WTAREA.WP_FOLIO = WPROCESO.WP_FOLIO ) AND
          ( WMODSTEP.WM_CODIGO = WPROSTEP.WM_CODIGO ) AND ( WMODSTEP.WE_ORDEN = WPROSTEP.WE_ORDEN + @OFFSET )

   INSERT @RETUSUARIOSTAREA SELECT US_CODIGO, US_NOMBRE FROM DBO.GETUSUARIOS( @USUARIOORIGINAL, @TIPOLISTA, @DETALLE, @TAREA )

   RETURN
END
GO

/* GETUSUARIOSSIGUIENTES : CR Integracion WF a Version*/
/* Patch 425 # Seq: 38 */
CREATE FUNCTION DBO.GETUSUARIOSSIGUIENTES( @TAREA GUID, @USUARIO USUARIO )
RETURNS @RETUSUARIOSSIG TABLE ( US_CODIGO SMALLINT PRIMARY KEY, US_NOMBRE VARCHAR(50) NOT NULL)
AS
BEGIN
     INSERT @RETUSUARIOSSIG SELECT US_CODIGO, US_NOMBRE FROM DBO.GETUSUARIOSTAREA( @TAREA, @USUARIO, 1 )
     RETURN
END
GO

/* GETUSUARIOSDELEGA : CR Integracion WF a Version*/
/* Patch 425 # Seq: 39 */
CREATE FUNCTION DBO.GETUSUARIOSDELEGA( @TAREA GUID, @USUARIO USUARIO )
RETURNS @RETUSUARIOSDELEGA TABLE ( US_CODIGO SMALLINT PRIMARY KEY, US_NOMBRE VARCHAR(50) NOT NULL)
AS
BEGIN
     INSERT @RETUSUARIOSDELEGA SELECT US_CODIGO, US_NOMBRE FROM DBO.GETUSUARIOSTAREA( @TAREA, @USUARIO, 0 )
     RETURN
END
GO

/* GETVACAPROPORCIONAL : CR Integracion WF a Version*/
/* Patch 425 # Seq: 40 */
CREATE FUNCTION DBO.GETVACAPROPORCIONAL
(
	@CB_CODIGO INT,
	@FECHAREGRESO DATETIME
)
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE
	@DANTIG DATETIME,
	@DULTIMOCIERRE DATETIME,
	@STABLA VARCHAR(30),
	@FECHAREF DATETIME,
	@DANIV DATETIME,
	@IANTIG INT,
	@RESULT FLOAT,
	@RANTIGULTIMO FLOAT,
	@RANTIGPROXIMO FLOAT,
	@LANIVULTIMO CHAR(1),
	@LANIVPROXIMO CHAR(1),
	@GLOBAL201 VARCHAR(8),
	@GLOBAL104 VARCHAR(8)

		--SE OBTIENE EL GLOBAL DE ASISTENCIA
		SELECT @GLOBAL201 = ISNULL(GL_FORMULA,'')
		FROM GLOBAL
		WHERE GL_CODIGO=201

		IF(ISNULL(@GLOBAL201,'')='N')
		BEGIN

			--SE OBTIENE EL GLOBAL DE LA TABLA
			SELECT @GLOBAL104 = GL_FORMULA
			FROM GLOBAL
			WHERE GL_CODIGO=104

			/* SE OBTIENE LA INFORMACI�N DEL EMPLEADO */
			SELECT  @DANTIG=C.CB_FEC_ING,
					@FECHAREF=C.CB_FEC_BAJ,
					@DULTIMOCIERRE=C.CB_DER_FEC
			FROM COLABORA C
			WHERE( C.CB_CODIGO = @CB_CODIGO )

			/* SE OBTIENE LA FECHA DE ANIVERSARIO DEL EMPLEADO */
			SELECT @LANIVPROXIMO = DBO.ESANIVERSARIO( @DANTIG , @FECHAREGRESO )
			SELECT @RANTIGPROXIMO = DBO.YEARSCOMPLETOS( @DANTIG , @FECHAREGRESO )

			IF(ISNULL(@GLOBAL104,'')<>'')
			BEGIN
				SELECT @RESULT = DBO.GETVACAPROP( @GLOBAL104, ABS(ROUND(@RANTIGPROXIMO,0)) )
				SELECT @RESULT = @RESULT * 0.01
			END
			ELSE
			BEGIN
				SELECT @RESULT = 0
			END
		END
		ELSE
		BEGIN
			SELECT @RESULT = 0
		END
	RETURN  @RESULT
END
GO

/* CACHEPRESTACIONESPROP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 41 */
CREATE FUNCTION DBO.CACHEPRESTACIONESPROP
(   @STABLAS VARCHAR(30) )
RETURNS @FCACHETABLA TABLE (
    ANIO NUMERIC(15,2),
    DIAS NUMERIC(15,2) )
AS
BEGIN
	DECLARE
	@K_MAX_YEARS INT,
	@I INT,
	@J INT,
	@DIASAUX FLOAT,
	@DIASANT FLOAT,
	@DIAS FLOAT

	SELECT @K_MAX_YEARS = 99, @I = 0 ,@DIAS =0, @DIASANT=0, @DIASAUX=0 , @J = 1

	/* SE LLENA LA TABLA CON LOS DIAS DISPONIBLES POR A�O PARA UN EMPLEADO*/
	WHILE( @I < @K_MAX_YEARS)
	BEGIN
		SELECT @DIASAUX = ISNULL(PT_PRIMAVA, 0 )
						FROM   PRESTACI
						WHERE  TB_CODIGO = @STABLAS
							AND PT_YEAR = @I
						ORDER BY PT_YEAR

		IF( @DIASAUX = 0 )
		BEGIN

			INSERT INTO @FCACHETABLA VALUES( @I , 0 )

		END
		ELSE
		BEGIN
			SELECT @DIAS = @DIASAUX

			INSERT INTO @FCACHETABLA VALUES( @I , @DIAS )
			--SELECT @DIASANT=@DIASAUX

			UPDATE @FCACHETABLA
			SET DIAS= @DIAS
			WHERE ANIO >0 AND ANIO<@I AND DIAS=0

		END

		SELECT @I= @I+1,@DIASAUX =0
	END

	/* SE ACTUALIZAN LOS REGISTROS QUE TIENEN 0 DIAS */
	UPDATE @FCACHETABLA
	SET DIAS= @DIAS
	WHERE ANIO > 0
	AND ANIO < @K_MAX_YEARS
	AND DIAS = 0

	RETURN
END
GO

/* GETVACAPROP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 42 */
CREATE FUNCTION DBO.GETVACAPROP
(   @STABLAS VARCHAR(30), @IYEAR INT )
RETURNS NUMERIC(15,2)
AS
BEGIN

	DECLARE
	@DIAS NUMERIC(15,2),
	@K_MAX_YEARS INT

	SELECT  @K_MAX_YEARS =99

	IF ( @IYEAR < 1 )
		SELECT @IYEAR = 1;
	IF ( @IYEAR > @K_MAX_YEARS )
		SELECT @IYEAR = @K_MAX_YEARS

	/* SE OBTIENE LA INFORMACI�N DE LA TABLA */
	SELECT @DIAS = DIAS FROM DBO.CACHEPRESTACIONESPROP(@STABLAS)WHERE ANIO=@IYEAR

	RETURN @DIAS
END
GO

/* YEARSCOMPLETOS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 43 */
CREATE FUNCTION DBO.YEARSCOMPLETOS
(   @DANTIG DATETIME,
	@DREFERENCIA DATETIME )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE
	@IDAYINI INT, @IMONTHINI INT, @IYEARINI INT,
	@IDAYFIN INT, @IMONTHFIN INT, @IYEARFIN INT,
	@RANTIGULTIMO NUMERIC(15,2)

	IF ( @DANTIG < @DREFERENCIA )
	BEGIN

		/* ESTA FUNCION DESMENUSA LA FECHA Y OBTIENE EL A�O, MES Y DIA*/
		SELECT 	@IYEARINI 	= YEAR(@DANTIG),
				@IMONTHINI 	= MONTH(@DANTIG),
				@IDAYINI 	= DAY(@DANTIG),
				@IYEARFIN 	= YEAR(@DREFERENCIA),
				@IMONTHFIN 	= MONTH(@DREFERENCIA),
				@IDAYFIN 	= DAY(@DREFERENCIA)


		IF ( @IMONTHINI = @IMONTHFIN ) AND ( @IDAYINI = @IDAYFIN )    -- (ES ANIVERSARIO)RESTA DE A�OS ENTEROS
			SELECT @RANTIGULTIMO= ABS(@IYEARFIN - @IYEARINI)
		ELSE  -- HAY UNA PARTE PROPORCIONAL //
			SELECT  @RANTIGULTIMO= (DATEDIFF(DAY, @DANTIG ,@DREFERENCIA ) / 365.25)
	END
	ELSE
	BEGIN
		SELECT  @RANTIGULTIMO = 0
	END
	RETURN @RANTIGULTIMO
END
GO

/* ESANIVERSARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 44 */
CREATE  FUNCTION DBO.ESANIVERSARIO
(   @DANTIG DATETIME, @DREFERENCIA DATETIME )
RETURNS CHAR(1)
AS
BEGIN
	DECLARE
	@IDAYINI INT, @IMONTHINI INT, @IYEARINI INT,
	@IDAYFIN INT, @IMONTHFIN INT, @IYEARFIN INT,
	@LANIVERSARIO INT, @ESANIV CHAR(1)

	SELECT @LANIVERSARIO= 0

	IF ( @DANTIG < @DREFERENCIA )
	BEGIN

		/* ESTA FUNCION DESMENUSA LA FECHA Y OBTIENE EL A�O, MES Y DIA*/
		SELECT 	@IYEARINI 	= YEAR(@DANTIG),
				@IMONTHINI 	= MONTH(@DANTIG),
				@IDAYINI 	= DAY(@DANTIG),
				@IYEARFIN 	= YEAR(@DREFERENCIA),
				@IMONTHFIN 	= MONTH(@DREFERENCIA),
				@IDAYFIN 	= DAY(@DREFERENCIA)

		-- SI COINCIDE CON EN EL ANIVERSARIO //
		IF( @IMONTHINI = @IMONTHFIN ) AND ( @IDAYINI = @IDAYFIN )
		BEGIN
			SELECT @ESANIV= 'S'
		END
		ELSE
		BEGIN
			SELECT @ESANIV= 'N'
		END

	END
	ELSE
	BEGIN
			SELECT @ESANIV= 'S'
	END
	RETURN @ESANIV
END
GO

/* CAMPOREP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 45*/
CREATE PROCEDURE DBO.SP_WFGETEMPRESAS( @US_CODIGO USUARIO, @CM_CONTROL REFERENCIA )
AS
BEGIN
     SET NOCOUNT ON;
	SELECT CM_CODIGO = LTRIM( RTRIM(C.CM_CODIGO) ), C.CM_NOMBRE, C.CM_ALIAS, C.CM_USRNAME, C.CM_PASSWRD, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO
		FROM COMPANY C, USUARIO U WHERE ( CM_CONTROL=@CM_CONTROL OR CM_CONTROL='' )
		AND ( ( U.US_CODIGO = @US_CODIGO ) AND ( ( U.GR_CODIGO = 1 )
			OR ( SELECT COUNT(*) FROM ACCESO A WHERE
				( A.CM_CODIGO = C.CM_CODIGO ) AND
                ( A.GR_CODIGO = U.GR_CODIGO ) AND
                ( A.AX_DERECHO > 0 )) > 0 ) )
                ORDER BY C.CM_CODIGO
END
GO

/* CAMPOREP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 47 */
CREATE PROCEDURE DBO.SP_WFGETEMPRESASTODAS( @CM_CONTROL REFERENCIA )
AS
BEGIN
     SET NOCOUNT ON;
	SELECT CM_CODIGO = LTRIM( RTRIM(C.CM_CODIGO) ), C.CM_NOMBRE, C.CM_ALIAS, C.CM_USRNAME, C.CM_PASSWRD, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO
		FROM COMPANY C WHERE ( CM_CONTROL = @CM_CONTROL ) ORDER BY C.CM_CODIGO
END
GO

/* CAMPOREP : CR Integracion WF a Version*/
/* Patch 425 # Seq: 47 */
CREATE PROCEDURE DBO.SP_WFGETEMPRESA(@CM_CODIGO CODIGOEMPRESA,  @CM_CONTROL REFERENCIA )
AS
BEGIN
     SET NOCOUNT ON;
	SELECT CM_CODIGO = LTRIM( RTRIM(C.CM_CODIGO) ), C.CM_NOMBRE, C.CM_ALIAS, C.CM_USRNAME, C.CM_PASSWRD, C.CM_NIVEL0, C.CM_DATOS, C.CM_DIGITO
	FROM COMPANY C
	WHERE (CM_CODIGO = @CM_CODIGO)
	AND ( ( CM_CONTROL = @CM_CONTROL ) OR ( CM_CONTROL = '' ) )
	ORDER BY C.CM_CODIGO
END
GO

/* PE_GET_OBSERVACIONES : CR Integracion WF a Version*/
/* Patch 425 # Seq: 48 */
CREATE FUNCTION PE_GET_OBSERVACIONES(	@FOLIO INTEGER )
RETURNS @WFOBSERVACIONES TABLE(	USR_NXT VARCHAR( 30 ),
								NOTA VARCHAR( 255 ),
								USR_INI VARCHAR( 30 ),
								FECHA VARCHAR(10),
								WN_MENSAJE VARCHAR(30),
								WE_ORDEN INT)
AS
BEGIN
	DECLARE @FECHA DATETIME;
	DECLARE	@USUARIOINICIAL INTEGER;
	DECLARE @USUARIOFINAL INTEGER;
	DECLARE @NOTAS VARCHAR(255);
	DECLARE	@NOMBREUSUARIOINICIAL VARCHAR(30);
	DECLARE @NOMBREUSUARIOFINAL VARCHAR(30);
	DECLARE	@WN_MENSAJE VARCHAR(30);
	DECLARE @WE_ORDEN INT

	DECLARE TOTALOBSERVACIONES CURSOR FOR
			SELECT WM.WN_USR_INI, WM.WN_USR_FIN, WM.WN_NOTAS, WM.WN_FECHA, WM.WN_MENSAJE, WM.WE_ORDEN
			FROM WMENSAJE WM, USUARIO U
			WHERE ( WM.WN_USR_INI = U.US_CODIGO )
			AND ( WM.WN_NOTAS IS NOT NULL )
			AND ( WM.WP_FOLIO = @FOLIO AND WM.WY_FOLIO = '0' )
			AND ( WM.WE_ORDEN IN ( SELECT WPS.WE_ORDEN
			FROM WPROSTEP WPS
			WHERE ( WPS.WP_FOLIO = WM.WP_FOLIO ) AND ( WPS.WA_CODIGO <> 'GRABA3' ) ) )
			ORDER BY WM.WN_FECHA

	OPEN TOTALOBSERVACIONES
	FETCH NEXT FROM TOTALOBSERVACIONES INTO @USUARIOINICIAL, @USUARIOFINAL, @NOTAS, @FECHA, @WN_MENSAJE, @WE_ORDEN

	WHILE @@FETCH_STATUS = 0
	BEGIN
			SELECT @NOMBREUSUARIOINICIAL = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIOINICIAL );

			SELECT @NOMBREUSUARIOFINAL = US_NOMBRE FROM USUARIO WHERE ( US_CODIGO = @USUARIOFINAL );

			INSERT INTO @WFOBSERVACIONES( USR_NXT, NOTA , USR_INI ,FECHA, WN_MENSAJE , WE_ORDEN)
			VALUES( @NOMBREUSUARIOFINAL, @NOTAS, @NOMBREUSUARIOINICIAL, CONVERT(VARCHAR,CONVERT(DATETIME,@FECHA,103),103), @WN_MENSAJE, @WE_ORDEN );

			FETCH NEXT FROM TOTALOBSERVACIONES INTO @USUARIOINICIAL, @USUARIOFINAL, @NOTAS, @FECHA, @WN_MENSAJE, @WE_ORDEN
	END

	CLOSE TOTALOBSERVACIONES
	DEALLOCATE TOTALOBSERVACIONES

	RETURN
END
GO

/* SP_GRABA_WVALUSO*/
/* Patch 425 # Seq: 49 */
CREATE PROCEDURE SP_GRABA_WVALUSO
(
	@MODULO NOMBRECAMPO,
	@USO DESCRIPCION
)
AS
BEGIN
     SET NOCOUNT ON;
	IF EXISTS( SELECT MODULO FROM WVALUSO WHERE MODULO = @MODULO )
		UPDATE WVALUSO SET USO = @USO WHERE MODULO = @MODULO
	ELSE
		INSERT INTO WVALUSO ( MODULO, USO ) VALUES ( @MODULO, @USO )
END
GO

/* PROCEDURE: JB PRO DEF # 1863 - Derechos sobre Tarjetas de Gasolina y Despensa est�n inver */
/* Patch 425 # Seq: 50 */
CREATE PROCEDURE SPP_CAMBIA_DERECHO_TARJETAS
AS
BEGIN
	SET NOCOUNT ON
	declare @DerechoOtros FolioChico
	declare @Empresa CodigoEmpresa
	declare @Grupo FolioChico
	declare @DerechosInfonavit FolioChico

	declare @Derechos integer

	declare CursorDerechoOtros CURSOR FOR
	select AX_DERECHO, CM_CODIGO, GR_CODIGO from ACCESO where ( AX_NUMERO = 7 )
	OPEN CursorDerechoOtros	FETCH NEXT FROM CursorDerechoOtros INTO @DerechoOtros, @Empresa, @Grupo

	WHILE @@FETCH_STATUS = 0
	BEGIN
		set @Derechos = ( select @DerechoOtros & 256 ) + ( select @DerechoOtros & 512 )

		if ( @Derechos = 256 ) or ( @Derechos = 512 )
		begin
			  update ACCESO
			  set AX_DERECHO = AX_DERECHO ^ 768
			  where CM_CODIGO = @Empresa and GR_CODIGO = @Grupo and AX_NUMERO = 7
		end

		FETCH NEXT FROM CursorDerechoOtros INTO @DerechoOtros, @Empresa, @Grupo
	END
	close CursorDerechoOtros
	deallocate CursorDerechoOtros
END
GO

/* **************************************************** */
/* cr 1984:Garantizar que las relaciones de COLABORA queden en el orden correcto. */

create procedure SP_ACTUALIZA_RELACIONES_COLABORA as
begin
	/* Entidad 59: Puestos */
	/* Entidad 62: Razon Social */
	/* En la lista de relacion de COLABORA, la razon social debe de estar en una posicion menor que puesto*/

	declare @iPosPuesto FolioChico
	declare @iPosPatron FolioChico

	select  @iPosPuesto = RL_ORDEN from R_RELACION
	where EN_CODIGO = 10 and RL_ENTIDAD = 59

	select  @iPosPatron = RL_ORDEN  from R_RELACION
	where EN_CODIGO = 10 and RL_ENTIDAD = 62

	if  ( @iPosPuesto < @iPosPatron )
	begin
			update R_RELACION set RL_ORDEN = 9999
			where EN_CODIGO = 10 and RL_ENTIDAD = 59

			update R_RELACION set RL_ORDEN = @iPosPuesto
			where EN_CODIGO = 10 and RL_ENTIDAD = 62

			update R_RELACION set RL_ORDEN = @iPosPatron
			where EN_CODIGO = 10 and RL_ENTIDAD = 59
	end
end
GO

execute SP_ACTUALIZA_RELACIONES_COLABORA
GO

drop procedure SP_ACTUALIZA_RELACIONES_COLABORA
GO

/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   TRIGGERS COMPARTE      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* TU_QUERYS : CR Integracion WF a Version*/
/* Patch 425 # Seq: 16 */
CREATE TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	DECLARE @NEWCODIGO CONDICION, @OLDCODIGO CONDICION;

	SELECT @NEWCODIGO = QU_CODIGO FROM INSERTED;
		SELECT @OLDCODIGO = QU_CODIGO FROM DELETED;

	IF ( @OLDCODIGO <> '' ) AND ( @NEWCODIGO <> '' )
	BEGIN
		 UPDATE REPORTE SET QU_CODIGO = @NEWCODIGO WHERE QU_CODIGO = @OLDCODIGO;
	END
  END
END
GO

/* TD_USUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 8 */
CREATE TRIGGER TD_USUARIO ON USUARIO AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
     declare @OldUS_CODIGO Usuario;
     select @OldUS_CODIGO = US_CODIGO from Deleted;
     delete from PAGINA where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
   	 UPDATE USUARIO SET US_JEFE = 0 WHERE ( US_JEFE = @OLDUS_CODIGO );
	   UPDATE USUARIO SET US_PAG_DEF = 0 WHERE ( US_PAG_DEF = @OLDUS_CODIGO );
	 update USUARIO set US_WF_ALT = 0, US_WF_OUT = 'N' where US_WF_ALT = @OldUS_CODIGO;
END
GO

/* TU_USUARIO : CR Integracion WF a Version*/
/* Patch 425 # Seq: 9 */
CREATE TRIGGER TU_USUARIO ON USUARIO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
	 declare @US_CODIGO USUARIO;
     IF UPDATE( US_CODIGO )
     BEGIN
	      declare @NewUS_CODIGO Usuario, @OldUS_CODIGO Usuario;
	      select @NewUS_CODIGO = US_CODIGO from Inserted;
        select @OldUS_CODIGO = US_CODIGO from Deleted;
        update PAGINA set PAGINA.US_CODIGO = @NewUS_CODIGO
        where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
		    update USUARIO set US_JEFE = @NEWUS_CODIGO
		    where ( US_JEFE = @OLDUS_CODIGO );
		    update USUARIO set US_PAG_DEF = @NEWUS_CODIGO
		    where ( US_PAG_DEF = @OLDUS_CODIGO );
     END
	 ELSE IF UPDATE( US_WF_OUT )
	 BEGIN
		declare @NEWUS_WF_OUT BOOLEANO;
		select @NEWUS_WF_OUT = US_WF_OUT, @US_CODIGO = US_CODIGO from INSERTED;
		IF( @NEWUS_WF_OUT = 'N' )
			update USUARIO set US_WF_ALT = 0 where US_CODIGO = @US_CODIGO;
	 END
	 ELSE IF UPDATE( US_ACTIVO )
	 BEGIN
		declare @NEWUS_ACTIVO BOOLEANO;
		select @US_CODIGO = US_CODIGO, @NEWUS_ACTIVO = US_ACTIVO from INSERTED;
		IF( @NEWUS_ACTIVO = 'N' )
			update USUARIO set US_WF_ALT = 0, US_WF_OUT = 'N' where US_CODIGO = @US_CODIGO OR US_WF_ALT = @US_CODIGO;
	 END;
END
GO

