/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Tabla COMPANY: ¿Configuración por Confidencialidad? ( PRO - Envio de Correos desde Kiosco )*/
/* Patch 415 # Seq: X Agregado */
alter table COMPANY add CM_KCONFI Booleano
GO

/* Tabla COMPANY: Lista de usuarios ( PRO - Envio de Correos Envio de Correos desde Kiosco)*/
/* Patch 415 # Seq: X Agregado */
alter table COMPANY add CM_KUSERS Descripcion
GO

/* COMPANY.CM_KCONFI: Inicialización de Configuración por confidencialidad ( PRO - Envio de Correos Envio de Correos desde Kiosco )*/
/* Patch 415 # Seq: X Agregado */
update COMPANY set CM_KCONFI = 'N' where ( CM_KCONFI is Null )
GO

/* COMPANY.CM_KUSERS : Inicialización de Usuarios ( PRO - Envio de Correos Envio de Correos desde Kiosco )*/
/* Patch 415 # Seq: X Agregado */
update COMPANY set CM_KUSERS = '' where ( CM_KUSERS is Null )
GO

/* Tabla KEM_CONFI: Correo por Confidencialidad */
/* Patch 415 # Seq: X Agregado */
CREATE TABLE KEM_CONFI(

	CM_CODIGO CodigoEmpresa, 
	TB_CODIGO Codigo,
	KEM_USERS	Descripcion
)
GO

/* Tabla KEM_CONFI: Llave Primaria  */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE KEM_CONFI
       ADD CONSTRAINT PK_KEM_CONFI PRIMARY KEY (CM_CODIGO, TB_CODIGO)
GO

/* Tabla KEM_CONFI: Llave Foránea a Company  */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE KEM_CONFI
       ADD CONSTRAINT FK_KEM_CONFI_COMPANY
       FOREIGN KEY (CM_CODIGO)
       REFERENCES COMPANY
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Insert para copiar los derechos de acceso de simulacion al nodo nuevo ( 1/3)*/
/* Patch 415 # Seq: X Agregado */
create procedure SIMULA_ACTUALIZA_DERECHOS
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 605 );
     if ( @Cuantos = 0 )
     begin
          INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
	SELECT GR_CODIGO, CM_CODIGO, 605, AX_DERECHO FROM ACCESO
	WHERE AX_NUMERO = 521
           /* Es el derecho nuevo, es el folder de \Empleados\Nomina\Simulacion de Finiquitos) */
     end
end
GO

/* Insert para copiar los derechos de acceso de simulacion al nodo nuevo ( 2/3)*/
/* Patch 415 # Seq: X Agregado */
execute SIMULA_ACTUALIZA_DERECHOS
GO

/* Insert para copiar los derechos de acceso de simulacion al nodo nuevo ( 3/3)*/
/* Patch 415 # Seq: X Agregado */
DROP PROCEDURE SIMULA_ACTUALIZA_DERECHOS
GO

/* DISPOSIT: Lista de Dispositivos*/
/* Patch 415 # Seq: X Agregado */
create table DISPOSIT
(
	DI_NOMBRE RelojLinx not null,
	DI_TIPO FolioChico not null,
	DI_DESCRIP Descripcion null,
	DI_NOTA Formula null,
	DI_IP ComputerName null
)
GO

/* DISPOSIT: Llave Primaria*/
/* Patch 415 # Seq: X Agregado */
alter table DISPOSIT
       add constraint PK_DISPOSIT
       primary key( DI_NOMBRE, DI_TIPO )
GO

/* DISXCOM: Dispositivos por empresa*/
/* Patch 415 # Seq: X Agregado */
create table DISXCOM
(
	CM_CODIGO CodigoEmpresa not null,
	DI_NOMBRE RelojLinx not null,
	DI_TIPO FolioChico not null
)
GO

/* DISXCOM: Dispositivos por empresa*/
/* Patch 415 # Seq: X Agregado */
alter table DISXCOM
   add constraint PK_DISXCOM
  primary key (CM_CODIGO, DI_NOMBRE, DI_TIPO)
GO

/* FK DISXCOM: Foreign Key hacia DISPOSIT */
/* Patch 415 # Seq: X Agregado */
alter table DISXCOM
      add constraint FK_DISPO_X_COMP
      foreign key( DI_NOMBRE, DI_TIPO )
      references DISPOSIT
      on delete CASCADE
      on update CASCADE
GO

/* PK PK_POLL: primary Key hacia POLL(1/2) */
/* Patch 415 # Seq: X Agregado */
alter table POLL 
	drop constraint PK_POLL
GO

/* PK PK_POLL: primary Key hacia POLL(2/2) */
/* Patch 415 # Seq: X Agregado */
alter table POLL
	add constraint PK_POLL PRIMARY KEY ( PO_EMPRESA, PO_NUMERO, PO_FECHA, PO_HORA, PO_LINX )
GO

/* FK KBOTON_ACC: Defecto 1205 Falta llave referencial a grupo  */
/* Patch 415 # Seq: X Agregado */
ALTER TABLE KBOTON_ACC
       ADD CONSTRAINT FK_KBOTON_ACC_GRUPO
       FOREIGN KEY (GR_CODIGO)
       REFERENCES GRUPO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

