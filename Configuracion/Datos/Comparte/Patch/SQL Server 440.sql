/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 440: Crear tabla DB_INFO 1/3 */
IF not exists (SELECT * FROM sysobjects where id = object_id(N'DB_INFO'))
begin
	CREATE TABLE  DB_INFO (
		   DB_CODIGO			CodigoEmpresa,
		   DB_DESCRIP     Observaciones,
		   DB_CONTROL			Referencia,
		   DB_TIPO        Status,
		   DB_DATOS       Accesos,
		   DB_USRNAME     UsuarioCorto,
		   DB_PASSWRD     Passwd,
		   DB_CTRL_RL     Referencia,
		   DB_CHKSUM      Accesos,
		   DB_ESPC        Booleano
	)
end
GO

/* Patch 440: Crear tabla DB_INFO 2/3 */
IF not exists (SELECT * FROM sysobjects where id = object_id(N'PK_DB_INFO'))
begin
	ALTER TABLE DB_INFO
		ADD CONSTRAINT PK_DB_INFO PRIMARY KEY (DB_CODIGO)
end
GO

/* Patch 440: Crear tabla DB_INFO 3/3 */
IF not exists (SELECT * FROM sysobjects where id = object_id(N'UK_DB_INFO'))
begin
	ALTER TABLE DB_INFO
		ADD CONSTRAINT UK_DB_INFO UNIQUE NONCLUSTERED (DB_DATOS)
end
GO

/* Patch 440: Agregar campo COMPANY.DB_CODIGO 1/3 */
if not exists (select * from syscolumns where id=object_id('COMPANY') and name='DB_CODIGO')
	alter table COMPANY add DB_CODIGO CodigoEmpresa null
GO

/* Patch 440: Agregar campo COMPANY.DB_CODIGO 2/3 */
IF EXISTS (select * from syscolumns where id=object_id('COMPANY') and name='DB_CODIGO')
	update COMPANY set  DB_CODIGO = '' where DB_CODIGO is null
GO

/* Patch 440: Agregar campo COMPANY.DB_CODIGO 3/3 */
IF EXISTS (select * from syscolumns where id=object_id('COMPANY') and name='DB_CODIGO')
	alter table COMPANY alter column DB_CODIGO CodigoEmpresa not null
GO
/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
/* A. Definir SP */
create procedure REGISTROS_DB_INFO
as
begin
	SET NOCOUNT ON;
	/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
	/* 1. Borrar objeto FK_DB_INFO_DB_CODIGO */
	IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[FK_DB_INFO_DB_CODIGO]') AND xtype in (N'F'))
		ALTER TABLE COMPANY DROP CONSTRAINT FK_DB_INFO_DB_CODIGO

	/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
	/* 2. Eliminar registros en DB_INFO */
	
	if (SELECT COUNT(DB_CODIGO) FROM DB_INFO) = 0 
	BEGIN
	
		/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
		/* 3. Programa para insertar registros en DB_INFO. Uno por BD (COMPANY.CM_DATOS) */	

		DECLARE @CM_DATOS ACCESOS;

		DECLARE DB_INFO_CURSOR CURSOR FOR
		SELECT DISTINCT CM_DATOS FROM COMPANY

		OPEN DB_INFO_CURSOR

		FETCH NEXT FROM DB_INFO_CURSOR INTO @CM_DATOS

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO DB_INFO (DB_CODIGO, DB_DESCRIP, DB_CONTROL, DB_TIPO, DB_DATOS, DB_USRNAME, DB_PASSWRD, DB_CTRL_RL, DB_CHKSUM)
			SELECT TOP 1 COMPANY.CM_CODIGO, COMPANY.CM_NOMBRE, COMPANY.CM_CONTROL,
			CASE
				WHEN CM_CTRL_RL = '3DATOS' THEN 0
				WHEN CM_CTRL_RL = '3RECLUTA' THEN 1
				WHEN CM_CTRL_RL = '3VISITA' THEN 2
				WHEN CM_CTRL_RL = '3OTRO' THEN 3
				WHEN CM_CTRL_RL = '3PRUEBA' THEN 4
				WHEN CM_CTRL_RL = '3PRESUP' THEN 5
				ELSE 0
			END, COMPANY.CM_DATOS, COMPANY.CM_USRNAME, COMPANY.CM_PASSWRD,
			COMPANY.CM_CTRL_RL, COMPANY.CM_CHKSUM FROM COMPANY
			WHERE CM_DATOS = @CM_DATOS
			ORDER BY CM_NIVEL0, CM_CODIGO;

		  FETCH NEXT FROM DB_INFO_CURSOR INTO @CM_DATOS
		END

		CLOSE DB_INFO_CURSOR;

		DEALLOCATE DB_INFO_CURSOR;

		/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
		/* 4. Actualizar tabla COMPANY con los C�digos que le corresponden de la tabla DB_INFO */
		UPDATE COMPANY SET DB_CODIGO =
		(SELECT DB_CODIGO FROM DB_INFO WHERE DB_DATOS = COMPANY.CM_DATOS);
	
	END

	/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
	/* 5. Crear llave for�nea FK_DB_INFO_DB_CODIGO */
	IF EXISTS (select * from syscolumns where id=object_id('COMPANY') and name='DB_CODIGO')
		ALTER TABLE COMPANY ADD CONSTRAINT FK_DB_INFO_DB_CODIGO FOREIGN KEY(DB_CODIGO) REFERENCES DB_INFO(DB_CODIGO)
			ON UPDATE CASCADE
end
GO

/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
/* B. Ejecutar SP */
execute REGISTROS_DB_INFO
GO

/* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
/* C. Eliminar SP */
DROP PROCEDURE REGISTROS_DB_INFO
GO

/* Patch 440: Agregar trigger DB_INFO -> COMPANY (1/2) */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[TR_DB_INFO]') AND xtype in (N'TR'))
DROP TRIGGER TR_DB_INFO
GO

/* Patch 440: Agregar trigger DB_INFO -> COMPANY (2/2) */
CREATE trigger TR_DB_INFO
on DB_INFO for update AS
begin
	set NOCOUNT on;

	declare @DB_CODIGO CodigoEmpresa;
	declare @DB_CONTROL Referencia;
	declare @DB_TIPO Status;
	declare @DB_DATOS Accesos;
	declare @DB_USRNAME UsuarioCorto;
	declare @DB_PASSWRD Passwd;
	declare @DB_CTRL_RL Referencia;
	declare @DB_CHKSUM Accesos;

	declare @OldCodigo CodigoEmpresa;
	declare @NewCodigo CodigoEmpresa;
	select @OldCodigo = DB_CODIGO from Deleted;
	select @NewCodigo = DB_CODIGO from Inserted;

	select @DB_CODIGO = DB_CODIGO, @DB_CONTROL = DB_CONTROL, @DB_TIPO = DB_TIPO, @DB_DATOS= DB_DATOS,
	@DB_USRNAME = DB_USRNAME, @DB_PASSWRD = DB_PASSWRD, @DB_CTRL_RL = DB_CTRL_RL, @DB_CHKSUM = DB_CHKSUM
	from INSERTED;

	update COMPANY
	set DB_CODIGO = @NewCodigo, CM_CONTROL = (
		CASE
			WHEN @DB_TIPO = 0 THEN '3DATOS'
			WHEN @DB_TIPO = 1 THEN '3RECLUTA'
			WHEN @DB_TIPO = 2 THEN '3VISITA'
			WHEN @DB_TIPO = 3 THEN '3OTRO'
			WHEN @DB_TIPO = 4 THEN '3DATOS' 
			WHEN @DB_TIPO = 5 THEN '3PRESUP'
			ELSE ''
		END
		),
		CM_DATOS = @DB_DATOS,
		CM_USRNAME = @DB_USRNAME,
		CM_PASSWRD = @DB_PASSWRD,
		CM_CTRL_RL  = (
		CASE
			WHEN @DB_TIPO = 0 THEN '3DATOS'
			WHEN @DB_TIPO = 1 THEN '3RECLUTA'
			WHEN @DB_TIPO = 2 THEN '3VISITA'
			WHEN @DB_TIPO = 3 THEN '3OTRO'
			WHEN @DB_TIPO = 4 THEN '3PRUEBA'
			WHEN @DB_TIPO = 5 THEN '3PRESUP'
			ELSE ''
		END
		),
		CM_CHKSUM = @DB_CHKSUM
		where DB_CODIGO = @OldCodigo
end
GO

/* Patch 440: Agregar trigger COMPANY -> DB_INFO (1/2) */
IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[TR_COMPANY_DB_CODIGO]') AND xtype in (N'TR'))
DROP TRIGGER TR_COMPANY_DB_CODIGO
GO

/* Patch 440: Agregar trigger COMPANY -> DB_INFO (2/2) */
CREATE trigger TR_COMPANY_DB_CODIGO
on COMPANY for insert, update AS
begin
	 set NOCOUNT on;

	 IF UPDATE( DB_CODIGO )
	 BEGIN
		declare @DB_TIPO Status;
		declare @DB_DATOS Accesos;
		declare @DB_USRNAME UsuarioCorto;
		declare @DB_PASSWRD Passwd;
		declare @DB_CHKSUM Accesos;

		select @DB_TIPO = DB_TIPO, @DB_DATOS= DB_DATOS,
			@DB_USRNAME = DB_USRNAME, @DB_PASSWRD = DB_PASSWRD, 
			@DB_CHKSUM = DB_CHKSUM
			from DB_INFO WHERE DB_CODIGO IN (SELECT DB_CODIGO FROM INSERTED);

		update COMPANY
			set CM_CONTROL = (
			CASE
				WHEN @DB_TIPO = 0 THEN '3DATOS'
				WHEN @DB_TIPO = 1 THEN '3RECLUTA'
				WHEN @DB_TIPO = 2 THEN '3VISITA'
				WHEN @DB_TIPO = 3 THEN '3OTRO'
				WHEN @DB_TIPO = 4 THEN '3DATOS'
				WHEN @DB_TIPO = 5 THEN '3PRESUP'
				ELSE ''
			END
			),
			CM_DATOS = COALESCE (@DB_DATOS, ''),
			CM_USRNAME = COALESCE (@DB_USRNAME, ''),
			CM_PASSWRD = COALESCE (@DB_PASSWRD, ''),
			CM_CTRL_RL = (
			CASE
				WHEN @DB_TIPO = 0 THEN '3DATOS'
				WHEN @DB_TIPO = 1 THEN '3RECLUTA'
				WHEN @DB_TIPO = 2 THEN '3VISITA'
				WHEN @DB_TIPO = 3 THEN '3OTRO'
				WHEN @DB_TIPO = 4 THEN '3PRUEBA'
				WHEN @DB_TIPO = 5 THEN '3PRESUP'
				ELSE ''
			END
			),
			CM_CHKSUM = COALESCE (@DB_CHKSUM, '')
		where CM_CODIGO IN (SELECT CM_CODIGO FROM INSERTED)
	END
end
GO

/* Patch 440: Agregar tabla CuentaTim */
IF NOT EXISTS( select * from sysobjects where NAME = 'CuentaTim' and type = 'U' )
begin
CREATE TABLE CuentaTim(
	CT_CODIGO Codigo,
	CT_DESCRIP Descripcion,
	CT_ID FolioGrande,
	CT_PASSWRD Formula,
	CT_ACTIVO Booleano,
	US_CODIGO Usuario,
	US_FEC_MOD Fecha,
 CONSTRAINT PK_CuentaTim  PRIMARY KEY
(
	CT_CODIGO
)
)
end
GO

/* Patch 440: Nueva Interfaz 2014 - Creacion de Columna USUARIO.US_CLASICA */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'US_CLASICA' AND id = object_id('USUARIO'))
BEGIN
	ALTER TABLE USUARIO ADD US_CLASICA Booleano NULL;
END
GO

/* Patch 440: Nueva Interfaz 2014 - Actualizacion de nuevo campo USUARIO.US_CLASICA en los registros donde sea nulo */
IF EXISTS( SELECT * FROM syscolumns WHERE NAME = 'US_CLASICA' AND id = object_id('USUARIO'))
BEGIN
     UPDATE USUARIO SET US_CLASICA='N' WHERE US_CLASICA IS NULL;
END
GO

/* Patch 440: Nueva Interfaz 2014 - Poner NOT NULL USUARIO.US_CLASICA */
IF EXISTS( SELECT * FROM syscolumns WHERE NAME = 'US_CLASICA' AND id = object_id('USUARIO'))
BEGIN
	ALTER TABLE USUARIO ALTER COLUMN US_CLASICA Booleano NOT NULL;
END
GO

/* Patch 440: Nueva Interfaz 2014 - Creacion de ARBOL.AB_MODULO */
IF NOT EXISTS( SELECT * FROM syscolumns WHERE NAME = 'AB_MODULO' AND id = object_id('ARBOL'))
BEGIN
	ALTER TABLE ARBOL ADD AB_MODULO Status NULL;
END
GO

/* Patch 440: Nueva Interfaz 2014 - Actualizacion de nuevos campo de ARBOL.AB_MODULO   */
IF EXISTS( SELECT * FROM syscolumns WHERE NAME = 'AB_MODULO' AND id = object_id('ARBOL'))
BEGIN
	/*Inicializar el campo AB_MODULO Con 0 para conservar arboles de usuario para sistema TRESS que puedieran existir en la BD,
		0=TRESS
		1=CAJAAHORRO*/
	UPDATE ARBOL SET AB_MODULO= 0 WHERE AB_MODULO IS NULL;
END
GO

/* Patch 440: Nueva Interfaz 2014 - Poner el nuevo campo de ARBOL.AB_MODULO  como NOT NULL */
IF EXISTS( SELECT * FROM syscolumns WHERE NAME = 'AB_MODULO'  AND id = object_id('ARBOL'))
BEGIN
	ALTER TABLE ARBOL ALTER COLUMN AB_MODULO Status NOT NULL;
END
GO

/* Patch 440: Nueva Interfaz 2014 - Remover el viejo PrimaryKey de la tabla */
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'PK_ARBOL'))
BEGIN
	ALTER TABLE ARBOL DROP CONSTRAINT PK_ARBOL;
END
GO

/* Patch 440: Nueva Interfaz 2014 - Agregar nuevo PrimaryKey a la tabla */
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'PK_ARBOL'))
BEGIN
	ALTER TABLE ARBOL ADD CONSTRAINT PK_ARBOL PRIMARY KEY (US_CODIGO,AB_ORDEN,AB_MODULO);
END
GO

/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 1/3 */
/* Patch 440 #Seq: 1*/
create procedure ACTUALIZA_DERECHOS
as
begin
	SET NOCOUNT ON;

	declare @Cuantos Integer;

	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 716);
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 716, 25 FROM ACCESO WHERE AX_NUMERO = 7 and AX_DERECHO > 0 
end
GO

/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 2/3 */
/* Patch 440 #Seq: 1*/
execute ACTUALIZA_DERECHOS
GO

/* CR:, Timbrado Actualizar Derechos Datos\Otros\Timbrado 3/3 */
/* Patch 440 #Seq: 1*/
DROP PROCEDURE ACTUALIZA_DERECHOS
GO

/* Agregar nuevo campo TE_ZONA_ES a Tabla TERMINAL */
if not exists( select * from syscolumns where NAME = 'TE_ZONA_ES' and id = object_id('TERMINAL'))
begin
     ALTER TABLE TERMINAL add TE_ZONA_ES DescLarga Null;
end
go
update TERMINAL set TE_ZONA_ES = '' where TE_ZONA_ES is null;
GO
alter table TERMINAL alter column TE_ZONA_ES DescLarga not null;
GO

/* Agregar nuevo campo TE_VERSION a Tabla TERMINAL */
if not exists( select * from syscolumns where NAME = 'TE_VERSION' and id = object_id('TERMINAL'))
begin
     ALTER TABLE TERMINAL add TE_VERSION Descripcion Null;
end
go
update TERMINAL set TE_VERSION = '' where TE_VERSION is null;
GO
alter table TERMINAL alter column TE_VERSION Descripcion null;
GO

/* Agregar nuevo campo TE_IP a Tabla TERMINAL */
if not exists( select * from syscolumns where NAME = 'TE_IP' and id = object_id('TERMINAL'))
begin
     ALTER TABLE TERMINAL add TE_IP Descripcion Null;
end
go
update TERMINAL set TE_IP = '' where TE_IP is null;
GO
alter table TERMINAL alter column TE_IP Descripcion null;
GO

/* Agregar nuevo campo TE_TER_HOR a Tabla TERMINAL */
if not exists( select * from syscolumns where NAME = 'TE_TER_HOR' and id = object_id('TERMINAL'))
begin
     ALTER TABLE TERMINAL add TE_TER_HOR fecha Null;
end
go
update TERMINAL set TE_TER_HOR = '18991230' where TE_TER_HOR is null;
GO
alter table TERMINAL alter column TE_TER_HOR fecha not null;
GO

/* Agregar nuevo campo TM_ULTIMA a Tabla TERM_MSG */
if not exists( select * from syscolumns where NAME = 'TM_ULTIMA' and id = object_id('TERM_MSG'))
begin
     ALTER TABLE TERM_MSG add TM_ULTIMA fecha Null;
end
go
update TERM_MSG set TM_ULTIMA = '18991230' where TM_ULTIMA is null;
GO
alter table TERM_MSG alter column TM_ULTIMA fecha not null;
GO

/* Agregar nuevo campo BORRAR a Tabla TERM_HUELLA */
if not exists( select * from syscolumns where NAME = 'BORRAR' and id = object_id('TERM_HUELLA'))
begin
     ALTER TABLE TERM_HUELLA add BORRAR booleano Null;
end
go

update TERM_HUELLA set BORRAR = 'N' where BORRAR is null;
GO

alter table TERM_HUELLA alter column BORRAR booleano not null;
GO

/* Agregar nuevo campo CM_CODIGO a Tabla GRUPOTERM */
if not exists( select * from syscolumns where NAME = 'CM_CODIGO' and id = object_id('GRUPOTERM'))
begin
     ALTER TABLE GRUPOTERM add CM_CODIGO CodigoEmpresa Null;
end
go

update GRUPOTERM set CM_CODIGO = '' where CM_CODIGO is null;
GO

alter table GRUPOTERM alter column CM_CODIGO CodigoEmpresa not null;
GO

/* Borrra la Huella del indice en Base de datos */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_TERMINAL_BORRAHUELLA')
BEGIN
  DROP PROCEDURE WS_TERMINAL_BORRAHUELLA
END
GO
create PROCEDURE [WS_TERMINAL_BORRAHUELLA] ( @TE_CODIGO NumeroEmpleado, 
													@ID_NUMERO int, @BORRAR char(1))
as 
begin 
	DECLARE @CM_CODIGO Codigoempresa 
	DECLARE @CB_CODIGO NumeroEmpleado 
	DECLARE @HU_INDICE usuario 
 
	SELECT @CM_CODIGO = Coalesce( CM_CODIGO, '' ), @CB_CODIGO = Coalesce( CB_CODIGO, 0 )
	FROM EMP_BIO 
	where ID_NUMERO = @ID_NUMERO;
 
	DELETE FROM TERM_HUELLA 
	WHERE  ( (CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (TE_CODIGO = @TE_CODIGO) and ( BORRAR = @BORRAR ) )
	
	select @CM_CODIGO as Empresa, @CB_CODIGO as Empleado
end
GO

/* Agregar Huella */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_TERMINAL_AGREGAHUELLA')
BEGIN
  DROP PROCEDURE WS_TERMINAL_AGREGAHUELLA
END
GO
Create PROCEDURE [WS_TERMINAL_AGREGAHUELLA] 
( @CM_CODIGO CodigoEmpresa, @CB_CODIGO NumeroEmpleado, @HU_INDICE Usuario, @HU_HUELLA Memo ) 
as 
begin 
	if not exists (SELECT CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA FROM HUELLAGTI WHERE(CM_CODIGO = @CM_CODIGO) AND (CB_CODIGO = @CB_CODIGO) AND (HU_INDICE = @HU_INDICE)) 
	begin 
		INSERT INTO HUELLAGTI(CM_CODIGO, CB_CODIGO, HU_INDICE, HU_HUELLA,HU_TIMEST) 
		VALUES (@CM_CODIGO,@CB_CODIGO,@HU_INDICE,@HU_HUELLA,getdate()) 
	end 
	else 
	begin 
		UPDATE HUELLAGTI SET HU_HUELLA = @HU_HUELLA WHERE CM_CODIGO = @CM_CODIGO and CB_CODIGO = @CB_CODIGO and HU_INDICE = @HU_INDICE 
	end 
end 
GO

/* Heartbeat */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_TERMINAL_HEARTBEAT')
BEGIN
  DROP PROCEDURE WS_TERMINAL_HEARTBEAT
END
GO
Create procedure [WS_TERMINAL_HEARTBEAT] 
( @TerminalID NumeroEmpleado , @Version varchar(30), @IpAddress varchar(30) , @TER_HOR datetime ) 
as 
begin 
	SELECT TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA,TE_ZONA_ES, TE_ACTIVO, TE_XML, TE_LOGO, TE_FONDO 
	FROM TERMINAL 
	WHERE (TE_CODIGO = @TerminalID) 
end 
GO

/* Huella Sobrante */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_TERMINAL_HUELLASOBRANTE')
BEGIN
  DROP PROCEDURE WS_TERMINAL_HUELLASOBRANTE
END
GO
Create PROCEDURE [WS_TERMINAL_HUELLASOBRANTE] 
( @TE_CODIGO NumeroEmpleado ) 
as 
begin 
	select * 
	from EMP_BIO 
	inner join TERM_HUELLA on EMP_BIO.CB_CODIGO = TERM_HUELLA.CB_CODIGO and EMP_BIO.CM_CODIGO = TERM_HUELLA.CM_CODIGO 
	where TERM_HUELLA.BORRAR = 'S' 
	and TERM_HUELLA.TE_CODIGO =@TE_CODIGO 
end;
GO

/*setHeartBeat*/
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_TERMINAL_SETHEARTBEAT')
BEGIN
  DROP PROCEDURE [WS_TERMINAL_SETHEARTBEAT]
END
GO
Create procedure [WS_TERMINAL_SETHEARTBEAT] 
( @TerminalID NumeroEmpleado , @Version varchar(30) , @IpAddress varchar(30)  , @TER_HOR datetime)
 as 
 begin 
	UPDATE TERMINAL SET TE_BEAT = GETDATE(),TE_Version=@Version, TE_IP=@IpAddress, TE_TER_HOR = @TER_HOR 
	WHERE (TE_CODIGO = @TerminalID) 
end 
GO

/*ws_set_pendiente*/
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_TERMINAL_SETPENDIENTE')
BEGIN
  DROP PROCEDURE [WS_TERMINAL_SETPENDIENTE]
END
GO
CREATE PROCEDURE [WS_TERMINAL_SETPENDIENTE] ( @TE_CODIGO NUMEROEMPLEADO, @DM_CODIGO CODIGO ) 
AS 
BEGIN 
	UPDATE TERM_MSG SET TM_NEXT = CASE WHEN TM_SLEEP = 0 THEN '29991231' 
			ELSE DATEADD(MI, TM_SLEEP, GETDATE()) 
			END 
	WHERE DM_CODIGO = @DM_CODIGO 
	AND TE_CODIGO =@TE_CODIGO 
END
GO

/*WS_TERMINAL_IDENTIFICA*/
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'WS_TERMINAL_IDENTIFICA')
BEGIN
  DROP PROCEDURE [WS_TERMINAL_IDENTIFICA]
END
GO
create procedure [WS_TERMINAL_IDENTIFICA]
as
begin
     SELECT TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA, TE_ACTIVO, TE_XML, TE_LOGO, TE_FONDO, TE_ZONA_ES
     FROM TERMINAL
end
GO

/* TR_ACTUALIZA_TMNEXT */
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'TR_ACTUALIZA_TMNEXT')
BEGIN
  DROP trigger TR_ACTUALIZA_TMNEXT
END
GO
Create trigger [TR_ACTUALIZA_TMNEXT] on [TERM_MSG]
after UPDATE AS
BEGIN
	set nocount on

	declare @nextMsg datetime 
	declare @dmCodigo int
	declare @tecodigo int 


	select @nextMsg = TM_NEXT from Inserted
	select @dmCodigo = DM_CODIGO from inserted
	select @tecodigo = TE_CODIGO from inserted
		
	if( update( TM_NEXT ) )
	begin
		if( @dmCodigo = 6 and @nextmsg > Convert(datetime,'18991230') )
		begin
			EXECUTE('update Term_MSG set tm_next =''18991230'' where TE_CODIGO='+@tecodigo+' and dm_Codigo = 4')
		end 

		if(@nextmsg > Convert(datetime,'18991230'))
		begin
			EXECUTE('update Term_MSG set tm_ultima =getdate() where TE_CODIGO='+@tecodigo+' and dm_Codigo = '+@dmCodigo)
		end 
	end
end
GO

/*trigger Update Huella GTI*/
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'TR_UPDATE_HUELLAGTI')
BEGIN
  DROP Trigger [TR_UPDATE_HUELLAGTI]
END
GO
Create trigger TR_UPDATE_HUELLAGTI on HUELLAGTI AFTER INSERT, UPDATE 
as 
begin 
	set nocount on 
	/* Trigger de actualizacion de Timestamp de huella gti */ 
	declare @HU_ID NumeroEmpleado 
	
	select @HU_ID = HU_ID 
	from Inserted 
	
	if UPDATE( HU_HUELLA ) 
	begin 
		update HUELLAGTI set HU_TIMEST = GETDATE() 
		where HU_ID = @HU_ID 
	end 
end 
GO

/* CREACION DE TRIGGER EN TERMINAL*/
if exists( select NAME from SYSOBJECTS where NAME = 'TR_ADD_TERMINAL' and XTYPE = 'TR' )
	drop trigger TR_ADD_TERMINAL;

go

create trigger TR_ADD_TERMINAL on TERMINAL after insert
as
begin
	set nocount on
	
	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx		
	declare ListaMensajes cursor for
		select DM_CODIGO, DM_SLEEP from MENSAJE where DM_ACTIVO='S' order by DM_ORDEN
		
	select @Terminal = TE_CODIGO from Inserted;
	
	open ListaMensajes
	fetch next from ListaMensajes into @Mensaje, @Sleep
	while (@@Fetch_Status = 0)
	begin
		insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP) values (@Terminal, @Mensaje, @Sleep)
		
		fetch next from ListaMensajes into @Mensaje, @Sleep
	end
	close ListaMensajes
	deallocate ListaMensajes
	
	update TERMINAL set TE_XML = 
'<?xml version="1.0" encoding="utf-8"?>
<properties>
   <ClockId>'+@Terminal+'</ClockId> 
   <WebService>http://SERVIDORWEB/WSAsistencia/WSAsistencia.asmx</WebService>
   <UseProxy>false</UseProxy>
   <Proxy>192.168.0.30</Proxy>
   <ProxyPort>8080</ProxyPort>
   <UseProxyAuthentication>false</UseProxyAuthentication>
   <ProxyUserName>NULL</ProxyUserName>
   <ProxyPassword>NULL</ProxyPassword>
   <Ip>DHCP</Ip>
   <NetMask></NetMask>
   <DefaultGateWay></DefaultGateWay>
   <DNS></DNS>
   <TimeOut>07</TimeOut>
   <HeartBeatTimer>60</HeartBeatTimer>
   <MsgTime>03</MsgTime>
   <ErrorMsgTime>05</ErrorMsgTime>
   <RelayTime>02</RelayTime>
   <DateTimeFormat>dd/MM hh:mm</DateTimeFormat>
   <Volume>70</Volume>
   <ProxReader>ON</ProxReader>
   <BarCodeReader>ON</BarCodeReader>
   <HID>ON</HID>
   <MenuAccessCode>1234</MenuAccessCode>
   <FpuNotIdetifyMessage>INVALIDO/INTENTE DE NUEVO</FpuNotIdetifyMessage>
   <NotValidByServerMessage>Servidor no pudo validar ID</NotValidByServerMessage>
   <DuplicateMessage>Registro Duplicado</DuplicateMessage>
   <AutenticateMessage>Exito</AutenticateMessage>
   <OfflineValidation>Offline-Almacenada</OfflineValidation>
   <OneMinuteValidate>true</OneMinuteValidate>   
   <FingerVerify>false</FingerVerify>
   <EnableOfflineRelay>false</EnableOfflineRelay>
   <FingerPrint>ON</FingerPrint>
   <EnableKeyPad>true</EnableKeyPad>
   <Version>TRESS</Version>
   <Account>0</Account>
   <ClientId>0</ClientId>
   <Password>0</Password>
   <CajaId>0</CajaId>
   <Identidad>1</Identidad>
   <UseSoapActionHeader>true</UseSoapActionHeader>
   <SoapActionHeader>Synel/Services</SoapActionHeader>
   <UpdateTimeByServer>true</UpdateTimeByServer>
   <ntpServer>SERVIDORNTP</ntpServer>
   <timeZone>Tijuana</timeZone>
   <MifareReader>OFF</MifareReader>
   <MagneticReader>OFF</MagneticReader>
   <ShowCameraPhoto>false</ShowCameraPhoto>
   <ShowPhoto>true</ShowPhoto>
   <Schedules>
   <Function code="1" start="00:00" end="23:59" bg="comida.jpg">Comida</Function>
   <Function code="3" start="12:01" end="14:00" bg="desayuno.jpg">Desayuno</Function>
   <Function code="2" start="14:01" end="23:59" bg="cena.jpg">Cena</Function>
   </Schedules>
</properties>' 
	where TE_CODIGO = @Terminal
end
go

if not exists( select * from syscolumns where NAME = 'GP_CODIGO' and id = object_id('EMP_ID'))
begin
     ALTER TABLE EMP_ID add GP_CODIGO Codigo Null;
end
go
update EMP_ID set GP_CODIGO = '' where GP_CODIGO is null;
GO
alter table EMP_ID alter column GP_CODIGO Codigo null;
GO


/* Patch 440: Agregar registros en ACC_ARBOL y ACC_DER a partir de COMPANY */
/* A. Definir SP */
create procedure ADD_DERECHOS
as
begin
	SET NOCOUNT ON;
	if (SELECT COUNT(*) FROM ACC_ARBOL WHERE AA_SOURCE= 0 AND AX_NUMERO=717) = 0 
		BEGIN	
		--Se eliminan todas las opciones de TRESS--
		DELETE FROM ACC_ARBOL WHERE AA_SOURCE=0 
		--Inserciones ACC_ARBOL--
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,1,'Empleados',416,1,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,2,'Empleados-Datos',416,2,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,3,'Empleados-Datos-Identificaci�n',416,3,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,4,'Empleados-Datos-Contrataci�n',416,4,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,5,'Empleados-Datos-Area',416,5,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,6,'Empleados-Datos-Percepciones',416,6,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,8,'Empleados-Datos-Adicionales',416,7,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,7,'Empleados-Datos-Otros',416,8,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,693,'Empleados-Datos-Otros-Mis Datos',435,9,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,692,'Empleados-Datos-Otros-Infonavit',435,10,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,684,'Empleados-Datos-Otros-Fonacot',435,11,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,685,'Empleados-Datos-Otros-Programa de Primer Empleo',435,12,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,686,'Empleados-Datos-Otros-Asistencia',435,13,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,687,'Empleados-Datos-Otros-Evaluaci�n',435,14,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,688,'Empleados-Datos-Otros-Cuentas',435,15,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,689,'Empleados-Datos-Otros-Confidencialidad',435,16,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,690,'Empleados-Datos-Otros-Datos M�dicos',435,17,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,691,'Empleados-Datos-Otros-Brigadas de Protecci�n Civil',435,18,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,716,'Empleados-Datos-Otros-Timbrado de N�mina',440,19,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,584,'Empleados-Datos-Usuario',416,20,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,9,'Empleados-Curr�culum',416,21,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,10,'Empleados-Curr�culum-Personales',416,22,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,11,'Empleados-Curr�culum-Foto e Im�genes',416,23,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,629,'Empleados-Curr�culum-Documentos',416,24,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,12,'Empleados-Curr�culum-Parientes',416,25,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,13,'Empleados-Curr�culum-Experiencia',416,26,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,14,'Empleados-Curr�culum-Cursos Anteriores',416,27,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,15,'Empleados-Curr�culum-Puestos Anteriores',416,28,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,16,'Empleados-Expediente',416,29,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,17,'Empleados-Expediente-Kardex',416,30,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,18,'Empleados-Expediente-Vacaciones',416,31,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,19,'Empleados-Expediente-Incapacidades',416,32,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,20,'Empleados-Expediente-Permisos',416,33,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,21,'Empleados-Expediente-Cursos Tomados',416,34,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,215,'Empleados-Expediente-Cursos Programados',416,35,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,505,'Empleados-Expediente-Certificaciones Tomadas',416,36,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,591,'Empleados-Expediente-Certificaciones Programadas',416,37,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,22,'Empleados-Expediente-Pagos IMSS',416,38,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,679,'Empleados-Expediente-Plan de Capacitaci�n',435,39,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,694,'Empleados-Expediente-Competencias Evaluadas',435,40,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,595,'Empleados-Expediente-Cr�dito INFONAVIT',416,41,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,287,'Empleados-Expediente-Herramientas',416,42,12,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,660,'Empleados-Expediente-Seguros de Gastos M�dicos',430,43,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,23,'Empleados-N�mina',416,44,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,24,'Empleados-N�mina-Ahorros',416,45,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,25,'Empleados-N�mina-Pr�stamos',416,46,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,26,'Empleados-N�mina-Acumulados',416,47,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,27,'Empleados-N�mina-Historial',416,48,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,654,'Empleados-N�mina-Pensiones Alimenticias',430,49,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,605,'Empleados-N�mina-Simulaci�n Finiquitos',416,50,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,606,'Empleados-N�mina-Simulaci�n Finiquitos-Totales',416,51,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,521,'Empleados-N�mina-Simulaci�n Finiquitos-Por Empleado',416,52,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,240,'Empleados-Cafeter�a',416,53,6,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,241,'Empleados-Cafeter�a-Comidas Diarias',416,54,6,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,242,'Empleados-Cafeter�a-Comidas Per�odo',416,55,6,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,243,'Empleados-Cafeter�a-Invitaciones',416,56,6,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,598,'Empleados-Caseta',416,57,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,599,'Empleados-Caseta-Checadas Diarias',416,58,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,28,'Empleados-Registro',416,59,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,29,'Empleados-Registro-Alta',416,60,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,30,'Empleados-Registro-Baja',416,61,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,31,'Empleados-Registro-Reingreso',416,62,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,32,'Empleados-Registro-Cambio de Salario',416,63,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,33,'Empleados-Registro-Cambio de Turno',416,64,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,34,'Empleados-Registro-Cambio de Puesto',416,65,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,35,'Empleados-Registro-Cambio de Area',416,66,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,36,'Empleados-Registro-Cambio de Contrato',416,67,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,37,'Empleados-Registro-Cambio de Prestaciones',416,68,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,523,'Empleados-Registro-Cambio de Tipo de N�mina',416,69,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,216,'Empleados-Registro-Cambios M�ltiples',416,70,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,38,'Empleados-Registro-Vacaciones',416,71,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,96,'Empleados-Registro-Cerrar Vacaciones',416,72,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,39,'Empleados-Registro-Incapacidad',416,73,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,40,'Empleados-Registro-Permiso',416,74,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,41,'Empleados-Registro-Kardex general',416,75,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,42,'Empleados-Registro-Curso Tomado',416,76,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,250,'Empleados-Registro-Comidas',416,77,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,373,'Empleados-Registro-Pr�stamos',416,78,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,288,'Empleados-Registro-Entregar Herramienta',416,79,12,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,289,'Empleados-Registro-Entrega Global',416,80,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,290,'Empleados-Registro-Regresar Herramienta',416,81,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,524,'Empleados-Registro-Certificaciones por Empleado',416,82,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,525,'Empleados-Registro-Empleados Certificados',416,83,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,600,'Empleados-Registro-Caseta',416,84,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,43,'Empleados-Procesos',416,85,0,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,44,'Empleados-Procesos-Recalcular Integrados',416,86,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,45,'Empleados-Procesos-Promediar Percepciones Variables',416,87,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,46,'Empleados-Procesos-Cambio de Salario Global',416,88,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,48,'Empleados-Procesos-Aplicar Tabulador',416,89,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,47,'Empleados-Procesos-Vacaciones Globales',416,90,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,211,'Empleados-Procesos-Cancelar Vacaciones',416,91,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,291,'Empleados-Procesos-Cierre Global de Vacaciones',416,92,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,460,'Empleados-Procesos-Cancelar Cierres de Vacaciones',416,93,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,519,'Empleados-Procesos-Recalcula Saldos de Vacaciones',416,94,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,49,'Empleados-Procesos-Aplicar Eventos',416,95,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,50,'Empleados-Procesos-Importar Kardex',416,96,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,51,'Empleados-Procesos-Cancelar Kardex',416,97,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,361,'Empleados-Procesos-Importar Altas',416,98,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,367,'Empleados-Procesos-Permisos Globales',416,99,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,516,'Empleados-Procesos-Cancelar Permisos Globales',416,100,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,52,'Empleados-Procesos-Curso Tomado Global',416,101,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,674,'Empleados-Procesos-Importar Organigrama',435,102,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,392,'Empleados-Procesos-Cancelar Curso Tomado Global',416,103,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,607,'Empleados-Procesos-Curso Programado Global',416,104,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,610,'Empleados-Procesos-Cancelar Curso Programado Global',416,105,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,617,'Empleados-Procesos-Foliar Capacitaciones de STPS',420,106,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,457,'Empleados-Procesos-Importar Cursos Tomados',416,107,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,227,'Empleados-Procesos-Transferencia de Empleado',416,108,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,366,'Empleados-Procesos-Renumera Empleados',416,109,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,292,'Empleados-Procesos-Entregar Herramienta',416,110,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,293,'Empleados-Procesos-Regresar Herramienta',416,111,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,396,'Empleados-Procesos-Registro Grupal de Comidas',416,112,0,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,397,'Empleados-Procesos-Corregir Fechas Globales de Cafeter�a',416,113,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,517,'Empleados-Procesos-Importar Ahorros/Pr�stamos',416,114,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,662,'Empleados-Procesos-Renovar Seguros de Gastos M�dicos',430,115,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,661,'Empleados-Procesos-Importar Seguros de Gastos M�dicos',430,116,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,663,'Empleados-Procesos-Borrar Seguros de Gastos M�dicos',430,117,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,714,'Empleados-Procesos-Importar Imagenes',440,118,0,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,53,'Asistencia',416,119,3,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,54,'Asistencia-Datos',416,120,3,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,55,'Asistencia-Datos-Tarjeta Diaria',416,121,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,56,'Asistencia-Datos-Pre-N�mina',416,122,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,57,'Asistencia-Datos-Calendario',416,123,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,265,'Asistencia-Datos-Autorizaci�n',416,124,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,407,'Asistencia-Datos-Clasificaciones',416,125,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,408,'Asistencia-Datos-Clasificaciones-Temporales',416,126,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,409,'Asistencia-Datos-Clasificaciones-Tipos',416,127,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,58,'Asistencia-Registro',416,128,3,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,59,'Asistencia-Registro-Autorizar Extras y Permisos',416,129,3,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,60,'Asistencia-Registro-Horarios Temporales',416,130,3,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,399,'Asistencia-Registro-Autorizaciones por Aprobar',416,131,3,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,61,'Asistencia-Procesos',416,132,3,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,62,'Asistencia-Procesos-Poll de Relojes',416,133,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,63,'Asistencia-Procesos-Procesar Tarjetas',416,134,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,64,'Asistencia-Procesos-Calcular Pre-N�mina',416,135,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,65,'Asistencia-Procesos-Extras y Permisos Globales',416,136,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,66,'Asistencia-Procesos-Registros Autom�ticos',416,137,3,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,67,'Asistencia-Procesos-Corregir Fechas Globales',416,138,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,68,'Asistencia-Procesos-Recalcular Tarjetas',416,139,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,514,'Asistencia-Procesos-Intercambio de Festivos',416,140,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,627,'Asistencia-Procesos-Registrar Excepciones de Festivos',416,141,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,515,'Asistencia-Procesos-Cancelar Excepciones de Festivos',416,142,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,611,'Asistencia-Procesos-Ajustar Incapacidades en Calendario',416,143,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,622,'Asistencia-Procesos-Autorizaci�n de Pre-N�mina',416,144,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,637,'Asistencia-Procesos-Importar Clasificaciones Temporales',430,145,3,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,398,'Asistencia-Bloqueo de Cambios',416,146,3,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,69,'N�minas',416,147,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,70,'N�minas-Datos',416,148,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,71,'N�minas-Datos-Totales',416,149,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,72,'N�minas-Datos-D�as/Horas',416,150,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,73,'N�minas-Datos-Montos',416,151,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,74,'N�minas-Datos-Pre-N�mina',416,152,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,75,'N�minas-Datos-Clasificaci�n',416,153,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,506,'N�minas-Datos-P�lizas',416,154,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,76,'N�minas-Excepciones',416,155,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,77,'N�minas-Excepciones-D�as/Horas',416,156,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,78,'N�minas-Excepciones-Montos',416,157,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,251,'N�minas-Excepciones-Globales',416,158,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,631,'N�minas-Excepciones-Transferencias',425,159,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,588,'N�minas-Pagos Fonacot',416,160,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,589,'N�minas-Pagos Fonacot-Totales',416,161,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,590,'N�minas-Pagos Fonacot-Totales por Empleado',416,162,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,79,'N�minas-Registro',416,163,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,80,'N�minas-Registro-N�mina Nueva',416,164,1,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,81,'N�minas-Registro-Borrar N�mina Activa',416,165,1,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,82,'N�minas-Registro-Excepci�n de D�a/Hora',416,166,1,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,83,'N�minas-Registro-Excepci�n de Monto',416,167,1,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,259,'N�minas-Registro-Excepciones de Monto Globales',416,168,1,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,84,'N�minas-Registro-Liquidaci�n',416,169,1,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,269,'N�minas-Registro-Pago de Recibos',416,170,1,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,85,'N�minas-Procesos',416,171,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,86,'N�minas-Procesos-Calcular N�mina',416,172,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,87,'N�minas-Procesos-Afectar N�mina',416,173,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,88,'N�minas-Procesos-Foliar Recibos',416,174,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,89,'N�minas-Procesos-Imprimir Listado',416,175,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,90,'N�minas-Procesos-Imprimir Recibos',416,176,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,91,'N�minas-Procesos-Generar P�liza Contable',416,177,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,92,'N�minas-Procesos-Imprimir P�liza Contable',416,178,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,93,'N�minas-Procesos-Exportar P�liza Contable',416,179,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,360,'N�minas-Procesos-SUBE Aplicado Mensual',416,180,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,94,'N�minas-Procesos-Desafectar N�mina',416,181,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,95,'N�minas-Procesos-Limpiar Acumulados',416,182,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,226,'N�minas-Procesos-Recalcular Acumulados',416,183,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,98,'N�minas-Procesos-Importar Movimientos',416,184,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,99,'N�minas-Procesos-Exportar Movimientos',416,185,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,271,'N�minas-Procesos-Importar Pago de Recibos',416,186,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,270,'N�minas-Procesos-Re-Foliar Recibos',416,187,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,374,'N�minas-Procesos-Calcular Retroactivos',416,188,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,214,'N�minas-Procesos-Copiar N�mina',416,189,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,264,'N�minas-Procesos-Calcular Diferencias',416,190,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,100,'N�minas-Procesos-Pagar por Fuera',416,191,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,101,'N�minas-Procesos-Cancelar N�minas Pasadas',416,192,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,102,'N�minas-Procesos-Liquidaci�n Global',416,193,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,103,'N�minas-Procesos-Calcular Salario Neto/Bruto',416,194,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,104,'N�minas-Procesos-Rastrear C�lculo',416,195,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,535,'N�minas-Procesos-Ajustar Retenci�n de Fonacot',416,196,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,536,'N�minas-Procesos-Calcular Pago Fonacot',416,197,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,633,'N�minas-Procesos-Transferencias',430,198,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,647,'N�minas-Procesos-Eliminar Transferencias',430,199,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,645,'N�minas-Procesos-C�lculo de Costeo',430,200,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,639,'N�minas-Procesos-C�lculo Previo de ISR',430,201,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,640,'N�minas-Procesos-Imprimir C�lculo Previo de ISR',430,202,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,105,'N�minas-Anuales',416,203,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,106,'N�minas-Anuales-Definir Per�odos',416,204,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,107,'N�minas-Anuales-Calcular Aguinaldos',416,205,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,233,'N�minas-Anuales-Imprimir Aguinaldos',416,206,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,228,'N�minas-Anuales-Pagar Aguinaldos',416,207,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,229,'N�minas-Anuales-Calcular Reparto de Ahorros',416,208,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,234,'N�minas-Anuales-Imprimir Reparto de Ahorros',416,209,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,108,'N�minas-Anuales-Pagar Ahorro',416,210,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,230,'N�minas-Anuales-Cierre de Ahorros',416,211,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,231,'N�minas-Anuales-Cierre de Pr�stamos',416,212,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,109,'N�minas-Anuales-Calcular PTU',416,213,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,238,'N�minas-Anuales-Imprimir PTU',416,214,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,239,'N�minas-Anuales-Pagar PTU',416,215,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,110,'N�minas-Anuales-Declaraci�n de Subsidio al Empleo',416,216,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,237,'N�minas-Anuales-Imprimir Declaraci�n de Subsidio al Empleo',416,217,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,111,'N�minas-Anuales-Comparar ISPT Anual',416,218,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,236,'N�minas-Anuales-Imprimir Comparativo Anual',416,219,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,235,'N�minas-Anuales-Diferencias de ISPT a N�mina',416,220,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,393,'N�minas-Anuales-Declaraci�n Anual',416,221,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,621,'N�minas-Anuales-Cierre Declaraci�n Anual',416,222,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,394,'N�minas-Anuales-Imprimir Declaraci�n Anual',416,223,1,3,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,612,'N�minas-Exceder L�mite de Monto en Excepciones',416,224,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,112,'Pagos IMSS',416,225,2,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,113,'Pagos IMSS-Datos',416,226,2,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,114,'Pagos IMSS-Datos-Totales',416,227,2,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,115,'Pagos IMSS-Datos-Por Empleado Mensual',416,228,2,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,116,'Pagos IMSS-Datos-Por Empleado Bimestral',416,229,2,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,596,'Pagos IMSS-Datos-Conciliaci�n Infonavit',416,230,2,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,117,'Pagos IMSS-Historia',416,231,2,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,118,'Pagos IMSS-Historia-Por Empleado Mensual',416,232,2,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,119,'Pagos IMSS-Historia-Por Empleado Bimestral',416,233,2,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,120,'Pagos IMSS-Registro',416,234,2,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,121,'Pagos IMSS-Registro-Pago de IMSS Nuevo',416,235,2,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,122,'Pagos IMSS-Registro-Borrar Pago de IMSS Activo',416,236,2,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,123,'Pagos IMSS-Procesos',416,237,2,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,124,'Pagos IMSS-Procesos-Calcular Pago',416,238,2,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,125,'Pagos IMSS-Procesos-Calcular Recargos',416,239,2,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,126,'Pagos IMSS-Procesos-Revisar Datos para SUA',416,240,2,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,127,'Pagos IMSS-Procesos-Exportar a SUA',416,241,2,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,128,'Pagos IMSS-Procesos-Calcular Prima de Riesgo',416,242,2,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,597,'Pagos IMSS-Procesos-Ajuste de Retenci�n Infonavit',416,243,2,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,630,'Pagos IMSS-Procesos-Validaci�n movimientos IDSE',416,244,2,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,471,'Capacitaci�n',416,245,4,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,395,'Capacitaci�n-Grupos',416,246,4,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,472,'Capacitaci�n-Reservaciones de aula',416,247,4,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,695,'Capacitaci�n-Matriz de Habilidades',435,248,4,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,129,'Consultas',416,249,25,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,130,'Consultas-Reportes',416,250,25,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,268,'Consultas-Reportes-Ver Datos Confidenciales',416,251,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,527,'Consultas-Reportes-Suscribirse a un Reporte',416,252,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,528,'Consultas-Reportes-Suscribir a otros Usuarios',416,253,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,567,'Consultas-Configurador de Reporteador',416,254,25,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,568,'Consultas-Configurador de Reporteador-Reporteador',416,255,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,569,'Consultas-Configurador de Reporteador-Reporteador-Reportes',416,256,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,571,'Consultas-Configurador de Reporteador-Reporteador-SQL',416,257,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,585,'Consultas-Configurador de Reporteador-Reporteador-Bit�cora',416,258,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,570,'Consultas-Configurador de Reporteador-Reporteador-Bit�cora de Procesos',416,259,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,572,'Consultas-Configurador de Reporteador-Configuraci�n',416,260,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,573,'Consultas-Configurador de Reporteador-Configuraci�n-Cat�logos de Clasificaciones',416,261,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,574,'Consultas-Configurador de Reporteador-Configuraci�n-Diccionario de Datos',416,262,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,575,'Consultas-Configurador de Reporteador-Configuraci�n-Cat�logo de Lista de Valores',416,263,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,576,'Consultas-Configurador de Reporteador-Configuraci�n-Cat�logos de Condiciones',416,264,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,577,'Consultas-Configurador de Reporteador-Configuraci�n-Cat�logos de M�dulos',416,265,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,586,'Consultas-Configurador de Reporteador-Configuraci�n-Procesos',416,266,25,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,587,'Consultas-Configurador de Reporteador-Configuraci�n-Procesos-Importar Diccionario',416,267,25,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,594,'Consultas-Configurador de Reporteador-Configuraci�n-Procesos-Exportar Diccionario',416,268,25,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,578,'Consultas-Configurador de Reporteador-Sistema',416,269,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,579,'Consultas-Configurador de Reporteador-Sistema-Grupo de Usuarios',416,270,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,133,'Consultas-SQL',416,271,25,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,161,'Consultas-Bit�cora / Procesos',416,272,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,131,'Consultas-Presupuesto de Personal',416,273,25,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,132,'Consultas-Presupuesto de Personal',416,274,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,375,'Consultas-Organigrama',416,275,25,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,508,'Consultas-Solicitud de vacaciones',416,276,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,134,'Cat�logos',416,277,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,135,'Cat�logos-Contrataci�n',416,278,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,136,'Cat�logos-Contrataci�n-Puestos',416,279,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,137,'Cat�logos-Contrataci�n-Clasificaciones',416,280,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,138,'Cat�logos-Contrataci�n-Percepciones Fijas',416,281,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,139,'Cat�logos-Contrataci�n-Turnos',416,282,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,140,'Cat�logos-Contrataci�n-Horarios',416,283,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,141,'Cat�logos-Contrataci�n-Prestaciones',416,284,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,142,'Cat�logos-Contrataci�n-Tipo de Contrato',416,285,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,511,'Cat�logos-Descripci�n de Puestos',416,286,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,522,'Cat�logos-Descripci�n de Puestos-Perfiles de Puestos',416,287,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,512,'Cat�logos-Descripci�n de Puestos-Secciones de Perfiles',416,288,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,513,'Cat�logos-Descripci�n de Puestos-Campos de Perfil',416,289,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,530,'Cat�logos-Valuaci�n de Puestos',416,290,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,534,'Cat�logos-Valuaci�n de Puestos-Valuaciones de Puestos',416,291,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,531,'Cat�logos-Valuaci�n de Puestos-Plantillas de Valuaci�n',416,292,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,532,'Cat�logos-Valuaci�n de Puestos-Factores de Valuaci�n',416,293,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,533,'Cat�logos-Valuaci�n de Puestos-Subfactores de Valuaci�n',416,294,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,143,'Cat�logos-N�mina',416,295,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,144,'Cat�logos-N�mina-Conceptos',416,296,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,97,'Cat�logos-N�mina-Par�metros',416,297,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,145,'Cat�logos-N�mina-Per�odos',416,298,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,146,'Cat�logos-N�mina-Folios de Recibos',416,299,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,507,'Cat�logos-N�mina-Tipos de P�liza',416,300,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,510,'Cat�logos-N�mina-Tipos de Periodo',416,301,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,653,'Cat�logos-N�mina-Tipos de Pensi�n',430,302,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,649,'Cat�logos-N�mina-Criterios de Costeo',430,303,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,650,'Cat�logos-N�mina-Conceptos de Costeo',430,304,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,147,'Cat�logos-Generales',416,305,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,566,'Cat�logos-Generales-Razones Sociales',416,306,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,148,'Cat�logos-Generales-Registros Patronales',416,307,26,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,149,'Cat�logos-Generales-Festivos Generales',416,308,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,150,'Cat�logos-Generales-Festivos por Turno',416,309,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,151,'Cat�logos-Generales-Condiciones',416,310,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,152,'Cat�logos-Generales-Eventos',416,311,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,406,'Cat�logos-Generales-Reglas de Caseta',416,312,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,659,'Cat�logos-Generales-Seguros de Gastos M�dicos',430,313,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,153,'Cat�logos-Capacitaci�n',416,314,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,191,'Cat�logos-Capacitaci�n-Tipo de Curso',416,315,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,372,'Cat�logos-Capacitaci�n-Clasifica Cursos',416,316,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,154,'Cat�logos-Capacitaci�n-Cursos',416,317,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,529,'Cat�logos-Capacitaci�n-Proveedores de Capacitaci�n',416,318,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,648,'Cat�logos-N�mina-Grupos de Costeo',430,319,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,155,'Cat�logos-Capacitaci�n-Maestros',416,320,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,157,'Cat�logos-Capacitaci�n-Matriz por  Puesto',416,321,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,156,'Cat�logos-Capacitaci�n-Matriz por Curso',416,322,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,592,'Cat�logos-Capacitaci�n-Matriz Certificaciones por Puesto',416,323,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,593,'Cat�logos-Capacitaci�n-Matriz Puestos por Certificaci�n',416,324,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,158,'Cat�logos-Capacitaci�n-Calendario',416,325,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,473,'Cat�logos-Capacitaci�n-Aulas',416,326,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,474,'Cat�logos-Capacitaci�n-Prerrequisitos por Curso',416,327,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,504,'Cat�logos-Capacitaci�n-Certificaciones',416,328,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,620,'Cat�logos-Capacitaci�n-Establecimientos',420,329,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,683,'Cat�logos-Competencias',435,330,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,678,'Cat�logos-Competencias-Competencias',435,331,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,677,'Cat�logos-Competencias-Grupos de Competencias',435,332,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,682,'Cat�logos-Competencias-Matriz de Funciones',435,333,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,159,'Cat�logos-Configuraci�n',416,334,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,160,'Cat�logos-Configuraci�n-Globales de Empresa',416,335,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,488,'Cat�logos-Configuraci�n-Globales de Empresa-Identificaci�n',416,336,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,489,'Cat�logos-Configuraci�n-Globales de Empresa-Areas',416,337,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,490,'Cat�logos-Configuraci�n-Globales de Empresa-Campos Adicionales',416,338,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,491,'Cat�logos-Configuraci�n-Globales de Empresa-Recursos Humanos',416,339,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,492,'Cat�logos-Configuraci�n-Globales de Empresa-N�mina',416,340,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,493,'Cat�logos-Configuraci�n-Globales de Empresa-Asistencia',416,341,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,494,'Cat�logos-Configuraci�n-Globales de Empresa-IMSS/INFONAVIT',416,342,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,495,'Cat�logos-Configuraci�n-Globales de Empresa-Capacitaci�n',416,343,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,496,'Cat�logos-Configuraci�n-Globales de Empresa-Variables globales',416,344,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,497,'Cat�logos-Configuraci�n-Globales de Empresa-Empleados',416,345,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,498,'Cat�logos-Configuraci�n-Globales de Empresa-Cafeter�a',416,346,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,499,'Cat�logos-Configuraci�n-Globales de Empresa-Supervisores',416,347,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,500,'Cat�logos-Configuraci�n-Globales de Empresa-Reportes V�a-Email',416,348,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,501,'Cat�logos-Configuraci�n-Globales de Empresa-Seguridad',416,349,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,502,'Cat�logos-Configuraci�n-Globales de Empresa-Bit�cora',416,350,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,503,'Cat�logos-Configuraci�n-Globales de Empresa-Bloqueo de Asistencia',416,351,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,580,'Cat�logos-Configuraci�n-Globales de Empresa-Enrolamiento de Usuarios',416,352,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,609,'Cat�logos-Configuraci�n-Globales de Empresa-Lista de Dispositivos',416,353,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,634,'Cat�logos-Configuraci�n-Globales de Empresa-Mis Datos',430,354,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,162,'Cat�logos-Configuraci�n-Diccionario de Datos',416,355,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,244,'Cat�logos-Cafeter�a',416,356,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,245,'Cat�logos-Cafeter�a-Reglas',416,357,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,246,'Cat�logos-Cafeter�a-Invitadores',416,358,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,283,'Cat�logos-Herramientas',416,359,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,284,'Cat�logos-Herramientas-Herramientas',416,360,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,285,'Cat�logos-Herramientas-Tallas',416,361,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,286,'Cat�logos-Herramientas-Devoluci�n',416,362,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,164,'Tablas',416,363,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,165,'Tablas-Personales',416,364,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,166,'Tablas-Personales-Estado Civil',416,365,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,167,'Tablas-Personales-En D�nde Vive',416,366,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,168,'Tablas-Personales-Con Qui�n Vive',416,367,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,169,'Tablas-Personales-Grado de Estudios',416,368,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,170,'Tablas-Personales-Transporte',416,369,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,171,'Tablas-Personales-Estados del Pa�s',416,370,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,619,'Tablas-Personales-Municipios',420,371,0,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,422,'Tablas-Personales-Colonias',416,372,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,172,'Tablas-Areas',416,373,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,173,'Tablas-Areas-Nivel 1',416,374,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,174,'Tablas-Areas-Nivel 2',416,375,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,175,'Tablas-Areas-Nivel 3',416,376,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,176,'Tablas-Areas-Nivel 4',416,377,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,177,'Tablas-Areas-Nivel 5',416,378,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,178,'Tablas-Areas-Nivel 6',416,379,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,179,'Tablas-Areas-Nivel 7',416,380,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,180,'Tablas-Areas-Nivel 8',416,381,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,181,'Tablas-Areas-Nivel 9',416,382,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,182,'Tablas-Adicionales',416,383,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,183,'Tablas-Adicionales-Adicional 1',416,384,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,184,'Tablas-Adicionales-Adicional 2',416,385,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,185,'Tablas-Adicionales-Adicional 3',416,386,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,186,'Tablas-Adicionales-Adicional 4',416,387,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,426,'Tablas-Adicionales-Adicional 5',416,388,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,427,'Tablas-Adicionales-Adicional 6',416,389,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,428,'Tablas-Adicionales-Adicional 7',416,390,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,429,'Tablas-Adicionales-Adicional 8',416,391,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,430,'Tablas-Adicionales-Adicional 9',416,392,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,431,'Tablas-Adicionales-Adicional 10',416,393,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,432,'Tablas-Adicionales-Adicional 11',416,394,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,433,'Tablas-Adicionales-Adicional 12',416,395,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,434,'Tablas-Adicionales-Adicional 13',416,396,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,435,'Tablas-Adicionales-Adicional 14',416,397,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,187,'Tablas-Historial',416,398,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,188,'Tablas-Historial-Tipo de Kardex',416,399,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,189,'Tablas-Historial-Motivo de Baja',416,400,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,190,'Tablas-Historial-Incidencias',416,401,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,266,'Tablas-Historial-Motivos de Autorizaci�n',416,402,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,613,'Tablas-Historial-Motivos Checadas Manuales',416,403,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,632,'Tablas-Historial-Motivos de Transferencias',425,404,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,192,'Tablas-N�mina',416,405,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,193,'Tablas-N�mina-Tipo de Ahorro',416,406,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,194,'Tablas-N�mina-Tipo de Pr�stamo',416,407,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,195,'Tablas-N�mina-Monedas',416,408,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,604,'Tablas-N�mina-Reglas de Pr�stamos',416,409,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,196,'Tablas-Oficiales',416,410,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,197,'Tablas-Oficiales-Salarios M�nimos',416,411,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,198,'Tablas-Oficiales-Cuotas de IMSS',416,412,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,199,'Tablas-Oficiales-ISPT y Num�ricas',416,413,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,200,'Tablas-Oficiales-Grados de Riesgo',416,414,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,267,'Tablas-Oficiales-Tipos de Cambio',416,415,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,614,'Tabla-Oficiales-STPS',420,416,26,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,699,'Tablas-Oficiales-Cat�logo Nacional de Competencias',435,417,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,615,'Tabla-Oficiales-STPS-Cat�logo Nacional de Ocupaciones',420,418,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,616,'Tabla-Oficiales-STPS-�reas Tem�ticas de Cursos',420,419,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,715,'Tablas-Oficiales-Cat�logo de Bancos',440,420,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,681,'Tablas-Competencias',435,421,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,675,'Tablas-Competencias-Tipos de Competencias',435,422,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,676,'Tablas-Competencias-Tipos de Grupos de Competencias',435,423,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,201,'Sistema',416,424,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,202,'Sistema-Datos',416,425,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,203,'Sistema-Datos-Empresas',416,426,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,205,'Sistema-Datos-Grupos de Usuarios',416,427,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,581,'Sistema-Datos-Roles',416,428,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,204,'Sistema-Datos-Usuarios',416,429,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,206,'Sistema-Datos-Poll Pendientes',416,430,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,249,'Sistema-Datos-Impresoras',416,431,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,282,'Sistema-Datos-Confidencialidad',416,432,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,526,'Sistema-Datos-Bit�cora de Sistema',416,433,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,608,'Sistema-Datos-Lista de Dispositivos',416,434,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,207,'Sistema-Registro',416,435,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,208,'Sistema-Registro-Empresa Nueva',416,436,26,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,210,'Sistema-Procesos',416,437,26,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,212,'Sistema-Procesos-Borrar Bajas',416,438,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,232,'Sistema-Procesos-Borrar N�minas',416,439,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,213,'Sistema-Procesos-Borrar Tarjetas',416,440,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,273,'Sistema-Procesos-Borrar POLL',416,441,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,277,'Sistema-Procesos-Borrar Bit�cora',416,442,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,294,'Sistema-Procesos-Borrar Herramienta',416,443,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,403,'Sistema-Procesos-Actualizar N�mero de Tarjeta',416,444,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,582,'Sistema-Procesos-Enrolamiento Masivo',416,445,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,583,'Sistema-Procesos-Importar Enrolamiento Masivo',416,446,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,642,'Sistema-Procesos-Asignaci�n de N�meros Biom�tricos',430,447,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,655,'Sistema-Procesos-Asignaci�n de Grupo de Dispositivos',430,448,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,719,'Sistema-Procesos-Borrar Timbrado',440,449,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,717,'Sistema-Procesos-Actualizaci�n de Base de Datos',440,450,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,368,'Sistema-Activar Usuarios',416,451,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,369,'Sistema-Bloquear Usuarios',416,452,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,370,'Sistema-Asignar Supervisores',416,453,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,371,'Sistema-Asignar Areas',416,454,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,651,'Sistema-Datos-Lista de Grupos',430,455,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,163,'Supervisores',416,456,5,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,252,'Supervisores-Ajustar Asistencia',416,457,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,260,'Supervisores-Autorizaciones de Asistencia',416,458,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,253,'Supervisores-Permisos en D�as',416,459,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,279,'Supervisores-Clase de Permisos',416,460,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,254,'Supervisores-Kardex de Empleados',416,461,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,404,'Supervisores-Vacaciones',416,462,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,410,'Supervisores-Clasificaciones',416,463,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,411,'Supervisores-Clasificaciones-Temporales',416,464,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,412,'Supervisores-Clasificaciones-Tipos',416,465,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,255,'Supervisores-Consultar Pre-N�mina',416,466,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,256,'Supervisores-Bit�cora',416,467,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,257,'Supervisores-Asignar Empleados',416,468,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,258,'Supervisores-Datos del Empleado',416,469,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,562,'Supervisores-Cursos Tomados',416,470,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,563,'Supervisores-Cursos Programados',416,471,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,564,'Supervisores-Grupos',416,472,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,565,'Supervisores-Grupos-Grupos de otros supervisores',416,473,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,280,'Supervisores-Labor',416,474,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,281,'Supervisores-C�dulas',416,475,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,295,'Supervisores-Asignar Area de Empleados',416,476,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,263,'Supervisores-Tipo de Kardex',416,477,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,261,'Supervisores-Procesos',416,478,5,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,262,'Supervisores-Procesos-Autorizaci�n Colectiva',416,479,5,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,322,'Supervisores-Procesos-Registro de Breaks',416,480,5,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,323,'Supervisores-Procesos-Cancelaci�n de Breaks',416,481,5,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,405,'Supervisores-Procesos-Cambio Masivo de Turnos',416,482,5,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,475,'Supervisores-Procesos-Autorizar Pren�mina',416,483,5,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,656,'Supervisores-Procesos-Aprobar/Cancelar Transferencias',430,484,5,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,248,'Supervisores-Reportes',416,485,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,324,'Supervisores-Archivos Temporales',416,486,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,272,'Supervisores-Ver Empleados Dados de BAJA',416,487,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,278,'Supervisores-Ver Todos Los Empleados',416,488,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,509,'Supervisores-Solicitud de vacaciones',416,489,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,623,'Supervisores-Plantillas de Producci�n',416,490,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,646,'Supervisores-Transferencias',430,491,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,696,'Supervisores-Plan de Capacitaci�n',435,492,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,698,'Supervisores-Competencias Evaluadas',435,493,5,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,325,'Labor',416,494,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,326,'Labor-Empleados',416,495,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,327,'Labor-Empleados-Datos',416,496,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,328,'Labor-Empleados-Operaciones',416,497,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,329,'Labor-Empleados-Ordenes Fijas',416,498,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,330,'Labor-Empleados-Kardex de Areas',416,499,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,331,'Labor-Labor',416,500,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,332,'Labor-Labor-Orden de Trabajo',416,501,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,333,'Labor-Labor-Kardex de Orden de Trabajo',416,502,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,334,'Labor-Labor-Historial de Orden de Trabajo',416,503,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,343,'Labor-Labor-C�dulas',416,504,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,335,'Labor-Labor-Registro',416,505,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,336,'Labor-Labor-Registro-Multi-Lote',416,506,7,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,337,'Labor-Labor-Procesos',416,507,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,338,'Labor-Labor-Procesos-C�lculo de Tiempos',416,508,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,376,'Labor-Labor-Procesos-Afectar Labor',416,509,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,339,'Labor-Labor-Procesos-Importar Ordenes de Trabajo',416,510,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,340,'Labor-Labor-Procesos-Importar Partes',416,511,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,379,'Labor-Labor-Procesos-Importar Componentes',416,512,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,400,'Labor-Labor-Procesos-Importar C�dulas',416,513,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,401,'Labor-Labor-Procesos-Importar Cambio de Area',416,514,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,402,'Labor-Labor-Procesos-Importar Producci�n Individual',416,515,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,341,'Labor-Labor-Procesos-Depuraci�n',416,516,7,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,624,'Labor-Labor-Plantillas Producci�n',416,517,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,364,'Labor-Calidad',416,518,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,363,'Labor-Calidad-C�dulas de Inspecci�n',416,519,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,365,'Labor-Calidad-C�dulas de Scrap',416,520,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,362,'Labor-Calidad-Clase de defecto',416,521,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,377,'Labor-Calidad-Componentes',416,522,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,378,'Labor-Calidad-Motivos de Scrap',416,523,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,342,'Labor-Consultas',416,524,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,276,'Labor-Consultas-Reportes',416,525,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,356,'Labor-Consultas-SQL',416,526,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,357,'Labor-Consultas-Bit�cora de Procesos',416,527,7,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,344,'Labor-Cat�logos',416,528,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,345,'Labor-Cat�logos-Partes',416,529,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,346,'Labor-Cat�logos-Operaciones',416,530,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,347,'Labor-Cat�logos-Clase de Partes',416,531,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,348,'Labor-Cat�logos-Clase de Operaciones',416,532,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,349,'Labor-Cat�logos-Areas',416,533,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,625,'Labor-Cat�logos-M�quinas',416,534,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,626,'Labor-Cat�logos-Clase de M�quinas',416,535,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,350,'Labor-Cat�logos-Tiempo Muerto',416,536,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,351,'Labor-Cat�logos-Breaks',416,537,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,352,'Labor-Cat�logos-Modulador #1',416,538,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,353,'Labor-Cat�logos-Modulador #2',416,539,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,354,'Labor-Cat�logos-Modulador #3',416,540,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,355,'Labor-Cat�logos-Globales',416,541,7,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,319,'Servicio M�dico',416,542,15,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,297,'Servicio M�dico-Pacientes',416,543,15,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,298,'Servicio M�dico-Pacientes-Expediente',416,544,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,299,'Servicio M�dico-Pacientes-Kardex de Consultas',416,545,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,300,'Servicio M�dico-Pacientes-Kardex de Embarazos',416,546,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,320,'Servicio M�dico-Pacientes-Medicina Entregada',416,547,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,301,'Servicio M�dico-Pacientes-Kardex de Accidentes/Enfermedades de Trabajo',416,548,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,314,'Servicio M�dico-Pacientes-Kardex de Incapacidades - Permisos',416,549,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,459,'Servicio M�dico-Pacientes-Kardex de Incapacidades - Permisos-Incapacidades',416,550,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,458,'Servicio M�dico-Pacientes-Kardex de Incapacidades - Permisos-Permisos',416,551,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,315,'Servicio M�dico-Pacientes-Clases de Permisos',416,552,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,302,'Servicio M�dico-Pacientes-Registro',416,553,15,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,313,'Servicio M�dico-Pacientes-Registro-Consulta M�dica Global',416,554,15,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,316,'Servicio M�dico-Consultas Del Sistema',416,555,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,321,'Servicio M�dico-Consultas Del Sistema-Reportes',416,556,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,317,'Servicio M�dico-Consultas Del Sistema-SQL',416,557,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,318,'Servicio M�dico-Consultas Del Sistema-Bit�cora de Procesos',416,558,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,304,'Servicio M�dico-Cat�logos',416,559,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,305,'Servicio M�dico-Cat�logos-Medicamentos',416,560,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,306,'Servicio M�dico-Cat�logos-Diagn�sticos',416,561,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,307,'Servicio M�dico-Cat�logos-Tipos de Consulta',416,562,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,308,'Servicio M�dico-Cat�logos-Tipos de Estudio de Laboratorio',416,563,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,309,'Servicio M�dico-Cat�logos-Tipos de Accidente',416,564,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,310,'Servicio M�dico-Cat�logos-Causas de Accidente',416,565,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,359,'Servicio M�dico-Cat�logos-Motivos de Accidente',416,566,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,311,'Servicio M�dico-Sistema',416,567,15,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,312,'Servicio M�dico-Sistema-Globales de Servicios M�dicos',416,568,15,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,380,'Plan Carrera Admin.',416,569,18,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,381,'Plan Carrera Admin.-Cat�logos',416,570,18,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,382,'Plan Carrera Admin.-Cat�logos-Tipo de Competencias',416,571,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,383,'Plan Carrera Admin.-Cat�logos-Calificaciones',416,572,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,384,'Plan Carrera Admin.-Cat�logos-Competencias / Habilidades',416,573,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,385,'Plan Carrera Admin.-Cat�logos-Acciones',416,574,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,386,'Plan Carrera Admin.-Cat�logos-Puestos',416,575,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,387,'Plan Carrera Admin.-Cat�logos-Familias',416,576,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,388,'Plan Carrera Admin.-Cat�logos-Niveles',416,577,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,389,'Plan Carrera Admin.-Cat�logos-Dimensiones',416,578,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,390,'Plan Carrera Admin.-Consultas',416,579,18,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,391,'Plan Carrera Admin.-Consultas-Reportes',416,580,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,413,'Plan Carrera Admin.-Sistema',416,581,18,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,414,'Plan Carrera Admin.-Sistema-Globales de Plan Carrera',416,582,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,415,'Plan Carrera',416,583,18,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,416,'Plan Carrera-Mi Plan Carrera',416,584,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,417,'Plan Carrera-Puestos',416,585,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,418,'Plan Carrera-Cat�logos',416,586,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,419,'Plan Carrera-Detalle de Cat�logos',416,587,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,420,'Plan Carrera-Anal�ticos',416,588,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,421,'Plan Carrera-Mis Colaboradores',416,589,18,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,425,'Administrador Evaluaci�n/Encuestas',416,590,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,436,'Administrador Evaluaci�n/Encuestas-Dise�o de Encuesta',416,591,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,437,'Administrador Evaluaci�n/Encuestas-Dise�o de Encuesta-Criterios a evaluar',416,592,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,438,'Administrador Evaluaci�n/Encuestas-Dise�o de Encuesta-Preguntas por criterio',416,593,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,439,'Administrador Evaluaci�n/Encuestas-Dise�o de Encuesta-Escalas de Encuesta',416,594,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,440,'Administrador Evaluaci�n/Encuestas-Dise�o de Encuesta-Perfil de evaluadores',416,595,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,441,'Administrador Evaluaci�n/Encuestas-Dise�o de Encuesta-Empleados a evaluar',416,596,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,443,'Administrador Evaluaci�n/Encuestas-Registro',416,597,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,476,'Administrador Evaluaci�n/Encuestas-Registro-Agregar empleados a evaluar',416,598,21,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,444,'Administrador Evaluaci�n/Encuestas-Procesos',416,599,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,477,'Administrador Evaluaci�n/Encuestas-Procesos-Generar lista de empleados',416,600,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,478,'Administrador Evaluaci�n/Encuestas-Procesos-Preparar Evaluaciones',416,601,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,479,'Administrador Evaluaci�n/Encuestas-Procesos-Calcular resultados',416,602,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,480,'Administrador Evaluaci�n/Encuestas-Procesos-Recalcular promedios',416,603,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,482,'Administrador Evaluaci�n/Encuestas-Procesos-Envia correos de invitaci�n',416,604,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,483,'Administrador Evaluaci�n/Encuestas-Procesos-Envia correos de notificaci�n',416,605,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,485,'Administrador Evaluaci�n/Encuestas-Procesos-Asignaci�n de relaciones',416,606,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,487,'Administrador Evaluaci�n/Encuestas-Procesos-Crear encuesta simple',416,607,21,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,445,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta',416,608,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,446,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta-Totales de Encuesta',416,609,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,447,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta-Empleados evaluados',416,610,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,448,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta-Evaluadores',416,611,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,449,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta-Evaluaciones',416,612,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,450,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta-An�lisis por Criterio',416,613,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,451,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta-An�lisis por pregunta',416,614,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,452,'Administrador Evaluaci�n/Encuestas-Resultados de Encuesta-Frecuencia de respuestas',416,615,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,453,'Administrador Evaluaci�n/Encuestas-Resultados individuales',416,616,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,454,'Administrador Evaluaci�n/Encuestas-Resultados individuales-Resultados por criterio',416,617,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,455,'Administrador Evaluaci�n/Encuestas-Resultados individuales-Resultados por pregunta',416,618,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,456,'Administrador Evaluaci�n/Encuestas-Consultas',416,619,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,461,'Administrador Evaluaci�n/Encuestas-Consultas-Reportes',416,620,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,462,'Administrador Evaluaci�n/Encuestas-Consultas-SQL',416,621,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,463,'Administrador Evaluaci�n/Encuestas-Consultas-Bit�cora de Procesos',416,622,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,481,'Administrador Evaluaci�n/Encuestas-Consultas-Correos',416,623,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,464,'Administrador Evaluaci�n/Encuestas-Cat�logos',416,624,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,467,'Administrador Evaluaci�n/Encuestas-Cat�logos-Tipos de competencia',416,625,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,465,'Administrador Evaluaci�n/Encuestas-Cat�logos-Competencias',416,626,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,466,'Administrador Evaluaci�n/Encuestas-Cat�logos-Preguntas',416,627,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,468,'Administrador Evaluaci�n/Encuestas-Cat�logos-Escalas',416,628,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,442,'Administrador Evaluaci�n/Encuestas-Cat�logos-Encuestas',416,629,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,469,'Administrador Evaluaci�n/Encuestas-Sistema',416,630,21,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,484,'Administrador Evaluaci�n/Encuestas-Sistema-Usuarios',416,631,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,470,'Administrador Evaluaci�n/Encuestas-Sistema-Globales de Empresa',416,632,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,486,'Administrador Evaluaci�n/Encuestas-Sistema-Diccionario de Datos',416,633,21,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,538,'Caja de Ahorro',416,634,27,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,539,'Caja de Ahorro-Administraci�n',416,635,27,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,540,'Caja de Ahorro-Administraci�n-Saldos de Empleado',416,636,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,541,'Caja de Ahorro-Administraci�n-Totales de Caja de Ahorro',416,637,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,542,'Caja de Ahorro-Administraci�n-Estado de Cuenta Bancaria',416,638,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,543,'Caja de Ahorro-Registro',416,639,27,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,544,'Caja de Ahorro-Registro-Inscribir Empleado',416,640,27,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,545,'Caja de Ahorro-Registro-Liquidaci�n/Retiro',416,641,27,1,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,547,'Caja de Ahorro-Procesos',416,642,27,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,548,'Caja de Ahorro-Procesos-Depositar Retenciones',416,643,27,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,549,'Caja de Ahorro-Consultas',416,644,27,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,518,'Caja de Ahorro-Consultas-Reportes',416,645,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,550,'Caja de Ahorro-Consultas-SQL',416,646,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,551,'Caja de Ahorro-Consultas-Bit�cora / Procesos',416,647,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,552,'Caja de Ahorro-Cat�logos',416,648,27,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,553,'Caja de Ahorro-Cat�logos-Cuentas Bancarias',416,649,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,554,'Caja de Ahorro-Cat�logos-Tipo de Dep�sito/Retiro',416,650,27,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,555,'Presupuestos de n�mina',416,651,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,556,'Presupuestos de n�mina-Configuraci�n',416,652,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,557,'Presupuestos de n�mina-Configuraci�n-Contrataciones',416,653,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,628,'Presupuestos de n�mina-Configuraci�n-Escenarios',416,654,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,558,'Presupuestos de n�mina-Configuraci�n-Supuestos de personal',416,655,1,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,559,'Presupuestos de n�mina-Procesos',416,656,1,7,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,560,'Presupuestos de n�mina-Procesos-Simulaci�n de presupuestos',416,657,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,561,'Presupuestos de n�mina-Procesos-Imprimir simulaci�n',416,658,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,673,'Presupuestos de n�mina-Procesos-Importar Kardex',435,659,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,700,'Presupuestos de n�mina-Procesos-Generar P�liza Contable',435,660,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,701,'Presupuestos de n�mina-Procesos-Imprimir P�liza Contable',435,661,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,702,'Presupuestos de n�mina-Procesos-Exportar P�liza Contable',435,662,1,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,643,'Sistema-Datos-Bit�cora de Registros Biom�tricos',430,663,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,644,'Sistema-Procesos-Borrar Bit�cora de Registros Biom�tricos',430,664,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,652,'Sistema-Datos-Dispositivos por Grupo',430,665,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,708,'Sistema-Datos-Bases de Datos',440,666,26,0,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,709,'Sistema-Procesos-Agregar BD de tipo Tress',440,667,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,710,'Sistema-Procesos-Agregar BD de tipo Pruebas',440,668,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,711,'Sistema-Procesos-Agregar BD de tipo Selecci�n',440,669,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,712,'Sistema-Procesos-Agregar BD de tipo Visitantes',440,670,26,2,'')
		INSERT INTO ACC_ARBOL( AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO, AA_MODULO, AA_SCREEN, AA_INGLES) 	VALUES(0,713,'Sistema-Procesos-Agregar BD de tipo Presupuestos',440,671,26,2,'')


		--Inserciones ACC_DER--
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	715	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	715	,	1	,	'Agregar'	,	0	,	1	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	715	,	2	,	'Borrar'	,	0	,	2	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	715	,	3	,	'Modificar'	,	0	,	3	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	715	,	4	,	'Imprimir'	,	0	,	4	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	1	,	'Agregar'	,	0	,	1	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	2	,	'Borrar'	,	0	,	2	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	3	,	'Modificar'	,	0	,	3	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	4	,	'Imprimir'	,	0	,	4	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	5	,	'Exportar Tablas XML'	,	0	,	5	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	6	,	'Importar Tablas XML'	,	0	,	6	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	708	,	7	,	'Importar Tablas CSV'	,	0	,	7	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	719	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	713	,	0	,	'Consultar'	,	0	,	0	,''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	712	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	711	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	710	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	709	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	717	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	716	,	0	,	'Consultar'	,	0	,	0	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	716	,	3	,	'Modificar'	,	0	,	3	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	716	,	4	,	'Imprimir'	,	0	,	4	,	''	)
		INSERT INTO ACC_DER( AA_SOURCE,AX_NUMERO,AD_TIPO,AD_ACCION, AD_IMPACTO, AD_POSICIO, AD_INGLES) 
		Values(	0	,	714	,	0	,	'Consultar'	,	0	,	0	,	''	)

	END
end
GO

/* Patch 440: Agregar registros en ACC_ARBOL y ACC_DER a partir de COMPANY */
/* B. Ejecutar SP */
execute ADD_DERECHOS
GO

/* Patch 440: Agregar registros en ACC_ARBOL y ACC_DER a partir de COMPANY */
/* C. Eliminar SP */
DROP PROCEDURE ADD_DERECHOS
GO






