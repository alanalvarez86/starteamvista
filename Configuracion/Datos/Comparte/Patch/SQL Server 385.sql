/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Tabla GLOBAL: Globales */
/* Patch 385 # Seq: 1 Agregado */
CREATE TABLE GLOBAL (
       GL_CODIGO            FolioChico,
       GL_DESCRIP           Descripcion,
       GL_FORMULA           Formula,
       GL_TIPO              Status,
       US_CODIGO            Usuario,
       GL_CAPTURA           Fecha,
       GL_NIVEL             Status )
GO

/* PK GLOBAL: Llave Primaria */
/* Patch 385 # Seq: 2 Agregado */
ALTER TABLE GLOBAL ADD CONSTRAINT PK_GLOBAL PRIMARY KEY (GL_CODIGO)
GO

/* Tabla PAGINA: Formatos de P�ginas WEB */
/* Patch 385 # Seq: 3 Agregado */
CREATE TABLE PAGINA (
       US_CODIGO            Usuario,
       PA_ORDEN             FolioChico,
       PA_TITULO            Descripcion,
       PA_COLS              FolioChico )
GO

/* PK PAGINA: Llave Primaria */
/* Patch 385 # Seq: 4 Agregado */
ALTER TABLE PAGINA ADD CONSTRAINT PK_PAGINA PRIMARY KEY (US_CODIGO, PA_ORDEN)
GO

/* Tabla CONTIENE: Contenido de P�ginas WEB */
/* Patch 385 # Seq: 5 Agregado */
CREATE TABLE CONTIENE (
       US_CODIGO            Usuario,
       PA_ORDEN             FolioChico,
       CT_COLUMNA           FolioChico,
       CT_ORDEN             FolioChico,
       CT_TIPO              Status,
       CT_MOSTRAR           FolioChico,
       CT_REPORTE           FolioGrande,
       CT_CLASIFI           Codigo )
GO

/* PK CONTIENE: Llave Primaria */
/* Patch 385 # Seq: 6 Agregado */
ALTER TABLE CONTIENE ADD CONSTRAINT PK_CONTIENE PRIMARY KEY (US_CODIGO, PA_ORDEN, CT_COLUMNA, CT_ORDEN)
GO

/* Tabla REPORTAL: Reportes Ad-Hoc De Portal */
/* Patch 385 # Seq: 7 Agregado */
CREATE TABLE REPORTAL (
       RP_FOLIO             FolioGrande,
       RP_TITULO            Titulo,
       RP_DESCRIP           Memo,
       RP_FECHA             Fecha,
       RP_HORA              Hora,
       RP_IMAGEN            Ruta_URL,
       RP_USUARIO           Usuario,
       RP_TIPO              Status,
       RP_CLASIFI           Codigo,
       RP_COLS              FolioChico,
       RP_ROWS              FolioGrande )
GO

/* PK REPORTAL: Llave Primaria */
/* Patch 385 # Seq: 8 Agregado */
ALTER TABLE REPORTAL ADD CONSTRAINT PK_REPORTAL PRIMARY KEY (RP_FOLIO)
GO

/* Tabla REP_DATA: Datos de Reportes Ad-Hoc */
/* Patch 385 # Seq: 9 Agregado */
CREATE TABLE REP_DATA (
       RP_FOLIO             FolioGrande,
       RD_ORDEN             FolioGrande,
       RD_DATO1             DatoReporte,
       RD_DATO2             DatoReporte,
       RD_DATO3             DatoReporte,
       RD_DATO4             DatoReporte,
       RD_DATO5             DatoReporte,
       RD_DATO6             DatoReporte,
       RD_DATO7             DatoReporte,
       RD_DATO8             DatoReporte,
       RD_DATO9             DatoReporte,
       RD_DATO10            DatoReporte )
GO

/* PK REP_DATA: Llave Primaria */
/* Patch 385 # Seq: 10 Agregado */
ALTER TABLE REP_DATA ADD CONSTRAINT PK_REP_DATA PRIMARY KEY (RP_FOLIO, RD_ORDEN)
GO

/* Tabla REP_COLS: Columnas de Reportes Ad-Hoc */
/* Patch 385 # Seq: 11 Agregado */
CREATE TABLE REP_COLS (
       RP_FOLIO             FolioGrande,
       RC_ORDEN             FolioChico,
       RC_TITULO            Descripcion,
       RC_TIPO              Status,
       RC_ANCHO             FolioChico )
GO

/* PK REP_COLS: Llave Primaria */
/* Patch 385 # Seq: 12 Agregado */
ALTER TABLE REP_COLS ADD CONSTRAINT PK_REP_COLS PRIMARY KEY (RP_FOLIO, RC_ORDEN)
GO

/* Tabla BUZONSUG: Buz�n de Sugerencias */
/* Patch 385 # Seq: 13 Agregado */
CREATE TABLE BUZONSUG (
       BU_FOLIO             FolioGrande,
       BU_FECHA             Fecha,
       BU_HORA              Hora,
       BU_MEMO              Memo,
       BU_TITULO            Titulo,
       BU_USUARIO           Usuario,
       BU_AUTOR             Descripcion,
       BU_EMAIL             Titulo )
GO

/* PK BUZONSUG: Llave Primaria */
/* Patch 385 # Seq: 14 Agregado */
ALTER TABLE BUZONSUG ADD CONSTRAINT PK_BUZONSUG PRIMARY KEY (BU_FOLIO)
GO

/* Tabla TDOCUMEN: CLases de Documentos */
/* Patch 385 # Seq: 15 Agregado */
CREATE TABLE TDOCUMEN (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

/* PK TDOCUMEN: Llave Primaria */
/* Patch 385 # Seq: 16 Agregado */
ALTER TABLE TDOCUMEN ADD CONSTRAINT PK_TDOCUMEN PRIMARY KEY(TB_CODIGO)
GO

/* Tabla TEVENTO: Clases de Eventos */
/* Patch 385 # Seq: 17 Agregado */
CREATE TABLE TEVENTO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

/* PK TEVENTO: Llave Primaria */
/* Patch 385 # Seq: 18 Agregado */
ALTER TABLE TEVENTO ADD CONSTRAINT PK_TEVENTO PRIMARY KEY (TB_CODIGO)
GO

/* Tabla TNOTICIA: Clases de Noticias */
/* Patch 385 # Seq: 19 Agregado */
CREATE TABLE TNOTICIA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Formula,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

/* PK TNOTICIA: Llave Primaria */
/* Patch 385 # Seq: 20 Agregado */
ALTER TABLE TNOTICIA ADD CONSTRAINT PK_TNOTICIA PRIMARY KEY (TB_CODIGO)
GO

/* Tabla DOCUMENT: Documentos */
/* Patch 385 # Seq: 21 Agregado */
CREATE TABLE DOCUMENT (
       DC_FOLIO             FolioGrande,
       DC_FECHA             Fecha,
       DC_HORA              Hora,
       DC_RUTA              Ruta_URL,
       DC_IMAGEN            Ruta_URL,
       DC_TITULO            Titulo,
       DC_MEMO              Memo,
       DC_USUARIO           Usuario,
       DC_AUTOR             Descripcion,
       DC_CLASIFI           Codigo,
       DC_PALABRA           Formula )
GO

/* PK DOCUMENT: Llave Primaria */
/* Patch 385 # Seq: 22 Agregado */
ALTER TABLE DOCUMENT ADD CONSTRAINT PK_DOCUMENT PRIMARY KEY (DC_FOLIO)
GO

/* Tabla EVENTO: Eventos */
/* Patch 385 # Seq: 23 Agregado */
CREATE TABLE EVENTO (
       EV_FOLIO             FolioGrande,
       EV_FEC_INI           Fecha,
       EV_FEC_FIN           Fecha,
       EV_HORA              Hora,
       EV_LUGAR             Titulo,
       EV_IMAGEN            Ruta_URL,
       EV_MEMO              Memo NOT NULL,
       EV_TITULO            Titulo,
       EV_USUARIO           Usuario,
       EV_CLASIFI           Codigo,
       EV_AUTOR             Descripcion,
       EV_URL               Ruta_URL )
GO

/* PK EVENTO: Llave Primaria */
/* Patch 385 # Seq: 24 Agregado */
ALTER TABLE EVENTO ADD CONSTRAINT PK_EVENTO PRIMARY KEY (EV_FOLIO)
GO

/* Tabla NOTICIA: Noticias */
/* Patch 385 # Seq: 25 Agregado */
CREATE TABLE NOTICIA (
       NT_FOLIO             FolioGrande,
       NT_FECHA             Fecha,
       NT_HORA              Hora,
       NT_TITULO            Titulo,
       NT_MEMO              Memo,
       NT_IMAGEN            Ruta_URL,
       NT_USUARIO           Usuario,
       NT_CLASIFI           Codigo,
       NT_AUTOR             Descripcion,
       NT_CORTA             Memo,
       NT_SUB_TIT           Titulo )
GO

/* PK NOTICIA: Llave Primaria */
/* Patch 385 # Seq: 26 Agregado */
ALTER TABLE NOTICIA ADD CONSTRAINT PK_NOTICIA PRIMARY KEY (NT_FOLIO)
GO

/* Tabla TARJETA: Rolodex */
/* Patch 385 # Seq: 27 Agregado */
CREATE TABLE TARJETA (
       TJ_EMPRESA           DescLarga,
       TJ_NOMBRES           DescLarga,
       TJ_TELEFON           DescLarga,
       TJ_DIRECC1           DescLarga,
       TJ_DIRECC2           DescLarga,
       TJ_COMENTA           DescLarga,
       TJ_EMAIL             DescLarga )
GO

/* Agregar COMPANY.CM_KIOSKO: Identificador de la Empresa en Gafetes de Proximidad */
/* Patch 385: # Seq: 30 Agregado */
alter table COMPANY add CM_KIOSKO Booleano;
GO

/* Agregar USUARIO.US_ANFITRI: # de Anfitri�n */
/* Patch 385: # Seq: 31 Agregado */
alter table USUARIO add US_ANFITRI NumeroEmpleado;
GO

/* Inicializar COMPANY.CM_KIOSKO */
/* Patch 385: # Seq: 32 Agregado */
update COMPANY set CM_KIOSKO = 'N' where ( CM_KIOSKO is NULL )
GO

/* Inicializar USUARIO.US_ANFITRI */
/* Patch 385: # Seq: 33 Agregado */
update USUARIO set US_ANFITRI = 0 where ( US_ANFITRI is NULL )
GO

/* Tabla EMP_ID: Identificadores de Empleados */
/* Patch 385 # Seq: 34 Agregado */
create table EMP_ID(
       ID_NUMERO            Descripcion,      /* # de Identificaci�n */
       CM_CODIGO            CodigoEmpresa,    /* Empresa */
       CB_CODIGO            NumeroEmpleado,   /* Empleado */
       IV_CODIGO            NumeroEmpleado    /* Invitador */
)
GO

/* PK EMP_ID: Llave Primaria */
/* Patch 385 # Seq: 35 Agregado */
ALTER TABLE EMP_ID ADD CONSTRAINT PK_EMP_ID PRIMARY KEY (ID_NUMERO )
GO

/***************************/
/**** Llaves For�neas ******/
/***************************/

/* Llave for�nea de REP_DATA hacia REPORTAL */
/* Patch 385 Seq: 28 Agregado */
alter table REP_DATA
       add constraint FK_REP_DATA_REPORTAL
       foreign key (RP_FOLIO)
       references REPORTAL
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de REP_COLS hacia REPORTAL */
/* Patch 385 Seq: 29 Agregado */
alter table REP_COLS
       add constraint FK_REP_COLS_REPORTAL
       foreign key (RP_FOLIO)
       references REPORTAL
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de EMP_ID hacia COMPANY */
/* Patch 385 Seq: 36 Agregado */
ALTER TABLE EMP_ID
       ADD CONSTRAINT FK_EMP_ID_COMPANY
       FOREIGN KEY (CM_CODIGO)
       REFERENCES COMPANY
       ON DELETE CASCADE
       ON UPDATE CASCADE
go

/***********************************/
/******* Triggers ******************/
/***********************************/

/* Patch 385 # Seq: 5 Agregado */
CREATE TRIGGER TU_TDOCUMEN ON TDOCUMEN AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
	      declare @NewTB_CODIGO Codigo, @OldTB_CODIGO Codigo;

	      select @NewTB_CODIGO = TB_CODIGO from Inserted;
          select @OldTB_CODIGO = TB_CODIGO from Deleted;

          update DOCUMENT set DOCUMENT.DC_CLASIFI = @NewTB_CODIGO
          where ( DOCUMENT.DC_CLASIFI = @OldTB_CODIGO );
     END
END
GO

/* Patch 385 # Seq: 6 Agregado */
CREATE TRIGGER TU_TEVENTO ON TEVENTO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
	      declare @NewTB_CODIGO Codigo, @OldTB_CODIGO Codigo;

	      select @NewTB_CODIGO = TB_CODIGO from Inserted;
          select @OldTB_CODIGO = TB_CODIGO from Deleted;

          update EVENTO set EVENTO.EV_CLASIFI = @NewTB_CODIGO
          where ( EVENTO.EV_CLASIFI = @OldTB_CODIGO );
     END
END
GO

/* Patch 385 # Seq: 7 Agregado */
CREATE TRIGGER TU_TNOTICIA ON TNOTICIA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
	      declare @NewTB_CODIGO Codigo, @OldTB_CODIGO Codigo;

	      select @NewTB_CODIGO = TB_CODIGO from Inserted;
          select @OldTB_CODIGO = TB_CODIGO from Deleted;

          update NOTICIA set NOTICIA.NT_CLASIFI = @NewTB_CODIGO
          where ( NOTICIA.NT_CLASIFI = @OldTB_CODIGO );
     END
END
GO

/* Patch 385 # Seq: 8 Agregado */
CREATE TRIGGER TD_USUARIO ON USUARIO AFTER DELETE AS
BEGIN
     declare @OldUS_CODIGO Usuario;
     select @OldUS_CODIGO = US_CODIGO from Deleted;
     delete from PAGINA where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
END
GO

/* Patch 385 # Seq: 9 Agregado */
CREATE TRIGGER TU_USUARIO ON USUARIO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( US_CODIGO )
     BEGIN
	      declare @NewUS_CODIGO Usuario, @OldUS_CODIGO Usuario;

	      select @NewUS_CODIGO = US_CODIGO from Inserted;
          select @OldUS_CODIGO = US_CODIGO from Deleted;
          update PAGINA set PAGINA.US_CODIGO = @NewUS_CODIGO
          where ( PAGINA.US_CODIGO = @OldUS_CODIGO );
     end
END
GO

/* Patch 385 # Seq: 10 Agregado */
CREATE TRIGGER TD_PAGINA ON PAGINA AFTER DELETE AS
BEGIN
     declare @OldUS_CODIGO Usuario;
     declare @OldPA_ORDEN FolioChico;
     select @OldUS_CODIGO = US_CODIGO, @OldPA_ORDEN = PA_ORDEN from Deleted;
     delete from CONTIENE where ( CONTIENE.US_CODIGO = @OldUS_CODIGO ) and
                                ( CONTIENE.PA_ORDEN = @OldPA_ORDEN );
END
GO

/* Patch 385 # Seq: 11 Agregado */
CREATE TRIGGER TU_PAGINA ON PAGINA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( US_CODIGO ) or UPDATE( PA_ORDEN )
     BEGIN
	      declare @NewUS_CODIGO Usuario, @OldUS_CODIGO Usuario;
          declare @NewPA_ORDEN FolioChico, @OldPA_ORDEN FolioChico;

	      select @NewUS_CODIGO = US_CODIGO, @NewPA_ORDEN = PA_ORDEN from Inserted;
          select @OldUS_CODIGO = US_CODIGO, @OldPA_ORDEN = PA_ORDEN from Deleted;
          update CONTIENE set
                 CONTIENE.US_CODIGO = @NewUS_CODIGO,
                 CONTIENE.PA_ORDEN = @NewPA_ORDEN
          where ( CONTIENE.US_CODIGO = @OldUS_CODIGO ) and
                ( CONTIENE.PA_ORDEN = @OldPA_ORDEN );
     end
END
GO

/****************************************/
/****************************************/
/****** Fin de Patch para COMPARTE ******/
/****************************************/
/****************************************/

