/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 474 */
--
GO
IF not EXISTS (select * from syscolumns where id=object_id('COMPANY') and name='CM_WFEMPID') 
begin
	ALTER TABLE COMPANY  ADD CM_WFEMPID FolioGrande null;  
end
GO
   update COMPANY set CM_WFEMPID = 0 where CM_WFEMPID is null 
GO
   ALTER TABLE COMPANY  ALTER COLUMN CM_WFEMPID FolioGrande not null
GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFEmpresa_Consulta') AND type IN ( N'P', N'PC' ))
	drop procedure WFEmpresa_Consulta

GO
create procedure WFEmpresa_Consulta
as
begin 	
	
	select CM_CODIGO, CM_NOMBRE, CM_USRNAME, CM_PASSWRD, CM_DATOS, CM_CONTROL, CM_WFEMPID
	from COMPANY where CM_NIVEL0 = '' and CM_CONTROL in ( '', '3DATOS')   
end 

GO 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFEmpresa_SetEmpresaID') AND type IN ( N'P', N'PC' ))
	drop procedure WFEmpresa_SetEmpresaID
GO
create procedure WFEmpresa_SetEmpresaID( @CM_CodiGO CodiGOEmpresa,  @EmpresaID FolioGrande , @US_CODIGO Usuario , @Error varchar(255) output) 
as
begin 
       set nocount on 
       
       if (( select count(*) from COMPANY where CM_WFEMPID  = @EmpresaID  ) > 0)   and (@EmpresaID > 0) and (@CM_CodiGO <> ( select CM_CodiGO from COMPANY where CM_WFEMPID  = @EmpresaID  )) 
       begin 
             set @Error = 'El Identificador Workflow '+LTRIM( STR(@EmpresaID) ) +' ya fue asignado a otra empresa'
             return 100
       end 
       else
       begin 
             update COMPANY set CM_WFEMPID = @EmpresaID where CM_CODIGO = @CM_CodiGO
       end 
       
       return 0     

end 

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFUsuario_Consulta') AND type IN ( N'P', N'PC' ))
	drop procedure WFUsuario_Consulta

GO


create procedure WFUsuario_Consulta
as
begin 	
	select US_CODIGO, US_CORTO, RTRIM( str(US_CODIGO)) + ' - ' + US_NOMBRE  as US_NOMBRE, US_ACTIVO, US_PASSWRD from USUARIO
end 
GO



IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'T_WFConfig')) 
begin
	CREATE TABLE T_WFConfig ( 
		ConfigTipo status, -- 0: Configuración General ,  1:Configuracion Sincronizador , -- 2: Configuracion Outbox
		ConfigXML XML, 
		ConfigFecha Fecha,
		ConfigUsuario Usuario
	) 
end 

go 


IF ( not EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE  TABLE_NAME = 'T_WFConfigParams')) 
begin
	CREATE TABLE T_WFConfigParams ( 
		ClienteID  FolioGrande, 
		ParamID FolioChico,
		ParamValue Formula  
	)

end 
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFConfig_Consulta') AND type IN ( N'P', N'PC' ))
	drop procedure WFConfig_Consulta

GO

create procedure WFConfig_Consulta( @ConfigTipo status ) 
as
begin 		
	select top 1 ConfigXML, ConfigFecha, ConfigUsuario 	
	from T_WFConfig
	where ConfigTipo = @ConfigTipo
	 order by ConfigFecha desc 
end 

GO 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFConfig_Update') AND type IN ( N'P', N'PC' ))
	drop procedure WFConfig_Update
GO
create procedure WFConfig_Update( @ConfigTipo status, @ConfigXML XML, @ConfigUsuario Usuario) 
as
begin 
	set nocount on 
	
	if ( select COUNT(*) from T_WFConfig where ConfigTipo = @ConfigTipo ) > 0  
	begin 
		update T_WFConfig set ConfigXML = @ConfigXML , ConfigUsuario = @ConfigUsuario, ConfigFecha = GETDATE() 		
	end 
	else
	begin 
		insert into T_WFConfig ( ConfigTipo, ConfigXML, ConfigUsuario, ConfigFecha ) 
		values ( @ConfigTipo, @ConfigXML, @ConfigUsuario, GETDATE() ) 
	end 
	
	return 0 	

end 

go 	
	
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFConfigParams_Consulta') AND type IN ( N'P', N'PC' ))
	drop procedure WFConfigParams_Consulta

GO

create procedure WFConfigParams_Consulta( @ClienteID FolioGrande ) 
as
begin 		
	select ParamID, ParamValue
	from T_WFConfigParams
	where ClienteID = @ClienteID
	order by ParamID  
end 

GO 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'WFConfigParams_Update') AND type IN ( N'P', N'PC' ))
	drop procedure WFConfigParams_Update
GO
create procedure WFConfigParams_Update( @ClienteID FolioGrande, @ParamID FolioChico, @ParamValue Formula ) 
as
begin 
	set nocount on 
	
	if ( select COUNT(*) from T_WFConfigParams where ClienteID = @ClienteID and ParamID = @ParamID ) > 0  
	begin 
		update T_WFConfigParams set ParamValue = @ParamValue
		where ClienteID = @ClienteID and ParamID = @ParamID		
	end 
	else
	begin 
		insert into T_WFConfigParams ( ClienteID, ParamID, ParamValue ) 
		values ( @ClienteID, @ParamID, @ParamValue ) 
	end 
	
	return 0 	
end 
GO
