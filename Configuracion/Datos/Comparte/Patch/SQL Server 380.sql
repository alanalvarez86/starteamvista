/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Agregar el campo GRUPO.GR_GRP_SUP para indicar el grupo supervisor */
/* Patch 380 # Seq: 1 Agregado */
alter table GRUPO add GR_PADRE FolioChico
GO

/* Fijar el padre del grupo ADMINISTRADOR a CERO*/
/* Patch 380 # Seq: 2 Agregado */
update GRUPO set GR_PADRE = 0 where ( GR_CODIGO = 1 )
GO

/* Fijar el padre de los grupos NO Administradores A ADMINISTRADOR*/
/* Patch 380 # Seq: 3 Agregado */
update GRUPO set GR_PADRE = 1 where ( GR_CODIGO <> 1 ) and  ( ( GR_PADRE is NULL ) or ( GR_PADRE = 0 ) )
GO

/* Portal: Default para CodigoVacio*/
/* Patch 380 # Seq: 4 Agregado */
CREATE DEFAULT CodigoVacio AS ''
go

/* Portal: Domain para Imagenes */
/* Patch 380 # Seq: 5 Agregado */
exec sp_addtype Imagen, 'image', 'NULL'
go

/* Portal: Domain para Memos */
/* Patch 380 # Seq: 6 Agregado */
exec sp_addtype Memo, 'text', 'NULL'
go

/* Portal: Domain para Folios Grandes */
/* Patch 380 # Seq: 7 Agregado */
exec sp_addtype FolioGrande, 'int', 'NOT NULL'
exec sp_bindefault Cero, FolioGrande
go

/* Portal: Domain para Pesos */
/* Patch 380 # Seq: 8 Agregado */
exec sp_addtype Pesos, 'NUMERIC(15,2)', 'NOT NULL'
exec sp_bindefault Cero, Pesos
go

/* Portal: Domain para Reportes */
/* Patch 380 # Seq: 9 Agregado */
exec sp_addtype DatoReporte, 'varchar(50)', 'NOT NULL'
exec sp_bindefault StringVacio, DatoReporte
go

/* Portal: Domain para Reportes */
/* Patch 380 # Seq: 10 Agregado */
exec sp_addtype Formula, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, Formula
go

/* Portal: Domain para Reportes */
/* Patch 380 # Seq: 11 Agregado */
exec sp_addtype HoraMinutosSegundos, 'varchar(8)', 'NOT NULL'
exec sp_bindefault StringVacio, HoraMinutosSegundos
go

/* Portal: Domain para Rutas de Internet */
/* Patch 380 # Seq: 12 Agregado */
exec sp_addtype Ruta_URL, 'varchar(255)', 'NOT NULL'
exec sp_bindefault StringVacio, Ruta_URL
go

/* Portal: Domain para Titulos de Noticias */
/* Patch 380 # Seq: 13 Agregado */
exec sp_addtype Titulo, 'varchar(70)', 'NOT NULL'
exec sp_bindefault StringVacio, Titulo
go

/* Portal: Domain para Colores */
/* Patch 380 # Seq: 14 Agregado */
exec sp_addtype Color, "varchar(8)", "NOT NULL"
exec sp_bindefault StringVacio, Color
go

/* Portal: Domain para Fonts */
/* Patch 380 # Seq: 15 Agregado */
exec sp_addtype FontName, "varchar(40)", "NOT NULL"
exec sp_bindefault StringVacio, FontName
go

/* Portal: Domain para Nombres de Archivo */
/* Patch 380 # Seq: 16 Agregado */
exec sp_addtype NombreArchivo, "varchar(150)", "NOT NULL"
exec sp_bindefault StringVacio, NombreArchivo
go

/* Portal: Domain para Textos */
/* Patch 380 # Seq: 17 Agregado */
exec sp_addtype Texto, "varchar(254)", "NOT NULL"
exec sp_bindefault StringVacio, Texto
go

/* Portal: Domain para Descripciones Largas */
/* Patch 380 # Seq: 18 Agregado */
exec sp_addtype DescLarga, "varchar(40)", "NOT NULL"
exec sp_bindefault StringVacio, DescLarga
go

/* Portal: Paginas Asignadas Al Usuario */
/* Patch 380 # Seq: 19 Agregado */
ALTER TABLE USUARIO ADD US_PAGINAS FolioChico
GO

/* Portal: Empresa Donde Es Empleado El Usuario */
/* Patch 380 # Seq: 20 Agregado */
ALTER TABLE USUARIO ADD CM_CODIGO CodigoEmpresa
GO

/* Portal: # de Empleado Del Usuario */
/* Patch 380 # Seq: 21 Agregado */
ALTER TABLE USUARIO ADD CB_CODIGO NumeroEmpleado
GO

/* Portal: Inicializar # de P�ginas */
/* Patch 380 # Seq: 22 Agregado */
update USUARIO set US_PAGINAS = 0 where ( US_PAGINAS is NULL );

/* Portal: Inicializar Empresa */
/* Patch 380 # Seq: 23 Agregado */
update USUARIO set CM_CODIGO = '' where ( CM_CODIGO is NULL );

/* Portal: Inicializar Empleado */
/* Patch 380 # Seq: 24 Agregado */
update USUARIO set CB_CODIGO = 0 where ( CB_CODIGO is NULL );

/* Usar excepci�n GRUPO_TIENE_SUB_GRUPOS */
/* Patch 380 # Seq: 1 Agregado */
CREATE TRIGGER TD_GRUPO ON GRUPO AFTER DELETE AS
begin
     set NOCOUNT ON;
     declare @NumRows Integer;
     declare @OldCodigo FolioChico;
     select @OldCodigo = GR_CODIGO from Deleted;
     select @NumRows = COUNT(*) from GRUPO where ( GRUPO.GR_PADRE = @OldCodigo );
     if ( @NumRows > 0 )
     begin
	  RAISERROR( 'No Es Posible Borrar Un Grupo Que Tiene Subordinados Otros Grupos', 16, 1 )
     end
end
GO

/* Modificar GR_PADRE al actualizar c�digo */
/* Patch 380 # Seq: 2 Agregado */
CREATE TRIGGER TU_GRUPO ON GRUPO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( GR_CODIGO )
  BEGIN
	declare @NewCodigo FolioChico;
        declare @OldCodigo FolioChico;

	select @NewCodigo = GR_CODIGO from Inserted;
        select @OldCodigo = GR_CODIGO from Deleted;

        update GRUPO set GR_PADRE = @NewCodigo
        where ( GRUPO.GR_PADRE = @OldCodigo );
  END
END
GO

/* Modificar USUARIO.CM_CODIGO y USUARIO.CB_CODIGO al borrar empresa */
/* Patch 380 # Seq: 3 Agregado */
CREATE TRIGGER TD_COMPANY ON COMPANY AFTER DELETE AS
begin
     set NOCOUNT ON;
     declare @OldCodigo CodigoEmpresa;
     select @OldCodigo = CM_CODIGO from Deleted;
     update USUARIO set USUARIO.CM_CODIGO = '', USUARIO.CB_CODIGO = 0
     where ( USUARIO.CM_CODIGO = @OldCodigo );
END
go

/* Modificar USUARIO.CM_CODIGO al actualizar c�digo */
/* Patch 380 # Seq: 4 Agregado */
CREATE TRIGGER TU_COMPANY ON COMPANY AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CM_CODIGO )
  BEGIN
	declare @NewCodigo CodigoEmpresa;
        declare @OldCodigo CodigoEmpresa;

	select @NewCodigo = CM_CODIGO from Inserted;
        select @OldCodigo = CM_CODIGO from Deleted;

        update USUARIO set USUARIO.CM_CODIGO = @NewCodigo
        where ( USUARIO.CM_CODIGO = @OldCodigo );
  END
END
GO

/* Determina dependencias RECURSIVAMENTE hacia abajo de un GRUPO */
/* Patch 380 # Seq: 4 Agregado */
create function DBO.GET_GRUPOS_HIJOS( @GRUPO SmallInt ) returns @HIJOS TABLE( GR_HIJO SmallInt )
as
begin
     declare @HIJO SmallInt
     declare HIJOS cursor for
     select GR_CODIGO from GRUPO where ( GR_PADRE = @GRUPO )
     order by GR_CODIGO;
     open HIJOS;
     fetch NEXT from HIJOS into @HIJO;
     while ( @@Fetch_Status = 0 )
     begin
          insert into @HIJOS( GR_HIJO ) values ( @HIJO );
          insert into @HIJOS select GR_HIJO from DBO.GET_GRUPOS_HIJOS( @HIJO );
          fetch NEXT from HIJOS into @HIJO;
     end;
     close HIJOS;
     deallocate HIJOS;
     return;
end
GO

/* Determina dependencias hacia abajo de un GRUPO */
/* Patch 380 # Seq: 5 Agregado */
create function DBO.GET_DESCENDENCIA( @GRUPO SmallInt ) returns @HIJOS TABLE( GR_HIJO SmallInt )
as
begin
     insert into @HIJOS( GR_HIJO ) values ( @GRUPO );
     insert into @HIJOS select GR_HIJO from DBO.GET_GRUPOS_HIJOS( @GRUPO );
     return;
end
GO

/* Determina si hay dependencias circulares */
/* Patch 380 # Seq: 6 Agregado */
create procedure DBO.CHECK_ASCENDENCIA( @GRUPO SmallInt )
as
begin
     SET NOCOUNT ON;
     declare @PADRE SmallInt;
     select @PADRE = GR_PADRE from GRUPO where ( GR_CODIGO = @GRUPO );
     while ( ( @PADRE is not NULL ) and ( @PADRE > 0 ) )
     begin
          if ( @PADRE = @GRUPO )
          begin
	       RAISERROR( 'La Cadena De Subordinaci�n Es Circular', 16, 1 )
               BREAK;
          end
          else
          begin
               select @PADRE = GR_PADRE from GRUPO where ( GR_CODIGO = @PADRE );
          end
     end
end
GO

/****************************************/
/****************************************/
/****** Fin de Patch para COMPARTE ******/
/****************************************/
/****************************************/

