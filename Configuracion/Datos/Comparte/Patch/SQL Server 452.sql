/****************************************/
/****************************************/
/****** Patch para COMPARTE *************/
/****************************************/
/****************************************/

/* Patch 452 */
--
/* Recalculo de Ahorro y Prestamos */
IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ACTUALIZA_DERECHOS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE ACTUALIZA_DERECHOS
GO

create procedure ACTUALIZA_DERECHOS
as
begin
	SET NOCOUNT ON;
	declare @Cuantos Integer;
	select @Cuantos = COUNT(*) from ACCESO where ( AX_NUMERO = 756);	
	if ( @Cuantos = 0 )
	   INSERT INTO ACCESO(GR_CODIGO, CM_CODIGO, AX_NUMERO, AX_DERECHO)
		SELECT GR_CODIGO, CM_CODIGO, 756, AX_DERECHO FROM ACCESO WHERE AX_NUMERO = 226
end
GO

/* Actualiza Derecho de Accesos */
IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ACTUALIZA_DERECHOS') AND type IN ( N'P', N'PC' ))
	execute ACTUALIZA_DERECHOS
GO

/* Recalculo de Ahorro y Prestamos */
IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'ACTUALIZA_DERECHOS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE ACTUALIZA_DERECHOS
GO
