
if exists (select * from sysobjects where id = object_id('TR_ADD_TERMINAL') )
 drop trigger TR_ADD_TERMINAL
GO

CREATE trigger TR_ADD_TERMINAL on TERMINAL after insert
as
begin
	set nocount on
	
	declare @Mensaje Codigo
	declare @Sleep FolioGrande
	declare @Terminal RelojLinx		
	declare ListaMensajes cursor for
		select DM_CODIGO, DM_SLEEP from MENSAJE where DM_ACTIVO='S' order by DM_ORDEN
		
	select @Terminal = TE_CODIGO from Inserted;
	
	open ListaMensajes
	fetch next from ListaMensajes into @Mensaje, @Sleep
	while (@@Fetch_Status = 0)
	begin
	    if (@Mensaje = 8 )
		begin
            insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP, TM_NEXT) values (@Terminal, @Mensaje, @Sleep, Convert(datetime,'29991231'))
		end
		else
		begin
			insert into TERM_MSG (TE_CODIGO, DM_CODIGO, TM_SLEEP) values (@Terminal, @Mensaje, @Sleep)
		end
		
		fetch next from ListaMensajes into @Mensaje, @Sleep
	end
	close ListaMensajes
	deallocate ListaMensajes
	
	update TERMINAL set TE_XML = 
'<?xml version="1.0" encoding="utf-8"?>
<properties>
   <ClockId>'+@Terminal+'</ClockId> 
   <WebService>http://SERVIDORWEB/WSAsistencia/WSAsistencia.asmx</WebService>
   <UseProxy>false</UseProxy>
   <Proxy>192.168.0.30</Proxy>
   <ProxyPort>8080</ProxyPort>
   <UseProxyAuthentication>false</UseProxyAuthentication>
   <ProxyUserName>NULL</ProxyUserName>
   <ProxyPassword>NULL</ProxyPassword>
   <Ip>DHCP</Ip>
   <NetMask></NetMask>
   <DefaultGateWay></DefaultGateWay>
   <DNS></DNS>
   <TimeOut>07</TimeOut>
   <HeartBeatTimer>60</HeartBeatTimer>
   <MsgTime>01</MsgTime>
   <ErrorMsgTime>05</ErrorMsgTime>
   <RelayTime>0</RelayTime>
   <DateTimeFormat>dd/MM hh:mm</DateTimeFormat>
   <Volume>70</Volume>
   <ProxReader>OFF</ProxReader>
   <BarCodeReader>OFF</BarCodeReader>
   <HID>OFF</HID>
   <MenuAccessCode>1234</MenuAccessCode>
   <FpuNotIdetifyMessage>INVALIDO/INTENTE DE NUEVO</FpuNotIdetifyMessage>
   <NotValidByServerMessage>Servidor no pudo validar ID</NotValidByServerMessage>
   <DuplicateMessage>Registro Duplicado</DuplicateMessage>
   <AutenticateMessage>Exito</AutenticateMessage>
   <OfflineValidation>Offline-Almacenada</OfflineValidation>
   <OneMinuteValidate>true</OneMinuteValidate>   
   <FingerVerify>false</FingerVerify>
   <EnableOfflineRelay>false</EnableOfflineRelay>
   <FingerPrint>ON</FingerPrint>
   <EnableKeyPad>true</EnableKeyPad>
   <Version>TRESS</Version>
   <Account>0</Account>
   <ClientId>0</ClientId>
   <Password>0</Password>
   <CajaId>0</CajaId>
   <Identidad>1</Identidad>
   <UseSoapActionHeader>true</UseSoapActionHeader>
   <SoapActionHeader>Synel/Services</SoapActionHeader>
   <UpdateTimeByServer>true</UpdateTimeByServer>
   <ntpServer>SERVIDORNTP</ntpServer>
   <timeZone>Tijuana</timeZone>
   <MifareReader>OFF</MifareReader>
   <MagneticReader>OFF</MagneticReader>
   <ShowCameraPhoto>false</ShowCameraPhoto>
   <ShowPhoto>true</ShowPhoto>
   <Schedules>
   <Function code="1" start="00:00" end="23:59" bg="comida.jpg">Comida</Function>
   <Function code="3" start="12:01" end="14:00" bg="desayuno.jpg">Desayuno</Function>
   <Function code="2" start="14:01" end="23:59" bg="cena.jpg">Cena</Function>
   </Schedules>
   <DetalleBitacora>false</DetalleBitacora>
</properties>' 
	where TE_CODIGO = @Terminal
end

GO 

create procedure SP_AJUSTAR_XML_TERMINALES
as
begin 
	set nocount on 
	update TERMINAL set TE_XML = CAST(STUFF(CAST(TE_XML as varchar(8000)), CHARINDEX('</properties>',TE_XML), 0,char(13)+char(10)+char(9)+char(9)+'<DetalleBitacora>true</DetalleBitacora>'+char(13)+char(10)) as text) WHERE CHARINDEX('<DetalleBitacora>',TE_XML) = 0 
end 

go 

exec SP_AJUSTAR_XML_TERMINALES

go

drop procedure SP_AJUSTAR_XML_TERMINALES 

go 
