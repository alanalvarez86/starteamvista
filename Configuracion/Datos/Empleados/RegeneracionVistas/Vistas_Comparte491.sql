CREATE VIEW V_USUARIO( US_CODIGO,
				  US_CORTO ,
 				  GR_CODIGO,
                      US_NIVEL ,
                      US_NOMBRE,
                      US_BLOQUEA,
                      US_CAMBIA,
                      US_FEC_IN,
                      US_FEC_OUT,
                      US_DENTRO,
                      US_ARBOL,
                      US_FORMA,
                      US_BIT_LST,
                      US_BIO_ID,
                      US_FEC_SUS,
                      US_FALLAS,
                      US_MAQUINA,
                      US_FEC_PWD,
                      US_EMAIL,
                      US_FORMATO,
                      US_LUGAR,
                      US_PAGINAS,
                      CM_CODIGO,
                      CB_CODIGO,
                      US_ANFITRI,
                      US_PORTAL,
                      US_ACTIVO,
                      US_DOMAIN,
                      US_JEFE
                               )

as

      SELECT      US_CODIGO,
                  US_CORTO ,
                  GR_CODIGO,
                  US_NIVEL ,
                  US_NOMBRE,
                  US_BLOQUEA,
                  US_CAMBIA ,
                  US_FEC_IN ,
                  US_FEC_OUT,
                  US_DENTRO ,
                  US_ARBOL  ,
                  US_FORMA  ,
                  US_BIT_LST,
                  US_BIO_ID ,
                  US_FEC_SUS,
                  US_FALLAS ,
                  US_MAQUINA,
                  US_FEC_PWD,
                  US_EMAIL  ,
                  US_FORMATO,
                  US_LUGAR  ,
                  US_PAGINAS,
                  CM_CODIGO ,
                  CB_CODIGO ,
                  US_ANFITRI,
                  US_PORTAL,
                  US_ACTIVO,
                  US_DOMAIN,
                  US_JEFE

      FROM #COMPARTE.dbo.USUARIO
GO

CREATE VIEW V_GRUPO(
				GR_CODIGO,
				GR_DESCRIP,
				GR_PADRE )
as
	SELECT 	GR_CODIGO,
			GR_DESCRIP,
			GR_PADRE
	FROM #COMPARTE.dbo.GRUPO
GO

CREATE VIEW V_PRINTER (
				       PI_NOMBRE ,
				       PI_EJECT  ,
				       PI_CHAR_10,
				       PI_CHAR_12,
				       PI_CHAR_17,
				       PI_EXTRA_1,
				       PI_EXTRA_2,
				       PI_RESET  ,
				       PI_UNDE_ON,
				       PI_UNDE_OF,
				       PI_BOLD_ON,
				       PI_BOLD_OF,
				       PI_6_LINES,
				       PI_8_LINES,
				       PI_ITAL_ON,
				       PI_ITAL_OF,
				       PI_LANDSCA )
as
	Select PI_NOMBRE ,
		   PI_EJECT  ,
		   PI_CHAR_10,
		   PI_CHAR_12,
		   PI_CHAR_17,
		   PI_EXTRA_1,
		   PI_EXTRA_2,
		   PI_RESET  ,
		   PI_UNDE_ON,
		   PI_UNDE_OF,
		   PI_BOLD_ON,
		   PI_BOLD_OF,
		   PI_6_LINES,
		   PI_8_LINES,
		   PI_ITAL_ON,
		   PI_ITAL_OF,
		   PI_LANDSCA
	FROM #COMPARTE.dbo.PRINTER
GO

CREATE VIEW V_POLL(
				PO_LINX,
				PO_EMPRESA,
				PO_NUMERO,
				PO_FECHA,
				PO_HORA,
				PO_LETRA )
AS
SELECT 	PO_LINX,
		PO_EMPRESA,
		PO_NUMERO,
		PO_FECHA,
		PO_HORA,
		PO_LETRA
FROM #Comparte.dbo.POLL
GO

CREATE VIEW V_NIVEL0(
					TB_CODIGO,
					TB_ELEMENT )
as
SELECT 	TB_CODIGO,
		TB_ELEMENT
FROM #Comparte.dbo.NIVEL0
GO

CREATE VIEW V_COMPANY (
					CM_CODIGO ,
					CM_NOMBRE,
					CM_ACUMULA,
					CM_CONTROL,
					CM_EMPATE,
					CM_ALIAS,
					CM_USRNAME,
					CM_USACAFE,
					CM_NIVEL0,
					CM_DATOS,
					CM_DIGITO,
					CM_KCLASIFI,
          CM_KCONFI,
          CM_KUSERS,
          CM_USACASE
					 )
AS
SELECT
		CM_CODIGO,
		CM_NOMBRE,
		CM_ACUMULA,
		CM_CONTROL,
		CM_EMPATE,
		CM_ALIAS,
		CM_USRNAME,
		CM_USACAFE,
		CM_NIVEL0,
		CM_DATOS,
		CM_DIGITO,
		CM_KCLASIFI,
    CM_KCONFI,
    CM_KUSERS,
    CM_USACASE
FROM #COMPARTE..COMPANY
GO

CREATE VIEW V_BITACORA(
					US_CODIGO,
					BI_FECHA,
					BI_HORA,
					BI_PROC_ID,
					BI_TIPO,
					BI_NUMERO,
					BI_TEXTO,
					CB_CODIGO,
					BI_DATA,
					BI_CLASE,
					BI_FEC_MOV )
as
SELECT 	US_CODIGO,
		BI_FECHA,
		BI_HORA,
		BI_PROC_ID,
		BI_TIPO,
		BI_NUMERO,
		BI_TEXTO,
		CB_CODIGO,
		BI_DATA,
		BI_CLASE,
		BI_FEC_MOV
FROM #Comparte.dbo.BITACORA
GO

CREATE VIEW V_ACCESO(
					GR_CODIGO,
					CM_CODIGO,
					AX_NUMERO,
					AX_DERECHO )
AS
SELECT 	GR_CODIGO,
		CM_CODIGO,
		AX_NUMERO,
		AX_DERECHO
FROM #Comparte.dbo.ACCESO
GO



CREATE VIEW V_BITKIOSC( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO

GO

CREATE VIEW V_BITMISD( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO
GO


CREATE VIEW V_BITCAFE(
       BC_FOLIO,
       CB_CODIGO,
       IV_CODIGO,
       BC_EMPRESA, 
       BC_CREDENC, 
       BC_FECHA,
       BC_HORA,     
       BC_RELOJ,    
       BC_TIPO,  
       BC_COMIDAS,  
       BC_EXTRAS,  
       BC_REG_EXT,  
       BC_MANUAL,  
       CL_CODIGO,  
       BC_TGAFETE,
       BC_TIEMPO,
       BC_STATUS,
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA      
      )
as
 SELECT   BC_FOLIO, 
    CB_CODIGO,   
    IV_CODIGO, 
    BC_EMPRESA, 
    BC_CREDENC,  
    BC_FECHA, 
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
       BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA

 FROM #COMPARTE.dbo.BITCAFE
GO


CREATE VIEW V_BITCAFEE(
       BC_FOLIO, 
       CB_CODIGO,   
       IV_CODIGO, 
       BC_EMPRESA, 
       BC_CREDENC, 
       BC_FECHA,
       BC_HORA,     
       BC_RELOJ,    
       BC_TIPO,  
       BC_COMIDAS,  
       BC_EXTRAS,  
       BC_REG_EXT,  
       BC_MANUAL,  
       CL_CODIGO,  
       BC_TGAFETE,  
       BC_TIEMPO,
       BC_STATUS,   
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA       
      )
as
 SELECT   BC_FOLIO, 
    CB_CODIGO,   
    IV_CODIGO, 
    BC_EMPRESA,
    BC_CREDENC,  
    BC_FECHA, 
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
    BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA     
 
 FROM #COMPARTE.dbo.BITCAFE BITCAFE
 WHERE BITCAFE.BC_EMPRESA = '#DIGITO'
GO

create view V_DISPOSIT(
		DI_NOMBRE,
		DI_TIPO,
 		DI_DESCRIP,
    DI_NOTA,
    DI_IP)
as
	select DI_NOMBRE,
	  DI_TIPO,
	  DI_DESCRIP,
	  DI_NOTA,
	  DI_IP
    from #COMPARTE.dbo.DISPOSIT
GO

CREATE VIEW V_DISXCOM(
		 CM_CODIGO,
		 DI_NOMBRE,
		 DI_TIPO )
as
    SELECT CM_CODIGO,
		DI_NOMBRE ,
		DI_TIPO
    FROM #COMPARTE.dbo.DISXCOM
GO



CREATE view V_ACCARBOL (
	AA_SOURCE,
	AX_NUMERO ,
	AA_DESCRIP,
	AA_VERSION,
	AA_POSICIO,
	AA_MODULO ,
	AA_SCREEN ,
	CM_CONTROL
) AS
SELECT AA_SOURCE,
	AX_NUMERO ,
	AA_DESCRIP COLLATE DATABASE_DEFAULT as AA_DESCRIP,
	AA_VERSION,
	AA_POSICIO,
	AA_MODULO ,
	AA_SCREEN,
	(CASE AA_SOURCE WHEN 0 then '3DATOS' WHEN 3 then '3RECLUTA' WHEN 4 then '3VISITA' ELSE '3OTRO' END)
	FROM #COMPARTE.dbo.ACC_ARBOL
where ((select COUNT(*) from #COMPARTE.dbo.AUTOMOD )=0)
/* Este view revisa contra los modulos autorizados. */
/* Si la tabla AUTOMOD esta vacia, se muestran todos los derechos de acceso */
/* AUTOMOD estara vacia cuando nunca se haya autorizado el Sentinel.*/
/* Si el sentinel se autoriza o se renueva la autorizacion, la tabla AUTOMOD se actualiza.*/
/* Esta tabla la actualiza solamente el TressCFG */
OR AA_MODULO in (select AM_CODIGO from #COMPARTE.dbo.AUTOMOD )
union
/* Derechos de acceso de los grupos adicionales creados por el cliente */
/* Los grupos adicionales se agregan en Globales\Campos Adicionales*/
SELECT  	0,
	10000+LLAVE,
	'Empleados-Datos-Adicionales-' + GX_TITULO COLLATE DATABASE_DEFAULT,
	0,
	7+(GX_POSICIO*.10),
	0,
	4,
	'3DATOS'
FROM GRUPO_AD
UNION
/* Derechos de acceso por clasificacion de Reporteador, esta parte cambia para cada cliente*/
SELECT 	1,
	RC_CODIGO,
	'Reporteador-Clasificaciones-'+RC_NOMBRE COLLATE DATABASE_DEFAULT,
	RC_VERSION,
	RC_ORDEN,
	25 ,
	5 ,
	'3DATOS'
FROM R_CLASIFI
/* Derechos de acceso por entidad de reporteador, esta parte cambia para cada cliente */
/* ESta parte se trae ordenado por modulo y despues por la posicion de la tabla dentro del modulo */
union
SELECT
	2,
	R_ENTIDAD.EN_CODIGO,
	'Reporteador-Entidades-' +
	COALESCE(R_MODULO.MO_NOMBRE,'<Sin m�dulo>') +'-' +
	COALESCE(R_ENTIDAD.EN_TITULO,'<Sin entidad>' ) +
	'('+COALESCE(R_ENTIDAD.EN_TABLA,' ')+')',
	R_ENTIDAD.EN_VERSION,
	R_MODULO.MO_ORDEN*10000 + R_MOD_ENT.ME_ORDEN,
	25 ,
	6 ,
	'3DATOS'
FROM R_MOD_ENT
left outer join R_MODULO on R_MODULO.MO_CODIGO= R_MOD_ENT.MO_CODIGO
left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO= R_MOD_ENT.EN_CODIGO
/* Esta parte se trae las tablas*/
union
select 	2,
	R_ENTIDAD.EN_CODIGO,
	'Reporteador-Entidades-<Sin m�dulo>' +'-' +
	COALESCE(R_ENTIDAD.EN_TITULO,'<Sin entidad>' ) +
	'('+COALESCE(R_ENTIDAD.EN_TABLA,' ')+')',
	R_ENTIDAD.EN_VERSION,
	9999999 ,
	25 ,
	6 ,
	'3DATOS'
from R_ENTIDAD
where EN_CODIGO not IN (select EN_CODIGO from R_MOD_ENT where R_MOD_ENT.EN_CODIGO = R_ENTIDAD.EN_CODIGO)
GO


CREATE view V_ACC_DER (
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
)AS
SELECT
	AD.AA_SOURCE,
	AD.AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT as AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
FROM #Comparte.dbo.ACC_DER AD
left outer join V_ACCARBOL A
	on A.AA_SOURCE = AD.AA_SOURCE
	AND A.AX_NUMERO = AD.AX_NUMERO
where AA_MODULO IS not null
union
select  AA_SOURCE,
	AX_NUMERO ,
  	CASE
    		WHEN charIndex('-Registro', AA_DESCRIP) <>0 THEN 1
    		WHEN charIndex('-Procesos',AA_DESCRIP) <>0 THEN 2
    		WHEN charIndex('-Anuales',AA_DESCRIP) <>0 THEN 3
		else 0
  	END,
  	CASE
    		WHEN charIndex('-Registro', AA_DESCRIP) <>0 THEN 'Registrar'
    		WHEN charIndex('-Procesos',AA_DESCRIP) <>0 THEN 'Ejecutar'
    		WHEN charIndex('-Anuales',AA_DESCRIP) <>0 THEN 'Ejecutar'
		else 'Consultar'
  	END COLLATE DATABASE_DEFAULT,

	4,
	0 from #Comparte.dbo.acc_arbol A
where A.ax_numero not in ( select ax_numero from #Comparte.dbo.acc_der)
UNION
SELECT
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT,
	AD_IMPACTO,
	AD_POSICIO
from V_ACC_AD
UNION
SELECT
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT,
	AD_IMPACTO,
	AD_POSICIO
from V_ACC_RDD
GO


create VIEW V_CO_X_GR (
	CM_CODIGO,
	CM_CONTROL,
	GR_CODIGO
)
AS
select  CM_CODIGO COLLATE DATABASE_DEFAULT,
	CASE CM_CONTROL
		WHEN '3DATOS' THEN 0
		WHEN '3RECLUTA' THEN 3
		WHEN '3VISITA' THEN 4
		ELSE 5 END,
	GR_CODIGO
from #COMPARTE.dbo.GRUPO, #COMPARTE.dbo.COMPANY
GO

CREATE VIEW V_ACCALL(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO
		 )
AS
SELECT 	CASE (SELECT CM_CONTROL FROM #COMPARTE.dbo.COMPANY C WHERE
		C.CM_CODIGO=A.CM_CODIGO)
	WHEN '3DATOS' THEN 0
	WHEN '3RECLUTA' THEN 3
	WHEN '3VISITA' THEN 4
	ELSE 5 END,
	CM_CODIGO  COLLATE DATABASE_DEFAULT as CM_CODIGO,
	GR_CODIGO,
	AX_NUMERO
FROM #COMPARTE.dbo.ACCESO A
union
select    AA_SOURCE,
	V_CO_X_GR.CM_CODIGO COLLATE DATABASE_DEFAULT,
	V_CO_X_GR.GR_CODIGO,
	AX_NUMERO
from V_CO_X_GR, #COMPARTE.dbo.ACC_ARBOL
where V_CO_X_GR.CM_CONTROL = AA_SOURCE
GO


CREATE VIEW V_RENTALL(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO
		 )
AS
SELECT 	2,
	CM_CODIGO ,
	GR_CODIGO,
	EN_CODIGO
FROM R_ENT_ACC
union
select  2,
	V_CO_X_GR.CM_CODIGO,
	V_CO_X_GR.GR_CODIGO,
	EN_CODIGO
from V_CO_X_GR, R_ENTIDAD
where V_CO_X_GR.CM_CONTROL = 0
GO

CREATE VIEW V_ACCEMP(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO,
		AX_DERECHO )
AS
/* Estos son los derechos de acceso que ya estan grabados, no importa si el derecho esta prendido o no*/
/* A la tabla ACCESO llega la informacion cuando el usuario entra a editar derechos de acceso , si el usuario nunca entra
a esa ventana (para determinado grupo) , ese grupo no tendra informacion en esa tabla*/
SELECT 	CASE (SELECT CM_CONTROL FROM #COMPARTE.dbo.COMPANY C
		WHERE C.CM_CODIGO=V.CM_CODIGO COLLATE DATABASE_DEFAULT)
	WHEN '3DATOS' THEN 0
	WHEN '3RECLUTA' THEN 3
	WHEN '3VISITA' THEN 4
	ELSE 5 END,
	CM_CODIGO ,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT A.AX_DERECHO FROM #COMPARTE.dbo.ACCESO A
		WHERE A.CM_CODIGO COLLATE DATABASE_DEFAULT = V.CM_CODIGO COLLATE DATABASE_DEFAULT
		AND A.GR_CODIGO = V.GR_CODIGO
		AND A.AX_NUMERO =  V.AX_NUMERO ) ,0)
	END
FROM V_ACCALL V
UNION
select 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	(select 10000+LLAVE from GRUPO_AD G WHERE G.GX_CODIGO=V_GRADALL.AX_NUMERO),
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT G.GX_DERECHO FROM GR_AD_ACC G
		WHERE G.CM_CODIGO = V_GRADALL.CM_CODIGO
		AND G.GR_CODIGO = V_GRADALL.GR_CODIGO
		AND G.GX_CODIGO =  V_GRADALL.AX_NUMERO ) ,0)
	END
FROM V_GRADALL
UNION
SELECT 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT RA_DERECHO FROM R_CLAS_ACC R
		WHERE R.CM_CODIGO = V_RCLAALL.CM_CODIGO
		AND R.GR_CODIGO = V_RCLAALL.GR_CODIGO
		AND R.RC_CODIGO =  V_RCLAALL.AX_NUMERO ) ,0) END
FROM V_RCLAALL
UNION
SELECT 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT RE_DERECHO FROM R_ENT_ACC R
		WHERE R.CM_CODIGO = V_RENTALL.CM_CODIGO
		AND R.GR_CODIGO   = V_RENTALL.GR_CODIGO
		AND R.EN_CODIGO   = V_RENTALL.AX_NUMERO ) ,0) END
FROM V_RENTALL
GO

CREATE VIEW V_ACCUSU( US_CODIGO,
		  US_CORTO ,
 		  GR_CODIGO,
                      US_NIVEL ,
                      US_NOMBRE,
                      US_BLOQUEA,
                      US_CAMBIA,
                      US_FEC_IN,
                      US_FEC_OUT,
                      US_DENTRO,
                      US_ARBOL,
                      US_FORMA,
                      US_BIT_LST,
                      US_BIO_ID,
                      US_FEC_SUS,
                      US_FALLAS,
                      US_MAQUINA,
                      US_FEC_PWD,
                      US_EMAIL,
                      US_FORMATO,
                      US_LUGAR,
                      US_PAGINAS,
                      US_ANFITRI,
                      US_PORTAL,
                      US_ACTIVO,
                      US_DOMAIN,
                      US_JEFE
                               )
as
      SELECT      US_CODIGO,
                  US_CORTO ,
                  GR_CODIGO,
                  US_NIVEL ,
                  US_NOMBRE,
                  US_BLOQUEA,
                  US_CAMBIA ,
                  US_FEC_IN ,
                  US_FEC_OUT,
                  US_DENTRO ,
                  US_ARBOL  ,
                  US_FORMA  ,
                  US_BIT_LST,
                  US_BIO_ID ,
                  US_FEC_SUS,
                  US_FALLAS ,
                  US_MAQUINA,
                  US_FEC_PWD,
                  US_EMAIL  ,
                  US_FORMATO,
                  US_LUGAR  ,
                  US_PAGINAS,
                  US_ANFITRI,
                  US_PORTAL,
                  US_ACTIVO,
                  US_DOMAIN,
                  US_JEFE
      FROM #COMPARTE.dbo.USUARIO
GO

CREATE VIEW vCurrentDateTime
AS
  SELECT gd = GETDATE()
  
GO

create view V_EMP_BIO as select ID_NUMERO, CM_CODIGO, CB_CODIGO, GP_CODIGO, IV_CODIGO from #COMPARTE.dbo.EMP_BIO
GO

create view V_TERMINAL
as
	select TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT,TE_ZONA, TE_ACTIVO, TE_XML,TE_LOGO,TE_FONDO,TE_ZONA_ES, TE_VERSION, TE_IP,TE_HUELLAS, TE_ID from #COMPARTE.dbo.TERMINAL
GO


create view V_GRUPOTER
as
	select GP_CODIGO, GP_DESCRIP, GP_NUMERO, GP_TEXTO, GP_ACTIVO from #COMPARTE.dbo.GRUPOTERM
GO

create view V_TERM_GPO
as
	select TE_CODIGO, GP_CODIGO from #COMPARTE.dbo.TERM_GPO
GO

create view V_WS_BITAC
as
	select DATEADD(dd, 0, DATEDIFF(dd, 0, WS_FECHA)) as WS_FECHA,CH_RELOJ,WS_CHECADA,CB_CODIGO,WS_MENSAJE,CM_CODIGO,WS_TIPO from #COMPARTE.dbo.WS_BITAC
GO

create view VWS_ACCESS
as
	select WA_FECHA, CM_CODIGO, CB_CODIGO, CH_RELOJ, WS_CHECADA from #COMPARTE.dbo.WS_ACCESS
GO

create view V_MENSAJE (DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO )
as
	select DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO from #COMPARTE.dbo.MENSAJE
GO

Create view V_TERM_MSG 
as 
  select DM_CODIGO, TE_CODIGO, TM_NEXT, TM_ULTIMA, TM_NEXTXHR,TM_NEXTHOR from #COMPARTE.dbo.TERM_MSG
GO

CREATE VIEW VHUELLAGTI
AS

SELECT COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	COUNT(HU_ID) AS HU_CNT
FROM #COMPARTE.DBO.HUELLAGTI AS H, #COMPARTE.DBO.EMP_BIO AS BIO, COLABORA AS C

WHERE H.CB_CODIGO = BIO.CB_CODIGO
		AND H.CM_CODIGO = BIO.CM_CODIGO
		AND H.IV_CODIGO = BIO.IV_CODIGO
		AND H.HU_INDICE < 11

GROUP BY HU_ID, H.CM_CODIGO, H.CB_CODIGO, HU_INDICE, H.IV_CODIGO
GO



create view V_KIOS_IMP
(
	CB_CODIGO ,
	CM_CODIGO ,
	PE_TIPO ,
	PE_YEAR ,
	PE_NUMERO ,
	KI_FECHA ,
	KI_KIOSCO ,
	KI_HORA ,
	RE_CODIGO
)as
select
    CB_CODIGO ,
	CM_CODIGO ,
	PE_TIPO ,
	PE_YEAR ,
	PE_NUMERO ,
	KI_FECHA ,
	KI_KIOSCO ,
	KI_HORA ,
	RE_CODIGO
from
	#COMPARTE.dbo.KIOS_IMPR
GO

CREATE VIEW V_ENCUESTA(
	   ENC_CODIGO
      ,ENC_NOMBRE
      ,ENC_PREGUN
      ,ENC_CONFID
      ,ENC_VOTOSE
      ,ENC_STATUS
      ,US_CODIGO
      ,ENC_FECINI
      ,ENC_FECFIN
      ,ENC_HORINI
      ,ENC_HORFIN
      ,ENC_MRESUL
      ,ENC_FILTRO
      ,ENC_COMPAN
      ,ENC_KIOSCS
      ,ENC_OPCION
      ,ENC_MSJCON
	  ,ENC_OPCGAN
	  ,ENC_VOTGAN
)
as
  SELECT ENC_CODIGO
      ,ENC_NOMBRE
      ,ENC_PREGUN
      ,ENC_CONFID
      ,ENC_VOTOSE
      ,ENC_STATUS
      ,US_CODIGO
      ,ENC_FECINI
      ,ENC_FECFIN
      ,ENC_HORINI
      ,ENC_HORFIN
      ,ENC_MRESUL
      ,ENC_FILTRO
      ,ENC_COMPAN
      ,ENC_KIOSCS
      ,ENC_OPCION
      ,ENC_MSJCON
	  ,#Comparte.[dbo].[SP_MAX_VOTOS_ENCUESTA](ENC_CODIGO,1)AS ENC_OPCGAN
	  ,#Comparte.[dbo].[SP_MAX_VOTOS_ENCUESTA](ENC_CODIGO,0)AS ENC_VOTGAN
      FROM #Comparte.dbo.KENCUESTAS
GO

CREATE VIEW V_VOTOS(
	   ENC_CODIGO
      ,CB_CODIGO
      ,CM_CODIGO
      ,CB_CREDENC
      ,VO_VALOR
      ,VO_FECHA
      ,VO_HORA
      ,VO_KIOSCO)
as
  SELECT
       ENC_CODIGO
      ,CB_CODIGO
      ,CM_CODIGO
      ,CB_CREDENC
      ,VO_VALOR
      ,VO_FECHA
      ,VO_HORA
      ,VO_KIOSCO
   FROM #Comparte.dbo.KVOTOS
GO

CREATE VIEW V_OPC_ENCS(
	  OP_ORDEN
      ,OP_TITULO
      ,OP_DESCRIP
      ,ENC_CODIGO
      ,US_CODIGO)
as
  SELECT OP_ORDEN
      ,OP_TITULO
      ,OP_DESCRIP
      ,ENC_CODIGO
      ,US_CODIGO
  FROM #Comparte.dbo.KOPCIONES
GO

Create View V_RES_ENCU(
	ENC_CODIGO,
	OP_ORDEN,
	OP_TITULO,
	PORCENTAJE,
	TOTAL)
AS
	select V.ENC_CODIGO,VO_VALOR,O.OP_TITULO ,CONVERT( VARCHAR, ( CONVERT( decimal(4,1), Round((CAST( Count(vo_valor)* 100 AS dECIMAL(6,2)) / CAST( (Select Count(*) From #Comparte.dbo.KVOTOS where Enc_Codigo = V.ENC_CODIGO ) as Decimal (6,2) ) ),1) ) ) )+'%' as Opcion, Count(VO_VALOR)as Total from
	#Comparte.dbo.KVOTOS V left outer join #Comparte.dbo.KOPCIONES O on O.OP_ORDEN = V.VO_VALOR and O.ENC_CODIGO = V.ENC_CODIGO
	Group by O.OP_TITULO,V.ENC_CODIGO,VO_VALOR
GO


CREATE VIEW V_PROCESO
AS
SELECT WP_FOLIO,WE_DESCRIP,WE_ORDEN,WM_CODIGO,WM_DESCRIP,WP_FEC_FIN,WP_FEC_INI,WP_NOM_INI,WP_NOMBRE,WP_NOTAS,WP_PASO,WP_PASOS,WP_STATUS,WP_USR_INI,WS_FEC_FIN,WS_FEC_INI,WS_NOM_INI,WS_NOMBRE,WS_STATUS,WS_USR_INI,WT_AVANCE,WT_DESCRIP,WT_FEC_FIN,WT_FEC_INI,WT_NOM_DES,WT_NOM_ORI,WT_NOTAS,WT_STATUS,WT_USR_DES,WT_USR_ORI,WP_MOV3
FROM #COMPARTE..V_PROCESO
GO

CREATE VIEW V_WPCAMBIO
AS
select W.* from #COMPARTE..V_WPCAMBIO W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPAUTO3
AS
select W.* from #COMPARTE..V_WPAUTO3 W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPMULTIP
AS
select W.* from #COMPARTE..V_WPMULTIP W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPPERM
AS
select W.* from #COMPARTE..V_WPPERM W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPVACA
AS
select W.* from #COMPARTE..V_WPVACA W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

create view V_DB_INFO (
	DB_CODIGO,
	DB_DESCRIP,
	DB_CONTROL,
	DB_TIPO,
	DB_DATOS,
	DB_USRNAME,
	DB_PASSWRD,
	DB_CTRL_RL,
	DB_CHKSUM
   ) as
select
	DB_CODIGO,
	DB_DESCRIP,
	DB_CONTROL,
	DB_TIPO,
	DB_DATOS,
	DB_USRNAME,
	DB_PASSWRD,
	DB_CTRL_RL,
	DB_CHKSUM
from #COMPARTE.dbo.DB_INFO
GO

CREATE PROCEDURE SP_AJUSTA_USUARIO_SUPERVISOR( @US_CODIGO Usuario,  @US_JEFE Usuario  )
AS
BEGIN
	SET NOCOUNT ON
	UPDATE #COMPARTE.DBO.USUARIO
	SET US_JEFE = @US_JEFE
	WHERE US_CODIGO = @US_CODIGO
END
GO

CREATE PROCEDURE SP_AJUSTA_SUPERVISOR_ENROLADOS_COMPARTE( @NivelSupervisores Formula, @TB_CODIGO Codigo, @US_CODIGO Usuario)
AS
BEGIN
	SET NOCOUNT ON 
   	
    		UPDATE #COMPARTE.DBO.USUARIO 
    		SET US_JEFE = @US_CODIGO 
    		WHERE US_CODIGO IN 
    		(
	    		SELECT CB.US_CODIGO FROM COLABORA CB
	    		WHERE COALESCE(CB.US_CODIGO,0) > 0 
	    		 AND (
		  		(CB.CB_NIVEL1 = @TB_CODIGO AND @NivelSupervisores = '1') OR
	        		(CB.CB_NIVEL2 = @TB_CODIGO AND @NivelSupervisores = '2') OR
		    		(CB.CB_NIVEL3 = @TB_CODIGO AND @NivelSupervisores = '3') OR
		    		(CB.CB_NIVEL4 = @TB_CODIGO AND @NivelSupervisores = '4') OR
		    		(CB.CB_NIVEL5 = @TB_CODIGO AND @NivelSupervisores = '5') OR
		    		(CB.CB_NIVEL6 = @TB_CODIGO AND @NivelSupervisores = '6') OR
		    		(CB.CB_NIVEL7 = @TB_CODIGO AND @NivelSupervisores = '7') OR
		    		(CB.CB_NIVEL8 = @TB_CODIGO AND @NivelSupervisores = '8') OR
		    		(CB.CB_NIVEL9 = @TB_CODIGO AND @NivelSupervisores = '9')
	    		)
    		)
END
GO

CREATE VIEW VTRESSFILE(
				TF_PATH, 
				TF_EXT, 
				TF_FILE, 
				TF_DESCR, 
				TF_VERSION, 
				TF_BUILD, 
				TF_FEC_MOD, 
				TF_CALLS, 
				TF_FEC_ACC, 
				TF_HOST, 
				TF_FEC_UPD)
as
		SELECT 	TF_PATH, 
				TF_EXT, 
				TF_FILE, 
				TF_DESCR, 
				TF_VERSION, 
				TF_BUILD, 
				TF_FEC_MOD, 
				TF_CALLS, 
				TF_FEC_ACC, 
				TF_HOST, 
				TF_FEC_UPD
	FROM #COMPARTE.dbo.TRESSFILES
GO

CREATE view V_SUSCRIP  as SELECT CM_CODIGO, RE_CODIGO=CA_REPORT, US_CODIGO from  #COMPARTE..V_SUSCRIP
group by CM_CODIGO, CA_REPORT, US_CODIGO 
GO

create view V_SUSC_CAL  as SELECT 
CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO,  CA_FREC, CA_HORA, CA_ULT_FEC, CA_REPORT, VS_TIPO, RO_CODIGO, RO_NOMBRE, US_CODIGO, US_NOMBRE, US_EMAIL 
 from  #COMPARTE..V_SUSCRIP
GO

CREATE view V_CALENDAR  as 
    SELECT CA_FOLIO,CA_NOMBRE,CA_DESCRIP,CA_ACTIVO,CM_CODIGO,CA_REPORT,CA_REPEMP,CA_FECHA,CA_HORA,CA_FREC,
        CA_RECUR,CA_DOWS,CA_MESES,CA_MESDIAS,CA_MES_ON,CA_MESWEEK,CA_FEC_ESP,CA_TSALIDA,CA_FSALIDA,CA_ULT_FEC,CA_NX_FEC,
        CA_NX_EVA,CA_US_CHG,CA_CAPTURA  
    FROM #COMPARTE..CALENDARIO
GO

CREATE PROCEDURE Sesion_SetValues (
	@SesionIDActual	FolioGrande,
	@SesionID	FolioGrande OUTPUT,
	@US_CODIGO	Usuario, 
	@CM_CODIGO	CodigoEmpresa, 	
	@PE_TIPO	FolioChico, 
	@PE_YEAR	FolioChico, 
	@PE_NUMERO	FolioChico, 
	@FechaActiva	Fecha	
	 )
AS
BEGIN 
	declare @PE_MES Mes 
	declare @MesIni Fecha 
	declare @MesFin Fecha 

	set nocount on

	select @PE_MES = PE_MES from PERIODO where @PE_TIPO = PE_TIPO and  @PE_YEAR = PE_YEAR and  @PE_NUMERO = PE_NUMERO
	if ( @PE_MES > 12 ) set @PE_MES = 12 
	set @PE_MES = coalesce( @PE_MES, 1 )

	select @MesIni = cast(0 as datetime)
	declare @dt Varchar(20)	
	
	set @dt = '' 
	if (@PE_MES > 9 ) 
		set @dt = CAST(@PE_YEAR as varchar(4)) + CAST(@PE_MES as varchar(2)) + '01'
	else
		set @dt = CAST(@PE_YEAR as varchar(4)) + '0' + CAST(@PE_MES as varchar(2)) +'01' 
			
	set @MesIni = CONVERT(datetime, @dt, 112 ) 
	set @MesFin = dateadd(day, -1,  dateadd( month, 1, @MesIni ) );  
	set @SesionID = @SesionIDActual;
	exec #COMPARTE..Sesion_Update @SesionID output, @US_CODIGO, @CM_CODIGO, @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @FechaActiva, @MesIni, @MesFin 

END 

GO

CREATE PROCEDURE Sesion_Delete ( @SesionID FolioGrande ) 
AS
BEGIN 
	exec #COMPARTE..Sesion_Delete @SesionID 
END 

GO

CREATE PROCEDURE SP_DASHLET_RUN( @SesionID FolioGrande, @DashletID  FolioGrande )	
AS
BEGIN	
	declare @Query nvarchar(max)
	
	select @Query = D.DashletQuery from #COMPARTE..Dashlets  D where D.DashletID = @DashletID 

	exec SP_DASHLET_RUNQUERY @SesionID, @Query 
END 


GO

CREATE VIEW VFACIALGTI
AS
SELECT COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	COUNT(HU_ID) AS HU_CNT
FROM #COMPARTE.DBO.HUELLAGTI AS H, #COMPARTE.DBO.EMP_BIO AS BIO, COLABORA AS C

WHERE H.CB_CODIGO = BIO.CB_CODIGO
		AND H.CM_CODIGO = BIO.CM_CODIGO
		AND H.IV_CODIGO = BIO.IV_CODIGO
		AND H.HU_INDICE = 11

GROUP BY HU_ID, H.CM_CODIGO, H.CB_CODIGO, HU_INDICE, H.IV_CODIGO
GO

CREATE VIEW VHGTI
AS
SELECT
	BIO.ID_NUMERO,
	COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	H.HU_ID AS HU_CNT,
	H.HU_HUELLA AS HUELLA,
	CO.CM_DIGITO + REPLICATE('0', 9 - LEN( H.CB_CODIGO ) ) + cast ( H.CB_CODIGO as Varchar) + 'A' as CHECADA
FROM #COMPARTE..HUELLAGTI H
left outer join #COMPARTE..EMP_BIO BIO on H.CB_CODIGO = BIO.CB_CODIGO and H.CM_CODIGO = BIO.CM_CODIGO
left outer join #COMPARTE..COMPANY CO on H.CM_CODIGO = CO.CM_CODIGO
WHERE ( BIO.CB_ACTIVO = 'S' ) 
or ( ( BIO.CB_ACTIVO = 'N' ) and ( BIO.CB_FEC_BAJ >= Getdate() ) )
and ( BIO.IV_CODIGO = 0 )
GO

CREATE VIEW VPGTI
AS
SELECT
	EI.ID_NUMERO,
	COALESCE (EI.CM_CODIGO, '') AS CM_CODIGO,
	EI.CB_CODIGO,
	CO.CM_DIGITO + REPLICATE('0', 9 - LEN( EI.CB_CODIGO ) ) + cast ( EI.CB_CODIGO as Varchar) + 'A' as CHECADA
FROM #COMPARTE..EMP_ID EI
left outer join #COMPARTE..COMPANY CO on EI.CM_CODIGO = CO.CM_CODIGO
GO

CREATE function Sesion_GetValues( @SesionID FolioGrande ) 
returns table 
as
return (  
select PE_TIPO, PE_YEAR, PE_NUMERO, PE_MES, FechaActiva, MesIni, MesFin, US_CODIGO, CM_CODIGO, CM_NIVEL0  from #COMPARTE..Sesion 
where SesionID = @SesionID 
) 
go

CREATE function WFConfig_GetUsuarioTRESS() 
returns Usuario 
as 
begin 
	
		declare @US_CODIGO Usuario 
		declare @XMLConfig XML 

		select @XMLConfig = ConfigXML from #COMPARTE..T_WFConfig 

		set @US_CODIGO= @XMLConfig.value('(/ControladorConfig/UsuarioSistema/.)[1]', 'INT' )

		set @US_CODIGO = coalesce( @US_CODIGO, 0 ) 

		return @US_CODIGO 
        
end

GO

CREATE function WFEmpresas_GetCodigoEmpresa( @ClienteID FolioGrande ) returns Varchar(20) 
as 
begin 

        declare @Cm_Codigo Varchar(20) 
        
        set @Cm_Codigo = '' 
        
        if @ClienteID > 0 
                select @Cm_Codigo = CM_CODIGO from #COMPARTE..COMPANY where CM_WFEMPID = @ClienteID 
         
        return RTRIM( @Cm_Codigo) 
end

go

create procedure Analitica_Poblar_Version2017
as
begin 	
	declare @CantidadRolTableros int 
	declare @UsaTablerosInicio booleano 
	
	select @CantidadRolTableros = count(*) from #COMPARTE..RolTableros  where RO_CODIGO <> 'MIEMP' 
	if ( @CantidadRolTableros > 0 ) 
		set @UsaTablerosInicio = 'S' 
	else 
		set @UsaTablerosInicio= 'N' 
	
	exec Analitica_Upsert 'Versi�n 2017' , 11, 'V2017_DB' ,  'Tableros de inicio - Uso',  @UsaTablerosInicio,  @CantidadRolTableros

	declare @CantidadEnviosProgramados int 
	
	select @CantidadEnviosProgramados  = COUNT(*) from #COMPARTE..CALENDARIO where CA_ACTIVO = 'S'
	exec Analitica_Upsert 'Versi�n 2017' , 12, 'V2017_RE1' ,  'Reportes Email - Env�os programados',  'N',  @CantidadEnviosProgramados
	
end 
go

create view VWSDIAGNOS(
	WS_FECHA,
	CH_RELOJ,
	WS_DATOS)
as
	select DATEADD(dd, 0, DATEDIFF(dd, 0, WS_FECHA)) as WS_FECHA, 
		CH_RELOJ,
		WS_DATOS 
	from #COMPARTE.dbo.WS_DIAGNOS
GO