CREATE  PROCEDURE DBO.RECALCULA_AREA @EMPLEADO INTEGER
AS
begin
     SET NOCOUNT ON;
     declare @VKA_FECHA DATETIME;
     declare @VKA_HORA CHAR(4);
     declare @VCB_AREA CHAR(6);
     declare Cur_RecalculaArea CURSOR FOR
     select TOP 1
            KA_FECHA,
            KA_HORA,
            CB_AREA
     from KAR_AREA
     where ( CB_CODIGO = @EMPLEADO )
     order by KA_FECHA desc, KA_HORA desc;
     open Cur_RecalculaArea;
     fetch next from Cur_RecalculaArea into @vKA_FECHA, @vKA_HORA, @vCB_AREA;
     if ( @@ROWCOUNT  <> 0 )
     begin
          update COLABORA set
          CB_AREA = @VCB_AREA,
          CB_AR_FEC = @VKA_FECHA,
          CB_AR_HOR = @VKA_HORA
          where ( CB_CODIGO = @EMPLEADO );
     end
     else
     begin
          update COLABORA set
          CB_AREA = '',
          CB_AR_FEC = '12/30/1899',
          CB_AR_HOR = '0000'
          where ( CB_CODIGO = @EMPLEADO );
     END
     close Cur_RecalculaArea;
     deallocate Cur_RecalculaArea;
END
GO

CREATE  PROCEDURE DBO.RECALCULA_AREA_TODOS
AS
DECLARE @CB_CODIGO INTEGER
begin
	SET NOCOUNT ON
        DECLARE Cursor_AreaTodos CURSOR FOR
		select CB_CODIGO
		from COLABORA
		order by CB_CODIGO
	OPEN Cursor_AreaTodos
	FETCH NEXT FROM Cursor_AreaTodos INTO	@CB_CODIGO
	WHILE @@FETCH_STATUS = 0
     	begin
          	EXEC RECALCULA_AREA @CB_CODIGO

		FETCH NEXT FROM Cursor_AreaTodos
		INTO	@CB_CODIGO
     	end
	CLOSE Cursor_AreaTodos
	DEALLOCATE Cursor_AreaTodos
END
GO

CREATE FUNCTION DBO.SP_FECHA_AREA_KARDEX (
	@EMPLEADO INTEGER,
	@FECHA DATETIME,
	@HORA CHAR(4) )
RETURNS @RegKardex TABLE (
      	KA_FECHA DATETIME,
	KA_HORA CHAR(4),
	CB_AREA CHAR(6),
	US_CODIGO SMALLINT,
	KA_FEC_MOV DATETIME,
	KA_HOR_MOV CHAR(4) )
AS
BEGIN
     insert into @RegKardex
             select TOP 1 KA_FECHA, KA_HORA, CB_AREA, US_CODIGO, KA_FEC_MOV, KA_HOR_MOV
             from KAR_AREA
             where ( CB_CODIGO = @Empleado ) and ( ( KA_FECHA < @Fecha ) or ( KA_FECHA = @Fecha and KA_HORA <= @Hora ) )
             order by KA_FECHA desc, KA_HORA desc
     if ( @@ROWCOUNT = 0 )
     begin
          set @Fecha = '12/30/1899'
          insert into @RegKardex values ( @Fecha, '0000', '', 0, @Fecha, '0000' )
     end
     return
END
GO

CREATE FUNCTION DBO.SP_FECHA_AREA
	( @EMPLEADO INTEGER,
    	  @FECHA DATETIME,
    	  @HORA CHAR(4) )
RETURNS CHAR(6)
AS
BEGIN
     DECLARE @AREA CHAR(6);
     select @AREA = CB_AREA from COLABORA where
     ( CB_CODIGO = @EMPLEADO ) and
     ( CB_FEC_ING <= @FECHA ) and
     ( ( CB_AR_FEC < @FECHA ) or
     ( ( CB_AR_FEC = @FECHA ) and ( CB_AR_HOR <= @HORA ) ) );
     IF ( @AREA IS NULL )
     BEGIN
       	  SELECT @AREA =( SELECT CB_AREA FROM DBO.SP_FECHA_AREA_KARDEX( @EMPLEADO, @FECHA, @HORA ) );
     END
     IF( @AREA IS NULL )
     BEGIN
    	  SELECT @AREA = '';
     END
     RETURN @AREA
END
GO

CREATE FUNCTION DBO.SP_LISTA_AREA
	( @FECHA DATETIME,
          @HORA CHAR(4),
    	  @USUARIO SMALLINT )
RETURNS @RegListaArea TABLE
	( CB_CODIGO INTEGER,
          CB_AREA CHAR(6))
AS
BEGIN
     DECLARE @MATCHES INTEGER,
	     @MATCHES2 INTEGER,
	     @CB_CODIGO INTEGER,
	     @CB_AREA CHAR(6);
     SELECT @MATCHES = (SELECT COUNT(*) FROM SUP_AREA WHERE ( US_CODIGO = @USUARIO ));
     DECLARE Temporal CURSOR for
        select COLABORA.CB_CODIGO, ( select DBO.SP_FECHA_AREA( COLABORA.CB_CODIGO, @FECHA, @HORA ) ) CB_AREA from COLABORA
        where ( ( select DBO.SP_STATUS_ACT ( @FECHA, COLABORA.CB_CODIGO ) ) = 1 )
        order by COLABORA.CB_CODIGO
     OPEN Temporal
     FETCH NEXT FROM Temporal INTO
     @CB_CODIGO,
     @CB_AREA
     WHILE @@FETCH_STATUS = 0
     BEGIN
   	IF ( ( @CB_AREA = '' ) or ( @Matches = 0 ) )
   	BEGIN
      		INSERT INTO @RegListaArea VALUES( @CB_CODIGO, @CB_AREA );
   	END
        ELSE
   	BEGIN
             SELECT @MATCHES2 = (SELECT COUNT(*) FROM SUP_AREA
             WHERE ( US_CODIGO = @USUARIO ) AND ( CB_AREA = @CB_AREA ));
             IF ( @MATCHES2 > 0 )
             BEGIN
                  INSERT INTO @RegListaArea VALUES( @CB_CODIGO, @CB_AREA );
             END
        END
	FETCH NEXT FROM Temporal INTO
     	@CB_CODIGO,
     	@CB_AREA
     END
     CLOSE Temporal
     DEALLOCATE Temporal
     RETURN
END
GO

CREATE FUNCTION DBO.SP_LISTA_AREA_EMPLEADOS
	( @FECHA DATETIME,
    	  @HORA CHAR(4),
          @AREA CHAR(6) )
RETURNS @RegListaEmpleados TABLE (
	CB_CODIGO Integer )
AS
BEGIN
     DECLARE @Empleado INTEGER;
     DECLARE Temporal CURSOR for
        select COLABORA.CB_CODIGO from COLABORA where
        ( ( select DBO.SP_STATUS_ACT ( @FECHA, COLABORA.CB_CODIGO ) ) = 1 ) and
        ( ( select DBO.SP_FECHA_AREA( COLABORA.CB_CODIGO, @FECHA, @HORA ) ) = @AREA )
        order by COLABORA.CB_CODIGO
     OPEN Temporal;
     FETCH NEXT FROM Temporal INTO @Empleado;
     WHILE @@FETCH_STATUS = 0
     BEGIN
          INSERT INTO @RegListaEmpleados VALUES ( @Empleado );
          FETCH NEXT FROM Temporal INTO @Empleado;
     END;
     CLOSE Temporal
     DEALLOCATE Temporal
     RETURN
END
GO

CREATE FUNCTION DBO.SP_CEDULA_EMPLEADOS
	( @CEDULA INTEGER,
          @FECHA DATETIME,
          @HORA CHAR(4),
	  @USUARIO INTEGER )
RETURNS @RegListaEmpleados TABLE (
	CB_CODIGO Integer,
        CAPTURADO Integer )
AS
BEGIN
     DECLARE @CUANTOS INTEGER;
     DECLARE @EMPLEADO INTEGER;
     DECLARE @HAY_EMPLEADOS INTEGER;
     DECLARE @AREA CHAR(6);
     SELECT @AREA = CE_AREA, @CUANTOS = ( select COUNT(*) from CED_EMP CE where ( CE.CE_FOLIO = C.CE_FOLIO ) )
     FROM CEDULA C where ( CE_FOLIO = @CEDULA )
     IF( @AREA IS NOT NULL )
     BEGIN
          IF ( @CUANTOS = 0 )
          BEGIN
               DECLARE Temporal CURSOR for
                     SELECT CB_CODIGO FROM SP_LISTA_AREA_EMPLEADOS( @FECHA, @HORA, @AREA )
               OPEN Temporal;
               FETCH NEXT FROM Temporal INTO
               @EMPLEADO;
               WHILE @@FETCH_STATUS = 0
               BEGIN
                      SELECT @HAY_EMPLEADOS = COUNT(*) FROM TMPLABOR T WHERE
                     ( T.US_CODIGO = @USUARIO ) and ( T.CB_CODIGO = @EMPLEADO ) and ( T.TL_APLICA = 'S' )
                     if ( @HAY_EMPLEADOS > 0 )
                     begin
                          INSERT INTO @RegListaEmpleados VALUES ( @EMPLEADO, @CUANTOS );
                     end
                      FETCH NEXT FROM Temporal INTO
                      @EMPLEADO;
               END
               CLOSE Temporal
               DEALLOCATE Temporal
          END
     END
     ELSE
     BEGIN
          DECLARE Temporal CURSOR for
             SELECT CB_CODIGO FROM CED_EMP CE WHERE ( CE.CE_FOLIO = @CEDULA ) and
             ( ( SELECT COUNT(*) FROM TMPLABOR T WHERE ( T.US_CODIGO = @USUARIO ) and ( T.CB_CODIGO = CE.CB_CODIGO ) and ( T.CB_AREA <> '' ) and ( T.TL_APLICA = 'S' ) ) > 0 )
          OPEN Temporal;
          FETCH NEXT FROM Temporal INTO
          @EMPLEADO;
          WHILE @@FETCH_STATUS = 0
          BEGIN
               INSERT INTO @RegListaEmpleados VALUES ( @EMPLEADO, @CUANTOS );
               FETCH NEXT FROM Temporal INTO
               @EMPLEADO;
          END
          CLOSE Temporal
          DEALLOCATE Temporal
     END
     RETURN
END
GO

CREATE PROCEDURE DBO.SP_CEDULA_PIEZAS( @CEDULA INTEGER )
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @PIEZAS NUMERIC( 15, 4 );
    DECLARE @TIEMPO NUMERIC( 15, 4 );
    SELECT @PIEZAS = CE_PIEZAS FROM CEDULA WHERE ( CE_FOLIO = @CEDULA );
    IF ( ( @PIEZAS IS NOT  NULL  ) AND ( @PIEZAS > 0 ) )
     BEGIN
          SELECT @TIEMPO = SUM( WK_TIEMPO ) FROM WORKS WHERE ( WK_CEDULA = @CEDULA );
          IF ( ( @TIEMPO IS  NOT NULL ) AND ( @TIEMPO > 0 ) )
          BEGIN
               UPDATE WORKS SET WK_PIEZAS = @PIEZAS * ( WK_TIEMPO / @TIEMPO )
               WHERE ( WK_CEDULA = @CEDULA );

          END
     END
END
GO

CREATE FUNCTION DBO.SP_EMPLEADO_AREAS
	( @EMPLEADO INTEGER,
    	  @FECHAINICIAL DATETIME,
    	  @HORAINICIAL CHAR(4),
    	  @FECHAFINAL DATETIME,
    	  @HORAFINAL CHAR(4) )
RETURNS @RegEmpleadoAreas TABLE
	( CB_AREA CHAR(6),
    	  KA_FECHA DATETIME,
    	  KA_HORA CHAR(4))
AS
BEGIN
     DECLARE @CB_AREA CHAR(6),
             @KA_FECHA DATETIME,
	     @KA_HORA CHAR(4)

     SELECT @CB_AREA = CB_AREA, @KA_FECHA = CB_AR_FEC, @KA_HORA = CB_AR_HOR
     FROM COLABORA WHERE ( CB_CODIGO = @EMPLEADO )
     IF ( ( @FECHAINICIAL > @KA_FECHA ) OR ( ( @FECHAINICIAL = @KA_FECHA ) AND ( @HORAINICIAL > @KA_HORA ) ) )
     BEGIN
          SELECT @KA_FECHA = @FECHAINICIAL
          SELECT @KA_HORA = @HORAINICIAL
          INSERT INTO @RegEmpleadoAreas VALUES ( @CB_AREA, @KA_FECHA, @KA_HORA );
     END
     ELSE
     BEGIN
          SELECT @CB_AREA = (SELECT DBO.SP_FECHA_AREA( @EMPLEADO, @FECHAINICIAL, @HORAINICIAL ) );
          SELECT @KA_FECHA = @FECHAINICIAL;
          SELECT @KA_HORA = @HORAINICIAL;
          INSERT INTO @RegEmpleadoAreas VALUES ( @CB_AREA, @KA_FECHA, @KA_HORA )
          DECLARE CursorTemporalEmp CURSOR for
             SELECT CB_AREA, KA_FECHA, KA_HORA FROM KAR_AREA WHERE
             ( CB_CODIGO = @EMPLEADO ) AND
             ( ( KA_FECHA > @FECHAINICIAL ) OR ( ( KA_FECHA = @FECHAINICIAL ) AND ( KA_HORA > @HORAINICIAL ) ) ) AND
             ( ( KA_FECHA < @FECHAFINAL ) OR ( ( KA_FECHA = @FECHAFINAL ) AND ( KA_HORA <= @HORAFINAL ) ) )
             ORDER BY KA_FECHA, KA_HORA
          OPEN CursorTemporalEmp
          FETCH NEXT FROM CursorTemporalEmp INTO @CB_AREA,
	                                         @KA_FECHA,
                                                 @KA_HORA
          WHILE @@FETCH_STATUS = 0
     	  BEGIN
               INSERT INTO @RegEmpleadoAreas VALUES ( @CB_AREA, @KA_FECHA, @KA_HORA );
               FETCH NEXT FROM CursorTemporalEmp INTO @CB_AREA,
                                                      @KA_FECHA,
                                                      @KA_HORA
          END
          CLOSE CursorTemporalEmp
          DEALLOCATE CursorTemporalEmp
     END
     RETURN
END
GO

CREATE FUNCTION DBO.SP_FECHA_AREA_DEFAULT( @EMPLEADO INTEGER, @FECHA DATETIME )
RETURNS CHAR(6)
as
begin
     declare @AREA CHAR(6);
     SELECT @AREA = CB_AREA FROM SP_FECHA_AREA_KARDEX( @EMPLEADO, @FECHA, '0000' );
     if ( ( @AREA is NULL ) or ( @AREA = '' ) )
     begin
          select @AREA = K1.CB_AREA from KAR_AREA K1 where
          ( K1.CB_CODIGO = @EMPLEADO ) and
          ( K1.KA_FECHA = @FECHA ) and
          ( K1.KA_HORA = ( select MIN( K2.KA_HORA ) from KAR_AREA K2 where
          ( K2.CB_CODIGO = K1.CB_CODIGO ) and
          ( K2.KA_FECHA = K1.KA_FECHA ) ) );
          if ( @AREA is NULL )
          begin
               select @AREA = '';
          end
     end
     return @AREA;
end

GO

CREATE PROCEDURE DBO.SP_TMPLABOR_START( @USUARIO INTEGER, @FECHA DATETIME)
AS
  DECLARE @STATUS_EMP SMALLINT;
  DECLARE @AU_CHECADAS INTEGER;
  DECLARE @CB_CODIGO INTEGER;
  DECLARE @CB_FEC_KAR DATETIME;
  DECLARE @CB_TURNO CHAR(6);
  DECLARE @CB_AREA CHAR(6);
BEGIN
     SET NOCOUNT ON;
     UPDATE TMPLABOR SET TL_APLICA = 'N', TL_CALCULA = 'N' WHERE ( US_CODIGO = @USUARIO );
     DECLARE Temporal CURSOR FOR
        SELECT TMPLABOR.CB_CODIGO, COLABORA.CB_FEC_KAR, COLABORA.CB_TURNO FROM TMPLABOR
        left outer join COLABORA ON ( COLABORA.CB_CODIGO = TMPLABOR.CB_CODIGO )
        WHERE ( TMPLABOR.US_CODIGO = @USUARIO ) order by TMPLABOR.CB_CODIGO
     OPEN Temporal
     FETCH NEXT FROM Temporal INTO @CB_CODIGO, @CB_FEC_KAR, @CB_TURNO;
     WHILE @@FETCH_STATUS = 0
     BEGIN
       SELECT @CB_AREA = ( SELECT DBO.SP_FECHA_AREA_DEFAULT( @CB_CODIGO, @FECHA ) );
       SELECT @AU_CHECADAS = COUNT(*) from CHECADAS where
                     ( CB_CODIGO = @CB_CODIGO ) and
                     ( AU_FECHA = @FECHA ) and
                     ( CH_TIPO in ( 1, 2 ) );
       IF ( @AU_CHECADAS = 0 )
       BEGIN
            SELECT @AU_CHECADAS = COUNT(*) from AUSENCIA where
            ( CB_CODIGO = @CB_CODIGO ) and
            ( AU_FECHA = @FECHA ) and
            ( ( AU_HORASCK + AU_NUM_EXT > 0 ) or
            ( ( AU_HORAS > 0 ) and not ( AU_TIPODIA in ( 3, 8 ) ) ) or
            ( ( AU_EXTRAS + AU_DES_TRA ) > 0 ) );
       END
       IF ( @AU_CHECADAS = 0 )
       BEGIN
            SELECT  @STATUS_EMP = ( SELECT DBO.SP_STATUS_EMP( @FECHA, @CB_CODIGO ) as RESULTADO );
            IF ( @FECHA < @CB_FEC_KAR )
            BEGIN
                 SELECT @CB_TURNO = CB_TURNO FROM SP_FECHA_KARDEX( @FECHA, @CB_CODIGO );
            END
            UPDATE TMPLABOR SET
            CB_AREA = @CB_AREA,
            CB_TURNO = @CB_TURNO,
            TL_STATUS = @STATUS_EMP
            WHERE ( US_CODIGO = @USUARIO ) and ( CB_CODIGO = @CB_CODIGO );
       END
       ELSE
       BEGIN
            UPDATE TMPLABOR SET
            CB_AREA = @CB_AREA,
            CB_TURNO = '',
            TL_STATUS = 0,
            TL_APLICA = 'S'
            WHERE ( US_CODIGO = @USUARIO ) and ( CB_CODIGO = @CB_CODIGO );
       END
       FETCH NEXT FROM Temporal INTO @CB_CODIGO, @CB_FEC_KAR, @CB_TURNO;
     END
     CLOSE Temporal
     DEALLOCATE Temporal
END
GO

CREATE PROCEDURE DBO.INIT_AREA( @EMPLEADO INTEGER, @AREA CHAR(6), @USUARIO INTEGER )
AS
begin
     SET NOCOUNT ON;
     if ( @AREA <> '' )
     begin
          declare @INGRESO DATETIME;
          select @INGRESO = CB_FEC_ING from COLABORA where ( CB_CODIGO = @EMPLEADO );
          if ( @INGRESO is not NULL )
          begin
               insert into KAR_AREA ( CB_CODIGO,
                                      KA_FECHA,
                                      KA_HORA,
                                      CB_AREA,
                                      KA_FEC_MOV,
                                      KA_HOR_MOV,
                                      US_CODIGO )
               values ( @EMPLEADO,
                        @INGRESO,
                        '0000',
                        @AREA,
                        GetDate(),
                        '0000',
                        @USUARIO );
               execute RECALCULA_AREA @EMPLEADO;
          end
     end
end
GO

