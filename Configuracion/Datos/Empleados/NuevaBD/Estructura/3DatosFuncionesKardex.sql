CREATE FUNCTION SP_KARDEX_CB_FECHA(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FECHA
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FECHA
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_CAP(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_CAP
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_CAP
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_CON(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_CON
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_CON
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_INT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_INT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_INT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_REV(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_REV
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_REV
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FECHA_2(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FECHA_2
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FECHA_2
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_ING(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_ING
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_ING
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_ANT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_ANT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_ANT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_SAL(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS     
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_SAL
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_SAL
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MONTO(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_MONTO
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_MONTO
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_OLD_INT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_OLD_INT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_OLD_INT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_OLD_SAL(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_OLD_SAL
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_OLD_SAL
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_OTRAS_P(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_OTRAS_P
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_OTRAS_P
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_PER_VAR(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_PER_VAR
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_PER_VAR
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_PRE_INT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_PRE_INT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_PRE_INT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_RANGO_S(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_RANGO_S
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_RANGO_S
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_SAL_INT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_SAL_INT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_SAL_INT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_SAL_TOT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_SAL_TOT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_SAL_TOT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_SALARIO(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_SALARIO
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_SALARIO
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_TOT_GRA(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Pesos
AS     
BEGIN
	declare @Regresa Pesos;
	set @Regresa = ( select TOP 1 CB_TOT_GRA
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_TOT_GRA
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_STATUS(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS SmallInt
AS     
BEGIN
	declare @Regresa SmallInt;
	set @Regresa = ( select TOP 1 CB_STATUS
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_STATUS
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_US_CODIGO(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS SmallInt
AS     
BEGIN
	declare @Regresa SmallInt;
	set @Regresa = ( select TOP 1 US_CODIGO
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 US_CODIGO
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NOMTIPO(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS SmallInt
AS     
BEGIN
	declare @Regresa SmallInt;
	set @Regresa = ( select TOP 1 CB_NOMTIPO
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NOMTIPO
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NOMYEAR(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS SmallInt
AS     
BEGIN
	declare @Regresa SmallInt;
	set @Regresa = ( select TOP 1 CB_NOMYEAR
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NOMYEAR
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NOMNUME(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS SmallInt
AS     
BEGIN
	declare @Regresa SmallInt;
	set @Regresa = ( select TOP 1 CB_NOMNUME
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NOMNUME
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_TIPO(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_TIPO
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_TIPO
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_CLASIFI(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_CLASIFI
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_CLASIFI
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_TURNO(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_TURNO
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_TURNO
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_PUESTO(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_PUESTO
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_PUESTO
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL1(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL1
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL1
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL2(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL2
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL2
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL3(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL3
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL3
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL4(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL4
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL4
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL5(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL5
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL5
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL6(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL6
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL6
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL7(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL7
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL7
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL8(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL8
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL8
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NIVEL9(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_NIVEL9
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_NIVEL9
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_TIP_REV(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(6)
AS     
BEGIN
	declare @Regresa Char(6);
	set @Regresa = ( select TOP 1 CB_TIP_REV
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_TIP_REV
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_CONTRAT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(1)
AS     
BEGIN
	declare @Regresa Char(1);
	set @Regresa = ( select TOP 1 CB_CONTRAT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_CONTRAT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_PATRON(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(1)
AS     
BEGIN
	declare @Regresa Char(1);
	set @Regresa = ( select TOP 1 CB_PATRON
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_PATRON
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_TABLASS(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(1)
AS     
BEGIN
	declare @Regresa Char(1);
	set @Regresa = ( select TOP 1 CB_TABLASS
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_TABLASS
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_ZONA_GE(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(1)
AS     
BEGIN
	declare @Regresa Char(1);
	set @Regresa = ( select TOP 1 CB_ZONA_GE
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_ZONA_GE
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOT_BAJ(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Char(3)
AS     
BEGIN
	declare @Regresa Char(3);
	set @Regresa = ( select TOP 1 CB_MOT_BAJ
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_MOT_BAJ
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_AUTOSAL(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS     
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_AUTOSAL
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_AUTOSAL
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_GLOBAL(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS     
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_GLOBAL
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_GLOBAL
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_REINGRE(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS     
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_REINGRE
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_REINGRE
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_COMENTA(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Observaciones
AS     
BEGIN
	declare @Regresa Observaciones;
	set @Regresa = ( select TOP 1 CB_COMENTA
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_COMENTA
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FAC_INT(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Tasa
AS     
BEGIN
	declare @Regresa Tasa;
	set @Regresa = ( select TOP 1 CB_FAC_INT
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FAC_INT
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV1(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV1 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV1 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV2(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV2 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV2 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV3(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV3 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV3 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV4(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV4 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV4 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV5(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV5 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV5 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV6(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV6 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV6 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV7(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV7 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV7 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV8(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV8 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV8 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_MOD_NV9(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV9 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV9 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

create function DBO.SP_GET_TURNO( @Fecha DATETIME, @Empleado INTEGER ) returns CHAR(6)
as
begin
     declare @FechaKardex DATETIME;
     declare @Turno CHAR(6);
     select @Turno = CB_TURNO, @FechaKardex = CB_FEC_KAR from COLABORA where ( CB_CODIGO = @Empleado );
     if ( @Fecha < @FechaKardex )
     begin
          select @TURNO = CB_TURNO from SP_FECHA_KARDEX( @Fecha, @Empleado );
     end
     return @Turno;
end
GO

CREATE FUNCTION SP_KARDEX_CB_NOMINA( @FECHA DATETIME, @EMPLEADO INTEGER ) RETURNS NominaTipo AS
BEGIN
  declare @Regresa NominaTipo;
  set @Regresa = ( select TOP 1 CB_NOMINA from KARDEX WHERE CB_CODIGO = @Empleado and
                   CB_FECHA <= @Fecha order by CB_FECHA desc, CB_NIVEL desc )
  if ( @Regresa is NULL )
      set @Regresa = ( select TOP 1 CB_NOMINA from KARDEX WHERE CB_CODIGO = @Empleado
                       order by CB_FECHA, CB_NIVEL )
  RETURN COALESCE( @Regresa, 0)
END
GO

CREATE FUNCTION SP_KARDEX_CB_PLAZA( @FECHA DATETIME, @EMPLEADO INTEGER ) RETURNS FolioGrande AS
BEGIN
  declare @Regresa FolioGrande;
  set @Regresa = ( select TOP 1 CB_PLAZA from KARDEX WHERE CB_CODIGO = @Empleado and
                   CB_FECHA <= @Fecha order by CB_FECHA desc, CB_NIVEL desc )
  if ( @Regresa is NULL )
      set @Regresa = ( select TOP 1 CB_PLAZA from KARDEX WHERE CB_CODIGO = @Empleado
                       order by CB_FECHA, CB_NIVEL )
  RETURN COALESCE( @Regresa, 0 )
END
GO

CREATE FUNCTION SP_GET_NOMTIPO( @Fecha DATETIME, @Empleado INTEGER ) returns NominaTipo as
begin
	declare @FechaKardex DATETIME;
	declare @NOMTIPO NominaTipo;
	select @NOMTIPO = CB_NOMINA, @FechaKardex = CB_FEC_NOM from COLABORA where ( CB_CODIGO = @Empleado );
	if ( @Fecha < @FechaKardex )
	begin
		select @NOMTIPO  = dbo.SP_KARDEX_CB_NOMINA( @Fecha, @Empleado );
    end
	return @NOMTIPO;
end
GO

CREATE FUNCTION SP_KARDEX_CB_RECONTR(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_RECONTR
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_RECONTR
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, 'S' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_NOTA(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Formula
AS
BEGIN
	declare @Regresa Formula;
	set @Regresa = ( select TOP 1 Substring( CB_NOTA, 1, 255 )
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 Substring( CB_NOTA, 1, 255 )
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '' )
END
GO

CREATE FUNCTION SP_KARDEX_CB_FEC_COV(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Fecha
AS
BEGIN
	declare @Regresa Fecha;
	set @Regresa = ( select TOP 1 CB_FEC_COV
			  from KARDEX
			  WHERE CB_CODIGO = @Empleado and CB_FECHA <= @Fecha
			  order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = 	( select TOP 1 CB_FEC_COV
				  from KARDEX
				  WHERE CB_CODIGO = @Empleado
				  order by CB_FECHA, CB_NIVEL )

	RETURN COALESCE( @Regresa, '12/30/1899' )
END
GO

