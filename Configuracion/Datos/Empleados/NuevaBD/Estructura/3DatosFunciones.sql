CREATE FUNCTION SP_AS (
  @Concepto SMALLINT,
  @MesIni SMALLINT,
  @MesFin SMALLINT,
  @Anio SMALLINT,
  @Empleado INTEGER )
RETURNS NUMERIC(15,2)
AS   
BEGIN
  declare @Mes_01 NUMERIC(15,2)
  declare @Mes_02 NUMERIC(15,2)
  declare @Mes_03 NUMERIC(15,2)
  declare @Mes_04 NUMERIC(15,2)
  declare @Mes_05 NUMERIC(15,2)
  declare @Mes_06 NUMERIC(15,2)
  declare @Mes_07 NUMERIC(15,2)
  declare @Mes_08 NUMERIC(15,2)
  declare @Mes_09 NUMERIC(15,2)
  declare @Mes_10 NUMERIC(15,2)
  declare @Mes_11 NUMERIC(15,2)
  declare @Mes_12 NUMERIC(15,2)
  declare @Mes_13 NUMERIC(15,2)
  declare @Resultado Pesos

  select @Mes_01 = AC_MES_01, @Mes_02 = AC_MES_02, @Mes_03 = AC_MES_03, @Mes_04 = AC_MES_04,
         @Mes_05 = AC_MES_05, @Mes_06 = AC_MES_06, @Mes_07 = AC_MES_07, @Mes_08 = AC_MES_08,
         @Mes_09 = AC_MES_09, @Mes_10 = AC_MES_10, @Mes_11 = AC_MES_11, @Mes_12 = AC_MES_12, @Mes_13 = AC_MES_13
  from ACUMULA
  where ( CB_CODIGO = @Empleado ) AND 
        ( AC_YEAR = @Anio  ) AND 
        ( CO_NUMERO = @Concepto )

  select @Resultado = 0
  if ( @MesIni <= 1  and  @MesFin >= 1 ) select @Resultado=@Resultado+@Mes_01
  if ( @MesIni <= 2  and  @MesFin >= 2 ) select @Resultado=@Resultado+@Mes_02
  if ( @MesIni <= 3  and  @MesFin >= 3 ) select @Resultado=@Resultado+@Mes_03
  if ( @MesIni <= 4  and  @MesFin >= 4 ) select @Resultado=@Resultado+@Mes_04
  if ( @MesIni <= 5  and  @MesFin >= 5 ) select @Resultado=@Resultado+@Mes_05
  if ( @MesIni <= 6  and  @MesFin >= 6 ) select @Resultado=@Resultado+@Mes_06
  if ( @MesIni <= 7  and  @MesFin >= 7 ) select @Resultado=@Resultado+@Mes_07
  if ( @MesIni <= 8  and  @MesFin >= 8 ) select @Resultado=@Resultado+@Mes_08
  if ( @MesIni <= 9  and  @MesFin >= 9 ) select @Resultado=@Resultado+@Mes_09
  if ( @MesIni <= 10 and  @MesFin >= 10 ) select @Resultado=@Resultado+@Mes_10
  if ( @MesIni <= 11 and  @MesFin >= 11 ) select @Resultado=@Resultado+@Mes_11
  if ( @MesIni <= 12 and  @MesFin >= 12 ) select @Resultado=@Resultado+@Mes_12
  if ( @MesIni <= 13 and  @MesFin >= 13 ) select @Resultado=@Resultado+@Mes_13
 
  if @Resultado IS NULL select @Resultado = 0
  RETURN @Resultado
END
GO

CREATE FUNCTION SP_C(
  @Concepto SMALLINT,
  @Numero SMALLINT,
  @Tipo SMALLINT,
  @Anio SMALLINT,
  @Empleado INTEGER )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @Resultado AS NUMERIC(15,2)
	select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
	from   MOVIMIEN
	where ( CB_CODIGO = @Empleado ) AND
  		( PE_YEAR = @Anio ) AND
		( PE_TIPO = @Tipo ) AND
		( PE_NUMERO = @Numero ) AND
		( CO_NUMERO = @Concepto ) AND
		( MO_ACTIVO = 'S' )
      	if ( @Resultado is NULL )
       		select @Resultado = 0	
	RETURN @Resultado
END
GO

CREATE FUNCTION SP_CM
				( 	@CONCEPTO SMALLINT,
    				@REFERENCIA VARCHAR(8),
    				@NUMERO SMALLINT,
    				@TIPO SMALLINT,
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER)

RETURNS NUMERIC(15,2)
AS
begin
	Declare @Resultado NUMERIC(15,2)
    if ( @Referencia = '' ) 
    begin
    	Select @Resultado = dbo.SP_C( @Concepto, @Numero, @Tipo, @Anio, @Empleado )
    end
    else
    begin
 		select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
 		from   MOVIMIEN
        where ( CB_CODIGO = @Empleado ) 
		AND ( PE_YEAR = @Anio ) 
		AND ( PE_TIPO = @Tipo ) 
		AND ( PE_NUMERO = @Numero ) 
		AND ( CO_NUMERO = @Concepto ) 
		AND ( MO_REFEREN = @Referencia ) 
		AND ( MO_ACTIVO = 'S' )
    end
	RETURN @Resultado
end
GO

CREATE FUNCTION SP_CMS 
		(@CONCEPTO Concepto,
		@REFERENCIA Referencia,
		@NUMEROINICIAL NominaNumero,
		@NUMEROFINAL NominaNumero,
		@TIPO NominaTipo,
		@ANIO Anio,
		@EMPLEADO NumeroEmpleado)

RETURNS NUMERIC(15,2)
AS
begin
	Declare @Resultado Pesos
	if ( @Referencia = '' ) 
	begin
		Select @Resultado = dbo.SP_CS( @Concepto, @NumeroInicial, @NumeroFinal, @Tipo, @Anio, @Empleado )
	end
	else
	begin
		select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
		from   MOVIMIEN
		where ( CB_CODIGO = @Empleado ) 
		AND ( PE_YEAR = @Anio ) 
		AND ( PE_TIPO = @Tipo ) 
		AND ( PE_NUMERO between @NumeroInicial and @NumeroFinal ) 
		AND ( CO_NUMERO = @Concepto ) 
		AND ( MO_REFEREN = @Referencia ) 
		AND ( MO_ACTIVO = 'S' )
	end
	RETURN @Resultado
end
GO

CREATE FUNCTION SP_CODIGO_OP(
  					@EMPLEADO INTEGER,
    				@FECHA DATETIME,
    				@TIPO CHAR(6),
    				@NUMERO SMALLINT)
RETURNS CHAR(2)
AS
BEGIN
	DECLARE @RESULTADO CHAR(2)
    Select @Resultado = KF_CODIGO FROM KAR_FIJA
            where CB_CODIGO = @Empleado 
			and  CB_FECHA = @Fecha 
			and CB_TIPO = @Tipo 
			and KF_FOLIO= @Numero
	Return @Resultado
END
GO

CREATE FUNCTION SP_CS( 
		@CONCEPTO SMALLINT,
    		@NUMEROINI SMALLINT,
    		@NUMEROFIN SMALLINT,
    		@TIPO SMALLINT,
    		@ANIO SMALLINT,
    		@EMPLEADO INTEGER)
RETURNS NUMERIC(15,2)
AS
begin
	declare @Temporal NUMERIC(15,2);
	declare @Resultado NUMERIC(15,2);

  	select @Resultado = 0;
  	while ( @NumeroIni <= @NumeroFin ) 
  	begin
	    Select @Temporal = dbo.SP_C ( @Concepto, @NumeroIni, @Tipo, @Anio, @Empleado )
    	if ( @Temporal is NOT NULL ) 
      		select @Resultado = @Resultado + @Temporal;
    	Select @NumeroIni = @NumeroIni + 1;
  	end
	Return @Resultado;
end
GO

CREATE FUNCTION SP_FECHA_KARDEX (
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS @RegKardex TABLE
( CB_FECHA DATETIME,
  CB_TIPO CHAR(6),
  CB_AUTOSAL CHAR(1),
  CB_CLASIFI CHAR(6),
  CB_COMENTA VARCHAR(50),
  CB_CONTRAT CHAR(1),
  CB_FAC_INT NUMERIC(15, 5),
  CB_FEC_CAP DATETIME,
  CB_FEC_CON DATETIME,
  CB_FEC_INT DATETIME,
  CB_FEC_REV DATETIME,
  CB_FECHA_2 DATETIME,
  CB_GLOBAL CHAR(1),
  CB_TURNO CHAR(6),
  CB_MONTO NUMERIC(15, 2),
  CB_MOT_BAJ CHAR(3),
  CB_OLD_INT NUMERIC(15, 2),
  CB_OLD_SAL NUMERIC(15, 2),
  CB_OTRAS_P NUMERIC(15, 2),
  CB_PATRON CHAR(1),
  CB_PER_VAR NUMERIC(15, 2),
  CB_PRE_INT NUMERIC(15, 2),
  CB_PUESTO CHAR(6),
  CB_RANGO_S NUMERIC(15, 2),
  CB_SAL_INT NUMERIC(15, 2),
  CB_SAL_TOT NUMERIC(15, 2),
  CB_SALARIO NUMERIC(15, 2),
  CB_STATUS SMALLINT,
  CB_TABLASS CHAR(1),
  CB_TOT_GRA NUMERIC(15, 2),
  US_CODIGO SMALLINT,
  CB_ZONA_GE CHAR(1),
  CB_NIVEL1 CHAR(6),
  CB_NIVEL2 CHAR(6),
  CB_NIVEL3 CHAR(6),
  CB_NIVEL4 CHAR(6),
  CB_NIVEL5 CHAR(6),
  CB_NIVEL6 CHAR(6),
  CB_NIVEL7 CHAR(6),
  CB_NIVEL8 CHAR(6),
  CB_NIVEL9 CHAR(6),
  CB_NOMTIPO SMALLINT,
  CB_NOMYEAR SMALLINT,
  CB_NOMNUME SMALLINT,
  CB_REINGRE CHAR(1),
  CB_FEC_ING DATETIME,
  CB_FEC_ANT DATETIME,
  CB_TIP_REV CHAR(6),
  CB_FEC_SAL DATETIME,
  CB_PLAZA INTEGER,
  CB_NOMINA SMALLINT,
  CB_RECONTR CHAR(1),
  CB_FEC_COV DATETIME
) AS
begin
	INSERT INTO @RegKardex
	select TOP 1
	       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       	       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       	       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       	       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT,
               CB_TIP_REV, CB_FEC_SAL, CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
	from KARDEX
	WHERE ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	order by CB_FECHA desc, CB_NIVEL desc

	if ( @@ROWCOUNT = 0 )
	BEGIN
		INSERT INTO @RegKardex
		select TOP 1
		       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
	               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       		       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
	               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       		       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
	               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       		       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
	               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT,
	               CB_TIP_REV, CB_FEC_SAL, CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
		from KARDEX
		WHERE ( CB_CODIGO = @Empleado )
		order by CB_FECHA, CB_NIVEL

		if ( @@ROWCOUNT = 0 )
		BEGIN
      		select @Fecha = '12/30/1899';
			INSERT INTO @RegKardex
			VALUES ( @Fecha, '', 'N', '', '', '',
	        	       0, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha,
	        	       'N', '', 0, '', 0, 0, 0,
		               '', 0, 0, '', 0,
        		       0, 0, 0, 0, '', 0,
		               0, '', '', '', '',
        		       '', '', '', '', '', '',
	        	       0, 0, 0, 'N', @Fecha, @Fecha, '', @Fecha, 0, 0, 'S', @Fecha )
		END
	END

	RETURN
END
GO

CREATE FUNCTION SP_FECHA_VENCI (
	@Fecha Fecha, 
	@Tipo Codigo1 )
RETURNS Fecha
AS
BEGIN
	DECLARE @Resultado Fecha
	DECLARE @Dias Integer
  
	Select @Dias = dbo.SP_DIAS_VENCI( @Tipo )

	if ( @Dias <=0 ) 
		select @Resultado = '12/30/1899'
  	else
		select @Resultado = DATEADD( DAY, @Dias - 1, @Fecha )

	RETURN @Resultado
END
GO

CREATE FUNCTION SP_DIAS_VENCI ( 
	@Tipo Codigo1 )
RETURNS Dias
AS
BEGIN
	DECLARE @Resultado Dias
  
	Select @Resultado = TB_DIAS
  	from CONTRATO 
	where TB_CODIGO = @Tipo

	if ( @Resultado is NULL )
		select @Resultado = 0

	RETURN @Resultado
END
GO


CREATE FUNCTION SP_HORAS_NT(  
				@ORD NUMERIC(15,2),
    			@CONGOCE NUMERIC(15,2),
    			@SINGOCE NUMERIC(15,2),
    			@CODIGO CHAR(6))
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @JORNADA NUMERIC(15,2);
	DECLARE @RESULTADO NUMERIC(15,2);

    Select @Jornada = HO_JORNADA 
	from HORARIO 
	where HO_CODIGO = @Codigo
    
	Select @Resultado = @Jornada - @Ord - @ConGoce - @SinGoce;

    if ( @Resultado < 0 ) Select  @Resultado = 0;
	
	Return @Resultado

end
GO

CREATE FUNCTION SP_IMSS_YEAR_V( @sTABLA CHAR( 1 ),
                                @iYEAR Integer,
                                @Resultado Integer,
                                @iEmpleado Integer )
RETURNS NUMERIC(15,5)
AS
BEGIN
     declare @DIAS_VA	NUMERIC(15,2)
     declare @PRIMAVA	NUMERIC(15,5)
     declare @DIAS_AG	NUMERIC(15,2)
     declare @FACTOR	NUMERIC(15,5)
     declare @PRIMADO	NUMERIC(15,5)
     declare @PAGO_7	CHAR(1)
     declare @PRIMA_7	CHAR(1)
     declare @DIAS_AD	NUMERIC(15,2)
     declare @RES	NUMERIC(15,5)
     declare @DiasVACAColabora NUMERIC(15,2);
     declare @DiasAGColabora NUMERIC(15,2);
     declare @PrimaVAColabora NUMERIC(15,5);
     declare @rDiasDominical NUMERIC(15,2);
     declare @Temporal NUMERIC(15,2);
     declare @rVacaciones NUMERIC(15,2);

     if ( @iYear > 0 )
     begin
          select @iYear = ( select MIN( PT_YEAR ) from PRESTACI
                           where ( TB_CODIGO = @sTabla ) and ( PT_YEAR >= @iYear ) )
     end
     if ( ( @iYear <= 0 ) or ( @iYear is NULL ) )
     begin
          select @RES = 0
     end
     else
     begin
          select @DIAS_VA = PT_DIAS_VA,
                 @PRIMAVA = PT_PRIMAVA,
                 @DIAS_AG = PT_DIAS_AG,
                 @FACTOR = PT_FACTOR,
                 @PRIMADO = PT_PRIMADO,
                 @PAGO_7 = PT_PAGO_7,
                 @PRIMA_7 = PT_PRIMA_7,
                 @DIAS_AD = PT_DIAS_AD
          from PRESTACI where ( TB_CODIGO = @sTabla ) and ( PT_YEAR = @iYear );

          if ( @Resultado = 1 )
             select @RES = @DIAS_VA
          else
          if ( @Resultado = 2 )
             set @RES = @PRIMAVA
          else
          if ( @Resultado = 3 )
             set @RES = @DIAS_AG
          else
          if ( @Resultado = 4 )
             set @RES = @FACTOR
          else
          if ( @Resultado = 5 )
             set @RES = @PRIMADO
          else
          if ( @Resultado = 6 )
          begin
               if ( @PAGO_7 = 'S' )
                  set @RES = 1
               else
                   set @RES = 0
          end
          else
          if ( @Resultado = 7 )
          begin
               if ( @PRIMA_7 = 'S' )
                  set @RES = 1
               else
                   set @RES = 0
          end
          else
          begin
               set @RES = @DIAS_AD
          end
     end
     return @RES
END
GO

CREATE FUNCTION SP_IMSS_YEAR( @sTABLA CHAR(1),
                              @iYEAR Integer,
                              @Resultado Integer )
RETURNS NUMERIC(15,5)
AS
BEGIN
     RETURN DBO.SP_IMSS_YEAR_V( @sTABLA, @iYEAR, @Resultado, 0 )
END
GO

CREATE FUNCTION ANIOSCOMPLETOS
(
        @FromDate DATETIME,
        @ToDate DATETIME
)
RETURNS INT
AS
BEGIN
        RETURN  CASE
                       WHEN @FromDate > @ToDate THEN NULL
                       WHEN DATEPART(day, @FromDate) > DATEPART(day, @ToDate) THEN DATEDIFF(month, @FromDate, @ToDate) - 1
                       ELSE DATEDIFF(month, @FromDate, @ToDate)
               END / 12
END
GO

CREATE FUNCTION SP_IMSS_V( @TABLA CHAR(1),
  		                     @INICIAL DATETIME,
  		                     @FINAL DATETIME,
		                       @RESULTADO INTEGER,
                           @EMPLEADO INTEGER )
RETURNS NUMERIC(15, 5)
AS
BEGIN
     declare @DiasAntig NUMERIC(15,2);
     declare @Anio INTEGER;
     declare @LastAniv DATETIME;
     if ( @Final < @Inicial )
     begin
          select @Anio = 0
     end
     else
     begin
    		  select @Anio = dbo.AniosCompletos( @Inicial, @Final )
          select @LastAniv = DATEADD(year, @Anio, @Inicial)
          if ( @LastAniv < @Final )
          begin
               select @Anio = @Anio + 1
          end
     end
     RETURN DBO.SP_IMSS_YEAR_V( @Tabla, @Anio, @RESULTADO, @EMPLEADO )
END
GO

CREATE FUNCTION SP_IMSS( @TABLA CHAR(1),
  		         @INICIAL DATETIME,
  		         @FINAL DATETIME,
		         @RESULTADO INTEGER )
RETURNS NUMERIC(15, 5)
AS
BEGIN
     RETURN DBO.SP_IMSS_V( @TABLA, @INICIAL, @FINAL, @RESULTADO, 0 )
END
GO

CREATE FUNCTION SP_GET_RENGLON( @Tabla CHAR( 1 ),
                                @Anio INTEGER,
                                @Empleado INTEGER )
RETURNS @Prestaci TABLE ( PT_FACTOR NUMERIC( 15, 5 ),
                          PT_DIAS_VA NUMERIC( 15, 2 ),
                          PT_DIAS_AG NUMERIC( 15, 2 ),
                          PT_PRIMAVA NUMERIC( 15, 5 ),
                          PT_PRIMA_7 CHAR( 1 ),
                          PT_PAGO_7 CHAR( 1 ) )
AS
begin
     insert into @Prestaci
     select TOP 1 PT_FACTOR,
                  PT_DIAS_VA,
                  PT_DIAS_AG,
                  PT_PRIMAVA,
                  PT_PRIMA_7,
                  PT_PAGO_7
     from PRESTACI where ( TB_CODIGO = @Tabla ) and ( PT_YEAR >= @Anio )
     order by PT_YEAR;
     RETURN
end
GO

CREATE FUNCTION SP_GET_CACHE( @Tabla CHAR( 1 ), @Empleado INTEGER )
RETURNS @Prestaci TABLE ( PT_YEAR SMALLINT,
                          PT_DIAS_VA NUMERIC( 15, 2 ),
                          PT_PRIMAVA NUMERIC( 15, 5 ) )
AS
begin
     insert into @Prestaci
     select PT_YEAR, PT_DIAS_VA, PT_PRIMAVA from PRESTACI
     where ( TB_CODIGO = @Tabla )
     order by PT_YEAR;
     RETURN
end
GO

CREATE FUNCTION SP_OLD_CLASIFI (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_CLASIFI, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'PUESTO','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_CONTRATO (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(1),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_CONTRAT, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'RENOVA','CIERRE' )
          order by CB_FECHA desc, CB_NIVEL desc 

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else 
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_TABLASS (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(1),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_TABLASS, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'PRESTA','CIERRE' )
          order by CB_FECHA desc, CB_NIVEL desc 

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else 
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_TURNO (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_TURNO, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'TURNO','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_PUESTO (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_PUESTO, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'PUESTO','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL1 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL1, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL2 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL2, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL3 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL3, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL4 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL4, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL5 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL5, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL6 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL6, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL7 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL7, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE','PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL8 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL8, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_NIVEL9 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL9, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE FUNCTION SP_OLD_PLAZA (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado INTEGER,
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual INTEGER;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_PLAZA, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'PLAZA','CIERRE' )
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

CREATE  FUNCTION SP_OLD_CODIGO6( @Empleado int, @Tipo int )
RETURNS CHAR(6)
AS
BEGIN
     declare @Resultado CHAR(6)
     if @Tipo = 1
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL1( @Empleado )
     else if @Tipo = 2
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL2( @Empleado )
     else if @Tipo = 3
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL3( @Empleado )
     else if @Tipo = 4
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL4( @Empleado )
     else if @Tipo = 5
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL5( @Empleado )
     else if @Tipo = 6
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL6( @Empleado )
     else if @Tipo = 7
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL7( @Empleado )
     else if @Tipo = 8
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL8( @Empleado )
     else if @Tipo = 9
	select @Resultado = RESULTADO from dbo.SP_OLD_NIVEL9( @Empleado )
     else if @Tipo = 10
	select @Resultado = RESULTADO from dbo.SP_OLD_PUESTO( @Empleado )
     else if @Tipo = 11
	select @Resultado = RESULTADO from dbo.SP_OLD_CLASIFI( @Empleado )
     else if @Tipo = 12
	select @Resultado = RESULTADO from dbo.SP_OLD_TURNO( @Empleado )
     else
	select @Resultado = ''
    RETURN @Resultado
END
GO

CREATE  FUNCTION SP_OLD_CODIGO1( @Empleado int, @Tipo int )
RETURNS CHAR(1)
AS
BEGIN
     declare @Resultado CHAR(1)
     if @Tipo = 13
	select @Resultado = RESULTADO from dbo.SP_OLD_TABLASS( @Empleado )
     else if @Tipo = 14
	select @Resultado = RESULTADO from dbo.SP_OLD_CONTRATO( @Empleado )
     else
	select @Resultado = ''
    RETURN @Resultado
END
GO

CREATE FUNCTION SP_OLD_CODIGO_PLAZA( @Empleado int )
RETURNS INTEGER
AS
BEGIN
     declare @Resultado INTEGER
     select @Resultado = RESULTADO from dbo.SP_OLD_PLAZA( @Empleado )
     RETURN @Resultado
END
GO

CREATE FUNCTION SP_OLD_FECHA( @Empleado int, @Tipo int )
RETURNS DATETIME
AS
BEGIN
     declare @Resultado DATETIME
     if @Tipo = 1
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL1( @Empleado )
     else if @Tipo = 2
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL2( @Empleado )
     else if @Tipo = 3
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL3( @Empleado )
     else if @Tipo = 4
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL4( @Empleado )
     else if @Tipo = 5
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL5( @Empleado )
     else if @Tipo = 6
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL6( @Empleado )
     else if @Tipo = 7
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL7( @Empleado )
     else if @Tipo = 8
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL8( @Empleado )
     else if @Tipo = 9
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL9( @Empleado )
     else if @Tipo = 10
	select @Resultado = FECHA from dbo.SP_OLD_PUESTO( @Empleado )
     else if @Tipo = 11
	select @Resultado = FECHA from dbo.SP_OLD_CLASIFI( @Empleado )
     else if @Tipo = 12
	select @Resultado = FECHA from dbo.SP_OLD_TURNO( @Empleado )
     else if @Tipo = 13
	select @Resultado = FECHA from dbo.SP_OLD_TABLASS( @Empleado )
     else if @Tipo = 14
	select @Resultado = FECHA from dbo.SP_OLD_CONTRATO( @Empleado )
	 else if @Tipo = 15
	select @Resultado = FECHA from dbo.SP_OLD_PLAZA( @Empleado )
     else
	select @Resultado = ''
    RETURN @Resultado
END
GO

CREATE FUNCTION SP_OP(
				@CODIGO CHAR(2) ,
    			@SALARIO NUMERIC(15,2))
RETURNS NUMERIC(15,2)
AS
begin
	DECLARE @TIPO INTEGER;
	DECLARE @MONTO NUMERIC(15,2);
	DECLARE @TASA NUMERIC(15,5);
	DECLARE @Resultado NUMERIC(15,5);
     
	Select @Tipo = TB_TIPO, @Monto = TB_MONTO, @Tasa = TB_TASA from OTRASPER 
            where TB_CODIGO = @Codigo

    if ( @Tipo = 0 ) 
        Select @Resultado = @Monto;
     else 
        Select @Resultado = (( @Salario  * @Tasa ) / 100) ;

	Return @Resultado
end
GO

CREATE FUNCTION SP_EMP_INCA
	(@FechaIni DateTime,
	@FechaFin DateTime,
	@Empleado int)
RETURNS Char(1)
AS
BEGIN
	Declare @Cuantos int
	Declare @Resultado char(1)

	select @Cuantos = dbo.SP_STATUS_INCA( @FechaIni, @FechaFin, @Empleado)

     	if ( @Cuantos > 0 ) 
	begin
		set @Resultado = 'S'
	end
     	else
	begin 
		set @Resultado = 'N';
	end
	
	Return @Resultado
END
GO

CREATE FUNCTION SP_EMP_PERM
	(@FechaIni DateTime,
	@FechaFin DateTime,
	@Empleado int)
RETURNS Char(1)
AS
BEGIN
	Declare @Cuantos int
	Declare @Resultado char(1)

	select @Cuantos = dbo.SP_STATUS_PERM( @FechaIni, @FechaFin, -1, @Empleado)

     	if ( @Cuantos > 0 ) 
	begin
		set @Resultado = 'S'
	end
     	else
	begin 
		set @Resultado = 'N';
	end
	
	Return @Resultado
END
GO

CREATE FUNCTION SP_EMP_VACA
	(@FechaIni DateTime,
	@FechaFin DateTime,
	@Empleado int)
RETURNS Char(1)
AS
BEGIN
	Declare @Cuantos int
	Declare @Resultado char(1)

	select @Cuantos = dbo.SP_STATUS_VACA( @FechaIni, @FechaFin, @Empleado)

     	if ( @Cuantos > 0 ) 
	begin
		set @Resultado = 'S'
	end
     	else
	begin 
		set @Resultado = 'N';
	end
	
	Return @Resultado
END
GO

CREATE FUNCTION SP_VALIDA_INGRESO (	  				
					@FECHA DATETIME,
    				@BAJA DATETIME,
    				@EMPLEADO INTEGER )
RETURNS int
AS
BEGIN
	Return( select COUNT(CB_TIPO) 
	from Kardex 
	where ( CB_CODIGO = @Empleado ) AND 
          ( CB_TIPO <> 'ALTA' ) AND
          ( CB_FECHA < @Fecha ) AND
          ( CB_FECHA > @Baja ))

END
GO

CREATE FUNCTION SP_V_FIJAS(
@TABLA CHAR(1),
@FECHAINI DATETIME,
@FECHAFIN DATETIME)
RETURNS NUMERIC(15,2)
AS
begin
	DECLARE @DIASANTIG NUMERIC(15,2);
	DECLARE @RESULTADO NUMERIC(15,2);
	DECLARE @ANTIGUEDAD SMALLINT;
	DECLARE @DIASIDEALES NUMERIC(15,2);
	DECLARE @DIFERENCIA SMALLINT;
	DECLARE @AntiguedadTemp SMALLINT;

	Select @DiasAntig = DATEDIFF(day, @FechaIni , @FechaFin) + 1;
  	if ( @DiasAntig < 0 ) Select @DiasAntig = 0;
  	Select @Antiguedad = ROUND( @DiasAntig / 365.25 , 0 );
  	if ( @Antiguedad > ( @DiasAntig / 365.25 ))
		Select @Antiguedad = @Antiguedad - 1;
  	Select @Diferencia = ROUND( @DiasAntig - ( @Antiguedad * 365.25 ) ,0 );
  	if ( @Diferencia = 365 )
  	begin
    	Select @Antiguedad = @Antiguedad + 1, @Diferencia = 0;
  	end

	Select @AntiguedadTemp = @Antiguedad+1;

  	select @DiasIdeales = dbo.SP_IMSS_YEAR( @Tabla, @AntiguedadTemp, 1 )
  	select @Resultado = @DiasIdeales * ( @Diferencia / 365.00 );

  	while ( @Antiguedad > 0 )
  	begin
    	select @DiasIdeales = dbo.SP_IMSS_YEAR( @Tabla, @Antiguedad, 1 )

    	Select @Resultado = @Resultado + @DiasIdeales;
    	Select @Antiguedad = @Antiguedad - 1;
  	end
	Return @Resultado;
end
GO

create function DBO.SP_PROM_SAL_MIN(
@FECHAINI FECHA,
@FECHAFIN FECHA,
@ZONAGEO CHAR( 1 ) )
returns NUMERIC( 15, 2 )
as
begin
     declare @Resultado Numeric( 15, 2 );
     declare @dFinal FECHA;
     declare @dCorte FECHA;
     declare @nDias  INTEGER;
     declare @nDiasTotales INTEGER;
     declare @nZonaA NUMERIC( 15, 2 );
     declare @nZonaB NUMERIC( 15, 2 );
     declare @nZonaC NUMERIC( 15, 2 );
     declare @nSalMin NUMERIC( 15, 2 );
     declare @nTotSalMin NUMERIC( 15, 2 );
     set @Resultado = 0;
     if ( @FechaFin >= @FechaIni )
     begin
          set @nTotSalMin = 0;
          set @nDiasTotales = CONVERT( SmallInt, ( @FECHAFIN - @FECHAINI ) ) + 1;
          set @dFinal = @FECHAFIN + 1;
          declare Temporal cursor for
             select S.SM_FEC_INI, S.SM_ZONA_A, S.SM_ZONA_B, S.SM_ZONA_C
             from DBO.SAL_MIN as S where ( S.SM_FEC_INI <= @dFinal )
             order by S.SM_FEC_INI desc;
          open Temporal;
          fetch NEXT from Temporal into @dCorte, @nZonaA, @nZonaB, @nZonaC;
          while @@Fetch_Status = 0
          begin
               if ( @FECHAINI > @dCorte )
               begin
                    set @dCorte = @FECHAINI;
               end;
               set @nDias = CONVERT( SmallInt, ( @dFinal - @dCorte ) );
               if ( @ZONAGEO = 'A' )
               begin
                    set @nSalMin = @nZonaA;
               end
               else
                   if ( @ZONAGEO = 'B' )
                   begin
                        set @nSalMin = @nZonaB;
                   end
                   else
                   begin
                        set @nSalMin = @nZonaC;
                   end;
               set @nTotSalMin = @nTotSalMin + ( @nDias * @nSalMin );
               set @Resultado = @nTotSalMin / @nDiasTotales;
               set @dFinal = @dCorte;
               if ( @dFinal > @FECHAINI )
               begin
                    fetch NEXT from Temporal into @dCorte, @nZonaA, @nZonaB, @nZonaC;
               end
               else
               begin
                    break;
               end;
          end
     end;
     RETURN @Resultado;
end
GO

CREATE  FUNCTION SP_SALARIO_CLA(  
					@CLASIFI CHAR(6),
    				@RANGO NUMERIC(15,2),
					@RESULTADO INTEGER)
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @OP1 CHAR(2);
	DECLARE @OP2 CHAR(2);
	DECLARE @OP3 CHAR(2);
	DECLARE @OP4 CHAR(2);
	DECLARE @OP5 CHAR(2);
	DECLARE @MONTOOP NUMERIC(15,2);
	DECLARE @Salario NUMERIC(15,2);
	DECLARE @Presta NUMERIC(15,2);
	

  	select 	@Salario=TB_SALARIO, 
			@OP1 = TB_OP1, 
			@OP2 = TB_OP2, 
			@OP3 = TB_OP3, 
			@OP4 = TB_OP4, 
			@OP5 = TB_OP5
  	from   	CLASIFI
  	where  TB_CODIGO = @Clasifi
  	
  	Select @Presta  = 0;
  	if ( @Salario is NULL ) 
    	Select @Salario = 0;
  	else
  	begin
    	if ( @OP1 > '' ) 
    	begin
      		Select @Presta = @Presta + dbo.SP_OP( @OP1, @Salario );
    	end
    	if ( @OP2 > '' ) 
    	begin
      		select @Presta = @Presta + dbo.SP_OP( @OP2, @Salario )
    	end
    	if ( @OP3 > '' ) 
    	begin
      		Select @Presta = @Presta + dbo.SP_OP( @OP3, @Salario ) 
    	end
    	if ( @OP4 > '' ) 
    	begin
      		select @Presta = @Presta + dbo.SP_OP( @OP4, @Salario ) 
      	end
    	if ( @OP5 > '' ) 
    	begin
      		select @Presta = @Presta + dbo.SP_OP( @OP5, @Salario ) 
      	end
  	end

  	select @Presta = CAST( @Presta*100 as INTEGER );
  	select @Presta = @Presta / 100;

  	if (( @Rango > 0 ) and ( @Rango <> 100 ) and ( @Salario > 0 )) 
  	begin
    	select @Rango   = @Rango / 100;
    	select @Salario = ( @Rango + ( @Presta / @Salario ) * ( @Rango - 1 )) * @Salario;
    	select @Salario = CAST( @Salario*100 as INTEGER );
    	select @Salario = @Salario / 100;
  	end
  	
	if @Resultado = 2
		Select @Salario = @Presta
	
	Return @Salario	
END
GO

CREATE FUNCTION SP_RANGO_ACTIVO(  
					@EMPLEADO INTEGER,
    				@FECHAINI DATETIME,
    				@FECHAFIN DATETIME )
RETURNS int
AS
BEGIN
	DECLARE @FEC_ING DATETIME;
	DECLARE @FEC_BAJ DATETIME;
	DECLARE @FEC_VAC DATETIME;
	DECLARE @CUANTOS INTEGER;
	DECLARE @RESULTADO INTEGER;

  	select 	@FEC_ING = CB_FEC_ING, 
			@FEC_BAJ = CB_FEC_BAJ, 
			@FEC_VAC = CB_FEC_VAC
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if (( @FEC_ING <= @FECHAINI ) 
	AND (( @FEC_BAJ = '12/30/1899' ) 
	OR ( @FEC_BAJ >= @FECHAFIN ))) 
    	SELECT @Resultado = 1;
  	else
    	SELECT @Resultado = 0;

  	if ( @Resultado = 1 ) 
  	begin
    	if ( @FEC_VAC > '12/30/1899' ) 
    	begin
      		select @CUANTOS = COUNT(*)
      		from   VACACION
      		where  CB_CODIGO = @Empleado 
			and VA_FEC_INI <= @FechaFin 
			and VA_FEC_FIN > @FechaIni and VA_TIPO = 1
      		
			if ( @Cuantos > 0 ) SELECT @Resultado = 0;
    	end
  	end

  	if ( @Resultado = 1 ) 
  	begin
    	select @Cuantos = COUNT(*)
    	from INCAPACI
    	where ( CB_CODIGO = @Empleado ) 
		AND ( IN_FEC_INI <= @FechaFin ) 
		AND ( IN_FEC_FIN > @FechaIni )
    	
		if ( @Cuantos > 0 ) select @Resultado = 0;
  	end

  	if ( @Resultado = 1 ) 
  	begin
    	select @Cuantos=COUNT(*)
    	from PERMISO
    	where ( CB_CODIGO = @Empleado ) 
		AND ( PM_FEC_INI <= @FechaFin ) 
		AND ( PM_FEC_FIN > @FechaIni )
    
    	if ( @Cuantos > 0 ) select @Resultado = 0;
  	end
	Return @Resultado
END
GO

CREATE FUNCTION SP_PROM_SALARIO(@FECHAINI DATETIME,
    				@FECHAFIN DATETIME,
    				@FECHAING DATETIME,
    				@FECHACAM DATETIME,
    				@OLDSAL Pesos,
    				@NEWSAL Pesos)
RETURNS Pesos
AS
BEGIN
	DECLARE @TOTDIAS INTEGER;
	DECLARE @OLDDIAS INTEGER;
	DECLARE @NEWDIAS INTEGER;
  	
	if ( @FechaIni < @FechaIng )
    		SET @FechaIni = @FechaIng

  	SET @TotDias = DATEDIFF( day, @FechaIni, @FechaFin  ) + 1
  	SET @OldDias = DATEDIFF( day, @FechaIni, @FechaCam )
  	SET @NewDias = @TotDias - @OldDias

  	Return  (( @OldSal * @OldDias ) + ( @NewSal * @NewDias )) / @TotDias;
END
GO

CREATE FUNCTION SP_RANGO_STATUS_ACT (
					@EMPLEADO INTEGER,
    				@FECHAINI DATETIME,
    				@FECHAFIN DATETIME,
					@TIPO INTEGER )

RETURNS INTEGER
AS
BEGIN
	DECLARE @REINGRESO CHAR(1);
	DECLARE @TEMPOINI DATETIME;
	DECLARE @TEMPOFIN DATETIME;
	DECLARE @FECHAALTA DATETIME;
	DECLARE @FECHABAJA DATETIME;
	DECLARE @DIAS_ACT INTEGER ;
    DECLARE @DIAS_BAJ INTEGER ;
	DECLARE @DIAS_ANT INTEGER ;

  	Select @Reingreso = 'N', @Dias_Act  = 0, @Dias_Baj  = 0
	Select @Dias_Ant  = 0, @TempoFin  = @FechaIni


   	DECLARE Temporal CURSOR FOR   
    	select CB_ALTA, CB_BAJA
    	from   ALTABAJA
    	where ( CB_CODIGO = @Empleado )
    	order by CB_ALTA
	OPEN Temporal
   	FETCH NEXT FROM Temporal INTO @FechaAlta, @FechaBaja
    
	WHILE @@FETCH_STATUS = 0
    BEGIN
		if ( @FechaIni >= @TempoFin ) 
        	Select @TempoIni = @FechaIni;
      	else
        	Select @TempoIni = @TempoFin;
      	if ( @FechaFin >= @FechaAlta ) 
        	Select @TempoFin = DateAdd(day, -1,@FechaAlta);
      	else
        	Select @TempoFin = @FechaFin;
      	if ( @TempoFin >= @TempoIni ) 
      	begin
        	if ( @Reingreso = 'S' ) 
          		Select @Dias_Baj = @Dias_Baj + DateDiff( day, @TempoIni, @TempoFin  ) + 1;
        	else
          		Select @Dias_Ant = @Dias_Ant + DateDiff( day, @TempoIni, @TempoFin  ) + 1;
      	end
      
      	if ( @FechaIni >= @FechaAlta ) 
        	Select @TempoIni = @FechaIni;
      	else
        	Select @TempoIni = @FechaAlta;
      	if (( @FechaBaja IS NULL ) or ( @FechaFin <= @FechaBaja )) 
        	Select @TempoFin = @FechaFin;
      	else
        	Select @TempoFin = @FechaBaja;

      	if ( @TempoFin >= @TempoIni ) 
        	Select @Dias_Act = @Dias_Act + DateDiff( day, @TempoIni, @TempoFin ) + 1;
      
      	select @TempoFin  = @TempoFin + 1;
      	select @Reingreso = 'S';
   		
		FETCH NEXT FROM Temporal INTO @FechaAlta, @FechaBaja
    END

  
  	if ( @FechaIni >= @TempoFin ) 
    	Select @TempoFin = @FechaIni;

  	if ( @FechaFin >= @TempoFin ) 
    	select @Dias_Baj = @Dias_Baj + DateDiff( day, @TempoFin, @FechaFin ) + 1;  

	if @Tipo = 0 SELECT @DIAS_ANT = @DIAS_ACT
    else if @Tipo = 1 SELECT @DIAS_ANT = @DIAS_BAJ
	
	Return @DIAS_ANT
   
END
GO

CREATE FUNCTION SP_RANGO_STATUS(  
					@EMPLEADO INTEGER,
    				@FECHAINI DATETIME,
    				@FECHAFIN DATETIME,
    				@STATUS SMALLINT)
RETURNS int
AS
BEGIN
	DECLARE @DACTIVO INTEGER;
	DECLARE @DBAJA INTEGER;
	DECLARE @DANTES INTEGER;
	DECLARE @RESULTADO INTEGER;
	DECLARE @DIAS_ACT INTEGER 
	DECLARE @DIAS_BAJ INTEGER 
	DECLARE @DIAS_ANT INTEGER 
  	Select @Resultado = 0; 
  	
	if ( @Status = 1 ) 
		select @Resultado = dbo.SP_RANGO_STATUS_ACT( @Empleado, @FechaIni, @FechaFin, 0 )
  	else if ( @Status = 9 ) 
    	select @Resultado = dbo.SP_RANGO_STATUS_ACT( @Empleado, @FechaIni, @FechaFin, 2 )
  	else if ( @Status = 2 ) 
    	select @Resultado = dbo.SP_STATUS_VACA( @FechaIni, @FechaFin, @Empleado )
  	else if ( @Status = 3 ) 
    	select @Resultado = dbo.SP_STATUS_INCA( @FechaIni, @FechaFin, @Empleado )
  	else if (( @Status >= 4 ) and ( @Status <= 8 )) 
  	begin
    	select @Status = @Status-4;
    	select @RESULTADO = dbo.SP_STATUS_PERM( @FechaIni, @FechaFin, @Status, @Empleado )
  	end
	Return @Resultado
end
GO

CREATE FUNCTION SP_STATUS_CONFLICTO
( @EMPLEADO INTEGER,
  @FECHAORIGINAL FECHA,
  @FECHAINICIAL FECHA,
  @FECHAFINAL FECHA,
  @ENTIDAD SMALLINT )
returns @VALORES table (
  RESULTADO INTEGER not null,
  INICIO DATETIME,
  FIN DATETIME )
as
begin
     declare @iClase Integer;
     declare @sActivo Char( 1 );
     declare @Valor Integer;
     declare @Inicial FECHA;
     declare @Final FECHA;
     set @Valor = 1;
     select @Inicial = C.CB_FEC_ING,
            @Final = C.CB_FEC_BAJ,
            @sActivo = C.CB_ACTIVO from DBO.COLABORA as C
     where ( C.CB_CODIGO = @EMPLEADO );
     if ( @FECHAINICIAL < @Inicial )
     begin
          set @Valor = 0;
     end
     else
     if ( ( @sActivo <> 'S' ) and ( @FECHAFINAL > @Final ) )
     begin
          set @Valor = 9;
     end
     else
     begin
          if ( @ENTIDAD = 1 )
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <> @FECHAORIGINAL ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          open Incapacidades;
          fetch NEXT from Incapacidades into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = 3;
               break;
          end;
	        close Incapacidades;
          deallocate Incapacidades;
          if ( @ENTIDAD = 2 )
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_GOZO > 0 ) and
                  ( V.VA_FEC_INI <> @FECHAORIGINAL ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_GOZO > 0 ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end;
          open Vacaciones;
          fetch NEXT from Vacaciones into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = 2;
		           break;
          end;
          close Vacaciones;
          deallocate Vacaciones;
          if ( @ENTIDAD = 3 )
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <> @FECHAORIGINAL ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end;
          open Permisos;
          fetch NEXT from Permisos into @iClase, @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = @iClase + 4;
		           break;
          end;
          close Permisos;
          deallocate Permisos;
          if ( @ENTIDAD = 4 )
          begin
               declare PlanVaca cursor for
                  select VP.VP_FEC_INI, VP.VP_FEC_FIN from DBO.VACAPLAN as VP where
                  ( VP.CB_CODIGO = @EMPLEADO ) and
                  ( VP.VP_STATUS in ( 0, 1 ) ) and          /* pendientes o autorizadas */
                  ( VP.VP_FEC_INI <> @FECHAORIGINAL ) and
                  ( VP.VP_FEC_INI <= @FECHAFINAL ) and
                  ( VP.VP_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare PlanVaca cursor for
                  select VP.VP_FEC_INI, VP.VP_FEC_FIN from DBO.VACAPLAN as VP where
                  ( VP.CB_CODIGO = @EMPLEADO ) and
                  ( VP.VP_STATUS in ( 0, 1 ) ) and          /* pendientes o autorizadas */
                  ( VP.VP_FEC_INI <= @FECHAFINAL ) and
                  ( VP.VP_FEC_FIN > @FECHAINICIAL );
          end;
          open PlanVaca;
          fetch NEXT from PlanVaca into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = 10;
		           break;
          end;
          close PlanVaca;
          deallocate PlanVaca;
     end;

     insert into @VALORES( RESULTADO, INICIO, FIN ) values ( @Valor, @Inicial, @Final );

     RETURN
end
GO

CREATE  FUNCTION AUTORIZACIONES_SELECT
(    @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS @RegAuto TABLE (
    Resultado NUMERIC(15,2),
    Motivo CHAR(4),
    US_COD_OK SMALLINT,
    HRS_APROB NUMERIC(15,2) )
AS
BEGIN
	declare @Resultado NUMERIC(15,2)
	declare	@Motivo CHAR(4)
        declare @US_COD_OK SMALLINT
        declare @HRS_APROB  NUMERIC(15,2)
  
	if ( @Tipo in (7,11,12) )
	begin
		select @Resultado = CH_HOR_EXT,
                       @Motivo = CH_RELOJ,
                       @US_COD_OK = US_COD_OK,
                       @HRS_APROB = CH_HOR_DES from CHECADAS
          	where 	( CB_CODIGO = @Empleado ) and
                	( AU_FECHA = @Fecha ) and
                	( CH_TIPO = @Tipo )
     	end
     	else
     	begin
          	select @Resultado = CH_HOR_ORD,
                       @Motivo = CH_RELOJ,
                       @US_COD_OK = US_COD_OK,
                       @HRS_APROB = CH_HOR_DES from CHECADAS
          	where 	( CB_CODIGO = @Empleado ) and
                	( AU_FECHA = @Fecha ) and
                	( CH_TIPO = @Tipo )
     	end
     	if ( @Resultado is Null )
     	begin
          	set @Resultado = 0
          	set @Motivo = ''
     	end

  	insert into @RegAuto
	values ( @Resultado, @Motivo, @US_COD_OK, @HRS_APROB )
	return
END
GO


CREATE  FUNCTION AUTORIZACIONES_SELECT_HORAS
(   @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS NUMERIC(15,2)
AS
BEGIN
	declare @Resultado numeric(15,2)
	select @Resultado = RESULTADO from dbo.AUTORIZACIONES_SELECT( @Empleado, @Fecha, @Tipo )

	RETURN @Resultado
END
GO

CREATE FUNCTION AUTORIZACIONES_SELECT_MOTIVO
(   @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS CHAR(4)
AS
BEGIN
	declare @Motivo CHAR(4)
	select @Motivo = Motivo from dbo.AUTORIZACIONES_SELECT( @Empleado, @Fecha, @Tipo )

	RETURN @Motivo
END
GO

CREATE FUNCTION AUTORIZACIONES_SELECT_APROBO
(   @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS SMALLINT
AS
BEGIN
	declare @Resultado SMALLINT
	select @Resultado = US_COD_OK from dbo.AUTORIZACIONES_SELECT( @Empleado, @Fecha, @Tipo )

	RETURN @Resultado
END
GO

CREATE FUNCTION AUTORIZACIONES_SELECT_HRSAPROBADAS
(   @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS NUMERIC(15,2)
AS
BEGIN
	declare @Resultado NUMERIC(15,2)
	select @Resultado = HRS_APROB from dbo.AUTORIZACIONES_SELECT( @Empleado, @Fecha, @Tipo )

	RETURN @Resultado
END
GO

create function DBO.SP_TASA_INFONAVIT( @EMPLEADO INTEGER, @FECHA FECHA )
returns NUMERIC(15, 2)
as
begin
     declare @iTipo SmallInt;
     declare @rInfOld Numeric( 15, 4 );
     declare @Resultado NUMERIC( 15, 2 );
     select @iTipo = C.CB_INFTIPO,
            @Resultado = C.CB_INFTASA,
            @rInfOld = C.CB_INF_OLD
     from DBO.COLABORA as C where ( C.CB_CODIGO = @EMPLEADO );
     if ( @Resultado is Null )
     begin
          set @Resultado = 0;
          set @iTipo = 0;
          set @rInfOld = 0;
     end;
     if ( ( @iTipo = 1 ) and ( @rInfOld > 0 ) )
     begin
          select @Resultado = KA.CB_MONTO from DBO.KARDEX as KA
          where ( KA.CB_CODIGO = @EMPLEADO ) and
                ( KA.CB_TIPO = 'TASINF' ) and
                ( KA.CB_FECHA = @FECHA );
          if ( @Resultado is Null )
          begin
               set @Resultado = 0;
          end
     end
     RETURN @Resultado;
end
GO

create function DBO.SP_A80_ESCALON( @CODIGO SMALLINT, @LIMITEINFERIOR NUMERIC( 15, 2 ),
@FECHA DATETIME )
returns @RESULTADO table ( CUOTA NUMERIC( 15, 2 ) NOT NULL, TASA NUMERIC( 15, 5 ) NOT NULL )
as
begin
     declare @V_CUOTA NUMERIC( 15, 2 );
     declare @V_TASA NUMERIC( 15, 5 );
     declare @V_FECHA DATETIME;

     select @V_FECHA = MAX( TI_INICIO )
     from T_ART_80
     where ( NU_CODIGO = @CODIGO ) and
           ( TI_INICIO <= @FECHA );

     if ( @V_FECHA is not null )
     begin
          select @V_CUOTA = A1.A80_CUOTA,
                 @V_TASA = A1.A80_TASA from DBO.ART_80 A1
          where ( A1.NU_CODIGO = @CODIGO ) and
                ( A1.TI_INICIO = @V_FECHA ) and
                ( A1.A80_LI = ( select MAX( A2.A80_LI ) from DBO.ART_80 A2
                                where ( A2.NU_CODIGO = @CODIGO ) and
                                      ( A2.TI_INICIO = @V_FECHA ) and
                                      ( A2.A80_LI <= @LIMITEINFERIOR ) ) );
     end

     if ( @V_FECHA is null ) or ( @V_Tasa is null )
     begin
          set @V_TASA = 0;
          set @V_CUOTA = 0;
     end

     insert into @RESULTADO values( @V_CUOTA, @V_TASA );
     RETURN
end
GO

CREATE FUNCTION SP_CHECADAS 
	( @FECHA DATETIME, @EMPLEADO INTEGER, @NUMERO SMALLINT )
RETURNS CHAR(4)
AS
BEGIN
	declare @Resultado CHAR(6)
	declare @Ciclo SMALLINT
  	SET @Ciclo = 1
  	declare Temporal cursor READ_ONLY STATIC for
     		select CH_H_REAL from CHECADAS 
		where ( CB_CODIGO = @EMPLEADO ) and ( AU_FECHA = @FECHA ) and
		( CH_TIPO < 5 ) and ( CH_SISTEMA <> 'S' )
		order by CH_H_REAL
	Open Temporal
	if ( @@CURSOR_ROWS >= @NUMERO )
	begin
		Fetch Next From Temporal Into @Resultado
		while ( @@Fetch_Status = 0 ) and ( @Ciclo <> @NUMERO )
		begin
			SET @Ciclo = @Ciclo + 1
			Fetch Next From Temporal Into @Resultado
		end
	end
	if ( @Resultado is Null ) or ( @Ciclo <> @NUMERO )
		SET @Resultado = ''

	close Temporal
	deallocate Temporal
	
	RETURN @Resultado
END
GO


CREATE FUNCTION LISTA_ENTEROS  
    (@INICIAL integer,  
    @FINAL integer) 
  RETURNS @TABLE_RESULT 
  TABLE  (RESULTADO integer) AS 
BEGIN 
 
 while ( @Inicial <= @Final )  
 BEGIN 
  Insert into @TABLE_RESULT VALUES(@Inicial) 
  set @Inicial = @Inicial + 1; 
 END 
 RETURN  
END 
GO

create function DBO.SP_CAFE_SUMA_COMIDAS(
@EMPLEADO INTEGER,
@FECHAINICIAL DATETIME,
@FECHAFINAL DATETIME,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT )
RETURNS Integer
as
begin
     declare @sTipo Char( 1 );
     declare @Resultado Integer;
     if ( @Empleado is Null )
        RETURN( 0 )
     else
     begin
          set @sTipo = CHAR( 48 + @TipoComida );
          if ( @TipoResultado = 1 )
              select @Resultado = SUM( C.CF_COMIDAS - C.CF_EXTRAS ) from DBO.CAF_COME as C
              where ( C.CB_CODIGO = @Empleado ) and
                    ( C.CF_FECHA >= @FechaInicial ) and
                    ( C.CF_FECHA <= @FechaFinal ) and
                    ( C.CF_TIPO = @sTipo )
          else
              if ( @TipoResultado = 2 )
                 select @Resultado = SUM( C.CF_COMIDAS ) from DBO.CAF_COME as C
                 where ( C.CB_CODIGO = @Empleado ) and
                       ( C.CF_FECHA >= @FechaInicial ) and
                       ( C.CF_FECHA <= @FechaFinal ) and
                       ( C.CF_TIPO = @sTipo )
              else
                  if ( @TipoResultado = 3 )
                     select @Resultado = SUM( C.CF_EXTRAS ) from DBO.CAF_COME as C
                     where ( C.CB_CODIGO = @Empleado ) and
                           ( C.CF_FECHA >= @FechaInicial ) and
                           ( C.CF_FECHA <= @FechaFinal ) and
                           ( C.CF_TIPO = @sTipo );
                  else
                      select @Resultado = COUNT(*) from DBO.CAF_COME as C
                       where ( C.CB_CODIGO = @Empleado ) and
                             ( C.CF_FECHA >= @FechaInicial ) and
                             ( C.CF_FECHA <= @FechaFinal ) and
                             ( C.CF_TIPO = @sTipo );
     end
     if ( @Resultado is Null )
        set @Resultado = 0;
     Return( @Resultado );
end
GO

create function DBO.SP_CAFE_DIARIO(
@EMPLEADO INTEGER,
@FECHA DATETIME,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT )
returns Integer
as
begin
     declare @Resultado Integer;
     if ( @Empleado is Null )
        set @Resultado = 0
     else
         set @Resultado = DBO.SP_CAFE_SUMA_COMIDAS( @Empleado, @Fecha, @Fecha, @TipoComida, @TipoResultado );
     Return( @Resultado );
end
GO

create function DBO.SP_CAFE_SEMANAL(
@EMPLEADO INTEGER,
@FECHA DATETIME,
@ANIO SMALLINT,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT,
@TIPOPERIODO SMALLINT )
RETURNS Integer
as
begin
     declare @FechaInicial DateTime;
     declare @FechaFinal DateTime;
     declare @Resultado Integer;
     if ( @Empleado is Null )
        set @Resultado = 0
     else
     begin
          select @FechaInicial = P.PE_ASI_INI, @FechaFinal = P.PE_ASI_FIN from DBO.PERIODO as P
          where ( P.PE_YEAR = @Anio ) and
                ( P.PE_TIPO = @TipoPeriodo ) and
                ( P.PE_ASI_INI <= @Fecha ) and
                ( P.PE_ASI_FIN >= @Fecha );
          if ( @FechaInicial is Null )
          begin
               set @FechaInicial = @Fecha;
               set @FechaFinal = @Fecha;
          end;
          select @Resultado = DBO.SP_CAFE_SUMA_COMIDAS( @Empleado, @FechaInicial, @FechaFinal, @TipoComida, @TipoResultado );
     end
     Return( @Resultado );
end
GO

create function DBO.SP_CAFE_AL_DIA(
@EMPLEADO INTEGER,
@FECHA DATETIME,
@ANIO SMALLINT,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT,
@TIPOPERIODO SMALLINT )
RETURNS Integer
as
begin
     declare @FechaInicial DateTime;
     declare @Resultado Integer;
     if ( @Empleado is Null )
        set @Resultado = 0
     else
     begin
          select @FechaInicial = P.PE_ASI_INI from DBO.PERIODO as P
          where ( P.PE_YEAR = @Anio ) and
                ( P.PE_TIPO = @TipoPeriodo ) and
                ( P.PE_ASI_INI <= @Fecha ) and
                ( P.PE_ASI_FIN >= @Fecha );
          if ( @FechaInicial is Null )
             set @FechaInicial = @Fecha;
          select @Resultado = DBO.SP_CAFE_SUMA_COMIDAS( @Empleado, @FechaInicial, @Fecha, @TipoComida, @TipoResultado );
     end
     Return( @Resultado );
end
GO

CREATE FUNCTION DBO.SP_CAFE_AUTORIZA_COMIDA(
@EMPLEADO INTEGER,
@DIA DATETIME,
@HORA CHAR(4),
@CREDENCIAL CHAR(1),
@TIPOCOMIDA CHAR( 1 ) )
RETURNS @RESULTADO TABLE ( AUTORIZACION SMALLINT NOT NULL, FECHABAJA DATETIME NULL )
AS
BEGIN
     DECLARE @cActivo Char( 1 )
     DECLARE @cCredencial Char( 1 )
     DECLARE @iRegistros Integer
     DECLARE @ValorAuto Smallint
     DECLARE @dBaja DATETIME
     DECLARE @iStatus SmallInt
     select @cActivo = C.CB_ACTIVO,
            @dBaja = C.CB_FEC_BAJ,
            @cCredencial = C.CB_CREDENC,
            @iStatus = ( select DBO.SP_STATUS_ACT ( @DIA, C.CB_CODIGO ) )
     from DBO.COLABORA as C where ( C.CB_CODIGO = @EMPLEADO )
     if @cActivo Is Null
        set @ValorAuto = 1
     else
         if ( @iStatus <> 1 )
            set @ValorAuto = 2
         else
             if ( ( @CREDENCIAL <> ' ' ) and ( @cCredencial <> @CREDENCIAL ) )
                set @ValorAuto = 3
             else
             begin
                  select @iRegistros = COUNT(*)
                  from DBO.CAF_COME C where
                  ( C.CB_CODIGO = @EMPLEADO ) and
                  ( C.CF_FECHA = @DIA ) and
                  ( C.CF_HORA = @HORA ) and
				  ( C.CF_TIPO = @TIPOCOMIDA)
                  if ( @iRegistros > 0 )
                     set @ValorAuto = 4
                  else
                      set @ValorAuto = 0
             end
     insert into @RESULTADO values( @ValorAuto, @dBaja )
     RETURN
end
GO

create function DBO.SP_CAFE_AUTORIZA_INVITACION(
@INVITADOR INTEGER,
@FECHA DATETIME,
@HORA CHAR( 4 ),
@TIPOCOMIDA CHAR( 1 ),
@NUMCOMIDAS SMALLINT )
RETURNS @Resultado TABLE( AUTORIZACION TinyInt, EMPLEADO INTEGER )
as
begin
     declare @cActivo Char( 1 );
     declare @dInicial DateTime;
     declare @dFinal DateTime;
     declare @iRegistros Integer;
     declare @iTope Integer;
     declare @iTope1 Integer;
     declare @iTope2 Integer;
     declare @iTope3 Integer;
     declare @iTope4 Integer;
     declare @iTope5 Integer;
     declare @iTope6 Integer;
     declare @iTope7 Integer;
     declare @iTope8 Integer;
     declare @iTope9 Integer;
     declare @iEmpleado Integer;
     declare @ValorAuto TinyInt;
     select @iEmpleado = IV.CB_CODIGO,
            @cActivo = IV.IV_ACTIVO,
            @dInicial = IV.IV_FEC_INI,
            @dFinal = IV.IV_FEC_FIN,
            @iTope1 = IV.IV_TOPE_1,
            @iTope2 = IV.IV_TOPE_2,
            @iTope3 = IV.IV_TOPE_3,
            @iTope4 = IV.IV_TOPE_4,
            @iTope5 = IV.IV_TOPE_5,
            @iTope6 = IV.IV_TOPE_6,
            @iTope7 = IV.IV_TOPE_7,
            @iTope8 = IV.IV_TOPE_8,
            @iTope9 = IV.IV_TOPE_9
            from DBO.INVITA as IV where ( IV.IV_CODIGO = @Invitador );
     if ( @cActivo is Null )
        set @ValorAuto = 1
     else
         if ( ( @cActivo <> 'S' ) or ( @Fecha < @dInicial ) or ( ( @dFinal > @dInicial ) and ( @Fecha > @dFinal ) ) )
            set @ValorAuto = 2
         else
         begin
              select @iTope = case when ( @TipoComida = '1' ) then @iTope1
                                   when ( @tipocomida = '2' ) then @itope2
                                   when ( @tipocomida = '3' ) then @itope3
                                   when ( @tipocomida = '4' ) then @itope4
                                   when ( @tipocomida = '5' ) then @itope5
                                   when ( @tipocomida = '6' ) then @itope6
                                   when ( @tipocomida = '7' ) then @itope7
                                   when ( @tipocomida = '8' ) then @itope8
                                   when ( @tipocomida = '9' ) then @itope9
                              else 0
                              end;
              if ( @NumComidas > @iTope )
                 set @ValorAuto = 3
              else
              begin
                   select @iRegistros = COUNT(*) from DBO.CAF_INV as C where
                   ( C.IV_CODIGO = @Invitador ) and
                   ( C.CF_FECHA = @Fecha ) and
                   ( C.CF_HORA = @Hora ) and
				   ( C.CF_TIPO = @TIPOCOMIDA ) ;
                   if ( @iRegistros > 0 )
                      set @ValorAuto = 4
                   else
                   begin
                        select @iRegistros = SUM( C.CF_COMIDAS ) from DBO.CAF_INV as C where
                        ( C.IV_CODIGO = @Invitador ) and
                        ( C.CF_FECHA = @Fecha ) and
                        ( C.CF_TIPO = @TipoComida );
                        if ( @iRegistros is Null )
                           set @iRegistros = 0;
                        if ( ( @NumComidas + @iRegistros ) > @iTope )
                           set @ValorAuto = 3
                        else
                            set @ValorAuto = 0;
                   end;
              end;
         end;
     insert into @RESULTADO values( @ValorAuto, @iEmpleado );
     RETURN;
end
GO

CREATE FUNCTION SP_FECHA_NIVEL (
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS CHAR(6)
AS
BEGIN
	DECLARE @Resultado CHAR(6)
	DECLARE Temporal CURSOR FOR
    
	select CB_NIVEL4 from KARDEX
    	where  ( CB_CODIGO = @Empleado ) and
		( CB_FECHA <= @Fecha )
	order by CB_FECHA DESC, CB_NIVEL DESC

	open Temporal
  
	fetch NEXT FROM Temporal
	INTO @Resultado

	close Temporal
	deallocate Temporal

	RETURN @Resultado
END
GO

CREATE FUNCTION SP_LISTA_KARDEX (
  @Fecha DATETIME,
  @Usuario SMALLINT,
  @NumNivel SMALLINT
)
RETURNS @RegKardex TABLE
(      CB_CODIGO            Integer,
       CB_NIVEL             CHAR(6) )
AS
BEGIN
  declare @CB_CODIGO Integer;
  declare @CB_NIVEL  CHAR(6);
  declare @Matches Integer;
  declare @Matches2 Integer;
  declare @Nivel1 CHAR(6);
  declare @Nivel2 CHAR(6);
  declare @Nivel3 CHAR(6);
  declare @Nivel4 CHAR(6);
  declare @Nivel5 CHAR(6);
  declare @Nivel6 CHAR(6);
  declare @Nivel7 CHAR(6);
  declare @Nivel8 CHAR(6);
  declare @Nivel9 CHAR(6);
  declare @FechaNivel DATETIME;
  declare Temporal CURSOR for
   select CB_CODIGO, CB_FEC_NIV, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9
   from COLABORA
   where ( ( select dbo.SP_STATUS_ACT( @Fecha, COLABORA.CB_CODIGO ) ) = 1 )

	open Temporal

	fetch NEXT FROM Temporal INTO
	@CB_CODIGO, @FechaNivel, @Nivel1, @Nivel2, @Nivel3, @Nivel4, @Nivel5, @Nivel6, @Nivel7, @Nivel8, @Nivel9

	while @@Fetch_Status = 0
	BEGIN
		if ( @Fecha < @FechaNivel )
			select @CB_NIVEL = dbo.SP_FECHA_NIVEL( @Fecha, @CB_CODIGO )
     		else if ( @NumNivel = 1 ) select @CB_NIVEL = @Nivel1
		else if ( @NumNivel = 2 ) select @CB_NIVEL = @Nivel2
     		else if ( @NumNivel = 3 ) select @CB_NIVEL = @Nivel3
     		else if ( @NumNivel = 4 ) select @CB_NIVEL = @Nivel4
     		else if ( @NumNivel = 5 ) select @CB_NIVEL = @Nivel5
     		else if ( @NumNivel = 6 ) select @CB_NIVEL = @Nivel6
     		else if ( @NumNivel = 7 ) select @CB_NIVEL = @Nivel7
     		else if ( @NumNivel = 8 ) select @CB_NIVEL = @Nivel8
     		else select @CB_NIVEL = @Nivel9

		if ( @CB_NIVEL <> '' )
		BEGIN
			select @Matches = ( select COUNT(*) from SUPER where ( US_CODIGO = @USUARIO ) and ( CB_NIVEL = @CB_NIVEL ))
       			if ( @Matches > 0 )
			BEGIN
           			select @Matches2 = ( select COUNT(*) from ASIGNA where ( AS_FECHA = @FECHA ) and ( CB_CODIGO = @CB_CODIGO ))
				if ( @Matches2 = 0 )
				BEGIN
					INSERT INTO @RegKardex
					VALUES ( @CB_CODIGO, @CB_NIVEL )
				END
			END
		END

		fetch NEXT FROM Temporal INTO
		@CB_CODIGO, @FechaNivel, @Nivel1, @Nivel2, @Nivel3, @Nivel4, @Nivel5, @Nivel6, @Nivel7, @Nivel8, @Nivel9
	END

	close Temporal
	deallocate Temporal
	
	RETURN
END
GO

CREATE FUNCTION SP_LISTA_DENTRO (
  @FECHA DATETIME,
  @USUARIO SMALLINT,
  @EMPLEADO INTEGER
) RETURNS INTEGER
AS       
BEGIN
	declare @Resultado Integer
	declare @Asignado   CHAR(6)

	select @Asignado = (  select CB_NIVEL 
				from ASIGNA 
  				where AS_FECHA = @Fecha and CB_CODIGO = @Empleado )
	if ( @Asignado IS NULL )
		select @Asignado = dbo.SP_FECHA_NIVEL( @Fecha, @Empleado )

	select @Resultado = ( select COUNT(*) 
				from SUPER
    				where US_CODIGO = @Usuario and 
					CB_NIVEL = @Asignado )
	RETURN @Resultado
END
GO

CREATE FUNCTION SP_LISTA_ASIGNA (
  @FECHA DATETIME,
  @USUARIO SMALLINT
) RETURNS @RegLista TABLE (
  CB_CODIGO INTEGER,
  CB_NIVEL CHAR(6),
  US_CODIGO SMALLINT )
AS           
BEGIN
	INSERT INTO @RegLista
	select ASIGNA.CB_CODIGO, ASIGNA.CB_NIVEL, ASIGNA.US_CODIGO
	from   ASIGNA 
	where  AS_FECHA = @Fecha and
         	dbo.SP_FECHA_NIVEL( @Fecha, ASIGNA.CB_CODIGO )
                 in (SELECT CB_NIVEL FROM SUPER WHERE US_CODIGO = @Usuario ) and
         	not ASIGNA.CB_NIVEL in ( SELECT CB_NIVEL FROM SUPER WHERE US_CODIGO = @Usuario )  

	RETURN
END
GO

CREATE FUNCTION SP_LISTA_EMPLEADOS (
  @FECHA DATETIME,
  @USUARIO SMALLINT
) RETURNS @RegLista TABLE (
  CB_CODIGO INTEGER,
  CB_NIVEL CHAR(6),
  CB_TIPO INTEGER )
AS
BEGIN
  declare @NumNivel SMALLINT;

  	/* Nivel de Organigrama de Supervisores */
	select @NumNivel = ( select CAST( GL_FORMULA AS SMALLINT )
  			from GLOBAL
  			where GL_CODIGO = 120 )

	insert into @RegLista
  	select CB_CODIGO, CB_NIVEL, 1
  	from   SP_LISTA_KARDEX( @FECHA, @USUARIO, @NumNivel )

  	insert into @RegLista
  	select CB_CODIGO, CB_NIVEL, 2
  	from   ASIGNA
  	where  AS_FECHA = @FECHA and
         ( ( select count(*) from SUPER where US_CODIGO = @USUARIO and CB_NIVEL = ASIGNA.CB_NIVEL ) > 0 ) and
         ( ( select DBO.SP_STATUS_ACT( @FECHA, ASIGNA.CB_CODIGO ) ) = 1 )

	RETURN
END
GO

CREATE FUNCTION SP_AUTO_X_APROBAR(
  @Fecha Fecha,
  @Usuario Usuario )
RETURNS @HorasXAprobar TABLE (
  CB_CODIGO Integer,
  CB_NIVEL CHAR(6),
  CH_TIPO SmallInt,
  CH_HORAS Numeric( 15, 2 ),
  CH_MOTIVO VARCHAR(4),
  CH_MOT_DES VARCHAR(30),
  CH_HOR_OK Numeric( 15, 2 ),
  US_CODIGO SmallInt,
  US_COD_OK SmallInt )
as
begin
     declare @Extras Horas;
     declare @CB_CODIGO NumeroEmpleado;
     declare @CB_NIVEL Codigo;

     declare Empleados cursor for
     select CB_CODIGO, CB_NIVEL
     from SP_LISTA_EMPLEADOS( @Fecha, @Usuario );

     Open Empleados
     Fetch Next From Empleados into @CB_CODIGO, @CB_NIVEL;
     while ( @@Fetch_Status = 0 )
     begin
           insert into @HorasXAprobar
           select @CB_CODIGO, @CB_NIVEL, CH.CH_TIPO,( CH.CH_HOR_EXT + CH.CH_HOR_ORD ),
                  CH.CH_RELOJ, M.TB_ELEMENT, CH.CH_HOR_DES, CH.US_CODIGO, CH.US_COD_OK
           from CHECADAS CH
           left outer join MOT_AUTO M on ( M.TB_CODIGO = CH.CH_RELOJ )
           where ( CH.CB_CODIGO = @CB_CODIGO ) and
                 ( CH.AU_FECHA = @Fecha ) and
                 ( CH.CH_TIPO in ( 5, 6, 7, 8, 9, 10, 11, 12) )
           order by CH.CH_TIPO;
           Fetch Next From Empleados into @CB_CODIGO, @CB_NIVEL;
     end
     close Empleados;
     deallocate Empleados;
     RETURN
end
GO

CREATE FUNCTION SP_AUTO_X_APROBAR_TODOS( @Fecha Fecha )
RETURNS @HorasXAprobar TABLE (
  CB_CODIGO Integer,
  CB_NIVEL CHAR(6),
  CH_TIPO SmallInt,
  CH_HORAS Numeric( 15, 2 ),
  CH_MOTIVO VARCHAR(4),
  CH_MOT_DES VARCHAR(30),
  CH_HOR_OK Numeric( 15, 2 ),
  US_CODIGO SmallInt,
  US_COD_OK SmallInt )
as
begin
     declare @Extras Horas;
     declare @CB_CODIGO NumeroEmpleado;
     declare @CB_NIVEL Codigo;
     declare @NumNivel smallint;
     declare @Nivel1 CHAR(6);
     declare @Nivel2 CHAR(6);
     declare @Nivel3 CHAR(6);
     declare @Nivel4 CHAR(6);
     declare @Nivel5 CHAR(6);
     declare @Nivel6 CHAR(6);
     declare @Nivel7 CHAR(6);
     declare @Nivel8 CHAR(6);
     declare @Nivel9 CHAR(6);
     declare @FechaNivel Fecha;
     declare @Anterior NumeroEmpleado;
     declare @CH_TIPO SmallInt;
     declare @CH_HORAS Numeric( 15, 2 );
     declare @CH_MOTIVO VARCHAR(4);
     declare @CH_MOT_DES VARCHAR(30);
     declare @CH_HOR_OK Numeric( 15, 2 );
     declare @US_CODIGO SmallInt;
     declare @US_COD_OK SmallInt;

     select @Anterior = -1;
     /* Nivel de Organigrama de Supervisores */
     select @NumNivel = ( select CAST( GL_FORMULA AS SMALLINT )
                          from GLOBAL
                          where GL_CODIGO = 120 )

     /* Checadas */
     declare Checadas cursor for
     select CH.CB_CODIGO, CH.CH_TIPO, ( CH.CH_HOR_EXT + CH.CH_HOR_ORD ), CH.CH_RELOJ, M.TB_ELEMENT, CH.CH_HOR_DES, CH.US_CODIGO, CH.US_COD_OK
     from CHECADAS CH
     left outer join MOT_AUTO M on ( M.TB_CODIGO = CH.CH_RELOJ )
     where ( CH.AU_FECHA = @Fecha ) and
     ( CH.CH_TIPO in ( 5, 6, 7, 8, 9, 10, 11, 12 ) )
     order by CH.CB_CODIGO, CH.CH_TIPO;

     Open Checadas
     Fetch Next From Checadas into
     @CB_CODIGO, @CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK;

     while ( @@Fetch_Status = 0 )
     begin
          if ( @CB_CODIGO <> @Anterior )
          begin
               select
               @FechaNivel = C.CB_FEC_NIV,
               @Nivel1 = C.CB_NIVEL1,
               @Nivel2 = C.CB_NIVEL2,
               @Nivel3 = C.CB_NIVEL3,
               @Nivel4 = C.CB_NIVEL4,
               @Nivel5 = C.CB_NIVEL5,
               @Nivel6 = C.CB_NIVEL6,
               @Nivel7 = C.CB_NIVEL7,
               @Nivel8 = C.CB_NIVEL8,
               @Nivel9 = C.CB_NIVEL9
               from COLABORA C
               where ( C.CB_CODIGO = @CB_CODIGO )

               if ( @Fecha < @FechaNivel )
                       select @CB_NIVEL = dbo.SP_FECHA_NIVEL( @Fecha, @CB_CODIGO )
               else if ( @NumNivel = 1 ) select @CB_NIVEL = @Nivel1;
               else if ( @NumNivel = 2 ) select @CB_NIVEL = @Nivel2;
               else if ( @NumNivel = 3 ) select @CB_NIVEL = @Nivel3;
               else if ( @NumNivel = 4 ) select @CB_NIVEL = @Nivel4;
               else if ( @NumNivel = 5 ) select @CB_NIVEL = @Nivel5;
               else if ( @NumNivel = 6 ) select @CB_NIVEL = @Nivel6;
               else if ( @NumNivel = 7 ) select @CB_NIVEL = @Nivel7;
               else if ( @NumNivel = 8 ) select @CB_NIVEL = @Nivel8;
               else select @CB_NIVEL = @Nivel9

               select @Anterior = @CB_CODIGO;
          end
            /* Lista de Checadas */
          insert into @HorasXAprobar ( CB_CODIGO, CB_NIVEL, CH_TIPO, CH_HORAS, CH_MOTIVO, CH_MOT_DES, CH_HOR_OK, US_CODIGO, US_COD_OK )
          values ( @CB_CODIGO, @CB_NIVEL,@CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK )

          Fetch Next From Checadas into
          @CB_CODIGO, @CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK;
     end
     close Checadas;
     deallocate Checadas;

     RETURN
end
GO

CREATE FUNCTION HRS_INV(
  @TIPO SMALLINT,
  @HORAS NUMERIC(15,2)
) RETURNS NUMERIC(15,2)
AS
BEGIN
	if ( @TIPO = 2 )
		RETURN( 0 )
	RETURN( @HORAS )
END
GO

CREATE FUNCTION TURNO_EMPLEADO(
  @Fecha Datetime,
  @Empleado Integer )
RETURNS @TablaTurno TABLE
(      TU_RIT_PAT           Varchar(255),
       TU_HOR_1             Char(6),
       TU_HOR_2             Char(6),
       TU_HOR_3             Char(6),
       TU_HOR_4             Char(6),
       TU_HOR_5             Char(6),
       TU_HOR_6             Char(6),
       TU_HOR_7             Char(6),
       TU_TIP_1             Smallint,
       TU_TIP_2             Smallint,
       TU_TIP_3             Smallint,
       TU_TIP_4             Smallint,
       TU_TIP_5             Smallint,
       TU_TIP_6             Smallint,
       TU_TIP_7             Smallint,
       TU_CODIGO            Char(6),
       TU_RIT_INI           Datetime,
       TU_HORARIO           Smallint,
       TU_HOR_FES	    Char(6),
       TU_VACA_HA           Numeric(15,2),
       TU_VACA_SA           Numeric(15,2),
       TU_VACA_DE           Numeric(15,2),
       CB_CREDENC           Char(1) )
AS
BEGIN
     INSERT @TablaTurno
     SELECT TU_RIT_PAT,
            TU_HOR_1,
            TU_HOR_2,
            TU_HOR_3,
            TU_HOR_4,
            TU_HOR_5,
            TU_HOR_6,
            TU_HOR_7,
            TU_TIP_1,
            TU_TIP_2,
            TU_TIP_3,
            TU_TIP_4,
            TU_TIP_5,
            TU_TIP_6,
            TU_TIP_7,
            TU_CODIGO,
            TU_RIT_INI,
            TU_HORARIO,
            TU_HOR_FES,
            TU_VACA_HA,
            TU_VACA_SA,
            TU_VACA_DE, 
            ( select C.CB_CREDENC from COLABORA C where ( C.CB_CODIGO = @Empleado ) ) CB_CREDENC
     FROM TURNO WHERE ( TU_CODIGO = dbo.SP_KARDEX_CB_TURNO( @Fecha, @Empleado ) );
     RETURN
END
GO

CREATE FUNCTION SP_BOOL( @Valor INTEGER )
RETURNS CHAR(1)
AS
BEGIN
	if @Valor <> 0
		Return 'S'
	Return 'N'
END
GO

CREATE FUNCTION SP_STATUS_ACC (
  @EXPEDIENTE INTEGER,
  @FECHAINICIAL DATETIME,
  @FECHAFINAL DATETIME
) RETURNS CHAR(1) AS
begin

     declare @Cuantos INTEGER;
     declare @Resultado Char(1);

     select @Cuantos = COUNT(*) from ACCIDENT where
     ( EX_CODIGO = @EXPEDIENTE ) and
     ( AX_FECHA = @FechaInicial ) and
     ( AX_FECHA <= @FechaFinal )

     if ( @Cuantos > 0 )
        Set @Resultado = 'S';
     else
         Set @Resultado = 'N';

     Return @Resultado
end
GO

CREATE FUNCTION SP_STATUS_EMB (
  @EXPEDIENTE INTEGER,
  @FECHAINICIAL DATETIME,
  @FECHAFINAL DATETIME
) RETURNS CHAR(1) AS
begin
     declare @Cuantos INTEGER;
     declare @Resultado Char(1);

     select @Cuantos = COUNT(*) from EMBARAZO where
     ( EX_CODIGO = @EXPEDIENTE ) and
     ( ( @FechaInicial between EM_FEC_UM and EM_FEC_PP ) or
     ( @FechaFinal between EM_FEC_UM and EM_FEC_PP ) )

     if ( @Cuantos > 0 )
        Set @Resultado = 'S';
     else
         Set @Resultado = 'N';

     Return @Resultado;

end
GO

create function DBO.GET_SUB_PUESTOS( @PUESTO CHAR(6) ) returns @SUBPUESTOS TABLE( PU_SUB CHAR(6) )
as
begin
     declare @SUB CHAR(6)
     declare SUBALTERNOS cursor for
     select PU_CODIGO from GRUPO where ( PU_REPORTA = @PUESTO )
     order by PU_CODIGO;
     open SUBALTERNOS;
     fetch NEXT from SUBALTERNOS into @SUB;
     while ( @@Fetch_Status = 0 )
     begin
          insert into @SUBPUESTOS( PU_SUB ) values ( @SUB );
          insert into @SUBPUESTOS select PU_SUB from DBO.GET_SUB_PUESTOS( @SUB );
          fetch NEXT from SUBALTERNOS into @SUB;
     end;
     close SUBALTERNOS;
     deallocate SUBALTERNOS;
     return;
end
GO

create function DBO.SP_JEFE( @EMPLEADO Integer ) returns Integer
as
begin
     declare @REPORTA Char(6);
     declare @JEFE Integer;
     select @REPORTA = PU_REPORTA from COLABORA
     left join PUESTO on ( PUESTO.PU_CODIGO = COLABORA.CB_PUESTO )
     where ( CB_CODIGO = @Empleado );
     if ( @REPORTA is NULL )
     begin
          select @JEFE = 0;
     end
     else
     begin
          select @JEFE = MIN( CB_CODIGO ) from COLABORA
          where ( CB_ACTIVO = 'S' ) and ( CB_PUESTO = @REPORTA );
          if ( @JEFE is NULL )
          begin
               select @JEFE = 0;
          end
     end
     return @JEFE;
end
GO

CREATE FUNCTION DBO.SP_POSICIONES( @PUESTO CHAR(6), @FECHA DATETIME )
RETURNS INTEGER
AS
begin
     DECLARE @Resultado AS INTEGER;
     select @Resultado = COUNT( CB_CODIGO ) from COLABORA
     where ( CB_ACTIVO = 'S' ) and ( CB_PUESTO = @PUESTO );
     if ( @Resultado is NULL )
        select @Resultado = 0
     RETURN @Resultado
end
GO

CREATE FUNCTION DBO.SP_CUMPLE_PUESTO ( @EMPLEADO NUMEROEMPLEADO, @PUESTO CODIGO )
RETURNS INTEGER
AS
begin
     declare @ORDEN1 INTEGER;
     declare @ORDEN2 INTEGER;
     declare @CUANTOS INTEGER;
     declare @CONTADOR INTEGER;
     declare @CUMPLE INTEGER;

     select @CONTADOR = 0;
     select @CUANTOS = 0;

     declare COMPETENCIAS cursor for
     select C1.CA_ORDEN, C2.CA_ORDEN from COMP_PTO
     left outer join COMPETEN on ( COMPETEN.CM_CODIGO = COMP_PTO.CM_CODIGO  )
     left outer join EMP_COMP on ( EMP_COMP.CM_CODIGO = COMP_PTO.CM_CODIGO ) and ( EMP_COMP.CB_CODIGO = @EMPLEADO )
     left outer join CALIFICA C2 on ( C2.CA_CODIGO = COMP_PTO.CA_CODIGO )
     left outer join CALIFICA C1 on ( C1.CA_CODIGO = EMP_COMP.CA_CODIGO )
     where ( COMP_PTO.PU_CODIGO = @PUESTO );
     open COMPETENCIAS;
     fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @ORDEN2 is not NULL )
          begin
	       if ( @ORDEN1 >= @ORDEN2 )
	       begin
	            select @CONTADOR = @CONTADOR + 1;
               end
               select @CUANTOS = @CUANTOS + 1;
          end
          fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     end;
     close COMPETENCIAS;
     deallocate COMPETENCIAS;
     if ( @CUANTOS > 0 )
     begin
          select @CUMPLE = ROUND( ( ( @CONTADOR / @CUANTOS ) * 100 ), 0, 1 );
     end
     else
     begin
          select @CUMPLE = 0;
     end
     return @CUMPLE;
end
GO

CREATE FUNCTION DBO.EMPLEADO_ADENTRO( @Empleado NumeroEmpleado, @Fecha Fecha ) RETURNS SmallInt
AS
BEGIN
     declare @iChecadas as INTEGER;
     select @iChecadas = COUNT(*) from ACCESLOG where
     ( ACCESLOG.CB_CODIGO = @Empleado ) and
     ( ACCESLOG.AL_FECHA = @Fecha ) and
     ( ( ( US_CODIGO = 0 ) and ( AL_OK_SIST = 'S' ) ) or ( ( US_CODIGO <> 0 ) and ( AL_OK_MAN = 'S' ) ) );
     RETURN ( @iChecadas % 2 );
END
GO

CREATE FUNCTION DBO.EMPLEADO_ULTACCESO( @Empleado NumeroEmpleado, @Fecha Fecha ) RETURNS HORA
AS
BEGIN
     declare @Checada as HORA;
     select TOP 1 @Checada = AL_HORA from ACCESLOG
     where ( ACCESLOG.CB_CODIGO = @Empleado ) and
           ( ACCESLOG.AL_FECHA = @Fecha ) and
           ( ( ( US_CODIGO = 0 ) and ( AL_OK_SIST = 'S' ) ) or ( ( US_CODIGO <> 0 ) and ( AL_OK_MAN = 'S' ) ) )
     order by AL_HORA desc;
     RETURN @Checada;
END
GO

CREATE FUNCTION DBO.EMPLEADO_ULTCASETA( @Empleado NumeroEmpleado, @Fecha Fecha ) RETURNS Codigo
AS
BEGIN
     declare @Caseta as Codigo;
     select TOP 1 @Caseta = AL_CASETA from ACCESLOG
     where ( ACCESLOG.CB_CODIGO = @Empleado ) and
           ( ACCESLOG.AL_FECHA = @Fecha ) and
           ( ( ( US_CODIGO = 0 ) and ( AL_OK_SIST = 'S' ) ) or ( ( US_CODIGO <> 0 ) and ( AL_OK_MAN = 'S' ) ) )
     order by AL_HORA desc;
     RETURN @Caseta;
END
GO

CREATE FUNCTION SP_INVESTIGA_STATUS ( @Empleado INTEGER, @Fecha DATETIME ) RETURNS SMALLINT AS
BEGIN
	declare @Status SMALLINT
	declare @FechaTipNom DATETIME
	declare @TipoNomina SmallInt
	declare @LimiteNomina SmallInt

	/*Obtener Limite de Nomina del Global #213 "Numero Limite de Nominas Ordinarias*/
	select @LimiteNomina = cast(GL_FORMULA as SmallInt) from GLOBAL where GL_CODIGO = 213

	select @TipoNomina = CB_NOMINA, @FechaTipNom = CB_FEC_NOM from COLABORA where CB_CODIGO = @Empleado

	if (@FechaTipNom > @Fecha) 
	select @TipoNomina = dbo.SP_KARDEX_CB_NOMINA( @Fecha, @Empleado )

	select @Status = MAX(PE_STATUS) from PERIODO
	where PE_NUMERO < @LimiteNomina and PE_ASI_INI <= @Fecha and PE_ASI_FIN >= @Fecha and PE_TIPO = @TipoNomina

	if @Status is NULL Select @Status = 0;

	Return @Status
end
GO

CREATE FUNCTION SP_STATUS_TARJETA (
  @Empleado INTEGER,
  @Fecha DATETIME
)
RETURNS SMALLINT
AS
BEGIN
     declare @Status SMALLINT

     select @Status = PE_STATUS
     from AUSENCIA
     left outer join PERIODO on  PERIODO.PE_YEAR = AUSENCIA.PE_YEAR
                             and PERIODO.PE_TIPO = AUSENCIA.PE_TIPO
                             and PERIODO.PE_NUMERO = AUSENCIA.PE_NUMERO
     where AUSENCIA.CB_CODIGO = @Empleado
     and   AUSENCIA.AU_FECHA = @Fecha

     if (@Status is NULL)
     begin
          Select @Status = dbo.SP_INVESTIGA_STATUS( @Empleado, @Fecha )
     end;

     Return @Status
END
GO

CREATE FUNCTION SP_CONFLICTO_RESERVA( @Original FolioGrande )
RETURNS FolioGrande
AS
BEGIN
  DECLARE @FolioConflicto FolioGrande
  DECLARE @Aula     Codigo
  DECLARE @FechaIni Fecha
  DECLARE @FechaFin Fecha
  DECLARE @HoraIni  Hora
  DECLARE @HoraFin  Hora

  set @FolioConflicto = 0;

  select @Aula     = AL_CODIGO,
         @FechaIni = RV_FEC_INI,
         @FechaFin = RV_FEC_FIN,
         @HoraIni  = RV_HOR_INI,
         @HoraFin  = RV_HOR_FIN
  from   RESERVA
  where  RV_FOLIO = @Original

  /* Valida que exista el folio original */
  if ( @FechaIni is not NULL )
  BEGIN
    select @FolioConflicto = MIN( RV_FOLIO )
    from   RESERVA
    where  RV_FOLIO <> @Original and
           AL_CODIGO = @Aula and
           ( RV_FEC_INI < @FechaFin OR ( RV_FEC_INI = @FechaFin and RV_HOR_INI < @HoraFin )) AND
           ( RV_FEC_FIN > @FechaIni OR ( RV_FEC_FIN = @FechaIni and RV_HOR_FIN > @HoraIni ))
  END

  RETURN COALESCE( @FolioConflicto, 0 )
END
GO

CREATE  FUNCTION SP_LISTA_CONFLICTO_RESERVA( @Original FolioGrande )
RETURNS @RegLista TABLE
( RV_FOLIO INTEGER )
AS
BEGIN
  DECLARE @FolioConflicto FolioGrande
  DECLARE @Aula     Codigo
  DECLARE @FechaIni Fecha
  DECLARE @FechaFin Fecha
  DECLARE @HoraIni  Hora
  DECLARE @HoraFin  Hora

  select @Aula     = AL_CODIGO,
         @FechaIni = RV_FEC_INI,
         @FechaFin = RV_FEC_FIN,
         @HoraIni  = RV_HOR_INI,
         @HoraFin  = RV_HOR_FIN
  from   RESERVA
  where  RV_FOLIO = @Original

  /* Valida que exista el folio original */
  if ( @FechaIni is not NULL )
  BEGIN
    insert into @RegLista
    select RV_FOLIO
    from   RESERVA
    where  RV_FOLIO <> @Original and
           AL_CODIGO = @Aula and
           ( RV_FEC_INI < @FechaFin OR ( RV_FEC_INI = @FechaFin and RV_HOR_INI < @HoraFin )) AND
           ( RV_FEC_FIN > @FechaIni OR ( RV_FEC_FIN = @FechaIni and RV_HOR_FIN > @HoraIni ))
    order by RV_FOLIO
  END

  RETURN
END
GO

CREATE FUNCTION SP_RESERVA_FOLIO( @SESION FolioGrande, @POSICION SMALLINT )
RETURNS FolioGrande
AS
BEGIN
     declare @Resultado FolioGrande;
     declare @Ciclo SMALLINT;
     set @Ciclo = 1;
     declare Temporal cursor READ_ONLY STATIC for
       select RV_FOLIO from RESERVA  where ( SE_FOLIO = @SESION )
       order by RV_FEC_INI, RV_HOR_INI;
     open Temporal;
     if ( @@CURSOR_ROWS >= @POSICION )
     begin
          fetch Next from Temporal into @Resultado;
          while ( @@Fetch_Status = 0 ) and ( @Ciclo <> @POSICION )
          begin
               set @Ciclo = @Ciclo + 1;
               fetch Next from Temporal into @Resultado;
          end
     end
     if ( @Resultado is NULL ) or ( @Ciclo <> @POSICION )
     begin
          set @Resultado = 0;
     end
     close Temporal;
     deallocate Temporal;
     RETURN @Resultado;
END
GO

CREATE FUNCTION SP_CUR_MAX( @EMPLEADO INTEGER, @CURSO CHAR(6), @FECHA DATETIME )
RETURNS DateTime
AS
BEGIN
	declare @Resultado DateTime
	select @Resultado = MAX( KC_FEC_TOM ) from KARCURSO where
    ( CB_CODIGO = @EMPLEADO ) and
    ( CU_CODIGO = @CURSO ) and
    ( KC_FEC_TOM <= @FECHA );
	return @Resultado;
END
GO

CREATE FUNCTION DBO.SALDO_CTA( @CT_CODIGO Codigo, @Fecha Fecha )
RETURNS Pesos
AS
BEGIN
	 return COALESCE( ( select SUM( case CM_DEP_RET when 'D' then CM_MONTO else -CM_MONTO end )
                        from CTA_MOVS where
                        ( CT_CODIGO = @CT_CODIGO ) and
                        ( CM_FECHA <= @Fecha ) ), 0 );
END
GO

CREATE FUNCTION DBO.SUMA_CTAMOVS( @CT_CODIGO Codigo, @FechaIni Fecha, @FechaFin Fecha, @Tipo Codigo1 )
RETURNS Pesos
AS
BEGIN
	 return COALESCE( ( select SUM( CM_MONTO ) from CTA_MOVS where
     ( CT_CODIGO = @CT_CODIGO ) and
     ( CM_FECHA between @FechaIni and @FechaFin ) and
     ( CM_DEP_RET = @Tipo ) ), 0 )
END
GO

CREATE FUNCTION DBO.SIGUIENTE_CHEQUE( @CT_CODIGO Codigo )
RETURNS FolioGrande
AS
BEGIN
	 return COALESCE( ( select MAX( CM_CHEQUE ) from CTA_MOVS
     where ( CT_CODIGO = @CT_CODIGO ) ), 0 ) + 1;
END
GO

CREATE FUNCTION SP_SUMA_SALARIO( @Empleado INTEGER, @FechaIni DATETIME, @Dias INTEGER )
RETURNS Pesos
AS
BEGIN
     declare @AU_FECHA Fecha;
     declare @FechaFin Fecha;
     declare @FechaRevision Fecha;
     declare @SalarioTotal Pesos;
     declare @SalarioFecha Pesos;
     declare @SalarioActual Pesos;

     set @AU_FECHA = @FechaIni;
     set @FechaFin = DATEADD( day, @Dias-1, @FechaIni );
     set @SalarioTotal = 0;
     select @FechaRevision = CB_FEC_REV, @SalarioActual = CB_SALARIO
     from COLABORA where ( CB_CODIGO = @Empleado );

     while ( @AU_FECHA <= @FechaFin )
     begin
          if ( @AU_FECHA >= @FechaRevision )          /* Puede Usar el Salario Actual */
             set @SalarioFecha = @SalarioActual;
          else
              select @SalarioFecha = DBO.SP_KARDEX_CB_SALARIO( @AU_FECHA, @Empleado );
          set @SalarioTotal = @SalarioTotal + @SalarioFecha;
          set @AU_FECHA = DATEADD( day, 1, @AU_FECHA );
     end
     return ( @SalarioTotal );
END
GO

create function TF( @Tabla SMALLINT, @Codigo SMALLINT ) returns VARCHAR( 50 )
as
begin
     declare @Descripcion Observaciones;
	 declare @DescripcionTipoPeriodo Observaciones;
	 if @Tabla = 14
	 begin	 		
		select @Descripcion = TP_DESCRIP from TPERIODO where
		( TP_TIPO = @Codigo );
		select TOP 1 @DescripcionTipoPeriodo = Items from dbo.FN_SPLIT( @Descripcion, '/' )	 
		return ISNULL( @DescripcionTipoPeriodo, '' );
	 end
     select @Descripcion = TF_DESCRIP from TFIJAS where
     ( TF_TABLA = @Tabla ) and
     ( TF_CODIGO = @Codigo );
     return ISNULL( @Descripcion, '???' );
end
GO

create function TF_GET_NUM( @Tabla VARCHAR(20), @Campo VARCHAR(20) ) returns Integer
as
begin
     declare @Numero Integer;
     select @Numero = CAMPOS.DI_NUMERO
     from DICCION CAMPOS
     left outer join DICCION TABLA on ( Tabla.DI_CLASIFI = -1 ) and ( TABLA.DI_CALC = CAMPOS.DI_CLASIFI )
     where
     ( TABLA.DI_NOMBRE = @Tabla ) and
     ( CAMPOS.DI_NOMBRE = @Campo ) and
     ( CAMPOS.DI_TRANGO = 2 );
     return ISNULL( @Numero, -1 );
end
GO

create function TF_GET_FK( @Tabla VARCHAR(20), @Campo VARCHAR(20) ) returns VARCHAR(20)
as
begin
     declare @Resultado VARCHAR(20);
     select @Resultado = TABLA_FK.DI_NOMBRE
     from DICCION TABLA_FK where
     ( TABLA_FK.DI_CLASIFI = -1 ) and
     ( TABLA_FK.DI_CALC = (
     select CAMPOS.DI_NUMERO from DICCION CAMPOS
     left outer join DICCION TABLA on ( TABLA.DI_CLASIFI = -1 ) and ( TABLA.DI_CALC = CAMPOS.DI_CLASIFI )
     where
     ( TABLA.DI_NOMBRE = @Tabla ) and
     ( CAMPOS.DI_NOMBRE = @Campo ) and
     ( CAMPOS.DI_TRANGO = 1 ) ) );
     return ISNULL( @Resultado, '' );
end
GO

CREATE FUNCTION VAL_FOLIO_ULTIMA( @PU_CODIGO Codigo )
RETURNS FolioGrande
AS
BEGIN
	RETURN COALESCE( (select TOP 1 VP_FOLIO from VAL_PTO where PU_CODIGO = @PU_CODIGO order by VP_FECHA desc), 0 )
END
GO

CREATE FUNCTION VAL_GET_CUAL(
	@VP_FOLIO	FolioGrande,
	@VT_ORDEN	FolioChico )
RETURNS	FolioChico
AS
BEGIN
	RETURN COALESCE( ( select VT_CUAL from VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN ), 0 )
END
GO

CREATE FUNCTION VAL_GET_PUNTOS(
	@VP_FOLIO	FolioGrande,
	@VT_ORDEN	FolioChico )
RETURNS	FolioChico
AS
BEGIN
	RETURN COALESCE( ( select VT_PUNTOS from V_VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN ), 0 )
END
GO

CREATE FUNCTION VAL_GET_NIVEL(
	@VP_FOLIO	FolioGrande,
	@VT_ORDEN	FolioChico,
	@Regresa	Status )
RETURNS Formula
AS
BEGIN
	declare @Texto	Formula
	if ( @Regresa = 1 )
		select @Texto = VN_CODIGO from V_VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN
	else if ( @Regresa = 2 )
		select @Texto = VN_NOMBRE from V_VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN
	else if ( @Regresa = 3 )
		select @Texto = CAST( VN_DESCRIP as Varchar(255)) from V_VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN
	else if ( @Regresa = 4 )
		select @Texto = VN_INGLES from V_VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN
	else if ( @Regresa = 5 )
		select @Texto = CAST( VN_DES_ING as Varchar(255)) from V_VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN
	else if ( @Regresa = 6 )
		select @Texto = VN_TEXTO from V_VPUNTOS where VP_FOLIO = @VP_FOLIO and VT_ORDEN = @VT_ORDEN
	else
		set @Texto = '???'

	RETURN @Texto
END
GO

CREATE FUNCTION DBO.SP_EXISTE_TABLA(@Tabla VarChar(30))
RETURNS SmallInt AS
begin
 	declare @Resultado as SmallInt;

	select @Resultado = Count(*) from INFORMATION_SCHEMA.TABLES
	where TABLE_NAME = @Tabla

	Return @Resultado;
end
GO

CREATE FUNCTION DBO.SP_RA(
  				@Concepto SMALLINT,
  				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
  				@Referencia char(8),
				@RazonSocial char(6),
  				@TipoNomina SMALLINT,  				
  				@ValidaReingreso CHAR(1) )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @Resultado AS NUMERIC(15,2)

	select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
	from   MOVIMIEN
	left outer join NOMINA on NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
	left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
	left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
    left outer join COLABORA on COLABORA.CB_CODIGO = MOVIMIEN.CB_CODIGO 
	where ( MOVIMIEN.PE_YEAR = @Anio ) AND
		 ( MOVIMIEN.CB_CODIGO = @Empleado ) AND 		  
		    ( MOVIMIEN.CO_NUMERO = @Concepto ) AND
		    ( MOVIMIEN.MO_ACTIVO = 'S' ) AND
		    ( PERIODO.PE_STATUS = 6 ) AND
		    ( PERIODO.PE_MES = @Mes ) AND
		    ( ( @TipoNomina = -1 ) OR ( MOVIMIEN.PE_TIPO = @TipoNomina ) ) AND		    
		    ( ( @Referencia = '' ) OR ( MOVIMIEN.MO_REFEREN = @Referencia ) ) AND		    
		    ( ( @RazonSocial = '' ) OR ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) AND            
              ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) )

 	if ( @Resultado is NULL )
        select @Resultado = 0
	RETURN @Resultado
END
GO

CREATE FUNCTION DBO.SP_VALOR_CONCEPTO(
 				@Concepto SMALLINT,
				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
				@Razonsocial CHAR(6),
				@TipoNomina SMALLINT,
				@ValidaReingreso CHAR(1)
				 )

RETURNS NUMERIC(15,2)
AS
BEGIN
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
	 DECLARE @M1216 NUMERIC(15,2);
	 DECLARE @TipoNom SMALLINT;
     DECLARE @Resultado NUMERIC(15,2);
     DECLARE @TP_Clasif SMALLINT;

select @Resultado = 0;

if ( @Concepto <> 1004 )
begin
	DECLARE TemporalCursorNom Cursor for

     select N.NO_PERCEPC,
            N.NO_DEDUCCI,
            N.NO_NETO,
            N.NO_X_ISPT,
            N.NO_PER_MEN,
            N.NO_X_MENS,
            N.NO_PER_CAL,
            N.NO_IMP_CAL,
            N.NO_X_CAL,
            N.NO_TOT_PRE,
			      N.NO_PREV_GR,
			      N.NO_PER_ISN,
            N.NO_TOTAL_1,
            N.NO_TOTAL_2,
            N.NO_TOTAL_3,
            N.NO_TOTAL_4,
            N.NO_TOTAL_5,
            N.NO_DIAS_FI,
            N.NO_DIAS_FJ,
            N.NO_DIAS_CG,
            N.NO_DIAS_SG,
            N.NO_DIAS_SU,
            N.NO_DIAS_OT,
            N.NO_DIAS_IN,
            N.NO_DIAS * 7 / 6,
            N.NO_DIAS_RE,
            N.NO_D_TURNO,
            N.NO_DIAS,
            N.NO_DIAS_AS,
            N.NO_DIAS_NT,
            N.NO_DIAS_FV,
            N.NO_DIAS_VA,
            N.NO_DIAS_AG,
            N.NO_DIAS_AJ,
            N.NO_DIAS_SS,
            N.NO_DIAS_EM,
            N.NO_DIAS_SI,
            N.NO_DIAS_BA,
            N.NO_DIAS_PV,
            N.NO_DIAS_VJ,
            N.NO_DIAS_VJ * 7 / 6,
            N.NO_DIAS_IH,
            N.NO_DIAS_ID,
            N.NO_DIAS_IT,
            N.NO_DIAS_DT,
            N.NO_DIAS_FS,
            N.NO_DIAS_FT,
            N.NO_JORNADA,
            N.NO_HORAS,
            N.NO_EXTRAS,
            N.NO_DOBLES,
            N.NO_TRIPLES,
            N.NO_ADICION,
            N.NO_TARDES,
            N.NO_HORA_PD,
            N.NO_HORA_CG,
            N.NO_HORA_SG,
            N.NO_FES_TRA,
            N.NO_DES_TRA,
            N.NO_VAC_TRA,
            N.NO_FES_PAG,
            N.NO_HORASNT,
            N.NO_HORAPDT,
		        N.PE_TIPO,
			      N.NO_PRE_EXT

		  from NOMINA N
		  LEFT OUTER JOIN PERIODO ON PERIODO.PE_YEAR = N.PE_YEAR AND PERIODO.PE_TIPO = N.PE_TIPO AND PERIODO.PE_NUMERO = N.PE_NUMERO
		  LEFT OUTER JOIN RPATRON ON RPATRON.TB_CODIGO = N.CB_PATRON
		  LEFT OUTER JOIN RSOCIAL ON RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
		  LEFT OUTER JOIN COLABORA ON COLABORA.CB_CODIGO = N.CB_CODIGO
		where
			( N.PE_YEAR = @Anio ) and
			( N.CB_CODIGO = @Empleado ) and
			( PERIODO.PE_STATUS = 6 ) and
			( PERIODO.PE_MES = @Mes ) and
			( ( @TipoNomina = -1 ) or ( N.PE_TIPO = @TipoNomina ) ) and
			( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
      		( ( @ValidaReingreso <> 'S' ) or ( COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN ) )

		open TemporalCursorNom
		Fetch next from TemporalCursorNom
		into
        @M1000,
        @M1001,
        @M1002,
        @M1003,
        @M1005,
        @M1006,
        @M1007,
        @M1008,
        @M1009,
        @M1010,
        @M1011,
		    @M1012,
		    @M1013,
		    @M1014,
		    @M1015,
		    @M1016,
		    @M1017,
        @M1100,
        @M1101,
        @M1102,
        @M1103,
        @M1104,
        @M1105,
        @M1106,
        @M1107,
        @M1108,
        @M1109,
        @M1110,
        @M1111,
        @M1112,
        @M1113,
        @M1114,
        @M1115,
        @M1116,
        @M1117,
        @M1118,
        @M1119,
        @M1120,
        @M1121,
        @M1122,
        @M1123,
        @M1124,
        @M1125,
        @M1126,
        @M1127,
        @M1128,
        @M1129,
        @M1200,
        @M1201,
        @M1202,
        @M1203,
        @M1204,
        @M1205,
        @M1206,
        @M1207,
        @M1208,
        @M1209,
        @M1210,
        @M1211,
        @M1212,
        @M1213,
        @M1214,
        @M1215,
        @TipoNom,
		@M1216

	WHILE @@FETCH_STATUS = 0
     begin

          if ( @Concepto = 1200 or @Concepto = 1109 ) and ( @M1200 <> 0 )
          begin
                Select @M1200 = @M1201 / @M1200;
          end
          if ( @Concepto = 1000 ) and (@M1000 <> 0)
            select @Resultado = @M1000 + @Resultado
          if ( @Concepto = 1001 ) and (@M1001 <> 0)
            select @Resultado = @M1001 + @Resultado
          if ( @Concepto = 1002 ) and (@M1002 <> 0)
            select @Resultado = @M1002 + @Resultado
          if ( @Concepto = 1003 ) and (@M1003 <> 0)
            select @Resultado = @M1003 + @Resultado
          if ( @Concepto = 1005 ) and (@M1005 <> 0)
            select @Resultado = @M1005 + @Resultado
          if ( @Concepto = 1006 ) and (@M1006 <> 0)
            select @Resultado = @M1006 + @Resultado
          if ( @Concepto = 1007 ) and (@M1007 <> 0)
            select @Resultado = @M1007 + @Resultado
          if ( @Concepto = 1008 ) and (@M1008 <> 0)
            select @Resultado = @M1008 + @Resultado
          if ( @Concepto = 1009 ) and (@M1009 <> 0)
            select @Resultado = @M1009 + @Resultado
          if ( @Concepto = 1010 ) and (@M1010 <> 0)
            select @Resultado = @M1010 + @Resultado
          if ( @Concepto = 1011 ) and (@M1011 <> 0)
            select @Resultado = @M1011 + @Resultado
          if ( @Concepto = 1012 ) and (@M1012 <> 0)
            select @Resultado = @M1012 + @Resultado
          if ( @Concepto = 1013 ) and (@M1013 <> 0)
            select @Resultado = @M1013 + @Resultado
          if ( @Concepto = 1014 ) and (@M1014 <> 0)
            select @Resultado = @M1014 + @Resultado
          if ( @Concepto = 1015 ) and (@M1015 <> 0)
            select @Resultado = @M1015 + @Resultado
          if ( @Concepto = 1016 ) and (@M1016 <> 0)
            select @Resultado = @M1016 + @Resultado
          if ( @Concepto = 1017 ) and (@M1017 <> 0)
            select @Resultado = @M1017 + @Resultado
          if ( @Concepto = 1100 ) and (@M1100 <> 0)
            select @Resultado = @M1100 + @Resultado
          if ( @Concepto = 1101 ) and (@M1101 <> 0)
            select @Resultado = @M1101 + @Resultado
          if ( @Concepto = 1102 ) and (@M1102 <> 0)
            select @Resultado = @M1102 + @Resultado
          if ( @Concepto = 1103 ) and (@M1103 <> 0)
            select @Resultado = @M1103 + @Resultado
          if ( @Concepto = 1104 ) and (@M1104 <> 0)
            select @Resultado = @M1104 + @Resultado
          if ( @Concepto = 1105 ) and (@M1105 <> 0)
            select @Resultado = @M1105 + @Resultado
          if ( @Concepto = 1106 ) and (@M1106 <> 0)
            select @Resultado = @M1106 + @Resultado
          if ( @Concepto = 1107 ) and (@M1107 <> 0)
            select @Resultado = @M1107 + @Resultado
          if ( @Concepto = 1108 ) and (@M1108 <> 0)
            select @Resultado = @M1108 + @Resultado
          if ( @Concepto = 1109 ) and (@M1109 <> 0)
		begin
			select @M1109 = @M1200 * @M1109;
               select @Resultado = @M1109 + @Resultado
		end
          if ( @Concepto = 1110 ) and (@M1110 <> 0)
            select @Resultado = @M1110 + @Resultado
          if ( @Concepto = 1111 ) and (@M1111 <> 0)
            select @Resultado = @M1111 + @Resultado
          if ( @Concepto = 1112 ) and (@M1112 <> 0)
            select @Resultado = @M1112 + @Resultado
          if ( @Concepto = 1113 ) and (@M1113 <> 0)
            select @Resultado = @M1113 + @Resultado
          if ( @Concepto = 1114 ) and (@M1114 <> 0)
            select @Resultado = @M1114 + @Resultado
          if ( @Concepto = 1115 ) and (@M1115 <> 0)
            select @Resultado = @M1115 + @Resultado
          if ( @Concepto = 1116 ) and (@M1116 <> 0)
            select @Resultado = @M1116 + @Resultado
          if ( @Concepto = 1117 ) and (@M1117 <> 0)
            select @Resultado = @M1117 + @Resultado
          if ( @Concepto = 1118 ) and (@M1118 <> 0)
            select @Resultado = @M1118 + @Resultado
          if ( @Concepto = 1119 ) and (@M1119 <> 0)
              select @Resultado = @M1119 + @Resultado
          if ( @Concepto = 1120 ) and (@M1120 <> 0)
            select @Resultado = @M1120 + @Resultado
          if ( @Concepto = 1121 ) and (@M1121 <> 0)
            select @Resultado = @M1121 + @Resultado
          if ( @Concepto = 1122 ) and (@M1122 <> 0)
            select @Resultado = @M1122 + @Resultado
          if ( @Concepto = 1123 ) and (@M1123 <> 0)
            select @Resultado = @M1123 + @Resultado
          if ( @Concepto = 1124 ) and (@M1124 <> 0)
            select @Resultado = @M1124 + @Resultado
          if ( @Concepto = 1125 ) and (@M1125 <> 0)
            select @Resultado = @M1125 + @Resultado
          if ( @Concepto = 1126 ) and (@M1126 <> 0)
            select @Resultado = @M1126 + @Resultado
          if ( @Concepto = 1127 ) and (@M1127 <> 0)
            select @Resultado = @M1127 + @Resultado
          if ( @Concepto = 1128 ) and (@M1128 <> 0)
            select @Resultado = @M1128 + @Resultado
          if ( @Concepto = 1129 ) and (@M1129 <> 0)
            select @Resultado = @M1129 + @Resultado

          if ( @Concepto = 1200 ) and (@M1200 <> 0 )
     	    begin
               select @TP_Clasif = TP_CLAS from TPERIODO where TP_TIPO = @TipoNom;
               if ( @TP_Clasif = 0 )
                 Select @M1200 = 8 * @M1200;
               else
               if ( @TP_Clasif = 1 )
                 Select @M1200 = 48 * @M1200;
               else
               if ( @TP_Clasif = 2 )
                 Select @M1200 = 96 * @M1200;
               else
               if ( @TP_Clasif = 3 )
                 Select @M1200 = 104 * @M1200;
               else
               if ( @TP_Clasif = 4 )
                 Select @M1200 = 208 * @M1200;
               else
               if ( @TP_Clasif = 5 )
                 Select @M1200 = 72 * @M1200;
               select @Resultado = @M1200 + @Resultado
        end
     		if ( @Concepto = 1201 and @M1201 <> 0 )
          select @Resultado = @M1201 + @Resultado
        if ( @Concepto = 1202 and @M1202 <> 0 )
          select @Resultado = @M1202 + @Resultado
        if ( @Concepto = 1203 and @M1203 <> 0 )
          select @Resultado = @M1203 + @Resultado
        if ( @Concepto = 1204 and @M1204 <> 0 )
          select @Resultado = @M1204 + @Resultado
        if ( @Concepto = 1205 and @M1205 <> 0 )
          select @Resultado = @M1205 + @Resultado
        if ( @Concepto = 1206 and @M1206 <> 0 )
          select @Resultado = @M1206 + @Resultado
        if ( @Concepto = 1207 and @M1207 <> 0 )
          select @Resultado = @M1207 + @Resultado
        if ( @Concepto = 1208 and @M1208 <> 0 )
          select @Resultado = @M1208 + @Resultado
        if ( @Concepto = 1209 and @M1209 <> 0 )
          select @Resultado = @M1209 + @Resultado
        if ( @Concepto = 1210 and @M1210 <> 0 )
          select @Resultado = @M1210 + @Resultado
        if ( @Concepto = 1211 and @M1211 <> 0 )
          select @Resultado = @M1211 + @Resultado
        if ( @Concepto = 1212 and @M1212 <> 0 )
          select @Resultado = @M1212 + @Resultado
        if ( @Concepto = 1213 and @M1213 <> 0 )
          select @Resultado = @M1213 + @Resultado
        if ( @Concepto = 1214 and @M1214 <> 0 )
          select @Resultado = @M1214 + @Resultado
        if ( @Concepto = 1215 and @M1215 <> 0 )
          select @Resultado = @M1215 + @Resultado
		if ( @Concepto = 1216 and @M1216 <> 0 )
          select @Resultado = @M1216 + @Resultado


			Fetch next from TemporalCursorNom
     	  into
			      @M1000,
            @M1001,
            @M1002,
            @M1003,
            @M1005,
            @M1006,
            @M1007,
            @M1008,
            @M1009,
            @M1010,
  	        @M1011,
            @M1012,
            @M1013,
            @M1014,
            @M1015,
            @M1016,
            @M1017,
            @M1100,
            @M1101,
            @M1102,
            @M1103,
            @M1104,
            @M1105,
            @M1106,
            @M1107,
            @M1108,
            @M1109,
            @M1110,
            @M1111,
            @M1112,
            @M1113,
            @M1114,
            @M1115,
            @M1116,
            @M1117,
            @M1118,
            @M1119,
            @M1120,
            @M1121,
            @M1122,
            @M1123,
            @M1124,
            @M1125,
            @M1126,
            @M1127,
            @M1128,
            @M1129,
            @M1200,
            @M1201,
            @M1202,
            @M1203,
            @M1204,
            @M1205,
            @M1206,
            @M1207,
            @M1208,
            @M1209,
            @M1210,
            @M1211,
            @M1212,
            @M1213,
            @M1214,
            @M1215,
		    @TipoNom,
            @M1216

	end
  CLOSE TemporalCursorNom
  DEALLOCATE TemporalCursorNom
end
else
begin
  	select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M
	left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
	left outer join NOMINA on NOMINA.PE_YEAR = M.PE_YEAR AND NOMINA.PE_TIPO = M.PE_TIPO AND NOMINA.PE_NUMERO = M.PE_NUMERO AND NOMINA.CB_CODIGO = M.CB_CODIGO
	left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
	left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
	left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
     left outer join COLABORA on COLABORA.CB_CODIGO = M.CB_CODIGO
     where
		( M.PE_YEAR = @Anio ) AND
		( M.CB_CODIGO = @Empleado ) AND
		( M.MO_ACTIVO = 'S' ) AND
		( PERIODO.PE_STATUS = 6 ) AND
		( PERIODO.PE_MES = @Mes ) AND
		( ( @TipoNomina = -1 ) or ( NOMINA.PE_TIPO = @TipoNomina ) ) and
		( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
          ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) ) and
          ( C.CO_TIPO = 1 ) and
		( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        select @Resultado = @M1004
end

  RETURN @Resultado

END
GO


CREATE FUNCTION DBO.SP_HuboReingresoEnMes( @Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER) 
RETURNS Booleano 
AS
BEGIN 	
		declare @FechaIngreso Fecha 
		declare @FechaPeriodoFinMin Fecha
		 
		select top 1 @FechaIngreso= CB_FEC_ING, @FechaPeriodoFinMin=PERIODO.PE_FEC_FIN  from COLABORA, PERIODO 
		where PERIODO.PE_STATUS = 6 and PE_YEAR = @Anio and PE_MES = @Mes  and COLABORA.CB_CODIGO = @Empleado
		order by PE_FEC_FIN 

		if (@FechaIngreso <=@FechaPeriodoFinMin ) 
			return 'N' 
        
		return 'S' 
END 


GO 

CREATE FUNCTION DBO.SP_VALOR_CONCEPTO_2(
 				@Concepto SMALLINT,
				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
				@Razonsocial CHAR(6),
				@TipoNomina SMALLINT,
				@ValidaReingreso CHAR(1)
				 )

RETURNS NUMERIC(15,2)
AS
BEGIN 
	Declare @Resultado  Pesos
	DECLARE @lUsaAcumulaRS Booleano 
	
	set @Resultado = 0 	
	set @lUsaAcumulaRS = 'N' 

	if ( @TipoNomina = -1 ) 
		set @lUsaAcumulaRS  = 'S' 
	
	if ( @lUsaAcumulaRS ='S' and @ValidaReingreso = 'S' ) 
	begin 		
		if  dbo.SP_HuboReingresoEnMes(@Mes, @Anio, @Empleado) = 'N'
			set @lUsaAcumulaRS = 'S' 
		else
			set @lUsaAcumulaRS = 'N' 		
	end 

	if @lUsaAcumulaRS = 'S' 
	begin 
		select @Resultado = dbo.SP_A2(@Concepto, @Mes, @Anio,  @Empleado, @RazonSocial ); 
	end 
	else
	begin 
		select @Resultado = DBO.SP_VALOR_CONCEPTO( @Concepto, @Mes, @Anio, @Empleado, @Razonsocial, @TipoNomina, @ValidaReingreso)
	end 

	return @Resultado 
END 

go

CREATE FUNCTION DBO.SP_RA_2(
  				@Concepto SMALLINT,
  				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
  				@Referencia char(8),
				@RazonSocial char(6),
  				@TipoNomina SMALLINT,  				
  				@ValidaReingreso CHAR(1) )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @Resultado AS NUMERIC(15,2)
	DECLARE @lUsaAcumulaRS Booleano 
	
	set @lUsaAcumulaRS = 'N' 

	if ( @Referencia = '' and @TipoNomina = -1 ) 
		set @lUsaAcumulaRS  = 'S' 
	
	if ( @lUsaAcumulaRS ='S' and @ValidaReingreso = 'S' ) 
	begin 		
		if  dbo.SP_HuboReingresoEnMes(@Mes, @Anio, @Empleado) = 'N'
			set @lUsaAcumulaRS = 'S' 
		else
			set @lUsaAcumulaRS = 'N' 
	end 

	if @lUsaAcumulaRS = 'S' 
	begin 	
		select @Resultado = dbo.SP_A2(@Concepto, @Mes, @Anio,  @Empleado, @RazonSocial ); 
	end 
	else
	begin 
		select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
		from   MOVIMIEN
		left outer join NOMINA on NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
		left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
		left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
		left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
		left outer join COLABORA on COLABORA.CB_CODIGO = MOVIMIEN.CB_CODIGO 
		where ( MOVIMIEN.PE_YEAR = @Anio ) AND
			 ( MOVIMIEN.CB_CODIGO = @Empleado ) AND 		  
				( MOVIMIEN.CO_NUMERO = @Concepto ) AND
				( MOVIMIEN.MO_ACTIVO = 'S' ) AND
				( PERIODO.PE_STATUS = 6 ) AND
				( PERIODO.PE_MES = @Mes ) AND
				( ( @TipoNomina = -1 ) OR ( MOVIMIEN.PE_TIPO = @TipoNomina ) ) AND		    
				( ( @Referencia = '' ) OR ( MOVIMIEN.MO_REFEREN = @Referencia ) ) AND		    
				( ( @RazonSocial = '' ) OR ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) AND            
				  ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) )
	end 

 	if ( @Resultado is NULL )
        select @Resultado = 0
	RETURN @Resultado
END

GO 
 
CREATE FUNCTION DBO.SP_RAS(
		     @Concepto SMALLINT,
    			@NumeroIni SMALLINT,
    			@Numerofin SMALLINT,
			@Anio SMALLINT,
			@Empleado INTEGER,
			@Referencia char(8),
			@RazonSocial CHAR(6),
    		     @TipoNomina SMALLINT,   		    		
			@ValidaReingreso CHAR(1))
RETURNS NUMERIC(15,2)
AS
begin
	declare @Temporal NUMERIC(15,2);
	declare @Resultado NUMERIC(15,2);

  select @Resultado = 0;
  while ( @NumeroIni <= @NumeroFin )
  begin
	  if @Concepto < 1000 
	  	select @Temporal = dbo.SP_RA_2 ( @Concepto, @NumeroIni, @Anio, @Empleado, @Referencia, @RazonSocial, @TipoNomina, @ValidaReingreso )
	  else
		select @Temporal = dbo.SP_VALOR_CONCEPTO_2 ( @Concepto, @NumeroIni, @Anio, @Empleado, @RazonSocial, @TipoNomina, @ValidaReingreso )
		  
	if ( @Temporal is NOT NULL )
	    select @Resultado = @Resultado + @Temporal;
	select @NumeroIni = @NumeroIni + 1;
  end
	Return @Resultado;
end
GO



CREATE FUNCTION dbo.GET_EDAD 
(@FechaIni DATETIME, 
@FechaHasta DATETIME ) 
RETURNS VARCHAR(18) 
AS 

BEGIN 

DECLARE 
@FechaHoy DATETIME, 
@mes1 INT, 
@mes2 INT, 
@anios INT, 
@meses INT, 
@Edad VARCHAR(18) 

SELECT @anios=DATEDIFF(month,@FechaIni, @FechaHasta )/12 

SELECT    @mes1=(DATEPART(month,@FechaIni)), 
   @mes2 =(DATEPART(month,@FechaHasta)) 

IF @mes1 > @mes2 
BEGIN 
   SELECT @meses = (12 -(@mes1 - DATEPART(month,@FechaHasta))) 
END 
ELSE 
BEGIN 
   SELECT @meses = ( @mes2 - DATEPART(month,@FechaIni)) 
END 

RETURN (CONVERT(VARCHAR(3),@anios) + ' A�os ' + CONVERT(varchar(2),@meses) + ' Meses ')    
END 

go


CREATE FUNCTION SP_STATUS_EVALUADORES (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusEvaluadores TABLE
( US_CODIGO  Smallint,
  SE_TOTAL   Integer,
  SE_NUEVA   Integer,
  SE_TERMINA Integer,
  SE_PROCESO Integer,
  SE_STATUS  Smallint
) AS     
BEGIN
	INSERT INTO @StatusEvaluadores
	SELECT US_CODIGO, COUNT(*), 
               SUM( CASE WHEN EV_STATUS <= 2 THEN 1 ELSE 0 END ),
               SUM( CASE WHEN EV_STATUS >= 4 THEN 1 ELSE 0 END ),
               SUM( CASE WHEN EV_STATUS = 3 THEN 1 ELSE 0 END ),
               0
	FROM   EVALUA
	WHERE  ET_CODIGO = @ET_CODIGO
	GROUP BY US_CODIGO

	UPDATE @StatusEvaluadores
	SET    SE_STATUS = CASE WHEN SE_NUEVA = SE_TOTAL THEN 2
                                WHEN SE_TERMINA = SE_TOTAL THEN 4 ELSE 3 END


	RETURN
END
GO


CREATE FUNCTION GET_PORC_CUMP_PUESTO ( @EMPLEADO NUMEROEMPLEADO, @PUESTO CODIGO )
RETURNS FLOAT
AS
begin

     declare @ORDEN1 INTEGER;
     declare @ORDEN2 INTEGER;
     declare @CUANTOS INTEGER;
     declare @CONTADOR INTEGER;
     declare @CUMPLE FLOAT;

     select @CONTADOR = 0;
     select @CUANTOS = 0;

     declare COMPETENCIAS cursor for
     select C1.CA_ORDEN, C2.CA_ORDEN from COMP_PTO
     left outer join COMPETEN on ( COMPETEN.CM_CODIGO = COMP_PTO.CM_CODIGO  )
     left outer join EMP_COMP on ( EMP_COMP.CM_CODIGO = COMP_PTO.CM_CODIGO ) and ( EMP_COMP.CB_CODIGO = @EMPLEADO )
     left outer join CALIFICA C2 on ( C2.CA_CODIGO = COMP_PTO.CA_CODIGO )
     left outer join CALIFICA C1 on ( C1.CA_CODIGO = EMP_COMP.CA_CODIGO )
     where ( COMP_PTO.PU_CODIGO = @PUESTO );
     open COMPETENCIAS;
     fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @ORDEN2 is not NULL )
          begin
	       if ( @ORDEN1 >= @ORDEN2 )
	       begin
	            select @CONTADOR = @CONTADOR + 1;
           end
           
			select @CUANTOS = @CUANTOS + 1;
          end
          fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     end;
     close COMPETENCIAS;
     deallocate COMPETENCIAS;


     if ( @CUANTOS > 0 )
     begin

          select @CUMPLE = ROUND( ( ( @CONTADOR * 100 / @CUANTOS )  ), 0, 1 );
     end
     else
     begin
          select @CUMPLE = 0;
     end

RETURN @CUMPLE
END
GO



CREATE FUNCTION Eficiencia_Por_Area_Por_Fecha(
	@Area		VARCHAR(30)
,	@Inicial	Fecha
,	@Final		Fecha )
RETURNS @Tabla	TABLE (
	LlaveArea	INT
,	Codigo		CHAR(6)
,	Nombre		VARCHAR(30)
,	Fecha		DATETIME
,	Invertidas	NUMERIC(15,2)
,	Ganadas	NUMERIC(15,2)
,	Eficiencia	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN

	INSERT INTO @Tabla ( 
		LlaveArea
	,	Codigo
	,	Nombre
	,	Fecha
	,	Invertidas
	,	Ganadas 
	)
	SELECT	AREA.LLAVE
	,	CB_AREA
	,	TB_ELEMENT
	,	AU_FECHA
	,	SUM(WK_TIEMPO/60.0)
	,	ROUND(SUM(WK_PIEZAS / WK_STD_HR ),4 )
	FROM	WORKS 
		JOIN AREA 
			ON CB_AREA = TB_CODIGO
	WHERE	WK_TIPO	= 0 
	AND	AU_FECHA	BETWEEN @Inicial AND @Final 
	AND	CB_AREA	LIKE @Area
	GROUP BY 
		AREA.LLAVE
	,	CB_AREA
	,	TB_ELEMENT
	,	AU_FECHA
	ORDER BY
		CB_AREA

	/* Segunda pasada, calcula eficiancia sobre totales */
	UPDATE @Tabla
	SET	Eficiencia	= Ganadas/ Invertidas * 100.0
	WHERE	Invertidas	> 0

	RETURN
END
GO


CREATE FUNCTION TiempoMuerto_Por_Motivo_Detalle(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15)
,	Nombre		VARCHAR(30)
,	HorasHombre	NUMERIC(15,2)
,	Area		VARCHAR(10)
,	NombreArea	VARCHAR(100)
,	Fecha		DATETIME
,	Porcentaje	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN
	INSERT INTO @Tabla ( Codigo, Nombre, Area, NombreArea, Fecha, HorasHombre )

	SELECT	W.WK_TMUERTO
	,	TM.TB_ELEMENT
	,	W.CB_AREA
	,	A.TB_ELEMENT
	,	W.AU_FECHA
	,	SUM(W.WK_TIEMPO/60.0)
	FROM	WORKS W 
			JOIN TMUERTO TM 
				ON TM.TB_CODIGO	= W.WK_TMUERTO
			JOIN AREA A
				ON A.TB_CODIGO	= W.CB_AREA
	WHERE	W.WK_TIPO = 2 
	AND	W.AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY
		W.WK_TMUERTO
	,	TM.TB_ELEMENT
	,	W.CB_AREA
	,	A.TB_ELEMENT
	,	W.AU_FECHA
	ORDER BY
		W.WK_TMUERTO
	,	W.CB_AREA
	,	W.AU_FECHA
	
	UPDATE @Tabla
	SET	Porcentaje = HorasHombre / ( SELECT SUM(HorasHombre) FROM @Tabla ) * 100.0

	RETURN
END
GO




CREATE FUNCTION F_GRAF_EVALUACIONES (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusGrafica2 TABLE
( 
  STATUS integer,
  CANTIDAD Integer

) AS     
BEGIN
	DECLARE @Valor Integer;
	DECLARE @Contador Integer;
	SET @Contador = 0

	WHILE @Contador <=4

	BEGIN	

	SELECT @Valor = ( SUM( CASE WHEN EV_STATUS = @Contador THEN 1 ELSE 0 END ) ) 
	FROM   EVALUA
	WHERE  ET_CODIGO = @ET_CODIGO

    INSERT INTO @StatusGrafica2 values ( @Contador,@Valor);
	SET @Contador = @Contador+1
	END

	RETURN
END
GO


CREATE  FUNCTION F_GRAF_EVALUADORES (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusGrafica4 TABLE
( 
  STATUS integer,
  CANTIDAD Integer

) AS     
BEGIN
	DECLARE @Valor Integer;
	DECLARE @Contador Integer;
	DECLARE @Contador2 Integer;
	DECLARE @TValor Integer;
	DECLARE @Temporal Integer;

	SET @Contador = 0
	SET @TValor = 0
	SET @Temporal = 0

	WHILE @Contador <=4
	BEGIN
	
		IF @Contador=0 
		BEGIN
			select @Valor = SUM( CASE WHEN SE_STATUS <=2 THEN 1 ELSE 0 END )
			FROM DBO.SP_STATUS_EVALUADORES(@ET_CODIGO)
			SET @Contador2 = 0
			INSERT INTO @StatusGrafica4 values ( @Contador2,@Valor);
			SET @Contador=2		
		END
		
		IF @Contador>=4
		BEGIN
			select @Valor = SUM( CASE WHEN SE_STATUS >=4 THEN 1 ELSE 0 END )
			FROM   DBO.SP_STATUS_EVALUADORES(@ET_CODIGO)
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica4 values ( @Contador2,@Valor);
		END

		IF @Contador=3
			BEGIN
			select @Valor = SUM( CASE WHEN SE_STATUS =3 THEN 1 ELSE 0 END )
			FROM   DBO.SP_STATUS_EVALUADORES(@ET_CODIGO)
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica4 values ( @Contador2,@Valor);
		END
		SET @Contador = @Contador+1

	END

	RETURN
END
GO


CREATE FUNCTION F_GRAF_EVALUADOS (
  @ET_CODIGO FolioGrande
)
RETURNS @StatusGrafica3 TABLE
( 
  STATUS integer,
  CANTIDAD Integer

) AS     
BEGIN
	DECLARE @Valor Integer;
	DECLARE @Contador Integer;
	DECLARE @Contador2 Integer;
	SET @Contador = 0

	WHILE @Contador <=4
	BEGIN	
		IF @Contador=0 
		BEGIN
			SELECT @Valor = ( SUM( CASE WHEN SJ_STATUS <= 2 THEN 1 ELSE 0 END ) ) 
			FROM   SUJETO
			WHERE  ET_CODIGO = @ET_CODIGO
			SET @Contador2 = 0
			INSERT INTO @StatusGrafica3 values ( @Contador2,@Valor);
			SET @Contador=2		
		END

		IF @Contador>=4 
		BEGIN

			SELECT @Valor = ( SUM( CASE WHEN SJ_STATUS >=4 THEN 1 ELSE 0 END ) ) 
			FROM   SUJETO
			WHERE  ET_CODIGO = @ET_CODIGO
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica3 values ( @Contador2,@Valor);
		END

		IF @Contador=3
		BEGIN

			SELECT @Valor = ( SUM( CASE WHEN SJ_STATUS = 3 THEN 1 ELSE 0 END ) ) 
			FROM   SUJETO
			WHERE  ET_CODIGO = @ET_CODIGO
			SET @Contador2 = @Contador
			INSERT INTO @StatusGrafica3 values ( @Contador2,@Valor);
		END
		
		SET @Contador = @Contador+1
	END

	RETURN
END
GO

CREATE FUNCTION GET_PORC_PLANACC( @CB_CODIGO INT )
RETURNS FLOAT 
AS
BEGIN

/* INICIA SECCION PLAN DE ACCION */

DECLARE

@CM_DESCRIP VARCHAR(120),
@CUMPLE CHAR(1),
@TOTAL_S1 FLOAT,
@TOTAL_N1 FLOAT

 
SELECT @TOTAL_S1 =0,@TOTAL_N1=0

DECLARE CUMPLIDO_cursor CURSOR FOR 
select A.AN_CODIGO,E.EP_TERMINO
from EMP_PLAN E
left outer join ACCION A on ( A.AN_CODIGO = E.AN_CODIGO ) 
where ( E.CB_CODIGO = @CB_CODIGO ) order by E.EP_FEC_INI 

OPEN CUMPLIDO_cursor
FETCH NEXT FROM CUMPLIDO_cursor 
INTO @CM_DESCRIP,@CUMPLE

 

WHILE @@FETCH_STATUS = 0
BEGIN
      IF @CUMPLE = 'S'
            SELECT @TOTAL_S1 = @TOTAL_S1 + 1 

   
      SELECT @TOTAL_N1 = @TOTAL_N1 + 1
FETCH NEXT FROM CUMPLIDO_cursor 

INTO @CM_DESCRIP,@CUMPLE

END

CLOSE CUMPLIDO_cursor

DEALLOCATE CUMPLIDO_cursor

/*Termina cursor de Cumplidos */

 IF @TOTAL_N1 > 0

      SELECT @TOTAL_N1 = ROUND(@TOTAL_S1 / @TOTAL_N1 * 100 , 0) 

 

RETURN @TOTAL_N1
END
GO

CREATE FUNCTION SP_DISM_TASA_INFONAVIT( @Empleado NumeroEmpleado, @Fecha Fecha  ) Returns NUMERIC(15, 2)
AS
BEGIN
  declare @SalarioIntegrado Numeric( 15, 2);
  declare @SalarioMinimo Numeric( 15, 2 );
  declare @FechaSalarioMinimo Fecha;
  declare @TasaIndex Numeric( 15, 2 );
  declare @ZonaGeografica Char( 1 );
  declare @Resultado SmallInt;
  declare @FechaIngreso Fecha;
  declare @AplicaDisminucion Booleano;
  declare @TipoCredito SmallInt;
  declare @FechaInicio Fecha;
  declare @Status SmallInt;

  declare @TasaNueva Numeric( 15, 2 );
  declare @FechaTasa Fecha;
  
  SET @TasaNueva = 0;
  set @FechaTasa = @Fecha

  select @FechaInicio = MIN(KI_FECHA) from KARINF where CB_CODIGO = @Empleado

  if ( @FechaInicio < @Fecha )
  begin
	   set @FechaInicio = @Fecha;
  end
  
  select @FechaIngreso = CB_FEC_ING from COLABORA where CB_CODIGO = @Empleado;


  select TOP 1 @TasaIndex = CB_INFTASA, @AplicaDisminucion = CB_INFDISM, @TipoCredito = CB_INFTIPO  
  from  KARINF
  where ( CB_CODIGO = @Empleado ) AND ( KI_FECHA <= @FechaInicio  ) order by KI_FECHA desc

  if ( @TasaIndex is not NULL ) and ( @TipoCredito = 1 ) and ( @AplicaDisminucion = 'S' )
  begin   
		    /*Obtener el status del empleado ala fecha de inicio del credito a la fecha de inicio del bimestre*/
        select @Status = DBO.SP_STATUS_ACT( @Fecha, @Empleado )

        if ( @Status <> 1 ) and (@FechaIngreso > @Fecha) /*Para los ingresos a medio bimestres y reingresos*/
		    begin
             /*Se trae de nuevo la fecha de ingreso debido a que puede haber mas de dos cortes*/
             select @FechaIngreso = ( select TOP 1 CB_FEC_ING from KARDEX where CB_CODIGO = @Empleado and
									                    ( CB_FECHA >= @Fecha) and ( CB_TIPO = 'ALTA') order by CB_FECHA asc)
             if ( @FechaIngreso > @Fecha )
             begin
                  set @FechaTasa = @FechaIngreso;
             end
		    end

        if ( @Status = 1 ) or ( @FechaTasa = @FechaIngreso )
        begin
              select @ZonaGeografica = CB_ZONA_GE
              from   COLABORA
              where ( CB_CODIGO = @Empleado );


              select @FechaSalarioMinimo = MAX( SM_FEC_INI )
              from SAL_MIN
              where ( SM_FEC_INI <= @FechaTasa )


              IF ( @ZonaGeografica = 'A' )
                select @SALARIOMINIMO = SM_ZONA_A
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )

              ELSE IF ( @ZonaGeografica = 'B' )
                select @SalarioMinimo = SM_ZONA_B
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )

              ELSE IF ( @ZonaGeografica = 'C' )
                select @SalarioMinimo = SM_ZONA_C
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )
              ELSE
             
              SET @SalarioMinimo = 0;

              if ( @SalarioMinimo > 0 )
              begin
                    select @SalarioIntegrado = CB_SAL_INT
                    FROM   DBO.SP_FECHA_KARDEX( @FechaTasa, @Empleado );

                    SET @SalarioIntegrado = @SalarioIntegrado / @SalarioMinimo;

                    SELECT @TasaNueva = Tasa
                    FROM   DBO.SP_A80_ESCALON( @TasaIndex, @SalarioIntegrado, @FechaTasa );
              end
        end
end

   if ( @TasaNueva = 0 ) or ( @TasaNueva is Null )
    set @TasaNueva = COALESCE( @TasaIndex, 0)

   Return @TasaNueva

END
GO

CREATE FUNCTION GET_COMPARTE ()
RETURNS VARCHAR(50)
AS 
BEGIN 
      RETURN 'COMPARTE'
END 
GO

CREATE FUNCTION DBO.GET_PORC_COMPETE( @CB_CODIGO INT )
RETURNS FLOAT 
AS
BEGIN

DECLARE 
@CM_DESCRIP VARCHAR(120),
@CUMPLE CHAR(1),
@TOTAL_S FLOAT,
@TOTAL_N FLOAT
SELECT @TOTAL_S =0, @TOTAL_N=0

DECLARE CUMPLIDO_cursor CURSOR FOR 
select CM_DESCRIP,(select dbo.SP_STR_TO_BOOL( C1.CA_ORDEN - C2.CA_ORDEN )) as CUMPLE
from COMP_PTO 
left outer join COMPETEN on COMPETEN.CM_CODIGO = COMP_PTO.CM_CODIGO
left outer join EMP_COMP on ( EMP_COMP.CM_CODIGO = COMP_PTO.CM_CODIGO ) 
and ( EMP_COMP.CB_CODIGO = @CB_CODIGO )
left outer join CALIFICA C2 ON C2.CA_CODIGO = COMP_PTO.CA_CODIGO
left outer join CALIFICA C1 ON C1.CA_CODIGO = EMP_COMP.CA_CODIGO 				
where( COMP_PTO.PU_CODIGO = (SELECT CB_PUESTO FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO) )
order by CM_DESCRIP


OPEN CUMPLIDO_cursor
FETCH NEXT FROM CUMPLIDO_cursor 
INTO @CM_DESCRIP,@CUMPLE

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @CUMPLE = 'S'
		SELECT @TOTAL_S = @TOTAL_S + 1 
	
	SELECT @TOTAL_N = @TOTAL_N + 1

FETCH NEXT FROM CUMPLIDO_cursor 
INTO @CM_DESCRIP,@CUMPLE
   
END

CLOSE CUMPLIDO_cursor
DEALLOCATE CUMPLIDO_cursor

/* ---- Termina cursor de Cumplidos --------------------------------------*/

IF @TOTAL_N > 0
	SELECT @TOTAL_N = ROUND(@TOTAL_S / @TOTAL_N  * 100 , 0)

RETURN @TOTAL_N

END

GO

CREATE FUNCTION Eficiencia_Por_Area(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(6),
	Nombre		VARCHAR(30),
	Invertidas	NUMERIC(15,2),
	Ganadas		NUMERIC(15,2),
	Eficiencia	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN

	INSERT INTO @Tabla ( Codigo, Nombre, Invertidas, Ganadas )
	SELECT CB_AREA, TB_ELEMENT, SUM(WK_TIEMPO/60.0), ROUND(SUM(WK_PIEZAS / WK_STD_HR ),4 )
	FROM WORKS JOIN AREA ON CB_AREA = TB_CODIGO
	WHERE WK_TIPO = 0 AND AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY CB_AREA, TB_ELEMENT
	ORDER BY 1, 2

	/* Segunda pasada, calcula eficiancia sobre totales */
	UPDATE @Tabla
	SET	Eficiencia = Ganadas/ Invertidas * 100.0
	WHERE Invertidas > 0

	RETURN
END
GO

CREATE FUNCTION Eficiencia_Por_Producto(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15),
	Nombre		VARCHAR(30),
	Producidas	NUMERIC(15,2),
	Standard	NUMERIC(15,2),
	Eficiencia	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN

	INSERT INTO @Tabla ( Codigo, Nombre, Producidas, Standard )
	SELECT WORKS.AR_CODIGO, AR_NOMBRE, ROUND(SUM(WK_PIEZAS),2), SUM(WK_TIEMPO/60.0 * WK_STD_HR)
	FROM WORKS JOIN PARTES ON WORKS.AR_CODIGO = PARTES.AR_CODIGO
	WHERE WK_TIPO = 0 AND AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY WORKS.AR_CODIGO, AR_NOMBRE
	ORDER BY 1, 2

	/* Segunda pasada, calcula eficiancia sobre totales */
	UPDATE @Tabla
	SET	Eficiencia = Producidas / Standard * 100.0
	WHERE Standard > 0

	RETURN
END
GO


CREATE FUNCTION TiempoMuerto_Por_Motivo(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15),
	Nombre		VARCHAR(30),
	HorasHombre	NUMERIC(15,2),
	Porcentaje	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN
	INSERT INTO @Tabla ( Codigo, Nombre, HorasHombre )
	SELECT WK_TMUERTO, TB_ELEMENT, SUM(WK_TIEMPO/60.0)
	FROM WORKS JOIN TMUERTO ON WORKS.WK_TMUERTO = TB_CODIGO
	WHERE WK_TIPO = 2 AND AU_FECHA BETWEEN @Inicial AND @Final
	GROUP BY WK_TMUERTO, TB_ELEMENT
	ORDER BY 1


	/* Segunda pasada, calcula porcentaje */
	UPDATE @Tabla
	SET	Porcentaje = HorasHombre / ( SELECT SUM(HorasHombre) FROM @Tabla ) * 100.0

	RETURN
END
GO

CREATE FUNCTION Defectos_Por_Motivo(
	@Inicial	Fecha,
	@Final		Fecha )
RETURNS @Tabla	TABLE (
	Codigo		CHAR(15),
	Nombre		VARCHAR(30),
	Defectos	NUMERIC(15,2),
	Porcentaje	NUMERIC(15,2) DEFAULT 0 )
AS
BEGIN
	INSERT INTO @Tabla ( Codigo, Nombre, Defectos )
	SELECT TB_CODIGO, TB_ELEMENT, SUM(DE_PIEZAS)
	FROM DEFECTO 
		JOIN TDEFECTO ON DE_CODIGO = TB_CODIGO
		JOIN CED_INSP ON DEFECTO.CI_FOLIO = CED_INSP.CI_FOLIO
		JOIN CEDULA ON CED_INSP.CE_FOLIO = CEDULA.CE_FOLIO
	WHERE CE_FECHA BETWEEN @Inicial AND @Final
	GROUP BY TB_CODIGO, TB_ELEMENT
	ORDER BY 1


	/* Segunda pasada, calcula porcentaje */
	UPDATE @Tabla
	SET	Porcentaje = Defectos / ( SELECT SUM(Defectos) FROM @Tabla ) * 100.0

	RETURN
END
GO

CREATE FUNCTION SP_STR_TO_BOOL( @Valor INTEGER )
RETURNS CHAR(1)
AS
BEGIN
  if( @Valor >= 0 )
    Return 'S'
    Return 'N'
END
GO

CREATE FUNCTION SP_CHECADA_MOT
	( @FECHA DATETIME, @EMPLEADO INTEGER, @NUMERO SMALLINT )
RETURNS CHAR(4)
AS
BEGIN
	declare @Resultado CHAR(4)
	declare @Ciclo SMALLINT
  	SET @Ciclo = 1
  	declare Temporal cursor READ_ONLY STATIC for
     		select CH_MOTIVO from CHECADAS
		where ( CB_CODIGO = @EMPLEADO ) and ( AU_FECHA = @FECHA ) and
		( CH_TIPO < 5 ) and ( CH_SISTEMA <> 'S' )
		order by CH_H_REAL
	Open Temporal
	if ( @@CURSOR_ROWS >= @NUMERO )
	begin
		Fetch Next From Temporal Into @Resultado
		while ( @@Fetch_Status = 0 ) and ( @Ciclo <> @NUMERO )
		begin
			SET @Ciclo = @Ciclo + 1
			Fetch Next From Temporal Into @Resultado
		end
	end
	if ( @Resultado is Null ) or ( @Ciclo <> @NUMERO )
		SET @Resultado = ''

	close Temporal
	deallocate Temporal

	RETURN @Resultado
END
GO

CREATE FUNCTION SP_CHECADA_USUARIO
	( @FECHA DATETIME, @EMPLEADO INTEGER, @NUMERO SMALLINT )
RETURNS Int
AS
BEGIN
	declare @Resultado Int
	declare @Ciclo SMALLINT
  	SET @Ciclo = 1
  	declare Temporal cursor READ_ONLY STATIC for
     		select US_CODIGO from CHECADAS
		where ( CB_CODIGO = @EMPLEADO ) and ( AU_FECHA = @FECHA ) and
		( CH_TIPO < 5 ) and ( CH_SISTEMA <> 'S' )
		order by CH_H_REAL
	Open Temporal
	if ( @@CURSOR_ROWS >= @NUMERO )
	begin
		Fetch Next From Temporal Into @Resultado
		while ( @@Fetch_Status = 0 ) and ( @Ciclo <> @NUMERO )
		begin
			SET @Ciclo = @Ciclo + 1
			Fetch Next From Temporal Into @Resultado
		end
	end
	if ( @Resultado is Null ) or ( @Ciclo <> @NUMERO )
		SET @Resultado = 0

	close Temporal
	deallocate Temporal
	
	RETURN @Resultado
END
GO

CREATE FUNCTION TDERECHO( @Derecho INTEGER, @Posicion INTEGER )
RETURNS CHAR(1)
AS
BEGIN

	return case @Derecho & POWER(2,@Posicion)
		when POWER(2,@Posicion) then 'S'
		else 'N' end
END
GO

CREATE FUNCTION QuitaAcentos(
@Texto Formula
) RETURNS Formula
AS
BEGIN
	declare @Resultado Formula;
	select @Resultado = @Texto;
	select @Resultado = REPLACE(@Resultado, '�', 'a');
	select @Resultado = REPLACE(@Resultado, '�', 'e');
	select @Resultado = REPLACE(@Resultado, '�', 'i');
	select @Resultado = REPLACE(@Resultado, '�', 'o');
	select @Resultado = REPLACE(@Resultado, '�', 'u');
	select @Resultado = REPLACE(@Resultado, '�', 'n');
	select @Resultado = REPLACE(@Resultado, '�', 'u');

	select @Resultado = Upper(@Resultado);

	Return @Resultado ;
END
GO

CREATE FUNCTION G_D
(
	@Tabla varchar(10), 
	@Campo varchar(10)
)
RETURNS varchar(100)
AS
BEGIN
	Declare @Result varchar(100)
	set @Result = (
	select
		case 
			when comm.text = 'CREATE DEFAULT BooleanoNO AS ''N'' ' then 'N'
			when comm.text = 'CREATE DEFAULT CodigoVacio AS '''' ' then ''
			when comm.text = 'CREATE DEFAULT FechaVacia AS ''12/30/1899'' ' then '12/30/1899'
			when comm.text = 'CREATE DEFAULT StringVacio AS '''' ' then ''
			when comm.text = 'CREATE DEFAULT Cero AS 0 ' then '0'
			else '' 
		end as valorDefault
	FROM         sysobjects AS tbl INNER JOIN
                      syscolumns AS col ON tbl.id = col.id INNER JOIN
                      syscomments AS comm ON col.cdefault = comm.id
	WHERE tbl.xtype = 'u' and tbl.name = @Tabla and col.name = @Campo)
	return @Result
END
GO

create FUNCTION SP_FECHA_SILLA_KARDEX (
	@EMPLEADO INTEGER,
	@FECHA DATETIME,
	@LAYOUT CHAR(6),
	@HORA CHAR(4) )
RETURNS @RegKardex TABLE (
      	EM_FECHA DATETIME,
	EM_HORA CHAR(4),
	SL_CODIGO CHAR(6),
	US_CODIGO SMALLINT,
	EM_FEC_MOV DATETIME,
	EM_HOR_MOV CHAR(4) )
AS
BEGIN
     insert into @RegKardex
             select  TOP 1 EM_FECHA, EM_HORA, SL_CODIGO, US_CODIGO, EM_FEC_MOV, EM_HOR_MOV
             from KAR_EMPSIL
             where (LY_CODIGO = @LAYOUT) and ( ( CB_CODIGO = @Empleado ) and (  EM_FECHA <= @Fecha  ) )
             order by LLAVE desc,EM_FECHA desc, EM_HORA desc
     if ( @@ROWCOUNT = 0 )
     begin
          set @Fecha = '12/30/1899'
          insert into @RegKardex values ( @Fecha, '0000', '', 0, @Fecha, '0000' )
     end
     return
END
GO

CREATE FUNCTION SP_CERTIFIC_EMPL (
	@USUARIO INTEGER,
	@FECHA DATETIME
	 )
RETURNS @RegKardex TABLE (
      	CI_CODIGO CHAR(6),
		KI_FEC_CER DATETIME,
		CB_CODIGO INT,
		KI_RENOVAR INT,
		KI_APROBO CHAR(1) 
     )
AS
BEGIN
     
 insert into @RegKardex
				select CI_CODIGO,KI_FEC_CER,CB_CODIGO,KI_RENOVAR,KI_APROBO 
				from KAR_CERT 
				where KI_APROBO = 'S' and
					 ( KI_RENOVAR = 0 or ( DateAdd(day,KI_RENOVAR,KI_FEC_CER ) >= @FECHA ) and (KI_FEC_CER <= @FECHA) )and
					 ( CB_CODIGO in (select CB_CODIGO from dbo.SP_LISTA_EMPLEADOS(@FECHA,@USUARIO ) ) )

    return
END
GO

CREATE FUNCTION SP_SALDOPRE ( @Empleado NumeroEmpleado, 
			@Concepto Concepto,
			@Referencia Referencia,
			@FechaPeriodo Fecha )
RETURNS Pesos 
AS
begin
  declare @Monto Pesos;
  declare @SaldoI Pesos;
  declare @Cargos Pesos;
  declare @Abonos Pesos;
  declare @Deducci Pesos;
  declare @Resultado Pesos;
  
  select @Monto  	= 0;
  select @SaldoI  	= 0;
  select @Cargos  	= 0;
  select @Abonos  	= 0;
  select @Deducci	= 0;
  select @Resultado = 0;
  	

  	SELECT 	@Monto = Coalesce( PRESTAMO.PR_MONTO, 0 ), 
		@SaldoI = coalesce( PRESTAMO.PR_SALDO_I, 0 )
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	WHERE ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_REFEREN = @Referencia)
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )

	SELECT @Deducci=  coalesce(sum(MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI),0 ) 
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	LEFT OUTER JOIN MOVIMIEN ON MOVIMIEN.CB_CODIGO = PRESTAMO.CB_CODIGO
			AND MOVIMIEN.CO_NUMERO = TPRESTA.TB_CONCEPT
			AND MOVIMIEN.MO_REFEREN = PRESTAMO.PR_REFEREN
	LEFT OUTER JOIN NOMINA 
 			ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
			AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO 
			AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
			AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	LEFT OUTER JOIN PERIODO
			ON NOMINA.PE_YEAR = PERIODO.PE_YEAR 
			AND NOMINA.PE_TIPO = PERIODO.PE_TIPO 
			AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO 

	WHERE 	( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_REFEREN = @Referencia)
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND	( NOMINA.NO_STATUS=6 ) 
	AND ( MOVIMIEN.MO_ACTIVO = 'S') 
	AND ( MOVIMIEN.CO_NUMERO = @Concepto )
	AND ( PE_FEC_FIN>= PR_FECHA )
	AND ( PE_FEC_FIN <= @FechaPeriodo)
	
	
	SELECT 	@Cargos = coalesce( sum(PCAR_ABO.CR_CARGO ), 0 ),
		@Abonos = coalesce( sum(PCAR_ABO.CR_ABONO) , 0 ) 
	FROM PCAR_ABO 
	LEFT OUTER JOIN PRESTAMO ON PRESTAMO.CB_CODIGO = PCAR_ABO.CB_CODIGO 	
							 AND PRESTAMO.PR_TIPO = PCAR_ABO.PR_TIPO
	  						 AND PRESTAMO.PR_REFEREN = PCAR_ABO.PR_REFEREN
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	where  ( PCAR_ABO.CR_FECHA<= @FechaPeriodo )
	AND ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_REFEREN = @Referencia)
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )
	
	Select @Resultado = @Monto - @SaldoI - @Deducci - @Abonos + @Cargos;
		
	RETURN @Resultado
end
GO

CREATE FUNCTION SP_SALDOAHO ( @Empleado NumeroEmpleado, 
			@Concepto Concepto,
			@FechaPeriodo Fecha )
RETURNS Pesos 
AS
begin
  declare @Monto Pesos;
  declare @SaldoI Pesos;
  declare @Cargos Pesos;
  declare @Abonos Pesos;
  declare @Deducci Pesos;
  declare @Resultado Pesos;
  
  select @SaldoI  	= 0;
  select @Cargos  	= 0;
  select @Abonos  	= 0;
  select @Deducci	= 0;
  select @Resultado = 0;
  	

  	SELECT 	@SaldoI = coalesce( AHORRO.AH_SALDO_I, 0 )
	FROM AHORRO
	LEFT OUTER JOIN TAHORRO ON TAHORRO.TB_CODIGO = AHORRO.AH_TIPO
	WHERE ( AHORRO.CB_CODIGO = @Empleado )
	AND ( AHORRO.AH_STATUS = 0 )
	AND ( TAHORRO.TB_CONCEPT = @Concepto )
	
	SELECT @Deducci=  coalesce(sum(MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI),0 ) 
	FROM AHORRO
	LEFT OUTER JOIN TAHORRO ON TAHORRO.TB_CODIGO = AHORRO.AH_TIPO
	LEFT OUTER JOIN MOVIMIEN ON MOVIMIEN.CB_CODIGO = AHORRO.CB_CODIGO
			AND MOVIMIEN.CO_NUMERO = TAHORRO.TB_CONCEPT
	LEFT OUTER JOIN NOMINA 
 			ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
			AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO 
			AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
			AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	LEFT OUTER JOIN PERIODO
			ON NOMINA.PE_YEAR = PERIODO.PE_YEAR 
			AND NOMINA.PE_TIPO = PERIODO.PE_TIPO 
			AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO 

	WHERE  ( AHORRO.CB_CODIGO = @Empleado )
	AND ( AHORRO.AH_STATUS = 0 )
	AND ( NOMINA.NO_STATUS=6 )
	AND ( MOVIMIEN.MO_ACTIVO = 'S') 
	AND ( MOVIMIEN.CO_NUMERO = @Concepto )
	AND ( PE_FEC_FIN>= AH_FECHA )
	AND ( PE_FEC_FIN <= @FechaPeriodo)
	
		
	
	SELECT 	@Cargos = coalesce( sum(ACAR_ABO.CR_CARGO ), 0 ),
		@Abonos = coalesce( sum(ACAR_ABO.CR_ABONO) , 0 ) 
	FROM ACAR_ABO 
	LEFT OUTER JOIN AHORRO ON AHORRO.CB_CODIGO = ACAR_ABO.CB_CODIGO AND AHORRO.AH_TIPO = ACAR_ABO.AH_TIPO
	LEFT OUTER JOIN TAHORRO ON TAHORRO.TB_CODIGO = AHORRO.AH_TIPO
	where  ( ACAR_ABO.CR_FECHA<= @FechaPeriodo )
	AND ( AHORRO.CB_CODIGO = @Empleado )
	AND ( AHORRO.AH_STATUS = 0 )
	AND ( TAHORRO.TB_CONCEPT = @Concepto )
		
	Select @Resultado = @Deducci + @SaldoI + @Abonos - @Cargos;
		
	RETURN @Resultado
end
GO

CREATE FUNCTION SP_SALDOPRE_SR ( @Empleado NumeroEmpleado, 
			@Concepto Concepto,
			@FechaPeriodo Fecha )
RETURNS Pesos 
AS
begin
  declare @Monto Pesos;
  declare @SaldoI Pesos;
  declare @Cargos Pesos;
  declare @Abonos Pesos;
  declare @Deducci Pesos;
  declare @Resultado Pesos;
  declare @ResPresta Pesos;
  
  select @Monto  	= 0;
  select @SaldoI  	= 0;
  select @Cargos  	= 0;
  select @Abonos  	= 0;
  select @Deducci	= 0;
  select @Resultado = 0;
  select @ResPresta = 0;
  	
	
	SELECT 	@Monto = sum(coalesce( PRESTAMO.PR_MONTO, 0 )), 
			@SaldoI = sum(coalesce( PRESTAMO.PR_SALDO_I, 0 ))
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	WHERE ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )
	
	
	SELECT @Deducci=  coalesce(sum(MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI),0 ) 
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	LEFT OUTER JOIN MOVIMIEN ON MOVIMIEN.CB_CODIGO = PRESTAMO.CB_CODIGO
			AND MOVIMIEN.CO_NUMERO = TPRESTA.TB_CONCEPT
			AND MOVIMIEN.MO_REFEREN = PRESTAMO.PR_REFEREN
	LEFT OUTER JOIN NOMINA 
			ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
			AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO 
			AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
			AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	LEFT OUTER JOIN PERIODO
			ON NOMINA.PE_YEAR = PERIODO.PE_YEAR 
			AND NOMINA.PE_TIPO = PERIODO.PE_TIPO 
			AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO 

	WHERE  ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( NOMINA.NO_STATUS=6 ) 
	AND ( MOVIMIEN.MO_ACTIVO = 'S') 
	AND ( MOVIMIEN.CO_NUMERO = @Concepto )
	AND ( PE_FEC_FIN>= PR_FECHA )
	AND ( PE_FEC_FIN <= @FechaPeriodo)
		
		
	SELECT 	@Cargos = coalesce( sum(PCAR_ABO.CR_CARGO ), 0 ),
			@Abonos = coalesce( sum(PCAR_ABO.CR_ABONO) , 0 ) 
	FROM PCAR_ABO 
	LEFT OUTER JOIN PRESTAMO ON PRESTAMO.CB_CODIGO = PCAR_ABO.CB_CODIGO 
							 AND PRESTAMO.PR_TIPO = PCAR_ABO.PR_TIPO
							 AND PRESTAMO.PR_REFEREN = PCAR_ABO.PR_REFEREN
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	where  ( PCAR_ABO.CR_FECHA<= @FechaPeriodo )
	AND ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )
	
	SELECT @RESPRESTA = @MONTO - @SALDOI - @DEDUCCI - @ABONOS + @CARGOS;

	SELECT @RESULTADO = @RESULTADO + @RESPRESTA;
		
	RETURN Coalesce( @Resultado,0)
end
GO

CREATE FUNCTION SP_TIENE_ENROLAMIENTO_SUPERVISORES() RETURNS Booleano
AS
BEGIN
	DECLARE @GLOBAL_Usar_Enrolamiento_de_Supervisores FolioChico
	
	SET @GLOBAL_Usar_Enrolamiento_de_Supervisores = 288
	
	IF ( SELECT COUNT(*) FROM GLOBAL WHERE GL_FORMULA = 'S' AND GL_CODIGO = @GLOBAL_Usar_Enrolamiento_de_Supervisores ) > 0 
		RETURN 'S'
		
	RETURN 'N'
END
GO

CREATE FUNCTION SP_GET_NIVEL_SUPERVISORES() RETURNS Formula
AS
BEGIN
	DECLARE @GLOBAL_Super_Nivel_de_Organigrama FolioChico
	DECLARE @Resultado Formula
	
	SET @GLOBAL_Super_Nivel_de_Organigrama = 120
		
	SELECT @Resultado = GL_FORMULA  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_Super_Nivel_de_Organigrama 
	
	SET @Resultado = COALESCE( @Resultado, '');

	RETURN @Resultado
END
GO

CREATE FUNCTION SP_GET_USUARIO_SUPERVISOR( @TB_CODIGO Codigo , @Nivel Formula ) RETURNS Usuario
AS
BEGIN
	DECLARE @US_CODIGO Usuario

	SELECT  @US_CODIGO =
		CASE  @Nivel
			WHEN '1' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL1 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '2' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL2 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '3' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL3 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '4' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL4 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '5' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL5 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '6' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL6 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '7' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL7 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '8' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL8 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '9' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL9 WHERE TB_CODIGO = @TB_CODIGO)
		END

	RETURN COALESCE(@US_CODIGO, 0)
END
GO

CREATE FUNCTION SP_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO( @CB_CODIGO NumeroEmpleado )
RETURNS @SupervisorEnrolado 
TABLE (
 CB_NIVEL_COD CHAR(10),
 US_JEFE SMALLINT,
 USA_ENROL TINYINT   
)
AS
BEGIN 
	DECLARE @NivelSupervisores Formula	
	DECLARE @CB_NIVELSup Codigo     		     		
     	DECLARE @US_JEFE Usuario
     	DECLARE @USA_ENROL SMALLINT
     	SET @CB_NIVELSup = '';
     	SET @US_JEFE = 0;

	IF DBO.SP_TIENE_ENROLAMIENTO_SUPERVISORES()='S'
		SET @USA_ENROL = 1
	ELSE
		SET @USA_ENROL = 0 
	
	IF  @USA_ENROL = 1 
	BEGIN 	
		SET @NivelSupervisores =  DBO.SP_GET_NIVEL_SUPERVISORES()
		
		IF  @NivelSupervisores <> ''
		BEGIN      		     		
				SELECT  @CB_NIVELSup = 
			CASE  @NivelSupervisores 
				WHEN '1' THEN CB_NIVEL1   
				WHEN '2' THEN CB_NIVEL2   			
				WHEN '3' THEN CB_NIVEL3   
				WHEN '4' THEN CB_NIVEL4   
				WHEN '5' THEN CB_NIVEL5  
				WHEN '6' THEN CB_NIVEL6   
				WHEN '7' THEN CB_NIVEL7  
				WHEN '8' THEN CB_NIVEL8  
				WHEN '9' THEN CB_NIVEL9  			
			END
			FROM COLABORA WHERE cb_codigo = @CB_CODIGO
		END
	END
	
	SET @CB_NIVELSup = COALESCE(@CB_NIVELSup, '')
	
	IF (@CB_NIVELSup <> '')
	 SET @US_JEFE = dbo.SP_GET_USUARIO_SUPERVISOR( @CB_NIVELSup, @NivelSupervisores )
	 
	SET @US_JEFE = COALESCE(@US_JEFE, 0)
	
	
	INSERT INTO @SupervisorEnrolado
	        ( CB_NIVEL_COD ,
	          US_JEFE, 
	          USA_ENROL
	        )
	VALUES  ( @CB_NIVELSup,
	          @US_JEFE,
	          @USA_ENROL  
	        )
	       
	RETURN 

END
GO

CREATE FUNCTION DBO.GETVACAPROPORCIONAL
(
	@CB_CODIGO INT,
	@FECHAREGRESO DATETIME
)
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE
	@DANTIG DATETIME,
	@DULTIMOCIERRE DATETIME,
	@STABLA VARCHAR(1),
	@FECHAREF DATETIME,
	@DANIV DATETIME,
	@IANTIG INT,
	@RESULT FLOAT,
	@RANTIGULTIMO FLOAT,
	@RANTIGPROXIMO FLOAT,
	@LANIVULTIMO CHAR(1),
	@LANIVPROXIMO CHAR(1),
	@GLOBAL201 VARCHAR(8),
	@GLOBAL104 VARCHAR(8)


		SELECT @GLOBAL201 = ISNULL( GL_FORMULA, 'N' )
		FROM GLOBAL
		WHERE GL_CODIGO = 201

			SELECT @GLOBAL104 = ISNULL( GL_FORMULA, '' )
			FROM GLOBAL
			WHERE GL_CODIGO=104

			/* SE OBTIENE LA INFORMACI�N DEL EMPLEADO */
			SELECT  @DANTIG=C.CB_FEC_ING,
					@FECHAREF=C.CB_FEC_BAJ,
					@DULTIMOCIERRE=C.CB_DER_FEC,
					@STABLA = C.CB_TABLASS
			FROM COLABORA C
			WHERE( C.CB_CODIGO = @CB_CODIGO )

			/* SE OBTIENE LA FECHA DE ANIVERSARIO DEL EMPLEADO */
			SELECT @LANIVPROXIMO = DBO.ESANIVERSARIO( @DANTIG , @FECHAREGRESO )
			SELECT @RANTIGPROXIMO = DBO.YEARSCOMPLETOS( @DANTIG , @FECHAREGRESO )

			IF(ISNULL( @STABLA, '' ) <> '' )
			BEGIN
				SELECT @RESULT = DBO.GETVACAPROP( @STABLA, ABS(ROUND(@RANTIGPROXIMO,0)) )
				SELECT @RESULT = @RESULT * 0.01
			END
			ELSE
			BEGIN
					IF ( ISNULL( @GLOBAL104, '' ) <> '' )
					BEGIN
						SELECT @RESULT = DBO.GETVACAPROP( @GLOBAL104, ABS(ROUND(@RANTIGPROXIMO,0)) )
						SELECT @RESULT = @RESULT * 0.01
					END
					ELSE
					BEGIN
						SELECT @RESULT = 0
					END
			END
	RETURN  @RESULT
END
GO

CREATE FUNCTION DBO.CACHEPRESTACIONESPROP
(   @STABLAS VARCHAR(30) )
RETURNS @FCACHETABLA TABLE (
    ANIO NUMERIC(15,2),
    DIAS NUMERIC(15,2) )
AS
BEGIN

	DECLARE
	@K_MAX_YEARS INT,
	@I INT,
	@J INT,
	@DIASAUX FLOAT,
	@DIASANT FLOAT,
	@DIAS FLOAT

	SELECT @K_MAX_YEARS = 99, @I = 0 ,@DIAS =0, @DIASANT=0, @DIASAUX=0 , @J = 1

	/* SE LLENA LA TABLA CON LOS DIAS DISPONIBLES POR A�O PARA UN EMPLEADO*/
	WHILE( @I < @K_MAX_YEARS)
	BEGIN
		SELECT @DIASAUX = ISNULL(PT_PRIMAVA, 0 )
						FROM   PRESTACI
						WHERE  TB_CODIGO = @STABLAS
							AND PT_YEAR = @I
						ORDER BY PT_YEAR

		IF( @DIASAUX = 0 )
		BEGIN

			INSERT INTO @FCACHETABLA VALUES( @I , 0 )

		END
		ELSE
		BEGIN
			SELECT @DIAS = @DIASAUX

			INSERT INTO @FCACHETABLA VALUES( @I , @DIAS )

			UPDATE @FCACHETABLA
			SET DIAS= @DIAS
			WHERE ANIO >0 AND ANIO<@I AND DIAS=0

		END

		SELECT @I= @I+1,@DIASAUX =0
	END

	/* SE ACTUALIZAN LOS REGISTROS QUE TIENEN 0 DIAS */
	UPDATE @FCACHETABLA
	SET DIAS= @DIAS
	WHERE ANIO > 0
	AND ANIO < @K_MAX_YEARS
	AND DIAS = 0

	RETURN
END
GO

CREATE FUNCTION DBO.GETVACAPROP
(   @STABLAS VARCHAR(30), @IYEAR INT )
RETURNS NUMERIC(15,2)
AS
BEGIN

	DECLARE
	@DIAS NUMERIC(15,2),
	@K_MAX_YEARS INT

	SELECT  @K_MAX_YEARS =99

	IF ( @IYEAR < 1 )
		SELECT @IYEAR = 1;
	IF ( @IYEAR > @K_MAX_YEARS )
		SELECT @IYEAR = @K_MAX_YEARS

	/* SE OBTIENE LA INFORMACI�N DE LA TABLA */
	SELECT @DIAS = DIAS FROM DBO.CACHEPRESTACIONESPROP(@STABLAS)WHERE ANIO=@IYEAR

	RETURN @DIAS
END
GO

CREATE FUNCTION DBO.YEARSCOMPLETOS
(   @DANTIG DATETIME,
	@DREFERENCIA DATETIME )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE
	@IDAYINI INT, @IMONTHINI INT, @IYEARINI INT,
	@IDAYFIN INT, @IMONTHFIN INT, @IYEARFIN INT,
	@RANTIGULTIMO NUMERIC(15,2)

	IF ( @DANTIG < @DREFERENCIA )
	BEGIN

		/* ESTA FUNCION DESMENUSA LA FECHA Y OBTIENE EL A�O, MES Y DIA*/
		SELECT 	@IYEARINI 	= YEAR(@DANTIG),
				@IMONTHINI 	= MONTH(@DANTIG),
				@IDAYINI 	= DAY(@DANTIG),
				@IYEARFIN 	= YEAR(@DREFERENCIA),
				@IMONTHFIN 	= MONTH(@DREFERENCIA),
				@IDAYFIN 	= DAY(@DREFERENCIA)


		IF ( @IMONTHINI = @IMONTHFIN ) AND ( @IDAYINI = @IDAYFIN )    
			SELECT @RANTIGULTIMO= ABS(@IYEARFIN - @IYEARINI)
		ELSE  
			SELECT  @RANTIGULTIMO= (DATEDIFF(DAY, @DANTIG ,@DREFERENCIA ) / 365.25)
	END
	ELSE
	BEGIN
		SELECT  @RANTIGULTIMO = 0
	END

	RETURN @RANTIGULTIMO
END
GO

CREATE  FUNCTION DBO.ESANIVERSARIO
(   @DANTIG DATETIME, @DREFERENCIA DATETIME )
RETURNS CHAR(1)
AS
BEGIN
	DECLARE
	@IDAYINI INT, @IMONTHINI INT, @IYEARINI INT,
	@IDAYFIN INT, @IMONTHFIN INT, @IYEARFIN INT,
	@LANIVERSARIO INT, @ESANIV CHAR(1)

	SELECT @LANIVERSARIO= 0

	IF ( @DANTIG < @DREFERENCIA )
	BEGIN

		/* ESTA FUNCION DESMENUSA LA FECHA Y OBTIENE EL A�O, MES Y DIA*/
		SELECT 	@IYEARINI 	= YEAR(@DANTIG),
				@IMONTHINI 	= MONTH(@DANTIG),
				@IDAYINI 	= DAY(@DANTIG),
				@IYEARFIN 	= YEAR(@DREFERENCIA),
				@IMONTHFIN 	= MONTH(@DREFERENCIA),
				@IDAYFIN 	= DAY(@DREFERENCIA)

		IF( @IMONTHINI = @IMONTHFIN ) AND ( @IDAYINI = @IDAYFIN )
		BEGIN
			SELECT @ESANIV= 'S'
		END
		ELSE
		BEGIN
			SELECT @ESANIV= 'N'
		END

	END
	ELSE
	BEGIN
			SELECT @ESANIV= 'S'
	END

	RETURN @ESANIV
END
GO

CREATE FUNCTION DBO.PUEDE_CAMBIAR_TARJETA( @FECHAINICIAL DATETIME, @FECHAFINAL DATETIME, @EMPLEADO INTEGER )
RETURNS CHAR( 100 )
AS
BEGIN
  DECLARE @MENSAJE CHAR(100);
  DECLARE @K_GLOBAL_BLOQUEO_X_STATUS CHAR(1);
  DECLARE @K_LIMITE_MODIFICAR_ASISTENCIA INTEGER;
  DECLARE @K_LIMITE_MODIFICAR_ASISTENCIA_DESC CHAR(30);
  DECLARE @ESTATUSACTUAL SMALLINT;
  DECLARE @LIMITE DATETIME;

  SET @MENSAJE = 'S';

  IF( @MENSAJE = 'S' )
  BEGIN
    SET @K_GLOBAL_BLOQUEO_X_STATUS = ( SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 187 );

	IF @K_GLOBAL_BLOQUEO_X_STATUS = 'S'
	BEGIN
	  IF( @EMPLEADO = 0 )
        SET @MENSAJE = 'Sin Empleado Definido';
      ELSE
      BEGIN
        IF( @FECHAINICIAL = @FECHAFINAL )
        BEGIN
          SET @K_LIMITE_MODIFICAR_ASISTENCIA = ( SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 132 );

          IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 0 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Nueva';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 1 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Pre-N�mina Parcial';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 2 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Sin Calcular';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 3 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Calculada Parcial';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 4 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Calculada Total';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 5 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Afectada Parcial';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 6 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Afectada Total';
          ELSE
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Nomina no encontrada';

		  SET @MENSAJE = 'No se Puede Modificar Tarjetas de Nominas con Status Mayor a ' + @K_LIMITE_MODIFICAR_ASISTENCIA_DESC;
		  SET @ESTATUSACTUAL = DBO.SP_STATUS_TARJETA( @EMPLEADO, @FECHAINICIAL );
          IF( @ESTATUSACTUAL <= @K_LIMITE_MODIFICAR_ASISTENCIA )
          BEGIN
	        SET @MENSAJE = '';
          END;
        END
        ELSE
          SET @MENSAJE = 'Bloqueo por Estatus de N�mina No se Puede Validar por Rango de Fechas';
      END;
	END
	ELSE
	BEGIN
	  SET @MENSAJE = '';
	  SET @LIMITE = ( SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 182 );
      IF NOT( ( @FECHAINICIAL > @LIMITE )AND( @FECHAFINAL > @LIMITE ) )
        SET @MENSAJE = 'No se Puede Cambiar Tarjetas Anteriores A la Fecha L�mite '+ CONVERT(VARCHAR, @LIMITE,103);
	END;
  END;

  RETURN @MENSAJE;
END
GO

CREATE FUNCTION GetDerechoGozo( @Empleado NumeroEmpleado , @Anio Dias ,@FechaFinAniv Fecha,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	    Numeric(15,2),
	Pago	    Numeric(15,2),
    Prima		Numeric(15,2) )
AS
BEGIN
	 declare @FechaCierre Fecha;
	 declare @GozoCierre DiasFrac;
	 declare @PagoCierre DiasFrac;
	 declare @PrimaCierre DiasFrac;
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DGozoPend DiasFrac;
	 declare @DPagoPend DiasFrac;
	 declare @DPrimaPend DiasFrac;
	 declare @Antiguedad Fecha;
	 declare @DiasTabla Dias;
	 declare @DiasTablaPrima Dias;

 
	 set @DGozo = 0;
	 set @DPago = 0;
	 set @DPrima = 0;

	 set @DGozoPend = 0;
	 set @DPagoPend = 0;
	 set @DPrimaPend = 0;

     select @Antiguedad = CB_FEC_ANT from COLABORA where CB_CODIGO = @Empleado;
     
	 
	 if ( Exists(select VA_FEC_INI from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad  ) )
	 begin
          
		  SELECT @DGozo = Coalesce(Sum(VA_D_GOZO),0),@DPago = Coalesce(Sum(VA_D_PAGO),0),@DPrima = Coalesce(Sum(VA_D_PRIMA),0) from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad;	
		  select top 1 @FechaCierre = VA_FEC_INI,@GozoCierre = VA_D_GOZO, @PagoCierre = VA_D_PAGO,@PrimaCierre = VA_D_PRIMA from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad order by VA_FEC_INI desc;  
		  if (@FechaCierre != @FechaFinAniv)
		  begin
		   	   if ((( DateDiff ( Day,@FechaCierre,@FechaFinAniv ))/365.00 ) < 1 )
			   begin
			    	set @FechaCierre = DateAdd(Day,1,@FechaCierre);
			   end
			   select Top 1 @DiasTablaPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTabla = PT_DIAS_VA  from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC

			   set @DGozoPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTabla); 	
			   set @DGozo = @GozoCierre + @DGozoPend;	 
	
			   set @DPagoPend = @DGozoPend; 	
			   set @DPago = @PagoCierre + @DPagoPend;	 
	
			   set @DPrimaPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTablaPrima);
			   set @DPrima = @PrimaCierre + @DPrimaPend;
		  end
	 end
     else	
     begin	 
		  select top 1 @DPrima =  ( PT_PRIMAVA / 100 )* PT_DIAS_VA,@DGozo = PT_DIAS_VA,@DPago = PT_DIAS_VA from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC;
	     
     end;		   
	 insert into @Tabla (Gozo,Pago,Prima)
	 values (@DGozo,@DPago,@DPrima);
		
	 RETURN
END

GO 

CREATE FUNCTION Get_Proporcional( @FechaCierre Fecha,@Antig Fecha,@Anio Dias,@Empleado NumeroEmpleado,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	Numeric(15,2),
	Pago	Numeric(15,2),
	Prima	Numeric(15,2)
   )
as
begin
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DCierre Dias;
	 declare @FechaFinAniv Fecha;
	 declare @DiasTb Dias;
	 declare @DiasTbPrima DiasFrac;
	 declare @DiasDif Dias;
	 
     set @FechaFinAniv = DateAdd(Year,@Anio,@Antig);
     
	 if ( ( ( Month(@FechaCierre) = Month(@Antig) ) and ( Day(@FechaCierre) = Day(@Antig) ) ) or ( @FechaCierre is null ) )
     begin
		  select Top 1 @DPago = PT_DIAS_VA ,@DGozo = PT_DIAS_VA, @DPrima = ( ( PT_PRIMAVA / 100 )* PT_DIAS_VA )
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC	
	 end
	 else
	 begin
		  select Top 1 @DiasTbPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTb = PT_DIAS_VA  
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC 
    
		  set @DiasDif = DateDiff(Day,DateAdd(Day,1,@FechaCierre),@FechaFinAniv);
		  set @DGozo = ( @DiasDif / 365.00 )*@DiasTb;
		  set @DPago = @DGozo;
		  
		  set @DPrima = ( @DiasDif / 365.00 )*@DiasTbPrima;

	 end 
	 insert into @Tabla
	 ( Gozo,Pago,Prima)values( @DGozo,@DPago,@DPrima);
	return	
end

GO 

CREATE FUNCTION AniosVacCompletos( @FechaInicial Fecha,@FechaFinal Fecha )
RETURNS  integer
as
begin
declare @Anios integer;
	 if ( Month(@FechaInicial) = Month(@FechaFinal) and Day(@FechaInicial) = Day(@FechaFinal) )
	 begin
		  Set @Anios = DateDiff(Year,@FechaInicial,@FechaFinal)
	 end
	 else
	 begin
		  Set @Anios  =  (DateDiff( day, @FechaInicial,@FechaFinal ) + 1 )/365.25;
	 end
	 return @Anios;
end

GO 

CREATE FUNCTION GET_SALDOS_VACACION( @Employee int)
RETURNS @Tabla	TABLE (
	VS_ANIV		int,
	CB_CODIGO	int,
	VS_D_GOZO	Numeric(15,2),
	VS_GOZO		Numeric(15,2),
	VS_S_GOZO	Numeric(15,2),
	VS_D_PAGO	Numeric(15,2),
	VS_PAGO		Numeric(15,2),
	VS_S_PAGO	Numeric(15,2),
	VS_D_PRIMA  Numeric(15,2),
	VS_PRIMA	Numeric(15,2),
	VS_S_PRIMA	Numeric(15,2),
	VS_FEC_VEN	DateTime,
	VS_CB_SAL	Numeric(15,2),CIERRE Numeric(15,2) )
AS
BEGIN
	declare @Empleado NumeroEmpleado;
	declare @Anios DiasFrac;
	declare @Anio Dias;

	declare @DerechoGozo DiasFrac;
	declare @GozadosDist DiasFrac;
	declare @GozadosRest DiasFrac;

    declare @GozoCierre DiasFrac;

	declare @DerechoPago DiasFrac;
	declare @PagadosDist DiasFrac;
	declare @PagadosRest DiasFrac;

	declare @PagoCierre DiasFrac;

	declare @DerechoPV DiasFrac;
	declare @PVDist DiasFrac;
	declare @PVRest DiasFrac;

	declare @PrimaCierre DiasFrac;
	declare @FechaCierre Fecha;

	declare @Antig Fecha;
	declare @AniosTotales Fecha;

	declare @Hoy Fecha;
	declare @YearCierre Dias;

	declare @DerechoGozoSum DiasFrac;
	declare @DerechoPagoSum DiasFrac;
	declare @DerechoPrimaSum DiasFrac;

    declare @FechaVencimiento Fecha;
    declare @Salario DiasFrac;
    declare @MesesGlobal Dias;

    declare @FechaFinAniv Fecha;
	declare @UltimaTabla Codigo;
	declare @FechaSalario Fecha;
    declare @FechaTabla Fecha;
    declare @Activo Booleano;
	declare @FechaBaja Fecha;


    if( @Employee > 0 )
    begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA where CB_CODIGO = @Employee
    end
    else
	begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA
    end;

	select @MesesGlobal = GL_FORMULA from GLOBAL where GL_CODIGO = 297;

	open EmpleadosVac;
     fetch NEXT from EmpleadosVac
     into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     while ( @@Fetch_Status = 0 )
     begin
		  
		  select @GozadosRest = Coalesce(SUM(VA_GOZO),0),@PagadosRest = Coalesce(SUM(VA_PAGO),0),@PVRest = Coalesce(SUM(VA_P_PRIMA),0)
							from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 1 and VA_FEC_INI >= @Antig;
		  
		  set @Anio = 1;
		  set @Anios = 0;
		  set @DerechoGozoSum = 0;
		  set @DerechoPagoSum = 0;
		  set @DerechoPrimaSum = 0;

		  Select @Hoy = gd FROM vCurrentDateTime;
		  if ( @Activo = 'N' )
		  begin
			   Set @Hoy = Coalesce(@FechaBaja,@Hoy);
		  end	
		  set @AniosTotales = dbo.AniosVacCompletos(@Antig,@Hoy );	
		  set @FechaCierre = @Antig;
		  
		  if ( ( @Antig is null )or (@Antig = '12/30/1899' ))
		  begin
		 	   select @Antig = CB_FEC_ING FROM COLABORA WHERE CB_CODIGO = @Empleado;  
	      end
			 
		  select Top 1 @FechaTabla = CB_FECHA FROM KARDEX where CB_CODIGO = @Empleado and CB_TIPO = 'PRESTA' order by CB_FECHA desc; 
		  
		  
 	      declare CierresEmp CURSOR for
		    select VA_FEC_INI,VA_D_GOZO,VA_D_PAGO,VA_D_PRIMA,VA_YEAR from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_FEC_INI >= @Antig order by VA_FEC_INI asc
		  
          open CierresEmp;
		  fetch NEXT from CierresEmp
		  into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  while ( @@Fetch_Status = 0 )
		  begin 
			   	Set @Anios = dbo.AniosVacCompletos(@Antig,@FechaCierre );
			    			   
		       if ( @Anios < @Anio )
		       begin
					set @DerechoGozoSum = @GozoCierre + @DerechoGozoSum;
					set @DerechoPagoSum = @PagoCierre + @DerechoPagoSum;
					set @DerechoPrimaSum = @PrimaCierre + @DerechoPrimaSum;
		       end
		       
		       While ( @Anios >= @Anio )
			   begin
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado; 
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) ) 
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado) ;
					end
					
					if( @DerechoGozoSum > 0)
					begin
						 set @GozoCierre = @GozoCierre + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end
					
					if( @DerechoPagoSum > 0)
					begin
						 set @PagoCierre = @PagoCierre + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end
					
					if( @DerechoPrimaSum > 0)
					begin
						 set @PrimaCierre = @PrimaCierre + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					
					
					
					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima 
								from dbo.GetDerechoGozo(@Empleado,@Anio,@FechaFinAniv,@UltimaTabla);
										
					
					
					if ( @DerechoGozo <= @GozoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoGozo = @GozoCierre;
							  set @GozoCierre = 0;
						 end
						 else
						 begin
							  set @GozoCierre = @GozoCierre - @DerechoGozo;
						 end
					end
					else
					begin  
						 set @DerechoGozo = @GozoCierre;
						 set @GozoCierre = 0;
				    end
					
					if ( @DerechoPago <= @PagoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPago = @PagoCierre;
							  set @PagoCierre = 0;
						 end
						 else
						 begin
							  set @PagoCierre = @PagoCierre - @DerechoPago;
						 end
					end
					else
					begin
						 set @DerechoPago = @PagoCierre;
						 set @PagoCierre = 0;
				    end
				    
					if ( @DerechoPV <= @PrimaCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPV = @PrimaCierre;
							  set @PrimaCierre = 0;
						 end
						 else
						 begin
							  set @PrimaCierre = @PrimaCierre - @DerechoPV;
						 end
					end
					else
					begin  
						 set @DerechoPV = @PrimaCierre;
						 set @PrimaCierre = 0;
				    end

					
					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end
					
					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end
				    
					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	

					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end

                    
					insert into @Tabla
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario		 
					 );
			 	
					 set @Anio = @Anio + 1;
			   end
			   fetch NEXT from CierresEmp
               into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  end		
		  close CierresEmp;
	      deallocate CierresEmp;
		  
		  if ( @Anios != @AniosTotales )
		  begin
			   While (@AniosTotales >= @Anio)
			   begin
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado; 
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) )
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado);
					end
					
					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima
							from dbo.Get_Proporcional(@FechaCierre,@Antig,@Anio,@Empleado,@UltimaTabla);

					
					set @FechaCierre = @FechaFinAniv;
					
					
					if( @DerechoGozoSum > 0)
					begin
						 set @DerechoGozo = @DerechoGozo + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end
					
					if( @DerechoPagoSum > 0)
					begin
						 set @DerechoPago = @DerechoPago + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end
					
					if( @DerechoPrimaSum > 0)
					begin
						 set @DerechoPV = @DerechoPV + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					

							
					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end	
					
					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end	
				    
					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	
					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end
					
					
					
					insert into @Tabla 
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario 
					 );

					 set @Anio = @Anio + 1;
			   end				
		  end
		 	
		  fetch NEXT from EmpleadosVac
          into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     end;
	 close EmpleadosVac;
	 deallocate EmpleadosVac;
	 	
	return 
END

GO

CREATE FUNCTION SP_GET_NIVEL_COSTEO() RETURNS FolioChico
AS
BEGIN
	DECLARE @GLOBAL_Nivel_de_Costeo FolioChico
	DECLARE @ResGlobal Formula
	DECLARE @Resultado FolioChico

	SET @GLOBAL_Nivel_de_Costeo = 294

	SELECT @ResGlobal = RTrim(LTrim(GL_FORMULA))  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_Nivel_de_Costeo

	/* Es necesario ajustarlo para ACS*/
	if  @ResGlobal in ( '1','2','3','4','5','6','7','8','9' )
		select @Resultado = convert( smallint,  @ResGlobal )
	else
		select @Resultado =0;

	RETURN @Resultado
END
GO

CREATE FUNCTION SP_AUSENCIA_COSTEO( @Empleado NumeroEmpleado, @Fecha Fecha)
RETURNS Codigo
AS
BEGIN
	declare @NivelCosteo FolioChico
	declare @Resultado Codigo

  select   @NivelCosteo = dbo.SP_GET_NIVEL_COSTEO();

	if @NivelCosteo = 0
		select @Resultado = ''
	else
		select @Resultado =
					CASE  @NivelCosteo
						WHEN 1 THEN CB_NIVEL1
						WHEN 2 THEN CB_NIVEL2
						WHEN 3 THEN CB_NIVEL3
						WHEN 4 THEN CB_NIVEL4
						WHEN 5 THEN CB_NIVEL5
						WHEN 6 THEN CB_NIVEL6
						WHEN 7 THEN CB_NIVEL7
						WHEN 8 THEN CB_NIVEL8
						WHEN 9 THEN CB_NIVEL9
					/* ACS
						WHEN 10 THEN CB_NIVEL10
						WHEN 11  THEN CB_NIVEL11
						WHEN 12 THEN CB_NIVEL12
					*/
					end
		from AUSENCIA
		where CB_CODIGO = @Empleado
		and AU_FECHA = @Fecha

	RETURN  @Resultado
END

GO

CREATE FUNCTION SP_NOMINA_COSTEO( @Empleado NumeroEmpleado,
								  @Year Anio,
								  @Tipo NominaTipo,
								  @Numero NominaNumero )
RETURNS Codigo
AS
BEGIN
	declare @NivelCosteo FolioChico
	declare @Resultado Codigo

  select   @NivelCosteo = dbo.SP_GET_NIVEL_COSTEO();

	if @NivelCosteo = 0
		select @Resultado = ''
	else
		select @Resultado =
					CASE  @NivelCosteo
						WHEN 1 THEN CB_NIVEL1
						WHEN 2 THEN CB_NIVEL2
						WHEN 3 THEN CB_NIVEL3
						WHEN 4 THEN CB_NIVEL4
						WHEN 5 THEN CB_NIVEL5
						WHEN 6 THEN CB_NIVEL6
						WHEN 7 THEN CB_NIVEL7
						WHEN 8 THEN CB_NIVEL8
						WHEN 9 THEN CB_NIVEL9
					/* ACS
						WHEN 10 THEN CB_NIVEL10
						WHEN 11  THEN CB_NIVEL11
						WHEN 12 THEN CB_NIVEL12
					*/
					end
		from NOMINA
		where CB_CODIGO = @Empleado
		and PE_YEAR = @Year
		and PE_TIPO = @Tipo
		and PE_NUMERO = @Numero
	RETURN  @Resultado
END
GO

CREATE FUNCTION FN_SPLIT(@String Formula, @Delimiter Codigo1)
returns @temptable TABLE (items varchar(255))
as
begin
    declare @idx int
    declare @slice varchar(255)

    select @idx = 1
        if len(@String)<1 or @String is null  return

    while @idx!= 0
    begin
        set @idx = charindex(@Delimiter,@String)
        if @idx!=0
            set @slice = left(@String,@idx - 1)
        else
            set @slice = @String

        if(len(@slice)>0)
            insert into @temptable(Items) values(@slice)

        set @String = right(@String,len(@String) - @idx)
        if len(@String) = 0 break
    end
return
end
GO

CREATE FUNCTION SP_GET_DB_FULLNAME()
returns Varchar(512)
as
begin
	declare @nameServer varchar(200)
	declare @nameMachine varchar(100)
	declare @kInstance varchar(100)
	declare @constrServer varchar(200)
	declare @retValue varchar(512)

	select @nameServer = convert(nvarchar(128), serverproperty('servername'));
	select @nameMachine = convert(nvarchar(128), serverproperty('machinename'));
	if len(@nameServer) = len(@nameMachine)
	select @kInstance = ''
	else
	select @kInstance = right(@nameServer, len(@nameServer) - (len(@nameMachine)+1));

	if @kInstance='' select @constrServer = @nameMachine
	else select @constrServer = @nameMachine + '\' + @kInstance;

	set @retValue = @constrServer + '.' + db_name();

	set @retValue = UPPER( RTRIM( @retValue ) ) ;

	return @retValue
end;
GO

create function SP_GET_EMPRESAS()
returns TABLE
as
	return
  select CM_CODIGO from V_COMPANY where UPPER(CM_DATOS) = DBO.SP_GET_DB_FULLNAME()
GO

CREATE FUNCTION FN_WFGetVacFechasPeriodos (
@CB_CODIGO NumeroEmpleado,
@FechaVacIni Fecha,
@FechaVacReg Fecha,
@YEAR Anio,
@TPO_NOMINA NominaTipo
)
  RETURNS @TABLE_RESULT
  TABLE  (
		A_FECHA Datetime, PE_YEAR int, PE_TIPO int, PE_NUMERO int
)
AS
BEGIN

declare @FechaVacFin Fecha
declare @currDate	 Fecha


set @currDate = @FechaVacIni
set @FechaVacFin = @FechaVacReg -1


WHILE @currDate <= @FechaVacFin
BEGIN

	set @TPO_NOMINA = dbo.SP_KARDEX_CB_NOMINA( @currDate, @CB_CODIGO )

    INSERT INTO @TABLE_RESULT (A_FECHA, PE_YEAR, PE_TIPO, PE_NUMERO)
	SELECT TOP 1   @currDate, PE_YEAR, PE_TIPO, PE_NUMERO  FROM PERIODO where
			( PE_YEAR >= @YEAR )
			AND ( PE_TIPO = @TPO_NOMINA )
			AND ( PE_NUMERO < 200 )
			AND ( PE_STATUS < 5 )
			AND ( PE_CAL = 'S' )
		    AND ( @currDate between PE_ASI_INI and PE_ASI_FIN )
	order by PE_YEAR, PE_NUMERO

    SET @currDate = @currDate + 1
END

return

end
GO

CREATE FUNCTION FN_WFGetVacacionesPeriodos
(
@CB_CODIGO NumeroEmpleado,
@FechaVacIni Fecha,
@FechaVacReg Fecha,
@YEAR Anio,
@TPO_NOMINA NominaTipo,
@NUM_NOMINA NominaNumero
)
  RETURNS @TABLE_RESULT
  TABLE  (
		Orden int identity(1,1), PE_YEAR int, PE_TIPO int, PE_NUMERO int, VA_FEC_INI Datetime, VA_FEC_FIN Datetime, DIAS int
)
as
begin

insert into @TABLE_RESULT (PE_YEAR, PE_TIPO, PE_NUMERO , VA_FEC_INI , VA_FEC_FIN, DIAS )
select PE_YEAR, PE_TIPO, PE_NUMERO, MIN(A_FECHA) AS VA_FEC_INI, MAX(A_FECHA)+1 as VA_FEC_FIN,  count(*) as DIAS  FROM
dbo.FN_WFGetVacFechasPeriodos( @CB_CODIGO, @FechaVacIni, @FechaVacReg, @YEAR, @TPO_NOMINA )
group by PE_YEAR, PE_TIPO, PE_NUMERO

/*pendiente actualizar el ultimo registro si es que faltaron periodos*/
declare @diasEsperados int
declare @diasACrear int

set @diasEsperados =  datediff( day,  @FechaVacIni ,@FechaVacReg )
select @diasACrear = sum(DIAS) from @TABLE_RESULT

declare @mismaNomina booleano

set @mismaNomina = 'N'
if ( select count(1) from @TABLE_RESULT where
	@YEAR = PE_YEAR and
	@TPO_NOMINA = PE_TIPO and
	@NUM_NOMINA = PE_NUMERO and Orden=1) > 0
	set @mismaNomina = 'S'

if ( @diasEsperados <> @diasACrear ) or ( @mismaNomina = 'N' )
begin
	delete from @TABLE_RESULT

	insert into @TABLE_RESULT (PE_YEAR, PE_TIPO, PE_NUMERO , VA_FEC_INI , VA_FEC_FIN, DIAS ) VALUES
		(@YEAR, @TPO_NOMINA, @NUM_NOMINA, @FechaVacIni, @FechaVacReg, @diasEsperados )

end;

return

end

GO

CREATE FUNCTION SP_GET_METODO_PAGO_DEFAULT() RETURNS Status
AS
BEGIN
	DECLARE @GLOBAL_Nomina_MetodoPago FolioChico
	DECLARE @Resultado Formula
	DECLARE @ResultadoInt  Status 
	
	SET @GLOBAL_Nomina_MetodoPago = 310
		
	SELECT @Resultado = GL_FORMULA  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_Nomina_MetodoPago 
	
	SET @Resultado = COALESCE( @Resultado, '0')	
	set  @ResultadoInt = cast( @Resultado as smallint )

	RETURN @ResultadoInt 
END
GO

create function FN_GetFacturasPendientes( @Inicial Fecha , @Final Fecha ) 
returns table 
as
return ( 
select NO.NO_FACTURA from NOMINA NO 
join PERIODO PE on NO.PE_TIPO = PE.PE_TIPO and NO.PE_YEAR = PE.PE_YEAR and NO.PE_NUMERO = PE.PE_NUMERO
where PE.PE_FEC_PAG between @Inicial and @Final  and NO.NO_FACTURA > 0 and ( NO.NO_FACUUID = '' or NO.NO_FACUUID is null ) 
)
GO

create function FN_GetGlobalBooleano ( @GlobalNo int ) 
returns Booleano 
as 
begin
     return coalesce( ( select top 1 SUBSTRING(GL_FORMULA,1,1) from GLOBAL where GL_CODIGO = @GlobalNo ) , 'N' ) 
end 
go 

create function FN_GetGlobalInteger ( @GlobalNo int ) 
returns integer  
as 
begin
     declare @Number int 
	 declare @ValueStr Formula 
	 set @ValueStr =  coalesce( ( select top 1 GL_FORMULA  from GLOBAL where GL_CODIGO = @GlobalNo ) , '0' )
	 
	 if ISNUMERIC ( @ValueStr )> 0  
		return cast( @ValueStr as int ) 
	  
	return 0 
end 
go 

create function FN_NotificacionTimbrado_GetNominasPendientes(@CM_CODIGO CodigoEmpresa, @Fecha Fecha) 
returns @Periodos TABLE  (
	[Fecha de pago] datetime, 
	[A�o] varchar(50) default '', 	
	[Tipo] varchar(50) default '', 
	[Periodo] varchar(50) default '', 	
	[Estatus] varchar(50) default '', 	
	[Recibos sin timbrar] int
) 
as 
begin 

 if dbo.FN_GetGlobalBooleano( 314 )  = 'N' 
    return 

 declare @PE_USO int 
 
 if dbo.FN_GetGlobalBooleano( 325 )  = 'S' 
	set @PE_USO = 0 
 else
	set @PE_USO = -1 

 declare @FechaMin Fecha 
 declare @iDias int 

 set @iDias = dbo.FN_GetGlobalInteger( 315) 
 if  @iDias >0 
	set @FechaMin = dateadd( day , -1* @iDias, @Fecha ) 
 else
	set @FechaMin ='2013-01-01' 



 insert into @Periodos 
 (  [Fecha de pago], [A�o], [Tipo],[Periodo],  [Estatus], [Recibos sin timbrar] ) 
 select cast( PE_FEC_PAG as varchar(50) ), 
        LTRIM( str(PE_YEAR) ), 
		TPERIODO.TP_DESCRIP, 
		LTRIM( str(PE_NUMERO) ) , 
    PE_StatusDesc = case PE_TIMBRO   
						when 0 then 'Pendiente' 
						when 1 then 'Timbrada Parcial' 
						when 2 then 'Timbrada' 
					end , 
     ( select count(*) from NOMINA NO where NO.NO_TIMBRO = 0  and NO.PE_YEAR = PERIODO.PE_YEAR and NO.PE_TIPO = PERIODO.PE_TIPO and NO.PE_NUMERO = PERIODO.PE_NUMERO ) as PE_SinTimbrar
 from PERIODO 
 join TPERIODO on PERIODO.PE_TIPO = TPERIODO.TP_TIPO 
 where PE_YEAR >= 2014 and PERIODO.PE_TIMBRO in (0,1) and PERIODO.PE_STATUS > 5  and ( PERIODO.PE_USO = @PE_USO or @PE_USO = -1 )  and PERIODO.PE_FEC_PAG between @FechaMin and @Fecha

return 
end 
go

create function FN_Hour2DateTime(@Fecha Fecha, @Hora Hora )
returns Fecha 
as
begin 
	declare @FechaTmp Fecha
	declare @HoraTmp Hora
	declare @iHora smallint 
	declare @sHora varchar(2) 
	set @HoraTmp = coalesce( @Hora , '' ) 	
	
	if ( @Hora <> '') 
	begin  
		if ( @Hora >= '2400' ) 
		begin 
			set @Fecha = dateadd( day, 1, @Fecha ) 
			set @iHora = cast( SUBSTRING(@Hora, 1, 2)  as smallint ) - 24 
			set @sHora = ltrim( str( @iHora ) )
			set @FechaTmp =  cast ( (  @sHora + ':'+  SUBSTRING(@Hora, 3, 2) ) as datetime ) 	
			set @FechaTmp = dateadd(dd, datediff(dd,0, @Fecha), 0)  + @FechaTmp
		end 
		else
		begin 
			set @FechaTmp =  cast ( (  SUBSTRING(@Hora, 1, 2) + ':'+  SUBSTRING(@Hora, 3, 2) ) as datetime ) 	
			set @FechaTmp = dateadd(dd, datediff(dd,0, @Fecha), 0)  + @FechaTmp
		end 
	end 
	else
		set @FechaTmp =  null 		

	return @FechaTmp 
end 

go 

CREATE  FUNCTION FN_GeneraRitmo(@patron AS Varchar(255), @horario1 Codigo, @horario2 Codigo, @horario3 Codigo )
RETURNS
 @Result TABLE(
	        iDia int identity(0,1), 
			HO_CODIGO  varchar(20), 
			TipoDia		int			
			 )      
AS
BEGIN
	  DECLARE @ritmos as table (
	        i int identity(0,1), 
			Patron varchar(20), 			
			HO_CODIGO  varchar(20), 
			TipoDia		int,
			Dias		int			
			 )
	
      DECLARE @str VARCHAR(20)
      DECLARE @ind Int
	  DECLARE @StatusDia int 

	  DECLARE @auHabil int 
	  DECLARE @auSabado int 
	  DECLARE @auDescanso int 
	  set  @AuHabil = 0 
	  set  @AuSabado = 1 
	  set  @AuDescanso = 2 
	  
	  DECLARE @lUSaSabados bit 

	  if charindex('#', @patron ) > 0 
	  begin 
		set @lUSaSabados = 1
		set @patron = SUBSTRING(@patron, 2, LEN(@patron)) 
	  end
	  else
	    set @lUSaSabados= 0;



	  set @StatusDia = @AuHabil 

      IF(@patron is not null)
      BEGIN
            SET @ind = CharIndex(',',@patron)
            WHILE @ind > 0
            BEGIN
                  SET @str = SUBSTRING(@patron,1,@ind-1)
                  SET @patron = SUBSTRING(@patron,@ind+1,LEN(@patron)-@ind)
                  INSERT INTO @ritmos (Patron, TipoDia) values (@str, @StatusDia)
                  SET @ind = CharIndex(',',@patron)


				  SET @StatusDia = case 
									when @StatusDia = @AuHabil and @lUsaSabados = 1  then @AuSabado
									when @StatusDia = @AuHabil and @lUsaSabados = 0  then @AuDescanso
									when @StatusDia = @AuSabado then @AuDescanso 
									when @StatusDia = @AuDescanso then @AuHabil
								else 
									@AuHabil 
							end 
            END
            SET @str = @patron
            INSERT INTO @ritmos (Patron, TipoDia) values (@str, @StatusDia)
      END



	  
	update @ritmos  set Patron = case when charindex('%', Patron) > 0 then  SUBSTRING( Patron, 1,  charindex('%', Patron)-1) else Patron end 
	update @ritmos  set Ho_Codigo = SUBSTRING( Patron, charindex(':', Patron)+1,  len( Patron)- charindex(':', Patron)+1  )  where Patron like '%:%' 
	update @ritmos  set Ho_Codigo = case 
									when TipoDia = @auHabil then @Horario1 
									when TipoDia = @auSabado then @Horario2 
									when TipoDia = @auDescanso and @lUSaSabados = 1  then @Horario3
									when TipoDia = @auDescanso and @lUSaSabados = 0  then @Horario2
								   else 
								    @Horario1 
								  end	
					where Ho_Codigo = '' or Ho_Codigo is NULL  
	update @ritmos  set Dias = cast( case  when charindex(':', Patron) > 0  then SUBSTRING( Patron, 1, charindex(':', Patron)-1 ) 
								   else  Patron 
							 end  as int ) 



	declare @iRitmo int 
	declare @nRitmos int 
	declare @nDias  int 
	declare @jRitmo int 
	set @iRitmo = 0 
	select @nRitmos = count(*) from @Ritmos
	while @iRitmo < @nRitmos 
	begin 
		select @nDias = Dias from @Ritmos where i= @iRitmo 
		set @jRitmo = 0 
		while @jRitmo < @nDias 
		begin 
			insert into @Result ( HO_CODIGO , TipoDia ) 
			select HO_CODIGO, TipoDia from @Ritmos where i = @iRitmo 
			 
			set @jRitmo = @jRitmo +1 
		end
		
		set @iRitmo = @iRitmo +1 
	end 


      RETURN
END

go 

	
create function FN_GetFestivos( @TU_CODIGO Codigo, @Fecha Fecha) 
RETURNS @Festivos
  TABLE  (
	FE_MES integer, 
	FE_DIA integer, 
	FECHA datetime, 
	TU_CODIGO char(6)
		)	
as
begin 

insert into @Festivos ( FE_MES, FE_DIA, FECHA, TU_CODIGO ) 
select FE_MES, FE_DIA, FE_CAMBIO, TU_CODIGO  from FESTIVO where FE_TIPO = 1 and FE_YEAR = YEAR(@Fecha) and  TU_CODIGO =  @TU_CODIGO 

insert into @Festivos ( FE_MES, FE_DIA, FECHA, TU_CODIGO ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO, Fe.TU_CODIGO  from FESTIVO FE
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
where FE_TIPO = 0 and Fe.TU_CODIGO = @TU_CODIGO and FE_YEAR = YEAR( @Fecha ) and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA,TU_CODIGO )
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO, Fe.TU_CODIGO    from FESTIVO FE
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
	where FE_TIPO = 0 and Fe.TU_CODIGO = @TU_CODIGO  and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO   from FESTIVO FE 
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
where FE_TIPO = 1 and Fe.FE_CAMBIO =  @Fecha  and  Fe.TU_CODIGO = '!!!'  and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO   from FESTIVO FE
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
 where FE_TIPO = 0 and FE_YEAR = YEAR( @Fecha )  and  Fe.TU_CODIGO = '!!!' and ft.FE_DIA is null 

insert into @Festivos ( FE_MES, FE_DIA, FECHA ) 
select Fe.FE_MES, Fe.FE_DIA, Fe.FE_CAMBIO   from FESTIVO FE 
left outer join @Festivos ft  on  ft.FE_MES = FE.FE_MES and ft.FE_DIA = FE.FE_DIA
where FE_TIPO = 0  and  Fe.TU_CODIGO = '!!!' and ft.FE_DIA is null 


update @Festivos set TU_CODIGO = '!!!' where TU_CODIGO is null 

update @Festivos set FECHA =  DATEADD(year, YEAR(@Fecha)-1900, DATEADD(month, FE_MES-1, DATEADD(day, FE_DIA-1, 0))) where FECHA = '1899-12-30' 

return 

end 



GO
create function  FN_EsFalta( @CB_CODIGO NumeroEmpleado, @AU_FECHA Fecha, @TU_CODIGO Codigo, @HO_CODIGO Codigo, @AU_STATUS status, @AU_TIPODIA status ) 
returns Status 
as 
begin 

	if ( select count(*) from CHECADAS where CB_CODIGO = @CB_CODIGO and AU_FECHA = @AU_FECHA and CH_TIPO in ( 1,2)   ) > 0 
	begin 
		return 0 
	end 

	return 1

end 


GO 


CREATE function FN_GetNominasSinTimbrar (@Pe_year int, @Pe_mes int, @CM_NIVEL0 Formula )
returns integer
as
begin
declare @Total int 

  set @Total = 0 
  select 
	@Total = count(*)
  from NOMINA NO 
  join PERIODO PE on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO   
  where NO.NO_STATUS > 5 and NO.NO_TIMBRO < 2  
  and PE.PE_YEAR = @Pe_year and PE.PE_MES = @Pe_mes and PE.PE_STATUS > 5 
  and (  @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='') )
  return @Total 

end 


go 

CREATE FUNCTION SP_A2(
  					@Concepto Concepto,
  					@Mes SMALLINT,
					@Anio Anio,
					@Empleado NumeroEmpleado,
					@RazonSocial Codigo
			   )
RETURNS Pesos 
AS
BEGIN
	DECLARE @Resultado AS Pesos
	  
	select @Resultado =  
	SUM( 
	case ( @Mes ) 
		when 1 then AC_MES_01
		when 2 then AC_MES_02
		when 3 then AC_MES_03
		when 4 then AC_MES_04
		when 5 then AC_MES_05
		when 6 then AC_MES_06
		when 7 then AC_MES_07
		when 8 then AC_MES_08
		when 9 then AC_MES_09
		when 10 then AC_MES_10
		when 11 then AC_MES_11
		when 12 then AC_MES_12
		when 13 then AC_MES_13
		else 0 
	end
	)
	from ACUMULA_RS where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado ) and ( RS_CODIGO = @RazonSocial  or @RazonSocial = '' ) and ( CO_NUMERO = @Concepto )

	set @Resultado = coalesce( @Resultado, 0 ) 

	RETURN @Resultado
END

GO

CREATE FUNCTION SP_AS2(
					@Concepto Concepto,
    				@NumeroIni SMALLINT,
    				@Numerofin SMALLINT,
					@Anio Anio,
					@Empleado NumeroEmpleado,			
					@RazonSocial Codigo
				)
RETURNS Pesos
AS
BEGIN
	declare @Temporal Pesos; 
	declare @Resultado Pesos; 

	select @Resultado = 0;
	while ( @NumeroIni <= @NumeroFin )
	begin		
		select @Temporal = dbo.SP_A2 ( @Concepto, @NumeroIni, @Anio, @Empleado, @RazonSocial )	  
		  
		if ( @Temporal is NOT NULL )
			select @Resultado = @Resultado + @Temporal;
		select @NumeroIni = @NumeroIni + 1;
	end

	set @Resultado = coalesce( @Resultado, 0 ) 
	Return @Resultado;
END

GO

CREATE FUNCTION FN_RazonSocialEmpleado( @Empleado NumeroEmpleado )
RETURNS Codigo 
AS
BEGIN
	DECLARE @RazonSocial Codigo

	SELECT @RazonSocial = PATRON.RS_CODIGO FROM COLABORA 
	JOIN RPATRON PATRON ON COLABORA.CB_PATRON = PATRON.TB_CODIGO 
	WHERE COLABORA.CB_CODIGO = @Empleado  

	SET @RazonSocial = coalesce( @RazonSocial, '' ) 

	RETURN @RazonSocial 
END
GO

CREATE  FUNCTION FN_GET_AC_MES( @Mes Status ) 
returns varchar(10)
as
begin 
	return 'AC_MES_'+RIGHT('00'+CAST(@Mes AS VARCHAR(2)),2)	
end 

GO 

CREATE FUNCTION fnConvert_TitleCase (@InputString VARCHAR(4000) )
RETURNS VARCHAR(4000)
AS
BEGIN
DECLARE @Index INT
DECLARE @Char CHAR(1)
DECLARE @OutputString VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 2
SET @OutputString = STUFF(@OutputString, 1, 1,UPPER(SUBSTRING(@InputString,1,1)))

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char = SUBSTRING(@InputString, @Index, 1)
    IF @Char IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&','''','(')
    IF @Index + 1 <= LEN(@InputString)
BEGIN
    IF @Char != ''''
    OR
    UPPER(SUBSTRING(@InputString, @Index + 1, 1)) != 'S'
    SET @OutputString =
    STUFF(@OutputString, @Index + 1, 1,UPPER(SUBSTRING(@InputString, @Index + 1, 1)))
END
    SET @Index = @Index + 1
END

RETURN ISNULL(@OutputString,'')
END

GO

create function SP_DASHLET_GET_MES_DESC ( @iMes status ) 
returns Descripcion 
as
begin 
	declare @MesDesc Descripcion 
	select 
		@MesDesc = 
		case ( @iMes-1 ) 			
			when 0	then 'Enero'
			when 1 then 'Febrero' 
			when 2 then 'Marzo' 
			when 3 then 'Abril' 
			when 4 then 'Mayo' 
			when 5 then 'Junio' 
			when 6 then 'Julio' 
			when 7 then 'Agosto' 
			when 8 then 'Septiembre' 
			when 9 then 'Octubre' 
			when 10 then 'Noviembre' 
			when 11 then 'Diciembre' 
		else '' 
	end 

	return @MesDesc
end 

GO 

CREATE FUNCTION GET_PERIODO_NOMBRE (@PE_TIPO NominaTipo, @PE_NUMERO NominaNumero )
RETURNS Descripcion 
as
begin 
	declare @PeriodoNombre Descripcion 

	select @PeriodoNombre = TP_NOMBRE from TPERIODO  where TP_TIPO = @PE_TIPO  

	if ( @PE_NUMERO > 0 ) 
		set @PeriodoNombre = coalesce( @PeriodoNombre, '' ) + ' ' + cast( @PE_NUMERO as varchar(20)) 
	else
		set @PeriodoNombre = 'S/D' 

	return @PeriodoNombre 

end 

GO 

CREATE function Sesion_GetValues( @SesionID FolioGrande ) 
returns table 
as
return (  
select PE_TIPO, PE_YEAR, PE_NUMERO, PE_MES, FechaActiva, MesIni, MesFin, US_CODIGO, CM_CODIGO, CM_NIVEL0  from #COMPARTE..Sesion 
where SesionID = @SesionID 
) 
go 

create function SP_DASHLET_GET_INCIDENCIA_DESC( @Incidencia status) 
returns Descripcion 
as
begin 
	return (select case @Incidencia 
        when 2  then 'Vacaciones'
        when 3  then 'Incapacidad'         
        when 4  then 'Perm. C/Goce'  
        when 5  then 'Perm. S/Goce'  
        when 6  then 'FJ'  
        when 7  then 'Suspensi�n'  
        when 8  then 'Otros'  
    else 'Sin clasificar' 
	end 
	)
end 
GO 

create function FN_MASK_PORC( @valor Decimal(15,2) ) 
returns Varchar(10) 
as
begin 
	
	declare @valorStr varchar(10) 
	set @valorStr = '' 
	set @valorStr = convert( varchar, @valor, 1 ) 
	if ( @valor < 100.00 ) 
		set @valorStr= RIGHT('00000'+@valorStr, 5) 
	
	set @valorStr = @valorStr + '%' 

	return @valorStr 
end
GO

CREATE FUNCTION FONACOT_CEDULA
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
	(
    NO_FONACOT varchar(30),
    RFC varchar(30),
	NOMBRE VARCHAR(250),
	NO_CREDITO varchar (8),
	RETENCION_MENSUAL numeric(15,2),
	CLAVE_EMPLEADO int,
	PLAZO smallint,
	MESES smallint,
	RETENCION_REAL numeric (15,2),
	INCIDENCIA char(1)	,
	FECHA_INI_BAJA varchar(10),
	FECHA_FIN varchar(10),
	REUBICADO CHAR(1)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_FONACOT, RFC, NOMBRE, NO_CREDITO, RETENCION_MENSUAL, CLAVE_EMPLEADO, PLAZO, MESES, RETENCION_REAL,
		INCIDENCIA, FECHA_INI_BAJA, FECHA_FIN, REUBICADO)	

	select CB_FONACOT, CB_RFC, PRETTYNAME, PR_REFEREN, PF_PAGO, CB_CODIGO,  PF_PLAZO, PF_PAGADAS, FC_NOMINA+FC_AJUSTE, 
			case 
				when FE_INCIDE = 'B' then FE_INCIDE 
			    when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '0' 	 
				when FE_INCIDE not in ('B', '0', 'I' ) then '0' 
				else FE_INCIDE 
			end as 
			FE_INCIDE, 

			case 
				when FE_INCIDE = 'B' then FE_FECHA1 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA1 
			end  as 
			FE_FECHA1, 

			case 
				when FE_INCIDE = 'B' then FE_FECHA2 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA2
			end  as 
			FE_FECHA2, 
            PF_REUBICA 
	from VCED_DET
	WHERE 	PF_YEAR = @YEAR AND PF_MES = @MES	AND ( RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')

		
	return
END
GO

create function FN_Fonacot_Get_Saldo_UltimaCedula(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1)
returns Pesos 
as
begin 

       declare @PR_MONTO Pesos 

       select top 1  @PR_MONTO = PF_PAGO from PCED_FON where CB_CODIGO = @CB_CODIGO and  PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN order by PF_YEAR desc, PF_MES desc 
       set @PR_MONTO = coalesce( @PR_MONTO , 0.000 ) 

       return @PR_MONTO 
       
end
go 

CREATE FUNCTION FN_FONACOT_PERIODOS
	(@AnioPeriodo Anio, @MesPeriodo Mes, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   PE_NUMERO Smallint,
	   PE_POS_FON Smallint,	   
       PE_USO Smallint 
	)
AS
BEGIN
	DECLARE @PE_POS_FON Dias

	insert into @Valores (PE_NUMERO, PE_POS_FON, PE_USO)
	SELECT PE_NUMERO, ROW_NUMBER () OVER (ORDER BY PE_NUMERO ASC), PE_USO
	FROM PERIODO WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodo  AND PE_TIPO = @TipoPeriodo AND PE_PRESTAM = 'S'
	GROUP BY PE_NUMERO, PE_USO
	ORDER BY PE_NUMERO ASC
	
	RETURN 
END
GO

CREATE FUNCTION FN_CALCULA_FON
	(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, 
	@TipoAjuste NumeroEmpleado, @AustarIncluyendoIncapacitados Booleano, 
	@SaldoPrestamoAjuste Decimal(15,2),
	@IncapacidadDias NumeroEmpleado, @IncapacidadesMes NumeroEmpleado, @AjusteMesAnterior Booleano,
	@AnioPeriodo Anio, @MesPeriodoFon Mes, @NumPeriodo NominaNumero, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   RETENCION_PERIODO Decimal(15,2), 
	   MONTO_CEDULA   Decimal(15,2), 
	   MONTO_RETENIDO Decimal(15,2), 
       MONTO_ESPERADO Decimal(15,2), 
       AJUSTE Decimal(15,2), 
       MONTO_CONCEPTO Decimal(15,2) ,
       DEUDA_MA Decimal(15,2) ,
	   NUM_PERIODO_MES int,
	   POS_PERIODO_MES int,
	   MES_ANTERIOR    int, 
	   YEAR_ANTERIOR   int 
	)
as
begin 
		declare @RETENCION_PERIODO Pesos 
		declare @MONTO_CEDULA   Pesos 
		declare @MONTO_RETENIDO Pesos 
		declare @MONTO_ESPERADO Pesos 
		declare @AJUSTE Pesos 
		declare @AJUSTE_BASE Pesos 
		declare @MONTO_CONCEPTO Pesos 
		declare @DEUDA_MA Pesos 
		DECLARE @NUM_PERIODO_MES Empleados
		DECLARE @POS_PERIODO_MES Empleados
		DECLARE @MES_ANTERIOR Status
		DECLARE @YEAR_ANTERIOR Anio
		DECLARE @PE_FEC_INI Fecha
		DECLARE @PE_FEC_FIN Fecha
		DECLARE @PE_USO Status
   	   
		SET @AJUSTE = 0;		
		SET @AJUSTE_BASE = 0;

		-- OBTENER INCAPACIDADES.
		IF @AustarIncluyendoIncapacitados = 'S' OR @TipoAjuste = 0
		BEGIN
			SET @IncapacidadesMes = 0
			SET @IncapacidadDias = 0
		END
		ELSE IF @AustarIncluyendoIncapacitados = 'N' AND @TipoAjuste > 0
		BEGIN
			-- OBTENER LOS D�AS DE INCAPACIDAD DE LOS PER�ODOS ANTERIORES			
			IF @IncapacidadDias < 0
			BEGIN
				SELECT @IncapacidadDias = NO_DIAS_IN FROM NOMINA WHERE CB_CODIGO = @CB_CODIGO
					AND PE_YEAR = @AnioPeriodo AND PE_NUMERO = @NumPeriodo AND PE_TIPO = @TipoPeriodo
			END

			IF @IncapacidadesMes < 0
			BEGIN
				SELECT @PE_FEC_INI = (MIN (PE_FEC_INI)), @PE_FEC_FIN =  (MAX (PE_FEC_FIN)) FROM PERIODO
					WHERE PE_NUMERO IN (SELECT PE_NUMERO FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo))
						AND PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo AND PE_PRESTAM = 'S'
			
				SELECT @IncapacidadesMes = COALESCE (SUM (IN_DIAS), 0) FROM INCAPACI WHERE CB_CODIGO = @CB_CODIGO					
					AND ((IN_FEC_INI >= @PE_FEC_INI AND IN_FEC_INI <= @PE_FEC_FIN ) OR (IN_FEC_FIN <= @PE_FEC_FIN AND IN_FEC_FIN > @PE_FEC_INI ))
			END				
		END
		-- ====================================================================================================================================

		-- OBTENER N�MERO DE PERIODO Y LA CANTIDAD DE PERIODOS DEL MES.
		SELECT @POS_PERIODO_MES = PE_POS_FON, @PE_USO = PE_USO FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo)
			WHERE PE_NUMERO = @NumPeriodo

		IF @PE_USO = 1
		BEGIN
			SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA, @POS_PERIODO_MES = PF_NOM_QTY + 1 FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
		END
		ELSE
		BEGIN
			SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
			set @MONTO_RETENIDO = coalesce( @MONTO_RETENIDO, 0.00 )
		END

		-- Cantidad de periodos Fonacot.
		SELECT @NUM_PERIODO_MES = COUNT(PE_NUMERO) FROM PERIODO
		WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo AND PE_PRESTAM = 'S' AND PE_USO = 0

		if (@MesPeriodoFon = 1 )
		begin
			set @MES_ANTERIOR = 12
			set @YEAR_ANTERIOR = @AnioPeriodo - 1;
		end
		else
		begin
			set @MES_ANTERIOR = @MesPeriodoFon - 1;
			set @YEAR_ANTERIOR = @AnioPeriodo;
		end		

		-- SI EL TIPO DE AJUSTE ES  1 (DURANTE EL MES, ES DECIR, SUAVIZADO) � 2 (AJUSTAR CADA PERIODO) Y SE PIDE AJUSTAR EL MES ANTERIOR,
		-- OBTENER AJUSTE DEL MES ANTERIOR.
		IF @AjusteMesAnterior = 'S'
		BEGIN
			SELECT @DEUDA_MA = (PF_PAGO-PF_NOMINA) FROM VCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @YEAR_ANTERIOR AND PF_MES = @MES_ANTERIOR  AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
			
			SET @DEUDA_MA = COALESCE (@DEUDA_MA, 0.000);
		END
		ELSE
			SET @DEUDA_MA = 0.000;
	    
		-- AJUSTE.
		-- 1. SUAVIZADO, ES DECIR, DURANTE EL MES.
		-- 2. AJUSTA CADA PER�ODO.
		-- 3. AJUSTA AL FINAL DE MES (�LTIMO PER�ODO).
	   
		-- OBTENER LA RETENCI�N POR PERIODO.
		IF @NUM_PERIODO_MES != 0
		BEGIN
			SELECT @RETENCION_PERIODO = ((PF_PAGO+@DEUDA_MA)/@NUM_PERIODO_MES) FROM PCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ;
			SET @RETENCION_PERIODO = coalesce( @RETENCION_PERIODO , 0.000 );
		END
		ELSE
		BEGIN
			SET @RETENCION_PERIODO = 0.0;
		END
		
		-- OBTENER MONTO ESPERADO.		
		IF ((@TipoAjuste = 1 OR @TipoAjuste = 2) OR (@TipoAjuste = 3 AND @POS_PERIODO_MES = @NUM_PERIODO_MES)) 			
			AND (@IncapacidadDias = 0) AND (@IncapacidadesMes = 0 )
		BEGIN
			SET @MONTO_ESPERADO = (@POS_PERIODO_MES - 1)*@RETENCION_PERIODO;			

			-- OBTENER EL AJUSTE,
			-- ES DECIR, LA DIFERENCIA ENTRE LO QUE DEBER�A LLEVAR DESCONTADO HASTA EL MOMENTO (@MONTO_ESPERADO)
			-- Y LO QUE EN REALIDAD LLEVA (VCED_FON.PF_NOMINA)	

			-- AJUSTE SUAVIZADO, ES DECIR, DURANTE LOS PER�ODOS QUE RESTAN EN EL MES.
			SET @AJUSTE_BASE = @MONTO_ESPERADO - @MONTO_RETENIDO;

			IF ( @SaldoPrestamoAjuste > 0 ) 
			BEGIN
			  SET @AJUSTE_BASE =  @AJUSTE_BASE - @SaldoPrestamoAjuste 
			END
 

            IF (@TipoAjuste = 1)
            BEGIN
                SET @AJUSTE = (@AJUSTE_BASE ) / ((@NUM_PERIODO_MES-@POS_PERIODO_MES)+1)
            END
            -- AJUSTE CADA PER�ODO O AL FINAL DEL MES.
            ELSE IF (@TipoAjuste = 2 OR @TipoAjuste = 3)
            BEGIN
                SET @AJUSTE = (@AJUSTE_BASE ) 
            END

			SET @AJUSTE = COALESCE (@AJUSTE, 0.000);
		END
		ELSE
		BEGIN
			SET @MONTO_ESPERADO = 0.000;
			SET @AJUSTE = 0.000;
		END	   
	   
		SET @MONTO_CONCEPTO = @RETENCION_PERIODO + @AJUSTE		 

		IF @POS_PERIODO_MES = @NUM_PERIODO_MES
		BEGIN
			IF ABS (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO)) < 1
			BEGIN
				SET @MONTO_CONCEPTO = @MONTO_CONCEPTO + (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO))				
			END
		END

		insert into @Valores (
				RETENCION_PERIODO, 
				MONTO_CEDULA, 
				MONTO_RETENIDO, 
				MONTO_ESPERADO, 
				AJUSTE, 
				MONTO_CONCEPTO ,
				DEUDA_MA, 
				NUM_PERIODO_MES,
				POS_PERIODO_MES,
				MES_ANTERIOR ,  
				YEAR_ANTERIOR
		) values ( 
				@RETENCION_PERIODO, 
				@MONTO_CEDULA, 
				@MONTO_RETENIDO, 
				@MONTO_ESPERADO, 
				@AJUSTE, 
				@MONTO_CONCEPTO ,
				@DEUDA_MA, 
				@NUM_PERIODO_MES,
				@POS_PERIODO_MES,
				@MES_ANTERIOR ,  
				@YEAR_ANTERIOR
		)

		RETURN 
end
GO

CREATE FUNCTION FN_SPLIT_NUMBERED(@String Formula, @Delimiter Codigo1) returns @temptable
 TABLE (idx int identity(1,1), items varchar(255)) as begin declare @idx int declare @slice varchar(255) 
 select @idx = 1 if len(@String)<1 or @String is null  return while @idx!= 0 begin set @idx = charindex(@Delimiter,@String) if @idx!=0 set @slice = left(@String,@idx - 1) else set @slice = @String if(len(@slice)>0) insert into @temptable(Items) values(@slice) set @String = right(@String,len(@String) - @idx) if len(@String) = 0 break end return end 
GO

create  function FN_GET_LAST_STRING(@String Formula, @Delimiter Codigo1) returns Formula 
as
begin 
	declare @Return Formula 

	set @Return = (
	select top 1 items from dbo.FN_SPLIT_NUMBERED( @String,  @Delimiter )
	order by idx desc 
	)

	set @Return = coalesce( @Return , '' ) 

	return @Return  
end
go 

create function FN_FONACOT_GET_EMPLEADOS_POR_APROX( @RFC varchar(255) ,  @Nombres varchar(255)  )
returns @Matches  
       table 
       (
             Orden int, 
             Seleccionado char(1), 
             CB_CODIGO int, 
             PRETTYNAME varchar(255), 
             DiffNombreCompleto int, 
             DiffApePat int, 
             DiffNombres int, 
             DiffRFC  int , 
             CB_RFC   varchar(30)
       ) 
as 
begin  

declare @ElementosStr Table 
(
       idx int, 
       item varchar(255) 
)

set @Nombres = LTRIM(RTRIM( @Nombres )) 
set @RFC  = LTRIM(RTRIM( @RFC )) 

if ( @Nombres = '' ) 
       return 

insert into @ElementosStr ( idx, item ) 
select idx, items from dbo.FN_SPLIT_NUMBERED(@Nombres, ' ' ) 

declare @Primero  varchar(255) 
declare @Ultimo  varchar(255) 

select @Primero = item from @ElementosStr where idx = 1 
select @Ultimo = item from @ElementosStr where idx = (select max(idx) from @ElementosStr ) 

set @Primero = coalesce( @Primero, '' ) 
set @Ultimo = coalesce( @Ultimo, '' ) 

if LTRIM(RTRIM( @Primero+@Ultimo )) <> '' and @RFC <> '' 
begin 

insert into @Matches ( Orden, Seleccionado, CB_CODIGO, PRETTYNAME, DiffNombreCompleto, DiffApePat, DiffNombres, DiffRFC , CB_RFC) 
select top 10  row_number() over ( order by DiffNombreCompleto desc, DiffApePat desc, DiffNombres desc ) as Orden , 
case when DiffNombreCompleto > 4 or DiffRFC > 4 then 'S' else 'N' end, 
CB_CODIGO, PRETTYNAME, DiffNombreCompleto, DiffApePat, DiffNombres , DiffRFC, CB_RFC   from  ( 
select CB_CODIGO, PRETTYNAME , 
DIFFERENCE(@Nombres, CB_APE_PAT + ' '+ CB_APE_MAT + ' ' + CB_NOMBRES) + 
case     
       when RTRIM( LTRIM( @Nombres )) = RTRIM( LTRIM( CB_APE_PAT + ' '+ CB_APE_MAT + ' ' + CB_NOMBRES))  then 4          
       when RTRIM( LTRIM( @Nombres )) = RTRIM( LTRIM( CB_APE_PAT + ' '+ CB_NOMBRES))  then 4 
       when RTRIM( LTRIM( @Nombres )) = RTRIM( LTRIM( CB_APE_PAT + ' '+ CB_APE_MAT))  then 4  
       else 0
end DiffNombreCompleto
, DIFFERENCE(@Primero, CB_APE_PAT) DiffApePat ,  DIFFERENCE(@Ultimo, CB_NOMBRES)  DiffNombres , DIFFERENCE( @RFC, CB_RFC ) 
+
case  
          when RTRIM( LTRIM( @RFC )) = RTRIM( LTRIM( CB_RFC))  then 4        
          else 0 
end 
DiffRFC , CB_RFC 
from COLABORA
where RTRIM(LTRIM(COLABORA.CB_FONACOT)) = '' and (  ( DIFFERENCE(@Nombres, CB_APE_PAT + ' '+ CB_APE_MAT + ' ' + CB_NOMBRES) >= 3   and  DIFFERENCE(@Primero, CB_APE_PAT) >=4  and  DIFFERENCE(@Ultimo, dbo.FN_GET_LAST_STRING( CB_NOMBRES, '') ) >=3    ) or ( LEFT( @RFC, 6) =  LEFT( CB_RFC, 6) ) )  
) Matches

end 

return 
end
go

create function FN_FONACOT_GET_CONTEO_CEDULA(  @PF_YEAR Anio, @PF_MES Mes, @RS_SOCIAL Codigo )
returns int 
as
begin 
	declare @Conteo int 
	select  @Conteo = count(*) from PCED_FON PF
		left outer join COLABORA CB on PF.CB_CODIGO = CB.CB_CODIGO 		
		where PF_YEAR = @PF_YEAR and PF_MES = @PF_MES  and ( PF.RS_CODIGO = @RS_SOCIAL or @RS_SOCIAL = '' ) 

	return @Conteo 
end 
GO

create function FN_FONACOT_CONTEO_CEDULA_AFECTADA (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6)) returns Integer 
as
begin
	 DECLARE @Return Integer

	 SET @Return = (SELECT Count(*)
     FROM VCED_FON V JOIN COLABORA C ON V.CB_CODIGO = C.CB_CODIGO
     WHERE V.PF_YEAR = @YEAR
	       AND V.PF_MES = @MES 
	       AND (V.RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')		   
		   and V.PF_NOM_QTY > 0 )

	 Return @Return
end
GO

create function PrestamoFonacot_PuedeCancelar(@PF_YEAR int, @PF_MES Status, @CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia ) 
returns Booleano 
as
begin 

	declare @PF_STATUS Status     
	declare @PF_YEAR_Anterior int 
	declare @PF_MES_Anterior int 

	if ( @PF_MES = 1 ) 
	begin 
		set @PF_YEAR_Anterior = @PF_YEAR - 1 
		set @PF_MES_Anterior = 12
	end 
	else
	begin
		set @PF_YEAR_Anterior = @PF_YEAR 
		set @PF_MES_Anterior = @PF_MES - 1 
	end 	
	

	if ( select COUNT(*) from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN and PF_YEAR >= @PF_YEAR and PF_MES > @PF_MES ) > 0 
		return 'N' 

	if ( select COUNT(*) from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN and PF_YEAR >= @PF_YEAR+1 and PF_MES > 0  ) > 0 
		return 'N' 


	select  @PF_STATUS = PF_STATUS from VCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN and PF_YEAR = @PF_YEAR_Anterior and PF_MES = @PF_MES_Anterior

	set @PF_STATUS = coalesce( @PF_STATUS, 0 ) 

	if ( @PF_STATUS in (0,2) ) 
		return 'S' 
	
	return 'N' 

end 
Go 

CREATE FUNCTION Fon_Diferencias_Totales( @Year int, @Mes int, @RSCodigo Codigo)
RETURNS TABLE
AS RETURN
		select
				Coalesce ([Baja], 0) as BajaIMSS, Coalesce (Incapacidad, 0) as Incapacidad, Coalesce ([Pr�stamo cancelado], 0) as PrestamoCancelado,
				Coalesce ([Pago de m�s], 0) as PagoMas, Coalesce ([Pago de menos], 0) as PagoMenos, 
				Coalesce (Otro, 0) as Otro, coalesce (Total, 0) Total,
				Coalesce ([Ausente en C�dula], 0) as Ausente, Coalesce ([Confirmaci�n de Fonacot], 0) as Confirmacion,
				Coalesce (CanceladoOtro, 0) as CanceladoOtro, Coalesce ([Pago directo en Fonacot], 0) as PagoDirecto,  coalesce (SubTotal, 0) SubTotal
			from
			(
				  SELECT FE_IN_DESC as columnname, SUM (DIFERENCIA) AS value
					  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND (RS_CODIGO = @RSCodigo OR @RSCodigo = '')
					  AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
					  GROUP BY FE_IN_DESC
				UNION
					  SELECT 'Otro' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND (RS_CODIGO = @RSCodigo OR @RSCodigo = '')	  
						  AND (FE_INCIDE = '0' OR FE_INCIDE = '')
				UNION
					  SELECT 'Total' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND (RS_CODIGO = @RSCodigo OR @RSCodigo = '')
				UNION
					 SELECT 
						PR_FON_MOT as columnname, SUM (DIFERENCIA) AS value
						FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND (RS_CODIGO = @RSCodigo OR @RSCodigo = '')
						AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
						AND FE_INCIDE = 'C'
						GROUP BY PR_FON_MOT
				UNION
					  SELECT 'SubTotal' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND (RS_CODIGO = @RSCodigo OR @RSCodigo = '')
						AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
						AND FE_INCIDE = 'C'
			  ) d
			pivot
			(
			  max(value)
			  for columnname in ([Baja], Incapacidad, [Pr�stamo cancelado], [Pago de m�s], [Pago de menos], Otro, Total,
				[Ausente en C�dula], [Confirmaci�n de Fonacot],  CanceladoOtro, [Pago directo en Fonacot], SubTotal)
			) piv
GO 

CREATE FUNCTION Periodo_GetStatusTimbrado(@PE_YEAR int,  @PE_TIPO int, @PE_NUMERO int,  @CM_NIVEL0 Formula = '' ) 
RETURNS TABLE 
AS RETURN
( 
select PE_YEAR, PE_TIPO, PE_NUMERO, PE_STATUS, PE_TIMBRO,  coalesce( [0], 0)  as Pendientes,  coalesce([2], 0) as Timbradas,  coalesce( [3], 0)  as TimbradoPendientes,  coalesce([4], 0) as CancelacionPendiente,  coalesce([0], 0)+coalesce([3], 0) as  PendientesTotal,  coalesce([2], 0)+coalesce([4], 0) as TimbradasTotal from 
  (
  select 
    PE.PE_YEAR,
    PE.PE_TIPO,
    PE.PE_STATUS, 
    PE.PE_TIMBRO,
    PE.PE_NUMERO,
    NO_TIMBRO, 
    COUNT(*) Empleados
  from PERIODO PE 
  left outer join NOMINA NO on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO   
  where 
  PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO 
  and (  @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='') )
  group by PE.PE_YEAR, PE.PE_TIPO, PE.PE_NUMERO,PE.PE_STATUS, PE.PE_TIMBRO, NO_TIMBRO 
  ) Timbs 
pivot 
(
    SUM(Empleados) 
    for NO_TIMBRO in ( [0], [1], [2], [3], [4] ) 
)  pvt 

)

GO

create function FN_FONACOT_GET_TOTALES_CEDULAS(  @PF_YEAR Anio, @PF_MES Mes, @RS_SOCIAL Codigo )
returns table  
return ( 
select  
    PF_YEAR, 
    PF_MES,
    SUM( CT_PAGO ) CT_PAGO,   
    SUM( CT_NOMINA ) CT_NOMINA , 
    SUM( CT_DIF ) CT_DIF, 
    SUM( CT_DIF_QTY ) CT_DIF_QTY, 
    SUM( CT_CRE_QTY ) CT_CRE_QTY
    from VCEDTFON CT
        left outer join COLABORA CB on CT.CB_CODIGO = CB.CB_CODIGO         
        where PF_YEAR = @PF_YEAR and PF_MES = @PF_MES  and ( RS_CODIGO = @RS_SOCIAL or @RS_SOCIAL = '' ) 
    group by PF_YEAR, PF_MES

)
GO

CREATE function WFConfig_GetUsuarioTRESS() 
returns Usuario 
as 
begin 
	
		declare @US_CODIGO Usuario 
		declare @XMLConfig XML 

		select @XMLConfig = ConfigXML from #COMPARTE..T_WFConfig 

		set @US_CODIGO= @XMLConfig.value('(/ControladorConfig/UsuarioSistema/.)[1]', 'INT' )

		set @US_CODIGO = coalesce( @US_CODIGO, 0 ) 

		return @US_CODIGO 
        
end

GO 

CREATE function WFEmpresas_GetCodigoEmpresa( @ClienteID FolioGrande ) returns Varchar(20) 
as 
begin 

        declare @Cm_Codigo Varchar(20) 
        
        set @Cm_Codigo = '' 
        
        if @ClienteID > 0 
                select @Cm_Codigo = CM_CODIGO from #COMPARTE..COMPANY where CM_WFEMPID = @ClienteID 
         
        return RTRIM( @Cm_Codigo) 
end

go 

create function WF_GetGlobalString ( @GlobalNo int ) 
returns Formula 
as 
begin
     return coalesce( ( select  top 1 GL_FORMULA from GLOBAL where GL_CODIGO = @GlobalNo ) , '' ) 
end 

go 

create function WF_GetGlobalBooleano ( @GlobalNo int ) 
returns Booleano 
as 
begin
     return coalesce( ( select top 1 SUBSTRING(GL_FORMULA,1,1) from GLOBAL where GL_CODIGO = @GlobalNo ) , 'N' ) 
end 

go 

create function WF_GetGlobalInteger ( @GlobalNo int ) 
returns integer  
as 
begin
     declare @Number int 
	 declare @ValueStr Formula 
	 set @ValueStr =  coalesce( ( select top 1 GL_FORMULA  from GLOBAL where GL_CODIGO = @GlobalNo ) , '0' )
	 
	 if ISNUMERIC ( @ValueStr )> 0  
		return cast( @ValueStr as int ) 
	  
	return 0 
end 

go 

create function WF_GetGlobalReal ( @GlobalNo int ) 
returns Pesos  
as 
begin
     
	 declare @ValueStr Formula 
	 set @ValueStr =  coalesce( ( select top 1 GL_FORMULA  from GLOBAL where GL_CODIGO = @GlobalNo ) , '0' )
	 
	 if ISNUMERIC ( @ValueStr )> 0  
		return cast( @ValueStr as Decimal(15,2)  ) 
	  
	return 0 
end 
go


create function  WF_GetGlobalesEmpleado() 
RETURNS @GlobalesEmpleado
  TABLE 
(
	Ciudad varchar(255) ,
	Estado varchar(255) ,
	Municipio varchar(255) ,
	Checa char(1),
	Nacion varchar(255) ,
	Sexo varchar(255) ,
	Credencial varchar(255) ,
	EstadoCivil varchar(255) ,
	ViveEn varchar(255) ,
	ViveCon varchar(255) ,
	MedioTransporte varchar(255) ,
	Estudio varchar(255) ,
	AutoSal char(1),
	ZonaGeografica varchar(1) ,
	Patron varchar(255) ,
	Salario Numeric(15,2) ,
	Contrato varchar(255) ,
	Puesto varchar(255) ,
	Clasifi varchar(255) ,
	Turno varchar(255) ,
	TablaSS varchar(255) ,
	Nivel1 varchar(255) ,
	Nivel2 varchar(255) ,
	Nivel3 varchar(255) ,
	Nivel4 varchar(255) ,
	Nivel5 varchar(255) ,
	Nivel6 varchar(255) ,
	Nivel7 varchar(255) ,
	Nivel8 varchar(255) ,
	Nivel9 varchar(255) ,
	Siguiente char(1) ,
	Regimen int ,
	Banco varchar(255) 
)  
as 
begin 

declare @K_GLOBAL_CIUDAD_EMPRESA  int 
declare @K_GLOBAL_ENTIDAD_EMPRESA  int 
declare @K_GLOBAL_DEF_MUNICIPIO  int 
declare @K_GLOBAL_DEF_CHECA_TARJETA int 
declare @K_GLOBAL_DEF_NACIONALIDAD  int 
declare @K_GLOBAL_DEF_SEXO  int 
declare @K_GLOBAL_DEF_LETRA_GAFETE  int 
declare @K_GLOBAL_DEF_ESTADO_CIVIL  int 
declare @K_GLOBAL_DEF_VIVE_EN   int 
declare @K_GLOBAL_DEF_VIVE_CON  int 
declare @K_GLOBAL_DEF_MEDIO_TRANSPORTE  int 
declare @K_GLOBAL_DEF_ESTUDIOS  int 
declare @K_GLOBAL_DEF_SALARIO_TAB  int 
declare @K_GLOBAL_ZONAS_GEOGRAFICAS  int 
declare @K_GLOBAL_DEF_REGISTRO_PATRONAL  int 
declare @K_GLOBAL_DEF_SALARIO_DIARIO  int 
declare @K_GLOBAL_DEF_CONTRATO  int 
declare @K_GLOBAL_DEF_PUESTO  int 
declare @K_GLOBAL_DEF_CLASIFICACION  int 
declare @K_GLOBAL_DEF_TURNO  int 
declare @K_GLOBAL_DEF_PRESTACIONES  int 
declare @K_GLOBAL_DEF_NIVEL_1  int 
declare @K_GLOBAL_DEF_NIVEL_2  int 
declare @K_GLOBAL_DEF_NIVEL_3  int 
declare @K_GLOBAL_DEF_NIVEL_4  int 
declare @K_GLOBAL_DEF_NIVEL_5  int 
declare @K_GLOBAL_DEF_NIVEL_6  int 
declare @K_GLOBAL_DEF_NIVEL_7  int 
declare @K_GLOBAL_DEF_NIVEL_8  int 
declare @K_GLOBAL_DEF_NIVEL_9  int 
declare @K_GLOBAL_NUM_EMP_AUTOMATICO  int 
declare @K_GLOBAL_DEF_REGIMEN  int 
declare @K_GLOBAL_DEF_BANCO  int 
    
set @K_GLOBAL_CIUDAD_EMPRESA           = 4; -- {Ciudad de Empresa}
set @K_GLOBAL_ENTIDAD_EMPRESA          = 5; -- {Entidad de Empresa}
set @K_GLOBAL_DEF_CHECA_TARJETA        = 89; -- { Default: Checa Tarjeta }
set @K_GLOBAL_DEF_NACIONALIDAD         = 90; -- { Default: Nacionalidad }
set @K_GLOBAL_DEF_SEXO                 = 91; -- { Default: Sexo }
set @K_GLOBAL_DEF_SALARIO_TAB          = 92; -- { Default: Salario por Tabulador }
set @K_GLOBAL_DEF_SALARIO_DIARIO       = 93; -- { Default: Salario Diario }
set @K_GLOBAL_DEF_LETRA_GAFETE         = 94; -- { Default: Letra de Gafete }
set @K_GLOBAL_DEF_ESTADO_CIVIL         = 95; -- { Default: C�digo de Estado Civil }
set @K_GLOBAL_DEF_VIVE_EN              = 96; -- { Default: C�digo de Vive En }
set @K_GLOBAL_DEF_VIVE_CON             = 97; -- { Default: C�digo de Vive Con }
set @K_GLOBAL_DEF_MEDIO_TRANSPORTE     = 98; -- { Default: C�digo de Medio de Transporte }
set @K_GLOBAL_DEF_ESTUDIOS             = 99; -- { Default: C�digo de Grado de Estudios }
set @K_GLOBAL_DEF_CONTRATO             = 100; --{ Default: Tipo de Contrato }
set @K_GLOBAL_DEF_PUESTO               = 101; --{ Default: C�digo de Puesto }
set @K_GLOBAL_DEF_CLASIFICACION        = 102; --{ Default: Clasificaci�n / Tabulador }
set @K_GLOBAL_DEF_TURNO                = 103; --{ Default: C�digo de Turno }
set @K_GLOBAL_DEF_PRESTACIONES         = 104; --{ Default: C�digo Tabla Prestaciones }
set @K_GLOBAL_DEF_REGISTRO_PATRONAL    = 105; --{ Default: C�digo Registro Patronal }
set @K_GLOBAL_DEF_NIVEL_1              = 106; --{ Default: C�digo de Nivel #1 }
set @K_GLOBAL_DEF_NIVEL_2              = 107; --{ Default: C�digo de Nivel #2 }
set @K_GLOBAL_DEF_NIVEL_3              = 108; --{ Default: C�digo de Nivel #3 }
set @K_GLOBAL_DEF_NIVEL_4              = 109; --{ Default: C�digo de Nivel #4 }
set @K_GLOBAL_DEF_NIVEL_5              = 110; --{ Default: C�digo de Nivel #5 }
set @K_GLOBAL_DEF_NIVEL_6              = 111; --{ Default: C�digo de Nivel #6 }
set @K_GLOBAL_DEF_NIVEL_7              = 112; --{ Default: C�digo de Nivel #7 }
set @K_GLOBAL_DEF_NIVEL_8              = 113; --{ Default: C�digo de Nivel #8 }
set @K_GLOBAL_DEF_NIVEL_9              = 114; --{ Default: C�digo de Nivel #9 }
set @K_GLOBAL_NUM_EMP_AUTOMATICO       = 116; -- { SI/NO se desea generar el # de Empleado autom�ticamente}
set @K_GLOBAL_DEF_MUNICIPIO            = 247;
set @K_GLOBAL_DEF_BANCO                =  311;
set @K_GLOBAL_DEF_REGIMEN              =  312;

insert into @GlobalesEmpleado 
select 
Ciudad            = dbo.WF_GetGlobalString( @K_GLOBAL_CIUDAD_EMPRESA  ), 
Estado            = dbo.WF_GetGlobalString( @K_GLOBAL_ENTIDAD_EMPRESA  ), 
Municipio         = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_MUNICIPIO  ), 
Checa             = dbo.WF_GetGlobalBooleano( @K_GLOBAL_DEF_CHECA_TARJETA ), 
Nacion            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NACIONALIDAD  ), 
Sexo              = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_SEXO  ), 
Credencial        = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_LETRA_GAFETE  ), 
EstadoCivil       = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_ESTADO_CIVIL  ), 
ViveEn            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_VIVE_EN   ), 
ViveCon           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_VIVE_CON  ), 
MedioTransporte   = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_MEDIO_TRANSPORTE  ), 
Estudio           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_ESTUDIOS  ), 
AutoSal           = dbo.WF_GetGlobalBooleano( @K_GLOBAL_DEF_SALARIO_TAB  ), 
ZonaGeografica    = SUBSTRING( dbo.WF_GetGlobalString( @K_GLOBAL_ZONAS_GEOGRAFICAS),1,1 ), 
Patron            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_REGISTRO_PATRONAL  ), 
Salario           = dbo.WF_GetGlobalReal( @K_GLOBAL_DEF_SALARIO_DIARIO  ), 
Contrato          = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_CONTRATO  ), 
Puesto            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_PUESTO  ), 
Clasifi           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_CLASIFICACION  ), 
Turno             = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_TURNO  ), 
TablaSS           = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_PRESTACIONES  ), 
Nivel1            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_1  ), 
Nivel2            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_2  ), 
Nivel3            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_3  ), 
Nivel4            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_4  ), 
Nivel5            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_5  ), 
Nivel6            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_6  ), 
Nivel7            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_7  ), 
Nivel8            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_8  ), 
Nivel9            = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_NIVEL_9  ), 
Siguiente         = dbo.WF_GetGlobalBooleano( @K_GLOBAL_NUM_EMP_AUTOMATICO  ), 
Regimen           = dbo.WF_GetGlobalInteger( @K_GLOBAL_DEF_REGIMEN  ), 
Banco             = dbo.WF_GetGlobalString( @K_GLOBAL_DEF_BANCO  )


return 
end 

go 

CREATE function WF_isEmpty( @var1 varchar(256),  @var2 varchar(256)  ) 
returns Varchar(256) 
as 
begin 	
	
	set @var1 = coalesce( @var1, '' ) 
	if ( @var1 = '' ) 
		return @var2 
	
	return @var1 

end
go 

CREATE function WF_GetCodigoEscolaridad( @iEscolaridad int ) 
returns Codigo2
as 
begin 	
  declare @Codigo Codigo2 
  declare @Numero Pesos 

  set @Numero = @iEscolaridad

  select  @Codigo = TB_CODIGO from  ESTUDIOS  where TB_NUMERO = @Numero 
  return @Codigo 

end
go 


create function KardexConceptos_GetVersionActual() 
returns FolioGrande 
as
begin 
declare @verTotal FolioGrande 
	
	select @verTotal = max(KT_VERSION) from CONCEPTOVT 	
	set @verTotal = coalesce( @VerTotal, 0 ) 	
	
	return @verTotal 
	
end
GO

CREATE function PrestamoFonacot_PuedeActivar(@PF_YEAR int, @PF_MES Status, @CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia ) 
returns Booleano 
as
begin 
    declare @FechaIni Fecha
    declare @CB_FEC_BAJ Fecha 
    declare @CB_FEC_ING Fecha 
    declare @CB_NOMINA NominaTipo 

    select @CB_FEC_BAJ = CB_FEC_BAJ, @CB_NOMINA = CB_NOMINA, @CB_FEC_ING = CB_FEC_ING from COLABORA where CB_CODIGO = @CB_CODIGO 
    select @FechaIni = MIN(PE_FEC_INI) from PERIODO where PE_MES_FON = @PF_MES and PE_YEAR = @PF_YEAR and PE_TIPO = @CB_NOMINA 

	if ( @CB_FEC_ING > @CB_FEC_BAJ  or @CB_FEC_BAJ >= @FechaIni )  
    begin 
        return 'S' 
    end 

    return 'N' 
end
GO

create function FONACOT_CEDULA_TRABAJADOR
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
(
	NO_CT int,
	ANIO_EMISION smallint,
	MES_EMISION smallint,
    NO_FONACOT varchar(30),
    RFC varchar(30),
    NO_SS varchar(11),
	NOMBRE VARCHAR(250),
	CLAVE_EMPLEADO int,
	CTD_CREDITOS int,
	RETENCION_MENSUAL numeric(15,2),
	TIPO char(1),
	RETENCION_REAL numeric (15,2),
	FECHA_BAJA varchar(10),
	FECHA_INI varchar(10),
	FECHA_FIN varchar(10)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_CT, ANIO_EMISION, MES_EMISION, NO_FONACOT, RFC, NO_SS, NOMBRE, CLAVE_EMPLEADO, CTD_CREDITOS, RETENCION_MENSUAL, TIPO, RETENCION_REAL,
		FECHA_BAJA, FECHA_INI, FECHA_FIN)	

	select PE.PE_NO_CT, @YEAR ANIO_EMISION, @MES MES_EMISION, CB_FONACOT, CB_RFC, CB_SEGSOC, PRETTYNAME, CB_CODIGO, PF_CTD, PF_PAGO,			
			case
				when FE_INCIDE = 'B' then FE_INCIDE
			    when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '0'				
				when FE_INCIDE not in ('B', '0', 'I', 'L', 'M' ) then '0' 				
				else FE_INCIDE 				
			end as 
			FE_INCIDE,
			FC_NOMINA+FC_AJUSTE AS RETENCION_REAL,
			case 
				when FE_INCIDE = 'B' then FE_FECHA1 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 		
				else '' 
			end as 
			FECHA_BAJA,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA1 
			end  as 
			FECHA_INI,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA2
			end  as 
			FECHA_FIN
	from VCED_DET VC, PCED_ENC PE
	WHERE 	PF_YEAR = @YEAR AND PF_MES = @MES	AND ( VC.RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')
	AND VC.PF_YEAR = PE.PE_YEAR AND VC.PF_MES = PE.PE_MES
	AND (VC.RS_CODIGO = PE.RS_CODIGO OR PE.RS_CODIGO = '') 
		
	return
END
GO 

create function TimbradoConcilia_GetPeriodos(  @RS_CODIGO Codigo, @PE_YEAR int,  @PE_MES status ) 
returns Table 
as
return ( 
select  PE_YEAR, PE_TIPO , PE_NUMERO, PE_TIMBRO, TOTAL, TIMBRADOS_TRESS, (TOTAL - CONCILIADOS) CONCILIADOS , 
	cast( case 
	when TOTAL > 0 then  100.00 -  ( CONCILIADOS*1.0 / TOTAL*1.0 ) * 100.00 
		else 0.00 
	end  as decimal(15,2) )  PORC_CONCILIADOS,  
	TIMBRADOS_SAT, 
		case 
		when CONCILIADOS = TOTAL then 'False'
		else 'True'  
	end  CONCILIAR,
	0 as CONCILIA_PROCESO ,
	cast ('' as VARCHAR(250) ) as CONCILIA_STATUS,
	0 as CONCILIA_AVANCE,
	0 as DB_PROCESO,
	cast ('' as VARCHAR(250) ) as NOMBRE_PERIODO, 
	cast ('' as VARCHAR(250) ) as DETALLE 	
from ( 

select NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO, COUNT(*) TOTAL, 
SUM( case  
		when NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_TRESS, 

SUM( case  
		when NC_RESULT  = 0  and NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_SAT, 

SUM( case  
		when NC_RESULT = 0 then 1 
		else 0 
	 end 
	 ) CONCILIADOS
from VTIMCONCIL NC
join PERIODO PE on  NC.PE_YEAR = PE.PE_YEAR and NC.PE_TIPO = PE.PE_TIPO and NC.PE_NUMERO = PE.PE_NUMERO 
where PE.PE_YEAR = @PE_YEAR and PE_MES = @PE_MES and NC.RS_CODIGO = @RS_CODIGO 
group by NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO

) Conciliacion 
) 

go 


create function TimbradoConcilia_GetNominasTRESSTabla(  @RS_CODIGO Codigo, @PE_YEAR int,  @PE_TIPO int, @PE_NUMERO int ) 
returns Table 
as
return 
(
SELECT CB_CODIGO [Numero], CB_CURP [CURP], NC_STATUS_TRESS [Status], NO_FACUUID [UUID], NO_FACTURA [FacturaID] from VTIMCONCIL as [NOMINA] where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO =  @PE_NUMERO
and [NOMINA].RS_CODIGO = @RS_CODIGO
)

go 

create function TimbradoConcilia_GetDetalle(  @RS_CODIGO Codigo, @PE_YEAR int,  @PE_TIPO status, @PE_NUMERO int ) 
returns Table 
as
return ( 
select CB_CODIGO, PRETTYNAME, CB_CURP, NO_TIM_DESC, NC_RES_DESC, NO_FACUUID, NC_UUID  from VTIMCONCIL
where RS_CODIGO = @RS_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO 	and NC_RESULT > 0 
)

go 

CREATE FUNCTION Periodo_GetStatusTimbrado_RazonSocial(@PE_YEAR int,  @PE_TIPO int, @PE_NUMERO int,  @CM_NIVEL0 Formula = '',  @RAZON_SOCIAL Codigo ) 
RETURNS TABLE 
AS RETURN
( 
select PE_YEAR, PE_TIPO, PE_NUMERO, PE_STATUS, PE_TIMBRO,  coalesce( [0], 0)  as Pendientes,  coalesce([2], 0) as Timbradas,  coalesce( [3], 0)  as TimbradoPendientes,  coalesce([4], 0) as CancelacionPendiente,  coalesce([0], 0)+coalesce([3], 0) as  PendientesTotal,  coalesce([2], 0)+coalesce([4], 0) as TimbradasTotal from 
  (
  select 
    PE.PE_YEAR,
    PE.PE_TIPO,
    PE.PE_STATUS, 
    PE.PE_TIMBRO,
    PE.PE_NUMERO,
    NO_TIMBRO, 
    COUNT(*) Empleados
  from PERIODO PE 
  left outer join NOMINA NO on PE.PE_YEAR = NO.PE_YEAR and PE.PE_TIPO = NO.PE_TIPO and PE.PE_NUMERO = NO.PE_NUMERO   
  left outer join RPATRON on RPATRON.TB_CODIGO = NO.CB_PATRON
  left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
  where 
  PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO 
  and (  @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='') )
  and ( RSOCIAL.RS_CODIGO = @RAZON_SOCIAL )
  group by PE.PE_YEAR, PE.PE_TIPO, PE.PE_NUMERO,PE.PE_STATUS, PE.PE_TIMBRO, NO_TIMBRO 
  ) Timbs 
pivot 
(
    SUM(Empleados) 
    for NO_TIMBRO in ( [0], [1], [2], [3], [4] ) 
)  pvt 

)

GO

CREATE FUNCTION FN_FONACOT_MONTO_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, @YEAR SMALLINT, @MES SMALLINT) returns Pesos 
as
begin
	 DECLARE @Return Pesos

	IF (SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 328 ) = 'N'
	BEGIN
		SELECT @Return = PR_PAG_PER FROM PRESTAMO WHERE CB_CODIGO = @CB_CODIGO AND PR_TIPO = @PR_TIPO AND PR_REFEREN = @PR_REFEREN
	END
	ELSE
	BEGIN
		SELECT @Return = COALESCE (PF_PAGO, 0) FROM PCED_FON WHERE CB_CODIGO = @CB_CODIGO AND PR_TIPO = @PR_TIPO AND PR_REFEREN = @PR_REFEREN
			AND PF_YEAR = @YEAR AND PF_MES = @MES

		IF @Return IS NULL
			SET @Return = 0
	END

	Return @Return

end
go
