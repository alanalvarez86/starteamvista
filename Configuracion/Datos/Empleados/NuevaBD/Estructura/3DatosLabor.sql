
CREATE TABLE TOPERA (
       TO_CODIGO            Codigo,
       TO_DESCRIP           Descripcion,
       TO_INGLES            Descripcion,
       TO_NUMERO            Pesos,
       TO_TEXTO             Descripcion
)
GO


ALTER TABLE TOPERA
       ADD CONSTRAINT PK_TOPERA PRIMARY KEY (TO_CODIGO)
GO


CREATE TABLE OPERA (
       OP_NUMBER            Operacion,
       TO_CODIGO            Codigo,
       OP_NOMBRE            Descripcion,
       OP_DESCRIP           Formula,
       OP_STD_HR            Tiempo,
       OP_TIPO              Status,
       OP_FACTOR            Factor,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       OP_STD_CST           Pesos,
       OP_INGLES            Descripcion
)
GO


ALTER TABLE OPERA
       ADD CONSTRAINT PK_OPERA PRIMARY KEY (OP_NUMBER)
GO


CREATE TABLE TPARTE (
       TT_CODIGO            Codigo,
       TT_DESCRIP           Descripcion,
       TT_INGLES            Descripcion,
       TT_NUMERO            Pesos,
       TT_TEXTO             Descripcion
)
GO


ALTER TABLE TPARTE
       ADD CONSTRAINT PK_TPARTE PRIMARY KEY (TT_CODIGO)
GO


CREATE TABLE PARTES (
       AR_CODIGO            CodigoParte,
       AR_NOMBRE            DescLarga,
       AR_SHORT             DescCorta,
       AR_BARCODE           DescCorta,
       AR_STD_HR            Tasa,
       AR_FACTOR            Factor,
       TT_CODIGO            Codigo,
       AR_STD_CST           Pesos,
       AR_INGLES            Descripcion
)
GO


ALTER TABLE PARTES
       ADD CONSTRAINT PK_PARTES PRIMARY KEY (AR_CODIGO)
GO


CREATE TABLE WORDER (
       WO_NUMBER            OrdenTrabajo,
       AR_CODIGO            CodigoParte,
       WO_DESCRIP           Formula,
       WO_QTY               Piezas,
       WO_FINISHD           Piezas,
       WO_FEC_INI           Fecha,
       WO_FEC_FIN           Fecha,
       WO_FEC_CIE           Fecha,
       WO_STATUS            Status,
       WO_LST_STP           FolioChico,
       WO_TEXTO             DescLarga,
       WO_NUM_GEN           Pesos
)
GO


ALTER TABLE WORDER
       ADD CONSTRAINT PK_WORDER PRIMARY KEY (WO_NUMBER)
GO


CREATE TABLE WOFIJA (
       CB_CODIGO            NumeroEmpleado,
       WO_NUMBER            OrdenTrabajo,
       WF_FEC_INI           Fecha,
       OP_NUMBER            Operacion,
       AR_CODIGO            CodigoParte
)
GO


ALTER TABLE WOFIJA
       ADD CONSTRAINT PK_WOFIJA PRIMARY KEY (CB_CODIGO, WF_FEC_INI)
GO


CREATE TABLE DEFSTEPS (
       AR_CODIGO            CodigoParte,
       DF_SEQUENC           FolioChico,
       OP_NUMBER            Operacion,
       DF_STD_HR            Tiempo
)
GO


ALTER TABLE DEFSTEPS
       ADD CONSTRAINT PK_DEFSTEPS PRIMARY KEY (AR_CODIGO, DF_SEQUENC)
GO


CREATE TABLE STEPS (
       WO_NUMBER            OrdenTrabajo,
       ST_SEQUENC           FolioChico,
       OP_NUMBER            Operacion,
       ST_STD_HR            Tiempo,
       ST_REAL_HR           Tiempo,
       ST_QTY               Piezas
)
GO

CREATE INDEX XIE1STEPS ON STEPS
(
       WO_NUMBER,
       OP_NUMBER
)
GO


ALTER TABLE STEPS
       ADD CONSTRAINT PK_STEPS PRIMARY KEY (WO_NUMBER, ST_SEQUENC)
GO


CREATE TABLE WORKS (
       CB_CODIGO            NumeroEmpleado,
       AU_FECHA             Fecha,
       WK_HORA_A            Hora,
       WK_FOLIO             FolioChico,
       OP_NUMBER            Operacion,
       WO_NUMBER            OrdenTrabajo,
       AR_CODIGO            CodigoParte,
       WK_FECHA_R           Fecha,
       WK_HORA_R            Hora,
       WK_HORA_I            Hora,
       WK_LINX_ID           LinxID,
       WK_TIEMPO            Tiempo,
       WK_PRE_CAL           Tiempo,
       WK_STATUS            Status,
       WK_MOD_1             Codigo1,
       WK_MOD_2             Codigo1,
       WK_MOD_3             Codigo1,
       WK_TMUERTO           Codigo3,
       WK_HRS_ORD           Horas,
       WK_HRS_2EX           Horas,
       WK_HRS_3EX           Horas,
       WK_MANUAL            Booleano,
       WK_PIEZAS            Piezas,
       WK_STD_HR            Tiempo,
       WK_STD_CST           Pesos,
       CB_PUESTO            Codigo,
       CB_AREA              Codigo,
       WK_TIPO              Status,
       WK_CEDULA            FolioGrande
)
GO

CREATE INDEX XIE1WORKS ON WORKS
(
       WO_NUMBER,
       OP_NUMBER
)
GO

CREATE INDEX XIE2WORKS ON WORKS
(
       WO_NUMBER,
       CB_CODIGO
)
GO

CREATE INDEX XIE3WORKS ON WORKS
(
       AU_FECHA,
       WK_HORA_A
)
GO

CREATE INDEX XIE4WORKS ON WORKS
(
       WK_CEDULA
)
GO


ALTER TABLE WORKS
       ADD CONSTRAINT PK_WORKS PRIMARY KEY (CB_CODIGO, AU_FECHA, WK_HORA_A, WK_FOLIO)
GO


CREATE TABLE MODULA1 (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO


ALTER TABLE MODULA1
       ADD CONSTRAINT PK_MODULA1 PRIMARY KEY (TB_CODIGO)
GO


CREATE TABLE LECTURAS (
       CB_CODIGO            NumeroEmpleado,
       LX_FECHA             Fecha,
       LX_HORA              Hora,
       LX_WORDER            OrdenTrabajo,
       LX_OPERA             Operacion,
       LX_PIEZAS            Piezas,
       LX_STATUS            Status,
       LX_MODULA1           Codigo1,
       LX_MODULA2           Codigo1,
       LX_MODULA3           Codigo1,
       LX_TMUERTO           Codigo3,
       LX_NUMERO            LinxID,
       LX_LINX_ID           LinxID,
       LX_FOLIO             FolioChico
)
GO


ALTER TABLE LECTURAS
       ADD CONSTRAINT PK_LECTURAS PRIMARY KEY (CB_CODIGO, LX_FECHA, LX_HORA, LX_WORDER, 
              LX_OPERA)
GO


CREATE TABLE MODULA2 (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO


ALTER TABLE MODULA2
       ADD CONSTRAINT PK_MODULA2 PRIMARY KEY (TB_CODIGO)
GO


CREATE TABLE MODULA3 (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO


ALTER TABLE MODULA3
       ADD CONSTRAINT PK_MODULA3 PRIMARY KEY (TB_CODIGO)
GO


CREATE TABLE TMUERTO (
       TB_CODIGO            Codigo3,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO


ALTER TABLE TMUERTO
       ADD CONSTRAINT PK_TMUERTO PRIMARY KEY (TB_CODIGO)
GO


CREATE TABLE AREA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       TB_BREAK_1           Codigo3,
       TB_BREAK_2           Codigo3,
       TB_BREAK_3           Codigo3,
       TB_BREAK_4           Codigo3,
       TB_BREAK_5           Codigo3,
       TB_BREAK_6           Codigo3,
       TB_BREAK_7           Codigo3,
       TB_OPERA             Operacion,
       TB_OP_UNI            Booleano,
       AR_TIPO              Status,
       AR_SHIFT             Status,
       AR_PRI_HOR           Hora 
)
GO


ALTER TABLE AREA
       ADD CONSTRAINT PK_AREA PRIMARY KEY (TB_CODIGO)
GO


CREATE TABLE CEDULA (
       CE_FOLIO             FolioGrande,
       WO_NUMBER            OrdenTrabajo,
       AR_CODIGO            CodigoParte,
       OP_NUMBER            Operacion,
       CE_AREA              Codigo,
       CE_TIPO              Status,
       CE_FECHA             Fecha,
       CE_HORA              Hora,
       CE_STATUS            Status,
       CE_MOD_1             Codigo1,
       CE_MOD_2             Codigo1,
       CE_MOD_3             Codigo1,
       CE_TMUERTO           Codigo3,
       CE_PIEZAS            Piezas,
       CE_COMENTA           DescLarga,
       CE_MULTI             Booleano,
       CE_TIEMPO            Tiempo,
       US_CODIGO            Usuario
)
GO

ALTER TABLE CEDULA
       ADD CONSTRAINT PK_CEDULA PRIMARY KEY (CE_FOLIO)
GO

CREATE UNIQUE INDEX XAK1CEDULA ON CEDULA
(
       CE_AREA,
       CE_FECHA,
       CE_HORA
)
GO

CREATE TABLE CED_EMP (
       CE_FOLIO             FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       CE_POSICIO           FolioChico,
       CB_PUESTO            Codigo
)
GO


ALTER TABLE CED_EMP
       ADD CONSTRAINT PK_CED_EMP PRIMARY KEY (CE_FOLIO, CB_CODIGO)
GO

CREATE TABLE CED_WORD (
       CE_FOLIO             FolioGrande,
       WO_NUMBER            OrdenTrabajo,
       CW_POSICIO           FolioChico,
       CW_PIEZAS            Piezas
)
GO


ALTER TABLE CED_WORD
       ADD CONSTRAINT PK_CED_WORD PRIMARY KEY (CE_FOLIO, WO_NUMBER)
GO

CREATE TABLE BREAKS (
       BR_CODIGO            Codigo3,
       BR_NOMBRE            Descripcion,
       BR_INGLES            Descripcion,
       BR_NUMERO            Pesos,
       BR_TEXTO             Descripcion
)
GO


ALTER TABLE BREAKS
       ADD CONSTRAINT PK_BREAKS PRIMARY KEY (BR_CODIGO)
GO


CREATE TABLE BRK_HORA (
       BR_CODIGO            Codigo3,
       BH_INICIO            Hora,
       BH_TIEMPO            FolioChico,
       BH_MOTIVO            Codigo3
)
GO


ALTER TABLE BRK_HORA
       ADD CONSTRAINT PK_BRK_HORA PRIMARY KEY (BR_CODIGO, BH_INICIO)
GO

CREATE TABLE KAR_AREA ( CB_CODIGO NUMEROEMPLEADO,
        		KA_FECHA FECHA,
        		KA_HORA HORA,
        		CB_AREA CODIGO,
        		US_CODIGO USUARIO,
       	 		KA_FEC_MOV FECHA,
        		KA_HOR_MOV HORA )
GO

ALTER TABLE KAR_AREA
      ADD CONSTRAINT PK_KAR_AREA PRIMARY KEY ( CB_CODIGO, KA_FECHA, KA_HORA )
GO

CREATE TABLE SUP_AREA ( US_CODIGO USUARIO, CB_AREA CODIGO )
GO

ALTER TABLE SUP_AREA
      ADD CONSTRAINT PK_SUP_AREA PRIMARY KEY ( US_CODIGO, CB_AREA )
GO

CREATE TABLE TMPLABOR ( US_CODIGO USUARIO,
       			CB_CODIGO NUMEROEMPLEADO,
       			CB_AREA CODIGO,
       			CB_CHECA BOOLEANO,
       			CB_TURNO CODIGO,
       			TL_STATUS INTEGER,
       			TL_APLICA BOOLEANO,
       			TL_CALCULA BOOLEANO)
GO

ALTER TABLE TMPLABOR
      ADD CONSTRAINT PK_TMPLABOR PRIMARY KEY ( US_CODIGO, CB_CODIGO )
GO

CREATE TABLE TDEFECTO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE TDEFECTO
       ADD CONSTRAINT PK_TDEFECTO PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE CED_INSP (
       CI_FOLIO             FolioGrande,
       CI_AREA              Codigo,
       CE_FOLIO             FolioGrande,
       WO_NUMBER            OrdenTrabajo,
       AR_CODIGO            CodigoParte,
       CI_FECHA             Fecha,
       CI_HORA              Hora,
       US_CODIGO            Usuario,
       CI_TIPO              Status,
       CI_RESULT            Status,
       CI_COMENTA           DescLarga,
       CI_OBSERVA           Memo,
       CI_TAMLOTE           Piezas,
       CI_MUESTRA           Piezas,
       CI_TIEMPO            Minutos,
       CI_NUMERO1           Pesos,
       CI_NUMERO2           Pesos
)
GO

ALTER TABLE CED_INSP
       ADD CONSTRAINT PK_CED_INSP PRIMARY KEY (CI_FOLIO)
GO

CREATE TABLE DEFECTO (
       CI_FOLIO             FolioGrande,
       DE_FOLIO             FolioChico,
       DE_CODIGO            Codigo,
       DE_PIEZAS            Piezas,
       DE_COMENTA           DescLarga
)
GO

ALTER TABLE DEFECTO
       ADD CONSTRAINT PK_DEFECTO PRIMARY KEY (CI_FOLIO, DE_FOLIO)
GO

CREATE TABLE MOTSCRAP (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion )
GO

ALTER TABLE MOTSCRAP
      ADD CONSTRAINT PK_MOTSCRAP PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE COMPONEN (
       CN_CODIGO            CodigoParte,
       CN_NOMBRE            DescLarga,
       CN_SHORT             DescCorta,
       CN_BARCODE           DescCorta,
       CN_INGLES            Descripcion,
       CN_COSTO             Pesos,
       CN_UNIDAD            Referencia,
       CN_DETALLE           Formula,
       CN_NUMERO1           Pesos,
       CN_NUMERO2           Pesos,
       CN_TEXTO1            Descripcion,
       CN_TEXTO2            Descripcion )
GO

ALTER TABLE COMPONEN
      ADD CONSTRAINT PK_COMPONEN PRIMARY KEY (CN_CODIGO)
GO

CREATE TABLE CEDSCRAP (
       CS_FOLIO             FolioGrande,
       CS_AREA              Codigo,
       CS_FECHA             Fecha,
       CS_HORA              Hora,
       WO_NUMBER            OrdenTrabajo,
       AR_CODIGO            CodigoParte,
       OP_NUMBER            Operacion,
       CS_FEC_FAB           Fecha,
       CS_TAMLOTE           Piezas,
       CS_COMENTA           DescLarga,
       CS_OBSERVA           Memo,
       CS_NUMERO1           Pesos,
       CS_NUMERO2           Pesos,
       CS_TEXTO1            Descripcion,
       CS_TEXTO2            Descripcion,
       US_CODIGO            Usuario )
GO

ALTER TABLE CEDSCRAP
      ADD CONSTRAINT PK_CEDSCRAP PRIMARY KEY (CS_FOLIO)
GO

CREATE TABLE SCRAP (
       CS_FOLIO             FolioGrande,
       SC_FOLIO             FolioChico,
       CN_CODIGO            CodigoParte,
       SC_MOTIVO            Codigo,
       SC_PIEZAS            Piezas,
       SC_COMENTA           DescLarga )
GO

ALTER TABLE SCRAP
      ADD CONSTRAINT PK_SCRAP PRIMARY KEY (CS_FOLIO, SC_FOLIO)
GO
