CREATE TABLE TALLA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go

ALTER TABLE TALLA
       ADD CONSTRAINT PK_TALLA PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE MOT_TOOL (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status,
       TB_PRESTA            Codigo1
)
go


ALTER TABLE MOT_TOOL
       ADD CONSTRAINT PK_MOT_TOOL PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE COLABORA (
       CB_CODIGO            NumeroEmpleado,
       CB_ACTIVO            Booleano,
       CB_APE_MAT           Descripcion,
       CB_APE_PAT           Descripcion,
       CB_AUTOSAL           Booleano,
       CB_AR_FEC            Fecha,
       CB_AR_HOR            Hora,
       CB_BAN_ELE           Descripcion,
       CB_CARRERA           DescLarga,
       CB_CHECA             Booleano,
       CB_CIUDAD            Descripcion,
       CB_CLASIFI           Codigo,
       CB_CODPOST           Referencia,
       CB_CONTRAT           Codigo1,
       CB_CREDENC           Codigo1,
       CB_CURP              Descripcion,
       CB_CALLE 		   Observaciones,
       CB_COLONIA           Descripcion,
       CB_EDO_CIV           Codigo1,
       CB_FEC_RES           Fecha,
       CB_EST_HOR           Descripcion,
       CB_EST_HOY           Booleano,
       CB_ESTADO            Codigo2,
       CB_ESTUDIO           Codigo2,
       CB_EVALUA            DiasFrac,
       CB_EXPERIE           Titulo,
       CB_FEC_ANT           Fecha,
       CB_FEC_BAJ           Fecha,
       CB_FEC_BSS           Fecha,
       CB_FEC_CON           Fecha,
       CB_FEC_ING           Fecha,
       CB_FEC_INT           Fecha,
       CB_FEC_NAC           Fecha,
       CB_FEC_REV           Fecha,
       CB_FEC_VAC           Fecha,
       CB_G_FEC_1           Fecha,
       CB_G_FEC_2           Fecha,
       CB_G_FEC_3           Fecha,
       CB_G_FEC_4           Fecha,
       CB_G_FEC_5           Fecha,
       CB_G_FEC_6           Fecha,
       CB_G_FEC_7           Fecha,
       CB_G_FEC_8           Fecha,
       CB_G_LOG_1           Booleano,
       CB_G_LOG_2           Booleano,
       CB_G_LOG_3           Booleano,
       CB_G_LOG_4           Booleano,
       CB_G_LOG_5           Booleano,
       CB_G_LOG_6           Booleano,
       CB_G_LOG_7           Booleano,
       CB_G_LOG_8           Booleano,
       CB_G_NUM_1           Pesos,
       CB_G_NUM_2           Pesos,
       CB_G_NUM_3           Pesos,
       CB_G_NUM_4           Pesos,
       CB_G_NUM_5           Pesos,
       CB_G_NUM_6           Pesos,
       CB_G_NUM_7           Pesos,
       CB_G_NUM_8           Pesos,
       CB_G_NUM_9           Pesos,
       CB_G_NUM10           Pesos,
       CB_G_NUM11           Pesos,
       CB_G_NUM12           Pesos,
       CB_G_NUM13           Pesos,
       CB_G_NUM14           Pesos,
       CB_G_NUM15           Pesos,
       CB_G_NUM16           Pesos,
       CB_G_NUM17           Pesos,
       CB_G_NUM18           Pesos,
       CB_G_TAB_1           Codigo,
       CB_G_TAB_2           Codigo,
       CB_G_TAB_3           Codigo,
       CB_G_TAB_4           Codigo,
       CB_G_TAB_5           Codigo,
       CB_G_TAB_6           Codigo,
       CB_G_TAB_7           Codigo,
       CB_G_TAB_8           Codigo,
       CB_G_TAB_9           Codigo,
       CB_G_TAB10           Codigo,
       CB_G_TAB11           Codigo,
       CB_G_TAB12           Codigo,
       CB_G_TAB13           Codigo,
       CB_G_TAB14           Codigo,
       CB_G_TEX_1           Descripcion,
       CB_G_TEX_2           Descripcion,
       CB_G_TEX_3           Descripcion,
       CB_G_TEX_4           Descripcion,
       CB_G_TEX_5           Descripcion,
       CB_G_TEX_6           Descripcion,
       CB_G_TEX_7           Descripcion,
       CB_G_TEX_8           Descripcion,
       CB_G_TEX_9           Descripcion,
       CB_G_TEX10           Descripcion,
       CB_G_TEX11           Descripcion,
       CB_G_TEX12           Descripcion,
       CB_G_TEX13           Descripcion,
       CB_G_TEX14           Descripcion,
       CB_G_TEX15           Descripcion,
       CB_G_TEX16           Descripcion,
       CB_G_TEX17           Descripcion,
       CB_G_TEX18           Descripcion,
       CB_G_TEX19           Descripcion,
       CB_HABLA             Booleano,
       CB_TURNO             Codigo,
       CB_IDIOMA            Descripcion,
       CB_INFCRED           Descripcion,
       CB_INFMANT           Booleano,
       CB_INFTASA           DescINFO,
       CB_LA_MAT            Descripcion,
       CB_LAST_EV           Fecha,
       CB_LUG_NAC           Descripcion,
       CB_MAQUINA           Titulo,
       CB_MED_TRA           Codigo1,
       CB_PASAPOR           Booleano,
       CB_FEC_INC           Fecha,
       CB_FEC_PER           Fecha,
       CB_NOMYEAR           Anio,
       CB_NACION            Descripcion,
       CB_NOMTIPO           NominaTipo,
       CB_NEXT_EV           Fecha,
       CB_NOMNUME           NominaNumero,
       CB_NOMBRES           Descripcion,
       CB_PATRON            RegPatronal,
       CB_PUESTO            Codigo,
       CB_RFC               Descripcion,
       CB_SAL_INT           PesosDiario,
       CB_SALARIO           PesosDiario,
       CB_SEGSOC            Descripcion,
       CB_SEXO              Codigo1,
       CB_TABLASS           Codigo1,
       CB_TEL               Descripcion,
       CB_VIVECON           Codigo1,
       CB_VIVEEN            Codigo2,
       CB_ZONA              Referencia,
       CB_ZONA_GE           ZonaGeo,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_INFTIPO           Status,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       CB_DER_FEC           Fecha,
       CB_DER_PAG           DiasFrac,
       CB_V_PAGO            DiasFrac,
       CB_DER_GOZ           DiasFrac,
       CB_V_GOZO            DiasFrac,
       CB_TIP_REV           Codigo,
       CB_MOT_BAJ           Codigo3,
       CB_FEC_SAL           Fecha,
       CB_INF_INI           Fecha,
       CB_INF_OLD           DescINFO,
       CB_OLD_SAL           PesosDiario,
       CB_OLD_INT           PesosDiario,
       CB_PRE_INT           PesosDiario,
       CB_PER_VAR           PesosDiario,
       CB_SAL_TOT           PesosDiario,
       CB_TOT_GRA           PesosDiario,
       CB_FAC_INT           Tasa,
       CB_RANGO_S           Tasa,
       CB_CLINICA           Codigo3,
       CB_NIVEL0            Codigo,
       CB_FEC_NIV           Fecha,
       CB_FEC_PTO           Fecha,
       CB_AREA              Codigo,
       CB_FEC_TUR           Fecha,
       CB_FEC_KAR           Fecha,
       CB_PENSION           Status,
       CB_CANDIDA           FolioGrande,
       CB_ID_NUM            Descripcion,
       CB_ENT_NAC           Codigo2,
       CB_COD_COL           Codigo,
       CB_PASSWRD           Passwd,
       CB_PLAZA             FolioGrande,
       CB_FEC_PLA           Fecha,
       CB_DER_PV            DiasFrac,
       CB_V_PRIMA           DiasFrac,
       CB_SUB_CTA           Descripcion,
       CB_NETO              Pesos,
       CB_E_MAIL            Formula,
       CB_PORTAL            Booleano,
       CB_DNN_OK            Booleano,
       CB_USRNAME           DescLarga,
       CB_NOMINA            NominaTipo,
       CB_FEC_NOM           Fecha,
       CB_RECONTR           Booleano,
       CB_DISCAPA           Booleano,
       CB_INDIGE            Booleano,
       CB_FONACOT           Descripcion,
       CB_EMPLEO            Booleano,
       US_CODIGO 	          Usuario,
       CB_FEC_COV 		      Fecha,
       CB_G_TEX20 		      Descripcion,
       CB_G_TEX21 		      Descripcion,
       CB_G_TEX22           Descripcion,
       CB_G_TEX23           Descripcion,
       CB_G_TEX24           Descripcion,
	     CB_INFDISM 		      Booleano,
	     CB_INFACT            Booleano,
       CB_NUM_EXT 		      NombreCampo,
       CB_NUM_INT 		      NombreCampo,
       CB_INF_ANT           Fecha,
       CB_TDISCAP           Status,
       CB_ESCUELA           DescLarga,
       CB_TESCUEL           Status,
       CB_TITULO            Status,
       CB_YTITULO           Anio,
       CB_CTA_GAS           Descripcion,
       CB_CTA_VAL           Descripcion,
       CB_MUNICIP           Codigo,
	     CB_ID_BIO 	 		      FolioGrande,
	     CB_GP_COD 			      Codigo,
	     CB_TIE_PEN           Booleano,
       PRETTYNAME AS (CB_APE_PAT + ' ' + CB_APE_MAT + ', ' + CB_NOMBRES),
       CB_TSANGRE           Descripcion,
       CB_ALERGIA           Formula,
       CB_BRG_ACT           Booleano,
       CB_BRG_TIP           Status,
       CB_BRG_ROL           Booleano,
       CB_BRG_JEF           Booleano,
       CB_BRG_CON           Booleano,
       CB_BRG_PRA           Booleano,
       CB_BRG_NOP           Descripcion,
       CB_BANCO             Codigo, 
       CB_REGIMEN			Status,
       CB_MIFARE            Booleano not null
)
go

ALTER TABLE COLABORA
       ADD CONSTRAINT PK_COLABORA PRIMARY KEY (CB_CODIGO)
go

create index XIE1COLABORA on COLABORA( CB_PUESTO )
GO

create index XIE2COLABORA on COLABORA( CB_ID_NUM )
GO

CREATE TABLE TOOL (
       TO_CODIGO            Codigo,
       TO_DESCRIP           Descripcion,
       TO_INGLES            Descripcion,
       TO_NUMERO            Pesos,
       TO_TEXTO             Descripcion,
       TO_COSTO             Pesos,
       TO_VAL_REP           Pesos,
       TO_VAL_BAJ           Pesos,
       TO_VIDA              Dias,
       TO_DESCTO            Status
)
go

ALTER TABLE TOOL
       ADD CONSTRAINT PK_TOOL PRIMARY KEY (TO_CODIGO)
go

CREATE TABLE KAR_TOOL (
       CB_CODIGO            NumeroEmpleado,
       KT_FEC_INI           Fecha,
       TO_CODIGO            Codigo,
       KT_REFEREN           Mascara,
       KT_MOT_FIN           Codigo,
       KT_TALLA             Codigo,
       KT_ACTIVO            Booleano,
       KT_COMENTA           Observaciones,
       KT_FEC_FIN           Fecha,
       US_CODIGO            Usuario
)
go


ALTER TABLE KAR_TOOL
       ADD CONSTRAINT PK_KAR_TOOL PRIMARY KEY (CB_CODIGO, KT_FEC_INI, TO_CODIGO, KT_REFEREN)
go

CREATE TABLE TMPNOM (
       TA_USER              Usuario,
       TA_NIVEL1            Codigo,
       TA_NIVEL2            Codigo,
       TA_NIVEL3            Codigo,
       TA_NIVEL4            Codigo,
       TA_NIVEL5            Codigo,
       CB_CODIGO            NumeroEmpleado,
       CB_SAL_INT           PesosEmpresa,
       CB_SALARIO           PesosEmpresa,
       NO_DIAS              PesosEmpresa,
       NO_ADICION           PesosEmpresa,
       TA_TOTAL             NumeroEmpleado,
       NO_DEDUCCI           PesosEmpresa,
       NO_NETO              PesosEmpresa,
       NO_DES_TRA           PesosEmpresa,
       NO_DIAS_AG           PesosEmpresa,
       NO_DIAS_VA           PesosEmpresa,
       NO_DOBLES            PesosEmpresa,
       NO_EXTRAS            PesosEmpresa,
       NO_FES_PAG           PesosEmpresa,
       NO_FES_TRA           PesosEmpresa,
       NO_HORA_CG           PesosEmpresa,
       NO_HORA_SG           PesosEmpresa,
       NO_HORAS             PesosEmpresa,
       NO_IMP_CAL           PesosEmpresa,
       NO_JORNADA           PesosEmpresa,
       NO_PER_CAL           PesosEmpresa,
       NO_PER_MEN           PesosEmpresa,
       NO_PERCEPC           PesosEmpresa,
       NO_TARDES            PesosEmpresa,
       NO_TRIPLES           PesosEmpresa,
       NO_TOT_PRE           PesosEmpresa,
       NO_VAC_TRA           PesosEmpresa,
       NO_X_CAL             PesosEmpresa,
       NO_X_ISPT            PesosEmpresa,
       NO_X_MENS            PesosEmpresa,
       NO_D_TURNO           PesosEmpresa,
       NO_DIAS_AJ           PesosEmpresa,
       NO_DIAS_AS           PesosEmpresa,
       NO_DIAS_CG           PesosEmpresa,
       NO_DIAS_EM           PesosEmpresa,
       NO_DIAS_FI           PesosEmpresa,
       NO_DIAS_FJ           PesosEmpresa,
       NO_DIAS_FV           PesosEmpresa,
       NO_DIAS_IN           PesosEmpresa,
       NO_DIAS_NT           PesosEmpresa,
       NO_DIAS_OT           PesosEmpresa,
       NO_DIAS_RE           PesosEmpresa,
       NO_DIAS_SG           PesosEmpresa,
       NO_DIAS_SS           PesosEmpresa,
       NO_DIAS_SU           PesosEmpresa,
       NO_HORA_PD           PesosEmpresa,
       NO_EXENTAS           PesosEmpresa,
       NO_DIAS_BA           DiasFrac,
       NO_DIAS_SI           DiasFrac,
       NO_HORASNT           Horas,
       NO_HORAPDT           Horas,
       NO_HOR_OK            Hora,
       NO_PREV_GR           Pesos,
       NO_DIAS_VJ           DiasFrac,
       NO_DIAS_PV           DiasFrac,
       NO_PRE_EXT           PesosEmpresa,
       NO_PER_ISN           Pesos,
       NO_DIAS_IH           PesosEmpresa,
       NO_DIAS_ID           PesosEmpresa,
       NO_DIAS_IT           PesosEmpresa
)
go


ALTER TABLE TMPNOM
       ADD CONSTRAINT PK_TMPNOM PRIMARY KEY (TA_USER, TA_NIVEL1, TA_NIVEL2, TA_NIVEL3, 
              TA_NIVEL4, TA_NIVEL5, CB_CODIGO)
go

CREATE TABLE TMPLISTA (
       TA_USER              Usuario,
       TA_NIVEL1            Codigo,
       TA_NIVEL2            Codigo,
       TA_NIVEL3            Codigo,
       TA_NIVEL4            Codigo,
       TA_NIVEL5            Codigo,
       CB_CODIGO            NumeroEmpleado,
       CO_NUMERO            Concepto,
       TA_REFEREN           Referencia,
       TA_PERCEPC           PesosEmpresa,
       TA_DEDUCCI           PesosEmpresa,
       TA_TOTAL             NumeroEmpleado,
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       TA_HORAS             Pesos
)
go


ALTER TABLE TMPLISTA
       ADD CONSTRAINT PK_TMPLISTA PRIMARY KEY (TA_USER, TA_NIVEL1, TA_NIVEL2, TA_NIVEL3, 
              TA_NIVEL4, TA_NIVEL5, CB_CODIGO, CO_NUMERO, TA_REFEREN)
go

CREATE TABLE TMPCALEN (
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       TL_YEAR              Anio,
       TL_MES               Mes,
       TL_FEC_MIN           Fecha,
       TL_FEC_MAX           Fecha,
       TL_NUMERO            Dias,
       TL_FECHA01           Fecha,
       TL_FECHA02           Fecha,
       TL_FECHA03           Fecha,
       TL_FECHA04           Fecha,
       TL_FECHA05           Fecha,
       TL_FECHA06           Fecha,
       TL_FECHA07           Fecha,
       TL_FECHA08           Fecha,
       TL_FECHA09           Fecha,
       TL_FECHA10           Fecha,
       TL_FECHA11           Fecha,
       TL_FECHA12           Fecha,
       TL_FECHA13           Fecha,
       TL_FECHA14           Fecha,
       TL_FECHA15           Fecha,
       TL_FECHA16           Fecha,
       TL_FECHA17           Fecha,
       TL_FECHA18           Fecha,
       TL_FECHA19           Fecha,
       TL_FECHA20           Fecha,
       TL_FECHA21           Fecha,
       TL_FECHA22           Fecha,
       TL_FECHA23           Fecha,
       TL_FECHA24           Fecha,
       TL_FECHA25           Fecha,
       TL_FECHA26           Fecha,
       TL_FECHA27           Fecha,
       TL_FECHA28           Fecha,
       TL_FECHA29           Fecha,
       TL_FECHA30           Fecha,
       TL_FECHA31           Fecha,
       TL_HABIL             Dias,
       TL_SABADO            Dias,
       TL_DESCAN            Dias,
       TL_NORMAL            Dias,
       TL_INCAPA            Dias,
       TL_VACA              Dias,
       TL_PER_CG            Dias,
       TL_PER_SG            Dias,
       TL_PER_FJ            Dias,
       TL_SUSPEN            Dias,
       TL_PER_OT            Dias,
       TL_FESTIVO           Dias,
       TL_NO_TRAB           Dias,
       TL_TRABAJO           Dias,
       TL_FALTAS            Dias,
       TL_RETARDO           Dias,
       TL_HORAS             Horas,
       TL_EXTRAS            Horas,
       TL_DOBLES            Horas,
       TL_DES_TRA           Horas,
       TL_HOR_CG            Horas,
       TL_HOR_SG            Horas,
       TL_TARDES            Horas
)
go


ALTER TABLE TMPCALEN
       ADD CONSTRAINT PK_TMPCALEN PRIMARY KEY (US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES)
go

CREATE TABLE FOLIO (
       FL_CODIGO            FolioChico,
       FL_DESCRIP           Descripcion,
       FL_REPORTE           FolioChico,
       FL_MONTO             Formula,
       FL_REPITE            Booleano,
       FL_MONEDA            Pesos,
       FL_CEROS             Booleano,
       FL_INICIAL           Empleados,
       FL_FINAL             Empleados,
       FL_FILTRO            Formula,
       QU_CODIGO            Condicion
)
go


ALTER TABLE FOLIO
       ADD CONSTRAINT PK_FOLIO PRIMARY KEY (FL_CODIGO)
go

CREATE TABLE ORDFOLIO (
       FL_CODIGO            FolioChico,
       OF_POSICIO           FolioChico,
       OF_CAMPO             Descripcion,
       OF_TITULO            TituloCampo,
       OF_DESCEND           Booleano
)
go


ALTER TABLE ORDFOLIO
       ADD CONSTRAINT PK_ORDFOLIO PRIMARY KEY (FL_CODIGO, OF_POSICIO)
go

CREATE TABLE CONTEO (
       CT_FECHA             Fecha,
       CT_NIVEL_1           Codigo,
       CT_NIVEL_2           Codigo,
       CT_NIVEL_3           Codigo,
       CT_NIVEL_4           Codigo,
       CT_NIVEL_5           Codigo,
       CT_NUMERO1           PesosEmpresa,
       CT_NUMERO2           PesosEmpresa,
       CT_NUMERO3           PesosEmpresa,
       CT_TEXTO1            Descripcion,
       CT_TEXTO2            Descripcion,
       CT_REAL              PesosEmpresa,
       CT_CUANTOS           Empleados
)
go


ALTER TABLE CONTEO
       ADD CONSTRAINT PK_CONTEO PRIMARY KEY (CT_FECHA, CT_NIVEL_1, CT_NIVEL_2, CT_NIVEL_3, 
              CT_NIVEL_4, CT_NIVEL_5)
go

CREATE TABLE MOT_AUTO (
       TB_CODIGO            Codigo4,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
go


ALTER TABLE MOT_AUTO
       ADD CONSTRAINT PK_MOT_AUTO PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE TCAMBIO (
       TC_FEC_INI           Fecha,
       TC_MONTO             Tasa,
       TC_NUMERO            Pesos,
       TC_TEXTO             Descripcion
)
go


ALTER TABLE TCAMBIO
       ADD CONSTRAINT PK_TCAMBIO PRIMARY KEY (TC_FEC_INI)
go

CREATE TABLE SAL_MIN (
       SM_FEC_INI           Fecha,
       SM_ZONA_A            PesosDiario,
       SM_ZONA_B            PesosDiario,
       SM_ZONA_C            PesosDiario
)
go


ALTER TABLE SAL_MIN
       ADD CONSTRAINT PK_SAL_MIN PRIMARY KEY (SM_FEC_INI)
go

CREATE TABLE REP_PTU (
       RU_YEAR              Anio,
       CB_CODIGO            NumeroEmpleado,
       RU_DIAS              Dias,
       RU_M_DIAS            Pesos,
       RU_MONTO             Pesos,
       RU_M_MONTO           Pesos,
       US_CODIGO            Usuario
)
go


ALTER TABLE REP_PTU
       ADD CONSTRAINT PK_REP_PTU PRIMARY KEY (RU_YEAR, CB_CODIGO)
go

CREATE TABLE TMPDEMO (
       TD_USER              Usuario,
       TD_GRUPO             Codigo,
       TD_DESCRIP           Descripcion,
       TD_EMPL              Empleados,
       TD_HOMBRES           Empleados,
       TD_MUJERES           Empleados,
       TD_FAMILIA           Empleados,
       TD_MICA              Empleados,
       TD_ESTUDIO           Empleados,
       TD_IDIOMA            Empleados,
       TD_TABULA            Empleados,
       TD_EDADMIN           DiasFrac,
       TD_EDADMAX           DiasFrac,
       TD_EDADAVG           DiasFrac,
       TD_ANTMIN            DiasFrac,
       TD_ANTMAX            DiasFrac,
       TD_ANTAVG            DiasFrac,
       TD_RESMIN            DiasFrac,
       TD_RESMAX            DiasFrac,
       TD_RESAVG            DiasFrac,
       TD_HIJOMIN           DiasFrac,
       TD_HIJOMAX           DiasFrac,
       TD_HIJOAVG           DiasFrac,
       TD_SALMIN            PesosDiario,
       TD_SALMAX            PesosDiario,
       TD_SALAVG            PesosDiario,
       TD_DIASMIN           DiasFrac,
       TD_DIASMAX           DiasFrac,
       TD_DIASAVG           DiasFrac,
       TD_EMPMIN            NumeroEmpleado,
       TD_EMPMAX            NumeroEmpleado
)
go

ALTER TABLE TMPDEMO
       ADD CONSTRAINT PK_TMPDEMO PRIMARY KEY (TD_USER, TD_GRUPO)
go

CREATE TABLE TAHORRO (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_CONCEPT           Concepto,
       TB_RELATIV           Concepto,
       TB_LIQUIDA           Status,
       TB_ALTA              Status,
       TB_PRIORID           Status,
       TB_PRESTA            Codigo1,
       TB_TASA1             Tasa,
       TB_TASA2             Tasa,
       TB_TASA3             Tasa,
       TB_FEC_INI 		      Fecha,
       TB_FEC_FIN 		      Fecha,
       TB_VAL_RAN 		      Booleano,
       TB_ACTIVO            Booleano,
       TB_NIVEL0            Formula
)
go

ALTER TABLE TAHORRO
       ADD CONSTRAINT PK_TAHORRO PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE REP_AHO (
       RA_YEAR              Anio,
       TB_CODIGO            Codigo1,
       CB_CODIGO            NumeroEmpleado,
       RA_AHORRO            Pesos,
       RA_REPARTO           Pesos,
       US_CODIGO            Usuario
)
go


ALTER TABLE REP_AHO
       ADD CONSTRAINT PK_REP_AHO PRIMARY KEY (RA_YEAR, TB_CODIGO, CB_CODIGO)
go

CREATE TABLE AGUINAL (
       AG_YEAR              Anio,
       CB_CODIGO            NumeroEmpleado,
       AG_FEC_ING           Fecha,
       AG_BASE              Dias,
       AG_FALTAS            Dias,
       AG_INCAPA            Dias,
       AG_NETOS             Dias,
       AG_DIAS_AG           DiasFrac,
       AG_SALARIO           PesosDiario,
       AG_AGUINAL           Pesos,
       US_CODIGO            Usuario
)
go


ALTER TABLE AGUINAL
       ADD CONSTRAINT PK_AGUINAL PRIMARY KEY (AG_YEAR, CB_CODIGO)
go

CREATE TABLE KARDEX (
       CB_CODIGO            NumeroEmpleado,
       CB_FECHA             Fecha,
       CB_TIPO              Codigo,
       CB_AUTOSAL           Booleano,
       CB_CLASIFI           Codigo,
       CB_COMENTA           Observaciones,
       CB_CONTRAT           Codigo1,
       CB_FAC_INT           Tasa,
       CB_FEC_CAP           Fecha,
       CB_FEC_CON           Fecha,
       CB_FEC_INT           Fecha,
       CB_FEC_REV           Fecha,
       CB_FECHA_2           Fecha,
       CB_GLOBAL            Booleano,
       CB_TURNO             Codigo,
       CB_MONTO             Pesos,
       CB_MOT_BAJ           Codigo3,
       CB_NIVEL             Status,
       CB_OLD_INT           PesosDiario,
       CB_OLD_SAL           PesosDiario,
       CB_OTRAS_P           PesosDiario,
       CB_PATRON            RegPatronal,
       CB_PER_VAR           PesosDiario,
       CB_PRE_INT           PesosDiario,
       CB_PUESTO            Codigo,
       CB_RANGO_S           Tasa,
       CB_SAL_INT           PesosDiario,
       CB_SAL_TOT           PesosDiario,
       CB_SALARIO           PesosDiario,
       CB_STATUS            Status,
       CB_TABLASS           Codigo1,
       CB_TOT_GRA           PesosDiario,
       US_CODIGO            Usuario,
       CB_ZONA_GE           ZonaGeo,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       CB_MOD_NV1           Booleano,
       CB_MOD_NV2           Booleano,
       CB_MOD_NV3           Booleano,
       CB_MOD_NV4           Booleano,
       CB_MOD_NV5           Booleano,
       CB_MOD_NV6           Booleano,
       CB_MOD_NV7           Booleano,
       CB_MOD_NV8           Booleano,
       CB_MOD_NV9           Booleano,
       CB_NOMTIPO           NominaTipo,
       CB_NOMYEAR           Anio,
       CB_NOMNUME           NominaNumero,
       CB_REINGRE           Booleano,
       CB_NOTA              Memo,
       CB_FEC_ING           Fecha,
       CB_FEC_ANT           Fecha,
       CB_PLAZA             FolioGrande,
       CB_NOMINA            NominaTipo,
       CB_RECONTR           Booleano,
       CB_FEC_COV 	        Fecha,
       CB_TIP_REV as ( case when ( CB_FEC_REV = CB_FEC_ING ) then 'ALTA' else 'CAMBIO' end ),
       CB_FEC_SAL as ( case when ( CB_FEC_REV > CB_FEC_INT ) then CB_FEC_REV else CB_FEC_INT end ),
       CB_LOT_IDS           DescLarga,
       CB_FEC_IDS           Fecha
)
go

ALTER TABLE KARDEX
       ADD CONSTRAINT PK_KARDEX PRIMARY KEY (CB_CODIGO, CB_FECHA, CB_TIPO)
go

CREATE INDEX XIE1KARDEX ON KARDEX
(
       CB_CODIGO,
       CB_FECHA,
       CB_NIVEL
)
go

CREATE INDEX XIE2KARDEX ON KARDEX
(
       CB_TIPO,
       CB_FECHA
)
go


CREATE TABLE TMPHORAS (
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       CO_NUMERO            Concepto,
       MO_MONTO             Pesos
)
go


ALTER TABLE TMPHORAS
       ADD CONSTRAINT PK_TMPHORAS PRIMARY KEY (US_CODIGO, CB_CODIGO, CO_NUMERO)
go

CREATE TABLE TMPEVENT (
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       TV_FECHA             Fecha,
       PRETTYNAME           DescLarga,
       CB_FEC_ING           Fecha,
       CB_ANTIG             Dias,
       CB_ACTIVO            Booleano,
       TV_COMENT            Observaciones,
       TV_PRIORID           Status,
       EV_CODIGO            Evento,
       TV_PUESTO            Codigo,
       TV_CLASIFI           Codigo,
       TV_TABLASS           Codigo1,
       TV_CONTRAT           Codigo,
       TV_TURNO             Codigo,
       TV_NIVEL1            Codigo,
       TV_NIVEL2            Codigo,
       TV_NIVEL3            Codigo,
       TV_NIVEL4            Codigo,
       TV_NIVEL5            Codigo,
       TV_NIVEL6            Codigo,
       TV_NIVEL7            Codigo,
       TV_NIVEL8            Codigo,
       TV_NIVEL9            Codigo,
       TV_SALARIO           PesosDiario,
       TV_OTRAS_1           Codigo2,
       TV_OTRAS_2           Codigo2,
       TV_OTRAS_3           Codigo2,
       TV_OTRAS_4           Codigo2,
       TV_OTRAS_5           Codigo2,
       TV_KARDEX            Codigo,
       TV_MONTO             Pesos,
       TV_AUTOSAL           Booleano,
       TV_CAMPO             NombreCampo,
       TV_VALOR             Formula,
       TV_ALTA              Booleano,
       TV_BAJA              Booleano,
       TV_MOT_BAJ           Codigo3,
       TV_NOMYEAR           Anio,
       TV_NOMTIPO           NominaTipo,
       TV_NOMNUME           NominaNumero,
       TV_FEC_BSS           Fecha,
       TV_PATRON            RegPatronal,
       TV_FEC_ANT           Fecha,
       TV_FEC_CON           Fecha
)
go


ALTER TABLE TMPEVENT
       ADD CONSTRAINT PK_TMPEVENT PRIMARY KEY (US_CODIGO, CB_CODIGO, TV_FECHA)
go

CREATE TABLE NOMPARAM (
       NP_FOLIO             FolioChico,
       NP_NOMBRE            NombreCampo,
       NP_FORMULA           Memo,
       NP_TIPO              Status,
       NP_DESCRIP           Descripcion,
       NP_ACTIVO            Booleano
)
go


ALTER TABLE NOMPARAM
       ADD CONSTRAINT PK_NOMPARAM PRIMARY KEY (NP_FOLIO)
go

CREATE TABLE PROCESO (
       PC_PROC_ID           Status,
       PC_NUMERO            FolioGrande,
       US_CODIGO            Usuario,
       PC_FEC_INI           Fecha,
       PC_HOR_INI           HoraMinutosSegundos,
       PC_FEC_FIN           Fecha,
       PC_HOR_FIN           HoraMinutosSegundos,
       PC_ERROR             Booleano,
       CB_CODIGO            NumeroEmpleado,
       PC_MAXIMO            FolioGrande,
       PC_PASO              FolioGrande,
       US_CANCELA           Usuario,
       PC_PARAMS            Formula
)
go

CREATE INDEX XIE1PROCESO ON PROCESO
(
       PC_FEC_INI,
       PC_ERROR
)
go


ALTER TABLE PROCESO
       ADD CONSTRAINT PK_PROCESO PRIMARY KEY (PC_NUMERO)
go

CREATE TABLE PERIODO (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PE_DESCRIP           Descripcion,
       PE_USO               Status,
       PE_STATUS            Status,
       PE_INC_BAJ           Booleano,
       PE_SOLO_EX           Booleano,
       PE_FEC_INI           Fecha,
       PE_FEC_FIN           Fecha,
       PE_FEC_PAG           Fecha,
       PE_MES               Mes,
       PE_DIAS              Dias,
       PE_DIAS_AC           Dias,
       PE_DIA_MES           Dias,
       PE_POS_MES           Dias,
       PE_PER_MES           Dias,
       PE_PER_TOT           Dias,
       PE_FEC_MOD           Fecha,
       PE_AHORRO            Booleano,
       PE_PRESTAM           Booleano,
       PE_NUM_EMP           Empleados,
       PE_TOT_PER           PesosEmpresa,
       PE_TOT_NET           PesosEmpresa,
       PE_TOT_DED           PesosEmpresa,
       US_CODIGO            Usuario,
       PE_CANDADO           Status,
       PE_LOG               Formula,
       PE_ASI_INI           Fecha,
       PE_ASI_FIN           Fecha,
       PE_CAL               Booleano,
       PE_TIMBRO            Status,
	   PE_MES_FON			Mes
)
go

CREATE UNIQUE INDEX XAK1PERIODO ON PERIODO
(
       PE_YEAR,
       PE_TIPO,
       PE_MES,
       PE_NUMERO
)
go


ALTER TABLE PERIODO
       ADD CONSTRAINT PK_PERIODO PRIMARY KEY (PE_YEAR, PE_TIPO, PE_NUMERO)
go

CREATE TABLE VACACION (
       CB_CODIGO            NumeroEmpleado,
       VA_FEC_INI           Fecha,
       VA_TIPO              Status,
       VA_FEC_FIN           Fecha,
       VA_COMENTA           Titulo,
       VA_CAPTURA           Fecha,
       US_CODIGO            Usuario,
       VA_D_PAGO            DiasFrac,
       VA_PAGO              DiasFrac,
       VA_S_PAGO            DiasFrac,
       VA_D_GOZO            DiasFrac,
       VA_GOZO              DiasFrac,
       VA_S_GOZO            DiasFrac,
       CB_SALARIO           PesosDiario,
       CB_TABLASS           Codigo1,
       VA_NOMYEAR           Anio,
       VA_NOMTIPO           NominaTipo,
       VA_NOMNUME           NominaNumero,
       VA_YEAR              Anio,
       VA_MONTO             Pesos,
       VA_SEVEN             Pesos,
       VA_TASA_PR           Tasa,
       VA_PRIMA             Pesos,
       VA_OTROS             Pesos,
       VA_TOTAL             Pesos,
       VA_PERIODO           Descripcion,
       VA_GLOBAL            Booleano,
       VA_D_PRIMA           DiasFrac,
       VA_P_PRIMA           DiasFrac,
       VA_S_PRIMA           DiasFrac,
       VA_AJUSTE            Booleano
)
go

CREATE INDEX XIE1VACACION ON VACACION
(
       VA_TIPO,
       VA_NOMYEAR,
       VA_NOMTIPO,
       CB_CODIGO,
       VA_NOMNUME
)
go


ALTER TABLE VACACION
       ADD CONSTRAINT PK_VACACION PRIMARY KEY (CB_CODIGO, VA_FEC_INI, VA_TIPO, US_CODIGO)
go

CREATE TABLE GLOBAL (
       GL_CODIGO            FolioChico,
       GL_DESCRIP           Descripcion,
       GL_FORMULA           Formula,
       GL_TIPO              Status,
       US_CODIGO            Usuario,
       GL_CAPTURA           Fecha,
       GL_NIVEL             Status
)
go


ALTER TABLE GLOBAL
       ADD CONSTRAINT PK_GLOBAL PRIMARY KEY (GL_CODIGO)
go

CREATE TABLE ANTESCUR (
       CB_CODIGO            NumeroEmpleado,
       AR_FOLIO             FolioChico,
       AR_CURSO             DescLarga,
       AR_FECHA             Fecha,
       AR_LUGAR             DescLarga
)
go


ALTER TABLE ANTESCUR
       ADD CONSTRAINT PK_ANTESCUR PRIMARY KEY (CB_CODIGO, AR_FOLIO)
go

CREATE TABLE ANTESPTO (
       CB_CODIGO            NumeroEmpleado,
       AP_FOLIO             FolioChico,
       AP_PUESTO            DescLarga,
       AP_EMPRESA           DescLarga,
       AP_FEC_INI           Fecha,
       AP_FEC_FIN           Fecha
)
go


ALTER TABLE ANTESPTO
       ADD CONSTRAINT PK_ANTESPTO PRIMARY KEY (CB_CODIGO, AP_FOLIO)
go

CREATE TABLE PARIENTE (
       CB_CODIGO            NumeroEmpleado,
       PA_RELACIO           Status,
       PA_FOLIO             FolioChico,
       PA_NOMBRE            Titulo,
       PA_SEXO              Codigo1,
       PA_FEC_NAC           Fecha,
       PA_TRABAJA           NumeroEmpleado,
       PA_NUMERO            Pesos,
       PA_TEXTO             Descripcion, 
       PA_APE_PAT 	    	Descripcion,
       PA_APE_MAT 	    	Descripcion,
       PA_NOMBRES 	    	Descripcion
)
go


ALTER TABLE PARIENTE
       ADD CONSTRAINT PK_PARIENTE PRIMARY KEY (CB_CODIGO, PA_RELACIO, PA_FOLIO)
go

CREATE TABLE KAR_FIJA (
       CB_CODIGO            NumeroEmpleado,
       CB_FECHA             Fecha,
       CB_TIPO              Codigo,
       KF_FOLIO             FolioChico,
       KF_CODIGO            Codigo2,
       KF_MONTO             PesosDiario,
       KF_GRAVADO           PesosDiario,
       KF_IMSS              Status
)
go


ALTER TABLE KAR_FIJA
       ADD CONSTRAINT PK_KAR_FIJA PRIMARY KEY (CB_CODIGO, CB_FECHA, CB_TIPO, KF_FOLIO)
go

CREATE TABLE TCURSO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go

ALTER TABLE TCURSO
       ADD CONSTRAINT PK_TCURSO PRIMARY KEY (TB_CODIGO)
go

create table CCURSO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE CCURSO
       ADD CONSTRAINT PK_CCURSO PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE ENTIDAD (
       TB_CODIGO            Codigo2,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_CURP              Codigo2,
       TB_STPS              FolioChico,
       TB_NIVEL0            Formula
)
go

ALTER TABLE ENTIDAD
       ADD CONSTRAINT PK_ENTIDAD PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE ESTUDIOS (
       TB_CODIGO            Codigo2,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status       
)
go


ALTER TABLE ESTUDIOS
       ADD CONSTRAINT PK_ESTUDIOS PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE TRANSPOR (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE TRANSPOR
       ADD CONSTRAINT PK_TRANSPOR PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE VIVE_EN (
       TB_CODIGO            Codigo2,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE VIVE_EN
       ADD CONSTRAINT PK_VIVE_EN PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE VIVE_CON (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE VIVE_CON
       ADD CONSTRAINT PK_VIVE_CON PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE EDOCIVIL (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE EDOCIVIL
       ADD CONSTRAINT PK_EDOCIVIL PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE EXTRA4 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE EXTRA4
       ADD CONSTRAINT PK_EXTRA4 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE EXTRA3 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE EXTRA3
       ADD CONSTRAINT PK_EXTRA3 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE EXTRA2 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE EXTRA2
       ADD CONSTRAINT PK_EXTRA2 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE EXTRA1 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go


ALTER TABLE EXTRA1
       ADD CONSTRAINT PK_EXTRA1 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NOMINA (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       CB_CODIGO            NumeroEmpleado,
       CB_CLASIFI           Codigo,
       CB_TURNO             Codigo,
       CB_PATRON            RegPatronal,
       CB_PUESTO            Codigo,
       CB_ZONA_GE           ZonaGeo,
       NO_FOLIO_1           Empleados,
       NO_FOLIO_2           Empleados,
       NO_FOLIO_3           Empleados,
       NO_FOLIO_4           Empleados,
       NO_FOLIO_5           Empleados,
       NO_OBSERVA           Descripcion,
       NO_STATUS            Status,
       US_CODIGO            Usuario,
       NO_USER_RJ           Usuario,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       CB_SAL_INT           PesosDiario,
       CB_SALARIO           PesosDiario,
       NO_DIAS              DiasFrac,
       NO_ADICION           Horas,
       NO_DEDUCCI           Pesos,
       NO_NETO              Pesos,
       NO_DES_TRA           Horas,
       NO_DIAS_AG           DiasFrac,
       NO_DIAS_VA           DiasFrac,
       NO_DOBLES            Horas,
       NO_EXTRAS            Horas,
       NO_FES_PAG           Horas,
       NO_FES_TRA           Horas,
       NO_HORA_CG           Horas,
       NO_HORA_SG           Horas,
       NO_HORAS             Horas,
       NO_IMP_CAL           Pesos,
       NO_JORNADA           Horas,
       NO_PER_CAL           Pesos,
       NO_PER_MEN           Pesos,
       NO_PERCEPC           Pesos,
       NO_TARDES            Horas,
       NO_TRIPLES           Horas,
       NO_TOT_PRE           Pesos,
       NO_VAC_TRA           Horas,
       NO_X_CAL             Pesos,
       NO_X_ISPT            Pesos,
       NO_X_MENS            Pesos,
       NO_D_TURNO           Dias,
       NO_DIAS_AJ           Dias,
       NO_DIAS_AS           Dias,
       NO_DIAS_CG           Dias,
       NO_DIAS_EM           Dias,
       NO_DIAS_FI           Dias,
       NO_DIAS_FJ           Dias,
       NO_DIAS_FV           Dias,
       NO_DIAS_IN           Dias,
       NO_DIAS_NT           Dias,
       NO_DIAS_OT           Dias,
       NO_DIAS_RE           Dias,
       NO_DIAS_SG           Dias,
       NO_DIAS_SS           Dias,
       NO_DIAS_SU           Dias,
       NO_HORA_PD           Horas,
       NO_LIQUIDA           Status,
       NO_FUERA             Booleano,
       NO_EXENTAS           Horas,
       NO_FEC_PAG           Fecha,
       NO_USR_PAG           Usuario,
       NO_HORASNT           Horas,
       CB_NIVEL0            Codigo,
       CB_BAN_ELE           Descripcion,
       NO_HORAPDT           Horas,
       NO_DIAS_SI           DiasFrac,
       NO_SUP_OK            Usuario,
       NO_FEC_OK            Fecha,
       NO_HOR_OK            Hora,
       NO_ASI_INI           Fecha,
       NO_ASI_FIN           Fecha,
       NO_DIAS_BA           Tasa,
       NO_FEC_LIQ           Fecha,
       CB_PLAZA             FolioGrande,
       NO_DIAS_VJ           DiasFrac,
       NO_PREV_GR           Pesos,
       NO_DIAS_PV           DiasFrac,
       NO_PRE_EXT           Horas,
	     LLAVE				        FolioGrande IDENTITY(1,1) NOT NULL,
       NO_DIAS_PE 		      Dias,
       NO_GLOBAL            Booleano,
	     NO_APROBA            Status,
       NO_RASTREO           Memo,
       CB_CTA_GAS           Descripcion,
       CB_CTA_VAL           Descripcion,
       NO_PER_ISN           Pesos, 
	     NO_DIAS_VT 			    Dias,
	     NO_DIAS_VC 			    Dias,
       NO_DIAS_IH           Dias,
       NO_DIAS_ID           Dias,
       NO_DIAS_IT           Dias,
       NO_TOTAL_1	          Pesos,
       NO_TOTAL_2	          Pesos,
       NO_TOTAL_3	          Pesos,
       NO_TOTAL_4	          Pesos,
       NO_TOTAL_5	          Pesos,
       NO_FACT_BA	          Tasa,
       NO_DIAS_DT	          Dias,
       NO_DIAS_FT	          Dias,
       NO_DIAS_FS	          Dias,
       NO_TIMBRO            Status,
       NO_SELLO             Formula,
       NO_FACTURA           FolioGrande, 
       NO_PAGO				Status, 
       NO_FACUUID	    DescLarga,
	   NO_CAN_UID			DescLarga,
	   CB_TABLASS			Codigo1,
	   NO_VER_CON			FolioGrande,
	   NO_VER_TIM			FolioGrande
)
GO

ALTER TABLE NOMINA
       ADD CONSTRAINT PK_NOMINA PRIMARY KEY NONCLUSTERED (PE_YEAR, PE_TIPO, PE_NUMERO,
              CB_CODIGO)

go

create index IDX_NOMINA_NO_FACTURA on NOMINA ( NO_FACTURA ASC ) 
GO 

CREATE INDEX IE_NOMINA_LLAVE ON NOMINA (LLAVE )
GO

CREATE TABLE MOVIMIEN (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       CB_CODIGO            NumeroEmpleado,
       MO_ACTIVO            Booleano,
       CO_NUMERO            Concepto,
       MO_IMP_CAL           Pesos,
       US_CODIGO            Usuario,
       MO_PER_CAL           Booleano,
       MO_REFEREN           Referencia,
       MO_X_ISPT            Pesos,
       MO_PERCEPC           Pesos,
       MO_DEDUCCI           Pesos
)
go


ALTER TABLE MOVIMIEN
       ADD CONSTRAINT PK_MOVIMIEN PRIMARY KEY (PE_YEAR, PE_TIPO, PE_NUMERO, 
              CB_CODIGO, CO_NUMERO, MO_REFEREN)
go

CREATE TABLE LIQ_IMSS (
       LS_ACT_AMO           PesosEmpresa,
       LS_ACT_APO           PesosEmpresa,
       LS_ACT_IMS           PesosEmpresa,
       LS_ACT_INF           PesosEmpresa,
       LS_ACT_RET           PesosEmpresa,
       LS_APO_VOL           PesosEmpresa,
       LS_CES_VEJ           PesosEmpresa,
       LS_DIAS_BM           Empleados,
       LS_DIAS_CO           Empleados,
       LS_EYM_DIN           PesosEmpresa,
       LS_EYM_ESP           PesosEmpresa,
       LS_EYM_EXC           PesosEmpresa,
       LS_EYM_FIJ           PesosEmpresa,
       LS_FAC_ACT           Tasa,
       LS_FAC_REC           Tasa,
       LS_FEC_REC           Fecha,
       LS_GUARDER           PesosEmpresa,
       LS_INF_ACR           PesosEmpresa,
       LS_INF_AMO           PesosEmpresa,
       LS_INF_NAC           PesosEmpresa,
       LS_INF_NUM           Empleados,
       LS_INV_VID           PesosEmpresa,
       LS_NUM_BIM           Empleados,
       LS_NUM_TRA           Empleados,
       LS_PATRON            RegPatronal,
       LS_REC_AMO           PesosEmpresa,
       LS_REC_APO           PesosEmpresa,
       LS_REC_IMS           PesosEmpresa,
       LS_REC_INF           PesosEmpresa,
       LS_REC_RET           PesosEmpresa,
       LS_RETIRO            PesosEmpresa,
       LS_RIESGOS           PesosEmpresa,
       LS_STATUS            Status,
       LS_YEAR              Anio,
       LS_MONTH             Mes,
       LS_TIPO              TipoLiq,
       LS_SUB_IMS           PesosEmpresa,
       LS_TOT_IMS           PesosEmpresa,
       LS_TOT_MES           PesosEmpresa,
       LS_SUB_RET           PesosEmpresa,
       LS_TOT_RET           PesosEmpresa,
       LS_SUB_INF           PesosEmpresa,
       LS_TOT_INF           PesosEmpresa,
       US_CODIGO            Usuario,
       LS_IMSS_OB           PesosEmpresa,
       LS_IMSS_PA           PesosEmpresa,
       LS_PARCIAL           PesosEmpresa,
       LS_FINAN             Booleano,
       LS_DES_20            Booleano
)
go


ALTER TABLE LIQ_IMSS
       ADD CONSTRAINT PK_LIQ_IMSS PRIMARY KEY (LS_PATRON, LS_YEAR, LS_MONTH, 
              LS_TIPO)
go


CREATE TABLE LIQ_EMP (
       LE_APO_VOL           Pesos,
       LE_CES_VEJ           Pesos,
       LE_DIAS_BM           Dias,
       LE_DIAS_CO           Dias,
       LE_EYM_DIN           Pesos,
       CB_CODIGO            NumeroEmpleado,
       LE_EYM_ESP           Pesos,
       LE_EYM_EXC           Pesos,
       LE_EYM_FIJ           Pesos,
       LE_GUARDER           Pesos,
       LE_INF_AMO           Pesos,
       LE_INF_PAT           Pesos,
       LE_INV_VID           Pesos,
       LE_RETIRO            Pesos,
       LE_RIESGOS           Pesos,
       LE_STATUS            Status,
       LS_YEAR              Anio,
       LS_MONTH             Mes,
       LS_PATRON            RegPatronal,
       LS_TIPO              TipoLiq,
       LE_TOT_IMS           Pesos,
       LE_TOT_RET           Pesos,
       LE_TOT_INF           Pesos,
       LE_IMSS_OB           Pesos,
       LE_IMSS_PA           Pesos,
	  LE_ACUMULA 		   Pesos,
		LE_BIM_ANT 		Pesos,
		LE_BIM_SIG 		Pesos,
		LE_VA_PAGO 		Dias,
		LE_VA_GOZO 		Dias,
		LE_PROV 			Pesos		
)		
go


ALTER TABLE LIQ_EMP
       ADD CONSTRAINT PK_LIQ_EMP PRIMARY KEY NONCLUSTERED (LS_PATRON, LS_YEAR, LS_MONTH, 
              LS_TIPO, CB_CODIGO)
go

CREATE TABLE LIQ_MOV (
       LM_AUSENCI           Dias,
       LM_BASE              PesosDiario,
       CB_CODIGO            NumeroEmpleado,
       LM_CES_VEJ           Pesos,
       LM_CLAVE             Codigo2,
       LM_DIAS              Dias,
       LM_EYM_DIN           Pesos,
       LM_EYM_ESP           Pesos,
       LM_EYM_EXC           Pesos,
       LM_EYM_FIJ           Pesos,
       LM_FECHA             Fecha,
       LM_GUARDER           Pesos,
       LM_INF_AMO           Pesos,
       LM_INV_VID           Pesos,
       LM_KAR_FEC           Fecha,
       LM_KAR_TIP           Codigo,
       LM_RETIRO            Pesos,
       LM_RIESGOS           Pesos,
       LM_TIPO              Codigo1,
       LS_PATRON            RegPatronal,
       LS_YEAR              Anio,
       LS_MONTH             Mes,
       LM_INF_PAT           Pesos,
       LS_TIPO              TipoLiq,
       LM_INCAPAC           Dias,
       LM_EYMESPP           Pesos,
       LM_EYMEXCP           Pesos,
       LM_EYMDINP           Pesos,
       LM_INVVIDP           Pesos,
       LM_CESVEJP           Pesos
)
go


ALTER TABLE LIQ_MOV
       ADD CONSTRAINT PK_LIQ_MOV PRIMARY KEY (LS_PATRON, LS_YEAR, LS_MONTH, 
              LS_TIPO, CB_CODIGO, LM_TIPO, LM_FECHA, LM_CLAVE)
go

CREATE TABLE TURNO (
       TU_CODIGO            Codigo,
       TU_DESCRIP           Descripcion,
       TU_DIAS              Dias,
       TU_DOBLES            Horas,
       TU_DOMINGO           Horas,
       TU_FESTIVO           Booleano,
       TU_HOR_1             Codigo,
       TU_HOR_2             Codigo,
       TU_HOR_3             Codigo,
       TU_HOR_4             Codigo,
       TU_HOR_5             Codigo,
       TU_HOR_6             Codigo,
       TU_HOR_7             Codigo,
       TU_HORARIO           Status,
       TU_JORNADA           Horas,
       TU_NOMINA            NominaTipo,
       TU_RIT_INI           Fecha,
       TU_RIT_PAT           Formula,
       TU_TIP_1             Status,
       TU_TIP_2             Status,
       TU_TIP_3             Status,
       TU_TIP_4             Status,
       TU_TIP_5             Status,
       TU_TIP_6             Status,
       TU_TIP_7             Status,
       TU_TIP_JOR           Status,
       TU_INGLES            Descripcion,
       TU_TEXTO             Descripcion,
       TU_NUMERO            Pesos,
       TU_HOR_FES           Codigo,
       TU_VACA_HA           DiasFrac,
       TU_VACA_SA           DiasFrac,
       TU_VACA_DE           DiasFrac,
       TU_SUB_CTA           Descripcion,
       TU_DIAS_BA           Tasa,
       TU_ACTIVO            Booleano,
       TU_TIP_JT  		      Status,
       TU_NIVEL0            Formula,
       TU_SAT_JOR 			Codigo
)
GO

ALTER TABLE TURNO
       ADD CONSTRAINT PK_TURNO PRIMARY KEY (TU_CODIGO)
go

CREATE TABLE SSOCIAL (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_PRIMAVA           Tasa,
       TB_DIAS_AG           DiasFrac,
       TB_PAGO_7            Booleano,
       TB_PRIMA_7           Booleano,
       TB_PRIMADO           Tasa,
       TB_DIAS_AD           DiasFrac,
       TB_ACTIVO            Booleano,
       TB_NIVEL0            Formula
)
go

ALTER TABLE SSOCIAL
       ADD CONSTRAINT PK_SSOCIAL PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE PRESTACI (
       TB_CODIGO            Codigo1,
       PT_YEAR              Anio,
       PT_DIAS_VA           DiasFrac,
       PT_PRIMAVA           Tasa,
       PT_DIAS_AG           DiasFrac,
       PT_DIAS_AD           DiasFrac,
       PT_PRIMADO           Tasa,
       PT_PAGO_7            Booleano,
       PT_PRIMA_7           Booleano,
       PT_FACTOR            Tasa
)
go


ALTER TABLE PRESTACI
       ADD CONSTRAINT PK_PRESTACI PRIMARY KEY (TB_CODIGO, PT_YEAR)
go

CREATE TABLE REPORTE (
       RE_CODIGO            FolioChico,
       RE_NOMBRE            Descripcion,
       RE_TIPO              Status,
       RE_TITULO            Titulo,
       RE_ENTIDAD           Status,
       RE_SOLOT             Booleano,
       RE_FECHA             Fecha,
       RE_GENERAL           Descripcion,
       RE_REPORTE           PathArchivo,
       RE_CFECHA            NombreCampo,
       RE_IFECHA            Status,
       RE_HOJA              Status,
       RE_ALTO              Tamanio,
       RE_ANCHO             Tamanio,
       RE_PRINTER           Formula,
       RE_COPIAS            Status,
       RE_PFILE             Status,
       RE_ARCHIVO           PathArchivo,
       RE_VERTICA           Booleano,
       US_CODIGO            Usuario,
       RE_FILTRO            Formula,
       RE_COLNUM            Status,
       RE_COLESPA           Tamanio,
       RE_RENESPA           Tamanio,
       RE_MAR_SUP           Tamanio,
       RE_MAR_IZQ           Tamanio,
       RE_MAR_DER           Tamanio,
       RE_MAR_INF           Tamanio,
       RE_NIVEL             Status,
       RE_FONTNAM           WindowsName,
       RE_FONTSIZ           Status,
       QU_CODIGO            Condicion,
       RE_CLASIFI           Status,
       RE_CANDADO           Status
)
go

CREATE UNIQUE INDEX XAK1REPORTE ON REPORTE
(
       RE_NOMBRE
)
go

CREATE INDEX XIE1REPORTE ON REPORTE
(
       RE_CLASIFI,
       RE_NOMBRE
)
go


ALTER TABLE REPORTE
       ADD CONSTRAINT PK_REPORTE PRIMARY KEY (RE_CODIGO)
go

CREATE TABLE CAMPOREP (
       RE_CODIGO            FolioChico,
       CR_TIPO              Status,
       CR_POSICIO           Status,
       CR_CLASIFI           Status,
       CR_SUBPOS            Status,
       CR_TABLA             Status,
       CR_TITULO            TituloCampo,
       CR_REQUIER           Formula,
       CR_CALC              Status,
       CR_MASCARA           Mascara,
       CR_ANCHO             Status,
       CR_OPER              Status,
       CR_TFIELD            Status,
       CR_SHOW              Status,
       CR_DESCRIP           NombreCampo,
       CR_COLOR             Status,
       CR_FORMULA           Formula,
       CR_BOLD              Booleano,
       CR_ITALIC            Booleano,
       CR_SUBRAYA           Booleano,
       CR_STRIKE            Booleano,
       CR_ALINEA            Status
)
go


ALTER TABLE CAMPOREP
       ADD CONSTRAINT PK_CAMPOREP PRIMARY KEY (RE_CODIGO, CR_TIPO, CR_POSICIO, 
              CR_SUBPOS)
go

CREATE TABLE RPATRON (
       TB_CODIGO            RegPatronal,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_NUMREG            NumeroPatronal,
       TB_MODULO            Status,
       RS_CODIGO            Codigo,
       TB_CALLE 			      Titulo,
       TB_NUMEXT 			      Referencia,
	     TB_NUMINT 			      Referencia,
	     TB_COLONIA 			    Titulo,
	     TB_CIUDAD 			      Titulo,
	     TB_ENTIDAD 			    Codigo2,
	     TB_CODPOST 			    Referencia,
	     TB_TEL 				      Descripcion,
	     TB_FAX 				      Descripcion,
	     TB_EMAIL 			      DescLarga,
	     TB_WEB 				      PathURL,
       TB_CLASE             Status,
       TB_FRACC             FolioGrande,
       TB_STYPS             Booleano,
       TB_SIEM              Referencia,
       TB_PLANPRE           Booleano,
       TB_PLANPER           Booleano,
       TB_PLANFOL           Titulo,
       TB_ACTIVO            Booleano,
       TB_NIVEL0            Formula
)
go

ALTER TABLE RPATRON
       ADD CONSTRAINT PK_RPATRON PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE QUERYS (
       QU_CODIGO            Condicion,
       QU_DESCRIP           Formula,
       QU_FILTRO            Formula,
       QU_NIVEL             Status,
       US_CODIGO            Usuario,
       QU_NAVEGA            Booleano,
       QU_ORDEN             FolioChico,
       QU_CANDADO           Booleano
)
go


ALTER TABLE QUERYS
       ADD CONSTRAINT PK_QUERYS PRIMARY KEY (QU_CODIGO)
go

CREATE TABLE PUESTO (
       PU_CODIGO            Codigo,
       PU_DESCRIP           Descripcion,
       PU_INGLES            Descripcion,
       PU_CLASIFI           Codigo,
       PU_NUMERO            Pesos,
       PU_TEXTO             Descripcion,
       PU_DETALLE           Formula,
       PU_REPORTA           Codigo,
       PU_PLAZAS            Empleados,
       PU_NIVEL0            Formula,
       PU_NIVEL1            Codigo,
       PU_NIVEL2            Codigo,
       PU_NIVEL3            Codigo,
       PU_NIVEL4            Codigo,
       PU_NIVEL5            Codigo,
       PU_NIVEL6            Codigo,
       PU_NIVEL7            Codigo,
       PU_NIVEL8            Codigo,
       PU_NIVEL9            Codigo,
       PU_TURNO	            Codigo,
       PU_AUTOSAL           Booleano,
       PU_CONTRAT           Codigo1,
       PU_TABLASS           Codigo1,
       PU_PATRON            RegPatronal,
       PU_AREA              Codigo,
       PU_ZONA_GE           ZonaGeo,
       PU_CHECA             Booleano,
       PU_SALARIO           PesosDiario,
       PU_PER_VAR           PesosDiario,
       PU_TIPO              Status,
       PU_ACTIVO            Booleano,
       PU_COSTO1            PesosEmpresa,
       PU_COSTO2            PesosEmpresa,
       PU_COSTO3            PesosEmpresa,
       PU_LEVEL	            FolioChico,
       PU_SAL_MIN           PesosDiario,
       PU_SAL_MAX           PesosDiario,
       PU_SAL_MED           PesosDiario,
       PU_SAL_EN1           PesosDiario,
       PU_SAL_EN2           PesosDiario,
       FP_CODIGO            Codigo,
       NP_CODIGO            Codigo,
       PU_SUB_CTA           Descripcion,
       PU_CLAVE             Operacion, 
       PU_SAT_RSG 			Codigo
)
go

ALTER TABLE PUESTO
       ADD CONSTRAINT PK_PUESTO PRIMARY KEY (PU_CODIGO)
go

CREATE TABLE PERMISO (
       CB_CODIGO            NumeroEmpleado,
       PM_FEC_INI           Fecha,
       PM_CLASIFI           Status,
       PM_COMENTA           Descripcion,
       PM_DIAS              Dias,
       PM_FEC_FIN           Fecha,
       PM_CAPTURA           Fecha,
       US_CODIGO            Usuario,
       PM_TIPO              Codigo3,
       PM_NUMERO            Referencia,
       PM_GLOBAL            Booleano
)
go


ALTER TABLE PERMISO
       ADD CONSTRAINT PK_PERMISO PRIMARY KEY (CB_CODIGO, PM_FEC_INI)
go

CREATE TABLE OTRASPER (
       TB_CODIGO            Codigo2,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status,
       TB_MONTO             PesosDiario,
       TB_TASA              Tasa,
       TB_IMSS              Status,
       TB_ISPT              Status,
       TB_TOPE              PesosDiario
)
go


ALTER TABLE OTRASPER
       ADD CONSTRAINT PK_OTRASPER PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL1 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go


ALTER TABLE NIVEL1
       ADD CONSTRAINT PK_NIVEL1 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE MONEDA (
       TB_CODIGO            Codigo2,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_VALOR             PesosDiario
)
go


ALTER TABLE MONEDA
       ADD CONSTRAINT PK_MONEDA PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE TKARDEX (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_NIVEL             Status,
       TB_SISTEMA           Booleano
)
go

ALTER TABLE TKARDEX
       ADD CONSTRAINT PK_TKARDEX PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE MOT_BAJA (
       TB_CODIGO            Codigo3,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_IMSS              Status,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
go

ALTER TABLE MOT_BAJA
       ADD CONSTRAINT PK_MOT_BAJA PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE MAESTRO (
       MA_CODIGO            Codigo,
       MA_NOMBRE            DescLarga,
       MA_CEDULA            Descripcion,
       MA_RFC               Descripcion,
       MA_NUMERO            Pesos,
       MA_TEXTO             Descripcion,
       MA_EMPRESA           Descripcion,
       MA_DIRECCI           Descripcion,
       MA_CIUDAD            Descripcion,
       MA_TEL               Descripcion,
       MA_ACTIVO            Booleano,
       MA_IMAGEN            Imagen,
       PC_CODIGO            Codigo,
       MA_TAGENTE           Status,
       MA_NAGENTE           Descripcion
)
go

ALTER TABLE MAESTRO
       ADD CONSTRAINT PK_MAESTRO PRIMARY KEY (MA_CODIGO)
go

CREATE TABLE FALTAS (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       CB_CODIGO            NumeroEmpleado,
       FA_FEC_INI           Fecha,
       FA_DIA_HOR           Codigo1,
       FA_MOTIVO            Status,
       FA_DIAS              DiasFrac,
       FA_HORAS             Horas
)
go

ALTER TABLE FALTAS
       ADD CONSTRAINT PK_FALTAS PRIMARY KEY (PE_YEAR, PE_TIPO, PE_NUMERO, 
              CB_CODIGO, FA_DIA_HOR, FA_FEC_INI, FA_MOTIVO)
go

CREATE TABLE PRESTAMO (
       PR_ABONOS            Pesos,
       PR_CARGOS            Pesos,
       PR_FECHA             Fecha,
       PR_FORMULA           Formula,
       PR_MONTO             Pesos,
       PR_NUMERO            FolioChico,
       PR_REFEREN           Referencia,
       PR_SALDO_I           Pesos,
       PR_STATUS            Status,
       PR_TIPO              Codigo1,
       PR_TOTAL             Pesos,
       PR_SALDO             Pesos,
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       PR_SUB_CTA           Descripcion,
       PR_MONTO_S           Pesos,
       PR_INTERES           Pesos,
       PR_TASA              Tasa,
       PR_MESES             FolioChico,
       PR_PAGOS             FolioChico,
       PR_PAG_PER           Pesos,
	   PR_OBSERVA           ReferenciaLarga,
	   PR_FON_MOT Status
)
go


ALTER TABLE PRESTAMO
       ADD CONSTRAINT PK_PRESTAMO PRIMARY KEY (CB_CODIGO, PR_TIPO, PR_REFEREN)
go

CREATE TABLE CURSO (
       CU_CODIGO            Codigo,
       CU_ACTIVO            Booleano,
       CU_CLASIFI           Codigo,
       CU_COSTO1            Pesos,
       CU_COSTO2            Pesos,
       CU_COSTO3            Pesos,
       CU_HORAS             Horas,
       CU_INGLES            DescLarga,
       CU_NOMBRE            Formula,
       CU_TEXTO             DescLarga,
       CU_TEXTO1            Memo,
       CU_TEXTO2            Memo,
       CU_NUMERO            Pesos,
       MA_CODIGO            Codigo,
       CU_CLASE             Codigo,
       CU_REVISIO           NombreCampo,
       CU_FEC_REV           Fecha,
       CU_STPS              Booleano,
       CU_FOLIO             ReferenciaLarga,
       CU_DOCUM             PathURL,
       AT_CODIGO            Operacion,
       CU_OBJETIV           Status,
       CU_MODALID           Status
)
go


ALTER TABLE CURSO
       ADD CONSTRAINT PK_CURSO PRIMARY KEY (CU_CODIGO)
go

CREATE TABLE KARCURSO (
       CB_CODIGO            NumeroEmpleado,
       CU_CODIGO            Codigo,
       CB_CLASIFI           Codigo,
       CB_TURNO             Codigo,
       KC_EVALUA            Horas,
       KC_FEC_TOM           Fecha,
       KC_HORAS             Horas,
       CB_PUESTO            Codigo,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       MA_CODIGO            Codigo,
       KC_FEC_FIN           Fecha,
       KC_REVISIO           NombreCampo,
       SE_FOLIO             FolioGrande,
       KC_FECPROG           Fecha,
       KC_NO_STPS           FolioGrande,
       KC_EST               Codigo
)
go


ALTER TABLE KARCURSO
       ADD CONSTRAINT PK_KARCURSO PRIMARY KEY (CB_CODIGO, KC_FEC_TOM, CU_CODIGO)
go

CREATE INDEX XIE1KARCURSO ON KARCURSO
(
       CU_CODIGO
)
go

CREATE INDEX XIE2KARCURSO ON KARCURSO
(
       SE_FOLIO
)
GO

CREATE TABLE INCIDEN (
       TB_CODIGO            Codigo3,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_INCIDEN           Status,
       TB_INCAPA            Status,
       TB_SISTEMA           Booleano,
	TB_PERMISO          Status
)
go


ALTER TABLE INCIDEN
       ADD CONSTRAINT PK_INCIDEN PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE INCAPACI (
       CB_CODIGO            NumeroEmpleado,
       IN_FEC_INI           Fecha,
       IN_COMENTA           Descripcion,
       IN_DIAS              Dias,
       IN_FEC_FIN           Fecha,
       IN_FIN               Status,
       IN_NUMERO            Descripcion,
       IN_TASA_IP           Tasa,
       IN_CAPTURA           Fecha,
       IN_TIPO              Codigo3,
       US_CODIGO            Usuario,
       IN_MOTIVO            Status,
       IN_DIASSUB           DiasFrac,
       IN_NOMYEAR           Anio,
       IN_NOMTIPO           NominaTipo,
       IN_NOMNUME           NominaNumero,
       IN_SUA_INI           Fecha,
       IN_SUA_FIN           Fecha,
       IN_FEC_RH            Fecha
)
go

ALTER TABLE INCAPACI
       ADD CONSTRAINT PK_INCAPACI PRIMARY KEY (CB_CODIGO, IN_FEC_INI)
go

CREATE TABLE HORARIO (
       HO_CODIGO            Codigo,
       HO_ADD_EAT           Booleano,
       HO_CHK_EAT           Booleano,
       HO_COMER             Minutos,
       HO_DESCRIP           Descripcion,
       HO_DOBLES            Horas,
       HO_EXT_FIJ           Horas,
       HO_IGN_EAT           Booleano,
       HO_IN_EAT            Hora,
       HO_IN_TARD           Minutos,
       HO_IN_TEMP           Minutos,
       HO_INTIME            Hora,
       HO_LASTOUT           Hora,
       HO_MIN_EAT           Minutos,
       HO_JORNADA           Horas,
       HO_OU_TARD           Minutos,
       HO_OU_TEMP           Minutos,
       HO_OUT_EAT           Hora,
       HO_OUTTIME           Hora,
       HO_TIPO              Status,
       HO_EXT_COM           Minutos,
       HO_EXT_MIN           Minutos,
       HO_OUT_BRK           Hora,
       HO_IN_BRK            Hora,
       HO_ACTIVO            Booleano,
       HO_PARES             Booleano,
       HO_TEXTO             Descripcion,
       HO_INGLES            Descripcion,
       HO_NUMERO	          Pesos,
       HO_NIVEL0            Formula,
	   HO_TIP_JT		    Pesos	
)
go


ALTER TABLE HORARIO
       ADD CONSTRAINT PK_HORARIO PRIMARY KEY (HO_CODIGO)
go

CREATE TABLE FESTIVO (
       TU_CODIGO            Codigo,
       FE_MES               Mes,
       FE_DIA               Dias,
       FE_DESCRIP           Descripcion,
       FE_TIPO              Status,
       FE_CAMBIO            Fecha,
       FE_YEAR              Anio
)
go


ALTER TABLE FESTIVO 
	ADD CONSTRAINT PK_FESTIVO PRIMARY KEY (TU_CODIGO, FE_MES, FE_DIA, FE_YEAR )
GO

CREATE TABLE EVENTO (
       EV_ACTIVO            Booleano,
       EV_ALTA              Booleano,
       EV_ANT_FIN           Dias,
       EV_ANT_INI           Dias,
       EV_AUTOSAL           Codigo1,
       EV_BAJA              Booleano,
       EV_CLASIFI           Codigo,
       EV_CODIGO            Evento,
       EV_CONTRAT           Codigo,
       EV_DESCRIP           Descripcion,
       EV_FEC_BSS           Fecha,
       EV_FILTRO            Formula,
       EV_KARDEX            Codigo,
       EV_MOT_BAJ           Codigo3,
       EV_OTRAS_1           Codigo2,
       EV_OTRAS_2           Codigo2,
       EV_OTRAS_3           Codigo2,
       EV_OTRAS_4           Codigo2,
       EV_OTRAS_5           Codigo2,
       EV_PATRON            RegPatronal,
       EV_NOMNUME           NominaNumero,
       EV_PRIORID           Status,
       EV_PUESTO            Codigo,
       EV_QUERY             Condicion,
       EV_SALARIO           Formula,
       EV_TABLASS           Codigo1,
       EV_NIVEL1            Codigo,
       EV_NIVEL2            Codigo,
       EV_NIVEL3            Codigo,
       EV_NIVEL4            Codigo,
       EV_NIVEL5            Codigo,
       EV_NIVEL6            Codigo,
       EV_NIVEL7            Codigo,
       EV_NIVEL8            Codigo,
       EV_NIVEL9            Codigo,
       EV_TURNO             Codigo,
       EV_INCLUYE           Status,
       EV_NOMTIPO           NominaTipo,
       EV_NOMYEAR           Anio,
       EV_TABULA            Booleano,
       EV_M_ANTIG           Booleano,
       EV_FEC_CON           Booleano,
       EV_CAMPO             NombreCampo,
       EV_FORMULA           Formula,
       EV_M_SVAC 		        Booleano,
 	     EV_M_TVAC 		        Status,
       EV_TIPNOM            NominaTipo,
       EV_CAMBNOM           Booleano
)
go


ALTER TABLE EVENTO
       ADD CONSTRAINT PK_EVENTO PRIMARY KEY (EV_CODIGO)
go

CREATE TABLE ENTRENA (
       CU_CODIGO            Codigo,
       PU_CODIGO            Codigo,
       EN_DIAS              Dias,
       EN_OPCIONA           Booleano,
       EN_LISTA             Booleano,
       EN_REPROG            Booleano,
       EN_RE_DIAS           Dias
)
go

ALTER TABLE ENTRENA
       ADD CONSTRAINT PK_ENTRENA PRIMARY KEY (PU_CODIGO, CU_CODIGO)
go

CREATE INDEX XIE1ENTRENA ON ENTRENA
(
       CU_CODIGO
)
go

CREATE TABLE CONTRATO (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_DIAS              Dias,
       TB_ACTIVO            Booleano,
       TB_NIVEL0            Formula,
       TB_SAT_CON           Codigo
)
go

ALTER TABLE CONTRATO
       ADD CONSTRAINT PK_CONTRATO PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE CONCEPTO (
       CO_A_PTU             Booleano,
       CO_ACTIVO            Booleano,
       CO_CALCULA           Booleano,
       CO_DESCRIP           Descripcion,
       CO_FORMULA           Formula,
       CO_G_IMSS            Booleano,
       CO_G_ISPT            Booleano,
       CO_IMP_CAL           Formula,
       CO_IMPRIME           Booleano,
       CO_LISTADO           Booleano,
       CO_MENSUAL           Booleano,
       CO_NUMERO            Concepto,
       CO_QUERY             Condicion,
       CO_RECIBO            Formula,
       CO_TIPO              Status,
       CO_X_ISPT            Formula,
       CO_SUB_CTA           Descripcion,
       CO_CAMBIA            Booleano,
       CO_D_EXT             Codigo,
       CO_D_NOM             Descripcion,
       CO_D_BLOB            Imagen,
       CO_NOTA              Memo,
       CO_LIM_SUP           Pesos,
       CO_LIM_INF           Pesos,
       CO_VER_INF           Booleano,
       CO_VER_SUP           Booleano,
       CO_VER_ACC           Booleano,
       CO_GPO_ACC           ListaContenido,
       CO_ISN               Booleano,
       CO_SUMRECI           Booleano,
       CO_FRM_ALT           Formula,
       CO_USO_NOM           Status,
       CO_SAT_CLP	          Status,
       CO_SAT_TPP	          Codigo,
       CO_SAT_CLN	          Status,
       CO_SAT_TPN	          Codigo,
       CO_SAT_EXE	          Status,
       CO_SAT_CON	          Concepto,
	   CO_PS_TIPO			Status,
       CO_TIMBRA            Booleano not null
)
go


ALTER TABLE CONCEPTO
       ADD CONSTRAINT PK_CONCEPTO PRIMARY KEY (CO_NUMERO)
go

create function SP_JORNADA_DIARIA (
  @Horario Codigo
) returns Horas
AS
begin
	 return ( select HO_JORNADA from HORARIO where ( HO_CODIGO = @Horario ) )
end
GO

CREATE FUNCTION SP_STATUS_ACT (
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS SMALLINT
AS
BEGIN
  declare @Tipo Varchar(6);
  declare @kFecha DATETIME;
  declare @FEC_ING DATETIME;
  declare @FEC_BAJ DATETIME;
  declare @ACTIVO Char(1);
  declare @Resultado Smallint;

  select @FEC_ING = CB_FEC_ING, @FEC_BAJ = CB_FEC_BAJ, @ACTIVO = CB_ACTIVO
  from   COLABORA
  where  CB_CODIGO = @Empleado

  set @Resultado = 0;


  if  ( ( @FECHA < @FEC_ING ) AND ( @FEC_BAJ = '12/30/1899') )
    set @Resultado = 0
  else if ( ( @ACTIVO = 'N' ) AND ( @FECHA > @FEC_BAJ ))
    set @Resultado = 9
  else  if( ( @ACTIVO = 'S' ) AND ( @FECHA >= @FEC_ING ))
    set @Resultado = 1
  else
  begin

    select TOP 1
         @Tipo = CB_TIPO, @kFecha = CB_FECHA
    from KARDEX
    where ( CB_CODIGO = @Empleado ) AND
          ( CB_FECHA <= @Fecha  ) AND
          ( CB_TIPO IN ( 'ALTA', 'BAJA' ))
    order by CB_FECHA DESC, CB_NIVEL DESC

    if ( @Tipo IS NULL )
        set @Resultado = 0
    else if (( @Tipo = 'BAJA' ) and ( @Fecha > @kFecha ))
    	set @Resultado = 9
    else
    	set @Resultado = 1
  end

  RETURN @Resultado
END
GO

CREATE FUNCTION SP_STATUS_INCA (
  @FechaIni DATETIME,
  @FechaFin DATETIME,
  @Empleado INTEGER
) RETURNS SMALLINT
AS
begin
  RETURN   ( SELECT COUNT(*)
    		from INCAPACI
		where ( CB_CODIGO = @Empleado ) AND
	          ( IN_FEC_INI <= @FechaFin ) AND
	          ( IN_FEC_FIN > @FechaIni ))
end
GO

CREATE FUNCTION SP_STATUS_VACA
		(@FechaIni DateTime,
    		@FechaFin DateTime,
    		@Empleado Int)
RETURNS SmallInt
AS
BEGIN
    	Return (SELECT COUNT(*)
  		from VACACION
  		where ( CB_CODIGO = @Empleado )
		AND ( VA_FEC_INI <= @FechaFin )
		AND ( VA_FEC_FIN > @FechaIni )
		AND ( VA_TIPO = 1 ))
end
GO

CREATE FUNCTION SP_STATUS_PERM
  		(@FechaIni DateTime,
  		@FechaFin DateTime,
		@Clase Int,
  		@Empleado Int)
RETURNS SmallInt
AS
BEGIN
	Declare @Resultado SmallInt

   	if ( @Clase < 0 )
    		SELECT @Resultado = COUNT(*)
    		from PERMISO
    		where ( CB_CODIGO = @Empleado )
		AND ( PM_FEC_INI <= @FechaFin )
		AND ( PM_FEC_FIN > @FechaIni )
  	else
    		SELECT @Resultado = COUNT(*)
    		from PERMISO
    		where ( CB_CODIGO = @Empleado )
		AND ( PM_FEC_INI <= @FechaFin )
		AND ( PM_FEC_FIN > @FechaIni )
		AND ( PM_CLASIFI = @Clase )

	Return @Resultado
END
GO

CREATE FUNCTION SP_STATUS_EMP(
					@FECHA DATETIME,
    				@EMPLEADO INTEGER)
RETURNS SmallInt
AS
begin
	DECLARE @CUANTOS INTEGER;
	DECLARE @CLASEPERMISO SMALLINT;
	DECLARE @RESULTADO SMALLINT;
  
	SELECT @Resultado = DBO.SP_STATUS_ACT( @Fecha, @Empleado )
  
    if ( @Resultado = 1 ) 
  	begin
    	SELECT @Cuantos = DBO.SP_STATUS_VACA( @Fecha, @Fecha, @Empleado )
    	
		if ( @Cuantos > 0 ) Select @Resultado = 2;
  	end

    if ( @Resultado = 1 ) 
  	begin
    	SELECT @Cuantos = DBO.SP_STATUS_INCA( @Fecha, @Fecha, @Empleado )
    	
		if ( @Cuantos > 0 ) Select @Resultado = 3;
  	end

    if ( @Resultado = 1 ) 
  	begin
    	SELECT @Cuantos = DBO.SP_STATUS_PERM( @Fecha, @Fecha, -1, @Empleado )
    
    	if ( @Cuantos > 0 ) 
    	begin
			Select @ClasePermiso = 0;
        	while (( @ClasePermiso <= 4 ) AND ( @Resultado = 1 )) 
        	begin
      			SELECT @Cuantos = DBO.SP_STATUS_PERM( @Fecha, @Fecha, @ClasePermiso, @Empleado )
      
        		if ( @Cuantos > 0 ) select @Resultado = @ClasePermiso + 4;
  
  				select @ClasePermiso = @ClasePermiso + 1;
        	end
    	end
  	end

  	Return @Resultado;
END
GO

CREATE TABLE AUSENCIA (
       CB_CODIGO            NumeroEmpleado,
       AU_DES_TRA           Horas,
       AU_DOBLES            Horas,
       AU_EXTRAS            Horas,
       AU_FECHA             Fecha,
       AU_HORAS             Horas,
       AU_PER_CG            Horas,
       AU_PER_SG            Horas,
       AU_TARDES            Horas,
       AU_TIPO              Codigo3,
       AU_TIPODIA           Status,
       CB_CLASIFI           Codigo,
       CB_TURNO             Codigo,
       CB_PUESTO            Codigo,
       HO_CODIGO            Codigo,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       AU_AUT_EXT           Horas,
       AU_AUT_TRA           Horas,
       AU_HOR_MAN           Booleano,
       AU_STATUS            Status,
       AU_NUM_EXT           Horas,
       AU_TRIPLES           Horas,
       US_CODIGO            Usuario,
       AU_POSICIO           Dias,
       AU_HORASCK           Horas,
       AU_OUT2EAT           Status,
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       AU_HORASNT           Horas,
       CB_SALARIO           PesosDiario,
       AU_STA_FES           Status,
       AU_PRE_EXT           Horas,
	     CB_NOMINA 		        NominaTipo,
       AU_JORNADA as dbo.SP_JORNADA_DIARIA(HO_CODIGO),
       AU_STATEMP as dbo.SP_STATUS_EMP(AU_FECHA,CB_CODIGO),
       CB_TABLASS           Codigo1
)
go

ALTER TABLE AUSENCIA
       ADD CONSTRAINT PK_AUSENCIA PRIMARY KEY (AU_FECHA, CB_CODIGO)
GO

CREATE INDEX XIE1AUSENCIA ON AUSENCIA
(
       CB_CODIGO,
       AU_FECHA
)
GO

CREATE INDEX XIE2AUSENCIA ON AUSENCIA
(
       CB_CODIGO,
       PE_YEAR,
       PE_TIPO,
       PE_NUMERO,
       AU_POSICIO
)
go

CREATE INDEX XIE1AUSENCIA_AU_TIPO ON AUSENCIA 
(
	AU_FECHA ASC,
	CB_CODIGO ASC, 
	AU_TIPO 
)
GO

CREATE INDEX XIE1AUSENCIA_AU_TIPODIA ON AUSENCIA
(
	AU_FECHA ASC,
	CB_CODIGO ASC,
	AU_TIPODIA
)
GO

CREATE INDEX XIE1AUSENCIA_AU_STATUS ON AUSENCIA
(
	AU_FECHA ASC,
	CB_CODIGO ASC,
	AU_STATUS
)
GO

CREATE TABLE CHECADAS (
       CB_CODIGO            NumeroEmpleado,
       AU_FECHA             Fecha,
       CH_DESCRIP           Status,
       CH_GLOBAL            Booleano,
       CH_H_AJUS            Hora,
       CH_H_REAL            Hora,
       CH_HOR_DES           Horas,
       CH_HOR_EXT           Horas,
       CH_HOR_ORD           Horas,
       CH_IGNORAR           Booleano,
       CH_RELOJ             RelojLinx,
       CH_SISTEMA           Booleano,
       CH_TIPO              Status,
       US_CODIGO            Usuario,
       US_COD_OK            Usuario,
       CH_POSICIO           Dias,
       CH_MOTIVO            Codigo4
)
go


ALTER TABLE CHECADAS
       ADD CONSTRAINT PK_CHECADAS PRIMARY KEY (AU_FECHA, CB_CODIGO, CH_H_REAL, 
              CH_TIPO)
go

CREATE TABLE CLASIFI (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SALARIO           PesosDiario,
       TB_OP1               Codigo2,
       TB_OP2               Codigo2,
       TB_OP3               Codigo2,
       TB_OP4               Codigo2,
       TB_OP5               Codigo2,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       TB_NIVEL0            Formula
)
go


ALTER TABLE CLASIFI
       ADD CONSTRAINT PK_CLASIFI PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE AHORRO (
       AH_ABONOS            Pesos,
       AH_CARGOS            Pesos,
       AH_FECHA             Fecha,
       AH_FORMULA           Formula,
       AH_NUMERO            FolioChico,
       AH_SALDO_I           Pesos,
       AH_STATUS            Status,
       AH_SALDO             Pesos,
       AH_TIPO              Codigo1,
       AH_TOTAL             Pesos,
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       AH_SUB_CTA           Descripcion
)
go


ALTER TABLE AHORRO
       ADD CONSTRAINT PK_AHORRO PRIMARY KEY (CB_CODIGO, AH_TIPO)
go

CREATE TABLE ACAR_ABO (
       AH_TIPO              Codigo1,
       CB_CODIGO            NumeroEmpleado,
       CR_CAPTURA           Fecha,
       CR_ABONO             Pesos,
       CR_FECHA             Fecha,
       CR_CARGO             Pesos,
       CR_OBSERVA           Descripcion,
       US_CODIGO            Usuario
)
go


ALTER TABLE ACAR_ABO
       ADD CONSTRAINT PK_ACAR_ABO PRIMARY KEY (CB_CODIGO, AH_TIPO, CR_FECHA)
go

CREATE TABLE CALCURSO (
       CU_CODIGO            Codigo,
       CC_FECHA             Fecha
)
go


ALTER TABLE CALCURSO
       ADD CONSTRAINT PK_CALCURSO PRIMARY KEY (CU_CODIGO, CC_FECHA)
go

CREATE TABLE ACUMULA (
       AC_MES_01            Pesos,
       AC_YEAR              Anio,
       AC_MES_02            Pesos,
       CO_NUMERO            Concepto,
       AC_MES_03            Pesos,
       AC_MES_04            Pesos,
       AC_MES_05            Pesos,
       AC_MES_06            Pesos,
       AC_MES_07            Pesos,
       AC_MES_08            Pesos,
       AC_MES_09            Pesos,
       AC_MES_10            Pesos,
       AC_MES_11            Pesos,
       AC_MES_12            Pesos,
       AC_MES_13            Pesos,
       CB_CODIGO            NumeroEmpleado,
       AC_ANUAL             Pesos
)
go

ALTER TABLE ACUMULA
       ADD CONSTRAINT PK_ACUMULA PRIMARY KEY (AC_YEAR, CB_CODIGO, CO_NUMERO)
go

CREATE TABLE TPRESTA (
       TB_CODIGO            Codigo1,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_CONCEPT           Concepto,
       TB_RELATIV           Concepto,
       TB_LIQUIDA           Status,
       TB_ALTA              Status,
       TB_PRIORID           Status,
       TB_TASA1             Tasa,
       TB_TASA2             Tasa,
       TB_TASA3             Tasa,
       TB_ACTIVO            Booleano,
       TB_NIVEL0            Formula
)
go

ALTER TABLE TPRESTA
       ADD CONSTRAINT PK_TPRESTA PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE PCAR_ABO (
       CB_CODIGO            NumeroEmpleado,
       CR_CAPTURA           Fecha,
       PR_TIPO              Codigo1,
       CR_FECHA             Fecha,
       PR_REFEREN           Referencia,
       CR_OBSERVA           DescLarga,
       CR_ABONO             Pesos,
       CR_CARGO             Pesos,
       US_CODIGO            Usuario
)
go


ALTER TABLE PCAR_ABO
       ADD CONSTRAINT PK_PCAR_ABO PRIMARY KEY (CB_CODIGO, PR_TIPO, PR_REFEREN, 
              CR_FECHA)
go

CREATE TABLE NIVEL2 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL2
       ADD CONSTRAINT PK_NIVEL2 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL3 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL3
       ADD CONSTRAINT PK_NIVEL3 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL4 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL4
       ADD CONSTRAINT PK_NIVEL4 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL5 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL5
       ADD CONSTRAINT PK_NIVEL5 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL6 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL6
       ADD CONSTRAINT PK_NIVEL6 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL7 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL7
       ADD CONSTRAINT PK_NIVEL7 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL8 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL8
       ADD CONSTRAINT PK_NIVEL8 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE NIVEL9 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_SUB_CTA           Descripcion,
       TB_ACTIVO            Booleano,
       US_CODIGO            Usuario,
       TB_NIVEL0            Formula
)
go

ALTER TABLE NIVEL9
       ADD CONSTRAINT PK_NIVEL9 PRIMARY KEY (TB_CODIGO)
go

CREATE TABLE PRIESGO (
       TB_CODIGO            RegPatronal,
       RT_FECHA             Fecha,
       RT_PRIMA             Tasa,
       RT_PTANT             Tasa,
       RT_S                 DiasFrac,
       RT_I                 Tasa,
       RT_D                 Numerico,
       RT_N                 Tasa,
       RT_M                 Tasa,
       RT_F                 Numerico
)
go

ALTER TABLE PRIESGO
       ADD CONSTRAINT PK_PRIESGO PRIMARY KEY (TB_CODIGO, RT_FECHA)
go

CREATE TABLE IMAGEN (
       CB_CODIGO            NumeroEmpleado,
       IM_TIPO              Codigo,
       IM_BLOB              Imagen,
       IM_OBSERVA           Descripcion
)
go


ALTER TABLE IMAGEN
       ADD CONSTRAINT PK_IMAGEN PRIMARY KEY (CB_CODIGO, IM_TIPO)
go

CREATE TABLE RIESGO (
       RI_CLASE             FolioChico,
       RI_GRADO             FolioChico,
       RI_INDICE            FolioGrande,
       RI_PRIMA             Tasa
)
go


ALTER TABLE RIESGO
       ADD CONSTRAINT PK_RIESGO PRIMARY KEY (RI_CLASE, RI_GRADO)
go

CREATE TABLE LEY_IMSS (
       SS_INICIAL           Fecha,
       SS_L_A1061           SalariosMinimos,
       SS_L_A1062           SalariosMinimos,
       SS_L_A107            SalariosMinimos,
       SS_L_A25             SalariosMinimos,
       SS_L_CV              SalariosMinimos,
       SS_L_GUARD           SalariosMinimos,
       SS_L_INFO            SalariosMinimos,
       SS_L_IV              SalariosMinimos,
       SS_L_RT              SalariosMinimos,
       SS_L_SAR             SalariosMinimos,
       SS_MAXIMO            SalariosMinimos,
       SS_O_A1061           Tasa,
       SS_O_A1062           Tasa,
       SS_O_A107            Tasa,
       SS_O_A25             Tasa,
       SS_O_CV              Tasa,
       SS_O_GUARD           Tasa,
       SS_O_INFO            Tasa,
       SS_O_IV              Tasa,
       SS_O_SAR             Tasa,
       SS_P_A1061           Tasa,
       SS_P_A1062           Tasa,
       SS_P_A107            Tasa,
       SS_P_A25             Tasa,
       SS_P_CV              Tasa,
       SS_P_GUARD           Tasa,
       SS_P_INFO            Tasa,
       SS_P_IV              Tasa,
       SS_P_SAR             Tasa
)
go


ALTER TABLE LEY_IMSS
       ADD CONSTRAINT PK_LEY_IMSS PRIMARY KEY (SS_INICIAL)
go

CREATE TABLE NUMERICA (
       NU_CODIGO            Status,
       NU_DESCRIP           Descripcion,
       NU_INGLES            Descripcion,
       NU_NUMERO            Pesos,
       NU_TEXTO             Descripcion
)
go

ALTER TABLE NUMERICA
       ADD CONSTRAINT PK_NUMERICA PRIMARY KEY (NU_CODIGO)
go

CREATE TABLE T_ART_80 (
       NU_CODIGO        Status,
       TI_INICIO        Fecha,
       TI_DESCRIP       Descripcion )
GO

ALTER TABLE T_ART_80
      ADD CONSTRAINT PK_T_ART_80 PRIMARY KEY ( NU_CODIGO, TI_INICIO )
GO

CREATE TABLE ART_80 (
       NU_CODIGO            Status,
       TI_INICIO            Fecha,
       A80_LI               Pesos,
       A80_CUOTA            Pesos,
       A80_TASA             Tasa
)
GO

ALTER TABLE ART_80
       ADD CONSTRAINT PK_ART_80 PRIMARY KEY (NU_CODIGO, TI_INICIO, A80_LI)
GO


CREATE TABLE DICCION (
       DI_CLASIFI           Status,
       DI_NOMBRE            NombreCampo,
       DI_TITULO            TituloCampo,
       DI_CALC              Status,
       DI_ANCHO             Status,
       DI_MASCARA           Descripcion,
       DI_TFIELD            Status,
       DI_ORDEN             Booleano,
       DI_REQUIER           CamposRequeridos,
       DI_TRANGO            Status,
       DI_NUMERO            Status,
       DI_VALORAC           Descripcion,
       DI_RANGOAC           Status,
       DI_CLAVES            TituloCampo,
       DI_TCORTO            TituloCampo,
       DI_CONFI             Booleano
)
go

CREATE INDEX XIE1DICCION ON DICCION
(
       DI_CLASIFI,
       DI_CALC
)
go


ALTER TABLE DICCION
       ADD CONSTRAINT PK_DICCION PRIMARY KEY (DI_CLASIFI, DI_NOMBRE)
go

CREATE TABLE BITACORA (
       US_CODIGO            Usuario,
       BI_FECHA             Fecha,
       BI_HORA              HoraMinutosSegundos,
       BI_PROC_ID           Status,
       BI_TIPO              Status,
       BI_NUMERO            FolioGrande,
       BI_TEXTO             Observaciones,
       CB_CODIGO            NumeroEmpleado,
       BI_DATA              Memo,
       BI_CLASE             Status,
       BI_FEC_MOV           Fecha
)
go

CREATE INDEX XIE1BITACORA ON BITACORA
(
       BI_NUMERO
)
go

CREATE INDEX XIE2BITACORA ON BITACORA
(
       BI_FECHA,
       BI_HORA
)
go


CREATE TABLE TMPBALAN (
       TZ_USER              Usuario,
       CB_CODIGO            NumeroEmpleado,
       TZ_CODIGO            FolioChico,
       TZ_NUM_PER           Concepto,
       TZ_DES_PER           Descripcion,
       TZ_MON_PER           Pesos,
       TZ_REF_PER           Referencia,
       TZ_NUM_DED           Concepto,
       TZ_HOR_PER           Horas,
       TZ_DES_DED           Descripcion,
       TZ_MON_DED           Pesos,
       TZ_REF_DED           Referencia,
       TZ_HOR_DED           Horas,
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero
)
go


ALTER TABLE TMPBALAN
       ADD CONSTRAINT PK_TMPBALAN PRIMARY KEY (TZ_USER, CB_CODIGO, TZ_CODIGO)
go

CREATE TABLE TMPESTAD (
       TE_USER              Usuario,
       TE_CODIGO            FolioChico,
       TE_FECHA             Fecha,
       TE_CARGO             Pesos,
       TE_ABONO             Pesos,
       TE_SALDO             Pesos,
       US_CODIGO            Usuario,
       TE_DESCRIP           Descripcion,
       CB_CODIGO            NumeroEmpleado,
       TE_TIPO              Codigo1,
       TE_REFEREN           Referencia
)
go


ALTER TABLE TMPESTAD
       ADD CONSTRAINT PK_TMPESTAD PRIMARY KEY (TE_USER, TE_CODIGO)
go

CREATE TABLE TMPTOTAL (
       US_CODIGO            Usuario,
       CO_NUMERO            Concepto,
       GRUPO1               Codigo,
       GRUPO2               Codigo,
       GRUPO3               Codigo,
       GRUPO4               Codigo,
       GRUPO5               Codigo,
       TOT_MONTO            PesosEmpresa,
       TOT_NUMERO           Empleados
)
go


ALTER TABLE TMPTOTAL
       ADD CONSTRAINT PK_TMPTOTAL PRIMARY KEY (US_CODIGO, CO_NUMERO, GRUPO1, GRUPO2, GRUPO3, 
              GRUPO4, GRUPO5)
go

CREATE TABLE TMPSALAR (
       TS_USER              Usuario,
       CB_CODIGO            NumeroEmpleado,
       PRETTYNAME           Observaciones,
       TS_MONTO1            Pesos,
       TS_MONTO2            Pesos,
       CB_SALARIO           Pesos,
       CB_TABLASS           Codigo1,
       CB_FEC_ANT           Fecha,
       CB_PER_VAR           Pesos,
       CB_FEC_SAL           Fecha,
       CB_AUTOSAL           Booleano,
       TS_DIF               PesosDiario,
       TS_POR               Tasa,
       CB_NOTA              Memo
)
go


ALTER TABLE TMPSALAR
       ADD CONSTRAINT PK_TMPSALAR PRIMARY KEY (TS_USER, CB_CODIGO)
go

CREATE TABLE TMPMOVS (
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       CO_NUMERO            Concepto,
       MO_REFEREN           Referencia,
       MO_MONTO             Pesos,
       MO_TIPO              Status,
       FA_FEC_INI           Fecha,
       FA_DIA_HOR           Codigo1,
       FA_MOTIVO            Status
)
go

CREATE INDEX XIE1TMPMOVS ON TMPMOVS
(
       CB_CODIGO
)
go

CREATE INDEX XIE2TMPMOVS ON TMPMOVS
(
       CO_NUMERO
)
go


ALTER TABLE TMPMOVS
       ADD CONSTRAINT PK_TMPMOVS PRIMARY KEY (US_CODIGO, CB_CODIGO, CO_NUMERO, MO_REFEREN)
go

CREATE TABLE TPERIODO (
       TP_TIPO              NominaTipo,
       TP_NOMBRE            Descripcion,
       TP_DESCRIP           Descripcion,
       TP_DIAS              DiasFrac,
       TP_DIAS_7            Tasa,
       TP_HORAS             Horas,
       TP_DIAS_BT           Status,
       TP_DIAS_EV           Formula,
       TP_HORASJO           Booleano,
       TP_TOT_EV1           Formula,
       TP_TOT_EV2           Formula,
       TP_TOT_EV3           Formula,
       TP_TOT_EV4           Formula,
       TP_TOT_EV5           Formula,
       TP_NIVEL0            Formula,
	   TP_CLAS              NominaTipo
)
go

ALTER TABLE TPERIODO
       ADD CONSTRAINT PK_TPERIODO PRIMARY KEY (TP_TIPO)
go

CREATE TABLE COMPARA (
       CP_YEAR              Anio,
       CB_CODIGO            NumeroEmpleado,
       CB_SALARIO           PesosDiario,
       CP_TOT_PER           Pesos,
       CP_GRAVADO           Pesos,
       CP_PAGADO            Pesos,
       CP_RETENID           Pesos,
       CP_ART_141           Pesos,
       CP_FAVOR             Pesos,
       CP_CONTRA            Pesos,
       CP_A_PAGAR           Pesos,
       US_CODIGO            Usuario,
       CP_CALCULO           Booleano
)
go


ALTER TABLE COMPARA
       ADD CONSTRAINT PK_COMPARA PRIMARY KEY (CP_YEAR, CB_CODIGO)
go

CREATE TABLE DECLARA (
       DC_YEAR              Anio,
       CB_CODIGO            NumeroEmpleado,
       DC_TOT_PER           Pesos,
       DC_GRAVADO           Pesos,
       DC_CREDITO           Pesos,
       US_CODIGO            Usuario
)
go


ALTER TABLE DECLARA
       ADD CONSTRAINT PK_DECLARA PRIMARY KEY (DC_YEAR, CB_CODIGO)
go

CREATE TABLE TMPROTA (
       TR_USER              Usuario,
       TR_GRUPO             Codigo,
       TR_DESCRIP           Descripcion,
       TR_INICIO            Empleados,
       TR_FINAL             Empleados,
       TR_ALTAS             Empleados,
       TR_BAJAS             Empleados,
       TR_PROM              Pesos,
       TR_FECHA             Fecha
)
go


ALTER TABLE TMPROTA
       ADD CONSTRAINT PK_TMPROTA PRIMARY KEY (TR_USER, TR_GRUPO)
go

CREATE TABLE TMPROTAI (
       TR_USER              Usuario,
       CB_CODIGO            NumeroEmpleado,
       TR_GRUPO             Codigo,
       TR_DESCRIP           Descripcion,
       CB_FEC_ING           Fecha,
       CB_FEC_BAJ           Fecha
)
go

ALTER TABLE TMPROTAI
       ADD CONSTRAINT PK_TMPROTAI PRIMARY KEY (TR_USER, CB_CODIGO)
go

CREATE TABLE CAFREGLA (
       CL_CODIGO            FolioChico,
       CL_LETRERO           DescLarga,
       CL_ACTIVO            Booleano,
       CL_QUERY             Condicion,
       CL_FILTRO            Formula,
       CL_TIPOS             DescLarga,
       CL_TOTAL             Formula,
       CL_LIMITE            FolioChico,
       CL_EXTRAS            Formula
)
go

ALTER TABLE CAFREGLA
       ADD CONSTRAINT PK_CAFREGLA PRIMARY KEY (CL_CODIGO)
go

CREATE TABLE INVITA (
       IV_CODIGO            NumeroEmpleado,
       IV_NOMBRE            Descripcion,
       IV_ID_NUM            Descripcion,
       CB_CODIGO            NumeroEmpleado,
       IV_ACTIVO            Booleano,
       IV_FEC_INI           Fecha,
       IV_FEC_FIN           Fecha,
       IV_TOPE_1            FolioGrande,
       IV_TOPE_2            FolioGrande,
       IV_TOPE_3            FolioGrande,
       IV_TOPE_4            FolioGrande,
       IV_TOPE_5            FolioGrande,
       IV_TOPE_6            FolioGrande,
       IV_TOPE_7            FolioGrande,
       IV_TOPE_8            FolioGrande,
       IV_TOPE_9            FolioGrande,
	     IV_ID_BIO 			      FolioGrande,
       IV_ID_GPO            CODIGO,
       IV_MIFARE            Booleano not null
)
go

ALTER TABLE INVITA
       ADD CONSTRAINT PK_INVITA PRIMARY KEY (IV_CODIGO)
go

CREATE TABLE CAF_INV (
       IV_CODIGO            NumeroEmpleado,
       CF_FECHA             Fecha,
       CF_HORA              Hora,
       CF_TIPO              Codigo1,
       CF_COMIDAS           FolioChico,
       CF_EXTRAS            FolioChico,
       CF_RELOJ             RelojLinx,
       US_CODIGO            Usuario,
       CL_CODIGO            FolioChico,
       CF_REG_EXT           Booleano,
	   CF_OFFLINE			Booleano
)
go

ALTER TABLE CAF_INV
       ADD CONSTRAINT PK_CAF_INV PRIMARY KEY (IV_CODIGO, CF_FECHA, CF_HORA, CF_TIPO)
go

CREATE TABLE CAF_COME (
       CB_CODIGO            NumeroEmpleado,
       CF_FECHA             Fecha,
       CF_HORA              Hora,
       CF_TIPO              Codigo1,
       CF_COMIDAS           FolioChico,
       CF_EXTRAS            FolioChico,
       CF_RELOJ             RelojLinx,
       US_CODIGO            Usuario,
       CL_CODIGO            FolioChico,
       CF_REG_EXT           Booleano,
	   CF_OFFLINE			Booleano
)
go

ALTER TABLE CAF_COME
       ADD CONSTRAINT PK_CAF_COME PRIMARY KEY (CB_CODIGO, CF_FECHA, CF_HORA, CF_TIPO)
go

CREATE TABLE ASIGNA (
       AS_FECHA             Fecha,
       CB_CODIGO            NumeroEmpleado,
       CB_NIVEL             Codigo,
       US_CODIGO            Usuario
)
go

CREATE INDEX XIE1ASIGNA ON ASIGNA
(
       AS_FECHA,
       CB_NIVEL
)
go


ALTER TABLE ASIGNA
       ADD CONSTRAINT PK_ASIGNA PRIMARY KEY (AS_FECHA, CB_CODIGO)
go

CREATE TABLE SUPER (
       US_CODIGO            Usuario,
       CB_NIVEL             Codigo
)
go


ALTER TABLE SUPER
       ADD CONSTRAINT PK_SUPER PRIMARY KEY (US_CODIGO, CB_NIVEL)
go

CREATE TABLE MOV_GRAL (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       CO_NUMERO            Concepto,
       MG_FIJO              Booleano,
       MG_MONTO             Pesos,
       US_CODIGO            Usuario
)
go


ALTER TABLE MOV_GRAL
       ADD CONSTRAINT PK_MOV_GRAL PRIMARY KEY (PE_YEAR, PE_TIPO, PE_NUMERO, CO_NUMERO)
go

CREATE TABLE ENTNIVEL (
       PU_CODIGO            Codigo,
       CU_CODIGO            Codigo,
       ET_CODIGO            Codigo
)
go


ALTER TABLE ENTNIVEL
       ADD CONSTRAINT PK_ENTNIVEL PRIMARY KEY (PU_CODIGO, CU_CODIGO, ET_CODIGO)
go

CREATE TABLE TMPFOLIO (
       TO_USER              Usuario,
       TO_FOLIO             Empleados,
       CB_CODIGO            NumeroEmpleado,
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero
)
go


ALTER TABLE TMPFOLIO
       ADD CONSTRAINT PK_TMPFOLIO PRIMARY KEY (TO_USER, TO_FOLIO, CB_CODIGO)
go

CREATE TABLE TMPCALHR (
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       TL_YEAR              Anio,
       TL_MES               Mes,
       TL_CAMPO             FolioChico,
       TL_FEC_MIN           Fecha,
       TL_FEC_MAX           Fecha,
       TL_NUMERO            Dias,
       TL_CAMPO01           Horas,
       TL_CAMPO02           Horas,
       TL_CAMPO03           Horas,
       TL_CAMPO04           Horas,
       TL_CAMPO05           Horas,
       TL_CAMPO06           Horas,
       TL_CAMPO07           Horas,
       TL_CAMPO08           Horas,
       TL_CAMPO09           Horas,
       TL_CAMPO10           Horas,
       TL_CAMPO11           Horas,
       TL_CAMPO12           Horas,
       TL_CAMPO13           Horas,
       TL_CAMPO14           Horas,
       TL_CAMPO15           Horas,
       TL_CAMPO16           Horas,
       TL_CAMPO17           Horas,
       TL_CAMPO18           Horas,
       TL_CAMPO19           Horas,
       TL_CAMPO20           Horas,
       TL_CAMPO21           Horas,
       TL_CAMPO22           Horas,
       TL_CAMPO23           Horas,
       TL_CAMPO24           Horas,
       TL_CAMPO25           Horas,
       TL_CAMPO26           Horas,
       TL_CAMPO27           Horas,
       TL_CAMPO28           Horas,
       TL_CAMPO29           Horas,
       TL_CAMPO30           Horas,
       TL_CAMPO31           Horas,
       TL_TIPO01            Codigo3,
       TL_TIPO02            Codigo3,
       TL_TIPO03            Codigo3,
       TL_TIPO04            Codigo3,
       TL_TIPO05            Codigo3,
       TL_TIPO06            Codigo3,
       TL_TIPO07            Codigo3,
       TL_TIPO08            Codigo3,
       TL_TIPO09            Codigo3,
       TL_TIPO10            Codigo3,
       TL_TIPO11            Codigo3,
       TL_TIPO12            Codigo3,
       TL_TIPO13            Codigo3,
       TL_TIPO14            Codigo3,
       TL_TIPO15            Codigo3,
       TL_TIPO16            Codigo3,
       TL_TIPO17            Codigo3,
       TL_TIPO18            Codigo3,
       TL_TIPO19            Codigo3,
       TL_TIPO20            Codigo3,
       TL_TIPO21            Codigo3,
       TL_TIPO22            Codigo3,
       TL_TIPO23            Codigo3,
       TL_TIPO24            Codigo3,
       TL_TIPO25            Codigo3,
       TL_TIPO26            Codigo3,
       TL_TIPO27            Codigo3,
       TL_TIPO28            Codigo3,
       TL_TIPO29            Codigo3,
       TL_TIPO30            Codigo3,
       TL_TIPO31            Codigo3,
       TL_HABIL             Dias,
       TL_SABADO            Dias,
       TL_DESCAN            Dias,
       TL_NORMAL            Dias,
       TL_INCAPA            Dias,
       TL_VACA              Dias,
       TL_PER_CG            Dias,
       TL_PER_SG            Dias,
       TL_PER_FJ            Dias,
       TL_SUSPEN            Dias,
       TL_PER_OT            Dias,
       TL_FESTIVO           Dias,
       TL_NO_TRAB           Dias,
       TL_TRABAJO           Dias,
       TL_FALTAS            Dias,
       TL_RETARDO           Dias,
       TL_HORAS             Horas,
       TL_EXTRAS            Horas,
       TL_DOBLES            Horas,
       TL_DES_TRA           Horas,
       TL_HOR_CG            Horas,
       TL_HOR_SG            Horas,
       TL_TARDES            Horas
)
go


ALTER TABLE TMPCALHR
       ADD CONSTRAINT PK_TMPCALHR PRIMARY KEY (US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES, 
              TL_CAMPO)
go

CREATE TABLE SUSCRIP (
       RE_CODIGO            FolioChico,
       US_CODIGO            Usuario,
       SU_FRECUEN           Status,
       SU_ENVIAR            Status,
       SU_VACIO             Status
)
go

ALTER TABLE SUSCRIP
       ADD CONSTRAINT PK_SUSCRIP PRIMARY KEY (RE_CODIGO, US_CODIGO)
go

CREATE TABLE TMPBALA2 (
  TP_USER USUARIO,
  TP_NIVEL1 CODIGO,
  TP_NIVEL2 CODIGO,
  TP_NIVEL3 CODIGO,
  TP_NIVEL4 CODIGO,
  TP_NIVEL5 CODIGO,
  TP_ORDEN FOLIOCHICO,
  CB_CODIGO NUMEROEMPLEADO,
  TP_NUM_PER CONCEPTO,
  TP_DES_PER DESCRIPCION,
  TP_MON_PER PESOS,
  TP_HOR_PER HORAS,
  TP_REF_PER REFERENCIA,
  TP_NUM_DED CONCEPTO,
  TP_DES_DED DESCRIPCION,
  TP_MON_DED PESOS,
  TP_REF_DED REFERENCIA,
  TP_HOR_DED HORAS,
  TP_TOTAL NUMEROEMPLEADO,
  PE_YEAR ANIO,
  PE_TIPO NOMINATIPO,
  PE_NUMERO NOMINANUMERO,
  TP_EMPLEAD OBSERVACIONES
)
GO

ALTER TABLE TMPBALA2
  ADD PRIMARY KEY (TP_USER, TP_NIVEL1, TP_NIVEL2, TP_NIVEL3, TP_NIVEL4, TP_NIVEL5, CB_CODIGO, TP_ORDEN)
GO

CREATE TABLE PTOFIJAS(
       PU_CODIGO           Codigo,
       PF_FOLIO		   FolioChico,
       PF_CODIGO	   Codigo2
)
GO

ALTER TABLE PTOFIJAS
      ADD CONSTRAINT PK_PTOFIJAS PRIMARY KEY (PU_CODIGO, PF_FOLIO)
GO

CREATE TABLE PTOTOOLS(
       PU_CODIGO      	   Codigo,
       TO_CODIGO	   Codigo,
       KT_REFEREN	   Mascara
)
GO

ALTER TABLE PTOTOOLS
      ADD CONSTRAINT PK_PTOTOOLS PRIMARY KEY (PU_CODIGO, TO_CODIGO)
GO

create table FAM_PTO (
       FP_CODIGO            Codigo,
       FP_DESCRIP           Descripcion,
       FP_INGLES            Descripcion,
       FP_NUMERO            Pesos,
       FP_TEXTO             Descripcion )
GO

alter table FAM_PTO ADD CONSTRAINT PK_FAM_PTO PRIMARY KEY (FP_CODIGO)
GO

create table NIV_PTO (
       NP_CODIGO            Codigo,
       NP_DESCRIP           Descripcion,
       NP_INGLES            Descripcion,
       NP_NUMERO            Pesos,
       NP_TEXTO             Descripcion,
       NP_ACTITUD           Observaciones,
       NP_DETALLE           Memo )
GO

alter table NIV_PTO ADD CONSTRAINT PK_NIV_PTO PRIMARY KEY (NP_CODIGO)
GO

create table DIMENSIO (
       DM_CODIGO            Codigo,
       DM_DESCRIP           Descripcion,
       DM_INGLES            Descripcion,
       DM_NUMERO            Pesos,
       DM_TEXTO             Descripcion )
GO

alter table DIMENSIO ADD CONSTRAINT PK_DIMENSIO PRIMARY KEY (DM_CODIGO)
GO

create table PTO_DIME (
       PU_CODIGO            Codigo,
       DM_CODIGO            Codigo,
       PD_DESCRIP           Formula )
GO

alter table PTO_DIME ADD CONSTRAINT PK_PTO_DIME PRIMARY KEY (PU_CODIGO, DM_CODIGO)
GO

create table CALIFICA (
       CA_CODIGO            Codigo,
       CA_DESCRIP           Descripcion,
       CA_INGLES            Descripcion,
       CA_NUMERO            Pesos,
       CA_TEXTO             Descripcion,
       CA_ORDEN             Status,
       CA_GRUPO             Referencia )
GO

alter table CALIFICA ADD CONSTRAINT PK_CALIFICA PRIMARY KEY (CA_CODIGO)
GO

create table TCOMPETE (
       TC_CODIGO            Codigo,
       TC_DESCRIP           Descripcion,
       TC_INGLES            Descripcion,
       TC_NUMERO            Pesos,
       TC_TEXTO             Descripcion,
       TC_TIPO              Status )
GO

alter table TCOMPETE ADD CONSTRAINT PK_TCOMPETE PRIMARY KEY (TC_CODIGO)
GO

create table COMPETEN (
       CM_CODIGO            Codigo,
       TC_CODIGO            Codigo,
       CM_DESCRIP           Descripcion,
       CM_INGLES            Descripcion,
       CM_NUMERO            Pesos,
       CM_TEXTO             Descripcion,
       CM_DETALLE           Memo )
GO

alter table COMPETEN ADD CONSTRAINT PK_COMPETEN PRIMARY KEY (CM_CODIGO)
GO

create table COMP_FAM (
       FP_CODIGO            Codigo,
       CM_CODIGO            Codigo,
       CF_OBSERVA           Observaciones )
GO

alter table COMP_FAM ADD CONSTRAINT PK_COMP_FAM PRIMARY KEY (FP_CODIGO, CM_CODIGO)
GO

create table COMP_CAL (
       CM_CODIGO            Codigo,
       CA_CODIGO            Codigo,
       MC_DESCRIP           Formula )
GO

alter table COMP_CAL ADD CONSTRAINT PK_COMP_CAL PRIMARY KEY (CM_CODIGO, CA_CODIGO)
GO

create table COMP_PTO (
       PU_CODIGO            Codigo,
       CM_CODIGO            Codigo,
       CA_CODIGO            Codigo,
       MP_DESCRIP           Formula )
GO

alter table COMP_PTO ADD CONSTRAINT PK_COMP_PTO PRIMARY KEY (PU_CODIGO, CM_CODIGO)
GO

create table EMP_COMP (
       CB_CODIGO            NumeroEmpleado,
       CM_CODIGO            Codigo,
       CA_CODIGO            Codigo,
       EC_OBSERVA           Formula,
       EC_FEC_MOD           Fecha,
       US_CODIGO            Usuario )
GO

alter table EMP_COMP ADD CONSTRAINT PK_EMP_COMP PRIMARY KEY (CB_CODIGO, CM_CODIGO)
GO

create table ACCION (
       AN_CODIGO            Codigo,
       CU_CODIGO            Codigo,
       AN_NOMBRE            Observaciones,
       AN_INGLES            Descripcion,
       AN_NUMERO            Pesos,
       AN_TEXTO             Descripcion,
       AN_CLASE             Status,
       AN_TIP_MAT           Descripcion,
       AN_DETALLE           Memo,
       AN_DIAS              Dias,
       AN_URL               PathArchivo )
GO

alter table ACCION ADD CONSTRAINT PK_ACCION PRIMARY KEY (AN_CODIGO)
GO

create table EMP_PLAN (
       CB_CODIGO            NumeroEmpleado,
       AN_CODIGO            Codigo,
       EP_FEC_INI           Fecha,
       EP_FEC_PRO           Fecha,
       EP_FEC_FIN           Fecha,
       EP_FEC_MOD           Fecha,
       US_CODIGO            Usuario,
       EP_TERMINO           Booleano,
       EP_OBSERVA           Formula,
       EP_RESULT            Pesos )
GO

alter table EMP_PLAN ADD CONSTRAINT PK_EMP_PLAN PRIMARY KEY (CB_CODIGO, AN_CODIGO)
GO

create table COMP_MAP (
       CM_CODIGO            Codigo,
       AN_CODIGO            Codigo,
       CM_OBSERVA           Formula,
       CM_ORDEN             Status )
GO

alter table COMP_MAP ADD CONSTRAINT PK_COMP_MAP PRIMARY KEY (CM_CODIGO, AN_CODIGO)
GO

create table NIV_DIME (
       DM_CODIGO            Codigo,
       NP_CODIGO            Codigo,
       ND_RESUMEN           Observaciones,
       ND_DESCRIP           Memo )
GO

alter table NIV_DIME ADD CONSTRAINT PK_NIV_DIME PRIMARY KEY (DM_CODIGO, NP_CODIGO)
GO

CREATE TABLE SESION (
       SE_FOLIO             FolioGrande,
       CU_CODIGO            Codigo,
       MA_CODIGO            Codigo,
       SE_LUGAR             Observaciones,
       SE_FEC_INI           Fecha,
       SE_HOR_INI           Hora,
       SE_FEC_FIN           Fecha,
       SE_HORAS             Horas,
       SE_HOR_FIN           Hora,
       SE_CUPO              FolioChico,
       SE_COSTO1            Pesos,
       SE_COSTO2            Pesos,
       SE_COSTO3            Pesos,
       SE_COMENTA           Observaciones,
       SE_REVISIO           NombreCampo,
       SE_STATUS            Status,
       US_CODIGO            Usuario,
       SE_EST               Codigo
)
GO

alter table SESION ADD CONSTRAINT PK_SESION PRIMARY KEY (SE_FOLIO)
GO

CREATE TABLE MISREPOR (
       US_CODIGO            Usuario,
       RE_CODIGO	          FolioChico )
GO

ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

CREATE TABLE TMPDIMM (
       TD_YEAR              Anio,
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       TD_CUANTOS           FolioChico,
       TD_REPETID           NumeroEmpleado,
       TD_DATA_01           Pesos,
       TD_DATA_02           Pesos,
       TD_DATA_03           Pesos,
       TD_DATA_04           Pesos,
       TD_DATA_05           Pesos,
       TD_DATA_06           Pesos,
       TD_DATA_07           Pesos,
       TD_DATA_08           Pesos,
       TD_DATA_09           Pesos,
       TD_DATA_10           Pesos,
       TD_DATA_11           Pesos,
       TD_DATA_12           Pesos,
       TD_DATA_13           Pesos,
       TD_DATA_14           Pesos,
       TD_DATA_15           Pesos,
       TD_DATA_16           Pesos,
       TD_DATA_17           Pesos,
       TD_DATA_18           Pesos,
       TD_DATA_19           Pesos,
       TD_DATA_20           Pesos,
       TD_DATA_21           Pesos,
       TD_DATA_22           Pesos,
       TD_DATA_23           Pesos,
       TD_DATA_24           Pesos,
       TD_DATA_25           Pesos,
       TD_DATA_26           Pesos,
       TD_DATA_27           Pesos,
       TD_DATA_28           Pesos,
       TD_DATA_29           Pesos,
       TD_DATA_30           Pesos,
       TD_DATA_31           Pesos,
       TD_DATA_32           Pesos,
       TD_DATA_33           Pesos,
       TD_DATA_34           Pesos,
       TD_DATA_35           Pesos,
       TD_DATA_36           Pesos,
       TD_DATA_37           Pesos,
       TD_DATA_38           Pesos,
       TD_DATA_39           Pesos,
       TD_DATA_40           Pesos,
       TD_DATA_41           Pesos,
       TD_DATA_42           Pesos,
       TD_DATA_43           Pesos,
       TD_DATA_44           Pesos,
       TD_DATA_45           Pesos,
       TD_DATA_46           Pesos,
       TD_DATA_47           Pesos,
       TD_DATA_48           Pesos,
       TD_DATA_49           Pesos,
       TD_DATA_50           Pesos,
       TD_DATA_51           Pesos,
       TD_DATA_52           Pesos,
       TD_DATA_53           Pesos,
       TD_DATA_54           Pesos,
       TD_DATA_55           Pesos,
       TD_DATA_56           Pesos,
       TD_DATA_57           Pesos,
       TD_DATA_58           Pesos,
       TD_DATA_59           Pesos,
       TD_DATA_60           Pesos,
       TD_DATA_61           Pesos,
       TD_DATA_62           Pesos,
       TD_DATA_63           Pesos,
       TD_DATA_64           Pesos,
       TD_DATA_65           Pesos,
       TD_DATA_66           Pesos,
       TD_DATA_67           Pesos,
       TD_DATA_68           Pesos,
       TD_DATA_69           Pesos,
       TD_DATA_70           Pesos,
       TD_DATA_71           Pesos,
       TD_DATA_72           Pesos,
       TD_DATA_73           Pesos,
       TD_DATA_74           Pesos,
       TD_DATA_75           Pesos,
       TD_DATA_76           Pesos,
       TD_DATA_77           Pesos,
       TD_DATA_78           Pesos,
       TD_DATA_79           Pesos,
       TD_DATA_80           Pesos,
       TD_DATA_81           Pesos,
       TD_DATA_82           Pesos,
       TD_DATA_83           Pesos,
       TD_DATA_84           Pesos,
       TD_DATA_85           Pesos,
       TD_DATA_86           Pesos,
       TD_DATA_87           Pesos,
       TD_DATA_88           Pesos,
       TD_DATA_89           Pesos,
       TD_DATA_90           Pesos,
       TD_DATA_91           Pesos,
       TD_DATA_92           Pesos,
       TD_DATA_93           Pesos,
       TD_DATA_94           Pesos,
       TD_DATA_95           Pesos,
       TD_DATA_96           Pesos,
       TD_DATA_97           Pesos,
       TD_DATA_98           Pesos,
       TD_DATA_99           Pesos,
       TD_DATA_A0           Pesos,
       TD_DATA_A1           Pesos,
       TD_DATA_A2           Pesos,
       TD_TEXT_01           Formula,
       TD_TEXT_02           Formula,
       TD_TEXT_03           Formula,
       TD_TEXT_04           Formula,
       TD_TEXT_05           Formula,
       TD_SELLO             Formula,
       TD_ORIG              Memo,
       TD_DIGES             Observaciones,
       TD_FEC_SEL           Fecha,
       TD_HOR_SEL           Hora,
       TD_MES_INI           Anio,
       TD_MES_FIN           Anio
)
GO

ALTER TABLE TMPDIMM ADD CONSTRAINT PK_TMPDIMM PRIMARY KEY (US_CODIGO, TD_YEAR, CB_CODIGO)
GO

CREATE TABLE EMP_PROG (
       CB_CODIGO            NumeroEmpleado,
       CU_CODIGO            Codigo,
       EP_DIAS              Dias,
       EP_OPCIONA           Booleano,
       EP_FECHA             Fecha,
       EP_PORDIAS           Booleano,
       EP_GLOBAL            Booleano )
GO

ALTER TABLE EMP_PROG ADD CONSTRAINT PK_EMP_PROG PRIMARY KEY ( CB_CODIGO, CU_CODIGO )
GO

CREATE TABLE ACCREGLA (
       AE_CODIGO            FolioChico,
       AE_TIPO              Status,
       AE_LETRERO           DescLarga,
       AE_FORMULA           Formula,
       QU_CODIGO            Condicion,
       AE_FILTRO            Formula,
       AE_SEVERI            Status,
       AE_ACTIVO            Booleano,
       US_CODIGO            Usuario
)
GO

ALTER TABLE ACCREGLA ADD CONSTRAINT PK_ACCREGLA PRIMARY KEY (AE_CODIGO)
GO

CREATE TABLE ACCESLOG (
       CB_CODIGO            NumeroEmpleado,
       AL_FECHA             Fecha,
       AL_HORA              Hora,
       AL_ENTRADA           Booleano,
       AL_OK_SIST           Booleano,
       AL_OK_MAN            Booleano,
       AL_OK_FEC            Fecha,
       AL_OK_HOR            Hora,
       AL_COMENT            Observaciones,
       AE_CODIGO            Codigo,
       AL_CASETA            Codigo,
       US_CODIGO            Usuario        
)
GO

ALTER TABLE ACCESLOG ADD CONSTRAINT PK_ACCESLOG PRIMARY KEY (CB_CODIGO, AL_FECHA, AL_HORA)
GO

create table CLASITMP(
       CB_CODIGO 	          NumeroEmpleado,
       AU_FECHA		          Fecha,
       CB_CLASIFI           Codigo,
       CB_TURNO             Codigo,
       CB_PUESTO            Codigo,
       CB_NIVEL1            Codigo,
       CB_NIVEL2            Codigo,
       CB_NIVEL3            Codigo,
       CB_NIVEL4            Codigo,
       CB_NIVEL5            Codigo,
       CB_NIVEL6            Codigo,
       CB_NIVEL7            Codigo,
       CB_NIVEL8            Codigo,
       CB_NIVEL9            Codigo,
       US_CODIGO	          Usuario,
       CT_FECHA		          Fecha,
       US_COD_OK	          Usuario,
       CT_FEC_OK	          Fecha
)
GO

ALTER TABLE CLASITMP ADD CONSTRAINT PK_CLASITMP PRIMARY KEY (CB_CODIGO, AU_FECHA)
GO

CREATE TABLE COLONIA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_CODPOST           Referencia,
       TB_CLINICA           Codigo3,
       TB_ZONA	            Referencia,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_NIVEL0            Formula
)
GO

ALTER TABLE COLONIA
       ADD CONSTRAINT PK_COLONIA PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE GRUPO_AD (
       GX_CODIGO            Codigo,
       GX_TITULO            Observaciones,
       GX_POSICIO           FolioChico
)
GO

ALTER TABLE GRUPO_AD
       ADD CONSTRAINT PK_GRUPO_AD PRIMARY KEY (GX_CODIGO)
GO

CREATE TABLE CAMPO_AD (
       CX_NOMBRE            NombreCampo,
       GX_CODIGO            Codigo,
       CX_POSICIO           FolioChico,
       CX_TITULO            Descripcion,
       CX_TIPO              Status,
       CX_DEFAULT           Formula,
       CX_MOSTRAR           Status,
       CX_ENTIDAD           Status,
       CX_OBLIGA 	    Booleano
)
GO

ALTER TABLE CAMPO_AD
       ADD CONSTRAINT PK_CAMPO_AD PRIMARY KEY (CX_NOMBRE)
GO

CREATE TABLE EXTRA5 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA5
       ADD CONSTRAINT PK_EXTRA5 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA6 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA6
       ADD CONSTRAINT PK_EXTRA6 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA7 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA7
       ADD CONSTRAINT PK_EXTRA7 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA8 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA8
       ADD CONSTRAINT PK_EXTRA8 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA9 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA9
       ADD CONSTRAINT PK_EXTRA9 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA10 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA10
       ADD CONSTRAINT PK_EXTRA10 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA11 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA11
       ADD CONSTRAINT PK_EXTRA11 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA12 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA12
       ADD CONSTRAINT PK_EXTRA12 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA13 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA13
       ADD CONSTRAINT PK_EXTRA13 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE EXTRA14 (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE EXTRA14
       ADD CONSTRAINT PK_EXTRA14 PRIMARY KEY(TB_CODIGO)
GO

CREATE TABLE CURSOPRE (
       CU_CODIGO            Codigo,
       CP_CURSO             Codigo,
       CP_COMENTA           Formula,
       CP_OPCIONA           Booleano
)
GO

ALTER TABLE CURSOPRE
       ADD CONSTRAINT PK_CURSOPRE PRIMARY KEY (CU_CODIGO, CP_CURSO)
GO

CREATE TABLE AULA (
       AL_CODIGO            Codigo,
       AL_NOMBRE            Descripcion,
       AL_INGLES            Descripcion,
       AL_NUMERO            Pesos,
       AL_TEXTO             Descripcion,
       AL_CUPO              Empleados,
       AL_DESCRIP           Formula,
       AL_ACTIVA            Booleano
)
GO

ALTER TABLE AULA
       ADD CONSTRAINT PK_AULA PRIMARY KEY (AL_CODIGO)
GO

CREATE TABLE RESERVA (
       RV_FOLIO             FolioGrande,
       AL_CODIGO            Codigo,
       SE_FOLIO             FolioGrande,
       MA_CODIGO            Codigo,
       RV_FEC_INI           Fecha,
       RV_HOR_INI           Hora,
       RV_FEC_FIN           Fecha,
       RV_HOR_FIN           Hora,
       RV_DURACIO           Horas,
       RV_RESUMEN           Observaciones,
       RV_DETALLE           Memo,
       US_CODIGO            Usuario,
       RV_FEC_RES           Fecha,
       RV_HOR_RES           Hora,
       RV_TIPO              Status,
       RV_LISTA             Booleano,
       RV_ORDEN             Referencia
)
GO

ALTER TABLE RESERVA
       ADD CONSTRAINT PK_RESERVA PRIMARY KEY (RV_FOLIO)
GO

CREATE TABLE INSCRITO (
       SE_FOLIO             FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       IC_STATUS            Status,
       IC_FEC_BAJ           Fecha,
       IC_COMENTA           Observaciones,
       IC_EVA_1             Numerico,
       IC_EVA_2             Numerico,
       IC_EVA_3             Numerico,
       IC_EVA_FIN           Numerico,
       US_CODIGO            Usuario,
       IC_FEC_INS           Fecha,
       IC_HOR_INS           Hora
)
GO

ALTER TABLE INSCRITO
       ADD CONSTRAINT PK_INSCRITO PRIMARY KEY (SE_FOLIO, CB_CODIGO)
GO

CREATE TABLE CUR_ASIS (
       RV_FOLIO             FolioGrande,
       SE_FOLIO             FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       CS_ASISTIO           Booleano,
       CS_EVA_1             Numerico,
       CS_EVA_2             Numerico,
       CS_EVA_3             Numerico,
       CS_EVA_FIN           Numerico,
       CS_COMENTA           Observaciones
)
GO

ALTER TABLE CUR_ASIS
       ADD CONSTRAINT PK_CUR_ASIS PRIMARY KEY (RV_FOLIO, SE_FOLIO, CB_CODIGO)
GO

create table PLAZA (
       PL_FOLIO             FolioGrande  IDENTITY (1, 1),
       PL_ORDEN             FolioChico,
       PL_CODIGO            Descripcion,
       PL_NOMBRE            Observaciones,
       PL_INGLES            Descripcion,
       PL_NUMERO            Pesos,
       PL_TEXTO             Descripcion,
       PL_REPORTA           FolioGrande,
       PL_TIPO              Status,
       PL_TIREP             Status,
       PL_FEC_INI           Fecha,
       PL_FEC_FIN           Fecha,
       PL_SUB_CTA           Descripcion,
       CB_CODIGO            NumeroEmpleado,
       PU_CODIGO            Codigo,
       PL_CLASIFI           Codigo,
       PL_TURNO             Codigo,
       PL_PATRON            RegPatronal,
       PL_NIVEL0            Codigo,
       PL_NIVEL1            Codigo,
       PL_NIVEL2            Codigo,
       PL_NIVEL3            Codigo,
       PL_NIVEL4            Codigo,
       PL_NIVEL5            Codigo,
       PL_NIVEL6            Codigo,
       PL_NIVEL7            Codigo,
       PL_NIVEL8            Codigo,
       PL_NIVEL9            Codigo,
       PL_CONTRAT           Codigo1,
       PL_AUTOSAL           Booleano,
       PL_SALARIO           PesosDiario,
       PL_PER_VAR           PesosDiario,
       PL_TABLASS           Codigo1,
       PL_ZONA_GE           ZonaGeo,
       PL_CHECA             Booleano,
       PL_AREA              Codigo,
       PL_NOMINA            NominaTipo
)
GO

alter table PLAZA add constraint PK_PLAZA primary key( PL_FOLIO )
GO

create unique index XAK1PLAZA on PLAZA ( PU_CODIGO, PL_ORDEN )
GO

create table POL_TIPO (
       PT_CODIGO            Codigo,
       PT_NOMBRE            DescLarga,
       PT_INGLES            DescLarga,
       PT_NUMERO            Pesos,
       PT_TEXTO             DescLarga,
       PT_REPORTE           FolioChico
)
GO

alter table POL_TIPO add constraint PK_POL_TIPO primary key( PT_CODIGO )
GO

create table POL_HEAD (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PT_CODIGO            Codigo,
       US_CODIGO            Usuario,
       PH_FECHA             Fecha,
       PH_HORA              Hora,
       PH_VECES             FolioChico,
       PH_REPORTE           FolioChico,
       PH_STATUS            Status
)
GO

alter table POL_HEAD add constraint PK_POL_HEAD
      primary key( PE_YEAR, PE_TIPO, PE_NUMERO, PT_CODIGO )
GO

CREATE TABLE POLIZA (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PT_CODIGO            Codigo,
       TP_FOLIO             FolioChico,
       TP_CUENTA            Formula,
       TP_CARGO             PesosEmpresa,
       TP_ABONO             PesosEmpresa,
       TP_COMENTA           Descripcion,
       TP_CAR_ABO           Codigo1,
       TP_TEXTO             NombreCampo,
       TP_NUMERO            Status
)
go


ALTER TABLE POLIZA
       ADD CONSTRAINT PK_POLIZA PRIMARY KEY (PE_YEAR, PE_TIPO, PE_NUMERO, PT_CODIGO, TP_FOLIO)
go

create table CERTIFIC (
       CI_CODIGO            Codigo,
       CI_NOMBRE            DescLarga,
       CI_RESUMEN           Formula,
       CI_DETALLE           Memo,
       CI_INGLES            DescLarga,
       CI_NUMERO            Pesos,
       CI_TEXTO             DescLarga,
       CI_RENOVAR           Dias,
       CI_ACTIVO            Booleano
)
GO

alter table CERTIFIC add constraint PK_CERTIFIC primary key( CI_CODIGO )
GO

create table KAR_CERT (
       CB_CODIGO            NumeroEmpleado,
       KI_FEC_CER           Fecha,
       CI_CODIGO            Codigo,
       KI_FOLIO             Descripcion,
       KI_RENOVAR           Dias,
       KI_APROBO            Booleano,
       KI_CALIF_1           Numerico,
       KI_CALIF_2           Numerico,
       KI_CALIF_3           Numerico,
       KI_SINOD_1           DescLarga,
       KI_SINOD_2           DescLarga,
       KI_SINOD_3           DescLarga,
       KI_OBSERVA           Memo,
       US_CODIGO            Usuario
)
GO

alter table KAR_CERT add constraint PK_KAR_CERT
      primary key( CB_CODIGO, KI_FEC_CER, CI_CODIGO )
GO

create table DESCTIPO(
       DT_CODIGO            Codigo,
       DT_NOMBRE            DescLarga,
       DT_INGLES            DescLarga,
       DT_ORDEN             FolioChico,
       DT_NUMERO            Pesos,
       DT_TEXTO             DescLarga
)
GO

alter table DESCTIPO add constraint PK_DESCTIPO primary key( DT_CODIGO )
GO

create unique index XAK1DESCTIPO on DESCTIPO( DT_ORDEN )
GO

create table PERFIL (
       PU_CODIGO            Codigo,
       PF_DESCRIP           Memo,
       PF_OBJETIV           Memo,
       PF_POSTING           Memo,
       PF_ESTUDIO           Status,
       PF_FECHA             Fecha,
       PF_EXP_PTO           FolioChico,
       PF_CONTRL1           DescLarga,
       PF_CONTRL2           DescLarga,
       PF_NUMERO1           Pesos,
       PF_TEXTO1            DescLarga,
       PF_NUMERO2           Pesos,
       PF_EDADMIN           Anio,
       PF_EDADMAX           Anio,
       PF_TEXTO2            DescLarga,
       PF_SEXO              Codigo1
)
GO

alter table PERFIL add constraint PK_PERFIL primary key( PU_CODIGO )
GO

create table DESC_PTO(
       PU_CODIGO            Codigo,
       DT_CODIGO            Codigo,
       DP_ORDEN             FolioChico,
       DP_TEXT_01           Formula,
       DP_TEXT_02           Formula,
       DP_TEXT_03           Formula,
       DP_MEMO_01           Memo,
       DP_MEMO_02           Memo,
       DP_MEMO_03           Memo,
       DP_NUME_01           Pesos,
       DP_FECH_01           Fecha
)
GO

alter table DESC_PTO add constraint PK_DESC_PTO
      primary key( PU_CODIGO, DT_CODIGO, DP_ORDEN )
GO

create table DESC_FLD (
       DT_CODIGO            Codigo,
       DF_ORDEN             FolioChico,
       DF_TITULO            DescLarga,
       DF_CAMPO             NombreCampo,
       DF_CONTROL           Status,
       DF_LIMITE            FolioGrande,
       DF_VALORES           Memo
)
GO

alter table DESC_FLD add constraint PK_DESC_FLD
      primary key (DT_CODIGO, DF_ORDEN)
GO

create table VACAPLAN (
       CB_CODIGO            NumeroEmpleado,
       VP_FEC_INI           Fecha,
       VP_FEC_FIN           Fecha,
       VP_DIAS              DiasFrac,
       VP_SOL_COM           Formula,
       VP_SOL_USR           Usuario,
       VP_SOL_FEC           Fecha,
       VP_STATUS            Status,
       VP_AUT_COM           Formula,
       VP_AUT_USR           Usuario,
       VP_AUT_FEC           Fecha,
       VP_NOMYEAR           Anio,
       VP_NOMTIPO           NominaTipo,
       VP_NOMNUME           NominaNumero,
       VP_SAL_ANT           DiasFrac,
       VP_SAL_PRO           DiasFrac,
       VP_PAGO_US           Booleano
)
GO

alter table VACAPLAN add constraint PK_VACAPLAN
      primary key( CB_CODIGO, VP_FEC_INI )
GO

create table TCTAMOVS(
       TB_CODIGO            Codigo,
       TB_ELEMENT           DescLarga,
       TB_INGLES            DescLarga,
       TB_NUMERO            Pesos,
       TB_TEXTO             DescLarga,
       TB_SISTEMA           Booleano
)
GO

alter table TCTAMOVS add constraint PK_TCTAMOVS primary key( TB_CODIGO )
GO

create table CTABANCO(
       CT_CODIGO            Codigo,
       CT_NUM_CTA           DescLarga,
       CT_NOMBRE            DescLarga,
       CT_BANCO             DescLarga,
       CT_NUMERO            Pesos,
       CT_TEXTO             Descripcion,
       AH_TIPO              Codigo1,
       CT_STATUS            Status,
       CT_REP_CHK           FolioChico,
       CT_REP_LIQ           FolioChico
)
GO

alter table CTABANCO add constraint PK_CTABANCO primary key( CT_CODIGO )
GO

create table CTA_MOVS(
       CM_FOLIO             FolioGrande IDENTITY( 1, 1 ),
       CT_CODIGO            Codigo,
       CM_TIPO              Codigo,
       CM_DEP_RET           Codigo1,
       CM_MONTO             Pesos,
       CM_DESCRIP           Observaciones,
       CM_CHEQUE            FolioGrande,
       CM_FECHA             Fecha,
       CM_BENEFI            Observaciones,
       CM_STATUS            Status,
       CM_TOT_AHO           Pesos,
       CM_INTERES           Pesos,
       CM_SAL_PRE           Pesos,
       CB_CODIGO            NumeroEmpleado,
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PR_REFEREN           Referencia,
       PR_TIPO              Codigo1,
       CM_PRESTA            Status,
       CM_DEPOSIT           as ( case CM_DEP_RET when 'D' then CM_MONTO else 0 end ),
       CM_RETIRO            as ( case CM_DEP_RET when 'D' then 0 else CM_MONTO end )
)
GO

alter table CTA_MOVS add constraint PK_CTA_MOVS primary key( CM_FOLIO )
GO

create index XIE1CTA_MOVS on CTA_MOVS( CT_CODIGO, CM_FECHA, CM_DEP_RET )
GO

create table REP_EMPS(
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado
)
GO

alter table REP_EMPS add constraint PK_REP_EMPS primary key( US_CODIGO, CB_CODIGO )
GO

CREATE TABLE TMPPROMVAR (
       CB_CODIGO           NumeroEmpleado,
       CB_FECHA            Fecha,
       CB_TIPO             Codigo,
       TV_DIAS             DiasFrac,
       TV_PROM             PesosDiario,
       TV_TOT_PRO          PesosDiario,
       CO_NUMERO           Concepto,
       TV_MONTO            PesosDiario		  
)
GO

alter table TMPPROMVAR add constraint PK_TMPPROMVAR primary key( CB_CODIGO, CB_FECHA,CB_TIPO,CO_NUMERO )
GO

CREATE TABLE PROV_CAP (
	PC_CODIGO	Codigo,
	PC_NOMBRE	DescLarga,
	PC_DIRECCI	DescLarga,
	PC_CIUDAD	Descripcion,
	PC_ESTADO	Codigo2,
	PC_PAIS		Descripcion,
	PC_CODPOST	Referencia,
	PC_RFC		Descripcion,
	PC_CONTACT	Descripcion,
	PC_TEL		Descripcion,
	PC_FAX		Descripcion,
	PC_EMAIL	DescLarga,
	PC_WEB		PathURL,
	PC_NUMERO	Pesos,
	PC_TEXTO	Descripcion,
	PC_ACTIVO	Booleano
)
GO

alter table PROV_CAP add constraint PK_PROV_CAP primary key( PC_CODIGO )
GO

CREATE TABLE R_LISTAVAL (
       LV_CODIGO            FolioChico,
       LV_NOMBRE            Descripcion,
	LV_ACTIVO 		Booleano, 
	LV_VERSION 		FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_LISTAVAL
       ADD PRIMARY KEY (LV_CODIGO)
GO

CREATE UNIQUE INDEX XAK1R_LISTAVAL ON R_LISTAVAL
(
       LV_NOMBRE
)
GO

CREATE TABLE R_VALOR (
       LV_CODIGO            FolioChico,
       VL_CODIGO            FolioChico,
       VL_DESCRIP           Descripcion,
	VL_VERSION 		FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_VALOR
       ADD PRIMARY KEY (LV_CODIGO, VL_CODIGO)
GO

CREATE TABLE R_ENTIDAD (
       EN_CODIGO            FolioChico,
       EN_TITULO            Observaciones,
       EN_TABLA             NombreCampo,
       EN_DESCRIP           Formula,
       EN_PRIMARY           Formula,
       EN_ATDESC	          NombreCampo,
       EN_ALIAS	            FolioChico,
	     EN_ACTIVO	          Booleano,
	     EN_VERSION 		      FolioChico,
	     US_CODIGO 		        Usuario,
       EN_NIVEL0            Booleano
)
GO

ALTER TABLE R_ENTIDAD
       ADD PRIMARY KEY (EN_CODIGO)
GO

CREATE TABLE R_ATRIBUTO (
       EN_CODIGO            FolioChico,
       AT_CAMPO             NombreCampo,
       AT_TITULO            Observaciones,
       AT_TIPO              Status,
       AT_ANCHO             FolioChico,
       AT_MASCARA           Descripcion,
       AT_FILTRO            FolioChico,
  	   AT_FILTROA           Descripcion,
       AT_TRANGO            Status,
       AT_ENTIDAD           FolioChico,
       LV_CODIGO            FolioChico,
       AT_CLAVES            TituloCampo,
       AT_DESCRIP           Formula,
       AT_TCORTO            TituloCampo,
       AT_SISTEMA           Booleano,
       AT_VALORAC           FolioChico,
       AT_TOTAL 	          FolioChico,
       AT_CONFI	            Booleano,
	     AT_ACTIVO 	          Booleano,
	     AT_VERSION           FolioChico,
       US_CODIGO 	          Usuario
)
GO

ALTER TABLE R_ATRIBUTO
       ADD PRIMARY KEY (EN_CODIGO, AT_CAMPO)
GO

CREATE TABLE R_RELACION (
       EN_CODIGO            FolioChico,
       RL_ORDEN             FolioChico,
       RL_ENTIDAD           FolioChico,
       RL_CAMPOS            Formula,
       RL_INNER             Booleano,
	RL_ACTIVO 		Booleano, 
	RL_VERSION 		FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_RELACION
       ADD PRIMARY KEY (EN_CODIGO, RL_ORDEN)
GO

CREATE TABLE R_CLASIFI (
       RC_CODIGO            FolioChico,
       RC_NOMBRE            Descripcion,
       RC_ORDEN             FolioChico,
	RC_ACTIVO 		Booleano, 
   	RC_VERSION 		FolioChico, 
	RC_MODULO 		FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_CLASIFI
       ADD PRIMARY KEY (RC_CODIGO)
GO

CREATE UNIQUE INDEX XAK1R_CLASIFI ON R_CLASIFI
(
       RC_NOMBRE
)
GO

CREATE TABLE R_CLAS_ACC (
       CM_CODIGO            CodigoEmpresa,
       GR_CODIGO            FolioChico,
       RC_CODIGO            FolioChico,
       RA_DERECHO           FolioChico,
       RA_VERSION           FolioChico, 
       US_CODIGO 	    Usuario
)
GO

ALTER TABLE R_CLAS_ACC
       ADD PRIMARY KEY (CM_CODIGO, GR_CODIGO, RC_CODIGO)
GO

CREATE TABLE R_ENT_ACC (
       CM_CODIGO            CodigoEmpresa,
       GR_CODIGO            FolioChico,
       EN_CODIGO            FolioChico,
       RE_DERECHO           FolioChico,
	RE_VERSION 		FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_ENT_ACC
       ADD PRIMARY KEY (CM_CODIGO, GR_CODIGO, EN_CODIGO)
GO

CREATE TABLE R_CLAS_ENT (
       RC_CODIGO            FolioChico,
       CE_ORDEN             FolioChico,
       EN_CODIGO            FolioChico,
 	CE_VERSION          FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_CLAS_ENT
       ADD PRIMARY KEY (RC_CODIGO, CE_ORDEN)
GO

CREATE TABLE R_MODULO (
       MO_CODIGO            FolioChico,
       MO_NOMBRE            Descripcion,
       MO_ORDEN             FolioChico,
	MO_ACTIVO 		Booleano, 
	MO_VERSION 		FolioChico, 
	US_CODIGO		 Usuario
)
GO

ALTER TABLE R_MODULO
       ADD PRIMARY KEY (MO_CODIGO)
GO

CREATE UNIQUE INDEX XAK1R_MODULO ON R_MODULO
(
       MO_NOMBRE
)
GO

CREATE TABLE R_MOD_ENT (
       MO_CODIGO            FolioChico,
       ME_ORDEN             FolioChico,
       EN_CODIGO            FolioChico,
	ME_VERSION 		FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_MOD_ENT
       ADD PRIMARY KEY (MO_CODIGO, ME_ORDEN)
GO

CREATE TABLE R_ORDEN (
       EN_CODIGO           FolioChico,
       RO_ORDEN            FolioChico,
       RO_ENTIDAD          FolioChico,
       AT_CAMPO            NombreCampo,
	RO_VERSION 		FolioChico, 
	US_CODIGO 		Usuario
)
GO

ALTER TABLE R_ORDEN
       ADD PRIMARY KEY (EN_CODIGO,RO_ORDEN)
GO

CREATE TABLE R_FILTRO (
       EN_CODIGO           FolioChico,
       RF_ORDEN            FolioChico,
       RF_ENTIDAD          FolioChico,
       AT_CAMPO            NombreCampo,
	RF_VERSION 	FolioChico, 
	US_CODIGO 	Usuario
)
GO

ALTER TABLE R_FILTRO
       ADD PRIMARY KEY (EN_CODIGO,RF_ORDEN)
GO

CREATE TABLE R_DEFAULT (
       EN_CODIGO           FolioChico,
       RD_ORDEN            FolioChico,
       RD_ENTIDAD          FolioChico,
       AT_CAMPO            NombreCampo,
	RD_VERSION 	FolioChico, 
	US_CODIGO 	Usuario
)
GO

ALTER TABLE R_DEFAULT
       ADD PRIMARY KEY (EN_CODIGO,RD_ORDEN)
GO

CREATE TABLE VALPLANT (
       VL_CODIGO            FolioChico IDENTITY,
       VL_NOMBRE            DescLarga,
       VL_INGLES            DescLarga,
       VL_NUMERO            Pesos,
       VL_TEXTO             DescLarga,
       VL_FECHA             Fecha,
       VL_COMENTA           Memo,
       VL_TAB_PTS           FolioChico,
       VL_STATUS            Status                   
)
GO

ALTER TABLE VALPLANT
       ADD CONSTRAINT PK_VALPLANT PRIMARY KEY (VL_CODIGO)
GO

CREATE TABLE VFACTOR (
       VL_CODIGO            FolioChico,
       VF_ORDEN             FolioChico,
       VF_CODIGO            Referencia,
       VF_NOMBRE            DescLarga,
       VF_DESCRIP           Memo,
       VF_INGLES            DescLarga,
       VF_DES_ING           Memo,
       VF_NUMERO            Pesos,
       VF_TEXTO             DescLarga
)
GO

ALTER TABLE VFACTOR
       ADD CONSTRAINT PK_VFACTOR PRIMARY KEY (VL_CODIGO, VF_ORDEN)
GO

CREATE TABLE VSUBFACT (
       VL_CODIGO            FolioChico,
       VS_ORDEN             FolioChico,
       VF_ORDEN             FolioChico,
       VS_CODIGO            Referencia,
       VS_NOMBRE            DescLarga,
       VS_DESCRIP           Memo,
       VS_INGLES            DescLarga,
       VS_DES_ING           Memo,
       VS_NUMERO            Pesos,
       VS_TEXTO             DescLarga
)
GO

ALTER TABLE VSUBFACT
       ADD CONSTRAINT PK_VSUBFACT PRIMARY KEY (VL_CODIGO, VS_ORDEN)
GO

CREATE TABLE VALNIVEL (
       VL_CODIGO            FolioChico,
       VS_ORDEN             FolioChico,
       VN_ORDEN             FolioChico,
       VN_CODIGO            Referencia,
       VN_NOMBRE            DescLarga,
       VN_DESCRIP           Memo,
       VN_INGLES            DescLarga,
       VN_DES_ING           Memo,
       VN_NUMERO            Pesos,
       VN_TEXTO             DescLarga,
       VN_PUNTOS            Numerico,
	     VN_COLOR				      FolioGrande
)
GO

ALTER TABLE VALNIVEL
       ADD CONSTRAINT PK_VALNIVEL PRIMARY KEY (VL_CODIGO, VS_ORDEN, VN_ORDEN)
GO

CREATE TABLE VAL_PTO (
       VP_FOLIO             FolioGrande IDENTITY,
       VL_CODIGO            FolioChico,
       PU_CODIGO            Codigo,
       VP_FECHA             Fecha,
       VP_COMENTA           Memo,
       VP_GRADO             Numerico,
       VP_PT_CODE           Descripcion,
       VP_PT_NOM            DescLarga,
       VP_PT_GRAD           Numerico,
       VP_FEC_INI           Fecha,
       VP_FEC_FIN           Fecha,
       US_CODIGO            Usuario,
       VP_STATUS            Status
)
GO

CREATE UNIQUE INDEX XAK1VAL_PTO ON VAL_PTO
(
	PU_CODIGO,
	VP_FECHA
)
GO

ALTER TABLE VAL_PTO
       ADD CONSTRAINT PK_VAL_PTO PRIMARY KEY (VP_FOLIO)
GO

CREATE TABLE VPUNTOS (
       VP_FOLIO             FolioGrande,
       VT_ORDEN             FolioChico,
       VT_CUAL              FolioChico,
       VT_COMENTA           Memo
)
GO

ALTER TABLE VPUNTOS
       ADD CONSTRAINT PK_VPUNTOS PRIMARY KEY (VP_FOLIO, VT_ORDEN)
GO

CREATE TABLE FON_TOT (
       FT_YEAR              Anio,
       FT_MONTH             Mes,
       PR_TIPO              Codigo1,
       FT_STATUS            Status,
       FT_RETENC            Pesos,
       FT_NOMINA            Pesos,
       FT_TAJUST            Codigo1,
       FT_AJUSTE            Pesos,
       FT_EMPLEAD           Empleados,
       FT_CUANTOS           Empleados,
       FT_BAJAS             Empleados,
       FT_INCAPA            Empleados,
       FT_DETALLE 		      Memo
)
GO

ALTER TABLE FON_TOT
       ADD CONSTRAINT PK_FON_TOT PRIMARY KEY (FT_YEAR, FT_MONTH, PR_TIPO )
GO

CREATE TABLE FON_EMP (
       FT_YEAR              Anio,
       FT_MONTH             Mes,
       PR_TIPO              Codigo1,
       CB_CODIGO            NumeroEmpleado,
       FE_RETENC            Pesos,
       FE_NOMINA            Pesos,
       FE_AJUSTE            Pesos,
       FE_CUANTOS           Empleados,
       FE_INCIDE            Codigo1,
       FE_FECHA1            Fecha,
       FE_FECHA2            Fecha, 
	   FE_DIF_QTY			Empleados
)
GO

ALTER TABLE FON_EMP
       ADD CONSTRAINT PK_FON_EMP PRIMARY KEY NONCLUSTERED (FT_YEAR, FT_MONTH, PR_TIPO, CB_CODIGO )
GO

CREATE TABLE FON_CRE (
       FT_YEAR              Anio,
       FT_MONTH             Mes,
       PR_TIPO              Codigo1,
       CB_CODIGO            NumeroEmpleado,
       PR_REFEREN           Referencia,
       FC_RETENC            Pesos,
       FC_NOMINA            Pesos,
       FC_AJUSTE            Pesos, 
	   FC_PR_STA  			Status, 
	   FC_FON_MOT			Status
)
GO

ALTER TABLE FON_CRE
       ADD CONSTRAINT PK_FON_CRE PRIMARY KEY (FT_YEAR, FT_MONTH, PR_TIPO, CB_CODIGO, PR_REFEREN)
GO

CREATE TABLE RSOCIAL (
       RS_CODIGO            Codigo,
       RS_NOMBRE            Titulo,
       RS_CALLE             Titulo,
       RS_NUMEXT            Referencia,
       RS_NUMINT            Referencia,
       RS_COLONIA           Titulo,
       RS_CIUDAD            Titulo,
       RS_ENTIDAD           Codigo2,
       RS_CODPOST           Referencia,
       RS_TEL               Descripcion,
       RS_FAX               Descripcion,
       RS_EMAIL             DescLarga,
       RS_WEB               PathURL,
       RS_RFC               Descripcion,
       RS_INFO              Descripcion,
       RS_GUIA              Descripcion,
       RS_RLEGAL            Titulo,
       RS_SUBSID            Tasa,
       RS_GIRO              Formula,
       RS_STPS              Descripcion,
       RS_STPS_R1           Titulo,
       RS_STPS_R2           Titulo,
       RS_NUMERO            Pesos,
       RS_TEXTO             Descripcion,
       RS_CURP              Descripcion,
       RS_RL_RFC            Descripcion,
       RS_RL_CURP           Descripcion,
       RS_SELLO             Booleano,
       RS_KEY_PR            Memo,
       RS_KEY_PU            Memo,
       RS_CERT              Memo,
       RS_VAL_INI           Fecha,
       RS_VAL_FIN           Fecha,
       RS_SERIAL            Descripcion,
       RS_ISSUETO           Observaciones,
       RS_ISSUEBY           Observaciones,
       CT_CODIGO            Codigo,
       RS_CONTID            FolioGrande
)
GO

ALTER TABLE RSOCIAL
       ADD CONSTRAINT PK_RSOCIAL PRIMARY KEY ( RS_CODIGO )
GO

CREATE TABLE PTO_CERT (
	CI_CODIGO Codigo NOT NULL,
	PU_CODIGO Codigo NOT NULL,
	PC_DIAS Dias NOT NULL,
	PC_OPCIONA Booleano NOT NULL,
  PC_LISTA Booleano NOT NULL,
	LLAVE FolioGrande IDENTITY (1, 1) NOT NULL
)
GO

ALTER TABLE PTO_CERT
       ADD CONSTRAINT PK_PTO_CERT PRIMARY KEY (CI_CODIGO, PU_CODIGO)
GO

CREATE TABLE CERNIVEL (
	PU_CODIGO Codigo NOT NULL ,
	CI_CODIGO Codigo NOT NULL ,
	CN_CODIGO Codigo NOT NULL ,
	LLAVE FolioGrande IDENTITY (1, 1) NOT NULL
)
GO

ALTER TABLE CERNIVEL
       ADD CONSTRAINT PK_CERNIVEL PRIMARY KEY (PU_CODIGO, CI_CODIGO,CN_CODIGO)
GO

CREATE TABLE GR_AD_ACC (
	CM_CODIGO CodigoEmpresa NOT NULL ,
	GR_CODIGO FolioChico NOT NULL ,
	GX_CODIGO Codigo NOT NULL ,
	GX_DERECHO FolioChico NOT NULL ,
	LLAVE FolioGrande IDENTITY (1, 1) NOT NULL
	)
GO

ALTER TABLE GR_AD_ACC
       ADD CONSTRAINT PK_GR_AD_ACC PRIMARY KEY (CM_CODIGO, GR_CODIGO, GX_CODIGO)
GO

CREATE TABLE KARINF( 
	    CB_CODIGO NumeroEmpleado,
  	  KI_FECHA Fecha,
      KI_TIPO Status,
      CB_INFTIPO Status,  
      CB_INFCRED Descripcion, 
      CB_INFTASA DescINFO,  
      CB_INFDISM  Booleano,
      KI_COMENTA Observaciones, 
      US_CODIGO Usuario, 
      KI_CAPTURA Fecha,
      CB_INF_ANT Fecha,
      KI_D_EXT Codigo,
      KI_D_NOM Descripcion,
      KI_D_BLOB Imagen,
      KI_NOTA Memo
)
GO

ALTER TABLE KARINF
       ADD CONSTRAINT PK_KARINF PRIMARY KEY (CB_CODIGO, KI_FECHA, KI_TIPO)
GO

CREATE TABLE REGLAPREST
(
      RP_CODIGO FolioChico,
      RP_LETRERO DescLarga,
      RP_ACTIVO Booleano,
      RP_PRE_STS FolioChico,
      RP_LIMITE FolioChico,
      RP_VAL_FEC Booleano,
      RP_TOPE Formula,        
      RP_EMP_STS FolioChico,  
      RP_ANT_INI FolioChico,  
      RP_ANT_FIN FolioChico,  
      QU_CODIGO Condicion ,  	
      RP_FILTRO Formula,
      RP_LISTA Booleano, 	
      RP_ORDEN FolioChico, 	
      LLAVE FolioGrande IDENTITY (1, 1)
)
GO

ALTER TABLE REGLAPREST
       ADD CONSTRAINT PK_REGLAPREST PRIMARY KEY (RP_CODIGO)
GO

CREATE TABLE PRESTAXREG
(
      RP_CODIGO FolioChico,
      TB_CODIGO Codigo1,
      LLAVE FolioGrande IDENTITY (1, 1)
)
GO

ALTER TABLE PRESTAXREG
       ADD CONSTRAINT PK_PRESTAXREG PRIMARY KEY (RP_CODIGO, TB_CODIGO)
GO

CREATE TABLE CUR_REV(
      CU_CODIGO Codigo NOT NULL,
      CH_REVISIO NombreCampo NOT NULL,
      CH_FECHA Fecha NOT NULL,
      CH_OBSERVA Observaciones NOT NULL,
      US_CODIGO Usuario NOT NULL
)
GO

ALTER TABLE CUR_REV
       ADD CONSTRAINT PK_CUR_REV PRIMARY KEY (CU_CODIGO, CH_REVISIO, CH_FECHA)
GO

CREATE TABLE MOT_CHECA(
       TB_CODIGO            Codigo4,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

ALTER TABLE MOT_CHECA
       ADD CONSTRAINT PK_MOT_CHECA PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE TMPDIMMTOT (
	US_CODIGO Usuario NOT NULL,
	TD_YEAR Anio NOT NULL,
	TD_STATUS Status NOT NULL)
GO

ALTER TABLE TMPDIMMTOT
       ADD CONSTRAINT PK_TMPDIMMTOT PRIMARY KEY (US_CODIGO, TD_YEAR)
GO

CREATE TABLE OCUPA_NAC(
	TB_CODIGO Operacion,
	TB_ELEMENT Titulo,
	TB_INGLES Titulo,
	TB_NUMERO Pesos,
	TB_TEXTO Descripcion,
	LLAVE int IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE OCUPA_NAC
       ADD CONSTRAINT PK_OCUPA_NAC PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE AR_TEM_CUR(
	TB_CODIGO Operacion,
	TB_ELEMENT Titulo,
	TB_INGLES Titulo,
	TB_NUMERO Pesos,
	TB_TEXTO Descripcion,
	LLAVE int IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE AR_TEM_CUR
    ADD CONSTRAINT PK_AR_TEM_CUR PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE ESTABLEC(
	ES_CODIGO 	Codigo,
	ES_ELEMENT 	Titulo,
	ES_INGLES 	Titulo,
	ES_NUMERO 	Pesos,
	ES_TEXTO 		Descripcion,
	ES_CALLE            Titulo,
	ES_NUMEXT           Referencia,
	ES_NUMINT           Referencia,
	ES_COLONIA          Titulo,
	ES_CIUDAD           Titulo,
	ES_ENTIDAD          Codigo2,
	ES_CODPOST          Referencia,
  ES_ACTIVO           Booleano,
	LLAVE int IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE ESTABLEC
       add constraint PK_ESTABLEC primary key (ES_CODIGO)
GO

CREATE TABLE MUNICIPIO(
	TB_CODIGO Codigo,
	TB_ELEMENT Titulo,
	TB_INGLES Titulo,
	TB_NUMERO Pesos,
	TB_TEXTO Descripcion,
	TB_ENTIDAD Codigo2,
	TB_STPS FolioChico,
	LLAVE int IDENTITY(1,1) NOT NULL
)
GO

ALTER TABLE MUNICIPIO
	   add constraint PK_MUNICIPIO primary key (TB_CODIGO)
GO

CREATE TABLE MAQUINA
(
	MQ_CODIGO Codigo,
	MQ_NOMBRE Titulo,
	MQ_INGLES  Descripcion,
	MQ_TEXTO Descripcion,
	MQ_NUMERO Pesos,
	MQ_RESUMEN Formula,
	MQ_IMAGEN Imagen,
	MQ_TMAQUIN Codigo,	
	MQ_MULTIP Booleano,
	LLAVE Int IDENTITY (1, 1)
)
GO

ALTER TABLE MAQUINA
       ADD CONSTRAINT PK_MAQUINA PRIMARY KEY (MQ_CODIGO);
GO

CREATE TABLE MAQ_CERT
(
	MQ_CODIGO Codigo,
	CI_CODIGO Codigo,
	MC_FECHA Fecha,
	US_CODIGO Usuario,  
	LLAVE Int IDENTITY (1, 1)
)
GO

ALTER TABLE MAQ_CERT
       ADD CONSTRAINT PK_MAQ_CERT PRIMARY KEY (MQ_CODIGO,CI_CODIGO);
GO

CREATE TABLE MAQ_AREA
(
	MQ_CODIGO Codigo,
	AR_CODIGO Codigo,
	MA_FECHA Fecha,
	US_CODIGO Usuario,  
	LLAVE Int IDENTITY (1, 1)
)
GO

CREATE TABLE TMAQUINA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
	   LLAVE				Int IDENTITY (1, 1)
)
GO

ALTER TABLE TMAQUINA
       ADD CONSTRAINT PK_TMAQUINA PRIMARY KEY (TB_CODIGO);
GO

CREATE TABLE LAY_PRO_CE (
       LY_CODIGO            Codigo,
       LY_NOMBRE            Descripcion,
       LY_INGLES	          Descripcion,
	     LY_NUMERO	          Pesos,
	     LY_TEXTO	            Descripcion,
	     LY_AREA              Codigo,
	     LY_FECHA		          Fecha,
	     US_CODIGO		        Usuario,		
	     LLAVE				        Int IDENTITY (1, 1)
)
GO

ALTER TABLE LAY_PRO_CE
       ADD CONSTRAINT PK_LAY_PRO_CE PRIMARY KEY (LY_CODIGO);
GO

CREATE TABLE LAY_MAQ (
       LY_CODIGO            Codigo,
       MQ_CODIGO            Codigo,
       LM_POS_X             FolioGrande,
       LM_POS_Y				      FolioGrande,
	     LLAVE				        Int IDENTITY (1, 1)
)
GO

CREATE TABLE SILLA_LAY (
       SL_CODIGO            Codigo,
       LY_CODIGO            Codigo,
       SL_POS_X             FolioGrande,
       SL_POS_Y				      FolioGrande,
	     LLAVE 				        Int IDENTITY (1, 1)
)
GO

CREATE TABLE SILLA_MAQ (
       LY_CODIGO            Codigo,
       MQ_CODIGO            Codigo,
       SL_CODIGO			      Codigo,
	     LLAVE				        Int IDENTITY (1, 1)
)
GO

create table KAR_EMPSIL
(
	     CB_CODIGO            Empleados,
	     SL_CODIGO            Codigo,
	     LY_CODIGO            Codigo,
	     EM_FECHA             Fecha,
	     EM_HORA              Hora,
	     EM_HOR_MOV           Hora,
	     EM_FEC_MOV           Fecha,
	     US_CODIGO            Usuario,
	     LLAVE int            identity(1,1)
)
GO

CREATE TABLE TMPBALAMES (
       TM_USER              	Usuario,
       CB_CODIGO            	NumeroEmpleado,
       TM_CODIGO            	FolioChico,

       TM_NUM_PER           	Concepto,
       TM_DES_PER           	Descripcion,
       TM_MON_PER           	Pesos,
       TM_HOR_PER           	Horas,
       TM_REF_PER           	Referencia,

       TM_NUM_DED           	Concepto,
       TM_DES_DED           	Descripcion,
       TM_MON_DED           	Pesos,
       TM_REF_DED           	Referencia,
       TM_HOR_DED           	Horas,
                                                            
       PE_YEAR              	Anio,
       PE_TIPO              	NominaTipo,
       PE_NUMERO            	NominaNumero,
       PE_MES		Mes
)
GO

ALTER TABLE TMPBALAMES
       ADD CONSTRAINT PK_TMPBALAMES PRIMARY KEY (TM_USER, CB_CODIGO, TM_CODIGO)
GO

CREATE TABLE KAR_FIJA2 (
       CB_CODIGO            NumeroEmpleado,
       CB_FECHA             Fecha,
       CB_TIPO              Codigo,
       KF_FOLIO             FolioChico,
       KF_CODIGO            Codigo2,
       KF_MONTO             PesosDiario,
       KF_GRAVADO           PesosDiario,
       KF_IMSS              Status,
	     LLAVE                int identity(1,1)
)
go

CREATE TABLE HUELLA
(
	     CB_CODIGO            Empleados,
	     NO_HUELLA            FolioChico,
	     HUELLA               Memo,
		 HU_SYNC 			  Fecha, 
		 HU_TIPO 			  FolioChico not null,		 
	     LLAVE                int identity(1,1)
)
GO

ALTER TABLE HUELLA
	ADD CONSTRAINT PK_HUELLA PRIMARY KEY (CB_CODIGO, NO_HUELLA, HU_TIPO );
GO

Create Table BIT_MIG
(
	     BM_FEC_EVE           Fecha,
	     BM_DESCRIP           Formula,
	     BM_DETALLE           Memo
)
GO

CREATE TABLE EV_ALTA (
       EV_CODIGO      Evento,
       EV_DESCRIP     Descripcion,
       EV_ACTIVO      Booleano,
       CB_CODIGO      NumeroEmpleado,
       EV_CONTRAT     Codigo,
       EV_BAJA        Booleano,
       EV_PUESTO      Codigo,
       EV_CLASIFI     Codigo,
       EV_TURNO       Codigo,
       EV_PATRON      RegPatronal,
       EV_NIVEL1      Codigo,
       EV_NIVEL2      Codigo,
       EV_NIVEL3      Codigo,
       EV_NIVEL4      Codigo,
       EV_NIVEL5      Codigo,
       EV_NIVEL6      Codigo,
       EV_NIVEL7      Codigo,
       EV_NIVEL8      Codigo,
       EV_NIVEL9      Codigo,
       EV_NIVEL0      Codigo,
       EV_AUTOSAL     Codigo1,
       EV_SALARIO     PesosDiario,
       EV_PER_VAR     PesosDiario,
       EV_ZONA_GE     ZonaGeo,
       EV_TABLASS     Codigo1,
       EV_OTRAS_1     Codigo2,
       EV_OTRAS_2     Codigo2,
       EV_OTRAS_3     Codigo2,
       EV_OTRAS_4     Codigo2,
       EV_OTRAS_5     Codigo2,
       EV_BAN_ELE     Booleano,
       EV_NOMINA      NominaTipo,
	     LLAVE          int identity(1,1)
)
go

ALTER TABLE EV_ALTA
       ADD CONSTRAINT PK_EV_ALTA PRIMARY KEY (EV_CODIGO)
go

CREATE TABLE ESCENARIO(
	     ES_CODIGO      Codigo,
	     ES_ELEMENT     Descripcion,
	     ES_INGLES      Descripcion,
	     ES_NUMERO      Pesos,
	     ES_TEXTO       Descripcion,
	     LLAVE          int IDENTITY(1,1)
)
GO

ALTER TABLE ESCENARIO
       ADD CONSTRAINT PK_ESCENARIO PRIMARY KEY ( ES_CODIGO )
GO

CREATE TABLE SUPUEST_RH (
	SR_FECHA	      Fecha,
	SR_TIPO         Status,
	SR_CUANTOS		  Empleados,
	EV_CODIGO	      Evento,
	US_CODIGO	      Usuario,
	LLAVE           int IDENTITY(1,1),
  ES_CODIGO       Codigo
)
GO

ALTER TABLE SUPUEST_RH ADD CONSTRAINT PK_SUPUEST_RH PRIMARY KEY (ES_CODIGO,SR_FECHA,SR_TIPO,EV_CODIGO)
GO

CREATE TABLE TOTPRESUP(
	PE_YEAR     ANIO,
	PE_MES      MES,
	PE_TIPO     NOMINATIPO,
	CO_NUMERO   CONCEPTO,
	TP_MONTO    PESOSDIARIO,
	TP_EMPLEAD  EMPLEADOS,
	CB_PUESTO   CODIGO,
	CB_CLASIFI  CODIGO,
	CB_TURNO    CODIGO,
	CB_NIVEL1   CODIGO,
	CB_NIVEL2   CODIGO,
	CB_NIVEL3   CODIGO,
	CB_NIVEL4   CODIGO,
	CB_NIVEL5   CODIGO,
	CB_NIVEL6   CODIGO,
	CB_NIVEL7   CODIGO,
	CB_NIVEL8   CODIGO,
	CB_NIVEL9   CODIGO,
	CB_NIVEL0   CODIGO,
	LLAVE       int identity(1,1),
  ES_CODIGO   Codigo
)
GO

CREATE TABLE DOCUMENTO(
	CB_CODIGO  NumeroEmpleado,
	DO_TIPO    CodigoEmpresa,
	DO_BLOB    Imagen,
	DO_NOMBRE  Formula,
	DO_EXT     Codigo,
	DO_OBSERVA Descripcion,
	LLAVE      Int IDENTITY(1,1)
)
GO

ALTER TABLE DOCUMENTO
       ADD CONSTRAINT PK_DOCUMENTO PRIMARY KEY ( CB_CODIGO, DO_TIPO )
GO

create table SY_ALARMA(
		RELOJ		RelojLinx not null,
		HORA		hora not null,
		LUNES		Booleano,
		MARTES		Booleano,
		MIERCOLES	Booleano,
		JUEVES		Booleano,
		VIERNES		Booleano,
		SABADO		Booleano,
		DOMINGO		Booleano,
		TPO_OPERA	Codigo1,
		RELAY		Numeroempleado,
		ACTIVO		FolioChico,
		INACTIVO	FolioChico,
		TOTAL		FolioChico
	)
GO

alter table SY_ALARMA
		add constraint PK_SY_ALARMA primary key ( RELOJ, HORA )
GO

create table TPENSION
(
	TP_CODIGO Codigo,
	TP_NOMBRE Observaciones,
	TP_INGLES Observaciones,
	TP_NUMERO Pesos,
	TP_TEXTO Observaciones,
	TP_PERCEP Booleano,
	TP_PRESTA Booleano,
	TP_CO_PENS Formula,
	TP_CO_DESC Formula,
	TP_APL_NOR Booleano,
	TP_APL_LIQ Booleano,
	TP_APL_IND Booleano,
	US_CODIGO Usuario,
	TP_DESC_N Booleano,
	LLAVE FolioGrande identity(1,1)
)
GO

alter table TPENSION add Constraint PK_TP_PENSION Primary KEY (TP_CODIGO)
GO

create table PENSION
(
	CB_CODIGO NumeroEmpleado,
	PS_FECHA Fecha,
	PS_ORDEN  FolioChico,
	PS_EXPEDIE Descripcion,
	PS_BENEFIC Titulo,
	US_CODIGO Usuario,
	PS_CTA_1  DescLarga,
	PS_BANCO_1  Descripcion,
	PS_CTA_2  DescLarga,
	PS_BANCO_2  Descripcion,
	PS_OBSERVA	Formula,
	PS_FIJA	 Pesos,
	PS_B_EMAIL DescLarga,
	PS_B_DOM ReferenciaLarga,
	PS_B_TEL Descripcion,
	PS_ACTIVA Codigo1,
	LLAVE FolioGrande identity(1,1)
)
GO

alter table PENSION add constraint PK_PENSION Primary KEY ( CB_CODIGO,PS_ORDEN )
GO

create table PEN_PORCEN
(
	TP_CODIGO Codigo,
	CB_CODIGO NumeroEmpleado,
	PS_ORDEN  FolioChico,
	PP_PORCENT Tasa,
	LLAVE FolioGrande identity(1,1)
)
GO

alter table PEN_PORCEN add Primary KEY (TP_CODIGO,CB_CODIGO,PS_ORDEN)
GO

create table POLIZA_MED
(
	PM_CODIGO  Codigo,
	PM_NUMERO  Descripcion,
	PM_TIPO	   Status,
	PM_DESCRIP Titulo,
	PM_ASEGURA Descripcion,
	PM_BROKER  Descripcion,
	PM_TEL_BRK DescLarga,
	PM_NOM_CT1 Observaciones,
	PM_COR_CT1 Titulo,
	PM_TEL1CT1 DescLarga,
	PM_TEL2CT1 DescLarga,
	PM_NOM_CT2 Observaciones,
	PM_COR_CT2 Titulo,
	PM_TEL1CT2 DescLarga,
	PM_TEL2CT2 DescLarga,
	PM_OBSERVA Memo,
	LLAVE	   FolioGrande identity(1,1)
)
GO

alter table POLIZA_MED add Constraint PK_POLIZA_MED Primary KEY (PM_CODIGO)
GO

create Table POL_VIGENC
(
	PM_CODIGO  Codigo,
	PV_REFEREN Referencia,
	PV_FEC_INI Fecha,
	PV_FEC_FIN Fecha,
	PV_CONDIC  Memo,
	PV_TEXTO   Descripcion,
	PV_NUMERO  Numerico,
  PV_CFIJO   Pesos,
	LLAVE	     FolioGrande identity(1,1)
)
GO

alter table POL_VIGENC add Constraint PK_POL_VIGENC Primary KEY (PM_CODIGO,PV_REFEREN)
GO

Create Table EMP_POLIZA
(
	CB_CODIGO  NumeroEmpleado,
	PM_CODIGO  Codigo,
	PV_REFEREN Referencia,
	EP_TIPO    Status,
	PA_FOLIO   FolioChico,
	PA_RELACIO Status,
	EP_CERTIFI Descripcion,
	EP_FEC_INI Fecha,
	EP_FEC_FIN Fecha,
	EP_CFIJO   Pesos,
	EP_CPOLIZA Pesos,
	EP_CTITULA Pesos,
	EP_OBSERVA Formula,
	EP_CANCELA Booleano,
	EP_STATUS  as Case WHEN EP_CANCELA = 'S' THEN 2 WHEN EP_FEC_INI > GetDate() THEN 0 When ( EP_CANCELA = 'N' and ( GetDate() between EP_FEC_INI and EP_FEC_FIN ) )then 0 else 1 end,
	EP_CAN_FEC Fecha,
	EP_CAN_MON Pesos,
	EP_CAN_MOT DescLarga,
	EP_GLOBAL  Booleano,
	US_CODIGO  Usuario,
	EP_FEC_REG Fecha,
  EP_ASEGURA Status,
  EP_ORDEN   Status,
  EP_END_ALT Mascara,
  EP_END_CAN Mascara,
  EP_CEMPRES as ( ( EP_CFIJO + EP_CPOLIZA ) - ( EP_CTITULA ) ),
	LLAVE	     FolioGrande identity(1,1)
)
GO

alter table EMP_POLIZA add primary key (CB_CODIGO,PM_CODIGO,PV_REFEREN,EP_TIPO,PA_FOLIO,PA_RELACIO,EP_FEC_INI,EP_ASEGURA,EP_ORDEN);
GO

create table AMORTIZ_TB
(
  AT_CODIGO  Codigo,
  AT_DESCRIP DescLarga,
  AT_INGLES  Descripcion,
  AT_NUMERO  Numerico,
  AT_TEXTO   DescLarga,
  AT_ACTIVO  Booleano,
  LLAVE      int identity(1,1)
);
GO

alter table AMORTIZ_TB ADD PRIMARY KEY (AT_CODIGO);
GO

create table AMORTIZ_CS
(
  AT_CODIGO  Codigo,
  AC_EDADINI Anio,
  AC_EDADFIN Anio,
  AC_CHOMBRE Pesos,
  AC_CMUJER  Pesos,
  LLAVE      int identity(1,1)
);
GO

alter table AMORTIZ_CS ADD PRIMARY KEY (AT_CODIGO,AC_EDADINI,AC_EDADFIN);
GO

CREATE TABLE ISR_AJUSTE (
			IS_YEAR              Anio,
			IS_MES				       Mes,
			CB_CODIGO            NumeroEmpleado,
			CB_SALARIO           PesosDiario,
			IS_ING_BR            Pesos,
			IS_GRAVADO           Pesos,
			IS_RETENID           Pesos,
			IS_SUBE              Pesos,
			IS_IMPCALC           Pesos,
			IS_FAVOR             Pesos,
			IS_CONTRA            Pesos,
			US_CODIGO            Usuario
)
GO

ALTER TABLE ISR_AJUSTE
       ADD CONSTRAINT PK_ISR_AJUSTE PRIMARY KEY (IS_YEAR, IS_MES, CB_CODIGO)
GO

CREATE TABLE T_CriteriosCosto
(
	CritCostoID 		  FolioGrande,
	CritCostoNombre 	Descripcion,
	CritCostoActivo 	Booleano
)
GO

ALTER TABLE T_CriteriosCosto ADD CONSTRAINT PK_CriteriosCosto PRIMARY KEY (CritCostoID)
GO

CREATE TABLE T_GruposCosto
(
	GpoCostoID 		  FolioGrande IDENTITY,
	GpoCostoCodigo 	Codigo10,
	GpoCostoNombre 	Observaciones,
	GpoCostoActivo 	Booleano,
)
GO

ALTER TABLE T_GruposCosto ADD CONSTRAINT PK_GruposCosto PRIMARY KEY (GpoCostoID)
GO

ALTER TABLE T_GruposCosto ADD CONSTRAINT AK_GruposCosto_Codigo UNIQUE (GpoCostoCodigo)
GO

CREATE TABLE T_ConceptosCosto
	(
		ConcepCostoID 	FolioGrande IDENTITY,
		CO_NUMERO 		  Concepto,
		GpoCostoID 		  FolioGrande,
		CritCostoID 	  FolioGrande
	)
GO

ALTER TABLE T_ConceptosCosto ADD CONSTRAINT PK_ConceptosCosto PRIMARY KEY (ConcepCostoID)
GO

ALTER TABLE T_ConceptosCosto ADD CONSTRAINT AK_ConceptosCosto_Concepto UNIQUE (CO_NUMERO,GpoCostoID)
GO

CREATE INDEX IE_ConceptosCosto_Grupo ON T_ConceptosCosto (GpoCostoID ,CO_NUMERO )
GO

CREATE TABLE T_ProporcionTotales
	(
		PropTotalID 	  FolioGrande IDENTITY,
		NominaID 		    FolioGrande,
		CritCostoID 	  FolioGrande,
		PropTotalConteo FolioGrande,
		PropTotalSuma 	Horas
	)
GO

ALTER TABLE T_ProporcionTotales ADD CONSTRAINT PK_ProporcionTotales PRIMARY KEY (PropTotalID)
GO

ALTER TABLE T_ProporcionTotales ADD CONSTRAINT AK_ProporcionTotales_Criterio UNIQUE (NominaID,CritCostoID)
GO

CREATE TABLE T_ProporcionCostos
	(
		PropCostoID 		  FolioGrande IDENTITY,
		NominaID 			    FolioGrande,
		CritCostoID 		  FolioGrande,
		CC_CODIGO 			  Codigo,
		PropCostoClasifi	OrdenTrabajo,
		PropCostoBase 		Horas
	)
GO

ALTER TABLE T_ProporcionCostos ADD CONSTRAINT PK_ProporcionCostos PRIMARY KEY (PropCostoID)
GO

CREATE INDEX IE_ProporcionCostos_Criterio ON T_ProporcionCostos (NominaID ,CritCostoID )
GO

CREATE TABLE T_Costos
	(
		CostoID 		  FolioGrande IDENTITY,
		NominaID 		  FolioGrande,
		CO_NUMERO 		Concepto,
		MO_REFEREN 		Referencia,
		GpoCostoID 		FolioGrande,
		CC_CODIGO 		Codigo,
		CostoClasifi 	OrdenTrabajo,
		CostoMonto 		Pesos
	)
GO

ALTER TABLE T_Costos ADD CONSTRAINT PK_Costos PRIMARY KEY (CostoID)
GO

CREATE INDEX IE_Costos_Concepto ON T_Costos (NominaID ,CO_NUMERO )
GO

CREATE INDEX IE_Costos_Grupo ON T_Costos (NominaID ,GpoCostoID )
GO

CREATE TABLE TRANSFER(
		CB_CODIGO    	NumeroEmpleado,
		AU_FECHA      Fecha,
		TR_HORAS      Horas,
		TR_TIPO			  Status,
		TR_MOTIVO		  Codigo,
		CC_CODIGO		  Codigo,
		TR_NUMERO     Pesos,
		TR_TEXTO      Descripcion,
		US_CODIGO     Usuario,
		TR_FECHA		  Fecha,
		TR_GLOBAL		  Booleano,
		TR_APRUEBA		Usuario,
		TR_FEC_APR		Fecha,
		TR_STATUS		  Status,
		TR_TXT_APR		Observaciones,
		LLAVE         FolioGrande IDENTITY(1,1)
	)
GO

ALTER TABLE TRANSFER ADD CONSTRAINT PK_TRANSFER PRIMARY KEY ( LLAVE )
GO

CREATE INDEX IE_TRANSFER_TARJETA ON TRANSFER ( AU_FECHA ,CB_CODIGO )
GO

CREATE TABLE MOT_TRANS(
		   TB_CODIGO            Codigo,
		   TB_ELEMENT           Descripcion,
		   TB_INGLES            Descripcion,
		   TB_NUMERO            Pesos,
		   TB_TEXTO             Descripcion,
		   TB_TIPO              Status,
		   LLAVE FolioGrande 	IDENTITY(1,1)
	)
GO

ALTER TABLE MOT_TRANS ADD CONSTRAINT PK_MOT_TRANS PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE SUP_COSTEO (
    US_CODIGO     Usuario,
    CB_NIVEL      Codigo,
	  LLAVE 			 	FolioGrande IDENTITY (1, 1) NOT NULL
	)
GO

ALTER TABLE SUP_COSTEO add CONSTRAINT PK_SUP_COSTEO PRIMARY KEY (US_CODIGO, CB_NIVEL)

GO

CREATE TABLE T_Costos_SubCta
	(
		CostoID 		    FolioGrande,
		NominaID 		    FolioGrande,
		CuentaContable 	Formula,
		Tipo			      Codigo1,
		Prorrat			    Numerico,
		Comentario		  Descripcion,
		Numero			    Pesos,
		Monto			      Pesos,
		LLAVE 			    FolioGrande IDENTITY(1,1)
	)
GO

ALTER TABLE T_Costos_SubCta ADD CONSTRAINT PK_T_Costos_SubCta PRIMARY KEY (CostoID, NominaID, CuentaContable, Tipo)

GO 

create table CONF_TAB  (
	CT_TABLA FolioChico,
	CT_CODIGO Codigo,
	CT_NIVEL0 Codigo
 )
GO

create index IDX_CONF_TAB on CONF_TAB( CT_TABLA )
GO

create index IDX_CONF_TAB_NIVEL0 on CONF_TAB( CT_NIVEL0 )
GO

CREATE TABLE T_COMPETEN(
	TB_CODIGO Codigo,
	TB_ELEMENT Observaciones,
	TB_INGLES Observaciones,
	TB_NUMERO Pesos,
	TB_TEXTO Descripcion,
	LLAVE FolioGrande IDENTITY(1,1)
)
GO

ALTER TABLE T_COMPETEN
       ADD CONSTRAINT PK_T_COMPETEN PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE C_T_NACOMP(
	TN_CODIGO CODIGO,
	TN_TITULO Titulo,
	TN_COMITE Titulo,
	TN_PROPOSI Memo,
	TN_FEC_APR Fecha,
	TN_NIVEL Codigo,
	TN_ACT_NIV Memo,
	TN_PERFIL Memo,
	TN_CR_EVA Memo,
	TN_AC_COMP Memo
)
GO

ALTER TABLE C_T_NACOMP
       ADD CONSTRAINT PK_C_T_NACOMP PRIMARY KEY (TN_CODIGO)
GO

create Table C_COMPETEN(
	CC_CODIGO Codigo,
	CC_ELEMENT Observaciones,
	CC_INGLES Observaciones,
	CC_NUMERO Pesos,
	CC_TEXTO Descripcion,
	CC_DETALLE Memo,
	TB_CODIGO Codigo,
	CC_ACTIVO Booleano,
	TN_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
)
GO

ALTER TABLE C_COMPETEN
    ADD CONSTRAINT PK_C_COMPETEN PRIMARY KEY (CC_CODIGO)
GO

create Table C_NIV_COMP(
	NC_NIVEL FolioChico,
	NC_DESCRIP Descripcion,
	CC_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
)
GO

ALTER TABLE C_NIV_COMP
    ADD CONSTRAINT PK_C_NIV_COMP PRIMARY KEY (NC_NIVEL,CC_CODIGO)
GO

create Table C_REV_COMP(
	RC_REVISIO NombreCampo,
	RC_OBSERVA Descripcion,
	RC_FEC_INI Fecha,
	RC_FEC_FIN Fecha,
	US_CODIGO Usuario,
	CC_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
)
GO

ALTER TABLE C_REV_COMP
    ADD CONSTRAINT PK_C_REV_COMP PRIMARY KEY (RC_FEC_INI,CC_CODIGO)
GO

create Table C_COMP_CUR(
	CC_ORDEN FolioChico,
	CC_CODIGO Codigo,
	CU_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
)
GO

ALTER TABLE C_COMP_CUR
    ADD CONSTRAINT PK_C_COMP_CUR PRIMARY KEY (CC_CODIGO,CU_CODIGO)
GO

create TABLE T_PERFIL
(
	TB_CODIGO Codigo,
	TB_ELEMENT Descripcion,
	TB_INGLES Descripcion,
	TB_NUMERO Numerico,
	TB_TEXTO Descripcion,
	LLAVE FolioGrande identity(1,1)
)
GO

ALTER TABLE T_PERFIL
       ADD CONSTRAINT PK_T_PERFIL PRIMARY KEY (TB_CODIGO)
GO

create TABLE C_PERFIL
(
	CP_CODIGO Codigo,
	CP_ELEMENT Observaciones,
	CP_NUMERO Numerico,
	CP_TEXTO Observaciones,
	CP_INGLES Observaciones,
	CP_DETALLE Memo,
	TB_CODIGO Codigo,
	CP_ACTIVO Booleano,
	LLAVE FolioGrande identity(1,1)
)
GO

ALTER TABLE C_PERFIL
       ADD CONSTRAINT PK_C_PERFIL PRIMARY KEY (CP_CODIGO)
GO

create Table C_REV_PERF(
	RP_REVISIO NombreCampo,
	RP_OBSERVA Descripcion,
	RP_FEC_INI Fecha,
	RP_FEC_FIN Fecha,
	US_CODIGO Usuario,
	CP_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
)
GO

ALTER TABLE C_REV_PERF
    ADD CONSTRAINT PK_C_REV_PERF PRIMARY KEY (RP_FEC_INI,CP_CODIGO)
GO

CREATE TABLE C_PER_COMP(
	CP_CODIGO Codigo,
	RP_FEC_INI Fecha,
	CC_CODIGO Codigo,
	NC_NIVEL  FolioChico,
	PC_PESO Numerico
)
GO

ALTER TABLE C_PER_COMP
    ADD CONSTRAINT PK_C_PER_COMP PRIMARY KEY (CP_CODIGO,CC_CODIGO,RP_FEC_INI)
GO

CREATE TABLE C_PERF_PTO(
	CP_CODIGO Codigo,
	PU_CODIGO Codigo,
	CP_FEC_INI Fecha,
	PP_ACTIVO Booleano,
  PP_PESO Numerico,
	US_CODIGO Usuario,
	PP_FEC_REG Fecha
)
GO

ALTER TABLE C_PERF_PTO
    ADD CONSTRAINT PK_C_PERF_PTO PRIMARY KEY (CP_CODIGO,PU_CODIGO)
GO

CREATE TABLE C_EMP_COMP(
	CB_CODIGO NumeroEmpleado,
	CC_CODIGO Codigo,
	RC_FEC_INI Fecha,
	NC_NIVEL FolioChico,
	ECC_FECHA Fecha,
  ECC_COMENT Formula,
	US_CODIGO Usuario
)
GO

ALTER TABLE C_EMP_COMP
    ADD CONSTRAINT PK_C_EMP_COMP PRIMARY KEY (CB_CODIGO,CC_CODIGO,RC_FEC_INI)
GO

CREATE TABLE C_EVA_COMP(
	CB_CODIGO NumeroEmpleado,
	CC_CODIGO Codigo,
	NC_NIVEL FolioChico,
	RC_FEC_INI Fecha,
	EC_FECHA Fecha,
	EC_CALIFIC Numerico,
	US_CODIGO Usuario,
	EC_COMENT Formula
)
GO

ALTER TABLE C_EVA_COMP
    ADD CONSTRAINT PK_C_EVA_COMP PRIMARY KEY (CB_CODIGO,CC_CODIGO,NC_NIVEL,RC_FEC_INI)
GO

create table TIPO_SAT (
  TB_CODIGO	Codigo PRIMARY KEY,
  TB_ELEMENT	Formula,
  TB_SAT_CLA	Status,
  TB_SAT_NUM	FolioChico,
  TB_INGLES	Descripcion,
  TB_NUMERO	Pesos,
  TB_TEXTO	Descripcion,
  TB_ACTIVO	Booleano
)
GO

create table TimListas ( 
    TL_IDLISTA FolioGrande,
    TL_VALOR FolioChico,
    TL_DESCRIP Formula,
    TL_TEXTO Descripcion,
    TL_INGLES Descripcion,
    TL_NUMERO Pesos,
    TL_ACTIVO Booleano
)
GO

ALTER TABLE TimListas
    ADD CONSTRAINT PK_TIMLISTAS PRIMARY KEY (TL_IDLISTA,TL_VALOR)
GO

create table BANCO (
  TB_CODIGO	Codigo PRIMARY KEY,
  TB_ELEMENT	DescLarga,
  TB_NOMBRE	Formula,
  TB_SAT_BAN	FolioChico,
  TB_INGLES	Descripcion,
  TB_NUMERO	Pesos,
  TB_TEXTO	Descripcion,
  TB_ACTIVO	Booleano
)
GO

CREATE TABLE EMP_TRANSF(
		[CB_CODIGO] [dbo].[NumeroEmpleado] NOT NULL,
		[CB_ACTIVO] [dbo].[Booleano] NOT NULL,
		[CB_APE_MAT] [dbo].[Descripcion] NOT NULL,
		[CB_APE_PAT] [dbo].[Descripcion] NOT NULL,
		[CB_AUTOSAL] [dbo].[Booleano] NOT NULL,
		[CB_AR_FEC] [dbo].[Fecha] NOT NULL,
		[CB_AR_HOR] [dbo].[Hora] NOT NULL,
		[CB_BAN_ELE] [dbo].[Descripcion] NOT NULL,
		[CB_CARRERA] [dbo].[DescLarga] NOT NULL,
		[CB_CHECA] [dbo].[Booleano] NOT NULL,
		[CB_CIUDAD] [dbo].[Descripcion] NOT NULL,
		[CB_CLASIFI] [dbo].[Codigo] NOT NULL,
		[CB_CODPOST] [dbo].[Referencia] NOT NULL,
		[CB_CONTRAT] [dbo].[Codigo1] NOT NULL,
		[CB_CREDENC] [dbo].[Codigo1] NOT NULL,
		[CB_CURP] [dbo].[Descripcion] NOT NULL,
		[CB_CALLE] [dbo].[Observaciones] NOT NULL,
		[CB_COLONIA] [dbo].[Descripcion] NOT NULL,
		[CB_EDO_CIV] [dbo].[Codigo1] NOT NULL,
		[CB_FEC_RES] [dbo].[Fecha] NOT NULL,
		[CB_EST_HOR] [dbo].[Descripcion] NOT NULL,
		[CB_EST_HOY] [dbo].[Booleano] NOT NULL,
		[CB_ESTADO] [dbo].[Codigo2] NOT NULL,
		[CB_ESTUDIO] [dbo].[Codigo2] NOT NULL,
		[CB_EVALUA] [dbo].[DiasFrac] NOT NULL,
		[CB_EXPERIE] [dbo].[Titulo] NOT NULL,
		[CB_FEC_ANT] [dbo].[Fecha] NOT NULL,
		[CB_FEC_BAJ] [dbo].[Fecha] NOT NULL,
		[CB_FEC_BSS] [dbo].[Fecha] NOT NULL,
		[CB_FEC_CON] [dbo].[Fecha] NOT NULL,
		[CB_FEC_ING] [dbo].[Fecha] NOT NULL,
		[CB_FEC_INT] [dbo].[Fecha] NOT NULL,
		[CB_FEC_NAC] [dbo].[Fecha] NOT NULL,
		[CB_FEC_REV] [dbo].[Fecha] NOT NULL,
		[CB_FEC_VAC] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_1] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_2] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_3] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_4] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_5] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_6] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_7] [dbo].[Fecha] NOT NULL,
		[CB_G_FEC_8] [dbo].[Fecha] NOT NULL,
		[CB_G_LOG_1] [dbo].[Booleano] NOT NULL,
		[CB_G_LOG_2] [dbo].[Booleano] NOT NULL,
		[CB_G_LOG_3] [dbo].[Booleano] NOT NULL,
		[CB_G_LOG_4] [dbo].[Booleano] NOT NULL,
		[CB_G_LOG_5] [dbo].[Booleano] NOT NULL,
		[CB_G_LOG_6] [dbo].[Booleano] NOT NULL,
		[CB_G_LOG_7] [dbo].[Booleano] NOT NULL,
		[CB_G_LOG_8] [dbo].[Booleano] NOT NULL,
		[CB_G_NUM_1] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_2] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_3] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_4] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_5] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_6] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_7] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_8] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM_9] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM10] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM11] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM12] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM13] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM14] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM15] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM16] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM17] [dbo].[Pesos] NOT NULL,
		[CB_G_NUM18] [dbo].[Pesos] NOT NULL,
		[CB_G_TAB_1] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_2] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_3] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_4] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_5] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_6] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_7] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_8] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB_9] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB10] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB11] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB12] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB13] [dbo].[Codigo] NOT NULL,
		[CB_G_TAB14] [dbo].[Codigo] NOT NULL,
		[CB_G_TEX_1] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_2] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_3] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_4] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_5] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_6] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_7] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_8] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX_9] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX10] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX11] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX12] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX13] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX14] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX15] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX16] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX17] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX18] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX19] [dbo].[Descripcion] NOT NULL,
		[CB_HABLA] [dbo].[Booleano] NOT NULL,
		[CB_TURNO] [dbo].[Codigo] NOT NULL,
		[CB_IDIOMA] [dbo].[Descripcion] NOT NULL,
		[CB_INFCRED] [dbo].[Descripcion] NOT NULL,
		[CB_INFMANT] [dbo].[Booleano] NOT NULL,
		[CB_INFTASA] [dbo].[DescINFO] NOT NULL,
		[CB_LA_MAT] [dbo].[Descripcion] NOT NULL,
		[CB_LAST_EV] [dbo].[Fecha] NOT NULL,
		[CB_LUG_NAC] [dbo].[Descripcion] NOT NULL,
		[CB_MAQUINA] [dbo].[Titulo] NOT NULL,
		[CB_MED_TRA] [dbo].[Codigo1] NOT NULL,
		[CB_PASAPOR] [dbo].[Booleano] NOT NULL,
		[CB_FEC_INC] [dbo].[Fecha] NOT NULL,
		[CB_FEC_PER] [dbo].[Fecha] NOT NULL,
		[CB_NOMYEAR] [dbo].[Anio] NOT NULL,
		[CB_NACION] [dbo].[Descripcion] NOT NULL,
		[CB_NOMTIPO] [dbo].[NominaTipo] NOT NULL,
		[CB_NEXT_EV] [dbo].[Fecha] NOT NULL,
		[CB_NOMNUME] [dbo].[NominaNumero] NOT NULL,
		[CB_NOMBRES] [dbo].[Descripcion] NOT NULL,
		[CB_PATRON] [dbo].[RegPatronal] NOT NULL,
		[CB_PUESTO] [dbo].[Codigo] NOT NULL,
		[CB_RFC] [dbo].[Descripcion] NOT NULL,
		[CB_SAL_INT] [dbo].[PesosDiario] NOT NULL,
		[CB_SALARIO] [dbo].[PesosDiario] NOT NULL,
		[CB_SEGSOC] [dbo].[Descripcion] NOT NULL,
		[CB_SEXO] [dbo].[Codigo1] NOT NULL,
		[CB_TABLASS] [dbo].[Codigo1] NOT NULL,
		[CB_TEL] [dbo].[Descripcion] NOT NULL,
		[CB_VIVECON] [dbo].[Codigo1] NOT NULL,
		[CB_VIVEEN] [dbo].[Codigo2] NOT NULL,
		[CB_ZONA] [dbo].[Referencia] NOT NULL,
		[CB_ZONA_GE] [dbo].[ZonaGeo] NOT NULL,
		[CB_NIVEL1] [dbo].[Codigo] NOT NULL,
		[CB_NIVEL2] [dbo].[Codigo] NOT NULL,
		[CB_NIVEL3] [dbo].[Codigo] NOT NULL,
		[CB_NIVEL4] [dbo].[Codigo] NOT NULL,
		[CB_NIVEL5] [dbo].[Codigo] NOT NULL,
		[CB_NIVEL6] [dbo].[Codigo] NOT NULL,
		[CB_NIVEL7] [dbo].[Codigo] NOT NULL,
		[CB_INFTIPO] [dbo].[Status] NOT NULL,
		[CB_NIVEL8] [dbo].[Codigo] NOT NULL,
		[CB_NIVEL9] [dbo].[Codigo] NOT NULL,
		[CB_DER_FEC] [dbo].[Fecha] NOT NULL,
		[CB_DER_PAG] [dbo].[DiasFrac] NOT NULL,
		[CB_V_PAGO] [dbo].[DiasFrac] NOT NULL,
		[CB_DER_GOZ] [dbo].[DiasFrac] NOT NULL,
		[CB_V_GOZO] [dbo].[DiasFrac] NOT NULL,
		[CB_TIP_REV] [dbo].[Codigo] NOT NULL,
		[CB_MOT_BAJ] [dbo].[Codigo3] NOT NULL,
		[CB_FEC_SAL] [dbo].[Fecha] NOT NULL,
		[CB_INF_INI] [dbo].[Fecha] NOT NULL,
		[CB_INF_OLD] [dbo].[DescINFO] NOT NULL,
		[CB_OLD_SAL] [dbo].[PesosDiario] NOT NULL,
		[CB_OLD_INT] [dbo].[PesosDiario] NOT NULL,
		[CB_PRE_INT] [dbo].[PesosDiario] NOT NULL,
		[CB_PER_VAR] [dbo].[PesosDiario] NOT NULL,
		[CB_SAL_TOT] [dbo].[PesosDiario] NOT NULL,
		[CB_TOT_GRA] [dbo].[PesosDiario] NOT NULL,
		[CB_FAC_INT] [dbo].[Tasa] NOT NULL,
		[CB_RANGO_S] [dbo].[Tasa] NOT NULL,
		[CB_CLINICA] [dbo].[Codigo3] NOT NULL,
		[CB_NIVEL0] [dbo].[Codigo] NOT NULL,
		[CB_FEC_NIV] [dbo].[Fecha] NOT NULL,
		[CB_FEC_PTO] [dbo].[Fecha] NOT NULL,
		[CB_AREA] [dbo].[Codigo] NOT NULL,
		[CB_FEC_TUR] [dbo].[Fecha] NOT NULL,
		[CB_FEC_KAR] [dbo].[Fecha] NOT NULL,
		[CB_PENSION] [dbo].[Status] NOT NULL,
		[CB_CANDIDA] [dbo].[FolioGrande] NOT NULL,
		[CB_ID_NUM] [dbo].[Descripcion] NOT NULL,
		[CB_ENT_NAC] [dbo].[Codigo2] NOT NULL,
		[CB_COD_COL] [dbo].[Codigo] NOT NULL,
		[CB_PASSWRD] [dbo].[Passwd] NOT NULL,
		[CB_PLAZA] [dbo].[FolioGrande] NOT NULL,
		[CB_FEC_PLA] [dbo].[Fecha] NOT NULL,
		[CB_DER_PV] [dbo].[DiasFrac] NOT NULL,
		[CB_V_PRIMA] [dbo].[DiasFrac] NOT NULL,
		[CB_SUB_CTA] [dbo].[Descripcion] NOT NULL,
		[CB_NETO] [dbo].[Pesos] NOT NULL,
		[CB_E_MAIL] [dbo].[Formula] NOT NULL,
		[CB_PORTAL] [dbo].[Booleano] NOT NULL,
		[CB_DNN_OK] [dbo].[Booleano] NOT NULL,
		[CB_USRNAME] [dbo].[DescLarga] NOT NULL,
		[CB_NOMINA] [dbo].[NominaTipo] NOT NULL,
		[CB_FEC_NOM] [dbo].[Fecha] NOT NULL,
		[CB_RECONTR] [dbo].[Booleano] NOT NULL,
		[CB_DISCAPA] [dbo].[Booleano] NOT NULL,
		[CB_INDIGE] [dbo].[Booleano] NOT NULL,
		[CB_FONACOT] [dbo].[Descripcion] NOT NULL,
		[CB_EMPLEO] [dbo].[Booleano] NOT NULL,
		[US_CODIGO] [dbo].[Usuario] NOT NULL,
		[CB_FEC_COV] [dbo].[Fecha] NOT NULL,
		[CB_G_TEX20] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX21] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX22] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX23] [dbo].[Descripcion] NOT NULL,
		[CB_G_TEX24] [dbo].[Descripcion] NOT NULL,
		[CB_INFDISM] [dbo].[Booleano] NOT NULL,
		[CB_INFACT] [dbo].[Booleano] NOT NULL,
		[CB_NUM_EXT] [dbo].[NombreCampo] NOT NULL,
		[CB_NUM_INT] [dbo].[NombreCampo] NOT NULL,
		[CB_INF_ANT] [dbo].[Fecha] NOT NULL,
		[CB_TDISCAP] [dbo].[Status] NOT NULL,
		[CB_ESCUELA] [dbo].[DescLarga] NOT NULL,
		[CB_TESCUEL] [dbo].[Status] NOT NULL,
		[CB_TITULO] [dbo].[Status] NOT NULL,
		[CB_YTITULO] [dbo].[Anio] NOT NULL,
		[CB_CTA_GAS] [dbo].[Descripcion] NOT NULL,
		[CB_CTA_VAL] [dbo].[Descripcion] NOT NULL,
		[CB_MUNICIP] [dbo].[Codigo] NOT NULL,
		[CB_ID_BIO] [dbo].[FolioGrande] NOT NULL,
		[CB_GP_COD] [dbo].[Codigo] NOT NULL,
		[CB_TIE_PEN] [dbo].[Booleano] NOT NULL,
		[PRETTYNAME]  AS (((([CB_APE_PAT]+' ')+[CB_APE_MAT])+', ')+[CB_NOMBRES]),
		[CB_TSANGRE] [dbo].[Descripcion] NOT NULL,
		[CB_ALERGIA] [dbo].[Formula] NOT NULL,
		[CB_BRG_ACT] [dbo].[Booleano] NOT NULL,
		[CB_BRG_TIP] [dbo].[Status] NOT NULL,
		[CB_BRG_ROL] [dbo].[Booleano] NOT NULL,
		[CB_BRG_JEF] [dbo].[Booleano] NOT NULL,
		[CB_BRG_CON] [dbo].[Booleano] NOT NULL,
		[CB_BRG_PRA] [dbo].[Booleano] NOT NULL,
		[CB_BRG_NOP] [dbo].[Descripcion] NOT NULL,
		[CB_BANCO] [dbo].[Codigo] NOT NULL,
		[CB_REGIMEN] [dbo].[Status] NOT NULL,
		[LLAVE] [int] IDENTITY(1,1) NOT NULL,
	 CONSTRAINT [PK_EMP_TRANSF] PRIMARY KEY CLUSTERED 
	(
		[CB_CODIGO] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO

create table TSATCONTRA
(
 TB_CODIGO    Codigo, 
 TB_ELEMENT   DescLarga, 
 TB_COD_SAT   DescLarga,  
 TB_INGLES    Descripcion,
 TB_NUMERO    Pesos,
 TB_TEXTO     Descripcion,
 TB_ACTIVO    Booleano
)
go 
ALTER TABLE TSATCONTRA
       ADD CONSTRAINT PK_TSATCONTRA PRIMARY KEY (TB_CODIGO)
GO 

create table TSATJORNAD
(
 TB_CODIGO    Codigo, 
 TB_ELEMENT   DescLarga, 
 TB_COD_SAT   DescLarga,  
 TB_INGLES    Descripcion,
 TB_NUMERO    Pesos,
 TB_TEXTO     Descripcion,
 TB_ACTIVO    Booleano
)
GO 
ALTER TABLE TSATJORNAD
       ADD CONSTRAINT PK_TSATJORNAD PRIMARY KEY (TB_CODIGO)
GO 

create table TSATRIESGO
(
 TB_CODIGO    Codigo, 
 TB_ELEMENT   DescLarga, 
 TB_COD_SAT   DescLarga,  
 TB_INGLES    Descripcion,
 TB_NUMERO    Pesos,
 TB_TEXTO     Descripcion,
 TB_ACTIVO    Booleano
)
GO 
ALTER TABLE TSATRIESGO
       ADD CONSTRAINT PK_TSATRIESGO PRIMARY KEY (TB_CODIGO)
GO 

CREATE TABLE VALOR_UMA (
       UM_FEC_INI           Fecha,
       UM_VALOR             PesosDiario,
	   UM_FDESC             PesosDiario
)
GO

ALTER TABLE VALOR_UMA
       ADD CONSTRAINT PK_VALOR_UMA PRIMARY KEY (UM_FEC_INI)
GO 

CREATE TABLE ACUMULA_RS (
	AC_ID int IDENTITY(1,1),
	AC_YEAR Anio ,
	CB_CODIGO NumeroEmpleado ,			
	CO_NUMERO Concepto ,
	RS_CODIGO Codigo , 	
	AC_MES_01 Pesos ,	
	AC_MES_02 Pesos ,	
	AC_MES_03 Pesos ,
	AC_MES_04 Pesos ,
	AC_MES_05 Pesos ,
	AC_MES_06 Pesos ,
	AC_MES_07 Pesos ,
	AC_MES_08 Pesos ,
	AC_MES_09 Pesos ,
	AC_MES_10 Pesos ,
	AC_MES_11 Pesos ,
	AC_MES_12 Pesos ,
	AC_MES_13 Pesos ,	
	AC_ANUAL Pesos 
)
GO

ALTER TABLE ACUMULA_RS ADD  CONSTRAINT PK_ACUMULA_RS PRIMARY KEY CLUSTERED 
(
	AC_YEAR ASC,
	CB_CODIGO ASC,
	CO_NUMERO ASC,
	RS_CODIGO ASC
)
GO

ALTER TABLE ACUMULA_RS  WITH NOCHECK ADD  CONSTRAINT FK_Acumula_RS_Colabora FOREIGN KEY(CB_CODIGO)
	REFERENCES COLABORA (CB_CODIGO)
	ON UPDATE CASCADE
	ON DELETE CASCADE
GO

ALTER TABLE ACUMULA_RS CHECK CONSTRAINT FK_Acumula_RS_Colabora
GO

ALTER TABLE ACUMULA_RS  WITH NOCHECK ADD  CONSTRAINT FK_Acumula_RS_Concepto FOREIGN KEY(CO_NUMERO)
	REFERENCES CONCEPTO (CO_NUMERO)
	ON UPDATE CASCADE
GO

ALTER TABLE ACUMULA_RS CHECK CONSTRAINT FK_Acumula_RS_Concepto
GO

Create Index IDX_ACUMULA_RS on ACUMULA_RS 
(
	AC_YEAR ASC,
	CB_CODIGO ASC,
	CO_NUMERO ASC
)
GO

CREATE UNIQUE NONCLUSTERED INDEX IDX_ACUMULA_RS_AC_ID ON ACUMULA_RS
(
	AC_ID ASC
)

GO 

CREATE NONCLUSTERED INDEX IDX_NOMINA_NO_STATUS ON NOMINA
(
	PE_YEAR ASC,
	PE_TIPO ASC,
	PE_NUMERO ASC,
	NO_STATUS ASC
)
INCLUDE ( CB_CODIGO, NO_TIMBRO) 

GO

CREATE TABLE PCED_ENC (
        PE_TIPO        Codigo1,
        PE_NO_CT    Empleados,
        RS_CODIGO    Codigo,
        PE_YEAR        Anio,
        PE_MES        Mes,
        PE_ARCHIVO    PathArchivo
    )
GO
    
ALTER TABLE PCED_ENC
    ADD CONSTRAINT PK_PCED_ENC PRIMARY KEY (RS_CODIGO, PE_YEAR, PE_MES)  
GO

CREATE TABLE PCED_FON (
       CB_CODIGO            NumeroEmpleado,
       PR_TIPO              Codigo1,
       PR_REFEREN           Referencia,
       PF_YEAR              Anio,
       PF_MES               Mes,
	   RS_CODIGO			Codigo,
       PF_PAGO              Pesos,
       PF_PLAZO             FolioChico,
       PF_PAGADAS           FolioChico,
	   PF_REUBICA			Codigo1,
       PF_CAPTURA           Fecha,
       US_CODIGO            Usuario,
	   PF_CTD				Empleados,
	   PF_NO_SS				Descripcion
)
GO

ALTER TABLE PCED_FON
	ADD CONSTRAINT PK_PCED_FON PRIMARY KEY (CB_CODIGO, PR_TIPO, PR_REFEREN, PF_YEAR, PF_MES)
GO

ALTER TABLE PCED_FON
		   ADD CONSTRAINT FK_PCed_Fon_Prestamo
		   FOREIGN KEY (CB_CODIGO, PR_TIPO, PR_REFEREN)
		   REFERENCES PRESTAMO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
GO

CREATE TABLE FON_TOT_RS
	(
		FT_YEAR Anio ,
		FT_MONTH Mes ,
		PR_TIPO Codigo1 ,
		RS_CODIGO Codigo,
		FT_STATUS Status ,
		FT_RETENC Pesos ,
		FT_NOMINA Pesos ,
		FT_TAJUST Codigo1 ,
		FT_AJUSTE Pesos ,
		FT_EMPLEAD Empleados ,
		FT_CUANTOS Empleados ,
		FT_BAJAS Empleados ,
		FT_INCAPA Empleados ,
		FT_DETALLE Memo NULL,
		LLAVE int IDENTITY(1,1) ,
		CONSTRAINT PK_FON_TOT_RS PRIMARY KEY CLUSTERED 
		(
			FT_YEAR ASC,
			FT_MONTH ASC,
			PR_TIPO ASC, 
			RS_CODIGO ASC
		)
	)
	
GO 

ALTER TABLE FON_TOT_RS  WITH NOCHECK ADD  CONSTRAINT FK_FON_TOT_RS_FON_TOT FOREIGN KEY(FT_YEAR, FT_MONTH, PR_TIPO)
		REFERENCES FON_TOT (FT_YEAR, FT_MONTH, PR_TIPO)
		ON UPDATE CASCADE
		ON DELETE CASCADE
		
GO 

ALTER TABLE FON_TOT_RS CHECK CONSTRAINT FK_FON_TOT_RS_FON_TOT

GO 


CREATE TABLE T_WFTablas  (
		TablaID FolioGrande,
		TablaNombre Observaciones,
		TablaOrden FolioChico,
		TablaActivo BooleanoWFN,
		TablaInsertGet varchar(max),
		TablaInsertPut Formula,
		TablaInsertLast FechaWFN,
		TablaUpdateGet varchar(max),
		TablaUpdatePut Formula,
		TablaUpdateLast FechaWFN,
		TablaRango		Status, 
		TablaRangoIni	Dias,
		TablaRangoFin	Dias, 
		TablaGrupo		FolioGrande,
		CONSTRAINT PK_WFTablas PRIMARY KEY CLUSTERED 
		(
			TablaID ASC
		)
) 


CREATE TABLE T_WFBorrados (
	BorradoID FolioGrande IDENTITY(1,1),
	TablaID FolioGrande,
	BorradoLlave FolioGrande,
	BorradoDeleted FechaWFN,
 	CONSTRAINT PK_WFBorrados PRIMARY KEY CLUSTERED 
	(
		BorradoID ASC
	)
)

GO


CREATE TABLE T_WFTRANSACCIONES(
	TRANSID FolioGrande IDENTITY(1,1),
	TRANSNOMBRE Formula,
	TAREAID FolioGrande,
	OBJETOID FolioGrande,
	VERBOID FolioGrande,
	TRANSCARGO XML NULL,
	ERRORID FolioGrande,
	TRANSERROR varchar(max),
	TRANSINICIAL FechaWFN,
	TRANSFINAL FechaWFN,
	TransCargoRespuesta XML null,
	TransCargoWFTress XML null,
	TransCargoRespuestaTress XML null,
	CONSTRAINT PK_WFTRANSACCIONES PRIMARY KEY CLUSTERED 
	(
		TRANSID ASC
	)
)

GO

CREATE TABLE KAR_CONCEP(	
	CO_NUMERO dbo.Concepto ,
	KC_VERSION FolioGrande,
	US_FEC_MOD Fecha, 
	US_CODIGO  Usuario, 
	CO_A_PTU dbo.Booleano ,
	CO_ACTIVO dbo.Booleano ,
	CO_CALCULA dbo.Booleano ,
	CO_DESCRIP dbo.Descripcion ,
	CO_FORMULA dbo.Formula ,
	CO_G_IMSS dbo.Booleano ,
	CO_G_ISPT dbo.Booleano ,
	CO_IMP_CAL dbo.Formula ,
	CO_IMPRIME dbo.Booleano ,
	CO_LISTADO dbo.Booleano ,
	CO_MENSUAL dbo.Booleano ,		
	CO_QUERY dbo.Condicion ,
	CO_RECIBO dbo.Formula ,
	CO_TIPO dbo.Status ,
	CO_X_ISPT dbo.Formula ,
	CO_SUB_CTA dbo.Descripcion ,
	CO_CAMBIA dbo.Booleano ,
	CO_D_EXT dbo.Codigo ,
	CO_D_NOM dbo.Descripcion ,
	CO_LIM_SUP dbo.Pesos ,
	CO_LIM_INF dbo.Pesos ,
	CO_VER_INF dbo.Booleano ,
	CO_VER_SUP dbo.Booleano ,
	CO_VER_ACC dbo.Booleano ,
	CO_GPO_ACC dbo.ListaContenido ,
	CO_ISN dbo.Booleano ,
	CO_SUMRECI dbo.Booleano ,
	CO_FRM_ALT dbo.Formula ,
	CO_USO_NOM dbo.Status ,
	CO_SAT_CLP dbo.Status ,
	CO_SAT_TPP dbo.Codigo ,
	CO_SAT_CLN dbo.Status ,
	CO_SAT_TPN dbo.Codigo ,
	CO_SAT_EXE dbo.Status ,
	CO_SAT_CON dbo.Concepto ,	
	CO_PS_TIPO dbo.Status ,
 CONSTRAINT PK_KAR_CONCEP PRIMARY KEY CLUSTERED 
(
	CO_NUMERO ASC, 
	KC_VERSION ASC
)
)
GO

CREATE TABLE CONCEPTOVR(
		CO_NUMERO Concepto, 
		KC_VERSION FolioGrande,
		US_FEC_MOD Fecha, 
		US_CODIGO  Usuario, 

		CONSTRAINT PK_CONCEPTOVR PRIMARY KEY CLUSTERED 
		(
		CO_NUMERO ASC 
		)
	)
GO

CREATE TABLE CONCEPTOVT(
		KT_VERSION FolioGrande, 
		CO_NUMERO Concepto, 
		KC_VERSION FolioGrande,
		KT_FECHA Fecha, 
		CONSTRAINT PK_CONCEPTOVT PRIMARY KEY CLUSTERED 
		(
		KT_VERSION ASC,
		CO_NUMERO ASC, 
		KC_VERSION ASC
		)
	)

GO

CREATE TABLE NOM_CONCIL(
		PE_YEAR Anio,
		PE_TIPO NominaTipo,
		PE_NUMERO NominaNumero,
		CB_CODIGO NumeroEmpleado,
		NC_CURP Descripcion, 
		NC_STATUS	Status, 
		NC_RESULT	Status, 
		NC_RES_DESC DescLarga, 
		NC_VER_COD  DescLarga, 
		NC_VERSION	DescLarga, 
		NC_UUID		DescLarga, 
		NC_FEC_TIM  Fecha,
		NC_FACTURA  FolioGrande,
		US_CODIGO	Usuario, 
		US_FEC_MOD	Fecha,
		NC_APLICADO Booleano,
	
	 CONSTRAINT PK_NOM_CONCIL PRIMARY KEY CLUSTERED 
	(
		PE_YEAR ASC,
		PE_TIPO ASC,
		PE_NUMERO ASC,
		CB_CODIGO ASC
	) 
	) 

GO 

ALTER TABLE NOM_CONCIL  WITH NOCHECK ADD  CONSTRAINT FK_PK_NOM_CONCIL_Periodo FOREIGN KEY(PE_YEAR, PE_TIPO, PE_NUMERO)
REFERENCES PERIODO (PE_YEAR, PE_TIPO, PE_NUMERO)
ON UPDATE CASCADE
ON DELETE CASCADE

GO 

ALTER TABLE NOM_CONCIL CHECK CONSTRAINT FK_PK_NOM_CONCIL_Periodo
GO 

create table T_Analitica
(
	AnaliticaID				FolioGrande Identity(1,1), 	
	AnaliticaModulo			Descripcion, 
	AnaliticaHelpContext	FolioGrande,
	AnaliticaCodigo			Descripcion, 
	AnaliticaDescripcion	Formula, 	
	AnaliticaBool			Booleano, 
	AnaliticaNumero			Pesos,
	AnaliticaFecha			Fecha,
	AnaliticaFechaUpd		Fecha, 
	AnaliticaUsuario		Usuario,
	AnaliticaTexto			varchar(max), 

	CONSTRAINT PK_Analitica PRIMARY KEY CLUSTERED 
	(
		AnaliticaID
	)
)
GO

CREATE UNIQUE INDEX UQIDX_Analitica ON T_Analitica
(
		AnaliticaModulo,
		AnaliticaHelpContext, 
		AnaliticaCodigo  
)
GO

CREATE TABLE TPROCESO
(
	TP_ID Status, 
	TP_MODULO  Descripcion, 
	TP_DESCRIP Formula,
	TP_VERSION Descripcion, 
	CONSTRAINT PK_TPROCESO PRIMARY KEY CLUSTERED (TP_ID) 
)

go 
