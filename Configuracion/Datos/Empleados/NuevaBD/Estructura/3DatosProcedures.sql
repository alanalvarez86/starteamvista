create procedure DBO.SET_STATUS_PERIODO(
@ANIO SMALLINT,
@TIPO SMALLINT,
@NUMERO SMALLINT,
@USUARIO SMALLINT,
@MAXSTATUS SMALLINT OUTPUT )
as
begin
     SET NOCOUNT ON
     declare @Diferentes SmallInt;
     declare @Status SmallInt;
     declare @Empleados Integer;
     declare @Percepciones NUMERIC( 15, 2 );
     declare @Deducciones NUMERIC( 15, 2 );
     declare @Netos NUMERIC( 15, 2 );
     declare @TotEmpleados Integer;
     declare @TotPercepciones NUMERIC( 15, 2 );
     declare @TotDeducciones NUMERIC( 15, 2 );
     declare @TotNetos NUMERIC( 15, 2 );
     declare TemporalNomina CURSOR for
        select N.NO_STATUS, COUNT(*), SUM( N.NO_PERCEPC ), SUM( N.NO_DEDUCCI ), SUM( N.NO_NETO )
        from DBO.NOMINA as N
        where  ( N.PE_YEAR = @Anio ) and
               ( N.PE_TIPO = @Tipo ) and
               ( N.PE_NUMERO = @Numero )
        group by N.NO_STATUS
        order by N.NO_STATUS;
     set @Diferentes = 0;
     set @MaxStatus = 0;
     set @TotEmpleados = 0;
     set @TotPercepciones = 0;
     set @TotDeducciones = 0;
     set @TotNetos = 0;
	 open TemporalNomina;
     fetch NEXT from TemporalNomina
     into @Status,
          @Empleados,
          @Percepciones,
          @Deducciones,
          @Netos;
     while ( @@Fetch_Status = 0 )
     begin
          set @Diferentes = @Diferentes + 1;
          set @MaxStatus = @Status;
          set @TotEmpleados = @TotEmpleados + @Empleados;
          set @TotPercepciones = @TotPercepciones + @Percepciones;
          set @TotDeducciones = @TotDeducciones + @Deducciones;
          set @TotNetos = @TotNetos + @Netos;
          fetch NEXT from TemporalNomina
          into @Status,
               @Empleados,
               @Percepciones,
               @Deducciones,
               @Netos;
     end;
	 close TemporalNomina;
	 deallocate TemporalNomina;
     if ( @Diferentes > 0 )
     begin
          if ( @Diferentes > 1 )
          begin
               if ( @MaxStatus = 4 )
                  set @MaxStatus = 3
               else
                   if ( @MaxStatus = 6 )
                      set @MaxStatus = 5;
               set @TotPercepciones = 0;
               set @TotDeducciones = 0;
               set @TotNetos = 0;
          end;
          if ( @MaxStatus = 1 )
             set @MaxStatus = 2;
     end;
     update DBO.PERIODO set
            PE_STATUS = @MaxStatus,
            PE_NUM_EMP = @TotEmpleados,
            PE_TOT_PER = @TotPercepciones,
            PE_TOT_DED = @TotDeducciones,
            PE_TOT_NET = @TotNetos,
            US_CODIGO = @Usuario
     where ( PE_YEAR = @Anio ) and
           ( PE_TIPO = @Tipo ) and
           ( PE_NUMERO = @Numero );
end
GO

create  procedure SET_STATUS_PRESTAMOS( @EMPLEADO Integer )
as
begin
     SET NOCOUNT ON

	 declare @DescartarPrestamosFonacot Booleano 
	 declare @UsaModalidadFonacotPrestamo Booleano 
	 declare @TipoPrestamoFonacot Codigo1

	 select @UsaModalidadFonacotPrestamo = LEFT( LTRIM(GL_FORMULA), 1)  from GLOBAL where GL_CODIGO = 328 
	 set @UsaModalidadFonacotPrestamo = COALESCE( @UsaModalidadFonacotPrestamo, 'N' ) 

	 select @TipoPrestamoFonacot = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	 set @TipoPrestamoFonacot = COALESCE( @TipoPrestamoFonacot, '' ) 

	 set @DescartarPrestamosFonacot = 'N' 
	 if ( @TipoPrestamoFonacot <> ''  and @UsaModalidadFonacotPrestamo = 'S' ) 
		set @DescartarPrestamosFonacot = 'S' 


     update DBO.PRESTAMO set PR_STATUS = 2 where			
            ( PR_STATUS = 0 ) and 
			( @DescartarPrestamosFonacot = 'N' or ( @DescartarPrestamosFonacot = 'S' and PR_TIPO <> @TipoPrestamoFonacot )  ) and 
            ( CB_CODIGO = @Empleado ) and
            ( ( ( PR_MONTO >= 0 ) and ( PR_SALDO < 0.01 ) ) or
            ( ( PR_MONTO < 0 ) and ( PR_SALDO > -0.01 ) ) );

     update DBO.PRESTAMO set PR_STATUS = 0 where
            ( PR_STATUS = 2 ) and 
			( @DescartarPrestamosFonacot = 'N' or ( @DescartarPrestamosFonacot = 'S' and PR_TIPO <> @TipoPrestamoFonacot )  ) and 
            ( CB_CODIGO = @Empleado ) and
            ( ( ( PR_MONTO >= 0 ) and ( PR_SALDO >= 0.01 ) ) or
            ( ( PR_MONTO < 0 ) and ( PR_SALDO <= -0.01 ) ) );
end
GO

CREATE PROCEDURE AFECTA_UN_CONCEPTO
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@CONCEPTO INTEGER,
    				@MES SMALLINT,
    				@FACTOR INTEGER,
    				@MONTO NUMERIC(15,2), 				
					@RS_CODIGO Codigo = ''
AS
BEGIN 
     SET NOCOUNT ON

	 -- Si no viene razon social le asigna la razon al final del mes del empleado 
	 IF ( @RS_CODIGO = '' ) 
	 begin 
		 declare @CB_PATRON Codigo 
		 declare @Fecha Fecha 
		 declare @MesIni Fecha 
		 declare @MesFin Fecha
		 

		 Select  @Fecha = DateAdd(yy, @ANIO-1900, DateAdd(m,  @MES - 1, 1- 1)) 
		 set @MesIni = CONVERT(datetime, @Fecha, 112 ) 
		 set @MesFin = dateadd(day, -1,  dateadd( month, 1, @MesIni ) );  
		 select @CB_PATRON = dbo.SP_KARDEX_CB_PATRON(@MesFin, @EMPLEADO)
		 set @CB_PATRON = coalesce( @CB_PATRON , '' ) 
		 select @RS_CODIGO = RS_CODIGO from RPATRON RP where RP.TB_CODIGO = @CB_PATRON 
		 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 
	 end 
	 EXEC AFECTA_UN_CONCEPTO_RAZON_SOCIAL @ANIO, @EMPLEADO, @CONCEPTO, @MES, @FACTOR, @MONTO, @RS_CODIGO
	 EXEC CALCULA_ACUMULADOS_TOTALES  @ANIO, @EMPLEADO, @CONCEPTO, @MES
END 

GO

CREATE PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON

	 	 DECLARE @RS_CODIGO Codigo 

	 select @RS_CODIGO = RSOCIAL.RS_CODIGO from NOMINA 
	 join RPATRON on NOMINA.CB_PATRON = RPATRON.TB_CODIGO 
	 join RSOCIAL on RSOCIAL.RS_CODIGO  = RPATRON.RS_CODIGO 
	 where NOMINA.PE_YEAR = @ANIO and NOMINA.PE_TIPO = @TIPO and NOMINA.PE_NUMERO = @NUMERO and NOMINA.CB_CODIGO = @EMPLEADO 

	 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 

     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
     DECLARE @M1216 NUMERIC(15,2);
     DECLARE @TP_Clasif SMALLINT;

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1012=N.NO_PER_ISN,
            @M1013=N.NO_TOTAL_1,
            @M1014=N.NO_TOTAL_2,
            @M1015=N.NO_TOTAL_3,
            @M1016=N.NO_TOTAL_4,
            @M1017=N.NO_TOTAL_5,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1124=N.NO_DIAS_IH,
            @M1125=N.NO_DIAS_ID,
            @M1126=N.NO_DIAS_IT,
            @M1127=N.NO_DIAS_DT,
            @M1128=N.NO_DIAS_FS,
            @M1129=N.NO_DIAS_FT,
            @M1200=N.NO_JORNADA,
            @M1201=N.NO_HORAS,
            @M1202=N.NO_EXTRAS,
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
            @M1216=N.NO_PRE_EXT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
		Select @M1200 = @M1201 / @M1200;
     Select @M1109 = @M1200 * @M1109;

     if ( @M1201 < 0 ) and ( @M1200 > 0 )
		set @M1200 = -@M1200;

     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000, @RS_CODIGO;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001, @RS_CODIGO;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002, @RS_CODIGO;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003, @RS_CODIGO;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005, @RS_CODIGO;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006, @RS_CODIGO;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007, @RS_CODIGO;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008, @RS_CODIGO;
     if ( @M1009 <> 0 )
       EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009, @RS_CODIGO;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010, @RS_CODIGO;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011, @RS_CODIGO;
  	 if ( @M1012 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1012, @Mes, @Factor, @M1012, @RS_CODIGO;
  	 if ( @M1013 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1013, @Mes, @Factor, @M1013, @RS_CODIGO;
  	 if ( @M1014 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1014, @Mes, @Factor, @M1014, @RS_CODIGO;
  	 if ( @M1015 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1015, @Mes, @Factor, @M1015, @RS_CODIGO;
  	 if ( @M1016 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1016, @Mes, @Factor, @M1016, @RS_CODIGO;
  	 if ( @M1017 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1017, @Mes, @Factor, @M1017, @RS_CODIGO;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100, @RS_CODIGO;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101, @RS_CODIGO;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102, @RS_CODIGO;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103, @RS_CODIGO;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104, @RS_CODIGO;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105, @RS_CODIGO;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106, @RS_CODIGO;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107, @RS_CODIGO;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108, @RS_CODIGO;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109, @RS_CODIGO;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110, @RS_CODIGO;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111, @RS_CODIGO;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112, @RS_CODIGO;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113, @RS_CODIGO;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114, @RS_CODIGO;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115, @RS_CODIGO;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116, @RS_CODIGO;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117, @RS_CODIGO;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118, @RS_CODIGO;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119, @RS_CODIGO;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120, @RS_CODIGO;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121, @RS_CODIGO;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122, @RS_CODIGO;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123, @RS_CODIGO;
     if ( @M1124 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1124, @Mes, @Factor, @M1124, @RS_CODIGO;
     if ( @M1125 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1125, @Mes, @Factor, @M1125, @RS_CODIGO;
     if ( @M1126 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1126, @Mes, @Factor, @M1126, @RS_CODIGO;
     if ( @M1127 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1127, @Mes, @Factor, @M1127, @RS_CODIGO;
     if ( @M1128 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1128, @Mes, @Factor, @M1128, @RS_CODIGO;
     if ( @M1129 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1129, @Mes, @Factor, @M1129, @RS_CODIGO;
     if ( @M1200 <> 0 )
     begin
          select @TP_Clasif = TP_CLAS from TPERIODO where TP_TIPO = @Tipo;

          if ( @TP_Clasif = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( @TP_Clasif = 1 )
             Select @M1200 = 48 * @M1200;
          else
          if ( @TP_Clasif = 2 )
             Select @M1200 = 96 * @M1200;
          else
          if ( @TP_Clasif = 3 )
             Select @M1200 = 104 * @M1200;
          else
          if ( @TP_Clasif = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @TP_Clasif = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200, @RS_CODIGO;

     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201, @RS_CODIGO;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202, @RS_CODIGO;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203, @RS_CODIGO;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204, @RS_CODIGO;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205, @RS_CODIGO;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206, @RS_CODIGO;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207, @RS_CODIGO;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208, @RS_CODIGO;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209, @RS_CODIGO;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210, @RS_CODIGO;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211, @RS_CODIGO;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212, @RS_CODIGO;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213, @RS_CODIGO;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214, @RS_CODIGO;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215, @RS_CODIGO;
	 if ( @M1216 <> 0 )
	    EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216, @RS_CODIGO;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000, @RS_CODIGO;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004,  @RS_CODIGO;


	EXECUTE AFECTA_EMPLEADO_PS_PREVISION_SOCIAL @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor, @RS_CODIGO
	EXECUTE AFECTA_EMPLEADO_PS_SALARIOS			@Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor, @RS_CODIGO 

END
GO 

CREATE PROCEDURE SP_CLAS_AUSENCIA_ADICIONAL @Empleado Int, @Fecha DateTime AS
GO

CREATE PROCEDURE SP_CLAS_AUSENCIA @Empleado Int, @Fecha DateTime AS
BEGIN
  SET NOCOUNT ON
	Declare @FEC_KAR DateTime
	Declare @PUESTO Char(6)
	Declare @CLASIFI Char(6)
	Declare @TURNO Char(6)
  Declare @NIVEL1 Char(6)
	Declare @NIVEL2 Char(6)
	Declare @NIVEL3 Char(6)
	Declare @NIVEL4 Char(6)
	Declare @NIVEL5 Char(6)
  Declare @NIVEL6 Char(6)
	Declare @NIVEL7 Char(6)
	Declare @NIVEL8 Char(6)
	Declare @NIVEL9 Char(6)
  Declare @SALARIO PesosDiario
  Declare @T_PUESTO Char(6)
	declare @T_CLASIFI Char(6)
	declare @T_TURNO Char(6)
  declare @T_NIVEL1 Char(6)
	declare @T_NIVEL2 Char(6)
	declare @T_NIVEL3 Char(6)
	declare @T_NIVEL4 Char(6)
	declare @T_NIVEL5 Char(6)
  declare @T_NIVEL6 Char(6)
	declare @T_NIVEL7 Char(6)
	declare @T_NIVEL8 Char(6)
	declare @T_NIVEL9 Char(6)
	declare @NOMINA smallint
  declare @TABLASS Char(1)
  declare @AUTOSAL Char(1)
  declare @TB_SAL PesosDiario
  declare @SalTemp Varchar(1)

 	select @FEC_KAR = CB_FEC_KAR,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
    @NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2,
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4,
		@NIVEL5 = CB_NIVEL5,
    @NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7,
		@NIVEL8 = CB_NIVEL8,
		@NIVEL9 = CB_NIVEL9,
    @SALARIO = CB_SALARIO,
 		@NOMINA = CB_NOMINA,
    @TABLASS = CB_TABLASS,
    @AUTOSAL = CB_AUTOSAL
  	from   	COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @FECHA < @FEC_KAR )
  	begin
 		select @PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI,
			@TURNO = CB_TURNO,
      @NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2,
			@NIVEL3 = CB_NIVEL3,
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
      @NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7,
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
      @SALARIO = CB_SALARIO,
			@NOMINA = CB_NOMINA,
      @TABLASS = CB_TABLASS,
      @AUTOSAL = CB_AUTOSAL
      from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

 	select @FEC_KAR = AU_FECHA,
	       @T_PUESTO = CB_PUESTO,
	       @T_CLASIFI = CB_CLASIFI,
	       @T_TURNO = CB_TURNO,
         @T_NIVEL1 = CB_NIVEL1,
	       @T_NIVEL2 = CB_NIVEL2,
	       @T_NIVEL3 = CB_NIVEL3,
	       @T_NIVEL4 = CB_NIVEL4,
	       @T_NIVEL5 = CB_NIVEL5,
         @T_NIVEL6 = CB_NIVEL6,
	       @T_NIVEL7 = CB_NIVEL7,
	       @T_NIVEL8 = CB_NIVEL8,
	       @T_NIVEL9 = CB_NIVEL9
  	from CLASITMP where
        ( CB_CODIGO = @Empleado ) and
        ( AU_FECHA = @Fecha ) and
        ( US_COD_OK <> 0 );
        if ( @FEC_KAR is not NULL )
        begin
             if ( @T_PUESTO <> '' )
             begin
                  select @PUESTO = @T_PUESTO;
             end
             if ( @T_CLASIFI <> '' )
             begin
                  select @CLASIFI = @T_CLASIFI;
                  if ( @AUTOSAL = 'S' )
                  begin
	                    select @SalTemp = GL_FORMULA from GLOBAL where GL_CODIGO = 304;
	                    if ( @SalTemp = 'S' )
                      begin
                           select @TB_SAL = TB_SALARIO from CLASIFI where TB_CODIGO = @CLASIFI
                           if ( @TB_SAL > @SALARIO )
                           begin
                               select @SALARIO = @TB_SAL;
                           end
                      end
                  end
             end
             if ( @T_TURNO <> '' )
             begin
                  select @TURNO = @T_TURNO;
             end
             if ( @T_NIVEL1 <> '' )
             begin
                  select @NIVEL1 = @T_NIVEL1;
             end
             if ( @T_NIVEL2 <> '' )
             begin
                  select @NIVEL2 = @T_NIVEL2;
             end
             if ( @T_NIVEL3 <> '' )
             begin
                  select @NIVEL3 = @T_NIVEL3;
             end
             if ( @T_NIVEL4 <> '' )
             begin
                  select @NIVEL4 = @T_NIVEL4;
             end
             if ( @T_NIVEL5 <> '' )
             begin
                  select @NIVEL5 = @T_NIVEL5;
             end
             if ( @T_NIVEL6 <> '' )
             begin
                  select @NIVEL6 = @T_NIVEL6;
             end
             if ( @T_NIVEL7 <> '' )
             begin
                  select @NIVEL7 = @T_NIVEL7;
             end
             if ( @T_NIVEL8 <> '' )
             begin
                  select @NIVEL8 = @T_NIVEL8;
             end
             if ( @T_NIVEL9 <> '' )
             begin
                  select @NIVEL9 = @T_NIVEL9;
             end
        end

  	update AUSENCIA
  	set   CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
          CB_SALARIO = @SALARIO,
          CB_NOMINA  = @NOMINA,
          CB_TABLASS = @TABLASS
  	where CB_CODIGO = @Empleado and AU_FECHA = @Fecha;

        EXECUTE SP_CLAS_AUSENCIA_ADICIONAL @Empleado, @Fecha;
END
GO

CREATE  PROCEDURE SP_STATUS_INCIDENCIA
    				@FECHA DATETIME,
	    			@EMPLEADO INTEGER,
    				@RESULTADO SMALLINT OUTPUT,
    				@INCIDENCIA CHAR(3) OUTPUT
AS
	DECLARE @CUANTOS INTEGER;
	DECLARE @CLASEPERMISO SMALLINT;
  	
	Select @Resultado = dbo.SP_STATUS_ACT(@Fecha, @Empleado)
  
  	if ( @Resultado = 0 ) Select @Incidencia = 'IGR';
  	else if ( @Resultado = 9 ) Select @Incidencia = 'BAJ';

 
  	if ( @Resultado = 1 ) 
  	begin
		select @Incidencia = 'VAC', @Resultado = 2
      	from   VACACION
      	where  CB_CODIGO = @Empleado 
		and VA_FEC_INI <= @Fecha 
		and VA_FEC_FIN > @Fecha 
		and VA_TIPO = 1
  	end

 
  	if ( @Resultado = 1 ) 
  	begin
      	select @Incidencia = IN_TIPO, @Resultado = 3
      	from   INCAPACI
      	where  CB_CODIGO = @Empleado 
		and IN_FEC_INI <= @Fecha 
		and IN_FEC_FIN > @Fecha
  	end

  
  	if ( @Resultado = 1 ) 
  	begin
      	select @Incidencia = PM_TIPO, @ClasePermiso = PM_CLASIFI,	
		@Resultado = @ClasePermiso + 4
		from 	PERMISO
      	where CB_CODIGO = @Empleado 
		and PM_FEC_INI <= @Fecha 
		and PM_FEC_FIN > @Fecha
	
  	end

  	if ( @Incidencia IS NULL ) select @Incidencia = '';
GO

CREATE PROCEDURE AFECTA_1_FALTA(
    			@EMPLEADO INTEGER,
    			@FECHA DATETIME,
    			@TIPODIA SMALLINT,
    			@INCIDENCIA CHAR(6),
    			@FACTOR INTEGER)
AS
        SET NOCOUNT ON
	DECLARE @AU_TIPO CHAR(6);
	DECLARE @AU_TIPODIA SMALLINT;
	DECLARE @AU_HORAS NUMERIC(15,2);
  
  	select 	@AU_TIPO = AU_TIPO, 
		@AU_TIPODIA = AU_TIPODIA, 
		@AU_HORAS = AU_HORAS
  	from   	AUSENCIA
  	where  ( CB_CODIGO = @Empleado ) 
	and ( AU_FECHA = @Fecha )

  	if ( @Factor >= 1 ) 
  	begin
    		if ( @AU_TIPODIA IS NULL ) 
    		begin
      			insert into AUSENCIA (CB_CODIGO, AU_FECHA, AU_TIPO, AU_TIPODIA )
                    	values (@Empleado, @Fecha, @Incidencia, @TipoDia );
      			execute SP_CLAS_AUSENCIA @Empleado, @Fecha 
    	end
    	else  
    	begin
      		update 	AUSENCIA
      		set    	AU_TIPO = @Incidencia, 
			AU_TIPODIA = @TipoDia
      		where  ( CB_CODIGO = @Empleado ) 
		and ( AU_FECHA = @Fecha );
    	end
  	end
  	else if (( @AU_TIPODIA = @TipoDia ) and ( @AU_HORAS = 0 )) 
  	begin
    		delete from AUSENCIA
    		where  ( CB_CODIGO = @Empleado ) 
		and ( AU_FECHA = @Fecha );
  	end
GO

CREATE PROCEDURE AFECTA_1_AJUSTE 
    		@EMPLEADO INTEGER,
    		@FECHA DATETIME,
    		@TIPODIA SMALLINT,
    		@INCIDENCIA CHAR(6) ,
    		@FACTOR INTEGER
AS
        SET NOCOUNT ON
	DECLARE @AU_TIPO CHAR(6);
	DECLARE @AU_TIPODIA SMALLINT;
	DECLARE @AU_HORAS NUMERIC(15,2);

  	select 	@AU_TIPO = AU_TIPO, 
		@AU_TIPODIA = AU_TIPODIA, 
		@AU_HORAS = AU_HORAS
  	from   AUSENCIA
  	where  ( CB_CODIGO = @Empleado ) 
	and ( AU_FECHA = @Fecha )

  	if ( @AU_TIPODIA IS NOT NULL ) 
  	begin
    		if ( @Factor >= 1 ) 
    		begin
      			if ( @AU_TIPO = 'FI' ) 
			begin
        			update AUSENCIA
        			set    AU_TIPO = @Incidencia
        			where  ( CB_CODIGO = @Empleado ) 
				and ( AU_FECHA = @Fecha )
			end
    		end
    		else  
    		begin
      			if (( @AU_TIPODIA = @TipoDia ) 
			and ( @AU_HORAS = 0 ))
        			update AUSENCIA
        			set    AU_TIPO = 'FI'
        			where  ( CB_CODIGO = @Empleado ) 
				and ( AU_FECHA = @Fecha );
    		end
  	end
GO

CREATE PROCEDURE AFECTA_FALTAS
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@FACTOR INTEGER
AS
        SET NOCOUNT ON
	declare @FechaIni   DateTime;
  	declare @Incidencia Char( 6 );
  	declare @TipoDia    SmallInt;
  	declare @Empleado   Integer;
  	declare @FEC_INI    DateTime;
  	declare @MOTIVO     SmallInt;
  	declare @DIAS       Numeric(15,2);
	Declare @Bloqueo    VARCHAR(1);
		
	select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289;
	
	if( @Bloqueo = 'S' )
	begin
		 Update GLOBAL set GL_FORMULA = 'N' where GL_CODIGO = 289;
	end
  	
	select @FechaIni = P.PE_ASI_INI  /* Fecha de inicio de asistencia */
	from PERIODO P
	where
         ( P.PE_YEAR = @Anio ) and
         ( P.PE_TIPO = @Tipo ) and
         ( P.PE_NUMERO = @Numero )
	
	DECLARE TemporalFaltas CURSOR FOR 
  		select F.CB_CODIGO, F.FA_FEC_INI, F.FA_MOTIVO, F.FA_DIAS
  		from   FALTAS F
       	left outer join NOMINA N on 
         	( N.PE_YEAR = @Anio ) 
		and ( N.PE_TIPO = @Tipo ) 
		and ( N.PE_NUMERO = @Numero ) 
		and ( N.CB_CODIGO = F.CB_CODIGO )
  		where ( F.PE_YEAR = @Anio ) 
		and ( F.PE_TIPO = @Tipo ) 
		and ( F.PE_NUMERO = @Numero )
		and ( F.FA_DIA_HOR = 'D' ) 
		and ( F.FA_MOTIVO  IN (0,1,2,4,12))

   	OPEN TemporalFaltas
   	FETCH NEXT FROM TemporalFaltas
   	into @Empleado, @FEC_INI, @MOTIVO, @DIAS
    WHILE @@FETCH_STATUS = 0
   	BEGIN
        if ( @FEC_INI <= '12/30/1899' ) 
		begin
			select @FEC_INI = @FechaIni;
		end
     	if ( @MOTIVO = 0 ) 
     	begin
       		Select @TipoDia = 0, @Incidencia = 'FI'
     	end
     	else if ( @MOTIVO = 1 )
     	begin
       		Select @TipoDia = 5, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 2 )  
     	begin
       		Select @TipoDia = 4, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 4 ) 
     	begin
       		Select @TipoDia = 6, @Incidencia = 'FJ'
     	end
     	else
     	begin
       		Select @TipoDia = 0, @Incidencia = '';
     	end
     
    
     	while ( @DIAS > 0 ) 
     	begin
       		if ( @MOTIVO <> 12 ) 
         		execute AFECTA_1_FALTA @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor 
       		else if ( @FEC_INI < @FechaIni )
         		execute AFECTA_1_AJUSTE @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor 
       
       		select @DIAS = @DIAS - 1, @FEC_INI = @FEC_INI + 1
     	end
   		
		FETCH NEXT FROM TemporalFaltas
   		into @Empleado, @FEC_INI, @MOTIVO, @DIAS 
  	end

   CLOSE TemporalFaltas
   DEALLOCATE TemporalFaltas
   if (@Bloqueo = 'S')
   begin
		Update GLOBAL set GL_FORMULA = 'S' where GL_CODIGO = 289;
   end
GO

CREATE PROCEDURE AFECTA_PERIODO(
       @ANIO SMALLINT,
       @TIPO SMALLINT,
       @NUMERO SMALLINT,
   @USUARIO SMALLINT,
       @STATUSVIEJO SMALLINT,
       @STATUSNUEVO SMALLINT,
       @FACTOR INTEGER )

AS
 SET NOCOUNT ON
 DECLARE @CONCEPTO INTEGER;
 DECLARE @MES SMALLINT;
 DECLARE @CONTADOR INTEGER;
 DECLARE @TOTAL NUMERIC(15,2);
 DECLARE @TIPOAHORRO CHAR(1);
 DECLARE @TIPOPRESTAMO CHAR(1);
 DECLARE @REFERENCIA VARCHAR(8);
        DECLARE @EMPLEADO INTEGER;

      select @MES = P.PE_MES from PERIODO P
 where ( P.PE_YEAR = @Anio )
 and ( P.PE_TIPO = @Tipo )
 and ( P.PE_NUMERO = @Numero )

 if ( select count(*) from NOMINA N
  left outer join COLABORA C
  on ( C.CB_CODIGO = N.CB_CODIGO )
 where
  ( N.PE_YEAR = @Anio )
  and ( N.PE_TIPO = @Tipo )
  and ( N.PE_NUMERO = @Numero )
  and not ( C.CB_CODIGO is NULL )
  and ( N.NO_TIMBRO > 0 )  ) > 0
 begin
   RAISERROR( 'La nomina esta Timbrada', 16, 1 )
   return 0
 end

 Declare TempColabora CURSOR for
         select N.CB_CODIGO from NOMINA N
         left outer join COLABORA C
  on ( C.CB_CODIGO = N.CB_CODIGO )
         where ( N.PE_YEAR = @Anio )
  and ( N.PE_TIPO = @Tipo )
  and ( N.PE_NUMERO = @Numero )
  and ( N.NO_STATUS = @StatusViejo )
  and not ( C.CB_CODIGO is NULL )
  and ( N.NO_TIMBRO = 0 )

 OPEN TempColabora
 fetch NEXT FROM TempColabora
 INTO @EMPLEADO

 set @Contador = 0
 WHILE @@FETCH_STATUS = 0
 BEGIN

  EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor;
  Declare TempAhorro CURSOR for
          select A.AH_TIPO, T.TB_CONCEPT
   from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO )
          where ( A.CB_CODIGO = @Empleado )
   and ( A.AH_STATUS = 0 )

  Open TempAhorro
  fetch NEXT FROM TempAhorro
  into @TipoAhorro, @Concepto
  while @@FETCH_STATUS = 0
         begin
          select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
   from MOVIMIEN M
   where ( M.PE_YEAR = @Anio )
   and ( M.PE_TIPO = @Tipo )
   and ( M.PE_NUMERO = @Numero )
   and ( M.CB_CODIGO= @Empleado )
   and ( M.CO_NUMERO = @Concepto )
   and ( M.MO_ACTIVO = 'S' )

   if ( @Contador > 0 )
          begin
                  Select @Total = @Factor * @Total;
                  update AHORRO  set
                        AH_NUMERO = AH_NUMERO + @Factor,
                         AH_TOTAL  = AH_TOTAL + @Total,
                         AH_SALDO = AH_SALDO + @Total
                  where ( CB_CODIGO = @Empleado )
    and ( AH_TIPO = @TipoAhorro );
          end
   fetch NEXT FROM TempAhorro
    into   @TipoAhorro, @Concepto
  end

  close TempAhorro
      deallocate TempAhorro

  Declare TempPrestamo CURSOR for
               select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT
   from PRESTAMO P join TPRESTA T on
                     ( T.TB_CODIGO = P.PR_TIPO )
               where ( P.CB_CODIGO = @Empleado )
   and ( P.PR_STATUS in ( 0, 2 ) )

  Open TempPrestamo
  fetch NEXT FROM TempPrestamo
  into @TipoPrestamo, @Referencia, @Concepto

  while @@FETCH_STATUS = 0
         begin
          select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
   from MOVIMIEN M
   where ( M.PE_YEAR = @Anio )
   and ( M.PE_TIPO = @Tipo )
   and ( M.PE_NUMERO = @Numero )
   and ( M.CB_CODIGO= @Empleado )
   and ( M.CO_NUMERO = @Concepto )
   and ( M.MO_ACTIVO = 'S' )
   and ( M.MO_REFEREN = @Referencia )

   if ( @Contador > 0 )
          begin
                  Select @Total = @Factor * @Total;
                  update PRESTAMO set
                         PR_NUMERO = PR_NUMERO + @Factor,
                         PR_TOTAL  = PR_TOTAL + @Total,
                         PR_SALDO = PR_SALDO - @Total
                  where ( CB_CODIGO = @Empleado )
    and ( PR_TIPO = @TipoPrestamo )
    and ( PR_REFEREN = @Referencia );
          end
   fetch NEXT FROM TempPrestamo
   into @TipoPrestamo, @Referencia, @Concepto
         end

  close TempPrestamo
      deallocate TempPrestamo

  EXECUTE SET_STATUS_PRESTAMOS @Empleado;

           if ( @StatusNuevo > 0 )
           begin
                 update NOMINA set NO_STATUS = @StatusNuevo
   where ( PE_YEAR = @Anio )
   and ( PE_TIPO = @Tipo )
   and ( PE_NUMERO = @Numero )
   and ( CB_CODIGO = @Empleado );
           end
  fetch NEXT FROM TempColabora
  INTO @EMPLEADO
  set @Contador = @Contador + 1
 end

 close TempColabora
     deallocate TempColabora

      EXECUTE AFECTA_FALTAS @Anio, @Tipo, @Numero, @Factor
      if ( @StatusNuevo > 0 )
      begin

           update NOMINA set NO_STATUS = @StatusNuevo
           where ( PE_YEAR = @Anio )
  and ( PE_TIPO = @Tipo )
  and ( PE_NUMERO = @Numero )
  and ( ( select COUNT(*) from COLABORA C where ( C.CB_CODIGO = Nomina.CB_CODIGO ) ) = 0 )

           EXECUTE SET_STATUS_PERIODO @Anio, @Tipo, @Numero, @Usuario, @StatusNuevo
      end

 RETURN @Contador
GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT,
					@STATUS SMALLINT, 
					@lAlgunosConceptos Booleano = 'N', 
					@ListaConceptos Formula  = '' 
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @TIPO SMALLINT;
	DECLARE @NUMERO SMALLINT;
	
	IF @lAlgunosConceptos = 'N' 
	begin
		exec LIMPIA_ACUMULADOS_RS		@ANIO, @EMPLEADO, @MES  
		exec LIMPIA_ACUMULADOS_TOTALES	@ANIO, @EMPLEADO, @MES 
	end
	else
	begin
		exec LIMPIA_ACUMULADOS_RS_CONCEPTOS @ANIO, @EMPLEADO, @MES, @ListaConceptos		
	end

	Declare TemporalAcumula Cursor for
		select P.PE_TIPO, P.PE_NUMERO from NOMINA N join PERIODO P on
               ( P.PE_YEAR = N.PE_YEAR ) and
               ( P.PE_TIPO = N.PE_TIPO ) and
               ( P.PE_NUMERO = N.PE_NUMERO )
        where
             ( N.NO_STATUS = @Status ) and
             ( N.CB_CODIGO = @Empleado ) and
             ( N.PE_YEAR = @Anio ) and
             ( P.PE_MES = @Mes )

	OPEN TemporalAcumula
	FETCH NEXT FROM TemporalAcumula
	INTO @Tipo, @Numero 
    WHILE @@FETCH_STATUS = 0
	begin
		IF @lAlgunosConceptos = 'N' 
			EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, 1;
		ELSE 
			EXECUTE RECALCULA_ACUMULADOS_CONCEPTOS @Anio, @Empleado, @Mes, @Tipo, @Numero, @ListaConceptos

		FETCH NEXT FROM TemporalAcumula
		INTO @Tipo, @Numero 
    end
    Close TemporalAcumula
    Deallocate TemporalAcumula
	
	IF @lAlgunosConceptos = 'N' 
		exec SP_AcumuladosRazonSocial_SumarAnual @ANIO, @EMPLEADO
	ELSE
		exec SP_AcumuladosRazonSocial_SumarAnual @ANIO, @EMPLEADO, @ListaConceptos 
END
GO

/** Recalcula acumulado del mes 1 al 13**/
CREATE  PROCEDURE RECALCULA_ACUMULADOS_MESES
					@ANIO SMALLINT,
					@MESINI SMALLINT, 
					@MESFIN SMALLINT, 
					@EMPLEADO INTEGER,					
					@STATUS SMALLINT, 
					@lAlgunosConceptos Booleano = 'N' , 
					@ListaConceptos Formula  = '',
					@RangoConceptos INTEGER = 0 
AS
BEGIN
    SET NOCOUNT ON
	declare @iMes smallint 
	set @iMes = @MESINI 
		
	declare @ListaConceptosRango Formula
	declare @Query   nvarchar(max)
    declare @Params	 nvarchar( 150 )

	if @ListaConceptos = '' and @RangoConceptos in ( 0, 1 ) 
		set @ListaConceptos = 'CO_NUMERO > 0 ' 

	if @RangoConceptos = 2 or @RangoConceptos = 3 
	begin
		set @Query = 'select @ListaConceptosRango = coalesce(@ListaConceptosRango' + ' + '','' ' + ', '''') + CONVERT(varchar(255), CO_NUMERO) from CONCEPTO 

where ' +  @ListaConceptos
        set @Params  = N'@ListaConceptos varchar(255), @ListaConceptosRango varchar(255) OUTPUT'		
        exec sp_executesql @Query, @Params, @ListaConceptos, @ListaConceptosRango OUTPUT 
		set @ListaConceptos = @ListaConceptosRango 
	end
	
   if @RangoConceptos in ( 0 , 1 ) 
	 set @ListaConceptos = '' 

	while @iMes <= @MESFIN 
	begin 
	   exec RECALCULA_ACUMULADOS @ANIO, @EMPLEADO, @iMes, @STATUS, @lAlgunosConceptos, @ListaConceptos
		set @iMes = @iMes + 1;
	end

END

GO

CREATE PROCEDURE RECALCULA_AHORROS
    				@EMPLEADO INTEGER,
    				@STATUS SMALLINT,
					@AH_TIPO Codigo1 = ''
AS
        SET NOCOUNT ON
	DECLARE @CONCEPTO INTEGER;
	DECLARE @NUMERO INTEGER;
	DECLARE @TOTAL NUMERIC(15,2);
	DECLARE @TIPOAHORRO CHAR(1);
	DECLARE @FECHAINICIAL DATETIME;

	DECLARE TemporalAhorro CURSOR for
		select A.AH_TIPO, T.TB_CONCEPT, A.AH_FECHA
        from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO )
        where ( A.CB_CODIGO = @Empleado ) and ( A.AH_STATUS = 0 ) and ( A.AH_TIPO = @AH_TIPO or @AH_TIPO = '' )

	OPEN TemporalAhorro

	FETCH NEXT FROM TemporalAhorro
    into @TipoAhorro, @Concepto, @FechaInicial 
	WHILE @@FETCH_STATUS = 0
    begin
		select @Numero = COUNT(*), 
				@Total = SUM( M.MO_DEDUCCI )
        from MOVIMIEN M 
		left outer join NOMINA N on
               ( N.PE_YEAR = M.PE_YEAR ) and
               ( N.PE_TIPO = M.PE_TIPO ) and
               ( N.PE_NUMERO = M.PE_NUMERO ) and
               ( N.CB_CODIGO = @Empleado )
		left outer join PERIODO P on
               ( N.PE_YEAR = P.PE_YEAR ) and
               ( N.PE_TIPO = P.PE_TIPO ) and
               ( N.PE_NUMERO = P.PE_NUMERO )
        where ( M.CB_CODIGO = @Empleado ) and
               ( M.CO_NUMERO = @Concepto ) and
               ( M.MO_ACTIVO = 'S' ) and
               ( N.NO_STATUS = @Status ) and
               ( P.PE_FEC_FIN >= @FechaInicial )

        if ( @Numero = 0 ) 
			Select @Total = 0;

        update AHORRO set
                 AH_NUMERO = @Numero,
                 AH_TOTAL  = @Total,
                 AH_SALDO = AH_SALDO_I + @Total + AH_ABONOS - AH_CARGOS
        where ( CB_CODIGO = @Empleado ) and
                ( AH_TIPO = @TipoAhorro );
		
		FETCH NEXT FROM TemporalAhorro
    	into @TipoAhorro, @Concepto, @FechaInicial 
     end
     Close TemporalAhorro
     Deallocate TemporalAhorro
GO

CREATE PROCEDURE RECALCULA_PRESTAMOS
                      @EMPLEADO INTEGER,
                    @STATUS SMALLINT,
                    @PR_TIPO Codigo1 = ''
AS
        SET NOCOUNT ON
    DECLARE @CONCEPTO INTEGER;
    DECLARE @NUMERO INTEGER;
    DECLARE @TOTAL NUMERIC(15,2);
    DECLARE @TIPOPRESTAMO CHAR(1);
    DECLARE @REFERENCIA VARCHAR(8);
    DECLARE @FECHAINICIAL DATETIME;
	
    declare @UsaModalidadFonacotPrestamo Booleano 
	declare @TipoPrestamoFonacot Codigo1

	select @UsaModalidadFonacotPrestamo = LEFT( LTRIM(GL_FORMULA), 1)  from GLOBAL where GL_CODIGO = 328 
	set @UsaModalidadFonacotPrestamo = COALESCE( @UsaModalidadFonacotPrestamo, 'N' ) 

	select @TipoPrestamoFonacot = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	set @TipoPrestamoFonacot = COALESCE( @TipoPrestamoFonacot, '' )  

    if ( @UsaModalidadFonacotPrestamo = 'S' )
    begin
        DECLARE TemporalPrestamo Cursor for
        select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT, P.PR_FECHA
        from PRESTAMO P 
        left outer join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO )
        where ( P.CB_CODIGO = @Empleado ) and ( P.PR_STATUS in ( 0, 2 ) ) and ( P.PR_TIPO = @PR_TIPO or @PR_TIPO = '' ) and ( P.PR_TIPO NOT IN ( @TipoPrestamoFonacot ) )
    end;
    else
    begin
        DECLARE TemporalPrestamo Cursor for
        select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT, P.PR_FECHA
        from PRESTAMO P 
        left outer join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO )
        where ( P.CB_CODIGO = @Empleado ) and ( P.PR_STATUS in ( 0, 2 ) ) and ( P.PR_TIPO = @PR_TIPO or @PR_TIPO = '' )
    end;
    
    OPEN TemporalPrestamo
    FETCH NEXT FROM TemporalPrestamo
    into @TipoPrestamo, @Referencia, @Concepto, @FechaInicial 
    WHILE @@FETCH_STATUS = 0
    begin
        Select @Numero = 0, @Total = 0;
        select @Numero = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
        from MOVIMIEN M 
        left outer join NOMINA N on
               ( N.PE_YEAR = M.PE_YEAR ) and
               ( N.PE_TIPO = M.PE_TIPO ) and
               ( N.PE_NUMERO = M.PE_NUMERO ) and
               ( N.CB_CODIGO = @Empleado )
        left outer join PERIODO P on
               ( N.PE_YEAR = P.PE_YEAR ) and
               ( N.PE_TIPO = P.PE_TIPO ) and
               ( N.PE_NUMERO = P.PE_NUMERO )
        where ( M.CB_CODIGO = @Empleado ) and
                ( M.CO_NUMERO = @Concepto ) and
                ( M.MO_REFEREN = @Referencia ) and
                ( M.MO_ACTIVO = 'S' ) and
                ( N.NO_STATUS = @Status ) and
                ( P.PE_FEC_FIN >= @FechaInicial )
        if ( @Numero = 0 ) Select @Total = 0;
        update PRESTAMO  set
                 PR_NUMERO = @Numero,
                 PR_TOTAL  = @Total,
                 PR_SALDO = PR_MONTO - PR_SALDO_I - @Total - PR_ABONOS + PR_CARGOS
        where ( CB_CODIGO = @Empleado ) and
                ( PR_TIPO = @TipoPrestamo ) and
                ( PR_REFEREN = @Referencia );
        FETCH NEXT FROM TemporalPrestamo
        into @TipoPrestamo, @Referencia, @Concepto, @FechaInicial 
    end
    Close TemporalPrestamo
    Deallocate TemporalPrestamo
    EXECUTE SET_STATUS_PRESTAMOS @Empleado;
GO

create procedure DBO.SP_TOPE_PTU(
@ANIO SMALLINT,
@UTILIDADES NUMERIC( 15, 2 ),
@TOPESALARIO NUMERIC( 15, 2 ) )
as
begin
     SET NOCOUNT ON
     declare @Empleado Integer;
     declare @iEmpleados Integer;
     declare @nDias SmallInt;
     declare @nTotalDias Integer;
     declare @nMonto NUMERIC( 15, 2 );
     declare @nTotalMonto NUMERIC( 15, 2 );
     declare @nFactor NUMERIC( 15, 5 );
     set @nTotalDias = 0;
     set @nTotalMonto = 0;
     set @iEmpleados = 0;
     declare Temporal cursor for
        select R.CB_CODIGO, R.RU_DIAS, R.RU_MONTO from DBO.REP_PTU as R where ( R.RU_YEAR = @ANIO );
     open Temporal;
     fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
     while @@Fetch_Status = 0
     begin
          if ( @nMonto > @TOPESALARIO )
          begin
               set @nMonto = @TOPESALARIO;
               update DBO.REP_PTU set RU_MONTO = @TopeSalario where
               ( RU_YEAR = @ANIO ) and ( CB_CODIGO = @Empleado );
          end;
          set @nTotalMonto = @nTotalMonto + @nMonto;
          set @nTotalDias = @nTotalDias + @nDias;
          set @iEmpleados = @iEmpleados + 1;
          fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
     end;
     close Temporal;
     if ( @iEmpleados > 0 )
     begin
          set @nFactor = ( @UTILIDADES / 2.0 );
          open Temporal;
          fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
          while @@Fetch_Status = 0
          begin
               update DBO.REP_PTU set RU_M_DIAS = ROUND( @nDias * ( @nFactor / @nTotalDias ), 2 ),
                                      RU_M_MONTO = ROUND( @nMonto * ( @nFactor / @nTotalMonto ), 2 )
               where ( CB_CODIGO = @Empleado ) and ( RU_YEAR = @ANIO );
               fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
          end;
          close Temporal;
     end;
     deallocate Temporal;
end
GO

CREATE PROCEDURE SP_SUBTOTAL_ROTACION
	(@USUARIO SMALLINT,
	 @ESTEGRUPO CHAR(6),
	 @ESTADESCRIPCION VARCHAR(30),
	 @INICIO INTEGER,
	 @ALTAS INTEGER,
 	 @BAJAS INTEGER,
         @DIASACTIVOS NUMERIC(15,2),
	 @DIASRANGO NUMERIC(15,2),
	 @FECHAINI DATETIME)
AS
BEGIN
     SET NOCOUNT ON
     declare @Promedio NUMERIC(15,2);
     if ( @DiasRAngo = 0 )
     begin
          Set @Promedio = 0;
     end
     else
     begin
          Set @Promedio = @DiasActivos / @DiasRango;
     end
     insert into TMPROTA
     (TR_GRUPO, TR_DESCRIP, TR_INICIO, TR_FINAL, TR_ALTAS, TR_BAJAS, TR_PROM, TR_FECHA, TR_USER ) values
     (@EsteGrupo, @EstaDescripcion, @Inicio, @Inicio+@Altas-@Bajas, @Altas, @Bajas, @Promedio, @FECHAINI, @Usuario );
end
GO

CREATE PROCEDURE SP_TOTALES_ROTACION
	(@USUARIO USUARIO,
	 @FECHAINI FECHA,
	 @FECHAFIN FECHA)
AS
begin
       SET NOCOUNT ON
	DECLARE @ESTEGRUPO CHAR(6);
	DECLARE @ESTADESCRIPCION VARCHAR(30);
	DECLARE @INICIO INTEGER;
	DECLARE @ALTAS  INTEGER;
	DECLARE @BAJAS  INTEGER;
	DECLARE @DIASACTIVOS INTEGER;  
  	DECLARE @GRUPO CHAR(6);
  	DECLARE @DESCRIPCION VARCHAR(30);
  	DECLARE @FECING DATETIME;
  	DECLARE @FECBAJ DATETIME;
  	DECLARE @DIASRANGO INTEGER;
  	DECLARE @NUMGRUPOS INTEGER;	

 	DELETE FROM TMPROTA WHERE TR_USER = @Usuario;

        SET @NumGrupos = 0
	SET @DiasRango = DATEDIFF(day, @FechaIni, @FechaFin )+1
		

	Declare Temporal CURSOR for
 		select TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ
 		from   TMPROTAI
 		where  TR_USER = @Usuario
        	order by TR_GRUPO
	OPEN TEMPORAL
	fetch NEXT FROM Temporal 
	into   @Grupo, @Descripcion, @FecIng, @FecBaj 
		WHILE @@FETCH_STATUS = 0 
		BEGIN
    			if (( @NumGrupos = 0 ) or ( @EsteGrupo <> @Grupo ))
            		begin
               
               			if ( @NumGrupos > 0 ) 
                  		EXECUTE sp_SUBTOTAL_ROTACION 
                           		@Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                           		@Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

	               		SELECT	@NumGrupos = @NumGrupos + 1, @EsteGrupo = @Grupo,
	               			@EstaDescripcion = @Descripcion,
               				@Inicio = 0,
               				@Altas = 0,
               				@Bajas = 0,            
               				@DiasActivos = 0
            		end

            		if (( @FecIng >= @FechaIni ) and ( @FecIng <= @FechaFin ))
               			SET @Altas = @Altas + 1;

            		if (( @FecBaj >= @FechaIni ) and ( @FecBaj <= @FechaFin ))
               			SET @Bajas = @Bajas + 1;

            		if (( @FecIng < @FechaIni ) or (( @FecBaj < @FecIng ) and ( @FecBaj >= @FechaIni )))
            			SELECT @Inicio = @Inicio + 1, @FecIng = @FechaIni

            		if (( @FecBaj > @FechaFin ) or ( @FecBaj < @FecIng )) 
               			SET @FecBaj = @FechaFin;

            		SET @DiasActivos = DATEDIFF(day, @FecIng,@FecBaj) + 1 + @DiasActivos ;
			fetch NEXT FROM Temporal 
 			into   @Grupo, @Descripcion, @FecIng, @FecBaj 
  		END;

   	close Temporal
    	deallocate Temporal
        

        if ( @NumGrupos > 0 ) 
           EXECUTE sp_SUBTOTAL_ROTACION 
                   @Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                   @Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 
end
GO

CREATE PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);
   declare @kPLAZA    FolioGrande;
   declare @kNOMINA   SMALLINT;
   declare @kRECONTR  Booleano;
   declare @kFEC_COV  Fecha;

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare @ePLAZA    FolioGrande;
   declare @eFEC_PLA  Fecha;
   declare @eNOMINA   SMALLINT;
   declare @eFEC_NOM  Fecha;
   declare @eRECONTR  Booleano;
   declare @eFEC_COV  Fecha;
   declare @eFEC_PRE  Fecha;

   declare CursorKardex CURSOR SCROLL_LOCKS FOR
   select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
                CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
                CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
                CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
                CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
                CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
                CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S, CB_PLAZA,
                CB_NOMINA,  CB_RECONTR, CB_FEC_COV
   from Kardex
   where CB_CODIGO = @Empleado
   order by cb_fecha,cb_nivel

      	SET @FechaVacia = '12/30/1899';
        SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;
        SET @eFEC_PLA = @FechaVacia;
        SET @eFEC_NOM = @FechaVacia;
        SET @eRECONTR = 'S';
        SET @eFEC_COV = @FechaVacia;
        SET @eFEC_PRE = @FechaVacia;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
      @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
      @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
      @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
      @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
      @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
      @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
      @kNOMINA,  @kRECONTR, @kFEC_COV


	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END


	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
                             SET @kOLD_INT = @eOLD_INT;
	                     SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eFEC_PTO  = @kFECHA;
                        if ( @kTIPO = 'PLAZA' AND @eAUTOSAL = 'S' )
                             SET @kCLASIFI = @eCLASIFI;
                        else
			     SET @eCLASIFI  = @kCLASIFI;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) OR ( @kTIPO = 'PLAZA' ))
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                                if ( @kNIVEL1 <> '' )
                                    SET @kMOD_NV1 = 'S';
                                else
                                    SET @kMOD_NV1 = 'N';
                                if ( @kNIVEL2 <> '' )
                                    SET @kMOD_NV2 = 'S';
                                else
                                    SET @kMOD_NV2 = 'N';
                                if ( @kNIVEL3 <> '' )
                                    SET @kMOD_NV3 = 'S';
                                else
                                    SET @kMOD_NV3 = 'N';
                                if ( @kNIVEL4 <> '' )
                                    SET @kMOD_NV4 = 'S';
                                else
                                    SET @kMOD_NV4 = 'N';
                                if ( @kNIVEL5 <> '' )
                                    SET @kMOD_NV5 = 'S';
                                else
                                    SET @kMOD_NV5 = 'N';
                                if ( @kNIVEL6 <> '' )
                                    SET @kMOD_NV6 = 'S';
                                else
                                    SET @kMOD_NV6 = 'N';
                                if ( @kNIVEL7 <> '' )
                                    SET @kMOD_NV7 = 'S';
                                else
                                    SET @kMOD_NV7 = 'N';
                                if ( @kNIVEL8 <> '' )
                                    SET @kMOD_NV8 = 'S';
                                else
                                    SET @kMOD_NV8 = 'N';
                                if ( @kNIVEL9 <> '' )
                                    SET @kMOD_NV9 = 'S';
                                else
                                    SET @kMOD_NV9 = 'N';

			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
                        SET @kMOD_NV1 = 'N';
                        SET @kMOD_NV2 = 'N';
                        SET @kMOD_NV3 = 'N';
                        SET @kMOD_NV4 = 'N';
                        SET @kMOD_NV5 = 'N';
                        SET @kMOD_NV6 = 'N';
                        SET @kMOD_NV7 = 'N';
                        SET @kMOD_NV8 = 'N';
                        SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
    BEGIN
			SET @eTABLASS = @kTABLASS;
      SET @eFEC_PRE = @kFECHA;
    END
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TIPNOM' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eNOMINA  = @kNOMINA;
			SET @eFEC_NOM = @kFECHA;
		END
		else
			SET @kNOMINA  = @eNOMINA;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
			SET @eFEC_COV = @kFEC_COV;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
			SET @kFEC_COV = @eFEC_COV;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
			SET @ePLAZA    =  0;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END

		if ( @kTIPO = 'ALTA'  OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePLAZA    = @kPLAZA;
			SET @eFEC_PLA  = @kFECHA;
		END
		else
		BEGIN
			SET @kPLAZA    = @ePLAZA;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'BAJA' )
		BEGIN
			if ( @kTIPO = 'ALTA' )
                        begin
                             SET @kRECONTR = 'S';
                             SET @eRECONTR = 'S';
                        end
                        else
                        begin
                             SET @eRECONTR = @kRECONTR;
                        end
		END
		else
		BEGIN
			SET @kRECONTR = @eRECONTR;
		END

		update KARDEX
     		set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S,
      CB_PLAZA   = @kPLAZA,
      CB_NOMINA  = @kNOMINA,
      CB_RECONTR = @kRECONTR,
      CB_FEC_COV = @kFEC_COV
		where CURRENT OF CursorKardex

		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
                        @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
                        @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
                        @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
                        @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
                        @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
                        @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
                        @kNOMINA,  @kRECONTR, @kFEC_COV
	END

	Close CursorKardex
	Deallocate CursorKardex

	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_TUR;
	if ( @eFEC_PLA > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PLA;
	if ( @eFEC_NOM > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NOM;
	if ( @eFEC_PRE > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PRE;

	update COLABORA
	set CB_AUTOSAL = @eAUTOSAL,
            CB_ACTIVO  = @eACTIVO,
            CB_CLASIFI = @eCLASIFI,
            CB_CONTRAT = @eCONTRAT,
            CB_FAC_INT = @eFAC_INT,
            CB_FEC_ANT = @eFEC_ANT,
            CB_FEC_CON = @eFEC_CON,
            CB_FEC_ING = @eFEC_ING,
            CB_FEC_INT = @eFEC_INT,
            CB_FEC_REV = @eFEC_REV,
            CB_FEC_SAL = @eFEC_SAL,
            CB_FEC_BAJ = @eFEC_BAJ,
            CB_FEC_BSS = @eFEC_BSS,
            CB_MOT_BAJ = @eMOT_BAJ,
            CB_NIVEL1  = @eNIVEL1,
            CB_NIVEL2  = @eNIVEL2,
            CB_NIVEL3  = @eNIVEL3,
            CB_NIVEL4  = @eNIVEL4,
            CB_NIVEL5  = @eNIVEL5,
            CB_NIVEL6  = @eNIVEL6,
            CB_NIVEL7  = @eNIVEL7,
            CB_NIVEL8  = @eNIVEL8,
            CB_NIVEL9  = @eNIVEL9,
            CB_NOMNUME = @eNOMNUME,
            CB_NOMTIPO = @eNOMTIPO,
            CB_NOMYEAR = @eNOMYEAR,
            CB_PATRON  = @ePATRON,
            CB_PER_VAR = @ePER_VAR,
            CB_PUESTO  = @ePUESTO,
            CB_SAL_INT = @eSAL_INT,
            CB_SALARIO = @eSALARIO,
            CB_TABLASS = @eTABLASS,
            CB_TURNO   = @eTURNO,
            CB_ZONA_GE = @eZONA_GE,
            CB_OLD_SAL = @eOLD_SAL,
            CB_OLD_INT = @eOLD_INT,
            CB_PRE_INT = @ePRE_INT,
            CB_SAL_TOT = @eSAL_TOT,
            CB_TOT_GRA = @eTOT_GRA,
            CB_TIP_REV = @eTIP_REV,
            CB_RANGO_S = @eRANGO_S,
            CB_FEC_NIV = @eFEC_NIV,
            CB_FEC_PTO = @eFEC_PTO,
            CB_FEC_TUR = @eFEC_TUR,
            CB_FEC_KAR = @eFEC_KAR,
            CB_PLAZA   = @ePLAZA,
            CB_FEC_PLA = @eFEC_PLA,
            CB_NOMINA  = @eNOMINA,
            CB_FEC_NOM = @eFEC_NOM,
            CB_RECONTR = @eRECONTR,
            CB_FEC_COV = @eFEC_COV
	where ( CB_CODIGO = @Empleado );
END
GO

CREATE PROCEDURE DBO.SP_TITULO_CAMPO_DICCION( @Titulo VARCHAR(30), @TituloDefault VARCHAR(30), @Prefijo VARCHAR(30), @Campo VARCHAR(30) )
AS
BEGIN
     SET NOCOUNT ON;
     if ( ( @Titulo is NULL ) or ( @Titulo = '' ) )
        set @Titulo = @TituloDefault;
     set @Titulo = SUBSTRING ( @Prefijo + @Titulo , 1 , 30 ) ;

	 /*--------------------------------------------------------------------------------------*/
	 /*RDD*/
	 /*Cuando se descontinue el uso del DICCION este codigo desaparece*/
     update DICCION set DI_TITULO = @Titulo,
                        DI_TCORTO= @Titulo,
                        DI_CLAVES = ''
     where ( DI_CLASIFI in ( 5, 10, 18, 32, 33, 50, 59, 124, 141, 142, 143, 144, 145, 146, 147, 148, 149, 152, 186, 187, 191, 218, 271 ) )
     and ( DI_NOMBRE = @Campo );
	 /*--------------------------------------------------------------------------------------*/

	 if ( DBO.SP_EXISTE_TABLA('R_ATRIBUTO') = 1 )
	 begin
		update R_ATRIBUTO set 	AT_TITULO = @Titulo,
								AT_TCORTO = @Titulo,
								AT_CLAVES = @Titulo,
								AT_DESCRIP = @Titulo
		where AT_CAMPO = @Campo;
	end;
END
GO

CREATE PROCEDURE DBO.SP_TITULO_CAMPO_EXTRA( @TituloDefault VARCHAR(30), @Campo VARCHAR(10) )
AS
begin
     SET NOCOUNT ON;
     declare @Titulo Varchar(30);
     select @Titulo = CX_TITULO from CAMPO_AD where ( CX_NOMBRE = @Campo );
     if ( @Titulo is NOT NULL )
     begin
          execute SP_TITULO_CAMPO_DICCION @Titulo, @TituloDefault, '', @Campo
     end
end
GO

CREATE PROCEDURE DBO.SP_TITULO_CAMPO_EXTRA_GLOBAL( @TituloDefault VARCHAR(30), @Campo VARCHAR(10), @Global Integer )
AS
begin
     SET NOCOUNT ON;
     declare @Titulo Varchar(30);
     select @Titulo = CX_TITULO from CAMPO_AD where ( CX_NOMBRE = @Campo );
     if ( @Titulo is NOT NULL )
     begin
          execute SP_TITULO_CAMPO_DICCION @Titulo, @TituloDefault, '', @Campo;
          update GLOBAL set GL_FORMULA = @Titulo where ( GL_CODIGO = @Global );
     end
end
GO

CREATE  PROCEDURE DBO.SP_TITULO_CAMPO_PLUS( @NUMGLOBAL SMALLINT, @TITULODEFAULT VARCHAR(30), @PREFIJO VARCHAR(30), @NOMBRE VARCHAR(30) )
AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @TITULO VARCHAR(30);
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @NumGlobal );
     execute SP_TITULO_CAMPO_DICCION @Titulo, @TituloDefault, @Prefijo, @Nombre
END
GO

CREATE PROCEDURE DBO.SP_TITULO_CAMPO(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
BEGIN
     SET NOCOUNT ON;
     execute  SP_TITULO_CAMPO_PLUS @NumGlobal, @TituloDefault, '', @Nombre
END
GO

CREATE PROCEDURE DBO.SP_TITULO_TABLA_DICCION( @Titulo VARCHAR(30), @TituloDefault VARCHAR(30), @Tabla SMALLINT)
as
begin
     SET NOCOUNT ON;
     if ( ( @Titulo is NULL ) or ( @Titulo = '' ) )
         set @Titulo = @TituloDefault;

	 /*--------------------------------------------------------------------------------------*/
	 /*RDD*/
	 /*Cuando se descontinue el uso del DICCION este codigo desaparece*/
     update DICCION set DI_TITULO = @Titulo, DI_TCORTO = @Titulo, DI_CLAVES = ''
     where ( DI_CLASIFI = -1 ) and ( DI_CALC = @Tabla );
	 /*--------------------------------------------------------------------------------------*/

	 if ( DBO.SP_EXISTE_TABLA('R_ENTIDAD') = 1 )
		update R_ENTIDAD set EN_TITULO = @Titulo, EN_DESCRIP = @Titulo
		where ( EN_CODIGO = @Tabla );
end
GO

CREATE PROCEDURE DBO.SP_TITULO_TABLA_EXTRA( @TituloDefault VARCHAR(30), @Nombre VARCHAR(10) )
AS
begin
     SET NOCOUNT ON
     declare @Titulo Varchar(30);
     declare @Tabla SMALLINT;
     select @Titulo = CX_TITULO, @Tabla = CX_ENTIDAD from CAMPO_AD where ( CX_NOMBRE = @Nombre );
     if ( ( @Titulo is NOT NULL ) and ( @Tabla is NOT NULL ) )
     begin
          execute SP_TITULO_TABLA_DICCION @Titulo, @TituloDefault, @Tabla;
     end
end
GO

CREATE PROCEDURE DBO.SP_TITULO_TABLA( @NumGlobal SMALLINT, @TituloDefault VARCHAR(30), @Tabla SMALLINT)
as
begin
     SET NOCOUNT ON
      declare @Titulo VARCHAR(30);
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @NumGlobal );
     execute SP_TITULO_TABLA_DICCION @Titulo, @TituloDefault, @Tabla
end
GO

CREATE PROCEDURE DBO.SP_TITULO_CONTEO (
  @NUMGLOBAL SMALLINT,
  @TITULODEFAULT VARCHAR(30),
  @NOMBRE VARCHAR(10)
)  AS
begin
     SET NOCOUNT ON;
     declare @Formula Varchar(30);
     declare @Titulo Varchar(30);
     declare @GlobalNivel SmallInt;
     declare @LookUp SmallInt;

     set @Titulo = '';
     set @Lookup = 86;

     select @Formula = GL_FORMULA
     from GLOBAL
     where ( GL_CODIGO = @NumGlobal )

     if (( @Formula is Null ) or ( @Formula = '' ) or ( @Formula = '0' ))
     begin
          set @Titulo = @TituloDefault;
     end
     else
     begin
          if ( @Formula = '1' )
          begin
               set @Titulo = 'Puesto';
               set @Lookup = 59;
          end
          else if ( @Formula = '2' )
          begin
               set @Titulo = 'Turno';
               set @Lookup = 69;
          end
          else if ( @Formula = '3' )
          begin
               set @Titulo = 'Clasificaci�n';
               set @Lookup = 9;
          end
          else if ( @Formula = '4' )
          begin
               set @Titulo = 'Confidencialidad';
               set @Lookup = 157;
          end
          else
          begin
               if ( @Formula = '5' )
               begin
                    set @GlobalNivel = 13;
                    set @Lookup = 41;
               end
               else if ( @Formula = '6' )
               begin
                    set @GlobalNivel = 14;
                    set @Lookup = 42;
               end
               else if ( @Formula = '7' )
               begin
                    set @GlobalNivel = 15;
                    set @Lookup = 43;
               end
               else if ( @Formula = '8' )
               begin
                    set @GlobalNivel = 16;
                    set @Lookup = 44;
               end
               else if ( @Formula = '9' )
               begin
                    set @GlobalNivel = 17;
                    set @Lookup = 45;
               end
               else if ( @Formula = '10' )
               begin
                    set @GlobalNivel = 18;
                    set @Lookup = 46;
               end
               else if ( @Formula = '11' )
               begin
                    set @GlobalNivel = 19;
                    set @Lookup = 47;
               end
               else if ( @Formula = '12' )
               begin
                    set @GlobalNivel = 20;
                    set @Lookup = 48;
               end
               else if ( @Formula = '13' )
               begin
                    set @GlobalNivel = 21;
                    set @Lookup = 49;
               end
               else
               begin
                    set @Titulo = @TituloDefault;
               end

               if ( @Titulo = '' )
               begin
                    execute SP_TITULO_CAMPO @GlobalNivel, @TituloDefault, @NOMBRE ;

					/*--------------------------------------------------------------------------------------*/
					/*RDD*/
					/*Cuando se descontinue el uso del DICCION este codigo desaparece*/
                    update DICCION
                    set    DI_NUMERO = @LookUp
                    where ( DI_CLASIFI = 124 ) and
                          ( DI_NOMBRE = @NOMBRE );
					/*--------------------------------------------------------------------------------------*/

					if ( DBO.SP_EXISTE_TABLA('R_ATRIBUTO') = 1 )
					begin
						update R_ATRIBUTO set AT_ENTIDAD = @Lookup
						where EN_CODIGO = 124 and AT_CAMPO = @Nombre;
					end;
				end;
          end;
     end;

     if (@Titulo <> '')
     begin
		/*--------------------------------------------------------------------------------------*/
		/*RDD*/
		/*Cuando se descontinue el uso del DICCION este codigo desaparece*/
          update DICCION
          set    DI_TITULO = @Titulo,
                 DI_TCORTO = @Titulo,
                 DI_CLAVES = '',
                 DI_NUMERO = @Lookup
          where ( DI_CLASIFI = 124 ) and
                ( DI_NOMBRE = @NOMBRE );
		/*--------------------------------------------------------------------------------------*/

		if ( DBO.SP_EXISTE_TABLA('R_ATRIBUTO') = 1 )
		begin
			update R_ATRIBUTO set
				 AT_TITULO = @Titulo,
                 AT_TCORTO = @Titulo,
                 AT_CLAVES = '',
				 AT_ENTIDAD = @Lookup
			where EN_CODIGO = 124 and AT_CAMPO = @Nombre;
		end;
     end;
end
GO

CREATE PROCEDURE DBO.SP_TITULO_EXPEDIEN(
  @Nombre VarChar(10),
  @Titulo VarChar( 30)
) as
begin
     SET NOCOUNT ON
     declare @NombreEx Varchar(10);
     declare @TituloEx Varchar(30);


     declare TemporalDiccion CURSOR for
             select DI_NOMBRE from DICCION
             where DI_CLASIFI = 169
             and DI_NOMBRE like @Nombre+'%'
             order by DI_NOMBRE

     open TemporalDiccion;
     fetch NEXT from TemporalDiccion
     into @NombreEx

     while ( @@Fetch_Status = 0 )
     begin

          select @TituloEx = CX_TITULO from CAMPO_EX
          where CX_NOMBRE = @NombreEx

          if (( @TituloEx is Null ) or ( @TituloEx = '' ))
          begin
	       set @TituloEx = @Titulo + ' #' + SUBSTRING( @NombreEx , 9 , 2 );
          end
		/*--------------------------------------------------------------------------------------*/
		/*RDD*/
		/*Cuando se descontinue el uso del DICCION este codigo desaparece*/

          Update DICCION
          set DI_TITULO = @TituloEx,
              DI_TCORTO = @TituloEx,
              DI_CLAVES = @TituloEx
          where DI_CLASIFI = 169 /*Clasificacion de Expediente*/
          and DI_NOMBRE = @NombreEx;
		/*--------------------------------------------------------------------------------------*/

			if ( DBO.SP_EXISTE_TABLA('R_ATRIBUTO') = 1 )
			begin
				update R_ATRIBUTO set
					 AT_TITULO = @TituloEx,
	                 AT_TCORTO = @TituloEx,
					 AT_CLAVES = @TituloEx
				where EN_CODIGO = 169 and AT_CAMPO = @NombreEx;
			end;


          fetch NEXT from TemporalDiccion
          into @NombreEx
     end

     close TemporalDiccion;
     deallocate TemporalDiccion;
end
GO

CREATE PROCEDURE DBO.SP_TITULO_EXPEDIEN_BOL(
  @Nombre VarChar(10),
  @Titulo VarChar( 30)
) as
begin
     SET NOCOUNT ON
     declare @NombreEx Varchar(10);
     declare @TituloEx Varchar(30);


     EXECUTE SP_TITULO_EXPEDIEN @NOMBRE, @TITULO ;

     declare TemporalDiccion CURSOR for
             select DI_TITULO, DI_NOMBRE from DICCION
             where DI_CLASIFI = 169
             and DI_NOMBRE like @Nombre+'%'
             order by DI_NOMBRE;

     open TemporalDiccion;
     fetch NEXT from TemporalDiccion
     into @TituloEx, @NombreEx;

     while ( @@Fetch_Status = 0 )
     begin

	  select @TituloEx = 'D:'+ SUBSTRING( @TituloEx, 1, 28 );


	  set @NombreEx = 'EX_G_DES'+ SUBSTRING( @NombreEx , 9 , 2 );

		/*--------------------------------------------------------------------------------------*/
		/*RDD*/
		/*Cuando se descontinue el uso del DICCION este codigo desaparece*/
          Update DICCION
          set DI_TITULO = @TituloEx,
              DI_TCORTO = @TituloEx,
              DI_CLAVES = @TituloEx
          where DI_CLASIFI = 169 /*Clasificacion de Expediente*/
          and DI_NOMBRE = @NombreEx;
		/*--------------------------------------------------------------------------------------*/

			if ( DBO.SP_EXISTE_TABLA('R_ATRIBUTO') = 1 )
			begin
				update R_ATRIBUTO set
					 AT_TITULO = @TituloEx,
	                 AT_TCORTO = @TituloEx,
					 AT_CLAVES = @TituloEx
				where EN_CODIGO = 169 and AT_CAMPO = @NombreEx;
			end;


          fetch NEXT from TemporalDiccion
          into @TituloEx, @NombreEx
     end

     close TemporalDiccion;
     deallocate TemporalDiccion;
end
GO

CREATE PROCEDURE SP_REFRESH_ADICIONALES
as
begin
     SET NOCOUNT ON;
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 1', 'CB_G_TAB_1'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 2', 'CB_G_TAB_2'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 3', 'CB_G_TAB_3'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 4', 'CB_G_TAB_4'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 5', 'CB_G_TAB_5'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 6', 'CB_G_TAB_6'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 7', 'CB_G_TAB_7'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 8', 'CB_G_TAB_8'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional # 9', 'CB_G_TAB_9'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #10', 'CB_G_TAB10'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #11', 'CB_G_TAB11'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #12', 'CB_G_TAB12'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #13', 'CB_G_TAB13'
     execute SP_TITULO_TABLA_EXTRA 'Tabla Adicional #14', 'CB_G_TAB14'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 1', 'CB_G_TAB_1', 35
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 2', 'CB_G_TAB_2', 36
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 3', 'CB_G_TAB_3', 37
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Tabla Adicional # 4', 'CB_G_TAB_4', 38
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 5', 'CB_G_TAB_5'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 6', 'CB_G_TAB_6'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 7', 'CB_G_TAB_7'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 8', 'CB_G_TAB_8'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional # 9', 'CB_G_TAB_9'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #10', 'CB_G_TAB10'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #11', 'CB_G_TAB11'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #12', 'CB_G_TAB12'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #13', 'CB_G_TAB13'
     execute SP_TITULO_CAMPO_EXTRA 'Tabla Adicional #14', 'CB_G_TAB14'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 1', 'CB_G_FEC_1', 22
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 2', 'CB_G_FEC_2', 23
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Fecha Adicional # 3', 'CB_G_FEC_3', 24
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 4', 'CB_G_FEC_4'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 5', 'CB_G_FEC_5'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 6', 'CB_G_FEC_6'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 7', 'CB_G_FEC_7'
     execute SP_TITULO_CAMPO_EXTRA 'Fecha Adicional # 8', 'CB_G_FEC_8'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 1', 'CB_G_LOG_1', 32
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 2', 'CB_G_LOG_2', 33
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'L�gico Adicional # 3', 'CB_G_LOG_3', 34
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 4', 'CB_G_LOG_4'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 5', 'CB_G_LOG_5'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 6', 'CB_G_LOG_6'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 7', 'CB_G_LOG_7'
     execute SP_TITULO_CAMPO_EXTRA 'L�gico Adicional # 8', 'CB_G_LOG_8'

     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 1', 'CB_G_TEX_1', 25
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 2', 'CB_G_TEX_2', 26
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 3', 'CB_G_TEX_3', 27
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'Texto Adicional # 4', 'CB_G_TEX_4', 28
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 5', 'CB_G_TEX_5'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 6', 'CB_G_TEX_6'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 7', 'CB_G_TEX_7'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 8', 'CB_G_TEX_8'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional # 9', 'CB_G_TEX_9'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #10', 'CB_G_TEX10'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #11', 'CB_G_TEX11'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #12', 'CB_G_TEX12'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #13', 'CB_G_TEX13'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #14', 'CB_G_TEX14'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #15', 'CB_G_TEX15'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #16', 'CB_G_TEX16'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #17', 'CB_G_TEX17'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #18', 'CB_G_TEX18'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #19', 'CB_G_TEX19'
     execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #20', 'CB_G_TEX20'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #21', 'CB_G_TEX21'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #22', 'CB_G_TEX22'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #23', 'CB_G_TEX23'
	execute SP_TITULO_CAMPO_EXTRA 'Texto Adicional #24', 'CB_G_TEX24'


     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 1', 'CB_G_NUM_1', 29
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 2', 'CB_G_NUM_2', 30
     execute SP_TITULO_CAMPO_EXTRA_GLOBAL 'N�mero Adicional # 3', 'CB_G_NUM_3', 31
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 4', 'CB_G_NUM_4'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 5', 'CB_G_NUM_5'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 6', 'CB_G_NUM_6'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 7', 'CB_G_NUM_7'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 8', 'CB_G_NUM_8'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional # 9', 'CB_G_NUM_9'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #10', 'CB_G_NUM10'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #11', 'CB_G_NUM11'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #12', 'CB_G_NUM12'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #13', 'CB_G_NUM13'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #14', 'CB_G_NUM14'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #15', 'CB_G_NUM15'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #16', 'CB_G_NUM16'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #17', 'CB_G_NUM17'
     execute SP_TITULO_CAMPO_EXTRA 'N�mero Adicional #18', 'CB_G_NUM18'
end
GO

CREATE PROCEDURE DBO.SP_CUSTOM_DICCION as
GO

CREATE PROCEDURE DBO.SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 176, 'Lista de Defectos', 187
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PU_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PU_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PU_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PU_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PU_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PU_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PU_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PU_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PU_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PL_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PL_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PL_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PL_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PL_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PL_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PL_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PL_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PL_NIVEL9'

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER'
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO'
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CI_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CE_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CS_AREA'
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO'
  execute  SP_TITULO_CAMPO 176, 'Defecto','DE_CODIGO'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

  execute  SP_TITULO_EXPEDIEN_BOL  'EX_G_BOL', 'Si/No/Texto'
  execute  SP_TITULO_EXPEDIEN  'EX_G_LOG', 'Si/No'
  execute  SP_TITULO_EXPEDIEN  'EX_G_NUM', 'Numero'
  execute  SP_TITULO_EXPEDIEN  'EX_G_TEX', 'Texto'

  execute  SP_TITULO_CAMPO_PLUS 13, 'Nivel #1', 'Mod. ', 'CB_MOD_NV1'
  execute  SP_TITULO_CAMPO_PLUS 14, 'Nivel #2', 'Mod. ', 'CB_MOD_NV2'
  execute  SP_TITULO_CAMPO_PLUS 15, 'Nivel #3', 'Mod. ', 'CB_MOD_NV3'
  execute  SP_TITULO_CAMPO_PLUS 16, 'Nivel #4', 'Mod. ', 'CB_MOD_NV4'
  execute  SP_TITULO_CAMPO_PLUS 17, 'Nivel #5', 'Mod. ', 'CB_MOD_NV5'
  execute  SP_TITULO_CAMPO_PLUS 18, 'Nivel #6', 'Mod. ', 'CB_MOD_NV6'
  execute  SP_TITULO_CAMPO_PLUS 19, 'Nivel #7', 'Mod. ', 'CB_MOD_NV7'
  execute  SP_TITULO_CAMPO_PLUS 20, 'Nivel #8', 'Mod. ', 'CB_MOD_NV8'
  execute  SP_TITULO_CAMPO_PLUS 21, 'Nivel #9', 'Mod. ', 'CB_MOD_NV9'

  execute SP_REFRESH_ADICIONALES

  execute  SP_CUSTOM_DICCION

end
GO

CREATE PROCEDURE DBO.SP_ANCHO_EMPLEADO_REPORTES @Ancho SmallInt AS
begin
     SET NOCOUNT ON
     update CAMPOREP set CR_ANCHO = @Ancho where
     ( CR_TIPO = 0 ) and
     ( CR_FORMULA like '%CB_CODIGO' ) and
     ( CR_FORMULA not like '%@%' ) and
     ( CR_ANCHO < @Ancho );
end
GO

CREATE PROCEDURE DBO.SP_ANCHO_EMPLEADO @Ancho SmallInt AS
begin
     SET NOCOUNT ON
     update DICCION set DI_ANCHO = @Ancho
     where ( DI_NOMBRE = 'CB_CODIGO' )
end
GO

CREATE PROCEDURE SP_KARDEX_CLASIFICACION
    @EMPLEADO INTEGER,
    @FECHA DATETIME,
    @CB_AUTOSAL CHAR(1) OUTPUT,
    @CB_CLASIFI CHAR(6) OUTPUT,
    @CB_CONTRAT CHAR(1) OUTPUT,
    @CB_TURNO CHAR(6) OUTPUT,
    @CB_PUESTO CHAR(6) OUTPUT,
    @CB_TABLASS CHAR(1) OUTPUT,
    @CB_NIVEL1 CHAR(6) OUTPUT,
    @CB_NIVEL2 CHAR(6) OUTPUT,
    @CB_NIVEL3 CHAR(6) OUTPUT,
    @CB_NIVEL4 CHAR(6) OUTPUT,
    @CB_NIVEL5 CHAR(6) OUTPUT,
    @CB_NIVEL6 CHAR(6) OUTPUT,
    @CB_NIVEL7 CHAR(6) OUTPUT,
    @CB_NIVEL8 CHAR(6) OUTPUT,
    @CB_NIVEL9 CHAR(6) OUTPUT,
    @CB_SALARIO PesosDiario OUTPUT,
    @CB_NOMINA SmallInt OUTPUT
AS
  SET NOCOUNT ON
	declare @FEC_KAR DateTime
  declare @T_PUESTO Char(6)
	declare @T_CLASIFI Char(6)
	declare @T_TURNO Char(6)
  declare @T_NIVEL1 Char(6)
	declare @T_NIVEL2 Char(6)
	declare @T_NIVEL3 Char(6)
	declare @T_NIVEL4 Char(6)
	declare @T_NIVEL5 Char(6)
  declare @T_NIVEL6 Char(6)
	declare @T_NIVEL7 Char(6)
	declare @T_NIVEL8 Char(6)
	declare @T_NIVEL9 Char(6)
  declare @TB_SAL PesosDiario
  declare @SalTemp Varchar(1)

  declare Temporal CURSOR FOR
	select CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
         CB_TURNO, CB_PUESTO, CB_TABLASS,
         CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
         CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
         CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
			   CB_SALARIO, CB_NOMINA
 	from KARDEX where ( CB_CODIGO = @Empleado ) and
                    ( CB_FECHA <= @Fecha  )
 	order by CB_FECHA desc, CB_NIVEL desc

	open Temporal

	fetch next from Temporal
  into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
       @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
       @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
       @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
       @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9,
		   @CB_SALARIO, @CB_NOMINA

	close Temporal
	deallocate Temporal

	if ( @CB_PUESTO is null )
	begin
		declare Temporal2 CURSOR FOR
		select  CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
            CB_TURNO, CB_PUESTO, CB_TABLASS,
            CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
            CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
            CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
			      CB_SALARIO, CB_NOMINA
 		from KARDEX where ( CB_CODIGO = @Empleado )
 		order by CB_FECHA desc, CB_NIVEL desc

		open Temporal2

		fetch next from Temporal2
    into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
         @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
         @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
         @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
         @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9,
		     @CB_SALARIO, @CB_NOMINA

		close Temporal2
		deallocate Temporal2
	end

 	select @FEC_KAR = AU_FECHA,
	       @T_PUESTO = CB_PUESTO,
	       @T_CLASIFI = CB_CLASIFI,
	       @T_TURNO = CB_TURNO,
         @T_NIVEL1 = CB_NIVEL1,
	       @T_NIVEL2 = CB_NIVEL2,
	       @T_NIVEL3 = CB_NIVEL3,
	       @T_NIVEL4 = CB_NIVEL4,
	       @T_NIVEL5 = CB_NIVEL5,
         @T_NIVEL6 = CB_NIVEL6,
	       @T_NIVEL7 = CB_NIVEL7,
	       @T_NIVEL8 = CB_NIVEL8,
	       @T_NIVEL9 = CB_NIVEL9
  	from CLASITMP where
        ( CB_CODIGO = @Empleado ) and
        ( AU_FECHA = @Fecha ) and
        ( US_COD_OK <> 0 );
    if ( @FEC_KAR is not NULL )
    begin
         if ( @T_PUESTO <> '' )
         begin
             select @CB_PUESTO = @T_PUESTO;
         end
         if ( @T_CLASIFI <> '' )
         begin
              select @CB_CLASIFI = @T_CLASIFI;
              if ( @CB_AUTOSAL = 'S' )
              begin
                  select @SalTemp = GL_FORMULA from GLOBAL where GL_CODIGO = 304;
                  if ( @SalTemp = 'S' )
                  begin
                       select @TB_SAL = TB_SALARIO from CLASIFI where TB_CODIGO = @CB_CLASIFI
                       if ( @TB_SAL > @CB_SALARIO )
                       begin
                          select @CB_SALARIO = @TB_SAL
                       end
                  end
              end
         end
         if ( @T_TURNO <> '' )
         begin
              select @CB_TURNO = @T_TURNO;
         end
         if ( @T_NIVEL1 <> '' )
         begin
              select @CB_NIVEL1 = @T_NIVEL1;
         end
         if ( @T_NIVEL2 <> '' )
         begin
              select @CB_NIVEL2 = @T_NIVEL2;
         end
         if ( @T_NIVEL3 <> '' )
         begin
              select @CB_NIVEL3 = @T_NIVEL3;
         end
         if ( @T_NIVEL4 <> '' )
         begin
              select @CB_NIVEL4 = @T_NIVEL4;
         end
         if ( @T_NIVEL5 <> '' )
         begin
              select @CB_NIVEL5 = @T_NIVEL5;
         end
         if ( @T_NIVEL6 <> '' )
         begin
              select @CB_NIVEL6 = @T_NIVEL6;
         end
         if ( @T_NIVEL7 <> '' )
         begin
              select @CB_NIVEL7 = @T_NIVEL7;
         end
         if ( @T_NIVEL8 <> '' )
         begin
              select @CB_NIVEL8 = @T_NIVEL8;
         end
         if ( @T_NIVEL9 <> '' )
         begin
              select @CB_NIVEL9 = @T_NIVEL9;
         end
    end
GO

CREATE PROCEDURE AUTORIZACION_AGREGAR
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @CH_TIPO SMALLINT,
    @CH_DESCRIP SMALLINT,
    @CH_HOR_EXT NUMERIC(15,2),
    @CH_HOR_ORD NUMERIC(15,2),
    @CH_RELOJ CHAR(4),
    @CH_GLOBAL CHAR(1),
    @US_CODIGO SMALLINT
AS
     SET NOCOUNT ON
     delete from CHECADAS where
	( CB_CODIGO = @CB_CODIGO ) and
     	( AU_FECHA = @AU_FECHA ) and
     	( CH_TIPO = @CH_TIPO )
     if ( ( ( @CH_TIPO in (7,11,12) ) and ( @CH_HOR_EXT > 0 ) ) or ( @CH_HOR_ORD > 0 ) )
     begin
       	insert into CHECADAS( 	CB_CODIGO,
                               	AU_FECHA,
                               	CH_H_REAL,
                               	CH_H_AJUS,
                               	CH_SISTEMA,
                               	CH_GLOBAL,
                               	CH_IGNORAR,
	                        CH_RELOJ,
                                US_CODIGO,
              	                CH_TIPO,
                       	        CH_DESCRIP,
                               	CH_HOR_EXT,
	                        CH_HOR_ORD )
        	values ( @CB_CODIGO,
	        	 @AU_FECHA,
        		 '----',
		         '----',
	                 'N',
        	         @CH_GLOBAL,
                	 'S',
	                 @CH_RELOJ,
        	         @US_CODIGO,
                	 @CH_TIPO,
	                 @CH_DESCRIP,
        	         @CH_HOR_EXT,
                	 @CH_HOR_ORD );
     end


GO

CREATE   PROCEDURE AUTORIZACIONES_CREAR_CHECADAS 
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @AU_AUT_EXT NUMERIC(15,2),
    @M_AU_AUT_EXT CHAR(4),
    @AU_AUT_TRA NUMERIC(15,2),
    @M_AU_AUT_TRA CHAR(4),
    @AU_PER_CG NUMERIC(15,2),
    @M_AU_PER_CG CHAR(4),
    @AU_PCG_ENT NUMERIC(15,2),
    @M_AU_PCG_ENT CHAR(4),
    @AU_PER_SG NUMERIC(15,2),
    @M_AU_PER_SG CHAR(4),
    @AU_PSG_ENT NUMERIC(15,2),
    @M_AU_PSG_ENT CHAR(4),
    @AU_PRE_FUERA_JOR NUMERIC(15,2),
    @M_AU_PRE_FUERA_JOR CHAR(4),
    @AU_PRE_DENTRO_JOR NUMERIC(15,2),
    @M_AU_PRE_DENTRO_JOR CHAR(4),	
    @CH_GLOBAL CHAR(1),
    @US_CODIGO INTEGER
AS
        SET NOCOUNT ON
	DECLARE @HO_CODIGO CHAR(6)
	DECLARE @AU_HOR_MAN CHAR(1)
	DECLARE @AU_STATUS SMALLINT
	DECLARE @OLD_AU_AUT_EXT NUMERIC(15,2)
	DECLARE @OLD_M_AU_AUT_EXT CHAR(4)
	DECLARE @OLD_AU_AUT_TRA NUMERIC(15,2)
	DECLARE @OLD_M_AU_AUT_TRA CHAR(4)
	DECLARE @OLD_AU_PER_CG NUMERIC(15,2)
	DECLARE @OLD_M_AU_PER_CG CHAR(4)
	DECLARE @OLD_AU_PCG_ENT NUMERIC(15,2)
	DECLARE @OLD_M_AU_PCG_ENT CHAR(4)
	DECLARE @OLD_AU_PER_SG NUMERIC(15,2)
	DECLARE @OLD_M_AU_PER_SG CHAR(4)
	DECLARE @OLD_AU_PSG_ENT NUMERIC(15,2)
	DECLARE @OLD_M_AU_PSG_ENT CHAR(4)
	DECLARE @OLD_AU_PRE_FUERA_JOR NUMERIC(15,2)
	DECLARE @OLD_M_AU_PRE_FUERA_JOR CHAR(4)
	DECLARE @OLD_AU_PRE_DENTRO_JOR NUMERIC(15,2)
	DECLARE @OLD_M_AU_PRE_DENTRO_JOR CHAR(4)

	select @OLD_AU_AUT_EXT = Resultado, @OLD_M_AU_AUT_EXT = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 7 )
	select @OLD_AU_AUT_TRA = Resultado, @OLD_M_AU_AUT_TRA = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 8 )
	select @OLD_AU_PER_CG = Resultado, @OLD_M_AU_PER_CG = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 5 )
	select @OLD_AU_PCG_ENT = Resultado, @OLD_M_AU_PCG_ENT = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 9 )
	select @OLD_AU_PER_SG = Resultado, @OLD_M_AU_PER_SG = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 6 )
	select @OLD_AU_PSG_ENT = Resultado, @OLD_M_AU_PSG_ENT = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 10 )
	select @OLD_AU_PRE_FUERA_JOR = Resultado, @OLD_M_AU_PRE_FUERA_JOR = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 11 )
	select @OLD_AU_PRE_DENTRO_JOR = Resultado, @OLD_M_AU_PRE_DENTRO_JOR = Motivo
	from dbo.AUTORIZACIONES_SELECT( @CB_CODIGO, @AU_FECHA, 12 )
	

	if ( (@OLD_AU_AUT_EXT <> @AU_AUT_EXT) or (@OLD_M_AU_AUT_EXT <> @M_AU_AUT_EXT) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 7, 4, @AU_AUT_EXT,
			0, @M_AU_AUT_EXT, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_AUT_TRA <> @AU_AUT_TRA) or (@OLD_M_AU_AUT_TRA <> @M_AU_AUT_TRA) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 8, 8, 0,
			@AU_AUT_TRA, @M_AU_AUT_TRA, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PER_CG <> @AU_PER_CG) or (@OLD_M_AU_PER_CG <> @M_AU_PER_CG) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 5, 1, 0,
			@AU_PER_CG, @M_AU_PER_CG, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PCG_ENT <> @AU_PCG_ENT) or (@OLD_M_AU_PCG_ENT <> @M_AU_PCG_ENT) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 9, 9, 0,
			@AU_PCG_ENT, @M_AU_PCG_ENT, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PER_SG <> @AU_PER_SG) or (@OLD_M_AU_PER_SG <> @M_AU_PER_SG) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 6, 1, 0,
			@AU_PER_SG, @M_AU_PER_SG, @CH_GLOBAL, @US_CODIGO
     	end
	if ( (@OLD_AU_PSG_ENT <> @AU_PSG_ENT) or (@OLD_M_AU_PSG_ENT <> @M_AU_PSG_ENT) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 10, 9, 0,
			@AU_PSG_ENT, @M_AU_PSG_ENT, @CH_GLOBAL, @US_CODIGO
     	end

	if ( (@OLD_AU_PRE_FUERA_JOR <> @AU_PRE_FUERA_JOR) or (@OLD_M_AU_PRE_FUERA_JOR <> @M_AU_PRE_FUERA_JOR) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 11, 4, @AU_PRE_FUERA_JOR,
			0, @M_AU_PRE_FUERA_JOR, @CH_GLOBAL, @US_CODIGO

     	end
	if ( (@OLD_AU_PRE_DENTRO_JOR <> @AU_PRE_DENTRO_JOR) or (@OLD_M_AU_PRE_DENTRO_JOR <> @M_AU_PRE_DENTRO_JOR) ) 
	begin
          	EXECUTE AUTORIZACION_AGREGAR @CB_CODIGO, @AU_FECHA, 12, 4, @AU_PRE_DENTRO_JOR,
			0, @M_AU_PRE_DENTRO_JOR, @CH_GLOBAL, @US_CODIGO
     	end
GO

create procedure DBO.IMSS_CALCULA_RECARGOS(
@PATRON CHAR( 1 ),
@ANIO SMALLINT,
@MES SMALLINT,
@TIPO SMALLINT,
@FACTOR NUMERIC( 15, 5 ),
@TASA NUMERIC( 15, 5 ) )
as
begin
     SET NOCOUNT ON
     declare @rTotImss Numeric( 15, 2 );
     declare @rTotRetiro Numeric( 15, 2 );
     declare @rTotInfo Numeric( 15, 2 );
     declare @rTotAmort Numeric( 15, 2 );
     declare @rTotApoVol Numeric( 15, 2 );
     declare @rContraFactor Numeric( 15, 9 );
     declare @rContraTasa Numeric( 15, 9 );
     declare @rActIMSS Numeric( 15, 2 );
     declare @rRecIMSS Numeric( 15, 2 );
     declare @rActRet Numeric( 15, 2 );
     declare @rRecRet Numeric( 15, 2 );
     declare @rActINFO Numeric( 15, 2 );
     declare @rRecINFO Numeric( 15, 2 );
     declare @Cuantos Integer;
     select @rTotImss = SUM( L.LE_TOT_IMS ),
            @rTotRetiro = SUM( L.LE_TOT_RET - L.LE_APO_VOL ),
            @rTotInfo = SUM( L.LE_INF_PAT ),
            @rTotAmort = SUM( L.LE_INF_AMO ),
            @rTotApoVol = SUM( L.LE_APO_VOL ),
            @Cuantos = COUNT(*)
     from DBO.LIQ_EMP as L where ( L.LS_PATRON = @PATRON ) and
                                 ( L.LS_YEAR = @ANIO ) and
                                 ( L.LS_MONTH = @MES ) and
                                 ( L.LS_TIPO = @TIPO );
     set @rContraFactor = ( @FACTOR - 1 );
     set @rContraTasa = @FACTOR * ( @TASA / 100 );
     if ( @Cuantos > 0 )
     begin
          set @rActIMSS = @rTotIMSS * @rContraFactor;
          set @rRecIMSS = @rTotIMSS * @rContraTasa;
          set @rActRET = @rTotRetiro * @rContraFactor;
          set @rRecRET = @rTotRetiro * @rContraTasa;
          set @rTotInfo = @rTotInfo + @rTotAmort;
          set @rActINFO = @rTotInfo * @rContraFactor;
          set @rRecINFO = @rTotInfo * @rContraTasa;
     end
     else
     begin
          set @rActIMSS = 0;
          set @rRecIMSS = 0;
          set @rActRET = 0;
          set @rRecRET = 0;
          set @rTotInfo = 0;
          set @rActINFO = 0;
          set @rRecINFO = 0;
     end
     set @rTotIMSS = @rActIMSS + @rRecIMSS;
     set @rTotRetiro = @rActRET + @rRecRET;
     set @rTotINFO = @rActINFO + @rRecINFO;
     update DBO.LIQ_IMSS set LS_ACT_IMS = @rActIMSS,
                             LS_REC_IMS = @rRecIMSS,
                             LS_ACT_RET = @rActRet,
                             LS_REC_RET = @rRecRet,
                             LS_ACT_INF = @rActINFO,
                             LS_REC_INF = @rRecINFO,
                             LS_ACT_AMO = @rTotAmort * @rContraFactor,
                             LS_REC_AMO = @rTotAmort * @rContraTasa,
                             LS_ACT_APO = @rTotApoVol * @rContraFactor,
                             LS_REC_APO = @rTotApoVol * @rContraTasa,
                             LS_FAC_ACT = @Factor,
                             LS_FAC_REC = @Tasa,
                             LS_TOT_IMS = LS_SUB_IMS + @rTotIMSS,
                             LS_TOT_RET = LS_SUB_RET + LS_APO_VOL + @rTotRetiro,
                             LS_TOT_INF = LS_SUB_INF + @rTotINFO,
                             LS_TOT_MES = LS_SUB_IMS + @rTotIMSS + LS_SUB_RET + LS_APO_VOL + @rTotRetiro +
                                          LS_SUB_INF + @rTotINFO
     where ( LS_PATRON = @PATRON ) and
           ( LS_YEAR = @ANIO ) and
           ( LS_MONTH = @MES ) and
           ( LS_TIPO = @TIPO );
end
GO

CREATE PROCEDURE GET_COMENTA_TKARDEX(
	@Tipo Char(6),
	@Comentario VarChar(50) OUTPUT,
	@Nivel      SmallInt    OUTPUT )
AS
BEGIN
  SET NOCOUNT ON
  declare @sComenta VarChar(50);

  select @Nivel = TB_NIVEL, @sComenta = TB_ELEMENT
  from TKARDEX
  where ( TB_CODIGO = @Tipo )

  if ( @Nivel IS NULL )
  BEGIN
	SET @Nivel = 5;
	SET @Comentario = '';
	RETURN( 1 )
  END

  if @Comentario = ''
	SET @Comentario = @sComenta
  RETURN( 0 )
END
GO

CREATE PROCEDURE KARDEX_TASA_INFONAVIT( 
	@CB_CODIGO  Integer,
	@CB_FECHA   Fecha,
	@CB_MONTO   Numeric( 15, 2 ),
	@CB_COMENTA VarChar( 50 ),
	@CB_FEC_CAP Fecha,
	@CB_GLOBAL  Char( 1 ),
	@US_CODIGO  SmallInt,
	@Resultado  SmallInt OUTPUT,
	@FechaIngreso Fecha  OUTPUT )
AS
BEGIN
  SET NOCOUNT ON
  declare @iNivel SmallInt;
  declare @sComenta VarChar( 50 );
  declare @sTipoKardex Char( 6 );

  SET @sTipoKardex = 'TASINF';

  select @FechaIngreso = CB_FEC_ING
  from   COLABORA
  where  CB_CODIGO = @CB_CODIGO
  

  SET @sComenta = @CB_COMENTA
  EXEC @Resultado = GET_COMENTA_TKARDEX @sTipoKardex, @sComenta OUTPUT, @iNivel OUTPUT

  delete from KARDEX
  where ( CB_CODIGO = @CB_CODIGO ) and
        ( CB_FECHA = @CB_FECHA ) and
        ( CB_TIPO = @sTipoKardex );

  insert into KARDEX ( CB_CODIGO,
                       CB_FECHA,
                       CB_TIPO,
                       CB_NIVEL,
                       CB_MONTO,
                       CB_COMENTA,
                       CB_FEC_CAP,
                       CB_GLOBAL,
                       US_CODIGO )
   values ( @CB_CODIGO,
                   @CB_FECHA,
                   @sTipoKardex,
                   @iNivel,
                   @CB_MONTO,
                   @sComenta,
                   @CB_FEC_CAP,
                   @CB_GLOBAL,
                   @US_CODIGO );


    EXECUTE RECALCULA_KARDEX @CB_CODIGO
END
GO

CREATE PROCEDURE ACTUALIZA_TASA_INFONAVIT(
	@Empleado Integer,
	@Fecha Fecha,
	@FechaCaptura Fecha,
	@Usuario SmallInt,
	@TasaNueva NUMERIC(15, 2) OUTPUT )
AS
BEGIN
  SET NOCOUNT ON
  declare @SalarioIntegrado Numeric( 15, 2);
  declare @SalarioMinimo Numeric( 15, 2 );
  declare @FechaSalarioMinimo Fecha;
  declare @TasaIndex Numeric( 15, 2 );
  declare @ZonaGeografica Char( 1 );
  declare @Resultado SmallInt;
  declare @FechaIngreso Fecha;

  SET @TasaNueva = 0;

  select @TasaIndex = CB_INF_OLD, @ZonaGeografica = CB_ZONA_GE
  from   COLABORA
  where ( CB_CODIGO = @Empleado );

  select @FechaSalarioMinimo = MAX( SM_FEC_INI )
  from SAL_MIN
  where ( SM_FEC_INI <= @Fecha )


  if ( @ZonaGeografica = 'A' )
          select @SalarioMinimo = SM_ZONA_A 
          from SAL_MIN
          where ( SM_FEC_INI = @FechaSalarioMinimo )
  else if ( @ZonaGeografica = 'B' )
          select @SalarioMinimo = SM_ZONA_B
          from SAL_MIN
          where ( SM_FEC_INI = @FechaSalarioMinimo )
  else if ( @ZonaGeografica = 'C' )
          select @SalarioMinimo = SM_ZONA_C
          from SAL_MIN
          where ( SM_FEC_INI = @FechaSalarioMinimo )
  else
	  SET @SalarioMinimo = 0;

  if ( @SalarioMinimo > 0 )
  BEGIN
    select @SalarioIntegrado = CB_SAL_INT
    from   dbo.SP_FECHA_KARDEX( @Fecha, @Empleado );

    SET @SalarioIntegrado = @SalarioIntegrado / @SalarioMinimo;

    select @TasaNueva = TASA
    from   dbo.SP_A80_ESCALON( @TasaIndex, @SalarioIntegrado, @Fecha );

    update COLABORA 
    set CB_INFTASA = @TasaNueva
    where ( CB_CODIGO = @Empleado );

    EXECUTE KARDEX_TASA_INFONAVIT @Empleado, @Fecha, @TasaNueva, '',
                                  @FechaCaptura, 'S', @Usuario,
                                  @Resultado OUTPUT, @FechaIngreso OUTPUT

    if @Resultado <> 0
	SET @TasaNueva = 0
  END
END
GO 

CREATE PROCEDURE ASISTENCIA_AGREGAR 
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @CH_REAL CHAR(4),
    @CH_GLOBAL CHAR(1),
    @US_CODIGO SMALLINT,
	@CH_MOTIVO CHAR(4)
AS
     SET NOCOUNT ON
     insert into CHECADAS( CB_CODIGO,
                           AU_FECHA,
                           CH_H_REAL,
                           CH_H_AJUS,
                           CH_SISTEMA,
                           CH_GLOBAL,
                           CH_RELOJ,
                           US_CODIGO,
                           CH_MOTIVO )
	values ( @CB_CODIGO,
                 @AU_FECHA,
                 @CH_REAL,
                 @CH_REAL,
                 'N',
                 @CH_GLOBAL,
                 '',
                 @US_CODIGO,
				        @CH_MOTIVO )
GO

CREATE PROCEDURE ASISTENCIA_CREAR_CHECADAS
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @CH_GLOBAL CHAR(1),
    @US_CODIGO SMALLINT

AS
        SET NOCOUNT ON
	DECLARE @OLD_CHECADA1 CHAR(4)
	DECLARE @OLD_CHECADA2 CHAR(4)
	DECLARE @OLD_CHECADA3 CHAR(4)
	DECLARE @OLD_CHECADA4 CHAR(4)
  DECLARE @OLD_MC_CHECADA1 CHAR(4)
	DECLARE @OLD_MC_CHECADA2 CHAR(4)
	DECLARE @OLD_MC_CHECADA3 CHAR(4)
	DECLARE @OLD_MC_CHECADA4 CHAR(4)


     	select @OLD_CHECADA1 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 1 )
     	select @OLD_CHECADA2 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 2 )
     	select @OLD_CHECADA3 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 3 )
     	select @OLD_CHECADA4 = dbo.SP_CHECADAS( @AU_FECHA, @CB_CODIGO, 4 )

      select @OLD_MC_CHECADA1 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 1 )
      select @OLD_MC_CHECADA2 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 2 )
      select @OLD_MC_CHECADA3 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 3 )
      select @OLD_MC_CHECADA4 = dbo.SP_CHECADA_MOT( @AU_FECHA, @CB_CODIGO, 3 )

	if ( @OLD_CHECADA1 is not null ) and ( ( @CHECADA1 <> @OLD_CHECADA1 ) or
                                         ( @MC_CHECADA1 <> @OLD_MC_CHECADA1 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA1 )
     	end
	if ( @OLD_CHECADA2 is not null ) and ( ( @CHECADA2 <> @OLD_CHECADA2 ) or
                                         ( @MC_CHECADA2 <> @OLD_MC_CHECADA2 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA2 )
     	end
	if ( @OLD_CHECADA3 is not null ) and ( ( @CHECADA3 <> @OLD_CHECADA3 ) or
                                         ( @MC_CHECADA3 <> @OLD_MC_CHECADA3 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA3 )
     	end
	if ( @OLD_CHECADA4 is not null ) and ( ( @CHECADA4 <> @OLD_CHECADA4 ) or
                                         ( @MC_CHECADA4 <> @OLD_MC_CHECADA4 ) )
     	begin
          	delete from CHECADAS where
          	( CB_CODIGO = @CB_CODIGO ) and
          	( AU_FECHA = @AU_FECHA ) and
          	( CH_H_REAL = @OLD_CHECADA4 )
     	end

	if ( ( @OLD_CHECADA1 is null or ( ( @CHECADA1 <> @OLD_CHECADA1 ) or ( @MC_CHECADA1 <> @OLD_MC_CHECADA1 ) )  ) and
                                  ( @CHECADA1 <> '' and @CHECADA1 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA1, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA1
     	end
	if ( ( @OLD_CHECADA2 is null or ( ( @CHECADA2 <> @OLD_CHECADA2 ) or ( @MC_CHECADA2 <> @OLD_MC_CHECADA2 ) ) ) and
                                  ( @CHECADA2 <> '' and @CHECADA2 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA2, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA2
     	end
	if ( ( @OLD_CHECADA3 is null or ( ( @CHECADA3 <> @OLD_CHECADA3 ) or ( @MC_CHECADA3 <> @OLD_MC_CHECADA3 )  ) ) and
                                  ( @CHECADA3 <> '' and @CHECADA3 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA3, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA3
     	end
	if ( ( @OLD_CHECADA4 is null or ( ( @CHECADA4 <> @OLD_CHECADA4 ) or ( @MC_CHECADA4 <> @OLD_MC_CHECADA4 ) ) ) and
                                  ( @CHECADA4 <> '' and @CHECADA4 <> '0000' ) )
	begin
		EXECUTE ASISTENCIA_AGREGAR @CB_CODIGO, @AU_FECHA, @CHECADA4, @CH_GLOBAL, @US_CODIGO,@MC_CHECADA4
     	end
GO

CREATE PROCEDURE ASISTENCIA_AGREGA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)
	DECLARE @CB_SALARIO PesosDiario
	DECLARE @CB_NOMINA SmallInt

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT,
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT,
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT,
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT, @CB_SALARIO OUTPUT,
		@CB_NOMINA OUTPUT

	insert into AUSENCIA
		( CB_CODIGO, AU_FECHA, AU_STATUS, HO_CODIGO, AU_HOR_MAN, AU_OUT2EAT,

		CB_PUESTO, CB_CLASIFI, CB_TURNO, CB_NIVEL1, CB_NIVEL2,
		CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7,
		CB_NIVEL8, CB_NIVEL9, CB_SALARIO, CB_NOMINA, CB_TABLASS )
	values	( @CB_CODIGO, @AU_FECHA, @AU_STATUS, @HO_CODIGO, @AU_HOR_MAN, @AU_OUT2EAT,
		@CB_PUESTO, @CB_CLASIFI, @CB_TURNO, @CB_NIVEL1, @CB_NIVEL2,
		@CB_NIVEL3, @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6, @CB_NIVEL7,
		@CB_NIVEL8, @CB_NIVEL9, @CB_SALARIO, @CB_NOMINA, @CB_TABLASS )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, @MC_CHECADA1, @MC_CHECADA2, @MC_CHECADA3,
    @MC_CHECADA4, 'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
		@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',
		@US_CODIGO
END
GO

CREATE PROCEDURE ASISTENCIA_MODIFICA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)
	DECLARE @CB_SALARIO PesosDiario
	DECLARE @CB_NOMINA SmallInt

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT,
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT,
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT,
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT, @CB_SALARIO OUTPUT,
		@CB_NOMINA OUTPUT

	update AUSENCIA set HO_CODIGO = @HO_CODIGO,
           AU_STATUS = @AU_STATUS,
		   AU_HOR_MAN = @AU_HOR_MAN,
           AU_OUT2EAT = @AU_OUT2EAT,
           CB_PUESTO = @CB_PUESTO,
		   CB_CLASIFI = @CB_CLASIFI,
           CB_TURNO = @CB_TURNO,
		   CB_NIVEL1 = @CB_NIVEL1,
           CB_NIVEL2 = @CB_NIVEL2,
		   CB_NIVEL3 = @CB_NIVEL3,
           CB_NIVEL4 = @CB_NIVEL4,
		   CB_NIVEL5 = @CB_NIVEL5,
           CB_NIVEL6 = @CB_NIVEL6,
		   CB_NIVEL7 = @CB_NIVEL7,
           CB_NIVEL8 = @CB_NIVEL8,
		   CB_NIVEL9 = @CB_NIVEL9,
		 CB_SALARIO = @CB_SALARIO,
		 CB_NOMINA = @CB_NOMINA,
     CB_TABLASS = @CB_TABLASS
	where ( CB_CODIGO = @CB_CODIGO ) and ( AU_FECHA = @AU_FECHA )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, @MC_CHECADA1, @MC_CHECADA2, @MC_CHECADA3,
    @MC_CHECADA4,'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
	 	@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',@US_CODIGO
END
GO

CREATE procedure SP_COPIA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER )
as
begin
     SET NOCOUNT ON
     declare @Status SmallInt;
     select @Status = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @Status is not Null )
     begin
          if ( @Status > 4 )
          begin
               set @Status = 4;
          end;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,
								  CB_BAN_ELE,
								  NO_HORAPDT,
							      NO_HORASNT,
								  NO_DIAS_SI,
								  NO_DIAS_VJ,
							      NO_DIAS_PV,
								  NO_SUP_OK,
								  NO_FEC_OK,
								  NO_HOR_OK,
								  NO_ASI_INI,
								  NO_ASI_FIN,
								  NO_DIAS_BA,
								  NO_PREV_GR,
								  CB_PLAZA,
								  NO_FEC_LIQ,
								  CB_CTA_GAS,
								  CB_CTA_VAL,
								  NO_PER_ISN,
                  NO_DIAS_IH,
                  NO_DIAS_ID,
                  NO_DIAS_IT,
                  NO_TOTAL_1,
                  NO_TOTAL_2,
                  NO_TOTAL_3,
                  NO_TOTAL_4,
                  NO_TOTAL_5,
                  NO_FACT_BA,
                  NO_DIAS_DT,
                  NO_DIAS_FS,
                  NO_DIAS_FT,
				  CB_TABLASS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N2.CB_CODIGO,
                 N2.CB_CLASIFI,
                 N2.CB_TURNO,
                 N2.CB_PATRON,
                 N2.CB_PUESTO,
                 N2.CB_ZONA_GE,
                 N2.NO_FOLIO_1,
                 N2.NO_FOLIO_2,
                 N2.NO_FOLIO_3,
                 N2.NO_FOLIO_4,
                 N2.NO_FOLIO_5,
                 N2.NO_OBSERVA,
                 @Status,
                 N2.US_CODIGO,
                 N2.NO_USER_RJ,
                 N2.CB_NIVEL1,
                 N2.CB_NIVEL2,
                 N2.CB_NIVEL3,
                 N2.CB_NIVEL4,
                 N2.CB_NIVEL5,
                 N2.CB_NIVEL6,
                 N2.CB_NIVEL7,
                 N2.CB_NIVEL8,
                 N2.CB_NIVEL9,
                 N2.CB_SAL_INT,
                 N2.CB_SALARIO,
                 N2.NO_DIAS,
                 N2.NO_ADICION,
                 N2.NO_DEDUCCI,
                 N2.NO_NETO,
                 N2.NO_DES_TRA,
                 N2.NO_DIAS_AG,
                 N2.NO_DIAS_VA,
                 N2.NO_DOBLES,
                 N2.NO_EXTRAS,
                 N2.NO_FES_PAG,
                 N2.NO_FES_TRA,
                 N2.NO_HORA_CG,
                 N2.NO_HORA_SG,
                 N2.NO_HORAS,
                 N2.NO_IMP_CAL,
                 N2.NO_JORNADA,
                 N2.NO_PER_CAL,
                 N2.NO_PER_MEN,
                 N2.NO_PERCEPC,
                 N2.NO_TARDES,
                 N2.NO_TRIPLES,
                 N2.NO_TOT_PRE,
                 N2.NO_VAC_TRA,
                 N2.NO_X_CAL,
                 N2.NO_X_ISPT,
                 N2.NO_X_MENS,
                 N2.NO_D_TURNO,
                 N2.NO_DIAS_AJ,
                 N2.NO_DIAS_AS,
                 N2.NO_DIAS_CG,
                 N2.NO_DIAS_EM,
                 N2.NO_DIAS_FI,
                 N2.NO_DIAS_FJ,
                 N2.NO_DIAS_FV,
                 N2.NO_DIAS_IN,
                 N2.NO_DIAS_NT,
                 N2.NO_DIAS_OT,
                 N2.NO_DIAS_RE,
                 N2.NO_DIAS_SG,
                 N2.NO_DIAS_SS,
                 N2.NO_DIAS_SU,
                 N2.NO_HORA_PD,
                 N2.NO_LIQUIDA,
                 N2.NO_FUERA,
                 N2.NO_EXENTAS,
                 N2.NO_FEC_PAG,
                 N2.NO_USR_PAG,
				 N2.CB_NIVEL0,
				 N2.CB_BAN_ELE,
				 N2.NO_HORAPDT,
				 N2.NO_HORASNT,
				 N2.NO_DIAS_SI,
				 N2.NO_DIAS_VJ,
				 N2.NO_DIAS_PV,
				 N2.NO_SUP_OK,
				 N2.NO_FEC_OK,
				 N2.NO_HOR_OK,
				 N2.NO_ASI_INI,
				 N2.NO_ASI_FIN,
				 N2.NO_DIAS_BA,
				 N2.NO_PREV_GR,
				 N2.CB_PLAZA,
				 N2.NO_FEC_LIQ,
				 N2.CB_CTA_GAS,
				 N2.CB_CTA_VAL,
				 N2.NO_PER_ISN,
         N2.NO_DIAS_IH,
         N2.NO_DIAS_ID,
         N2.NO_DIAS_IT,
         N2.NO_TOTAL_1,
         N2.NO_TOTAL_2,
         N2.NO_TOTAL_3,
         N2.NO_TOTAL_4,
         N2.NO_TOTAL_5,
         N2.NO_FACT_BA,
         N2.NO_DIAS_DT,
         N2.NO_DIAS_FS,
         N2.NO_DIAS_FT,
		 N2.CB_TABLASS
          from DBO.NOMINA as N2 where ( N2.PE_YEAR = @YEARORIGINAL ) and
                                      ( N2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( N2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( N2.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M2.CB_CODIGO,
                 M2.MO_ACTIVO,
                 M2.CO_NUMERO,
                 M2.MO_IMP_CAL,
                 M2.US_CODIGO,
                 M2.MO_PER_CAL,
                 M2.MO_REFEREN,
                 M2.MO_X_ISPT,
                 M2.MO_PERCEPC,
                 M2.MO_DEDUCCI
          from DBO.MOVIMIEN as M2 where ( M2.PE_YEAR = @YEARORIGINAL ) and
                                        ( M2.PE_TIPO = @TIPOORIGINAL ) and
                                        ( M2.PE_NUMERO = @NUMEROORIGINAL ) and
                                        ( M2.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  FA_FEC_INI,
                                  FA_DIA_HOR,
                                  FA_MOTIVO,
                                  FA_DIAS,
                                  FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F2.CB_CODIGO,
                 F2.FA_FEC_INI,
                 F2.FA_DIA_HOR,
                 F2.FA_MOTIVO,
                 F2.FA_DIAS,
                 F2.FA_HORAS
          from DBO.FALTAS as F2 where ( F2.PE_YEAR = @YEARORIGINAL ) and
                                      ( F2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( F2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( F2.CB_CODIGO = @EMPLEADO );
     end;
end
GO

CREATE PROCEDURE SP_ADD_AUSENCIA
    				@EMPLEADO INTEGER,
    				@FECHA DATETIME,
    				@HORARIO CHAR(6),
    				@STATUS SMALLINT,
    				@CB_CODIGO INTEGER OUTPUT,
    				@AU_FECHA DATETIME OUTPUT,
    				@HO_CODIGO CHAR(6) OUTPUT,
    				@AU_STATUS SMALLINT OUTPUT,
    				@AU_AUT_EXT NUMERIC(15,2) OUTPUT,
    				@US_CODIGO SMALLINT OUTPUT
AS
        SET NOCOUNT ON
  	select 	@AU_AUT_EXT = AU_AUT_EXT, 
			@US_CODIGO = US_CODIGO, 
			@HO_CODIGO = HO_CODIGO, 
			@AU_STATUS = AU_STATUS
  	from AUSENCIA
  	where  CB_CODIGO = @Empleado and AU_FECHA = @Fecha

  	if  ( @AU_AUT_EXT is NULL ) 
  	begin
    	insert into AUSENCIA
      	(CB_CODIGO, AU_FECHA, HO_CODIGO, AU_STATUS)
    	values
      	(@Empleado, @Fecha, @Horario, @Status);

    	execute SP_CLAS_AUSENCIA @Empleado, @Fecha ;

    	Select @AU_AUT_EXT = 0, @US_CODIGO = 0;
    	Select @HO_CODIGO = @Horario, @AU_STATUS = @Status;
  	end
	Select @CB_CODIGO = @Empleado,  @AU_FECHA  = @Fecha;

GO

CREATE PROCEDURE SP_UPDATE_AUSENCIA
    				@EMPLEADO INTEGER,
    				@FECHA DATETIME,
    				@HORARIO CHAR(6),
    				@STATUS SMALLINT,
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@POSICION SMALLINT,
    				@INCIDENCIA CHAR(3),
    				@TIPODIA SMALLINT,
    				@ORDINARIAS NUMERIC(15,2),
    				@DOBLES NUMERIC(15,2),
    				@TRIPLES NUMERIC(15,2),
    				@DESCANSO NUMERIC(15,2),
    				@PERMISOCG NUMERIC(15,2),
    				@PERMISOSG NUMERIC(15,2),
                                @HORASNT NUMERIC(15,2)
AS
        SET NOCOUNT ON
  	declare @Usuario SmallInt;
  	declare @Extras Numeric( 15, 2 );
  	declare @Tardes Numeric( 15, 2 );
    select @Extras = @Dobles + @Triples;

    select @Usuario = US_CODIGO, @Tardes = AU_TARDES
    from AUSENCIA
    where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha )

	if ( @Usuario is NULL )
    begin
		insert into AUSENCIA( CB_CODIGO,
                              AU_FECHA,
                              HO_CODIGO,
                              AU_STATUS,
                              PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              AU_POSICIO,
                              AU_TIPO,
                              AU_TIPODIA,
                              AU_HORAS,
                              AU_DOBLES,
                              AU_TRIPLES,
                              AU_EXTRAS,
                              AU_DES_TRA,
                              AU_PER_CG,
                              AU_PER_SG,
                              AU_HORASNT )
         values ( @Empleado,
                  @Fecha,
                  @Horario,
                  @Status,
                  @Anio,
                  @Tipo,
                  @Numero,
                  @Posicion,
                  @Incidencia,
                  @TipoDia,
                  @Ordinarias,
                  @Dobles,
                  @Triples,
                  @Extras,
                  @Descanso,
                  @PermisoCG,
                  @PermisoSG,
                  @HorasNT );
    end
    else
    begin
          if ( @Usuario = 0 )
          begin

               if (( @TipoDia = 8 ) and ( @Tardes > 0 )) 
               begin
                 	Select @Tardes = 0;
                 	if ( @Incidencia = 'RE' ) Select @Incidencia = '';


               end
               update AUSENCIA set PE_YEAR = @Anio,
                                   PE_TIPO = @Tipo,
                                   PE_NUMERO = @Numero,
                                   AU_POSICIO = @Posicion,
                                   AU_TIPO = @Incidencia,
                                   AU_TIPODIA = @TipoDia,
                                   AU_HORAS = @Ordinarias,
                                   AU_DOBLES = @Dobles,
                                   AU_TRIPLES = @Triples,
                                   AU_EXTRAS = @Extras,
                                   AU_DES_TRA = @Descanso,
                                   AU_PER_CG = @PermisoCG,
                                   AU_PER_SG = @PermisoSG,
                                   AU_TARDES = @Tardes,
                                   AU_HORASNT = @HorasNT
               where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha );
          end
          else
               update AUSENCIA set PE_YEAR = @Anio,
                                   PE_TIPO = @Tipo,
                                   PE_NUMERO = @Numero,
                                   AU_DOBLES = @Dobles,
                                   AU_TRIPLES = @Triples,
                                   AU_EXTRAS = @Extras,
                                   AU_POSICIO = @Posicion
               where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha );
     end
     execute SP_CLAS_AUSENCIA @Empleado, @Fecha ;

GO

CREATE PROCEDURE SP_DIAS_JORNADA(
		@TURNO CHAR(6),
    		@FECHAINI DATETIME,
    		@FECHAFIN DATETIME,
    		@TU_DIAS SMALLINT OUTPUT,
    		@TU_JORNADA NUMERIC(15,2) OUTPUT,
    		@ES_ROTA CHAR(1)OUTPUT)
AS
BEGIN
     set NOCOUNT ON
     select @TU_DIAS = TU_DIAS,
            @TU_JORNADA = TU_JORNADA,
            @ES_ROTA = ( case when ( ( TU_RIT_PAT is NULL ) or ( TU_RIT_PAT = '' ) ) then 'N' else 'S' end )
     from TURNO
     where ( TU_CODIGO = @Turno );
     if ( @TU_DIAS is NULL )
     begin
          select @TU_DIAS = 0, @TU_JORNADA = 0, @ES_ROTA = 'N'
     end
END
GO

CREATE PROCEDURE SP_RANGO_SALARIO
	    			@FECHAINI DATETIME,
    				@FECHAFIN DATETIME,
    				@EMPLEADO INTEGER, 
    				@CB_SALARIO PESOS OUTPUT,
    				@CB_SAL_INT PESOS OUTPUT
AS
        SET NOCOUNT ON
	DECLARE @CB_FEC_REV DATETIME;
	DECLARE @CB_OLD_SAL PESOS;
	DECLARE @CB_FEC_INT DATETIME;
	DECLARE @CB_OLD_INT PESOS;
	DECLARE @CB_TIPO CHAR(6);
	DECLARE @CB_FEC_ING DATETIME;

	DECLARE Temporal CURSOR for
  		select  K.CB_FEC_REV, K.CB_SALARIO, K.CB_OLD_SAL,
         		K.CB_FEC_INT, K.CB_SAL_INT, K.CB_OLD_INT,
         		K.CB_TIPO, C.CB_FEC_ING
  		from   	KARDEX K
		INNER JOIN COLABORA C ON C.CB_CODIGO = K.CB_CODIGO
  		where  	K.CB_CODIGO = @Empleado and K.CB_FECHA <= @FechaFin and
         		K.CB_TIPO IN ( 'ALTA', 'CAMBIO' ) and
         		C.CB_CODIGO = K.CB_CODIGO
  		order by K.CB_FECHA DESC, K.CB_NIVEL DESC
	OPEN Temporal
  	fetch NEXT FROM Temporal
	into   	@CB_FEC_REV, @CB_SALARIO, @CB_OLD_SAL,
         	@CB_FEC_INT, @CB_SAL_INT, @CB_OLD_INT,
         	@CB_TIPO, @CB_FEC_ING
	if @@FETCH_STATUS = 0 
	begin
    		if (( @CB_FEC_REV > @FechaIni ) and ( @CB_FEC_REV > @CB_FEC_ING )) 
			SELECT @CB_SALARIO = dbo.SP_PROM_SALARIO( @FechaIni, @FechaFin, @CB_FEC_ING,
                              					  @CB_FEC_REV, @CB_OLD_SAL, @CB_SALARIO )      		

    		if (( @CB_FEC_INT > @FechaIni ) and ( @CB_FEC_INT > @CB_FEC_ING )) 
      			select @CB_SAL_INT = dbo.SP_PROM_SALARIO( @FechaIni, @FechaFin, @CB_FEC_ING,
                              					  @CB_FEC_INT, @CB_OLD_INT, @CB_SAL_INT )

	end;
   	close Temporal
  	deallocate Temporal
GO

CREATE PROCEDURE SP_CLAS_NOMINA
    	@ANIO SMALLINT,
    	@TIPO SMALLINT,
    	@NUMERO SMALLINT,
    	@EMPLEADO INTEGER,
        @ROTATIVO CHAR(6) OUTPUT
AS
    SET NOCOUNT ON
	declare @FechaIni DATETIME;
 	declare @FechaFin DATETIME;
 	declare @ZONA_GE  CHAR(1);
 	declare @TABLA    CHAR(1);
 	declare @PUESTO   CHAR(6);
 	declare @CLASIFI  CHAR(6);
 	declare @TURNO    CHAR(6);
 	declare @PATRON   CHAR(1);
 	declare @NIVEL1   CHAR(6);
 	declare @NIVEL2   CHAR(6);
 	declare @NIVEL3   CHAR(6);
 	declare @NIVEL4   CHAR(6);
 	declare @NIVEL5   CHAR(6);
 	declare @NIVEL6   CHAR(6);
 	declare @NIVEL7   CHAR(6);
 	declare @NIVEL8   CHAR(6);
 	declare @NIVEL9   CHAR(6);
 	declare @SALARIO  NUMERIC(15,2);
 	declare @SAL_INT  NUMERIC(15,2);
 	declare @PROMEDIA CHAR(1);
 	declare @JORNADA  NUMERIC(15,2);
 	declare @D_TURNO  SMALLINT;
 	declare @ES_ROTA  CHAR(1);
 	declare @FEC_KAR  DATETIME;
 	declare @FEC_SAL  DATETIME;
	declare @BAN_ELE  VARCHAR(30);
	declare @NIVEL0   CHAR(6);
	declare @PLAZA	  INTEGER;	
	declare @CB_NOMINA	SMALLINT;
	declare @FEC_ING	DATETIME;
	declare @FEC_BAJ	DATETIME;
	declare @FEC_NOM	DATETIME;
	declare @Fecha		DATETIME;
	declare @k_NOMINA		SMALLINT;
	declare @CTA_GAS  VARCHAR(30);
	declare @CTA_VAL  VARCHAR(30);
	declare @PAGO	SMALLINT; 
	
  	select @FechaIni = PE_FEC_INI,
               @FechaFin = PE_FEC_FIN
  	from   PERIODO
  	where  PE_YEAR = @Anio 
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero

	select 
		@CB_NOMINA = CB_NOMINA
        from   SP_FECHA_KARDEX( @FechaIni, @Empleado )     

	
	SELECT @FEC_ING = CB_FEC_ING, 
		  @FEC_BAJ = CB_FEC_BAJ,
		  @FEC_NOM = CB_FEC_NOM,
		  @k_NOMINA = CB_NOMINA		 
		  from COLABORA where CB_CODIGO = @Empleado

	

	IF ( @FEC_NOM between @FechaIni and @FechaFin ) and ( @CB_NOMINA <> @k_NOMINA )
	begin
		if ( @CB_NOMINA = @Tipo ) 
		begin
			set @Fecha = @FechaIni
		end
		else
		begin
			set @Fecha = @FechaFin
		end 
	end
	else
	begin
		set @Fecha = @FechaFin
	end  
	

  	select @ZONA_GE = CB_ZONA_GE,
  		@TABLA = CB_TABLASS,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
		@PATRON = CB_PATRON,
                @NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2, 
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4, 
		@NIVEL5 = CB_NIVEL5,
                @NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7, 
		@NIVEL8 = CB_NIVEL8, 
		@NIVEL9 = CB_NIVEL9,
                @SALARIO = CB_SALARIO,
		@SAL_INT = CB_SAL_INT, 
		@FEC_KAR = CB_FEC_KAR, 
		@FEC_SAL = CB_FEC_SAL,
                @BAN_ELE = CB_BAN_ELE,
                @NIVEL0 = CB_NIVEL0,
		@PLAZA	= CB_PLAZA,
		@CTA_GAS = CB_CTA_GAS,
        @CTA_VAL = CB_CTA_VAL
        
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @Fecha < @FEC_KAR ) 
  	begin
    		select 
			@ZONA_GE = CB_ZONA_GE,
			@TABLA = CB_TABLASS,
			@PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI, 
			@TURNO = CB_TURNO, 
			@PATRON = CB_PATRON,
         	        @NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
         	        @NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
         	        @SALARIO = CB_SALARIO,
			@SAL_INT = CB_SAL_INT,
			@PLAZA	= CB_PLAZA
            from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

  	select @PROMEDIA = GL_FORMULA
  	from   GLOBAL
  	where  GL_CODIGO = 42

  	if (( @PROMEDIA = 'S' ) and ( @FEC_SAL > @FechaIni ))
        EXECUTE SP_RANGO_SALARIO @FechaIni, @FechaFin, @Empleado,
                                 @SALARIO OUTPUT, @SAL_INT OUTPUT

	EXECUTE SP_DIAS_JORNADA @Turno, @FechaIni, @FechaFin,
    				@D_Turno OUTPUT, @Jornada OUTPUT, @Es_Rota OUTPUT

	if ( @ES_ROTA = 'S' )
    		SET @Rotativo = @Turno
  	else
    		SET @Rotativo = '';

	set @PAGO = dbo.SP_GET_METODO_PAGO_DEFAULT() 

  	update NOMINA
  	set     CB_ZONA_GE = @ZONA_GE,
			CB_TABLASS = @TABLA,
        	CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_PATRON  = @PATRON,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
        	CB_SALARIO = @SALARIO,
        	CB_SAL_INT = @SAL_INT,
        	NO_JORNADA = @JORNADA,
        	NO_D_TURNO = @D_TURNO,
                CB_BAN_ELE = @BAN_ELE,
                CB_NIVEL0  = @NIVEL0,
                CB_PLAZA   = @PLAZA,
				CB_CTA_GAS = @CTA_GAS,
                CB_CTA_VAL = @CTA_VAL,
			NO_PAGO = case  when  NO_PAGO > 0  then NO_PAGO else @PAGO  end 

  	where PE_YEAR = @Anio
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero
	and CB_CODIGO = @Empleado;
GO

CREATE PROCEDURE SP_ADD_NOMINA
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@EMPLEADO INTEGER,
    			@ROTATIVO CHAR(6) OUTPUT
AS
  SET NOCOUNT ON
  INSERT INTO NOMINA
  ( PE_YEAR, PE_TIPO, PE_NUMERO, CB_CODIGO )
  VALUES ( @Anio, @Tipo, @Numero, @Empleado );

  
  exec SP_CLAS_NOMINA @Anio, @Tipo, @Numero, @Empleado, @ROTATIVO OUTPUT
GO

CREATE PROCEDURE SP_CLAS_NOMINA_VACA  
    		@ANIO SMALLINT,
    		@TIPO SMALLINT,
    		@NUMERO SMALLINT,
    		@EMPLEADO INTEGER,
    		@ROTATIVO CHAR(6) OUTPUT,
    		@DIASVACA PESOS OUTPUT
AS
        SET NOCOUNT ON
  	declare @Existe INTEGER;

  	select @DiasVaca=sum( VA_PAGO )
  	from   VACACION
  	where ( CB_CODIGO = @Empleado  ) 
	and ( VA_TIPO = 1 ) 
	and ( VA_NOMYEAR = @Anio ) 
	and ( VA_NOMTIPO = @Tipo ) 
	and ( VA_NOMNUME = @Numero )

  	if ( @DiasVaca > 0 ) 
  	begin
	    	select @Existe = CB_CODIGO
    		from   NOMINA
    		where  ( CB_CODIGO = @Empleado ) 
		and ( PE_YEAR = @Anio ) 
		and ( PE_TIPO = @Tipo ) 
		and ( PE_NUMERO = @Numero )

    		if ( @Existe is NULL ) 
      			execute SP_ADD_NOMINA  @Anio, @Tipo, @Numero, @Empleado, @Rotativo

    		update NOMINA
    		set    NO_DIAS_VA = @DiasVaca
    		where  ( CB_CODIGO = @Empleado ) 
		and ( PE_YEAR = @Anio ) 
		and ( PE_TIPO = @Tipo ) 
		and ( PE_NUMERO = @Numero );
  	end

  	execute SP_CLAS_NOMINA @Anio, @Tipo, @Numero, @Empleado, @Rotativo OUTPUT;
GO

CREATE PROCEDURE SP_ADD_CHECADA 
    				@EMPLEADO INTEGER,
    				@FECHA DATETIME,
    				@HORARIO CHAR(6) ,
    				@STATUS SMALLINT,
    				@HORA CHAR(4) ,
    				@RELOJ CHAR(4),
    				@CB_CODIGO INTEGER OUTPUT,
    				@AU_FECHA DATETIME OUTPUT,
    				@HO_CODIGO CHAR(6) OUTPUT,
    				@AU_STATUS SMALLINT OUTPUT,
    				@AU_AUT_EXT NUMERIC(15,2) OUTPUT,
    				@US_CODIGO SMALLINT OUTPUT,
    				@STATUS_EMP SMALLINT OUTPUT
AS
BEGIN
     SET NOCOUNT ON
     execute SP_ADD_AUSENCIA @Empleado, @Fecha, @Horario, @Status,
 			 @CB_CODIGO OUTPUT, @AU_FECHA OUTPUT, @HO_CODIGO OUTPUT, 
			 @AU_STATUS OUTPUT, @AU_AUT_EXT OUTPUT, @US_CODIGO OUTPUT

     insert into CHECADAS( CB_CODIGO, AU_FECHA, CH_H_REAL, CH_H_AJUS, CH_RELOJ )
     values ( @CB_CODIGO, @AU_FECHA, @Hora, @Hora, @Reloj );

     Select @STATUS_EMP = dbo.SP_STATUS_EMP( @Fecha, @Empleado )

END
GO

CREATE PROCEDURE RECALCULA_TODOS
AS
BEGIN
        SET NOCOUNT ON
	declare @Numero integer;
	declare CursorEmpleados CURSOR FOR
     		select CB_CODIGO 
		from COLABORA 
		order by CB_CODIGO

	
	Open CursorEmpleados
	fetch NEXT from CursorEmpleados
	into @Numero

	while ( @@FETCH_STATUS = 0 )
	BEGIN
		EXECUTE RECALCULA_KARDEX @Numero
		if ( @@ERROR <> 0 )
			BREAK
	
		fetch NEXT from CursorEmpleados
		into @Numero
	END

	Close CursorEmpleados
	Deallocate CursorEmpleados
END
GO

create procedure DBO.SP_BALANZA(
@USUARIO SMALLINT,
@ANIO SMALLINT,
@TIPO SMALLINT,
@NUMERO SMALLINT,
@EMPLEADO INTEGER,
@CONCEPTO SMALLINT,
@COLUMNA SMALLINT,
@DESCRIPCION CHAR( 30 ),
@MONTO NUMERIC( 15, 2 ),
@HORAS NUMERIC( 15, 2 ),
@REFERENCIA VARCHAR( 8 ) )
as
begin
     SET NOCOUNT ON
     declare @Percepcion SMALLINT;
     declare @Deduccion SMALLINT;
     select @Percepcion = COUNT(*) from DBO.TMPBALAN as T
     where ( T.TZ_USER = @USUARIO ) and
           ( T.CB_CODIGO = @EMPLEADO ) and
           ( T.TZ_NUM_PER > 0 );
     select @Deduccion = COUNT(*) from DBO.TMPBALAN as T
     where ( T.TZ_USER = @USUARIO ) and
           ( T.CB_CODIGO = @EMPLEADO ) and
           ( T.TZ_NUM_DED > 0 );
     if ( @COLUMNA = 1 )
     begin
          if ( @Percepcion >= @Deduccion )
          begin
               insert into DBO.TMPBALAN( TZ_CODIGO,
                                         CB_CODIGO,
                                         PE_YEAR,
                                         PE_TIPO,
                                         PE_NUMERO,
                                         TZ_USER,
                                         TZ_NUM_PER,
                                         TZ_MON_PER,
                                         TZ_DES_PER,
                                         TZ_HOR_PER,
                                         TZ_REF_PER ) values (
                                        @PERCEPCION,
                                        @EMPLEADO,
                                        @ANIO,
                                        @TIPO,
                                        @NUMERO,
                                        @USUARIO,
                                        @CONCEPTO,
                                        @MONTO,
                                        @DESCRIPCION,
                                        @HORAS,
                                        @REFERENCIA );
          end
          else
          begin
               update DBO.TMPBALAN set TZ_NUM_PER = @CONCEPTO,
                                       TZ_MON_PER = @MONTO,
                                       TZ_DES_PER = @DESCRIPCION,
                                       TZ_HOR_PER = @HORAS,
                                       TZ_REF_PER = @REFERENCIA
               where ( TZ_USER = @USUARIO ) and
                     ( CB_CODIGO = @EMPLEADO ) and
                     ( TZ_CODIGO = @PERCEPCION );
          end
     end
     else
     begin
          if ( @Deduccion >= @Percepcion  )
          begin
               insert into DBO.TMPBALAN( TZ_CODIGO,
                                         CB_CODIGO,
                                         PE_YEAR,
                                         PE_TIPO,
                                         PE_NUMERO,
                                         TZ_USER,
                                         TZ_NUM_DED,
                                         TZ_MON_DED,
                                         TZ_DES_DED,
                                         TZ_HOR_DED,
                                         TZ_REF_DED ) values (
                                        @DEDUCCION,
                                        @EMPLEADO,
                                        @ANIO,
                                        @TIPO,
                                        @NUMERO,
                                        @USUARIO,
                                        @CONCEPTO,
                                        @MONTO,
                                        @DESCRIPCION,
                                        @HORAS,
                                        @REFERENCIA );
          end
          else
          begin
               update DBO.TMPBALAN set TZ_NUM_DED = @CONCEPTO,
                                       TZ_MON_DED = @MONTO,
                                       TZ_DES_DED = @DESCRIPCION,
                                       TZ_HOR_DED = @HORAS,
                                       TZ_REF_DED = @REFERENCIA
               where ( TZ_USER = @USUARIO ) and
                     ( CB_CODIGO = @EMPLEADO ) and
                     ( TZ_CODIGO = @DEDUCCION );
          end;
     end;
end
GO

create procedure DBO.PREPARA_CALENDARIO(
@USUARIO SMALLINT,
@EMPLEADO INTEGER,
@ANIO SMALLINT,
@MES SMALLINT,
@FECHA_MES DATETIME,
@FECHA_INI DATETIME,
@FECHA_FIN DATETIME )
as
begin
     SET NOCOUNT ON
     declare @CUANTOS Numeric( 15, 2 )
     declare @Fecha_Min DATETIME;
     declare @Fecha_Max DATETIME;
     declare @FechaActual DATETIME;
     declare @Posicion SMALLINT;
     declare @Fecha01 DATETIME;
     declare @Fecha02 DATETIME;
     declare @Fecha03 DATETIME;
     declare @Fecha04 DATETIME;
     declare @Fecha05 DATETIME;
     declare @Fecha06 DATETIME;
     declare @Fecha07 DATETIME;
     declare @Fecha08 DATETIME;
     declare @Fecha09 DATETIME;
     declare @Fecha10 DATETIME;
     declare @Fecha11 DATETIME;
     declare @Fecha12 DATETIME;
     declare @Fecha13 DATETIME;
     declare @Fecha14 DATETIME;
     declare @Fecha15 DATETIME;
     declare @Fecha16 DATETIME;
     declare @Fecha17 DATETIME;
     declare @Fecha18 DATETIME;
     declare @Fecha19 DATETIME;
     declare @Fecha20 DATETIME;
     declare @Fecha21 DATETIME;
     declare @Fecha22 DATETIME;
     declare @Fecha23 DATETIME;
     declare @Fecha24 DATETIME;
     declare @Fecha25 DATETIME;
     declare @Fecha26 DATETIME;
     declare @Fecha27 DATETIME;
     declare @Fecha28 DATETIME;
     declare @Fecha29 DATETIME;
     declare @Fecha30 DATETIME;
     declare @Fecha31 DATETIME;
     declare @STATUS SMALLINT;
     declare @TIPODIA SMALLINT;
     declare @HORAS   NUMERIC( 15, 2 );
     declare @EXTRAS  NUMERIC( 15, 2 );
     declare @DOBLES  NUMERIC( 15, 2 );
     declare @TARDES  NUMERIC( 15, 2 );
     declare @DES_TRA NUMERIC( 15, 2 );
     declare @PER_CG  NUMERIC( 15, 2 );
     declare @PER_SG  NUMERIC( 15, 2 );
     declare @TotHabil   SMALLINT;
     declare @TotSabado  SMALLINT;
     declare @TotDescan  SMALLINT;
     declare @TotNormal  SMALLINT;
     declare @TotIncapa  SMALLINT;
     declare @TotVaca    SMALLINT;
     declare @TotPer_CG  SMALLINT;
     declare @TotPer_SG  SMALLINT;
     declare @TotPer_FJ  SMALLINT;
     declare @TotSuspen  SMALLINT;
     declare @TotPer_OT  SMALLINT;
     declare @TotFestivo SMALLINT;
     declare @TotNo_Trab SMALLINT;
     declare @TotTrabajo SMALLINT;
     declare @TotFaltas  SMALLINT;
     declare @TotRetardo SMALLINT;
     declare @TotHoras   NUMERIC( 15, 2 );
     declare @TotExtras  NUMERIC( 15, 2 );
     declare @TotDobles  NUMERIC( 15, 2 );
     declare @TotDes_Tra NUMERIC( 15, 2 );
     declare @TotHor_CG  NUMERIC( 15, 2 );
     declare @TotHor_SG  NUMERIC( 15, 2 );
     declare @TotTardes  NUMERIC( 15, 2 );
     declare Temporal CURSOR for
        select A.AU_FECHA,
               A.AU_STATUS,
               A.AU_TIPODIA,
               A.AU_HORAS,
               A.AU_EXTRAS,
               A.AU_DOBLES,
               A.AU_TARDES,
               A.AU_DES_TRA,
               A.AU_PER_CG,
               A.AU_PER_SG
        from DBO.AUSENCIA as A where
        ( A.CB_CODIGO = @Empleado ) and
        ( A.AU_FECHA between @Fecha_Ini and @Fecha_Fin )
        order by A.AU_FECHA
     set @Cuantos = 0;
     set @Fecha01 = '12/30/1899';
     set @Fecha02 = @Fecha01;
     set @Fecha03 = @Fecha01;
     set @Fecha04 = @Fecha01;
     set @Fecha05 = @Fecha01;
     set @Fecha06 = @Fecha01;
     set @Fecha07 = @Fecha01;
     set @Fecha08 = @Fecha01;
     set @Fecha09 = @Fecha01;
     set @Fecha10 = @Fecha01;
     set @Fecha11 = @Fecha01;
     set @Fecha12 = @Fecha01;
     set @Fecha13 = @Fecha01;
     set @Fecha14 = @Fecha01;
     set @Fecha15 = @Fecha01;
     set @Fecha16 = @Fecha01;
     set @Fecha17 = @Fecha01;
     set @Fecha18 = @Fecha01;
     set @Fecha19 = @Fecha01;
     set @Fecha20 = @Fecha01;
     set @Fecha21 = @Fecha01;
     set @Fecha22 = @Fecha01;
     set @Fecha23 = @Fecha01;
     set @Fecha24 = @Fecha01;
     set @Fecha25 = @Fecha01;
     set @Fecha26 = @Fecha01;
     set @Fecha27 = @Fecha01;
     set @Fecha28 = @Fecha01;
     set @Fecha29 = @Fecha01;
     set @Fecha30 = @Fecha01;
     set @Fecha31 = @Fecha01;
     set @TotHabil   = 0;
     set @TotSabado  = 0;
     set @TotDescan  = 0;
     set @TotNormal  = 0;
     set @TotIncapa  = 0;
     set @TotVaca    = 0;
     set @TotPer_CG  = 0;
     set @TotPer_SG  = 0;
     set @TotPer_FJ  = 0;
     set @TotSuspen  = 0;
     set @TotPer_OT  = 0;
     set @TotFestivo = 0;
     set @TotNo_Trab = 0;
     set @TotTrabajo = 0;
     set @TotFaltas  = 0;
     set @TotRetardo = 0;
     set @TotHoras   = 0;
     set @TotExtras  = 0;
     set @TotDobles  = 0;
     set @TotDes_Tra = 0;
     set @TotHor_CG  = 0;
     set @TotHor_SG  = 0;
     set @TotTardes  = 0;
     Open Temporal;
     fetch NEXT from Temporal
     into @FechaActual,
          @Status,
          @TipoDia,
          @Horas,
          @Extras,
          @Dobles,
          @Tardes,
          @Des_Tra,
          @Per_CG,
          @Per_SG;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @TipoDia = 9 )
             set @TotNo_Trab = @TotNo_Trab + 1;
          else
          begin
               if ( @Cuantos = 0 )
                  set @Fecha_Min = @FechaActual;
               set @Fecha_Max = @FechaActual;
               set @Cuantos = @Cuantos + 1;
               if ( @Status = 0 )
                  set @TotHabil = @TotHabil + 1
               else
                   if ( @Status = 1 )
                      set @TotSabado = @TotSabado + 1;
                   else
                       set @TotDescan = @TotDescan + 1;
               if ( @TipoDia = 0 ) set @TotNormal = @TotNormal + 1
               else
               if ( @TipoDia = 1 ) set @TotIncapa = @TotIncapa + 1
               else
               if ( @TipoDia = 2 ) set @TotVaca = @TotVaca + 1
               else
               if ( @TipoDia = 3 ) set @TotPer_CG = @TotPer_CG + 1
               else
               if ( @TipoDia = 4 ) set @TotPer_SG = @TotPer_SG + 1
               else
               if ( @TipoDia = 5 ) set @TotPer_FJ = @TotPer_FJ + 1
               else
               if ( @TipoDia = 6 ) set @TotSuspen = @TotSuspen + 1
               else
               if ( @TipoDia = 7 ) set @TotPer_OT = @TotPer_OT + 1
               else
                   set @TotFestivo = @TotFestivo + 1;
               if ( @Horas > 0 )
                  set @TotTrabajo = @TotTrabajo + 1
               else
                   if ( ( @Status = 0 ) and ( @TipoDia = 0 ) )
                      set @TotFaltas = @TotFaltas + 1;
               if ( @Tardes > 0 )
                  set @TotRetardo = @TotRetardo + 1;
               set @TotHoras   = @TotHoras + @Horas;
               set @TotExtras  = @TotExtras + @Extras;
               set @TotDobles  = @TotDobles + @Dobles;
               set @TotDes_Tra = @TotDes_Tra + @Des_Tra;
               set @TotHor_CG  = @TotHor_CG + @Per_CG;
               set @TotHor_SG  = @TotHor_SG + @Per_SG;
               set @TotTardes  = @TotTardes + @Tardes;
          end
          set @Posicion = CONVERT( SmallInt, @FechaActual - @Fecha_Mes ) + 1;
          if ( @Posicion =  1 )
             set @Fecha01 = @FechaActual
          else
          if ( @Posicion =  2 )
             set @Fecha02 = @FechaActual
          else
          if ( @Posicion =  3 )
             set @Fecha03 = @FechaActual
          else
          if ( @Posicion =  4 )
             set @Fecha04 = @FechaActual
          else
          if ( @Posicion =  5 )
             set @Fecha05 = @FechaActual
          else
          if ( @Posicion =  6 )
             set @Fecha06 = @FechaActual
          else
          if ( @Posicion =  7 )
             set @Fecha07 = @FechaActual
          else
          if ( @Posicion =  8 )
             set @Fecha08 = @FechaActual
          else
          if ( @Posicion =  9 )
             set @Fecha09 = @FechaActual
          else
          if ( @Posicion = 10 )
             set @Fecha10 = @FechaActual
          else
          if ( @Posicion = 11 )
             set @Fecha11 = @FechaActual
          else
          if ( @Posicion = 12 )
             set @Fecha12 = @FechaActual
          else
          if ( @Posicion = 13 )
             set @Fecha13 = @FechaActual
          else
          if ( @Posicion = 14 )
             set @Fecha14 = @FechaActual
          else
          if ( @Posicion = 15 )
             set @Fecha15 = @FechaActual
          else
          if ( @Posicion = 16 )
             set @Fecha16 = @FechaActual
          else
          if ( @Posicion = 17 )
             set @Fecha17 = @FechaActual
          else
          if ( @Posicion = 18 )
             set @Fecha18 = @FechaActual
          else
          if ( @Posicion = 19 )
             set @Fecha19 = @FechaActual
          else
          if ( @Posicion = 20 )
             set @Fecha20 = @FechaActual
          else
          if ( @Posicion = 21 )
             set @Fecha21 = @FechaActual
          else
          if ( @Posicion = 22 )
             set @Fecha22 = @FechaActual
          else
          if ( @Posicion = 23 )
             set @Fecha23 = @FechaActual
          else
          if ( @Posicion = 24 )
             set @Fecha24 = @FechaActual
          else
          if ( @Posicion = 25 )
             set @Fecha25 = @FechaActual
          else
          if ( @Posicion = 26 )
             set @Fecha26 = @FechaActual
          else
          if ( @Posicion = 27 )
             set @Fecha27 = @FechaActual
          else
          if ( @Posicion = 28 )
             set @Fecha28 = @FechaActual
          else
          if ( @Posicion = 29 )
             set @Fecha29 = @FechaActual
          else
          if ( @Posicion = 30 )
             set @Fecha30 = @FechaActual
          else
          if ( @Posicion = 31 )
             set @Fecha31 = @FechaActual;
          fetch NEXT from Temporal
          into @FechaActual,
               @Status,
               @TipoDia,
               @Horas,
               @Extras,
               @Dobles,
               @Tardes,
               @Des_Tra,
               @Per_CG,
               @Per_SG;
     end;
	 close Temporal;
	 deallocate Temporal;
     if ( @Cuantos > 0 )
     begin
          insert into DBO.TMPCALEN
            ( US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES, TL_FEC_MIN, TL_FEC_MAX, TL_NUMERO,
              TL_FECHA01, TL_FECHA02, TL_FECHA03, TL_FECHA04, TL_FECHA05, TL_FECHA06, TL_FECHA07, TL_FECHA08, TL_FECHA09, TL_FECHA10,
              TL_FECHA11, TL_FECHA12, TL_FECHA13, TL_FECHA14, TL_FECHA15, TL_FECHA16, TL_FECHA17, TL_FECHA18, TL_FECHA19, TL_FECHA20,
              TL_FECHA21, TL_FECHA22, TL_FECHA23, TL_FECHA24, TL_FECHA25, TL_FECHA26, TL_FECHA27, TL_FECHA28, TL_FECHA29, TL_FECHA30, TL_FECHA31,
              TL_HABIL, TL_SABADO, TL_DESCAN, TL_NORMAL, TL_INCAPA, TL_VACA, TL_PER_CG, TL_PER_SG, TL_PER_FJ, TL_SUSPEN, TL_PER_OT, TL_FESTIVO,
              TL_NO_TRAB, TL_TRABAJO, TL_FALTAS, TL_RETARDO, TL_HORAS, TL_EXTRAS, TL_DOBLES, TL_DES_TRA, TL_HOR_CG, TL_HOR_SG, TL_TARDES )
          values
            ( @USUARIO, @Empleado, @Anio, @Mes, @Fecha_Min, @Fecha_Max, @Cuantos,
              @Fecha01, @Fecha02, @Fecha03, @Fecha04, @Fecha05, @Fecha06, @Fecha07, @Fecha08, @Fecha09, @Fecha10,
              @Fecha11, @Fecha12, @Fecha13, @Fecha14, @Fecha15, @Fecha16, @Fecha17, @Fecha18, @Fecha19, @Fecha20,
              @Fecha21, @Fecha22, @Fecha23, @Fecha24, @Fecha25, @Fecha26, @Fecha27, @Fecha28, @Fecha29, @Fecha30, @Fecha31,
              @TotHabil, @TotSabado, @TotDescan, @TotNormal, @TotIncapa, @TotVaca, @TotPer_CG, @TotPer_SG, @TotPer_FJ, @TotSuspen, @TotPer_OT, @TotFestivo,
              @TotNo_Trab, @TotTrabajo, @TotFaltas, @TotRetardo, @TotHoras, @TotExtras, @TotDobles, @TotDes_Tra, @TotHor_CG, @TotHor_SG, @TotTardes );
     end;
end
GO

CREATE PROCEDURE DBO.INIT_PROCESS_LOG(
  @Proceso Integer,
  @Usuario Smallint,
  @Fecha   DateTime,
  @Hora    Varchar(8),
  @Maximo  Integer,
  @Params   varchar(255),
  @Folio   Integer OUTPUT )
AS
  SET NOCOUNT ON
  select @Folio = MAX( PC_NUMERO ) from PROCESO

  if ( @Folio is NULL )
    select @Folio = 1
  else
    select @Folio = @Folio + 1

  insert into PROCESO ( PC_PROC_ID,
                        PC_NUMERO,
                        US_CODIGO,
                        PC_FEC_INI,
                        PC_HOR_INI,
                        PC_MAXIMO,
                        PC_ERROR,
			PC_PARAMS ) values
                         ( @Proceso,
                           @Folio,
                           @Usuario,
                           @Fecha,
                           @Hora,
                           @Maximo,
                           'A',
			   @Params )
GO

CREATE PROCEDURE UPDATE_PROCESS_LOG( 
  @Proceso  Integer, 
  @Empleado Integer, 
  @Paso     Integer, 
  @Fecha    DateTime, 
  @Hora     VARCHAR( 8 ),
  @Status   SmallInt OUTPUT ) 
AS
  SET NOCOUNT ON
  declare @Bandera Char( 1 )

  update PROCESO set
         CB_CODIGO = @Empleado,
         PC_PASO = @Paso,
         PC_FEC_FIN = @Fecha,
         PC_HOR_FIN = @Hora
  where ( PC_NUMERO = @Proceso )

  select @Bandera = PC_ERROR 
  from   PROCESO
  where ( PC_NUMERO = @Proceso )

  if ( @Bandera = 'A' )
    select @Status = 0
  else
    select @Status = 1

GO

CREATE procedure SP_CANCELA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER,
@USUARIO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @StatusNuevo SmallInt;
     select @StatusNuevo = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @StatusNuevo is not Null )
     begin
          if ( @StatusNuevo > 4 )
             set @StatusNuevo = 4;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,
								  CB_BAN_ELE,
								  NO_HORAPDT,
								  NO_HORASNT,
								  NO_DIAS_SI,
								  NO_DIAS_VJ,
							      NO_DIAS_PV,
								  NO_SUP_OK,
								  NO_FEC_OK,
								  NO_HOR_OK,
								  NO_ASI_INI,
								  NO_ASI_FIN,
								  NO_DIAS_BA,
								  NO_PREV_GR,
								  CB_PLAZA,
								  NO_FEC_LIQ,
								  CB_CTA_GAS,
								  CB_CTA_VAL,
								  NO_PER_ISN,
                  NO_DIAS_IH,
                  NO_DIAS_ID,
                  NO_DIAS_IT,
                  NO_TOTAL_1,
                  NO_TOTAL_2,
                  NO_TOTAL_3,
                  NO_TOTAL_4,
                  NO_TOTAL_5,
                  NO_FACT_BA,
                  NO_DIAS_DT,
                  NO_DIAS_FS,
                  NO_DIAS_FT,
				  CB_TABLASS
								  )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N1.CB_CODIGO,
                 N1.CB_CLASIFI,
                 N1.CB_TURNO,
                 N1.CB_PATRON,
                 N1.CB_PUESTO,
                 N1.CB_ZONA_GE,
                 N1.NO_FOLIO_1,
                 N1.NO_FOLIO_2,
                 N1.NO_FOLIO_3,
                 N1.NO_FOLIO_4,
                 N1.NO_FOLIO_5,
                 N1.NO_OBSERVA,
                 @StatusNuevo,
                 N1.US_CODIGO,
                 N1.NO_USER_RJ,
                 N1.CB_NIVEL1,
                 N1.CB_NIVEL2,
                 N1.CB_NIVEL3,
                 N1.CB_NIVEL4,
                 N1.CB_NIVEL5,
                 N1.CB_NIVEL6,
                 N1.CB_NIVEL7,
                 N1.CB_NIVEL8,
                 N1.CB_NIVEL9,
                 N1.CB_SAL_INT,
                 N1.CB_SALARIO,
                 -N1.NO_DIAS,
                 -N1.NO_ADICION,
                 -N1.NO_DEDUCCI,
                 -N1.NO_NETO,
                 -N1.NO_DES_TRA,
                 -N1.NO_DIAS_AG,
                 -N1.NO_DIAS_VA,
                 -N1.NO_DOBLES,
                 -N1.NO_EXTRAS,
                 -N1.NO_FES_PAG,
                 -N1.NO_FES_TRA,
                 -N1.NO_HORA_CG,
                 -N1.NO_HORA_SG,
                 -N1.NO_HORAS,
                 -N1.NO_IMP_CAL,
                 -N1.NO_JORNADA,
                 -N1.NO_PER_CAL,
                 -N1.NO_PER_MEN,
                 -N1.NO_PERCEPC,
                 -N1.NO_TARDES,
                 -N1.NO_TRIPLES,
                 -N1.NO_TOT_PRE,
                 -N1.NO_VAC_TRA,
                 -N1.NO_X_CAL,
                 -N1.NO_X_ISPT,
                 -N1.NO_X_MENS,
                 -N1.NO_D_TURNO,
                 -N1.NO_DIAS_AJ,
                 -N1.NO_DIAS_AS,
                 -N1.NO_DIAS_CG,
                 -N1.NO_DIAS_EM,
                 -N1.NO_DIAS_FI,
                 -N1.NO_DIAS_FJ,
                 -N1.NO_DIAS_FV,
                 -N1.NO_DIAS_IN,
                 -N1.NO_DIAS_NT,
                 -N1.NO_DIAS_OT,
                 -N1.NO_DIAS_RE,
                 -N1.NO_DIAS_SG,
                 -N1.NO_DIAS_SS,
                 -N1.NO_DIAS_SU,
                 -N1.NO_HORA_PD,
                 N1.NO_LIQUIDA,
                 N1.NO_FUERA,
                 -N1.NO_EXENTAS,
                 N1.NO_FEC_PAG,
                 N1.NO_USR_PAG,
				 N1.CB_NIVEL0,
				 N1.CB_BAN_ELE,
				 -N1.NO_HORAPDT,
				 -N1.NO_HORASNT,
				 -N1.NO_DIAS_SI,
				 -N1.NO_DIAS_VJ,
				 -N1.NO_DIAS_PV,
				 N1.NO_SUP_OK,
				 N1.NO_FEC_OK,
				 N1.NO_HOR_OK,
				 N1.NO_ASI_INI,
				 N1.NO_ASI_FIN,
				 -N1.NO_DIAS_BA,
				 -N1.NO_PREV_GR,
				 N1.CB_PLAZA,
				 N1.NO_FEC_LIQ,
				 N1.CB_CTA_GAS,
				 N1.CB_CTA_VAL,
				 -N1.NO_PER_ISN,
         -N1.NO_DIAS_IH,
         -N1.NO_DIAS_ID,
         -N1.NO_DIAS_IT,
         -N1.NO_TOTAL_1,
         -N1.NO_TOTAL_2,
         -N1.NO_TOTAL_3,
         -N1.NO_TOTAL_4,
         -N1.NO_TOTAL_5,
         -N1.NO_FACT_BA,
         -N1.NO_DIAS_DT,
         -N1.NO_DIAS_FS,
         -N1.NO_DIAS_FT,
		 N1.CB_TABLASS
          from DBO.NOMINA as N1 where
          ( N1.PE_YEAR = @YEARORIGINAL ) and
          ( N1.PE_TIPO = @TIPOORIGINAL ) and
          ( N1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( N1.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M1.CB_CODIGO,
                 M1.MO_ACTIVO,
                 M1.CO_NUMERO,
                 -M1.MO_IMP_CAL,
                 @USUARIO,
                 M1.MO_PER_CAL,
                 M1.MO_REFEREN,
                 -M1.MO_X_ISPT,
                 -M1.MO_PERCEPC,
                 -M1.MO_DEDUCCI
          from DBO.MOVIMIEN as M1 where
          ( M1.PE_YEAR = @YEARORIGINAL ) and
          ( M1.PE_TIPO = @TIPOORIGINAL ) and
          ( M1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( M1.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              CB_CODIGO,
                              FA_FEC_INI,
                              FA_DIA_HOR,
                              FA_MOTIVO,
                              FA_DIAS,
                              FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F1.CB_CODIGO,
                 F1.FA_FEC_INI,
                 F1.FA_DIA_HOR,
                 F1.FA_MOTIVO,
                 -F1.FA_DIAS,
                 -F1.FA_HORAS
          from DBO.FALTAS as F1 where
          ( F1.PE_YEAR = @YEARORIGINAL ) and
          ( F1.PE_TIPO = @TIPOORIGINAL ) and
          ( F1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( F1.CB_CODIGO = @EMPLEADO );
     end
end
GO

create procedure DBO.PREPARA_CALENDARIO_HORAS(
@USUARIO SMALLINT,
@EMPLEADO INTEGER,
@ANIO SMALLINT,
@MES SMALLINT,
@FECHA_MES DATETIME,
@FECHA_INI DATETIME,
@FECHA_FIN DATETIME,
@CAMPO SMALLINT,
@PRIMERO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @Cuantos NUMERIC(15, 2);
     declare @Fecha_Min DATETIME;
     declare @Fecha_Max DATETIME;
     declare @FechaActual DATETIME;
     declare @Posicion SMALLINT;
     declare @Tipo CHAR(3);
     declare @AuTipo01 CHAR(3);
     declare @AuTipo02 CHAR(3);
     declare @AuTipo03 CHAR(3);
     declare @AuTipo04 CHAR(3);
     declare @AuTipo05 CHAR(3);
     declare @AuTipo06 CHAR(3);
     declare @AuTipo07 CHAR(3);
     declare @AuTipo08 CHAR(3);
     declare @AuTipo09 CHAR(3);
     declare @AuTipo10 CHAR(3);
     declare @AuTipo11 CHAR(3);
     declare @AuTipo12 CHAR(3);
     declare @AuTipo13 CHAR(3);
     declare @AuTipo14 CHAR(3);
     declare @AuTipo15 CHAR(3);
     declare @AuTipo16 CHAR(3);
     declare @AuTipo17 CHAR(3);
     declare @AuTipo18 CHAR(3);
     declare @AuTipo19 CHAR(3);
     declare @AuTipo20 CHAR(3);
     declare @AuTipo21 CHAR(3);
     declare @AuTipo22 CHAR(3);
     declare @AuTipo23 CHAR(3);
     declare @AuTipo24 CHAR(3);
     declare @AuTipo25 CHAR(3);
     declare @AuTipo26 CHAR(3);
     declare @AuTipo27 CHAR(3);
     declare @AuTipo28 CHAR(3);
     declare @AuTipo29 CHAR(3);
     declare @AuTipo30 CHAR(3);
     declare @AuTipo31 CHAR(3);
     declare @Monto NUMERIC( 15, 2 );
     declare @Monto01 NUMERIC( 15, 2 );
     declare @Monto02 NUMERIC( 15, 2 );
     declare @Monto03 NUMERIC( 15, 2 );
     declare @Monto04 NUMERIC( 15, 2 );
     declare @Monto05 NUMERIC( 15, 2 );
     declare @Monto06 NUMERIC( 15, 2 );
     declare @Monto07 NUMERIC( 15, 2 );
     declare @Monto08 NUMERIC( 15, 2 );
     declare @Monto09 NUMERIC( 15, 2 );
     declare @Monto10 NUMERIC( 15, 2 );
     declare @Monto11 NUMERIC( 15, 2 );
     declare @Monto12 NUMERIC( 15, 2 );
     declare @Monto13 NUMERIC( 15, 2 );
     declare @Monto14 NUMERIC( 15, 2 );
     declare @Monto15 NUMERIC( 15, 2 );
     declare @Monto16 NUMERIC( 15, 2 );
     declare @Monto17 NUMERIC( 15, 2 );
     declare @Monto18 NUMERIC( 15, 2 );
     declare @Monto19 NUMERIC( 15, 2 );
     declare @Monto20 NUMERIC( 15, 2 );
     declare @Monto21 NUMERIC( 15, 2 );
     declare @Monto22 NUMERIC( 15, 2 );
     declare @Monto23 NUMERIC( 15, 2 );
     declare @Monto24 NUMERIC( 15, 2 );
     declare @Monto25 NUMERIC( 15, 2 );
     declare @Monto26 NUMERIC( 15, 2 );
     declare @Monto27 NUMERIC( 15, 2 );
     declare @Monto28 NUMERIC( 15, 2 );
     declare @Monto29 NUMERIC( 15, 2 );
     declare @Monto30 NUMERIC( 15, 2 );
     declare @Monto31 NUMERIC( 15, 2 );
     declare @STATUS SMALLINT;
     declare @TIPODIA SMALLINT;
     declare @HORAS   NUMERIC( 15, 2 );
     declare @EXTRAS  NUMERIC( 15, 2 );
     declare @DOBLES  NUMERIC( 15, 2 );
     declare @TRIPLES NUMERIC( 15, 2 );
     declare @TARDES  NUMERIC( 15, 2 );
     declare @DES_TRA NUMERIC( 15, 2 );
     declare @PER_CG  NUMERIC( 15, 2 );
     declare @PER_SG  NUMERIC( 15, 2 );
     declare @TotHabil   SMALLINT;
     declare @TotSabado  SMALLINT;
     declare @TotDescan  SMALLINT;
     declare @TotNormal  SMALLINT;
     declare @TotIncapa  SMALLINT;
     declare @TotVaca    SMALLINT;
     declare @TotPer_CG  SMALLINT;
     declare @TotPer_SG  SMALLINT;
     declare @TotPer_FJ  SMALLINT;
     declare @TotSuspen  SMALLINT;
     declare @TotPer_OT  SMALLINT;
     declare @TotFestivo SMALLINT;
     declare @TotNo_Trab SMALLINT;
     declare @TotTrabajo SMALLINT;
     declare @TotFaltas  SMALLINT;
     declare @TotRetardo SMALLINT;
     declare @TotHoras   NUMERIC( 15, 2 );
     declare @TotExtras  NUMERIC( 15, 2 );
     declare @TotDobles  NUMERIC( 15, 2 );
     declare @TotDes_Tra NUMERIC( 15, 2 );
     declare @TotHor_CG  NUMERIC( 15, 2 );
     declare @TotHor_SG  NUMERIC( 15, 2 );
     declare @TotTardes  NUMERIC( 15, 2 );
     declare Temporal CURSOR for
        select A.AU_FECHA,
               A.AU_STATUS,
               A.AU_TIPODIA,
               A.AU_HORAS,
               A.AU_EXTRAS,
               A.AU_DOBLES,
               A.AU_TRIPLES,
               A.AU_TIPO,
               A.AU_TARDES,
               A.AU_DES_TRA,
               A.AU_PER_CG,
               A.AU_PER_SG
        from DBO.AUSENCIA as A where
        ( A.CB_CODIGO = @Empleado ) and
        ( A.AU_FECHA between @FECHA_INI and @FECHA_FIN )
        order by A.AU_FECHA

     set @Cuantos = 0;
     set @Monto01 = 0;
     set @Monto02 = @Monto01;
     set @Monto03 = @Monto01;
     set @Monto04 = @Monto01;
     set @Monto05 = @Monto01;
     set @Monto06 = @Monto01;
     set @Monto07 = @Monto01;
     set @Monto08 = @Monto01;
     set @Monto09 = @Monto01;
     set @Monto10 = @Monto01;
     set @Monto11 = @Monto01;
     set @Monto12 = @Monto01;
     set @Monto13 = @Monto01;
     set @Monto14 = @Monto01;
     set @Monto15 = @Monto01;
     set @Monto16 = @Monto01;
     set @Monto17 = @Monto01;
     set @Monto18 = @Monto01;
     set @Monto19 = @Monto01;
     set @Monto20 = @Monto01;
     set @Monto21 = @Monto01;
     set @Monto22 = @Monto01;
     set @Monto23 = @Monto01;
     set @Monto24 = @Monto01;
     set @Monto25 = @Monto01;
     set @Monto26 = @Monto01;
     set @Monto27 = @Monto01;
     set @Monto28 = @Monto01;
     set @Monto29 = @Monto01;
     set @Monto30 = @Monto01;
     set @Monto31 = @Monto01;
     set @AuTipo01 = '';
     set @AuTipo02 = @AuTipo01;
     set @AuTipo03 = @AuTipo01;
     set @AuTipo04 = @AuTipo01;
     set @AuTipo05 = @AuTipo01;
     set @AuTipo06 = @AuTipo01;
     set @AuTipo07 = @AuTipo01;
     set @AuTipo08 = @AuTipo01;
     set @AuTipo09 = @AuTipo01;
     set @AuTipo10 = @AuTipo01;
     set @AuTipo11 = @AuTipo01;
     set @AuTipo12 = @AuTipo01;
     set @AuTipo13 = @AuTipo01;
     set @AuTipo14 = @AuTipo01;
     set @AuTipo15 = @AuTipo01;
     set @AuTipo16 = @AuTipo01;
     set @AuTipo17 = @AuTipo01;
     set @AuTipo18 = @AuTipo01;
     set @AuTipo19 = @AuTipo01;
     set @AuTipo20 = @AuTipo01;
     set @AuTipo21 = @AuTipo01;
     set @AuTipo22 = @AuTipo01;
     set @AuTipo23 = @AuTipo01;
     set @AuTipo24 = @AuTipo01;
     set @AuTipo25 = @AuTipo01;
     set @AuTipo26 = @AuTipo01;
     set @AuTipo27 = @AuTipo01;
     set @AuTipo28 = @AuTipo01;
     set @AuTipo29 = @AuTipo01;
     set @AuTipo30 = @AuTipo01;
     set @AuTipo31 = @AuTipo01;
     set @TotHabil   = 0;
     set @TotSabado  = 0;
     set @TotDescan  = 0;
     set @TotNormal  = 0;
     set @TotIncapa  = 0;
     set @TotVaca    = 0;
     set @TotPer_CG  = 0;
     set @TotPer_SG  = 0;
     set @TotPer_FJ  = 0;
     set @TotSuspen  = 0;
     set @TotPer_OT  = 0;
     set @TotFestivo = 0;
     set @TotNo_Trab = 0;
     set @TotTrabajo = 0;
     set @TotFaltas  = 0;
     set @TotRetardo = 0;
     set @TotHoras   = 0;
     set @TotExtras  = 0;
     set @TotDobles  = 0;
     set @TotDes_Tra = 0;
     set @TotHor_CG  = 0;
     set @TotHor_SG  = 0;
     set @TotTardes  = 0;
     Open Temporal;
     fetch NEXT from Temporal
     into @FechaActual,
          @Status,
          @TipoDia,
          @Horas,
          @Extras,
          @Dobles,
          @Triples,
          @Tipo,
          @Tardes,
          @Des_Tra,
          @Per_CG,
          @Per_SG;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @TipoDia = 9 )
          begin
               if ( @Primero = 1 )
                  set @TotNo_Trab = @TotNo_Trab + 1;
          end
          else
          begin
               if ( @Cuantos = 0 )
                  set @Fecha_Min = @FechaActual;
               set @Fecha_Max = @FechaActual;
               set @Cuantos = @Cuantos + 1;
               if ( @Primero = 1 )
               begin
                    if ( @Status = 0 )
                       set @TotHabil = @TotHabil + 1
                    else
                        if ( @Status = 1 )
                            set @TotSabado = @TotSabado + 1
                        else
                            set @TotDescan = @TotDescan + 1;
                    if ( @TipoDia = 0 )
                       set @TotNormal = @TotNormal + 1
                    else
                    if ( @TipoDia = 1 )
                       set @TotIncapa = @TotIncapa + 1
                    else
                    if ( @TipoDia = 2 )
                       set @TotVaca = @TotVaca + 1
                    else
                    if ( @TipoDia = 3 )
                       set @TotPer_CG = @TotPer_CG + 1
                    else
                    if ( @TipoDia = 4 )
                       set @TotPer_SG = @TotPer_SG + 1
                    else
                    if ( @TipoDia = 5 )
                       set @TotPer_FJ = @TotPer_FJ + 1
                    else
                    if ( @TipoDia = 6 )
                       set @TotSuspen = @TotSuspen + 1
                    else
                    if ( @TipoDia = 7 )
                       set @TotPer_OT = @TotPer_OT + 1
                    else
                        set @TotFestivo = @TotFestivo + 1;
                    if ( @Horas > 0 )
                       set @TotTrabajo = @TotTrabajo + 1
                    else
                        if ( ( @Status = 0 ) AND ( @TipoDia = 0 ) )
                           set @TotFaltas = @TotFaltas + 1;
                    if ( @Tardes > 0 )
                       set @TotRetardo = @TotRetardo + 1;
                    set @TotHoras   = @TotHoras + @Horas;
                    set @TotExtras  = @TotExtras + @Extras;
                    set @TotDobles  = @TotDobles + @Dobles;
                    set @TotDes_Tra = @TotDes_Tra + @Des_Tra;
                    set @TotHor_CG  = @TotHor_CG + @Per_CG;
                    set @TotHor_SG  = @TotHor_SG + @Per_SG;
                    set @TotTardes  = @TotTardes + @Tardes;
               end;
          end;
          if ( @Campo = 0 )
             set @Monto = @Horas
          else
          if ( @Campo = 1 )
             set @Monto = @Extras
          else
          if ( @Campo = 2 )
             set @Monto = @Dobles
          else
          if ( @Campo = 3 )
             set @Monto = @Triples
          else
          if ( @Campo = 4 )
             set @Monto = @Tardes
          else
          if ( @Campo = 5 )
             set @Monto = @Des_Tra
          else
          if ( @Campo = 6 )
             set @Monto = @Per_CG
          else
          if ( @Campo = 7 )
             set @Monto = @Per_SG
          else
          if ( @Campo = 8 )
             set @Monto = @Status
          else
          if ( @Campo = 9 )
             set @Monto = @TipoDia
          else
              set @Monto = 0;
          set @Posicion = CONVERT( SmallInt, @FechaActual - @Fecha_Mes ) + 1;
          if ( @Posicion =  1 )
          begin
               set @AuTipo01 = @Tipo;
               set @Monto01 = @Monto;
          end
          else if ( @Posicion =  2 )
          begin
               set @AuTipo02 = @Tipo;
               set @Monto02 = @Monto;
          end
          else if ( @Posicion =  3 )
          begin
               set @AuTipo03 = @Tipo;
               set @Monto03 = @Monto;
          end
          else if ( @Posicion =  4 )
          begin
               set @AuTipo04 = @Tipo;
               set @Monto04 = @Monto;
          end
          else if ( @Posicion =  5 )
          begin
               set @AuTipo05 = @Tipo;
               set @Monto05 = @Monto;
          end
          else if ( @Posicion =  6 )
          begin
               set @AuTipo06 = @Tipo;
               set @Monto06 = @Monto;
          end
          else if ( @Posicion =  7 )
          begin
               set @AuTipo07 = @Tipo;
               set @Monto07 = @Monto;
          end
          else if ( @Posicion =  8 )
          begin
               set @AuTipo08 = @Tipo;
               set @Monto08 = @Monto;
          end
          else if ( @Posicion =  9 )
          begin
               set @AuTipo09 = @Tipo;
               set @Monto09 = @Monto;
          end
          else if ( @Posicion = 10 )
          begin
               set @AuTipo10 = @Tipo;
               set @Monto10 = @Monto;
          end
          else if ( @Posicion = 11 )
          begin
               set @AuTipo11 = @Tipo;
               set @Monto11 = @Monto;
          end
          else if ( @Posicion = 12 )
          begin
               set @AuTipo12 = @Tipo;
               set @Monto12 = @Monto;
          end
          else if ( @Posicion = 13 )
          begin
               set @AuTipo13 = @Tipo;
               set @Monto13 = @Monto;
          end
          else if ( @Posicion = 14 )
          begin
               set @AuTipo14 = @Tipo;
               set @Monto14 = @Monto;
          end
          else if ( @Posicion = 15 )
          begin
               set @AuTipo15 = @Tipo;
               set @Monto15 = @Monto;
          end
          else if ( @Posicion = 16 )
          begin
               set @AuTipo16 = @Tipo;
               set @Monto16 = @Monto;
          end
          else if ( @Posicion = 17 )
          begin
               set @AuTipo17 = @Tipo;
               set @Monto17 = @Monto;
          end
          else if ( @Posicion = 18 )
          begin
               set @AuTipo18 = @Tipo;
               set @Monto18 = @Monto;
          end
          else if ( @Posicion = 19 )
          begin
               set @AuTipo19 = @Tipo;
               set @Monto19 = @Monto;
          end
          else if ( @Posicion = 20 )
          begin
               set @AuTipo20 = @Tipo;
               set @Monto20 = @Monto;
          end
          else if ( @Posicion = 21 )
          begin
               set @AuTipo21 = @Tipo;
               set @Monto21 = @Monto;
          end
          else if ( @Posicion = 22 )
          begin
               set @AuTipo22 = @Tipo;
               set @Monto22 = @Monto;
          end
          else if ( @Posicion = 23 )
          begin
               set @AuTipo23 = @Tipo;
               set @Monto23 = @Monto;
          end
          else if ( @Posicion = 24 )
          begin
               set @AuTipo24 = @Tipo;
               set @Monto24 = @Monto;
          end
          else if ( @Posicion = 25 )
          begin
               set @AuTipo25 = @Tipo;
               set @Monto25 = @Monto;
          end
          else if ( @Posicion = 26 )
          begin
               set @AuTipo26 = @Tipo;
               set @Monto26 = @Monto;
          end
          else if ( @Posicion = 27 )
          begin
               set @AuTipo27 = @Tipo;
               set @Monto27 = @Monto;
          end
          else if ( @Posicion = 28 )
          begin
               set @AuTipo28 = @Tipo;
               set @Monto28 = @Monto;
          end
          else if ( @Posicion = 29 )
          begin
               set @AuTipo29 = @Tipo;
               set @Monto29 = @Monto;
          end
          else if ( @Posicion = 30 )
          begin
               set @AuTipo30 = @Tipo;
               set @Monto30 = @Monto;
          end
          else if ( @Posicion = 31 )
          begin
               set @AuTipo31 = @Tipo;
               set @Monto31 = @Monto;
          end
          fetch NEXT from Temporal
          into @FechaActual,
               @Status,
               @TipoDia,
               @Horas,
               @Extras,
               @Dobles,
               @Triples,
               @Tipo,
               @Tardes,
               @Des_Tra,
               @Per_CG,
               @Per_SG;
     end
	 close Temporal;
	 deallocate Temporal;
     if ( @Cuantos > 0 )
     begin
          insert into DBO.TMPCALHR
         ( US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES, TL_CAMPO, TL_FEC_MIN, TL_FEC_MAX, TL_NUMERO,
           TL_CAMPO01, TL_CAMPO02, TL_CAMPO03, TL_CAMPO04, TL_CAMPO05, TL_CAMPO06, TL_CAMPO07, TL_CAMPO08, TL_CAMPO09, TL_CAMPO10,
           TL_CAMPO11, TL_CAMPO12, TL_CAMPO13, TL_CAMPO14, TL_CAMPO15, TL_CAMPO16, TL_CAMPO17, TL_CAMPO18, TL_CAMPO19, TL_CAMPO20,
           TL_CAMPO21, TL_CAMPO22, TL_CAMPO23, TL_CAMPO24, TL_CAMPO25, TL_CAMPO26, TL_CAMPO27, TL_CAMPO28, TL_CAMPO29, TL_CAMPO30, TL_CAMPO31,
           TL_TIPO01, TL_TIPO02, TL_TIPO03, TL_TIPO04, TL_TIPO05, TL_TIPO06, TL_TIPO07, TL_TIPO08, TL_TIPO09, TL_TIPO10,
           TL_TIPO11, TL_TIPO12, TL_TIPO13, TL_TIPO14, TL_TIPO15, TL_TIPO16, TL_TIPO17, TL_TIPO18, TL_TIPO19, TL_TIPO20,
           TL_TIPO21, TL_TIPO22, TL_TIPO23, TL_TIPO24, TL_TIPO25, TL_TIPO26, TL_TIPO27, TL_TIPO28, TL_TIPO29, TL_TIPO30, TL_TIPO31,
           TL_HABIL, TL_SABADO, TL_DESCAN, TL_NORMAL, TL_INCAPA, TL_VACA, TL_PER_CG, TL_PER_SG, TL_PER_FJ, TL_SUSPEN, TL_PER_OT, TL_FESTIVO,
           TL_NO_TRAB, TL_TRABAJO, TL_FALTAS, TL_RETARDO, TL_HORAS, TL_EXTRAS, TL_DOBLES, TL_DES_TRA, TL_HOR_CG, TL_HOR_SG, TL_TARDES )
       values
         ( @Usuario, @Empleado, @Anio, @Mes, @Campo, @Fecha_Min, @Fecha_Max, @Cuantos,
           @Monto01, @Monto02, @Monto03, @Monto04, @Monto05, @Monto06, @Monto07, @Monto08, @Monto09, @Monto10,
           @Monto11, @Monto12, @Monto13, @Monto14, @Monto15, @Monto16, @Monto17, @Monto18, @Monto19, @Monto20,
           @Monto21, @Monto22, @Monto23, @Monto24, @Monto25, @Monto26, @Monto27, @Monto28, @Monto29, @Monto30, @Monto31,
           @AuTipo01, @AuTipo02, @AuTipo03, @AuTipo04, @AuTipo05, @AuTipo06, @AuTipo07, @AuTipo08, @AuTipo09, @AuTipo10,
           @AuTipo11, @AuTipo12, @AuTipo13, @AuTipo14, @AuTipo15, @AuTipo16, @AuTipo17, @AuTipo18, @AuTipo19, @AuTipo20,
           @AuTipo21, @AuTipo22, @AuTipo23, @AuTipo24, @AuTipo25, @AuTipo26, @AuTipo27, @AuTipo28, @AuTipo29, @AuTipo30, @AuTipo31,
           @TotHabil, @TotSabado, @TotDescan, @TotNormal, @TotIncapa, @TotVaca, @TotPer_CG, @TotPer_SG, @TotPer_FJ, @TotSuspen, @TotPer_OT, @TotFestivo,
           @TotNo_Trab, @TotTrabajo, @TotFaltas, @TotRetardo, @TotHoras, @TotExtras, @TotDobles, @TotDes_Tra, @TotHor_CG, @TotHor_SG, @TotTardes );
     end;
end
GO

CREATE PROCEDURE SP_FECHA_TURNO
    				@FECHA DATETIME,
    				@EMPLEADO INTEGER,
					@CB_TURNO CHAR(6) OUTPUT
AS
begin
        SET NOCOUNT ON
	DECLARE TEMPORAL CURSOR FOR
		select CB_TURNO
		from KARDEX
		where ( CB_CODIGO = @Empleado ) AND
            ( CB_FECHA <= @Fecha  )
		order by CB_FECHA DESC, CB_NIVEL DESC
	OPEN TEMPORAL
	FETCH NEXT FROM TEMPORAL
	into @CB_TURNO

   	CLOSE TEMPORAL
   	DEALLOCATE TEMPORAL
end
GO

CREATE PROCEDURE DBO.SP_LISTADO_BALANZA (
  @USUARIO SMALLINT,
  @ANIO SMALLINT,
  @TIPO SMALLINT,
  @NUMERO SMALLINT,
  @EMPLEADO INTEGER,
  @CONCEPTO SMALLINT,
  @COLUMNA SMALLINT,
  @DESCRIPCION CHAR(30),
  @MONTO NUMERIC(15, 2),
  @HORAS NUMERIC(15, 2),
  @REFERENCIA VARCHAR(8),
  @TP_NIVEL1 CHAR(6),
  @TP_NIVEL2 CHAR(6),
  @TP_NIVEL3 CHAR(6),
  @TP_NIVEL4 CHAR(6),
  @TP_NIVEL5 CHAR(6)
)  AS
BEGIN
     SET NOCOUNT ON
     DECLARE @PERCEPCION SMALLINT;
     DECLARE @DEDUCCION SMALLINT;

     SELECT @PERCEPCION = COUNT(*) FROM DBO.TMPBALA2
     WHERE TP_USER = @USUARIO AND
           CB_CODIGO = @EMPLEADO AND
           TP_NIVEL1 = @TP_NIVEL1 AND
           TP_NIVEL2 = @TP_NIVEL2 AND
           TP_NIVEL3 = @TP_NIVEL3 AND
           TP_NIVEL4 = @TP_NIVEL4 AND
           TP_NIVEL5 = @TP_NIVEL5 AND
           TP_NUM_PER > 0

     SELECT @DEDUCCION=COUNT(*) FROM TMPBALA2
     WHERE TP_USER = @USUARIO AND
           CB_CODIGO = @EMPLEADO AND
           TP_NIVEL1 = @TP_NIVEL1 AND
           TP_NIVEL2 = @TP_NIVEL2 AND
           TP_NIVEL3 = @TP_NIVEL3 AND
           TP_NIVEL4 = @TP_NIVEL4 AND
           TP_NIVEL5 = @TP_NIVEL5 AND
           TP_NUM_DED > 0

     IF (@COLUMNA = 1) 
     BEGIN
          IF (@PERCEPCION >= @DEDUCCION)
          BEGIN
               INSERT INTO TMPBALA2 ( TP_ORDEN,
                                      CB_CODIGO,
                                      PE_YEAR,
                                      PE_TIPO,
                                      PE_NUMERO,
                                      TP_USER,
                                      TP_NUM_PER,
                                      TP_MON_PER,
                                      TP_DES_PER,
                                      TP_HOR_PER,
                                      TP_REF_PER,
                                      TP_NIVEL1,
                                      TP_NIVEL2,
                                      TP_NIVEL3,
                                      TP_NIVEL4,
                                      TP_NIVEL5)
                            VALUES( @PERCEPCION,
                                    @EMPLEADO,
                                    @ANIO,
                                    @TIPO,
                                    @NUMERO,
                                    @USUARIO,
                                    @CONCEPTO,
                                    @MONTO,
                                    @DESCRIPCION,
                                    @HORAS,
                                    @REFERENCIA,
                                    @TP_NIVEL1,
                                    @TP_NIVEL2,
                                    @TP_NIVEL3,
                                    @TP_NIVEL4,
                                    @TP_NIVEL5 );

          END
          ELSE
          BEGIN
               UPDATE DBO.TMPBALA2 SET
                      TP_NUM_PER=@CONCEPTO,
                      TP_MON_PER=@MONTO,
                      TP_DES_PER=@DESCRIPCION,
                      TP_HOR_PER=@HORAS,
                      TP_REF_PER=@REFERENCIA
               WHERE TP_USER=@USUARIO AND
                     CB_CODIGO=@EMPLEADO AND
                     TP_NIVEL1 = @TP_NIVEL1 AND
                     TP_NIVEL2 = @TP_NIVEL2 AND
                     TP_NIVEL3 = @TP_NIVEL3 AND
                     TP_NIVEL4 = @TP_NIVEL4 AND
                     TP_NIVEL5 = @TP_NIVEL5 AND
                     TP_ORDEN=@PERCEPCION;
          END
     END
     ELSE
     BEGIN
          IF (@DEDUCCION >= @PERCEPCION)
          BEGIN
               INSERT INTO DBO.TMPBALA2 ( TP_ORDEN,
                                      CB_CODIGO,
                                      PE_YEAR,
                                      PE_TIPO,
                                      PE_NUMERO,
                                      TP_USER,
                                      TP_NUM_DED,
                                      TP_MON_DED,
                                      TP_DES_DED,
                                      TP_HOR_DED,
                                      TP_REF_DED,
                                      TP_NIVEL1,
                                      TP_NIVEL2,
                                      TP_NIVEL3,
                                      TP_NIVEL4,
                                      TP_NIVEL5)
                            VALUES( @DEDUCCION,
                                    @EMPLEADO,
                                    @ANIO,
                                    @TIPO,
                                    @NUMERO,
                                    @USUARIO,
                                    @CONCEPTO,
                                    @MONTO,
                                    @DESCRIPCION,
                                    @HORAS,
                                    @REFERENCIA,
                                    @TP_NIVEL1,
                                    @TP_NIVEL2,
                                    @TP_NIVEL3,
                                    @TP_NIVEL4,
                                    @TP_NIVEL5 );
          END
          ELSE
          BEGIN
               UPDATE DBO.TMPBALA2 SET
                      TP_NUM_DED=@CONCEPTO,
                      TP_MON_DED=@MONTO,
                      TP_DES_DED=@DESCRIPCION,
                      TP_HOR_DED=@HORAS,
                      TP_REF_DED=@REFERENCIA
               WHERE TP_USER=@USUARIO AND
                     CB_CODIGO=@EMPLEADO AND
                     TP_NIVEL1 = @TP_NIVEL1 AND
                     TP_NIVEL2 = @TP_NIVEL2 AND
                     TP_NIVEL3 = @TP_NIVEL3 AND
                     TP_NIVEL4 = @TP_NIVEL4 AND
                     TP_NIVEL5 = @TP_NIVEL5 AND
                     TP_ORDEN=@DEDUCCION;
          END;
     END;
END
GO

CREATE PROCEDURE DBO.RECOMPILA_TODO
as
begin
     set NOCOUNT on
     declare @TABLA sysname
     declare TEMPORAL cursor for
     select TABLE_NAME from INFORMATION_SCHEMA.TABLES
     order by TABLE_NAME
     open TEMPORAL
     fetch next from TEMPORAL into @TABLA
     while ( @@FETCH_STATUS = 0 )
     begin
          exec SP_RECOMPILE @TABLA
          fetch next from TEMPORAL into @TABLA
     end
     close TEMPORAL
     deallocate TEMPORAL
end
GO

CREATE PROCEDURE DBO.INIT_EXPEDIENTE
as
begin
     BEGIN TRANSACTION TX1
     delete from CAMPO_EX
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX01', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX02', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX03', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX04', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX05', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX06', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX07', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX08', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX09', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX10', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX11', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX12', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX13', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX14', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX15', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX16', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX17', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX18', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX19', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX20', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX21', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX22', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX23', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX24', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX25', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX26', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX27', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX28', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX29', 0 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_TEX30', 0 )

     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG01', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG02', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG03', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG04', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG05', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG06', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG07', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG08', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG09', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG10', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG11', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG12', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG13', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG14', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG15', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG16', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG17', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG18', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG19', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG20', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG21', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG22', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG23', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG24', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG25', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG26', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG27', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG28', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG29', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG30', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG31', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG32', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG33', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG34', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG35', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG36', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG37', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG38', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG39', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG40', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG41', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG42', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG43', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG44', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG45', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG46', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG47', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG48', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG49', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG50', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG51', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG52', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG53', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG54', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG55', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG56', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG57', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG58', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG59', 2 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_LOG60', 2 )

     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM01', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM02', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM03', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM04', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM05', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM06', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM07', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM08', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM09', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM10', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM11', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM12', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM13', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM14', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM15', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM16', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM17', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM18', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM19', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM20', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM21', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM22', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM23', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM24', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM25', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM26', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM27', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM28', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM29', 1 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_NUM30', 1 )

     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL01', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL02', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL03', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL04', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL05', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL06', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL07', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL08', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL09', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL10', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL11', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL12', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL13', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL14', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL15', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL16', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL17', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL18', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL19', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL20', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL21', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL22', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL23', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL24', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL25', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL26', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL27', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL28', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL29', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL30', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL31', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL32', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL33', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL34', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL35', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL36', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL37', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL38', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL39', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL40', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL41', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL42', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL43', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL44', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL45', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL46', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL47', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL48', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL49', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL50', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL51', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL52', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL53', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL54', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL55', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL56', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL57', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL58', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL59', 3 )
     INSERT INTO CAMPO_EX( CX_NOMBRE, CX_TIPO ) VALUES ( 'EX_G_BOL60', 3 )
     COMMIT TRANSACTION TX1
end
GO

create procedure DBO.CHECK_REPORT_CHAIN( @PUESTO CHAR(6) )
as
begin
     SET NOCOUNT ON;
     declare @REPORTA CHAR(6);
     select @REPORTA = PU_REPORTA from PUESTO where ( PU_CODIGO = @PUESTO );
     while ( ( @REPORTA is not NULL ) and ( @REPORTA > '' ) )
     begin
          if ( @REPORTA = @PUESTO )
          begin
	       RAISERROR( 'La Cadena De Subordinaci�n Es Circular', 16, 1 )
               BREAK;
          end
          else
          begin
               select @REPORTA = PU_REPORTA from PUESTO where ( PU_CODIGO = @REPORTA );
          end
     end
end
GO

CREATE PROCEDURE ADICIONAL_INIT
as
begin
     SET NOCOUNT ON;
     BEGIN TRANSACTION TX1
     delete from CAMPO_AD

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_1', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_2', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_3', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_4', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_5', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_6', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_7', 4 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_FEC_8', 4 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_1', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_2', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_3', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_4', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_5', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_6', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_7', 2 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_LOG_8', 2 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_1', 5, 19 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_2', 5, 20 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_3', 5, 21 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_4', 5, 22 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_5', 5, 223 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_6', 5, 224 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_7', 5, 225 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_8', 5, 226 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB_9', 5, 227 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB10', 5, 228 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB11', 5, 229 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB12', 5, 230 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB13', 5, 231 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO, CX_ENTIDAD ) VALUES ( 'CB_G_TAB14', 5, 232 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_1', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_2', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_3', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_4', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_5', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_6', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_7', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_8', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX_9', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX10', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX11', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX12', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX13', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX14', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX15', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX16', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX17', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX18', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX19', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX20', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX21', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX22', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX23', 0 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_TEX24', 0 )

     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_1', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_2', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_3', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_4', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_5', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_6', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_7', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_8', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM_9', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM10', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM11', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM12', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM13', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM14', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM15', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM16', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM17', 1 )
     INSERT INTO CAMPO_AD( CX_NOMBRE, CX_TIPO ) VALUES ( 'CB_G_NUM18', 1 )
     COMMIT TRANSACTION TX1
end
GO


exec DBO.INIT_EXPEDIENTE
GO

exec DBO.ADICIONAL_INIT
GO

CREATE PROCEDURE SP_BORRA_DESCTIPO( @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from DESCTIPO where ( DT_ORDEN = @Actual );
    if ( @@ROWCOUNT > 0 )
    begin
	     update DESCTIPO set DT_ORDEN = DT_ORDEN - 1
	     where ( DT_ORDEN > @Actual );
    end
END
GO

CREATE PROCEDURE SP_CAMBIA_DESCTIPO( @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	declare @Maximo FolioChico;
	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( DT_ORDEN ) from DESCTIPO;
	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN;
	update DESCTIPO	set DT_ORDEN = 0 where ( DT_ORDEN = @Actual );
	update DESCTIPO	set DT_ORDEN = @Actual where ( DT_ORDEN = @Nuevo );
	update DESCTIPO set DT_ORDEN = @Nuevo where ( DT_ORDEN = 0 );
END
GO

CREATE PROCEDURE SP_BORRA_DESC_FLD( @DT_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from DESC_FLD where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = @Actual );
	if ( @@ROWCOUNT > 0 )
    begin
	     update DESC_FLD set DF_ORDEN = DF_ORDEN - 1
         where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN > @Actual );
    end
END
GO

CREATE PROCEDURE SP_CAMBIA_DESC_FLD( @DT_CODIGO Codigo, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	declare @Maximo FolioChico

	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( DF_ORDEN ) from DESC_FLD
    where ( DT_CODIGO = @DT_CODIGO );
	if ( ( @Nuevo > @Maximo ) or ( @Actual > @Maximo ) )
		RETURN;

	update DESC_FLD set DF_ORDEN = 0
    where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = @Actual );

	update DESC_FLD set DF_ORDEN = @Actual
    where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = @Nuevo );

	update DESC_FLD set DF_ORDEN = @Nuevo
    where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = 0 );

END
GO

CREATE PROCEDURE SP_BORRA_DESC_PTO( @PU_CODIGO Codigo, @DT_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from DESC_PTO where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = @Actual );
	if ( @@ROWCOUNT > 0 )
    begin
	     update DESC_PTO set DP_ORDEN = DP_ORDEN - 1 where
         ( PU_CODIGO = @PU_CODIGO ) and
         ( DT_CODIGO = @DT_CODIGO ) and
         ( DP_ORDEN > @Actual );
    end
END
GO

CREATE PROCEDURE SP_CAMBIA_DESC_PTO( @PU_CODIGO Codigo, @DT_CODIGO Codigo, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	declare @Maximo FolioChico;

	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( DP_ORDEN ) from DESC_PTO
	where ( PU_CODIGO = @PU_CODIGO ) and ( DT_CODIGO = @DT_CODIGO );
	if ( ( @Nuevo > @Maximo ) or ( @Actual > @Maximo ) )
		RETURN

	update DESC_PTO set DP_ORDEN = 0 where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = @Actual );

	update DESC_PTO set DP_ORDEN = @Actual where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = @Nuevo );

	update DESC_PTO set DP_ORDEN = @Nuevo where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = 0 );
END
GO

CREATE PROCEDURE SP_COPIA_PERFIL( @Anterior Codigo, @Nuevo Codigo )
AS
BEGIN
	SET NOCOUNT ON;
	insert into PERFIL( PU_CODIGO,
                        PF_ESTUDIO,
                        PF_DESCRIP,
                        PF_OBJETIV,
                        PF_FECHA,
                        PF_EXP_PTO,
                        PF_POSTING,
                        PF_CONTRL1,
                        PF_CONTRL2,
                        PF_NUMERO1,
                        PF_NUMERO2,
                        PF_TEXTO1,
                        PF_TEXTO2,
                        PF_EDADMIN,
                        PF_EDADMAX,
                        PF_SEXO )
	select @Nuevo,
           PF_ESTUDIO,
           PF_DESCRIP,
           PF_OBJETIV,
           PF_FECHA,
           PF_EXP_PTO,
           PF_POSTING,
           PF_CONTRL1,
           PF_CONTRL2,
           PF_NUMERO1,
           PF_NUMERO2,
           PF_TEXTO1,
           PF_TEXTO2,
           PF_EDADMIN,
           PF_EDADMAX,
           PF_SEXO
	from PERFIL where ( PU_CODIGO = @Anterior );

	insert into DESC_PTO( PU_CODIGO,
                          DT_CODIGO,
                          DP_ORDEN,
                          DP_TEXT_01,
                          DP_TEXT_02,
                          DP_TEXT_03,
                          DP_MEMO_01,
                          DP_MEMO_02,
                          DP_MEMO_03,
                          DP_NUME_01,
                          DP_FECH_01 )
	select @Nuevo,
           DT_CODIGO,
           DP_ORDEN,
           DP_TEXT_01,
           DP_TEXT_02,
           DP_TEXT_03,
           DP_MEMO_01,
           DP_MEMO_02,
           DP_MEMO_03,
           DP_NUME_01,
           DP_FECH_01
	from DESC_PTO where ( PU_CODIGO = @Anterior );
END
GO

create procedure SP_TOT_EMPLEADOS(
  @Usuario INTEGER,
  @Empleado INTEGER,
  @Resultado INTEGER OUTPUT )
as
begin
     SET NOCOUNT ON
     declare @TotalEmpleados INTEGER;
     select @TotalEmpleados = COUNT(*) from REP_EMPS where
     ( US_CODIGO = @Usuario ) and ( CB_CODIGO = @Empleado );
     if ( @TotalEmpleados = 0 )
     begin
          insert into REP_EMPS( US_CODIGO, CB_CODIGO )
          values( @Usuario, @Empleado );
     end;
     select @RESULTADO = COUNT(*) from REP_EMPS where ( US_CODIGO = @Usuario );
end
GO

CREATE PROCEDURE VAL_PREPARA_PUNTOS(
	@VP_FOLIO	FolioGrande,
	@VL_CODIGO	FolioChico )
AS
BEGIN
	SET NOCOUNT ON
	delete from VPUNTOS
	where  VP_FOLIO = @VP_FOLIO

	insert into VPUNTOS ( VP_FOLIO, VT_ORDEN, VT_CUAL )
	select	@VP_FOLIO, VS_ORDEN, 0
	from	VSUBFACT
	where	VL_CODIGO = @VL_CODIGO
	order by VS_ORDEN
END
GO

CREATE PROCEDURE VAL_COPIA_PUESTO(
	@VP_FOLIO	FolioGrande,
	@PU_CODIGO	Codigo,
	@VP_FECHA	Fecha,
	@US_CODIGO	Usuario,
	@NewFolio	FolioGrande OUTPUT )
AS
BEGIN
	SET NOCOUNT ON

	/* Copia el encabezado */
	/* Sobreescribe el Puesto, la fecha y el usuario */
	insert into VAL_PTO ( VL_CODIGO, PU_CODIGO, VP_FECHA, VP_COMENTA, VP_GRADO,
							VP_PT_CODE, VP_PT_NOM, VP_PT_GRAD, VP_FEC_INI, VP_FEC_FIN, US_CODIGO, VP_STATUS )
	select VL_CODIGO, @PU_CODIGO, @VP_FECHA, VP_COMENTA, VP_GRADO,
							VP_PT_CODE, VP_PT_NOM, VP_PT_GRAD, VP_FEC_INI, VP_FEC_FIN, @US_CODIGO, 0
	from	VAL_PTO
	where	VP_FOLIO = @VP_FOLIO

	set @NewFolio = @@IDENTITY

	/* Copia la puntuaci�n de los subfactores incluyendo comentarios */
	insert into VPUNTOS ( VP_FOLIO, VT_ORDEN, VT_CUAL, VT_COMENTA )
	select	@NewFolio, VT_ORDEN, VT_CUAL, VT_COMENTA
	from	VPUNTOS
	where	VP_FOLIO = @VP_FOLIO
	order by VT_ORDEN
END
GO

CREATE PROCEDURE VAL_CAMBIA_FACTOR( @VL_CODIGO FolioChico, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Maximo FolioChico

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
		RETURN

	select @Maximo = MAX( VF_ORDEN )
	from   VFACTOR
	where  VL_CODIGO = @VL_CODIGO

	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN

	UPDATE VFACTOR
	SET    VF_ORDEN = 0
	WHERE  VL_CODIGO = @VL_CODIGO and VF_ORDEN = @Actual

	UPDATE VFACTOR
	SET    VF_ORDEN = @Actual
	WHERE  VL_CODIGO = @VL_CODIGO and VF_ORDEN = @Nuevo

	UPDATE VFACTOR
	SET    VF_ORDEN = @Nuevo
	WHERE  VL_CODIGO = @VL_CODIGO and VF_ORDEN = 0
END
GO

CREATE PROCEDURE VAL_CAMBIA_SUBFACTOR( @VL_CODIGO FolioChico, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Maximo FolioChico

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
		RETURN

	select @Maximo = MAX( VS_ORDEN )
	from   VSUBFACT
	where  VL_CODIGO = @VL_CODIGO

	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN

	UPDATE VSUBFACT
	SET    VS_ORDEN = 0
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @Actual

	UPDATE VSUBFACT
	SET    VS_ORDEN = @Actual
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @Nuevo

	UPDATE VSUBFACT
	SET    VS_ORDEN = @Nuevo
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = 0
END
GO

CREATE PROCEDURE VAL_CAMBIA_NIVEL( @VL_CODIGO FolioChico, @VS_ORDEN FolioChico, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Maximo FolioChico

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
		RETURN

	select @Maximo = MAX( VN_ORDEN )
	from   VALNIVEL
	where  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @VS_ORDEN

	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN

	UPDATE VALNIVEL
	SET    VN_ORDEN = 0
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @VS_ORDEN and VN_ORDEN = @Actual

	UPDATE VALNIVEL
	SET    VN_ORDEN = @Actual
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @VS_ORDEN and VN_ORDEN = @Nuevo

	UPDATE VALNIVEL
	SET    VN_ORDEN = @Nuevo
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @VS_ORDEN and VN_ORDEN = 0
END
GO

CREATE PROCEDURE VAL_BORRA_FACTOR( @VL_CODIGO FolioChico, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM VFACTOR
	WHERE  VL_CODIGO = @VL_CODIGO and VF_ORDEN = @Actual

	IF ( @@ROWCOUNT = 0 )
		RETURN

	UPDATE VFACTOR
	SET    VF_ORDEN = VF_ORDEN-1
	WHERE  VL_CODIGO = @VL_CODIGO and VF_ORDEN > @Actual
END
GO

CREATE PROCEDURE VAL_BORRA_SUBFACTOR( @VL_CODIGO FolioChico, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM VSUBFACT
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @Actual

	IF ( @@ROWCOUNT = 0 )
		RETURN

	UPDATE VSUBFACT
	SET    VS_ORDEN = VS_ORDEN-1
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN > @Actual
END
GO

CREATE PROCEDURE VAL_BORRA_NIVEL( @VL_CODIGO FolioChico, @VS_ORDEN FolioChico, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM VALNIVEL
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @VS_ORDEN and VN_ORDEN = @Actual

	IF ( @@ROWCOUNT = 0 )
		RETURN

	UPDATE VALNIVEL
	SET    VN_ORDEN = VN_ORDEN-1
	WHERE  VL_CODIGO = @VL_CODIGO and VS_ORDEN = @VS_ORDEN and VN_ORDEN > @Actual
END
GO

CREATE PROCEDURE SP_BORRA_REGISTRO(@Tabla NombreCampo, @Campo NombreCampo, @Codigo NombreCampo, @Llave FolioChico, @Actual FolioChico  )
AS
BEGIN
	SET NOCOUNT ON;

	declare @ParamDefinition nVarchar(255);
	declare @Delete nVarChar(255);
	declare @Update nVarChar(255);
	set @ParamDefinition = N'@CodigoLLave FolioChico, @Codigo FolioChico';
	set @Delete =  N'delete from '+ @Tabla + ' where ' + @Codigo + '=@CodigoLlave and ' + @Campo + '=@Codigo';
	set @Update =  N'Update ' + @Tabla + ' set ' + @Campo + '='+ @Campo + '-1' + ' where ' + @Codigo + '=@CodigoLlave and ' + @Campo + '>=@Codigo';

	EXECUTE sp_executesql 	@Delete, @ParamDefinition, @CodigoLlave=@Llave, @Codigo = @Actual

	if ( @@ROWCOUNT > 0 )
   	begin
		EXECUTE sp_executesql 	@Update, @ParamDefinition, @CodigoLlave=@Llave, @Codigo = @Actual
	end
END
GO

CREATE PROCEDURE SP_CAMBIA_ORDEN_CODIGO (@Tabla NombreCampo, @Campo NombreCampo , @CampoLlave NombreCampo,  @Llave FolioChico, @Actual FolioChico, @Nuevo FolioChico ) AS
BEGIN
	set NOCOUNT ON;

	declare @Max FolioChico;

	declare @ParamDefinition nVarchar(255);
	declare @Script nVarChar(800);
	declare @Update1 nVarChar(255);
	declare @Update2 nVarChar(255);
	declare @Update3 nVarChar(255);



	set @Script =   N'select @Maximo = MAX( ' + @Campo + ') '+
					N'from   ' + @Tabla + ' where '+ @CampoLlave +' = @Codigo' ;

	set @Update1 = 	N'UPDATE ' + @Tabla +
					N' SET    ' + @Campo + '= -1 '+
					N'WHERE  '+ @CampoLlave +' = @Llave and ' + @Campo + '= @Actual ';

	set @Update2 =  N'UPDATE ' + @Tabla +
					N' SET    ' + @Campo + '= @Actual '+
					N'WHERE  '+ @CampoLlave +' = @Llave and ' + @Campo + '= @Nuevo ';

	set @Update3 = 	N'UPDATE ' + @Tabla +
					N' SET    ' + @Campo + '= @Nuevo '+
					N'WHERE  '+ @CampoLlave +' = @Llave and ' + @Campo + '= -1 ';

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
	   RETURN

	set @ParamDefinition =N'@Codigo FolioChico, @Maximo FolioChico output';
	EXECUTE sp_executesql @Script, @ParamDefinition, @Codigo=@Llave, @Maximo=@Max output;

	if ( @Nuevo > @Max ) or ( @Actual > @Max )
		RETURN

	set @ParamDefinition = N'@Llave FolioChico,@Actual FolioChico,@Nuevo FolioChico';
	EXECUTE sp_executesql 	@Update1, @ParamDefinition, @Llave, @Actual,@Nuevo;
	EXECUTE sp_executesql 	@Update2, @ParamDefinition, @Llave, @Actual,@Nuevo;
	EXECUTE sp_executesql 	@Update3, @ParamDefinition, @Llave, @Actual,@Nuevo;

END
GO

CREATE PROCEDURE SP_CAMBIA_ORDEN (@Tabla NombreCampo, @Campo NombreCampo, @Actual FolioChico, @Nuevo FolioChico ) AS
BEGIN
	set NOCOUNT ON;

	declare @Max FolioChico;

	declare @ParamDefinition nVarchar(255);
	declare @Script nVarChar(800);
	declare @Update1 nVarChar(255);
	declare @Update2 nVarChar(255);
	declare @Update3 nVarChar(255);

	set @Script =   N'select @Maximo = MAX( ' + @Campo + ') '+
					N'from   ' + @Tabla  ;

	set @Update1 = 	N'UPDATE ' + @Tabla +
					N' SET    ' + @Campo + '= -1 '+
					N'WHERE  '+ @Campo + '= @Actual ';

	set @Update2 =  N'UPDATE ' + @Tabla +
					N' SET    ' + @Campo + '= @Actual '+
					N'WHERE  '+ @Campo + '= @Nuevo ';

	set @Update3 = 	N'UPDATE ' + @Tabla +
					N' SET    ' + @Campo + '= @Nuevo '+
					N'WHERE  '+ @Campo + '= -1 ';

	IF ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo )
	   RETURN

	set @ParamDefinition =N'@Maximo FolioChico output';
	EXECUTE sp_executesql @Script, @ParamDefinition, @Maximo=@Max output;

	if ( @Nuevo > @Max ) or ( @Actual > @Max )
		RETURN

	set @ParamDefinition = N'@Actual FolioChico,@Nuevo FolioChico';
	EXECUTE sp_executesql 	@Update1, @ParamDefinition, @Actual,@Nuevo;
	EXECUTE sp_executesql 	@Update2, @ParamDefinition, @Actual,@Nuevo;
	EXECUTE sp_executesql 	@Update3, @ParamDefinition, @Actual,@Nuevo;

END
GO

CREATE PROCEDURE SP_VALIDA_CURSO_ASIG( @CB_CODIGO NumeroEmpleado, 
							   @CU_CODIGO Codigo,
							   @CB_PUESTO Codigo, 
							   @CB_CLASIFI Codigo, 
							   @CB_TURNO Codigo, 
							   @KC_EVALUA Pesos,
							   @KC_FEC_TOM Fecha,
							   @KC_HORAS Pesos, 							    
							   @CB_NIVEL1 Codigo, 
							   @CB_NIVEL2 Codigo,
							   @CB_NIVEL3 Codigo, 
							   @CB_NIVEL4 Codigo, 
							   @CB_NIVEL5 Codigo, 
							   @CB_NIVEL6 Codigo, 
						        @CB_NIVEL7 Codigo,
							   @CB_NIVEL8 Codigo, 
							   @CB_NIVEL9 Codigo, 
							   @MA_CODIGO Codigo, 
							   @KC_FEC_FIN Fecha, 
						        @KC_REVISIO NombreCampo, 
							   @SE_FOLIO Codigo, 
							   @MensajeError Formula OUTPUT,
							   @Status Codigo1 OUTPUT )
AS
BEGIN
	SET NOCOUNT ON	
	/* Siempre manda status 0 ( Proceder ) y ningun mensaje de error en la base instalada*/

	set @Status = 0
	set @MensajeError = ''
	
END
GO

CREATE PROCEDURE SP_VALIDA_CAMPOS_SESION( @MA_CODIGO Codigo,
								  @SE_LUGAR Observaciones,
								  @SE_FEC_INI Fecha,
								  @SE_HOR_INI Hora,
								  @SE_FEC_FIN Fecha,
								  @SE_HOR_FIN Hora,
								  @SE_HORAS Horas,
								  @SE_CUPO FolioChico,
								  @SE_STATUS Status,
								  @SE_COSTO1 Pesos,
								  @SE_COSTO2 Pesos,
								  @SE_COSTO3 Pesos,
								  @SE_COMENTA Observaciones, 
 								  @SE_REVISIO NombreCampo,	
							       @MensajeError Formula OUTPUT,
							       @Status Codigo1 OUTPUT )
AS
BEGIN
	SET NOCOUNT ON	
	/* Siempre manda status 0 ( Proceder ) y ningun mensaje de error en la base instalada*/

	set @Status = 0
	set @MensajeError = ''
	
END
GO

CREATE procedure Get_CursosProgramados 
@CB_CODIGO int
as

SELECT CP.CU_CODIGO, CP.KC_FEC_PRO, CP.KC_EVALUA, 
CP.KC_FEC_TOM, CP.KC_REVISIO, CP.KC_HORAS, 
CP.CU_HORAS, CP.EN_OPCIONA, CP.KC_PROXIMO, C.CU_NOMBRE
from CUR_PROG CP
left outer join CURSO C 
	on( CP.CU_CODIGO = C.CU_CODIGO )
left outer join COLABORA 
	on( CP.CB_CODIGO = COLABORA.CB_CODIGO ) 
where( CP.CB_CODIGO = @CB_CODIGO ) 
	and ((CP.EN_LISTA = 'N') 
	or (COLABORA.CB_CLASIFI in (select ET_CODIGO from ENTNIVEL 
					where (ENTNIVEL.PU_CODIGO = CP.CB_PUESTO) 
					and (ENTNIVEL.CU_CODIGO = CP.CU_CODIGO))))
					order by KC_FEC_PRO 

GO

CREATE PROCEDURE SP_GET_DETALLE_AHORROS
@CB_CODIGO INT,
@LLAVE INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE
	@TIPODENTRONOMINA 	CHAR(1),
	@TIPOFUERANOMINA 	CHAR(1),
	@PERIODOSTATUS		SMALLINT,
	@MOVIMIENTOACTIVO	CHAR(1),
	@USUARIO_PORTAL		INT,
	@US_CODIGO			INT,
	@AHORRO_TIPO		CHAR(1)

	SELECT 	@USUARIO_PORTAL	= 5000,
			@TIPODENTRONOMINA='M',
			@TIPOFUERANOMINA ='A',
			@PERIODOSTATUS =6,
			@MOVIMIENTOACTIVO='S',
			@US_CODIGO=-1

	DECLARE
	@L_CB_CODIGO 	INT,
	@L_AH_TIPO 		CHAR(1),
	@L_AH_FECHA 	DATETIME,
	@TB_CONCEPT 	NUMERIC(15,2),
	@L_AH_SALDO_I 	NUMERIC(15,2),
	@L_AH_SALDO 	NUMERIC(15,2),
	@L_AH_MONTO 	NUMERIC(15,2),
	@L_AH_REFEREN 	VARCHAR(8),
	@L_US_CODIGO	SMALLINT,
	@L_AH_ABONOS	NUMERIC(15,2),
	@L_AH_CARGOS	NUMERIC(15,2),
	@L_AH_TOTAL		NUMERIC(15,2),
	@L_AH_NUMERO	SMALLINT,
	@L_AH_STATUS 	SMALLINT,
	@L_AH_STATUS_DES VARCHAR(8),

	@D_CB_CODIGO	INT,
	@D_TE_TIPO		CHAR(1),
	@D_TE_REFEREN	VARCHAR(8),
	@D_PE_FEC_INI	DATETIME,
	@D_MO_PERCEPC	NUMERIC(15,2),
	@D_MO_DEDUCCI	NUMERIC(15,2),
	@D_US_CODIGO	SMALLINT,
	@D_PE_YEAR		SMALLINT,
	@D_PE_TIPO		CHAR(1),
	@D_PE_NUMERO	SMALLINT,
	@D_CO_NUMERO	SMALLINT,
	@D_TIPO			CHAR(1),

	@TE_CODIGO		INT,
	@TE_CODIGO_DET	INT ,
	@SALDO			NUMERIC(15,2),
	@DESCRIPCION 	VARCHAR(30),
	@NOMINA_DESC	VARCHAR(30),
	@REFERENCIA		INT,
	@ALTERN_ROW		INT,
	@FECHA_PERIODO	DATETIME

	SELECT @AHORRO_TIPO=AH_TIPO FROM AHORRO WHERE LLAVE= @LLAVE
	SELECT	@TE_CODIGO=0,@TE_CODIGO_DET=0,@SALDO=0,@REFERENCIA=1,@ALTERN_ROW=0

	DELETE FROM TMPESTAD WHERE TE_USER = @USUARIO_PORTAL
	CREATE TABLE #TMP_AHORROS
	(
		[TE_USER] 		SMALLINT NOT NULL ,
		[TE_CODIGO] 	SMALLINT NOT NULL ,
		[TE_FECHA] 		DATETIME NOT NULL ,
		[TE_CARGO] 		NUMERIC(15,2) NOT NULL ,
		[TE_ABONO] 		NUMERIC(15,2) NOT NULL ,
		[TE_SALDO] 		NUMERIC(15,2) NOT NULL ,
		[US_CODIGO] 	SMALLINT NOT NULL ,
		[TE_DESCRIP] 	VARCHAR(30) NOT NULL ,
		[CB_CODIGO] 	INT NOT NULL ,
		[TE_TIPO] 		CHAR(6) NOT NULL ,
		[TE_REFEREN] 	VARCHAR(8) NOT NULL,
		[A_ANIOS_ANT] 	NUMERIC(15,2) NOT NULL ,
		[OTROS_ABONOS] 	NUMERIC(15,2) NOT NULL ,
		[OTROS_CARGOS] 	NUMERIC(15,2) NOT NULL ,
		[SALDO_ACTUAL] 	NUMERIC(15,2) NOT NULL ,
		[AH_TOTAL] 		NUMERIC(15,2) NOT NULL ,
		[AH_NUMERO] 	SMALLINT NOT NULL ,
		[AH_STATUS] 	SMALLINT NOT NULL ,
		[AH_STATUS_DES] VARCHAR(10),
		[TIPO_REGISTRO] VARCHAR(10),
		[ALTERN_ROW] 	INT
	)
		SELECT @FECHA_PERIODO=(SELECT MAX(PE.PE_FEC_FIN) MAXIMO
								    FROM PERIODO PE
									INNER JOIN NOMINA N
										ON PE.PE_YEAR=N.PE_YEAR
										AND PE.PE_TIPO=N.PE_TIPO
										AND PE.PE_NUMERO=N.PE_NUMERO
									INNER JOIN TURNO T
										ON N.CB_TURNO=T.TU_CODIGO
									WHERE PE.PE_STATUS = @PERIODOSTATUS
										AND N.CB_CODIGO= @CB_CODIGO)

	DECLARE  LISTA_AHORROS_CURSOR CURSOR FOR
		SELECT 	A.CB_CODIGO,		A.AH_TIPO,		A.AH_FECHA,
				T.TB_CONCEPT,	 	A.AH_SALDO_I,	A.AH_SALDO,	0 AS 'AH_MONTO',
				'' AS AH_REFEREN,	A.US_CODIGO,	A.AH_ABONOS,
	        	A.AH_CARGOS,		A.AH_TOTAL,		A.AH_NUMERO,
				A.AH_STATUS,
				'A.AH_STATUS_DES'=
				CASE
					WHEN A.AH_STATUS=0 THEN 'Activo'
					WHEN A.AH_STATUS=1 THEN 'Saldado'
					WHEN A.AH_STATUS=2 THEN 'Cancelado'
				END
		FROM AHORRO A
		INNER JOIN TAHORRO T
			ON  A.AH_TIPO= T.TB_CODIGO
		WHERE 	A.CB_CODIGO=@CB_CODIGO
			AND A.AH_TIPO=@AHORRO_TIPO
			AND A.AH_FECHA<(@FECHA_PERIODO)
			AND ((A.AH_ABONOS+AH_CARGOS<>0)
			OR (A.AH_NUMERO<>0 ))
		ORDER BY A.CB_CODIGO,
				 A.AH_TIPO

	OPEN LISTA_AHORROS_CURSOR

	FETCH NEXT FROM LISTA_AHORROS_CURSOR
	INTO 	@L_CB_CODIGO ,	@L_AH_TIPO,		@L_AH_FECHA,	@TB_CONCEPT ,
			@L_AH_SALDO_I,	@L_AH_SALDO,	@L_AH_MONTO,  	@L_AH_REFEREN,
			@L_US_CODIGO,	@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_TOTAL,
			@L_AH_NUMERO,	@L_AH_STATUS,	@L_AH_STATUS_DES

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT  @TE_CODIGO	=@TE_CODIGO	+ 1,
				@SALDO		=@L_AH_MONTO + @L_AH_SALDO_I

		INSERT INTO TMPESTAD (TE_CODIGO,	CB_CODIGO,		TE_TIPO,		TE_FECHA,
							 TE_DESCRIP,	TE_CARGO,		TE_ABONO,		TE_SALDO,
							 US_CODIGO,		TE_USER,		TE_REFEREN )
					VALUES 	(@TE_CODIGO, 	@L_CB_CODIGO,	@L_AH_TIPO,		@L_AH_FECHA,
							'SALDO INICIAL',0, 	@L_AH_SALDO_I,	@SALDO,
							@US_CODIGO, 	@USUARIO_PORTAL, @REFERENCIA)

	INSERT INTO #TMP_AHORROS(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
							TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,
							US_CODIGO,		TE_USER,		TE_REFEREN,		A_ANIOS_ANT,
							OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	AH_TOTAL,
							AH_NUMERO,		AH_STATUS,		AH_STATUS_DES,	TIPO_REGISTRO,
							ALTERN_ROW)
					VALUES	(@TE_CODIGO, 	@L_CB_CODIGO, 	@L_AH_TIPO,		@L_AH_FECHA,
							'SALDO INICIAL',0, 	0,				@SALDO,
							@US_CODIGO, 	@USUARIO_PORTAL,@REFERENCIA,	@L_AH_SALDO_I,
							@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_SALDO,	@L_AH_TOTAL,
							@L_AH_NUMERO,	@L_AH_STATUS ,	@L_AH_STATUS_DES,'ENCABEZADO',
							@ALTERN_ROW)
		DECLARE MOVIMIENTOS_AHORRO_CURSOR CURSOR FOR
			SELECT 	TMPESTAD.CB_CODIGO,
					TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					PERIODO.PE_FEC_INI PE_FEC_INI,
					MOVIMIEN.MO_PERCEPC MO_PERCEPC,
					MOVIMIEN.MO_DEDUCCI MO_DEDUCCI,
					MOVIMIEN.US_CODIGO,
					NOMINA.PE_YEAR,
					NOMINA.PE_TIPO,
					NOMINA.PE_NUMERO,
					MOVIMIEN.CO_NUMERO,
					@TIPODENTRONOMINA AS TIPO
			FROM TMPESTAD
			LEFT OUTER JOIN NOMINA
				ON NOMINA.CB_CODIGO = TMPESTAD.CB_CODIGO
			LEFT OUTER JOIN PERIODO
				ON PERIODO.PE_YEAR = NOMINA.PE_YEAR
				AND PERIODO.PE_TIPO = 	NOMINA.PE_TIPO
				AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
			LEFT OUTER JOIN MOVIMIEN
				ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR
				AND NOMINA.PE_TIPO = 	MOVIMIEN.PE_TIPO
				AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO
				AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
			WHERE ( TMPESTAD.TE_USER = @USUARIO_PORTAL)
			AND ( TMPESTAD.US_CODIGO = @US_CODIGO )
			AND ( MOVIMIEN.CB_CODIGO = @CB_CODIGO)
			AND  ( MOVIMIEN.CO_NUMERO = @TB_CONCEPT )
			AND  ( NOMINA.NO_STATUS	=	@PERIODOSTATUS)
			AND ( MOVIMIEN.MO_ACTIVO = @MOVIMIENTOACTIVO)
			AND  ( PERIODO.PE_FEC_FIN >= TMPESTAD.TE_FECHA )
			AND TMPESTAD.TE_REFEREN=@REFERENCIA
			UNION ALL
            SELECT	TMPESTAD.CB_CODIGO,
                  	TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					ACAR_ABO.CR_FECHA PE_FEC_INI,
					ACAR_ABO.CR_CARGO MO_PERCEPC,
					ACAR_ABO.CR_ABONO MO_DEDUCCI,
					ACAR_ABO.US_CODIGO,
					CAST(0 AS SMALLINT) PE_YEAR,
					CAST(0 AS SMALLINT) PE_TIPO,
					CAST(0 AS SMALLINT) PE_NUMERO,
					CAST(0 AS SMALLINT) CO_NUMERO,
					@TIPOFUERANOMINA AS TIPO
            FROM TMPESTAD
            LEFT OUTER JOIN ACAR_ABO
				ON ACAR_ABO.CB_CODIGO = TMPESTAD.CB_CODIGO
                AND ACAR_ABO.AH_TIPO = TMPESTAD.TE_TIPO
			WHERE TMPESTAD.TE_USER = @USUARIO_PORTAL
            	AND TMPESTAD.US_CODIGO = @US_CODIGO
				AND (ACAR_ABO.CB_CODIGO=@CB_CODIGO)
                AND (ACAR_ABO.CR_CARGO +ACAR_ABO.CR_ABONO)>0
                AND TMPESTAD.TE_FECHA <= (@FECHA_PERIODO)
            ORDER BY 1,2,3,4

		   	OPEN MOVIMIENTOS_AHORRO_CURSOR
		   	FETCH NEXT FROM MOVIMIENTOS_AHORRO_CURSOR
			INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,
					@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
					@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO

			   WHILE @@FETCH_STATUS = 0
			   BEGIN

				SELECT 	@DESCRIPCION='',
						@NOMINA_DESC=(select TP_NOMBRE from tperiodo WHERE TP_TIPO=@D_PE_TIPO)

				IF(@D_TIPO=@TIPODENTRONOMINA)
				BEGIN
					SELECT @DESCRIPCION =@NOMINA_DESC + ' # ' +
										 CONVERT(VARCHAR(30),@D_PE_NUMERO)+ ' A�o: ' +
										 CONVERT(VARCHAR(30),@D_PE_YEAR)
				END
				ELSE
				BEGIN
					IF((@D_MO_DEDUCCI=NULL) OR (@D_MO_DEDUCCI=0))
					BEGIN
						SELECT @DESCRIPCION='Cargo'
					END
					ELSE
					BEGIN
						SELECT @DESCRIPCION='Abono'
					END
				END

				if(@D_MO_DEDUCCI<>0)
				BEGIN
					SELECT @SALDO=(@SALDO+@D_MO_DEDUCCI)
				END
				ELSE
				BEGIN
					SELECT @SALDO=(@SALDO-@D_MO_PERCEPC)
				END

				SELECT @TE_CODIGO=@TE_CODIGO+1

				IF(@ALTERN_ROW=0)
				BEGIN
					SELECT @ALTERN_ROW=1
				END
				ELSE
				BEGIN
					SELECT @ALTERN_ROW=0
				END

				INSERT INTO #TMP_AHORROS(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
										TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,
										US_CODIGO,		TE_USER,		TE_REFEREN,		A_ANIOS_ANT,
										OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	AH_TOTAL,
										AH_NUMERO,		AH_STATUS,		AH_STATUS_DES,	TIPO_REGISTRO,
										ALTERN_ROW)
								VALUES(	@TE_CODIGO,		@D_CB_CODIGO, 	@D_TE_TIPO,		@D_PE_FEC_INI,
										@DESCRIPCION, 	@D_MO_PERCEPC, 	@D_MO_DEDUCCI,	@SALDO,
										@US_CODIGO,		@USUARIO_PORTAL,@D_TE_REFEREN,	0,
										0,				0,				0,				0,
										0,				0,				'',				'GRID',
										@ALTERN_ROW)

			FETCH NEXT FROM MOVIMIENTOS_AHORRO_CURSOR
			INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,
					@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
					@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO

			END

			CLOSE MOVIMIENTOS_AHORRO_CURSOR
			DEALLOCATE MOVIMIENTOS_AHORRO_CURSOR

   	FETCH NEXT FROM LISTA_AHORROS_CURSOR
	INTO 	@L_CB_CODIGO ,	@L_AH_TIPO,		@L_AH_FECHA,	@TB_CONCEPT ,
			@L_AH_SALDO_I,	@L_AH_SALDO,	@L_AH_MONTO,  	@L_AH_REFEREN,
			@L_US_CODIGO,	@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_TOTAL,
			@L_AH_NUMERO,	@L_AH_STATUS,	@L_AH_STATUS_DES

	END

	CLOSE LISTA_AHORROS_CURSOR
	DEALLOCATE LISTA_AHORROS_CURSOR

	SELECT * FROM #TMP_AHORROS
	DROP TABLE #TMP_AHORROS
END
GO

CREATE PROCEDURE SP_GET_DETALLES_PRESTAMOS
	@CB_CODIGO INT,
	@LLAVE INT
AS
BEGIN
	SET NOCOUNT ON

	/* CONSTANTES para los Querys*/
	DECLARE
	@TIPODENTRONOMINA 	CHAR(1),
	@TIPOFUERANOMINA 	CHAR(1),
	@PERIODOSTATUS		SMALLINT,
	@MOVIMIENTOACTIVO	CHAR(1),
	@USUARIO_PORTAL		INT,
	@US_CODIGO			INT,
	@PRE_TIPO		CHAR(1),
	@PRE_REFEREN	CHAR(8)

	/*Se inicializan las constantes*/
	SELECT 	@USUARIO_PORTAL		= 5000,
			@TIPODENTRONOMINA	='M',
			@TIPOFUERANOMINA 	='P',
			@PERIODOSTATUS 		=6,
			@MOVIMIENTOACTIVO	='S',
			@US_CODIGO			=-1

	/* VARIABLES en las que se almacena la informaci�n que se va a procesar en el cursor*/

	DECLARE
	/*Variables del cursor LISTA_PRESTAMOS_CURSOR*/
	@L_CB_CODIGO 	INT,
	@L_PR_TIPO 		CHAR(1),
	@L_PR_FECHA 	DATETIME,
	@TB_CONCEPT 	NUMERIC(15,2),
	@L_PR_SALDO_I 	NUMERIC(15,2),
	@L_PR_SALDO 	NUMERIC(15,2),
	@L_PR_MONTO 	NUMERIC(15,2),
	@L_PR_REFEREN 	VARCHAR(8),
	@L_US_CODIGO	SMALLINT,
	@L_PR_ABONOS	NUMERIC(15,2),
	@L_PR_CARGOS	NUMERIC(15,2),
	@L_PR_TOTAL		NUMERIC(15,2),
	@L_PR_NUMERO	SMALLINT,
	@L_PR_STATUS 	SMALLINT,
	@L_PR_STATUS_DES VARCHAR(8),

	/*Variables del cursor MOVIMIENTOS_PRESTAMO_CURSOR*/
	@D_CB_CODIGO	INT,
	@D_TE_TIPO		CHAR(1),
	@D_TE_REFEREN	VARCHAR(8),
	@D_PE_FEC_INI	DATETIME,
	@D_MO_PERCEPC	NUMERIC(15,2),
	@D_MO_DEDUCCI	NUMERIC(15,2),
	@D_US_CODIGO	SMALLINT,
	@D_PE_YEAR		SMALLINT,
	@D_PE_TIPO		CHAR(1),
	@D_PE_NUMERO	SMALLINT,
	@D_CO_NUMERO	SMALLINT,
	@D_TIPO			CHAR(1),

	/*Variables utilizadas para los calculos*/
	@TE_CODIGO		INT,
	@TE_CODIGO_DET	INT ,
	@SALDO			NUMERIC(15,2),
	@DESCRIPCION 	VARCHAR(30),
	@NOMINA_DESC	VARCHAR(30),
	@ALTERN_ROW		INT,
	@FECHA_PERIODO	DATETIME


	/*Se inicializan las variables de calculos*/
	SELECT @PRE_TIPO=PR_TIPO, @PRE_REFEREN=PR_REFEREN FROM PRESTAMO WHERE LLAVE= @LLAVE
	SELECT	@TE_CODIGO=0,@TE_CODIGO_DET=0,@SALDO=0,@ALTERN_ROW=0
/*----------------------------------------------------------------------------------*/
/*	1.- SE ELIMINAN TODOS LOS REGISTROS DE LA TABLA TEMPESTAD DEL USUARIO DE PORTAL */
/*----------------------------------------------------------------------------------*/
	DELETE FROM TMPESTAD WHERE TE_USER = @USUARIO_PORTAL
/*----------------------------------------------------------------------------------*/
/*	2.- SE CREA TABLA TEMPORAL PARA REPORTE FINAL                                   */
/*----------------------------------------------------------------------------------*/

CREATE TABLE #TMP_PRESTAMOS
(
	[TE_USER] 		SMALLINT NOT NULL ,
	[TE_CODIGO] 	SMALLINT NOT NULL ,
	[TE_FECHA] 		DATETIME NOT NULL ,
	[TE_CARGO] 		NUMERIC(15,2) NOT NULL ,
	[TE_ABONO] 		NUMERIC(15,2) NOT NULL ,
	[TE_SALDO] 		NUMERIC(15,2) NOT NULL ,
	[US_CODIGO] 	SMALLINT NOT NULL ,
	[TE_DESCRIP] 	VARCHAR(30) NOT NULL ,
	[CB_CODIGO] 	INT NOT NULL ,
	[TE_TIPO] 		CHAR(6) NOT NULL ,
	[TE_REFEREN]	VARCHAR(8) NOT NULL,
	[P_ANIOS_ANT] 	NUMERIC(15,2) NOT NULL ,
	[OTROS_ABONOS] 	NUMERIC(15,2) NOT NULL ,
	[OTROS_CARGOS] 	NUMERIC(15,2) NOT NULL ,
	[SALDO_ACTUAL] 	NUMERIC(15,2) NOT NULL ,
	[PR_TOTAL] 		NUMERIC(15,2) NOT NULL ,
	[PR_NUMERO] 	SMALLINT NOT NULL ,
	[PR_STATUS] 	SMALLINT NOT NULL ,
	[PR_STATUS_DES] VARCHAR(10),
	[TIPO_REGISTRO] VARCHAR(10),
	[ALTERN_ROW] 	INT
)
/*----------------------------------------------------------------------------*/
/*	3.- SE OBTIENE LA FECHA DEL ULTIMO PERIODO PARA EL EMPLEADO               */
/*----------------------------------------------------------------------------*/
		SELECT @FECHA_PERIODO=(SELECT MAX(PE.PE_FEC_FIN) MAXIMO
								    FROM PERIODO PE
									INNER JOIN NOMINA N
										ON PE.PE_YEAR=N.PE_YEAR
										AND PE.PE_TIPO=N.PE_TIPO
										AND PE.PE_NUMERO=N.PE_NUMERO
									INNER JOIN TURNO T
										ON N.CB_TURNO=T.TU_CODIGO
									WHERE PE.PE_STATUS = @PERIODOSTATUS
										AND N.CB_CODIGO= @CB_CODIGO)
/*------------------------------------------------------------------------------*/
/*	4.- CURSOR PARA OBTENER EL LISTADO DE LOS PRESTAMOS DEL EMPLEADO            */
/*------------------------------------------------------------------------------*/
	DECLARE  LISTA_PRESTAMOS_CURSOR CURSOR FOR
		SELECT 	P.CB_CODIGO,	P.PR_TIPO,		P.PR_FECHA,		T.TB_CONCEPT,
				P.PR_SALDO_I,	P.PR_SALDO,		P.PR_MONTO,		P.PR_REFEREN,
				P.US_CODIGO,	P.PR_ABONOS,	P.PR_CARGOS,	P.PR_TOTAL,
				P.PR_NUMERO,	P.PR_STATUS,
				'PR_STATUS_DES'=
				CASE
					WHEN PR_STATUS=0 THEN 'Activo'
					WHEN PR_STATUS=1 THEN 'Saldado'
					WHEN PR_STATUS=2 THEN 'Cancelado'
				END
		FROM PRESTAMO P
		INNER JOIN TPRESTA T
			ON  P.PR_TIPO= T.TB_CODIGO
		WHERE P.CB_CODIGO=@CB_CODIGO
			AND P.PR_TIPO=@PRE_TIPO
			AND P.PR_REFEREN=@PRE_REFEREN
			AND PR_STATUS=0
			AND P.PR_FECHA<(@FECHA_PERIODO)
			AND ((P.PR_ABONOS+PR_CARGOS<>0)
			OR (P.PR_NUMERO<>0 ))
	OPEN LISTA_PRESTAMOS_CURSOR

	FETCH NEXT FROM LISTA_PRESTAMOS_CURSOR
	INTO 	@L_CB_CODIGO,	@L_PR_TIPO ,	@L_PR_FECHA,	@TB_CONCEPT ,	@L_PR_SALDO_I ,
			@L_PR_SALDO ,	@L_PR_MONTO,	@L_PR_REFEREN,	@L_US_CODIGO,	@L_PR_ABONOS,
			@L_PR_CARGOS,	@L_PR_TOTAL,	@L_PR_NUMERO,	@L_PR_STATUS,	@L_PR_STATUS_DES

	WHILE @@FETCH_STATUS = 0
	BEGIN

	SELECT  @TE_CODIGO=@TE_CODIGO+1,
			@SALDO=@L_PR_MONTO - @L_PR_SALDO_I


	/*Se inserta un registro por cada Prestamo del empleado en la tabla TempStad*/
	INSERT INTO TMPESTAD(TE_CODIGO,		CB_CODIGO,		TE_TIPO,	TE_FECHA,
						TE_DESCRIP,		TE_CARGO, 		TE_ABONO,	TE_SALDO,
						US_CODIGO, 		TE_USER, 		TE_REFEREN)
				VALUES (@TE_CODIGO, 	@L_CB_CODIGO, 	@L_PR_TIPO,	@L_PR_FECHA,
			'SALDO INICIAL',		0, 	0,			@SALDO,
						@US_CODIGO, 	@USUARIO_PORTAL,@L_PR_REFEREN)

	/*Se inserta registro en la tabla temporal para reporte final, este registro contiene los totales del prestamo*/
	INSERT INTO #TMP_PRESTAMOS
						(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
						TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,
						US_CODIGO,		TE_USER,		TE_REFEREN,		P_ANIOS_ANT,
						OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	PR_TOTAL,
						PR_NUMERO,		PR_STATUS,		PR_STATUS_DES,	TIPO_REGISTRO,
						ALTERN_ROW)

				VALUES(@TE_CODIGO, 		@L_CB_CODIGO,	@L_PR_TIPO,		@L_PR_FECHA,
	'SALDO INICIAL',0, 	0,				@SALDO,
						@US_CODIGO,		@USUARIO_PORTAL,@L_PR_REFEREN,	@L_PR_SALDO_I,
						@L_PR_ABONOS,	@L_PR_CARGOS,	@L_PR_SALDO,	@L_PR_TOTAL,
						@L_PR_NUMERO,	@L_PR_STATUS,	@L_PR_STATUS_DES,'ENCABEZADO',
						@ALTERN_ROW)

	/*---------------------------------------------------------------------------------------*/
  /*  4.- CURSOR PARA OBTENER EL DETALLE DEL PRESTAMO REVISADO(MOVIMIENTOS_PRESTAMO_CURSOR)*/
	/*---------------------------------------------------------------------------------------*/
		DECLARE MOVIMIENTOS_PRESTAMO_CURSOR CURSOR FOR
			SELECT 	TMPESTAD.CB_CODIGO,
					TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					PERIODO.PE_FEC_INI PE_FEC_INI,
					MOVIMIEN.MO_PERCEPC MO_PERCEPC,
					MOVIMIEN.MO_DEDUCCI MO_DEDUCCI,
					MOVIMIEN.US_CODIGO,
					NOMINA.PE_YEAR,
					NOMINA.PE_TIPO,
					NOMINA.PE_NUMERO,
					MOVIMIEN.CO_NUMERO,
					@TIPODENTRONOMINA AS TIPO
			FROM TMPESTAD
			LEFT OUTER JOIN NOMINA
				ON NOMINA.CB_CODIGO = TMPESTAD.CB_CODIGO
			LEFT OUTER JOIN PERIODO
				ON PERIODO.PE_YEAR = NOMINA.PE_YEAR
				AND PERIODO.PE_TIPO = 	NOMINA.PE_TIPO
				AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
			LEFT OUTER JOIN MOVIMIEN
				ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR
				AND NOMINA.PE_TIPO = 	MOVIMIEN.PE_TIPO
				AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO
				AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
			WHERE ( TMPESTAD.TE_USER = @USUARIO_PORTAL)
			AND ( TMPESTAD.US_CODIGO = @US_CODIGO )
			AND ( MOVIMIEN.CB_CODIGO = @CB_CODIGO)
			AND  ( MOVIMIEN.CO_NUMERO = @TB_CONCEPT )
			AND  ( NOMINA.NO_STATUS	=	@PERIODOSTATUS)
			AND  ( MOVIMIEN.MO_REFEREN= TMPESTAD.TE_REFEREN )
			AND ( MOVIMIEN.MO_ACTIVO = @MOVIMIENTOACTIVO)
			AND  ( PERIODO.PE_FEC_FIN >= TMPESTAD.TE_FECHA )
			AND TMPESTAD.TE_REFEREN=@L_PR_REFEREN
			 UNION ALL
			SELECT TMPESTAD.CB_CODIGO,TMPESTAD.TE_TIPO,TMPESTAD.TE_REFEREN,
				PCAR_ABO.CR_FECHA PE_FEC_INI,
				PCAR_ABO.CR_CARGO MO_PERCEPC,
				PCAR_ABO.CR_ABONO MO_DEDUCCI,
				PCAR_ABO.US_CODIGO,
				CAST(0 AS SMALLINT) PE_YEAR,
				CAST(0 AS SMALLINT) PE_TIPO,
				CAST(0 AS SMALLINT) PE_NUMERO,
				CAST(0 AS SMALLINT) CO_NUMERO,
				@TIPOFUERANOMINA AS TIPO
			FROM TMPESTAD
			LEFT OUTER JOIN PCAR_ABO
				ON PCAR_ABO.CB_CODIGO = TMPESTAD.CB_CODIGO
				AND PCAR_ABO.PR_TIPO = TMPESTAD.TE_TIPO
				AND PCAR_ABO.PR_REFEREN = TMPESTAD.TE_REFEREN
			WHERE TMPESTAD.TE_USER = @USUARIO_PORTAL
			AND TMPESTAD.US_CODIGO = @US_CODIGO
			AND (PCAR_ABO.CR_CARGO +PCAR_ABO.CR_ABONO)>0
			AND TMPESTAD.TE_FECHA <= (@FECHA_PERIODO)
			AND TMPESTAD.TE_REFEREN=@L_PR_REFEREN
			ORDER BY 1,2,3,4

		OPEN MOVIMIENTOS_PRESTAMO_CURSOR
		FETCH NEXT FROM MOVIMIENTOS_PRESTAMO_CURSOR
		INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,
				@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
				@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO

			WHILE @@FETCH_STATUS = 0
			BEGIN

			/*Se valida que tipo de movimiento se realizo y se obtiene su descripcion*/

				SELECT 	@DESCRIPCION='',
						@NOMINA_DESC=(select TP_NOMBRE from tperiodo WHERE TP_TIPO=@D_PE_TIPO)

				if(@D_TIPO=@TipoDentroNomina)
				begin
					SELECT @DESCRIPCION =@NOMINA_DESC + ' # ' +
										 CONVERT(VARCHAR(30),@D_PE_NUMERO)+ ' A�o: ' +
										 CONVERT(VARCHAR(4),@D_PE_YEAR)
				end
				else
				begin
					IF((@D_MO_DEDUCCI=NULL) OR (@D_MO_DEDUCCI=0))
					BEGIN
						SELECT @DESCRIPCION='Cargo'
					END
					ELSE
					BEGIN
						SELECT @DESCRIPCION='Abono'
					END
				end

				/*Se calcula el saldo del prestamo*/
				if(@D_MO_DEDUCCI<>0)
				BEGIN
					SELECT @SALDO=(@SALDO-@D_MO_DEDUCCI)
				END
				ELSE
				BEGIN
					SELECT @SALDO=(@SALDO+@D_MO_PERCEPC)
				END

				/* Se incrementa para ordenar los registros */
				SELECT @TE_CODIGO=@TE_CODIGO+1

				/* Se indica valor para alternar los renglones del grid*/
				IF(@ALTERN_ROW=0)
				BEGIN
					SELECT @ALTERN_ROW=1
				END
				ELSE
				BEGIN
					SELECT @ALTERN_ROW=0
				END

				/*Se inserta un registro en la tabla TempStad por cada Movimiento del prestamo en revision*/
				INSERT INTO #TMP_PRESTAMOS
								(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
								TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,
								US_CODIGO,		TE_USER,		TE_REFEREN,		P_ANIOS_ANT,
								OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	PR_TOTAL,
								PR_NUMERO,		PR_STATUS,		PR_STATUS_DES,	TIPO_REGISTRO,
								ALTERN_ROW)
						VALUES (@TE_CODIGO,		@D_CB_CODIGO,	@D_TE_TIPO,		@D_PE_FEC_INI,
								@DESCRIPCION, 	@D_MO_PERCEPC, 	@D_MO_DEDUCCI,	@SALDO,
								@US_CODIGO,		@USUARIO_PORTAL,@D_TE_REFEREN,	0,
								0,				0,				0,				0,
								0,				0,				'',				'GRID',
								@ALTERN_ROW)

	    	FETCH NEXT FROM MOVIMIENTOS_PRESTAMO_CURSOR
					INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,
							@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
							@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO

		   END

			CLOSE MOVIMIENTOS_PRESTAMO_CURSOR
			DEALLOCATE MOVIMIENTOS_PRESTAMO_CURSOR
   /*-----------------Termina MOVIMIENTOS_PRESTAMO_CURSOR-------------------------------------*/
   /* Get the next Prestamo.*/
	FETCH NEXT FROM LISTA_PRESTAMOS_CURSOR 
	INTO 	@L_CB_CODIGO,	@L_PR_TIPO ,	@L_PR_FECHA,	@TB_CONCEPT ,	@L_PR_SALDO_I ,
			@L_PR_SALDO ,	@L_PR_MONTO,	@L_PR_REFEREN,	@L_US_CODIGO,	@L_PR_ABONOS,
			@L_PR_CARGOS,	@L_PR_TOTAL,	@L_PR_NUMERO,	@L_PR_STATUS,	@L_PR_STATUS_DES 


	END

	/* Close the LISTA_PRESTAMOS_CURSOR */
	CLOSE LISTA_PRESTAMOS_CURSOR
	DEALLOCATE LISTA_PRESTAMOS_CURSOR

	/*Se envia el reporte de los prestamos con su detalle y se elimina la tabla temporal*/
	SELECT * FROM #TMP_PRESTAMOS
	DROP TABLE #TMP_PRESTAMOS
END
GO

CREATE PROCEDURE SP_AVG_ENTREV_VACANT
(
	@CB_CLIENTE varchar(30),
	@CB_ESTATUS varchar(30),
	@FECHA_INI varchar(30),
	@FECHA_FIN varchar(30),
	@SERVIDOR VARCHAR(1000)
)
AS
BEGIN

	SET @CB_CLIENTE = CASE @CB_CLIENTE  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CB_CLIENTE  END
	SET @CB_ESTATUS = CASE @CB_ESTATUS  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CB_ESTATUS  END
	
	EXEC('
	Declare @Actualizar Table
	(
		PROMEDIO int,
		PROMEDIO2 int
	)
	Declare @Vacantes varchar(30)
	Declare @Entrevista varchar(30)

	declare TmpCosto cursor for
	select R.RQ_VACANTE,COUNT(E.RQ_FOLIO) Entrevista
	from '+@SERVIDOR+'.DBO.ENTREVIS E
	left outer join '+@SERVIDOR+'.DBO.REQUIERE R on (R.RQ_FOLIO=E.RQ_FOLIO)
	left outer join '+@SERVIDOR+'.DBO.PUESTO P on (P.PU_CODIGO=R.RQ_PUESTO)
	where ( RQ_CLIENTE LIKE LTRIM(RTRIM('''+@CB_CLIENTE+''')))
	and( RQ_STATUS LIKE LTRIM(RTRIM('''+@CB_ESTATUS+''')))
	and(RQ_FEC_INI >= '''+@FECHA_INI+''' AND RQ_FEC_FIN <= '''+@FECHA_FIN+''' ) 
	group by E.RQ_FOLIO,P.PU_DESCRIP,R.RQ_FEC_INI,R.RQ_VACANTE

	Open TmpCosto
		Fetch Next From TmpCosto Into 
		@Vacantes,
		@Entrevista

		while @@Fetch_Status = 0
		begin
				insert into @Actualizar (PROMEDIO,PROMEDIO2) values(@Vacantes,@Entrevista) 

			Fetch Next From TmpCosto Into
			@Vacantes,
			@Entrevista
		end
	  close TmpCosto
	  deallocate TmpCosto
		select sum(PROMEDIO2)/cast(sum(PROMEDIO)as decimal (5,2)) as PROMEDIO from @Actualizar
	')

END
GO

CREATE PROCEDURE SP_MOTIVO_VACANTE(
	@CB_CLIENTE varchar(30),
	@CB_PUESTO varchar(30),
	@SERVIDOR varchar(1000)
)
AS
BEGIN
	
	DECLARE @QUERY char(1000);


IF @CB_CLIENTE = '-100' AND @CB_PUESTO = '-100'
	BEGIN
		SELECT @QUERY = '';
	END

ELSE IF @CB_CLIENTE <>'-100'
	BEGIN
		SET @QUERY = 'WHERE RQ_CLIENTE='''+@CB_CLIENTE+''' '
	END

ELSE IF @CB_PUESTO <> '-100' 
	BEGIN
		SET @QUERY = 'WHERE RQ_PUESTO='''+@CB_PUESTO+''' '
	END

ELSE
	BEGIN
		SET @QUERY = 'WHERE RQ_CLIENTE='''+@CB_CLIENTE+''' AND RQ_PUESTO='''+@CB_PUESTO+''''
	END

	exec('
	Declare @MotivoVacante Table
	(	
	MOTIVO integer,
	CANTIDAD Integer
	)

	DECLARE @Valor Integer;	
	DECLARE @Contador Integer;
	SET @Contador = 0

	WHILE @Contador <=1
	BEGIN
		SELECT @Valor = ( SUM( CASE WHEN RQ_MOTIVO = @Contador THEN 1 ELSE 0 END ) ) 
		FROM '+@SERVIDOR+'.DBO.REQUIERE
		'+@QUERY+' 

		INSERT INTO @MotivoVacante values ( @Contador,@Valor);
		SET @Contador = @Contador+1 
	END
		SELECT MV.*, DBO.TF( 94, MV.MOTIVO) DESCRIPCION from @MotivoVacante MV
')

END
GO

CREATE  PROCEDURE SP_VACANTES_AC (
	@SERVIDOR varchar(1000)
)
AS
BEGIN
	exec('
	Declare @MotivoVacante Table
	(
	MOTIVO integer,
	CANTIDAD Integer
	)

	DECLARE @Valor Integer;
	DECLARE @Contador Integer;
	SET @Contador = 0

	WHILE @Contador <=2
	BEGIN
		SELECT @Valor = ( SUM( CASE WHEN RQ_STATUS = @Contador THEN 1 ELSE 0 END ) )
		FROM '+@SERVIDOR+'.DBO.REQUIERE

		INSERT INTO @MotivoVacante values ( @Contador,@Valor);
		SET @Contador = @Contador+1
	END
		SELECT MV.*, DBO.TF( 93, MV.MOTIVO) DESCRIPCION from @MotivoVacante MV
')
END
GO

CREATE PROCEDURE SP_EST_MOTIVOS_AUTORIZA_EXT (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and AUSENCIA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
		select case when A.CH_RELOJ = '''' then ''Sin Motivo'' else M.TB_ELEMENT end Motivo,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 1)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Enero,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 2)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Febrero,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 3)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Marzo,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 4)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Abril,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 5)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Mayo,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 6)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Junio,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 7)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Julio,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 8)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Agosto,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''')
		and (datepart( mm, B.AU_FECHA ) = 9)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Septiembre,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 10)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Octubre,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 11)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Noviembre,
		(
		select SUM(B.CH_HOR_EXT)
		from CHECADAS B
		left outer join AUSENCIA on (AUSENCIA.CB_CODIGO=B.CB_CODIGO and AUSENCIA.AU_FECHA=B.AU_FECHA)
		where B.CH_TIPO>=7
		and (datepart( yyyy, B.AU_FECHA ) = '''+@YEAR+''' )
		and (datepart( mm, B.AU_FECHA ) = 12)
		and A.CH_RELOJ=B.CH_RELOJ
		and ( AUSENCIA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		) Diciembre

		from CHECADAS A
		left outer join MOT_AUTO M on (M.TB_CODIGO=A.CH_RELOJ)
		where A.CH_TIPO>=7
		and (datepart( yyyy, A.AU_FECHA ) = '''+@YEAR+''' )
		group by A.CH_RELOJ,M.TB_ELEMENT
		order by A.CH_RELOJ,M.TB_ELEMENT
	')
END
GO

CREATE PROCEDURE SP_EST_TIEMPO_EXTRA (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and AUSENCIA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
		select 
		datepart( mm, au_fecha ) N�mero, 
		DBO.TF( 61, datepart( mm, au_fecha )-1 ) Mes,
		SUM(AU_NUM_EXT) Trabajadas, 
		SUM(AU_AUT_EXT) Autorizadas,
		SUM(AU_DOBLES) Dobles,
		SUM(AU_TRIPLES) Triples
		from AUSENCIA
		where (datepart( yyyy, AU_FECHA ) = '''+@YEAR+''')
		and ( CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')))
		'+@QUERY+'
		group by datepart( mm, AU_FECHA )
		order by datepart( mm, AU_FECHA )
	')
END
GO

CREATE PROCEDURE SP_GRAF_TOTALES_HORAS_DIAS (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT,
@HORAS VARCHAR(30)
)
AS
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
		select VA.AC_MES, DBO.TF( 61, VA.AC_MES-1 ) Mes,
			( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES and (VP.CO_NUMERO LIKE LTRIM(RTRIM('''+@HORAS+'''))) and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) '+@QUERY+'  ) MONTO
		from V_ACUMULA VA
		where (VA.AC_YEAR = '''+@YEAR+''')
		and VA.AC_MES<>13
		group by VA.AC_YEAR, VA.AC_MES
	')
END
GO

CREATE PROCEDURE SP_TOTALES_DIAS_AUSENTISMO (
		@CLASIFI VARCHAR(30),
		@NIVEL VARCHAR(30),
		@LISTA VARCHAR(30),
		@YEAR INT
	)
	AS
	BEGIN

		DECLARE @QUERY VARCHAR(8000);
		DECLARE @EJECUCION VARCHAR(8000);
		DECLARE @CLASIFIQUERY VARCHAR(8000);

		IF @NIVEL <> '-100' AND @LISTA <> '-100'
		BEGIN
			SET @NIVEL = 'CB_NIVEL' + @NIVEL
			SELECT @QUERY = ' and COLABORA.' + @NIVEL + ' = ''' + @LISTA + ''''
		END
		ELSE
			SELECT @QUERY = '';

		IF @CLASIFI <> '-100' and @CLASIFI <> '-1' and @CLASIFI is not null
			SELECT @CLASIFIQUERY = ' and COLABORA.CB_CLASIFI = ''' + @CLASIFI + ''' '
		ELSE
			SELECT @CLASIFIQUERY = '';

		set @EJECUCION = '
		select VA.AC_MES, DBO.TF( 61, VA.AC_MES-1 ) Mes,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1107 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) Trabajadas,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1100 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) FI,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1103 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoSG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1102 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoCG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1113 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) VAC,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1106 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) INC
			from V_ACUMULA VA
			where (VA.AC_YEAR = ' + convert(varchar(4), @YEAR) + ')
			and VA.AC_MES<>13
			group by VA.AC_YEAR, VA.AC_MES
			union
			select 14, ''Total'',
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1107 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) Trabajadas,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1100 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) FI,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1103 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoSG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1102 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoCG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1113 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) VAC,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1106 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) INC
			from V_ACUMULA VA
			where (VA.AC_YEAR = ' + convert(varchar(4), @YEAR) + ')
			and VA.AC_MES<>13
			group by VA.AC_YEAR'

		EXEC(@EJECUCION)
	END
GO

CREATE PROCEDURE SP_TOTALES_HORAS (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@YEAR INT
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END

IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
	select VA.AC_MES, DBO.TF( 61, VA.AC_MES-1 ) Mes,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1201 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Trabajadas,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1206 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Retardo,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1203 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtDobles,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1204 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtTriples,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1207 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Descanso
	from V_ACUMULA VA
	where (VA.AC_YEAR = '''+@YEAR+''' )
	and VA.AC_MES<>13
	group by VA.AC_YEAR, VA.AC_MES
	union
	select 14, ''Total'',
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1201 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Trabajadas,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1206 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Retardo,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1203 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtDobles,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1204 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) ExtTriples,
		( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1207 and (CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))) 	'+@QUERY+'  ) Descanso
	from V_ACUMULA VA 
	left outer join COLABORA ON (COLABORA.CB_CODIGO=VA.CB_CODIGO)
	where (VA.AC_YEAR = '''+@YEAR+''' )
	and VA.AC_MES<>13
	group by VA.AC_YEAR
	')
END
GO

CREATE PROCEDURE SP_COMBO_LISTADINAMICA
(
	@NIVEL VARCHAR(30)
)
AS
BEGIN

	IF @NIVEL<>'-100'
	BEGIN
		SET @NIVEL = CASE @NIVEL WHEN '1' THEN 'NIVEL1' WHEN '2' THEN 'NIVEL2' WHEN '3' THEN 'NIVEL3' WHEN '4' THEN 'NIVEL4' 
		WHEN '5' THEN 'NIVEL5' WHEN '6' THEN 'NIVEL6' ELSE @NIVEL END

		EXEC('SELECT TB_CODIGO, TB_ELEMENT FROM '+@NIVEL+' where TB_ACTIVO = ''S'' ')
	END
	ELSE
	BEGIN
		Select TB_CODIGO = -1 , TB_ELEMENT=''
	END

END
GO

CREATE PROCEDURE SP_NIVELES_DINAMICOS (
@CLASIFI VARCHAR(30),
@NIVEL VARCHAR(30),
@LISTA VARCHAR(30),
@NOMINA VARCHAR(30)
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CLASIFI = CASE @CLASIFI  WHEN '-100' THEN @TMPCLAS WHEN '-1' THEN @TMPCLAS WHEN null THEN @TMPCLAS WHEN '' THEN @TMPCLAS ELSE @CLASIFI END
	SET @NIVEL = CASE @NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @NIVEL END


IF @NIVEL <> '-100' AND @LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@NIVEL+' = '''+@LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
	select ACUMULA.CO_NUMERO,CO_DESCRIP, sum(AC_ANUAL) as Percepciones, CAST(0 AS SMALLINT) as deducciones from ACUMULA
	left outer join CONCEPTO on (ACUMULA.CO_NUMERO= CONCEPTO.CO_NUMERO)
	left outer join COLABORA on (ACUMULA.CB_CODIGO=COLABORA.CB_CODIGO)
	where (AC_YEAR LIKE LTRIM(RTRIM('''+@NOMINA+'''))) and CONCEPTO.CO_TIPO=1
	and CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+'''))
	'+@QUERY+' 
	group by ACUMULA.CO_NUMERO,CO_DESCRIP

	UNION ALL

	select ACUMULA.CO_NUMERO, CO_DESCRIP,CAST(0 AS SMALLINT),sum(AC_ANUAL) from ACUMULA
	left outer join CONCEPTO on (ACUMULA.CO_NUMERO= CONCEPTO.CO_NUMERO)
	left outer join COLABORA on (ACUMULA.CB_CODIGO=COLABORA.CB_CODIGO)
	where (AC_YEAR LIKE LTRIM(RTRIM('''+@NOMINA+'''))) and CONCEPTO.CO_TIPO=2
	and CB_CLASIFI LIKE LTRIM(RTRIM('''+@CLASIFI+''')) 
	'+@QUERY+'
	group by ACUMULA.CO_NUMERO,CO_DESCRIP

	order by 1
	')
END
GO

CREATE PROCEDURE SP_TOTALES_NOMINA(
@CB_LIQUIDA VARCHAR(30),
@CB_TURNO VARCHAR(30),
@CB_CLASIFI VARCHAR(30),
@CB_NIVEL VARCHAR(30),
@CB_LISTA VARCHAR(30),
@YEAR VARCHAR(30),
@PERIODO VARCHAR(30),
@TIPO VARCHAR(30)
)
AS  
BEGIN

	DECLARE @TMPCLAS char(30);
	DECLARE @QUERY char(1000);
	
	SELECT @TMPCLAS = MIN(TB_CODIGO) FROM CLASIFI;
	
	SET @CB_LIQUIDA = CASE @CB_LIQUIDA  WHEN '-100' THEN 0 WHEN '-1' THEN 0 WHEN null THEN 0 WHEN '' THEN 0 ELSE @CB_LIQUIDA END
	SET @CB_TURNO = CASE @CB_TURNO  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @CB_TURNO END
	SET @CB_CLASIFI = CASE @CB_CLASIFI  WHEN '-100' THEN @TMPCLAS WHEN '-1' THEN @TMPCLAS WHEN null THEN @TMPCLAS WHEN '' THEN @TMPCLAS ELSE @CB_CLASIFI END
	SET @CB_NIVEL = CASE @CB_NIVEL  WHEN '1' THEN 'CB_NIVEL1' WHEN '2' THEN 'CB_NIVEL2' WHEN '3' THEN 'CB_NIVEL3' WHEN '4' THEN 'CB_NIVEL4' WHEN '5' THEN 'CB_NIVEL5' WHEN '6' THEN 'CB_NIVEL6'
	ELSE @CB_NIVEL END

IF @CB_NIVEL <> '-100' AND @CB_LISTA <> '-100' 
	BEGIN
		SELECT @QUERY = ' and COLABORA.'+@CB_NIVEL+' = '''+@CB_LISTA+''''
	END

ELSE
	BEGIN
		 SELECT @QUERY = '';
	END

	exec('
	select NOMINA.CB_CODIGO,PRETTYNAME,NOMINA.CB_SALARIO, NO_PERCEPC, NO_DEDUCCI, NO_NETO,NO_HORAS,
	NO_DOBLES, NO_TRIPLES, NO_DES_TRA, NO_DIAS_VA,NO_DIAS_IN, NO_DIAS_AG
	from NOMINA
	left outer join COLABORA on (NOMINA.CB_CODIGO=COLABORA.CB_CODIGO)
	where PE_YEAR='''+@YEAR+''' and PE_NUMERO='''+@PERIODO+''' and PE_TIPO='''+@TIPO+'''
	and ( NO_LIQUIDA LIKE LTRIM(RTRIM('''+@CB_LIQUIDA+''')))
	and  ( NOMINA.CB_TURNO LIKE LTRIM(RTRIM('''+@CB_TURNO+''' )))
	and  ( NOMINA.CB_CLASIFI LIKE LTRIM(RTRIM('''+@CB_CLASIFI+''' )))
	'+@QUERY+'
	')
END
GO

CREATE PROCEDURE SP_EST_VISITAS_MENS (
@YEAR INT,
@SERVIDOR VARCHAR(1000)
)
AS
BEGIN


	exec('
		select 1 as NUMERO,''Enero'' as MES,count(*) as TOTAL from  '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 1)
		union all
		select 2,''Febrero'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 2)
		union all
		select 3,''Marzo'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 3)
		union all
		select 4,''Abril'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 4)
		union all
		select 5,''Mayo'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 5)
		union all
		select 6,''Junio'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 6)
		union all
		select 7,''Julio'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 7)
		union all
		select 8,''Agosto'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 8)
		union all
		select 9,''Septiembre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 9)
		union all
		select 10,''Octubre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 10)
		union all
		select 11,''Noviembre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 11)
		union all
		select 12,''Diciembre'',count(*) from '+@SERVIDOR+'.LIBRO B
		where (datepart( yyyy, B.LI_ENT_FEC ) >='''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC ) <= '''+@YEAR+''')
		and (datepart( mm, B.LI_ENT_FEC ) = 12)
		')
END
GO

CREATE PROCEDURE SP_EST_VISITAS_ASUNTO (
@YEAR INT,
@SERVIDOR VARCHAR(1000)
)
AS
BEGIN

	exec('
		select A.LI_ASUNTO as Codigo,T.TB_ELEMENT as Asunto,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 1)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Enero,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 2)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Febrero,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 3)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Marzo,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 4)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Abril,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 5)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Mayo,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 6)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Junio,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 7)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Julio,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 8)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Agosto,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 9)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Septiembre,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 10)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Octubre,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 11)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Noviembre,
		(
			select COUNT(*)
			from '+@SERVIDOR+'.LIBRO B
 			where(datepart( yyyy, B.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, B.LI_ENT_FEC )  <= '''+@YEAR+''' )
			and (datepart( mm, B.LI_ENT_FEC ) = 12)
			and A.LI_ASUNTO=B.LI_ASUNTO
		) Diciembre
		from '+@SERVIDOR+'.LIBRO A
		left outer join '+@SERVIDOR+'.TASUNTO T on (T.TB_CODIGO=A.LI_ASUNTO)
		where (datepart( yyyy, A.LI_ENT_FEC ) >= '''+@YEAR+''' and datepart( yyyy, A.LI_ENT_FEC )  <= '''+@YEAR+''' )
		group by T.TB_ELEMENT,A.LI_ASUNTO
		')
END
GO

CREATE PROCEDURE RECALCULA_INFONAVIT(@Empleado NumeroEmpleado) AS
BEGIN
	SET NOCOUNT ON
	declare @CB_INFTIPO Status
	declare @CB_INFCRED	Descripcion
	declare @CB_INFTASA	DescINFO	
	declare @CB_INFDISM Booleano
	declare @KI_FECHA Fecha
	declare @KI_TIPO Status
	declare @CB_INF_ANT Fecha	
	
	declare @Old_InfTipo Status
	set @Old_InfTipo = 0

	declare @Old_Infcred Descripcion
	set @Old_Infcred = ''

	declare @Old_Inftasa DescINFO
	set @Old_Inftasa = 0
	
	declare @Old_AplicaDis Booleano
	set @Old_AplicaDis = 'N'

	declare @FecIniCredito Fecha
	set @FecIniCredito = '12/30/1899'

	declare @CB_INFACT Booleano
	set @CB_INFACT = 'N' 

	declare @Old_INF_ANT Fecha
	set @Old_INF_ANT = '12/30/1899'


	declare CursorInfonavit CURSOR FOR
	select CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INFDISM,
		  KI_FECHA, KI_TIPO, CB_INF_ANT
 	from KARINF
	where CB_CODIGO = @Empleado
	order by KI_FECHA

	Open CursorInfonavit
	Fetch NEXT from CursorInfonavit
	into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO, @CB_INF_ANT
	

	if ( @KI_TIPO <> 0 )
	BEGIN
		Close CursorInfonavit
		Deallocate CursorInfonavit
		RAISERROR( 'Empleado #%d, ERROR en Kardex Infonavit: Movimiento(s) previo(s) a la alta de Infonavit', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
		/* Inicio */
		if ( @KI_TIPO in ( 0  ) ) 
		begin
			set @CB_INFACT = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_Infcred = @CB_INFCRED
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			set @Old_INF_ANT = @CB_INF_ANT
		
		end
		
		/* Reinicio */
		
		if ( @KI_TIPO = 2 ) 
		begin
			set @CB_INFACT = 'S'
			set @Old_AplicaDis = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_Infcred = @CB_INFCRED
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			set @Old_INF_ANT = @CB_INF_ANT
		end
		 

		/* Suspensi�n */
		if ( @KI_TIPO = 1 )
		BEGIN
			set @CB_INFACT = 'N'
		END

		/* Cambios */
		
		/* Cambio de Tipo de Descuento */
		if ( @KI_TIPO = 3 )
		begin
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		end

		/* Cambio de Valor de Descuento */
		if ( @KI_TIPO = 4 )
		begin
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM

		end

		/* Cambio de # de Cr�dito */
		if ( @KI_TIPO = 5 )
		begin
			set @Old_Infcred = @CB_INFCRED
      set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		end


		update KARINF set CB_INFTIPO = @Old_inftipo,
					    CB_INFCRED = @Old_Infcred,
	 			    	    CB_INFTASA = @Old_Inftasa,
	 			         CB_INFDISM = @Old_AplicaDis,
						 CB_INF_ANT = @Old_INF_ANT
		where CURRENT OF CursorInfonavit

		Fetch NEXT from CursorInfonavit
		into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO, @CB_INF_ANT
	END

	update COLABORA set CB_INF_INI = @FecIniCredito,
					CB_INFTIPO = @Old_InfTipo,
					CB_INFCRED = @Old_InfCred,
					CB_INFTASA = @Old_InfTasa,
					CB_INFACT = @CB_INFACT,
					CB_INFDISM = @Old_AplicaDis,
					CB_INF_ANT = @Old_INF_ANT
	where ( CB_CODIGO = @Empleado )

	close CursorInfonavit;
	deallocate CursorInfonavit;
END
GO

CREATE PROCEDURE SP_CALENDARIO_HORAS (
@FECHAINI VARCHAR(30),
@FECHAFIN VARCHAR(30),
@NUMERO VARCHAR(30),
@CB_DIA VARCHAR(30),
@CB_HORAS VARCHAR(30)
)
AS  
BEGIN
DECLARE @QUERY varchar(1000);
DECLARE @QUERY2 varchar(1000);
DECLARE @QUERY3 varchar(1000);

SET @QUERY = ' '
SET @QUERY2 = ' '
SET @QUERY3 = ' '

IF @CB_DIA ='' or @CB_DIA ='-100'
	BEGIN
		SELECT @QUERY3 = ''
	END

IF @CB_DIA <> '-100'
	BEGIN
		SELECT @QUERY3 = 'and( AU_STATUS LIKE LTRIM(RTRIM('''+@CB_DIA+'''))) '
	END


IF @CB_HORAS = '0'
	BEGIN
		SELECT @QUERY = 'and AU_HORASCK <> 0.00 ';
	END
IF @CB_HORAS = '1'
	BEGIN
		SELECT @QUERY = 'and AU_EXTRAS <> 0.00 ';
	END
IF @CB_HORAS = '2' 
	BEGIN
		SELECT @QUERY = 'and AU_PER_CG <> 0.00 ';
	END
IF @CB_HORAS = '3' 
	BEGIN
		SELECT @QUERY = 'and AU_PER_SG <> 0.00 ';
	END
IF @CB_HORAS = '4' 
	BEGIN
		SELECT @QUERY = 'and AU_DES_TRA <> 0.00 ';
	END
IF @CB_HORAS = '5' 
	BEGIN
		SELECT @QUERY = 'and AU_TARDES <> 0.00 ';
	END

IF @CB_HORAS ='' or @CB_HORAS ='-100'
	BEGIN
		SELECT @QUERY = ''
	END

IF @NUMERO ='' or @NUMERO ='-100'
	BEGIN
		SELECT @QUERY2 = ''
	END

IF @NUMERO <> '-100'
	BEGIN
		SELECT @QUERY2 = ' and ( AUSENCIA.CB_CODIGO = '''+@NUMERO+''') '
	END


	exec('
		select AUSENCIA.CB_CODIGO,
		PRETTYNAME,
		AUSENCIA.AU_FECHA,
		AUSENCIA.AU_STATUS,
		DBO.TF( 40, AUSENCIA.AU_STATUS ) Dia,
		AUSENCIA.AU_TIPODIA,
		DBO.TF( 41, AUSENCIA.AU_STATUS ) Tipo,
		AUSENCIA.AU_HORASCK,
		AUSENCIA.AU_HORAS,
		AUSENCIA.AU_EXTRAS,
		AUSENCIA.AU_PER_CG,
		AUSENCIA.AU_PER_SG,
		AUSENCIA.AU_DES_TRA,
		AUSENCIA.AU_TARDES
		from AUSENCIA

		left outer join COLABORA on(COLABORA.CB_CODIGO=AUSENCIA.CB_CODIGO)
		where ( AUSENCIA.AU_FECHA >='''+@FECHAINI+''' and AUSENCIA.AU_FECHA<='''+@FECHAFIN+''')
		'+@QUERY3+'
		'+@QUERY2+'
		'+@QUERY+'
		order by AUSENCIA.CB_CODIGO,AUSENCIA.AU_FECHA
	')
END
GO

CREATE PROCEDURE SP_CALENDARIO_INCIDENCIAS (
@FECHAINI VARCHAR(30),
@FECHAFIN VARCHAR(30),
@NUMERO VARCHAR(30),
@TIPO VARCHAR(30)
)
AS  
BEGIN

SET @TIPO = CASE @TIPO  WHEN '-100' THEN '%' WHEN '-1' THEN '%' WHEN null THEN '%' WHEN '' THEN '%' ELSE @TIPO END


DECLARE @QUERY2 varchar(1000);

SET @QUERY2 = ''

IF @NUMERO = '' or @NUMERO ='-100'
	BEGIN
		SELECT @QUERY2 = ''
	END

IF @NUMERO <> -100
	BEGIN
		SELECT @QUERY2 = 'and ( AUSENCIA.CB_CODIGO LIKE LTRIM(RTRIM('''+@NUMERO+''' )))  '
	END

	exec('
		select 
		AUSENCIA.CB_CODIGO,
		PRETTYNAME,
		AUSENCIA.AU_FECHA,
		DBO.TF( 40, AUSENCIA.AU_STATUS ) Dia,
		DBO.TF( 41, AUSENCIA.AU_STATUS ) Tipo,
		AUSENCIA.AU_TIPO
		from AUSENCIA
		left outer join COLABORA on(COLABORA.CB_CODIGO=AUSENCIA.CB_CODIGO)
		where ( AUSENCIA.AU_FECHA >='''+@FECHAINI+''' and AUSENCIA.AU_FECHA<='''+@FECHAFIN+''')
		and ( AUSENCIA.AU_TIPO LIKE LTRIM(RTRIM('''+@TIPO+''' )))
		'+@QUERY2+'
		order by AUSENCIA.CB_CODIGO,AUSENCIA.AU_FECHA
	')
END
GO

CREATE PROCEDURE SP_GETTIPONOMINA
@CB_CODIGO INT,
@VERSIONTRESS VARCHAR(10)
AS
BEGIN
	IF (@VERSIONTRESS = '')
	BEGIN
		SELECT 'TIPO_NOMINA' = 0 
	END

	IF ( @VERSIONTRESS = '2006' )
	BEGIN

		SELECT 'TIPO_NOMINA' = TURNO.TU_NOMINA 
		FROM COLABORA 
		LEFT OUTER JOIN TURNO 
		ON ( TURNO.TU_CODIGO = ( SELECT DBO.SP_GET_TURNO( GETDATE(), COLABORA.CB_CODIGO ) ) ) 
		WHERE ( COLABORA.CB_CODIGO = @CB_CODIGO )

	END
	
	IF ( @VERSIONTRESS = '2007')
	BEGIN
	
		SELECT 'TIPO_NOMINA' = CB_NOMINA 
		FROM COLABORA
		WHERE CB_CODIGO = @CB_CODIGO
	
	END

END
GO

CREATE PROCEDURE SP_CAMBIA_REGLAPRESTA(@CODIG FolioChico, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
SET NOCOUNT ON;
	declare @Maximo FolioChico;
	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( RP_ORDEN ) from REGLAPREST;
	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN;
	update REGLAPREST	set RP_ORDEN = 0 where ( RP_ORDEN = @Actual );
	update REGLAPREST	set RP_ORDEN = @Actual where ( RP_ORDEN = @Nuevo );
	update REGLAPREST  set RP_ORDEN = @Nuevo where ( RP_ORDEN = 0 );
END
GO

CREATE PROCEDURE SP_BORRA_REGLAPREST( @RP_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from REGLAPREST where ( RP_CODIGO = @RP_CODIGO ) and ( RP_ORDEN = @Actual );
	if ( @@ROWCOUNT > 0 )
    	begin
	     update REGLAPREST set RP_ORDEN = RP_ORDEN - 1
             where ( RP_ORDEN > @Actual );
        end
END
GO

create procedure SET_KARCURSO_CONTADOR( @Anio smallint, @Inicio FolioGrande )
as
begin
	SET NOCOUNT ON
	declare @Contador FolioGrande;
	declare @Curso Codigo;
	declare @Patron RegPatronal;
	declare @NextPatron RegPatronal;
	declare @Establecimiento Codigo;
	declare @Fecha Fecha;
	
		
	update KARCURSO set KC_NO_STPS = 0 where year(KC_FEC_TOM)= @Anio;

	declare TemporalCursos CURSOR for
	select    K.CU_CODIGO,
		K.KC_FEC_TOM,
		DBO.SP_KARDEX_CB_PATRON(K.KC_FEC_TOM,C.CB_CODIGO),
		KC_EST
		
	from KARCURSO K
	left outer join COLABORA C ON C.CB_CODIGO = K.CB_CODIGO
	left outer join CURSO CU ON CU.CU_CODIGO = K.CU_CODIGO
	WHERE CU.CU_STPS='S' and  year(K.KC_FEC_TOM)= @Anio
	group by 
		K.CU_CODIGO,
		K.KC_FEC_TOM,
		DBO.SP_KARDEX_CB_PATRON(K.KC_FEC_TOM,C.CB_CODIGO),
		K.KC_EST

	order by  K.CU_CODIGO,
		K.KC_FEC_TOM,
		DBO.SP_KARDEX_CB_PATRON(K.KC_FEC_TOM,C.CB_CODIGO),
		K.KC_EST

	open TemporalCursos;

	fetch NEXT from TemporalCursos
	into @Curso, @Fecha, @Patron, @Establecimiento

	select @Contador = @Inicio

	while ( @@Fetch_Status = 0 )
	begin

		Update KARCURSO
		set KC_NO_STPS = @Contador
		WHERE year(KC_FEC_TOM)= @Anio
		and CU_CODIGO = @Curso
		and KC_FEC_TOM = @Fecha
		and DBO.SP_KARDEX_CB_PATRON(KC_FEC_TOM,CB_CODIGO)=@Patron
		and KC_EST = @Establecimiento

		select @Contador = @Contador + 1

		fetch NEXT from TemporalCursos
		into @Curso, @Fecha, @Patron, @Establecimiento
	end;
	close TemporalCursos;
	deallocate TemporalCursos;
end
GO

create procedure SP_BALANZAMENSUAL(
	@USUARIO SMALLINT,
	@ANIO SMALLINT,
	@TIPO SMALLINT,
	@NUMERO SMALLINT,
	@EMPLEADO INTEGER,
	@CONCEPTO SMALLINT,
	@COLUMNA SMALLINT,
	@DESCRIPCION CHAR( 30 ),
	@MONTO NUMERIC( 15, 2 ),
	@HORAS NUMERIC( 15, 2 ),
	@REFERENCIA VARCHAR( 8 ) )
as
begin
     SET NOCOUNT ON
     declare @Percepcion SMALLINT;
     declare @Deduccion SMALLINT;
     declare @Existe SMALLINT;

     if (@Columna= 1)
	select @Existe = COUNT(*) from DBO.TMPBALAMES as T
	where 	( T.TM_USER = @USUARIO ) and
		( T.CB_CODIGO = @EMPLEADO ) and
		( T.TM_NUM_PER = @CONCEPTO ) and
		( T.TM_REF_PER = @REFERENCIA )
     else
	select @Existe = COUNT(*) from DBO.TMPBALAMES as T
	where 	( T.TM_USER = @USUARIO ) and
		( T.CB_CODIGO = @EMPLEADO ) and
		( T.TM_NUM_DED = @CONCEPTO ) and
		( T.TM_REF_DED = @REFERENCIA )

     if ( @Existe = 0 )
	begin
	     select @Percepcion = COUNT(*) from DBO.TMPBALAMES as T
	     where ( T.TM_USER = @USUARIO ) and
		 ( T.CB_CODIGO = @EMPLEADO ) and
		 ( T.TM_NUM_PER > 0 )

	     select @Deduccion = COUNT(*) from DBO.TMPBALAMES as T
	     where ( T.TM_USER = @USUARIO ) and
		 ( T.CB_CODIGO = @EMPLEADO ) and
		 ( T.TM_NUM_DED > 0 )

	     if ( @COLUMNA = 1 )
	     begin
		if ( @Percepcion >= @Deduccion )
		begin
		     insert into DBO.TMPBALAMES( TM_CODIGO,
					 CB_CODIGO,
					 PE_YEAR,
					 PE_TIPO,
					 PE_NUMERO,
					 TM_USER,
					 TM_NUM_PER,
					 TM_MON_PER,
					 TM_DES_PER,
					 TM_HOR_PER,
					 TM_REF_PER ) values (
					@PERCEPCION,
					@EMPLEADO,
					@ANIO,
					@TIPO,
					@NUMERO,
					@USUARIO,
					@CONCEPTO,
					@MONTO,
					@DESCRIPCION,
					@HORAS,
					@REFERENCIA );
		end
		else
		begin
		     update DBO.TMPBALAMES set TM_NUM_PER = @CONCEPTO,
				         TM_MON_PER = @MONTO,
				         TM_DES_PER = @DESCRIPCION,
				         TM_HOR_PER = @HORAS,
				         TM_REF_PER = @REFERENCIA
		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_CODIGO = @PERCEPCION )

		end
	     end
	     else
	     begin
		if ( @Deduccion >= @Percepcion  )
		begin
		     insert into DBO.TMPBALAMES( TM_CODIGO,
					 CB_CODIGO,
					 PE_YEAR,
					 PE_TIPO,
					 PE_NUMERO,
					 TM_USER,
					 TM_NUM_DED,
					 TM_MON_DED,
					 TM_DES_DED,
					 TM_HOR_DED,
					 TM_REF_DED ) values (
					@DEDUCCION,
					@EMPLEADO,
					@ANIO,
					@TIPO,
					@NUMERO,
					@USUARIO,
					@CONCEPTO,
					@MONTO,
					@DESCRIPCION,
					@HORAS,
					@REFERENCIA );
		end
		else
		begin
		     update DBO.TMPBALAMES set TM_NUM_DED = @CONCEPTO,
				         TM_MON_DED = @MONTO,
				         TM_DES_DED = @DESCRIPCION,
				         TM_HOR_DED = @HORAS,
				         TM_REF_DED = @REFERENCIA
		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_CODIGO = @DEDUCCION )
		end;
	     end;
	end
	else
	begin
		if ( @COLUMNA = 1 )
		     update DBO.TMPBALAMES set
					TM_MON_PER = TM_MON_PER + @MONTO,
					TM_HOR_PER = TM_HOR_PER + @HORAS

		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_NUM_PER = @CONCEPTO )
		else
		     update DBO.TMPBALAMES set
					TM_MON_DED = TM_MON_DED + @MONTO,
					TM_HOR_DED = TM_HOR_DED + @HORAS

		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_NUM_DED = @CONCEPTO ) and
			 ( TM_REF_DED = @REFERENCIA )

	end;
end;
GO

CREATE PROCEDURE SP_ORDENA_BALAMES( @Usuario Usuario )
AS
begin
	SET NOCOUNT ON

	declare @Empleado		NumeroEmpleado;
	declare @Concepto           	Concepto;
	declare @Descripcion          Descripcion;
	declare @Monto            	Pesos;
	declare @Horas            	Horas;
	declare @Referencia         	Referencia;
	declare @Posicion		FolioChico;


	DECLARE EMPLEADOS CURSOR FOR
		select DISTINCT(CB_CODIGO) from TMPBALAMES where ( TM_USER = @Usuario )

	OPEN EMPLEADOS
	FETCH NEXT FROM EMPLEADOS
	into @Empleado

	WHILE @@FETCH_STATUS = 0
	BEGIN
		select  @Posicion =0;
		DECLARE PERCEPCION CURSOR STATIC FOR
			select TM_NUM_PER,
			       TM_DES_PER,
			       TM_MON_PER,
			       TM_HOR_PER,
			       TM_REF_PER
			from TMPBALAMES
			where 	( TM_USER = @Usuario ) and
				( CB_CODIGO = @Empleado ) and
				( TM_NUM_PER <> 0 )
 			order by TM_NUM_PER
		OPEN PERCEPCION
		FETCH NEXT FROM PERCEPCION
		into @Concepto, @Descripcion, @Monto, @Horas, @Referencia

		WHILE @@FETCH_STATUS = 0
		BEGIN
			Update TMPBALAMES
			set
				TM_NUM_PER = @Concepto,
				TM_DES_PER = @Descripcion,
				TM_MON_PER = @Monto,
				TM_HOR_PER = @Horas,
				TM_REF_PER = @Referencia
			where 	( TM_USER = @Usuario ) and
				( CB_CODIGO = @Empleado ) and
				( TM_CODIGO = @Posicion )

			select  @Posicion = @Posicion+1;

			FETCH NEXT FROM PERCEPCION
			into @Concepto, @Descripcion, @Monto, @Horas, @Referencia

		END;

		CLOSE PERCEPCION
		DEALLOCATE PERCEPCION

		select  @Posicion =0;
		DECLARE DEDUCCION CURSOR STATIC FOR
			select TM_NUM_DED,
			       TM_DES_DED,
			       TM_MON_DED,
			       TM_HOR_DED,
			       TM_REF_DED
			from TMPBALAMES
			where 	( TM_USER = @Usuario ) and
				( CB_CODIGO = @Empleado ) and
				( TM_NUM_DED <> 0 )
 			order by TM_NUM_DED
		OPEN DEDUCCION
		FETCH NEXT FROM DEDUCCION
		into @Concepto, @Descripcion, @Monto, @Horas, @Referencia

		WHILE @@FETCH_STATUS = 0
		BEGIN
			Update TMPBALAMES
			set
				TM_NUM_DED = @Concepto,
				TM_DES_DED = @Descripcion,
				TM_MON_DED = @Monto,
				TM_HOR_DED = @Horas,
				TM_REF_DED = @Referencia
			where 	( TM_USER = @Usuario ) and
				( CB_CODIGO = @Empleado ) and
				( TM_CODIGO = @Posicion )

			select  @Posicion = @Posicion+1;

			FETCH NEXT FROM DEDUCCION
			into @Concepto, @Descripcion, @Monto, @Horas, @Referencia
		END;

		CLOSE DEDUCCION
		DEALLOCATE DEDUCCION

		FETCH NEXT FROM EMPLEADOS
		into @Empleado

	END

	CLOSE EMPLEADOS
   	DEALLOCATE EMPLEADOS
end
go

CREATE PROCEDURE SP_ACTUALIZA_USUARIO( @US_CODIGO Usuario, @US_CODIGO_ANT Usuario )
AS
BEGIN
	 SET NOCOUNT ON
	 IF ( @US_CODIGO_ANT > 0 )
	 BEGIN
		UPDATE NIVEL1 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL2 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL3 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL4 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL5 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL6 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL7 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL8 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL9 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
	 END
END
GO

CREATE PROCEDURE SP_MIGRACIONPRESUPUESTOTABLA ( 
	@TABLA NombreCampo,
	@BASEORIGEN Formula,
	@BASEDESTINO Formula
)
AS
BEGIN
	SET NOCOUNT ON
	Declare @Sentencia varchar(8000)
	Declare @Sentencia2 varchar(8000)
	Declare @Condicion varchar(8000)
	Declare @CamposCantidad int
	Declare @RegistrosCantidad int
	Declare @TablaEspecial varchar(8000)
	DECLARE @CamposConcatenados VARCHAR(8000) 
	DECLARE @CamposConcatenadosCoalesce VARCHAR(8000) 
	declare @Campos table(Campos varchar(10), Coal varchar(1000))

	insert into BIT_MIG  values( getdate(), '', '');
	insert into BIT_MIG  values( getdate(), 'Migrando ' + @TABLA, 'Migraci�n Tabla: ' + @TABLA );

	insert into @campos (Campos, Coal)
	SELECT     c.name, 'COALESCE(' + c.name + ',dbo.G_D(''' + @TABLA + ''',''' + c.name + '''))'
	FROM         syscolumns AS c INNER JOIN
						  sysobjects AS o ON c.id = o.id
	Where UPPER(o.xtype) = 'U'
			and o.name = @TABLA
			and c.iscomputed <> 1 
			and c.status <> 128

	set @CamposCantidad = (SELECT count(*) from @campos)	
	
	Set @TablaEspecial = case when @TABLA = 'KAR_FIJA' then 'KAR_FIJA2' else @TABLA end


	Select @CamposConcatenados = COALESCE( @CamposConcatenados + ',', '') + Campos FROM @Campos
	Select @CamposConcatenadosCoalesce = COALESCE( @CamposConcatenadosCoalesce + ',', '') + Coal FROM @Campos

	set @Sentencia  = ' INSERT INTO ' + @BASEDESTINO + '..' + @TablaEspecial + ' (' + @CamposConcatenados + ') '
	set @Sentencia2 = ' select ' +  @CamposConcatenadosCoalesce + ' from ' + @BASEORIGEN + '..' + @TABLA 
	
	insert into BIT_MIG values( getdate(), 'Sentencia de Inserci�n: ' + @TABLA, @Sentencia + @Sentencia2 );
	EXEC ( @Sentencia + @Sentencia2 )
	insert into BIT_MIG values( getdate(), 'Migrado ' + @TABLA, 'Registros Migrados: ' + convert( varchar(100), @@Rowcount ) );
		
END
GO

CREATE PROCEDURE SPP_MigracionPresupuesto_DELETE( @lDejarConceptos Booleano )
AS
BEGIN
		/* El parametro @lDejarConceptos, NO borra los catalogos de CONCEPTOS y  PARAMETROS */

		SET NOCOUNT ON;
		Delete from BIT_MIG
		insert into BIT_MIG 
		values(	getdate(), 'Se trunca la Tabla de Bitacora', '' );
		DELETE FROM ACUMULA;
		DELETE FROM ACUMULA_RS; 
		DELETE FROM KAR_TOOL;
		DELETE FROM VACACION;
		DELETE FROM PCAR_ABO;
		DELETE FROM PRESTAMO;
		DELETE FROM AHORRO;
		DELETE FROM ACAR_ABO;
		DELETE FROM KARDEX;
		DELETE FROM COLABORA;		
		DELETE FROM KAR_FIJA2;
		DELETE FROM KAR_FIJA;
		DELETE FROM TAHORRO;
		DELETE FROM CAMPOREP
		DELETE FROM REPORTE
		DELETE FROM R_VALOR;
		DELETE FROM R_LISTAVAL;
		DELETE FROM R_MOD_ENT;
		DELETE FROM R_ORDEN;
		DELETE FROM R_DEFAULT;
		DELETE FROM R_FILTRO;
		DELETE FROM R_CLAS_ENT;
		DELETE FROM R_RELACION;
		DELETE FROM R_ATRIBUTO;
		DELETE FROM R_ENTIDAD;
		DELETE FROM R_MODULO;
		DELETE FROM R_CLASIFI;
		DELETE FROM PLAZA
		DELETE FROM PTOFIJAS
		DELETE FROM PTOTOOLS
		DELETE FROM MOT_TOOL;
		DELETE FROM TOOL;
		DELETE FROM TALLA;
		DELETE FROM PERIODO; 
		DELETE FROM TPERIODO;
		DELETE FROM TKARDEX;
		DELETE FROM TCAMBIO;
		DELETE FROM SAL_MIN;
		DELETE FROM RIESGO;
		DELETE FROM QUERYS;
		DELETE FROM PUESTO;
		DELETE FROM PRIESGO;
		DELETE FROM PRESTAXREG;
		DELETE FROM REGLAPREST;
		DELETE FROM TPRESTA;
		DELETE FROM PRESTACI;
		DELETE FROM SSOCIAL;
		DELETE FROM OTRASPER;
		if NOT (@lDejarConceptos = 'S')
			DELETE FROM NOMPARAM;
		DELETE FROM NIVEL9;
		DELETE FROM NIVEL8;
		DELETE FROM NIVEL7;
		DELETE FROM NIVEL6;
		DELETE FROM NIVEL5;
		DELETE FROM NIVEL4;
		DELETE FROM NIVEL3;
		DELETE FROM NIVEL2;
		DELETE FROM NIVEL1;
		DELETE FROM MOT_BAJA;
		DELETE FROM LEY_IMSS;
		DELETE FROM HORARIO;
		DELETE FROM FESTIVO;
		DELETE FROM ENTIDAD;
		DELETE FROM CONTRATO;
		DELETE FROM CLASIFI;
		DELETE FROM TURNO;
		if NOT (@lDejarConceptos = 'S')
			DELETE FROM CONCEPTO;
		DELETE FROM GLOBAL;
		DELETE FROM ART_80;
		DELETE FROM T_ART_80;
		DELETE FROM NUMERICA;
		DELETE FROM RPATRON;
		DELETE FROM RSOCIAL;
		DELETE FROM VALOR_UMA;
END
GO

CREATE PROCEDURE SPP_MigracionPresupuesto
	(
		@BaseOrigen Formula,
		@BaseDestino Formula
	)
AS
BEGIN
		execute SP_MIGRACIONPRESUPUESTOTABLA 'RSOCIAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'RPATRON',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NUMERICA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'T_ART_80',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'ART_80',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'GLOBAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CONCEPTO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TURNO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CLASIFI',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CONTRATO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'ENTIDAD',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'FESTIVO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'HORARIO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'LEY_IMSS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'MOT_BAJA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL1',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL2',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL3',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL4',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL5',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL6',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL7',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL8',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL9',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NOMPARAM',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'OTRASPER',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'SSOCIAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PRESTACI',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TPRESTA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'REGLAPREST',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PRESTAXREG',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PRIESGO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PUESTO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'QUERYS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'RIESGO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'SAL_MIN',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TCAMBIO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TKARDEX',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TPERIODO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TALLA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TOOL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'MOT_TOOL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PTOTOOLS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PTOFIJAS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PLAZA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_CLASIFI',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_MODULO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_ENTIDAD' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_ATRIBUTO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_RELACION',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_CLAS_ENT',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_FILTRO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_DEFAULT' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_ORDEN' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_MOD_ENT' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_LISTAVAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_VALOR',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'REPORTE',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CAMPOREP',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TAHORRO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'KAR_FIJA',@BaseOrigen,@BaseDestino;
END
GO

CREATE PROCEDURE CALCULA_CUBO
(
	@ANIO Anio, 
	@ES_CODIGO Codigo
)
AS
begin
     SET NOCOUNT ON
	 INSERT INTO TOTPRESUP( PE_YEAR,PE_MES,PE_TIPO,
                            CO_NUMERO,CB_CLASIFI,CB_PUESTO,CB_TURNO,
                            CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,
                            CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,
                            CB_NIVEL9,CB_NIVEL0,
                            TP_MONTO, TP_EMPLEAD, ES_CODIGO)

     select M.PE_YEAR,P.PE_MES,M.PE_TIPO,
            M.CO_NUMERO,N.CB_CLASIFI,N.CB_PUESTO,N.CB_TURNO,
            N.CB_NIVEL1,N.CB_NIVEL2,N.CB_NIVEL3,N.CB_NIVEL4,
            N.CB_NIVEL5,N.CB_NIVEL6,N.CB_NIVEL7,N.CB_NIVEL8,
            N.CB_NIVEL9,N.CB_NIVEL0,
     SUM( M.MO_DEDUCCI+M.MO_PERCEPC ),  COUNT(DISTINCT(M.CB_CODIGO)), @ES_CODIGO 
     FROM MOVIMIEN M
     LEFT OUTER JOIN NOMINA  N ON N.PE_YEAR = M.PE_YEAR AND N.PE_TIPO = M.PE_TIPO AND N.PE_NUMERO = M.PE_NUMERO AND N.CB_CODIGO = M.CB_CODIGO
     LEFT OUTER JOIN PERIODO P ON P.PE_YEAR = N.PE_YEAR AND P.PE_TIPO = N.PE_TIPO AND P.PE_NUMERO = N.PE_NUMERO 
     WHERE M.PE_YEAR = @Anio
     GROUP BY M.PE_YEAR,P.PE_MES,M.PE_TIPO,
           M.CO_NUMERO,N.CB_CLASIFI,N.CB_PUESTO,N.CB_TURNO,
           N.CB_NIVEL1,N.CB_NIVEL2,N.CB_NIVEL3,N.CB_NIVEL4,
           N.CB_NIVEL5,N.CB_NIVEL6,N.CB_NIVEL7,N.CB_NIVEL8,
           CB_NIVEL9,N.CB_NIVEL0 ;
end;
GO

CREATE PROCEDURE SP_COMPACTA_KARDEX
AS
BEGIN
      SET NOCOUNT ON

      DELETE FROM KARDEX

      INSERT INTO KARDEX
      ( CB_CODIGO, CB_FECHA, CB_TIPO, CB_AUTOSAL,
        CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
        CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
        CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_NIVEL,
        CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        CB_STATUS, CB_TABLASS, CB_TOT_GRA, US_CODIGO, CB_ZONA_GE,
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        CB_MOD_NV1, CB_MOD_NV2, CB_MOD_NV3, CB_MOD_NV4, CB_MOD_NV5, CB_MOD_NV6, CB_MOD_NV7, CB_MOD_NV8, CB_MOD_NV9,
        CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_NOTA, CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV )
      SELECT
        CB_CODIGO, CB_FEC_ING, 'ALTA', CB_AUTOSAL,
        CB_CLASIFI, 'ALTA en Presupuesto', CB_CONTRAT,
        CB_FAC_INT, GETDATE(), CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FEC_ING,
        'S', CB_TURNO, 0, '', 0,
        CB_OLD_INT, CB_OLD_SAL, 0, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        0, CB_TABLASS, CB_TOT_GRA, 0, CB_ZONA_GE,
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S',
        0, 0, 0, 'N', '', CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
      FROM COLABORA

      INSERT INTO KARDEX
      ( CB_CODIGO, CB_FECHA, CB_TIPO, CB_AUTOSAL,
        CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
        CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
        CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_NIVEL,
        CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        CB_STATUS, CB_TABLASS, CB_TOT_GRA, US_CODIGO, CB_ZONA_GE,
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        CB_MOD_NV1, CB_MOD_NV2, CB_MOD_NV3, CB_MOD_NV4, CB_MOD_NV5, CB_MOD_NV6, CB_MOD_NV7, CB_MOD_NV8, CB_MOD_NV9,
        CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_NOTA, CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV )
      SELECT
        CB_CODIGO, CB_FEC_BAJ, 'BAJA', CB_AUTOSAL,
        CB_CLASIFI, 'BAJA en Presupuesto', CB_CONTRAT,
        CB_FAC_INT, GETDATE(), CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FEC_BSS,
        'S', CB_TURNO, 0, CB_MOT_BAJ, 9,
        CB_OLD_INT, CB_OLD_SAL, 0, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        0, CB_TABLASS, CB_TOT_GRA, 0, CB_ZONA_GE,
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
        CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, 'N', '', CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
      FROM COLABORA
      WHERE CB_ACTIVO = 'N'

	  DELETE FROM KAR_FIJA
      INSERT INTO KAR_FIJA
       ( CB_CODIGO, CB_FECHA, CB_TIPO, KF_FOLIO, KF_CODIGO, KF_MONTO, KF_GRAVADO, KF_IMSS )
      SELECT KAR_FIJA2.CB_CODIGO, COLABORA.CB_FEC_ING, 'ALTA', KF_FOLIO, KF_CODIGO, KF_MONTO, KF_GRAVADO, KF_IMSS
      FROM  KAR_FIJA2, COLABORA
      WHERE KAR_FIJA2.CB_CODIGO = COLABORA.CB_CODIGO AND
            KAR_FIJA2.CB_FECHA  = COLABORA.CB_FEC_SAL AND
            KAR_FIJA2.CB_TIPO   = COLABORA.CB_TIP_REV

END
GO

CREATE PROCEDURE SPP_MigracionOptimiza(@CambioMensuales Booleano)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @TP_Tipo NominaTipo;

    if ( @CambioMensuales = 'S')
    begin
        select @TP_Tipo = TP_TIPO from TPERIODO where TP_CLAS = 4 and TP_DESCRIP <> '';
        UPDATE COLABORA set CB_CREDENC = '', CB_ID_NUM = '',CB_NOMINA = @TP_Tipo;
    end
    else
    begin
        UPDATE COLABORA set CB_CREDENC = '', CB_ID_NUM = '';
    end

		insert into BIT_MIG
		values(getdate(), 'Actualizacion COLABORA CB_CREDENC, CB_ID_NUM','Cantidad de Registros Actualizados: ' + convert(varchar(10), @@Rowcount));

		EXECUTE SP_COMPACTA_KARDEX

 		update KARDEX set US_CODIGO = 0

		insert into BIT_MIG
		values(getdate(), 'Actualizacion KARDEX | US_CODIGO = 0','Cantidad de Registros Actualizados: ' + convert(varchar(10), @@Rowcount));

		delete from KAR_FIJA2
		insert into BIT_MIG
		values(getdate(), 'Eliminacion de KAR_FIJA2','Cantidad de Registros Eliminados: ' + convert(varchar(10), @@Rowcount));
END
GO

CREATE PROCEDURE SP_RECALCULA_PRESTAMOS_TODOS
AS
BEGIN
	 SET NOCOUNT ON
	Declare @Sentencia varchar(1000)
	Declare Procedures CURSOR FOR
	select 'EXECUTE RECALCULA_PRESTAMOS ' + convert(varchar(20),CB_CODIGO) + ',6' [Procedure] from colabora

	OPEN Procedures

	FETCH NEXT FROM Procedures into @Sentencia
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Exec ( @Sentencia )
	END

	CLOSE Procedures;
	DEALLOCATE Procedures;
END
GO

CREATE PROCEDURE SP_RECALCULA_AHORROS_TODOS
AS
BEGIN
	SET NOCOUNT ON
	Declare @Sentencia varchar(1000)
	Declare Procedures CURSOR FOR
	select 'EXECUTE RECALCULA_AHORROS ' + convert(varchar(20),CB_CODIGO) + ',6' [Procedure] from colabora

	OPEN Procedures

	FETCH NEXT FROM Procedures into @Sentencia
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Exec ( @Sentencia )
	END

	CLOSE Procedures;
	DEALLOCATE Procedures;
END
GO

CREATE PROCEDURE SP_ACTUALIZAR_MOVIMIENTO_IDSE(
@TipoSUA    Status,
@NSS        Descripcion,
@Fecha      Fecha,
@LoteIDSE   DescLarga,
@FechaIDSE  Fecha,
@US_CODIGO  Usuario,
@REG_PATRON DescLarga,
@CB_CODIGO  NumeroEmpleado output,
@RESPUESTA  Status output
)
as
BEGIN
      SET NOCOUNT ON
      declare
            @CB_TIPO Codigo;
      DECLARE
          @CB_PATRON NumeroPatronal;
      declare
            @SSValido NumeroEmpleado;

      set @CB_CODIGO = 0
      set @Respuesta = -10

      SET @SSValido = ( SELECT Count(CB_CODIGO)  FROM COLABORA WHERE CB_SEGSOC = @NSS ) ;
      SET @SSValido = COALESCE ( @SSValido, 0 )

      IF ( @SSValido <= 0 )
      BEGIN
            SET @RESPUESTA = -10
      END
      ELSE
      BEGIN
            IF ( @TIPOSUA = 2 )
                  SET @CB_TIPO = 'BAJA'
            ELSE
            IF ( @TIPOSUA = 7 )
                  SET @CB_TIPO = 'CAMBIO'
            ELSE
            IF ( @TIPOSUA = 8 )
                  SET @CB_TIPO = 'ALTA'
            ELSE
                  SET @CB_TIPO = '';

            IF ( @CB_TIPO = '' )
            BEGIN
                  SET @RESPUESTA = -30
            END
            ELSE
            BEGIN
                  SELECT @CB_PATRON = TB_CODIGO FROM RPATRON WHERE TB_NUMREG = @REG_PATRON;
                  set @CB_CODIGO = 
						COALESCE (( SELECT TOP 1 CB.CB_CODIGO from COLABORA CB join KARDEX K on K.CB_CODIGO = CB.CB_CODIGO  where CB_SEGSOC = @NSS and  K.CB_PATRON = @CB_PATRON and K.CB_TIPO = @CB_TIPO and K.CB_FECHA_2 = @FECHA), 0)

                  IF ( SELECT COUNT(*) FROM KARDEX WHERE CB_TIPO = @CB_TIPO AND CB_CODIGO = @CB_CODIGO  AND CB_FECHA_2 =  @FECHA AND CB_PATRON = @CB_PATRON ) > 0
                  BEGIN
                        UPDATE KARDEX
                        SET
                             CB_LOT_IDS = @LOTEIDSE,
                              CB_FEC_IDS = @FECHAIDSE
                        WHERE
                             CB_TIPO = @CB_TIPO AND CB_CODIGO = @CB_CODIGO  AND CB_FECHA_2 =  @FECHA AND CB_PATRON = @CB_PATRON

                        SET @RESPUESTA = 0
                  END
                  ELSE
                        SET  @RESPUESTA = 20
            end
      end
END
GO

CREATE PROCEDURE SP_AJUSTA_USUARIO_SUPERVISOR( @US_CODIGO Usuario,  @US_JEFE Usuario  )
AS
BEGIN
	SET NOCOUNT ON
	UPDATE #COMPARTE.DBO.USUARIO
	SET US_JEFE = @US_JEFE
	WHERE US_CODIGO = @US_CODIGO
END
GO

CREATE PROCEDURE SP_AJUSTA_SUPERVISOR_ENROLADOS_COMPARTE( @NivelSupervisores Formula, @TB_CODIGO Codigo, @US_CODIGO Usuario)
AS
BEGIN
	SET NOCOUNT ON 
   	
    		UPDATE #COMPARTE.DBO.USUARIO 
    		SET US_JEFE = @US_CODIGO 
    		WHERE US_CODIGO IN 
    		(
	    		SELECT CB.US_CODIGO FROM COLABORA CB
	    		WHERE COALESCE(CB.US_CODIGO,0) > 0 
	    		 AND (
		  		(CB.CB_NIVEL1 = @TB_CODIGO AND @NivelSupervisores = '1') OR
	        		(CB.CB_NIVEL2 = @TB_CODIGO AND @NivelSupervisores = '2') OR
		    		(CB.CB_NIVEL3 = @TB_CODIGO AND @NivelSupervisores = '3') OR
		    		(CB.CB_NIVEL4 = @TB_CODIGO AND @NivelSupervisores = '4') OR
		    		(CB.CB_NIVEL5 = @TB_CODIGO AND @NivelSupervisores = '5') OR
		    		(CB.CB_NIVEL6 = @TB_CODIGO AND @NivelSupervisores = '6') OR
		    		(CB.CB_NIVEL7 = @TB_CODIGO AND @NivelSupervisores = '7') OR
		    		(CB.CB_NIVEL8 = @TB_CODIGO AND @NivelSupervisores = '8') OR
		    		(CB.CB_NIVEL9 = @TB_CODIGO AND @NivelSupervisores = '9')
	    		)
    		)
END
GO

CREATE PROCEDURE SP_AJUSTA_SUPERVISOR_ENROLADOS( @Nivel Formula, @TB_CODIGO Codigo, @US_CODIGO Usuario)
AS
BEGIN 
	SET NOCOUNT ON 
   	DECLARE @NivelSupervisores Formula
	
   	SET @NivelSupervisores =  DBO.SP_GET_NIVEL_SUPERVISORES()
   	
	IF  DBO.SP_TIENE_ENROLAMIENTO_SUPERVISORES() = 'S' AND @NivelSupervisores = @Nivel
    	BEGIN 
    		EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS_COMPARTE @NivelSupervisores, @TB_CODIGO, @US_CODIGO    		
    	END
END
GO

CREATE PROCEDURE SP_WFGETTIPONOMINA
@CB_CODIGO INT,
@VERSION   VARCHAR(10)
AS
BEGIN

	IF ( @VERSION = '2006' )
	BEGIN
		SELECT 'TIPO_NOMINA' = TURNO.TU_NOMINA
		FROM COLABORA
		LEFT OUTER JOIN TURNO
		ON ( TURNO.TU_CODIGO = ( SELECT DBO.SP_GET_TURNO( GETDATE(), COLABORA.CB_CODIGO ) ) )
		WHERE (  COLABORA.CB_CODIGO = @CB_CODIGO )
	END

	IF ( @VERSION = '2007')
	BEGIN

		SELECT 'TIPO_NOMINA' = CB_NOMINA
		FROM COLABORA
		WHERE CB_CODIGO = @CB_CODIGO

	END
END
GO

CREATE PROCEDURE DBO.SP_WFGETEMPLEADOVACACIONES(@EMPLEADO NUMEROEMPLEADO , @CONFIDEN CODIGO, @USUARIO USUARIO, @EMPLEADO_L NUMEROEMPLEADO)  
AS BEGIN  
 DECLARE @FECHA DESCRIPCION  
 SET @FECHA = LTRIM(RTRIM(STR( MONTH(GETDATE()) )))+'/'+ LTRIM(RTRIM(STR( DAY(GETDATE()) )))+'/'+ LTRIM(RTRIM(STR( YEAR(GETDATE()))) )  
  
 SELECT  CB_CODIGO, CB_APE_PAT+' '+CB_APE_MAT+', '+CB_NOMBRES AS PRETTYNAME, C.CB_ACTIVO, C.CB_FEC_ING, C.CB_FEC_BAJ,  
  C.CB_DER_FEC, C.CB_DER_PAG, C.CB_DER_GOZ, C.CB_V_GOZO, C.CB_V_PAGO, C.CB_FEC_ANT, C.CB_FEC_VAC,  
  CB_TOT_PAG = CB_DER_PAG - CB_V_PAGO,  
  CB_TOT_GOZ = CB_DER_GOZ - CB_V_GOZO, 0.0 AS CB_SUB_PAG, 0.0 AS CB_SUB_GOZ, C.CB_ACTIVO,  
  0.0 AS CB_PRO_PAG, 0.0 AS CB_PRO_GOZ, 0.0 AS CB_PEN_PAG, 0.0 AS CB_PEN_GOZ, C.CB_TABLASS,  
  PORC_PRIMA = DBO.GETVACAPROPORCIONAL(C.CB_CODIGO, GETDATE()), TU_VACA_HA = ISNULL(T.TU_VACA_HA,0)  
  FROM COLABORA C  
  LEFT OUTER JOIN PUESTO P ON( P.PU_CODIGO = CB_PUESTO )  
  LEFT OUTER JOIN TURNO T ON ( T.TU_CODIGO = CB_TURNO )  
  LEFT OUTER JOIN CLASIFI CLAS ON (CLAS.TB_CODIGO = C.CB_CLASIFI)  
  LEFT OUTER JOIN CONTRATO CNT ON (CNT.TB_CODIGO = C.CB_CONTRAT)  
  LEFT OUTER JOIN RPATRON RPAT ON (RPAT.TB_CODIGO = C.CB_PATRON)  
  LEFT OUTER JOIN NIVEL1 N1 ON ( N1.TB_CODIGO = C.CB_NIVEL1 )  
  LEFT OUTER JOIN NIVEL2 N2 ON ( N2.TB_CODIGO = C.CB_NIVEL2 )  
  LEFT OUTER JOIN NIVEL3 N3 ON ( N3.TB_CODIGO = C.CB_NIVEL3 )  
  LEFT OUTER JOIN NIVEL4 N4 ON ( N4.TB_CODIGO = C.CB_NIVEL4 )  
  LEFT OUTER JOIN NIVEL5 N5 ON ( N5.TB_CODIGO = C.CB_NIVEL5 )  
  LEFT OUTER JOIN NIVEL6 N6 ON ( N6.TB_CODIGO = C.CB_NIVEL6 )  
  LEFT OUTER JOIN NIVEL7 N7 ON ( N7.TB_CODIGO = C.CB_NIVEL7 )  
  LEFT OUTER JOIN NIVEL8 N8 ON ( N8.TB_CODIGO = C.CB_NIVEL8 )  
  LEFT OUTER JOIN NIVEL9 N9 ON ( N9.TB_CODIGO = C.CB_NIVEL9 )  
  LEFT OUTER JOIN EXTRA1 X1 ON ( X1.TB_CODIGO = C.CB_G_TAB_1 )  
  LEFT OUTER JOIN EXTRA2 X2 ON ( X2.TB_CODIGO = C.CB_G_TAB_2 )  
  LEFT OUTER JOIN EXTRA3 X3 ON ( X3.TB_CODIGO = C.CB_G_TAB_3 )  
  LEFT OUTER JOIN EXTRA4 X4 ON ( X4.TB_CODIGO = C.CB_G_TAB_4 )  
  LEFT OUTER JOIN ENTIDAD E ON (E.TB_CODIGO = C.CB_ENT_NAC)  
  WHERE
( 
  (@CONFIDEN = '' OR CB_NIVEL0=@CONFIDEN)  
  AND (
            C.CB_CODIGO=@EMPLEADO AND 
            (
                        @USUARIO = -1 
                        OR 
                (
                      SELECT COUNT(*) FROM SP_LISTA_EMPLEADOS( @FECHA, @USUARIO ) LISTA  
                               LEFT OUTER JOIN COLABORA C  
                               ON LISTA.CB_CODIGO = C.CB_CODIGO  
                               WHERE LISTA.CB_CODIGO = @EMPLEADO  
                             AND (@CONFIDEN ='' OR CB_NIVEL0 = @CONFIDEN)
                 ) > 0
      
                  ) 
       ) 
  )  
  OR ( CB_CODIGO = @EMPLEADO_L AND CB_CODIGO = @EMPLEADO)  
END
GO

CREATE PROCEDURE DBO.SP_WFGETPERIODODATOS(@YEAR ANIO, @TPO_NOMINA NOMINATIPO, @NUM_NOMINA NOMINANUMERO)
AS
BEGIN
	SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ,
	PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC,
	PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM,
	PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG,
	US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
	PE_PERIODO_TEXTO = 'DE ' + (CASE
								WHEN DATEPART(DD,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI) )
								ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI))
								END) + '/'
							  + (CASE
								WHEN DATEPART(MM,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI) )
								ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI))
								END)+ '/'
							  +	CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_INI)) +
						' A ' + (CASE
								WHEN DATEPART(DD,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN) )
								ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN))
								END) + '/'
							  + (CASE
								WHEN DATEPART(MM,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN) )
								ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN))
								END)+ '/'
							  +	CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_FIN))
	FROM PERIODO
	WHERE ( PE_YEAR = @YEAR )
	AND ( PE_TIPO = @TPO_NOMINA )
	AND ( PE_NUMERO = @NUM_NOMINA )
END
GO

CREATE PROCEDURE SP_WFGETTIPOPERIODO
AS
BEGIN

	SELECT 	CODIGO = TP_TIPO, DESCRIPCION = TP_DESCRIP, TP_TIPO, TP_NOMBRE, TP_DESCRIP, TP_DIAS, TP_HORAS
			TP_DIAS_BT, TP_DIAS_7, TP_DIAS_EV
	FROM TPERIODO
	WHERE TP_DESCRIP <> ''

END
GO

CREATE PROCEDURE SP_WFGETDIASGOZADOS ( @CB_CODIGO INT, @FECHAINI DATETIME, @FECHAFIN DATETIME )
AS
BEGIN
	SELECT CONVERT( INT ,ISNULL( SUM(VA_GOZO), 0))  AS TOTALDIAS
	FROM VACACION
	WHERE CB_CODIGO=@CB_CODIGO
	AND VA_FEC_INI BETWEEN @FECHAINI AND @FECHAFIN
	AND VA_TIPO = 1
END
GO

CREATE PROCEDURE DBO.SP_WFGETPERIODOACTUALDATOS(@YEAR ANIO, @TPO_NOMINA NOMINATIPO )
AS
BEGIN
		DECLARE
		@MAX_PERIODO INT,
		@STATUS INT
		SET @STATUS = 0;

		 SELECT @MAX_PERIODO= (SELECT ISNULL(MAX(PE_NUMERO),1)       FROM PERIODO
			   WHERE ( PE_YEAR = @YEAR )
			   AND ( PE_TIPO = @TPO_NOMINA )
			   AND ( PE_NUMERO < 200 )
			   AND ( PE_STATUS <> @STATUS ) )

		SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ,
		PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC,
		PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM,
		PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG,
		US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
		PE_PERIODO_TEXTO = 'DE ' + (CASE
									WHEN DATEPART(DD,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI) )
									ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI))
									END) + '/'
								 + (CASE
									WHEN DATEPART(MM,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI) )
									ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI))
									END)+ '/'
								 +            CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_INI)) +
									' A ' + (CASE
									WHEN DATEPART(DD,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN) )
									ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN))
									END) + '/'
								 + (CASE
									WHEN DATEPART(MM,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN) )
									ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN))
									END)+ '/'
								 +            CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_FIN))
		FROM PERIODO
		WHERE ( PE_YEAR = @YEAR )
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO =  @MAX_PERIODO )
		AND ( PE_STATUS <> @STATUS )
END
GO

create procedure SP_WFGetPeriodoActualDatosNoAfectados(@YEAR Anio, @TPO_NOMINA NominaTipo, @TomarInicio Booleano = 'N', @FechaVacIni Fecha = '1899-12-30' )
as
begin
	DECLARE
	@MAX_PERIODO INT,
	@STATUS INT
	SET @STATUS = 0;
	SET @MAX_PERIODO = 0;

    if ( @TomarInicio = 'S')
	begin
		SELECT @MAX_PERIODO= (SELECT ISNULL(MIN(PE_NUMERO),0)
		FROM PERIODO
		WHERE ( PE_YEAR = @YEAR )
			AND ( PE_TIPO = @TPO_NOMINA )
			AND ( PE_NUMERO < 200 )
			AND ( PE_STATUS < 5 )
			AND ( @FechaVacIni between PE_ASI_INI and PE_ASI_FIN )
			AND ( PE_CAL = 'S' ) 	)
	end

	if ( @TomarInicio = 'N') or ( @MAX_PERIODO <= 0 )
	begin
		SELECT @MAX_PERIODO= (SELECT ISNULL(MAX(PE_NUMERO),1)
		FROM PERIODO
		WHERE ( PE_YEAR = @YEAR )
			AND ( PE_TIPO = @TPO_NOMINA )
			AND ( PE_NUMERO < 200 )
			AND ( PE_STATUS <> @STATUS )
			AND ( PE_STATUS < 5 ) )
	end

	SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ,
		PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC,
		PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM,
		PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG,
		US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
		PE_PERIODO_TEXTO = 'De ' + (case
								when datepart(dd,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_INI) )
								else convert(varchar(2), datepart(dd,PE_FEC_INI))
								end) + '/'
							 + (case
								when datepart(mm,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_INI) )
								else convert(varchar(2), datepart(mm,PE_FEC_INI))
								end)+ '/'
							 +            convert(varchar(4), datepart(yyyy,PE_FEC_INI)) +
								' a ' + (case
								when datepart(dd,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_FIN) )
								else convert(varchar(2), datepart(dd,PE_FEC_FIN))
								end) + '/'
							 + (case
								when datepart(mm,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_FIN) )
								else convert(varchar(2), datepart(mm,PE_FEC_FIN))
								end)+ '/'
							 +            convert(varchar(4), datepart(yyyy,PE_FEC_FIN))
	FROM PERIODO
	WHERE ( PE_YEAR = @YEAR )
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO =  @MAX_PERIODO )
		AND ( PE_STATUS < 5 )
end
GO

create procedure SP_WFGetPeriodoDatosNoAfectados(@YEAR Anio, @TPO_NOMINA NominaTipo, @NUM_NOMINA NominaNumero)
as
begin
	SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ, 
		PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC, 
		PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM,
		PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG, 
		US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
		PE_PERIODO_TEXTO = 'De ' + (case
								when datepart(dd,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_INI) )
								else convert(varchar(2), datepart(dd,PE_FEC_INI))
								end) + '/'
							  + (case 
								when datepart(mm,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_INI) )
								else convert(varchar(2), datepart(mm,PE_FEC_INI))
								end)+ '/'
							  +	convert(varchar(4), datepart(yyyy,PE_FEC_INI)) +
						' a ' + (case 
								when datepart(dd,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_FIN) )
								else convert(varchar(2), datepart(dd,PE_FEC_FIN))
								end) + '/'
							  + (case
								when datepart(mm,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_FIN) )
								else convert(varchar(2), datepart(mm,PE_FEC_FIN))
								end)+ '/'
							  +	convert(varchar(4), datepart(yyyy,PE_FEC_FIN))
	FROM PERIODO
	WHERE ( PE_YEAR = @YEAR )
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO = @NUM_NOMINA )
		AND ( PE_STATUS < 5 )
end
go

CREATE PROCEDURE Costos_CalcProporciones_Hook(
	@NominaID	FolioGrande )
AS
BEGIN
	/* 0 = Centro de Costos Default */
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase )
	SELECT	@NominaID, 0,
			dbo.SP_NOMINA_COSTEO( NOMINA.CB_CODIGO, NOMINA.PE_YEAR, NOMINA.PE_TIPO, NOMINA.PE_NUMERO ),
			1
	FROM	NOMINA
	WHERE	NOMINA.LLAVE = @NominaID

	/* 1 = Horas Ordinarias */
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase )
	SELECT	@NominaID, 1, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), SUM( AUSENCIA.AU_HORAS )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
	WHERE	NOMINA.LLAVE = @NominaID AND AUSENCIA.AU_HORAS > 0
	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	/* 1.1 Horas Ordinarias Transferidas */
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
	SELECT	@NominaID, 1, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), 'Transferidas', SUM( -TR_HORAS )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
				JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 0 AND TR_STATUS = 1
	WHERE	NOMINA.LLAVE = @NominaID
	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	/* 1.2 Horas Ordinarias Recibidas para el otro centro de Costo */
	IF ( @@ROWCOUNT > 0 )
		INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
		SELECT	@NominaID, 1, TRANSFER.CC_CODIGO, 'Recibidas', SUM( TR_HORAS )
		FROM	NOMINA
					JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
					JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 0 AND TR_STATUS = 1
		WHERE	NOMINA.LLAVE = @NominaID
		GROUP BY TRANSFER.CC_CODIGO
	
	/* 2 = Horas Extras */
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoBase )
	SELECT	@NominaID, 2, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), SUM( AUSENCIA.AU_EXTRAS + AUSENCIA.AU_DES_TRA )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
	WHERE	NOMINA.LLAVE = @NominaID AND ( AUSENCIA.AU_EXTRAS > 0 OR AUSENCIA.AU_DES_TRA > 0 )
	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	/* 2.1 Horas Extras Transferidas */
	INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
	SELECT	@NominaID, 2, dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA), 'Transferidas', SUM( -TR_HORAS )
	FROM	NOMINA
				JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
				JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 1 AND TR_STATUS = 1
	WHERE	NOMINA.LLAVE = @NominaID

	GROUP BY dbo.SP_AUSENCIA_COSTEO(AUSENCIA.CB_CODIGO, AUSENCIA.AU_FECHA)

	/* 2.2 Horas Extras Recibidas para el otro centro de Costo */
	IF ( @@ROWCOUNT > 0 )
		INSERT INTO T_ProporcionCostos ( NominaID, CritCostoID, CC_CODIGO, PropCostoClasifi, PropCostoBase )
		SELECT	@NominaID, 2, TRANSFER.CC_CODIGO, 'Recibidas', SUM( TR_HORAS )
		FROM	NOMINA
					JOIN AUSENCIA ON NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO AND AUSENCIA.AU_FECHA BETWEEN NOMINA.NO_ASI_INI AND NOMINA.NO_ASI_FIN
					JOIN TRANSFER ON AUSENCIA.CB_CODIGO = TRANSFER.CB_CODIGO AND AUSENCIA.AU_FECHA = TRANSFER.AU_FECHA AND TR_TIPO = 1 AND TR_STATUS = 1
		WHERE	NOMINA.LLAVE = @NominaID
		GROUP BY TRANSFER.CC_CODIGO
END
GO

CREATE PROCEDURE Costos_CalcProporciones(
	@NominaID	FolioGrande )
AS
BEGIN

	DELETE	T_ProporcionTotales
	WHERE	NominaID = @NominaID

	DELETE	T_ProporcionCostos
	WHERE	NominaID = @NominaID


	EXEC Costos_CalcProporciones_Hook @NominaID


	INSERT INTO T_ProporcionTotales ( NominaID, CritCostoID, PropTotalConteo, PropTotalSuma )
	SELECT	@NominaID, CritCostoID, COUNT(*), SUM( PropCostoBase )
	FROM	T_ProporcionCostos
	WHERE	NominaID = @NominaID
	GROUP BY CritCostoID
END
GO

CREATE PROCEDURE Costos_ReparteCriterio(
	@NominaID		FolioGrande,
	@CO_NUMERO		Concepto,
	@MO_REFEREN		Referencia,
	@GpoCostoID		FolioGrande,
	@CritCostoID	FolioGrande,
	@Monto			Pesos )
AS
BEGIN
	DECLARE @Conteo			FolioGrande
	DECLARE @SumaBase		Horas
	DECLARE @SumaMonto		Pesos
	DECLARE @Diferencia		Pesos
	DECLARE @CostoID		FolioGrande

	SELECT	@Conteo		= PropTotalConteo,
			@SumaBase	= PropTotalSuma
	FROM	T_ProporcionTotales
	WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID

	/* No hay horas base para ese Criterio */
	IF ( @Conteo IS NULL OR @Conteo = 0 OR @SumaBase = 0 )
		RETURN 0

	IF ( @Conteo = 1 )	/* Hay un solo Centro de Costo, reparte el 100% del monto a ese registro */
	BEGIN
		INSERT INTO T_Costos ( NominaID, CO_NUMERO, MO_REFEREN, GpoCostoID, CC_CODIGO, CostoClasifi, CostoMonto )
		SELECT	 @NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, CC_CODIGO, PropCostoClasifi, @Monto
		FROM	T_ProporcionCostos
		WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID
	END
	ELSE	/* Varios centros de Costo, reparte proporcional */
	BEGIN
		INSERT INTO T_Costos ( NominaID, CO_NUMERO, MO_REFEREN, GpoCostoID, CC_CODIGO, CostoClasifi, CostoMonto )
		SELECT	@NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, CC_CODIGO, PropCostoClasifi, ROUND( @Monto * PropCostoBase / @SumaBase , 2 )
		FROM	T_ProporcionCostos
		WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID

		/* Asegurar que no se pierdan centavos en el redondeo
		 Suma el monto efectivamente repartido
		 Obtiene el ID del primer costo para enviar diferencia */
		SELECT	@SumaMonto	= SUM( CostoMonto ),
				@CostoID	= MIN( CostoID )
		FROM	T_Costos
		WHERE	NominaID = @NominaID AND CO_NUMERO = @CO_NUMERO AND MO_REFEREN = @MO_REFEREN AND GpoCostoID = @GpoCostoID

		/* Lo compara contra el monto que tenia que repartir */
		SET @Diferencia = @Monto - @SumaMonto

		/* Si se perdieron se mandan arbitrariamente al primer renglon */
		IF ( @Diferencia <> 0 )
			UPDATE	T_Costos
			SET		CostoMonto = CostoMonto + @Diferencia
			WHERE	CostoID = @CostoID
	END

	RETURN @Conteo
END
GO

CREATE PROCEDURE Costos_CalculaFijos_Hook(
	@NominaID	FolioGrande )
AS
BEGIN
	SET NOCOUNT ON

	DECLARE	@GpoCostoID		FolioGrande
	DECLARE @CritCostoID	FolioGrande

	SET @CritCostoID	= 102
	SET @GpoCostoID		= 7

	INSERT INTO T_Costos ( NominaID, CO_NUMERO, MO_REFEREN, GpoCostoID, CC_CODIGO, CostoClasifi, CostoMonto )
	SELECT	@NominaID, 0, '', @GpoCostoID, T_ProporcionCostos.CC_CODIGO, PropCostoClasifi, PropCostoBase * CAST( CC_SUB_CTA AS FLOAT ) / 1000.0
	FROM	T_ProporcionCostos
				JOIN CCOSTO ON T_ProporcionCostos.CC_CODIGO = CCOSTO.CC_CODIGO
	WHERE	NominaID = @NominaID AND CritCostoID = @CritCostoID
END
GO

create procedure COSTO_SUBCTA_CARGO_HIJO(
	@CostoID FolioGrande,
	@NominaID FolioGrande,
	@Concepto Concepto,
	@CostoClasifi OrdenTrabajo,
	@CC_codigo Codigo,
	@Monto Pesos
	)
as
begin
	/* En este SP, se programan las reglas para crear las cuentas contables*/
	return 0
end
GO

create procedure COSTO_SUBCTA_ABONO_hijo(
	@CostoID FolioGrande,
	@NominaID FolioGrande,
	@Concepto Concepto,
	@CostoClasifi OrdenTrabajo,
	@CC_codigo Codigo,
	@Monto Pesos
	)
as
begin
	/* En este SP, se programan las reglas para crear las cuentas contables*/
	return 0
end
GO

CREATE PROCEDURE dbo.Costos_CalculaCuentasContables_Hook( @NominaID	FolioGrande )
AS
BEGIN
	declare @CostoID FolioGrande;
	declare @Concepto Concepto;
	declare @CostoClasifi OrdenTrabajo;
	declare @CC_codigo Codigo;
	declare @Monto Pesos;


	DELETE	T_Costos_SubCta
	WHERE	NominaID = @NominaID

	DECLARE CursorCostos CURSOR LOCAL FOR
		select CostoID, CO_NUMERO, CostoClasifi, CC_CODIGO, CostoMonto
		from T_COSTOS
		where NominaID = @NominaID

	OPEN CursorCostos
	FETCH NEXT FROM CursorCostos
	INTO	@CostoID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto

	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN
		exec COSTO_SUBCTA_Cargo_HIJO @CostoID, @NominaID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto
		exec COSTO_SUBCTA_Abono_HIJO @CostoID, @NominaID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto

		FETCH NEXT FROM CursorCostos
		INTO	@CostoID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto
	END

	CLOSE CursorCostos
	DEALLOCATE CursorCostos
END
GO

CREATE PROCEDURE Costos_CalculaNomina(
	@NominaID	FolioGrande )
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @GpoCostoID		FolioGrande
	DECLARE @CritCostoID	FolioGrande
	DECLARE @Monto			Pesos
	DECLARE @Registros		FolioGrande
	DECLARE @CO_NUMERO		Concepto
	DECLARE @MO_REFEREN		Referencia

	BEGIN TRANSACTION


	DELETE	T_Costos
	WHERE	NominaID = @NominaID


	EXEC Costos_CalcProporciones @NominaID


	DECLARE CursorConceptosCosto CURSOR LOCAL FOR
		SELECT	MOVIMIEN.CO_NUMERO, MO_REFEREN, CC.GpoCostoID, CC.CritCostoID, ( MO_PERCEPC+MO_DEDUCCI )
		FROM	NOMINA
					JOIN MOVIMIEN ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR
										AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO
										AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO
										AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
					JOIN T_ConceptosCosto CC ON MOVIMIEN.CO_NUMERO = CC.CO_NUMERO
					JOIN T_GruposCosto ON CC.GpoCostoID = T_GruposCosto.GpoCostoID
					JOIN T_CriteriosCosto ON CC.CritCostoID = T_CriteriosCosto.CritCostoID
		WHERE	NOMINA.LLAVE = @NominaID
				AND MOVIMIEN.MO_ACTIVO = 'S' AND ( MO_PERCEPC+MO_DEDUCCI ) <> 0
				AND GpoCostoActivo = 'S' AND CritCostoActivo = 'S'

	OPEN CursorConceptosCosto
	FETCH NEXT FROM CursorConceptosCosto
	INTO	@CO_NUMERO, @MO_REFEREN, @GpoCostoID, @CritCostoID, @Monto

	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN

		EXEC @Registros = Costos_ReparteCriterio @NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, @CritCostoID, @Monto
		IF ( @Registros = 0 )
		BEGIN
			EXEC @Registros = Costos_ReparteCriterio @NominaID, @CO_NUMERO, @MO_REFEREN, @GpoCostoID, 0, @Monto
			IF ( @Registros = 0 )
			BEGIN
				RAISERROR( 'Falt� definir la proporci�n del Criterio CERO', 16, 1 )
				BREAK
			END
		END

		FETCH NEXT FROM CursorConceptosCosto
		INTO	@CO_NUMERO, @MO_REFEREN, @GpoCostoID, @CritCostoID, @Monto
	END

	CLOSE CursorConceptosCosto
	DEALLOCATE CursorConceptosCosto



	EXEC Costos_CalculaFijos_Hook @NominaID



	EXEC Costos_CalculaCuentasContables_Hook @NominaID


	COMMIT TRANSACTION
END
GO

CREATE PROCEDURE Costos_CalculaPeriodo(
	@PE_YEAR	Anio,
	@PE_TIPO	NominaTipo,
	@PE_NUMERO	NominaNumero )
AS
BEGIN
	DECLARE @NominaID	FolioGrande

	DECLARE CursorNOMINA CURSOR LOCAL FOR
		SELECT		LLAVE
		FROM		NOMINA
		WHERE		PE_YEAR = @PE_YEAR AND PE_TIPO = @PE_TIPO AND PE_NUMERO = @PE_NUMERO
		ORDER BY	CB_CODIGO

	OPEN CursorNOMINA
	FETCH NEXT FROM CursorNOMINA
	INTO	@NominaID

	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN

		EXEC Costos_CalculaNomina @NominaID

		FETCH NEXT FROM CursorNOMINA
		INTO	@NominaID
	END

	CLOSE CursorNOMINA
	DEALLOCATE CursorNOMINA
END
GO

CREATE PROCEDURE RECALCULA_PLAZAS(@Kardex char(1)) AS
BEGIN
   SET NOCOUNT ON
   declare @Empleado int;
   declare @Plaza int;
   declare @Fecha DateTime;

   declare CursorPlazas CURSOR FOR
   select	CB_CODIGO, PL_FOLIO
   from PLAZA
   where CB_CODIGO != 0
   order by PL_FOLIO

   Open CursorPlazas
   Fetch NEXT from CursorPlazas
   into	@Empleado,@Plaza

   while ( @@FETCH_STATUS = 0 )
   BEGIN
		if @Kardex = 'S'
    begin
			SELECT TOP 1 @Fecha =  CB_FECHA FROM kardex WHERE CB_CODIGO = @Empleado and CB_TIPO = 'ALTA' ORDER BY CB_FECHA asc
			Delete from Kardex where CB_CODIGO = @Empleado and CB_TIPO ='PLAZA';
			Update Kardex set CB_PLAZA = @Plaza
			where CB_CODIGO = @Empleado and
				  CB_TIPO = 'ALTA' and
				  CB_FECHA = @Fecha;
			EXECUTE RECALCULA_KARDEX @Empleado;
        end
	    else
	    begin
      Update COLABORA set CB_PLAZA = 0 WHERE cb_codigo = @Empleado;

		end
    Fetch NEXT from CursorPlazas
	  into	@Empleado,@Plaza
   END
   Close CursorPlazas
   Deallocate CursorPlazas

END
GO

create procedure SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA ( @EN_CODIGO FolioChico, @CODIGO Codigo,  @ListaNIVEL0 Formula)
as
begin
		set NOCOUNT ON

    	DELETE FROM CONF_TAB where CT_CODIGO = @CODIGO and CT_TABLA = @EN_CODIGO

		INSERT INTO CONF_TAB ( CT_TABLA, CT_CODIGO, CT_NIVEL0 )
		select @EN_CODIGO, @CODIGO, Items
		from dbo.FN_SPLIT(  @ListaNIVEL0, ',' )
end
GO

create procedure SP_ACTUALIZA_CONDIFDENCIALIDAD( @EN_CODIGO FolioChico, @Codigo Codigo )
as
begin
		declare @NIVEL0 Formula
		declare @ListaNIVEL0 Formula

		set @ListaNIVEL0 = ''

		select
			@ListaNivel0 =
			(
			case @EN_CODIGO
				when 59 then (select PU_NIVEL0 from PUESTO where PU_CODIGO = @Codigo )
				when 69 then (select TU_NIVEL0 from TURNO where TU_CODIGO = @Codigo )
				when 27 then (select HO_NIVEL0 from HORARIO where HO_CODIGO = @Codigo )

				when 9 then  (select TB_NIVEL0 from CLASIFI	 where TB_CODIGO = @Codigo )
				when 63 then (select TB_NIVEL0 from SSOCIAL	 where TB_CODIGO = @Codigo )
				when 12 then (select TB_NIVEL0 from CONTRATO where TB_CODIGO = @Codigo )
				when 41 then (select TB_NIVEL0 from NIVEL1	 where TB_CODIGO = @Codigo )
				when 42 then (select TB_NIVEL0 from NIVEL2	 where TB_CODIGO = @Codigo )
				when 43 then (select TB_NIVEL0 from NIVEL3	 where TB_CODIGO = @Codigo )
				when 44 then (select TB_NIVEL0 from NIVEL4	 where TB_CODIGO = @Codigo )
				when 45 then (select TB_NIVEL0 from NIVEL5	 where TB_CODIGO = @Codigo )
				when 46 then (select TB_NIVEL0 from NIVEL6	 where TB_CODIGO = @Codigo )
				when 47 then (select TB_NIVEL0 from NIVEL7	 where TB_CODIGO = @Codigo )
				when 48 then (select TB_NIVEL0 from NIVEL8	 where TB_CODIGO = @Codigo )
				when 49 then (select TB_NIVEL0 from NIVEL9	 where TB_CODIGO = @Codigo )
				when 62 then (select TB_NIVEL0 from RPATRON	 where TB_CODIGO = @Codigo )
				when 220 then (select TB_NIVEL0 from COLONIA where TB_CODIGO = @Codigo )
				when 15 then (select TB_NIVEL0 from ENTIDAD	 where TB_CODIGO = @Codigo )
				when 64 then (select TB_NIVEL0 from TAHORRO	 where TB_CODIGO = @Codigo )
				when 67 then (select TB_NIVEL0 from TPRESTA	 where TB_CODIGO = @Codigo )

				when 129 then (select TP_NIVEL0 from TPERIODO where TP_TIPO = CAST( @Codigo as int ) )

				else ''
			end
			)

		set @ListaNIVEL0 = coalesce( @ListaNivel0, '' )
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA @EN_CODIGO, @Codigo, @ListaNivel0

end;
GO

CREATE procedure SP_TIMBRAR_EMPLEADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @CB_CODIGO NumeroEmpleado, @CB_RFC Descripcion, @Status status, @Timbre Formula, @FolioTimbramex FolioGrande, @Usuario usuario, @FacturaUUID  DescLarga = '' )
as 
begin 
	declare @StatusTimbrado status 
	declare @StatusTimbradoPendiente status    
	declare @StatusTimbradoParcial status    
	declare @StatusTimbradoTotal status    
	declare @StatusTimbradoPendienteNuevo status
	declare @StatusCancelacionPendienteNuevo status
	declare @StatusTimbradoPendienteLimpiar status
	declare @StatusCancelacionPendienteLimpiar status
   
	set @StatusTimbradoPendiente = 0
	set @StatusTimbradoParcial = 1   
	set @StatusTimbradoTotal = 2   
	set @StatusTimbradoPendienteNuevo = 3
	set @StatusCancelacionPendienteNuevo = 4
	set @StatusTimbradoPendienteLimpiar = 5
	set @StatusCancelacionPendienteLimpiar = 6

	if ( @Status = @StatusTimbradoPendienteLimpiar )
	begin 
		   update NOMINA set NO_TIMBRO = @StatusTimbradoPendiente
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 and NO_TIMBRO = @StatusTimbradoPendienteNuevo   
	end	
	else
	if ( @Status = @StatusCancelacionPendienteLimpiar )
	begin 
		   update NOMINA set NO_TIMBRO = @StatusTimbradoTotal
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 and NO_TIMBRO = @StatusCancelacionPendienteNuevo   
	end
	else
	if ( @Status = @StatusTimbradoPendiente )
	begin 
		   update NOMINA set NO_TIMBRO = @Status, NO_SELLO = '',  NO_FACTURA = 0, NO_FACUUID = '', NO_CAN_UID = NO_FACUUID 
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 
	end
	else
	if ( @Status = @StatusTimbradoTotal )
	begin 
		   declare @verTotal FolioGrande     
		   set @verTotal = dbo.KardexConceptos_GetVersionActual() 

		   update NOMINA set NO_TIMBRO = @Status, NO_SELLO = @Timbre , NO_FACTURA = @FolioTimbramex  , NO_FACUUID = @FacturaUUID, NO_VER_TIM  = @verTotal 
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO 
		   and NO_STATUS = 6  
	end
	else
	if ( @Status = @StatusTimbradoPendienteNuevo ) or( @Status = @StatusCancelacionPendienteNuevo )
	begin 
		   update NOMINA set NO_TIMBRO = @Status
		   where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
		   and NO_STATUS = 6 
	end
end
GO

CREATE procedure AFECTAR_STATUS_TIMBRADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @StatusActual status, @StatusNuevo status )   
as  
begin  
	exec TimbradoConcilia_CalcularStatusPeriodo @PE_YEAR, @PE_TIPO, @PE_NUMERO, @StatusActual, @StatusNuevo	        
end

GO

create procedure SP_ACTUALIZA_FACTURA_UUID( @FacturaNo FolioGrande,  @FacturaUUID  DescLarga)  
as
begin 
       set nocount on 
       update NOMINA set NO_FACUUID = @FacturaUUID where NO_FACTURA = @FacturaNo
end
GO

create procedure SP_Dashlet_Indicador_EmpleadosActivos( @Fecha Fecha,  @IndicadorValor int output, @FechaAnterior fecha, @IndicadorTendencia Pesos output, @CM_NIVEL0 Formula = ''  ) 
as
begin 
	set nocount on 

	declare  @IndicadorValorAnterior int 

	select 
		@IndicadorValor = SUM( 	
		case 
			when dbo.SP_STATUS_ACT( @Fecha, CB_CODIGO )= 1 then 1
			else 0 
		end 
		), 
		@IndicadorValorAnterior = SUM( 	
		case 
			when dbo.SP_STATUS_ACT( @FechaAnterior, CB_CODIGO )= 1 then 1
			else 0 
		end 
		) 
	from COLABORA 
	where @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='')

	if (@IndicadorValor > 0  )
		set @IndicadorTendencia =  ( ( @IndicadorValor*1.0 - @IndicadorValorAnterior*1.0 ) / @IndicadorValor*1.0 ) * 100.00
	else 
		set @IndicadorTendencia = 0; 

end 
go 

create procedure SP_Dashlet_Indicador_SinTimbrar(@Fecha Fecha, @PE_TIPO int, @PE_NUMERO int, @PE_YEAR int,  @IndicadorValor int output, @IndicadorTendencia Pesos output, @CM_NIVEL0 Formula = '' ) 
as
begin 
	set nocount on 

	declare  @IndicadorValorAnterior int 
	declare  @PeMes int 
	declare  @PeMesAnterior int 
	declare  @PeYearAnterior int 

	select @PeMEs = PE_MES from PERIODO where PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO and PE_YEAR = @PE_YEAR 
	set @IndicadorValor = dbo.FN_GetNominasSinTimbrar(@PE_YEAR, @PeMEs, @CM_NIVEL0 ) 

	if ( @PeMes = 1 ) 
	begin 
		set @PeMesAnterior = 12 
		set @PeYearAnterior = @Pe_YEAR -1 
	end 
	else
	begin 
		set @PeMesAnterior =  @PeMes - 1  
		set @PeYearAnterior = @Pe_YEAR 
	end

	set @IndicadorValorAnterior = dbo.FN_GetNominasSinTimbrar(@PeYearAnterior, @PeMesAnterior, @CM_NIVEL0 ) 
	
	
	if (@IndicadorValor > 0  )
		set @IndicadorTendencia =  ( ( @IndicadorValor*1.0 - @IndicadorValorAnterior*1.0 ) / @IndicadorValor*1.0 ) * 100.00
	else 
		set @IndicadorTendencia = 0; 

end 
GO 

create procedure SP_Dashlet_Indicador_ContratosRenovar( @Fecha Fecha,  @IndicadorValor int output, @FechaAnterior fecha, @IndicadorTendencia Pesos output, @CM_NIVEL0 Formula = '' ) 
as
begin 
	set nocount on 

	declare  @IndicadorValorAnterior int 

	declare @FinDeMes Fecha 
	Select @FinDeMes =  DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Fecha)+1,0))
	declare @FinDeMesAnterior  Fecha 
	SELECT @FinDeMesAnterior = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@Fecha),0))

	select 
		@IndicadorValor = SUM( 	
		   case 
				when ( CB_FEC_COV between '2000-01-01' and @FinDeMes  )and dbo.SP_STATUS_ACT( @FinDeMes, CB_CODIGO )= 1 then 1 
				else 0 
		   end 
		), 
		@IndicadorValorAnterior = SUM( 	
		case 
			when ( dbo.SP_KARDEX_CB_FEC_COV( @FinDeMesAnterior, CB_CODIGO ) between '2000-01-01' and @FinDeMesAnterior ) and dbo.SP_STATUS_ACT( @FinDeMesAnterior, CB_CODIGO )= 1 then 1  
			else 0 
		end 
		) 
	from COLABORA 
	where @CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0='')

	if (@IndicadorValor > 0  )
		set @IndicadorTendencia =  ( ( @IndicadorValor*1.0 - @IndicadorValorAnterior*1.0 ) / @IndicadorValor*1.0 ) * 100.00
	else 
		set @IndicadorTendencia = 0; 


end 
GO 

CREATE  PROCEDURE AFECTA_UN_CONCEPTO_RAZON_SOCIAL ( 
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@CONCEPTO INTEGER,
    				@MES SMALLINT,
    				@FACTOR INTEGER,
    				@MONTO NUMERIC(15,2), 				
					@RS_CODIGO Codigo				
					)
AS
BEGIN 
	Select @Monto = @Factor * @Monto
    if ( @Monto <> 0 ) 
    begin        
		select @FACTOR = COUNT(*) from ACUMULA_RS A where			 	
				( RS_CODIGO = @RS_CODIGO ) and 
                ( AC_YEAR = @Anio ) and
                ( CB_CODIGO = @Empleado ) and
                ( CO_NUMERO = @Concepto ) 
        if ( @Factor = 0 ) 
		begin 
            insert into ACUMULA_RS (RS_CODIGO, AC_YEAR, CB_CODIGO, CO_NUMERO )  values ( @RS_CODIGO, @Anio, @Empleado, @Concepto );				
		end 


        if ( @Mes = 1 ) 
            update ACUMULA_RS  
		set AC_MES_01 = AC_MES_01 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 2 ) 
				update ACUMULA_RS set AC_MES_02 = AC_MES_02 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 	
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 3 ) 
				update ACUMULA_RS set AC_MES_03 = AC_MES_03 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 4 ) 
				update ACUMULA_RS set AC_MES_04 = AC_MES_04 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 5 ) 
				update ACUMULA_RS set AC_MES_05 = AC_MES_05 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 6 ) 
				update ACUMULA_RS set AC_MES_06 = AC_MES_06 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 7 ) 
				update ACUMULA_RS set AC_MES_07 = AC_MES_07 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 8 ) 
				update ACUMULA_RS set AC_MES_08 = AC_MES_08 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 9 ) 
				update ACUMULA_RS set AC_MES_09 = AC_MES_09 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 10 ) 
				update ACUMULA_RS set AC_MES_10 = AC_MES_10 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 11 ) 
				update ACUMULA_RS set AC_MES_11 = AC_MES_11 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 12 ) 
				update ACUMULA_RS set AC_MES_12 = AC_MES_12 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );
			else
			if ( @Mes = 13 ) 
				update ACUMULA_RS set AC_MES_13 = AC_MES_13 + @Monto 
		where		
					( RS_CODIGO = @RS_CODIGO ) and 
					( AC_YEAR = @Anio ) and
					( CB_CODIGO = @Empleado ) and
					( CO_NUMERO = @Concepto );

		/* No filtra por Razon Social para limpiar AC_ANUAL de las razones sociales no asociadas a la nomina del empleados en caso de datos sucios */ 
		update ACUMULA_RS set AC_ANUAL = (AC_MES_01 +
										AC_MES_02 +
										AC_MES_03 +
										AC_MES_04 +
										AC_MES_05 +
										AC_MES_06 +
										AC_MES_07 +
										AC_MES_08 +
										AC_MES_09 +
										AC_MES_10 +
										AC_MES_11 +
										AC_MES_12 +
										AC_MES_13)
		where ( AC_YEAR = @Anio ) and
			( CB_CODIGO = @Empleado ) and
			( CO_NUMERO = @Concepto );
    end
END 
GO

CREATE  PROCEDURE CALCULA_ACUMULADOS_TOTALES ( 
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@CONCEPTO INTEGER,
    				@MES SMALLINT    			
					)
AS
BEGIN 
	declare @MontoTotal Pesos 
	declare @iExisteAcumula int 

	select @iExisteAcumula = COUNT(*) from ACUMULA A where
			( AC_YEAR = @Anio ) and
			( CB_CODIGO = @Empleado ) and
			( CO_NUMERO = @Concepto ) 

	if ( @iExisteAcumula = 0 ) 
	begin 
		insert into ACUMULA (AC_YEAR, CB_CODIGO, CO_NUMERO )  values ( @Anio, @Empleado, @Concepto );				
	end 
		  
	if ( @Mes = 1 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_01), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_01 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 2 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_02), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_02 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 3 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_03), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_03 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 4 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_04), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_04 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 5 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_05), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_05 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 6 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_06), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_06 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 7 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_07), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_07 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 8 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_08), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_08 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 9 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_09), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );
																
		update ACUMULA  set ACUMULA.AC_MES_09 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 10 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_10), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_10 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 11 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_11), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_11 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 12 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_12), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );	
															
		update ACUMULA  set ACUMULA.AC_MES_12 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	else
	if ( @Mes = 13 ) 
	begin 
		select   @MontoTotal = coalesce( SUM(AR.AC_MES_13), 0 ) 
		from ACUMULA_RS AR 
		where ( AR.AC_YEAR = @Anio ) and ( AR.CB_CODIGO = @Empleado ) and   ( AR.CO_NUMERO = @Concepto );		
														
		update ACUMULA  set ACUMULA.AC_MES_13 = @MontoTotal 
		from ACUMULA 				
		where( AC_YEAR = @Anio ) and  ( CB_CODIGO = @Empleado ) and  ( CO_NUMERO = @Concepto ); 
	end 
	        		                
	update ACUMULA set AC_ANUAL = (AC_MES_01 +
									AC_MES_02 +
									AC_MES_03 +
									AC_MES_04 +
									AC_MES_05 +
									AC_MES_06 +
									AC_MES_07 +
									AC_MES_08 +
									AC_MES_09 +
									AC_MES_10 +
									AC_MES_11 +
									AC_MES_12 +
									AC_MES_13)
	where ( AC_YEAR = @Anio ) and
		( CB_CODIGO = @Empleado ) and
		( CO_NUMERO = @Concepto );    
END
GO

CREATE  PROCEDURE CALCULA_ACUMULADOS_TOTALES_ANUAL ( 
    				@AC_YEAR SMALLINT,
    				@CB_CODIGO INTEGER,
    				@CO_NUMERO INTEGER    						
					)
AS
BEGIN 
	set nocount on 
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 1
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 2
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 3
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 4
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 5
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 6
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 7
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 8
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 9
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 10
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 11
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 12
	exec CALCULA_ACUMULADOS_TOTALES @AC_YEAR, @CB_CODIGO, @CO_NUMERO, 13
END 
GO

CREATE  PROCEDURE LIMPIA_ACUMULADOS_TOTALES( 	
					@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@MES SMALLINT
					) 
AS
BEGIN
	set nocount on 

     if ( @Mes = 1 )
        update ACUMULA set AC_MES_01 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 2 )
        update ACUMULA set AC_MES_02 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 3 )
        update ACUMULA set AC_MES_03 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 4 )
        update ACUMULA set AC_MES_04 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 5 )
        update ACUMULA set AC_MES_05 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 6 )
        update ACUMULA set AC_MES_06 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 7 )
        update ACUMULA set AC_MES_07 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 8 )
        update ACUMULA set AC_MES_08 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 9 )
        update ACUMULA set AC_MES_09 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 10 )
        update ACUMULA set AC_MES_10 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 11 )
        update ACUMULA set AC_MES_11 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 12 )
        update ACUMULA set AC_MES_12 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 13 )
        update ACUMULA set AC_MES_13 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );

END
GO

CREATE  PROCEDURE LIMPIA_ACUMULADOS_RS(
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT
					) 
AS
BEGIN
	set nocount on 	
	 if ( @Mes = 1 )
        update ACUMULA_RS set AC_MES_01 = 0  where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 2 )
        update ACUMULA_RS set AC_MES_02 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 3 )
        update ACUMULA_RS set AC_MES_03 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 4 )
        update ACUMULA_RS set AC_MES_04 = 0  where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 5 )
        update ACUMULA_RS set AC_MES_05 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 6 )
        update ACUMULA_RS set AC_MES_06 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 7 )
        update ACUMULA_RS set AC_MES_07 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 8 )
        update ACUMULA_RS set AC_MES_08 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 9 )
        update ACUMULA_RS set AC_MES_09 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 10 )
        update ACUMULA_RS set AC_MES_10 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 11 )
        update ACUMULA_RS set AC_MES_11 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 12 )
        update ACUMULA_RS set AC_MES_12 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
     else
     if ( @Mes = 13 )
        update ACUMULA_RS set AC_MES_13 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
END
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Create( 
	@AC_ID	FolioGrande OUTPUT, 
	@AC_YEAR Anio ,
	@CB_CODIGO NumeroEmpleado ,			
	@CO_NUMERO Concepto ,
	@RS_CODIGO Codigo , 	
	@AC_MES_01 Pesos ,	
	@AC_MES_02 Pesos ,	
	@AC_MES_03 Pesos ,
	@AC_MES_04 Pesos ,
	@AC_MES_05 Pesos ,
	@AC_MES_06 Pesos ,
	@AC_MES_07 Pesos ,
	@AC_MES_08 Pesos ,
	@AC_MES_09 Pesos ,
	@AC_MES_10 Pesos ,
	@AC_MES_11 Pesos ,
	@AC_MES_12 Pesos ,
	@AC_MES_13 Pesos ,	
	@AC_ANUAL Pesos )
AS
BEGIN
	set nocount on 
		
	set @AC_ANUAL = @AC_MES_01+ @AC_MES_02+ @AC_MES_03+ @AC_MES_04+ @AC_MES_05+ @AC_MES_06+ @AC_MES_07+ 
	@AC_MES_08+ @AC_MES_09+ @AC_MES_10+ @AC_MES_11+ @AC_MES_12+ @AC_MES_13; 

	insert into ACUMULA_RS ( AC_YEAR, CB_CODIGO, CO_NUMERO, RS_CODIGO, 
	AC_MES_01, AC_MES_02, AC_MES_03, AC_MES_04, AC_MES_05, AC_MES_06, AC_MES_07, 
	AC_MES_08, AC_MES_09, AC_MES_10, AC_MES_11, AC_MES_12, AC_MES_13, AC_ANUAL ) 
	values  ( @AC_YEAR, @CB_CODIGO, @CO_NUMERO, @RS_CODIGO, 
	@AC_MES_01, @AC_MES_02, @AC_MES_03, @AC_MES_04, @AC_MES_05, @AC_MES_06, @AC_MES_07, 
	@AC_MES_08, @AC_MES_09, @AC_MES_10, @AC_MES_11, @AC_MES_12, @AC_MES_13,	@AC_ANUAL ) 
	
	SET @AC_ID	= SCOPE_IDENTITY()
	exec CALCULA_ACUMULADOS_TOTALES_ANUAL @AC_YEAR, @CB_CODIGO, @CO_NUMERO
	RETURN 0 
END
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Update( 
					@AC_ID	FolioGrande, 
					@AC_YEAR Anio ,
					@CB_CODIGO NumeroEmpleado ,			
					@CO_NUMERO Concepto ,
					@RS_CODIGO Codigo , 	
					@AC_MES_01 Pesos ,	
					@AC_MES_02 Pesos ,	
					@AC_MES_03 Pesos ,
					@AC_MES_04 Pesos ,
					@AC_MES_05 Pesos ,
					@AC_MES_06 Pesos ,
					@AC_MES_07 Pesos ,
					@AC_MES_08 Pesos ,
					@AC_MES_09 Pesos ,
					@AC_MES_10 Pesos ,
					@AC_MES_11 Pesos ,
					@AC_MES_12 Pesos ,
					@AC_MES_13 Pesos ,	
					@AC_ANUAL Pesos
					)
AS
BEGIN
	set nocount on 
	
	set @AC_ANUAL = @AC_MES_01+ @AC_MES_02+ @AC_MES_03+ @AC_MES_04+ @AC_MES_05+ @AC_MES_06+ @AC_MES_07+ 
	@AC_MES_08+ @AC_MES_09+ @AC_MES_10+ @AC_MES_11+ @AC_MES_12+ @AC_MES_13; 

	update ACUMULA_RS 
		set AC_YEAR = @AC_YEAR, 
			CB_CODIGO = @CB_CODIGO, 
			CO_NUMERO = @CO_NUMERO, 
			RS_CODIGO = @RS_CODIGO, 
			AC_MES_01 = @AC_MES_01, 
			AC_MES_02 = @AC_MES_02, 
			AC_MES_03 = @AC_MES_03, 
			AC_MES_04 = @AC_MES_04, 
			AC_MES_05 = @AC_MES_05, 
			AC_MES_06 = @AC_MES_06, 
			AC_MES_07 = @AC_MES_07, 
			AC_MES_08 = @AC_MES_08, 
			AC_MES_09 = @AC_MES_09, 
			AC_MES_10 = @AC_MES_10,
			AC_MES_11 = @AC_MES_11, 
			AC_MES_12 = @AC_MES_12, 
			AC_MES_13 = @AC_MES_13, 
			AC_ANUAL  = @AC_ANUAL 
	where  AC_ID = @AC_ID

	exec CALCULA_ACUMULADOS_TOTALES_ANUAL @AC_YEAR, @CB_CODIGO, @CO_NUMERO 
END
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Delete(
					@AC_ID	FolioGrande,
					@AC_YEAR Anio,
					@CB_CODIGO NumeroEmpleado,
					@CO_NUMERO Concepto,
					@RS_CODIGO Codigo
					)
AS
BEGIN
	set nocount on 
		
	delete from ACUMULA_RS 
	where  ( AC_ID = @AC_ID )

	exec CALCULA_ACUMULADOS_TOTALES_ANUAL @AC_YEAR, @CB_CODIGO, @CO_NUMERO 

END
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_SumarAnual( 	
					@AC_YEAR Anio ,
					@CB_CODIGO NumeroEmpleado, 
					@ListaConceptos Formula = '' 
					)
AS
BEGIN
	set nocount on 
	
	-- Suma de ACUMULA_RS
	update ACUMULA_RS set AC_ANUAL = (AC_MES_01 +
                                        AC_MES_02 +
                                        AC_MES_03 +
                                        AC_MES_04 +
                                        AC_MES_05 +
                                        AC_MES_06 +
                                        AC_MES_07 +
                                        AC_MES_08 +
                                        AC_MES_09 +
                                        AC_MES_10 +
                                        AC_MES_11 +
                                        AC_MES_12 +
                                        AC_MES_13)
    where ( AC_YEAR = @AC_YEAR  ) and  ( CB_CODIGO = @CB_CODIGO  ) and ( @ListaConceptos = '' OR CO_NUMERO in ( select  items from dbo.FN_SPLIT( @ListaConceptos, ',' ) ) ) 
	-- Recalculo de Totales en ACUMULA
	update ACUMULA 
	set 
	     ACUMULA.AC_MES_01  = AR.AC_MES_01,
		  ACUMULA.AC_MES_02  = AR.AC_MES_02,
		  ACUMULA.AC_MES_03  = AR.AC_MES_03, 
		  ACUMULA.AC_MES_04  = AR.AC_MES_04,
		  ACUMULA.AC_MES_05  = AR.AC_MES_05,
		  ACUMULA.AC_MES_06  = AR.AC_MES_06,
		  ACUMULA.AC_MES_07  = AR.AC_MES_07,
		  ACUMULA.AC_MES_08  = AR.AC_MES_08,
		  ACUMULA.AC_MES_09  = AR.AC_MES_09,
		  ACUMULA.AC_MES_10  = AR.AC_MES_10,
		  ACUMULA.AC_MES_11  = AR.AC_MES_11,
		  ACUMULA.AC_MES_12  = AR.AC_MES_12,
		  ACUMULA.AC_MES_13  = AR.AC_MES_13,
		  ACUMULA.AC_ANUAL   = AR.AC_ANUAL
	from ACUMULA
	left outer join VACUMRSTOT AR  on ACUMULA.CB_CODIGO = AR.CB_CODIGO and ACUMULA.AC_YEAR = AR.AC_YEAR and ACUMULA.CO_NUMERO = AR.CO_NUMERO 
	where ( AR.AC_YEAR = @AC_YEAR ) and ( AR.CB_CODIGO = @CB_CODIGO ) and (  @ListaConceptos = '' OR  AR.CO_NUMERO in ( select  items from dbo.FN_SPLIT( @ListaConceptos, ',' ) ) ) 
END
GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS_CONCEPTOS(					
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
				    @MES SMALLINT, 
					@TIPO SMALLINT, 
					@Numero SMALLINT, 
					@ListaConceptos Formula  
			) 
AS
BEGIN 
	SET NOCOUNT ON 

	 DECLARE @RS_CODIGO Codigo 


	 -- Encapsular en Funcion 
	 select @RS_CODIGO = RSOCIAL.RS_CODIGO from NOMINA 
	 join RPATRON on NOMINA.CB_PATRON = RPATRON.TB_CODIGO 
	 join RSOCIAL on RSOCIAL.RS_CODIGO  = RPATRON.RS_CODIGO 
	 where NOMINA.PE_YEAR = @ANIO and NOMINA.PE_TIPO = @TIPO and NOMINA.PE_NUMERO = @NUMERO and NOMINA.CB_CODIGO = @EMPLEADO 

	 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 

	 -- Fin de futura fujncion 


	 Declare @Concepto Concepto 
	 Declare @Monto Pesos 
	 Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and 
			   ( M.CO_NUMERO in (select items from dbo.FN_SPLIT( @ListaConceptos, ',') ) ) 
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @Monto
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, 1, @Monto, @RS_CODIGO;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @Monto
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien
END
GO 

CREATE  PROCEDURE LIMPIA_ACUMULADOS_RS_CONCEPTOS(
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT, 
					@ListaConceptos Formula 
					) 
AS
BEGIN
	set nocount on 	

	declare @Query   nvarchar(210)
    declare @mesChar nvarchar(2)
    declare @Params	 nvarchar( 150  )
	
	if ( @Mes < 10 )                   
        set @mesChar = '0'         
	else
		set @mesChar = '' 
    set @mesChar = @mesChar + cast( @Mes as varchar(2) )

	set @Query = 'update ACUMULA_RS set AC_MES_' + @mesChar + ' = 0 
				  where ( AC_YEAR = @Anio ) and (CB_CODIGO = @Empleado ) and ( CO_NUMERO in ( select items from dbo.FN_SPLIT(@ListaConceptos, '','' ) ) )'
                                
	set @Params  = N'@Anio status, @Empleado int,  @ListaConceptos varchar(255)'	
	
	exec sp_executesql @Query, @Params, @Anio, @Empleado, @ListaConceptos 

	set @Query = 'update ACUMULA set AC_MES_' + @mesChar + ' = 0 
				  where ( AC_YEAR = @Anio ) and (CB_CODIGO = @Empleado ) and ( CO_NUMERO in ( select items from dbo.FN_SPLIT(@ListaConceptos, '','' ) ) )'
                                      
	set @Params  = N'@Anio status, @Empleado int,  @ListaConceptos varchar(255)'
	
	exec sp_executesql @Query, @Params, @Anio, @Empleado, @ListaConceptos 
END
GO 

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Importa( 
                                  @Concepto Concepto,
                                  @Mes SMALLINT,
                                  @Anio Anio,
                                  @Empleado NumeroEmpleado,
                                  @Monto Pesos ,      
                                  @RazonSocial Codigo, 
                                  @limpiarAcumulados Booleano,
								  @Operacion SMALLINT
                                  )
AS
BEGIN
    set nocount on 

	declare @Query   nvarchar(500)
    declare @mesCol nvarchar(10)
    declare @Params	 nvarchar( 150  )
	declare @omSustituir int
	declare @omSumar int 
	declare @omRestar int 	
	set  @omSustituir = 0
	set  @omSumar = 1
	set  @omRestar = 2
	
	set @mesCol = ''

    if ( RTRIM( @RazonSocial ) = '' )
            SET @RazonSocial = dbo.FN_RazonSocialEmpleado(@Empleado)

	set @mesCol = dbo.FN_GET_AC_MES(@Mes); 
	     	                 
                                               
    if ( @limpiarAcumulados = 'S' ) 
    begin 
        set @Query = 'update ACUMULA_RS set '+ @mesCol + ' = 0 where
						( AC_YEAR = @Anio ) and 
                        ( CB_CODIGO = @Empleado ) and 
						( CO_NUMERO = @Concepto )'                                       
        set @Params  = N' @Anio status, @Empleado int, @Concepto int'
        exec sp_executesql @Query, @Params, @Anio, @Empleado, @Concepto 
    end

                    
    set @Query = 'update ACUMULA_RS set ' + @mesCol + ' = 	
							case ( @Operacion )
								when @omSumar  then ' + @mesCol + ' + @Monto
								when @omRestar then ' + @mesCol + ' - @Monto
								else @Monto
							end
					where ' +
					'( RS_CODIGO = @RazonSocial ) and ' +
					'( AC_YEAR = @Anio ) and ' +
					'( CB_CODIGO = @Empleado ) and ' +
					'( CO_NUMERO = @Concepto )'	
    set @Params = N'@RazonSocial char(6), @Anio status, @Empleado int, @Concepto int, @Monto decimal(15,2), @Operacion smallint, @omSumar smallint, @omRestar smallint'	
    exec sp_executesql @Query, @Params, @RazonSocial, @Anio, @Empleado, @Concepto, @Monto, @Operacion, @omSumar, @OmRestar 
	    
	IF @@ROWCOUNT = 0  
    begin 
		set @Query = 'insert into ACUMULA_RS (RS_CODIGO, AC_YEAR, CB_CODIGO, CO_NUMERO, ' + @mesCol + ' ) values ( @RazonSocial, @Anio, @Empleado, @Concepto, @Monto )' 			                                      
        set @Params  = N'@RazonSocial char(6), @Anio status, @Empleado int, @Concepto int,  @Monto decimal(15,2)'		
        exec sp_executesql @Query, @Params, @RazonSocial, @Anio, @Empleado, @Concepto, @Monto                 		
    end     
             
    /* No filtra por Razon Social para limpiar AC_ANUAL de las razones sociales no asociadas a la nomina del empleados en caso de datos sucios */ 
    update ACUMULA_RS set AC_ANUAL = (AC_MES_01 +
                                                        AC_MES_02 +
                                                        AC_MES_03 +
                                                        AC_MES_04 +
                                                        AC_MES_05 +
                                                        AC_MES_06 +
                                                        AC_MES_07 +
                                                        AC_MES_08 +
                                                        AC_MES_09 +
                                                        AC_MES_10 +
                                                        AC_MES_11 +
                                                        AC_MES_12 +
                                                        AC_MES_13)
	where ( AC_YEAR = @Anio ) and
		  ( CB_CODIGO = @Empleado ) and
		  ( CO_NUMERO = @Concepto );
             
    /* Recalcula Monto Anunal de la tabla ACUMULA */
    exec CALCULA_ACUMULADOS_TOTALES_ANUAL @Anio, @Empleado, @Concepto
END
GO

/*** GRAFICAS ***/
create PROCEDURE SP_DASHLET_ROTACION_SUBTOTAL
	(@USUARIO SMALLINT,
	 @ESTEGRUPO CHAR(6),
	 @ESTADESCRIPCION VARCHAR(30),
	 @INICIO INTEGER,
	 @ALTAS INTEGER,
 	 @BAJAS INTEGER,
         @DIASACTIVOS NUMERIC(15,2),
	 @DIASRANGO NUMERIC(15,2),
	 @FECHAINI DATETIME	 )
AS
BEGIN
     SET NOCOUNT ON
     declare @Promedio NUMERIC(15,2);
     if ( @DiasRAngo = 0 )
     begin
          Set @Promedio = 0;
     end
     else
     begin
          Set @Promedio = @DiasActivos / @DiasRango;
     end
     insert into #TMPROTA
     (TR_GRUPO, TR_DESCRIP, TR_INICIO, TR_FINAL, TR_ALTAS, TR_BAJAS, TR_PROM, TR_FECHA, TR_USER ) values
     (@EsteGrupo, @EstaDescripcion, @Inicio, @Inicio+@Altas-@Bajas, @Altas, @Bajas, @Promedio, @FECHAINI, @USUARIO );
end

GO

CREATE PROCEDURE SP_DASHLET_ROTACION 
	( @FECHAINI FECHA,
	 @FECHAFIN FECHA, 
	 @CM_NIVEL0 Formula )
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @USUARIO int
	DECLARE @ESTEGRUPO CHAR(6);
	DECLARE @ESTADESCRIPCION VARCHAR(30);
	DECLARE @INICIO INTEGER;
	DECLARE @ALTAS  INTEGER;
	DECLARE @BAJAS  INTEGER;
	DECLARE @DIASACTIVOS INTEGER;  
  	DECLARE @GRUPO CHAR(6);
  	DECLARE @DESCRIPCION VARCHAR(30);
  	DECLARE @FECING DATETIME;
  	DECLARE @FECBAJ DATETIME;
  	DECLARE @DIASRANGO INTEGER;
  	DECLARE @NUMGRUPOS INTEGER;	

	set @USUARIO = 1 
	
	CREATE TABLE #TMPROTAI(
		TR_USER int ,
		CB_CODIGO int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		CB_FEC_ING Datetime ,
		CB_FEC_BAJ Datetime
	) 

	CREATE TABLE #TMPROTA(
		TR_USER int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		TR_INICIO int,
		TR_FINAL int ,
		TR_ALTAS int ,
		TR_BAJAS int ,
		TR_PROM Decimal(15,2) ,
		TR_FECHA Datetime 
	) 
 	

        SET @NumGrupos = 0
	SET @DiasRango = DATEDIFF(day, @FechaIni, @FechaFin )+1
	
	insert into #TMPROTAI (TR_USER, CB_CODIGO, TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ) 
	select @USUARIO, COLABORA.CB_CODIGO, '', '', COLABORA.CB_FEC_ING, COLABORA.CB_FEC_BAJ from COLABORA 
	where
	(
	 (( COLABORA.CB_FEC_ING < @FECHAFIN ) AND 
      ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING OR COLABORA.CB_FEC_BAJ >= @FECHAINI )) OR 
	  ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING AND COLABORA.CB_FEC_BAJ>=@FECHAINI ) 
	)
	and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))



	Declare Temporal CURSOR for
 		select TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ
 		from   #TMPROTAI
 		where  TR_USER = @Usuario
        	order by TR_GRUPO
	OPEN TEMPORAL
	fetch NEXT FROM Temporal 
	into   @Grupo, @Descripcion, @FecIng, @FecBaj 
		WHILE @@FETCH_STATUS = 0 
		BEGIN
    			if (( @NumGrupos = 0 ) or ( @EsteGrupo <> @Grupo )) 
            		begin
               
               			if ( @NumGrupos > 0 ) 
                  		EXECUTE sp_SUBTOTAL_ROTACION 
                           		@Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                           		@Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

	               		SELECT	@NumGrupos = @NumGrupos + 1, @EsteGrupo = @Grupo,
	               			@EstaDescripcion = @Descripcion,
               				@Inicio = 0,
               				@Altas = 0,
               				@Bajas = 0,            
               				@DiasActivos = 0
            		end

            		if (( @FecIng >= @FechaIni ) and ( @FecIng <= @FechaFin ))
               			SET @Altas = @Altas + 1;

            		if (( @FecBaj >= @FechaIni ) and ( @FecBaj <= @FechaFin ))
               			SET @Bajas = @Bajas + 1;

            		if (( @FecIng < @FechaIni ) or (( @FecBaj < @FecIng ) and ( @FecBaj >= @FechaIni )))
            			SELECT @Inicio = @Inicio + 1, @FecIng = @FechaIni

            		if (( @FecBaj > @FechaFin ) or ( @FecBaj < @FecIng )) 
               			SET @FecBaj = @FechaFin;

            		SET @DiasActivos = DATEDIFF(day, @FecIng,@FecBaj) + 1 + @DiasActivos ;
			fetch NEXT FROM Temporal 
 			into   @Grupo, @Descripcion, @FecIng, @FecBaj 
  		END;

   	close Temporal
    	deallocate Temporal
        

        if ( @NumGrupos > 0 ) 
           EXECUTE SP_DASHLET_ROTACION_SUBTOTAL 
                   @Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                   @Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

  	
	/*select ID=1, Descripcion='Inicio' , Cantidad= TR_INICIO*1.0 from #TMPROTA 
	union all */
	select ID=2, Descripcion='Altas' , Cantidad= TR_ALTAS*1.0 from #TMPROTA 
	union all 
	select ID=3, Descripcion='Bajas' , Cantidad= TR_BAJAS*1.0 from #TMPROTA 
	/*union all 
	select ID=4, Descripcion='Final' , Cantidad= TR_FINAL*1.0 from #TMPROTA 
	union all 
	select ID=4, Descripcion='Promedio' , Cantidad= TR_PROM from #TMPROTA */
	
	drop table #TMPROTA
	drop table #TMPROTAI 

END

go 
CREATE PROCEDURE SP_INFO_ROTACION  
	( @FECHAINI FECHA,
	 @FECHAFIN FECHA, 
	 @CM_NIVEL0 Formula, 
	 @EmpInicio decimal(15,2)  output,
	 @EmpAltas decimal(15,2)  output,
	 @EmpBajas decimal(15,2)  output,
	 @EmpFinal decimal(15,2)  output,
	 @EmpPromedio decimal(15,2) output	  
	  )
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @USUARIO int
	DECLARE @ESTEGRUPO CHAR(6);
	DECLARE @ESTADESCRIPCION VARCHAR(30);
	DECLARE @INICIO INTEGER;
	DECLARE @ALTAS  INTEGER;
	DECLARE @BAJAS  INTEGER;
	DECLARE @DIASACTIVOS INTEGER;  
  	DECLARE @GRUPO CHAR(6);
  	DECLARE @DESCRIPCION VARCHAR(30);
  	DECLARE @FECING DATETIME;
  	DECLARE @FECBAJ DATETIME;
  	DECLARE @DIASRANGO INTEGER;
  	DECLARE @NUMGRUPOS INTEGER;	

	set @USUARIO = 1 
	
	CREATE TABLE #TMPROTAI(
		TR_USER int ,
		CB_CODIGO int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		CB_FEC_ING Datetime ,
		CB_FEC_BAJ Datetime
	) 

	CREATE TABLE #TMPROTA(
		TR_USER int ,
		TR_GRUPO varchar(6) ,
		TR_DESCRIP varchar(30) ,
		TR_INICIO int,
		TR_FINAL int ,
		TR_ALTAS int ,
		TR_BAJAS int ,
		TR_PROM Decimal(15,2) ,
		TR_FECHA Datetime 
	) 
 	

        SET @NumGrupos = 0
	SET @DiasRango = DATEDIFF(day, @FechaIni, @FechaFin )+1
	
	insert into #TMPROTAI (TR_USER, CB_CODIGO, TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ) 
	select @USUARIO, COLABORA.CB_CODIGO, '', '', COLABORA.CB_FEC_ING, COLABORA.CB_FEC_BAJ from COLABORA 
	where
	(
	 (( COLABORA.CB_FEC_ING < @FECHAFIN ) AND 
      ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING OR COLABORA.CB_FEC_BAJ >= @FECHAINI )) OR 
	  ( COLABORA.CB_FEC_BAJ < COLABORA.CB_FEC_ING AND COLABORA.CB_FEC_BAJ>=@FECHAINI ) 
	)
	and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))



	Declare Temporal CURSOR for
 		select TR_GRUPO, TR_DESCRIP, CB_FEC_ING, CB_FEC_BAJ
 		from   #TMPROTAI
 		where  TR_USER = @Usuario
        	order by TR_GRUPO
	OPEN TEMPORAL
	fetch NEXT FROM Temporal 
	into   @Grupo, @Descripcion, @FecIng, @FecBaj 
		WHILE @@FETCH_STATUS = 0 
		BEGIN
    			if (( @NumGrupos = 0 ) or ( @EsteGrupo <> @Grupo )) 
            		begin
               
               			if ( @NumGrupos > 0 ) 
                  		EXECUTE sp_SUBTOTAL_ROTACION 
                           		@Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                           		@Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

	               		SELECT	@NumGrupos = @NumGrupos + 1, @EsteGrupo = @Grupo,
	               			@EstaDescripcion = @Descripcion,
               				@Inicio = 0,
               				@Altas = 0,
               				@Bajas = 0,            
               				@DiasActivos = 0
            		end

            		if (( @FecIng >= @FechaIni ) and ( @FecIng <= @FechaFin ))
               			SET @Altas = @Altas + 1;

            		if (( @FecBaj >= @FechaIni ) and ( @FecBaj <= @FechaFin ))
               			SET @Bajas = @Bajas + 1;

            		if (( @FecIng < @FechaIni ) or (( @FecBaj < @FecIng ) and ( @FecBaj >= @FechaIni )))
            			SELECT @Inicio = @Inicio + 1, @FecIng = @FechaIni

            		if (( @FecBaj > @FechaFin ) or ( @FecBaj < @FecIng )) 
               			SET @FecBaj = @FechaFin;

            		SET @DiasActivos = DATEDIFF(day, @FecIng,@FecBaj) + 1 + @DiasActivos ;
			fetch NEXT FROM Temporal 
 			into   @Grupo, @Descripcion, @FecIng, @FecBaj 
  		END;

   	close Temporal
    	deallocate Temporal
        

        if ( @NumGrupos > 0 ) 
           EXECUTE SP_DASHLET_ROTACION_SUBTOTAL 
                   @Usuario, @EsteGrupo, @EstaDescripcion, @Inicio,
                   @Altas, @Bajas, @DiasActivos, @DiasRango, @FechaIni 

  	
	select 
		@EmpInicio = TR_INICIO * 1.0, 
		@EmpAltas = TR_ALTAS * 1.0, 
		@EmpBajas = TR_BAJAS * 1.0, 
		@EmpFinal = TR_FINAL * 1.0, 
		@EmpPromedio= TR_PROM * 1.0
	from #TMPROTA 
	
	drop table #TMPROTA
	drop table #TMPROTAI 

END

GO 


create procedure SP_DASHLET_ROTACION_COMPARACION_MES ( @FechaMesIni Fecha,  @FechaMesFin Fecha, @CM_NIVEL0 Formula ) 
as
begin 
	-- Mes actual 	
	DECLARE @EmpInicio decimal(15,2)
	DECLARE @EmpAltas decimal(15,2)
	DECLARE @EmpBajas decimal(15,2)
	DECLARE @EmpFinal decimal(15,2)
	DECLARE @EmpPromedio decimal(15,2)
	DECLARE @MesActualDesc Descripcion 
	DECLARE @MesAntDesc	Descripcion 

	-- Mes anterior 
	DECLARE @Ant_FechaMesIni Fecha	
	DECLARE @Ant_FechaMesFin Fecha 
	DECLARE @Ant_EmpInicio decimal(15,2)
	DECLARE @Ant_EmpAltas decimal(15,2)
	DECLARE @Ant_EmpBajas decimal(15,2)
	DECLARE @Ant_EmpFinal decimal(15,2)
	DECLARE @Ant_EmpPromedio decimal(15,2)

	EXEC SP_INFO_ROTACION    @FechaMesIni  ,@FechaMesFin  ,@CM_NIVEL0  ,@EmpInicio OUTPUT  ,@EmpAltas OUTPUT  ,@EmpBajas OUTPUT  ,@EmpFinal OUTPUT  ,@EmpPromedio OUTPUT

	set @Ant_FechaMesIni = dateadd( month, -1, @FechaMesIni ) 
	set @Ant_FechaMesFin = dateadd( day, -1, @FechaMesIni ) 

	EXEC SP_INFO_ROTACION    @Ant_FechaMesIni  ,@Ant_FechaMesFin  ,@CM_NIVEL0  ,@Ant_EmpInicio OUTPUT  ,@Ant_EmpAltas OUTPUT  ,@Ant_EmpBajas OUTPUT  ,@Ant_EmpFinal OUTPUT  ,@Ant_EmpPromedio OUTPUT
	
		
	set @MesActualDesc = dbo.SP_DASHLET_GET_MES_DESC( month(@FechaMesIni ) ) 
	set @MesAntDesc = dbo.SP_DASHLET_GET_MES_DESC( month(@Ant_FechaMesIni ) ) 
				
	select ID=1, Descripcion=@MesAntDesc  , Altas= @Ant_EmpAltas*1.0, Bajas= @Ant_EmpBajas*1.0
	union all 
	select ID=2, Descripcion=@MesActualDesc, Altas= @EmpAltas*1.0, Bajas= @EmpBajas*1.0

end

GO 

create procedure SP_INDICADOR_SHOW( @valor Pesos, @valorAnterior Pesos, @textoAdicional Formula= '') 
as
begin 
	declare @tendencia Pesos 
	set @valorAnterior = coalesce( @ValorAnterior, 0 ) 
	set @valor = coalesce( @valor, 0 ) 

	if ( @valorAnterior = 0.00 and @valor = 0.00 ) 
		set @tendencia = 0 
	else
	if ( @valorAnterior = 0.00 ) 
		set @tendencia = 100.00 
	else 
		set @tendencia =  ( @valor  - @valorAnterior  ) / @valorAnterior * 100.00 
	
	select valor=@valor , tendencia=@tendencia,  anterior = @valorAnterior, textoAdicional = @textoAdicional 
end 

GO 

create procedure SP_INDICADOR_ROTACION_MES ( @FechaMesIni Fecha,  @FechaMesFin Fecha, @CM_NIVEL0 Formula, @iCual status ) 
as
begin 
	-- Mes actual 	
	DECLARE @EmpInicio decimal(15,2)
	DECLARE @EmpAltas decimal(15,2)
	DECLARE @EmpBajas decimal(15,2)
	DECLARE @EmpFinal decimal(15,2)
	DECLARE @EmpPromedio decimal(15,2)
	DECLARE @MesActualDesc Descripcion 
	DECLARE @MesAntDesc	Descripcion 

	-- Mes anterior 
	DECLARE @Ant_FechaMesIni Fecha	
	DECLARE @Ant_FechaMesFin Fecha 
	DECLARE @Ant_EmpInicio decimal(15,2)
	DECLARE @Ant_EmpAltas decimal(15,2)
	DECLARE @Ant_EmpBajas decimal(15,2)
	DECLARE @Ant_EmpFinal decimal(15,2)
	DECLARE @Ant_EmpPromedio decimal(15,2)

	EXEC SP_INFO_ROTACION    @FechaMesIni  ,@FechaMesFin  ,@CM_NIVEL0  ,@EmpInicio OUTPUT  ,@EmpAltas OUTPUT  ,@EmpBajas OUTPUT  ,@EmpFinal OUTPUT  ,@EmpPromedio OUTPUT

	set @Ant_FechaMesIni = dateadd( month, -1, @FechaMesIni ) 
	set @Ant_FechaMesFin = dateadd( day, -1, @FechaMesIni ) 

	EXEC SP_INFO_ROTACION    @Ant_FechaMesIni  ,@Ant_FechaMesFin  ,@CM_NIVEL0  ,@Ant_EmpInicio OUTPUT  ,@Ant_EmpAltas OUTPUT  ,@Ant_EmpBajas OUTPUT  ,@Ant_EmpFinal OUTPUT  ,@Ant_EmpPromedio OUTPUT

	declare @valor Pesos 
	declare @Ant_valor Pesos

	if ( @iCual  = 1 )
	begin 		
		set @valor = @EmpFinal
		set @Ant_valor = @Ant_EmpFinal			
	end 
	else
	if ( @iCual  = 2 )
	begin 		
		set @valor = @EmpPromedio
		set @Ant_valor = @Ant_EmpPromedio			
	end 
	else
	if ( @iCual  = 3 )
	begin 		
		set @valor = @EmpBajas
		set @Ant_valor = @Ant_EmpBajas			
	end 
	else 
	if ( @iCual  = 4 )
	begin 
		if ( @EmpPromedio > 0 ) 
			set @valor = ( @EmpBajas / @EmpPromedio ) * 100.00 
		else
			set @valor = 0.00 
				
		if ( @Ant_EmpPromedio > 0 ) 
			set @Ant_valor = ( @Ant_EmpBajas / @Ant_EmpPromedio ) * 100.00 
		else
			set @Ant_valor = 0.00 
	end 
	
	exec SP_INDICADOR_SHOW @valor, @Ant_valor

end

GO 

CREATE PROCEDURE Sesion_SetValues (
	@SesionIDActual	FolioGrande,
	@SesionID	FolioGrande OUTPUT,
	@US_CODIGO	Usuario, 
	@CM_CODIGO	CodigoEmpresa, 	
	@PE_TIPO	FolioChico, 
	@PE_YEAR	FolioChico, 
	@PE_NUMERO	FolioChico, 
	@FechaActiva	Fecha	
	 )
AS
BEGIN 
	declare @PE_MES Mes 
	declare @MesIni Fecha 
	declare @MesFin Fecha 

	set nocount on

	select @PE_MES = PE_MES from PERIODO where @PE_TIPO = PE_TIPO and  @PE_YEAR = PE_YEAR and  @PE_NUMERO = PE_NUMERO
	if ( @PE_MES > 12 ) set @PE_MES = 12 
	set @PE_MES = coalesce( @PE_MES, 1 )

	select @MesIni = cast(0 as datetime)
	declare @dt Varchar(20)	
	
	set @dt = '' 
	if (@PE_MES > 9 ) 
		set @dt = CAST(@PE_YEAR as varchar(4)) + CAST(@PE_MES as varchar(2)) + '01'
	else
		set @dt = CAST(@PE_YEAR as varchar(4)) + '0' + CAST(@PE_MES as varchar(2)) +'01' 
			
	set @MesIni = CONVERT(datetime, @dt, 112 ) 
	set @MesFin = dateadd(day, -1,  dateadd( month, 1, @MesIni ) );  
	set @SesionID = @SesionIDActual;
	exec #COMPARTE..Sesion_Update @SesionID output, @US_CODIGO, @CM_CODIGO, @PE_TIPO, @PE_YEAR, @PE_NUMERO, @PE_MES, @FechaActiva, @MesIni, @MesFin 

END 

GO 

CREATE PROCEDURE Sesion_Delete ( @SesionID FolioGrande ) 
AS
BEGIN 
	exec #COMPARTE..Sesion_Delete @SesionID 
END 

GO 

create procedure SP_DASHLET_MOTIVOSBAJAS_MES ( 
	@MesIni Fecha, 
	@MesFin Fecha, 
	@CM_NIVEL0 Formula) 
as
begin 
	set nocount on
	select dbo.fnConvert_TitleCase( MB.TB_ELEMENT) Motivo, count(*) Cantidad from KARDEX 
	join COLABORA CB on KARDEX.CB_CODIGO = CB.CB_CODIGO 
	join MOT_BAJA MB on MB.TB_CODIGO = KARDEX.CB_MOT_BAJ 
	where CB_FECHA between @MesIni and @MesFin and CB_TIPO = 'BAJA' 
	and  (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))
	group by MB.TB_ELEMENT 
	order by Cantidad desc 

end 

GO 


CREATE PROCEDURE SP_DASHLET_RUNQUERY( @SesionID FolioGrande, @Query nvarchar(max) )	
AS
BEGIN
	
	DECLARE @Params		NVARCHAR( MAX )

	declare @FechaActiva Fecha 
	declare @US_CODIGO Usuario 
	declare @PE_TIPO NominaTipo 
	declare @PE_YEAR Anio
	declare @PE_NUMERO NominaNumero 
	declare @PE_MES Mes 
	declare @MesIni Fecha 
	declare @MesFin Fecha 
	declare @CM_NIVEL0 Formula 

	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


	select @FechaActiva = FechaActiva, @MesIni = MesIni, @MesFin = MesFin, @PE_MES = PE_MES, 
	 @PE_TIPO = PE_TIPO, @PE_YEAR = PE_YEAR, @PE_NUMERO = PE_NUMERO, @CM_NIVEL0 = CM_NIVEL0, 
	 @US_CODIGO = US_CODIGO from dbo.Sesion_GetValues( @SesionID ) 


	SET @Params	= N'@US_CODIGO int, @FechaActiva DATETIME, @MesIni DATETIME, @MesFin DATETIME, @PE_TIPO int,  @PE_YEAR int, @PE_NUMERO int, @PE_MES int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params, @US_CODIGO, @FechaActiva, @MesIni, @MesFin,  @PE_TIPO,  @PE_YEAR, @PE_NUMERO, @PE_MES, @CM_NIVEL0

END

GO 


CREATE PROCEDURE SP_DASHLET_RUN( @SesionID FolioGrande, @DashletID  FolioGrande )	
AS
BEGIN	
	declare @Query nvarchar(max)
	
	select @Query = D.DashletQuery from #COMPARTE..Dashlets  D where D.DashletID = @DashletID 

	exec SP_DASHLET_RUNQUERY @SesionID, @Query 
END 


GO 

create procedure SP_DASHLET_INCIDENCIAS_RH ( @Fecha Fecha, @CM_NIVEL0 Formula ) 
as
begin 
select  
	Incidencia as ID, 
    dbo.SP_DASHLET_GET_INCIDENCIA_DESC(Incidencia)  Descripcion, 
    count(*) Cantidad
    from (
select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_STATUS_EMP(@Fecha, CB_CODIGO ) Incidencia from COLABORA 
where COLABORA.CB_ACTIVO = 'S'  and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))
) Empleados 
where Incidencia > 1 and Incidencia <> 9 
group by Incidencia 
 order by Cantidad desc 

end 

GO 

create procedure SP_DASHLET_INCIDENCIAS_LISTA_RH ( @Fecha Fecha, @CM_NIVEL0 Formula ) 
as
begin 
	select dbo.SP_DASHLET_GET_INCIDENCIA_DESC(Incidencia)  Incidencia,  Empleado, Nombre from ( 
	select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_STATUS_EMP(@Fecha, CB_CODIGO ) Incidencia from COLABORA 
	where COLABORA.CB_ACTIVO = 'S'  and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
	) Lista 
	where LIsta.Incidencia > 1 and Incidencia <> 9 
	order by Incidencia, Empleado  
end 
GO 

create procedure SP_GET_PERIODO_ANTERIOR ( 
	 @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 	 
	 @Ant_PE_YEAR Anio  output,
	 @Ant_PE_NUMERO NominaNumero  output)
as
begin 
	declare @Pe_Uso status 
	declare @Pe_Fec_Pag Fecha 
	select @Pe_Uso = PE_USO, @Pe_Fec_Pag = PE_FEC_PAG  from Periodo where Pe_tipo = @PE_TIPO and pe_year = @PE_YEAR and pe_numero = @PE_NUMERO 

	-- La anterior debera estar afectada 
	if ( @Pe_Uso = 0 ) 
		select top 1 @Ant_PE_YEAR = PE_YEAR, @Ant_PE_NUMERO = PE_NUMERO  from PERIODO  
		where PE_TIPO = @PE_TIPO and PE_YEAR <= @PE_YEAR and PE_USO = 0  and PE_STATUS >5  and PE_FEC_PAG < @PE_FEC_PAG
		order by PE_FEC_PAG desc 
	else
	begin 
		set @Ant_PE_NUMERO = 0 
		set @Ant_PE_YEAR = 0 
	end 

	set @Ant_PE_NUMERO = coalesce( @Ant_PE_NUMERO, 0 ) 
	set @Ant_PE_YEAR = coalesce( @Ant_PE_YEAR, 0 ) 
end 

GO 


create procedure SP_INDICADOR_NOMINA ( 
	 @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 	 
	 @PE_MES	Status, 
	 @CM_NIVEL0 Formula, 
	 @iCual status
 ) 
as
begin 
	
	declare @Ant_PE_YEAR Anio
	declare @Ant_PE_NUMERO NominaNumero 

	declare @Valor Pesos 
	declare @ValorAnterior Pesos

	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

	set @Valor = 0 
	set @ValorAnterior = 0
	
	-- Empleados Calculaods
	if ( @iCual = 1 ) 
	begin 
		select @Valor = count(*)*1.0 from NOMINA where PE_TIPO = @PE_TIPO and PE_YEAR = @PE_YEAR and PE_NUMERO = @PE_NUMERO and NO_STATUS >=4 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		select @ValorAnterior = count(*)*1.0 from NOMINA where PE_TIPO = @PE_TIPO and PE_YEAR = @Ant_PE_YEAR and PE_NUMERO = @Ant_PE_NUMERO and NO_STATUS > 5 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))
	end 
	else
	-- Neto a pagar 
	if ( @iCual = 2 ) 
	begin 		
		select  @Valor=  cast( SUM(NO_NETO)  as Decimal(15,2))  from NOMINA	NO
		where NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @PE_YEAR and NO.PE_NUMERO = @PE_NUMERO and NO_STATUS >= 4 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		select  @ValorAnterior= cast( SUM(NO_NETO) as Decimal(15,2))  from NOMINA NO				
		where NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @Ant_PE_YEAR and NO.PE_NUMERO = @Ant_PE_NUMERO and NO_STATUS > 5 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))
	end 
	else
	-- Recibos a Timbrar 
	if ( @iCual = 3 ) 
	begin 	
		select @Valor = count(*) from NOMINA NO
		join PERIODO PE on NO.PE_TIPO = PE.PE_TIPO and NO.PE_YEAR = PE.PE_YEAR and NO.PE_NUMERO = PE.PE_NUMERO 
		where  PE.PE_TIPO = @PE_TIPO and PE.PE_YEAR = @PE_YEAR and PE.PE_MES = @PE_MES and  NO_TIMBRO <> 2 and PE.PE_STATUS > 5		
		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		set @ValorAnterior = @Valor 

	end 
	else
	-- Neto a pagar  extras
	if ( @iCual = 4 ) 
	begin 		
		select  @Valor=  cast( SUM(MO_PERCEPC+MO_DEDUCCI)  as Decimal(15,2))  from MOVIMIEN MO
		join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
		join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO 
		where CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  )
		and NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @PE_YEAR and NO.PE_NUMERO = @PE_NUMERO and NO_STATUS >= 4 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))

		select  @ValorAnterior= cast( SUM(MO_PERCEPC+MO_DEDUCCI)  as Decimal(15,2))  from MOVIMIEN MO
		join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
		join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO 
		where CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  )
		and NO.PE_TIPO = @PE_TIPO and NO.PE_YEAR = @Ant_PE_YEAR and NO.PE_NUMERO = @Ant_PE_NUMERO and NO_STATUS > 5 
 		and (@CM_NIVEL0 = ''  or ( CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB_NIVEL0=''))
	end 

	exec SP_INDICADOR_SHOW @Valor, @ValorAnterior 

end

go 


create procedure SP_DASHLET_NOMINA_DIFERENCIAS_PERCEPCIONES ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	 
	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 
	
	
	select MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoActual from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where 		
		MO.PE_NUMERO = @PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @PE_YEAR and (CONCEPTO.CO_SAT_CLP > 0  or CONCEPTO.CO_SAT_CLN > 0 or CONCEPTO.CO_SAT_EXE > 0  ) 
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP
	order by MONTO desc, CONCEPTO.CO_NUMERO

	select 		
		MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoAnterior from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where 		
		MO.PE_NUMERO = @Ant_PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @Ant_PE_YEAR and (CONCEPTO.CO_SAT_CLP > 0  or CONCEPTO.CO_SAT_CLN > 0 or CONCEPTO.CO_SAT_EXE > 0  ) 
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP 
	order by MONTO desc, CONCEPTO.CO_NUMERO


	declare @Query nvarchar(1000) 
	
	set @Query = 
	'select * from ( 
	select top 5 ID=CO_NUMERO, Descripcion=''(''+ cast(CO_NUMERO as varchar(10))  + '') '' + LEFT( dbo.fnConvert_TitleCase(CO_DESCRIP), 20 ), 	
	cast( MontoAnterior as Decimal(15,2)) ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +'], 
	cast( Monto  as Decimal(15,2))  ['+ dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']	
		 from (
		select Act.CO_NUMERO, Act.CO_DESCRIP, Act.MONTO, COALESCE(Ant.Monto,0) MontoAnterior , Act.Monto - COALESCE(Ant.Monto,0) Diferencia, ABS( Act.Monto - COALESCE(Ant.Monto,0) ) DiferenciaAbs from #PeriodoActual Act
		left outer join #PeriodoAnterior Ant on Act.CO_NUMERO = Ant.CO_NUMERO
		) Diferencias
	where MontoAnterior <> Monto 
	order by (
		case 
			when MontoAnterior <> 0.00 then ABS(  (Diferencia/MontoAnterior) * 100.00 ) 
			else 0.00 
		end 
	) desc ) Diffs 
	order by  ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +'] -  ['+ dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +'] '
	
	EXEC sp_executesql @Query 
	


	drop table #PeriodoActual 
	drop table #PeriodoAnterior 


end

GO 


create procedure SP_DASHLET_NOMINA_DIFERENCIAS_TIEMPOEXTRA ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	 
	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 
		
	select MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoActual from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where 
		CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  ) and
		MO.PE_NUMERO = @PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @PE_YEAR and CONCEPTO.CO_TIPO in (1,4)
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP 
	order by MONTO desc, CONCEPTO.CO_NUMERO

	select MO.PE_NUMERO, CONCEPTO.CO_NUMERO, CONCEPTO.CO_DESCRIP, SUM(MO_PERCEPC+MO_DEDUCCI) MONTO into #PeriodoAnterior from MOVIMIEN MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	where  
		CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = 'P019' or  CONCEPTO.CO_SAT_TPN = 'P019'  ) and
		MO.PE_NUMERO = @Ant_PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @Ant_PE_YEAR and CONCEPTO.CO_TIPO in (1,4)
		and (@CM_NIVEL0 = ''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and  NO.CB_NIVEL0=''))
	group BY MO.PE_NUMERO, CONCEPTO.CO_NUMERO,CONCEPTO.CO_DESCRIP 
	order by MONTO desc, CONCEPTO.CO_NUMERO
	
	declare @Query nvarchar(1000) 
	
	set @Query = 
	'select ID=CO_NUMERO, Descripcion=''(''+ cast(CO_NUMERO as varchar(10))  + '') '' + LEFT( dbo.fnConvert_TitleCase(CO_DESCRIP), 20 ), 	
	cast( MontoAnterior as Decimal(15,2)) ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +'], 
	cast( Monto as Decimal(15,2))  ['+ dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +'] 	
		 from (
		select Act.CO_NUMERO, Act.CO_DESCRIP, Act.MONTO, COALESCE(Ant.Monto,0) MontoAnterior , Act.Monto - COALESCE(Ant.Monto,0) Diferencia, ABS( Act.Monto - COALESCE(Ant.Monto,0) ) DiferenciaAbs from #PeriodoActual Act
		left outer join #PeriodoAnterior Ant on Act.CO_NUMERO = Ant.CO_NUMERO
		) Diferencias
	order by DiferenciaAbs desc'
	
	EXEC sp_executesql @Query
	
	drop table #PeriodoActual 
	drop table #PeriodoAnterior 


end

GO 


create procedure SP_DASHLET_NOMINA_SUPERVISORES_TIEMPOEXTRA ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	
	declare @iNivelSupervisor status  
	declare @CampoSupervisor varchar(20) 
	declare @TablaSupervisor varchar(20) 
	declare @NombreNivel     varchar(20) 
		
	set @iNivelSupervisor = dbo.SP_GET_NIVEL_SUPERVISORES()

	if ( @iNivelSupervisor = 0 ) 
		set @iNivelSupervisor = 1; 

	set @CampoSupervisor = 'CB_NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 
	set @TablaSupervisor = 'NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 

	if (@iNivelSupervisor >=1 ) and ( @iNivelSupervisor <= 9 ) 
		select @NombreNivel = dbo.fnConvert_TitleCase(GL_FORMULA) from global where GL_CODIGO = 12 + @iNivelSupervisor
	else
		set @NombreNivel = 'Supervisor' 
	
	set @NombreNivel = REPLACE( REPLACE( @NombreNivel, '[', '') , ']' , '' ) 
	declare @Query nvarchar(1000) 
	declare @Params nvarchar(1000) 
	
	set @Query = 		
	'select  REPLACE(Supervisor,'','', '''') as ['+@NombreNivel+'], CAST( MontoTotal as Decimal(15,2)) as  [Monto] from ( 
	select SUM(MO_PERCEPC+MO_DEDUCCI) as MontoTotal, RTRIM(Nivel.TB_CODIGO) + '' - ''+ dbo.fnConvert_TitleCase(Nivel.TB_ELEMENT) as Supervisor   from MOVIMIEN�MO
	join CONCEPTO on MO.CO_NUMERO = CONCEPTO.CO_NUMERO
	join NOMINA NO on NO.PE_TIPO = MO.PE_TIPO and NO.PE_YEAR = MO.PE_YEAR and NO.PE_NUMERO = MO.PE_NUMERO and MO.CB_CODIGO = NO.CB_CODIGO  
	join '+@TablaSupervisor+' Nivel on NO.'+@CampoSupervisor+' = Nivel.TB_CODIGO 
	where  
		CONCEPTO.CO_TIPO in (1,4)	and ( CONCEPTO.CO_SAT_TPP = ''P019'' or  CONCEPTO.CO_SAT_TPN = ''P019''  ) and
		MO.PE_NUMERO = @PE_NUMERO and MO.PE_TIPO = @PE_TIPO and MO.PE_YEAR = @PE_YEAR and CONCEPTO.CO_TIPO in (1,4)
		and (@CM_NIVEL0 = ''''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  NO.CB_NIVEL0=''''))
	group BY Nivel.TB_ELEMENT, Nivel.TB_CODIGO ) Montos 
	order by MontoTotal desc'

	
	SET @Params	= N'@PE_TIPO int,  @PE_YEAR int, @PE_NUMERO int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params,  @PE_TIPO,  @PE_YEAR, @PE_NUMERO, @CM_NIVEL0	
	
end

GO 

create procedure SP_DASHLET_NOMINA_EMPLEADOS_DIFERENCIAS ( @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 
	 @CM_NIVEL0 Formula ) 
as
begin 

declare @Query nvarchar(2000) 
declare @Params nvarchar(1000) 
declare @Ant_PE_YEAR Anio
declare @AnT_PE_NUMERO NominaNumero
	 
exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

set @Query = 
';with NominaActual
as
(select  NO_NETO  , CB.CB_CODIGO, CB.PRETTYNAME
from NOMINA NO 
join COLABORA CB on CB.CB_CODIGO = NO.CB_CODIGO 
where PE_YEAR = @Ant_PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @Ant_PE_NUMERO 
	  and (@CM_NIVEL0 = ''''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  NO.CB_NIVEL0=''''))
)
select TOP 20 CAST( (NominaActual.NO_NETO - NominaAnterior.NO_NETO ) as Decimal(15,2) ) as Diferencia,  NominaAnterior.CB_CODIGO as [N�mero], dbo.fnConvert_TitleCase( NominaAnterior.PRETTYNAME ) as [Empleado] from ( 
select  NO_NETO, CB.CB_CODIGO, CB.PRETTYNAME  
from NOMINA NO 
join COLABORA CB on CB.CB_CODIGO = NO.CB_CODIGO 
where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO
and (@CM_NIVEL0 = ''''  or ( NO.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  NO.CB_NIVEL0=''''))
) NominaAnterior 
join NominaActual  on NominaActual.CB_CODIGO = NominaAnterior.CB_CODIGO 
order by ABS(NominaActual.NO_NETO - NominaAnterior.NO_NETO ) desc'
 

	SET @Params	= N'@PE_TIPO int,  @PE_YEAR int, @PE_NUMERO int, @Ant_PE_YEAR int, @Ant_PE_NUMERO int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params,  @PE_TIPO,  @PE_YEAR, @PE_NUMERO,@Ant_PE_YEAR,@AnT_PE_NUMERO,  @CM_NIVEL0

	
end 

GO 


create procedure SP_INDICADOR_ASISTENCIA ( 
	 @PE_TIPO NominaTipo, 
	 @PE_YEAR Anio,
	 @PE_NUMERO NominaNumero, 	 
	 @PE_MES	Status, 
	 @FechaActiva Fecha,
	 @CM_NIVEL0 Formula, 
	 @iCual status
 ) 
as
begin 
	
	declare @Ant_PE_YEAR Anio
	declare @Ant_PE_NUMERO NominaNumero 

	declare @Valor Pesos 
	declare @ValorAnterior Pesos

	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

	set @Valor = 0 
	set @ValorAnterior = 0
	
	-- Empleados Checaron  
	if ( @iCual = 1 ) 
	begin 
		select @Valor=count(*) from ( 
		select CHECADAS.CB_CODIGO, count(*)*1.0 Cuantas from CHECADAS 
		join COLABORA CB on CHECADAS.CB_CODIGO = CB.CB_CODIGO 
		where AU_FECHA = @FechaActiva 
			and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))
		group by CHECADAS.CB_CODIGO 
		) EmpleadosChecaron  						

		set @ValorAnterior = @valor 
	end 
	else
	-- Tiempo extra autorizado 
	if ( @iCual = 2 ) 
	begin 		
		select @Valor = SUM(AU_EXTRAS) from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

		select @ValorAnterior = SUM(AU_EXTRAS) from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @Ant_PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @Ant_PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

	end 
	else
	-- Tiempo extra por autorizar 
	if ( @iCual = 3 ) 
	begin 	
		select @Valor = 
		 SUM ( case 
				when AU_NUM_EXT  >= AU_EXTRAS then AU_NUM_EXT - AU_EXTRAS 
				else  0 
			end 
			)		
		from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

		select @ValorAnterior = 
			SUM ( case 
					when  AU_NUM_EXT >= AU_EXTRAS then AU_NUM_EXT - AU_EXTRAS
					else  0 
				end 
			  )		
		from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		where PE.PE_YEAR = @Ant_PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @Ant_PE_NUMERO  
 		and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))

	end 


	exec SP_INDICADOR_SHOW @Valor, @ValorAnterior 

end

GO 


create procedure SP_DASHLET_ASISTENCIA_SUPERVISORES_INCIDENCIAS ( @FechaActiva Fecha,  @CM_NIVEL0 Formula ) 
as
begin 
             
       declare @Ant_PE_YEAR Anio
       declare @AnT_PE_NUMERO NominaNumero
       
       declare @iNivelSupervisor status  
       declare @CampoSupervisor varchar(20) 
       declare @TablaSupervisor varchar(20) 
       declare @NombreNivel     varchar(20) 
             
       set @iNivelSupervisor = dbo.SP_GET_NIVEL_SUPERVISORES()

       if ( @iNivelSupervisor = 0 ) 
             set @iNivelSupervisor = 1; 

       set @CampoSupervisor = 'CB_NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 
       set @TablaSupervisor = 'NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 

       if (@iNivelSupervisor >=1 ) and ( @iNivelSupervisor <= 9 ) 
             select @NombreNivel = dbo.fnConvert_TitleCase(GL_FORMULA) from global where GL_CODIGO = 12 + @iNivelSupervisor
       else
             set @NombreNivel = 'Supervisor' 
       
       set @NombreNivel = REPLACE( REPLACE( @NombreNivel, '[', '') , ']' , '' ) 
       declare @Query nvarchar(2500) 
       declare @Params nvarchar(1000) 
       
       set @Query =        
'
with Empleados as 
( 
select CB_CODIGO, dbo.SP_KARDEX_'+@CampoSupervisor+'(@FechaActiva, CB_CODIGO) as '+@CampoSupervisor+', CB_NIVEL0 from COLABORA CB
where dbo.SP_STATUS_ACT(@FechaActiva, CB_CODIGO )  > 0  
and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  CB.CB_NIVEL0=''''))
)

select  '+@CampoSupervisor+', count(*) Activos into #ActivosFecha from Empleados CB
group by '+@CampoSupervisor+'; 

select AU.'+@CampoSupervisor+', count(*) Calculados into #CalculadosFecha from AUSENCIA AU
join #ActivosFecha on AU.'+@CampoSupervisor+' = #ActivosFecha.'+@CampoSupervisor+'
join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
where  AU.AU_FECHA = @FechaActiva 
and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  CB.CB_NIVEL0=''''))
group by AU.'+@CampoSupervisor+'; 



;with Incidentes as 
( 
select AU.'+@CampoSupervisor+' , count(*) as Incidencias    from AUSENCIA AU
       join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
       where  AU_TIPO <> ''''  and AU_TIPO not in (''RE'' ) and AU_FECHA = @FechaActiva
                    and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and  CB.CB_NIVEL0=''''))
       group by AU.'+@CampoSupervisor+' ) 
       
select RTRIM(Nivel.TB_CODIGO)+ '' - ''+ REPLACE(Nivel.TB_ELEMENT, '','', '''' )  ['+@NombreNivel+'], 
             
             case                              
                    when Calculados > 0  then  dbo.FN_MASK_PORC( ((Calculados-coalesce(Incidencias,0.00)*1.0)/( Calculados*1.0)) *100.00   )    
                    else ''S/D''
             end [% Asistencia], 
             coalesce(Calculados, 0) as [Empleados asignados], coalesce(Incidencias,0) as Ausentismos
from #ActivosFecha 
 left outer join #CalculadosFecha on #ActivosFecha.'+@CampoSupervisor+' =  #CalculadosFecha.'+@CampoSupervisor+'   
 left outer join Incidentes  on #ActivosFecha.'+@CampoSupervisor+' = Incidentes.'+@CampoSupervisor+'  
join '+@TablaSupervisor+' Nivel on #ActivosFecha.'+@CampoSupervisor+' = Nivel.TB_CODIGO 
where Activos > 0  and Nivel.TB_ACTIVO = ''S''
order by ( case                                
                    when Calculados > 0  then ((Calculados-coalesce(Incidencias,0.00)*1.0)/( Calculados*1.0)) *100.00   
                    else 10000
             end ), [Empleados asignados] desc  ; 
drop table #ActivosFecha; 
drop table #CalculadosFecha '; 

         
             print @Query      
       SET @Params  = N'@FechaActiva Datetime, @CM_NIVEL0 varchar(256) '
       EXEC sp_executesql @Query, @Params, @FechaActiva, @CM_NIVEL0 
       
end 


GO 

create procedure SP_DASHLET_INCIDENCIAS_PRENOMINA ( @Fecha Fecha, @CM_NIVEL0 Formula ) 
as
begin 

select row_number() over (order by SUM(Empleados) desc ) as ID , Tipo as Descripcion, SUM(Empleados) Cantidad from ( 
select 
case 
   when  AU_TIPO = ''  then 'Asistencia' 
   when  TB_INCIDEN =0 then  'Baja/Ingreso' 
   when  TB_INCIDEN =1 and AU_TIPO = 'FI' then 'Falta Injustificada' 
   when  TB_INCIDEN =1 and AU_TIPO <> 'FI' then 'Falta' 
   when  TB_INCIDEN =2 then 'Incapacidad'
   when  TB_INCIDEN =3 or AU_TIPO = 'VAC' then 'Vacaciones'
   when  TB_INCIDEN =4 then   INC.TB_ELEMENT     
   when  TB_INCIDEN =5 then 'Retardo'
   when  TB_INCIDEN is NULL then AU_TIPO 
   else 'Otros' 
  end Tipo,  count(*) Empleados from AUSENCIA 
  join COLABORA on AUSENCIA.CB_CODIGO = COLABORA.CB_CODIGO 
left outer join INCIDEN INC on INC.TB_CODIGO = AU_TIPO 
where  AU_FECHA =  @Fecha  and  AU_TIPO not in ('FSS', 'BAJ', 'IGR' ) 
and COLABORA.CB_ACTIVO = 'S'  and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0='')) 
group by INC.TB_INCIDEN, INC.TB_PERMISO, AU_TIPO, INC.TB_ELEMENT    
) ResumenPrenomina 
group by Tipo
order by  Cantidad desc 

end 

GO 

create procedure SP_DASHLET_ASISTENCIA_SUPERVISORES_TIEMPOEXTRA ( @PE_TIPO int, @PE_YEAR int, @PE_NUMERO int,  @CM_NIVEL0 Formula ) 
as
begin 
		
	declare @Ant_PE_YEAR Anio
	declare @AnT_PE_NUMERO NominaNumero
	
	declare @iNivelSupervisor status  
	declare @CampoSupervisor varchar(20) 
	declare @TablaSupervisor varchar(20) 
	declare @NombreNivel     varchar(20) 
		
	set @iNivelSupervisor = dbo.SP_GET_NIVEL_SUPERVISORES()

	if ( @iNivelSupervisor = 0 ) 
		set @iNivelSupervisor = 1; 

	set @CampoSupervisor = 'CB_NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 
	set @TablaSupervisor = 'NIVEL'+cast( @iNivelSupervisor as varchar(2) ) 

	if (@iNivelSupervisor >=1 ) and ( @iNivelSupervisor <= 9 ) 
		select @NombreNivel = dbo.fnConvert_TitleCase(GL_FORMULA) from global where GL_CODIGO = 12 + @iNivelSupervisor
	else
		set @NombreNivel = 'Supervisor' 
	
	set @NombreNivel = REPLACE( REPLACE( @NombreNivel, '[', '') , ']' , '' ) 
	declare @Query nvarchar(2000) 
	declare @Params nvarchar(1000) 
	

	set @Query = 		
'	select  	 
	 RTRIM( Nivel.TB_CODIGO )  + '' - ''+ REPLACE(NIVEL.TB_ELEMENT, '','', '''' )  ['+@NombreNivel+'] , 
SUM(AU_EXTRAS) [Extras autorizadas], 
	 SUM ( case 
				when AU_NUM_EXT >= AU_EXTRAS then AU_NUM_EXT - AU_EXTRAS				
				else  0 
			end 
		)  [Por Autorizar]

	    from AUSENCIA AU
		join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
		join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
		join  '+@TablaSupervisor+'  Nivel on AU.'+@CampoSupervisor+' = Nivel.TB_CODIGO
	where  
		PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  and Nivel.TB_ACTIVO = ''S'' 
 		and (@CM_NIVEL0 = ''''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, '','') )   )  or (@CM_NIVEL0 = '''' and CB.CB_NIVEL0=''''))
	group by Nivel.TB_CODIGO, Nivel.TB_ELEMENT
	order by [Extras autorizadas] desc' 
	

	SET @Params	= N'@PE_YEAR int, @PE_TIPO int, @PE_NUMERO int, @CM_NIVEL0 varchar(256) '
	EXEC sp_executesql @Query, @Params, @PE_YEAR , @PE_TIPO , @PE_NUMERO, @CM_NIVEL0	
	

	
end 

go 

create procedure SP_DASHLET_TIEMPOEXTRA_PRENOMINA(  @PE_TIPO int, @PE_YEAR int, @PE_NUMERO int,  @CM_NIVEL0 Formula ) 
as
begin 

	declare @Ant_PE_YEAR Anio
	declare @Ant_PE_NUMERO NominaNumero 

	declare @Dobles Pesos 
	declare @Triples Pesos
	declare @DT Pesos

	declare @Ant_Dobles Pesos 
	declare @Ant_Triples Pesos
	declare @Ant_DT Pesos

	exec SP_GET_PERIODO_ANTERIOR @PE_TIPO, @PE_YEAR, @PE_NUMERO, @Ant_PE_YEAR output, @Ant_PE_NUMERO output 

	select @Dobles = SUM(AU_DOBLES),  @Triples = SUM(AU_TRIPLES),  @DT = SUM(AU_DES_TRA)
	from AUSENCIA AU 
	join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
	join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
	where  
	PE.PE_YEAR = @PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @PE_NUMERO  
	and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))


	select @Ant_Dobles = SUM(AU_DOBLES),  @Ant_Triples = SUM(AU_TRIPLES),  @Ant_DT = SUM(AU_DES_TRA)
	from AUSENCIA AU 
	join COLABORA CB on AU.CB_CODIGO = CB.CB_CODIGO 
	join PERIODO PE on AU.PE_YEAR = PE.PE_YEAR and AU.PE_TIPO = PE.PE_TIPO and AU.PE_NUMERO = PE.PE_NUMERO 
	where  
	PE.PE_YEAR = @Ant_PE_YEAR and PE.PE_TIPO = @PE_TIPO and PE.PE_NUMERO = @Ant_PE_NUMERO  
	and (@CM_NIVEL0 = ''  or ( CB.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  or (@CM_NIVEL0 = '' and CB.CB_NIVEL0=''))


	declare @Query nvarchar(1000) 
	declare @Params nvarchar(500) 
	set @Params  = '@Ant_Dobles Decimal(15,2), @Ant_Triples Decimal(15,2),  @Ant_DT Decimal(15,2), @Dobles Decimal(15,2), @Triples Decimal(15,2),  @DT Decimal(15,2)' 

	set @Query = 
	'
	select ID=1, Descripcion=''Horas extras dobles''  , ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +']=@Ant_Dobles, ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']=@Dobles 
	union all 
	select ID=2, Descripcion=''Horas extras triples''  , ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +']=@Ant_Triples, ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']=@Triples 
	union all 
	select ID=3, Descripcion=''Descanso trabajado''  , ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @Ant_PE_NUMERO ) +']=@Ant_DT, ['+  dbo.GET_PERIODO_NOMBRE(@PE_TIPO, @PE_NUMERO ) +']=@DT
	'
	EXEC sp_executesql @Query, @Params, @Ant_Dobles , @Ant_Triples,  @Ant_DT, @Dobles , @Triples ,  @DT
	

end 
go

/* Patch 460: US #15852:  (1/4) Tablero de Mi Empresa */
create procedure SP_DASHLET_GENERAL_CUMPLEANOS ( @CM_NIVEL0 Formula ) 
as
begin 
    declare @Fecha Fecha 
    
    set @Fecha = getdate()         
    set @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0,@Fecha))

      declare @MesVar varchar(20) 
    set @MesVar = dbo.SP_DASHLET_GET_MES_DESC( MONTH(@Fecha) ) 

    declare @lunes datetime
    set @lunes = '17530101'

    declare @FecIni Fecha 
    declare @FecFin Fecha 

    set @FecIni = @lunes + datediff(day,@lunes,@Fecha)/7*7 
    set @FecFin = dateadd(day, 6, @FecIni)

    select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_DASHLET_GET_MES_DESC( MONTH(CB_FEC_NAC) ) as Mes,  DAY(CB_FEC_NAC) as [D_ia] from COLABORA 
    where 
        dateadd( year, datediff( year, CB_FEC_NAC, @Fecha+7 ), CB_FEC_NAC)  between @FecIni  and @FecFin and 
        dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
        or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))    
    order by dateadd( year, datediff( year, CB_FEC_NAC, @Fecha+7 ), CB_FEC_NAC) 

end
go 

/* Patch 460: US #15852:  (2/4) Tablero de Mi Empresa */
create procedure SP_DASHLET_GENERAL_ANIVERSARIOS ( @CM_NIVEL0 Formula ) 
as
begin 
    declare @Fecha Fecha 
    set @Fecha = getdate() 
    set @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0,@Fecha))
    
    
    declare @lunes datetime
    set @lunes = '17530101'

    declare @FecIni Fecha 
    declare @FecFin Fecha 

    set @FecIni = @lunes + datediff(day,@lunes,@Fecha)/7*7 
    set @FecFin = dateadd(day, 6, @FecIni)
    
    
    select CB_CODIGO Empleado, dbo.fnConvert_TitleCase(REPLACE(PRETTYNAME, ',', '')) Nombre, dbo.SP_DASHLET_GET_MES_DESC( MONTH(CB_FEC_ANT) ) as Mes,  DAY(CB_FEC_ANT) as [D_ia], Datediff(year, CB_FEC_ANT, @Fecha) as [Antig__uedad]   from COLABORA 
    where 
        dateadd( year, datediff( year, CB_FEC_ANT, @Fecha+7 ), CB_FEC_ANT)  between @FecIni  and @FecFin and  Datediff(year, CB_FEC_ANT, @Fecha) > 0 and 
        dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
        or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))    
    order by dateadd( year, datediff( year, CB_FEC_ANT, @Fecha+7 ), CB_FEC_ANT) 
end
go

/* Patch 460: US #15852:  (3/4) Tablero de Mi Empresa */
create procedure SP_DASHLET_GENERAL_DEMOGRAFICO_GENERO (  @CM_NIVEL0 Formula ) 
as
begin 

declare @Fecha Fecha 

set @Fecha = getdate() 

select row_number() over (order by SUM(Empleados) desc ) as ID , Tipo as Descripcion, SUM(Empleados) Cantidad from ( 
select 
case 
   when  CB_SEXO = 'M'  then 'Masculino' 
   when  CB_SEXO = 'F'  then 'Femenino' 
   else 'No especificado' 
  end Tipo,  count(*) Empleados from COLABORA  
  where dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
		or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
  group by CB_SEXO 
) ResumenGenero
group by Tipo
order by  Descripcion desc 

end 

GO 

/* Patch 460: US #15852:  (4/4) Tablero de Mi Empresa */
create procedure SP_DASHLET_GENERAL_DEMOGRAFICO_EDAD (  @CM_NIVEL0 Formula ) 
as
begin 


	declare @Fecha Datetime 
	set @Fecha = getdate() 

	select row_number() over ( order by Grupo) as Id, Grupo as Descripcion,  SUM(Hombre) Hombre, SUM(Mujer) Mujer 
	from ( 
	select 
	case 
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 1 and 14  then '14 o -' 
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 15 and 19  then '15 a 19'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 20 and 24  then '20 a 24'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 25 and 29  then '25 a 29'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 30 and 34  then '30 a 34'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 35 and 39  then '35 a 39'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 40 and 44  then '40 a 44'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 45 and 49  then '45 a 49'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 50 and 54  then '50 a 54'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 55 and 59  then '55 a 59'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 60 and 64  then '60 a 64'    
	   when  datediff(year, CB_FEC_NAC, @Fecha ) between 65 and 120 then '65 o +'       
	   else 'No especificado' 
	  end Grupo,  Case when CB_SEXO = 'M' then -1 else 0 end  as Hombre ,  Case when CB_SEXO = 'F' then 1 else 0 end  as Mujer  
	  from COLABORA  
		 where dbo.SP_STATUS_ACT(@Fecha, CB_CODIGO ) = 1   and (@CM_NIVEL0 = ''  or ( COLABORA.CB_NIVEL0 in (select items from dbo.FN_Split(@CM_NIVEL0, ',') )   )  
		or (@CM_NIVEL0 = '' and COLABORA.CB_NIVEL0=''))	
	  union all 
	  select '14 o -' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '15 a 19' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '20 a 24' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '25 a 29'  as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '30 a 34' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '35 a 39' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '40 a 44' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '45 a 49' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '50 a 54' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '55 a 59' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '60 a 64' as Grupo , 0 as Hombre, 0 as Mujer
	  union all 
	  select '65 o +' as Grupo , 0 as Hombre, 0 as Mujer
	) Emp
	Group By Grupo 
	order by Grupo 

end
go


CREATE PROCEDURE SP_INSERTAR_BITACORA
 ( 
    @US_CODIGO Usuario, 
    @BI_PROC_ID Status, 
    @BI_TIPO Status,
    @BI_NUMERO FolioGrande, 
    @BI_TEXTO Observaciones, 
    @CB_CODIGO NumeroEmpleado, 
    @BI_DATA Memo, 
    @BI_CLASE Status, 
    @BI_FEC_MOV Fecha 
) 
AS 
BEGIN 
    DECLARE @BI_FECHA Fecha
    DECLARE @BI_HORA HoraMinutosSegundos
    SET @BI_FECHA = getdate()
    SET @BI_HORA = SUBSTRING( CONVERT(CHAR(24),@BI_FECHA,113), 13,8 ) 
    SET @BI_FECHA = CONVERT(CHAR(10),@BI_FECHA,110) 
INSERT INTO BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_PROC_ID, BI_TIPO, 
         BI_NUMERO, BI_TEXTO, CB_CODIGO, BI_DATA, BI_CLASE, BI_FEC_MOV)
VALUES ( @US_CODIGO, @BI_FECHA, @BI_HORA, @BI_PROC_ID, @BI_TIPO, @BI_NUMERO, @BI_TEXTO, @CB_CODIGO,
        @BI_DATA, @BI_CLASE, @BI_FEC_MOV ) 
        
END
GO

CREATE PROCEDURE SP_FONACOT_ACTUALIZAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1,  @iPR_STATUS status, @iUS_CODIGO Usuario, @PR_PAG_PER Pesos, @lGrabarBitacora Booleano = 'S' ) 
as
begin 
    set nocount on 
    DECLARE @BI_TEXTO Observaciones
    DECLARE @BI_DATA NVARCHAR(MAX)
    DECLARE @BI_FEC_MOV Fecha
    DECLARE @PR_MONTO Pesos 
	declare @PR_SALDO_DEF Pesos	
    declare @PR_FECHA Fecha 
	declare @FormulaAnterior Observaciones
	declare @FormulaActual Observaciones

	set @PR_SALDO_DEF = 9999999.00

	select @PR_FECHA = PR_FECHA from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	
	declare @year int 
	declare @mes int 

	select top 1  @year = PF_YEAR , @mes = PF_MES  from PCED_FON where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
	order by PF_YEAR, PF_MES 

	declare @Fecha_Inicio Fecha 

	SELECT @Fecha_Inicio = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (@Year, @Mes, (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)) FP
		ON P.PE_NUMERO = FP.PE_NUMERO
		where
			PE_YEAR = @Year and PE_MES_FON = @Mes 
			AND PE_TIPO = (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)
			AND PE_POS_FON = 1

	set @Fecha_Inicio = coalesce( @Fecha_Inicio, @PR_FECHA ) 
 
	set @PR_MONTO = @PR_SALDO_DEF

 	set @FormulaAnterior = (select PR_FORMULA from PRESTAMO  where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO);  
 
    update PRESTAMO set PR_FECHA = @Fecha_Inicio, PR_STATUS = @iPR_STATUS,  US_CODIGO= @iUS_CODIGO ,  PR_MONTO = @PR_MONTO, PR_TOTAL = @PR_MONTO, PR_SALDO = @PR_MONTO, PR_PAG_PER = @PR_PAG_PER , PR_FORMULA = '', 
        PR_FON_MOT =  case                                       
                    when @iPR_STATUS = 1 
					then 3 
                    else
						0 
					end                                               
    where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO                     

	set @FormulaActual = (select PR_FORMULA from PRESTAMO  where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO);
    if ( @lGrabarBitacora  = 'S' ) 
    begin 
            SET @BI_TEXTO =  'Modificaci�n desde C�dula Fonacot: Ref:' + @PR_REFEREN;
            SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + 'PR_STATUS' + CHAR(13)+CHAR(10) + ' A : ' + cast (@iPR_STATUS as varchar)
    
            SELECT @BI_FEC_MOV = GETDATE();
            exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;

	        if (@FormulaAnterior <> '')
			begin
				--PARA ESPECIFICAR ANTES Y DESPUES DE FORMULA
				SET @BI_TEXTO =  'Modificaci�n de Pr�stamo: Empl.:'+cast (@CB_CODIGO as varchar) +' Tipo:'+@PR_TIPO+' Ref:' + @PR_REFEREN;
				SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + ' PR_FORMULA : ' + CHAR(13)+CHAR(10) + ' De: '+ @FormulaAnterior + CHAR(13)+CHAR(10) +' A: '+@FormulaActual   
				SELECT @BI_FEC_MOV = GETDATE();
				exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;
			end;
    end;
end
GO
 
CREATE procedure SP_FONACOT_CANCELAR_PRESTAMOS (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6), @US_CODIGO SMALLINT, @PROC_ID INTEGER, @BI_NUMERO INTEGER) 
as
begin
    set nocount on 
    DECLARE @PR_TIPO Codigo1;
    DECLARE @Prestamos TABLE
    (
        i int identity(1,1),
        iCB_CODIGO int, 
        sPR_REFEREN varchar(8) 
    )
    SELECT @PR_TIPO = GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 211
    insert into @Prestamos ( iCB_CODIGO , sPR_REFEREN )
		 select CB.CB_CODIGO, PR.PR_REFEREN  from PRESTAMO PR
			join COLABORA CB on PR.CB_CODIGO = CB.CB_CODIGO 
			join PCED_FON PF on PR.PR_REFEREN = PF.PR_REFEREN AND PR.PR_TIPO = PF.PR_TIPO
			 where PR.PR_TIPO = @PR_TIPO and PR_STATUS in (0)
			 AND (PF.RS_CODIGO = @RAZON_SOCIAL OR @RAZON_SOCIAL = '')
			 AND CB.CB_CODIGO = PF.CB_CODIGO

    declare @i int = 1 
    declare @n int 
    declare @iCB_CODIGO NumeroEmpleado 
    declare @sPR_REFEREN Referencia 
    select @n = count(*) from @Prestamos 
    while @i <= @n 
    begin          select @iCB_CODIGO = iCB_CODIGO , @sPR_REFEREN = sPR_REFEREN from @Prestamos where i = @i 
        
		if ( dbo.PrestamoFonacot_PuedeCancelar( @YEAR, @MES, @iCB_CODIGO, @PR_TIPO,  @sPR_REFEREN) = 'S' ) 
			exec SP_FONACOT_ACTUALIZAR_UN_PRESTAMO @iCB_CODIGO, @sPR_REFEREN, @PR_TIPO, 1, @US_CODIGO, 0
                
        set @i = @i + 1 
    end 
end
GO

CREATE PROCEDURE KARDEX_FONACOT( 
	@CB_CODIGO  Integer,
	@CB_FONACOT Descripcion,
	@US_CODIGO  SmallInt,
	@Resultado  SmallInt OUTPUT)
AS
BEGIN
  SET NOCOUNT ON
  declare @iNivel SmallInt;
  declare @sComenta VarChar( 50 );
  declare @sTipoKardex Char( 6 );
  declare @CB_NOTA Varchar(max);
  DECLARE @FECHA Fecha;
  DECLARE @CB_COMENTA VarChar( 50 );

  SET @sTipoKardex = 'EVENTO'; 
  SET @CB_NOTA = 'CB_FONACOT:=' + @CB_FONACOT
  SET @FECHA = CAST (GETDATE() AS DATE);
  SET @CB_COMENTA = 'Importaci�n datos Fonacot';

  update COLABORA 
	set CB_FONACOT = @CB_FONACOT
    where ( CB_CODIGO = @CB_CODIGO );  

  SET @sComenta = @CB_COMENTA
  EXEC @Resultado = GET_COMENTA_TKARDEX @sTipoKardex, @sComenta OUTPUT, @iNivel OUTPUT

  delete from KARDEX
  where ( CB_CODIGO = @CB_CODIGO ) and
        ( CB_FECHA = @FECHA ) and
        ( CB_TIPO = @sTipoKardex );

  insert into KARDEX ( CB_CODIGO,
                       CB_FECHA,
                       CB_TIPO,
                       CB_NIVEL,
                       CB_COMENTA,
					   CB_NOTA,
                       CB_FEC_CAP,
                       CB_GLOBAL,
                       US_CODIGO )
   values ( @CB_CODIGO,
                   @FECHA,
                   @sTipoKardex,
                   @iNivel,
                   @sComenta,
				   @CB_NOTA,
                   @FECHA,
                   'S',
                   @US_CODIGO );

    EXECUTE RECALCULA_KARDEX @CB_CODIGO
END
GO

CREATE PROCEDURE SP_FONACOT_AGREGAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia, @PR_FECHA Fecha, @PR_STATUS status, @PR_OBSERVA ReferenciaLarga,  @US_CODIGO Usuario, @PF_PAGO Pesos) 
as
begin 
	set nocount on 
	declare @Fecha_Inicio Fecha 
	declare @PR_SALDO_DEF Pesos
	
	set @PR_SALDO_DEF = 9999999.0
	
	SELECT @Fecha_Inicio = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (year( @PR_FECHA), month( @PR_FECHA ), (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)) FP
		ON P.PE_NUMERO = FP.PE_NUMERO
		where
			PE_YEAR = year( @PR_FECHA) and PE_MES_FON = month( @PR_FECHA ) 
			AND PE_TIPO = (SELECT CB_NOMINA FROM COLABORA WHERE CB_CODIGO = @CB_CODIGO)
			AND PE_POS_FON = 1


	set @Fecha_Inicio = coalesce( @Fecha_Inicio, @PR_FECHA ) 


	if ( select count(*) from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ) = 0
	begin 
		INSERT INTO PRESTAMO (CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_STATUS, PR_OBSERVA, US_CODIGO, PR_MONTO, PR_TOTAL, PR_SALDO, PR_PAG_PER, PR_FON_MOT) 
		VALUES ( @CB_CODIGO, @PR_TIPO, @PR_REFEREN, @Fecha_Inicio, @PR_STATUS, @PR_OBSERVA, @US_CODIGO, @PR_SALDO_DEF, @PR_SALDO_DEF, @PR_SALDO_DEF , @PF_PAGO, 0 )
	end

end
go

create PROCEDURE AFECTA_EMPLEADO_PS_PREVISION_SOCIAL( @EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER, 
				@RS_CODIGO Codigo)
as
begin 
    DECLARE @M1018 NUMERIC(15,2); 
	
	select @M1018 = sum( M.MO_PERCEPC + M.MO_DEDUCCI )
    from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
    where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO in ( 1, 4)  ) and
               ( C.CO_PS_TIPO = 1 )

     if ( @M1018 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1018, @Mes, @Factor, @M1018, @RS_CODIGO ;

end 
GO 

create PROCEDURE AFECTA_EMPLEADO_PS_SALARIOS( @EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER, 
				@RS_CODIGO Codigo)
as
begin 
    DECLARE @M1019 NUMERIC(15,2); 
	
	select @M1019 = sum( M.MO_PERCEPC + M.MO_DEDUCCI )
    from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
    where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO in ( 1, 4)  ) and
               ( C.CO_PS_TIPO = 2 )

     if ( @M1019 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1019, @Mes, @Factor, @M1019, @RS_CODIGO;

end 
GO
 
CREATE procedure SP_FONACOT_ELIMINAR_CEDULA (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6), @US_CODIGO SMALLINT, @PROC_ID INTEGER, @BI_NUMERO INTEGER) 
as
begin
	 set nocount on
	 DECLARE @NOMINAS_AFECTADAS INTEGER;
	 DECLARE @BI_TEXTO Observaciones
	 DECLARE @BI_DATA NVARCHAR(MAX)
	 DECLARE @BI_FEC_MOV Fecha
	 DECLARE @PR_TIPO Codigo1
	 DECLARE @CONTADOR_ELIMINADO INTEGER
	 DECLARE @EMPLEADO int;
	 DECLARE @REFERENCIA VARCHAR(8)


	 select @PR_TIPO = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	 set @PR_TIPO = COALESCE( @PR_TIPO, '' ) 

	 SELECT @NOMINAS_AFECTADAS = dbo.FN_FONACOT_CONTEO_CEDULA_AFECTADA(@YEAR, @MES, @RAZON_SOCIAL)  
	 
	 if @NOMINAS_AFECTADAS = 0
	 begin
		  SELECT @BI_FEC_MOV = GETDATE();
		  		  
		  Declare Temporal CURSOR for
 		  SELECT PR.CB_CODIGO, PR.PR_REFEREN from  PRESTAMO PR JOIN COLABORA C ON PR.CB_CODIGO = C.CB_CODIGO
			WHERE PR.CB_CODIGO IN (SELECT CB_CODIGO FROM PCED_FON WHERE PF_YEAR = @YEAR AND PF_MES =@MES AND  PR_TIPO = @PR_TIPO AND (RS_CODIGO = @RAZON_SOCIAL OR @RAZON_SOCIAL = ''))
			AND PR.PR_TIPO = @PR_TIPO			
			

		  OPEN TEMPORAL
		     fetch NEXT FROM Temporal 
	         into  @EMPLEADO, @REFERENCIA
		  WHILE @@FETCH_STATUS = 0 
		  BEGIN
				if ( dbo.PrestamoFonacot_PuedeCancelar( @YEAR, @MES, @EMPLEADO, @PR_TIPO, @REFERENCIA) = 'S' ) 
				begin 
					UPDATE PRESTAMO SET PR_STATUS = 1, PR_FON_MOT = 4 WHERE CB_CODIGO = @EMPLEADO AND PR_REFEREN = @REFERENCIA AND PR_TIPO = @PR_TIPO 
					SET @BI_TEXTO = 'Se cancel� pr�stamo Fonacot con referencia '  + @REFERENCIA; 
					exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, @EMPLEADO, '', 62, @BI_FEC_MOV;
				end; 

				fetch NEXT FROM Temporal 
 					into  @EMPLEADO, @REFERENCIA
  		   END;

   	       close Temporal
    	   deallocate Temporal

		   DELETE PF FROM PCED_FON PF JOIN COLABORA C ON PF.CB_CODIGO = C.CB_CODIGO
		  WHERE PF.PF_YEAR = @YEAR AND PF.PF_MES = @MES AND (PF.RS_CODIGO = @RAZON_SOCIAL OR @RAZON_SOCIAL = '')		  
			   AND PF.PR_TIPO = @PR_TIPO; 

		  SELECT @CONTADOR_ELIMINADO = @@ROWCOUNT; 

		  -- Eliminar registro de encabezado de c�dula (tabla PCED_ENC)
		  DELETE FROM PCED_ENC WHERE PE_YEAR = @YEAR AND PE_MES = @MES AND (RS_CODIGO = @RAZON_SOCIAL OR @RAZON_SOCIAL = '')
		  
		  if @CONTADOR_ELIMINADO = 0 
			 SET @BI_TEXTO = 'No se eliminaron registros de c�dula Fonacot.';
		  else
			  SET @BI_TEXTO = 'Se eliminaron: ' + cast (@CONTADOR_ELIMINADO as varchar) + ' registro(s) de c�dula Fonacot.'; 		  
		  
		  exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, 0, '', 62, @BI_FEC_MOV;
		
	 end;
end
GO 

create procedure SP_FONACOT_RECALCULAR_INCIDENCIAS(@FT_YEAR Anio, @FT_MONTH Mes) 
as 
begin 
	set nocount on 

	update FON_EMP set FE_INCIDE = VD.FE_INCIDE, FE_FECHA1 = VD.FE_FECHA1, FE_FECHA2 = VD.FE_FECHA2, FE_DIF_QTY = coalesce( VD.CT_DIF_QTY , 0 ) 
	from  FON_EMP, VCED_CALCULA VD
	where  VD.CB_CODIGO = FON_EMP.CB_CODIGO and  VD.PF_YEAR = FON_EMP.FT_YEAR and VD.PF_MES = FON_EMP.FT_MONTH and 
	VD.PF_YEAR = @FT_YEAR and VD.PF_MES = @FT_MONTH 


	update FON_CRE set FC_PR_STA = VD.PR_STATUS, FC_FON_MOT = VD.PR_FON_MOT 
	from FON_CRE, VCED_CALCULA VD 
	where  VD.CB_CODIGO = FON_CRE.CB_CODIGO and  VD.PF_YEAR = FON_CRE.FT_YEAR and VD.PF_MES = FON_CRE.FT_MONTH and VD.PR_REFEREN = FON_CRE.PR_REFEREN and VD.PR_TIPO = FON_CRE.PR_TIPO and 
	VD.PF_YEAR = @FT_YEAR and VD.PF_MES = @FT_MONTH 

	
end

GO 

CREATE PROCEDURE SP_FONACOT_PAGO_RAZON_SOCIAL
( @FT_YEAR Anio, @FT_MONTH Mes, @GLOBAL_ACTIVO Booleano, @RecalcularIncidencias Booleano = 'S' )
AS
BEGIN	
	-- Revisar si se import� para todas las razones sociales.	
	DECLARE @TODAS_LAS_RAZONES Booleano
	SELECT @TODAS_LAS_RAZONES =  COALESCE  (CASE WHEN RS_CODIGO <> '' THEN 'N' ELSE 'S' END, 'S')  FROM PCED_ENC 
	WHERE PE_YEAR = @FT_YEAR AND PE_MES = @FT_MONTH

	IF @TODAS_LAS_RAZONES = 'N'
	BEGIN
		EXEC SP_FONACOT_PAGO_CEDULA @FT_YEAR, @FT_MONTH, @GLOBAL_ACTIVO, @RecalcularIncidencias; 
	END
	ELSE
	BEGIN
		-- Se import� para todas las razones.
		-- Actualizar PCED_FON con la raz�n social que le corresponde a cada empleado.
		UPDATE PCED_FON SET RS_CODIGO = 
			(SELECT RS_CODIGO FROM RPATRON WHERE TB_CODIGO = (SELECT CB_PATRON FROM COLABORA C WHERE C.CB_CODIGO = PCED_FON.CB_CODIGO))
				WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH

		-- Solo se recalculan las incidencias en FON_EMP cuando la opcion esta encendida. 
		if @GLOBAL_ACTIVO = 'S' and @RecalcularIncidencias = 'S' 
			exec SP_FONACOT_RECALCULAR_INCIDENCIAS @FT_YEAR, @FT_MONTH 


		-- Borrar FON_TOT_RS
		DELETE FROM FON_TOT_RS WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH

		-- Abrir cursor
		DECLARE @RS_CODIGO Codigo

		DECLARE RAZONES_SOCIALES CURSOR FOR
			SELECT DISTINCT RS_CODIGO FROM RPATRON WHERE TB_CODIGO IN
				(SELECT DISTINCT CB_PATRON FROM COLABORA WHERE CB_CODIGO IN (SELECT DISTINCT CB_CODIGO FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH))
		-- Abrir cursor
		OPEN RAZONES_SOCIALES 
			FETCH NEXT FROM RAZONES_SOCIALES INTO @RS_CODIGO

		-- Ciclo
		WHILE ( @@FETCH_STATUS = 0 )
		BEGIN
			-- PRINT (@RS_CODIGO)
			-- Grabar en FON_TOT_RS
			INSERT INTO FON_TOT_RS
				(FT_YEAR, FT_MONTH, PR_TIPO, RS_CODIGO, FT_STATUS, FT_RETENC, FT_NOMINA, FT_TAJUST, FT_AJUSTE, FT_EMPLEAD, FT_CUANTOS, FT_BAJAS, FT_INCAPA, FT_DETALLE)
			-- SELECT FT_YEAR, FT_MONTH, PR_TIPO, @RS_CODIGO, 0 FT_STATUS, SUM (FC_RETENC), SUM(FC_NOMINA), '' FT_TAJUST, SUM (FC_AJUSTE), 
			SELECT FT_YEAR, FT_MONTH, PR_TIPO, @RS_CODIGO, 0 FT_STATUS, 0, SUM(FC_NOMINA), '' FT_TAJUST, SUM (FC_AJUSTE), 
			0 FT_EMPLEAD, 0 FT_CUANTOS, 0 FT_BAJAS, 0 FT_INCAPA, '' FT_DETALLE FROM FON_CRE
				WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				GROUP BY FT_YEAR, FT_MONTH, PR_TIPO
			
			-- Indicar cantidad de empleados y cantidad de cr�ditos.
			DECLARE @FT_EMPLEAD Empleados
			DECLARE @FT_CUANTOS Empleados			
			-- Indicar monto de c�dula, incapacidades y bajas
			-- Obtener de FON_CRE o PCED_FON dependiendo de lo que indique el global.
			DECLARE @FC_RETENC Pesos SET @FC_RETENC = 0
			DECLARE @FT_BAJAS Empleados SET @FT_BAJAS = 0
			DECLARE @FT_INCAPA Empleados SET @FT_INCAPA = 0

			IF @GLOBAL_ACTIVO = 'S' 
			BEGIN
				SELECT @FC_RETENC = SUM (PF_PAGO) FROM PCED_FON WHERE  PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH 
					AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				
				SELECT @FT_BAJAS = COUNT(DISTINCT CB_CODIGO)  FROM VCED_DET WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
					AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
					(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
					AND FE_INCIDE = 'B'
				SELECT @FT_INCAPA = COUNT(DISTINCT CB_CODIGO)  FROM VCED_DET WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
					AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
					(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
					AND FE_INCIDE = 'I'
			
				-- FT_CUANTOS: # de Cr�ditos Fonacot
				SELECT @FT_CUANTOS = COUNT(DISTINCT PR_REFEREN) FROM VCED_FON WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				-- FT_EMPLEAD: # Empleados con Pr�stamos Fonacot
				SELECT @FT_EMPLEAD = COUNT(DISTINCT CB_CODIGO) FROM VCED_FON WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))

			END
			ELSE
			BEGIN			
				SELECT @FC_RETENC = SUM (FC_RETENC) FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH 
					AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				 
			
				SELECT @FT_BAJAS = COUNT(DISTINCT CB_CODIGO)  FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
					AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
					(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
					AND FE_INCIDE = 'B'
				SELECT @FT_INCAPA = COUNT(DISTINCT CB_CODIGO)  FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
					AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
					(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
					AND FE_INCIDE = 'I'

				-- FT_CUANTOS: # de Cr�ditos Fonacot
				SELECT @FT_CUANTOS = COUNT(DISTINCT PR_REFEREN) FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				-- FT_EMPLEAD: # Empleados con Pr�stamos Fonacot
				SELECT @FT_EMPLEAD = COUNT(DISTINCT CB_CODIGO) FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
			END
		
			-- Actualizar tabla
			UPDATE FON_TOT_RS SET FT_RETENC = @FC_RETENC, FT_BAJAS = @FT_BAJAS, FT_INCAPA = @FT_INCAPA, FT_EMPLEAD = @FT_EMPLEAD, FT_CUANTOS = @FT_CUANTOS 
			WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH AND RS_CODIGO = @RS_CODIGO

			FETCH NEXT FROM RAZONES_SOCIALES
				INTO	@RS_CODIGO
		END

		CLOSE RAZONES_SOCIALES
		DEALLOCATE RAZONES_SOCIALES 

		-- Actualizar estatus, tipo de ajuste y detalle.
		-- Se obtienen de FON_TOT.
		DECLARE @FT_STATUS Status
		DECLARE @FT_TAJUST Codigo1
		DECLARE @FT_DETALLE NVARCHAR(MAX)
		SELECT @FT_STATUS = FT_STATUS, @FT_TAJUST = FT_TAJUST, @FT_DETALLE = FT_DETALLE FROM FON_TOT WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
		UPDATE FON_TOT_RS SET FT_STATUS = @FT_STATUS, FT_TAJUST = @FT_TAJUST, FT_DETALLE = @FT_DETALLE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH

	END

END
GO

CREATE PROCEDURE AFECTA_FALTAS_UN_EMPLEADO
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@FACTOR INTEGER,
					@EMPLEADO INTEGER
AS
	SET NOCOUNT ON
	declare @FechaIni   DateTime;
  	declare @Incidencia Char( 6 );
  	declare @TipoDia    SmallInt;
  	declare @FEC_INI    DateTime;
  	declare @MOTIVO     SmallInt;
  	declare @DIAS       Numeric(15,2);
	Declare @Bloqueo    VARCHAR(1);

	select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289;
	
	if( @Bloqueo = 'S' )
	begin
		 Update GLOBAL set GL_FORMULA = 'N' where GL_CODIGO = 289;
	end
  	
	select @FechaIni = P.PE_ASI_INI  /* Fecha de inicio de asistencia */
	from PERIODO P
	where
         ( P.PE_YEAR = @Anio ) and
         ( P.PE_TIPO = @Tipo ) and
         ( P.PE_NUMERO = @Numero )
	
	DECLARE TemporalFaltas CURSOR FOR 
  		select F.FA_FEC_INI, F.FA_MOTIVO, F.FA_DIAS
  		from   FALTAS F
       	left outer join NOMINA N on
         	( N.PE_YEAR = @Anio )
		and ( N.PE_TIPO = @Tipo )
		and ( N.PE_NUMERO = @Numero )
		and ( N.CB_CODIGO = F.CB_CODIGO )
  		where ( F.PE_YEAR = @Anio )
		and ( F.PE_TIPO = @Tipo )
		and ( F.PE_NUMERO = @Numero )
		and ( F.FA_DIA_HOR = 'D' )
		and ( F.FA_MOTIVO  IN (0,1,2,4,12))
		and N.CB_CODIGO = @EMPLEADO

   	OPEN TemporalFaltas
   	FETCH NEXT FROM TemporalFaltas
   	into  @FEC_INI, @MOTIVO, @DIAS
    WHILE @@FETCH_STATUS = 0
   	BEGIN
		if ( @FEC_INI <= '12/30/1899' )
		begin
			select @FEC_INI = @FechaIni;
		end
		if ( @MOTIVO = 0 )
		begin
       		Select @TipoDia = 0, @Incidencia = 'FI'
		end
		else if ( @MOTIVO = 1 )
		begin
       		Select @TipoDia = 5, @Incidencia = 'FJ'
		end
		else if ( @MOTIVO = 2 )
		begin
       		Select @TipoDia = 4, @Incidencia = 'FJ'
		end
		else if ( @MOTIVO = 4 )
		begin
       		Select @TipoDia = 6, @Incidencia = 'FJ'
		end
		else
		begin
       		Select @TipoDia = 0, @Incidencia = '';
		end
    
		while ( @DIAS > 0 )
		begin
       		if ( @MOTIVO <> 12 )
         		execute AFECTA_1_FALTA @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor
       		else if ( @FEC_INI < @FechaIni )
         		execute AFECTA_1_AJUSTE @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor
       
       		select @DIAS = @DIAS - 1, @FEC_INI = @FEC_INI + 1
		end
   		
		FETCH NEXT FROM TemporalFaltas
   		into @FEC_INI, @MOTIVO, @DIAS
	END

	CLOSE TemporalFaltas
	DEALLOCATE TemporalFaltas
	if (@Bloqueo = 'S')
	begin
		Update GLOBAL set GL_FORMULA = 'S' where GL_CODIGO = 289;
	end
GO

CREATE PROCEDURE AFECTA_PERIODO_UN_EMPLEADO(
	@ANIO SMALLINT,
	@TIPO SMALLINT,
	@NUMERO SMALLINT,
	@USUARIO SMALLINT,
	@STATUSVIEJO SMALLINT,
	@STATUSNUEVO SMALLINT,
	@FACTOR INTEGER,
	@EMPLEADO INTEGER )
AS
	SET NOCOUNT ON
	DECLARE @CONCEPTO INTEGER;
	DECLARE @MES SMALLINT;
	DECLARE @CONTADOR INTEGER;
	DECLARE @TOTAL NUMERIC(15,2);
	DECLARE @TIPOAHORRO CHAR(1);
	DECLARE @TIPOPRESTAMO CHAR(1);
	DECLARE @REFERENCIA VARCHAR(8);

	select @MES = P.PE_MES from PERIODO P where ( P.PE_YEAR = @Anio ) and ( P.PE_TIPO = @Tipo ) and ( P.PE_NUMERO = @Numero )

	if (select count(*) from NOMINA N left outer join COLABORA C  on ( C.CB_CODIGO = N.CB_CODIGO ) where ( N.PE_YEAR = @Anio ) and ( N.PE_TIPO = @Tipo ) and ( N.PE_NUMERO = @Numero )  and not ( C.CB_CODIGO is NULL )  and N.CB_CODIGO = @EMPLEADO  and ( coalesce( N.NO_TIMBRO, 0 ) > 0 )  ) > 0
	begin
		RAISERROR( 'La n�mina est� Timbrada', 16, 1 )
		return 0
	end
				
	if (
    select count(*) from NOMINA N  left outer join COLABORA C  on ( C.CB_CODIGO = N.CB_CODIGO ) where ( N.PE_YEAR = @Anio )  and ( N.PE_TIPO = @Tipo )
		and ( N.PE_NUMERO = @Numero )		and ( N.NO_STATUS = @StatusViejo ) 		and not ( C.CB_CODIGO is NULL )
		and ( coalesce( N.NO_TIMBRO, 0 ) = 0 )	and N.CB_CODIGO = @EMPLEADO
		) >  0
	BEGIN
		EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor;
		
		Declare TempAhorro CURSOR for
			select A.AH_TIPO, T.TB_CONCEPT from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO )  where ( A.CB_CODIGO = @Empleado ) and ( A.AH_STATUS = 0 )
			Open TempAhorro
			fetch NEXT FROM TempAhorro
			into @TipoAhorro, @Concepto
			while @@FETCH_STATUS = 0
       		begin
       			select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
				from MOVIMIEN M  where ( M.PE_YEAR = @Anio )
				and ( M.PE_TIPO = @Tipo )
				and ( M.PE_NUMERO = @Numero )
				and ( M.CB_CODIGO= @Empleado )
				and ( M.CO_NUMERO = @Concepto )
				and ( M.MO_ACTIVO = 'S' )
				if ( @Contador > 0 )
				begin
					Select @Total = @Factor * @Total;
					update AHORRO  set
                      			AH_NUMERO = AH_NUMERO + @Factor,
                       			AH_TOTAL  = AH_TOTAL + @Total,
                       			AH_SALDO = AH_SALDO + @Total
								where ( CB_CODIGO = @Empleado )
								and ( AH_TIPO = @TipoAhorro );
        		end
				fetch NEXT FROM TempAhorro
 				into   @TipoAhorro, @Concepto
			end
		close TempAhorro
		deallocate TempAhorro
 		
		Declare TempPrestamo CURSOR for
			select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT
			from PRESTAMO P join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO ) where ( P.CB_CODIGO = @Empleado ) and ( P.PR_STATUS in ( 0, 2 ) ) Open TempPrestamo fetch NEXT FROM TempPrestamo
			into @TipoPrestamo, @Referencia, @Concepto
			
			while @@FETCH_STATUS = 0
       		begin
       			select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
				from MOVIMIEN M   where ( M.PE_YEAR = @Anio )
				and ( M.PE_TIPO = @Tipo )
				and ( M.PE_NUMERO = @Numero )
				and ( M.CB_CODIGO= @Empleado )
				and ( M.CO_NUMERO = @Concepto )
				and ( M.MO_ACTIVO = 'S' ) and ( M.MO_REFEREN = @Referencia )
				if ( @Contador > 0 )
       			begin
               		Select @Total = @Factor * @Total;
               		update PRESTAMO set
						PR_NUMERO = PR_NUMERO + @Factor,
						PR_TOTAL  = PR_TOTAL + @Total,
						PR_SALDO = PR_SALDO - @Total
						where ( CB_CODIGO = @Empleado )
						and ( PR_TIPO = @TipoPrestamo )
						and ( PR_REFEREN = @Referencia );
       			end
				fetch NEXT FROM TempPrestamo
				into @TipoPrestamo, @Referencia, @Concepto
			end
		close TempPrestamo
		deallocate TempPrestamo

		EXECUTE SET_STATUS_PRESTAMOS @Empleado;
		if ( @StatusNuevo > 0 )
		begin
			update NOMINA set NO_STATUS = @StatusNuevo  where ( PE_YEAR = @Anio )
			and ( PE_TIPO = @Tipo )
			and ( PE_NUMERO = @Numero )
			and ( CB_CODIGO = @Empleado );
		end

		EXECUTE AFECTA_FALTAS_UN_EMPLEADO @Anio, @Tipo, @Numero, @Factor, @Empleado
			  
		if ( @StatusNuevo > 0 )
		begin
			update NOMINA set NO_STATUS = @StatusNuevo where ( PE_YEAR = @Anio )
				and ( PE_TIPO = @Tipo )
				and ( PE_NUMERO = @Numero )
				and ( CB_CODIGO = @EMPLEADO )
								
			EXECUTE SET_STATUS_PERIODO @Anio, @Tipo, @Numero, @Usuario, @StatusNuevo
		end
		RETURN 1
	END
RETURN 0

GO
CREATE PROCEDURE WFTabla_SetLastUpdate(
	@TablaID	INT,
	@Last		DATETIME )
AS
BEGIN
	UPDATE	T_WFTablas
	SET		TablaUpdateLast	= @Last
	WHERE	TablaID = @TablaID
END
GO
CREATE PROCEDURE WFTabla_SetLastInsert(
	@TablaID	INT,
	@Last		DATETIME )
AS
BEGIN
	UPDATE	T_WFTablas
	SET		TablaInsertLast	= @Last
	WHERE	TablaID = @TablaID
END
GO

CREATE PROCEDURE PE_INSERTAR_BITACORA
 ( 
        @US_CODIGO Usuario, 
	@BI_PROC_ID Status, 
	@BI_TIPO Status,
        @BI_NUMERO FolioGrande, 
	@BI_TEXTO Observaciones, 
	@CB_CODIGO NumeroEmpleado, 
        @BI_DATA Memo, 
	@BI_CLASE Status, 
	@BI_FEC_MOV Fecha 
) 
AS 
BEGIN 
	DECLARE @BI_FECHA Fecha
	DECLARE @BI_HORA HoraMinutosSegundos
	SET @BI_FECHA = getdate()
	SET @BI_HORA = SUBSTRING( CONVERT(CHAR(24),@BI_FECHA,113), 13,8 ) 
	SET @BI_FECHA = CONVERT(CHAR(10),@BI_FECHA,110) 

INSERT INTO BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_PROC_ID, BI_TIPO, 
         BI_NUMERO, BI_TEXTO, CB_CODIGO, BI_DATA, BI_CLASE, BI_FEC_MOV)
VALUES ( @US_CODIGO, @BI_FECHA, @BI_HORA, @BI_PROC_ID, @BI_TIPO, @BI_NUMERO, @BI_TEXTO, @CB_CODIGO,
        @BI_DATA, @BI_CLASE, @BI_FEC_MOV ) 
END
GO

CREATE PROCEDURE WFTabla_Consulta(
       @Grupo        FolioGrande = 0,
       @TablaID      FolioGrande = 0 )
AS
BEGIN
       SET NOCOUNT ON
       SELECT TablaID, TablaNombre, TablaInsertGet, TablaInsertPut, TablaInsertLast, TablaUpdateGet, TablaUpdatePut, TablaUpdateLast
       FROM   T_WFTablas
       WHERE  TablaActivo = 'TRUE' AND ( @TablaID = 0 AND TablaGrupo = @Grupo OR TablaID = @TablaID )
       ORDER BY TablaOrden, TablaID
END
go 

create procedure BORRAR_INDICES_LLAVE ( @tabla Formula ) 
as
begin 

	declare @qry nvarchar(max);
	select @qry = (
	select 
	  'IF EXISTS(SELECT * FROM sys.indexes WHERE name='''+ i.name +''' AND object_id = OBJECT_ID(''['+s.name+'].['+o.name+']''))      drop index ['+i.name+'] ON ['+s.name+'].['+o.name+'];  '
	from sys.indexes i 
			inner join sys.objects o on  i.object_id=o.object_id
			inner join sys.schemas s on o.schema_id = s.schema_id
			inner join sys.index_columns ic on ic.object_id = i.object_id  and ic.index_id = i.index_id
			inner join sys.columns c on c.object_id =  i.object_id  and c.column_id = ic.column_id
		where o.type<>'S' and is_primary_key<>1 and i.index_id>0
		and s.name!='sys' and s.name!='sys' and is_unique_constraint=0 and o.name = @tabla and c.name = 'LLAVE' 
		for xml path('')) ;
	
		exec sp_executesql @qry

end 
go 

create procedure REGENERAR_LLAVE ( @tabla Formula ) 
as
begin 
	declare @campo nvarchar(100)
	declare @sql nvarchar(1024)

	set @campo = @tabla +'.LLAVE'

	exec BORRAR_INDICES_LLAVE @tabla 

	if ( select count(*) from Syscolumns  where id = OBJECT_ID( @tabla ) and name = 'LLAVE' and cdefault > 0  ) > 0 
		   exec sp_unbindefault @campo

	if ( select count(*) from Syscolumns  where id = OBJECT_ID( @tabla ) and name = 'LLAVE' ) > 0 
	begin 
		   set @sql = 'alter table '+@tabla+' drop column LLAVE'
		   EXECUTE  sp_executesql  @sql 
	end 

	set @sql = 'alter table '+@tabla+' add LLAVE int identity(1,1) '
	EXECUTE  sp_executesql  @sql 

end 
go

create procedure WFTabla_PreparaCampoLlave ( @tabla Formula ) 
as
begin 

	DECLARE @SQLString  NVARCHAR( MAX )  
	declare @FolioGrandeType int 
	declare @intType int 
	select @FolioGrandeType = user_type_id  from sys.types where name='FolioGrande' 
	select @intType = user_type_id  from sys.types where name='int' 


    if ( left(@tabla, 3) = 'S3_' ) 
		set @tabla = replace( @tabla, 'S3_', '' ) 



	IF ((SELECT OBJECTPROPERTY( OBJECT_ID(@Tabla), 'TableHasIdentity')) = 1) and 
		   (( select count(*) from Syscolumns  where id = OBJECT_ID( @tabla ) and name = 'LLAVE' and xusertype <> @intType ))>0 
	begin 		
		 SET @SQLString =  
			N'IF  EXISTS (SELECT * FROM sysobjects WHERE xtype = ''UQ'' AND parent_obj=object_id(''' + @tabla + ''') AND name = ''SYNC_AK_'+@tabla+'_LLAVE'' ) 
					ALTER TABLE '+@tabla+' DROP CONSTRAINT SYNC_AK_'+@tabla+'_LLAVE' 
		
		EXECUTE sp_executesql @SQLString 
		
		exec REGENERAR_LLAVE @Tabla  
	end 

end 
go 

CREATE PROCEDURE WFTabla_GetRows(
	@TablaID	INT,
	@SincTime	DATETIME,
	@Cual		SMALLINT,
	@Forzar		SMALLINT )
AS
BEGIN
	DECLARE	@Query		NVARCHAR( MAX )
	DECLARE @Params		NVARCHAR( MAX )
	DECLARE @LastSinc	DATETIME
	DECLARE @Rango		SMALLINT
	DECLARE @RangoIni	SMALLINT
	DECLARE @RangoFin	SMALLINT
	DECLARE @Inicial	DATETIME
	DECLARE @Final		DATETIME
	SET NOCOUNT ON	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT	@Query		= CASE WHEN @Cual = 1 THEN TablaInsertGet ELSE TablaUpdateGet END,
			@LastSinc	= CASE WHEN @Forzar = 1 THEN '1899-12-30' WHEN @Cual = 1 THEN TablaInsertLast ELSE TablaUpdateLast END,
			@Rango		= TablaRango,
			@RangoIni	= TablaRangoIni,
			@RangoFin	= TablaRangoFin
	FROM	T_WFTablas
	WHERE	TablaID = @TablaID
	
	-- TablaRango: 0=Sin rango, 1=Segundos, 2=Minutos, 3=Horas, 4=Dias
	IF ( @Rango = 0 )
	BEGIN
		SET @Inicial	= 0
		SET @Final		= @SincTime
	END
	ELSE IF ( @Rango = 1 )
	BEGIN
		SET @Inicial	= DATEADD( ss, @RangoIni, @SincTime )
		SET @Final		= DATEADD( ss, @RangoFin, @SincTime )
	END
	ELSE IF ( @Rango = 2 )
	BEGIN
		SET @Inicial	= DATEADD( mi, @RangoIni, @SincTime )
		SET @Final		= DATEADD( mi, @RangoFin, @SincTime )
	END
	ELSE IF ( @Rango = 3 )
	BEGIN
		SET @Inicial	= DATEADD( hh, @RangoIni, @SincTime )
		SET @Final		= DATEADD( hh, @RangoFin, @SincTime )
	END
	ELSE IF ( @Rango = 4 )
	BEGIN
		SET @Inicial	= DATEADD( dd, @RangoIni, FLOOR( CAST( @SincTime AS FLOAT )))	-- Elimina hora, solo fecha
		SET @Final		= DATEADD( dd, @RangoFin, FLOOR( CAST( @SincTime AS FLOAT )))
	END

	SET @Params	= N'@LastSinc DATETIME, @SincTime DATETIME, @Inicial DATETIME, @Final DATETIME'
	EXEC sp_executesql @Query, @Params, @LastSinc, @SincTime, @Inicial, @Final

END
go 

CREATE PROCEDURE WFTabla_PreparaTriggers(  
 @TablaID FolioGrande )  
AS  
BEGIN  
 DECLARE @TablaNombre Observaciones  
 DECLARE @SQLString  NVARCHAR( MAX )  
 DECLARE @TriggerName Observaciones  
   
 SELECT @TablaNombre = TablaNombre  
 FROM T_WFTablas  
 WHERE TablaID = @TablaID  

 if ( left(@TablaNombre, 3) = 'S3_' ) 
	set @TablaNombre = replace( @TablaNombre, 'S3_', '' ) 
  
 -- 1) Agrega Timestamps de Created/Updated  
 SET @SQLString =   
N'IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id(''' + @TablaNombre + ''') AND name=''SINC_CREATED'')   
 ALTER TABLE ' + @TablaNombre + ' ADD SINC_CREATED Fecha NULL, SINC_UPDATED Fecha NULL'  
 EXECUTE sp_executesql @SQLString  
  
 -- 2) TimeStamps pone CERO para que se agreguen la primera sincronizaci�n  
 SET @SQLString = N'UPDATE ' + @TablaNombre + ' SET SINC_CREATED = 0, SINC_UPDATED = 0 WHERE SINC_CREATED IS NULL'  
 EXECUTE sp_executesql @SQLString  
    
 SET @SQLString = N'ALTER TABLE ' + @TablaNombre + ' ALTER COLUMN SINC_CREATED Fecha NOT NULL'  
 EXECUTE sp_executesql @SQLString  
 
 SET @SQLString = N'ALTER TABLE ' + @TablaNombre + ' ALTER COLUMN SINC_UPDATED Fecha NOT NULL'  
 EXECUTE sp_executesql @SQLString  
  
 SET @SQLString =  
  N'IF NOT EXISTS (SELECT * FROM sysobjects WHERE xtype = ''UQ'' AND parent_obj=object_id(''' + @TablaNombre + ''') AND name = ''SYNC_AK_'+@TablaNombre+'_LLAVE'' ) 
	ALTER TABLE '+@TablaNombre+' ADD CONSTRAINT SYNC_AK_'+@TablaNombre+'_LLAVE UNIQUE ( LLAVE )' 
	
 EXECUTE sp_executesql @SQLString 	
	
   -- 3) Crea Trigger de Insert  
 SET @TriggerName = 'SINC_TI_' + @TablaNombre  
 SET @SQLString =   
N'IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id( ''' + @TriggerName + ''') AND xtype in (''TR''))  
  DROP TRIGGER ' + @TriggerName  
 EXECUTE sp_executesql @SQLString  
  
 SET @SQLString =   
N'CREATE TRIGGER ' + @TriggerName + ' ON ' + @TablaNombre + ' AFTER INSERT AS  
BEGIN  
 SET NOCOUNT ON 
 UPDATE ' + @TablaNombre + '  
 SET  SINC_CREATED = GETDATE()  
 FROM ' + @TablaNombre + '  
    JOIN Inserted ON ' + @TablaNombre + '.LLAVE = Inserted.LLAVE  
END'  
 EXECUTE sp_executesql @SQLString  
  
 -- 4) Crea trigger de Update  
 SET @TriggerName = 'SINC_TU_' + @TablaNombre  
 SET @SQLString =   
N'IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id( ''' + @TriggerName + ''') AND xtype in (''TR''))  
  DROP TRIGGER ' + @TriggerName  
 EXECUTE sp_executesql @SQLString  
  
 SET @SQLString =   
N'CREATE TRIGGER ' + @TriggerName + ' ON ' + @TablaNombre + ' AFTER UPDATE AS  
BEGIN  
 SET NOCOUNT ON  
 IF ( TRIGGER_NESTLEVEL() = 1 )  
  UPDATE ' + @TablaNombre + '  
  SET  SINC_UPDATED = GETDATE()  
  FROM ' + @TablaNombre + '  
     JOIN Inserted ON ' + @TablaNombre + '.LLAVE = Inserted.LLAVE  
END'  
 EXECUTE sp_executesql @SQLString  
   
 -- 5) Crea trigger de Delete  
 SET @TriggerName = 'SINC_TD_' + @TablaNombre  
 SET @SQLString =   
N'IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id( ''' + @TriggerName + ''') AND xtype in (''TR''))  
  DROP TRIGGER ' + @TriggerName  
 EXECUTE sp_executesql @SQLString  
  
SET @SQLString =   
N'CREATE TRIGGER ' + @TriggerName + ' ON ' + @TablaNombre + ' AFTER DELETE AS  
BEGIN  
 SET NOCOUNT ON
 INSERT INTO T_WFBorrados ( TablaID, BorradoLlave, BorradoDeleted )  
 SELECT  ' + CAST( @TablaID AS VARCHAR( 10 )) + ', LLAVE, GETDATE()  
 FROM   Deleted  
END'  
 EXECUTE sp_executesql @SQLString  
END  

GO 

CREATE PROCEDURE WFTabla_Prepara (
	@TablaID	INT,
	@Nombre		VARCHAR( 50 ),
	@Orden		INT,
	@Activo		BIT,
	@InsertGet	VARCHAR( MAX ),
	@InsertPut	VARCHAR( 250 ),
	@UpdateGet	VARCHAR( MAX ),
	@UpdatePut	VARCHAR( 250 ),
	@Rango		SMALLINT,
	@RangoIni	SMALLINT,
	@RangoFin	SMALLINT,
	@Grupo		INT )
AS
BEGIN
	-- Si no existe,la agrega y crea sus Triggers, else la actualiza
	IF ( SELECT COUNT(*) FROM T_WFTablas WHERE TablaID = @TablaID ) = 0
	BEGIN
	    BEGIN TRAN 
			-- Agrega el registro
			INSERT INTO T_WFTablas ( TablaID, TablaNombre, TablaOrden, TablaActivo,
				TablaInsertGet, TablaInsertPut, TablaInsertLast, TablaUpdateGet, TablaUpdatePut, TablaUpdateLast,
				TablaRango, TablaRangoIni, TablaRangoFin, TablaGrupo )
			VALUES ( @TablaID, @Nombre, @Orden, @Activo,
				@InsertGet, @InsertPut, 0, @UpdateGEt, @UpdatePut, 0,
				@Rango, @RangoIni, @RangoFin, @Grupo )
			-- Prepara los Triggers	
	
			if ( LEFT( @Nombre, 2 ) <> 'V_' )
			begin 				    
				EXEC WFTabla_PreparaCampoLlave @Nombre
				EXEC WFTabla_PreparaTriggers @TablaID
			end 
		
		IF @@ERROR = 0 
		   COMMIT TRAN 
		ELSE 
			ROLLBACK TRANSACTION 

	END
	ELSE
	BEGIN 
		UPDATE	T_WFTablas
		SET		TablaOrden		= @Orden,
				TablaActivo		= @Activo,
				TablaInsertGet	= @InsertGet,
				TablaInsertPut	= @InsertPut,
				TablaUpdateGet	= @UpdateGet,
				TablaUpdatePut	= @UpdatePut,
				TablaRango		= @Rango,
				TablaRangoIni	= @RangoIni,
				TablaRangoFin	= @RangoFin,
				TablaGrupo		= @Grupo
		WHERE	TablaID = @TablaID

		if ( LEFT( @Nombre, 2 ) <> 'V_' )
		begin 				    			
			EXEC WFTabla_PreparaTriggers @TablaID
		end 
	END
END

go 

create procedure  WFSync_GetSincTime ( @SincTime Datetime OUTPUT ) 
as
begin 
	set @SincTime = getdate() 
	return 
end 
GO
CREATE PROCEDURE WFBorrados_GetRegistros(
	@LastSinc	DATETIME,
	@SincTime	DATETIME )
AS
BEGIN
	SELECT	TablaID, BorradoLlave
	FROM	T_WFBorrados
	WHERE	BorradoDeleted >= @LastSinc AND BorradoDeleted < @SincTime
	ORDER BY BorradoID
END

go 
CREATE PROCEDURE WFBorrados_GetLastSync
as
BEGIN
	DECLARE @GLOBAL_WFN_SYNC_BORRADO FolioChico
	DECLARE @Resultado Formula
	DECLARE @Fecha Fecha 
	
	SET @GLOBAL_WFN_SYNC_BORRADO = 313
		
	SELECT @Resultado = GL_FORMULA  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_WFN_SYNC_BORRADO 
	
	SET @Resultado = COALESCE( @Resultado, '1899-12-30');
	
	select convert( Datetime, @Resultado, 120 ) as LastSinc
	
END

GO

CREATE PROCEDURE WFBorrados_SetLastSync( @SincTime	DATETIME ) 
as
BEGIN
	set nocount on 
	DECLARE @GLOBAL_WFN_SYNC_BORRADO FolioChico
	DECLARE @SincTimeStr Formula
	DECLARE @Fecha Fecha 
	
	SET @GLOBAL_WFN_SYNC_BORRADO = 313
	
	set @SincTimeStr = convert(varchar(255), @SincTime, 120 )
		 
	if ( select COUNT(1) from GLOBAL where  GL_CODIGO = @GLOBAL_WFN_SYNC_BORRADO ) = 0 
	begin 
		insert into GLOBAL (GL_CODIGO, GL_DESCRIP, GL_FORMULA ) 
		values (@GLOBAL_WFN_SYNC_BORRADO, 'Ultima sinc. borrado en WFN', @SincTimeStr) 
	end
	else
	begin 
		update GLOBAL set GL_FORMULA = @SincTimeStr where  GL_CODIGO = @GLOBAL_WFN_SYNC_BORRADO  
	end; 		
	
END

go

CREATE PROCEDURE WFTransaccion_SetError (
	@TransID	INT,
	@ErrorID	INT,
	@Error		VARCHAR( max ))
AS
BEGIN
	UPDATE	T_WFTransacciones
	SET		ErrorID		= @ErrorID,
			TransError	= @Error,
			TransFinal	= GETDATE()
	WHERE	TransID = @TransID
END
GO 

CREATE PROCEDURE WFTransaccion_Vacaciones(
	@TransID	INT )
AS
BEGIN
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
END
GO 

CREATE PROCEDURE WFTransaccion_Permisos(
	@TransID	INT )
AS
BEGIN
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
END
GO 

create procedure Certificacion_Agregar ( 
@CB_CODIGO	NumeroEmpleado,
@KI_FEC_CER	Fecha,
@CI_CODIGO	Codigo,
@KI_FOLIO	Descripcion,
@KI_RENOVAR	Dias,
@KI_APROBO	Booleano,
@KI_CALIF_1	Numerico,
@KI_CALIF_2	Numerico,
@KI_CALIF_3	Numerico,
@KI_SINOD_1	DescLarga,
@KI_SINOD_2	DescLarga,
@KI_SINOD_3	DescLarga,
@KI_OBSERVA	Memo,
@US_CODIGO	Usuario
)
as 
begin 
	set nocount on 
	
	insert into KAR_CERT (
		CB_CODIGO,
		KI_FEC_CER,
		CI_CODIGO,
		KI_FOLIO,
		KI_RENOVAR,
		KI_APROBO,
		KI_CALIF_1,
		KI_CALIF_2,
		KI_CALIF_3,
		KI_SINOD_1,
		KI_SINOD_2,
		KI_SINOD_3,
		KI_OBSERVA,
		US_CODIGO) 
	values (
		@CB_CODIGO,
		@KI_FEC_CER,
		@CI_CODIGO,
		@KI_FOLIO,
		@KI_RENOVAR,
		@KI_APROBO,
		@KI_CALIF_1,
		@KI_CALIF_2,
		@KI_CALIF_3,
		@KI_SINOD_1,
		@KI_SINOD_2,
		@KI_SINOD_3,
		@KI_OBSERVA,
		@US_CODIGO
	)
end 
go 

create procedure Certificacion_Actualizar(
@CB_CODIGO	NumeroEmpleado,
@KI_FEC_CER	Fecha,
@CI_CODIGO	Codigo,
@KI_FOLIO	Descripcion,
@KI_RENOVAR	Dias,
@KI_APROBO	Booleano,
@KI_CALIF_1	Numerico,
@KI_CALIF_2	Numerico,
@KI_CALIF_3	Numerico,
@KI_SINOD_1	DescLarga,
@KI_SINOD_2	DescLarga,
@KI_SINOD_3	DescLarga,
@KI_OBSERVA	Memo,
@US_CODIGO	Usuario
)
as 
begin 
set nocount on 
	
	update KAR_CERT 
	set		
		KI_FOLIO = @KI_FOLIO,
		KI_RENOVAR = @KI_RENOVAR, 
		KI_APROBO  = @KI_APROBO,
		KI_CALIF_1 = @KI_CALIF_1,
		KI_CALIF_2 = @KI_CALIF_2,
		KI_CALIF_3 = @KI_CALIF_3,
		KI_SINOD_1 = @KI_SINOD_1,
		KI_SINOD_2 = @KI_SINOD_2,
		KI_SINOD_3 = @KI_SINOD_3,
		KI_OBSERVA = @KI_OBSERVA,
		US_CODIGO = @US_CODIGO
	where CB_CODIGO = @CB_CODIGO and KI_FEC_CER = @KI_FEC_CER and CI_CODIGO = @CI_CODIGO


end 
go 

CREATE PROCEDURE WFTransaccion_Certificaciones(
	@TransID	INT )
AS
BEGIN 
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT
	DECLARE @NOMBRE		Formula 
	
	DECLARE @PermisoInicial Fecha 

    DECLARE @CertificaFecha Fecha -- ="2015/02/21"
    DECLARE @CompetenCodigo Codigo --- ="ABC1"
    DECLARE @CertificaFolio Descripcion -- ="001"
    DECLARE @CertificaRenovar Foliochico -- ="365"
    DECLARE @CertificaAprobado Status  -- ="1"
	DECLARE @CertificaAprobadoBool  Booleano   -- ="1"
    DECLARE @CertificaCalif1 Numerico -- ="85.5"
    DECLARE @CertificaCalif2 Numerico -- ="100"
    DECLARE @CertificaCalif3 Numerico --="0"
    DECLARE @CertificaSinod1 DescLarga -- ="Juan Sinodal"
    DECLARE @CertificaSinod2 DescLarga --=" "
    DECLARE @CertificaSinod3 DescLarga --=" "
    DECLARE @CertificaObserva varchar(max) -- ="Buen esfuerzo"
	DECLARE @US_CODIGO Usuario 
	
	DECLARE @XMLTress  Varchar(max) 

	declare @ClienteID FolioGrande 
			
	SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0) 
	SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
	SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 	
	SET @CertificaFecha   = coalesce(@Cargo.value('(//REGISTRO/@CertificaFecha)[1]', 'DateTime' ), '1899-12-30' ) 	
	SET	@CompetenCodigo		= coalesce( @Cargo.value('(//REGISTRO/@CompetenCodigo)[1]', 'Varchar(6)' ), '' ) 	
	SET	@CertificaFolio		= coalesce( @Cargo.value('(//REGISTRO/@CertificaFolio)[1]', 'Varchar(256)' ), '' ) 	
	
	SET	@CertificaRenovar	= coalesce(@Cargo.value('(//REGISTRO/@CertificaRenovar)[1]', 'INT' ), 0)	
	SET	@CertificaAprobado	= coalesce(@Cargo.value('(//REGISTRO/@CertificaAprobado)[1]', 'INT' ), 0)	

	if ( @CertificaAprobado = 1 ) 
		Set @CertificaAprobadoBool = 'S' 
	else
		Set @CertificaAprobadoBool = 'N' 

	SET	@CertificaCalif1	= coalesce(@Cargo.value('(//REGISTRO/@CertificaCalif1)[1]', 'DECIMAL(15,2)' ), 0.00)
	SET	@CertificaCalif2	= coalesce(@Cargo.value('(//REGISTRO/@CertificaCalif2)[1]', 'DECIMAL(15,2)' ), 0.00)
	SET	@CertificaCalif3	= coalesce(@Cargo.value('(//REGISTRO/@CertificaCalif3)[1]', 'DECIMAL(15,2)' ), 0.00)

	SET	@CertificaSinod1		= coalesce( @Cargo.value('(//REGISTRO/@CertificaSinod1)[1]', 'Varchar(256)' ), '' ) 		
	SET	@CertificaSinod2		= coalesce( @Cargo.value('(//REGISTRO/@CertificaSinod2)[1]', 'Varchar(256)' ), '' ) 		
	SET	@CertificaSinod3		= coalesce( @Cargo.value('(//REGISTRO/@CertificaSinod3)[1]', 'Varchar(256)' ), '' ) 		
	SET	@CertificaObserva	= coalesce( @Cargo.value('(//REGISTRO/@CertificaObserva)[1]', 'Varchar(max)' ), '' ) 			
	
	--- SET	@FileID	= coalesce(@Cargo.value('(//REGISTRO/@FileID)[1]', 'INT' ), 0)	

	set @US_CODIGO = dbo.WFConfig_GetUsuarioTRESS(); 

	if ( select COUNT(*) from KAR_CERT where CB_CODIGO = @CB_CODIGO and KI_FEC_CER = @CertificaFecha and CI_CODIGO = @CompetenCodigo ) > 0 
	begin 
		exec Certificacion_Actualizar @CB_CODIGO, @CertificaFecha, @CompetenCodigo, @CertificaFolio, @CertificaRenovar, @CertificaAprobadoBool, 
			@CertificaCalif1, @CertificaCalif2, @CertificaCalif3, @CertificaSinod1, @CertificaSinod2, @CertificaSinod3, @CertificaObserva , @US_CODIGO 
		
	end 
	else
	begin 
		exec Certificacion_Agregar @CB_CODIGO, @CertificaFecha, @CompetenCodigo, @CertificaFolio, @CertificaRenovar, @CertificaAprobadoBool, 
			@CertificaCalif1, @CertificaCalif2, @CertificaCalif3, @CertificaSinod1, @CertificaSinod2, @CertificaSinod3, @CertificaObserva , @US_CODIGO 
	end; 
		
end 
GO 

CREATE PROCEDURE WFTransaccion_Infonavit(
	@TransID	INT )
AS
BEGIN
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
					
	update KARINF set KI_NOTA = InfoNota  		
	from KARINF KI, ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			InfoTipo	= coalesce(t.c.value('(@InfoTipo)', 'INT' ), 0),		
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,			
			InfoFecha   = coalesce(t.c.value('(@InfoFecha)', 'Datetime' ), '1899-12-30' ) ,			
			InfoNota	= coalesce(t.c.value('(@InfoNota)', 'Varchar(max)' ), '' ) 					
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	where KI.CB_CODIGO = CargoTabla.EmpNumero and KI.KI_FECHA = CargoTabla.InfoFecha  	 

	return 0 
END
GO

CREATE PROCEDURE WFTransaccion_Create(
	@TransID	INT OUTPUT,
	@Nombre		VARCHAR( 255 ),
	@TareaID	INT,
	@ObjetoID	INT,
	@VerboID	INT,
	@Cargo		XML )
AS
BEGIN
	INSERT INTO T_WFTransacciones ( TransNombre, TareaID, ObjetoID, VerboID, TransCargo, ErrorID, TransInicial, TransFinal )
	VALUES ( @Nombre, @TareaID, @ObjetoID, @VerboID, @Cargo, 0, GETDATE(), 0 )
	
	SET @TransID	= SCOPE_IDENTITY()
END
GO

CREATE PROCEDURE WFTransaccion_Aplica(
	@TransID	INT,
	@Error		VARCHAR( 255 ) OUTPUT )
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE	@ObjetoID	INT
	DECLARE @ErrorID	INT
	
	SELECT	@VerboID	= VerboID, @ObjetoID = ObjetoID 
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	

	IF ( @VerboID = 1312 )
		EXEC @ErrorID = WFTransaccion_Vacaciones @TransID
	ELSE 
	IF ( @VerboID = 1304 )
		EXEC @ErrorID = WFTransaccion_Permisos @TransID		
	ELSE
	IF ( @VerboID = 67 ) and ( @ObjetoID = 1090 ) 
	begin 
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE	
	IF ( @VerboID = 1239 ) and ( @ObjetoID = 1060 )    --- Guardar documento(s)
	begin 
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1341 ) and ( @ObjetoID = 1068 )   --- Cambiar datos de empleado
	begin 
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1456 ) and ( @ObjetoID = 1096 )   --- Guardar Excepciones
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1077 ) and ( @ObjetoID = 1308 )   --- Guardar Incapacidades
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1469 ) and ( @ObjetoID = 1075)   --- Guardar Prestamos
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 86) and ( @ObjetoID = 1068)   --- Cambio de Puesto
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 94) and ( @ObjetoID = 1068)   --- Cambio de Turno
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 150) and ( @ObjetoID = 1068)   --- Cambio de Equipo
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 1282) and ( @ObjetoID = 1068)   --- Baja de Empleado
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 90) and ( @ObjetoID = 1068)   --- Cambio de Salario
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE 	
	IF ( @VerboID = 106) and ( @ObjetoID = 1068)   --- Cambio de Area
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE
	IF ( @VerboID = 98) and ( @ObjetoID = 1068)   --- Cambio de Contrato
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 
	ELSE
	IF ( @VerboID = 1285) and ( @ObjetoID = 1071)   --- CRUD Parientes
	begin 		
		-- No tiene programacion adicional 
		set @ErrorID = 0 
	end 	
	ELSE 	
	IF ( @VerboID = 1490 ) and ( @ObjetoID = 1107 )   --- Guardar cambios Infonavit 
	begin 	
		-- No tiene programacion adicional 
		set @ErrorID = 0 
		EXEC @ErrorID = WFTransaccion_Infonavit @TransID		

	end 
	ELSE
    IF ( @VerboID = 2085 ) and ( @ObjetoID = 1213 )   --- Guardar Seguro de Gastos Medicos Empleado
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end 
	ELSE
	IF ( @VerboID = 100269 ) and ( @ObjetoID = 1051 )   --- Realiza intercambio de festivos
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end 
	ELSE 
	IF ( @VerboID = 1281 ) and ( @ObjetoID = 1068 )   --- Realiza Alta de Empleado oficial 
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end 
    ELSE
    IF ( @VerboID = 80 ) and ( @ObjetoID = 1068 )   --- Realiza Reingreso de Empleado 
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end     
    ELSE
    IF ( @VerboID = 2138 ) and ( @ObjetoID = 1222 )   --- Realiza Checada Manual
    begin 
         -- No tiene programacion adicional 
         set @ErrorID = 0 
    end  
	ELSE
	BEGIN
		SET @ErrorID	= -1
		EXEC WFTransaccion_SetError @TransID, @ErrorID, 'Tipo de Transacci�n no es v�lido'
	END
	
	IF ( @ErrorID <> 0 )
		SELECT	@Error	= TransError
		FROM	T_WFTransacciones
		WHERE	TransID = @TransID
	ELSE
	BEGIN 
		SELECT	@Error	= TransError, @ErrorID =  ErrorID
		FROM	T_WFTransacciones
		WHERE	TransID = @TransID				
	END 

	set @Error   = coalesce(@Error, '')
	set @ErrorID = coalesce(@ErrorID, 0 ) 

	RETURN @ErrorID
END
GO

CREATE PROCEDURE WFTransaccion_GetXMLVacaciones( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    DECLARE @EMPRESA  varchar(10) 
    DECLARE @CB_CODIGO    INT
    DECLARE @NOMBRE        Formula 
    DECLARE @VaFecIni  Fecha 
    DECLARE @VaFecFin  Fecha 
    DECLARE @VaDias    Pesos
    DECLARE @VaDiasGozo Pesos 
    DECLARE @VaDiasPago Pesos 
    DECLARE @VaPrima     Pesos
    DECLARE @PeriodoID    FolioGrande  
    DECLARE @VaNomYear Anio 
    DECLARE @VaNomTipo NominaTipo
    DECLARE @VaNomNum  NominaNumero
    DECLARE @VaPeriodo  Descripcion 
    DECLARE @VaComenta    Titulo 
	DECLARE @UsarVaGozo status 
    DECLARE @XMLTress  Varchar(max)     

    declare @ClienteID FolioGrande 
    set @FileID = 0         

    select  
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            CB_CODIGO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
            NOMBRE        = coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),    
            VA_FEC_INI   = coalesce(t.c.value('(@VacacionInicial)', 'DateTime' ), '1899-12-30' ) ,
            VA_FEC_FIN  = coalesce(t.c.value('(@VacacionFinal)', 'DateTime' ), '1899-12-30' ) ,
            VaDias        = coalesce(t.c.value('(@VacacionDias)', 'DECIMAL(15,2)' ), 0.00),
            VA_GOZO    = coalesce(t.c.value('(@VacacionGozo)', 'DECIMAL(15,2)' ), 0.00),
            VA_PAGO    = coalesce(t.c.value('(@VacacionPago)', 'DECIMAL(15,2)' ), 0.00),
            VA_PRIMA    =      coalesce(t.c.value('(@VacacionPrima)', 'DECIMAL(15,2)' ), 0.00),
            VA_NOMYEAR    = coalesce(t.c.value('(@PeriodoYear)', 'INT' ), 0),
            VA_NOMTIPO    = coalesce(t.c.value('(@TipoNominaCodigo)', 'INT' ), 0),
            VA_NOMNUME    = coalesce(t.c.value('(@PeriodoNumero)', 'INT' ), 0),
            VA_PERIODO    = coalesce(t.c.value('(@VacacionPeriodo)', 'Varchar(256)' ), '' ) ,
            VA_COMENTA  = coalesce(t.c.value('(@VacacionDescrip)', 'Varchar(255)' ), '' ), 
			USAR_VA_GOZO = coalesce(tr.cr.value('(@VacacionGozoEditar)', 'INT' ), 0),
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)    
        
     into #VACA_TEMP
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 cross apply @Cargo.nodes('//REGISTROS') tr(cr)
     

    select TOP 1 
        @VaPeriodo = VA_PERIODO, 
        @VaComenta = VA_COMENTA, 
        @VaNomYear = VA_NOMYEAR, 
        @VaNomTipo = VA_NOMTIPO, 
        @VaNomNum = VA_NOMNUME, 
		@UsarVaGozo = USAR_VA_GOZO
    from #VACA_TEMP order by VA_FEC_INI
     

         declare @XML_Mult XML 

         set @XML_Mult = (     
           select 
                EMPRESA, 
                'VACA' as MOV3,
				CB_CODIGO as EMPLEADO, 
                VA_FEC_INI = convert( varchar(20), MIN(VA_FEC_INI), 112 ), 
                VA_FEC_FIN = convert( varchar(20), MAX(VA_FEC_FIN), 112  ),
				USAR_VA_GOZO = @UsarVaGozo, 
                VA_GOZO = SUM(VA_GOZO), 
                VA_PAGO = SUM(VA_PAGO), 
                VA_P_PRIMA = SUM( VA_PRIMA ),
                VA_PERIODO = MAX(VA_PERIODO),
                VA_COMENTA  = MAX(VA_COMENTA),  
                (     
            select  
                convert( varchar(20), VA_FEC_INI , 112 )   as VA_FEC_INI, 
                convert( varchar(20), VA_FEC_FIN , 112 )   as VA_FEC_FIN,   
				USAR_VA_GOZO = @UsarVaGozo,       
                VA_GOZO, 
                VA_PAGO, 
                VA_NOMTIPO, 
                VA_NOMNUME, 
                VA_NOMYEAR 
            from #VACA_TEMP VTU
            where VTG.CB_CODIGO  = VTU.CB_CODIGO and VTG.EMPRESA = VTU.EMPRESA 
            order by VA_FEC_INI 
            FOR XML PATH('VACACION'), ROOT('GENERAR_MULTIPLE'), TYPE )
           from #VACA_TEMP VTG
            group by EMPRESA, CB_CODIGO 
            FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
         )
                     
        set @XML = @XML_Mult

    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #VACA_TEMP     
    
end
GO


create  PROCEDURE WFTransaccion_GetXMLReingresoEmpleado( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA			varchar(10) 
	DECLARE @kCB_CODIGO			INT
	DECLARE @NOMBRE				Formula 
	DECLARE @KarFecha			Fecha 
	
	DECLARE @PersonaID			FolioGrande 		
	DECLARE @EquipoID			FolioGrande  	
	DECLARE @PersonaTipo		int 
	DECLARE @KarObservaciones	Titulo  
	DECLARE @kCB_AUTOSAL		Booleano
	DECLARE @kCB_AUTOSAL_GLOBAL Booleano

	DECLARE @kCB_APE_MAT	Descripcion
	DECLARE @kCB_APE_PAT	Descripcion
	DECLARE @kCB_NOMBRES	Descripcion


	DECLARE @kCB_BAN_ELE	Descripcion
	DECLARE @kCB_CALLE		Observaciones
	DECLARE @kCB_CARRERA	DescLarga
	DECLARE @kCB_CHECA		Booleano
	DECLARE @kCB_CIUDAD		Descripcion
	DECLARE @kCB_CLASIFI	Codigo
	DECLARE @kCB_CODPOST	Referencia
	DECLARE @kCB_COLONIA	Descripcion
	DECLARE @kCB_CONTRAT	Codigo1
	DECLARE @kCB_CREDENC	Codigo1
	DECLARE @kCB_CTA_VAL	Descripcion
	DECLARE @kCB_CURP		Descripcion
	DECLARE @kCB_E_MAIL		Formula
	DECLARE @kCB_ESTADO		Codigo2
	DECLARE @kCB_ESTUDIO	Codigo2
	DECLARE @kCB_FEC_ANT	Fecha
	DECLARE @kCB_FEC_CON	Fecha
	DECLARE @kCB_FEC_ING	Fecha
	DECLARE @kCB_FEC_NAC	Fecha
	DECLARE @kCB_LUG_NAC	Descripcion
	DECLARE @kCB_MUNICIP	Codigo
	DECLARE @kCB_NACION		Descripcion
	DECLARE @kCB_NIVEL0		Codigo
	DECLARE @kCB_NIVEL1		Codigo
	DECLARE @kCB_NIVEL2		Codigo
	DECLARE @kCB_NIVEL3		Codigo
	DECLARE @kCB_NIVEL4		Codigo
	DECLARE @kCB_NIVEL5		Codigo
	DECLARE @kCB_NIVEL6		Codigo
	DECLARE @kCB_NIVEL7		Codigo
	DECLARE @kCB_NIVEL8		Codigo
	DECLARE @kCB_NIVEL9		Codigo
	DECLARE @kCB_PATRON		RegPatronal
	DECLARE @kCB_PUESTO		Codigo
	DECLARE @kCB_RFC		Descripcion
	DECLARE @kCB_SALARIO	PesosDiario
	DECLARE @PersonaSalario PesosDiario
	DECLARE @kCB_SALARIO_BRUTO	PesosDiario
	DECLARE @kCB_SEGSOC		Descripcion
	DECLARE @kCB_SEXO		Codigo1
	DECLARE @kCB_TABLASS	Codigo1
	DECLARE @kCB_TEL		Descripcion
	DECLARE @kCB_TURNO		Codigo
	DECLARE @kCB_ZONA_GE	ZonaGeo

	---PADRES
	DECLARE @kNOM_PADRE	Descripcion
	DECLARE @kNOM_MADRE	Descripcion

	---- CAMPOS NUEVOS A MAPEAR
	DECLARE @kCB_AR_FEC as Fecha
	DECLARE @kCB_AR_HOR as Hora
	DECLARE @kCB_EDO_CIV as Codigo1
	DECLARE @kCB_FEC_RES as Fecha
	DECLARE @kCB_EST_HOR as Descripcion
	DECLARE @kCB_EST_HOY as Booleano
	DECLARE @kCB_EST_HOY_INT as Status
	DECLARE @kCB_EVALUA as DiasFrac
	DECLARE @kCB_EXPERIE as Titulo
	DECLARE @kCB_FEC_BAJ as Fecha
	DECLARE @kCB_FEC_BSS as Fecha
	DECLARE @kCB_FEC_INT as Fecha
	DECLARE @kCB_FEC_REV as Fecha
	DECLARE @kCB_FEC_VAC as Fecha
	DECLARE @kCB_G_FEC_1 as Fecha
	DECLARE @kCB_G_FEC_2 as Fecha
	DECLARE @kCB_G_FEC_3 as Fecha
	DECLARE @kCB_G_FEC_4 as Fecha
	DECLARE @kCB_G_FEC_5 as Fecha
	DECLARE @kCB_G_FEC_6 as Fecha
	DECLARE @kCB_G_FEC_7 as Fecha
	DECLARE @kCB_G_FEC_8 as Fecha
	DECLARE @kCB_G_LOG_1 as Booleano
	DECLARE @kCB_G_LOG_1_INT as Status
	DECLARE @kCB_G_LOG_2 as Booleano
	DECLARE @kCB_G_LOG_2_INT as Status
	DECLARE @kCB_G_LOG_3 as Booleano
	DECLARE @kCB_G_LOG_3_INT as Status
	DECLARE @kCB_G_LOG_4 as Booleano
	DECLARE @kCB_G_LOG_4_INT as Status
	DECLARE @kCB_G_LOG_5 as Booleano
	DECLARE @kCB_G_LOG_5_INT as Status
	DECLARE @kCB_G_LOG_6 as Booleano
	DECLARE @kCB_G_LOG_6_INT as Status
	DECLARE @kCB_G_LOG_7 as Booleano
	DECLARE @kCB_G_LOG_7_INT as Status
	DECLARE @kCB_G_LOG_8 as Booleano
	DECLARE @kCB_G_LOG_8_INT as Status
	DECLARE @kCB_G_NUM_1 as Pesos
	DECLARE @kCB_G_NUM_2 as Pesos
	DECLARE @kCB_G_NUM_3 as Pesos
	DECLARE @kCB_G_NUM_4 as Pesos
	DECLARE @kCB_G_NUM_5 as Pesos
	DECLARE @kCB_G_NUM_6 as Pesos
	DECLARE @kCB_G_NUM_7 as Pesos
	DECLARE @kCB_G_NUM_8 as Pesos
	DECLARE @kCB_G_NUM_9 as Pesos
	DECLARE @kCB_G_NUM10 as Pesos
	DECLARE @kCB_G_NUM11 as Pesos
	DECLARE @kCB_G_NUM12 as Pesos
	DECLARE @kCB_G_NUM13 as Pesos
	DECLARE @kCB_G_NUM14 as Pesos
	DECLARE @kCB_G_NUM15 as Pesos
	DECLARE @kCB_G_NUM16 as Pesos
	DECLARE @kCB_G_NUM17 as Pesos
	DECLARE @kCB_G_NUM18 as Pesos
	DECLARE @kCB_G_TAB_1 as Codigo
	DECLARE @kCB_G_TAB_2 as Codigo
	DECLARE @kCB_G_TAB_3 as Codigo
	DECLARE @kCB_G_TAB_4 as Codigo
	DECLARE @kCB_G_TAB_5 as Codigo
	DECLARE @kCB_G_TAB_6 as Codigo
	DECLARE @kCB_G_TAB_7 as Codigo
	DECLARE @kCB_G_TAB_8 as Codigo
	DECLARE @kCB_G_TAB_9 as Codigo
	DECLARE @kCB_G_TAB10 as Codigo
	DECLARE @kCB_G_TAB11 as Codigo
	DECLARE @kCB_G_TAB12 as Codigo
	DECLARE @kCB_G_TAB13 as Codigo
	DECLARE @kCB_G_TAB14 as Codigo
	DECLARE @kCB_G_TEX_1 as Descripcion
	DECLARE @kCB_G_TEX_2 as Descripcion
	DECLARE @kCB_G_TEX_3 as Descripcion
	DECLARE @kCB_G_TEX_4 as Descripcion
	DECLARE @kCB_G_TEX_5 as Descripcion
	DECLARE @kCB_G_TEX_6 as Descripcion
	DECLARE @kCB_G_TEX_7 as Descripcion
	DECLARE @kCB_G_TEX_8 as Descripcion
	DECLARE @kCB_G_TEX_9 as Descripcion
	DECLARE @kCB_G_TEX10 as Descripcion
	DECLARE @kCB_G_TEX11 as Descripcion
	DECLARE @kCB_G_TEX12 as Descripcion
	DECLARE @kCB_G_TEX13 as Descripcion
	DECLARE @kCB_G_TEX14 as Descripcion
	DECLARE @kCB_G_TEX15 as Descripcion
	DECLARE @kCB_G_TEX16 as Descripcion
	DECLARE @kCB_G_TEX17 as Descripcion
	DECLARE @kCB_G_TEX18 as Descripcion
	DECLARE @kCB_G_TEX19 as Descripcion
	DECLARE @kCB_HABLA as Booleano
	DECLARE @kCB_HABLA_INT as Status
	DECLARE @kCB_IDIOMA as Descripcion
	DECLARE @kCB_INFCRED as Descripcion
	DECLARE @kCB_INFMANT as Booleano
	DECLARE @kCB_INFMANT_INT as Status
	DECLARE @kCB_INFTASA as DescINFO
	DECLARE @kCB_LA_MAT as Descripcion
	DECLARE @kCB_LAST_EV as Fecha
	DECLARE @kCB_MAQUINA as Titulo
	DECLARE @kCB_MED_TRA as Codigo1
	DECLARE @kCB_PASAPOR as Booleano
	DECLARE @kCB_PASAPOR_INT as Status
	DECLARE @kCB_FEC_INC as Fecha
	DECLARE @kCB_FEC_PER as Fecha
	DECLARE @kCB_NOMYEAR as Anio
	DECLARE @kCB_NOMTIPO as NominaTipo
	DECLARE @kCB_NEXT_EV as Fecha
	DECLARE @kCB_NOMNUME as NominaNumero
	DECLARE @kCB_SAL_INT as PesosDiario
	DECLARE @kCB_VIVECON as Codigo1
	DECLARE @kCB_VIVEEN as Codigo2
	DECLARE @kCB_ZONA as Referencia
	DECLARE @kCB_INFTIPO as Status
	DECLARE @kCB_DER_FEC as Fecha
	DECLARE @kCB_DER_PAG as DiasFrac
	DECLARE @kCB_V_PAGO as DiasFrac
	DECLARE @kCB_DER_GOZ as DiasFrac
	DECLARE @kCB_V_GOZO as DiasFrac
	DECLARE @kCB_TIP_REV as Codigo
	DECLARE @kCB_MOT_BAJ as Codigo3
	DECLARE @kCB_FEC_SAL as Fecha
	DECLARE @kCB_INF_INI as Fecha
	DECLARE @kCB_INF_OLD as DescINFO
	DECLARE @kCB_OLD_SAL as PesosDiario
	DECLARE @kCB_OLD_INT as PesosDiario
	DECLARE @kCB_PRE_INT as PesosDiario
	DECLARE @kCB_PER_VAR as PesosDiario
	DECLARE @kCB_SAL_TOT as PesosDiario
	DECLARE @kCB_TOT_GRA as PesosDiario
	DECLARE @kCB_FAC_INT as Tasa
	DECLARE @kCB_RANGO_S as Tasa
	DECLARE @kCB_CLINICA as Codigo3
	DECLARE @kCB_FEC_NIV as Fecha
	DECLARE @kCB_FEC_PTO as Fecha
	DECLARE @kCB_AREA as Codigo
	DECLARE @kCB_FEC_TUR as Fecha
	DECLARE @kCB_FEC_KAR as Fecha
	DECLARE @kCB_PENSION as Status
	DECLARE @kCB_CANDIDA as FolioGrande
	DECLARE @kCB_ID_NUM as Descripcion
	DECLARE @kCB_ENT_NAC as Codigo2
	DECLARE @kCB_COD_COL as Codigo
	DECLARE @kCB_PASSWRD as Passwd
	DECLARE @kCB_PLAZA as FolioGrande
	DECLARE @kCB_FEC_PLA as Fecha
	DECLARE @kCB_DER_PV as DiasFrac
	DECLARE @kCB_V_PRIMA as DiasFrac
	DECLARE @kCB_SUB_CTA as Descripcion
	DECLARE @kCB_NETO as Pesos
	DECLARE @kCB_PORTAL as Booleano
	DECLARE @kCB_PORTAL_INT as Status
	DECLARE @kCB_DNN_OK as Booleano
	DECLARE @kCB_DNN_OK_INT as Status
	DECLARE @kCB_USRNAME as DescLarga
	DECLARE @kCB_NOMINA as NominaTipo
	DECLARE @kCB_FEC_NOM as Fecha
	DECLARE @kCB_RECONTR as Booleano
	DECLARE @kCB_RECONTR_INT as Status
	DECLARE @kCB_DISCAPA as Booleano
	DECLARE @kCB_DISCAPA_INT as Status
	DECLARE @kCB_INDIGE as Booleano
	DECLARE @kCB_INDIGE_INT as Status
	DECLARE @kCB_FONACOT as Descripcion
	DECLARE @kCB_EMPLEO as Booleano
	DECLARE @kCB_EMPLEO_INT as Status
	DECLARE @kUS_CODIGO as Usuario
	DECLARE @kCB_FEC_COV as Fecha
	DECLARE @kCB_G_TEX20 as Descripcion
	DECLARE @kCB_G_TEX21 as Descripcion
	DECLARE @kCB_G_TEX22 as Descripcion
	DECLARE @kCB_G_TEX23 as Descripcion
	DECLARE @kCB_G_TEX24 as Descripcion
	DECLARE @kCB_INFDISM as Booleano
	DECLARE @kCB_INFDISM_INT as Status
	DECLARE @kCB_INFACT as Booleano
	DECLARE @kCB_INFACT_INT as Status
	DECLARE @kCB_NUM_EXT as NombreCampo 
	DECLARE @kCB_NUM_INT as NombreCampo
	DECLARE @kCB_INF_ANT as Fecha
	DECLARE @kCB_TDISCAP as Status
	DECLARE @kCB_ESCUELA as DescLarga
	DECLARE @kCB_TESCUEL as Status
	DECLARE @kCB_TITULO as Status
	DECLARE @kCB_YTITULO as Anio
	DECLARE @kCB_CTA_GAS as Descripcion
	DECLARE @kCB_ID_BIO as FolioGrande
	DECLARE @kCB_GP_COD as Codigo
	DECLARE @kCB_TIE_PEN as Booleano
	DECLARE @kCB_TIE_PEN_INT as Status
	DECLARE @kCB_TSANGRE as Descripcion
	DECLARE @kCB_ALERGIA as Formula
	DECLARE @kCB_BRG_ACT as Booleano
	DECLARE @kCB_BRG_ACT_INT as Status
	DECLARE @kCB_BRG_TIP as Status
	DECLARE @kCB_BRG_ROL as Booleano
	DECLARE @kCB_BRG_ROL_INT as Status
	DECLARE @kCB_BRG_JEF as Booleano
	DECLARE @kCB_BRG_JEF_INT as Status
	DECLARE @kCB_BRG_CON as Booleano
	DECLARE @kCB_BRG_CON_INT as Status
	DECLARE @kCB_BRG_PRA as Booleano
	DECLARE @kCB_BRG_PRA_INT as Status
	DECLARE @kCB_BRG_NOP as Descripcion
	DECLARE @kCB_BANCO as Codigo
	DECLARE @kCB_REGIMEN as Status
	----- FIN	
	
	DECLARE @XMLTress  Varchar(max) 
	

	declare @ClienteID FolioGrande 
	set @FileID = 0 
	SET @ErrorID = 0 
	set @ERROR = '' 

	--Toma globales de Empleado 		
	select 
		@kCB_Ciudad = Ciudad , 
		@kCB_Estado = Estado , 
		@kCB_MUNICIP = Municipio , 
		@kCB_Checa = Checa , 
		@kCB_Nacion = Nacion , 
		@kCB_Sexo = Sexo , 
		@kCB_Credenc = Credencial , 
		@kCB_EDO_CIV = EstadoCivil , 
		@kCB_ViveEn = ViveEn , 
		@kCB_ViveCon = ViveCon , 
		@kCB_MED_TRA = MedioTransporte , 
		@kCB_Estudio = Estudio , 
		@kCB_AUTOSAL_GLOBAL = AutoSal , 
		@kCB_Zona = ZonaGeografica , 
		@kCB_Patron = Patron , 
		@kCB_Salario = Salario , 
		@kCB_Contrat = Contrato , 
		@kCB_Puesto = Puesto , 
		@kCB_Clasifi = Clasifi , 
		@kCB_Turno = Turno , 
		@kCB_TablaSS = TablaSS , 
		@kCB_Nivel1 = Nivel1 , 
		@kCB_Nivel2 = Nivel2 , 
		@kCB_Nivel3 = Nivel3 , 
		@kCB_Nivel4 = Nivel4 , 
		@kCB_Nivel5 = Nivel5 , 
		@kCB_Nivel6 = Nivel6 , 
		@kCB_Nivel7 = Nivel7 , 
		@kCB_Nivel8 = Nivel8 , 
		@kCB_Nivel9 = Nivel9 , 
		@kCB_Regimen = Regimen , 
		@KCB_Banco = Banco 
	from dbo.WF_GetGlobalesEmpleado() 

	SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0) 
	SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
	
	SET	@PersonaID	= coalesce(  @Cargo.value('(//REGISTRO/@PersonaID)[1]', 'INT' ),  @TransID )     	
	
	--- IDENTIFICACION DEL EMPLEADO 
	SET	@KCB_APE_PAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApePat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_APE_MAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApeMat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_NOMBRES		= coalesce( @Cargo.value('(//REGISTRO/@PersonaNombres)[1]', 'Varchar(256)' ), '' ) 	
	SET @kCB_FEC_NAC  = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET	@KCB_RFC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaRFC)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_CURP		= coalesce( @Cargo.value('(//REGISTRO/@PersonaCURP)[1]', 'Varchar(256)' ), '' ) 
	SET	@kCB_SEGSOC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaIMSS)[1]', 'Varchar(256)' ), '' ) 	
	SET	@KCB_SEXO		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaGenero)[1]', 'int' ), -1 ) ) 
									when  1 then 'M' 
	                                when  2 then 'F'
									else  @KCB_SEXO 
						end;  
	SET @kCB_ENT_NAC = coalesce( @Cargo.value('(//REGISTRO/@NacEstadoCodigo)[1]', 'Varchar(2)' ), '' ) -- as Codigo2
	
	-- PERSONALES  
	SET @kCB_LUG_NAC   = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacCiudad)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_NACION    = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacionalidad)[1]', 'Varchar(30)' ), '' ) 	
	SET	@kCB_PASAPOR_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaPasaporte)[1]', 'INT' ), 0)	
	if ( @kCB_PASAPOR_INT = 1 ) 
		Set @kCB_PASAPOR = 'S' 
	else
		Set @kCB_PASAPOR = 'N'
	SET @kCB_EDO_CIV = coalesce( @Cargo.value('(//REGISTRO/@EdoCivilCodigo)[1]', 'Varchar(1)' ), @kCB_EDO_CIV ) --Estado Civil
	SET @kCB_LA_MAT = coalesce( @Cargo.value('(//REGISTRO/@PersonaLugarMat)[1]', 'Varchar(30)' ), '' ) --Lugar y Fecha de matrimonio
	SET @kNOM_PADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomPad)[1]', 'Varchar(30)' ), '' )
	SET @kNOM_MADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomMad)[1]', 'Varchar(30)' ), '' )
	SET @kCB_MED_TRA = coalesce( @Cargo.value('(//REGISTRO/@TransporteCodigo)[1]', 'Varchar(1)' ), @kCB_MED_TRA  ) --Transporte	
	SET	@kCB_DISCAPA_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaSufreDiscapa)[1]', 'INT' ), 0)	
	if ( @kCB_DISCAPA_INT = 1 ) 
		Set @kCB_DISCAPA = 'S' 
	else
		Set @kCB_DISCAPA = 'N'
	SET @kCB_TDISCAP = coalesce(@Cargo.value('(//REGISTRO/@PersonaDiscapa)[1]', 'INT' ), 0) -- as Status		
	--SET @kCB_INDIGE as Booleano
	SET	@kCB_INDIGE_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaIndigena)[1]', 'INT' ), 0)	
	if ( @kCB_INDIGE_INT = 1 ) 
		Set @kCB_INDIGE = 'S' 
	else
		Set @kCB_INDIGE = 'N'
		
	-- DOMICILIO 
	SET @kCB_CALLE = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCalle)[1]', 'Varchar(50)' ), '' ) 
	SET @kCB_NUM_EXT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumExt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_NUM_INT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumInt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_COLONIA = coalesce( @Cargo.value('(//REGISTRO/@DomicilioColonia)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_CODPOST = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCodPost)[1]', 'Varchar(8)' ), '' ) 
	SET @kCB_COD_COL = coalesce( @Cargo.value('(//REGISTRO/@ColoniaCodigo)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_CLINICA = coalesce( @Cargo.value('(//REGISTRO/@Clinica)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_ZONA 	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioZona)[1]', 'Varchar(8)' ), @kCB_ZONA )  -- as Referencia	
	SET	@KCB_CIUDAD	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioCiudad)[1]', 'Varchar(30)' ), @kCB_Ciudad ) 	
	SET	@KCB_ESTADO	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstadoCodigo)[1]', 'Varchar(6)' ), @kCB_ESTADO ) 
	SET @kCB_MUNICIP =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@MunicipioCodigo)[1]', 'Varchar(6)' ), @kCB_MUNICIP ) 
	
	-- CONTACTO 
	SET @kCB_TEL = coalesce( @Cargo.value('(//REGISTRO/@PersonaTelefono)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_E_MAIL = coalesce( @Cargo.value('(//REGISTRO/@PersonaEmail)[1]', 'Varchar(255)' ), '' ) 
	SET @kCB_VIVECON = coalesce( @Cargo.value('(//REGISTRO/@ViveConCodigo)[1]', 'Varchar(1)' ), @kCB_ViveCon  )  -- as Codigo1
	SET @kCB_VIVEEN = coalesce( @Cargo.value('(//REGISTRO/@ViveEnCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_FEC_RES = coalesce(@Cargo.value('(//REGISTRO/@ResidenciaFecha)[1]', 'Date' ), '1899-12-30' ) --Fecha Residencia 	

	--- EXPERIENCIA  
	-- Exp. Academica 
	SET @kCB_ESTUDIO =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstudioCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_ESCUELA =  coalesce( @Cargo.value('(//REGISTRO/@PersonaEscuela)[1]', 'Varchar(40)' ), '' )	
	SET @kCB_TESCUEL = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstTipo)[1]', 'INT' ), 0) -- as Status
	SET @kCB_CARRERA  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCarrera)[1]', 'Varchar(40)' ), '' ) 		
	SET @kCB_TITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaTitulo)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_YTITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstYear)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_EST_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaEstudiaHorario)[1]', 'Varchar(30)' ), '' ) --Carrera y Horario
	---DECLARE @kCB_EST_HOY as Booleano
	SET	@kCB_EST_HOY_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstudia)[1]', 'INT' ), 0)	
	if ( @kCB_EST_HOY_INT = 1 ) 
		Set @kCB_EST_HOY = 'S' 
	else
		Set @kCB_EST_HOY = 'N'

	-- Habilidades 
	SET	@kCB_HABLA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaOtroIdioma)[1]', 'INT' ), 0)	
	if ( @kCB_HABLA_INT = 1 ) 
		Set @kCB_HABLA = 'S' 
	else
		Set @kCB_HABLA = 'N'
	SET @kCB_IDIOMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdiomas)[1]', 'Varchar(30)' ), '' ) --Idioma o dominio
	SET @kCB_MAQUINA = coalesce( @Cargo.value('(//REGISTRO/@PersonaMaquinas)[1]', 'Varchar(100)' ), '' ) --Maquinas que conoce
	SET @kCB_EXPERIE = coalesce( @Cargo.value('(//REGISTRO/@PersonaExperiencia)[1]', 'Varchar(100)' ), '' )--Experiencia
	
	
	-- Contratacion 	
	SET @kCB_FEC_ING  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_FEC_ANT  = coalesce(@Cargo.value('(//REGISTRO/@EmpAntigFecha)[1]', 'Date' ), '1899-12-30' ) 	
	
	-- Contrato 
	SET @kCB_FEC_CON  = coalesce(@Cargo.value('(//REGISTRO/@EmpContratoIni)[1]', 'Date' ), @kCB_FEC_ING ) 	
	SET	@KCB_CONTRAT  =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ContratoCodigo)[1]', 'Varchar(6)' ), @KCB_CONTRAT ) 
		--Calcular el numero de dias para la fecha de vencimiento del contrato
		declare @DiasContrato as FolioChico
		SELECT @DiasContrato = TB_DIAS FROM CONTRATO WHERE TB_CODIGO = @kCB_CONTRAT
		if ( @DiasContrato > 0 )
		begin		 
			set @kCB_FEC_COV = cast( dateadd( day, @DiasContrato -1, @kCB_FEC_CON ) as Datetime ) 
		end
	
		
	SET	@KCB_PUESTO		= coalesce( @Cargo.value('(//REGISTRO/@PuestoCodigo)[1]', 'Varchar(6)' ), '' ) 
	SET @kCB_NOMINA 	= coalesce(@Cargo.value('(//REGISTRO/@TipoNominaCodigo)[1]', 'INT' ), @kCB_NOMINA) -- as NominaTipo
	
	-- Tomados del puesto en caso de que no esten definidos 
	----------------- TOMADOS DEL PUESTO  --------------------------------
	declare @kPU_PATRON Codigo1 
	declare @kPU_TURNO Codigo 
	declare @kPU_AUTOSAL Codigo 
	declare @kPU_TABLASS Codigo 
	declare @KPU_CLASIFI Codigo 
	
	select @KPU_CLASIFI = PU_CLASIFI, @kPU_PATRON = PU_PATRON, @kPU_TURNO = PU_TURNO,  @kPU_AUTOSAL  = PU_AUTOSAL, @kPU_TABLASS = PU_TABLASS from PUESTO where PU_CODIGO = @kCB_PUESTO 

	-- Si no estan definidas en el puesto tomas las default 
	set @kCB_AUTOSAL_GLOBAL = @KCB_AUTOSAL;
    set @kCB_PATRON  = case  coalesce(@kPU_PATRON,'')   when '' then @kCB_PATRON   else @kPU_PATRON  end
	set @kCB_TURNO = case  coalesce(@kPU_TURNO,'')  when '' then @kCB_TURNO  else @kPU_TURNO end	
	set @kCB_TABLASS = case  coalesce(@kPU_TABLASS,'')  when '' then @kCB_TABLASS  else @kPU_TABLASS end
	set @kCB_CLASIFI = case  coalesce(@KPU_CLASIFI,'')  when '' then @kCB_CLASIFI  else @KPU_CLASIFI end
	set @kCB_CLASIFI = coalesce( @kCB_CLASIFI, '' ); 

    -- Al ultimo lee los del Cargo 	
	SET	@KCB_CLASIFI 	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TabuladorCodigo)[1]', 'Varchar(6)' ), @KCB_CLASIFI ) 
	SET	@KCB_TURNO		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TurnoCodigo)[1]', 'Varchar(6)' ), @KCB_TURNO ) 
	SET	@KCB_PATRON		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@RegPatronCodigo)[1]', 'Varchar(1)' ), @KCB_PATRON ) 	
		
	-- TimbradoNomina 
	SET @kCB_REGIMEN = coalesce(@Cargo.value('(//REGISTRO/@RegimenSAT)[1]', 'INT' ), @kCB_REGIMEN) -- as Status
	
	
	-- AREA 
	SET	@kCB_NIVEL1		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel1Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL1 ) 
	SET	@kCB_NIVEL2		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel2Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL2 ) 
	SET	@kCB_NIVEL3		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel3Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL3 ) 
	SET	@kCB_NIVEL4		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel4Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL4 ) 
	SET	@kCB_NIVEL5		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel5Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL5 ) 
	SET	@kCB_NIVEL6		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel6Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL6 ) 
	SET	@kCB_NIVEL7		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel7Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL7 ) 
	SET	@kCB_NIVEL8		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel8Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL8 ) 
	SET	@kCB_NIVEL9		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel9Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL9 ) 
	
	
    -- SALARIO 	
	SET	@KCB_TABLASS		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@PrestaLeyCodigo)[1]', 'Varchar(6)' ), @KCB_TABLASS ) 	
	SET	@KCB_AUTOSAL	=case coalesce( @Cargo.value('(//REGISTRO/@SalTabula)[1]', 'int' ),  -1 ) 
								    when  0 then 'N'
									when  1 then 'S'
									else @KCB_AUTOSAL
						end;	
	SET	@kCB_SALARIO	= coalesce( @Cargo.value('(//REGISTRO/@SalDiario)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET @kCB_PER_VAR    = coalesce( @Cargo.value('(//REGISTRO/@PromVar)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET	@KCB_ZONA_GE	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ZonaGeo)[1]', 'Varchar(1)' ), 'A' ) 	
	
	
	-- Datos del movimiento 
	SET @KarFecha  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 		
	SET @KarObservaciones  = coalesce(@Cargo.value('(//REGISTRO/@KarObservaciones)[1]', 'Varchar(255)' ), '' ) 
	SET	@FileID	= coalesce(@Cargo.value('(//REGISTRO/@FileID)[1]', 'INT' ), 0)	
	
	SET	@KCB_CHECA		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaCheca)[1]', 'int' ), -1 ) ) 
	                                when  0 then 'N'
									when  1 then 'S'
									else @KCB_CHECA
						end;  




	
	
	-----Campos que faltaban de mapear del XML "CargoAltaEmpleado.xml"  -------
	SET @kCB_CTA_VAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaVale)[1]', 'Varchar(30)' ), '' )
	

	---- Campos Faltantes que no se encontraban en el XML "CargoAltaEmpleado.xml" para futuras Altas
	SET @kCB_AR_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaArFec)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_AR_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaArHor)[1]', 'Varchar(4)' ), '' )
	
	
	
	SET @kCB_EVALUA = coalesce( @Cargo.value('(//REGISTRO/@PersonaEvalua)[1]', 'Decimal(15,2)' ), 0.00 ) --Resultado Evaluacion
	
	SET @kCB_FEC_BAJ = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBaj)[1]', 'Date' ), '1899-12-30' ) --Fecha Baja	
	SET @kCB_FEC_BSS = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBss)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInt)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_REV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecRev)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_VAC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecVac)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_1 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec1)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_2 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec2)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_3 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec3)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_4 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec4)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_5 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec5)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_6 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec6)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_7 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec7)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_8 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec8)[1]', 'Date' ), '1899-12-30' )
	
	----------------------------------------- VALORES ADICIONALES DE TRESS ---------------------------	
	--DECLARE @kCB_G_LOG_1 as Booleano
	SET	@kCB_G_LOG_1_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog1)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_1_INT = 1 ) 
		Set @kCB_G_LOG_1 = 'S' 
	else
		Set @kCB_G_LOG_1 = 'N'
	--DECLARE @kCB_G_LOG_2 as Booleano
	SET	@kCB_G_LOG_2_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog2)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_2_INT = 1 ) 
		Set @kCB_G_LOG_2 = 'S' 
	else
		Set @kCB_G_LOG_2 = 'N' 
	--DECLARE @kCB_G_LOG_3 as Booleano
	SET	@kCB_G_LOG_3_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog3)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_3_INT = 1 ) 
		Set @kCB_G_LOG_3 = 'S' 
	else
		Set @kCB_G_LOG_3 = 'N' 
	--DECLARE @kCB_G_LOG_4 as Booleano
	SET	@kCB_G_LOG_4_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog4)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_4_INT = 1 ) 
		Set @kCB_G_LOG_4 = 'S' 
	else
		Set @kCB_G_LOG_4 = 'N' 
	--DECLARE @kCB_G_LOG_5 as Booleano
	SET	@kCB_G_LOG_5_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog5)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_5_INT = 1 ) 
		Set @kCB_G_LOG_5 = 'S' 
	else
		Set @kCB_G_LOG_5 = 'N' 
	--DECLARE @kCB_G_LOG_6 as Booleano
	SET	@kCB_G_LOG_6_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog6)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_6_INT = 1 ) 
		Set @kCB_G_LOG_6 = 'S' 
	else
		Set @kCB_G_LOG_6 = 'N' 
	--DECLARE @kCB_G_LOG_7 as Booleano
	SET	@kCB_G_LOG_7_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog7)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_7_INT = 1 ) 
		Set @kCB_G_LOG_7 = 'S' 
	else
		Set @kCB_G_LOG_7 = 'N' 
	--DECLARE @kCB_G_LOG_8 as Booleano
	SET	@kCB_G_LOG_8_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog8)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_8_INT = 1 ) 
		Set @kCB_G_LOG_8 = 'S' 
	else
		Set @kCB_G_LOG_8 = 'N' 

	SET @kCB_G_NUM_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum1)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum2)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum3)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum4)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum5)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum6)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum7)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum8)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum9)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum10)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum11)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum12)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum13)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum14)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM15 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum15)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM16 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum16)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM17 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum17)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM18 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum18)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_TAB_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab1)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab2)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab3)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab4)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab5)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab6)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab7)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab8)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab9)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab10)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab11)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab12)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab13)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab14)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TEX_1  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText1)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_2  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText2)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_3  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText3)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_4  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCLABE)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_5  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText5)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_6  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText6)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_7  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText7)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_8  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText8)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_9  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText9)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX10  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText10)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX11  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText11)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX12  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText12)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX13  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText13)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX14  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText14)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX15  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText15)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX16  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText16)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX17  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText17)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX18  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText18)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX19  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText19)[1]', 'Varchar(30)' ), '' ) -- as Descripcion		
	SET @kCB_G_TEX20  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText20)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX21  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText21)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX22  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText22)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX23  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText23)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX24  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText24)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	SET @kCB_INFCRED = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfCred)[1]', 'Varchar(30)' ), '' ) --Credito Infonavit 
	---DECLARE @kCB_INFMANT as Booleano 
	SET	@kCB_INFMANT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfMant)[1]', 'INT' ), 0)	
	if ( @kCB_INFMANT_INT = 1 ) 
		Set @kCB_INFMANT = 'S' 
	else
		Set @kCB_INFMANT = 'N'

	SET @kCB_INFTASA = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfTasa)[1]', 'Decimal(15,2)' ), 0.00 ) --Valor Descuento Infonavit 
	
	SET @kCB_LAST_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaLastEv)[1]', 'Date' ), '1899-12-30' ) --Fecha de ultima evaluacion
	
	
	SET @kCB_FEC_INC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInc)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_PER = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPer)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_NOMYEAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomYear)[1]', 'INT' ), 0)
	SET @kCB_NOMTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomTipo)[1]', 'INT' ), 0)
	

	SET @kCB_NEXT_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaNextEv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_NOMNUME = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0) -- as NominaNumero
	SET @kCB_SAL_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalInt)[1]', 'Decimal(15,2)' ), 0.00 ) -- as PesosDiario
	
	SET @kCB_INFTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0)  -- as Status
	SET @kCB_DER_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaDerFec)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PAG = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPag)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_PAGO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPago)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_DER_GOZ = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerGoz)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_GOZO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVGozo)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac


	SET @kCB_TIP_REV = coalesce( @Cargo.value('(//REGISTRO/@PersonaTipRev)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_MOT_BAJ = coalesce( @Cargo.value('(//REGISTRO/@PersonaMotBaj)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_FEC_SAL = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecSal)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_INI = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfIni)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_OLD = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfOld)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DescINFO
	SET @kCB_OLD_SAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldSal)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_OLD_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_PRE_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaPreInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_SAL_TOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalTot)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_TOT_GRA = coalesce( @Cargo.value('(//REGISTRO/@PersonaTotGra)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_FAC_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFacInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	SET @kCB_RANGO_S = coalesce( @Cargo.value('(//REGISTRO/@PersonaRangoS)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	
	SET @kCB_FEC_NIV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNiv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_PTO = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPto)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_AREA = coalesce( @Cargo.value('(//REGISTRO/@PersonaArea)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_FEC_TUR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecTur)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_KAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecKar)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_PENSION = coalesce(@Cargo.value('(//REGISTRO/@PersonaPension)[1]', 'INT' ), 0) -- as Status

	SET @kCB_CANDIDA = coalesce(@Cargo.value('(//REGISTRO/@PersonaCandida)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_ID_NUM = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdNum)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	
	SET @kCB_PASSWRD = coalesce( @Cargo.value('(//REGISTRO/@PersonaPasswrd)[1]', 'Varchar(30)' ), '' ) -- as Passwd
	SET @kCB_PLAZA = coalesce(@Cargo.value('(//REGISTRO/@PersonaPlaza)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_FEC_PLA = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPla)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PV = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPv)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_V_PRIMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPrima)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_SUB_CTA = coalesce( @Cargo.value('(//REGISTRO/@PersonaSubCta)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_NETO = coalesce( @Cargo.value('(//REGISTRO/@PersonaNeto)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	
	--SET @kCB_PORTAL as Booleano
	SET	@kCB_PORTAL_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaPortal)[1]', 'INT' ), 0)	
	if ( @kCB_PORTAL_INT = 1 ) 
		Set @kCB_PORTAL = 'S' 
	else
		Set @kCB_PORTAL = 'N' 
	--SET @kCB_DNN_OK as Booleano
	SET	@kCB_DNN_OK_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaDnnOk)[1]', 'INT' ), 0)	
	if ( @kCB_DNN_OK_INT = 1 ) 
		Set @kCB_DNN_OK = 'S' 
	else
		Set @kCB_DNN_OK = 'N' 

	SET @kCB_USRNAME = coalesce( @Cargo.value('(//REGISTRO/@PersonaUsrname)[1]', 'Varchar(40)' ), '' ) -- as DescLarga
	
	SET @kCB_FEC_NOM = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNom)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	--SET @kCB_RECONTR as Booleano
	SET	@kCB_RECONTR_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaRecontr)[1]', 'INT' ), 0)	
	if ( @kCB_RECONTR_INT = 1 ) 
		Set @kCB_RECONTR = 'S' 
	else
		Set @kCB_RECONTR = 'N'

		
	SET @kCB_FONACOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFonacot)[1]', 'Varchar(30)' ), '' ) -- as Descripcion

	--SET @kCB_EMPLEO as Booleano
	SET	@kCB_EMPLEO_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaEmpleo)[1]', 'INT' ), 0)	
	if ( @kCB_EMPLEO_INT = 1 ) 
		Set @kCB_EMPLEO = 'S' 
	else
		Set @kCB_EMPLEO = 'N'

	SET @kUS_CODIGO = coalesce(@Cargo.value('(//REGISTRO/@PersonaUsCodigo)[1]', 'INT' ), 0) -- as Usuario
	
	--SET @kCB_INFDISM as Booleano
	SET	@kCB_INFDISM_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfDism)[1]', 'INT' ), 0)	
	if ( @kCB_INFDISM_INT = 1 ) 
		Set @kCB_INFDISM = 'S' 
	else
		Set @kCB_INFDISM = 'N'

	--SET @kCB_INFACT as Booleano
	SET	@kCB_INFACT_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAct)[1]', 'INT' ), 0)	
	if ( @kCB_INFACT_INT = 1 ) 
		Set @kCB_INFACT = 'S' 
	else
		Set @kCB_INFACT = 'N'
	
	SET @kCB_INF_ANT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAnt)[1]', 'Date' ), '1899-12-30' ) -- as Fecha 
	
	SET @kCB_CTA_GAS = coalesce( @Cargo.value('(//REGISTRO/@PersonaCtaGas)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_ID_BIO = coalesce(@Cargo.value('(//REGISTRO/@PersonaIdBio)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_GP_COD = coalesce( @Cargo.value('(//REGISTRO/@PersonaGpCod)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	--SET @kCB_TIE_PEN as Booleano
	SET	@kCB_TIE_PEN_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaTiePen)[1]', 'INT' ), 0)	
	if ( @kCB_TIE_PEN_INT = 1 ) 
		Set @kCB_TIE_PEN = 'S' 
	else
		Set @kCB_TIE_PEN = 'N'

	SET @kCB_TSANGRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaTSangre)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_ALERGIA = coalesce( @Cargo.value('(//REGISTRO/@PersonaAlergia)[1]', 'Varchar(255)' ), '' ) -- as Formula

	--SET @kCB_BRG_ACT as Booleano
	SET	@kCB_BRG_ACT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgAct)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ACT_INT = 1 ) 
		Set @kCB_BRG_ACT = 'S' 
	else
		Set @kCB_BRG_ACT = 'N'

	SET @kCB_BRG_TIP = coalesce(@Cargo.value('(//REGISTRO/@PersonaBgrTip)[1]', 'INT' ), 0) -- as Status
	--SET @kCB_BRG_ROL as Booleano
	SET	@kCB_BRG_ROL_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgRol)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ROL_INT = 1 ) 
		Set @kCB_BRG_ROL = 'S' 
	else
		Set @kCB_BRG_ROL = 'N'

	--SET @kCB_BRG_JEF as Booleano
	SET	@kCB_BRG_JEF_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgJef)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_JEF_INT = 1 ) 
		Set @kCB_BRG_JEF = 'S' 
	else
		Set @kCB_BRG_JEF = 'N'

	--SET @kCB_BRG_CON as Booleano
	SET	@kCB_BRG_CON_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgCon)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_CON_INT = 1 ) 
		Set @kCB_BRG_CON = 'S' 
	else
		Set @kCB_BRG_CON = 'N'
	--SET @kCB_BRG_PRA as Booleano
	SET	@kCB_BRG_PRA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgPra)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_PRA_INT = 1 ) 
		Set @kCB_BRG_PRA = 'S' 
	else
		Set @kCB_BRG_PRA = 'N'

	SET @kCB_BRG_NOP = coalesce( @Cargo.value('(//REGISTRO/@PersonaBgrNop)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_BANCO = coalesce( @Cargo.value('(//REGISTRO/@PersonaBanco)[1]', 'Varchar(6)' ), @kCB_BANCO ) -- as Codigo
	
	
    if ( @ErrorID = 0 ) 
    begin 

	
	-- Si ya existe el empleado con los mismos nombres, rfc, fecha de Persona y correo  define el mismo numero de empleado 
	-- para que la validacion la realice el WFTress.dll 

	set @kCB_CODIGO = 0 
	set @kCB_SALARIO_BRUTO = 0 

	select @kCB_CODIGO = coalesce(@Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0)	

	--coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) 


	set @kCB_CODIGO = coalesce (@kCB_CODIGO ,  0 ) 

	-- Se obtiene el numero de empleado mayor + 1 
	if ( @kCB_CODIGO = 0 ) 
		SELECT @kCB_CODIGO = CB_CODIGO + 1  FROM dbo.ULTIMO_EMPLEADO

	
	 set @Xml =(	 

	   select MOV3= 'REINGRESO_EMPLEADO'		, 
			@EMPRESA as EMPRESA,  ( 
	   select * from  ( 
		 SELECT 		
		        @PersonaID  as IngresoID , 
				@kCB_CODIGO  as CB_CODIGO  , 
				--'S'  as CB_ACTIVO,		
				@kCB_AUTOSAL as CB_AUTOSAL , --Booleano
				@kCB_APE_MAT as CB_APE_MAT , --Descripcion
				@kCB_APE_PAT as CB_APE_PAT , --Descripcion
				@kCB_NOMBRES as CB_NOMBRES , --Descripcion
				--@kCB_BAN_ELE as CB_BAN_ELE , --Descripcion Se Quita por peticion de Roch
				@kCB_CALLE as CB_CALLE , --Observaciones
				@kCB_CARRERA as CB_CARRERA , --DescLarga
				@kCB_CHECA as CB_CHECA , --Booleano
				@kCB_CIUDAD as CB_CIUDAD , --Descripcion
				@kCB_CLASIFI as CB_CLASIFI , --Codigo
				@kCB_CODPOST as CB_CODPOST , --Referencia
				@kCB_COLONIA as CB_COLONIA , --Descripcion
				@kCB_CONTRAT as CB_CONTRAT , --Codigo1
				@kCB_CREDENC as CB_CREDENC , --Codigo1
				@kCB_CTA_VAL as CB_CTA_VAL , --Descripcion
				@kCB_CURP as CB_CURP , --Descripcion
				@kCB_E_MAIL as CB_E_MAIL , --Formula
				@kCB_ESTADO as CB_ESTADO , --Codigo2
				@kCB_ESTUDIO as CB_ESTUDIO , --Codigo2
				convert( varchar(20), @kCB_FEC_ANT , 112 )  as CB_FEC_ANT , --Fecha
				convert( varchar(20), @kCB_FEC_CON , 112 )  as CB_FEC_CON , --Fecha
				convert( varchar(20), @kCB_FEC_COV , 112 )  as CB_FEC_COV , --Fecha
				convert( varchar(20), @kCB_FEC_ING , 112 )  as CB_FEC_ING , --Fecha
				convert( varchar(20), @kCB_FEC_NAC , 112 )  as CB_FEC_NAC , --Fecha
				@kCB_LUG_NAC as CB_LUG_NAC , --Descripcion
				@kCB_MUNICIP as CB_MUNICIP , --Codigo
				@kCB_NACION as CB_NACION , --Descripcion
				@kCB_NIVEL0 as CB_NIVEL0 , --Codigo
				@kCB_NIVEL1 as CB_NIVEL1 , --Codigo
				@kCB_NIVEL2 as CB_NIVEL2 , --Codigo
				@kCB_NIVEL3 as CB_NIVEL3 , --Codigo
				@kCB_NIVEL4 as CB_NIVEL4 , --Codigo
				@kCB_NIVEL5 as CB_NIVEL5 , --Codigo
				@kCB_NIVEL6 as CB_NIVEL6 , --Codigo
				@kCB_NIVEL7 as CB_NIVEL7 , --Codigo
				@kCB_NIVEL8 as CB_NIVEL8 , --Codigo
				@kCB_NIVEL9 as CB_NIVEL9 , --Codigo
				@kCB_PATRON as CB_PATRON , --RegPatronal
				@kCB_PUESTO as CB_PUESTO , --Codigo
				@kCB_RFC as CB_RFC , --Descripcion
				@kCB_SALARIO as CB_SALARIO , --PesosDiario
				@kCB_SALARIO_BRUTO as CB_SALARIO_BRUTO , --PesosDiario Salario Bruto Mensual
				@kCB_SEGSOC as CB_SEGSOC , --Descripcion
				@kCB_SEXO as CB_SEXO , --Codigo1
				@kCB_TABLASS as CB_TABLASS , --Codigo1
				@kCB_TEL as CB_TEL , --Descripcion
				@kCB_TURNO as CB_TURNO , --Codigo
				@kCB_ZONA_GE as CB_ZONA_GE , --ZonaGeo										
				---Nuevos Campos Mapeados
				@kCB_EDO_CIV as CB_EDO_CIV , -- Codigo1
				convert( varchar(20), @kCB_FEC_RES , 112 )  as CB_FEC_RES , -- Fecha
				@kCB_EST_HOR as CB_EST_HOR , -- Descripcion
				@kCB_EST_HOY as CB_EST_HOY , -- Booleano
				@kCB_EVALUA as CB_EVALUA , -- DiasFrac
				@kCB_EXPERIE as CB_EXPERIE , -- Titulo
				convert( varchar(20), @kCB_FEC_BAJ , 112 )    as CB_FEC_BAJ , -- Fecha
				convert( varchar(20), @kCB_FEC_BSS , 112 )    as CB_FEC_BSS , -- Fecha
				convert( varchar(20), @kCB_FEC_INT , 112 )    as CB_FEC_INT , -- Fecha
				convert( varchar(20), @kCB_FEC_REV , 112 )    as CB_FEC_REV , -- Fecha
				convert( varchar(20), @kCB_FEC_VAC , 112 )    as CB_FEC_VAC , -- Fecha
				@kCB_G_FEC_1 as CB_G_FEC_1 , -- Fecha
				@kCB_G_FEC_2 as CB_G_FEC_2 , -- Fecha
				@kCB_G_FEC_3 as CB_G_FEC_3 , -- Fecha
				@kCB_G_FEC_4 as CB_G_FEC_4 , -- Fecha
				@kCB_G_FEC_5 as CB_G_FEC_5 , -- Fecha
				@kCB_G_FEC_6 as CB_G_FEC_6 , -- Fecha
				@kCB_G_FEC_7 as CB_G_FEC_7 , -- Fecha
				@kCB_G_FEC_8 as CB_G_FEC_8 , -- Fecha
				@kCB_G_LOG_1 as CB_G_LOG_1 , -- Booleano
				@kCB_G_LOG_2 as CB_G_LOG_2 , -- Booleano
				@kCB_G_LOG_3 as CB_G_LOG_3 , -- Booleano
				@kCB_G_LOG_4 as CB_G_LOG_4 , -- Booleano
				@kCB_G_LOG_5 as CB_G_LOG_5 , -- Booleano
				@kCB_G_LOG_6 as CB_G_LOG_6 , -- Booleano
				@kCB_G_LOG_7 as CB_G_LOG_7 , -- Booleano
				@kCB_G_LOG_8 as CB_G_LOG_8 , -- Booleano
				@kCB_G_NUM_1 as CB_G_NUM_1 , -- Pesos
				@kCB_G_NUM_2 as CB_G_NUM_2 , -- Pesos
				@kCB_G_NUM_3 as CB_G_NUM_3 , -- Pesos
				@kCB_G_NUM_4 as CB_G_NUM_4 , -- Pesos
				@kCB_G_NUM_5 as CB_G_NUM_5 , -- Pesos
				@kCB_G_NUM_6 as CB_G_NUM_6 , -- Pesos
				@kCB_G_NUM_7 as CB_G_NUM_7 , -- Pesos
				@kCB_G_NUM_8 as CB_G_NUM_8 , -- Pesos
				@kCB_G_NUM_9 as CB_G_NUM_9 , -- Pesos
				@kCB_G_NUM10 as CB_G_NUM10 , -- Pesos
				@kCB_G_NUM11 as CB_G_NUM11 , -- Pesos
				@kCB_G_NUM12 as CB_G_NUM12 , -- Pesos
				@kCB_G_NUM13 as CB_G_NUM13 , -- Pesos
				@kCB_G_NUM14 as CB_G_NUM14 , -- Pesos
				@kCB_G_NUM15 as CB_G_NUM15 , -- Pesos
				@kCB_G_NUM16 as CB_G_NUM16 , -- Pesos
				@kCB_G_NUM17 as CB_G_NUM17 , -- Pesos
				@kCB_G_NUM18 as CB_G_NUM18 , -- Pesos
				@kCB_G_TAB_1 as CB_G_TAB_1 , -- Codigo
				@kCB_G_TAB_2 as CB_G_TAB_2 , -- Codigo
				@kCB_G_TAB_3 as CB_G_TAB_3 , -- Codigo
				@kCB_G_TAB_4 as CB_G_TAB_4 , -- Codigo
				@kCB_G_TAB_5 as CB_G_TAB_5 , -- Codigo
				@kCB_G_TAB_6 as CB_G_TAB_6 , -- Codigo
				@kCB_G_TAB_7 as CB_G_TAB_7 , -- Codigo
				@kCB_G_TAB_8 as CB_G_TAB_8 , -- Codigo
				@kCB_G_TAB_9 as CB_G_TAB_9 , -- Codigo
				@kCB_G_TAB10 as CB_G_TAB10 , -- Codigo
				@kCB_G_TAB11 as CB_G_TAB11 , -- Codigo
				@kCB_G_TAB12 as CB_G_TAB12 , -- Codigo
				@kCB_G_TAB13 as CB_G_TAB13 , -- Codigo
				@kCB_G_TAB14 as CB_G_TAB14 , -- Codigo
				@kCB_G_TEX_1 as CB_G_TEX_1 , -- Descripcion
				@kCB_G_TEX_2 as CB_G_TEX_2 , -- Descripcion
				@kCB_G_TEX_3 as CB_G_TEX_3 , -- Descripcion
				@kCB_G_TEX_4 as CB_G_TEX_4 , -- Descripcion
				@kCB_G_TEX_5 as CB_G_TEX_5 , -- Descripcion
				@kCB_G_TEX_6 as CB_G_TEX_6 , -- Descripcion
				@kCB_G_TEX_7 as CB_G_TEX_7 , -- Descripcion
				@kCB_G_TEX_8 as CB_G_TEX_8 , -- Descripcion
				@kCB_G_TEX_9 as CB_G_TEX_9 , -- Descripcion
				@kCB_G_TEX10 as CB_G_TEX10 , -- Descripcion
				@kCB_G_TEX11 as CB_G_TEX11 , -- Descripcion
				@kCB_G_TEX12 as CB_G_TEX12 , -- Descripcion
				@kCB_G_TEX13 as CB_G_TEX13 , -- Descripcion
				@kCB_G_TEX14 as CB_G_TEX14 , -- Descripcion
				@kCB_G_TEX15 as CB_G_TEX15 , -- Descripcion
				@kCB_G_TEX16 as CB_G_TEX16 , -- Descripcion
				@kCB_G_TEX17 as CB_G_TEX17 , -- Descripcion
				@kCB_G_TEX18 as CB_G_TEX18 , -- Descripcion
				@kCB_G_TEX19 as CB_G_TEX19 , -- Descripcion
				@kCB_HABLA as CB_HABLA , -- Booleano
				@kCB_IDIOMA as CB_IDIOMA , -- Descripcion
				@kCB_INFCRED as CB_INFCRED , -- Descripcion
				@kCB_INFMANT as CB_INFMANT , -- Booleano
				@kCB_INFTASA as CB_INFTASA , -- DescINFO
				@kCB_LA_MAT as CB_LA_MAT , -- Descripcion
				@kCB_LAST_EV as CB_LAST_EV , -- Fecha
				@kCB_MAQUINA as CB_MAQUINA , -- Titulo
				@kCB_MED_TRA as CB_MED_TRA , -- Codigo1
				@kCB_PASAPOR as CB_PASAPOR , -- Booleano				
				@kCB_VIVECON as CB_VIVECON , -- Codigo1
				@kCB_VIVEEN as CB_VIVEEN , -- Codigo2
				@kCB_ZONA as CB_ZONA , -- Referencia
				@kCB_TIP_REV as CB_TIP_REV , -- Codigo				
				@kCB_INF_INI as CB_INF_INI , -- Fecha
				@kCB_INF_OLD as CB_INF_OLD , -- DescINFO
				@kCB_OLD_SAL as CB_OLD_SAL , -- PesosDiario
				@kCB_OLD_INT as CB_OLD_INT , -- PesosDiario
				@kCB_PRE_INT as CB_PRE_INT , -- PesosDiario
				@kCB_PER_VAR as CB_PER_VAR , -- PesosDiario
				@kCB_SAL_TOT as CB_SAL_TOT , -- PesosDiario
				@kCB_TOT_GRA as CB_TOT_GRA , -- PesosDiario
				@kCB_FAC_INT as CB_FAC_INT , -- Tasa
				@kCB_RANGO_S as CB_RANGO_S , -- Tasa
				@kCB_CLINICA as CB_CLINICA , -- Codigo3																
				@kCB_CANDIDA as CB_CANDIDA , -- FolioGrande
				@kCB_ID_NUM as CB_ID_NUM , -- Descripcion
				@kCB_ENT_NAC as CB_ENT_NAC , -- Codigo2
				@kCB_COD_COL as CB_COD_COL , -- Codigo								
				@kCB_DER_PV as CB_DER_PV , -- DiasFrac
				@kCB_V_PRIMA as CB_V_PRIMA , -- DiasFrac
				@kCB_SUB_CTA as CB_SUB_CTA , -- Descripcion
				@kCB_NETO as CB_NETO , -- Pesos
				@kCB_NOMINA as CB_NOMINA , -- NominaTipo
				@kCB_RECONTR as CB_RECONTR , -- Booleano
				@kCB_DISCAPA as CB_DISCAPA , -- Booleano
				@kCB_INDIGE as CB_INDIGE , -- Booleano
				@kCB_FONACOT as CB_FONACOT , -- Descripcion
				@kCB_EMPLEO as CB_EMPLEO , -- Booleano								
				@kCB_G_TEX20 as CB_G_TEX20 , -- Descripcion
				@kCB_G_TEX21 as CB_G_TEX21 , -- Descripcion
				@kCB_G_TEX22 as CB_G_TEX22 , -- Descripcion
				@kCB_G_TEX23 as CB_G_TEX23 , -- Descripcion
				@kCB_G_TEX24 as CB_G_TEX24 , -- Descripcion
				@kCB_INFDISM as CB_INFDISM , -- Booleano
				@kCB_INFACT as CB_INFACT , -- Booleano
				@kCB_NUM_EXT as CB_NUM_EXT , -- NombreCampo
				@kCB_NUM_INT as CB_NUM_INT , -- NombreCampo
				@kCB_INF_ANT as CB_INF_ANT , -- Fecha
				@kCB_TDISCAP as CB_TDISCAP , -- Status
				@kCB_ESCUELA as CB_ESCUELA , -- DescLarga
				@kCB_TESCUEL as CB_TESCUEL , -- Status
				@kCB_TITULO as CB_TITULO , -- Status
				@kCB_YTITULO as CB_YTITULO , -- Anio
				@kCB_CTA_GAS as CB_CTA_GAS , -- Descripcion
				@kCB_ID_BIO as CB_ID_BIO , -- FolioGrande
				@kCB_GP_COD as CB_GP_COD , -- Codigo				
				@kCB_TSANGRE as CB_TSANGRE , -- Descripcion
				@kCB_ALERGIA as CB_ALERGIA , -- Formula
				@kCB_BRG_ACT as CB_BRG_ACT , -- Booleano
				@kCB_BRG_TIP as CB_BRG_TIP , -- Status
				@kCB_BRG_ROL as CB_BRG_ROL , -- Booleano
				@kCB_BRG_JEF as CB_BRG_JEF , -- Booleano
				@kCB_BRG_CON as CB_BRG_CON , -- Booleano
				@kCB_BRG_PRA as CB_BRG_PRA , -- Booleano
				@kCB_BRG_NOP as CB_BRG_NOP , -- Descripcion
				@kCB_BANCO as CB_BANCO ,	-- Codigo 
				@kCB_REGIMEN as CB_REGIMEN -- Status

		   )  REGISTRO    FOR XML AUTO, TYPE 
		   )   REGISTROS  FOR XML PATH('DATOS'), TYPE  
		   )  
		 	
		SET @ErrorID = 0 
		set @Error = '' 
	end
	
	-- Solo pruebas
	UPDATE T_Wftransacciones set TransCargoWFTress = @Xml where TransID = @transID 

	return @ErrorID
end
go 

CREATE PROCEDURE WFTransaccion_GetXMLPermisos( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT
	DECLARE @NOMBRE		Formula 
	
	DECLARE @PermisoInicial Fecha 
	DECLARE @PermisoFinal   Fecha 
	DECLARE @PermisoDias    Pesos
	DECLARE @TipoPermisoClase Status 
	DECLARE @PermisoReferen Descripcion 
	DECLARE @PermisoDescrip	Titulo 
	DECLARE @TipoPermisoCodigo Codigo 
	DECLARE @XMLTress  Varchar(max) 

	declare @ClienteID FolioGrande 
	set @FileID = 0 
	
	select  
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			CB_CODIGO	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
			NOMBRE		= coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),	

			INICIO   = coalesce(t.c.value('(@PermisoInicial)', 'DateTime' ), '1899-12-30' ) ,
			RETORNO  = coalesce(t.c.value('(@PermisoFinal)', 'DateTime' ), '1899-12-30' ) ,
			DIAS		= coalesce(t.c.value('(@PermisoDias)', 'DECIMAL(15,2)' ), 0.00),
			CLASE	= coalesce(t.c.value('(@TipoPermisoClase)', 'INT' ), 0),
			TIPO	= coalesce(t.c.value('(@TipoPermisoCodigo)', 'Varchar(256)' ), '' ) ,
			REFERENCIA  = coalesce(t.c.value('(@PermisoReferen)', 'Varchar(255)' ), '' ), 
			OBSERVACIONES  = coalesce(t.c.value('(@PermisoDescrip)', 'Varchar(255)' ), '' ), 
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)	
		
	 into #PERMI_TEMP 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c) 
		declare @XML_DatosMult XML 
		set @XML_DatosMult = ( 		
			select 					
				EMPRESA,
				'PERM' as MOV3,
				CB_CODIGO as EMPLEADO,
				convert( varchar(20), INICIO , 112 )   as INICIO, 
				convert( varchar(20), RETORNO , 112 )   as RETORNO, 	
				DIAS, 
				CLASE, 						
				TIPO, 
				REFERENCIA, 
				OBSERVACIONES				 
			from #PERMI_TEMP PT
			--group by PT.EMPRESA, PT.CB_CODIGO, INICIO, RETORNO 
			order by INICIO 
			FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE	
			)	
		declare @XMlDatosMult_varchar varchar(max)			
		set @XMlDatosMult_varchar = cast( @XML_DatosMult as varchar(max))	

		set @XmlTress = @XMlDatosMult_varchar

	set @Xml = @XMLTress 
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #PERMI_TEMP

end
go 


CREATE PROCEDURE WFTransaccion_GetXMLAutorizarExtras( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
 	
set nocount on 
DECLARE @ErrorID	INT
declare  @Cargo XML 

	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	

DECLARE @XMLTress  Varchar(max) 	

	select  
			ClienteID	= coalesce(t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EMPLEADO	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
			FECHAAUTO   = coalesce(r.r.value('(@EmpCalFecha)', 'DateTime' ), t.c.value('(@EmpCalFecha)', 'DateTime' ), '1899-12-30' ) ,
			NOMBRE		= coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),	
			HORAS		= coalesce(t.c.value('(@AutCalHoras)', 'DECIMAL(15,2)' ), 0.00),
			MOTIVO = coalesce(  r.r.value('(@MotivoCalCodigo)', 'Varchar(256)' ), '' ),	
			TIPOAUTO = coalesce(  r.r.value('(@MotivoCalClase)', 'INT' ), 0 ),	
			MOTIVOPREVIA = coalesce(  r.r.value('(@MotivoCalPrevia)', 'INT' ), 1),	
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)	
		
	 into #AUTORIZACIONES
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c), 
	 @Cargo.nodes('//REGISTROS') r(r)

  declare @XML_Mult XML 

  -- SUSTITUIR, SUMAR, DEJAR_ANTERIOR 

  set @XMLTress = ( 
		   select 
			 
			 'AUTO3MULT' as MOV3,
			 1 as US_CODIGO, 
			 EMPRESA, 
			 ACCION_SI_EXISTE = 
					case MOTIVOPREVIA 
						when 1 then 'SUSTITUIR'
						when 2 then 'SUMAR' 
						when 3 then 'DEJAR_ANTERIOR' 
					else 
						'SUSTITUIR' 
					end, 
			 SUM( HORAS ) as TOTALHORAS, 
			 COUNT( EMPLEADO )  as TOTALREG 			  
			from #AUTORIZACIONES 
			group by EMPRESA,MOTIVOPREVIA
		   FOR XML PATH('')
		   
		   )	
	
		 set @XML_Mult = ( 		
			select  
				EMPRESA, 
				EMPLEADO, 
				MOV3 = 'AUTO3MULT', 
				TIPOSOLICITUD='AUTORIZACION', 
				ACCION_SI_EXISTE =
					case MOTIVOPREVIA 
						when 1 then 'SUSTITUIR'
						when 2 then 'SUMAR' 
						when 3 then 'DEJAR_ANTERIOR' 
					else 
						'SUSTITUIR' 
					end, 
				FECHAAUTO = convert( varchar(20), FECHAAUTO , 112 ) , 
				TIPOAUTO,
				HORAS,
				MOTIVO,
				EMPRESA_TXT = EMPRESA  

			from #AUTORIZACIONES 			
			FOR XML PATH('INFO'), ROOT('EMPLEADOS') 
		 )
		 		 
		declare @XMlMult_varchar varchar(max)
			
		set @XMlMult_varchar = cast( @XMl_Mult as varchar(max))	
		set @XmlTress = '<DATOS>' + @XmlTress + @XMlMult_varchar + '</DATOS>' 
		set @XML = @XmlTress
	 
	drop table #AUTORIZACIONES 

	set @FileID = 0 
	SET @ErrorID = 0 
	set @Error = '' 

end
go 

CREATE PROCEDURE WFTransaccion_GetXMLExcepciones( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 		

	select * into #EXCP_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
			PersonaName		= coalesce(  t.c.value('(@PersonaName)', 'Varchar(256)' ), '' ),	
			PeriodoYear	= coalesce(t.c.value('(@PeriodoYear)', 'INT' ), 0),
			TipoNominaCodigo	= coalesce(t.c.value('(@TipoNominaCodigo)', 'INT' ), 0),
			PeriodoNumero	= coalesce(t.c.value('(@PeriodoNumero)', 'INT' ), 0),
			ConceptoNumero	= coalesce(t.c.value('(@ConceptoID)', 'INT' ), 0),
			TipoOperacion	= coalesce(t.c.value('(@TipoOperacion)', 'INT' ), 0),
			RevisarIncapacidad= coalesce(t.c.value('(@RevisarIncapacidad)', 'INT'  ), 0 ),			
			MovimientoMonto	= coalesce(t.c.value('(@ExcepcionMonto)', 'DECIMAL(15,2)' ), 0.00),
			MovimientoReferencia	= coalesce(t.c.value('(@ExcepcionObserva)', 'Varchar(256)' ), '' ) ,						
			VA_COMENTA  = coalesce(t.c.value('(@VacacionDescrip)', 'Varchar(255)' ), '' ), 
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0), 
			FileID2	= coalesce(t.c.value('(@FileID2)', 'INT' ), 0)	
			 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 
	
	 if ( select count(*) from MOVIMIEN MO
	 join  #EXCP_TEMP ET on MO.CO_NUMERO = ET.ConceptoNumero 
	 and MO.CB_CODIGO = ET.EmpNumero and MO.PE_YEAR = ET.PeriodoYear and MO.PE_TIPO = ET.TipoNominaCodigo and MO.PE_NUMERO = ET.PeriodoNumero
	 where MO.US_CODIGO > 0 ) > 0 
	 begin 
		SET @ErrorID = 500; 
		SET @Error = 'Ya existen montos registrados previamente en el sistema para los empleados solicitados.' 
	 end  
	 else
	 begin 
		 set @Xml = ( 
			 select 		
				MOV3='EXNOMINA', 
				EMPRESA, 
 			( select 
				 EMPRESA, 
				 TipoNominaCodigo,PeriodoYear,PeriodoNumero,TipoOperacion, 
					case 
						when RevisarIncapacidad= 0 then 'False' 
						when RevisarIncapacidad= 1 then 'True' 
					else 
						'False' 
					end RevisarIncapacidad, 
				 count(*) Cuantos, 

					( SELECT ClienteId, ConceptoNumero, MovimientoMonto, MovimientoReferencia, EmpNumero, PersonaName FROM #EXCP_TEMP REGISTRO 
					     where REGISTROS.EMPRESA = REGISTRO.EMPRESA and REGISTROS.PeriodoNumero = REGISTRO.PeriodoNumero and
						       REGISTROS.PeriodoYear = REGISTRO.PeriodoYear and REGISTROS.TipoNominaCodigo = REGISTRO.TipoNominaCodigo   FOR XML AUTO, TYPE) 
			 from #EXCP_TEMP REGISTROS 
			 where DATOS.EMPRESA = REGISTROS.EMPRESA and DATOS.TipoOperacion = REGISTROS.TipoOperacion and DATOS.RevisarIncapacidad = REGISTROS.RevisarIncapacidad and 
				   DATOS.TipoNominaCodigo = REGISTROS.TipoNominaCodigo and  DATOS.PeriodoYear = REGISTROS.PeriodoYear and DATOS.PeriodoNumero = REGISTROS.PeriodoNumero 
				   			
			group by EMPRESA,TipoNominaCodigo,PeriodoYear,PeriodoNumero,TipoOperacion,RevisarIncapacidad 
			 FOR XML AUTO, TYPE 
			 ) 

			 from #EXCP_TEMP DATOS 
			group by EMPRESA,TipoNominaCodigo,PeriodoYear,PeriodoNumero,TipoOperacion,RevisarIncapacidad 
			 FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
		) 
					
		SET @ErrorID = 0 
		set @Error = '' 

	end 
	
	drop table #EXCP_TEMP 	
	
end
GO 

CREATE PROCEDURE WFTransaccion_GetXMLDocumentos( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID  VARCHAR(max) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT

	declare @ClienteID FolioGrande 
	set @FileID = 0 

	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  		
			/*ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 			*/
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	set @Xml = '' 	
	SET @ErrorID = 0 
	set @Error = '' 


end
GO 

CREATE  PROCEDURE WFTransaccion_GetXMLDatosEmpleado ( 
       @TransID     INT,
       @Xml         XML OUTPUT ,
       @FileID             INT OUTPUT, 
       @Error       VARCHAR( 255 ) OUTPUT)
AS
begin 
       DECLARE @ErrorID    INT
       DECLARE @Cargo      XML
       
       SELECT @Cargo = TransCargo
       FROM   T_WFTransacciones
       WHERE  TransID = @TransID  
       
       DECLARE @EMPRESA  varchar(10) 
       DECLARE @CB_CODIGO  INT
       DECLARE @NOMBRE            Formula 


       declare @ClienteID FolioGrande 
       set @FileID = 0             

       select * into #EXCP_TEMP 
       from ( 
       select              
                    ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
                    EMPRESA      = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
                    EmpNumero    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,
                    Campo        = coalesce(  t.c.value('(@Campo)', 'Varchar(256)' ), NULL ), 
                    valor        = coalesce(  t.c.value('(@Valor)', 'Varchar(256)' ),  NULL )                                                                   
        from 
        @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
       ) CargoTabla
       

       set @Xml = ( 
              select             
                    MOV3='MODCOLABORA', 
                    EMPRESA, 
             ( select 
                     EMPRESA,
                    count(*) Cuantos, 

                           ( SELECT ClienteId, EmpNumero, Campo, Valor  FROM #EXCP_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA  FOR XML AUTO, TYPE) 
              from #EXCP_TEMP REGISTROS 
              where DATOS.EMPRESA = REGISTROS.EMPRESA                
                    
             group by EMPRESA
             FOR XML AUTO, TYPE 
              ) 

             from #EXCP_TEMP DATOS 
             group by EMPRESA
             FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
       ) 
                           
       
       SET @ErrorID = 0 
       set @Error = '' 
       
       drop table #EXCP_TEMP      
       
end
GO 

CREATE PROCEDURE WFTransaccion_GetXMLInfonavit( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID Varchar(max) output )
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 
		

	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  					
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla  where FileID > 0 
	 


	select * into #EXCP_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			InfoTipo	= coalesce(t.c.value('(@InfoTipo)', 'INT' ), 0),		
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,			
			InfoFecha   = coalesce(t.c.value('(@InfoFecha)', 'Datetime' ), '1899-12-30' ) ,
			InfoNumero	= coalesce(t.c.value('(@InfoNumero)', 'Varchar(256)' ), '' ) ,		
			InfoClase	= coalesce(t.c.value('(@InfoClase)', 'INT' ), 0),		
			InfoValor	= coalesce(t.c.value('(@InfoValor)', 'DECIMAL(15,4)' ), 0.0000)			
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	 set @Xml = ( 
		 select 		
			MOV3= 
			case InfoTipo 
				when 0 then 'INFONAVIT_AGREGAR'
				when 1 then 'INFONAVIT_MODIFICAR'
				when 2 then 'INFONAVIT_SUSPENDER'
				when 3 then 'INFONAVIT_REINICIAR'
			else 
				'INFONAVIT_AGREGAR' 
			end
				, 
			EMPRESA, 
 		( select 
			 EMPRESA, 
			 InfoTipo, 				
			 count(*) Cuantos, 

				( SELECT ClienteId,  EmpNumero, FECHA= convert( varchar(20), InfoFecha , 112 ), CB_INF_INI = convert( varchar(20), InfoFecha , 112 ), CB_INF_ANT = convert( varchar(20), InfoFecha , 112 ) , CB_INFCRED = InfoNumero, CB_INFTIPO=InfoClase, CB_INFTASA=InfoValor FROM #EXCP_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA and REGISTROS.InfoTipo = REGISTRO.InfoTipo  FOR XML AUTO, TYPE) 
		 from #EXCP_TEMP REGISTROS 
		 where DATOS.EMPRESA = REGISTROS.EMPRESA and DATOS.InfoTipo = REGISTROS.InfoTipo
			  
			
		group by EMPRESA,InfoTipo
		 FOR XML AUTO, TYPE 
		 ) 

		 from #EXCP_TEMP DATOS 
		group by EMPRESA,InfoTipo
		 FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
	) 
				
	
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #EXCP_TEMP 	
	
end
GO 

CREATE PROCEDURE WFTransaccion_GetXMLIncapacidades( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID Varchar(max) output )
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 		

	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  					
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	select * into #INCAPAS_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 			
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,			
			IncapacidadInicial   = convert( varchar(20),coalesce(t.c.value('(@IncapaInicial)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadFechaRH   =  convert( varchar(20),coalesce(t.c.value('(@IncapaFechaRH)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadSUAInicial   =  convert( varchar(20),coalesce(t.c.value('(@IncapaSUAInicial)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadSUAFinal   =  convert( varchar(20),coalesce(t.c.value('(@IncapaSUAFinal)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			IncapacidadDias	= coalesce(t.c.value('(@IncapaDias)', 'INT' ), 0),		
			IncapacidadNumero	= coalesce(t.c.value('(@IncapaNumero)', 'Varchar(256)' ), '' ) ,		
			IncapacidadTipo	=  coalesce(t.c.value('(@TipoIncapaCodigo)', 'Varchar(10)' ), '' ) ,		
			IncapacidadObserva	=  coalesce(t.c.value('(@IncapaDescrip)', 'Varchar(256)' ), '' ) ,	
			IncapacidadClaseMotivo = coalesce(t.c.value('(@IncapaControl)', 'INT' ), 0),		
			IncapacidadTipoFin = coalesce(t.c.value('(@IncapaTipoFin)', 'INT' ), 0),	
			IncapacidadPermanente  = coalesce(t.c.value('(@IncapaPermanente)', 'Decimal(15,4)' ), 0.0000),
			IncapacidadDiasSubsidiar =  coalesce(t.c.value('(@IncapaDiasSubsidiar)', 'Decimal(15,2)' ), 0.00),		
			IncapacidadAjustarFecha = coalesce(t.c.value('(@IncapaAjustarFecha)', 'INT' ), 0),	
			-- PeriodoYear="2015" TipoNominaCodigo="1" PeriodoNumero="54" 	
			PeriodoYear = coalesce(t.c.value('(@PeriodoYear)', 'INT' ), 0),
			TipoNominaCodigo = coalesce(t.c.value('(@TipoNominaCodigo)', 'INT' ), 0),
			PeriodoNumero = coalesce(t.c.value('(@PeriodoNumero)', 'INT' ), 0)
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	 set @Xml = ( 
		 select 		
			MOV3= 'INCAPACIDAD_AGREGAR',			
			EMPRESA, 
 		( select 
			 EMPRESA, 			 
			 count(*) Cuantos, 

				( SELECT ClienteId,  EmpNumero, IncapacidadInicial,
						case IncapacidadFechaRH
							when '18991230' then IncapacidadInicial 
							else IncapacidadFechaRH 
						end IncapacidadFechaRH, 						
						case IncapacidadSUAInicial
							when '18991230' then IncapacidadInicial 
							else IncapacidadSUAInicial 
						end IncapacidadSUAInicial, 
						case IncapacidadSUAFinal
							when '18991230' then convert( varchar(20), cast(  dateadd( day, IncapacidadDias, IncapacidadInicial ) as Datetime )  , 112 ) 
							else IncapacidadSUAFinal 
						end IncapacidadSUAFinal, 						
						IncapacidadDias,
						IncapacidadNumero,
						IncapacidadTipo,
						IncapacidadObserva,
						IncapacidadClaseMotivo,												
						IncapacidadTipoFin,
						IncapacidadPermanente, 
						IncapacidadDiasSubsidiar,
						IncapacidadAjustarFecha,
						PeriodoYear,
						TipoNominaCodigo,
						PeriodoNumero				
						FROM #INCAPAS_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA   FOR XML AUTO, TYPE) 
		 from #INCAPAS_TEMP REGISTROS 
		 where DATOS.EMPRESA = REGISTROS.EMPRESA 
			  
			
		group by EMPRESA
		 FOR XML AUTO, TYPE 
		 ) 

		 from #INCAPAS_TEMP DATOS 
		group by EMPRESA
		 FOR XML PATH('DATOS'), ROOT('MULTIPLE'), TYPE 
	) 
				
	
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #INCAPAS_TEMP 	
	
end
GO

CREATE PROCEDURE WFTransaccion_GetXMLPrestamos( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT, 
	@MultiplesFileID Varchar(max) output )
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	declare @ClienteID FolioGrande 
	set @FileID = 0 
		


	set @MultiplesFileID = '' 
	select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
	from ( 
	select  					
			FileID	= coalesce(t.c.value('(@FileID)', 'INT' ), 0)						 
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	select * into #PREST_TEMP 
	from ( 
	select  		
			ClienteID	= coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			PersonaID	= coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0), 			
			EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			EmpNumero	= coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,	
			TipoDesCodigo	= coalesce(t.c.value('(@TipoDesCodigo)', 'Varchar(256)' ), '' ) ,	
			DescuentoReferencia	= coalesce(t.c.value('(@DescuentoReferencia)', 'Varchar(256)' ), '' ) ,	
			DescuentoInicial   = convert( varchar(20),coalesce(t.c.value('(@DescuentoInicial)', 'Datetime' ), '1899-12-30' ) , 112 ) , 
			DescuentoMonto =  coalesce(t.c.value('(@DescuentoMonto)', 'Decimal(15,2)' ), 0.00),
			DescuentoFormula	= coalesce(t.c.value('(@DescuentoFormula)', 'Varchar(256)' ), '' ) ,
			DescuentoSubCta	= coalesce(t.c.value('(@DescuentoSubCta)', 'Varchar(256)' ), '' ) ,
			DescuentoObserva	= coalesce(t.c.value('(@DescuentoObserva)', 'Varchar(256)' ), '' ) ,
			DescuentoSolicita =  coalesce(t.c.value('(@DescuentoSolicita)', 'Decimal(15,2)' ), 0.00),
			DescuentoInteres =  coalesce(t.c.value('(@DescuentoInteres)', 'Decimal(15,2)' ), 0.00),
			DescuentoMeses =  coalesce(t.c.value('(@DescuentoMeses)', 'Decimal(15,2)' ), 0.00),
			DescuentoPagPer =  coalesce(t.c.value('(@DescuentoPagPer)', 'Decimal(15,2)' ), 0.00),
			DescuentoPagos =  coalesce(t.c.value('(@DescuentoPagos)', 'Decimal(15,2)' ), 0.00),
			DescuentoTasa =  coalesce(t.c.value('(@DescuentoTasa)', 'Decimal(15,2)' ), 0.00)
	 from 
	 @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
	 ) CargoTabla
	 

	 set @Xml = ( 
		 select 		
			MOV3= 'PRESTAMO_AGREGAR',			
			EMPRESA, 
 		( select 
			 EMPRESA, 			 
			 count(*) Cuantos, 

				( SELECT ClienteId, 
						PersonaID, 
						EmpNumero, 
						TipoDesCodigo,
						DescuentoReferencia,
						DescuentoInicial,
						DescuentoMonto,												
						DescuentoFormula,
						DescuentoSubCta,
						DescuentoObserva,
						DescuentoSolicita,
						DescuentoInteres,
						DescuentoMeses,
						DescuentoPagPer,
						DescuentoPagos,
						DescuentoTasa						
						FROM #PREST_TEMP REGISTRO where REGISTROS.EMPRESA = REGISTRO.EMPRESA   FOR XML AUTO, TYPE) 
		 from #PREST_TEMP REGISTROS 
		 where DATOS.EMPRESA = REGISTROS.EMPRESA 
			  
			
		group by EMPRESA
		 FOR XML AUTO, TYPE 
		 ) 

		 from #PREST_TEMP DATOS 
		group by EMPRESA
		 FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
	) 
				
	
	SET @ErrorID = 0 
	set @Error = '' 
	
	drop table #PREST_TEMP 	
	
end
GO 

CREATE PROCEDURE WFTransaccion_GetXMLRespuesta(
       @TransID            INT,
       @Xml                XML,
       @HayXML                    bit OUTPUT, 
       @XmlRespuesta XML OUTPUT , 
       @Error       VARCHAR( 255 ) OUTPUT)
AS
begin 

       DECLARE      @VerboID     INT
       DECLARE      @ObjetoID    INT
       DECLARE @ErrorID    INT
       DECLARE @XmlCargo   XML 
       
       --SET ARITHABORT OFF
       
       SELECT @VerboID     = VerboID,  @ObjetoID = ObjetoID, @XmlCargo = TransCargo
       FROM   T_WFTransacciones
       WHERE  TransID = @TransID
       
       set @HayXML = 0 

	   update T_WFTransacciones set TransCargoRespuestaTress = @Xml where TransID = @TransID 


       IF ( @VerboID = 100198) and ( @ObjetoID = 100033)  
       begin 
             set @HayXML = 1 
             set @XmlRespuesta = @XmlCargo  

             --Parametro="8719" Clave1="No Maestro C1" Clave2="No Maestro C2"
             declare @i int 
             declare @iMax int          
             select ROW_NUMBER() over (order by Numero) as I , Numero, Parametro, Clave1, Clave2  into #Autorizados 
             from ( 
             select                                         
                           Numero = coalesce(  t.c.value('(@Numero)', 'INT' ), 0),      
                           Parametro  =  coalesce(  t.c.value('(@Parametro)', 'INT' ), 0),           
                           Clave1  =  coalesce(  t.c.value('(@Clave1)', 'varchar(255)' ), ''),              
                           Clave2  =  coalesce(  t.c.value('(@Clave2)', 'varchar(255)' ), '')
             from 
              @Xml.nodes('//Respuesta/Sentinel') t(c)
             ) XmlTabla

             select @iMax = count(*) from #Autorizados
             declare @Parametro int 
             declare @Numero int 
             declare @Clave1 varchar(255) 
             declare @Clave2 varchar(255) 
             set @i= 1 
             while ( @i <= @iMax ) 
             begin 

                    select @Parametro = Parametro , @Numero = Numero ,  @Clave1 = Clave1 , @Clave2 = Clave2 from #Autorizados where i =@i                  
                    SET @XmlRespuesta.modify( 'insert attribute SentinelParametroNew {sql:variable("@Parametro") } into (/REGISTROS/REGISTRO[@SentinelSerie=sql:variable("@Numero")])[1]'  )
                    SET @XmlRespuesta.modify( 'insert attribute SentinelClave1New {sql:variable("@Clave1") } into (/REGISTROS/REGISTRO[@SentinelSerie=sql:variable("@Numero")])[1]'  )
                    SET @XmlRespuesta.modify( 'insert attribute SentinelClave2New {sql:variable("@Clave2") } into (/REGISTROS/REGISTRO[@SentinelSerie=sql:variable("@Numero")])[1]'  ) 
                    set @i= @i +1 
             end 
       
             drop table #Autorizados
       end
	   else
	   IF ( @VerboID = 1281 ) and ( @ObjetoID = 1068 )  -- Alta de empleado 
       begin 
             set @HayXML = 1 
             set @XmlRespuesta = @XmlCargo  
			
             declare @CB_CODIGO int 
			 declare @EmpNumero int 
			 
             select                                                                    
                 @CB_CODIGO  =  coalesce(  t.c.value('(@CB_CODIGO)', 'INT' ), 0)                                      
             from 
              @Xml.nodes('//DATOS/REGISTROS/REGISTRO') t(c)

			 set @EmpNumero = -1 
			 select                                                                    
                 @EmpNumero  = coalesce( t.c.value('(@EmpNumero)', 'INT' ), -1 )                                     
             from 
              @XmlRespuesta.nodes('//REGISTROS/REGISTRO') t(c)
             			            			 
                                                       
             -- Se modifica el cargo para 
             if ( @EmpNumero < 0 )                    
                 SET @XmlRespuesta.modify( 'insert attribute EmpNumero {sql:variable("@CB_CODIGO") } into (//REGISTROS/REGISTRO)[1]'  )                      
             else
              SET @XmlRespuesta.modify( 'replace value of (//REGISTROS/REGISTRO/@EmpNumero)[1]  with sql:variable("@CB_CODIGO") '  )                                    

       end
       
	   update T_WFTransacciones set TransCargoRespuesta = @XmlRespuesta where TransID = @TransID 
	   
       
       return 0 
end
GO 

CREATE PROCEDURE WFTransaccion_GetXMLPuestoClasificacion(
       @TransID     INT,
       @Xml         XML OUTPUT ,
       @FileID             INT OUTPUT,
       @Error       VARCHAR( 255 ) OUTPUT,
       @MultiplesFileID Varchar(max) output )
AS
begin
       DECLARE @ErrorID    INT
       DECLARE @Cargo      XML
      
       SELECT @Cargo = TransCargo
       FROM   T_WFTransacciones
       WHERE  TransID = @TransID 
      
       declare @ClienteID FolioGrande
       set @FileID = 0
            
 
 
       set @MultiplesFileID = ''
       select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
       from (
       select                                 
                    FileID = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                                   
        from
        @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
       ) CargoTabla
      
 
       select * into #PUESTO_TEMP
       from (
       select             
                    ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0),
                    PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),               
                    EMPRESA      = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ),
                    EMPLEADO     = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) , 
                    CB_PUESTO    = coalesce(t.c.value('(@PuestoCodigo)', 'Varchar(256)' ), '' ) ,  
                    CB_CLASIFI   = coalesce(t.c.value('(@ClasificacionCodigo)', 'Varchar(256)' ), '' ) ,   
                    CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,                   
            	    DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
                    OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )                       
        from
        @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
       ) CargoTabla
      
 
       set @Xml = (
              select            
                    MOV3= 'CAMBIOPUESTO',                  
                    EMPRESA,
             ( select
                     EMPRESA,                   
                     count(*) Cuantos,
 
                           ( SELECT ClienteId,
                                        PersonaID,
                                        EMPLEADO,
                                        CB_CLASIFI,
                                        CB_PUESTO,                                    
                                        CB_FECHAREG,
                                        DESCRIPCION,
                                        OBSERVACIONES             
                                        FROM #PUESTO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE )
              from #PUESTO_TEMP EMPLEADOS
              where DATOS.EMPRESA = EMPLEADOS.EMPRESA
                      
                   
             group by EMPRESA
             FOR XML AUTO, TYPE
              )
 
             from #PUESTO_TEMP DATOS
             group by EMPRESA
             FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE
       )
                          
      
       SET @ErrorID = 0
       set @Error = ''
      
       drop table #PUESTO_TEMP   
      
end
GO

CREATE PROCEDURE WFTransaccion_GetXMLCambioTurno( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #TURNO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_TURNO    = coalesce(t.c.value('(@TurnoCodigo)', 'Varchar(256)' ), '' ) ,                
            CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )               
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOTURNO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_TURNO, 
                        CB_FECHAREG,
                        DESCRIPCION,
                        OBSERVACIONES            
                        FROM #TURNO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #TURNO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #TURNO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #TURNO_TEMP     
    
end
GO 

CREATE PROCEDURE WFTransaccion_GetXMLCambioEquipo( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        

    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_EQUIPO    = coalesce(t.c.value('(@EquipoCodigo)', 'Varchar(256)' ), '' ) ,                
            CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOEQUIPO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_EQUIPO, 
                        CB_FECHAREG,
                        DESCRIPCION,
                        OBSERVACIONES            
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
GO

CREATE PROCEDURE WFTransaccion_GetXMLBajaEmpleado( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        

    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            PE_YEAR    = coalesce(  t.c.value('(@PeriodoYear)', 'INT' ), 0) ,    
            PE_TIPO    = coalesce(  t.c.value('(@TipoNominaCodigo)', 'INT' ), 0) ,    
            PE_NUMERO    = coalesce(  t.c.value('(@PeriodoNumero)', 'INT' ), 0) ,    
            CB_MOTIVO    = coalesce(t.c.value('(@MotKarCodigo)', 'Varchar(256)' ), '' ) ,                
            CB_FECHAREG = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_FECHASS   = convert( varchar(20),coalesce(t.c.value('(@KarFechaSS)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' ),
			CB_RECONTR    = coalesce(t.c.value('(@KarReingreso)', 'Varchar(256)' ), '' )
             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'BAJA',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_MOTIVO, 
                        CB_FECHAREG,
                        CB_FECHASS,
                        DESCRIPCION,
                        OBSERVACIONES, 
                        PE_YEAR, 
                        PE_TIPO, 
                        PE_NUMERO,
						CB_RECONTR = 
							case CB_RECONTR 
								when 'true' then 'S'
								when 'false' then 'N'
							else 
								'N' 
							end						          
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
GO

CREATE PROCEDURE WFTransaccion_GetXMLCambioSalario( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #SALARIO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,               
            CB_SALARIO   = coalesce(  t.c.value('(@CompSalario)', 'Numeric(15,2)' ), 0.00) ,               
            CB_FECHAREG = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_FECHASS   = convert( varchar(20),coalesce(t.c.value('(@KarFechaSS)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            KARAUTOSAL = coalesce(t.c.value('(@KarAutoSal )', 'INT' ), 0 )     , 
            DESCRIPCION     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarObserva)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOSALARIO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_SALARIO, 
                        CB_FECHAREG,
                        CB_FECHASS,
                        CB_AUTOSAL = case  KARAUTOSAL  when 0 then 'N'  else 'S'  end , 
                        DESCRIPCION,
                        OBSERVACIONES
                        FROM #SALARIO_TEMP REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #SALARIO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #SALARIO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #SALARIO_TEMP     
    
end
go 

CREATE PROCEDURE WFTransaccion_GetXMLCambioAreas( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_NIVEL1    = coalesce(t.c.value('(@Nivel1Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL2    = coalesce(t.c.value('(@Nivel2Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL3    = coalesce(t.c.value('(@Nivel3Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL4    = coalesce(t.c.value('(@Nivel4Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL5    = coalesce(t.c.value('(@Nivel5Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL6    = coalesce(t.c.value('(@Nivel6Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL7    = coalesce(t.c.value('(@Nivel7Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL8    = coalesce(t.c.value('(@Nivel8Codigo)', 'Varchar(256)' ), '' ) ,                
            CB_NIVEL9    = coalesce(t.c.value('(@Nivel9Codigo)', 'Varchar(256)' ), '' ) ,               
		    CB_PUESTO    = coalesce(t.c.value('(@PuestoCodigo)', 'Varchar(256)' ), '' ) ,               
			CB_TURNO    = coalesce(t.c.value('(@TurnoCodigo)', 'Varchar(256)' ), '' ) ,               
            CB_FECHAREG   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            DESCRIPCION     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )     , 
            OBSERVACIONES     = coalesce(t.c.value('(@KarObserva)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOAREAS',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_NIVEL1, 
                        CB_NIVEL2, 
                        CB_NIVEL3, 
                        CB_NIVEL4, 
                        CB_NIVEL5, 
                        CB_NIVEL6, 
                        CB_NIVEL7, 
                        CB_NIVEL8, 
                        CB_NIVEL9, 
						CB_PUESTO, 
						CB_TURNO,
                        CB_FECHAREG,
                        DESCRIPCION,
                        OBSERVACIONES            
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go 

CREATE PROCEDURE WFTransaccion_GetXMLCambioContrato( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
            CB_CONTRAT    = coalesce(t.c.value('(@ContratoCodigo)', 'Varchar(256)' ), '' ) ,                                        
            CB_FECHA   = convert( varchar(20),coalesce(t.c.value('(@KarFecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_FEC_VEC   = convert( varchar(20),coalesce(t.c.value('(@EmpContratoFin)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            CB_NOTA     = coalesce(t.c.value('(@KarDescrip)', 'Varchar(256)' ), '' )     , 
            CB_COMENTA     = coalesce(t.c.value('(@MotKarNombre)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CAMBIOCONTRATO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
                        CB_CONTRAT,                         
                        CB_FECHA,
                        CB_FEC_CON=CB_FECHA, 
                        CB_FEC_VEC=case when CB_FEC_VEC ='2999-12-31' then '1899-12-30' else CB_FEC_VEC end , 
                        CB_COMENTA,
                        CB_NOTA             
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go

CREATE PROCEDURE WFTransaccion_GetXMLCRUDParientes( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    
			PA_FEC_NAC  = convert( varchar(20),coalesce(t.c.value('(@ParienteNacFecha)', 'Datetime' ), '1899-12-30' ) , 112 ), 
			ParienteTipo =   coalesce(  t.c.value('(@ParienteTipo)', 'INT' ), 0),          
			PA_FOLIO =   coalesce(  t.c.value('(@ParienteFolio)', 'INT' ), 0),          
			PA_RELACIO =   coalesce(  t.c.value('(@ParentescoID)', 'INT' ), 0),          
			PA_SEXO =  case  coalesce(  t.c.value('(@ParienteGenero)', 'INT' ), 0)
							when 2 then 'F' 
							else 'M' 
						end, 
			PA_APE_PAT    = coalesce(t.c.value('(@ParienteApePat)', 'Varchar(256)' ), '' ) ,                                        
			PA_APE_MAT    = coalesce(t.c.value('(@ParienteApeMat)', 'Varchar(256)' ), '' ) ,                                        
			PA_NOMBRES    = coalesce(t.c.value('(@ParienteNombres)', 'Varchar(256)' ), '' ) ,          								                             
			LLAVE =   coalesce(  t.c.value('(@Llave)', 'INT' ), 0)         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla

	 delete from #EQUIPO_TEMP where ParienteTipo = 0 
     
     set @Xml = ( 
         select         
            MOV3= case ParienteTipo when 1 then 'PARIENTE_AGREGAR'
									when 2 then 'PARIENTE_MODIFICAR' 
									when 3 then 'PARIENTE_BORRAR' 
									ELSE 'PARIENTE_AGREGAR' 
					end,  
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO, 
						PA_FEC_NAC , 
						PA_FOLIO	, 
						PA_RELACIO ,  
						PA_SEXO    ,  
						PA_APE_PAT,  
						PA_APE_MAT,  
						PA_NOMBRES,  
						LLAVE
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA  and  REGISTRO.ParienteTipo = DATOS.ParienteTipo  FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA and DATOS.ParienteTipo = EMPLEADOS.ParienteTipo  and DATOS.ParienteTipo >0 
              
            
        group by EMPRESA, ParienteTipo
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA, ParienteTipo
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go 

CREATE PROCEDURE WFTransaccion_GetXMLAgregarAsegurado( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
                    EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,    

                    PM_CODIGO    = coalesce(  t.c.value('(@SeguroCodigo)', 'varchar(6)' ), '') ,    
                    PV_REFEREN    = coalesce(  t.c.value('(@SeguVigReferen)', 'varchar(20)' ), '') ,    
            EP_ASEGURA    = coalesce(  t.c.value('(@AseguraTipo)', 'INT' ), 0) ,    
                    PA_FOLIO =   coalesce(  t.c.value('(@ParienteFolio)', 'INT' ), 0),      
                    PA_RELACIO =   coalesce(  t.c.value('(@ParentescoID)', 'INT' ), 0),          
                    EP_TIPO =   coalesce(  t.c.value('(@AseguraTipoPol)', 'INT' ), 0),          
                    EP_CFIJO =   coalesce(  t.c.value('(@AseguraCFijo)', 'numeric(15,2)' ), 0.00),          
                    EP_CERTIFI    = coalesce(  t.c.value('(@AseguraCertifi)', 'varchar(30)' ), '') ,                            
                    EP_FEC_INI  = convert( varchar(20),coalesce(t.c.value('(@AseguraInicial)', 'Datetime' ), '1899-12-30' ) , 112 ), 
                    EP_FEC_FIN  = convert( varchar(20),coalesce(t.c.value('(@AseguraFinal)', 'Datetime' ), '1899-12-30' ) , 112 ), 
                    EP_OBSERVA    = coalesce(  t.c.value('(@AseguraObserva)', 'varchar(255)' ), '') ,    
                    EP_END_ALT    = coalesce(  t.c.value('(@AseguraEndoso)', 'varchar(30)' ), '') ,                                            
                    LLAVE =   coalesce(  t.c.value('(@Llave)', 'INT' ), 0)         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
        select * from #EQUIPO_TEMP 

     set @Xml = ( 
         select         
            MOV3= 'EMPSEGURO_AGREGAR',  
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO,                           
                                        PM_CODIGO, 
                                        PV_REFEREN, 
                                        EP_ASEGURA, 
                                        PA_FOLIO, 
                                        PA_RELACIO, 
                                        EP_TIPO, 
                                        EP_CFIJO, 
                                        EP_CERTIFI, 
                                        EP_FEC_INI, 
                                        EP_FEC_FIN, 
                                        EP_OBSERVA, 
                                        EP_END_ALT,  
                                        LLAVE
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go


CREATE PROCEDURE WFTransaccion_GetXMLChecadas( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #CHECADA_TEMP 
    from ( 
    select    
			      
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
			Fecha   = convert( varchar(20),coalesce(t.c.value('(@Fecha)', 'Datetime' ), '1899-12-30' ) , 112 ) ,  
			--Fecha   = coalesce(t.c.value('(@Fecha)', 'Date' ), '1899-12-30' ) ,  
			EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
			Empleado    = coalesce(  t.c.value('(@Empleado)', 'INT' ), 0) ,    
            Hora    = coalesce(t.c.value('(@Hora)', 'Varchar(10)' ), '') ,
			Credencial  = coalesce(t.c.value('(@Credencial)', 'Varchar(10)' ), '') ,
			Reloj  =  coalesce(t.c.value('(@Reloj)', 'Varchar(10)' ), ''),
			Motivo    = coalesce(t.c.value('(@Motivo)', 'Varchar(10)' ), '')         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'CHECADASIMPLE',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        Fecha, 
                        Empleado, 
                        Hora, 
                        Credencial,
                        Reloj,
						Motivo      
                        FROM #CHECADA_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #CHECADA_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #CHECADA_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')--, ROOT('MULTIPLE'), TYPE 
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #CHECADA_TEMP     
    
end
go

CREATE PROCEDURE WFTransaccion_GetXMLIntercambioFestivo( 
    @TransID    INT,
    @Xml        XML OUTPUT ,
    @FileID        INT OUTPUT, 
    @Error        VARCHAR( 255 ) OUTPUT, 
    @MultiplesFileID Varchar(max) output )
AS
begin     
    DECLARE @ErrorID    INT
    DECLARE @Cargo    XML
    
    SELECT    @Cargo    = TransCargo
    FROM    T_WFTransacciones
    WHERE    TransID = @TransID    
    
    declare @ClienteID FolioGrande 
    set @FileID = 0 
        
    set @MultiplesFileID = '' 
    select @MultiplesFileID  = COALESCE(@MultiplesFileID+',' , '' ) + RTRIM( LTRIM( STR( FileID ) ) )
    from ( 
    select                      
            FileID    = coalesce(t.c.value('(@FileID)', 'INT' ), 0)                         
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
    select * into #EQUIPO_TEMP 
    from ( 
    select          
            ClienteID    = coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0), 
            PersonaID    = coalesce(  t.c.value('(@PersonaID)', 'INT' ), 0),             
            EMPRESA    = dbo.WFEmpresas_GetCodigoEmpresa( coalesce(  t.c.value('(@ClienteID)', 'INT' ), 0)  ), 
            EMPLEADO    = coalesce(  t.c.value('(@EmpNumero)', 'INT' ), 0) ,                
            FECHAORIGINAL   = convert( varchar(20),coalesce(t.c.value('(@InterOriginal)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            FECHANUEVA   = convert( varchar(20),coalesce(t.c.value('(@InterNueva)', 'Datetime' ), '1899-12-30' ) , 112 ) ,             
            NOTA     = coalesce(t.c.value('(@Nota)', 'Varchar(256)' ), '' )     , 
            COMENTA     = coalesce(t.c.value('(@Comentario)', 'Varchar(256)' ), '' )             
     from 
     @Cargo.nodes('//REGISTROS/REGISTRO') t(c)
     ) CargoTabla
     
     set @Xml = ( 
         select         
            MOV3= 'INTERCAMBIOFESTIVO',            
            EMPRESA, 
         ( select 
             EMPRESA,              
             count(*) Cuantos, 
                ( SELECT ClienteId, 
                        PersonaID, 
                        EMPLEADO,                         
                        FECHAORIGINAL, 
                        FECHANUEVA, 
                        COMENTA,
                        NOTA             
                        FROM #EQUIPO_TEMP  REGISTRO  where EMPLEADOS.EMPRESA = REGISTRO.EMPRESA   FOR XML PATH('INFO'), TYPE ) 
         from #EQUIPO_TEMP EMPLEADOS 
         where DATOS.EMPRESA = EMPLEADOS.EMPRESA 
              
            
        group by EMPRESA
         FOR XML AUTO, TYPE 
         ) 
         from #EQUIPO_TEMP DATOS 
        group by EMPRESA
         FOR XML PATH('DATOS')
    ) 
                
    
    SET @ErrorID = 0 
    set @Error = '' 
    
    drop table #EQUIPO_TEMP     
    
end
go 


CREATE  PROCEDURE WFTransaccion_GetXMLAltaEmpleadoGenerica( 
	@TransID	INT,
	@Xml		XML OUTPUT ,
	@FileID		INT OUTPUT, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
begin 	
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML
	
	SELECT	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID	
	
	DECLARE @EMPRESA			varchar(10) 
	DECLARE @kCB_CODIGO			INT
	DECLARE @NOMBRE				Formula 
	DECLARE @KarFecha			Fecha 
	
	DECLARE @PersonaID			FolioGrande 		
	DECLARE @EquipoID			FolioGrande  	
	DECLARE @PersonaTipo		int 
	DECLARE @KarObservaciones	Titulo  
	DECLARE @kCB_AUTOSAL		Booleano
	DECLARE @kCB_AUTOSAL_GLOBAL Booleano

	DECLARE @kCB_APE_MAT	Descripcion
	DECLARE @kCB_APE_PAT	Descripcion
	DECLARE @kCB_NOMBRES	Descripcion


	DECLARE @kCB_BAN_ELE	Descripcion
	DECLARE @kCB_CALLE		Observaciones
	DECLARE @kCB_CARRERA	DescLarga
	DECLARE @kCB_CHECA		Booleano
	DECLARE @kCB_CIUDAD		Descripcion
	DECLARE @kCB_CLASIFI	Codigo
	DECLARE @kCB_CODPOST	Referencia
	DECLARE @kCB_COLONIA	Descripcion
	DECLARE @kCB_CONTRAT	Codigo1
	DECLARE @kCB_CREDENC	Codigo1
	DECLARE @kCB_CTA_VAL	Descripcion
	DECLARE @kCB_CURP		Descripcion
	DECLARE @kCB_E_MAIL		Formula
	DECLARE @kCB_ESTADO		Codigo2
	DECLARE @kCB_ESTUDIO	Codigo2
	DECLARE @kCB_FEC_ANT	Fecha
	DECLARE @kCB_FEC_CON	Fecha
	DECLARE @kCB_FEC_ING	Fecha
	DECLARE @kCB_FEC_NAC	Fecha
	DECLARE @kCB_LUG_NAC	Descripcion
	DECLARE @kCB_MUNICIP	Codigo
	DECLARE @kCB_NACION		Descripcion
	DECLARE @kCB_NIVEL0		Codigo
	DECLARE @kCB_NIVEL1		Codigo
	DECLARE @kCB_NIVEL2		Codigo
	DECLARE @kCB_NIVEL3		Codigo
	DECLARE @kCB_NIVEL4		Codigo
	DECLARE @kCB_NIVEL5		Codigo
	DECLARE @kCB_NIVEL6		Codigo
	DECLARE @kCB_NIVEL7		Codigo
	DECLARE @kCB_NIVEL8		Codigo
	DECLARE @kCB_NIVEL9		Codigo
	DECLARE @kCB_PATRON		RegPatronal
	DECLARE @kCB_PUESTO		Codigo
	DECLARE @kCB_RFC		Descripcion
	DECLARE @kCB_SALARIO	PesosDiario
	DECLARE @PersonaSalario PesosDiario
	DECLARE @kCB_SALARIO_BRUTO	PesosDiario
	DECLARE @kCB_SEGSOC		Descripcion
	DECLARE @kCB_SEXO		Codigo1
	DECLARE @kCB_TABLASS	Codigo1
	DECLARE @kCB_TEL		Descripcion
	DECLARE @kCB_TURNO		Codigo
	DECLARE @kCB_ZONA_GE	ZonaGeo

	---PADRES
	DECLARE @kNOM_PADRE	Descripcion
	DECLARE @kNOM_MADRE	Descripcion

	---- CAMPOS NUEVOS A MAPEAR
	DECLARE @kCB_AR_FEC as Fecha
	DECLARE @kCB_AR_HOR as Hora
	DECLARE @kCB_EDO_CIV as Codigo1
	DECLARE @kCB_FEC_RES as Fecha
	DECLARE @kCB_EST_HOR as Descripcion
	DECLARE @kCB_EST_HOY as Booleano
	DECLARE @kCB_EST_HOY_INT as Status
	DECLARE @kCB_EVALUA as DiasFrac
	DECLARE @kCB_EXPERIE as Titulo
	DECLARE @kCB_FEC_BAJ as Fecha
	DECLARE @kCB_FEC_BSS as Fecha
	DECLARE @kCB_FEC_INT as Fecha
	DECLARE @kCB_FEC_REV as Fecha
	DECLARE @kCB_FEC_VAC as Fecha
	DECLARE @kCB_G_FEC_1 as Fecha
	DECLARE @kCB_G_FEC_2 as Fecha
	DECLARE @kCB_G_FEC_3 as Fecha
	DECLARE @kCB_G_FEC_4 as Fecha
	DECLARE @kCB_G_FEC_5 as Fecha
	DECLARE @kCB_G_FEC_6 as Fecha
	DECLARE @kCB_G_FEC_7 as Fecha
	DECLARE @kCB_G_FEC_8 as Fecha
	DECLARE @kCB_G_LOG_1 as Booleano
	DECLARE @kCB_G_LOG_1_INT as Status
	DECLARE @kCB_G_LOG_2 as Booleano
	DECLARE @kCB_G_LOG_2_INT as Status
	DECLARE @kCB_G_LOG_3 as Booleano
	DECLARE @kCB_G_LOG_3_INT as Status
	DECLARE @kCB_G_LOG_4 as Booleano
	DECLARE @kCB_G_LOG_4_INT as Status
	DECLARE @kCB_G_LOG_5 as Booleano
	DECLARE @kCB_G_LOG_5_INT as Status
	DECLARE @kCB_G_LOG_6 as Booleano
	DECLARE @kCB_G_LOG_6_INT as Status
	DECLARE @kCB_G_LOG_7 as Booleano
	DECLARE @kCB_G_LOG_7_INT as Status
	DECLARE @kCB_G_LOG_8 as Booleano
	DECLARE @kCB_G_LOG_8_INT as Status
	DECLARE @kCB_G_NUM_1 as Pesos
	DECLARE @kCB_G_NUM_2 as Pesos
	DECLARE @kCB_G_NUM_3 as Pesos
	DECLARE @kCB_G_NUM_4 as Pesos
	DECLARE @kCB_G_NUM_5 as Pesos
	DECLARE @kCB_G_NUM_6 as Pesos
	DECLARE @kCB_G_NUM_7 as Pesos
	DECLARE @kCB_G_NUM_8 as Pesos
	DECLARE @kCB_G_NUM_9 as Pesos
	DECLARE @kCB_G_NUM10 as Pesos
	DECLARE @kCB_G_NUM11 as Pesos
	DECLARE @kCB_G_NUM12 as Pesos
	DECLARE @kCB_G_NUM13 as Pesos
	DECLARE @kCB_G_NUM14 as Pesos
	DECLARE @kCB_G_NUM15 as Pesos
	DECLARE @kCB_G_NUM16 as Pesos
	DECLARE @kCB_G_NUM17 as Pesos
	DECLARE @kCB_G_NUM18 as Pesos
	DECLARE @kCB_G_TAB_1 as Codigo
	DECLARE @kCB_G_TAB_2 as Codigo
	DECLARE @kCB_G_TAB_3 as Codigo
	DECLARE @kCB_G_TAB_4 as Codigo
	DECLARE @kCB_G_TAB_5 as Codigo
	DECLARE @kCB_G_TAB_6 as Codigo
	DECLARE @kCB_G_TAB_7 as Codigo
	DECLARE @kCB_G_TAB_8 as Codigo
	DECLARE @kCB_G_TAB_9 as Codigo
	DECLARE @kCB_G_TAB10 as Codigo
	DECLARE @kCB_G_TAB11 as Codigo
	DECLARE @kCB_G_TAB12 as Codigo
	DECLARE @kCB_G_TAB13 as Codigo
	DECLARE @kCB_G_TAB14 as Codigo
	DECLARE @kCB_G_TEX_1 as Descripcion
	DECLARE @kCB_G_TEX_2 as Descripcion
	DECLARE @kCB_G_TEX_3 as Descripcion
	DECLARE @kCB_G_TEX_4 as Descripcion
	DECLARE @kCB_G_TEX_5 as Descripcion
	DECLARE @kCB_G_TEX_6 as Descripcion
	DECLARE @kCB_G_TEX_7 as Descripcion
	DECLARE @kCB_G_TEX_8 as Descripcion
	DECLARE @kCB_G_TEX_9 as Descripcion
	DECLARE @kCB_G_TEX10 as Descripcion
	DECLARE @kCB_G_TEX11 as Descripcion
	DECLARE @kCB_G_TEX12 as Descripcion
	DECLARE @kCB_G_TEX13 as Descripcion
	DECLARE @kCB_G_TEX14 as Descripcion
	DECLARE @kCB_G_TEX15 as Descripcion
	DECLARE @kCB_G_TEX16 as Descripcion
	DECLARE @kCB_G_TEX17 as Descripcion
	DECLARE @kCB_G_TEX18 as Descripcion
	DECLARE @kCB_G_TEX19 as Descripcion
	DECLARE @kCB_HABLA as Booleano
	DECLARE @kCB_HABLA_INT as Status
	DECLARE @kCB_IDIOMA as Descripcion
	DECLARE @kCB_INFCRED as Descripcion
	DECLARE @kCB_INFMANT as Booleano
	DECLARE @kCB_INFMANT_INT as Status
	DECLARE @kCB_INFTASA as DescINFO
	DECLARE @kCB_LA_MAT as Descripcion
	DECLARE @kCB_LAST_EV as Fecha
	DECLARE @kCB_MAQUINA as Titulo
	DECLARE @kCB_MED_TRA as Codigo1
	DECLARE @kCB_PASAPOR as Booleano
	DECLARE @kCB_PASAPOR_INT as Status
	DECLARE @kCB_FEC_INC as Fecha
	DECLARE @kCB_FEC_PER as Fecha
	DECLARE @kCB_NOMYEAR as Anio
	DECLARE @kCB_NOMTIPO as NominaTipo
	DECLARE @kCB_NEXT_EV as Fecha
	DECLARE @kCB_NOMNUME as NominaNumero
	DECLARE @kCB_SAL_INT as PesosDiario
	DECLARE @kCB_VIVECON as Codigo1
	DECLARE @kCB_VIVEEN as Codigo2
	DECLARE @kCB_ZONA as Referencia
	DECLARE @kCB_INFTIPO as Status
	DECLARE @kCB_DER_FEC as Fecha
	DECLARE @kCB_DER_PAG as DiasFrac
	DECLARE @kCB_V_PAGO as DiasFrac
	DECLARE @kCB_DER_GOZ as DiasFrac
	DECLARE @kCB_V_GOZO as DiasFrac
	DECLARE @kCB_TIP_REV as Codigo
	DECLARE @kCB_MOT_BAJ as Codigo3
	DECLARE @kCB_FEC_SAL as Fecha
	DECLARE @kCB_INF_INI as Fecha
	DECLARE @kCB_INF_OLD as DescINFO
	DECLARE @kCB_OLD_SAL as PesosDiario
	DECLARE @kCB_OLD_INT as PesosDiario
	DECLARE @kCB_PRE_INT as PesosDiario
	DECLARE @kCB_PER_VAR as PesosDiario
	DECLARE @kCB_SAL_TOT as PesosDiario
	DECLARE @kCB_TOT_GRA as PesosDiario
	DECLARE @kCB_FAC_INT as Tasa
	DECLARE @kCB_RANGO_S as Tasa
	DECLARE @kCB_CLINICA as Codigo3
	DECLARE @kCB_FEC_NIV as Fecha
	DECLARE @kCB_FEC_PTO as Fecha
	DECLARE @kCB_AREA as Codigo
	DECLARE @kCB_FEC_TUR as Fecha
	DECLARE @kCB_FEC_KAR as Fecha
	DECLARE @kCB_PENSION as Status
	DECLARE @kCB_CANDIDA as FolioGrande
	DECLARE @kCB_ID_NUM as Descripcion
	DECLARE @kCB_ENT_NAC as Codigo2
	DECLARE @kCB_COD_COL as Codigo
	DECLARE @kCB_PASSWRD as Passwd
	DECLARE @kCB_PLAZA as FolioGrande
	DECLARE @kCB_FEC_PLA as Fecha
	DECLARE @kCB_DER_PV as DiasFrac
	DECLARE @kCB_V_PRIMA as DiasFrac
	DECLARE @kCB_SUB_CTA as Descripcion
	DECLARE @kCB_NETO as Pesos
	DECLARE @kCB_PORTAL as Booleano
	DECLARE @kCB_PORTAL_INT as Status
	DECLARE @kCB_DNN_OK as Booleano
	DECLARE @kCB_DNN_OK_INT as Status
	DECLARE @kCB_USRNAME as DescLarga
	DECLARE @kCB_NOMINA as NominaTipo
	DECLARE @kCB_FEC_NOM as Fecha
	DECLARE @kCB_RECONTR as Booleano
	DECLARE @kCB_RECONTR_INT as Status
	DECLARE @kCB_DISCAPA as Booleano
	DECLARE @kCB_DISCAPA_INT as Status
	DECLARE @kCB_INDIGE as Booleano
	DECLARE @kCB_INDIGE_INT as Status
	DECLARE @kCB_FONACOT as Descripcion
	DECLARE @kCB_EMPLEO as Booleano
	DECLARE @kCB_EMPLEO_INT as Status
	DECLARE @kUS_CODIGO as Usuario
	DECLARE @kCB_FEC_COV as Fecha
	DECLARE @kCB_G_TEX20 as Descripcion
	DECLARE @kCB_G_TEX21 as Descripcion
	DECLARE @kCB_G_TEX22 as Descripcion
	DECLARE @kCB_G_TEX23 as Descripcion
	DECLARE @kCB_G_TEX24 as Descripcion
	DECLARE @kCB_INFDISM as Booleano
	DECLARE @kCB_INFDISM_INT as Status
	DECLARE @kCB_INFACT as Booleano
	DECLARE @kCB_INFACT_INT as Status
	DECLARE @kCB_NUM_EXT as NombreCampo 
	DECLARE @kCB_NUM_INT as NombreCampo
	DECLARE @kCB_INF_ANT as Fecha
	DECLARE @kCB_TDISCAP as Status
	DECLARE @kCB_ESCUELA as DescLarga
	DECLARE @kCB_TESCUEL as Status
	DECLARE @kCB_TITULO as Status
	DECLARE @kCB_YTITULO as Anio
	DECLARE @kCB_CTA_GAS as Descripcion
	DECLARE @kCB_ID_BIO as FolioGrande
	DECLARE @kCB_GP_COD as Codigo
	DECLARE @kCB_TIE_PEN as Booleano
	DECLARE @kCB_TIE_PEN_INT as Status
	DECLARE @kCB_TSANGRE as Descripcion
	DECLARE @kCB_ALERGIA as Formula
	DECLARE @kCB_BRG_ACT as Booleano
	DECLARE @kCB_BRG_ACT_INT as Status
	DECLARE @kCB_BRG_TIP as Status
	DECLARE @kCB_BRG_ROL as Booleano
	DECLARE @kCB_BRG_ROL_INT as Status
	DECLARE @kCB_BRG_JEF as Booleano
	DECLARE @kCB_BRG_JEF_INT as Status
	DECLARE @kCB_BRG_CON as Booleano
	DECLARE @kCB_BRG_CON_INT as Status
	DECLARE @kCB_BRG_PRA as Booleano
	DECLARE @kCB_BRG_PRA_INT as Status
	DECLARE @kCB_BRG_NOP as Descripcion
	DECLARE @kCB_BANCO as Codigo
	DECLARE @kCB_REGIMEN as Status

	DECLARE @MOV3 as Descripcion
	DECLARE @EmpNumero as Codigo
	DECLARE @IngresoReingreso as Codigo
	----- FIN	
	
	DECLARE @XMLTress  Varchar(max) 
	

	declare @ClienteID FolioGrande 
	set @FileID = 0 
	SET @ErrorID = 0 
	set @ERROR = '' 

	--Toma globales de Empleado 		
	select 
		@kCB_Ciudad = Ciudad , 
		@kCB_Estado = Estado , 
		@kCB_MUNICIP = Municipio , 
		@kCB_Checa = Checa , 
		@kCB_Nacion = Nacion , 
		@kCB_Sexo = Sexo , 
		@kCB_Credenc = Credencial , 
		@kCB_EDO_CIV = EstadoCivil , 
		@kCB_ViveEn = ViveEn , 
		@kCB_ViveCon = ViveCon , 
		@kCB_MED_TRA = MedioTransporte , 
		@kCB_Estudio = Estudio , 
		@kCB_AUTOSAL_GLOBAL = AutoSal , 
		@kCB_Zona = ZonaGeografica , 
		@kCB_Patron = Patron , 
		@kCB_Salario = Salario , 
		@kCB_Contrat = Contrato , 
		@kCB_Puesto = Puesto , 
		@kCB_Clasifi = Clasifi , 
		@kCB_Turno = Turno , 
		@kCB_TablaSS = TablaSS , 
		@kCB_Nivel1 = Nivel1 , 
		@kCB_Nivel2 = Nivel2 , 
		@kCB_Nivel3 = Nivel3 , 
		@kCB_Nivel4 = Nivel4 , 
		@kCB_Nivel5 = Nivel5 , 
		@kCB_Nivel6 = Nivel6 , 
		@kCB_Nivel7 = Nivel7 , 
		@kCB_Nivel8 = Nivel8 , 
		@kCB_Nivel9 = Nivel9 , 
		@kCB_Regimen = Regimen , 
		@KCB_Banco = Banco 
	from dbo.WF_GetGlobalesEmpleado() 

	SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0)
	SET	@EmpNumero	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0)  
	SET	@IngresoReingreso	= coalesce(  @Cargo.value('(//REGISTRO/@IngresoReingreso)[1]', 'INT' ), 0) 
	SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
	
	SET	@PersonaID	= coalesce(  @Cargo.value('(//REGISTRO/@PersonaID)[1]', 'INT' ),  @TransID )     	
	
	--- IDENTIFICACION DEL EMPLEADO 
	SET	@KCB_APE_PAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApePat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_APE_MAT		= coalesce( @Cargo.value('(//REGISTRO/@PersonaApeMat)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_NOMBRES		= coalesce( @Cargo.value('(//REGISTRO/@PersonaNombres)[1]', 'Varchar(256)' ), '' ) 	
	SET @kCB_FEC_NAC  = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET	@KCB_RFC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaRFC)[1]', 'Varchar(256)' ), '' ) 
	SET	@KCB_CURP		= coalesce( @Cargo.value('(//REGISTRO/@PersonaCURP)[1]', 'Varchar(256)' ), '' ) 
	SET	@kCB_SEGSOC		= coalesce( @Cargo.value('(//REGISTRO/@PersonaIMSS)[1]', 'Varchar(256)' ), '' ) 	
	SET	@KCB_SEXO		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaGenero)[1]', 'int' ), -1 ) ) 
									when  1 then 'M' 
	                                when  2 then 'F'
									else  @KCB_SEXO 
						end;  
	SET @kCB_ENT_NAC = coalesce( @Cargo.value('(//REGISTRO/@NacEstadoCodigo)[1]', 'Varchar(2)' ), '' ) -- as Codigo2
	
	-- PERSONALES  
	SET @kCB_LUG_NAC   = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacCiudad)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_NACION    = coalesce(@Cargo.value('(//REGISTRO/@PersonaNacionalidad)[1]', 'Varchar(30)' ), '' ) 	
	SET	@kCB_PASAPOR_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaPasaporte)[1]', 'INT' ), 0)	
	if ( @kCB_PASAPOR_INT = 1 ) 
		Set @kCB_PASAPOR = 'S' 
	else
		Set @kCB_PASAPOR = 'N'
	SET @kCB_EDO_CIV = coalesce( @Cargo.value('(//REGISTRO/@EdoCivilCodigo)[1]', 'Varchar(1)' ), @kCB_EDO_CIV ) --Estado Civil
	SET @kCB_LA_MAT = coalesce( @Cargo.value('(//REGISTRO/@PersonaLugarMat)[1]', 'Varchar(30)' ), '' ) --Lugar y Fecha de matrimonio
	SET @kNOM_PADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomPad)[1]', 'Varchar(30)' ), '' )
	SET @kNOM_MADRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaNomMad)[1]', 'Varchar(30)' ), '' )
	SET @kCB_MED_TRA = coalesce( @Cargo.value('(//REGISTRO/@TransporteCodigo)[1]', 'Varchar(1)' ), @kCB_MED_TRA  ) --Transporte	
	SET	@kCB_DISCAPA_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaSufreDiscapa)[1]', 'INT' ), 0)	
	if ( @kCB_DISCAPA_INT = 1 ) 
		Set @kCB_DISCAPA = 'S' 
	else
		Set @kCB_DISCAPA = 'N'
	SET @kCB_TDISCAP = coalesce(@Cargo.value('(//REGISTRO/@PersonaDiscapa)[1]', 'INT' ), 0) -- as Status		
	--SET @kCB_INDIGE as Booleano
	SET	@kCB_INDIGE_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaIndigena)[1]', 'INT' ), 0)	
	if ( @kCB_INDIGE_INT = 1 ) 
		Set @kCB_INDIGE = 'S' 
	else
		Set @kCB_INDIGE = 'N'
		
	-- DOMICILIO 
	SET @kCB_CALLE = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCalle)[1]', 'Varchar(50)' ), '' ) 
	SET @kCB_NUM_EXT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumExt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_NUM_INT = coalesce( @Cargo.value('(//REGISTRO/@DomicilioNumInt)[1]', 'Varchar(10)' ), '' ) 
	SET @kCB_COLONIA = coalesce( @Cargo.value('(//REGISTRO/@DomicilioColonia)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_CODPOST = coalesce( @Cargo.value('(//REGISTRO/@DomicilioCodPost)[1]', 'Varchar(8)' ), '' ) 
	SET @kCB_COD_COL = coalesce( @Cargo.value('(//REGISTRO/@ColoniaCodigo)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_CLINICA = coalesce( @Cargo.value('(//REGISTRO/@Clinica)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_ZONA 	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioZona)[1]', 'Varchar(8)' ), @kCB_ZONA )  -- as Referencia	
	SET	@KCB_CIUDAD	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@DomicilioCiudad)[1]', 'Varchar(30)' ), @kCB_Ciudad ) 	
	SET	@KCB_ESTADO	 =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstadoCodigo)[1]', 'Varchar(6)' ), @kCB_ESTADO ) 
	SET @kCB_MUNICIP =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@MunicipioCodigo)[1]', 'Varchar(6)' ), @kCB_MUNICIP ) 
	
	-- CONTACTO 
	SET @kCB_TEL = coalesce( @Cargo.value('(//REGISTRO/@PersonaTelefono)[1]', 'Varchar(30)' ), '' ) 
	SET @kCB_E_MAIL = coalesce( @Cargo.value('(//REGISTRO/@PersonaEmail)[1]', 'Varchar(255)' ), '' ) 
	SET @kCB_VIVECON = coalesce( @Cargo.value('(//REGISTRO/@ViveConCodigo)[1]', 'Varchar(1)' ), @kCB_ViveCon  )  -- as Codigo1
	SET @kCB_VIVEEN = coalesce( @Cargo.value('(//REGISTRO/@ViveEnCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_FEC_RES = coalesce(@Cargo.value('(//REGISTRO/@ResidenciaFecha)[1]', 'Date' ), '1899-12-30' ) --Fecha Residencia 	

	--- EXPERIENCIA  
	-- Exp. Academica 
	SET @kCB_ESTUDIO =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@EstudioCodigo)[1]', 'Varchar(2)' ),  @kCB_VIVEEN )  -- as Codigo2
	SET @kCB_ESCUELA =  coalesce( @Cargo.value('(//REGISTRO/@PersonaEscuela)[1]', 'Varchar(40)' ), '' )	
	SET @kCB_TESCUEL = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstTipo)[1]', 'INT' ), 0) -- as Status
	SET @kCB_CARRERA  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCarrera)[1]', 'Varchar(40)' ), '' ) 		
	SET @kCB_TITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaTitulo)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_YTITULO = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstYear)[1]', 'INT' ), 0) -- as Anio
	SET @kCB_EST_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaEstudiaHorario)[1]', 'Varchar(30)' ), '' ) --Carrera y Horario
	---DECLARE @kCB_EST_HOY as Booleano
	SET	@kCB_EST_HOY_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaEstudia)[1]', 'INT' ), 0)	
	if ( @kCB_EST_HOY_INT = 1 ) 
		Set @kCB_EST_HOY = 'S' 
	else
		Set @kCB_EST_HOY = 'N'

	-- Habilidades 
	SET	@kCB_HABLA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaOtroIdioma)[1]', 'INT' ), 0)	
	if ( @kCB_HABLA_INT = 1 ) 
		Set @kCB_HABLA = 'S' 
	else
		Set @kCB_HABLA = 'N'
	SET @kCB_IDIOMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdiomas)[1]', 'Varchar(30)' ), '' ) --Idioma o dominio
	SET @kCB_MAQUINA = coalesce( @Cargo.value('(//REGISTRO/@PersonaMaquinas)[1]', 'Varchar(100)' ), '' ) --Maquinas que conoce
	SET @kCB_EXPERIE = coalesce( @Cargo.value('(//REGISTRO/@PersonaExperiencia)[1]', 'Varchar(100)' ), '' )--Experiencia
	
	
	-- Contratacion 	
	SET @kCB_FEC_ING  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_FEC_ANT  = coalesce(@Cargo.value('(//REGISTRO/@EmpAntigFecha)[1]', 'Date' ), '1899-12-30' ) 	
	
	-- Contrato 
	SET @kCB_FEC_CON  = coalesce(@Cargo.value('(//REGISTRO/@EmpContratoIni)[1]', 'Date' ), @kCB_FEC_ING ) 	
	SET	@KCB_CONTRAT  =  dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ContratoCodigo)[1]', 'Varchar(6)' ), @KCB_CONTRAT ) 
		--Calcular el numero de dias para la fecha de vencimiento del contrato
		declare @DiasContrato as FolioChico
		SELECT @DiasContrato = TB_DIAS FROM CONTRATO WHERE TB_CODIGO = @kCB_CONTRAT
		if ( @DiasContrato > 0 )
		begin		 
			set @kCB_FEC_COV = cast( dateadd( day, @DiasContrato -1, @kCB_FEC_CON ) as Datetime ) 
		end
	
		
	SET	@KCB_PUESTO		= coalesce( @Cargo.value('(//REGISTRO/@PuestoCodigo)[1]', 'Varchar(6)' ), '' ) 
	SET @kCB_NOMINA 	= coalesce(@Cargo.value('(//REGISTRO/@TipoNominaCodigo)[1]', 'INT' ), @kCB_NOMINA) -- as NominaTipo
	
	-- Tomados del puesto en caso de que no esten definidos 
	----------------- TOMADOS DEL PUESTO  --------------------------------
	declare @kPU_PATRON Codigo1 
	declare @kPU_TURNO Codigo 
	declare @kPU_AUTOSAL Codigo 
	declare @kPU_TABLASS Codigo 
	declare @KPU_CLASIFI Codigo 
	
	select @KPU_CLASIFI = PU_CLASIFI, @kPU_PATRON = PU_PATRON, @kPU_TURNO = PU_TURNO,  @kPU_AUTOSAL  = PU_AUTOSAL, @kPU_TABLASS = PU_TABLASS from PUESTO where PU_CODIGO = @kCB_PUESTO 

	-- Si no estan definidas en el puesto tomas las default 
	set @kCB_AUTOSAL_GLOBAL = @KCB_AUTOSAL;
    set @kCB_PATRON  = case  coalesce(@kPU_PATRON,'')   when '' then @kCB_PATRON   else @kPU_PATRON  end
	set @kCB_TURNO = case  coalesce(@kPU_TURNO,'')  when '' then @kCB_TURNO  else @kPU_TURNO end	
	set @kCB_TABLASS = case  coalesce(@kPU_TABLASS,'')  when '' then @kCB_TABLASS  else @kPU_TABLASS end
	set @kCB_CLASIFI = case  coalesce(@KPU_CLASIFI,'')  when '' then @kCB_CLASIFI  else @KPU_CLASIFI end
	set @kCB_CLASIFI = coalesce( @kCB_CLASIFI, '' ); 

    -- Al ultimo lee los del Cargo 	
	SET	@KCB_CLASIFI 	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TabuladorCodigo)[1]', 'Varchar(6)' ), @KCB_CLASIFI ) 
	SET	@KCB_TURNO		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@TurnoCodigo)[1]', 'Varchar(6)' ), @KCB_TURNO ) 
	SET	@KCB_PATRON		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@RegPatronCodigo)[1]', 'Varchar(1)' ), @KCB_PATRON ) 	
		
	-- TimbradoNomina 
	SET @kCB_REGIMEN = coalesce(@Cargo.value('(//REGISTRO/@RegimenSAT)[1]', 'INT' ), @kCB_REGIMEN) -- as Status
	
	
	-- AREA 
	SET	@kCB_NIVEL1		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel1Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL1 ) 
	SET	@kCB_NIVEL2		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel2Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL2 ) 
	SET	@kCB_NIVEL3		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel3Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL3 ) 
	SET	@kCB_NIVEL4		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel4Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL4 ) 
	SET	@kCB_NIVEL5		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel5Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL5 ) 
	SET	@kCB_NIVEL6		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel6Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL6 ) 
	SET	@kCB_NIVEL7		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel7Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL7 ) 
	SET	@kCB_NIVEL8		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel8Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL8 ) 
	SET	@kCB_NIVEL9		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@Nivel9Codigo)[1]', 'Varchar(6)' ), @kCB_NIVEL9 ) 
	
	
    -- SALARIO 	
	SET	@KCB_TABLASS		= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@PrestaLeyCodigo)[1]', 'Varchar(6)' ), @KCB_TABLASS ) 	
	SET	@KCB_AUTOSAL	=case coalesce( @Cargo.value('(//REGISTRO/@SalTabula)[1]', 'int' ),  -1 ) 
								    when  0 then 'N'
									when  1 then 'S'
									else @KCB_AUTOSAL
						end;	
	SET	@kCB_SALARIO	= coalesce( @Cargo.value('(//REGISTRO/@SalDiario)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET @kCB_PER_VAR    = coalesce( @Cargo.value('(//REGISTRO/@PromVar)[1]', 'Decimal(15,2)' ), @kCB_SALARIO ) 
	SET	@KCB_ZONA_GE	= dbo.WF_isEmpty( @Cargo.value('(//REGISTRO/@ZonaGeo)[1]', 'Varchar(1)' ), 'A' ) 	
	
	
	-- Datos del movimiento 
	SET @KarFecha  = coalesce(@Cargo.value('(//REGISTRO/@EmpIngresoFecha)[1]', 'Date' ), '1899-12-30' ) 		
	SET @KarObservaciones  = coalesce(@Cargo.value('(//REGISTRO/@KarObservaciones)[1]', 'Varchar(255)' ), '' ) 
	SET	@FileID	= coalesce(@Cargo.value('(//REGISTRO/@FileID)[1]', 'INT' ), 0)	
	
	SET	@KCB_CHECA		=case ( coalesce( @Cargo.value('(//REGISTRO/@PersonaCheca)[1]', 'int' ), -1 ) ) 
	                                when  0 then 'N'
									when  1 then 'S'
									else @KCB_CHECA
						end;  




	
	
	-----Campos que faltaban de mapear del XML "CargoAltaEmpleado.xml"  -------
	SET @kCB_CTA_VAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaVale)[1]', 'Varchar(30)' ), '' )
	

	---- Campos Faltantes que no se encontraban en el XML "CargoAltaEmpleado.xml" para futuras Altas
	SET @kCB_AR_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaArFec)[1]', 'Date' ), '1899-12-30' ) 	
	SET @kCB_AR_HOR = coalesce( @Cargo.value('(//REGISTRO/@PersonaArHor)[1]', 'Varchar(4)' ), '' )
	
	
	
	SET @kCB_EVALUA = coalesce( @Cargo.value('(//REGISTRO/@PersonaEvalua)[1]', 'Decimal(15,2)' ), 0.00 ) --Resultado Evaluacion
	
	SET @kCB_FEC_BAJ = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBaj)[1]', 'Date' ), '1899-12-30' ) --Fecha Baja	
	SET @kCB_FEC_BSS = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecBss)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInt)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_REV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecRev)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_VAC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecVac)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_1 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec1)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_2 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec2)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_3 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec3)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_4 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec4)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_5 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec5)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_6 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec6)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_7 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec7)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_G_FEC_8 = coalesce(@Cargo.value('(//REGISTRO/@PersonaGFec8)[1]', 'Date' ), '1899-12-30' )
	
	----------------------------------------- VALORES ADICIONALES DE TRESS ---------------------------	
	--DECLARE @kCB_G_LOG_1 as Booleano
	SET	@kCB_G_LOG_1_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog1)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_1_INT = 1 ) 
		Set @kCB_G_LOG_1 = 'S' 
	else
		Set @kCB_G_LOG_1 = 'N'
	--DECLARE @kCB_G_LOG_2 as Booleano
	SET	@kCB_G_LOG_2_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog2)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_2_INT = 1 ) 
		Set @kCB_G_LOG_2 = 'S' 
	else
		Set @kCB_G_LOG_2 = 'N' 
	--DECLARE @kCB_G_LOG_3 as Booleano
	SET	@kCB_G_LOG_3_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog3)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_3_INT = 1 ) 
		Set @kCB_G_LOG_3 = 'S' 
	else
		Set @kCB_G_LOG_3 = 'N' 
	--DECLARE @kCB_G_LOG_4 as Booleano
	SET	@kCB_G_LOG_4_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog4)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_4_INT = 1 ) 
		Set @kCB_G_LOG_4 = 'S' 
	else
		Set @kCB_G_LOG_4 = 'N' 
	--DECLARE @kCB_G_LOG_5 as Booleano
	SET	@kCB_G_LOG_5_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog5)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_5_INT = 1 ) 
		Set @kCB_G_LOG_5 = 'S' 
	else
		Set @kCB_G_LOG_5 = 'N' 
	--DECLARE @kCB_G_LOG_6 as Booleano
	SET	@kCB_G_LOG_6_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog6)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_6_INT = 1 ) 
		Set @kCB_G_LOG_6 = 'S' 
	else
		Set @kCB_G_LOG_6 = 'N' 
	--DECLARE @kCB_G_LOG_7 as Booleano
	SET	@kCB_G_LOG_7_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog7)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_7_INT = 1 ) 
		Set @kCB_G_LOG_7 = 'S' 
	else
		Set @kCB_G_LOG_7 = 'N' 
	--DECLARE @kCB_G_LOG_8 as Booleano
	SET	@kCB_G_LOG_8_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaGLog8)[1]', 'INT' ), 0)	
	if ( @kCB_G_LOG_8_INT = 1 ) 
		Set @kCB_G_LOG_8 = 'S' 
	else
		Set @kCB_G_LOG_8 = 'N' 

	SET @kCB_G_NUM_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum1)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum2)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum3)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum4)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum5)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum6)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum7)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum8)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum9)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum10)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum11)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum12)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum13)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum14)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM15 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum15)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM16 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum16)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM17 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum17)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_NUM18 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGNum18)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	SET @kCB_G_TAB_1 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab1)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_2 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab2)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_3 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab3)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_4 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab4)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_5 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab5)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_6 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab6)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_7 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab7)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_8 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab8)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB_9 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab9)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB10 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab10)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB11 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab11)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB12 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab12)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB13 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab13)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TAB14 = coalesce( @Cargo.value('(//REGISTRO/@PersonaGTab14)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_G_TEX_1  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText1)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_2  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText2)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_3  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText3)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_4  = coalesce( @Cargo.value('(//REGISTRO/@PersonaCLABE)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_5  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText5)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_6  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText6)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_7  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText7)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_8  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText8)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX_9  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText9)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX10  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText10)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX11  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText11)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX12  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText12)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX13  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText13)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX14  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText14)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX15  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText15)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX16  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText16)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX17  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText17)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX18  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText18)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX19  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText19)[1]', 'Varchar(30)' ), '' ) -- as Descripcion		
	SET @kCB_G_TEX20  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText20)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX21  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText21)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX22  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText22)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX23  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText23)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_G_TEX24  = coalesce( @Cargo.value('(//REGISTRO/@PersonaGText24)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	SET @kCB_INFCRED = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfCred)[1]', 'Varchar(30)' ), '' ) --Credito Infonavit 
	---DECLARE @kCB_INFMANT as Booleano 
	SET	@kCB_INFMANT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfMant)[1]', 'INT' ), 0)	
	if ( @kCB_INFMANT_INT = 1 ) 
		Set @kCB_INFMANT = 'S' 
	else
		Set @kCB_INFMANT = 'N'

	SET @kCB_INFTASA = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfTasa)[1]', 'Decimal(15,2)' ), 0.00 ) --Valor Descuento Infonavit 
	
	SET @kCB_LAST_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaLastEv)[1]', 'Date' ), '1899-12-30' ) --Fecha de ultima evaluacion
	
	
	SET @kCB_FEC_INC = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecInc)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_FEC_PER = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPer)[1]', 'Date' ), '1899-12-30' )
	SET @kCB_NOMYEAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomYear)[1]', 'INT' ), 0)
	SET @kCB_NOMTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomTipo)[1]', 'INT' ), 0)
	

	SET @kCB_NEXT_EV = coalesce(@Cargo.value('(//REGISTRO/@PersonaNextEv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_NOMNUME = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0) -- as NominaNumero
	SET @kCB_SAL_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalInt)[1]', 'Decimal(15,2)' ), 0.00 ) -- as PesosDiario
	
	SET @kCB_INFTIPO = coalesce(@Cargo.value('(//REGISTRO/@PersonaNomNume)[1]', 'INT' ), 0)  -- as Status
	SET @kCB_DER_FEC = coalesce(@Cargo.value('(//REGISTRO/@PersonaDerFec)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PAG = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPag)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_PAGO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPago)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_DER_GOZ = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerGoz)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac
	SET @kCB_V_GOZO = coalesce( @Cargo.value('(//REGISTRO/@PersonaVGozo)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DiasFrac


	SET @kCB_TIP_REV = coalesce( @Cargo.value('(//REGISTRO/@PersonaTipRev)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_MOT_BAJ = coalesce( @Cargo.value('(//REGISTRO/@PersonaMotBaj)[1]', 'Varchar(3)' ), '' ) -- as Codigo3
	SET @kCB_FEC_SAL = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecSal)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_INI = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfIni)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_INF_OLD = coalesce( @Cargo.value('(//REGISTRO/@PersonaInfOld)[1]', 'Decimal(15,2)' ), 0.00 )  -- as DescINFO
	SET @kCB_OLD_SAL = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldSal)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_OLD_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaOldInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_PRE_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaPreInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_SAL_TOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaSalTot)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_TOT_GRA = coalesce( @Cargo.value('(//REGISTRO/@PersonaTotGra)[1]', 'Decimal(15,2)' ), 0.00 )  -- as PesosDiario
	SET @kCB_FAC_INT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFacInt)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	SET @kCB_RANGO_S = coalesce( @Cargo.value('(//REGISTRO/@PersonaRangoS)[1]', 'Decimal(15,2)' ), 0.00 )  -- as Tasa
	
	SET @kCB_FEC_NIV = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNiv)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_PTO = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPto)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_AREA = coalesce( @Cargo.value('(//REGISTRO/@PersonaArea)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	SET @kCB_FEC_TUR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecTur)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_FEC_KAR = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecKar)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_PENSION = coalesce(@Cargo.value('(//REGISTRO/@PersonaPension)[1]', 'INT' ), 0) -- as Status

	SET @kCB_CANDIDA = coalesce(@Cargo.value('(//REGISTRO/@PersonaCandida)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_ID_NUM = coalesce( @Cargo.value('(//REGISTRO/@PersonaIdNum)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	
	
	SET @kCB_PASSWRD = coalesce( @Cargo.value('(//REGISTRO/@PersonaPasswrd)[1]', 'Varchar(30)' ), '' ) -- as Passwd
	SET @kCB_PLAZA = coalesce(@Cargo.value('(//REGISTRO/@PersonaPlaza)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_FEC_PLA = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecPla)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	SET @kCB_DER_PV = coalesce( @Cargo.value('(//REGISTRO/@PersonaDerPv)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_V_PRIMA = coalesce( @Cargo.value('(//REGISTRO/@PersonaVPrima)[1]', 'Decimal(15,2)' ), 0.00 ) -- as DiasFrac
	SET @kCB_SUB_CTA = coalesce( @Cargo.value('(//REGISTRO/@PersonaSubCta)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_NETO = coalesce( @Cargo.value('(//REGISTRO/@PersonaNeto)[1]', 'Decimal(15,2)' ), 0.00 ) -- as Pesos
	
	--SET @kCB_PORTAL as Booleano
	SET	@kCB_PORTAL_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaPortal)[1]', 'INT' ), 0)	
	if ( @kCB_PORTAL_INT = 1 ) 
		Set @kCB_PORTAL = 'S' 
	else
		Set @kCB_PORTAL = 'N' 
	--SET @kCB_DNN_OK as Booleano
	SET	@kCB_DNN_OK_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaDnnOk)[1]', 'INT' ), 0)	
	if ( @kCB_DNN_OK_INT = 1 ) 
		Set @kCB_DNN_OK = 'S' 
	else
		Set @kCB_DNN_OK = 'N' 

	SET @kCB_USRNAME = coalesce( @Cargo.value('(//REGISTRO/@PersonaUsrname)[1]', 'Varchar(40)' ), '' ) -- as DescLarga
	
	SET @kCB_FEC_NOM = coalesce(@Cargo.value('(//REGISTRO/@PersonaFecNom)[1]', 'Date' ), '1899-12-30' ) -- as Fecha
	--SET @kCB_RECONTR as Booleano
	SET	@kCB_RECONTR_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaRecontr)[1]', 'INT' ), 0)	
	if ( @kCB_RECONTR_INT = 1 ) 
		Set @kCB_RECONTR = 'S' 
	else
		Set @kCB_RECONTR = 'N'

		
	SET @kCB_FONACOT = coalesce( @Cargo.value('(//REGISTRO/@PersonaFonacot)[1]', 'Varchar(30)' ), '' ) -- as Descripcion

	--SET @kCB_EMPLEO as Booleano
	SET	@kCB_EMPLEO_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaEmpleo)[1]', 'INT' ), 0)	
	if ( @kCB_EMPLEO_INT = 1 ) 
		Set @kCB_EMPLEO = 'S' 
	else
		Set @kCB_EMPLEO = 'N'

	SET @kUS_CODIGO = coalesce(@Cargo.value('(//REGISTRO/@PersonaUsCodigo)[1]', 'INT' ), 0) -- as Usuario
	
	--SET @kCB_INFDISM as Booleano
	SET	@kCB_INFDISM_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfDism)[1]', 'INT' ), 0)	
	if ( @kCB_INFDISM_INT = 1 ) 
		Set @kCB_INFDISM = 'S' 
	else
		Set @kCB_INFDISM = 'N'

	--SET @kCB_INFACT as Booleano
	SET	@kCB_INFACT_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAct)[1]', 'INT' ), 0)	
	if ( @kCB_INFACT_INT = 1 ) 
		Set @kCB_INFACT = 'S' 
	else
		Set @kCB_INFACT = 'N'
	
	SET @kCB_INF_ANT = coalesce(@Cargo.value('(//REGISTRO/@PersonaInfAnt)[1]', 'Date' ), '1899-12-30' ) -- as Fecha 
	
	SET @kCB_CTA_GAS = coalesce( @Cargo.value('(//REGISTRO/@PersonaCtaGas)[1]', 'Varchar(30)' ), '' ) -- as Descripcion
	SET @kCB_ID_BIO = coalesce(@Cargo.value('(//REGISTRO/@PersonaIdBio)[1]', 'INT' ), 0) -- as FolioGrande
	SET @kCB_GP_COD = coalesce( @Cargo.value('(//REGISTRO/@PersonaGpCod)[1]', 'Varchar(6)' ), '' ) -- as Codigo
	--SET @kCB_TIE_PEN as Booleano
	SET	@kCB_TIE_PEN_INT	= coalesce(@Cargo.value('(//REGISTRO/@PersonaTiePen)[1]', 'INT' ), 0)	
	if ( @kCB_TIE_PEN_INT = 1 ) 
		Set @kCB_TIE_PEN = 'S' 
	else
		Set @kCB_TIE_PEN = 'N'

	SET @kCB_TSANGRE = coalesce( @Cargo.value('(//REGISTRO/@PersonaTSangre)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_ALERGIA = coalesce( @Cargo.value('(//REGISTRO/@PersonaAlergia)[1]', 'Varchar(255)' ), '' ) -- as Formula

	--SET @kCB_BRG_ACT as Booleano
	SET	@kCB_BRG_ACT_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgAct)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ACT_INT = 1 ) 
		Set @kCB_BRG_ACT = 'S' 
	else
		Set @kCB_BRG_ACT = 'N'

	SET @kCB_BRG_TIP = coalesce(@Cargo.value('(//REGISTRO/@PersonaBgrTip)[1]', 'INT' ), 0) -- as Status
	--SET @kCB_BRG_ROL as Booleano
	SET	@kCB_BRG_ROL_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgRol)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_ROL_INT = 1 ) 
		Set @kCB_BRG_ROL = 'S' 
	else
		Set @kCB_BRG_ROL = 'N'

	--SET @kCB_BRG_JEF as Booleano
	SET	@kCB_BRG_JEF_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgJef)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_JEF_INT = 1 ) 
		Set @kCB_BRG_JEF = 'S' 
	else
		Set @kCB_BRG_JEF = 'N'

	--SET @kCB_BRG_CON as Booleano
	SET	@kCB_BRG_CON_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgCon)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_CON_INT = 1 ) 
		Set @kCB_BRG_CON = 'S' 
	else
		Set @kCB_BRG_CON = 'N'
	--SET @kCB_BRG_PRA as Booleano
	SET	@kCB_BRG_PRA_INT = coalesce(@Cargo.value('(//REGISTRO/@PersonaBrgPra)[1]', 'INT' ), 0)	
	if ( @kCB_BRG_PRA_INT = 1 ) 
		Set @kCB_BRG_PRA = 'S' 
	else
		Set @kCB_BRG_PRA = 'N'

	SET @kCB_BRG_NOP = coalesce( @Cargo.value('(//REGISTRO/@PersonaBgrNop)[1]', 'Varchar(6)' ), '' ) -- as Descripcion
	SET @kCB_BANCO = coalesce( @Cargo.value('(//REGISTRO/@PersonaBanco)[1]', 'Varchar(6)' ), @kCB_BANCO ) -- as Codigo
	
	
    if ( @ErrorID = 0 ) 
    begin 

	
	-- Si ya existe el empleado con los mismos nombres, rfc, fecha de Persona y correo  define el mismo numero de empleado 
	-- para que la validacion la realice el WFTress.dll 

	set @kCB_SALARIO_BRUTO = 0 
    if ( (@EmpNumero <> 0) and (@IngresoReingreso = 1)) 
    begin 
         set @kCB_CODIGO = @EmpNumero
         set @MOV3 = 'REINGRESO_EMPLEADO' 
    end
    else 
    begin 
        set @MOV3 = 'ALTA_EMPLEADO' 
		set @kCB_CODIGO = 0 

		select @kCB_CODIGO = CB_CODIGO from COLABORA  where CB_NOMBRES = @kCB_NOMBRES and CB_APE_MAT = @kCB_APE_MAT and CB_APE_PAT = @kCB_APE_PAT 
													and CB_FEC_ING = @kCB_FEC_ING and CB_E_MAIL  = @kCB_E_MAIL  and CB_RFC     = @kCB_RFC

		set @kCB_CODIGO = coalesce (@kCB_CODIGO ,  0 ) 

		-- Se obtiene el numero de empleado mayor + 1 
		if ( @kCB_CODIGO = 0 ) 
			SELECT @kCB_CODIGO = CB_CODIGO + 1  FROM dbo.ULTIMO_EMPLEADO
	 end 

	 set @Xml =(	 

	   select @MOV3 as MOV3, 
			@EMPRESA as EMPRESA,  ( 
	   select * from  ( 
		 SELECT 		
		        @PersonaID  as IngresoID , 
				@kCB_CODIGO  as CB_CODIGO  , 
				'S'  as CB_ACTIVO,		
				@kCB_AUTOSAL as CB_AUTOSAL , --Booleano
				@kCB_APE_MAT as CB_APE_MAT , --Descripcion
				@kCB_APE_PAT as CB_APE_PAT , --Descripcion
				@kCB_NOMBRES as CB_NOMBRES , --Descripcion
				--@kCB_BAN_ELE as CB_BAN_ELE , --Descripcion Se Quita por peticion de Roch
				@kCB_CALLE as CB_CALLE , --Observaciones
				@kCB_CARRERA as CB_CARRERA , --DescLarga
				@kCB_CHECA as CB_CHECA , --Booleano
				@kCB_CIUDAD as CB_CIUDAD , --Descripcion
				@kCB_CLASIFI as CB_CLASIFI , --Codigo
				@kCB_CODPOST as CB_CODPOST , --Referencia
				@kCB_COLONIA as CB_COLONIA , --Descripcion
				@kCB_CONTRAT as CB_CONTRAT , --Codigo1
				@kCB_CREDENC as CB_CREDENC , --Codigo1
				@kCB_CTA_VAL as CB_CTA_VAL , --Descripcion
				@kCB_CURP as CB_CURP , --Descripcion
				@kCB_E_MAIL as CB_E_MAIL , --Formula
				@kCB_ESTADO as CB_ESTADO , --Codigo2
				@kCB_ESTUDIO as CB_ESTUDIO , --Codigo2
				convert( varchar(20), @kCB_FEC_ANT , 112 )  as CB_FEC_ANT , --Fecha
				convert( varchar(20), @kCB_FEC_CON , 112 )  as CB_FEC_CON , --Fecha
				convert( varchar(20), @kCB_FEC_COV , 112 )  as CB_FEC_COV , --Fecha
				convert( varchar(20), @kCB_FEC_ING , 112 )  as CB_FEC_ING , --Fecha
				convert( varchar(20), @kCB_FEC_NAC , 112 )  as CB_FEC_NAC , --Fecha
				@kCB_LUG_NAC as CB_LUG_NAC , --Descripcion
				@kCB_MUNICIP as CB_MUNICIP , --Codigo
				@kCB_NACION as CB_NACION , --Descripcion
				@kCB_NIVEL0 as CB_NIVEL0 , --Codigo
				@kCB_NIVEL1 as CB_NIVEL1 , --Codigo
				@kCB_NIVEL2 as CB_NIVEL2 , --Codigo
				@kCB_NIVEL3 as CB_NIVEL3 , --Codigo
				@kCB_NIVEL4 as CB_NIVEL4 , --Codigo
				@kCB_NIVEL5 as CB_NIVEL5 , --Codigo
				@kCB_NIVEL6 as CB_NIVEL6 , --Codigo
				@kCB_NIVEL7 as CB_NIVEL7 , --Codigo
				@kCB_NIVEL8 as CB_NIVEL8 , --Codigo
				@kCB_NIVEL9 as CB_NIVEL9 , --Codigo
				@kCB_PATRON as CB_PATRON , --RegPatronal
				@kCB_PUESTO as CB_PUESTO , --Codigo
				@kCB_RFC as CB_RFC , --Descripcion
				@kCB_SALARIO as CB_SALARIO , --PesosDiario
				@kCB_SALARIO_BRUTO as CB_SALARIO_BRUTO , --PesosDiario Salario Bruto Mensual
				@kCB_SEGSOC as CB_SEGSOC , --Descripcion
				@kCB_SEXO as CB_SEXO , --Codigo1
				@kCB_TABLASS as CB_TABLASS , --Codigo1
				@kCB_TEL as CB_TEL , --Descripcion
				@kCB_TURNO as CB_TURNO , --Codigo
				@kCB_ZONA_GE as CB_ZONA_GE , --ZonaGeo										
				---Nuevos Campos Mapeados
				@kCB_EDO_CIV as CB_EDO_CIV , -- Codigo1
				convert( varchar(20), @kCB_FEC_RES , 112 )  as CB_FEC_RES , -- Fecha
				@kCB_EST_HOR as CB_EST_HOR , -- Descripcion
				@kCB_EST_HOY as CB_EST_HOY , -- Booleanos
				@kCB_EVALUA as CB_EVALUA , -- DiasFrac
				@kCB_EXPERIE as CB_EXPERIE , -- Titulo
				convert( varchar(20), @kCB_FEC_BAJ , 112 )    as CB_FEC_BAJ , -- Fecha
				convert( varchar(20), @kCB_FEC_BSS , 112 )    as CB_FEC_BSS , -- Fecha
				convert( varchar(20), @kCB_FEC_INT , 112 )    as CB_FEC_INT , -- Fecha
				convert( varchar(20), @kCB_FEC_REV , 112 )    as CB_FEC_REV , -- Fecha
				convert( varchar(20), @kCB_FEC_VAC , 112 )    as CB_FEC_VAC , -- Fecha
				@kCB_G_FEC_1 as CB_G_FEC_1 , -- Fecha
				@kCB_G_FEC_2 as CB_G_FEC_2 , -- Fecha
				@kCB_G_FEC_3 as CB_G_FEC_3 , -- Fecha
				@kCB_G_FEC_4 as CB_G_FEC_4 , -- Fecha
				@kCB_G_FEC_5 as CB_G_FEC_5 , -- Fecha
				@kCB_G_FEC_6 as CB_G_FEC_6 , -- Fecha
				@kCB_G_FEC_7 as CB_G_FEC_7 , -- Fecha
				@kCB_G_FEC_8 as CB_G_FEC_8 , -- Fecha
				@kCB_G_LOG_1 as CB_G_LOG_1 , -- Booleano
				@kCB_G_LOG_2 as CB_G_LOG_2 , -- Booleano
				@kCB_G_LOG_3 as CB_G_LOG_3 , -- Booleano
				@kCB_G_LOG_4 as CB_G_LOG_4 , -- Booleano
				@kCB_G_LOG_5 as CB_G_LOG_5 , -- Booleano
				@kCB_G_LOG_6 as CB_G_LOG_6 , -- Booleano
				@kCB_G_LOG_7 as CB_G_LOG_7 , -- Booleano
				@kCB_G_LOG_8 as CB_G_LOG_8 , -- Booleano
				@kCB_G_NUM_1 as CB_G_NUM_1 , -- Pesos
				@kCB_G_NUM_2 as CB_G_NUM_2 , -- Pesos
				@kCB_G_NUM_3 as CB_G_NUM_3 , -- Pesos
				@kCB_G_NUM_4 as CB_G_NUM_4 , -- Pesos
				@kCB_G_NUM_5 as CB_G_NUM_5 , -- Pesos
				@kCB_G_NUM_6 as CB_G_NUM_6 , -- Pesos
				@kCB_G_NUM_7 as CB_G_NUM_7 , -- Pesos
				@kCB_G_NUM_8 as CB_G_NUM_8 , -- Pesos
				@kCB_G_NUM_9 as CB_G_NUM_9 , -- Pesos
				@kCB_G_NUM10 as CB_G_NUM10 , -- Pesos
				@kCB_G_NUM11 as CB_G_NUM11 , -- Pesos
				@kCB_G_NUM12 as CB_G_NUM12 , -- Pesos
				@kCB_G_NUM13 as CB_G_NUM13 , -- Pesos
				@kCB_G_NUM14 as CB_G_NUM14 , -- Pesos
				@kCB_G_NUM15 as CB_G_NUM15 , -- Pesos
				@kCB_G_NUM16 as CB_G_NUM16 , -- Pesos
				@kCB_G_NUM17 as CB_G_NUM17 , -- Pesos
				@kCB_G_NUM18 as CB_G_NUM18 , -- Pesos
				@kCB_G_TAB_1 as CB_G_TAB_1 , -- Codigo
				@kCB_G_TAB_2 as CB_G_TAB_2 , -- Codigo
				@kCB_G_TAB_3 as CB_G_TAB_3 , -- Codigo
				@kCB_G_TAB_4 as CB_G_TAB_4 , -- Codigo
				@kCB_G_TAB_5 as CB_G_TAB_5 , -- Codigo
				@kCB_G_TAB_6 as CB_G_TAB_6 , -- Codigo
				@kCB_G_TAB_7 as CB_G_TAB_7 , -- Codigo
				@kCB_G_TAB_8 as CB_G_TAB_8 , -- Codigo
				@kCB_G_TAB_9 as CB_G_TAB_9 , -- Codigo
				@kCB_G_TAB10 as CB_G_TAB10 , -- Codigo
				@kCB_G_TAB11 as CB_G_TAB11 , -- Codigo
				@kCB_G_TAB12 as CB_G_TAB12 , -- Codigo
				@kCB_G_TAB13 as CB_G_TAB13 , -- Codigo
				@kCB_G_TAB14 as CB_G_TAB14 , -- Codigo
				@kCB_G_TEX_1 as CB_G_TEX_1 , -- Descripcion
				@kCB_G_TEX_2 as CB_G_TEX_2 , -- Descripcion
				@kCB_G_TEX_3 as CB_G_TEX_3 , -- Descripcion
				@kCB_G_TEX_4 as CB_G_TEX_4 , -- Descripcion
				@kCB_G_TEX_5 as CB_G_TEX_5 , -- Descripcion
				@kCB_G_TEX_6 as CB_G_TEX_6 , -- Descripcion
				@kCB_G_TEX_7 as CB_G_TEX_7 , -- Descripcion
				@kCB_G_TEX_8 as CB_G_TEX_8 , -- Descripcion
				@kCB_G_TEX_9 as CB_G_TEX_9 , -- Descripcion
				@kCB_G_TEX10 as CB_G_TEX10 , -- Descripcion
				@kCB_G_TEX11 as CB_G_TEX11 , -- Descripcion
				@kCB_G_TEX12 as CB_G_TEX12 , -- Descripcion
				@kCB_G_TEX13 as CB_G_TEX13 , -- Descripcion
				@kCB_G_TEX14 as CB_G_TEX14 , -- Descripcion
				@kCB_G_TEX15 as CB_G_TEX15 , -- Descripcion
				@kCB_G_TEX16 as CB_G_TEX16 , -- Descripcion
				@kCB_G_TEX17 as CB_G_TEX17 , -- Descripcion
				@kCB_G_TEX18 as CB_G_TEX18 , -- Descripcion
				@kCB_G_TEX19 as CB_G_TEX19 , -- Descripcion
				@kCB_HABLA as CB_HABLA , -- Booleano
				@kCB_IDIOMA as CB_IDIOMA , -- Descripcion
				@kCB_INFCRED as CB_INFCRED , -- Descripcion
				@kCB_INFMANT as CB_INFMANT , -- Booleano
				@kCB_INFTASA as CB_INFTASA , -- DescINFO
				@kCB_LA_MAT as CB_LA_MAT , -- Descripcion
				@kCB_LAST_EV as CB_LAST_EV , -- Fecha
				@kCB_MAQUINA as CB_MAQUINA , -- Titulo
				@kCB_MED_TRA as CB_MED_TRA , -- Codigo1
				@kCB_PASAPOR as CB_PASAPOR , -- Booleano				
				@kCB_VIVECON as CB_VIVECON , -- Codigo1
				@kCB_VIVEEN as CB_VIVEEN , -- Codigo2
				@kCB_ZONA as CB_ZONA , -- Referencia
				@kCB_TIP_REV as CB_TIP_REV , -- Codigo				
				@kCB_INF_INI as CB_INF_INI , -- Fecha
				@kCB_INF_OLD as CB_INF_OLD , -- DescINFO
				@kCB_OLD_SAL as CB_OLD_SAL , -- PesosDiario
				@kCB_OLD_INT as CB_OLD_INT , -- PesosDiario
				@kCB_PRE_INT as CB_PRE_INT , -- PesosDiario
				@kCB_PER_VAR as CB_PER_VAR , -- PesosDiario
				@kCB_SAL_TOT as CB_SAL_TOT , -- PesosDiario
				@kCB_TOT_GRA as CB_TOT_GRA , -- PesosDiario
				@kCB_FAC_INT as CB_FAC_INT , -- Tasa
				@kCB_RANGO_S as CB_RANGO_S , -- Tasa
				@kCB_CLINICA as CB_CLINICA , -- Codigo3																
				@kCB_CANDIDA as CB_CANDIDA , -- FolioGrande
				@kCB_ID_NUM as CB_ID_NUM , -- Descripcion
				@kCB_ENT_NAC as CB_ENT_NAC , -- Codigo2
				@kCB_COD_COL as CB_COD_COL , -- Codigo								
				@kCB_DER_PV as CB_DER_PV , -- DiasFrac
				@kCB_V_PRIMA as CB_V_PRIMA , -- DiasFrac
				@kCB_SUB_CTA as CB_SUB_CTA , -- Descripcion
				@kCB_NETO as CB_NETO , -- Pesos
				@kCB_NOMINA as CB_NOMINA , -- NominaTipo
				@kCB_RECONTR as CB_RECONTR , -- Booleano
				@kCB_DISCAPA as CB_DISCAPA , -- Booleano
				@kCB_INDIGE as CB_INDIGE , -- Booleano
				@kCB_FONACOT as CB_FONACOT , -- Descripcion
				@kCB_EMPLEO as CB_EMPLEO , -- Booleano								
				@kCB_G_TEX20 as CB_G_TEX20 , -- Descripcion
				@kCB_G_TEX21 as CB_G_TEX21 , -- Descripcion
				@kCB_G_TEX22 as CB_G_TEX22 , -- Descripcion
				@kCB_G_TEX23 as CB_G_TEX23 , -- Descripcion
				@kCB_G_TEX24 as CB_G_TEX24 , -- Descripcion
				@kCB_INFDISM as CB_INFDISM , -- Booleano
				@kCB_INFACT as CB_INFACT , -- Booleano
				@kCB_NUM_EXT as CB_NUM_EXT , -- NombreCampo
				@kCB_NUM_INT as CB_NUM_INT , -- NombreCampo
				@kCB_INF_ANT as CB_INF_ANT , -- Fecha
				@kCB_TDISCAP as CB_TDISCAP , -- Status
				@kCB_ESCUELA as CB_ESCUELA , -- DescLarga
				@kCB_TESCUEL as CB_TESCUEL , -- Status
				@kCB_TITULO as CB_TITULO , -- Status
				@kCB_YTITULO as CB_YTITULO , -- Anio
				@kCB_CTA_GAS as CB_CTA_GAS , -- Descripcion
				@kCB_ID_BIO as CB_ID_BIO , -- FolioGrande
				@kCB_GP_COD as CB_GP_COD , -- Codigo				
				@kCB_TSANGRE as CB_TSANGRE , -- Descripcion
				@kCB_ALERGIA as CB_ALERGIA , -- Formula
				@kCB_BRG_ACT as CB_BRG_ACT , -- Booleano
				@kCB_BRG_TIP as CB_BRG_TIP , -- Status
				@kCB_BRG_ROL as CB_BRG_ROL , -- Booleano
				@kCB_BRG_JEF as CB_BRG_JEF , -- Booleano
				@kCB_BRG_CON as CB_BRG_CON , -- Booleano
				@kCB_BRG_PRA as CB_BRG_PRA , -- Booleano
				@kCB_BRG_NOP as CB_BRG_NOP , -- Descripcion
				@kCB_BANCO as CB_BANCO ,	-- Codigo 
				@kCB_REGIMEN as CB_REGIMEN -- Status

		   )  REGISTRO    FOR XML AUTO, TYPE 
		   )   REGISTROS  FOR XML PATH('DATOS'), TYPE  
		   )  
		 	
		SET @ErrorID = 0 
		set @Error = '' 
	end
	
	-- Solo pruebas
	UPDATE T_Wftransacciones set TransCargoWFTress = @Xml where TransID = @transID 

	return @ErrorID
end
GO

CREATE PROCEDURE WFTransaccion_GetXMLTRESS(
	@TransID	INT,
	@Xml		XML OUTPUT , 
	@FileID		INT OUTPUT , 	
	@Error		VARCHAR( 255 ) OUTPUT , 
	@MultiplesFileID  VARCHAR(Max) OUTPUT )
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE	@ObjetoID	INT
	DECLARE @ErrorID	INT
	
	SET ARITHABORT OFF
	
	SELECT	@VerboID	= VerboID, @ObjetoID  = ObjetoID 
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
	SET @ErrorID = 0 
	SET @FileID = 0 
	SET @MultiplesFileID  = '' 

	IF ( @VerboID = 1312 )
		EXEC @ErrorID = WFTransaccion_GetXMLVacaciones @TransID, @Xml output , @FileID output,  @Error output 
	ELSE
	IF ( @VerboID = 1304 )
		EXEC @ErrorID = WFTransaccion_GetXMLPermisos @TransID, @Xml output , @FileID output,  @Error output 		
	Else
	IF ( @VerboID = 67 ) and ( @ObjetoID = 1090 ) 
		EXEC @ErrorID = WFTransaccion_GetXMLAutorizarExtras @TransID, @Xml output , @FileID output,  @Error output 		
	Else	
	IF ( @VerboID = 1239 ) and ( @ObjetoID = 1060 ) 
		EXEC @ErrorID = WFTransaccion_GetXMLDocumentos @TransID, @Xml output , @FileID output,  @Error output , @MultiplesFileID output 	
	Else
	IF ( @VerboID = 1341 ) and ( @ObjetoID = 1068 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLDatosEmpleado @TransID, @Xml output , @FileID output,  @Error output 	
	Else	
	IF ( @VerboID = 1490 ) and ( @ObjetoID = 1107 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLInfonavit @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1077 ) and ( @ObjetoID = 1308 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLIncapacidades @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1469 ) and ( @ObjetoID = 1075 ) 
		EXEC @ErrorID =  WFTransaccion_GetXMLPrestamos @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 86) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLPuestoClasificacion @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 94) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioTurno @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 150) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioEquipo @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1282) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLBajaEmpleado @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 90) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioSalario @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else
	IF ( @VerboID = 106) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioAreas @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else
	IF ( @VerboID = 98) and ( @ObjetoID = 1068) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCambioContrato @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else
	IF ( @VerboID = 1285) and ( @ObjetoID = 1071) 
		EXEC @ErrorID =  WFTransaccion_GetXMLCRUDParientes @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output 		
	Else	
	IF ( @VerboID = 1456 ) and ( @ObjetoID = 1096 ) 
		EXEC @ErrorID = WFTransaccion_GetXMLExcepciones @TransID, @Xml output , @FileID output,  @Error output 	
	Else
	IF ( @VerboID = 2085) and ( @ObjetoID = 1213) 
            EXEC @ErrorID =  WFTransaccion_GetXMLAgregarAsegurado @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output           
        Else
	IF ( @VerboID = 100269) and ( @ObjetoID = 1051) 
            EXEC @ErrorID =  WFTransaccion_GetXMLIntercambioFestivo @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output        
	Else 
 	IF ( @VerboID = 2138) and ( @ObjetoID = 1222) 
            EXEC @ErrorID =  WFTransaccion_GetXMLChecadas @TransID, @Xml output , @FileID output,  @Error output  , @MultiplesFileID output        
	Else  
	IF ( @VerboID = 1281) and ( @ObjetoID = 1068 ) 
            EXEC @ErrorID =  WFTransaccion_GetXMLAltaEmpleadoGenerica @TransID, @Xml output , @FileID output,  @Error output 
        Else
        IF ( @VerboID = 80) and ( @ObjetoID = 1068 ) 
            EXEC @ErrorID =  WFTransaccion_GetXMLReingresoEmpleado @TransID, @Xml output , @FileID output,  @Error output 
        Else    
	begin 
		set @Xml = '' -- No aplica para el Transaccion / no ES ERROR 
		set @Error = '' 
	end 
	

	RETURN @ErrorID
END
GO

create procedure WFFile_GrabaDocumentoInfonavit(@File Imagen, @Nombre Descripcion , @Ext Codigo, @CB_CODIGO NumeroEmpleado, @KI_FECHA Fecha, @KI_TIPO Status,  @Usuario Usuario) 
as 
begin 
	set nocount on 
					
	update KARINF set KI_D_EXT = @Ext, KI_D_NOM = @Nombre, KI_D_BLOB = @File 
	where CB_CODIGO = @CB_CODIGO and KI_FECHA = @KI_FECHA  
		
end
go 

create procedure WFFile_GrabaFoto(@File Imagen, @CB_CODIGO NumeroEmpleado, @Usuario Usuario) 
as 
begin 
	set nocount on 		
	
	update IMAGEN set IM_BLOB = @File, IM_OBSERVA = 'Actualizada por WFN' where CB_CODIGO = @CB_CODIGO  and IM_TIPO = 'FOTO' 

	if ( @@ROWCOUNT = 0  ) 
	begin 
		insert into IMAGEN ( CB_CODIGO,  IM_TIPO, IM_BLOB, IM_OBSERVA ) values ( @CB_CODIGO, 'FOTO' , @File, 'Ingresada por WFN' ) 
	end 	
		
end
go 


CREATE PROCEDURE WFFile_GrabaTRESS(
	@TransID	INT,	
	@FileID		INT, 	
	@File		Imagen, 	
	@Nombre		Formula, 
	@Ext		Codigo,	
	@Usuario	Usuario, 
	@Error		VARCHAR( 255 ) OUTPUT)
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE	@ObjetoID	INT
	DECLARE @ErrorID	INT
	DECLARE @Cargo	XML

	DECLARE @EMPRESA  varchar(10) 
	DECLARE @CB_CODIGO	INT	
	DECLARE @DocumentoNombre Varchar(255)
	declare @ClienteID FolioGrande 
	
	SET ARITHABORT OFF
	
	
	SELECT	@VerboID	= VerboID, @ObjetoID  = ObjetoID,  
	@Cargo	= TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
	SET @ErrorID = 0 	
	
	SET @Ext =  RTRIM( REPLACE( @Ext, '.', '' ) ) 

	IF ( @VerboID = 1312 )
	begin 
	
		SET	@ClienteID	= coalesce(  @Cargo.value('(//REGISTRO/@ClienteID)[1]', 'INT' ), 0) 
		SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 
		SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 
		
		declare @hoy Fecha 
		declare @fecha Fecha 
		declare @hora Hora
		declare @ClaseDoc Codigo 
		
		set @ClaseDoc = '017'
		set @hoy = GETDATE()
		set @fecha = DATEADD(dd, 0, DATEDIFF(dd, 0,@hoy))
		set @hora =  REPLACE( convert(varchar(5), @hoy , 108), ':', '' ) 		
		
		insert into DOCUMENTO (CB_CODIGO, DO_TIPO, DO_BLOB, DO_NOMBRE, DO_EXT ) 
		values ( @CB_CODIGO, 'WF'+@EMPRESA, @File, @Nombre, @Ext ) 
										
	end 	
	else
	if ( @VerboID = 1490 ) and ( @ObjetoID = 1107 ) 
	begin 

		declare @InfoFecha Fecha 
		declare @InfoTipo  Status 

		SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 					
		select 
				@CB_CODIGO		 = x.value(N'@EmpNumero', N'int') , 				
				@ClienteID		 = x.value(N'@ClienteID', N'int'), 
				@InfoFecha        = x.value(N'@InfoFecha', N'Datetime'), 
				@InfoTipo =		   x.value(N'@InfoTipo', N'int')
		from @Cargo.nodes(N'//REGISTRO[@FileID=sql:variable("@FileID")]') t(x)						

		SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 

		set @Nombre = coalesce( @Nombre, '' ) 
		set @Nombre = left( @Nombre, 30 ) 
	
		exec WFFile_GrabaDocumentoInfonavit @File, @Nombre, @Ext, @CB_CODIGO, @InfoFecha, @InfoTipo, @Usuario 		
	end 
	else
	begin
		-- Se asume que es el cargo de carga de documento  
		SET	@CB_CODIGO	= coalesce(  @Cargo.value('(//REGISTRO/@EmpNumero)[1]', 'INT' ), 0) 	
		declare @TipoDocCodigo varchar(10) 
		
		select 
				@CB_CODIGO		 = x.value(N'@EmpNumero', N'int') , 
				@TipoDocCodigo	 = x.value(N'@TipoDocCodigo', N'varchar(10)') , 
				@ClienteID		 = x.value(N'@ClienteID', N'int'), 
				@Ext             = x.value(N'@DocumentoExt', N'varchar(6)'), 
				@DocumentoNombre = x.value(N'@DocumentoNombre', N'varchar(255)')
		from @Cargo.nodes(N'//REGISTRO[@FileID=sql:variable("@FileID")]') t(x)
						

		SET @EMPRESA	= dbo.WFEmpresas_GetCodigoEmpresa( @ClienteID ) ; 


		        
		if ( @TipoDocCodigo = 'FOTO'  ) 
		begin 
			exec WFFile_GrabaFoto @File, @CB_CODIGO, @Usuario
		end
		else			
		begin
			insert into DOCUMENTO (CB_CODIGO, DO_TIPO, DO_BLOB, DO_NOMBRE, DO_EXT ) 
			values ( @CB_CODIGO, @TipoDocCodigo, @File, @DocumentoNombre, @Ext ) 
		end
			
	end 

	RETURN @ErrorID
END
GO 


CREATE PROCEDURE WFTransaccion_GetXMLPlugin(
	@TransID	INT,
	@Xml		XML OUTPUT , 
	@Plugin		VARCHAR(1024) OUTPUT, 
	@FileID		INT OUTPUT , 	
	@Error		VARCHAR( 255 ) OUTPUT)
AS
BEGIN
	DECLARE	@VerboID	INT
	DECLARE @ErrorID	INT
	
	SET ARITHABORT OFF
	
	SELECT	@VerboID	= VerboID,  @Xml = TransCargo
	FROM	T_WFTransacciones
	WHERE	TransID = @TransID
	
	SET @ErrorID = 0 
	SET @FileID = 0 
	SET @Error = '' 

    IF ( @VerboID = 9998 )	
		set  @Plugin = 'WFTest.dll'
	ELSE
	IF ( @VerboID = 10000 )
		set  @Plugin = 'WFCompaq.dll'
	Else	
	begin 
		set @Xml = '<DATOS/>'
		set @Plugin = '' 
		set @Error = 'No existe plugin correspondiente al Verbo  #'+ RTRIM ( LTRIM( STR( @VerboID ) ) ) 
		set @ErrorID = 100
	end
	

	RETURN @ErrorID
END
GO

CREATE procedure KardexConceptos_TomarFoto
as
begin 
	set nocount on 
	declare @verTotal FolioGrande 
	
	set @verTotal = dbo.KardexConceptos_GetVersionActual() 

	set @verTotal = @verTotal + 1 
	
	declare @hoy Fecha 
	set @hoy = GETDATE() 
			
	insert into CONCEPTOVT (KT_VERSION, CO_NUMERO, KC_VERSION, KT_FECHA ) 
	select @verTotal, CO_NUMERO, KC_VERSION, @hoy from CONCEPTOVR

end
GO

CREATE procedure KardexConceptos_NotifyUpdate( @CO_NUMERO Concepto, @US_CODIGO Usuario, @US_FEC_MOD Fecha , @lTomarFoto Booleano = 'S' ) 
as
begin 
	set nocount on 
	
	declare @KC_VERSION FolioGrande 

	if (@CO_NUMERO >= 1000 ) 
		return 
	

	select @KC_VERSION = KC_VERSION from CONCEPTOVR where CO_NUMERO = @CO_NUMERO 
	set @KC_VERSION = coalesce( @KC_VERSION, 0 ) 
	
	set @KC_VERSION = @KC_VERSION + 1 
		
	insert into KAR_CONCEP ( CO_NUMERO, KC_VERSION,  US_CODIGO, US_FEC_MOD
	  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
      ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
      ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
      ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
      ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
      ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
      ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
      ,CO_PS_TIPO)
	select 
	   CO_NUMERO, @KC_VERSION,  @US_CODIGO, @US_FEC_MOD
	  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
      ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
      ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
      ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
      ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
      ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
      ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
      ,CO_PS_TIPO
	from CONCEPTO where CO_NUMERO = @CO_NUMERO 

	update CONCEPTOVR set KC_VERSION = @KC_VERSION, US_CODIGO = @US_CODIGO, US_FEC_MOD = @US_FEC_MOD where CO_NUMERO = @CO_NUMERO 

	if @@ROWCOUNT = 0 
		insert into CONCEPTOVR (CO_NUMERO, KC_VERSION , US_CODIGO, US_FEC_MOD) values ( @CO_NUMERO, @KC_VERSION, @US_CODIGO, @US_FEC_MOD)  



	if @lTomarFoto = 'S' 
		exec KardexConceptos_TomarFoto
end;

go

CREATE  PROCEDURE SP_INICIAR_KARDEX_CONCEPTOS
AS
BEGIN 
		select row_number() over ( order by CO_NUMERO ) i, CO_NUMERO into #ListaConceptos from CONCEPTO where CO_NUMERO < 1000 
		declare @i int 
		declare @n  int 
		declare @CO_NUMERO Concepto 
		declare @Fecha Fecha 

		set @i = 1 

		select @n= COUNT(*) from #ListaConceptos

		set @Fecha = GETDATE() 

		while @i <=  @n 
		begin 
			select @CO_NUMERO = CO_NUMERO from #ListaConceptos where i = @i 
			if (@CO_NUMERO > 0 ) 
				exec KardexConceptos_NotifyUpdate @CO_NUMERO, 1, @Fecha, 'N' 	

			set @i = @i  + 1 
		end 

		exec KardexConceptos_TomarFoto; 
END
GO

create procedure TimbradoConcilia_Conciliar( 	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero, 
	@Xml		nVarchar(max) ,
	@US_CODIGO	Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_PROC_ID FolioGrande,
	@Error		VARCHAR( 255 ) OUTPUT
)
as
begin 
	set @Error = '' 
---	parse XML 
--- por cada elemento va a existir una execucion 
--- TimbradoConcilia_ConciliarUnaNomina

	declare @XMLDoc XML 
	set @XMLDoc = convert ( XML, @XML  ) 
	
	
	select row_number() over (order by Numero) i,  Numero, CURP, Status, Result, ResultDesc, VersionCodigo, VersionDesc, UUID, FacturaID, FechaTimbre into #TmpNominasConcilia
	from ( 
	select            
            Numero    = coalesce(  t.c.value('(@Numero)', 'INT' ), 0) ,
            CURP        = coalesce(  t.c.value('(@CURP)', 'Varchar(50)' ), '' ),             
            Status   = coalesce(t.c.value('(@Status)', 'INT' ), 0),
            Result   = coalesce(t.c.value('(@Result)', 'INT' ), 0),            
			ResultDesc        = coalesce(  t.c.value('(@ResultDesc)', 'Varchar(256)' ), '' ),             
			VersionCodigo        = coalesce(  t.c.value('(@Version)', 'Varchar(50)' ), '' ),    
			VersionDesc    = coalesce(  t.c.value('(@VersionDesc)', 'Varchar(50)' ), '' ),    
			         
			UUID        = coalesce(  t.c.value('(@UUID)', 'Varchar(50)' ), '' ) , 
			FacturaID	=  coalesce(t.c.value('(@FacturaID)', 'INT' ), 0),    
			FechaTimbre =  coalesce(t.c.value('(@FechaTimbre)', 'Datetime' ), '1899-12-30')
     from 
     @XMLDoc.nodes('//RESPUESTA/SALIDA/NOMINAS/NOMINA') t(c)
	 cross apply @XMLDoc.nodes('//RESPUESTA/RESULTADO') tr(cr)
	 ) TmpNominasConcilia 

	
	delete from NOM_CONCIL 
	where PE_YEAR= @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO 
	and CB_CODIGO in  ( select Numero from #TmpNominasConcilia ) 

 
	insert NOM_CONCIL (
			PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			NC_CURP,
			NC_STATUS,
			NC_RESULT,
			NC_RES_DESC,
			NC_VERSION,
			NC_UUID, 
			US_CODIGO, 
			US_FEC_MOD,
			NC_APLICADO, 
			NC_VER_COD, 
			NC_FEC_TIM, 
			NC_FACTURA 
		)
	select @PE_YEAR, @PE_TIPO, @PE_NUMERO, Numero, CURP, Status, Result, ResultDesc, VersionDesc, UUID, @US_CODIGO, @US_FEC_MOD, 
		case
			when Result = 0 then 'S'  
			else 'N' 
		end, 
		VersionCodigo, FechaTimbre, FacturaID
    from #TmpNominasConcilia
	left join VTIMCONCIL TC on TC.PE_YEAR = @PE_YEAR and TC.PE_TIPO = @PE_TIPO and TC.PE_NUMERO = @PE_NUMERO and TC.CB_CODIGO = Numero


	drop table #TmpNominasConcilia

	return 
end 

GO 

create procedure TimbradoConcilia_BitacoraTimbrar(
	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero,
	@CB_CODIGO NumeroEmpleado,
	@US_CODIGO Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_NUMERO  FolioGrande, 
	@NO_TIMBRO	Status, 
	@NO_FACTURA FolioGrande, 
	@NO_FACUUID	DescLarga,
	@NO_FACUUIDAnterior	DescLarga
) as
begin 
	DECLARE @BI_TIPO Status
	DECLARE @BI_PROC_ID Status	
	DECLARE @BI_TEXTO Observaciones	
	DECLARE @BI_DATA varchar(max) 
	DECLARE @TipoNominaStr Descripcion 
	DECLARE @BI_CLASE Status
	DECLARE @BI_FEC_MOV Fecha

set @BI_TIPO = 0  /* Informativo */ 
SET @BI_CLASE = 0 
select @BI_PROC_ID = PC_PROC_ID from PROCESO where PC_NUMERO = @PC_NUMERO
set @BI_FEC_MOV = @US_FEC_MOD 
	
select @TipoNominaStr = TP_NOMBRE from TPERIODO where TP_TIPO = @PE_TIPO 
set @TipoNominaStr = coalesce( @TipoNominaStr, 'Periodo')

--@NO_TIMBRO
set @BI_DATA = '' 
if ( @NO_TIMBRO = 2 ) 
begin 
	select  @BI_TEXTO = 'Nuevo estatus: Timbrado, Empleado: '+cast( @CB_CODIGO as varchar(20)) 
		select  @BI_DATA =  @BI_TEXTO + ' , A�o:'+cast( @PE_YEAR as varchar(20)) + ' Numero:'+cast( @PE_NUMERO as varchar(20))+' Tipo: '+@TipoNominaStr +
		' UUID: '+ @NO_FACUUID + ' UUID Anterior: '+ @NO_FACUUIDAnterior
		
	end 
	if ( @NO_TIMBRO = 0 ) 
	begin 
		select  @BI_TEXTO = 'Nuevo estatus: Pendiente, Empleado: '+cast( @CB_CODIGO as varchar(20))
		select  @BI_DATA = @BI_TEXTO + ' , A�o:'+cast( @PE_YEAR as varchar(20)) + ' Numero:'+cast( @PE_NUMERO as varchar(20))+' Tipo: '+@TipoNominaStr +
	    ' UUID Anterior: '+ @NO_FACUUIDAnterior
	end 
	
	EXECUTE SP_INSERTAR_BITACORA   @US_CODIGO,@BI_PROC_ID,@BI_TIPO,@PC_NUMERO ,@BI_TEXTO ,@CB_CODIGO ,@BI_DATA ,@BI_CLASE ,@BI_FEC_MOV

end 

go 

create procedure TimbradoConcilia_AplicarUnEmpleado(
	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero,
	@CB_CODIGO NumeroEmpleado,
	@US_CODIGO Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_NUMERO  FolioGrande
	)
as
begin 

	set nocount on 

	declare @NC_CURP		Descripcion	
	declare @NC_STATUS		Status
	declare @NC_RESULT		Status
	declare @NC_RES_DESC	DescLarga
	declare @NC_VERSION		DescLarga
	declare @NC_UUID		DescLarga
	declare @NO_FACUUID		DescLarga
	declare @NC_STATUS_TRESS		Status 
	declare @NC_FACTURA		FolioGrande
	declare @CB_RFC		Descripcion
	declare @lAplicado		Booleano
	--
	select 
			@NC_CURP	 = NC_CURP,
			@NC_STATUS	 = NC_STATUS,
			@NC_RESULT	 = NC_RESULT,
			@NC_RES_DESC = NC_RES_DESC,
			@NC_VERSION  = NC_VERSION,
			@NO_FACUUID  = NO_FACUUID,
			@NC_UUID	 = NC_UUID, 
			@NC_STATUS_TRESS = NC_STATUS_TRESS, 
			@NC_FACTURA  = NC_FACTURA, 
			@CB_RFC		= CB_RFC
	from VTIMCONCIL 
	where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO and CB_CODIGO = @CB_CODIGO
	

	set @lAplicado = 'N' 
	-- Si el timbre esta cancelado en la nube pero en TRESS est� timbrado , se cancela
	if ( @NC_RESULT = 5 ) and (@NC_STATUS_TRESS > 6) 
	begin 
		-- Debe cancelarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 0, '',0, @US_CODIGO
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 0, 0, '', @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	else
	-- Si el timbre esta cancelado en TRESS pero en Nube est� timbrado , se timbra
	if ( @NC_RESULT in (4, 6 )  ) and (@NC_STATUS_TRESS <= 6 ) 
	begin 
		-- Debe timbrarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 2, '',0, @US_CODIGO, @NC_UUID 
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 2, @NC_FACTURA, @NC_UUID, @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	else
	if ( @NC_RESULT = 98 ) and (@NC_STATUS_TRESS > 6 ) and ( @NC_UUID <> '' ) 
	begin 
		-- Debe timbrarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 2, '',0, @US_CODIGO, @NC_UUID 
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 2, @NC_FACTURA, @NC_UUID, @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	else
	-- Si no existe datos en la nube pero en TRESS est� timbrado , se cancela en TRESS
	if ( @NC_RESULT = 99 ) and (@NC_STATUS_TRESS > 6) 
	begin 
		-- Debe cancelarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 0, '',0, @US_CODIGO
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 0, 0, '', @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	

	if @lAplicado = 'S' 
	begin 
		update NOM_CONCIL 
		set 
			NC_APLICADO = 'S', 
			US_CODIGO = @US_CODIGO,		
			US_FEC_MOD = @US_FEC_MOD
		where 
			PE_YEAR = @PE_YEAR and
			PE_TIPO = @PE_TIPO and 
			PE_NUMERO = @PE_NUMERO and
			CB_CODIGO = @CB_CODIGO
	end 
	
end;

GO 

CREATE procedure TimbradoConcilia_CalcularStatusPeriodo( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @StatusActual status, @StatusNuevo status)   
as  
begin  
	set nocount on   
	declare @Total int   
	declare @TotalAfectados int   
	declare @TotalTimbradoPendiente int   
	declare @TotalCancelacionPendiente int   

	declare @Timbrados int   
	declare @StatusTimbradoPendiente status    
	declare @StatusTimbradoParcial status    
	declare @StatusTimbradoTotal status    
	declare @NominaAfectada status   
	declare @NominaAfectadaParcial status      
	declare @StatusTimbradoPendienteNuevo status
	declare @StatusCancelacionPendienteNuevo status
   
	set @StatusTimbradoPendiente = 0
	set @StatusTimbradoParcial = 1   
	set @StatusTimbradoTotal = 2   
	set @NominaAfectada = 6   
	set @NominaAfectadaParcial = 5
	set @StatusTimbradoPendienteNuevo = 3
	set @StatusCancelacionPendienteNuevo = 4
   
 	select @Total                     = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  
	select @TotalAfectados            = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_STATUS = @NominaAfectada   
	select @TotalTimbradoPendiente    = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_TIMBRO = @StatusTimbradoPendienteNuevo   
	select @TotalCancelacionPendiente = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_TIMBRO = @StatusCancelacionPendienteNuevo   
   
	 if @Total > 0   
	 begin     
		  select @Timbrados = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  NO_STATUS = @NominaAfectada  and NO_TIMBRO = @StatusTimbradoTotal  

		  if ( @TotalAfectados > 0 ) 
		  begin 
			  if ( @TotalTimbradoPendiente > 0 )              
				update PERIODO set PE_TIMBRO = @StatusTimbradoPendienteNuevo where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial                      
			  else 
				if ( @TotalCancelacionPendiente > 0 )          
					update PERIODO set PE_TIMBRO = @StatusCancelacionPendienteNuevo where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial          
				else                  
					if ( @Total = @Timbrados )  
						update PERIODO set PE_TIMBRO = @StatusTimbradoTotal  where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial                            
					else  
					   if ( @Timbrados > 0 ) 
							update PERIODO set PE_TIMBRO = @StatusTimbradoParcial where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial      
					   else 
							update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial                                                                    
		  end
		  else
		  begin
			update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial   
		  end; 
	end;    
end
GO 

create procedure TimbradoConcilia_Aplicar( 	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero, 	
	@US_CODIGO	Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_NUMERO FolioGrande, 
	@Error		VARCHAR( 255 ) OUTPUT
)
as
begin 
	set @Error = '' 
	select row_number() over (order by CB_CODIGO) i,  CB_CODIGO
	into #TmpNominasAplicar
	from NOM_CONCIL
	where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO and NOM_CONCIL.NC_APLICADO = 'N' and NOM_CONCIL.NC_RESULT <> 0 


	declare @i FolioGrande 
	declare @iMax FolioGrande 

	select @iMax = COUNT(*) from #TmpNominasAplicar

	declare @CB_CODIGO  NumeroEmpleado 

	set @i= 1 
	while @i <= @iMax 
	begin 
		select @CB_CODIGO = CB_CODIGO from #TmpNominasAplicar where i = @i 
		exec TimbradoConcilia_AplicarUnEmpleado @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO
		set @i= @i+1 
	end 

	exec TimbradoConcilia_CalcularStatusPeriodo @PE_YEAR, @PE_TIPO, @PE_NUMERO, 0, 0
	drop table #TmpNominasAplicar
	return 
end 


GO 

CREATE PROCEDURE SP_FONACOT_PAGO_CEDULA
( @FT_YEAR Anio, @FT_MONTH Mes, @GLOBAL_ACTIVO Booleano, @RecalcularIncidencias Booleano = 'S' )
AS
BEGIN
	-- DECLARE @TODAS_LAS_RAZONES Booleano
	-- SET @TODAS_LAS_RAZONES = 'S' 
	-- Solo se recalculan las incidencias en FON_EMP cuando la opcion esta encendida. 
	if @GLOBAL_ACTIVO = 'S' and @RecalcularIncidencias = 'S' 
		exec SP_FONACOT_RECALCULAR_INCIDENCIAS @FT_YEAR, @FT_MONTH 

	-- Borrar FON_TOT_RS
	DELETE FROM FON_TOT_RS WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH

	-- Abrir cursor
	DECLARE @RS_CODIGO Codigo

	DECLARE RAZONES_SOCIALES CURSOR FOR
		SELECT DISTINCT RS_CODIGO FROM PCED_ENC WHERE PE_YEAR = @FT_YEAR AND PE_MES = @FT_MONTH

	-- Abrir cursor
	OPEN RAZONES_SOCIALES 
		FETCH NEXT FROM RAZONES_SOCIALES INTO @RS_CODIGO

	-- Ciclo
	WHILE ( @@FETCH_STATUS = 0 )
	-- IF @TODAS_LAS_RAZONES = 'S'
	BEGIN
		-- Grabar en FON_TOT_RS
		INSERT INTO FON_TOT_RS
			(FT_YEAR, FT_MONTH, PR_TIPO, RS_CODIGO, FT_STATUS, FT_RETENC, FT_NOMINA, FT_TAJUST, FT_AJUSTE, FT_EMPLEAD, FT_CUANTOS, FT_BAJAS, FT_INCAPA, FT_DETALLE)		
		SELECT FT_YEAR, FT_MONTH, FC.PR_TIPO, @RS_CODIGO, 0 FT_STATUS, 0, SUM(FC_NOMINA), '' FT_TAJUST, SUM (FC_AJUSTE), 
		0 FT_EMPLEAD, 0 FT_CUANTOS, 0 FT_BAJAS, 0 FT_INCAPA, '' FT_DETALLE FROM FON_CRE FC, VCED_FON VF			
			WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH 			
			AND FC.FT_YEAR = VF.PF_YEAR AND FC.FT_MONTH = VF.PF_MES AND FC.PR_TIPO = VF.PR_TIPO AND FC.PR_REFEREN = VF.PR_REFEREN AND FC.CB_CODIGO = VF.CB_CODIGO AND (VF.RS_CODIGO = @RS_CODIGO ) 
			GROUP BY FT_YEAR, FT_MONTH, FC.PR_TIPO
			
		-- Indicar cantidad de empleados y cantidad de cr�ditos.
		DECLARE @FT_EMPLEAD Empleados
		DECLARE @FT_CUANTOS Empleados			
		-- Indicar monto de c�dula, incapacidades y bajas
		-- Obtener de FON_CRE o PCED_FON dependiendo de lo que indique el global.
		DECLARE @FC_RETENC Pesos SET @FC_RETENC = 0
		DECLARE @FT_BAJAS Empleados SET @FT_BAJAS = 0
		DECLARE @FT_INCAPA Empleados SET @FT_INCAPA = 0

		IF @GLOBAL_ACTIVO = 'S' 
		BEGIN
			SELECT @FC_RETENC = SUM (PF_PAGO) FROM PCED_FON WHERE  PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH 
				AND (RS_CODIGO = @RS_CODIGO )
				 
			SELECT @FT_BAJAS = COUNT(DISTINCT CB_CODIGO)  FROM VCED_DET WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
				AND (RS_CODIGO = @RS_CODIGO )
				AND FE_INCIDE = 'B'
			SELECT @FT_INCAPA = COUNT(DISTINCT CB_CODIGO)  FROM VCED_DET WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
				AND (RS_CODIGO = @RS_CODIGO )
				AND FE_INCIDE = 'I'
			
			-- FT_CUANTOS: # de Cr�ditos Fonacot
			SELECT @FT_CUANTOS = COUNT(DISTINCT PR_REFEREN) FROM VCED_FON WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
			AND (RS_CODIGO = @RS_CODIGO )
			-- FT_EMPLEAD: # Empleados con Pr�stamos Fonacot
			SELECT @FT_EMPLEAD = COUNT(DISTINCT CB_CODIGO) FROM VCED_FON WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
			AND (RS_CODIGO = @RS_CODIGO )

		END
		ELSE
		BEGIN			
			SELECT @FC_RETENC = SUM (FC_RETENC) FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH 				
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				 
			
			SELECT @FT_BAJAS = COUNT(DISTINCT CB_CODIGO)  FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
				(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				AND FE_INCIDE = 'B'
			SELECT @FT_INCAPA = COUNT(DISTINCT CB_CODIGO)  FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
				(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				AND FE_INCIDE = 'I'

			-- FT_CUANTOS: # de Cr�ditos Fonacot
			SELECT @FT_CUANTOS = COUNT(DISTINCT PR_REFEREN) FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
			AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
			-- FT_EMPLEAD: # Empleados con Pr�stamos Fonacot
			SELECT @FT_EMPLEAD = COUNT(DISTINCT CB_CODIGO) FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
			AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
		END
		
		-- Actualizar tabla
		UPDATE FON_TOT_RS SET FT_RETENC = @FC_RETENC, FT_BAJAS = @FT_BAJAS, FT_INCAPA = @FT_INCAPA, FT_EMPLEAD = @FT_EMPLEAD, FT_CUANTOS = @FT_CUANTOS 
		WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH AND (RS_CODIGO = @RS_CODIGO  )

		FETCH NEXT FROM RAZONES_SOCIALES
			INTO	@RS_CODIGO

		-- IF @TODAS_LAS_RAZONES = 'S'
			-- BREAK;
	END

	CLOSE RAZONES_SOCIALES
	DEALLOCATE RAZONES_SOCIALES 

	-- Actualizar estatus, tipo de ajuste y detalle.
	-- Se obtienen de FON_TOT.
	DECLARE @FT_STATUS Status
	DECLARE @FT_TAJUST Codigo1
	DECLARE @FT_DETALLE NVARCHAR(MAX)
	SELECT @FT_STATUS = FT_STATUS, @FT_TAJUST = FT_TAJUST, @FT_DETALLE = FT_DETALLE FROM FON_TOT WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
	UPDATE FON_TOT_RS SET FT_STATUS = @FT_STATUS, FT_TAJUST = @FT_TAJUST, FT_DETALLE = @FT_DETALLE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH

END
GO

create procedure Analitica_Upsert( @Modulo Descripcion,  @HelpContext FolioGrande, @Codigo Descripcion,  @Descripcion Formula, @Bool Booleano = 'N' , @Numero Pesos = 0.00 , @Fecha Fecha = '1899-12-30' , @Usuario Usuario = 1 , @Texto varchar(max) = '') 
as
begin
	set nocount on  
	
	set @Modulo = coalesce( @Modulo, '' ) 
	set @HelpContext = coalesce( @HelpContext, 0) 
	set @Descripcion = coalesce( @Descripcion, '' ) 
	set @Bool = coalesce( @Bool, 'N' ) 
	set @Numero = coalesce( @Numero, 0.00) 
	set @Usuario = coalesce( @Usuario, 0 ) 
	set @Texto = coalesce( @Texto, '' ) 

	update T_Analitica 
	set 
		AnaliticaDescripcion = case when @Descripcion = '' then AnaliticaDescripcion else @Descripcion end, 
		AnaliticaBool = @Bool, 
		AnaliticaNumero = @Numero, 
		AnaliticaTexto = @Texto, 
		AnaliticaUsuario = @Usuario, 
		AnaliticaFechaUpd = getdate(), 
		AnaliticaFecha = @Fecha
	where 
		AnaliticaModulo = @Modulo and AnaliticaHelpContext = @HelpContext and AnaliticaCodigo = @Codigo


	if @@ROWCOUNT > 0 
		return 
	else 
	begin 
		insert into T_Analitica (AnaliticaModulo, AnaliticaHelpContext, AnaliticaCodigo, AnaliticaDescripcion, AnaliticaBool, AnaliticaNumero, AnaliticaUsuario,  AnaliticaFecha, AnaliticaFechaUpd, AnaliticaTexto ) 
		values ( @Modulo, @HelpContext,  @Codigo, @Descripcion, @Bool, @Numero, @Usuario, @Fecha, GETDATE(), @Texto ) 
	end 
			

end 
go 

create procedure Analitica_Iniciar_Version2017 
as
begin 
	if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Version 2017' ) = 0 
	begin 
		exec Analitica_Upsert 'Versi�n 2017' , 1, 'V2017_DB' ,   'Tableros de inicio - Uso'
		exec Analitica_Upsert 'Versi�n 2017' , 2, 'V2017_RE1' ,  'Reportes Email - Env�os programados'
	end 
end 
go 

create  procedure Analitica_Poblar_Version2017
as
begin 	
	declare @CantidadRolTableros int 
	declare @UsaTablerosInicio booleano 
	
	select @CantidadRolTableros = count(*) from #Comparte..RolTableros rt join #Comparte..CompanyTableros ct on rt.CompanyTableroID = ct.CompanyTableroID  
    join #Comparte..Tableros t on t.TableroID = ct.TableroID where TableroNombre <> 'Mi Empresa' 
	if ( @CantidadRolTableros > 0 ) 
		set @UsaTablerosInicio = 'S' 
	else 
		set @UsaTablerosInicio= 'N' 
	
	exec Analitica_Upsert 'Versi�n 2017' , 1, 'V2017_DB' ,  '',  @UsaTablerosInicio,  @CantidadRolTableros

	declare @CantidadEnviosProgramados int 
	
	select @CantidadEnviosProgramados  = COUNT(*) from #Comparte..CALENDARIO where CA_ACTIVO = 'S'
	exec Analitica_Upsert 'Versi�n 2017' , 2, 'V2017_RE1' ,  '',  'N',  @CantidadEnviosProgramados
	
end 
go

create procedure Analitica_Iniciar_Version2018 
as
begin 
	if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Version 2018' ) = 0 
	begin 
		exec Analitica_Upsert 'Versi�n 2018' , 3, 'V2018_DP' ,   'Desafectaci�n Parcial' 
		exec Analitica_Upsert 'Versi�n 2018' , 4, 'V2018_PS1' ,  'Previsi�n Social - Conceptos'
		exec Analitica_Upsert 'Versi�n 2018' , 5, 'V2018_PS2' ,  'Previsi�n Social - Uso Funci�n'
		exec Analitica_Upsert 'Versi�n 2018' , 6, 'V2018_FON1' , 'Fonacot - Global Encendido'
		exec Analitica_Upsert 'Versi�n 2018' , 7, 'V2018_FON2' , 'Fonacot - Importar C�dula'
		exec Analitica_Upsert 'Versi�n 2018' , 8, 'V2018_FON3' , 'Fonacot - Generaci�n de c�dula desde Sistema TRESS'
		exec Analitica_Upsert 'Versi�n 2018' , 9, 'V2018_FON4' , 'Fonacot - Empleados c/Fonacot'
		exec Analitica_Upsert 'Versi�n 2018' , 10, 'V2018_CUR1' , 'Cursos - Registrados con aplicaci�n'
		exec Analitica_Upsert 'Versi�n 2018' , 11, 'V2018_CUR2' , 'Cursos - Total de cursos', 'N' 
		exec Analitica_Upsert 'Versi�n 2018' , 12, 'V2018_CUR3' , 'Cursos - Total de cursos configurados en Matriz de entrenamiento'
		exec Analitica_Upsert 'Versi�n 2018' , 13, 'V2018_CUR4' , 'Cursos - Total de Grupos de Capacitaci�n'
	end 
end 
go 

create  procedure Analitica_Poblar_Version2018
as
begin 
	
	declare @UltimoProcDesafectarParcial Fecha
	select top 1 @UltimoProcDesafectarParcial = PC_FEC_INI from Proceso where PC_PROC_ID = 22 and CB_CODIGO > 0  and PC_FEC_INI > '2018-07-01' order by PC_FEC_INI desc

	set @UltimoProcDesafectarParcial = coalesce( @UltimoProcDesafectarParcial, '1899-12-30' ) 

	if ( @UltimoProcDesafectarParcial > '1899-12-30' ) 
		exec Analitica_Upsert 'Versi�n 2018' , 3, 'V2018_DP', '', 'S', 0.00, @UltimoProcDesafectarParcial


	-- Prevision Social 
	declare @ConceptosPrevisionSocial int 
	declare @UsaConceptosPrevisionSocial booleano 
	
	select @ConceptosPrevisionSocial = COUNT(*) from CONCEPTO where CO_PS_TIPO = 1 and CO_ACTIVO = 'S'  
	if ( @ConceptosPrevisionSocial > 0 ) 
		set @UsaConceptosPrevisionSocial = 'S' 
	else 
		set @UsaConceptosPrevisionSocial= 'N' 
	
	exec Analitica_Upsert 'Versi�n 2018' , 4, 'V2018_PS1' ,  '',  @UsaConceptosPrevisionSocial,  @ConceptosPrevisionSocial


	select @ConceptosPrevisionSocial = COUNT(*) from CONCEPTO where CO_X_ISPT like '%CAL_EX_PS%'
	if ( @ConceptosPrevisionSocial > 0 ) 
		set @UsaConceptosPrevisionSocial = 'S' 
	else 
		set @UsaConceptosPrevisionSocial= 'N' 
	exec Analitica_Upsert 'Versi�n 2018' , 5, 'V2018_PS2' ,  '',  @UsaConceptosPrevisionSocial,  @ConceptosPrevisionSocial

	/* Fonacot v2018 */
	-- Global.
	declare @GlobalFonacot Booleano SET @GlobalFonacot = 'N';
	SELECT @GlobalFonacot = COALESCE (GL_FORMULA, 'N') FROM GLOBAL WHERE GL_CODIGO = 328 	
	exec Analitica_Upsert 'Versi�n 2018' , 6, 'V2018_FON1' ,  '', @GlobalFonacot 
	-- Importar c�dula
	declare @Numero Empleados set @Numero = 0;
	select @Numero = coalesce (count(pe_tipo), 0) from pced_enc
	declare @Fecha Fecha set @Fecha = '1899-12-30 00:00:00.000'
	SELECT TOP 1 @Fecha = coalesce (PC_FEC_INI, '1899-12-30 00:00:00.000') FROM PROCESO WHERE PC_PROC_ID = 209 ORDER BY PC_FEC_INI DESC, PC_HOR_INI DESC
	exec Analitica_Upsert 'Versi�n 2018' , 7, 'V2018_FON2' ,  '', null, @Numero, @Fecha
	-- Generaci�n de c�dula
	set @Numero = 0
	set @Fecha = '1899-12-30 00:00:00.000'
	SELECT @Numero = coalesce (COUNT (PC_PROC_ID), 0), @Fecha = Coalesce (MAX (PC_FEC_INI), '1899-12-30 00:00:00.000') FROM PROCESO WHERE PC_PROC_ID = 210
	exec Analitica_Upsert 'Versi�n 2018' , 8, 'V2018_FON3' ,  '', null, @Numero, @Fecha
	-- Empleados Fonacot
	set @Numero = 0
	SELECT @Numero = coalesce (COUNT(CB_FONACOT), 0) FROM COLABORA WHERE CB_FONACOT <> '' and CB_ACTIVO = 'S' 
	exec Analitica_Upsert 'Versi�n 2018' , 9, 'V2018_FON4' ,  '', null, @Numero

	/* Cursos v2018 */
	-- V2018_CUR1.
	-- Cursos - Registrados con aplicaci�n [pendiente]
	-- V2018_CUR2.
	set @Numero = 0;
	SELECT @Numero = coalesce (COUNT(CU_CODIGO), 0) FROM CURSO  where CU_ACTIVO='S' 
	exec Analitica_Upsert 'Versi�n 2018' , 11, 'V2018_CUR2' ,  '', null, @Numero
	-- V2018_CUR3
	set @Numero = 0;
	SELECT @Numero = COALESCE (COUNT(DISTINCT E.CU_CODIGO), 0) FROM ENTRENA E JOIN  CURSO C ON E.CU_CODIGO = C.CU_CODIGO WHERE C.CU_ACTIVO = 'S'
	exec Analitica_Upsert 'Versi�n 2018' , 12, 'V2018_CUR3' ,  '', null, @Numero 
	-- V2018_CUR4
	set @Numero = 0;
	SELECT @Numero = coalesce (COUNT(SE_FOLIO), 0) FROM  SESION join CURSO on SESION.CU_CODIGO = CURSO.CU_CODIGO where CURSO.CU_ACTIVO = 'S' 
	exec Analitica_Upsert 'Versi�n 2018' , 13, 'V2018_CUR4' ,  '', null, @Numero 
end 
go

CREATE PROCEDURE SP_POBLAR_TPROCESO
as
begin 
SET NOCOUNT ON 
	IF (  select COUNT(*) from TPROCESO )  = 0 
	BEGIN 
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         1, 'Recursos ', 'Salario Integrado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         2, 'Recursos ', 'Cambio Salario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         3, 'Recursos ', 'Promediar Variables', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         4, 'Recursos ', 'Vacaciones Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         5, 'Recursos ', 'Aplicar Tabulador', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         6, 'Recursos ', 'Aplicar Eventos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         7, 'Recursos ', 'Importar Kardex', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         8, 'Recursos ', 'Cancelar Kardex', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         9, 'Recursos ', 'Curso Tomado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        10, 'Asistencia ', 'Poll Relojes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        11, 'Asistencia ', 'Procesar Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        12, 'Asistencia ', 'Calculo PreN�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        13, 'Asistencia ', 'Extras y Permisos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        14, 'Asistencia ', 'Registros Autom�ticos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        15, 'Asistencia ', 'Corregir Fechas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        16, 'Asistencia ', 'Recalcular Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        17, 'N�mina ', 'Calcular', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        18, 'N�mina ', 'Afectar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        19, 'N�mina ', 'Imprimir Listado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        20, 'N�mina ', 'Imprimir Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        21, 'N�mina ', 'P�liza Contable', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        22, 'N�mina ', 'Desafectar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        23, 'N�mina ', 'Limpiar Acumulados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        24, 'N�mina ', 'Importar Excepciones de N�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        25, 'N�mina ', 'Exportar Movimientos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        26, 'N�mina ', 'Pagos Por Fuera', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        27, 'N�mina ', 'Cancelar N�minas Pasadas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        28, 'N�mina ', 'Liquidaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        29, 'N�mina ', 'Calcular Salario Neto/Bruto', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        30, 'N�mina ', 'Rastrear C�lculo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        31, 'N�mina ', 'Foliar Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        32, 'N�mina ', 'Definir Per�odos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        33, 'N�mina ', 'Calcular Aguinaldo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        34, 'N�mina ', 'Reparto de Ahorro', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        35, 'N�mina ', 'Calcular PTU', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        36, 'N�mina ', 'Declaraci�n Subsidio al Empleo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        37, 'N�mina ', 'Declaraci�n de ISPT Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        38, 'N�mina ', 'Recalcular Acumulados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        39, 'IMSS ', 'Calcular Pagos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        40, 'IMSS ', 'Calcular Recargos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        41, 'IMSS ', 'Revisar SUA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        42, 'IMSS ', 'Exportar SUA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        43, 'IMSS ', 'Calcular Prima de Riesgo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        44, 'IMSS ', 'Calcular Tasa INFONAVIT', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        45, 'Asistencia ', 'Ajuste Colectivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        46, 'Sistema ', 'Borrar Bajas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        47, 'Sistema ', 'Borrar Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        48, 'Sistema ', 'Depurar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        49, 'Sistema ', 'Migraci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        50, 'Recursos ', 'Transferencia de Empleados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        51, 'N�mina ', 'Pagar Aguinaldo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        52, 'N�mina ', 'Pagar Reparto de Ahorro', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        53, 'N�mina ', 'Pagar PTU', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        54, 'Sistema ', 'Cierre de Ahorros', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        55, 'Sistema ', 'Cierre de Pr�stamos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        56, 'Sistema ', 'Borrar N�minas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        57, 'N�mina ', 'Pagar ISPT Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        58, 'N�mina ', 'Pago de Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        59, 'N�mina ', 'ReFoliar Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        60, 'N�mina ', 'Copiar N�minas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        61, 'N�mina ', 'Calcular Diferencias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        62, 'Recursos ', 'Cancelar Vacaciones Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        63, 'Sistema ', 'Borrar POLL', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        64, 'Labor ', 'Calcular Tiempos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        65, 'Labor ', 'Depuraci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        66, 'Labor ', 'Procesar Lecturas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        67, 'Labor ', 'Importar Ordenes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        68, 'Consultas ', 'Reportes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        69, 'Recursos ', 'Cierre Global De Vacaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        70, 'Recursos ', 'Entregar Herramienta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        71, 'Recursos ', 'Regresar Herramienta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        72, 'Sistema ', 'Borrar Herramienta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        73, 'Labor ', 'Importar Partes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        74, 'N�mina ', 'Preparar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        75, 'Selecci�n ', 'Depurar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        76, 'Labor ', 'Cancelar Breaks', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        77, 'Recursos ', 'Importar Altas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        78, 'N�mina ', 'SUBE Aplicado Mensual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        79, 'Recursos ', 'Renumeraci�n de Empleados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        80, 'Recursos ', 'Permisos Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        81, 'N�mina ', 'Calcular Retroactivos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        82, 'Labor ', 'Afectar Labor', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        83, 'Labor ', 'Importar Componentes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        84, 'Recursos ', 'Cancelar Curso Tomado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        85, 'N�minas ', 'Declaraci�n Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        86, 'Cafeter�a ', 'Registro Grupal de Comidas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        87, 'Cafeter�a ', 'Corregir Fechas Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        88, 'Labor ', 'Importar C�dulas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        89, 'Labor ', 'Importar Cambio de Area', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        90, 'Labor ', 'Importar Producci�n Individual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        91, 'Sistema ', 'Actualizar Numero de Tarjeta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        92, 'Recursos ', 'Cambio Masivo de Turnos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        93, 'Evaluaci�n ', 'Agregar Empleados a evaluar global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        94, 'Evaluaci�n ', 'Preparar Evaluaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        95, 'Evaluaci�n ', 'Calcular resultados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        96, 'Evaluaci�n ', 'Recalcular promedios', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        97, 'Evaluaci�n ', 'Cerrar Evaluaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        98, 'Recursos ', 'Importar cursos tomados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        99, 'Recursos ', 'Cancelar cierre de vacaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       100, 'Evaluaci�n ', 'Enviar correos de invitaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       101, 'Evaluaci�n ', 'Enviar correos de notificaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       102, 'Evaluaci�n ', 'Asignaci�n de relaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       103, 'Presupuestos ', 'Depuraci�n de Supuestos de Personal', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       104, 'Presupuestos ', 'Depuraci�n de N�minas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       105, 'Presupuestos ', 'Calculo del Cubo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       106, 'Presupuestos ', 'Simulaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       107, 'Evaluaci�n ', 'Crear encuesta simple', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       108, 'Asistencia ', 'Intercambio de festivos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       109, 'Asistencia ', 'Cancelar excepciones de festivos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       110, 'Recursos ', 'Cancelar permisos globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       111, 'Recursos ', 'Importar ahorros y pr�stamos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       112, 'Recursos ', 'Recalcular saldos de vacaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       113, 'Adm. de Guardias ', 'C�lculo de Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       114, 'Adm. de Guardias ', 'Cambiar Plazas Vac.', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       115, 'Adm. de Guardias ', 'Enviar a transici�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       116, 'Adm. de Guardias ', 'Calendario Guardias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       117, 'Adm. de Guardias ', 'Subieron', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       118, 'Adm. de Guardias ', 'Bajaron', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       119, 'Adm. de Guardias ', 'Permanecen a bordo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       120, 'Adm. de Guardias ', 'Permanecen en tierra', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       121, 'Log�stica ', 'Programar Viajes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       122, 'Log�stica ', 'Asignar Viaje', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       123, 'Adm. de Guardias ', 'Liberar a Log�stica', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       124, 'Log�stica ', 'Poll PDA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       125, 'Adm. de Guardias ', 'Cambio Status Vacante', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       126, 'Adm. de Guardias ', 'Generar Recibo Empleado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       127, 'Transporte ', 'Programar viajes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       128, 'Transporte ', 'Asignar viajes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       129, 'Transporte ', 'Importar registros de viaje', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       130, 'Transporte ', 'PDA de pasajeros', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       131, '', 'Aplicar Permutas de Asistencia', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       132, '', 'Depurar Permutas de Asistencia', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       133, 'Adm. de Guardias ', 'Alta M�dica', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       134, 'N�mina ', 'Ajuste de Retenci�n Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       135, 'N�mina ', 'Calcular Pago Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       136, 'Adm. de Guardias ', 'Generar Plantillas OT', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       137, 'Sincronizador ', 'Exportar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       138, 'Sincronizador ', 'Respaldar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       139, 'Sincronizador ', 'Comprimir', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       140, 'Sincronizador ', 'Transferir', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       141, 'Sincronizador ', 'Descomprimir', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       142, 'Sincronizador ', 'Restaurar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       143, 'Sincronizador ', 'Importar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       144, 'Adm. de Guardias ', 'Importar Horas Extras', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       145, 'Sistema ', 'Enrolamiento Masivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       146, 'Sistema ', 'Importar Enrolamiento Masivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       147, 'Configurador Reportes ', 'Exportar Diccionario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       148, 'Configurador Reportes ', 'Importar Diccionario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       149, 'IMSS ', 'Ajuste de Retenci�n INFONAVIT', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       150, 'Adm. de Guardias ', 'Generar Solicitudes PDA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       151, '', 'Adm. de Guardias = Abrir Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       152, 'Cabinas y Camas ', 'Generar Cabinas y Camas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       153, 'Adm. de Guardias ', 'Cambio Registro Patronal', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       154, 'Asistencia ', 'Reprocesar Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       155, 'Recursos ', 'Aplicar Finiquitos Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       156, 'Recursos ', 'Simulaci�n Finiquitos Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       157, 'Recursos ', 'Importar Kardex de Infonavit', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       158, 'Recursos ', 'Curso Programado Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       159, 'Recursos ', 'Cancelar Curso Programado Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       160, 'Asistencia ', 'Ajustar Incapacidades en Calendario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       161, 'Talleres estrella ', 'C�lculo de tarifas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       162, 'Recursos ', 'Foliar Capacitaciones de STPS', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       163, 'N�mina ', 'Cierre Declaraci�n Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       164, 'Asistencia ', 'Autorizaci�n de Pre-N�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       165, 'Asistencia ', 'Registro de Excepciones de Festivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       166, 'IMSS ', 'Validaci�n Movimientos IDSE', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       167, 'N�mina ', 'Transferencias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       168, 'N�mina ', 'C�lculo de Costeo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       169, 'N�mina ', 'Cancelaci�n de Transferencias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       170, 'Asistencia ', 'Importaci�n de Clasificaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       171, 'Sistema ', 'Borrar Bit�cora Terminales GTI', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       172, 'Sistema ', 'Asigna N�meros Biom�tricos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       173, 'Sistema ', 'Asigna Grupo de Terminales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       174, 'Recursos ', 'Importar Seguros de Gastos M�dicos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       175, 'Recursos ', 'Renovar Seguros de Gastos M�dicos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       176, 'Recursos ', 'Borrar Seguros de Gastos M�dicos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       177, 'N�mina ', 'C�lculo Previo ISR', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       178, 'Recursos ', 'Revisar datos para STPS', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       179, 'Recursos ', 'Reiniciar Folio Capacitaciones de STPS', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       180, 'Sistema ', 'Importaci�n de Terminales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       181, 'Recursos ', 'Importar Organigrama', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       182, 'N�mina ', 'Timbrar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       183, 'Sistema ', 'Actualizaci�n de Base de Datos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       184, 'Sistema ', 'Importar Diccionario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       185, 'Sistema ', 'Importar Reporte', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       186, 'Sistema ', 'Script Especial', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       187, 'Sistema ', 'Preparar Presupuestos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       188, 'Sistema ', 'Importar Tablas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       189, 'Recursos ', 'Importar Imagenes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       190, 'Sistema ', 'Importar Tablas en CSV', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       191, 'Sistema ', 'Agregar Base de Datos Seleccion', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       192, 'Sistema ', 'Agregar Base de Datos Visitantes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       193, 'Sistema ', 'Agregar Base de Datos Pruebas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       194, 'Sistema ', 'Agregar Base de Datos Tress', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       195, 'Sistema ', 'Borrar estatus de Timbrado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       196, 'Sistema ', 'Extraer checadas Terminales GTI', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       197, 'Sistema ', 'Transformar checadas Terminales GTI', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       198, 'N�mina ', 'Cancelar Ajuste de Retenci�n Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       199, 'Recursos ', 'Cambio Masivo de Turnos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       200, 'Empleados ', 'Pensi�n Alimenticia', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       201, 'Cafeter�a ', 'Historial', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       202, 'N�mina ', 'Importar Acumulados Raz�n social', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       203, 'N�mina ', 'Recalcular Ahorros', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       204, 'Sistema ', 'Copiar Configuraci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       205, 'Evaluaci�n ', 'Agregar encuesta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       206, 'N�mina ', 'Recalcular Pr�stamos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       207, 'Sistema ', 'Agregar nuevo registro al Administrador de env�os programados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       208, 'Sistema ', 'Servicio de Correos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       209, 'N�mina ', 'Importar c�dula de cr�ditos Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       210, 'N�mina ', 'Generaci�n de c�dula de pago Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       211, 'N�mina ', 'Eliminar registros de c�dula Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       212, 'N�mina ', 'Conciliaci�n de periodos de n�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       213, 'N�mina ', 'Aplicar conciliaci�n a periodos de n�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       214, 'N�mina ', 'Cambiar estatus a pendiente de empleado', 490 );
	END 
END 
go 

exec SP_POBLAR_TPROCESO

go 

drop procedure SP_POBLAR_TPROCESO

go

create procedure Analitica_Iniciar_ConciliadorIMSS
as
begin 
    if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'ConciliadorIMSS' ) = 0 
    begin 
        exec Analitica_Upsert 'ConciliadorIMSS' , 400000, 'CONCIL' ,   'Conciliador IMSS'
        exec Analitica_Upsert 'ConciliadorIMSS' , 300000, 'CONCIL' ,  'Conciliador SUA'
    end 
end 

go

create procedure Analitica_Iniciar_Empleados
as
begin 
    if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Empleados' ) = 0 
    begin 
        exec Analitica_Upsert 'Empleados' , 14, 'EMP_GM' ,  'Administraci�n de Seguros de Gastos M�dicos'
        exec Analitica_Upsert 'Empleados' , 15, 'EMP_PV' ,  'Plan de vacaciones' 
		exec Analitica_Upsert 'Empleados' , 16, 'EMP_VP' ,  'Valuaci�n de puestos' 
		exec Analitica_Upsert 'Empleados' , 17, 'EMP_DP' ,  'Descripci�n de puestos'
    end 
end 

go
 
create procedure Analitica_Iniciar_Cursos
as
begin 
    if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Cursos' ) = 0 
    begin 
        exec Analitica_Upsert 'Cursos' , 1, 'CUR_MF' ,  'Matriz de Funciones'
        exec Analitica_Upsert 'Cursos' , 2, 'CUR_CO' ,  'Total de competencias evaluadas' 
    end 
end 

go 

create  procedure Analitica_ContabilizarUso(@Modulo Descripcion,  @HelpContext FolioGrande, @Cuantos FolioGrande) 
as
begin 
    set nocount on 
    declare @ValorActual FolioGrande 

    update T_Analitica set  AnaliticaNumero = ta.AnaliticaNumero + @Cuantos, AnaliticaFecha = GETDATE() from  T_Analitica  ta
    where ta.AnaliticaModulo = @Modulo and ta.AnaliticaHelpContext = @HelpContext
    
end 

go

create  procedure Analitica_Poblar_Empleados
as
begin 
	set nocount on 
	declare @Cuantos FolioGrande 
	set @Cuantos = 0 

	select @Cuantos = count(*) from EMP_POLIZA
	where EP_FEC_FIN >= DATEADD( year, -1, getdate() );	 
	exec Analitica_Upsert 'Empleados' , 14, 'EMP_GM' ,  '', 'N', @Cuantos
		
	select @Cuantos = count(vp.cb_codigo) from VACAPLAN vp
	inner join COLABORA c on vp.CB_CODIGO = c.CB_CODIGO
	where c.CB_ACTIVO = 'S' and vp.VP_FEC_FIN >=  DATEADD( year, -1, getdate() ) and vp.VP_STATUS = 3  ;
	exec Analitica_Upsert 'Empleados' , 15, 'EMP_PV' ,  '', 'N', @Cuantos


	select @Cuantos = COUNT(vp_folio) from v_val_pto vp join PUESTO
	p on vp.PU_CODIGO = p.PU_CODIGO where p.PU_ACTIVO = 'S'  and vp.VP_FEC_FIN >=  DATEADD( year, -1, getdate() ) ;		
	exec Analitica_Upsert 'Empleados' , 16, 'EMP_VP' ,   '', 'N', @Cuantos

	select @Cuantos=COUNT(PUESTO.PU_CODIGO)  from PERFIL 
	join PUESTO	on PERFIL.PU_CODIGO = PUESTO.PU_CODIGO where PUESTO.PU_ACTIVO = 'S'
	exec Analitica_Upsert 'Empleados' , 17, 'EMP_DP' ,   '', 'N', @Cuantos

end 

go

create  procedure Analitica_Poblar_Procesos
as
begin 
	set nocount on 
	DECLARE @ProcesosResumen TABLE 
	( 
		iProceso int identity (1,1), 
		PC_PROC_ID  int, 
		Modulo		varchar(30), 
		Cuantos		int, 
		Descrip		varchar(255), 
		FechaMax	datetime
	) 

	insert into @ProcesosResumen (PC_PROC_ID, Modulo, Descrip, Cuantos, FechaMax ) 
	select  PROCESO.PC_PROC_ID,  coalesce( TP.TP_MODULO, 'N/D') Modulo,  coalesce( TP.TP_DESCRIP, 'N/D') Descrip,  count(*) Cuantos, MAX(PC_FEC_INI) FechaMax
	from PROCESO 
		LEFT OUTER JOIN TPROCESO TP ON TP.TP_ID = PROCESO.PC_PROC_ID 
		where PC_FEC_INI >=  DATEADD( year, -1, getdate() ) 
	group by TP.TP_MODULO, PROCESO.PC_PROC_ID, TP.TP_DESCRIP 
	order by PROCESO.PC_PROC_ID 

	declare @n int 
	declare @i int 
	select @n= COUNT(*) from @ProcesosResumen 
	set @i=1 

	declare @PC_PROC_ID  int 
	declare @Modulo		 varchar(30)
	declare @Cuantos	 int
	declare @Descrip	 varchar(255)
	declare @FechaMax	 datetime
	declare @Codigo		 Codigo10 

	while @i <= @n 
	begin 
		select @PC_PROC_ID = PC_PROC_ID, @Modulo = Modulo, @Cuantos = Cuantos, @Descrip = Descrip, @FechaMax = FechaMax from @ProcesosResumen where iProceso = @i 

		set @Codigo = 'PR'+CAST( @PC_PROC_ID as varchar(3) ) 

		exec Analitica_Upsert 'Procesos' , @PC_PROC_ID, @Codigo, @Descrip, 'N', @Cuantos, @FechaMax  

		set @i = @i + 1 
	end 

end 

go 

create procedure Analitica_Poblar_Cursos
as 
begin 
	set nocount on 
	declare @Cuantos FolioGrande 
	set @Cuantos = 0 
	

	select @Cuantos = count(*) from C_PER_COMP
	where RP_FEC_INI >= DATEADD( year, -1, getdate() );	 
		
    exec Analitica_Upsert 'Cursos' , 1, 'CUR_MF' ,  'Matriz de Funciones', 'N', @Cuantos 

	select @Cuantos = count(*) from C_EVA_COMP
	where EC_FECHA >= DATEADD( year, -1, getdate() );	 

    exec Analitica_Upsert 'Cursos' , 2, 'CUR_CO' ,  'Total de competencias evaluadas', 'N', @Cuantos 


end 

GO

create procedure Analitica_Poblar
as
begin 
	exec Analitica_Poblar_Version2017; 
	exec Analitica_Poblar_Version2018;
	exec Analitica_Poblar_Empleados; 	
	exec Analitica_Poblar_Procesos; 
	exec Analitica_Poblar_Cursos; 
end; 
go

exec Analitica_Iniciar_Version2017
go

exec Analitica_Iniciar_Version2018
go

exec Analitica_Iniciar_Empleados
go

exec Analitica_Iniciar_ConciliadorIMSS
go

exec Analitica_Poblar
go

create procedure AuditoriaConceptosTimbradoTRESS
as
begin 
    Select 
    cast
    ( 
    ( 
        SELECT
        CO_NUMERO ConceptoID,
        CO_DESCRIP ConceptoDescripcion,
        case CO_CALCULA
            when 'S' then 1 
            else 0 
        end  ConceptoCalcular,
        case CO_ACTIVO 
            when 'S' then 1 
            else 0 
        end ConceptoActivo,
        CO_TIPO ConceptoTipo,
        CO_FORMULA ConceptoFormula,
        CO_FRM_ALT ConceptoFormulaAlterna,
        CO_IMP_CAL ConceptoISPTIndividual,
        case CO_G_ISPT
            when 'S' then 1 
            else 0 
        end ConceptoISPTGravar,
        CO_X_ISPT ConceptoISPTExentoFormula,
        CO_SAT_CLN ConceptoClaseMontoNegativo,
        CO_SAT_CLP ConceptoClaseMontoPositivo,
        CO_SAT_TPN ConceptoSATMontoNegativoClave,
        CO_SAT_TPP ConceptoSATMontoPositivoClave,
        CO_SAT_EXE ConceptoPercepcionExentaTratamiento,
        CO_SAT_CON ConceptoExentoConceptoId,
		case CO_TIMBRA
			when 'S' then 1 
			else 0 
		end  ConceptoTimbra
        FROM CONCEPTO
        ORDER BY 1
    FOR XML RAW ('ConceptosTRESS'), ROOT ('ConceptosSistemaTRESS'), ELEMENTS
    ) as varchar(max) )  ConceptosXML  
end
go

