CREATE TABLE CAUSACCI (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

ALTER TABLE CAUSACCI
       ADD CONSTRAINT PK_CAUSACCI PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE MOTACCI (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

ALTER TABLE MOTACCI
       ADD CONSTRAINT PK_MOTACCI PRIMARY KEY (TB_CODIGO)
 
GO

CREATE TABLE TACCIDEN (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

ALTER TABLE TACCIDEN
       ADD CONSTRAINT PK_TACCIDEN PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE EXPEDIEN (
       EX_CODIGO  NumeroEmpleado,
       EX_TIPO    Status,
       EX_NOMBRES Descripcion,
       EX_APE_PAT Descripcion,
       EX_APE_MAT Descripcion,
       EX_SEXO    Codigo1,
       EX_FEC_NAC Fecha,
       EX_CALLE   Observaciones,
       EX_COLONIA Descripcion,
       EX_CIUDAD  Descripcion,
       EX_CODPOST Referencia,
       EX_ESTADO  Codigo2,
       EX_TEL     Descripcion,
       EX_FEC_INI Fecha,
       EX_EMB_INI Fecha,
       EX_EMB_FIN Fecha,
       EX_OBSERVA Memo,
       CB_CODIGO  NumeroEmpleado,
       US_CODIGO  Usuario,

       EX_G_TEX01 Formula,
       EX_G_TEX02 Formula,
       EX_G_TEX03 Formula,
       EX_G_TEX04 Formula,
       EX_G_TEX05 Formula,
       EX_G_TEX06 Formula,
       EX_G_TEX07 Formula,
       EX_G_TEX08 Formula,
       EX_G_TEX09 Formula,
       EX_G_TEX10 Formula,
       EX_G_TEX11 Formula,
       EX_G_TEX12 Formula,
       EX_G_TEX13 Formula,
       EX_G_TEX14 Formula,
       EX_G_TEX15 Formula,
       EX_G_TEX16 Formula,
       EX_G_TEX17 Formula,
       EX_G_TEX18 Formula,
       EX_G_TEX19 Formula,
       EX_G_TEX20 Formula,
       EX_G_TEX21 Formula,
       EX_G_TEX22 Formula,
       EX_G_TEX23 Formula,
       EX_G_TEX24 Formula,
       EX_G_TEX25 Formula,
       EX_G_TEX26 Formula,
       EX_G_TEX27 Formula,
       EX_G_TEX28 Formula,
       EX_G_TEX29 Formula,
       EX_G_TEX30 Formula,

       EX_G_LOG01 Booleano,
       EX_G_LOG02 Booleano,
       EX_G_LOG03 Booleano,
       EX_G_LOG04 Booleano,
       EX_G_LOG05 Booleano,
       EX_G_LOG06 Booleano,
       EX_G_LOG07 Booleano,
       EX_G_LOG08 Booleano,
       EX_G_LOG09 Booleano,
       EX_G_LOG10 Booleano,
       EX_G_LOG11 Booleano,
       EX_G_LOG12 Booleano,
       EX_G_LOG13 Booleano,
       EX_G_LOG14 Booleano,
       EX_G_LOG15 Booleano,
       EX_G_LOG16 Booleano,
       EX_G_LOG17 Booleano,
       EX_G_LOG18 Booleano,
       EX_G_LOG19 Booleano,
       EX_G_LOG20 Booleano,
       EX_G_LOG21 Booleano,
       EX_G_LOG22 Booleano,
       EX_G_LOG23 Booleano,
       EX_G_LOG24 Booleano,
       EX_G_LOG25 Booleano,
       EX_G_LOG26 Booleano,
       EX_G_LOG27 Booleano,
       EX_G_LOG28 Booleano,
       EX_G_LOG29 Booleano,
       EX_G_LOG30 Booleano,
       EX_G_LOG31 Booleano,
       EX_G_LOG32 Booleano,
       EX_G_LOG33 Booleano,
       EX_G_LOG34 Booleano,
       EX_G_LOG35 Booleano,
       EX_G_LOG36 Booleano,
       EX_G_LOG37 Booleano,
       EX_G_LOG38 Booleano,
       EX_G_LOG39 Booleano,
       EX_G_LOG40 Booleano,
       EX_G_LOG41 Booleano,
       EX_G_LOG42 Booleano,
       EX_G_LOG43 Booleano,
       EX_G_LOG44 Booleano,
       EX_G_LOG45 Booleano,
       EX_G_LOG46 Booleano,
       EX_G_LOG47 Booleano,
       EX_G_LOG48 Booleano,
       EX_G_LOG49 Booleano,
       EX_G_LOG50 Booleano,
       EX_G_LOG51 Booleano,
       EX_G_LOG52 Booleano,
       EX_G_LOG53 Booleano,
       EX_G_LOG54 Booleano,
       EX_G_LOG55 Booleano,
       EX_G_LOG56 Booleano,
       EX_G_LOG57 Booleano,
       EX_G_LOG58 Booleano,
       EX_G_LOG59 Booleano,
       EX_G_LOG60 Booleano,

       EX_G_NUM01 Pesos,
       EX_G_NUM02 Pesos,
       EX_G_NUM03 Pesos,
       EX_G_NUM04 Pesos,
       EX_G_NUM05 Pesos,
       EX_G_NUM06 Pesos,
       EX_G_NUM07 Pesos,
       EX_G_NUM08 Pesos,
       EX_G_NUM09 Pesos,
       EX_G_NUM10 Pesos,
       EX_G_NUM11 Pesos,
       EX_G_NUM12 Pesos,
       EX_G_NUM13 Pesos,
       EX_G_NUM14 Pesos,
       EX_G_NUM15 Pesos,
       EX_G_NUM16 Pesos,
       EX_G_NUM17 Pesos,
       EX_G_NUM18 Pesos,
       EX_G_NUM19 Pesos,
       EX_G_NUM20 Pesos,
       EX_G_NUM21 Pesos,
       EX_G_NUM22 Pesos,
       EX_G_NUM23 Pesos,
       EX_G_NUM24 Pesos,
       EX_G_NUM25 Pesos,
       EX_G_NUM26 Pesos,
       EX_G_NUM27 Pesos,
       EX_G_NUM28 Pesos,
       EX_G_NUM29 Pesos,
       EX_G_NUM30 Pesos,

       EX_G_BOL01 Booleano,
       EX_G_BOL02 Booleano,
       EX_G_BOL03 Booleano,
       EX_G_BOL04 Booleano,
       EX_G_BOL05 Booleano,
       EX_G_BOL06 Booleano,
       EX_G_BOL07 Booleano,
       EX_G_BOL08 Booleano,
       EX_G_BOL09 Booleano,
       EX_G_BOL10 Booleano,
       EX_G_BOL11 Booleano,
       EX_G_BOL12 Booleano,
       EX_G_BOL13 Booleano,
       EX_G_BOL14 Booleano,
       EX_G_BOL15 Booleano,
       EX_G_BOL16 Booleano,
       EX_G_BOL17 Booleano,
       EX_G_BOL18 Booleano,
       EX_G_BOL19 Booleano,
       EX_G_BOL20 Booleano,
       EX_G_BOL21 Booleano,
       EX_G_BOL22 Booleano,
       EX_G_BOL23 Booleano,
       EX_G_BOL24 Booleano,
       EX_G_BOL25 Booleano,
       EX_G_BOL26 Booleano,
       EX_G_BOL27 Booleano,
       EX_G_BOL28 Booleano,
       EX_G_BOL29 Booleano,
       EX_G_BOL30 Booleano,
       EX_G_BOL31 Booleano,
       EX_G_BOL32 Booleano,
       EX_G_BOL33 Booleano,
       EX_G_BOL34 Booleano,
       EX_G_BOL35 Booleano,
       EX_G_BOL36 Booleano,
       EX_G_BOL37 Booleano,
       EX_G_BOL38 Booleano,
       EX_G_BOL39 Booleano,
       EX_G_BOL40 Booleano,
       EX_G_BOL41 Booleano,
       EX_G_BOL42 Booleano,
       EX_G_BOL43 Booleano,
       EX_G_BOL44 Booleano,
       EX_G_BOL45 Booleano,
       EX_G_BOL46 Booleano,
       EX_G_BOL47 Booleano,
       EX_G_BOL48 Booleano,
       EX_G_BOL49 Booleano,
       EX_G_BOL50 Booleano,
       EX_G_BOL51 Booleano,
       EX_G_BOL52 Booleano,
       EX_G_BOL53 Booleano,
       EX_G_BOL54 Booleano,
       EX_G_BOL55 Booleano,
       EX_G_BOL56 Booleano,
       EX_G_BOL57 Booleano,
       EX_G_BOL58 Booleano,
       EX_G_BOL59 Booleano,
       EX_G_BOL60 Booleano,

       EX_G_DES01 Formula,
       EX_G_DES02 Formula,
       EX_G_DES03 Formula,
       EX_G_DES04 Formula,
       EX_G_DES05 Formula,
       EX_G_DES06 Formula,
       EX_G_DES07 Formula,
       EX_G_DES08 Formula,
       EX_G_DES09 Formula,
       EX_G_DES10 Formula,
       EX_G_DES11 Formula,
       EX_G_DES12 Formula,
       EX_G_DES13 Formula,
       EX_G_DES14 Formula,
       EX_G_DES15 Formula,
       EX_G_DES16 Formula,
       EX_G_DES17 Formula,
       EX_G_DES18 Formula,
       EX_G_DES19 Formula,
       EX_G_DES20 Formula,
       EX_G_DES21 Formula,
       EX_G_DES22 Formula,
       EX_G_DES23 Formula,
       EX_G_DES24 Formula,
       EX_G_DES25 Formula,
       EX_G_DES26 Formula,
       EX_G_DES27 Formula,
       EX_G_DES28 Formula,
       EX_G_DES29 Formula,
       EX_G_DES30 Formula,
       EX_G_DES31 Formula,
       EX_G_DES32 Formula,
       EX_G_DES33 Formula,
       EX_G_DES34 Formula,
       EX_G_DES35 Formula,
       EX_G_DES36 Formula,
       EX_G_DES37 Formula,
       EX_G_DES38 Formula,
       EX_G_DES39 Formula,
       EX_G_DES40 Formula,
       EX_G_DES41 Formula,
       EX_G_DES42 Formula,
       EX_G_DES43 Formula,
       EX_G_DES44 Formula,
       EX_G_DES45 Formula,
       EX_G_DES46 Formula,
       EX_G_DES47 Formula,
       EX_G_DES48 Formula,
       EX_G_DES49 Formula,
       EX_G_DES50 Formula,
       EX_G_DES51 Formula,
       EX_G_DES52 Formula,
       EX_G_DES53 Formula,
       EX_G_DES54 Formula,
       EX_G_DES55 Formula,
       EX_G_DES56 Formula,
       EX_G_DES57 Formula,
       EX_G_DES58 Formula,
       EX_G_DES59 Formula,
       EX_G_DES60 Formula,
		   EX_G_TEX31  Formula,
		   EX_G_TEX32  Formula,
		   EX_G_TEX33  Formula,		
		   EX_G_TEX34  Formula,		
		   EX_G_TEX35  Formula,		
		   EX_G_TEX36  Formula,		
		   EX_G_TEX37  Formula,		
		   EX_G_TEX38  Formula,		
		   EX_G_TEX39  Formula,		
		   EX_G_TEX40  Formula,		
		   EX_G_TEX41  Formula,		
		   EX_G_TEX42  Formula,		
		   EX_G_TEX43  Formula,		
		   EX_G_TEX44  Formula,		
		   EX_G_TEX45  Formula,		
		   EX_G_TEX46  Formula,		
		   EX_G_TEX47  Formula,		
		   EX_G_TEX48  Formula,		
		   EX_G_TEX49  Formula,		
		   EX_G_TEX50  Formula,		
		   EX_G_TEX51  Formula,		
		   EX_G_TEX52  Formula,		
		   EX_G_TEX53  Formula,		
		   EX_G_TEX54  Formula,		
		   EX_G_TEX55  Formula,		
		   EX_G_TEX56  Formula,		
		   EX_G_TEX57  Formula,		
		   EX_G_TEX58  Formula,		
		   EX_G_TEX59  Formula,		
		   EX_G_TEX60  Formula,	
		   EX_G_TEX61  Formula,		
		   EX_G_TEX62  Formula,		
		   EX_G_TEX63  Formula,		
		   EX_G_TEX64  Formula,		
		   EX_G_TEX65  Formula,		
		   EX_G_TEX66  Formula,		
		   EX_G_TEX67  Formula,		
		   EX_G_TEX68  Formula,		
		   EX_G_TEX69  Formula,		
		   EX_G_TEX70  Formula,		
		   EX_G_TEX71  Formula,		
		   EX_G_TEX72  Formula,		
		   EX_G_TEX73  Formula,		
		   EX_G_TEX74  Formula,		
		   EX_G_TEX75  Formula,		
		   EX_G_TEX76  Formula,		
		   EX_G_TEX77  Formula,		
		   EX_G_TEX78  Formula,		
		   EX_G_TEX79  Formula,		
		   EX_G_TEX80  Formula,		
		   EX_G_TEX81  Formula,		
		   EX_G_TEX82  Formula,		
		   EX_G_TEX83  Formula,		
		   EX_G_TEX84  Formula,		
		   EX_G_TEX85  Formula,		
		   EX_G_TEX86  Formula,		
		   EX_G_TEX87  Formula,		
		   EX_G_TEX88  Formula,		
		   EX_G_TEX89  Formula,		
		   EX_G_TEX90  Formula,		
		   EX_G_TEX91  Formula,		
		   EX_G_TEX92  Formula,		
		   EX_G_TEX93  Formula,		
		   EX_G_TEX94  Formula,		
		   EX_G_TEX95  Formula,		
		   EX_G_TEX96  Formula,		
		   EX_G_TEX97  Formula,
       EX_G_TEX98  Formula,		
		   EX_G_TEX99  Formula,		
	
		   EX_G_LOG61 Booleano,		
		   EX_G_LOG62 Booleano,		
		   EX_G_LOG63 Booleano,		
		   EX_G_LOG64 Booleano,		
		   EX_G_LOG65 Booleano,		
		   EX_G_LOG66 Booleano,		
		   EX_G_LOG67 Booleano,		
		   EX_G_LOG68 Booleano,		
		   EX_G_LOG69 Booleano,		
		   EX_G_LOG70 Booleano,		
		   EX_G_LOG71 Booleano,		
		   EX_G_LOG72 Booleano,		
		   EX_G_LOG73 Booleano,		
		   EX_G_LOG74 Booleano,		
		   EX_G_LOG75 Booleano,		
		   EX_G_LOG76 Booleano,		
		   EX_G_LOG77 Booleano,		
		   EX_G_LOG78 Booleano,		
		   EX_G_LOG79 Booleano,		
		   EX_G_LOG80 Booleano,		
		   EX_G_LOG81 Booleano,		
		   EX_G_LOG82 Booleano,		
		   EX_G_LOG83 Booleano,		
		   EX_G_LOG84 Booleano,		
		   EX_G_LOG85 Booleano,		
		   EX_G_LOG86 Booleano,		
		   EX_G_LOG87 Booleano,		
		   EX_G_LOG88 Booleano,		
		   EX_G_LOG89 Booleano,		
		   EX_G_LOG90 Booleano,		
		   EX_G_LOG91 Booleano,		
		   EX_G_LOG92 Booleano,		
		   EX_G_LOG93 Booleano,		
		   EX_G_LOG94 Booleano,		
		   EX_G_LOG95 Booleano,		
		   EX_G_LOG96 Booleano,		
		   EX_G_LOG97 Booleano,		
		   EX_G_LOG98 Booleano,		
		   EX_G_LOG99 Booleano,		
	
		   EX_G_BOL61 Booleano,		
		   EX_G_BOL62 Booleano,		
		   EX_G_BOL63 Booleano,		
		   EX_G_BOL64 Booleano,		
		   EX_G_BOL65 Booleano,		
		   EX_G_BOL66 Booleano,		
		   EX_G_BOL67 Booleano,		
		   EX_G_BOL68 Booleano,		
		   EX_G_BOL69 Booleano,		
		   EX_G_BOL70 Booleano,		
		   EX_G_BOL71 Booleano,		
		   EX_G_BOL72 Booleano,		
		   EX_G_BOL73 Booleano,		
		   EX_G_BOL74 Booleano,		
		   EX_G_BOL75 Booleano,		
		   EX_G_BOL76 Booleano,		
		   EX_G_BOL77 Booleano,		
		   EX_G_BOL78 Booleano,		
		   EX_G_BOL79 Booleano,		
		   EX_G_BOL80 Booleano,		
		   EX_G_BOL81 Booleano,		
		   EX_G_BOL82 Booleano,		
		   EX_G_BOL83 Booleano,		
		   EX_G_BOL84 Booleano,		
		   EX_G_BOL85 Booleano,		
		   EX_G_BOL86 Booleano,		
		   EX_G_BOL87 Booleano,		
		   EX_G_BOL88 Booleano,		
		   EX_G_BOL89 Booleano,		
		   EX_G_BOL90 Booleano,		
		   EX_G_BOL91 Booleano,		
		   EX_G_BOL92 Booleano,		
		   EX_G_BOL93 Booleano,		
		   EX_G_BOL94 Booleano,		
		   EX_G_BOL95 Booleano,		
		   EX_G_BOL96 Booleano,		
		   EX_G_BOL97 Booleano,		
		   EX_G_BOL98 Booleano,		
		   EX_G_BOL99 Booleano,		
	
		   EX_G_DES61  Formula,		
		   EX_G_DES62  Formula,		
		   EX_G_DES63  Formula,		
		   EX_G_DES64  Formula,		
		   EX_G_DES65  Formula,		
		   EX_G_DES66  Formula,		
		   EX_G_DES67  Formula,		
		   EX_G_DES68  Formula,		
		   EX_G_DES69  Formula,		
		   EX_G_DES70  Formula,		
		   EX_G_DES71  Formula,		
		   EX_G_DES72  Formula,		
		   EX_G_DES73  Formula,		
		   EX_G_DES74  Formula,		
		   EX_G_DES75  Formula,		
		   EX_G_DES76  Formula,		
		   EX_G_DES77  Formula,		
		   EX_G_DES78  Formula,		
		   EX_G_DES79  Formula,		
		   EX_G_DES80  Formula,	
		   EX_G_DES81  Formula,		
		   EX_G_DES82  Formula,		
		   EX_G_DES83  Formula,		
		   EX_G_DES84  Formula,		
		   EX_G_DES85  Formula,		
		   EX_G_DES86  Formula,		
		   EX_G_DES87  Formula,		
		   EX_G_DES88  Formula,		
		   EX_G_DES89  Formula,		
		   EX_G_DES90  Formula,		
		   EX_G_DES91  Formula,		
		   EX_G_DES92  Formula,		
		   EX_G_DES93  Formula,		
		   EX_G_DES94  Formula,		
		   EX_G_DES95  Formula,		
		   EX_G_DES96  Formula,		
		   EX_G_DES97  Formula,		
		   EX_G_DES98  Formula,		
		   EX_G_DES99  Formula,
       EX_NUM_EXT  NombreCampo,
		   EX_NUM_INT  NombreCampo,
       PRETTY_EXP AS (EX_APE_PAT + ' ' + EX_APE_MAT + ', ' + EX_NOMBRES),
       EX_TSANGRE Descripcion,
       EX_ALERGIA Formula
)
GO

ALTER TABLE EXPEDIEN
       ADD CONSTRAINT PK_EXPEDIEN PRIMARY KEY (EX_CODIGO)
GO

CREATE TABLE ACCIDENT (
       EX_CODIGO            NumeroEmpleado,
       AX_FECHA             Fecha,
       AX_FEC_REG           Fecha,
       AX_CAUSA             Codigo,
       AX_MOTIVO            Codigo,
       AX_HORA              Hora,
       AX_TIP_ACC           Codigo,
       AX_FEC_SUS           Fecha,
       AX_HOR_SUS           Hora,
       AX_PER_REG           Descripcion,
       AX_TIP_LES           Status,
       AX_DESCRIP           Memo,
       AX_ATENDIO           Status,
       AX_OBSERVA           Observaciones,
       AX_NUM_INC           Descripcion,
       US_CODIGO            Usuario,
       AX_TIPO              Status,
       AX_FEC_COM           Fecha,
       AX_HOR_COM           Hora,
       AX_RIESGO            Booleano,
       AX_INCAPA            Booleano,
       AX_DAN_MAT           Booleano,
       AX_INF_ACC           Memo,
       AX_INF_TES           Memo,
       AX_INF_SUP           Memo,
       AX_NUM_TES           NumeroEmpleado,
       AX_NUMERO            Descripcion
)
GO

ALTER TABLE ACCIDENT
       ADD CONSTRAINT PK_ACCIDENT PRIMARY KEY (EX_CODIGO, AX_FECHA)
GO

ALTER TABLE ACCIDENT
       ADD CONSTRAINT FK_ACCIDENT_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TABLE EMBARAZO (
       EX_CODIGO            NumeroEmpleado,
       EM_FEC_UM            Fecha,
       EM_FEC_PP            Fecha,
       EM_FINAL             Booleano,
       EM_FEC_FIN           Fecha,
       EM_TERMINO           Status,
       EM_NORMAL            Booleano,
       EM_MORTAL            Booleano,
       EM_COMENTA           Observaciones,
       EM_PRENAT            Fecha,
       EM_POSNAT            Fecha,
       EM_RIESGO            Booleano,
       EM_INC_INI           Fecha,
       EM_INC_FIN           Fecha,
       EM_OBS_RIE           Observaciones,
       US_CODIGO            Usuario
)
GO

ALTER TABLE EMBARAZO
       ADD CONSTRAINT PK_EMBARAZO PRIMARY KEY (EX_CODIGO, EM_FEC_UM)
GO

ALTER TABLE EMBARAZO
       ADD CONSTRAINT FK_EMBARAZO_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TABLE MEDICINA (
       ME_CODIGO            Codigo,
       ME_NOMBRE            Descripcion,
       ME_TIPO              Status,
       ME_INGLES            Descripcion,
       ME_NUMERO            Pesos,
       ME_TEXTO             Descripcion,
       ME_MEDIDA            Referencia,
       ME_DESCRIP           Observaciones
)
GO

ALTER TABLE MEDICINA
       ADD CONSTRAINT PK_MEDICINA PRIMARY KEY (ME_CODIGO)
GO

CREATE TABLE MED_ENTR (
       ME_CODIGO            Codigo,
       EX_CODIGO            NumeroEmpleado,
       MT_FECHA             Fecha,
       MT_CANTIDA           Pesos,
       MT_COMENTA           Observaciones,
       US_CODIGO            Usuario
)
GO

ALTER TABLE MED_ENTR
       ADD CONSTRAINT PK_MED_ENTR PRIMARY KEY (ME_CODIGO, MT_FECHA, EX_CODIGO)
GO

ALTER TABLE MED_ENTR
       ADD CONSTRAINT FK_MED_ENTR_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE MED_ENTR
       ADD CONSTRAINT FK_MED_ENTR_MEDICINA
       FOREIGN KEY (ME_CODIGO)
       REFERENCES MEDICINA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TABLE TESTUDIO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

ALTER TABLE TESTUDIO
       ADD CONSTRAINT PK_TESTUDIO PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE DIAGNOST (
       DA_CODIGO            Codigo,
       DA_NOMBRE            Descripcion,
       DA_TIPO              Status,
       DA_INGLES            Descripcion,
       DA_NUMERO            Pesos,
       DA_TEXTO             Descripcion
)
GO

ALTER TABLE DIAGNOST
       ADD CONSTRAINT PK_DIAGNOST PRIMARY KEY (DA_CODIGO)
GO

CREATE TABLE TCONSLTA (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion,
       TB_TIPO              Status
)
GO

ALTER TABLE TCONSLTA
       ADD CONSTRAINT PK_TCONSLTA PRIMARY KEY (TB_CODIGO)
GO

CREATE TABLE CONSULTA (
       EX_CODIGO            NumeroEmpleado,
       CN_FECHA             Fecha,
       CN_HOR_INI           Hora,
       DA_CODIGO            Codigo,
       CN_TIPO              Codigo,
       CN_MOTIVO            Observaciones,
       CN_SINTOMA           Memo,
       CN_EXPLORA           Memo,
       CN_OBSERVA           Memo,
       CN_DIAGNOS           Memo,
       CN_PULSO             FolioChico,
       CN_RESPIRA           FolioChico,
       CN_PESO              Pesos,
       CN_TEMPERA           FolioChico,
       CN_ALTURA            Pesos,
       CN_PRE_SIS           FolioChico,
       CN_PRE_DIA           FolioChico,
       CN_CUIDADO           Memo,
       CN_RECETA            Memo,
       CN_AV_IZQ            FolioChico,
       CN_AV_DER            FolioChico,
       CN_EST_TIP           Codigo,
       CN_EST_DES           Observaciones,
       CN_EST_HAY           Booleano,
       CN_EST_FEC           Fecha,
       CN_EST_RES           Memo,
       CN_EST_OBS           Memo,
       CN_HOR_FIN           Hora,
       CN_IMSS              Booleano,
       US_CODIGO            Usuario,
       CN_FEC_MOD           Fecha,
       CN_SUB_SEC           Booleano,
       CN_D_EXT             Codigo,
       CN_D_BLOB            Imagen,
       CN_D_OBS             Descripcion
)
GO

ALTER TABLE CONSULTA
       ADD CONSTRAINT PK_CONSULTA PRIMARY KEY (EX_CODIGO, CN_FECHA, CN_TIPO)
GO

ALTER TABLE CONSULTA
       ADD CONSTRAINT FK_CONSULTA_EXPEDIEN
       FOREIGN KEY (EX_CODIGO)
       REFERENCES EXPEDIEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TABLE GRUPO_EX (
       GX_CODIGO            Codigo,
       GX_TITULO            Observaciones,
       GX_POSICIO           FolioChico
)
GO

ALTER TABLE GRUPO_EX
       ADD CONSTRAINT PK_GRUPO_EX PRIMARY KEY (GX_CODIGO)
GO

CREATE TABLE CAMPO_EX (
       CX_NOMBRE            NombreCampo,
       GX_CODIGO            Codigo,
       CX_POSICIO           FolioChico,
       CX_TITULO            Descripcion,
       CX_TIPO              Status,
       CX_DEFAULT           Formula,
       CX_MOSTRAR           Status
)
GO

ALTER TABLE CAMPO_EX
       ADD CONSTRAINT PK_CAMPO_EX PRIMARY KEY (CX_NOMBRE)
GO





