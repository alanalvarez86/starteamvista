
CREATE DEFAULT BooleanoNO
	AS 'N'
go

CREATE DEFAULT Cero
	AS 0
go

CREATE DEFAULT CodigoVacio
	AS ''
go

CREATE DEFAULT FechaVacia
	AS '12/30/1899'
go


CREATE DEFAULT StringVacio
	AS ''
go
