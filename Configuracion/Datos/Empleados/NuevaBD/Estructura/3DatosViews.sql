/* Se agrega el campo llave despues de que crea todas las estructuras de tipo tabla  */
CREATE PROCEDURE AGREGA_LLAVES
AS
BEGIN
	SET NOCOUNT ON
	declare @NombreTabla Formula
	DECLARE @Comando NVarchar(4000)

	declare Temporal CURSOR FOR
    	select table_name from information_schema.tables where 
	( TABLE_TYPE = 'BASE TABLE' ) and 
	( TABLE_NAME not like 'TMP_%' ) 
	order by TABLE_NAME
	OPEN Temporal
   	FETCH NEXT FROM Temporal INTO @NombreTabla

	WHILE @@FETCH_STATUS = 0
    	BEGIN
		if ( IDENT_CURRENT ( @NombreTabla ) is NULL ) 
		begin
			set @Comando = N'alter table #TABLA add LLAVE Int identity( 1, 1)'
		
			set @Comando = REPLACE( @Comando, '#TABLA', @NombreTabla )
   		
			EXEC SP_EXECUTESQL @Comando 
		end
		FETCH NEXT FROM Temporal INTO @NombreTabla
    	END
	close Temporal
        deallocate Temporal

END
GO


exec AGREGA_LLAVES
GO


drop procedure AGREGA_LLAVES
GO

/* Inicia la creaci�n de views */

create view ALTABAJA (
  CB_CODIGO,
  CB_PATRON,
  CB_ALTA,
  CB_BAJA ) as
select
  K1.CB_CODIGO,
  K1.CB_PATRON,
  K1.CB_FECHA CB_ALTA,
  ( select MIN( K2.CB_FECHA ) from KARDEX K2 where
  ( K2.CB_CODIGO = K1.CB_CODIGO ) and
  ( K2.CB_TIPO = 'BAJA' ) and
  ( K2.CB_PATRON = K1.CB_PATRON ) and
  ( K2.CB_FECHA >= K1.CB_FECHA ) ) CB_BAJA
from KARDEX K1 where ( K1.CB_TIPO = 'ALTA' )
GO

create VIEW CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CB_NIVEL0,
  CU_CODIGO,
  KC_FEC_PRO,
  KC_EVALUA,
  KC_FEC_TOM,
  KC_HORAS,
  CU_HORAS,
  EN_OPCIONA,
  EN_LISTA,
  MA_CODIGO,
  KC_PROXIMO,
  KC_REVISIO,
  CU_REVISIO,
  CP_MANUAL,
  EP_GLOBAL,
  CU_CLASIFI,
  CU_CLASE
) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  C.CB_NIVEL0,
  CURSO.CU_CODIGO,
 ( Select case when (KARCURSO.KC_FEC_TOM IS NULL )then DATEADD( DAY, E.EN_DIAS, C.CB_FEC_PTO ) else KARCURSO.KC_FECPROG end )KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO,
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_MANUAL,
  cast( 'N' as CHAR(1) ) EP_GLOBAL,
  CURSO.CU_CLASIFI,
  CURSO.CU_CLASE
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' )
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  C.CB_NIVEL0,
  CU.CU_CODIGO,
  ( select case when K.KC_FEC_TOM is null then NULL
		        when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) OR ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' )and ( K.KC_REVISIO = CU.CU_REVISIO ) ) )then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' ) and ( K.KC_FEC_TOM > ( select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) ) ) and (K.KC_REVISIO <> CU.CU_REVISIO ))then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				else (select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) )End ) KC_FEC_PRO,
  cast(0 as Decimal(15,2)) KC_EVALUA,
  cast(NULL as DateTime) KC_FEC_TOM,
  cast(0 as Decimal(15,2)) KC_HORAS,
  CU.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CU.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS DateTime))) KC_PROXIMO,
  cast(' ' as varchar(10))KC_REVISIO,               
  CU.CU_REVISIO,                  
  cast( 'N' as CHAR(1) ) CP_MANUAL,  
  cast( 'N' as CHAR(1) ) EP_GLOBAL,
  CU.CU_CLASIFI,
  CU_CLASE
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO K on ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CU_CODIGO = E.CU_CODIGO ) and ( K.KC_FEC_TOM = ( select MAX( KC_FEC_TOM ) from KARCURSO AS K1 where ( K1.CB_CODIGO = C.CB_CODIGO ) and ( K1.CU_CODIGO = E.CU_CODIGO ) ))
  left join CURSO CU on ( CU.CU_CODIGO = E.CU_CODIGO )
where ( Cu.CU_ACTIVO = 'S' ) and
		( ( E.EN_RE_DIAS > 0 ) AND NOT ( K.KC_FEC_TOM is NULL) or
        ( ( E.EN_REPROG = 'S' ) and ( K.KC_REVISIO <> CU.CU_REVISIO ) and (
          ( select count(k2.KC_REVISIO) FROM karcurso k2 where k2.KC_REVISIO = CU.CU_REVISIO AND ( k2.CB_CODIGO = C.CB_CODIGO ) and ( k2.CU_CODIGO = E.CU_CODIGO ) ) = 0 ) ))
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  C.CB_NIVEL0,
  CURSO.CU_CODIGO,
  (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end	)KC_FEC_PRO,
  K.KC_EVALUA,
  K.KC_FEC_TOM,
  K.KC_HORAS,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM dbo.SESION WHERE (CU_CODIGO = EP.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO,
  K.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_MANUAL,	
  EP.EP_GLOBAL,
  CURSO.CU_CLASIFI,
  CURSO.CU_CLASE
  from COLABORA C
  join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
  left join KARCURSO K on ( K.CU_CODIGO = EP.CU_CODIGO and EP.CB_CODIGO = K.CB_CODIGO and K.KC_FEC_TOM >= (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end	) )
where ( CURSO.CU_ACTIVO = 'S' )

GO

CREATE VIEW DEFECT_V(
       CI_FOLIO,
       DE_FOLIO,
       DE_CODIGO,
       DE_PIEZAS,
       DE_COMENTA,
       CI_AREA,
       CE_FOLIO,
       WO_NUMBER,
       AR_CODIGO,
       CI_FECHA,
       CI_HORA,
       US_CODIGO,
       CI_TIPO,
       CI_RESULT,
       CI_COMENTA,
       CI_OBSERVA,
       CI_TAMLOTE,
       CI_MUESTRA,
       CI_TIEMPO,
       CI_NUMERO1,
       CI_NUMERO2 ) as
select D.CI_FOLIO,
       D.DE_FOLIO,
       D.DE_CODIGO,
       D.DE_PIEZAS,
       D.DE_COMENTA,
       C.CI_AREA,
       C.CE_FOLIO,
       C.WO_NUMBER,
       C.AR_CODIGO,
       C.CI_FECHA,
       C.CI_HORA,
       C.US_CODIGO,
       C.CI_TIPO,
       C.CI_RESULT,
       C.CI_COMENTA,
       C.CI_OBSERVA,
       C.CI_TAMLOTE,
       C.CI_MUESTRA,
       C.CI_TIEMPO,
       C.CI_NUMERO1,
       C.CI_NUMERO2
from DEFECTO D left outer join CED_INSP C on ( C.CI_FOLIO = D.CI_FOLIO )
GO

create view SCRAP_V(
       CS_FOLIO,
       SC_FOLIO,
       CN_CODIGO,
       SC_MOTIVO,
       DE_PIEZAS,
       DE_COMENTA,
       CS_AREA,
       CS_FECHA,
       CS_HORA,
       WO_NUMBER,
       AR_CODIGO,
       OP_NUMBER,
       CS_FEC_FAB,
       CS_TAMLOTE,
       CS_COMENTA,
       CS_OBSERVA,
       CS_NUMERO1,
       CS_NUMERO2,
       CS_TEXTO1,
       CS_TEXTO2,
       US_CODIGO  ) as
select S.CS_FOLIO,
       S.SC_FOLIO,
       S.CN_CODIGO,
       S.SC_MOTIVO,
       S.SC_PIEZAS,
       S.SC_COMENTA,
       C.CS_AREA,
       C.CS_FECHA,
       C.CS_HORA,
       C.WO_NUMBER,
       C.AR_CODIGO,
       C.OP_NUMBER,
       C.CS_FEC_FAB,
       C.CS_TAMLOTE,
       C.CS_COMENTA,
       C.CS_OBSERVA,
       C.CS_NUMERO1,
       C.CS_NUMERO2,
       C.CS_TEXTO1,
       C.CS_TEXTO2,
       C.US_CODIGO
from SCRAP S left outer join CEDSCRAP C on ( C.CS_FOLIO = S.CS_FOLIO )
GO

create view AHO_PRE( CB_CODIGO, FECHA, PRIORIDAD, TIPO, PR_TIPO, PR_REFEREN, PR_MONTO, PR_SALDO, PR_FORMULA )
as
select
A.CB_CODIGO CB_CODIGO,
A.AH_FECHA FECHA,
TA.TB_PRIORID PRIORIDAD,
CAST ( ( 0 ) as INTEGER ) TIPO,
A.AH_TIPO PR_TIPO,
CAST( ( '' ) as VARCHAR( 8 ) ) PR_REFEREN,
CAST ( ( 0 ) as NUMERIC( 15,2 ) ) PR_MONTO,
CAST ( ( 0 ) as NUMERIC( 15,2 ) ) PR_SALDO,
A.AH_FORMULA PR_FORMULA
from AHORRO A
left outer join TAHORRO TA on ( TA.TB_CODIGO = A.AH_TIPO )
where ( A.AH_STATUS = 0 )
union
select
P.CB_CODIGO CB_CODIGO,
P.PR_FECHA FECHA,
TP.TB_PRIORID PRIORIDAD,
CAST ( ( 1 ) as INTEGER ) TIPO,
P.PR_TIPO,
P.PR_REFEREN,
P.PR_MONTO,
P.PR_SALDO,
P.PR_FORMULA
from PRESTAMO P
left outer join TPRESTA TP on ( TP.TB_CODIGO = P.PR_TIPO )
where ( P.PR_STATUS = 0 )
GO

create view ENT_NAC( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO, TB_CURP )
as
select TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO, TB_CURP from ENTIDAD
GO

create view ULTIMO_EMPLEADO( CB_CODIGO ) as
select MAX( CB_CODIGO ) from COLABORA
GO

create view EMPLEADOS_X_VERIFICAR(
        CB_CODIGO,
        CB_APE_PAT,
        CB_APE_MAT,
        CB_NOMBRES,
        CB_CURP,
        CB_RFC,
        CB_SEGSOC,
        CB_SEXO,
        CB_FEC_NAC,
        CB_ACTIVO,
        CB_RECONTR,
        PRETTYNAME,
        FOTO,
        REFERENCIA )
as
     select C.CB_CODIGO, C.CB_APE_PAT, C.CB_APE_MAT, C.CB_NOMBRES, C.CB_CURP, C.CB_RFC, C.CB_SEGSOC,
            C.CB_SEXO, C.CB_FEC_NAC, C.CB_ACTIVO, C.CB_RECONTR, C.PRETTYNAME, I.IM_BLOB as FOTO, CAST( '' as varchar(30) ) as REFERENCIA
     from COLABORA C
     left outer join IMAGEN I on ( I.CB_CODIGO = C.CB_CODIGO ) and ( I.IM_TIPO = 'FOTO' )
GO



create VIEW R_DATOSDEF( EN_CODIGO, RD_ORDEN , RD_ENTIDAD, AT_CAMPO, RD_VERSION, US_CODIGO )
AS
select EN_CODIGO, RD_ORDEN, RD_ENTIDAD, AT_CAMPO, RD_VERSION,US_CODIGO from r_default
union 
select EN_CODIGO, RO_ORDEN, RO_ENTIDAD, AT_CAMPO, RO_VERSION,US_CODIGO from r_orden
union 
select EN_CODIGO, RF_ORDEN, RF_ENTIDAD, AT_CAMPO, RF_VERSION,US_CODIGO from r_filtro
GO


CREATE VIEW V_USUARIO( US_CODIGO,
				  US_CORTO ,
 				  GR_CODIGO,
                      US_NIVEL ,
                      US_NOMBRE,
                      US_BLOQUEA,
                      US_CAMBIA,
                      US_FEC_IN,
                      US_FEC_OUT,
                      US_DENTRO,
                      US_ARBOL,
                      US_FORMA,
                      US_BIT_LST,
                      US_BIO_ID,
                      US_FEC_SUS,
                      US_FALLAS,
                      US_MAQUINA,
                      US_FEC_PWD,
                      US_EMAIL,
                      US_FORMATO,
                      US_LUGAR,
                      US_PAGINAS,
                      CM_CODIGO,
                      CB_CODIGO,
                      US_ANFITRI,
                      US_PORTAL,
                      US_ACTIVO,
                      US_DOMAIN,
                      US_JEFE
                               )

as

      SELECT      US_CODIGO,
                  US_CORTO ,
                  GR_CODIGO,
                  US_NIVEL ,
                  US_NOMBRE,
                  US_BLOQUEA,
                  US_CAMBIA ,
                  US_FEC_IN ,
                  US_FEC_OUT,
                  US_DENTRO ,
                  US_ARBOL  ,
                  US_FORMA  ,
                  US_BIT_LST,
                  US_BIO_ID ,
                  US_FEC_SUS,
                  US_FALLAS ,
                  US_MAQUINA,
                  US_FEC_PWD,
                  US_EMAIL  ,
                  US_FORMATO,
                  US_LUGAR  ,
                  US_PAGINAS,
                  CM_CODIGO ,
                  CB_CODIGO ,
                  US_ANFITRI,
                  US_PORTAL,
                  US_ACTIVO,
                  US_DOMAIN,
                  US_JEFE

      FROM #COMPARTE.dbo.USUARIO
GO

CREATE VIEW V_GRUPO(
				GR_CODIGO,
				GR_DESCRIP,
				GR_PADRE )
as
	SELECT 	GR_CODIGO,
			GR_DESCRIP,
			GR_PADRE
	FROM #COMPARTE.dbo.GRUPO
GO

CREATE VIEW V_PRINTER (
				       PI_NOMBRE ,
				       PI_EJECT  ,
				       PI_CHAR_10,
				       PI_CHAR_12,
				       PI_CHAR_17,
				       PI_EXTRA_1,
				       PI_EXTRA_2,
				       PI_RESET  ,
				       PI_UNDE_ON,
				       PI_UNDE_OF,
				       PI_BOLD_ON,
				       PI_BOLD_OF,
				       PI_6_LINES,
				       PI_8_LINES,
				       PI_ITAL_ON,
				       PI_ITAL_OF,
				       PI_LANDSCA )
as
	Select PI_NOMBRE ,
		   PI_EJECT  ,
		   PI_CHAR_10,
		   PI_CHAR_12,
		   PI_CHAR_17,
		   PI_EXTRA_1,
		   PI_EXTRA_2,
		   PI_RESET  ,
		   PI_UNDE_ON,
		   PI_UNDE_OF,
		   PI_BOLD_ON,
		   PI_BOLD_OF,
		   PI_6_LINES,
		   PI_8_LINES,
		   PI_ITAL_ON,
		   PI_ITAL_OF,
		   PI_LANDSCA
	FROM #COMPARTE.dbo.PRINTER
GO

CREATE VIEW V_POLL(
				PO_LINX,
				PO_EMPRESA,
				PO_NUMERO,
				PO_FECHA,
				PO_HORA,
				PO_LETRA )
AS
SELECT 	PO_LINX,
		PO_EMPRESA,
		PO_NUMERO,
		PO_FECHA,
		PO_HORA,
		PO_LETRA
FROM #Comparte.dbo.POLL
GO

CREATE VIEW V_NIVEL0(
					TB_CODIGO,
					TB_ELEMENT )
as
SELECT 	TB_CODIGO,
		TB_ELEMENT
FROM #Comparte.dbo.NIVEL0
GO

CREATE VIEW V_COMPANY (
					CM_CODIGO ,
					CM_NOMBRE,
					CM_ACUMULA,
					CM_CONTROL,
					CM_EMPATE,
					CM_ALIAS,
					CM_USRNAME,
					CM_USACAFE,
					CM_NIVEL0,
					CM_DATOS,
					CM_DIGITO,
					CM_KCLASIFI,
          CM_KCONFI,
          CM_KUSERS,
          CM_USACASE
					 )
AS
SELECT
		CM_CODIGO,
		CM_NOMBRE,
		CM_ACUMULA,
		CM_CONTROL,
		CM_EMPATE,
		CM_ALIAS,
		CM_USRNAME,
		CM_USACAFE,
		CM_NIVEL0,
		CM_DATOS,
		CM_DIGITO,
		CM_KCLASIFI,
    CM_KCONFI,
    CM_KUSERS,
    CM_USACASE
FROM #COMPARTE..COMPANY
GO

CREATE VIEW V_BITACORA(
					US_CODIGO,
					BI_FECHA,
					BI_HORA,
					BI_PROC_ID,
					BI_TIPO,
					BI_NUMERO,
					BI_TEXTO,
					CB_CODIGO,
					BI_DATA,
					BI_CLASE,
					BI_FEC_MOV )
as
SELECT 	US_CODIGO,
		BI_FECHA,
		BI_HORA,
		BI_PROC_ID,
		BI_TIPO,
		BI_NUMERO,
		BI_TEXTO,
		CB_CODIGO,
		BI_DATA,
		BI_CLASE,
		BI_FEC_MOV
FROM #Comparte.dbo.BITACORA
GO

CREATE VIEW V_ACCESO(
					GR_CODIGO,
					CM_CODIGO,
					AX_NUMERO,
					AX_DERECHO )
AS
SELECT 	GR_CODIGO,
		CM_CODIGO,
		AX_NUMERO,
		AX_DERECHO
FROM #Comparte.dbo.ACCESO
GO

CREATE VIEW V_VSUBFACT
AS
select
	VSUBFACT.*,
	(select COUNT(*) from VALNIVEL VN where VN.VL_CODIGO = VSUBFACT.VL_CODIGO and VN.VS_ORDEN = VSUBFACT.VS_ORDEN  )  VS_NUM_NIV,
	(select MIN(VN_PUNTOS) from VALNIVEL VN where VN.VL_CODIGO = VSUBFACT.VL_CODIGO and VN.VS_ORDEN = VSUBFACT.VS_ORDEN  )  VS_MIN_PTS,
	(select MAX(VN_PUNTOS) from VALNIVEL VN where VN.VL_CODIGO = VSUBFACT.VL_CODIGO and VN.VS_ORDEN = VSUBFACT.VS_ORDEN  )  VS_MAX_PTS   
from
	VSUBFACT
GO

CREATE VIEW V_VFACTOR
AS
select
	VFACTOR.*,
	(select COUNT(*) from VSUBFACT VS where VS.VL_CODIGO = VFACTOR.VL_CODIGO and VS.VF_ORDEN = VFACTOR.VF_ORDEN ) VF_NUM_SUB,
	(select SUM(VS_MIN_PTS) from V_VSUBFACT VS where VS.VL_CODIGO = VFACTOR.VL_CODIGO and VS.VF_ORDEN = VFACTOR.VF_ORDEN  )  VF_MIN_PTS,
	(select SUM(VS_MAX_PTS) from V_VSUBFACT VS where VS.VL_CODIGO = VFACTOR.VL_CODIGO and VS.VF_ORDEN = VFACTOR.VF_ORDEN  )  VF_MAX_PTS  
from
	VFACTOR
GO

CREATE VIEW V_VALPLANT
AS
select
	VALPLANT.*,
	(select COUNT(*) from VFACTOR where VFACTOR.VL_CODIGO = VALPLANT.VL_CODIGO) VL_NUM_FAC,
	(select COUNT(*) from VSUBFACT where VSUBFACT.VL_CODIGO = VALPLANT.VL_CODIGO) VL_NUM_SUB,
	(select SUM(VS_MIN_PTS) from V_VSUBFACT VS where VS.VL_CODIGO = VALPLANT.VL_CODIGO )  VL_MIN_PTS,
	(select SUM(VS_MAX_PTS) from V_VSUBFACT VS where VS.VL_CODIGO = VALPLANT.VL_CODIGO )  VL_MAX_PTS,
	(select COUNT(*) from VAL_PTO where VAL_PTO.VL_CODIGO = VALPLANT.VL_CODIGO) VL_NUM_PTO            
from
	VALPLANT
GO

CREATE VIEW V_VPUNTOS
AS
select
	VPUNTOS.VP_FOLIO,VPUNTOS.VT_ORDEN,VPUNTOS.VT_CUAL,VPUNTOS.VT_COMENTA, 
	COALESCE( VN_PUNTOS, 0 ) VT_PUNTOS,
	VN.*                                               
from
	VPUNTOS
		join VAL_PTO VP on VPUNTOS.VP_FOLIO = VP.VP_FOLIO
		left join VALNIVEL VN  on VN.VL_CODIGO = VP.VL_CODIGO and VN.VS_ORDEN = VPUNTOS.VT_ORDEN and VN.VN_ORDEN = VPUNTOS.VT_CUAL
GO

CREATE FUNCTION VAL_FOLIO_ULTIMA( @PU_CODIGO Codigo )
RETURNS FolioGrande
AS
BEGIN
	RETURN COALESCE( (select TOP 1 VP_FOLIO from VAL_PTO where PU_CODIGO = @PU_CODIGO order by VP_FECHA desc), 0 )
END
GO

CREATE VIEW V_VAL_PTO
AS
select
	VAL_PTO.*,
	(select COUNT(*) from VPUNTOS VT where VT.VP_FOLIO = VAL_PTO.VP_FOLIO and VT_CUAL > 0 ) VP_NUM_FIN,
	(select SUM(VT_PUNTOS) from V_VPUNTOS VT where VT.VP_FOLIO = VAL_PTO.VP_FOLIO ) VP_PUNTOS,
	CASE WHEN VP_FOLIO = dbo.VAL_FOLIO_ULTIMA( PU_CODIGO ) THEN 'S' else 'N' END VP_ULTIMA               
from
	VAL_PTO
GO

DROP FUNCTION VAL_FOLIO_ULTIMA
GO

CREATE VIEW V_BITKIOSC( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO

GO

CREATE VIEW V_BITMISD( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO
GO

CREATE VIEW  V_CURSO (
			CU_CODIGO,
			CU_FOLIO,
			CU_NOMBRE,
			CU_ACTIVO,
			MA_CODIGO,
			CU_HORAS,
			CU_REVISIO,
			CU_COSTO1,
			CU_COSTO2,
			CU_COSTO3 )
as
select  CU_CODIGO,CU_FOLIO, 
			 CU_NOMBRE,
			 CU_ACTIVO,
			 MA_CODIGO,
			 CU_HORAS,
			 CU_REVISIO,
			 CU_COSTO1,
			 CU_COSTO2,
			 CU_COSTO3
		 FROM CURSO
		 
		
GO

CREATE VIEW TFIJAS( TF_TABLA,
                    TF_CODIGO,
                    TF_DESCRIP )
as
SELECT              LV_CODIGO,
                    VL_CODIGO,
                    VL_DESCRIP 
FROM R_VALOR
GO

create view CER_PROG (
   CB_CODIGO, 
   CB_PUESTO, 
   CI_CODIGO, 
   KI_FEC_PRO, 
   KI_FEC_TOM,
   KI_CALIF_1, 
   KI_CALIF_2, 
   KI_CALIF_3, 
   PC_OPCIONA, 
   PC_LISTA 
   ) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CERTIFIC.CI_CODIGO,
  DATEADD( DAY, E.PC_DIAS, C.CB_FEC_PTO ) KC_FEC_PRO,
  KAR_CERT.KI_FEC_CER,
  KAR_CERT.KI_CALIF_1,
  KAR_CERT.KI_CALIF_2,
  KAR_CERT.KI_CALIF_3,
  E.PC_OPCIONA,
  E.PC_LISTA
from COLABORA C
join PTO_CERT E on ( E.PU_CODIGO = C.CB_PUESTO )
left join KAR_CERT on ( KAR_CERT.CB_CODIGO = C.CB_CODIGO ) and ( KAR_CERT.CI_CODIGO = E.CI_CODIGO )
left join CERTIFIC on ( CERTIFIC.CI_CODIGO = E.CI_CODIGO )
where ( CERTIFIC.CI_ACTIVO = 'S' )

go


CREATE VIEW V_ACUMULA (
  CB_CODIGO,
  CO_NUMERO,
  AC_YEAR,
  AC_MES,
  AC_MONTO
   ) as
select CB_CODIGO, CO_NUMERO, AC_YEAR, 1, AC_MES_01
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 2, AC_MES_02
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 3, AC_MES_03
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 4, AC_MES_04
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 5, AC_MES_05
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 6, AC_MES_06
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 7, AC_MES_07
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 8, AC_MES_08
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 9, AC_MES_09
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 10, AC_MES_10
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 11, AC_MES_11
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 12, AC_MES_12
from ACUMULA
union
select CB_CODIGO, CO_NUMERO, AC_YEAR, 13, AC_MES_13
from ACUMULA
GO

CREATE VIEW V_BITCAFE(
       BC_FOLIO,
       CB_CODIGO,
       IV_CODIGO,
       BC_EMPRESA, 
       BC_CREDENC, 
       BC_FECHA,
       BC_HORA,     
       BC_RELOJ,    
       BC_TIPO,  
       BC_COMIDAS,  
       BC_EXTRAS,  
       BC_REG_EXT,  
       BC_MANUAL,  
       CL_CODIGO,  
       BC_TGAFETE,
       BC_TIEMPO,
       BC_STATUS,
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA      
      )
as
 SELECT   BC_FOLIO, 
    CB_CODIGO,   
    IV_CODIGO, 
    BC_EMPRESA, 
    BC_CREDENC,  
    BC_FECHA, 
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
       BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA

 FROM #COMPARTE.dbo.BITCAFE
GO


CREATE VIEW V_BITCAFEE(
       BC_FOLIO, 
       CB_CODIGO,   
       IV_CODIGO, 
       BC_EMPRESA, 
       BC_CREDENC, 
       BC_FECHA,
       BC_HORA,     
       BC_RELOJ,    
       BC_TIPO,  
       BC_COMIDAS,  
       BC_EXTRAS,  
       BC_REG_EXT,  
       BC_MANUAL,  
       CL_CODIGO,  
       BC_TGAFETE,  
       BC_TIEMPO,
       BC_STATUS,   
       BC_MENSAJE,
      BC_RESPUES,
      BC_CHECADA       
      )
as
 SELECT   BC_FOLIO, 
    CB_CODIGO,   
    IV_CODIGO, 
    BC_EMPRESA,
    BC_CREDENC,  
    BC_FECHA, 
    BC_HORA,
    BC_RELOJ,
    BC_TIPO,
    BC_COMIDAS,
    BC_EXTRAS,
    BC_REG_EXT,
    BC_MANUAL,
    CL_CODIGO,
    BC_TGAFETE,
    BC_TIEMPO,
    BC_STATUS,
    BC_MENSAJE,
    BC_RESPUES,
    BC_CHECADA     
 
 FROM #COMPARTE.dbo.BITCAFE BITCAFE
 WHERE BITCAFE.BC_EMPRESA = #DIGITO
GO

CREATE VIEW V_LIQ_EMP
(
	 LS_YEAR,
	 LS_MONTH,
      LE_ACUMULA,
      LE_PROV,
      LE_VA_GOZO,
      LE_VA_PAGO,
	 LE_INF_PAT,
	 LE_INF_AMO,
	 CB_CODIGO,
	 LE_TOT_INF,
	LE_DIAS_BM,
     LE_BIM_SIG,
	LE_BIM_ANT
)
AS
SELECT
	LS_YEAR LS_YEAR,
	LS_MONTH LS_MONTH,
     MAX( LE_ACUMULA) LE_ACUMULA,
	MAX( LE_PROV ) LE_PROV,
	MAX( LE_VA_GOZO ) LE_VA_GOZO,
	MAX( LE_VA_PAGO ) LE_VA_PAGO,
	SUM(LE_INF_PAT) LE_INF_PAT,
	SUM(LE_INF_AMO) LE_INF_AMO,
	CB_CODIGO CB_CODIGO,
	SUM(LE_TOT_INF) LE_TOT_INF,
	SUM(LE_DIAS_BM) LE_DIAS_BM,
     SUM(LE_BIM_SIG) LE_BIM_SIG,
     SUM(LE_BIM_ANT) LE_BIM_ANT
FROM LIQ_EMP
GROUP BY CB_CODIGO, LS_YEAR, LS_MONTH
GO

create view V_DISPOSIT(
		DI_NOMBRE,
		DI_TIPO,
 		DI_DESCRIP,
    DI_NOTA,
    DI_IP)
as
	select DI_NOMBRE,
	  DI_TIPO,
	  DI_DESCRIP,
	  DI_NOTA,
	  DI_IP
    from #COMPARTE.dbo.DISPOSIT
GO

CREATE VIEW V_DISXCOM(
		 CM_CODIGO,
		 DI_NOMBRE,
		 DI_TIPO )
as
    SELECT CM_CODIGO,
		DI_NOMBRE ,
		DI_TIPO
    FROM #COMPARTE.dbo.DISXCOM
GO

CREATE VIEW V_CED_WORD(
	CE_FOLIO,
	WO_NUMBER,
	CW_POSICIO,
	CW_PIEZAS,
	V_DESCRIP,
	VAR_CODIGO,
	CE_FECHA,
	US_CODIGO,
	CE_TIPO,
	CE_COMENTA,
	AR_CODIGO,
	OP_NUMBER,
	CE_AREA,
	CE_HORA,
	CE_STATUS,
	CE_MOD_1, 
	CE_MOD_2, 
	CE_MOD_3, 
	CE_TMUERTO, 
	CE_PIEZAS,
    CE_MULTI, 
	CE_TIEMPO, 
	SETCAMBIOS
 )
as
	select CED_WORD.CE_FOLIO, CED_WORD.WO_NUMBER, CED_WORD.CW_POSICIO, CED_WORD.CW_PIEZAS, 
	WORDER.WO_DESCRIP, WORDER.AR_CODIGO, CEDULA.CE_FECHA, CEDULA.US_CODIGO, CEDULA.CE_TIPO, CEDULA.CE_COMENTA,
	WORDER.AR_CODIGO, CEDULA.OP_NUMBER, CEDULA.CE_AREA, CEDULA.CE_HORA, CEDULA.CE_STATUS, 
	CEDULA.CE_MOD_1, CEDULA.CE_MOD_2, CEDULA.CE_MOD_3, CEDULA.CE_TMUERTO, CEDULA.CE_PIEZAS,
        CEDULA.CE_MULTI, CEDULA.CE_TIEMPO, 'N' SETCAMBIOS
	from CED_WORD
	left outer join WORDER on WORDER.WO_NUMBER = CED_WORD.WO_NUMBER
	left outer join CEDULA on CEDULA.CE_FOLIO = CED_WORD.CE_FOLIO
	where  CED_WORD.CE_FOLIO in ( select CE_FOLIO from CEDULA where ( CE_MULTI = 'S' ) )
	union
	select CEDULA.CE_FOLIO, CEDULA.WO_NUMBER, 1, CEDULA.CE_PIEZAS, WORDER.WO_DESCRIP,
	CEDULA.AR_CODIGO, CEDULA.CE_FECHA, CEDULA.US_CODIGO, CEDULA.CE_TIPO, CEDULA.CE_COMENTA,
	CEDULA.AR_CODIGO, CEDULA.OP_NUMBER, CEDULA.CE_AREA, CEDULA.CE_HORA, CEDULA.CE_STATUS,
	CEDULA.CE_MOD_1, CEDULA.CE_MOD_2, CEDULA.CE_MOD_3, CEDULA.CE_TMUERTO, CEDULA.CE_PIEZAS,
        CEDULA.CE_MULTI, CEDULA.CE_TIEMPO, 'N' SETCAMBIOS
	from CEDULA
	left outer join WORDER on WORDER.WO_NUMBER = CEDULA.WO_NUMBER
	where ( CE_MULTI = 'N' )
GO

CREATE view V_ACCARBOL (
	AA_SOURCE,
	AX_NUMERO ,
	AA_DESCRIP,
	AA_VERSION,
	AA_POSICIO,
	AA_MODULO ,
	AA_SCREEN ,
	CM_CONTROL
) AS
SELECT AA_SOURCE,
	AX_NUMERO ,
	AA_DESCRIP COLLATE DATABASE_DEFAULT as AA_DESCRIP,
	AA_VERSION,
	AA_POSICIO,
	AA_MODULO ,
	AA_SCREEN,
	(CASE AA_SOURCE WHEN 0 then '3DATOS' WHEN 3 then '3RECLUTA' WHEN 4 then '3VISITA' ELSE '3OTRO' END)
	FROM #COMPARTE.dbo.ACC_ARBOL
where ((select COUNT(*) from #COMPARTE.dbo.AUTOMOD )=0)
/* Este view revisa contra los modulos autorizados. */
/* Si la tabla AUTOMOD esta vacia, se muestran todos los derechos de acceso */
/* AUTOMOD estara vacia cuando nunca se haya autorizado el Sentinel.*/
/* Si el sentinel se autoriza o se renueva la autorizacion, la tabla AUTOMOD se actualiza.*/
/* Esta tabla la actualiza solamente el TressCFG */
OR AA_MODULO in (select AM_CODIGO from #COMPARTE.dbo.AUTOMOD )
union
/* Derechos de acceso de los grupos adicionales creados por el cliente */
/* Los grupos adicionales se agregan en Globales\Campos Adicionales*/
SELECT  	0,
	10000+LLAVE,
	'Empleados-Datos-Adicionales-' + GX_TITULO COLLATE DATABASE_DEFAULT,
	0,
	7+(GX_POSICIO*.10),
	0,
	4,
	'3DATOS'
FROM GRUPO_AD
UNION
/* Derechos de acceso por clasificacion de Reporteador, esta parte cambia para cada cliente*/
SELECT 	1,
	RC_CODIGO,
	'Reporteador-Clasificaciones-'+RC_NOMBRE COLLATE DATABASE_DEFAULT,
	RC_VERSION,
	RC_ORDEN,
	25 ,
	5 ,
	'3DATOS'
FROM R_CLASIFI
/* Derechos de acceso por entidad de reporteador, esta parte cambia para cada cliente */
/* ESta parte se trae ordenado por modulo y despues por la posicion de la tabla dentro del modulo */
union
SELECT
	2,
	R_ENTIDAD.EN_CODIGO,
	'Reporteador-Entidades-' +
	COALESCE(R_MODULO.MO_NOMBRE,'<Sin m�dulo>') +'-' +
	COALESCE(R_ENTIDAD.EN_TITULO,'<Sin entidad>' ) +
	'('+COALESCE(R_ENTIDAD.EN_TABLA,' ')+')',
	R_ENTIDAD.EN_VERSION,
	R_MODULO.MO_ORDEN*10000 + R_MOD_ENT.ME_ORDEN,
	25 ,
	6 ,
	'3DATOS'
FROM R_MOD_ENT
left outer join R_MODULO on R_MODULO.MO_CODIGO= R_MOD_ENT.MO_CODIGO
left outer join R_ENTIDAD on R_ENTIDAD.EN_CODIGO= R_MOD_ENT.EN_CODIGO
/* Esta parte se trae las tablas*/
union
select 	2,
	R_ENTIDAD.EN_CODIGO,
	'Reporteador-Entidades-<Sin m�dulo>' +'-' +
	COALESCE(R_ENTIDAD.EN_TITULO,'<Sin entidad>' ) +
	'('+COALESCE(R_ENTIDAD.EN_TABLA,' ')+')',
	R_ENTIDAD.EN_VERSION,
	9999999 ,
	25 ,
	6 ,
	'3DATOS'
from R_ENTIDAD
where EN_CODIGO not IN (select EN_CODIGO from R_MOD_ENT where R_MOD_ENT.EN_CODIGO = R_ENTIDAD.EN_CODIGO)
GO

create view V_ACC_AD(
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
)AS

SELECT    0,
	10000+LLAVE,
	0,
	'Consultar',
	2,
	1 FROM GRUPO_AD
UNION
SELECT  	0,
	10000+LLAVE,
	3,
	'Modificar',
	0,
	2 FROM GRUPO_AD
UNION
SELECT  	0,
	10000+LLAVE,
	4,
	'Imprimir',
	1,
	3 FROM GRUPO_AD
GO

create VIEW V_ACC_RDD
(
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
)AS
select
	1,
	R_CLASIFI.RC_CODIGO,
	0,
	'Consultar',
	2,
	1
from R_CLASIFI
union
select
	1,
	R_CLASIFI.RC_CODIGO,
	1,
	'Agregar',
	0,
	2
from R_CLASIFI
union
select
	1,
	R_CLASIFI.RC_CODIGO,
	2,
	'Modificar',
	0,
	3
from R_CLASIFI
union
select
	1,
	R_CLASIFI.RC_CODIGO,
	3,
	'Borrar',
	1,
	4
from R_CLASIFI
union
select
	1,
	R_CLASIFI.RC_CODIGO,
	4,
	'Imprimir',
	2,
	5
from R_CLASIFI
union
select
	2,
	R_ENTIDAD.EN_CODIGO,
	0,
	'Reportear',
	2,
	1
from R_ENTIDAD
GO

CREATE view V_ACC_DER (
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
)AS
SELECT
	AD.AA_SOURCE,
	AD.AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT as AD_ACCION,
	AD_IMPACTO,
	AD_POSICIO
FROM #Comparte.dbo.ACC_DER AD
left outer join V_ACCARBOL A
	on A.AA_SOURCE = AD.AA_SOURCE
	AND A.AX_NUMERO = AD.AX_NUMERO
where AA_MODULO IS not null
union
select  AA_SOURCE,
	AX_NUMERO ,
  	CASE
    		WHEN charIndex('-Registro', AA_DESCRIP) <>0 THEN 1
    		WHEN charIndex('-Procesos',AA_DESCRIP) <>0 THEN 2
    		WHEN charIndex('-Anuales',AA_DESCRIP) <>0 THEN 3
		else 0
  	END,
  	CASE
    		WHEN charIndex('-Registro', AA_DESCRIP) <>0 THEN 'Registrar'
    		WHEN charIndex('-Procesos',AA_DESCRIP) <>0 THEN 'Ejecutar'
    		WHEN charIndex('-Anuales',AA_DESCRIP) <>0 THEN 'Ejecutar'
		else 'Consultar'
  	END COLLATE DATABASE_DEFAULT,

	4,
	0 from #Comparte.dbo.acc_arbol A
where A.ax_numero not in ( select ax_numero from #Comparte.dbo.acc_der)
UNION
SELECT
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT,
	AD_IMPACTO,
	AD_POSICIO
from V_ACC_AD
UNION
SELECT
	AA_SOURCE,
	AX_NUMERO,
	AD_TIPO,
	AD_ACCION COLLATE DATABASE_DEFAULT,
	AD_IMPACTO,
	AD_POSICIO
from V_ACC_RDD
GO

create VIEW V_CO_X_GR (
	CM_CODIGO,
	CM_CONTROL,
	GR_CODIGO
)
AS
select  CM_CODIGO COLLATE DATABASE_DEFAULT,
	CASE CM_CONTROL
		WHEN '3DATOS' THEN 0
		WHEN '3RECLUTA' THEN 3
		WHEN '3VISITA' THEN 4
		ELSE 5 END,
	GR_CODIGO
from #COMPARTE.dbo.GRUPO, #COMPARTE.dbo.COMPANY
GO

CREATE VIEW V_ACCALL(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO
		 )
AS
SELECT 	CASE (SELECT CM_CONTROL FROM #COMPARTE.dbo.COMPANY C WHERE
		C.CM_CODIGO=A.CM_CODIGO)
	WHEN '3DATOS' THEN 0
	WHEN '3RECLUTA' THEN 3
	WHEN '3VISITA' THEN 4
	ELSE 5 END,
	CM_CODIGO  COLLATE DATABASE_DEFAULT as CM_CODIGO,
	GR_CODIGO,
	AX_NUMERO
FROM #COMPARTE.dbo.ACCESO A
union
select    AA_SOURCE,
	V_CO_X_GR.CM_CODIGO COLLATE DATABASE_DEFAULT,
	V_CO_X_GR.GR_CODIGO,
	AX_NUMERO
from V_CO_X_GR, #COMPARTE.dbo.ACC_ARBOL
where V_CO_X_GR.CM_CONTROL = AA_SOURCE
GO

CREATE VIEW V_GRADALL(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO
		 )
AS
SELECT 	0,
	CM_CODIGO ,
	GR_CODIGO,
	GX_CODIGO
FROM GR_AD_ACC
union
select  0,
	V_CO_X_GR.CM_CODIGO,
	V_CO_X_GR.GR_CODIGO,
	GX_CODIGO
from V_CO_X_GR, GRUPO_AD
where V_CO_X_GR.CM_CONTROL = 0
GO

CREATE VIEW V_RCLAALL(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO
		 )
AS
SELECT 	1,
	CM_CODIGO ,
	GR_CODIGO,
	RC_CODIGO
FROM R_CLAS_ACC
union
select  1,
	V_CO_X_GR.CM_CODIGO,
	V_CO_X_GR.GR_CODIGO,
	RC_CODIGO
from V_CO_X_GR, R_CLASIFI
where V_CO_X_GR.CM_CONTROL = 0
GO

CREATE VIEW V_RENTALL(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO
		 )
AS
SELECT 	2,
	CM_CODIGO ,
	GR_CODIGO,
	EN_CODIGO
FROM R_ENT_ACC
union
select  2,
	V_CO_X_GR.CM_CODIGO,
	V_CO_X_GR.GR_CODIGO,
	EN_CODIGO
from V_CO_X_GR, R_ENTIDAD
where V_CO_X_GR.CM_CONTROL = 0
GO

CREATE VIEW V_ACCEMP(
		AA_SOURCE,
		CM_CODIGO,
		GR_CODIGO,
		AX_NUMERO,
		AX_DERECHO )
AS
/* Estos son los derechos de acceso que ya estan grabados, no importa si el derecho esta prendido o no*/
/* A la tabla ACCESO llega la informacion cuando el usuario entra a editar derechos de acceso , si el usuario nunca entra
a esa ventana (para determinado grupo) , ese grupo no tendra informacion en esa tabla*/
SELECT 	CASE (SELECT CM_CONTROL FROM #COMPARTE.dbo.COMPANY C
		WHERE C.CM_CODIGO=V.CM_CODIGO COLLATE DATABASE_DEFAULT)
	WHEN '3DATOS' THEN 0
	WHEN '3RECLUTA' THEN 3
	WHEN '3VISITA' THEN 4
	ELSE 5 END,
	CM_CODIGO ,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT A.AX_DERECHO FROM #COMPARTE.dbo.ACCESO A
		WHERE A.CM_CODIGO COLLATE DATABASE_DEFAULT = V.CM_CODIGO COLLATE DATABASE_DEFAULT
		AND A.GR_CODIGO = V.GR_CODIGO
		AND A.AX_NUMERO =  V.AX_NUMERO ) ,0)
	END
FROM V_ACCALL V
UNION
select 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	(select 10000+LLAVE from GRUPO_AD G WHERE G.GX_CODIGO=V_GRADALL.AX_NUMERO),
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT G.GX_DERECHO FROM GR_AD_ACC G
		WHERE G.CM_CODIGO = V_GRADALL.CM_CODIGO
		AND G.GR_CODIGO = V_GRADALL.GR_CODIGO
		AND G.GX_CODIGO =  V_GRADALL.AX_NUMERO ) ,0)
	END
FROM V_GRADALL
UNION
SELECT 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT RA_DERECHO FROM R_CLAS_ACC R
		WHERE R.CM_CODIGO = V_RCLAALL.CM_CODIGO
		AND R.GR_CODIGO = V_RCLAALL.GR_CODIGO
		AND R.RC_CODIGO =  V_RCLAALL.AX_NUMERO ) ,0) END
FROM V_RCLAALL
UNION
SELECT 	AA_SOURCE,
	CM_CODIGO COLLATE DATABASE_DEFAULT,
	GR_CODIGO,
	AX_NUMERO,
	CASE WHEN GR_CODIGO=1 THEN 4095 ELSE
	coalesce( (SELECT RE_DERECHO FROM R_ENT_ACC R
		WHERE R.CM_CODIGO = V_RENTALL.CM_CODIGO
		AND R.GR_CODIGO   = V_RENTALL.GR_CODIGO
		AND R.EN_CODIGO   = V_RENTALL.AX_NUMERO ) ,0) END
FROM V_RENTALL
GO

CREATE VIEW V_ACCUSU( US_CODIGO,
		  US_CORTO ,
 		  GR_CODIGO,
                      US_NIVEL ,
                      US_NOMBRE,
                      US_BLOQUEA,
                      US_CAMBIA,
                      US_FEC_IN,
                      US_FEC_OUT,
                      US_DENTRO,
                      US_ARBOL,
                      US_FORMA,
                      US_BIT_LST,
                      US_BIO_ID,
                      US_FEC_SUS,
                      US_FALLAS,
                      US_MAQUINA,
                      US_FEC_PWD,
                      US_EMAIL,
                      US_FORMATO,
                      US_LUGAR,
                      US_PAGINAS,
                      US_ANFITRI,
                      US_PORTAL,
                      US_ACTIVO,
                      US_DOMAIN,
                      US_JEFE
                               )
as
      SELECT      US_CODIGO,
                  US_CORTO ,
                  GR_CODIGO,
                  US_NIVEL ,
                  US_NOMBRE,
                  US_BLOQUEA,
                  US_CAMBIA ,
                  US_FEC_IN ,
                  US_FEC_OUT,
                  US_DENTRO ,
                  US_ARBOL  ,
                  US_FORMA  ,
                  US_BIT_LST,
                  US_BIO_ID ,
                  US_FEC_SUS,
                  US_FALLAS ,
                  US_MAQUINA,
                  US_FEC_PWD,
                  US_EMAIL  ,
                  US_FORMATO,
                  US_LUGAR  ,
                  US_PAGINAS,
                  US_ANFITRI,
                  US_PORTAL,
                  US_ACTIVO,
                  US_DOMAIN,
                  US_JEFE
      FROM #COMPARTE.dbo.USUARIO
GO

CREATE VIEW V_NOMMES
AS
select * from NOMINA
GO

CREATE VIEW V_ENROLA
AS
select * from COLABORA
GO

CREATE FUNCTION GetDerechoGozo( @Empleado NumeroEmpleado , @Anio Dias ,@FechaFinAniv Fecha,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	    Numeric(15,2),
	Pago	    Numeric(15,2),
    Prima		Numeric(15,2) )
AS
BEGIN
	 declare @FechaCierre Fecha;
	 declare @GozoCierre DiasFrac;
	 declare @PagoCierre DiasFrac;
	 declare @PrimaCierre DiasFrac;
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DGozoPend DiasFrac;
	 declare @DPagoPend DiasFrac;
	 declare @DPrimaPend DiasFrac;
	 declare @Antiguedad Fecha;
	 declare @DiasTabla Dias;
	 declare @DiasTablaPrima Dias;

 
	 set @DGozo = 0;
	 set @DPago = 0;
	 set @DPrima = 0;

	 set @DGozoPend = 0;
	 set @DPagoPend = 0;
	 set @DPrimaPend = 0;

     select @Antiguedad = CB_FEC_ANT from COLABORA where CB_CODIGO = @Empleado;
     
	 
	 if ( Exists(select VA_FEC_INI from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad  ) )
	 begin
          
		  SELECT @DGozo = Coalesce(Sum(VA_D_GOZO),0),@DPago = Coalesce(Sum(VA_D_PAGO),0),@DPrima = Coalesce(Sum(VA_D_PRIMA),0) from VACACION where CB_cODIGO = @Empleado and va_tipo = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad;
		  select top 1 @FechaCierre = VA_FEC_INI,@GozoCierre = VA_D_GOZO, @PagoCierre = VA_D_PAGO,@PrimaCierre = VA_D_PRIMA from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_YEAR = @Anio and VA_FEC_INI >= @Antiguedad order by VA_FEC_INI desc;  
		  if (@FechaCierre != @FechaFinAniv)
		  begin
		   	   if ((( DateDiff ( Day,@FechaCierre,@FechaFinAniv ))/365.00 ) < 1 )
			   begin
			    	set @FechaCierre = DateAdd(Day,1,@FechaCierre);
			   end
			   select Top 1 @DiasTablaPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTabla = PT_DIAS_VA  from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC     

			   set @DGozoPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTabla); 	
			   set @DGozo = @GozoCierre + @DGozoPend;	 
	
			   set @DPagoPend = @DGozoPend; 	
			   set @DPago = @PagoCierre + @DPagoPend;	 
	
			   set @DPrimaPend = ( ( DateDiff ( Day,@FechaCierre,@FechaFinAniv ) / 365.00 ) * @DiasTablaPrima);
			   set @DPrima = @PrimaCierre + @DPrimaPend;
		  end
	 end
     else	
     begin	 
		  select top 1 @DPrima =  ( PT_PRIMAVA / 100 )* PT_DIAS_VA,@DGozo = PT_DIAS_VA,@DPago = PT_DIAS_VA from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC;
	     
     end;		   
	 insert into @Tabla (Gozo,Pago,Prima)
	 values (@DGozo,@DPago,@DPrima);
		
	 RETURN 
END
GO

CREATE FUNCTION Get_Proporcional( @FechaCierre Fecha,@Antig Fecha,@Anio Dias,@Empleado NumeroEmpleado,@UltimaTabla Codigo)
RETURNS @Tabla	TABLE (
	Gozo	Numeric(15,2),
	Pago	Numeric(15,2),
	Prima	Numeric(15,2)
   )
as 
begin
	 declare @DGozo DiasFrac;
	 declare @DPago DiasFrac;
	 declare @DPrima DiasFrac;
	 declare @DCierre Dias;
	 declare @FechaFinAniv Fecha;
	 declare @DiasTb Dias;
	 declare @DiasTbPrima DiasFrac;
	 declare @DiasDif Dias;
	 
     set @FechaFinAniv = DateAdd(Year,@Anio,@Antig);
     
	 if ( ( ( Month(@FechaCierre) = Month(@Antig) ) and ( Day(@FechaCierre) = Day(@Antig) ) ) or ( @FechaCierre is null ) )
     begin
		  select Top 1 @DPago = PT_DIAS_VA ,@DGozo = PT_DIAS_VA, @DPrima = ( ( PT_PRIMAVA / 100 )* PT_DIAS_VA )
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio order by PT_YEAR ASC	
	 end
	 else
	 begin
		  select Top 1 @DiasTbPrima = (( PT_PRIMAVA / 100 )* PT_DIAS_VA),@DiasTb = PT_DIAS_VA  
					from PRESTACI where TB_CODIGO = @UltimaTabla and PT_YEAR >= @Anio  order by PT_YEAR ASC 
    
		  set @DiasDif = DateDiff(Day,DateAdd(Day,1,@FechaCierre),@FechaFinAniv);
		  set @DGozo = ( @DiasDif / 365.00 )*@DiasTb;
		  set @DPago = @DGozo;
		  
		  set @DPrima = ( @DiasDif / 365.00 )*@DiasTbPrima;

	 end 
	 insert into @Tabla
	 ( Gozo,Pago,Prima)values( @DGozo,@DPago,@DPrima);
	return	
end
GO

CREATE FUNCTION AniosVacCompletos( @FechaInicial Fecha,@FechaFinal Fecha )
RETURNS  integer
as
begin
declare @Anios integer;
	 if ( Month(@FechaInicial) = Month(@FechaFinal) and Day(@FechaInicial) = Day(@FechaFinal) )
	 begin
		  Set @Anios = DateDiff(Year,@FechaInicial,@FechaFinal)
	 end
	 else
	 begin
		  Set @Anios  =  (DateDiff( day, @FechaInicial,@FechaFinal ) + 1 )/365.25;
	 end
	 return @Anios;
end
GO

CREATE FUNCTION GET_SALDOS_VACACION( @Employee int)
RETURNS @Tabla	TABLE (
	VS_ANIV		int,
	CB_CODIGO	int,
	VS_D_GOZO	Numeric(15,2),
	VS_GOZO		Numeric(15,2),
	VS_S_GOZO	Numeric(15,2),
	VS_D_PAGO	Numeric(15,2),
	VS_PAGO		Numeric(15,2),
	VS_S_PAGO	Numeric(15,2),
	VS_D_PRIMA  Numeric(15,2),
	VS_PRIMA	Numeric(15,2),
	VS_S_PRIMA	Numeric(15,2),
	VS_FEC_VEN	DateTime,
	VS_CB_SAL	Numeric(15,2),CIERRE Numeric(15,2) )
AS
BEGIN
	declare @Empleado NumeroEmpleado;
	declare @Anios DiasFrac;
	declare @Anio Dias;

	declare @DerechoGozo DiasFrac;
	declare @GozadosDist DiasFrac;
	declare @GozadosRest DiasFrac;

    declare @GozoCierre DiasFrac;

	declare @DerechoPago DiasFrac;
	declare @PagadosDist DiasFrac;
	declare @PagadosRest DiasFrac;

	declare @PagoCierre DiasFrac;

	declare @DerechoPV DiasFrac;
	declare @PVDist DiasFrac;
	declare @PVRest DiasFrac;

	declare @PrimaCierre DiasFrac;
	declare @FechaCierre Fecha;

	declare @Antig Fecha;
	declare @AniosTotales Fecha;

	declare @Hoy Fecha;
	declare @YearCierre Dias;

	declare @DerechoGozoSum DiasFrac;
	declare @DerechoPagoSum DiasFrac;
	declare @DerechoPrimaSum DiasFrac;

    declare @FechaVencimiento Fecha;
    declare @Salario DiasFrac;
    declare @MesesGlobal Dias;

    declare @FechaFinAniv Fecha;
	declare @UltimaTabla Codigo;
	declare @FechaSalario Fecha;
    declare @FechaTabla Fecha;
    declare @Activo Booleano;
	declare @FechaBaja Fecha;


    if( @Employee > 0 )
    begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA where CB_CODIGO = @Employee
    end
    else
	begin
		 declare EmpleadosVac CURSOR for
		   select CB_CODIGO,CB_FEC_ANT,CB_TABLASS,CB_SALARIO,CB_FEC_SAL,CB_ACTIVO,CB_FEC_BAJ from COLABORA
    end;

	select @MesesGlobal = GL_FORMULA from GLOBAL where GL_CODIGO = 297;

	open EmpleadosVac;
     fetch NEXT from EmpleadosVac
     into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     while ( @@Fetch_Status = 0 )
     begin
		  
		  select @GozadosRest = Coalesce(SUM(VA_GOZO),0),@PagadosRest = Coalesce(SUM(VA_PAGO),0),@PVRest = Coalesce(SUM(VA_P_PRIMA),0)
							from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 1 and VA_FEC_INI >= @Antig;
		  
		  set @Anio = 1;
		  set @Anios = 0;
		  set @DerechoGozoSum = 0;
		  set @DerechoPagoSum = 0;
		  set @DerechoPrimaSum = 0;

		  Select @Hoy = gd FROM vCurrentDateTime;
		  if ( @Activo = 'N' )
		  begin
			   Set @Hoy = Coalesce(@FechaBaja,@Hoy);
		  end	
		  set @AniosTotales = dbo.AniosVacCompletos(@Antig,@Hoy );	
		  set @FechaCierre = @Antig;
		  
		  if ( ( @Antig is null )or (@Antig = '12/30/1899' ))
		  begin
		 	   select @Antig = CB_FEC_ING FROM COLABORA WHERE CB_CODIGO = @Empleado;  
	      end
			 
		  select Top 1 @FechaTabla = CB_FECHA FROM KARDEX where CB_CODIGO = @Empleado and CB_TIPO = 'PRESTA' order by CB_FECHA desc; 
		  
		  
 	      declare CierresEmp CURSOR for
		    select VA_FEC_INI,VA_D_GOZO,VA_D_PAGO,VA_D_PRIMA,VA_YEAR from VACACION where CB_CODIGO = @Empleado and VA_TIPO = 0 and VA_FEC_INI >= @Antig order by VA_FEC_INI asc
		  
          open CierresEmp;
		  fetch NEXT from CierresEmp
		  into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  while ( @@Fetch_Status = 0 )
		  begin 
			   	Set @Anios = dbo.AniosVacCompletos(@Antig,@FechaCierre );
			    			   
		       if ( @Anios < @Anio )
		       begin
					set @DerechoGozoSum = @GozoCierre + @DerechoGozoSum;
					set @DerechoPagoSum = @PagoCierre + @DerechoPagoSum;
					set @DerechoPrimaSum = @PrimaCierre + @DerechoPrimaSum;
		       end
		       
		       While ( @Anios >= @Anio )
			   begin
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado; 
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) ) 
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado) ;
					end

					if( @DerechoGozoSum > 0)
					begin
						 set @GozoCierre = @GozoCierre + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end

					if( @DerechoPagoSum > 0)
					begin
						 set @PagoCierre = @PagoCierre + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end

					if( @DerechoPrimaSum > 0)
					begin
						 set @PrimaCierre = @PrimaCierre + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					


					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima 
								from dbo.GetDerechoGozo(@Empleado,@Anio,@FechaFinAniv,@UltimaTabla);
										


					if ( @DerechoGozo <= @GozoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoGozo = @GozoCierre;
							  set @GozoCierre = 0;
						 end
						 else
						 begin
							  set @GozoCierre = @GozoCierre - @DerechoGozo;
						 end
					end
					else
					begin  
						 set @DerechoGozo = @GozoCierre;
						 set @GozoCierre = 0;
				    end

					if ( @DerechoPago <= @PagoCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPago = @PagoCierre;
							  set @PagoCierre = 0;
						 end
						 else
						 begin
							  set @PagoCierre = @PagoCierre - @DerechoPago;
						 end
					end
					else
					begin
						 set @DerechoPago = @PagoCierre;
						 set @PagoCierre = 0;
				    end

					if ( @DerechoPV <= @PrimaCierre )
					begin
						 if ( @Anios = @Anio )
						 begin
							  set @DerechoPV = @PrimaCierre;
							  set @PrimaCierre = 0;
						 end
						 else
						 begin
							  set @PrimaCierre = @PrimaCierre - @DerechoPV;
						 end
					end
					else
					begin  
						 set @DerechoPV = @PrimaCierre;
						 set @PrimaCierre = 0;
				    end


					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end

					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end

					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	

					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end


					insert into @Tabla
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario		 
					 );
			 	
					 set @Anio = @Anio + 1;
			   end
			   fetch NEXT from CierresEmp
               into @FechaCierre,@GozoCierre,@PagoCierre,@PrimaCierre,@YearCierre;
		  end		
		  close CierresEmp;
	      deallocate CierresEmp;
		  
		  if ( @Anios != @AniosTotales )
		  begin
			   While (@AniosTotales >= @Anio)
			   begin
					set @FechaFinAniv = DateAdd( Year,@Anio,@Antig );
					select @UltimaTabla = CB_TABLASS FROM COLABORA where CB_CODIGO = @Empleado; 
					if ( @FechaTabla != '12/30/1899' and ( not( @FechaTabla is null ) ) and ( @FechaFinAniv < @FechaTabla ) )
					begin
						select @UltimaTabla = dbo.SP_KARDEX_CB_TABLASS(@FechaFinAniv,@Empleado);
					end
					
					select @DerechoGozo = Gozo,@DerechoPago = Pago,@DerechoPV = Prima
							from dbo.Get_Proporcional(@FechaCierre,@Antig,@Anio,@Empleado,@UltimaTabla);


					set @FechaCierre = @FechaFinAniv;
					


					if( @DerechoGozoSum > 0)
					begin
						 set @DerechoGozo = @DerechoGozo + @DerechoGozoSum;
						 set @DerechoGozoSum = 0;
					end

					if( @DerechoPagoSum > 0)
					begin
						 set @DerechoPago = @DerechoPago + @DerechoPagoSum;
						 set @DerechoPagoSum = 0;
					end

					if( @DerechoPrimaSum > 0)
					begin
						 set @DerechoPV = @DerechoPV + @DerechoPrimaSum;
						 set @DerechoPrimaSum = 0;
					end
					



					if ( @GozadosRest > @DerechoGozo )
					begin
						set @GozadosDist = @DerechoGozo;
					    set @GozadosRest = @GozadosRest - @GozadosDist;
					end
					else
					begin
						 set @GozadosDist = @GozadosRest;
						 set @GozadosRest = 0;
					end	

					if ( @PagadosRest > @DerechoPago )
					begin
						set @PagadosDist = @DerechoPago;
					    set @PagadosRest = @PagadosRest - @PagadosDist;
					end
					else
					begin
						 set @PagadosDist = @PagadosRest;
						 set @PagadosRest = 0;
					end	

					if ( @PVRest > @DerechoPV )
					begin
						set @PVDist = @DerechoPV;
					    set @PVRest = @PVRest - @PVDist;
					end
					else
					begin
						 set @PVDist = @PVRest;
						 set @PVRest = 0;
					end	
					
					if ( ( ( @DerechoGozo - @GozadosDist ) > 0 ) and ( @MesesGlobal > 0 ) )
					begin
						 set @FechaVencimiento = DateAdd( Month,@MesesGlobal,@FechaFinAniv );
						 select @Salario = dbo.SP_KARDEX_CB_SALARIO ( @FechaFinAniv,@Empleado );
					end
					else
					begin
						 set @FechaVencimiento = null;
						 set @Salario = null;
					end
					
					

					insert into @Tabla 
					( VS_ANIV,
					  CB_CODIGO,
					  VS_D_GOZO,
					  VS_GOZO,
					  VS_S_GOZO,
					  VS_D_PAGO,
					  VS_PAGO,
					  VS_S_PAGO,
					  VS_D_PRIMA,
					  VS_PRIMA,
					  VS_S_PRIMA,
					  VS_FEC_VEN,
					  VS_CB_SAL)values
					(
					  @Anio,
					  @Empleado,
					  @DerechoGozo,
					  @GozadosDist,
					  @DerechoGozo - @GozadosDist,
					  @DerechoPago,
					  @PagadosDist,
					  @DerechoPago - @PagadosDist,
					  @DerechoPV,
					  @PVDist ,
					  @DerechoPV - @PVDist,
					  @FechaVencimiento,
					  @Salario 
					 );

					 set @Anio = @Anio + 1;
			   end				
		  end
		 	
		  fetch NEXT from EmpleadosVac
          into @Empleado,@Antig,@UltimaTabla,@Salario,@FechaSalario,@Activo,@FechaBaja;
     end;
	 close EmpleadosVac;
	 deallocate EmpleadosVac;
	 	
	return 
END
GO

Create View V_SALD_VAC
(
	CB_CODIGO,
	VS_ANIV,
	VS_D_GOZO,
	VS_GOZO,
	VS_S_GOZO,
	VS_D_PAGO,
	VS_PAGO,
	VS_S_PAGO,
	VS_D_PRIMA,
	VS_PRIMA,
	VS_S_PRIMA,
	VS_FEC_VEN,
	VS_CB_SAL
)AS
	Select CB_CODIGO,VS_ANIV,VS_D_GOZO,VS_GOZO,VS_S_GOZO,VS_D_PAGO,VS_PAGO,VS_S_PAGO,VS_D_PRIMA,VS_PRIMA,VS_S_PRIMA,VS_FEC_VEN,VS_CB_SAL from GET_SALDOS_VACACION(-1)
go

drop function GetDerechoGozo
GO

drop function Get_Proporcional
GO

drop function AniosVacCompletos
GO

DROP function GET_SALDOS_VACACION
GO

CREATE VIEW dbo.vCurrentDateTime
AS
  SELECT gd = GETDATE()
GO

create view V_EMP_BIO as select ID_NUMERO, CM_CODIGO, CB_CODIGO, GP_CODIGO, IV_CODIGO from #COMPARTE.dbo.EMP_BIO
GO

create view V_TERMINAL
as
	select TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT,TE_ZONA, TE_ACTIVO, TE_XML,TE_LOGO,TE_FONDO,TE_ZONA_ES, TE_VERSION, TE_IP,TE_HUELLAS, TE_ID from #COMPARTE.dbo.TERMINAL
GO

Create view V_TERM_MSG 
as 
  select DM_CODIGO, TE_CODIGO, TM_NEXT, TM_ULTIMA, TM_NEXTXHR,TM_NEXTHOR from #COMPARTE.dbo.TERM_MSG
GO

create view V_GRUPOTER
as
	select GP_CODIGO, GP_DESCRIP, GP_NUMERO, GP_TEXTO, GP_ACTIVO from #COMPARTE.dbo.GRUPOTERM
GO

create view V_TERM_GPO
as
	select TE_CODIGO, GP_CODIGO from #COMPARTE.dbo.TERM_GPO
GO

create view V_WS_BITAC
as
	select DATEADD(dd, 0, DATEDIFF(dd, 0, WS_FECHA)) as WS_FECHA,CH_RELOJ,WS_CHECADA,CB_CODIGO,WS_MENSAJE,CM_CODIGO,WS_TIPO from #COMPARTE.dbo.WS_BITAC
GO

create view VWS_ACCESS
as
	select WA_FECHA, CM_CODIGO, CB_CODIGO, CH_RELOJ, WS_CHECADA from #COMPARTE.dbo.WS_ACCESS
GO

create view V_MENSAJE (DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO )
as
	select DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO from #COMPARTE.dbo.MENSAJE
GO

CREATE VIEW VHUELLAGTI
AS
--> Siempre traer todas las huellas, no filtrar por IV_CODIGO.
SELECT COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	COUNT(HU_ID) AS HU_CNT
FROM #COMPARTE.DBO.HUELLAGTI AS H, #COMPARTE.DBO.EMP_BIO AS BIO, COLABORA AS C

WHERE H.CB_CODIGO = BIO.CB_CODIGO
		AND H.CM_CODIGO = BIO.CM_CODIGO
		AND H.IV_CODIGO = BIO.IV_CODIGO
		AND H.HU_INDICE < 11

GROUP BY HU_ID, H.CM_CODIGO, H.CB_CODIGO, HU_INDICE, H.IV_CODIGO
GO

CREATE VIEW VFACIALGTI
AS
SELECT COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	COUNT(HU_ID) AS HU_CNT
FROM #COMPARTE.DBO.HUELLAGTI AS H, #COMPARTE.DBO.EMP_BIO AS BIO, COLABORA AS C

WHERE H.CB_CODIGO = BIO.CB_CODIGO
		AND H.CM_CODIGO = BIO.CM_CODIGO
		AND H.IV_CODIGO = BIO.IV_CODIGO
		AND H.HU_INDICE = 11

GROUP BY HU_ID, H.CM_CODIGO, H.CB_CODIGO, HU_INDICE, H.IV_CODIGO
GO

create view V_KIOS_IMP
(
	CB_CODIGO ,
	CM_CODIGO ,
	PE_TIPO ,
	PE_YEAR ,
	PE_NUMERO ,
	KI_FECHA ,
	KI_KIOSCO ,
	KI_HORA ,
	RE_CODIGO
)as
select
    CB_CODIGO ,
	CM_CODIGO ,
	PE_TIPO ,
	PE_YEAR ,
	PE_NUMERO ,
	KI_FECHA ,
	KI_KIOSCO ,
	KI_HORA ,
	RE_CODIGO
from
	#COMPARTE.dbo.KIOS_IMPR
GO

CREATE VIEW V_ENCUESTA(
	   ENC_CODIGO
      ,ENC_NOMBRE
      ,ENC_PREGUN
      ,ENC_CONFID
      ,ENC_VOTOSE
      ,ENC_STATUS
      ,US_CODIGO
      ,ENC_FECINI
      ,ENC_FECFIN
      ,ENC_HORINI
      ,ENC_HORFIN
      ,ENC_MRESUL
      ,ENC_FILTRO
      ,ENC_COMPAN
      ,ENC_KIOSCS
      ,ENC_OPCION
      ,ENC_MSJCON
	  ,ENC_OPCGAN
	  ,ENC_VOTGAN
)
as
  SELECT ENC_CODIGO
      ,ENC_NOMBRE
      ,ENC_PREGUN
      ,ENC_CONFID
      ,ENC_VOTOSE
      ,ENC_STATUS
      ,US_CODIGO
      ,ENC_FECINI
      ,ENC_FECFIN
      ,ENC_HORINI
      ,ENC_HORFIN
      ,ENC_MRESUL
      ,ENC_FILTRO
      ,ENC_COMPAN
      ,ENC_KIOSCS
      ,ENC_OPCION
      ,ENC_MSJCON
	  ,#Comparte.[dbo].[SP_MAX_VOTOS_ENCUESTA](ENC_CODIGO,1)AS ENC_OPCGAN
	  ,#Comparte.[dbo].[SP_MAX_VOTOS_ENCUESTA](ENC_CODIGO,0)AS ENC_VOTGAN
      FROM #Comparte.dbo.KENCUESTAS
GO

CREATE VIEW V_VOTOS(
	   ENC_CODIGO
      ,CB_CODIGO
      ,CM_CODIGO
      ,CB_CREDENC
      ,VO_VALOR
      ,VO_FECHA
      ,VO_HORA
      ,VO_KIOSCO)
as
  SELECT
       ENC_CODIGO
      ,CB_CODIGO
      ,CM_CODIGO
      ,CB_CREDENC
      ,VO_VALOR
      ,VO_FECHA
      ,VO_HORA
      ,VO_KIOSCO
   FROM #Comparte.dbo.KVOTOS
GO

CREATE VIEW V_OPC_ENCS(
	  OP_ORDEN
      ,OP_TITULO
      ,OP_DESCRIP
      ,ENC_CODIGO
      ,US_CODIGO)
as
  SELECT OP_ORDEN
      ,OP_TITULO
      ,OP_DESCRIP
      ,ENC_CODIGO
      ,US_CODIGO
  FROM #Comparte.dbo.KOPCIONES
GO

Create View V_RES_ENCU(
	ENC_CODIGO,
	OP_ORDEN,
	OP_TITULO,
	PORCENTAJE,
	TOTAL)
AS
	select V.ENC_CODIGO,VO_VALOR,O.OP_TITULO ,CONVERT( VARCHAR, ( CONVERT( decimal(4,1), Round((CAST( Count(vo_valor)* 100 AS dECIMAL(6,2)) / CAST( (Select Count(*) From #Comparte.dbo.KVOTOS where Enc_Codigo = V.ENC_CODIGO ) as Decimal (6,2) ) ),1) ) ) )+'%' as Opcion, Count(VO_VALOR)as Total from
	#Comparte.dbo.KVOTOS V left outer join #Comparte.dbo.KOPCIONES O on O.OP_ORDEN = V.VO_VALOR and O.ENC_CODIGO = V.ENC_CODIGO
	Group by O.OP_TITULO,V.ENC_CODIGO,VO_VALOR
GO

create VIEW V_EMP_POL(
  CB_CODIGO  ,
	PM_CODIGO  ,
	PV_REFEREN ,
	EP_TIPO    ,
	PA_FOLIO   ,
	PA_RELACIO ,
	EP_CERTIFI ,
	EP_FEC_INI ,
	EP_FEC_FIN ,
	EP_CFIJO   ,
	EP_CPOLIZA ,
	EP_CTITULA ,
	EP_OBSERVA ,
	EP_CANCELA ,
	EP_STATUS ,
	EP_CAN_FEC ,
	EP_CAN_MON ,
	EP_CAN_MOT ,
	EP_GLOBAL  ,
	US_CODIGO  ,
	EP_FEC_REG ,
	EP_DEPEND ,
	EP_ASEGURA,
	EP_ORDEN,
	EP_CEMPRES,
	PM_NUMERO,
	EP_END_ALT,
	EP_END_CAN,
	LLAVE
)
AS
SELECT E.CB_CODIGO  ,
	E.PM_CODIGO  ,
	E.PV_REFEREN ,
	E.EP_TIPO    ,
	E.PA_FOLIO   ,
	E.PA_RELACIO ,
	E.EP_CERTIFI ,
	E.EP_FEC_INI ,
	E.EP_FEC_FIN ,
	E.EP_CFIJO   ,
	E.EP_CPOLIZA ,
	E.EP_CTITULA ,
	E.EP_OBSERVA ,
	E.EP_CANCELA ,
	E.EP_STATUS,
	E.EP_CAN_FEC ,
	E.EP_CAN_MON ,
	E.EP_CAN_MOT ,
	E.EP_GLOBAL  ,
	E.US_CODIGO  ,
	E.EP_FEC_REG ,
  ( P.PA_APE_PAT+' '+P.PA_APE_MAT+' , '+PA_NOMBRES ) As EP_DEPEND,
	E.EP_ASEGURA,
	E.EP_ORDEN,
	E.EP_CEMPRES,
	PM.PM_NUMERO,
	E.EP_END_ALT,
	E.EP_END_CAN,
  E.LLAVE
from EMP_POLIZA E
LEFT OUTER JOIN PARIENTE P on (P.PA_RELACIO = E.PA_RELACIO and P.PA_FOLIO = E.PA_FOLIO and P.CB_CODIGO = E.CB_CODIGO)
LEFT OUTER JOIN POLIZA_MED PM on PM.PM_CODIGO = E.PM_CODIGO
GO

CREATE VIEW V_POL_VIG(
  PM_CODIGO  ,
	PV_REFEREN ,
	PV_FEC_INI ,
	PV_FEC_FIN ,
	PV_CONDIC  ,
	PV_TEXTO   ,
	PV_NUMERO  ,
	PV_CFIJO ,
	PV_STITULA  ,
	PV_SDEPEND
)
AS
SELECT
	P.PM_CODIGO  ,
	P.PV_REFEREN ,
	P.PV_FEC_INI ,
	P.PV_FEC_FIN ,
	P.PV_CONDIC  ,
	P.PV_TEXTO   ,
	P.PV_NUMERO  ,
	P.PV_CFIJO   ,
	(select Count(CB_CODIGO) from EMP_POLIZA E where E.PM_CODIGO = P.PM_CODIGO and E.PV_REFEREN = P.PV_REFEREN and E.EP_TIPO = 0) PV_STITULA,
	(select Count(CB_CODIGO) from EMP_POLIZA E where E.PM_CODIGO = P.PM_CODIGO and E.PV_REFEREN = P.PV_REFEREN and E.EP_TIPO = 1) PV_SDEPEND
from POL_VIGENC P;
GO

CREATE VIEW CRIT_COSTO
AS
	SELECT	CritCostoID		CR_ID,
			CritCostoNombre	CR_NOMBRE,
			CritCostoActivo	CR_ACTIVO
	FROM	T_CriteriosCosto
GO

CREATE VIEW GPO_COSTO
AS
	SELECT	GpoCostoID		GC_ID,
			GpoCostoCodigo	GC_CODIGO,
			GpoCostoNombre	GC_NOMBRE,
			GpoCostoActivo	GC_ACTIVO
	FROM	T_GruposCosto
GO

CREATE VIEW CONC_COSTO
AS
	SELECT	CC.CO_NUMERO	CO_NUMERO,
			CONCEPTO.CO_DESCRIP,
			CC.GpoCostoID	GC_ID,
			GpoCostoCodigo	GC_CODIGO,
			GpoCostoNombre	GC_NOMBRE,
			CC.CritCostoID	CR_ID,
			CritCostoNombre	CR_NOMBRE
	FROM	T_ConceptosCosto CC
				JOIN CONCEPTO ON CC.CO_NUMERO = CONCEPTO.CO_NUMERO
				JOIN T_GruposCosto ON CC.GpoCostoID = T_GruposCosto.GpoCostoID
				JOIN T_CriteriosCosto ON CC.CritCostoID = T_CriteriosCosto.CritCostoID
GO

CREATE VIEW CCOSTO
AS
	SELECT	TB_CODIGO	CC_CODIGO,
			TB_ELEMENT	CC_NOMBRE,
			TB_INGLES	CC_INGLES,
			TB_NUMERO	CC_NUMERO,
			TB_TEXTO	CC_TEXTO,
			TB_SUB_CTA	CC_SUB_CTA,
			TB_ACTIVO	CC_ACTIVO,
			LLAVE
	FROM	NIVEL9
GO
CREATE VIEW PROP_COSTO
AS
	SELECT	PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			PC.CritCostoID CR_ID,
			CritCostoNombre	CR_NOMBRE,
			PC.CC_CODIGO,
			CC_NOMBRE,
			PropCostoClasifi PC_CLASIFI,
			PropCostoBase PC_BASE
	FROM	T_ProporcionCostos PC
				JOIN NOMINA ON PC.NominaID = NOMINA.LLAVE
				JOIN T_CriteriosCosto ON PC.CritCostoID = T_CriteriosCosto.CritCostoID
				LEFT JOIN CCOSTO ON PC.CC_CODIGO = CCOSTO.CC_CODIGO
GO


CREATE VIEW COSTOS
AS
	SELECT	PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			CT.CO_NUMERO,
			COALESCE( CO_DESCRIP, '' ) CO_DESCRIP,
			CT.MO_REFEREN,
			CT.GpoCostoID GC_ID,
			GpoCostoCodigo GC_CODIGO,
			GpoCostoNombre GC_NOMBRE,
			CT.CC_CODIGO,
			COALESCE( CC_NOMBRE, '' ) CC_NOMBRE,
			CostoClasifi CT_CLASIFI,
			CostoMonto CT_MONTO
	FROM	T_Costos CT
				JOIN NOMINA ON CT.NominaID = NOMINA.LLAVE
				JOIN T_GruposCosto ON CT.GpoCostoID = T_GruposCosto.GpoCostoID
				LEFT JOIN CONCEPTO ON CT.CO_NUMERO = CONCEPTO.CO_NUMERO
				LEFT JOIN CCOSTO ON CT.CC_CODIGO = CCOSTO.CC_CODIGO
GO

CREATE FUNCTION SP_AUSENCIA_COSTEO( @Empleado NumeroEmpleado, @Fecha Fecha)
RETURNS Codigo
AS
BEGIN
	declare @NivelCosteo FolioChico
	declare @Resultado Codigo

    select   @NivelCosteo = dbo.SP_GET_NIVEL_COSTEO();

	if @NivelCosteo = 0
		select @Resultado = ''
	else
		select @Resultado =
					CASE  @NivelCosteo
						WHEN 1 THEN CB_NIVEL1
						WHEN 2 THEN CB_NIVEL2
						WHEN 3 THEN CB_NIVEL3
						WHEN 4 THEN CB_NIVEL4
						WHEN 5 THEN CB_NIVEL5
						WHEN 6 THEN CB_NIVEL6
						WHEN 7 THEN CB_NIVEL7
						WHEN 8 THEN CB_NIVEL8
						WHEN 9 THEN CB_NIVEL9
					/* ACS
						WHEN 10 THEN CB_NIVEL10
						WHEN 11  THEN CB_NIVEL11
						WHEN 12 THEN CB_NIVEL12
					*/
					end
		from AUSENCIA
		where CB_CODIGO = @Empleado
		and AU_FECHA = @Fecha

	RETURN  @Resultado
END
GO

CREATE  VIEW V_TRANSFER(
CB_CODIGO,
AU_FECHA,
CC_ORIGEN,
CC_DESTINO,
TR_HORAS,
TR_TIPO,
TR_MOTIVO,
TR_NUMERO,
TR_TEXTO,
US_CODIGO,
TR_FECHA,
TR_GLOBAL,
TR_APRUEBA,
TR_FEC_APR,
TR_STATUS,
TR_TXT_APR,
LLAVE
)
as
select
T.CB_CODIGO,
T.AU_FECHA Fecha,
dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA),
T.CC_CODIGO,
T.TR_HORAS,
T.TR_TIPO,
T.TR_MOTIVO,
T.TR_NUMERO,
T.TR_TEXTO,
T.US_CODIGO,
T.TR_FECHA,
T.TR_GLOBAL,
T.TR_APRUEBA,
T.TR_FEC_APR,
T.TR_STATUS,
T.TR_TXT_APR,
T.LLAVE
from TRANSFER T
left outer join COLABORA on COLABORA.CB_CODIGO = T.CB_CODIGO
left outer join AUSENCIA on AUSENCIA.CB_CODIGO = T.CB_CODIGO
and AUSENCIA.AU_FECHA = T.AU_FECHA
GO

CREATE VIEW CHORAS (
	AU_FECHA,
	CB_CODIGO,
	CC_CODIGO,
	CH_TIPO,
	CH_CLASIFI,
    CH_HORAS )
as
	select 	AU_FECHA, CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(CB_CODIGO, AU_FECHA), 0, '', AU_HORAS
	from	AUSENCIA
	where	( AU_HORAS > 0 )
    union

	select	T.AU_FECHA, T.CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA), 0, 'Transferidas',
			case when A.AU_HORAS >= TR_HORAS then coalesce(-TR_HORAS,0) else -AU_HORAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 0 )
	union

	select	T.AU_FECHA, T.CB_CODIGO, T.CC_CODIGO, 0, 'Recibidas',
			case when A.AU_HORAS >= TR_HORAS then coalesce(TR_HORAS,0) else AU_HORAS end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 0 )
	union

	select 	AU_FECHA, CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(CB_CODIGO, AU_FECHA), 1, '', AU_EXTRAS + AU_DES_TRA
	from	AUSENCIA
	where	( AU_EXTRAS > 0 ) or ( AU_DES_TRA > 0 )
    union

	select	T.AU_FECHA, T.CB_CODIGO, dbo.SP_AUSENCIA_COSTEO(T.CB_CODIGO, T.AU_FECHA), 1, 'Transferidas',
			case when A.AU_EXTRAS + A.AU_DES_TRA >= TR_HORAS then coalesce(-TR_HORAS,0) else -AU_EXTRAS + -AU_DES_TRA end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 1 )
	union

	select	T.AU_FECHA, T.CB_CODIGO, T.CC_CODIGO, 1, 'Recibidas',
			case when A.AU_EXTRAS + A.AU_DES_TRA >= TR_HORAS then coalesce(TR_HORAS,0) else AU_EXTRAS + AU_DES_TRA end
	from	TRANSFER T
	join	AUSENCIA A on (A.AU_FECHA = T.AU_FECHA) and (A.CB_CODIGO = T.CB_CODIGO)
	where	( T.TR_TIPO = 1 )
GO

drop function SP_AUSENCIA_COSTEO

GO

CREATE VIEW COSTOS_POL AS
	SELECT
			PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			CT.CO_NUMERO,
			COALESCE( CO_DESCRIP, '' ) CO_DESCRIP,
			CT.MO_REFEREN,
			CT.GpoCostoID GC_ID,
			GpoCostoCodigo GC_CODIGO,
			GpoCostoNombre GC_NOMBRE,
			CT.CC_CODIGO,
			COALESCE( CC_NOMBRE, '' ) CC_NOMBRE,
			CostoClasifi CT_CLASIFI,
			CostoMonto * (case T_Costos_SubCta.Prorrat when 0 then 1 else Coalesce( T_Costos_SubCta.Prorrat, 1) end ) CT_MONTO,
			CostoMonto CT_MONTO_OR,
			T_Costos_SubCta.CuentaContable CT_SUB_CTA,
			T_Costos_SubCta.Tipo CT_CAR_ABO,
			case T_Costos_SubCta.Prorrat when 0 then 1 else Coalesce( T_Costos_SubCta.Prorrat, 1) end CT_PRORRAT,
			T_Costos_SubCta.Comentario CT_COMENTA,
			T_Costos_SubCta.Numero CT_NUMERO
	FROM T_Costos_SubCta
				left outer join T_Costos CT on CT.CostoID = T_Costos_SubCta.CostoID
				JOIN NOMINA ON CT.NominaID = NOMINA.LLAVE
				JOIN T_GruposCosto ON CT.GpoCostoID = T_GruposCosto.GpoCostoID
				LEFT OUTER JOIN CONCEPTO ON CT.CO_NUMERO = CONCEPTO.CO_NUMERO
				LEFT OUTER JOIN CCOSTO ON CT.CC_CODIGO = CCOSTO.CC_CODIGO
GO

CREATE view TOT_COSTOS_POLIZA
as
SELECT PE_YEAR, PE_TIPO, PE_NUMERO, COSTOS_POL.CT_SUB_CTA,
	cast ( ( sum(CT_MONTO)) as NUMERIC(15,2)) CP_MONTO, COSTOS_POL.CT_CAR_ABO
FROM COSTOS_POL
group by PE_YEAR, PE_TIPO, PE_NUMERO, COSTOS_POL.CT_SUB_CTA, CT_CAR_ABO
GO

create view COST_CRITE
as
     SELECT CritCostoID AS CC_CODIGO, CritCostoNombre AS CC_DESCRIP, CritCostoActivo AS CC_ACTIVO
     FROM T_CriteriosCosto
GO

CREATE VIEW COST_GRUPO
AS
     SELECT GpoCostoID AS GC_LLAVE, GpoCostoCodigo AS GC_CODIGO, GpoCostoNombre AS GC_DESCRIP,GpoCostoActivo AS GC_ACTIVO
     FROM T_GruposCosto
GO

CREATE VIEW COST_CON
AS
     SELECT ConcepCostoID AS CR_LLAVE, CO_NUMERO, GpoCostoID AS GC_LLAVE, CritCostoID AS CC_CODIGO
     FROM T_ConceptosCosto
GO

create view VCONF_TAB 
as 
select CT_TABLA, CT_TABNAME=E.EN_TABLA, CT_TABDES=E.EN_TITULO, CT_CODIGO, CT_NIVEL0 from CONF_TAB CT
join R_ENTIDAD E on E.EN_CODIGO = CT.CT_TABLA
join V_NIVEL0 N on N.TB_CODIGO COLLATE DATABASE_DEFAULT  = CT.CT_NIVEL0 COLLATE DATABASE_DEFAULT
GO

CREATE FUNCTION SP_GET_DB_FULLNAME()
returns Varchar(512)
as
begin
	declare @nameServer varchar(200)
	declare @nameMachine varchar(100)
	declare @kInstance varchar(100)
	declare @constrServer varchar(200)
	declare @retValue varchar(512)

	select @nameServer = convert(nvarchar(128), serverproperty('servername'));
	select @nameMachine = convert(nvarchar(128), serverproperty('machinename'));
	if len(@nameServer) = len(@nameMachine)
	select @kInstance = ''
	else
	select @kInstance = right(@nameServer, len(@nameServer) - (len(@nameMachine)+1));

	if @kInstance='' select @constrServer = @nameMachine
	else select @constrServer = @nameMachine + '\' + @kInstance;

	set @retValue = @constrServer + '.' + db_name();

	set @retValue = UPPER( RTRIM( @retValue ) ) ;

	return @retValue
end
GO

create function SP_GET_EMPRESAS() 
returns TABLE 
as
  return
  select CM_CODIGO  from V_COMPANY where UPPER(CM_DATOS) COLLATE DATABASE_DEFAULT = DBO.SP_GET_DB_FULLNAME() COLLATE DATABASE_DEFAULT
GO

CREATE VIEW V_PROCESO
AS
SELECT WP_FOLIO,WE_DESCRIP,WE_ORDEN,WM_CODIGO,WM_DESCRIP,WP_FEC_FIN,WP_FEC_INI,WP_NOM_INI,WP_NOMBRE,WP_NOTAS,WP_PASO,WP_PASOS,WP_STATUS,WP_USR_INI,WS_FEC_FIN,WS_FEC_INI,WS_NOM_INI,WS_NOMBRE,WS_STATUS,WS_USR_INI,WT_AVANCE,WT_DESCRIP,WT_FEC_FIN,WT_FEC_INI,WT_NOM_DES,WT_NOM_ORI,WT_NOTAS,WT_STATUS,WT_USR_DES,WT_USR_ORI,WP_MOV3
FROM #COMPARTE..V_PROCESO
GO

CREATE VIEW V_WPCAMBIO
AS
select W.* from #COMPARTE..V_WPCAMBIO W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPAUTO3
AS
select W.* from #COMPARTE..V_WPAUTO3 W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPMULTIP
AS
select W.* from #COMPARTE..V_WPMULTIP W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPPERM
AS
select W.* from #COMPARTE..V_WPPERM W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

CREATE VIEW V_WPVACA
AS
select W.* from #COMPARTE..V_WPVACA W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

drop function SP_GET_EMPRESAS;
GO

drop function SP_GET_DB_FULLNAME;
GO

create View V_PLAN_CAP
AS
select
		   C.CB_CODIGO,
		   C.CB_PUESTO,
		   CPC.CC_CODIGO,
		   CPC.NC_NIVEL,
		   CCE.EC_FECHA,
		   ISNULL(CCE.NC_NIVEL,0) AS EC_NIVEL,
		   CCE.EC_CALIFIC,
		   CCE.US_CODIGO,
		   'Puesto' as PC_GLOBAL,
		   CPO.CP_CODIGO,
		   (CASE WHEN ( CCE.NC_NIVEL is NULL)THEN 0 Else CPC.PC_PESO end ) as PC_PESO,
		   ' ' as Observaciones
	from COLABORA C
	JOIN PUESTO P ON (P.PU_CODIGO = C.CB_PUESTO)
	left join C_PERF_PTO CPO on ( CPO.PU_CODIGO = P.PU_CODIGO)
	left join C_PER_COMP CPC on ( CPC.CP_CODIGO = CPO.CP_CODIGO )
	left join C_EVA_COMP CCE on ( CCE.CB_CODIGO = C.CB_CODIGO and CCE.CC_CODIGO = CPC.CC_CODIGO and CCE.NC_NIVEL >= CPC.NC_NIVEL)
	left join C_PERFIL CP on (CP.CP_CODIGO = CPC.CP_CODIGO  )
	left join C_COMPETEN CC on (CC.CC_CODIGO = CPC.CC_CODIGO )
	where NOT CPC.CC_CODIGO is NULL and CC.CC_ACTIVO = 'S' and CP.CP_ACTIVO = 'S' and CPO.PP_ACTIVO = 'S'
union all
	select
		   C.CB_CODIGO,
		   C.CB_PUESTO,
		   CEC.CC_CODIGO,
		   CEC.NC_NIVEL,
		   CCE.EC_FECHA,
		   isnull(CCE.NC_NIVEL,0)AS EC_NIVEL,
		   CCE.EC_CALIFIC,
		   CCE.US_CODIGO,
		   'Individual' as PC_GLOBAL,
		   ' ' as CP_CODIGO,
			0 AS PC_PESO,
		   CEC.ECC_COMENT  as Observaciones
	from COLABORA C
	join C_EMP_COMP CEC on (CEC.CB_CODIGO = C.CB_CODIGO )
	left join C_COMPETEN CC on (CC.CC_CODIGO = CEC.CC_CODIGO )
	left join C_EVA_COMP CCE on ( CCE.CB_CODIGO = C.CB_CODIGO and CCE.CC_CODIGO = CEC.CC_CODIGO and CCE.NC_NIVEL >= CEC.NC_NIVEL)
	where CC.CC_ACTIVO = 'S'
GO

CREATE function F_GET_MTRZ_HAB()
 RETURNS
  @Matriz TABLE (
	CB_CODIGO int,
	CP_CODIGO char(6),
	RP_FEC_INI Datetime,
	CC_CODIGO char(6),
	NC_NIVEL  int,
	RC_FEC_INI DateTime,
	PC_PESO Decimal(15,2),
	EC_NIVEL Smallint
   )
AS
BEGIN
   declare @PERFIL char(6);
   declare @Empleado int;
   declare @Compentencia char(6);
   declare @Nivel smallint;
   declare @PerFecIni DateTime;
   declare @Peso Decimal(15,2);

   declare cPERFILs cursor for Select CP_CODIGO from C_PERFIL ;

	OPEN cPERFILs
	FETCH NEXT FROM cPERFILs
	into @PERFIL
	WHILE @@FETCH_STATUS = 0
	begin
	     declare cCompt cursor for select CP.CC_CODIGO,CP.NC_NIVEL,CP.RP_FEC_INI,CP.PC_PESO from C_PER_COMP CP
								   left outer join C_COMPETEN CC on CC.CC_CODIGO = CP.CC_CODIGO
								   left outer join C_PERFIL PP on PP.CP_CODIGO = CP.CP_CODIGO
							   where CP.CP_CODIGO = @PERFIL and PP.CP_ACTIVO = 'S' and CC.CC_ACTIVO = 'S';
		 open cCompt;
		 FETCH NEXT FROM cCompt
	     into @Compentencia,@Nivel,@PerFecIni,@Peso

		 WHILE @@FETCH_STATUS = 0
		 begin
		 	  insert into @Matriz (cb_codigo,CP_CODIGO,RP_FEC_INI,CC_CODIGO,NC_NIVEL,RC_FEC_INI,PC_PESO,EC_NIVEL)
			  select
					C.CB_CODIGO,
					@PERFIL as PERFIL,
					@PerFecIni as FechaPerfil,
					isnull(CCE.CC_CODIGO,@Compentencia) as CC_CODIGO,
					isnull(CCE.NC_NIVEL,@Nivel)as NC_NIVEL,
					CRC.RC_FEC_INI AS RC_FEC_INI,
					(CASE WHEN ( CCE.NC_NIVEL >= @Nivel)THEN 1 Else 0 end ) as PC_PESO,
					IsNull(CCE.NC_NIVEL,0)as EC_NIVEL
					from COLABORA C
					left outer join C_EVA_COMP CCE on ( CCE.CB_CODIGO = C.CB_CODIGO ) and ( CCE.CC_CODIGO = @Compentencia )
					left outer join C_REV_COMP CRC on ( CRC.CC_CODIGO = @Compentencia )
					where CB_ACTIVO = 'S'
			  FETCH NEXT FROM cCompt
			  into @Compentencia,@Nivel,@PerFecIni,@Peso
		 end
		 close cCompt
	     deallocate cCompt
		 FETCH NEXT FROM cPERFILs
		 into @PERFIL
    end
	close cPERFILs
    deallocate cPERFILs
	RETURN
END
GO

create view V_MAT_HABL
AS
	SELECT F.CB_CODIGO,
	cast( rtrim(F.CP_CODIGO) as Varchar(6)) as CP_CODIGO,
	F.RP_FEC_INI,
	cast( rtrim(F.CC_CODIGO) as Varchar(6)) as CC_CODIGO,
	F.NC_NIVEL,
	F.RC_FEC_INI,
	F.PC_PESO,
	F.EC_NIVEL,
	CAST(C.CB_CODIGO as VarCHAR(10))+' : '+C.PRETTYNAME as PRETTYNAME,
	cast( rtrim(C.CB_NIVEL0) as Varchar(6)) as CB_NIVEL0,
	cast( rtrim(C.CB_PUESTO) as Varchar(6)) as CB_PUESTO,
	P.PU_DESCRIP,
	N1.TB_ELEMENT as Nivel_1,
	N2.TB_ELEMENT as Nivel_2,
	N3.TB_ELEMENT as Nivel_3,
	N4.TB_ELEMENT as Nivel_4,
	N5.TB_ELEMENT as Nivel_5,
	N6.TB_ELEMENT as Nivel_6,
	N7.TB_ELEMENT as Nivel_7,
	N8.TB_ELEMENT as Nivel_8,
	N9.TB_ELEMENT as Nivel_9,
	CASE WHEN(EC_NIVEL = 0 )THEN '0' else (CAST(F.EC_NIVEL as VARCHAR(5)) +' : '+NC.NC_DESCRIP) end AS NC_DESCRIP
	FROM F_GET_MTRZ_HAB() F
	left outer join COLABORA C on C.CB_CODIGO =F.CB_CODIGO
	left outer join PUESTO P on P.PU_CODIGO = C.CB_PUESTO
	left outer join NIVEL1 N1 on N1.TB_CODIGO = C.CB_NIVEL1
	left outer join NIVEL2 N2 on N2.TB_CODIGO = C.CB_NIVEL2
	left outer join NIVEL3 N3 on N3.TB_CODIGO = C.CB_NIVEL3
	left outer join NIVEL4 N4 on N4.TB_CODIGO = C.CB_NIVEL4
	left outer join NIVEL5 N5 on N5.TB_CODIGO = C.CB_NIVEL5
	left outer join NIVEL6 N6 on N6.TB_CODIGO = C.CB_NIVEL6
	left outer join NIVEL7 N7 on N7.TB_CODIGO = C.CB_NIVEL7
	left outer join NIVEL8 N8 on N8.TB_CODIGO = C.CB_NIVEL8
	left outer join NIVEL9 N9 on N9.TB_CODIGO = C.CB_NIVEL9
	left outer join C_NIV_COMP NC on NC.NC_NIVEL = F.EC_NIVEL and F.CC_CODIGO = NC.CC_CODIGO
	where C.CB_ACTIVO = 'S'
GO

create view V_DB_INFO (
	DB_CODIGO,
	DB_DESCRIP,
	DB_CONTROL,
	DB_TIPO,
	DB_DATOS,
	DB_USRNAME,
	DB_PASSWRD,
	DB_CTRL_RL,
	DB_CHKSUM
   ) as
select
	DB_CODIGO,
	DB_DESCRIP,
	DB_CONTROL,
	DB_TIPO,
	DB_DATOS,
	DB_USRNAME,
	DB_PASSWRD,
	DB_CTRL_RL,
	DB_CHKSUM
from #COMPARTE.dbo.DB_INFO
GO

CREATE VIEW V_EMP_TIMB 
AS
SELECT 'COLABORA' AS TABLA, 
       [CB_CODIGO]       ,[CB_ACTIVO]       ,[CB_APE_MAT]       ,[CB_APE_PAT]       ,[CB_AUTOSAL]       ,[CB_AR_FEC]       ,[CB_AR_HOR]       ,
       [CB_BAN_ELE]       ,[CB_CARRERA]       ,[CB_CHECA]       ,[CB_CIUDAD]       ,[CB_CLASIFI]       ,[CB_CODPOST]       ,[CB_CONTRAT]       ,
       [CB_CREDENC]       ,[CB_CURP]       ,[CB_CALLE]       ,[CB_COLONIA]       ,[CB_EDO_CIV]       ,[CB_FEC_RES]       ,[CB_EST_HOR]       ,
       [CB_EST_HOY]       ,[CB_ESTADO]       ,[CB_ESTUDIO]       ,[CB_EVALUA]       ,[CB_EXPERIE]       ,[CB_FEC_ANT]       ,[CB_FEC_BAJ]       ,
       [CB_FEC_BSS]       ,[CB_FEC_CON]       ,[CB_FEC_ING]       ,[CB_FEC_INT]       ,[CB_FEC_NAC]       ,[CB_FEC_REV]       ,[CB_FEC_VAC]       ,
       [CB_G_FEC_1]       ,[CB_G_FEC_2]       ,[CB_G_FEC_3]       ,[CB_G_FEC_4]       ,[CB_G_FEC_5]       ,[CB_G_FEC_6]       ,[CB_G_FEC_7]       ,
       [CB_G_FEC_8]       ,[CB_G_LOG_1]       ,[CB_G_LOG_2]       ,[CB_G_LOG_3]       ,[CB_G_LOG_4]       ,[CB_G_LOG_5]       ,[CB_G_LOG_6]       ,
       [CB_G_LOG_7]       ,[CB_G_LOG_8]       ,[CB_G_NUM_1]       ,[CB_G_NUM_2]       ,[CB_G_NUM_3]       ,[CB_G_NUM_4]       ,[CB_G_NUM_5]       ,
       [CB_G_NUM_6]       ,[CB_G_NUM_7]       ,[CB_G_NUM_8]       ,[CB_G_NUM_9]       ,[CB_G_NUM10]       ,[CB_G_NUM11]       ,[CB_G_NUM12]       ,
       [CB_G_NUM13]       ,[CB_G_NUM14]       ,[CB_G_NUM15]       ,[CB_G_NUM16]       ,[CB_G_NUM17]       ,[CB_G_NUM18]       ,[CB_G_TAB_1]       ,
       [CB_G_TAB_2]       ,[CB_G_TAB_3]       ,[CB_G_TAB_4]       ,[CB_G_TAB_5]       ,[CB_G_TAB_6]       ,[CB_G_TAB_7]       ,[CB_G_TAB_8]       ,
       [CB_G_TAB_9]       ,[CB_G_TAB10]       ,[CB_G_TAB11]       ,[CB_G_TAB12]       ,[CB_G_TAB13]       ,[CB_G_TAB14]       ,[CB_G_TEX_1]       ,
       [CB_G_TEX_2]       ,[CB_G_TEX_3]       ,[CB_G_TEX_4]       ,[CB_G_TEX_5]       ,[CB_G_TEX_6]       ,[CB_G_TEX_7]       ,[CB_G_TEX_8]       ,
       [CB_G_TEX_9]       ,[CB_G_TEX10]       ,[CB_G_TEX11]       ,[CB_G_TEX12]       ,[CB_G_TEX13]       ,[CB_G_TEX14]       ,[CB_G_TEX15]       ,
       [CB_G_TEX16]       ,[CB_G_TEX17]       ,[CB_G_TEX18]       ,[CB_G_TEX19]       ,[CB_HABLA]       ,[CB_TURNO]       ,[CB_IDIOMA]       ,
       [CB_INFCRED]       ,[CB_INFMANT]       ,[CB_INFTASA]       ,[CB_LA_MAT]       ,[CB_LAST_EV]       ,[CB_LUG_NAC]       ,[CB_MAQUINA]       ,
       [CB_MED_TRA]       ,[CB_PASAPOR]       ,[CB_FEC_INC]       ,[CB_FEC_PER]       ,[CB_NOMYEAR]       ,[CB_NACION]       ,[CB_NOMTIPO]       ,
       [CB_NEXT_EV]       ,[CB_NOMNUME]       ,[CB_NOMBRES]       ,[CB_PATRON]       ,[CB_PUESTO]       ,[CB_RFC]       ,[CB_SAL_INT]       ,
       [CB_SALARIO]       ,[CB_SEGSOC]       ,[CB_SEXO]       ,[CB_TABLASS]       ,[CB_TEL]       ,[CB_VIVECON]       ,[CB_VIVEEN]       ,
       [CB_ZONA]       ,[CB_ZONA_GE]       ,[CB_NIVEL1]       ,[CB_NIVEL2]       ,[CB_NIVEL3]       ,[CB_NIVEL4]       ,[CB_NIVEL5]       ,
       [CB_NIVEL6]       ,[CB_NIVEL7]       ,[CB_INFTIPO]       ,[CB_NIVEL8]       ,[CB_NIVEL9]       ,[CB_DER_FEC]       ,[CB_DER_PAG]       ,
       [CB_V_PAGO]       ,[CB_DER_GOZ]       ,[CB_V_GOZO]       ,[CB_TIP_REV]       ,[CB_MOT_BAJ]       ,[CB_FEC_SAL]       ,[CB_INF_INI]       ,
       [CB_INF_OLD]       ,[CB_OLD_SAL]       ,[CB_OLD_INT]       ,[CB_PRE_INT]       ,[CB_PER_VAR]       ,[CB_SAL_TOT]       ,[CB_TOT_GRA]       ,
       [CB_FAC_INT]       ,[CB_RANGO_S]       ,[CB_CLINICA]       ,[CB_NIVEL0]       ,[CB_FEC_NIV]       ,[CB_FEC_PTO]       ,[CB_AREA]       ,
       [CB_FEC_TUR]       ,[CB_FEC_KAR]       ,[CB_PENSION]       ,[CB_CANDIDA]       ,[CB_ID_NUM]       ,[CB_ENT_NAC]       ,[CB_COD_COL]       ,
       [CB_PLAZA]       ,[CB_FEC_PLA]       ,[CB_DER_PV]       ,[CB_V_PRIMA]       ,[CB_SUB_CTA]       ,[CB_NETO]       ,[CB_E_MAIL]       ,
       [CB_PORTAL]       ,[CB_DNN_OK]       ,[CB_USRNAME]       ,[CB_NOMINA]       ,[CB_FEC_NOM]       ,[CB_RECONTR]       ,[CB_DISCAPA]       ,
       [CB_INDIGE]       ,[CB_FONACOT]       ,[CB_EMPLEO]       ,[PRETTYNAME]       ,[CB_PASSWRD]       ,[US_CODIGO]       ,[LLAVE]       ,
       [CB_FEC_COV]       ,[CB_G_TEX20]       ,[CB_G_TEX21]       ,[CB_G_TEX22]       ,[CB_G_TEX23]       ,[CB_G_TEX24]       ,[CB_INFDISM]       ,
       [CB_INFACT]       ,[CB_NUM_EXT]       ,[CB_NUM_INT]       ,[CB_INF_ANT]       ,[CB_TDISCAP]       ,[CB_ESCUELA]       ,[CB_TESCUEL]       ,
       [CB_TITULO]       ,[CB_YTITULO]       ,[CB_CTA_GAS]       ,[CB_CTA_VAL]       ,[CB_MUNICIP]       ,[CB_ID_BIO]       ,[CB_GP_COD]       ,
       [CB_TIE_PEN]       ,[CB_TSANGRE]       ,[CB_ALERGIA]       ,[CB_BRG_ACT]       ,[CB_BRG_TIP]       ,[CB_BRG_ROL]       ,[CB_BRG_JEF]       ,
       [CB_BRG_CON]       ,[CB_BRG_PRA]       ,[CB_BRG_NOP]       ,[CB_BANCO]       ,[CB_REGIMEN]   
FROM COLABORA
UNION ALL
SELECT 'EMP_TRANSF' AS TABLA, 
       [CB_CODIGO]       ,[CB_ACTIVO]       ,[CB_APE_MAT]       ,[CB_APE_PAT]       ,[CB_AUTOSAL]       ,[CB_AR_FEC]       ,[CB_AR_HOR]       ,
       [CB_BAN_ELE]       ,[CB_CARRERA]       ,[CB_CHECA]       ,[CB_CIUDAD]       ,[CB_CLASIFI]       ,[CB_CODPOST]       ,[CB_CONTRAT]       ,
       [CB_CREDENC]       ,[CB_CURP]       ,[CB_CALLE]       ,[CB_COLONIA]       ,[CB_EDO_CIV]       ,[CB_FEC_RES]       ,[CB_EST_HOR]       ,
       [CB_EST_HOY]       ,[CB_ESTADO]       ,[CB_ESTUDIO]       ,[CB_EVALUA]       ,[CB_EXPERIE]       ,[CB_FEC_ANT]       ,[CB_FEC_BAJ]       ,
       [CB_FEC_BSS]       ,[CB_FEC_CON]       ,[CB_FEC_ING]       ,[CB_FEC_INT]       ,[CB_FEC_NAC]       ,[CB_FEC_REV]       ,[CB_FEC_VAC]       ,
       [CB_G_FEC_1]       ,[CB_G_FEC_2]       ,[CB_G_FEC_3]       ,[CB_G_FEC_4]       ,[CB_G_FEC_5]       ,[CB_G_FEC_6]       ,[CB_G_FEC_7]       ,
       [CB_G_FEC_8]       ,[CB_G_LOG_1]       ,[CB_G_LOG_2]       ,[CB_G_LOG_3]       ,[CB_G_LOG_4]       ,[CB_G_LOG_5]       ,[CB_G_LOG_6]       ,
       [CB_G_LOG_7]       ,[CB_G_LOG_8]       ,[CB_G_NUM_1]       ,[CB_G_NUM_2]       ,[CB_G_NUM_3]       ,[CB_G_NUM_4]       ,[CB_G_NUM_5]       ,
       [CB_G_NUM_6]       ,[CB_G_NUM_7]       ,[CB_G_NUM_8]       ,[CB_G_NUM_9]       ,[CB_G_NUM10]       ,[CB_G_NUM11]       ,[CB_G_NUM12]       ,
       [CB_G_NUM13]       ,[CB_G_NUM14]       ,[CB_G_NUM15]       ,[CB_G_NUM16]       ,[CB_G_NUM17]       ,[CB_G_NUM18]       ,[CB_G_TAB_1]       ,
       [CB_G_TAB_2]       ,[CB_G_TAB_3]       ,[CB_G_TAB_4]       ,[CB_G_TAB_5]       ,[CB_G_TAB_6]       ,[CB_G_TAB_7]       ,[CB_G_TAB_8]       ,
       [CB_G_TAB_9]       ,[CB_G_TAB10]       ,[CB_G_TAB11]       ,[CB_G_TAB12]       ,[CB_G_TAB13]       ,[CB_G_TAB14]       ,[CB_G_TEX_1]       ,
       [CB_G_TEX_2]       ,[CB_G_TEX_3]       ,[CB_G_TEX_4]       ,[CB_G_TEX_5]       ,[CB_G_TEX_6]       ,[CB_G_TEX_7]       ,[CB_G_TEX_8]       ,
       [CB_G_TEX_9]       ,[CB_G_TEX10]       ,[CB_G_TEX11]       ,[CB_G_TEX12]       ,[CB_G_TEX13]       ,[CB_G_TEX14]       ,[CB_G_TEX15]       ,
       [CB_G_TEX16]       ,[CB_G_TEX17]       ,[CB_G_TEX18]       ,[CB_G_TEX19]       ,[CB_HABLA]       ,[CB_TURNO]       ,[CB_IDIOMA]       ,
       [CB_INFCRED]       ,[CB_INFMANT]       ,[CB_INFTASA]       ,[CB_LA_MAT]       ,[CB_LAST_EV]       ,[CB_LUG_NAC]       ,[CB_MAQUINA]       ,
       [CB_MED_TRA]       ,[CB_PASAPOR]       ,[CB_FEC_INC]       ,[CB_FEC_PER]       ,[CB_NOMYEAR]       ,[CB_NACION]       ,[CB_NOMTIPO]       ,
       [CB_NEXT_EV]       ,[CB_NOMNUME]       ,[CB_NOMBRES]       ,[CB_PATRON]       ,[CB_PUESTO]       ,[CB_RFC]       ,[CB_SAL_INT]       ,
       [CB_SALARIO]       ,[CB_SEGSOC]       ,[CB_SEXO]       ,[CB_TABLASS]       ,[CB_TEL]       ,[CB_VIVECON]       ,[CB_VIVEEN]       ,
       [CB_ZONA]       ,[CB_ZONA_GE]       ,[CB_NIVEL1]       ,[CB_NIVEL2]       ,[CB_NIVEL3]       ,[CB_NIVEL4]       ,[CB_NIVEL5]       ,
       [CB_NIVEL6]       ,[CB_NIVEL7]       ,[CB_INFTIPO]       ,[CB_NIVEL8]       ,[CB_NIVEL9]       ,[CB_DER_FEC]       ,[CB_DER_PAG]       ,
       [CB_V_PAGO]       ,[CB_DER_GOZ]       ,[CB_V_GOZO]       ,[CB_TIP_REV]       ,[CB_MOT_BAJ]       ,[CB_FEC_SAL]       ,[CB_INF_INI]       ,
       [CB_INF_OLD]       ,[CB_OLD_SAL]       ,[CB_OLD_INT]       ,[CB_PRE_INT]       ,[CB_PER_VAR]       ,[CB_SAL_TOT]       ,[CB_TOT_GRA]       ,
       [CB_FAC_INT]       ,[CB_RANGO_S]       ,[CB_CLINICA]       ,[CB_NIVEL0]       ,[CB_FEC_NIV]       ,[CB_FEC_PTO]       ,[CB_AREA]       ,
       [CB_FEC_TUR]       ,[CB_FEC_KAR]       ,[CB_PENSION]       ,[CB_CANDIDA]       ,[CB_ID_NUM]       ,[CB_ENT_NAC]       ,[CB_COD_COL]       ,
       [CB_PLAZA]       ,[CB_FEC_PLA]       ,[CB_DER_PV]       ,[CB_V_PRIMA]       ,[CB_SUB_CTA]       ,[CB_NETO]       ,[CB_E_MAIL]       ,
       [CB_PORTAL]       ,[CB_DNN_OK]       ,[CB_USRNAME]       ,[CB_NOMINA]       ,[CB_FEC_NOM]       ,[CB_RECONTR]       ,[CB_DISCAPA]       ,
       [CB_INDIGE]       ,[CB_FONACOT]       ,[CB_EMPLEO]       ,[PRETTYNAME]       ,[CB_PASSWRD]       ,[US_CODIGO]       ,[LLAVE]       ,
       [CB_FEC_COV]       ,[CB_G_TEX20]       ,[CB_G_TEX21]       ,[CB_G_TEX22]       ,[CB_G_TEX23]       ,[CB_G_TEX24]       ,[CB_INFDISM]       ,
       [CB_INFACT]       ,[CB_NUM_EXT]       ,[CB_NUM_INT]       ,[CB_INF_ANT]       ,[CB_TDISCAP]       ,[CB_ESCUELA]       ,[CB_TESCUEL]       ,
       [CB_TITULO]       ,[CB_YTITULO]       ,[CB_CTA_GAS]       ,[CB_CTA_VAL]       ,[CB_MUNICIP]       ,[CB_ID_BIO]       ,[CB_GP_COD]       ,
       [CB_TIE_PEN]       ,[CB_TSANGRE]       ,[CB_ALERGIA]       ,[CB_BRG_ACT]       ,[CB_BRG_TIP]       ,[CB_BRG_ROL]       ,[CB_BRG_JEF]       ,
       [CB_BRG_CON]       ,[CB_BRG_PRA]       ,[CB_BRG_NOP]       ,[CB_BANCO]       ,[CB_REGIMEN]   
FROM EMP_TRANSF
GO

CREATE VIEW VTRESSFILE(
				TF_PATH, 
				TF_EXT, 
				TF_FILE, 
				TF_DESCR, 
				TF_VERSION, 
				TF_BUILD, 
				TF_FEC_MOD, 
				TF_CALLS, 
				TF_FEC_ACC, 
				TF_HOST, 
				TF_FEC_UPD)
as
		SELECT 	TF_PATH, 
				TF_EXT, 
				TF_FILE, 
				TF_DESCR, 
				TF_VERSION, 
				TF_BUILD, 
				TF_FEC_MOD, 
				TF_CALLS, 
				TF_FEC_ACC, 
				TF_HOST, 
				TF_FEC_UPD
	FROM #COMPARTE.dbo.TRESSFILES
GO



/* US #13089 Reporte de costeo pueda presentar los criterios de costeo */
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW COSTOS
AS
	SELECT	PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			CT.CO_NUMERO,
			COALESCE( CO_DESCRIP, '' ) CO_DESCRIP,
			CT.MO_REFEREN,
			CT.GpoCostoID GC_ID,
			GpoCostoCodigo GC_CODIGO,
			GpoCostoNombre GC_NOMBRE,
			CT.CC_CODIGO,
			COALESCE( CC_NOMBRE, '' ) CC_NOMBRE,
			CostoClasifi CT_CLASIFI,
			CostoMonto CT_MONTO,
			T_ConceptosCosto.CritCostoID CR_ID,
			CritCostoNombre CR_NOMBRE
	FROM	T_Costos CT
				JOIN NOMINA ON CT.NominaID = NOMINA.LLAVE
				JOIN T_GruposCosto ON CT.GpoCostoID = T_GruposCosto.GpoCostoID
				JOIN T_ConceptosCosto ON CT.CO_NUMERO = T_ConceptosCosto.CO_NUMERO and CT.GpoCostoID = T_ConceptosCosto.GpoCostoID
				JOIN T_CriteriosCosto ON T_CriteriosCosto.CritCostoID = T_ConceptosCosto.CritCostoID
				LEFT JOIN CONCEPTO ON CT.CO_NUMERO = CONCEPTO.CO_NUMERO
				LEFT JOIN CCOSTO ON CT.CC_CODIGO = CCOSTO.CC_CODIGO
GO

CREATE VIEW VACUMRSTOT WITH SCHEMABINDING 
AS
	SELECT  AC_YEAR  ,
			CB_CODIGO ,			
			CO_NUMERO  ,
			AC_MES_01  = coalesce( SUM(AR.AC_MES_01), 0 ) ,
			AC_MES_02  = coalesce( SUM(AR.AC_MES_02), 0 ) ,
			AC_MES_03  = coalesce( SUM(AR.AC_MES_03), 0 ) , 
			AC_MES_04  = coalesce( SUM(AR.AC_MES_04), 0 ) ,
			AC_MES_05  = coalesce( SUM(AR.AC_MES_05), 0 ) ,
			AC_MES_06  = coalesce( SUM(AR.AC_MES_06), 0 ) ,
			AC_MES_07  = coalesce( SUM(AR.AC_MES_07), 0 ) ,
			AC_MES_08  = coalesce( SUM(AR.AC_MES_08), 0 ) ,
			AC_MES_09  = coalesce( SUM(AR.AC_MES_09), 0 ) ,
			AC_MES_10  = coalesce( SUM(AR.AC_MES_10), 0 ) ,
			AC_MES_11  = coalesce( SUM(AR.AC_MES_11), 0 ) ,
			AC_MES_12  = coalesce( SUM(AR.AC_MES_12), 0 ) ,
			AC_MES_13  = coalesce( SUM(AR.AC_MES_13), 0 ) ,
			AC_ANUAL   = coalesce( SUM(AR.AC_ANUAL), 0 )
	FROM	dbo.ACUMULA_RS AR
	GROUP BY AC_YEAR, CB_CODIGO, CO_NUMERO
GO

create view V_TIM_REG as 
Select TL_VALOR, TL_DESCRIP, TL_TEXTO, TL_INGLES, TL_NUMERO, TL_ACTIVO
from TimListas 
where TL_IDLISTA = 5002
 
GO


CREATE VIEW TOT_COSTOS
as
select PE_YEAR, PE_TIPO, PE_NUMERO, CT_SUB_CTA, CT_CAR_ABO, CT_NUMERO,
    cast ( ( sum(CT_MONTO)) as numeric(15,2)) CP_MONTO
from COSTOS_POL
group by PE_YEAR, PE_TIPO, PE_NUMERO, CT_SUB_CTA, CT_CAR_ABO, CT_NUMERO
GO

create view VHorasTim
		as
			select CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO,
					  SUM(AU_DOBLES) AU_DOBLES, SUM(AU_TRIPLES) AU_TRIPLES, SUM(AU_DES_TRA) AU_DES_TRA, SUM(AU_FES_TRA) AU_FES_TRA
			from
			(
			(select CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO, AU_DOBLES, AU_TRIPLES,
			case 
				   when AU_TIPODIA = 8 then 0
				   else  AU_DES_TRA
			end AU_DES_TRA,
			case 
				   when AU_TIPODIA = 8 then AU_DES_TRA
				   else 0
			end AU_FES_TRA
			from AUSENCIA )
			union all
			(select CB_CODIGO, AU_FECHA=FALTAS.FA_FEC_INI,PE_TIPO, PE_YEAR, PE_NUMERO,
			sum(CASE WHEN  FA_MOTIVO IN (1,2) THEN FA_HORAS ELSE 0 end) AS AU_DOBLES,
			sum(CASE WHEN  FA_MOTIVO =3 THEN FA_HORAS ELSE 0 end) AS AU_TRIPLES,
			sum(CASE WHEN FA_MOTIVO =8 THEN FA_HORAS ELSE 0 end) AS AU_DES_TRA,
			sum(CASE WHEN FA_MOTIVO =7 THEN FA_HORAS ELSE 0 end) AS AU_FES_TRA
			from FALTAS where FA_DIA_HOR = 'H'
			group by CB_CODIGO,  PE_TIPO, PE_YEAR, PE_NUMERO, FALTAS.FA_FEC_INI )
			) HorasTotales
			group by CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO

GO

CREATE view V_SUSCRIP  as SELECT CM_CODIGO, RE_CODIGO=CA_REPORT, US_CODIGO from  #COMPARTE..V_SUSCRIP
group by CM_CODIGO, CA_REPORT, US_CODIGO 
GO

create view V_SUSC_CAL  as SELECT 
CA_FOLIO, CA_NOMBRE, CA_DESCRIP, CA_ACTIVO, CM_CODIGO,  CA_FREC, CA_HORA, CA_ULT_FEC, CA_REPORT, VS_TIPO, RO_CODIGO, RO_NOMBRE, US_CODIGO, US_NOMBRE, US_EMAIL 
 from  #COMPARTE..V_SUSCRIP
GO
 
CREATE view V_CALENDAR  as 
    SELECT CA_FOLIO,CA_NOMBRE,CA_DESCRIP,CA_ACTIVO,CM_CODIGO,CA_REPORT,CA_REPEMP,CA_FECHA,CA_HORA,CA_FREC,
        CA_RECUR,CA_DOWS,CA_MESES,CA_MESDIAS,CA_MES_ON,CA_MESWEEK,CA_FEC_ESP,CA_TSALIDA,CA_FSALIDA,CA_ULT_FEC,CA_NX_FEC,
        CA_NX_EVA,CA_US_CHG,CA_CAPTURA  
    FROM #COMPARTE..CALENDARIO
GO

create view V_CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CB_NIVEL0,
  CU_CODIGO,
  CU_NOMBRE,
  KC_FEC_PRO,
  TOMADO,
  KC_FEC_TOM,
  CU_HORAS,
  EN_OPCIONA,
  EN_LISTA,
  CU_STPS,
  MA_CODIGO,
  CU_REVISIO,
  CP_MANUAL,
  EP_GLOBAL,
  CU_CLASIFI,
  CU_CLASE
) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  C.CB_NIVEL0,
  CU.CU_CODIGO,
  CU.CU_NOMBRE,
  ( select case when ( K.KC_FEC_TOM is NULL ) then DATEADD( DAY, E.EN_DIAS, C.CB_FEC_PTO )
                when ( K.KC_FEC_TOM is not NULL ) and ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'N' ) or ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'S' ) and ( K.KC_REVISIO = CU.CU_REVISIO ) ) ) then K.KC_FECPROG
				when ( K.KC_FEC_TOM is not NULL ) and ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) or ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' ) and ( K.KC_REVISIO = CU.CU_REVISIO ) ) ) then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				when ( K.KC_FEC_TOM is not NULL ) and ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'S' ) and ( ( K.KC_FEC_TOM < CU.CU_FEC_REV ) ) and (K.KC_REVISIO <> CU.CU_REVISIO )) then CU.CU_FEC_REV
				else CU.CU_FEC_REV end ) KC_FEC_PRO,
  ( case when ( K.KC_FEC_TOM is NOT NULL ) then 1 else 0 end ) TOMADO,
  ( select case when ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'N' ) then K.KC_FEC_TOM
				when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) OR ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' )and ( K.KC_REVISIO <> CU.CU_REVISIO ) ) ) then null
				when ( ( E.EN_RE_DIAS = 0 AND E.EN_REPROG = 'S' ) and ( K.KC_REVISIO <> CU.CU_REVISIO ) ) then null
				else K.KC_FEC_TOM End ) KC_FEC_TOM,
  CU.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CU.CU_STPS,
  CU.MA_CODIGO,
  CU.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_MANUAL,
  cast( 'N' as CHAR(1) ) EP_GLOBAL,
  CU.CU_CLASIFI,
  CU.CU_CLASE
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO K on ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CU_CODIGO = E.CU_CODIGO ) and ( K.KC_FEC_TOM = ( select MAX( KC_FEC_TOM ) from KARCURSO AS K1 where ( K1.CB_CODIGO = C.CB_CODIGO ) and ( K1.CU_CODIGO = E.CU_CODIGO ) ))
  left join CURSO CU on ( CU.CU_CODIGO = E.CU_CODIGO )
  where ( CU.CU_ACTIVO = 'S' )

UNION ALL

select
  C.CB_CODIGO,
  C.CB_PUESTO,
  C.CB_NIVEL0,
  CURSO.CU_CODIGO,
  CURSO.CU_NOMBRE,
  (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end) KC_FEC_PRO,
  ( case when ( K.KC_FEC_TOM is NOT NULL ) then 1 else 0 end ) TOMADO,
  K.KC_FEC_TOM,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.CU_STPS,
  CURSO.MA_CODIGO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_MANUAL,
  EP.EP_GLOBAL,
  CURSO.CU_CLASIFI,
  CURSO.CU_CLASE
  from COLABORA C
  join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
  left join KARCURSO K on ( K.CU_CODIGO = EP.CU_CODIGO and EP.CB_CODIGO = K.CB_CODIGO and K.KC_FEC_TOM >= (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_PTO + EP.EP_DIAS else EP.EP_FECHA end	) )
where ( CURSO.CU_ACTIVO = 'S' )
GO

create function FN_GetConceptoFonacot() returns Concepto
as 
begin 
	declare @TipoPrestamoFonacot Codigo1
	declare @ConceptoNumero Concepto

	set @ConceptoNumero = 0

	select @TipoPrestamoFonacot = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	set @TipoPrestamoFonacot = COALESCE( @TipoPrestamoFonacot, '' ) 

	if (@TipoPrestamoFonacot <> '' ) 
		select  @ConceptoNumero = p.TB_CONCEPT from TPRESTA p where P.TB_CODIGO = @TipoPrestamoFonacot

	return @ConceptoNumero 
end 
GO

create function CedFon_GetStatus(@PE_YEAR int, @PE_MES_FON int, @CB_CODIGO NumeroEmpleado, @MO_REFEREN Referencia, @FT_STATUS Status ) 
returns int 
as
begin 

	declare @NumeroPeriodo int 
	declare @StatusPeriodo int 
	declare @TipoNomina	   int 
	
	set @FT_STATUS = coalesce( @FT_STATUS, 0 ) 
	
	-- Si el pago fonacot no se ha calculado total , entonces la cedula esta abierta
	if ( @FT_STATUS < 2 ) 
		return 1 

	-- Si el pago fonacot esta cerrado, entonces tambien la c�dula 
	if ( @FT_STATUS >= 3 ) 
		return 2 
			
	-- Si el usuario no acostumbra a cerrar la nomina entonces debemos buscar en los periodos del mes fonacot del tipo de nomina del empleado 	
	select @TipoNomina = CB_NOMINA from COLABORA where CB_CODIGO = @CB_CODIGO 

	
	-- Si hay al menos una nomina sin afectar entonces arroja que todavia hay pendientes
	if ( select COUNT(*) from PERIODO where PE_MES_FON = @PE_MES_FON  and PE_YEAR = @PE_YEAR  and PE_TIPO = @TipoNomina and PE_STATUS < 6  ) = 0  
		return 1
	else
		return 2
		


	return 0 
	
end  
GO 

create view VMOV_FON 
as
select M.PE_YEAR, P.PE_MES_FON,  NO.CB_CODIGO, M.MO_REFEREN, ( M.MO_PERCEPC + M.MO_DEDUCCI ) MF_MONTO, P.PE_NUMERO, P.PE_FEC_PAG, P.PE_MES, P.PE_POS_MES  from MOVIMIEN M 
      join NOMINA NO on 
              ( NO.PE_YEAR = M.PE_YEAR ) and
               ( NO.PE_TIPO = M.PE_TIPO ) and
               ( NO.PE_NUMERO = M.PE_NUMERO ) and 
               ( NO.CB_CODIGO  = M.CB_CODIGO ) 
      join PERIODO P on
               ( P.PE_YEAR = NO.PE_YEAR ) and
               ( P.PE_TIPO = NO.PE_TIPO ) and
               ( P.PE_NUMERO = NO.PE_NUMERO )
      where ( M.MO_ACTIVO = 'S' ) and ( NO.NO_STATUS >= 6 ) and M.CO_NUMERO = dbo.FN_GetConceptoFonacot()


GO 

create function MovFon_GetPeriodoInicial(@PE_YEAR int, @PE_MES_FON int, @CB_CODIGO NumeroEmpleado, @MO_REFEREN Referencia ) 
returns int 
as
begin 

	declare @NumeroPeriodo int 


	select TOP 1 @NumeroPeriodo = PE_NUMERO from VMOV_FON where PE_YEAR = @PE_YEAR and  PE_MES_FON = @PE_MES_FON	 and CB_CODIGO = @CB_CODIGO and MO_REFEREN = @MO_REFEREN 
	order by PE_FEC_PAG 

	set @NumeroPeriodo = coalesce( @NumeroPeriodo, 0 ) 

	return @NumeroPeriodo 
	
end
GO 

create function MovFon_GetPeriodoFinal(@PE_YEAR int, @PE_MES_FON int, @CB_CODIGO NumeroEmpleado, @MO_REFEREN Referencia ) 
returns int 
as
begin 

	declare @NumeroPeriodo int 


	select TOP 1 @NumeroPeriodo = PE_NUMERO from VMOV_FON where PE_YEAR = @PE_YEAR and  PE_MES_FON = @PE_MES_FON	 and CB_CODIGO = @CB_CODIGO and MO_REFEREN = @MO_REFEREN 
	order by PE_FEC_PAG desc 

	set @NumeroPeriodo = coalesce( @NumeroPeriodo, 0 ) 

	return @NumeroPeriodo 
	
end
GO 

CREATE view VCED_FON(
    CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
	RS_CODIGO,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    PF_NOMINA, 
    PF_NOM_QTY, 
	PF_STATUS,
	PERIODO_INICIAL, 
	PERIODO_FINAL )
as
select
    PF.CB_CODIGO,
    PF.PR_TIPO,
    PF.PR_REFEREN,
    PF_YEAR,
    PF_MES,
	RS_CODIGO,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
	PF_REUBICA,
    US_CODIGO,
    coalesce( Afectadas.PF_Monto, 0.00 ) , 
    coalesce( Afectadas.PF_Cuantos, 0 ) , 
	coalesce(dbo.CedFon_GetStatus(PE_YEAR, PE_MES_FON, PF.CB_CODIGO, MO_REFEREN, FT.FT_STATUS ), 0), 
	coalesce(dbo.MovFon_GetPeriodoInicial(PE_YEAR, PE_MES_FON, PF.CB_CODIGO, MO_REFEREN) , 0 ) , 
	coalesce( dbo.MovFon_GetPeriodoFinal(PE_YEAR, PE_MES_FON, PF.CB_CODIGO, MO_REFEREN)  , 0 )  
from PCED_FON PF
left outer join 
(
    select M.PE_YEAR, PE_MES_FON,  CB_CODIGO, M.MO_REFEREN, coalesce( SUM( MF_MONTO ) , 0.00 )  PF_Monto, count(*) PF_Cuantos from 
	VMOV_FON M
    group by  PE_YEAR, PE_MES_FON,  CB_CODIGO, MO_REFEREN
) Afectadas on Afectadas.PE_YEAR = PF_YEAR and Afectadas.PE_MES_FON = PF.PF_MES and  Afectadas.CB_CODIGO = PF.CB_CODIGO and Afectadas.MO_REFEREN = PF.PR_REFEREN 
LEFT OUTER JOIN FON_TOT FT on FT.FT_YEAR = PF.PF_YEAR and FT.FT_MONTH = PF.PF_MES 
GO 

create view VCEDTFON(
    CB_CODIGO,   
	PF_YEAR, 
    PF_MES,
	RS_CODIGO,
    CT_PAGO,   
    CT_NOMINA, 
	CT_AJUSTE,
	CT_DIF, 
	CT_DIF_QTY, 
	CT_CRE_QTY   )
as
select VF.CB_CODIGO, PF_YEAR, PF_MES, RS_CODIGO, coalesce(SUM(PF_PAGO),0.00)  CT_PAGO, coalesce(SUM(PF_NOMINA),0.00) CT_NOMINA,  coalesce( SUM(FC.FC_AJUSTE), 0.00) CT_AJUSTE,   coalesce( SUM( PF_PAGO - (PF_NOMINA + FC.FC_AJUSTE) ),0.00) CT_DIF, 
coalesce( SUM(case when PF_PAGO > ( PF_NOMINA + FC.FC_AJUSTE ) then 1 else 0 end), 0 ) CT_DIF_QTY , 
count(*) CT_CRE_QTY
from VCED_FON  VF
join FON_CRE FC on FC.FT_YEAR = VF.PF_YEAR and FC.FT_MONTH = VF.PF_MES and FC.CB_CODIGO = VF.CB_CODIGO  and FC.PR_TIPO = VF.PR_TIPO and FC.PR_REFEREN = VF.PR_REFEREN
group by  VF.CB_CODIGO, PF_YEAR, PF_MES, RS_CODIGO

GO 

CREATE FUNCTION FONACOT_GET_FECHA_INICIO( @FT_YEAR INT, @FT_MONTH INT, @CB_NOMINA INT) RETURNS FECHA 
AS
BEGIN 
	DECLARE @FECHA FECHA

	SELECT @FECHA = PE_FEC_INI FROM PERIODO P INNER JOIN	FN_FONACOT_PERIODOS (@FT_YEAR, @FT_MONTH, @CB_NOMINA) FP
			ON P.PE_NUMERO = FP.PE_NUMERO
			where
				PE_YEAR = @FT_YEAR and PE_MES_FON = @FT_MONTH 	AND PE_TIPO =@CB_NOMINA		AND PE_POS_FON = 1

	RETURN COALESCE( @FECHA, '2018-01-01' ) 
END 
GO

CREATE view VCED_CALCULA
as
	select
	PF_YEAR, PF_MES, CB_CODIGO, PR_REFEREN,  PR_TIPO, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE,
	PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA,
	FE_INCIDE,
	PR_STATUS,
	PR_FON_MOT,
	FECHA_INI_BAJA as FE_FECHA1,
	FECHA_FIN as FE_FECHA2,
	PF_REUBICA,
	CT_DIF_QTY,
	FE_CUANTOS
	from (

		SELECT F.PF_YEAR, F.PF_MES, C.CB_CODIGO, F.PR_REFEREN,  F.PR_TIPO, F.PF_PAGO, F.PF_PLAZO, F.PF_PAGADAS,
			coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,
						coalesce(case
								when FE.FE_INCIDE = 'B'  then 'B'
								when FE.FE_INCIDE is null  and C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then 'B'
								when ((FE.FE_INCIDE is null or FE.FE_INCIDE = '' or FE.FE_INCIDE = '0') AND P.PR_STATUS = 0) then '0'
								else FE.FE_INCIDE
						end, '')   FE_INCIDE ,
						coalesce(case
							when FE.FE_INCIDE = 'B'  then   FE.FE_FECHA1
								when FE.FE_INCIDE is null  and C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then CB_FEC_BAJ
							else FE.FE_FECHA1
						end , '' ) AS FECHA_INI_BAJA,
						coalesce( case
							when FE.FE_INCIDE = 'B'  then  '1899-12-30'
							else FE.FE_FECHA2
						end, ''  ) AS FECHA_FIN,
							F.PF_REUBICA,
						coalesce(PR_STATUS,0) PR_STATUS,
						coalesce(PR_FON_MOT,0) PR_FON_MOT,
						coalesce(CT.CT_DIF_QTY, 0) CT_DIF_QTY,
						coalesce(FE.FE_CUANTOS, 0 ) FE_CUANTOS
			FROM PCED_FON F
				LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
				LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
				LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN AND F.PR_TIPO = FC.PR_TIPO
				LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES
				LEFT JOIN PRESTAMO P ON P.PR_REFEREN = F.PR_REFEREN AND P.PR_TIPO = F.PR_TIPO AND P.CB_CODIGO = F.CB_CODIGO
		) Cedula
GO

CREATE view VCED_CONSULTA
as
	select 
	PF_YEAR, PF_MES, RS_CODIGO, CB_CODIGO, PR_REFEREN,  PR_TIPO, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE, 
	PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA, 
	FE_INCIDE, 
	PR_STATUS,
	PR_FON_MOT,
	FECHA_INI_BAJA as FE_FECHA1, 
	FECHA_FIN as FE_FECHA2, 
	PF_REUBICA, 
	FE_DIF_QTY, 
	FE_CUANTOS,
	PF_CTD
	from ( 

	SELECT F.PF_YEAR, F.PF_MES, F.RS_CODIGO, C.CB_CODIGO, F.PR_REFEREN,  F.PR_TIPO, F.PF_PAGO, F.PF_PLAZO, F.PF_PAGADAS,
		coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,				
					case
					 when (FE.FE_INCIDE is null or FE.FE_INCIDE = '0' ) and  C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then 'B' 	
					 else coalesce(FE_INCIDE, '0') 
					end 					 
					AS FE_INCIDE,
					case
					 when (FE.FE_INCIDE is null or FE.FE_INCIDE = '0' ) and C.CB_ACTIVO = 'N' and C.CB_FEC_BAJ < dbo.FONACOT_GET_FECHA_INICIO(F.PF_YEAR, F.PF_MES, C.CB_NOMINA )	then CB_FEC_BAJ	
					 when  FE.FE_INCIDE = 'B' then CB_FEC_BAJ
					 else coalesce(FE.FE_FECHA1, '1899-12-30') 
					end 					 
					AS FECHA_INI_BAJA, 
					coalesce(FE.FE_FECHA2, '1899-12-30') AS FECHA_FIN, 
					F.PF_REUBICA, 
					FC.FC_PR_STA AS PR_STATUS,
					FC.FC_FON_MOT AS PR_FON_MOT, 
					FE.FE_DIF_QTY, 
					(SELECT COUNT(PCF1.CB_CODIGO) FROM PCED_FON PCF1
					WHERE F.CB_CODIGO = PCF1.CB_CODIGO AND F.PF_YEAR = PCF1.PF_YEAR
					AND F.PF_MES = PCF1.PF_MES AND F.PR_TIPO = PCF1.PR_TIPO) FE_CUANTOS,
					PF_CTD				
		FROM PCED_FON F
			LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
			LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
			LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN  AND F.PR_TIPO = FC.PR_TIPO 
			LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES				
	
		) Cedula

GO

CREATE view VCED_DET
as
select
PF_YEAR, PF_MES, RS_CODIGO, CB_FONACOT, CB_RFC, CB_SEGSOC, CB_CODIGO, PRETTYNAME, PR_REFEREN, PR_TIPO, PR_STATUS,  PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE,
PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA,
FE_INCIDE,
case FE_INCIDE
	when '0' then ''
	when 'B' then 'Baja'
	when 'I' then 'Incapacidad'
	when 'C' then 'Pr�stamo cancelado'
	when 'M' then 'Pago de m�s'
	when 'L' then 'Pago de menos'
	else  'Otro'
end FE_IN_DESC,
CASE PR_STATUS
	when 0 then ''
	else PR_FON_MOT
END PR_FON_MOT,
FECHA_INI_BAJA as FE_FECHA1, FECHA_FIN as FE_FECHA2, PF_REUBICA, FE_DIF_QTY, FE_CUANTOS, PF_CTD
from ( 
SELECT VC.PF_YEAR, VC.PF_MES,  coalesce(VC.RS_CODIGO,'') RS_CODIGO,  C.CB_FONACOT, C.CB_RFC, C.CB_SEGSOC, coalesce(UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES), '')  AS PRETTYNAME, VC.PR_REFEREN, VC.PR_TIPO, VC.PR_STATUS,  VC.PF_PAGO, coalesce(C.CB_CODIGO,0) CB_CODIGO, VC.PF_PLAZO, VC.PF_PAGADAS,
	coalesce(VC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(VC.FC_AJUSTE,0.00) FC_AJUSTE ,
				
				case
					when coalesce( FE_INCIDE, '0')  = '0'  and PR_STATUS = 1  then 'C'
				    when coalesce( FE_INCIDE, '0')  = '0' AND ((FC_NOMINA+FC_AJUSTE)>PF_PAGO) then 'M'
				    when coalesce( FE_INCIDE, '0')  = '0' AND ((FC_NOMINA+FC_AJUSTE)<PF_PAGO) then 'L'
					else coalesce( FE_INCIDE, '0' )
				end AS FE_INCIDE,
				case
					when CONVERT (VARCHAR(10), VC.FE_FECHA1, 103) = '30/12/1899'  then ''
					else CONVERT (VARCHAR(10), VC.FE_FECHA1, 103)
				end AS FECHA_INI_BAJA,
				case
					when CONVERT (VARCHAR(10), VC.FE_FECHA2, 103) = '30/12/1899' then ''
					else CONVERT (VARCHAR(10), VC.FE_FECHA2, 103)
				end AS FECHA_FIN, 
			    VC.PF_REUBICA,				 
				case PR_FON_MOT 
					when 0 then 'No aplica' 
					when 1 then 'Pago directo en Fonacot' 
					when 2 then 'Confirmaci�n de Fonacot' 
					when 3 then 'Ausente en C�dula'
					when 4 then 'CanceladoOtro'
					else  '' 
				end PR_FON_MOT, VC.FE_DIF_QTY, VC.FE_CUANTOS, PF_CTD
	FROM VCED_CONSULTA VC
		LEFT JOIN COLABORA C ON VC.CB_CODIGO = C.CB_CODIGO
	) Cedula 

GO   

create view V_CONCEPVT
as
select KT.KT_VERSION, KT.KT_FECHA, KT.CO_NUMERO, KT.KC_VERSION, KC.US_CODIGO, KC.US_FEC_MOD
	  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
      ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
      ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
      ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
      ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
      ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
      ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
      ,CO_PS_TIPO
 from CONCEPTOVT KT
left outer join KAR_CONCEP  KC on KC.CO_NUMERO = KT.CO_NUMERO and KC.KC_VERSION = KT.KC_VERSION

go

create view V_CONTNOM
as
	select NO.PE_YEAR, NO.PE_TIPO, NO.PE_NUMERO, NO.CB_CODIGO,  KT.CO_NUMERO, KC.KC_VERSION
		  ,CO_A_PTU      ,CO_ACTIVO      ,CO_CALCULA      ,CO_DESCRIP      ,CO_FORMULA
		  ,CO_G_IMSS      ,CO_G_ISPT      ,CO_IMP_CAL      ,CO_IMPRIME      ,CO_LISTADO
		  ,CO_MENSUAL      ,CO_QUERY      ,CO_RECIBO      ,CO_TIPO      ,CO_X_ISPT
		  ,CO_SUB_CTA      ,CO_CAMBIA      ,CO_D_EXT      ,CO_D_NOM      ,CO_LIM_SUP
		  ,CO_LIM_INF      ,CO_VER_INF      ,CO_VER_SUP      ,CO_VER_ACC      ,CO_GPO_ACC
		  ,CO_ISN      ,CO_SUMRECI      ,CO_FRM_ALT      ,CO_USO_NOM      ,CO_SAT_CLP
		  ,CO_SAT_TPP      ,CO_SAT_CLN      ,CO_SAT_TPN      ,CO_SAT_EXE      ,CO_SAT_CON
		  ,CO_PS_TIPO, NO_VER_TIM
	 from NOMINA NO
	left outer join CONCEPTOVT  KT on KT.KT_VERSION = NO.NO_VER_TIM 
	left outer join KAR_CONCEP  KC on KC.CO_NUMERO = KT.CO_NUMERO and KC.KC_VERSION = KT.KC_VERSION
go

create view VMONTOSTIM
as
	select MO.PE_YEAR, MO.PE_TIPO, MO.PE_NUMERO, MO.CB_CODIGO, MO.MO_ACTIVO, MO.CO_NUMERO, MO.MO_IMP_CAL, MO.US_CODIGO, MO.MO_PER_CAL, MO.MO_REFEREN, MO.MO_X_ISPT, MO.MO_PERCEPC, 
		   MO.MO_DEDUCCI, CN.CO_SAT_CLP, CN.CO_SAT_TPP, CN.CO_SAT_CLN, CN.CO_SAT_TPN, CN.CO_SAT_EXE, CN.CO_SAT_CON, CN.NO_VER_TIM,	   
		   ( CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN CN.CO_SAT_CLN ELSE CN.CO_SAT_CLP END ) AS CLASE,
		   ( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPN ) AS TPN,
		   ( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPP ) AS TPP,	   
		   CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN (( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPN )) ELSE ( select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CN.CO_SAT_TPP ) END AS TIPO_SAT,
		   CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN ( CN.CO_SAT_TPN ) ELSE ( CN.CO_SAT_TPP ) END AS VT_TIP_SAT,
		   ( 	   
			CASE
				WHEN ( ( CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN CN.CO_SAT_CLN ELSE CN.CO_SAT_CLP END )  = 4 )
				THEN ( CASE WHEN ( MO.MO_PERCEPC+MO.MO_DEDUCCI<0) THEN (-(MO.MO_PERCEPC+MO_DEDUCCI)) ELSE (MO.MO_PERCEPC+MO.MO_DEDUCCI) END  )
				ELSE ( CASE WHEN ( MO.MO_PERCEPC+MO.MO_DEDUCCI<0 AND CN.CO_SAT_CLP<>CN.CO_SAT_CLN ) THEN (-(MO_PERCEPC+MO_DEDUCCI)) ELSE (MO_PERCEPC+MO_DEDUCCI) END ) END
		   ) AS MONTO,
		   (
			CASE
				WHEN ( ( CASE WHEN( MO.MO_PERCEPC + MO.MO_DEDUCCI < 0 ) THEN CN.CO_SAT_CLN ELSE CN.CO_SAT_CLP END ) = 1 )
				THEN (MO.MO_X_ISPT)
				ELSE (0) END
			) AS EXENTO
		   from MOVIMIEN MO
	join V_CONTNOM CN on CN.PE_YEAR = MO.PE_YEAR and CN.PE_TIPO = MO.PE_TIPO and CN.PE_NUMERO = MO.PE_NUMERO and CN.CB_CODIGO = MO.CB_CODIGO and CN.CO_NUMERO = MO.CO_NUMERO
	join NOMINA NO on  NO.PE_YEAR = MO.PE_YEAR and NO.PE_TIPO = MO.PE_TIPO and NO.PE_NUMERO = MO.PE_NUMERO and NO.CB_CODIGO = MO.CB_CODIGO  
	where NO.NO_STATUS > 4 and NO.NO_TIMBRO = 2 

go

CREATE VIEW VHGTI
AS
SELECT
	BIO.ID_NUMERO,
	COALESCE (HU_ID, 0) AS HU_ID,
	COALESCE (H.CM_CODIGO, '') AS CM_CODIGO,
	H.CB_CODIGO,
	COALESCE (HU_INDICE, 0) AS HU_INDICE,
	COALESCE (H.IV_CODIGO, 0) AS IV_CODIGO,
	H.HU_ID AS HU_CNT,
	H.HU_HUELLA AS HUELLA,
	CO.CM_DIGITO + REPLICATE('0', 9 - LEN( H.CB_CODIGO ) ) + cast ( H.CB_CODIGO as Varchar) + 'A' as CHECADA
FROM #COMPARTE..HUELLAGTI H
left outer join #COMPARTE..EMP_BIO BIO on H.CB_CODIGO = BIO.CB_CODIGO and H.CM_CODIGO = BIO.CM_CODIGO
left outer join #COMPARTE..COMPANY CO on H.CM_CODIGO = CO.CM_CODIGO
WHERE ( BIO.CB_ACTIVO = 'S' ) 
or ( ( BIO.CB_ACTIVO = 'N' ) and ( BIO.CB_FEC_BAJ >= Getdate() ) )
and ( BIO.IV_CODIGO = 0 )
GO

CREATE VIEW VPGTI
AS
SELECT
	EI.ID_NUMERO,
	COALESCE (EI.CM_CODIGO, '') AS CM_CODIGO,
	EI.CB_CODIGO,
	CO.CM_DIGITO + REPLICATE('0', 9 - LEN( EI.CB_CODIGO ) ) + cast ( EI.CB_CODIGO as Varchar) + 'A' as CHECADA
FROM #COMPARTE..EMP_ID EI
left outer join #COMPARTE..COMPANY CO on EI.CM_CODIGO = CO.CM_CODIGO
GO

create view V_EMP_TIMB_SIMPLE
as
select distinct CB_CODIGO, PRETTYNAME, CB_CURP, CB_RFC  FROM V_EMP_TIMB EMP 

Go 

create view VTIMCONCIL
as
select 
	RSOCIAL.RS_CODIGO,
    NO.PE_YEAR, NO.PE_TIPO, NO.PE_NUMERO, 
	NO.CB_CODIGO, 
	EMP.PRETTYNAME, 	
	NO.NO_TIMBRO, 
	case 
		when NO.NO_TIMBRO = 0 then 'Pendiente' 
		when NO.NO_TIMBRO = 2 then 'Timbrada' 
		else 'Pendiente' 
	end  NO_TIM_DESC, 
	case 
		when NO.NO_TIMBRO = 0 then 'Pendiente' 
		when NO.NO_TIMBRO = 2 then 'Timbrada' 
		else 'Pendiente' 
	end  NO_TIM_DES, 
	case  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID = ''	then NO.NO_STATUS  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID <> ''   then NO.NO_STATUS 
		when NO.NO_TIMBRO = 2							then 8 
		else	
			NO.NO_STATUS 				
	end NC_STATUS_TRESS, 	
	coalesce(NC_STATUS,-1) NC_STATUS, 
	coalesce(NC_RESULT, -1 ) NC_RESULT, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DESC, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DES, 
    coalesce(NC_VER_COD, '' ) NC_VER_COD, 
	coalesce(NC_VERSION, '' ) NC_VERSION, 
	NO.NO_FACUUID,
	NO.NO_FACTURA, 
	coalesce(NC_FACTURA, 0 ) NC_FACTURA,
	coalesce(NC_UUID , '' ) NC_UUID, 
	coalesce(NC_FEC_TIM, '1899-12-30') NC_FEC_TIM,
	EMP.CB_CURP, 
	coalesce(NC_CURP, '') NC_CURP , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICADO , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICAD , 
	EMP.CB_RFC 
from NOMINA  NO
join RPATRON on RPATRON.TB_CODIGO = NO.CB_PATRON 
join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
left outer join V_EMP_TIMB_SIMPLE EMP on EMP.CB_CODIGO = NO.CB_CODIGO 
left outer join NOM_CONCIL NC on NC.PE_YEAR = NO.PE_YEAR and NC.PE_TIPO = NO.PE_TIPO and NC.PE_NUMERO = NO.PE_NUMERO  and  NC.CB_CODIGO = NO.CB_CODIGO 

GO 

create view VWSDIAGNOS(
	FECHA,
	RELOJ,
	IP,
	DNS,
	HUELLAS,
	MEMORIA,
	CPU,
	LINUX,
	BITACORA)
as
	select DATEADD(dd, 0, DATEDIFF(dd, 0, WS_FECHA)) as FECHA, 
		CH_RELOJ as RELOJ,
		WS_DATOS.value('(/Diagnostico/IP)[1]', 'varchar(300)') as IP,
		WS_DATOS.value('(/Diagnostico/DNS)[1]', 'varchar(300)') as DNS,
		WS_DATOS.value('(/Diagnostico/FPFileCount)[1]', 'varchar(300)') as Huellas,
		WS_DATOS.value('(/Diagnostico/Memoria)[1]', 'varchar(300)') as Memoria,
		WS_DATOS.value('(/Diagnostico/CPU)[1]', 'varchar(300)') as CPU,
		WS_DATOS.value('(/Diagnostico/LinuxVersion)[1]', 'varchar(300)') as LinuxVersion,
		WS_DATOS.value('(/Diagnostico/Bitacora)[1]', 'varchar(900)') as Bitacora
	from #COMPARTE.dbo.WS_DIAGNOS
GO

CREATE VIEW V_TOT_TIM as
select RS_CODIGO, PE_YEAR, PE_TIPO , PE_NUMERO,  TOTAL TOT_EMPTRE, TIMBRADOS_TRESS TOT_TIMTRE,  CONCILIADOS TOT_CONCIL , APLICADOS TOT_APLICA, 
	cast( case 
	when TOTAL > 0 then ( CONCILIADOS*1.0 / TOTAL*1.0 ) * 100.00 
		else 0.00 
	end  as decimal(15,2) )  TOT_PORCON,  
	TIMBRADOS_SAT TOT_TIMSAT
from ( 

select NC.RS_CODIGO, NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO, COUNT(*) TOTAL,  
SUM( case  
		when NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_TRESS, 

SUM( case  
		when NC_RESULT  = 0  and NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_SAT, 

SUM( case  
		when NC_RESULT = 0 then 1 
		else 0 
	 end 
	 ) CONCILIADOS, 
SUM( case  
		when NC_APLICAD = 'S' then 1 
		else 0 
	 end 
	 ) APLICADOS
from VTIMCONCIL NC
join PERIODO PE on  NC.PE_YEAR = PE.PE_YEAR and NC.PE_TIPO = PE.PE_TIPO and NC.PE_NUMERO = PE.PE_NUMERO 
group by NC.RS_CODIGO, NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO

) Conciliacion 


go
