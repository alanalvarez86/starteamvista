CREATE TRIGGER TU_NIVEL1 ON NIVEL1 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL1 = @NewCodigo WHERE PU_NIVEL1 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL1 = @NewCodigo WHERE PL_NIVEL1 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '1', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL1 ON NIVEL1 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '1', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL1 ON NIVEL1 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '1', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 41 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL2 ON NIVEL2 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL2 = @NewCodigo WHERE PU_NIVEL2 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL2 = @NewCodigo WHERE PL_NIVEL2 = @OldCodigo;
	UPDATE AREA		SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '2', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL2 ON NIVEL2 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '2', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL2 ON NIVEL2 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '2', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 42 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL3 ON NIVEL3 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL3 = @NewCodigo WHERE PU_NIVEL3 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL3 = @NewCodigo WHERE PL_NIVEL3 = @OldCodigo;
  UPDATE AREA		SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;

       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '3', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL3 ON NIVEL3 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '3', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL3 ON NIVEL3 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '3', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 43 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL4 ON NIVEL4 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL4 = @NewCodigo WHERE PU_NIVEL4 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL4 = @NewCodigo WHERE PL_NIVEL4 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '4', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL4 ON NIVEL4 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '4', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL4 ON NIVEL4 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '4', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 44 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL5 ON NIVEL5 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL5 = @NewCodigo WHERE PU_NIVEL5 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL5 = @NewCodigo WHERE PL_NIVEL5 = @OldCodigo;
 	UPDATE AREA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '5', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL5 ON NIVEL5 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '5', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL5 ON NIVEL5 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '5', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 45 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL6 ON NIVEL6 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL6 = @NewCodigo WHERE PU_NIVEL6 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL6 = @NewCodigo WHERE PL_NIVEL6 = @OldCodigo;
 	UPDATE AREA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '6', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL6 ON NIVEL6 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '6', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL6 ON NIVEL6 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '6', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 46 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL7 ON NIVEL7 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL7 = @NewCodigo WHERE PU_NIVEL7 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL7 = @NewCodigo WHERE PL_NIVEL7 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '7', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL7 ON NIVEL7 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '7', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL7 ON NIVEL7 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '7', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 47 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL8 ON NIVEL8 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL8 = @NewCodigo WHERE PU_NIVEL8 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL8 = @NewCodigo WHERE PL_NIVEL8 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '8', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL8 ON NIVEL8 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '8', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL8 ON NIVEL8 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '8', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 48 and CT_CODIGO in ( select TB_CODIGO from Deleted )

END
GO

CREATE TRIGGER TU_NIVEL9 ON NIVEL9 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL9 = @NewCodigo WHERE PU_NIVEL9 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL9 = @NewCodigo WHERE PL_NIVEL9 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
  END

  IF UPDATE( US_CODIGO )
  BEGIN
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario )
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '9', @TB_CODIGO, @NewUsuario
       END
  END
END
GO

CREATE TRIGGER TI_NIVEL9 ON NIVEL9 AFTER INSERT AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '9', @TB_CODIGO, @US_CODIGO
       END
END
GO

CREATE TRIGGER TD_NIVEL9 ON NIVEL9 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '9', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 49 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_EXTRA1 ON EXTRA1 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_G_TAB_1 = @NewCodigo WHERE CB_G_TAB_1 = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_EXTRA2 ON EXTRA2 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	     declare @NewCodigo Codigo, @OldCodigo Codigo;
	     select @NewCodigo = TB_CODIGO from   Inserted;
       select @OldCodigo = TB_CODIGO from   Deleted;

	     UPDATE COLABORA	SET CB_G_TAB_2 = @NewCodigo WHERE CB_G_TAB_2 = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_EXTRA3 ON EXTRA3 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_G_TAB_3 = @NewCodigo WHERE CB_G_TAB_3 = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_EXTRA4 ON EXTRA4 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_G_TAB_4 = @NewCodigo WHERE CB_G_TAB_4 = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_EXTRA5 ON EXTRA5 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_5 = @NewCodigo where ( COLABORA.CB_G_TAB_5 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA6 ON EXTRA6 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_6 = @NewCodigo where ( COLABORA.CB_G_TAB_6 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA7 ON EXTRA7 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_7 = @NewCodigo where ( COLABORA.CB_G_TAB_7 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA8 ON EXTRA8 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_8 = @NewCodigo where ( COLABORA.CB_G_TAB_8 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA9 ON EXTRA9 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB_9 = @NewCodigo where ( COLABORA.CB_G_TAB_9 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA10 ON EXTRA10 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	     declare @NewCodigo Codigo, @OldCodigo Codigo;
	     select @NewCodigo = TB_CODIGO from Inserted;
  select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB10 = @NewCodigo where ( COLABORA.CB_G_TAB10 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA11 ON EXTRA11 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB11 = @NewCodigo where ( COLABORA.CB_G_TAB11 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA12 ON EXTRA12 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB12 = @NewCodigo where ( COLABORA.CB_G_TAB12 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA13 ON EXTRA13 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB13 = @NewCodigo where ( COLABORA.CB_G_TAB13 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_EXTRA14 ON EXTRA14 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_G_TAB14 = @NewCodigo where ( COLABORA.CB_G_TAB14 = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_ENTIDAD ON ENTIDAD AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_ESTADO = @NewCodigo WHERE CB_ESTADO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_ESTUDIOS ON ESTUDIOS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_ESTUDIO = @NewCodigo WHERE CB_ESTUDIO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_TRANSPOR ON TRANSPOR AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_MED_TRA = @NewCodigo WHERE CB_MED_TRA = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_VIVE_EN ON VIVE_EN AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_VIVEEN = @NewCodigo WHERE CB_VIVEEN = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_VIVE_CON ON VIVE_CON AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_VIVECON = @NewCodigo WHERE CB_VIVECON = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_EDOCIVIL ON EDOCIVIL AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_EDO_CIV = @NewCodigo WHERE CB_EDO_CIV = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_CONTRATO ON CONTRATO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_CONTRAT = @NewCodigo WHERE CB_CONTRAT = @OldCodigo;
	UPDATE KARDEX	SET CB_CONTRAT = @NewCodigo WHERE CB_CONTRAT = @OldCodigo;
	UPDATE PUESTO	SET PU_CONTRAT = @NewCodigo WHERE PU_CONTRAT = @OldCodigo;
    UPDATE PLAZA    SET PL_CONTRAT = @NewCodigo WHERE PL_CONTRAT = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_MOT_BAJA ON MOT_BAJA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_MOT_BAJ = @NewCodigo WHERE CB_MOT_BAJ = @OldCodigo;
	UPDATE KARDEX	SET CB_MOT_BAJ = @NewCodigo WHERE CB_MOT_BAJ = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_OTRASPER ON OTRASPER AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE CLASIFI  SET TB_OP1 = @NewCodigo WHERE TB_OP1 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP2 = @NewCodigo WHERE TB_OP2 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP3 = @NewCodigo WHERE TB_OP3 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP4 = @NewCodigo WHERE TB_OP4 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP5 = @NewCodigo WHERE TB_OP5 = @OldCodigo;
	UPDATE KAR_FIJA SET KF_CODIGO = @NewCodigo WHERE KF_CODIGO = @OldCodigo;
	UPDATE PTOFIJAS SET PF_CODIGO = @NewCodigo WHERE PF_CODIGO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_INCIDEN ON INCIDEN AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE AUSENCIA  SET AU_TIPO = @NewCodigo WHERE AU_TIPO = @OldCodigo;
	UPDATE PERMISO   SET PM_TIPO = @NewCodigo WHERE PM_TIPO = @OldCodigo;
	UPDATE INCAPACI  SET IN_TIPO = @NewCodigo WHERE IN_TIPO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_TCURSO ON TCURSO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE CURSO  SET CU_CLASIFI = @NewCodigo WHERE CU_CLASIFI = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_CCURSO ON CCURSO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;

          select @NewCodigo = TB_CODIGO from Inserted;
          select @OldCodigo = TB_CODIGO from Deleted;

          update CURSO set CU_CLASE = @NewCodigo
          where ( CU_CLASE = @OldCodigo );
     END
END
GO

CREATE TRIGGER TU_HORARIO ON HORARIO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( HO_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = HO_CODIGO from   Inserted;
	select @OldCodigo = HO_CODIGO from   Deleted;

	UPDATE AUSENCIA SET HO_CODIGO = @NewCodigo WHERE HO_CODIGO = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_1 = @NewCodigo WHERE TU_HOR_1 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_2 = @NewCodigo WHERE TU_HOR_2 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_3 = @NewCodigo WHERE TU_HOR_3 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_4 = @NewCodigo WHERE TU_HOR_4 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_5 = @NewCodigo WHERE TU_HOR_5 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_6 = @NewCodigo WHERE TU_HOR_6 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_7 = @NewCodigo WHERE TU_HOR_7 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_FES = @NewCodigo WHERE TU_HOR_FES = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( QU_CODIGO )
  BEGIN
	declare @NewCodigo Condicion, @OldCodigo Condicion;

	select @NewCodigo = QU_CODIGO from   Inserted;
	select @OldCodigo = QU_CODIGO from   Deleted;

	IF ( @OldCodigo <> '' ) AND ( @NewCodigo <> '' )
	BEGIN
		UPDATE CONCEPTO SET CO_QUERY  = @NewCodigo WHERE CO_QUERY = @OldCodigo;
		UPDATE EVENTO   SET EV_QUERY  = @NewCodigo WHERE EV_QUERY = @OldCodigo;
		UPDATE REPORTE  SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
		UPDATE FOLIO    SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
		UPDATE CAFREGLA SET CL_QUERY  = @NewCodigo WHERE CL_QUERY = @OldCodigo;
		UPDATE ACCREGLA SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
	END
  END
END
GO

CREATE TRIGGER TU_MAESTRO ON MAESTRO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( MA_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = MA_CODIGO from   Inserted;
	select @OldCodigo = MA_CODIGO from   Deleted;

	UPDATE CURSO    SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
	UPDATE KARCURSO SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
	UPDATE SESION 	SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
        UPDATE RESERVA  SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_MOT_AUTO ON MOT_AUTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE CHECADAS SET CH_RELOJ = @NewCodigo
	WHERE CH_RELOJ = @OldCodigo AND CH_IGNORAR = 'S';
  END
END
GO

CREATE TRIGGER TD_PUESTO ON PUESTO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update PUESTO set PUESTO.PU_REPORTA = Deleted.PU_REPORTA
  from PUESTO, Deleted
  where ( PUESTO.PU_REPORTA = Deleted.PU_CODIGO );

  delete from CONF_TAB where CT_TABLA = 59 and CT_CODIGO in ( select PU_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_PUESTO ON PUESTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( PU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = PU_CODIGO from   Inserted;
        select @OldCodigo = PU_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE KARDEX	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE NOMINA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE KARCURSO	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE PUESTO	SET PU_REPORTA = @NewCodigo WHERE PU_REPORTA = @OldCodigo;
	UPDATE CLASITMP	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_TURNO ON TURNO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TU_CODIGO from   Inserted;
        select @OldCodigo = TU_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARDEX	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE NOMINA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARCURSO	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE PUESTO	SET PU_TURNO = @NewCodigo WHERE PU_TURNO = @OldCodigo;
	UPDATE CLASITMP	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
        UPDATE PLAZA    SET PL_TURNO = @NewCodigo WHERE PL_TURNO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_CLASIFI ON CLASIFI AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE PUESTO   SET PU_CLASIFI = @NewCodigo WHERE PU_CLASIFI = @OldCodigo;
	UPDATE COLABORA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE KARDEX	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE NOMINA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE AUSENCIA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE KARCURSO	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE CLASITMP	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
        UPDATE PLAZA    SET PL_CLASIFI = @NewCodigo where PL_CLASIFI = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_SSOCIAL ON SSOCIAL AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE KARDEX	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE VACACION	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE PUESTO	SET PU_TABLASS = @NewCodigo WHERE PU_TABLASS = @OldCodigo;
        UPDATE PLAZA    set PL_TABLASS = @NewCodigo WHERE PL_TABLASS = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_RPATRON ON RPATRON AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE LIQ_IMSS	SET LS_PATRON = @NewCodigo WHERE LS_PATRON = @OldCodigo;
	UPDATE COLABORA	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE KARDEX	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE NOMINA	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE PUESTO	SET PU_PATRON = @NewCodigo WHERE PU_PATRON = @OldCodigo;
    UPDATE PLAZA    SET PL_PATRON = @NewCodigo WHERE PL_PATRON = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_PRESTAMO ON PRESTAMO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( PR_REFEREN )
  BEGIN
      update MOVIMIEN
      set    MOVIMIEN.MO_REFEREN = INSERTED.PR_REFEREN
      from   MOVIMIEN, INSERTED, DELETED, TPRESTA
      where  MOVIMIEN.CB_CODIGO  = DELETED.CB_CODIGO and
             MOVIMIEN.CO_NUMERO  = TPRESTA.TB_CONCEPT and
             MOVIMIEN.MO_REFEREN = DELETED.PR_REFEREN and
             TPRESTA.TB_CODIGO   = DELETED.PR_TIPO and
             INSERTED.CB_CODIGO  = DELETED.CB_CODIGO
  END
END
GO

CREATE TRIGGER TU_TPARTE ON TPARTE AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TT_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TT_CODIGO from   Inserted;
	select @OldCodigo = TT_CODIGO from   Deleted;

	UPDATE PARTES	SET TT_CODIGO = @NewCodigo WHERE TT_CODIGO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_PARTES ON PARTES AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AR_CODIGO )
  BEGIN
	declare @NewCodigo CodigoParte, @OldCodigo CodigoParte;

	select @NewCodigo = AR_CODIGO from   Inserted;
    select @OldCodigo = AR_CODIGO from   Deleted;

	UPDATE WORKS SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
	UPDATE WORDER SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
	UPDATE CEDULA SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
	UPDATE CED_INSP SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
    UPDATE CEDSCRAP SET AR_CODIGO = @NewCodigo WHERE ( AR_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TOPERA ON TOPERA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TO_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TO_CODIGO from   Inserted;
	select @OldCodigo = TO_CODIGO from   Deleted;

	UPDATE OPERA	SET TO_CODIGO = @NewCodigo WHERE TO_CODIGO = @OldCodigo;
  END
END
GO

CREATE TRIGGER TU_OPERA ON OPERA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( OP_NUMBER )
  BEGIN
	declare @NewCodigo Operacion, @OldCodigo Operacion;

	select @NewCodigo = OP_NUMBER from   Inserted;
	select @OldCodigo = OP_NUMBER from   Deleted;

	UPDATE WORKS	SET OP_NUMBER = @NewCodigo WHERE OP_NUMBER = @OldCodigo;
	UPDATE STEPS	SET OP_NUMBER = @NewCodigo WHERE OP_NUMBER = @OldCodigo;
	UPDATE DEFSTEPS	SET OP_NUMBER = @NewCodigo WHERE OP_NUMBER = @OldCodigo;
	UPDATE CEDULA	SET OP_NUMBER = @NewCodigo WHERE OP_NUMBER = @OldCodigo;
	UPDATE AREA	SET TB_OPERA  = @NewCodigo WHERE TB_OPERA  = @OldCodigo;
   	update CEDSCRAP SET OP_NUMBER = @NewCodigo WHERE ( OP_NUMBER = @OldCodigo );    
  END
END
GO

CREATE TRIGGER TU_WORDER ON WORDER AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( WO_NUMBER )
  BEGIN
	declare @NewCodigo OrdenTrabajo, @OldCodigo OrdenTrabajo;

	select @NewCodigo = WO_NUMBER from   Inserted;
	select @OldCodigo = WO_NUMBER from   Deleted;

	UPDATE WORKS SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
	UPDATE CEDULA SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
	UPDATE CED_WORD	SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
	UPDATE CED_INSP	SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
  	update CEDSCRAP SET WO_NUMBER = @NewCodigo WHERE ( WO_NUMBER = @OldCodigo );
  END
END
GO

CREATE TRIGGER TD_TALLA ON TALLA AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;  
	update KAR_TOOL
	set    KAR_TOOL.KT_TALLA = ''
	from   KAR_TOOL, Deleted
	where  KAR_TOOL.KT_TALLA = Deleted.TB_CODIGO
END
GO

CREATE TRIGGER TU_TALLA ON TALLA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE KAR_TOOL	SET KT_TALLA = @NewCodigo WHERE KT_TALLA = @OldCodigo;
  END
END
GO

CREATE TRIGGER TD_MOT_TOOL ON MOT_TOOL AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
	 update KAR_TOOL set KAR_TOOL.KT_MOT_FIN = ''
	 from KAR_TOOL, Deleted
	 where ( KAR_TOOL.KT_MOT_FIN = Deleted.TB_CODIGO )
END
GO

CREATE TRIGGER TU_MOT_TOOL ON MOT_TOOL AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE KAR_TOOL	SET KT_MOT_FIN = @NewCodigo WHERE KT_MOT_FIN = @OldCodigo;
  END
END
GO

CREATE TRIGGER TD_COLABORA ON COLABORA AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  declare @OldCodigo NumeroEmpleado;
  select @OldCodigo = CB_CODIGO from Deleted;
  delete NOMINA	from NOMINA where ( NOMINA.CB_CODIGO = @OldCodigo ) and ( NOMINA.NO_STATUS <= 4 )
  if ( @OldCodigo > 0 )
  begin
       delete from EXPEDIEN where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
       update PLAZA set PLAZA.CB_CODIGO = 0 where ( PLAZA.CB_CODIGO = @OldCodigo );
  end
END
GO

CREATE TRIGGER TU_COLABORA ON COLABORA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  declare @NewCodigo NumeroEmpleado, @OldCodigo NumeroEmpleado ,@NewActivo Booleano,@ValidaPlaza Booleano;
  IF UPDATE( CB_CODIGO )
  BEGIN
	select @NewCodigo = CB_CODIGO from Inserted;
        select @OldCodigo = CB_CODIGO from Deleted;

	UPDATE NOMINA	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE LIQ_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE WORKS	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE CED_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
        if ( @OldCodigo > 0 )
        begin
             update EXPEDIEN set EXPEDIEN.CB_CODIGO = @NewCodigo
             where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
             update PLAZA set PLAZA.CB_CODIGO = @NewCodigo
             where ( PLAZA.CB_CODIGO = @OldCodigo );
        end
  END
  IF UPDATE( CB_PLAZA )
  BEGIN
       declare @NewPlaza FolioGrande;
       declare @OldPLaza FolioGrande;
       select @NewPlaza = CB_PLAZA from Inserted;
       select @OldPLaza = CB_PLAZA from Deleted;
       select @NewCodigo = CB_CODIGO from Inserted;
       update PLAZA set CB_CODIGO = 0 where ( PL_FOLIO = @OldPLaza ) and ( CB_CODIGO = @NewCodigo );
       update PLAZA set CB_CODIGO = @NewCodigo where ( PL_FOLIO = @NewPlaza );
  END

  IF UPDATE(CB_NIVEL1) OR
     UPDATE(CB_NIVEL2) OR
     UPDATE(CB_NIVEL3) OR
     UPDATE(CB_NIVEL4) OR
     UPDATE(CB_NIVEL5) OR
     UPDATE(CB_NIVEL6) OR
     UPDATE(CB_NIVEL7) OR
     UPDATE(CB_NIVEL8) OR
     UPDATE(CB_NIVEL9)
   BEGIN
   	DECLARE @NivelSupervisores Formula
	DECLARE @US_CODIGO Usuario

   	select @US_CODIGO = US_CODIGO from Inserted;
   	SET @NivelSupervisores =  DBO.SP_GET_NIVEL_SUPERVISORES()

     	IF  DBO.SP_TIENE_ENROLAMIENTO_SUPERVISORES() = 'S' AND @NivelSupervisores <> '' AND @US_CODIGO > 0
     	BEGIN
     		DECLARE @CB_NIVELSup Codigo
     		DECLARE @US_JEFE Usuario

     		SELECT  @CB_NIVELSup =
		CASE  @NivelSupervisores
			WHEN '1' THEN (SELECT TOP 1 CB_NIVEL1 FROM Inserted)
			WHEN '2' THEN (SELECT TOP 1 CB_NIVEL2 FROM Inserted)
			WHEN '3' THEN (SELECT TOP 1 CB_NIVEL3 FROM Inserted)
			WHEN '4' THEN (SELECT TOP 1 CB_NIVEL4 FROM Inserted)
			WHEN '5' THEN (SELECT TOP 1 CB_NIVEL5 FROM Inserted)
			WHEN '6' THEN (SELECT TOP 1 CB_NIVEL6 FROM Inserted)
			WHEN '7' THEN (SELECT TOP 1 CB_NIVEL7 FROM Inserted)
			WHEN '8' THEN (SELECT TOP 1 CB_NIVEL8 FROM Inserted)
			WHEN '9' THEN (SELECT TOP 1 CB_NIVEL9 FROM Inserted)
		END

     		IF ( @CB_NIVELSup <> '' )
     		BEGIN
	     		SET @US_JEFE = DBO.SP_GET_USUARIO_SUPERVISOR(@CB_NIVELSup, @NivelSupervisores)
			EXEC SP_AJUSTA_USUARIO_SUPERVISOR  @US_CODIGO, @US_JEFE
     		END
     	END
   END
   if Update(CB_ACTIVO)
   begin
        select @NewActivo = CB_ACTIVO from Inserted;
        select @NewCodigo = CB_CODIGO from Inserted;
		select @ValidaPlaza = GL_FORMULA from GLOBAL where GL_CODIGO = 128;
		if (@NewActivo = 'N' AND ( @ValidaPlaza != 'S' or @ValidaPlaza is Null))
		begin
			 update PLAZA set CB_CODIGO = 0 WHERE CB_CODIGO = @NewCodigo
		end
   end
END
GO

CREATE TRIGGER TU_AREA ON AREA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo CODIGO, @OldCodigo CODIGO;

	select @NewCodigo = TB_CODIGO from Inserted;
    select @OldCodigo = TB_CODIGO from Deleted;

    update SUP_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update KAR_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update COLABORA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
	update WORKS    set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update CEDULA   set CE_AREA = @NewCodigo where ( CE_AREA = @OldCodigo );
    update CED_INSP set CI_AREA = @NewCodigo where ( CI_AREA = @OldCodigo );
	update CEDSCRAP set CS_AREA = @NewCodigo where ( CS_AREA = @OldCodigo );
    update PLAZA    set PL_AREA = @NewCodigo where ( PL_AREA = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_CAUSACCI ON CAUSACCI AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
    declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
    update ACCIDENT set ACCIDENT.AX_CAUSA = @NewCodigo
    where ( ACCIDENT.AX_CAUSA = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_MOTACCI ON MOTACCI AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
 	declare @NewCodigo Codigo;
	declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update ACCIDENT set ACCIDENT.AX_MOTIVO = @NewCodigo
	where ( ACCIDENT.AX_MOTIVO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TACCIDEN ON TACCIDEN AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
    declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
    update ACCIDENT set ACCIDENT.AX_TIP_ACC = @NewCodigo
    where ( ACCIDENT.AX_TIP_ACC = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TESTUDIO ON TESTUDIO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
    declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
    update CONSULTA set CONSULTA.CN_EST_TIP = @NewCodigo
    where ( CONSULTA.CN_EST_TIP = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_DIAGNOST ON DIAGNOST AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( DA_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
    declare @OldCodigo Codigo;
	select @NewCodigo = DA_CODIGO from Inserted;
	select @OldCodigo = DA_CODIGO from Deleted;
    update CONSULTA set CONSULTA.DA_CODIGO = @NewCodigo
    where ( CONSULTA.DA_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_TCONSLTA ON TCONSLTA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
    declare @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
    update CONSULTA set CONSULTA.CN_TIPO = @NewCodigo
    where ( CONSULTA.CN_TIPO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TD_GRUPO_EX ON GRUPO_EX AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update CAMPO_EX set CAMPO_EX.GX_CODIGO = '' from CAMPO_EX, Deleted where ( CAMPO_EX.GX_CODIGO = Deleted.GX_CODIGO );
END
GO

CREATE TRIGGER TU_GRUPO_EX ON GRUPO_EX AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( GX_CODIGO )
  BEGIN
	declare @NewCodigo Codigo
    declare @OldCodigo Codigo;
	select @NewCodigo = GX_CODIGO from Inserted;
	select @OldCodigo = GX_CODIGO from Deleted;
    update CAMPO_EX set CAMPO_EX.GX_CODIGO = @NewCodigo
    where ( CAMPO_EX.GX_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_FAM_PTO ON FAM_PTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( FP_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = FP_CODIGO from Inserted;
    select @OldCodigo = FP_CODIGO from Deleted;

	update PUESTO set PUESTO.FP_CODIGO = @NewCodigo
    where ( PUESTO.FP_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_NIV_PTO ON NIV_PTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( NP_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = NP_CODIGO from Inserted;
    select @OldCodigo = NP_CODIGO from Deleted;

	update PUESTO set PUESTO.NP_CODIGO = @NewCodigo
    where ( PUESTO.NP_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_COMP_CAL ON COMP_CAL AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CM_CODIGO ) or UPDATE( CA_CODIGO )
  BEGIN
	declare @NewCM_CODIGO Codigo, @OldCM_CODIGO Codigo;
	declare @NewCA_CODIGO Codigo, @OldCA_CODIGO Codigo;

	select @NewCM_CODIGO = CM_CODIGO from Inserted;
    select @OldCM_CODIGO = CM_CODIGO from Deleted;
	select @NewCA_CODIGO = CA_CODIGO from Inserted;
    select @OldCA_CODIGO = CA_CODIGO from Deleted;

    update EMP_COMP set EMP_COMP.CM_CODIGO = @NewCM_CODIGO,
                        EMP_COMP.CA_CODIGO = @NewCA_CODIGO
    where ( EMP_COMP.CM_CODIGO = @OldCM_CODIGO ) and
          ( EMP_COMP.CA_CODIGO = @OldCA_CODIGO );
    update COMP_PTO set COMP_PTO.CM_CODIGO = @NewCM_CODIGO,
                        COMP_PTO.CA_CODIGO = @NewCA_CODIGO
    where ( COMP_PTO.CM_CODIGO = @OldCM_CODIGO ) and
          ( COMP_PTO.CA_CODIGO = @OldCA_CODIGO );
  END
END
GO

CREATE TRIGGER TD_SESION ON SESION AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  declare @OldFolio FolioGrande;
  select @OldFolio = SE_FOLIO from Deleted;
  delete from KARCURSO where ( KARCURSO.SE_FOLIO = @OldFolio );
END
GO

CREATE TRIGGER TU_SESION ON SESION AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;

     IF ( UPDATE( SE_FOLIO ) OR
        UPDATE( CU_CODIGO ) OR
        UPDATE( MA_CODIGO ) OR
        UPDATE( SE_REVISIO ) OR
        UPDATE( SE_FEC_INI ) OR
        UPDATE( SE_FEC_FIN ) OR
        UPDATE( SE_EST ) )

     BEGIN
          declare @OldFolio FolioGrande;
          declare @NewFolio FolioGrande;
          declare @NewCU_CODIGO Codigo;
          declare @NewMA_CODIGO Codigo;
          declare @NewSE_REVISIO NombreCampo;
          declare @NewSE_FEC_INI Fecha;
          declare @NewSE_FEC_FIN Fecha;
		  declare @NewSE_EST Codigo;

          select @OldFolio = SE_FOLIO from Deleted;
          select @NewFolio = SE_FOLIO,
                 @NewCU_CODIGO = CU_CODIGO,
                 @NewMA_CODIGO = MA_CODIGO,
                 @NewSE_REVISIO = SE_REVISIO,
                 @NewSE_FEC_INI = SE_FEC_INI,
                 @NewSE_FEC_FIN = SE_FEC_FIN,
                 @NewSE_EST = SE_EST
          from Inserted;

          update KARCURSO set
                 KARCURSO.SE_FOLIO = @NewFolio,
                 KARCURSO.CU_CODIGO = @NewCU_CODIGO,
                 KARCURSO.MA_CODIGO = @NewMA_CODIGO,
                 KARCURSO.KC_REVISIO = @NewSE_REVISIO,
                 KARCURSO.KC_FEC_TOM = @NewSE_FEC_INI,
                 KARCURSO.KC_FEC_FIN = @NewSE_FEC_FIN,
                 KARCURSO.KC_EST = @NewSE_EST

          where ( KARCURSO.SE_FOLIO = @OldFolio );

	  IF UPDATE( SE_FOLIO )
	  BEGIN
	       UPDATE RESERVA SET SE_FOLIO = @NewFolio WHERE SE_FOLIO = @OldFolio;
	  END

     END
END
GO

CREATE TRIGGER TU_CONCEPTO ON CONCEPTO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON
     IF UPDATE( CO_NUMERO )
     BEGIN
          declare @OldCO_NUMERO Concepto;
          declare @NewCO_NUMERO Concepto;
          select @OldCO_NUMERO = CO_NUMERO from Deleted;
          select @NewCO_NUMERO = CO_NUMERO from Inserted;

          update TAHORRO set TAHORRO.TB_CONCEPT = @NewCO_NUMERO
          where ( TAHORRO.TB_CONCEPT = @OldCO_NUMERO );
          update TAHORRO set TAHORRO.TB_RELATIV = @NewCO_NUMERO
          where ( TAHORRO.TB_RELATIV = @OldCO_NUMERO );
          update TPRESTA set TPRESTA.TB_CONCEPT = @NewCO_NUMERO
          where (  TPRESTA.TB_CONCEPT = @OldCO_NUMERO );
          update TPRESTA set TPRESTA.TB_RELATIV = @NewCO_NUMERO
          where ( TPRESTA.TB_RELATIV = @OldCO_NUMERO );
     END
END
GO

CREATE TRIGGER TU_ACCREGLA ON ACCREGLA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AE_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = AE_CODIGO from Inserted;
	select @OldCodigo = AE_CODIGO from Deleted;
    update ACCESLOG set AE_CODIGO = @NewCodigo where ( AE_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_COLONIA ON COLONIA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = TB_CODIGO from Inserted;
	select @OldCodigo = TB_CODIGO from Deleted;
	update COLABORA set CB_COD_COL = @NewCodigo where ( COLABORA.CB_COD_COL = @OldCodigo );
  END
END
GO

CREATE TRIGGER TD_GRUPO_AD ON GRUPO_AD AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update CAMPO_AD set CAMPO_AD.GX_CODIGO = '' from CAMPO_AD, Deleted where ( CAMPO_AD.GX_CODIGO = Deleted.GX_CODIGO );
END
GO

CREATE TRIGGER TU_GRUPO_AD ON GRUPO_AD AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( GX_CODIGO )
  BEGIN
       declare @NewCodigo Codigo
       declare @OldCodigo Codigo;
       select @NewCodigo = GX_CODIGO from Inserted;
       select @OldCodigo = GX_CODIGO from Deleted;
       update CAMPO_AD set CAMPO_AD.GX_CODIGO = @NewCodigo
       where ( CAMPO_AD.GX_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_AULA ON AULA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON
  IF UPDATE( AL_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = AL_CODIGO from   Inserted;
	select @OldCodigo = AL_CODIGO from   Deleted;

	UPDATE RESERVA	SET AL_CODIGO = @NewCodigo
        WHERE ( AL_CODIGO = @OldCodigo );
  END
END
GO

CREATE TRIGGER TU_CURSO ON CURSO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON
     IF UPDATE( CU_CODIGO )
     BEGIN
      declare @NewCodigo Codigo, @OldCodigo Codigo;

   select @NewCodigo = CU_CODIGO from   Inserted;
     select @OldCodigo = CU_CODIGO from   Deleted;

   update CURSOPRE set CP_CURSO = @NewCodigo
       where ( CP_CURSO = @OldCodigo );

       update KARCURSO set CU_CODIGO = @NewCodigo
       where ( CU_CODIGO = @OldCodigo );

       update SESION set CU_CODIGO = @NewCodigo
       where ( CU_CODIGO = @OldCodigo );
     END
END
GO

CREATE TRIGGER TU_PLAZA ON PLAZA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( PL_FOLIO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;
          select @NewCodigo = PL_FOLIO from Inserted;
          select @OldCodigo = PL_FOLIO from Deleted;
          update COLABORA set CB_PLAZA = @NewCodigo where ( CB_PLAZA = @OldCodigo );
          update KARDEX	set CB_PLAZA = @NewCodigo where ( CB_PLAZA = @OldCodigo );
          update NOMINA	set CB_PLAZA = @NewCodigo where ( CB_PLAZA = @OldCodigo );
     END
END
GO

CREATE TRIGGER TU_TCTAMOVS ON TCTAMOVS AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
	      declare @NewCodigo Codigo, @OldCodigo Codigo;
	      select @NewCodigo = TB_CODIGO from Inserted;
          select @OldCodigo = TB_CODIGO from Deleted;
	      update CTA_MOVS set CM_TIPO = @NewCodigo
          where ( CM_TIPO = @OldCodigo );
     END
END
GO

CREATE TRIGGER TU_TPRESTA ON TPRESTA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;
	      select @NewCodigo = TB_CODIGO from Inserted;
          select @OldCodigo = TB_CODIGO from Deleted;
	      update TAHORRO set TB_PRESTA = @NewCodigo
          where ( TB_PRESTA = @OldCodigo );
     END
END
GO

CREATE TRIGGER TD_TCOMPETE ON TCOMPETE AFTER DELETE AS
BEGIN
     SET NOCOUNT ON
     update COMPETEN set COMPETEN.TC_CODIGO = ''
     from COMPETEN, Deleted where ( COMPETEN.TC_CODIGO = Deleted.TC_CODIGO );
END
GO

CREATE TRIGGER TU_TCOMPETE ON TCOMPETE AFTER UPDATE AS
BEGIN
     IF UPDATE( TC_CODIGO )
     BEGIN
          SET NOCOUNT ON
          declare @NewCodigo Codigo, @OldCodigo Codigo;

          select @NewCodigo = TC_CODIGO from Inserted;
          select @OldCodigo = TC_CODIGO from Deleted;

          update COMPETEN set TC_CODIGO = @NewCodigo
          where ( TC_CODIGO = @OldCodigo );
     END
END
GO

CREATE TRIGGER TU_PROV_CAP ON PROV_CAP AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE ( PC_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;
          select @NewCodigo = PC_CODIGO from Inserted;
          select @OldCodigo = PC_CODIGO from Deleted;
          UPDATE MAESTRO SET PC_CODIGO = @NewCodigo WHERE PC_CODIGO = @OldCodigo;
     END
END
GO

CREATE TRIGGER TU_R_ENTIDAD ON R_ENTIDAD AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( EN_CODIGO )
     BEGIN
          declare @NewCodigo FolioChico, @OldCodigo FolioChico;
          select @NewCodigo = EN_CODIGO from Inserted;
          select @OldCodigo = EN_CODIGO from Deleted;

	      update R_ENTIDAD set EN_ALIAS = @NewCodigo where (EN_ALIAS = @OldCodigo)
          update R_ATRIBUTO set AT_ENTIDAD = @NewCodigo where ( AT_ENTIDAD = @OldCodigo );
          update R_RELACION set RL_ENTIDAD = @NewCodigo where ( RL_ENTIDAD = @OldCodigo );
          update R_DEFAULT set EN_CODIGO = @NewCodigo where (EN_CODIGO = @OldCodigo );
          update R_ORDEN set EN_CODIGO = @NewCodigo where (EN_CODIGO = @OldCodigo );
          update R_FILTRO set EN_CODIGO = @NewCodigo where (EN_CODIGO = @OldCodigo );
          UPDATE R_ENT_ACC SET EN_CODIGO = @NewCodigo WHERE EN_CODIGO = @OldCodigo;
		  UPDATE R_CLAS_ENT SET EN_CODIGO = @NewCodigo WHERE EN_CODIGO = @OldCodigo;
		  UPDATE R_MOD_ENT SET EN_CODIGO = @NewCodigo WHERE EN_CODIGO = @OldCodigo;
 	      update CAMPOREP set CR_TABLA = @NewCodigo where ( CR_TABLA = @OldCodigo );
     END
END
GO

CREATE TRIGGER TD_R_ENTIDAD ON R_ENTIDAD AFTER DELETE AS
BEGIN
  	SET NOCOUNT ON;
    if ( ( select COUNT(*) from DELETED, R_ENTIDAD  where ( DELETED.EN_CODIGO = R_ENTIDAD.EN_ALIAS ) ) > 0 )
  	begin
        	RAISERROR( 'No se puede borrar porque hay Entidades Alias que lo utilizan', 16, 1 )
  	end;

    if ( ( select COUNT(*) from DELETED, R_ATRIBUTO  where ( DELETED.EN_CODIGO = R_ATRIBUTO.AT_ENTIDAD ) ) > 0 )
  	begin
        	RAISERROR( 'No se puede borrar porque hay Atributos que lo utilizan', 16, 1 )
  	end;

    if ( ( select COUNT(*) from DELETED, R_RELACION  where ( DELETED.EN_CODIGO = R_RELACION.RL_ENTIDAD ) ) > 0 )
  	begin
        	RAISERROR( 'No se puede borrar porque hay Relaciones que lo utilizan', 16, 1 )
  	end;

    if ( ( select COUNT(*) from DELETED, R_DEFAULT  where ( DELETED.EN_CODIGO = R_DEFAULT.EN_CODIGO ) ) > 0 )
  	begin
        	RAISERROR( 'No se puede borrar porque hay Atributos Default que lo utilizan', 16, 1 )
  	end;

    if ( ( select COUNT(*) from DELETED, R_ORDEN  where ( DELETED.EN_CODIGO = R_ORDEN. EN_CODIGO ) ) > 0 )
  	begin
        	RAISERROR( 'No se puede borrar porque hay Criterios de Orden Default que lo utilizan', 16, 1 )
  	end;

    if ( ( select COUNT(*) from DELETED, R_FILTRO  where ( DELETED.EN_CODIGO = R_FILTRO. EN_CODIGO ) ) > 0 )
  	begin
        	RAISERROR( 'No se puede borrar porque hay Filtros Default que lo utilizan', 16, 1 )
  	end;
END
GO

CREATE TRIGGER TU_VFACTOR ON VFACTOR AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON
  IF UPDATE( VF_ORDEN )
  BEGIN
	declare @NewOrden	FolioChico;
	declare @OldOrden	FolioChico;
	declare @VL_CODIGO	FolioChico;

	select	@OldOrden = VF_ORDEN
	from	Deleted;

	select	@NewOrden = VF_ORDEN, @VL_CODIGO = VL_CODIGO
	from	Inserted;

	update	VSUBFACT
	set		VF_ORDEN = @NewOrden
	where	VL_CODIGO = @VL_CODIGO and VF_ORDEN = @OldOrden
  END
END
GO

CREATE TRIGGER TD_VFACTOR ON VFACTOR AFTER DELETE AS
BEGIN
	SET NOCOUNT ON
	declare @OldOrden	FolioChico;
	declare @VL_CODIGO	FolioChico;

	select	@OldOrden = VF_ORDEN, @VL_CODIGO = VL_CODIGO
	from	Deleted;


	if ( select COUNT(*) from VSUBFACT where VL_CODIGO = @VL_CODIGO and VF_ORDEN = @OldOrden ) > 0
	BEGIN
			ROLLBACK TRAN
			RAISERROR( 'No se puede borrar ese factor ya que tiene Sub-Factores', 16, 1 )
			RETURN
	END
END
GO

CREATE TRIGGER TU_R_LISTAVAL ON R_LISTAVAL AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE ( LV_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;
          select @NewCodigo = LV_CODIGO from Inserted;
          select @OldCodigo = LV_CODIGO from Deleted;
          UPDATE R_ATRIBUTO SET LV_CODIGO = @NewCodigo WHERE LV_CODIGO = @OldCodigo;
     END
END
GO

CREATE TRIGGER TI_KARCURSO ON KARCURSO AFTER INSERT AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;

     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );

	  /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_PTO from COLABORA where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);

	 /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ; En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end
     else
     begin
         set @FechaProgram =  ( select case when @FecTomada is null then @FechaAntig
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' and ( not( @NewKcRevisio = '' ) ) and ( (select Count(CH_REVISIO) FROM cur_rev cr where cr.CH_REVISIO = @NewKcRevisio ) > 0 ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ) )			    				
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
     end

     if (@FechaProgram is null)
     begin
          set @FechaProgram = @FechaAntig;
     end
      /* Se actualiza la fecha de programacion para el curso en cuestion Este ya no se puede reprogramar */ 
	update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
END

GO

CREATE TRIGGER TU_KARCURSO ON KARCURSO AFTER UPDATE AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;
declare @FechaRevision Fecha;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;
 
     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );
     
	 /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_PTO from colabora where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 
	  /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	  /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ;
		En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     select @FechaRevision = max(CH_FECHA) from dbo.CUR_REV where ( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ); 
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end;
    
     IF Update(CU_CODIGO)
	 begin
		  set @FechaProgram =  ( select case when @FecTomada is null then @FechaAntig
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) )
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
		
		  update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end;
	 if ( Update ( KC_REVISIO ) and @ReprogVersion = 'S'  )
     begin	
            If( (@FechaRevision is null) or (@FechaRevision = '12/30/1899') )
			begin 	
			       if (@ReprogDias > 0 )
				   begin
						Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom = @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
				   end
				   else
						set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
					
			end			
			else
			begin				
				set @FechaProgram = @FechaRevision;				
			end;
			
			update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end        
END

GO

CREATE TRIGGER TR_AUSENCIA ON AUSENCIA AFTER UPDATE,INSERT,DELETE AS
BEGIN
	 SET NOCOUNT ON
	 Declare @Aprobado int;
	 Declare @AprobadoU int
	 Declare @Empleado int;
	 Declare @AnioPeriodo int;
	 Declare @TipoPeriodo int;
	 Declare @NumPeriodo int;
	 Declare @EmpleadoU int;
	 Declare @AnioPeriodoU int;	 
	 Declare @TipoPeriodoU int;
	 Declare @NumPeriodoU int;
	 Declare @Bloqueo VARCHAR(1);
		
     if (  Not Update( CB_CODIGO )  )
     begin
		  select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289
		  if (@Bloqueo = 'S')
		  begin		  
			    select @Empleado = CB_CODIGO from Inserted;
				 
				select @NumPeriodo = PE_NUMERO from Inserted;
				select @TipoPeriodo = PE_TIPO from Inserted;
				select @AnioPeriodo = PE_YEAR from Inserted;
				 
				if ( @Empleado is null and @NumPeriodo is null )
				begin
					   select @Empleado = CB_CODIGO from Deleted;
					   select @NumPeriodo = PE_NUMERO from Deleted;
					   select @TipoPeriodo = PE_TIPO from Deleted;
					   select @AnioPeriodo = PE_YEAR from Deleted;	
				end	
				ELSE
				BEGIN
					   select @EmpleadoU = CB_CODIGO from Deleted;
					   select @NumPeriodoU = PE_NUMERO from Deleted;
					   select @TipoPeriodoU = PE_TIPO from Deleted;
					   select @AnioPeriodoU = PE_YEAR from Deleted;
				END

				 if ( (@NumPeriodo > 0 and @TipoPeriodo > 0 and @AnioPeriodo > 0 )OR (@NumPeriodoU > 0 and @TipoPeriodoU > 0 and @AnioPeriodoU > 0) )
				 begin
					  select @Aprobado = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @Empleado and N.PE_YEAR = @AnioPeriodo and N.PE_TIPO = @TipoPeriodo and N.PE_NUMERO = @NumPeriodo
					  select @AprobadoU = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @EmpleadoU and N.PE_YEAR = @AnioPeriodoU and N.PE_TIPO = @TipoPeriodoU and N.PE_NUMERO = @NumPeriodoU
								  
					  if ( @Aprobado > 0 or  @AprobadoU > 0 )
					  begin
							RAISERROR(' Tarjeta de Pre-N�mina Autorizada - No se pueden realizar cambios', 15, 2);
							ROLLBACK TRANSACTION;
					  end
				 end
		  end
	 end
END

GO

Create TRIGGER TR_CHECADAS ON CHECADAS AFTER UPDATE,INSERT,DELETE AS
BEGIN
	 SET NOCOUNT ON
	 Declare @Aprobado int;
	 Declare @Empleado int;
     Declare @FechaTarjeta DateTime;
	 Declare @AnioPeriodo int;
     Declare @TipoPeriodo int;
     Declare @NumPeriodo int;
	 Declare @Bloqueo CHAR(1);

	 if (  Not Update( CB_CODIGO )  )
     begin	 
		  select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289	 
		 if ( @Bloqueo = 'S' ) 
		 begin 
			 select @Empleado = CB_CODIGO from Inserted;

			 select @FechaTarjeta = AU_FECHA from Inserted;
			 
			 if ( ( @Empleado is Null ) and ( @FechaTarjeta is null ) )     
			 begin
				  select @Empleado = CB_CODIGO from Deleted;
				  select @FechaTarjeta = AU_FECHA from Deleted;
			 end
			 
			 select @AnioPeriodo = PE_YEAR,@TipoPeriodo = PE_TIPO,@NumPeriodo = PE_NUMERO from AUSENCIA A where A.CB_CODIGO = @Empleado and A.AU_FECHA = @FechaTarjeta;
			 if (@NumPeriodo > 0 and @TipoPeriodo > 0 and @AnioPeriodo > 0 )
			 begin
				   select @Aprobado = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @Empleado and N.PE_YEAR = @AnioPeriodo and N.PE_TIPO = @TipoPeriodo and N.PE_NUMERO = @NumPeriodo 
				   if ( @Aprobado > 0 )
				   begin
						RAISERROR(' Tarjeta de Pre-N�mina Autorizada - No se pueden realizar cambios', 15, 2);
						ROLLBACK TRANSACTION;
				   end
			 end    
		 end
	 end
END
GO

create trigger TR_CB_GP_COD on COLABORA after update as
begin
	set NOCOUNT on;
	declare @OldCodigo NumeroEmpleado;
	select @OldCodigo = CB_CODIGO from Deleted;

	if update( CB_GP_COD )
		update HUELLA set HU_SYNC = getdate() where CB_CODIGO = @OldCodigo
end

GO

CREATE TRIGGER TD_NOMINA_COSTEO ON NOMINA AFTER DELETE AS
BEGIN
  SET NOCOUNT ON
  DELETE	T_ProporcionCostos
  FROM	T_ProporcionCostos, DELETED
  WHERE	T_ProporcionCostos.NominaID = DELETED.LLAVE

  DELETE	T_ProporcionTotales
  FROM	T_ProporcionTotales, DELETED
  WHERE	T_ProporcionTotales.NominaID = DELETED.LLAVE

  DELETE	T_Costos
  FROM	T_Costos, DELETED
  WHERE	T_Costos.NominaID = DELETED.LLAVE

  DELETE	T_Costos_SubCta
  FROM	T_Costos_SubCta, DELETED
  WHERE	T_Costos_SubCta.NominaID = DELETED.LLAVE
END
GO

CREATE TRIGGER TU_PARIENTE ON PARIENTE AFTER UPDATE AS
BEGIN
    SET NOCOUNT ON;
    declare @NewFolio int;
    declare @OldFolio int;
    declare @NewRelacio int;
    declare @OldRelacio int;
    declare @Empleado int;

    select @NewFolio = PA_FOLIO,@NewRelacio = PA_RELACIO,@Empleado = CB_CODIGO from Inserted;

    select @OldFolio = PA_FOLIO,@OldRelacio = PA_RELACIO from Deleted;

    If ( Update (PA_RELACIO) ) and ( ( select count(*) from EMP_POLIZA where PA_FOLIO = @OldFolio and PA_RELACIO = @OldRelacio and CB_CODIGO = @Empleado and EP_ASEGURA = 1 ) > 0 )
    begin
       raiserror('No Se Puede Modificar El Tipo de Parentesco a Parientes con Seguros de Gastos M�dicos',16,1);
    end
    else
    begin
         If Update (PA_FOLIO)
          update EMP_POLIZA set PA_FOLIO = @NewFolio where PA_FOLIO = @OldFolio and PA_RELACIO = @OldRelacio and CB_CODIGO = @Empleado;
    end
END
GO

CREATE TRIGGER TD_PARIENTE ON PARIENTE AFTER DELETE AS
BEGIN
    SET NOCOUNT ON;
    declare @OldFolio int;
    declare @OldRelacio int;
    declare @Empleado int;

    select @OldFolio = PA_FOLIO,@OldRelacio = PA_RELACIO,@Empleado = CB_CODIGO from Deleted;

    if ( (select count(*) from EMP_POLIZA where PA_FOLIO = @OldFolio and PA_RELACIO = @OldRelacio and CB_CODIGO = @Empleado and EP_ASEGURA = 1 ) > 0 )
    begin
       raiserror('No Se Puede Borrar Parientes con Seguros de Gastos M�dicos',16,1);
    end
END
GO

create trigger TU_EXPEDIEN on EXPEDIEN after update as
begin
  set nocount on;
 if update( EX_TSANGRE ) or update( EX_ALERGIA )
 begin
	if @@nestlevel < 32
	begin
		update COLABORA set CB_TSANGRE = EX_TSANGRE, CB_ALERGIA = EX_ALERGIA
		from Inserted EX where  COLABORA.CB_CODIGO = EX.CB_CODIGO and EX.EX_TIPO = 0
	end
 end
end
GO

create trigger TU_COLABORA_MEDICOS
on COLABORA after update as
begin
  set nocount on;
 if update( CB_TSANGRE ) or update( CB_ALERGIA )
 begin
	if @@nestlevel < 32
	begin
		update EXPEDIEN set EX_TSANGRE = CB_TSANGRE, EX_ALERGIA = CB_ALERGIA
		from Inserted CB where  EXPEDIEN.CB_CODIGO = CB.CB_CODIGO and EXPEDIEN.EX_TIPO = 0
	end
 end
end
GO

CREATE TRIGGER TU_PUESTO_CONF ON PUESTO AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  /* Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( PU_NIVEL0 )
  BEGIN
		declare @PU_NIVEL0 Formula
		declare @PU_CODIGO Codigo
		select  @PU_NIVEL0 = PU_NIVEL0, @PU_CODIGO =  PU_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 59, @PU_CODIGO, @PU_NIVEL0
  END
END
GO

CREATE TRIGGER TU_TURNO_CONF ON TURNO AFTER UPDATE, INSERT AS
BEGIN
  SET NOCOUNT ON;
  /* Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TU_NIVEL0 )
  BEGIN
		declare @TU_NIVEL0 Formula
		declare @TU_CODIGO Codigo
		select  @TU_NIVEL0 = TU_NIVEL0, @TU_CODIGO =  TU_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 69, @TU_CODIGO, @TU_NIVEL0
  END
END
GO

CREATE TRIGGER TD_TURNO ON TURNO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 69 and CT_CODIGO in ( select TU_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_HORARIO_CONF ON HORARIO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  /* Esto es un proceso invidiual , no se realizar� por medio de update general */
  IF UPDATE( HO_NIVEL0 )
  BEGIN
		declare @HO_NIVEL0 Formula
		declare @HO_CODIGO Codigo
		select  @HO_NIVEL0 = HO_NIVEL0, @HO_CODIGO =  HO_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 27, @HO_CODIGO, @HO_NIVEL0
  END
END
GO

CREATE TRIGGER TD_HORARIO ON HORARIO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 27 and CT_CODIGO in ( select HO_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_CLASIFI_CONF ON CLASIFI AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 9, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_CLASIFI ON CLASIFI AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 9 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_SSOCIAL_CONF ON SSOCIAL AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 63, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_SSOCIAL ON SSOCIAL AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 63 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_CONTRATO_CONF ON CONTRATO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 12, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_CONTRATO ON CONTRATO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 12 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_NIVEL1_CONF ON NIVEL1 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 41, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL2_CONF ON NIVEL2 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 42, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL3_CONF ON NIVEL3 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 43, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL4_CONF ON NIVEL4 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 44, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL5_CONF ON NIVEL5 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;

  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 45, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL6_CONF ON NIVEL6 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 46, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL7_CONF ON NIVEL7 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 47, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL8_CONF ON NIVEL8 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 48, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_NIVEL9_CONF ON NIVEL9 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 49, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TU_RPATRON_CONF ON RPATRON AFTER UPDATE, INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 62, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_RPATRON ON RPATRON AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 62 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_COLONIA_CONF ON COLONIA AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 220, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_COLONIA ON COLONIA AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 220 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_ENTIDAD_CONF ON ENTIDAD AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 15, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_ENTIDAD ON ENTIDAD AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 15 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_TAHORRO_CONF ON TAHORRO AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 64, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_TAHORRO ON TAHORRO AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 64 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_TPRESTA_CONF ON TPRESTA AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 67, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

CREATE TRIGGER TD_TPRESTA ON TPRESTA AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 67 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

CREATE TRIGGER TU_TPERIODO_CONF ON TPERIODO AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TP_NIVEL0 )
  BEGIN
		declare @TP_NIVEL0 Formula
		declare @TP_CODIGO Codigo
		select  @TP_NIVEL0 = TP_NIVEL0, @TP_CODIGO = LTRIM(RTRIM( STR( TP_TIPO ) )) from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 129, @TP_CODIGO, @TP_NIVEL0
  END
END
GO

CREATE TRIGGER TD_TPERIODO ON TPERIODO AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 129 and CT_CODIGO in ( select LTRIM(RTRIM(STR(TP_TIPO))) from Deleted )
END
GO

CREATE TRIGGER TI_C_COMPETEN ON C_COMPETEN AFTER INSERT AS
BEGIN
SET NOCOUNT ON;

declare @Competencia Varchar(6);
declare @Fecha DateTime;

Select @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()));
select @Competencia = CC_CODIGO from Inserted;

insert into C_REV_COMP (CC_CODIGO,RC_REVISIO,RC_OBSERVA,US_CODIGO,RC_FEC_INI)
	values (@Competencia,'1','Agregada por Sistema',1,@Fecha);

END
GO

CREATE TRIGGER TI_C_PERFIL ON C_PERFIL AFTER INSERT AS
BEGIN
SET NOCOUNT ON;

declare @Perfil Varchar(6);
declare @Fecha DateTime;

Select @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()));
select @Perfil = CP_CODIGO from Inserted;

insert into C_REV_PERF (CP_CODIGO,RP_REVISIO,RP_OBSERVA,US_CODIGO,RP_FEC_INI)
	values (@Perfil,'1','Agregada por Sistema',1,@Fecha);

END
GO

CREATE TRIGGER TI_COLABORA ON COLABORA AFTER INSERT AS
BEGIN	
  SET NOCOUNT ON;
  declare @NewCodigo NumeroEmpleado;
  select @NewCodigo = CB_CODIGO FROM Inserted;
  
  IF EXISTS (SELECT cb_codigo FROM EMP_TRANSF WHERE CB_CODIGO = @NewCodigo)
  BEGIN
	ROLLBACK TRANSACTION;
	RAISERROR ('El n�mero de empleado: %d no puede reutilizarse', 16, 1, @NewCodigo);
  END;
  
END
GO

CREATE TRIGGER TU_FON_TON ON FON_TOT AFTER UPDATE 
AS 
BEGIN 
	SET NOCOUNT ON 

	update FON_TOT_RS 
	set FON_TOT_RS.FT_STATUS = ins.FT_STATUS 
	from inserted ins, FON_TOT_RS 
	where ins.FT_YEAR = FON_TOT_RS.FT_YEAR and ins.FT_MONTH = FON_TOT_RS.FT_MONTH 
	
END

GO 
