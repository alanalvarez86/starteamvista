/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

/******** TIPOS DE PERIODOS **********/
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('TPERIODO') AND name='TP_CLAS' )
	ALTER TABLE TPERIODO ADD TP_CLAS NominaTipo NULL 
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_ACTUALIZAR_CLASIFICACION_TPERIODO') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_ACTUALIZAR_CLASIFICACION_TPERIODO
GO

CREATE  PROCEDURE SP_ACTUALIZAR_CLASIFICACION_TPERIODO
AS
BEGIN
	SET NOCOUNT ON
	IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('TPERIODO') AND name='TP_CLAS' )
	BEGIN
		UPDATE TPERIODO SET TP_CLAS = case
			WHEN TP_TIPO IN ( 0,1,2,3,4,5 ) then TP_TIPO
			WHEN TP_TIPO IN ( 6,7 ) then 1
			WHEN TP_TIPO IN ( 8,9 ) then 3
			WHEN TP_TIPO IN ( 10,11 ) then 2
			ELSE
				''
			END
		WHERE ( TP_CLAS is null ) OR ( TP_CLAS = '' );
		ALTER TABLE TPERIODO ALTER COLUMN TP_CLAS NominaTipo NOT NULL
	END
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_ACTUALIZAR_CLASIFICACION_TPERIODO') AND type IN ( N'P', N'PC' ))
	execute SP_ACTUALIZAR_CLASIFICACION_TPERIODO
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_ACTUALIZAR_CLASIFICACION_TPERIODO') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_ACTUALIZAR_CLASIFICACION_TPERIODO
GO

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE id=OBJECT_ID('PERIODO'))
BEGIN	
	 IF NOT EXISTS (Select * from sys.foreign_keys WHERE parent_object_id = object_id('PERIODO') AND name='FK_PERIODO')
	 BEGIN
		  ALTER TABLE PERIODO ADD CONSTRAINT FK_PERIODO FOREIGN KEY (PE_TIPO) REFERENCES TPERIODO (TP_TIPO)
	 END
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_ACTUALIZAR_FILTROS_REPORTE') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_ACTUALIZAR_FILTROS_REPORTE
GO

CREATE  PROCEDURE SP_ACTUALIZAR_FILTROS_REPORTE(
	@NumeroTabla		Status,
	@TipoRelelacion		Status,
	@TipoFiltro			Status,
	@ListaFijaUpdate	Status	
)
AS
BEGIN
	SET NOCOUNT ON
	IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CAMPOREP') )
	BEGIN
		UPDATE CAMPOREP SET CR_OPER = @NumeroTabla, CR_CALC = @TipoRelelacion 
		WHERE CR_TIPO = @TipoFiltro AND CR_OPER = @ListaFijaUpdate
	END
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_ACTUALIZAR_FILTROS_REPORTE') AND type IN ( N'P', N'PC' ))
	EXEC SP_ACTUALIZAR_FILTROS_REPORTE 129, 1, 5, 14;
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_ACTUALIZAR_FILTROS_REPORTE') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_ACTUALIZAR_FILTROS_REPORTE
GO

IF EXISTS (select * from sysobjects where id = object_id(N'TF') and xtype in (N'FN', N'IF', N'TF')) 
	DROP FUNCTION TF
GO

create function TF( @Tabla SMALLINT, @Codigo SMALLINT ) returns VARCHAR( 50 )
as
begin
     declare @Descripcion Observaciones;
	 declare @DescripcionTipoPeriodo Observaciones;
	 if @Tabla = 14
	 begin	 		
		select @Descripcion = TP_DESCRIP from TPERIODO where
		( TP_TIPO = @Codigo );
		select TOP 1 @DescripcionTipoPeriodo = Items from dbo.FN_SPLIT( @Descripcion, '/' )	 
		return ISNULL( @DescripcionTipoPeriodo, '' );
	 end
     select @Descripcion = TF_DESCRIP from TFIJAS where
     ( TF_TABLA = @Tabla ) and
     ( TF_CODIGO = @Codigo );
     return ISNULL( @Descripcion, '???' );
end
GO

ALTER FUNCTION DBO.SP_VALOR_CONCEPTO(
 				@Concepto SMALLINT,
				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
				@Razonsocial CHAR(6),
				@TipoNomina SMALLINT,
				@ValidaReingreso CHAR(1)
				 )

RETURNS NUMERIC(15,2)
AS
BEGIN
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
	   DECLARE @M1216 NUMERIC(15,2);
	   DECLARE @TipoNom SMALLINT;
     DECLARE @Resultado NUMERIC(15,2);
     DECLARE @TP_Clasif SMALLINT;

select @Resultado = 0;

if ( @Concepto <> 1004 )
begin
	DECLARE TemporalCursorNom Cursor for

     select N.NO_PERCEPC,
            N.NO_DEDUCCI,
            N.NO_NETO,
            N.NO_X_ISPT,
            N.NO_PER_MEN,
            N.NO_X_MENS,
            N.NO_PER_CAL,
            N.NO_IMP_CAL,
            N.NO_X_CAL,
            N.NO_TOT_PRE,
			      N.NO_PREV_GR,
			      N.NO_PER_ISN,
            N.NO_TOTAL_1,
            N.NO_TOTAL_2,
            N.NO_TOTAL_3,
            N.NO_TOTAL_4,
            N.NO_TOTAL_5,
            N.NO_DIAS_FI,
            N.NO_DIAS_FJ,
            N.NO_DIAS_CG,
            N.NO_DIAS_SG,
            N.NO_DIAS_SU,
            N.NO_DIAS_OT,
            N.NO_DIAS_IN,
            N.NO_DIAS * 7 / 6,
            N.NO_DIAS_RE,
            N.NO_D_TURNO,
            N.NO_DIAS,
            N.NO_DIAS_AS,
            N.NO_DIAS_NT,
            N.NO_DIAS_FV,
            N.NO_DIAS_VA,
            N.NO_DIAS_AG,
            N.NO_DIAS_AJ,
            N.NO_DIAS_SS,
            N.NO_DIAS_EM,
            N.NO_DIAS_SI,
            N.NO_DIAS_BA,
            N.NO_DIAS_PV,
            N.NO_DIAS_VJ,
            N.NO_DIAS_VJ * 7 / 6,
            N.NO_DIAS_IH,
            N.NO_DIAS_ID,
            N.NO_DIAS_IT,
            N.NO_DIAS_DT,
            N.NO_DIAS_FS,
            N.NO_DIAS_FT,
            N.NO_JORNADA,
            N.NO_HORAS,
            N.NO_EXTRAS,
            N.NO_DOBLES,
            N.NO_TRIPLES,
            N.NO_ADICION,
            N.NO_TARDES,
            N.NO_HORA_PD,
            N.NO_HORA_CG,
            N.NO_HORA_SG,
            N.NO_FES_TRA,
            N.NO_DES_TRA,
            N.NO_VAC_TRA,
            N.NO_FES_PAG,
            N.NO_HORASNT,
            N.NO_HORAPDT,
		        N.PE_TIPO,
			      N.NO_PRE_EXT

		  from NOMINA N
		  LEFT OUTER JOIN PERIODO ON PERIODO.PE_YEAR = N.PE_YEAR AND PERIODO.PE_TIPO = N.PE_TIPO AND PERIODO.PE_NUMERO = N.PE_NUMERO
		  LEFT OUTER JOIN RPATRON ON RPATRON.TB_CODIGO = N.CB_PATRON
		  LEFT OUTER JOIN RSOCIAL ON RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
		  LEFT OUTER JOIN COLABORA ON COLABORA.CB_CODIGO = N.CB_CODIGO
		where
			( N.PE_YEAR = @Anio ) and
			( N.CB_CODIGO = @Empleado ) and
			( PERIODO.PE_STATUS = 6 ) and
			( PERIODO.PE_MES = @Mes ) and
			( ( @TipoNomina = -1 ) or ( N.PE_TIPO = @TipoNomina ) ) and
			( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
      		( ( @ValidaReingreso <> 'S' ) or ( COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN ) )

		open TemporalCursorNom
		Fetch next from TemporalCursorNom
		into
        @M1000,
        @M1001,
        @M1002,
        @M1003,
        @M1005,
        @M1006,
        @M1007,
        @M1008,
        @M1009,
        @M1010,
        @M1011,
		    @M1012,
		    @M1013,
		    @M1014,
		    @M1015,
		    @M1016,
		    @M1017,
        @M1100,
        @M1101,
        @M1102,
        @M1103,
        @M1104,
        @M1105,
        @M1106,
        @M1107,
        @M1108,
        @M1109,
        @M1110,
        @M1111,
        @M1112,
        @M1113,
        @M1114,
        @M1115,
        @M1116,
        @M1117,
        @M1118,
        @M1119,
        @M1120,
        @M1121,
        @M1122,
        @M1123,
        @M1124,
        @M1125,
        @M1126,
        @M1127,
        @M1128,
        @M1129,
        @M1200,
        @M1201,
        @M1202,
        @M1203,
        @M1204,
        @M1205,
        @M1206,
        @M1207,
        @M1208,
        @M1209,
        @M1210,
        @M1211,
        @M1212,
        @M1213,
        @M1214,
        @M1215,
        @TipoNom,
		@M1216

	WHILE @@FETCH_STATUS = 0
     begin

          if ( @Concepto = 1200 or @Concepto = 1109 ) and ( @M1200 <> 0 )
          begin
                Select @M1200 = @M1201 / @M1200;
          end
          if ( @Concepto = 1000 ) and (@M1000 <> 0)
            select @Resultado = @M1000 + @Resultado
          if ( @Concepto = 1001 ) and (@M1001 <> 0)
            select @Resultado = @M1001 + @Resultado
          if ( @Concepto = 1002 ) and (@M1002 <> 0)
            select @Resultado = @M1002 + @Resultado
          if ( @Concepto = 1003 ) and (@M1003 <> 0)
            select @Resultado = @M1003 + @Resultado
          if ( @Concepto = 1005 ) and (@M1005 <> 0)
            select @Resultado = @M1005 + @Resultado
          if ( @Concepto = 1006 ) and (@M1006 <> 0)
            select @Resultado = @M1006 + @Resultado
          if ( @Concepto = 1007 ) and (@M1007 <> 0)
            select @Resultado = @M1007 + @Resultado
          if ( @Concepto = 1008 ) and (@M1008 <> 0)
            select @Resultado = @M1008 + @Resultado
          if ( @Concepto = 1009 ) and (@M1009 <> 0)
            select @Resultado = @M1009 + @Resultado
          if ( @Concepto = 1010 ) and (@M1010 <> 0)
            select @Resultado = @M1010 + @Resultado
          if ( @Concepto = 1011 ) and (@M1011 <> 0)
            select @Resultado = @M1011 + @Resultado
          if ( @Concepto = 1012 ) and (@M1012 <> 0)
            select @Resultado = @M1012 + @Resultado
          if ( @Concepto = 1013 ) and (@M1013 <> 0)
            select @Resultado = @M1013 + @Resultado
          if ( @Concepto = 1014 ) and (@M1014 <> 0)
            select @Resultado = @M1014 + @Resultado
          if ( @Concepto = 1015 ) and (@M1015 <> 0)
            select @Resultado = @M1015 + @Resultado
          if ( @Concepto = 1016 ) and (@M1016 <> 0)
            select @Resultado = @M1016 + @Resultado
          if ( @Concepto = 1017 ) and (@M1017 <> 0)
            select @Resultado = @M1017 + @Resultado
          if ( @Concepto = 1100 ) and (@M1100 <> 0)
            select @Resultado = @M1100 + @Resultado
          if ( @Concepto = 1101 ) and (@M1101 <> 0)
            select @Resultado = @M1101 + @Resultado
          if ( @Concepto = 1102 ) and (@M1102 <> 0)
            select @Resultado = @M1102 + @Resultado
          if ( @Concepto = 1103 ) and (@M1103 <> 0)
            select @Resultado = @M1103 + @Resultado
          if ( @Concepto = 1104 ) and (@M1104 <> 0)
            select @Resultado = @M1104 + @Resultado
          if ( @Concepto = 1105 ) and (@M1105 <> 0)
            select @Resultado = @M1105 + @Resultado
          if ( @Concepto = 1106 ) and (@M1106 <> 0)
            select @Resultado = @M1106 + @Resultado
          if ( @Concepto = 1107 ) and (@M1107 <> 0)
            select @Resultado = @M1107 + @Resultado
          if ( @Concepto = 1108 ) and (@M1108 <> 0)
            select @Resultado = @M1108 + @Resultado
          if ( @Concepto = 1109 ) and (@M1109 <> 0)
		begin
			select @M1109 = @M1200 * @M1109;
               select @Resultado = @M1109 + @Resultado
		end
          if ( @Concepto = 1110 ) and (@M1110 <> 0)
            select @Resultado = @M1110 + @Resultado
          if ( @Concepto = 1111 ) and (@M1111 <> 0)
            select @Resultado = @M1111 + @Resultado
          if ( @Concepto = 1112 ) and (@M1112 <> 0)
            select @Resultado = @M1112 + @Resultado
          if ( @Concepto = 1113 ) and (@M1113 <> 0)
            select @Resultado = @M1113 + @Resultado
          if ( @Concepto = 1114 ) and (@M1114 <> 0)
            select @Resultado = @M1114 + @Resultado
          if ( @Concepto = 1115 ) and (@M1115 <> 0)
            select @Resultado = @M1115 + @Resultado
          if ( @Concepto = 1116 ) and (@M1116 <> 0)
            select @Resultado = @M1116 + @Resultado
          if ( @Concepto = 1117 ) and (@M1117 <> 0)
            select @Resultado = @M1117 + @Resultado
          if ( @Concepto = 1118 ) and (@M1118 <> 0)
            select @Resultado = @M1118 + @Resultado
          if ( @Concepto = 1119 ) and (@M1119 <> 0)
              select @Resultado = @M1119 + @Resultado
          if ( @Concepto = 1120 ) and (@M1120 <> 0)
            select @Resultado = @M1120 + @Resultado
          if ( @Concepto = 1121 ) and (@M1121 <> 0)
            select @Resultado = @M1121 + @Resultado
          if ( @Concepto = 1122 ) and (@M1122 <> 0)
            select @Resultado = @M1122 + @Resultado
          if ( @Concepto = 1123 ) and (@M1123 <> 0)
            select @Resultado = @M1123 + @Resultado
          if ( @Concepto = 1124 ) and (@M1124 <> 0)
            select @Resultado = @M1124 + @Resultado
          if ( @Concepto = 1125 ) and (@M1125 <> 0)
            select @Resultado = @M1125 + @Resultado
          if ( @Concepto = 1126 ) and (@M1126 <> 0)
            select @Resultado = @M1126 + @Resultado
          if ( @Concepto = 1127 ) and (@M1127 <> 0)
            select @Resultado = @M1127 + @Resultado
          if ( @Concepto = 1128 ) and (@M1128 <> 0)
            select @Resultado = @M1128 + @Resultado
          if ( @Concepto = 1129 ) and (@M1129 <> 0)
            select @Resultado = @M1129 + @Resultado

          if ( @Concepto = 1200 ) and (@M1200 <> 0 )
     	    begin
               select @TP_Clasif = TP_CLAS from TPERIODO where TP_TIPO = @TipoNom;
               if ( @TP_Clasif = 0 )
                 Select @M1200 = 8 * @M1200;
               else
               if ( @TP_Clasif = 1 )
                 Select @M1200 = 48 * @M1200;
               else
               if ( @TP_Clasif = 2 )
                 Select @M1200 = 96 * @M1200;
               else
               if ( @TP_Clasif = 3 )
                 Select @M1200 = 104 * @M1200;
               else
               if ( @TP_Clasif = 4 )
                 Select @M1200 = 208 * @M1200;
               else
               if ( @TP_Clasif = 5 )
                 Select @M1200 = 72 * @M1200;
               select @Resultado = @M1200 + @Resultado
          end
     		if ( @Concepto = 1201 and @M1201 <> 0 )
          select @Resultado = @M1201 + @Resultado
        if ( @Concepto = 1202 and @M1202 <> 0 )
          select @Resultado = @M1202 + @Resultado
        if ( @Concepto = 1203 and @M1203 <> 0 )
          select @Resultado = @M1203 + @Resultado
        if ( @Concepto = 1204 and @M1204 <> 0 )
          select @Resultado = @M1204 + @Resultado
        if ( @Concepto = 1205 and @M1205 <> 0 )
          select @Resultado = @M1205 + @Resultado
        if ( @Concepto = 1206 and @M1206 <> 0 )
          select @Resultado = @M1206 + @Resultado
        if ( @Concepto = 1207 and @M1207 <> 0 )
          select @Resultado = @M1207 + @Resultado
        if ( @Concepto = 1208 and @M1208 <> 0 )
          select @Resultado = @M1208 + @Resultado
        if ( @Concepto = 1209 and @M1209 <> 0 )
          select @Resultado = @M1209 + @Resultado
        if ( @Concepto = 1210 and @M1210 <> 0 )
          select @Resultado = @M1210 + @Resultado
        if ( @Concepto = 1211 and @M1211 <> 0 )
          select @Resultado = @M1211 + @Resultado
        if ( @Concepto = 1212 and @M1212 <> 0 )
          select @Resultado = @M1212 + @Resultado
        if ( @Concepto = 1213 and @M1213 <> 0 )
          select @Resultado = @M1213 + @Resultado
        if ( @Concepto = 1214 and @M1214 <> 0 )
          select @Resultado = @M1214 + @Resultado
        if ( @Concepto = 1215 and @M1215 <> 0 )
          select @Resultado = @M1215 + @Resultado
		if ( @Concepto = 1216 and @M1216 <> 0 )
          select @Resultado = @M1216 + @Resultado

			Fetch next from TemporalCursorNom
     	  into
			      @M1000,
            @M1001,
            @M1002,
            @M1003,
            @M1005,
            @M1006,
            @M1007,
            @M1008,
            @M1009,
            @M1010,
  	        @M1011,
            @M1012,
            @M1013,
            @M1014,
            @M1015,
            @M1016,
            @M1017,
            @M1100,
            @M1101,
            @M1102,
            @M1103,
            @M1104,
            @M1105,
            @M1106,
            @M1107,
            @M1108,
            @M1109,
            @M1110,
            @M1111,
            @M1112,
            @M1113,
            @M1114,
            @M1115,
            @M1116,
            @M1117,
            @M1118,
            @M1119,
            @M1120,
            @M1121,
            @M1122,
            @M1123,
            @M1124,
            @M1125,
            @M1126,
            @M1127,
            @M1128,
            @M1129,
            @M1200,
            @M1201,
            @M1202,
            @M1203,
            @M1204,
            @M1205,
            @M1206,
            @M1207,
            @M1208,
            @M1209,
            @M1210,
            @M1211,
            @M1212,
            @M1213,
            @M1214,
            @M1215,
		    @TipoNom,
            @M1216
	end
  CLOSE TemporalCursorNom
  DEALLOCATE TemporalCursorNom
end
else
begin
  	select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M
	left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
	left outer join NOMINA on NOMINA.PE_YEAR = M.PE_YEAR AND NOMINA.PE_TIPO = M.PE_TIPO AND NOMINA.PE_NUMERO = M.PE_NUMERO AND NOMINA.CB_CODIGO = M.CB_CODIGO
	left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
	left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
	left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
     left outer join COLABORA on COLABORA.CB_CODIGO = M.CB_CODIGO
     where
		( M.PE_YEAR = @Anio ) AND
		( M.CB_CODIGO = @Empleado ) AND
		( M.MO_ACTIVO = 'S' ) AND
		( PERIODO.PE_STATUS = 6 ) AND
		( PERIODO.PE_MES = @Mes ) AND
		( ( @TipoNomina = -1 ) or ( NOMINA.PE_TIPO = @TipoNomina ) ) and
		( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
          ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) ) and
          ( C.CO_TIPO = 1 ) and
		( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        select @Resultado = @M1004
end

  RETURN @Resultado

END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AFECTA_EMPLEADO') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE AFECTA_EMPLEADO
GO

CREATE  PROCEDURE AFECTA_EMPLEADO
    				@EMPLEADO INTEGER,
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@MES SMALLINT,
    				@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON

	 DECLARE @RS_CODIGO Codigo 

	 select @RS_CODIGO = RSOCIAL.RS_CODIGO from NOMINA 
	 join RPATRON on NOMINA.CB_PATRON = RPATRON.TB_CODIGO 
	 join RSOCIAL on RSOCIAL.RS_CODIGO  = RPATRON.RS_CODIGO 
	 where NOMINA.PE_YEAR = @ANIO and NOMINA.PE_TIPO = @TIPO and NOMINA.PE_NUMERO = @NUMERO and NOMINA.CB_CODIGO = @EMPLEADO 

	 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 

     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
     DECLARE @M1216 NUMERIC(15,2);
	 DECLARE @TP_Clasif SMALLINT;

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1012=N.NO_PER_ISN,
            @M1013=N.NO_TOTAL_1,
            @M1014=N.NO_TOTAL_2,
            @M1015=N.NO_TOTAL_3,
            @M1016=N.NO_TOTAL_4,
            @M1017=N.NO_TOTAL_5,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1124=N.NO_DIAS_IH,
            @M1125=N.NO_DIAS_ID,
            @M1126=N.NO_DIAS_IT,
            @M1127=N.NO_DIAS_DT,
            @M1128=N.NO_DIAS_FS,
            @M1129=N.NO_DIAS_FT,
            @M1200=N.NO_JORNADA,
            @M1201=N.NO_HORAS,
            @M1202=N.NO_EXTRAS,
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
            @M1216=N.NO_PRE_EXT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
		Select @M1200 = @M1201 / @M1200;
     Select @M1109 = @M1200 * @M1109;

     if ( @M1201 < 0 ) and ( @M1200 > 0 )
		set @M1200 = -@M1200;

     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000, @RS_CODIGO;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001, @RS_CODIGO;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002, @RS_CODIGO;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003, @RS_CODIGO;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005, @RS_CODIGO;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006, @RS_CODIGO;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007, @RS_CODIGO;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008, @RS_CODIGO;
     if ( @M1009 <> 0 )
       EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009, @RS_CODIGO;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010, @RS_CODIGO;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011, @RS_CODIGO;
  	 if ( @M1012 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1012, @Mes, @Factor, @M1012, @RS_CODIGO;
  	 if ( @M1013 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1013, @Mes, @Factor, @M1013, @RS_CODIGO;
  	 if ( @M1014 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1014, @Mes, @Factor, @M1014, @RS_CODIGO;
  	 if ( @M1015 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1015, @Mes, @Factor, @M1015, @RS_CODIGO;
  	 if ( @M1016 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1016, @Mes, @Factor, @M1016, @RS_CODIGO;
  	 if ( @M1017 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1017, @Mes, @Factor, @M1017, @RS_CODIGO;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100, @RS_CODIGO;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101, @RS_CODIGO;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102, @RS_CODIGO;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103, @RS_CODIGO;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104, @RS_CODIGO;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105, @RS_CODIGO;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106, @RS_CODIGO;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107, @RS_CODIGO;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108, @RS_CODIGO;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109, @RS_CODIGO;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110, @RS_CODIGO;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111, @RS_CODIGO;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112, @RS_CODIGO;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113, @RS_CODIGO;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114, @RS_CODIGO;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115, @RS_CODIGO;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116, @RS_CODIGO;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117, @RS_CODIGO;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118, @RS_CODIGO;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119, @RS_CODIGO;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120, @RS_CODIGO;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121, @RS_CODIGO;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122, @RS_CODIGO;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123, @RS_CODIGO;
     if ( @M1124 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1124, @Mes, @Factor, @M1124, @RS_CODIGO;
     if ( @M1125 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1125, @Mes, @Factor, @M1125, @RS_CODIGO;
     if ( @M1126 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1126, @Mes, @Factor, @M1126, @RS_CODIGO;
     if ( @M1127 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1127, @Mes, @Factor, @M1127, @RS_CODIGO;
     if ( @M1128 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1128, @Mes, @Factor, @M1128, @RS_CODIGO;
     if ( @M1129 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1129, @Mes, @Factor, @M1129, @RS_CODIGO;
     if ( @M1200 <> 0 )
     begin
          select @TP_Clasif = TP_CLAS from TPERIODO where TP_TIPO = @Tipo;

          if ( @TP_Clasif = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( @TP_Clasif = 1 )
             Select @M1200 = 48 * @M1200;
          else
          if ( @TP_Clasif = 2 )
             Select @M1200 = 96 * @M1200;
          else
          if ( @TP_Clasif = 3 )
             Select @M1200 = 104 * @M1200;
          else
          if ( @TP_Clasif = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @TP_Clasif = 5 )
             Select @M1200 = 72 * @M1200;

          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200, @RS_CODIGO;

     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201, @RS_CODIGO;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202, @RS_CODIGO;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203, @RS_CODIGO;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204, @RS_CODIGO;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205, @RS_CODIGO;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206, @RS_CODIGO;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207, @RS_CODIGO;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208, @RS_CODIGO;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209, @RS_CODIGO;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210, @RS_CODIGO;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211, @RS_CODIGO;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212, @RS_CODIGO;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213, @RS_CODIGO;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214, @RS_CODIGO;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215, @RS_CODIGO;
	 if ( @M1216 <> 0 )
	    EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216, @RS_CODIGO;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000, @RS_CODIGO;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004,  @RS_CODIGO;
END
GO

ALTER PROCEDURE SPP_MigracionOptimiza(@CambioMensuales Booleano)
AS
BEGIN
    SET NOCOUNT ON
    DECLARE @TP_Tipo NominaTipo;

    if ( @CambioMensuales = 'S')
    begin
        select @TP_Tipo = TP_TIPO from TPERIODO where TP_CLAS = 4 and TP_DESCRIP <> '';
        UPDATE COLABORA set CB_CREDENC = '', CB_ID_NUM = '',CB_NOMINA = @TP_Tipo;
    end
    else
    begin
        UPDATE COLABORA set CB_CREDENC = '', CB_ID_NUM = '';
    end

		insert into BIT_MIG
		values(getdate(), 'Actualizacion COLABORA CB_CREDENC, CB_ID_NUM','Cantidad de Registros Actualizados: ' + convert(varchar(10), @@Rowcount));

		EXECUTE SP_COMPACTA_KARDEX

 		update KARDEX set US_CODIGO = 0

		insert into BIT_MIG
		values(getdate(), 'Actualizacion KARDEX | US_CODIGO = 0','Cantidad de Registros Actualizados: ' + convert(varchar(10), @@Rowcount));

		delete from KAR_FIJA2
		insert into BIT_MIG
		values(getdate(), 'Eliminacion de KAR_FIJA2','Cantidad de Registros Eliminados: ' + convert(varchar(10), @@Rowcount));
END
GO

