/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

/* Opci�n de CALC_FON no debe ajustar a empleados con incapacidad (1/2) */
if exists (select * from dbo.sysobjects where id = object_id(N'FN_CALCULA_FON') and xtype in (N'FN', N'IF', N'TF')) 
       drop function FN_CALCULA_FON
go 

/* Opci�n de CALC_FON no debe ajustar a empleados con incapacidad (2/2) */
CREATE FUNCTION FN_CALCULA_FON
	(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, 
	@TipoAjuste NumeroEmpleado, @AustarIncluyendoIncapacitados Booleano, @IncapacidadDias NumeroEmpleado, @IncapacidadesMes NumeroEmpleado, @AjusteMesAnterior Booleano,
	@AnioPeriodo Anio, @MesPeriodoFon Mes, @NumPeriodo NominaNumero, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   RETENCION_PERIODO Decimal(15,2), 
	   MONTO_CEDULA   Decimal(15,2), 
	   MONTO_RETENIDO Decimal(15,2), 
       MONTO_ESPERADO Decimal(15,2), 
       AJUSTE Decimal(15,2), 
       MONTO_CONCEPTO Decimal(15,2) ,
       DEUDA_MA Decimal(15,2) ,
	   NUM_PERIODO_MES int,
	   POS_PERIODO_MES int,
	   MES_ANTERIOR    int, 
	   YEAR_ANTERIOR   int 
	)
as
begin 
		declare @RETENCION_PERIODO Pesos 
		declare @MONTO_CEDULA   Pesos 
		declare @MONTO_RETENIDO Pesos 
		declare @MONTO_ESPERADO Pesos 
		declare @AJUSTE Pesos 
		declare @MONTO_CONCEPTO Pesos 
		declare @DEUDA_MA Pesos 
		DECLARE @NUM_PERIODO_MES Empleados
		DECLARE @POS_PERIODO_MES Empleados
		DECLARE @MES_ANTERIOR Status
		DECLARE @YEAR_ANTERIOR Anio
		DECLARE @PE_FEC_INI Fecha
		DECLARE @PE_FEC_FIN Fecha
   	   
		SET @AJUSTE = 0;		

		-- OBTENER INCAPACIDADES.
		IF @AustarIncluyendoIncapacitados = 'S' OR @TipoAjuste = 0
		BEGIN
			SET @IncapacidadesMes = 0
			SET @IncapacidadDias = 0
		END
		ELSE IF @AustarIncluyendoIncapacitados = 'N' AND @TipoAjuste > 0
		BEGIN
			-- OBTENER LOS D�AS DE INCAPACIDAD DE LOS PER�ODOS ANTERIORES			
			IF @IncapacidadDias < 0
			BEGIN
				SELECT @IncapacidadDias = NO_DIAS_IN FROM NOMINA WHERE CB_CODIGO = @CB_CODIGO
					AND PE_YEAR = @AnioPeriodo AND PE_NUMERO = @NumPeriodo AND PE_TIPO = @TipoPeriodo
			END

			IF @IncapacidadesMes < 0
			BEGIN
				SELECT @PE_FEC_INI = (MIN (PE_FEC_INI)), @PE_FEC_FIN =  (MAX (PE_FEC_FIN)) FROM PERIODO
					WHERE PE_NUMERO IN (SELECT PE_NUMERO FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo))
						AND PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo
			
				SELECT @IncapacidadesMes = COALESCE (SUM (IN_DIAS), 0) FROM INCAPACI WHERE CB_CODIGO = @CB_CODIGO					
					AND ((IN_FEC_INI >= @PE_FEC_INI AND IN_FEC_INI <= @PE_FEC_FIN ) OR (IN_FEC_FIN <= @PE_FEC_FIN AND IN_FEC_FIN > @PE_FEC_INI ))
			END				
		END
		-- ====================================================================================================================================

		-- OBTENER N�MERO DE PERIODO Y LA CANTIDAD DE PERIODOS DEL MES.
		SELECT @POS_PERIODO_MES = PE_POS_FON FROM FN_FONACOT_PERIODOS (@AnioPeriodo, @MesPeriodoFon, @TipoPeriodo)
		WHERE PE_NUMERO = @NumPeriodo
		-- Cantidad de periodos Fonacot.
		SELECT @NUM_PERIODO_MES = COUNT(PE_NUMERO) FROM PERIODO
		WHERE PE_YEAR = @AnioPeriodo AND PE_MES_FON = @MesPeriodoFon AND PE_TIPO = @TipoPeriodo

		SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
		set @MONTO_RETENIDO = coalesce( @MONTO_RETENIDO, 0.00 )


		if (@MesPeriodoFon = 1 )
		begin
			set @MES_ANTERIOR = 12
			set @YEAR_ANTERIOR = @AnioPeriodo - 1;
		end
		else
		begin
			set @MES_ANTERIOR = @MesPeriodoFon - 1;
			set @YEAR_ANTERIOR = @AnioPeriodo;
		end		

		-- SI EL TIPO DE AJUSTE ES  1 (DURANTE EL MES, ES DECIR, SUAVIZADO) � 2 (AJUSTAR CADA PERIODO) Y SE PIDE AJUSTAR EL MES ANTERIOR,
		-- OBTENER AJUSTE DEL MES ANTERIOR.
		IF @AjusteMesAnterior = 'S'
		BEGIN
			SELECT @DEUDA_MA = (PF_PAGO-PF_NOMINA) FROM VCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @YEAR_ANTERIOR AND PF_MES = @MES_ANTERIOR  AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
			
			SET @DEUDA_MA = COALESCE (@DEUDA_MA, 0.000);
		END
		ELSE
			SET @DEUDA_MA = 0.000;
	    
		-- AJUSTE.
		-- 1. SUAVIZADO, ES DECIR, DURANTE EL MES.
		-- 2. AJUSTA CADA PER�ODO.
		-- 3. AJUSTA AL FINAL DE MES (�LTIMO PER�ODO).
	   
		-- OBTENER LA RETENCI�N POR PERIODO.
		IF @NUM_PERIODO_MES != 0
		BEGIN
			SELECT @RETENCION_PERIODO = ((PF_PAGO+@DEUDA_MA)/@NUM_PERIODO_MES) FROM PCED_FON
				WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodoFon AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ;
			SET @RETENCION_PERIODO = coalesce( @RETENCION_PERIODO , 0.000 );
		END
		ELSE
		BEGIN
			SET @RETENCION_PERIODO = 0.0;
		END
		
		-- OBTENER MONTO ESPERADO.		
		IF ((@TipoAjuste = 1 OR @TipoAjuste = 2) OR (@TipoAjuste = 3 AND @POS_PERIODO_MES = @NUM_PERIODO_MES)) 			
			AND (@IncapacidadDias = 0) AND (@IncapacidadesMes = 0 )
		BEGIN
			SET @MONTO_ESPERADO = (@POS_PERIODO_MES - 1)*@RETENCION_PERIODO;			

			-- OBTENER EL AJUSTE,
			-- ES DECIR, LA DIFERENCIA ENTRE LO QUE DEBER�A LLEVAR DESCONTADO HASTA EL MOMENTO (@MONTO_ESPERADO)
			-- Y LO QUE EN REALIDAD LLEVA (VCED_FON.PF_NOMINA)	

			-- AJUSTE SUAVIZADO, ES DECIR, DURANTE LOS PER�ODOS QUE RESTAN EN EL MES.
			IF (@TipoAjuste = 1)
			BEGIN
				SET @AJUSTE = (@MONTO_ESPERADO - @MONTO_RETENIDO ) / ((@NUM_PERIODO_MES-@POS_PERIODO_MES)+1)
			END
			-- AJUSTE CADA PER�ODO O AL FINAL DEL MES.
			ELSE IF (@TipoAjuste = 2 OR @TipoAjuste = 3)
			BEGIN
				SET @AJUSTE = (@MONTO_ESPERADO - @MONTO_RETENIDO ) 
			END

			SET @AJUSTE = COALESCE (@AJUSTE, 0.000);
		END
		ELSE
		BEGIN
			SET @MONTO_ESPERADO = 0.000;
			SET @AJUSTE = 0.000;
		END	   
	   
		SET @MONTO_CONCEPTO = @RETENCION_PERIODO + @AJUSTE		 

		IF @POS_PERIODO_MES = @NUM_PERIODO_MES
		BEGIN
			IF ABS (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO)) < 1
			BEGIN
				SET @MONTO_CONCEPTO = @MONTO_CONCEPTO + (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO))				
			END
		END

		insert into @Valores (
				RETENCION_PERIODO, 
				MONTO_CEDULA, 
				MONTO_RETENIDO, 
				MONTO_ESPERADO, 
				AJUSTE, 
				MONTO_CONCEPTO ,
				DEUDA_MA, 
				NUM_PERIODO_MES,
				POS_PERIODO_MES,
				MES_ANTERIOR ,  
				YEAR_ANTERIOR
		) values ( 
				@RETENCION_PERIODO, 
				@MONTO_CEDULA, 
				@MONTO_RETENIDO, 
				@MONTO_ESPERADO, 
				@AJUSTE, 
				@MONTO_CONCEPTO ,
				@DEUDA_MA, 
				@NUM_PERIODO_MES,
				@POS_PERIODO_MES,
				@MES_ANTERIOR ,  
				@YEAR_ANTERIOR
		)

		RETURN 
end
GO


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ELIMINAR_CEDULA') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_ELIMINAR_CEDULA
GO

CREATE procedure SP_FONACOT_ELIMINAR_CEDULA (@YEAR SMALLINT, @MES SMALLINT, @RAZON_SOCIAL CHAR(6), @US_CODIGO SMALLINT, @PROC_ID INTEGER, @BI_NUMERO INTEGER) 
as
begin
	 set nocount on
	 DECLARE @NOMINAS_AFECTADAS INTEGER;
	 DECLARE @BI_TEXTO Observaciones
	 DECLARE @BI_DATA NVARCHAR(MAX)
	 DECLARE @BI_FEC_MOV Fecha
	 DECLARE @PR_TIPO Codigo1
	 DECLARE @CONTADOR_ELIMINADO INTEGER
	 DECLARE @EMPLEADO int;
	 DECLARE @REFERENCIA VARCHAR(8)


	 select @PR_TIPO = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	 set @PR_TIPO = COALESCE( @PR_TIPO, '' ) 

	 SELECT @NOMINAS_AFECTADAS = dbo.FN_FONACOT_CONTEO_CEDULA_AFECTADA(@YEAR, @MES, @RAZON_SOCIAL)  
	 
	 if @NOMINAS_AFECTADAS = 0
	 begin
		  SELECT @BI_FEC_MOV = GETDATE();
		  		  
		  Declare Temporal CURSOR for
 		  SELECT PR.CB_CODIGO, PR.PR_REFEREN from  PRESTAMO PR JOIN COLABORA C ON PR.CB_CODIGO = C.CB_CODIGO
			WHERE PR.CB_CODIGO IN (SELECT CB_CODIGO FROM PCED_FON WHERE PF_YEAR = @YEAR AND PF_MES =@MES AND  PR_TIPO = @PR_TIPO) AND PR.PR_TIPO = @PR_TIPO
			AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '') 

		  OPEN TEMPORAL
		     fetch NEXT FROM Temporal 
	         into  @EMPLEADO, @REFERENCIA
		  WHILE @@FETCH_STATUS = 0 
		  BEGIN
				if ( dbo.PrestamoFonacot_PuedeCancelar( @YEAR, @MES, @EMPLEADO, @PR_TIPO, @REFERENCIA) = 'S' ) 
				begin 
					UPDATE PRESTAMO SET PR_STATUS = 1, PR_FON_MOT = 4 WHERE CB_CODIGO = @EMPLEADO AND PR_REFEREN = @REFERENCIA AND PR_TIPO = @PR_TIPO 
					SET @BI_TEXTO = 'Se cancel� pr�stamo Fonacot con referencia '  + @REFERENCIA; 
					exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, @EMPLEADO, '', 62, @BI_FEC_MOV;
				end; 

				fetch NEXT FROM Temporal 
 					into  @EMPLEADO, @REFERENCIA
  		   END;

   	       close Temporal
    	   deallocate Temporal

		   DELETE PF FROM PCED_FON PF JOIN COLABORA C ON PF.CB_CODIGO = C.CB_CODIGO
		  WHERE PF.PF_YEAR = @YEAR AND PF.PF_MES = @MES AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '') 
			   AND PF.PR_TIPO = @PR_TIPO; 

		  SELECT @CONTADOR_ELIMINADO = @@ROWCOUNT; 

		  -- Eliminar registro de encabezado de c�dula (tabla PCED_ENC)
		  DELETE FROM PCED_ENC WHERE PE_YEAR = @YEAR AND PE_MES = @MES -- AND RS_CODIGO = @RAZON_SOCIAL
		  
		  if @CONTADOR_ELIMINADO = 0 
			 SET @BI_TEXTO = 'No se eliminaron registros de c�dula Fonacot.';
		  else
			  SET @BI_TEXTO = 'Se eliminaron: ' + cast (@CONTADOR_ELIMINADO as varchar) + ' registro(s) de c�dula Fonacot.'; 		  
		  
		  exec SP_INSERTAR_BITACORA @US_CODIGO, @PROC_ID, 0, @BI_NUMERO, @BI_TEXTO, 0, '', 62, @BI_FEC_MOV;
		
	 end;
end
GO 