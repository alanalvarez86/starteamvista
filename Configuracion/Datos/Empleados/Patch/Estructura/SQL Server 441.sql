-- 1. CREAR EMP_TRANSF SI NO EXISTE
IF NOT EXISTS (SELECT * FROM sysobjects where id = object_id(N'EMP_TRANSF'))
	CREATE TABLE EMP_TRANSF(
		CB_CODIGO NumeroEmpleado NOT NULL,
		CB_ACTIVO Booleano NOT NULL,
		CB_APE_MAT Descripcion NOT NULL,
		CB_APE_PAT Descripcion NOT NULL,
		CB_AUTOSAL Booleano NOT NULL,
		CB_AR_FEC Fecha NOT NULL,
		CB_AR_HOR Hora NOT NULL,
		CB_BAN_ELE Descripcion NOT NULL,
		CB_CARRERA DescLarga NOT NULL,
		CB_CHECA Booleano NOT NULL,
		CB_CIUDAD Descripcion NOT NULL,
		CB_CLASIFI Codigo NOT NULL,
		CB_CODPOST Referencia NOT NULL,
		CB_CONTRAT Codigo1 NOT NULL,
		CB_CREDENC Codigo1 NOT NULL,
		CB_CURP Descripcion NOT NULL,
		CB_CALLE Observaciones NOT NULL,
		CB_COLONIA Descripcion NOT NULL,
		CB_EDO_CIV Codigo1 NOT NULL,
		CB_FEC_RES Fecha NOT NULL,
		CB_EST_HOR Descripcion NOT NULL,
		CB_EST_HOY Booleano NOT NULL,
		CB_ESTADO Codigo2 NOT NULL,
		CB_ESTUDIO Codigo2 NOT NULL,
		CB_EVALUA DiasFrac NOT NULL,
		CB_EXPERIE Titulo NOT NULL,
		CB_FEC_ANT Fecha NOT NULL,
		CB_FEC_BAJ Fecha NOT NULL,
		CB_FEC_BSS Fecha NOT NULL,
		CB_FEC_CON Fecha NOT NULL,
		CB_FEC_ING Fecha NOT NULL,
		CB_FEC_INT Fecha NOT NULL,
		CB_FEC_NAC Fecha NOT NULL,
		CB_FEC_REV Fecha NOT NULL,
		CB_FEC_VAC Fecha NOT NULL,
		CB_G_FEC_1 Fecha NOT NULL,
		CB_G_FEC_2 Fecha NOT NULL,
		CB_G_FEC_3 Fecha NOT NULL,
		CB_G_FEC_4 Fecha NOT NULL,
		CB_G_FEC_5 Fecha NOT NULL,
		CB_G_FEC_6 Fecha NOT NULL,
		CB_G_FEC_7 Fecha NOT NULL,
		CB_G_FEC_8 Fecha NOT NULL,
		CB_G_LOG_1 Booleano NOT NULL,
		CB_G_LOG_2 Booleano NOT NULL,
		CB_G_LOG_3 Booleano NOT NULL,
		CB_G_LOG_4 Booleano NOT NULL,
		CB_G_LOG_5 Booleano NOT NULL,
		CB_G_LOG_6 Booleano NOT NULL,
		CB_G_LOG_7 Booleano NOT NULL,
		CB_G_LOG_8 Booleano NOT NULL,
		CB_G_NUM_1 Pesos NOT NULL,
		CB_G_NUM_2 Pesos NOT NULL,
		CB_G_NUM_3 Pesos NOT NULL,
		CB_G_NUM_4 Pesos NOT NULL,
		CB_G_NUM_5 Pesos NOT NULL,
		CB_G_NUM_6 Pesos NOT NULL,
		CB_G_NUM_7 Pesos NOT NULL,
		CB_G_NUM_8 Pesos NOT NULL,
		CB_G_NUM_9 Pesos NOT NULL,
		CB_G_NUM10 Pesos NOT NULL,
		CB_G_NUM11 Pesos NOT NULL,
		CB_G_NUM12 Pesos NOT NULL,
		CB_G_NUM13 Pesos NOT NULL,
		CB_G_NUM14 Pesos NOT NULL,
		CB_G_NUM15 Pesos NOT NULL,
		CB_G_NUM16 Pesos NOT NULL,
		CB_G_NUM17 Pesos NOT NULL,
		CB_G_NUM18 Pesos NOT NULL,
		CB_G_TAB_1 Codigo NOT NULL,
		CB_G_TAB_2 Codigo NOT NULL,
		CB_G_TAB_3 Codigo NOT NULL,
		CB_G_TAB_4 Codigo NOT NULL,
		CB_G_TAB_5 Codigo NOT NULL,
		CB_G_TAB_6 Codigo NOT NULL,
		CB_G_TAB_7 Codigo NOT NULL,
		CB_G_TAB_8 Codigo NOT NULL,
		CB_G_TAB_9 Codigo NOT NULL,
		CB_G_TAB10 Codigo NOT NULL,
		CB_G_TAB11 Codigo NOT NULL,
		CB_G_TAB12 Codigo NOT NULL,
		CB_G_TAB13 Codigo NOT NULL,
		CB_G_TAB14 Codigo NOT NULL,
		CB_G_TEX_1 Descripcion NOT NULL,
		CB_G_TEX_2 Descripcion NOT NULL,
		CB_G_TEX_3 Descripcion NOT NULL,
		CB_G_TEX_4 Descripcion NOT NULL,
		CB_G_TEX_5 Descripcion NOT NULL,
		CB_G_TEX_6 Descripcion NOT NULL,
		CB_G_TEX_7 Descripcion NOT NULL,
		CB_G_TEX_8 Descripcion NOT NULL,
		CB_G_TEX_9 Descripcion NOT NULL,
		CB_G_TEX10 Descripcion NOT NULL,
		CB_G_TEX11 Descripcion NOT NULL,
		CB_G_TEX12 Descripcion NOT NULL,
		CB_G_TEX13 Descripcion NOT NULL,
		CB_G_TEX14 Descripcion NOT NULL,
		CB_G_TEX15 Descripcion NOT NULL,
		CB_G_TEX16 Descripcion NOT NULL,
		CB_G_TEX17 Descripcion NOT NULL,
		CB_G_TEX18 Descripcion NOT NULL,
		CB_G_TEX19 Descripcion NOT NULL,
		CB_HABLA Booleano NOT NULL,
		CB_TURNO Codigo NOT NULL,
		CB_IDIOMA Descripcion NOT NULL,
		CB_INFCRED Descripcion NOT NULL,
		CB_INFMANT Booleano NOT NULL,
		CB_INFTASA DescINFO NOT NULL,
		CB_LA_MAT Descripcion NOT NULL,
		CB_LAST_EV Fecha NOT NULL,
		CB_LUG_NAC Descripcion NOT NULL,
		CB_MAQUINA Titulo NOT NULL,
		CB_MED_TRA Codigo1 NOT NULL,
		CB_PASAPOR Booleano NOT NULL,
		CB_FEC_INC Fecha NOT NULL,
		CB_FEC_PER Fecha NOT NULL,
		CB_NOMYEAR Anio NOT NULL,
		CB_NACION Descripcion NOT NULL,
		CB_NOMTIPO NominaTipo NOT NULL,
		CB_NEXT_EV Fecha NOT NULL,
		CB_NOMNUME NominaNumero NOT NULL,
		CB_NOMBRES Descripcion NOT NULL,
		CB_PATRON RegPatronal NOT NULL,
		CB_PUESTO Codigo NOT NULL,
		CB_RFC Descripcion NOT NULL,
		CB_SAL_INT PesosDiario NOT NULL,
		CB_SALARIO PesosDiario NOT NULL,
		CB_SEGSOC Descripcion NOT NULL,
		CB_SEXO Codigo1 NOT NULL,
		CB_TABLASS Codigo1 NOT NULL,
		CB_TEL Descripcion NOT NULL,
		CB_VIVECON Codigo1 NOT NULL,
		CB_VIVEEN Codigo2 NOT NULL,
		CB_ZONA Referencia NOT NULL,
		CB_ZONA_GE ZonaGeo NOT NULL,
		CB_NIVEL1 Codigo NOT NULL,
		CB_NIVEL2 Codigo NOT NULL,
		CB_NIVEL3 Codigo NOT NULL,
		CB_NIVEL4 Codigo NOT NULL,
		CB_NIVEL5 Codigo NOT NULL,
		CB_NIVEL6 Codigo NOT NULL,
		CB_NIVEL7 Codigo NOT NULL,
		CB_INFTIPO Status NOT NULL,
		CB_NIVEL8 Codigo NOT NULL,
		CB_NIVEL9 Codigo NOT NULL,
		CB_DER_FEC Fecha NOT NULL,
		CB_DER_PAG DiasFrac NOT NULL,
		CB_V_PAGO DiasFrac NOT NULL,
		CB_DER_GOZ DiasFrac NOT NULL,
		CB_V_GOZO DiasFrac NOT NULL,
		CB_TIP_REV Codigo NOT NULL,
		CB_MOT_BAJ Codigo3 NOT NULL,
		CB_FEC_SAL Fecha NOT NULL,
		CB_INF_INI Fecha NOT NULL,
		CB_INF_OLD DescINFO NOT NULL,
		CB_OLD_SAL PesosDiario NOT NULL,
		CB_OLD_INT PesosDiario NOT NULL,
		CB_PRE_INT PesosDiario NOT NULL,
		CB_PER_VAR PesosDiario NOT NULL,
		CB_SAL_TOT PesosDiario NOT NULL,
		CB_TOT_GRA PesosDiario NOT NULL,
		CB_FAC_INT Tasa NOT NULL,
		CB_RANGO_S Tasa NOT NULL,
		CB_CLINICA Codigo3 NOT NULL,
		CB_NIVEL0 Codigo NOT NULL,
		CB_FEC_NIV Fecha NOT NULL,
		CB_FEC_PTO Fecha NOT NULL,
		CB_AREA Codigo NOT NULL,
		CB_FEC_TUR Fecha NOT NULL,
		CB_FEC_KAR Fecha NOT NULL,
		CB_PENSION Status NOT NULL,
		CB_CANDIDA FolioGrande NOT NULL,
		CB_ID_NUM Descripcion NOT NULL,
		CB_ENT_NAC Codigo2 NOT NULL,
		CB_COD_COL Codigo NOT NULL,
		CB_PASSWRD Passwd NOT NULL,
		CB_PLAZA FolioGrande NOT NULL,
		CB_FEC_PLA Fecha NOT NULL,
		CB_DER_PV DiasFrac NOT NULL,
		CB_V_PRIMA DiasFrac NOT NULL,
		CB_SUB_CTA Descripcion NOT NULL,
		CB_NETO Pesos NOT NULL,
		CB_E_MAIL Formula NOT NULL,
		CB_PORTAL Booleano NOT NULL,
		CB_DNN_OK Booleano NOT NULL,
		CB_USRNAME DescLarga NOT NULL,
		CB_NOMINA NominaTipo NOT NULL,
		CB_FEC_NOM Fecha NOT NULL,
		CB_RECONTR Booleano NOT NULL,
		CB_DISCAPA Booleano NOT NULL,
		CB_INDIGE Booleano NOT NULL,
		CB_FONACOT Descripcion NOT NULL,
		CB_EMPLEO Booleano NOT NULL,
		US_CODIGO Usuario NOT NULL,
		CB_FEC_COV Fecha NOT NULL,
		CB_G_TEX20 Descripcion NOT NULL,
		CB_G_TEX21 Descripcion NOT NULL,
		CB_G_TEX22 Descripcion NOT NULL,
		CB_G_TEX23 Descripcion NOT NULL,
		CB_G_TEX24 Descripcion NOT NULL,
		CB_INFDISM Booleano NOT NULL,
		CB_INFACT Booleano NOT NULL,
		CB_NUM_EXT NombreCampo NOT NULL,
		CB_NUM_INT NombreCampo NOT NULL,
		CB_INF_ANT Fecha NOT NULL,
		CB_TDISCAP Status NOT NULL,
		CB_ESCUELA DescLarga NOT NULL,
		CB_TESCUEL Status NOT NULL,
		CB_TITULO Status NOT NULL,
		CB_YTITULO Anio NOT NULL,
		CB_CTA_GAS Descripcion NOT NULL,
		CB_CTA_VAL Descripcion NOT NULL,
		CB_MUNICIP Codigo NOT NULL,
		CB_ID_BIO FolioGrande NOT NULL,
		CB_GP_COD Codigo NOT NULL,
		CB_TIE_PEN Booleano NOT NULL,
		PRETTYNAME  AS ((((CB_APE_PAT+' ')+CB_APE_MAT)+', ')+CB_NOMBRES),
		CB_TSANGRE Descripcion NOT NULL,
		CB_ALERGIA Formula NOT NULL,
		CB_BRG_ACT Booleano NOT NULL,
		CB_BRG_TIP Status NOT NULL,
		CB_BRG_ROL Booleano NOT NULL,
		CB_BRG_JEF Booleano NOT NULL,
		CB_BRG_CON Booleano NOT NULL,
		CB_BRG_PRA Booleano NOT NULL,
		CB_BRG_NOP Descripcion NOT NULL,
		CB_BANCO Codigo NOT NULL,
		CB_REGIMEN Status NOT NULL,
		LLAVE int IDENTITY(1,1) NOT NULL,
	 CONSTRAINT PK_EMP_TRANSF PRIMARY KEY CLUSTERED 
	(
		CB_CODIGO ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

GO

-- 2. ELIMINAR VISTA V_EMP_TIMB
IF EXISTS (SELECT * FROM sysobjects where id = object_id(N'V_EMP_TIMB') AND xtype in (N'V'))
	DROP VIEW V_EMP_TIMB
GO 

-- 3. CREAR VISTA V_EMP_TIMB
CREATE VIEW V_EMP_TIMB 
AS
SELECT 'COLABORA' AS TABLA, 
       CB_CODIGO       ,CB_ACTIVO       ,CB_APE_MAT       ,CB_APE_PAT       ,CB_AUTOSAL       ,CB_AR_FEC       ,CB_AR_HOR       ,
       CB_BAN_ELE       ,CB_CARRERA       ,CB_CHECA       ,CB_CIUDAD       ,CB_CLASIFI       ,CB_CODPOST       ,CB_CONTRAT       ,
       CB_CREDENC       ,CB_CURP       ,CB_CALLE       ,CB_COLONIA       ,CB_EDO_CIV       ,CB_FEC_RES       ,CB_EST_HOR       ,
       CB_EST_HOY       ,CB_ESTADO       ,CB_ESTUDIO       ,CB_EVALUA       ,CB_EXPERIE       ,CB_FEC_ANT       ,CB_FEC_BAJ       ,
       CB_FEC_BSS       ,CB_FEC_CON       ,CB_FEC_ING       ,CB_FEC_INT       ,CB_FEC_NAC       ,CB_FEC_REV       ,CB_FEC_VAC       ,
       CB_G_FEC_1       ,CB_G_FEC_2       ,CB_G_FEC_3       ,CB_G_FEC_4       ,CB_G_FEC_5       ,CB_G_FEC_6       ,CB_G_FEC_7       ,
       CB_G_FEC_8       ,CB_G_LOG_1       ,CB_G_LOG_2       ,CB_G_LOG_3       ,CB_G_LOG_4       ,CB_G_LOG_5       ,CB_G_LOG_6       ,
       CB_G_LOG_7       ,CB_G_LOG_8       ,CB_G_NUM_1       ,CB_G_NUM_2       ,CB_G_NUM_3       ,CB_G_NUM_4       ,CB_G_NUM_5       ,
       CB_G_NUM_6       ,CB_G_NUM_7       ,CB_G_NUM_8       ,CB_G_NUM_9       ,CB_G_NUM10       ,CB_G_NUM11       ,CB_G_NUM12       ,
       CB_G_NUM13       ,CB_G_NUM14       ,CB_G_NUM15       ,CB_G_NUM16       ,CB_G_NUM17       ,CB_G_NUM18       ,CB_G_TAB_1       ,
       CB_G_TAB_2       ,CB_G_TAB_3       ,CB_G_TAB_4       ,CB_G_TAB_5       ,CB_G_TAB_6       ,CB_G_TAB_7       ,CB_G_TAB_8       ,
       CB_G_TAB_9       ,CB_G_TAB10       ,CB_G_TAB11       ,CB_G_TAB12       ,CB_G_TAB13       ,CB_G_TAB14       ,CB_G_TEX_1       ,
       CB_G_TEX_2       ,CB_G_TEX_3       ,CB_G_TEX_4       ,CB_G_TEX_5       ,CB_G_TEX_6       ,CB_G_TEX_7       ,CB_G_TEX_8       ,
       CB_G_TEX_9       ,CB_G_TEX10       ,CB_G_TEX11       ,CB_G_TEX12       ,CB_G_TEX13       ,CB_G_TEX14       ,CB_G_TEX15       ,
       CB_G_TEX16       ,CB_G_TEX17       ,CB_G_TEX18       ,CB_G_TEX19       ,CB_HABLA       ,CB_TURNO       ,CB_IDIOMA       ,
       CB_INFCRED       ,CB_INFMANT       ,CB_INFTASA       ,CB_LA_MAT       ,CB_LAST_EV       ,CB_LUG_NAC       ,CB_MAQUINA       ,
       CB_MED_TRA       ,CB_PASAPOR       ,CB_FEC_INC       ,CB_FEC_PER       ,CB_NOMYEAR       ,CB_NACION       ,CB_NOMTIPO       ,
       CB_NEXT_EV       ,CB_NOMNUME       ,CB_NOMBRES       ,CB_PATRON       ,CB_PUESTO       ,CB_RFC       ,CB_SAL_INT       ,
       CB_SALARIO       ,CB_SEGSOC       ,CB_SEXO       ,CB_TABLASS       ,CB_TEL       ,CB_VIVECON       ,CB_VIVEEN       ,
       CB_ZONA       ,CB_ZONA_GE       ,CB_NIVEL1       ,CB_NIVEL2       ,CB_NIVEL3       ,CB_NIVEL4       ,CB_NIVEL5       ,
       CB_NIVEL6       ,CB_NIVEL7       ,CB_INFTIPO       ,CB_NIVEL8       ,CB_NIVEL9       ,CB_DER_FEC       ,CB_DER_PAG       ,
       CB_V_PAGO       ,CB_DER_GOZ       ,CB_V_GOZO       ,CB_TIP_REV       ,CB_MOT_BAJ       ,CB_FEC_SAL       ,CB_INF_INI       ,
       CB_INF_OLD       ,CB_OLD_SAL       ,CB_OLD_INT       ,CB_PRE_INT       ,CB_PER_VAR       ,CB_SAL_TOT       ,CB_TOT_GRA       ,
       CB_FAC_INT       ,CB_RANGO_S       ,CB_CLINICA       ,CB_NIVEL0       ,CB_FEC_NIV       ,CB_FEC_PTO       ,CB_AREA       ,
       CB_FEC_TUR       ,CB_FEC_KAR       ,CB_PENSION       ,CB_CANDIDA       ,CB_ID_NUM       ,CB_ENT_NAC       ,CB_COD_COL       ,
       CB_PLAZA       ,CB_FEC_PLA       ,CB_DER_PV       ,CB_V_PRIMA       ,CB_SUB_CTA       ,CB_NETO       ,CB_E_MAIL       ,
       CB_PORTAL       ,CB_DNN_OK       ,CB_USRNAME       ,CB_NOMINA       ,CB_FEC_NOM       ,CB_RECONTR       ,CB_DISCAPA       ,
       CB_INDIGE       ,CB_FONACOT       ,CB_EMPLEO       ,PRETTYNAME       ,CB_PASSWRD       ,US_CODIGO       ,LLAVE       ,
       CB_FEC_COV       ,CB_G_TEX20       ,CB_G_TEX21       ,CB_G_TEX22       ,CB_G_TEX23       ,CB_G_TEX24       ,CB_INFDISM       ,
       CB_INFACT       ,CB_NUM_EXT       ,CB_NUM_INT       ,CB_INF_ANT       ,CB_TDISCAP       ,CB_ESCUELA       ,CB_TESCUEL       ,
       CB_TITULO       ,CB_YTITULO       ,CB_CTA_GAS       ,CB_CTA_VAL       ,CB_MUNICIP       ,CB_ID_BIO       ,CB_GP_COD       ,
       CB_TIE_PEN       ,CB_TSANGRE       ,CB_ALERGIA       ,CB_BRG_ACT       ,CB_BRG_TIP       ,CB_BRG_ROL       ,CB_BRG_JEF       ,
       CB_BRG_CON       ,CB_BRG_PRA       ,CB_BRG_NOP       ,CB_BANCO       ,CB_REGIMEN   
FROM COLABORA
UNION ALL
SELECT 'EMP_TRANSF' AS TABLA, 
       CB_CODIGO       ,CB_ACTIVO       ,CB_APE_MAT       ,CB_APE_PAT       ,CB_AUTOSAL       ,CB_AR_FEC       ,CB_AR_HOR       ,
       CB_BAN_ELE       ,CB_CARRERA       ,CB_CHECA       ,CB_CIUDAD       ,CB_CLASIFI       ,CB_CODPOST       ,CB_CONTRAT       ,
       CB_CREDENC       ,CB_CURP       ,CB_CALLE       ,CB_COLONIA       ,CB_EDO_CIV       ,CB_FEC_RES       ,CB_EST_HOR       ,
       CB_EST_HOY       ,CB_ESTADO       ,CB_ESTUDIO       ,CB_EVALUA       ,CB_EXPERIE       ,CB_FEC_ANT       ,CB_FEC_BAJ       ,
       CB_FEC_BSS       ,CB_FEC_CON       ,CB_FEC_ING       ,CB_FEC_INT       ,CB_FEC_NAC       ,CB_FEC_REV       ,CB_FEC_VAC       ,
       CB_G_FEC_1       ,CB_G_FEC_2       ,CB_G_FEC_3       ,CB_G_FEC_4       ,CB_G_FEC_5       ,CB_G_FEC_6       ,CB_G_FEC_7       ,
       CB_G_FEC_8       ,CB_G_LOG_1       ,CB_G_LOG_2       ,CB_G_LOG_3       ,CB_G_LOG_4       ,CB_G_LOG_5       ,CB_G_LOG_6       ,
       CB_G_LOG_7       ,CB_G_LOG_8       ,CB_G_NUM_1       ,CB_G_NUM_2       ,CB_G_NUM_3       ,CB_G_NUM_4       ,CB_G_NUM_5       ,
       CB_G_NUM_6       ,CB_G_NUM_7       ,CB_G_NUM_8       ,CB_G_NUM_9       ,CB_G_NUM10       ,CB_G_NUM11       ,CB_G_NUM12       ,
       CB_G_NUM13       ,CB_G_NUM14       ,CB_G_NUM15       ,CB_G_NUM16       ,CB_G_NUM17       ,CB_G_NUM18       ,CB_G_TAB_1       ,
       CB_G_TAB_2       ,CB_G_TAB_3       ,CB_G_TAB_4       ,CB_G_TAB_5       ,CB_G_TAB_6       ,CB_G_TAB_7       ,CB_G_TAB_8       ,
       CB_G_TAB_9       ,CB_G_TAB10       ,CB_G_TAB11       ,CB_G_TAB12       ,CB_G_TAB13       ,CB_G_TAB14       ,CB_G_TEX_1       ,
       CB_G_TEX_2       ,CB_G_TEX_3       ,CB_G_TEX_4       ,CB_G_TEX_5       ,CB_G_TEX_6       ,CB_G_TEX_7       ,CB_G_TEX_8       ,
       CB_G_TEX_9       ,CB_G_TEX10       ,CB_G_TEX11       ,CB_G_TEX12       ,CB_G_TEX13       ,CB_G_TEX14       ,CB_G_TEX15       ,
       CB_G_TEX16       ,CB_G_TEX17       ,CB_G_TEX18       ,CB_G_TEX19       ,CB_HABLA       ,CB_TURNO       ,CB_IDIOMA       ,
       CB_INFCRED       ,CB_INFMANT       ,CB_INFTASA       ,CB_LA_MAT       ,CB_LAST_EV       ,CB_LUG_NAC       ,CB_MAQUINA       ,
       CB_MED_TRA       ,CB_PASAPOR       ,CB_FEC_INC       ,CB_FEC_PER       ,CB_NOMYEAR       ,CB_NACION       ,CB_NOMTIPO       ,
       CB_NEXT_EV       ,CB_NOMNUME       ,CB_NOMBRES       ,CB_PATRON       ,CB_PUESTO       ,CB_RFC       ,CB_SAL_INT       ,
       CB_SALARIO       ,CB_SEGSOC       ,CB_SEXO       ,CB_TABLASS       ,CB_TEL       ,CB_VIVECON       ,CB_VIVEEN       ,
       CB_ZONA       ,CB_ZONA_GE       ,CB_NIVEL1       ,CB_NIVEL2       ,CB_NIVEL3       ,CB_NIVEL4       ,CB_NIVEL5       ,
       CB_NIVEL6       ,CB_NIVEL7       ,CB_INFTIPO       ,CB_NIVEL8       ,CB_NIVEL9       ,CB_DER_FEC       ,CB_DER_PAG       ,
       CB_V_PAGO       ,CB_DER_GOZ       ,CB_V_GOZO       ,CB_TIP_REV       ,CB_MOT_BAJ       ,CB_FEC_SAL       ,CB_INF_INI       ,
       CB_INF_OLD       ,CB_OLD_SAL       ,CB_OLD_INT       ,CB_PRE_INT       ,CB_PER_VAR       ,CB_SAL_TOT       ,CB_TOT_GRA       ,
       CB_FAC_INT       ,CB_RANGO_S       ,CB_CLINICA       ,CB_NIVEL0       ,CB_FEC_NIV       ,CB_FEC_PTO       ,CB_AREA       ,
       CB_FEC_TUR       ,CB_FEC_KAR       ,CB_PENSION       ,CB_CANDIDA       ,CB_ID_NUM       ,CB_ENT_NAC       ,CB_COD_COL       ,
       CB_PLAZA       ,CB_FEC_PLA       ,CB_DER_PV       ,CB_V_PRIMA       ,CB_SUB_CTA       ,CB_NETO       ,CB_E_MAIL       ,
       CB_PORTAL       ,CB_DNN_OK       ,CB_USRNAME       ,CB_NOMINA       ,CB_FEC_NOM       ,CB_RECONTR       ,CB_DISCAPA       ,
       CB_INDIGE       ,CB_FONACOT       ,CB_EMPLEO       ,PRETTYNAME       ,CB_PASSWRD       ,US_CODIGO       ,LLAVE       ,
       CB_FEC_COV       ,CB_G_TEX20       ,CB_G_TEX21       ,CB_G_TEX22       ,CB_G_TEX23       ,CB_G_TEX24       ,CB_INFDISM       ,
       CB_INFACT       ,CB_NUM_EXT       ,CB_NUM_INT       ,CB_INF_ANT       ,CB_TDISCAP       ,CB_ESCUELA       ,CB_TESCUEL       ,
       CB_TITULO       ,CB_YTITULO       ,CB_CTA_GAS       ,CB_CTA_VAL       ,CB_MUNICIP       ,CB_ID_BIO       ,CB_GP_COD       ,
       CB_TIE_PEN       ,CB_TSANGRE       ,CB_ALERGIA       ,CB_BRG_ACT       ,CB_BRG_TIP       ,CB_BRG_ROL       ,CB_BRG_JEF       ,
       CB_BRG_CON       ,CB_BRG_PRA       ,CB_BRG_NOP       ,CB_BANCO       ,CB_REGIMEN   
FROM EMP_TRANSF
GO

-- 4. ELIMINAR TRIGGER TI_COLABORA SI EXISTE
IF EXISTS (SELECT * FROM sysobjects where id = object_id(N'TI_COLABORA') AND xtype in (N'TR'))
	DROP TRIGGER TI_COLABORA
GO 

-- 5. CREAR TRIGGER TI_COLABORA
CREATE TRIGGER TI_COLABORA ON COLABORA AFTER INSERT AS
BEGIN	
  SET NOCOUNT ON;
  declare @NewCodigo NumeroEmpleado;
  select @NewCodigo = CB_CODIGO FROM Inserted;
  
  IF EXISTS (SELECT cb_codigo FROM EMP_TRANSF WHERE CB_CODIGO = @NewCodigo)
  BEGIN
	ROLLBACK TRANSACTION;
	RAISERROR ('El n�mero de empleado: %d no puede reutilizarse', 16, 1, @NewCodigo);
  END;
  
END
GO

/* Patch 441: Modificar procedure SP_ACTUALIZAR_MOVIMIENTO_IDSE (1/2) */
if exists (select * from sysobjects where id = object_id(N'SP_ACTUALIZAR_MOVIMIENTO_IDSE') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
 drop procedure SP_ACTUALIZAR_MOVIMIENTO_IDSE
GO

/* Patch 441: Modificar procedure SP_ACTUALIZAR_MOVIMIENTO_IDSE (2/2) */
CREATE PROCEDURE SP_ACTUALIZAR_MOVIMIENTO_IDSE(
@TipoSUA    Status, 
@NSS        Descripcion, 
@Fecha            Fecha, 
@LoteIDSE   DescLarga,
@FechaIDSE  Fecha,
@US_CODIGO  Usuario,
@REG_PATRON DescLarga,
@CB_CODIGO  NumeroEmpleado output,
@RESPUESTA  Status output
) 
as
BEGIN 
      SET NOCOUNT ON 
      declare 
            @CB_TIPO Codigo;
      DECLARE
          @CB_PATRON NumeroPatronal;
      declare 
            @SSValido NumeroEmpleado;
            
      set @CB_CODIGO = 0
      set @Respuesta = -10
      
      SET @SSValido = ( SELECT Count(CB_CODIGO)  FROM COLABORA WHERE CB_SEGSOC = @NSS ) ;
      SET @SSValido = COALESCE ( @SSValido, 0 )

      IF ( @SSValido <= 0 )
      BEGIN
            SET @RESPUESTA = -10
      END
      ELSE
      BEGIN
            IF ( @TIPOSUA = 2 )
                  SET @CB_TIPO = 'BAJA'
            ELSE
            IF ( @TIPOSUA = 7 )
                  SET @CB_TIPO = 'CAMBIO'
            ELSE
            IF ( @TIPOSUA = 8 )
                  SET @CB_TIPO = 'ALTA'
            ELSE
                  SET @CB_TIPO = '';

            IF ( @CB_TIPO = '' )
            BEGIN
                  SET @RESPUESTA = -30
            END
            ELSE
            BEGIN
                  SELECT @CB_PATRON = TB_CODIGO FROM RPATRON WHERE TB_NUMREG = @REG_PATRON;
                  set @CB_CODIGO = 
						COALESCE (( SELECT TOP 1 CB.CB_CODIGO from COLABORA CB join KARDEX K on K.CB_CODIGO = CB.CB_CODIGO  where CB_SEGSOC = @NSS and  K.CB_PATRON = @CB_PATRON and K.CB_TIPO = @CB_TIPO and K.CB_FECHA_2 = @FECHA), 0)

                  IF ( SELECT COUNT(*) FROM KARDEX WHERE CB_TIPO = @CB_TIPO AND CB_CODIGO = @CB_CODIGO  AND CB_FECHA_2 =  @FECHA AND CB_PATRON = @CB_PATRON ) > 0
                  BEGIN
                        UPDATE KARDEX
                        SET
                             CB_LOT_IDS = @LOTEIDSE,
                              CB_FEC_IDS = @FECHAIDSE
                        WHERE
                             CB_TIPO = @CB_TIPO AND CB_CODIGO = @CB_CODIGO  AND CB_FECHA_2 =  @FECHA AND CB_PATRON = @CB_PATRON

                        SET @RESPUESTA = 0  
                  END
                  ELSE
                        SET  @RESPUESTA = 20 
            end
      end
END
GO

IF not EXISTS (select * from syscolumns where id=object_id('NOMINA') and name='NO_FACUUID')
alter table NOMINA add NO_FACUUID  DescLarga  null
go       
      
update NOMINA 
set NOMINA.NO_FACUUID  = '' 
from NOMINA NO 
join PERIODO PE on NO.PE_TIPO = PE.PE_TIPO and NO.PE_YEAR = PE.PE_YEAR and NO.PE_NUMERO = PE.PE_NUMERO
where NO.NO_FACUUID is null and PE.PE_FEC_PAG >= '2013-11-01'
go 

IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_NOMINA_NO_FACTURA') 
DROP INDEX IDX_NOMINA_NO_FACTURA ON NOMINA 
Go 

create index IDX_NOMINA_NO_FACTURA on NOMINA ( NO_FACTURA ASC ) 
go 

if exists (select * from sysobjects where id = object_id(N'FN_GetFacturasPendientes') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_GetFacturasPendientes
go

create function FN_GetFacturasPendientes( @Inicial Fecha , @Final Fecha ) 
returns table 
as
return ( 
select NO.NO_FACTURA from NOMINA NO 
join PERIODO PE on NO.PE_TIPO = PE.PE_TIPO and NO.PE_YEAR = PE.PE_YEAR and NO.PE_NUMERO = PE.PE_NUMERO
where PE.PE_FEC_PAG between @Inicial and @Final  and NO.NO_FACTURA > 0 and ( NO.NO_FACUUID = '' or NO.NO_FACUUID is null ) 
)
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_TIMBRAR_EMPLEADO') AND type IN ( N'P', N'PC' ))
	drop procedure SP_TIMBRAR_EMPLEADO
go
 
CREATE procedure SP_TIMBRAR_EMPLEADO( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero, @CB_CODIGO NumeroEmpleado, @CB_RFC Descripcion, @Status status, @Timbre Formula, @FolioTimbramex FolioGrande, @Usuario usuario, @FacturaUUID  DescLarga = '' )  
as  
begin  
 set nocount on  
 declare @StatusTimbrado status  
 set @StatusTimbrado = 2  
  
 if ( @Status = @StatusTimbrado )  
  update NOMINA set NO_TIMBRO = @Status, NO_SELLO = @Timbre , NO_FACTURA = @FolioTimbramex  , NO_FACUUID = @FacturaUUID
  where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
      and NO_STATUS = 6  
   else  
  update NOMINA set NO_TIMBRO = @Status, NO_SELLO = '',  NO_FACTURA = 0, NO_FACUUID = ''   
  where  CB_CODIGO = @CB_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO  
      and NO_STATUS = 6  
end  
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_ACTUALIZA_FACTURA_UUID') AND type IN ( N'P', N'PC' ))
	drop procedure SP_ACTUALIZA_FACTURA_UUID
go

create procedure SP_ACTUALIZA_FACTURA_UUID( @FacturaNo FolioGrande,  @FacturaUUID  DescLarga)  
as
begin 
       set nocount on 
       update NOMINA set NO_FACUUID = @FacturaUUID where NO_FACTURA = @FacturaNo
end
GO

