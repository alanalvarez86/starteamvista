/*****************************/
/* Creaci�n de campos nuevos */
/*****************************/

/* Para guardar valores usados en el c�lculo (% subsidio, etc.)*/
/* Patch 380 Seq: 1 Agregado */
alter table PERIODO add PE_LOG FORMULA
GO

/* Para guardar horario a usar en d�as Festivos*/
/* Patch 380 Seq: 3 Agregado */
alter table TURNO add TU_HOR_FES Codigo
GO

/* Para guardar CLASE de un Curso*/
/* Patch 380 Seq: 4 Agregado */
alter table CURSO add CU_CLASE Codigo
GO

/* Para guardar d�as equivalentes de vacaciones para d�as h�biles */
/* Patch 380 Seq: 7 Agregado */
alter table TURNO add TU_VACA_HA DiasFrac
GO

/* Para guardar d�as equivalentes de vacaciones para S�bados */
/* Patch 380 Seq: 8 Agregado */
alter table TURNO add TU_VACA_SA DiasFrac
GO

/* Para guardar d�as equivalentes de vacaciones para Descansos */
/* Patch 380 Seq: 9 Agregado */
alter table TURNO add TU_VACA_DE DiasFrac
GO

/* Para inicializar el valor de d�as equivalentes de vacaciones para Descansos */
/* Patch 380 Seq: 10 Agregado */
update TURNO set TU_VACA_DE = 0 where ( TU_VACA_DE is NULL )
GO

/* Para inicializar el valor de d�as equivalentes de vacaciones para S�bados */
/* Patch 380 Seq: 11 Agregado */
update TURNO set TU_VACA_SA = 0 where ( TU_VACA_SA is NULL )
GO

/* Para inicializar el valor de d�as equivalentes de vacaciones para D�as H�biles cuando la jornada es de 5 d�as*/
/* Patch 380 Seq: 12 Agregado */
update TURNO set TU_VACA_HA = 1.2 where
( TU_DIAS = 5 ) and
( ( TU_VACA_HA is NULL ) or ( ( TU_VACA_HA = 0 ) and ( TU_VACA_SA = 0 ) and ( TU_VACA_DE = 0 ) ) )
GO

/* Para inicializar el valor de d�as equivalentes de vacaciones para D�as H�biles cuando la jornada no es de 5 d�as*/
/* Patch 380 Seq: 13 Agregado */
update TURNO set TU_VACA_HA = 1 where
( TU_DIAS <> 5 ) and
( ( TU_VACA_HA is NULL ) or ( ( TU_VACA_HA = 0 ) and ( TU_VACA_SA = 0 ) and ( TU_VACA_DE = 0 ) ) )
GO

/* Puesto al que reporta */
/* Patch 380 Seq: 14 Agregado */
alter table PUESTO add PU_REPORTA Codigo
GO

/* Plazas autorizadas (Default=1) */
/* Patch 380 Seq: 15 Agregado */
alter table PUESTO add PU_PLAZAS Empleados
GO

/* Nivel de Confidencialidad */
/* Patch 380 Seq: 16 Agregado */
alter table PUESTO add PU_NIVEL0 Codigo
GO

/* Nivel 1 */
/* Patch 380 Seq: 17 Agregado */
alter table PUESTO add PU_NIVEL1 Codigo
GO

/* Nivel 2 */
/* Patch 380 Seq: 18 Agregado */
alter table PUESTO add PU_NIVEL2 Codigo
GO

/* Nivel 3 */
/* Patch 380 Seq: 19 Agregado */
alter table PUESTO add PU_NIVEL3 Codigo
GO

/* Nivel 4 */
/* Patch 380 Seq: 20 Agregado */
alter table PUESTO add PU_NIVEL4 Codigo
GO

/* Nivel 5 */
/* Patch 380 Seq: 21 Agregado */
alter table PUESTO add PU_NIVEL5 Codigo
GO

/* Nivel 6 */
/* Patch 380 Seq: 22 Agregado */
alter table PUESTO add PU_NIVEL6 Codigo
GO

/* Nivel 7 */
/* Patch 380 Seq: 23 Agregado */
alter table PUESTO add PU_NIVEL7 Codigo
GO

/* Nivel 8 */
/* Patch 380 Seq: 24 Agregado */
alter table PUESTO add PU_NIVEL8 Codigo
GO

/* Nivel 9 */
/* Patch 380 Seq: 25 Agregado */
alter table PUESTO add PU_NIVEL9 Codigo
GO

/* Turno */
/* Patch 380 Seq: 26 Agregado */
alter table PUESTO add PU_TURNO	Codigo
GO

/*  �Salario por tabulador? */
/* Patch 380 Seq: 27 Agregado */
alter table PUESTO add PU_AUTOSAL Booleano
GO

/* Tipo de Contrato */
/* Patch 380 Seq: 28 Agregado */
alter table PUESTO add PU_CONTRAT Codigo1
GO

/* Tabla de Prestaciones */
/* Patch 380 Seq: 29 Agregado */
alter table PUESTO add PU_TABLASS Codigo1
GO

/* Registro Patronal */
/* Patch 380 Seq: 30 Agregado */
alter table PUESTO add PU_PATRON RegPatronal
GO

/* Area de Labor */
/* Patch 380 Seq: 31 Agregado */
alter table PUESTO add PU_AREA Codigo
GO

/* Zona Geogr�fica */
/* Patch 380 Seq: 32 Agregado */
alter table PUESTO add PU_ZONA_GE ZonaGeo
GO

/* �Checa Tarjeta? */
/* Patch 380 Seq: 33 Agregado */
alter table PUESTO add PU_CHECA Booleano
GO

/* Salario Diario */
/* Patch 380 Seq: 34 Agregado */
alter table PUESTO add PU_SALARIO PesosDiario
GO

/* Promedio percepciones variables */
/* Patch 380 Seq: 35 Agregado */
alter table PUESTO add PU_PER_VAR PesosDiario
GO

/* Tipo de Puesto */
/* Patch 380 Seq: 36 Agregado */
alter table PUESTO add PU_TIPO Status
GO

/* �Puesto activo? (Permite contratar o cambiar a este) */
/* Patch 380 Seq: 37 Agregado */
alter table PUESTO add PU_ACTIVO Booleano
GO

/* Presupuesto de Costo 1 */
/* Patch 380 Seq: 38 Agregado */
alter table PUESTO add PU_COSTO1 PesosEmpresa
GO

/* Presupuesto de Costo 2 */
/* Patch 380 Seq: 39 Agregado */
alter table PUESTO add PU_COSTO2 PesosEmpresa
GO

/* Presupuesto de Costo 3 */
/* Patch 380 Seq: 40 Agregado */
alter table PUESTO add PU_COSTO3 PesosEmpresa
GO

/* Nivel de Profundidad en Arbol */
/* Patch 380 Seq: 41 Agregado */
alter table PUESTO add PU_LEVEL	FolioChico
GO

/* Indice alterno por Puesto sobre COLABORA */
/* Patch 380 Seq: 42 Agregado */
create index XIE1COLABORA on COLABORA( CB_PUESTO )
GO

/* Inicialiar PUESTO.PU_ACTIVO */
/* Patch 380 Seq: 50 Agregado */
update PUESTO set PUESTO.PU_ACTIVO = 'S' where ( PUESTO.PU_ACTIVO is NULL );
GO

/* Inicialiar columnas nuevas de PUESTO */
/* Patch 380 Seq: 51 Agregado */
update PUESTO set
PUESTO.PU_REPORTA = '',
PUESTO.PU_PLAZAS = 0,
PUESTO.PU_NIVEL0 = '',
PUESTO.PU_NIVEL1 = '',
PUESTO.PU_NIVEL2 = '',
PUESTO.PU_NIVEL3 = '',
PUESTO.PU_NIVEL4 = '',
PUESTO.PU_NIVEL5 = '',
PUESTO.PU_NIVEL6 = '',
PUESTO.PU_NIVEL7 = '',
PUESTO.PU_NIVEL8 = '',
PUESTO.PU_NIVEL9 = '',
PUESTO.PU_TURNO = '',
PUESTO.PU_AUTOSAL = 'G',
PUESTO.PU_CONTRAT = '',
PUESTO.PU_TABLASS = '',
PUESTO.PU_PATRON = '',
PUESTO.PU_AREA = '',
PUESTO.PU_ZONA_GE = '',
PUESTO.PU_CHECA = 'G',
PUESTO.PU_SALARIO = 0,
PUESTO.PU_PER_VAR = 0,
PUESTO.PU_TIPO = 0,
PUESTO.PU_COSTO1 = 0,
PUESTO.PU_COSTO2 = 0,
PUESTO.PU_COSTO3 = 0,
PUESTO.PU_LEVEL = 0
where
( PUESTO.PU_REPORTA is NULL ) and
( PUESTO.PU_PLAZAS is NULL ) and
( PUESTO.PU_TURNO is NULL )
GO

/* Inicialiar PERIODO. */
/* Patch 380 Seq: 52 Agregado */
update PERIODO set PERIODO.PE_LOG = '' where ( PERIODO.PE_LOG is NULL )
GO

/* Inicialiar TURNO.TU_HOR_FES */
/* Patch 380 Seq: 53 Agregado */
update TURNO set TURNO.TU_HOR_FES = '' where ( TURNO.TU_HOR_FES is NULL )
GO

/* Inicialiar CURSO.CU_CLASE*/
/* Patch 380 Seq: 54 Agregado */
update CURSO set CURSO.CU_CLASE = '' where ( CURSO.CU_CLASE is NULL )
GO

/* Para almacenar costo est�ndard de la checada */
/* Patch 380 # Seq: 55 Agregado */
alter table WORKS add WK_STD_CST Pesos
GO

/* Inicialiar costo est�ndard de la checada */
/* Patch 380 # Seq: 56 Agregado */
update WORKS set WK_STD_CST = 0 where ( WK_STD_CST is NULL )
GO

/***************************************/
/**** Tablas Nuevas ********************/
/***************************************/

/* Para clasificar los cursos ( contrasta con TCURSO ) */
/* Patch 380 Seq: 5 Agregado */
create table CCURSO (
TB_CODIGO            Codigo,
TB_ELEMENT           Descripcion,
TB_INGLES            Descripcion,
TB_NUMERO            Pesos,
TB_TEXTO             Descripcion
)
GO

/* Llave primaria de CCURSO */
/* Patch 380 Seq: 6 Agregado */
ALTER TABLE CCURSO
       ADD CONSTRAINT PK_CCURSO PRIMARY KEY (TB_CODIGO)
GO

/* Para capturar percepciones fijas de PUESTOS */
/* Patch 380 Seq: 43 Agregado */
CREATE TABLE PTOFIJAS(
       PU_CODIGO           Codigo,
       PF_FOLIO		   FolioChico,
       PF_CODIGO	   Codigo2
)
GO

/* Llave primaria de PTOFIJAS */
/* Patch 380 Seq: 44 Agregado */
ALTER TABLE PTOFIJAS
      ADD CONSTRAINT PK_PTOFIJAS PRIMARY KEY (PU_CODIGO, PF_FOLIO)
GO

/* Llave for�nea de PTOFIJAS hacia PUESTO */
/* Patch 380 Seq: 45 Agregado */
ALTER TABLE PTOFIJAS
       ADD CONSTRAINT FK_PTOFIJAS_PUESTO
       FOREIGN KEY (PU_CODIGO)
       REFERENCES PUESTO
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Para capturar resguardo a entregar a PUESTOS */
/* Patch 380 Seq: 46 Agregado */
CREATE TABLE PTOTOOLS(
       PU_CODIGO      	   Codigo,
       TO_CODIGO	   Codigo,
       KT_REFEREN	   Mascara
)
GO

/* Llave primaria de PTOTOOLS */
/* Patch 380 Seq: 47 Agregado */
ALTER TABLE PTOTOOLS
      ADD CONSTRAINT PK_PTOTOOLS PRIMARY KEY (PU_CODIGO, TO_CODIGO)
GO

/* Llave for�nea de PTOTOOLS hacia PUESTO */
/* Patch 380 Seq: 48 Agregado */
ALTER TABLE PTOTOOLS
       ADD CONSTRAINT FK_PTOTOOLS_PUESTO
       FOREIGN KEY (PU_CODIGO)
       REFERENCES PUESTO
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Llave for�nea de PTOTOOLS hacia TOOLS */
/* Patch 380 Seq: 49 Agregado */
ALTER TABLE PTOTOOLS
       ADD CONSTRAINT FK_PTOTOOLS_TOOL
       FOREIGN KEY (TO_CODIGO)
       REFERENCES TOOL
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/******************************************/
/*** VIEWS ********************************/
/******************************************/

/* Para modificar VIEW */
/* Patch 380 Seq: 2 Agregado */
ALTER VIEW CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CU_CODIGO,
  KC_FEC_PRO,
  KC_EVALUA,
  KC_FEC_TOM,
  KC_HORAS,
  CU_HORAS,
  EN_OPCIONA,
  EN_LISTA,
  MA_CODIGO,
  KC_PROXIMO
) AS
SELECT
C.CB_CODIGO,
C.CB_PUESTO,
CURSO.CU_CODIGO,
DATEADD( DAY, E.EN_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
KARCURSO.KC_EVALUA,
KARCURSO.KC_FEC_TOM,
KARCURSO.KC_HORAS,
CU_HORAS,
E.EN_OPCIONA,
E.EN_LISTA,
CURSO.MA_CODIGO,
(SELECT MIN( CC_FECHA ) FROM CALCURSO WHERE ( CU_CODIGO = E.CU_CODIGO ) AND ( CC_FECHA >= cast( cast( getdate() as integer ) as datetime ))) KC_PROXIMO
FROM COLABORA C
JOIN ENTRENA E ON ( E.PU_CODIGO = C.CB_PUESTO )
LEFT JOIN KARCURSO ON ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) AND ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
LEFT JOIN CURSO ON ( CURSO.CU_CODIGO = E.CU_CODIGO )
WHERE ( CURSO.CU_ACTIVO = 'S' )

/******************************************/
/*** TRIGGERS *****************************/
/******************************************/

/* Trigger para edici�n de CCURSO */
/* Patch 380 Seq: 56 Agregado */
CREATE TRIGGER TU_CCURSO ON CCURSO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;

          select @NewCodigo = TB_CODIGO from Inserted;
          select @OldCodigo = TB_CODIGO from Deleted;

          update CURSO set CU_CLASE = @NewCodigo
          where ( CU_CLASE = @OldCodigo );
     END
END
GO

/* Trigger para edici�n de HORARIO: Considerar TURNO.TU_HOR_FES */
/* Patch 380 Seq: 26 Modificado */
ALTER TRIGGER TU_HORARIO ON HORARIO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( HO_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = HO_CODIGO from   Inserted;
        select @OldCodigo = HO_CODIGO from   Deleted;

	UPDATE AUSENCIA SET HO_CODIGO = @NewCodigo WHERE HO_CODIGO = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_1 = @NewCodigo WHERE TU_HOR_1 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_2 = @NewCodigo WHERE TU_HOR_2 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_3 = @NewCodigo WHERE TU_HOR_3 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_4 = @NewCodigo WHERE TU_HOR_4 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_5 = @NewCodigo WHERE TU_HOR_5 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_6 = @NewCodigo WHERE TU_HOR_6 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_7 = @NewCodigo WHERE TU_HOR_7 = @OldCodigo;
	UPDATE TURNO    SET TU_HOR_FES = @NewCodigo WHERE TU_HOR_FES = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL1: Considerar PUESTO.PU_NIVEL1*/
/* Patch 380 Seq: 1 Modificado */
ALTER TRIGGER TU_NIVEL1 ON NIVEL1 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;  
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL1 = @NewCodigo WHERE PU_NIVEL1 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL2: Considerar PUESTO.PU_NIVEL2*/
/* Patch 380 Seq: 2 Modificado */
ALTER TRIGGER TU_NIVEL2 ON NIVEL2 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL2 = @NewCodigo WHERE PU_NIVEL2 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL3: Considerar PUESTO.PU_NIVEL3*/
/* Patch 380 Seq: 3 Modificado */
ALTER TRIGGER TU_NIVEL3 ON NIVEL3 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL3 = @NewCodigo WHERE PU_NIVEL3 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL4: Considerar PUESTO.PU_NIVEL4*/
/* Patch 380 Seq: 4 Modificado */
ALTER TRIGGER TU_NIVEL4 ON NIVEL4 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL4 = @NewCodigo WHERE PU_NIVEL4 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL5: Considerar PUESTO.PU_NIVEL5*/
/* Patch 380 Seq: 5 Modificado */
ALTER TRIGGER TU_NIVEL5 ON NIVEL5 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL5 = @NewCodigo WHERE PU_NIVEL5 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL6: Considerar PUESTO.PU_NIVEL6*/
/* Patch 380 Seq: 6 Modificado */
ALTER TRIGGER TU_NIVEL6 ON NIVEL6 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL6 = @NewCodigo WHERE PU_NIVEL6 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL7: Considerar PUESTO.PU_NIVEL7*/
/* Patch 380 Seq: 7 Modificado */
ALTER TRIGGER TU_NIVEL7 ON NIVEL7 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL7 = @NewCodigo WHERE PU_NIVEL7 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL8: Considerar PUESTO.PU_NIVEL8*/
/* Patch 380 Seq: 8 Modificado */
ALTER TRIGGER TU_NIVEL8 ON NIVEL8 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL8 = @NewCodigo WHERE PU_NIVEL8 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de NIVEL9: Considerar PUESTO.PU_NIVEL9*/
/* Patch 380 Seq: 9 Modificado */
ALTER TRIGGER TU_NIVEL9 ON NIVEL9 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL9 = @NewCodigo WHERE PU_NIVEL9 = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de CONTRATO: Considerar PUESTO.PU_CONTRAT*/
/* Patch 380 Seq: 21 Modificado */
ALTER TRIGGER TU_CONTRATO ON CONTRATO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_CONTRAT = @NewCodigo WHERE CB_CONTRAT = @OldCodigo;
	UPDATE KARDEX	SET CB_CONTRAT = @NewCodigo WHERE CB_CONTRAT = @OldCodigo;
	UPDATE PUESTO	SET PU_CONTRAT = @NewCodigo WHERE PU_CONTRAT = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de TURNO: Considerar PUESTO.PU_TURNO*/
/* Patch 380 Seq: 31 Modificado */
ALTER TRIGGER TU_TURNO ON TURNO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TU_CODIGO from   Inserted;
        select @OldCodigo = TU_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARDEX	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE NOMINA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARCURSO	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE PUESTO	SET PU_TURNO = @NewCodigo WHERE PU_TURNO = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de SSOCIAL: Considerar PUESTO.PU_TABLASS*/
/* Patch 380 Seq: 32 Modificado */
ALTER TRIGGER TU_SSOCIAL ON SSOCIAL AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE KARDEX	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE VACACION	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE PUESTO	SET PU_TABLASS = @NewCodigo WHERE PU_TABLASS = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de RPATRON: Considerar PUESTO.PU_PATRON*/
/* Patch 380 Seq: 33 Modificado */
ALTER TRIGGER TU_RPATRON ON RPATRON AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE LIQ_IMSS	SET LS_PATRON = @NewCodigo WHERE LS_PATRON = @OldCodigo;
	UPDATE COLABORA	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE KARDEX	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE NOMINA	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE PUESTO	SET PU_PATRON = @NewCodigo WHERE PU_PATRON = @OldCodigo;
  END
END
GO

/* Trigger para borrado de PUESTO: Hacer que los subordinados reporten al jefe del puesto borrado */
/* Patch 380 Seq: 57 Agregado */
CREATE TRIGGER TD_PUESTO ON PUESTO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update PUESTO set PUESTO.PU_REPORTA = Deleted.PU_REPORTA
  from PUESTO, Deleted
  where ( PUESTO.PU_REPORTA = Deleted.PU_CODIGO );
END
GO

/* Trigger para edici�n de PUESTO: Considerar PUESTO.PU_REPORTA*/
/* Patch 380 Seq: 30 Modificado */
ALTER TRIGGER TU_PUESTO ON PUESTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( PU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = PU_CODIGO from   Inserted;
        select @OldCodigo = PU_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE KARDEX	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE NOMINA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE KARCURSO	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE PUESTO	SET PU_REPORTA = @NewCodigo WHERE PU_REPORTA = @OldCodigo;
  END
END
GO

/* Trigger para edici�n de OTRASPER: Considerar PTOFIJAS.PF_CODIGO*/
/* Patch 380 Seq: 23 Modificado */
ALTER TRIGGER TU_OTRASPER ON OTRASPER AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE CLASIFI  SET TB_OP1 = @NewCodigo WHERE TB_OP1 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP2 = @NewCodigo WHERE TB_OP2 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP3 = @NewCodigo WHERE TB_OP3 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP4 = @NewCodigo WHERE TB_OP4 = @OldCodigo;
	UPDATE CLASIFI  SET TB_OP5 = @NewCodigo WHERE TB_OP5 = @OldCodigo;
	UPDATE KAR_FIJA SET KF_CODIGO = @NewCodigo WHERE KF_CODIGO = @OldCodigo;
	UPDATE PTOFIJAS SET PF_CODIGO = @NewCodigo WHERE PF_CODIGO = @OldCodigo;
  END
END
GO

/* TU_PARTES: Considerar tabla CED_INSP */
/* Patch 380 # Seq: 36 Modificado */
ALTER TRIGGER TU_PARTES ON PARTES AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AR_CODIGO )
  BEGIN
	declare @NewCodigo CodigoParte, @OldCodigo CodigoParte;

	select @NewCodigo = AR_CODIGO from   Inserted;
        select @OldCodigo = AR_CODIGO from   Deleted;

	UPDATE WORKS	SET AR_CODIGO = @NewCodigo WHERE AR_CODIGO = @OldCodigo;
	UPDATE WORDER	SET AR_CODIGO = @NewCodigo WHERE AR_CODIGO = @OldCodigo;
	UPDATE CEDULA	SET AR_CODIGO = @NewCodigo WHERE AR_CODIGO = @OldCodigo;
	UPDATE CED_INSP	SET AR_CODIGO = @NewCodigo WHERE AR_CODIGO = @OldCodigo;
  END
END
GO

/* TU_AREA: Considerar tabla CEDULA, CED_INSP */
/* Patch 380 # Seq: 47 Modificado */
ALTER TRIGGER TU_AREA ON AREA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo CODIGO, @OldCodigo CODIGO;

	select @NewCodigo = TB_CODIGO from Inserted;
        select @OldCodigo = TB_CODIGO from Deleted;

        update SUP_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
        update KAR_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
        update COLABORA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
	update WORKS    set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
        update CEDULA   set CE_AREA = @NewCodigo where ( CE_AREA = @OldCodigo );
        update CED_INSP set CI_AREA = @NewCodigo where ( CI_AREA = @OldCodigo );
  END
END
GO

/* TU_WORDER: Considerar tabla CED_INSP */
/* Patch 380 # Seq: Modificado */
ALTER TRIGGER TU_WORDER ON WORDER AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( WO_NUMBER )
  BEGIN
	declare @NewCodigo OrdenTrabajo, @OldCodigo OrdenTrabajo;

	select @NewCodigo = WO_NUMBER from   Inserted;
        select @OldCodigo = WO_NUMBER from   Deleted;

	UPDATE WORKS SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
	UPDATE CEDULA SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
	UPDATE CED_WORD SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
	UPDATE CED_INSP	SET WO_NUMBER = @NewCodigo WHERE WO_NUMBER = @OldCodigo;
  END
END
GO

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

/* Patch 380: No considerar VACACION.VA_GOZO = 0 ( Vacaciones sin Dias Gozados ) */
/* Patch 380: Seq: 75 Modificado */
ALTER FUNCTION SP_STATUS_CONFLICTO
( @EMPLEADO INTEGER,
  @FECHAORIGINAL FECHA,
  @FECHAINICIAL FECHA,
  @FECHAFINAL FECHA,
  @ENTIDAD SMALLINT )
returns @VALORES table (
  RESULTADO INTEGER not null,
  INICIO DATETIME,
  FIN DATETIME )
as
begin
     declare @iClase Integer;
     declare @sActivo Char( 1 );
     declare @Valor Integer;
     declare @Inicial FECHA;
     declare @Final FECHA;
     set @Valor = 1;
     select @Inicial = C.CB_FEC_ING,
            @Final = C.CB_FEC_BAJ,
            @sActivo = C.CB_ACTIVO from DBO.COLABORA as C
     where ( C.CB_CODIGO = @EMPLEADO );
     if ( @FECHAINICIAL < @Inicial )
     begin
          set @Valor = 0;
     end
     else
     if ( ( @sActivo <> 'S' ) and ( @FECHAFINAL > @Final ) )
     begin
          set @Valor = 9;
     end
     else
     begin
          if ( @ENTIDAD = 1 )
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <> @FECHAORIGINAL ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          open Incapacidades;
          fetch NEXT from Incapacidades into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	set @Valor = 3;
               	break;
          end;
	  close Incapacidades;
          deallocate Incapacidades;
          if ( @ENTIDAD = 2 )
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_GOZO > 0 ) and
                  ( V.VA_FEC_INI <> @FECHAORIGINAL ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_GOZO > 0 ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end;
          open Vacaciones;
          fetch NEXT from Vacaciones into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	set @Valor = 2;
		break;
          end;
          close Vacaciones;
          deallocate Vacaciones;
          if ( @ENTIDAD = 3 )
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <> @FECHAORIGINAL ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end;
          open Permisos;
          fetch NEXT from Permisos into @iClase, @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	set @Valor = @iClase + 4;
		break;
          end;
          close Permisos;
          deallocate Permisos;
     end;
     insert into @VALORES( RESULTADO, INICIO, FIN ) values ( @Valor, @Inicial, @Final );
     RETURN
end
GO

/* Proteger contra divisi�n en cero cuando DiasRango = 0 */
/* Redondear Promedio a 2 decimales */
/* Patch 380: Seq: 160 Modificado */
ALTER PROCEDURE SP_SUBTOTAL_ROTACION
	(@USUARIO SMALLINT,
	 @ESTEGRUPO CHAR(6),
	 @ESTADESCRIPCION VARCHAR(30),
	 @INICIO INTEGER,
	 @ALTAS INTEGER,
 	 @BAJAS INTEGER,
         @DIASACTIVOS NUMERIC(15,2),
	 @DIASRANGO NUMERIC(15,2),
	 @FECHAINI DATETIME)
AS
BEGIN
     SET NOCOUNT ON
     declare @Promedio NUMERIC(15,2);
     if ( @DiasRAngo = 0 )
     begin
          Set @Promedio = 0;
     end
     else
     begin
          Set @Promedio = @DiasActivos / @DiasRango;
     end
     insert into TMPROTA
     (TR_GRUPO, TR_DESCRIP, TR_INICIO, TR_FINAL, TR_ALTAS, TR_BAJAS, TR_PROM, TR_FECHA, TR_USER ) values
     (@EsteGrupo, @EstaDescripcion, @Inicio, @Inicio+@Altas-@Bajas, @Altas, @Bajas, @Promedio, @FECHAINI, @Usuario );
end

/* Debe dejar COLABORA.CB_AREA, CB_AR_FEC = NullDateTime, CB_AR_HOR = '' */
/* Cuando no hay ninguna asignaci�n a �reas. Anteriormente, se usaba lo que estaba */
/* almacenado en COLABORA.CB_AREA para agregar al Kardex de Area una asignaci�n con */
/* la fecha de ingreso del empleado (ver INIT_AREA )*/
/* Patch 380: Seq: 51 Modificado */
ALTER PROCEDURE DBO.RECALCULA_AREA @EMPLEADO INTEGER
AS
begin
     SET NOCOUNT ON;
     declare @VKA_FECHA DATETIME;
     declare @VKA_HORA CHAR(4);
     declare @VCB_AREA CHAR(6);
     declare Cur_RecalculaArea CURSOR FOR
     select TOP 1
            KA_FECHA,
            KA_HORA,
            CB_AREA
     from KAR_AREA
     where ( CB_CODIGO = @EMPLEADO )
     order by KA_FECHA desc, KA_HORA desc;
     open Cur_RecalculaArea;
     fetch next from Cur_RecalculaArea into @vKA_FECHA, @vKA_HORA, @vCB_AREA;
     if ( @@ROWCOUNT  <> 0 )
     begin
          update COLABORA set
          CB_AREA = @VCB_AREA,
          CB_AR_FEC = @VKA_FECHA,
          CB_AR_HOR = @VKA_HORA
          where ( CB_CODIGO = @EMPLEADO );
     end
     else
     begin
          update COLABORA set
          CB_AREA = '',
          CB_AR_FEC = '12/30/1899',
          CB_AR_HOR = '0000'
          where ( CB_CODIGO = @EMPLEADO );
     END
     close Cur_RecalculaArea;
     deallocate Cur_RecalculaArea;
END
GO

/* No Regresa VACIO cuando la fecha y hora son anteriores */
/* a la primera asignacion de un empleado a un �rea */
/* No es igual que en KARDEX dado que cuando se pregunta */
/* para un empleado un dato anterior a su fecha de ingreso */
/* se debe dar lo primero que se tiene en KARDEX; sin embargo */
/* la asignaci�n de �reas se puede dar cuando el empleado ya es */
/* parte de la empresa*/
/* Patch 380: Seq: 55 Modificado */
ALTER FUNCTION DBO.SP_FECHA_AREA_KARDEX (
	@EMPLEADO INTEGER,
	@FECHA DATETIME,
	@HORA CHAR(4) )
RETURNS @RegKardex TABLE (
      	KA_FECHA DATETIME,
	KA_HORA CHAR(4),
	CB_AREA CHAR(6),
	US_CODIGO SMALLINT,
	KA_FEC_MOV DATETIME,
	KA_HOR_MOV CHAR(4) )
AS
BEGIN
     insert into @RegKardex
             select TOP 1 KA_FECHA, KA_HORA, CB_AREA, US_CODIGO, KA_FEC_MOV, KA_HOR_MOV
             from KAR_AREA where
             ( CB_CODIGO = @Empleado ) and
             ( ( KA_FECHA < @Fecha ) or ( ( KA_FECHA = @Fecha ) and ( KA_HORA <= @Hora ) ) )
             order by KA_FECHA desc, KA_HORA desc
     if ( @@ROWCOUNT = 0 )
     begin
          set @Fecha = '12/30/1899'
          insert into @RegKardex values ( @Fecha, '0000', '', 0, @Fecha, '0000' )
     end
     return
END
GO

/* Considerar TURNO.TU_HOR_FES */
/* Patch 380: 95 Modificado*/
ALTER FUNCTION TURNO_EMPLEADO(
  @Fecha Datetime,
  @Empleado Integer )
RETURNS @TablaTurno TABLE
(      TU_RIT_PAT           Varchar(100),
       TU_HOR_1             Char(6),
       TU_HOR_2             Char(6),
       TU_HOR_3             Char(6),
       TU_HOR_4             Char(6),
       TU_HOR_5             Char(6),
       TU_HOR_6             Char(6),
       TU_HOR_7             Char(6),
       TU_TIP_1             Smallint,
       TU_TIP_2             Smallint,
       TU_TIP_3             Smallint,
       TU_TIP_4             Smallint,
       TU_TIP_5             Smallint,
       TU_TIP_6             Smallint,
       TU_TIP_7             Smallint,
       TU_CODIGO            Char(6),
       TU_RIT_INI           Datetime,
       TU_HORARIO           Smallint,
       TU_HOR_FES	    Char(6),
       TU_VACA_HA           Numeric(15,2),
       TU_VACA_SA           Numeric(15,2),
       TU_VACA_DE           Numeric(15,2),
       CB_CREDENC           Char(1) )
AS
BEGIN
     INSERT @TablaTurno
     SELECT TU_RIT_PAT,
            TU_HOR_1,
            TU_HOR_2,
            TU_HOR_3,
            TU_HOR_4,
            TU_HOR_5,
            TU_HOR_6,
            TU_HOR_7,
            TU_TIP_1,
            TU_TIP_2,
            TU_TIP_3,
            TU_TIP_4,
            TU_TIP_5,
            TU_TIP_6,
            TU_TIP_7,
            TU_CODIGO,
            TU_RIT_INI,
            TU_HORARIO,
            TU_HOR_FES,
            TU_VACA_HA,
            TU_VACA_SA,
            TU_VACA_DE, 
            ( select C.CB_CREDENC from COLABORA C where ( C.CB_CODIGO = @Empleado ) ) CB_CREDENC
     FROM TURNO WHERE ( TU_CODIGO = dbo.SP_KARDEX_CB_TURNO( @Fecha, @Empleado ) );
     RETURN
END
GO

/* Determina dependencias RECURSIVAMENTE hacia abajo de un PUESTO */
/* Patch 380 # Seq: 198 Agregado */
create function DBO.GET_SUB_PUESTOS( @PUESTO CHAR(6) ) returns @SUBPUESTOS TABLE( PU_SUB CHAR(6) )
as
begin
     declare @SUB CHAR(6)
     declare SUBALTERNOS cursor for
     select PU_CODIGO from GRUPO where ( PU_REPORTA = @PUESTO )
     order by PU_CODIGO;
     open SUBALTERNOS;
     fetch NEXT from SUBALTERNOS into @SUB;
     while ( @@Fetch_Status = 0 )
     begin
          insert into @SUBPUESTOS( PU_SUB ) values ( @SUB );
          insert into @SUBPUESTOS select PU_SUB from DBO.GET_SUB_PUESTOS( @SUB );
          fetch NEXT from SUBALTERNOS into @SUB;
     end;
     close SUBALTERNOS;
     deallocate SUBALTERNOS;
     return;
end
GO

/* Determina si hay dependencias circulares entre PUESTOS */
/* Patch 380 # Seq: 199 Agregado */
create procedure DBO.CHECK_REPORT_CHAIN( @PUESTO CHAR(6) )
as
begin
     SET NOCOUNT ON;
     declare @REPORTA CHAR(6);
     select @REPORTA = PU_REPORTA from PUESTO where ( PU_CODIGO = @PUESTO );
     while ( ( @REPORTA is not NULL ) and ( @REPORTA > '' ) )
     begin
          if ( @REPORTA = @PUESTO )
          begin
	       RAISERROR( 'La Cadena De Subordinaci�n Es Circular', 16, 1 )
               BREAK;
          end
          else
          begin
               select @REPORTA = PU_REPORTA from PUESTO where ( PU_CODIGO = @REPORTA );
          end
     end
end
GO

/* Determina quien es el jefe de un empleado */
/* Patch 380 # Seq: 201 Agregado */
create function DBO.SP_JEFE( @EMPLEADO Integer ) returns Integer
as
begin
     declare @REPORTA Char(6);
     declare @JEFE Integer;
     select @REPORTA = PU_REPORTA from COLABORA
     left join PUESTO on ( PUESTO.PU_CODIGO = COLABORA.CB_PUESTO )
     where ( CB_CODIGO = @Empleado );
     if ( @REPORTA is NULL )
     begin
          select @JEFE = 0;
     end
     else
     begin
          select @JEFE = MIN( CB_CODIGO ) from COLABORA
          where ( CB_ACTIVO = 'S' ) and ( CB_PUESTO = @REPORTA );
          if ( @JEFE is NULL )
          begin
               select @JEFE = 0;
          end
     end
     return @JEFE;
end
GO

/* Patch 380: Usada para Contar cuantos empleados tienen un puesto */
/* El par�metro FECHA se usar� posteriormente cuando se desee saber */
/* Cuantos empleados tienen un puesto en una fecha */
/* Patch 380 # Seq: 202 Agregado */
CREATE FUNCTION DBO.SP_POSICIONES( @PUESTO CHAR(6), @FECHA DATETIME )
RETURNS INTEGER
AS
begin
     DECLARE @Resultado AS INTEGER;
     select @Resultado = COUNT( CB_CODIGO ) from COLABORA
     where ( CB_ACTIVO = 'S' ) and ( CB_PUESTO = @PUESTO );
     if ( @Resultado is NULL )
        select @Resultado = 0
     RETURN @Resultado
end
GO

/* Usada para inicializar el k�rdes de �reas de un */
/* empleado al momento de hacer registrar su alta */
/* Patch 380 # Seq: 200 Agregado */
CREATE PROCEDURE DBO.INIT_AREA( @EMPLEADO INTEGER, @AREA CHAR(6), @USUARIO INTEGER )
AS
begin
     SET NOCOUNT ON;
     if ( @AREA <> '' )
     begin
          declare @INGRESO DATETIME;
          select @INGRESO = CB_FEC_ING from COLABORA where ( CB_CODIGO = @EMPLEADO );
          if ( @INGRESO is not NULL )
          begin
               insert into KAR_AREA ( CB_CODIGO,
                                      KA_FECHA,
                                      KA_HORA,
                                      CB_AREA,
                                      KA_FEC_MOV,
                                      KA_HOR_MOV,
                                      US_CODIGO )
               values ( @EMPLEADO,
                        @INGRESO,
                        '0000',
                        @AREA,
                        GetDate(),
                        '0000',
                        @USUARIO );
               execute RECALCULA_AREA @EMPLEADO;
          end
     end
end
GO

/* Patch 380: Agregar PUESTO (59) a lista de Entidades */
/* Patch 380 # Seq: 185 Modificado */
ALTER PROCEDURE DBO.SP_TITULO_CAMPO(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @TITULO VARCHAR(30);
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @NumGlobal );
     if ( ( @Titulo is Null ) or ( @Titulo = '' ) )
        SET @Titulo = @TituloDefault;
     update DICCION set DI_TITULO = @Titulo,
                        DI_TCORTO= @Titulo,
                        DI_CLAVES = ''
     where ( DI_CLASIFI in ( 5, 10, 18, 32, 33, 50, 59, 124, 141, 142, 143, 144, 145, 146, 147, 148, 149, 152, 186, 187 ) )
     and ( DI_NOMBRE = @Nombre );
END
GO

/* Patch 380: Agregar llamadas a SP_TITULO_CAMPO por Niveles de PUESTO */
/* Patch 380 # Seq: 195 Modificado */
ALTER PROCEDURE DBO.SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 35, 'Tabla Adicional #1', 19
  execute  SP_TITULO_TABLA 36, 'Tabla Adicional #2', 20
  execute  SP_TITULO_TABLA 37, 'Tabla Adicional #3', 21
  execute  SP_TITULO_TABLA 38, 'Tabla Adicional #4', 22

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 176, 'Lista de Defectos', 187
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PU_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PU_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PU_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PU_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PU_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PU_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PU_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PU_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PU_NIVEL9'

  execute  SP_TITULO_CAMPO 22, 'Fecha Adicional #1', 'CB_G_FEC_1'
  execute  SP_TITULO_CAMPO 23, 'Fecha Adicional #2', 'CB_G_FEC_2'
  execute  SP_TITULO_CAMPO 24, 'Fecha Adicional #3', 'CB_G_FEC_3'
  execute  SP_TITULO_CAMPO 25, 'Texto Adicional #1', 'CB_G_TEX_1'
  execute  SP_TITULO_CAMPO 26, 'Texto Adicional #2', 'CB_G_TEX_2'
  execute  SP_TITULO_CAMPO 27, 'Texto Adicional #3', 'CB_G_TEX_3'
  execute  SP_TITULO_CAMPO 28, 'Texto Adicional #4', 'CB_G_TEX_4'
  execute  SP_TITULO_CAMPO 29, 'N�mero Adicional #1', 'CB_G_NUM_1'
  execute  SP_TITULO_CAMPO 30, 'N�mero Adicional #2', 'CB_G_NUM_2'
  execute  SP_TITULO_CAMPO 31, 'N�mero Adicional #3', 'CB_G_NUM_3'
  execute  SP_TITULO_CAMPO 32, 'L�gico Adicional #1', 'CB_G_LOG_1'
  execute  SP_TITULO_CAMPO 33, 'L�gico Adicional #2', 'CB_G_LOG_2'
  execute  SP_TITULO_CAMPO 34, 'L�gico Adicional #3', 'CB_G_LOG_3'
  execute  SP_TITULO_CAMPO 35, 'Tabla Adicional #1','CB_G_TAB_1'
  execute  SP_TITULO_CAMPO 36, 'Tabla Adicional #2','CB_G_TAB_2'
  execute  SP_TITULO_CAMPO 37, 'Tabla Adicional #3','CB_G_TAB_3'
  execute  SP_TITULO_CAMPO 38, 'Tabla Adicional #4','CB_G_TAB_4'

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER'
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO'
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CI_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CE_AREA'
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO'
  execute  SP_TITULO_CAMPO 176, 'Defecto','DE_CODIGO'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

  execute  SP_TITULO_EXPEDIEN_BOL  'EX_G_BOL', 'Si/No/Texto'
  execute  SP_TITULO_EXPEDIEN  'EX_G_LOG', 'Si/No'
  execute  SP_TITULO_EXPEDIEN  'EX_G_NUM', 'Numero'
  execute  SP_TITULO_EXPEDIEN  'EX_G_TEX', 'Texto'

  execute  SP_CUSTOM_DICCION

end
GO
