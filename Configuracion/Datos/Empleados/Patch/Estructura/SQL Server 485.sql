/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

if not exists( select NAME from SYSOBJECTS  where NAME='T_Analitica' and XTYPE='U' )
begin
	create table T_Analitica
	(
		AnaliticaID				FolioGrande Identity(1,1), 	
		AnaliticaModulo			Descripcion, 
		AnaliticaHelpContext	FolioGrande,
		AnaliticaCodigo			Descripcion, 
		AnaliticaDescripcion	Formula, 	
		AnaliticaBool			Booleano, 
		AnaliticaNumero			Pesos,
		AnaliticaFecha			Fecha,
		AnaliticaFechaUpd		Fecha, 
		AnaliticaUsuario		Usuario,
		AnaliticaTexto			varchar(max), 

		CONSTRAINT PK_Analitica PRIMARY KEY CLUSTERED 
		(
			AnaliticaID
		)

	)
end
go

IF not exists ( select * from dbo.sysindexes where name = 'UQIDX_Analitica'  ) 
begin
	CREATE UNIQUE INDEX UQIDX_Analitica ON T_Analitica
	(
		   AnaliticaModulo,
		   AnaliticaHelpContext, 
		   AnaliticaCodigo  
	)
end
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Upsert') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Upsert
go

create procedure Analitica_Upsert( @Modulo Descripcion,  @HelpContext FolioGrande, @Codigo Descripcion,  @Descripcion Formula, @Bool Booleano = 'N' , @Numero Pesos = 0.00 , @Fecha Fecha = '1899-12-30' , @Usuario Usuario = 1 , @Texto varchar(max) = '') 
as
begin
	set nocount on  
	
	set @Modulo = coalesce( @Modulo, '' ) 
	set @HelpContext = coalesce( @HelpContext, 0) 
	set @Descripcion = coalesce( @Descripcion, '' ) 
	set @Bool = coalesce( @Bool, 'N' ) 
	set @Numero = coalesce( @Numero, 0.00) 
	set @Usuario = coalesce( @Usuario, 0 ) 
	set @Texto = coalesce( @Texto, '' ) 

	update T_Analitica 
	set 
		AnaliticaDescripcion = case when @Descripcion = '' then AnaliticaDescripcion else @Descripcion end, 
		AnaliticaBool = @Bool, 
		AnaliticaNumero = @Numero, 
		AnaliticaTexto = @Texto, 
		AnaliticaUsuario = @Usuario, 
		AnaliticaFechaUpd = getdate(), 
		AnaliticaFecha = @Fecha
	where 
		AnaliticaModulo = @Modulo and AnaliticaHelpContext = @HelpContext and AnaliticaCodigo = @Codigo


	if @@ROWCOUNT > 0 
		return 
	else 
	begin 
		insert into T_Analitica (AnaliticaModulo, AnaliticaHelpContext, AnaliticaCodigo, AnaliticaDescripcion, AnaliticaBool, AnaliticaNumero, AnaliticaUsuario,  AnaliticaFecha, AnaliticaFechaUpd, AnaliticaTexto ) 
		values ( @Modulo, @HelpContext,  @Codigo, @Descripcion, @Bool, @Numero, @Usuario, @Fecha, GETDATE(), @Texto ) 
	end 
			

end 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Iniciar_Version2017') AND type IN ( N'P', N'PC' ))
drop procedure Analitica_Iniciar_Version2017
go 

create procedure Analitica_Iniciar_Version2017 
as
begin 
	if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Version 2017' ) = 0 
	begin 
		exec Analitica_Upsert 'Versi�n 2017' , 1, 'V2017_DB' ,   'Tableros de inicio - Uso'
		exec Analitica_Upsert 'Versi�n 2017' , 2, 'V2017_RE1' ,  'Reportes Email - Env�os programados'
	end 
end 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Poblar_Version2017') AND type IN ( N'P', N'PC' ))
	drop  procedure Analitica_Poblar_Version2017
go 

create  procedure Analitica_Poblar_Version2017
as
begin 	
	declare @CantidadRolTableros int 
	declare @UsaTablerosInicio booleano 
	
	select @CantidadRolTableros = count(*) from #Comparte..RolTableros rt join #Comparte..CompanyTableros ct on rt.CompanyTableroID = ct.CompanyTableroID  
    join #Comparte..Tableros t on t.TableroID = ct.TableroID where TableroNombre <> 'Mi Empresa' 
	if ( @CantidadRolTableros > 0 ) 
		set @UsaTablerosInicio = 'S' 
	else 
		set @UsaTablerosInicio= 'N' 
	
	exec Analitica_Upsert 'Versi�n 2017' , 1, 'V2017_DB' ,  '',  @UsaTablerosInicio,  @CantidadRolTableros

	declare @CantidadEnviosProgramados int 
	
	select @CantidadEnviosProgramados  = COUNT(*) from #Comparte..CALENDARIO where CA_ACTIVO = 'S'
	exec Analitica_Upsert 'Versi�n 2017' , 2, 'V2017_RE1' ,  '',  'N',  @CantidadEnviosProgramados
	
end 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Iniciar_Version2018') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Iniciar_Version2018
go

create procedure Analitica_Iniciar_Version2018 
as
begin 
	if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Version 2018' ) = 0 
	begin 
		exec Analitica_Upsert 'Versi�n 2018' , 3, 'V2018_DP' ,   'Desafectaci�n Parcial' 
		exec Analitica_Upsert 'Versi�n 2018' , 4, 'V2018_PS1' ,  'Previsi�n Social - Conceptos'
		exec Analitica_Upsert 'Versi�n 2018' , 5, 'V2018_PS2' ,  'Previsi�n Social - Uso Funci�n'
		exec Analitica_Upsert 'Versi�n 2018' , 6, 'V2018_FON1' , 'Fonacot - Global Encendido'
		exec Analitica_Upsert 'Versi�n 2018' , 7, 'V2018_FON2' , 'Fonacot - Importar C�dula'
		exec Analitica_Upsert 'Versi�n 2018' , 8, 'V2018_FON3' , 'Fonacot - Generaci�n de c�dula desde Sistema TRESS'
		exec Analitica_Upsert 'Versi�n 2018' , 9, 'V2018_FON4' , 'Fonacot - Empleados c/Fonacot'
		exec Analitica_Upsert 'Versi�n 2018' , 10, 'V2018_CUR1' , 'Cursos - Registrados con aplicaci�n'
		exec Analitica_Upsert 'Versi�n 2018' , 11, 'V2018_CUR2' , 'Cursos - Total de cursos', 'N' 
		exec Analitica_Upsert 'Versi�n 2018' , 12, 'V2018_CUR3' , 'Cursos - Total de cursos configurados en Matriz de entrenamiento'
		exec Analitica_Upsert 'Versi�n 2018' , 13, 'V2018_CUR4' , 'Cursos - Total de Grupos de Capacitaci�n'
	end 
end 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Poblar_Version2018') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Poblar_Version2018
go

create  procedure Analitica_Poblar_Version2018
as
begin 
	
	declare @UltimoProcDesafectarParcial Fecha
	select top 1 @UltimoProcDesafectarParcial = PC_FEC_INI from Proceso where PC_PROC_ID = 22 and CB_CODIGO > 0  and PC_FEC_INI > '2018-07-01' order by PC_FEC_INI desc

	set @UltimoProcDesafectarParcial = coalesce( @UltimoProcDesafectarParcial, '1899-12-30' ) 

	if ( @UltimoProcDesafectarParcial > '1899-12-30' ) 
		exec Analitica_Upsert 'Versi�n 2018' , 3, 'V2018_DP', '', 'S', 0.00, @UltimoProcDesafectarParcial


	-- Prevision Social 
	declare @ConceptosPrevisionSocial int 
	declare @UsaConceptosPrevisionSocial booleano 
	
	select @ConceptosPrevisionSocial = COUNT(*) from CONCEPTO where CO_PS_TIPO = 1 and CO_ACTIVO = 'S'  
	if ( @ConceptosPrevisionSocial > 0 ) 
		set @UsaConceptosPrevisionSocial = 'S' 
	else 
		set @UsaConceptosPrevisionSocial= 'N' 
	
	exec Analitica_Upsert 'Versi�n 2018' , 4, 'V2018_PS1' ,  '',  @UsaConceptosPrevisionSocial,  @ConceptosPrevisionSocial


	select @ConceptosPrevisionSocial = COUNT(*) from CONCEPTO where CO_X_ISPT like '%CAL_EX_PS%'
	if ( @ConceptosPrevisionSocial > 0 ) 
		set @UsaConceptosPrevisionSocial = 'S' 
	else 
		set @UsaConceptosPrevisionSocial= 'N' 
	exec Analitica_Upsert 'Versi�n 2018' , 5, 'V2018_PS2' ,  '',  @UsaConceptosPrevisionSocial,  @ConceptosPrevisionSocial

	/* Fonacot v2018 */
	-- Global.
	declare @GlobalFonacot Booleano SET @GlobalFonacot = 'N';
	SELECT @GlobalFonacot = COALESCE (GL_FORMULA, 'N') FROM GLOBAL WHERE GL_CODIGO = 328 	
	exec Analitica_Upsert 'Versi�n 2018' , 6, 'V2018_FON1' ,  '', @GlobalFonacot 
	-- Importar c�dula
	declare @Numero Empleados set @Numero = 0;
	select @Numero = coalesce (count(pe_tipo), 0) from pced_enc
	declare @Fecha Fecha set @Fecha = '1899-12-30 00:00:00.000'
	SELECT TOP 1 @Fecha = coalesce (PC_FEC_INI, '1899-12-30 00:00:00.000') FROM PROCESO WHERE PC_PROC_ID = 209 ORDER BY PC_FEC_INI DESC, PC_HOR_INI DESC
	exec Analitica_Upsert 'Versi�n 2018' , 7, 'V2018_FON2' ,  '', null, @Numero, @Fecha
	-- Generaci�n de c�dula
	set @Numero = 0
	set @Fecha = '1899-12-30 00:00:00.000'
	SELECT @Numero = coalesce (COUNT (PC_PROC_ID), 0), @Fecha = Coalesce (MAX (PC_FEC_INI), '1899-12-30 00:00:00.000') FROM PROCESO WHERE PC_PROC_ID = 210
	exec Analitica_Upsert 'Versi�n 2018' , 8, 'V2018_FON3' ,  '', null, @Numero, @Fecha
	-- Empleados Fonacot
	set @Numero = 0
	SELECT @Numero = coalesce (COUNT(CB_FONACOT), 0) FROM COLABORA WHERE CB_FONACOT <> '' and CB_ACTIVO = 'S' 
	exec Analitica_Upsert 'Versi�n 2018' , 9, 'V2018_FON4' ,  '', null, @Numero

	/* Cursos v2018 */
	-- V2018_CUR1.
	-- Cursos - Registrados con aplicaci�n [pendiente]
	-- V2018_CUR2.
	set @Numero = 0;
	SELECT @Numero = coalesce (COUNT(CU_CODIGO), 0) FROM CURSO  where CU_ACTIVO='S' 
	exec Analitica_Upsert 'Versi�n 2018' , 11, 'V2018_CUR2' ,  '', null, @Numero
	-- V2018_CUR3
	set @Numero = 0;
	SELECT @Numero = COALESCE (COUNT(DISTINCT E.CU_CODIGO), 0) FROM ENTRENA E JOIN  CURSO C ON E.CU_CODIGO = C.CU_CODIGO WHERE C.CU_ACTIVO = 'S'
	exec Analitica_Upsert 'Versi�n 2018' , 12, 'V2018_CUR3' ,  '', null, @Numero 
	-- V2018_CUR4
	set @Numero = 0;
	SELECT @Numero = coalesce (COUNT(SE_FOLIO), 0) FROM  SESION join CURSO on SESION.CU_CODIGO = CURSO.CU_CODIGO where CURSO.CU_ACTIVO = 'S' 
	exec Analitica_Upsert 'Versi�n 2018' , 13, 'V2018_CUR4' ,  '', null, @Numero 
end 
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Poblar') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Poblar
go

create procedure Analitica_Poblar
as
begin 
	exec Analitica_Poblar_Version2017; 
	exec Analitica_Poblar_Version2018; 	
end; 
go

exec Analitica_Iniciar_Version2017
go 

exec Analitica_Iniciar_Version2018
go 


exec Analitica_Poblar
go