/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   Base De Datos DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/************************************************/
/***** Tablas Nuevas ****************************/
/************************************************/

/* Tabla PLAZA: Plazas de Puestos ( Sug. 84 ) */
/* Patch 400 # Seq: 1 Agregado */
create table PLAZA (
       PL_FOLIO             FolioGrande  IDENTITY (1, 1),
       PL_ORDEN             FolioChico,
       PL_CODIGO            Descripcion,
       PL_NOMBRE            Observaciones,
       PL_INGLES            Descripcion,
       PL_NUMERO            Pesos,
       PL_TEXTO             Descripcion,
       PL_REPORTA           FolioGrande,
       PL_TIPO              Status,
       PL_TIREP             Status,
       PL_FEC_INI           Fecha,
       PL_FEC_FIN           Fecha,
       PL_SUB_CTA           Descripcion,
       CB_CODIGO            NumeroEmpleado,
       PU_CODIGO            Codigo,
       PL_CLASIFI           Codigo,
       PL_TURNO             Codigo,
       PL_PATRON            RegPatronal,
       PL_NIVEL0            Codigo,
       PL_NIVEL1            Codigo,
       PL_NIVEL2            Codigo,
       PL_NIVEL3            Codigo,
       PL_NIVEL4            Codigo,
       PL_NIVEL5            Codigo,
       PL_NIVEL6            Codigo,
       PL_NIVEL7            Codigo,
       PL_NIVEL8            Codigo,
       PL_NIVEL9            Codigo,
       PL_CONTRAT           Codigo1,
       PL_AUTOSAL           Booleano,
       PL_SALARIO           PesosDiario,
       PL_PER_VAR           PesosDiario,
       PL_TABLASS           Codigo1,
       PL_ZONA_GE           ZonaGeo,
       PL_CHECA             Booleano,
       PL_AREA              Codigo
)
GO

/* Tabla PLAZA: PK Plazas de Puestos ( Sug. 84 ) */
/* Patch 400 # Seq: 2 Agregado */
alter table PLAZA add constraint PK_PLAZA primary key( PL_FOLIO )
GO

/* Tabla PLAZA: Indice sobre PLAZA por Puesto y Orden ( Sug. 84 ) */
/* Patch 400 # Seq: 3 Agregado */
create unique index XAK1PLAZA on PLAZA ( PU_CODIGO, PL_ORDEN )
GO

/* Tabla PLAZA: FK PLAZAS hacia PUESTO ( Sug. 84 ) */
/* Patch 400 # Seq: 4 Agregado */
alter table PLAZA
       add constraint FK_PLAZA_PUESTO
       foreign key ( PU_CODIGO )
       references PUESTO
       on delete NO ACTION
       on update CASCADE
GO

/* Tabla POL_TIPO: Tipos de P�lizas ( Sug. 738 ) */
/* Patch 400 # Seq: 6 Agregado */
create table POL_TIPO (
       PT_CODIGO            Codigo,
       PT_NOMBRE            DescLarga,
       PT_INGLES            DescLarga,
       PT_NUMERO            Pesos,
       PT_TEXTO             DescLarga,
       PT_REPORTE           FolioChico
)
GO

/* Tabla POL_TIPO: PK Tipos de P�lizas ( Sug. 738 ) */
/* Patch 400 # Seq: 7 Agregado */
alter table POL_TIPO add constraint PK_POL_TIPO primary key( PT_CODIGO )
GO

/* Tabla POL_HEAD: Encabezados de P�lizas ?( Sug. 738 ) */
/* Patch 400 # Seq: 8 Agregado */
create table POL_HEAD (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PT_CODIGO            Codigo,
       US_CODIGO            Usuario,
       PH_FECHA             Fecha,
       PH_HORA              Hora,
       PH_VECES             FolioChico,
       PH_REPORTE           FolioChico,
       PH_STATUS            Status
)
GO

/* Tabla POL_HEAD: PK Encabezados de P�lizas ?( Sug. 738 ) */
/* Patch 400 # Seq: 9 Agregado */
alter table POL_HEAD add constraint PK_POL_HEAD primary key( PE_YEAR, PE_TIPO, PE_NUMERO, PT_CODIGO )
GO

/* Tabla POL_HEAD: FK hacia POL_TIPO ( Sug. 738 ) */
/* Patch 400 # Seq: 10 Agregado */
alter table POL_HEAD
       add constraint FK_POL_HEAD_POL_TIPO
       foreign key( PT_CODIGO )
       references POL_TIPO
       on delete CASCADE
       on update CASCADE
GO

/* Tabla POL_HEAD: FK hacia PERIODO ( Sug. 738 ) */
/* Patch 400 # Seq: 11 Agregado */
alter table POL_HEAD
       add constraint FK_POL_HEAD_PERIODO
       foreign key( PE_YEAR, PE_TIPO, PE_NUMERO )
       references PERIODO
       on delete CASCADE
       on update CASCADE
GO

/* Tabla CERTIFIC:  ( Sug. 790 ) */
/* Patch 400 # Seq: 12 Agregado */
create table CERTIFIC (
       CI_CODIGO            Codigo,
       CI_NOMBRE            DescLarga,
       CI_RESUMEN           Formula,
       CI_DETALLE           Memo,
       CI_INGLES            DescLarga,
       CI_NUMERO            Pesos,
       CI_TEXTO             DescLarga,
       CI_RENOVAR           Dias
)
GO

/* Tabla CERTIFIC: PK CERTIFIC ( Sug. 790 ) */
/* Patch 400 # Seq: 13 Agregado */
alter table CERTIFIC add constraint PK_CERTIFIC primary key( CI_CODIGO )
GO

/* Tabla KAR_CERT: ( Sug. 790 ) */
/* Patch 400 # Seq: 14 Agregado */
create table KAR_CERT (
       CB_CODIGO            NumeroEmpleado,
       KI_FEC_CER           Fecha,
       CI_CODIGO            Codigo,
       KI_FOLIO             Descripcion,
       KI_RENOVAR           Dias,
       KI_APROBO            Booleano,
       KI_CALIF_1           Numerico,
       KI_CALIF_2           Numerico,
       KI_CALIF_3           Numerico,
       KI_SINOD_1           DescLarga,
       KI_SINOD_2           DescLarga,
       KI_SINOD_3           DescLarga,
       KI_OBSERVA           Memo,
       US_CODIGO            Usuario
)
GO

/* Tabla KAR_CERT: PK KAR_CERT ( Sug. 790 ) */
/* Patch 400 # Seq: 15 Agregado */
alter table KAR_CERT add constraint PK_KAR_CERT primary key( CB_CODIGO, KI_FEC_CER, CI_CODIGO )
GO

/* Tabla KAR_CERT: FK hacia CERTIFIC ( Sug. 790 ) */
/* Patch 400 # Seq: 16 Agregado */
alter table KAR_CERT
      add constraint FK_KAR_CERT_CERTIFIC
      foreign key( CI_CODIGO )
      references CERTIFIC
      on delete CASCADE
      on update CASCADE
GO

/* Tabla KAR_CERT: FK hacia COLABORA ( Sug. 790 ) */
/* Patch 400 # Seq: 17 Agregado */
alter table KAR_CERT
      add constraint FK_KAR_CERT_COLABORA
      foreign key( CB_CODIGO )
      references COLABORA
      on delete CASCADE
      on update CASCADE
GO

/* Tabla DESCTIPO: ( Sug. 791 ) */
/* Patch 400 # Seq: 18 Agregado */
create table DESCTIPO(
       DT_CODIGO            Codigo,
       DT_NOMBRE            DescLarga,
       DT_INGLES            DescLarga,
       DT_ORDEN             FolioChico,
       DT_NUMERO            Pesos,
       DT_TEXTO             DescLarga
)
GO

/* Tabla DESCTIPO: PK DESCTIPO ( Sug. 791 ) */
/* Patch 400 # Seq: 19 Agregado */
alter table DESCTIPO add constraint PK_DESCTIPO primary key( DT_CODIGO )
GO

/* Tabla DESCTIPO: Indice sobre Orden ( Sug. 791 ) */
/* Patch 400 # Seq: 20 Agregado */
create unique index XAK1DESCTIPO on DESCTIPO( DT_ORDEN )
GO

/* Tabla PERFIL: ( Sug. 791 ) */
/* Patch 400 # Seq: 21 Agregado */
create table PERFIL (
       PU_CODIGO            Codigo,
       PF_DESCRIP           Memo,
       PF_OBJETIV           Memo,
       PF_POSTING           Memo,
       PF_ESTUDIO           Status,
       PF_FECHA             Fecha,
       PF_EXP_PTO           FolioChico,
       PF_CONTRL1           DescLarga,
       PF_CONTRL2           DescLarga,
       PF_NUMERO1           Pesos,
       PF_TEXTO1            DescLarga,
       PF_NUMERO2           Pesos,
       PF_EDADMIN           Anio,
       PF_EDADMAX           Anio,
       PF_TEXTO2            DescLarga,
       PF_SEXO              Codigo1
)
GO

/* Tabla PERFIL: PK PERFIL ( Sug. 791 ) */
/* Patch 400 # Seq: 22 Agregado */
alter table PERFIL add constraint PK_PERFIL primary key( PU_CODIGO )
GO

/* Tabla PERFIL: FK hacia PUESTO ( Sug. 791 ) */
/* Patch 400 # Seq: 23 Agregado */
alter table PERFIL
      add constraint FK_PERFIL_PUESTO
      foreign key( PU_CODIGO )
      references PUESTO
      on delete CASCADE
      on update CASCADE
GO

/* Tabla DESC_PTO: Descripci�n de Puesto ( Sug. 791 ) */
/* Patch 400 # Seq: 24 Agregado */
create table DESC_PTO(
       PU_CODIGO            Codigo,
       DT_CODIGO            Codigo,
       DP_ORDEN             FolioChico,
       DP_TEXT_01           Formula,
       DP_TEXT_02           Formula,
       DP_TEXT_03           Formula,
       DP_MEMO_01           Memo,
       DP_MEMO_02           Memo,
       DP_MEMO_03           Memo,
       DP_NUME_01           Pesos,
       DP_FECH_01           Fecha
)
GO

/* Tabla DESC_PTO: PK DESC_PTO ( Sug. 791 ) */
/* Patch 400 # Seq: 25 Agregado */
alter table DESC_PTO add constraint PK_DESC_PTO primary key( PU_CODIGO, DT_CODIGO, DP_ORDEN )
GO

/* Tabla DESC_PTO: FK hacia DESCTIPO ( Sug. 791 ) */
/* Patch 400 # Seq: 26 Agregado */
alter table DESC_PTO
      add constraint FK_DESC_PTO_DESCTIPO
      foreign key( DT_CODIGO )
      references DESCTIPO
      on delete CASCADE
      on update CASCADE
GO

/* Tabla DESC_PTO: FK hacia PERFIL ( Sug. 791 ) */
/* Patch 400 # Seq: 27 Agregado */
alter table DESC_PTO
      add constraint FK_DESC_PTO_PERFIL
      foreign key( PU_CODIGO )
      references PERFIL
      on delete CASCADE
      on update CASCADE
GO

/* Tabla DESC_FLD: Campos de Secci�n( Sug. 791 ) */
/* Patch 400 # Seq: 28 Agregado */
create table DESC_FLD (
       DT_CODIGO            Codigo,
       DF_ORDEN             FolioChico,
       DF_TITULO            DescLarga,
       DF_CAMPO             NombreCampo,
       DF_CONTROL           Status,
       DF_LIMITE            FolioGrande,
       DF_VALORES           Memo
)
GO

/* Tabla DESC_FLD: PK DESC_FLD ( Sug. 791 ) */
/* Patch 400 # Seq: 29 Agregado */
alter table DESC_FLD add constraint PK_DESC_FLD primary key (DT_CODIGO, DF_ORDEN)
GO

/* Tabla DESC_FLD: FK hacia DESCTIPO ( Sug. 791 ) */
/* Patch 400 # Seq: 30 Agregado */
alter table DESC_FLD
      add constraint FK_DESC_FLD_DESCTIPO
      foreign key( DT_CODIGO )
      references DESCTIPO
      on delete CASCADE
      on update CASCADE
GO

/* Tabla VACAPLAN: Plan de Vacaciones ( Sug. 792 ) */
/* Patch 400 # Seq: 31 Agregado */
create table VACAPLAN (
       CB_CODIGO            NumeroEmpleado,
       VP_FEC_INI           Fecha,
       VP_FEC_FIN           Fecha,
       VP_DIAS              DiasFrac,
       VP_SOL_COM           Formula,
       VP_SOL_USR           Usuario,
       VP_SOL_FEC           Fecha,
       VP_STATUS            Status,
       VP_AUT_COM           Formula,
       VP_AUT_USR           Usuario,
       VP_AUT_FEC           Fecha,
       VP_NOMYEAR           Anio,
       VP_NOMTIPO           NominaTipo,
       VP_NOMNUME           NominaNumero,
       VP_SAL_ANT           DiasFrac,
       VP_SAL_PRO           DiasFrac
)
GO

/* Tabla VACAPLAN: PK VACAPLAN ( Sug. 792 ) */
/* Patch 400 # Seq: 32 Agregado */
alter table VACAPLAN add constraint PK_VACAPLAN primary key( CB_CODIGO, VP_FEC_INI )
GO

/* Tabla VACAPLAN: FK hacia COLABORA ( Sug. 792 ) */
/* Patch 400 # Seq: 33 Agregado */
alter table VACAPLAN
      add constraint FK_VACAPLAN_COLABORA
      foreign key( CB_CODIGO )
      references COLABORA
      on delete CASCADE
      on update CASCADE
GO

/* Tabla TCTAMOVS: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 114 Agregado */
create table TCTAMOVS(
       TB_CODIGO            Codigo,          
       TB_ELEMENT           DescLarga,       
       TB_INGLES            DescLarga,       
       TB_NUMERO            Pesos,           
       TB_TEXTO             DescLarga,       
       TB_SISTEMA           Booleano         
)
GO

/* Tabla TCTAMOVS: PK TCTAMOVS ( Sug. 759 ) */
/* Patch 400 # Seq: 115 Agregado */
alter table TCTAMOVS add constraint PK_TCTAMOVS primary key( TB_CODIGO )
GO

/* Tabla CTABANCO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 116 Agregado */
create table CTABANCO(
       CT_CODIGO            Codigo,          
       CT_NUM_CTA           DescLarga,       
       CT_NOMBRE            DescLarga,       
       CT_BANCO             DescLarga,       
       CT_NUMERO            Pesos,
       CT_TEXTO             Descripcion,     
       AH_TIPO              Codigo1,         
       CT_STATUS            Status,          
       CT_REP_CHK           FolioChico,      
       CT_REP_LIQ           FolioChico       
)
GO

/* Tabla CTABANCO: PK CTABANCO ( Sug. 759 ) */
/* Patch 400 # Seq: 117 Agregado */
alter table CTABANCO add constraint PK_CTABANCO primary key( CT_CODIGO )
GO

/* Tabla CTABANCO: FK TAHORRO ( Sug. 759 ) */
/* Patch 400 # Seq: 118 Agregado */
alter table CTABANCO
       add constraint FK_CTABANCO_TAHORRO
       foreign key( AH_TIPO )
       references TAHORRO
       on delete CASCADE
       on update CASCADE
GO

/* Tabla CTA_MOVS: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 119 Agregado */
create table CTA_MOVS(
       CM_FOLIO             FolioGrande IDENTITY( 1, 1 ),
       CT_CODIGO            Codigo,
       CM_TIPO              Codigo,
       CM_DEP_RET           Codigo1,
       CM_MONTO             Pesos,
       CM_DESCRIP           Observaciones,
       CM_CHEQUE            FolioGrande,
       CM_FECHA             Fecha,
       CM_BENEFI            Observaciones,
       CM_STATUS            Status,
       CM_TOT_AHO           Pesos,
       CM_INTERES           Pesos,
       CM_SAL_PRE           Pesos,
       CB_CODIGO            NumeroEmpleado,
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PR_REFEREN           Referencia,
       PR_TIPO              Codigo1,
       CM_PRESTA            Status,
       CM_DEPOSIT           as ( case CM_DEP_RET when 'D' then CM_MONTO else 0 end ),
       CM_RETIRO            as ( case CM_DEP_RET when 'D' then 0 else CM_MONTO end )
)
GO

/* Tabla CTA_MOVS: PK CTA_MOVS ( Sug. 759 ) */
/* Patch 400 # Seq: 120 Agregado */
alter table CTA_MOVS add constraint PK_CTA_MOVS primary key( CM_FOLIO )
GO

/* Tabla CTA_MOVS: Indice adicional  ( Sug. 759 ) */
/* Patch 400 # Seq: 121 Agregado */
create index XIE1CTA_MOVS on CTA_MOVS( CT_CODIGO, CM_FECHA, CM_DEP_RET )
GO

/* Tabla CTA_MOVS: FK CTABANCO ( Sug. 759 ) */
/* Patch 400 # Seq: 122 Agregado */
alter table CTA_MOVS
       add constraint FK_CTA_MOVS_CTABANCO
       foreign key( CT_CODIGO )
       references CTABANCO
       on delete CASCADE
       on update CASCADE
GO

/* Tabla REP_EMPS: Reporte total de empleados en ausencia ( Sug. 998 ) */
/* Patch 400 # Seq: 133 Agregado */
create table REP_EMPS(
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado
)
GO

/* Tabla REP_EMPS: PK REP_EMPS ( Sug. 998 ) */
/* Patch 400 # Seq: 134 Agregado */
alter table REP_EMPS add constraint PK_REP_EMPS primary key( US_CODIGO, CB_CODIGO )
GO

/************************************************/
/***** Tablas Modificadas ***********************/
/************************************************/

/* Tabla COLABORA: C�digo de Plaza ( FK PLAZA ) ( Sug. 84 ) */
/* Patch 400 # Seq: 40 Agregado */
alter table COLABORA add CB_PLAZA FolioGrande
GO

/* Tabla COLABORA: Fecha de ultimo cambio de Plaza ( Sug. 84 ) */
/* Patch 400 # Seq: 131 Agregado */
alter table COLABORA add CB_FEC_PLA Fecha;

/* Tabla COLABORA: Vaca. Derecho prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 41 Agregado */
alter table COLABORA add CB_DER_PV DiasFrac
GO

/* Tabla COLABORA: Vaca. Pagado prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 42 Agregado */
alter table COLABORA add CB_V_PRIMA DiasFrac
GO

/* Tabla COLABORA: SubCuenta para P�liza Contable ( Sug. 752 ) */
/* Patch 400 # Seq: 43 Agregado */
alter table COLABORA add CB_SUB_CTA Descripcion
GO

/* Tabla COLABORA: Neto ( Piramidaci�n ) ( Sug. 789 ) */
/* Patch 400 # Seq: 44 Agregado */
alter table COLABORA add CB_NETO Pesos
GO

/* Tabla COLABORA: Correo Electronico ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 45 Agregado */
alter table COLABORA add CB_E_MAIL Formula
GO

/* Tabla COLABORA: Empleado usa Portal ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 46 Agregado */
alter table COLABORA add CB_PORTAL Booleano
GO

/* Tabla COLABORA: Empleado actualizado en Portal ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 47 Agregado */
alter table COLABORA add CB_DNN_OK Booleano
GO

/* Tabla COLABORA: Nombre de usuario para Portal ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 48 Agregado */
alter table COLABORA add CB_USRNAME DescLarga
GO

/* Tabla COLABORA: Inicializar C�digo de Plaza ( Sug. 84 ) */
/* Patch 400 # Seq: 49 Agregado */
update COLABORA set CB_PLAZA = 0 where ( CB_PLAZA is NULL )
GO

/* Tabla COLABORA: Inicializar Ultimo cambio de Plaza ( Sug. 84 ) */
/* Patch 400 # Seq: 132 Agregado */
update COLABORA set CB_FEC_PLA = '12/30/1899' where ( CB_FEC_PLA is NULL );

/* Tabla COLABORA: Inicializar Vaca. Derecho prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 50 Agregado */
update COLABORA set CB_DER_PV = 0 where ( CB_DER_PV is NULL )
GO

/* Tabla COLABORA: Inicializar Vaca. Pagado prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 51 Agregado */
update COLABORA set CB_V_PRIMA = 0 where ( CB_V_PRIMA is NULL )
GO

/* Tabla COLABORA: Inicializar SubCuenta para P�liza Contable ( Sug. 752 ) */
/* Patch 400 # Seq: 52 Agregado */
update COLABORA set CB_SUB_CTA = '' where ( CB_SUB_CTA is NULL )
GO

/* Tabla COLABORA: Inicializar Neto ( Piramidaci�n ) ( Sug. 789 ) */
/* Patch 400 # Seq: 53 Agregado */
update COLABORA set CB_NETO = 0 where ( CB_NETO is NULL )
GO

/* Tabla COLABORA: Inicializar Correo Electronico ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 54 Agregado */
update COLABORA set CB_E_MAIL = '' where ( CB_E_MAIL is NULL )
GO

/* Tabla COLABORA: Inicializar Empleado usa Portal ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 55 Agregado */
update COLABORA set CB_PORTAL = 'N' where ( CB_PORTAL is NULL )
GO

/* Tabla COLABORA: Inicializar Empleado actualizado en Portal ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 56 Agregado */
update COLABORA set CB_DNN_OK = 'S' where ( CB_DNN_OK is NULL )
GO

/* Tabla COLABORA: Inicializar Nombre de usuario para Portal ( Proyecto PORTAL2 ) */
/* Patch 400 # Seq: 57 Agregado */
update COLABORA set CB_USRNAME = '' where ( CB_USRNAME is NULL )
GO

/* Tabla KARDEX: C�digo de Plaza ( FK PLAZA ) ( Sug. 84 ) */
/* Patch 400 # Seq: 58 Agregado */
alter table KARDEX add CB_PLAZA FolioGrande
GO

/* Tabla KARDEX: Inicializar C�digo de Plaza ( Sug. 84 ) */
/* Patch 400 # Seq: 59 Agregado */
update KARDEX set CB_PLAZA = 0 where ( CB_PLAZA is NULL )
GO

/* Tabla VACACION: D�as derecho prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 60 Agregado */
alter table VACACION add VA_D_PRIMA DiasFrac
GO

/* Tabla VACACION: D�as pagados prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 61 Agregado */
alter table VACACION add VA_P_PRIMA DiasFrac
GO

/* Tabla VACACION: Saldo de d�as prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 62 Agregado */
alter table VACACION add VA_S_PRIMA DiasFrac
GO

/* Tabla VACACION: Inicializar D�as derecho prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 63 Agregado */
update VACACION set VA_D_PRIMA = 0 where ( VA_D_PRIMA is NULL )
GO

/* Tabla VACACION: Inicializar D�as pagados prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 64 Agregado */
update VACACION set VA_P_PRIMA = 0 where ( VA_P_PRIMA is NULL )
GO

/* Tabla VACACION: Inicializar Saldo de d�as prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 65 Agregado */
update VACACION set VA_S_PRIMA = 0 where ( VA_S_PRIMA is NULL )
GO

/* Tabla PERMISO: � Movimiento Global ? ( Sug. 627 ) */
/* Patch 400 # Seq: 66 Agregado */
alter table PERMISO add PM_GLOBAL Booleano
GO

/* Tabla PERMISO: Inicializar � Movimiento Global ? ( Sug. 627 ) */
/* Patch 400 # Seq: 67 Agregado */
update PERMISO set PM_GLOBAL = 'N' where ( PM_GLOBAL is NULL )
GO

/* Tabla INCAPACI: Fecha Inicio para SUA ( Sug. 768 ) */
/* Patch 400 # Seq: 68 Agregado */
alter table INCAPACI add IN_SUA_INI Fecha
GO

/* Tabla INCAPACI: Fecha Final para SUA ( Sug. 768 ) */
/* Patch 400 # Seq: 69 Agregado */
alter table INCAPACI add IN_SUA_FIN Fecha
GO

/* Tabla INCAPACI: Inicializar Fecha Inicio para SUA ( Sug. 768 ) */
/* Patch 400 # Seq: 70 Agregado */
update INCAPACI set IN_SUA1INI = IN_FEC_INI
where ( IN_SUA_INI is NULL ) or ( IN_SUA_INI = '12/30/1899' )
GO

/* Tabla INCAPACI: Inicializar Fecha Final para SUA ( Sug. 768 ) */
/* Patch 400 # Seq: 71 Agregado */
update INCAPACI set IN_SUA_FIN = IN_FEC_FIN
where ( IN_SUA_FIN is NULL ) or ( IN_SUA_FIN = '12/30/1899' )
GO

/* Tabla NOMINA: Fecha Liquidacion ( Sug. 779 ) */
/* Patch 400 # Seq: 72 Agregado */
alter table NOMINA add NO_FEC_LIQ Fecha
GO

/* Tabla NOMINA: Inicializar Fecha Liquidacion ( Sug. 779 ) */
/* Patch 400 # Seq: 73 Agregado */
update NOMINA set NO_FEC_LIQ = '12/30/1899' where ( NO_FEC_LIQ is NULL )
GO

/* Tabla NOMINA: C�digo de Plaza ( FK PLAZA ) ( Sug. 84 ) */
/* Patch 400 # Seq: 74 Agregado */
alter table NOMINA add CB_PLAZA FolioGrande
GO

/* Tabla NOMINA: Faltas vacaciones valor jornada ( Sug. 317 ) */
/* Patch 400 # Seq: 75 Agregado */
alter table NOMINA add NO_DIAS_VJ DiasFrac
GO

/* Tabla NOMINA: Parte gravada de previsi�n social ( Sug. 503 ) */
/* Patch 400 # Seq: 76 Agregado */
alter table NOMINA add NO_PREV_GR Pesos
GO

/* Tabla NOMINA: Dias prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 77 Agregado */
alter table NOMINA add NO_DIAS_PV DiasFrac
GO

/* Tabla NOMINA: Inicializar C�digo de Plaza ( Sug. 84 ) */
/* Patch 400 # Seq: 78 Agregado */
update NOMINA set CB_PLAZA = 0 where ( CB_PLAZA is NULL )
GO

/* Tabla NOMINA: Inicializar Faltas vacaciones ( Sug. 317 ) */
/* Patch 400 # Seq: 79 Agregado */
update NOMINA set NO_DIAS_VJ = 0 where ( NO_DIAS_VJ is NULL )
GO

/* Tabla NOMINA: Inicializar Parte gravada de previsi�n social ( Sug. 503 ) */
/* Patch 400 # Seq: 80 Agregado */
update NOMINA set NO_PREV_GR = 0 where ( NO_PREV_GR is NULL )
GO

/* Tabla NOMINA: Inicializar Dias prima vacacional ( Sug. 530 ) */
/* Patch 400 # Seq: 81 Agregado */
update NOMINA set NO_DIAS_PV = 0 where ( NO_DIAS_PV is NULL )
GO

/* Tabla AUSENCIA: Status Festivo ( Sug. 488 ) */
/* Patch 400 # Seq: 82 Agregado */
alter table AUSENCIA add AU_STA_FES Status
GO

/* Tabla AUSENCIA: Inicializar Status Festivo ( Sug. 488 ) */
/* Patch 400 # Seq: 83 Agregado */
update AUSENCIA set AU_STA_FES = 0 where ( AU_STA_FES is NULL )
GO

/* Tabla AHORRO: SubCuenta para P�liza Contable ( Sug. 752 ) */
/* Patch 400 # Seq: 84 Agregado */
alter table AHORRO add AH_SUB_CTA Descripcion
GO

/* Tabla AHORRO: Inicializar SubCuenta para P�liza Contable ( Sug. 752 ) */
/* Patch 400 # Seq: 85 Agregado */
update AHORRO set AH_SUB_CTA = '' where ( AH_SUB_CTA is NULL )
GO

/* Tabla PRESTAMO: SubCuenta para P�liza Contable ( Sug. 752 ) */
/* Patch 400 # Seq: 86 Agregado */
alter table PRESTAMO add PR_SUB_CTA Descripcion
GO

/* Tabla PRESTAMO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 87 Agregado */
alter table PRESTAMO add PR_MONTO_S Pesos
GO

/* Tabla PRESTAMO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 88 Agregado */
alter table PRESTAMO add PR_INTERES Pesos
GO

/* Tabla PRESTAMO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 89 Agregado */
alter table PRESTAMO add PR_TASA Tasa
GO

/* Tabla PRESTAMO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 90 Agregado */
alter table PRESTAMO add PR_MESES FolioChico
GO

/* Tabla PRESTAMO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 91 Agregado */
alter table PRESTAMO add PR_PAGOS FolioChico
GO

/* Tabla PRESTAMO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 92 Agregado */
alter table PRESTAMO add PR_PAG_PER Pesos
GO

/* Tabla PRESTAMO: Inicializar SubCuenta para P�liza Contable ( Sug. 752 ) */
/* Patch 400 # Seq: 93 Agregado */
update PRESTAMO set PR_SUB_CTA = '' where ( PR_SUB_CTA is NULL )
GO

/* Tabla PRESTAMO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 94 Agregado */
update PRESTAMO set PR_MONTO_S = 0 where ( PR_MONTO_S is NULL )
GO

/* Tabla PRESTAMO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 95 Agregado */
update PRESTAMO set PR_INTERES = 0 where ( PR_INTERES is NULL )
GO

/* Tabla PRESTAMO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 96 Agregado */
update PRESTAMO set PR_TASA = 0 where ( PR_TASA is NULL )
GO

/* Tabla PRESTAMO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 97 Agregado */
update PRESTAMO set PR_MESES = 0 where ( PR_MESES is NULL )
GO

/* Tabla PRESTAMO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 98 Agregado */
update PRESTAMO set PR_PAGOS = 0 where ( PR_PAGOS is NULL )
GO

/* Tabla PRESTAMO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 99 Agregado */
update PRESTAMO set PR_PAG_PER = 0 where ( PR_PAG_PER is NULL )
GO

/* Tabla TAHORRO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 100 Agregado */
alter table TAHORRO add TB_PRESTA Codigo1
GO

/* Tabla TAHORRO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 101 Agregado */
alter table TAHORRO add TB_TASA1 Tasa
GO

/* Tabla TAHORRO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 102 Agregado */
alter table TAHORRO add TB_TASA2 Tasa
GO

/* Tabla TAHORRO: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 103 Agregado */
alter table TAHORRO add TB_TASA3 Tasa
GO

/* Tabla TAHORRO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 104 Agregado */
update TAHORRO set TB_PRESTA = '' where ( TB_PRESTA is NULL )
GO

/* Tabla TAHORRO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 105 Agregado */
update TAHORRO set TB_TASA1 = 0 where ( TB_TASA1 is NULL )
GO

/* Tabla TAHORRO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 106 Agregado */
update TAHORRO set TB_TASA2 = 0 where ( TB_TASA2 is NULL )
GO

/* Tabla TAHORRO: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 107 Agregado */
update TAHORRO set TB_TASA3 = 0 where ( TB_TASA3 is NULL )
GO

/* Tabla TPRESTA: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 108 Agregado */
alter table TPRESTA add TB_TASA1 Tasa
GO

/* Tabla TPRESTA: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 109 Agregado */
alter table TPRESTA add TB_TASA2 Tasa
GO

/* Tabla TPRESTA: ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 110 Agregado */
alter table TPRESTA add TB_TASA3 Tasa
GO

/* Tabla TPRESTA: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 111 Agregado */
update TPRESTA set TB_TASA1 = 0 where ( TB_TASA1 is NULL )
GO

/* Tabla TPRESTA: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 112 Agregado */
update TPRESTA set TB_TASA2 = 0 where ( TB_TASA2 is NULL )
GO

/* Tabla TPRESTA: Inicializar ?? ( Sug. 759 ) */
/* Patch 400 # Seq: 113 Agregado */
update TPRESTA set TB_TASA3 = 0 where ( TB_TASA3 is NULL )
GO

/* Tabla TFIJAS: Aumentar ancho de descripcion ( Sug. ??? ) */
/* Patch 400 # Seq: 124 Agregado */
exec SP_UNBINDEFAULT 'TFIJAS.TF_DESCRIP'
alter table TFIJAS drop column TF_DESCRIP;

/* Tabla TFIJAS: Aumentar ancho de descripcion ( Sug. ??? ) */
/* Patch 400 # Seq: 125 Agregado */
alter table TFIJAS add TF_DESCRIP Observaciones;

/* Tabla TMPNOM: Agregar  ( Sugs. 503 / 530 ) */
/* Patch 400 # Seq: 127 Agregado */
alter table TMPNOM add NO_HOR_OK Hora;

/* Tabla TMPNOM: Agregar  ( Sugs. 503 / 530 ) */
/* Patch 400 # Seq: 128 Agregado */
alter table TMPNOM add NO_PREV_GR Pesos;

/* Tabla TMPNOM: Agregar  ( Sugs. 503 / 530 ) */
/* Patch 400 # Seq: 129 Agregado */
alter table TMPNOM add NO_DIAS_VJ DiasFrac;

/* Tabla TMPNOM: Agregar  ( Sugs. 503 / 530 ) */
/* Patch 400 # Seq: 130 Agregado */
alter table TMPNOM add NO_DIAS_PV DiasFrac;

/* Tabla PLAZAS: Inicializar datos (1/3) ( Sug. 84 ) */
/* Patch 400 # Seq: 140 Agregado */
create procedure PLAZAS_INIT_DATOS
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from TKARDEX where
     ( TB_CODIGO = 'PLAZA' ) or ( TB_CODIGO = 'JEFE' );
     if ( @Cuantos = 0 )
     begin
          update TKARDEX set TB_NIVEL = TB_NIVEL + 2 where ( TB_NIVEL >= 3 );
          insert into TKARDEX( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NIVEL, TB_SISTEMA )
          values( 'PLAZA', 'Cambio de Plaza', 'Occupation change', 3, 'S' );
          insert into TKARDEX( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NIVEL, TB_SISTEMA )
          values( 'JEFE', 'Cambio de Jefe', 'Supervisor change', 4, 'S' );
          insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) values ( 196, '� Usar mecanismo de Plazas ?', 'N' );
     end
end
GO

/* Tabla PLAZAS: Inicializar datos (2/3) ( Sug. 84 ) */
/* Patch 400 # Seq: 141 Agregado */
exec PLAZAS_INIT_DATOS
GO

/* Tabla PLAZAS: Inicializar datos (2/3) ( Sug. 84 ) */
/* Patch 400 # Seq: 142 Agregado */
drop procedure PLAZAS_INIT_DATOS
GO

/* Tabla POLIZA: Cambiar Llave primaria (01/15) ( Sug. 738 ) */
/*
Nota: Si hubiera proyectos especiales que le agregaron campos a esta tabla,
considerarlo, porque al hacer un DROP se perder�an
*/
/* Patch 400 # Seq: 150 Agregado */
create table OLD_POLIZA (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       TP_FOLIO             FolioChico,
       TP_CUENTA            Descripcion,
       TP_CARGO             PesosEmpresa,
       TP_ABONO             PesosEmpresa,
       TP_COMENTA           Descripcion,
       TP_CAR_ABO           Codigo1,
       TP_TEXTO             NombreCampo,
       TP_NUMERO            Status
)

/* Tabla POLIZA: Buffer para proteger corridas posteriores de este patch (02/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 151 Agregado */
create table OLD_POLIZA2(
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PT_CODIGO            Codigo,
       TP_FOLIO             FolioChico,
       TP_CUENTA            Descripcion,
       TP_CARGO             PesosEmpresa,
       TP_ABONO             PesosEmpresa,
       TP_COMENTA           Descripcion,
       TP_CAR_ABO           Codigo1,
       TP_TEXTO             NombreCampo,
       TP_NUMERO            Status
)

/* Tabla POLIZA: Cambiar Llave primaria (03/15) ( Sug. 738 ) */
/*
Agregar POLIZA.PT_CODIGO para que la 1a. vez se pueda
compilar POLIZA_GUARDAR
La 2a. vez o posteriores, este comando debe fallar debido
a que POLIZA.PT_CODIGO ya existe
*/
/* Patch 400 # Seq: 152 Agregado */
alter table POLIZA add PT_CODIGO Codigo
GO

/* Tabla POLIZA: Cambiar Llave primaria (04/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 153 Agregado */
create procedure POLIZA_GUARDAR
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from POL_TIPO where ( PT_CODIGO = 'POLDEF' );
     if ( @Cuantos = 0 )
     begin
          insert into OLD_POLIZA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  TP_FOLIO,
                                  TP_CUENTA,
                                  TP_CARGO,
                                  TP_ABONO,
                                  TP_COMENTA,
                                  TP_CAR_ABO,
                                  TP_TEXTO,
                                  TP_NUMERO )
          select PE_YEAR,
                 PE_TIPO,
                 PE_NUMERO,
                 TP_FOLIO,
                 TP_CUENTA,
                 TP_CARGO,
                 TP_ABONO,
                 TP_COMENTA,
                 TP_CAR_ABO,
                 ISNULL( TP_TEXTO, '' ),
                 ISNULL( TP_NUMERO, 0 )
          from POLIZA;
     end
     else
     begin
          insert into OLD_POLIZA2( PE_YEAR,
                                   PE_TIPO,
                                   PE_NUMERO,
                                   PT_CODIGO,
                                   TP_FOLIO,
                                   TP_CUENTA,
                                   TP_CARGO,
                                   TP_ABONO,
                                   TP_COMENTA,
                                   TP_CAR_ABO,
                                   TP_TEXTO,
                                   TP_NUMERO )
          select PE_YEAR,
                 PE_TIPO,
                 PE_NUMERO,
                 PT_CODIGO,
                 TP_FOLIO,
                 TP_CUENTA,
                 TP_CARGO,
                 TP_ABONO,
                 TP_COMENTA,
                 TP_CAR_ABO,
                 ISNULL( TP_TEXTO, '' ),
                 ISNULL( TP_NUMERO, 0 )
          from POLIZA;
     end
end
GO

/* Tabla POLIZA: Cambiar Llave primaria (05/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 154 Agregado */
exec POLIZA_GUARDAR
GO

/* Tabla POLIZA: Cambiar Llave primaria (06/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 155 Agregado */
drop procedure POLIZA_GUARDAR
GO

/* Tabla POLIZA: Cambiar Llave primaria (07/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 156 Agregado */
drop table POLIZA
GO

/* Tabla POLIZA: Cambiar Llave primaria (08/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 157 Agregado */
create table POLIZA (
       PE_YEAR              Anio,
       PE_TIPO              NominaTipo,
       PE_NUMERO            NominaNumero,
       PT_CODIGO            Codigo,            /* Tipo de P�liza ( FK POL_TIPO ) */
       TP_FOLIO             FolioChico,
       TP_CUENTA            Descripcion,
       TP_CARGO             PesosEmpresa,
       TP_ABONO             PesosEmpresa,
       TP_COMENTA           Descripcion,
       TP_CAR_ABO           Codigo1,
       TP_TEXTO             NombreCampo,
       TP_NUMERO            Status
)
GO

/* Tabla POLIZA: Cambiar Llave primaria (09/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 158 Agregado */
alter table POLIZA add constraint PK_POLIZA primary key( PE_YEAR, PE_TIPO, PE_NUMERO, PT_CODIGO, TP_FOLIO )
GO

/* Tabla POLIZA: Cambiar Llave primaria (10/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 159 Agregado */
alter table POLIZA
       add constraint FK_POLIZA_POL_HEAD
       foreign key( PE_YEAR, PE_TIPO, PE_NUMERO, PT_CODIGO )
       references POL_HEAD
       on delete CASCADE
       on update CASCADE

/* Tabla POLIZA: Cambiar Llave primaria (11/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 160 Agregado */
create procedure POL_TIPO_INIT
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     declare @ReporteCodigo Varchar(6);
     select @Cuantos = COUNT(*) from POL_TIPO where ( PT_CODIGO = 'POLDEF' );
     if ( @Cuantos = 0 )
     begin
          select @Cuantos = COUNT(*) from GLOBAL where ( GL_CODIGO = 2006 );
          if ( @Cuantos = 0 )
          begin
               insert into GLOBAL( GL_CODIGO, GL_FORMULA ) values ( 2006, '0' );
          end
          else
          begin
               update GLOBAL set GL_FORMULA = '0' where ( GL_CODIGO = 2006 ) and ( GL_FORMULA = '' );
          end
          insert into POL_TIPO( PT_CODIGO, PT_NOMBRE, PT_REPORTE )
          select 'POLDEF', 'P�liza default', GL_FORMULA from GLOBAL where ( GL_CODIGO = 2006 );
          update GLOBAL set GL_FORMULA = 'POLDEF' where ( GL_CODIGO = 2006 );
          insert into POL_HEAD( PE_YEAR,
                                PE_TIPO,
                                PE_NUMERO,
                                PT_CODIGO,
                                US_CODIGO,
                                PH_FECHA,
                                PH_HORA,
                                PH_VECES,
                                PH_REPORTE,
                                PH_STATUS )
          select DISTINCT OP.PE_YEAR,
                          OP.PE_TIPO,
                          OP.PE_NUMERO,
                          PT.PT_CODIGO,
                          1,
                          PE.PE_FEC_PAG,
                          '1200',
                          1,
                          PT.PT_REPORTE,
                          0
          from OLD_POLIZA OP
          join PERIODO PE on ( OP.PE_YEAR = PE.PE_YEAR ) and ( OP.PE_TIPO = PE.PE_TIPO ) and ( OP.PE_NUMERO = PE.PE_NUMERO )
          join POL_TIPO PT on ( PT.PT_CODIGO = 'POLDEF' );
          insert into POLIZA( PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              PT_CODIGO,
                              TP_FOLIO,
                              TP_CUENTA,
                              TP_CARGO,
                              TP_ABONO,
                              TP_COMENTA,
                              TP_CAR_ABO,
                              TP_TEXTO,
                              TP_NUMERO )
          select PE_YEAR,
                 PE_TIPO,
                 PE_NUMERO,
                 'POLDEF',
                 TP_FOLIO,
                 TP_CUENTA,
                 TP_CARGO,
                 TP_ABONO,
                 TP_COMENTA,
                 TP_CAR_ABO,
                 TP_TEXTO,
                 TP_NUMERO from OLD_POLIZA;
          declare TemporalReporte cursor for
          select RE_CODIGO from REPORTE where ( RE_ENTIDAD = 96 ) order by RE_CODIGO;
          open TemporalReporte;
          fetch NEXT from TemporalReporte into @ReporteCodigo;
          while ( @@Fetch_Status = 0 )
          begin
               insert into CAMPOREP( RE_CODIGO,
                                     CR_TIPO,
                                     CR_POSICIO,
                                     CR_SUBPOS,
                                     CR_TABLA,
                                     CR_TITULO,
                                     CR_CALC,
                                     CR_ANCHO,
                                     CR_OPER,
                                     CR_TFIELD,
                                     CR_FORMULA,
                                     CR_REQUIER )
               values( @ReporteCodigo,
                       5,
                       99,
                       -1,
                       96,
                       'Tipo de P�liza',
                       1,
                       3,
                       264,
                       4,
                       'POLIZA.PT_CODIGO',
                       'POLDEF' );
               fetch NEXT from TemporalReporte into @ReporteCodigo;
          end
          close TemporalReporte;
          deallocate TemporalReporte;
     end
     else
     begin
          insert into POLIZA( PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              PT_CODIGO,
                              TP_FOLIO,
                              TP_CUENTA,
                              TP_CARGO,
                              TP_ABONO,
                              TP_COMENTA,
                              TP_CAR_ABO,
                              TP_TEXTO,
                              TP_NUMERO )
          select PE_YEAR,
                 PE_TIPO,
                 PE_NUMERO,
                 PT_CODIGO,
                 TP_FOLIO,
                 TP_CUENTA,
                 TP_CARGO,
                 TP_ABONO,
                 TP_COMENTA,
                 TP_CAR_ABO,
                 TP_TEXTO,
                 TP_NUMERO from OLD_POLIZA2;
     end
end
GO

/* Tabla POLIZA: Cambiar Llave primaria (12/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 161 Agregado */
exec POL_TIPO_INIT
GO

/* Tabla POLIZA: Cambiar Llave primaria (13/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 162 Agregado */
drop procedure POL_TIPO_INIT
GO

/* Tabla POLIZA: Cambiar Llave primaria (14/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 163 Agregado */
drop table OLD_POLIZA
GO

/* Tabla POLIZA: Cambiar Llave primaria (15/15) ( Sug. 738 ) */
/* Patch 400 # Seq: 164 Agregado */
drop table OLD_POLIZA2
GO

/* AGREGA_CONCEPTO_SISTEMA: Agrega un concepto de sistema cuando no existe ( Sugerencia 503 / 530 ) */
/* Patch 400 # Seq: 170 Agregado */
create procedure AGREGA_CONCEPTO_SISTEMA( @Concepto Smallint, @Descripcion VARCHAR(30) )
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = @Concepto );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( @Concepto, @Descripcion, 'S' );
          if ( @Concepto = 1011 ) then
          begin
               update CONCEPTO set CO_G_ISPT = 'N' where CO_TIPO = 4;
          end
     end
end
GO

/* AGREGA_CONCEPTOS_SISTEMA: Agregar conceptos sistema nuevos 1011,1121,1122,1123 ( Sugerencia 503 / 530 ) */
/* Patch 400 # Seq: 171 Agregado */
create procedure AGREGA_CONCEPTOS_SISTEMA
as
begin
     SET NOCOUNT ON;
     execute AGREGA_CONCEPTO_SISTEMA 1011, 'Previsi�n Gravada';
     execute AGREGA_CONCEPTO_SISTEMA 1121, 'D�as prima vacacional';
     execute AGREGA_CONCEPTO_SISTEMA 1122, 'Faltas vac/jornada sin 7o D�a';
     execute AGREGA_CONCEPTO_SISTEMA 1123, 'Faltas vac/jornada con 7o D�a';
end
GO

/* AGREGA_CONCEPTOS_SISTEMA: Ejecutar para agregar conceptos sistema nuevos 11011,1121,1122,1123 ( Sugerencia 503 / 530 ) */
/* Patch 400 # Seq: 172 Agregado */
execute AGREGA_CONCEPTOS_SISTEMA
GO

/* AGREGA_CONCEPTOS_SISTEMA: Borrar ( Sugerencia 503 / 530 ) */
/* Patch 400 # Seq: 173 Agregado */
drop procedure AGREGA_CONCEPTOS_SISTEMA
GO

/* AGREGA_CONCEPTO_SISTEMA: Borrar ( Sugerencia 503 / 530 ) */
/* Patch 400 # Seq: 174 Agregado */
drop procedure AGREGA_CONCEPTO_SISTEMA
GO

/* GLOBALES: Inicializar (1/5) */
/* Patch 400 # Seq: 200 Agregado */
create procedure GLOBAL_AGREGAR( @Global INTEGER, @Descripcion VARCHAR(30), @Formula VARCHAR(255) )
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from GLOBAL where ( GL_CODIGO = @Global );
     if ( @Cuantos = 0 )
     begin
          insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) values ( @Global, @Descripcion, @Formula );
     end
end
GO

/* GLOBALES: Inicializar (2/5) */
/* Patch 400 # Seq: 201 Agregado */
create procedure GLOBAL_INIT
as
begin
     SET NOCOUNT ON;
     exec GLOBAL_AGREGAR 194, 'Tolerancia para piramidaci�n', '0.50';
     exec GLOBAL_AGREGAR 202, 'Simulaci�n de Finiquitos', '990';
end
GO

/* GLOBALES: Inicializar (3/5) */
/* Patch 400 # Seq: 202 Agregado */
exec GLOBAL_INIT
GO

/* GLOBALES: Inicializar (4/5) */
/* Patch 400 # Seq: 203 Agregado */
drop procedure GLOBAL_INIT
GO

/* GLOBALES: Inicializar (5/5) */
/* Patch 400 # Seq: 204 Agregado */
drop procedure GLOBAL_AGREGAR
GO

/*****************************************/
/**** Views ******************************/
/*****************************************/

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/* Trigger Actualizaci�n TU_PLAZA ( Sug 84 ) */
/* Patch 400 # Seq: 86 Agregado */
CREATE TRIGGER TU_PLAZA ON PLAZA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( PL_FOLIO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;
          select @NewCodigo = PL_FOLIO from Inserted;
          select @OldCodigo = PL_FOLIO from Deleted;
          update COLABORA set CB_PLAZA = @NewCodigo where ( CB_PLAZA = @OldCodigo );
          update KARDEX	set CB_PLAZA = @NewCodigo where ( CB_PLAZA = @OldCodigo );
          update NOMINA	set CB_PLAZA = @NewCodigo where ( CB_PLAZA = @OldCodigo );
     END
END
GO

/* Trigger Actualizaci�n TU_CTA_MOVS ( Sug. 759 )*/
/* Patch 400 # Seq: 87 Agregado */
CREATE TRIGGER TU_TCTAMOVS ON TCTAMOVS AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
	      declare @NewCodigo Codigo, @OldCodigo Codigo;
	      select @NewCodigo = TB_CODIGO from Inserted;
          select @OldCodigo = TB_CODIGO from Deleted;
	      update CTA_MOVS set CM_TIPO = @NewCodigo
          where ( CM_TIPO = @OldCodigo );
     END
END
GO

/* Trigger TU_TPRESTA */
/* Sug 759: Considerar TAHORRO.TB_PRESTA */
/* Patch 400 # Seq: 88 Agregado */
CREATE TRIGGER TU_TPRESTA ON TPRESTA AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF UPDATE( TB_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;
	      select @NewCodigo = TB_CODIGO from Inserted;
          select @OldCodigo = TB_CODIGO from Deleted;
	      update TAHORRO set TB_PRESTA = @NewCodigo
          where ( TB_PRESTA = @OldCodigo );
     END
END
GO

/* Trigger TD_COLABORA */
/* Sug 84: Considerar PLAZA.CB_CODIGO */
/* Patch 400 # Seq: 45 Modificado */
ALTER TRIGGER TD_COLABORA ON COLABORA AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  declare @OldCodigo NumeroEmpleado;
  select @OldCodigo = CB_CODIGO from Deleted;
  delete NOMINA	from NOMINA where ( NOMINA.CB_CODIGO = @OldCodigo ) and ( NOMINA.NO_STATUS <= 4 )
  if ( @OldCodigo > 0 )
  begin
       delete from EXPEDIEN where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
       update PLAZA set PLAZA.CB_CODIGO = 0 where ( PLAZA.CB_CODIGO = @OldCodigo );
  end
END
GO

/* Trigger TU_COLABORA */
/* Sug 84: Considerar PLAZA.CB_CODIGO */
/* Patch 400 # Seq: 46 Modificado */
ALTER TRIGGER TU_COLABORA ON COLABORA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  declare @NewCodigo NumeroEmpleado, @OldCodigo NumeroEmpleado;
  IF UPDATE( CB_CODIGO )
  BEGIN
	select @NewCodigo = CB_CODIGO from Inserted;
        select @OldCodigo = CB_CODIGO from Deleted;

	UPDATE NOMINA	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE LIQ_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE WORKS	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE CED_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
        if ( @OldCodigo > 0 )
        begin
             update EXPEDIEN set EXPEDIEN.CB_CODIGO = @NewCodigo
             where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
             update PLAZA set PLAZA.CB_CODIGO = @NewCodigo
             where ( PLAZA.CB_CODIGO = @OldCodigo );
        end
  END
  IF UPDATE( CB_PLAZA )
  BEGIN
       declare @NewPlaza FolioGrande;
       declare @OldPLaza FolioGrande;
       select @NewPlaza = CB_PLAZA from Inserted;
       select @OldPLaza = CB_PLAZA from Deleted;
       select @NewCodigo = CB_CODIGO from Inserted;
       update PLAZA set CB_CODIGO = 0 where ( PL_FOLIO = @OldPLaza ) and ( CB_CODIGO = @NewCodigo );
       update PLAZA set CB_CODIGO = @NewCodigo where ( PL_FOLIO = @NewPlaza );
  END
END
GO

/* Trigger TU_TURNO */
/* Sug 84: Considerar PLAZA.PL_TURNO */
/* Patch 400 # Seq: 31 Modificado */
ALTER TRIGGER TU_TURNO ON TURNO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TU_CODIGO from Inserted;
    select @OldCodigo = TU_CODIGO from Deleted;

	UPDATE COLABORA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARDEX	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE NOMINA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARCURSO	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE PUESTO	SET PU_TURNO = @NewCodigo WHERE PU_TURNO = @OldCodigo;
	UPDATE CLASITMP	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
    UPDATE PLAZA    SET PL_TURNO = @NewCodigo WHERE PL_TURNO = @OldCodigo;
  END
END
GO

/* Trigger TU_CLASIFI */
/* Sug 84: Considerar PLAZA.PL_CLASIFI */
/* Patch 400 # Seq: 20 Modificado */
ALTER TRIGGER TU_CLASIFI ON CLASIFI AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
    select @OldCodigo = TB_CODIGO from Deleted;

	UPDATE PUESTO   SET PU_CLASIFI = @NewCodigo WHERE PU_CLASIFI = @OldCodigo;
	UPDATE COLABORA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE KARDEX	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE NOMINA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE AUSENCIA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE KARCURSO	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE CLASITMP	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
    UPDATE PLAZA    SET PL_CLASIFI = @NewCodigo where PL_CLASIFI = @OldCodigo;

  END
END
GO

/* Trigger TU_RPATRON */
/* Sug 84: Considerar PLAZA.PL_PATRON */
/* Patch 400 # Seq: 33 Modificado */
ALTER TRIGGER TU_RPATRON ON RPATRON AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
    select @OldCodigo = TB_CODIGO from Deleted;

	UPDATE LIQ_IMSS	SET LS_PATRON = @NewCodigo WHERE LS_PATRON = @OldCodigo;
	UPDATE COLABORA	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE KARDEX	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE NOMINA	SET CB_PATRON = @NewCodigo WHERE CB_PATRON = @OldCodigo;
	UPDATE PUESTO	SET PU_PATRON = @NewCodigo WHERE PU_PATRON = @OldCodigo;
    UPDATE PLAZA    SET PL_PATRON = @NewCodigo WHERE PL_PATRON = @OldCodigo;
  END
END
GO

/* Trigger TU_SSOCIAL */
/* Sug 84: Considerar PLAZA.PL_PATRON */
/* Patch 400 # Seq: 32 Modificado */
ALTER TRIGGER TU_SSOCIAL ON SSOCIAL AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
	select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE KARDEX	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE VACACION	SET CB_TABLASS = @NewCodigo WHERE CB_TABLASS = @OldCodigo;
	UPDATE PUESTO	SET PU_TABLASS = @NewCodigo WHERE PU_TABLASS = @OldCodigo;
	/* Patch 400: Considerar PLAZA.PU_TABLASS*/
        UPDATE PLAZA    set PL_TABLASS = @NewCodigo WHERE PL_TABLASS = @OldCodigo;
  END
END
GO

/* Trigger TU_CONTRATO */
/* Sug 84: Considerar PLAZA.PL_CONTRAT */
/* Patch 400 # Seq: 21 Modificado */
ALTER TRIGGER TU_CONTRATO ON CONTRATO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from Inserted;
    select @OldCodigo = TB_CODIGO from Deleted;

	UPDATE COLABORA	SET CB_CONTRAT = @NewCodigo WHERE CB_CONTRAT = @OldCodigo;
	UPDATE KARDEX	SET CB_CONTRAT = @NewCodigo WHERE CB_CONTRAT = @OldCodigo;
	UPDATE PUESTO	SET PU_CONTRAT = @NewCodigo WHERE PU_CONTRAT = @OldCodigo;
    UPDATE PLAZA    SET PL_CONTRAT = @NewCodigo WHERE PL_CONTRAT = @OldCodigo;
  END
END
GO

/* Trigger TU_AREA */
/* Sug 84: Considerar PLAZA.TU_AREA */
/* Patch 400 # Seq: 47 Modificado */
ALTER TRIGGER TU_AREA ON AREA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo CODIGO, @OldCodigo CODIGO;

	select @NewCodigo = TB_CODIGO from Inserted;
    select @OldCodigo = TB_CODIGO from Deleted;

    update SUP_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update KAR_AREA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update COLABORA set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
	update WORKS    set CB_AREA = @NewCodigo where ( CB_AREA = @OldCodigo );
    update CEDULA   set CE_AREA = @NewCodigo where ( CE_AREA = @OldCodigo );
    update CED_INSP set CI_AREA = @NewCodigo where ( CI_AREA = @OldCodigo );
	update CEDSCRAP set CS_AREA = @NewCodigo where ( CS_AREA = @OldCodigo );
    update PLAZA    set PL_AREA = @NewCodigo where ( PL_AREA = @OldCodigo );
  END
END
GO

/* Trigger TU_NIVEL1 */
/* Sug 84: Considerar PLAZA.PL_NIVEL1 */
/* Patch 400 # Seq: 1 Modificado */
ALTER TRIGGER TU_NIVEL1 ON NIVEL1 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL1 = @NewCodigo WHERE PU_NIVEL1 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL1 = @NewCodigo WHERE PL_NIVEL1 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL2 */
/* Sug 84: Considerar PLAZA.PL_NIVEL2 */
/* Patch 400 # Seq: 2 Modificado */
ALTER TRIGGER TU_NIVEL2 ON NIVEL2 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL2 = @NewCodigo WHERE PU_NIVEL2 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL2 = @NewCodigo WHERE PL_NIVEL2 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL3 */
/* Sug 84: Considerar PLAZA.PL_NIVEL3 */
/* Patch 400 # Seq: 3 Modificado */
ALTER TRIGGER TU_NIVEL3 ON NIVEL3 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL3 = @NewCodigo WHERE PU_NIVEL3 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL3 = @NewCodigo WHERE PL_NIVEL3 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL4 */
/* Sug 84: Considerar PLAZA.PL_NIVEL4 */
/* Patch 400 # Seq: 4 Modificado */
ALTER TRIGGER TU_NIVEL4 ON NIVEL4 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL4 = @NewCodigo WHERE PU_NIVEL4 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL4 = @NewCodigo WHERE PL_NIVEL4 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL5 */
/* Sug 84: Considerar PLAZA.PL_NIVEL5 */
/* Patch 400 # Seq: 5 Modificado */
ALTER TRIGGER TU_NIVEL5 ON NIVEL5 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL5 = @NewCodigo WHERE PU_NIVEL5 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL5 = @NewCodigo WHERE PL_NIVEL5 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL6 */
/* Sug 84: Considerar PLAZA.PL_NIVEL6 */
/* Patch 400 # Seq: 6 Modificado */
ALTER TRIGGER TU_NIVEL6 ON NIVEL6 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL6 = @NewCodigo WHERE PU_NIVEL6 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL6 = @NewCodigo WHERE PL_NIVEL6 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL7 */
/* Sug 84: Considerar PLAZA.PL_NIVEL7 */
/* Patch 400 # Seq: 7 Modificado */
ALTER TRIGGER TU_NIVEL7 ON NIVEL7 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL7 = @NewCodigo WHERE PU_NIVEL7 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL7 = @NewCodigo WHERE PL_NIVEL7 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL8 */
/* Sug 84: Considerar PLAZA.PL_NIVEL8 */
/* Patch 400 # Seq: 8 Modificado */
ALTER TRIGGER TU_NIVEL8 ON NIVEL8 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL8 = @NewCodigo WHERE PU_NIVEL8 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL8 = @NewCodigo WHERE PL_NIVEL8 = @OldCodigo;
  END
END
GO

/* Trigger TU_NIVEL9 */
/* Sug 84: Considerar PLAZA.PL_NIVEL9 */
/* Patch 400 # Seq: 9 Modificado */
ALTER TRIGGER TU_NIVEL9 ON NIVEL9 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
    select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL9 = @NewCodigo WHERE PU_NIVEL9 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
    UPDATE PLAZA    SET PL_NIVEL9 = @NewCodigo WHERE PL_NIVEL9 = @OldCodigo;
  END
END
GO

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

/* SP_TITULO_CAMPO_DICCION:*/
/* Sug 89: Considerar entidad de PLAZA (#271) */
/* Patch 400 # Seq: 183 Modificado */
ALTER  PROCEDURE DBO.SP_TITULO_CAMPO_DICCION( @Titulo VARCHAR(30), @TituloDefault VARCHAR(30), @Prefijo VARCHAR(30), @Campo VARCHAR(30) )
AS
BEGIN
     SET NOCOUNT ON;
     if ( ( @Titulo is NULL ) or ( @Titulo = '' ) )
        set @Titulo = @TituloDefault;
     set @Titulo = SUBSTRING ( @Prefijo + @Titulo , 1 , 30 ) ;
     update DICCION set DI_TITULO = @Titulo,
                        DI_TCORTO= @Titulo,
                        DI_CLAVES = ''
     where ( DI_CLASIFI in ( 5, 10, 18, 32, 33, 50, 59, 124, 141, 142, 143, 144, 145, 146, 147, 148, 149, 152, 186, 187, 191, 218, 271 ) )
     and ( DI_NOMBRE = @Campo );
END
GO


/* SP_REFRESH_DICCION:*/
/* Sug 89: Niveles de Plazas ( PLAZA.PL_NIVEL<x> ) */
/* Patch 400 # Seq: 197 Modificado */
ALTER PROCEDURE DBO.SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 176, 'Lista de Defectos', 187
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PU_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PU_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PU_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PU_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PU_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PU_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PU_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PU_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PU_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PL_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PL_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PL_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PL_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PL_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PL_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PL_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PL_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PL_NIVEL9'

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER'
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO'
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CI_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CE_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CS_AREA'
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO'
  execute  SP_TITULO_CAMPO 176, 'Defecto','DE_CODIGO'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

  execute  SP_TITULO_EXPEDIEN_BOL  'EX_G_BOL', 'Si/No/Texto'
  execute  SP_TITULO_EXPEDIEN  'EX_G_LOG', 'Si/No'
  execute  SP_TITULO_EXPEDIEN  'EX_G_NUM', 'Numero'
  execute  SP_TITULO_EXPEDIEN  'EX_G_TEX', 'Texto'

  execute  SP_TITULO_CAMPO_PLUS 13, 'Nivel #1', 'Mod. ', 'CB_MOD_NV1'
  execute  SP_TITULO_CAMPO_PLUS 14, 'Nivel #2', 'Mod. ', 'CB_MOD_NV2'
  execute  SP_TITULO_CAMPO_PLUS 15, 'Nivel #3', 'Mod. ', 'CB_MOD_NV3'
  execute  SP_TITULO_CAMPO_PLUS 16, 'Nivel #4', 'Mod. ', 'CB_MOD_NV4'
  execute  SP_TITULO_CAMPO_PLUS 17, 'Nivel #5', 'Mod. ', 'CB_MOD_NV5'
  execute  SP_TITULO_CAMPO_PLUS 18, 'Nivel #6', 'Mod. ', 'CB_MOD_NV6'
  execute  SP_TITULO_CAMPO_PLUS 19, 'Nivel #7', 'Mod. ', 'CB_MOD_NV7'
  execute  SP_TITULO_CAMPO_PLUS 20, 'Nivel #8', 'Mod. ', 'CB_MOD_NV8'
  execute  SP_TITULO_CAMPO_PLUS 21, 'Nivel #9', 'Mod. ', 'CB_MOD_NV9'

  execute SP_REFRESH_ADICIONALES

  execute  SP_CUSTOM_DICCION

end
GO

/* SP_CUR_MAX: Determina cual es la fecha de la �ltima vez que un empleado tom� un curso antes de una fecha especificada (Sug 375 ) */
/* Patch 400 # Seq: 235 Agregado */
CREATE FUNCTION SP_CUR_MAX( @EMPLEADO INTEGER, @CURSO CHAR(6), @FECHA DATETIME )
RETURNS DateTime
AS
BEGIN
	declare @Resultado DateTime
	select @Resultado = MAX( KC_FEC_TOM ) from KARCURSO where
    ( CB_CODIGO = @EMPLEADO ) and
    ( CU_CODIGO = @CURSO ) and
    ( KC_FEC_TOM <= @FECHA );
	return @Resultado;
END
GO

/* SALDO_CTA: ?? ( Sug 759 ) */
/* Patch 400 # Seq: 236 Agregado */
CREATE FUNCTION DBO.SALDO_CTA( @CT_CODIGO Codigo, @Fecha Fecha )
RETURNS Pesos
AS
BEGIN
	 return COALESCE( ( select SUM( case CM_DEP_RET when 'D' then CM_MONTO else -CM_MONTO end )
                        from CTA_MOVS where
                        ( CT_CODIGO = @CT_CODIGO ) and
                        ( CM_FECHA <= @Fecha ) ), 0 );
END
GO

/* SUMA_CTAMOVS: ?? ( Sug 759 ) */
/* Patch 400 # Seq: 237 Agregado */
CREATE FUNCTION DBO.SUMA_CTAMOVS( @CT_CODIGO Codigo, @FechaIni Fecha, @FechaFin Fecha, @Tipo Codigo1 )
RETURNS Pesos
AS
BEGIN
	 return COALESCE( ( select SUM( CM_MONTO ) from CTA_MOVS where
     ( CT_CODIGO = @CT_CODIGO ) and
     ( CM_FECHA between @FechaIni and @FechaFin ) and
     ( CM_DEP_RET = @Tipo ) ), 0 )
END
GO

/* SIGUIENTE_CHEQUE: ?? ( Sug 759 ) */
/* Patch 400 # Seq: 238 Agregado */
CREATE FUNCTION DBO.SIGUIENTE_CHEQUE( @CT_CODIGO Codigo )
RETURNS FolioGrande
AS
BEGIN
	 return COALESCE( ( select MAX( CM_CHEQUE ) from CTA_MOVS
     where ( CT_CODIGO = @CT_CODIGO ) ), 0 ) + 1;
END
GO

/* SP_SUMA_SALARIO: Obtiene el SalarioTotal = Suma( SalarioDiario ) para un periodo de tiempo considerando cambios ( Funcion SAL_PROMEDIO ) ( Sug 769 ) */
/* Patch 400 # Seq: 239 Agregado */
CREATE FUNCTION SP_SUMA_SALARIO( @Empleado INTEGER, @FechaIni DATETIME, @Dias INTEGER )
RETURNS Pesos
AS
BEGIN
     declare @AU_FECHA Fecha;
     declare @FechaFin Fecha;
     declare @FechaRevision Fecha;
     declare @SalarioTotal Pesos;
     declare @SalarioFecha Pesos;
     declare @SalarioActual Pesos;

     set @AU_FECHA = @FechaIni;
     set @FechaFin = DATEADD( day, @Dias-1, @FechaIni );
     set @SalarioTotal = 0;
     select @FechaRevision = CB_FEC_REV, @SalarioActual = CB_SALARIO
     from COLABORA where ( CB_CODIGO = @Empleado );

     while ( @AU_FECHA <= @FechaFin )
     begin
          if ( @AU_FECHA >= @FechaRevision )          /* Puede Usar el Salario Actual */
             set @SalarioFecha = @SalarioActual;
          else
              select @SalarioFecha = DBO.SP_KARDEX_CB_SALARIO( @AU_FECHA, @Empleado );
          set @SalarioTotal = @SalarioTotal + @SalarioFecha;
          set @AU_FECHA = DATEADD( day, 1, @AU_FECHA );
     end
     return ( @SalarioTotal );
END
GO

/* SP_BORRA_DESCTIPO: Borrado de Seccion ( Sug 791 ) */
/* Patch 400 # Seq: 240 Agregado */
CREATE PROCEDURE SP_BORRA_DESCTIPO( @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from DESCTIPO where ( DT_ORDEN = @Actual );
    if ( @@ROWCOUNT > 0 )
    begin
	     update DESCTIPO set DT_ORDEN = DT_ORDEN - 1
	     where ( DT_ORDEN > @Actual );
    end
END
GO

/* SP_CAMBIA_DESCTIPO: ?? ( Sug 791 ) */
/* Patch 400 # Seq: 241 Agregado */
CREATE PROCEDURE SP_CAMBIA_DESCTIPO( @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	declare @Maximo FolioChico;
	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( DT_ORDEN ) from DESCTIPO;
	if ( @Nuevo > @Maximo ) or ( @Actual > @Maximo )
		RETURN;
	update DESCTIPO	set DT_ORDEN = 0 where ( DT_ORDEN = @Actual );
	update DESCTIPO	set DT_ORDEN = @Actual where ( DT_ORDEN = @Nuevo );
	update DESCTIPO set DT_ORDEN = @Nuevo where ( DT_ORDEN = 0 );
END
GO

/* SP_BORRA_DESC_FLD: Borrado de Seccion ( Sug 791 ) */
/* Patch 400 # Seq: 242 Agregado */
CREATE PROCEDURE SP_BORRA_DESC_FLD( @DT_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from DESC_FLD where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = @Actual );
	if ( @@ROWCOUNT > 0 )
    begin
	     update DESC_FLD set DF_ORDEN = DF_ORDEN - 1
         where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN > @Actual );
    end
END
GO

/* SP_CAMBIA_DESC_FLD: Cambios de orden en campos ( Sug 791 ) */
/* Patch 400 # Seq: 243 Agregado */
CREATE PROCEDURE SP_CAMBIA_DESC_FLD( @DT_CODIGO Codigo, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	declare @Maximo FolioChico

	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( DF_ORDEN ) from DESC_FLD
    where ( DT_CODIGO = @DT_CODIGO );
	if ( ( @Nuevo > @Maximo ) or ( @Actual > @Maximo ) )
		RETURN;

	update DESC_FLD set DF_ORDEN = 0
    where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = @Actual );

	update DESC_FLD set DF_ORDEN = @Actual
    where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = @Nuevo );

	update DESC_FLD set DF_ORDEN = @Nuevo
    where ( DT_CODIGO = @DT_CODIGO ) and ( DF_ORDEN = 0 );

END
GO

/* SP_BORRA_DESC_PTO: Borra Descripcion de puestos ( Sug 791 ) */
/* Patch 400 # Seq: 244 Agregado */
CREATE PROCEDURE SP_BORRA_DESC_PTO( @PU_CODIGO Codigo, @DT_CODIGO Codigo, @Actual FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	delete from DESC_PTO where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = @Actual );
	if ( @@ROWCOUNT > 0 )
    begin
	     update DESC_PTO set DP_ORDEN = DP_ORDEN - 1 where
         ( PU_CODIGO = @PU_CODIGO ) and
         ( DT_CODIGO = @DT_CODIGO ) and
         ( DP_ORDEN > @Actual );
    end
END
GO

/* SP_CAMBIA_DESC_PTO: ?? ( Sug 791 ) */
/* Patch 400 # Seq: 245 Agregado */
CREATE PROCEDURE SP_CAMBIA_DESC_PTO( @PU_CODIGO Codigo, @DT_CODIGO Codigo, @Actual FolioChico, @Nuevo FolioChico )
AS
BEGIN
	SET NOCOUNT ON;
	declare @Maximo FolioChico;

	if ( ( @Nuevo < 1 ) or ( @Actual < 1 ) or ( @Actual = @Nuevo ) )
		RETURN;
	select @Maximo = MAX( DP_ORDEN ) from DESC_PTO
	where ( PU_CODIGO = @PU_CODIGO ) and ( DT_CODIGO = @DT_CODIGO );
	if ( ( @Nuevo > @Maximo ) or ( @Actual > @Maximo ) )
		RETURN

	update DESC_PTO set DP_ORDEN = 0 where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = @Actual );

	update DESC_PTO set DP_ORDEN = @Actual where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = @Nuevo );

	update DESC_PTO set DP_ORDEN = @Nuevo where
    ( PU_CODIGO = @PU_CODIGO ) and
    ( DT_CODIGO = @DT_CODIGO ) and
    ( DP_ORDEN = 0 );
END
GO

/* SP_COPIA_PERFIL: Copiar Perfil ( Sug 791 ) */
/* Patch 400 # Seq: 246 Agregado */
CREATE PROCEDURE SP_COPIA_PERFIL( @Anterior Codigo, @Nuevo Codigo )
AS
BEGIN
	SET NOCOUNT ON;
	insert into PERFIL( PU_CODIGO,
                        PF_ESTUDIO,
                        PF_DESCRIP,
                        PF_OBJETIV,
                        PF_FECHA,
                        PF_EXP_PTO,
                        PF_POSTING,
                        PF_CONTRL1,
                        PF_CONTRL2,
                        PF_NUMERO1,
                        PF_NUMERO2,
                        PF_TEXTO1,
                        PF_TEXTO2,
                        PF_EDADMIN,
                        PF_EDADMAX,
                        PF_SEXO )
	select @Nuevo,
           PF_ESTUDIO,
           PF_DESCRIP,
           PF_OBJETIV,
           PF_FECHA,
           PF_EXP_PTO,
           PF_POSTING,
           PF_CONTRL1,
           PF_CONTRL2,
           PF_NUMERO1,
           PF_NUMERO2,
           PF_TEXTO1,
           PF_TEXTO2,
           PF_EDADMIN,
           PF_EDADMAX,
           PF_SEXO
	from PERFIL where ( PU_CODIGO = @Anterior );

	insert into DESC_PTO( PU_CODIGO,
                          DT_CODIGO,
                          DP_ORDEN,
                          DP_TEXT_01,
                          DP_TEXT_02,
                          DP_TEXT_03,
                          DP_MEMO_01,
                          DP_MEMO_02,
                          DP_MEMO_03,
                          DP_NUME_01,
                          DP_FECH_01 )
	select @Nuevo,
           DT_CODIGO,
           DP_ORDEN,
           DP_TEXT_01,
           DP_TEXT_02,
           DP_TEXT_03,
           DP_MEMO_01,
           DP_MEMO_02,
           DP_MEMO_03,
           DP_NUME_01,
           DP_FECH_01
	from DESC_PTO where ( PU_CODIGO = @Anterior );
END
GO

/* SP_TOPE_PTU: ?? ( Defecto ?? ) */
/* Patch 400 # Seq: 162 Modificado */
ALTER procedure DBO.SP_TOPE_PTU(
@ANIO SMALLINT,
@UTILIDADES NUMERIC( 15, 2 ),
@TOPESALARIO NUMERIC( 15, 2 ) )
as
begin
     SET NOCOUNT ON
     declare @Empleado Integer;
     declare @iEmpleados Integer;
     declare @nDias SmallInt;
     declare @nTotalDias Integer;
     declare @nMonto NUMERIC( 15, 2 );
     declare @nTotalMonto NUMERIC( 15, 2 );
     declare @nFactor NUMERIC( 15, 5 );
     set @nTotalDias = 0;
     set @nTotalMonto = 0;
     set @iEmpleados = 0;
     declare Temporal cursor for
        select R.CB_CODIGO, R.RU_DIAS, R.RU_MONTO from DBO.REP_PTU as R where ( R.RU_YEAR = @ANIO );
     open Temporal;
     fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
     while @@Fetch_Status = 0
     begin
          if ( @nMonto > @TOPESALARIO )
          begin
               set @nMonto = @TOPESALARIO;
               update DBO.REP_PTU set RU_MONTO = @TopeSalario where
               ( RU_YEAR = @ANIO ) and ( CB_CODIGO = @Empleado );
          end;
          set @nTotalMonto = @nTotalMonto + @nMonto;
          set @nTotalDias = @nTotalDias + @nDias;
          set @iEmpleados = @iEmpleados + 1;
          fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
     end;
     close Temporal;
     if ( @iEmpleados > 0 )
     begin
          set @nFactor = ( @UTILIDADES / 2.0 );
          open Temporal;
          fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
          while @@Fetch_Status = 0
          begin
               update DBO.REP_PTU set RU_M_DIAS = ROUND( @nDias * ( @nFactor / @nTotalDias ), 2 ),
                                      RU_M_MONTO = ROUND( @nMonto * ( @nFactor / @nTotalMonto ), 2 )
               where ( CB_CODIGO = @Empleado ) and ( RU_YEAR = @ANIO );
               fetch NEXT from Temporal into @Empleado, @nDias, @nMonto;
          end;
          close Temporal;
     end;
     deallocate Temporal;
end
GO

/* AFECTA_EMPLEADO: Considerar conceptos nuevos 1011,1121,1122,1123 ( Sugerencia 503 / 530 ) */
/* Patch 400 # Seq: 151 Modificado */
ALTER PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON
     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,         
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,         
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1200=N.NO_JORNADA,         
            @M1201=N.NO_HORAS,           
            @M1202=N.NO_EXTRAS,          
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,         
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
     begin
          Select @M1200 = @M1201 / @M1200;
     end
     Select @M1109 = @M1200 * @M1109;
     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008;
     if ( @M1009 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123;
     if ( @M1200 <> 0 )
     begin
          if ( @Tipo = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( ( @Tipo = 1 ) or ( @Tipo = 6 ) or ( @Tipo = 7 ) )
             Select @M1200 = 48 * @M1200;
          else
          if ( @Tipo = 2 )
             Select @M1200 = 96 * @M1200;
          else
          if ( ( @Tipo = 3 ) or ( @Tipo = 8 ) )
             Select @M1200 = 104 * @M1200;
          else
          if ( @Tipo = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @Tipo = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200;
     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004;
END
GO

/* SP_CANCELA_NOMINA: Considerar conceptos nuevos 1011,1121,1122,1123 ( Sugerencia 503 ) */
/* Patch 400 # Seq: 45 Modificado */
ALTER procedure DBO.SP_CANCELA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER,
@USUARIO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @StatusNuevo SmallInt;
     select @StatusNuevo = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @StatusNuevo is not Null )
     begin
          if ( @StatusNuevo > 4 )
             set @StatusNuevo = 4;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,  	
								  CB_BAN_ELE,	
								  NO_HORAPDT,	
								  NO_HORASNT,	
								  NO_DIAS_SI,	
								  NO_DIAS_VJ,	
							      NO_DIAS_PV,	
								  NO_SUP_OK,	
								  NO_FEC_OK,	
								  NO_HOR_OK,	
								  NO_ASI_INI,	
								  NO_ASI_FIN,	
								  NO_DIAS_BA,	
								  NO_PREV_GR,	
								  CB_PLAZA,		
								  NO_FEC_LIQ )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N1.CB_CODIGO,
                 N1.CB_CLASIFI,
                 N1.CB_TURNO,
                 N1.CB_PATRON,
                 N1.CB_PUESTO,
                 N1.CB_ZONA_GE,
                 N1.NO_FOLIO_1,
                 N1.NO_FOLIO_2,
                 N1.NO_FOLIO_3,
                 N1.NO_FOLIO_4,
                 N1.NO_FOLIO_5,
                 N1.NO_OBSERVA,
                 @StatusNuevo,
                 N1.US_CODIGO,
                 N1.NO_USER_RJ,
                 N1.CB_NIVEL1,
                 N1.CB_NIVEL2,
                 N1.CB_NIVEL3,
                 N1.CB_NIVEL4,
                 N1.CB_NIVEL5,
                 N1.CB_NIVEL6,
                 N1.CB_NIVEL7,
                 N1.CB_NIVEL8,
                 N1.CB_NIVEL9,
                 N1.CB_SAL_INT,
                 N1.CB_SALARIO,
                 -N1.NO_DIAS,
                 -N1.NO_ADICION,
                 -N1.NO_DEDUCCI,
                 -N1.NO_NETO,
                 -N1.NO_DES_TRA,
                 -N1.NO_DIAS_AG,
                 -N1.NO_DIAS_VA,
                 -N1.NO_DOBLES,
                 -N1.NO_EXTRAS,
                 -N1.NO_FES_PAG,
                 -N1.NO_FES_TRA,
                 -N1.NO_HORA_CG,
                 -N1.NO_HORA_SG,
                 -N1.NO_HORAS,
                 -N1.NO_IMP_CAL,
                 -N1.NO_JORNADA,
                 -N1.NO_PER_CAL,
                 -N1.NO_PER_MEN,
                 -N1.NO_PERCEPC,
                 -N1.NO_TARDES,
                 -N1.NO_TRIPLES,
                 -N1.NO_TOT_PRE,
                 -N1.NO_VAC_TRA,
                 -N1.NO_X_CAL,
                 -N1.NO_X_ISPT,
                 -N1.NO_X_MENS,
                 -N1.NO_D_TURNO,
                 -N1.NO_DIAS_AJ,
                 -N1.NO_DIAS_AS,
                 -N1.NO_DIAS_CG,
                 -N1.NO_DIAS_EM,
                 -N1.NO_DIAS_FI,
                 -N1.NO_DIAS_FJ,
                 -N1.NO_DIAS_FV,
                 -N1.NO_DIAS_IN,
                 -N1.NO_DIAS_NT,
                 -N1.NO_DIAS_OT,
                 -N1.NO_DIAS_RE,
                 -N1.NO_DIAS_SG,
                 -N1.NO_DIAS_SS,
                 -N1.NO_DIAS_SU,
                 -N1.NO_HORA_PD,
                 N1.NO_LIQUIDA,
                 N1.NO_FUERA,
                 -N1.NO_EXENTAS,
                 N1.NO_FEC_PAG,
                 N1.NO_USR_PAG,
				 N1.CB_NIVEL0,  	
				 N1.CB_BAN_ELE,		
				 -N1.NO_HORAPDT,	
				 -N1.NO_HORASNT,	
				 -N1.NO_DIAS_SI,	
				 -N1.NO_DIAS_VJ,	
				 -N1.NO_DIAS_PV,	
				 N1.NO_SUP_OK,		
				 N1.NO_FEC_OK,		
				 N1.NO_HOR_OK,		
				 N1.NO_ASI_INI,		
				 N1.NO_ASI_FIN,		
				 -N1.NO_DIAS_BA,	
				 -N1.NO_PREV_GR,
				 N1.CB_PLAZA, 		
				 N1.NO_FEC_LIQ		
          from DBO.NOMINA as N1 where
          ( N1.PE_YEAR = @YEARORIGINAL ) and
          ( N1.PE_TIPO = @TIPOORIGINAL ) and
          ( N1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( N1.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M1.CB_CODIGO,
                 M1.MO_ACTIVO,
                 M1.CO_NUMERO,
                 -M1.MO_IMP_CAL,
                 @USUARIO,
                 M1.MO_PER_CAL,
                 M1.MO_REFEREN,
                 -M1.MO_X_ISPT,
                 -M1.MO_PERCEPC,
                 -M1.MO_DEDUCCI
          from DBO.MOVIMIEN as M1 where
          ( M1.PE_YEAR = @YEARORIGINAL ) and
          ( M1.PE_TIPO = @TIPOORIGINAL ) and
          ( M1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( M1.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              CB_CODIGO,
                              FA_FEC_INI,
                              FA_DIA_HOR,
                              FA_MOTIVO,
                              FA_DIAS,
                              FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F1.CB_CODIGO,
                 F1.FA_FEC_INI,
                 F1.FA_DIA_HOR,
                 F1.FA_MOTIVO,
                 -F1.FA_DIAS,
                 -F1.FA_HORAS
          from DBO.FALTAS as F1 where
          ( F1.PE_YEAR = @YEARORIGINAL ) and
          ( F1.PE_TIPO = @TIPOORIGINAL ) and
          ( F1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( F1.CB_CODIGO = @EMPLEADO );
     end
end
GO

/* SP_COPIA_NOMINA: Considerar conceptos nuevos 1011,1121,1122,1123 ( Sugerencia 503 ) */
/* Patch 400 # Seq: 17 Modificado */
ALTER procedure DBO.SP_COPIA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER )
as
begin
     SET NOCOUNT ON
     declare @Status SmallInt;
     select @Status = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @Status is not Null )
     begin
          if ( @Status > 4 )
          begin
               set @Status = 4;
          end;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,	
								  CB_BAN_ELE,	
								  NO_HORAPDT,	
							      NO_HORASNT,	
								  NO_DIAS_SI,	
								  NO_DIAS_VJ,	
							      NO_DIAS_PV,	
								  NO_SUP_OK,	
								  NO_FEC_OK,	
								  NO_HOR_OK,	
								  NO_ASI_INI,	
								  NO_ASI_FIN,	
								  NO_DIAS_BA,	
								  NO_PREV_GR,	
								  CB_PLAZA,  	
								  NO_FEC_LIQ )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N2.CB_CODIGO,
                 N2.CB_CLASIFI,
                 N2.CB_TURNO,
                 N2.CB_PATRON,
                 N2.CB_PUESTO,
                 N2.CB_ZONA_GE,
                 N2.NO_FOLIO_1,
                 N2.NO_FOLIO_2,
                 N2.NO_FOLIO_3,
                 N2.NO_FOLIO_4,
                 N2.NO_FOLIO_5,
                 N2.NO_OBSERVA,
                 @Status,
                 N2.US_CODIGO,
                 N2.NO_USER_RJ,
                 N2.CB_NIVEL1,
                 N2.CB_NIVEL2,
                 N2.CB_NIVEL3,
                 N2.CB_NIVEL4,
                 N2.CB_NIVEL5,
                 N2.CB_NIVEL6,
                 N2.CB_NIVEL7,
                 N2.CB_NIVEL8,
                 N2.CB_NIVEL9,
                 N2.CB_SAL_INT,
                 N2.CB_SALARIO,
                 N2.NO_DIAS,
                 N2.NO_ADICION,
                 N2.NO_DEDUCCI,
                 N2.NO_NETO,
                 N2.NO_DES_TRA,
                 N2.NO_DIAS_AG,
                 N2.NO_DIAS_VA,
                 N2.NO_DOBLES,
                 N2.NO_EXTRAS,
                 N2.NO_FES_PAG,
                 N2.NO_FES_TRA,
                 N2.NO_HORA_CG,
                 N2.NO_HORA_SG,
                 N2.NO_HORAS,
                 N2.NO_IMP_CAL,
                 N2.NO_JORNADA,
                 N2.NO_PER_CAL,
                 N2.NO_PER_MEN,
                 N2.NO_PERCEPC,
                 N2.NO_TARDES,
                 N2.NO_TRIPLES,
                 N2.NO_TOT_PRE,
                 N2.NO_VAC_TRA,
                 N2.NO_X_CAL,
                 N2.NO_X_ISPT,
                 N2.NO_X_MENS,
                 N2.NO_D_TURNO,
                 N2.NO_DIAS_AJ,
                 N2.NO_DIAS_AS,
                 N2.NO_DIAS_CG,
                 N2.NO_DIAS_EM,
                 N2.NO_DIAS_FI,
                 N2.NO_DIAS_FJ,
                 N2.NO_DIAS_FV,
                 N2.NO_DIAS_IN,
                 N2.NO_DIAS_NT,
                 N2.NO_DIAS_OT,
                 N2.NO_DIAS_RE,
                 N2.NO_DIAS_SG,
                 N2.NO_DIAS_SS,
                 N2.NO_DIAS_SU,
                 N2.NO_HORA_PD,
                 N2.NO_LIQUIDA,
                 N2.NO_FUERA,
                 N2.NO_EXENTAS,
                 N2.NO_FEC_PAG,
                 N2.NO_USR_PAG,
				 N2.CB_NIVEL0,	
				 N2.CB_BAN_ELE,	
				 N2.NO_HORAPDT,	
				 N2.NO_HORASNT,	
				 N2.NO_DIAS_SI,	
				 N2.NO_DIAS_VJ,	
				 N2.NO_DIAS_PV,	
				 N2.NO_SUP_OK,	
				 N2.NO_FEC_OK,	
				 N2.NO_HOR_OK,	
				 N2.NO_ASI_INI,	
				 N2.NO_ASI_FIN,	
				 N2.NO_DIAS_BA,	
				 N2.NO_PREV_GR,	
				 N2.CB_PLAZA,  	
				 N2.NO_FEC_LIQ
          from DBO.NOMINA as N2 where ( N2.PE_YEAR = @YEARORIGINAL ) and
                                      ( N2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( N2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( N2.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M2.CB_CODIGO,
                 M2.MO_ACTIVO,
                 M2.CO_NUMERO,
                 M2.MO_IMP_CAL,
                 M2.US_CODIGO,
                 M2.MO_PER_CAL,
                 M2.MO_REFEREN,
                 M2.MO_X_ISPT,
                 M2.MO_PERCEPC,
                 M2.MO_DEDUCCI
          from DBO.MOVIMIEN as M2 where ( M2.PE_YEAR = @YEARORIGINAL ) and
                                        ( M2.PE_TIPO = @TIPOORIGINAL ) and
                                        ( M2.PE_NUMERO = @NUMEROORIGINAL ) and
                                        ( M2.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  FA_FEC_INI,
                                  FA_DIA_HOR,
                                  FA_MOTIVO,
                                  FA_DIAS,
                                  FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F2.CB_CODIGO,
                 F2.FA_FEC_INI,
                 F2.FA_DIA_HOR,
                 F2.FA_MOTIVO,
                 F2.FA_DIAS,
                 F2.FA_HORAS
          from DBO.FALTAS as F2 where ( F2.PE_YEAR = @YEARORIGINAL ) and
                                      ( F2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( F2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( F2.CB_CODIGO = @EMPLEADO );
     end;
end
GO

/* RECALCULA_KARDEX: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 165 Modificado */
ALTER PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);
   declare @kPLAZA    FolioGrande;

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare @ePLAZA    FolioGrande;
   declare @eFEC_PLA  Fecha;
   declare CursorKardex CURSOR SCROLL_LOCKS FOR
   select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
    CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
    CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
    CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
    CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
    CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
    CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S, CB_PLAZA
   from Kardex
   where CB_CODIGO = @Empleado
   order by cb_fecha,cb_nivel

      	SET @FechaVacia = '12/30/1899';
        SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;
        SET @eFEC_PLA = @FechaVacia;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
      @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
      @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
      @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
      @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
      @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
      @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA


	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END


	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
                                SET @kOLD_INT = @eOLD_INT;
	                        SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eFEC_PTO  = @kFECHA;
                        if ( @kTIPO = 'PLAZA' AND @eAUTOSAL = 'S' )
                             SET @kCLASIFI = @eCLASIFI;
                        else
			     SET @eCLASIFI  = @kCLASIFI;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) OR ( @kTIPO = 'PLAZA' ))
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                                if ( @kNIVEL1 <> '' )
                                    SET @kMOD_NV1 = 'S';
                                else
                                    SET @kMOD_NV1 = 'N';
                                if ( @kNIVEL2 <> '' )
                                    SET @kMOD_NV2 = 'S';
                                else
                                    SET @kMOD_NV2 = 'N';
                                if ( @kNIVEL3 <> '' )
                                    SET @kMOD_NV3 = 'S';
                                else
                                    SET @kMOD_NV3 = 'N';
                                if ( @kNIVEL4 <> '' )
                                    SET @kMOD_NV4 = 'S';
                                else
                                    SET @kMOD_NV4 = 'N';
                                if ( @kNIVEL5 <> '' )
                                    SET @kMOD_NV5 = 'S';
                                else
                                    SET @kMOD_NV5 = 'N';
                                if ( @kNIVEL6 <> '' )
                                    SET @kMOD_NV6 = 'S';
                                else
                                    SET @kMOD_NV6 = 'N';
                                if ( @kNIVEL7 <> '' )
                                    SET @kMOD_NV7 = 'S';
                                else
                                    SET @kMOD_NV7 = 'N';
                                if ( @kNIVEL8 <> '' )
                                    SET @kMOD_NV8 = 'S';
                                else
                                    SET @kMOD_NV8 = 'N';
                                if ( @kNIVEL9 <> '' )
                                    SET @kMOD_NV9 = 'S';
                                else
                                    SET @kMOD_NV9 = 'N';

			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
                        SET @kMOD_NV1 = 'N';
                        SET @kMOD_NV2 = 'N';
                        SET @kMOD_NV3 = 'N';
                        SET @kMOD_NV4 = 'N';
                        SET @kMOD_NV5 = 'N';
                        SET @kMOD_NV6 = 'N';
                        SET @kMOD_NV7 = 'N';
                        SET @kMOD_NV8 = 'N';
                        SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
			SET @eTABLASS = @kTABLASS;
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
			SET @ePLAZA    =  0;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END


		if ( @kTIPO = 'ALTA'  OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePLAZA    = @kPLAZA;
			SET @eFEC_PLA  = @kFECHA;
		END
		else
		BEGIN
			SET @kPLAZA    = @ePLAZA;
		END

		update KARDEX
     		set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S,
                        CB_PLAZA   = @kPLAZA
		where CURRENT OF CursorKardex


		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
       @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
       @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
       @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
       @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
       @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
       @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA
	END

	Close CursorKardex
	Deallocate CursorKardex

	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR )  SET @eFEC_KAR = @eFEC_TUR;
	if ( @eFEC_PLA > @eFEC_KAR )  SET @eFEC_KAR = @eFEC_PLA;

	update COLABORA
	set CB_AUTOSAL = @eAUTOSAL,
		CB_ACTIVO  = @eACTIVO,
		CB_CLASIFI = @eCLASIFI,
		CB_CONTRAT = @eCONTRAT,
		CB_FAC_INT = @eFAC_INT,
		CB_FEC_ANT = @eFEC_ANT,
		CB_FEC_CON = @eFEC_CON,
		CB_FEC_ING = @eFEC_ING,
		CB_FEC_INT = @eFEC_INT,
		CB_FEC_REV = @eFEC_REV,
		CB_FEC_SAL = @eFEC_SAL,
		CB_FEC_BAJ = @eFEC_BAJ,
		CB_FEC_BSS = @eFEC_BSS,
		CB_MOT_BAJ = @eMOT_BAJ,
		CB_NIVEL1  = @eNIVEL1,
		CB_NIVEL2  = @eNIVEL2,
		CB_NIVEL3  = @eNIVEL3,
		CB_NIVEL4  = @eNIVEL4,
		CB_NIVEL5  = @eNIVEL5,
		CB_NIVEL6  = @eNIVEL6,
		CB_NIVEL7  = @eNIVEL7,
                CB_NIVEL8  = @eNIVEL8,
		CB_NIVEL9  = @eNIVEL9,
		CB_NOMNUME = @eNOMNUME,
		CB_NOMTIPO = @eNOMTIPO,
		CB_NOMYEAR = @eNOMYEAR,
		CB_PATRON  = @ePATRON,
		CB_PER_VAR = @ePER_VAR,
		CB_PUESTO  = @ePUESTO,
		CB_SAL_INT = @eSAL_INT,
		CB_SALARIO = @eSALARIO,
		CB_TABLASS = @eTABLASS,
		CB_TURNO   = @eTURNO,
		CB_ZONA_GE = @eZONA_GE,
		CB_OLD_SAL = @eOLD_SAL,
		CB_OLD_INT = @eOLD_INT,
		CB_PRE_INT = @ePRE_INT,
		CB_SAL_TOT = @eSAL_TOT,
		CB_TOT_GRA = @eTOT_GRA,
		CB_TIP_REV = @eTIP_REV,
		CB_RANGO_S = @eRANGO_S,
		CB_FEC_NIV = @eFEC_NIV,
		CB_FEC_PTO = @eFEC_PTO,
		CB_FEC_TUR = @eFEC_TUR,
		CB_FEC_KAR = @eFEC_KAR,
                CB_PLAZA   = @ePLAZA,
                CB_FEC_PLA = @eFEC_PLA
	where ( CB_CODIGO = @Empleado );
END
GO

/* SP_CLAS_NOMINA: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 27 Modificado */
ALTER PROCEDURE SP_CLAS_NOMINA
    	@ANIO SMALLINT,
    	@TIPO SMALLINT,
    	@NUMERO SMALLINT,
    	@EMPLEADO INTEGER,
        @ROTATIVO CHAR(6) OUTPUT
AS
    SET NOCOUNT ON
	declare @FechaIni DATETIME;
 	declare @FechaFin DATETIME;
 	declare @ZONA_GE  CHAR(1);
 	declare @PUESTO   CHAR(6);
 	declare @CLASIFI  CHAR(6);
 	declare @TURNO    CHAR(6);
 	declare @PATRON   CHAR(1);
 	declare @NIVEL1   CHAR(6);
 	declare @NIVEL2   CHAR(6);
 	declare @NIVEL3   CHAR(6);
 	declare @NIVEL4   CHAR(6);
 	declare @NIVEL5   CHAR(6);
 	declare @NIVEL6   CHAR(6);
 	declare @NIVEL7   CHAR(6);
 	declare @NIVEL8   CHAR(6);
 	declare @NIVEL9   CHAR(6);
 	declare @SALARIO  NUMERIC(15,2);
 	declare @SAL_INT  NUMERIC(15,2);
 	declare @PROMEDIA CHAR(1);
 	declare @JORNADA  NUMERIC(15,2);
 	declare @D_TURNO  SMALLINT;
 	declare @ES_ROTA  CHAR(1);
 	declare @FEC_KAR  DATETIME;
 	declare @FEC_SAL  DATETIME;
	declare @BAN_ELE  VARCHAR(30);
	declare @NIVEL0   CHAR(6);
	declare @PLAZA	  INTEGER;	

  	select @FechaIni = PE_FEC_INI,
               @FechaFin = PE_FEC_FIN
  	from   PERIODO
  	where  PE_YEAR = @Anio 
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero

  	select @ZONA_GE = CB_ZONA_GE,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
		@PATRON = CB_PATRON,
                @NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2, 
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4, 
		@NIVEL5 = CB_NIVEL5,
                @NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7, 
		@NIVEL8 = CB_NIVEL8, 
		@NIVEL9 = CB_NIVEL9,
                @SALARIO = CB_SALARIO,
		@SAL_INT = CB_SAL_INT, 
		@FEC_KAR = CB_FEC_KAR, 
		@FEC_SAL = CB_FEC_SAL,
                @BAN_ELE = CB_BAN_ELE,
                @NIVEL0 = CB_NIVEL0,
		@PLAZA	= CB_PLAZA
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @FechaFin < @FEC_KAR ) 
  	begin
    		select 
			@ZONA_GE = CB_ZONA_GE,
			@PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI, 
			@TURNO = CB_TURNO, 
			@PATRON = CB_PATRON,
         	        @NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
         	        @NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
         	        @SALARIO = CB_SALARIO,
			@SAL_INT = CB_SAL_INT,
			@PLAZA	= CB_PLAZA
            from   SP_FECHA_KARDEX( @FechaFin, @Empleado )
  	end

  	select @PROMEDIA = GL_FORMULA
  	from   GLOBAL
  	where  GL_CODIGO = 42

  	if (( @PROMEDIA = 'S' ) and ( @FEC_SAL > @FechaIni ))
        EXECUTE SP_RANGO_SALARIO @FechaIni, @FechaFin, @Empleado,
                                 @SALARIO OUTPUT, @SAL_INT OUTPUT

	EXECUTE SP_DIAS_JORNADA @Turno, @FechaIni, @FechaFin,
    				@D_Turno OUTPUT, @Jornada OUTPUT, @Es_Rota OUTPUT

  	if ( @ES_ROTA = 'S' )
    		SET @Rotativo = @Turno
  	else
    		SET @Rotativo = '';

  	update NOMINA
  	set     CB_ZONA_GE = @ZONA_GE,
        	CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_PATRON  = @PATRON,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
        	CB_SALARIO = @SALARIO,
        	CB_SAL_INT = @SAL_INT,
        	NO_JORNADA = @JORNADA,
        	NO_D_TURNO = @D_TURNO,
                CB_BAN_ELE = @BAN_ELE,
                CB_NIVEL0  = @NIVEL0,
                CB_PLAZA   = @PLAZA
  	where PE_YEAR = @Anio
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero
	and CB_CODIGO = @Empleado;
GO

/* SP_FECHA_KARDEX: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 174 Modificado */
ALTER FUNCTION SP_FECHA_KARDEX (
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS @RegKardex TABLE
( CB_FECHA DATETIME,
  CB_TIPO CHAR(6),
  CB_AUTOSAL CHAR(1),
  CB_CLASIFI CHAR(6),
  CB_COMENTA VARCHAR(50),
  CB_CONTRAT CHAR(1),
  CB_FAC_INT NUMERIC(15, 5),
  CB_FEC_CAP DATETIME,
  CB_FEC_CON DATETIME,
  CB_FEC_INT DATETIME,
  CB_FEC_REV DATETIME,
  CB_FECHA_2 DATETIME,
  CB_GLOBAL CHAR(1),
  CB_TURNO CHAR(6),
  CB_MONTO NUMERIC(15, 2),
  CB_MOT_BAJ CHAR(3),
  CB_OLD_INT NUMERIC(15, 2),
  CB_OLD_SAL NUMERIC(15, 2),
  CB_OTRAS_P NUMERIC(15, 2),
  CB_PATRON CHAR(1),
  CB_PER_VAR NUMERIC(15, 2),
  CB_PRE_INT NUMERIC(15, 2),
  CB_PUESTO CHAR(6),
  CB_RANGO_S NUMERIC(15, 2),
  CB_SAL_INT NUMERIC(15, 2),
  CB_SAL_TOT NUMERIC(15, 2),
  CB_SALARIO NUMERIC(15, 2),
  CB_STATUS SMALLINT,
  CB_TABLASS CHAR(1),
  CB_TOT_GRA NUMERIC(15, 2),
  US_CODIGO SMALLINT,
  CB_ZONA_GE CHAR(1),
  CB_NIVEL1 CHAR(6),
  CB_NIVEL2 CHAR(6),
  CB_NIVEL3 CHAR(6),
  CB_NIVEL4 CHAR(6),
  CB_NIVEL5 CHAR(6),
  CB_NIVEL6 CHAR(6),
  CB_NIVEL7 CHAR(6),
  CB_NIVEL8 CHAR(6),
  CB_NIVEL9 CHAR(6),
  CB_NOMTIPO SMALLINT,
  CB_NOMYEAR SMALLINT,
  CB_NOMNUME SMALLINT,
  CB_REINGRE CHAR(1),
  CB_FEC_ING DATETIME,
  CB_FEC_ANT DATETIME,
  CB_TIP_REV CHAR(6),
  CB_FEC_SAL DATETIME,
  CB_PLAZA   INTEGER
) AS
begin

	INSERT INTO @RegKardex
	select TOP 1
	       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       	       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       	       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       	       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT, CB_TIP_REV, CB_FEC_SAL, CB_PLAZA
	from KARDEX
	WHERE ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	order by CB_FECHA desc, CB_NIVEL desc

	if ( @@ROWCOUNT = 0 )
	BEGIN
		INSERT INTO @RegKardex
		select TOP 1
		       CB_FECHA, CB_TIPO, CB_AUTOSAL, CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
	               CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
       		       CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P,
	               CB_PATRON, CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_RANGO_S,
       		       CB_SAL_INT, CB_SAL_TOT, CB_SALARIO, CB_STATUS, CB_TABLASS, CB_TOT_GRA,
	               US_CODIGO, CB_ZONA_GE, CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
       		       CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
	               CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_FEC_ING, CB_FEC_ANT, CB_TIP_REV, CB_FEC_SAL, CB_PLAZA
		from KARDEX
		WHERE ( CB_CODIGO = @Empleado )
		order by CB_FECHA, CB_NIVEL


		if ( @@ROWCOUNT = 0 )
		BEGIN
      			select @Fecha = '12/30/1899';
			INSERT INTO @RegKardex
			VALUES ( @Fecha, '', 'N', '', '', '',
	        	       0, @Fecha, @Fecha, @Fecha, @Fecha, @Fecha,
	        	       'N', '', 0, '', 0, 0, 0,
		               '', 0, 0, '', 0,
        		       0, 0, 0, 0, '', 0,
		               0, '', '', '', '',
        		       '', '', '', '', '', '',
	        	       0, 0, 0, 'N', @Fecha, @Fecha, '', @Fecha, 0 )
		END
	END

	RETURN
END
GO

/* SP_OLD_CLASIFI: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 12 Modificado */
ALTER FUNCTION SP_OLD_CLASIFI (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_CLASIFI, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'PUESTO','CIERRE', 'PLAZA' ) /* Patch 400 - Sug (84)*/
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_TURNO: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 18 Modificado */
ALTER FUNCTION SP_OLD_TURNO (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_TURNO, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'TURNO','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_PUESTO: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 20 Modificado */
ALTER FUNCTION SP_OLD_PUESTO (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_PUESTO, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'PUESTO','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL1: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 22 Modificado */
ALTER FUNCTION SP_OLD_NIVEL1 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL1, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL2: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 24 Modificado */
ALTER FUNCTION SP_OLD_NIVEL2 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL2, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL3: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 26 Modificado */
ALTER FUNCTION SP_OLD_NIVEL3 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL3, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL4: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 28 Modificado */
ALTER FUNCTION SP_OLD_NIVEL4 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL4, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL5: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 30 Modificado */
ALTER FUNCTION SP_OLD_NIVEL5 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL5, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL6: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 32 Modificado */
ALTER FUNCTION SP_OLD_NIVEL6 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL6, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL7: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 34 Modificado */
ALTER FUNCTION SP_OLD_NIVEL7 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL7, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE','PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL8: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 36 Modificado */
ALTER FUNCTION SP_OLD_NIVEL8 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL8, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_NIVEL9: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 38 Modificado */
ALTER FUNCTION SP_OLD_NIVEL9 (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado CHAR(6),
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual Codigo;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_NIVEL9, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'AREA','CIERRE', 'PLAZA' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_FECHA: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 44 Modificado */
ALTER FUNCTION SP_OLD_FECHA( @Empleado int, @Tipo int )
RETURNS DATETIME
AS
BEGIN
     declare @Resultado DATETIME
     if @Tipo = 1
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL1( @Empleado )
     else if @Tipo = 2
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL2( @Empleado )
     else if @Tipo = 3
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL3( @Empleado )
     else if @Tipo = 4
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL4( @Empleado )
     else if @Tipo = 5
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL5( @Empleado )
     else if @Tipo = 6
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL6( @Empleado )
     else if @Tipo = 7
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL7( @Empleado )
     else if @Tipo = 8
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL8( @Empleado )
     else if @Tipo = 9
	select @Resultado = FECHA from dbo.SP_OLD_NIVEL9( @Empleado )
     else if @Tipo = 10
	select @Resultado = FECHA from dbo.SP_OLD_PUESTO( @Empleado )
     else if @Tipo = 11
	select @Resultado = FECHA from dbo.SP_OLD_CLASIFI( @Empleado )
     else if @Tipo = 12
	select @Resultado = FECHA from dbo.SP_OLD_TURNO( @Empleado )
     else if @Tipo = 13
	select @Resultado = FECHA from dbo.SP_OLD_TABLASS( @Empleado )
     else if @Tipo = 14
	select @Resultado = FECHA from dbo.SP_OLD_CONTRATO( @Empleado )
	 else if @Tipo = 15
	select @Resultado = FECHA from dbo.SP_OLD_PLAZA( @Empleado )
     else
	select @Resultado = ''
    RETURN @Resultado
END
GO

/* SP_OLD_PLAZA: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 251 Agregado */
CREATE FUNCTION SP_OLD_PLAZA (
    @Empleado INTEGER)
RETURNS @RegKardex TABLE (
    Resultado INTEGER,
    Fecha DATETIME)
AS
begin
  declare @FechaActual DATETIME;
  declare @CodigoActual INTEGER;
  declare @Fecha DATETIME;
  declare @Resultado Codigo;

  declare Temporal cursor for
          select CB_PLAZA, CB_FECHA
          from KARDEX
          where CB_CODIGO = @Empleado AND
                CB_TIPO IN ( 'ALTA', 'PLAZA','CIERRE' ) /* Patch400- Sug. 84 */
          order by CB_FECHA desc, CB_NIVEL desc

  Open Temporal
  Fetch Next From Temporal Into @CodigoActual, @FechaActual

  while @@Fetch_Status = 0
  begin
    if ( @Fecha IS NULL )
    begin
      select @Fecha = @FechaActual, @Resultado = @CodigoActual;
    end
    else
    begin
      if ( @CodigoActual = @Resultado )
      begin
        set @Fecha = @FechaActual;
      end
      else
      begin
        set @Resultado = @CodigoActual;
        insert into @RegKardex values ( @Resultado, @Fecha );
        break;
      end
    end
    Fetch Next From Temporal into @CodigoActual, @FechaActual
  end

  close Temporal
  deallocate Temporal

  RETURN

end
GO

/* SP_OLD_CODIGO_PLAZA: Considerar campos de PLAZA ( Sugerencia 84 ) */
/* Patch 400 # Seq: 252 Agregado */
CREATE FUNCTION SP_OLD_CODIGO_PLAZA( @Empleado int )
RETURNS INTEGER
AS
BEGIN
     declare @Resultado INTEGER
     select @Resultado = RESULTADO from dbo.SP_OLD_PLAZA( @Empleado )
     RETURN @Resultado
END
GO

/* SP_TOT_EMPLEADOS: Reporte total de empleados en ausencia ( Sug 998 ) */
/* Patch 400 # Seq: 247 Agregado */
create procedure SP_TOT_EMPLEADOS(
  @Usuario INTEGER,
  @Empleado INTEGER,
  @Resultado INTEGER OUTPUT )
as
begin
     SET NOCOUNT ON
     declare @TotalEmpleados INTEGER;
     select @TotalEmpleados = COUNT(*) from REP_EMPS where
     ( US_CODIGO = @Usuario ) and ( CB_CODIGO = @Empleado );
     if ( @TotalEmpleados = 0 )
     begin
          insert into REP_EMPS( US_CODIGO, CB_CODIGO )
          values( @Usuario, @Empleado );
     end;
     select @RESULTADO = COUNT(*) from REP_EMPS where ( US_CODIGO = @Usuario );
end
GO

/* SP_STATUS_CONFLICTO: ( Sug 792 ) */
/* Patch 400 # Seq: 77 Modificado */
ALTER FUNCTION SP_STATUS_CONFLICTO
( @EMPLEADO INTEGER,
  @FECHAORIGINAL FECHA,
  @FECHAINICIAL FECHA,
  @FECHAFINAL FECHA,
  @ENTIDAD SMALLINT )
returns @VALORES table (
  RESULTADO INTEGER not null,
  INICIO DATETIME,
  FIN DATETIME )
as
begin
     declare @iClase Integer;
     declare @sActivo Char( 1 );
     declare @Valor Integer;
     declare @Inicial FECHA;
     declare @Final FECHA;
     set @Valor = 1;
     select @Inicial = C.CB_FEC_ING,
            @Final = C.CB_FEC_BAJ,
            @sActivo = C.CB_ACTIVO from DBO.COLABORA as C
     where ( C.CB_CODIGO = @EMPLEADO );
     if ( @FECHAINICIAL < @Inicial )
     begin
          set @Valor = 0;
     end
     else
     if ( ( @sActivo <> 'S' ) and ( @FECHAFINAL > @Final ) )
     begin
          set @Valor = 9;
     end
     else
     begin
          if ( @ENTIDAD = 1 )
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <> @FECHAORIGINAL ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Incapacidades cursor for
                  select I.IN_FEC_INI, I.IN_FEC_FIN from DBO.INCAPACI as I where
                  ( I.CB_CODIGO = @EMPLEADO ) and
                  ( I.IN_FEC_INI <= @FECHAFINAL ) and
                  ( I.IN_FEC_FIN > @FECHAINICIAL );
          end
          open Incapacidades;
          fetch NEXT from Incapacidades into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = 3;
               break;
          end;
	        close Incapacidades;
          deallocate Incapacidades;
          if ( @ENTIDAD = 2 )
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_GOZO > 0 ) and
                  ( V.VA_FEC_INI <> @FECHAORIGINAL ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Vacaciones cursor for
                  select V.VA_FEC_INI, V.VA_FEC_FIN from DBO.VACACION as V where
                  ( V.CB_CODIGO = @EMPLEADO ) and
                  ( V.VA_TIPO = 1 ) and
                  ( V.VA_GOZO > 0 ) and
                  ( V.VA_FEC_INI <= @FECHAFINAL ) and
                  ( V.VA_FEC_FIN > @FECHAINICIAL );
          end;
          open Vacaciones;
          fetch NEXT from Vacaciones into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = 2;
		           break;
          end;
          close Vacaciones;
          deallocate Vacaciones;
          if ( @ENTIDAD = 3 )
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <> @FECHAORIGINAL ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare Permisos cursor for
                  select P.PM_CLASIFI, P.PM_FEC_INI, P.PM_FEC_FIN from DBO.PERMISO as P where
                  ( P.CB_CODIGO = @EMPLEADO ) and
                  ( P.PM_FEC_INI <= @FECHAFINAL ) and
                  ( P.PM_FEC_FIN > @FECHAINICIAL );
          end;
          open Permisos;
          fetch NEXT from Permisos into @iClase, @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = @iClase + 4;
		           break;
          end;
          close Permisos;
          deallocate Permisos;
          if ( @ENTIDAD = 4 )
          begin
               declare PlanVaca cursor for
                  select VP.VP_FEC_INI, VP.VP_FEC_FIN from DBO.VACAPLAN as VP where
                  ( VP.CB_CODIGO = @EMPLEADO ) and
                  ( VP.VP_STATUS in ( 0, 1 ) ) and          /* pendientes o autorizadas */
                  ( VP.VP_FEC_INI <> @FECHAORIGINAL ) and
                  ( VP.VP_FEC_INI <= @FECHAFINAL ) and
                  ( VP.VP_FEC_FIN > @FECHAINICIAL );
          end
          else
          begin
               declare PlanVaca cursor for
                  select VP.VP_FEC_INI, VP.VP_FEC_FIN from DBO.VACAPLAN as VP where
                  ( VP.CB_CODIGO = @EMPLEADO ) and
                  ( VP.VP_STATUS in ( 0, 1 ) ) and          /* pendientes o autorizadas */
                  ( VP.VP_FEC_INI <= @FECHAFINAL ) and
                  ( VP.VP_FEC_FIN > @FECHAINICIAL );
          end;
          open PlanVaca;
          fetch NEXT from PlanVaca into @Inicial, @Final;
          while @@Fetch_Status = 0
          begin
	  	         set @Valor = 10;
		           break;
          end;
          close PlanVaca;
          deallocate PlanVaca;
     end;

     insert into @VALORES( RESULTADO, INICIO, FIN ) values ( @Valor, @Inicial, @Final );

     RETURN
end
GO

/* TF: ( Sug GA - Portal: para obtener descripciones de ennumerados en TFIJAS ) */
/* Patch 400 # Seq: 248 Agregado */
create function TF( @Tabla SMALLINT, @Codigo SMALLINT ) returns VARCHAR( 50 )
as
begin
     declare @Descripcion Observaciones;
     select @Descripcion = TF_DESCRIP from TFIJAS where
     ( TF_TABLA = @Tabla ) and
     ( TF_CODIGO = @Codigo );
     return ISNULL( @Descripcion, '???' );
end
GO

/* TF_GET_NUM: ( Sug GA - Portal: para obtener n�mero de la tabla que contiene las descripciones de ennumerados en TFIJAS ) ( 2/3 ) */
/* Patch 400 # Seq: 249 Agregado */
create function TF_GET_NUM( @Tabla VARCHAR(20), @Campo VARCHAR(20) ) returns Integer
as
begin
     declare @Numero Integer;
     select @Numero = CAMPOS.DI_NUMERO
     from DICCION CAMPOS
     left outer join DICCION TABLA on ( Tabla.DI_CLASIFI = -1 ) and ( TABLA.DI_CALC = CAMPOS.DI_CLASIFI )
     where
     ( TABLA.DI_NOMBRE = @Tabla ) and
     ( CAMPOS.DI_NOMBRE = @Campo ) and
     ( CAMPOS.DI_TRANGO = 2 );
     return ISNULL( @Numero, -1 );
end
GO

/* TF_GET_FK: ( Sug GA - Portal: para obtener nombre de la tabla hacia la cual hace referencia una llave for�nea ) ( 3/3 ) */
/* Patch 400 # Seq: 250 Agregado */
create function TF_GET_FK( @Tabla VARCHAR(20), @Campo VARCHAR(20) ) returns VARCHAR(20)
as
begin
     declare @Resultado VARCHAR(20);
     select @Resultado = TABLA_FK.DI_NOMBRE
     from DICCION TABLA_FK where
     ( TABLA_FK.DI_CLASIFI = -1 ) and
     ( TABLA_FK.DI_CALC = (
     select CAMPOS.DI_NUMERO from DICCION CAMPOS
     left outer join DICCION TABLA on ( TABLA.DI_CLASIFI = -1 ) and ( TABLA.DI_CALC = CAMPOS.DI_CLASIFI )
     where
     ( TABLA.DI_NOMBRE = @Tabla ) and
     ( CAMPOS.DI_NOMBRE = @Campo ) and
     ( CAMPOS.DI_TRANGO = 1 ) ) );
     return ISNULL( @Resultado, '' );
end
GO

/****************************************/
/***** Actualizar versi�n de Datos ******/
/****************************************/

update GLOBAL set GL_FORMULA = '400' where ( GL_CODIGO = 133 );

commit;
