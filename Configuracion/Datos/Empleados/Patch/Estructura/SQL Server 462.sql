/* Patch 462: 16964 */ 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'RECALCULA_ACUMULADOS_MESES') AND type IN ( N'P', N'PC' ))
	drop procedure RECALCULA_ACUMULADOS_MESES

GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS_MESES
					@ANIO SMALLINT,
					@MESINI SMALLINT, 
					@MESFIN SMALLINT, 
					@EMPLEADO INTEGER,					
					@STATUS SMALLINT, 
					@lAlgunosConceptos Booleano = 'N' , 
					@ListaConceptos Formula  = '',
					@RangoConceptos INTEGER = 0 
AS
BEGIN
    SET NOCOUNT ON
	declare @iMes smallint 
	set @iMes = @MESINI 
		
	declare @ListaConceptosRango Formula
	declare @Query   nvarchar(max)
    declare @Params	 nvarchar( 150 )

	if @ListaConceptos = '' and @RangoConceptos in ( 0, 1 ) 
		set @ListaConceptos = 'CO_NUMERO > 0 ' 

	if @RangoConceptos = 2 or @RangoConceptos = 3 
	begin
		set @Query = 'select @ListaConceptosRango = coalesce(@ListaConceptosRango' + ' + '','' ' + ', '''') + CONVERT(varchar(255), CO_NUMERO) from CONCEPTO where ' +  @ListaConceptos
        set @Params  = N'@ListaConceptos varchar(255), @ListaConceptosRango varchar(255) OUTPUT'		
        exec sp_executesql @Query, @Params, @ListaConceptos, @ListaConceptosRango OUTPUT 
		set @ListaConceptos = @ListaConceptosRango 
	end
	
   if @RangoConceptos in ( 0 , 1 ) 
	 set @ListaConceptos = '' 

	while @iMes <= @MESFIN 
	begin 
	   exec RECALCULA_ACUMULADOS @ANIO, @EMPLEADO, @iMes, @STATUS, @lAlgunosConceptos, @ListaConceptos
		set @iMes = @iMes + 1;
	end

END

GO 

/* Patch 462: #16931*/
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'AFECTA_UN_CONCEPTO') AND type IN ( N'P', N'PC' ))
	drop procedure AFECTA_UN_CONCEPTO


GO

CREATE PROCEDURE AFECTA_UN_CONCEPTO
    				@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@CONCEPTO INTEGER,
    				@MES SMALLINT,
    				@FACTOR INTEGER,
    				@MONTO NUMERIC(15,2), 				
					@RS_CODIGO Codigo = ''
AS
BEGIN 
     SET NOCOUNT ON

	 -- Si no viene razon social le asigna la razon al final del mes del empleado 
	 IF ( @RS_CODIGO = '' )
	 begin 
		 declare @CB_PATRON Codigo 
		 declare @Fecha Fecha 
		 declare @MesIni Fecha 
		 declare @MesFin Fecha
		 

		 Select  @Fecha = DateAdd(yy, @ANIO-1900, DateAdd(m,  @MES - 1, 1- 1)) 
		 set @MesIni = CONVERT(datetime, @Fecha, 112 ) 
		 set @MesFin = dateadd(day, -1,  dateadd( month, 1, @MesIni ) );  
		 select @CB_PATRON = dbo.SP_KARDEX_CB_PATRON(@MesFin, @EMPLEADO)
		 set @CB_PATRON = coalesce( @CB_PATRON , '' ) 
		 select @RS_CODIGO = RS_CODIGO from RPATRON RP where RP.TB_CODIGO = @CB_PATRON 
		 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 
	 end 
	 EXEC AFECTA_UN_CONCEPTO_RAZON_SOCIAL @ANIO, @EMPLEADO, @CONCEPTO, @MES, @FACTOR, @MONTO, @RS_CODIGO
	 EXEC CALCULA_ACUMULADOS_TOTALES  @ANIO, @EMPLEADO, @CONCEPTO, @MES
END 

GO 

/* Patch 462: 16964 */ 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'AJUSTA_ACUMULADOS_FIX') AND type IN ( N'P', N'PC' ))
	drop procedure AJUSTA_ACUMULADOS_FIX

GO

create procedure AJUSTA_ACUMULADOS_FIX 
as
begin 
	
	declare @CO_NUMERO Concepto 
	declare @CO_NUMERO_Str Formula 

	select @CO_NUMERO_Str = GL_FORMULA  from GLOBAL where GL_CODIGO = 2047 
	set @CO_NUMERO_Str = coalesce( @CO_NUMERO_Str, '0' ) 

	set @CO_NUMERO = 0; 
	if ( ISNUMERIC(@CO_NUMERO_Str) <>0  ) 
		set @CO_NUMERO = CAST( @CO_Numero_Str as int ) 

	if ( @CO_NUMERO = 0 )
		return 


	if ( select count(*) from ACUMULA_RS where RS_CODIGO = ''  and CO_NUMERO = @CO_NUMERO ) = 0 
		return; 

	IF NOT EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('ACUMULA_RS_SINRS_BACKUP') )
	BEGIN 
		select * into ACUMULA_RS_SINRS_BACKUP from ACUMULA_RS 
		where AC_YEAR >= 2017  and RS_CODIGO = ''  and CO_NUMERO = @CO_NUMERO
	END 


	select row_number() over ( order by AC_ID) i, AC.AC_ID, AC.CB_CODIGO, AC_YEAR, CO_NUMERO, RP.RS_CODIGO as RS_CODIGO_EMPLEADO into #AcumuladosSinRS from ACUMULA_RS AC
	join COLABORA CB on AC.CB_CODIGO = CB.CB_CODIGO 
	join RPATRON  RP on CB.CB_PATRON = RP.TB_CODIGO 
	where AC.AC_YEAR >= 2017  and AC.RS_CODIGO = ''  and AC.CO_NUMERO = @CO_NUMERO


	DECLARE @i int 
	DECLARE @max int 
	DECLARE @AC_ID FolioGrande
	DECLARE @ANIO smallint
	DECLARE @EMPLEADO int
	DECLARE @CONCEPTO int
	DECLARE @MES smallint
	DECLARE @FACTOR int
	DECLARE @MONTO numeric(15,2)
	DECLARE @RS_CODIGO Codigo

	select @max = max( i ) from #AcumuladosSinRS
	set @i = 1
	while @i <= @max 
	begin 
		select @AC_ID = AC_ID, @ANIO = AC_YEAR, @EMPLEADO = CB_CODIGO, @CONCEPTO = CO_NUMERO, @MES = 1, @FACTOR = 1, @MONTO = 0, @RS_CODIGO = RS_CODIGO_EMPLEADO
		from #AcumuladosSinRS where i = @i 	
	
		if ( @RS_CODIGO <> '' ) 
		begin 

			if ( select COUNT(*) from ACUMULA_RS where CB_CODIGO = @EMPLEADO and AC_YEAR = @ANIO and RS_CODIGO = @RS_CODIGO and CO_NUMERO = @CONCEPTO  ) = 0 
				insert into ACUMULA_RS (RS_CODIGO, AC_YEAR, CB_CODIGO, CO_NUMERO )  values ( @RS_CODIGO, @Anio, @Empleado, @Concepto );			

			update ACUMULA_RS 
			set 
				AC_ANUAL  = ACUMULA_RS.AC_ANUAL + Origen.AC_ANUAL, 
				AC_MES_01 = ACUMULA_RS.AC_MES_01 + Origen.AC_MES_01,
				AC_MES_02 = ACUMULA_RS.AC_MES_02 + Origen.AC_MES_02,
				AC_MES_03 = ACUMULA_RS.AC_MES_03 + Origen.AC_MES_03,
				AC_MES_04 = ACUMULA_RS.AC_MES_04 + Origen.AC_MES_04,
				AC_MES_05 = ACUMULA_RS.AC_MES_05 + Origen.AC_MES_05,
				AC_MES_06 = ACUMULA_RS.AC_MES_06 + Origen.AC_MES_06,
				AC_MES_07 = ACUMULA_RS.AC_MES_07 + Origen.AC_MES_07,
				AC_MES_08 = ACUMULA_RS.AC_MES_08 + Origen.AC_MES_08,
				AC_MES_09 = ACUMULA_RS.AC_MES_09 + Origen.AC_MES_09,
				AC_MES_10 = ACUMULA_RS.AC_MES_10 + Origen.AC_MES_10,
				AC_MES_11 = ACUMULA_RS.AC_MES_11 + Origen.AC_MES_11,
				AC_MES_12 = ACUMULA_RS.AC_MES_12 + Origen.AC_MES_12,
				AC_MES_13 = ACUMULA_RS.AC_MES_13 + Origen.AC_MES_13
			from ACUMULA_RS, ACUMULA_RS Origen
			where ACUMULA_RS.AC_YEAR = @ANIO and ACUMULA_RS.CB_CODIGO = @EMPLEADO and ACUMULA_RS.CO_NUMERO = @CONCEPTO  and Origen.AC_ID = @AC_ID ; 
					
			delete from ACUMULA_RS where AC_ID = @AC_ID and RS_CODIGO = '' and CO_NUMERO = @CO_NUMERO 

			exec CALCULA_ACUMULADOS_TOTALES_ANUAL @ANIO, @EMPLEADO, @CO_NUMERO 

		end 

		set @i = @i + 1 	
	end 

	drop table #AcumuladosSinRS 

end 
go

exec AJUSTA_ACUMULADOS_FIX

go 

drop procedure AJUSTA_ACUMULADOS_FIX

go 




