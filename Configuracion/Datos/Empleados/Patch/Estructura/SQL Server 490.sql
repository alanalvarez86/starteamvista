
if not exists( select NAME from SYSOBJECTS  where NAME='TPROCESO' and XTYPE='U' )
begin

CREATE TABLE TPROCESO
(
	TP_ID Status, 
	TP_MODULO  Descripcion, 
	TP_DESCRIP Formula,
	TP_VERSION Descripcion, 
	CONSTRAINT PK_TPROCESO PRIMARY KEY CLUSTERED (TP_ID) 
)

end 
go 

CREATE PROCEDURE SP_POBLAR_TPROCESO
as
begin 
SET NOCOUNT ON 
	IF (  select COUNT(*) from TPROCESO )  = 0 
	BEGIN 
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         1, 'Recursos ', 'Salario Integrado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         2, 'Recursos ', 'Cambio Salario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         3, 'Recursos ', 'Promediar Variables', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         4, 'Recursos ', 'Vacaciones Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         5, 'Recursos ', 'Aplicar Tabulador', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         6, 'Recursos ', 'Aplicar Eventos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         7, 'Recursos ', 'Importar Kardex', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         8, 'Recursos ', 'Cancelar Kardex', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (         9, 'Recursos ', 'Curso Tomado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        10, 'Asistencia ', 'Poll Relojes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        11, 'Asistencia ', 'Procesar Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        12, 'Asistencia ', 'Calculo PreN�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        13, 'Asistencia ', 'Extras y Permisos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        14, 'Asistencia ', 'Registros Autom�ticos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        15, 'Asistencia ', 'Corregir Fechas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        16, 'Asistencia ', 'Recalcular Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        17, 'N�mina ', 'Calcular', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        18, 'N�mina ', 'Afectar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        19, 'N�mina ', 'Imprimir Listado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        20, 'N�mina ', 'Imprimir Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        21, 'N�mina ', 'P�liza Contable', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        22, 'N�mina ', 'Desafectar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        23, 'N�mina ', 'Limpiar Acumulados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        24, 'N�mina ', 'Importar Excepciones de N�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        25, 'N�mina ', 'Exportar Movimientos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        26, 'N�mina ', 'Pagos Por Fuera', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        27, 'N�mina ', 'Cancelar N�minas Pasadas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        28, 'N�mina ', 'Liquidaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        29, 'N�mina ', 'Calcular Salario Neto/Bruto', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        30, 'N�mina ', 'Rastrear C�lculo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        31, 'N�mina ', 'Foliar Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        32, 'N�mina ', 'Definir Per�odos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        33, 'N�mina ', 'Calcular Aguinaldo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        34, 'N�mina ', 'Reparto de Ahorro', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        35, 'N�mina ', 'Calcular PTU', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        36, 'N�mina ', 'Declaraci�n Subsidio al Empleo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        37, 'N�mina ', 'Declaraci�n de ISPT Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        38, 'N�mina ', 'Recalcular Acumulados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        39, 'IMSS ', 'Calcular Pagos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        40, 'IMSS ', 'Calcular Recargos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        41, 'IMSS ', 'Revisar SUA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        42, 'IMSS ', 'Exportar SUA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        43, 'IMSS ', 'Calcular Prima de Riesgo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        44, 'IMSS ', 'Calcular Tasa INFONAVIT', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        45, 'Asistencia ', 'Ajuste Colectivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        46, 'Sistema ', 'Borrar Bajas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        47, 'Sistema ', 'Borrar Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        48, 'Sistema ', 'Depurar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        49, 'Sistema ', 'Migraci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        50, 'Recursos ', 'Transferencia de Empleados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        51, 'N�mina ', 'Pagar Aguinaldo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        52, 'N�mina ', 'Pagar Reparto de Ahorro', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        53, 'N�mina ', 'Pagar PTU', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        54, 'Sistema ', 'Cierre de Ahorros', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        55, 'Sistema ', 'Cierre de Pr�stamos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        56, 'Sistema ', 'Borrar N�minas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        57, 'N�mina ', 'Pagar ISPT Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        58, 'N�mina ', 'Pago de Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        59, 'N�mina ', 'ReFoliar Recibos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        60, 'N�mina ', 'Copiar N�minas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        61, 'N�mina ', 'Calcular Diferencias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        62, 'Recursos ', 'Cancelar Vacaciones Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        63, 'Sistema ', 'Borrar POLL', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        64, 'Labor ', 'Calcular Tiempos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        65, 'Labor ', 'Depuraci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        66, 'Labor ', 'Procesar Lecturas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        67, 'Labor ', 'Importar Ordenes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        68, 'Consultas ', 'Reportes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        69, 'Recursos ', 'Cierre Global De Vacaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        70, 'Recursos ', 'Entregar Herramienta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        71, 'Recursos ', 'Regresar Herramienta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        72, 'Sistema ', 'Borrar Herramienta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        73, 'Labor ', 'Importar Partes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        74, 'N�mina ', 'Preparar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        75, 'Selecci�n ', 'Depurar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        76, 'Labor ', 'Cancelar Breaks', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        77, 'Recursos ', 'Importar Altas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        78, 'N�mina ', 'SUBE Aplicado Mensual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        79, 'Recursos ', 'Renumeraci�n de Empleados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        80, 'Recursos ', 'Permisos Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        81, 'N�mina ', 'Calcular Retroactivos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        82, 'Labor ', 'Afectar Labor', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        83, 'Labor ', 'Importar Componentes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        84, 'Recursos ', 'Cancelar Curso Tomado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        85, 'N�minas ', 'Declaraci�n Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        86, 'Cafeter�a ', 'Registro Grupal de Comidas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        87, 'Cafeter�a ', 'Corregir Fechas Globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        88, 'Labor ', 'Importar C�dulas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        89, 'Labor ', 'Importar Cambio de Area', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        90, 'Labor ', 'Importar Producci�n Individual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        91, 'Sistema ', 'Actualizar Numero de Tarjeta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        92, 'Recursos ', 'Cambio Masivo de Turnos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        93, 'Evaluaci�n ', 'Agregar Empleados a evaluar global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        94, 'Evaluaci�n ', 'Preparar Evaluaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        95, 'Evaluaci�n ', 'Calcular resultados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        96, 'Evaluaci�n ', 'Recalcular promedios', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        97, 'Evaluaci�n ', 'Cerrar Evaluaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        98, 'Recursos ', 'Importar cursos tomados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (        99, 'Recursos ', 'Cancelar cierre de vacaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       100, 'Evaluaci�n ', 'Enviar correos de invitaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       101, 'Evaluaci�n ', 'Enviar correos de notificaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       102, 'Evaluaci�n ', 'Asignaci�n de relaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       103, 'Presupuestos ', 'Depuraci�n de Supuestos de Personal', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       104, 'Presupuestos ', 'Depuraci�n de N�minas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       105, 'Presupuestos ', 'Calculo del Cubo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       106, 'Presupuestos ', 'Simulaci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       107, 'Evaluaci�n ', 'Crear encuesta simple', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       108, 'Asistencia ', 'Intercambio de festivos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       109, 'Asistencia ', 'Cancelar excepciones de festivos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       110, 'Recursos ', 'Cancelar permisos globales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       111, 'Recursos ', 'Importar ahorros y pr�stamos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       112, 'Recursos ', 'Recalcular saldos de vacaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       113, 'Adm. de Guardias ', 'C�lculo de Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       114, 'Adm. de Guardias ', 'Cambiar Plazas Vac.', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       115, 'Adm. de Guardias ', 'Enviar a transici�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       116, 'Adm. de Guardias ', 'Calendario Guardias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       117, 'Adm. de Guardias ', 'Subieron', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       118, 'Adm. de Guardias ', 'Bajaron', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       119, 'Adm. de Guardias ', 'Permanecen a bordo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       120, 'Adm. de Guardias ', 'Permanecen en tierra', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       121, 'Log�stica ', 'Programar Viajes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       122, 'Log�stica ', 'Asignar Viaje', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       123, 'Adm. de Guardias ', 'Liberar a Log�stica', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       124, 'Log�stica ', 'Poll PDA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       125, 'Adm. de Guardias ', 'Cambio Status Vacante', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       126, 'Adm. de Guardias ', 'Generar Recibo Empleado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       127, 'Transporte ', 'Programar viajes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       128, 'Transporte ', 'Asignar viajes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       129, 'Transporte ', 'Importar registros de viaje', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       130, 'Transporte ', 'PDA de pasajeros', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       131, '', 'Aplicar Permutas de Asistencia', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       132, '', 'Depurar Permutas de Asistencia', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       133, 'Adm. de Guardias ', 'Alta M�dica', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       134, 'N�mina ', 'Ajuste de Retenci�n Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       135, 'N�mina ', 'Calcular Pago Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       136, 'Adm. de Guardias ', 'Generar Plantillas OT', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       137, 'Sincronizador ', 'Exportar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       138, 'Sincronizador ', 'Respaldar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       139, 'Sincronizador ', 'Comprimir', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       140, 'Sincronizador ', 'Transferir', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       141, 'Sincronizador ', 'Descomprimir', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       142, 'Sincronizador ', 'Restaurar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       143, 'Sincronizador ', 'Importar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       144, 'Adm. de Guardias ', 'Importar Horas Extras', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       145, 'Sistema ', 'Enrolamiento Masivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       146, 'Sistema ', 'Importar Enrolamiento Masivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       147, 'Configurador Reportes ', 'Exportar Diccionario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       148, 'Configurador Reportes ', 'Importar Diccionario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       149, 'IMSS ', 'Ajuste de Retenci�n INFONAVIT', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       150, 'Adm. de Guardias ', 'Generar Solicitudes PDA', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       151, '', 'Adm. de Guardias = Abrir Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       152, 'Cabinas y Camas ', 'Generar Cabinas y Camas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       153, 'Adm. de Guardias ', 'Cambio Registro Patronal', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       154, 'Asistencia ', 'Reprocesar Tarjetas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       155, 'Recursos ', 'Aplicar Finiquitos Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       156, 'Recursos ', 'Simulaci�n Finiquitos Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       157, 'Recursos ', 'Importar Kardex de Infonavit', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       158, 'Recursos ', 'Curso Programado Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       159, 'Recursos ', 'Cancelar Curso Programado Global', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       160, 'Asistencia ', 'Ajustar Incapacidades en Calendario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       161, 'Talleres estrella ', 'C�lculo de tarifas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       162, 'Recursos ', 'Foliar Capacitaciones de STPS', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       163, 'N�mina ', 'Cierre Declaraci�n Anual', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       164, 'Asistencia ', 'Autorizaci�n de Pre-N�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       165, 'Asistencia ', 'Registro de Excepciones de Festivo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       166, 'IMSS ', 'Validaci�n Movimientos IDSE', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       167, 'N�mina ', 'Transferencias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       168, 'N�mina ', 'C�lculo de Costeo', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       169, 'N�mina ', 'Cancelaci�n de Transferencias', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       170, 'Asistencia ', 'Importaci�n de Clasificaciones', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       171, 'Sistema ', 'Borrar Bit�cora Terminales GTI', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       172, 'Sistema ', 'Asigna N�meros Biom�tricos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       173, 'Sistema ', 'Asigna Grupo de Terminales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       174, 'Recursos ', 'Importar Seguros de Gastos M�dicos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       175, 'Recursos ', 'Renovar Seguros de Gastos M�dicos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       176, 'Recursos ', 'Borrar Seguros de Gastos M�dicos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       177, 'N�mina ', 'C�lculo Previo ISR', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       178, 'Recursos ', 'Revisar datos para STPS', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       179, 'Recursos ', 'Reiniciar Folio Capacitaciones de STPS', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       180, 'Sistema ', 'Importaci�n de Terminales', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       181, 'Recursos ', 'Importar Organigrama', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       182, 'N�mina ', 'Timbrar', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       183, 'Sistema ', 'Actualizaci�n de Base de Datos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       184, 'Sistema ', 'Importar Diccionario', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       185, 'Sistema ', 'Importar Reporte', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       186, 'Sistema ', 'Script Especial', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       187, 'Sistema ', 'Preparar Presupuestos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       188, 'Sistema ', 'Importar Tablas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       189, 'Recursos ', 'Importar Imagenes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       190, 'Sistema ', 'Importar Tablas en CSV', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       191, 'Sistema ', 'Agregar Base de Datos Seleccion', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       192, 'Sistema ', 'Agregar Base de Datos Visitantes', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       193, 'Sistema ', 'Agregar Base de Datos Pruebas', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       194, 'Sistema ', 'Agregar Base de Datos Tress', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       195, 'Sistema ', 'Borrar estatus de Timbrado', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       196, 'Sistema ', 'Extraer checadas Terminales GTI', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       197, 'Sistema ', 'Transformar checadas Terminales GTI', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       198, 'N�mina ', 'Cancelar Ajuste de Retenci�n Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       199, 'Recursos ', 'Cambio Masivo de Turnos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       200, 'Empleados ', 'Pensi�n Alimenticia', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       201, 'Cafeter�a ', 'Historial', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       202, 'N�mina ', 'Importar Acumulados Raz�n social', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       203, 'N�mina ', 'Recalcular Ahorros', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       204, 'Sistema ', 'Copiar Configuraci�n', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       205, 'Evaluaci�n ', 'Agregar encuesta', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       206, 'N�mina ', 'Recalcular Pr�stamos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       207, 'Sistema ', 'Agregar nuevo registro al Administrador de env�os programados', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       208, 'Sistema ', 'Servicio de Correos', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       209, 'N�mina ', 'Importar c�dula de cr�ditos Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       210, 'N�mina ', 'Generaci�n de c�dula de pago Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       211, 'N�mina ', 'Eliminar registros de c�dula Fonacot', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       212, 'N�mina ', 'Conciliaci�n de periodos de n�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       213, 'N�mina ', 'Aplicar conciliaci�n a periodos de n�mina', 490 );
		INSERT INTO TPROCESO ( TP_ID, TP_MODULO, TP_DESCRIP, TP_VERSION ) VALUES (       214, 'N�mina ', 'Cambiar estatus a pendiente de empleado', 490 );
	END 
END 
go 

exec SP_POBLAR_TPROCESO

go 

drop procedure SP_POBLAR_TPROCESO

go 
 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('Analitica_Iniciar_ConciliadorIMSS') AND type IN ( N'P', N'PC' ))     
drop procedure Analitica_Iniciar_ConciliadorIMSS  

go 
create procedure Analitica_Iniciar_ConciliadorIMSS
as
begin 
    if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'ConciliadorIMSS' ) = 0 
    begin 
        exec Analitica_Upsert 'ConciliadorIMSS' , 400000, 'CONCIL' ,   'Conciliador IMSS'
        exec Analitica_Upsert 'ConciliadorIMSS' , 300000, 'CONCIL' ,  'Conciliador SUA'
    end 
end 

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('Analitica_Iniciar_Empleados') AND type IN ( N'P', N'PC' ))     
drop procedure Analitica_Iniciar_Empleados  

go 
create procedure Analitica_Iniciar_Empleados
as
begin 
    if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Empleados' ) = 0 
    begin 
        exec Analitica_Upsert 'Empleados' , 14, 'EMP_GM' ,  'Administraci�n de Seguros de Gastos M�dicos'
        exec Analitica_Upsert 'Empleados' , 15, 'EMP_PV' ,  'Plan de vacaciones' 
		exec Analitica_Upsert 'Empleados' , 16, 'EMP_VP' ,  'Valuaci�n de puestos' 
		exec Analitica_Upsert 'Empleados' , 17, 'EMP_DP' ,  'Descripci�n de puestos'
    end 
end 

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('Analitica_Iniciar_Cursos') AND type IN ( N'P', N'PC' ))     
drop procedure Analitica_Iniciar_Cursos  

go 
create procedure Analitica_Iniciar_Cursos
as
begin 
    if ( select COUNT(*) from T_Analitica where AnaliticaModulo = 'Cursos' ) = 0 
    begin 
        exec Analitica_Upsert 'Cursos' , 1, 'CUR_MF' ,  'Matriz de Funciones'
        exec Analitica_Upsert 'Cursos' , 2, 'CUR_CO' ,  'Total de competencias evaluadas' 
    end 
end 

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_ContabilizarUso') AND type IN ( N'P', N'PC' ))
    drop procedure Analitica_ContabilizarUso 
go

create  procedure Analitica_ContabilizarUso(@Modulo Descripcion,  @HelpContext FolioGrande, @Cuantos FolioGrande) 
as
begin 
    set nocount on 
    declare @ValorActual FolioGrande 

    update T_Analitica set  AnaliticaNumero = ta.AnaliticaNumero + @Cuantos, AnaliticaFecha = GETDATE() from  T_Analitica  ta
    where ta.AnaliticaModulo = @Modulo and ta.AnaliticaHelpContext = @HelpContext
    
end 

go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Poblar_Empleados') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Poblar_Empleados
go

create  procedure Analitica_Poblar_Empleados
as
begin 
	set nocount on 
	declare @Cuantos FolioGrande 
	set @Cuantos = 0 

	select @Cuantos = count(*) from EMP_POLIZA
	where EP_FEC_FIN >= DATEADD( year, -1, getdate() );	 
	exec Analitica_Upsert 'Empleados' , 14, 'EMP_GM' ,  '', 'N', @Cuantos
		
	select @Cuantos = count(vp.cb_codigo) from VACAPLAN vp
	inner join COLABORA c on vp.CB_CODIGO = c.CB_CODIGO
	where c.CB_ACTIVO = 'S' and vp.VP_FEC_FIN >=  DATEADD( year, -1, getdate() ) and vp.VP_STATUS = 3  ;
	exec Analitica_Upsert 'Empleados' , 15, 'EMP_PV' ,  '', 'N', @Cuantos


	select @Cuantos = COUNT(vp_folio) from v_val_pto vp join PUESTO
	p on vp.PU_CODIGO = p.PU_CODIGO where p.PU_ACTIVO = 'S'  and vp.VP_FEC_FIN >=  DATEADD( year, -1, getdate() ) ;		
	exec Analitica_Upsert 'Empleados' , 16, 'EMP_VP' ,   '', 'N', @Cuantos

	select @Cuantos=COUNT(PUESTO.PU_CODIGO)  from PERFIL 
	join PUESTO	on PERFIL.PU_CODIGO = PUESTO.PU_CODIGO where PUESTO.PU_ACTIVO = 'S'
	exec Analitica_Upsert 'Empleados' , 17, 'EMP_DP' ,   '', 'N', @Cuantos

end 

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Poblar_Procesos') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Poblar_Procesos 
go

create  procedure Analitica_Poblar_Procesos
as
begin 
	set nocount on 
	DECLARE @ProcesosResumen TABLE 
	( 
		iProceso int identity (1,1), 
		PC_PROC_ID  int, 
		Modulo		varchar(30), 
		Cuantos		int, 
		Descrip		varchar(255), 
		FechaMax	datetime
	) 

	insert into @ProcesosResumen (PC_PROC_ID, Modulo, Descrip, Cuantos, FechaMax ) 
	select  PROCESO.PC_PROC_ID,  coalesce( TP.TP_MODULO, 'N/D') Modulo,  coalesce( TP.TP_DESCRIP, 'N/D') Descrip,  count(*) Cuantos, MAX(PC_FEC_INI) FechaMax
	from PROCESO 
		LEFT OUTER JOIN TPROCESO TP ON TP.TP_ID = PROCESO.PC_PROC_ID 
		where PC_FEC_INI >=  DATEADD( year, -1, getdate() ) 
	group by TP.TP_MODULO, PROCESO.PC_PROC_ID, TP.TP_DESCRIP 
	order by PROCESO.PC_PROC_ID 

	declare @n int 
	declare @i int 
	select @n= COUNT(*) from @ProcesosResumen 
	set @i=1 

	declare @PC_PROC_ID  int 
	declare @Modulo		 varchar(30)
	declare @Cuantos	 int
	declare @Descrip	 varchar(255)
	declare @FechaMax	 datetime
	declare @Codigo		 Codigo10 

	while @i <= @n 
	begin 
		select @PC_PROC_ID = PC_PROC_ID, @Modulo = Modulo, @Cuantos = Cuantos, @Descrip = Descrip, @FechaMax = FechaMax from @ProcesosResumen where iProceso = @i 

		set @Codigo = 'PR'+CAST( @PC_PROC_ID as varchar(3) ) 

		exec Analitica_Upsert 'Procesos' , @PC_PROC_ID, @Codigo, @Descrip, 'N', @Cuantos, @FechaMax  

		set @i = @i + 1 
	end 

end 

go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Poblar_Cursos') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Poblar_Cursos
go


	
create procedure Analitica_Poblar_Cursos
as 
begin 
	set nocount on 
	declare @Cuantos FolioGrande 
	set @Cuantos = 0 
	

	select @Cuantos = count(*) from C_PER_COMP
	where RP_FEC_INI >= DATEADD( year, -1, getdate() );	 
		
    exec Analitica_Upsert 'Cursos' , 1, 'CUR_MF' ,  'Matriz de Funciones', 'N', @Cuantos 

	select @Cuantos = count(*) from C_EVA_COMP
	where EC_FECHA >= DATEADD( year, -1, getdate() );	 

    exec Analitica_Upsert 'Cursos' , 2, 'CUR_CO' ,  'Total de competencias evaluadas', 'N', @Cuantos 


end 

GO 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'Analitica_Poblar') AND type IN ( N'P', N'PC' ))
	drop procedure Analitica_Poblar
go

create procedure Analitica_Poblar
as
begin 
	exec Analitica_Poblar_Version2017; 
	exec Analitica_Poblar_Version2018; 		
	exec Analitica_Poblar_Empleados; 	
	exec Analitica_Poblar_Procesos; 
	exec Analitica_Poblar_Cursos; 
end; 
go

exec Analitica_Iniciar_Empleados

go 
exec Analitica_Iniciar_ConciliadorIMSS
go 

exec Analitica_Poblar
go

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID('AuditoriaConceptosTimbradoTRESS') AND type IN ( N'P', N'PC' ))     
drop procedure AuditoriaConceptosTimbradoTRESS

go 
create procedure AuditoriaConceptosTimbradoTRESS
as
begin 
    Select 
	cast
	( 
	( 
		SELECT
		CO_NUMERO ConceptoID,
		CO_DESCRIP ConceptoDescripcion,
		case CO_CALCULA
			when 'S' then 1 
			else 0 
		end  ConceptoCalcular,
		case CO_ACTIVO 
			when 'S' then 1 
			else 0 
		end ConceptoActivo,
		CO_TIPO ConceptoTipo,
		CO_FORMULA ConceptoFormula,
		CO_FRM_ALT ConceptoFormulaAlterna,
		CO_IMP_CAL ConceptoISPTIndividual,
		case CO_G_ISPT
			when 'S' then 1 
			else 0 
		end ConceptoISPTGravar,
		CO_X_ISPT ConceptoISPTExentoFormula,
		CO_SAT_CLN ConceptoClaseMontoNegativo,
		CO_SAT_CLP ConceptoClaseMontoPositivo,
		CO_SAT_TPN ConceptoSATMontoNegativoClave,
		CO_SAT_TPP ConceptoSATMontoPositivoClave,
		CO_SAT_EXE ConceptoPercepcionExentaTratamiento,
		CO_SAT_CON ConceptoExentoConceptoId
		FROM CONCEPTO
		ORDER BY 1
	FOR XML RAW ('ConceptosTRESS'), ROOT ('ConceptosSistemaTRESS'), ELEMENTS
	) as varchar(max) )  ConceptosXML  
end
go


if not exists (select * from syscolumns where id=object_id('CONCEPTO') and name='CO_TIMBRA')
	alter table CONCEPTO add CO_TIMBRA booleano null;
go

UPDATE CONCEPTO  
SET     CO_TIMBRA =  CASE  
                        WHEN CO_TIPO IN (1, 4) OR (CO_SAT_CLP > 0) OR (CO_SAT_CLN > 0) OR (CO_SAT_EXE > 0) THEN 'S'  
                        ELSE 'N'
                    END 
WHERE CO_TIMBRA is NULL 
go 

alter table CONCEPTO alter column CO_TIMBRA booleano not null;
go

if exists( select NAME from SYSOBJECTS where NAME = 'VWSDIAGNOS' and XTYPE = 'V' )
	drop view VWSDIAGNOS
go

create view VWSDIAGNOS(
	WS_FECHA,
	CH_RELOJ,
	WS_DATOS)
as
	select DATEADD(dd, 0, DATEDIFF(dd, 0, WS_FECHA)) as WS_FECHA, 
		CH_RELOJ,
		WS_DATOS 
	from #COMPARTE.dbo.WS_DIAGNOS
GO

if exists (select * from dbo.sysobjects where id = object_id(N'FONACOT_CEDULA_TRABAJADOR') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_CEDULA_TRABAJADOR
GO 

create function FONACOT_CEDULA_TRABAJADOR
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
(
	NO_CT int,
	ANIO_EMISION smallint,
	MES_EMISION smallint,
    NO_FONACOT varchar(30),
    RFC varchar(30),
    NO_SS varchar(11),
	NOMBRE VARCHAR(250),
	CLAVE_EMPLEADO int,
	CTD_CREDITOS int,
	RETENCION_MENSUAL numeric(15,2),
	TIPO char(1),
	RETENCION_REAL numeric (15,2),
	FECHA_BAJA varchar(10),
	FECHA_INI varchar(10),
	FECHA_FIN varchar(10)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_CT, ANIO_EMISION, MES_EMISION, NO_FONACOT, RFC, NO_SS, NOMBRE, CLAVE_EMPLEADO, CTD_CREDITOS, RETENCION_MENSUAL, TIPO, RETENCION_REAL,
		FECHA_BAJA, FECHA_INI, FECHA_FIN)	

	select PE.PE_NO_CT, @YEAR ANIO_EMISION, @MES MES_EMISION, CB_FONACOT, CB_RFC, CB_SEGSOC, PRETTYNAME, CB_CODIGO, PF_CTD, PF_PAGO,			
			case
				when FE_INCIDE = 'B' then FE_INCIDE
			    when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '0'				
				when FE_INCIDE not in ('B', '0', 'I', 'L', 'M' ) then '0' 				
				else FE_INCIDE 				
			end as 
			FE_INCIDE,
			FC_NOMINA+FC_AJUSTE AS RETENCION_REAL,
			case 
				when FE_INCIDE = 'B' then FE_FECHA1 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 		
				else '' 
			end as 
			FECHA_BAJA,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA1 
			end  as 
			FECHA_INI,
			case 
				when FE_INCIDE = 'B' then '' 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA2
			end  as 
			FECHA_FIN
	from VCED_DET VC, PCED_ENC PE
	WHERE 	PF_YEAR = @YEAR AND PF_MES = @MES	AND ( VC.RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')
	AND VC.PF_YEAR = PE.PE_YEAR AND VC.PF_MES = PE.PE_MES
	AND (VC.RS_CODIGO = PE.RS_CODIGO OR PE.RS_CODIGO = '')
		
	return
END
GO 