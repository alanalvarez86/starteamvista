/* Tabla TDEFECTO: Nuevo */
/* Patch 376 # Seq: 1 Agregado */
CREATE TABLE TDEFECTO (
       TB_CODIGO            Codigo,
       TB_ELEMENT           Descripcion,
       TB_INGLES            Descripcion,
       TB_NUMERO            Pesos,
       TB_TEXTO             Descripcion
)
GO

/* PK TDEFECTO: Nuevo */
/* Patch 376 # Seq: 2 Agregado */
ALTER TABLE TDEFECTO
       ADD CONSTRAINT PK_TDEFECTO PRIMARY KEY (TB_CODIGO)
GO

/* Tabla CED_INSP: Nuevo */
/* Patch 376 # Seq: 3 Agregado */
CREATE TABLE CED_INSP (
       CI_FOLIO             FolioGrande,
       CI_AREA              Codigo,
       CE_FOLIO             FolioGrande,
       WO_NUMBER            OrdenTrabajo,
       AR_CODIGO            CodigoParte,
       CI_FECHA             Fecha,
       CI_HORA              Hora,
       US_CODIGO            Usuario,
       CI_TIPO              Status,
       CI_RESULT            Status,
       CI_COMENTA           DescLarga,
       CI_OBSERVA           Memo,
       CI_TAMLOTE           Piezas,
       CI_MUESTRA           Piezas,
       CI_TIEMPO            Minutos,
       CI_NUMERO1           Pesos,
       CI_NUMERO2           Pesos
)
GO

/* PK CED_INSP: Nuevo */
/* Patch 376 # Seq: 4 Agregado */
ALTER TABLE CED_INSP
       ADD CONSTRAINT PK_CED_INSP PRIMARY KEY (CI_FOLIO)
GO

/* Tabla DEFECTO: Nuevo */
/* Patch 376 # Seq: 5 Agregado */
CREATE TABLE DEFECTO (
       CI_FOLIO             FolioGrande,
       DE_FOLIO             FolioChico,
       DE_CODIGO            Codigo,
       DE_PIEZAS            Piezas,
       DE_COMENTA           DescLarga
)
GO

/* PK DEFECTO: Nuevo */
/* Patch 376 # Seq: 6 Agregado */
ALTER TABLE DEFECTO
       ADD CONSTRAINT PK_DEFECTO PRIMARY KEY (CI_FOLIO, DE_FOLIO)
GO

/* FK DEFECTO -> CED_INSP: Nuevo */
/* Patch 376 # Seq: 7 Agregado */
ALTER TABLE DEFECTO
       ADD CONSTRAINT FK_DEFECTO_CED_INSP
       FOREIGN KEY (CI_FOLIO)
       REFERENCES CED_INSP
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* FK DEFECTO -> TDEFECTO: Nuevo */
/* Patch 376 # Seq: 8 Agregado */
ALTER TABLE DEFECTO
       ADD CONSTRAINT FK_DEFECTO_TDEFECTO
       FOREIGN KEY (DE_CODIGO)
       REFERENCES TDEFECTO
       ON DELETE NO ACTION
       ON UPDATE CASCADE;
GO

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

/* SP_TITULO_CAMPO: Agregar entidad de CED_INSP, DEFECTO */
/* Patch 380 # Seq: 185 Modificado */
ALTER PROCEDURE DBO.SP_TITULO_CAMPO(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
BEGIN
        SET NOCOUNT ON
  	DECLARE @TITULO VARCHAR(30)

	select @Titulo = GL_FORMULA
  	from GLOBAL
  	where GL_CODIGO = @NumGlobal

  	if (( @Titulo is Null ) or ( @Titulo = '' ))
    		SET @Titulo = @TituloDefault;

   	update DICCION
  	set DI_TITULO = @Titulo, DI_TCORTO= @Titulo, DI_CLAVES = ''
  	where ( DI_CLASIFI IN ( 5, 10, 18, 32, 33, 50, 124, 141, 142, 143, 144, 145, 146, 147, 148, 149, 152, 186, 187 ) ) and
	and DI_NOMBRE = @Nombre;
END
GO

/* CV: 06-Febrero-2003 */
/* Este stored procedure se utiliza para refrescar el diccionario de datos*/
/* Se cambio este stored procedure para que tambien cambie los datos  */
/* del m�dulo Labor - Defectos */
/* SP_REFRESH_DICCION: Agregar entidades de CED_INSP, DEFECTO */
/* Patch 380 # Seq: 195 Modificado */
ALTER PROCEDURE SP_REFRESH_DICCION AS
begin
  execute procedure SP_TITULO_TABLA( 13, 'Nivel de Organigrama #1', 41 );
  execute procedure SP_TITULO_TABLA( 14, 'Nivel de Organigrama #2', 42 );
  execute procedure SP_TITULO_TABLA( 15, 'Nivel de Organigrama #3', 43 );
  execute procedure SP_TITULO_TABLA( 16, 'Nivel de Organigrama #4', 44 );
  execute procedure SP_TITULO_TABLA( 17, 'Nivel de Organigrama #5', 45 );
  execute procedure SP_TITULO_TABLA( 18, 'Nivel de Organigrama #6', 46 );
  execute procedure SP_TITULO_TABLA( 19, 'Nivel de Organigrama #7', 47 );
  execute procedure SP_TITULO_TABLA( 20, 'Nivel de Organigrama #8', 48 );
  execute procedure SP_TITULO_TABLA( 21, 'Nivel de Organigrama #9', 49 );

  execute procedure SP_TITULO_TABLA( 35, 'Tabla Adicional #1', 19 );
  execute procedure SP_TITULO_TABLA( 36, 'Tabla Adicional #2', 20 );
  execute procedure SP_TITULO_TABLA( 37, 'Tabla Adicional #3', 21 );
  execute procedure SP_TITULO_TABLA( 38, 'Tabla Adicional #4', 22 );

  execute procedure SP_TITULO_TABLA( 134, 'Orden de Trabajo', 149 );
  execute procedure SP_TITULO_TABLA( 136, 'Cat�logo de Partes', 141 );
  execute procedure SP_TITULO_TABLA( 138, 'Cat�logo de Area', 148 );
  execute procedure SP_TITULO_TABLA( 140, 'Cat�logo de Operaciones', 142 );
  execute procedure SP_TITULO_TABLA( 142, 'Cat�logo de Tiempo Muerto', 138 );
  execute procedure SP_TITULO_TABLA( 176, 'Lista de Defectos', 187 );
  execute procedure SP_TITULO_TABLA( 143, 'Modulador #1', 135 );
  execute procedure SP_TITULO_TABLA( 144, 'Modulador #2', 136 );
  execute procedure SP_TITULO_TABLA( 145, 'Modulador #3', 137 );

  execute procedure SP_TITULO_CAMPO( 13, 'C�digo de Nivel #1', 'CB_NIVEL1' );
  execute procedure SP_TITULO_CAMPO( 14, 'C�digo de Nivel #2', 'CB_NIVEL2' );
  execute procedure SP_TITULO_CAMPO( 15, 'C�digo de Nivel #3', 'CB_NIVEL3' );
  execute procedure SP_TITULO_CAMPO( 16, 'C�digo de Nivel #4', 'CB_NIVEL4' );
  execute procedure SP_TITULO_CAMPO( 17, 'C�digo de Nivel #5', 'CB_NIVEL5' );
  execute procedure SP_TITULO_CAMPO( 18, 'C�digo de Nivel #6', 'CB_NIVEL6' );
  execute procedure SP_TITULO_CAMPO( 19, 'C�digo de Nivel #7', 'CB_NIVEL7' );
  execute procedure SP_TITULO_CAMPO( 20, 'C�digo de Nivel #8', 'CB_NIVEL8' );
  execute procedure SP_TITULO_CAMPO( 21, 'C�digo de Nivel #9', 'CB_NIVEL9' );

  execute procedure SP_TITULO_CAMPO( 13, 'Nuevo Nivel #1', 'EV_NIVEL1' );
  execute procedure SP_TITULO_CAMPO( 14, 'Nuevo Nivel #2', 'EV_NIVEL2' );
  execute procedure SP_TITULO_CAMPO( 15, 'Nuevo Nivel #3', 'EV_NIVEL3' );
  execute procedure SP_TITULO_CAMPO( 16, 'Nuevo Nivel #4', 'EV_NIVEL4' );
  execute procedure SP_TITULO_CAMPO( 17, 'Nuevo Nivel #5', 'EV_NIVEL5' );
  execute procedure SP_TITULO_CAMPO( 18, 'Nuevo Nivel #6', 'EV_NIVEL6' );
  execute procedure SP_TITULO_CAMPO( 19, 'Nuevo Nivel #7', 'EV_NIVEL7' );
  execute procedure SP_TITULO_CAMPO( 20, 'Nuevo Nivel #8', 'EV_NIVEL8' );
  execute procedure SP_TITULO_CAMPO( 21, 'Nuevo Nivel #9', 'EV_NIVEL9' );

  execute procedure SP_TITULO_CAMPO( 22, 'Fecha Adicional #1', 'CB_G_FEC_1' );
  execute procedure SP_TITULO_CAMPO( 23, 'Fecha Adicional #2', 'CB_G_FEC_2' );
  execute procedure SP_TITULO_CAMPO( 24, 'Fecha Adicional #3', 'CB_G_FEC_3' );
  execute procedure SP_TITULO_CAMPO( 25, 'Texto Adicional #1', 'CB_G_TEX_1' );
  execute procedure SP_TITULO_CAMPO( 26, 'Texto Adicional #2', 'CB_G_TEX_2' );
  execute procedure SP_TITULO_CAMPO( 27, 'Texto Adicional #3', 'CB_G_TEX_3' );
  execute procedure SP_TITULO_CAMPO( 28, 'Texto Adicional #4', 'CB_G_TEX_4' );
  execute procedure SP_TITULO_CAMPO( 29, 'N�mero Adicional #1', 'CB_G_NUM_1' );
  execute procedure SP_TITULO_CAMPO( 30, 'N�mero Adicional #2', 'CB_G_NUM_2' );
  execute procedure SP_TITULO_CAMPO( 31, 'N�mero Adicional #3', 'CB_G_NUM_3' );
  execute procedure SP_TITULO_CAMPO( 32, 'L�gico Adicional #1', 'CB_G_LOG_1' );
  execute procedure SP_TITULO_CAMPO( 33, 'L�gico Adicional #2', 'CB_G_LOG_2' );
  execute procedure SP_TITULO_CAMPO( 34, 'L�gico Adicional #3', 'CB_G_LOG_3' );
  execute procedure SP_TITULO_CAMPO( 35, 'Tabla Adicional #1','CB_G_TAB_1' );
  execute procedure SP_TITULO_CAMPO( 36, 'Tabla Adicional #2','CB_G_TAB_2' );
  execute procedure SP_TITULO_CAMPO( 37, 'Tabla Adicional #3','CB_G_TAB_3' );
  execute procedure SP_TITULO_CAMPO( 38, 'Tabla Adicional #4','CB_G_TAB_4' );

  execute procedure SP_TITULO_CAMPO( 134, 'Orden de Trabajo','WO_NUMBER' );
  execute procedure SP_TITULO_CAMPO( 136, 'Parte','AR_CODIGO' );
  execute procedure SP_TITULO_CAMPO( 138, 'Area','CB_AREA' );
  execute procedure SP_TITULO_CAMPO( 138, 'Area','CI_AREA' );
  execute procedure SP_TITULO_CAMPO( 138, 'Area','CE_AREA' );
  execute procedure SP_TITULO_CAMPO( 140, 'Operaci�n','OP_NUMBER' );
  execute procedure SP_TITULO_CAMPO( 142, 'Tiempo Muerto','WK_TMUERTO' );
  execute procedure SP_TITULO_CAMPO( 142, 'Tiempo Muerto','LX_TMUERTO' );
  execute procedure SP_TITULO_CAMPO( 176, 'Defecto','DE_CODIGO' );
  execute procedure SP_TITULO_CAMPO( 143, 'Modulador #1','LX_MODULA1' );
  execute procedure SP_TITULO_CAMPO( 144, 'Modulador #2','LX_MODULA2' );
  execute procedure SP_TITULO_CAMPO( 145, 'Modulador #3','LX_MODULA3' );
  execute procedure SP_TITULO_CAMPO( 143, 'Modulador #1','WK_MOD_1' );
  execute procedure SP_TITULO_CAMPO( 144, 'Modulador #2','WK_MOD_2' );
  execute procedure SP_TITULO_CAMPO( 145, 'Modulador #3','WK_MOD_3' );

  execute procedure SP_TITULO_CONTEO ( 161, 'Criterio #1',  'CT_NIVEL_1' );
  execute procedure SP_TITULO_CONTEO ( 162, 'Criterio #2',  'CT_NIVEL_2' );
  execute procedure SP_TITULO_CONTEO ( 163, 'Criterio #3',  'CT_NIVEL_3' );
  execute procedure SP_TITULO_CONTEO ( 164, 'Criterio #4',  'CT_NIVEL_4' );
  execute procedure SP_TITULO_CONTEO ( 165, 'Criterio #5',  'CT_NIVEL_5' );
end
GO
