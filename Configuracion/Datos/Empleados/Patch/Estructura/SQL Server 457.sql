/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'RECALCULA_AHORROS') AND type IN ( N'P', N'PC' ))
	drop procedure RECALCULA_AHORROS
go

CREATE PROCEDURE RECALCULA_AHORROS
    				@EMPLEADO INTEGER,
    				@STATUS SMALLINT,
					@AH_TIPO Codigo1 = ''
AS
        SET NOCOUNT ON
	DECLARE @CONCEPTO INTEGER;
	DECLARE @NUMERO INTEGER;
	DECLARE @TOTAL NUMERIC(15,2);
	DECLARE @TIPOAHORRO CHAR(1);
	DECLARE @FECHAINICIAL DATETIME;

	DECLARE TemporalAhorro CURSOR for
		select A.AH_TIPO, T.TB_CONCEPT, A.AH_FECHA
        from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO )
        where ( A.CB_CODIGO = @Empleado ) and ( A.AH_STATUS = 0 ) and ( A.AH_TIPO = @AH_TIPO or @AH_TIPO = '' )

	OPEN TemporalAhorro

	FETCH NEXT FROM TemporalAhorro
    into @TipoAhorro, @Concepto, @FechaInicial 
	WHILE @@FETCH_STATUS = 0
    begin
		select @Numero = COUNT(*), 
				@Total = SUM( M.MO_DEDUCCI )
        from MOVIMIEN M 
		left outer join NOMINA N on
               ( N.PE_YEAR = M.PE_YEAR ) and
               ( N.PE_TIPO = M.PE_TIPO ) and
               ( N.PE_NUMERO = M.PE_NUMERO ) and
               ( N.CB_CODIGO = @Empleado )
		left outer join PERIODO P on
               ( N.PE_YEAR = P.PE_YEAR ) and
               ( N.PE_TIPO = P.PE_TIPO ) and
               ( N.PE_NUMERO = P.PE_NUMERO )
        where ( M.CB_CODIGO = @Empleado ) and
               ( M.CO_NUMERO = @Concepto ) and
               ( M.MO_ACTIVO = 'S' ) and
               ( N.NO_STATUS = @Status ) and
               ( P.PE_FEC_FIN >= @FechaInicial )

        if ( @Numero = 0 ) 
			Select @Total = 0;

        update AHORRO set
                 AH_NUMERO = @Numero,
                 AH_TOTAL  = @Total,
                 AH_SALDO = AH_SALDO_I + @Total + AH_ABONOS - AH_CARGOS
        where ( CB_CODIGO = @Empleado ) and
                ( AH_TIPO = @TipoAhorro );
		
		FETCH NEXT FROM TemporalAhorro
    	into @TipoAhorro, @Concepto, @FechaInicial 
     end
     Close TemporalAhorro
     Deallocate TemporalAhorro
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'RECALCULA_PRESTAMOS') AND type IN ( N'P', N'PC' ))
	drop procedure RECALCULA_PRESTAMOS
go

CREATE PROCEDURE RECALCULA_PRESTAMOS
  					@EMPLEADO INTEGER,
    				@STATUS SMALLINT,
					@PR_TIPO Codigo1 = ''
AS
        SET NOCOUNT ON
	DECLARE @CONCEPTO INTEGER;
	DECLARE @NUMERO INTEGER;
	DECLARE @TOTAL NUMERIC(15,2);
	DECLARE @TIPOPRESTAMO CHAR(1);
	DECLARE @REFERENCIA VARCHAR(8);
	DECLARE @FECHAINICIAL DATETIME;

	DECLARE TemporalPrestamo Cursor for
		select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT, P.PR_FECHA
        from PRESTAMO P 
		left outer join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO )
        where ( P.CB_CODIGO = @Empleado ) and ( P.PR_STATUS in ( 0, 2 ) ) and ( P.PR_TIPO = @PR_TIPO or @PR_TIPO = '' )
	
	OPEN TemporalPrestamo
	FETCH NEXT FROM TemporalPrestamo
	into @TipoPrestamo, @Referencia, @Concepto, @FechaInicial 
	WHILE @@FETCH_STATUS = 0
	begin
		Select @Numero = 0, @Total = 0;
		select @Numero = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
		from MOVIMIEN M 
		left outer join NOMINA N on
               ( N.PE_YEAR = M.PE_YEAR ) and
               ( N.PE_TIPO = M.PE_TIPO ) and
               ( N.PE_NUMERO = M.PE_NUMERO ) and
               ( N.CB_CODIGO = @Empleado )
		left outer join PERIODO P on
               ( N.PE_YEAR = P.PE_YEAR ) and
               ( N.PE_TIPO = P.PE_TIPO ) and
               ( N.PE_NUMERO = P.PE_NUMERO )
        where ( M.CB_CODIGO = @Empleado ) and
                ( M.CO_NUMERO = @Concepto ) and
                ( M.MO_REFEREN = @Referencia ) and
                ( M.MO_ACTIVO = 'S' ) and
                ( N.NO_STATUS = @Status ) and
                ( P.PE_FEC_FIN >= @FechaInicial )

		if ( @Numero = 0 ) Select @Total = 0;

		update PRESTAMO  set
                 PR_NUMERO = @Numero,
                 PR_TOTAL  = @Total,
                 PR_SALDO = PR_MONTO - PR_SALDO_I - @Total - PR_ABONOS + PR_CARGOS
		where ( CB_CODIGO = @Empleado ) and
                ( PR_TIPO = @TipoPrestamo ) and
                ( PR_REFEREN = @Referencia );

		FETCH NEXT FROM TemporalPrestamo
		into @TipoPrestamo, @Referencia, @Concepto, @FechaInicial 
    end
    Close TemporalPrestamo
    Deallocate TemporalPrestamo
    EXECUTE SET_STATUS_PRESTAMOS @Empleado;
GO

IF EXISTS ( SELECT * FROM sys.indexes WHERE name='IDX_ACUMULA_RS' AND object_id = OBJECT_ID('ACUMULA_RS') ) 
ALTER INDEX IDX_ACUMULA_RS ON ACUMULA_RS REBUILD;

GO
	
	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COMPLEMENTO_SAT_1_2') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_COMPLEMENTO_SAT_1_2 
	GO

     create procedure SP_COMPLEMENTO_SAT_1_2
     as 
     begin 
	   set nocount on 
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP01' ) BEGIN
		      INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'OP01', 'Devoluci�n de ISR', 1, 4, 'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP02' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'OP02', 'Subsidio efectivamente entregado al trabajador', 2, 4, 'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP03' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'OP03', 'Vi�ticos', 3, 4, 'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'D022' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'D022', 'Impuestos Locales', 22, 2, 'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'D023' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'D023', 'Aportaciones voluntarias', 23, 2, 'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P044' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) VALUES ( 'P044', 'Jubilaciones, pensiones o haberes de retiro en parcialidades', 44, 1, 'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P045' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) 
          VALUES ( 'P045', 'Ingresos en acciones o t�tulos valor que representan bienes', 45, 1, 'S');
        END


        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P046' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO ) 
          VALUES ( 'P046', 'Ingresos asimilados a salarios', 46, 1, 'S');
        END
        IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'D018' ) BEGIN
          UPDATE TIPO_SAT SET TB_ELEMENT = 'Cuotas para la constituci�n y fomento de sociedades cooperativas y de cajas de ahorro', TB_SAT_NUM = 18, TB_SAT_CLA = 2 WHERE TB_CODIGO = 'D018';
        END
        IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P017' ) BEGIN
          UPDATE TIPO_SAT SET TB_ELEMENT = 'Subsidio para el empleo', TB_SAT_NUM = 17, TB_SAT_CLA = 1, TB_ACTIVO = 'N' WHERE TB_CODIGO = 'P017';
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM BANCO WHERE TB_CODIGO = '670' ) BEGIN
          INSERT INTO BANCO(TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN, TB_ACTIVO ) VALUES ( '670', 'LIBERTAD', 'Libertad Servicios Financieros, S.A. De C.V.', 670, 'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM BANCO WHERE TB_CODIGO = '999' ) BEGIN
          INSERT INTO BANCO(TB_CODIGO, TB_ELEMENT, TB_NOMBRE, TB_SAT_BAN, TB_ACTIVO ) VALUES ( '999', 'N/A', 'N/A', 999, 'S');
        END

        IF NOT EXISTS( SELECT [EN_CODIGO],[AT_CAMPO] FROM [dbo].[R_ATRIBUTO] WHERE [EN_CODIGO] = 69 AND [AT_CAMPO] = 'TU_SAT_JOR' ) BEGIN
          INSERT [dbo].[R_ATRIBUTO] ([EN_CODIGO], [AT_CAMPO], [AT_TITULO], [AT_TIPO], [AT_ANCHO], [AT_MASCARA], [AT_FILTRO], [AT_FILTROA], [AT_TRANGO], [AT_ENTIDAD], [LV_CODIGO], [AT_CLAVES],
          [AT_DESCRIP], [AT_TCORTO], [AT_SISTEMA], [AT_VALORAC], [AT_TOTAL], [AT_CONFI], [AT_ACTIVO], [AT_VERSION], [US_CODIGO]) VALUES (69, N'TU_SAT_JOR', N'Tipo de Jornada SAT', 4, 6, N'', 0,
          N'', 0, 0, 0, N'TIPO JORNADA SAT', N'Tipo de Jornada SAT', N'Jornada SAT', N'S', 0, 0, N'N', N'S', 450, 1);
        END

        IF NOT EXISTS( SELECT [EN_CODIGO],[AT_CAMPO] FROM [dbo].[R_ATRIBUTO] WHERE [EN_CODIGO] = 12 AND [AT_CAMPO] = 'TB_SAT_CON' ) BEGIN
          INSERT [dbo].[R_ATRIBUTO] ([EN_CODIGO], [AT_CAMPO], [AT_TITULO], [AT_TIPO], [AT_ANCHO], [AT_MASCARA], [AT_FILTRO], [AT_FILTROA], [AT_TRANGO], [AT_ENTIDAD], [LV_CODIGO], [AT_CLAVES],
          [AT_DESCRIP], [AT_TCORTO], [AT_SISTEMA], [AT_VALORAC], [AT_TOTAL], [AT_CONFI], [AT_ACTIVO], [AT_VERSION], [US_CODIGO]) VALUES (12, N'TB_SAT_CON', N'Tipo de Contrato SAT', 4, 6, N'', 0, 
          N'', 0, 0, 0, N'TIPO CONTRATO SAT', N'Tipo de Contrato SAT', N'Contrato SAT', N'S', 0, 0, N'N', N'S', 450, 1);
        END

        IF NOT EXISTS( SELECT [EN_CODIGO],[AT_CAMPO] FROM [dbo].[R_ATRIBUTO] WHERE [EN_CODIGO] = 59 AND [AT_CAMPO] = 'PU_SAT_RSG' ) BEGIN
          INSERT [dbo].[R_ATRIBUTO] ([EN_CODIGO], [AT_CAMPO], [AT_TITULO], [AT_TIPO], [AT_ANCHO], [AT_MASCARA], [AT_FILTRO], [AT_FILTROA], [AT_TRANGO], [AT_ENTIDAD], [LV_CODIGO], [AT_CLAVES],
          [AT_DESCRIP], [AT_TCORTO], [AT_SISTEMA], [AT_VALORAC], [AT_TOTAL], [AT_CONFI], [AT_ACTIVO], [AT_VERSION], [US_CODIGO]) VALUES (59, N'PU_SAT_RSG', N'Riesgo de Puesto', 4, 6, N'', 0, 
          N'', 0, 0, 0, N'RIESGO PUESTO', N'Riesgo de Puesto', N'Riesgo de Puesto', N'S', 0, 0, N'N', N'S', 450, 1);
        END

        IF NOT EXISTS( SELECT [EN_CODIGO],[RL_ORDEN] FROM [dbo].[R_RELACION] WHERE [EN_CODIGO] = 69 AND [RL_ORDEN] = 2 ) BEGIN
          INSERT [dbo].[R_RELACION] ([EN_CODIGO], [RL_ORDEN], [RL_ENTIDAD], [RL_CAMPOS], [RL_INNER], [RL_ACTIVO], [RL_VERSION], [US_CODIGO]) 
          VALUES (69, 2, 390, N'TU_SAT_JOR', N'N', N'S', 450, 1);
        END

        IF NOT EXISTS( SELECT [EN_CODIGO],[RL_ORDEN] FROM [dbo].[R_RELACION] WHERE [EN_CODIGO] = 12 AND [RL_ORDEN] = 1 ) BEGIN
          INSERT [dbo].[R_RELACION] ([EN_CODIGO], [RL_ORDEN], [RL_ENTIDAD], [RL_CAMPOS], [RL_INNER], [RL_ACTIVO], [RL_VERSION], [US_CODIGO]) 
          VALUES (12, 1, 389, N'TB_SAT_CON', N'N', N'S', 450, 1);
        END

        IF NOT EXISTS( SELECT [EN_CODIGO],[RL_ORDEN] FROM [dbo].[R_RELACION] WHERE [EN_CODIGO] = 59 AND [RL_ORDEN] = 19 ) BEGIN
          INSERT [dbo].[R_RELACION] ([EN_CODIGO], [RL_ORDEN], [RL_ENTIDAD], [RL_CAMPOS], [RL_INNER], [RL_ACTIVO], [RL_VERSION], [US_CODIGO]) 
          VALUES (59, 19, 391, N'PU_SAT_RSG', N'N', N'S', 450, 1);
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '01' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'01', N'Tiempo indeterminado', N'1', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '02' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'02', N'Obra determinada', N'2', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '03' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'03', N'Tiempo determinado', N'3', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '04' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'04', N'Temporada', N'4', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '05' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'05', N'Sujeto a prueba', N'5', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '06' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'06', N'Capacitaci�n inicial', N'6', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '07' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'07', N'Pago de hora laborada', N'7', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '08' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'08', N'Comisi�n laboral', N'8', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '09' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'09', N'No existe relaci�n de trabajo', N'9', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '10' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'10', N'Jubilaci�n, pensi�n, retiro', N'10', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATCONTRA WHERE TB_CODIGO = '99' ) BEGIN
                INSERT [dbo].[TSATCONTRA] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'99', N'Otro contrato', N'99', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END


        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '01' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'01', N'Diurna', N'1', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '02' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'02', N'Nocturna', N'2', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '03' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'03', N'Mixta', N'3', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '04' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'04', N'Por hora', N'4', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '05' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'05', N'Reducida', N'5', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '06' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'06', N'Continuada', N'6', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '07' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'07', N'Partida', N'7', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '08' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'08', N'Por turnos', N'8', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATJORNAD WHERE TB_CODIGO = '99' ) BEGIN
                INSERT [dbo].[TSATJORNAD] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'99', N'Otra Jornada', N'99', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATRIESGO WHERE TB_CODIGO = '1' ) BEGIN
                INSERT [dbo].[TSATRIESGO] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'1', N'Clase I', N'1', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATRIESGO WHERE TB_CODIGO = '2' ) BEGIN
                INSERT [dbo].[TSATRIESGO] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'2', N'Clase II', N'2', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATRIESGO WHERE TB_CODIGO = '3' ) BEGIN
                INSERT [dbo].[TSATRIESGO] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'3', N'Clase III', N'3', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATRIESGO WHERE TB_CODIGO = '4' ) BEGIN
                INSERT [dbo].[TSATRIESGO] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'4', N'Clase IV', N'4', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END
        IF NOT EXISTS( SELECT TB_CODIGO FROM TSATRIESGO WHERE TB_CODIGO = '5' ) BEGIN
                INSERT [dbo].[TSATRIESGO] ([TB_CODIGO], [TB_ELEMENT], [TB_COD_SAT], [TB_INGLES], [TB_NUMERO], [TB_TEXTO], [TB_ACTIVO]) VALUES (N'5', N'Clase V', N'5', N'', CAST(0.00 AS Numeric(15, 2)), N'', N'S');
        END


        DELETE FROM R_RELACION WHERE EN_CODIGO = 59 AND RL_ORDEN = 3 AND RL_CAMPOS = 'PU_PATRON'; 
        DELETE FROM R_RELACION WHERE EN_CODIGO = 59 AND RL_ORDEN = 14 AND RL_CAMPOS = 'PU_CONTRAT'; 

        update R_RELACION set RL_ORDEN =0 where EN_CODIGO = 50 and RL_ORDEN = 4 and RL_ENTIDAD = 69 
        update R_RELACION set RL_ORDEN =4 where EN_CODIGO = 50 and RL_ORDEN = 1 and RL_ENTIDAD = 10 
        update R_RELACION set RL_ORDEN =1 where EN_CODIGO = 50 and RL_ORDEN = 0 and RL_ENTIDAD = 69 
        update R_RELACION set RL_ORDEN =0 where EN_CODIGO = 50 and RL_ORDEN = 3 and RL_ENTIDAD = 59 
        update R_RELACION set RL_ORDEN =3 where EN_CODIGO = 50 and RL_ORDEN = 2 and RL_ENTIDAD = 54 
        update R_RELACION set RL_ORDEN =2 where EN_CODIGO = 50 and RL_ORDEN = 0 and RL_ENTIDAD = 59 
        update R_RELACION set RL_ORDEN =0 where EN_CODIGO = 50 and RL_ORDEN = 6 and RL_ENTIDAD = 62 
        update R_RELACION set RL_ORDEN =6 where EN_CODIGO = 50 and RL_ORDEN = 3 and RL_ENTIDAD = 54 
        update R_RELACION set RL_ORDEN =3 where EN_CODIGO = 50 and RL_ORDEN = 0 and RL_ENTIDAD = 62 
        update R_RELACION set RL_ORDEN =0 where EN_CODIGO = 50 and RL_ORDEN = 6 and RL_ENTIDAD = 54 
        update R_RELACION set RL_ORDEN =6 where EN_CODIGO = 50 and RL_ORDEN = 5 and RL_ENTIDAD = 9  
        update R_RELACION set RL_ORDEN =5 where EN_CODIGO = 50 and RL_ORDEN = 0 and RL_ENTIDAD = 54 


        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP04' ) BEGIN
                   INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'OP04', 'Aplicaci�n de saldo a favor compensaci�n anual', 4, 4, 'S') 
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'OP999' ) BEGIN
                   INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'OP999', 'Pagos distintos a los listados', 4, 999, 'S'); 
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P047' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P047', 'Alimentaci�n', 1, 47, 'S')
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P048' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P048', 'Habitaci�n', 1, 48, 'S')
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P049' ) BEGIN
          INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_CLA, TB_SAT_NUM, TB_ACTIVO ) VALUES ( 'P049', 'Premios por asistencia', 1, 49, 'S')
        END

        IF EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO = 'P016' ) BEGIN
          update TIPO_SAT set TB_ACTIVO  = 'N' where TB_CODIGO = 'P016' AND TB_SAT_CLA = 1 
        END

     END

	 GO

     EXECUTE dbo.SP_COMPLEMENTO_SAT_1_2;

	 GO
	 IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COMPLEMENTO_SAT_1_2_B') AND type IN ( N'P', N'PC' ))
		 DROP PROCEDURE SP_COMPLEMENTO_SAT_1_2_B 
	 GO

     create procedure SP_COMPLEMENTO_SAT_1_2_B
     as 
     begin 
	   set nocount on 
        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D024' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D024', 'Ajuste en Gratificaci�n Anual (Aguinaldo) Exento', 24, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D025' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D025', 'Ajuste en Gratificaci�n Anual (Aguinaldo) Gravado', 25, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D026' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D026', 'Ajuste en Participaci�n de los Trabajadores en las Utilidades PTU Exento', 26, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D027' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D027', 'Ajuste en Participaci�n de los Trabajadores en las Utilidades PTU Gravado', 27, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D028' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D028', 'Ajuste en Reembolso de Gastos M�dicos Dentales y Hospitalarios Exento', 28, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D029' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D029', 'Ajuste en Fondo de ahorro Exento', 29, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D030' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D030', 'Ajuste en Caja de ahorro Exento', 30, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D031' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D031', 'Ajuste en Contribuciones a Cargo del Trabajador Pagadas por el Patr�n Exento', 31, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D032' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D032', 'Ajuste en Premios por puntualidad Gravado', 32, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D033' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D033', 'Ajuste en Prima de Seguro de vida Exento', 33, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D034' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D034', 'Ajuste en Seguro de Gastos M�dicos Mayores Exento', 34, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D035' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D035', 'Ajuste en Cuotas Sindicales Pagadas por el Patr�n Exento', 35, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D036' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D036', 'Ajuste en Subsidios por incapacidad Exento', 36, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D037' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D037', 'Ajuste en Becas para trabajadores y/o hijos Exento', 37, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D038' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D038', 'Ajuste en Horas extra Exento', 38, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D039' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D039', 'Ajuste en Horas extra Gravado', 39, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D040' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D040', 'Ajuste en Prima dominical Exento', 40, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D041' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D041', 'Ajuste en Prima dominical Gravado', 41, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D042' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D042', 'Ajuste en Prima vacacional Exento', 42, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D043' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D043', 'Ajuste en Prima vacacional Gravado', 43, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D044' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D044', 'Ajuste en Prima por antig�edad Exento', 44, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D045' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D045', 'Ajuste en Prima por antig�edad Gravado', 45, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D046' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D046', 'Ajuste en Pagos por separaci�n Exento', 46, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D047' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D047', 'Ajuste en Pagos por separaci�n Gravado', 47, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D048' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D048', 'Ajuste en Seguro de retiro Exento', 48, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D049' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D049', 'Ajuste en Indemnizaciones Exento', 49, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D050' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D050', 'Ajuste en Indemnizaciones Gravado', 50, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D051' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D051', 'Ajuste en Reembolso por funeral Exento', 51, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D052' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D052', 'Ajuste en Cuotas de seguridad social pagadas por el patr�n Exento', 52, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D053' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D053', 'Ajuste en Comisiones Gravado', 53, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D054' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D054', 'Ajuste en Vales de despensa Exento', 54, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D055' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D055', 'Ajuste en Vales de restaurante Exento', 55, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D056' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D056', 'Ajuste en Vales de gasolina Exento', 56, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D057' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D057', 'Ajuste en Vales de ropa Exento', 57, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D058' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D058', 'Ajuste en Ayuda para renta Exento', 58, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D059' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D059', 'Ajuste en Ayuda para art�culos escolares Exento', 59, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D060' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D060', 'Ajuste en Ayuda para anteojos Exento', 60, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D061' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D061', 'Ajuste en Ayuda para transporte Exento', 61, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D062' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D062', 'Ajuste en Ayuda para gastos de funeral Exento', 62, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D063' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D063', 'Ajuste en Otros ingresos por salarios Exento', 63, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D064' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D064', 'Ajuste en Otros ingresos por salarios Gravado', 64, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D065' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D065', 'Ajuste en Jubilaciones, pensiones o haberes de retiro Exento', 65, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D066' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D066', 'Ajuste en Jubilaciones, pensiones o haberes de retiro Gravado', 66, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D067' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D067', 'Ajuste en Pagos por separaci�n Acumulable', 67, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D068' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D068', 'Ajuste en Pagos por separaci�n No acumulable', 68, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D069' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D069', 'Ajuste en Jubilaciones, pensiones o haberes de retiro Acumulable', 69, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D070' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D070', 'Ajuste en Jubilaciones, pensiones o haberes de retiro No acumulable', 70, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D071' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D071', 'Ajuste en Subsidio para el empleo (efectivamente entregado al trabajador)', 71, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D072' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D072', 'Ajuste en Ingresos en acciones o t�tulos valor que representan bienes Exento', 72, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D073' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D073', 'Ajuste en Ingresos en acciones o t�tulos valor que representan bienes Gravado', 73, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D074' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D074', 'Ajuste en Alimentaci�n Exento', 74, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D075' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D075', 'Ajuste en Alimentaci�n Gravado', 75, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D076' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D076', 'Ajuste en Habitaci�n Exento', 76, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D077' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D077', 'Ajuste en Habitaci�n Gravado', 77, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D078' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D078', 'Ajuste en Premios por asistencia', 78, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D079' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D079', 'Ajuste en Pagos distintos a los listados y que no deben considerarse como ingreso por sueldos, salarios o ingresos asimilados.', 79, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D080' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D080', 'Ajuste en Vi�ticos no comprobados', 80, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D081' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'D081', 'Ajuste en Vi�ticos anticipados', 81, 2, 'S');
        END

        IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='P050' )  BEGIN
        INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
        VALUES ( 'P050', 'Vi�ticos no comprobados', 50, 1, 'S');
        END
     END

	 GO
     
     EXECUTE dbo.SP_COMPLEMENTO_SAT_1_2_B;
     
	 GO

	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_Reporte_Copiar') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_Reporte_Copiar;
	GO
     
     create procedure SP_Reporte_Copiar( @ReporteDescOrigen Descripcion , @ReporteDescDestino Descripcion, @iReporteDestino int output, @MaxPosic int output ) 
     as 
     begin 
        set nocount on 

         declare @iReporte int
         declare @iReporte12 int
         declare @iSigReporte int 

         select @iReporte = RE_CODIGO from REPORTE where RE_NOMBRE = @ReporteDescOrigen
         select @iReporte12 = RE_CODIGO from REPORTE where RE_NOMBRE = @ReporteDescDestino
         set @iReporteDestino = 0
         set @MaxPosic = 0 

             set @iReporte = coalesce( @iReporte, 0 ) 
             set @iReporte12 = coalesce( @iReporte12, 0 ) 

             if ( @iReporte > 0 ) and ( @iReporte12 = 0 ) 
             begin 
                 select @iSigReporte =MAX(RE_CODIGO)+1 from REPORTE 
                 insert into REPORTE (RE_CODIGO, RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,RE_SOLOT,RE_FECHA,RE_GENERAL,
                 RE_REPORTE,RE_CFECHA,RE_IFECHA,RE_HOJA,RE_ALTO,RE_ANCHO,RE_PRINTER,RE_COPIAS,RE_PFILE,RE_ARCHIVO,
                 RE_VERTICA,US_CODIGO,RE_FILTRO,RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,RE_MAR_DER,
                 RE_MAR_INF,RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,QU_CODIGO,RE_CLASIFI ) 
                 select @iSigReporte,@ReporteDescDestino,RE_TIPO,RE_TITULO,RE_ENTIDAD,RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,
                 RE_CFECHA,RE_IFECHA,RE_HOJA,RE_ALTO,RE_ANCHO,RE_PRINTER,RE_COPIAS,RE_PFILE,RE_ARCHIVO,RE_VERTICA,
                 US_CODIGO,RE_FILTRO,RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,RE_MAR_DER,RE_MAR_INF,
                 RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,QU_CODIGO,RE_CLASIFI
                 from REPORTE where RE_CODIGO = @iReporte 
                 insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,
                 CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,
                 CR_SUBRAYA,CR_STRIKE,CR_ALINEA)
                 select @iSigREporte,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,
                 CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,
                 CR_SUBRAYA,CR_STRIKE,CR_ALINEA 
                 FROM CAMPOREP where RE_CODIGO = @iReporte 
                 set @iReporteDestino = @iSigReporte
                 select @MaxPosic = MAX( CR_POSICIO ) from CAMPOREP where RE_CODIGO = @iSigReporte 
             end 
    end;

	GO

	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_CopiarReportesTimbrado') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_CopiarReportesTimbrado;
	GO
     
	create procedure SP_CopiarReportesTimbrado
		 as
		 begin
			set nocount on
		declare @ReporteDescOrigen Descripcion , @ReporteDescDestino Descripcion 
		declare @ReCodigo  int 
		declare @MaxPosic  int 
		declare @ReporteDescOrigenDetalle Descripcion , @ReporteDescDestinoDetalle Descripcion 
		declare @ReCodigoDetalle  int 
		declare @MaxPosicDetalle  int 

		set @ReporteDescOrigen = 'Timbrado De N�mina' 
		set @ReporteDescDestino = '1.2 Timbrado de n�mina' 

		set @ReporteDescOrigenDetalle = 'Timbrado detalle concepto' 
		set @ReporteDescDestinoDetalle = '1.2 Timbrado detalle concepto' 

		select @ReCodigo = RE_CODIGO from REPORTE where RE_NOMBRE = @ReporteDescDestino 
		set @ReCodigo = coalesce( @ReCodigo , 0 ) 
		set @MaxPosic = 0 

		if ( @ReCodigo = 0  ) 
		begin 
			exec SP_Reporte_Copiar @ReporteDescOrigen, @ReporteDescDestino, @ReCodigo output , @MaxPosic output 
			if ( @ReCodigo > 0 ) 
			begin 
				update CAMPOREP set CR_TITULO ='UbicacionNom11' where CR_TITULO = 'Ubicacion' and RE_CODIGO = @ReCodigo 
				if ( select count(*) from CAMPOREP where RE_CODIGO = @ReCodigo  and CR_TITULO in ( 
				'Baja','SalMes','TotOtros','Sindica','Entidad','RFCOrigen','RecuOrigen','RecuPropio' 
				,'Subsidio','SaldoFavor','SaldoYear','SaldoRema') ) = 0 
					begin

				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+1,0,-1,101,'Ubicacion',-1,'####0;-####0',5,0,2,0,'',0,'RPATRON.TB_CODPOST')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+2,0,-1,101,'Baja',-1,'',10,6,6,0,'',0,'COLABORA.CB_FEC_BAJ')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+3,0,-1,101,'SalMes',-1,'',15,6,6,0,'',0,'SALARIO * 30.4')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+4,0,-1,101,'TotOtros',-1,'',10,6,6,0,'',0,'(C(53)+c(72))*-1')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+5,0,-1,101,'Entidad',-1,'',10,6,4,0,'',0,'@SELECT B.TB_STPS FROM  RPATRON A, ENTIDAD B WHERE A.TB_ENTIDAD=B.TB_CODIGO AND A.TB_CODIGO=COLABORA.CB_PATRON')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+6,0,-1,101,'Sindica',-1,'#0;-#0',10,6,2,0,'',0,'0')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+7,0,-1,101,'RFCOrigen',-1,'',25,0,4,0,'',0,'')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+8,0,-1,101,'RecuOrigen',-1,'',10,6,6,0,'',0,'''IM''')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+9,0,-1,101,'RecuPropio',-1,'#0;-#0',10,6,2,0,'',0,'0')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+10,0,-1,101,'Subsidio',-1,'',10,6,6,0,'',0,'C(950)')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+11,0,-1,101,'SaldoFavor',-1,'',10,6,6,0,'',0,'C(955)')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+12,0,-1,101,'SaldoYear',-1,'',10,6,6,0,'',0,'IF( C(955)+C(956) > 0 ,  PE_YEAR-1, 0 )')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+13,0,-1,101,'SaldoRema',-1,'',10,6,6,0,'',0,'C(956)')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+14,0,-1,101,'AccionValor',-1,'',10,6,6,0,'',0,'0')
				   insert into CAMPOREP (RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
				   CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA)  values (@ReCodigo,0, @MaxPosic+15,0,-1,101,'AccionPrecio',-1,'',10,6,6,0,'',0,'0')
					end 
			end 
			exec SP_Reporte_Copiar  @ReporteDescOrigenDetalle, @ReporteDescDestinoDetalle, @ReCodigoDetalle output , @MaxPosicDetalle output 
		if ( @ReCodigoDetalle > 0 )
		begin
		if ( select count(*) from CAMPOREP where RE_CODIGO = @ReCodigoDetalle  and CR_TITULO in (
		'Monto') ) = 0
		begin
		INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
		CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @ReCodigoDetalle,'0','7','0','-1','101','Monto','','-1','#,0.00;-#,0.00','15',
		'0','1','0','','0','IF(RESULT(4)=4,IF(MO_PERCEPC+MO_DEDUCCI<0,-(MO_PERCEPC+MO_DEDUCCI),MO_PERCEPC+MO_DEDUCCI)
		IF(MO_PERCEPC+MO_DEDUCCI<0 AND    CONCEPTO.CO_SAT_CLP<>CONCEPTO.CO_SAT_CLN,  -(MO_PERCEPC+MO_DEDUCCI),MO_PERCEPC+MO_DEDUCCI ))',' ',' ',' ',' ','0')  
		end
		else
		begin
		update CAMPOREP set CR_FORMULA ='IF(RESULT(4)=4,IF(MO_PERCEPC+MO_DEDUCCI<0,-(MO_PERCEPC+MO_DEDUCCI),MO_PERCEPC+MO_DEDUCCI)
		IF(MO_PERCEPC+MO_DEDUCCI<0 AND    CONCEPTO.CO_SAT_CLP<>CONCEPTO.CO_SAT_CLN,  -(MO_PERCEPC+MO_DEDUCCI),MO_PERCEPC+MO_DEDUCCI ))' where CR_TITULO = 'Monto' and RE_CODIGO = 60
		end
		end
		end 
		end     

		GO

		EXECUTE dbo.SP_CopiarReportesTimbrado;

		GO

	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_REPORTE_TIMBRADO_NOMINA12') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_REPORTE_TIMBRADO_NOMINA12;
	GO

       create procedure SP_REPORTE_TIMBRADO_NOMINA12
     as 
     begin 
        set nocount on 

            declare @iReporte int
            set @iReporte = (select max(RE_CODIGO) from REPORTE) + 1;


            IF NOT EXISTS( SELECT RE_NOMBRE FROM REPORTE WHERE RE_NOMBRE = '1.2 Timbrado de N�mina' ) BEGIN

                    INSERT INTO DBO.REPORTE(RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,RE_IFECHA,RE_HOJA,RE_ALTO,
          RE_ANCHO,RE_PRINTER,RE_COPIAS,RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,RE_MAR_DER,RE_MAR_INF,
          RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,QU_CODIGO,RE_CLASIFI,RE_CANDADO) Values( @iReporte,'1.2 Timbrado de N�mina','0','1.2 Timbrado de N�mina',
          '50','N','2016-11-10 17:48:47','S','',',','0','0','2.000','2.000','IMPRESORA DEFAULT:','1','0',
          'D:\Grupo Tress\Plantillas\1.2 Timbrado de N�mina.txt','S','1','','0','0.000','66.000','0.000','0.000','0.000','0.000','0','Test',
          '0','        ','2','0')

                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','0','0','-1','50','Numero',
          '','0','#0;-#0','7','1','2','0','','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','1','0','-1','10','Nombre','',
          '-1','','50','6','6','0','','0','PRETTY_NAM',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','2','0','-1','101','TotPer','',
          '-1','','10','6','6','0','','0','NO_PERCEPC + C(35)',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','3','0','-1','101','TotDed','',
          '-1','','10','6','6','0','','0','NO_DEDUCCI - C(53) - C(72)',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','4','0','-1','101','Exento','',
          '-1','','10','6','6','0','','0','NOMINA.NO_X_ISPT + NOMINA.NO_X_CAL + NOMINA.NO_X_MENS + C(35)',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','5','0','-1','62','RegPat','',
          '0','','11','0','4','0','','0','RPATRON.TB_NUMREG',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','6','0','-1','10','NSS','','0',
          '','30','0','4','0','','0','COLABORA.CB_SEGSOC',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','7','0','-1','10','CURP','','0',
          '','30','0','4','0','','0','COLABORA.CB_CURP',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','8','0','-1','12','Regimen','',
          '-1','#0;-#0','15','6','2','0','','0','@2',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','9','0','-1','54','Dias','','-1',
          '','7','6','6','0','','0','MAX(NOMINA.NO_DIAS,1)',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','10','0','-1','42','Depto','','-1',
          '','30','6','4','0','','0','NIVEL2.TB_ELEMENT',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','11','0','-1','10','CLABE','',
          '-1','','30','6','4','0','','0','@12345678910',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','12','0','-1','223','Banco','',
          '-1','#0;-#0','6','6','2','0','','0','@12',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','13','0','-1','10','Ingreso','',
          '0','dd/mmm/yy','9','0','3','0','','0','COLABORA.CB_FEC_ING',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','14','0','-1','10','Antig','',
          '0','dd/mmm/yy','9','0','3','0','','0','COLABORA.CB_FEC_ANT',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','15','0','-1','59','Puesto',
          '','-1','','30','6','6','0','','0','PUESTO.PU_DESCRIP',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','16','0','-1','101','Contrato',
          '','-1','#0;-#0','10','6','2','0','','0','@1',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','17','0','-1','101','Jornada',
          '','-1','#0;-#0','10','6','2','0','','0','@1',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','18','0','-1','50','Salario',
          '','0','#,0.00;-#,0.00','15','0','1','0','','0','NOMINA.CB_SALARIO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','19','0','-1','50','SalInt',
          '','0','#,0.00;-#,0.00','15','0','1','0','','0','NOMINA.CB_SAL_INT',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','20','0','-1','391','Riesgo',
          '','-1','#0;-#0','40','6','2','0','','0','@1',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','21','0','-1','101','ISR','','-1',
          '','10','6','6','0','','0','C(51)',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','22','0','-1','101','Pago','',
          '-1','#0;-#0','10','6','2','0','','0','@2',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','23','0','-1','10','RFC','',
          '0','LLLL-999999-AAA;0','30','0','4','0','','0','COLABORA.CB_RFC',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','24','0','-1','101','SUBE','',
          '-1','','10','6','6','0','','0','C(53)*-1',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','25','0','-1','101','IMSS','',
          '-1','','10','6','6','0','','0','C(52)',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','26','0','-1','10','Email','',
          '0','','50','0','4','0','','0','COLABORA.CB_E_MAIL',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','27','0','-1','101','Ubicacion',
          '','-1','####0;-####0','5','0','2','0','','0','RPATRON.TB_CODPOST',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','28','0','-1','101','Baja','',
          '-1','','10','6','6','0','','0','COLABORA.CB_FEC_BAJ',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','29','0','-1','101','SalMes',
          '','-1','','15','6','6','0','','0','SALARIO * 30.4',' ',' ',' ',' ','0')
          INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
            CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','30','0','-1',
            '101','TotOtros','','-1','','10','6','6','0','0','0','(C(53)+c(72))*-1',' ',' ',' ',' ','0')
          INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
            CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','31','0','-1',
            '101','Entidad','','-1','#0;-#0','10','6','2','0','','0','@SELECT B.TB_STPS FROM  RPATRON A, ENTIDAD B WHERE A.TB_ENTIDAD=B.TB_CODIGO AND A.TB_CODIGO=COLABORA.CB_PATRON',
            ' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','32','0','-1','101','Sindica',
          '','-1','#0;-#0','10','6','2','0','','0','0',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','33','0','-1','101','RFCOrigen',
          '','-1','','25','0','4','0','','0','',' ',' ',' ',' ','0')
          INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
            CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','34','0','-1',
            '101','RecuOrigen','','-1','','10','6','6','0','','0','',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','35','0','-1','101','RecuPropio',
          '','-1','#0;-#0','10','6','2','0','','0','0',' ',' ',' ',' ','0')
          INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
            CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','36','0','-1',
            '101','Subsidio','','-1','','10','6','6','0','','0','C(950)',' ',' ',' ',' ','0')
          INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
            CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','37','0','-1',
            '101','SaldoFavor','','-1','','10','6','6','0','','0','C(955)',' ',' ',' ',' ','0')
          INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
            CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','38','0','-1',
            '101','SaldoYear','','-1','#0;-#0','10','6','2','0','','0','IF( C(955)+C(956) > 0 ,  PE_YEAR-1, 0 )',' ',' ',' ',' ','0')
          INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
            CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','39','0','-1',
            '101','SaldoRema','','-1','','10','6','6','0','','0','C(956)',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','40','0','-1','101','AccionValor',
          '','-1','','10','6','6','0','','0','0',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','41','0','-1','101','AccionPrecio',
          '','-1','','10','6','6','0','','0','0',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'1','0','0','-1','0','Grupo Nivel Empresa',
          '','0','','0','1','1','1','','0','',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','0','0','-1','50','A�o','','0',
          '','0','0','0','0','','0','NOMINA.PE_YEAR',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','1','0','-1','50','Tipo de Per�odo',
          '','0','','0','0','0','0','','0','NOMINA.PE_TIPO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','2','0','-1','50','N�mina','','0',
          '','0','0','0','0','','0','NOMINA.PE_NUMERO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','3','0','-1','50','N�mero','','0',
          '','0','0','0','0','','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','0','0','-1','50','A�o de Per�odo',
          '','1','','1','-1','2','0','4','0','NOMINA.PE_YEAR',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','1','0','-1','50','Tipo de Per�odo',
          '','2','','1','14','2','0','3','0','NOMINA.PE_TIPO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','2','0','-1','50','N�mero de N�mina',
          '','1','','1','54','2','0','2','0','NOMINA.PE_NUMERO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
          CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','3','0','-1','50','N�mero de Empleado',
          '','1','','0','10','2','0','1','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')

            END
    end; 

	GO


	EXECUTE dbo.SP_REPORTE_TIMBRADO_NOMINA12;

	GO


	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_REPORTE_TIMBRADO_DETALLE12') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_REPORTE_TIMBRADO_DETALLE12 
	GO


     
	   create procedure SP_REPORTE_TIMBRADO_DETALLE12
		 as 
		 begin 
			set nocount on 

			 declare @iReporte int
			 set @iReporte = (select max(RE_CODIGO) from REPORTE) + 1;


			  IF NOT EXISTS( SELECT RE_NOMBRE FROM REPORTE WHERE RE_NOMBRE = '1.2 Timbrado detalle concepto' ) BEGIN

							INSERT INTO DBO.REPORTE(RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,RE_IFECHA,RE_HOJA,RE_ALTO,RE_ANCHO,
			RE_PRINTER,RE_COPIAS,RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,RE_MAR_DER,RE_MAR_INF,RE_NIVEL,
			RE_FONTNAM,RE_FONTSIZ,QU_CODIGO,RE_CLASIFI,RE_CANDADO) Values( @iReporte,'1.2 Timbrado detalle concepto','0','1.2 Timbrado detalle concepto','40','N',
			'2016-11-10 17:48:55','','',',','0','0','0.000','66.000','IMPRESORA DEFAULT:','1','0','','N','1',
			'(MOVIMIEN.MO_DEDUCCI+ MOVIMIEN.MO_PERCEPC) <> 0','0','0.000','66.000','0.000','0.000','0.000','0.000','0','','0','        ','2','0')

			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','0','0','-1','40','Numero','','0','#0;-#0','7','0',
			'2','0','','0','MOVIMIEN.CB_CODIGO',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,
			CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','1','0','-1','101','ConceptoID','','-1','#0;-#0','4','0','2','0','',
			'0','MOVIMIEN.CO_NUMERO',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,
			CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','2','0','-1','101','Concepto','','-1','','25','0','4','0','','0',
			'CONCEPTO.CO_DESCRIP',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,
			CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','3','0','-1','101','Clase','','-1','#0;-#0','1','0','2','0','',
			'0','IF( MO_PERCEPC+MO_DEDUCCI < 0, CONCEPTO.CO_SAT_CLN,  CONCEPTO.CO_SAT_CLP)',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','4','0','-1','101','TPN','','-1','#0;-#0','10','6',
			'2','0','','0','@select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CONCEPTO.CO_SAT_TPN',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','5','0','-1','101','TPP','','-1','#0;-#0','10','6',
			'2','0','','0','@select TB_SAT_NUM from TIPO_SAT where TB_CODIGO = CONCEPTO.CO_SAT_TPP',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','6','0','-1','101','TipoSAT','','-1','#0;-#0','10','6',
			'2','0','','0','IF( MO_PERCEPC+MO_DEDUCCI < 0,  Result(5),  Result(6) )',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','7','0','-1','101','Monto','','-1','#,0.00;-#,0.00','15',
			'0','1','0','','0','IF(RESULT(4)=4,IF(MO_PERCEPC+MO_DEDUCCI<0,-(MO_PERCEPC+MO_DEDUCCI),MO_PERCEPC+MO_DEDUCCI),
			IF(MO_PERCEPC+MO_DEDUCCI<0 AND    CONCEPTO.CO_SAT_CLP<>CONCEPTO.CO_SAT_CLN,  -(MO_PERCEPC+MO_DEDUCCI),MO_PERCEPC+MO_DEDUCCI ))',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','8','0','-1','101','ExentoTipo','','-1','#0;-#0','10',
			'6','2','0','','0','CONCEPTO.CO_SAT_EXE',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','9','0','-1','101','ExentoID','','-1','','10','6',
			'6','0','','0','CONCEPTO.CO_SAT_CON',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,
			CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','10','0','-1','101','Exento','','-1','','15','6','6','0',
			'','0','IF(RESULT(4)=1,MOVIMIEN.MO_X_ISPT,0)',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'1','0','0','-1','0','Grupo Nivel Empresa','','0','','0','1',
			'1','1','','0','',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','0','0','-1','40','A�o de Per�odo','','0','','0','0','0',
			'0','','0','MOVIMIEN.PE_YEAR',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','1','0','-1','40','Tipo de Per�odo','','0','','0','0',
			'0','0','','0','MOVIMIEN.PE_TIPO',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','2','0','-1','40','N�mero de N�mina','','0','','0','0',
			'0','0','','0','MOVIMIEN.PE_NUMERO',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','3','0','-1','40','N�mero','','0','','0','0','0','0',
			'','0','MOVIMIEN.CB_CODIGO',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','0','0','-1','40','N�mero de N�mina','','1','','1','54',
			'2','0','2','0','MOVIMIEN.PE_NUMERO',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','1','0','-1','40','Tipo de Per�odo','','2','','1','14',
			'2','0','3','0','MOVIMIEN.PE_TIPO',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','2','0','-1','40','A�o de Per�odo','','1','','1','-1',
			'2','0','4','0','MOVIMIEN.PE_YEAR',' ',' ',' ',' ','0')
			INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,
			CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','3','0','-1','40','N�mero de Empleado','','1','','0','10',
			'2','0','1','0','MOVIMIEN.CB_CODIGO',' ',' ',' ',' ','0')

			  END
		end; 

     GO

	 EXECUTE dbo.SP_REPORTE_TIMBRADO_DETALLE12;

	 GO

	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_REPORTE_TIMBRADO_INCAPACIDADES12') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_REPORTE_TIMBRADO_INCAPACIDADES12 
	GO

 
     create procedure SP_REPORTE_TIMBRADO_INCAPACIDADES12
     as 
     begin 
        set nocount on 

         declare @iReporte int
         set @iReporte = (select max(RE_CODIGO) from REPORTE) + 1;


          IF NOT EXISTS( SELECT RE_NOMBRE FROM REPORTE WHERE RE_NOMBRE = '1.2 Timbrado incapacidades' ) BEGIN

                        INSERT INTO DBO.REPORTE(RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,RE_IFECHA,RE_HOJA,RE_ALTO,
        RE_ANCHO,RE_PRINTER,RE_COPIAS,RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,RE_MAR_DER,RE_MAR_INF,
        RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,QU_CODIGO,RE_CLASIFI,RE_CANDADO) Values( @iReporte,'1.2 Timbrado incapacidades','0','1.2 Timbrado incapacidades','5',
        'S','2016-11-10 17:49:01','','',',','0','0','0.000','66.000','IMPRESORA DEFAULT:','1','0','','N','1','','0','0.000',
        '66.000','0.000','0.000','0.000','0.000','0','','0','        ','2','0')

        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','0','0','-1','5',
        'Numero','','-1','#0;-#0','10','8','2','0','','0','AUSENCIA.CB_CODIGO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','1','0','-1',
        '101','Dias','','-1','#0;-#0','10','2','2','0','','0','@1',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','2','0','-1',
        '101','Descuento','','-1','#,0.00;-#,0.00','10','8','1','0','','0','CASE(AU_TIPO,''AIC'',C(29),''IET'',C(29),  ''IN1'',
        3,''IN2'',3,''IN3'',3,''INC'',C(27)+C(28),  ''ITR'',C(29),0)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'1','0','0','-1','0',
        'Grupo Nivel Empresa','','0','','0','1','1','1','','0','',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,
        CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'1','1','0','-1',
        '5','N�mero de Empleado','','0','','0','1','1','1','','0','AUSENCIA.CB_CODIGO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,
        CR_OPER,CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'1','2','0',
        '-1','101','T�tulo','','0','','0','1','1','1','','0','CASE(AU_TIPO,''AIC'',1,''IET'',1,''IN1'',3,''IN2'',3,
        ''IN3'',3,''INC'',2,''ITR'',1,0)',' ',' ',' ',' ','-1')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','0','0','-1',
        '5','N�mero','','0','','0','0','0','0','','0','AUSENCIA.CB_CODIGO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','1','0','-1',
        '5','Fecha','','0','','0','0','0','0','','0','AUSENCIA.AU_FECHA',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'3','1','0','0','5',
        'NumeroGrupo','','-1','#0;-#0','7','0','2','0','','0','INT( AUSENCIA.CB_CODIGO )',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'3','2','0','0','101','Tipo',
        '','-1','#0;-#0','10','0','2','0','','0','CASE(AU_TIPO,''AIC'',1,''IET'',1,''IN1'',3,''IN2'',3,''IN3'',3,''INC'',2,''ITR'',1,0)',
        ' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','0','0','-1','5',
        'N�mero de Empleado','','1','','0','10','2','0','1','0','AUSENCIA.CB_CODIGO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','1','0','-1','5',
        'Fecha de Asistencia Diaria','','3','','2','0','3','0','0','1','AUSENCIA.AU_FECHA',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','2','0','-1',
        '30','Tipo de Incidencia','2','2','','3','9','2','0','0','0','INCIDEN.TB_INCIDEN',' ',' ',' ',' ','0')
        END
    end; 

     GO
	 EXECUTE dbo.SP_REPORTE_TIMBRADO_INCAPACIDADES12;

	 GO

	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_REPORTE_TIMBRADO_SUBCONTRATOS12') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_REPORTE_TIMBRADO_SUBCONTRATOS12
	GO

    create procedure SP_REPORTE_TIMBRADO_SUBCONTRATOS12
     as 
     begin 
        set nocount on 

         declare @iReporte int
         set @iReporte = (select max(RE_CODIGO) from REPORTE) + 1;


          IF NOT EXISTS( SELECT RE_NOMBRE FROM REPORTE WHERE RE_NOMBRE = '1.2 Timbrado subcontratos' ) BEGIN

                        INSERT INTO DBO.REPORTE(RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,RE_IFECHA,RE_HOJA,
        RE_ALTO,RE_ANCHO,RE_PRINTER,RE_COPIAS,RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,
        RE_MAR_DER,RE_MAR_INF,RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,QU_CODIGO,RE_CLASIFI,RE_CANDADO) Values( @iReporte,'1.2 Timbrado subcontratos','0',
        'Timbrado Subcontratacion','50','N','2016-11-10 17:49:07','S','',',','0','0','2.000','2.000','IMPRESORA DEFAULT:','1',
        '0','','S','1','','0','0.000','66.000','0.000','0.000','0.000','0.000',
        '0','Test','0','        ','2','0')

                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','0','0','-1','50',
        'Numero','','0','#0;-#0','7','1','2','0','','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','1','0','-1','10',
        'Nombre','','-1','','50','6','6','0','','0','PRETTY_NAM',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','2','0','-1','101',
        'RFC','','-1','','10','6','6','0','','0','0',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','3','0','-1','101',
        'Tiempo','','-1','','10','6','6','0','','0','0',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'1','0','0','-1','0',
        'Grupo Nivel Empresa','','0','','0','1','1','1','','0','',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','0','0','-1','50',
        'A�o','','0','','0','0','0','0','','0','NOMINA.PE_YEAR',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','1','0','-1','50',
        'Tipo de Per�odo','','0','','0','0','0','0','','0','NOMINA.PE_TIPO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','2','0','-1','50',
        'N�mina','','0','','0','0','0','0','','0','NOMINA.PE_NUMERO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','3','0','-1','50',
        'N�mero','','0','','0','0','0','0','','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','0','0','-1','50',
        'A�o de Per�odo','','1','','1','-1','2','0','4','0','NOMINA.PE_YEAR',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','1','0','-1','50',
        'Tipo de Per�odo','','2','','1','14','2','0','3','0','NOMINA.PE_TIPO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','2','0','-1','50',
        'N�mero de N�mina','','1','','1','54','2','0','2','0','NOMINA.PE_NUMERO',' ',' ',' ',' ','0')
                        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,
        CR_TFIELD,CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','3','0','-1','50',
        'N�mero de Empleado','','1','','0','10','2','0','1','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
          END
    end; 

	GO

	EXECUTE dbo.SP_REPORTE_TIMBRADO_SUBCONTRATOS12;

	GO


	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_REPORTE_TIMBRADO_EXTRA12') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_REPORTE_TIMBRADO_EXTRA12
	GO

     create procedure SP_REPORTE_TIMBRADO_EXTRA12
     as 
     begin 
        set nocount on 

         declare @iReporte int
         set @iReporte = (select max(RE_CODIGO) from REPORTE) + 1;


          IF NOT EXISTS( SELECT RE_NOMBRE FROM REPORTE WHERE RE_NOMBRE = '1.2 Timbrado tiempo extra' ) BEGIN

            INSERT INTO DBO.REPORTE(RE_CODIGO,RE_NOMBRE,RE_TIPO,RE_TITULO,RE_ENTIDAD,RE_SOLOT,RE_FECHA,RE_GENERAL,RE_REPORTE,RE_CFECHA,RE_IFECHA,RE_HOJA,
        RE_ALTO,RE_ANCHO,RE_PRINTER,RE_COPIAS,RE_PFILE,RE_ARCHIVO,RE_VERTICA,US_CODIGO,RE_FILTRO,RE_COLNUM,RE_COLESPA,RE_RENESPA,RE_MAR_SUP,RE_MAR_IZQ,
        RE_MAR_DER,RE_MAR_INF,RE_NIVEL,RE_FONTNAM,RE_FONTSIZ,QU_CODIGO,RE_CLASIFI,RE_CANDADO) Values( @iReporte,'1.2 Timbrado tiempo extra','0',
        '1.2 Timbrado tiempo extra','50','N','2016-11-14 08:57:24','','',',','0','0','0.000','66.000','IMPRESORA DEFAULT:',
        '1','0','','S','1','( C(3) + C(4) + C(17) + C(18) ) > 0','0','0.000','66.000','0.000','0.000','0.000','0.000','0','',
        '0','        ','2','0')

        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','0','0','-1','50','Numero','',
        '0','#0;-#0','7','0','2','0','','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','1','0','-1','101','Tipo1','',
        '-1','#0;-#0','10','6','2','0','','0','@2',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','2','0','-1','101','Horas1','',
        '-1','#0;-#0','10','6','2','0','','0','ROUND(NOMINA.NO_DOBLES,0)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','3','0','-1','101','Dias1','',
        '-1','#0;-#0','10','6','2','0','','0','INT( MAX(C(951),1 ))',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','4','0','-1','101','Importe1','',
        '-1','','10','6','6','0','','0','C(3)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','5','0','-1','101','Conce1','',
        '-1','#0;-#0','10','6','2','0','','0','@3',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','6','0','-1','101','Tipo2','','-1',
        '#0;-#0','10','6','2','0','','0','@3',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','7','0','-1','101','Horas2','','-1',
        '#0;-#0','10','6','2','0','','0','ROUND(NOMINA.NO_TRIPLES,0)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','8','0','-1','101','Dias2','',
        '-1','#0;-#0','10','6','2','0','','0','INT( MAX(C(952),1 ))',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','9','0','-1','101','Importe2','',
        '-1','','10','6','6','0','','0','C(4)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','10','0','-1','101','Conce2','',
        '-1','#0;-#0','10','6','2','0','','0','@4',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','11','0','-1','101','Tipo3','',
        '-1','#0;-#0','10','6','2','0','','0','@2',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','12','0','-1','101','Horas3','',
        '-1','#0;-#0','10','6','2','0','','0','ROUND(NOMINA.NO_DES_TRA,0)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','13','0','-1','101','Dias3','',
        '-1','#0;-#0','10','6','2','0','','0','INT( MAX(C(953),1 ))',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','14','0','-1','101','Importe3','',
        '-1','','10','6','6','0','','0','C(17)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','15','0','-1','101','Conce3','',
        '-1','#0;-#0','10','6','2','0','','0','@17',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','16','0','-1','101','Tipo4','',
        '-1','#0;-#0','10','6','2','0','','0','@2',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','17','0','-1','101','Horas4','',
        '-1','#0;-#0','10','6','2','0','','0','ROUND(NOMINA.NO_FES_TRA,0)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','18','0','-1','101','Dias4','',
        '-1','#0;-#0','10','6','2','0','','0','INT( MAX(C(954),1 ))',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','19','0','-1','101','Importe4',
        '','-1','#0;-#0','10','6','2','0','','0','C(18)',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'0','20','0','-1','101','Conce4','',
        '-1','#0;-#0','10','6','2','0','','0','@18',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'1','0','0','-1','0','Grupo Nivel Empresa',
        '','0','','0','1','1','1','','0','',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','0','0','-1','50','A�o de Per�odo',
        '','0','','0','0','0','0','','0','NOMINA.PE_YEAR',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','1','0','-1','50','Tipo de Per�odo',
        '','0','','0','0','0','0','','0','NOMINA.PE_TIPO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','2','0','-1','50','N�mero de N�mina',
        '','0','','0','0','0','0','','0','NOMINA.PE_NUMERO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'2','3','0','-1','50','N�mero','','0','',
        '0','0','0','0','','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','0','0','-1','50','N�mero de N�mina',
        '','1','','1','54','2','0','2','0','NOMINA.PE_NUMERO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','1','0','-1','50','Tipo de Per�odo',
        '','2','','1','14','2','0','3','0','NOMINA.PE_TIPO',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','2','0','-1','50','A�o de Per�odo','',
        '1','','1','-1','2','0','4','0','NOMINA.PE_YEAR',' ',' ',' ',' ','0')
        INSERT INTO DBO.CAMPOREP(RE_CODIGO,CR_TIPO,CR_POSICIO,CR_CLASIFI,CR_SUBPOS,CR_TABLA,CR_TITULO,CR_REQUIER,CR_CALC,CR_MASCARA,CR_ANCHO,CR_OPER,CR_TFIELD,
        CR_SHOW,CR_DESCRIP,CR_COLOR,CR_FORMULA,CR_BOLD,CR_ITALIC,CR_SUBRAYA,CR_STRIKE,CR_ALINEA) Values( @iReporte,'5','3','0','-1','50','N�mero de Empleado',
        '','1','','0','10','2','0','1','0','NOMINA.CB_CODIGO',' ',' ',' ',' ','0')
          END
    end; 
     
	GO
	     
    EXECUTE dbo.SP_REPORTE_TIMBRADO_EXTRA12;

	GO


	IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_VHorasTimFix') AND type IN ( N'P', N'PC' ))
		DROP PROCEDURE SP_VHorasTimFix
	GO


          create procedure SP_VHorasTimFix
          as
          declare @VHorasTim nvarchar(2000)
          set @VHorasTim = 'ALTER VIEW VHorasTim
          as
                    select CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO,
                              SUM(AU_DOBLES) AU_DOBLES, SUM(AU_TRIPLES) AU_TRIPLES, SUM(AU_DES_TRA) AU_DES_TRA, SUM(AU_FES_TRA) AU_FES_TRA
                    from
                    (
                    (select CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO, AU_DOBLES, AU_TRIPLES,
                    case 
                           when AU_TIPODIA = 8 then 0
                           else  AU_DES_TRA
                    end AU_DES_TRA,
                    case 
                           when AU_TIPODIA = 8 then AU_DES_TRA
                           else 0
                    end AU_FES_TRA
                    from AUSENCIA )
                    union all
                    (select CB_CODIGO, AU_FECHA=FALTAS.FA_FEC_INI,PE_TIPO, PE_YEAR, PE_NUMERO,
                    sum(CASE WHEN  FA_MOTIVO IN (1,2) THEN FA_HORAS ELSE 0 end) AS AU_DOBLES,
                    sum(CASE WHEN  FA_MOTIVO =3 THEN FA_HORAS ELSE 0 end) AS AU_TRIPLES,
                    sum(CASE WHEN FA_MOTIVO =8 THEN FA_HORAS ELSE 0 end) AS AU_DES_TRA,
                    sum(CASE WHEN FA_MOTIVO =7 THEN FA_HORAS ELSE 0 end) AS AU_FES_TRA
                    from FALTAS where FA_DIA_HOR = ''H''
                    group by CB_CODIGO,  PE_TIPO, PE_YEAR, PE_NUMERO, FALTAS.FA_FEC_INI )
                    ) HorasTotales
                    group by CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO'

        if exists (select * from sysobjects where name='VHorasTim' and xtype='V')
              exec sp_sqlexec @VHorasTim		
		GO

		EXECUTE dbo.SP_VHorasTimFix;

		GO

		IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_VHorasTim') AND type IN ( N'P', N'PC' ))
			DROP PROCEDURE SP_VHorasTim
		GO

		create procedure SP_VHorasTim
		as
		declare @VHorasTim nvarchar(2000)
		set @VHorasTim = 'create view VHorasTim
		as
			select CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO,
					  SUM(AU_DOBLES) AU_DOBLES, SUM(AU_TRIPLES) AU_TRIPLES, SUM(AU_DES_TRA) AU_DES_TRA, SUM(AU_FES_TRA) AU_FES_TRA
			from
			(
			(select CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO, AU_DOBLES, AU_TRIPLES,
			case 
				   when AU_TIPODIA = 8 then 0
				   else  AU_DES_TRA
			end AU_DES_TRA,
			case 
				   when AU_TIPODIA = 8 then AU_DES_TRA
				   else 0
			end AU_FES_TRA
			from AUSENCIA )
			union all
			(select CB_CODIGO, AU_FECHA=FALTAS.FA_FEC_INI,PE_TIPO, PE_YEAR, PE_NUMERO,
			sum(CASE WHEN  FA_MOTIVO IN (1,2) THEN FA_HORAS ELSE 0 end) AS AU_DOBLES,
			sum(CASE WHEN  FA_MOTIVO =3 THEN FA_HORAS ELSE 0 end) AS AU_TRIPLES,
			sum(CASE WHEN FA_MOTIVO =8 THEN FA_HORAS ELSE 0 end) AS AU_DES_TRA,
			sum(CASE WHEN FA_MOTIVO =7 THEN FA_HORAS ELSE 0 end) AS AU_FES_TRA
			from FALTAS where FA_DIA_HOR = ''H''
			group by CB_CODIGO,  PE_TIPO, PE_YEAR, PE_NUMERO, FALTAS.FA_FEC_INI )
			) HorasTotales
			group by CB_CODIGO, AU_FECHA, PE_TIPO, PE_YEAR, PE_NUMERO'

			if not exists (select * from sysobjects where name='VHorasTim' and xtype='V')
			exec sp_sqlexec @VHorasTim		

			GO

			EXECUTE dbo.SP_VHorasTim;

			GO

			IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COMPLEMENTO_SAT_1_2_C') AND type IN ( N'P', N'PC' ))
				DROP PROCEDURE SP_COMPLEMENTO_SAT_1_2_C
			GO

			 create procedure SP_COMPLEMENTO_SAT_1_2_C
			 as 
			 begin 
			 set nocount on 

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D082' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D082', 'Ajuste en Fondo de ahorro Gravado', 082, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D083' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D083', 'Ajuste en Caja de ahorro Gravado', 083, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D084' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D084', 'Ajuste en Prima de Seguro de vida Gravado', 084, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D085' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D085', 'Ajuste en Seguro de Gastos M�dicos Mayores Gravado', 085, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D086' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D086', 'Ajuste en Subsidios por incapacidad Gravado', 086, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D087' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D087', 'Ajuste en Becas para trabajadores y/o hijos Gravado', 087, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D088' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D088', 'Ajuste en Seguro de retiro Gravado', 088, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D089' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D089', 'Ajuste en Vales de despensa Gravado', 089, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D090' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D090', 'Ajuste en Vales de restaurante Gravado', 090, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D091' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D091', 'Ajuste en Vales de gasolina Gravado', 091, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D092' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D092', 'Ajuste en Vales de ropa Gravado', 092, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D093' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D093', 'Ajuste en Ayuda para renta Gravado', 093, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D094' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D094', 'Ajuste en Ayuda para art�culos escolares Gravado', 094, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D095' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D095', 'Ajuste en Ayuda para anteojos Gravado', 095, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D096' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D096', 'Ajuste en Ayuda para transporte Gravado', 096, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D097' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D097', 'Ajuste en Ayuda para gastos de funeral Gravado', 097, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D098' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D098', 'Ajuste a ingresos asimilados a salarios gravados', 098, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D099' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D099', 'Ajuste a ingresos por sueldos y salarios gravados', 099, 2, 'S');
				END
			 END
     
			 GO

			 EXECUTE dbo.SP_COMPLEMENTO_SAT_1_2_C;

			 GO

			IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_COMPLEMENTO_SAT_1_2_D') AND type IN ( N'P', N'PC' ))
				DROP PROCEDURE SP_COMPLEMENTO_SAT_1_2_D
			GO

			 create procedure SP_COMPLEMENTO_SAT_1_2_D
			 as 
			 begin 
			 set nocount on 

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='D100' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'D100', 'Ajuste en Vi�ticos exentos', 100, 2, 'S');
				END

				IF NOT EXISTS( SELECT TB_CODIGO FROM TIPO_SAT WHERE TB_CODIGO ='P050' )  BEGIN
				INSERT INTO TIPO_SAT (TB_CODIGO, TB_ELEMENT, TB_SAT_NUM, TB_SAT_CLA, TB_ACTIVO) 
				VALUES ( 'P050', 'Vi�ticos', 50, 1, 'S');
				END
				ELSE
				BEGIN
				UPDATE TIPO_SAT SET TB_ELEMENT = 'Vi�ticos' WHERE  TB_CODIGO ='P050'
				END
			 END
     
			 GO 

			 EXECUTE dbo.SP_COMPLEMENTO_SAT_1_2_D;

			 GO

IF EXISTS ( SELECT  * FROM sys.objects WHERE object_id = OBJECT_ID(N'SPP_MigracionPresupuesto_DELETE') AND type IN ( N'P', N'PC' ))
	drop procedure SPP_MigracionPresupuesto_DELETE
go

CREATE PROCEDURE SPP_MigracionPresupuesto_DELETE( @lDejarConceptos Booleano )
AS
BEGIN
		/* El parametro @lDejarConceptos, NO borra los catalogos de CONCEPTOS y  PARAMETROS */

		SET NOCOUNT ON;
		Delete from BIT_MIG
		insert into BIT_MIG 
		values(	getdate(), 'Se trunca la Tabla de Bitacora', '' );
		DELETE FROM ACUMULA;
		DELETE FROM KAR_TOOL;
		DELETE FROM VACACION;
		DELETE FROM PCAR_ABO;
		DELETE FROM PRESTAMO;
		DELETE FROM AHORRO;
		DELETE FROM ACAR_ABO;
		DELETE FROM KARDEX;
		DELETE FROM COLABORA;		
		DELETE FROM KAR_FIJA2;
		DELETE FROM KAR_FIJA;
		DELETE FROM TAHORRO;
		DELETE FROM CAMPOREP
		DELETE FROM REPORTE
		DELETE FROM R_VALOR;
		DELETE FROM R_LISTAVAL;
		DELETE FROM R_MOD_ENT;
		DELETE FROM R_ORDEN;
		DELETE FROM R_DEFAULT;
		DELETE FROM R_FILTRO;
		DELETE FROM R_CLAS_ENT;
		DELETE FROM R_RELACION;
		DELETE FROM R_ATRIBUTO;
		DELETE FROM R_ENTIDAD;
		DELETE FROM R_MODULO;
		DELETE FROM R_CLASIFI;
		DELETE FROM PLAZA
		DELETE FROM PTOFIJAS
		DELETE FROM PTOTOOLS
		DELETE FROM MOT_TOOL;
		DELETE FROM TOOL;
		DELETE FROM TALLA;
		DELETE FROM TPERIODO;
		DELETE FROM TKARDEX;
		DELETE FROM TCAMBIO;
		DELETE FROM SAL_MIN;
		DELETE FROM RIESGO;
		DELETE FROM QUERYS;
		DELETE FROM PUESTO;
		DELETE FROM PRIESGO;
		DELETE FROM PRESTAXREG;
		DELETE FROM REGLAPREST;
		DELETE FROM TPRESTA;
		DELETE FROM PRESTACI;
		DELETE FROM SSOCIAL;
		DELETE FROM OTRASPER;
		if NOT (@lDejarConceptos = 'S')
			DELETE FROM NOMPARAM;
		DELETE FROM NIVEL9;
		DELETE FROM NIVEL8;
		DELETE FROM NIVEL7;
		DELETE FROM NIVEL6;
		DELETE FROM NIVEL5;
		DELETE FROM NIVEL4;
		DELETE FROM NIVEL3;
		DELETE FROM NIVEL2;
		DELETE FROM NIVEL1;
		DELETE FROM MOT_BAJA;
		DELETE FROM LEY_IMSS;
		DELETE FROM HORARIO;
		DELETE FROM FESTIVO;
		DELETE FROM ENTIDAD;
		DELETE FROM CONTRATO;
		DELETE FROM CLASIFI;
		DELETE FROM TURNO;
		if NOT (@lDejarConceptos = 'S')
			DELETE FROM CONCEPTO;
		DELETE FROM GLOBAL;
		DELETE FROM ART_80;
		DELETE FROM T_ART_80;
		DELETE FROM NUMERICA;
		DELETE FROM RPATRON;
		DELETE FROM RSOCIAL;
		DELETE FROM VALOR_UMA;

END
GO


IF EXISTS ( SELECT  * FROM  sys.objects WHERE  object_id = OBJECT_ID(N'SP_DISM_TASA_INFONAVIT'))
   drop function SP_DISM_TASA_INFONAVIT
go
CREATE FUNCTION SP_DISM_TASA_INFONAVIT( @Empleado NumeroEmpleado, @Fecha Fecha  ) Returns NUMERIC(15, 2)
AS
BEGIN
  declare @SalarioIntegrado Numeric( 15, 2);
  declare @SalarioMinimo Numeric( 15, 2 );
  declare @UMAMinimo Numeric( 15, 2 );
  declare @ComparaSalarioUMA Numeric( 15, 2 );
  declare @FechaSalarioMinimo Fecha;
  declare @FechaUmaMinimo Fecha;
  declare @TasaIndex Numeric( 15, 2 );
  declare @ZonaGeografica Char( 1 );
  declare @Resultado SmallInt;
  declare @FechaIngreso Fecha;
  declare @AplicaDisminucion Booleano;
  declare @TipoCredito SmallInt;
  declare @FechaInicio Fecha;
  declare @Status SmallInt;

  declare @TasaNueva Numeric( 15, 2 );
  declare @FechaTasa Fecha;
  
  SET @TasaNueva = 0;
  set @FechaTasa = @Fecha

  select @FechaInicio = MIN(KI_FECHA) from KARINF where CB_CODIGO = @Empleado

  if ( @FechaInicio < @Fecha )
  begin
	   set @FechaInicio = @Fecha;
  end
  
  select @FechaIngreso = CB_FEC_ING from COLABORA where CB_CODIGO = @Empleado;


  select TOP 1 @TasaIndex = CB_INFTASA, @AplicaDisminucion = CB_INFDISM, @TipoCredito = CB_INFTIPO  
  from  KARINF
  where ( CB_CODIGO = @Empleado ) AND ( KI_FECHA <= @FechaInicio  ) order by KI_FECHA desc

  if ( @TasaIndex is not NULL ) and ( @TipoCredito = 1 ) and ( @AplicaDisminucion = 'S' )
  begin   
		    /*Obtener el status del empleado ala fecha de inicio del credito a la fecha de inicio del bimestre*/
        select @Status = DBO.SP_STATUS_ACT( @Fecha, @Empleado )

        if ( @Status <> 1 ) and (@FechaIngreso > @Fecha) /*Para los ingresos a medio bimestres y reingresos*/
		    begin
             /*Se trae de nuevo la fecha de ingreso debido a que puede haber mas de dos cortes*/
             select @FechaIngreso = ( select TOP 1 CB_FEC_ING from KARDEX where CB_CODIGO = @Empleado and
									                    ( CB_FECHA >= @Fecha) and ( CB_TIPO = 'ALTA') order by CB_FECHA asc)
             if ( @FechaIngreso > @Fecha )
             begin
                  set @FechaTasa = @FechaIngreso;
             end
		    end

        if ( @Status = 1 ) or ( @FechaTasa = @FechaIngreso )
        begin
              select @ZonaGeografica = CB_ZONA_GE
              from   COLABORA
              where ( CB_CODIGO = @Empleado );


              select @FechaSalarioMinimo = MAX( SM_FEC_INI )
              from SAL_MIN
              where ( SM_FEC_INI <= @FechaTasa )


              IF ( @ZonaGeografica = 'A' )
                select @SALARIOMINIMO = SM_ZONA_A
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )

              ELSE IF ( @ZonaGeografica = 'B' )
                select @SalarioMinimo = SM_ZONA_B
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )

              ELSE IF ( @ZonaGeografica = 'C' )
                select @SalarioMinimo = SM_ZONA_C
                from SAL_MIN
                where ( SM_FEC_INI = @FechaSalarioMinimo )
              ELSE

              SET @SalarioMinimo = 0;
                    
              /* VERIFICACION DEL UMA */
                          
              select @FechaUmaMinimo = MAX( UM_FEC_INI )
              from VALOR_UMA
              where ( UM_FEC_INI <= @FechaTasa );

              select @ComparaSalarioUMA = UM_VALOR
              from VALOR_UMA
              where ( UM_FEC_INI = @FechaUmaMinimo )
              
              IF @ComparaSalarioUMA IS NULL
                SET @ComparaSalarioUMA = @SalarioMinimo;
                            
              if (@SalarioMinimo > @ComparaSalarioUMA)
                 Set @SalarioMinimo = @ComparaSalarioUMA;
              
              /* */

              if ( @SalarioMinimo > 0 )
              begin
                    select @SalarioIntegrado = CB_SAL_INT
                    FROM   DBO.SP_FECHA_KARDEX( @FechaTasa, @Empleado );

                    SET @SalarioIntegrado = @SalarioIntegrado / @SalarioMinimo;

                    SELECT @TasaNueva = Tasa
                    FROM   DBO.SP_A80_ESCALON( @TasaIndex, @SalarioIntegrado, @FechaTasa );
              end
        end
end

   if ( @TasaNueva = 0 ) or ( @TasaNueva is Null )
    set @TasaNueva = COALESCE( @TasaIndex, 0)

   Return @TasaNueva

END
GO


IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATCLP1')    
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATCLP1'
	 alter table CONCEPTO  drop column CO_SATCLP1       
end 

GO 
          
IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATTPP1')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATTPP1'
     alter table CONCEPTO  drop column CO_SATTPP1   
end                 

GO 

 IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATCLN1')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATCLN1'
     alter table CONCEPTO  drop column CO_SATCLN1   
end                 

GO 
 IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATTPN1')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATTPN1'
     alter table CONCEPTO  drop column CO_SATTPN1   
end

GO                  
 IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATEXE1')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATEXE1'
     alter table CONCEPTO  drop column CO_SATEXE1   
end

GO                  
 IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATCON1')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATCON1'
     alter table CONCEPTO  drop column CO_SATCON1   
end

GO                  
 IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATCLP2')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATCLP2'
    alter table CONCEPTO  drop column CO_SATCLP2          
end

GO 
                 
 IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATTPP2')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATTPP2'
    alter table CONCEPTO  drop column CO_SATTPP2   
end                 

GO 

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATCLN2')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATCLN2'
    alter table CONCEPTO  drop column CO_SATCLN2   
end                 

GO 

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATTPN2')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATTPN2'
    alter table CONCEPTO  drop column CO_SATTPN2   
end                 

GO 

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATEXE2')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATEXE2'
    alter table CONCEPTO  drop column CO_SATEXE2   
end                 

GO 

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('CONCEPTO') AND name='CO_SATCON2')  
begin 	 	 
	EXEC sp_unbindefault 'CONCEPTO.CO_SATCON2'
    alter table CONCEPTO  drop column CO_SATCON2   
end               

GO 
