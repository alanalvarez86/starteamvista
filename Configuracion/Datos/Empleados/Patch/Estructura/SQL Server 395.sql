/************************************************/
/************************************************/
/**********   Base De Datos DATOS      **********/
/************************************************/
/************************************************/

/************************************************/
/***** Tablas Modificadas ***********************/
/************************************************/

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 1 Agregado */
alter table COLABORA add CB_TMP_PASS CHAR(8)
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 2 Agregado */
update COLABORA set CB_TMP_PASS = CB_PASSWRD
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 3 Agregado */
exec sp_unbindefault 'COLABORA.CB_PASSWRD'
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 4 Agregado */
alter table COLABORA drop column CB_PASSWRD
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 5 Agregado */
exec sp_droptype Passwrd
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 6 Agregado */
exec sp_addtype Passwd, 'varchar(30)', 'NOT NULL'
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 7 Agregado */
exec sp_bindefault StringVacio, Passwd
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 8 Agregado */
alter table COLABORA add CB_PASSWRD Passwd
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 9 Agregado */
update COLABORA set CB_PASSWRD = CB_TMP_PASS
GO

/* Domain PASSWRD: Defecto 338 */
/* Patch 395 # Seq: 10 Agregado */
alter table COLABORA drop column CB_TMP_PASS
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 11 Agregado */
alter table PRESTACI add PT_TEMP_VA Dias
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 12 Agregado */
update PRESTACI set PT_TEMP_VA = PT_DIAS_VA
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 13 Agregado */
exec sp_unbindefault 'PRESTACI.PT_DIAS_VA'
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 14 Agregado */
alter table PRESTACI drop column PT_DIAS_VA
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 15 Agregado */
alter table PRESTACI add PT_DIAS_VA DiasFrac
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 16 Agregado */
update PRESTACI set PT_DIAS_VA = PT_TEMP_VA
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 17 Agregado */
exec sp_unbindefault 'PRESTACI.PT_TEMP_VA'
GO

/* Tabla PRESTACI: Agregar 2 decimales a los d�as de vacaciones en la tabla de Prestaciones ( Sugerencia 491 ) */
/* Patch 395 # Seq: 18 Agregado */
alter table PRESTACI drop column PT_TEMP_VA
GO

/* Tabla LIQ_MOV: Campo para Contribuci�n Patronal en rama Enfermedad y Maternidad - Especie ( Sugerencia 204 ) */
/* Patch 395 # Seq: 19 Agregado */
alter table LIQ_MOV add LM_EYMESPP Pesos
GO

/* Tabla LIQ_MOV: Campo para Contribuci�n Patronal en rama Enfermedad y Maternidad - Excedente ( Sugerencia 204 ) */
/* Patch 395 # Seq: 20 Agregado */
alter table LIQ_MOV add LM_EYMEXCP Pesos
GO

/* Tabla LIQ_MOV: Campo para Contribuci�n Patronal en rama Enfermedad y Maternidad - Dinero ( Sugerencia 204 ) */
/* Patch 395 # Seq: 21 Agregado */
alter table LIQ_MOV add LM_EYMDINP Pesos
GO

/* Tabla LIQ_MOV: Campo para Contribuci�n Patronal en rama Invalidez y Vida ( Sugerencia 204 ) */
/* Patch 395 # Seq: 22 Agregado */
alter table LIQ_MOV add LM_INVVIDP Pesos
GO

/* Tabla LIQ_MOV: Campo para Contribuci�n Patronal en rama Cesant�a y Vejez ( Sugerencia 204 ) */
/* Patch 395 # Seq: 23 Agregado */
alter table LIQ_MOV add LM_CESVEJP Pesos
GO

/* Tabla NOMINA: Almacenar Confidencialidad en tabla NOMINA ( Sugerencia 466 ) */
/* Patch 395 # Seq: 28 Agregado */
alter table NOMINA add CB_NIVEL0 Codigo
GO

/* Tabla NOMINA: Almacenar Banca Electr�nica en tabla NOMINA ( Sugerencia 695 ) */
/* Patch 395 # Seq: 29 Agregado */
alter table NOMINA add CB_BAN_ELE Descripcion
GO

/* Tabla NOMINA: Almacenar Horas Totales de Prima Dominical en tabla NOMINA ( Sugerencia 613 ) */
/* Patch 395 # Seq: 30 Agregado */
alter table NOMINA add NO_HORAPDT Horas
GO

/* Tabla NOMINA: Almacenar Dias de Subsidio de Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 31 Agregado */
alter table NOMINA add NO_DIAS_SI DiasFrac
GO

/* Tabla NOMINA: Usuario quien autoriz� la pren�mina ( Sugerencia ST 368 ) */
/* Patch 395 # Seq: 53 Agregado */
alter table NOMINA add NO_SUP_OK USUARIO
GO

/* Tabla NOMINA: Fecha en la que se autoriz� la pren�mina ( Sugerencia ST 368 ) */
/* Patch 395 # Seq: 54 Agregado */
alter table NOMINA add NO_FEC_OK Fecha
GO

/* Tabla NOMINA: Hora en la que se autoriz� la pren�mina ( Sugerencia ST 368 ) */
/* Patch 395 # Seq: 55 Agregado */
alter table NOMINA add NO_HOR_OK Hora
GO

/* Tabla INCAPACI: Almacenar Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 32 Agregado */
alter table INCAPACI add IN_DIASSUB DiasFrac
GO

/* Tabla INCAPACI: Almacenar Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 33 Agregado */
alter table INCAPACI add IN_NOMYEAR Anio
GO

/* Tabla INCAPACI: Almacenar Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 34 Agregado */
alter table INCAPACI add IN_NOMTIPO NominaTipo
GO

/* Tabla INCAPACI: Almacenar Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 35 Agregado */
alter table INCAPACI add IN_NOMNUME NominaNumero
GO

/* Tabla VACACION: Aumentar espacio para comentarios 1/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 59 Agregado */
alter table VACACION add VA_COM_TMP Titulo
GO

/* Tabla VACACION: Aumentar espacio para comentarios 2/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 60 Agregado */
update VACACION set VA_COM_TMP = VA_COMENTA
GO

/* Tabla VACACION: Aumentar espacio para comentarios 3/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 61 Agregado */
exec sp_unbindefault 'VACACION.VA_COMENTA'
GO

/* Tabla VACACION: Aumentar espacio para comentarios 4/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 62 Agregado */
alter table VACACION drop column VA_COMENTA
GO

/* Tabla VACACION: Aumentar espacio para comentarios 5/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 63 Agregado */
alter table VACACION add VA_COMENTA Titulo
GO

/* Tabla VACACION: Aumentar espacio para comentarios 6/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 64 Agregado */
update VACACION set VA_COMENTA = VA_COM_TMP
GO

/* Tabla VACACION: Aumentar espacio para comentarios 7/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 65 Agregado */
exec sp_unbindefault 'VACACION.VA_COM_TMP'
GO

/* Tabla VACACION: Aumentar espacio para comentarios 8/8 ( Sugerencia 480 - ST # 363 ) */
/* Patch 395 # Seq: 66 Agregado */
alter table VACACION drop column VA_COM_TMP
GO

/* Tabla CHECADAS: Almacenar motivo de autorizaci�n / modificaci�n de checadas ( Sugerencia ST # 350 ) */
/* Patch 395 # Seq: 67 Agregado */
alter table CHECADAS add CH_MOTIVO Codigo4
GO

/* Tabla SESION: Almacenar Status de la Sesion ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 69 Agregado */
alter table SESION add SE_STATUS Status
go

/**********************************/
/* Inicializar Columnas Agregadas */
/**********************************/

/* Tabla LIQ_MOV: Inicializar Campo para Contribuci�n Patronal en rama Enfermedad y Maternidad - Especie ( Sugerencia 204 ) */
/* Patch 395 # Seq: 36 Agregado */
update LIQ_MOV set LM_EYMESPP = 0 where ( LM_EYMESPP is NULL )
GO

/* Tabla LIQ_MOV: Inicializar Campo para Contribuci�n Patronal en rama Enfermedad y Maternidad - Excedente ( Sugerencia 204 ) */
/* Patch 395 # Seq: 37 Agregado */
update LIQ_MOV set LM_EYMEXCP = 0 where ( LM_EYMEXCP is NULL )
GO

/* Tabla LIQ_MOV: Inicializar Campo para Contribuci�n Patronal en rama Enfermedad y Maternidad - Dinero ( Sugerencia 204 ) */
/* Patch 395 # Seq: 38 Agregado */
update LIQ_MOV set LM_EYMDINP = 0 where ( LM_EYMDINP is NULL )
GO

/* Tabla LIQ_MOV: Inicializar Campo para Contribuci�n Patronal en rama Invalidez y Vida ( Sugerencia 204 ) */
/* Patch 395 # Seq: 39 Agregado */
update LIQ_MOV set LM_INVVIDP = 0 where ( LM_INVVIDP is NULL )
GO

/* Tabla LIQ_MOV: Inicializar Campo para Contribuci�n Patronal en rama Cesant�a y Vejez ( Sugerencia 204 ) */
/* Patch 395 # Seq: 40 Agregado */
update LIQ_MOV set LM_CESVEJP = 0 where ( LM_CESVEJP is NULL )
GO

/* Tabla NOMINA: Inicializar Confidencialidad en tabla NOMINA ( Sugerencia 466 ) */
/* Patch 395 # Seq: 45 Agregado */
update NOMINA
set CB_NIVEL0 = COLABORA.CB_NIVEL0 from NOMINA, COLABORA where
( NOMINA.CB_CODIGO = COLABORA.CB_CODIGO ) and
( ( NOMINA.CB_NIVEL0 is NULL ) or ( NOMINA.CB_NIVEL0 = '' ) )
GO

/* Tabla NOMINA: Inicializar Banca Electr�nica en tabla NOMINA ( Sugerencia 695 ) */
/* Patch 395 # Seq: 46 Agregado */
update NOMINA set CB_BAN_ELE = '' where ( CB_BAN_ELE is NULL )
GO

/* Tabla NOMINA: Inicializar Horas Totales de Prima Dominical en tabla NOMINA ( Sugerencia 613 ) */
/* Patch 395 # Seq: 47 Agregado */
update NOMINA set NO_HORAPDT = 0 where ( NO_HORAPDT is NULL )
GO

/* Tabla NOMINA: Inicializar Dias de Subsidio de Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 48 Agregado */
update NOMINA set NO_DIAS_SI = 0 where ( NO_DIAS_SI is NULL )
GO

/* Tabla NOMINA: Inicializar Usuario quien autoriz� la pren�mina ( Sugerencia ST 368 ) */
/* Patch 395 # Seq: 56 Agregado */
update NOMINA set NO_SUP_OK = 0 where ( NO_SUP_OK is NULL )
GO

/* Tabla NOMINA: Inicializar Fecha en la que se autoriz� la pren�mina ( Sugerencia ST 368 ) */
/* Patch 395 # Seq: 57 Agregado */
update NOMINA set NO_FEC_OK = '12/30/1899' where ( NO_FEC_OK is NULL )
GO

/* Tabla NOMINA: Inicializar Hora en la que se autoriz� la pren�mina ( Sugerencia ST 368 ) */
/* Patch 395 # Seq: 58 Agregado */
update NOMINA set NO_HOR_OK = '' where ( NO_HOR_OK is NULL )
GO

/* Tabla INCAPACI: Inicializar Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 49 Agregado */
update INCAPACI set IN_DIASSUB = 0 where ( IN_DIASSUB is NULL )
GO

/* Tabla INCAPACI: Inicializar N�mina de Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 50 Agregado */
update INCAPACI set IN_NOMYEAR = 0 where ( IN_NOMYEAR is NULL )
GO

/* Tabla INCAPACI: Inicializar N�mina de Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 51 Agregado */
update INCAPACI set IN_NOMTIPO = 0 where ( IN_NOMTIPO is NULL )
GO

/* Tabla INCAPACI: Inicializar N�mina de Dias de Subsidio en Incapacidades ( Sugerencia 660 ) */
/* Patch 395 # Seq: 52 Agregado */
update INCAPACI set IN_NOMNUME = 0 where ( IN_NOMNUME is NULL )
GO

/* Tabla CHECADAS: Inicializar motivo de autorizaci�n / modificaci�n de checadas ( Sugerencia ST # 350 ) */
/* Patch 395 # Seq: 68 Agregado */
update CHECADAS set CH_MOTIVO = '' where ( CH_MOTIVO is NULL )
GO

/* Tabla SESION: Inicializar Status de la Sesion ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 70 Agregado */
update SESION set SE_STATUS = 0 where ( SE_STATUS is NULL )
GO

/************************************************/
/****** Tablas Nuevas ***************************/
/************************************************/

/* Tabla CURSOPRE: Prerequisitos de Cursos ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 71 Agregado */
CREATE TABLE CURSOPRE (
       CU_CODIGO            Codigo,
       CP_CURSO             Codigo,
       CP_COMENTA           Formula,
       CP_OPCIONA           Booleano
)
GO

/* Tabla CURSOPRE: Llave Primaria ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 72 Agregado */
ALTER TABLE CURSOPRE
       ADD CONSTRAINT PK_CURSOPRE PRIMARY KEY (CU_CODIGO, CP_CURSO)
GO

/* Tabla AULA: Cat�logo de Aulas ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 73 Agregado */
CREATE TABLE AULA (
       AL_CODIGO            Codigo,
       AL_NOMBRE            Descripcion,
       AL_INGLES            Descripcion,
       AL_NUMERO            Pesos,
       AL_TEXTO             Descripcion,
       AL_CUPO              Empleados,
       AL_DESCRIP           Formula,
       AL_ACTIVA            Booleano
)
GO

/* Tabla AULA: Llave Primaria ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 74 Agregado */
ALTER TABLE AULA
       ADD CONSTRAINT PK_AULA PRIMARY KEY (AL_CODIGO)
GO

/* Tabla RESERVA: Reservaciones de Aulas para Sesiones ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 75 Agregado */
CREATE TABLE RESERVA (
       RV_FOLIO             FolioGrande,
       AL_CODIGO            Codigo,
       SE_FOLIO             FolioGrande,
       MA_CODIGO            Codigo,
       RV_FEC_INI           Fecha,
       RV_HOR_INI           Hora,
       RV_FEC_FIN           Fecha,
       RV_HOR_FIN           Hora,
       RV_DURACIO           Horas,
       RV_RESUMEN           Observaciones,
       RV_DETALLE           Memo,
       US_CODIGO            Usuario,
       RV_FEC_RES           Fecha,
       RV_HOR_RES           Hora,
       RV_TIPO              Status,
       RV_LISTA             Booleano,
       RV_ORDEN             Referencia
)
GO

/* Tabla RESERVA: Llave Primaria ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 76 Agregado */
ALTER TABLE RESERVA
       ADD CONSTRAINT PK_RESERVA PRIMARY KEY (RV_FOLIO)
GO

/* Tabla INSCRITO: Listas de Inscripci�n a Sesiones  ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 77 Agregado */
CREATE TABLE INSCRITO (
       SE_FOLIO             FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       IC_STATUS            Status,
       IC_FEC_BAJ           Fecha,
       IC_COMENTA           Observaciones,
       IC_EVA_1             Numerico,
       IC_EVA_2             Numerico,
       IC_EVA_3             Numerico,
       IC_EVA_FIN           Numerico,
       US_CODIGO            Usuario,
       IC_FEC_INS           Fecha,
       IC_HOR_INS           Hora
)
GO

/* Tabla INSCRITO: Llave Primaria ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 78 Agregado */
ALTER TABLE INSCRITO
       ADD CONSTRAINT PK_INSCRITO PRIMARY KEY (SE_FOLIO, CB_CODIGO)
GO

/* Tabla CUR_ASIS: Lista de Asistencia a Sesiones ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 79 Agregado */
CREATE TABLE CUR_ASIS (
       RV_FOLIO             FolioGrande,
       SE_FOLIO             FolioGrande,
       CB_CODIGO            NumeroEmpleado,
       CS_ASISTIO           Booleano,
       CS_EVA_1             Numerico,
       CS_EVA_2             Numerico,
       CS_EVA_3             Numerico,
       CS_EVA_FIN           Numerico,
       CS_COMENTA           Observaciones
)
GO

/* Tabla CUR_ASIS: Llave Primaria ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 80 Agregado */
ALTER TABLE CUR_ASIS
       ADD CONSTRAINT PK_CUR_ASIS PRIMARY KEY (RV_FOLIO, SE_FOLIO, CB_CODIGO)
GO

/*****************************************/
/**************** Views ******************/
/*****************************************/

/* View CUR_PROG: Programaci�n de Cursos 1/2 ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 81 Agregado */
drop view CUR_PROG;

/* View CUR_PROG: Programaci�n de Cursos 2/2 ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 82 Modificado */
create view CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CU_CODIGO,
  KC_FEC_PRO,
  KC_EVALUA,
  KC_FEC_TOM,
  KC_HORAS,
  CU_HORAS,
  EN_OPCIONA,
  EN_LISTA,
  MA_CODIGO,
  KC_PROXIMO,
  KC_REVISIO,
  CU_REVISIO,
  CP_INDIVID ) as
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  DATEADD( DAY, E.EN_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CURSO.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CURSO.MA_CODIGO,
  ( select MIN( SESION.SE_FEC_INI ) from SESION where ( SESION.CU_CODIGO = E.CU_CODIGO ) and ( SESION.SE_FEC_INI >= cast( floor( cast( GetDate() as float ) ) as Datetime ))) KC_PROXIMO,
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_INDIVID
from COLABORA C
join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' )
union
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  DATEADD( DAY, EP.EP_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.MA_CODIGO,
  ( select MIN( SESION.SE_FEC_INI ) from SESION where ( SESION.CU_CODIGO = EP.CU_CODIGO ) and ( SESION.SE_FEC_INI >= cast( floor( cast( GetDate() as float ) ) as Datetime ))) KC_PROXIMO,
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_INDIVID
from COLABORA C
join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = EP.CU_CODIGO )
left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' )
GO

/*****************************************/
/**** Foreign Keys ***********************/
/*****************************************/

/* Tabla CUR_ASIS: Llave For�nea hacia RESERVA ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 83 Agregado */
ALTER TABLE CUR_ASIS
       ADD CONSTRAINT FK_CUR_ASIS_RESERVA
       FOREIGN KEY (RV_FOLIO)
       REFERENCES RESERVA
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Tabla CUR_ASIS: Llave For�nea hacia INSCRITO ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 84 Agregado */
ALTER TABLE CUR_ASIS
       ADD CONSTRAINT FK_CUR_ASIS_INSCRITO
       FOREIGN KEY (SE_FOLIO, CB_CODIGO)
       REFERENCES INSCRITO
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Tabla INSCRITO: Llave For�nea hacia COLABORA ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 85 Agregado */
ALTER TABLE INSCRITO
       ADD CONSTRAINT FK_INSCRITO_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Tabla INSCRITO: Llave For�nea hacia SESION ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 86 Agregado */
ALTER TABLE INSCRITO
       ADD CONSTRAINT FK_INSCRITO_SESION
       FOREIGN KEY (SE_FOLIO)
       REFERENCES SESION
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Tabla CURSOPRE: Llave For�nea hacia CURSO ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 87 Agregado */
ALTER TABLE CURSOPRE
       ADD CONSTRAINT FK_CURSOPRE_CURSO
       FOREIGN KEY (CU_CODIGO)
       REFERENCES CURSO
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/* Trigger Actualizaci�n TU_AULA */
/* Patch 395 # Seq: 82 Agregado */
CREATE TRIGGER TU_AULA ON AULA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON
  IF UPDATE( AL_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = AL_CODIGO from   Inserted;
        select @OldCodigo = AL_CODIGO from   Deleted;

	UPDATE RESERVA	SET AL_CODIGO = @NewCodigo WHERE ( AL_CODIGO = @OldCodigo );
  END
END
GO

/* Trigger Actualizaci�n TU_CURSO */
/* Patch 395 # Seq: 83 Agregado */
CREATE TRIGGER TU_CURSO ON CURSO AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON
     IF UPDATE( CU_CODIGO )
     BEGIN
          declare @NewCodigo Codigo, @OldCodigo Codigo;

	  select @NewCodigo = CU_CODIGO from   Inserted;
          select @OldCodigo = CU_CODIGO from   Deleted;

	  UPDATE CURSOPRE SET CP_CURSO = @NewCodigo WHERE ( CP_CURSO = @OldCodigo );

     END
END
GO

/* Patch 395 # Seq: 28 Modificado */
ALTER TRIGGER TU_MAESTRO ON MAESTRO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( MA_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = MA_CODIGO from   Inserted;
        select @OldCodigo = MA_CODIGO from   Deleted;

	UPDATE CURSO    SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
	UPDATE KARCURSO SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
	UPDATE SESION 	SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
        UPDATE RESERVA  SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
  END
END
GO

/* Patch 395 # Seq: 66 Modificado */
ALTER TRIGGER TU_SESION ON SESION AFTER UPDATE AS
BEGIN
     SET NOCOUNT ON;
     IF ( UPDATE( SE_FOLIO ) OR
        UPDATE( CU_CODIGO ) OR
        UPDATE( MA_CODIGO ) OR
        UPDATE( SE_REVISIO ) OR
        UPDATE( SE_FEC_INI ) OR
        UPDATE( SE_FEC_FIN ) )
     BEGIN
          declare @OldFolio FolioGrande;
          declare @NewFolio FolioGrande;
          declare @NewCU_CODIGO Codigo;
          declare @NewMA_CODIGO Codigo;
          declare @NewSE_REVISIO NombreCampo;
          declare @NewSE_FEC_INI Fecha;
          declare @NewSE_FEC_FIN Fecha;

          select @OldFolio = SE_FOLIO from Deleted;
          select @NewFolio = SE_FOLIO,
                 @NewCU_CODIGO = CU_CODIGO,
                 @NewMA_CODIGO = MA_CODIGO,
                 @NewSE_REVISIO = SE_REVISIO,
                 @NewSE_FEC_INI = SE_FEC_INI,
                 @NewSE_FEC_FIN = SE_FEC_FIN
          from Inserted;

          update KARCURSO set
                 KARCURSO.SE_FOLIO = @NewFolio,
                 KARCURSO.CU_CODIGO = @NewCU_CODIGO,
                 KARCURSO.MA_CODIGO = @NewMA_CODIGO,
                 KARCURSO.KC_REVISIO = @NewSE_REVISIO,
                 KARCURSO.KC_FEC_TOM = @NewSE_FEC_INI,
                 KARCURSO.KC_FEC_FIN = @NewSE_FEC_FIN
          where ( KARCURSO.SE_FOLIO = @OldFolio );

	  IF UPDATE( SE_FOLIO )
	  BEGIN
	       UPDATE RESERVA SET SE_FOLIO = @NewFolio WHERE SE_FOLIO = @OldFolio;
	  END

     END
END
GO

/**************************************************/
/******** STORED PROCEDURES ***********************/
/**************************************************/

/*****************************************************/
/** D:\3Win_13\Datos\SQLServer\3DatosProcedures.sql **/
/*****************************************************/

/* SP_CLAS_NOMINA: Agrega los campos NOMINA.CB_NIVEL0 ( Sug. 466 ) y NOMINA.CB_BAN_ELE ( Sug. 695 ) */
/* Patch 395 # Seq: 25 Modificado */
ALTER PROCEDURE SP_CLAS_NOMINA
    		@ANIO SMALLINT,
    		@TIPO SMALLINT,
    		@NUMERO SMALLINT,
    		@EMPLEADO INTEGER,
		@ROTATIVO CHAR(6) OUTPUT
AS
        SET NOCOUNT ON
	declare @FechaIni DATETIME;
 	declare @FechaFin DATETIME;
 	declare @ZONA_GE  CHAR(1);
 	declare @PUESTO   CHAR(6);
 	declare @CLASIFI  CHAR(6);
 	declare @TURNO    CHAR(6);
 	declare @PATRON   CHAR(1);
 	declare @NIVEL1   CHAR(6);
 	declare @NIVEL2   CHAR(6);
 	declare @NIVEL3   CHAR(6);
 	declare @NIVEL4   CHAR(6);
 	declare @NIVEL5   CHAR(6);
 	declare @NIVEL6   CHAR(6);
 	declare @NIVEL7   CHAR(6);
 	declare @NIVEL8   CHAR(6);
 	declare @NIVEL9   CHAR(6);
 	declare @SALARIO  NUMERIC(15,2);
 	declare @SAL_INT  NUMERIC(15,2);
 	declare @PROMEDIA CHAR(1);
 	declare @JORNADA  NUMERIC(15,2);
 	declare @D_TURNO  SMALLINT;
 	declare @ES_ROTA  CHAR(1);
 	declare @FEC_KAR  DATETIME;
 	declare @FEC_SAL  DATETIME;
        declare @BAN_ELE  VARCHAR(30);
        declare @NIVEL0   CHAR(6);

  	select @FechaIni = PE_FEC_INI,
		@FechaFin = PE_FEC_FIN
  	from   PERIODO
  	where  PE_YEAR = @Anio 
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero

  	select @ZONA_GE = CB_ZONA_GE,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO, 
		@PATRON = CB_PATRON,
         	@NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2, 
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4, 
		@NIVEL5 = CB_NIVEL5,
         	@NIVEL6 = CB_NIVEL6, 
		@NIVEL7 = CB_NIVEL7, 
		@NIVEL8 = CB_NIVEL8, 
		@NIVEL9 = CB_NIVEL9,
         	@SALARIO = CB_SALARIO, 
		@SAL_INT = CB_SAL_INT, 
		@FEC_KAR = CB_FEC_KAR, 
		@FEC_SAL = CB_FEC_SAL,
                @BAN_ELE = CB_BAN_ELE,
                @NIVEL0 = CB_NIVEL0
  	from   COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @FechaFin < @FEC_KAR ) 
  	begin
    		select 
			@ZONA_GE = CB_ZONA_GE,
			@PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI, 
			@TURNO = CB_TURNO, 
			@PATRON = CB_PATRON,
         		@NIVEL1 = CB_NIVEL1, 
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
         		@NIVEL6 = CB_NIVEL6, 
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
         		@SALARIO = CB_SALARIO, 
			@SAL_INT = CB_SAL_INT
                 	from   SP_FECHA_KARDEX( @FechaFin, @Empleado )
  	end

  	select @PROMEDIA = GL_FORMULA
  	from   GLOBAL
  	where  GL_CODIGO = 42

  	if (( @PROMEDIA = 'S' ) and ( @FEC_SAL > @FechaIni ))
        EXECUTE SP_RANGO_SALARIO @FechaIni, @FechaFin, @Empleado,
                                 @SALARIO OUTPUT, @SAL_INT OUTPUT

	EXECUTE SP_DIAS_JORNADA @Turno, @FechaIni, @FechaFin,
    				@D_Turno OUTPUT, @Jornada OUTPUT, @Es_Rota OUTPUT

  	if ( @ES_ROTA = 'S' )
    		SET @Rotativo = @Turno
  	else
    		SET @Rotativo = '';

  	update NOMINA
  	set   CB_ZONA_GE = @ZONA_GE,
        	CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_PATRON  = @PATRON,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
        	CB_SALARIO = @SALARIO,
        	CB_SAL_INT = @SAL_INT,
        	NO_JORNADA = @JORNADA,
        	NO_D_TURNO = @D_TURNO,
                CB_BAN_ELE = @BAN_ELE,
                CB_NIVEL0  = @NIVEL0
  	where PE_YEAR = @Anio
	and PE_TIPO = @Tipo
	and PE_NUMERO = @Numero
	and CB_CODIGO = @Empleado;
GO

/* SP_IMSS_YEAR: Maneja 2 decimales para PRESTACI.PT_DIAS_VA ( Sug. 491 ) */
/* Patch 395 # Seq: 6 Modificado */
ALTER FUNCTION SP_IMSS_YEAR(
@sTABLA  CHAR(1),
@iYEAR   Integer,
@Resultado integer )
RETURNS NUMERIC(15,5)
AS
BEGIN
     declare @DIAS_VA	NUMERIC(15,2)
     declare @PRIMAVA	NUMERIC(15,5)
     declare @DIAS_AG	NUMERIC(15,2)
     declare @FACTOR	NUMERIC(15,5)
     declare @PRIMADO	NUMERIC(15,5)
     declare @PAGO_7	CHAR(1)
     declare @PRIMA_7	CHAR(1)
     declare @DIAS_AD	NUMERIC(15,2)
     declare @RES	NUMERIC(15,5)

	if ( @iYear > 0 )
		select @iYear = ( select MIN( PT_YEAR )
				from PRESTACI
				where TB_CODIGO = @sTabla and PT_YEAR >= @iYear )

	if ( @iYear <= 0 ) or ( @iYear is NULL )
		Select @RES = 0
	else
	begin
     	select @DIAS_VA = PT_DIAS_VA, @PRIMAVA = PT_PRIMAVA, @DIAS_AG=PT_DIAS_AG,
				@FACTOR = PT_FACTOR, @PRIMADO=PT_PRIMADO,
				@PAGO_7 = PT_PAGO_7, @PRIMA_7 = PT_PRIMA_7, @DIAS_AD = PT_DIAS_AD
		from PRESTACI
		where ( TB_CODIGO = @sTabla ) AND ( PT_YEAR = @iYear  )

	if @Resultado = 1 Select @RES = @DIAS_VA
    	else if @Resultado = 2 Set @RES = @PRIMAVA
    	else if @Resultado = 3 Set @RES = @DIAS_AG
	else if @Resultado = 4 Set @RES = @FACTOR
	else if @Resultado = 5 Set @RES = @PRIMADO
	else if @Resultado = 6
	begin
		if @PAGO_7 = 'N' Set @RES = 0
		else Set @RES = 1
	end
	else if @Resultado = 7
	begin
		if @PRIMA_7 = 'N' Set @RES = 0
		else Set @RES = 1
	end
	else
		Set @RES = @DIAS_AD
 	end
	Return @RES
END
GO

/* SP_V_FIJAS: Maneja 2 decimales para PRESTACI.PT_DIAS_VA ( Sug. 491 ) */
/* Patch 395 # Seq: 64 Modificado */
ALTER FUNCTION SP_V_FIJAS(
@TABLA CHAR(1),
@FECHAINI DATETIME,
@FECHAFIN DATETIME)
RETURNS NUMERIC(15,2)
AS
begin
	DECLARE @DIASANTIG NUMERIC(15,2);
	DECLARE @RESULTADO NUMERIC(15,2);
	DECLARE @ANTIGUEDAD SMALLINT;
	DECLARE @DIASIDEALES NUMERIC(15,2);
	DECLARE @DIFERENCIA SMALLINT;
	DECLARE @AntiguedadTemp SMALLINT;

	Select @DiasAntig = DATEDIFF(day, @FechaIni , @FechaFin) + 1;
  	if ( @DiasAntig < 0 ) Select @DiasAntig = 0;
  	Select @Antiguedad = ROUND( @DiasAntig / 365.25 , 0 );
  	if ( @Antiguedad > ( @DiasAntig / 365.25 ))
		Select @Antiguedad = @Antiguedad - 1;
  	Select @Diferencia = ROUND( @DiasAntig - ( @Antiguedad * 365.25 ) ,0 );
  	if ( @Diferencia = 365 )
  	begin
    	Select @Antiguedad = @Antiguedad + 1, @Diferencia = 0;
  	end

	Select @AntiguedadTemp = @Antiguedad+1;

  	select @DiasIdeales = dbo.SP_IMSS_YEAR( @Tabla, @AntiguedadTemp, 1 )
  	select @Resultado = @DiasIdeales * ( @Diferencia / 365.00 );

  	while ( @Antiguedad > 0 )
  	begin
    	select @DiasIdeales = dbo.SP_IMSS_YEAR( @Tabla, @Antiguedad, 1 )

    	Select @Resultado = @Resultado + @DiasIdeales;
    	Select @Antiguedad = @Antiguedad - 1;
  	end
	Return @Resultado;
end
GO

/* SP_UPDATE_AUSENCIA: Considerar AUSENCIA.AU_HORAS_NT ( Sugerencia QUINCENALES ) */
/* Patch 395 # Seq: 19 Modificado */
ALTER PROCEDURE SP_UPDATE_AUSENCIA
    				@EMPLEADO INTEGER,
    				@FECHA DATETIME,
    				@HORARIO CHAR(6),
    				@STATUS SMALLINT,
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@POSICION SMALLINT,
    				@INCIDENCIA CHAR(3),
    				@TIPODIA SMALLINT,
    				@ORDINARIAS NUMERIC(15,2),
    				@DOBLES NUMERIC(15,2),
    				@TRIPLES NUMERIC(15,2),
    				@DESCANSO NUMERIC(15,2),
    				@PERMISOCG NUMERIC(15,2),
    				@PERMISOSG NUMERIC(15,2),
                                @HORASNT NUMERIC(15,2)
AS
        SET NOCOUNT ON
  	declare @Usuario SmallInt;
  	declare @Extras Numeric( 15, 2 );
  	declare @Tardes Numeric( 15, 2 );
    select @Extras = @Dobles + @Triples;

    select @Usuario = US_CODIGO, @Tardes = AU_TARDES
    from AUSENCIA
    where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha )

	if ( @Usuario is NULL )
    begin
		insert into AUSENCIA( CB_CODIGO,
                              AU_FECHA,
                              HO_CODIGO,
                              AU_STATUS,
                              PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              AU_POSICIO,
                              AU_TIPO,
                              AU_TIPODIA,
                              AU_HORAS,
                              AU_DOBLES,
                              AU_TRIPLES,
                              AU_EXTRAS,
                              AU_DES_TRA,
                              AU_PER_CG,
                              AU_PER_SG,
                              AU_HORASNT )
         values ( @Empleado,
                  @Fecha,
                  @Horario,
                  @Status,
                  @Anio,
                  @Tipo,
                  @Numero,
                  @Posicion,
                  @Incidencia,
                  @TipoDia,
                  @Ordinarias,
                  @Dobles,
                  @Triples,
                  @Extras,
                  @Descanso,
                  @PermisoCG,
                  @PermisoSG,
                  @HorasNT );
    end
    else
    begin
          if ( @Usuario = 0 )
          begin

               if (( @TipoDia = 8 ) and ( @Tardes > 0 )) 
               begin
                 	Select @Tardes = 0;
                 	if ( @Incidencia = 'RE' ) Select @Incidencia = '';


               end
               update AUSENCIA set PE_YEAR = @Anio,
                                   PE_TIPO = @Tipo,
                                   PE_NUMERO = @Numero,
                                   AU_POSICIO = @Posicion,
                                   AU_TIPO = @Incidencia,
                                   AU_TIPODIA = @TipoDia,
                                   AU_HORAS = @Ordinarias,
                                   AU_DOBLES = @Dobles,
                                   AU_TRIPLES = @Triples,
                                   AU_EXTRAS = @Extras,
                                   AU_DES_TRA = @Descanso,
                                   AU_PER_CG = @PermisoCG,
                                   AU_PER_SG = @PermisoSG,
                                   AU_TARDES = @Tardes,
                                   AU_HORASNT = @HorasNT
               where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha );
          end
          else
               update AUSENCIA set PE_YEAR = @Anio,
                                   PE_TIPO = @Tipo,
                                   PE_NUMERO = @Numero,
                                   AU_DOBLES = @Dobles,
                                   AU_TRIPLES = @Triples,
                                   AU_EXTRAS = @Extras,
                                   AU_POSICIO = @Posicion
               where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha );
     end
     execute SP_CLAS_AUSENCIA @Empleado, @Fecha ;
GO

/* SP_INVESTIGA_STATUS: Considerar fechas de Asistencia del Per�odo ( Idea ST # 362 ) */
/* Patch 395 # Seq: 225 Modificado */
ALTER FUNCTION SP_INVESTIGA_STATUS (
  @Empleado INTEGER,
  @Fecha DATETIME
)
RETURNS SMALLINT
AS
BEGIN
     declare @Status SMALLINT
     declare @Turno CHAR(6)
     declare @FechaTurno DATETIME
     declare @TipoNomina SmallInt

     select @Turno = CB_TURNO, @FechaTurno = CB_FEC_TUR
     from COLABORA
     where CB_CODIGO = @Empleado

     if (@FechaTurno > @Fecha)
        select @Turno = dbo.SP_KARDEX_CB_TURNO( @Fecha, @Empleado )

     select @TipoNomina=TU_NOMINA
     from TURNO
     where TU_CODIGO = @Turno

     select @Status = MAX(PE_STATUS)
     from PERIODO
     where PE_NUMERO < 200
     and PE_ASI_INI <= @Fecha    /* Se Compara contra las fechas de asistencia */
     and PE_ASI_FIN >= @Fecha
     and PE_TIPO = @TipoNomina

     if @Status is NULL Select @Status = 0;
     Return @Status
end
GO

/* SP_CAFE_SEMANAL: Considerar fechas de Asistencia del Per�odo ( Idea ST # 362 ) */
/* Patch 395 # Seq: 85 Modificado */
ALTER function DBO.SP_CAFE_SEMANAL(
@EMPLEADO INTEGER,
@FECHA DATETIME,
@ANIO SMALLINT,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT,
@TIPOPERIODO SMALLINT )
RETURNS Integer
as
begin
     declare @FechaInicial DateTime;
     declare @FechaFinal DateTime;
     declare @Resultado Integer;
     if ( @Empleado is Null )
        set @Resultado = 0
     else
     begin
          select @FechaInicial = P.PE_ASI_INI, @FechaFinal = P.PE_ASI_FIN from DBO.PERIODO as P
          where ( P.PE_YEAR = @Anio ) and
                ( P.PE_TIPO = @TipoPeriodo ) and
                ( P.PE_ASI_INI <= @Fecha ) and
                ( P.PE_ASI_FIN >= @Fecha );
          if ( @FechaInicial is Null )
          begin
               set @FechaInicial = @Fecha;
               set @FechaFinal = @Fecha;
          end;
          select @Resultado = DBO.SP_CAFE_SUMA_COMIDAS( @Empleado, @FechaInicial, @FechaFinal, @TipoComida, @TipoResultado );
     end
     Return( @Resultado );
end
GO

/* SP_CAFE_AL_DIA: Considerar fechas de Asistencia del Per�odo ( Idea ST # 362 ) */
/* Patch 395 # Seq: 86 Modificado */
ALTER function DBO.SP_CAFE_AL_DIA(
@EMPLEADO INTEGER,
@FECHA DATETIME,
@ANIO SMALLINT,
@TIPOCOMIDA SMALLINT,
@TIPORESULTADO SMALLINT,
@TIPOPERIODO SMALLINT )
RETURNS Integer
as
begin
     declare @FechaInicial DateTime;
     declare @Resultado Integer;
     if ( @Empleado is Null )
        set @Resultado = 0
     else
     begin
          select @FechaInicial = P.PE_ASI_INI from DBO.PERIODO as P
          where ( P.PE_YEAR = @Anio ) and
                ( P.PE_TIPO = @TipoPeriodo ) and
                ( P.PE_ASI_INI <= @Fecha ) and
                ( P.PE_ASI_FIN >= @Fecha );
          if ( @FechaInicial is Null )
             set @FechaInicial = @Fecha;
          select @Resultado = DBO.SP_CAFE_SUMA_COMIDAS( @Empleado, @FechaInicial, @Fecha, @TipoComida, @TipoResultado );
     end
     Return( @Resultado );
end
GO

/* AFECTA_FALTAS: Considerar fechas de Asistencia del Per�odo ( Idea ST # 362 ) */
/* Patch 395 # Seq: 154 Modificado */
ALTER PROCEDURE AFECTA_FALTAS
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@FACTOR INTEGER
AS
        SET NOCOUNT ON
	declare @FechaIni   DateTime;
  	declare @Incidencia Char( 6 );
  	declare @TipoDia    SmallInt;
  	declare @Empleado   Integer;
  	declare @FEC_INI    DateTime;
  	declare @MOTIVO     SmallInt;
  	declare @DIAS       Numeric(15,2);

  	select @FechaIni = P.PE_ASI_INI  /* Fecha de inicio de asistencia */
	from PERIODO P
	where
         ( P.PE_YEAR = @Anio ) and
         ( P.PE_TIPO = @Tipo ) and
         ( P.PE_NUMERO = @Numero )
	
	DECLARE TemporalFaltas CURSOR FOR 
  		select F.CB_CODIGO, F.FA_FEC_INI, F.FA_MOTIVO, F.FA_DIAS
  		from   FALTAS F
       	left outer join NOMINA N on 
         	( N.PE_YEAR = @Anio ) 
		and ( N.PE_TIPO = @Tipo ) 
		and ( N.PE_NUMERO = @Numero ) 
		and ( N.CB_CODIGO = F.CB_CODIGO )
  		where ( F.PE_YEAR = @Anio ) 
		and ( F.PE_TIPO = @Tipo ) 
		and ( F.PE_NUMERO = @Numero )
		and ( F.FA_DIA_HOR = 'D' ) 
		and ( F.FA_MOTIVO  IN (0,1,2,4,12))

   	OPEN TemporalFaltas
   	FETCH NEXT FROM TemporalFaltas
   	into @Empleado, @FEC_INI, @MOTIVO, @DIAS
    WHILE @@FETCH_STATUS = 0
   	BEGIN
        if ( @FEC_INI <= '12/30/1899' ) 
		begin
			select @FEC_INI = @FechaIni;
		end
     	if ( @MOTIVO = 0 ) 
     	begin
       		Select @TipoDia = 0, @Incidencia = 'FI'
     	end
     	else if ( @MOTIVO = 1 )
     	begin
       		Select @TipoDia = 5, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 2 )  
     	begin
       		Select @TipoDia = 4, @Incidencia = 'FJ'
     	end
     	else if ( @MOTIVO = 4 ) 
     	begin
       		Select @TipoDia = 6, @Incidencia = 'FJ'
     	end
     	else  
     	begin
       		Select @TipoDia = 0, @Incidencia = '';
     	end
     
    
     	while ( @DIAS > 0 ) 
     	begin
       		if ( @MOTIVO <> 12 ) 
         		execute AFECTA_1_FALTA @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor 
       		else if ( @FEC_INI < @FechaIni )
         		execute AFECTA_1_AJUSTE @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor 
       
       		select @DIAS = @DIAS - 1, @FEC_INI = @FEC_INI + 1
     	end
   		
		FETCH NEXT FROM TemporalFaltas
   		into @Empleado, @FEC_INI, @MOTIVO, @DIAS 
  	end

   CLOSE TemporalFaltas
   DEALLOCATE TemporalFaltas
GO

/* SP_CONFLICTO_RESERVA: Detecta conflictos en reservaciones ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 227 Agregado */
CREATE FUNCTION SP_CONFLICTO_RESERVA( @Original FolioGrande )
RETURNS FolioGrande
AS
BEGIN
  DECLARE @FolioConflicto FolioGrande
  DECLARE @Aula     Codigo
  DECLARE @FechaIni Fecha
  DECLARE @FechaFin Fecha
  DECLARE @HoraIni  Hora
  DECLARE @HoraFin  Hora

  set @FolioConflicto = 0;

  select @Aula     = AL_CODIGO,
         @FechaIni = RV_FEC_INI,
         @FechaFin = RV_FEC_FIN,
         @HoraIni  = RV_HOR_INI,
         @HoraFin  = RV_HOR_FIN
  from   RESERVA
  where  RV_FOLIO = @Original

  -- Valida que exista el folio original
  if ( @FechaIni is not NULL )
  BEGIN
    select @FolioConflicto = MIN( RV_FOLIO )
    from   RESERVA
    where  RV_FOLIO <> @Original and
           AL_CODIGO = @Aula and
           ( RV_FEC_INI < @FechaFin OR ( RV_FEC_INI = @FechaFin and RV_HOR_INI < @HoraFin )) AND
           ( RV_FEC_FIN > @FechaIni OR ( RV_FEC_FIN = @FechaIni and RV_HOR_FIN > @HoraIni ))
  END

  RETURN COALESCE( @FolioConflicto, 0 )
END
GO

/* SP_LISTA_CONFLICTO_RESERVA: Lista conflictos en reservaciones ( Sugerencia ST # 369 ) */
/* Patch 395 # Seq: 228 Agregado */
CREATE  FUNCTION SP_LISTA_CONFLICTO_RESERVA( @Original FolioGrande )
RETURNS @RegLista TABLE
( RV_FOLIO INTEGER )
AS
BEGIN
  DECLARE @FolioConflicto FolioGrande
  DECLARE @Aula     Codigo
  DECLARE @FechaIni Fecha
  DECLARE @FechaFin Fecha
  DECLARE @HoraIni  Hora
  DECLARE @HoraFin  Hora

  select @Aula     = AL_CODIGO,
         @FechaIni = RV_FEC_INI,
         @FechaFin = RV_FEC_FIN,
         @HoraIni  = RV_HOR_INI,
         @HoraFin  = RV_HOR_FIN
  from   RESERVA
  where  RV_FOLIO = @Original
 
  -- Valida que exista el folio original
  if ( @FechaIni is not NULL )
  BEGIN
    insert into @RegLista
    select RV_FOLIO
    from   RESERVA
    where  RV_FOLIO <> @Original and 
           AL_CODIGO = @Aula and 
           ( RV_FEC_INI < @FechaFin OR ( RV_FEC_INI = @FechaFin and RV_HOR_INI < @HoraFin )) AND 
           ( RV_FEC_FIN > @FechaIni OR ( RV_FEC_FIN = @FechaIni and RV_HOR_FIN > @HoraIni ))
    order by RV_FOLIO
  END

  RETURN
END
GO

/* SP_RESERVA_FOLIO: Auxilio para reportear cursos */
/* Patch 395 # Seq: Agregado */
CREATE FUNCTION SP_RESERVA_FOLIO( @SESION FolioGrande, @POSICION SMALLINT )
RETURNS FolioGrande
AS
BEGIN
     declare @Resultado FolioGrande;
     declare @Ciclo SMALLINT;
     set @Ciclo = 1;
     declare Temporal cursor READ_ONLY STATIC for
       select RV_FOLIO from RESERVA  where ( SE_FOLIO = @SESION )
       order by RV_FEC_INI, RV_HOR_INI;
     open Temporal;
     if ( @@CURSOR_ROWS >= @POSICION )
     begin
          fetch Next from Temporal into @Resultado;
          while ( @@Fetch_Status = 0 ) and ( @Ciclo <> @POSICION )
          begin
               set @Ciclo = @Ciclo + 1;
               fetch Next from Temporal into @Resultado;
          end
     end
     if ( @Resultado is NULL ) or ( @Ciclo <> @POSICION )
     begin
          set @Resultado = 0;
     end
     close Temporal;
     deallocate Temporal;
     RETURN @Resultado;
END
GO

/**************************************************/
/* D:\3Win_13\Datos\SQLServer\3DatosFunciones.sql */
/**************************************************/

/*********************************************/
/******** # de VERSION ***********************/
/*********************************************/

update GLOBAL set GL_FORMULA = '395' where ( GL_CODIGO = 133 )
GO
