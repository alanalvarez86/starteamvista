
/* Patch 463: 17418 */ 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'LIMPIA_ACUMULADOS_TOTALES_CONCEPTOS') AND type IN ( N'P', N'PC' ))
	drop procedure LIMPIA_ACUMULADOS_TOTALES_CONCEPTOS

GO

/* Patch 463: 17418 */ 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'RECALCULA_ACUMULADOS') AND type IN ( N'P', N'PC' ))
	drop procedure RECALCULA_ACUMULADOS

GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT,
					@STATUS SMALLINT, 
					@lAlgunosConceptos Booleano = 'N', 
					@ListaConceptos Formula  = '' 
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @TIPO SMALLINT;
	DECLARE @NUMERO SMALLINT;
	
	IF @lAlgunosConceptos = 'N' 
	begin
		exec LIMPIA_ACUMULADOS_RS		@ANIO, @EMPLEADO, @MES  
		exec LIMPIA_ACUMULADOS_TOTALES	@ANIO, @EMPLEADO, @MES 
	end
	else
	begin
		exec LIMPIA_ACUMULADOS_RS_CONCEPTOS @ANIO, @EMPLEADO, @MES, @ListaConceptos		
	end

	Declare TemporalAcumula Cursor for
		select P.PE_TIPO, P.PE_NUMERO from NOMINA N join PERIODO P on
               ( P.PE_YEAR = N.PE_YEAR ) and
               ( P.PE_TIPO = N.PE_TIPO ) and
               ( P.PE_NUMERO = N.PE_NUMERO )
        where
             ( N.NO_STATUS = @Status ) and
             ( N.CB_CODIGO = @Empleado ) and
             ( N.PE_YEAR = @Anio ) and
             ( P.PE_MES = @Mes )

	OPEN TemporalAcumula
	FETCH NEXT FROM TemporalAcumula
	INTO @Tipo, @Numero 
    WHILE @@FETCH_STATUS = 0
	begin
		IF @lAlgunosConceptos = 'N' 
			EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, 1;
		ELSE 
			EXECUTE RECALCULA_ACUMULADOS_CONCEPTOS @Anio, @Empleado, @Mes, @Tipo, @Numero, @ListaConceptos

		FETCH NEXT FROM TemporalAcumula
		INTO @Tipo, @Numero 
    end
    Close TemporalAcumula
    Deallocate TemporalAcumula
	
	IF @lAlgunosConceptos = 'N' 
		exec SP_AcumuladosRazonSocial_SumarAnual @ANIO, @EMPLEADO
	ELSE
		exec SP_AcumuladosRazonSocial_SumarAnual @ANIO, @EMPLEADO, @ListaConceptos 
END

GO

/* Patch 463: 17418 */ 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'LIMPIA_ACUMULADOS_RS_CONCEPTOS') AND type IN ( N'P', N'PC' ))
	drop procedure LIMPIA_ACUMULADOS_RS_CONCEPTOS

GO

CREATE  PROCEDURE LIMPIA_ACUMULADOS_RS_CONCEPTOS(
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT, 
					@ListaConceptos Formula 
					) 
AS
BEGIN
	set nocount on 	

	declare @Query   nvarchar(210)
    declare @mesChar nvarchar(2)
    declare @Params	 nvarchar( 150  )
	
	if ( @Mes < 10 )                   
        set @mesChar = '0'         
	else
		set @mesChar = '' 

    set @mesChar = @mesChar + cast( @Mes as varchar(2) )

	set @Query = 'update ACUMULA_RS set AC_MES_' + @mesChar + ' = 0 
				  where ( AC_YEAR = @Anio ) and (CB_CODIGO = @Empleado ) and ( CO_NUMERO in ( select items from dbo.FN_SPLIT(@ListaConceptos, '','' ) ) )'
                                
	set @Params  = N'@Anio status, @Empleado int,  @ListaConceptos varchar(255)'	
	
	exec sp_executesql @Query, @Params, @Anio, @Empleado, @ListaConceptos 

	set @Query = 'update ACUMULA set AC_MES_' + @mesChar + ' = 0 
				  where ( AC_YEAR = @Anio ) and (CB_CODIGO = @Empleado ) and ( CO_NUMERO in ( select items from dbo.FN_SPLIT(@ListaConceptos, '','' ) ) )'
                                      
	set @Params  = N'@Anio status, @Empleado int,  @ListaConceptos varchar(255)'
	
	exec sp_executesql @Query, @Params, @Anio, @Empleado, @ListaConceptos 
END

