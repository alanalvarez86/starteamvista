/*****************************************/
/**** Foreign Keys ***********************/
/*****************************************/

/* Tabla COMPETEN: Eliminar llave for�nea hacia TCOMPETE */
/* Patch 396 # Seq: 1 Agregado */
alter table COMPETEN drop constraint FK_COMPETEN_TCOMPETE
GO

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/* Trigger TD_TCOMPETE */
/* Patch 398 # Seq: 84 Agregado */
CREATE TRIGGER TD_TCOMPETE ON TCOMPETE AFTER DELETE AS
BEGIN
     SET NOCOUNT ON
     update COMPETEN set COMPETEN.TC_CODIGO = ''
     from COMPETEN, Deleted where ( COMPETEN.TC_CODIGO = Deleted.TC_CODIGO );
END
GO

/* Trigger TU_TCOMPETE */
/* Patch 398 # Seq: 85 Agregado */
CREATE TRIGGER TU_TCOMPETE ON TCOMPETE AFTER UPDATE AS
BEGIN
     IF UPDATE( TC_CODIGO )
     BEGIN
          SET NOCOUNT ON
          declare @NewCodigo Codigo, @OldCodigo Codigo;

          select @NewCodigo = TC_CODIGO from Inserted;
          select @OldCodigo = TC_CODIGO from Deleted;

          update COMPETEN set TC_CODIGO = @NewCodigo
          where ( TC_CODIGO = @OldCodigo );
     END
END
GO

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

update GLOBAL set GL_FORMULA = '396' where ( GL_CODIGO = 133 )
GO
