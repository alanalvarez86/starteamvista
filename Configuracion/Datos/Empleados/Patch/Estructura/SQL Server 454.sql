/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/


if exists (select * from sysobjects where id = object_id(N'SP_HuboReingresoEnMes') and xtype in (N'FN', N'IF', N'TF')) 
	drop function SP_HuboReingresoEnMes
GO 

CREATE FUNCTION DBO.SP_HuboReingresoEnMes( @Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER) 
RETURNS Booleano 
AS
BEGIN 	
		declare @FechaIngreso Fecha 
		declare @FechaPeriodoFinMin Fecha
		 
		select top 1 @FechaIngreso= CB_FEC_ING, @FechaPeriodoFinMin=PERIODO.PE_FEC_FIN  from COLABORA, PERIODO 
		where PERIODO.PE_STATUS = 6 and PE_YEAR = @Anio and PE_MES = @Mes  and COLABORA.CB_CODIGO = @Empleado
		order by PE_FEC_FIN 

		if (@FechaIngreso <=@FechaPeriodoFinMin ) 
			return 'N' 
        
		return 'S' 
END 


GO 

if exists (select * from sysobjects where id = object_id(N'SP_VALOR_CONCEPTO_2') and xtype in (N'FN', N'IF', N'TF')) 
	drop function SP_VALOR_CONCEPTO_2

GO 
CREATE FUNCTION DBO.SP_VALOR_CONCEPTO_2(
 				@Concepto SMALLINT,
				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
				@Razonsocial CHAR(6),
				@TipoNomina SMALLINT,
				@ValidaReingreso CHAR(1)
				 )

RETURNS NUMERIC(15,2)
AS
BEGIN 
	Declare @Resultado  Pesos
	DECLARE @lUsaAcumulaRS Booleano 
	
	set @Resultado = 0 	
	set @lUsaAcumulaRS = 'N' 

	if ( @TipoNomina = -1 ) 
		set @lUsaAcumulaRS  = 'S' 
	
	if ( @lUsaAcumulaRS ='S' and @ValidaReingreso = 'S' ) 
	begin 		
		if  dbo.SP_HuboReingresoEnMes(@Mes, @Anio, @Empleado) = 'N'
			set @lUsaAcumulaRS = 'S' 
		else
			set @lUsaAcumulaRS = 'N' 		
	end 

	if @lUsaAcumulaRS = 'S' 
	begin 
		select @Resultado = dbo.SP_A2(@Concepto, @Mes, @Anio,  @Empleado, @RazonSocial ); 
	end 
	else
	begin 
		select @Resultado = DBO.SP_VALOR_CONCEPTO( @Concepto, @Mes, @Anio, @Empleado, @Razonsocial, @TipoNomina, @ValidaReingreso)
	end 

	return @Resultado 
END 
go

if exists (select * from sysobjects where id = object_id(N'SP_RA_2') and xtype in (N'FN', N'IF', N'TF')) 
	drop function SP_RA_2

GO  

CREATE FUNCTION DBO.SP_RA_2(
  				@Concepto SMALLINT,
  				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
  				@Referencia char(8),
				@RazonSocial char(6),
  				@TipoNomina SMALLINT,  				
  				@ValidaReingreso CHAR(1) )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE @Resultado AS NUMERIC(15,2)
	DECLARE @lUsaAcumulaRS Booleano 
	
	set @lUsaAcumulaRS = 'N' 

	if ( @Referencia = '' and @TipoNomina = -1 ) 
		set @lUsaAcumulaRS  = 'S' 
	
	if ( @lUsaAcumulaRS ='S' and @ValidaReingreso = 'S' ) 
	begin 		
		if  dbo.SP_HuboReingresoEnMes(@Mes, @Anio, @Empleado) = 'N'
			set @lUsaAcumulaRS = 'S' 
		else
			set @lUsaAcumulaRS = 'N' 
	end 

	if @lUsaAcumulaRS = 'S' 
	begin 	
		select @Resultado = dbo.SP_A2(@Concepto, @Mes, @Anio,  @Empleado, @RazonSocial ); 
	end 
	else
	begin 
		select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
		from   MOVIMIEN
		left outer join NOMINA on NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
		left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
		left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
		left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
		left outer join COLABORA on COLABORA.CB_CODIGO = MOVIMIEN.CB_CODIGO 
		where ( MOVIMIEN.PE_YEAR = @Anio ) AND
			 ( MOVIMIEN.CB_CODIGO = @Empleado ) AND 		  
				( MOVIMIEN.CO_NUMERO = @Concepto ) AND
				( MOVIMIEN.MO_ACTIVO = 'S' ) AND
				( PERIODO.PE_STATUS = 6 ) AND
				( PERIODO.PE_MES = @Mes ) AND
				( ( @TipoNomina = -1 ) OR ( MOVIMIEN.PE_TIPO = @TipoNomina ) ) AND		    
				( ( @Referencia = '' ) OR ( MOVIMIEN.MO_REFEREN = @Referencia ) ) AND		    
				( ( @RazonSocial = '' ) OR ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) AND            
				  ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) )
	end 

 	if ( @Resultado is NULL )
        select @Resultado = 0
	RETURN @Resultado
END

GO 
 

if exists (select * from sysobjects where id = object_id(N'SP_RAS') and xtype in (N'FN', N'IF', N'TF')) 
	drop function SP_RAS

GO

CREATE FUNCTION DBO.SP_RAS(
		     @Concepto SMALLINT,
    			@NumeroIni SMALLINT,
    			@Numerofin SMALLINT,
			@Anio SMALLINT,
			@Empleado INTEGER,
			@Referencia char(8),
			@RazonSocial CHAR(6),
    		     @TipoNomina SMALLINT,   		    		
			@ValidaReingreso CHAR(1))
RETURNS NUMERIC(15,2)
AS
begin
	declare @Temporal NUMERIC(15,2);
	declare @Resultado NUMERIC(15,2);

  select @Resultado = 0;
  while ( @NumeroIni <= @NumeroFin )
  begin
	  if @Concepto < 1000 
	  	select @Temporal = dbo.SP_RA_2 ( @Concepto, @NumeroIni, @Anio, @Empleado, @Referencia, @RazonSocial, @TipoNomina, @ValidaReingreso )
	  else
		select @Temporal = dbo.SP_VALOR_CONCEPTO_2 ( @Concepto, @NumeroIni, @Anio, @Empleado, @RazonSocial, @TipoNomina, @ValidaReingreso )
		  
	if ( @Temporal is NOT NULL )
	    select @Resultado = @Resultado + @Temporal;
	select @NumeroIni = @NumeroIni + 1;
  end
	Return @Resultado;
end


go 


/* #14180: Se elimina columna del diccionario */
IF (EXISTS(SELECT * FROM sys.columns WHERE Name = N'AT_CAMPO' AND Object_ID = Object_ID(N'R_ATRIBUTO')) and (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'R_ATRIBUTO')) )
    DELETE R_ATRIBUTO WHERE EN_CODIGO = 310 AND AT_CAMPO = 'CC_NOMBRE'
GO

/* #13917: Columna para el calculo de la pension */
IF not EXISTS (select * from syscolumns where id=object_id('TPENSION') and name='TP_DESC_N')
    alter table TPENSION add TP_DESC_N Booleano null
GO

update TPENSION set TP_DESC_N = 'N' where TP_DESC_N is null
GO

alter table TPENSION alter column TP_DESC_N Booleano not null
GO

/* 14972*/
alter procedure COSTO_SUBCTA_CARGO_HIJO(
	@CostoID FolioGrande,
	@NominaID FolioGrande,
	@Concepto Concepto,
	@CostoClasifi OrdenTrabajo,
	@CC_codigo Codigo,
	@Monto Pesos
	)
as
begin
	/* En este SP, se programan las reglas para crear las cuentas contables*/
	return 0
end
GO

alter procedure COSTO_SUBCTA_ABONO_hijo(
	@CostoID FolioGrande,
	@NominaID FolioGrande,
	@Concepto Concepto,
	@CostoClasifi OrdenTrabajo,
	@CC_codigo Codigo,
	@Monto Pesos
	)
as
begin
	/* En este SP, se programan las reglas para crear las cuentas contables*/
	return 0
end
GO

ALTER PROCEDURE dbo.Costos_CalculaCuentasContables_Hook( @NominaID	FolioGrande )
AS
BEGIN
	declare @CostoID FolioGrande;
	declare @Concepto Concepto;
	declare @CostoClasifi OrdenTrabajo;
	declare @CC_codigo Codigo;
	declare @Monto Pesos;


	DELETE	T_Costos_SubCta
	WHERE	NominaID = @NominaID

	DECLARE CursorCostos CURSOR LOCAL FOR
		select CostoID, CO_NUMERO, CostoClasifi, CC_CODIGO, CostoMonto
		from T_COSTOS
		where NominaID = @NominaID

	OPEN CursorCostos
	FETCH NEXT FROM CursorCostos
	INTO	@CostoID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto

	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN
		exec COSTO_SUBCTA_Cargo_HIJO @CostoID, @NominaID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto
		exec COSTO_SUBCTA_Abono_HIJO @CostoID, @NominaID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto

		FETCH NEXT FROM CursorCostos
		INTO	@CostoID, @Concepto, @CostoClasifi, @CC_CODIGO, @Monto
	END

	CLOSE CursorCostos
	DEALLOCATE CursorCostos
END
GO

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('TOT_COSTOS') )
	DROP VIEW TOT_COSTOS
GO

CREATE VIEW TOT_COSTOS
as
select PE_YEAR, PE_TIPO, PE_NUMERO, CT_SUB_CTA, CT_CAR_ABO, CT_NUMERO,
    cast ( ( sum(CT_MONTO)) as numeric(15,2)) CP_MONTO
from COSTOS_POL
group by PE_YEAR, PE_TIPO, PE_NUMERO, CT_SUB_CTA, CT_CAR_ABO, CT_NUMERO
GO

IF NOT EXISTS (select * from sysobjects where name='TimListas' ) 
BEGIN 
    create table TimListas ( 
    TL_IDLISTA FolioGrande,
    TL_VALOR FolioChico,
    TL_DESCRIP Formula,
    TL_TEXTO Descripcion,
    TL_INGLES Descripcion,
    TL_NUMERO Pesos,
    TL_ACTIVO Booleano, 
     constraint [PK_TIMLISTAS] Primary key ([TL_IDLISTA],[TL_VALOR]));
END
ELSE
BEGIN

	if not exists (SELECT * FROM syscolumns WHERE id=object_id('TimListas') AND name='TL_ACTIVO')
	BEGIN
		 alter table TimListas add TL_ACTIVO Booleano NULL
	END
END
GO


if exists (SELECT * FROM syscolumns WHERE id=object_id('TimListas') AND name='TL_ACTIVO')
BEGIN
	update TimListas set TL_ACTIVO = 'S' where TL_ACTIVO is null
END 
GO


if exists (SELECT * FROM syscolumns WHERE id=object_id('TimListas') AND name='TL_ACTIVO')
BEGIN
	alter table TimListas alter column TL_ACTIVO Booleano NOT NULL 
END 
GO

if (SELECT COUNT(TL_IDLISTA) AS Cantidad FROM TimListas) = 0
begin

		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 1 )
		begin
             INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 1, 'General de Ley Personas Morales', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 3 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 3, 'Personas Morales con Fines no Lucrativos', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 5 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 5, 'Sueldos, Salarios e Ingresos Asimilados a Salarios', 'S') 			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 6 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 6, 'Arrendamiento', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 7 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 7, 'Enajenaci�n o Adquisici�n de Bienes', 'S') 			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 8 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 8, 'Dem�s ingresos', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 9 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 9, 'Consolidaci�n', 'S') 			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 10 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 10, 'Residentes en el Extranjero', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 11 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 11, 'Ingresos por Dividendos', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 12 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 12, 'Personas F�sicas con Actividad Empresarial', 'S')			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 14 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 14, 'Ingresos por Intereses', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 15 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 15, 'Ingresos por Obtenci�n de Premios', 'S')			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 16 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 16, 'Sin obligaciones fiscales', 'S') 			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 20 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 20, 'Sociedades Cooperativas de Producci�n', 'S') 			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 21 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 21, 'Incorporaci�n Fiscal', 'S')     			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 22 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 22, 'Actividades Agr�colas, Ganaderas, Silv y Pesqueras', 'S')  			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 23 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 23, 'Opcional para Grupos de Sociedades', 'S')   			
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 600 and TL_VALOR = 24 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(600, 24, 'Coordinados', 'S')			
		end								                 
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 0 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 0, 'Sin Asignar', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 1 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 1, 'Asimilados a salarios', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 2 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 2, 'Sueldos y salarios', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 3 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 3, 'Jubilados', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 4 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 4, 'Pensionados', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 5 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 5, 'Asimilados, Sociedades Cooperativas', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 6 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 6, 'Asimilados, Asociaciones Civiles', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 7 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 7, 'Asimilados, Consejeros', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 8 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 8, 'Asimilados, Comisionistas', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 9 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 9, 'Asimilados, Honorarios', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 10 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 10, 'Asimilados, Acciones', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 11 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 11, 'Asimilados, Otros', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 99 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 99, 'Otro Regimen', 'S')
		end
end
GO

if (SELECT COUNT(TL_IDLISTA) AS Cantidad FROM TimListas where TL_IDLISTA = 5002) <> 13
begin

		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 0 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 0, 'Sin Asignar', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 1 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 1, 'Asimilados a salarios', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 2 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 2, 'Sueldos y salarios', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 3 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 3, 'Jubilados', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 4 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 4, 'Pensionados', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 5 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 5, 'Asimilados, Sociedades Cooperativas', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 6 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 6, 'Asimilados, Asociaciones Civiles', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 7 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 7, 'Asimilados, Consejeros', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 8 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 8, 'Asimilados, Comisionistas', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 9 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 9, 'Asimilados, Honorarios', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 10 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 10, 'Asimilados, Acciones', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 11 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 11, 'Asimilados, Otros', 'S')
		end
		if not EXISTS ( select 1 from TimListas where TL_IDLISTA = 5002 and TL_VALOR = 99 )
		begin
			INSERT INTO TimListas (TL_IDLISTA, TL_VALOR, TL_DESCRIP, TL_ACTIVO) VALUES(5002 , 99, 'Otro Regimen', 'S')
		end
end;

IF EXISTS(SELECT * FROM sys.objects WHERE object_id = object_id('V_TIM_REG') )
	DROP VIEW V_TIM_REG
GO

create view V_TIM_REG as 
Select TL_VALOR, TL_DESCRIP, TL_TEXTO, TL_INGLES, TL_NUMERO, TL_ACTIVO
from TimListas 
where TL_IDLISTA = 5002 
GO