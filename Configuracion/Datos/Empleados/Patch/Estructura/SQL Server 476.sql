if not exists( select NAME from SYSOBJECTS  where NAME='NOM_CONCIL' and XTYPE='U' )
begin
	CREATE TABLE NOM_CONCIL(
		PE_YEAR Anio,
		PE_TIPO NominaTipo,
		PE_NUMERO NominaNumero,
		CB_CODIGO NumeroEmpleado,
		NC_CURP Descripcion, 
		NC_STATUS	Status, 
		NC_RESULT	Status, 
		NC_RES_DESC DescLarga, 
		NC_VER_COD  DescLarga, 
		NC_VERSION	DescLarga, 
		NC_UUID		DescLarga, 
		NC_FEC_TIM  Fecha,
		NC_FACTURA  FolioGrande,
		US_CODIGO	Usuario, 
		US_FEC_MOD	Fecha,
		NC_APLICADO Booleano,
	
	 CONSTRAINT PK_NOM_CONCIL PRIMARY KEY CLUSTERED 
	(
		PE_YEAR ASC,
		PE_TIPO ASC,
		PE_NUMERO ASC,
		CB_CODIGO ASC
	) 
	) 


	ALTER TABLE NOM_CONCIL  WITH NOCHECK ADD  CONSTRAINT FK_PK_NOM_CONCIL_Periodo FOREIGN KEY(PE_YEAR, PE_TIPO, PE_NUMERO)
	REFERENCES PERIODO (PE_YEAR, PE_TIPO, PE_NUMERO)
	ON UPDATE CASCADE
	ON DELETE CASCADE

	ALTER TABLE NOM_CONCIL CHECK CONSTRAINT FK_PK_NOM_CONCIL_Periodo

end 
GO

IF exists( select * from sys.objects where object_id = object_id('VTIMCONCIL') )
    drop view VTIMCONCIL
GO
create view VTIMCONCIL
as
select 
	RSOCIAL.RS_CODIGO,
    NO.PE_YEAR, NO.PE_TIPO, NO.PE_NUMERO, 
	NO.CB_CODIGO, 
	EMP.PRETTYNAME, 	
	NO.NO_TIMBRO, 
	case 
		when NO.NO_TIMBRO = 0 then 'Pendiente' 
		when NO.NO_TIMBRO = 2 then 'Timbrada' 
		else 'Pendiente' 
	end  NO_TIM_DESC, 
	case  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID = ''	then NO.NO_STATUS  
		when NO.NO_TIMBRO = 0 and NO.NO_CAN_UID <> ''   then NO.NO_STATUS 
		when NO.NO_TIMBRO = 2							then 8 
		else	
			NO.NO_STATUS 				
	end NC_STATUS_TRESS, 	
	coalesce(NC_STATUS,-1) NC_STATUS, 
	coalesce(NC_RESULT, -1 ) NC_RESULT, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DESC, 
	coalesce(NC_RES_DESC, '' ) NC_RES_DES, 
    coalesce(NC_VER_COD, '' ) NC_VER_COD, 
	coalesce(NC_VERSION, '' ) NC_VERSION, 
	NO.NO_FACUUID,
	NO.NO_FACTURA, 
	coalesce(NC_FACTURA, 0 ) NC_FACTURA,
	coalesce(NC_UUID , '' ) NC_UUID, 
	coalesce(NC_FEC_TIM, '1899-12-30') NC_FEC_TIM,
	EMP.CB_CURP, 
	coalesce(NC_CURP, '') NC_CURP , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICADO , 
	coalesce(NC_APLICADO, 'N' ) NC_APLICAD , 
	EMP.CB_RFC 
from NOMINA  NO
join RPATRON on RPATRON.TB_CODIGO = NO.CB_PATRON 
join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
left outer join V_EMP_TIMB EMP on EMP.CB_CODIGO = NO.CB_CODIGO 
left outer join NOM_CONCIL NC on NC.PE_YEAR = NO.PE_YEAR and NC.PE_TIPO = NO.PE_TIPO and NC.PE_NUMERO = NO.PE_NUMERO  and  NC.CB_CODIGO = NO.CB_CODIGO 


GO 

if exists (select * from dbo.sysobjects where id = object_id(N'TimbradoConcilia_GetPeriodos') and xtype in (N'FN', N'IF', N'TF')) 
	drop function TimbradoConcilia_GetPeriodos
go 

create function TimbradoConcilia_GetPeriodos(  @RS_CODIGO Codigo, @PE_YEAR int,  @PE_MES status ) 
returns Table 
as
return ( 
select  PE_YEAR, PE_TIPO , PE_NUMERO, PE_TIMBRO, TOTAL, TIMBRADOS_TRESS, (TOTAL - CONCILIADOS) CONCILIADOS , 
	cast( case 
	when TOTAL > 0 then  100.00 -  ( CONCILIADOS*1.0 / TOTAL*1.0 ) * 100.00 
		else 0.00 
	end  as decimal(15,2) )  PORC_CONCILIADOS,  
	TIMBRADOS_SAT, 
		case 
		when CONCILIADOS = TOTAL then 'False'
		else 'True'  
	end  CONCILIAR,
	0 as CONCILIA_PROCESO ,
	cast ('' as VARCHAR(250) ) as CONCILIA_STATUS,
	0 as CONCILIA_AVANCE,
	0 as DB_PROCESO,
	cast ('' as VARCHAR(250) ) as NOMBRE_PERIODO, 
	cast ('' as VARCHAR(250) ) as DETALLE 	
from ( 

select NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO, COUNT(*) TOTAL, 
SUM( case  
		when NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_TRESS, 

SUM( case  
		when NC_RESULT  = 0  and NC_STATUS_TRESS = 8 then 1 
		else 0 
	 end 
	 ) TIMBRADOS_SAT, 

SUM( case  
		when NC_RESULT = 0 then 1 
		else 0 
	 end 
	 ) CONCILIADOS
from VTIMCONCIL NC
join PERIODO PE on  NC.PE_YEAR = PE.PE_YEAR and NC.PE_TIPO = PE.PE_TIPO and NC.PE_NUMERO = PE.PE_NUMERO 
where PE.PE_YEAR = @PE_YEAR and PE_MES = @PE_MES and NC.RS_CODIGO = @RS_CODIGO 
group by NC.PE_YEAR, NC.PE_TIPO , NC.PE_NUMERO, PE.PE_TIMBRO

) Conciliacion 
) 

go 

if exists (select * from dbo.sysobjects where id = object_id(N'TimbradoConcilia_GetNominasTRESSTabla') and xtype in (N'FN', N'IF', N'TF')) 
	drop function TimbradoConcilia_GetNominasTRESSTabla
go 

create function TimbradoConcilia_GetNominasTRESSTabla(  @RS_CODIGO Codigo, @PE_YEAR int,  @PE_TIPO int, @PE_NUMERO int ) 
returns Table 
as
return 
(
SELECT CB_CODIGO [Numero], CB_CURP [CURP], NC_STATUS_TRESS [Status], NO_FACUUID [UUID], NO_FACTURA [FacturaID] from VTIMCONCIL as [NOMINA] where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO =  @PE_NUMERO
and [NOMINA].RS_CODIGO = @RS_CODIGO
)

go 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TimbradoConcilia_Conciliar') AND type IN ( N'P', N'PC' ))
    drop procedure TimbradoConcilia_Conciliar
go

create procedure TimbradoConcilia_Conciliar( 	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero, 
	@Xml		nVarchar(max) ,
	@US_CODIGO	Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_PROC_ID FolioGrande,
	@Error		VARCHAR( 255 ) OUTPUT
)
as
begin 
	set @Error = '' 
---	parse XML 
--- por cada elemento va a existir una execucion 
--- TimbradoConcilia_ConciliarUnaNomina

	declare @XMLDoc XML 
	set @XMLDoc = convert ( XML, @XML  ) 
	
	
	select row_number() over (order by Numero) i,  Numero, CURP, Status, Result, ResultDesc, VersionCodigo, VersionDesc, UUID, FacturaID, FechaTimbre into #TmpNominasConcilia
	from ( 
	select            
            Numero    = coalesce(  t.c.value('(@Numero)', 'INT' ), 0) ,
            CURP        = coalesce(  t.c.value('(@CURP)', 'Varchar(50)' ), '' ),             
            Status   = coalesce(t.c.value('(@Status)', 'INT' ), 0),
            Result   = coalesce(t.c.value('(@Result)', 'INT' ), 0),            
			ResultDesc        = coalesce(  t.c.value('(@ResultDesc)', 'Varchar(256)' ), '' ),             
			VersionCodigo        = coalesce(  t.c.value('(@Version)', 'Varchar(50)' ), '' ),    
			VersionDesc    = coalesce(  t.c.value('(@VersionDesc)', 'Varchar(50)' ), '' ),    
			         
			UUID        = coalesce(  t.c.value('(@UUID)', 'Varchar(50)' ), '' ) , 
			FacturaID	=  coalesce(t.c.value('(@FacturaID)', 'INT' ), 0),    
			FechaTimbre =  coalesce(t.c.value('(@FechaTimbre)', 'Datetime' ), '1899-12-30')
     from 
     @XMLDoc.nodes('//RESPUESTA/SALIDA/NOMINAS/NOMINA') t(c)
	 cross apply @XMLDoc.nodes('//RESPUESTA/RESULTADO') tr(cr)
	 ) TmpNominasConcilia 

	
	delete from NOM_CONCIL 
	where PE_YEAR= @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO 
	and CB_CODIGO in  ( select Numero from #TmpNominasConcilia ) 

 
	insert NOM_CONCIL (
			PE_YEAR,
			PE_TIPO,
			PE_NUMERO,
			CB_CODIGO,
			NC_CURP,
			NC_STATUS,
			NC_RESULT,
			NC_RES_DESC,
			NC_VERSION,
			NC_UUID, 
			US_CODIGO, 
			US_FEC_MOD,
			NC_APLICADO, 
			NC_VER_COD, 
			NC_FEC_TIM, 
			NC_FACTURA 
		)
	select @PE_YEAR, @PE_TIPO, @PE_NUMERO, Numero, CURP, Status, Result, ResultDesc, VersionDesc, UUID, @US_CODIGO, @US_FEC_MOD, 
		case
			when Result = 0 then 'S'  
			else 'N' 
		end, 
		VersionCodigo, FechaTimbre, FacturaID
    from #TmpNominasConcilia
	left join VTIMCONCIL TC on TC.PE_YEAR = @PE_YEAR and TC.PE_TIPO = @PE_TIPO and TC.PE_NUMERO = @PE_NUMERO and TC.CB_CODIGO = Numero


	drop table #TmpNominasConcilia

	return 
end 
go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TimbradoConcilia_BitacoraTimbrar') AND type IN ( N'P', N'PC' ))
    drop procedure TimbradoConcilia_BitacoraTimbrar
go

create procedure TimbradoConcilia_BitacoraTimbrar(
	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero,
	@CB_CODIGO NumeroEmpleado,
	@US_CODIGO Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_NUMERO  FolioGrande, 
	@NO_TIMBRO	Status, 
	@NO_FACTURA FolioGrande, 
	@NO_FACUUID	DescLarga,
	@NO_FACUUIDAnterior	DescLarga
) as
begin 
	DECLARE @BI_TIPO Status
	DECLARE @BI_PROC_ID Status	
	DECLARE @BI_TEXTO Observaciones	
	DECLARE @BI_DATA varchar(max) 
	DECLARE @TipoNominaStr Descripcion 
	DECLARE @BI_CLASE Status
	DECLARE @BI_FEC_MOV Fecha

set @BI_TIPO = 0  /* Informativo */ 
SET @BI_CLASE = 0 
select @BI_PROC_ID = PC_PROC_ID from PROCESO where PC_NUMERO = @PC_NUMERO
set @BI_FEC_MOV = @US_FEC_MOD 
	
select @TipoNominaStr = TP_NOMBRE from TPERIODO where TP_TIPO = @PE_TIPO 
set @TipoNominaStr = coalesce( @TipoNominaStr, 'Periodo')

--@NO_TIMBRO
set @BI_DATA = '' 
if ( @NO_TIMBRO = 2 ) 
begin 
	select  @BI_TEXTO = 'Nuevo estatus: Timbrado, Empleado: '+cast( @CB_CODIGO as varchar(20)) 
		select  @BI_DATA =  @BI_TEXTO + ' , A�o:'+cast( @PE_YEAR as varchar(20)) + ' Numero:'+cast( @PE_NUMERO as varchar(20))+' Tipo: '+@TipoNominaStr +
		' UUID: '+ @NO_FACUUID + ' UUID Anterior: '+ @NO_FACUUIDAnterior
		
	end 
	if ( @NO_TIMBRO = 0 ) 
	begin 
		select  @BI_TEXTO = 'Nuevo estatus: Pendiente, Empleado: '+cast( @CB_CODIGO as varchar(20))
		select  @BI_DATA = @BI_TEXTO + ' , A�o:'+cast( @PE_YEAR as varchar(20)) + ' Numero:'+cast( @PE_NUMERO as varchar(20))+' Tipo: '+@TipoNominaStr +
	    ' UUID Anterior: '+ @NO_FACUUIDAnterior
	end 
	
	EXECUTE SP_INSERTAR_BITACORA   @US_CODIGO,@BI_PROC_ID,@BI_TIPO,@PC_NUMERO ,@BI_TEXTO ,@CB_CODIGO ,@BI_DATA ,@BI_CLASE ,@BI_FEC_MOV

end 


go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TimbradoConcilia_AplicarUnEmpleado') AND type IN ( N'P', N'PC' ))
    drop procedure TimbradoConcilia_AplicarUnEmpleado
go

create procedure TimbradoConcilia_AplicarUnEmpleado(
	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero,
	@CB_CODIGO NumeroEmpleado,
	@US_CODIGO Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_NUMERO  FolioGrande
	)
as
begin 

	set nocount on 

	declare @NC_CURP		Descripcion	
	declare @NC_STATUS		Status
	declare @NC_RESULT		Status
	declare @NC_RES_DESC	DescLarga
	declare @NC_VERSION		DescLarga
	declare @NC_UUID		DescLarga
	declare @NO_FACUUID		DescLarga
	declare @NC_STATUS_TRESS		Status 
	declare @NC_FACTURA		FolioGrande
	declare @CB_RFC		Descripcion
	declare @lAplicado		Booleano
	--
	select 
			@NC_CURP	 = NC_CURP,
			@NC_STATUS	 = NC_STATUS,
			@NC_RESULT	 = NC_RESULT,
			@NC_RES_DESC = NC_RES_DESC,
			@NC_VERSION  = NC_VERSION,
			@NO_FACUUID  = NO_FACUUID,
			@NC_UUID	 = NC_UUID, 
			@NC_STATUS_TRESS = NC_STATUS_TRESS, 
			@NC_FACTURA  = NC_FACTURA, 
			@CB_RFC		= CB_RFC
	from VTIMCONCIL 
	where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO and CB_CODIGO = @CB_CODIGO
	

	set @lAplicado = 'N' 
	-- Si el timbre esta cancelado en la nube pero en TRESS est� timbrado , se cancela
	if ( @NC_RESULT = 5 ) and (@NC_STATUS_TRESS > 6) 
	begin 
		-- Debe cancelarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 0, '',0, @US_CODIGO
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 0, 0, '', @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	else
	-- Si el timbre esta cancelado en TRESS pero en Nube est� timbrado , se timbra
	if ( @NC_RESULT in (4, 6 )  ) and (@NC_STATUS_TRESS <= 6 ) 
	begin 
		-- Debe timbrarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 2, '',0, @US_CODIGO, @NC_UUID 
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 2, @NC_FACTURA, @NC_UUID, @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	else
	if ( @NC_RESULT = 98 ) and (@NC_STATUS_TRESS > 6 ) and ( @NC_UUID <> '' ) 
	begin 
		-- Debe timbrarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 2, '',0, @US_CODIGO, @NC_UUID 
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 2, @NC_FACTURA, @NC_UUID, @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	else
	-- Si no existe datos en la nube pero en TRESS est� timbrado , se cancela en TRESS
	if ( @NC_RESULT = 99 ) and (@NC_STATUS_TRESS > 6) 
	begin 
		-- Debe cancelarse en TRESS 
		exec SP_TIMBRAR_EMPLEADO @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @CB_RFC, 0, '',0, @US_CODIGO
		exec TimbradoConcilia_BitacoraTimbrar @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO, 0, 0, '', @NO_FACUUID
		set @lAplicado = 'S' 
	end 
	

	if @lAplicado = 'S' 
	begin 
		update NOM_CONCIL 
		set 
			NC_APLICADO = 'S', 
			US_CODIGO = @US_CODIGO,		
			US_FEC_MOD = @US_FEC_MOD
		where 
			PE_YEAR = @PE_YEAR and
			PE_TIPO = @PE_TIPO and 
			PE_NUMERO = @PE_NUMERO and
			CB_CODIGO = @CB_CODIGO
	end 
	
end; 


go 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TimbradoConcilia_CalcularStatusPeriodo') AND type IN ( N'P', N'PC' ))
    drop procedure TimbradoConcilia_CalcularStatusPeriodo
go

CREATE procedure TimbradoConcilia_CalcularStatusPeriodo( @PE_YEAR Anio, @PE_TIPO NominaTipo, @PE_NUMERO NominaNumero)   
as  
begin  
 set nocount on   
 declare @Total int   
 declare @TotalAfectados int   
 declare @Timbrados int   
 declare @StatusTimbradoPendiente status    
 declare @StatusTimbradoParcial status    
 declare @StatusTimbradoTotal status    
 declare @NominaAfectada status   
 declare @NominaAfectadaParcial status      
   
	 set @StatusTimbradoPendiente = 0   
	 set @StatusTimbradoParcial = 1   
	 set @StatusTimbradoTotal = 2   
	 set @NominaAfectada = 6   
	 set @NominaAfectadaParcial = 5
   
 
	 select @Total = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  
	 select @TotalAfectados = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and NO_STATUS = @NominaAfectada   
   
	 if @Total > 0   
	 begin     
		  select @Timbrados = count(*) from NOMINA where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  NO_STATUS = @NominaAfectada  and NO_TIMBRO = @StatusTimbradoTotal  

		  if ( @TotalAfectados > 0 ) 
		  begin 			
			  if ( @Total = @Timbrados )  
				  update PERIODO set PE_TIMBRO = @StatusTimbradoTotal  where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial   
			  else 
			  if ( @Timbrados = 0 ) 
				  update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial   
			  else  
				  update PERIODO set PE_TIMBRO = @StatusTimbradoParcial where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and  PE_STATUS >= @NominaAfectadaParcial      			  		 		  			
		  end
		  else
		  begin
				 update PERIODO set PE_TIMBRO = @StatusTimbradoPendiente where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO  and PE_NUMERO = @PE_NUMERO  and PE_STATUS >= @NominaAfectadaParcial   
		  end; 
	end;  
    
end


GO 

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TimbradoConcilia_Aplicar') AND type IN ( N'P', N'PC' ))
    drop procedure TimbradoConcilia_Aplicar
go


create procedure TimbradoConcilia_Aplicar( 	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero, 	
	@US_CODIGO	Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_NUMERO FolioGrande, 
	@Error		VARCHAR( 255 ) OUTPUT
)
as
begin 
	set @Error = '' 
	select row_number() over (order by CB_CODIGO) i,  CB_CODIGO
	into #TmpNominasAplicar
	from NOM_CONCIL
	where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO and NOM_CONCIL.NC_APLICADO = 'N' and NOM_CONCIL.NC_RESULT <> 0 


	declare @i FolioGrande 
	declare @iMax FolioGrande 

	select @iMax = COUNT(*) from #TmpNominasAplicar

	declare @CB_CODIGO  NumeroEmpleado 

	set @i= 1 
	while @i <= @iMax 
	begin 
		select @CB_CODIGO = CB_CODIGO from #TmpNominasAplicar where i = @i 
		exec TimbradoConcilia_AplicarUnEmpleado @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO
		set @i= @i+1 
	end 

	exec TimbradoConcilia_CalcularStatusPeriodo @PE_YEAR, @PE_TIPO, @PE_NUMERO
	drop table #TmpNominasAplicar
	return 
end 




go 



if exists (select * from dbo.sysobjects where id = object_id(N'TimbradoConcilia_GetDetalle') and xtype in (N'FN', N'IF', N'TF')) 
	drop function TimbradoConcilia_GetDetalle
go 

create function TimbradoConcilia_GetDetalle(  @RS_CODIGO Codigo, @PE_YEAR int,  @PE_TIPO status, @PE_NUMERO int ) 
returns Table 
as
return ( 
select CB_CODIGO, PRETTYNAME, CB_CURP, NO_TIM_DESC, NC_RES_DESC, NO_FACUUID, NC_UUID from VTIMCONCIL
where RS_CODIGO = @RS_CODIGO and PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO 	and NC_RESULT > 0 
)


go
