/****************************************/
/****************************************/
/******* Patch para DATOS  **************/
/****************************************/
/****************************************/

/* Deshabilitar Triggers */
 DISABLE TRIGGER ALL on DBO.EXPEDIEN;
 GO
 DISABLE TRIGGER ALL on DBO.COLABORA;
 GO

/* CR:2570, Tipo de Asegurado de la p�liza (0: Empleado 1:Pariente) 1/3 */
/* Patch 435 #Seq: 1*/
alter table EMP_POLIZA add EP_ASEGURA Status null;
GO

/* CR:2570, Tipo de Asegurado de la p�liza (0: Empleado 1:Pariente) 2/3 */
/* Patch 435 #Seq: 2*/
update EMP_POLIZA set EP_ASEGURA = EP_TIPO;
GO

/* CR:2570, Tipo de Asegurado de la p�liza (0: Empleado 1:Pariente) 3/3 */
/* Patch 435 #Seq: 3*/
alter table EMP_POLIZA alter column EP_ASEGURA Status not null;
GO

/* CR:2570, Orden de Poliza vigentes por empleado 1/3 */
/* Patch 435 #Seq: 4*/
alter table EMP_POLIZA add EP_ORDEN Status null;
GO

/* CR:2570, Orden de Poliza vigentes por empleado 2/3 */
/* Patch 435 #Seq: 5*/
update EMP_POLIZA set EP_ORDEN = 0 where EP_ORDEN is null;
GO

/* CR:2570, Orden de Poliza vigentes por empleado 3/3 */
/* Patch 435 #Seq: 6*/
alter table EMP_POLIZA alter column EP_ORDEN Status not null;
GO

/* CR:2570, Campo calculado de Costo de Empresa */
/* Patch 435 #Seq: 7*/
alter table EMP_POLIZA add EP_CEMPRES as ( ( EP_CFIJO + EP_CPOLIZA ) - ( EP_CTITULA ) );
GO

/* CR:2570, Costo Fijo por Vigencia de Poliza 1/3 */
/* Patch 435 #Seq: 1*/
alter table POL_VIGENC add PV_CFIJO Pesos null;
GO

/* CR:2570, Costo Fijo por Vigencia de Poliza 2/3 */
/* Patch 435 #Seq: 1*/
update POL_VIGENC set PV_CFIJO = 0 where PV_CFIJO is null;
GO

/* CR:2570, Orden de Poliza vigentes por empleado 3/3 */
/* Patch 435 #Seq: 1*/
alter table POL_VIGENC alter column PV_CFIJO Pesos not null;
GO

/* CR:2570, folio de Endoso al dar de Alta 1/3 */
/* Patch 435 #Seq: 1*/
alter table EMP_POLIZA add EP_END_ALT Mascara null;
GO

/* CR:2570, folio de Endoso al dar de Alta 2/3 */
/* Patch 435 #Seq: 1*/
update EMP_POLIZA set EP_END_ALT = '' where EP_END_ALT is null;
GO

/* CR:2570, folio de Endoso al dar de Alta 3/3 */
/* Patch 435 #Seq: 1*/
alter table EMP_POLIZA alter column EP_END_ALT Mascara not null;
GO

/* CR:2570, folio de Endoso al cancelar 1/3 */
/* Patch 435 #Seq: 1*/
alter table EMP_POLIZA add EP_END_CAN Mascara null;
GO

/* CR:2570, folio de Endoso al cancelar 2/3 */
/* Patch 435 #Seq: 1*/
update EMP_POLIZA set EP_END_CAN = '' where EP_END_CAN is null;
GO

/* CR:2570, folio de Endoso al cancelar 3/3 */
/* Patch 435 #Seq: 1*/
alter table EMP_POLIZA alter column EP_END_CAN Mascara not null;
GO

/* CR:2570, Actualizar Llave primaria EMP_POLIZA 1/2 */
/* Patch 435 #Seq: 1*/
IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'PK_EMP_POLIZA'))
alter table EMP_POLIZA drop PK_EMP_POLIZA;
GO

/* CR:2570, Actualizar Llave primaria EMP_POLIZA 2/2 */
/* Patch 435 #Seq: 1*/
alter table EMP_POLIZA add primary key (CB_CODIGO,PM_CODIGO,PV_REFEREN,EP_TIPO,PA_FOLIO,PA_RELACIO,EP_FEC_INI,EP_ASEGURA,EP_ORDEN);
GO

/* CR:2570, Orden de Polizas vigentes 1/5 */
/* Patch 435 #Seq: 1*/
create procedure SetOrdenEmpleadoPolizas @Empleado int ,@Poliza varchar(10),@Referencia varchar(10)
as
  declare @Llave int;
  declare @Orden int;
begin
	   set @Orden = 0;
	   declare PolizasTemp Cursor for
     select LLAVE from EMP_POLIZA EP where CB_CODIGO=@Empleado and PM_CODIGO = @Poliza and PV_REFEREN = @Referencia order by LLAVE
     open PolizasTemp
     Fetch next	from PolizasTemp
     into  @Llave
     WHILE @@FETCH_STATUS = 0
     begin
		  set @Orden = @Orden + 1;
		  update EMP_POLIZA set EP_ORDEN = @Orden where LLAVE = @Llave;
		  fetch NEXT from PolizasTemp
          into @Llave;
     end

     CLOSE PolizasTemp
     DEALLOCATE PolizasTemp
end
GO

/* CR:2570, Orden de Polizas vigentes 2/5 */
/* Patch 435 #Seq: 1*/
create procedure Set_Orden_Polizas
as
  declare @Empleado int;
  declare @Poliza varchar(10);
  declare @Referencia varchar(10);
begin
	   declare EmpleadosTemp Cursor for
     select CB_CODIGO,PM_CODIGO,PV_REFEREN from EMP_POLIZA EP group by CB_CODIGO,PM_CODIGO,PV_REFEREN
     open EmpleadosTemp
     Fetch next	from EmpleadosTemp
     into  @Empleado,@Poliza,@Referencia
     WHILE @@FETCH_STATUS = 0
     begin
		  execute SetOrdenEmpleadoPolizas @Empleado,@Poliza,@Referencia;
		  Fetch next from EmpleadosTemp
		  into  @Empleado,@Poliza,@Referencia;
     end

     CLOSE EmpleadosTemp
     DEALLOCATE EmpleadosTemp
end
GO

/* CR:2570, Orden de Polizas vigentes 3/5 */
/* Patch 435 #Seq: 1*/
execute Set_Orden_Polizas;
GO

/* CR:2570, Orden de Polizas vigentes 4/5 */
/* Patch 435 #Seq: 1*/
drop procedure SetOrdenEmpleadoPolizas;
GO

/* CR:2570, Orden de Polizas vigentes 5/5 */
/* Patch 435 #Seq: 1*/
drop procedure Set_Orden_Polizas;
GO

/* CR:2570, Crear tabla AMORTIZ_TB */
/* Patch 435 #Seq: 1*/
IF NOT exists (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='AMORTIZ_TB')
BEGIN
/*Tabla de Amortizaciones de SGM */
	create table AMORTIZ_TB
	(
		AT_CODIGO  Codigo,
		AT_DESCRIP DescLarga,
		AT_INGLES  Descripcion,
		AT_NUMERO  Numerico,
		AT_TEXTO   DescLarga,
		AT_ACTIVO  Booleano,
		LLAVE int identity(1,1)
	);

	/*Llave primaria*/
  alter table AMORTIZ_TB
	ADD PRIMARY KEY (AT_CODIGO);
END
GO

/* CR:2570, Crear tabla AMORTIZ_CS */
/* Patch 435 #Seq: 1*/
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_NAME='AMORTIZ_CS')
BEGIN
	/*Tabla de Amortizaciones de SGM */
	create table AMORTIZ_CS
	(
		AT_CODIGO  Codigo,
		AC_EDADINI Anio,
		AC_EDADFIN Anio,
		AC_CHOMBRE Pesos,
		AC_CMUJER  Pesos,
		LLAVE  int identity(1,1)
	);

	/*Llave primaria*/
	alter table AMORTIZ_CS
	ADD PRIMARY KEY (AT_CODIGO,AC_EDADINI,AC_EDADFIN);
END
GO

/* CR:2693, Fix - Al generar un permiso muestra todas las incidencias incluyendo BAJA */
/* Patch 435 #Seq: 1*/
update INCIDEN set TB_PERMISO = -1 where TB_INCIDEN <> 4
GO

/* CR:2696, Fix - Tiempo de acceso a Grupos de capacitaci�n es lento */
/* Patch 435 #Seq: 1*/
CREATE INDEX XIE2KARCURSO ON KARCURSO
(
       SE_FOLIO
)
GO

/* SOP-5229, Reagrupar objetivos de cursos de la STPS - Se condiciona con campo NOMINA.NO_DIAS_IH para evitar dejar mal los datos si se ejecuta el parche varias veces */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_DIAS_IH' and ID = object_id(N'NOMINA'))
begin
    update CURSO set CU_OBJETIV = 1 where CU_STPS = 'S' and CU_OBJETIV = 2       /* Nuevas tecnologias se mueve del tipo 2 al 1 */
    update CURSO set CU_OBJETIV = 2 where CU_STPS = 'S' and CU_OBJETIV = 4       /* Prevenir riesgos se mueve del tipo 4 al 2 */
    update CURSO set CU_OBJETIV = 4 where CU_STPS = 'S' and CU_OBJETIV = 3       /* Preparar ocupar vacantes se mueve temporalmente del tipo 3 al 4 */
    update CURSO set CU_OBJETIV = 3 where CU_STPS = 'S' and CU_OBJETIV = 5       /* Incrementar productividad se mueve del tipo 5 al 3  */
    update CURSO set CU_OBJETIV = 5 where CU_STPS = 'S' and CU_OBJETIV = 4       /* Preparar ocupar vacantes se mueve del tipo temporal 4 al 5  */
end
GO

/* CR:2669, Agrega campo NOMINA.NO_DIAS_IH */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_DIAS_IH' and ID = object_id(N'NOMINA'))
begin
	alter table NOMINA add NO_DIAS_IH Dias null
end
GO

/* CR:2669, Inicializa campo NOMINA.NO_DIAS_IH */
/* Patch 435 #Seq: X*/
update NOMINA set NO_DIAS_IH = 0 where NO_DIAS_IH is null
GO

/* CR:2669, Quitar propiedad null a campo NOMINA.NO_DIAS_IH */
/* Patch 435 #Seq: X*/
alter table NOMINA alter column NO_DIAS_IH Dias not null
GO

/* CR:2669, Agrega campo NOMINA.NO_DIAS_ID */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_DIAS_ID' and ID = object_id(N'NOMINA'))
begin
	alter table NOMINA add NO_DIAS_ID Dias null
end
GO

/* CR:2669, Inicializa campo NOMINA.NO_DIAS_ID */
/* Patch 435 #Seq: X*/
update NOMINA set NO_DIAS_ID = 0 where NO_DIAS_ID is null
GO

/* CR:2669, Quitar propiedad null a campo NOMINA.NO_DIAS_ID */
/* Patch 435 #Seq: X*/
alter table NOMINA alter column NO_DIAS_ID Dias not null
GO

/* CR:2669, Agrega campo NOMINA.NO_DIAS_IT */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_DIAS_IT' and ID = object_id(N'NOMINA'))
begin
	alter table NOMINA add NO_DIAS_IT Dias null
end
GO

/* CR:2669, Inicializa campo NOMINA.NO_DIAS_IT */
/* Patch 435 #Seq: X*/
update NOMINA set NO_DIAS_IT = 0 where NO_DIAS_IT is null
GO

/* CR:2669, Quitar propiedad null a campo NOMINA.NO_DIAS_IT */
/* Patch 435 #Seq: X*/
alter table NOMINA alter column NO_DIAS_IT Dias not null
GO

/* CR:2669, Agrega campo TMPNOM.NO_DIAS_IH */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_DIAS_IH' and ID = object_id(N'TMPNOM'))
begin
	alter table TMPNOM add NO_DIAS_IH PesosEmpresa null
end
GO

/* CR:2669, Inicializa campo TMPNOM.NO_DIAS_IH */
/* Patch 435 #Seq: X*/
update TMPNOM set NO_DIAS_IH = 0 where NO_DIAS_IH is null
GO

/* CR:2669, Quitar propiedad null a campo TMPNOM.NO_DIAS_IH */
/* Patch 435 #Seq: X*/
alter table TMPNOM alter column NO_DIAS_IH PesosEmpresa not null
GO

/* CR:2669, Agrega campo TMPNOM.NO_DIAS_ID */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_DIAS_ID' and ID = object_id(N'TMPNOM'))
begin
	alter table TMPNOM add NO_DIAS_ID PesosEmpresa null
end
GO

/* CR:2669, Inicializa campo TMPNOM.NO_DIAS_ID */
/* Patch 435 #Seq: X*/
update TMPNOM set NO_DIAS_ID = 0 where NO_DIAS_ID is null
GO

/* CR:2669, Quitar propiedad null a campo TMPNOM.NO_DIAS_ID */
/* Patch 435 #Seq: X*/
alter table TMPNOM alter column NO_DIAS_ID PesosEmpresa not null
GO

/* CR:2669, Agrega campo TMPNOM.NO_DIAS_IT */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'NO_DIAS_IT' and ID = object_id(N'TMPNOM'))
begin
	alter table TMPNOM add NO_DIAS_IT PesosEmpresa null
end
GO

/* CR:2669, Inicializa campo TMPNOM.NO_DIAS_IT */
/* Patch 435 #Seq: X*/
update TMPNOM set NO_DIAS_IT = 0 where NO_DIAS_IT is null
GO

/* CR:2669, Quitar propiedad null a campo TMPNOM.NO_DIAS_IT */
/* Patch 435 #Seq: X*/
alter table TMPNOM alter column NO_DIAS_IT PesosEmpresa not null
GO

/* CR:2669, Agrega campo INCAPACI.IN_FEC_RH */
/* Patch 435 #Seq: X*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'IN_FEC_RH' and ID = object_id(N'INCAPACI'))
begin
	alter table INCAPACI add IN_FEC_RH Fecha null
end
GO

/* CR:2669, Inicializa campo INCAPACI.IN_FEC_RH */
/* Patch 435 #Seq: X*/
update INCAPACI set IN_FEC_RH = IN_FEC_INI where IN_FEC_RH is null
GO

/* CR:2669, Quitar propiedad null a campo INCAPACI.IN_FEC_RH */
/* Patch 435 #Seq: X*/
alter table INCAPACI alter column IN_FEC_RH Fecha not null
GO

/* CR:2669, 2704, Agregar par�metros de Dias de Incapacidad general y Mejoras de N�mina (1/3)*/
/* Patch 435 #Seq: X*/
CREATE PROCEDURE AGREGA_PARAM AS
BEGIN
    SET NOCOUNT ON;
    declare @Cuantos Integer;
    declare @Maximo int
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_IH' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_IH', 'NO_DIAS_IH', 2, 'Subsidio de Inc. en d�a h�bil', 'S' )
    end
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_ID' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_ID', 'NO_DIAS_ID', 2, 'Subsidio de Inc. en descansos', 'S' )
    end
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_IT' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_IT', 'NO_DIAS_IT', 2, 'D�as sin subsidio Inc. General', 'S' )
    end
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'FACT_BASE' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'FACT_BASE', 'NO_FACT_BA', 1, 'Factor diario de d�as h�biles', 'S' )
    end
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_DESCT' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_DESCT', 'NO_DIAS_DT', 2, 'D�as de descanso trabajados', 'S' )
    end
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_FEST' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_FEST', 'NO_DIAS_FS', 2, 'D�as Festivos del periodo', 'S' )
    end
    select @Cuantos = COUNT(*) from NOMPARAM where ( NP_NOMBRE = 'DIAS_FESTT' );
    if ( @Cuantos = 0 )
    begin
        select @Maximo = MAX(NP_FOLIO) from NOMPARAM
        insert into NOMPARAM (NP_FOLIO, NP_NOMBRE, NP_FORMULA, NP_TIPO, NP_DESCRIP, NP_ACTIVO )
        values (@Maximo+1, 'DIAS_FESTT', 'NO_DIAS_FT', 2, 'D�as de Festivo Trabajado', 'S' )
    end
END
GO

/* CR:2669, 2704, Agregar par�metros de Dias de Incapacidad general y Mejoras de N�mina (2/3)*/
/* Patch 435 #Seq: X*/
EXECUTE AGREGA_PARAM
GO

/* CR:2669, 2704, Agregar par�metros de Dias de Incapacidad general y Mejoras de N�mina (3/3)*/
/* Patch 435 #Seq: X*/
DROP PROCEDURE AGREGA_PARAM
GO

/* CR:2669, 2704 Agregar Conceptos de Acumulados D�as/Horas Incapacidad general y Mejoras de N�mina (1/3)*/
/* Patch 435 #Seq: X*/
create procedure AGREGA_CONCEPTOS_SISTEMA
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1124 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1124, 'Subsidio de Inc. en d�a h�bil', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1125 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1125, 'Subsidio de Inc. en descansos', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1126 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1126, 'D�as sin subsidio Inc. General', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1013 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1013, 'Total config. de Pre-n�mina #1', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1014 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1014, 'Total config. de Pre-n�mina #2', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1015 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1015, 'Total config. de Pre-n�mina #3', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1016 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1016, 'Total config. de Pre-n�mina #4', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1017 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1017, 'Total config. de Pre-n�mina #5', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1127 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1127, 'D�as de descanso trabajados', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1128 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1128, 'D�as Festivos del periodo', 'S' );
     end
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = 1129 );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( 1129, 'D�as de Festivo Trabajado', 'S' );
     end
end
GO

/* CR:2669, 2704 Agregar Conceptos de Acumulados D�as/Horas Incapacidad general y Mejoras de N�mina (2/3)*/
/* Patch 435 #Seq: X*/
execute AGREGA_CONCEPTOS_SISTEMA
GO

/* CR:2669, 2704 Agregar Conceptos de Acumulados D�as/Horas Incapacidad general y Mejoras de N�mina (3/3)*/
/* Patch 435 #Seq: X*/
drop procedure AGREGA_CONCEPTOS_SISTEMA;
GO

/* CR:2621, Jornada Diaria de acuerdo al horario ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: X*/
create function SP_JORNADA_DIARIA (
  @Horario Codigo
) returns Horas
AS
begin
	 return ( select HO_JORNADA from HORARIO where ( HO_CODIGO = @Horario ) )
end
GO

/* CR:2621, Tabla AUSENCIA: Jornada Diaria de acuerdo al horario ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: X*/
alter table AUSENCIA add AU_JORNADA as dbo.SP_JORNADA_DIARIA(HO_CODIGO)
GO

/* CR:2621, Tabla AUSENCIA: Estatus del empleado ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: X*/
alter table AUSENCIA add AU_STATEMP as dbo.SP_STATUS_EMP(AU_FECHA,CB_CODIGO)
GO

/* CR:2621, Tabla AUSENCIA: Tabla de Prestaciones del empleado ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) (1/2) */
/* Patch 435 #Seq: 1*/
alter table AUSENCIA add CB_TABLASS Codigo1 null;
GO

/* CR:2621, Tabla AUSENCIA: Inicializar Tabla de Prestaciones del empleado ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: 1*/
update AUSENCIA set CB_TABLASS = ( select DBO.SP_KARDEX_CB_TABLASS( AUSENCIA.AU_FECHA, AUSENCIA.CB_CODIGO ) )
where ( Year( AU_FECHA ) = Year( GetDate() ) ) and ( CB_TABLASS is null )
GO

/* CR:2621, Tabla AUSENCIA: Inicializar tabla de prestaciones del empleado con valor vacio -a�os anteriores- ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: 1*/
update AUSENCIA set CB_TABLASS = ''
where ( CB_TABLASS is null )
GO

/* CR:2621, Tabla AUSENCIA: Tabla de Prestaciones del empleado ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) (2/2) */
/* Patch 435 #Seq: 1*/
alter table AUSENCIA alter column CB_TABLASS Codigo1 not null;
GO

/* CR:2621, Tabla GLOBAL: Reemplazar salario de Clasificaciones temporales ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: 1*/
if not exists ( select 1 from GLOBAL where GL_CODIGO = 304 )
	insert into GLOBAL (GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO, US_CODIGO, GL_CAPTURA,GL_NIVEL)
	values (304,'Tarjetas con Salario Temporal','N',1,0,GETDATE(),0);
GO

/* CR:2669: Se agrega GLOBAL 306 - Incidencia de Incapacidad General*/
/* Patch 435 #Seq: X*/
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 306 )
   insert into GLOBAL ( GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO, US_CODIGO, GL_CAPTURA, GL_NIVEL )
   values ( 306, 'Incidencia Incapacidad General', 'INC', 4, 0, GETDATE(), 0 )
GO

/* CR:2704: Se agregan campos de formulas a tabla TPERIODO */
/* Patch 435 #Seq: X */
alter table TPERIODO add TP_TOT_EV1 Formula null
GO
alter table TPERIODO add TP_TOT_EV2 Formula null
GO
alter table TPERIODO add TP_TOT_EV3 Formula null
GO
alter table TPERIODO add TP_TOT_EV4 Formula null
GO
alter table TPERIODO add TP_TOT_EV5 Formula null
GO

/* CR:2704: Se inicializan campos de formulas de tabla TPERIODO */
/* Patch 435 #Seq: X */
update TPERIODO
set
	TP_TOT_EV1 = '',
	TP_TOT_EV2 = '',
	TP_TOT_EV3 = '',
	TP_TOT_EV4 = '',
	TP_TOT_EV5 = ''
where TP_TOT_EV1 is null
GO

/* CR:2704: Quitar propiedad null a campos de formulas de tabla TPERIODO */
/* Patch 435 #Seq: X */
alter table TPERIODO alter column TP_TOT_EV1 Formula not null
GO 
alter table TPERIODO alter column TP_TOT_EV2 Formula not null
GO 
alter table TPERIODO alter column TP_TOT_EV3 Formula not null
GO 
alter table TPERIODO alter column TP_TOT_EV4 Formula not null
GO 
alter table TPERIODO alter column TP_TOT_EV5 Formula not null
GO

/* CR:2704: Se agrega campo CONCEPTO.CO_FRM_ALT */
/* Patch 435 #Seq: X */
alter table CONCEPTO add CO_FRM_ALT Formula null
GO

/* CR:2704: Se inicializa campo CONCEPTO.CO_FRM_ALT */
/* Patch 435 #Seq: X */
update CONCEPTO set CO_FRM_ALT = '' where CO_FRM_ALT is null
GO

/* CR:2704, Quitar propiedad null a campo CONCEPTO.CO_FRM_ALT */
/* Patch 435 #Seq: X*/
alter table CONCEPTO alter column CO_FRM_ALT Formula not null
GO

/* CR:2704: Se agrega campo CONCEPTO.CO_USO_NOM */
/* Patch 435 #Seq: X */
alter table CONCEPTO add CO_USO_NOM Status null
GO

/* CR:2704: Se inicializa campo CONCEPTO.CO_USO_NOM */
/* Patch 435 #Seq: X */
update CONCEPTO set CO_USO_NOM = 0 where CO_USO_NOM is null
GO

/* CR:2704, Quitar propiedad null a campo CONCEPTO.CO_USO_NOM */
/* Patch 435 #Seq: X*/
alter table CONCEPTO alter column CO_USO_NOM Status not null
GO

/* CR:2704: Se agregan campos de totales de formulas a tabla NOMINA */
/* Patch 435 #Seq: X */
alter table NOMINA add  NO_TOTAL_1	Pesos null
GO
alter table NOMINA add NO_TOTAL_2	Pesos null
GO
alter table NOMINA add NO_TOTAL_3	Pesos null
GO
alter table NOMINA add NO_TOTAL_4	Pesos null
GO
alter table NOMINA add NO_TOTAL_5	Pesos null
GO

/* CR:2704: Se inicializan campos de totales de formulas de tabla NOMINA */
/* Patch 435 #Seq: X */
update NOMINA set
NO_TOTAL_1 = 0, 
NO_TOTAL_2 = 0, 
NO_TOTAL_3 = 0, 
NO_TOTAL_4 = 0, 
NO_TOTAL_5 = 0
where NO_TOTAL_1 is null
GO

/* CR:2704: Quitar propiedad null a campos de totales de formulas de tabla NOMINA */
/* Patch 435 #Seq: X */
alter table NOMINA alter column NO_TOTAL_1	Pesos not null
GO
alter table NOMINA alter column NO_TOTAL_2	Pesos not null
GO
alter table NOMINA alter column NO_TOTAL_3	Pesos not null
GO
alter table NOMINA alter column NO_TOTAL_4	Pesos not null
GO
alter table NOMINA alter column NO_TOTAL_5	Pesos not null
GO

/* CR:2704: Se agregan campos de totales de d�as a tabla NOMINA */
/* Patch 435 #Seq: X */
alter table NOMINA add NO_FACT_BA	Tasa null
GO
alter table NOMINA add NO_DIAS_DT	Dias null
GO
alter table NOMINA add NO_DIAS_FT	Dias null
GO
alter table NOMINA add NO_DIAS_FS	Dias null
GO

/* CR:2704: Se inicializan campos de totales de d�as de tabla NOMINA */
/* Patch 435 #Seq: X */
update NOMINA set 
NO_FACT_BA = 0, 
NO_DIAS_DT = 0, 
NO_DIAS_FT = 0, 
NO_DIAS_FS = 0
where NO_FACT_BA is null
GO

/* CR:2704: Quitar propiedad null a campos de totales de d�as de tabla NOMINA */
/* Patch 435 #Seq: X */
alter table NOMINA alter column NO_FACT_BA	Tasa not null
GO
alter table NOMINA alter column NO_DIAS_DT	Dias not null
GO
alter table NOMINA alter column NO_DIAS_FT	Dias not null
GO
alter table NOMINA alter column NO_DIAS_FS	Dias not null
GO

/* CR:2704: Crear indice para eficiencia de busqueda en campo AUSENCIA.AU_TIPO */
/* Patch 435 #Seq: X */
CREATE INDEX XIE1AUSENCIA_AU_TIPO ON AUSENCIA 
(
	AU_FECHA ASC,
	CB_CODIGO ASC, 
	AU_TIPO 
)
GO

/* CR:2704: Crear indice para eficiencia de busqueda en campo AUSENCIA.AU_TIPODIA */
/* Patch 435 #Seq: X */
CREATE INDEX XIE1AUSENCIA_AU_TIPODIA ON AUSENCIA
(
	AU_FECHA ASC,
	CB_CODIGO ASC, 
	AU_TIPODIA
)
GO

/* CR:2704: Crear indice para eficiencia de busqueda en campo AUSENCIA.AU_STATUS */
/* Patch 435 #Seq: X */
CREATE INDEX XIE1AUSENCIA_AU_STATUS ON AUSENCIA 
(
	AU_FECHA ASC,
	CB_CODIGO ASC, 
	AU_STATUS
)
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_TSANGRE */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_TSANGRE' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_TSANGRE Descripcion NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_ALERGIA */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_ALERGIA' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_ALERGIA Formula NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_BRG_ACT */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_BRG_ACT' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_BRG_ACT Booleano NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_BRG_TIP */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_BRG_TIP' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_BRG_TIP Status NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_BRG_ROL */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_BRG_ROL' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_BRG_ROL Booleano NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_BRG_JEF */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_BRG_JEF' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_BRG_JEF Booleano NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_BRG_CON */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_BRG_CON' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_BRG_CON Booleano NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_BRG_PRA */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_BRG_PRA' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_BRG_PRA Booleano NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo COLABORA.CB_BRG_NOP */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'CB_BRG_NOP' and ID = object_id(N'COLABORA'))
begin
	ALTER TABLE COLABORA ADD CB_BRG_NOP Descripcion NULL
end
GO

/* CR:2691, Protecci�n Civil - Inicializar campos de COLABORA */
/* Patch 435 #Seq: 1*/
UPDATE COLABORA
SET
CB_TSANGRE = '', CB_ALERGIA = '', CB_BRG_ACT = 'N', CB_BRG_TIP = 0, CB_BRG_ROL = 'N',
CB_BRG_JEF = 'N', CB_BRG_CON = 'N', CB_BRG_PRA = 'N', CB_BRG_NOP = ''
where CB_TSANGRE is NULL

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_TSANGRE */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_TSANGRE Descripcion NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_ALERGIA */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_ALERGIA Formula NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_BRG_ACT */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_BRG_ACT Booleano NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_BRG_TIP */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_BRG_TIP Status NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_BRG_ROL */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_BRG_ROL Booleano NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_BRG_JEF */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_BRG_JEF Booleano NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_BRG_CON */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_BRG_CON Booleano NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_BRG_PRA */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_BRG_PRA Booleano NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo COLABORA.CB_BRG_NOP */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLABORA ALTER COLUMN CB_BRG_NOP Descripcion NOT NULL
GO

/* CR:2691, Protecci�n Civil - Agrega campo EXPEDIEN.EX_TSANGRE */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'EX_TSANGRE' and ID = object_id(N'EXPEDIEN'))
begin
	ALTER TABLE EXPEDIEN ADD EX_TSANGRE Descripcion NULL
end
GO

/* CR:2691, Protecci�n Civil - Agrega campo EXPEDIEN.EX_ALERGIA */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS
where NAME = 'EX_ALERGIA' and ID = object_id(N'EXPEDIEN'))
begin
	ALTER TABLE EXPEDIEN ADD EX_ALERGIA Formula NULL
end
GO

/* CR:2691, Protecci�n Civil - Inicializar campos de EXPEDIEN */
/* Patch 435 #Seq: 1*/
UPDATE EXPEDIEN SET EX_TSANGRE = '', EX_ALERGIA = '' where EX_TSANGRE is NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo EXPEDIEN.EX_TSANGRE */
/* Patch 435 #Seq: 1*/
ALTER TABLE EXPEDIEN ALTER COLUMN EX_TSANGRE Descripcion NOT NULL
GO

/* CR:2691, Protecci�n Civil - Propiedad Not null a campo EXPEDIEN.EX_ALERGIA */
/* Patch 435 #Seq: 1*/
ALTER TABLE EXPEDIEN ALTER COLUMN EX_ALERGIA Formula NOT NULL
GO

/* CR:2653, Mejora Confidencialidad - Cambiar dominio PUESTO.PU_NIVEL0 1/4 */
/* Patch 435 #Seq: 1*/
EXECUTE sp_unbindefault N'PUESTO.PU_NIVEL0'
GO

/* CR:2653, Mejora Confidencialidad - Cambiar dominio PUESTO.PU_NIVEL0 2/4 */
/* Patch 435 #Seq: 1*/
ALTER TABLE PUESTO ALTER COLUMN PU_NIVEL0 Formula
GO

/* CR:2653, Mejora Confidencialidad - Cambiar dominio PUESTO.PU_NIVEL0 3/4 */
/* Patch 435 #Seq: 1*/
EXECUTE sp_bindefault 'dbo.StringVacio',  N'PUESTO.PU_NIVEL0'
GO

/* CR:2653, Mejora Confidencialidad - Cambiar dominio PUESTO.PU_NIVEL0 4/4 */
/* Patch 435 #Seq: 1*/
update PUESTO set PU_NIVEL0 = PU_NIVEL0

/* CR:2653, Mejora Confidencialidad - Agrega campo TURNO.TU_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE TURNO ADD TU_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo TURNO.TU_NIVEL0 */
/* Patch 435 #Seq: 1*/
update TURNO set TU_NIVEL0=''  where TU_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo TURNO.TU_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table TURNO alter column TU_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo HORARIO.HO_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE HORARIO ADD HO_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo HORARIO.HO_NIVEL0 */
/* Patch 435 #Seq: 1*/
update HORARIO set HO_NIVEL0=''  where HO_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo HORARIO.HO_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table HORARIO alter column HO_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo CLASIFI.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE CLASIFI ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo CLASIFI.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update CLASIFI set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo CLASIFI.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table CLASIFI alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo SSOCIAL.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE SSOCIAL ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo SSOCIAL.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update SSOCIAL set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo SSOCIAL.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table SSOCIAL alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo CONTRATO.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE CONTRATO ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo CONTRATO.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update CONTRATO set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo CONTRATO.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table CONTRATO alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL1.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL1 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL1.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL1 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL1.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL1 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL2.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL2 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL2.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL2 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL2.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL2 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL3.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL3 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL3.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL3 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL3.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL3 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL4.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL4 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL4.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL4 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL4.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL4 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL5.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL5 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL5.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL5 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL5.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL5 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL6.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL6 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL6.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL6 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL6.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL6 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL7.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL7 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL7.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL7 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL7.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL7 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL8.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL8 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL8.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL8 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL8.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL8 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo NIVEL9.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE NIVEL9 ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo NIVEL9.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update NIVEL9 set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo NIVEL9.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table NIVEL9 alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo RPATRON.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE RPATRON ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo RPATRON.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update RPATRON set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo RPATRON.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table RPATRON alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo COLONIA.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE COLONIA ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo COLONIA.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update COLONIA set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo COLONIA.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table COLONIA alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo ENTIDAD.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE ENTIDAD ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo ENTIDAD.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update ENTIDAD set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo ENTIDAD.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table ENTIDAD alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo TAHORRO.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE TAHORRO  ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo TAHORRO.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update TAHORRO  set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo TAHORRO.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table TAHORRO alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo TPRESTA.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE TPRESTA ADD TB_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo TPRESTA.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
update TPRESTA set TB_NIVEL0=''  where TB_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo TPRESTA.TB_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table TPRESTA alter column TB_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Agrega campo TPERIODO.TP_NIVEL0 */
/* Patch 435 #Seq: 1*/
ALTER TABLE TPERIODO ADD TP_NIVEL0 Formula null
GO

/* CR:2653, Mejora Confidencialidad - Inicializa campo TPERIODO.TP_NIVEL0 */
/* Patch 435 #Seq: 1*/
update TPERIODO set TP_NIVEL0=''  where TP_NIVEL0 is NULL
GO

/* CR:2653, Mejora Confidencialidad - Not null campo TPERIODO.TP_NIVEL0 */
/* Patch 435 #Seq: 1*/
alter table TPERIODO alter column TP_NIVEL0 Formula not null;
GO

/* CR:2653, Mejora Confidencialidad - Crear tabla CONF_TAB */
/* Patch 435 #Seq: 1*/
create table  CONF_TAB  (
	CT_TABLA FolioChico,
	CT_CODIGO Codigo,
	CT_NIVEL0 Codigo
 )
GO
/* CR:2653, Mejora Confidencialidad - Borrar Relacion  */
DELETE FROM R_RELACION WHERE EN_CODIGO = 59 AND RL_ENTIDAD = 157 AND RL_CAMPOS = 'PU_NIVEL0';
GO
/* CR:2653, Mejora Confidencialidad - Crear indice IDX_CONF_TAB */
/* Patch 435 #Seq: 1*/
create index IDX_CONF_TAB on CONF_TAB( CT_TABLA )
GO

/* CR:2653, Mejora Confidencialidad - Crear indice IDX_CONF_TAB_NIVEL0 */
/* Patch 435 #Seq: 1*/
create index IDX_CONF_TAB_NIVEL0 on CONF_TAB( CT_NIVEL0 )
GO

/* CR:2675, Cafeter�a con Biometrico - INVITA.IV_ID_GPO */
/* Patch 435 #Seq: 1*/
if not exists( select NAME from SYSCOLUMNS where NAME = 'IV_ID_GPO' and ID = object_id(N'INVITA'))
begin
	alter table INVITA add IV_ID_GPO CODIGO null;
end
GO

/* CR:2675, Cafeter�a con Biometrico - INVITA.IV_ID_GPO */
/* Patch 435 #Seq: 1*/
update INVITA set IV_ID_GPO = '';
GO

/* CR:2675, Cafeter�a con Biometrico - INVITA.IV_ID_GPO */
/* Patch 435 #Seq: 1*/
alter table INVITA alter column IV_ID_GPO CODIGO not null;
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Clasificacion de Competencias 1/2*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'T_COMPETEN')
CREATE TABLE T_COMPETEN(
	TB_CODIGO Codigo,
	TB_ELEMENT Observaciones,
	TB_INGLES Observaciones,
	TB_NUMERO Pesos,
	TB_TEXTO Descripcion,
	LLAVE FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Clasificacion de Competencias 2/2*/
/* Patch 435 #Seq: 1*/
ALTER TABLE T_COMPETEN
       ADD CONSTRAINT PK_T_COMPETEN PRIMARY KEY (TB_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo Nacional de Competencias 1/2*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_T_NACOMP')
CREATE TABLE C_T_NACOMP(
	TN_CODIGO CODIGO,
	TN_TITULO Titulo,
	TN_COMITE Titulo,
	TN_PROPOSI Memo,
	TN_FEC_APR Fecha,
	TN_NIVEL Codigo,
	TN_ACT_NIV Memo,
	TN_PERFIL Memo,
	TN_CR_EVA Memo,
	TN_AC_COMP Memo,
	Llave FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo Nacional de Competencias 2/2*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_T_NACOMP
       ADD CONSTRAINT PK_C_T_NACOMP PRIMARY KEY (TN_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo de Competencias 1/3*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_COMPETEN')
Create Table C_COMPETEN(
	CC_CODIGO Codigo,
	CC_ELEMENT Observaciones,
	CC_INGLES Observaciones,
	CC_NUMERO Pesos,
	CC_TEXTO Descripcion,
	CC_DETALLE Memo,
	TB_CODIGO Codigo,
	CC_ACTIVO Booleano,
	TN_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
);
/*Llave Primaria CC_CODIGO*/

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo de Competencias 2/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_COMPETEN
    ADD CONSTRAINT PK_C_COMPETEN PRIMARY KEY (CC_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo de Competencias 3/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_COMPETEN WITH CHECK ADD  CONSTRAINT FK_C_COMPETEN_T_COMPETEN
FOREIGN KEY(TB_CODIGO)
REFERENCES T_COMPETEN (TB_CODIGO)
ON UPDATE CASCADE
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Niveles de Competencia 1/3*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_NIV_COMP')
Create Table C_NIV_COMP(
	NC_NIVEL FolioChico,
	NC_DESCRIP Descripcion,
	CC_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Niveles de Competencia 2/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_NIV_COMP
    ADD CONSTRAINT PK_C_NIV_COMP PRIMARY KEY (NC_NIVEL,CC_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Niveles de Competencia 3/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_NIV_COMP  WITH CHECK ADD  CONSTRAINT FK_C_NIV_COMP_C_COMPETEN
FOREIGN KEY(CC_CODIGO)
REFERENCES C_COMPETEN (CC_CODIGO)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Revisiones de Competencia 1/3*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_REV_COMP')
Create Table C_REV_COMP(
	RC_REVISIO NombreCampo,
	RC_OBSERVA Descripcion,
	RC_FEC_INI Fecha,
	RC_FEC_FIN Fecha,
	US_CODIGO Usuario,
	CC_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Revisiones de Competencia 2/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_REV_COMP
    ADD CONSTRAINT PK_C_REV_COMP PRIMARY KEY (RC_FEC_INI,CC_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Revisiones de Competencia 3/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_REV_COMP  WITH CHECK ADD  CONSTRAINT FK_C_REV_COMP_C_COMPETEN
FOREIGN KEY(CC_CODIGO)
REFERENCES C_COMPETEN (CC_CODIGO)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Cursos de Competencia 1/4*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_COMP_CUR')
Create Table C_COMP_CUR(
	CC_ORDEN FolioChico,
	CC_CODIGO Codigo,
	CU_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Cursos de Competencia 2/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_COMP_CUR
    ADD CONSTRAINT PK_C_COMP_CUR PRIMARY KEY (CC_CODIGO,CU_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Cursos de Competencia 3/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_COMP_CUR  WITH CHECK ADD  CONSTRAINT FK_C_COMP_CUR_C_COMPETEN
FOREIGN KEY(CC_CODIGO)
REFERENCES C_COMPETEN (CC_CODIGO)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Cursos de Competencia 4/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_COMP_CUR  WITH CHECK ADD  CONSTRAINT FK_C_COMP_CUR_CURSO
FOREIGN KEY(CU_CODIGO)
REFERENCES CURSO (CU_CODIGO)
ON UPDATE CASCADE
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Cursos de Competencia 1/2*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'T_PERFIL')
Create TABLE T_PERFIL
(
	TB_CODIGO Codigo,
	TB_ELEMENT Descripcion,
	TB_INGLES Descripcion,
	TB_NUMERO Numerico,
	TB_TEXTO Descripcion,
	LLAVE FolioGrande identity(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Cursos de Competencia 2/2*/
/* Patch 435 #Seq: 1*/
ALTER TABLE T_PERFIL
       ADD CONSTRAINT PK_T_PERFIL PRIMARY KEY (TB_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo de PERFIL 1/4*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_PERFIL')
Create TABLE C_PERFIL
(
	CP_CODIGO Codigo,
	CP_ELEMENT Observaciones,
	CP_NUMERO Numerico,
	CP_TEXTO Observaciones,
	CP_INGLES Observaciones,
	CP_DETALLE Memo,
	TB_CODIGO Codigo,
	CP_ACTIVO Booleano,
	LLAVE FolioGrande identity(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo de PERFIL 2/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PERFIL
       ADD CONSTRAINT PK_C_PERFIL PRIMARY KEY (CP_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Catalogo de PERFIL 3/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PERFIL  WITH CHECK ADD  CONSTRAINT FK_C_PERFIL_T_PERFIL
FOREIGN KEY(TB_CODIGO)
REFERENCES T_PERFIL (TB_CODIGO)
ON UPDATE CASCADE
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Revisiones del PERFIL 1/3*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_REV_PERF')
Create Table C_REV_PERF(
	RP_REVISIO NombreCampo,
	RP_OBSERVA Descripcion,
	RP_FEC_INI Fecha,
	RP_FEC_FIN Fecha,
	US_CODIGO Usuario,
	CP_CODIGO Codigo,
	LLAVE FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Revisiones del PERFIL 2/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_REV_PERF
    ADD CONSTRAINT PK_C_REV_PERF PRIMARY KEY (RP_FEC_INI,CP_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Revisiones del PERFIL 3/3*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_REV_PERF  WITH CHECK ADD  CONSTRAINT FK_C_REV_PERF_C_PERFIL
FOREIGN KEY(CP_CODIGO)
REFERENCES C_PERFIL (CP_CODIGO)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - MATRIZ de Funciones 1/4*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_PER_COMP')
CREATE TABLE C_PER_COMP(
	CP_CODIGO Codigo,
	RP_FEC_INI Fecha,
	CC_CODIGO Codigo,
	NC_NIVEL  FolioChico,
	PC_PESO Numerico,
	Llave FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - MATRIZ de Funciones 2/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PER_COMP
    ADD CONSTRAINT PK_C_PER_COMP PRIMARY KEY (CP_CODIGO,CC_CODIGO,RP_FEC_INI)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - MATRIZ de Funciones 3/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PER_COMP WITH CHECK ADD  CONSTRAINT FK_C_PER_COMP_C_REV_PERF
FOREIGN KEY(RP_FEC_INI,CP_CODIGO)
REFERENCES C_REV_PERF (RP_FEC_INI,CP_CODIGO)
ON UPDATE NO ACTION
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - MATRIZ de Funciones 4/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PER_COMP WITH CHECK ADD  CONSTRAINT FK_C_PER_COMP_C_NIV_COMP
FOREIGN KEY(NC_NIVEL,CC_CODIGO)
REFERENCES C_NIV_COMP (NC_NIVEL,CC_CODIGO)
ON UPDATE NO ACTION
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Grupos de Competencias - Puestos  1/4*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_PERF_PTO')
CREATE TABLE C_PERF_PTO(
	CP_CODIGO Codigo,
	PU_CODIGO Codigo,
	CP_FEC_INI Fecha,
	PP_ACTIVO Booleano,
  PP_PESO Numerico,
	US_CODIGO Usuario,
	PP_FEC_REG Fecha,
	Llave FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Grupos de Competencias - Puestos  2/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PERF_PTO
    ADD CONSTRAINT PK_C_PERF_PTO PRIMARY KEY (CP_CODIGO,PU_CODIGO)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Grupos de Competencias - Puestos  3/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PERF_PTO WITH CHECK ADD  CONSTRAINT FK_C_PERF_PTO_C_PERFIL
FOREIGN KEY(CP_CODIGO)
REFERENCES C_PERFIL (CP_CODIGO)
ON UPDATE CASCADE
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Grupos de Competencias - Puestos  4/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_PERF_PTO WITH CHECK ADD  CONSTRAINT FK_C_PERF_PTO_PUESTO
FOREIGN KEY(PU_CODIGO)
REFERENCES PUESTO (PU_CODIGO)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Competencias del Empleado 1/4*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_EMP_COMP')
CREATE TABLE C_EMP_COMP(
	CB_CODIGO NumeroEmpleado,
	CC_CODIGO Codigo,
	RC_FEC_INI Fecha,
	NC_NIVEL FolioChico,
	ECC_FECHA Fecha,
  ECC_COMENT Formula,
	US_CODIGO Usuario,
	Llave FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Competencias del Empleado 2/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_EMP_COMP
    ADD CONSTRAINT PK_C_EMP_COMP PRIMARY KEY (CB_CODIGO,CC_CODIGO,RC_FEC_INI)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Competencias del Empleado 3/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_EMP_COMP  ADD CONSTRAINT FK_C_EMP_COMP_C_REV_COMP
FOREIGN KEY(RC_FEC_INI,CC_CODIGO)
REFERENCES C_REV_COMP (RC_FEC_INI,CC_CODIGO)
ON UPDATE NO ACTION
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Competencias del Empleado 4/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_EMP_COMP WITH CHECK ADD  CONSTRAINT FK_C_EMP_COMP_COLABORA
FOREIGN KEY(CB_CODIGO)
REFERENCES COLABORA (CB_CODIGO)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Evaluaciones de Competencias del Empleado 1/4*/
/* Patch 435 #Seq: 1*/
IF not EXISTS(SELECT * FROM sys.tables WHERE name = 'C_EVA_COMP')
CREATE TABLE C_EVA_COMP(
	CB_CODIGO NumeroEmpleado,
	CC_CODIGO Codigo,
	NC_NIVEL FolioChico,
	RC_FEC_INI Fecha,
	EC_FECHA Fecha,
	EC_CALIFIC Numerico,
	US_CODIGO Usuario,
	EC_COMENT Formula,
	Llave FolioGrande IDENTITY(1,1)
);
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Evaluaciones de Competencias del Empleado 2/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_EVA_COMP
    ADD CONSTRAINT PK_C_EVA_COMP PRIMARY KEY (CB_CODIGO,CC_CODIGO,NC_NIVEL,RC_FEC_INI)
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Evaluaciones de Competencias del Empleado 3/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_EVA_COMP WITH CHECK ADD  CONSTRAINT FK_C_EVA_COMP_C_REV_COMP
FOREIGN KEY(RC_FEC_INI,CC_CODIGO)
REFERENCES C_REV_COMP (RC_FEC_INI,CC_CODIGO)
ON UPDATE NO ACTION
ON DELETE NO ACTION
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Historial de Evaluaciones de Competencias del Empleado 4/4*/
/* Patch 435 #Seq: 1*/
ALTER TABLE C_EVA_COMP WITH CHECK ADD  CONSTRAINT FK_C_EVA_COMP_COLABORA
FOREIGN KEY(CB_CODIGO)
REFERENCES COLABORA (CB_CODIGO)
ON UPDATE CASCADE
ON DELETE CASCADE
GO

/* Agrega valores sugeridos de contratos e incidencias (1/3)*/
/* Patch 435 #Seq: X*/
CREATE PROCEDURE AGREGA_SUGERIDOS AS
BEGIN
    SET NOCOUNT ON;
    declare @Cuantos Integer;
    select @Cuantos = COUNT(*) from CONTRATO where ( TB_CODIGO = 'P' );
    if ( @Cuantos = 0 )
    begin
        insert into CONTRATO ( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_DIAS, TB_ACTIVO )
        values ( 'P', 'Prueba', 'Testing', 30, 'S' )
    end
    select @Cuantos = COUNT(*) from CONTRATO where ( TB_CODIGO = 'C' );
    if ( @Cuantos = 0 )
    begin
        insert into CONTRATO ( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_DIAS, TB_ACTIVO )
        values ( 'C', 'Capacitaci�n', 'Training', 90, 'S' )
    end
    select @Cuantos = COUNT(*) from INCIDEN where ( TB_CODIGO = 'PAT' );
    if ( @Cuantos = 0 )
    begin
        insert into INCIDEN ( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_INCIDEN, TB_PERMISO )
        values ( 'PAT', 'Permiso por Paternidad', 'Paternity', 4, 0 )
    end
END
GO

/* Agrega valores sugeridos de contratos e incidencias (1/3)*/
/* Patch 435 #Seq: X*/
EXECUTE AGREGA_SUGERIDOS
GO

/* Agrega valores sugeridos de contratos e incidencias (1/3)*/
/* Patch 435 #Seq: X*/
DROP PROCEDURE AGREGA_SUGERIDOS
GO

/*****************************************/
/************* VIEWS *********************/
/*****************************************/

/* CR:2570, Modificar V_POL_VIG para agregar los nuevos campos 1/2 */
/* Patch 435 #Seq: 1*/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[V_POL_VIG]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_POL_VIG
GO

/* CR:2570, Modificar V_POL_VIG para agregar los nuevos campos 2/2 */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_POL_VIG(
  PM_CODIGO  ,
	PV_REFEREN ,
	PV_FEC_INI ,
	PV_FEC_FIN ,
	PV_CONDIC  ,
	PV_TEXTO   ,
	PV_NUMERO  ,
	PV_CFIJO ,
	PV_STITULA  ,
	PV_SDEPEND
)
AS
SELECT
	P.PM_CODIGO  ,
	P.PV_REFEREN ,
	P.PV_FEC_INI ,
	P.PV_FEC_FIN ,
	P.PV_CONDIC  ,
	P.PV_TEXTO   ,
	P.PV_NUMERO  ,
	P.PV_CFIJO   ,
	(select Count(CB_CODIGO) from EMP_POLIZA E where E.PM_CODIGO = P.PM_CODIGO and E.PV_REFEREN = P.PV_REFEREN and E.EP_TIPO = 0) PV_STITULA,
	(select Count(CB_CODIGO) from EMP_POLIZA E where E.PM_CODIGO = P.PM_CODIGO and E.PV_REFEREN = P.PV_REFEREN and E.EP_TIPO = 1) PV_SDEPEND
from POL_VIGENC P;
GO

/* CR:2570, Modificar V_EMP_POL para agregar los nuevos campos 1/2 */
/* Patch 435 #Seq: 1*/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[V_POL_VIG]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view V_EMP_POL
GO

/* CR:2570, Modificar V_EMP_POL para agregar los nuevos campos 2/2 */
/* Patch 435 #Seq: 1*/
create VIEW V_EMP_POL(
  CB_CODIGO  ,
	PM_CODIGO  ,
	PV_REFEREN ,
	EP_TIPO    ,
	PA_FOLIO   ,
	PA_RELACIO ,
	EP_CERTIFI ,
	EP_FEC_INI ,
	EP_FEC_FIN ,
	EP_CFIJO   ,
	EP_CPOLIZA ,
	EP_CTITULA ,
	EP_OBSERVA ,
	EP_CANCELA ,
	EP_STATUS ,
	EP_CAN_FEC ,
	EP_CAN_MON ,
	EP_CAN_MOT ,
	EP_GLOBAL  ,
	US_CODIGO  ,
	EP_FEC_REG ,
	EP_DEPEND ,
	EP_ASEGURA,
	EP_ORDEN,
	EP_CEMPRES,
	PM_NUMERO,
	EP_END_ALT,
	EP_END_CAN,
	LLAVE
)
AS
SELECT E.CB_CODIGO  ,
	E.PM_CODIGO  ,
	E.PV_REFEREN ,
	E.EP_TIPO    ,
	E.PA_FOLIO   ,
	E.PA_RELACIO ,
	E.EP_CERTIFI ,
	E.EP_FEC_INI ,
	E.EP_FEC_FIN ,
	E.EP_CFIJO   ,
	E.EP_CPOLIZA ,
	E.EP_CTITULA ,
	E.EP_OBSERVA ,
	E.EP_CANCELA ,
	E.EP_STATUS,
	E.EP_CAN_FEC ,
	E.EP_CAN_MON ,
	E.EP_CAN_MOT ,
	E.EP_GLOBAL  ,
	E.US_CODIGO  ,
	E.EP_FEC_REG ,
  ( P.PA_APE_PAT+' '+P.PA_APE_MAT+' , '+PA_NOMBRES ) As EP_DEPEND,
	E.EP_ASEGURA,
	E.EP_ORDEN,
	E.EP_CEMPRES,
	PM.PM_NUMERO,
	E.EP_END_ALT,
	E.EP_END_CAN,
  E.LLAVE
from EMP_POLIZA E
LEFT OUTER JOIN PARIENTE P on (P.PA_RELACIO = E.PA_RELACIO and P.PA_FOLIO = E.PA_FOLIO and P.CB_CODIGO = E.CB_CODIGO)
LEFT OUTER JOIN POLIZA_MED PM on PM.PM_CODIGO = E.PM_CODIGO
GO

/* CR:2669, Agregar nuevos campos de NOMINA a vista V_NOMMES (1/2)*/
/* Patch 435 #Seq: X*/
drop view V_NOMMES
GO

/* CR:2669, Agregar nuevos campos de NOMINA a vista V_NOMMES (2/2)*/
/* Patch 435 #Seq: X*/
create view V_NOMMES
as
select * from NOMINA
GO

/* CR:2653, Mejora Confidencialidad - Crear view VCONF_TAB */
/* Patch 435 #Seq: 1*/

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'VCONF_TAB'))
  drop view VCONF_TAB
GO

create view VCONF_TAB 
as 
select CT_TABLA, CT_TABNAME=E.EN_TABLA, CT_TABDES=E.EN_TITULO, CT_CODIGO, CT_NIVEL0 from CONF_TAB CT
join R_ENTIDAD E on E.EN_CODIGO = CT.CT_TABLA
join V_NIVEL0 N on N.TB_CODIGO COLLATE DATABASE_DEFAULT  = CT.CT_NIVEL0 COLLATE DATABASE_DEFAULT
GO

/* Patch 435 #Seq: 1*/
/* CR:2681, Mejoras WF 2013 - Crear funcion de uso en nuevas vistas de modelos WF - SP_GET_EMPRESAS */

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'SP_GET_DB_FULLNAME'))
  drop function SP_GET_DB_FULLNAME
GO

/* CR:2681, Mejoras WF 2013 - Crear funcion de uso en nuevas vistas de modelos WF - SP_GET_DB_FULLNAME */
/* Patch 435 #Seq: 1*/
CREATE FUNCTION SP_GET_DB_FULLNAME()
returns Varchar(512)
as
begin
	declare @nameServer varchar(200)
	declare @nameMachine varchar(100)
	declare @kInstance varchar(100)
	declare @constrServer varchar(200)
	declare @retValue varchar(512)

	select @nameServer = convert(nvarchar(128), serverproperty('servername'));
	select @nameMachine = convert(nvarchar(128), serverproperty('machinename'));
	if len(@nameServer) = len(@nameMachine)
	select @kInstance = '' -- 'NN'
	else
	select @kInstance = right(@nameServer, len(@nameServer) - (len(@nameMachine)+1));

	if @kInstance='' select @constrServer = @nameMachine
	else select @constrServer = @nameMachine + '\' + @kInstance;

	set @retValue = @constrServer + '.' + db_name();

	set @retValue = UPPER( RTRIM( @retValue ) ) ;

	return @retValue
end;
GO

IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_COMPANY'))
  DROP view V_COMPANY
GO

Create VIEW V_COMPANY (
					CM_CODIGO ,
					CM_NOMBRE,
					CM_ACUMULA,
					CM_CONTROL,
					CM_EMPATE,
					CM_ALIAS,
					CM_USRNAME,
					CM_USACAFE,
					CM_NIVEL0,
					CM_DATOS,
					CM_DIGITO,
					CM_KCLASIFI,
          CM_KCONFI,
          CM_KUSERS, 
          CM_USACASE
					 )
AS
SELECT
		CM_CODIGO,
		CM_NOMBRE,
		CM_ACUMULA,
		CM_CONTROL,
		CM_EMPATE,
		CM_ALIAS,
		CM_USRNAME,
		CM_USACAFE,
		CM_NIVEL0,
		CM_DATOS,
		CM_DIGITO,
		CM_KCLASIFI,
    CM_KCONFI,
    CM_KUSERS, 
    CM_USACASE
FROM #COMPARTE..COMPANY
GO


IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'SP_GET_EMPRESAS'))
  DROP FUNCTION SP_GET_EMPRESAS
GO

create function SP_GET_EMPRESAS()
returns TABLE
as
	return
  select CM_CODIGO from V_COMPANY where UPPER(CM_DATOS) = DBO.SP_GET_DB_FULLNAME()
GO
/* CR:2681, Mejoras WF 2013 - Crear view V_PROCESO */
/* Patch 435 #Seq: 1*/
IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_PROCESO'))
 DROP VIEW V_PROCESO
GO

CREATE VIEW V_PROCESO
AS
SELECT WP_FOLIO,WE_DESCRIP,WE_ORDEN,WM_CODIGO,WM_DESCRIP,WP_FEC_FIN,WP_FEC_INI,WP_NOM_INI,WP_NOMBRE,WP_NOTAS,WP_PASO,WP_PASOS,WP_STATUS,WP_USR_INI,WS_FEC_FIN,WS_FEC_INI,WS_NOM_INI,WS_NOMBRE,WS_STATUS,WS_USR_INI,WT_AVANCE,WT_DESCRIP,WT_FEC_FIN,WT_FEC_INI,WT_NOM_DES,WT_NOM_ORI,WT_NOTAS,WT_STATUS,WT_USR_DES,WT_USR_ORI,WP_MOV3
FROM #COMPARTE..V_PROCESO
GO

/* CR:2681, Mejoras WF 2013 - Crear view V_WPCAMBIO */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPCAMBIO
AS
select W.* from #COMPARTE..V_WPCAMBIO W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

/* CR:2681, Mejoras WF 2013 - Crear view V_WPAUTO3 */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPAUTO3
AS
select W.* from #COMPARTE..V_WPAUTO3 W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

/* CR:2681, Mejoras WF 2013 - Crear view V_WPMULTIP */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPMULTIP
AS
select W.* from #COMPARTE..V_WPMULTIP W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

/* CR:2681, Mejoras WF 2013 - Crear view V_WPPERM */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPPERM
AS
select W.* from #COMPARTE..V_WPPERM W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

/* CR:2681, Mejoras WF 2013 - Crear view V_WPVACA */
/* Patch 435 #Seq: 1*/
CREATE VIEW V_WPVACA
AS
select W.* from #COMPARTE..V_WPVACA W
join dbo.SP_GET_EMPRESAS() CM on W.CM_CODIGO = CM.CM_CODIGO
GO

/* CR:2534, Terminales GTI 2013 - view V_EMP_BIO */
/* Patch 435 #Seq: 1*/
IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_EMP_BIO'))
  drop view V_EMP_BIO
go
create view V_EMP_BIO as select ID_NUMERO, CM_CODIGO, CB_CODIGO, GP_CODIGO, IV_CODIGO from #COMPARTE.dbo.EMP_BIO
GO

/* CR:2534, Terminales GTI 2013 - view V_TERMINAL */
/* Patch 435 #Seq: 1*/
IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_TERMINAL'))
  drop view V_TERMINAL
GO
create view V_TERMINAL
as
	select TE_CODIGO, TE_NOMBRE, TE_NUMERO, TE_TEXTO, TE_MAC, TE_APP, TE_BEAT, TE_ZONA, TE_ACTIVO, TE_XML from #COMPARTE.dbo.TERMINAL
GO

/* CR:2534, Terminales GTI 2013 - view V_GRUPOTER */
/* Patch 435 #Seq: 1*/

IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_GRUPOTER'))
  drop view V_GRUPOTER
GO
create view V_GRUPOTER
as
	select GP_CODIGO, GP_DESCRIP, GP_NUMERO, GP_TEXTO, GP_ACTIVO from #COMPARTE.dbo.GRUPOTERM
GO

/* CR:2534, Terminales GTI 2013 - view V_TERM_GPO */
/* Patch 435 #Seq: 1*/
IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_TERM_GPO'))
  drop view V_TERM_GPO
GO
create view V_TERM_GPO
as
	select TE_CODIGO, GP_CODIGO from #COMPARTE.dbo.TERM_GPO
GO

/* CR:2534, Terminales GTI 2013 - view V_WS_BITAC */
/* Patch 435 #Seq: 1*/

IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_WS_BITAC'))
  drop view V_WS_BITAC
go
create view V_WS_BITAC
as
	select WS_FECHA,CH_RELOJ,WS_CHECADA,CB_CODIGO,WS_MENSAJE,CM_CODIGO,WS_TIPO from #COMPARTE.dbo.WS_BITAC
GO

/* CR:2534, Terminales GTI 2013 - view VWS_ACCESS */
/* Patch 435 #Seq: 1*/
IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'VWS_ACCESS'))
  drop view VWS_ACCESS
go
create view VWS_ACCESS
as
	select WA_FECHA, CM_CODIGO, CB_CODIGO, CH_RELOJ, WS_CHECADA from #COMPARTE.dbo.WS_ACCESS
GO

/* CR:2534, Terminales GTI 2013 - view V_MENSAJE */
/* Patch 435 #Seq: 1*/
IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_MENSAJE'))
  drop view V_MENSAJE
GO
create view V_MENSAJE (DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO )
as
	select DM_CODIGO, DM_MENSAJE, DM_ORDEN, DM_SLEEP, DM_ACTIVO from #COMPARTE.dbo.MENSAJE
GO

/* CR:2534, Terminales GTI 2013 - view V_TERM_MSG */
/* Patch 435 #Seq: 1*/
IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'V_TERM_MSG'))
  drop view V_TERM_MSG
GO
create view V_TERM_MSG (DM_CODIGO, TE_CODIGO, TM_NEXT)
as
	select DM_CODIGO, TE_CODIGO, TM_NEXT from #COMPARTE.dbo.TERM_MSG
GO

/* CR:2534, Terminales GTI 2013 - view VHUELLAGTI */
/* Patch 435 #Seq: 1*/
create view VHUELLAGTI
as
	select HU_ID, CM_CODIGO, CB_CODIGO, HU_INDICE from #COMPARTE.dbo.HUELLAGTI
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Vista de Entrenamiento del Empleado */
/* Patch 435 #Seq: 1*/
Create View V_PLAN_CAP
AS
	/*Historial de Evaluaciones de Competencia*/
select
		   C.CB_CODIGO,
		   C.CB_PUESTO,
		   CPC.CC_CODIGO,
		   CPC.NC_NIVEL,
		   CCE.EC_FECHA,
		   ISNULL(CCE.NC_NIVEL,0) AS EC_NIVEL,
		   CCE.EC_CALIFIC,
		   CCE.US_CODIGO,
		   'Puesto' as PC_GLOBAL,
		   CPO.CP_CODIGO,
		   (CASE WHEN ( CCE.NC_NIVEL is NULL)THEN 0 Else CPC.PC_PESO end ) as PC_PESO,
		   ' ' as Observaciones
	from COLABORA C
	JOIN PUESTO P ON (P.PU_CODIGO = C.CB_PUESTO)
	left join C_PERF_PTO CPO on ( CPO.PU_CODIGO = P.PU_CODIGO)
	left join C_PER_COMP CPC on ( CPC.CP_CODIGO = CPO.CP_CODIGO )
	left join C_EVA_COMP CCE on ( CCE.CB_CODIGO = C.CB_CODIGO and CCE.CC_CODIGO = CPC.CC_CODIGO and CCE.NC_NIVEL >= CPC.NC_NIVEL)
	left join C_PERFIL CP on (CP.CP_CODIGO = CPC.CP_CODIGO  )
	left join C_COMPETEN CC on (CC.CC_CODIGO = CPC.CC_CODIGO )
	where NOT CPC.CC_CODIGO is NULL and CC.CC_ACTIVO = 'S' and CP.CP_ACTIVO = 'S' and CPO.PP_ACTIVO = 'S'
union all
	/*Excepcion de Competencias Registradas*/
	select
		   C.CB_CODIGO,
		   C.CB_PUESTO,
		   CEC.CC_CODIGO,
		   CEC.NC_NIVEL,
		   CCE.EC_FECHA,
		   isnull(CCE.NC_NIVEL,0)AS EC_NIVEL,
		   CCE.EC_CALIFIC,
		   CCE.US_CODIGO,
		   'Individual' as PC_GLOBAL,
		   ' ' as CP_CODIGO,
			0 AS PC_PESO,
		   CEC.ECC_COMENT  as Observaciones
	from COLABORA C
	join C_EMP_COMP CEC on (CEC.CB_CODIGO = C.CB_CODIGO )
	left join C_COMPETEN CC on (CC.CC_CODIGO = CEC.CC_CODIGO )
	left join C_EVA_COMP CCE on ( CCE.CB_CODIGO = C.CB_CODIGO and CCE.CC_CODIGO = CEC.CC_CODIGO and CCE.NC_NIVEL >= CEC.NC_NIVEL)
	where CC.CC_ACTIVO = 'S'
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Funcion de Vista de Matriz de Habilidades */
/* Patch 435 #Seq: 1*/
CREATE function F_GET_MTRZ_HAB()
 RETURNS
  @Matriz TABLE (
	CB_CODIGO int,
	CP_CODIGO char(6),
	RP_FEC_INI Datetime,
	CC_CODIGO char(6),
	NC_NIVEL  int,
	RC_FEC_INI DateTime,
	PC_PESO Decimal(15,2),
	EC_NIVEL Smallint
   )
AS
BEGIN
   declare @PERFIL char(6);
   declare @Empleado int;
   declare @Compentencia char(6);
   declare @Nivel smallint;
   declare @PerFecIni DateTime;
   declare @Peso Decimal(15,2);

   declare cPERFILs cursor for Select CP_CODIGO from C_PERFIL ;
   
	OPEN cPERFILs
	FETCH NEXT FROM cPERFILs
	into @PERFIL
	WHILE @@FETCH_STATUS = 0
	begin
	     declare cCompt cursor for select CP.CC_CODIGO,CP.NC_NIVEL,CP.RP_FEC_INI,CP.PC_PESO from C_PER_COMP CP
								   left outer join C_COMPETEN CC on CC.CC_CODIGO = CP.CC_CODIGO
								   left outer join C_PERFIL PP on PP.CP_CODIGO = CP.CP_CODIGO	 	
							   where CP.CP_CODIGO = @PERFIL and PP.CP_ACTIVO = 'S' and CC.CC_ACTIVO = 'S';
		 open cCompt;
		 FETCH NEXT FROM cCompt
	     into @Compentencia,@Nivel,@PerFecIni,@Peso

		 WHILE @@FETCH_STATUS = 0
		 begin
		 	  insert into @Matriz (cb_codigo,CP_CODIGO,RP_FEC_INI,CC_CODIGO,NC_NIVEL,RC_FEC_INI,PC_PESO,EC_NIVEL)
			  select
					C.CB_CODIGO,
					@PERFIL as PERFIL,
					@PerFecIni as FechaPerfil,
					isnull(CCE.CC_CODIGO,@Compentencia) as CC_CODIGO,
					isnull(CCE.NC_NIVEL,@Nivel)as NC_NIVEL,
					CRC.RC_FEC_INI AS RC_FEC_INI,
					(CASE WHEN ( CCE.NC_NIVEL >= @Nivel)THEN 1 Else 0 end ) as PC_PESO,
					IsNull(CCE.NC_NIVEL,0)as EC_NIVEL
					from COLABORA C
					left outer join C_EVA_COMP CCE on ( CCE.CB_CODIGO = C.CB_CODIGO ) and ( CCE.CC_CODIGO = @Compentencia )
					left outer join C_REV_COMP CRC on ( CRC.CC_CODIGO = @Compentencia )
					where CB_ACTIVO = 'S'
			  FETCH NEXT FROM cCompt
			  into @Compentencia,@Nivel,@PerFecIni,@Peso
		 end
		 close cCompt
	     deallocate cCompt
		 FETCH NEXT FROM cPERFILs
		 into @PERFIL
    end
	close cPERFILs
    deallocate cPERFILs
	RETURN
END
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Vista de Matriz de Habilidades */
/* Patch 435 #Seq: 1*/
create view V_MAT_HABL
AS
	SELECT F.*,
	CAST(C.CB_CODIGO as VarCHAR(10))+' : '+C.PRETTYNAME as PRETTYNAME,
	C.CB_NIVEL0,
	C.CB_PUESTO,
	P.PU_DESCRIP,
	N1.TB_ELEMENT as Nivel_1,
	N2.TB_ELEMENT as Nivel_2,
	N3.TB_ELEMENT as Nivel_3,
	N4.TB_ELEMENT as Nivel_4,
	N5.TB_ELEMENT as Nivel_5,
	N6.TB_ELEMENT as Nivel_6,
	N7.TB_ELEMENT as Nivel_7,
	N8.TB_ELEMENT as Nivel_8,
	N9.TB_ELEMENT as Nivel_9,
	CASE WHEN(EC_NIVEL = 0 )THEN '0' else (CAST(F.EC_NIVEL as VARCHAR(5)) +' : '+NC.NC_DESCRIP) end AS NC_DESCRIP
	FROM F_GET_MTRZ_HAB() F
	left outer join COLABORA C on C.CB_CODIGO =F.CB_CODIGO
	left outer join PUESTO P on P.PU_CODIGO = C.CB_PUESTO
	left outer join NIVEL1 N1 on N1.TB_CODIGO = C.CB_NIVEL1
	left outer join NIVEL2 N2 on N2.TB_CODIGO = C.CB_NIVEL2
	left outer join NIVEL3 N3 on N3.TB_CODIGO = C.CB_NIVEL3
	left outer join NIVEL4 N4 on N4.TB_CODIGO = C.CB_NIVEL4
	left outer join NIVEL5 N5 on N5.TB_CODIGO = C.CB_NIVEL5
	left outer join NIVEL6 N6 on N6.TB_CODIGO = C.CB_NIVEL6
	left outer join NIVEL7 N7 on N7.TB_CODIGO = C.CB_NIVEL7
	left outer join NIVEL8 N8 on N8.TB_CODIGO = C.CB_NIVEL8
	left outer join NIVEL9 N9 on N9.TB_CODIGO = C.CB_NIVEL9
	left outer join C_NIV_COMP NC on NC.NC_NIVEL = F.EC_NIVEL and F.CC_CODIGO = NC.CC_CODIGO
	where C.CB_ACTIVO = 'S'
GO

/* CR:2682: Actualizacion de ista V_COMPANY para fin de Reporte del campo Usa Caseta*/
/* Patch 435 #Seq: X */


/* CR:2534, Terminales GTI 2013 - Cambio de la vista V_DISPOSIT para quitar el campo DI_SYNC */
/* Patch 435 #Seq: 1*/
if exists( select NAME from SYSOBJECTS where NAME = 'V_DISPOSIT' and XTYPE = 'V' )
	drop view V_DISPOSIT
go

/* CR:2534, Terminales GTI 2013 - crear la vista V_DISPOSIT sin el campo DI_SYNC */
/* Patch 435 #Seq: 1*/
create view V_DISPOSIT(
		DI_NOMBRE,
		DI_TIPO,
 		DI_DESCRIP,
        DI_NOTA,
        DI_IP)

as 
	select DI_NOMBRE,
	  DI_TIPO,
	  DI_DESCRIP,
	  DI_NOTA,
	  DI_IP
      from #COMPARTE.dbo.DISPOSIT
go

/* CR:2534, Terminales GTI 2013 - Borrar la vista V_DISPOGRU ya no se requiere */
/* Patch 435 #Seq: 1*/
if exists( select NAME from SYSOBJECTS where NAME = 'V_DISPOGRU' and XTYPE = 'V' )
	drop view V_DISPOGRU
go

delete from R_ATRIBUTO where AT_CAMPO = 'DI_SYNC'
GO

/*****************************************/
/*********** PROCEDURES ******************/
/*****************************************/

/* CR:2690, Recalcular plazas */
/* Patch 435 #Seq: X*/
CREATE PROCEDURE RECALCULA_PLAZAS(@Kardex char(1)) AS
BEGIN
   SET NOCOUNT ON
   declare @Empleado int;
   declare @Plaza int;
   declare @Fecha DateTime;

   declare CursorPlazas CURSOR FOR
   select	CB_CODIGO, PL_FOLIO
   from PLAZA
   where CB_CODIGO != 0
   order by PL_FOLIO

   Open CursorPlazas
   Fetch NEXT from CursorPlazas
   into	@Empleado,@Plaza

   while ( @@FETCH_STATUS = 0 )
   BEGIN
		if @Kardex = 'S' 
    begin
			SELECT TOP 1 @Fecha =  CB_FECHA FROM kardex WHERE CB_CODIGO = @Empleado and CB_TIPO = 'ALTA' ORDER BY CB_FECHA asc
			--Borrar movimientos de Plaza
			Delete from Kardex where CB_CODIGO = @Empleado and CB_TIPO ='PLAZA';
			--Actualizar las altas de los empleados para colocar la plaza
			Update Kardex set CB_PLAZA = @Plaza
			where CB_CODIGO = @Empleado and
				  CB_TIPO = 'ALTA' and
				  CB_FECHA = @Fecha;
			EXECUTE RECALCULA_KARDEX @Empleado;
        end
	    else
	    begin
      Update COLABORA set CB_PLAZA = 0 WHERE cb_codigo = @Empleado;

		end
    Fetch NEXT from CursorPlazas
	  into	@Empleado,@Plaza
   END
   Close CursorPlazas
   Deallocate CursorPlazas

END
GO

/* CR:2669, 2704 Cambios a SP_VALOR_CONCEPTO */
/* Patch 435 #Seq: X*/
ALTER FUNCTION DBO.SP_VALOR_CONCEPTO(
 				@Concepto SMALLINT,
				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
				@Razonsocial CHAR(6),
				@TipoNomina SMALLINT,
				@ValidaReingreso CHAR(1)
				 )

RETURNS NUMERIC(15,2)
AS
BEGIN
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
	 DECLARE @M1216 NUMERIC(15,2);
	 DECLARE @TipoNom SMALLINT;
     DECLARE @Resultado NUMERIC(15,2);

select @Resultado = 0;

if ( @Concepto <> 1004 )
begin
	DECLARE TemporalCursorNom Cursor for

     select N.NO_PERCEPC,
            N.NO_DEDUCCI,
            N.NO_NETO,
            N.NO_X_ISPT,
            N.NO_PER_MEN,
            N.NO_X_MENS,
            N.NO_PER_CAL,
            N.NO_IMP_CAL,
            N.NO_X_CAL,
            N.NO_TOT_PRE,
			      N.NO_PREV_GR,
			      N.NO_PER_ISN,
            N.NO_TOTAL_1,
            N.NO_TOTAL_2,
            N.NO_TOTAL_3,
            N.NO_TOTAL_4,
            N.NO_TOTAL_5,
            N.NO_DIAS_FI,
            N.NO_DIAS_FJ,
            N.NO_DIAS_CG,
            N.NO_DIAS_SG,
            N.NO_DIAS_SU,
            N.NO_DIAS_OT,
            N.NO_DIAS_IN,
            N.NO_DIAS * 7 / 6,
            N.NO_DIAS_RE,
            N.NO_D_TURNO,
            N.NO_DIAS,
            N.NO_DIAS_AS,
            N.NO_DIAS_NT,
            N.NO_DIAS_FV,
            N.NO_DIAS_VA,
            N.NO_DIAS_AG,
            N.NO_DIAS_AJ,
            N.NO_DIAS_SS,
            N.NO_DIAS_EM,
            N.NO_DIAS_SI,
            N.NO_DIAS_BA,
            N.NO_DIAS_PV,
            N.NO_DIAS_VJ,
            N.NO_DIAS_VJ * 7 / 6,
            N.NO_DIAS_IH,
            N.NO_DIAS_ID,
            N.NO_DIAS_IT,
            N.NO_DIAS_DT,
            N.NO_DIAS_FS,
            N.NO_DIAS_FT,
            N.NO_JORNADA,
            N.NO_HORAS,
            N.NO_EXTRAS,
            N.NO_DOBLES,
            N.NO_TRIPLES,
            N.NO_ADICION,
            N.NO_TARDES,
            N.NO_HORA_PD,
            N.NO_HORA_CG,
            N.NO_HORA_SG,
            N.NO_FES_TRA,
            N.NO_DES_TRA,
            N.NO_VAC_TRA,
            N.NO_FES_PAG,
            N.NO_HORASNT,
            N.NO_HORAPDT,
		        N.PE_TIPO,
			      N.NO_PRE_EXT

		  from NOMINA N
		  LEFT OUTER JOIN PERIODO ON PERIODO.PE_YEAR = N.PE_YEAR AND PERIODO.PE_TIPO = N.PE_TIPO AND PERIODO.PE_NUMERO = N.PE_NUMERO
		  LEFT OUTER JOIN RPATRON ON RPATRON.TB_CODIGO = N.CB_PATRON
		  LEFT OUTER JOIN RSOCIAL ON RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
		  LEFT OUTER JOIN COLABORA ON COLABORA.CB_CODIGO = N.CB_CODIGO
		where
			( N.PE_YEAR = @Anio ) and
			( N.CB_CODIGO = @Empleado ) and
			( PERIODO.PE_STATUS = 6 ) and
			( PERIODO.PE_MES = @Mes ) and
			( ( @TipoNomina = -1 ) or ( N.PE_TIPO = @TipoNomina ) ) and
			( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
      		( ( @ValidaReingreso <> 'S' ) or ( COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN ) )



		open TemporalCursorNom
		Fetch next from TemporalCursorNom
		into
        @M1000,
        @M1001,
        @M1002,
        @M1003,
        @M1005,
        @M1006,
        @M1007,
        @M1008,
        @M1009,
        @M1010,
        @M1011,
		    @M1012,
		    @M1013,
		    @M1014,
		    @M1015,
		    @M1016,
		    @M1017,
        @M1100,
        @M1101,
        @M1102,
        @M1103,
        @M1104,
        @M1105,
        @M1106,
        @M1107,
        @M1108,
        @M1109,
        @M1110,
        @M1111,
        @M1112,
        @M1113,
        @M1114,
        @M1115,
        @M1116,
        @M1117,
        @M1118,
        @M1119,
        @M1120,
        @M1121,
        @M1122,
        @M1123,
        @M1124,
        @M1125,
        @M1126,
        @M1127,
        @M1128,
        @M1129,
        @M1200,
        @M1201,
        @M1202,
        @M1203,
        @M1204,
        @M1205,
        @M1206,
        @M1207,
        @M1208,
        @M1209,
        @M1210,
        @M1211,
        @M1212,
        @M1213,
        @M1214,
        @M1215,
        @TipoNom,
		@M1216

	WHILE @@FETCH_STATUS = 0
     begin

          if ( @Concepto = 1200 or @Concepto = 1109 ) and ( @M1200 <> 0 )
          begin
                Select @M1200 = @M1201 / @M1200;
          end
          if ( @Concepto = 1000 ) and (@M1000 <> 0)
            select @Resultado = @M1000 + @Resultado
          if ( @Concepto = 1001 ) and (@M1001 <> 0)
            select @Resultado = @M1001 + @Resultado
          if ( @Concepto = 1002 ) and (@M1002 <> 0)
            select @Resultado = @M1002 + @Resultado
          if ( @Concepto = 1003 ) and (@M1003 <> 0)
            select @Resultado = @M1003 + @Resultado
          if ( @Concepto = 1005 ) and (@M1005 <> 0)
            select @Resultado = @M1005 + @Resultado
          if ( @Concepto = 1006 ) and (@M1006 <> 0)
            select @Resultado = @M1006 + @Resultado
          if ( @Concepto = 1007 ) and (@M1007 <> 0)
            select @Resultado = @M1007 + @Resultado
          if ( @Concepto = 1008 ) and (@M1008 <> 0)
            select @Resultado = @M1008 + @Resultado
          if ( @Concepto = 1009 ) and (@M1009 <> 0)
            select @Resultado = @M1009 + @Resultado
          if ( @Concepto = 1010 ) and (@M1010 <> 0)
            select @Resultado = @M1010 + @Resultado
          if ( @Concepto = 1011 ) and (@M1011 <> 0)
            select @Resultado = @M1011 + @Resultado
          if ( @Concepto = 1012 ) and (@M1012 <> 0)
            select @Resultado = @M1012 + @Resultado
          if ( @Concepto = 1013 ) and (@M1013 <> 0)
            select @Resultado = @M1013 + @Resultado
          if ( @Concepto = 1014 ) and (@M1014 <> 0)
            select @Resultado = @M1014 + @Resultado
          if ( @Concepto = 1015 ) and (@M1015 <> 0)
            select @Resultado = @M1015 + @Resultado
          if ( @Concepto = 1016 ) and (@M1016 <> 0)
            select @Resultado = @M1016 + @Resultado
          if ( @Concepto = 1017 ) and (@M1017 <> 0)
            select @Resultado = @M1017 + @Resultado
          if ( @Concepto = 1100 ) and (@M1100 <> 0)
            select @Resultado = @M1100 + @Resultado
          if ( @Concepto = 1101 ) and (@M1101 <> 0)
            select @Resultado = @M1101 + @Resultado
          if ( @Concepto = 1102 ) and (@M1102 <> 0)
            select @Resultado = @M1102 + @Resultado
          if ( @Concepto = 1103 ) and (@M1103 <> 0)
            select @Resultado = @M1103 + @Resultado
          if ( @Concepto = 1104 ) and (@M1104 <> 0)
            select @Resultado = @M1104 + @Resultado
          if ( @Concepto = 1105 ) and (@M1105 <> 0)
            select @Resultado = @M1105 + @Resultado
          if ( @Concepto = 1106 ) and (@M1106 <> 0)
            select @Resultado = @M1106 + @Resultado
          if ( @Concepto = 1107 ) and (@M1107 <> 0)
            select @Resultado = @M1107 + @Resultado
          if ( @Concepto = 1108 ) and (@M1108 <> 0)
            select @Resultado = @M1108 + @Resultado
          if ( @Concepto = 1109 ) and (@M1109 <> 0)
		begin
			select @M1109 = @M1200 * @M1109;
               select @Resultado = @M1109 + @Resultado
		end
          if ( @Concepto = 1110 ) and (@M1110 <> 0)
            select @Resultado = @M1110 + @Resultado
          if ( @Concepto = 1111 ) and (@M1111 <> 0)
            select @Resultado = @M1111 + @Resultado
          if ( @Concepto = 1112 ) and (@M1112 <> 0)
            select @Resultado = @M1112 + @Resultado
          if ( @Concepto = 1113 ) and (@M1113 <> 0)
            select @Resultado = @M1113 + @Resultado
          if ( @Concepto = 1114 ) and (@M1114 <> 0)
            select @Resultado = @M1114 + @Resultado
          if ( @Concepto = 1115 ) and (@M1115 <> 0)
            select @Resultado = @M1115 + @Resultado
          if ( @Concepto = 1116 ) and (@M1116 <> 0)
            select @Resultado = @M1116 + @Resultado
          if ( @Concepto = 1117 ) and (@M1117 <> 0)
            select @Resultado = @M1117 + @Resultado
          if ( @Concepto = 1118 ) and (@M1118 <> 0)
            select @Resultado = @M1118 + @Resultado
          if ( @Concepto = 1119 ) and (@M1119 <> 0)
              select @Resultado = @M1119 + @Resultado
          if ( @Concepto = 1120 ) and (@M1120 <> 0)
            select @Resultado = @M1120 + @Resultado
          if ( @Concepto = 1121 ) and (@M1121 <> 0)
            select @Resultado = @M1121 + @Resultado
          if ( @Concepto = 1122 ) and (@M1122 <> 0)
            select @Resultado = @M1122 + @Resultado
          if ( @Concepto = 1123 ) and (@M1123 <> 0)
            select @Resultado = @M1123 + @Resultado
          if ( @Concepto = 1124 ) and (@M1124 <> 0)
            select @Resultado = @M1124 + @Resultado
          if ( @Concepto = 1125 ) and (@M1125 <> 0)
            select @Resultado = @M1125 + @Resultado
          if ( @Concepto = 1126 ) and (@M1126 <> 0)
            select @Resultado = @M1126 + @Resultado
          if ( @Concepto = 1127 ) and (@M1127 <> 0)
            select @Resultado = @M1127 + @Resultado
          if ( @Concepto = 1128 ) and (@M1128 <> 0)
            select @Resultado = @M1128 + @Resultado
          if ( @Concepto = 1129 ) and (@M1129 <> 0)
            select @Resultado = @M1129 + @Resultado

          if ( @Concepto = 1200 ) and (@M1200 <> 0 )
     	    begin
              if ( @TipoNom = 0 )
                Select @M1200 = 8 * @M1200;
              else
              if ( ( @TipoNom = 1 ) or ( @TipoNom = 6 ) or ( @TipoNom = 7 ) )
                Select @M1200 = 48 * @M1200;
              else
              if ( ( @TipoNom = 2 ) or ( @TipoNom = 10 ) or ( @TipoNom = 11 ) )
                Select @M1200 = 96 * @M1200;
              else
              if ( ( @TipoNom = 3 ) or ( @TipoNom = 8 ) or ( @TipoNom = 9 ) )
                Select @M1200 = 104 * @M1200;
              else
              if ( @TipoNom = 4 )
                Select @M1200 = 208 * @M1200;
              else
              if ( @TipoNom = 5 )
                Select @M1200 = 72 * @M1200;
              select @Resultado = @M1200 + @Resultado
        end
     		if ( @Concepto = 1201 and @M1201 <> 0 )
          select @Resultado = @M1201 + @Resultado
        if ( @Concepto = 1202 and @M1202 <> 0 )
          select @Resultado = @M1202 + @Resultado
        if ( @Concepto = 1203 and @M1203 <> 0 )
          select @Resultado = @M1203 + @Resultado
        if ( @Concepto = 1204 and @M1204 <> 0 )
          select @Resultado = @M1204 + @Resultado
        if ( @Concepto = 1205 and @M1205 <> 0 )
          select @Resultado = @M1205 + @Resultado
        if ( @Concepto = 1206 and @M1206 <> 0 )
          select @Resultado = @M1206 + @Resultado
        if ( @Concepto = 1207 and @M1207 <> 0 )
          select @Resultado = @M1207 + @Resultado
        if ( @Concepto = 1208 and @M1208 <> 0 )
          select @Resultado = @M1208 + @Resultado
        if ( @Concepto = 1209 and @M1209 <> 0 )
          select @Resultado = @M1209 + @Resultado
        if ( @Concepto = 1210 and @M1210 <> 0 )
          select @Resultado = @M1210 + @Resultado
        if ( @Concepto = 1211 and @M1211 <> 0 )
          select @Resultado = @M1211 + @Resultado
        if ( @Concepto = 1212 and @M1212 <> 0 )
          select @Resultado = @M1212 + @Resultado
        if ( @Concepto = 1213 and @M1213 <> 0 )
          select @Resultado = @M1213 + @Resultado
        if ( @Concepto = 1214 and @M1214 <> 0 )
          select @Resultado = @M1214 + @Resultado
        if ( @Concepto = 1215 and @M1215 <> 0 )
          select @Resultado = @M1215 + @Resultado
		if ( @Concepto = 1216 and @M1216 <> 0 )
          select @Resultado = @M1216 + @Resultado


			Fetch next from TemporalCursorNom
     	  into
			      @M1000,
            @M1001,
            @M1002,
            @M1003,
            @M1005,
            @M1006,
            @M1007,
            @M1008,
            @M1009,
            @M1010,
  	        @M1011,
            @M1012,
            @M1013,
            @M1014,
            @M1015,
            @M1016,
            @M1017,
            @M1100,
            @M1101,
            @M1102,
            @M1103,
            @M1104,
            @M1105,
            @M1106,
            @M1107,
            @M1108,
            @M1109,
            @M1110,
            @M1111,
            @M1112,
            @M1113,
            @M1114,
            @M1115,
            @M1116,
            @M1117,
            @M1118,
            @M1119,
            @M1120,
            @M1121,
            @M1122,
            @M1123,
            @M1124,
            @M1125,
            @M1126,
            @M1127,
            @M1128,
            @M1129,
            @M1200,
            @M1201,
            @M1202,
            @M1203,
            @M1204,
            @M1205,
            @M1206,
            @M1207,
            @M1208,
            @M1209,
            @M1210,
            @M1211,
            @M1212,
            @M1213,
            @M1214,
            @M1215,
		    @TipoNom,
            @M1216

	end
  CLOSE TemporalCursorNom
  DEALLOCATE TemporalCursorNom
end
else
begin
  	select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M
	left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
	left outer join NOMINA on NOMINA.PE_YEAR = M.PE_YEAR AND NOMINA.PE_TIPO = M.PE_TIPO AND NOMINA.PE_NUMERO = M.PE_NUMERO AND NOMINA.CB_CODIGO = M.CB_CODIGO
	left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
	left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
	left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
     left outer join COLABORA on COLABORA.CB_CODIGO = M.CB_CODIGO
     where
		( M.PE_YEAR = @Anio ) AND
		( M.CB_CODIGO = @Empleado ) AND
		( M.MO_ACTIVO = 'S' ) AND
		( PERIODO.PE_STATUS = 6 ) AND
		( PERIODO.PE_MES = @Mes ) AND
		( ( @TipoNomina = -1 ) or ( NOMINA.PE_TIPO = @TipoNomina ) ) and
		( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
          ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) ) and
          ( C.CO_TIPO = 1 ) and
		( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        select @Resultado = @M1004
end

  RETURN @Resultado

END
GO

/* CR:2669, 2704 Cambios a AFECTA_EMPLEADO */
/* Patch 435 #Seq: X*/
ALTER PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON
     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1013 NUMERIC(15,2);
     DECLARE @M1014 NUMERIC(15,2);
     DECLARE @M1015 NUMERIC(15,2);
     DECLARE @M1016 NUMERIC(15,2);
     DECLARE @M1017 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1124 NUMERIC(15,2);
     DECLARE @M1125 NUMERIC(15,2);
     DECLARE @M1126 NUMERIC(15,2);
     DECLARE @M1127 NUMERIC(15,2);
     DECLARE @M1128 NUMERIC(15,2);
     DECLARE @M1129 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
     DECLARE @M1216 NUMERIC(15,2);

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1012=N.NO_PER_ISN,
            @M1013=N.NO_TOTAL_1,
            @M1014=N.NO_TOTAL_2,
            @M1015=N.NO_TOTAL_3,
            @M1016=N.NO_TOTAL_4,
            @M1017=N.NO_TOTAL_5,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1124=N.NO_DIAS_IH,
            @M1125=N.NO_DIAS_ID,
            @M1126=N.NO_DIAS_IT,
            @M1127=N.NO_DIAS_DT,
            @M1128=N.NO_DIAS_FS,
            @M1129=N.NO_DIAS_FT,
            @M1200=N.NO_JORNADA,
            @M1201=N.NO_HORAS,
            @M1202=N.NO_EXTRAS,
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
            @M1216=N.NO_PRE_EXT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
		Select @M1200 = @M1201 / @M1200;
     Select @M1109 = @M1200 * @M1109;

     if ( @M1201 < 0 ) and ( @M1200 > 0 )
		set @M1200 = -@M1200;

     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008;
     if ( @M1009 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011;
  	 if ( @M1012 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1012, @Mes, @Factor, @M1012;
  	 if ( @M1013 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1013, @Mes, @Factor, @M1013;
  	 if ( @M1014 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1014, @Mes, @Factor, @M1014;
  	 if ( @M1015 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1015, @Mes, @Factor, @M1015;
  	 if ( @M1016 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1016, @Mes, @Factor, @M1016;
  	 if ( @M1017 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1017, @Mes, @Factor, @M1017;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123;
     if ( @M1124 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1124, @Mes, @Factor, @M1124;
     if ( @M1125 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1125, @Mes, @Factor, @M1125;
     if ( @M1126 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1126, @Mes, @Factor, @M1126;
     if ( @M1127 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1127, @Mes, @Factor, @M1127;
     if ( @M1128 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1128, @Mes, @Factor, @M1128;
     if ( @M1129 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1129, @Mes, @Factor, @M1129;
     if ( @M1200 <> 0 )
     begin
          if ( @Tipo = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( ( @Tipo = 1 ) or ( @Tipo = 6 ) or ( @Tipo = 7 ) )
             Select @M1200 = 48 * @M1200;
          else
          if ( ( @Tipo = 2 ) or ( @Tipo = 10 ) or ( @Tipo = 11 ) )
             Select @M1200 = 96 * @M1200;
          else
          if ( ( @Tipo = 3 ) or ( @Tipo = 8 ) or ( @Tipo = 9 ) )
             Select @M1200 = 104 * @M1200;
          else
          if ( @Tipo = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @Tipo = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200;

     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215;
	 if ( @M1216 <> 0 )
	    EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004;
END
GO

/* CR:2669, Cambios a SP_COPIA_NOMINA */
/* Patch 435 #Seq: X*/
ALTER procedure DBO.SP_COPIA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER )
as
begin
     SET NOCOUNT ON
     declare @Status SmallInt;
     select @Status = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @Status is not Null )
     begin
          if ( @Status > 4 )
          begin
               set @Status = 4;
          end;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,
								  CB_BAN_ELE,
								  NO_HORAPDT,
							      NO_HORASNT,
								  NO_DIAS_SI,
								  NO_DIAS_VJ,
							      NO_DIAS_PV,
								  NO_SUP_OK,
								  NO_FEC_OK,
								  NO_HOR_OK,
								  NO_ASI_INI,
								  NO_ASI_FIN,
								  NO_DIAS_BA,
								  NO_PREV_GR,
								  CB_PLAZA,
								  NO_FEC_LIQ,
								  CB_CTA_GAS,
								  CB_CTA_VAL,
								  NO_PER_ISN,
                  NO_DIAS_IH,
                  NO_DIAS_ID,
                  NO_DIAS_IT,
                  NO_TOTAL_1,
                  NO_TOTAL_2,
                  NO_TOTAL_3,
                  NO_TOTAL_4,
                  NO_TOTAL_5,
                  NO_FACT_BA,
                  NO_DIAS_DT,
                  NO_DIAS_FS,
                  NO_DIAS_FT )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N2.CB_CODIGO,
                 N2.CB_CLASIFI,
                 N2.CB_TURNO,
                 N2.CB_PATRON,
                 N2.CB_PUESTO,
                 N2.CB_ZONA_GE,
                 N2.NO_FOLIO_1,
                 N2.NO_FOLIO_2,
                 N2.NO_FOLIO_3,
                 N2.NO_FOLIO_4,
                 N2.NO_FOLIO_5,
                 N2.NO_OBSERVA,
                 @Status,
                 N2.US_CODIGO,
                 N2.NO_USER_RJ,
                 N2.CB_NIVEL1,
                 N2.CB_NIVEL2,
                 N2.CB_NIVEL3,
                 N2.CB_NIVEL4,
                 N2.CB_NIVEL5,
                 N2.CB_NIVEL6,
                 N2.CB_NIVEL7,
                 N2.CB_NIVEL8,
                 N2.CB_NIVEL9,
                 N2.CB_SAL_INT,
                 N2.CB_SALARIO,
                 N2.NO_DIAS,
                 N2.NO_ADICION,
                 N2.NO_DEDUCCI,
                 N2.NO_NETO,
                 N2.NO_DES_TRA,
                 N2.NO_DIAS_AG,
                 N2.NO_DIAS_VA,
                 N2.NO_DOBLES,
                 N2.NO_EXTRAS,
                 N2.NO_FES_PAG,
                 N2.NO_FES_TRA,
                 N2.NO_HORA_CG,
                 N2.NO_HORA_SG,
                 N2.NO_HORAS,
                 N2.NO_IMP_CAL,
                 N2.NO_JORNADA,
                 N2.NO_PER_CAL,
                 N2.NO_PER_MEN,
                 N2.NO_PERCEPC,
                 N2.NO_TARDES,
                 N2.NO_TRIPLES,
                 N2.NO_TOT_PRE,
                 N2.NO_VAC_TRA,
                 N2.NO_X_CAL,
                 N2.NO_X_ISPT,
                 N2.NO_X_MENS,
                 N2.NO_D_TURNO,
                 N2.NO_DIAS_AJ,
                 N2.NO_DIAS_AS,
                 N2.NO_DIAS_CG,
                 N2.NO_DIAS_EM,
                 N2.NO_DIAS_FI,
                 N2.NO_DIAS_FJ,
                 N2.NO_DIAS_FV,
                 N2.NO_DIAS_IN,
                 N2.NO_DIAS_NT,
                 N2.NO_DIAS_OT,
                 N2.NO_DIAS_RE,
                 N2.NO_DIAS_SG,
                 N2.NO_DIAS_SS,
                 N2.NO_DIAS_SU,
                 N2.NO_HORA_PD,
                 N2.NO_LIQUIDA,
                 N2.NO_FUERA,
                 N2.NO_EXENTAS,
                 N2.NO_FEC_PAG,
                 N2.NO_USR_PAG,
				 N2.CB_NIVEL0,
				 N2.CB_BAN_ELE,
				 N2.NO_HORAPDT,
				 N2.NO_HORASNT,
				 N2.NO_DIAS_SI,
				 N2.NO_DIAS_VJ,
				 N2.NO_DIAS_PV,
				 N2.NO_SUP_OK,
				 N2.NO_FEC_OK,
				 N2.NO_HOR_OK,
				 N2.NO_ASI_INI,
				 N2.NO_ASI_FIN,
				 N2.NO_DIAS_BA,
				 N2.NO_PREV_GR,
				 N2.CB_PLAZA,
				 N2.NO_FEC_LIQ,
				 N2.CB_CTA_GAS,
				 N2.CB_CTA_VAL,
				 N2.NO_PER_ISN,
         N2.NO_DIAS_IH,
         N2.NO_DIAS_ID,
         N2.NO_DIAS_IT,
         N2.NO_TOTAL_1,
         N2.NO_TOTAL_2,
         N2.NO_TOTAL_3,
         N2.NO_TOTAL_4,
         N2.NO_TOTAL_5,
         N2.NO_FACT_BA,
         N2.NO_DIAS_DT,
         N2.NO_DIAS_FS,
         N2.NO_DIAS_FT
          from DBO.NOMINA as N2 where ( N2.PE_YEAR = @YEARORIGINAL ) and
                                      ( N2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( N2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( N2.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M2.CB_CODIGO,
                 M2.MO_ACTIVO,
                 M2.CO_NUMERO,
                 M2.MO_IMP_CAL,
                 M2.US_CODIGO,
                 M2.MO_PER_CAL,
                 M2.MO_REFEREN,
                 M2.MO_X_ISPT,
                 M2.MO_PERCEPC,
                 M2.MO_DEDUCCI
          from DBO.MOVIMIEN as M2 where ( M2.PE_YEAR = @YEARORIGINAL ) and
                                        ( M2.PE_TIPO = @TIPOORIGINAL ) and
                                        ( M2.PE_NUMERO = @NUMEROORIGINAL ) and
                                        ( M2.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  FA_FEC_INI,
                                  FA_DIA_HOR,
                                  FA_MOTIVO,
                                  FA_DIAS,
                                  FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F2.CB_CODIGO,
                 F2.FA_FEC_INI,
                 F2.FA_DIA_HOR,
                 F2.FA_MOTIVO,
                 F2.FA_DIAS,
                 F2.FA_HORAS
          from DBO.FALTAS as F2 where ( F2.PE_YEAR = @YEARORIGINAL ) and
                                      ( F2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( F2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( F2.CB_CODIGO = @EMPLEADO );
     end;
end
GO

/* CR:2669, Cambios a SP_CANCELA_NOMINA */
/* Patch 435 #Seq: X*/
ALTER procedure DBO.SP_CANCELA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER,
@USUARIO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @StatusNuevo SmallInt;
     select @StatusNuevo = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @StatusNuevo is not Null )
     begin
          if ( @StatusNuevo > 4 )
             set @StatusNuevo = 4;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,
								  CB_BAN_ELE,
								  NO_HORAPDT,
								  NO_HORASNT,
								  NO_DIAS_SI,
								  NO_DIAS_VJ,
							      NO_DIAS_PV,
								  NO_SUP_OK,
								  NO_FEC_OK,
								  NO_HOR_OK,
								  NO_ASI_INI,
								  NO_ASI_FIN,
								  NO_DIAS_BA,
								  NO_PREV_GR,
								  CB_PLAZA,
								  NO_FEC_LIQ,
								  CB_CTA_GAS,
								  CB_CTA_VAL,
								  NO_PER_ISN,
                  NO_DIAS_IH,
                  NO_DIAS_ID,
                  NO_DIAS_IT,
                  NO_TOTAL_1,
                  NO_TOTAL_2,
                  NO_TOTAL_3,
                  NO_TOTAL_4,
                  NO_TOTAL_5,
                  NO_FACT_BA,
                  NO_DIAS_DT,
                  NO_DIAS_FS,
                  NO_DIAS_FT
								  )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N1.CB_CODIGO,
                 N1.CB_CLASIFI,
                 N1.CB_TURNO,
                 N1.CB_PATRON,
                 N1.CB_PUESTO,
                 N1.CB_ZONA_GE,
                 N1.NO_FOLIO_1,
                 N1.NO_FOLIO_2,
                 N1.NO_FOLIO_3,
                 N1.NO_FOLIO_4,
                 N1.NO_FOLIO_5,
                 N1.NO_OBSERVA,
                 @StatusNuevo,
                 N1.US_CODIGO,
                 N1.NO_USER_RJ,
                 N1.CB_NIVEL1,
                 N1.CB_NIVEL2,
                 N1.CB_NIVEL3,
                 N1.CB_NIVEL4,
                 N1.CB_NIVEL5,
                 N1.CB_NIVEL6,
                 N1.CB_NIVEL7,
                 N1.CB_NIVEL8,
                 N1.CB_NIVEL9,
                 N1.CB_SAL_INT,
                 N1.CB_SALARIO,
                 -N1.NO_DIAS,
                 -N1.NO_ADICION,
                 -N1.NO_DEDUCCI,
                 -N1.NO_NETO,
                 -N1.NO_DES_TRA,
                 -N1.NO_DIAS_AG,
                 -N1.NO_DIAS_VA,
                 -N1.NO_DOBLES,
                 -N1.NO_EXTRAS,
                 -N1.NO_FES_PAG,
                 -N1.NO_FES_TRA,
                 -N1.NO_HORA_CG,
                 -N1.NO_HORA_SG,
                 -N1.NO_HORAS,
                 -N1.NO_IMP_CAL,
                 -N1.NO_JORNADA,
                 -N1.NO_PER_CAL,
                 -N1.NO_PER_MEN,
                 -N1.NO_PERCEPC,
                 -N1.NO_TARDES,
                 -N1.NO_TRIPLES,
                 -N1.NO_TOT_PRE,
                 -N1.NO_VAC_TRA,
                 -N1.NO_X_CAL,
                 -N1.NO_X_ISPT,
                 -N1.NO_X_MENS,
                 -N1.NO_D_TURNO,
                 -N1.NO_DIAS_AJ,
                 -N1.NO_DIAS_AS,
                 -N1.NO_DIAS_CG,
                 -N1.NO_DIAS_EM,
                 -N1.NO_DIAS_FI,
                 -N1.NO_DIAS_FJ,
                 -N1.NO_DIAS_FV,
                 -N1.NO_DIAS_IN,
                 -N1.NO_DIAS_NT,
                 -N1.NO_DIAS_OT,
                 -N1.NO_DIAS_RE,
                 -N1.NO_DIAS_SG,
                 -N1.NO_DIAS_SS,
                 -N1.NO_DIAS_SU,
                 -N1.NO_HORA_PD,
                 N1.NO_LIQUIDA,
                 N1.NO_FUERA,
                 -N1.NO_EXENTAS,
                 N1.NO_FEC_PAG,
                 N1.NO_USR_PAG,
				 N1.CB_NIVEL0,
				 N1.CB_BAN_ELE,
				 -N1.NO_HORAPDT,
				 -N1.NO_HORASNT,
				 -N1.NO_DIAS_SI,
				 -N1.NO_DIAS_VJ,
				 -N1.NO_DIAS_PV,
				 N1.NO_SUP_OK,
				 N1.NO_FEC_OK,
				 N1.NO_HOR_OK,
				 N1.NO_ASI_INI,
				 N1.NO_ASI_FIN,
				 -N1.NO_DIAS_BA,
				 -N1.NO_PREV_GR,
				 N1.CB_PLAZA,
				 N1.NO_FEC_LIQ,
				 N1.CB_CTA_GAS,
				 N1.CB_CTA_VAL,
				 -N1.NO_PER_ISN,
         -N1.NO_DIAS_IH,
         -N1.NO_DIAS_ID,
         -N1.NO_DIAS_IT,
         -N1.NO_TOTAL_1,
         -N1.NO_TOTAL_2,
         -N1.NO_TOTAL_3,
         -N1.NO_TOTAL_4,
         -N1.NO_TOTAL_5,
         -N1.NO_FACT_BA,
         -N1.NO_DIAS_DT,
         -N1.NO_DIAS_FS,
         -N1.NO_DIAS_FT
          from DBO.NOMINA as N1 where
          ( N1.PE_YEAR = @YEARORIGINAL ) and
          ( N1.PE_TIPO = @TIPOORIGINAL ) and
          ( N1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( N1.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M1.CB_CODIGO,
                 M1.MO_ACTIVO,
                 M1.CO_NUMERO,
                 -M1.MO_IMP_CAL,
                 @USUARIO,
                 M1.MO_PER_CAL,
                 M1.MO_REFEREN,
                 -M1.MO_X_ISPT,
                 -M1.MO_PERCEPC,
                 -M1.MO_DEDUCCI
          from DBO.MOVIMIEN as M1 where
          ( M1.PE_YEAR = @YEARORIGINAL ) and
          ( M1.PE_TIPO = @TIPOORIGINAL ) and
          ( M1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( M1.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              CB_CODIGO,
                              FA_FEC_INI,
                              FA_DIA_HOR,
                              FA_MOTIVO,
                              FA_DIAS,
                              FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F1.CB_CODIGO,
                 F1.FA_FEC_INI,
                 F1.FA_DIA_HOR,
                 F1.FA_MOTIVO,
                 -F1.FA_DIAS,
                 -F1.FA_HORAS
          from DBO.FALTAS as F1 where
          ( F1.PE_YEAR = @YEARORIGINAL ) and
          ( F1.PE_TIPO = @TIPOORIGINAL ) and
          ( F1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( F1.CB_CODIGO = @EMPLEADO );
     end
end
GO

/* CR:2621, SP_CLAS_AUSENCIA: Modificar SP para incluir CB_TABLASS y salario temporal de tabulador ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: X*/
ALTER PROCEDURE SP_CLAS_AUSENCIA @Empleado Int, @Fecha DateTime AS
BEGIN
  SET NOCOUNT ON
	Declare @FEC_KAR DateTime
	Declare @PUESTO Char(6)
	Declare @CLASIFI Char(6)
	Declare @TURNO Char(6)
  Declare @NIVEL1 Char(6)
	Declare @NIVEL2 Char(6)
	Declare @NIVEL3 Char(6)
	Declare @NIVEL4 Char(6)
	Declare @NIVEL5 Char(6)
  Declare @NIVEL6 Char(6)
	Declare @NIVEL7 Char(6)
	Declare @NIVEL8 Char(6)
	Declare @NIVEL9 Char(6)
  Declare @SALARIO PesosDiario
  Declare @T_PUESTO Char(6)
	declare @T_CLASIFI Char(6)
	declare @T_TURNO Char(6)
  declare @T_NIVEL1 Char(6)
	declare @T_NIVEL2 Char(6)
	declare @T_NIVEL3 Char(6)
	declare @T_NIVEL4 Char(6)
	declare @T_NIVEL5 Char(6)
  declare @T_NIVEL6 Char(6)
	declare @T_NIVEL7 Char(6)
	declare @T_NIVEL8 Char(6)
	declare @T_NIVEL9 Char(6)
	declare @NOMINA smallint
  declare @TABLASS Char(1)
  declare @AUTOSAL Char(1)
  declare @TB_SAL PesosDiario
  declare @SalTemp Varchar(1)

 	select @FEC_KAR = CB_FEC_KAR,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
    @NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2,
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4,
		@NIVEL5 = CB_NIVEL5,
    @NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7,
		@NIVEL8 = CB_NIVEL8,
		@NIVEL9 = CB_NIVEL9,
    @SALARIO = CB_SALARIO,
 		@NOMINA = CB_NOMINA,
    @TABLASS = CB_TABLASS,
    @AUTOSAL = CB_AUTOSAL
  	from   	COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @FECHA < @FEC_KAR )
  	begin
 		select @PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI,
			@TURNO = CB_TURNO,
      @NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2,
			@NIVEL3 = CB_NIVEL3,
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
      @NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7,
			@NIVEL8 = CB_NIVEL8, 
			@NIVEL9 = CB_NIVEL9,
      @SALARIO = CB_SALARIO,
			@NOMINA = CB_NOMINA,
      @TABLASS = CB_TABLASS,
      @AUTOSAL = CB_AUTOSAL
      from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

 	select @FEC_KAR = AU_FECHA,
	       @T_PUESTO = CB_PUESTO,
	       @T_CLASIFI = CB_CLASIFI,
	       @T_TURNO = CB_TURNO,
         @T_NIVEL1 = CB_NIVEL1,
	       @T_NIVEL2 = CB_NIVEL2,
	       @T_NIVEL3 = CB_NIVEL3,
	       @T_NIVEL4 = CB_NIVEL4,
	       @T_NIVEL5 = CB_NIVEL5,
         @T_NIVEL6 = CB_NIVEL6,
	       @T_NIVEL7 = CB_NIVEL7,
	       @T_NIVEL8 = CB_NIVEL8,
	       @T_NIVEL9 = CB_NIVEL9
  	from CLASITMP where
        ( CB_CODIGO = @Empleado ) and
        ( AU_FECHA = @Fecha ) and
        ( US_COD_OK <> 0 );
        if ( @FEC_KAR is not NULL )
        begin
             if ( @T_PUESTO <> '' )
             begin
                  select @PUESTO = @T_PUESTO;
             end
             if ( @T_CLASIFI <> '' )
             begin
                  select @CLASIFI = @T_CLASIFI;
                  if ( @AUTOSAL = 'S' )
                  begin
	                    select @SalTemp = GL_FORMULA from GLOBAL where GL_CODIGO = 304;
	                    if ( @SalTemp = 'S' )
                      begin
                           select @TB_SAL = TB_SALARIO from CLASIFI where TB_CODIGO = @CLASIFI
                           if ( @TB_SAL > @SALARIO )
                           begin
                               select @SALARIO = @TB_SAL;
                           end
                      end
                  end
             end
             if ( @T_TURNO <> '' )
             begin
                  select @TURNO = @T_TURNO;
             end
             if ( @T_NIVEL1 <> '' )
             begin
                  select @NIVEL1 = @T_NIVEL1;
             end
             if ( @T_NIVEL2 <> '' )
             begin
                  select @NIVEL2 = @T_NIVEL2;
             end
             if ( @T_NIVEL3 <> '' )
             begin
                  select @NIVEL3 = @T_NIVEL3;
             end
             if ( @T_NIVEL4 <> '' )
             begin
                  select @NIVEL4 = @T_NIVEL4;
             end
             if ( @T_NIVEL5 <> '' )
             begin
                  select @NIVEL5 = @T_NIVEL5;
             end
             if ( @T_NIVEL6 <> '' )
             begin
                  select @NIVEL6 = @T_NIVEL6;
             end
             if ( @T_NIVEL7 <> '' )
             begin
                  select @NIVEL7 = @T_NIVEL7;
             end
             if ( @T_NIVEL8 <> '' )
             begin
                  select @NIVEL8 = @T_NIVEL8;
             end
             if ( @T_NIVEL9 <> '' )
             begin
                  select @NIVEL9 = @T_NIVEL9;
             end
        end

  	update AUSENCIA
  	set   CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9,
          CB_SALARIO = @SALARIO,
          CB_NOMINA  = @NOMINA,
          CB_TABLASS = @TABLASS
  	where CB_CODIGO = @Empleado and AU_FECHA = @Fecha;

        EXECUTE SP_CLAS_AUSENCIA_ADICIONAL @Empleado, @Fecha;
END
GO

/* CR:2621, SP_KARDEX_CLASIFICACION: Considerar clasificaciones temporales en ajustes desde Supervisores ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: X*/
ALTER PROCEDURE SP_KARDEX_CLASIFICACION
    @EMPLEADO INTEGER,
    @FECHA DATETIME,
    @CB_AUTOSAL CHAR(1) OUTPUT,
    @CB_CLASIFI CHAR(6) OUTPUT,
    @CB_CONTRAT CHAR(1) OUTPUT,
    @CB_TURNO CHAR(6) OUTPUT,
    @CB_PUESTO CHAR(6) OUTPUT,
    @CB_TABLASS CHAR(1) OUTPUT,
    @CB_NIVEL1 CHAR(6) OUTPUT,
    @CB_NIVEL2 CHAR(6) OUTPUT,
    @CB_NIVEL3 CHAR(6) OUTPUT,
    @CB_NIVEL4 CHAR(6) OUTPUT,
    @CB_NIVEL5 CHAR(6) OUTPUT,
    @CB_NIVEL6 CHAR(6) OUTPUT,
    @CB_NIVEL7 CHAR(6) OUTPUT,
    @CB_NIVEL8 CHAR(6) OUTPUT,
    @CB_NIVEL9 CHAR(6) OUTPUT,
    @CB_SALARIO PesosDiario OUTPUT,
    @CB_NOMINA SmallInt OUTPUT
AS
  SET NOCOUNT ON
	declare @FEC_KAR DateTime
  declare @T_PUESTO Char(6)
	declare @T_CLASIFI Char(6)
	declare @T_TURNO Char(6)
  declare @T_NIVEL1 Char(6)
	declare @T_NIVEL2 Char(6)
	declare @T_NIVEL3 Char(6)
	declare @T_NIVEL4 Char(6)
	declare @T_NIVEL5 Char(6)
  declare @T_NIVEL6 Char(6)
	declare @T_NIVEL7 Char(6)
	declare @T_NIVEL8 Char(6)
	declare @T_NIVEL9 Char(6)
  declare @TB_SAL PesosDiario
  declare @SalTemp Varchar(1)

  declare Temporal CURSOR FOR
	select CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
         CB_TURNO, CB_PUESTO, CB_TABLASS,
         CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
         CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
         CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
			   CB_SALARIO, CB_NOMINA
 	from KARDEX where ( CB_CODIGO = @Empleado ) and
                    ( CB_FECHA <= @Fecha  )
 	order by CB_FECHA desc, CB_NIVEL desc

	open Temporal

	fetch next from Temporal
  into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
       @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
       @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
       @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
       @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9,
		   @CB_SALARIO, @CB_NOMINA

	close Temporal
	deallocate Temporal

	if ( @CB_PUESTO is null )
	begin
		declare Temporal2 CURSOR FOR
		select  CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT,
            CB_TURNO, CB_PUESTO, CB_TABLASS,
            CB_NIVEL1, CB_NIVEL2, CB_NIVEL3,
            CB_NIVEL4, CB_NIVEL5, CB_NIVEL6,
            CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
			      CB_SALARIO, CB_NOMINA
 		from KARDEX where ( CB_CODIGO = @Empleado )
 		order by CB_FECHA desc, CB_NIVEL desc

		open Temporal2

		fetch next from Temporal2
    into @CB_AUTOSAL, @CB_CLASIFI, @CB_CONTRAT,
         @CB_TURNO, @CB_PUESTO, @CB_TABLASS,
         @CB_NIVEL1, @CB_NIVEL2, @CB_NIVEL3,
         @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6,
         @CB_NIVEL7, @CB_NIVEL8, @CB_NIVEL9,
		     @CB_SALARIO, @CB_NOMINA

		close Temporal2
		deallocate Temporal2
	end

 	select @FEC_KAR = AU_FECHA,
	       @T_PUESTO = CB_PUESTO,
	       @T_CLASIFI = CB_CLASIFI,
	       @T_TURNO = CB_TURNO,
         @T_NIVEL1 = CB_NIVEL1,
	       @T_NIVEL2 = CB_NIVEL2,
	       @T_NIVEL3 = CB_NIVEL3,
	       @T_NIVEL4 = CB_NIVEL4,
	       @T_NIVEL5 = CB_NIVEL5,
         @T_NIVEL6 = CB_NIVEL6,
	       @T_NIVEL7 = CB_NIVEL7,
	       @T_NIVEL8 = CB_NIVEL8,
	       @T_NIVEL9 = CB_NIVEL9
  	from CLASITMP where
        ( CB_CODIGO = @Empleado ) and
        ( AU_FECHA = @Fecha ) and
        ( US_COD_OK <> 0 );
    if ( @FEC_KAR is not NULL )
    begin
         if ( @T_PUESTO <> '' )
         begin
             select @CB_PUESTO = @T_PUESTO;
         end
         if ( @T_CLASIFI <> '' )
         begin
              select @CB_CLASIFI = @T_CLASIFI;
              if ( @CB_AUTOSAL = 'S' )
              begin
                  select @SalTemp = GL_FORMULA from GLOBAL where GL_CODIGO = 304;
                  if ( @SalTemp = 'S' )
                  begin
                       select @TB_SAL = TB_SALARIO from CLASIFI where TB_CODIGO = @CB_CLASIFI
                       if ( @TB_SAL > @CB_SALARIO )
                       begin
                          select @CB_SALARIO = @TB_SAL
                       end
                  end
              end
         end
         if ( @T_TURNO <> '' )
         begin
              select @CB_TURNO = @T_TURNO;
         end
         if ( @T_NIVEL1 <> '' )
         begin
              select @CB_NIVEL1 = @T_NIVEL1;
         end
         if ( @T_NIVEL2 <> '' )
         begin
              select @CB_NIVEL2 = @T_NIVEL2;
         end
         if ( @T_NIVEL3 <> '' )
         begin
              select @CB_NIVEL3 = @T_NIVEL3;
         end
         if ( @T_NIVEL4 <> '' )
         begin
              select @CB_NIVEL4 = @T_NIVEL4;
         end
         if ( @T_NIVEL5 <> '' )
         begin
              select @CB_NIVEL5 = @T_NIVEL5;
         end
         if ( @T_NIVEL6 <> '' )
         begin
              select @CB_NIVEL6 = @T_NIVEL6;
         end
         if ( @T_NIVEL7 <> '' )
         begin
              select @CB_NIVEL7 = @T_NIVEL7;
         end
         if ( @T_NIVEL8 <> '' )
         begin
              select @CB_NIVEL8 = @T_NIVEL8;
         end
         if ( @T_NIVEL9 <> '' )
         begin
              select @CB_NIVEL9 = @T_NIVEL9;
         end
    end
GO

/* CR:2621, ASISTENCIA_AGREGA: Considerar clasificaciones temporales en ajustes desde Supervisores ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: X*/
ALTER PROCEDURE ASISTENCIA_AGREGA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)
	DECLARE @CB_SALARIO PesosDiario
	DECLARE @CB_NOMINA SmallInt

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT,
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT,
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT,
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT, @CB_SALARIO OUTPUT,
		@CB_NOMINA OUTPUT

	insert into AUSENCIA
		( CB_CODIGO, AU_FECHA, AU_STATUS, HO_CODIGO, AU_HOR_MAN, AU_OUT2EAT,

		CB_PUESTO, CB_CLASIFI, CB_TURNO, CB_NIVEL1, CB_NIVEL2,
		CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7,
		CB_NIVEL8, CB_NIVEL9, CB_SALARIO, CB_NOMINA, CB_TABLASS )
	values	( @CB_CODIGO, @AU_FECHA, @AU_STATUS, @HO_CODIGO, @AU_HOR_MAN, @AU_OUT2EAT,
		@CB_PUESTO, @CB_CLASIFI, @CB_TURNO, @CB_NIVEL1, @CB_NIVEL2,
		@CB_NIVEL3, @CB_NIVEL4, @CB_NIVEL5, @CB_NIVEL6, @CB_NIVEL7,
		@CB_NIVEL8, @CB_NIVEL9, @CB_SALARIO, @CB_NOMINA, @CB_TABLASS )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, @MC_CHECADA1, @MC_CHECADA2, @MC_CHECADA3,
    @MC_CHECADA4, 'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
		@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',
		@US_CODIGO
END
GO

/* CR:2621, ASISTENCIA_MODIFICA: Considerar clasificaciones temporales en ajustes desde Supervisores ( PRO-748 Mejoras configuraci�n n�mina - Proyectos ) */
/* Patch 435 #Seq: X*/
ALTER PROCEDURE ASISTENCIA_MODIFICA
    @CB_CODIGO INTEGER,
    @AU_FECHA DATETIME,
    @HO_CODIGO CHAR(6),
    @AU_HOR_MAN CHAR(1),
    @AU_STATUS SMALLINT,
    @HRS_EXTRAS NUMERIC(15,2),
    @M_HRS_EXTRAS CHAR(4),
    @DESCANSO NUMERIC(15,2),
    @M_DESCANSO CHAR(4),
    @PER_CG NUMERIC(15,2),
    @M_PER_CG CHAR(4),
    @PER_CG_ENT NUMERIC(15,2),
    @M_PER_CG_ENT CHAR(4),
    @PER_SG NUMERIC(15,2),
    @M_PER_SG CHAR(4),
    @PER_SG_ENT NUMERIC(15,2),
    @M_PER_SG_ENT CHAR(4),
    @PRE_FUERA_JOR NUMERIC(15,2),
    @M_PRE_FUERA_JOR CHAR(4),
    @PRE_DENTRO_JOR NUMERIC(15,2),
    @M_PRE_DENTRO_JOR CHAR(4),  
    @CHECADA1 CHAR(4),
    @CHECADA2 CHAR(4),
    @CHECADA3 CHAR(4),
    @CHECADA4 CHAR(4),
    @MC_CHECADA1 CHAR(4),
    @MC_CHECADA2 CHAR(4),
    @MC_CHECADA3 CHAR(4),
    @MC_CHECADA4 CHAR(4),
    @US_CODIGO SMALLINT,
    @AU_OUT2EAT SMALLINT
AS
BEGIN
  SET NOCOUNT ON
  DECLARE @CB_AUTOSAL CHAR(1)
	DECLARE @CB_CONTRAT CHAR(1)
	DECLARE @CB_TABLASS CHAR(1)
	DECLARE @CB_PUESTO CHAR(6)
	DECLARE @CB_CLASIFI CHAR(6)
	DECLARE @CB_TURNO CHAR(6)
	DECLARE @CB_NIVEL1 CHAR(6)
	DECLARE @CB_NIVEL2 CHAR(6)
	DECLARE @CB_NIVEL3 CHAR(6)
	DECLARE @CB_NIVEL4 CHAR(6)
	DECLARE @CB_NIVEL5 CHAR(6)
	DECLARE @CB_NIVEL6 CHAR(6)
	DECLARE @CB_NIVEL7 CHAR(6)
	DECLARE @CB_NIVEL8 CHAR(6)
	DECLARE @CB_NIVEL9 CHAR(6)
	DECLARE @CB_SALARIO PesosDiario
	DECLARE @CB_NOMINA SmallInt

	execute SP_KARDEX_CLASIFICACION @CB_CODIGO, @AU_FECHA, @CB_AUTOSAL OUTPUT,
		@CB_CLASIFI OUTPUT, @CB_CONTRAT OUTPUT, @CB_TURNO OUTPUT,
		@CB_PUESTO OUTPUT, @CB_TABLASS OUTPUT, @CB_NIVEL1 OUTPUT, 
		@CB_NIVEL2 OUTPUT, @CB_NIVEL3 OUTPUT, @CB_NIVEL4 OUTPUT, 
		@CB_NIVEL5 OUTPUT, @CB_NIVEL6 OUTPUT, @CB_NIVEL7 OUTPUT,
		@CB_NIVEL8 OUTPUT, @CB_NIVEL9 OUTPUT, @CB_SALARIO OUTPUT,
		@CB_NOMINA OUTPUT

	update AUSENCIA set HO_CODIGO = @HO_CODIGO,
           AU_STATUS = @AU_STATUS,
		   AU_HOR_MAN = @AU_HOR_MAN,
           AU_OUT2EAT = @AU_OUT2EAT,
           CB_PUESTO = @CB_PUESTO,
		   CB_CLASIFI = @CB_CLASIFI,
           CB_TURNO = @CB_TURNO,
		   CB_NIVEL1 = @CB_NIVEL1,
           CB_NIVEL2 = @CB_NIVEL2,
		   CB_NIVEL3 = @CB_NIVEL3,
           CB_NIVEL4 = @CB_NIVEL4,
		   CB_NIVEL5 = @CB_NIVEL5,
           CB_NIVEL6 = @CB_NIVEL6,
		   CB_NIVEL7 = @CB_NIVEL7,
           CB_NIVEL8 = @CB_NIVEL8,
		   CB_NIVEL9 = @CB_NIVEL9,
		 CB_SALARIO = @CB_SALARIO,
		 CB_NOMINA = @CB_NOMINA,
     CB_TABLASS = @CB_TABLASS
	where ( CB_CODIGO = @CB_CODIGO ) and ( AU_FECHA = @AU_FECHA )

     	execute ASISTENCIA_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @CHECADA1,
		@CHECADA2, @CHECADA3, @CHECADA4, @MC_CHECADA1, @MC_CHECADA2, @MC_CHECADA3,
    @MC_CHECADA4,'N', @US_CODIGO

	execute AUTORIZACIONES_CREAR_CHECADAS @CB_CODIGO, @AU_FECHA, @HRS_EXTRAS,
		@M_HRS_EXTRAS, @DESCANSO, @M_DESCANSO, @PER_CG, @M_PER_CG, @PER_CG_ENT,
		@M_PER_CG_ENT, @PER_SG, @M_PER_SG, @PER_SG_ENT, @M_PER_SG_ENT, @PRE_FUERA_JOR,
	 	@M_PRE_FUERA_JOR, @PRE_DENTRO_JOR, @M_PRE_DENTRO_JOR,'N',@US_CODIGO
END
GO

/* CR:2621, RECALCULA_KARDEX: Corregir problema de que no cambia la fecha de kardex COLABORA.CB_FEC_KAR cuando cambia la tabla de prestaciones */
/* Patch 435 #Seq: X*/
ALTER PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);
   declare @kPLAZA    FolioGrande;
   declare @kNOMINA   SMALLINT;
   declare @kRECONTR  Booleano;
   declare @kFEC_COV  Fecha;

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare @ePLAZA    FolioGrande;
   declare @eFEC_PLA  Fecha;
   declare @eNOMINA   SMALLINT;
   declare @eFEC_NOM  Fecha;
   declare @eRECONTR  Booleano;
   declare @eFEC_COV  Fecha;
   declare @eFEC_PRE  Fecha;

   declare CursorKardex CURSOR SCROLL_LOCKS FOR
   select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
                CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
                CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
                CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
                CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
                CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
                CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S, CB_PLAZA,
                CB_NOMINA,  CB_RECONTR, CB_FEC_COV
   from Kardex
   where CB_CODIGO = @Empleado
   order by cb_fecha,cb_nivel

      	SET @FechaVacia = '12/30/1899';
        SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;
        SET @eFEC_PLA = @FechaVacia;
        SET @eFEC_NOM = @FechaVacia;
        SET @eRECONTR = 'S';
        SET @eFEC_COV = @FechaVacia;
        SET @eFEC_PRE = @FechaVacia;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
      @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
      @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
      @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
      @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
      @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
      @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
      @kNOMINA,  @kRECONTR, @kFEC_COV


	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END


	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
                             SET @kOLD_INT = @eOLD_INT;
	                     SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eFEC_PTO  = @kFECHA;
                        if ( @kTIPO = 'PLAZA' AND @eAUTOSAL = 'S' )
                             SET @kCLASIFI = @eCLASIFI;
                        else
			     SET @eCLASIFI  = @kCLASIFI;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) OR ( @kTIPO = 'PLAZA' ))
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                                if ( @kNIVEL1 <> '' )
                                    SET @kMOD_NV1 = 'S';
                                else
                                    SET @kMOD_NV1 = 'N';
                                if ( @kNIVEL2 <> '' )
                                    SET @kMOD_NV2 = 'S';
                                else
                                    SET @kMOD_NV2 = 'N';
                                if ( @kNIVEL3 <> '' )
                                    SET @kMOD_NV3 = 'S';
                                else
                                    SET @kMOD_NV3 = 'N';
                                if ( @kNIVEL4 <> '' )
                                    SET @kMOD_NV4 = 'S';
                                else
                                    SET @kMOD_NV4 = 'N';
                                if ( @kNIVEL5 <> '' )
                                    SET @kMOD_NV5 = 'S';
                                else
                                    SET @kMOD_NV5 = 'N';
                                if ( @kNIVEL6 <> '' )
                                    SET @kMOD_NV6 = 'S';
                                else
                                    SET @kMOD_NV6 = 'N';
                                if ( @kNIVEL7 <> '' )
                                    SET @kMOD_NV7 = 'S';
                                else
                                    SET @kMOD_NV7 = 'N';
                                if ( @kNIVEL8 <> '' )
                                    SET @kMOD_NV8 = 'S';
                                else
                                    SET @kMOD_NV8 = 'N';
                                if ( @kNIVEL9 <> '' )
                                    SET @kMOD_NV9 = 'S';
                                else
                                    SET @kMOD_NV9 = 'N';

			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
                        SET @kMOD_NV1 = 'N';
                        SET @kMOD_NV2 = 'N';
                        SET @kMOD_NV3 = 'N';
                        SET @kMOD_NV4 = 'N';
                        SET @kMOD_NV5 = 'N';
                        SET @kMOD_NV6 = 'N';
                        SET @kMOD_NV7 = 'N';
                        SET @kMOD_NV8 = 'N';
                        SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
    BEGIN
			SET @eTABLASS = @kTABLASS;
      SET @eFEC_PRE = @kFECHA;
    END
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TIPNOM' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eNOMINA  = @kNOMINA;
			SET @eFEC_NOM = @kFECHA;
		END
		else
			SET @kNOMINA  = @eNOMINA;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
			SET @eFEC_COV = @kFEC_COV;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
			SET @kFEC_COV = @eFEC_COV;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
			SET @ePLAZA    =  0;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END

		if ( @kTIPO = 'ALTA'  OR @kTIPO = 'CIERRE' OR @kTIPO = 'PLAZA' )
		BEGIN
			SET @ePLAZA    = @kPLAZA;
			SET @eFEC_PLA  = @kFECHA;
		END
		else
		BEGIN
			SET @kPLAZA    = @ePLAZA;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'BAJA' )
		BEGIN
			if ( @kTIPO = 'ALTA' )
                        begin
                             SET @kRECONTR = 'S';
                             SET @eRECONTR = 'S';
                        end
                        else
                        begin
                             SET @eRECONTR = @kRECONTR;
                        end
		END
		else
		BEGIN
			SET @kRECONTR = @eRECONTR;
		END

		update KARDEX
     		set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S,
      CB_PLAZA   = @kPLAZA,
      CB_NOMINA  = @kNOMINA,
      CB_RECONTR = @kRECONTR,
      CB_FEC_COV = @kFEC_COV
		where CURRENT OF CursorKardex

		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
                        @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
                        @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
                        @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
                        @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
                        @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
                        @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S, @kPLAZA,
                        @kNOMINA,  @kRECONTR, @kFEC_COV
	END

	Close CursorKardex
	Deallocate CursorKardex

	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_TUR;
	if ( @eFEC_PLA > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PLA;
	if ( @eFEC_NOM > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NOM;
	if ( @eFEC_PRE > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PRE;

	update COLABORA
	set CB_AUTOSAL = @eAUTOSAL,
            CB_ACTIVO  = @eACTIVO,
            CB_CLASIFI = @eCLASIFI,
            CB_CONTRAT = @eCONTRAT,
            CB_FAC_INT = @eFAC_INT,
            CB_FEC_ANT = @eFEC_ANT,
            CB_FEC_CON = @eFEC_CON,
            CB_FEC_ING = @eFEC_ING,
            CB_FEC_INT = @eFEC_INT,
            CB_FEC_REV = @eFEC_REV,
            CB_FEC_SAL = @eFEC_SAL,
            CB_FEC_BAJ = @eFEC_BAJ,
            CB_FEC_BSS = @eFEC_BSS,
            CB_MOT_BAJ = @eMOT_BAJ,
            CB_NIVEL1  = @eNIVEL1,
            CB_NIVEL2  = @eNIVEL2,
            CB_NIVEL3  = @eNIVEL3,
            CB_NIVEL4  = @eNIVEL4,
            CB_NIVEL5  = @eNIVEL5,
            CB_NIVEL6  = @eNIVEL6,
            CB_NIVEL7  = @eNIVEL7,
            CB_NIVEL8  = @eNIVEL8,
            CB_NIVEL9  = @eNIVEL9,
            CB_NOMNUME = @eNOMNUME,
            CB_NOMTIPO = @eNOMTIPO,
            CB_NOMYEAR = @eNOMYEAR,
            CB_PATRON  = @ePATRON,
            CB_PER_VAR = @ePER_VAR,
            CB_PUESTO  = @ePUESTO,
            CB_SAL_INT = @eSAL_INT,
            CB_SALARIO = @eSALARIO,
            CB_TABLASS = @eTABLASS,
            CB_TURNO   = @eTURNO,
            CB_ZONA_GE = @eZONA_GE,
            CB_OLD_SAL = @eOLD_SAL,
            CB_OLD_INT = @eOLD_INT,
            CB_PRE_INT = @ePRE_INT,
            CB_SAL_TOT = @eSAL_TOT,
            CB_TOT_GRA = @eTOT_GRA,
            CB_TIP_REV = @eTIP_REV,
            CB_RANGO_S = @eRANGO_S,
            CB_FEC_NIV = @eFEC_NIV,
            CB_FEC_PTO = @eFEC_PTO,
            CB_FEC_TUR = @eFEC_TUR,
            CB_FEC_KAR = @eFEC_KAR,
            CB_PLAZA   = @ePLAZA,
            CB_FEC_PLA = @eFEC_PLA,
            CB_NOMINA  = @eNOMINA,
            CB_FEC_NOM = @eFEC_NOM,
            CB_RECONTR = @eRECONTR,
            CB_FEC_COV = @eFEC_COV
	where ( CB_CODIGO = @Empleado );
END
GO

/* CR:2653, Mejora Confidencialidad - Funcion FN_SPLIT */
/* Patch 435 #Seq: 1*/
CREATE FUNCTION FN_SPLIT(@String Formula, @Delimiter Codigo1)
returns @temptable TABLE (items varchar(255))       
as       
begin       
    declare @idx int       
    declare @slice varchar(255)       

    select @idx = 1       
        if len(@String)<1 or @String is null  return       

    while @idx!= 0       
    begin       
        set @idx = charindex(@Delimiter,@String)       
        if @idx!=0       
            set @slice = left(@String,@idx - 1)       
        else       
            set @slice = @String       

        if(len(@slice)>0)  
            insert into @temptable(Items) values(@slice)       

        set @String = right(@String,len(@String) - @idx)       
        if len(@String) = 0 break       
    end   
return       
end
GO

/* CR:2653, Mejora Confidencialidad - procedure SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA */
/* Patch 435 #Seq: 1*/
CREATE procedure SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA ( @EN_CODIGO FolioChico, @CODIGO Codigo,  @ListaNIVEL0 Formula)
as
begin
		set NOCOUNT ON

    	DELETE FROM CONF_TAB where CT_CODIGO = @CODIGO and CT_TABLA = @EN_CODIGO

		INSERT INTO CONF_TAB ( CT_TABLA, CT_CODIGO, CT_NIVEL0 )
		select @EN_CODIGO, @CODIGO, Items
		from dbo.FN_SPLIT(  @ListaNIVEL0, ',' )
end
GO

/* CR:2653, Mejora Confidencialidad - procedure SP_ACTUALIZA_CONDIFDENCIALIDAD */
/* Patch 435 #Seq: 1*/
CREATE procedure SP_ACTUALIZA_CONDIFDENCIALIDAD( @EN_CODIGO FolioChico, @Codigo Codigo )
as
begin 
		declare @NIVEL0 Formula	
		declare @ListaNIVEL0 Formula

		set @ListaNIVEL0 = '' 
		
		select
			@ListaNivel0 = 
			(
			case @EN_CODIGO 
				when 59 then (select PU_NIVEL0 from PUESTO where PU_CODIGO = @Codigo )  				
				when 69 then (select TU_NIVEL0 from TURNO where TU_CODIGO = @Codigo )		
				when 27 then (select HO_NIVEL0 from HORARIO where HO_CODIGO = @Codigo )  								
				
				when 9 then  (select TB_NIVEL0 from CLASIFI	 where TB_CODIGO = @Codigo )
				when 63 then (select TB_NIVEL0 from SSOCIAL	 where TB_CODIGO = @Codigo )
				when 12 then (select TB_NIVEL0 from CONTRATO where TB_CODIGO = @Codigo )
				when 41 then (select TB_NIVEL0 from NIVEL1	 where TB_CODIGO = @Codigo )
				when 42 then (select TB_NIVEL0 from NIVEL2	 where TB_CODIGO = @Codigo )
				when 43 then (select TB_NIVEL0 from NIVEL3	 where TB_CODIGO = @Codigo )
				when 44 then (select TB_NIVEL0 from NIVEL4	 where TB_CODIGO = @Codigo )
				when 45 then (select TB_NIVEL0 from NIVEL5	 where TB_CODIGO = @Codigo )				
				when 46 then (select TB_NIVEL0 from NIVEL6	 where TB_CODIGO = @Codigo )
				when 47 then (select TB_NIVEL0 from NIVEL7	 where TB_CODIGO = @Codigo )
				when 48 then (select TB_NIVEL0 from NIVEL8	 where TB_CODIGO = @Codigo )
				when 49 then (select TB_NIVEL0 from NIVEL9	 where TB_CODIGO = @Codigo )
				when 62 then (select TB_NIVEL0 from RPATRON	 where TB_CODIGO = @Codigo )
				when 220 then (select TB_NIVEL0 from COLONIA where TB_CODIGO = @Codigo )
				when 15 then (select TB_NIVEL0 from ENTIDAD	 where TB_CODIGO = @Codigo )
				when 64 then (select TB_NIVEL0 from TAHORRO	 where TB_CODIGO = @Codigo )
				when 67 then (select TB_NIVEL0 from TPRESTA	 where TB_CODIGO = @Codigo )
				
				when 129 then (select TP_NIVEL0 from TPERIODO where TP_TIPO = CAST( @Codigo as int ) )

				else ''
			end
			)	
			
		set @ListaNIVEL0 = coalesce( @ListaNivel0, '' )
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA @EN_CODIGO, @Codigo, @ListaNivel0
	
end;
GO




/* CR:2681, Mejoras WF 2013 - Agrega FN_WFGetVacFechasPeriodos */
/* Patch 435 #Seq: 1*/
CREATE FUNCTION FN_WFGetVacFechasPeriodos (
@CB_CODIGO NumeroEmpleado,
@FechaVacIni Fecha,
@FechaVacReg Fecha,
@YEAR Anio,
@TPO_NOMINA NominaTipo
)
  RETURNS @TABLE_RESULT
  TABLE  (
		A_FECHA Datetime, PE_YEAR int, PE_TIPO int, PE_NUMERO int
)
AS
BEGIN

declare @FechaVacFin Fecha
declare @currDate	 Fecha


set @currDate = @FechaVacIni
set @FechaVacFin = @FechaVacReg -1


WHILE @currDate <= @FechaVacFin
BEGIN

	set @TPO_NOMINA = dbo.SP_KARDEX_CB_NOMINA( @currDate, @CB_CODIGO )

    INSERT INTO @TABLE_RESULT (A_FECHA, PE_YEAR, PE_TIPO, PE_NUMERO)
	SELECT TOP 1   @currDate, PE_YEAR, PE_TIPO, PE_NUMERO  FROM PERIODO where
			( PE_YEAR >= @YEAR )
			AND ( PE_TIPO = @TPO_NOMINA )
			AND ( PE_NUMERO < 200 )
			AND ( PE_STATUS < 5 )
			AND ( PE_CAL = 'S' )
		    AND ( @currDate between PE_ASI_INI and PE_ASI_FIN )
	order by PE_YEAR, PE_NUMERO

    SET @currDate = @currDate + 1
END

return

end
GO

/* CR:2681, Mejoras WF 2013 - Agrega FN_WFGetVacacionesPeriodos */
/* Patch 435 #Seq: 1*/
CREATE FUNCTION FN_WFGetVacacionesPeriodos
(
@CB_CODIGO NumeroEmpleado,
@FechaVacIni Fecha,
@FechaVacReg Fecha,
@YEAR Anio,
@TPO_NOMINA NominaTipo,
@NUM_NOMINA NominaNumero
)
  RETURNS @TABLE_RESULT
  TABLE  (
		Orden int identity(1,1), PE_YEAR int, PE_TIPO int, PE_NUMERO int, VA_FEC_INI Datetime, VA_FEC_FIN Datetime, DIAS int
)
as
begin

insert into @TABLE_RESULT (PE_YEAR, PE_TIPO, PE_NUMERO , VA_FEC_INI , VA_FEC_FIN, DIAS )
select PE_YEAR, PE_TIPO, PE_NUMERO, MIN(A_FECHA) AS VA_FEC_INI, MAX(A_FECHA)+1 as VA_FEC_FIN,  count(*) as DIAS  FROM
dbo.FN_WFGetVacFechasPeriodos( @CB_CODIGO, @FechaVacIni, @FechaVacReg, @YEAR, @TPO_NOMINA )
group by PE_YEAR, PE_TIPO, PE_NUMERO

/*pendiente actualizar el ultimo registro si es que faltaron periodos*/
declare @diasEsperados int
declare @diasACrear int

set @diasEsperados =  datediff( day,  @FechaVacIni ,@FechaVacReg )
select @diasACrear = sum(DIAS) from @TABLE_RESULT

declare @mismaNomina booleano

set @mismaNomina = 'N'
if ( select count(1) from @TABLE_RESULT where
	@YEAR = PE_YEAR and
	@TPO_NOMINA = PE_TIPO and
	@NUM_NOMINA = PE_NUMERO and Orden=1) > 0
	set @mismaNomina = 'S'

if ( @diasEsperados <> @diasACrear ) or ( @mismaNomina = 'N' )
begin
	delete from @TABLE_RESULT

	insert into @TABLE_RESULT (PE_YEAR, PE_TIPO, PE_NUMERO , VA_FEC_INI , VA_FEC_FIN, DIAS ) VALUES
		(@YEAR, @TPO_NOMINA, @NUM_NOMINA, @FechaVacIni, @FechaVacReg, @diasEsperados )

end;

return

end
GO

IF  EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID(N'SP_WFGetPeriodoActualDatosNoAfectados'))
 begin
     drop procedure SP_WFGetPeriodoActualDatosNoAfectados;
 end
GO

/* CR:2681, Mejoras WF 2013 - Agrega SP_WFGetPeriodoActualDatosNoAfectados */
/* Patch 435 #Seq: 1*/
CREATE procedure SP_WFGetPeriodoActualDatosNoAfectados(@YEAR Anio, @TPO_NOMINA NominaTipo, @TomarInicio Booleano = 'N', @FechaVacIni Fecha = '1899-12-30' )
as
begin
	DECLARE
	@MAX_PERIODO INT,
	@STATUS INT
	SET @STATUS = 0;
	SET @MAX_PERIODO = 0;

    if ( @TomarInicio = 'S')
	begin
		SELECT @MAX_PERIODO= (SELECT ISNULL(MIN(PE_NUMERO),0)
		FROM PERIODO
		WHERE ( PE_YEAR = @YEAR )
			AND ( PE_TIPO = @TPO_NOMINA )
			AND ( PE_NUMERO < 200 )
			AND ( PE_STATUS < 5 )
			AND ( @FechaVacIni between PE_ASI_INI and PE_ASI_FIN )
			AND ( PE_CAL = 'S' ) 	)
	end

	if ( @TomarInicio = 'N') or ( @MAX_PERIODO <= 0 )
	begin
		SELECT @MAX_PERIODO= (SELECT ISNULL(MAX(PE_NUMERO),1)
		FROM PERIODO
		WHERE ( PE_YEAR = @YEAR )
			AND ( PE_TIPO = @TPO_NOMINA )
			AND ( PE_NUMERO < 200 )
			AND ( PE_STATUS <> @STATUS )
			AND ( PE_STATUS < 5 ) )
	end

	SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ,
		PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC,
		PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM,
		PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG,
		US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
		PE_PERIODO_TEXTO = 'De ' + (case
								when datepart(dd,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_INI) )
								else convert(varchar(2), datepart(dd,PE_FEC_INI))
								end) + '/'
							 + (case
								when datepart(mm,PE_FEC_INI)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_INI) )
								else convert(varchar(2), datepart(mm,PE_FEC_INI))
								end)+ '/'
							 +            convert(varchar(4), datepart(yyyy,PE_FEC_INI)) +
								' a ' + (case
								when datepart(dd,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(dd,PE_FEC_FIN) )
								else convert(varchar(2), datepart(dd,PE_FEC_FIN))
								end) + '/'
							 + (case
								when datepart(mm,PE_FEC_FIN)< 10 then '0' + convert(varchar(2), datepart(mm,PE_FEC_FIN) )
								else convert(varchar(2), datepart(mm,PE_FEC_FIN))
								end)+ '/'
							 +            convert(varchar(4), datepart(yyyy,PE_FEC_FIN))
	FROM PERIODO
	WHERE ( PE_YEAR = @YEAR )
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO =  @MAX_PERIODO )
		AND ( PE_STATUS < 5 )
end
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - Funcion de Vista de Matriz de Habilidades */
/* Patch 435 #Seq: 1*/
ALTER function F_GET_MTRZ_HAB()
 RETURNS
  @Matriz TABLE (
	CB_CODIGO int,
	CP_CODIGO char(6),
	RP_FEC_INI Datetime,
	CC_CODIGO char(6),
	NC_NIVEL  int,
	RC_FEC_INI DateTime,
	PC_PESO Decimal(15,2),
	EC_NIVEL Smallint
   )
AS
BEGIN
   declare @PERFIL char(6);
   declare @Empleado int;
   declare @Compentencia char(6);
   declare @Nivel smallint;
   declare @PerFecIni DateTime;
   declare @Peso Decimal(15,2);

   declare cPERFILs cursor for Select CP_CODIGO from C_PERFIL ;
   
	OPEN cPERFILs
	FETCH NEXT FROM cPERFILs
	into @PERFIL
	WHILE @@FETCH_STATUS = 0
	begin
	     declare cCompt cursor for select CP.CC_CODIGO,CP.NC_NIVEL,CP.RP_FEC_INI,CP.PC_PESO from C_PER_COMP CP
								   left outer join C_COMPETEN CC on CC.CC_CODIGO = CP.CC_CODIGO
								   left outer join C_PERFIL PP on PP.CP_CODIGO = CP.CP_CODIGO	 	
							   where CP.CP_CODIGO = @PERFIL and PP.CP_ACTIVO = 'S' and CC.CC_ACTIVO = 'S';
		 open cCompt;
		 FETCH NEXT FROM cCompt
	     into @Compentencia,@Nivel,@PerFecIni,@Peso

		 WHILE @@FETCH_STATUS = 0
		 begin
		 	  insert into @Matriz (cb_codigo,CP_CODIGO,RP_FEC_INI,CC_CODIGO,NC_NIVEL,RC_FEC_INI,PC_PESO,EC_NIVEL)
			  select 
					C.CB_CODIGO,
					@PERFIL as PERFIL,
					@PerFecIni as FechaPerfil,
					isnull(CCE.CC_CODIGO,@Compentencia) as CC_CODIGO,
					isnull(CCE.NC_NIVEL,@Nivel)as NC_NIVEL,
					CRC.RC_FEC_INI AS RC_FEC_INI,
					(CASE WHEN ( CCE.NC_NIVEL >= @Nivel)THEN 1 Else 0 end ) as PC_PESO,
					IsNull(CCE.NC_NIVEL,0)as EC_NIVEL
					from COLABORA C
					left outer join C_EVA_COMP CCE on ( CCE.CB_CODIGO = C.CB_CODIGO ) and ( CCE.CC_CODIGO = @Compentencia )
					left outer join C_REV_COMP CRC on ( CRC.CC_CODIGO = @Compentencia )
					where CB_ACTIVO = 'S'
			  FETCH NEXT FROM cCompt
			  into @Compentencia,@Nivel,@PerFecIni,@Peso
		 end
		 close cCompt
	     deallocate cCompt
		 FETCH NEXT FROM cPERFILs
		 into @PERFIL
    end
	close cPERFILs
    deallocate cPERFILs
	RETURN
END
GO

/* CR:2493, Validaci�n de movimientos afilatorios con Diferente Registro Patronal */
/* Patch 435 #Seq: 1*/
ALTER PROCEDURE SP_ACTUALIZAR_MOVIMIENTO_IDSE(
@TipoSUA    Status, 
@NSS        Descripcion, 
@Fecha            Fecha, 
@LoteIDSE   DescLarga,
@FechaIDSE  Fecha,
@US_CODIGO  Usuario,
@REG_PATRON DescLarga,
@CB_CODIGO  NumeroEmpleado output,
@RESPUESTA  Status output
) 
as
BEGIN 
      SET NOCOUNT ON 
      declare 
            @CB_TIPO Codigo;
      DECLARE
          @CB_PATRON NumeroPatronal;
      declare 
            @SSValido NumeroEmpleado;
            
      set @CB_CODIGO = 0
      set @Respuesta = -10
      
      SET @SSValido = ( SELECT Count(CB_CODIGO)  FROM COLABORA WHERE CB_SEGSOC = @NSS ) ;
      SET @SSValido = COALESCE ( @SSValido, 0 )

      IF ( @SSValido <= 0 )
      BEGIN
            SET @RESPUESTA = -10
      END
      ELSE
      BEGIN
            IF ( @TIPOSUA = 2 )
                  SET @CB_TIPO = 'BAJA'
            ELSE
            IF ( @TIPOSUA = 7 )
                  SET @CB_TIPO = 'CAMBIO'
            ELSE
            IF ( @TIPOSUA = 8 )
                  SET @CB_TIPO = 'ALTA'
            ELSE
                  SET @CB_TIPO = '';

            IF ( @CB_TIPO = '' )
            BEGIN
                  SET @RESPUESTA = -30
            END
            ELSE
            BEGIN
                  SELECT @CB_PATRON = TB_CODIGO FROM RPATRON WHERE TB_NUMREG = @REG_PATRON;
                  set @CB_CODIGO = ( SELECT TOP 1 CB.CB_CODIGO from COLABORA CB join KARDEX K on K.CB_CODIGO = CB.CB_CODIGO  where CB_SEGSOC = @NSS and  K.CB_PATRON = @CB_PATRON and K.CB_TIPO = @CB_TIPO and K.CB_FECHA_2 = @FECHA)

                  IF ( SELECT COUNT(*) FROM KARDEX WHERE CB_TIPO = @CB_TIPO AND CB_CODIGO = @CB_CODIGO  AND CB_FECHA_2 =  @FECHA AND CB_PATRON = @CB_PATRON ) > 0
                  BEGIN
                        UPDATE KARDEX
                        SET
                             CB_LOT_IDS = @LOTEIDSE,
                              CB_FEC_IDS = @FECHAIDSE
                        WHERE
                             CB_TIPO = @CB_TIPO AND CB_CODIGO = @CB_CODIGO  AND CB_FECHA_2 =  @FECHA AND CB_PATRON = @CB_PATRON

                        SET @RESPUESTA = 0  -- OK
                  END
                  ELSE
                        SET  @RESPUESTA = 20 -- ADVERTENCIA 2
            end
      end
END
GO

/* CR:2729 CRM-161039-Infonavit con Creditos Diferentes*/
/* Patch 435 #Seq: X*/
ALTER PROCEDURE RECALCULA_INFONAVIT(@Empleado NumeroEmpleado) AS
BEGIN
	SET NOCOUNT ON
	declare @CB_INFTIPO Status
	declare @CB_INFCRED	Descripcion
	declare @CB_INFTASA	DescINFO	
	declare @CB_INFDISM Booleano
	declare @KI_FECHA Fecha
	declare @KI_TIPO Status
	declare @CB_INF_ANT Fecha	
	
	declare @Old_InfTipo Status
	set @Old_InfTipo = 0

	declare @Old_Infcred Descripcion
	set @Old_Infcred = ''

	declare @Old_Inftasa DescINFO
	set @Old_Inftasa = 0
	
	declare @Old_AplicaDis Booleano
	set @Old_AplicaDis = 'N'

	declare @FecIniCredito Fecha
	set @FecIniCredito = '12/30/1899'

	declare @CB_INFACT Booleano
	set @CB_INFACT = 'N' 

	declare @Old_INF_ANT Fecha
	set @Old_INF_ANT = '12/30/1899'


	declare CursorInfonavit CURSOR FOR
	select CB_INFTIPO, CB_INFCRED, CB_INFTASA, CB_INFDISM,
		  KI_FECHA, KI_TIPO, CB_INF_ANT
 	from KARINF
	where CB_CODIGO = @Empleado
	order by KI_FECHA

	Open CursorInfonavit
	Fetch NEXT from CursorInfonavit
	into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO, @CB_INF_ANT
	

	if ( @KI_TIPO <> 0 )
	BEGIN
		Close CursorInfonavit
		Deallocate CursorInfonavit
		RAISERROR( 'Empleado #%d, ERROR en Kardex Infonavit: Movimiento(s) previo(s) a la alta de Infonavit', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
		/* Inicio */
		if ( @KI_TIPO in ( 0  ) ) 
		begin
			set @CB_INFACT = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_Infcred = @CB_INFCRED
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			set @Old_INF_ANT = @CB_INF_ANT
		
		end
		
		/* Reinicio */
		
		if ( @KI_TIPO = 2 ) 
		begin
			set @CB_INFACT = 'S'
			set @Old_AplicaDis = 'S'
			set @FecIniCredito = @KI_FECHA
			set @Old_Infcred = @CB_INFCRED
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
			set @Old_INF_ANT = @CB_INF_ANT
		end
		 

		/* Suspensi�n */
		if ( @KI_TIPO = 1 )
		BEGIN
			set @CB_INFACT = 'N'
		END

		/* Cambios */
		
		/* Cambio de Tipo de Descuento */
		if ( @KI_TIPO = 3 )
		begin
			set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		end

		/* Cambio de Valor de Descuento */
		if ( @KI_TIPO = 4 )
		begin
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM

		end

		/* Cambio de # de Cr�dito */
		if ( @KI_TIPO = 5 )
		begin
			set @Old_Infcred = @CB_INFCRED
      set @Old_InfTipo = @CB_INFTIPO
			set @Old_Inftasa = @CB_INFTASA
			set @Old_AplicaDis = @CB_INFDISM
		end


		update KARINF set CB_INFTIPO = @Old_inftipo,
					    CB_INFCRED = @Old_Infcred,
	 			    	    CB_INFTASA = @Old_Inftasa,
	 			         CB_INFDISM = @Old_AplicaDis,
						 CB_INF_ANT = @Old_INF_ANT
		where CURRENT OF CursorInfonavit

		Fetch NEXT from CursorInfonavit
		into	@CB_INFTIPO, @CB_INFCRED, @CB_INFTASA, @CB_INFDISM, @KI_FECHA, @KI_TIPO, @CB_INF_ANT
	END

	update COLABORA set CB_INF_INI = @FecIniCredito,
					CB_INFTIPO = @Old_InfTipo,
					CB_INFCRED = @Old_InfCred,
					CB_INFTASA = @Old_InfTasa,
					CB_INFACT = @CB_INFACT,
					CB_INFDISM = @Old_AplicaDis,
					CB_INF_ANT = @Old_INF_ANT
	where ( CB_CODIGO = @Empleado )

	close CursorInfonavit;
	deallocate CursorInfonavit;
END
GO

/* CR:2681 Mejoras WorkFlow 2013.*/
/* Patch 435 #Seq: X*/
ALTER PROCEDURE DBO.SP_WFGETEMPLEADOVACACIONES(@EMPLEADO NUMEROEMPLEADO , @CONFIDEN CODIGO, @USUARIO USUARIO, @EMPLEADO_L NUMEROEMPLEADO)
AS BEGIN  
 DECLARE @FECHA DESCRIPCION  
 SET @FECHA = LTRIM(RTRIM(STR( MONTH(GETDATE()) )))+'/'+ LTRIM(RTRIM(STR( DAY(GETDATE()) )))+'/'+ LTRIM(RTRIM(STR( YEAR(GETDATE()))) )  
  
 SELECT  CB_CODIGO, CB_APE_PAT+' '+CB_APE_MAT+', '+CB_NOMBRES AS PRETTYNAME, C.CB_ACTIVO, C.CB_FEC_ING, C.CB_FEC_BAJ,  
  C.CB_DER_FEC, C.CB_DER_PAG, C.CB_DER_GOZ, C.CB_V_GOZO, C.CB_V_PAGO, C.CB_FEC_ANT, C.CB_FEC_VAC,  
  CB_TOT_PAG = CB_DER_PAG - CB_V_PAGO,  
  CB_TOT_GOZ = CB_DER_GOZ - CB_V_GOZO, 0.0 AS CB_SUB_PAG, 0.0 AS CB_SUB_GOZ, C.CB_ACTIVO,  
  0.0 AS CB_PRO_PAG, 0.0 AS CB_PRO_GOZ, 0.0 AS CB_PEN_PAG, 0.0 AS CB_PEN_GOZ, C.CB_TABLASS,  
  PORC_PRIMA = DBO.GETVACAPROPORCIONAL(C.CB_CODIGO, GETDATE()), TU_VACA_HA = ISNULL(T.TU_VACA_HA,0)  
  FROM COLABORA C  
  LEFT OUTER JOIN PUESTO P ON( P.PU_CODIGO = CB_PUESTO )  
  LEFT OUTER JOIN TURNO T ON ( T.TU_CODIGO = CB_TURNO )  
  LEFT OUTER JOIN CLASIFI CLAS ON (CLAS.TB_CODIGO = C.CB_CLASIFI)  
  LEFT OUTER JOIN CONTRATO CNT ON (CNT.TB_CODIGO = C.CB_CONTRAT)  
  LEFT OUTER JOIN RPATRON RPAT ON (RPAT.TB_CODIGO = C.CB_PATRON)  
  LEFT OUTER JOIN NIVEL1 N1 ON ( N1.TB_CODIGO = C.CB_NIVEL1 )  
  LEFT OUTER JOIN NIVEL2 N2 ON ( N2.TB_CODIGO = C.CB_NIVEL2 )  
  LEFT OUTER JOIN NIVEL3 N3 ON ( N3.TB_CODIGO = C.CB_NIVEL3 )  
  LEFT OUTER JOIN NIVEL4 N4 ON ( N4.TB_CODIGO = C.CB_NIVEL4 )  
  LEFT OUTER JOIN NIVEL5 N5 ON ( N5.TB_CODIGO = C.CB_NIVEL5 )  
  LEFT OUTER JOIN NIVEL6 N6 ON ( N6.TB_CODIGO = C.CB_NIVEL6 )  
  LEFT OUTER JOIN NIVEL7 N7 ON ( N7.TB_CODIGO = C.CB_NIVEL7 )  
  LEFT OUTER JOIN NIVEL8 N8 ON ( N8.TB_CODIGO = C.CB_NIVEL8 )  
  LEFT OUTER JOIN NIVEL9 N9 ON ( N9.TB_CODIGO = C.CB_NIVEL9 )  
  LEFT OUTER JOIN EXTRA1 X1 ON ( X1.TB_CODIGO = C.CB_G_TAB_1 )  
  LEFT OUTER JOIN EXTRA2 X2 ON ( X2.TB_CODIGO = C.CB_G_TAB_2 )  
  LEFT OUTER JOIN EXTRA3 X3 ON ( X3.TB_CODIGO = C.CB_G_TAB_3 )  
  LEFT OUTER JOIN EXTRA4 X4 ON ( X4.TB_CODIGO = C.CB_G_TAB_4 )  
  LEFT OUTER JOIN ENTIDAD E ON (E.TB_CODIGO = C.CB_ENT_NAC)  
  WHERE
( 
  (@CONFIDEN = '' OR CB_NIVEL0=@CONFIDEN)  
  AND (
            C.CB_CODIGO=@EMPLEADO AND 
            (
                        @USUARIO = -1 
                        OR 
                (
                      SELECT COUNT(*) FROM SP_LISTA_EMPLEADOS( @FECHA, @USUARIO ) LISTA  
                               LEFT OUTER JOIN COLABORA C  
                               ON LISTA.CB_CODIGO = C.CB_CODIGO  
                               WHERE LISTA.CB_CODIGO = @EMPLEADO  
                             AND (@CONFIDEN ='' OR CB_NIVEL0 = @CONFIDEN)
                 ) > 0
      
                  ) 
       ) 
  )  
  OR ( CB_CODIGO = @EMPLEADO_L AND CB_CODIGO = @EMPLEADO)  
END
GO


/* SP_PATCH : Actualizar Extension de Reportes Excel con Presentacion  (1/3) */
/* Patch 435 #Seq: */
create procedure ACTUALIZA_EXTENSION_EXCEL
as
begin 
set nocount on 
update REPORTE set RE_ARCHIVO =  RTrim(RE_ARCHIVO) + 'X'  where 
(
	( RE_TIPO = 1  and RE_PFILE = 4 ) OR 
	( RE_TIPO <> 1  and RE_PFILE = 7 ) 
)
and 
(
	upper( RE_ARCHIVO ) like '%.XLS'
)

end 
GO

/* SP_PATCH : Actualizar Extension de Reportes Excel con Presentacion  (2/3) */
/* Patch 435 #Seq: */
execute  ACTUALIZA_EXTENSION_EXCEL
GO

/* SP_PATCH : Actualizar Extension de Reportes Excel con Presentacion  (3/3) */
drop procedure ACTUALIZA_EXTENSION_EXCEL
GO

/*****************************************/
/************ TRIGGERS *******************/
/*****************************************/

/* CR:2690, Recalcular plazas */
/* Patch 435 #Seq: X*/
ALTER TRIGGER TU_COLABORA ON COLABORA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  declare @NewCodigo NumeroEmpleado, @OldCodigo NumeroEmpleado ,@NewActivo Booleano,@ValidaPlaza Booleano;
  IF UPDATE( CB_CODIGO )
  BEGIN
	select @NewCodigo = CB_CODIGO from Inserted;
        select @OldCodigo = CB_CODIGO from Deleted;

	UPDATE NOMINA	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE LIQ_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE WORKS	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE CED_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
        if ( @OldCodigo > 0 )
        begin
             update EXPEDIEN set EXPEDIEN.CB_CODIGO = @NewCodigo
             where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
             update PLAZA set PLAZA.CB_CODIGO = @NewCodigo
             where ( PLAZA.CB_CODIGO = @OldCodigo );
        end
  END
  IF UPDATE( CB_PLAZA )
  BEGIN
       declare @NewPlaza FolioGrande;
       declare @OldPLaza FolioGrande;
       select @NewPlaza = CB_PLAZA from Inserted;
       select @OldPLaza = CB_PLAZA from Deleted;
       select @NewCodigo = CB_CODIGO from Inserted;
       update PLAZA set CB_CODIGO = 0 where ( PL_FOLIO = @OldPLaza ) and ( CB_CODIGO = @NewCodigo );
       update PLAZA set CB_CODIGO = @NewCodigo where ( PL_FOLIO = @NewPlaza );
  END

  IF UPDATE(CB_NIVEL1) OR
     UPDATE(CB_NIVEL2) OR
     UPDATE(CB_NIVEL3) OR
     UPDATE(CB_NIVEL4) OR
     UPDATE(CB_NIVEL5) OR
     UPDATE(CB_NIVEL6) OR
     UPDATE(CB_NIVEL7) OR
     UPDATE(CB_NIVEL8) OR
     UPDATE(CB_NIVEL9)
   BEGIN
   	DECLARE @NivelSupervisores Formula
	DECLARE @US_CODIGO Usuario

   	select @US_CODIGO = US_CODIGO from Inserted;   	
   	SET @NivelSupervisores =  DBO.SP_GET_NIVEL_SUPERVISORES()

     	IF  DBO.SP_TIENE_ENROLAMIENTO_SUPERVISORES() = 'S' AND @NivelSupervisores <> '' AND @US_CODIGO > 0 
     	BEGIN 
     		DECLARE @CB_NIVELSup Codigo     		     		
     		DECLARE @US_JEFE Usuario
     		
     		SELECT  @CB_NIVELSup =
		CASE  @NivelSupervisores 
			WHEN '1' THEN (SELECT TOP 1 CB_NIVEL1 FROM Inserted) 
			WHEN '2' THEN (SELECT TOP 1 CB_NIVEL2 FROM Inserted)
			WHEN '3' THEN (SELECT TOP 1 CB_NIVEL3 FROM Inserted)
			WHEN '4' THEN (SELECT TOP 1 CB_NIVEL4 FROM Inserted)
			WHEN '5' THEN (SELECT TOP 1 CB_NIVEL5 FROM Inserted)
			WHEN '6' THEN (SELECT TOP 1 CB_NIVEL6 FROM Inserted)
			WHEN '7' THEN (SELECT TOP 1 CB_NIVEL7 FROM Inserted)
			WHEN '8' THEN (SELECT TOP 1 CB_NIVEL8 FROM Inserted)
			WHEN '9' THEN (SELECT TOP 1 CB_NIVEL9 FROM Inserted)
		END

     		IF ( @CB_NIVELSup <> '' )
     		BEGIN
	     		SET @US_JEFE = DBO.SP_GET_USUARIO_SUPERVISOR(@CB_NIVELSup, @NivelSupervisores)
			EXEC SP_AJUSTA_USUARIO_SUPERVISOR  @US_CODIGO, @US_JEFE
     		END
     	END
   END
   if Update(CB_ACTIVO)
   begin
        select @NewActivo = CB_ACTIVO from Inserted;
        select @NewCodigo = CB_CODIGO from Inserted;
		select @ValidaPlaza = GL_FORMULA from GLOBAL where GL_CODIGO = 128;
		if (@NewActivo = 'N' AND ( @ValidaPlaza != 'S' or @ValidaPlaza is Null))
		begin
			 update PLAZA set CB_CODIGO = 0 WHERE CB_CODIGO = @NewCodigo
		end
   end	
END
GO

/* CR:2570, Trigger para revisar la actualizacion de Parentesco y Orden */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_PARIENTE ON PARIENTE AFTER UPDATE AS
BEGIN
    SET NOCOUNT ON;
    declare @NewFolio int;
    declare @OldFolio int;
    declare @NewRelacio int;
    declare @OldRelacio int;
    declare @Empleado int;

    select @NewFolio = PA_FOLIO,@NewRelacio = PA_RELACIO,@Empleado = CB_CODIGO from Inserted;

    select @OldFolio = PA_FOLIO,@OldRelacio = PA_RELACIO from Deleted;

    If ( Update (PA_RELACIO) ) and ( ( select count(*) from EMP_POLIZA where PA_FOLIO = @OldFolio and PA_RELACIO = @OldRelacio and CB_CODIGO = @Empleado and EP_ASEGURA = 1 ) > 0 )
    begin
       raiserror('No Se Puede Modificar El Tipo de Parentesco a Parientes con Seguros de Gastos M�dicos',16,1);
    end
    else
    begin
         If Update (PA_FOLIO)
          update EMP_POLIZA set PA_FOLIO = @NewFolio where PA_FOLIO = @OldFolio and PA_RELACIO = @OldRelacio and CB_CODIGO = @Empleado;
    end
END
GO

/* CR:2570, Trigger para revisar el borrado de parientes con SGM */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_PARIENTE ON PARIENTE AFTER DELETE AS
BEGIN
    SET NOCOUNT ON;
    declare @OldFolio int;
    declare @OldRelacio int;
    declare @Empleado int;

    select @OldFolio = PA_FOLIO,@OldRelacio = PA_RELACIO,@Empleado = CB_CODIGO from Deleted;

    if ( (select count(*) from EMP_POLIZA where PA_FOLIO = @OldFolio and PA_RELACIO = @OldRelacio and CB_CODIGO = @Empleado and EP_ASEGURA = 1 ) > 0 )
    begin
       raiserror('No Se Puede Borrar Parientes con Seguros de Gastos M�dicos',16,1);
    end
END
GO

/* CR:2691, Protecci�n Civil - Trigger de cambios de COLABORA a EXPEDIEN */
/* Patch 435 #Seq: 1*/
create trigger TU_EXPEDIEN on EXPEDIEN after update as
begin
  set nocount on;
 if update( EX_TSANGRE ) or update( EX_ALERGIA )
 begin
	if @@nestlevel < 32
	begin
		update COLABORA set CB_TSANGRE = EX_TSANGRE, CB_ALERGIA = EX_ALERGIA
		from Inserted EX where  COLABORA.CB_CODIGO = EX.CB_CODIGO and EX.EX_TIPO = 0
	end
 end
end
GO

/* CR:2691, Protecci�n Civil - Trigger de cambios de EXPEDIEN a COLABORA */
/* Patch 435 #Seq: 1*/
create trigger TU_COLABORA_MEDICOS
on COLABORA after update as
begin
  set nocount on;  
 if update( CB_TSANGRE ) or update( CB_ALERGIA ) 
 begin 
	if @@nestlevel < 32 
	begin
		update EXPEDIEN set EX_TSANGRE = CB_TSANGRE, EX_ALERGIA = CB_ALERGIA 
		from Inserted CB where  EXPEDIEN.CB_CODIGO = CB.CB_CODIGO and EXPEDIEN.EX_TIPO = 0
	end
 end
end
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_PUESTO_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_PUESTO_CONF ON PUESTO AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  /* Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( PU_NIVEL0 )
  BEGIN
		declare @PU_NIVEL0 Formula
		declare @PU_CODIGO Codigo
		select  @PU_NIVEL0 = PU_NIVEL0, @PU_CODIGO =  PU_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 59, @PU_CODIGO, @PU_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_PUESTO_CONF */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_PUESTO ON PUESTO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  update PUESTO set PUESTO.PU_REPORTA = Deleted.PU_REPORTA
  from PUESTO, Deleted
  where ( PUESTO.PU_REPORTA = Deleted.PU_CODIGO );

  delete from CONF_TAB where CT_TABLA = 59 and CT_CODIGO in ( select PU_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_TURNO_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_TURNO_CONF ON TURNO AFTER UPDATE, INSERT AS
BEGIN
  SET NOCOUNT ON;
  /* Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TU_NIVEL0 )
  BEGIN
		declare @TU_NIVEL0 Formula
		declare @TU_CODIGO Codigo
		select  @TU_NIVEL0 = TU_NIVEL0, @TU_CODIGO =  TU_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 69, @TU_CODIGO, @TU_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_TURNO */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_TURNO ON TURNO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 69 and CT_CODIGO in ( select TU_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_HORARIO_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_HORARIO_CONF ON HORARIO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  /* Esto es un proceso invidiual , no se realizar� por medio de update general */
  IF UPDATE( HO_NIVEL0 )
  BEGIN
		declare @HO_NIVEL0 Formula
		declare @HO_CODIGO Codigo
		select  @HO_NIVEL0 = HO_NIVEL0, @HO_CODIGO =  HO_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 27, @HO_CODIGO, @HO_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_HORARIO */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_HORARIO ON HORARIO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 27 and CT_CODIGO in ( select HO_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_CLASIFI_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_CLASIFI_CONF ON CLASIFI AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 9, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_CLASIFI */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_CLASIFI ON CLASIFI AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 9 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_SSOCIAL_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_SSOCIAL_CONF ON SSOCIAL AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 63, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_SSOCIAL */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_SSOCIAL ON SSOCIAL AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 63 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_CONTRATO_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_CONTRATO_CONF ON CONTRATO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 12, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_CONTRATO */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_CONTRATO ON CONTRATO AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  delete from CONF_TAB where CT_TABLA = 12 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL1_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL1_CONF ON NIVEL1 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 41, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL1 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL1 ON NIVEL1 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '1', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 41 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL2_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL2_CONF ON NIVEL2 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 42, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL2 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL2 ON NIVEL2 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '2', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 42 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL3_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL3_CONF ON NIVEL3 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 43, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL3 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL3 ON NIVEL3 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '3', @TB_CODIGO, @US_CODIGO
       END
       
       delete from CONF_TAB where CT_TABLA = 43 and CT_CODIGO in ( select TB_CODIGO from Deleted ) 
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL4_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL4_CONF ON NIVEL4 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 44, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL4 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL4 ON NIVEL4 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '4', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 44 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL5_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL5_CONF ON NIVEL5 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;

  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 45, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL5 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL5 ON NIVEL5 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '5', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 45 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL6_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL6_CONF ON NIVEL6 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 46, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL6 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL6 ON NIVEL6 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '6', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 46 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL7_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL7_CONF ON NIVEL7 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 ) 
  BEGIN 
		declare @TB_NIVEL0 Formula	
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted	
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 47, @TB_CODIGO, @TB_NIVEL0
  END 
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL7 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL7 ON NIVEL7 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '7', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 47 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL8_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL8_CONF ON NIVEL8 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 48, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL8 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL8 ON NIVEL8 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '8', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 48 and CT_CODIGO in ( select TB_CODIGO from Deleted )

END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_NIVEL9_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_NIVEL9_CONF ON NIVEL9 AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  declare @TB_CODIGO  Codigo;
  /*Esto es un proceso invidiual , no se realizar� por medio de update general*/
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 49, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_NIVEL9 */
/* Patch 435 #Seq: 1*/
ALTER TRIGGER TD_NIVEL9 ON NIVEL9 AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;

       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')

       IF (@TB_CODIGO <> '')
       BEGIN
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '9', @TB_CODIGO, @US_CODIGO
       END

       delete from CONF_TAB where CT_TABLA = 49 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_RPATRON_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_RPATRON_CONF ON RPATRON AFTER UPDATE, INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 62, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_RPATRON */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_RPATRON ON RPATRON AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 62 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_COLONIA_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_COLONIA_CONF ON COLONIA AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 220, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_COLONIA */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_COLONIA ON COLONIA AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 220 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_ENTIDAD_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_ENTIDAD_CONF ON ENTIDAD AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 15, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_ENTIDAD */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_ENTIDAD ON ENTIDAD AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 15 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_TAHORRO_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_TAHORRO_CONF ON TAHORRO AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 64, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_TAHORRO */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_TAHORRO ON TAHORRO AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 64 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_TAHORRO */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_TPRESTA_CONF ON TPRESTA AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_NIVEL0 )
  BEGIN
		declare @TB_NIVEL0 Formula
		declare @TB_CODIGO Codigo
		select  @TB_NIVEL0 = TB_NIVEL0, @TB_CODIGO =  TB_CODIGO from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 67, @TB_CODIGO, @TB_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_TPRESTA */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_TPRESTA ON TPRESTA AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 67 and CT_CODIGO in ( select TB_CODIGO from Deleted )
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TU_TPERIODO_CONF */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TU_TPERIODO_CONF ON TPERIODO AFTER UPDATE,INSERT AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TP_NIVEL0 )
  BEGIN
		declare @TP_NIVEL0 Formula
		declare @TP_CODIGO Codigo
		select  @TP_NIVEL0 = TP_NIVEL0, @TP_CODIGO = LTRIM(RTRIM( STR( TP_TIPO ) )) from Inserted
		exec SP_ACTUALIZA_CONFIDENCIALIDAD_POR_TABLA 129, @TP_CODIGO, @TP_NIVEL0
  END
END
GO

/* CR:2653, Mejora Confidencialidad - TRIGGER TD_TPERIODO */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TD_TPERIODO ON TPERIODO AFTER DELETE AS
BEGIN
       SET NOCOUNT ON;
       delete from CONF_TAB where CT_TABLA = 129 and CT_CODIGO in ( select LTRIM(RTRIM(STR(TP_TIPO))) from Deleted )
END
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - TRIGGER Competencias */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TI_C_COMPETEN ON C_COMPETEN AFTER INSERT AS
BEGIN
SET NOCOUNT ON;

declare @Competencia Varchar(6);
declare @Fecha DateTime;

Select @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()));
select @Competencia = CC_CODIGO from Inserted;

insert into C_REV_COMP (CC_CODIGO,RC_REVISIO,RC_OBSERVA,US_CODIGO,RC_FEC_INI)
	values (@Competencia,'1','Agregada por Sistema',1,@Fecha);

END;
GO

/* CR:2711, Mejora Capacitacion en Base a Competencias - TRIGGER Perfiles */
/* Patch 435 #Seq: 1*/
CREATE TRIGGER TI_C_PERFIL ON C_PERFIL AFTER INSERT AS
BEGIN
SET NOCOUNT ON;
	
declare @Perfil Varchar(6);
declare @Fecha DateTime;

Select @Fecha = DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()));
select @Perfil = CP_CODIGO from Inserted;

insert into C_REV_PERF (CP_CODIGO,RP_REVISIO,RP_OBSERVA,US_CODIGO,RP_FEC_INI)
	values (@Perfil,'1','Agregada por Sistema',1,@Fecha);

END;
GO

/* Habilitar Triggers */
ENABLE TRIGGER ALL on DBO.EXPEDIEN;
GO
ENABLE TRIGGER ALL on DBO.COLABORA;
GO