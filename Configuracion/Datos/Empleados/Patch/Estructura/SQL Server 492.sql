IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'TimbradoConcilia_Aplicar') AND type IN ( N'P', N'PC' ))
    drop procedure TimbradoConcilia_Aplicar
go


create procedure TimbradoConcilia_Aplicar( 	@PE_YEAR Anio,
	@PE_TIPO NominaTipo,
	@PE_NUMERO NominaNumero, 	
	@US_CODIGO	Usuario, 
	@US_FEC_MOD Fecha, 
	@PC_NUMERO FolioGrande, 
	@Error		VARCHAR( 255 ) OUTPUT
)
as
begin 
	SET NOCOUNT ON
	set @Error = '' 
	select row_number() over (order by CB_CODIGO) i,  CB_CODIGO
	into #TmpNominasAplicar
	from NOM_CONCIL
	where PE_YEAR = @PE_YEAR and PE_TIPO = @PE_TIPO and PE_NUMERO = @PE_NUMERO and NOM_CONCIL.NC_APLICADO = 'N' and NOM_CONCIL.NC_RESULT <> 0 


	declare @i FolioGrande 
	declare @iMax FolioGrande 

	select @iMax = COUNT(*) from #TmpNominasAplicar

	declare @CB_CODIGO  NumeroEmpleado 

	set @i= 1 
	while @i <= @iMax 
	begin 
		select @CB_CODIGO = CB_CODIGO from #TmpNominasAplicar where i = @i 
		exec TimbradoConcilia_AplicarUnEmpleado @PE_YEAR, @PE_TIPO, @PE_NUMERO, @CB_CODIGO, @US_CODIGO, @US_FEC_MOD, @PC_NUMERO
		set @i= @i+1 
	end 

	exec TimbradoConcilia_CalcularStatusPeriodo @PE_YEAR, @PE_TIPO, @PE_NUMERO, 0, 0
	drop table #TmpNominasAplicar
	return 
end
GO

/* US #19852 */
UPDATE R1 SET R1.RE_DERECHO = R2.RE_DERECHO FROM R_ENT_ACC R1 INNER JOIN R_ENT_ACC R2 
ON R1.CM_CODIGO = R2.CM_CODIGO AND R1.GR_CODIGO = R2.GR_CODIGO
WHERE R1.EN_CODIGO = 387 AND R2.EN_CODIGO = 10
GO