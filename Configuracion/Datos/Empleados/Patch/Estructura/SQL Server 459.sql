/* #15664: Integrar cambios hechos para Terminales GTI MIFARE */
if not exists (select * from syscolumns where id=object_id('COLABORA') and name='CB_MIFARE')
	alter table COLABORA add CB_MIFARE booleano null;
go

update COLABORA set CB_MIFARE = 'N' where CB_MIFARE is null;
go

alter table COLABORA alter column CB_MIFARE booleano not null;
go

/* #15664: Integrar cambios hechos para Terminales GTI MIFARE */
if not exists (select * from syscolumns where id=object_id('INVITA') and name='IV_MIFARE')
	alter table INVITA add IV_MIFARE booleano null;
go

update INVITA set IV_MIFARE = 'N' where IV_MIFARE is null;
go

alter table INVITA alter column IV_MIFARE booleano not null;
go
