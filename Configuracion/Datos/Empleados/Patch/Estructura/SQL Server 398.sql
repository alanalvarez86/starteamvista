/*****************************************/
/****  Tablas ****************************/
/*****************************************/

/* Tabla AUSENCIA: Salario Diario ( Quincenales 2 ) */
/* Patch 397 # Seq: 1 Agregado */
alter table AUSENCIA add CB_SALARIO PesosDiario
GO

/* Tabla NOMINA: Fecha Inicial de Prenomina ( Quincenales 2 ) */
/* Patch 397 # Seq: 2 Agregado */
alter table NOMINA add NO_ASI_INI Fecha
GO

/* Tabla NOMINA: Fecha Final de Prenomina ( Quincenales 2 ) */
/* Patch 397 # Seq: 3 Agregado */
alter table NOMINA add NO_ASI_FIN Fecha
GO

/* Tabla TPERIODO: Tipo de D�as base ( Quincenales 2 ) */
/* Patch 397 # Seq: 4 Agregado */
alter table TPERIODO add TP_DIAS_BT Status
GO

/* Tabla TPERIODO: D�as base ( Quincenales 2 ) */
/* Patch 397 # Seq: 5 Agregado */
alter table TPERIODO add TP_TEMP_D7 DiasFrac
GO

/* Tabla TPERIODO: Cambiar Domian a D�as base (1/7) ( Quincenales 2 ) */
/* Patch 398 # Seq: 6 Agregado */
update TPERIODO set TP_TEMP_D7 = TP_DIAS_7
GO

/* Tabla TPERIODO: Cambiar Domian a D�as base (2/7) ( Quincenales 2 ) */
/* Patch 398 # Seq: 7 Agregado */
exec sp_unbindefault 'TPERIODO.TP_DIAS_7'
GO

/* Tabla TPERIODO: Cambiar Domian a D�as base (3/7) ( Quincenales 2 ) */
/* Patch 398 # Seq: 8 Agregado */
alter table TPERIODO drop column TP_DIAS_7
GO

/* Tabla TPERIODO: Cambiar Domian a D�as base (4/7) ( Quincenales 2 ) */
/* Patch 398 # Seq: 9 Agregado */
alter table TPERIODO add TP_DIAS_7 Tasa
GO

/* Tabla TPERIODO: Cambiar Domian a D�as base (5/7) ( Quincenales 2 ) */
/* Patch 398 # Seq: 10 Agregado */
update TPERIODO set TP_DIAS_7 = TP_TEMP_D7
GO

/* Tabla TPERIODO: Cambiar Domian a D�as base (6/7) ( Quincenales 2 ) */
/* Patch 398 # Seq: 11 Agregado */
exec sp_unbindefault 'TPERIODO.TP_TEMP_D7'
GO

/* Tabla TPERIODO: Cambiar Domian a D�as base (7/7) ( Quincenales 2 ) */
/* Patch 398 # Seq: 12 Agregado */
alter table TPERIODO drop column TP_TEMP_D7
GO

/* Tabla TPERIODO: Formula para calcular Dias Ordinarios ( Quincenales 2 ) */
/* Patch 397 # Seq: 13 Agregado */
alter table TPERIODO add TP_DIAS_EV Formula
GO

/* Tabla TPERIODO: � Sumar Jornada de Horarios ? ( Quincenales 2 ) */
/* Patch 397 # Seq: 14 Agregado */
alter table TPERIODO add TP_HORASJO Booleano
GO

/* Tabla TURNO: Dias Base ( Quincenales 2 ) */
/* Patch 397 # Seq: 15 Agregado */
alter table TURNO add TU_DIAS_BA Tasa
GO

/* Tabla NOMINA: Dias Base ( Quincenales 2 ) */
/* Patch 397 # Seq: 16 Agregado */
alter table NOMINA add NO_DIAS_BA Tasa
GO

/* Tabla TMPNOM: Dias Base ( Quincenales 2 ) */
/* Patch 397 # Seq: 17 Agregado */
alter table TMPNOM add NO_DIAS_BA DiasFrac
GO

/* Tabla TMPNOM: Dias Subsidio Incapacidad ( Quincenales 2 ) */
/* Patch 397 # Seq: 18 Agregado */
alter table TMPNOM add NO_DIAS_SI DiasFrac
GO

/* Tabla TMPNOM: Horas no trabajadas ( Quincenales 2 ) */
/* Patch 397 # Seq: 19 Agregado */
alter table TMPNOM add NO_HORASNT Horas
GO

/* Tabla TMPNOM: Horas de Prima Dominical Total ( Quincenales 2 ) */
/* Patch 397 # Seq: 20 Agregado */
alter table TMPNOM add NO_HORAPDT Horas
GO

/* Tabla CONCEPTO: � Cambiar de columna si es negativo ? ( Quincenales 2 ) */
/* Patch 397 # Seq: 21 Agregado */
alter table CONCEPTO add CO_CAMBIA Booleano
GO

/* Inicialiar AUSENCIA.CB_SALARIO */
/* Patch 398 # Seq: 22 Agregado */
update AUSENCIA
set CB_SALARIO = ISNULL( ( select NOMINA.CB_SALARIO from NOMINA where
                 ( NOMINA.CB_CODIGO = AUSENCIA.CB_CODIGO ) and
                 ( NOMINA.PE_YEAR = AUSENCIA.PE_YEAR ) and
                 ( NOMINA.PE_TIPO = AUSENCIA.PE_TIPO ) and
                 (  NOMINA.PE_NUMERO = AUSENCIA.PE_NUMERO ) ), 0 )
where ( AUSENCIA.CB_SALARIO is NULL )
GO

/* Inicialiar NOMINA.NO_ASI_INI */
/* Patch 398 # Seq: 23 Agregado */
update NOMINA
set NO_ASI_INI = ISNULL( ( select MIN( AUSENCIA.AU_FECHA ) from AUSENCIA where
                 ( AUSENCIA.CB_CODIGO = NOMINA.CB_CODIGO ) and
                 ( AUSENCIA.PE_YEAR = NOMINA.PE_YEAR ) and
                 ( AUSENCIA.PE_TIPO = NOMINA.PE_TIPO ) and
                 ( AUSENCIA.PE_NUMERO = NOMINA.PE_NUMERO ) ),
                 ( select P.PE_FEC_INI from PERIODO P where
                 ( P.PE_YEAR = NOMINA.PE_YEAR ) and
                 ( P.PE_TIPO = NOMINA.PE_TIPO ) and
                 ( P.PE_NUMERO = NOMINA.PE_NUMERO ) ) )
where ( NOMINA.NO_ASI_INI is NULL )
GO

/* Inicialiar NOMINA.NO_ASI_FIN */
/* Patch 398 # Seq: 24 Agregado */
update NOMINA
set NO_ASI_FIN = ISNULL( ( select MAX( AUSENCIA.AU_FECHA ) from AUSENCIA where
                 ( AUSENCIA.CB_CODIGO = NOMINA.CB_CODIGO ) and
                 ( AUSENCIA.PE_YEAR = NOMINA.PE_YEAR ) and
                 ( AUSENCIA.PE_TIPO = NOMINA.PE_TIPO ) and
                 ( AUSENCIA.PE_NUMERO = NOMINA.PE_NUMERO ) ),
                 ( select P.PE_FEC_FIN from PERIODO P where
                 ( P.PE_YEAR = NOMINA.PE_YEAR ) and
                 ( P.PE_TIPO = NOMINA.PE_TIPO ) and
                 ( P.PE_NUMERO = NOMINA.PE_NUMERO ) ) )
where ( NOMINA.NO_ASI_FIN is NULL )
GO

/* Inicialiar TURNO.TU_DIAS_BA */
/* Patch 398 # Seq: 25 Agregado */
update TURNO set TU_DIAS_BA = ( select TPERIODO. TP_DIAS_7 from TPERIODO where ( TPERIODO.TP_TIPO = TURNO.TU_NOMINA ) )
where ( TURNO.TU_DIAS_BA is NULL )
GO

/* Inicialiar NOMINA.NO_DIAS_BA */
/* Patch 398 # Seq: 26 Agregado */
update NOMINA set NO_DIAS_BA = ( select TURNO.TU_DIAS_BA from TURNO where ( TURNO.TU_CODIGO = NOMINA.CB_TURNO ) )
where ( NOMINA.NO_DIAS_BA is NULL )
GO

/* Inicialiar TPERIODO.TP_DIAS_BT */
/* Patch 398 # Seq: 27 Agregado */
update TPERIODO set TP_DIAS_BT = 0 where ( TP_DIAS_BT is NULL )
GO

/* Inicialiar TPERIODO.TP_DIAS_EV */
/* Patch 398 # Seq: 28 Agregado */
update TPERIODO set TP_DIAS_EV = '' where ( TP_DIAS_EV is NULL )
GO

/* Inicialiar TPERIODO.TP_HORASJO */
/* Patch 398 # Seq: 29 Agregado */
update TPERIODO set TP_HORASJO = ( select GLOBAL.GL_FORMULA from GLOBAL where ( GL_CODIGO = 79 ) )
where ( TPERIODO.TP_HORASJO is NULL )
GO

/* Inicialiar CONCEPTO.CO_CAMBIA */
/* Patch 398 # Seq: 30 Agregado */
update CONCEPTO set CO_CAMBIA = 'N' where ( CO_CAMBIA is NULL )
GO

/* AGREGA_CONCEPTO_SISTEMA: Agrega un concepto de sistema cuando no existe ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 31 Agregado */
create procedure AGREGA_CONCEPTO_SISTEMA( @Concepto Smallint, @Descripcion VARCHAR(30) )
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = @Concepto );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( @Concepto, @Descripcion, 'S' );
     end
end
GO

/* AGREGA_CONCEPTOS_SISTEMA: Agregar conceptos sistema nuevos 1119,1120,1214,1215 ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 32 Agregado */
create procedure AGREGA_CONCEPTOS_SISTEMA
as
begin
     SET NOCOUNT ON;
     execute AGREGA_CONCEPTO_SISTEMA 1119, 'Subsidio de incapacidades';
     execute AGREGA_CONCEPTO_SISTEMA 1120, 'Base para ordinario';
     execute AGREGA_CONCEPTO_SISTEMA 1214, 'No trabajadas';
     execute AGREGA_CONCEPTO_SISTEMA 1215, 'Prima Dominical Total';
end
GO

/* AGREGA_CONCEPTOS_SISTEMA: Ejecutar para agregar conceptos sistema nuevos 1119,1120,1214,1215 ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 33 Agregado */
execute AGREGA_CONCEPTOS_SISTEMA
GO

/* AGREGA_CONCEPTOS_SISTEMA: Borrar ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 34 Agregado */
drop procedure AGREGA_CONCEPTOS_SISTEMA
GO

/* AGREGA_CONCEPTO_SISTEMA: Borrar ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 35 Agregado */
drop procedure AGREGA_CONCEPTO_SISTEMA
GO

/*****************************************/
/**** Foreign Keys ***********************/
/*****************************************/

/*****************************************/
/**** Views ******************************/
/*****************************************/

/*****************************************/
/****  Triggers  *************************/
/*****************************************/

/**************************************************/
/* STORED PROCEDURES ******************************/
/**************************************************/

/* SP_CLAS_AUSENCIA_ADICIONAL: 'Hook' para poder clasificar tarjeta en casos especiales ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 150 Agregado */
CREATE PROCEDURE SP_CLAS_AUSENCIA_ADICIONAL @Empleado Int, @Fecha DateTime AS
GO

/* SP_CLAS_AUSENCIA: Agregar Salario Diario y llamada a SP_CLAS_AUSENCIA_ADICIONAL ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 151 Modificado */
ALTER PROCEDURE SP_CLAS_AUSENCIA @Empleado Int, @Fecha DateTime AS
    SET NOCOUNT ON
	Declare @FEC_KAR DateTime
	Declare @PUESTO Char(6)
	Declare @CLASIFI Char(6)
	Declare @TURNO Char(6)
    Declare @NIVEL1 Char(6)
	Declare @NIVEL2 Char(6)
	Declare @NIVEL3 Char(6)
	Declare @NIVEL4 Char(6)
	Declare @NIVEL5 Char(6)
    Declare @NIVEL6 Char(6)
	Declare @NIVEL7 Char(6)
	Declare @NIVEL8 Char(6)
	Declare @NIVEL9 Char(6)
    Declare @SALARIO PesosDiario

   	declare @T_PUESTO Char(6)
	declare @T_CLASIFI Char(6)
	declare @T_TURNO Char(6)
    declare @T_NIVEL1 Char(6)
	declare @T_NIVEL2 Char(6)
	declare @T_NIVEL3 Char(6)
	declare @T_NIVEL4 Char(6)
	declare @T_NIVEL5 Char(6)
    declare @T_NIVEL6 Char(6)
	declare @T_NIVEL7 Char(6)
	declare @T_NIVEL8 Char(6)
	declare @T_NIVEL9 Char(6)

 	select @FEC_KAR = CB_FEC_KAR,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
       	@NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2,
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4,
		@NIVEL5 = CB_NIVEL5,
       	@NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7,
		@NIVEL8 = CB_NIVEL8,
		@NIVEL9 = CB_NIVEL9,
        @SALARIO = CB_SALARIO
  	from COLABORA
  	where ( CB_CODIGO = @Empleado );

  	if ( @FECHA < @FEC_KAR )
  	begin
 		select @PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI,
 		    @TURNO = CB_TURNO,
         	@NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2,
			@NIVEL3 = CB_NIVEL3,
			@NIVEL4 = CB_NIVEL4,
			@NIVEL5 = CB_NIVEL5,
         	@NIVEL6 = CB_NIVEL6,
			@NIVEL7 = CB_NIVEL7,
			@NIVEL8 = CB_NIVEL8,
			@NIVEL9 = CB_NIVEL9,
            @SALARIO = CB_SALARIO
        from SP_FECHA_KARDEX( @Fecha, @Empleado );
  	end

 	select @FEC_KAR = AU_FECHA,
	       @T_PUESTO = CB_PUESTO,
	       @T_CLASIFI = CB_CLASIFI,
	       @T_TURNO = CB_TURNO,
           @T_NIVEL1 = CB_NIVEL1,
	       @T_NIVEL2 = CB_NIVEL2,
	       @T_NIVEL3 = CB_NIVEL3,
	       @T_NIVEL4 = CB_NIVEL4,
	       @T_NIVEL5 = CB_NIVEL5,
           @T_NIVEL6 = CB_NIVEL6,
	       @T_NIVEL7 = CB_NIVEL7,
	       @T_NIVEL8 = CB_NIVEL8,
	       @T_NIVEL9 = CB_NIVEL9
  	from CLASITMP where
        ( CB_CODIGO = @Empleado ) and
        ( AU_FECHA = @Fecha ) and
        ( US_COD_OK <> 0 );
        if ( @FEC_KAR is not NULL )
        begin
             if ( @T_PUESTO <> '' )
             begin
                  select @PUESTO = @T_PUESTO;
             end
             if ( @T_CLASIFI <> '' )
             begin
                  select @CLASIFI = @T_CLASIFI;
             end
             if ( @T_TURNO <> '' )
             begin
                  select @TURNO = @T_TURNO;
             end
             if ( @T_NIVEL1 <> '' )
             begin
                  select @NIVEL1 = @T_NIVEL1;
             end
             if ( @T_NIVEL2 <> '' )
             begin
                  select @NIVEL2 = @T_NIVEL2;
             end
             if ( @T_NIVEL3 <> '' )
             begin
                  select @NIVEL3 = @T_NIVEL3;
             end
             if ( @T_NIVEL4 <> '' )
             begin
                  select @NIVEL4 = @T_NIVEL4;
             end
             if ( @T_NIVEL5 <> '' )
             begin
                  select @NIVEL5 = @T_NIVEL5;
             end
             if ( @T_NIVEL6 <> '' )
             begin
                  select @NIVEL6 = @T_NIVEL6;
             end
             if ( @T_NIVEL7 <> '' )
             begin
                  select @NIVEL7 = @T_NIVEL7;
             end
             if ( @T_NIVEL8 <> '' )
             begin
                  select @NIVEL8 = @T_NIVEL8;
             end
             if ( @T_NIVEL9 <> '' )
             begin
                  select @NIVEL9 = @T_NIVEL9;
             end
        end

  	update AUSENCIA
  	set CB_PUESTO  = @PUESTO,
        CB_CLASIFI = @CLASIFI,
        CB_TURNO   = @TURNO,
        CB_NIVEL1  = @NIVEL1,
        CB_NIVEL2  = @NIVEL2,
        CB_NIVEL3  = @NIVEL3,
        CB_NIVEL4  = @NIVEL4,
        CB_NIVEL5  = @NIVEL5,
        CB_NIVEL6  = @NIVEL6,
        CB_NIVEL7  = @NIVEL7,
        CB_NIVEL8  = @NIVEL8,
        CB_NIVEL9  = @NIVEL9,
        CB_SALARIO = @SALARIO
  	where ( CB_CODIGO = @Empleado ) and ( AU_FECHA = @Fecha );
    EXECUTE SP_CLAS_AUSENCIA_ADICIONAL @Empleado, @Fecha;
GO

/* AFECTA_EMPLEADO: Considerar conceptos nuevos 1119,1120,1214,1215 ( Sugerencia QUINCENALES ) */
/* Patch 398 # Seq: 149 Modificado */
ALTER PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON
     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);

     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,         
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,         
            @M1117=N.NO_DIAS_SS,         
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1200=N.NO_JORNADA,         
            @M1201=N.NO_HORAS,           
            @M1202=N.NO_EXTRAS,          
            @M1203=N.NO_DOBLES,          
            @M1204=N.NO_TRIPLES,         
            @M1205=N.NO_ADICION,         
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT
            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
     begin
          Select @M1200 = @M1201 / @M1200;
     end
     Select @M1109 = @M1200 * @M1109;
     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008;
     if ( @M1009 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120;
     if ( @M1200 <> 0 )
     begin
          if ( @Tipo = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( @Tipo = 1 )
             Select @M1200 = 48 * @M1200;
          else
          if ( @Tipo = 2 )
             Select @M1200 = 96 * @M1200;
          else
          if ( @Tipo = 3 )
             Select @M1200 = 104 * @M1200;
          else
          if ( @Tipo = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @Tipo = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200;
     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215;

     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004;
END
GO

/* SP_IMSS_YEAR_V: Maneja # Empleado para calcular beneficios ( Beneficios Flexibles - Proyecto Especial BAT )
/* Patch 398 # Seq: 6 Agregado */
CREATE FUNCTION SP_IMSS_YEAR_V( @sTABLA CHAR( 1 ),
                                @iYEAR Integer,
                                @Resultado Integer,
                                @iEmpleado Integer )
RETURNS NUMERIC(15,5)
AS
BEGIN
     declare @DIAS_VA	NUMERIC(15,2)
     declare @PRIMAVA	NUMERIC(15,5)
     declare @DIAS_AG	NUMERIC(15,2)
     declare @FACTOR	NUMERIC(15,5)
     declare @PRIMADO	NUMERIC(15,5)
     declare @PAGO_7	CHAR(1)
     declare @PRIMA_7	CHAR(1)
     declare @DIAS_AD	NUMERIC(15,2)
     declare @RES	NUMERIC(15,5)
     declare @DiasVACAColabora NUMERIC(15,2);
     declare @DiasAGColabora NUMERIC(15,2);
     declare @PrimaVAColabora NUMERIC(15,5);
     declare @rDiasDominical NUMERIC(15,2);
     declare @Temporal NUMERIC(15,2);
     declare @rVacaciones NUMERIC(15,2);

     if ( @iYear > 0 )
     begin
          select @iYear = ( select MIN( PT_YEAR ) from PRESTACI
                           where ( TB_CODIGO = @sTabla ) and ( PT_YEAR >= @iYear ) )
     end
     if ( ( @iYear <= 0 ) or ( @iYear is NULL ) )
     begin
          select @RES = 0
     end
     else
     begin
          select @DIAS_VA = PT_DIAS_VA,
                 @PRIMAVA = PT_PRIMAVA,
                 @DIAS_AG = PT_DIAS_AG,
                 @FACTOR = PT_FACTOR,
                 @PRIMADO = PT_PRIMADO,
                 @PAGO_7 = PT_PAGO_7,
                 @PRIMA_7 = PT_PRIMA_7,
                 @DIAS_AD = PT_DIAS_AD
          from PRESTACI where ( TB_CODIGO = @sTabla ) and ( PT_YEAR = @iYear );

          if ( @Resultado = 1 )
             select @RES = @DIAS_VA
          else
          if ( @Resultado = 2 )
             set @RES = @PRIMAVA
          else
          if ( @Resultado = 3 )
             set @RES = @DIAS_AG
          else
          if ( @Resultado = 4 )
             set @RES = @FACTOR
          else
          if ( @Resultado = 5 )
             set @RES = @PRIMADO
          else
          if ( @Resultado = 6 )
          begin
               if ( @PAGO_7 = 'S' )
                  set @RES = 1
               else
                   set @RES = 0
          end
          else
          if ( @Resultado = 7 )
          begin
               if ( @PRIMA_7 = 'S' )
                  set @RES = 1
               else
                   set @RES = 0
          end
          else
          begin
               set @RES = @DIAS_AD
          end
     end
     return @RES
END
GO

/* SP_IMSS_YEAR: Manejar nuevo SP_IMSS_YEAR_V ( Beneficios Flexibles - Proyecto Especial BAT )
/* Patch 398 # Seq: 8 Modificado */
ALTER FUNCTION SP_IMSS_YEAR( @sTABLA CHAR(1),
                              @iYEAR Integer,
                              @Resultado Integer )
RETURNS NUMERIC(15,5)
AS
BEGIN
     RETURN DBO.SP_IMSS_YEAR_V( @sTABLA, @iYEAR, @Resultado, 0 )
END
GO

/* SP_IMSS_V: Maneja # Empleado para calcular beneficios ( Beneficios Flexibles - Proyecto Especial BAT )
/* Patch 398 # Seq: 7 Agregado */
CREATE FUNCTION SP_IMSS_V( @TABLA CHAR(1),
  		           @INICIAL DATETIME,
  		           @FINAL DATETIME,
		           @RESULTADO INTEGER,
                           @EMPLEADO INTEGER )
RETURNS NUMERIC(15, 5)
AS
BEGIN
     declare @DiasAntig NUMERIC(15,2);
     declare @Anio INTEGER;
     if ( @Final < @Inicial )
     begin
          select @Anio = 0
     end
     else
     begin
          select @DiasAntig = DATEDIFF( Day, @Inicial, @Final ) + 1
          select @Anio = CAST( @DiasAntig / 365.25 AS INTEGER )
       	  if ( @Anio <= ( @DiasAntig / 365.25 ) )
          begin
	       select @Anio = @Anio + 1
          end
     end
     RETURN DBO.SP_IMSS_YEAR_V( @Tabla, @Anio, @RESULTADO, @EMPLEADO )
END
GO

/* SP_IMSS: Manejar nuevo SP_IMSS_V ( Beneficios Flexibles - Proyecto Especial BAT )
/* Patch 398 # Seq: 9 Modificado */
ALTER FUNCTION SP_IMSS( @TABLA CHAR(1),
  		         @INICIAL DATETIME,
  		         @FINAL DATETIME,
		         @RESULTADO INTEGER )
RETURNS NUMERIC(15, 5)
AS
BEGIN
     RETURN DBO.SP_IMSS_V( @TABLA, @INICIAL, @FINAL, @RESULTADO, 0 )
END
GO

/* SP_GET_RENGLON: Maneja # Empleado para leer prestaciones ( Beneficios Flexibles - Proyecto Especial BAT )
/* Patch 398 # Seq: 232 Agregado */
CREATE FUNCTION SP_GET_RENGLON( @Tabla CHAR( 1 ),
                                @Anio INTEGER,
                                @Empleado INTEGER )
RETURNS @Prestaci TABLE ( PT_FACTOR NUMERIC( 15, 5 ),
                          PT_DIAS_VA NUMERIC( 15, 2 ),
                          PT_DIAS_AG NUMERIC( 15, 2 ),
                          PT_PRIMAVA NUMERIC( 15, 5 ),
                          PT_PRIMA_7 CHAR( 1 ),
                          PT_PAGO_7 CHAR( 1 ) )
AS
begin
     insert into @Prestaci
     select TOP 1 PT_FACTOR,
                  PT_DIAS_VA,
                  PT_DIAS_AG,
                  PT_PRIMAVA,
                  PT_PRIMA_7,
                  PT_PAGO_7
     from PRESTACI where ( TB_CODIGO = @Tabla ) and ( PT_YEAR >= @Anio )
     order by PT_YEAR;
     RETURN
end
GO

/* SP_GET_CACHE: Maneja # Empleado para leer prestaciones ( Beneficios Flexibles - Proyecto Especial BAT )
/* Patch 398 # Seq: 233 Agregado */
CREATE FUNCTION SP_GET_CACHE( @Tabla CHAR( 1 ), @Empleado INTEGER )
RETURNS @Prestaci TABLE ( PT_YEAR SMALLINT,
                          PT_DIAS_VA NUMERIC( 15, 2 ),
                          PT_PRIMAVA NUMERIC( 15, 5 ) )
AS
begin
     insert into @Prestaci
     select PT_YEAR, PT_DIAS_VA, PT_PRIMAVA from PRESTACI
     where ( TB_CODIGO = @Tabla )
     order by PT_YEAR;
     RETURN
end
GO

/****************************************/
/***** Actualizar versi�n de Datos ******/
/****************************************/

update GLOBAL set GL_FORMULA = '398' where ( GL_CODIGO = 133 )
GO
