/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/


/* #17499: En caso de tener el mecanismo encendido es necesario que el monto "Tot. Retenci�n Mensual" sea el requerido por Fonacot en la c�dula 1/7 */
if not exists( select NAME from SYSOBJECTS  where NAME='FON_TOT_RS' and XTYPE='U' )
begin
	CREATE TABLE FON_TOT_RS
	(
		FT_YEAR Anio ,
		FT_MONTH Mes ,
		PR_TIPO Codigo1 ,
		RS_CODIGO Codigo,
		FT_STATUS Status ,
		FT_RETENC Pesos ,
		FT_NOMINA Pesos ,
		FT_TAJUST Codigo1 ,
		FT_AJUSTE Pesos ,
		FT_EMPLEAD Empleados ,
		FT_CUANTOS Empleados ,
		FT_BAJAS Empleados ,
		FT_INCAPA Empleados ,
		FT_DETALLE Memo NULL,
		LLAVE int IDENTITY(1,1) ,
		CONSTRAINT PK_FON_TOT_RS PRIMARY KEY CLUSTERED 
		(
			FT_YEAR ASC,
			FT_MONTH ASC,
			PR_TIPO ASC, 
			RS_CODIGO ASC
		)
	)

end
GO

/* #17499: En caso de tener el mecanismo encendido es necesario que el monto "Tot. Retenci�n Mensual" sea el requerido por Fonacot en la c�dula 2/7 */
IF NOT EXISTS( SELECT NAME from sys.sysobjects WHERE NAME = 'FK_FON_TOT_RS_FON_TOT' and XTYPE = 'F' )
	ALTER TABLE FON_TOT_RS  WITH NOCHECK ADD  CONSTRAINT FK_FON_TOT_RS_FON_TOT FOREIGN KEY(FT_YEAR, FT_MONTH, PR_TIPO)
		REFERENCES FON_TOT (FT_YEAR, FT_MONTH, PR_TIPO)
		ON UPDATE CASCADE
		ON DELETE CASCADE
GO

/* #17499: En caso de tener el mecanismo encendido es necesario que el monto "Tot. Retenci�n Mensual" sea el requerido por Fonacot en la c�dula 3/7 */
IF EXISTS( select NAME from sys.sysobjects WHERE NAME = 'FK_FON_TOT_RS_FON_TOT' and XTYPE = 'F' )
	ALTER TABLE FON_TOT_RS CHECK CONSTRAINT FK_FON_TOT_RS_FON_TOT
GO


if exists ( select * from dbo.sysobjects where id = object_id(N'TU_FON_TON') and xtype = 'TR' ) 
	DROP TRIGGER TU_FON_TON
go 

CREATE TRIGGER TU_FON_TON ON FON_TOT AFTER UPDATE 
AS 
BEGIN 
	SET NOCOUNT ON 

	update FON_TOT_RS 
	set FON_TOT_RS.FT_STATUS = ins.FT_STATUS 
	from inserted ins, FON_TOT_RS 
	where ins.FT_YEAR = FON_TOT_RS.FT_YEAR and ins.FT_MONTH = FON_TOT_RS.FT_MONTH 
	
END

GO 

IF not EXISTS (select * from syscolumns where id=object_id('FON_CRE') and name='FC_PR_STA')
	ALTER TABLE FON_CRE  ADD FC_PR_STA  Status NULL 
go 
IF not EXISTS (select * from syscolumns where id=object_id('FON_CRE') and name='FC_FON_MOT')
	ALTER TABLE FON_CRE  ADD FC_FON_MOT Status NULL 
go 
IF not EXISTS (select * from syscolumns where id=object_id('FON_EMP') and name='FE_DIF_QTY')
	ALTER TABLE FON_EMP  ADD FE_DIF_QTY Empleados NULL 
go 

update FON_CRE set FC_PR_STA =  0 where FC_PR_STA is null 
go 
update FON_CRE set FC_FON_MOT = 0 where FC_FON_MOT is null 
go 
update FON_EMP set FE_DIF_QTY = 0  where FE_DIF_QTY is null 
go 

ALTER TABLE FON_CRE ALTER COLUMN FC_PR_STA Status NOT NULL 
go 
ALTER TABLE FON_CRE ALTER COLUMN FC_FON_MOT Status NOT NULL 
go 
ALTER TABLE FON_EMP ALTER COLUMN FE_DIF_QTY Empleados NOT NULL 
go 


IF exists( select * from sys.objects where object_id = object_id('VCEDTFON') )
    drop view VCEDTFON
GO

/* #17444 Se sugiere se agregue tener el indicador del motivo de Cancelaci�n de Descuento
	por Aviso de FONACOT y otro cuando la n�mina es cero o bien, el empleado no tiene liquidez. Estos casos sacarlos del Otros en el Resumen. 1/4 */

create view VCEDTFON(
    CB_CODIGO,   
	PF_YEAR, 
    PF_MES,
    CT_PAGO,   
    CT_NOMINA, 
	CT_AJUSTE,
	CT_DIF, 
	CT_DIF_QTY, 
	CT_CRE_QTY   )
as
select VF.CB_CODIGO, PF_YEAR, PF_MES, coalesce(SUM(PF_PAGO),0.00)  CT_PAGO, coalesce(SUM(PF_NOMINA),0.00) CT_NOMINA,  coalesce( SUM(FC.FC_AJUSTE), 0.00) CT_AJUSTE,   coalesce( SUM( PF_PAGO - (PF_NOMINA + FC.FC_AJUSTE) ),0.00) CT_DIF, 
coalesce( SUM(case when PF_PAGO > ( PF_NOMINA + FC.FC_AJUSTE ) then 1 else 0 end), 0 ) CT_DIF_QTY , 
count(*) CT_CRE_QTY
from VCED_FON  VF
join FON_CRE FC on FC.FT_YEAR = VF.PF_YEAR and FC.FT_MONTH = VF.PF_MES and FC.CB_CODIGO = VF.CB_CODIGO  and FC.PR_TIPO = VF.PR_TIPO and FC.PR_REFEREN = VF.PR_REFEREN
group by  VF.CB_CODIGO, PF_YEAR, PF_MES

GO 

/* Vista para facilitar el recalculo de las incidencias de Fonacot Nueva modalidad */
IF exists( select * from sys.objects where object_id = object_id('VCED_CALCULA') )
    drop view VCED_CALCULA
GO

CREATE view VCED_CALCULA
as
select 
PF_YEAR, PF_MES, CB_CODIGO, PR_REFEREN,  PR_TIPO, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE, 
PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA, 
FE_INCIDE, 
PR_STATUS,
PR_FON_MOT,
FECHA_INI_BAJA as FE_FECHA1, 
FECHA_FIN as FE_FECHA2, 
PF_REUBICA, 
CT_DIF_QTY, 
FE_CUANTOS
from ( 

SELECT F.PF_YEAR, F.PF_MES, C.CB_CODIGO, F.PR_REFEREN,  F.PR_TIPO, F.PF_PAGO, F.PF_PLAZO, F.PF_PAGADAS,
	coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,
				coalesce(case			
					 when FE.FE_INCIDE = 'B'  then 'B' 					 					 
					 when ((FE.FE_INCIDE is null or FE.FE_INCIDE = '' or FE.FE_INCIDE = '0') AND P.PR_STATUS = 0) then '0'
					 else FE.FE_INCIDE   
				end, '')   FE_INCIDE , 	
				coalesce(case 
					when FE.FE_INCIDE = 'B'  then   FE.FE_FECHA1														
					else FE.FE_FECHA1
				end , '' ) AS FECHA_INI_BAJA, 
				coalesce( case 
					when FE.FE_INCIDE = 'B'  then  '1899-12-30' 										
					else FE.FE_FECHA2
				end, ''  ) AS FECHA_FIN, 
			     F.PF_REUBICA, 
				coalesce(PR_STATUS,0) PR_STATUS, 
				coalesce(PR_FON_MOT,0) PR_FON_MOT, 
				coalesce(CT.CT_DIF_QTY, 0) CT_DIF_QTY, 
				coalesce(FE.FE_CUANTOS, 0 ) FE_CUANTOS
	FROM PCED_FON F
		LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
		LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
		LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN AND F.PR_TIPO = FC.PR_TIPO 
		LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES		
		LEFT JOIN PRESTAMO P ON P.PR_REFEREN = F.PR_REFEREN AND P.PR_TIPO = F.PR_TIPO AND P.CB_CODIGO = F.CB_CODIGO 
	
	) Cedula 

GO 


/* Vista para facilitar el recalculo de las incidencias de Fonacot Nueva modalidad */
IF exists( select * from sys.objects where object_id = object_id('VCED_CONSULTA') )
    drop view VCED_CONSULTA
GO

CREATE view VCED_CONSULTA
as
select 
PF_YEAR, PF_MES, CB_CODIGO, PR_REFEREN,  PR_TIPO, PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE, 
PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA, 
FE_INCIDE, 
PR_STATUS,
PR_FON_MOT,
FECHA_INI_BAJA as FE_FECHA1, 
FECHA_FIN as FE_FECHA2, 
PF_REUBICA, 
FE_DIF_QTY, 
FE_CUANTOS
from ( 

SELECT F.PF_YEAR, F.PF_MES, C.CB_CODIGO, F.PR_REFEREN,  F.PR_TIPO, F.PF_PAGO, F.PF_PLAZO, F.PF_PAGADAS,
	coalesce(FC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(FC.FC_AJUSTE,0.00) FC_AJUSTE ,
				coalesce(FE_INCIDE, '0') AS FE_INCIDE , 	
   				coalesce(FE.FE_FECHA1, '1899-12-30') AS FECHA_INI_BAJA, 
				coalesce(FE.FE_FECHA2, '1899-12-30') AS FECHA_FIN, 
			    F.PF_REUBICA, 
				FC.FC_PR_STA AS PR_STATUS, 
				FC.FC_FON_MOT AS PR_FON_MOT, 
				FE.FE_DIF_QTY, 
				FE.FE_CUANTOS		
	FROM PCED_FON F
		LEFT JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
		LEFT JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
		LEFT JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN  AND F.PR_TIPO = FC.PR_TIPO 
		LEFT JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES				
	
	) Cedula 

GO 
/* #17444 Se sugiere se agregue tener el indicador del motivo de Cancelaci�n de Descuento
	por Aviso de FONACOT y otro cuando la n�mina es cero o bien, el empleado no tiene liquidez. Estos casos sacarlos del Otros en el Resumen. 1/4 */
IF exists( select * from sys.objects where object_id = object_id('VCED_DET') )
    drop view VCED_DET
GO

/* #17444 Se sugiere se agregue tener el indicador del motivo de Cancelaci�n de Descuento
	por Aviso de FONACOT y otro cuando la n�mina es cero o bien, el empleado no tiene liquidez. Estos casos sacarlos del Otros en el Resumen. 2/4 */
CREATE view VCED_DET
as
select 
PF_YEAR, PF_MES, RS_CODIGO, CB_FONACOT, CB_RFC, CB_CODIGO, PRETTYNAME, PR_REFEREN, PR_TIPO,  PF_PAGO, PF_PLAZO, PF_PAGADAS, FC_NOMINA, FC_AJUSTE, 
PF_PAGO - (FC_NOMINA + FC_AJUSTE )  as DIFERENCIA, 
FE_INCIDE, 
case FE_INCIDE 
	when '0' then '' 
	when 'B' then 'Baja IMSS' 
	when 'I' then 'Incapacidad' 
	when 'C' then 'Pr�stamo cancelado'
	else  'Otro' 
end FE_IN_DESC, 
case FE_INCIDE 
	when '0' then '' 
	when 'C' then PR_FON_MOT
	else  '' 
end PR_FON_MOT,
FECHA_INI_BAJA as FE_FECHA1, FECHA_FIN as FE_FECHA2, PF_REUBICA, FE_DIF_QTY, FE_CUANTOS
from ( 
SELECT VC.PF_YEAR, VC.PF_MES,  coalesce(RP.RS_CODIGO,'') RS_CODIGO,  C.CB_FONACOT, C.CB_RFC, coalesce(UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES), '')  AS PRETTYNAME, VC.PR_REFEREN, VC.PR_TIPO,  VC.PF_PAGO, coalesce(C.CB_CODIGO,0) CB_CODIGO, VC.PF_PLAZO, VC.PF_PAGADAS,
	coalesce(VC.FC_NOMINA,0.00) FC_NOMINA,  coalesce(VC.FC_AJUSTE,0.00) FC_AJUSTE ,
				
				case 
					when coalesce( FE_INCIDE, '0')  = '0'  and PR_STATUS = 1  then 'C' 
					else coalesce( FE_INCIDE, '0' ) 
				end AS FE_INCIDE , 	
				case 									
					when CONVERT (VARCHAR(10), VC.FE_FECHA1, 103) = '30/12/1899'  then '' 					
					else CONVERT (VARCHAR(10), VC.FE_FECHA1, 103) 
				end AS FECHA_INI_BAJA, 
				case 
					when CONVERT (VARCHAR(10), VC.FE_FECHA2, 103) = '30/12/1899' then '' 					
					else CONVERT (VARCHAR(10), VC.FE_FECHA2, 103) 
				end AS FECHA_FIN, 
			    VC.PF_REUBICA,				 
				case PR_FON_MOT 
					when 0 then 'No aplica' 
					when 1 then 'Pago directo en Fonacot' 
					when 2 then 'Confirmaci�n de Fonacot' 
					when 3 then 'Ausente en C�dula'
					when 4 then 'CanceladoOtro'
					else  '' 
				end PR_FON_MOT, VC.FE_DIF_QTY, VC.FE_CUANTOS
	FROM VCED_CONSULTA VC
		LEFT JOIN COLABORA C ON VC.CB_CODIGO = C.CB_CODIGO
		LEFT JOIN RPATRON RP on CB_PATRON = RP.TB_CODIGO
	) Cedula 

GO 

/* #17499: En caso de tener el mecanismo encendido es necesario que el monto "Tot. Retenci�n Mensual" sea el requerido por Fonacot en la c�dula 4/7 */
if exists (select * from dbo.sysobjects where id = object_id(N'FN_FONACOT_GET_TOTALES_CEDULAS') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_FONACOT_GET_TOTALES_CEDULAS
go

/* #17499: En caso de tener el mecanismo encendido es necesario que el monto "Tot. Retenci�n Mensual" sea el requerido por Fonacot en la c�dula 5/7 */
create function FN_FONACOT_GET_TOTALES_CEDULAS(  @PF_YEAR Anio, @PF_MES Mes, @RS_SOCIAL Codigo )
returns table  
return ( 
select  
    PF_YEAR, 
    PF_MES,
    SUM( CT_PAGO ) CT_PAGO,   
    SUM( CT_NOMINA ) CT_NOMINA , 
    SUM( CT_DIF ) CT_DIF, 
    SUM( CT_DIF_QTY ) CT_DIF_QTY, 
    SUM( CT_CRE_QTY ) CT_CRE_QTY
    from VCEDTFON CT
        left outer join COLABORA CB on CT.CB_CODIGO = CB.CB_CODIGO 
        left outer join RPATRON RP on CB.CB_PATRON = RP.TB_CODIGO 
        where PF_YEAR = @PF_YEAR and PF_MES = @PF_MES  and ( RP.RS_CODIGO = @RS_SOCIAL or @RS_SOCIAL = '' ) 
    group by PF_YEAR, PF_MES

)
GO


/* */ 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_RECALCULAR_INCIDENCIAS') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_RECALCULAR_INCIDENCIAS
go

create procedure SP_FONACOT_RECALCULAR_INCIDENCIAS(@FT_YEAR Anio, @FT_MONTH Mes) 
as 
begin 
	set nocount on 

	update FON_EMP set FE_INCIDE = VD.FE_INCIDE, FE_FECHA1 = VD.FE_FECHA1, FE_FECHA2 = VD.FE_FECHA2, FE_DIF_QTY = coalesce( VD.CT_DIF_QTY , 0 ) 
	from  FON_EMP, VCED_CALCULA VD
	where  VD.CB_CODIGO = FON_EMP.CB_CODIGO and  VD.PF_YEAR = FON_EMP.FT_YEAR and VD.PF_MES = FON_EMP.FT_MONTH and 
	VD.PF_YEAR = @FT_YEAR and VD.PF_MES = @FT_MONTH 


	update FON_CRE set FC_PR_STA = VD.PR_STATUS, FC_FON_MOT = VD.PR_FON_MOT 
	from FON_CRE, VCED_CALCULA VD 
	where  VD.CB_CODIGO = FON_CRE.CB_CODIGO and  VD.PF_YEAR = FON_CRE.FT_YEAR and VD.PF_MES = FON_CRE.FT_MONTH and VD.PR_REFEREN = FON_CRE.PR_REFEREN and VD.PR_TIPO = FON_CRE.PR_TIPO and 
	VD.PF_YEAR = @FT_YEAR and VD.PF_MES = @FT_MONTH 

	
end; 

GO 

/* #17499: En caso de tener el mecanismo encendido es necesario que el monto "Tot. Retenci�n Mensual" sea el requerido por Fonacot en la c�dula 6/7 */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'[SP_FONACOT_PAGO_RAZON_SOCIAL]') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE SP_FONACOT_PAGO_RAZON_SOCIAL
go

/* #17499: En caso de tener el mecanismo encendido es necesario que el monto "Tot. Retenci�n Mensual" sea el requerido por Fonacot en la c�dula 7/7 */
CREATE PROCEDURE SP_FONACOT_PAGO_RAZON_SOCIAL
( @FT_YEAR Anio, @FT_MONTH Mes, @GLOBAL_ACTIVO Booleano, @RecalcularIncidencias Booleano = 'S' )
AS
BEGIN	
	-- Solo se recalculan las incidencias en FON_EMP cuando la opcion esta encendida. 
	if @GLOBAL_ACTIVO = 'S' and @RecalcularIncidencias = 'S' 
		exec SP_FONACOT_RECALCULAR_INCIDENCIAS @FT_YEAR, @FT_MONTH 


	-- Borrar FON_TOT_RS
	DELETE FROM FON_TOT_RS WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH

	-- Abrir cursor
	DECLARE @RS_CODIGO Codigo

	DECLARE RAZONES_SOCIALES CURSOR FOR
		SELECT DISTINCT RS_CODIGO FROM RPATRON WHERE TB_CODIGO IN
			(SELECT DISTINCT CB_PATRON FROM COLABORA WHERE CB_CODIGO IN (SELECT DISTINCT CB_CODIGO FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH))
	-- Abrir cursor
	OPEN RAZONES_SOCIALES 
		FETCH NEXT FROM RAZONES_SOCIALES INTO @RS_CODIGO

	-- Ciclo
	WHILE ( @@FETCH_STATUS = 0 )
	BEGIN
		-- PRINT (@RS_CODIGO)
		-- Grabar en FON_TOT_RS
		INSERT INTO FON_TOT_RS
			(FT_YEAR, FT_MONTH, PR_TIPO, RS_CODIGO, FT_STATUS, FT_RETENC, FT_NOMINA, FT_TAJUST, FT_AJUSTE, FT_EMPLEAD, FT_CUANTOS, FT_BAJAS, FT_INCAPA, FT_DETALLE)
		-- SELECT FT_YEAR, FT_MONTH, PR_TIPO, @RS_CODIGO, 0 FT_STATUS, SUM (FC_RETENC), SUM(FC_NOMINA), '' FT_TAJUST, SUM (FC_AJUSTE), 
		SELECT FT_YEAR, FT_MONTH, PR_TIPO, @RS_CODIGO, 0 FT_STATUS, 0, SUM(FC_NOMINA), '' FT_TAJUST, SUM (FC_AJUSTE), 
		0 FT_EMPLEAD, 0 FT_CUANTOS, 0 FT_BAJAS, 0 FT_INCAPA, '' FT_DETALLE FROM FON_CRE
			WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
			GROUP BY FT_YEAR, FT_MONTH, PR_TIPO
			
		-- Indicar cantidad de empleados y cantidad de cr�ditos.
		DECLARE @FT_EMPLEAD Empleados
		DECLARE @FT_CUANTOS Empleados			
		-- Indicar monto de c�dula, incapacidades y bajas
		-- Obtener de FON_CRE o PCED_FON dependiendo de lo que indique el global.
		DECLARE @FC_RETENC Pesos SET @FC_RETENC = 0
		DECLARE @FT_BAJAS Empleados SET @FT_BAJAS = 0
		DECLARE @FT_INCAPA Empleados SET @FT_INCAPA = 0

		IF @GLOBAL_ACTIVO = 'S' 
		BEGIN
			SELECT @FC_RETENC = SUM (PF_PAGO) FROM PCED_FON WHERE  PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH 
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				
			SELECT @FT_BAJAS = COUNT(DISTINCT CB_CODIGO)  FROM VCED_DET WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
				(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				AND FE_INCIDE = 'B'
			SELECT @FT_INCAPA = COUNT(DISTINCT CB_CODIGO)  FROM VCED_DET WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
				(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				AND FE_INCIDE = 'I'
			
			-- FT_CUANTOS: # de Cr�ditos Fonacot
			SELECT @FT_CUANTOS = COUNT(DISTINCT PR_REFEREN) FROM VCED_FON WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
			AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
			-- FT_EMPLEAD: # Empleados con Pr�stamos Fonacot
			SELECT @FT_EMPLEAD = COUNT(DISTINCT CB_CODIGO) FROM VCED_FON WHERE PF_YEAR = @FT_YEAR AND PF_MES = @FT_MONTH
			AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))

		END
		ELSE
		BEGIN			
			SELECT @FC_RETENC = SUM (FC_RETENC) FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH 
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				 
			
			SELECT @FT_BAJAS = COUNT(DISTINCT CB_CODIGO)  FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
				(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				AND FE_INCIDE = 'B'
			SELECT @FT_INCAPA = COUNT(DISTINCT CB_CODIGO)  FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
				AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN
				(SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
				AND FE_INCIDE = 'I'

			-- FT_CUANTOS: # de Cr�ditos Fonacot
			SELECT @FT_CUANTOS = COUNT(DISTINCT PR_REFEREN) FROM FON_CRE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
			AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
			-- FT_EMPLEAD: # Empleados con Pr�stamos Fonacot
			SELECT @FT_EMPLEAD = COUNT(DISTINCT CB_CODIGO) FROM FON_EMP WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
			AND CB_CODIGO IN (SELECT CB_CODIGO FROM COLABORA WHERE CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RS_CODIGO))
		END
		
		-- Actualizar tabla
		UPDATE FON_TOT_RS SET FT_RETENC = @FC_RETENC, FT_BAJAS = @FT_BAJAS, FT_INCAPA = @FT_INCAPA, FT_EMPLEAD = @FT_EMPLEAD, FT_CUANTOS = @FT_CUANTOS 
		WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH AND RS_CODIGO = @RS_CODIGO

		FETCH NEXT FROM RAZONES_SOCIALES
			INTO	@RS_CODIGO
	END

	CLOSE RAZONES_SOCIALES
	DEALLOCATE RAZONES_SOCIALES 

	-- Actualizar estatus, tipo de ajuste y detalle.
	-- Se obtienen de FON_TOT.
	DECLARE @FT_STATUS Status
	DECLARE @FT_TAJUST Codigo1
	DECLARE @FT_DETALLE NVARCHAR(MAX)
	SELECT @FT_STATUS = FT_STATUS, @FT_TAJUST = FT_TAJUST, @FT_DETALLE = FT_DETALLE FROM FON_TOT WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH
	UPDATE FON_TOT_RS SET FT_STATUS = @FT_STATUS, FT_TAJUST = @FT_TAJUST, FT_DETALLE = @FT_DETALLE WHERE FT_YEAR = @FT_YEAR AND FT_MONTH = @FT_MONTH


END
GO


/* #17444 Se sugiere se agregue tener el indicador del motivo de Cancelaci�n de Descuento
	por Aviso de FONACOT y otro cuando la n�mina es cero o bien, el empleado no tiene liquidez. Estos casos sacarlos del Otros en el Resumen. 3/4 */
if exists (select * from dbo.sysobjects where id = object_id(N'Fon_Diferencias_Totales') and xtype in (N'FN', N'IF', N'TF')) 
	DROP FUNCTION Fon_Diferencias_Totales
go 

/* #17444 Se sugiere se agregue tener el indicador del motivo de Cancelaci�n de Descuento
	por Aviso de FONACOT y otro cuando la n�mina es cero o bien, el empleado no tiene liquidez. Estos casos sacarlos del Otros en el Resumen. 4/4 */
CREATE FUNCTION Fon_Diferencias_Totales( @Year int, @Mes int, @RSCodigo Codigo) 
RETURNS TABLE 
AS RETURN
		select  
				Coalesce ([Baja IMSS], 0) as BajaIMSS, Coalesce (Incapacidad, 0) as Incapacidad, Coalesce ([Pr�stamo cancelado], 0) as PrestamoCancelado,
				Coalesce (Otro, 0) as Otro, coalesce (Total, 0) Total,
				Coalesce ([Ausente en C�dula], 0) as Ausente, Coalesce ([Confirmaci�n de Fonacot], 0) as Confirmacion,
				Coalesce (CanceladoOtro, 0) as CanceladoOtro, Coalesce ([Pago directo en Fonacot], 0) as PagoDirecto,  coalesce (SubTotal, 0) SubTotal	
			from
			(		
				  SELECT FE_IN_DESC as columnname, SUM (DIFERENCIA) AS value
					  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo		  
					  AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
					  GROUP BY FE_IN_DESC
				UNION
					  SELECT 'Otro' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo		  
						  AND (FE_INCIDE = '0' OR FE_INCIDE = '')
				UNION
					  SELECT 'Total' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo
				UNION
					 SELECT 
						PR_FON_MOT as columnname, SUM (DIFERENCIA) AS value
						FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo
						AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
						AND FE_INCIDE = 'C'
						GROUP BY PR_FON_MOT
				UNION
					  SELECT 'SubTotal' as columnname, SUM (DIFERENCIA) AS value
						  FROM VCED_DET WHERE PF_YEAR = @Year AND PF_MES = @Mes AND RS_CODIGO = @RSCodigo
						AND FE_IN_DESC <> '' AND FE_IN_DESC <> 'Otro'
						AND FE_INCIDE = 'C'
			  ) d
			pivot
			(
			  max(value)
			  for columnname in ([Baja IMSS], Incapacidad, [Pr�stamo cancelado], Otro, Total,
				[Ausente en C�dula], [Confirmaci�n de Fonacot],  CanceladoOtro, [Pago directo en Fonacot], SubTotal)
			) piv
GO 


/* #16855: Poder desafectar y afectar la n�mina filtrando por empleados. */
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'[AFECTA_FALTAS_UN_EMPLEADO]') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE AFECTA_FALTAS_UN_EMPLEADO
GO
 
CREATE PROCEDURE AFECTA_FALTAS_UN_EMPLEADO
    				@ANIO SMALLINT,
    				@TIPO SMALLINT,
    				@NUMERO SMALLINT,
    				@FACTOR INTEGER,
					@EMPLEADO INTEGER
AS
	SET NOCOUNT ON
	declare @FechaIni   DateTime;
  	declare @Incidencia Char( 6 );
  	declare @TipoDia    SmallInt;
  	declare @FEC_INI    DateTime;
  	declare @MOTIVO     SmallInt;
  	declare @DIAS       Numeric(15,2);
	Declare @Bloqueo    VARCHAR(1);

	select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289;
	
	if( @Bloqueo = 'S' )
	begin
		 Update GLOBAL set GL_FORMULA = 'N' where GL_CODIGO = 289;
	end
  	
	select @FechaIni = P.PE_ASI_INI  /* Fecha de inicio de asistencia */
	from PERIODO P
	where
         ( P.PE_YEAR = @Anio ) and
         ( P.PE_TIPO = @Tipo ) and
         ( P.PE_NUMERO = @Numero )
	
	DECLARE TemporalFaltas CURSOR FOR 
  		select F.FA_FEC_INI, F.FA_MOTIVO, F.FA_DIAS
  		from   FALTAS F
       	left outer join NOMINA N on
         	( N.PE_YEAR = @Anio )
		and ( N.PE_TIPO = @Tipo )
		and ( N.PE_NUMERO = @Numero )
		and ( N.CB_CODIGO = F.CB_CODIGO )
  		where ( F.PE_YEAR = @Anio )
		and ( F.PE_TIPO = @Tipo )
		and ( F.PE_NUMERO = @Numero )
		and ( F.FA_DIA_HOR = 'D' )
		and ( F.FA_MOTIVO  IN (0,1,2,4,12))
		and N.CB_CODIGO = @EMPLEADO

   	OPEN TemporalFaltas
   	FETCH NEXT FROM TemporalFaltas
   	into  @FEC_INI, @MOTIVO, @DIAS
    WHILE @@FETCH_STATUS = 0
   	BEGIN
		if ( @FEC_INI <= '12/30/1899' )
		begin
			select @FEC_INI = @FechaIni;
		end
		if ( @MOTIVO = 0 )
		begin
       		Select @TipoDia = 0, @Incidencia = 'FI'
		end
		else if ( @MOTIVO = 1 )
		begin
       		Select @TipoDia = 5, @Incidencia = 'FJ'
		end
		else if ( @MOTIVO = 2 )
		begin
       		Select @TipoDia = 4, @Incidencia = 'FJ'
		end
		else if ( @MOTIVO = 4 )
		begin
       		Select @TipoDia = 6, @Incidencia = 'FJ'
		end
		else
		begin
       		Select @TipoDia = 0, @Incidencia = '';
		end
    
		while ( @DIAS > 0 )
		begin
       		if ( @MOTIVO <> 12 )
         		execute AFECTA_1_FALTA @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor
       		else if ( @FEC_INI < @FechaIni )
         		execute AFECTA_1_AJUSTE @Empleado, @FEC_INI, @TipoDia, @Incidencia, @Factor
       
       		select @DIAS = @DIAS - 1, @FEC_INI = @FEC_INI + 1
		end
   		
		FETCH NEXT FROM TemporalFaltas
   		into @FEC_INI, @MOTIVO, @DIAS
	END

	CLOSE TemporalFaltas
	DEALLOCATE TemporalFaltas
	if (@Bloqueo = 'S')
	begin
		Update GLOBAL set GL_FORMULA = 'S' where GL_CODIGO = 289;
	end
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'[AFECTA_PERIODO_UN_EMPLEADO]') AND type IN ( N'P', N'PC' ))
    DROP PROCEDURE AFECTA_PERIODO_UN_EMPLEADO
GO

CREATE PROCEDURE AFECTA_PERIODO_UN_EMPLEADO(
	@ANIO SMALLINT,
	@TIPO SMALLINT,
	@NUMERO SMALLINT,
	@USUARIO SMALLINT,
	@STATUSVIEJO SMALLINT,
	@STATUSNUEVO SMALLINT,
	@FACTOR INTEGER,
	@EMPLEADO INTEGER )
AS
	SET NOCOUNT ON
	DECLARE @CONCEPTO INTEGER;
	DECLARE @MES SMALLINT;
	DECLARE @CONTADOR INTEGER;
	DECLARE @TOTAL NUMERIC(15,2);
	DECLARE @TIPOAHORRO CHAR(1);
	DECLARE @TIPOPRESTAMO CHAR(1);
	DECLARE @REFERENCIA VARCHAR(8);

	select @MES = P.PE_MES from PERIODO P where ( P.PE_YEAR = @Anio ) and ( P.PE_TIPO = @Tipo ) and ( P.PE_NUMERO = @Numero )

	if (select count(*) from NOMINA N left outer join COLABORA C  on ( C.CB_CODIGO = N.CB_CODIGO ) where ( N.PE_YEAR = @Anio ) and ( N.PE_TIPO = @Tipo ) and ( N.PE_NUMERO = @Numero )  and not ( C.CB_CODIGO is NULL )  and N.CB_CODIGO = @EMPLEADO  and ( coalesce( N.NO_TIMBRO, 0 ) > 0 )  ) > 0
	begin
		RAISERROR( 'La n�mina est� Timbrada', 16, 1 )
		return 0
	end
				
	if (
    select count(*) from NOMINA N  left outer join COLABORA C  on ( C.CB_CODIGO = N.CB_CODIGO ) where ( N.PE_YEAR = @Anio )  and ( N.PE_TIPO = @Tipo )
		and ( N.PE_NUMERO = @Numero )		and ( N.NO_STATUS = @StatusViejo ) 		and not ( C.CB_CODIGO is NULL )
		and ( coalesce( N.NO_TIMBRO, 0 ) = 0 )	and N.CB_CODIGO = @EMPLEADO
		) >  0
	BEGIN
		EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, @Factor;
		
		Declare TempAhorro CURSOR for
			select A.AH_TIPO, T.TB_CONCEPT from AHORRO A join TAHORRO T on ( T.TB_CODIGO = A.AH_TIPO )  where ( A.CB_CODIGO = @Empleado ) and ( A.AH_STATUS = 0 )
			Open TempAhorro
			fetch NEXT FROM TempAhorro
			into @TipoAhorro, @Concepto
			while @@FETCH_STATUS = 0
       		begin
       			select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
				from MOVIMIEN M  where ( M.PE_YEAR = @Anio )
				and ( M.PE_TIPO = @Tipo )
				and ( M.PE_NUMERO = @Numero )
				and ( M.CB_CODIGO= @Empleado )
				and ( M.CO_NUMERO = @Concepto )
				and ( M.MO_ACTIVO = 'S' )
				if ( @Contador > 0 )
				begin
					Select @Total = @Factor * @Total;
					update AHORRO  set
                      			AH_NUMERO = AH_NUMERO + @Factor,
                       			AH_TOTAL  = AH_TOTAL + @Total,
                       			AH_SALDO = AH_SALDO + @Total
								where ( CB_CODIGO = @Empleado )
								and ( AH_TIPO = @TipoAhorro );
        		end
				fetch NEXT FROM TempAhorro
 				into   @TipoAhorro, @Concepto
			end
		close TempAhorro
		deallocate TempAhorro
 		
		Declare TempPrestamo CURSOR for
			select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT
			from PRESTAMO P join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO ) where ( P.CB_CODIGO = @Empleado ) and ( P.PR_STATUS in ( 0, 2 ) ) Open TempPrestamo fetch NEXT FROM TempPrestamo
			into @TipoPrestamo, @Referencia, @Concepto
			
			while @@FETCH_STATUS = 0
       		begin
       			select @Contador = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
				from MOVIMIEN M   where ( M.PE_YEAR = @Anio )
				and ( M.PE_TIPO = @Tipo )
				and ( M.PE_NUMERO = @Numero )
				and ( M.CB_CODIGO= @Empleado )
				and ( M.CO_NUMERO = @Concepto )
				and ( M.MO_ACTIVO = 'S' ) and ( M.MO_REFEREN = @Referencia )
				if ( @Contador > 0 )
       			begin
               		Select @Total = @Factor * @Total;
               		update PRESTAMO set
						PR_NUMERO = PR_NUMERO + @Factor,
						PR_TOTAL  = PR_TOTAL + @Total,
						PR_SALDO = PR_SALDO - @Total
						where ( CB_CODIGO = @Empleado )
						and ( PR_TIPO = @TipoPrestamo )
						and ( PR_REFEREN = @Referencia );
       			end
				fetch NEXT FROM TempPrestamo
				into @TipoPrestamo, @Referencia, @Concepto
			end
		close TempPrestamo
		deallocate TempPrestamo

		EXECUTE SET_STATUS_PRESTAMOS @Empleado;
		if ( @StatusNuevo > 0 )
		begin
			update NOMINA set NO_STATUS = @StatusNuevo  where ( PE_YEAR = @Anio )
			and ( PE_TIPO = @Tipo )
			and ( PE_NUMERO = @Numero )
			and ( CB_CODIGO = @Empleado );
		end

		EXECUTE AFECTA_FALTAS_UN_EMPLEADO @Anio, @Tipo, @Numero, @Factor, @Empleado
			  
		if ( @StatusNuevo > 0 )
		begin
			update NOMINA set NO_STATUS = @StatusNuevo where ( PE_YEAR = @Anio )
				and ( PE_TIPO = @Tipo )
				and ( PE_NUMERO = @Numero )
				and ( CB_CODIGO = @EMPLEADO )
								
			EXECUTE SET_STATUS_PERIODO @Anio, @Tipo, @Numero, @Usuario, @StatusNuevo
		end
		RETURN 1
	END
RETURN 0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'FONACOT_CEDULA') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_CEDULA

GO 

create function FONACOT_CEDULA
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
	(
    NO_FONACOT varchar(30),
    RFC varchar(30),
	NOMBRE VARCHAR(250),
	NO_CREDITO varchar (8),
	RETENCION_MENSUAL numeric(15,2),
	CLAVE_EMPLEADO int,
	PLAZO smallint,
	MESES smallint,
	RETENCION_REAL numeric (15,2),
	INCIDENCIA char(1)	,
	FECHA_INI_BAJA varchar(10),
	FECHA_FIN varchar(10),
	REUBICADO CHAR(1)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_FONACOT, RFC, NOMBRE, NO_CREDITO, RETENCION_MENSUAL, CLAVE_EMPLEADO, PLAZO, MESES, RETENCION_REAL,
		INCIDENCIA, FECHA_INI_BAJA, FECHA_FIN, REUBICADO)	

	select CB_FONACOT, CB_RFC, PRETTYNAME, PR_REFEREN, PF_PAGO, CB_CODIGO,  PF_PLAZO, PF_PAGADAS, FC_NOMINA+FC_AJUSTE, 
			case 
				when FE_INCIDE = 'B' then FE_INCIDE 
			    when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '0' 	 
				when FE_INCIDE not in ('B', '0', 'I' ) then '0' 
				else FE_INCIDE 
			end as 
			FE_INCIDE, 

			case 
				when FE_INCIDE = 'B' then FE_FECHA1 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA1 
			end  as 
			FE_FECHA1, 

			case 
				when FE_INCIDE = 'B' then FE_FECHA2 
				when @IgnorarIncapacidades = 'S' and FE_INCIDE = 'I' and FE_DIF_QTY < FE_CUANTOS  then  '' 			
				else FE_FECHA2
			end  as 
			FE_FECHA2, 
            PF_REUBICA 
	from VCED_DET
	WHERE 	PF_YEAR = @YEAR AND PF_MES = @MES	AND ( RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')

		
	return
END

GO 
-- recalcular los totales de todas las nominas . 
create procedure SP_FONACOT_RECALCULAR_FON_TOT_RS
as
begin

	set nocount on; 

	if ( select COUNT(*) from FON_TOT_RS ) > 0 
		return;

	
	select row_number() over (order by FT_YEAR, FT_MONTH, PR_TIPO ) as i,  FT_YEAR, FT_MONTH, PR_TIPO into #CalculosFonacot from FON_TOT 

	declare @i int 
	declare @nCalculos int
	declare @FT_YEAR Anio 
	declare @FT_MONTH Mes 
	declare @PR_TIPO Codigo1 
	declare @GLOBAL_ACTIVO Booleano
	declare @GlobalActivoConCedula Booleano
	declare @RecalcularIncidencias Booleano

	select @GLOBAL_ACTIVO = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 328 


	set @RecalcularIncidencias = 'N' 

	set @i=1 
	select @nCalculos=COUNT(*) from #CalculosFonacot 

	while @i <= @nCalculos
	begin 
		select @FT_YEAR = FT_YEAR, @FT_MONTH = FT_MONTH , @PR_TIPO = PR_TIPO from #CalculosFonacot where i=@i 

		if @GLOBAL_ACTIVO = 'S' and (select COUNT(*) from PCED_FON  PF where PF.PF_YEAR = @FT_YEAR and PF.PF_MES = @FT_MONTH )> 0 
		begin 
			set @GlobalActivoConCedula = 'S' 
			set @RecalcularIncidencias = 'S' 
		end 
		else
		begin 
			set @GlobalActivoConCedula = 'N'; 
			set @RecalcularIncidencias = 'N' 
		end; 

		exec SP_FONACOT_PAGO_RAZON_SOCIAL @FT_YEAR, @FT_MONTH, @GlobalActivoConCedula, @RecalcularIncidencias

		set @i = @i + 1 
	end 
	
	drop table #CalculosFonacot

end 

go
 
exec SP_FONACOT_RECALCULAR_FON_TOT_RS

go 

drop procedure SP_FONACOT_RECALCULAR_FON_TOT_RS

go 

/*17402 - BORRA DICCIONARIO VISTA HUELLAS */
DELETE FROM R_ENTIDAD WHERE EN_CODIGO = 330 AND EN_TITULO = 'Huellas'

