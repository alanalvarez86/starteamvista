/* Nuevo campo en tabla PCEPRESTAMOD_FON: 1/3 
0 No Aplica. 
1 Pago Directo en Fonacot
2 Confirmaci�n de Fonacot (  No Adeudo por Fonacot)
3 Ausente en C�dula. 
4 Otro.
*/
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('PRESTAMO') AND name='PR_FON_MOT'    )
     alter table PRESTAMO add PR_FON_MOT Status NULL 
go	
	update PRESTAMO set PR_FON_MOT = 0  where PR_FON_MOT is null 
go 

/* Nuevo campo en tabla PRESTAMO: 3/3 */
	alter table PRESTAMO alter column PR_FON_MOT Status NOT NULL 
go

IF exists( select * from sys.objects where object_id = object_id('VCEDTFON') )
    drop view VCEDTFON
GO

create view VCEDTFON(
    CB_CODIGO,   
	PF_YEAR, 
    PF_MES,
    CT_PAGO,   
    CT_NOMINA, 
	CT_DIF, 
	CT_DIF_QTY, 
	CT_CRE_QTY   )
as
select CB_CODIGO, PF_YEAR, PF_MES, coalesce(SUM(PF_PAGO),0.00)  CT_PAGO, coalesce(SUM(PF_NOMINA),0.00) CT_NOMINA, coalesce( SUM( PF_PAGO - PF_NOMINA),0.00) CT_DIF, 
coalescE( SUM(case when PF_PAGO > PF_NOMINA then 1 else 0 end), 0 ) CT_DIF_QTY , 
count(*) CT_CRE_QTY
from VCED_FON 
group by  CB_CODIGO, PF_YEAR, PF_MES

go 

/* Funci�n para obtener la c�dula de pago Fonacot: 1/2 */
if exists (select * from sysobjects where id = object_id(N'FONACOT_CEDULA') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FONACOT_CEDULA
go

/* Funci�n para obtener la c�dula de pago Fonacot: 2/2 */
CREATE FUNCTION FONACOT_CEDULA
(	@YEAR SMALLINT,
    @MES SMALLINT,
	@RAZON_SOCIAL CHAR(6), 
	@IgnorarIncapacidades Booleano)
RETURNS @CEDULA TABLE
	(
    NO_FONACOT varchar(30),
    RFC varchar(30),
	NOMBRE VARCHAR(250),
	NO_CREDITO varchar (8),
	RETENCION_MENSUAL numeric(15,2),
	CLAVE_EMPLEADO int,
	PLAZO smallint,
	MESES smallint,
	RETENCION_REAL numeric (15,2),
	INCIDENCIA char(1)	,
	FECHA_INI_BAJA varchar(10),
	FECHA_FIN varchar(10),
	REUBICADO CHAR(1)
	)
AS
BEGIN
	insert into @CEDULA
	( NO_FONACOT, RFC, NOMBRE, NO_CREDITO, RETENCION_MENSUAL, CLAVE_EMPLEADO, PLAZO, MESES, RETENCION_REAL,
		INCIDENCIA, FECHA_INI_BAJA, FECHA_FIN, REUBICADO)	

		SELECT C.CB_FONACOT, C.CB_RFC, UPPER (C.CB_APE_PAT + ' ' + C.CB_APE_MAT +' ' + C.CB_NOMBRES) AS PRETTYNAME, F.PR_REFEREN, F.PF_PAGO, C.CB_CODIGO, F.PF_PLAZO, F.PF_PAGADAS,	FC.FC_NOMINA,
				case					 
					 when @IgnorarIncapacidades = 'S' and FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY  then  '0' 					  
					 else FE.FE_INCIDE   
				end  FE_INCIDE , 	
				case 
					when CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) = '30/12/1899'  then '' 
					when @IgnorarIncapacidades = 'S' and FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY  then  '' 					  					
					else CONVERT (VARCHAR(10), FE.FE_FECHA1, 103) 
				end AS FECHA_INI_BAJA, 
				case 
					when CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) = '30/12/1899' then '' 
					when @IgnorarIncapacidades = 'S' and FE.FE_INCIDE = 'I' and CT.CT_DIF_QTY < CT.CT_CRE_QTY then  '' 					  					
					else CONVERT (VARCHAR(10), FE.FE_FECHA2, 103) 
				end AS FECHA_FIN, 
			     F.PF_REUBICA
	FROM PCED_FON F
		JOIN COLABORA C ON F.CB_CODIGO = C.CB_CODIGO
		JOIN FON_EMP FE ON FE.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FE.FT_YEAR AND F.PF_MES = FE.FT_MONTH
		JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_REFEREN = FC.PR_REFEREN
		JOIN VCEDTFON CT  ON CT.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = CT.PF_YEAR AND F.PF_MES = CT.PF_MES
		--LEFT OUTER JOIN FON_CRE FC ON FC.CB_CODIGO = C.CB_CODIGO AND F.PF_YEAR = FC.FT_YEAR AND F.PF_MES = FC.FT_MONTH AND F.PR_TIPO = FC.PR_TIPO  AND F.PR_REFEREN = FC.PR_REFEREN
	WHERE 
		F.PF_YEAR = @YEAR
		AND F.PF_MES = @MES
		AND C.CB_PATRON IN (SELECT TB_CODIGO FROM RPATRON WHERE RS_CODIGO = @RAZON_SOCIAL or @RAZON_SOCIAL = '')

	return
END
GO

/* Funci�n utilitaria  */
if exists (select * from sysobjects where id = object_id(N'FN_SPLIT_NUMBERED') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_SPLIT_NUMBERED
go

CREATE FUNCTION dbo.FN_SPLIT_NUMBERED(@String Formula, @Delimiter Codigo1) returns @temptable
 TABLE (idx int identity(1,1), items varchar(255)) as begin declare @idx int declare @slice varchar(255) 
 select @idx = 1 if len(@String)<1 or @String is null  return while @idx!= 0 begin set @idx = charindex(@Delimiter,@String) if @idx!=0 set @slice = left(@String,@idx - 1) else set @slice = @String if(len(@slice)>0) insert into @temptable(Items) values(@slice) set @String = right(@String,len(@String) - @idx) if len(@String) = 0 break end return end 

GO

/* Funci�n utilitaria  */
if exists (select * from sysobjects where id = object_id(N'FN_GET_LAST_STRING') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_GET_LAST_STRING
	

go

create  function FN_GET_LAST_STRING(@String Formula, @Delimiter Codigo1) returns Formula 
as
begin 
	declare @Return Formula 

	set @Return = (
	select top 1 items from dbo.FN_SPLIT_NUMBERED( @String,  @Delimiter )
	order by idx desc 
	)

	set @Return = coalesce( @Return , '' ) 

	return @Return  
end 


go 

/* Funci�n de Fonacto para tener una lista de empleados por aproximaci�n  */
if exists (select * from sysobjects where id = object_id(N'FN_FONACOT_GET_EMPLEADOS_POR_APROX') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_FONACOT_GET_EMPLEADOS_POR_APROX
go

create function FN_FONACOT_GET_EMPLEADOS_POR_APROX( @RFC varchar(255) ,  @Nombres varchar(255)  )
returns @Matches  
       table 
       (
             Orden int, 
             Seleccionado char(1), 
             CB_CODIGO int, 
             PRETTYNAME varchar(255), 
             DiffNombreCompleto int, 
             DiffApePat int, 
             DiffNombres int, 
             DiffRFC  int , 
             CB_RFC   varchar(30)
       ) 
as 
begin  

declare @ElementosStr Table 
(
       idx int, 
       item varchar(255) 
)

set @Nombres = LTRIM(RTRIM( @Nombres )) 
set @RFC  = LTRIM(RTRIM( @RFC )) 

if ( @Nombres = '' ) 
       return 

insert into @ElementosStr ( idx, item ) 
select idx, items from dbo.FN_SPLIT_NUMBERED(@Nombres, ' ' ) 

declare @Primero  varchar(255) 
declare @Ultimo  varchar(255) 

select @Primero = item from @ElementosStr where idx = 1 
select @Ultimo = item from @ElementosStr where idx = (select max(idx) from @ElementosStr ) 

set @Primero = coalesce( @Primero, '' ) 
set @Ultimo = coalesce( @Ultimo, '' ) 

if LTRIM(RTRIM( @Primero+@Ultimo )) <> '' and @RFC <> '' 
begin 

insert into @Matches ( Orden, Seleccionado, CB_CODIGO, PRETTYNAME, DiffNombreCompleto, DiffApePat, DiffNombres, DiffRFC , CB_RFC) 
select top 10  row_number() over ( order by DiffNombreCompleto desc, DiffApePat desc, DiffNombres desc ) as Orden , 
case when DiffNombreCompleto > 4 or DiffRFC > 4 then 'S' else 'N' end, 
CB_CODIGO, PRETTYNAME, DiffNombreCompleto, DiffApePat, DiffNombres , DiffRFC, CB_RFC   from  ( 
select CB_CODIGO, PRETTYNAME , 
DIFFERENCE(@Nombres, CB_APE_PAT + ' '+ CB_APE_MAT + ' ' + CB_NOMBRES) + 
case     
       when RTRIM( LTRIM( @Nombres )) = RTRIM( LTRIM( CB_APE_PAT + ' '+ CB_APE_MAT + ' ' + CB_NOMBRES))  then 4          
       when RTRIM( LTRIM( @Nombres )) = RTRIM( LTRIM( CB_APE_PAT + ' '+ CB_NOMBRES))  then 4 
       when RTRIM( LTRIM( @Nombres )) = RTRIM( LTRIM( CB_APE_PAT + ' '+ CB_APE_MAT))  then 4  
       else 0
end DiffNombreCompleto
, DIFFERENCE(@Primero, CB_APE_PAT) DiffApePat ,  DIFFERENCE(@Ultimo, CB_NOMBRES)  DiffNombres , DIFFERENCE( @RFC, CB_RFC ) 
+
case  
          when RTRIM( LTRIM( @RFC )) = RTRIM( LTRIM( CB_RFC))  then 4        
          else 0 
end 
DiffRFC , CB_RFC 
from COLABORA
where RTRIM(LTRIM(COLABORA.CB_FONACOT)) = '' and (  ( DIFFERENCE(@Nombres, CB_APE_PAT + ' '+ CB_APE_MAT + ' ' + CB_NOMBRES) >= 3   and  DIFFERENCE(@Primero, CB_APE_PAT) >=4  and  DIFFERENCE(@Ultimo, dbo.FN_GET_LAST_STRING( CB_NOMBRES, '') ) >=3    ) or ( LEFT( @RFC, 6) =  LEFT( CB_RFC, 6) ) )  
) Matches

end 

return 
end

go 



IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_AGREGAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_AGREGAR_UN_PRESTAMO
go
 
create procedure SP_FONACOT_AGREGAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_TIPO Codigo1, @PR_REFEREN Referencia, @PR_FECHA Fecha, @PR_STATUS status, @PR_OBSERVA ReferenciaLarga,  @US_CODIGO Usuario, @PF_PAGO Pesos) 
as
begin 
	set nocount on 

	if ( select count(*) from PRESTAMO where CB_CODIGO = @CB_CODIGO and PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ) = 0
	begin 
		INSERT INTO PRESTAMO (CB_CODIGO, PR_TIPO, PR_REFEREN, PR_FECHA, PR_STATUS, PR_OBSERVA, US_CODIGO, PR_MONTO, PR_TOTAL, PR_SALDO, PR_PAG_PER, PR_FON_MOT) 
		VALUES ( @CB_CODIGO, @PR_TIPO, @PR_REFEREN, @PR_FECHA, @PR_STATUS, @PR_OBSERVA, @US_CODIGO, @PF_PAGO, @PF_PAGO, @PF_PAGO , @PF_PAGO, 0 )
	end

end 
GO 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ACTUALIZAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_ACTUALIZAR_UN_PRESTAMO
go

create procedure SP_FONACOT_ACTUALIZAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1,  @iPR_STATUS status, @iUS_CODIGO Usuario, @lGrabarBitacora Booleano = 'S' ) 
as
begin 
    set nocount on 
       DECLARE @BI_TEXTO Observaciones
       DECLARE @BI_DATA NVARCHAR(MAX)
          DECLARE @BI_FEC_MOV Fecha
       DECLARE @PR_MONTO Pesos 
          DECLARE @PR_PAG_PER Pesos 

          set @PR_PAG_PER = dbo.FN_Fonacot_Get_Saldo_UltimaCedula( @CB_CODIGO, @PR_REFEREN, @PR_TIPO ) 
          SELECT @PR_MONTO = SUM(PF_PAGO) FROM PCED_FON WHERE CB_CODIGO = @CB_CODIGO AND PR_REFEREN = @PR_REFEREN AND PR_TIPO = @PR_TIPO 
          SET @PR_MONTO = COALESCE(@PR_MONTO, 0.00) 
       

       update PRESTAMO set PR_STATUS = @iPR_STATUS,  US_CODIGO= @iUS_CODIGO ,  PR_MONTO = @PR_MONTO, PR_TOTAL = @PR_MONTO, PR_SALDO = @PR_MONTO, PR_PAG_PER = @PR_PAG_PER , 
          PR_FON_MOT =  case                                       
                                        when @iPR_STATUS = 1 then 3 
                                        else
                                               0 
                                    end                                               
       where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO                     

       if ( @lGrabarBitacora  = 'S' ) 
       begin 
             SET @BI_TEXTO =  'Modificaci�n desde C�dula Fonacot: Ref:' + @PR_REFEREN;
             SET @BI_DATA = 'Pr�stamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + 'PR_STATUS' + CHAR(13)+CHAR(10) + ' A : ' + cast (@iPR_STATUS as varchar)
    
             SELECT @BI_FEC_MOV = GETDATE();
             exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;
       end; 

       
end

GO 
 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'RECALCULA_PRESTAMOS') AND type IN ( N'P', N'PC' ))
    drop procedure RECALCULA_PRESTAMOS
go

CREATE PROCEDURE RECALCULA_PRESTAMOS
                      @EMPLEADO INTEGER,
                    @STATUS SMALLINT,
                    @PR_TIPO Codigo1 = ''
AS
        SET NOCOUNT ON
    DECLARE @CONCEPTO INTEGER;
    DECLARE @NUMERO INTEGER;
    DECLARE @TOTAL NUMERIC(15,2);
    DECLARE @TIPOPRESTAMO CHAR(1);
    DECLARE @REFERENCIA VARCHAR(8);
    DECLARE @FECHAINICIAL DATETIME;
	
    declare @UsaModalidadFonacotPrestamo Booleano 
	declare @TipoPrestamoFonacot Codigo1

	select @UsaModalidadFonacotPrestamo = LEFT( LTRIM(GL_FORMULA), 1)  from GLOBAL where GL_CODIGO = 328 
	set @UsaModalidadFonacotPrestamo = COALESCE( @UsaModalidadFonacotPrestamo, 'N' ) 

	select @TipoPrestamoFonacot = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	set @TipoPrestamoFonacot = COALESCE( @TipoPrestamoFonacot, '' )  

    if ( @UsaModalidadFonacotPrestamo = 'S' )
    begin
        DECLARE TemporalPrestamo Cursor for
        select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT, P.PR_FECHA
        from PRESTAMO P 
        left outer join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO )
        where ( P.CB_CODIGO = @Empleado ) and ( P.PR_STATUS in ( 0, 2 ) ) and ( P.PR_TIPO = @PR_TIPO or @PR_TIPO = '' ) and ( P.PR_TIPO NOT IN ( @TipoPrestamoFonacot ) )
    end;
    else
    begin
        DECLARE TemporalPrestamo Cursor for
        select P.PR_TIPO, P.PR_REFEREN, T.TB_CONCEPT, P.PR_FECHA
        from PRESTAMO P 
        left outer join TPRESTA T on ( T.TB_CODIGO = P.PR_TIPO )
        where ( P.CB_CODIGO = @Empleado ) and ( P.PR_STATUS in ( 0, 2 ) ) and ( P.PR_TIPO = @PR_TIPO or @PR_TIPO = '' )
    end;
    
    OPEN TemporalPrestamo
    FETCH NEXT FROM TemporalPrestamo
    into @TipoPrestamo, @Referencia, @Concepto, @FechaInicial 
    WHILE @@FETCH_STATUS = 0
    begin
        Select @Numero = 0, @Total = 0;
        select @Numero = COUNT(*), @Total = SUM( M.MO_DEDUCCI )
        from MOVIMIEN M 
        left outer join NOMINA N on
               ( N.PE_YEAR = M.PE_YEAR ) and
               ( N.PE_TIPO = M.PE_TIPO ) and
               ( N.PE_NUMERO = M.PE_NUMERO ) and
               ( N.CB_CODIGO = @Empleado )
        left outer join PERIODO P on
               ( N.PE_YEAR = P.PE_YEAR ) and
               ( N.PE_TIPO = P.PE_TIPO ) and
               ( N.PE_NUMERO = P.PE_NUMERO )
        where ( M.CB_CODIGO = @Empleado ) and
                ( M.CO_NUMERO = @Concepto ) and
                ( M.MO_REFEREN = @Referencia ) and
                ( M.MO_ACTIVO = 'S' ) and
                ( N.NO_STATUS = @Status ) and
                ( P.PE_FEC_FIN >= @FechaInicial )
        if ( @Numero = 0 ) Select @Total = 0;
        update PRESTAMO  set
                 PR_NUMERO = @Numero,
                 PR_TOTAL  = @Total,
                 PR_SALDO = PR_MONTO - PR_SALDO_I - @Total - PR_ABONOS + PR_CARGOS
        where ( CB_CODIGO = @Empleado ) and
                ( PR_TIPO = @TipoPrestamo ) and
                ( PR_REFEREN = @Referencia );
        FETCH NEXT FROM TemporalPrestamo
        into @TipoPrestamo, @Referencia, @Concepto, @FechaInicial 
    end
    Close TemporalPrestamo
    Deallocate TemporalPrestamo
    EXECUTE SET_STATUS_PRESTAMOS @Empleado;
GO 

/* #16385: Funci�n en SQL FN_CALCULA_FON con la l�gica principal para la funci�n de Tress: CALC_FON. 1/2 */
if exists (select * from dbo.sysobjects where id = object_id(N'FN_CALCULA_FON') and xtype in (N'FN', N'IF', N'TF')) 
       drop function FN_CALCULA_FON
go 

/* #16385: Funci�n en SQL FN_CALCULA_FON con la l�gica principal para la funci�n de Tress: CALC_FON. 2/2 */
create function FN_CALCULA_FON 
	(@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1, @TipoAjuste NumeroEmpleado, @Suavizar Booleano, @AjusteMesAnterior Booleano,
	@AnioPeriodo Anio, @MesPeriodo Mes, @NumPeriodo NominaNumero, @TipoPeriodo NominaTipo)
RETURNS @Valores TABLE
	(
	   RETENCION_PERIODO Decimal(15,2), 
	   MONTO_CEDULA   Decimal(15,2), 
	   MONTO_RETENIDO Decimal(15,2), 
       MONTO_ESPERADO Decimal(15,2), 
       AJUSTE Decimal(15,2), 
       MONTO_CONCEPTO Decimal(15,2) ,
       DEUDA_MA Decimal(15,2) ,
	   NUM_PERIODO_MES int,
	   POS_PERIODO_MES int,
	   MES_ANTERIOR    int, 
	   YEAR_ANTERIOR   int 
	)
as
begin 
       declare @RETENCION_PERIODO Pesos 
	   declare @MONTO_CEDULA   Pesos 
       declare @MONTO_RETENIDO Pesos 
	   declare @MONTO_ESPERADO Pesos 
       declare @AJUSTE Pesos 
       declare @MONTO_CONCEPTO Pesos 
       declare @DEUDA_MA Pesos 
	   DECLARE @NUM_PERIODO_MES Empleados
	   DECLARE @POS_PERIODO_MES Empleados

	   DECLARE @MES_ANTERIOR Status
	   DECLARE @YEAR_ANTERIOR Anio
	   	   
	   -- OBTENER N�MERO DE PERIODO Y LA CANTIDAD DE PERIODOS DEL MES.	   
	   SELECT @POS_PERIODO_MES = PE_POS_MES, @NUM_PERIODO_MES = PE_PER_MES FROM PERIODO 
	   WHERE PE_NUMERO = @NumPeriodo AND PE_TIPO = @TipoPeriodo AND PE_YEAR = @AnioPeriodo;

	   SELECT @MONTO_CEDULA = PF_PAGO,  @MONTO_RETENIDO = PF_NOMINA FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodo AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN
	   set @MONTO_RETENIDO = coalesce( @MONTO_RETENIDO, 0.00 ) 


	   if (@MesPeriodo = 1 ) 
	   begin 
			set @MES_ANTERIOR = 12 
			set @YEAR_ANTERIOR = @AnioPeriodo - 1; 
	   end
	   else
	   begin 
			set @MES_ANTERIOR = @MesPeriodo - 1 ; 
			set @YEAR_ANTERIOR = @AnioPeriodo; 
	   end 
		

	   -- SI EL TIPO DE AJUSTE ES 1 (AJUSTAR CADA PERIODO) Y SE PIDE AJUSTAR EL MES ANTERIOR,
	   -- OBTENER AJUSTE DEL MES ANTERIOR.
	   IF (@TipoAjuste = 1) AND (@AjusteMesAnterior = 'S')
	   BEGIN
			SELECT @DEUDA_MA = (PF_PAGO-PF_NOMINA) FROM VCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @YEAR_ANTERIOR AND PF_MES = @MES_ANTERIOR  AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN 
			SET @DEUDA_MA = COALESCE (@DEUDA_MA, 0.000);
	   END
	   ELSE
			SET @DEUDA_MA = 0.000;
	    
	   -- AJUSTE.
	   -- 1. AJUSTA CADA PER�ODO.
	   -- 2. AJUSTA AL FINAL DE MES (�LTIMO PER�ODO).
	   IF (@TipoAjuste = 1) OR (@TipoAjuste = 2)
	   BEGIN
			-- OBTENER LA RETENCI�N POR PERIODO.
			IF @NUM_PERIODO_MES != 0
			BEGIN
				SELECT @RETENCION_PERIODO = ((PF_PAGO+@DEUDA_MA)/@NUM_PERIODO_MES) FROM PCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodo AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ;
				SET @RETENCION_PERIODO = coalesce( @RETENCION_PERIODO , 0.000 );
			END
			ELSE
			BEGIN
				SET @RETENCION_PERIODO = 0.0;
			END

			-- OBTENER MONTO ESPERADO.
			IF (@TipoAjuste = 1) OR (@TipoAjuste = 2 AND @POS_PERIODO_MES = @NUM_PERIODO_MES)
			BEGIN
				SET @MONTO_ESPERADO = (@POS_PERIODO_MES - 1)*@RETENCION_PERIODO;
				-- OBTENER EL AJUSTE,
				-- ES DECIR, LA DIFERENCIA ENTRE LO QUE DEBER�A LLEVAR DESCONTADO HASTA EL MOMENTO (@MONTO_ESPERADO)
				-- Y LO QUE EN REALIDAD LLEVA (VCED_FON.PF_NOMINA)				
				set @AJUSTE = (@MONTO_ESPERADO - @MONTO_RETENIDO ) 
				SET @AJUSTE = COALESCE (@AJUSTE, 0.000);
			END
			ELSE
			BEGIN
				SET @MONTO_ESPERADO = 0.000;
				SET @AJUSTE = 0.000;
			END

	   END	  
	   -- SIN AJUSTE.
	   ELSE
	   BEGIN
			SET @AJUSTE = 0;
			-- OBTENER LA RETENCI�N POR PERIODO.
			IF @NUM_PERIODO_MES != 0
			BEGIN
				SELECT @RETENCION_PERIODO = (PF_PAGO/@NUM_PERIODO_MES) FROM PCED_FON WHERE CB_CODIGO = @CB_CODIGO and PF_YEAR = @AnioPeriodo AND PF_MES = @MesPeriodo AND PR_TIPO = @PR_TIPO and PR_REFEREN = @PR_REFEREN ;
				SET @RETENCION_PERIODO = coalesce( @RETENCION_PERIODO , 0.000 );
			END
			ELSE
			BEGIN
				SET @RETENCION_PERIODO = 0.0
			END
	   END

	   -- Suavizado.
	   IF @Suavizar = 'S'
	   BEGIN
			SET @MONTO_CONCEPTO = @RETENCION_PERIODO +  (@AJUSTE/ ((@NUM_PERIODO_MES-@POS_PERIODO_MES)+1))
	   END
	   ELSE
	   BEGIN
			SET @MONTO_CONCEPTO = @RETENCION_PERIODO + @AJUSTE
	   END

	   IF @POS_PERIODO_MES = @NUM_PERIODO_MES
	   BEGIN
			IF ABS (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO)) < 1
			BEGIN
				SET @MONTO_CONCEPTO = @MONTO_CONCEPTO + (@MONTO_CEDULA - (@MONTO_RETENIDO + @MONTO_CONCEPTO))				
			END
	   END

	   insert into @Valores (
			   RETENCION_PERIODO, 
			   MONTO_CEDULA, 
			   MONTO_RETENIDO, 
			   MONTO_ESPERADO, 
			   AJUSTE, 
			   MONTO_CONCEPTO ,
			   DEUDA_MA, 
			   NUM_PERIODO_MES,
			   POS_PERIODO_MES,
			   MES_ANTERIOR ,  
			   YEAR_ANTERIOR
	    ) values ( 
			   @RETENCION_PERIODO, 
			   @MONTO_CEDULA, 
			   @MONTO_RETENIDO, 
			   @MONTO_ESPERADO, 
			   @AJUSTE, 
			   @MONTO_CONCEPTO ,
			   @DEUDA_MA, 
			   @NUM_PERIODO_MES,
			   @POS_PERIODO_MES,
			   @MES_ANTERIOR ,  
			   @YEAR_ANTERIOR
		)

       RETURN 
end
GO
