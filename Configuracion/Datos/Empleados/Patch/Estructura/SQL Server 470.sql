/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

/* Patch 470: #16384 */

if not exists( select NAME from SYSOBJECTS  where NAME='PCED_FON' and XTYPE='U' )
begin
	CREATE TABLE PCED_FON (
		   CB_CODIGO            NumeroEmpleado,
		   PR_TIPO              Codigo1,
		   PR_REFEREN           Referencia,
		   PF_YEAR              Anio,
		   PF_MES               Mes,
		   PF_PAGO              Pesos,
		   PF_PLAZO             FolioChico,
		   PF_PAGADAS           FolioChico,
		   PF_CAPTURA           Fecha,
		   US_CODIGO            Usuario
	)
	
	ALTER TABLE PCED_FON
	ADD CONSTRAINT PK_PCED_FON PRIMARY KEY (CB_CODIGO, PR_TIPO, PR_REFEREN, PF_YEAR, PF_MES)
	 
	ALTER TABLE PCED_FON
		   ADD CONSTRAINT FK_PCed_Fon_Prestamo
		   FOREIGN KEY (CB_CODIGO, PR_TIPO, PR_REFEREN)
		   REFERENCES PRESTAMO
		   ON DELETE CASCADE
		   ON UPDATE CASCADE;
end
GO

if exists (select * from dbo.sysobjects where id = object_id(N'FN_GetConceptoFonacot') and xtype in (N'FN', N'IF', N'TF')) 
	drop function FN_GetConceptoFonacot
go 

create function FN_GetConceptoFonacot() returns Concepto
as 
begin 
	declare @TipoPrestamoFonacot Codigo1
	declare @ConceptoNumero Concepto

	set @ConceptoNumero = 0

	select @TipoPrestamoFonacot = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	set @TipoPrestamoFonacot = COALESCE( @TipoPrestamoFonacot, '' ) 

	if (@TipoPrestamoFonacot <> '' ) 
		select  @ConceptoNumero = p.TB_CONCEPT from TPRESTA p where P.TB_CODIGO = @TipoPrestamoFonacot

	return @ConceptoNumero 
end 
GO 

IF exists( select * from sys.objects where object_id = object_id('VCED_FON') )
    drop view VCED_FON
GO

create view VCED_FON(
    CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
    US_CODIGO,
    PF_NOMINA, 
    PF_NOM_QTY )
as
select
    PCED_FON.CB_CODIGO,
    PR_TIPO,
    PR_REFEREN,
    PF_YEAR,
    PF_MES,
    PF_PAGO,
    PF_PLAZO,
    PF_PAGADAS,
    PF_CAPTURA,
    US_CODIGO,
    coalesce( Afectadas.PF_Monto, 0.00 ) , 
    coalesce( Afectadas.PF_Cuantos, 0 ) 
from PCED_FON
left outer join 
(
    select M.PE_YEAR, P.PE_MES,  NO.CB_CODIGO, M.MO_REFEREN, coalesce( SUM( M.MO_PERCEPC + M.MO_DEDUCCI ), 0.00 )  PF_Monto, count(*) PF_Cuantos from MOVIMIEN M 
      join NOMINA NO on 
              ( NO.PE_YEAR = M.PE_YEAR ) and
               ( NO.PE_TIPO = M.PE_TIPO ) and
               ( NO.PE_NUMERO = M.PE_NUMERO ) and 
               ( NO.CB_CODIGO  = M.CB_CODIGO ) 
      join PERIODO P on
               ( P.PE_YEAR = NO.PE_YEAR ) and
               ( P.PE_TIPO = NO.PE_TIPO ) and
               ( P.PE_NUMERO = NO.PE_NUMERO )
      where ( M.MO_ACTIVO = 'S' ) and ( NO.NO_STATUS >= 6 ) and M.CO_NUMERO = dbo.FN_GetConceptoFonacot()
    group by  M.PE_YEAR, P.PE_MES,  NO.CB_CODIGO, M.MO_REFEREN
) Afectadas on Afectadas.PE_YEAR = PF_YEAR and Afectadas.PE_MES = PCED_FON.PF_MES and  Afectadas.CB_CODIGO = PCED_FON.CB_CODIGO and Afectadas.MO_REFEREN = PCED_FON.PR_REFEREN 
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_INSERTAR_BITACORA') AND type IN ( N'P', N'PC' ))
    drop procedure SP_INSERTAR_BITACORA
go

CREATE PROCEDURE SP_INSERTAR_BITACORA
 ( 
    @US_CODIGO Usuario, 
    @BI_PROC_ID Status, 
    @BI_TIPO Status,
    @BI_NUMERO FolioGrande, 
    @BI_TEXTO Observaciones, 
    @CB_CODIGO NumeroEmpleado, 
    @BI_DATA Memo, 
    @BI_CLASE Status, 
    @BI_FEC_MOV Fecha 
) 
AS 
BEGIN 
    DECLARE @BI_FECHA Fecha
    DECLARE @BI_HORA HoraMinutosSegundos
    SET @BI_FECHA = getdate()
    SET @BI_HORA = SUBSTRING( CONVERT(CHAR(24),@BI_FECHA,113), 13,8 ) 
    SET @BI_FECHA = CONVERT(CHAR(10),@BI_FECHA,110) 
INSERT INTO BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_PROC_ID, BI_TIPO, 
         BI_NUMERO, BI_TEXTO, CB_CODIGO, BI_DATA, BI_CLASE, BI_FEC_MOV)
VALUES ( @US_CODIGO, @BI_FECHA, @BI_HORA, @BI_PROC_ID, @BI_TIPO, @BI_NUMERO, @BI_TEXTO, @CB_CODIGO,
        @BI_DATA, @BI_CLASE, @BI_FEC_MOV ) 
        
END
GO
 
IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_ACTUALIZAR_UN_PRESTAMO') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_ACTUALIZAR_UN_PRESTAMO
go
 
create procedure SP_FONACOT_ACTUALIZAR_UN_PRESTAMO (@CB_CODIGO NumeroEmpleado, @PR_REFEREN Referencia, @PR_TIPO Codigo1,  @iPR_STATUS status, @iUS_CODIGO Usuario) 
as
begin 
    set nocount on 
    DECLARE @BI_TEXTO Observaciones
    DECLARE @BI_DATA NVARCHAR(MAX)
    DECLARE @BI_FEC_MOV Fecha
    update PRESTAMO set PR_STATUS = @iPR_STATUS,  US_CODIGO= @iUS_CODIGO where CB_CODIGO = @CB_CODIGO and PR_REFEREN = @PR_REFEREN and PR_TIPO = @PR_TIPO 
    
    SET @BI_TEXTO =  'Modificación desde Cédula Fonacot: Ref:' + @PR_REFEREN;
    SET @BI_DATA = 'Préstamo: Empl.:' + cast (@CB_CODIGO as varchar) + ' Tipo:' + @PR_TIPO + ' Ref:' + @PR_REFEREN + CHAR(13)+CHAR(10) + 'PR_STATUS' + CHAR(13)+CHAR(10) + ' A : ' + cast (@iPR_STATUS as varchar)
    
    SELECT @BI_FEC_MOV = GETDATE();
    exec SP_INSERTAR_BITACORA @iUS_CODIGO, 0, 0, 0, @BI_TEXTO, @CB_CODIGO, @BI_DATA, 62, @BI_FEC_MOV;
end
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SP_FONACOT_CANCELAR_PRESTAMOS') AND type IN ( N'P', N'PC' ))
    drop procedure SP_FONACOT_CANCELAR_PRESTAMOS
go

create procedure SP_FONACOT_CANCELAR_PRESTAMOS (@iUS_CODIGO Usuario) 
as
begin
    set nocount on 
    DECLARE @CONCEPTO_FONACOT Codigo1;
    DECLARE @Prestamos TABLE
    (
        i int identity(1,1),
        iCB_CODIGO int, 
        sPR_REFEREN varchar(8) 
    )
    SELECT @CONCEPTO_FONACOT = GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 211
    insert into @Prestamos ( iCB_CODIGO , sPR_REFEREN ) 
        select CB_CODIGO, PR_REFEREN  from PRESTAMO where PR_TIPO = @CONCEPTO_FONACOT and PR_STATUS in (0) 
    declare @i int = 1 
    declare @n int 
    declare @iCB_CODIGO NumeroEmpleado 
    declare @sPR_REFEREN Referencia 
    select @n = count(*) from @Prestamos 
    while @i <= @n 
    begin          select @iCB_CODIGO = iCB_CODIGO , @sPR_REFEREN = sPR_REFEREN from @Prestamos where i = @i 
        
        exec SP_FONACOT_ACTUALIZAR_UN_PRESTAMO @iCB_CODIGO, @sPR_REFEREN, @CONCEPTO_FONACOT, 1, @iUS_CODIGO 
                
        set @i = @i + 1 
    end 
end
GO

IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'SET_STATUS_PRESTAMOS') AND type IN ( N'P', N'PC' ))
    drop procedure SET_STATUS_PRESTAMOS
go

create  procedure SET_STATUS_PRESTAMOS( @EMPLEADO Integer )
as
begin
     SET NOCOUNT ON

	 declare @DescartarPrestamosFonacot Booleano 
	 declare @UsaModalidadFonacotPrestamo Booleano 
	 declare @TipoPrestamoFonacot Codigo1

	 select @UsaModalidadFonacotPrestamo = LEFT( LTRIM(GL_FORMULA), 1)  from GLOBAL where GL_CODIGO = 328 
	 set @UsaModalidadFonacotPrestamo = COALESCE( @UsaModalidadFonacotPrestamo, 'N' ) 

	 select @TipoPrestamoFonacot = LEFT( LTRIM(GL_FORMULA) , 1)  from GLOBAL where GL_CODIGO = 211 
	 set @TipoPrestamoFonacot = COALESCE( @TipoPrestamoFonacot, '' ) 

	 set @DescartarPrestamosFonacot = 'N' 
	 if ( @TipoPrestamoFonacot <> ''  and @UsaModalidadFonacotPrestamo = 'S' ) 
		set @DescartarPrestamosFonacot = 'S' 


     update DBO.PRESTAMO set PR_STATUS = 2 where			
            ( PR_STATUS = 0 ) and 
			( @DescartarPrestamosFonacot = 'N' or ( @DescartarPrestamosFonacot = 'S' and PR_TIPO <> @TipoPrestamoFonacot )  ) and 
            ( CB_CODIGO = @Empleado ) and
            ( ( ( PR_MONTO >= 0 ) and ( PR_SALDO < 0.01 ) ) or
            ( ( PR_MONTO < 0 ) and ( PR_SALDO > -0.01 ) ) );

     update DBO.PRESTAMO set PR_STATUS = 0 where
            ( PR_STATUS = 2 ) and 
			( @DescartarPrestamosFonacot = 'N' or ( @DescartarPrestamosFonacot = 'S' and PR_TIPO <> @TipoPrestamoFonacot )  ) and 
            ( CB_CODIGO = @Empleado ) and
            ( ( ( PR_MONTO >= 0 ) and ( PR_SALDO >= 0.01 ) ) or
            ( ( PR_MONTO < 0 ) and ( PR_SALDO <= -0.01 ) ) );
end
go 


IF EXISTS ( SELECT  * FROM    sys.objects WHERE   object_id = OBJECT_ID(N'KARDEX_FONACOT') AND type IN ( N'P', N'PC' ))
    drop procedure KARDEX_FONACOT
go

CREATE PROCEDURE KARDEX_FONACOT( 
	@CB_CODIGO  Integer,
	@CB_FONACOT Descripcion,
	@US_CODIGO  SmallInt,
	@Resultado  SmallInt OUTPUT)
AS
BEGIN
  SET NOCOUNT ON
  declare @iNivel SmallInt;
  declare @sComenta VarChar( 50 );
  declare @sTipoKardex Char( 6 );
  declare @CB_NOTA Varchar(max);
  DECLARE @FECHA Fecha;
  DECLARE @CB_COMENTA VarChar( 50 );

  SET @sTipoKardex = 'EVENTO'; 
  SET @CB_NOTA = 'CB_FONACOT:=' + @CB_FONACOT
  SET @FECHA = CAST (GETDATE() AS DATE);
  SET @CB_COMENTA = 'Importación datos Fonacot';

  update COLABORA 
	set CB_FONACOT = @CB_FONACOT
    where ( CB_CODIGO = @CB_CODIGO );  

  SET @sComenta = @CB_COMENTA
  EXEC @Resultado = GET_COMENTA_TKARDEX @sTipoKardex, @sComenta OUTPUT, @iNivel OUTPUT

  delete from KARDEX
  where ( CB_CODIGO = @CB_CODIGO ) and
        ( CB_FECHA = @FECHA ) and
        ( CB_TIPO = @sTipoKardex );

  insert into KARDEX ( CB_CODIGO,
                       CB_FECHA,
                       CB_TIPO,
                       CB_NIVEL,
                       CB_COMENTA,
					   CB_NOTA,
                       CB_FEC_CAP,
                       CB_GLOBAL,
                       US_CODIGO )
   values ( @CB_CODIGO,
                   @FECHA,
                   @sTipoKardex,
                   @iNivel,
                   @sComenta,
				   @CB_NOTA,
                   @FECHA,
                   'S',
                   @US_CODIGO );

    EXECUTE RECALCULA_KARDEX @CB_CODIGO
END
GO