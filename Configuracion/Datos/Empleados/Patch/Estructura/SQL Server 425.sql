/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$   Base De Datos DATOS      $$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/
/*$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$*/

/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 1 */
	Alter table KARINF add KI_D_EXT Codigo; --Extensi�n del archivo
Go
/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 2 */
	UPDATE KARINF SET KI_D_EXT = '' where KI_D_EXT is null
GO
/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 3 */
	ALTER TABLE KARINF ALTER COLUMN KI_D_EXT Codigo Not Null
GO

/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 4 */
	Alter table KARINF add KI_D_NOM Descripcion; --Nombre del documento
Go
/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 5 */
	UPDATE KARINF SET KI_D_NOM= '' WHERE KI_D_NOM is Null 
Go
/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 6 */
	ALTER TABLE KARINF ALTER COLUMN KI_D_NOM Descripcion Not Null;
Go

/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 7 */
	Alter table KARINF add KI_D_BLOB Imagen; --binaria del documento
Go

/* TABLA KARINF: JB : Se actualiza tabla para poder abrir documento de Infonavit - TASK 951 - CR 1842*/
/* Patch 425 # Seq: 8 */
	Alter table KARINF add KI_NOTA Memo; --Notas del documento
Go

/* TABLA EVENTO: JB Se agrega columna EV_TIPNOM a EVENTO - 1840 Desarrollo Cambio de Tipo de N�mina Eventos */
/* Patch 425 # Seq: 9 */
	Alter table EVENTO add EV_TIPNOM NominaTipo
GO
/* TABLA EVENTO: JB Se agrega columna EV_TIPNOM a EVENTO - 1840 Desarrollo Cambio de Tipo de N�mina Eventos */
/* Patch 425 # Seq: 10 */
	update EVENTO set EV_TIPNOM = 0 where EV_TIPNOM is Null
GO
/* TABLA EVENTO: JB Se agrega columna EV_TIPNOM a EVENTO - 1840 Desarrollo Cambio de Tipo de N�mina Eventos */
/* Patch 425 # Seq: 11 */
	alter table EVENTO alter column EV_TIPNOM NominaTipo Not Null
GO

/* TABLA EVENTO: JB Se agrega columna EV_CAMBNOM a EVENTO - 1840 Desarrollo Cambio de Tipo de N�mina Eventos */
/* Patch 425 # Seq: 12 */
	Alter table EVENTO add EV_CAMBNOM Booleano;
GO
/* TABLA EVENTO: JB Se agrega columna EV_CAMBNOM a EVENTO - 1840 Desarrollo Cambio de Tipo de N�mina Eventos */
/* Patch 425 # Seq: 13 */
	update EVENTO set EV_CAMBNOM = 'N' where EV_CAMBNOM is null
GO
/* TABLA EVENTO: JB Se agrega columna EV_CAMBNOM a EVENTO - 1840 Desarrollo Cambio de Tipo de N�mina Eventos */
/* Patch 425 # Seq: 14 */
	alter table EVENTO alter column EV_CAMBNOM Booleano Not Null;
GO

/* TABLA GLOBAL: JB Se agrega GLOBAL 290 - 1831 :Procesa Credenciales Invalidas*/
/* Patch 425 # Seq: 15 */
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 290 )
   INSERT INTO GLOBAL (GL_CODIGO,GL_DESCRIP,GL_FORMULA,GL_TIPO,US_CODIGO,GL_CAPTURA,GL_NIVEL) 
   VALUES (290,'Procesa Credenciales Inv�lidas','S',0,0,GETDATE(),0)
GO 

/* TABLA RPATRON Se agrega campo TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 16 */
	ALTER TABLE RPATRON ADD TB_ACTIVO Booleano 
GO
/* TABLA RPATRON Asigna valor default TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 17 */
	UPDATE RPATRON SET TB_ACTIVO = 'S' WHERE TB_ACTIVO IS NULL 
GO
/* TABLA RPATRON Asigna valor default TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 18 */
	alter table RPATRON alter column TB_ACTIVO Booleano Not Null
GO

/* TABLA TAHORRO Se agrega campo TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 19 */
	ALTER TABLE TAHORRO ADD TB_ACTIVO Booleano 
GO
/* TABLA TAHORRO Asigna valor default TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 20 */
	UPDATE TAHORRO SET TB_ACTIVO = 'S' WHERE TB_ACTIVO IS NULL 
GO
/* TABLA TAHORRO Asigna valor default TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 21 */
	alter table TAHORRO alter column TB_ACTIVO Booleano Not Null
GO

/* TABLA TPRESTA Se agrega campo TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 22 */
	ALTER TABLE TPRESTA ADD TB_ACTIVO Booleano 
GO
/* TABLA TPRESTA Asigna valor default TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 23 */
	UPDATE TPRESTA SET TB_ACTIVO = 'S' WHERE TB_ACTIVO IS NULL 
GO 
/* TABLA TPRESTA Asigna valor default TB_ACTIVO - 1837 : Validacion campos activos*/
/* Patch 425 # Seq: 24 */
	alter table TPRESTA alter column TB_ACTIVO Booleano Not Null
GO

/* TABLA CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq: 25 */
	ALTER TABLE CONCEPTO ADD CO_ISN BOOLEANO;
GO
/* TABLA CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq: 26*/
	UPDATE CONCEPTO set CO_ISN = 'N' where CO_ISN is NULL
GO
/* TABLA CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq: 27*/
	alter table CONCEPTO alter column CO_ISN Booleano Not Null
GO

/* TABLA CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq: 28 */
	ALTER TABLE NOMINA ADD NO_PER_ISN Pesos;
GO
/* TABLA CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  29 */
	UPDATE NOMINA set NO_PER_ISN = 0 where NO_PER_ISN is NULL
GO
/* TABLA CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  30 */
	alter table NOMINA alter column NO_PER_ISN Pesos Not Null
GO


/* AGREGA_CONCEPTO_SISTEMA: Agrega un concepto de sistema cuando no existe (CR 1878: Calculo Impuesto Estatal) (1/3) */
/* Patch 425 # Seq:  31 */
create procedure AGREGA_CONCEPTO_SISTEMA( @Concepto Smallint, @Descripcion VARCHAR(30) )
as
begin
     SET NOCOUNT ON;
     declare @Cuantos Integer;
     select @Cuantos = COUNT(*) from CONCEPTO where( CO_NUMERO = @Concepto );
     if ( @Cuantos = 0 )
     begin
          insert into CONCEPTO( CO_NUMERO, CO_DESCRIP, CO_ACTIVO ) values ( @Concepto, @Descripcion, 'S' );
     end
end
GO
/* AGREGA_CONCEPTO_SISTEMA: Agrega el concepto 1012 (CR 1878: Calculo de Impuesto Estatal ) (2/3)*/
/* Patch 425 # Seq:  32 */
execute AGREGA_CONCEPTO_SISTEMA 1012, 'Monto Gravado para ISN';
GO
/* AGREGA_CONCEPTO_SISTEMA: Borra procedure (CR 1878 Calculo de Impuesto Estatal) (3/3)*/
/* Patch 425 # Seq:  33 */
drop procedure AGREGA_CONCEPTO_SISTEMA;
GO
 
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  34 */
CREATE TABLE MAQUINA
(
	MQ_CODIGO Codigo,	/*Codigo de la m�quina*/
	MQ_NOMBRE Titulo,	/*Nombre de la m�quina*/
	MQ_INGLES  Descripcion, /*Ingles M�quina*/
	MQ_TEXTO Descripcion, /*Texto de la m�quina*/
	MQ_NUMERO Pesos, /*Numero de la m�quina*/
	MQ_RESUMEN Formula, /*Resumen de la m�quina*/
	MQ_IMAGEN Imagen,	/*Imagen de la m�quina*/
	MQ_TMAQUIN Codigo,	/*Tipo de M�quina*/
	MQ_MULTIP Booleano,
	LLAVE Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  35 */
ALTER TABLE MAQUINA
       ADD CONSTRAINT PK_MAQUINA PRIMARY KEY (MQ_CODIGO);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  36 */ /*Certtificaciones que tiene la maquina*/
CREATE TABLE MAQ_CERT
(
	MQ_CODIGO Codigo,	/*Codigo M�quina*/
	CI_CODIGO Codigo, /*Certificacion */
	MC_FECHA Fecha,   /*Fecha de Configuracion*/
	US_CODIGO Usuario,  /*Usuario que configuro*/
	LLAVE Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  37 */
ALTER TABLE MAQ_CERT
       ADD CONSTRAINT PK_MAQ_CERT PRIMARY KEY (MQ_CODIGO,CI_CODIGO);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  38 */
ALTER TABLE MAQ_CERT
       ADD CONSTRAINT FK_MAQUINA_MAQ_CERT
       FOREIGN KEY (MQ_CODIGO)
       REFERENCES MAQUINA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  39 */
ALTER TABLE MAQ_CERT
       ADD CONSTRAINT FK_CERTIFICACION_MAQ_CERT
       FOREIGN KEY (CI_CODIGO)
       REFERENCES CERTIFIC
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  40 */ /*Lineas que se esta usando la maquina*/
CREATE TABLE MAQ_AREA
(
	MQ_CODIGO Codigo,	/*Codigo M�quina*/
	AR_CODIGO Codigo,	/*Area */
	MA_FECHA Fecha,   /*Fecha de la asignacion*/
	US_CODIGO Usuario,  /*Usuario quien configuro*/
	LLAVE Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  41 */ /* Cat�logo de Tipos de M�quinas*/
CREATE TABLE TMAQUINA (
       TB_CODIGO            Codigo,     /*C�digo del Tipo Maquina*/
       TB_ELEMENT           Descripcion,/*Descripci�n del Tipo Maquina*/ 
       TB_INGLES            Descripcion,/*Ingl�s del Tipo Maquina*/
       TB_NUMERO            Pesos,		/*N�mero del Tipo Maquina*/
       TB_TEXTO             Descripcion,	/*Texto del Tipo Maquina*/
	   LLAVE				Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  42 */
ALTER TABLE TMAQUINA
       ADD CONSTRAINT PK_TMAQUINA PRIMARY KEY (TB_CODIGO);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  43 */ /* Cat�logo de Layouts de Producci�n*/
CREATE TABLE LAY_PRO_CE (
       LY_CODIGO            Codigo,     /*C�digo del layout Producci�n*/
       LY_NOMBRE            Descripcion,/*Descripcion del Layout*/
       LY_INGLES	        Descripcion,/*Ingles del Layout*/
	   LY_NUMERO	        Pesos,		/*N�mero del Layout*/
	   LY_TEXTO	            Descripcion,/*Texto del Layout*/
	   LY_AREA              Codigo,		/*Area/Linea de Aplicacion del Layout*/
	   LY_FECHA		        Fecha,		/*Fecha de ultima modificaci�n*/
	   US_CODIGO		    Usuario,		/*Usuario quien modific�*/
	   LLAVE				Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  44 */
ALTER TABLE LAY_PRO_CE
       ADD CONSTRAINT PK_LAY_PRO_CE PRIMARY KEY (LY_CODIGO);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  45 */ /* Asignaciones de Maquina Espacio en el Layout*/
CREATE TABLE LAY_MAQ (
       LY_CODIGO            Codigo,		/*C�digo del layout Producci�n*/
       MQ_CODIGO            Codigo,		/*C�digo de la m�quina asignada*/
       LM_POS_X             FolioGrande,/*Posici�n en X */
       LM_POS_Y				FolioGrande,/*Posici�n en Y */
	   LLAVE				Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  46 */
ALTER TABLE LAY_MAQ
       ADD CONSTRAINT FK_MAQUINA_LAY_MAQ
       FOREIGN KEY (MQ_CODIGO)
       REFERENCES MAQUINA
       ON UPDATE CASCADE;
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  47 */
ALTER TABLE LAY_MAQ
       ADD CONSTRAINT FK_LAYOUT_LAY_MAQ
       FOREIGN KEY (LY_CODIGO)
       REFERENCES LAY_PRO_CE
	   ON DELETE CASCADE	
       ON UPDATE CASCADE;
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  48 *//* Sillas en el Layout*/
CREATE TABLE SILLA_LAY (
       SL_CODIGO            Codigo,		/*C�digo de la Silla  */
       LY_CODIGO            Codigo,		/*C�digo de la m�quina asignada*/
       SL_POS_X             FolioGrande,/*Posici�n en X */
       SL_POS_Y				FolioGrande,/*Posici�n en Y */
	   LLAVE 				Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  49 */
ALTER TABLE SILLA_LAY
       ADD CONSTRAINT FK_LAYOUT_SILLA_LAY
       FOREIGN KEY (LY_CODIGO)
       REFERENCES LAY_PRO_CE
	   ON DELETE CASCADE	
       ON UPDATE CASCADE;
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  50 */ /* Asignaciones de Sillas- Maquina en el Layout*/
CREATE TABLE SILLA_MAQ (
       LY_CODIGO            Codigo,		/*C�digo del layout Producci�n*/
       MQ_CODIGO            Codigo,		/*C�digo de la m�quina asignada*/
       SL_CODIGO			Codigo,			/*C�digo de la silla*/
	   LLAVE				Int IDENTITY (1, 1)
);
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  51 */
ALTER TABLE SILLA_MAQ
       ADD CONSTRAINT FK_MAQUINA_SILLA_MAQ
       FOREIGN KEY (MQ_CODIGO)
       REFERENCES MAQUINA
       ON UPDATE CASCADE;
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  52 */
ALTER TABLE SILLA_MAQ
       ADD CONSTRAINT FK_LAYOUT_SILLA_MAQ
       FOREIGN KEY (LY_CODIGO)
       REFERENCES LAY_PRO_CE
	   ON DELETE CASCADE
       ON UPDATE CASCADE;
GO
/* Programacion Layout de Producci�n (CR 1854: Layout de Produccion) */
/* Patch 425 # Seq:  53 */ /*Kardex de asignacion de maquinas*/
create table KAR_EMPSIL 
(
	CB_CODIGO Empleados, /*Empleado asignado*/
	SL_CODIGO Codigo,	 /*M�quina de asignaci�n*/
	LY_CODIGO Codigo,	 /*Plantilla de Producci�n*/
	EM_FECHA Fecha,		 /*Fecha de asignaci�n*/
	EM_HORA Hora,		 /*Hora de asignaci�n*/
	EM_HOR_MOV Hora,	 /*Hora del movimiento*/		
	EM_FEC_MOV Fecha,	 /*Fecha del movimiento*/
	US_CODIGO Usuario,	 /*Usuario quien modific�*/
	LLAVE int identity(1,1)
);
GO
/* Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* Tabla para almacenar la Balanza de Movimientos Mensual */
/* Patch 425 # Seq: 54 */
CREATE TABLE TMPBALAMES (
       --Llave primaria
       TM_USER              	Usuario,			--Usuario que genera el reporte 
       CB_CODIGO            	NumeroEmpleado,               --N�mero de empleado
       TM_CODIGO            	FolioChico,                  	--Campo para control interno de la balanza
       
       --Datos de la Balanza, Percepciones
       TM_NUM_PER           	Concepto,			--N�mero Concepto Percepci�n
       TM_DES_PER           	Descripcion,                  --Descripci�n Percepci�n
       TM_MON_PER           	Pesos,                        --Monto Percepci�n
       TM_HOR_PER           	Horas,                        --Horas Percepci�n
       TM_REF_PER           	Referencia,                   --Referencia Percepci�n
                                                       
       --Datos de la Balanza,  Deducciones
       TM_NUM_DED           	Concepto,			--N�mero Concepto Deducci�n
       TM_DES_DED           	Descripcion,                  --Descripci�n Deducci�n
       TM_MON_DED           	Pesos,                        --Monto Deducci�n 
       TM_REF_DED           	Referencia,                   --Referencia Deducci�n
       TM_HOR_DED           	Horas,                        --Horas Deducci�n
                                                            
       --Datos del periodo                                      
       PE_YEAR              	Anio,			--A�o de periodo 
       PE_TIPO              	NominaTipo,                   --Tipo de periodo 
       PE_NUMERO            	NominaNumero,                 --Ultima nomina ordinaria
       PE_MES		Mes                         	--Mes(es) para obtener el recibo mensual
)
go
/* Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* Tabla para almacenar la Balanza de Movimientos Mensual */
/* Patch 425 # Seq: 55 */
ALTER TABLE TMPBALAMES
       ADD CONSTRAINT PK_TMPBALAMES PRIMARY KEY (TM_USER, CB_CODIGO, TM_CODIGO)
GO
/* Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* Cambios en Catalogos de Conceptos */
/* Patch 425 # Seq: 56 */
ALTER TABLE CONCEPTO ADD CO_SUMRECI BOOLEANO;
GO
/* Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* Cambios en Catalogos de Conceptos */
/* Patch 425 # Seq: 57 */
UPDATE CONCEPTO set CO_SUMRECI = 'S' where CO_NUMERO < 1000 and CO_SUMRECI is NULL
GO
/* Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* Cambios en Catalogos de Conceptos */
/* Patch 425 # Seq: 58 */
UPDATE CONCEPTO set CO_SUMRECI = 'N' where CO_NUMERO >= 1000 and CO_SUMRECI is NULL
GO
/* Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* Cambios en Catalogos de Conceptos */
/* Patch 425 # Seq: 59 */
alter table CONCEPTO alter column CO_SUMRECI Booleano Not Null
GO

/* TABLA KARDEX: Campos especiales de IDSE  - TASK  - CR */
/* Patch 425 # Seq: 60 */
	Alter table KARDEX add CB_LOT_IDS DescLarga; --# de Lote de transmisi�n IDSE
GO
/* TABLA KARDEX: Campos especiales de IDSE  - TASK  - CR */
/* Patch 425 # Seq: 61 */
	UPDATE KARDEX SET CB_LOT_IDS = '' WHERE CB_LOT_IDS is NULL 
GO
/* TABLA KARDEX: Campos especiales de IDSE  - TASK  - CR */
/* Patch 425 # Seq: 62 */
	alter table KARDEX alter column CB_LOT_IDS DescLarga Not Null
GO
 
/* TABLA KARDEX: Campos especiales de IDSE  - TASK  - CR */
/* Patch 425 # Seq: 63 */
	Alter table KARDEX add CB_FEC_IDS Fecha; --# de Fecha de transacci�n IDSE
GO
/* TABLA KARDEX: Campos especiales de IDSE  - TASK  - CR */
/* Patch 425 # Seq: 64 */
	update KARDEX set CB_FEC_IDS = '12/30/1899' where CB_FEC_IDS is Null;  --# de Fecha de transacci�n IDSE
GO
/* TABLA KARDEX: Campos especiales de IDSE  - TASK  - CR */
/* Patch 425 # Seq: 65 */
	Alter table KARDEX alter column CB_FEC_IDS Fecha Not Null;  --# de Fecha de transacci�n IDSE
GO

/* Se agrega la entidad V_ENROLA en el Diccionario de Datos */
/* es necesario borrar la entidad de COLABORA de la tabla de V_USUARIO */
/* si no existiera esa relacion no pasa nada */
/* Patch 425 # Seq: 66 */
delete R_RELACION where EN_CODIGO = 85 and RL_ENTIDAD = 10

/* Patch 425 # Seq: 67 */
ALTER TABLE NIVEL1 ADD US_CODIGO Usuario
GO 
 /* Patch 425 # Seq: 68 */
UPDATE NIVEL1 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go
/* Patch 425 # Seq: 69 */
alter table NIVEL1 alter column US_CODIGO Usuario NOT NULL
GO

/* Patch 425 # Seq:  70*/
ALTER TABLE NIVEL2 ADD US_CODIGO Usuario
GO 
/* Patch 425 # Seq:  71*/
UPDATE NIVEL2 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go
/* Patch 425 # Seq:  72*/
alter table NIVEL2 alter column US_CODIGO Usuario NOT NULL
GO

/* Patch 425 # Seq:  73*/
ALTER TABLE NIVEL3 ADD US_CODIGO Usuario
GO
/* Patch 425 # Seq:  74*/
UPDATE NIVEL3 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go 
/* Patch 425 # Seq:  75*/
alter table NIVEL3 alter column US_CODIGO Usuario NOT NULL
GO
 
/* Patch 425 # Seq:  76*/
ALTER TABLE NIVEL4 ADD US_CODIGO Usuario
GO
/* Patch 425 # Seq:  77*/
UPDATE NIVEL4 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go
/* Patch 425 # Seq:  78*/
alter table NIVEL4 alter column US_CODIGO Usuario NOT NULL
GO

/* Patch 425 # Seq:  79*/
ALTER TABLE NIVEL5 ADD US_CODIGO Usuario
GO
/* Patch 425 # Seq:  80*/
UPDATE NIVEL5 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go 
/* Patch 425 # Seq:  81*/
alter table NIVEL5 alter column US_CODIGO Usuario NOT NULL
GO
 
/* Patch 425 # Seq:  82*/
ALTER TABLE NIVEL6 ADD US_CODIGO Usuario
GO
/* Patch 425 # Seq:  83*/
UPDATE NIVEL6 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go 
/* Patch 425 # Seq:  84*/
alter table NIVEL6 alter column US_CODIGO Usuario NOT NULL
GO
 
/* Patch 425 # Seq:  85*/
ALTER TABLE NIVEL7 ADD US_CODIGO Usuario
GO
/* Patch 425 # Seq:  86*/
UPDATE NIVEL7 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go 
/* Patch 425 # Seq:  87*/
alter table NIVEL7 alter column US_CODIGO Usuario NOT NULL
GO
 
/* Patch 425 # Seq:  88*/
ALTER TABLE NIVEL8 ADD US_CODIGO Usuario
GO
/* Patch 425 # Seq:  89*/
UPDATE NIVEL8 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go 
/* Patch 425 # Seq:  90*/
alter table NIVEL8 alter column US_CODIGO Usuario NOT NULL
GO
 
/* Patch 425 # Seq:  91*/
ALTER TABLE NIVEL9 ADD US_CODIGO Usuario
go 
/* Patch 425 # Seq:  92*/
UPDATE NIVEL9 SET US_CODIGO = 0 WHERE US_CODIGO IS NULL 
go
/* Patch 425 # Seq:  93*/
alter table NIVEL9 alter column US_CODIGO Usuario NOT NULL
GO

/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  94*/
CREATE TABLE KAR_FIJA2 (
       CB_CODIGO            NumeroEmpleado,
       CB_FECHA             Fecha,
       CB_TIPO              Codigo,
       KF_FOLIO             FolioChico,
       KF_CODIGO            Codigo2,
       KF_MONTO             PesosDiario,
       KF_GRAVADO           PesosDiario,
       KF_IMSS              Status,
	   LLAVE int identity(1,1)
)
go

/* Cambios para la sugerencia Integracion ZK     */
/* Cambios en Catalogos de HUELLA */
/* Patch 425 # Seq:  95*/
CREATE TABLE HUELLA
(
	CB_CODIGO Empleados,
	NO_HUELLA FolioChico,
	HUELLA Memo,
	LLAVE int identity(1,1)
)
GO

/* Cambios para la sugerencia Integracion ZK     */
/* Cambios en Catalogos de HUELLA */
/* Patch 425 # Seq:  96*/
ALTER TABLE HUELLA
	ADD CONSTRAINT PK_HUELLA PRIMARY KEY (CB_CODIGO, NO_HUELLA);
GO

/* Cambios para la sugerencia Integracion ZK     */
/* Cambios en Catalogos de HUELLA */
/* Patch 425 # Seq:  97*/
ALTER TABLE HUELLA
	ADD CONSTRAINT FK_HUELLA FOREIGN KEY (CB_CODIGO) REFERENCES COLABORA (CB_CODIGO) 
		ON DELETE CASCADE
		ON UPDATE CASCADE

GO

/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  98*/
Create Table BIT_MIG
(
	BM_FEC_EVE Fecha,
	BM_DESCRIP Formula,
	BM_DETALLE Memo
)
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  99*/
----------------------------------------------------------------------------------------------------
CREATE TABLE EV_ALTA (
       EV_CODIGO      Evento,
       EV_DESCRIP     Descripcion,
       EV_ACTIVO      Booleano,
       CB_CODIGO      NumeroEmpleado,
       EV_CONTRAT     Codigo,
       EV_BAJA        Booleano,
       EV_PUESTO      Codigo,
       EV_CLASIFI     Codigo,
       EV_TURNO       Codigo,
       EV_PATRON      RegPatronal,
       EV_NIVEL1      Codigo,
       EV_NIVEL2      Codigo,
       EV_NIVEL3      Codigo,
       EV_NIVEL4      Codigo,
       EV_NIVEL5      Codigo,
       EV_NIVEL6      Codigo,
       EV_NIVEL7      Codigo,
       EV_NIVEL8      Codigo,
       EV_NIVEL9      Codigo,
       EV_NIVEL0      Codigo,
       EV_AUTOSAL     Codigo1,
       EV_SALARIO     PesosDiario,
       EV_PER_VAR     PesosDiario,
       EV_ZONA_GE     ZonaGeo,
       EV_TABLASS     Codigo1,
       EV_OTRAS_1     Codigo2,
       EV_OTRAS_2     Codigo2,
       EV_OTRAS_3     Codigo2,
       EV_OTRAS_4     Codigo2,
       EV_OTRAS_5     Codigo2,
       EV_BAN_ELE     Booleano,
       EV_NOMINA      NominaTipo,
	   LLAVE int identity(1,1)
)
go

/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  100*/
ALTER TABLE EV_ALTA
       ADD CONSTRAINT PK_EV_ALTA PRIMARY KEY (EV_CODIGO)

/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  101*/
CREATE TABLE ESCENARIO(
	ES_CODIGO Codigo, 
	ES_ELEMENT Descripcion,
	ES_INGLES Descripcion,
	ES_NUMERO Pesos,
	ES_TEXTO Descripcion,
	LLAVE int IDENTITY(1,1)
)
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  102*/
ALTER TABLE ESCENARIO
       ADD CONSTRAINT PK_ESCENARIO PRIMARY KEY ( ES_CODIGO )
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  103*/
IF not exists (SELECT * FROM ESCENARIO WHERE ES_CODIGO = 'DEF')
	INSERT INTO ESCENARIO (ES_CODIGO, ES_ELEMENT, ES_INGLES, ES_TEXTO)
	VALUES     ('DEF', 'Default', 'Default', 'Default')

/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  104*/
CREATE TABLE SUPUEST_RH (
	SR_FECHA	      Fecha,
	SR_TIPO           Status,
	SR_CUANTOS		  Empleados,
	EV_CODIGO	      Evento,
	US_CODIGO	      Usuario,
	LLAVE int IDENTITY(1,1)
)
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  105*/
	ALTER TABLE SUPUEST_RH ADD CONSTRAINT PK_SUPUEST_RH PRIMARY KEY (SR_FECHA,SR_TIPO,EV_CODIGO)
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  106*/
	ALTER TABLE SUPUEST_RH ADD ES_CODIGO Codigo -- (JB) Escenarios de presupuestos T1060 CR1872
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  107*/
	UPDATE SUPUEST_RH SET ES_CODIGO = 'DEF' where ES_CODIGO = '' or ES_CODIGO is null
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  108*/
	alter table SUPUEST_RH alter column ES_CODIGO Codigo Not Null
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  109*/
    ALTER TABLE SUPUEST_RH DROP CONSTRAINT PK_SUPUEST_RH 
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  110*/
	ALTER TABLE SUPUEST_RH ADD CONSTRAINT PK_SUPUEST_RH PRIMARY KEY (ES_CODIGO,SR_FECHA,SR_TIPO,EV_CODIGO)
GO


/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  111*/
CREATE TABLE TOTPRESUP(
	PE_YEAR ANIO,
	PE_MES MES,
	PE_TIPO NOMINATIPO,
	CO_NUMERO CONCEPTO,
	TP_MONTO PESOSDIARIO,
	TP_EMPLEAD EMPLEADOS,
	CB_PUESTO CODIGO,
	CB_CLASIFI CODIGO,
	CB_TURNO CODIGO,
	CB_NIVEL1 CODIGO,
	CB_NIVEL2 CODIGO,
	CB_NIVEL3 CODIGO,
	CB_NIVEL4 CODIGO,
	CB_NIVEL5 CODIGO,
	CB_NIVEL6 CODIGO,
	CB_NIVEL7 CODIGO,
	CB_NIVEL8 CODIGO,
	CB_NIVEL9 CODIGO,
	CB_NIVEL0 CODIGO,
	LLAVE int identity(1,1)
)
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  112*/
ALTER TABLE TOTPRESUP ADD ES_CODIGO Codigo -- (JB) Escenarios de presupuestos T1060 CR1872
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  113*/
UPDATE TOTPRESUP SET ES_CODIGO = 'DEF' where ES_CODIGO = '' or ES_CODIGO is null
GO
/* (JB) Se agregan KAR_FIJA2, BIT_MIG, TOTPRESUP, SUPUEST_RH, EV_ALTA para Presupuestos T1059 */
/* Patch 425 # Seq:  114*/
ALTER TABLE TOTPRESUP ALTER COLUMN ES_CODIGO Codigo Not Null

/* (JB) Anexar al expediente del trabajador documentos CR1880 T1107 */
/* Patch 425 # Seq:  115*/-- 
CREATE TABLE DOCUMENTO(
	CB_CODIGO  NumeroEmpleado,
	DO_TIPO    CodigoEmpresa,
	DO_BLOB    Imagen,
	DO_NOMBRE  Formula,
	DO_EXT     Codigo,
	DO_OBSERVA Descripcion,
	LLAVE      Int IDENTITY(1,1)
)
go
/* (JB) Anexar al expediente del trabajador documentos CR1880 T1107 */
/* Patch 425 # Seq:  116*/-- 
ALTER TABLE DOCUMENTO
       ADD CONSTRAINT PK_DOCUMENTO PRIMARY KEY ( CB_CODIGO, DO_TIPO ) 
GO



/****************************************/
/*************** VIEWS      *************/
/****************************************/

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/*    Comienza: Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Patch 425 # Seq: 117 */
/* Comienza: View de soporte para generar la relacion entre la Balanza de Movimientos Mensual y Nomina */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[V_NOMMES]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[V_NOMMES]
go
/* Patch 425 # Seq: 118 */
/* Comienza: View de soporte para generar la relacion entre la Balanza de Movimientos Mensual y Nomina */
CREATE VIEW V_NOMMES
AS
select * from NOMINA

GO

/* Comienza: View de soporte para generar la relacion entre la Usuario y Colabora */
/* Patch 425 # Seq: 119 */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[V_ENROLA]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[V_ENROLA]
go
/* Patch 425 # Seq: 120 */
/* Comienza: View de soporte para generar la relacion entre la Balanza de Movimientos Mensual y Nomina */
CREATE VIEW V_ENROLA
AS
select * from COLABORA

go

/* Patch 425 # Seq: 121 */
/* CR 1675 : Cambios en Cursos Programados  */
if exists (select * from dbo.sysobjects where id = object_id(N'CUR_PROG') and OBJECTPROPERTY(id, N'IsView') = 1)
	drop view CUR_PROG
GO

/* Patch 425 # Seq:  122*/
/* CR 1675 : Cambios en Cursos Programados  */
CREATE VIEW CUR_PROG (
  CB_CODIGO, 
  CB_PUESTO, 
  CU_CODIGO, 
  KC_FEC_PRO, 
  KC_EVALUA, 
  KC_FEC_TOM, 
  KC_HORAS, 
  CU_HORAS, 
  EN_OPCIONA, 
  EN_LISTA, 
  MA_CODIGO, 
  KC_PROXIMO, 
  KC_REVISIO, 
  CU_REVISIO,
  CP_MANUAL,
  EP_GLOBAL
) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
 ( Select case when (KARCURSO.KC_FEC_TOM IS NULL )then DATEADD( DAY, E.EN_DIAS, C.CB_FEC_ING ) else KARCURSO.KC_FECPROG end )KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_MANUAL,
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' ) 
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CU.CU_CODIGO,
  ( select case when K.KC_FEC_TOM is null then NULL
		        when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) OR ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' )and ( K.KC_REVISIO = CU.CU_REVISIO ) ) )then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' ) and ( K.KC_FEC_TOM > ( select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) ) ) and (K.KC_REVISIO <> CU.CU_REVISIO ))then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				else (select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) )End ) KC_FEC_PRO,
  cast(0 as Decimal(15,2)) KC_EVALUA,
  cast(NULL as DateTime) KC_FEC_TOM,
  cast(0 as Decimal(15,2)) KC_HORAS,
  CU.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CU.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS DateTime))) KC_PROXIMO,
  cast(' ' as varchar(10))KC_REVISIO,               
  CU.CU_REVISIO,                  
  cast( 'N' as CHAR(1) ) CP_MANUAL,  
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO K on ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CU_CODIGO = E.CU_CODIGO ) and ( K.KC_FEC_TOM = ( select MAX( KC_FEC_TOM ) from KARCURSO AS K1 where ( K1.CB_CODIGO = C.CB_CODIGO ) and ( K1.CU_CODIGO = E.CU_CODIGO ) ))
  left join CURSO CU on ( CU.CU_CODIGO = E.CU_CODIGO )
where ( Cu.CU_ACTIVO = 'S' ) and 
		( ( E.EN_RE_DIAS > 0 ) AND NOT ( K.KC_FEC_TOM is NULL) or
        ( ( E.EN_REPROG = 'S' ) and ( K.KC_REVISIO <> CU.CU_REVISIO ) and ( 
          ( select count(k2.KC_REVISIO) FROM karcurso k2 where k2.KC_REVISIO = CU.CU_REVISIO AND ( k2.CB_CODIGO = C.CB_CODIGO ) and ( k2.CU_CODIGO = E.CU_CODIGO ) ) = 0 ) ))
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_ING + EP.EP_DIAS else EP.EP_FECHA end	)KC_FEC_PRO,
  K.KC_EVALUA,
  K.KC_FEC_TOM,
  K.KC_HORAS,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM dbo.SESION WHERE (CU_CODIGO = EP.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  K.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_MANUAL,	
  EP.EP_GLOBAL
  from COLABORA C
  join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
  left join KARCURSO K on ( K.CU_CODIGO = EP.CU_CODIGO and EP.CB_CODIGO = K.CB_CODIGO and K.KC_FEC_TOM >= (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_ING + EP.EP_DIAS else EP.EP_FECHA end	) )
where ( CURSO.CU_ACTIVO = 'S' )
GO 

/* TABLA TMPNOM: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq: 123 */
	ALTER TABLE TMPNOM ADD NO_PER_ISN Pesos;
GO

/* TABLA TMPNOM: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  124 */
	UPDATE TMPNOM set NO_PER_ISN = 0 where NO_PER_ISN is NULL
GO

/* TABLA TMPNOM: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  125 */
	alter table TMPNOM alter column NO_PER_ISN Pesos Not Null
GO

/* TABLA R_ATRIBUTO: JB : Se agrega campo de AT_FILTROA ya que presupuestos marca error. - 
   TASK 951 - CR 1842 */
/* Patch 425 # Seq:  126 */
   if not exists (select 1 from syscolumns where name = 'AT_FILTROA')
	begin
		Alter table R_ATRIBUTO add AT_FILTROA Descripcion; 
	end
Go

/* TABLA R_ATRIBUTO: JB : Se inicializa el campo en caso de que tenga nulos. - 
   TASK 951 - CR 1842 */
/* Patch 425 # Seq:  127 */
   UPDATE R_ATRIBUTO set AT_FILTROA = '' where AT_FILTROA is null
Go

/* TABLA colabora: JB : Se realiza actualizacion de US_CODIGO ya que en la transferencia de 
   empleados y en migracion de presupuestos marca error. - TASK 951 - CR 1842*/
  /* Patch 425 # Seq:  128 */
	UPDATE colabora SET US_CODIGO = 0 WHERE US_CODIGO IS NULL
Go

/* TABLA GLOBAL: JB Se agrega GLOBAL 2074 Valor Da�os a Vivienda*/
/* Patch 425 # Seq: 129 */
if not EXISTS ( select 1 from GLOBAL where GL_CODIGO = 2074 )
	INSERT INTO GLOBAL (GL_CODIGO, GL_DESCRIP, GL_FORMULA, GL_TIPO, US_CODIGO, GL_CAPTURA,GL_NIVEL)
	VALUES (2074,'Seguro de da�os a Vivienda','15',1,0,GETDATE(),0);
GO 

/*(CV) CR1726, Confidencialidad configurable en Corporativa*/
/* Patch 425 # Seq: 130 */
alter table R_ENTIDAD add EN_NIVEL0 BOOLEANO
GO

/*(CV) CR1726, Confidencialidad configurable en Corporativa*/
/* Patch 425 # Seq: 131 */
update R_ENTIDAD set EN_NIVEL0='N' where EN_NIVEL0 is NULL
go

/*(CV) CR1726, Confidencialidad configurable en Corporativa*/
/* Patch 425 # Seq: 132 */
update R_ENTIDAD set EN_NIVEL0='S' 
where EN_CODIGO in(	  	1,2,5,4,3,8,10,23,28,29,32,33,31,34,36,40,50,52,53,
55,57,70,89,91,92,95,107,108,110,111,112,114,115,
116,131,132,133,134,143,144,149,150,153,118,145,155,161,162,169,
170,171,172,174,183,206,207,210,213,217,218,282,75,286,260,256,
243,283,284,255,263,280,271,97,244,245,242,87,90,290,291,293,
246,247,292,81,85,266,327,326,314,315)
go

/*(CV) Clasificacion Presupuestos de N�mina*/
/* Patch 425 # Seq: 133 */
update R_CLASIFI set RC_NOMBRE = 'Presupuestos de N�mina' where RC_NOMBRE = 'Presupuesto de Personal'
go

/*(EZ) Clasificacion Mis Datos */
/* Patch 425 # Seq: 134 */
create procedure AGREGA_CLASIFICACION(
@NOMBRE varchar(30)
)as
begin
    declare @MaxClasifi int;
    declare @Orden int;
    select @MaxClasifi =  max(RC_CODIGO) from R_CLASIFI where RC_CODIGO<999;
    select @Orden =  max(RC_ORDEN) from R_CLASIFI where RC_CODIGO<999;
    set @MaxClasifi = @MaxClasifi + 1;
    set @Orden = @Orden + 1;
    if not Exists(select RC_NOMBRE from R_CLASIFI where RC_NOMBRE = @NOMBRE)
       insert into R_CLASIFI (RC_CODIGO,RC_NOMBRE,RC_ACTIVO,RC_ORDEN,RC_VERSION) Values (@MaxClasifi , @NOMBRE,'S',@Orden,425);
end;
go

/*(EZ) Clasificacion Mis Datos */
/* Patch 425 # Seq: 135 */
exec AGREGA_CLASIFICACION 'Mis Datos';
go

/*(EZ) Clasificacion Mis Datos */
/* Patch 425 # Seq: 136 */
exec AGREGA_CLASIFICACION 'Env�o Email Empleados';
go

/*(EZ) Clasificacion Mis Datos */
/* Patch 425 # Seq: 137 */
drop procedure AGREGA_CLASIFICACION;
go

/*(EZ) Clasificacion Mis Datos */
/* Patch 425 # Seq: 137 */  
/*Bitacora de Mis Datos*/          
CREATE VIEW V_BITMISD( CB_CODIGO,
                        CM_CODIGO,
                        BI_FECHA,
                        BI_HORA,
                        BI_TIPO,
                        BI_ACCION,
                        BI_NUMERO,
                        BI_TEXTO,
                        BI_UBICA,
                        BI_KIOSCO,
                        BI_FEC_MOV,
                        BI_FEC_HOR )
as
  SELECT           CB_CODIGO,
                   CM_CODIGO,
                   BI_FECHA,
                   BI_HORA,
                   BI_TIPO,
                   BI_ACCION,
                   BI_NUMERO,
                   BI_TEXTO,
                   BI_UBICA,
                   BI_KIOSCO,
                   BI_FEC_MOV,
                   BI_FEC_HORA

            FROM #COMPARTE.dbo.BITKIOSCO
GO

					   
/*****************************************/
/***************PROCEDURES *************/
/***************************************/
if exists (select * from sysobjects where name = 'SP_TOTALES_DIAS_AUSENTISMO')
	drop PROCEDURE SP_TOTALES_DIAS_AUSENTISMO
go

/* PROCEDURE SP_TOTALES_DIAS_AUSENTISMO: JB : TASK 991 - CR 1517*/
/* Patch 425 # Seq:  297*/
	CREATE PROCEDURE SP_TOTALES_DIAS_AUSENTISMO (
		@CLASIFI VARCHAR(30),  
		@NIVEL VARCHAR(30),  
		@LISTA VARCHAR(30),  
		@YEAR INT  
	)
	AS
	BEGIN

		DECLARE @QUERY VARCHAR(8000); 
		DECLARE @EJECUCION VARCHAR(8000); 
		DECLARE @CLASIFIQUERY VARCHAR(8000); 

		IF @NIVEL <> '-100' AND @LISTA <> '-100' 
		BEGIN 
			SET @NIVEL = 'CB_NIVEL' + @NIVEL
			SELECT @QUERY = ' and COLABORA.' + @NIVEL + ' = ''' + @LISTA + '''' 
		END 
		ELSE 
			SELECT @QUERY = ''; 
		
		IF @CLASIFI <> '-100' and @CLASIFI <> '-1' and @CLASIFI is not null
			SELECT @CLASIFIQUERY = ' and COLABORA.CB_CLASIFI = ''' + @CLASIFI + ''' ' 
		ELSE 
			SELECT @CLASIFIQUERY = ''; 
		
		set @EJECUCION = '
		select VA.AC_MES, DBO.TF( 61, VA.AC_MES-1 ) Mes,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1107 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) Trabajadas,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1100 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) FI,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1103 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoSG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1102 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoCG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1113 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) VAC,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND VP.AC_MES = VA.AC_MES AND VP.CO_NUMERO = 1106 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) INC
			from V_ACUMULA VA
			where (VA.AC_YEAR = ' + convert(varchar(4), @YEAR) + ')
			and VA.AC_MES<>13
			group by VA.AC_YEAR, VA.AC_MES
			union
			select 14, ''Total'',
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1107 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) Trabajadas,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1100 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) FI,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1103 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoSG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1102 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) PermisoCG,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1113 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) VAC,
				( select SUM(VP.AC_MONTO) from V_ACUMULA VP left outer join COLABORA ON (COLABORA.CB_CODIGO=VP.CB_CODIGO) where VP.AC_YEAR = VA.AC_YEAR AND  VP.CO_NUMERO = 1106 ' + @CLASIFIQUERY + ' ' + @QUERY+'  ) INC
			from V_ACUMULA VA 
			where (VA.AC_YEAR = ' + convert(varchar(4), @YEAR) + ')
			and VA.AC_MES<>13
			group by VA.AC_YEAR'
		
		EXEC(@EJECUCION)
	END
GO

/* Cambios para la correccion EdoCtaAhorro, CR_1793      */
/* Comienza: procedure SP_GET_DETALLE_AHORROS */
/* Patch 425 # Seq: 289 */
if exists (select * from sysobjects where name = 'SP_GET_DETALLE_AHORROS')
	drop PROCEDURE SP_GET_DETALLE_AHORROS
go
/* Cambios para la correccion EdoCtaAhorro, CR_1793      */
/* Comienza: procedure SP_GET_DETALLE_AHORROS */
/* Patch 425 # Seq: 289 */
CREATE PROCEDURE SP_GET_DETALLE_AHORROS
@CB_CODIGO INT,
@LLAVE INT
AS
BEGIN
	SET NOCOUNT ON
	
	/* CONSTANTES para los Querys*/
	DECLARE
	@TIPODENTRONOMINA 	CHAR(1), 	/* Indica si el movimiento se realizo por n�mina('M') */
	@TIPOFUERANOMINA 	CHAR(1),	/* Indica si el movimiento se realizo por fuera de la n�mina('A') */
	@PERIODOSTATUS		SMALLINT,	/* Valor del estatus del periodo de n�mina */
	@MOVIMIENTOACTIVO	CHAR(1),	/* Indica el tipo de movimientos que se validaran */
	@USUARIO_PORTAL		INT,		/* Usuario fijo para la consulta en la tabla de TMPESTAD, este usuario no existe en la tabla de Comparte.Usuario */
	@US_CODIGO			INT,			/* Se utiliza -1 como constante, solo para realizar la consulta */
	@AHORRO_TIPO		CHAR(1)

	/*Se inicializan las constantes*/
	SELECT 	@USUARIO_PORTAL	= 5000,
			@TIPODENTRONOMINA='M',
			@TIPOFUERANOMINA ='A',
			@PERIODOSTATUS =6,
			@MOVIMIENTOACTIVO='S',
			@US_CODIGO=-1

	/*VARIABLES en las que se almacena la informaci�n que se va a procesar en el cursor*/	
	DECLARE

	/*Variables del cursor LISTA_AHORROS_CURSOR*/
	@L_CB_CODIGO 	INT,
	@L_AH_TIPO 		CHAR(1),
	@L_AH_FECHA 	DATETIME,
	@TB_CONCEPT 	NUMERIC(15,2),
	@L_AH_SALDO_I 	NUMERIC(15,2),
	@L_AH_SALDO 	NUMERIC(15,2),
	@L_AH_MONTO 	NUMERIC(15,2),
	@L_AH_REFEREN 	VARCHAR(8),
	@L_US_CODIGO	SMALLINT,
	@L_AH_ABONOS	NUMERIC(15,2),
	@L_AH_CARGOS	NUMERIC(15,2),
	@L_AH_TOTAL		NUMERIC(15,2),
	@L_AH_NUMERO	SMALLINT,
	@L_AH_STATUS 	SMALLINT,
	@L_AH_STATUS_DES VARCHAR(8),
	
	/*Variables del cursor MOVIMIENTOS_AHORRO_CURSOR*/
	@D_CB_CODIGO	INT, 
	@D_TE_TIPO		CHAR(1),
	@D_TE_REFEREN	VARCHAR(8),
	@D_PE_FEC_INI	DATETIME,	
	@D_MO_PERCEPC	NUMERIC(15,2),
	@D_MO_DEDUCCI	NUMERIC(15,2),
	@D_US_CODIGO	SMALLINT,
	@D_PE_YEAR		SMALLINT,
	@D_PE_TIPO		CHAR(1),
	@D_PE_NUMERO	SMALLINT, 
	@D_CO_NUMERO	SMALLINT,
	@D_TIPO			CHAR(1),

	/*Variables utilizadas para los calculos*/	
	@TE_CODIGO		INT,
	@TE_CODIGO_DET	INT ,
	@SALDO			NUMERIC(15,2),
	@DESCRIPCION 	VARCHAR(30),
	@NOMINA_DESC	VARCHAR(30),
	@REFERENCIA		INT,
	@ALTERN_ROW		INT,
	@FECHA_PERIODO	DATETIME

	/*Se inicializan las variables de calculos*/
	SELECT @AHORRO_TIPO=AH_TIPO FROM AHORRO WHERE LLAVE= @LLAVE
	SELECT	@TE_CODIGO=0,@TE_CODIGO_DET=0,@SALDO=0,@REFERENCIA=1,@ALTERN_ROW=0

/*----------------------------------------------------------------------------------*/
/*	1.- SE ELIMINAN TODOS LOS REGISTROS DE LA TABLA TEMPESTAD DEL USUARIO DE PORTAL */
/*----------------------------------------------------------------------------------*/
	DELETE FROM TMPESTAD WHERE TE_USER = @USUARIO_PORTAL
/*----------------------------------------------------------------------------------*/
/*	2.- SE CREA TABLA TEMPORAL*/
/*------------------------------------------------------------------------------------*/
	CREATE TABLE #TMP_AHORROS
	(
		[TE_USER] 		SMALLINT NOT NULL ,
		[TE_CODIGO] 	SMALLINT NOT NULL ,
		[TE_FECHA] 		DATETIME NOT NULL ,
		[TE_CARGO] 		NUMERIC(15,2) NOT NULL ,
		[TE_ABONO] 		NUMERIC(15,2) NOT NULL ,
		[TE_SALDO] 		NUMERIC(15,2) NOT NULL ,
		[US_CODIGO] 	SMALLINT NOT NULL ,
		[TE_DESCRIP] 	VARCHAR(30) NOT NULL ,
		[CB_CODIGO] 	INT NOT NULL ,
		[TE_TIPO] 		CHAR(6) NOT NULL ,
		[TE_REFEREN] 	VARCHAR(8) NOT NULL,
		[A_ANIOS_ANT] 	NUMERIC(15,2) NOT NULL ,
		[OTROS_ABONOS] 	NUMERIC(15,2) NOT NULL ,
		[OTROS_CARGOS] 	NUMERIC(15,2) NOT NULL ,
		[SALDO_ACTUAL] 	NUMERIC(15,2) NOT NULL ,
		[AH_TOTAL] 		NUMERIC(15,2) NOT NULL ,
		[AH_NUMERO] 	SMALLINT NOT NULL ,
		[AH_STATUS] 	SMALLINT NOT NULL ,
		[AH_STATUS_DES] VARCHAR(10),
		[TIPO_REGISTRO] VARCHAR(10),
		[ALTERN_ROW] 	INT
	)
/*----------------------------------------------------------------------------*/
/*	3.- SE OBTIENE LA FECHA DEL ULTIMO PERIODO PARA EL EMPLEADO                 */
/*------------------------------------------------------------------------------*/
		SELECT @FECHA_PERIODO=(SELECT MAX(PE.PE_FEC_FIN) MAXIMO
								    FROM PERIODO PE
									INNER JOIN NOMINA N
										ON PE.PE_YEAR=N.PE_YEAR
										AND PE.PE_TIPO=N.PE_TIPO
										AND PE.PE_NUMERO=N.PE_NUMERO
									INNER JOIN TURNO T
										ON N.CB_TURNO=T.TU_CODIGO
									WHERE PE.PE_STATUS = @PERIODOSTATUS
										AND N.CB_CODIGO= @CB_CODIGO)
/*----------------------------------------------------------------------------*/
/*	4.- CURSOR PARA OBTENER EL LISTADO DE LOS AHORROS DEL EMPLEADO             */
/*------------------------------------------------------------------------------*/

	DECLARE  LISTA_AHORROS_CURSOR CURSOR FOR
		SELECT 	A.CB_CODIGO,		A.AH_TIPO,		A.AH_FECHA,
				T.TB_CONCEPT,	 	A.AH_SALDO_I,	A.AH_SALDO,	0 AS 'AH_MONTO',
				'' AS AH_REFEREN,	A.US_CODIGO,	A.AH_ABONOS,
	        	A.AH_CARGOS,		A.AH_TOTAL,		A.AH_NUMERO,
				A.AH_STATUS,
				'A.AH_STATUS_DES'=
				CASE
					WHEN A.AH_STATUS=0 THEN 'Activo'
					WHEN A.AH_STATUS=1 THEN 'Saldado'
					WHEN A.AH_STATUS=2 THEN 'Cancelado'
				END
		FROM AHORRO A
		INNER JOIN TAHORRO T
			ON  A.AH_TIPO= T.TB_CODIGO
		WHERE 	A.CB_CODIGO=@CB_CODIGO
			AND A.AH_TIPO=@AHORRO_TIPO
			AND A.AH_FECHA<(@FECHA_PERIODO)
			AND ((A.AH_ABONOS+AH_CARGOS<>0)
			OR (A.AH_NUMERO<>0 ))
		ORDER BY A.CB_CODIGO,
				 A.AH_TIPO

	OPEN LISTA_AHORROS_CURSOR

	FETCH NEXT FROM LISTA_AHORROS_CURSOR
	INTO 	@L_CB_CODIGO ,	@L_AH_TIPO,		@L_AH_FECHA,	@TB_CONCEPT ,
			@L_AH_SALDO_I,	@L_AH_SALDO,	@L_AH_MONTO,  	@L_AH_REFEREN,
			@L_US_CODIGO,	@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_TOTAL,
			@L_AH_NUMERO,	@L_AH_STATUS,	@L_AH_STATUS_DES

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SELECT  @TE_CODIGO	=@TE_CODIGO	+ 1,
				@SALDO		=@L_AH_MONTO + @L_AH_SALDO_I

	/*Se inserta un registro por cada ahorro en la tabla TempStad*/
		INSERT INTO TMPESTAD (TE_CODIGO,	CB_CODIGO,		TE_TIPO,		TE_FECHA,
							 TE_DESCRIP,	TE_CARGO,		TE_ABONO,		TE_SALDO,
							 US_CODIGO,		TE_USER,		TE_REFEREN )
					VALUES 	(@TE_CODIGO, 	@L_CB_CODIGO,	@L_AH_TIPO,		@L_AH_FECHA,
							'SALDO INICIAL',0, 	@L_AH_SALDO_I,	@SALDO,
							@US_CODIGO, 	@USUARIO_PORTAL, @REFERENCIA)

	/*Se inserta registro en la tabla temporal para reporte final, este registro contiene los totales del ahorro*/
	INSERT INTO #TMP_AHORROS(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
							TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,
							US_CODIGO,		TE_USER,		TE_REFEREN,		A_ANIOS_ANT,
							OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	AH_TOTAL,
							AH_NUMERO,		AH_STATUS,		AH_STATUS_DES,	TIPO_REGISTRO,
							ALTERN_ROW)
					VALUES	(@TE_CODIGO, 	@L_CB_CODIGO, 	@L_AH_TIPO,		@L_AH_FECHA,
							'SALDO INICIAL',0, 	0,				@SALDO,
							@US_CODIGO, 	@USUARIO_PORTAL,@REFERENCIA,	@L_AH_SALDO_I,
							@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_SALDO,	@L_AH_TOTAL,
							@L_AH_NUMERO,	@L_AH_STATUS ,	@L_AH_STATUS_DES,'ENCABEZADO',
							@ALTERN_ROW)
/*---------------------------------------------------------------------------------------*/
/*		5.- CURSOR PARA OBTENER EL DETALLE DEL AHORRO EN REVISIO(MOVIMIENTOS_AHORRO_CURSOR) */
/*		EL listado obtenido son los movimientos que ha realizado el empleado en su ahorro   */
/*	------------------------------------------------------------------------------------------*/
		DECLARE MOVIMIENTOS_AHORRO_CURSOR CURSOR FOR
			SELECT 	TMPESTAD.CB_CODIGO,
					TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					PERIODO.PE_FEC_INI PE_FEC_INI,
					MOVIMIEN.MO_PERCEPC MO_PERCEPC,
					MOVIMIEN.MO_DEDUCCI MO_DEDUCCI,
					MOVIMIEN.US_CODIGO,
					NOMINA.PE_YEAR,
					NOMINA.PE_TIPO,
					NOMINA.PE_NUMERO,
					MOVIMIEN.CO_NUMERO,
					@TIPODENTRONOMINA AS TIPO
			FROM TMPESTAD
			LEFT OUTER JOIN NOMINA
				ON NOMINA.CB_CODIGO = TMPESTAD.CB_CODIGO
			LEFT OUTER JOIN PERIODO
				ON PERIODO.PE_YEAR = NOMINA.PE_YEAR
				AND PERIODO.PE_TIPO = 	NOMINA.PE_TIPO
				AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
			LEFT OUTER JOIN MOVIMIEN
				ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR
				AND NOMINA.PE_TIPO = 	MOVIMIEN.PE_TIPO
				AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO
				AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
			WHERE ( TMPESTAD.TE_USER = @USUARIO_PORTAL)
			AND ( TMPESTAD.US_CODIGO = @US_CODIGO )
			AND ( MOVIMIEN.CB_CODIGO = @CB_CODIGO)
			AND  ( MOVIMIEN.CO_NUMERO = @TB_CONCEPT )
			AND  ( NOMINA.NO_STATUS	=	@PERIODOSTATUS)
			AND ( MOVIMIEN.MO_ACTIVO = @MOVIMIENTOACTIVO)
			AND  ( PERIODO.PE_FEC_FIN >= TMPESTAD.TE_FECHA )
			AND TMPESTAD.TE_REFEREN=@REFERENCIA
			UNION ALL
            SELECT	TMPESTAD.CB_CODIGO,
                  	TMPESTAD.TE_TIPO,
					TMPESTAD.TE_REFEREN,
					ACAR_ABO.CR_FECHA PE_FEC_INI,
					ACAR_ABO.CR_CARGO MO_PERCEPC,
					ACAR_ABO.CR_ABONO MO_DEDUCCI,
					ACAR_ABO.US_CODIGO,
					CAST(0 AS SMALLINT) PE_YEAR,
					CAST(0 AS SMALLINT) PE_TIPO,
					CAST(0 AS SMALLINT) PE_NUMERO,
					CAST(0 AS SMALLINT) CO_NUMERO,
					@TIPOFUERANOMINA AS TIPO
            FROM TMPESTAD
            LEFT OUTER JOIN ACAR_ABO
				ON ACAR_ABO.CB_CODIGO = TMPESTAD.CB_CODIGO
                AND ACAR_ABO.AH_TIPO = TMPESTAD.TE_TIPO
			WHERE TMPESTAD.TE_USER = @USUARIO_PORTAL
            	AND TMPESTAD.US_CODIGO = @US_CODIGO
				AND (ACAR_ABO.CB_CODIGO=@CB_CODIGO)
                AND (ACAR_ABO.CR_CARGO +ACAR_ABO.CR_ABONO)>0
                AND TMPESTAD.TE_FECHA <= (@FECHA_PERIODO)
            ORDER BY 1,2,3,4

		   	OPEN MOVIMIENTOS_AHORRO_CURSOR
		   	FETCH NEXT FROM MOVIMIENTOS_AHORRO_CURSOR
			INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,
					@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
					@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO

			   WHILE @@FETCH_STATUS = 0
			   BEGIN

				/*Se genera la descripci�n del movimiento*/

				SELECT 	@DESCRIPCION='',
						@NOMINA_DESC=(select TP_NOMBRE from tperiodo WHERE TP_TIPO=@D_PE_TIPO)

				/*Si el movimiento fue por nomina se genera etiqueta indicando la n�mina y el a�o*/
				IF(@D_TIPO=@TIPODENTRONOMINA)
				BEGIN
					SELECT @DESCRIPCION =@NOMINA_DESC + ' # ' +
										 CONVERT(VARCHAR(30),@D_PE_NUMERO)+ ' A�o: ' +
										 CONVERT(VARCHAR(30),@D_PE_YEAR)
				END
				ELSE
				BEGIN
					/*	Si el movimiento fue por fuera de nomina se genera etiqueta*/
					/*	indicando Cargo o Abono segun sea el caso */
					IF((@D_MO_DEDUCCI=NULL) OR (@D_MO_DEDUCCI=0))
					BEGIN
						SELECT @DESCRIPCION='Cargo'
					END
					ELSE
					BEGIN
						SELECT @DESCRIPCION='Abono'
					END
				END

				/*Se calcula el Saldo que se genera con el movimiento*/
				if(@D_MO_DEDUCCI<>0)
				BEGIN
					SELECT @SALDO=(@SALDO+@D_MO_DEDUCCI)
				END
				ELSE
				BEGIN
					SELECT @SALDO=(@SALDO-@D_MO_PERCEPC)
				END

				/* Se incrementa para ordenar los registros */
				SELECT @TE_CODIGO=@TE_CODIGO+1

				/* Se utiliza para alternar los renglones del grid*/
				IF(@ALTERN_ROW=0)
				BEGIN
					SELECT @ALTERN_ROW=1
				END
				ELSE
				BEGIN
					SELECT @ALTERN_ROW=0
				END

				/*	Se inserta el registro del movimiento a la tabla temporal para el reporte final*/
				INSERT INTO #TMP_AHORROS(TE_CODIGO,		CB_CODIGO,		TE_TIPO,		TE_FECHA,
										TE_DESCRIP, 	TE_CARGO, 		TE_ABONO,		TE_SALDO,
										US_CODIGO,		TE_USER,		TE_REFEREN,		A_ANIOS_ANT,
										OTROS_ABONOS,	OTROS_CARGOS,	SALDO_ACTUAL,	AH_TOTAL,
										AH_NUMERO,		AH_STATUS,		AH_STATUS_DES,	TIPO_REGISTRO,
										ALTERN_ROW)
								VALUES(	@TE_CODIGO,		@D_CB_CODIGO, 	@D_TE_TIPO,		@D_PE_FEC_INI,
										@DESCRIPCION, 	@D_MO_PERCEPC, 	@D_MO_DEDUCCI,	@SALDO,
										@US_CODIGO,		@USUARIO_PORTAL,@D_TE_REFEREN,	0,
										0,				0,				0,				0,
										0,				0,				'',				'GRID',
										@ALTERN_ROW)

			FETCH NEXT FROM MOVIMIENTOS_AHORRO_CURSOR
			INTO 	@D_CB_CODIGO, 	@D_TE_TIPO,		@D_TE_REFEREN,	@D_PE_FEC_INI,
					@D_MO_PERCEPC,	@D_MO_DEDUCCI,	@D_US_CODIGO,	@D_PE_YEAR,
					@D_PE_TIPO,		@D_PE_NUMERO, 	@D_CO_NUMERO,	@D_TIPO

			END

			CLOSE MOVIMIENTOS_AHORRO_CURSOR
			DEALLOCATE MOVIMIENTOS_AHORRO_CURSOR

   /*-----------------Termina MOVIMIENTOS_AHORRO_CURSOR-------------------------------------*/
   /* Get the next Ahorro. */
   	FETCH NEXT FROM LISTA_AHORROS_CURSOR
	INTO 	@L_CB_CODIGO ,	@L_AH_TIPO,		@L_AH_FECHA,	@TB_CONCEPT ,
			@L_AH_SALDO_I,	@L_AH_SALDO,	@L_AH_MONTO,  	@L_AH_REFEREN,
			@L_US_CODIGO,	@L_AH_ABONOS,	@L_AH_CARGOS,	@L_AH_TOTAL,
			@L_AH_NUMERO,	@L_AH_STATUS,	@L_AH_STATUS_DES

	END

	/* Close the LISTA_AHORROS_CURSOR */
	CLOSE LISTA_AHORROS_CURSOR
	DEALLOCATE LISTA_AHORROS_CURSOR

	SELECT * FROM #TMP_AHORROS
	DROP TABLE #TMP_AHORROS
END
GO


/* Comienza: SP_BALANZAMENSUAL */
/* Patch 425 # Seq: 324 */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_BALANZAMENSUAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[SP_BALANZAMENSUAL]
go
/* Comienza: SP_BALANZAMENSUAL */
/* Patch 425 # Seq: 324 */
create procedure SP_BALANZAMENSUAL(
	@USUARIO SMALLINT,
	@ANIO SMALLINT,
	@TIPO SMALLINT,
	@NUMERO SMALLINT,
	@EMPLEADO INTEGER,
	@CONCEPTO SMALLINT,
	@COLUMNA SMALLINT,
	@DESCRIPCION CHAR( 30 ),
	@MONTO NUMERIC( 15, 2 ),
	@HORAS NUMERIC( 15, 2 ),
	@REFERENCIA VARCHAR( 8 ) )
as
begin
     SET NOCOUNT ON
     declare @Percepcion SMALLINT;
     declare @Deduccion SMALLINT;
     declare @Existe SMALLINT;
     
     if (@Columna= 1)
	select @Existe = COUNT(*) from DBO.TMPBALAMES as T
	where 	( T.TM_USER = @USUARIO ) and
		( T.CB_CODIGO = @EMPLEADO ) and
		( T.TM_NUM_PER = @CONCEPTO ) and
		( T.TM_REF_PER = @REFERENCIA )
     else
	select @Existe = COUNT(*) from DBO.TMPBALAMES as T
	where 	( T.TM_USER = @USUARIO ) and
		( T.CB_CODIGO = @EMPLEADO ) and
		( T.TM_NUM_DED = @CONCEPTO ) and
		( T.TM_REF_DED = @REFERENCIA )
	 
     if ( @Existe = 0 )
	begin
	     select @Percepcion = COUNT(*) from DBO.TMPBALAMES as T
	     where ( T.TM_USER = @USUARIO ) and
		 ( T.CB_CODIGO = @EMPLEADO ) and
		 ( T.TM_NUM_PER > 0 ) 
	     
	     select @Deduccion = COUNT(*) from DBO.TMPBALAMES as T
	     where ( T.TM_USER = @USUARIO ) and
		 ( T.CB_CODIGO = @EMPLEADO ) and
		 ( T.TM_NUM_DED > 0 )
		 
	     if ( @COLUMNA = 1 )
	     begin
		if ( @Percepcion >= @Deduccion )
		begin
		     insert into DBO.TMPBALAMES( TM_CODIGO,
					 CB_CODIGO,
					 PE_YEAR,
					 PE_TIPO,
					 PE_NUMERO,
					 TM_USER,
					 TM_NUM_PER,
					 TM_MON_PER,
					 TM_DES_PER,
					 TM_HOR_PER,
					 TM_REF_PER ) values (
					@PERCEPCION,
					@EMPLEADO,
					@ANIO,
					@TIPO,
					@NUMERO,
					@USUARIO,
					@CONCEPTO,
					@MONTO,
					@DESCRIPCION,
					@HORAS,
					@REFERENCIA );
		end
		else
		begin
		     update DBO.TMPBALAMES set TM_NUM_PER = @CONCEPTO,
				         TM_MON_PER = @MONTO,
				         TM_DES_PER = @DESCRIPCION,
				         TM_HOR_PER = @HORAS,
				         TM_REF_PER = @REFERENCIA
		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_CODIGO = @PERCEPCION ) 
			 
		end
	     end
	     else
	     begin
		if ( @Deduccion >= @Percepcion  )
		begin
		     insert into DBO.TMPBALAMES( TM_CODIGO,
					 CB_CODIGO,
					 PE_YEAR,
					 PE_TIPO,
					 PE_NUMERO,
					 TM_USER,
					 TM_NUM_DED,
					 TM_MON_DED,
					 TM_DES_DED,
					 TM_HOR_DED,
					 TM_REF_DED ) values (
					@DEDUCCION,
					@EMPLEADO,
					@ANIO,
					@TIPO,
					@NUMERO,
					@USUARIO,
					@CONCEPTO,
					@MONTO,
					@DESCRIPCION,
					@HORAS,
					@REFERENCIA );
		end
		else
		begin
		     update DBO.TMPBALAMES set TM_NUM_DED = @CONCEPTO,
				         TM_MON_DED = @MONTO,
				         TM_DES_DED = @DESCRIPCION,
				         TM_HOR_DED = @HORAS,
				         TM_REF_DED = @REFERENCIA
		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_CODIGO = @DEDUCCION ) 
		end;
	     end;
	end
	else
	begin
		if ( @COLUMNA = 1 )		
		     update DBO.TMPBALAMES set 
					TM_MON_PER = TM_MON_PER + @MONTO,
					TM_HOR_PER = TM_HOR_PER + @HORAS
			         
		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_NUM_PER = @CONCEPTO )
		else
		     update DBO.TMPBALAMES set
					TM_MON_DED = TM_MON_DED + @MONTO,
					TM_HOR_DED = TM_HOR_DED + @HORAS
				         
		     where ( TM_USER = @USUARIO ) and
			 ( CB_CODIGO = @EMPLEADO ) and
			 ( TM_NUM_DED = @CONCEPTO ) and
			 ( TM_REF_DED = @REFERENCIA )
			 		
	end;
end;
GO


/* Comienza: SP_ORDENA_BALAMES */
/* Patch 425 # Seq: 325 */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ORDENA_BALAMES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[SP_ORDENA_BALAMES]
go

/* Comienza: SP_ORDENA_BALAMES */
/* Patch 425 # Seq: 325 */
CREATE PROCEDURE SP_ORDENA_BALAMES( @Usuario Usuario )
    				
AS
begin
	SET NOCOUNT ON
	
	declare @Empleado		NumeroEmpleado;
	declare @Concepto           	Concepto;
	declare @Descripcion          Descripcion;
	declare @Monto            	Pesos;
	declare @Horas            	Horas;
	declare @Referencia         	Referencia;
	declare @Posicion		FolioChico;
	
	
	DECLARE EMPLEADOS CURSOR FOR
		select DISTINCT(CB_CODIGO) from TMPBALAMES where ( TM_USER = @Usuario )

	OPEN EMPLEADOS
	FETCH NEXT FROM EMPLEADOS
	into @Empleado 
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--Se reordenan las percepciones
		select  @Posicion =0;
		DECLARE PERCEPCION CURSOR STATIC FOR
			select TM_NUM_PER,
			       TM_DES_PER,
			       TM_MON_PER,
			       TM_HOR_PER,
			       TM_REF_PER
			from TMPBALAMES
			where 	( TM_USER = @Usuario ) and 
				( CB_CODIGO = @Empleado ) and 
				( TM_NUM_PER <> 0 )
 			order by TM_NUM_PER
		OPEN PERCEPCION
		FETCH NEXT FROM PERCEPCION
		into @Concepto, @Descripcion, @Monto, @Horas, @Referencia 	
	
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			Update TMPBALAMES
			set 
				TM_NUM_PER = @Concepto,
				TM_DES_PER = @Descripcion,
				TM_MON_PER = @Monto,
				TM_HOR_PER = @Horas,
				TM_REF_PER = @Referencia
			where 	( TM_USER = @Usuario ) and 
				( CB_CODIGO = @Empleado ) and 
				( TM_CODIGO = @Posicion )
			
			select  @Posicion = @Posicion+1;			
			
			FETCH NEXT FROM PERCEPCION
			into @Concepto, @Descripcion, @Monto, @Horas, @Referencia 
			
		END;
		
		CLOSE PERCEPCION
		DEALLOCATE PERCEPCION
		
		--Se reordenan las deducciones
		select  @Posicion =0;
		DECLARE DEDUCCION CURSOR STATIC FOR
			select TM_NUM_DED,
			       TM_DES_DED,
			       TM_MON_DED,
			       TM_HOR_DED,
			       TM_REF_DED
			from TMPBALAMES
			where 	( TM_USER = @Usuario ) and 
				( CB_CODIGO = @Empleado ) and 
				( TM_NUM_DED <> 0 )
 			order by TM_NUM_DED
		OPEN DEDUCCION
		FETCH NEXT FROM DEDUCCION
		into @Concepto, @Descripcion, @Monto, @Horas, @Referencia 	
	
		WHILE @@FETCH_STATUS = 0 
		BEGIN
			Update TMPBALAMES
			set 
				TM_NUM_DED = @Concepto,
				TM_DES_DED = @Descripcion,
				TM_MON_DED = @Monto,
				TM_HOR_DED = @Horas,
				TM_REF_DED = @Referencia
			where 	( TM_USER = @Usuario ) and 
				( CB_CODIGO = @Empleado ) and 
				( TM_CODIGO = @Posicion )
			
			select  @Posicion = @Posicion+1;			
			
			FETCH NEXT FROM DEDUCCION
			into @Concepto, @Descripcion, @Monto, @Horas, @Referencia 
		END;
		
		CLOSE DEDUCCION
		DEALLOCATE DEDUCCION
		
		FETCH NEXT FROM EMPLEADOS
		into @Empleado
	
	END
	
	CLOSE EMPLEADOS
   	DEALLOCATE EMPLEADOS
end
go

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Procedimiento  para actualizar el usuario del  nivel de supervision llamado desde un cambio en el cat�logo de usuario  */
/* CR: 1800 */
/* Patch 425 # Seq: 326 */
CREATE PROCEDURE SP_ACTUALIZA_USUARIO( @US_CODIGO Usuario, @US_CODIGO_ANT Usuario )
AS
BEGIN
	 SET NOCOUNT ON
	IF ( @US_CODIGO_ANT > 0 ) 
	BEGIN 
		UPDATE NIVEL1 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL2 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL3 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL4 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL5 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL6 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL7 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL8 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT							
		UPDATE NIVEL9 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT								
	
	/* ACS 
		UPDATE NIVEL10 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT
		UPDATE NIVEL11 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT							
		UPDATE NIVEL12 SET US_CODIGO = @US_CODIGO WHERE US_CODIGO = @US_CODIGO_ANT								
	*/
	END

END
GO 

/* (JB) Se agrega Procedures:

			SPP_MigracionPresupuesto
			SP_MIGRACIONPRESUPUESTOTABLA
			CALCULA_CUBO
			PE_COMPACTA_KARDEX
			SP_MigracionOptimiza  

			Presupuestos T1059 */
----------------------------------------------------------------------------------------------------
/*SPP_MigracionPresupuesto*/
/* Patch 425 # Seq: 327 */
if exists (select * from dbo.sysobjects where id = object_id(N'SP_MIGRACIONPRESUPUESTOTABLA') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure SP_MIGRACIONPRESUPUESTOTABLA
go
/*SPP_MigracionPresupuesto*/
/* Patch 425 # Seq: 327 */
CREATE PROCEDURE SP_MIGRACIONPRESUPUESTOTABLA ( 
	@TABLA NombreCampo,
	@BASEORIGEN Formula,
	@BASEDESTINO Formula
)
AS
BEGIN
	SET NOCOUNT ON
	Declare @Sentencia varchar(8000)
	Declare @Sentencia2 varchar(8000)
	Declare @Condicion varchar(8000)
	Declare @CamposCantidad int
	Declare @RegistrosCantidad int
	Declare @TablaEspecial varchar(8000)
	DECLARE @CamposConcatenados VARCHAR(8000) 
	DECLARE @CamposConcatenadosCoalesce VARCHAR(8000) 
	declare @Campos table(Campos varchar(10), Coal varchar(1000))

	insert into BIT_MIG  values( getdate(), '', '');
	insert into BIT_MIG  values( getdate(), 'Migrando ' + @TABLA, 'Migraci�n Tabla: ' + @TABLA );

	insert into @campos (Campos, Coal)
	SELECT     c.name, 'COALESCE(' + c.name + ',dbo.G_D(''' + @TABLA + ''',''' + c.name + '''))'
	FROM         syscolumns AS c INNER JOIN
						  sysobjects AS o ON c.id = o.id
	Where UPPER(o.xtype) = 'U'
			and o.name = @TABLA
			and c.iscomputed <> 1 
			and c.status <> 128

	set @CamposCantidad = (SELECT count(*) from @campos)	
	
	Set @TablaEspecial = case when @TABLA = 'KAR_FIJA' then 'KAR_FIJA2' else @TABLA end


	Select @CamposConcatenados = COALESCE( @CamposConcatenados + ',', '') + Campos FROM @Campos
	Select @CamposConcatenadosCoalesce = COALESCE( @CamposConcatenadosCoalesce + ',', '') + Coal FROM @Campos

	set @Sentencia  = ' INSERT INTO ' + @BASEDESTINO + '..' + @TablaEspecial + ' (' + @CamposConcatenados + ') '
	set @Sentencia2 = ' select ' +  @CamposConcatenadosCoalesce + ' from ' + @BASEORIGEN + '..' + @TABLA 
	
	insert into BIT_MIG values( getdate(), 'Sentencia de Inserci�n: ' + @TABLA, @Sentencia + @Sentencia2 );
	EXEC ( @Sentencia + @Sentencia2 )
	insert into BIT_MIG values( getdate(), 'Migrado ' + @TABLA, 'Registros Migrados: ' + convert( varchar(100), @@Rowcount ) );
		
END
GO
/*SPP_MigracionPresupuesto_DELETE*/
/* Patch 425 # Seq: 328 */
if exists (select * from dbo.sysobjects where id = object_id(N'SPP_MigracionPresupuesto_DELETE') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure SPP_MigracionPresupuesto_DELETE
go
/*SPP_MigracionPresupuesto_DELETE*/
/* Patch 425 # Seq: 328 */
CREATE PROCEDURE SPP_MigracionPresupuesto_DELETE( @lDejarConceptos Booleano )
AS
BEGIN
		/* El parametro @lDejarConceptos, NO borra los catalogos de CONCEPTOS y  PARAMETROS */

		SET NOCOUNT ON;
		Delete from BIT_MIG
		insert into BIT_MIG 
		values(	getdate(), 'Se trunca la Tabla de Bitacora', '' );
		DELETE FROM ACUMULA;
		DELETE FROM KAR_TOOL;
		DELETE FROM VACACION;
		DELETE FROM PCAR_ABO;
		DELETE FROM PRESTAMO;
		DELETE FROM AHORRO;
		DELETE FROM ACAR_ABO;
		DELETE FROM KARDEX;
		DELETE FROM COLABORA;		
		DELETE FROM KAR_FIJA2;
		DELETE FROM KAR_FIJA;
		DELETE FROM TAHORRO;
		DELETE FROM CAMPOREP
		DELETE FROM REPORTE
		DELETE FROM R_VALOR;
		DELETE FROM R_LISTAVAL;
		DELETE FROM R_MOD_ENT;
		DELETE FROM R_ORDEN;
		DELETE FROM R_DEFAULT;
		DELETE FROM R_FILTRO;
		DELETE FROM R_CLAS_ENT;
		DELETE FROM R_RELACION;
		DELETE FROM R_ATRIBUTO;
		DELETE FROM R_ENTIDAD;
		DELETE FROM R_MODULO;
		DELETE FROM R_CLASIFI;
		DELETE FROM PLAZA
		DELETE FROM PTOFIJAS
		DELETE FROM PTOTOOLS
		DELETE FROM MOT_TOOL;
		DELETE FROM TOOL;
		DELETE FROM TALLA;
		DELETE FROM TPERIODO;
		DELETE FROM TKARDEX;
		DELETE FROM TCAMBIO;
		DELETE FROM SAL_MIN;
		DELETE FROM RIESGO;
		DELETE FROM QUERYS;
		DELETE FROM PUESTO;
		DELETE FROM PRIESGO;
		DELETE FROM PRESTAXREG;
		DELETE FROM REGLAPREST;
		DELETE FROM TPRESTA;
		DELETE FROM PRESTACI;
		DELETE FROM SSOCIAL;
		DELETE FROM OTRASPER;
		if NOT (@lDejarConceptos = 'S')
			DELETE FROM NOMPARAM;
		DELETE FROM NIVEL9;
		DELETE FROM NIVEL8;
		DELETE FROM NIVEL7;
		DELETE FROM NIVEL6;
		DELETE FROM NIVEL5;
		DELETE FROM NIVEL4;
		DELETE FROM NIVEL3;
		DELETE FROM NIVEL2;
		DELETE FROM NIVEL1;
		DELETE FROM MOT_BAJA;
		DELETE FROM LEY_IMSS;
		DELETE FROM HORARIO;
		DELETE FROM FESTIVO;
		DELETE FROM ENTIDAD;
		DELETE FROM CONTRATO;
		DELETE FROM CLASIFI;
		DELETE FROM TURNO;
		if NOT (@lDejarConceptos = 'S')
			DELETE FROM CONCEPTO;
		DELETE FROM GLOBAL;
		DELETE FROM ART_80;
		DELETE FROM T_ART_80;
		DELETE FROM NUMERICA;
		DELETE FROM RPATRON;
		DELETE FROM RSOCIAL;
END
GO

/*SPP_MigracionPresupuesto*/
/* Patch 425 # Seq: 329 */
if exists (select * from dbo.sysobjects where id = object_id(N'SPP_MigracionPresupuesto') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure SPP_MigracionPresupuesto
go
/*SPP_MigracionPresupuesto*/
/* Patch 425 # Seq: 329 */
CREATE PROCEDURE SPP_MigracionPresupuesto
	(
		@BaseOrigen Formula,
		@BaseDestino Formula
	)
AS
BEGIN
	    SET NOCOUNT ON;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'RSOCIAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'RPATRON',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NUMERICA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'T_ART_80',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'ART_80',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'GLOBAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CONCEPTO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TURNO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CLASIFI',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CONTRATO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'ENTIDAD',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'FESTIVO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'HORARIO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'LEY_IMSS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'MOT_BAJA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL1',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL2',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL3',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL4',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL5',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL6',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL7',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL8',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NIVEL9',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'NOMPARAM',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'OTRASPER',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'SSOCIAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PRESTACI',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TPRESTA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'REGLAPREST',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PRESTAXREG',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PRIESGO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PUESTO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'QUERYS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'RIESGO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'SAL_MIN',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TCAMBIO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TKARDEX',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TPERIODO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TALLA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TOOL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'MOT_TOOL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PTOTOOLS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PTOFIJAS',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'PLAZA',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_CLASIFI',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_MODULO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_ENTIDAD' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_ATRIBUTO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_RELACION',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_CLAS_ENT',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_FILTRO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_DEFAULT' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_ORDEN' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_MOD_ENT' ,@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_LISTAVAL',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'R_VALOR',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'REPORTE',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'CAMPOREP',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'TAHORRO',@BaseOrigen,@BaseDestino;
		execute SP_MIGRACIONPRESUPUESTOTABLA 'KAR_FIJA',@BaseOrigen,@BaseDestino;
		
END
GO
/*CALCULA_CUBO*/
/* Patch 425 # Seq: 330 */
if exists (select * from dbo.sysobjects where id = object_id(N'CALCULA_CUBO') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure CALCULA_CUBO
go
/*CALCULA_CUBO*/
/* Patch 425 # Seq: 330 */
CREATE PROCEDURE CALCULA_CUBO
(
	@ANIO Anio, 
	@ES_CODIGO Codigo
)
AS
begin
     SET NOCOUNT ON
	 INSERT INTO TOTPRESUP( PE_YEAR,PE_MES,PE_TIPO,
                            CO_NUMERO,CB_CLASIFI,CB_PUESTO,CB_TURNO,
                            CB_NIVEL1,CB_NIVEL2,CB_NIVEL3,CB_NIVEL4,
                            CB_NIVEL5,CB_NIVEL6,CB_NIVEL7,CB_NIVEL8,
                            CB_NIVEL9,CB_NIVEL0,
                            TP_MONTO, TP_EMPLEAD, ES_CODIGO)

     select M.PE_YEAR,P.PE_MES,M.PE_TIPO,
            M.CO_NUMERO,N.CB_CLASIFI,N.CB_PUESTO,N.CB_TURNO,
            N.CB_NIVEL1,N.CB_NIVEL2,N.CB_NIVEL3,N.CB_NIVEL4,
            N.CB_NIVEL5,N.CB_NIVEL6,N.CB_NIVEL7,N.CB_NIVEL8,
            N.CB_NIVEL9,N.CB_NIVEL0,
     SUM( M.MO_DEDUCCI+M.MO_PERCEPC ),  COUNT(DISTINCT(M.CB_CODIGO)), @ES_CODIGO -- (JB) Escenarios de presupuestos T1060 CR1872
     FROM MOVIMIEN M
     LEFT OUTER JOIN NOMINA  N ON N.PE_YEAR = M.PE_YEAR AND N.PE_TIPO = M.PE_TIPO AND N.PE_NUMERO = M.PE_NUMERO AND N.CB_CODIGO = M.CB_CODIGO
     LEFT OUTER JOIN PERIODO P ON P.PE_YEAR = N.PE_YEAR AND P.PE_TIPO = N.PE_TIPO AND P.PE_NUMERO = N.PE_NUMERO 
     WHERE M.PE_YEAR = @Anio
     GROUP BY M.PE_YEAR,P.PE_MES,M.PE_TIPO,
           M.CO_NUMERO,N.CB_CLASIFI,N.CB_PUESTO,N.CB_TURNO,
           N.CB_NIVEL1,N.CB_NIVEL2,N.CB_NIVEL3,N.CB_NIVEL4,
           N.CB_NIVEL5,N.CB_NIVEL6,N.CB_NIVEL7,N.CB_NIVEL8,
           CB_NIVEL9,N.CB_NIVEL0 ;
end;
GO
/*SP_COMPACTA_KARDEX*/
/* Patch 425 # Seq: 331 */
if exists (select * from dbo.sysobjects where id = object_id(N'SP_COMPACTA_KARDEX') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure SP_COMPACTA_KARDEX
go
/*SP_COMPACTA_KARDEX*/
/* Patch 425 # Seq: 331 */
CREATE PROCEDURE SP_COMPACTA_KARDEX
AS
BEGIN
      SET NOCOUNT ON

      -- Guarda una copia de KAR_FIJA antes de borrar el KARDEX
      -- Solo guarda los registros relacionados con el �ltimo movimiento de 'ALTA'/'CAMBIO'
      -- Se cambian los datos de Fecha y Tipo, correspondiento da 'CB_FEC_ING' y 'ALTA'
      -- para que formen parte del registro de Kardex que se agregar� adelante.

		
      -- Borra todo el KARDEX y por trigger, KAR_FIJA
		

      DELETE FROM KARDEX

      -- Agrega un movimiento de KARDEX de 'ALTA' para todos los empleados, a su fecha de ingreso

      INSERT INTO KARDEX
      ( CB_CODIGO, CB_FECHA, CB_TIPO, CB_AUTOSAL,
        CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
        CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
        CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_NIVEL,
        CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        CB_STATUS, CB_TABLASS, CB_TOT_GRA, US_CODIGO, CB_ZONA_GE, 
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        CB_MOD_NV1, CB_MOD_NV2, CB_MOD_NV3, CB_MOD_NV4, CB_MOD_NV5, CB_MOD_NV6, CB_MOD_NV7, CB_MOD_NV8, CB_MOD_NV9,
        CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_NOTA, CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV )
      SELECT
        CB_CODIGO, CB_FEC_ING, 'ALTA', CB_AUTOSAL,
        CB_CLASIFI, 'ALTA en Presupuesto', CB_CONTRAT,
        CB_FAC_INT, GETDATE(), CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FEC_ING,
        'S', CB_TURNO, 0, '', 0,
        CB_OLD_INT, CB_OLD_SAL, 0, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        0, CB_TABLASS, CB_TOT_GRA, 0, CB_ZONA_GE, 
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S',
        0, 0, 0, 'N', '', CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
      FROM COLABORA



      -- Agrega los registros temporales de KAR_FIJA2 a KAR_FIJA
      -- Van a corresponder al registro �nico de KARDEX
 
      -- Para los empleados dados de baja, agrega registros de 'BAJA'

      INSERT INTO KARDEX
      ( CB_CODIGO, CB_FECHA, CB_TIPO, CB_AUTOSAL,
        CB_CLASIFI, CB_COMENTA, CB_CONTRAT,
        CB_FAC_INT, CB_FEC_CAP, CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FECHA_2,
        CB_GLOBAL, CB_TURNO, CB_MONTO, CB_MOT_BAJ, CB_NIVEL,
        CB_OLD_INT, CB_OLD_SAL, CB_OTRAS_P, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        CB_STATUS, CB_TABLASS, CB_TOT_GRA, US_CODIGO, CB_ZONA_GE, 
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        CB_MOD_NV1, CB_MOD_NV2, CB_MOD_NV3, CB_MOD_NV4, CB_MOD_NV5, CB_MOD_NV6, CB_MOD_NV7, CB_MOD_NV8, CB_MOD_NV9,
        CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, CB_REINGRE, CB_NOTA, CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV )
      SELECT
        CB_CODIGO, CB_FEC_BAJ, 'BAJA', CB_AUTOSAL,
        CB_CLASIFI, 'BAJA en Presupuesto', CB_CONTRAT,
        CB_FAC_INT, GETDATE(), CB_FEC_CON, CB_FEC_INT, CB_FEC_REV, CB_FEC_BSS,
        'S', CB_TURNO, 0, CB_MOT_BAJ, 9,
        CB_OLD_INT, CB_OLD_SAL, 0, CB_PATRON, CB_PER_VAR, CB_PRE_INT,
        CB_PUESTO, CB_RANGO_S, CB_SAL_INT, CB_SAL_TOT, CB_SALARIO,
        0, CB_TABLASS, CB_TOT_GRA, 0, CB_ZONA_GE, 
        CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9,
        'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'N',
        CB_NOMTIPO, CB_NOMYEAR, CB_NOMNUME, 'N', '', CB_FEC_ING, CB_FEC_ANT,
		CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV
      FROM COLABORA
      WHERE CB_ACTIVO = 'N'


	  DELETE FROM KAR_FIJA
      INSERT INTO KAR_FIJA
       ( CB_CODIGO, CB_FECHA, CB_TIPO, KF_FOLIO, KF_CODIGO, KF_MONTO, KF_GRAVADO, KF_IMSS )
      SELECT KAR_FIJA2.CB_CODIGO, COLABORA.CB_FEC_ING, 'ALTA', KF_FOLIO, KF_CODIGO, KF_MONTO, KF_GRAVADO, KF_IMSS
      FROM  KAR_FIJA2, COLABORA
      WHERE KAR_FIJA2.CB_CODIGO = COLABORA.CB_CODIGO AND
            KAR_FIJA2.CB_FECHA  = COLABORA.CB_FEC_SAL AND
            KAR_FIJA2.CB_TIPO   = COLABORA.CB_TIP_REV

END;

/* 

Notas: Sobre los registros que se agregan al KARDEX:

	-Una ALTA por cada empleado, a su fecha de ingreso. Reflejando todos sus datos actuales (Salarios, Puesto, Niveles, etc.)
	-Una BAJA para cada empleado cuyo status actual sea BAJA
	-Para los �Reingresos�, se pierden los registros de ALTA y BAJA anteriores
	-Los US_CODIGO se ponen en CERO
	-Las descripciones de los movimientos son �ALTA en Presupuesto� Y �BAJA en Presupuesto�
	-Los movimientos se graban como GLOBAL = �S�
	-Se pierde el dato informativo CB_OTRAS_P
	-La fecha de �captura� de los movimientos es la fecha del sistema al momento en que se corre el Stored Procedure
	-Los registros de KAR_FIJA quedan asociados al registro de ALTA
	-El salario se toma el actual, y aparecer� como que el empleado lo tiene desde la fecha de �ALTA�
	Ejemplo:
	Si en COLABORA hay 100 empleados, de los cuales 80 activos y 20 bajas
	Entonces, en KARDEX quedar�n 100 registros de ALTA y 20 registros de BAJA, en total 120. 

Ejemplo:
Si en COLABORA hay 100 empleados, de los cuales 80 activos y 20 bajas
Entonces, en KARDEX quedar�n 100 registros de ALTA y 20 registros de BAJA, en total 120.
	
	JB Faltaban los campos CB_PLAZA, CB_NOMINA, CB_RECONTR, CB_FEC_COV en Kardex asi que fueron agregados. 
	El campo llave sera llenado autmaticamente por la estructura de la tabla por autoicremento
	LLos campos CB_TIP_REV, CB_FEC_SAL Son Calculados
*/
GO
----------------------------------------------------------------------------------------------------
/*SPP_MigracionOptimiza*/
/* Patch 425 # Seq: 333 */
if exists (select * from dbo.sysobjects where id = object_id(N'SPP_MigracionOptimiza') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure SPP_MigracionOptimiza
go
/*SPP_MigracionOptimiza*/
/* Patch 425 # Seq: 333 */
CREATE PROCEDURE SPP_MigracionOptimiza
AS
BEGIN
		SET NOCOUNT ON
		UPDATE COLABORA set CB_CREDENC = '', CB_ID_NUM = ''
		--Print( 'Actualiza Campos CB_CREDENC CB_ID_NUM de COLABORA Satisfactorio!' )
		insert into BIT_MIG
		values(getdate(), 'Actualizacion COLABORA CB_CREDENC, CB_ID_NUM','Cantidad de Registros Actualizados: ' + convert(varchar(10), @@Rowcount));
		
		EXECUTE SP_COMPACTA_KARDEX
		--Print( 'Termina SP_COMPACTA_KARDEX' )

		update KARDEX set US_CODIGO = 0
		--Print( 'Actualiza Campo US_CODIGO de KARDEX Satisfactorio!' )
		insert into BIT_MIG 
		values(getdate(), 'Actualizacion KARDEX | US_CODIGO = 0','Cantidad de Registros Actualizados: ' + convert(varchar(10), @@Rowcount));
		
		delete from KAR_FIJA2
		--Print( 'Eliminaci�n Satisfactoria de Tabla temporal!' )
		insert into BIT_MIG
		values(getdate(), 'Eliminacion de KAR_FIJA2','Cantidad de Registros Eliminados: ' + convert(varchar(10), @@Rowcount));
		
END
GO
----------------------------------------------------------------------------------------------------
-- (JB) Escenarios de presupuestos T1060 CR1872
----------------------------------------------------------------------------------------------------
/*SP_RECALCULA_PRESTAMOS_TODOS*/
/* Patch 425 # Seq: 332 */
if exists (select * from sysobjects where name = 'SP_RECALCULA_PRESTAMOS_TODOS' and xtype = 'P') 
	drop Procedure SP_RECALCULA_PRESTAMOS_TODOS
GO
/*SP_RECALCULA_PRESTAMOS_TODOS*/
/* Patch 425 # Seq: 332 */
CREATE PROCEDURE SP_RECALCULA_PRESTAMOS_TODOS
AS
BEGIN
	 SET NOCOUNT ON
	Declare @Sentencia varchar(1000)
	Declare Procedures CURSOR FOR
	select 'EXECUTE RECALCULA_PRESTAMOS ' + convert(varchar(20),CB_CODIGO) + ',6' [Procedure] from colabora
	
	OPEN Procedures

	FETCH NEXT FROM Procedures into @Sentencia
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Exec ( @Sentencia )
	END

	CLOSE Procedures;
	DEALLOCATE Procedures;
END
GO
----------------------------------------------------------------------------------------------------
/*SP_RECALCULA_AHORROS_TODOS*/
/* Patch 425 # Seq: 334 */
if exists (select * from sysobjects where name = 'SP_RECALCULA_AHORROS_TODOS' and xtype = 'P') 
	drop Procedure SP_RECALCULA_AHORROS_TODOS
GO
/*SP_RECALCULA_AHORROS_TODOS*/
/* Patch 425 # Seq: 334 */
CREATE PROCEDURE SP_RECALCULA_AHORROS_TODOS
AS
BEGIN
	SET NOCOUNT ON
	Declare @Sentencia varchar(1000)
	Declare Procedures CURSOR FOR
	select 'EXECUTE RECALCULA_AHORROS ' + convert(varchar(20),CB_CODIGO) + ',6' [Procedure] from colabora
	
	OPEN Procedures

	FETCH NEXT FROM Procedures into @Sentencia
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Exec ( @Sentencia )
	END

	CLOSE Procedures;
	DEALLOCATE Procedures;
END
GO

/* SP_Actualizar_Movimiento_IDSE: CR #*/
/* Patch 425 # Seq: 335 */
/* (AV)  */

CREATE  PROCEDURE SP_ACTUALIZAR_MOVIMIENTO_IDSE (  
@TipoSUA	Status, 
@NSS		Descripcion, 
@Fecha		Fecha, 
@LoteIDSE	DescLarga,
@FechaIDSE	Fecha,
@US_CODIGO  Usuario,
@CB_CODIGO  NumeroEmpleado output,
@RESPUESTA  Status output
) 
as
BEGIN 
	SET NOCOUNT ON 
	declare 
		@CB_TIPO Codigo;
		

	set @CB_CODIGO = 0
	set @Respuesta = -10
	
	if ( SELECT count(*)  FROM COLABORA WHERE CB_SEGSOC = @NSS ) > 1 
	begin 
		set @Respuesta =  -20 
	end
	else
	begin 
		set @CB_CODIGO = ( SELECT CB_CODIGO  FROM COLABORA WHERE CB_SEGSOC = @NSS ) 
		set @CB_CODIGO = coalesce ( @CB_CODIGO, 0 ) 
		
		if ( @CB_CODIGO <= 0 ) 
		begin 
			set @Respuesta = -10 			
		end
		else
		begin 
			if ( @TipoSUA = 2 ) 
				set @CB_TIPO = 'BAJA'
			else
			if ( @TipoSUA = 7 ) 
				set @CB_TIPO = 'CAMBIO'
			else
			if ( @TipoSUA = 8 ) 
				set @CB_TIPO = 'ALTA'
			else
				set @CB_TIPO = '';

			if ( @CB_TIPO = '' ) 
			begin 
				set @Respuesta = -30			
			end
			else
			begin 
				if ( select count(*) from KARDEX where CB_TIPO = @CB_TIPO and CB_CODIGO = @CB_CODIGO  and CB_FECHA_2 =  @Fecha ) > 0 
				begin 
					UPDATE KARDEX   
					SET 
						CB_LOT_IDS = @LoteIDSE,
						CB_FEC_IDS = @FechaIDSE
					WHERE 
						CB_TIPO = @CB_TIPO and CB_CODIGO = @CB_CODIGO  and CB_FECHA_2 =  @Fecha 
				
					set @Respuesta = 0  -- OK 
				end	
				else
					set  @Respuesta = 20 -- Advertencia 2
			end
		end
	end

	--SELECT  CB_CODIGO = @CB_CODIGO, RESPUESTA = @Respuesta
END

----------------------------------------------------------------------------------------------------
/* PROCEDURE AFECTA_EMPLEADO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  152 */
if exists (select * from dbo.sysobjects where id = object_id(N'AFECTA_EMPLEADO') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop Procedure AFECTA_EMPLEADO
GO

/* PROCEDURE AFECTA_EMPLEADO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  152 */
CREATE PROCEDURE AFECTA_EMPLEADO
    			@EMPLEADO INTEGER,
    			@ANIO SMALLINT,
    			@TIPO SMALLINT,
    			@NUMERO SMALLINT,
    			@MES SMALLINT,
    			@FACTOR INTEGER
AS
BEGIN
     SET NOCOUNT ON
     DECLARE @CONCEPTO INTEGER;
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
	 DECLARE @M1216 NUMERIC(15,2);
	 
     select @M1000=N.NO_PERCEPC,
            @M1001=N.NO_DEDUCCI,
            @M1002=N.NO_NETO,
            @M1003=N.NO_X_ISPT,
            @M1005=N.NO_PER_MEN,
            @M1006=N.NO_X_MENS,
            @M1007=N.NO_PER_CAL,
            @M1008=N.NO_IMP_CAL,
            @M1009=N.NO_X_CAL,
            @M1010=N.NO_TOT_PRE,
            @M1011=N.NO_PREV_GR,
            @M1012=N.NO_PER_ISN,
            @M1100=N.NO_DIAS_FI,
            @M1101=N.NO_DIAS_FJ,
            @M1102=N.NO_DIAS_CG,
            @M1103=N.NO_DIAS_SG,
            @M1104=N.NO_DIAS_SU,
            @M1105=N.NO_DIAS_OT,
            @M1106=N.NO_DIAS_IN,
            @M1107=N.NO_DIAS * 7 / 6,
            @M1108=N.NO_DIAS_RE,
            @M1109=N.NO_D_TURNO,
            @M1110=N.NO_DIAS,
            @M1111=N.NO_DIAS_AS,
            @M1112=N.NO_DIAS_NT,
            @M1113=N.NO_DIAS_FV,
            @M1114=N.NO_DIAS_VA,
            @M1115=N.NO_DIAS_AG,
            @M1116=N.NO_DIAS_AJ,         
            @M1117=N.NO_DIAS_SS,
            @M1118=N.NO_DIAS_EM,
            @M1119=N.NO_DIAS_SI,
            @M1120=N.NO_DIAS_BA,
            @M1121=N.NO_DIAS_PV,
            @M1122=N.NO_DIAS_VJ,
            @M1123=N.NO_DIAS_VJ * 7 / 6,
            @M1200=N.NO_JORNADA,
            @M1201=N.NO_HORAS,           
            @M1202=N.NO_EXTRAS,
            @M1203=N.NO_DOBLES,
            @M1204=N.NO_TRIPLES,         
            @M1205=N.NO_ADICION,
            @M1206=N.NO_TARDES,
            @M1207=N.NO_HORA_PD,
            @M1208=N.NO_HORA_CG,
            @M1209=N.NO_HORA_SG,
            @M1210=N.NO_FES_TRA,
            @M1211=N.NO_DES_TRA,
            @M1212=N.NO_VAC_TRA,
            @M1213=N.NO_FES_PAG,
            @M1214=N.NO_HORASNT,
            @M1215=N.NO_HORAPDT,
			@M1216=N.NO_PRE_EXT

            from NOMINA N where
            ( N.PE_YEAR = @Anio ) and
            ( N.PE_TIPO = @Tipo ) and
            ( N.PE_NUMERO = @Numero ) and
            ( N.CB_CODIGO = @Empleado )

     if ( @M1200 <> 0 )
		Select @M1200 = @M1201 / @M1200;
     Select @M1109 = @M1200 * @M1109;

     /* Se forza la siguiente operaci�n (-Dias Turno/-Jornada)= -Jornada. Para los casos
        que es una cancelaci�n de n�mina*/
     if ( @M1201 < 0 ) and ( @M1200 > 0 )
		set @M1200 = -@M1200;
	  
     if ( @M1000 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1000, @Mes, @Factor, @M1000;
     if ( @M1001 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1001, @Mes, @Factor, @M1001;
     if ( @M1002 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1002, @Mes, @Factor, @M1002;
     if ( @M1003 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1003, @Mes, @Factor, @M1003;
     if ( @M1005 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1005, @Mes, @Factor, @M1005;
     if ( @M1006 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1006, @Mes, @Factor, @M1006;
     if ( @M1007 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1007, @Mes, @Factor, @M1007;
     if ( @M1008 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1008, @Mes, @Factor, @M1008;
     if ( @M1009 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1009, @Mes, @Factor, @M1009;
     if ( @M1010 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1010, @Mes, @Factor, @M1010;
     if ( @M1011 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1011, @Mes, @Factor, @M1011;
	 if ( @M1012 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1012, @Mes, @Factor, @M1012;
     if ( @M1100 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1100, @Mes, @Factor, @M1100;
     if ( @M1101 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1101, @Mes, @Factor, @M1101;
     if ( @M1102 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1102, @Mes, @Factor, @M1102;
     if ( @M1103 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1103, @Mes, @Factor, @M1103;
     if ( @M1104 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1104, @Mes, @Factor, @M1104;
     if ( @M1105 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1105, @Mes, @Factor, @M1105;
     if ( @M1106 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1106, @Mes, @Factor, @M1106;
     if ( @M1107 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1107, @Mes, @Factor, @M1107;
     if ( @M1108 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1108, @Mes, @Factor, @M1108;
     if ( @M1109 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1109, @Mes, @Factor, @M1109;
     if ( @M1110 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1110, @Mes, @Factor, @M1110;
     if ( @M1111 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1111, @Mes, @Factor, @M1111;
     if ( @M1112 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1112, @Mes, @Factor, @M1112;
     if ( @M1113 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1113, @Mes, @Factor, @M1113;
     if ( @M1114 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1114, @Mes, @Factor, @M1114;
     if ( @M1115 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1115, @Mes, @Factor, @M1115;
     if ( @M1116 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1116, @Mes, @Factor, @M1116;
     if ( @M1117 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1117, @Mes, @Factor, @M1117;
     if ( @M1118 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1118, @Mes, @Factor, @M1118;
     if ( @M1119 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1119, @Mes, @Factor, @M1119;
     if ( @M1120 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1120, @Mes, @Factor, @M1120;
     if ( @M1121 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1121, @Mes, @Factor, @M1121;
     if ( @M1122 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1122, @Mes, @Factor, @M1122;
     if ( @M1123 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1123, @Mes, @Factor, @M1123;
     if ( @M1200 <> 0 )
     begin
          if ( @Tipo = 0 )
             Select @M1200 = 8 * @M1200;
          else
          if ( ( @Tipo = 1 ) or ( @Tipo = 6 ) or ( @Tipo = 7 ) )
             Select @M1200 = 48 * @M1200;
          else
          if ( ( @Tipo = 2 ) or ( @Tipo = 10 ) or ( @Tipo = 11 ) )
             Select @M1200 = 96 * @M1200;
          else
          if ( ( @Tipo = 3 ) or ( @Tipo = 8 ) or ( @Tipo = 9 ) )
             Select @M1200 = 104 * @M1200;
          else
          if ( @Tipo = 4 )
             Select @M1200 = 208 * @M1200;
          else
          if ( @Tipo = 5 )
             Select @M1200 = 72 * @M1200;
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1200, @Mes, @Factor, @M1200;

     end
     if ( @M1201 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1201, @Mes, @Factor, @M1201;
     if ( @M1202 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1202, @Mes, @Factor, @M1202;
     if ( @M1203 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1203, @Mes, @Factor, @M1203;
     if ( @M1204 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1204, @Mes, @Factor, @M1204;
     if ( @M1205 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1205, @Mes, @Factor, @M1205;
     if ( @M1206 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1206, @Mes, @Factor, @M1206;
     if ( @M1207 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1207, @Mes, @Factor, @M1207;
     if ( @M1208 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1208, @Mes, @Factor, @M1208;
     if ( @M1209 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1209, @Mes, @Factor, @M1209;
     if ( @M1210 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1210, @Mes, @Factor, @M1210;
     if ( @M1211 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1211, @Mes, @Factor, @M1211;
     if ( @M1212 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1212, @Mes, @Factor, @M1212;
     if ( @M1213 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1213, @Mes, @Factor, @M1213;
     if ( @M1214 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1214, @Mes, @Factor, @M1214;
     if ( @M1215 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1215, @Mes, @Factor, @M1215;
	 if ( @M1216 <> 0 )
	    EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1216, @Mes, @Factor, @M1216;
	
     Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado )
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @M1000
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, @Factor, @M1000;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @M1000
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien


     select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO
     where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and
               ( C.CO_TIPO = 1 ) and
               ( C.CO_A_PTU = 'S' )

     if ( @M1004 <> 0 )
        EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, 1004, @Mes, @Factor, @M1004;
END
GO

/* PROCEDURE SP_COPIA_NOMINA: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  18 */
if exists (select * from dbo.sysobjects where id = object_id(N'DBO.SP_COPIA_NOMINA') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop Procedure DBO.SP_COPIA_NOMINA
GO

/* PROCEDURE SP_COPIA_NOMINA: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  18 */
Create procedure DBO.SP_COPIA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER )
as
begin
     SET NOCOUNT ON
     declare @Status SmallInt;
     select @Status = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @Status is not Null )
     begin
          if ( @Status > 4 )
          begin
               set @Status = 4;
          end;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,	
								  CB_BAN_ELE,	
								  NO_HORAPDT,	
							      NO_HORASNT,	
								  NO_DIAS_SI,	
								  NO_DIAS_VJ,	
							      NO_DIAS_PV,	
								  NO_SUP_OK,	
								  NO_FEC_OK,	
								  NO_HOR_OK,	
								  NO_ASI_INI,	
								  NO_ASI_FIN,	
								  NO_DIAS_BA,	
								  NO_PREV_GR,	
								  CB_PLAZA,
								  NO_FEC_LIQ, 	
								  CB_CTA_GAS, 	
								  CB_CTA_VAL,
								  NO_PER_ISN )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N2.CB_CODIGO,
                 N2.CB_CLASIFI,
                 N2.CB_TURNO,
                 N2.CB_PATRON,
                 N2.CB_PUESTO,
                 N2.CB_ZONA_GE,
                 N2.NO_FOLIO_1,
                 N2.NO_FOLIO_2,
                 N2.NO_FOLIO_3,
                 N2.NO_FOLIO_4,
                 N2.NO_FOLIO_5,
                 N2.NO_OBSERVA,
                 @Status,
                 N2.US_CODIGO,
                 N2.NO_USER_RJ,
                 N2.CB_NIVEL1,
                 N2.CB_NIVEL2,
                 N2.CB_NIVEL3,
                 N2.CB_NIVEL4,
                 N2.CB_NIVEL5,
                 N2.CB_NIVEL6,
                 N2.CB_NIVEL7,
                 N2.CB_NIVEL8,
                 N2.CB_NIVEL9,
                 N2.CB_SAL_INT,
                 N2.CB_SALARIO,
                 N2.NO_DIAS,
                 N2.NO_ADICION,
                 N2.NO_DEDUCCI,
                 N2.NO_NETO,
                 N2.NO_DES_TRA,
                 N2.NO_DIAS_AG,
                 N2.NO_DIAS_VA,
                 N2.NO_DOBLES,
                 N2.NO_EXTRAS,
                 N2.NO_FES_PAG,
                 N2.NO_FES_TRA,
                 N2.NO_HORA_CG,
                 N2.NO_HORA_SG,
                 N2.NO_HORAS,
                 N2.NO_IMP_CAL,
                 N2.NO_JORNADA,
                 N2.NO_PER_CAL,
                 N2.NO_PER_MEN,
                 N2.NO_PERCEPC,
                 N2.NO_TARDES,
                 N2.NO_TRIPLES,
                 N2.NO_TOT_PRE,
                 N2.NO_VAC_TRA,
                 N2.NO_X_CAL,
                 N2.NO_X_ISPT,
                 N2.NO_X_MENS,
                 N2.NO_D_TURNO,
                 N2.NO_DIAS_AJ,
                 N2.NO_DIAS_AS,
                 N2.NO_DIAS_CG,
                 N2.NO_DIAS_EM,
                 N2.NO_DIAS_FI,
                 N2.NO_DIAS_FJ,
                 N2.NO_DIAS_FV,
                 N2.NO_DIAS_IN,
                 N2.NO_DIAS_NT,
                 N2.NO_DIAS_OT,
                 N2.NO_DIAS_RE,
                 N2.NO_DIAS_SG,
                 N2.NO_DIAS_SS,
                 N2.NO_DIAS_SU,
                 N2.NO_HORA_PD,
                 N2.NO_LIQUIDA,
                 N2.NO_FUERA,
                 N2.NO_EXENTAS,
                 N2.NO_FEC_PAG,
                 N2.NO_USR_PAG,
				 N2.CB_NIVEL0,	
				 N2.CB_BAN_ELE,	
				 N2.NO_HORAPDT,	
				 N2.NO_HORASNT,	
				 N2.NO_DIAS_SI,
				 N2.NO_DIAS_VJ,	
				 N2.NO_DIAS_PV,	
				 N2.NO_SUP_OK,	
				 N2.NO_FEC_OK,	
				 N2.NO_HOR_OK,	
				 N2.NO_ASI_INI,	
				 N2.NO_ASI_FIN,	
				 N2.NO_DIAS_BA,	
				 N2.NO_PREV_GR,	
				 N2.CB_PLAZA,  	
				 N2.NO_FEC_LIQ,
				 N2.CB_CTA_GAS,
				 N2.CB_CTA_VAL,
				 N2.NO_PER_ISN
          from DBO.NOMINA as N2 where ( N2.PE_YEAR = @YEARORIGINAL ) and
                                      ( N2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( N2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( N2.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M2.CB_CODIGO,
                 M2.MO_ACTIVO,
                 M2.CO_NUMERO,
                 M2.MO_IMP_CAL,
                 M2.US_CODIGO,
                 M2.MO_PER_CAL,
                 M2.MO_REFEREN,
                 M2.MO_X_ISPT,
                 M2.MO_PERCEPC,
                 M2.MO_DEDUCCI
          from DBO.MOVIMIEN as M2 where ( M2.PE_YEAR = @YEARORIGINAL ) and
                                        ( M2.PE_TIPO = @TIPOORIGINAL ) and
                                        ( M2.PE_NUMERO = @NUMEROORIGINAL ) and
                                        ( M2.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  FA_FEC_INI,
                                  FA_DIA_HOR,
                                  FA_MOTIVO,
                                  FA_DIAS,
                                  FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F2.CB_CODIGO,
                 F2.FA_FEC_INI,
                 F2.FA_DIA_HOR,
                 F2.FA_MOTIVO,
                 F2.FA_DIAS,
                 F2.FA_HORAS
          from DBO.FALTAS as F2 where ( F2.PE_YEAR = @YEARORIGINAL ) and
                                      ( F2.PE_TIPO = @TIPOORIGINAL ) and
                                      ( F2.PE_NUMERO = @NUMEROORIGINAL ) and
                                      ( F2.CB_CODIGO = @EMPLEADO );
     end;
end
GO


/* PROCEDURE SP_CANCELA_NOMINA: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  46 */
if exists (select * from dbo.sysobjects where id = object_id(N'DBO.SP_CANCELA_NOMINA') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop Procedure DBO.SP_CANCELA_NOMINA
GO

/* PROCEDURE SP_CANCELA_NOMINA: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  46 */
create procedure DBO.SP_CANCELA_NOMINA(
@YEARORIGINAL SMALLINT,
@TIPOORIGINAL SMALLINT,
@NUMEROORIGINAL SMALLINT,
@YEARNUEVO SMALLINT,
@TIPONUEVO SMALLINT,
@NUMERONUEVO SMALLINT,
@EMPLEADO INTEGER,
@USUARIO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @StatusNuevo SmallInt;
     select @StatusNuevo = N.NO_STATUS from DBO.NOMINA as N
     where ( N.PE_YEAR = @YEARORIGINAL ) and
           ( N.PE_TIPO = @TIPOORIGINAL ) and
           ( N.PE_NUMERO = @NUMEROORIGINAL ) and
           ( N.CB_CODIGO = @EMPLEADO );
     if ( @StatusNuevo is not Null )
     begin
          if ( @StatusNuevo > 4 )
             set @StatusNuevo = 4;
          insert into DBO.NOMINA( PE_YEAR,
                                  PE_TIPO,
                                  PE_NUMERO,
                                  CB_CODIGO,
                                  CB_CLASIFI,
                                  CB_TURNO,
                                  CB_PATRON,
                                  CB_PUESTO,
                                  CB_ZONA_GE,
                                  NO_FOLIO_1,
                                  NO_FOLIO_2,
                                  NO_FOLIO_3,
                                  NO_FOLIO_4,
                                  NO_FOLIO_5,
                                  NO_OBSERVA,
                                  NO_STATUS,
                                  US_CODIGO,
                                  NO_USER_RJ,
                                  CB_NIVEL1,
                                  CB_NIVEL2,
                                  CB_NIVEL3,
                                  CB_NIVEL4,
                                  CB_NIVEL5,
                                  CB_NIVEL6,
                                  CB_NIVEL7,
                                  CB_NIVEL8,
                                  CB_NIVEL9,
                                  CB_SAL_INT,
                                  CB_SALARIO,
                                  NO_DIAS,
                                  NO_ADICION,
                                  NO_DEDUCCI,
                                  NO_NETO,
                                  NO_DES_TRA,
                                  NO_DIAS_AG,
                                  NO_DIAS_VA,
                                  NO_DOBLES,
                                  NO_EXTRAS,
                                  NO_FES_PAG,
                                  NO_FES_TRA,
                                  NO_HORA_CG,
                                  NO_HORA_SG,
                                  NO_HORAS,
                                  NO_IMP_CAL,
                                  NO_JORNADA,
                                  NO_PER_CAL,
                                  NO_PER_MEN,
                                  NO_PERCEPC,
                                  NO_TARDES,
                                  NO_TRIPLES,
                                  NO_TOT_PRE,
                                  NO_VAC_TRA,
                                  NO_X_CAL,
                                  NO_X_ISPT,
                                  NO_X_MENS,
                                  NO_D_TURNO,
                                  NO_DIAS_AJ,
                                  NO_DIAS_AS,
                                  NO_DIAS_CG,
                                  NO_DIAS_EM,
                                  NO_DIAS_FI,
                                  NO_DIAS_FJ,
                                  NO_DIAS_FV,
                                  NO_DIAS_IN,
                                  NO_DIAS_NT,
                                  NO_DIAS_OT,
                                  NO_DIAS_RE,
                                  NO_DIAS_SG,
                                  NO_DIAS_SS,
                                  NO_DIAS_SU,
                                  NO_HORA_PD,
                                  NO_LIQUIDA,
                                  NO_FUERA,
                                  NO_EXENTAS,
                                  NO_FEC_PAG,
                                  NO_USR_PAG,
								  CB_NIVEL0,  	
								  CB_BAN_ELE,	
								  NO_HORAPDT,	
								  NO_HORASNT,	
								  NO_DIAS_SI,	
								  NO_DIAS_VJ,	
							      NO_DIAS_PV,	
								  NO_SUP_OK,
								  NO_FEC_OK,	
								  NO_HOR_OK,	
								  NO_ASI_INI,	
								  NO_ASI_FIN,	
								  NO_DIAS_BA,	
								  NO_PREV_GR,	
								  CB_PLAZA,
								  NO_FEC_LIQ,
								  CB_CTA_GAS,
								  CB_CTA_VAL,
								  NO_PER_ISN
								  )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 N1.CB_CODIGO,
                 N1.CB_CLASIFI,
                 N1.CB_TURNO,
                 N1.CB_PATRON,
                 N1.CB_PUESTO,
                 N1.CB_ZONA_GE,
                 N1.NO_FOLIO_1,
                 N1.NO_FOLIO_2,
                 N1.NO_FOLIO_3,
                 N1.NO_FOLIO_4,
                 N1.NO_FOLIO_5,
                 N1.NO_OBSERVA,
                 @StatusNuevo,
                 N1.US_CODIGO,
                 N1.NO_USER_RJ,
                 N1.CB_NIVEL1,
                 N1.CB_NIVEL2,
                 N1.CB_NIVEL3,
                 N1.CB_NIVEL4,
                 N1.CB_NIVEL5,
                 N1.CB_NIVEL6,
                 N1.CB_NIVEL7,
                 N1.CB_NIVEL8,
                 N1.CB_NIVEL9,
                 N1.CB_SAL_INT,
                 N1.CB_SALARIO,
                 -N1.NO_DIAS,
                 -N1.NO_ADICION,
                 -N1.NO_DEDUCCI,
                 -N1.NO_NETO,
                 -N1.NO_DES_TRA,
                 -N1.NO_DIAS_AG,
                 -N1.NO_DIAS_VA,
                 -N1.NO_DOBLES,
                 -N1.NO_EXTRAS,
                 -N1.NO_FES_PAG,
                 -N1.NO_FES_TRA,
                 -N1.NO_HORA_CG,
                 -N1.NO_HORA_SG,
                 -N1.NO_HORAS,
                 -N1.NO_IMP_CAL,
                 -N1.NO_JORNADA,
                 -N1.NO_PER_CAL,
                 -N1.NO_PER_MEN,
                 -N1.NO_PERCEPC,
                 -N1.NO_TARDES,
                 -N1.NO_TRIPLES,
                 -N1.NO_TOT_PRE,
                 -N1.NO_VAC_TRA,
                 -N1.NO_X_CAL,
                 -N1.NO_X_ISPT,
                 -N1.NO_X_MENS,
                 -N1.NO_D_TURNO,
                 -N1.NO_DIAS_AJ,
                 -N1.NO_DIAS_AS,
                 -N1.NO_DIAS_CG,
                 -N1.NO_DIAS_EM,
                 -N1.NO_DIAS_FI,
                 -N1.NO_DIAS_FJ,
                 -N1.NO_DIAS_FV,
                 -N1.NO_DIAS_IN,
                 -N1.NO_DIAS_NT,
                 -N1.NO_DIAS_OT,
                 -N1.NO_DIAS_RE,
                 -N1.NO_DIAS_SG,
                 -N1.NO_DIAS_SS,
                 -N1.NO_DIAS_SU,
                 -N1.NO_HORA_PD,
                 N1.NO_LIQUIDA,
                 N1.NO_FUERA,
                 -N1.NO_EXENTAS,
                 N1.NO_FEC_PAG,
                 N1.NO_USR_PAG,
				 N1.CB_NIVEL0,  	
				 N1.CB_BAN_ELE,		
				 -N1.NO_HORAPDT,	
				 -N1.NO_HORASNT,
				 -N1.NO_DIAS_SI,
				 -N1.NO_DIAS_VJ,	
				 -N1.NO_DIAS_PV,	
				 N1.NO_SUP_OK,
				 N1.NO_FEC_OK,
				 N1.NO_HOR_OK,		
				 N1.NO_ASI_INI,		
				 N1.NO_ASI_FIN,		
				 -N1.NO_DIAS_BA,	
				 -N1.NO_PREV_GR,
				 N1.CB_PLAZA, 		
				 N1.NO_FEC_LIQ,
				 N1.CB_CTA_GAS, 		
				 N1.CB_CTA_VAL,
				 -N1.NO_PER_ISN
          from DBO.NOMINA as N1 where
          ( N1.PE_YEAR = @YEARORIGINAL ) and
          ( N1.PE_TIPO = @TIPOORIGINAL ) and
          ( N1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( N1.CB_CODIGO = @EMPLEADO );
          insert into DBO.MOVIMIEN( PE_YEAR,
                                    PE_TIPO,
                                    PE_NUMERO,
                                    CB_CODIGO,
                                    MO_ACTIVO,
                                    CO_NUMERO,
                                    MO_IMP_CAL,
                                    US_CODIGO,
                                    MO_PER_CAL,
                                    MO_REFEREN,
                                    MO_X_ISPT,
                                    MO_PERCEPC,
                                    MO_DEDUCCI )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 M1.CB_CODIGO,
                 M1.MO_ACTIVO,
                 M1.CO_NUMERO,
                 -M1.MO_IMP_CAL,
                 @USUARIO,
                 M1.MO_PER_CAL,
                 M1.MO_REFEREN,
                 -M1.MO_X_ISPT,
                 -M1.MO_PERCEPC,
                 -M1.MO_DEDUCCI
          from DBO.MOVIMIEN as M1 where
          ( M1.PE_YEAR = @YEARORIGINAL ) and
          ( M1.PE_TIPO = @TIPOORIGINAL ) and
          ( M1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( M1.CB_CODIGO = @EMPLEADO );
          insert into DBO.FALTAS( PE_YEAR,
                              PE_TIPO,
                              PE_NUMERO,
                              CB_CODIGO,
                              FA_FEC_INI,
                              FA_DIA_HOR,
                              FA_MOTIVO,
                              FA_DIAS,
                              FA_HORAS )
          select @YEARNUEVO,
                 @TIPONUEVO,
                 @NUMERONUEVO,
                 F1.CB_CODIGO,
                 F1.FA_FEC_INI,
                 F1.FA_DIA_HOR,
                 F1.FA_MOTIVO,
                 -F1.FA_DIAS,
                 -F1.FA_HORAS
          from DBO.FALTAS as F1 where
          ( F1.PE_YEAR = @YEARORIGINAL ) and
          ( F1.PE_TIPO = @TIPOORIGINAL ) and
          ( F1.PE_NUMERO = @NUMEROORIGINAL ) and
          ( F1.CB_CODIGO = @EMPLEADO );
     end
end
GO



/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Procedimiento  para ajustar el usuario del supervisor a un usuario especifico  */
/* CR: 1800 */
/* Patch 425 # Seq: 347 */
CREATE PROCEDURE SP_AJUSTA_USUARIO_SUPERVISOR( @US_CODIGO Usuario,  @US_JEFE Usuario  )
AS
BEGIN 
	SET NOCOUNT ON 
	UPDATE #COMPARTE.DBO.USUARIO 
	SET US_JEFE = @US_JEFE
	WHERE US_CODIGO = @US_CODIGO 
END 
/* Termina: Procedimiento  para ajustar el usuario del supervisor  */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
GO

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Procedimiento  para ajustar el usuario del supervisor a los usuarios de los empleados enrloados de un nivel de supervision determinado  */
/* CR: 1800 */	
/* Patch 425 # Seq: 348  */
CREATE PROCEDURE SP_AJUSTA_SUPERVISOR_ENROLADOS_COMPARTE( @NivelSupervisores Formula, @TB_CODIGO Codigo, @US_CODIGO Usuario)
AS
BEGIN 
	SET NOCOUNT ON 
   	
    		UPDATE #COMPARTE.DBO.USUARIO 
    		SET US_JEFE = @US_CODIGO 
    		WHERE US_CODIGO IN 
    		(
	    		SELECT CB.US_CODIGO FROM COLABORA CB
	    		WHERE COALESCE(CB.US_CODIGO,0) > 0 
	    		 AND (
		  		(CB.CB_NIVEL1 = @TB_CODIGO AND @NivelSupervisores = '1') OR
	        		(CB.CB_NIVEL2 = @TB_CODIGO AND @NivelSupervisores = '2') OR
		    		(CB.CB_NIVEL3 = @TB_CODIGO AND @NivelSupervisores = '3') OR
		    		(CB.CB_NIVEL4 = @TB_CODIGO AND @NivelSupervisores = '4') OR
		    		(CB.CB_NIVEL5 = @TB_CODIGO AND @NivelSupervisores = '5') OR
		    		(CB.CB_NIVEL6 = @TB_CODIGO AND @NivelSupervisores = '6') OR
		    		(CB.CB_NIVEL7 = @TB_CODIGO AND @NivelSupervisores = '7') OR
		    		(CB.CB_NIVEL8 = @TB_CODIGO AND @NivelSupervisores = '8') OR
		    		(CB.CB_NIVEL9 = @TB_CODIGO AND @NivelSupervisores = '9')
		    		/* ACS 
		    		OR 
		    		(CB.CB_NIVEL10 = @TB_CODIGO AND @NivelSupervisores = '10') OR
		    		(CB.CB_NIVEL11 = @TB_CODIGO AND @NivelSupervisores = '11') OR
		    		(CB.CB_NIVEL12 = @TB_CODIGO AND @NivelSupervisores = '12')	    		    		
		    		*/
	    		)    	
    		)
END
/* Termina: Procedimiento  para ajustar el usuario del supervisor a los usuarios de los empleados..  */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
Go 
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Procedimiento  para ajustar el usuario del supervisor a los usuarios de los empleados enrloados de un nivel de supervision determinado   */
/*  validando si se usa enrolamiento y si hay un nivel de supervisores determinado  */
/* CR: 1800 */
/* Patch 425 # Seq: 349 */
CREATE PROCEDURE SP_AJUSTA_SUPERVISOR_ENROLADOS( @Nivel Formula, @TB_CODIGO Codigo, @US_CODIGO Usuario)
AS
BEGIN 
	SET NOCOUNT ON 
   	DECLARE @NivelSupervisores Formula
	
   	SET @NivelSupervisores =  DBO.SP_GET_NIVEL_SUPERVISORES()
   	
	IF  DBO.SP_TIENE_ENROLAMIENTO_SUPERVISORES() = 'S' AND @NivelSupervisores = @Nivel
    	BEGIN 
    		EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS_COMPARTE @NivelSupervisores, @TB_CODIGO, @US_CODIGO    		
    	END
END
/* Termina: Procedimiento  para ajustar el usuario del supervisor a los usuarios de los empleados.. con validaci�n  */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
GO 

----------------------------------------------------------------------------------------------------


/*****************************************/
/****  FUNCIONES **************************/
/*****************************************/

/* (JB) Se agrega Funcion G_D para Presupuestos T1059 */
----------------------------------------------------------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'G_D') and xtype = 'FN')
	drop FUNCTION G_D
go
/* (JB) Se agrega Funcion G_D para Presupuestos T1059 */
/* Patch 425 # Seq: 336 */
CREATE FUNCTION G_D
(
	@Tabla varchar(10), 
	@Campo varchar(10)
)
RETURNS varchar(100)
AS
BEGIN
	Declare @Result varchar(100)
	set @Result = (
	select
		case 
			when comm.text = 'CREATE DEFAULT BooleanoNO AS ''N'' ' then 'N'
			when comm.text = 'CREATE DEFAULT CodigoVacio AS '''' ' then ''
			when comm.text = 'CREATE DEFAULT FechaVacia AS ''12/30/1899'' ' then '12/30/1899'
			when comm.text = 'CREATE DEFAULT StringVacio AS '''' ' then ''
			when comm.text = 'CREATE DEFAULT Cero AS 0 ' then '0'
			else '' 
		end as valorDefault
	FROM         sysobjects AS tbl INNER JOIN
                      syscolumns AS col ON tbl.id = col.id INNER JOIN
                      syscomments AS comm ON col.cdefault = comm.id
	WHERE tbl.xtype = 'u' and tbl.name = @Tabla and col.name = @Campo)
	return @Result
END
GO
/*Funcion para obtener el kardex de sillas de los empleados*/
/* Patch 425 # Seq: 338 */
create FUNCTION SP_FECHA_SILLA_KARDEX (
	@EMPLEADO INTEGER,
	@FECHA DATETIME,
	@LAYOUT CHAR(6),
	@HORA CHAR(4) )
RETURNS @RegKardex TABLE (
      	EM_FECHA DATETIME,
	EM_HORA CHAR(4),
	SL_CODIGO CHAR(6),
	US_CODIGO SMALLINT,
	EM_FEC_MOV DATETIME,
	EM_HOR_MOV CHAR(4) )
AS
BEGIN
     insert into @RegKardex
             select  TOP 1 EM_FECHA, EM_HORA, SL_CODIGO, US_CODIGO, EM_FEC_MOV, EM_HOR_MOV
             from KAR_EMPSIL
             where (LY_CODIGO = @LAYOUT) and ( ( CB_CODIGO = @Empleado ) and (  EM_FECHA <= @Fecha  ) )
             order by LLAVE desc,EM_FECHA desc, EM_HORA desc
     if ( @@ROWCOUNT = 0 )
     begin
          set @Fecha = '12/30/1899'
          insert into @RegKardex values ( @Fecha, '0000', '', 0, @Fecha, '0000' )
     end
     return
END
GO
 
/*Funcion para obtener las certificaciones de los empleados a cargo del supervisor*/
if exists (select * from sysobjects where name = 'SP_CERTIFIC_EMPL')
	drop function SP_CERTIFIC_EMPL
go
/*Funcion para obtener el kardex de sillas de los empleados*/
/* Patch 425 # Seq: 337 */

CREATE FUNCTION SP_CERTIFIC_EMPL (
	@USUARIO INTEGER,
	@FECHA DATETIME
	 )
RETURNS @RegKardex TABLE (
      	CI_CODIGO CHAR(6),
		KI_FEC_CER DATETIME,
		CB_CODIGO INT,
		KI_RENOVAR INT,
		KI_APROBO CHAR(1) 
     )
AS
BEGIN
     
 insert into @RegKardex
				select CI_CODIGO,KI_FEC_CER,CB_CODIGO,KI_RENOVAR,KI_APROBO 
				from KAR_CERT 
				where KI_APROBO = 'S' and
					 ( KI_RENOVAR = 0 or ( DateAdd(day,KI_RENOVAR,KI_FEC_CER ) >= @FECHA ) and (KI_FEC_CER <= @FECHA) )and
					 ( CB_CODIGO in (select CB_CODIGO from dbo.SP_LISTA_EMPLEADOS(@FECHA,@USUARIO ) ) )

    return
END
GO

/* Cambios para la sugerencia de Recibo Mensual CR:  1777     */
/* Comienza: Funcion SP_CMS, suma de conceptos con referencia y con rango de periodos */
/* Patch 425 # Seq: 339 */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CMS]') and OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
	drop function [dbo].[SP_CMS]
go
/* Comienza: Funcion SP_CMS, suma de conceptos con referencia y con rango de periodos */
/* Patch 425 # Seq: 339 */
CREATE FUNCTION SP_CMS 
		(@CONCEPTO Concepto,
		@REFERENCIA Referencia,
		@NUMEROINICIAL NominaNumero,
		@NUMEROFINAL NominaNumero,
		@TIPO NominaTipo,
		@ANIO Anio,
		@EMPLEADO NumeroEmpleado)

RETURNS NUMERIC(15,2)
AS
begin
	Declare @Resultado Pesos
	if ( @Referencia = '' ) 
	begin
		Select @Resultado = dbo.SP_CS( @Concepto, @NumeroInicial, @NumeroFinal, @Tipo, @Anio, @Empleado )
	end
	else
	begin
		select @Resultado = SUM(MO_PERCEPC+MO_DEDUCCI)
		from   MOVIMIEN
		where ( CB_CODIGO = @Empleado ) 
		AND ( PE_YEAR = @Anio ) 
		AND ( PE_TIPO = @Tipo ) 
		AND ( PE_NUMERO between @NumeroInicial and @NumeroFinal ) 
		AND ( CO_NUMERO = @Concepto ) 
		AND ( MO_REFEREN = @Referencia ) 
		AND ( MO_ACTIVO = 'S' )
	end
	RETURN @Resultado
end
GO
/* Termina: Funcion SP_CMS, suma de conceptos con referencia y con rango de periodos */
----------------------------------------------------------------------------------------------------
/* Comienza: SP_SALDOPRE */
/* Patch 425 # Seq: 340 */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SALDOPRE]') and OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
	drop function [dbo].[SP_SALDOPRE]
go
/* Comienza: SP_SALDOPRE */
/* Patch 425 # Seq: 340 */
CREATE FUNCTION SP_SALDOPRE ( @Empleado NumeroEmpleado, 
			@Concepto Concepto,
			@Referencia Referencia,
			@FechaPeriodo Fecha )
RETURNS Pesos 
AS
begin
  declare @Monto Pesos;
  declare @SaldoI Pesos;
  declare @Cargos Pesos;
  declare @Abonos Pesos;
  declare @Deducci Pesos;
  declare @Resultado Pesos;
  
  select @Monto  	= 0;
  select @SaldoI  	= 0;
  select @Cargos  	= 0;
  select @Abonos  	= 0;
  select @Deducci	= 0;
  select @Resultado = 0;
  	

  	SELECT 	@Monto = Coalesce( PRESTAMO.PR_MONTO, 0 ), 
		@SaldoI = coalesce( PRESTAMO.PR_SALDO_I, 0 )
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	WHERE ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_REFEREN = @Referencia)
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )

	SELECT @Deducci=  coalesce(sum(MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI),0 ) 
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	LEFT OUTER JOIN MOVIMIEN ON MOVIMIEN.CB_CODIGO = PRESTAMO.CB_CODIGO
			AND MOVIMIEN.CO_NUMERO = TPRESTA.TB_CONCEPT
			AND MOVIMIEN.MO_REFEREN = PRESTAMO.PR_REFEREN
	LEFT OUTER JOIN NOMINA 
 			ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
			AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO 
			AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
			AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	LEFT OUTER JOIN PERIODO
			ON NOMINA.PE_YEAR = PERIODO.PE_YEAR 
			AND NOMINA.PE_TIPO = PERIODO.PE_TIPO 
			AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO 

	WHERE 	( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_REFEREN = @Referencia)
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND	( NOMINA.NO_STATUS=6 ) 
	AND ( MOVIMIEN.MO_ACTIVO = 'S') 
	AND ( MOVIMIEN.CO_NUMERO = @Concepto )
	AND ( PE_FEC_FIN>= PR_FECHA )
	AND ( PE_FEC_FIN <= @FechaPeriodo)
	
	
	SELECT 	@Cargos = coalesce( sum(PCAR_ABO.CR_CARGO ), 0 ),
		@Abonos = coalesce( sum(PCAR_ABO.CR_ABONO) , 0 ) 
	FROM PCAR_ABO 
	LEFT OUTER JOIN PRESTAMO ON PRESTAMO.CB_CODIGO = PCAR_ABO.CB_CODIGO 	
							 AND PRESTAMO.PR_TIPO = PCAR_ABO.PR_TIPO
	  						 AND PRESTAMO.PR_REFEREN = PCAR_ABO.PR_REFEREN
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	where  ( PCAR_ABO.CR_FECHA<= @FechaPeriodo )
	AND ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_REFEREN = @Referencia)
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )
	
	
	--Como se calcula el saldo actual:
	--	   Monto del Prestamo (@Monto)
	--	- Abonos a�os anteriores (@SaldoI)
	--	- Abonado en el a�o (sum( @Deducci de Movimien))
	--	- Otros Abonos (sum(@cr_abonos de PCarAbo)
	--	+ Otro Cargos (sum(@cr_cargos de PCarAbo)
	--	= Saldo actual
	Select @Resultado = @Monto - @SaldoI - @Deducci - @Abonos + @Cargos;
		
	RETURN @Resultado
end
go
/* Termina: SP_SALDOPRE */


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SALDOAHO]') and OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
	drop function [dbo].[SP_SALDOAHO]
go

/* Comienza: SP_SALDOAHO */
/* Patch 425 # Seq: 341 */
CREATE FUNCTION SP_SALDOAHO ( @Empleado NumeroEmpleado, 
			@Concepto Concepto,
			@FechaPeriodo Fecha )
RETURNS Pesos 
AS
begin
  declare @Monto Pesos;
  declare @SaldoI Pesos;
  declare @Cargos Pesos;
  declare @Abonos Pesos;
  declare @Deducci Pesos;
  declare @Resultado Pesos;
  
  select @SaldoI  	= 0;
  select @Cargos  	= 0;
  select @Abonos  	= 0;
  select @Deducci	= 0;
  select @Resultado = 0;
  	

  	SELECT 	@SaldoI = coalesce( AHORRO.AH_SALDO_I, 0 )
	FROM AHORRO
	LEFT OUTER JOIN TAHORRO ON TAHORRO.TB_CODIGO = AHORRO.AH_TIPO
	WHERE ( AHORRO.CB_CODIGO = @Empleado )
	AND ( AHORRO.AH_STATUS = 0 )
	AND ( TAHORRO.TB_CONCEPT = @Concepto )
	
	SELECT @Deducci=  coalesce(sum(MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI),0 ) 
	FROM AHORRO
	LEFT OUTER JOIN TAHORRO ON TAHORRO.TB_CODIGO = AHORRO.AH_TIPO
	LEFT OUTER JOIN MOVIMIEN ON MOVIMIEN.CB_CODIGO = AHORRO.CB_CODIGO
			AND MOVIMIEN.CO_NUMERO = TAHORRO.TB_CONCEPT
	LEFT OUTER JOIN NOMINA 
 			ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
			AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO 
			AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
			AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	LEFT OUTER JOIN PERIODO
			ON NOMINA.PE_YEAR = PERIODO.PE_YEAR 
			AND NOMINA.PE_TIPO = PERIODO.PE_TIPO 
			AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO 

	WHERE  ( AHORRO.CB_CODIGO = @Empleado )
	AND ( AHORRO.AH_STATUS = 0 )
	AND ( NOMINA.NO_STATUS=6 ) 
	AND ( MOVIMIEN.MO_ACTIVO = 'S') 
	AND ( MOVIMIEN.CO_NUMERO = @Concepto )
	AND ( PE_FEC_FIN>= AH_FECHA )
	AND ( PE_FEC_FIN <= @FechaPeriodo)
	
		
	
	SELECT 	@Cargos = coalesce( sum(ACAR_ABO.CR_CARGO ), 0 ),
		@Abonos = coalesce( sum(ACAR_ABO.CR_ABONO) , 0 ) 
	FROM ACAR_ABO 
	LEFT OUTER JOIN AHORRO ON AHORRO.CB_CODIGO = ACAR_ABO.CB_CODIGO AND AHORRO.AH_TIPO = ACAR_ABO.AH_TIPO
	LEFT OUTER JOIN TAHORRO ON TAHORRO.TB_CODIGO = AHORRO.AH_TIPO
	where  ( ACAR_ABO.CR_FECHA<= @FechaPeriodo )
	AND ( AHORRO.CB_CODIGO = @Empleado )
	AND ( AHORRO.AH_STATUS = 0 )
	AND ( TAHORRO.TB_CONCEPT = @Concepto )
		
	--Como se calcula el saldo actual:
	--	   Ahorrado en el a�o sum( @Deducci de Movimien))
	--	+ Abonos a�os anteriores (@SaldoI)
	--	+ Otros Abonos (sum(@cr_abonos de ACarAbo)
	--	- Otro Cargos (sum(@cr_cargos de ACarAbo)
	--	= Saldo actual
	Select @Resultado = @Deducci + @SaldoI + @Abonos - @Cargos;
		
	RETURN @Resultado
end
/* Termina: SP_SALDOAHO */
go


/* Comienza: SP_SALDOPRE_SR */
/* Patch 425 # Seq: 342 */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SALDOPRE_SR]') and OBJECTPROPERTY(id, N'IsScalarFunction') = 1)
	drop function [dbo].[SP_SALDOPRE_SR]
go

/* Comienza: SP_SALDOPRE_SR */
/* Patch 425 # Seq: 342 */
CREATE FUNCTION SP_SALDOPRE_SR ( @Empleado NumeroEmpleado, 
			@Concepto Concepto,
			@FechaPeriodo Fecha )
RETURNS Pesos 
AS
begin
  declare @Monto Pesos;
  declare @SaldoI Pesos;
  declare @Cargos Pesos;
  declare @Abonos Pesos;
  declare @Deducci Pesos;
  declare @Resultado Pesos;
  declare @ResPresta Pesos;
  
  select @Monto  	= 0;
  select @SaldoI  	= 0;
  select @Cargos  	= 0;
  select @Abonos  	= 0;
  select @Deducci	= 0;
  select @Resultado = 0;
  select @ResPresta = 0;
  	
	
	SELECT 	@Monto = sum(coalesce( PRESTAMO.PR_MONTO, 0 )), 
			@SaldoI = sum(coalesce( PRESTAMO.PR_SALDO_I, 0 ))
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	WHERE ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )
	
	
	SELECT @Deducci=  coalesce(sum(MOVIMIEN.MO_PERCEPC+MOVIMIEN.MO_DEDUCCI),0 ) 
	FROM PRESTAMO
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	LEFT OUTER JOIN MOVIMIEN ON MOVIMIEN.CB_CODIGO = PRESTAMO.CB_CODIGO
			AND MOVIMIEN.CO_NUMERO = TPRESTA.TB_CONCEPT
			AND MOVIMIEN.MO_REFEREN = PRESTAMO.PR_REFEREN
	LEFT OUTER JOIN NOMINA 
			ON NOMINA.PE_YEAR = MOVIMIEN.PE_YEAR 
			AND NOMINA.PE_TIPO = MOVIMIEN.PE_TIPO 
			AND NOMINA.PE_NUMERO = MOVIMIEN.PE_NUMERO 
			AND NOMINA.CB_CODIGO = MOVIMIEN.CB_CODIGO
	LEFT OUTER JOIN PERIODO
			ON NOMINA.PE_YEAR = PERIODO.PE_YEAR 
			AND NOMINA.PE_TIPO = PERIODO.PE_TIPO 
			AND NOMINA.PE_NUMERO = PERIODO.PE_NUMERO 

	WHERE  ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( NOMINA.NO_STATUS=6 ) 
	AND ( MOVIMIEN.MO_ACTIVO = 'S') 
	AND ( MOVIMIEN.CO_NUMERO = @Concepto )
	AND ( PE_FEC_FIN>= PR_FECHA )
	AND ( PE_FEC_FIN <= @FechaPeriodo)
		
		
	SELECT 	@Cargos = coalesce( sum(PCAR_ABO.CR_CARGO ), 0 ),
			@Abonos = coalesce( sum(PCAR_ABO.CR_ABONO) , 0 ) 
	FROM PCAR_ABO 
	LEFT OUTER JOIN PRESTAMO ON PRESTAMO.CB_CODIGO = PCAR_ABO.CB_CODIGO 
							 AND PRESTAMO.PR_TIPO = PCAR_ABO.PR_TIPO
							 AND PRESTAMO.PR_REFEREN = PCAR_ABO.PR_REFEREN
	LEFT OUTER JOIN TPRESTA ON TPRESTA.TB_CODIGO = PRESTAMO.PR_TIPO
	where  ( PCAR_ABO.CR_FECHA<= @FechaPeriodo )
	AND ( PRESTAMO.CB_CODIGO = @Empleado )
	AND ( PRESTAMO.PR_STATUS = 0 )
	AND ( TPRESTA.TB_CONCEPT = @Concepto )
	
		
	--COMO SE CALCULA EL SALDO ACTUAL:
	--	   MONTO DEL PRESTAMO (@MONTO)
	--	- ABONOS A�OS ANTERIORES (@SALDOI)
	--	- ABONADO EN EL A�O (SUM( @DEDUCCI DE MOVIMIEN))
	--	- OTROS ABONOS (SUM(@CR_ABONOS DE PCARABO)
	--	+ OTRO CARGOS (SUM(@CR_CARGOS DE PCARABO)
	--	= SALDO ACTUAL
	SELECT @RESPRESTA = @MONTO - @SALDOI - @DEDUCCI - @ABONOS + @CARGOS;
	
	--SE VA ACUMULANDO EL RESULTADO DE CADA PRESTAMO.
	SELECT @RESULTADO = @RESULTADO + @RESPRESTA;
		
	RETURN Coalesce( @Resultado,0)
end

GO
/* Termina: SP_SALDOPRE_SR */

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Funcion para verificar por global si se usa enrolamiento por supervisores */
/* CR: 1800 */
/* Patch 425 # Seq: 343*/
CREATE FUNCTION SP_TIENE_ENROLAMIENTO_SUPERVISORES() RETURNS Booleano
AS
BEGIN
	DECLARE @GLOBAL_Usar_Enrolamiento_de_Supervisores FolioChico
	
	SET @GLOBAL_Usar_Enrolamiento_de_Supervisores = 288
	
	IF ( SELECT COUNT(*) FROM GLOBAL WHERE GL_FORMULA = 'S' AND GL_CODIGO = @GLOBAL_Usar_Enrolamiento_de_Supervisores ) > 0 
		RETURN 'S'
		
	RETURN 'N'
END
/* Termina:Funcion para verificar por global si se usa enrolamiento por supervisores */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */

GO

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Funcion para tomar el numero de nivel de Supervisores por global */
/* CR: 1800 */
/* Patch 425 # Seq: 344 */
CREATE FUNCTION SP_GET_NIVEL_SUPERVISORES() RETURNS Formula
AS
BEGIN
	DECLARE @GLOBAL_Super_Nivel_de_Organigrama FolioChico
	DECLARE @Resultado Formula
	
	SET @GLOBAL_Super_Nivel_de_Organigrama = 120
		
	SELECT @Resultado = GL_FORMULA  FROM GLOBAL WHERE  GL_CODIGO = @GLOBAL_Super_Nivel_de_Organigrama 
	
	SET @Resultado = COALESCE( @Resultado, '');

	RETURN @Resultado
END
/* Termina: Funci�n para tomar el numero de nivel de Supervisores por globales */                

GO 

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Funcion para tomar el usuario del supervisor seg�n codigo de nivel */
/* CR: 1800 */
/* Patch 425 # Seq:  345*/
CREATE FUNCTION SP_GET_USUARIO_SUPERVISOR( @TB_CODIGO Codigo , @Nivel Formula ) RETURNS Usuario
AS
BEGIN 
	DECLARE @US_CODIGO Usuario
	
	SELECT  @US_CODIGO = 
		CASE  @Nivel 
			WHEN '1' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL1 WHERE TB_CODIGO = @TB_CODIGO) 
			WHEN '2' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL2 WHERE TB_CODIGO = @TB_CODIGO) 
			WHEN '3' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL3 WHERE TB_CODIGO = @TB_CODIGO) 			
			WHEN '4' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL4 WHERE TB_CODIGO = @TB_CODIGO) 			
			WHEN '5' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL5 WHERE TB_CODIGO = @TB_CODIGO) 			
			WHEN '6' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL6 WHERE TB_CODIGO = @TB_CODIGO) 			
			WHEN '7' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL7 WHERE TB_CODIGO = @TB_CODIGO) 			
			WHEN '8' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL8 WHERE TB_CODIGO = @TB_CODIGO) 			
			WHEN '9' THEN (SELECT TOP 1 US_CODIGO FROM NIVEL9 WHERE TB_CODIGO = @TB_CODIGO) 			
/* ACS 
			WHEN '10' THEN (SELECT US_CODIGO FROM NIVEL10 WHERE TB_CODIGO = @TB_CODIGO)
			WHEN '11' THEN (SELECT US_CODIGO FROM NIVEL11 WHERE TB_CODIGO = @TB_CODIGO) 																											
			WHEN '12' THEN (SELECT US_CODIGO FROM NIVEL12 WHERE TB_CODIGO = @TB_CODIGO)
*/
		END
		
	RETURN COALESCE(@US_CODIGO, 0)
END
/* Termina: Funci�n para tomar el usuario del supervisor seg�n codigo de nivel */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
GO 


/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza: Funcion para tomar el usuario del supervisor correspondiente seg�n un n�mero de empleado  */
/* CR: 1800 */
/* Patch 425 # Seq: 346 */
CREATE FUNCTION SP_GET_SUPERVISOR_ENROLADO_POR_EMPLEADO( @CB_CODIGO NumeroEmpleado )
RETURNS @SupervisorEnrolado 
TABLE (
 CB_NIVEL_COD CHAR(10),
 US_JEFE SMALLINT,
 USA_ENROL TINYINT  -- 0 No  -- 1 Si 
)
AS
BEGIN 
	DECLARE @NivelSupervisores Formula	
	DECLARE @CB_NIVELSup Codigo     		     		
     	DECLARE @US_JEFE Usuario	
     	DECLARE @USA_ENROL SMALLINT
     	SET @CB_NIVELSup = '';
     	SET @US_JEFE = 0;

	IF DBO.SP_TIENE_ENROLAMIENTO_SUPERVISORES()='S'
		SET @USA_ENROL = 1
	ELSE
		SET @USA_ENROL = 0 
	
	IF  @USA_ENROL = 1 
	BEGIN 	
		SET @NivelSupervisores =  DBO.SP_GET_NIVEL_SUPERVISORES()
		
		-- Verificar si cambio CB_NIVEL[SUPERVISOR]
		IF  @NivelSupervisores <> ''
		BEGIN      		     		
				SELECT  @CB_NIVELSup = 
			CASE  @NivelSupervisores 
				WHEN '1' THEN CB_NIVEL1   
				WHEN '2' THEN CB_NIVEL2   			
				WHEN '3' THEN CB_NIVEL3   
				WHEN '4' THEN CB_NIVEL4   
				WHEN '5' THEN CB_NIVEL5  
				WHEN '6' THEN CB_NIVEL6   
				WHEN '7' THEN CB_NIVEL7  
				WHEN '8' THEN CB_NIVEL8  
				WHEN '9' THEN CB_NIVEL9  			
			/* ACS 
				WHEN '10' THEN CB_NIVEL10
				WHEN '11  THEN CB_NIVEL11
				WHEN '12' THEN CB_NIVEL12
			*/
			END			
			FROM COLABORA WHERE cb_codigo = @CB_CODIGO
		END
	END
	
	SET @CB_NIVELSup = COALESCE(@CB_NIVELSup, '')
	
	IF (@CB_NIVELSup <> '')
	 SET @US_JEFE = dbo.SP_GET_USUARIO_SUPERVISOR( @CB_NIVELSup, @NivelSupervisores )
	 
	SET @US_JEFE = COALESCE(@US_JEFE, 0)
	
	
	INSERT INTO @SupervisorEnrolado
	        ( CB_NIVEL_COD ,
	          US_JEFE, 
	          USA_ENROL
	        )
	VALUES  ( @CB_NIVELSup, -- CB_NIVEL_COD - char(10)
	          @US_JEFE,  -- US_JEFE - smallint
	          @USA_ENROL  -- USA_ENROL status
	        )
	       
	RETURN 

END 

/* Termina: Funcion para tomar el usuario del supervisor correspondiente seg�n un n�mero de empleado */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
go 

/* PROCEDURE SP_VALOR_CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  276 */
if exists (select * from dbo.sysobjects where id = object_id(N'SP_VALOR_CONCEPTO') )
	drop function DBO.SP_VALOR_CONCEPTO
GO

/* PROCEDURE SP_VALOR_CONCEPTO: CR 1878 Calculo Impuestos Estatal */
/* Patch 425 # Seq:  276 */
CREATE  FUNCTION DBO.SP_VALOR_CONCEPTO(
 				@Concepto SMALLINT,
				@Mes SMALLINT,
				@Anio SMALLINT,
				@Empleado INTEGER,
				@Razonsocial CHAR(6),
				@TipoNomina SMALLINT,							
				@ValidaReingreso CHAR(1)
				 )

RETURNS NUMERIC(15,2)
AS
BEGIN
     DECLARE @M1000 NUMERIC(15,2);
     DECLARE @M1001 NUMERIC(15,2);
     DECLARE @M1002 NUMERIC(15,2);
     DECLARE @M1003 NUMERIC(15,2);
     DECLARE @M1004 NUMERIC(15,2);
     DECLARE @M1005 NUMERIC(15,2);
     DECLARE @M1006 NUMERIC(15,2);
     DECLARE @M1007 NUMERIC(15,2);
     DECLARE @M1008 NUMERIC(15,2);
     DECLARE @M1009 NUMERIC(15,2);
     DECLARE @M1010 NUMERIC(15,2);
     DECLARE @M1011 NUMERIC(15,2);
     DECLARE @M1012 NUMERIC(15,2);
     DECLARE @M1100 NUMERIC(15,2);
     DECLARE @M1101 NUMERIC(15,2);
     DECLARE @M1102 NUMERIC(15,2);
     DECLARE @M1103 NUMERIC(15,2);
     DECLARE @M1104 NUMERIC(15,2);
     DECLARE @M1105 NUMERIC(15,2);
     DECLARE @M1106 NUMERIC(15,2);
     DECLARE @M1107 NUMERIC(15,2);
     DECLARE @M1108 NUMERIC(15,2);
     DECLARE @M1109 NUMERIC(15,2);
     DECLARE @M1110 NUMERIC(15,2);
     DECLARE @M1111 NUMERIC(15,2);
     DECLARE @M1112 NUMERIC(15,2);
     DECLARE @M1113 NUMERIC(15,2);
     DECLARE @M1114 NUMERIC(15,2);
     DECLARE @M1115 NUMERIC(15,2);
     DECLARE @M1116 NUMERIC(15,2);
     DECLARE @M1117 NUMERIC(15,2);
     DECLARE @M1118 NUMERIC(15,2);
     DECLARE @M1119 NUMERIC(15,2);
     DECLARE @M1120 NUMERIC(15,2);
     DECLARE @M1121 NUMERIC(15,2);
     DECLARE @M1122 NUMERIC(15,2);
     DECLARE @M1123 NUMERIC(15,2);
     DECLARE @M1200 NUMERIC(15,2);
     DECLARE @M1201 NUMERIC(15,2);
     DECLARE @M1202 NUMERIC(15,2);
     DECLARE @M1203 NUMERIC(15,2);
     DECLARE @M1204 NUMERIC(15,2);
     DECLARE @M1205 NUMERIC(15,2);
     DECLARE @M1206 NUMERIC(15,2);
     DECLARE @M1207 NUMERIC(15,2);
     DECLARE @M1208 NUMERIC(15,2);
     DECLARE @M1209 NUMERIC(15,2);
     DECLARE @M1210 NUMERIC(15,2);
     DECLARE @M1211 NUMERIC(15,2);
     DECLARE @M1212 NUMERIC(15,2);
     DECLARE @M1213 NUMERIC(15,2);
     DECLARE @M1214 NUMERIC(15,2);
     DECLARE @M1215 NUMERIC(15,2);
	 DECLARE @M1216 NUMERIC(15,2);     
	 DECLARE @TipoNom SMALLINT;
     DECLARE @Resultado NUMERIC(15,2);

select @Resultado = 0;

if ( @Concepto <> 1004 ) 
begin
	DECLARE TemporalCursorNom Cursor for
	
     select N.NO_PERCEPC,
            N.NO_DEDUCCI,
            N.NO_NETO,
            N.NO_X_ISPT,
            N.NO_PER_MEN,
            N.NO_X_MENS,
            N.NO_PER_CAL,
            N.NO_IMP_CAL,
            N.NO_X_CAL,
            N.NO_TOT_PRE,
			N.NO_PREV_GR,
			N.NO_PER_ISN,
            N.NO_DIAS_FI,
            N.NO_DIAS_FJ,
            N.NO_DIAS_CG,
            N.NO_DIAS_SG,
            N.NO_DIAS_SU,
            N.NO_DIAS_OT,
            N.NO_DIAS_IN,
            N.NO_DIAS * 7 / 6,
            N.NO_DIAS_RE,
            N.NO_D_TURNO,
            N.NO_DIAS,
            N.NO_DIAS_AS,
            N.NO_DIAS_NT,
            N.NO_DIAS_FV,
            N.NO_DIAS_VA,
            N.NO_DIAS_AG,
            N.NO_DIAS_AJ,
            N.NO_DIAS_SS,
            N.NO_DIAS_EM,
            N.NO_DIAS_SI,
            N.NO_DIAS_BA,
            N.NO_DIAS_PV,
            N.NO_DIAS_VJ,
            N.NO_DIAS_VJ * 7 / 6,
            N.NO_JORNADA,         
            N.NO_HORAS,           
            N.NO_EXTRAS,          
            N.NO_DOBLES,
            N.NO_TRIPLES,
            N.NO_ADICION,
            N.NO_TARDES,
            N.NO_HORA_PD,
            N.NO_HORA_CG,
            N.NO_HORA_SG,
            N.NO_FES_TRA,
            N.NO_DES_TRA,
            N.NO_VAC_TRA,
            N.NO_FES_PAG,
            N.NO_HORASNT,
            N.NO_HORAPDT,
		    N.PE_TIPO,
			N.NO_PRE_EXT

		  from NOMINA N 
		  LEFT OUTER JOIN PERIODO ON PERIODO.PE_YEAR = N.PE_YEAR AND PERIODO.PE_TIPO = N.PE_TIPO AND PERIODO.PE_NUMERO = N.PE_NUMERO
		  LEFT OUTER JOIN RPATRON ON RPATRON.TB_CODIGO = N.CB_PATRON
		  LEFT OUTER JOIN RSOCIAL ON RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
		  LEFT OUTER JOIN COLABORA ON COLABORA.CB_CODIGO = N.CB_CODIGO
		where
			( N.PE_YEAR = @Anio ) and
			( N.CB_CODIGO = @Empleado ) and
			( PERIODO.PE_STATUS = 6 ) and
			( PERIODO.PE_MES = @Mes ) and
			( ( @TipoNomina = -1 ) or ( N.PE_TIPO = @TipoNomina ) ) and			 
			( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and			 
      		( ( @ValidaReingreso <> 'S' ) or ( COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN ) ) 
			 
			 
			
		open TemporalCursorNom
		Fetch next from TemporalCursorNom
		into 
        @M1000,
        @M1001,
        @M1002,
        @M1003,
        @M1005,
        @M1006,
        @M1007,
        @M1008,
        @M1009,
        @M1010,
        @M1011,
		@M1012,
        @M1100,
        @M1101,
        @M1102,
        @M1103,
        @M1104,
        @M1105,
        @M1106,
        @M1107,
        @M1108,
        @M1109,
        @M1110,
        @M1111,
        @M1112,
        @M1113,
        @M1114,
        @M1115,
        @M1116,
        @M1117,
        @M1118,
        @M1119,
        @M1120,
        @M1121,
        @M1122,
        @M1123,
        @M1200,
        @M1201,
        @M1202,
        @M1203,
        @M1204,
        @M1205,
        @M1206,
        @M1207,
        @M1208,
        @M1209,
        @M1210,
        @M1211,
        @M1212,
        @M1213,
        @M1214,
        @M1215,
        @TipoNom,
		@M1216
        

	WHILE @@FETCH_STATUS = 0
     begin
		
          if ( @Concepto = 1200 or @Concepto = 1109 ) and ( @M1200 <> 0 ) 
          begin
                Select @M1200 = @M1201 / @M1200;
          end
          if ( @Concepto = 1000 ) and (@M1000 <> 0)
            select @Resultado = @M1000 + @Resultado
          if ( @Concepto = 1001 ) and (@M1001 <> 0)
            select @Resultado = @M1001 + @Resultado
          if ( @Concepto = 1002 ) and (@M1002 <> 0)
            select @Resultado = @M1002 + @Resultado
          if ( @Concepto = 1003 ) and (@M1003 <> 0)
            select @Resultado = @M1003 + @Resultado
          if ( @Concepto = 1005 ) and (@M1005 <> 0)
            select @Resultado = @M1005 + @Resultado
          if ( @Concepto = 1006 ) and (@M1006 <> 0)
            select @Resultado = @M1006 + @Resultado
          if ( @Concepto = 1007 ) and (@M1007 <> 0)
            select @Resultado = @M1007 + @Resultado
          if ( @Concepto = 1008 ) and (@M1008 <> 0)
            select @Resultado = @M1008 + @Resultado
          if ( @Concepto = 1009 ) and (@M1009 <> 0)
            select @Resultado = @M1009 + @Resultado
          if ( @Concepto = 1010 ) and (@M1010 <> 0)
            select @Resultado = @M1010 + @Resultado
          if ( @Concepto = 1011 ) and (@M1011 <> 0)
            select @Resultado = @M1011 + @Resultado
          if ( @Concepto = 1012 ) and (@M1012 <> 0)
            select @Resultado = @M1012 + @Resultado
          if ( @Concepto = 1100 ) and (@M1100 <> 0)
            select @Resultado = @M1100 + @Resultado
          if ( @Concepto = 1101 ) and (@M1101 <> 0)
            select @Resultado = @M1101 + @Resultado
          if ( @Concepto = 1102 ) and (@M1102 <> 0)
            select @Resultado = @M1102 + @Resultado
          if ( @Concepto = 1103 ) and (@M1103 <> 0)
            select @Resultado = @M1103 + @Resultado
          if ( @Concepto = 1104 ) and (@M1104 <> 0)
            select @Resultado = @M1104 + @Resultado
          if ( @Concepto = 1105 ) and (@M1105 <> 0)
            select @Resultado = @M1105 + @Resultado
          if ( @Concepto = 1106 ) and (@M1106 <> 0)
            select @Resultado = @M1106 + @Resultado
          if ( @Concepto = 1107 ) and (@M1107 <> 0)
            select @Resultado = @M1107 + @Resultado
          if ( @Concepto = 1108 ) and (@M1108 <> 0)
            select @Resultado = @M1108 + @Resultado
          if ( @Concepto = 1109 ) and (@M1109 <> 0) 
		begin
			select @M1109 = @M1200 * @M1109;
               select @Resultado = @M1109 + @Resultado
		end
          if ( @Concepto = 1110 ) and (@M1110 <> 0)
            select @Resultado = @M1110 + @Resultado
          if ( @Concepto = 1111 ) and (@M1111 <> 0)
            select @Resultado = @M1111 + @Resultado
          if ( @Concepto = 1112 ) and (@M1112 <> 0)
            select @Resultado = @M1112 + @Resultado
          if ( @Concepto = 1113 ) and (@M1113 <> 0)
            select @Resultado = @M1113 + @Resultado
          if ( @Concepto = 1114 ) and (@M1114 <> 0)
            select @Resultado = @M1114 + @Resultado
          if ( @Concepto = 1115 ) and (@M1115 <> 0)
            select @Resultado = @M1115 + @Resultado
          if ( @Concepto = 1116 ) and (@M1116 <> 0)
            select @Resultado = @M1116 + @Resultado
          if ( @Concepto = 1117 ) and (@M1117 <> 0)
            select @Resultado = @M1117 + @Resultado
          if ( @Concepto = 1118 ) and (@M1118 <> 0)
            select @Resultado = @M1118 + @Resultado
          if ( @Concepto = 1119 ) and (@M1119 <> 0)
              select @Resultado = @M1119 + @Resultado
          if ( @Concepto = 1120 ) and (@M1120 <> 0)
            select @Resultado = @M1120 + @Resultado
          if ( @Concepto = 1121 ) and (@M1121 <> 0)
            select @Resultado = @M1121 + @Resultado
          if ( @Concepto = 1122 ) and (@M1122 <> 0)
            select @Resultado = @M1122 + @Resultado
          if ( @Concepto = 1123 ) and (@M1123 <> 0)
            select @Resultado = @M1123 + @Resultado
          if ( @Concepto = 1200 ) and (@M1200 <> 0 )
     	    begin
              if ( @TipoNom = 0 )
                Select @M1200 = 8 * @M1200;
              else
              if ( ( @TipoNom = 1 ) or ( @TipoNom = 6 ) or ( @TipoNom = 7 ) )
                Select @M1200 = 48 * @M1200;
              else
              if ( ( @TipoNom = 2 ) or ( @TipoNom = 10 ) or ( @TipoNom = 11 ) )
                Select @M1200 = 96 * @M1200;
              else
              if ( ( @TipoNom = 3 ) or ( @TipoNom = 8 ) or ( @TipoNom = 9 ) )
                Select @M1200 = 104 * @M1200;
              else
              if ( @TipoNom = 4 )
                Select @M1200 = 208 * @M1200;
              else
              if ( @TipoNom = 5 )
                Select @M1200 = 72 * @M1200;
              select @Resultado = @M1200 + @Resultado
        end
     		if ( @Concepto = 1201 and @M1201 <> 0 )
          select @Resultado = @M1201 + @Resultado
        if ( @Concepto = 1202 and @M1202 <> 0 )
          select @Resultado = @M1202 + @Resultado
        if ( @Concepto = 1203 and @M1203 <> 0 )
          select @Resultado = @M1203 + @Resultado
        if ( @Concepto = 1204 and @M1204 <> 0 )
          select @Resultado = @M1204 + @Resultado
        if ( @Concepto = 1205 and @M1205 <> 0 )
          select @Resultado = @M1205 + @Resultado
        if ( @Concepto = 1206 and @M1206 <> 0 )
          select @Resultado = @M1206 + @Resultado
        if ( @Concepto = 1207 and @M1207 <> 0 )
          select @Resultado = @M1207 + @Resultado
        if ( @Concepto = 1208 and @M1208 <> 0 )
          select @Resultado = @M1208 + @Resultado
        if ( @Concepto = 1209 and @M1209 <> 0 )
          select @Resultado = @M1209 + @Resultado
        if ( @Concepto = 1210 and @M1210 <> 0 )
          select @Resultado = @M1210 + @Resultado
        if ( @Concepto = 1211 and @M1211 <> 0 )
          select @Resultado = @M1211 + @Resultado
        if ( @Concepto = 1212 and @M1212 <> 0 )
          select @Resultado = @M1212 + @Resultado
        if ( @Concepto = 1213 and @M1213 <> 0 )
          select @Resultado = @M1213 + @Resultado
        if ( @Concepto = 1214 and @M1214 <> 0 )
          select @Resultado = @M1214 + @Resultado
        if ( @Concepto = 1215 and @M1215 <> 0 )
          select @Resultado = @M1215 + @Resultado
		if ( @Concepto = 1216 and @M1216 <> 0 )
          select @Resultado = @M1216 + @Resultado
		

			Fetch next from TemporalCursorNom
     	  into  
			      @M1000,
            @M1001,
            @M1002,
            @M1003,
            @M1005,
            @M1006,
            @M1007,
            @M1008,
            @M1009,
            @M1010,
	        @M1011,
            @M1012,
            @M1100,
            @M1101,
            @M1102,
            @M1103,
            @M1104,
            @M1105,
            @M1106,
            @M1107,
            @M1108,
            @M1109,
            @M1110,
            @M1111,
            @M1112,
            @M1113,
            @M1114,
            @M1115,
            @M1116,
            @M1117,
            @M1118,
            @M1119,
            @M1120,
            @M1121,
            @M1122,
            @M1123,
            @M1200,         
            @M1201,           
            @M1202,          
            @M1203,
            @M1204,
            @M1205,
            @M1206,
            @M1207,
            @M1208,
            @M1209,
            @M1210,
            @M1211,
            @M1212,
            @M1213,
            @M1214,
            @M1215,
		    @TipoNom,
            @M1216
 		
	end
  CLOSE TemporalCursorNom
  DEALLOCATE TemporalCursorNom
end
else
begin
  	select @M1004 = sum( M.MO_PERCEPC )
     from MOVIMIEN M
	left outer join CONCEPTO C on C.CO_NUMERO=M.CO_NUMERO 
	left outer join NOMINA on NOMINA.PE_YEAR = M.PE_YEAR AND NOMINA.PE_TIPO = M.PE_TIPO AND NOMINA.PE_NUMERO = M.PE_NUMERO AND NOMINA.CB_CODIGO = M.CB_CODIGO
	left outer join PERIODO on PERIODO.PE_YEAR = NOMINA.PE_YEAR AND PERIODO.PE_TIPO = NOMINA.PE_TIPO AND PERIODO.PE_NUMERO = NOMINA.PE_NUMERO
	left outer join RPATRON on RPATRON.TB_CODIGO = NOMINA.CB_PATRON
	left outer join RSOCIAL on RSOCIAL.RS_CODIGO = RPATRON.RS_CODIGO
     left outer join COLABORA on COLABORA.CB_CODIGO = M.CB_CODIGO 
     where
		( M.PE_YEAR = @Anio ) AND
		( M.CB_CODIGO = @Empleado ) AND  		
		( M.MO_ACTIVO = 'S' ) AND
		( PERIODO.PE_STATUS = 6 ) AND
		( PERIODO.PE_MES = @Mes ) AND
		( ( @TipoNomina = -1 ) or ( NOMINA.PE_TIPO = @TipoNomina ) ) and	
		( ( @RazonSocial = '' ) or ( RSOCIAL.RS_CODIGO = @RazonSocial ) ) and
          ( ( @ValidaReingreso <> 'S' ) OR (  COLABORA.CB_FEC_ING <= PERIODO.PE_FEC_FIN  ) ) and
          ( C.CO_TIPO = 1 ) and
		( C.CO_A_PTU = 'S' ) 


     if ( @M1004 <> 0 )
        select @Resultado = @M1004
end

  RETURN @Resultado

END
GO

/* PROCEDURE SP_WFGETTIPONOMINA: CR Integracion de WF  */
/* Patch 425 # Seq: 350 */
CREATE PROCEDURE SP_WFGETTIPONOMINA
@CB_CODIGO INT,
@VERSION   VARCHAR(10)
AS
BEGIN

	IF ( @VERSION = '2006' )
	BEGIN
		SELECT 'TIPO_NOMINA' = TURNO.TU_NOMINA
		FROM COLABORA
		LEFT OUTER JOIN TURNO
		ON ( TURNO.TU_CODIGO = ( SELECT DBO.SP_GET_TURNO( GETDATE(), COLABORA.CB_CODIGO ) ) )
		WHERE (  COLABORA.CB_CODIGO = @CB_CODIGO )
	END

	IF ( @VERSION = '2007')
	BEGIN

		SELECT 'TIPO_NOMINA' = CB_NOMINA
		FROM COLABORA
		WHERE CB_CODIGO = @CB_CODIGO

	END
END
GO
/* PROCEDURE GETVACAPROPORCIONAL: CR Integracion de WF  */
/* Patch 425 # Seq:  351 */
CREATE FUNCTION DBO.GETVACAPROPORCIONAL
(
	@CB_CODIGO INT,
	@FECHAREGRESO DATETIME
)
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE
	@DANTIG DATETIME,
	@DULTIMOCIERRE DATETIME,
	@STABLA VARCHAR(1),
	@FECHAREF DATETIME,
	@DANIV DATETIME,
	@IANTIG INT,
	@RESULT FLOAT,
	@RANTIGULTIMO FLOAT,
	@RANTIGPROXIMO FLOAT,
	@LANIVULTIMO CHAR(1),
	@LANIVPROXIMO CHAR(1),
	@GLOBAL201 VARCHAR(8),
	@GLOBAL104 VARCHAR(8)


		--SE OBTIENE EL GLOBAL DE ASISTENCIA
		SELECT @GLOBAL201 = ISNULL( GL_FORMULA, 'N' )
		FROM GLOBAL
		WHERE GL_CODIGO = 201

		--IF ( @GLOBAL201 = 'N' )
		--BEGIN

			--SE OBTIENE EL GLOBAL DE LA TABLA
			SELECT @GLOBAL104 = ISNULL( GL_FORMULA, '' )
			FROM GLOBAL
			WHERE GL_CODIGO=104

			/* SE OBTIENE LA INFORMACI�N DEL EMPLEADO */
			SELECT  @DANTIG=C.CB_FEC_ING,
					@FECHAREF=C.CB_FEC_BAJ,
					@DULTIMOCIERRE=C.CB_DER_FEC,
					@STABLA = C.CB_TABLASS
			FROM COLABORA C
			WHERE( C.CB_CODIGO = @CB_CODIGO )

			/* SE OBTIENE LA FECHA DE ANIVERSARIO DEL EMPLEADO */
			SELECT @LANIVPROXIMO = DBO.ESANIVERSARIO( @DANTIG , @FECHAREGRESO )
			SELECT @RANTIGPROXIMO = DBO.YEARSCOMPLETOS( @DANTIG , @FECHAREGRESO )

			IF(ISNULL( @STABLA, '' ) <> '' )
			BEGIN
				SELECT @RESULT = DBO.GETVACAPROP( @STABLA, ABS(ROUND(@RANTIGPROXIMO,0)) )
				SELECT @RESULT = @RESULT * 0.01
			END
			ELSE
			BEGIN
					IF ( ISNULL( @GLOBAL104, '' ) <> '' )
					BEGIN
						SELECT @RESULT = DBO.GETVACAPROP( @GLOBAL104, ABS(ROUND(@RANTIGPROXIMO,0)) )
						SELECT @RESULT = @RESULT * 0.01
					END
					ELSE
					BEGIN
						SELECT @RESULT = 0
					END
			END
		--END
		--ELSE
		--BEGIN
			--SELECT @RESULT = 0
		--END
	RETURN  @RESULT
END
GO
/* FUNCTION DBO.CACHEPRESTACIONESPROP: CR Integracion de WF  */
/* Patch 425 # Seq:  352 */
CREATE FUNCTION DBO.CACHEPRESTACIONESPROP
(   @STABLAS VARCHAR(30) )
RETURNS @FCACHETABLA TABLE (
    ANIO NUMERIC(15,2),
    DIAS NUMERIC(15,2) )
AS
BEGIN

	DECLARE
	@K_MAX_YEARS INT,
	@I INT,
	@J INT,
	@DIASAUX FLOAT,
	@DIASANT FLOAT,
	@DIAS FLOAT

	SELECT @K_MAX_YEARS = 99, @I = 0 ,@DIAS =0, @DIASANT=0, @DIASAUX=0 , @J = 1

	/* SE LLENA LA TABLA CON LOS DIAS DISPONIBLES POR A�O PARA UN EMPLEADO*/
	WHILE( @I < @K_MAX_YEARS)
	BEGIN
		SELECT @DIASAUX = ISNULL(PT_PRIMAVA, 0 )
						FROM   PRESTACI
						WHERE  TB_CODIGO = @STABLAS
							AND PT_YEAR = @I
						ORDER BY PT_YEAR

		IF( @DIASAUX = 0 )
		BEGIN

			INSERT INTO @FCACHETABLA VALUES( @I , 0 )

		END
		ELSE
		BEGIN
			SELECT @DIAS = @DIASAUX

			INSERT INTO @FCACHETABLA VALUES( @I , @DIAS )
			--SELECT @DIASANT=@DIASAUX

			UPDATE @FCACHETABLA
			SET DIAS= @DIAS
			WHERE ANIO >0 AND ANIO<@I AND DIAS=0

		END

		SELECT @I= @I+1,@DIASAUX =0
	END

	/* SE ACTUALIZAN LOS REGISTROS QUE TIENEN 0 DIAS */
	UPDATE @FCACHETABLA
	SET DIAS= @DIAS
	WHERE ANIO > 0
	AND ANIO < @K_MAX_YEARS
	AND DIAS = 0

	RETURN
END
GO
/*FUNCTION DBO.GETVACAPROP: CR Integracion de WF  */
/* Patch 425 # Seq:  353 */
CREATE FUNCTION DBO.GETVACAPROP
(   @STABLAS VARCHAR(30), @IYEAR INT )
RETURNS NUMERIC(15,2)
AS
BEGIN

	DECLARE
	@DIAS NUMERIC(15,2),
	@K_MAX_YEARS INT

	SELECT  @K_MAX_YEARS =99

	IF ( @IYEAR < 1 )
		SELECT @IYEAR = 1;
	IF ( @IYEAR > @K_MAX_YEARS )
		SELECT @IYEAR = @K_MAX_YEARS

	/* SE OBTIENE LA INFORMACI�N DE LA TABLA */
	SELECT @DIAS = DIAS FROM DBO.CACHEPRESTACIONESPROP(@STABLAS)WHERE ANIO=@IYEAR

	RETURN @DIAS
END

GO

/*FUNCTION DBO.YEARSCOMPLETOS: CR Integracion de WF  */
/* Patch 425 # Seq:  354 */
CREATE FUNCTION DBO.YEARSCOMPLETOS
(   @DANTIG DATETIME,
	@DREFERENCIA DATETIME )
RETURNS NUMERIC(15,2)
AS
BEGIN
	DECLARE
	@IDAYINI INT, @IMONTHINI INT, @IYEARINI INT,
	@IDAYFIN INT, @IMONTHFIN INT, @IYEARFIN INT,
	@RANTIGULTIMO NUMERIC(15,2)

	IF ( @DANTIG < @DREFERENCIA )
	BEGIN

		/* ESTA FUNCION DESMENUSA LA FECHA Y OBTIENE EL A�O, MES Y DIA*/
		SELECT 	@IYEARINI 	= YEAR(@DANTIG),
				@IMONTHINI 	= MONTH(@DANTIG),
				@IDAYINI 	= DAY(@DANTIG),
				@IYEARFIN 	= YEAR(@DREFERENCIA),
				@IMONTHFIN 	= MONTH(@DREFERENCIA),
				@IDAYFIN 	= DAY(@DREFERENCIA)


		IF ( @IMONTHINI = @IMONTHFIN ) AND ( @IDAYINI = @IDAYFIN )    -- (ES ANIVERSARIO)RESTA DE A�OS ENTEROS
			SELECT @RANTIGULTIMO= ABS(@IYEARFIN - @IYEARINI)
		ELSE  -- HAY UNA PARTE PROPORCIONAL //
			SELECT  @RANTIGULTIMO= (DATEDIFF(DAY, @DANTIG ,@DREFERENCIA ) / 365.25)
	END
	ELSE
	BEGIN
		SELECT  @RANTIGULTIMO = 0
	END

	RETURN @RANTIGULTIMO
END

GO

/*FUNCTION DBO.ESANIVERSARIO: CR Integracion de WF  */
/* Patch 425 # Seq:  355 */
CREATE  FUNCTION DBO.ESANIVERSARIO
(   @DANTIG DATETIME, @DREFERENCIA DATETIME )
RETURNS CHAR(1)
AS
BEGIN
	DECLARE
	@IDAYINI INT, @IMONTHINI INT, @IYEARINI INT,
	@IDAYFIN INT, @IMONTHFIN INT, @IYEARFIN INT,
	@LANIVERSARIO INT, @ESANIV CHAR(1)

	SELECT @LANIVERSARIO= 0

	IF ( @DANTIG < @DREFERENCIA )
	BEGIN

		/* ESTA FUNCION DESMENUSA LA FECHA Y OBTIENE EL A�O, MES Y DIA*/
		SELECT 	@IYEARINI 	= YEAR(@DANTIG),
				@IMONTHINI 	= MONTH(@DANTIG),
				@IDAYINI 	= DAY(@DANTIG),
				@IYEARFIN 	= YEAR(@DREFERENCIA),
				@IMONTHFIN 	= MONTH(@DREFERENCIA),
				@IDAYFIN 	= DAY(@DREFERENCIA)

		-- SI COINCIDE CON EN EL ANIVERSARIO //
		IF( @IMONTHINI = @IMONTHFIN ) AND ( @IDAYINI = @IDAYFIN )
		BEGIN
			SELECT @ESANIV= 'S'
		END
		ELSE
		BEGIN
			SELECT @ESANIV= 'N'
		END

	END
	ELSE
	BEGIN
			SELECT @ESANIV= 'S'
	END

	RETURN @ESANIV
END

GO

/*DBO.SP_WFGETEMPLEADOVACACIONES : CR Integracion de WF  */
/* Patch 425 # Seq:  356 */
CREATE PROCEDURE DBO.SP_WFGETEMPLEADOVACACIONES(@EMPLEADO NUMEROEMPLEADO , @CONFIDEN CODIGO, @USUARIO USUARIO, @EMPLEADO_L NUMEROEMPLEADO)
AS BEGIN
	DECLARE @FECHA DESCRIPCION
	SET @FECHA = LTRIM(RTRIM(STR( MONTH(GETDATE()) )))+'/'+ LTRIM(RTRIM(STR( DAY(GETDATE()) )))+'/'+ LTRIM(RTRIM(STR( YEAR(GETDATE()))) )

	SELECT  CB_CODIGO, CB_APE_PAT+' '+CB_APE_MAT+', '+CB_NOMBRES AS PRETTYNAME, C.CB_ACTIVO, C.CB_FEC_ING, C.CB_FEC_BAJ,
		C.CB_DER_FEC, C.CB_DER_PAG, C.CB_DER_GOZ, C.CB_V_GOZO, C.CB_V_PAGO, C.CB_FEC_ANT, C.CB_FEC_VAC,
		CB_TOT_PAG = CB_DER_PAG - CB_V_PAGO,
		CB_TOT_GOZ = CB_DER_GOZ - CB_V_GOZO, 0.0 AS CB_SUB_PAG, 0.0 AS CB_SUB_GOZ, C.CB_ACTIVO,
		0.0 AS CB_PRO_PAG, 0.0 AS CB_PRO_GOZ, 0.0 AS CB_PEN_PAG, 0.0 AS CB_PEN_GOZ, C.CB_TABLASS,
		PORC_PRIMA = DBO.GETVACAPROPORCIONAL(C.CB_CODIGO, GETDATE()), TU_VACA_HA = ISNULL(T.TU_VACA_HA,0)
		FROM COLABORA C
		LEFT OUTER JOIN PUESTO P ON( P.PU_CODIGO = CB_PUESTO )
		LEFT OUTER JOIN TURNO T ON ( T.TU_CODIGO = CB_TURNO )
		LEFT OUTER JOIN CLASIFI CLAS ON (CLAS.TB_CODIGO = C.CB_CLASIFI)
		LEFT OUTER JOIN CONTRATO CNT ON (CNT.TB_CODIGO = C.CB_CONTRAT)
		LEFT OUTER JOIN RPATRON RPAT ON (RPAT.TB_CODIGO = C.CB_PATRON)
		LEFT OUTER JOIN NIVEL1 N1 ON ( N1.TB_CODIGO = C.CB_NIVEL1 )
		LEFT OUTER JOIN NIVEL2 N2 ON ( N2.TB_CODIGO = C.CB_NIVEL2 )
		LEFT OUTER JOIN NIVEL3 N3 ON ( N3.TB_CODIGO = C.CB_NIVEL3 )
		LEFT OUTER JOIN NIVEL4 N4 ON ( N4.TB_CODIGO = C.CB_NIVEL4 )
		LEFT OUTER JOIN NIVEL5 N5 ON ( N5.TB_CODIGO = C.CB_NIVEL5 )
		LEFT OUTER JOIN NIVEL6 N6 ON ( N6.TB_CODIGO = C.CB_NIVEL6 )
		LEFT OUTER JOIN NIVEL7 N7 ON ( N7.TB_CODIGO = C.CB_NIVEL7 )
		LEFT OUTER JOIN NIVEL8 N8 ON ( N8.TB_CODIGO = C.CB_NIVEL8 )
		LEFT OUTER JOIN NIVEL9 N9 ON ( N9.TB_CODIGO = C.CB_NIVEL9 )
		LEFT OUTER JOIN EXTRA1 X1 ON ( X1.TB_CODIGO = C.CB_G_TAB_1 )
		LEFT OUTER JOIN EXTRA2 X2 ON ( X2.TB_CODIGO = C.CB_G_TAB_2 )
		LEFT OUTER JOIN EXTRA3 X3 ON ( X3.TB_CODIGO = C.CB_G_TAB_3 )
		LEFT OUTER JOIN EXTRA4 X4 ON ( X4.TB_CODIGO = C.CB_G_TAB_4 )
		LEFT OUTER JOIN ENTIDAD E ON (E.TB_CODIGO = C.CB_ENT_NAC)
		WHERE( (@CONFIDEN = '' OR CB_NIVEL0=@CONFIDEN)
		AND (C.CB_CODIGO=@EMPLEADO AND (SELECT COUNT(*) FROM SP_LISTA_EMPLEADOS( @FECHA, @USUARIO ) LISTA
										LEFT OUTER JOIN COLABORA C
										ON LISTA.CB_CODIGO = C.CB_CODIGO
										WHERE LISTA.CB_CODIGO = @EMPLEADO
										AND (@CONFIDEN =''
										OR CB_NIVEL0 = @CONFIDEN)) > 0) )
		OR ( CB_CODIGO = @EMPLEADO_L AND CB_CODIGO = @EMPLEADO)
END

GO
/*DBO.SP_WFGETPERIODODATOS : CR Integracion de WF  */
/* Patch 425 # Seq:  357 */
CREATE PROCEDURE DBO.SP_WFGETPERIODODATOS(@YEAR ANIO, @TPO_NOMINA NOMINATIPO, @NUM_NOMINA NOMINANUMERO)
AS
BEGIN
	SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ,
	PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC,
	PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM,
	PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG,
	US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
	PE_PERIODO_TEXTO = 'DE ' + (CASE
								WHEN DATEPART(DD,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI) )
								ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI))
								END) + '/'
							  + (CASE
								WHEN DATEPART(MM,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI) )
								ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI))
								END)+ '/'
							  +	CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_INI)) +
						' A ' + (CASE
								WHEN DATEPART(DD,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN) )
								ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN))
								END) + '/'
							  + (CASE
								WHEN DATEPART(MM,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN) )
								ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN))
								END)+ '/'
							  +	CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_FIN))
	FROM PERIODO
	WHERE ( PE_YEAR = @YEAR )
	AND ( PE_TIPO = @TPO_NOMINA )
	AND ( PE_NUMERO = @NUM_NOMINA )
END

GO
/*SP_WFGETTIPOPERIODO : CR Integracion de WF  */
/* Patch 425 # Seq:  358 */
CREATE PROCEDURE SP_WFGETTIPOPERIODO
AS
BEGIN

	SELECT 	CODIGO = TP_TIPO, DESCRIPCION = TP_DESCRIP, TP_TIPO, TP_NOMBRE, TP_DESCRIP, TP_DIAS, TP_HORAS
			TP_DIAS_BT, TP_DIAS_7, TP_DIAS_EV
	FROM TPERIODO
	WHERE TP_DESCRIP <> ''

END

GO
/*SP_WFGETDIASGOZADOS : CR Integracion de WF  */
/* Patch 425 # Seq:  359 */
CREATE PROCEDURE SP_WFGETDIASGOZADOS ( @CB_CODIGO INT, @FECHAINI DATETIME, @FECHAFIN DATETIME )
AS
BEGIN
	SELECT CONVERT( INT ,ISNULL( SUM(VA_GOZO), 0))  AS TOTALDIAS
	FROM VACACION
	WHERE CB_CODIGO=@CB_CODIGO
	AND VA_FEC_INI BETWEEN @FECHAINI AND @FECHAFIN
	AND VA_TIPO = 1
END

GO
/* DBO.SP_WFGETPERIODOACTUALDATOS : CR Integracion de WF  */
/* Patch 425 # Seq:  360 */
CREATE PROCEDURE DBO.SP_WFGETPERIODOACTUALDATOS(@YEAR ANIO, @TPO_NOMINA NOMINATIPO )
AS
BEGIN
		DECLARE
		@MAX_PERIODO INT,
		@STATUS INT
		/*SET @STATUS = 6;  OP: 03-07-2008 LA EXCLUSI�N SER�A DE N�MINAS NUEVAS */
		SET @STATUS = 0;

		 SELECT @MAX_PERIODO= (SELECT ISNULL(MAX(PE_NUMERO),1)       FROM PERIODO

			   WHERE ( PE_YEAR = @YEAR )

			   AND ( PE_TIPO = @TPO_NOMINA )

			   AND ( PE_NUMERO < 200 )

			   AND ( PE_STATUS <> @STATUS ) )/*OP: 03-07-2008 EL �LTIMO PERIODO NO ESTATUS 'N�MINA NUEVA'*/

/*
		IF NOT EXISTS ( SELECT * FROM PERIODO WHERE ( PE_YEAR = @YEAR ) AND ( PE_TIPO = @TPO_NOMINA ) AND ( PE_NUMERO =  @MAX_PERIODO) AND ( PE_STATUS >@STATUS ) )
		BEGIN
			 SET @MAX_PERIODO = 1
			 SET @STATUS = -1
		END
*/
		SELECT PE_YEAR, PE_TIPO, PE_NUMERO, PE_DESCRIP, PE_USO, PE_STATUS, PE_INC_BAJ,
		PE_SOLO_EX, PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, PE_MES, PE_DIAS, PE_DIAS_AC,
		PE_DIA_MES, PE_POS_MES, PE_PER_MES, PE_PER_TOT, PE_FEC_MOD, PE_AHORRO, PE_PRESTAM,
		PE_ASI_INI, PE_ASI_FIN, PE_NUM_EMP, PE_TOT_PER, PE_TOT_NET, PE_TOT_DED, PE_LOG,
		US_CODIGO, (PE_MES - 1) AS PE_MES_A ,
		PE_PERIODO_TEXTO = 'DE ' + (CASE
									WHEN DATEPART(DD,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI) )
									ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_INI))
									END) + '/'
								 + (CASE
									WHEN DATEPART(MM,PE_FEC_INI)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI) )
									ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_INI))
									END)+ '/'
								 +            CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_INI)) +
									' A ' + (CASE
									WHEN DATEPART(DD,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN) )
									ELSE CONVERT(VARCHAR(2), DATEPART(DD,PE_FEC_FIN))
									END) + '/'
								 + (CASE
									WHEN DATEPART(MM,PE_FEC_FIN)< 10 THEN '0' + CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN) )
									ELSE CONVERT(VARCHAR(2), DATEPART(MM,PE_FEC_FIN))
									END)+ '/'
								 +            CONVERT(VARCHAR(4), DATEPART(YYYY,PE_FEC_FIN))
		FROM PERIODO
		WHERE ( PE_YEAR = @YEAR )
		AND ( PE_TIPO = @TPO_NOMINA )
		AND ( PE_NUMERO =  @MAX_PERIODO )
		AND ( PE_STATUS <> @STATUS )
END

GO

/*DBO.PUEDE_CAMBIAR_TARJETA : CR Integracion de WF  */
/* Patch 425 # Seq:  361 */
CREATE FUNCTION DBO.PUEDE_CAMBIAR_TARJETA( @FECHAINICIAL DATETIME, @FECHAFINAL DATETIME, @EMPLEADO INTEGER )
RETURNS CHAR( 100 )
AS
BEGIN
  -- DECLARACI�N DE VARIABLES
  DECLARE @MENSAJE CHAR(100);
  DECLARE @K_GLOBAL_BLOQUEO_X_STATUS CHAR(1);
  DECLARE @K_LIMITE_MODIFICAR_ASISTENCIA INTEGER;
  DECLARE @K_LIMITE_MODIFICAR_ASISTENCIA_DESC CHAR(30);
  DECLARE @ESTATUSACTUAL SMALLINT;
  DECLARE @LIMITE DATETIME;

  -- SET @MENSAJE = CHECKDERECHO( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_TARJETAS_ANTERIORES ); --
  SET @MENSAJE = 'S';

  IF( @MENSAJE = 'S' )
  BEGIN
    SET @K_GLOBAL_BLOQUEO_X_STATUS = ( SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 187 );

	IF @K_GLOBAL_BLOQUEO_X_STATUS = 'S'
	BEGIN
	  IF( @EMPLEADO = 0 )
        SET @MENSAJE = 'Sin Empleado Definido';
      ELSE
      BEGIN
        IF( @FECHAINICIAL = @FECHAFINAL )
        BEGIN
          SET @K_LIMITE_MODIFICAR_ASISTENCIA = ( SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 132 );

          IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 0 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Nueva';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 1 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Pre-N�mina Parcial';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 2 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Sin Calcular';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 3 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Calculada Parcial';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 4 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Calculada Total';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 5 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Afectada Parcial';
          ELSE IF( @K_LIMITE_MODIFICAR_ASISTENCIA = 6 )
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Afectada Total';
          ELSE
            SET @K_LIMITE_MODIFICAR_ASISTENCIA_DESC = 'Nomina no encontrada';

		  SET @MENSAJE = 'No se Puede Modificar Tarjetas de Nominas con Status Mayor a ' + @K_LIMITE_MODIFICAR_ASISTENCIA_DESC;
		  SET @ESTATUSACTUAL = DBO.SP_STATUS_TARJETA( @EMPLEADO, @FECHAINICIAL );
          IF( @ESTATUSACTUAL <= @K_LIMITE_MODIFICAR_ASISTENCIA )
          BEGIN
	        SET @MENSAJE = '';
          END;
        END
        ELSE
          SET @MENSAJE = 'Bloqueo por Estatus de N�mina No se Puede Validar por Rango de Fechas';
      END;
	END
	ELSE
	BEGIN
	  SET @MENSAJE = '';
		-- SE ACTUALIZA EL CAMPO QUE SE TOMA DE GLOBAL, EN ESTE CASO DEBE SER EL GL_FORMULA, EL CUAL VIENE EN EL
		-- FORMATO NEUTRAL DE YYYYMMDD.
	  SET @LIMITE = ( SELECT GL_FORMULA FROM GLOBAL WHERE GL_CODIGO = 182 );
      IF NOT( ( @FECHAINICIAL > @LIMITE )AND( @FECHAFINAL > @LIMITE ) )
        SET @MENSAJE = 'No se Puede Cambiar Tarjetas Anteriores A la Fecha L�mite '+ CONVERT(VARCHAR, @LIMITE,103);
	END;
  END;

  RETURN @MENSAJE;
END
GO

/*****************************************/
/************ TRIGGERS *******************/
/*****************************************/

/* TR_AUSENCIA: Excepcion para 1825 Autorizacion y Bloque de PreNomina*/
/* Patch 425 # Seq:  97*/
Create TRIGGER TR_AUSENCIA ON AUSENCIA AFTER UPDATE,INSERT,DELETE AS
BEGIN
	 SET NOCOUNT ON
	 Declare @Aprobado int;
	 Declare @AprobadoU int
	 Declare @Empleado int;
	 Declare @AnioPeriodo int;	 
	 Declare @TipoPeriodo int;
	 Declare @NumPeriodo int;
	 Declare @EmpleadoU int;
	 Declare @AnioPeriodoU int;	 
	 Declare @TipoPeriodoU int;
	 Declare @NumPeriodoU int;
	 Declare @Bloqueo VARCHAR(1);
		
	 select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289
	 
     if ( @Bloqueo = 'S' )
     begin
		 select @Empleado = CB_CODIGO from Inserted;
		 
		 select @NumPeriodo = PE_NUMERO from Inserted;
		 select @TipoPeriodo = PE_TIPO from Inserted;
		 select @AnioPeriodo = PE_YEAR from Inserted;
		 
		 if ( @Empleado is null and @NumPeriodo is null )
		 begin
		       select @Empleado = CB_CODIGO from Deleted;
			   select @NumPeriodo = PE_NUMERO from Deleted;
		       select @TipoPeriodo = PE_TIPO from Deleted;
		       select @AnioPeriodo = PE_YEAR from Deleted;	
		 end	
		ELSE
		BEGIN
			   select @EmpleadoU = CB_CODIGO from Deleted;
			   select @NumPeriodoU = PE_NUMERO from Deleted;
		       select @TipoPeriodoU = PE_TIPO from Deleted;
		       select @AnioPeriodoU = PE_YEAR from Deleted;
		END
         	 
         if ( (@NumPeriodo > 0 and @TipoPeriodo > 0 and @AnioPeriodo > 0 )OR (@NumPeriodoU > 0 and @TipoPeriodoU > 0 and @AnioPeriodoU > 0) )
	     begin
			  select @Aprobado = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @Empleado and N.PE_YEAR = @AnioPeriodo and N.PE_TIPO = @TipoPeriodo and N.PE_NUMERO = @NumPeriodo
			  select @AprobadoU = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @EmpleadoU and N.PE_YEAR = @AnioPeriodoU and N.PE_TIPO = @TipoPeriodoU and N.PE_NUMERO = @NumPeriodoU
						  
			  if ( @Aprobado > 0 or  @AprobadoU > 0 )
			  begin
					RAISERROR(' Tarjeta de Pre-N�mina Autorizada - No se pueden realizar cambios', 15, 2);
					ROLLBACK TRANSACTION;
			  end
	     end
	 end
END
GO
/* TR_CHECADAS: Excepcion para 1825 Autorizacion y Bloque de PreNomina*/
/* Patch 425 # Seq:  98*/
Create TRIGGER TR_CHECADAS ON CHECADAS AFTER UPDATE,INSERT,DELETE AS
BEGIN
	 SET NOCOUNT ON
	 Declare @Aprobado int;
	 Declare @Empleado int;
     Declare @FechaTarjeta DateTime;
	 Declare @AnioPeriodo int;
     Declare @TipoPeriodo int;
     Declare @NumPeriodo int;
	 Declare @Bloqueo CHAR(1);
	 
	 select @Bloqueo = GL_FORMULA from GLOBAL where GL_CODIGO = 289	 

	 if ( @Bloqueo = 'S' ) 
     begin 
		 select @Empleado = CB_CODIGO from Inserted;

		 select @FechaTarjeta = AU_FECHA from Inserted;
	     
		 if ( ( @Empleado is Null ) and ( @FechaTarjeta is null ) )     
	     begin
			  select @Empleado = CB_CODIGO from Deleted;
			  select @FechaTarjeta = AU_FECHA from Deleted;
		 end
		 
		 select @AnioPeriodo = PE_YEAR,@TipoPeriodo = PE_TIPO,@NumPeriodo = PE_NUMERO from AUSENCIA A where A.CB_CODIGO = @Empleado and A.AU_FECHA = @FechaTarjeta;
		 if (@NumPeriodo > 0 and @TipoPeriodo > 0 and @AnioPeriodo > 0 )
		 begin
			   select @Aprobado = NO_SUP_OK  from Nomina N where N.CB_CODIGO = @Empleado and N.PE_YEAR = @AnioPeriodo and N.PE_TIPO = @TipoPeriodo and N.PE_NUMERO = @NumPeriodo 
			   if ( @Aprobado > 0 )
			   begin
			   		RAISERROR(' Tarjeta de Pre-N�mina Autorizada - No se pueden realizar cambios', 15, 2);
					ROLLBACK TRANSACTION;
			   end
		 end    
     end
END
GO

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Nota: Se modifica al Trigger  TU_COLABORA  para ajustar el usuario del supervisor al empleado actualizado  en caso de usar enrolamiento de supervisores */
/* CR: 1800 */
/* Patch 425 # Seq:  46*/
ALTER TRIGGER TU_COLABORA ON COLABORA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  declare @NewCodigo NumeroEmpleado, @OldCodigo NumeroEmpleado;
  IF UPDATE( CB_CODIGO )
  BEGIN
	select @NewCodigo = CB_CODIGO from Inserted;
        select @OldCodigo = CB_CODIGO from Deleted;

	UPDATE NOMINA	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE LIQ_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE WORKS	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
	UPDATE CED_EMP	SET CB_CODIGO = @NewCodigo WHERE CB_CODIGO = @OldCodigo;
        if ( @OldCodigo > 0 )
        begin
             update EXPEDIEN set EXPEDIEN.CB_CODIGO = @NewCodigo
             where ( EXPEDIEN.CB_CODIGO = @OldCodigo )
             update PLAZA set PLAZA.CB_CODIGO = @NewCodigo
             where ( PLAZA.CB_CODIGO = @OldCodigo );
        end
  END
  IF UPDATE( CB_PLAZA )
  BEGIN
       declare @NewPlaza FolioGrande;
       declare @OldPLaza FolioGrande;
       select @NewPlaza = CB_PLAZA from Inserted;
       select @OldPLaza = CB_PLAZA from Deleted;
       select @NewCodigo = CB_CODIGO from Inserted;
       update PLAZA set CB_CODIGO = 0 where ( PL_FOLIO = @OldPLaza ) and ( CB_CODIGO = @NewCodigo );
       update PLAZA set CB_CODIGO = @NewCodigo where ( PL_FOLIO = @NewPlaza );
  END
  
/* CR: 1800 Inicio de lo cambios  para cambiar supervisor enrolado */
  IF UPDATE(CB_NIVEL1) OR 
     UPDATE(CB_NIVEL2) OR 
     UPDATE(CB_NIVEL3) OR 
     UPDATE(CB_NIVEL4) OR 
     UPDATE(CB_NIVEL5) OR 
     UPDATE(CB_NIVEL6) OR 
     UPDATE(CB_NIVEL7) OR 
     UPDATE(CB_NIVEL8) OR 
     UPDATE(CB_NIVEL9)
/* ACS    
     OR 
     UPDATE(CB_NIVEL10) OR 
     UPDATE(CB_NIVEL11) OR 
     UPDATE(CB_NIVEL12) OR */
   BEGIN 
   	DECLARE @NivelSupervisores Formula
	DECLARE @US_CODIGO Usuario
     		
   	select @US_CODIGO = US_CODIGO from Inserted;   	
   	SET @NivelSupervisores =  DBO.SP_GET_NIVEL_SUPERVISORES()
   	
	-- Verificar si cambio CB_NIVEL[SUPERVISOR]
     	IF  DBO.SP_TIENE_ENROLAMIENTO_SUPERVISORES() = 'S' AND @NivelSupervisores <> '' AND @US_CODIGO > 0 
     	BEGIN 
     		DECLARE @CB_NIVELSup Codigo     		     		
     		DECLARE @US_JEFE Usuario
     		
     		-- Se pone TOP 1 en caso de que venga de una actualizaci�n directa por UPDATE 
     		-- Vease el caso de un cambio de codigo de un nivel de organigrama
     		SELECT  @CB_NIVELSup = 
		CASE  @NivelSupervisores 
			WHEN '1' THEN (SELECT TOP 1 CB_NIVEL1 FROM Inserted) 
			WHEN '2' THEN (SELECT TOP 1 CB_NIVEL2 FROM Inserted) 			
			WHEN '3' THEN (SELECT TOP 1 CB_NIVEL3 FROM Inserted) 
			WHEN '4' THEN (SELECT TOP 1 CB_NIVEL4 FROM Inserted) 
			WHEN '5' THEN (SELECT TOP 1 CB_NIVEL5 FROM Inserted) 
			WHEN '6' THEN (SELECT TOP 1 CB_NIVEL6 FROM Inserted) 
			WHEN '7' THEN (SELECT TOP 1 CB_NIVEL7 FROM Inserted)
			WHEN '8' THEN (SELECT TOP 1 CB_NIVEL8 FROM Inserted)
			WHEN '9' THEN (SELECT TOP 1 CB_NIVEL9 FROM Inserted)			
/* ACS 
			WHEN '10' THEN (SELECT TOP 1 CB_NIVEL10 FROM Inserted)
			WHEN '11  THEN (SELECT TOP 1 CB_NIVEL11 FROM Inserted)
			WHEN '12' THEN (SELECT TOP 1 CB_NIVEL12 FROM Inserted)	
*/
		END
     		
     		IF ( @CB_NIVELSup <> '' )
     		BEGIN 
	     		SET @US_JEFE = DBO.SP_GET_USUARIO_SUPERVISOR(@CB_NIVELSup, @NivelSupervisores)
			EXEC SP_AJUSTA_USUARIO_SUPERVISOR  @US_CODIGO, @US_JEFE
     		END
   		
     	END 
   END	
/* CR: 1800 Terminan los cambios  para cambiar supervisor enrolado */
END
/* Termina: Trigger para ajustar el usuario del supervisor  */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
GO 

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza:  Modificaci�n al Trigger  TU_NIVEL1 al TU_NIVEL9 para ajustar el usuario del supervisor a los empleados subordinados del nivel modificiado  */
/* actualizado  en caso de usar enrolamiento de supervisores, se verifica que sea el nivel de supervisi�n */
/* CR: 1800 */
/* Patch 425 # Seq:  1*/
ALTER  TRIGGER TU_NIVEL1 ON NIVEL1 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL1 = @NewCodigo WHERE PU_NIVEL1 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
        UPDATE PLAZA    SET PL_NIVEL1 = @NewCodigo WHERE PL_NIVEL1 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
  END
  
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)

       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '1', @TB_CODIGO, @NewUsuario
       END
  END 
END

GO 
/* Patch 425 # Seq:  2*/
ALTER  TRIGGER TU_NIVEL2 ON NIVEL2 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL2 = @NewCodigo WHERE PU_NIVEL2 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL2 = @NewCodigo WHERE PL_NIVEL2 = @OldCodigo;
	UPDATE AREA		SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
  END
  
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)
              
       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '2', @TB_CODIGO, @NewUsuario
       END
  END 
  
END

GO 
/* Patch 425 # Seq:  3*/
ALTER  TRIGGER TU_NIVEL3 ON NIVEL3 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
  select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL3 = @NewCodigo WHERE PU_NIVEL3 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
  UPDATE PLAZA    SET PL_NIVEL3 = @NewCodigo WHERE PL_NIVEL3 = @OldCodigo;
  UPDATE AREA		SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
  END
  
    
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       
       SET @OldUsuario = COALESCE( @OldUsuario, 0)
       
       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '3', @TB_CODIGO, @NewUsuario
       END
  END 
END

GO
/* Patch 425 # Seq: 4 */
ALTER  TRIGGER TU_NIVEL4 ON NIVEL4 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL4 = @NewCodigo WHERE PU_NIVEL4 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
        UPDATE PLAZA    SET PL_NIVEL4 = @NewCodigo WHERE PL_NIVEL4 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
  END
  
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)
       
       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '4', @TB_CODIGO, @NewUsuario
       END
  END 
END


GO 
/* Patch 425 # Seq:  5*/
ALTER  TRIGGER TU_NIVEL5 ON NIVEL5 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL5 = @NewCodigo WHERE PU_NIVEL5 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
        UPDATE PLAZA    SET PL_NIVEL5 = @NewCodigo WHERE PL_NIVEL5 = @OldCodigo;
 	UPDATE AREA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
  END
    
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)
              
       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '5', @TB_CODIGO, @NewUsuario
       END
  END 
END

GO
/* Patch 425 # Seq:  6*/
ALTER  TRIGGER TU_NIVEL6 ON NIVEL6 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL6 = @NewCodigo WHERE PU_NIVEL6 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
        UPDATE PLAZA    SET PL_NIVEL6 = @NewCodigo WHERE PL_NIVEL6 = @OldCodigo;
 	UPDATE AREA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
  END
  
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)
              
       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '6', @TB_CODIGO, @NewUsuario
       END
  END 
END

GO
/* Patch 425 # Seq:  7*/
ALTER  TRIGGER TU_NIVEL7 ON NIVEL7 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL7 = @NewCodigo WHERE PU_NIVEL7 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
        UPDATE PLAZA    SET PL_NIVEL7 = @NewCodigo WHERE PL_NIVEL7 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
  END
      
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)
              
       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '7', @TB_CODIGO, @NewUsuario
       END
  END 
END

GO
/* Patch 425 # Seq: 8 */
ALTER  TRIGGER TU_NIVEL8 ON NIVEL8 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL8 = @NewCodigo WHERE PU_NIVEL8 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
        UPDATE PLAZA    SET PL_NIVEL8 = @NewCodigo WHERE PL_NIVEL8 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
  END
         
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)
              
       IF ( @NewUsuario <> @OldUsuario )
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '8', @TB_CODIGO, @NewUsuario
       END
  END 
END

GO
/* Patch 425 # Seq: 9 */
ALTER  TRIGGER TU_NIVEL9 ON NIVEL9 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL9 = @NewCodigo WHERE PU_NIVEL9 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
        UPDATE PLAZA    SET PL_NIVEL9 = @NewCodigo WHERE PL_NIVEL9 = @OldCodigo;
	UPDATE AREA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE OPERA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
  END
  
  IF UPDATE( US_CODIGO ) 
  BEGIN 
       declare @NewUsuario Usuario;
       declare @OldUsuario Usuario;
       declare @TB_CODIGO  Codigo;

       select @NewUsuario = US_CODIGO, @TB_CODIGO = TB_CODIGO from Inserted;
       select @OldUsuario = US_CODIGO from Deleted;
       SET @OldUsuario = COALESCE( @OldUsuario, 0)       
       
       IF ( @NewUsuario <> @OldUsuario ) 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '9', @TB_CODIGO, @NewUsuario
       END
  END 
END
/* Termina:  Actualizacion a Triggers  TU_NIVEL1... TU_NIVEL9 para ajustar el usuario del supervisor de los empleados enrolados al nivel modificado  */
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */

GO 

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza:  Triggers de Insercion  para las tablas NIVEL1..NIVEL9 para ajustar el usuario del supervisor a los empleados subordinados al nuevo nivel   */
/* actualizado  en caso de usar enrolamiento de supervisores, se verifica que sea el nivel de supervisi�n */
/* CR: 1800 */
/* Patch 425 # Seq:  99*/
CREATE  TRIGGER TI_NIVEL1 ON NIVEL1 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '1', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq:  100*/
CREATE  TRIGGER TI_NIVEL2 ON NIVEL2 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '2', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq: 101 */
CREATE  TRIGGER TI_NIVEL3 ON NIVEL3 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '3', @TB_CODIGO, @US_CODIGO
       END
END	    	
GO 

/* Patch 425 # Seq:  102*/
CREATE  TRIGGER TI_NIVEL4 ON NIVEL4 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '4', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq:  103*/
CREATE  TRIGGER TI_NIVEL5 ON NIVEL5 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '5', @TB_CODIGO, @US_CODIGO
       END
END	    	


GO 

/* Patch 425 # Seq:  104*/
CREATE  TRIGGER TI_NIVEL6 ON NIVEL6 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '6', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq:  105*/
CREATE  TRIGGER TI_NIVEL7 ON NIVEL7 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '7', @TB_CODIGO, @US_CODIGO
       END
END	    	
GO 

/* Patch 425 # Seq:  106*/
CREATE  TRIGGER TI_NIVEL8 ON NIVEL8 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '8', @TB_CODIGO, @US_CODIGO
       END
END	    	
GO 

/* Patch 425 # Seq: 107 */
CREATE  TRIGGER TI_NIVEL9 ON NIVEL9 AFTER INSERT AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = US_CODIGO, @TB_CODIGO = TB_CODIGO FROM INSERTED;
       SET @US_CODIGO = COALESCE( @US_CODIGO, 0)
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '9', @TB_CODIGO, @US_CODIGO
       END
END	    	

/* Termina: Nuevo Trigger de Inserci�n  para ajustar el usuario del supervisor a los empleados subordinados al nuevo nivel  */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */

GO 

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
/* Comienza:  Triggers de Borrado  para las tablas NIVEL1..NIVEL9 para ajustar el usuario del supervisor a los empleados subordinados al nuevo nivel   */
/* actualizado  en caso de usar enrolamiento de supervisores, se verifica que sea el nivel de supervisi�n, los US_JEFE seran 0 */
/* CR: 1800 */
/* Patch 425 # Seq: 108 */
CREATE  TRIGGER TD_NIVEL1 ON NIVEL1 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '1', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq: 109 */
CREATE  TRIGGER TD_NIVEL2 ON NIVEL2 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '2', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq: 110 */
CREATE  TRIGGER TD_NIVEL3 ON NIVEL3 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '3', @TB_CODIGO, @US_CODIGO
       END
END	    	
GO 

/* Patch 425 # Seq: 111 */
CREATE  TRIGGER TD_NIVEL4 ON NIVEL4 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '4', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq: 112 */
CREATE  TRIGGER TD_NIVEL5 ON NIVEL5 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '5', @TB_CODIGO, @US_CODIGO
       END
END	    	


GO 

/* Patch 425 # Seq: 113 */
CREATE  TRIGGER TD_NIVEL6 ON NIVEL6 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '6', @TB_CODIGO, @US_CODIGO
       END
END	    	

GO 

/* Patch 425 # Seq:  114 */
CREATE  TRIGGER TD_NIVEL7 ON NIVEL7 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '7', @TB_CODIGO, @US_CODIGO
       END
END	    	
GO 

/* Patch 425 # Seq: 115 */
CREATE  TRIGGER TD_NIVEL8 ON NIVEL8 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '8', @TB_CODIGO, @US_CODIGO
       END
END	    	
GO 

/* Patch 425 # Seq: 116 */
CREATE  TRIGGER TD_NIVEL9 ON NIVEL9 AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
       DECLARE @US_CODIGO USUARIO;
       DECLARE @TB_CODIGO  CODIGO;

       SELECT @US_CODIGO = 0 , @TB_CODIGO = TB_CODIGO FROM DELETED;
       
       SET @TB_CODIGO = COALESCE( @TB_CODIGO, '')
       
       IF (@TB_CODIGO <> '') 
       BEGIN 
       	EXEC SP_AJUSTA_SUPERVISOR_ENROLADOS '9', @TB_CODIGO, @US_CODIGO
       END
END	    	
/* Termina :  Triggers de Borrado  para las tablas NIVEL1..NIVEL9 para ajustar el usuario del supervisor a los empleados subordinados al nuevo nivel... (US_JEFE=0) */                
/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */
GO 
/* Patch 425 # Seq: 95 */

CREATE TRIGGER TI_KARCURSO ON KARCURSO AFTER INSERT AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;

     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );

	 /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_ING from colabora where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);

	 /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ;
		En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end
     else
     begin
         set @FechaProgram =  ( select case when @FecTomada is null then @FechaAntig
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' and ( not( @NewKcRevisio = '' ) ) and ( (select Count(CH_REVISIO) FROM cur_rev cr where cr.CH_REVISIO = @NewKcRevisio ) > 0 ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ) )			    				
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
     end

     if (@FechaProgram is null)
     begin
          set @FechaProgram = @FechaAntig;
     end
     /* Se actualiza la fecha de programacion para el curso en cuestion Este ya no se puede reprogramar */ 
	update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
END
GO
/* Patch 425 # Seq: 96 */
CREATE TRIGGER TU_KARCURSO ON KARCURSO AFTER UPDATE AS
BEGIN
SET NOCOUNT ON;
DECLARE  @FechaProgram Fecha;
declare @ReprogDias Integer;
declare @DiasAntig Integer;
declare @ReprogVersion char(1);
declare @cu_revisio char(10);
declare @FechaAntig Fecha;
declare @FecTomada Fecha;
declare @NewCbCodigo Integer;
declare @NewKcFecTom Fecha;
declare @NewCuCodigo Codigo;
declare @NewKcRevisio Codigo;
declare @FechaRevision Fecha;

	 select @NewCbCodigo = CB_CODIGO,@NewKcFecTom = KC_FEC_TOM ,@NewCuCodigo = CU_CODIGO,@NewKcRevisio = KC_REVISIO from Inserted;
 
     /* Obtener los datos de la matriz de entrenamiento */
	 select @ReprogDias = E.EN_RE_DIAS,@DiasAntig = E.EN_DIAS,@ReprogVersion = E.EN_REPROG,@Cu_Revisio = C.CU_REVISIO FROM ENTRENA E join CURSO C on E.CU_CODIGO = C.CU_CODIGO where E.CU_CODIGO = @NewCuCodigo and E.PU_CODIGO = (select CB_PUESTO from colabora where CB_CODIGO = @NewCbCodigo );
     
	 /* Obtener la fecha de ingreso del empleado*/
     select @FechaAntig = CB_FEC_ING from colabora where CB_CODIGO = @NewCbCodigo;

	 /* Obtener la fecha del ultimo curso tomado en cuestion */
     Select @FecTomada = max(kc_fec_tom) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 
	 /* Obtener la ultima fecha de Programacion para el curso */
     Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom < @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
	 /* Si aun no se ha tomado el curso , se toma la fecha de antiguedad mas los dias de la matriz ;
		En caso contrario se investiga cual era la fecha de programaci�n para ese curso */
     select @FechaRevision = max(CH_FECHA) from dbo.CUR_REV where ( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @NewKcRevisio ) ); 
     if (@FechaProgram is null)
     begin
          set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
     end;
    
     IF Update(CU_CODIGO)
	 begin
		  set @FechaProgram =  ( select case when @FecTomada is null then @FechaAntig
		        when ( @ReprogDias > 0 AND @ReprogVersion = 'N'  )THEN (  DATEADD( DAY, @ReprogDias , @FecTomada ))
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ( @NewKcRevisio = @cu_revisio ) and (  not (@NewKcRevisio = '')  ) )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) ) 
				when ( ( @ReprogDias > 0 AND @ReprogVersion = 'S' ) and ((( @NewKcRevisio = @cu_revisio ) and ( @NewKcRevisio = '' ))or ( ( @NewKcRevisio <> @cu_revisio ) ) ) ) then ( DATEADD( DAY, @ReprogDias , @FecTomada ) )              
				when ( @ReprogDias = 0 AND @ReprogVersion = 'N' )then ( DATEADD( DAY, @DiasAntig , @FechaAntig ))			    
				when ( @ReprogDias = 0 AND @ReprogVersion = 'S' )then ( select max(CH_FECHA) from dbo.CUR_REV where( ( CUR_REV.CU_CODIGO = @NewCuCodigo ) AND ( CUR_REV.CH_REVISIO = @cu_revisio ) ) )			    				
				else ( DATEADD( DAY, @DiasAntig , @FechaAntig ) )End );
		
		  update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end;
	 if ( Update ( KC_REVISIO ) and @ReprogVersion = 'S'  )
     begin	
            If( (@FechaRevision is null) or (@FechaRevision = '12/30/1899') )
			begin 	
			       if (@ReprogDias > 0 )
				   begin
						Select @FechaProgram = max(KC_FECPROG) from KARCURSO where (kc_fec_tom = @NewKcFecTom and CU_CODIGO = @NewCuCodigo and CB_CODIGO = @NewCbCodigo);
				   end
				   else
						set @FechaProgram = DATEADD( DAY, @DiasAntig , @FechaAntig );
					
			end			
			else
			begin				
				set @FechaProgram = @FechaRevision;				
			end;
			
			update KARCURSO set KC_FECPROG = @FechaProgram WHERE karcurso.cb_codigo = @NewCbCodigo and karcurso.kc_fec_tom = @NewKcFecTom and karcurso.cu_codigo = @NewCuCodigo;
	 end        
END
GO

