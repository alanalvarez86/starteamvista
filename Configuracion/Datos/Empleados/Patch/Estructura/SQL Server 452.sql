/****************************************/
/****************************************/
/****** Patch para DATOS ****************/
/****************************************/
/****************************************/

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_AcumuladosRazonSocial_SumarAnual') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_AcumuladosRazonSocial_SumarAnual
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_SumarAnual( 	
					@AC_YEAR Anio ,
					@CB_CODIGO NumeroEmpleado, 
					@ListaConceptos Formula = '' 
					)
AS
BEGIN
	set nocount on 
	
	-- Suma de ACUMULA_RS
	update ACUMULA_RS set AC_ANUAL = (AC_MES_01 +
                                        AC_MES_02 +
                                        AC_MES_03 +
                                        AC_MES_04 +
                                        AC_MES_05 +
                                        AC_MES_06 +
                                        AC_MES_07 +
                                        AC_MES_08 +
                                        AC_MES_09 +
                                        AC_MES_10 +
                                        AC_MES_11 +
                                        AC_MES_12 +
                                        AC_MES_13)
    where ( AC_YEAR = @AC_YEAR  ) and  ( CB_CODIGO = @CB_CODIGO  ) and ( @ListaConceptos = '' OR CO_NUMERO in ( select  items from dbo.FN_SPLIT( @ListaConceptos, ',' ) ) ) 
	-- Recalculo de Totales en ACUMULA
	update ACUMULA 
	set 
	     ACUMULA.AC_MES_01  = AR.AC_MES_01,
		  ACUMULA.AC_MES_02  = AR.AC_MES_02,
		  ACUMULA.AC_MES_03  = AR.AC_MES_03, 
		  ACUMULA.AC_MES_04  = AR.AC_MES_04,
		  ACUMULA.AC_MES_05  = AR.AC_MES_05,
		  ACUMULA.AC_MES_06  = AR.AC_MES_06,
		  ACUMULA.AC_MES_07  = AR.AC_MES_07,
		  ACUMULA.AC_MES_08  = AR.AC_MES_08,
		  ACUMULA.AC_MES_09  = AR.AC_MES_09,
		  ACUMULA.AC_MES_10  = AR.AC_MES_10,
		  ACUMULA.AC_MES_11  = AR.AC_MES_11,
		  ACUMULA.AC_MES_12  = AR.AC_MES_12,
		  ACUMULA.AC_MES_13  = AR.AC_MES_13,
		  ACUMULA.AC_ANUAL   = AR.AC_ANUAL
	from ACUMULA
	left outer join VACUMRSTOT AR  on ACUMULA.CB_CODIGO = AR.CB_CODIGO and ACUMULA.AC_YEAR = AR.AC_YEAR and ACUMULA.CO_NUMERO = AR.CO_NUMERO 
	where ( AR.AC_YEAR = @AC_YEAR ) and ( AR.CB_CODIGO = @CB_CODIGO ) and (  @ListaConceptos = '' OR  AR.CO_NUMERO in ( select  items from dbo.FN_SPLIT( @ListaConceptos, ',' ) ) ) 
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'RECALCULA_ACUMULADOS_CONCEPTOS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE RECALCULA_ACUMULADOS_CONCEPTOS	
GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS_CONCEPTOS(					
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
				    @MES SMALLINT, 
					@TIPO SMALLINT, 
					@Numero SMALLINT, 
					@ListaConceptos Formula  
			) 
AS
BEGIN 
	SET NOCOUNT ON 

	 DECLARE @RS_CODIGO Codigo 


	 -- Encapsular en Funcion 
	 select @RS_CODIGO = RSOCIAL.RS_CODIGO from NOMINA 
	 join RPATRON on NOMINA.CB_PATRON = RPATRON.TB_CODIGO 
	 join RSOCIAL on RSOCIAL.RS_CODIGO  = RPATRON.RS_CODIGO 
	 where NOMINA.PE_YEAR = @ANIO and NOMINA.PE_TIPO = @TIPO and NOMINA.PE_NUMERO = @NUMERO and NOMINA.CB_CODIGO = @EMPLEADO 

	 set @RS_CODIGO = coalesce( @RS_CODIGO, '' ) 

	 -- Fin de futura fujncion 


	 Declare @Concepto Concepto 
	 Declare @Monto Pesos 
	 Declare TemporalMovimien Cursor for
        select M.CO_NUMERO, SUM( M.MO_PERCEPC + M.MO_DEDUCCI )
        from MOVIMIEN M where
               ( M.MO_ACTIVO = 'S' ) and
               ( M.PE_YEAR = @Anio ) and
               ( M.PE_TIPO = @Tipo ) and
               ( M.PE_NUMERO = @Numero ) and
               ( M.CB_CODIGO = @Empleado ) and 
			   ( M.CO_NUMERO in (select items from dbo.FN_SPLIT( @ListaConceptos, ',') ) ) 
        group by M.CO_NUMERO
     open TemporalMovimien
     Fetch next	from TemporalMovimien
     into  @Concepto, @Monto
     WHILE @@FETCH_STATUS = 0
     begin
          EXECUTE AFECTA_UN_CONCEPTO @Anio, @Empleado, @Concepto, @Mes, 1, @Monto, @RS_CODIGO;
     	  Fetch next from TemporalMovimien
     	  into  @Concepto, @Monto
     end

     CLOSE TemporalMovimien
     DEALLOCATE TemporalMovimien
END
GO 


IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'LIMPIA_ACUMULADOS_TOTALES_CONCEPTOS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE LIMPIA_ACUMULADOS_TOTALES_CONCEPTOS
GO

CREATE  PROCEDURE LIMPIA_ACUMULADOS_TOTALES_CONCEPTOS( 	
					@ANIO SMALLINT,
    				@EMPLEADO INTEGER,
    				@MES SMALLINT, 
					@ListaConceptos Formula 
					) 
AS
BEGIN
	set nocount on 

     if ( @Mes = 1 )
        update ACUMULA set AC_MES_01 = 0 where ( AC_YEAR = @Anio ) and ( CB_CODIGO = @Empleado );
 
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'LIMPIA_ACUMULADOS_RS_CONCEPTOS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE LIMPIA_ACUMULADOS_RS_CONCEPTOS
GO

CREATE  PROCEDURE LIMPIA_ACUMULADOS_RS_CONCEPTOS(
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT, 
					@ListaConceptos Formula 
					) 
AS
BEGIN
	set nocount on 	

	declare @Query   nvarchar(210)
    declare @mesChar nvarchar(2)
    declare @Params	 nvarchar( 150  )
	
	if ( @Mes < 10 )                   
        set @mesChar = '0'         
    set @mesChar = @mesChar + cast( @Mes as varchar(2) )

	set @Query = 'update ACUMULA_RS set AC_MES_' + @mesChar + ' = 0 
				  where ( AC_YEAR = @Anio ) and (CB_CODIGO = @Empleado ) and ( CO_NUMERO in ( select items from dbo.FN_SPLIT(@ListaConceptos, '','' ) ) )'
                                
	set @Params  = N'@Anio status, @Empleado int,  @ListaConceptos varchar(255)'	
	
	exec sp_executesql @Query, @Params, @Anio, @Empleado, @ListaConceptos 

	set @Query = 'update ACUMULA set AC_MES_' + @mesChar + ' = 0 
				  where ( AC_YEAR = @Anio ) and (CB_CODIGO = @Empleado ) and ( CO_NUMERO in ( select items from dbo.FN_SPLIT(@ListaConceptos, '','' ) ) )'
                                      
	set @Params  = N'@Anio status, @Empleado int,  @ListaConceptos varchar(255)'
	
	exec sp_executesql @Query, @Params, @Anio, @Empleado, @ListaConceptos 
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'RECALCULA_ACUMULADOS') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE RECALCULA_ACUMULADOS
GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS
					@ANIO SMALLINT,
					@EMPLEADO INTEGER,
					@MES SMALLINT,
					@STATUS SMALLINT, 
					@lAlgunosConceptos Booleano = 'N', 
					@ListaConceptos Formula  = '' 
AS
BEGIN
    SET NOCOUNT ON
	DECLARE @TIPO SMALLINT;
	DECLARE @NUMERO SMALLINT;
	
	IF @lAlgunosConceptos = 'N' 
	begin
		exec LIMPIA_ACUMULADOS_RS		@ANIO, @EMPLEADO, @MES  
		exec LIMPIA_ACUMULADOS_TOTALES	@ANIO, @EMPLEADO, @MES 
	end
	else
	begin
		exec LIMPIA_ACUMULADOS_RS_CONCEPTOS @ANIO, @EMPLEADO, @MES, @ListaConceptos
		exec LIMPIA_ACUMULADOS_TOTALES_CONCEPTOS @ANIO, @EMPLEADO, @MES, @ListaConceptos
	end
	-- Borra 

	Declare TemporalAcumula Cursor for
		select P.PE_TIPO, P.PE_NUMERO from NOMINA N join PERIODO P on
               ( P.PE_YEAR = N.PE_YEAR ) and
               ( P.PE_TIPO = N.PE_TIPO ) and
               ( P.PE_NUMERO = N.PE_NUMERO )
        where
             ( N.NO_STATUS = @Status ) and
             ( N.CB_CODIGO = @Empleado ) and
             ( N.PE_YEAR = @Anio ) and
             ( P.PE_MES = @Mes )

	OPEN TemporalAcumula
	FETCH NEXT FROM TemporalAcumula
	INTO @Tipo, @Numero 
    WHILE @@FETCH_STATUS = 0
	begin
		IF @lAlgunosConceptos = 'N' 
			EXECUTE AFECTA_EMPLEADO @Empleado, @Anio, @Tipo, @Numero, @Mes, 1;
		ELSE 
			EXECUTE RECALCULA_ACUMULADOS_CONCEPTOS @Anio, @Empleado, @Mes, @Tipo, @Numero, @ListaConceptos

		FETCH NEXT FROM TemporalAcumula
		INTO @Tipo, @Numero 
    end
    Close TemporalAcumula
    Deallocate TemporalAcumula
	
	IF @lAlgunosConceptos = 'N' 
		exec SP_AcumuladosRazonSocial_SumarAnual @ANIO, @EMPLEADO
	ELSE
		exec SP_AcumuladosRazonSocial_SumarAnual @ANIO, @EMPLEADO, @ListaConceptos 
END
GO

/** Recalcula acumulado del mes 1 al 13**/
IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'RECALCULA_ACUMULADOS_MESES') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE RECALCULA_ACUMULADOS_MESES
GO

CREATE  PROCEDURE RECALCULA_ACUMULADOS_MESES
					@ANIO SMALLINT,
					@MESINI SMALLINT, 
					@MESFIN SMALLINT, 
					@EMPLEADO INTEGER,					
					@STATUS SMALLINT, 
					@lAlgunosConceptos Booleano = 'N' , 
					@ListaConceptos Formula  = '',
					@RangoConceptos INTEGER = 0 
AS
BEGIN
    SET NOCOUNT ON
	declare @iMes smallint 
	set @iMes = @MESINI 

	/*** Generar Lista de Conceptos si es de tipo Rango apartir del filtro ListaConceptos ***/	
	declare @ListaConceptosRango Formula
	declare @Query   nvarchar(max)
    declare @Params	 nvarchar( 150 )
	if @RangoConceptos = 2 or @RangoConceptos = 3 --Rango
	begin
		set @Query = 'select @ListaConceptosRango = coalesce(@ListaConceptosRango' + ' + '','' ' + ', '''') + CONVERT(varchar(255), CO_NUMERO) from CONCEPTO where ' +  @ListaConceptos
        set @Params  = N'@ListaConceptos varchar(255), @ListaConceptosRango varchar(255) OUTPUT'		
        exec sp_executesql @Query, @Params, @ListaConceptos, @ListaConceptosRango OUTPUT 
		set @ListaConceptos = @ListaConceptosRango 
	end
	/*** FIN ***/	
	while @iMes <= @MESFIN 
	begin 
		exec RECALCULA_ACUMULADOS @ANIO, @EMPLEADO, @iMes, @STATUS, @lAlgunosConceptos, @ListaConceptos
		set @iMes = @iMes + 1;
	end
END
GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_AcumuladosRazonSocial_Delete') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_AcumuladosRazonSocial_Delete
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Delete(
					@AC_ID	FolioGrande,
					@AC_YEAR Anio,
					@CB_CODIGO NumeroEmpleado,
					@CO_NUMERO Concepto,
					@RS_CODIGO Codigo
					)
AS
BEGIN
	set nocount on 
		
	delete from ACUMULA_RS 
	where  ( AC_ID = @AC_ID )

	exec CALCULA_ACUMULADOS_TOTALES_ANUAL @AC_YEAR, @CB_CODIGO, @CO_NUMERO 

END
GO

IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'FN_RazonSocialEmpleado'  AND XTYPE = 'FN' )
	DROP FUNCTION FN_RazonSocialEmpleado
GO

CREATE FUNCTION FN_RazonSocialEmpleado( @Empleado NumeroEmpleado )
RETURNS Codigo 
AS
BEGIN
	DECLARE @RazonSocial Codigo

	SELECT @RazonSocial = PATRON.RS_CODIGO FROM COLABORA 
	JOIN RPATRON PATRON ON COLABORA.CB_PATRON = PATRON.TB_CODIGO 
	WHERE COLABORA.CB_CODIGO = @Empleado  

	SET @RazonSocial = coalesce( @RazonSocial, '' ) 

	RETURN @RazonSocial 
END

GO 

IF EXISTS ( SELECT *  FROM SYSOBJECTS WHERE NAME = 'FN_GET_AC_MES'  AND XTYPE = 'FN' )
	DROP FUNCTION FN_GET_AC_MES

GO


CREATE  FUNCTION FN_GET_AC_MES( @Mes Status ) 
returns varchar(10)
as
begin 
	return 'AC_MES_'+RIGHT('00'+CAST(@Mes AS VARCHAR(2)),2)	
end 


GO

IF EXISTS ( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'SP_AcumuladosRazonSocial_Importa') AND type IN ( N'P', N'PC' ))
	DROP PROCEDURE SP_AcumuladosRazonSocial_Importa
GO

CREATE  PROCEDURE SP_AcumuladosRazonSocial_Importa( 
                                  @Concepto Concepto,
                                  @Mes SMALLINT,
                                  @Anio Anio,
                                  @Empleado NumeroEmpleado,
                                  @Monto Pesos ,      
                                  @RazonSocial Codigo, 
                                  @limpiarAcumulados Booleano,
								  @Operacion SMALLINT
                                  )
AS
BEGIN
    set nocount on 

	declare @Query   nvarchar(500)
    declare @mesCol nvarchar(10)
    declare @Params	 nvarchar( 150  )
	declare @omSustituir int
	declare @omSumar int 
	declare @omRestar int 	
	set  @omSustituir = 0
	set  @omSumar = 1
	set  @omRestar = 2
	
	set @mesCol = ''

    if ( RTRIM( @RazonSocial ) = '' )
            SET @RazonSocial = dbo.FN_RazonSocialEmpleado(@Empleado)

	set @mesCol = dbo.FN_GET_AC_MES(@Mes); 
	     	                 
                                               
    if ( @limpiarAcumulados = 'S' ) 
    begin 
        set @Query = 'update ACUMULA_RS set '+ @mesCol + ' = 0 where
						( AC_YEAR = @Anio ) and 
                        ( CB_CODIGO = @Empleado ) and 
						( CO_NUMERO = @Concepto )'                                       
        set @Params  = N' @Anio status, @Empleado int, @Concepto int'
        exec sp_executesql @Query, @Params, @Anio, @Empleado, @Concepto 
    end

                    
    set @Query = 'update ACUMULA_RS set ' + @mesCol + ' = 	
							case ( @Operacion )
								when @omSumar  then ' + @mesCol + ' + @Monto
								when @omRestar then ' + @mesCol + ' - @Monto
								else @Monto
							end
					where ' +
					'( RS_CODIGO = @RazonSocial ) and ' +
					'( AC_YEAR = @Anio ) and ' +
					'( CB_CODIGO = @Empleado ) and ' +
					'( CO_NUMERO = @Concepto )'	
    set @Params = N'@RazonSocial char(6), @Anio status, @Empleado int, @Concepto int, @Monto decimal(15,2), @Operacion smallint, @omSumar smallint, @omRestar smallint'	
    exec sp_executesql @Query, @Params, @RazonSocial, @Anio, @Empleado, @Concepto, @Monto, @Operacion, @omSumar, @OmRestar 
	    
	IF @@ROWCOUNT = 0  
    begin 
		set @Query = 'insert into ACUMULA_RS (RS_CODIGO, AC_YEAR, CB_CODIGO, CO_NUMERO, ' + @mesCol + ' ) values ( @RazonSocial, @Anio, @Empleado, @Concepto, @Monto )' 			                                      
        set @Params  = N'@RazonSocial char(6), @Anio status, @Empleado int, @Concepto int,  @Monto decimal(15,2)'		
        exec sp_executesql @Query, @Params, @RazonSocial, @Anio, @Empleado, @Concepto, @Monto                 		
    end     
             
    /* No filtra por Razon Social para limpiar AC_ANUAL de las razones sociales no asociadas a la nomina del empleados en caso de datos sucios */ 
    update ACUMULA_RS set AC_ANUAL = (AC_MES_01 +
                                                        AC_MES_02 +
                                                        AC_MES_03 +
                                                        AC_MES_04 +
                                                        AC_MES_05 +
                                                        AC_MES_06 +
                                                        AC_MES_07 +
                                                        AC_MES_08 +
                                                        AC_MES_09 +
                                                        AC_MES_10 +
                                                        AC_MES_11 +
                                                        AC_MES_12 +
                                                        AC_MES_13)
	where ( AC_YEAR = @Anio ) and
		  ( CB_CODIGO = @Empleado ) and
		  ( CO_NUMERO = @Concepto );
             
    /* Recalcula Monto Anunal de la tabla ACUMULA */
    exec CALCULA_ACUMULADOS_TOTALES_ANUAL @Anio, @Empleado, @Concepto
END

GO

/* PARIENTE */
/* Inicializar valores null*/
update PARIENTE set PA_APE_PAT = '' where PA_APE_PAT is NULL
GO

update PARIENTE set PA_APE_MAT = '' where PA_APE_MAT is NULL
GO

update PARIENTE set PA_NOMBRES = '' where PA_NOMBRES is NULL
GO

/* NOMINA */
update NOMINA set NO_CAN_UID = '' where NO_CAN_UID is NULL
GO

/* ENTRENA */
update ENTRENA set EN_REPROG = 'N' where EN_REPROG is NULL
GO

update ENTRENA set EN_RE_DIAS = 0 where EN_RE_DIAS is NULL
GO