/***************************************/
/****** Campos nuevos ******************/
/***************************************/

/* Agregar PUESTO.PU_SAL_MIN: Salario M�nimo */
/* Patch 385: # Seq: 1 Agregado */
alter table PUESTO add PU_SAL_MIN PESOSDIARIO

/* Agregar PUESTO.PU_SAL_MAX: Salario M�ximo */
/* Patch 385: # Seq: 2 Agregado */
alter table PUESTO add PU_SAL_MAX PESOSDIARIO

/* Agregar PUESTO.PU_SAL_MED: Salario Promedio */
/* Patch 385: # Seq: 3 Agregado */
alter table PUESTO add PU_SAL_MED PESOSDIARIO

/* Agregar PUESTO.PU_SAL_EN1: Salario en Encuesta # 1 */
/* Patch 385: # Seq: 4 Agregado */
alter table PUESTO add PU_SAL_EN1 PESOSDIARIO

/* Agregar PUESTO.PU_SAL_EN2: Salario en Encuesta #2 */
/* Patch 385: # Seq: 5 Agregado */
alter table PUESTO add PU_SAL_EN2 PESOSDIARIO

/* Patch 385: Cambio en Nivel1*/
/* Patch 385: # Seq: 6 Agregado */
alter table KARDEX add CB_MOD_NV1 Booleano

/* Patch 385: Cambio en Nivel2*/
/* Patch 385: # Seq: 7 Agregado */
alter table KARDEX add CB_MOD_NV2 Booleano

/* Patch 385: Cambio en Nivel3*/
/* Patch 385: # Seq: 8 Agregado */
alter table KARDEX add CB_MOD_NV3 Booleano

/* Patch 385: Cambio en Nivel4*/
/* Patch 385: # Seq: 9 Agregado */
alter table KARDEX add CB_MOD_NV4 Booleano

/* Patch 385: Cambio en Nivel5*/
/* Patch 385: # Seq: 10 Agregado */
alter table KARDEX add CB_MOD_NV5 Booleano

/* Patch 385: Cambio en Nivel6*/
/* Patch 385: # Seq: 11 Agregado */
alter table KARDEX add CB_MOD_NV6 Booleano

/* Patch 385: Cambio en Nivel7*/
/* Patch 385: # Seq: 12 Agregado */
alter table KARDEX add CB_MOD_NV7 Booleano

/* Patch 385: Cambio en Nivel8*/
/* Patch 385: # Seq: 13 Agregado */
alter table KARDEX add CB_MOD_NV8 Booleano

/* Patch 385: Cambio en Nivel9*/
/* Patch 385: # Seq: 14 Agregado */
alter table KARDEX add CB_MOD_NV9 Booleano

/* Patch 385: # De Revisi�n */
/* Patch 385: # Seq: 65 Agregado */
alter table CURSO add CU_REVISIO NombreCampo

/* Patch 385: Fecha De Revisi�n */
/* Patch 385: # Seq: 66 Agregado */
alter table CURSO add CU_FEC_REV Fecha

/* Patch 385: # De Revisi�n */
/* Patch 385: # Seq: 67 Agregado */
alter table KARCURSO add KC_REVISIO NombreCampo

/* Patch 385: # De Sesi�n */
/* Patch 385: # Seq: 68 Agregado */
alter table KARCURSO add SE_FOLIO FolioGrande

/* Patch 385: Usuario Quien Aprueba */
/* Patch 385: # Seq: 69 Agregado */
alter table CHECADAS add US_COD_OK Usuario

/* Patch 385: Prioridad Del Tipo de Ahorro */
/* Patch 385: # Seq: 70 Agregado */
alter table TAHORRO add TB_PRIORID Status

/* Patch 385: Prioridad Del Tipo de Pr�stamo */
/* Patch 385: # Seq: 71 Agregado */
alter table TPRESTA add TB_PRIORID Status

/* Patch 385: # de Solicitante en Selecci�n de Personal */
/* Patch 385: # Seq: 81 Agregado */
alter table COLABORA add CB_CANDIDA FolioGrande

/* Patch 385: Tipo de Estudios */
/* Patch 385: # Seq: 87 Agregado */
alter table ESTUDIOS add TB_TIPO Status

/* Patch 385: Global para Per�odo de N�mina de Fecha L�mite de Bloqueo */
/* Patch 385: # Seq: 88 Agregado */
insert into GLOBAL( GL_CODIGO, GL_DESCRIP, GL_FORMULA ) values ( 181, 'Per�odo Para Fijar Bloqueo', '1' )

/* Inicializar PUESTO.PU_SAL_MIN: Salario M�nimo */
/* Patch 385: # Seq: 105 Agregado */
update PUESTO set PU_SAL_MIN = 0 where ( PU_SAL_MIN is NULL )

/* Inicializar PUESTO.PU_SAL_MAX: Salario M�ximo */
/* Patch 385: # Seq: 106 Agregado */
update PUESTO set PU_SAL_MAX = 0 where ( PU_SAL_MAX is NULL )

/* Inicializar PUESTO.PU_SAL_MED: Salario Promedio */
/* Patch 385: # Seq: 107 Agregado */
update PUESTO set PU_SAL_MED = 0 where ( PU_SAL_MED is NULL )

/* Inicializar PUESTO.PU_SAL_EN1: Salario en Encuesta # 1 */
/* Patch 385: # Seq: 108 Agregado */
update PUESTO set PU_SAL_EN1 = 0 where ( PU_SAL_EN1 is NULL )

/* Inicializar PUESTO.PU_SAL_EN2: Salario en Encuesta #2 */
/* Patch 385: # Seq: 109 Agregado */
update PUESTO set PU_SAL_EN2 = 0 where ( PU_SAL_EN2 is NULL )

/* Inicializar Cambio en Nivel1*/
/* Patch 385: # Seq: 110 Agregado */
update KARDEX set CB_MOD_NV1 = 'N' where ( CB_MOD_NV1 is NULL )

/* Inicializar Cambio en Nivel2*/
/* Patch 385: # Seq: 111 Agregado */
update KARDEX set CB_MOD_NV2 = 'N' where ( CB_MOD_NV2 is NULL )

/* Inicializar Cambio en Nivel3*/
/* Patch 385: # Seq: 112 Agregado */
update KARDEX set CB_MOD_NV3 = 'N' where ( CB_MOD_NV3 is NULL )

/* Inicializar Cambio en Nivel4*/
/* Patch 385: # Seq: 113 Agregado */
update KARDEX set CB_MOD_NV4 = 'N' where ( CB_MOD_NV4 is NULL )

/* Inicializar Cambio en Nivel5*/
/* Patch 385: # Seq: 114 Agregado */
update KARDEX set CB_MOD_NV5 = 'N' where ( CB_MOD_NV5 is NULL )

/* Inicializar Cambio en Nivel6*/
/* Patch 385: # Seq: 115 Agregado */
update KARDEX set CB_MOD_NV6 = 'N' where ( CB_MOD_NV6 is NULL )

/* Inicializar Cambio en Nivel7*/
/* Patch 385: # Seq: 116 Agregado */
update KARDEX set CB_MOD_NV7 = 'N' where ( CB_MOD_NV7 is NULL )

/* Inicializar Cambio en Nivel8*/
/* Patch 385: # Seq: 117 Agregado */
update KARDEX set CB_MOD_NV8 = 'N' where ( CB_MOD_NV8 is NULL )

/* Inicializar Cambio en Nivel9*/
/* Patch 385: # Seq: 118 Agregado */
update KARDEX set CB_MOD_NV9 = 'N' where ( CB_MOD_NV9 is NULL )

/* Inicializar # De Revisi�n */
/* Patch 385: # Seq: 119 Agregado */
update CURSO set CU_REVISIO = '' where ( CU_REVISIO is NULL )

/* Inicializar # De Revisi�n */
/* Patch 385: # Seq: 120 Agregado */
update KARCURSO set KC_REVISIO = '' where ( KC_REVISIO is NULL )

/* Inicializar # De Sesi�n */
/* Patch 385: # Seq: 121 Agregado */
update KARCURSO set SE_FOLIO = 0 where ( SE_FOLIO is NULL )

/* Inicializar Usuario Quien Aprueba */
/* Patch 385: # Seq: 122 Agregado */
update CHECADAS set US_COD_OK = 0 where ( US_COD_OK is NULL )

/* Inicializar Prioridad Del Tipo de Ahorro */
/* Patch 385: # Seq: 123 Agregado */
update TAHORRO set TB_PRIORID = 0 where ( TB_PRIORID is NULL )

/* Inicializar Prioridad Del Tipo de Pr�stamo */
/* Patch 385: # Seq: 124 Agregado */
update TPRESTA set TB_PRIORID = 0 where ( TB_PRIORID is NULL )

/* Inicializar # de Solicitante en Selecci�n de Personal */
/* Patch 385: # Seq: 125 Agregado */
update COLABORA set CB_CANDIDA = 0 where ( CB_CANDIDA is NULL )

/* Inicializar Tipo de Estudios */
/* Patch 385: # Seq: 126 Agregado */
update ESTUDIOS set TB_TIPO = 0 where ( TB_TIPO is NULL )

/* Patch 385: Identificador de Tarjeta de Proximidad */
/* Patch 385: # Seq: 127 Agregado */
alter table COLABORA add CB_ID_NUM Descripcion;

/* Inicializar Identificador de Tarjeta de Proximidad */
/* Patch 385: # Seq: 128 Agregado */
update COLABORA set CB_ID_NUM = '' where ( CB_ID_NUM is NULL );

/* Indice alterno por Identificador de Tarjeta sobre COLABORA */
/* Patch 385: # Seq: 129 Agregado */
create index XIE2COLABORA on COLABORA( CB_ID_NUM )

/* Patch 385: Identificador de Tarjeta de Proximidad */
/* Patch 385: # Seq: 130 Agregado */
alter table INVITA add IV_ID_NUM Descripcion

/* Inicializar Identificador de Tarjeta de Proximidad */
/* Patch 385: # Seq: 131 Agregado */
update INVITA set IV_ID_NUM = '' where ( IV_ID_NUM is NULL )

/* Inicializar Valores del Nuevo Periodo Semanal - A */
/* Patch 385: # Seq: 140 Agregado */
insert into TPERIODO( TP_TIPO, TP_NOMBRE, TP_DESCRIP, TP_DIAS, TP_DIAS_7, TP_HORAS ) values
( 6, 'Semana A', 'Semanal A', 6, 7, 48 )
GO

/* Inicializar Valores del Nuevo Periodo Semanal - B */
/* Patch 385: # Seq: 141 Agregado */
insert into TPERIODO( TP_TIPO, TP_NOMBRE, TP_DESCRIP, TP_DIAS, TP_DIAS_7, TP_HORAS ) values
( 7, 'Semana B', 'Semanal B', 6, 7, 48 )
GO

/* Inicializar Valores del Nuevo Periodo Quincenal - A */
/* Patch 385: # Seq: 142 Agregado */
insert into TPERIODO( TP_TIPO, TP_NOMBRE, TP_DESCRIP, TP_DIAS, TP_DIAS_7, TP_HORAS ) values
( 8, 'Quincena A', 'Quincenal A', 13, 15.2, 104 )
GO

/********************************/
/********* Tablas Nuevas ********/
/********************************/

/* Tabla FAM_PTO: Familias de Puestos */
/* Patch 385 # Seq: 15 Agregado */
create table FAM_PTO (
       FP_CODIGO            Codigo,
       FP_DESCRIP           Descripcion,
       FP_INGLES            Descripcion,
       FP_NUMERO            Pesos,
       FP_TEXTO             Descripcion )
GO

/* PK FAM_PTO: Llave Primaria */
/* Patch 385 # Seq: 16 Agregado */
alter table FAM_PTO ADD CONSTRAINT PK_FAM_PTO PRIMARY KEY (FP_CODIGO)
GO

/* Tabla NIV_PTO: Niveles de Puesto  */
/* Patch 385 # Seq: 17 Agregado */
create table NIV_PTO (
       NP_CODIGO            Codigo,
       NP_DESCRIP           Descripcion,
       NP_INGLES            Descripcion,
       NP_NUMERO            Pesos,
       NP_TEXTO             Descripcion,
       NP_ACTITUD           Observaciones,
       NP_DETALLE           Memo )
GO

/* PK NIV_PTO: Llave Primaria */
/* Patch 385 # Seq: 18 Agregado */
alter table NIV_PTO ADD CONSTRAINT PK_NIV_PTO PRIMARY KEY (NP_CODIGO)
GO

/* PUESTO: Apuntador hacia Familia de Puesto */
/* Patch 385 # Seq: 19 Agregado */
alter table PUESTO ADD FP_CODIGO Codigo
GO

/* PUESTO: Apuntador hacia Nivel de Puesto */
/* Patch 385 # Seq: 20 Agregado */
alter table PUESTO ADD NP_CODIGO Codigo
GO

/* Tabla DIMENSIO: Dimensiones  */
/* Patch 385 # Seq: 21 Agregado */
create table DIMENSIO (
       DM_CODIGO            Codigo,
       DM_DESCRIP           Descripcion,
       DM_INGLES            Descripcion,
       DM_NUMERO            Pesos,
       DM_TEXTO             Descripcion )
GO

/* PK DIMENSIO: Llave Primaria */
/* Patch 385 # Seq: 22 Agregado */
alter table DIMENSIO ADD CONSTRAINT PK_DIMENSIO PRIMARY KEY (DM_CODIGO)
GO

/* Tabla PTO_DIME: Dimensiones de Puestos */
/* Patch 385 # Seq: 23 Agregado */
create table PTO_DIME (
       PU_CODIGO            Codigo,
       DM_CODIGO            Codigo,
       PD_DESCRIP           Formula )
GO

/* PK PTO_DIME: Llave Primaria */
/* Patch 385 # Seq: 24 Agregado */
alter table PTO_DIME ADD CONSTRAINT PK_PTO_DIME PRIMARY KEY (PU_CODIGO, DM_CODIGO)
GO

/* Tabla CALIFICA: Calificaciones */
/* Patch 385 # Seq: 25 Agregado */
create table CALIFICA (
       CA_CODIGO            Codigo,
       CA_DESCRIP           Descripcion,
       CA_INGLES            Descripcion,
       CA_NUMERO            Pesos,
       CA_TEXTO             Descripcion,
       CA_ORDEN             Status,
       CA_GRUPO             Referencia )
GO

/* PK CALIFICA: Llave Primaria */
/* Patch 385 # Seq: 26 Agregado */
alter table CALIFICA ADD CONSTRAINT PK_CALIFICA PRIMARY KEY (CA_CODIGO)
GO

/* Tabla TCOMPETE: Tipos de Competencias */
/* Patch 385 # Seq: 27 Agregado */
create table TCOMPETE (
       TC_CODIGO            Codigo,
       TC_DESCRIP           Descripcion,
       TC_INGLES            Descripcion,
       TC_NUMERO            Pesos,
       TC_TEXTO             Descripcion,
       TC_TIPO              Status )
GO

/* PK TCOMPETE: Llave Primaria */
/* Patch 385 # Seq: 28 Agregado */
alter table TCOMPETE ADD CONSTRAINT PK_TCOMPETE PRIMARY KEY (TC_CODIGO)
GO

/* Tabla COMPETEN: Competencias */
/* Patch 385 # Seq: 29 Agregado */
create table COMPETEN (
       CM_CODIGO            Codigo,
       TC_CODIGO            Codigo,
       CM_DESCRIP           Descripcion,
       CM_INGLES            Descripcion,
       CM_NUMERO            Pesos,
       CM_TEXTO             Descripcion,
       CM_DETALLE           Memo )
GO

/* PK COMPETEN: Llave Primaria */
/* Patch 385 # Seq: 30 Agregado */
alter table COMPETEN ADD CONSTRAINT PK_COMPETEN PRIMARY KEY (CM_CODIGO)
GO

/* Tabla COMP_FAM: Familias de Competencias */
/* Patch 385 # Seq: 31 Agregado */
create table COMP_FAM (
       FP_CODIGO            Codigo,
       CM_CODIGO            Codigo,
       CF_OBSERVA           Observaciones )
GO

/* PK COMP_FAM: Llave Primaria */
/* Patch 385 # Seq: 32 Agregado */
alter table COMP_FAM ADD CONSTRAINT PK_COMP_FAM PRIMARY KEY (FP_CODIGO, CM_CODIGO)
GO

/* Tabla COMP_CAL: Competencias de Calificaciones */
/* Patch 385 # Seq: 33 Agregado */
create table COMP_CAL (
       CM_CODIGO            Codigo,
       CA_CODIGO            Codigo,
       MC_DESCRIP           Formula )
GO

/* PK COMP_CAL: Llave Primaria */
/* Patch 385 # Seq: 34 Agregado */
alter table COMP_CAL ADD CONSTRAINT PK_COMP_CAL PRIMARY KEY (CM_CODIGO, CA_CODIGO)
GO

/* Tabla COMP_PTO: Competencias de Puestos  */
/* Patch 385 # Seq: 35 Agregado */
create table COMP_PTO (
       PU_CODIGO            Codigo,
       CM_CODIGO            Codigo,
       CA_CODIGO            Codigo,
       MP_DESCRIP           Formula )
GO

/* PK COMP_PTO: Llave Primaria */
/* Patch 385 # Seq: 36 Agregado */
alter table COMP_PTO ADD CONSTRAINT PK_COMP_PTO PRIMARY KEY (PU_CODIGO, CM_CODIGO)
GO

/* Tabla EMP_COMP: Competencias de Empleados */
/* Patch 385 # Seq: 37 Agregado */
create table EMP_COMP (
       CB_CODIGO            NumeroEmpleado,
       CM_CODIGO            Codigo,
       CA_CODIGO            Codigo,
       EC_OBSERVA           Formula,
       EC_FEC_MOD           Fecha,
       US_CODIGO            Usuario )
GO

/* PK EMP_COMP: Llave Primaria */
/* Patch 385 # Seq: 38 Agregado */
alter table EMP_COMP ADD CONSTRAINT PK_EMP_COMP PRIMARY KEY (CB_CODIGO, CM_CODIGO)
GO

/* Tabla ACCION: Acciones  */
/* Patch 385 # Seq: 39 Agregado */
create table ACCION (
       AN_CODIGO            Codigo,
       CU_CODIGO            Codigo,
       AN_NOMBRE            Observaciones,
       AN_INGLES            Descripcion,
       AN_NUMERO            Pesos,
       AN_TEXTO             Descripcion,
       AN_CLASE             Status,
       AN_TIP_MAT           Descripcion,
       AN_DETALLE           Memo,
       AN_DIAS              Dias,
       AN_URL               PathArchivo )
GO

/* PK ACCION: Llave Primaria */
/* Patch 385 # Seq: 40 Agregado */
alter table ACCION ADD CONSTRAINT PK_ACCION PRIMARY KEY (AN_CODIGO)
GO

/* Tabla EMP_PLAN: Plan de Acci�n Por Empleado */
/* Patch 385 # Seq: 41 Agregado */
create table EMP_PLAN (
       CB_CODIGO            NumeroEmpleado,
       AN_CODIGO            Codigo,
       EP_FEC_INI           Fecha,
       EP_FEC_PRO           Fecha,
       EP_FEC_FIN           Fecha,
       EP_FEC_MOD           Fecha,
       US_CODIGO            Usuario,
       EP_TERMINO           Booleano,
       EP_OBSERVA           Formula,
       EP_RESULT            Pesos )
GO

/* PK EMP_PLAN: Llave Primaria */
/* Patch 385 # Seq: 42 Agregado */
alter table EMP_PLAN ADD CONSTRAINT PK_EMP_PLAN PRIMARY KEY (CB_CODIGO, AN_CODIGO)
GO

/* Tabla COMP_MAP: Plan de Acci�n Por Competencia */
/* Patch 385 # Seq: 43 Agregado */
create table COMP_MAP (
       CM_CODIGO            Codigo,
       AN_CODIGO            Codigo,
       CM_OBSERVA           Formula,
       CM_ORDEN             Status )
GO

/* PK COMP_MAP: Llave Primaria */
/* Patch 385 # Seq: 44 Agregado */
alter table COMP_MAP ADD CONSTRAINT PK_COMP_MAP PRIMARY KEY (CM_CODIGO, AN_CODIGO)
GO

/* Tabla NIV_DIME: Niveles de Dimensiones */
/* Patch 385 # Seq: 45 Agregado */
create table NIV_DIME (
       DM_CODIGO            Codigo,
       NP_CODIGO            Codigo,
       ND_RESUMEN           Observaciones,
       ND_DESCRIP           Memo )
GO

/* PK NIV_DIME: Llave Primaria */
/* Patch 385 # Seq: 46 Agregado */
alter table NIV_DIME ADD CONSTRAINT PK_NIV_DIME PRIMARY KEY (DM_CODIGO, NP_CODIGO)
GO

/* Tabla SESION: Sesiones de Cursos */
/* Patch 385 # Seq: 72 Agregado */
CREATE TABLE SESION (
       SE_FOLIO             FolioGrande,
       CU_CODIGO            Codigo,
       MA_CODIGO            Codigo,
       SE_LUGAR             Observaciones,
       SE_FEC_INI           Fecha,
       SE_HOR_INI           Hora,
       SE_FEC_FIN           Fecha,
       SE_HORAS             Horas,
       SE_HOR_FIN           Hora,
       SE_CUPO              FolioChico,
       SE_COMENTA           Observaciones,
       SE_REVISIO           NombreCampo )
GO

/* PK SESION: Llave Primaria */
/* Patch 385 # Seq: 73 Agregado */
alter table SESION ADD CONSTRAINT PK_SESION PRIMARY KEY (SE_FOLIO)
GO

/* Tabla MISREPOR: Reportes Favoritos */
/* Patch 385 # Seq: 74 Agregado */
CREATE TABLE MISREPOR (
       US_CODIGO        Usuario,
       RE_CODIGO	    FolioChico )
GO

/* PK MISREPOR: Llave Primaria */
/* Patch 385 # Seq: 75 Agregado */
ALTER TABLE MISREPOR ADD CONSTRAINT PK_MISREPOR PRIMARY KEY (US_CODIGO,RE_CODIGO)
GO

/* Tabla TMPDIMM: Declaraciones Anuales */
/* Patch 385 # Seq: 79 Agregado */
CREATE TABLE TMPDIMM (
       TD_YEAR              Anio,
       US_CODIGO            Usuario,
       CB_CODIGO            NumeroEmpleado,
       TD_CUANTOS           FolioChico,
       TD_REPETID           NumeroEmpleado,
       TD_DATA_01           Pesos,
       TD_DATA_02           Pesos,
       TD_DATA_03           Pesos,
       TD_DATA_04           Pesos,
       TD_DATA_05           Pesos,
       TD_DATA_06           Pesos,
       TD_DATA_07           Pesos,
       TD_DATA_08           Pesos,
       TD_DATA_09           Pesos,
       TD_DATA_10           Pesos,
       TD_DATA_11           Pesos,
       TD_DATA_12           Pesos,
       TD_DATA_13           Pesos,
       TD_DATA_14           Pesos,
       TD_DATA_15           Pesos,
       TD_DATA_16           Pesos,
       TD_DATA_17           Pesos,
       TD_DATA_18           Pesos,
       TD_DATA_19           Pesos,
       TD_DATA_20           Pesos,
       TD_DATA_21           Pesos,
       TD_DATA_22           Pesos,
       TD_DATA_23           Pesos,
       TD_DATA_24           Pesos,
       TD_DATA_25           Pesos,
       TD_DATA_26           Pesos,
       TD_DATA_27           Pesos,
       TD_DATA_28           Pesos,
       TD_DATA_29           Pesos,
       TD_DATA_30           Pesos,
       TD_DATA_31           Pesos,
       TD_DATA_32           Pesos,
       TD_DATA_33           Pesos,
       TD_DATA_34           Pesos,
       TD_DATA_35           Pesos,
       TD_DATA_36           Pesos,
       TD_DATA_37           Pesos,
       TD_DATA_38           Pesos,
       TD_DATA_39           Pesos,
       TD_DATA_40           Pesos,
       TD_DATA_41           Pesos,
       TD_DATA_42           Pesos,
       TD_DATA_43           Pesos,
       TD_DATA_44           Pesos,
       TD_DATA_45           Pesos,
       TD_DATA_46           Pesos,
       TD_DATA_47           Pesos,
       TD_DATA_48           Pesos,
       TD_DATA_49           Pesos,
       TD_DATA_50           Pesos,
       TD_DATA_51           Pesos,
       TD_DATA_52           Pesos,
       TD_DATA_53           Pesos,
       TD_DATA_54           Pesos,
       TD_DATA_55           Pesos,
       TD_DATA_56           Pesos,
       TD_DATA_57           Pesos,
       TD_DATA_58           Pesos,
       TD_DATA_59           Pesos,
       TD_DATA_60           Pesos,
       TD_DATA_61           Pesos,
       TD_DATA_62           Pesos,
       TD_DATA_63           Pesos,
       TD_DATA_64           Pesos,
       TD_DATA_65           Pesos,
       TD_DATA_66           Pesos,
       TD_DATA_67           Pesos,
       TD_DATA_68           Pesos,
       TD_DATA_69           Pesos,
       TD_DATA_70           Pesos,
       TD_DATA_71           Pesos,
       TD_DATA_72           Pesos,
       TD_DATA_73           Pesos,
       TD_DATA_74           Pesos,
       TD_DATA_75           Pesos,
       TD_DATA_76           Pesos,
       TD_DATA_77           Pesos,
       TD_DATA_78           Pesos,
       TD_DATA_79           Pesos,
       TD_DATA_80           Pesos,
       TD_DATA_81           Pesos,
       TD_DATA_82           Pesos,
       TD_DATA_83           Pesos,
       TD_DATA_84           Pesos,
       TD_DATA_85           Pesos,
       TD_DATA_86           Pesos,
       TD_DATA_87           Pesos,
       TD_DATA_88           Pesos,
       TD_DATA_89           Pesos,
       TD_DATA_90           Pesos,
       TD_DATA_91           Pesos,
       TD_DATA_92           Pesos,
       TD_DATA_93           Pesos,
       TD_DATA_94           Pesos,
       TD_DATA_95           Pesos,
       TD_DATA_96           Pesos,
       TD_DATA_97           Pesos,
       TD_DATA_98           Pesos,
       TD_DATA_99           Pesos,
       TD_DATA_A0           Pesos )
GO

/* PK TMPDIMM: Llave Primaria */
/* Patch 385 # Seq: 80 Agregado */
ALTER TABLE TMPDIMM ADD CONSTRAINT PK_TMPDIMM PRIMARY KEY (US_CODIGO, TD_YEAR, CB_CODIGO)
GO

/* Tabla EMP_PROG: Cursos Programados Por Empleado */
/* Patch 385 # Seq: 82 Agregado */
CREATE TABLE EMP_PROG (
       CB_CODIGO        NumeroEmpleado,
       CU_CODIGO        Codigo,
       EP_DIAS          Dias,
       EP_OPCIONA       Booleano )
GO

/* PK EMP_PROG: Llave Primaria */
/* Patch 385 # Seq: 83 Agregado */
ALTER TABLE EMP_PROG ADD CONSTRAINT PK_EMP_PROG PRIMARY KEY ( CB_CODIGO, CU_CODIGO )
GO

/* Tabla T_ART_80: Vigencias de Tablas Num�ricas */
/* Patch 385 # Seq: 90 Agregado */
CREATE TABLE T_ART_80 (NU_CODIGO STATUS,
       TI_INICIO        Fecha,
       TI_DESCRIP       Descripcion )
GO

/* PK T_ART_80: Llave Primaria */
/* Patch 385 # Seq: 91 Agregado */
ALTER TABLE T_ART_80 ADD CONSTRAINT PK_T_ART_80 PRIMARY KEY ( NU_CODIGO, TI_INICIO )
GO

/* Poblar T_ART_80 */
/* Patch 385 # Seq: 93 Agregado */
insert into T_ART_80 select NU_CODIGO, '01/01/2004', 'Patch 385' from NUMERICA
GO

/* Tabla ART_80_TEMP: Tablas Num�ricas ( Transitoria - No Agregar Al Diccionario) */
/* Patch 385 # Seq: 94 Agregado */
CREATE TABLE ART_80_TEMP (
       NU_CODIGO            Status,
       TI_INICIO            Fecha,
       A80_LI               Pesos,
       A80_CUOTA            Pesos,
       A80_TASA             Tasa )
GO

/* Poblar ART_80_TEMP */
/* Patch 385 # Seq: 95 Agregado */
insert into ART_80_TEMP select NU_CODIGO, '01/01/2004', A80_LI, A80_CUOTA, A80_TASA from ART_80
GO

/* Borrar Llave For�nea ART_80 hacia NUMERICA */
/* Patch 385 # Seq: 96 Agregado */
drop constraint FK_Art_80_Numerica
GO

/* Borrar Llave Primaria ART_80 */
/* Patch 385 # Seq: 97 Agregado */
drop constraint PK_ART_80
GO

/* Borrar Tabla ART_80 */
/* Patch 385 # Seq: 98 Agregado */
drop table ART_80
GO

/* Crear Tabla ART_80: Tablas Num�ricas ( Transitoria - Ya Debe Estar En El Diccionario) */
/* Patch 385 # Seq: 99 Agregado */
CREATE TABLE ART_80 (
       NU_CODIGO            Status,
       TI_INICIO            Fecha,
       A80_LI               Pesos,
       A80_CUOTA            Pesos,
       A80_TASA             Tasa )
GO

/* PK ART_80: Llave Primaria */
/* Patch 385 # Seq: 100 Agregado */
ALTER TABLE ART_80 ADD CONSTRAINT PK_ART_80 PRIMARY KEY (NU_CODIGO, TI_INICIO, A80_LI)
GO

/* Poblar ART_80 a partir de ART_80_TEMP */
/* Patch 385 # Seq: 102 Agregado */
insert into ART_80 select NU_CODIGO, TI_INICIO, A80_LI, A80_CUOTA, A80_TASA from ART_80_TEMP
GO

/* Borrar Tabla Transitoria ART_80_TEMP */
/* Patch 385 # Seq: 103 Agregado */
drop table ART_80_TEMP
GO

/* Tabla ACCREGLA: Regla para Control de Acceso */
/* Patch 385 # Seq: 132 Agregado */
CREATE TABLE ACCREGLA (
       AE_CODIGO            FolioChico,  /* C�digo De La Regla */
       AE_TIPO              Status,      /* Tipo ( eAccesoRegla = ( arInformativo, arEvalEntrada, arEvalSalida, arEvaluacion ) ) */
       AE_LETRERO           DescLarga,   /* INF: T�tulo EVAL: Mensaje */
       AE_FORMULA           Formula,     /* F�rmula ( INF: Regresa Texto EVAL: Regresa Booleano ) */
       AE_QUERY             Condicion,   /* Condici�n -> QUERYS.QU_CODIGO */
       AE_SEVERI            Status,      /* Severidad ( Usar eTipoBitacora ) */
       AE_ACTIVO            Booleano,    /* Activo / Inactivo */
       US_CODIGO            Usuario      /* Ultimo Usuario En Modificar Regla */
)
GO

/* PK ACCREGLA: Llave Primaria de la Tabla ACCREGLA */
/* Patch 385 # Seq: 133 Agregado */
ALTER TABLE ACCREGLA ADD CONSTRAINT PK_ACCREGLA PRIMARY KEY (AE_CODIGO)
GO

/* Tabla ACCESLOG: Bit�cora De Entrada / Salida En Caseta */
/* Patch 385 # Seq: 134 Agregado */
CREATE TABLE ACCESLOG (
       CB_CODIGO            NumeroEmpleado, /* Empleado */
       AL_FECHA             Fecha,          /* Fecha */
       AL_HORA              Hora,           /* Hora */
       AL_ENTRADA           Booleano,       /* Si es Entrada */
       AL_OK_SIST           Booleano,       /* OK Del Sistema */
       AL_OK_MAN            Booleano,       /* OK Del Guardia */
       AL_OK_FEC            Fecha,          /* Fecha Del OK Del Guardia */
       AL_OK_HOR            Hora,           /* Hora Del OK Del Guardia */
       AL_COMENT            Observaciones,  /* Comentarios */
       AE_CODIGO            Codigo,         /* Regla Que Rechaza */
       AL_CASETA            Codigo,         /* Caseta Usada */
       US_CODIGO            Usuario         /* Usuario */
)
GO

/* PK ACCESLOG: Llave Primaria de la Tabla ACCESLOG */
/* Patch 385 # Seq: 135 Agregado */
ALTER TABLE ACCESLOG ADD CONSTRAINT PK_ACCESLOG PRIMARY KEY (CB_CODIGO, AL_FECHA, AL_HORA)
GO

/* Tabla CLASITMP: Cambios Temporales de Clasificaci�n de AUSENCIA */
/* Patch 385 # Seq: 136 Agregado */
create table CLASITMP( 	                     /* Cambios Temporales De Clasificaci�n */
       CB_CODIGO 	    NumeroEmpleado,  /* Empleado */
       AU_FECHA		    Fecha,	     /* Fecha */
       CB_CLASIFI           Codigo,	     /* Clasificaci�n */
       CB_TURNO             Codigo,	     /* Turno */
       CB_PUESTO            Codigo,	     /* Puesto */
       CB_NIVEL1            Codigo,	     /* Nivel 1 */
       CB_NIVEL2            Codigo, 	     /* Nivel 2 */
       CB_NIVEL3            Codigo, 	     /* Nivel 3 */
       CB_NIVEL4            Codigo, 	     /* Nivel 4 */
       CB_NIVEL5            Codigo, 	     /* Nivel 5 */
       CB_NIVEL6            Codigo, 	     /* Nivel 6 */
       CB_NIVEL7            Codigo, 	     /* Nivel 7 */
       CB_NIVEL8            Codigo, 	     /* Nivel 8 */
       CB_NIVEL9            Codigo, 	     /* Nivel 9 */
       US_CODIGO	    Usuario,	     /* Usuario Quien Propone Cambio  */
       CT_FECHA		    Fecha,	     /* Fecha De Captura */
       US_COD_OK	    Usuario,	     /* Usuario Quien Aprueba */
       CT_FEC_OK	    Fecha	     /* Fecha De Aprobaci�n */
)
GO

/* PK CLASITMP: Llave Primaria de la Tabla CLASITMP */
/* Patch 385 # Seq: 137 Agregado */
ALTER TABLE CLASITMP ADD CONSTRAINT PK_CLASITMP PRIMARY KEY (CB_CODIGO, AU_FECHA)
GO

/***********************************/
/******* Views *********************/
/***********************************/

/* Modificar CUR_PROG */
/* Patch 385 # Seq: 76 Agregado */
alter view CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CU_CODIGO,
  KC_FEC_PRO,
  KC_EVALUA,
  KC_FEC_TOM,
  KC_HORAS,
  CU_HORAS,
  EN_OPCIONA,
  EN_LISTA,
  MA_CODIGO,
  KC_PROXIMO,
  KC_REVISIO,
  CU_REVISIO,
  CP_INDIVID ) as
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  DATEADD( DAY, E.EN_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CURSO.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  CURSO.MA_CODIGO,
  ( select MIN( CALCURSO.CC_FECHA ) from CALCURSO where ( CALCURSO.CU_CODIGO = E.CU_CODIGO ) and ( CALCURSO.CC_FECHA >= cast( cast( GetDate() as Integer ) as Datetime ))) KC_PROXIMO,
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_INDIVID
from COLABORA C
join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' )
union
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  DATEADD( DAY, EP.EP_DIAS, C.CB_FEC_ING ) KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA,
  CURSO.MA_CODIGO,
  ( select MIN( CALCURSO.CC_FECHA ) from CALCURSO where ( CALCURSO.CU_CODIGO = EP.CU_CODIGO ) and ( CALCURSO.CC_FECHA >= cast( cast( GetDate() as Integer ) as Datetime ))) KC_PROXIMO,
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_INDIVID
from COLABORA C
join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = EP.CU_CODIGO )
left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' )
GO

/* Crear VIEW AHO_PRE para ser invocado al calcular N�mina */
/* Patch 385 # Seq: 89 Agregado */
create view AHO_PRE( CB_CODIGO, FECHA, PRIORIDAD, TIPO, PR_TIPO, PR_REFEREN, PR_MONTO, PR_SALDO, PR_FORMULA )
as
select
A.CB_CODIGO CB_CODIGO,
A.AH_FECHA FECHA,
TA.TB_PRIORID PRIORIDAD,
CAST ( ( 0 ) as INTEGER ) TIPO,
A.AH_TIPO PR_TIPO,
CAST( ( '' ) as VARCHAR( 8 ) ) PR_REFEREN,
CAST ( ( 0 ) as NUMERIC( 15,2 ) ) PR_MONTO,
CAST ( ( 0 ) as NUMERIC( 15,2 ) ) PR_SALDO,
A.AH_FORMULA PR_FORMULA
from AHORRO A
left outer join TAHORRO TA on ( TA.TB_CODIGO = A.AH_TIPO )
where ( A.AH_STATUS = 0 )
union
select
P.CB_CODIGO CB_CODIGO,
P.PR_FECHA FECHA,
TP.TB_PRIORID PRIORIDAD,
CAST ( ( 1 ) as INTEGER ) TIPO,
P.PR_TIPO,
P.PR_REFEREN,
P.PR_MONTO,
P.PR_SALDO,
P.PR_FORMULA
from PRESTAMO P
left outer join TPRESTA TP on ( TP.TB_CODIGO = P.PR_TIPO )
where ( P.PR_STATUS = 0 )

/***************************/
/**** Llaves For�neas ******/
/***************************/

/* Llave for�nea de COMP_FAM hacia FAM_PTO */
/* Patch 385 Seq: 47 Agregado */
alter table COMP_FAM
       add constraint FK_COMP_FAM_FAM_PTO
       foreign key (FP_CODIGO)
       references FAM_PTO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de COMP_FAM hacia COMPETEN */
/* Patch 385 Seq: 48 Agregado */
alter table COMP_FAM
       add constraint FK_COMP_FAM_COMPETEN
       foreign key (CM_CODIGO)
       references COMPETEN
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de NIV_DIME hacia NIV_PTO */
/* Patch 385 Seq: 49 Agregado */
alter table NIV_DIME
       add constraint FK_NIV_DIME_NIV_PTO
       foreign key (NP_CODIGO)
       references NIV_PTO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de NIV_DIME hacia DIMENSIO */
/* Patch 385 Seq: 50 Agregado */
alter table NIV_DIME
       add constraint FK_NIV_DIME_DIMENSIO
       foreign key (DM_CODIGO)
       references DIMENSIO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de PTO_DIME hacia DIMENSIO */
/* Patch 385 Seq: 51 Agregado */
alter table PTO_DIME
       add constraint FK_PTO_DIME_DIMENSIO
       foreign key (DM_CODIGO)
       references DIMENSIO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de PTO_DIME hacia PUESTO */
/* Patch 385 Seq: 52 Agregado */
alter table PTO_DIME
       add constraint FK_PTO_DIME_PUESTO
       foreign key (PU_CODIGO)
       references PUESTO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de COMP_CAL hacia CALIFICA */
/* Patch 385 Seq: 53 Agregado */
alter table COMP_CAL
       add constraint FK_COMP_CAL_CALIFICA
       foreign key (CA_CODIGO)
       references CALIFICA
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de COMP_CAL hacia COMPETEN */
/* Patch 385 Seq: 54 Agregado */
alter table COMP_CAL
       add constraint FK_COMP_CAL_COMPETEN
       foreign key (CM_CODIGO)
       references COMPETEN
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de COMPETEN hacia TCOMPETE */
/* Patch 385 Seq: 55 Agregado */
alter table COMPETEN
       add constraint FK_COMPETEN_TCOMPETE
       foreign key (TC_CODIGO)
       references TCOMPETE
       on delete NO ACTION
       on update CASCADE
GO

/* Llave for�nea de COMP_MAP hacia COMPETEN */
/* Patch 385 Seq: 56 Agregado */
alter table COMP_MAP
       add constraint FK_COMP_MAP_COMPETEN
       foreign key (CM_CODIGO)
       references COMPETEN
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de COMP_MAP hacia ACCION */
/* Patch 385 Seq: 57 Agregado */
alter table COMP_MAP
       add constraint FK_COMP_MAP_ACCION
       foreign key (AN_CODIGO)
       references ACCION
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de COMP_PTO hacia COMPETEN */
/* Patch 385 Seq: 58 Agregado */
alter table COMP_PTO
       add constraint FK_COMP_PTO_COMPETEN
       foreign key (CM_CODIGO)
       references COMPETEN
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de COMP_PTO hacia PUESTO */
/* Patch 385 Seq: 59 Agregado */
alter table COMP_PTO
       add constraint FK_COMP_PTO_PUESTO
       foreign key (PU_CODIGO)
       references PUESTO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de EMP_COMP hacia COMPETEN */
/* Patch 385 Seq: 60 Agregado */
alter table EMP_COMP
       add constraint FK_EMP_COMP_COMPETEN
       foreign key (CM_CODIGO)
       references COMPETEN
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de EMP_PLAN hacia ACCION */
/* Patch 385 Seq: 61 Agregado */
alter table EMP_PLAN
       add constraint FK_EMP_PLAN_ACCION
       foreign key (AN_CODIGO)
       references ACCION
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de EMP_PLAN hacia COLABORA */
/* Patch 385 Seq: 62 Agregado */
alter table EMP_PLAN
       add constraint FK_EMP_PLAN_COLABORA
       foreign key (CB_CODIGO)
       references COLABORA
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de EMP_COMP hacia COLABORA */
/* Patch 385 Seq: 63 Agregado */
alter table EMP_COMP
       add constraint FK_EMP_COMP_COLABORA
       foreign key (CB_CODIGO)
       references COLABORA
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de ACCION hacia CURSO */
/* Patch 385 Seq: 64 Agregado */
alter table ACCION
       add constraint FK_ACCION_CURSO
       foreign key (CU_CODIGO)
       references CURSO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de SESION hacia CURSO */
/* Patch 385 Seq: 77 Agregado */
ALTER TABLE SESION
       ADD CONSTRAINT FK_SESION_CURSO
       FOREIGN KEY (CU_CODIGO)
       REFERENCES CURSO
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Llave for�nea de MISREPOR hacia REPORTE */
/* Patch 385 Seq: 78 Agregado */
ALTER TABLE MISREPOR
       ADD CONSTRAINT FK_MISREPOR_REPORTE
       FOREIGN KEY (RE_CODIGO)
       REFERENCES REPORTE
       ON DELETE CASCADE
       ON UPDATE CASCADE
GO

/* Llave for�nea de EMP_PROG hacia COLABORA */
/* Patch 385 Seq: 84 Agregado */
alter table EMP_PROG
       add constraint FK_EMP_PROG_COLABORA
       foreign key (CB_CODIGO)
       references COLABORA
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de EMP_PROG hacia CURSO */
/* Patch 385 Seq: 85 Agregado */
alter table EMP_PROG
       add constraint FK_EMP_PROG_CURSO
       foreign key (CU_CODIGO)
       references CURSO
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de T_ART_80 hacia NUMERICA */
/* Patch 385 Seq: 92 Agregado */
alter table T_ART_80
       add constraint FK_T_ART_80_NUMERICA
       foreign key (NU_CODIGO)
       references NUMERICA
       on delete CASCADE
       on update CASCADE
GO

/* Llave for�nea de ART_80 hacia T_ART_80 */
/* Patch 385 Seq: 101 Agregado */
ALTER TABLE ART_80
       ADD CONSTRAINT FK_ART_80_T_ART_80
       FOREIGN KEY (NU_CODIGO, TI_INICIO)
       REFERENCES T_ART_80
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Llave for�nea de ACCESLOG hacia COLABORA */
/* Patch 385 Seq: 138 Agregado */
ALTER TABLE ACCESLOG
       ADD CONSTRAINT FK_ACCESLOG_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/* Llave for�nea de CLASITMP hacia COLABORA */
/* Patch 385 Seq: 139 Agregado */
ALTER TABLE CLASITMP
       ADD CONSTRAINT FK_CLASITMP_COLABORA
       FOREIGN KEY (CB_CODIGO)
       REFERENCES COLABORA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

/***************************/
/******** Triggers *********/
/***************************/

/* Patch 385 # Seq: 44 Borrado */
drop trigger TU_AUSENCIA;

/* Patch 385 # Seq: 60 Agregado */
CREATE TRIGGER TU_FAM_PTO ON FAM_PTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( FP_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = FP_CODIGO from Inserted;
    select @OldCodigo = FP_CODIGO from Deleted;

	update PUESTO set PUESTO.FP_CODIGO = @NewCodigo
    where ( PUESTO.FP_CODIGO = @OldCodigo );
  END
END
GO

/* Patch 385 # Seq: 61 Agregado */
CREATE TRIGGER TU_NIV_PTO ON NIV_PTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( NP_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = NP_CODIGO from Inserted;
    select @OldCodigo = NP_CODIGO from Deleted;

	update PUESTO set PUESTO.NP_CODIGO = @NewCodigo
    where ( PUESTO.NP_CODIGO = @OldCodigo );
  END
END
GO

/* Patch 385 # Seq: 62 Agregado */
CREATE TRIGGER TU_COMP_CAL ON COMP_CAL AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( CM_CODIGO ) or UPDATE( CA_CODIGO )
  BEGIN
	declare @NewCM_CODIGO Codigo, @OldCM_CODIGO Codigo;
	declare @NewCA_CODIGO Codigo, @OldCA_CODIGO Codigo;

	select @NewCM_CODIGO = CM_CODIGO from Inserted;
    select @OldCM_CODIGO = CM_CODIGO from Deleted;
	select @NewCA_CODIGO = CA_CODIGO from Inserted;
    select @OldCA_CODIGO = CA_CODIGO from Deleted;

    update EMP_COMP set EMP_COMP.CM_CODIGO = @NewCM_CODIGO,
                        EMP_COMP.CA_CODIGO = @NewCA_CODIGO
    where ( EMP_COMP.CM_CODIGO = @OldCM_CODIGO ) and
          ( EMP_COMP.CA_CODIGO = @OldCA_CODIGO );
    update COMP_PTO set COMP_PTO.CM_CODIGO = @NewCM_CODIGO,
                        COMP_PTO.CA_CODIGO = @NewCA_CODIGO
    where ( COMP_PTO.CM_CODIGO = @OldCM_CODIGO ) and
          ( COMP_PTO.CA_CODIGO = @OldCA_CODIGO );
  END
END
GO

/* Patch 385 # Seq: 65 Agregado */
CREATE TRIGGER TD_SESION ON SESION AFTER DELETE AS
BEGIN
  SET NOCOUNT ON;
  declare @OldFolio FolioGrande;
  select @OldFolio = SE_FOLIO from Deleted;
  delete from KARCURSO where ( KARCURSO.SE_FOLIO = @OldFolio );
END
GO

/* Patch 385 # Seq: 66 Agregado */
CREATE TRIGGER TU_SESION ON SESION AFTER UPDATE AS
BEGIN
     IF ( UPDATE( SE_FOLIO ) OR
        UPDATE( CU_CODIGO ) OR
        UPDATE( MA_CODIGO ) OR
        UPDATE( SE_REVISIO ) OR
        UPDATE( SE_FEC_INI ) OR
        UPDATE( SE_FEC_FIN ) )
     BEGIN
          declare @OldFolio FolioGrande;
          declare @NewFolio FolioGrande;
          declare @NewCU_CODIGO Codigo;
          declare @NewMA_CODIGO Codigo;
          declare @NewSE_REVISIO NombreCampo;
          declare @NewSE_FEC_INI Fecha;
          declare @NewSE_FEC_FIN Fecha;

          select @OldFolio = SE_FOLIO from Deleted;
          select @NewFolio = SE_FOLIO,
                 @NewCU_CODIGO = CU_CODIGO,
                 @NewMA_CODIGO = MA_CODIGO,
                 @NewSE_REVISIO = SE_REVISIO,
                 @NewSE_FEC_INI = SE_FEC_INI,
                 @NewSE_FEC_FIN = SE_FEC_FIN
          from Inserted;

          update KARCURSO set
                 KARCURSO.SE_FOLIO = @NewFolio,
                 KARCURSO.CU_CODIGO = @NewCU_CODIGO,
                 KARCURSO.MA_CODIGO = @NewMA_CODIGO,
                 KARCURSO.KC_REVISIO = @NewSE_REVISIO,
                 KARCURSO.KC_FEC_TOM = @NewSE_FEC_INI,
                 KARCURSO.KC_FEC_FIN = @NewSE_FEC_FIN
          where ( KARCURSO.SE_FOLIO = @OldFolio );
     END
END
GO
/* Patch 385 # Seq: 67 Agregado */
CREATE TRIGGER TU_CONCEPTO ON CONCEPTO AFTER UPDATE AS
BEGIN
     IF UPDATE( CO_NUMERO )
     BEGIN
          declare @OldCO_NUMERO Concepto;
          declare @NewCO_NUMERO Concepto;
          select @OldCO_NUMERO = CO_NUMERO from Deleted;
          select @NewCO_NUMERO = CO_NUMERO from Inserted;

          update TAHORRO set TAHORRO.TB_CONCEPT = @NewCO_NUMERO
          where ( TAHORRO.TB_CONCEPT = @OldCO_NUMERO );
          update TAHORRO set TAHORRO.TB_RELATIV = @NewCO_NUMERO
          where ( TAHORRO.TB_RELATIV = @OldCO_NUMERO );
          update TPRESTA set TPRESTA.TB_CONCEPT = @NewCO_NUMERO
          where (  TPRESTA.TB_CONCEPT = @OldCO_NUMERO );
          update TPRESTA set TPRESTA.TB_RELATIV = @NewCO_NUMERO
          where ( TPRESTA.TB_RELATIV = @OldCO_NUMERO );
     END
END
GO

/* Triggers de MAESTRO modificados */
/* Patch 385 # Seq: 28 Modificado */
ALTER TRIGGER TU_MAESTRO ON MAESTRO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( MA_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = MA_CODIGO from   Inserted;
        select @OldCodigo = MA_CODIGO from   Deleted;

	UPDATE CURSO    SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
	UPDATE KARCURSO SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
	UPDATE SESION 	SET MA_CODIGO = @NewCodigo WHERE MA_CODIGO = @OldCodigo;
  END
END
GO

/* Trigger sobre ACCREGLA Agregado */
/* Patch 386 # Seq: 68 Agregado */
CREATE TRIGGER TU_ACCREGLA ON ACCREGLA AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( AE_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;
	select @NewCodigo = AE_CODIGO from Inserted;
        select @OldCodigo = AE_CODIGO from Deleted;
        update ACCESLOG set AE_CODIGO = @NewCodigo where ( AE_CODIGO = @OldCodigo );
  END
END
GO

/* Trigger sobre QUERYS modificado */
/* Patch 386 # Seq: 27 Modificado */
ALTER TRIGGER TU_QUERYS ON QUERYS AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( QU_CODIGO )
  BEGIN
	declare @NewCodigo Condicion, @OldCodigo Condicion;

	select @NewCodigo = QU_CODIGO from   Inserted;
        select @OldCodigo = QU_CODIGO from   Deleted;

	IF ( @OldCodigo <> '' ) AND ( @NewCodigo <> '' )
	BEGIN
		UPDATE CONCEPTO SET CO_QUERY  = @NewCodigo WHERE CO_QUERY = @OldCodigo;
		UPDATE EVENTO   SET EV_QUERY  = @NewCodigo WHERE EV_QUERY = @OldCodigo;
		UPDATE REPORTE  SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
		UPDATE FOLIO    SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
		UPDATE CAFREGLA SET CL_QUERY  = @NewCodigo WHERE CL_QUERY = @OldCodigo;
		UPDATE ACCREGLA SET QU_CODIGO = @NewCodigo WHERE QU_CODIGO = @OldCodigo;
	END
  END
END
GO

/* Trigger sobre NIVEL1 modificado */
/* Patch 385 # Seq: 1 Modificado */
ALTER TRIGGER TU_NIVEL1 ON NIVEL1 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL1 = @NewCodigo WHERE PU_NIVEL1 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL1 = @NewCodigo WHERE CB_NIVEL1 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL2 modificado */
/* Patch 385 # Seq: 2 Modificado */
ALTER TRIGGER TU_NIVEL2 ON NIVEL2 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL2 = @NewCodigo WHERE PU_NIVEL2 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL2 = @NewCodigo WHERE CB_NIVEL2 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL3 modificado */
/* Patch 385 # Seq: 3 Modificado */
ALTER TRIGGER TU_NIVEL3 ON NIVEL3 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL3 = @NewCodigo WHERE PU_NIVEL3 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL3 = @NewCodigo WHERE CB_NIVEL3 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL4 modificado */
/* Patch 385 # Seq: 4 Modificado */
ALTER TRIGGER TU_NIVEL4 ON NIVEL4 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL4 = @NewCodigo WHERE PU_NIVEL4 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL4 = @NewCodigo WHERE CB_NIVEL4 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL5 modificado */
/* Patch 385 # Seq: 5 Modificado */
ALTER TRIGGER TU_NIVEL5 ON NIVEL5 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL5 = @NewCodigo WHERE PU_NIVEL5 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL5 = @NewCodigo WHERE CB_NIVEL5 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL6 modificado */
/* Patch 385 # Seq: 6 Modificado */
ALTER TRIGGER TU_NIVEL6 ON NIVEL6 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL6 = @NewCodigo WHERE PU_NIVEL6 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL6 = @NewCodigo WHERE CB_NIVEL6 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL7 modificado */
/* Patch 385 # Seq: 7 Modificado */
ALTER TRIGGER TU_NIVEL7 ON NIVEL7 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL7 = @NewCodigo WHERE PU_NIVEL7 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL7 = @NewCodigo WHERE CB_NIVEL7 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL8 modificado */
/* Patch 385 # Seq: 8 Modificado */
ALTER TRIGGER TU_NIVEL8 ON NIVEL8 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL8 = @NewCodigo WHERE PU_NIVEL8 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL8 = @NewCodigo WHERE CB_NIVEL8 = @OldCodigo;
  END
END
GO

/* Trigger sobre NIVEL9 modificado */
/* Patch 385 # Seq: 9 Modificado */
ALTER TRIGGER TU_NIVEL9 ON NIVEL9 AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARDEX	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE NOMINA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE AUSENCIA	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE KARCURSO	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
	UPDATE PUESTO	SET PU_NIVEL9 = @NewCodigo WHERE PU_NIVEL9 = @OldCodigo;
	UPDATE CLASITMP	SET CB_NIVEL9 = @NewCodigo WHERE CB_NIVEL9 = @OldCodigo;
  END
END
GO

/* Trigger sobre PUESTO modificado */
/* Patch 385 # Seq: 30 Modificado */
ALTER TRIGGER TU_PUESTO ON PUESTO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( PU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = PU_CODIGO from   Inserted;
        select @OldCodigo = PU_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE KARDEX	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE NOMINA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE KARCURSO	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
	UPDATE PUESTO	SET PU_REPORTA = @NewCodigo WHERE PU_REPORTA = @OldCodigo;
	UPDATE CLASITMP	SET CB_PUESTO = @NewCodigo WHERE CB_PUESTO = @OldCodigo;
  END
END
GO

/* Trigger sobre TURNO modificado */
/* Patch 385 # Seq: 31 Modificado */
ALTER TRIGGER TU_TURNO ON TURNO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TU_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TU_CODIGO from   Inserted;
        select @OldCodigo = TU_CODIGO from   Deleted;

	UPDATE COLABORA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARDEX	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE NOMINA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE AUSENCIA	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE KARCURSO	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
	UPDATE PUESTO	SET PU_TURNO = @NewCodigo WHERE PU_TURNO = @OldCodigo;
	UPDATE CLASITMP	SET CB_TURNO = @NewCodigo WHERE CB_TURNO = @OldCodigo;
  END
END
GO

/* Trigger sobre CLASIFI modificado */
/* Patch 385 # Seq: 20 Modificado */
ALTER TRIGGER TU_CLASIFI ON CLASIFI AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( TB_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = TB_CODIGO from   Inserted;
        select @OldCodigo = TB_CODIGO from   Deleted;

	UPDATE PUESTO   SET PU_CLASIFI = @NewCodigo WHERE PU_CLASIFI = @OldCodigo;
	UPDATE COLABORA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE KARDEX	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE NOMINA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE AUSENCIA	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE KARCURSO	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
	UPDATE CLASITMP	SET CB_CLASIFI = @NewCodigo WHERE CB_CLASIFI = @OldCodigo;
  END
END
GO

/*************************************/
/******* Stored Procedures ***********/
/*************************************/

/* Patch 385: Cambiar c�lculo de faltas */
/* Patch 385: # Seq: 37 Modificado */
ALTER procedure DBO.PREPARA_CALENDARIO(
@USUARIO SMALLINT,
@EMPLEADO INTEGER,
@YEAR SMALLINT,
@MONTH SMALLINT,
@FECHA_MES DATETIME,
@FECHA_INI DATETIME,
@FECHA_FIN DATETIME )
as
begin
     SET NOCOUNT ON
     declare @CUANTOS Numeric( 15, 2 )
     declare @Fecha_Min DATETIME;
     declare @Fecha_Max DATETIME;
     declare @FechaActual DATETIME;
     declare @Posicion SMALLINT;
     declare @Fecha01 DATETIME;
     declare @Fecha02 DATETIME;
     declare @Fecha03 DATETIME;
     declare @Fecha04 DATETIME;
     declare @Fecha05 DATETIME;
     declare @Fecha06 DATETIME;
     declare @Fecha07 DATETIME;
     declare @Fecha08 DATETIME;
     declare @Fecha09 DATETIME;
     declare @Fecha10 DATETIME;
     declare @Fecha11 DATETIME;
     declare @Fecha12 DATETIME;
     declare @Fecha13 DATETIME;
     declare @Fecha14 DATETIME;
     declare @Fecha15 DATETIME;
     declare @Fecha16 DATETIME;
     declare @Fecha17 DATETIME;
     declare @Fecha18 DATETIME;
     declare @Fecha19 DATETIME;
     declare @Fecha20 DATETIME;
     declare @Fecha21 DATETIME;
     declare @Fecha22 DATETIME;
     declare @Fecha23 DATETIME;
     declare @Fecha24 DATETIME;
     declare @Fecha25 DATETIME;
     declare @Fecha26 DATETIME;
     declare @Fecha27 DATETIME;
     declare @Fecha28 DATETIME;
     declare @Fecha29 DATETIME;
     declare @Fecha30 DATETIME;
     declare @Fecha31 DATETIME;
     declare @STATUS SMALLINT;
     declare @TIPODIA SMALLINT;
     declare @HORAS   NUMERIC( 15, 2 );
     declare @EXTRAS  NUMERIC( 15, 2 );
     declare @DOBLES  NUMERIC( 15, 2 );
     declare @TARDES  NUMERIC( 15, 2 );
     declare @DES_TRA NUMERIC( 15, 2 );
     declare @PER_CG  NUMERIC( 15, 2 );
     declare @PER_SG  NUMERIC( 15, 2 );
     declare @TotHabil   SMALLINT;
     declare @TotSabado  SMALLINT;
     declare @TotDescan  SMALLINT;
     declare @TotNormal  SMALLINT;
     declare @TotIncapa  SMALLINT;
     declare @TotVaca    SMALLINT;
     declare @TotPer_CG  SMALLINT;
     declare @TotPer_SG  SMALLINT;
     declare @TotPer_FJ  SMALLINT;
     declare @TotSuspen  SMALLINT;
     declare @TotPer_OT  SMALLINT;
     declare @TotFestivo SMALLINT;
     declare @TotNo_Trab SMALLINT;
     declare @TotTrabajo SMALLINT;
     declare @TotFaltas  SMALLINT;
     declare @TotRetardo SMALLINT;
     declare @TotHoras   NUMERIC( 15, 2 );
     declare @TotExtras  NUMERIC( 15, 2 );
     declare @TotDobles  NUMERIC( 15, 2 );
     declare @TotDes_Tra NUMERIC( 15, 2 );
     declare @TotHor_CG  NUMERIC( 15, 2 );
     declare @TotHor_SG  NUMERIC( 15, 2 );
     declare @TotTardes  NUMERIC( 15, 2 );
     declare Temporal CURSOR for
        select A.AU_FECHA,
               A.AU_STATUS,
               A.AU_TIPODIA,
               A.AU_HORAS,
               A.AU_EXTRAS,
               A.AU_DOBLES,
               A.AU_TARDES,
               A.AU_DES_TRA,
               A.AU_PER_CG,
               A.AU_PER_SG
        from DBO.AUSENCIA as A where
        ( A.CB_CODIGO = @Empleado ) and
        ( A.AU_FECHA between @Fecha_Ini and @Fecha_Fin )
        order by A.AU_FECHA
     set @Cuantos = 0;
     set @Fecha01 = '12/30/1899';
     set @Fecha02 = @Fecha01;
     set @Fecha03 = @Fecha01;
     set @Fecha04 = @Fecha01;
     set @Fecha05 = @Fecha01;
     set @Fecha06 = @Fecha01;
     set @Fecha07 = @Fecha01;
     set @Fecha08 = @Fecha01;
     set @Fecha09 = @Fecha01;
     set @Fecha10 = @Fecha01;
     set @Fecha11 = @Fecha01;
     set @Fecha12 = @Fecha01;
     set @Fecha13 = @Fecha01;
     set @Fecha14 = @Fecha01;
     set @Fecha15 = @Fecha01;
     set @Fecha16 = @Fecha01;
     set @Fecha17 = @Fecha01;
     set @Fecha18 = @Fecha01;
     set @Fecha19 = @Fecha01;
     set @Fecha20 = @Fecha01;
     set @Fecha21 = @Fecha01;
     set @Fecha22 = @Fecha01;
     set @Fecha23 = @Fecha01;
     set @Fecha24 = @Fecha01;
     set @Fecha25 = @Fecha01;
     set @Fecha26 = @Fecha01;
     set @Fecha27 = @Fecha01;
     set @Fecha28 = @Fecha01;
     set @Fecha29 = @Fecha01;
     set @Fecha30 = @Fecha01;
     set @Fecha31 = @Fecha01;
     set @TotHabil   = 0;
     set @TotSabado  = 0;
     set @TotDescan  = 0;
     set @TotNormal  = 0;
     set @TotIncapa  = 0;
     set @TotVaca    = 0;
     set @TotPer_CG  = 0;
     set @TotPer_SG  = 0;
     set @TotPer_FJ  = 0;
     set @TotSuspen  = 0;
     set @TotPer_OT  = 0;
     set @TotFestivo = 0;
     set @TotNo_Trab = 0;
     set @TotTrabajo = 0;
     set @TotFaltas  = 0;
     set @TotRetardo = 0;
     set @TotHoras   = 0;
     set @TotExtras  = 0;
     set @TotDobles  = 0;
     set @TotDes_Tra = 0;
     set @TotHor_CG  = 0;
     set @TotHor_SG  = 0;
     set @TotTardes  = 0;
     Open Temporal;
     fetch NEXT from Temporal
     into @FechaActual,
          @Status,
          @TipoDia,
          @Horas,
          @Extras,
          @Dobles,
          @Tardes,
          @Des_Tra,
          @Per_CG,
          @Per_SG;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @TipoDia = 9 )
             set @TotNo_Trab = @TotNo_Trab + 1;
          else
          begin
               if ( @Cuantos = 0 )
                  set @Fecha_Min = @FechaActual;
               set @Fecha_Max = @FechaActual;
               set @Cuantos = @Cuantos + 1;
               if ( @Status = 0 )
                  set @TotHabil = @TotHabil + 1
               else
                   if ( @Status = 1 )
                      set @TotSabado = @TotSabado + 1;
                   else
                       set @TotDescan = @TotDescan + 1;
               if ( @TipoDia = 0 ) set @TotNormal = @TotNormal + 1
               else
               if ( @TipoDia = 1 ) set @TotIncapa = @TotIncapa + 1
               else
               if ( @TipoDia = 2 ) set @TotVaca = @TotVaca + 1
               else
               if ( @TipoDia = 3 ) set @TotPer_CG = @TotPer_CG + 1
               else
               if ( @TipoDia = 4 ) set @TotPer_SG = @TotPer_SG + 1
               else
               if ( @TipoDia = 5 ) set @TotPer_FJ = @TotPer_FJ + 1
               else
               if ( @TipoDia = 6 ) set @TotSuspen = @TotSuspen + 1
               else
               if ( @TipoDia = 7 ) set @TotPer_OT = @TotPer_OT + 1
               else
                   set @TotFestivo = @TotFestivo + 1;
               if ( @Horas > 0 )
                  set @TotTrabajo = @TotTrabajo + 1
               else
                   if ( ( @Status = 0 ) and ( @TipoDia = 0 ) )
                      set @TotFaltas = @TotFaltas + 1;
               if ( @Tardes > 0 )
                  set @TotRetardo = @TotRetardo + 1;
               set @TotHoras   = @TotHoras + @Horas;
               set @TotExtras  = @TotExtras + @Extras;
               set @TotDobles  = @TotDobles + @Dobles;
               set @TotDes_Tra = @TotDes_Tra + @Des_Tra;
               set @TotHor_CG  = @TotHor_CG + @Per_CG;
               set @TotHor_SG  = @TotHor_SG + @Per_SG;
               set @TotTardes  = @TotTardes + @Tardes;
          end
          set @Posicion = CONVERT( SmallInt, @FechaActual - @Fecha_Mes ) + 1;
          if ( @Posicion =  1 )
             set @Fecha01 = @FechaActual
          else
          if ( @Posicion =  2 )
             set @Fecha02 = @FechaActual
          else
          if ( @Posicion =  3 )
             set @Fecha03 = @FechaActual
          else
          if ( @Posicion =  4 )
             set @Fecha04 = @FechaActual
          else
          if ( @Posicion =  5 )
             set @Fecha05 = @FechaActual
          else
          if ( @Posicion =  6 )
             set @Fecha06 = @FechaActual
          else
          if ( @Posicion =  7 )
             set @Fecha07 = @FechaActual
          else
          if ( @Posicion =  8 )
             set @Fecha08 = @FechaActual
          else
          if ( @Posicion =  9 )
             set @Fecha09 = @FechaActual
          else
          if ( @Posicion = 10 )
             set @Fecha10 = @FechaActual
          else
          if ( @Posicion = 11 )
             set @Fecha11 = @FechaActual
          else
          if ( @Posicion = 12 )
             set @Fecha12 = @FechaActual
          else
          if ( @Posicion = 13 )
             set @Fecha13 = @FechaActual
          else
          if ( @Posicion = 14 )
             set @Fecha14 = @FechaActual
          else
          if ( @Posicion = 15 )
             set @Fecha15 = @FechaActual
          else
          if ( @Posicion = 16 )
             set @Fecha16 = @FechaActual
          else
          if ( @Posicion = 17 )
             set @Fecha17 = @FechaActual
          else
          if ( @Posicion = 18 )
             set @Fecha18 = @FechaActual
          else
          if ( @Posicion = 19 )
             set @Fecha19 = @FechaActual
          else
          if ( @Posicion = 20 )
             set @Fecha20 = @FechaActual
          else
          if ( @Posicion = 21 )
             set @Fecha21 = @FechaActual
          else
          if ( @Posicion = 22 )
             set @Fecha22 = @FechaActual
          else
          if ( @Posicion = 23 )
             set @Fecha23 = @FechaActual
          else
          if ( @Posicion = 24 )
             set @Fecha24 = @FechaActual
          else
          if ( @Posicion = 25 )
             set @Fecha25 = @FechaActual
          else
          if ( @Posicion = 26 )
             set @Fecha26 = @FechaActual
          else
          if ( @Posicion = 27 )
             set @Fecha27 = @FechaActual
          else
          if ( @Posicion = 28 )
             set @Fecha28 = @FechaActual
          else
          if ( @Posicion = 29 )
             set @Fecha29 = @FechaActual
          else
          if ( @Posicion = 30 )
             set @Fecha30 = @FechaActual
          else
          if ( @Posicion = 31 )
             set @Fecha31 = @FechaActual;
          fetch NEXT from Temporal
          into @FechaActual,
               @Status,
               @TipoDia,
               @Horas,
               @Extras,
               @Dobles,
               @Tardes,
               @Des_Tra,
               @Per_CG,
               @Per_SG;
     end;
	 close Temporal;
	 deallocate Temporal;
     if ( @Cuantos > 0 )
     begin
          insert into DBO.TMPCALEN
            ( US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES, TL_FEC_MIN, TL_FEC_MAX, TL_NUMERO,
              TL_FECHA01, TL_FECHA02, TL_FECHA03, TL_FECHA04, TL_FECHA05, TL_FECHA06, TL_FECHA07, TL_FECHA08, TL_FECHA09, TL_FECHA10,
              TL_FECHA11, TL_FECHA12, TL_FECHA13, TL_FECHA14, TL_FECHA15, TL_FECHA16, TL_FECHA17, TL_FECHA18, TL_FECHA19, TL_FECHA20,
              TL_FECHA21, TL_FECHA22, TL_FECHA23, TL_FECHA24, TL_FECHA25, TL_FECHA26, TL_FECHA27, TL_FECHA28, TL_FECHA29, TL_FECHA30, TL_FECHA31,
              TL_HABIL, TL_SABADO, TL_DESCAN, TL_NORMAL, TL_INCAPA, TL_VACA, TL_PER_CG, TL_PER_SG, TL_PER_FJ, TL_SUSPEN, TL_PER_OT, TL_FESTIVO,
              TL_NO_TRAB, TL_TRABAJO, TL_FALTAS, TL_RETARDO, TL_HORAS, TL_EXTRAS, TL_DOBLES, TL_DES_TRA, TL_HOR_CG, TL_HOR_SG, TL_TARDES )
          values
            ( @USUARIO, @Empleado, @Year, @Month, @Fecha_Min, @Fecha_Max, @Cuantos,
              @Fecha01, @Fecha02, @Fecha03, @Fecha04, @Fecha05, @Fecha06, @Fecha07, @Fecha08, @Fecha09, @Fecha10,
              @Fecha11, @Fecha12, @Fecha13, @Fecha14, @Fecha15, @Fecha16, @Fecha17, @Fecha18, @Fecha19, @Fecha20,
              @Fecha21, @Fecha22, @Fecha23, @Fecha24, @Fecha25, @Fecha26, @Fecha27, @Fecha28, @Fecha29, @Fecha30, @Fecha31,
              @TotHabil, @TotSabado, @TotDescan, @TotNormal, @TotIncapa, @TotVaca, @TotPer_CG, @TotPer_SG, @TotPer_FJ, @TotSuspen, @TotPer_OT, @TotFestivo,
              @TotNo_Trab, @TotTrabajo, @TotFaltas, @TotRetardo, @TotHoras, @TotExtras, @TotDobles, @TotDes_Tra, @TotHor_CG, @TotHor_SG, @TotTardes );
     end;
end
GO

/* Patch 385: Cambiar c�lculo de faltas */
/* Patch 385: # Seq: 45 Modificado */
ALTER procedure DBO.PREPARA_CALENDARIO_HORAS(
@USUARIO SMALLINT,
@EMPLEADO INTEGER,
@YEAR SMALLINT,
@MONTH SMALLINT,
@FECHA_MES DATETIME,
@FECHA_INI DATETIME,
@FECHA_FIN DATETIME,
@CAMPO SMALLINT,
@PRIMERO SMALLINT )
as
begin
     SET NOCOUNT ON
     declare @Cuantos NUMERIC(15, 2);
     declare @Fecha_Min DATETIME;
     declare @Fecha_Max DATETIME;
     declare @FechaActual DATETIME;
     declare @Posicion SMALLINT;
     declare @Tipo CHAR(3);
     declare @AuTipo01 CHAR(3);
     declare @AuTipo02 CHAR(3);
     declare @AuTipo03 CHAR(3);
     declare @AuTipo04 CHAR(3);
     declare @AuTipo05 CHAR(3);
     declare @AuTipo06 CHAR(3);
     declare @AuTipo07 CHAR(3);
     declare @AuTipo08 CHAR(3);
     declare @AuTipo09 CHAR(3);
     declare @AuTipo10 CHAR(3);
     declare @AuTipo11 CHAR(3);
     declare @AuTipo12 CHAR(3);
     declare @AuTipo13 CHAR(3);
     declare @AuTipo14 CHAR(3);
     declare @AuTipo15 CHAR(3);
     declare @AuTipo16 CHAR(3);
     declare @AuTipo17 CHAR(3);
     declare @AuTipo18 CHAR(3);
     declare @AuTipo19 CHAR(3);
     declare @AuTipo20 CHAR(3);
     declare @AuTipo21 CHAR(3);
     declare @AuTipo22 CHAR(3);
     declare @AuTipo23 CHAR(3);
     declare @AuTipo24 CHAR(3);
     declare @AuTipo25 CHAR(3);
     declare @AuTipo26 CHAR(3);
     declare @AuTipo27 CHAR(3);
     declare @AuTipo28 CHAR(3);
     declare @AuTipo29 CHAR(3);
     declare @AuTipo30 CHAR(3);
     declare @AuTipo31 CHAR(3);
     declare @Monto NUMERIC( 15, 2 );
     declare @Monto01 NUMERIC( 15, 2 );
     declare @Monto02 NUMERIC( 15, 2 );
     declare @Monto03 NUMERIC( 15, 2 );
     declare @Monto04 NUMERIC( 15, 2 );
     declare @Monto05 NUMERIC( 15, 2 );
     declare @Monto06 NUMERIC( 15, 2 );
     declare @Monto07 NUMERIC( 15, 2 );
     declare @Monto08 NUMERIC( 15, 2 );
     declare @Monto09 NUMERIC( 15, 2 );
     declare @Monto10 NUMERIC( 15, 2 );
     declare @Monto11 NUMERIC( 15, 2 );
     declare @Monto12 NUMERIC( 15, 2 );
     declare @Monto13 NUMERIC( 15, 2 );
     declare @Monto14 NUMERIC( 15, 2 );
     declare @Monto15 NUMERIC( 15, 2 );
     declare @Monto16 NUMERIC( 15, 2 );
     declare @Monto17 NUMERIC( 15, 2 );
     declare @Monto18 NUMERIC( 15, 2 );
     declare @Monto19 NUMERIC( 15, 2 );
     declare @Monto20 NUMERIC( 15, 2 );
     declare @Monto21 NUMERIC( 15, 2 );
     declare @Monto22 NUMERIC( 15, 2 );
     declare @Monto23 NUMERIC( 15, 2 );
     declare @Monto24 NUMERIC( 15, 2 );
     declare @Monto25 NUMERIC( 15, 2 );
     declare @Monto26 NUMERIC( 15, 2 );
     declare @Monto27 NUMERIC( 15, 2 );
     declare @Monto28 NUMERIC( 15, 2 );
     declare @Monto29 NUMERIC( 15, 2 );
     declare @Monto30 NUMERIC( 15, 2 );
     declare @Monto31 NUMERIC( 15, 2 );
     declare @STATUS SMALLINT;
     declare @TIPODIA SMALLINT;
     declare @HORAS   NUMERIC( 15, 2 );
     declare @EXTRAS  NUMERIC( 15, 2 );
     declare @DOBLES  NUMERIC( 15, 2 );
     declare @TRIPLES NUMERIC( 15, 2 );
     declare @TARDES  NUMERIC( 15, 2 );
     declare @DES_TRA NUMERIC( 15, 2 );
     declare @PER_CG  NUMERIC( 15, 2 );
     declare @PER_SG  NUMERIC( 15, 2 );
     declare @TotHabil   SMALLINT;
     declare @TotSabado  SMALLINT;
     declare @TotDescan  SMALLINT;
     declare @TotNormal  SMALLINT;
     declare @TotIncapa  SMALLINT;
     declare @TotVaca    SMALLINT;
     declare @TotPer_CG  SMALLINT;
     declare @TotPer_SG  SMALLINT;
     declare @TotPer_FJ  SMALLINT;
     declare @TotSuspen  SMALLINT;
     declare @TotPer_OT  SMALLINT;
     declare @TotFestivo SMALLINT;
     declare @TotNo_Trab SMALLINT;
     declare @TotTrabajo SMALLINT;
     declare @TotFaltas  SMALLINT;
     declare @TotRetardo SMALLINT;
     declare @TotHoras   NUMERIC( 15, 2 );
     declare @TotExtras  NUMERIC( 15, 2 );
     declare @TotDobles  NUMERIC( 15, 2 );
     declare @TotDes_Tra NUMERIC( 15, 2 );
     declare @TotHor_CG  NUMERIC( 15, 2 );
     declare @TotHor_SG  NUMERIC( 15, 2 );
     declare @TotTardes  NUMERIC( 15, 2 );
     declare Temporal CURSOR for
        select A.AU_FECHA,
               A.AU_STATUS,
               A.AU_TIPODIA,
               A.AU_HORAS,
               A.AU_EXTRAS,
               A.AU_DOBLES,
               A.AU_TRIPLES,
               A.AU_TIPO,
               A.AU_TARDES,
               A.AU_DES_TRA,
               A.AU_PER_CG,
               A.AU_PER_SG
        from DBO.AUSENCIA as A where
        ( A.CB_CODIGO = @Empleado ) and
        ( A.AU_FECHA between @FECHA_INI and @FECHA_FIN )
        order by A.AU_FECHA

     set @Cuantos = 0;
     set @Monto01 = 0;
     set @Monto02 = @Monto01;
     set @Monto03 = @Monto01;
     set @Monto04 = @Monto01;
     set @Monto05 = @Monto01;
     set @Monto06 = @Monto01;
     set @Monto07 = @Monto01;
     set @Monto08 = @Monto01;
     set @Monto09 = @Monto01;
     set @Monto10 = @Monto01;
     set @Monto11 = @Monto01;
     set @Monto12 = @Monto01;
     set @Monto13 = @Monto01;
     set @Monto14 = @Monto01;
     set @Monto15 = @Monto01;
     set @Monto16 = @Monto01;
     set @Monto17 = @Monto01;
     set @Monto18 = @Monto01;
     set @Monto19 = @Monto01;
     set @Monto20 = @Monto01;
     set @Monto21 = @Monto01;
     set @Monto22 = @Monto01;
     set @Monto23 = @Monto01;
     set @Monto24 = @Monto01;
     set @Monto25 = @Monto01;
     set @Monto26 = @Monto01;
     set @Monto27 = @Monto01;
     set @Monto28 = @Monto01;
     set @Monto29 = @Monto01;
     set @Monto30 = @Monto01;
     set @Monto31 = @Monto01;
     set @AuTipo01 = '';
     set @AuTipo02 = @AuTipo01;
     set @AuTipo03 = @AuTipo01;
     set @AuTipo04 = @AuTipo01;
     set @AuTipo05 = @AuTipo01;
     set @AuTipo06 = @AuTipo01;
     set @AuTipo07 = @AuTipo01;
     set @AuTipo08 = @AuTipo01;
     set @AuTipo09 = @AuTipo01;
     set @AuTipo10 = @AuTipo01;
     set @AuTipo11 = @AuTipo01;
     set @AuTipo12 = @AuTipo01;
     set @AuTipo13 = @AuTipo01;
     set @AuTipo14 = @AuTipo01;
     set @AuTipo15 = @AuTipo01;
     set @AuTipo16 = @AuTipo01;
     set @AuTipo17 = @AuTipo01;
     set @AuTipo18 = @AuTipo01;
     set @AuTipo19 = @AuTipo01;
     set @AuTipo20 = @AuTipo01;
     set @AuTipo21 = @AuTipo01;
     set @AuTipo22 = @AuTipo01;
     set @AuTipo23 = @AuTipo01;
     set @AuTipo24 = @AuTipo01;
     set @AuTipo25 = @AuTipo01;
     set @AuTipo26 = @AuTipo01;
     set @AuTipo27 = @AuTipo01;
     set @AuTipo28 = @AuTipo01;
     set @AuTipo29 = @AuTipo01;
     set @AuTipo30 = @AuTipo01;
     set @AuTipo31 = @AuTipo01;
     set @TotHabil   = 0;
     set @TotSabado  = 0;
     set @TotDescan  = 0;
     set @TotNormal  = 0;
     set @TotIncapa  = 0;
     set @TotVaca    = 0;
     set @TotPer_CG  = 0;
     set @TotPer_SG  = 0;
     set @TotPer_FJ  = 0;
     set @TotSuspen  = 0;
     set @TotPer_OT  = 0;
     set @TotFestivo = 0;
     set @TotNo_Trab = 0;
     set @TotTrabajo = 0;
     set @TotFaltas  = 0;
     set @TotRetardo = 0;
     set @TotHoras   = 0;
     set @TotExtras  = 0;
     set @TotDobles  = 0;
     set @TotDes_Tra = 0;
     set @TotHor_CG  = 0;
     set @TotHor_SG  = 0;
     set @TotTardes  = 0;
     Open Temporal;
     fetch NEXT from Temporal
     into @FechaActual,
          @Status,
          @TipoDia,
          @Horas,
          @Extras,
          @Dobles,
          @Triples,
          @Tipo,
          @Tardes,
          @Des_Tra,
          @Per_CG,
          @Per_SG;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @TipoDia = 9 )
          begin
               if ( @Primero = 1 )
                  set @TotNo_Trab = @TotNo_Trab + 1;
          end
          else
          begin
               if ( @Cuantos = 0 )
                  set @Fecha_Min = @FechaActual;
               set @Fecha_Max = @FechaActual;
               set @Cuantos = @Cuantos + 1;
               if ( @Primero = 1 )
               begin
                    if ( @Status = 0 )
                       set @TotHabil = @TotHabil + 1
                    else
                        if ( @Status = 1 )
                            set @TotSabado = @TotSabado + 1
                        else
                            set @TotDescan = @TotDescan + 1;
                    if ( @TipoDia = 0 )
                       set @TotNormal = @TotNormal + 1
                    else
                    if ( @TipoDia = 1 )
                       set @TotIncapa = @TotIncapa + 1
                    else
                    if ( @TipoDia = 2 )
                       set @TotVaca = @TotVaca + 1
                    else
                    if ( @TipoDia = 3 )
                       set @TotPer_CG = @TotPer_CG + 1
                    else
                    if ( @TipoDia = 4 )
                       set @TotPer_SG = @TotPer_SG + 1
                    else
                    if ( @TipoDia = 5 )
                       set @TotPer_FJ = @TotPer_FJ + 1
                    else
                    if ( @TipoDia = 6 )
                       set @TotSuspen = @TotSuspen + 1
                    else
                    if ( @TipoDia = 7 )
                       set @TotPer_OT = @TotPer_OT + 1
                    else
                        set @TotFestivo = @TotFestivo + 1;
                    if ( @Horas > 0 )
                       set @TotTrabajo = @TotTrabajo + 1
                    else
                        if ( ( @Status = 0 ) AND ( @TipoDia = 0 ) )
                           set @TotFaltas = @TotFaltas + 1;
                    if ( @Tardes > 0 )
                       set @TotRetardo = @TotRetardo + 1;
                    set @TotHoras   = @TotHoras + @Horas;
                    set @TotExtras  = @TotExtras + @Extras;
                    set @TotDobles  = @TotDobles + @Dobles;
                    set @TotDes_Tra = @TotDes_Tra + @Des_Tra;
                    set @TotHor_CG  = @TotHor_CG + @Per_CG;
                    set @TotHor_SG  = @TotHor_SG + @Per_SG;
                    set @TotTardes  = @TotTardes + @Tardes;
               end;
          end;
          if ( @Campo = 0 )
             set @Monto = @Horas
          else
          if ( @Campo = 1 )
             set @Monto = @Extras
          else
          if ( @Campo = 2 )
             set @Monto = @Dobles
          else
          if ( @Campo = 3 )
             set @Monto = @Triples
          else
          if ( @Campo = 4 )
             set @Monto = @Tardes
          else
          if ( @Campo = 5 )
             set @Monto = @Des_Tra
          else
          if ( @Campo = 6 )
             set @Monto = @Per_CG
          else
          if ( @Campo = 7 )
             set @Monto = @Per_SG
          else
          if ( @Campo = 8 )
             set @Monto = @Status
          else
          if ( @Campo = 9 )
             set @Monto = @TipoDia
          else
              set @Monto = 0;
          set @Posicion = CONVERT( SmallInt, @FechaActual - @Fecha_Mes ) + 1;
          if ( @Posicion =  1 )
          begin
               set @AuTipo01 = @Tipo;
               set @Monto01 = @Monto;
          end
          else if ( @Posicion =  2 )
          begin
               set @AuTipo02 = @Tipo;
               set @Monto02 = @Monto;
          end
          else if ( @Posicion =  3 )
          begin
               set @AuTipo03 = @Tipo;
               set @Monto03 = @Monto;
          end
          else if ( @Posicion =  4 )
          begin
               set @AuTipo04 = @Tipo;
               set @Monto04 = @Monto;
          end
          else if ( @Posicion =  5 )
          begin
               set @AuTipo05 = @Tipo;
               set @Monto05 = @Monto;
          end
          else if ( @Posicion =  6 )
          begin
               set @AuTipo06 = @Tipo;
               set @Monto06 = @Monto;
          end
          else if ( @Posicion =  7 )
          begin
               set @AuTipo07 = @Tipo;
               set @Monto07 = @Monto;
          end
          else if ( @Posicion =  8 )
          begin
               set @AuTipo08 = @Tipo;
               set @Monto08 = @Monto;
          end
          else if ( @Posicion =  9 )
          begin
               set @AuTipo09 = @Tipo;
               set @Monto09 = @Monto;
          end
          else if ( @Posicion = 10 )
          begin
               set @AuTipo10 = @Tipo;
               set @Monto10 = @Monto;
          end
          else if ( @Posicion = 11 )
          begin
               set @AuTipo11 = @Tipo;
               set @Monto11 = @Monto;
          end
          else if ( @Posicion = 12 )
          begin
               set @AuTipo12 = @Tipo;
               set @Monto12 = @Monto;
          end
          else if ( @Posicion = 13 )
          begin
               set @AuTipo13 = @Tipo;
               set @Monto13 = @Monto;
          end
          else if ( @Posicion = 14 )
          begin
               set @AuTipo14 = @Tipo;
               set @Monto14 = @Monto;
          end
          else if ( @Posicion = 15 )
          begin
               set @AuTipo15 = @Tipo;
               set @Monto15 = @Monto;
          end
          else if ( @Posicion = 16 )
          begin
               set @AuTipo16 = @Tipo;
               set @Monto16 = @Monto;
          end
          else if ( @Posicion = 17 )
          begin
               set @AuTipo17 = @Tipo;
               set @Monto17 = @Monto;
          end
          else if ( @Posicion = 18 )
          begin
               set @AuTipo18 = @Tipo;
               set @Monto18 = @Monto;
          end
          else if ( @Posicion = 19 )
          begin
               set @AuTipo19 = @Tipo;
               set @Monto19 = @Monto;
          end
          else if ( @Posicion = 20 )
          begin
               set @AuTipo20 = @Tipo;
               set @Monto20 = @Monto;
          end
          else if ( @Posicion = 21 )
          begin
               set @AuTipo21 = @Tipo;
               set @Monto21 = @Monto;
          end
          else if ( @Posicion = 22 )
          begin
               set @AuTipo22 = @Tipo;
               set @Monto22 = @Monto;
          end
          else if ( @Posicion = 23 )
          begin
               set @AuTipo23 = @Tipo;
               set @Monto23 = @Monto;
          end
          else if ( @Posicion = 24 )
          begin
               set @AuTipo24 = @Tipo;
               set @Monto24 = @Monto;
          end
          else if ( @Posicion = 25 )
          begin
               set @AuTipo25 = @Tipo;
               set @Monto25 = @Monto;
          end
          else if ( @Posicion = 26 )
          begin
               set @AuTipo26 = @Tipo;
               set @Monto26 = @Monto;
          end
          else if ( @Posicion = 27 )
          begin
               set @AuTipo27 = @Tipo;
               set @Monto27 = @Monto;
          end
          else if ( @Posicion = 28 )
          begin
               set @AuTipo28 = @Tipo;
               set @Monto28 = @Monto;
          end
          else if ( @Posicion = 29 )
          begin
               set @AuTipo29 = @Tipo;
               set @Monto29 = @Monto;
          end
          else if ( @Posicion = 30 )
          begin
               set @AuTipo30 = @Tipo;
               set @Monto30 = @Monto;
          end
          else if ( @Posicion = 31 )
          begin
               set @AuTipo31 = @Tipo;
               set @Monto31 = @Monto;
          end
          fetch NEXT from Temporal
          into @FechaActual,
               @Status,
               @TipoDia,
               @Horas,
               @Extras,
               @Dobles,
               @Triples,
               @Tipo,
               @Tardes,
               @Des_Tra,
               @Per_CG,
               @Per_SG;
     end
	 close Temporal;
	 deallocate Temporal;
     if ( @Cuantos > 0 )
     begin
          insert into DBO.TMPCALHR
         ( US_CODIGO, CB_CODIGO, TL_YEAR, TL_MES, TL_CAMPO, TL_FEC_MIN, TL_FEC_MAX, TL_NUMERO,
           TL_CAMPO01, TL_CAMPO02, TL_CAMPO03, TL_CAMPO04, TL_CAMPO05, TL_CAMPO06, TL_CAMPO07, TL_CAMPO08, TL_CAMPO09, TL_CAMPO10,
           TL_CAMPO11, TL_CAMPO12, TL_CAMPO13, TL_CAMPO14, TL_CAMPO15, TL_CAMPO16, TL_CAMPO17, TL_CAMPO18, TL_CAMPO19, TL_CAMPO20,
           TL_CAMPO21, TL_CAMPO22, TL_CAMPO23, TL_CAMPO24, TL_CAMPO25, TL_CAMPO26, TL_CAMPO27, TL_CAMPO28, TL_CAMPO29, TL_CAMPO30, TL_CAMPO31,
           TL_TIPO01, TL_TIPO02, TL_TIPO03, TL_TIPO04, TL_TIPO05, TL_TIPO06, TL_TIPO07, TL_TIPO08, TL_TIPO09, TL_TIPO10,
           TL_TIPO11, TL_TIPO12, TL_TIPO13, TL_TIPO14, TL_TIPO15, TL_TIPO16, TL_TIPO17, TL_TIPO18, TL_TIPO19, TL_TIPO20,
           TL_TIPO21, TL_TIPO22, TL_TIPO23, TL_TIPO24, TL_TIPO25, TL_TIPO26, TL_TIPO27, TL_TIPO28, TL_TIPO29, TL_TIPO30, TL_TIPO31,
           TL_HABIL, TL_SABADO, TL_DESCAN, TL_NORMAL, TL_INCAPA, TL_VACA, TL_PER_CG, TL_PER_SG, TL_PER_FJ, TL_SUSPEN, TL_PER_OT, TL_FESTIVO,
           TL_NO_TRAB, TL_TRABAJO, TL_FALTAS, TL_RETARDO, TL_HORAS, TL_EXTRAS, TL_DOBLES, TL_DES_TRA, TL_HOR_CG, TL_HOR_SG, TL_TARDES )
       values
         ( @Usuario, @Empleado, @Year, @Month, @Campo, @Fecha_Min, @Fecha_Max, @Cuantos,
           @Monto01, @Monto02, @Monto03, @Monto04, @Monto05, @Monto06, @Monto07, @Monto08, @Monto09, @Monto10,
           @Monto11, @Monto12, @Monto13, @Monto14, @Monto15, @Monto16, @Monto17, @Monto18, @Monto19, @Monto20,
           @Monto21, @Monto22, @Monto23, @Monto24, @Monto25, @Monto26, @Monto27, @Monto28, @Monto29, @Monto30, @Monto31,
           @AuTipo01, @AuTipo02, @AuTipo03, @AuTipo04, @AuTipo05, @AuTipo06, @AuTipo07, @AuTipo08, @AuTipo09, @AuTipo10,
           @AuTipo11, @AuTipo12, @AuTipo13, @AuTipo14, @AuTipo15, @AuTipo16, @AuTipo17, @AuTipo18, @AuTipo19, @AuTipo20,
           @AuTipo21, @AuTipo22, @AuTipo23, @AuTipo24, @AuTipo25, @AuTipo26, @AuTipo27, @AuTipo28, @AuTipo29, @AuTipo30, @AuTipo31,
           @TotHabil, @TotSabado, @TotDescan, @TotNormal, @TotIncapa, @TotVaca, @TotPer_CG, @TotPer_SG, @TotPer_FJ, @TotSuspen, @TotPer_OT, @TotFestivo,
           @TotNo_Trab, @TotTrabajo, @TotFaltas, @TotRetardo, @TotHoras, @TotExtras, @TotDobles, @TotDes_Tra, @TotHor_CG, @TotHor_SG, @TotTardes );
     end;
end
GO

/* Patch 385: Considerar campos KARDEX.CB_MOD_NV1..KARDEX.CB_MOD_NV9*/
/* Patch 385: # Seq: 162 Modificado */
ALTER PROCEDURE RECALCULA_KARDEX(@Empleado INTEGER) AS
BEGIN
   SET NOCOUNT ON
   declare @kAUTOSAL  CHAR(1);
   declare @kClasifi  CHAR(6);
   declare @kCONTRAT  CHAR(1);
   declare @kFAC_INT  NUMERIC(15,5);
   declare @kFEC_ANT  Fecha;
   declare @kFEC_CON  Fecha;
   declare @kFEC_ING  Fecha;
   declare @kFEC_INT  Fecha;
   declare @kFEC_REV  Fecha;
   declare @kFECHA_2  Fecha;
   declare @kFECHA    Fecha;
   declare @kMOT_BAJ  CHAR(3);
   declare @kNivel1   CHAR(6);
   declare @kNivel2   CHAR(6);
   declare @kNivel3   CHAR(6);
   declare @kNivel4   CHAR(6);
   declare @kNivel5   CHAR(6);
   declare @kNivel6   CHAR(6);
   declare @kNivel7   CHAR(6);
   declare @kNivel8   CHAR(6);
   declare @kNivel9   CHAR(6);
   declare @kNOMNUME  SMALLINT;
   declare @kNOMTIPO  SMALLINT;
   declare @kNOMYEAR  SMALLINT;
   declare @kPatron   CHAR(1);
   declare @kPER_VAR  NUMERIC(15,2);
   declare @kPuesto   CHAR(6);
   declare @kSAL_INT  NUMERIC(15,2);
   declare @kSALARIO  NUMERIC(15,2);
   declare @kTABLASS  CHAR(1);
   declare @kTurno    CHAR(6);
   declare @kZona_GE  CHAR(1);
   declare @kOLD_SAL  NUMERIC(15,2);
   declare @kOLD_INT  NUMERIC(15,2);
   declare @kOTRAS_P  NUMERIC(15,2);
   declare @kPRE_INT  NUMERIC(15,2);
   declare @kREINGRE  Char(1);
   declare @kSAL_TOT  NUMERIC(15,2);
   declare @kTOT_GRA  NUMERIC(15,2);
   declare @kTIPO     CHAR(6);
   declare @kRANGO_S  NUMERIC(15,2);
   declare @kMOD_NV1  CHAR(1);
   declare @kMOD_NV2  CHAR(1);
   declare @kMOD_NV3  CHAR(1);
   declare @kMOD_NV4  CHAR(1);
   declare @kMOD_NV5  CHAR(1);
   declare @kMOD_NV6  CHAR(1);
   declare @kMOD_NV7  CHAR(1);
   declare @kMOD_NV8  CHAR(1);
   declare @kMOD_NV9  CHAR(1);

   declare @eACTIVO   CHAR(1);
   declare @eAUTOSAL  CHAR(1);
   declare @eClasifi  CHAR(6);
   declare @eCONTRAT  CHAR(1);
   declare @eFAC_INT  NUMERIC(15,5);
   declare @eFEC_ANT  Fecha;
   declare @eFEC_CON  Fecha;
   declare @eFEC_ING  Fecha;
   declare @eFEC_INT  Fecha;
   declare @eFEC_REV  Fecha;
   declare @eFEC_SAL  Fecha;
   declare @eFEC_BAJ  Fecha;
   declare @eFEC_BSS  Fecha;
   declare @eMOT_BAJ  CHAR(3);
   declare @eNivel1   CHAR(6);
   declare @eNivel2   CHAR(6);
   declare @eNivel3   CHAR(6);
   declare @eNivel4   CHAR(6);
   declare @eNivel5   CHAR(6);
   declare @eNivel6   CHAR(6);
   declare @eNivel7   CHAR(6);
   declare @eNivel8   CHAR(6);
   declare @eNivel9   CHAR(6);
   declare @eNOMNUME  SMALLINT;
   declare @eNOMTIPO  SMALLINT;
   declare @eNOMYEAR  SMALLINT;
   declare @ePatron   CHAR(1);
   declare @ePER_VAR  NUMERIC(15,2);
   declare @ePuesto   CHAR(6);
   declare @eSAL_INT  NUMERIC(15,2);
   declare @eSALARIO  NUMERIC(15,2);
   declare @eTABLASS  CHAR(1);
   declare @eTIP_REV  CHAR(6);
   declare @eTurno    CHAR(6);
   declare @eZona_GE  CHAR(1);
   declare @eOLD_SAL  NUMERIC(15,2);
   declare @eOLD_INT  NUMERIC(15,2);
   declare @eOTRAS_P  NUMERIC(15,2);
   declare @ePRE_INT  NUMERIC(15,2);
   declare @eREINGRE  Char(1);
   declare @eSAL_TOT  NUMERIC(15,2);
   declare @eTOT_GRA  NUMERIC(15,2);
   declare @eRANGO_S  NUMERIC(15,2);
   declare @eFEC_NIV  Fecha;
   declare @eFEC_PTO  Fecha;
   declare @eFEC_TUR  Fecha;
   declare @FechaVacia Fecha;
   declare @eFEC_KAR  Fecha;
   declare CursorKardex CURSOR SCROLL_LOCKS FOR
	select	CB_AUTOSAL, CB_CLASIFI,   CB_CONTRAT, CB_FAC_INT, CB_FEC_ANT,
		CB_FEC_CON, CB_FEC_ING,   CB_FEC_INT, CB_FEC_REV, CB_FECHA,    CB_FECHA_2 ,
		CB_MOT_BAJ, CB_NIVEL1,    CB_NIVEL2 , CB_NIVEL3 , CB_NIVEL4,   CB_NIVEL5 ,   CB_NIVEL6 ,
		CB_NIVEL7 , CB_NIVEL8,    CB_NIVEL9 , CB_NOMNUME ,CB_NOMTIPO , CB_NOMYEAR ,
		CB_OTRAS_P, CB_PATRON ,   CB_PER_VAR ,CB_PRE_INT ,
		CB_PUESTO , CB_REINGRE ,  CB_SAL_INT ,CB_SAL_TOT, CB_SALARIO,  CB_TABLASS ,
		CB_TIPO ,   CB_TOT_GRA,   CB_TURNO ,  CB_ZONA_GE, CB_RANGO_S
	from Kardex
	where CB_CODIGO = @Empleado
	order by cb_fecha,cb_nivel
	

	
      	SET @FechaVacia = '12/30/1899';
	SET @eACTIVO  = 'S';
      	SET @eFEC_BAJ = @FechaVacia;
      	SET @eFEC_BSS = @FechaVacia;
      	SET @eMOT_BAJ = ' ';
      	SET @eNOMYEAR = 0;
      	SET @eNOMTIPO = 0;
      	SET @eNOMNUME = 0;
      	SET @eREINGRE = 'N';
      	SET @eSALARIO = 0;
      	SET @eSAL_INT = 0;
      	SET @eFAC_INT = 0;
      	SET @ePRE_INT = 0;
      	SET @ePER_VAR = 0;
      	SET @eSAL_TOT = 0;
      	SET @eAUTOSAL = 'N';
      	SET @eTOT_GRA = 0;
      	SET @eFEC_REV = @FechaVacia;
      	SET @eFEC_INT = @FechaVacia;
      	SET @eOTRAS_P = 0;
      	SET @eOLD_SAL = 0;
      	SET @eOLD_INT = 0;
      	SET @eRANGO_S = 0;

	Open CursorKardex
	Fetch NEXT from CursorKardex
	into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
                @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
   	        @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
	        @kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
                @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
                @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
                @kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S

	
	if ( @@FETCH_STATUS <> 0 )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RETURN
	END
	
	
	if ( @kTIPO <> 'ALTA' )
	BEGIN
		Close CursorKardex
		Deallocate CursorKardex
		RAISERROR( 'Empleado #%d, ERROR en KARDEX: Movimiento(s) previo(s) a la ALTA', 16, 1, @Empleado )
		RETURN
	END

	while ( @@FETCH_STATUS = 0 )
	BEGIN
        	if ( @kTIPO = 'ALTA' OR @kTIPO = 'CAMBIO' OR @kTIPO = 'CIERRE' )
          	BEGIN
			if ABS( @kSALARIO - @eSALARIO ) >= 0.01
			BEGIN
				SET @kOLD_SAL = @eSALARIO;
				SET @kFEC_REV = @kFECHA;
				SET @eFEC_REV = @kFEC_REV;
				SET @eSALARIO = @kSALARIO;
				SET @eOLD_SAL = @kOLD_SAL;
			END
			else
			BEGIN
				SET @kOLD_SAL = @eOLD_SAL;
				SET @kFEC_REV = @eFEC_REV;
			END
			if ABS( @kSAL_INT - @eSAL_INT ) >= 0.01
			BEGIN
				SET @kOLD_INT = @eSAL_INT;
				if ( @kTIPO = 'CAMBIO' )
					SET @kFEC_INT = @kFECHA_2;
				else
					SET @kFEC_INT = @kFECHA;
				SET @eFEC_INT = @kFEC_INT;
				SET @eSAL_INT = @kSAL_INT;
				SET @eOLD_INT = @kOLD_INT;
			END
			else
			BEGIN
                		SET @kOLD_INT = @eOLD_INT;
	        		SET @kFEC_INT = @eFEC_INT;
			END
			SET @eFAC_INT = @kFAC_INT;
			SET @ePRE_INT = @kPRE_INT;
			SET @ePER_VAR = @kPER_VAR;
			SET @eSAL_TOT = @kSAL_TOT;
			SET @eTOT_GRA = @kTOT_GRA;
			SET @eOTRAS_P = @kOTRAS_P;
			SET @eZONA_GE = @kZONA_GE;
			SET @eTIP_REV = @kTIPO;
			SET @eFEC_SAL = @kFECHA;
			SET @eRANGO_S = @kRANGO_S;
		END
		else
		BEGIN
			SET @kSALARIO = @eSALARIO;
			SET @kOLD_SAL = @eOLD_SAL;
			SET @kFEC_REV = @eFEC_REV;
			SET @kSAL_INT = @eSAL_INT;
			SET @kOLD_INT = @eOLD_INT;
			SET @kFEC_INT = @eFEC_INT;
			SET @kFAC_INT = @eFAC_INT;
			SET @kPRE_INT = @ePRE_INT;
			SET @kPER_VAR = @ePER_VAR;
			SET @kSAL_TOT = @eSAL_TOT;
			SET @kTOT_GRA = @eTOT_GRA;
			SET @kOTRAS_P = @eOTRAS_P;
			SET @kZONA_GE = @eZONA_GE;
			SET @kRANGO_S = @eRANGO_S;
		END

		if ( @kTIPO = 'ALTA' or @kTIPO = 'PUESTO' or @kTIPO = 'CAMBIO' or @kTIPO = 'CIERRE' )
			SET @eAUTOSAL = @kAUTOSAL;
		else
			SET @kAUTOSAL = @eAUTOSAL;

		if ( @kAUTOSAL <> 'S' )
		BEGIN
			SET @kRANGO_S = 0;
			SET @eRANGO_S = 0;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PUESTO' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @ePUESTO   = @kPUESTO;
			SET @eCLASIFI  = @kCLASIFI;
			SET @eFEC_PTO  = @kFECHA;
		END
		else
		BEGIN
			SET @kPUESTO   = @ePUESTO;
			SET @kCLASIFI  = @eCLASIFI;
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'AREA' OR @kTIPO = 'CIERRE' )
		BEGIN
			if ( ( @kTIPO = 'AREA' ) OR ( @kTIPO = 'CIERRE' ) )
			BEGIN
				if ( @kNIVEL1 <> @eNIVEL1 )
     					SET @kMOD_NV1 = 'S';
            			else
                 			SET @kMOD_NV1 = 'N';
				if ( @kNIVEL2 <> @eNIVEL2 )
     					SET @kMOD_NV2 = 'S';
            			else
                 			SET @kMOD_NV2 = 'N';
				if ( @kNIVEL3 <> @eNIVEL3 )
     					SET @kMOD_NV3 = 'S';
            			else
                 			SET @kMOD_NV3 = 'N';
				if ( @kNIVEL4 <> @eNIVEL4 )
     					SET @kMOD_NV4 = 'S';
            			else
                 			SET @kMOD_NV4 = 'N';
				if ( @kNIVEL5 <> @eNIVEL5 )
     					SET @kMOD_NV5 = 'S';
            			else
                 			SET @kMOD_NV5 = 'N';
				if ( @kNIVEL6 <> @eNIVEL6 )
     					SET @kMOD_NV6 = 'S';
            			else
                 			SET @kMOD_NV6 = 'N';
				if ( @kNIVEL7 <> @eNIVEL7 )
     					SET @kMOD_NV7 = 'S';
            			else
                 			SET @kMOD_NV7 = 'N';
				if ( @kNIVEL8 <> @eNIVEL8 )
     					SET @kMOD_NV8 = 'S';
            			else
                 			SET @kMOD_NV8 = 'N';
				if ( @kNIVEL9 <> @eNIVEL9 )
     					SET @kMOD_NV9 = 'S';
            			else
                 			SET @kMOD_NV9 = 'N';
			END
			else
			BEGIN
                    		if ( @kNIVEL1 <> '' )
                         		SET @kMOD_NV1 = 'S';
                    		else
                         		SET @kMOD_NV1 = 'N';
                    		if ( @kNIVEL2 <> '' )
                         		SET @kMOD_NV2 = 'S';
                    		else
                         		SET @kMOD_NV2 = 'N';
                    		if ( @kNIVEL3 <> '' )
                         		SET @kMOD_NV3 = 'S';
                    		else
                         		SET @kMOD_NV3 = 'N';
                    		if ( @kNIVEL4 <> '' )
                         		SET @kMOD_NV4 = 'S';
                    		else
                         		SET @kMOD_NV4 = 'N';
                    		if ( @kNIVEL5 <> '' )
                         		SET @kMOD_NV5 = 'S';
                    		else
                         		SET @kMOD_NV5 = 'N';
                    		if ( @kNIVEL6 <> '' )
                         		SET @kMOD_NV6 = 'S';
                    		else
                         		SET @kMOD_NV6 = 'N';
                    		if ( @kNIVEL7 <> '' )
                         		SET @kMOD_NV7 = 'S';
                    		else
                         		SET @kMOD_NV7 = 'N';
                    		if ( @kNIVEL8 <> '' )
                         		SET @kMOD_NV8 = 'S';
                    		else
                         		SET @kMOD_NV8 = 'N';
                    		if ( @kNIVEL9 <> '' )
                         		SET @kMOD_NV9 = 'S';
                    		else
                         		SET @kMOD_NV9 = 'N';
				
			END
			SET @eNIVEL1 = @kNIVEL1;
			SET @eNIVEL2 = @kNIVEL2;
			SET @eNIVEL3 = @kNIVEL3;
			SET @eNIVEL4 = @kNIVEL4;
			SET @eNIVEL5 = @kNIVEL5;
			SET @eNIVEL6 = @kNIVEL6;
			SET @eNIVEL7 = @kNIVEL7;
			SET @eNIVEL8 = @kNIVEL8;
			SET @eNIVEL9 = @kNIVEL9;
			SET @eFEC_NIV = @kFECHA;
		END
		else
		BEGIN
			SET @kNIVEL1 = @eNIVEL1;
			SET @kNIVEL2 = @eNIVEL2;
			SET @kNIVEL3 = @eNIVEL3;
			SET @kNIVEL4 = @eNIVEL4;
			SET @kNIVEL5 = @eNIVEL5;
			SET @kNIVEL6 = @eNIVEL6;
			SET @kNIVEL7 = @eNIVEL7;
			SET @kNIVEL8 = @eNIVEL8;
			SET @kNIVEL9 = @eNIVEL9;
               		SET @kMOD_NV1 = 'N';
               		SET @kMOD_NV2 = 'N';
               		SET @kMOD_NV3 = 'N';
               		SET @kMOD_NV4 = 'N';
               		SET @kMOD_NV5 = 'N';
               		SET @kMOD_NV6 = 'N';
               		SET @kMOD_NV7 = 'N';
               		SET @kMOD_NV8 = 'N';
               		SET @kMOD_NV9 = 'N';
		END

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'PRESTA' OR @kTIPO = 'CIERRE' )
			SET @eTABLASS = @kTABLASS;
		else
			SET @kTABLASS = @eTABLASS;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'TURNO' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eTURNO   = @kTURNO;
			SET @eFEC_TUR = @kFECHA;
		END
		else
			SET @kTURNO   = @eTURNO;

		if ( @kTIPO = 'ALTA' OR @kTIPO = 'RENOVA' OR @kTIPO = 'CIERRE' )
		BEGIN
			SET @eCONTRAT = @kCONTRAT;
			SET @eFEC_CON = @kFEC_CON;
		END
		else
		BEGIN
			SET @kCONTRAT = @eCONTRAT;
			SET @kFEC_CON = @eFEC_CON;
		END

		if ( @kTIPO = 'ALTA' )
		BEGIN
			if ( @eACTIVO = 'S' )
				SET @kREINGRE = 'N';
            		else
			BEGIN
				SET @kREINGRE = 'S';
				SET @eREINGRE = 'S';
			END

			SET @eFEC_ING = @kFECHA;
			SET @eFEC_ANT = @kFEC_ANT;
			SET @eACTIVO  = 'S';
			SET @ePATRON  = @kPATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END
		else
		BEGIN
			SET @kREINGRE = @eREINGRE;
			SET @kPATRON  = @ePATRON;
			SET @kFEC_ING = @eFEC_ING;
			SET @kFEC_ANT = @eFEC_ANT;
		END

		if ( @kTIPO = 'BAJA' )
		BEGIN
			SET @eFEC_BAJ  =  @kFECHA;
			SET @eFEC_BSS  =  @kFECHA_2;
			SET @eMOT_BAJ  =  @kMOT_BAJ;
			SET @eACTIVO   =  'N';
			SET @eNOMYEAR  =  @kNOMYEAR;
			SET @eNOMTIPO  =  @kNOMTIPO;
			SET @eNOMNUME  =  @kNOMNUME;
		END
		else
		BEGIN
			SET @kMOT_BAJ = @eMOT_BAJ;
			SET @kNOMYEAR = @eNOMYEAR;
			SET @kNOMTIPO = @eNOMTIPO;
			SET @kNOMNUME = @eNOMNUME;
		END

		
		update KARDEX
     		set 	CB_AUTOSAL = @kAUTOSAL,
			CB_CLASIFI = @kCLASIFI,
			CB_CONTRAT = @kCONTRAT,
			CB_FEC_ANT = @kFEC_ANT,
			CB_FEC_CON = @kFEC_CON,
			CB_FEC_ING = @kFEC_ING,
			CB_FEC_INT = @kFEC_INT,
			CB_FEC_REV = @kFEC_REV,
			CB_FECHA_2 = @kFECHA_2,
			CB_MOT_BAJ = @kMOT_BAJ,
			CB_NIVEL1  = @kNIVEL1,
			CB_NIVEL2  = @kNIVEL2,
			CB_NIVEL3  = @kNIVEL3,
			CB_NIVEL4  = @kNIVEL4,
			CB_NIVEL5  = @kNIVEL5,
			CB_NIVEL6  = @kNIVEL6,
			CB_NIVEL7  = @kNIVEL7,
			CB_NIVEL8  = @kNIVEL8,
			CB_NIVEL9  = @kNIVEL9,
			CB_MOD_NV1 = @kMOD_NV1,
			CB_MOD_NV2 = @kMOD_NV2,
			CB_MOD_NV3 = @kMOD_NV3,
			CB_MOD_NV4 = @kMOD_NV4,
			CB_MOD_NV5 = @kMOD_NV5,
			CB_MOD_NV6 = @kMOD_NV6,
			CB_MOD_NV7 = @kMOD_NV7,
			CB_MOD_NV8 = @kMOD_NV8,
			CB_MOD_NV9 = @kMOD_NV9,
			CB_NOMNUME = @kNOMNUME,
			CB_NOMTIPO = @kNOMTIPO,
			CB_NOMYEAR = @kNOMYEAR,
			CB_OTRAS_P = @kOTRAS_P,
			CB_PATRON  = @kPATRON,
			CB_PER_VAR = @kPER_VAR,
			CB_PRE_INT = @kPRE_INT,
			CB_PUESTO  = @kPUESTO,
			CB_REINGRE = @kREINGRE,
			CB_SAL_INT = @kSAL_INT,
			CB_SAL_TOT = @kSAL_TOT,
			CB_SALARIO = @kSALARIO,
			CB_TABLASS = @kTABLASS,
			CB_TOT_GRA = @kTOT_GRA,
			CB_TURNO   = @kTURNO,
			CB_ZONA_GE = @kZONA_GE,
			CB_OLD_SAL = @kOLD_SAL,
			CB_OLD_INT = @kOLD_INT,
			CB_FAC_INT = @kFAC_INT,
			CB_RANGO_S = @kRANGO_S
		where CURRENT OF CursorKardex


		Fetch NEXT from CursorKardex
		into	@kAUTOSAL, @kCLASIFI, @kCONTRAT,  @kFAC_INT,  @kFEC_ANT ,
	                @kFEC_CON, @kFEC_ING, @kFEC_INT , @kFEC_REV  ,@kFECHA,   @kFECHA_2,
   		        @kMOT_BAJ ,@kNivel1,  @kNivel2 ,  @kNivel3,   @kNivel4,  @kNivel5,  @kNivel6,
	        	@kNivel7,  @kNivel8,  @kNivel9,   @kNOMNUME , @kNOMTIPO, @kNOMYEAR,
	                @kOTRAS_P, @kPatron,  @kPER_VAR,  @kPRE_INT,
        	        @kPuesto,  @kREINGRE, @kSAL_INT,  @kSAL_TOT,  @kSALARIO, @kTABLASS,
                	@kTIPO,    @kTOT_GRA, @kTurno,    @kZONA_GE,  @kRANGO_S
	END

	Close CursorKardex
	Deallocate CursorKardex


	
	SET @eFEC_KAR = @eFEC_ING;
	if ( @eFEC_NIV > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_NIV;
	if ( @eFEC_SAL > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_SAL;
	if ( @eFEC_PTO > @eFEC_KAR ) SET @eFEC_KAR = @eFEC_PTO;
	if ( @eFEC_TUR > @eFEC_KAR )  SET @eFEC_KAR = @eFEC_TUR;

	update COLABORA
	set 	CB_AUTOSAL = @eAUTOSAL,
		CB_ACTIVO  = @eACTIVO,
		CB_CLASIFI = @eCLASIFI,
		CB_CONTRAT = @eCONTRAT,
		CB_FAC_INT = @eFAC_INT,
		CB_FEC_ANT = @eFEC_ANT,
		CB_FEC_CON = @eFEC_CON,
		CB_FEC_ING = @eFEC_ING,
		CB_FEC_INT = @eFEC_INT,
		CB_FEC_REV = @eFEC_REV,
		CB_FEC_SAL = @eFEC_SAL,
		CB_FEC_BAJ = @eFEC_BAJ,
		CB_FEC_BSS = @eFEC_BSS,
		CB_MOT_BAJ = @eMOT_BAJ,
		CB_NIVEL1  = @eNIVEL1,
		CB_NIVEL2  = @eNIVEL2,
		CB_NIVEL3  = @eNIVEL3,
		CB_NIVEL4  = @eNIVEL4,
		CB_NIVEL5  = @eNIVEL5,
		CB_NIVEL6  = @eNIVEL6,
		CB_NIVEL7  = @eNIVEL7,
		CB_NIVEL8  = @eNIVEL8,
		CB_NIVEL9  = @eNIVEL9,
		CB_NOMNUME = @eNOMNUME,
		CB_NOMTIPO = @eNOMTIPO,
		CB_NOMYEAR = @eNOMYEAR,
		CB_PATRON  = @ePATRON,
		CB_PER_VAR = @ePER_VAR,
		CB_PUESTO  = @ePUESTO,
		CB_SAL_INT = @eSAL_INT,
		CB_SALARIO = @eSALARIO,
		CB_TABLASS = @eTABLASS,
		CB_TURNO   = @eTURNO,
		CB_ZONA_GE = @eZONA_GE,
		CB_OLD_SAL = @eOLD_SAL,
		CB_OLD_INT = @eOLD_INT,
		CB_PRE_INT = @ePRE_INT,
		CB_SAL_TOT = @eSAL_TOT,
		CB_TOT_GRA = @eTOT_GRA,
		CB_TIP_REV = @eTIP_REV,
		CB_RANGO_S = @eRANGO_S,
		CB_FEC_NIV = @eFEC_NIV,
		CB_FEC_PTO = @eFEC_PTO,
		CB_FEC_TUR = @eFEC_TUR,
		CB_FEC_KAR = @eFEC_KAR
	where ( CB_CODIGO = @Empleado );
END
GO

/* Patch 385: Considerar CHECADAS.US_COD_OK, CHECADAS.CH_HOR_DES */
/* Patch 385: # Seq: 76 Modificado */
ALTER FUNCTION AUTORIZACIONES_SELECT
(    @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS @RegAuto TABLE (
    Resultado NUMERIC(15,2),
    Motivo CHAR(4),
    US_COD_OK SMALLINT,
    HRS_APROB NUMERIC(15,2) )
AS
BEGIN
	declare @Resultado NUMERIC(15,2)
	declare	@Motivo CHAR(4)
        declare @US_COD_OK SMALLINT
        declare @HRS_APROB  NUMERIC(15,2)

	if ( @Tipo = 7 )
	begin
		select @Resultado = CH_HOR_EXT,
                       @Motivo = CH_RELOJ,
                       @US_COD_OK = US_COD_OK,
                       @HRS_APROB = CH_HOR_DES from CHECADAS
          	where 	( CB_CODIGO = @Empleado ) and
                	( AU_FECHA = @Fecha ) and
                	( CH_TIPO = @Tipo )
     	end
     	else
     	begin
          	select @Resultado = CH_HOR_ORD,
                       @Motivo = CH_RELOJ,
                       @US_COD_OK = US_COD_OK,
                       @HRS_APROB = CH_HOR_DES from CHECADAS
          	where 	( CB_CODIGO = @Empleado ) and
                	( AU_FECHA = @Fecha ) and
                	( CH_TIPO = @Tipo )
     	end
     	if ( @Resultado is Null )
     	begin
          	set @Resultado = 0
          	set @Motivo = ''
     	end

  	insert into @RegAuto
	values ( @Resultado, @Motivo, @US_COD_OK, @HRS_APROB )
	return
END
GO

/* Patch 385: Considerar CHECADAS.US_COD_OK */
/* Patch 385: # Seq: 205 Agregado */
CREATE FUNCTION AUTORIZACIONES_SELECT_APROBO
(   @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS SMALLINT
AS
BEGIN
	declare @Resultado SMALLINT
	select @Resultado = US_COD_OK from dbo.AUTORIZACIONES_SELECT( @Empleado, @Fecha, @Tipo )

	RETURN @Resultado
END
GO

/* Patch 385: Considerar CHECADAS.CH_HOR_DES */
/* Patch 385: # Seq: 206 Agregado */
CREATE FUNCTION AUTORIZACIONES_SELECT_HRSAPROBADAS
(   @Empleado INTEGER,
    @Fecha DATETIME,
    @Tipo SMALLINT )
RETURNS NUMERIC(15,2)
AS
BEGIN
	declare @Resultado NUMERIC(15,2)
	select @Resultado = HRS_APROB from dbo.AUTORIZACIONES_SELECT( @Empleado, @Fecha, @Tipo )

	RETURN @Resultado
END
GO

/* Patch 385: Leer Autorizaciones para Aprobar ( Workflow ) en Supervisores */
/* Patch 385: # Seq: 207 Agregado */
CREATE FUNCTION SP_AUTO_X_APROBAR(
  @Fecha Fecha,
  @Usuario Usuario )
RETURNS @HorasXAprobar TABLE (
  CB_CODIGO Integer,
  CB_NIVEL CHAR(6),
  CH_TIPO SmallInt,
  CH_HORAS Numeric( 15, 2 ),
  CH_MOTIVO VARCHAR(4),
  CH_MOT_DES VARCHAR(30),
  CH_HOR_OK Numeric( 15, 2 ),
  US_CODIGO SmallInt,
  US_COD_OK SmallInt )
as
begin
     declare @Extras Horas;
     declare @CB_CODIGO NumeroEmpleado;
     declare @CB_NIVEL Codigo;

     declare Empleados cursor for
     select CB_CODIGO, CB_NIVEL
     from SP_LISTA_EMPLEADOS( @Fecha, @Usuario );

     Open Empleados
     Fetch Next From Empleados into @CB_CODIGO, @CB_NIVEL;
     while ( @@Fetch_Status = 0 )
     begin
           insert into @HorasXAprobar
           select @CB_CODIGO, @CB_NIVEL, CH.CH_TIPO,( CH.CH_HOR_EXT + CH.CH_HOR_ORD ),
                  CH.CH_RELOJ, M.TB_ELEMENT, CH.CH_HOR_DES, CH.US_CODIGO, CH.US_COD_OK
           from CHECADAS CH
           left outer join MOT_AUTO M on ( M.TB_CODIGO = CH.CH_RELOJ )
           where ( CH.CB_CODIGO = @CB_CODIGO ) and
                 ( CH.AU_FECHA = @Fecha ) and
                 ( CH.CH_TIPO in ( 5, 6, 7, 8, 9, 10 ) )
           order by CH.CH_TIPO;
     end
     close Empleados;
     deallocate Empleados;
     RETURN
end
GO

/* Patch 385: Leer Autorizaciones para Aprobar ( Workflow ) en Tress */
/* Patch 385: # Seq: 208 Agregado */
CREATE FUNCTION SP_AUTO_X_APROBAR_TODOS( @Fecha Fecha )
RETURNS @HorasXAprobar TABLE (
  CB_CODIGO Integer,
  CB_NIVEL CHAR(6),
  CH_TIPO SmallInt,
  CH_HORAS Numeric( 15, 2 ),
  CH_MOTIVO VARCHAR(4),
  CH_MOT_DES VARCHAR(30),
  CH_HOR_OK Numeric( 15, 2 ),
  US_CODIGO SmallInt,
  US_COD_OK SmallInt )
as
begin
     declare @Extras Horas;
     declare @CB_CODIGO NumeroEmpleado;
     declare @CB_NIVEL Codigo;
     declare @NumNivel smallint;
     declare @Nivel1 CHAR(6);
     declare @Nivel2 CHAR(6);
     declare @Nivel3 CHAR(6);
     declare @Nivel4 CHAR(6);
     declare @Nivel5 CHAR(6);
     declare @Nivel6 CHAR(6);
     declare @Nivel7 CHAR(6);
     declare @Nivel8 CHAR(6);
     declare @Nivel9 CHAR(6);
     declare @FechaNivel Fecha;
     declare @Anterior NumeroEmpleado;
     declare @CH_TIPO SmallInt;
     declare @CH_HORAS Numeric( 15, 2 );
     declare @CH_MOTIVO VARCHAR(4);
     declare @CH_MOT_DES VARCHAR(30);
     declare @CH_HOR_OK Numeric( 15, 2 );
     declare @US_CODIGO SmallInt;
     declare @US_COD_OK SmallInt;

     select @Anterior = -1;
     /* Nivel de Organigrama de Supervisores */
     select @NumNivel = ( select CAST( GL_FORMULA AS SMALLINT )
                          from GLOBAL
                          where GL_CODIGO = 120 )

     /* Checadas */
     declare Checadas cursor for
     select CH.CB_CODIGO, CH.CH_TIPO, ( CH.CH_HOR_EXT + CH.CH_HOR_ORD ), CH.CH_RELOJ, M.TB_ELEMENT, CH.CH_HOR_DES, CH.US_CODIGO, CH.US_COD_OK
     from CHECADAS CH
     left outer join MOT_AUTO M on ( M.TB_CODIGO = CH.CH_RELOJ )
     where ( CH.AU_FECHA = @Fecha ) and
     ( CH.CH_TIPO in ( 5, 6, 7, 8, 9, 10 ) )
     order by CH.CB_CODIGO, CH.CH_TIPO;

     Open Checadas
     Fetch Next From Checadas into
     @CB_CODIGO, @CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK;

     while ( @@Fetch_Status = 0 )
     begin
          if ( @CB_CODIGO <> @Anterior )
          begin
               select
               @FechaNivel = C.CB_FEC_NIV,
               @Nivel1 = C.CB_NIVEL1,
               @Nivel2 = C.CB_NIVEL2,
               @Nivel3 = C.CB_NIVEL3,
               @Nivel4 = C.CB_NIVEL4,
               @Nivel5 = C.CB_NIVEL5,
               @Nivel6 = C.CB_NIVEL6,
               @Nivel7 = C.CB_NIVEL7,
               @Nivel8 = C.CB_NIVEL8,
               @Nivel9 = C.CB_NIVEL9
               from COLABORA C
               where ( C.CB_CODIGO = @CB_CODIGO )

               if ( @Fecha < @FechaNivel )
                       select @CB_NIVEL = dbo.SP_FECHA_NIVEL( @Fecha, @CB_CODIGO )
               else if ( @NumNivel = 1 ) select @CB_NIVEL = @Nivel1;
               else if ( @NumNivel = 2 ) select @CB_NIVEL = @Nivel2;
               else if ( @NumNivel = 3 ) select @CB_NIVEL = @Nivel3;
               else if ( @NumNivel = 4 ) select @CB_NIVEL = @Nivel4;
               else if ( @NumNivel = 5 ) select @CB_NIVEL = @Nivel5;
               else if ( @NumNivel = 6 ) select @CB_NIVEL = @Nivel6;
               else if ( @NumNivel = 7 ) select @CB_NIVEL = @Nivel7;
               else if ( @NumNivel = 8 ) select @CB_NIVEL = @Nivel8;
               else select @CB_NIVEL = @Nivel9

               select @Anterior = @CB_CODIGO;
          end
            /* Lista de Checadas */
          insert into @HorasXAprobar ( CB_CODIGO, CB_NIVEL, CH_TIPO, CH_HORAS, CH_MOTIVO, CH_MOT_DES, CH_HOR_OK, US_CODIGO, US_COD_OK )
          values ( @CB_CODIGO, @CB_NIVEL,@CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK )

          Fetch Next From Checadas into
          @CB_CODIGO, @CH_TIPO, @CH_HORAS,@CH_MOTIVO,@CH_MOT_DES,@CH_HOR_OK, @US_CODIGO,@US_COD_OK;
     end
     close Checadas;
     deallocate Checadas;

     RETURN
end
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV1 en FECHA_KARDEX */
/* Patch 385: # Seq: 210 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV1(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV1 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV1 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV2 en FECHA_KARDEX */
/* Patch 385: # Seq: 211 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV2(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV2 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV2 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV3 en FECHA_KARDEX */
/* Patch 385: # Seq: 212 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV3(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV3 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV3 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV4 en FECHA_KARDEX */
/* Patch 385: # Seq: 213 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV4(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV4 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV4 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV5 en FECHA_KARDEX */
/* Patch 385: # Seq: 214 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV5(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV5 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV5 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV6 en FECHA_KARDEX */
/* Patch 385: # Seq: 215 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV6(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV6 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV6 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV7 en FECHA_KARDEX */
/* Patch 385: # Seq: 216 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV7(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV7 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV7 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV8 en FECHA_KARDEX */
/* Patch 385: # Seq: 217 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV8(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV8 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV8 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Para reportar KARDEX.CB_MOD_NV9 en FECHA_KARDEX */
/* Patch 385: # Seq: 218 Agregado */
CREATE FUNCTION SP_KARDEX_CB_MOD_NV9(
  @FECHA DATETIME,
  @EMPLEADO INTEGER
) RETURNS Booleano
AS
BEGIN
	declare @Regresa Booleano;
	set @Regresa = ( select TOP 1 CB_MOD_NV9 from KARDEX where
                     ( CB_CODIGO = @Empleado ) and ( CB_FECHA <= @Fecha )
	                 order by CB_FECHA desc, CB_NIVEL desc )
	if ( @Regresa is NULL )
		set @Regresa = ( select TOP 1 CB_MOD_NV9 from KARDEX where
                         ( CB_CODIGO = @Empleado )
                         order by CB_FECHA, CB_NIVEL )
	RETURN COALESCE( @Regresa, 'N' )
END
GO

/* Patch 385: Considerar ART_80.TI_INICIO */
/* Patch 385: # Seq: 80 Modificado */
ALTER function DBO.SP_A80_ESCALON( @CODIGO SMALLINT, @LIMITEINFERIOR NUMERIC( 15, 2 ),
@FECHA DATETIME )
returns @RESULTADO table ( CUOTA NUMERIC( 15, 2 ) NOT NULL, TASA NUMERIC( 15, 5 ) NOT NULL )
as
begin
     declare @V_CUOTA NUMERIC( 15, 2 );
     declare @V_TASA NUMERIC( 15, 5 );
     declare @V_FECHA DATETIME;

     select @V_FECHA = MAX( TI_INICIO )
     from T_ART_80
     where ( NU_CODIGO = @CODIGO ) and
           ( TI_INICIO <= @FECHA );

     if ( @V_FECHA is not null )
     begin
          select @V_CUOTA = A1.A80_CUOTA,
                 @V_TASA = A1.A80_TASA from DBO.ART_80 A1
          where ( A1.NU_CODIGO = @CODIGO ) and
                ( A1.TI_INICIO = @V_FECHA ) and
                ( A1.A80_LI = ( select MAX( A2.A80_LI ) from DBO.ART_80 A2
                                where ( A2.NU_CODIGO = @CODIGO ) and
                                      ( A2.TI_INICIO = @V_FECHA ) and
                                      ( A2.A80_LI <= @LIMITEINFERIOR ) ) );
     end

     if ( @V_FECHA is null ) or ( @V_Tasa is null )
     begin
          set @V_TASA = 0;
          set @V_CUOTA = 0;
     end

     insert into @RESULTADO values( @V_CUOTA, @V_TASA );
     RETURN
end
GO

/* Patch 385: Considerar Fecha de Vigencia para SP_A80_ESCALON */
/* Patch 385: # Seq: 5 Modificado */
ALTER PROCEDURE ACTUALIZA_TASA_INFONAVIT(
	@Empleado Integer,
	@Fecha Fecha,
	@FechaCaptura Fecha,
	@Usuario SmallInt,
	@TasaNueva NUMERIC(15, 2) OUTPUT )
AS
BEGIN
  SET NOCOUNT ON
  declare @SalarioIntegrado Numeric( 15, 2);
  declare @SalarioMinimo Numeric( 15, 2 );
  declare @FechaSalarioMinimo Fecha;
  declare @TasaIndex Numeric( 15, 2 );
  declare @ZonaGeografica Char( 1 );
  declare @Resultado SmallInt;
  declare @FechaIngreso Fecha;

  SET @TasaNueva = 0;

  select @TasaIndex = CB_INF_OLD, @ZonaGeografica = CB_ZONA_GE
  from   COLABORA
  where ( CB_CODIGO = @Empleado );

  select @FechaSalarioMinimo = MAX( SM_FEC_INI )
  from SAL_MIN
  where ( SM_FEC_INI <= @Fecha )


  if ( @ZonaGeografica = 'A' )
          select @SalarioMinimo = SM_ZONA_A
          from SAL_MIN
          where ( SM_FEC_INI = @FechaSalarioMinimo )
  else if ( @ZonaGeografica = 'B' )
          select @SalarioMinimo = SM_ZONA_B
          from SAL_MIN
          where ( SM_FEC_INI = @FechaSalarioMinimo )
  else if ( @ZonaGeografica = 'C' )
          select @SalarioMinimo = SM_ZONA_C
          from SAL_MIN
          where ( SM_FEC_INI = @FechaSalarioMinimo )
  else
	  SET @SalarioMinimo = 0;

  if ( @SalarioMinimo > 0 )
  BEGIN
    select @SalarioIntegrado = CB_SAL_INT
    from   dbo.SP_FECHA_KARDEX( @Fecha, @Empleado );

    SET @SalarioIntegrado = @SalarioIntegrado / @SalarioMinimo;

    select @TasaNueva = TASA
    from   dbo.SP_A80_ESCALON( @TasaIndex, @SalarioIntegrado, @Fecha );

    update COLABORA 
    set CB_INFTASA = @TasaNueva
    where ( CB_CODIGO = @Empleado );

    EXECUTE KARDEX_TASA_INFONAVIT @Empleado, @Fecha, @TasaNueva, '',
                                  @FechaCaptura, 'S', @Usuario,
                                  @Resultado OUTPUT, @FechaIngreso OUTPUT

    if @Resultado <> 0
	SET @TasaNueva = 0
  END
END
GO

/* Amplia la funcionalidad de SP_TITULO_CAMPO para agregar un prefijo al Titulo */
/* Patch 385: # Seq: 185 Agregado */
CREATE  PROCEDURE DBO.SP_TITULO_CAMPO_PLUS(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@PREFIJO VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
BEGIN
     SET NOCOUNT ON;
     DECLARE @TITULO VARCHAR(30);
     select @Titulo = GL_FORMULA from GLOBAL where ( GL_CODIGO = @NumGlobal );
     if ( ( @Titulo is Null ) or ( @Titulo = '' ) )
        SET @Titulo = @TituloDefault;

     SET @Titulo = SUBSTRING ( @Prefijo + @Titulo , 1 , 30 ) ;

     update DICCION set DI_TITULO = @Titulo,
                        DI_TCORTO= @Titulo,
                        DI_CLAVES = ''
     where ( DI_CLASIFI in ( 5, 10, 18, 32, 33, 50, 59, 124, 141, 142, 143, 144, 145, 146, 147, 148, 149, 152, 186, 187, 191, 218 ) )
     and ( DI_NOMBRE = @Nombre );
END
GO

/* Cambiar para invocar SP_TITULO_CAMPO_PLUS */
/* Patch 385: # Seq: 186 Modificado */
ALTER PROCEDURE DBO.SP_TITULO_CAMPO(
	@NUMGLOBAL SMALLINT,
	@TITULODEFAULT VARCHAR(30),
	@NOMBRE VARCHAR(30))
AS
BEGIN
     SET NOCOUNT ON;
     execute  SP_TITULO_CAMPO_PLUS @NumGlobal, @TituloDefault, '', @Nombre
END
GO

/* Considerar los campos KARDEX.CB_MOD_NV<x> */
/* Patch 385: # Seq: 195 Modificado */
ALTER PROCEDURE DBO.SP_REFRESH_DICCION
as
begin
  SET NOCOUNT ON
  execute  SP_TITULO_TABLA 13, 'Nivel de Organigrama #1', 41 ;
  execute  SP_TITULO_TABLA 14, 'Nivel de Organigrama #2', 42 ;
  execute  SP_TITULO_TABLA 15, 'Nivel de Organigrama #3', 43 ;
  execute  SP_TITULO_TABLA 16, 'Nivel de Organigrama #4', 44 ;
  execute  SP_TITULO_TABLA 17, 'Nivel de Organigrama #5', 45 ;
  execute  SP_TITULO_TABLA 18, 'Nivel de Organigrama #6', 46 ;
  execute  SP_TITULO_TABLA 19, 'Nivel de Organigrama #7', 47 ;
  execute  SP_TITULO_TABLA 20, 'Nivel de Organigrama #8', 48 ;
  execute  SP_TITULO_TABLA 21, 'Nivel de Organigrama #9', 49 ;

  execute  SP_TITULO_TABLA 35, 'Tabla Adicional #1', 19
  execute  SP_TITULO_TABLA 36, 'Tabla Adicional #2', 20
  execute  SP_TITULO_TABLA 37, 'Tabla Adicional #3', 21
  execute  SP_TITULO_TABLA 38, 'Tabla Adicional #4', 22

  execute  SP_TITULO_TABLA 134, 'Orden de Trabajo', 149
  execute  SP_TITULO_TABLA 136, 'Cat�logo de Partes', 141
  execute  SP_TITULO_TABLA 138, 'Cat�logo de Area', 148
  execute  SP_TITULO_TABLA 140, 'Cat�logo de Operaciones', 142
  execute  SP_TITULO_TABLA 142, 'Cat�logo de Tiempo Muerto', 138
  execute  SP_TITULO_TABLA 176, 'Lista de Defectos', 187
  execute  SP_TITULO_TABLA 143, 'Modulador #1', 135
  execute  SP_TITULO_TABLA 144, 'Modulador #2', 136
  execute  SP_TITULO_TABLA 145, 'Modulador #3', 137

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'CB_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'CB_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'CB_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'CB_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'CB_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'CB_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'CB_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'CB_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'CB_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'Nuevo Nivel #1', 'EV_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'Nuevo Nivel #2', 'EV_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'Nuevo Nivel #3', 'EV_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'Nuevo Nivel #4', 'EV_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'Nuevo Nivel #5', 'EV_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'Nuevo Nivel #6', 'EV_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'Nuevo Nivel #7', 'EV_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'Nuevo Nivel #8', 'EV_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'Nuevo Nivel #9', 'EV_NIVEL9'

  execute  SP_TITULO_CAMPO 13, 'C�digo de Nivel #1', 'PU_NIVEL1'
  execute  SP_TITULO_CAMPO 14, 'C�digo de Nivel #2', 'PU_NIVEL2'
  execute  SP_TITULO_CAMPO 15, 'C�digo de Nivel #3', 'PU_NIVEL3'
  execute  SP_TITULO_CAMPO 16, 'C�digo de Nivel #4', 'PU_NIVEL4'
  execute  SP_TITULO_CAMPO 17, 'C�digo de Nivel #5', 'PU_NIVEL5'
  execute  SP_TITULO_CAMPO 18, 'C�digo de Nivel #6', 'PU_NIVEL6'
  execute  SP_TITULO_CAMPO 19, 'C�digo de Nivel #7', 'PU_NIVEL7'
  execute  SP_TITULO_CAMPO 20, 'C�digo de Nivel #8', 'PU_NIVEL8'
  execute  SP_TITULO_CAMPO 21, 'C�digo de Nivel #9', 'PU_NIVEL9'

  execute  SP_TITULO_CAMPO 22, 'Fecha Adicional #1', 'CB_G_FEC_1'
  execute  SP_TITULO_CAMPO 23, 'Fecha Adicional #2', 'CB_G_FEC_2'
  execute  SP_TITULO_CAMPO 24, 'Fecha Adicional #3', 'CB_G_FEC_3'
  execute  SP_TITULO_CAMPO 25, 'Texto Adicional #1', 'CB_G_TEX_1'
  execute  SP_TITULO_CAMPO 26, 'Texto Adicional #2', 'CB_G_TEX_2'
  execute  SP_TITULO_CAMPO 27, 'Texto Adicional #3', 'CB_G_TEX_3'
  execute  SP_TITULO_CAMPO 28, 'Texto Adicional #4', 'CB_G_TEX_4'
  execute  SP_TITULO_CAMPO 29, 'N�mero Adicional #1', 'CB_G_NUM_1'
  execute  SP_TITULO_CAMPO 30, 'N�mero Adicional #2', 'CB_G_NUM_2'
  execute  SP_TITULO_CAMPO 31, 'N�mero Adicional #3', 'CB_G_NUM_3'
  execute  SP_TITULO_CAMPO 32, 'L�gico Adicional #1', 'CB_G_LOG_1'
  execute  SP_TITULO_CAMPO 33, 'L�gico Adicional #2', 'CB_G_LOG_2'
  execute  SP_TITULO_CAMPO 34, 'L�gico Adicional #3', 'CB_G_LOG_3'
  execute  SP_TITULO_CAMPO 35, 'Tabla Adicional #1','CB_G_TAB_1'
  execute  SP_TITULO_CAMPO 36, 'Tabla Adicional #2','CB_G_TAB_2'
  execute  SP_TITULO_CAMPO 37, 'Tabla Adicional #3','CB_G_TAB_3'
  execute  SP_TITULO_CAMPO 38, 'Tabla Adicional #4','CB_G_TAB_4'

  execute  SP_TITULO_CAMPO 134, 'Orden de Trabajo','WO_NUMBER'
  execute  SP_TITULO_CAMPO 136, 'Parte','AR_CODIGO'
  execute  SP_TITULO_CAMPO 138, 'Area','CB_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CI_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CE_AREA'
  execute  SP_TITULO_CAMPO 138, 'Area','CS_AREA'
  execute  SP_TITULO_CAMPO 140, 'Operaci�n','OP_NUMBER'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','WK_TMUERTO'
  execute  SP_TITULO_CAMPO 142, 'Tiempo Muerto','LX_TMUERTO'
  execute  SP_TITULO_CAMPO 176, 'Defecto','DE_CODIGO'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','LX_MODULA1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','LX_MODULA2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','LX_MODULA3'
  execute  SP_TITULO_CAMPO 143, 'Modulador #1','WK_MOD_1'
  execute  SP_TITULO_CAMPO 144, 'Modulador #2','WK_MOD_2'
  execute  SP_TITULO_CAMPO 145, 'Modulador #3','WK_MOD_3'

  execute  SP_TITULO_CONTEO  161, 'Criterio #1',  'CT_NIVEL_1'
  execute  SP_TITULO_CONTEO  162, 'Criterio #2',  'CT_NIVEL_2'
  execute  SP_TITULO_CONTEO  163, 'Criterio #3',  'CT_NIVEL_3'
  execute  SP_TITULO_CONTEO  164, 'Criterio #4',  'CT_NIVEL_4'
  execute  SP_TITULO_CONTEO  165, 'Criterio #5',  'CT_NIVEL_5'

  execute  SP_TITULO_EXPEDIEN_BOL  'EX_G_BOL', 'Si/No/Texto'
  execute  SP_TITULO_EXPEDIEN  'EX_G_LOG', 'Si/No'
  execute  SP_TITULO_EXPEDIEN  'EX_G_NUM', 'Numero'
  execute  SP_TITULO_EXPEDIEN  'EX_G_TEX', 'Texto'

  execute  SP_TITULO_CAMPO_PLUS 13, 'Nivel #1', 'Mod. ', 'CB_MOD_NV1'
  execute  SP_TITULO_CAMPO_PLUS 14, 'Nivel #2', 'Mod. ', 'CB_MOD_NV2'
  execute  SP_TITULO_CAMPO_PLUS 15, 'Nivel #3', 'Mod. ', 'CB_MOD_NV3'
  execute  SP_TITULO_CAMPO_PLUS 16, 'Nivel #4', 'Mod. ', 'CB_MOD_NV4'
  execute  SP_TITULO_CAMPO_PLUS 17, 'Nivel #5', 'Mod. ', 'CB_MOD_NV5'
  execute  SP_TITULO_CAMPO_PLUS 18, 'Nivel #6', 'Mod. ', 'CB_MOD_NV6'
  execute  SP_TITULO_CAMPO_PLUS 19, 'Nivel #7', 'Mod. ', 'CB_MOD_NV7'
  execute  SP_TITULO_CAMPO_PLUS 20, 'Nivel #8', 'Mod. ', 'CB_MOD_NV8'
  execute  SP_TITULO_CAMPO_PLUS 21, 'Nivel #9', 'Mod. ', 'CB_MOD_NV9'

  execute  SP_CUSTOM_DICCION

end
GO

/* Considerar lA tabla CLASITMP */
/* Patch 385: # Seq: 150 Modificado */
ALTER PROCEDURE SP_CLAS_AUSENCIA
	@Empleado Int,
	@Fecha DateTime
AS
        SET NOCOUNT ON
	Declare @FEC_KAR DateTime
	Declare @PUESTO Char(6)
	Declare @CLASIFI Char(6)
	Declare @TURNO Char(6)
        Declare @NIVEL1 Char(6)
	Declare @NIVEL2 Char(6)
	Declare @NIVEL3 Char(6)
	Declare @NIVEL4 Char(6)
	Declare @NIVEL5 Char(6)
        Declare @NIVEL6 Char(6)
	Declare @NIVEL7 Char(6)
	Declare @NIVEL8 Char(6)
	Declare @NIVEL9 Char(6)

       	declare @T_PUESTO Char(6)
	declare @T_CLASIFI Char(6)
	declare @T_TURNO Char(6)
        declare @T_NIVEL1 Char(6)
	declare @T_NIVEL2 Char(6)
	declare @T_NIVEL3 Char(6)
	declare @T_NIVEL4 Char(6)
	declare @T_NIVEL5 Char(6)
        declare @T_NIVEL6 Char(6)
	declare @T_NIVEL7 Char(6)
	declare @T_NIVEL8 Char(6)
	declare @T_NIVEL9 Char(6)

 	select @FEC_KAR = CB_FEC_KAR,
		@PUESTO = CB_PUESTO,
		@CLASIFI = CB_CLASIFI,
		@TURNO = CB_TURNO,
         	@NIVEL1 = CB_NIVEL1,
		@NIVEL2 = CB_NIVEL2,
		@NIVEL3 = CB_NIVEL3,
		@NIVEL4 = CB_NIVEL4,
		@NIVEL5 = CB_NIVEL5,
         	@NIVEL6 = CB_NIVEL6,
		@NIVEL7 = CB_NIVEL7,
		@NIVEL8 = CB_NIVEL8,
		@NIVEL9 = CB_NIVEL9
  	from   	COLABORA
  	where  CB_CODIGO = @Empleado

  	if ( @FECHA < @FEC_KAR )
  	begin
 		select @PUESTO = CB_PUESTO,
			@CLASIFI = CB_CLASIFI,
			@TURNO = CB_TURNO,
         		@NIVEL1 = CB_NIVEL1,
			@NIVEL2 = CB_NIVEL2, 
			@NIVEL3 = CB_NIVEL3, 
			@NIVEL4 = CB_NIVEL4, 
			@NIVEL5 = CB_NIVEL5,
         		@NIVEL6 = CB_NIVEL6, 
			@NIVEL7 = CB_NIVEL7, 
			@NIVEL8 = CB_NIVEL8, 
			@NIVEL9 = CB_NIVEL9
    		from   SP_FECHA_KARDEX( @Fecha, @Empleado )
  	end

 	select @FEC_KAR = AU_FECHA,
	       @T_PUESTO = CB_PUESTO,
	       @T_CLASIFI = CB_CLASIFI,
	       @T_TURNO = CB_TURNO,
               @T_NIVEL1 = CB_NIVEL1,
	       @T_NIVEL2 = CB_NIVEL2,
	       @T_NIVEL3 = CB_NIVEL3,
	       @T_NIVEL4 = CB_NIVEL4,
	       @T_NIVEL5 = CB_NIVEL5,
               @T_NIVEL6 = CB_NIVEL6,
	       @T_NIVEL7 = CB_NIVEL7,
	       @T_NIVEL8 = CB_NIVEL8,
	       @T_NIVEL9 = CB_NIVEL9
  	from CLASITMP where
        ( CB_CODIGO = @Empleado ) and
        ( AU_FECHA = @Fecha ) and
        ( US_COD_OK <> 0 );
        if ( @FEC_KAR is not NULL )
        begin
             if ( @T_PUESTO <> '' )
             begin
                  select @PUESTO = @T_PUESTO;
             end
             if ( @T_CLASIFI <> '' )
             begin
                  select @CLASIFI = @T_CLASIFI;
             end
             if ( @T_TURNO <> '' )
             begin
                  select @TURNO = @T_TURNO;
             end
             if ( @T_NIVEL1 <> '' )
             begin
                  select @NIVEL1 = @T_NIVEL1;
             end
             if ( @T_NIVEL2 <> '' )
             begin
                  select @NIVEL2 = @T_NIVEL2;
             end
             if ( @T_NIVEL3 <> '' )
             begin
                  select @NIVEL3 = @T_NIVEL3;
             end
             if ( @T_NIVEL4 <> '' )
             begin
                  select @NIVEL4 = @T_NIVEL4;
             end
             if ( @T_NIVEL5 <> '' )
             begin
                  select @NIVEL5 = @T_NIVEL5;
             end
             if ( @T_NIVEL6 <> '' )
             begin
                  select @NIVEL6 = @T_NIVEL6;
             end
             if ( @T_NIVEL7 <> '' )
             begin
                  select @NIVEL7 = @T_NIVEL7;
             end
             if ( @T_NIVEL8 <> '' )
             begin
                  select @NIVEL8 = @T_NIVEL8;
             end
             if ( @T_NIVEL9 <> '' )
             begin
                  select @NIVEL9 = @T_NIVEL9;
             end
        end

  	update AUSENCIA
  	set   CB_PUESTO  = @PUESTO,
        	CB_CLASIFI = @CLASIFI,
        	CB_TURNO   = @TURNO,
        	CB_NIVEL1  = @NIVEL1,
        	CB_NIVEL2  = @NIVEL2,
        	CB_NIVEL3  = @NIVEL3,
        	CB_NIVEL4  = @NIVEL4,
        	CB_NIVEL5  = @NIVEL5,
        	CB_NIVEL6  = @NIVEL6,
        	CB_NIVEL7  = @NIVEL7,
        	CB_NIVEL8  = @NIVEL8,
        	CB_NIVEL9  = @NIVEL9
  	where CB_CODIGO = @Empleado and AU_FECHA = @Fecha;

GO

/* Determinar Candidatos Que Cumplen un Set de Competencias */
/* Patch 385: # Seq: 219 Agregado */
CREATE FUNCTION DBO.SP_CUMPLE_PUESTO ( @EMPLEADO NUMEROEMPLEADO, @PUESTO CODIGO )
RETURNS INTEGER
AS
begin
     declare @ORDEN1 INTEGER;
     declare @ORDEN2 INTEGER;
     declare @CUANTOS INTEGER;
     declare @CONTADOR INTEGER;
     declare @CUMPLE INTEGER;

     select @CONTADOR = 0;
     select @CUANTOS = 0;

     declare COMPETENCIAS cursor for
     select C1.CA_ORDEN, C2.CA_ORDEN from COMP_PTO
     left outer join COMPETEN on ( COMPETEN.CM_CODIGO = COMP_PTO.CM_CODIGO  )
     left outer join EMP_COMP on ( EMP_COMP.CM_CODIGO = COMP_PTO.CM_CODIGO ) and ( EMP_COMP.CB_CODIGO = @EMPLEADO )
     left outer join CALIFICA C2 on ( C2.CA_CODIGO = COMP_PTO.CA_CODIGO )
     left outer join CALIFICA C1 on ( C1.CA_CODIGO = EMP_COMP.CA_CODIGO )
     where ( COMP_PTO.PU_CODIGO = @PUESTO );
     open COMPETENCIAS;
     fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     while ( @@Fetch_Status = 0 )
     begin
          if ( @ORDEN2 is not NULL )
          begin
	       if ( @ORDEN1 >= @ORDEN2 )
	       begin
	            select @CONTADOR = @CONTADOR + 1;
               end
               select @CUANTOS = @CUANTOS + 1;
          end
          fetch NEXT from COMPETENCIAS into @ORDEN1, @ORDEN2;
     end;
     close COMPETENCIAS;
     deallocate COMPETENCIAS;
     if ( @CUANTOS > 0 )
     begin
          select @CUMPLE = ROUND( ( ( @CONTADOR / @CUANTOS ) * 100 ), 0, 1 );
     end
     else
     begin
          select @CUMPLE = 0;
     end
     return @CUMPLE;
end
GO

